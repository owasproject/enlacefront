package mx.altec.enlace.bo;
import java.io.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.FileUtilities;


/**
 * Clase para copiar archivos remotos utilizando rcp del sistema operativo.
 * Fecha de creaci�n: (5/3/01 9:38:25)
 * @author: Rodolfo M�ndez
 */
public class ArchivoRemoto implements Serializable{
	private java.lang.String rutaRemota = "";
	private java.lang.String hostRemoto = "";
	private String USR                  = "";
	private java.lang.String rutaLocal  = "";
	private java.lang.String hostLocal  = "";
    private java.lang.String nombreArchivo = "";
	//private String atributos            = "";  //CBP - Q19958 - 07012005
	/**
	 * Comentario de constructor ArchivoRemoto.
	 */
	public ArchivoRemoto(){
		this.rutaRemota = Global.DIRECTORIO_REMOTO;
		this.hostRemoto = Global.HOST_REMOTO;
		this.USR        = Global.USUARIO_REMOTO;
		this.rutaLocal  = Global.DIRECTORIO_LOCAL;
		this.hostLocal  = Global.HOST_LOCAL;
	}

	/**
	 * Inserte aqu� la descripci�n del m�todo.
	 * Fecha de creaci�n: (5/3/01 9:42:59)
	 * @param Ruta java.lang.String
	 */
	/*
	public ArchivoRemoto( String Ruta ) {
		this.rutaRemota = Ruta;
	}
	*/

	public ArchivoRemoto( String tipo ) {
		if (tipo.equals( "WEB" ) ) {
			this.rutaRemota = Global.DIRECTORIO_REMOTO_WEB;
			this.hostRemoto = Global.HOST_REMOTO_WEB;
		}
	}

    /**
	  * Inserte aqu� la descripci�n del m�todo.
	  * Esta Rutina llama a un shell que utiliza ftp con el get para poder
	  * traer un archivo de una computadora remota a la computadora local
	  * Fecha de creaci�n: (26/9/03 9:52:42)
	  * @return java.lang.String
	  * @param Archivo java.lang.String
	  * @param Destino java.lang.String
	 */

    //private boolean ftpget(String ip, String login, String password, String direcciondondeestaelarchivo, String nombrearchivo)
	//No jala, de esta forma los BASH

    public String getNombreArchivo()
	{
	  return this.nombreArchivo;
	}

    /**
	 * Inserte aqu� la descripci�n del m�todo.
	 * Obtiene el nombre del Archivo
	 * Fecha de creaci�n: (5/3/01 9:52:42)
	 * @return java.lang.String
	 * @param Archivo java.lang.String
	 * @param Destino java.lang.String
	 */
    public void setNombreArchivo(String nombarch)
	{
	  this.nombreArchivo = nombarch;
	}

	public boolean copiaArchivo(String NomArchivo) {
		this.nombreArchivo=NomArchivo;
		
		EIGlobal.mensajePorTrace( "copiaArchivo -> De ruta NAS a LOCAL." , EIGlobal.NivelLog.DEBUG);

		boolean resultado = true;
         
		EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
		
		try {
						
			EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo & envia:Copia de archivo " + Global.N43_PATH_INTERNET + this.nombreArchivo + " a " + Global.DIRECTORIO_LOCAL + "/" + this.nombreArchivo, EIGlobal.NivelLog.DEBUG);
       	    if(FileUtilities.copiarArchivo(Global.N43_PATH_INTERNET + this.nombreArchivo, 
					Global.DIRECTORIO_LOCAL + "/" + this.nombreArchivo))
       	    {
       	          EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
	            
		    }
       	    else
		    {
		    	  EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
		    	  resultado = false;
		    }

		} catch ( Exception e ) {

			EIGlobal.mensajePorTrace("ArchivoRemoto.copiaArchivo -> Error en copia de archivo -> Mensaje: "
					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			resultado = false;
		}

     EIGlobal.mensajePorTrace( "Resultado en copiado de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
	 return resultado;
		
	}
	

	/** Copia el Archivo especificado.
	* Fecha de creaci�n: (5/3/01 9:41:35)*/
	public boolean copia( String Archivo )
	{
		return this.copia( Archivo, this.rutaLocal );
	}
	/**
	 * Copia el Archivo especificado, en la ruta especificada.
	 * Fecha de creaci�n: (19/09/03)
	 * @return boolean
	 * @param Archivo java.lang.String
	 * @param Destino java.lang.String
	 */
//Version JSCH, Proyecto CNBV
	public boolean copiaN43( String Archivo, String Destino ) {

		EIGlobal.mensajePorTrace( "copiaN43 -> Se direcciona a la ruta NAS " , EIGlobal.NivelLog.DEBUG);


		boolean resultado = true;

		if(!this.hostLocal.trim().equals(this.hostRemoto.trim()) ||
		   !this.rutaLocal.trim().equals(this.rutaRemota.trim()) )
		 {
			EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
			
			try {
				
				EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43 & envia:Copia de archivo " + Global.DOWNLOAD_PATH + Archivo + " a " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
	       	    			
				if(FileUtilities.copiarArchivo(Global.DOWNLOAD_PATH + Archivo, Destino + "/" + Archivo))
	       	    {
	       	       
					EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43 & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
		            
			     }
	       	    else
			     {
			    	EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43 & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
			    	resultado = false;
			     }
				
			} catch ( Exception e ) {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copiaN43 -> Error en copia de archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				      resultado = false;
			         }
		 }
		else
		 {
			resultado = false;
			EIGlobal.mensajePorTrace("Archivo Remoto: NO SE REALIZO LA COPIA, hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		 }
		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		
		return resultado;
	}

	/**
	 * Copia el Archivo especificado, en la ruta especificada.
	 * Fecha de creaci�n: (19/09/03)
	 * @return boolean
	 * @param Archivo java.lang.String
	 * @param Destino java.lang.String
	 */
	

	public boolean copiaN43Modi( String Archivo, String Destino ) {

		EIGlobal.mensajePorTrace( "copiaN43Modi -> Se direcciona a la ruta NAS " , EIGlobal.NivelLog.DEBUG);


		boolean resultado = true;

		if(!this.hostLocal.trim().equals(this.hostRemoto.trim()) ||
		   !this.rutaLocal.trim().equals(this.rutaRemota.trim()) )
		 {
			EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
			
			try {
								
				EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43Modi& envia:Copia de archivo " + Destino + Archivo + " a " + Global.DIRECTORIO_LOCAL + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
	       	    if(FileUtilities.copiarArchivo(Destino + Archivo, 
	       	    		Global.DIRECTORIO_LOCAL + "/" + Archivo))
	       	    {
	       	        EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43Modi & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
		            
			     }else
			     {
			    	EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaN43Modi & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
			    	resultado = false;
			     }


				
			} catch ( Exception e ) {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copiaN43Modi -> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				resultado = false;
			}
		 }
		else
		 {
			resultado = false;
			EIGlobal.mensajePorTrace("Archivo Remoto: NO SE REALIZO LA COPIA, hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		 }
		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		
		return resultado;
		
	}

	public boolean copia( String Archivo, String Destino )
	{	
		EIGlobal.mensajePorTrace( "copia (2p) -> Se hace llamado de la ruta NAS" , EIGlobal.NivelLog.DEBUG);


		boolean resultado = true;
	
			try {
				     

				        EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo: " + Destino + "/" + Archivo + " en " + Destino, EIGlobal.NivelLog.DEBUG);				
			            File arch=new File(Global.DIRECTORIO_LOCAL +"/"+ Archivo); 
			            if(arch.exists())
			            { 
			            	
			            	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo:  Archivo existe en ruta " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
			            	
			            }else
			            {			            	
			            	resultado = FileUtilities.copiarArchivo(Destino + "/" + Archivo,Global.DIRECTORIO_LOCAL + "/" + Archivo);
			            	
			            }			

			} catch ( Exception e ) {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copia (2p) -> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				resultado = false;
			}

	
		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		
		return resultado;
	}
	
    public boolean copia( String Archivo, String Destino, String tipoCopia ) {
       
       EIGlobal.mensajePorTrace( "copia (3p)-> Se hace llamado de la ruta NAS" , EIGlobal.NivelLog.DEBUG);


		boolean resultado = true;

			EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
      
			try {   
					 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & Destino: " + Destino, EIGlobal.NivelLog.DEBUG);
					 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & Tipo de Copia: " + tipoCopia, EIGlobal.NivelLog.DEBUG);
					 
					 File arch=new File(Destino +"/"+ Archivo);
					 if (tipoCopia.equals("1"))
					 {
			            tipoCopia = "1";
					 }
					 else
					 {
					  if (tipoCopia.equals("3"))
					  {
						  tipoCopia = "3";
					  }
					  else
					  {
					    if ("2".equals(tipoCopia))					    
					    {
					        tipoCopia = "2";			
					    }
					    else
						{
			                if(arch.exists())
			                {
							  if (tipoCopia != "0" )
							  {
			            	  tipoCopia = "9";
			            	  EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaArchivo:  Archivo existe en ruta " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
							  }
			                }
						}
					  }
					  }

				     switch(Integer.parseInt(tipoCopia)){
				     
				        case 9:
				    	 
				    	     break;
 
						case 1:
   
						     EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia: La ruta es diferente de LR.", EIGlobal.NivelLog.DEBUG);
						     EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia:Copia de archivo " + this.rutaLocal + "/" + Archivo + " a " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
						     resultado = FileUtilities.copiarArchivo(this.rutaLocal + "/" + Archivo, Destino + "/" + Archivo);
                             
							 break;
						case 3:								

							 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia: Nombre del archivo: " + Archivo , EIGlobal.NivelLog.DEBUG);
							 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia: Nombre del archivo2: " + Archivo + "_preprocesar" , EIGlobal.NivelLog.DEBUG);
							
							 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia:Copia de archivo " + this.rutaLocal + "/" + Archivo + " a " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
							
							 resultado = FileUtilities.copiarArchivo(this.rutaLocal + "/" + Archivo, Destino + Archivo + "_preprocesar");
					    								
							 break;
						default:
			
							 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia: La ruta es diferente de RL.", EIGlobal.NivelLog.DEBUG);
						     EIGlobal.mensajePorTrace( "***ArchivoRemoto.copia (3p) & envia:Copia de archivo " + Destino+ "/" + Archivo + " a " + this.rutaLocal  + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
						     FileUtilities.copiarArchivo(Destino + "/" + Archivo, this.rutaLocal  + "/" + Archivo);	

							 break;
					}
				     						 		
				     
			} catch ( Exception e ) {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copia (3p)-> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				      resultado = false;
			        }

		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		
		return resultado;
	}
    
    
	public boolean copiaOtroPathRemoto(String Archivo, String Destino)
	{
		boolean resultado = true;
		 EIGlobal.mensajePorTrace( "copiaOtroPathRemoto -> Se hace el llamado de la ruta NAS" , EIGlobal.NivelLog.DEBUG);

		 EIGlobal.mensajePorTrace(String.valueOf(Thread.activeCount()), EIGlobal.NivelLog.DEBUG);
		
		try {
			   
			
			    EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto & envia:Copia de archivo " + Global.DIRECTORIO_REMOTO_TASASRVR + "/" + Archivo + " a " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
			    if(FileUtilities.copiarArchivo(Global.DIRECTORIO_REMOTO_TASASRVR + "/" + Archivo, 
			    		Destino + "/" + Archivo))
			    {
	       	    	
	       	        EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto  & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
		            
			     }else
			     {
			    	EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto  & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
			    	resultado = false;
			     }
			    
				
      	} catch (Exception e) {
      		EIGlobal.mensajePorTrace("ArchivoRemoto.copiaOtroPathRemoto -> Error al copiar el archivo -> Mensaje: "
					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			     resultado = false;
		        }
	
      	EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		return resultado;
	}
	
	public boolean copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
	{
		 boolean resultado = true;
		 
		 EIGlobal.mensajePorTrace( "copiaOtroPathRemoto (3p)-> Remoto a Local" , EIGlobal.NivelLog.DEBUG);
		 //EIGlobal.mensajePorTrace("Archivo Remoto: hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		
		EIGlobal.mensajePorTrace(String.valueOf(Thread.activeCount()), EIGlobal.NivelLog.DEBUG);
		
		try {
		    
			    EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto (3p) & envia:Copia de archivo " + pathDestino + Archivo + " a " + Destino , EIGlobal.NivelLog.DEBUG);
			    if(FileUtilities.copiarArchivo(pathDestino + Archivo, Destino))
			    {
	       	    	
	       	        EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto (3p) & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
		            
			     }else
			     {
			    	EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto (3p) & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
			    	resultado = false;
			     }
			    
			
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(
						"ArchivoRemoto.copiaOtroPathRemoto -> Mensaje: "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				resultado = false;
			}
			

		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		return resultado;
	}


public boolean copiaLocalARemoto(String Archivo) {
	EIGlobal.mensajePorTrace("metodo copiaLocalARemoto", EIGlobal.NivelLog.DEBUG);
	return this.copiaLocalARemoto(Archivo, this.rutaLocal);
}

public boolean copiaLocalARemoto(String Archivo, String Destino, String rutaremota) {


			this.rutaRemota = rutaremota;
			this.hostRemoto = Global.HOST_REMOTO_WEB;
			this.USR= Global.USUARIO_REMOTO_WEB;
			Destino=this.rutaLocal;

		    boolean resultado = true;

		if(!this.hostLocal.trim().equals(this.hostRemoto.trim()) ||
		   !this.rutaLocal.trim().equals(this.rutaRemota.trim()) )
		 {
	
			EIGlobal.mensajePorTrace( "copiaLocalARemoto -> Se hace llamado de la ruta NAS." , EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace(String.valueOf(Thread.activeCount()), EIGlobal.NivelLog.DEBUG);
			
            //Validar ruta
			try {
			
				    EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaOtroPathRemoto (3p) & envia:Copia de archivo " + Destino + "/" + Archivo + "a" + this.rutaRemota + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
				    if(FileUtilities.copiarArchivo(Destino + "/" + Archivo, 
				    		this.rutaRemota + "/" + Archivo))
				    {	
		       	        EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemoto (3p) & envia: Se copio el archivo correctamente.", EIGlobal.NivelLog.DEBUG);        			
			            
				     }else
				     {
				    	EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemoto (3p) & envia: No se copio el archivo.", EIGlobal.NivelLog.DEBUG);
				    	resultado = false;
				     }
				    
				 
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("ArchivoRemoto.copiaLocalARemoto (3p)-> Error al copiar el archivo -> Mensaje: "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				         resultado = false;
			          }
		 }
 		else
		 {
			resultado = false;
			EIGlobal.mensajePorTrace("Archivo Remoto: NO SE REALIZO LA COPIA, hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		 }

	EIGlobal.mensajePorTrace( "Resultado en copia de archivo" + resultado , EIGlobal.NivelLog.DEBUG);
	return resultado;

}
//Version JSCH, Proyecto CNBV
	public boolean copiaLocalARemoto(String Archivo, String Destino) {
		EIGlobal.mensajePorTrace(
				"<<<<<<<<<<<<<<El valor de Archivo y Destino>>>>" + Archivo
						+ "<<<<<>>>>" + Destino, EIGlobal.NivelLog.DEBUG);
		
		if (Destino.equals("WEB")) {
		    EIGlobal.mensajePorTrace("Entre a validar copiaLocalARemoto(2p) ",
					EIGlobal.NivelLog.DEBUG);
			this.rutaRemota = Global.DIRECTORIO_REMOTO_WEB;
			this.hostRemoto = Global.HOST_REMOTO_WEB;
			this.USR = Global.USUARIO_REMOTO_WEB;
			Destino = this.rutaLocal;
			EIGlobal.mensajePorTrace("this.rutaRemota " + this.rutaRemota,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("this.hostRemoto " + this.hostRemoto,
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("this.USR " + this.USR,
					EIGlobal.NivelLog.DEBUG);
					
		        return FileUtilities.copiarArchivo(this.rutaLocal + "/" + Archivo, 
				      this.rutaRemota + "/" + Archivo);
		        	
		}

		EIGlobal.mensajePorTrace("Archivo Remoto: hostLocal = "
				+ this.hostLocal + ", hostRemoto = " + this.hostRemoto,
				EIGlobal.NivelLog.DEBUG);

		boolean resultado = true;
		

		if (!this.hostLocal.trim().equals(this.hostRemoto.trim())
				|| !this.rutaLocal.trim().equals(this.rutaRemota.trim())) {
			EIGlobal.mensajePorTrace( "copiaLocalARemoto (2p)-> Local a Remoto" , EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace(String.valueOf(Thread.activeCount()),
					EIGlobal.NivelLog.DEBUG);
			
			try {
								
		        File arch=new File(Global.DIRECTORIO_LOCAL +"/"+ Archivo);
		        
		        EIGlobal.mensajePorTrace( "***copiaLocalARemoto (2p):  Ruta Origen: " + this.rutaLocal + "/" + Archivo + " Ruta Destino: " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
		        
		        if(arch.exists())
		        { 
		        	
		        	 EIGlobal.mensajePorTrace( "***copiaLocalARemoto (2p):  Archivo existe en ruta " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
		        	
		        }else
		        {			            	
		        	resultado = FileUtilities.copiarArchivo(this.rutaLocal + "/" + Archivo, Destino + "/" + Archivo);
		        	
		        }	
				 
				
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copiaLocalARemoto(2p) -> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				resultado = false;
			}
		} else {
			resultado = false;
			EIGlobal.mensajePorTrace(
					"ArchivoRemoto.copiaLocalARemoto(2p): NO SE REALIZO LA COPIA, hostLocal = "
							+ this.hostLocal + ", hostRemoto = "
							+ this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		}

		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
		return resultado;
	}
	
//Version JSCH, Proyecto CNBV
	public boolean copiaArchivo(String NomArchivo, String RutaRemota) {
		this.nombreArchivo=NomArchivo;

		boolean resultado = true;
		EIGlobal.mensajePorTrace( "copiaArchivo (2p) -> Remoto a Local" , EIGlobal.NivelLog.DEBUG);
		//EIGlobal.mensajePorTrace("Archivo Remoto: hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
		
		EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
		
		
		try {
		 
			     
			        File arch=new File(Global.DIRECTORIO_LOCAL +"/"+ NomArchivo); 
			        if(arch.exists())
			        { 
			        	
			        	 EIGlobal.mensajePorTrace( "***copiaArchivo (2p):  Archivo existe en ruta " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
			        	
			        }else
			        {			            	
			        	resultado = FileUtilities.copiarArchivo(RutaRemota + NomArchivo, this.rutaLocal + "/" + NomArchivo);
			        	
			        }	
			     
			     
		} catch ( Exception e ) {

			EIGlobal.mensajePorTrace("ArchivoRemoto.copiaArchivo -> Error al copiar el archivo -> Mensaje: "
					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			resultado = false;
		}
		EIGlobal.mensajePorTrace( "Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);
	    return resultado;
		
	}


	// CBP - Q19958 - 07012005

	/* T E S O F E  ***************************************************/
	/** Copia el Archivo especificado con path, en la ruta especificada.
	*  Getronics, Alejandro Rada V.
	*  Fecha de creaci�n: (15/09/2004 10:26 a.m.)
	*  @param Archivo String (nombre de archivo con Path)
	*  @param Destino String (path destino donde se copia)
	*/
		
//Version JSCH, Proyecto CNBV
	public boolean copiaTESOFE( String Archivo, String Destino )
	{
		String [] arch; 
        arch = Archivo.split("/");
        int numobj = arch.length;
        String nomArch = arch [numobj - 1];

		
		EIGlobal.mensajePorTrace( "copiaTESOFE -> Remoto a Local" , EIGlobal.NivelLog.DEBUG);

		boolean resultado = true;

			EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
			
			try
			 {
			  		File archivotesofe=new File(Destino + "/" + nomArch); 
			        if(archivotesofe.exists())
			        { 
			        	
			        	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaTESOFE & Archivo existe en: " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
			        	
			        }else
			        {	
					    EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaTESOFE & envia:Copia de archivo " + Archivo + "a" + Destino + "/" + nomArch, EIGlobal.NivelLog.DEBUG);		            	
			        	FileUtilities.copiarArchivo(Archivo , Destino + "/" + nomArch);
			        	
			        }				
			  

			} catch ( Exception e )
			 {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copiaTESOFE -> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				resultado = false;
			 
			 }
	
		EIGlobal.mensajePorTrace( "TESOFE Resultado transferencia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);	
		
		return resultado;
	}


    /**
	 * Arma el copiando para la copia local a host remoto del archivo con path.
	 * Getronics, Alejandro Rada V.
	 * Fecha de creaci�n: (15/09/2004 10:26 a.m.)
	 * @return boolean
	 * @param Archivo java.lang.String (archivo con path)
	 * @param Destino java.lang.String (ruta donde se copia)
	 */
	
//Version JSCH, Proyecto CNBV	
	public boolean copiaLocalARemotoTESOFE(String Archivo, String Destino)
	 {

		EIGlobal.mensajePorTrace( "copiaLocalARemotoTESOFE -> Local a Remoto" , EIGlobal.NivelLog.DEBUG);

		if ( Destino.equals( "WEB" ) )
		 {
				this.rutaRemota = Global.DIRECTORIO_REMOTO_WEB;
				this.hostRemoto = Global.HOST_REMOTO_WEB;
				this.USR= Global.USUARIO_REMOTO_WEB;
				Destino=this.rutaLocal;
				EIGlobal.mensajePorTrace("this.rutaRemota " + this.rutaRemota, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("this.hostRemoto " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("this.USR " + this.USR, EIGlobal.NivelLog.DEBUG);
		 }

		this.rutaRemota=Global.DIRECTORIO_REMOTO_TESOFE_INTERNET;

			boolean resultado = true;

			if(!this.hostLocal.trim().equals(this.hostRemoto.trim()) ||
			   !this.rutaLocal.trim().equals(this.rutaRemota.trim()) )
			 {
				EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
				

				try {											 
					     
					     File arch=new File(Global.DIRECTORIO_LOCAL +"/"+ Archivo);  
					        if(arch.exists())
					        { 
					        	
					        	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemotoTESOFE:  Copia archivo a Ruta TESOFE " + this.rutaRemota, EIGlobal.NivelLog.DEBUG);
					        	 resultado = FileUtilities.copiarArchivo(Destino + "/" + Archivo, this.rutaRemota + "/" + Archivo);
					        }else
					        {			            	
						    	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemotoTESOFE  & envia: El archivo no existe en la ruta " + Destino + "/" + Archivo, EIGlobal.NivelLog.DEBUG);
						    	        	
					        }	
					     	     
						 
					} catch ( Exception e ) {
						EIGlobal.mensajePorTrace("ArchivoRemoto.copiaLocalARemotoTESOFE -> Error al copiar el archivo-> Mensaje: "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
							  resultado = false;
						   }
			 }
			else
			 {
				resultado = false;
				EIGlobal.mensajePorTrace("TESOFE Archivo Remoto: NO SE REALIZO LA COPIA, hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
			 }


			EIGlobal.mensajePorTrace("TESOFE Resultado en copia de archivo " + resultado , EIGlobal.NivelLog.DEBUG);

			return resultado;
	}

  
	/* ALTA DE CUENTAS  ***************************************************/
	/** Copia el Archivo especificado con path, en la ruta especificada.
	*  Getronics, Alejandro Rada V.
	*  Fecha de creaci�n: (15/06/2005 10:26 a.m.)
	*  @param Archivo String (nombre de archivo con Path)
	*  @param Destino String (path destino donde se copia)
	*/
   
 //Version JSCH, Proyecto CNBV
	public boolean copiaCUENTAS( String Archivo, String Destino )
	{

		String [] arch; 
		Archivo = Archivo.replaceAll("//", "/");	
        arch = Archivo.split("/");
        int numobj = arch.length;
        String nomArch = arch [numobj - 1];
		String nomArchDest = "";
		EIGlobal.mensajePorTrace( "copiaCUENTAS -> Remoto a Local" , EIGlobal.NivelLog.DEBUG);
		
		boolean resultado = true;

			EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
			
			try
			 {	   			  					
					nomArchDest = Destino + "/" + nomArch;
					nomArchDest = nomArchDest.replaceAll("//", "/");
						
			        if(nomArchDest.equals(Archivo))
			        { 
			        	
			        	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaCUENTAS:  Archivo generado en la ruta: " + nomArchDest, EIGlobal.NivelLog.DEBUG);
			        	
			        }else
			        {	
					    EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaCUENTAS & envia:Copia de archivo " + Archivo + "a" + nomArchDest, EIGlobal.NivelLog.DEBUG);		            	
			        	FileUtilities.copiarArchivo(Archivo , nomArchDest);
			        	
			        }					
							  
			} catch ( Exception e )
			 {
				EIGlobal.mensajePorTrace("ArchivoRemoto.copiaCUENTAS -> Error al copiar el archivo -> Mensaje: "
						+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				  resultado = false;
			 }
		
		EIGlobal.mensajePorTrace( "CUENTAS Resultado al copiar el archivo " + resultado , EIGlobal.NivelLog.DEBUG);	
		
		return resultado;
	}


    /**
	 * Arma el copiando para la copia local a host remoto del archivo con path.
	 * Getronics, Alejandro Rada V.
	 * Fecha de creaci�n: (15/06/2005 10:26 a.m.)
	 * @return boolean
	 * @param Archivo java.lang.String (archivo con path)
	 * @param Destino java.lang.String (ruta donde se copia)
	 */
	
//Version JSCH, Proyecto CNBV
	public boolean copiaLocalARemotoCUENTAS(String Archivo, String Destino)
	 {

		EIGlobal.mensajePorTrace( "copiaLocalARemotoCUENTAS -> Local a Remoto" , EIGlobal.NivelLog.DEBUG);

		if ( Destino.equals( "WEB" ) )
		 {
				this.rutaRemota = Global.DIRECTORIO_REMOTO_WEB;
				this.hostRemoto = Global.HOST_REMOTO_WEB;
				this.USR= Global.USUARIO_REMOTO_WEB;
				Destino=this.rutaLocal;
				EIGlobal.mensajePorTrace("this.rutaRemota " + this.rutaRemota, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("this.hostRemoto " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("this.USR " + this.USR, EIGlobal.NivelLog.DEBUG);
		 }

		this.rutaRemota=Global.DIRECTORIO_REMOTO_CUENTAS;

			boolean resultado = true;

			if(!this.hostLocal.trim().equals(this.hostRemoto.trim()) ||
			   !this.rutaLocal.trim().equals(this.rutaRemota.trim()) )
			 {
				EIGlobal.mensajePorTrace( String.valueOf(Thread.activeCount()) , EIGlobal.NivelLog.DEBUG);
				
				try {
					
				        File arch=new File(Global.DIRECTORIO_LOCAL +"/"+ Archivo); 
				        if(arch.exists())
				        { 
				        	
				        	 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemotoCUENTAS: Copia archivo a Ruta Cuentas:" + this.rutaRemota, EIGlobal.NivelLog.DEBUG);
				        	 resultado = FileUtilities.copiarArchivo(Destino + "/" + Archivo, this.rutaRemota + "/" + Archivo);
				        }else
				        {			            	
							 EIGlobal.mensajePorTrace( "***ArchivoRemoto.copiaLocalARemotoCUENTAS  & envia : Archivo no existe en ruta " + Global.DIRECTORIO_LOCAL + "/" + Archivo, EIGlobal.NivelLog.DEBUG);

				        }
		
						
					} catch ( Exception e ) {
						EIGlobal.mensajePorTrace("ArchivoRemoto.copiaLocalARemotoCUENTAS -> Error en tranferencia de archivo -> Mensaje: "
								+ e.getMessage(), EIGlobal.NivelLog.ERROR);
							  resultado = false;
						   }
			 }
			else
			 {
				resultado = false;
				EIGlobal.mensajePorTrace("CUENTAS Archivo Remoto: NO SE REALIZO LA COPIA, hostLocal = " + this.hostLocal + ", hostRemoto = " + this.hostRemoto, EIGlobal.NivelLog.DEBUG);
			 }


			EIGlobal.mensajePorTrace("CUENTAS Resultado en copia de archivo" + resultado , EIGlobal.NivelLog.DEBUG);

			return resultado;
	}


}