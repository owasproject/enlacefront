
// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;

/**
 * A Bean class.
 * <P>
 * @author Hugo Ru&iacute;z Zepeda
 * Fecha creaci&oacute;n: 12/04/2002
 *
 */
public class IndicaPosicion implements Serializable
{
  private int posicion=0;
  private int direccion=0;

  /**
   * Constructor
   */
  public IndicaPosicion()
  {
  } // Fin contructor


  public int incrementa()
  {
    return posicion++;
  } // Fin incrementa


  public int decrementa()
  {
    if(posicion>0)
      posicion--;

    return posicion;
  } // Fin decrementa


  public int getPosicion()
  {
    return posicion;
  } // Fin getPosicion

  public void setDireccion(int direccion)
  {
    if(direccion==-1)
      this.direccion=direccion;

    if(direccion==1)
      this.direccion=direccion;
  } // Fin m�todo setPosicion


  public int getDireccion()
  {
    return direccion;
  } // Fin m�todo getDireccion

} // Fin clase IndicaPosicion

