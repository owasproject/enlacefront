/****************************************************************************************
*Convertidor de codigos de cliente.
*Autor: Alejandro Rada Vazquez
****************************************************************************************/


package mx.altec.enlace.bo;

import java.io.*;
import java.lang.Integer;

/**
 * Programa que convierte codigos de cliente de 7 caracteres (Letra + 6 numeros) a
 * codigos de cliente de 8 caracteres (numericos),
 * utilizando la siguiente tabla de conversion:
 * <pre>
 * 
 * BD   RE    BD   RE    BD   RE    BD   RE
 * --   --    --   --    --   --    --   --
 * A    10    H    13    O    20    V    26
 * B    11    I    14    P    21    W    27
 * C     *    J    15    Q    22    X    28
 * D     *    K    16    R    23    Y    29
 * E     *    L    17    S     *    Z     *
 * F     *    M    18    T    24    [	  30
 * G    12    N    19    U    25    ]	  31
 * _    32
 * ~	  33
 * #    34
 * * = Excepcion
 * </pre>.
 */
public class  Convertidor implements Serializable {	
	/**
	 * Atributo estatico y constante serialVersionUID.
	 */
	private static final long serialVersionUID = -1965611566153179709L;
	
	/**
	 * Atributo estatico y constante para establecer la base a la cual sera
	 * convertida el codigo de cliente.
	 */
	public static final int BASE = 32;
	
	/**
	 * Metodo estatico para la conversion del codigo de cliente de 7 a 8
	 * posiciones. Soporta conversion hasta el codigo de cliente 99,999,999
	 * 
	 * @param usr (String)con el codigo de cliente de 7 posiciones.
	 * @return String con el codigo de cliente convertido a 8 posiciones.
	 */
	public String cliente_7To8(String usr) {
		if (usr == null) {
			return "";
		}
		String result = null;
		int codCliente;
		if (usr.length() == 0) {
			result = usr;
			return result;
		}
		if (usr.length() == 7) {
			if (usr.charAt(0) >= '0' && usr.charAt(0) <= '9')
				result = "0" + usr;
			else {
				switch (usr.charAt(0)) {
				case 'A':
					result = "10" + usr.substring(1);
					break;
				case 'B':
					result = "11" + usr.substring(1);
					break;
				case 'C':
				case 'D':
				case 'E':
				case 'F':
					result = "00000000".substring(0,8-usr.length()) + usr;
					break;
				case 'G':
					result = "12" + usr.substring(1);
					break;
				case 'H':
					result = "13" + usr.substring(1);
					break;
				case 'I':
					result = "14" + usr.substring(1);
					break;
				case 'J':
					result = "15" + usr.substring(1);
					break;
				case 'K':
					result = "16" + usr.substring(1);
					break;
				case 'L':
					result = "17" + usr.substring(1);
					break;
				case 'M':
					result = "18" + usr.substring(1);
					break;
				case 'N':
					result = "19" + usr.substring(1);
					break;
				case 'O':
					result = "20" + usr.substring(1);
					break;
				case 'P':
					result = "21" + usr.substring(1);
					break;
				case 'Q':
					result = "22" + usr.substring(1);
					break;
				case 'R':
					result = "23" + usr.substring(1);
					break;
				case 'S':
					result = "00000000".substring(0,8-usr.length()) + usr;
					break;
				case 'T':
					result = "24" + usr.substring(1);
					break;
				case 'U':
					result = "25" + usr.substring(1);
					break;
				case 'V':
					result = "26" + usr.substring(1);
					break;
				case 'W':
					result = "27" + usr.substring(1);
					break;
				case 'X':
					result = "28" + usr.substring(1);
					break;
				case 'Y':
					result = "29" + usr.substring(1);
					break;
				case '_':
					result = "30" + usr.substring(1);
					break;
				case '~':
					result = "31" + usr.substring(1);
					break;
				case '#':
					result = "32" + usr.substring(1);
					break;
				case '[':
					result = "33" + usr.substring(1);
					break;
				case ']':
					codCliente = Integer.parseInt(usr.substring(1), BASE);
					result = Integer.toString(codCliente);
					break;
				default:
					result = "00000000".substring(0,8-usr.length()) + usr;
					break;
				}
			}
		} else {
			result = "00000000".substring(0,8-usr.length()) + usr;
		}
		return result;
	}
   
	/**
	 * Metodo estatico para la conversion del codigo de cliente de 8 a 7
	 * posiciones. Soporta conversion desde el codigo de cliente 99999999 hasta
	 * el codigo 00000001.
	 * 
	 * @param usr (String)con el codigo de cliente de 8 posiciones.
	 * @return String con el codigo de cliente convertido a 7 posiciones.
	 */
	public String cliente_8To7(String usr) {
		String result = null;
		int codCliente;
		if (usr.length() == 8) {
			if (usr.charAt(0) == '0')
				result = usr.substring(1);
			else {
				String usuConv = usr.substring(0, 2);
				int usuConvInt = Integer.parseInt(usuConv);

				switch (usuConvInt) {
				case 10:
					result = 'A' + usr.substring(2);
					break;
				case 11:
					result = 'B' + usr.substring(2);
					break;
				case 12:
					result = 'G' + usr.substring(2);
					break;
				case 13:
					result = 'H' + usr.substring(2);
					break;
				case 14:
					result = 'I' + usr.substring(2);
					break;
				case 15:
					result = 'J' + usr.substring(2);
					break;
				case 16:
					result = 'K' + usr.substring(2);
					break;
				case 17:
					result = 'L' + usr.substring(2);
					break;
				case 18:
					result = 'M' + usr.substring(2);
					break;
				case 19:
					result = 'N' + usr.substring(2);
					break;
				case 20:
					result = 'O' + usr.substring(2);
					break;
				case 21:
					result = 'P' + usr.substring(2);
					break;
				case 22:
					result = 'Q' + usr.substring(2);
					break;
				case 23:
					result = 'R' + usr.substring(2);
					break;
				case 24:
					result = 'T' + usr.substring(2);
					break;
				case 25:
					result = 'U' + usr.substring(2);
					break;
				case 26:
					result = 'V' + usr.substring(2);
					break;
				case 27:
					result = 'W' + usr.substring(2);
					break;
				case 28:
					result = 'X' + usr.substring(2);
					break;
				case 29:
					result = 'Y' + usr.substring(2);
					break;
				case 30:
					result = '_' + usr.substring(2);
					break;
				case 31:
					result = '~' + usr.substring(2);
					break;
				case 32:
					result = '#' + usr.substring(2);
					break;
				case 33:
					result = '[' + usr.substring(2);
					break;
				default:
					if (isBetween(usuConvInt, 34, 99)) {
						codCliente = Integer.parseInt(usr);
						result = "]" + Integer.toString(codCliente, BASE).toUpperCase();
					} else {
						result = "ZZZZZZZZ";
					}
					break;
				}
			}
		} else {
			result = "ZZZZZZZZ";
		}
		return result;
	}
   
	/**
	 * Checa si un valor entero proporcionado se encuentra dentro de un rango definido.
	 *
	 * @param valor (int) con el valor a verificar entre el rango inicial y el rango final definido.
	 * @param rangoIni (int) con el valor inicial del rango.
	 * @param rangoFin (int) con el valor final del rango.
	 * @return true, si el valor se encuentra dentro del rango proporcionado.
	 */
	public static boolean isBetween(final int valor, final int rangoIni, final int rangoFin) {
		return rangoIni <= valor && valor <= rangoFin;
	}

}