/**
 * Isban Mexico
 *   Clase: ConsultaNominaProgramadaBO.java
 *   Descripcion: Implementacion de ConsultaNominaProgramadaBO, para el control de servicios
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 */
package mx.altec.enlace.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConsultaPaginadaBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.ConsultaNominaProgramadaDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase ConsultaNominaProgramadaBO
 * @author Arturo
 * @version 1.0
 * 
 */
public class ConsultaNominaProgramadaBO {

	/**
	 * Metodo para consultar todas las operaciones de nomina
	 * @param contrato : numero de contrato
	 * @return : mapa de datos
	 */
	public Map<String, Object>  consultarNominaProgramada(String contrato)  {
		ConsultaNominaProgramadaDAO consultaDAO = new ConsultaNominaProgramadaDAO();
		Map<String, Object>  respuesta;
		try {
			respuesta = consultaDAO.consultarNominaProgramada(contrato);
		} catch (SQLException e) {
			//Se manda lista null si hay error
			EIGlobal.mensajePorTrace(">>> Error al consultar nominda programada ::: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			respuesta = new HashMap<String, Object>();
			respuesta.put("resultadoNomina", "ERROR");
		}
		return respuesta;
	}
	
	/**
	 * Metodo para consultar la nomina programada por paginacion
	 * @param contrato : contrato
	 * @param regInicio : registro de inicio
	 * @param regFin : registro fin
	 * @return ConsultaPaginadaBean
	 * @throws BusinessException : exception
	 */
	public ConsultaPaginadaBean  consultarNominaProgramadaPag(String contrato, int regInicio, int regFin) throws BusinessException  {
		ConsultaNominaProgramadaDAO consultaDAO = new ConsultaNominaProgramadaDAO();
		ConsultaPaginadaBean  respuesta;
		try {
			EIGlobal.mensajePorTrace( "***ConsultaNominaProgramadaBO.class::consultarNominaProgramadaPag()&", EIGlobal.NivelLog.INFO);
			respuesta = consultaDAO.consultarNominaProgramadaPag(contrato, regInicio, regFin);
		} catch (SQLException e) {
			throw new BusinessException(e);
		}
		return respuesta;
	}
	
	

	
	/**
	 * Metodo para bitacorizar cuando se realiza una consulta
	 * @param req : HttpServletRequest
	 * @param session : BaseResource
	 * @param sess : HttpSession
	 * @param totalImporte : total del importe
	 * @param archivo : archivo generado
	 */
	public void realizarBitacora(HttpServletRequest req, BaseResource session, 
			HttpSession sess ,double totalImporte, String archivo ) {
		try{
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EC_OPER_CONSULTA_OPER_PROG);
			bt.setContrato(session.getContractNumber());
			bt.setNombreArchivo(archivo);
			bt.setImporte((double) totalImporte);
			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(NumberFormatException e){}
		catch(SQLException e){
			EIGlobal.mensajePorTrace("ConsultaNominaProgramada - realizarBitacora()  Excepcion SQL "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("ConsultaNominaProgramada - realizarBitacora()  Excepcion  "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	}
	

	
}