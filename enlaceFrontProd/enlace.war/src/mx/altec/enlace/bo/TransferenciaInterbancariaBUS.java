package mx.altec.enlace.bo;
import java.util.HashMap;

import mx.altec.enlace.beans.CepBean;
import mx.altec.enlace.beans.TranInterbancaria;
import mx.altec.enlace.dao.TransferenciaInterbancariaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.TranInterConstantes;
/**
 *
 * @author Z708764
 *
 */
public class TransferenciaInterbancariaBUS {

    /**
     * INSTANCE instancia
     */
	private static TransferenciaInterbancariaBUS INSTANCE = null;
    /**
     * createInstance crea la instancia
     */
    private synchronized static void createInstance() {
        if(INSTANCE == null) {
            INSTANCE = new TransferenciaInterbancariaBUS();
        }
    }
    /**
     * Metodo que realiza el getInstance
     * @return instancia
     */
    public static TransferenciaInterbancariaBUS getInstance() {
    	if(INSTANCE == null) createInstance(); {
    		return INSTANCE;
    	}
    }
    /**
     * Metodo que registra la transferencia
     * @param trama trama
     * @param resultado resultado
     * @return boolean
     */
    public boolean registraTransferencia(String trama, String resultado){
    	try{
    		TransferenciaInterbancariaDAO transferenciaInterbancariaDAO= TransferenciaInterbancariaDAO.getInstance();
    		TranInterbancaria tranInterbancaria=new TranInterbancaria();
    		String datos[]=trama.split("\\|");
    		EIGlobal.mensajePorTrace("trama-->"+trama,EIGlobal.NivelLog.ERROR);
    		for (int i=0;i<datos.length;i++) {
    			EIGlobal.mensajePorTrace("datos["+i+"]-->"+datos[i],EIGlobal.NivelLog.ERROR);
    		}
    		tranInterbancaria.setUsuario(datos[1]);
    		tranInterbancaria.setContrato(datos[3]);
    		tranInterbancaria.setCuentaCargo(datos[6]);
    		tranInterbancaria.setCuentaAbono(datos[9]);
    		tranInterbancaria.setBeneficiario(datos[11]);
    		tranInterbancaria.setImporte(datos[13]);
    		tranInterbancaria.setReferenciaInterb(datos[17]);
    		tranInterbancaria.setFolioMancomunado(datos[18]);
    		tranInterbancaria.setFormaLiquidacion(datos[19]);
//    		if (datos[20].equals("USD")) {
//    			tranInterbancaria.setReferencia(resultado.substring(79, 86).toString().trim());
//    			tranInterbancaria.setCuentaCLABE(datos[9]);
//				
//    		}else {
    		tranInterbancaria.setReferencia(resultado.substring(8,resultado.indexOf('|')));
        	tranInterbancaria.setCuentaCLABE(resultado.substring(resultado.indexOf('|')+1, resultado.length()));
//    		}    		
    		tranInterbancaria.setEstatus("LI");
    		tranInterbancaria.setTitular(datos[8]);
    		String[] datosDet=descomponeDatos(datos[16]);
    		tranInterbancaria.setConceptoPago(datosDet[0]);
    		tranInterbancaria.setRfc(datosDet[1]);
    		tranInterbancaria.setIva(datosDet[2]);
    		tranInterbancaria.setBanco(datos[10]);
    		tranInterbancaria.setPlaza(datos[14]);
    		tranInterbancaria.setSucursal(datos[12]);
    		if (datosDet[3] != null) {
    			tranInterbancaria.setTipoDivisa(datosDet[3]); /*SPID Vector 2016*/
    		} else {
    			tranInterbancaria.setTipoDivisa(datos[20]);
    		}
    		
    		return transferenciaInterbancariaDAO.registraTransferenciaInterbancaria(tranInterbancaria);
    	}catch(Exception e) {
    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
    	}
    	return false;
    }
    
    /**
     * Metodo que realiza la consulta de transferencias
     * @param contrato contrato
     * @param fechaInicio fecha inicio
     * @param fechaFin fecha fin
     * @param tipoMoneda tipo de Divisa para la consulta
     * @return lista de beans
     */
    public  HashMap<Integer,TranInterbancaria> consultaTransferencias(String contrato, String fechaInicio, String fechaFin, String tipoMoneda){
    	TransferenciaInterbancariaDAO transferenciaInterbancariaDAO= TransferenciaInterbancariaDAO.getInstance();
    	return transferenciaInterbancariaDAO.consultaTransferenciaInterbancaria(contrato, fechaInicio, fechaFin, tipoMoneda);
    }
    /**
     * Metodo que realiza la consulta de la clave del banco
     * @param bean bean que contiene los parametros
     * @return bean que guarda los parametros
     */
    public CepBean consultaClaveBanco(CepBean bean){
    	TransferenciaInterbancariaDAO transferenciaInterbancariaDAO= new TransferenciaInterbancariaDAO();
    	bean.setBanco(transferenciaInterbancariaDAO.consultaClaveBanco(bean.getNomBanco()));
    	bean.setBancoSantander(transferenciaInterbancariaDAO.consultaClaveBanco(TranInterConstantes.CVEBANCO));
    	return bean;
    }
    /**
     * Metodo que obtiene concepto, rfc, iva
     * @param varConceptoRfcIva variable a separar
     * @return arreglo de los datos
     */
    public String[] descomponeDatos(String varConceptoRfcIva) {
    	EIGlobal.mensajePorTrace("varConceptoRfcIva-->"+varConceptoRfcIva,EIGlobal.NivelLog.DEBUG);
    	String []valor=new String[4];
   		String rfc="";
   		String iva="";
   		String concepto="";
   		
   		try {
   			if(varConceptoRfcIva.length()>40) {
   				concepto = varConceptoRfcIva.substring(0,40);
   				varConceptoRfcIva=varConceptoRfcIva.substring(40);
    			int rfcIn  = varConceptoRfcIva.indexOf("RFC ");
        		if(rfcIn >= 0){
        			rfcIn=varConceptoRfcIva.indexOf("RFC ") + 4;
        			varConceptoRfcIva = varConceptoRfcIva.substring(rfcIn);
        			int posTermRfc=varConceptoRfcIva.indexOf(' ') + 1;
        			if(posTermRfc==0) {
        				posTermRfc=varConceptoRfcIva.length();
        			}
        			rfc=varConceptoRfcIva.substring(0,posTermRfc);
    				varConceptoRfcIva=varConceptoRfcIva.substring(posTermRfc);
        		}
        		int ivaIn  = varConceptoRfcIva.lastIndexOf("IVA ");
        		if(ivaIn >= 0 && varConceptoRfcIva.contains("@")) {
        			ivaIn=varConceptoRfcIva.indexOf("IVA ") + 4;
        			iva = varConceptoRfcIva.substring(ivaIn, varConceptoRfcIva.indexOf("@")).trim();        			
    			 }else if(ivaIn >= 0){    				 
    				 ivaIn=varConceptoRfcIva.indexOf("IVA ") + 4;
    				 EIGlobal.mensajePorTrace("OBTINE VALOR DEL IVA --->"+varConceptoRfcIva.substring(ivaIn, varConceptoRfcIva.length()).trim()+"<--",EIGlobal.NivelLog.DEBUG);
    				 iva = varConceptoRfcIva.substring(ivaIn, varConceptoRfcIva.length()).trim(); 
    			 }
			} else {
				concepto=varConceptoRfcIva;
			}
	   		EIGlobal.mensajePorTrace("concepto-->"+concepto,EIGlobal.NivelLog.DEBUG);
	   		EIGlobal.mensajePorTrace("rfc-->"+rfc,EIGlobal.NivelLog.DEBUG);
	   		EIGlobal.mensajePorTrace("iva-->"+iva,EIGlobal.NivelLog.DEBUG);
   		}catch(Exception e) {
   			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
   		}

   		valor[0]=concepto;
   		valor[1]=rfc;
   		valor[2]=iva;
   		if (varConceptoRfcIva.contains("@MXP") || varConceptoRfcIva.contains("@MXN") || varConceptoRfcIva.contains("@USD")) {
			valor[3]=varConceptoRfcIva.substring(varConceptoRfcIva.indexOf('@')+1, varConceptoRfcIva.indexOf("@")+4);
   		}  		
   		return valor;
    }    
}