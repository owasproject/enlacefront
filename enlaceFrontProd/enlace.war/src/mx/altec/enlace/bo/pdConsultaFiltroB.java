/*
 * pdConsultaFiltroB.java
 *
 * Created on 26 de marzo de 2002, 10:30 AM
 */

package mx.altec.enlace.bo;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Rafael Martinez
 * @version 1.1  deprecated Beneficiarios por aBeneficiarios
 */
public class pdConsultaFiltroB extends Object implements java.io.Serializable {

    private static final String PROP_AL_CUENTAS = "aCuentas";
    private static final String PROP_FECHA_INI ="FechaIni";
    private static final String PROP_FECHA_FIN ="FechaFin";
    private static final String PROP_IMPORTE ="Importe";
    private static final String PROP_NO_PAGO_INI ="NoPagoIni";
    private static final String PROP_NO_PAGO_FIN ="NoPagoFin";
    private static final String PROP_A_BENEFICIARIOS ="aBeneficiarios";

    private static final String PROP_REGISTRADOS ="Registrados";
    private static final String PROP_PENDIENTES ="Pendientes";
    private static final String PROP_CANCELADOS ="Cancelados";
    private static final String PROP_LIQUIDADOS ="Liquidados";
    private static final String PROP_VENCIDOS ="Vencidos";
    private static final String PROP_MODIFICADOS ="Modificados";

    private java.util.ArrayList aCuentas;
    private String FechaIni = "";
    private String FechaFin = "";
    private String Importe = "";
    private String NoPagoIni ="";
    private String NoPagoFin = "";
    private java.util.ArrayList aBeneficiarios;

    private boolean Registrados=true;
    private boolean Pendientes=true;
    private boolean Cancelados=true;
    private boolean Liquidados=true;
    private boolean Vencidos=true;
    private boolean Modificados=true;



    /** Creates new pdConsultaFiltroB */
    public pdConsultaFiltroB() {
        aBeneficiarios = new java.util.ArrayList();
        aCuentas = new java.util.ArrayList();
    }
/*GET---------------------------*/

    public java.util.ArrayList getACuentas(){
        return aCuentas;
    }

    public String getFechaIni(){
        return FechaIni;
    }

    public String getFechaFin(){
        return FechaFin;
    }

    public String getImporte(){
        return Importe;
    }

    public String getNoPagoIni(){
        return NoPagoIni;
    }

    public String getNoPagoFin(){
        return NoPagoFin;
    }


    public boolean getRegistrados(){
        return Registrados;
    }

    public boolean getPendientes(){
        return Pendientes;
    }

    public boolean getCancelados(){
        return Cancelados;
    }

    public boolean getLiquidados(){
        return Liquidados;
    }

    public boolean getVencidos(){
        return Vencidos;
    }

    public boolean getModificados(){
        return Modificados;
    }

    public java.util.ArrayList getABeneficiarios(){
        return aBeneficiarios;
    }
/*SET---------------------------*/
    public void setABeneficiarios(java.util.ArrayList value){
        aBeneficiarios = value;
    }

    public void setACuentas(java.util.ArrayList value){
        aCuentas = value;
    }

    public void setFechaIni(String Fecha){
        FechaIni = Fecha;

    }

    public void setFechaFin(String Fecha){
        FechaFin = Fecha;
    }

    public void setImporte(String Imp){
        Importe = Imp;
    }

    public void setNoPagoIni(String No){
        NoPagoIni = No;
    }

    public void setNoPagoFin(String No){
        NoPagoFin = No;
    }

    public void setVencidos(boolean b){
        Vencidos = b;
    }

    public void setModificados(boolean b){
        Modificados = b;
    }
/*-----*/

}
