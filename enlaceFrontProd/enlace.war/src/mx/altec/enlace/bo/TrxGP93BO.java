package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.Vector;

import mx.altec.enlace.beans.AdmTrxGP93;
import mx.altec.enlace.dao.TrxGP93DAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase Bussines para manejar el procedimiento para la transaccion GP93
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 22, 2011
 */
public class TrxGP93BO {
	
	/**
	 * Objeto contenedor de tipo AdmTrxGP93
	 */
	private AdmTrxGP93 bean;

	/**
	 * Obtener el valor de bean.
     *
     * @return bean valor asignado.
	 */
	public AdmTrxGP93 getBean() {
		return bean;
	}
	
	/**
	 * Metodo para obtener todo el catalogo de divisas
	 * 
	 * @return catAdmTrxGP93	Vector contenedor de objetos de tipo TrxGP93VO
	 */
	public Vector obtenCatalogoDivisas(){
		EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoDivisas:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxGP93DAO dao = new TrxGP93DAO();
	    MQQueueSession mqsess = null;
	    Vector catAdmTrxGP93 = new Vector();
	    String llavePag = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	do{
	    		if(bean!=null)
	    			llavePag = bean.getLlavePaginacion();
	    		
		    	bean = dao.consultaTrxGP93(rellenar("D",1,' ','D'), 
		    							   rellenar("",3,' ','D'),
		    							   rellenar(llavePag,3,' ','D'));
		    	EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoDivisas:: Codigo operacion>>" 
		    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
		    			    	   	
	    		for(int i=0; i<bean.getCatGP93Bean().size(); i++){
	    			try{	    				
	    				catAdmTrxGP93.add((TrxGP93VO)bean.getCatGP93Bean().get(i));
	    			}catch(Exception e){}    			    			
	    		}
		    	
	    	}while(bean.getMasPaginacion());	    		
	    	    	
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenCatalogoDivisas...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoDivisas:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoDivisas:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoDivisas:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return catAdmTrxGP93;
	}
	
	/**
	 * Metodo para obtener todo el catalogo de paises
	 * 
	 * @return catAdmTrxGP93	Vector contenedor de objetos de tipo TrxGP93VO
	 */
	public Vector obtenCatalogoPaises(){
		EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoPaises:: Entrando....", EIGlobal.NivelLog.DEBUG);		

		TrxGP93DAO dao = new TrxGP93DAO();
	    MQQueueSession mqsess = null;
	    Vector catAdmTrxGP93 = new Vector();
	    String llavePag = "";
	    try{
	    	mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, 
	    									NP_JNDI_QUEUE_RESPONSE, 
	    									NP_JNDI_QUEUE_REQUEST);
	    	dao.setMqsess(mqsess);
	    	do{
	    		if(bean!=null)
	    			llavePag = bean.getLlavePaginacion();
	    		
		    	bean = dao.consultaTrxGP93(rellenar("P",1,' ','D'), 
		    							   rellenar("",3,' ','D'),
		    							   rellenar(llavePag,3,' ','D'));
		    	EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoPaises:: Codigo operacion>>" 
		    			+ bean.getCodigoOperacion(), EIGlobal.NivelLog.DEBUG);
		    			    	   	
	    		for(int i=0; i<bean.getCatGP93Bean().size(); i++){
	    			try{	    				
	    				if(((TrxGP93VO)bean.getCatGP93Bean().get(i)).getOVarCod().length()>4)
	    					catAdmTrxGP93.add((TrxGP93VO)bean.getCatGP93Bean().get(i));
	    			}catch(Exception e){}    			    			
	    		}
		    	
	    	}while(bean.getMasPaginacion());	    		
	    	    	
	    }catch (Exception e){
	    	StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Error al ejecutar metodo obtenCatalogoPaises...", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoPaises:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.DEBUG);
	    }finally{
	    	try{
	    		mqsess.close();
	    	}catch(Exception e1) {
		    	StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoPaises:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.DEBUG);
	    	}
	    }
		EIGlobal.mensajePorTrace("TrxGP93BO::obtenCatalogoPaises:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return catAdmTrxGP93;
	}

}
