//11/04/02
//Esta clase nos permite Establecer adecuadamente los valores de Cuenta y Descripci�n.
////////////////////Modificaci�n para integraci�n {BASE CERO (Mto Estructura)}///////////////////////////////////////
package mx.altec.enlace.bo;
public class BC_Cuenta
	{
	/////////Campos del formulario /////////////////////////////////////////////
	private String numCta;
	private String descripcion;

	//////// Constructores de la Clase /////////////////////////////////////////
	public BC_Cuenta(String numCta)
		{
		this.numCta = numCta;
		this.descripcion = "";
		}

	public BC_Cuenta(String numCta, String descripcion)
		{
		this.numCta = numCta;
		this.descripcion = descripcion;
		}

	//////// Regresa la descripci�n del objeto actual //////////////////////////////
	public String getDescripcion()
		{return descripcion;}

	//////////Modifica para la descripci�n del objeto actual////////////////////////

	public void setDescripcion(String descripcion)
		{
		if(descripcion == null) descripcion = "";
		this.descripcion = descripcion;
		}

	//////////Regresa el n�mero de cuenta ////////////////////////
	public String getNumCta()
		{return numCta;}

	////////// Modifica el n�mero de cuenta del objeto actual////////////////////////
	protected void setNumCta(String numCta)
		{if(numCta == null) numCta = "" ;
		this.numCta = numCta;}

	//////////////  regresa una trama de la cuenta actual ///////////////////////////
	public String trama()
		{
		return "@" + numCta + "|" + (descripcion.equals("")?" ":descripcion);
		}

	// --- m�todo equals para indicar si se trata de la misma cuenta
	public boolean equals(Object x)
		{
		if(!(x instanceof BC_Cuenta)) return false;
		return ((BC_Cuenta)x).getNumCta().equals(getNumCta());
		}

	// --- ESTATICO: crea una cuenta a partir de una trama -----------------------------
	static public BC_Cuenta creaCuenta(String trama)
		{
		String numCuenta;
		String descripcion;
		int a, b;

		a = trama.indexOf("@");
		b = trama.indexOf("|");
		if(a!=0 || b < 1) return null;
		numCuenta = trama.substring(1,b);
		descripcion = trama.substring(b+1);
		numCuenta = numCuenta.trim();
		if(numCuenta.equals("")) return null;
		return new BC_Cuenta(numCuenta,descripcion); //Regresa la cuenta y la descripcion adecuada
		}

	}
