package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.dao.ProveedorConf;

public class ArchivoConf implements java.io.Serializable {
	// --
	public static final int SIN_ENVIAR = 1;
	public static final int ENVIADO = 2;
	public static final int RECUPERADO = 3;
	public static final int IMPORTADO = 4;

	// --
	private Vector provs = null;
	private String nombre = null;
	private int estado = 1;
	private String fecha = "";
	private String secuencia = "";

	// --
	public ArchivoConf(String nombre)
		{
		this.nombre=nombre;
		provs = new Vector();
		estado = SIN_ENVIAR;
		}

	// --
	public void agregaProv(ProveedorConf prov)
		{provs.add(prov);}

	// --
	public ProveedorConf obtenProv(int indice)
		{return (ProveedorConf)provs.get(indice);}

	// --
	public ProveedorConf borraProv(int indice)
		{return (ProveedorConf)provs.remove(indice);}

	// --
	public void cambiaProv(int indice, ProveedorConf prov)
		{provs.set(indice, prov);}

	// --
	public int getNumProveedores()
		{return provs.size();}

	// --
	public String getNombre()
		{return nombre;}

	// --
	public boolean setNombre(String strNombre)
		{
		if(estado == ENVIADO || estado == RECUPERADO) return false;
		nombre = strNombre;
		return true;
		}

	// --
	public int getEstado()
		{return estado;}

	// --
	public void setEstado(int estado)
		{this.estado = estado;}

	// --
	public String getFecha()
		{return fecha;}

	// --
	public String getSecuencia()
		{return secuencia;}

	// --
	public boolean escribeEnvio(String strArchivo)
		{
		boolean todoOK = true;
		try
			{
			FileWriter archivo = new FileWriter(strArchivo);
			for(int a=0;a<provs.size();a++) archivo.write(((ProveedorConf)provs.get(a)).tramaEnvio() + "\r\n");
			archivo.close();
			}
		catch(Exception e)
			{todoOK = false;}
		return todoOK;
		}

	// --
	public boolean escribeExportacion(String strArchivo)
		{
		boolean todoOK = true;
		try
			{
			FileWriter archivo = new FileWriter(strArchivo);
			for(int a=0;a<provs.size();a++) archivo.write(((ProveedorConf)provs.get(a)).tramaExportacion() + "\r\n");
			archivo.close();
			}
		catch(Exception e)
			{todoOK = false;}
		return todoOK;
		}

	// --
	public boolean leeRecuperacion(String strArchivo)
		{
		boolean todoOK = true;
		try
			{
			String linea;
			BufferedReader entrada = new BufferedReader(new FileReader(strArchivo));
			ProveedorConf prov = null;

			linea = entrada.readLine();
			fecha = linea.substring(linea.indexOf(";")+1);
			while((linea = entrada.readLine()) != null)
				{
				prov = new ProveedorConf();
				prov.leeLineaRecuperacion(linea);
				provs.add(prov);
				}
			entrada.close();
			if(prov != null) secuencia = prov.noTransmision;
			}
		catch(Exception e)
			{todoOK = false;}
		return todoOK;
		}

	public boolean leeRecuperacion2(String strArchivo)
		{
		boolean todoOK = true;
		try
			{
			String linea;
			BufferedReader entrada = new BufferedReader(new FileReader(strArchivo));
			ProveedorConf prov = null;

			linea = entrada.readLine();
			fecha = linea.substring(linea.indexOf(";")+1);
			linea = entrada.readLine();
			while((linea = entrada.readLine()) != null)
				{
				prov = new ProveedorConf();
				prov.leeLineaRecuperacion(linea);
				provs.add(prov);
				}
			entrada.close();
			if(prov != null) secuencia = prov.noTransmision;
			}
		catch(Exception e)
			{todoOK = false;}
		return todoOK;
		}


	// --
	public boolean leeRegreso(String strArchivo)
		{
		boolean todoOK = true;
		try
			{
			String linea, aux;
			BufferedReader entrada = new BufferedReader(new FileReader(strArchivo));
			ProveedorConf prov = null;
			int a=-1;

			linea = entrada.readLine();
			secuencia = linea.substring(linea.indexOf(";")+1);
			while((linea = entrada.readLine()) != null)
				{
				a++;
				aux = linea.substring(0,linea.indexOf(";"));
				linea = linea.substring(linea.indexOf(";")+1);
				/*
				for(a=0;a<provs.size();a++)
					{prov = (ProveedorConf)provs.get(a); if(prov.rfc.equals(aux)) break;}
				*/
				prov = (ProveedorConf)provs.get(a);
				prov.claveStatus = linea.substring(0,linea.indexOf(";"));
				prov.status = linea.substring(linea.indexOf(";")+1,linea.length()-1);
				}
			entrada.close();
			}
		catch(Exception e)
			{todoOK = false;}
		return todoOK;
		}

	// --
	public Vector leeImportacion(String contArchivo, String vTProv)
		{
		return leeImportacion(contArchivo, null, vTProv);
		}

	// --
	public Vector leeImportacion(String contArchivo, Hashtable clavesBanco, String vTProv)
		{
		Vector errores = new Vector();
		Vector erroresLinea;

		try
			{
			String linea;
			BufferedReader entrada = new BufferedReader(new FileReader(contArchivo));
			ProveedorConf prov;
			int i = 0;

			while((linea = entrada.readLine()) != null)
				{
				prov = new ProveedorConf();
				erroresLinea = prov.leeLineaImportacion(linea, clavesBanco, vTProv);
				errores.add(erroresLinea);
                    provs.add(prov);
                    i++;
                }
			if(i==0){
				erroresLinea = new Vector();
				erroresLinea.add("El formato de la l&iacute;nea no es el correcto");
				errores.add(erroresLinea);
			}
			entrada.close();
		}
		catch(Exception e)
			{;}
		return errores;
	}

	public static void main(String args[])
		{
/*
		x.leeRecuperacion("recuperado.txt");
		x.leeRegreso("regreso.txt");
		int total = x.getNumProveedores(), a;
		for(a=0;a<total;a++)
			{
			//System.out.println(((ProveedorConf)x.obtenProv(a)).tramaEnvio());
			System.out.println(((ProveedorConf)x.obtenProv(a)).status);
			}
		x.escribeEnvio("paraEnviar.txt");
*/
    }

}