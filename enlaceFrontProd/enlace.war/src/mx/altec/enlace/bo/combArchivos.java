package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

public class combArchivos {

	public combArchivos () {
	}

//	public void mLee ( String tipo, String persona) throws IOException {
	public Vector pLee ( String Archivo, String tipo) throws IOException {
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		String campo1 = "";
		Vector clave = new Vector();
		Vector lista = new Vector();
		RandomAccessFile fileAmbiente=null;

		try {
			File arch = new File(Archivo);
			System.out.println(arch.getPath());
			fileAmbiente= new RandomAccessFile(arch,"r");
		} catch ( IOException e ) {
			System.out.println( "Error al leer el archivo de ambiente, e1 " + e);
		}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");

		try {
			do {
				if ( registroLeido.startsWith( tipo ) ) {
					if (tipo != "8") {
						campo =	registroLeido.substring(
							registroLeido.indexOf( ';' , 2 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) );
						lista.add(campo);
					} else {
						campo = registroLeido.substring(
                                registroLeido.indexOf( ';' , 5 ) + 1 ,
                                registroLeido.lastIndexOf( ';' ) );
						campo1 = registroLeido.substring(
						         registroLeido.indexOf(';') ,
							     registroLeido.indexOf(';') + 5);
					    campo1 = campo1.replace(';', ' ');
                        campo = campo.replace(';',' ') ;
						lista.add(campo);
						clave.add(campo1);
					}
				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);

	}
}