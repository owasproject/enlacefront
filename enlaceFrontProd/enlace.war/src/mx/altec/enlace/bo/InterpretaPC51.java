// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

/**<code><B><I>InterpretaPC51</I></B></code> interpreta la trama de respuesta de consulta de tasas<BR>
 * lee el archivo y crea los renglones de HTML para desplegarlo.
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.2.0
 * <P>Modificaciones:<BR>
 * 17/10/2002 Se inici&oacute; la modifiaci&oacute;n. de <B>interpretaTrama</B>.<BR>
 * 18/10/2002 Se termin&oacute; la modificaci&oacute;n de <B>interpretaTrama</B>.<BR>
 * 03/12/2002 Se asegur&oacute; el cierre del archivo en el m&eacute;todo <B><I>leeArchivo</I></B>.<BR>
 * 03/12/2002 Se presenta s&oacute;lo el plazo y la tasa; se omite la referencia y los puntos.<BR>
 * </P>
 */
public class InterpretaPC51 extends Object implements Serializable
{

/**Versi&oacute;n del <I>Bean</I>.
*/
public static final String version="1.2.0";

/**Dos primeros caracteres de respuesta de una trama correcta.
*/
public static final String OK="OK";

/**C&oacute;digo de error de una trama correcta.
*/
public static final String BIEN="0000";

/**Caracter "pipe".
*/
public static final String pipe="|";

/**Etiqueta de cierre de tabla.
*/
public static final String CTR="</TR>";

/**Etiqueta de cierre de columna.
*/
public static final String CTD="</TD>";

/**Arrenglo de enteros con las posiciones de cada parte de la trama.
*/
public static int[] posiciones={3, 10, 15, 17, 19};
//private static String renglonClaro="<TR  bgcolor=\"#CCCCCC\">";
//private static String renglonObscuro="";
//private static String columnaClara="";
//private static String columnaObscura="";

private static final int[] posicionesCodigoError={4, 8};

/**Contiene la trama de respuesta de consulta de tasas (PC51).
*/
private String tramaRespuesta="";

/**Separador de campos
*/
private String separador;

/**Guarda la ruta y el nombre del archivo de respuesta del servicio de tuxedo.
*/
private String nombreArchivo="";

/**Contiene los renglones del archivo de respuesta de tasas.
*/
private Vector lineas;

/**Contiene todos los datos de las tasas como renglones de tabla HTML
*/
private String cadenaDatos="";

/**Folio de la trama de respuesta.
*/
private String folio="";

/**Este atributo permite saber si el archivo est&acute; vac&iacute;o o s&iacute; conten&iacute;a datos.
*/
private int renglonesLeidos=0;

  /**
   * Constructor.
   * <P>Asigna por defecto el separador de campos al "pipe" e inicializa el vector <I>l&iacute;neas</I>.
   */
  public InterpretaPC51()
  {
    separador=pipe;
    lineas=new Vector();
  } // Fin constructor


  public String getTramaRespuesta()
  {
    return tramaRespuesta;
  }


  public void setTramaRespuesta(String tramaRespuesta)
  {
    if(tramaRespuesta!=null && tramaRespuesta.length()>0)
    {
      this.tramaRespuesta=tramaRespuesta;
    }
  } // Fin setTramaRespuesta


   /**<code><B><I>interpretaTrama</I></B><code> revisa la cadena contenida en <I>trama</I> obtenida<BR>
   * como la respuesta de tuxedo del servicio de tasas <B>PC51</B>, de donde obtiene el nombre del archivo de respuesta de tasas.
   * <P>Regresa verdadero si la trama de respuesta es correcta; falso si es de error, tiene c&oacute;digo de<BR>
   * error distinto de cero o no contiene nombre de archivo.  Si la trama es correcta asigna a<BR>
   * <I>nombreArchivo</I>.
   * </P>
   */
  public boolean interpretaTrama()
  {
    String aux1;
    StringTokenizer st;
    String terceraParte;

    if(tramaRespuesta.length()<posiciones[4])
      return false;

      st=new StringTokenizer(tramaRespuesta, " ");

      if(st.countTokens()<3)
         return false;

    try
    {
      //aux1=tramaRespuesta.substring(0, posiciones[0]).trim();
      aux1=st.nextToken();

      if(!aux1.equalsIgnoreCase(OK))
        return false;

      aux1=st.nextToken();

      if(aux1==null  || aux1.length()<1)
      {
         return false;
      }

      folio=aux1;

      //aux1=tramaRespuesta.substring(posiciones[2]-1, posiciones[2]+3).trim();
      terceraParte=(st.nextToken());

      aux1=terceraParte.substring(posicionesCodigoError[0], posicionesCodigoError[1]);

      if(!aux1.equalsIgnoreCase(BIEN))
        return false;

      //aux1=tramaRespuesta.substring(posiciones[3]+1, posiciones[3]+2);
      aux1=terceraParte.substring(posicionesCodigoError[1], posicionesCodigoError[1]+1);

      if(aux1.equalsIgnoreCase(separador))
        return false;

      //aux1=tramaRespuesta.substring(posiciones[3]+1);
      aux1=terceraParte.substring(posicionesCodigoError[1], terceraParte.length());

      if(aux1!=null && aux1.length()>1)
      {
        setNombreArchivo(aux1);
      }
      else
      {
        return false;
      } // Fin if-else
    }
    catch(ArrayIndexOutOfBoundsException iaoobe)
    {
      return false;
    } //Fin try-catch

    return true;
  } // Fin interpretaTrama


  public String getSeparador()
  {
    return separador;
  }

  public void setSeparador(String separador)
  {
    if(separador!=null && separador.length()>0)
      this.separador=separador;
  }


  public String getNombreArchivo()
  {
    return nombreArchivo;
  }

  public void setNombreArchivo(String nombreArchivo)
  {
    if(nombreArchivo!=null && nombreArchivo.length()>0)
    {
      this.nombreArchivo=nombreArchivo;
    }
  }

   /**<code><B><I>leeArchivo</I></B></code> se encarga de la lectura del archivo cuyo nombre est&aacute;<BR>
   * contenido en <I>nombreArchivo</I>, leyendo rengl&oacute;n por rengl&oacute;n.
   */
  public boolean leeArchivo()
  {
    File archivo;
    BufferedReader bf=null;
    String linea;
    renglonesLeidos=0;
    boolean resultadoArchivo=false;

    archivo=new File(getNombreArchivo());
    lineas=new Vector();
    cadenaDatos=new String();

    if(!archivo.isFile())
      return false;

    try
    {
      bf=new BufferedReader(new FileReader(archivo));

      while(bf.ready())
      {
        linea=bf.readLine();


        if(linea!=null && linea.length()>0)
        {
          renglonesLeidos++;
          lineas.addElement(linea);
          cadenaDatos+=construyeRenglon(linea);
        } // Fin if
      } // Fin while

      resultadoArchivo=true;
    }
    catch(IOException iox)
    {
      //return false;
      resultadoArchivo=false;
    }
    finally
    {
      if(bf!=null)
      {
         try
         {
            bf.close();
         }
         catch(IOException iox02)
         {
         } // Fin try-catch
      } // Fin if bf
    } // Fin try-catch-finally



    //return true;
    return resultadoArchivo;
  } // Fin leeArchivo

  /*
  public Object[] getLineas()
  {
    return lineas.toArray();
  }
  */

  public Vector getLineas()
  {
    return lineas;
  }

   /**<code><B><I>construyeRenglon</I></B></code> recibe un rengl&oacute;n del archivo generado por la trama
   * <B>PC51</B> con los datos de las tasas, los separa y construye un rengl&oacute;n de HTML con sus<BR>
   * respectivas celdas.
   * <P>Regresa una cadena vac&iacute;a si no hubo datos; con longitud mayor que cero si hubo datos de tasas.
   * @param datos Es una l&iacute;nea del archivo de tasas.
   */
  public String construyeRenglon(String datos)
  {
    String renglon=new String();
    int numeroDatos=0;


    if(datos!=null && datos.length()>0)
    {
      StringTokenizer st=new StringTokenizer(datos, separador);
      renglon+=InterpretaPC46.renglonClaro;
      numeroDatos=st.countTokens();

      for(int i=0;i<numeroDatos;i++)
      {
         if(i==0 || i==3)
         {
            renglon+=InterpretaPC46.fondoClaro;
            renglon+=st.nextToken().trim();
            renglon+=CTD;
         }
         else
         {
            st.nextToken();
         } // Fin if plazo y tasa
      } // Fin for

      renglon+=CTR;

    } // Fin if

    return renglon;
  } // Fin construyeRenglon


  public String getCadenaDatos()
  {
    return cadenaDatos;
  }


   public String getFolio()
   {
      return folio;
   }

   public int getRenglonesLeidos()
   {
      return renglonesLeidos;
   }

} // Fin clase InterpretaPC51

