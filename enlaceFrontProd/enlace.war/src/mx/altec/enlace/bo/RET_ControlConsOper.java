package mx.altec.enlace.bo;

import java.util.Vector;

/**
 * Clase encargada de administrar el procesamiento de una consulta
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 09, 2012
 */
public class RET_ControlConsOper {

	/**
	 * Constante para limitar los registros de una consulta
	 */
	public static final int MAX_REGISTROS_PAG = 50;

	/**
	 * Identificador de la(s) pagina(s) de consulta
	 */
	private int idPage = 0;

	/**
	 * Numero de paginas de consulta
	 */
	private int numPages = 0;

	/**
	 * Numero total de los registros
	 */
	private int numRecords = 0;

	/**
	 * Numero total de los registros
	 */
	private int maxNumRecordsPage = 0;

	/**
	 * Numero del registro inicial de la pagina de consulta
	 */
	private int numRecordPageDesde = 0;

	/**
	 * Numero del registro final de la pagina de consulta
	 */
	private int numRecordPageHasta = 0;

	/**
	 * Condicion del contexto de la consulta
	 */
	private String whereCondicion = null;

	/**
	 * Vector con los registros de la consulta
	 */
	private Vector lista = null;

	/**
	 * Obtener el valor de idPage.
     *
     * @return idPage valor asignado.
	 */
	public int getIdPage() {
		return idPage;
	}

	/**
     * Asignar un valor a idPage.
     *
     * @param idPage Valor a asignar.
     */
	public void setIdPage(int idPage) {
		this.idPage = idPage;
	}

	/**
	 * Obtener el valor de numPages.
     *
     * @return numPages valor asignado.
	 */
	public int getNumPages() {
		return numPages;
	}

	/**
     * Asignar un valor a numPages.
     *
     * @param numPages Valor a asignar.
     */
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	/**
	 * Obtener el valor de numRecords.
     *
     * @return numRecords valor asignado.
	 */
	public int getNumRecords() {
		return numRecords;
	}

	/**
     * Asignar un valor a numRecords.
     *
     * @param numRecords Valor a asignar.
     */
	public void setNumRecords(int numRecords) {
		this.numRecords = numRecords;
	}

	/**
	 * Obtener el valor de numRecordPageDesde.
     *
     * @return numRecordPageDesde valor asignado.
	 */
	public int getNumRecordPageDesde() {
		return numRecordPageDesde;
	}

	/**
     * Asignar un valor a numRecordPageDesde.
     *
     * @param numRecordPageDesde Valor a asignar.
     */
	public void setNumRecordPageDesde(int numRecordPageDesde) {
		this.numRecordPageDesde = numRecordPageDesde;
	}
	/**
	 * Obtener el valor de numRecordPageHasta.
     *
     * @return numRecordPageHasta valor asignado.
	 */
	public int getNumRecordPageHasta() {
		return numRecordPageHasta;
	}

	/**
     * Asignar un valor a numRecordPageHasta.
     *
     * @param numRecordPageHasta Valor a asignar.
     */
	public void setNumRecordPageHasta(int numRecordPageHasta) {
		this.numRecordPageHasta = numRecordPageHasta;
	}

	/**
	 * Obtener el valor de lista.
     *
     * @return lista valor asignado.
	 */
	public Vector getLista() {
		return lista;
	}

	/**
     * Asignar un valor a lista.
     *
     * @param lista Valor a asignar.
     */
	public void setLista(Vector lista) {
		this.lista = lista;
	}

	/**
	 * Obtener el valor de maxNumRecordsPage.
     *
     * @return maxNumRecordsPage valor asignado.
	 */
	public int getMaxNumRecordsPage() {
		return maxNumRecordsPage;
	}

	/**
     * Asignar un valor a maxNumRecordsPage.
     *
     * @param maxNumRecordsPage Valor a asignar.
     */
	public void setMaxNumRecordsPage(int maxNumRecordsPage) {
		this.maxNumRecordsPage = maxNumRecordsPage;
	}

	/**
	 * Obtener el valor de whereCondicion.
     *
     * @return whereCondicion valor asignado.
	 */
	public String getWhereCondicion() {
		return whereCondicion;
	}

	/**
     * Asignar un valor a whereCondicion.
     *
     * @param whereCondicion Valor a asignar.
     */
	public void setWhereCondicion(String whereCondicion) {
		this.whereCondicion = whereCondicion;
	}

}