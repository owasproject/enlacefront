package mx.altec.enlace.bo;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import mx.altec.enlace.beans.InterbancariasRecibidasBeanUSD;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.TranInterConstantes;

import java.text.DecimalFormat;

/**
 *
 * @author FSW TCS
 *
 */
public class InterbancariasRecibidasBOUSD {


	/**
	 * Consulta de transferencias interbancarias recibidas USD
	 * @param bean datos de entrada
	 * @param req request
	 * @param res response
	 * @return bean de resultado
	 * @throws SQLException control de excepciones
	 */
	public InterbancariasRecibidasBeanUSD consultarInterbancariasRecibidasUSD(
			InterbancariasRecibidasBeanUSD bean, HttpServletRequest req,
			HttpServletResponse res) throws SQLException {
		EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java :: consultarInterbancariasRecibidasUSD :: Iniciando Consulta", EIGlobal.NivelLog.INFO);
		InterbancariasRecibidasBeanUSD recibidasBean=null;
		@SuppressWarnings("rawtypes")
		Hashtable hs = null;
		ServicioTux tuxGlobal = new ServicioTux();
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaSalida = "";
		String[] fecha=bean.getFchTrasnfer().split("/");
		EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java :: consultarInterbancariasRecibidasUSD :: bean.getFchTrasnfer   " + bean.getFchTrasnfer(), EIGlobal.NivelLog.INFO);
  		try{
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD.java::Verifica valor  referencia  "+ bean.getReferencia(), EIGlobal.NivelLog.INFO);
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD.java::Verifica Valor  clave de rastreo  "+ bean.getCveRastreo(), EIGlobal.NivelLog.INFO);
  			tramaEntrada.append('R');
  			tramaEntrada.append(fecha[0]);
  			tramaEntrada.append(fecha[1]);
  			tramaEntrada.append(fecha[2]);
  			tramaEntrada.append("        ");
  			if (null == bean.getCveRastreo()  || "".equals(bean.getCveRastreo())) {
  				tramaEntrada.append(bean.getReferencia()); // <------ref interbanc-----
  				tramaEntrada.append("                              ");
  				bean.setCveRastreo("");
			} else if ("".equals(bean.getReferencia()) || null == bean.getReferencia()) {
				tramaEntrada.append("       ");
				tramaEntrada.append(bean.getCveRastreo());
				bean.setReferencia("");
			}
  			tramaEntrada.append("                    ");
  			tramaEntrada.append("   ");
  			tramaEntrada.append(bean.getCuenta()+"         ");
  			tramaEntrada.append("101");
  			if(null == bean.getCveRastreo() || "".equals(bean.getCveRastreo())){
  				tramaEntrada.append("     ");
  			} else {
  				tramaEntrada.append(bean.getBancoBN().substring(0,bean.getBancoBN().indexOf('+')));
  				EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD.java::Verifica valor  BANCO>>>>>  "+ bean.getBancoBN().substring(0,bean.getBancoBN().indexOf('+')), EIGlobal.NivelLog.INFO);
  			}
  			tramaEntrada.append("     ");
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD.java::consultarInterbancariasRecibidasUSD:: Trama Entrada->"+ tramaEntrada.toString(), EIGlobal.NivelLog.INFO);
  			hs = tuxGlobal.tranInfoDet(tramaEntrada.toString(), "TRAN_INFO_DET");
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD.java::consultarInterbancariasRecibidasUSD:: Servicio TUX ::::>    "+ "TRAN_INFO_DET", EIGlobal.NivelLog.INFO);
  			StringBuffer URL=new StringBuffer();
			if(hs != null) {
				recibidasBean=new InterbancariasRecibidasBeanUSD();
				tramaSalida = (String) hs.get("BUFFER");
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::consultarInterbancariasRecibidasUSD:: Trama Salida->"+ tramaSalida, EIGlobal.NivelLog.INFO);
				String datosTrama=tramaSalida;
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::datosTrama------>"+ datosTrama, EIGlobal.NivelLog.INFO);
				setBeanInterbancariasRecibidasBeanUSD(recibidasBean, datosTrama);
				URL.append("<a href=\"MDI_CepServlet?");
				URL.append("opcion=3");
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("fecha=");
				URL.append(bean.getFchTrasnfer().trim());
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("cveRastreo=");
				URL.append(recibidasBean.getCveRastreo().trim());
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("banco=");
				URL.append(datosTrama.substring(86,91));
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("ctaAbono=");
				URL.append(recibidasBean.getNumCtaReceptor().trim());
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("importe=");
				URL.append(formatoImporteS(datosTrama.substring(1149,1169).trim()));
				URL.append(TranInterConstantes.AMPERSON);
				URL.append("ctaCargo=");
				if(null!=recibidasBean.getNumCtaOrdenante()){
					EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::Numero de cuenta de recepcion->"+ recibidasBean.getNumCtaOrdenante() + "<-", EIGlobal.NivelLog.INFO);
					URL.append(datosTrama.substring(596,616).trim());
				}

				URL.append(TranInterConstantes.AMPERSON);
				URL.append(TranInterConstantes.DEPOSITO_A_BANCO);
				URL.append("N");

				URL.append("\" onclick=\"return validaNavegador(this);\">Descargar</a>");
				recibidasBean.setUrl(URL.toString());
				List<InterbancariasRecibidasBeanUSD> lstRecibidas=new ArrayList<InterbancariasRecibidasBeanUSD>();
				lstRecibidas.add(recibidasBean);
				recibidasBean.setLstInterbancarias(lstRecibidas);
				if(recibidasBean != null){
					exportar(recibidasBean, req, res);
				}
			}else{
				EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::consultarInterbancariasRecibidasUSD:: Sin resultado", EIGlobal.NivelLog.INFO);
			}

  		}catch (IOException e) {
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::consultarInterbancariasRecibidasUSD:: Error al crear archivo "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		}catch(Exception e){
  			EIGlobal.mensajePorTrace("InterbancariasRecibidasBOUSD.java::consultarInterbancariasRecibidasUSD:: Error al crear la conexion "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
  		}
		return recibidasBean;
	}

	/**
	 * Aloja de manera odenada dentro del bean InterbancariasRecibidasBeanUSD
	 * los datos recibidos desde la trama recibida.
	 *
	 * @param recibidasBean fija parametros de entrada para consulta de flujo interbancarias recibidas en doalares
	 * @param datosTrama datso obtenidos del servicio Tuxedo
	 */
	public void setBeanInterbancariasRecibidasBeanUSD(InterbancariasRecibidasBeanUSD recibidasBean, String datosTrama){
		recibidasBean.setCodError(datosTrama.substring(0,8) != null ? datosTrama.substring(0,8) : "");//Codigo de error
		recibidasBean.setDescError(datosTrama.substring(8,78) != null ? datosTrama.substring(8,78) : "");// Descripciï¿½n del error
		recibidasBean.setCveMecanismo(datosTrama.substring(78,86) != null ? datosTrama.substring(78,86) : "");// Clave de mecanismo
		recibidasBean.setInstOrdenante(datosTrama.substring(86,91) != null ? consultaDescripcionBanco(datosTrama.substring(86,91)): "");//Institucion Ordenante
		recibidasBean.setNumBanxOrdenante(datosTrama.substring(91,98) != null ? datosTrama.substring(91,98) : "");// Num banxico ordenante
		recibidasBean.setNomBanxLargOrdenante(datosTrama.substring(98,138) != null ? datosTrama.substring(98,138) : "");// Nombre largo de la instituciï¿½n ordenante  <-------------
		recibidasBean.setInstBenef(datosTrama.substring(138,143) != null ? datosTrama.substring(138,143) : "");// Institucion Beneficiaria
		recibidasBean.setNumBanxBenef(datosTrama.substring(143,150) != null ? datosTrama.substring(143,150) : "");//  Numero Banxico Inst benef
		recibidasBean.setNombLargInstBenef(datosTrama.substring(150,190) != null ? datosTrama.substring(150,190) : "");//  Nombre Largo Inst Benef
		recibidasBean.setEstatus(datosTrama.substring(190,192) != null ? datosTrama.substring(190,192) : "");//  Estatus de la operacion
		recibidasBean.setDescEstatus(datosTrama.substring(192,232) != null ? datosTrama.substring(192,232) : "");// Descripcion Estatus
		recibidasBean.setCveMotivDev(datosTrama.substring(232,234) != null ? datosTrama.substring(232,234) : "");// Clave motivo devolucion
		recibidasBean.setDescMotivDev(datosTrama.substring(234,274) != null ? datosTrama.substring(234,274) : "");// descripcion motivo devolucion
		recibidasBean.setFchConstOrdenante(datosTrama.substring(274,282) != null ? datosTrama.substring(274,282) : "");// Fecha de Constituciï¿½n Ordenante
		recibidasBean.setFechaHrConfirmacion(datosTrama.substring(282,300) != null ? datosTrama.substring(282,300) : "");// Fecha de Aprobaciï¿½n
		recibidasBean.setRfcOrdenante(datosTrama.substring(300,318) != null ? datosTrama.substring(300,318) : "");// RFC del Ordenante
		recibidasBean.setRfcReceptor(datosTrama.substring(318,336) != null ? datosTrama.substring(318,336) : "");// RFC del Receptor
		recibidasBean.setNomOrdenante(datosTrama.substring(336,456) != null ? datosTrama.substring(336,456) : "");// Nombre del Ordenante   <-------------
		recibidasBean.setNomReceptor(datosTrama.substring(456,576) != null ? datosTrama.substring(456,576) : "");// Nombre del Receptor
		recibidasBean.setRefPago(datosTrama.substring(576,596) != null ? datosTrama.substring(576,596) : "");//  Referencia del pago < -------num ref---------
		recibidasBean.setNumCtaOrdenante(datosTrama.substring(596,616) != null ? enmascararDigito(datosTrama.substring(596,616), 4) : "");//Cuenta Ordenante
		recibidasBean.setTipoCtaOrdenante(datosTrama.substring(616,619) != null ? datosTrama.substring(616,619) : "");// Tipo de cuenta Ordenante
		recibidasBean.setNumCtaReceptor(datosTrama.substring(619,639) != null ? datosTrama.substring(619,639) : "");// Nï¿½mero de cuenta Receptora
		recibidasBean.setTipoCtaReceptor(datosTrama.substring(639,642) != null ? datosTrama.substring(639,642) : "");// Tipo de cuenta Receptora
		recibidasBean.setCveRastreo(datosTrama.substring(642,672) != null ? datosTrama.substring(642,672) : "");// Clave de Rastreo
		recibidasBean.setRefTransfer(datosTrama.substring(672,679) != null ? datosTrama.substring(672,679) : "");//  Referencia Transfer
		recibidasBean.setConceptoPagoTransfer(datosTrama.substring(679,889) != null ? datosTrama.substring(679,889) : "");//  Concepto Pago
		recibidasBean.setDireccOrdenante(datosTrama.substring(889,1009) != null ? datosTrama.substring(889,1009) : "");//  Direcciï¿½n Ordenante
		recibidasBean.setDireccionBenef(datosTrama.substring(1009,1129) != null ? datosTrama.substring(1009,1129) : "");// Direcciï¿½n Beneficiaria
		recibidasBean.setImpCargo(datosTrama.substring(1129,1149) != null ? datosTrama.substring(1129,1149) : "");// Importe Cargo
		recibidasBean.setImpAbono(formatoImporte(datosTrama.substring(1149,1169) != null ? datosTrama.substring(1149,1169) : ""));// Importe Abono  <---------Formatear
		recibidasBean.setIvaMonto(datosTrama.substring(1169,1189) != null ? datosTrama.substring(1169,1189) : "");// iva
		recibidasBean.setTipoCambio(datosTrama.substring(1189,1204) != null ? datosTrama.substring(1189,1204) : "");// Tipo cambio
		recibidasBean.setImpDivisa(datosTrama.substring(1204,1224) != null ? datosTrama.substring(1204,1224) : "");// imp divisa
		recibidasBean.setDivisaOrdenante(datosTrama.substring(1224,1228) != null ? datosTrama.substring(1224,1228) : "");// divisa
		recibidasBean.setDivisaBenef(datosTrama.substring(1228,1232) != null ? datosTrama.substring(1228,1232) : "");// divisa
		recibidasBean.setLeyendaOrde(datosTrama.substring(1232,1382) != null ? datosTrama.substring(1232,1382) : "");// leyenda
		recibidasBean.setLeyendaBenef(datosTrama.substring(1382,1532) != null ? datosTrama.substring(1382,1532) : "");// leyenda
		String estatus="";
		if(("CO").equals(datosTrama.substring(190,192))) {
			estatus="ACEPTADA Y CONFIRMADA";
		}else if(("DV").equals(datosTrama.substring(190,192))) {
			estatus="DEVUELTA POR EL BANCO RECEPTOR EN EL SPEI";
		}else if(("CA").equals(datosTrama.substring(190,192))) {
			estatus="RECHAZADA POR SANTANDER";
		}else if(("EN").equals(datosTrama.substring(190,192))) {
			estatus="ACEPTADA";
		}else if(("PV").equals(datosTrama.substring(190,192))) {
			estatus="EN PROCESO DE VALIDACI&Oacute;N";
		}else {
			estatus="EN PROCESO DE VALIDACI&Oacute;N";
		}
		recibidasBean.setEstatus(estatus);//se agrega el estatus segun las validaciones
	}

	/**
	 * Metodo para manejar el negocio para exportar el resultado
	 * asociadas al contrato.
	 *
	 * @param recibidasBean			Bean con datos de exportaciï¿½n
	 * @param req					request de la operaciï¿½n
	 * @param res					response de la operaciï¿½n
	 * @throws IOException			Dispara Exception
	 */
	public void exportar(InterbancariasRecibidasBeanUSD recibidasBean, HttpServletRequest req, HttpServletResponse res)
	throws IOException{
		EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::exportar:: Entrando...", EIGlobal.NivelLog.DEBUG);

		char COMA=',';

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer linea = null;
		FileWriter fileExport = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID() + ".doc");
		EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);
		try{
			linea = new StringBuffer();
			linea.append(rellenar("NUMERO DE REFERENCIA",22,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("REFERENCIA INTERBANCARIA",26,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CUENTA ORDENANTE",36,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("NOMBRE DEL ORDENANTE",51,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("RFC DEL ORDENANTE",19,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("BANCO ORDENANTE",41,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("IMPORTE",22,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("ESTATUS",15,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("FECHA Y HORA DE CONFIRMACION",29,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CLAVE DE RASTREO",31,' ','D'));
			linea.append(COMA);
			linea.append(rellenar("CONCEPTO DE PAGO/TRANSFERENCIA",121,' ','D'));
			linea.append(COMA);
			linea.append('\n');

			fileExport.write(linea.toString());
			for(int i=0; i<recibidasBean.getLstInterbancarias().size(); i++){
				InterbancariasRecibidasBeanUSD beanExport = (InterbancariasRecibidasBeanUSD)recibidasBean.getLstInterbancarias().get(i);
				linea = new StringBuffer();
				linea.append(rellenar(beanExport.getRefTransfer(),22,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getRefPago(),26,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getNumCtaOrdenante(),36,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getNomOrdenante(),51,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getRfcOrdenante(),19,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getNomBanxLargOrdenante(),41,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getImpAbono(),22,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getEstatus(),15,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getFechaHrConfirmacion(),29,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getCveRastreo(),31,' ','D'));
				linea.append(COMA);
				linea.append(rellenar(beanExport.getConceptoPagoTransfer(),121,' ','D'));
				linea.append(COMA);
				linea.append('\n');

			    fileExport.write(linea.toString());
			}
			sess.setAttribute("fileExport","/Download/" + session.getUserID() + ".doc");
		}catch (IOException e2) {
			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::exportar:: IOException->" + e2.getMessage(), EIGlobal.NivelLog.INFO);
		}
		catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::exportar:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()+ "Linea de truene->" + lineaError[0], EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::exportar:: Se cerro el archivo.", EIGlobal.NivelLog.DEBUG);
		}
	}

	/**
	 * Rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 * @return aux valor de respuesta
	 */
	public static String rellenar (String cad, int lon, char rel, char tip){
		StringBuffer aux = new StringBuffer();
		if(cad.length()>lon){
			return cad.substring(0,lon);
		}if('I' != tip && 'D' != tip){
			tip = 'I';
		}if( 'D' == tip ){
			aux.append(cad);
			//aux = cad;
		}
		for (int i=0; i<(lon-cad.length()); i++){
			aux.append("");
			aux.append(rel);
			//aux += ""+ rel;
		}
		if( 'I' == tip ){
			aux.append("");
			aux.append(cad);
			//aux += ""+ cad;
		}
		return aux.toString();
	}

	/**
	  * Enmascara un digito ****000
	  * @param digitoTmp valor a enmascarar
	  * @param caracteresVisibles cantidad de caracteres visibles
	  * @return enmascarado digito enmasacarado
	  */
	 public static String enmascararDigito(String digitoTmp, int caracteresVisibles){

		 StringBuffer enmascarado = new StringBuffer();
		 String digito = "";
		 digito = digitoTmp.trim();

		 int beginIndex= 0, endIndex=0, iteraciones=0;
			beginIndex=digito.length() - caracteresVisibles;
			endIndex=digito.length();
			iteraciones=digito.length() - caracteresVisibles;

		 digito = digito != null ? digito :"";
		 if(digito.length() > caracteresVisibles && caracteresVisibles > 0){

			 for(int i=0; i < iteraciones; i++){
					enmascarado.append("*");
				}
				enmascarado.append(digito.substring(beginIndex, endIndex));
				EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD:: DIGITO>> " +
						 "\nCaracteres visibles: "+ caracteresVisibles +
						 "\nLongitud: "+ digito.length() +
						 "\nEnmascarado: " + enmascarado.toString(), EIGlobal.NivelLog.DEBUG);
		 }else{
			 enmascarado.append(digito);
		 }

		 return enmascarado.toString();
	}

	/**
	 * Formato para importe
	 * @param importe valor de importe
	* @return importe formateado
	*/
	public String formatoImporte(String importe){
		try{
			importe=(importe==null || "".equals(importe)) ? "0" : importe;
			double valor = Double.parseDouble(importe); 
			DecimalFormat format = new DecimalFormat("$###,###,###,###,##0.00");
			importe=format.format(valor);
		}catch(Exception e){
			importe="$0.00";
			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::formatoImporte:: error al formatear importe:: "+ e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}

		return importe;
	}
	/**
	 * Formato para importe
	 * @param importe valor de importe
	* @return importe formateado
	*/
	public String formatoImporteS(String importe){
		try{
			importe=(importe==null || "".equals(importe)) ? "0" : importe;
			double valor = Double.parseDouble(importe);
			DecimalFormat format = new DecimalFormat("###,###,###,###,##0.00");
			importe=format.format(valor);
		}catch(Exception e){
			importe="0.00";
			EIGlobal.mensajePorTrace("InterbancariasRecibidasUSD::formatoImporte:: error al formatear importe:: "+ e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}

		return importe;
	}
	/**
	 * Metodo que realiza la consulta de la clave del banco
	 * @param banco banco a consultar
	 * @return clave numerica del banco
	 */
	public static String consultaDescripcionBanco(String banco) {
		String claveBanco="";
		ResultSet result =null;
		Statement stmt = null;
		Connection conexion=null;
		try {
			String query="SELECT NOMBRE_CORTO FROM COMU_INTERME_FIN WHERE CVE_INTERME= '"+banco+"'";
			conexion=createiASConn_static (Global.DATASOURCE_ORACLE);
			stmt = conexion.createStatement();
			result = stmt.executeQuery(query);
			EIGlobal.mensajePorTrace("TransferenciaInterbancariaDAO::consultaClaveBanco::query:" + query,EIGlobal.NivelLog.DEBUG);
			while (result.next ()) {
				try {
					claveBanco=result.getString("NOMBRE_CORTO");
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				cierraConexion(conexion);
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			try{
				if(null!=conexion) {
					conexion.close();
					conexion=null;
				}
			}catch(SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return claveBanco;
	}
	/**
	 * Metodo que realiza la conexion a la Base de Datos
	 * @param dbName nombre de base de datos
	 * @return Connection conexion
	 * @throws SQLException Excepcion de Base de Datos
	 */
	public static Connection createiASConn_static ( String dbName )
		throws SQLException {
		Connection conn = null;
		ServiceLocator servLocator = ServiceLocator.getInstance();
		DataSource ds = null;
		try {
			 ds = servLocator.getDataSource(dbName);
		 }catch(NamingException e) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		 }
		 if(null == ds) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
			 return null;
		 }
		 conn = ds.getConnection ();
		 return conn;
	}
	/**
	 * Metodo que realiza el cierre de la conexion
	 * @param conexion
	 * @throws SQLException
	 */
	public static void cierraConexion(Connection conexion)	throws SQLException{
		try{
			if(null!=conexion) {
				conexion.close();
				conexion=null;
			}
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}
	}
}