package mx.altec.enlace.bo;

import java.util.List;

import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.beans.CuentasSantanderBean;
import mx.altec.enlace.beans.CuentaInterbancariaBean;
import mx.altec.enlace.beans.CuentaInternacionalBean;
import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.dao.ConsultaCtasDAO;
/**
 * 
 * @author OLALDE BRAVO JORGE HORACIO
 *
 */
public class ConsultaCtasBO {
	/**
	 * ConsultaCuentasDAO conexion para realizar los querys y la invocacion del servidor tuxedo
	 */
	private final transient ConsultaCtasDAO ConsultaCuentasDAO = new ConsultaCtasDAO();
	/**
	 * Metodo consultaCtasInterbancarias que realiza la consulta a oracle de cuentas Interbancarias
	 * @param consultaCtasInterbancariasBean bean con los parametros para realizar la consulta
	 * @return consultaCuentaInterbancariaBean lista de beans con el resultado de la consulta
	 */
	public List<CuentaInterbancariaBean> consultaCtasInterbancarias(CuentaInterbancariaBean consultaCtasInterbancariasBean){
		
		return ConsultaCuentasDAO.consultaCuentaInterbancariaBean(consultaCtasInterbancariasBean);
	}
	/**
	 * Metodo consultaCtasInterbancarias que realiza la consulta a oracle de cuentas Interbancarias
	 * @param consultaCtasInterbancariasBean bean con los parametros para realizar la consulta
	 * @return CuentaInternacionalBean lista de beans con el resultado de la consulta
	 */
	public List<CuentaInternacionalBean> consultaCtasInternacionales(CuentaInternacionalBean consultaCtasInterbancariasBean){
		
		return ConsultaCuentasDAO.consultaCuentaInternacionalBean(consultaCtasInterbancariasBean);
	}
	/**
	 * 
	 * @param consultaCtasSantanderBean bean con los parametros para realizar la consulta
	 * @return CuentasSantanderBean lista de beans con el resultado de la consulta
	 */
	public List<CuentasSantanderBean> consultaCtasSantander(CuentasSantanderBean consultaCtasSantanderBean){
		return ConsultaCuentasDAO.consultaCuentaSantanderBean(consultaCtasSantanderBean);
	}
	/**
	 * 
	 * @param consultaCtasSantanderBean bean con los parametros para realizar la consulta
	 * @return CuentasSantanderBean lista de beans con el resultado de la consulta
	 */
	public List<CuentaMovilBean> consultaCtasMovil(ConsultaCtasMovilesBean consultaCtasMovilBean){
		return ConsultaCuentasDAO.consultaCtasMoviles(consultaCtasMovilBean);
	}
	/**
	 * 
	 * @param consultaCtasInterbancariasBean bean con los parametros para realizar la consulta
	 * @return CuentaInterbancariaBean lista de beans con el resultado de la consulta
	 */
	public int consultaCtasInterbancariasTotal(CuentaInterbancariaBean consultaCtasInterbancariasBean){
		
		return ConsultaCuentasDAO.consultaCuentaInterbancariaTotales(consultaCtasInterbancariasBean);
	}
	/**
	 * 
	 * @param consultaCtasInterbancariasBean bean con los parametros para realizar la consulta
	 * @return CuentaInternacionalBean lista de beans con el resultado de la consulta
	 */
	public int consultaCtasInternacionalesTotal(CuentaInternacionalBean consultaCtasInterbancariasBean){
		
		return ConsultaCuentasDAO.consultaCuentaInternacionalTotal(consultaCtasInterbancariasBean);
	}
	/**
	 * 
	 * @param consultaCtasSantanderBean bean con los parametros para realizar la consulta
	 * @return CuentasSantanderBean lista de beans con el resultado de la consulta
	 */
	public int consultaCtasSantanderTotal(CuentasSantanderBean consultaCtasSantanderBean){
		return ConsultaCuentasDAO.consultaCuentaSantanderTotal(consultaCtasSantanderBean);
	}
	/**
	 * 
	 * @param consultaCtasSantanderBean bean con los parametros para realizar la consulta
	 * @return CuentasSantanderBean lista de beans con el resultado de la consulta
	 */
	public int consultaCtasMovilTotal(ConsultaCtasMovilesBean consultaCtasMovilBean){
		return ConsultaCuentasDAO.consultaCuentaMovilTotal(consultaCtasMovilBean);
	}
}
