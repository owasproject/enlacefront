package mx.altec.enlace.bo;

import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase bean para manejar el procedimiento para la transaccion PE80
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Sep 23, 2010
 */
public class TrxPE80VO implements Serializable {			

	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Variable peNumPer
	 */
	private String peNumPer = null;
	
	/**
	 * Variable peCalPar
	 */
	private String peCalPar = null;
	
	/**
	 * Variable peOrdPar
	 */
	private String peOrdPar = null;
	
	/**
	 * Variable peCodPro
	 */
	private String peCodPro = null;
	
	/**
	 * Variable peCodSub
	 */
	private String peCodSub = null;
	
	/**
	 * Variable fecBaja
	 */
	private String fecBaja = null;
	
	/**
	 * Variable peEstRel
	 */
	private String peEstRel = null;
	
	/**
	 * Variable peResInt
	 */
	private String peResInt = null;
	
	/**
	 * Variable peMarPaq
	 */
	private String peMarPaq = null;
	
	/**
	 * Variable peMotBaj
	 */
	private String peMotBaj = null;
	
	/**
	 * Variable peTipDoc
	 */
	private String peTipDoc = null;
	
	/**
	 * Variable peNumDoc
	 */
	private String peNumDoc = null;
	
	/**
	 * Variable peSecDoc
	 */
	private String peSecDoc = null;
	
	/**
	 * Variable peNomFan
	 */
	private String peNomFan = null;
	
	/**
	 * Variable pePriApe
	 */
	private String pePriApe = null;
	
	/**
	 * Variable peSegApe
	 */
	private String peSegApe = null;
	
	/**
	 * Variable peNomPer
	 */
	private String peNomPer = null;
	
	/**
	 * Variable peFecNac
	 */
	private String peFecNac = null;
	
	/**
	 * Variable peTipPer
	 */
	private String peTipPer = null;
	
	/**
	 * Variable peEstPer
	 */
	private String peEstPer = null;
	
	/**
	 * Variable peConPer
	 */
	private String peConPer = null;
	
	/**
	 * Variable peNivAcc
	 */
	private String peNivAcc = null;
	
	/**
	 * Variable peIndRel
	 */
	private String peIndRel = null;
	
	/**
	 * Variable peIndGru
	 */
	private String peIndGru = null;
	
	/**
	 * Variable peIndN01
	 */
	private String peIndN01 = null;
	
	/**
	 * Variable peIndN02
	 */
	private String peIndN02 = null;
	
	/**
	 * Variable peIndN03
	 */
	private String peIndN03 = null;
	
	/**
	 * Variable peIndN04
	 */
	private String peIndN04 = null;
	
	/**
	 * Variable peIndN05
	 */
	private String peIndN05 = null;
	
	/**
	 * Variable peTipVi
	 */
	private String peTipVi = null;

	/**
	 * Variable peNomCa
	 */
	private String peNomCa = null;
	
	/**
	 * Variable peObse1
	 */
	private String peObse1 = null;
	
	/**
	 * Variable peObse2
	 */
	private String peObse2 = null;
	
	/**
	 * Variable peNumBl
	 */
	private String peNumBl = null;
	
	/**
	 * Variable peNomLo
	 */
	private String peNomLo = null;
	
	/**
	 * Variable peNomCo
	 */
	private String peNomCo = null;
	
	/**
	 * Variable peCodPo
	 */
	private String peCodPo = null;
	
	/**
	 * Variable peRutCa
	 */
	private String peRutCa = null;
	
	/**
	 * Variable codPrv
	 */
	private String codPrv = null;
	
	/**
	 * Variable peCodPa
	 */
	private String peCodPa = null;
	
	/**
	 * Variable peMarNo
	 */
	private String peMarNo = null;

	/**
	 * Variable peHStam
	 */
	private String peHStam = null;
	
	/**
	 * Variable peNumTc
	 */
	private String peNumTc = null;
	
	/**
	 * Variable peHStamPEDT207
	 */
	private String peHStamPEDT207 = null;

	/**
	 * Obtener el valor de peNumPer.
     *
     * @return peNumPer valor asignado.
	 */
	public String getPeNumPer() {
		return peNumPer;
	}

	/**
     * Asignar un valor a peNumPer.
     *
     * @param peNumPer Valor a asignar.
     */
	public void setPeNumPer(String peNumPer) {
		this.peNumPer = peNumPer;
	}

	/**
	 * Obtener el valor de peCalPar.
     *
     * @return peCalPar valor asignado.
	 */
	public String getPeCalPar() {
		return peCalPar;
	}

	/**
     * Asignar un valor a peCalPar.
     *
     * @param peCalPar Valor a asignar.
     */
	public void setPeCalPar(String peCalPar) {
		this.peCalPar = peCalPar;
	}

	/**
	 * Obtener el valor de peOrdPar.
     *
     * @return peOrdPar valor asignado.
	 */
	public String getPeOrdPar() {
		return peOrdPar;
	}

	/**
     * Asignar un valor a peOrdPar.
     *
     * @param peOrdPar Valor a asignar.
     */
	public void setPeOrdPar(String peOrdPar) {
		this.peOrdPar = peOrdPar;
	}

	/**
	 * Obtener el valor de peCodPro.
     *
     * @return peCodPro valor asignado.
	 */
	public String getPeCodPro() {
		return peCodPro;
	}

	/**
     * Asignar un valor a peCodPro.
     *
     * @param peCodPro Valor a asignar.
     */
	public void setPeCodPro(String peCodPro) {
		this.peCodPro = peCodPro;
	}

	/**
	 * Obtener el valor de peCodSub.
     *
     * @return peCodSub valor asignado.
	 */
	public String getPeCodSub() {
		return peCodSub;
	}

	/**
     * Asignar un valor a peCodSub.
     *
     * @param peCodSub Valor a asignar.
     */
	public void setPeCodSub(String peCodSub) {
		this.peCodSub = peCodSub;
	}

	/**
	 * Obtener el valor de fecBaja.
     *
     * @return fecBaja valor asignado.
	 */
	public String getFecBaja() {
		return fecBaja;
	}

	/**
     * Asignar un valor a fecBaja.
     *
     * @param fecBaja Valor a asignar.
     */
	public void setFecBaja(String fecBaja) {
		this.fecBaja = fecBaja;
	}

	/**
	 * Obtener el valor de peEstRel.
     *
     * @return peEstRel valor asignado.
	 */
	public String getPeEstRel() {
		return peEstRel;
	}

	/**
     * Asignar un valor a peEstRel.
     *
     * @param peEstRel Valor a asignar.
     */
	public void setPeEstRel(String peEstRel) {
		this.peEstRel = peEstRel;
	}

	/**
	 * Obtener el valor de peResInt.
     *
     * @return peResInt valor asignado.
	 */
	public String getPeResInt() {
		return peResInt;
	}

	/**
     * Asignar un valor a peResInt.
     *
     * @param peResInt Valor a asignar.
     */
	public void setPeResInt(String peResInt) {
		this.peResInt = peResInt;
	}

	/**
	 * Obtener el valor de peMarPaq.
     *
     * @return peMarPaq valor asignado.
	 */
	public String getPeMarPaq() {
		return peMarPaq;
	}

	/**
     * Asignar un valor a peMarPaq.
     *
     * @param peMarPaq Valor a asignar.
     */
	public void setPeMarPaq(String peMarPaq) {
		this.peMarPaq = peMarPaq;
	}

	/**
	 * Obtener el valor de peMotBaj.
     *
     * @return peMotBaj valor asignado.
	 */
	public String getPeMotBaj() {
		return peMotBaj;
	}

	/**
     * Asignar un valor a peMotBaj.
     *
     * @param peMotBaj Valor a asignar.
     */
	public void setPeMotBaj(String peMotBaj) {
		this.peMotBaj = peMotBaj;
	}

	/**
	 * Obtener el valor de peTipDoc.
     *
     * @return peTipDoc valor asignado.
	 */
	public String getPeTipDoc() {
		return peTipDoc;
	}

	/**
     * Asignar un valor a peTipDoc.
     *
     * @param peTipDoc Valor a asignar.
     */
	public void setPeTipDoc(String peTipDoc) {
		this.peTipDoc = peTipDoc;
	}

	/**
	 * Obtener el valor de peNumDoc.
     *
     * @return peNumDoc valor asignado.
	 */
	public String getPeNumDoc() {
		return peNumDoc;
	}

	/**
     * Asignar un valor a peNumDoc.
     *
     * @param peNumDoc Valor a asignar.
     */
	public void setPeNumDoc(String peNumDoc) {
		this.peNumDoc = peNumDoc;
	}

	/**
	 * Obtener el valor de peSecDoc.
     *
     * @return peSecDoc valor asignado.
	 */
	public String getPeSecDoc() {
		return peSecDoc;
	}

	/**
     * Asignar un valor a peSecDoc.
     *
     * @param peSecDoc Valor a asignar.
     */
	public void setPeSecDoc(String peSecDoc) {
		this.peSecDoc = peSecDoc;
	}

	/**
	 * Obtener el valor de peNomFan.
     *
     * @return peNomFan valor asignado.
	 */
	public String getPeNomFan() {
		return peNomFan;
	}

	/**
     * Asignar un valor a peNomFan.
     *
     * @param peNomFan Valor a asignar.
     */
	public void setPeNomFan(String peNomFan) {
		this.peNomFan = peNomFan;
	}

	/**
	 * Obtener el valor de pePriApe.
     *
     * @return pePriApe valor asignado.
	 */
	public String getPePriApe() {
		return pePriApe;
	}

	/**
     * Asignar un valor a pePriApe.
     *
     * @param pePriApe Valor a asignar.
     */
	public void setPePriApe(String pePriApe) {
		this.pePriApe = pePriApe;
	}

	/**
	 * Obtener el valor de peSegApe.
     *
     * @return peSegApe valor asignado.
	 */
	public String getPeSegApe() {
		return peSegApe;
	}

	/**
     * Asignar un valor a peSegApe.
     *
     * @param peSegApe Valor a asignar.
     */
	public void setPeSegApe(String peSegApe) {
		this.peSegApe = peSegApe;
	}

	/**
	 * Obtener el valor de peNomPer.
     *
     * @return peNomPer valor asignado.
	 */
	public String getPeNomPer() {
		return peNomPer;
	}

	/**
     * Asignar un valor a peNomPer.
     *
     * @param peNomPer Valor a asignar.
     */
	public void setPeNomPer(String peNomPer) {
		this.peNomPer = peNomPer;
	}

	/**
	 * Obtener el valor de peFecNac.
     *
     * @return peFecNac valor asignado.
	 */
	public String getPeFecNac() {
		return peFecNac;
	}

	/**
     * Asignar un valor a peFecNac.
     *
     * @param peFecNac Valor a asignar.
     */
	public void setPeFecNac(String peFecNac) {
		this.peFecNac = peFecNac;
	}

	/**
	 * Obtener el valor de peTipPer.
     *
     * @return peTipPer valor asignado.
	 */
	public String getPeTipPer() {
		return peTipPer;
	}

	/**
     * Asignar un valor a peTipPer.
     *
     * @param peTipPer Valor a asignar.
     */
	public void setPeTipPer(String peTipPer) {
		this.peTipPer = peTipPer;
	}

	/**
	 * Obtener el valor de peEstPer.
     *
     * @return peEstPer valor asignado.
	 */
	public String getPeEstPer() {
		return peEstPer;
	}

	/**
     * Asignar un valor a peEstPer.
     *
     * @param peEstPer Valor a asignar.
     */
	public void setPeEstPer(String peEstPer) {
		this.peEstPer = peEstPer;
	}

	/**
	 * Obtener el valor de peConPer.
     *
     * @return peConPer valor asignado.
	 */
	public String getPeConPer() {
		return peConPer;
	}

	/**
     * Asignar un valor a peConPer.
     *
     * @param peConPer Valor a asignar.
     */
	public void setPeConPer(String peConPer) {
		this.peConPer = peConPer;
	}

	/**
	 * Obtener el valor de peNivAcc.
     *
     * @return peNivAcc valor asignado.
	 */
	public String getPeNivAcc() {
		return peNivAcc;
	}

	/**
     * Asignar un valor a peNivAcc.
     *
     * @param peNivAcc Valor a asignar.
     */
	public void setPeNivAcc(String peNivAcc) {
		this.peNivAcc = peNivAcc;
	}

	/**
	 * Obtener el valor de peIndRel.
     *
     * @return peIndRel valor asignado.
	 */
	public String getPeIndRel() {
		return peIndRel;
	}

	/**
     * Asignar un valor a peIndRel.
     *
     * @param peIndRel Valor a asignar.
     */
	public void setPeIndRel(String peIndRel) {
		this.peIndRel = peIndRel;
	}

	/**
	 * Obtener el valor de peIndGru.
     *
     * @return peIndGru valor asignado.
	 */
	public String getPeIndGru() {
		return peIndGru;
	}

	/**
     * Asignar un valor a peIndGru.
     *
     * @param peIndGru Valor a asignar.
     */
	public void setPeIndGru(String peIndGru) {
		this.peIndGru = peIndGru;
	}

	/**
	 * Obtener el valor de peIndN01.
     *
     * @return peIndN01 valor asignado.
	 */
	public String getPeIndN01() {
		return peIndN01;
	}

	/**
     * Asignar un valor a peIndN01.
     *
     * @param peIndN01 Valor a asignar.
     */
	public void setPeIndN01(String peIndN01) {
		this.peIndN01 = peIndN01;
	}

	/**
	 * Obtener el valor de peIndN02.
     *
     * @return peIndN02 valor asignado.
	 */
	public String getPeIndN02() {
		return peIndN02;
	}

	/**
     * Asignar un valor a peIndN02.
     *
     * @param peIndN02 Valor a asignar.
     */
	public void setPeIndN02(String peIndN02) {
		this.peIndN02 = peIndN02;
	}

	/**
	 * Obtener el valor de peIndN03.
     *
     * @return peIndN03 valor asignado.
	 */
	public String getPeIndN03() {
		return peIndN03;
	}

	/**
     * Asignar un valor a peIndN03.
     *
     * @param peIndN03 Valor a asignar.
     */
	public void setPeIndN03(String peIndN03) {
		this.peIndN03 = peIndN03;
	}

	/**
	 * Obtener el valor de peIndN04.
     *
     * @return peIndN04 valor asignado.
	 */
	public String getPeIndN04() {
		return peIndN04;
	}

	/**
     * Asignar un valor a peIndN04.
     *
     * @param peIndN04 Valor a asignar.
     */
	public void setPeIndN04(String peIndN04) {
		this.peIndN04 = peIndN04;
	}

	/**
	 * Obtener el valor de peIndN05.
     *
     * @return peIndN05 valor asignado.
	 */
	public String getPeIndN05() {
		return peIndN05;
	}

	/**
     * Asignar un valor a peIndN05.
     *
     * @param peIndN05 Valor a asignar.
     */
	public void setPeIndN05(String peIndN05) {
		this.peIndN05 = peIndN05;
	}

	/**
	 * Obtener el valor de peTipVi.
     *
     * @return peTipVi valor asignado.
	 */
	public String getPeTipVi() {
		return peTipVi;
	}

	/**
     * Asignar un valor a peTipVi.
     *
     * @param peTipVi Valor a asignar.
     */
	public void setPeTipVi(String peTipVi) {
		this.peTipVi = peTipVi;
	}

	/**
	 * Obtener el valor de peNomCa.
     *
     * @return peNomCa valor asignado.
	 */
	public String getPeNomCa() {
		return peNomCa;
	}

	/**
     * Asignar un valor a peNomCa.
     *
     * @param peNomCa Valor a asignar.
     */
	public void setPeNomCa(String peNomCa) {
		this.peNomCa = peNomCa;
	}

	/**
	 * Obtener el valor de peObse1.
     *
     * @return peObse1 valor asignado.
	 */
	public String getPeObse1() {
		return peObse1;
	}

	/**
     * Asignar un valor a peObse1.
     *
     * @param peObse1 Valor a asignar.
     */
	public void setPeObse1(String peObse1) {
		this.peObse1 = peObse1;
	}

	/**
	 * Obtener el valor de peObse2.
     *
     * @return peObse2 valor asignado.
	 */
	public String getPeObse2() {
		return peObse2;
	}

	/**
     * Asignar un valor a peObse2.
     *
     * @param peObse2 Valor a asignar.
     */
	public void setPeObse2(String peObse2) {
		this.peObse2 = peObse2;
	}

	/**
	 * Obtener el valor de peNumBl.
     *
     * @return peNumBl valor asignado.
	 */
	public String getPeNumBl() {
		return peNumBl;
	}

	/**
     * Asignar un valor a peNumBl.
     *
     * @param peNumBl Valor a asignar.
     */
	public void setPeNumBl(String peNumBl) {
		this.peNumBl = peNumBl;
	}

	/**
	 * Obtener el valor de peNomLo.
     *
     * @return peNomLo valor asignado.
	 */
	public String getPeNomLo() {
		return peNomLo;
	}

	/**
     * Asignar un valor a peNomLo.
     *
     * @param peNomLo Valor a asignar.
     */
	public void setPeNomLo(String peNomLo) {
		this.peNomLo = peNomLo;
	}

	/**
	 * Obtener el valor de peNomCo.
     *
     * @return peNomCo valor asignado.
	 */
	public String getPeNomCo() {
		return peNomCo;
	}

	/**
     * Asignar un valor a peNomCo.
     *
     * @param peNomCo Valor a asignar.
     */
	public void setPeNomCo(String peNomCo) {
		this.peNomCo = peNomCo;
	}

	/**
	 * Obtener el valor de peCodPo.
     *
     * @return peCodPo valor asignado.
	 */
	public String getPeCodPo() {
		return peCodPo;
	}

	/**
     * Asignar un valor a peCodPo.
     *
     * @param peCodPo Valor a asignar.
     */
	public void setPeCodPo(String peCodPo) {
		this.peCodPo = peCodPo;
	}

	/**
	 * Obtener el valor de peRutCa.
     *
     * @return peRutCa valor asignado.
	 */
	public String getPeRutCa() {
		return peRutCa;
	}

	/**
     * Asignar un valor a peRutCa.
     *
     * @param peRutCa Valor a asignar.
     */
	public void setPeRutCa(String peRutCa) {
		this.peRutCa = peRutCa;
	}

	/**
	 * Obtener el valor de codPrv.
     *
     * @return codPrv valor asignado.
	 */
	public String getCodPrv() {
		return codPrv;
	}

	/**
     * Asignar un valor a codPrv.
     *
     * @param codPrv Valor a asignar.
     */
	public void setCodPrv(String codPrv) {
		this.codPrv = codPrv;
	}

	/**
	 * Obtener el valor de peCodPa.
     *
     * @return peCodPa valor asignado.
	 */
	public String getPeCodPa() {
		return peCodPa;
	}

	/**
     * Asignar un valor a peCodPa.
     *
     * @param peCodPa Valor a asignar.
     */
	public void setPeCodPa(String peCodPa) {
		this.peCodPa = peCodPa;
	}

	/**
	 * Obtener el valor de peMarNo.
     *
     * @return peMarNo valor asignado.
	 */
	public String getPeMarNo() {
		return peMarNo;
	}

	/**
     * Asignar un valor a peMarNo.
     *
     * @param peMarNo Valor a asignar.
     */
	public void setPeMarNo(String peMarNo) {
		this.peMarNo = peMarNo;
	}

	/**
	 * Obtener el valor de peHStam.
     *
     * @return peHStam valor asignado.
	 */
	public String getPeHStam() {
		return peHStam;
	}

	/**
     * Asignar un valor a peHStam.
     *
     * @param peHStam Valor a asignar.
     */
	public void setPeHStam(String peHStam) {
		this.peHStam = peHStam;
	}

	/**
	 * Obtener el valor de peNumTc.
     *
     * @return peNumTc valor asignado.
	 */
	public String getPeNumTc() {
		return peNumTc;
	}

	/**
     * Asignar un valor a peNumTc.
     *
     * @param peNumTc Valor a asignar.
     */
	public void setPeNumTc(String peNumTc) {
		this.peNumTc = peNumTc;
	}

	/**
	 * Obtener el valor de peHStamPEDT207.
     *
     * @return peHStamPEDT207 valor asignado.
	 */
	public String getPeHStamPEDT207() {
		return peHStamPEDT207;
	}

	/**
     * Asignar un valor a peHStamPEDT207.
     *
     * @param peHStamPEDT207 Valor a asignar.
     */
	public void setPeHStamPEDT207(String peHStamPEDT207) {
		this.peHStamPEDT207 = peHStamPEDT207;
	}
	
	/**
	 * Override toString
	 */
	public String toString() {
		StringBuffer registro = new StringBuffer();		
		registro.append("peNumPer->" + getPeNumPer());
		registro.append("peCalPar->" + getPeCalPar());
		registro.append("peOrdPar->" + getPeOrdPar());
		registro.append("peCodPro->" + getPeCodPro());
		registro.append("peCodSub->" + getPeCodSub());
		registro.append("fecBaja->" + getFecBaja()); 
		registro.append("peEstRel->" + getPeEstRel());
		registro.append("peResInt->" + getPeResInt());
		registro.append("peMarPaq->" + getPeMarPaq());
		registro.append("peMotBaj->" + getPeMotBaj());
		registro.append("peTipDoc->" + getPeTipDoc());
		registro.append("peNumDoc->" + getPeNumDoc());
		registro.append("peSecDoc->" + getPeSecDoc());
		registro.append("peNomFan" + getPeNomFan());
		registro.append("pePriApe->" + getPePriApe());
		registro.append("peSegApe->" + getPeSegApe());
		registro.append("peNomPer->" + getPeNomPer());
		registro.append("peFecNac->" + getPeFecNac());
		registro.append("peTipPer->" + getPeTipPer());
		registro.append("peEstPer->" + getPeEstPer());
		registro.append("peConPer->" + getPeConPer());
		registro.append("peNivAcc->" + getPeNivAcc());
		registro.append("peIndRel->" + getPeIndRel());
		registro.append("peIndGru->" + getPeIndGru());
		registro.append("peIndN01->" + getPeIndN01());
		registro.append("peIndN02->" + getPeIndN02());
		registro.append("peIndN03->" + getPeIndN03());
		registro.append("peIndN04->" + getPeIndN04());
		registro.append("peIndN05->" + getPeIndN05());
		registro.append("peTipVi->" + getPeTipVi());
		registro.append("peNomCa->" + getPeNomCa());
		registro.append("peObse1->" + getPeObse1());
		registro.append("peObse2->" + getPeObse2());
		registro.append("peNumBl->" + getPeNumBl());
		registro.append("peNomLo->" + getPeNomLo());
		registro.append("peNomCo->" + getPeNomCo());
		registro.append("peCodPo->" + getPeCodPo());
		registro.append("peRutCa->" + getPeRutCa());
		registro.append("codPrv->" + getCodPrv());
		registro.append("peCodPa->" + getPeCodPa());
		registro.append("peMarNo->" + getPeMarNo());
		registro.append("peHStam->" + getPeHStam());
		registro.append("peNumTc->" + getPeNumTc());
		registro.append("peHStamPEDT207->" + getPeHStamPEDT207());		
		return registro.toString();
	}

}
