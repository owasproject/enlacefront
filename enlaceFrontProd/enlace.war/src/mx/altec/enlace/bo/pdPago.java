/*
 * pdPago.java
 *
 * Created on 19 de marzo de 2002, 05:18 PM
 */

package mx.altec.enlace.bo;

import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.text.*;


/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class pdPago implements java.io.Serializable {

	/** Cuenta donde se hara el cargo */
	protected String CuentaCargo;
	/** Numero de pago */
	protected String NoPago;
	/**Clave del Beneficiario no regsitrado*/
	protected String ClavenoReg;
	/** Clave del beneficiario */
	protected String ClaveBen;
	/** Nombre del beneficiario */
	protected String NomBen;
	/** Importe del pago */
	protected double Importe;
	/** Fecha de liberacion del pago */
	protected GregorianCalendar FechaLib;
	/** Fecha de vencimiento del pago */
	protected GregorianCalendar FechaPago;
	/** Concepto del pago */
	protected String Concepto;
	/** Forma del pago (Efectivo o cheque) */
	protected char FormaPago;
	/** Clave de la sucursal donde se puede hacer el cobro */
	protected String ClaveSucursal;
	/**Clave para todas las sucursales*/
	protected char TSucursales;
	/** Estatus del pago */
	protected char Estatus;
	/** Numero de referencia */
	protected String Referencia;
	/** Descripcion del estatus */
	protected String DescripcionEstatus;
	/** Fecha de pago */
	protected GregorianCalendar FechaPagado;
	/** Fecha de cancelacion del pago */
	protected GregorianCalendar FechaCancelacion;
	/** Fecha de vencimiento del pago */
	protected GregorianCalendar FechaVencimiento;
	/** Fecha de modificacion del pago */
	protected GregorianCalendar FechaModificacion;
	/** Clave de region*/
	protected String ClaveRegion;
	/** Clave de plaza */
	protected String ClavePlaza;
	/** Fecha en que vencio el pago */
	protected GregorianCalendar FechaVencio;

	/** Constructor que no recibe parametros para crear el bean con un JSP */
	public pdPago() {
		CuentaCargo = "";
		NoPago = "0";
		ClavenoReg = "";
		ClaveBen = "";
		NomBen = "";
		Importe = 0.0;
		FechaLib = new GregorianCalendar();
		FechaPago = new GregorianCalendar();
		Concepto = "";
		FormaPago = 'C';
                ClaveSucursal = "";
                Estatus = ' ';
                Referencia = "";
                DescripcionEstatus = "";
	}

	// Constructor con los valores del bean
	public pdPago(String cc, String np, String cln, String cb, String nb, double imp,
                    GregorianCalendar fl, GregorianCalendar fp, String con, char fop, String suc) {
		CuentaCargo = cc;
		NoPago = np;
		ClavenoReg = cln;
		ClaveBen = cb;
		NomBen = nb;
		Importe = imp;
		FechaLib = fl;
		FechaPago = fp;
		Concepto = con;
		FormaPago = fop;
		ClaveSucursal = suc;
                Estatus = ' ';
                Referencia = "";
                DescripcionEstatus = "";
	}

        /**
         * Constructor para copiar una clase pdCPago a una pdPago
         * @param pag Pago en la clase de pdCPago
         */
        public pdPago (pdCPago pag) {
            CuentaCargo = pag.getNoCta ();
            NoPago = pag.getNoOrden();
			ClavenoReg = pag.getClvnoreg ();
            ClaveBen = pag.getCveBenef ();
            NomBen = pag.getNombreBenefNoRegistrado ();
            Importe = pag.getImporte ();
            FechaLib = pag.getFechaRecepcion ();
            FechaPago = pag.getFechaLimPago ();
            Concepto = pag.getInstrucciones ();
            FormaPago = pag.getFmaPago ();
            ClaveSucursal = pag.getCveSucursal ();
            Estatus = pag.getStatus ();
            Referencia = pag.getNoSec ().toString ();
            DescripcionEstatus = pag.getStatusDesc ();
            FechaCancelacion = pag.getFechaCancelacion ();
            FechaModificacion = pag.getFechaModificacion ();
            FechaPagado = pag.getFechaPago ();
            FechaVencimiento = pag.getFechaAplic ();
            FechaVencio = pag.getFechaVencimiento ();
        }

	/**
         * Metodo para obtener la cuenta de cargo
         * @return Cuenta de cargo
         */
	public String getCuentaCargo() {return CuentaCargo;}

        /**
         * Metodo para obtener el numero de pago
         * @return Numero de pago
         */
	public String getNoPago() {return NoPago;}

        /**
		 * Metodo para obtenre la clave del beneficairio no regsitrado
		 * @return Clave del beneficiario no regsitrado
		 */
	public String getClavenoReg() {return ClavenoReg;}
		 /**
         * Metodo para obtener la clave del beneficiario
         * @return Clave de beneficiario
         */

	public String getClaveBen() {return ClaveBen;}

        /**
         * Metodo para obtener el nombre del beneficiario
         * @return Nombre de beneficiario
         */
	public String getNomBen() {return NomBen;}

        /**
         * Metodo para obtener el importe del pago
         * @return Importe del pago
         */
	public double getImporte() {return Importe;}

        /**
         * Metodo para obtener el importe formateado en moneda
         * @return Importe con formato de moneda
         */
    public String getImporteFor () {
        NumberFormat nf = NumberFormat.getCurrencyInstance (java.util.Locale.US);
        String formato = nf.format (Importe);
        if ((formato.length() >= 3)&& (formato.substring(0, 3).equals("MXN"))){
        	formato = "$ " + formato.substring(3, formato.length());
        }
        return formato;
    }

        /**
         * Metodo para obtener la fecha de liberacion del pago
         * @return Fecha de liberacion del pago
         */
	public GregorianCalendar getFechaLib() {return FechaLib;}

        /**
         * Metodo para obtener la fecha limite del pago
         * @return Fecha limite de pago
         */
	public GregorianCalendar getFechaPago() {return FechaPago;}

        /**
         * Metodo para obtener el concepto del pago
         * @return Concepto del pago
         */
	public String getConcepto() {return Concepto;}

        /**
         * Metodo para obtener la forma de pago
         * @return Forma de pago en caracter
         */
	public char getFormaPago() {return FormaPago;}

        /**
         * Metodo para obtener la clave de sucursal
         * @return Clave de sucursal
         */
	public String getClaveSucursal() {return ClaveSucursal;}

        /**
         * Metodo para obtener la bandera de pago en todas las sucursales
         * @return true si se puede pagar en todas, de lo contrario false
         */
        public boolean getTodasSucursales() {return ClaveSucursal.equals("");}

		/**
		 * Metodo para obtener Todas las sucursales
		 * @return TSucursales en caracter
		 */
		public char getTSucursales(){return TSucursales;}

        /**
         * Metodo para obtener el estatus del pago
         * @return Estatus en caracter
         */
        public char getEstatusPago() {return Estatus;}

        /**
         * Metodo para obtener el numero de referencia del pago
         * @return Numero de referencia
         */
        public String getNumeroReferencia() {return Referencia;}

        /**
         * Metodo para obtener la descripcion del estatus del pago
         * @return Descripcion del estatus del pago
         */
        public String getDescripcionEstatus() {return DescripcionEstatus;}

        /**
         * Metodo para obtener la fecha del pago
         * @return Fecha de pago
         */
        public GregorianCalendar getFechaPagado() {return FechaPagado;}

        /**
         * Metodo para obtener la fecha de cancelacion del pago
         * @return Fecha de cancelacion
         */
        public GregorianCalendar getFechaCancelacion() {return FechaCancelacion;}

        /**
         * Metodo para obtener la fecha de vencimiento del pago
         * @return Fecha de vencimiento del pago
         */
        public GregorianCalendar getFechaVencimiento() {return FechaVencimiento;}

        /**
         * Metodo para obtener la fecha de modificacion del pago
         * @return Fecha de modificacion del pago
         */
        public GregorianCalendar getFechaModificacion() {return FechaModificacion;}

        /**
         * Metodo para obtener la clave de la region
         * @return Clave de la region
         */
        public String getClaveRegion() {return ClaveRegion;}

        /**
         * Metodo para obtener la clave de la plaza
         * @return Clave de la plaza
         */
        public String getClavePlaza() {return ClavePlaza;}

        /**
         * Metodo para obtener la fecha en que vencio el pago
         * @return Fecha en que vencio el pago
         */
        public GregorianCalendar getFechaVencio() {return FechaVencio;}

	/**
         * Metodo para establecer la cuenta de cargo
         * @param String Cuenta de cargo
         */
	public void setCuentaCargo(String cc) {CuentaCargo = cc;}

	/**
	 * Metodo para establecer el numero de pago
	 * @param String Numero de pago
	 */
	public void setNoPago(String p) {
		NoPago = p;
	}

		/**
		 *Metodo para establecer la clave del beneficiario no registrado
		 *@param String ClavenoReg para la clave del no registrado
		*/
		public void setClavenoReg(String cnr){ ClavenoReg = cnr;}

        /**
         * Metodo para establecer la clave del beneficiario
         * @param String Clave del beneficiario
         */
	public void setClaveBen(String cb) {ClaveBen = cb;}

        /**
         * Metodo para establecer el nombre del beneficiario
         * @param String Nombre del beneficiario
         */
	public void setNomBen(String nb) {NomBen = nb;}

        /**
         * Metodo para establecer el importe del pago
         * @param double Importe del pago
         */
	public void setImporte(double i) {Importe = i;}

        /**
         * Metodo para establecer el importe si se obtiene de un string
         * @param importe Importe del pago
         */
        public void setImporte(String importe) {
            try {
                Importe = Double.parseDouble(importe);
            } catch (Exception ex) {
                Importe = -1;
            }
        }

	/**
	 * Metodo para establecer el importe del pago sin punto decimal
	 * @param importe Importe sin punto decimal
	*/
	public void setImporteSin (String importe) {
		if(importe.length() <= 3)
			importe = "00"+importe;
	    try {
		Importe = Double.parseDouble (
		    importe.substring (0, importe.length () - 2));
 		Importe += Double.parseDouble (
		    importe.substring (importe.length () - 2)) / 100;
	    } catch (Exception ex) {
		Importe = -1;
	    }
	}

        /**
         * Metodo para establecer la fecha de liberacion del pago
         * @param GregorianCalendar Fecha de liberacion del pago
         */
	public void setFechaLib(GregorianCalendar f) {FechaLib = f;}

        /**
         * Metodo para establecer la fecha de liberacion del pago
         * @param fecha String con la fecha
         */
        public void setFechaLib (String fecha) {FechaLib = getFechaString (fecha);}

        /**
         * Metodo para establecer la fecha limite del pago
         * @param GregorianCalendar Fecha limite de pago
         */
	public void setFechaPago(GregorianCalendar f) {FechaPago = f;}

        /**
         * Metodo para establecer la fecha limite del pago
         * @param fecha Fecha limite de pago
         */
	public void setFechaPago(String fecha) {FechaPago = getFechaString (fecha);}

        /**
         * Metodo para establecer el concepto del pago
         * @param String Concepto del pago
         */
	public void setConcepto(String c) {Concepto = c;}

        /**
         * Metodo para establecer la forma de pago
         * @param char Forma de pago
         */
	public void setFormaPago(char f) {FormaPago = f;}

        /**
         * Metodo para establecer la clave de la sucursal
         * @param String Clave de la sucursal
         */
	public void setClaveSucursal(String s) {ClaveSucursal = s;}

		/**
		 * Metodo para establecer Todas las sucursales
		 * @param char Todas Sucursales
		 */
	public void setTSucursales(char s) {TSucursales = s;}

        /**
         * Metodo para establecer el estatus del pago
         * @param char Estatus del pago
         */
        public void setEstatusPago(char c) {Estatus = c;}

        /**
         * Metodo para establecer el numero de referencia del pago
         * @param String Numero de referencia
         */
        public void setNumeroReferencia(String s) {Referencia = s;}

        /**
         * Metodo para establecer la descripcion del estatus del pago
         * @param String Descripcion
         */
        public void setDescripcionEstatus(String s) {DescripcionEstatus = s;}

        /**
         * Metodo para establecer la fecha de cancelacion del pago
         * @param GregorianCalendar Fecha de cancelacion del pago
         */
        public void setFechaCancelacion (GregorianCalendar d) {FechaCancelacion = d;}

        /**
         * Metodo para establecer la fecha de cancelacion del pago
         * @param fecha Fecha de cancelacion del pago
         */
        public void setFechaCancelacion (String fecha) {FechaCancelacion = getFechaString (fecha);}

        /**
         * Metodo para establecer la fecha en que se vencio el pago
         * @param GregorianCalendar Fecha de vencimiento del pago
         */
        public void setFechaVencimiento(GregorianCalendar d) {FechaVencimiento = d;}

        /**
         * Metodo para establecer la fecha en que se vencio el pago
         * @param fecha Fecha de vencimiento del pago
         */
        public void setFechaVencimiento(String fecha) {FechaVencimiento = getFechaString (fecha);}

        /**
         * Metodo para establecer la fecha de modificacion del pago
         * @param GregorianCalendar Fecha de modificacion del pago
         */
        public void setFechaModificacion(GregorianCalendar d) {FechaModificacion = d;}

        /**
         * Metodo para establecer la fecha de modificacion del pago
         * @param fecha Fecha de modificacion del pago
         */
        public void setFechaModificacion(String fecha) {FechaModificacion = getFechaString (fecha);}

        /**
         * Metodo para establecer la clave de la region
         * @param region Clave de region
         */
        public void setClaveRegion(String region) {ClaveRegion = region;}

        /**
         * Metodo para establecer la clave de la plaza
         * @param plaza Clave de la plaza
         */
        public void setClavePlaza(String plaza) {ClavePlaza = plaza;}

        /**
         * Metodo para establecer la fecha en que vencio el pago
         * @param Fecha Fecha en que vencio el pago
         */
        public void setFechaVencio(GregorianCalendar fv) {FechaVencio = fv;}

        /**
         * Metodo para establecer la fecha en que vencio el pago
         * @param Fecha Fecha en que vencio el pago
         */
        public void setFechaVencio(String fecha) {FechaVencio = getFechaString (fecha);}

        /**
         * Metodo para generar un GregorianCalendar a partir de un string
         * @param fecha Fecha a convertir a GregorianCalendar
         * @return GregorianCalendar con la fecha del String
         */
        public static GregorianCalendar getFechaString (String fecha) {
            try {
                StringTokenizer stFecha = new StringTokenizer (fecha, "/");
                int Dia = Integer.parseInt (stFecha.nextToken ());
                int Mes = Integer.parseInt (stFecha.nextToken ()) - 1;
                int Anio = Integer.parseInt (stFecha.nextToken ());
                return new GregorianCalendar (Anio, Mes, Dia);
            } catch (Exception ex) {
                return null;
            }
        }
}