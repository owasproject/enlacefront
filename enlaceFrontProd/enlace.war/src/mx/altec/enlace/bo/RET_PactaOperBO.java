package mx.altec.enlace.bo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.reuters.adt.jvalidationapi.ConnectionException;
import com.reuters.adt.jvalidationapi.ValidateUser;
import com.reuters.adt.jvalidationapi.ValidationConstants;
import com.reuters.adt.jvalidationapi.ValidationReturnData;
import com.santander.sbp.security.client.SbpSingleSignOnClient;
import com.santander.sbp.security.client.sso.LegalEntityUserIdentifier;
import com.santander.sbp.security.client.sso.SingleSignOnClientFactory;
import com.santander.sbp.security.common.config.SecurityConfiguration;
import com.santander.sbp.security.common.exception.SSLInitializationException;
import com.santander.sbp.security.server.rest.model.TokenResponse;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.RET_PactaOperDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FlameListener;
import mx.altec.enlace.utilerias.Global;

/**
 *  Clase Business para realizar el pactado de operaciones con la herramienta Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 02, 2012
 * @author TCS - Ricardo Mart韓ez
 * @version 1.1 Octubre 10, 2016
 */

public class RET_PactaOperBO extends BaseServlet implements ValidationConstants{

	/**
	 * Constante FLAME_SESSION_STATUS
	 */
	private static final String FLAME_SESSION_STATUS = "flameSessionStatus";

	/**
	 * Constante FLAME_CLOSE_SESSION
	 */
	private static final String FLAME_CLOSE_SESSION = "closeFlameSession";

	/**
	 * Constante ACCION
	 */
	private static final String ACCION = "accion";

	/**
	 * Constante FLAME_USER_IDENTIFIER
	 */
	 private static final String FLAME_USER_IDENTIFIER = "flameUserIdentifier";

	/**
	 * Constante S_GUID
	 */
	 private static final String S_GUID = "guid";

	/**
	 * Constante S_SESSION
	 */
	 private static final String S_SESSION = "session";

	/**
	 * Constante RET_PACTAOPERBO_C
	 */
	 private static final String RET_PACTAOPERBO_C  = "RET_PactaOperBO::iniciar::";

	/**
	 * Constante CONTRATO_RET
	 */
	 private static final String CONTRATO_RET = "contratoRET";

	/**
	 * Constante NEW_MENU
	 */
	 private static final String NEW_MENU = "newMenu";

	/**
	 * Constante MENU_PRINCIPAL
	 */
	 private static final String MENU_PRINCIPAL = "MenuPrincipal";

	/**
	 * Constante S_ENCABEZADO
	 */
	 private static final String S_ENCABEZADO = "Encabezado";

	 /**
	 * Serial Version ID
	 */
	 private static final long serialVersionUID = 1L;

	/**
	 * ERROR_9001
	 */
	private static final int ERROR_9001 = 9001;

	/**
	 * ERROR_9002
	 */
	private static final int ERROR_9002 = 9002;

	/**
	 * ERROR_9003
	 */
	private static final int ERROR_9003 = 9003;

	/**
	 * ERROR_9999
	 */
	private static final int ERROR_9999 = 9999;

	/**
	 * FLAME Single Sign On Security Service
	 */
	private static SbpSingleSignOnClient singleSignOnClient = null;
	/**
	 * FLAME Security Config
	 */
	private static SecurityConfiguration securityConfig = null;

	/**
	 * FLAME Listener para el estatus de la conexion
	 */
	private static transient final FlameListener FLAME_LISTENER = new FlameListener();;


	/********************************************/
	/** Metodo: Constructor. Inicializa los objetos Single Sign On y Security Config de Flame
	 */
	/********************************************/

	public RET_PactaOperBO() {
		try {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame:: Creando Objetos Single Sign On y Security Config para Flame.", EIGlobal.NivelLog.INFO);
			securityConfig = new SecurityConfiguration();
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame::REST " + Global.FLAME_REST_SECURITY_CONFIG, EIGlobal.NivelLog.DEBUG);
			securityConfig.setRestURLs(Arrays.asList(Global.FLAME_REST_SECURITY_CONFIG.split(",")));
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame::WS " + Global.FLAME_WS_SECURITY_CONFIG, EIGlobal.NivelLog.DEBUG);
			securityConfig.setWsURLs(Arrays.asList(Global.FLAME_WS_SECURITY_CONFIG.split(",")));
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame::KEYSTORE " + Global.FLAME_KEYSTORE_PATH, EIGlobal.NivelLog.DEBUG);
			securityConfig.setKeyStoreLocation(Global.FLAME_KEYSTORE_PATH);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame::PASS " + Global.FLAME_STORE_PASS, EIGlobal.NivelLog.DEBUG);
			securityConfig.setKeyStorePassword(Global.FLAME_STORE_PASS);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame::SSL " + Global.FLAME_SSL_ENABLED, EIGlobal.NivelLog.DEBUG);
			securityConfig.setSSLEnabled(Boolean.parseBoolean(Global.FLAME_SSL_ENABLED));
			singleSignOnClient = SingleSignOnClientFactory.create(securityConfig);
		    singleSignOnClient.addConnectionListener(FLAME_LISTENER);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame:: Objetos Single Sign On y Security Config para Flame inicializados correctamente.", EIGlobal.NivelLog.INFO);
		} catch (URISyntaxException e) {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame:: Problemas al iniciar Single Sign On Flame->", EIGlobal.NivelLog.ERROR,e);
		} catch (SSLInitializationException e) {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::flame:: Problemas al iniciar Single Sign On Flame->", EIGlobal.NivelLog.ERROR,e);
		}
	}

	/**
	 * Metodo para manejar el negocio de la acci贸n iniciar para realizar el pactado
	 * de operaciones con la herramienta Reuters
	 *
	 * @param req					request de la operaci贸n
	 * @param res					response de la operaci贸n
	 * @throws ServletException		Dispara ServletException
	 * @throws IOException			Dispara IOException
	 */
	public void iniciar( HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: Entrando a iniciar.", EIGlobal.NivelLog.DEBUG);
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute(S_SESSION);
			ServicioTux tuxGlobal = new ServicioTux();
			RET_PactaOperDAO dao = new RET_PactaOperDAO();
			Hashtable hs = null;
			Integer referencia = 0 ;
			ValidationReturnData rd = null;
			String strHost = null;
			String codError = null;
			int iPort = 0;
			String strResource = null;
			String strPwdEncMethod = null;
			boolean errorSingleSingOn = false;
			int codigoErr = 9000;

			try{
				if(!esHorarioValido()){
					paginaError("<b>FUERA DE HORARIO PARA OPERAR.</b>", req, res);
					return;
				}

				try {
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: Llamado al servicio SREFERENCIA",	EIGlobal.NivelLog.DEBUG);
					hs = tuxGlobal.sreferencia("901");;
					codError = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: Codigo de Error llamado->"+ codError, EIGlobal.NivelLog.DEBUG);
					referencia = (Integer) hs.get("REFERENCIA");
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: FOLIO ENLA->"+ referencia, EIGlobal.NivelLog.DEBUG);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: Problemas al obtener la FOLIO ENLA->"+e.getMessage(), EIGlobal.NivelLog.INFO);
					codError = null;
					referencia = 0;
				}

				sess.removeAttribute(S_GUID);
				rd = new ValidationReturnData();
				int iRc = 0;
				strHost = Global.STR_HOST_FX_ONLINE;
				iPort = Integer.parseInt(Global.I_PORT_FX_ONLINE);
				strResource = Global.STR_RESOURCE_FX_ONLINE;
				strPwdEncMethod = Global.STR_PWD_ENC_METHOD_FX_ONLINE;
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: LOS DATOS SON:.", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: strHost->" + strHost, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: iPort->" + iPort, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: strResource->" + strResource, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: strPwdEncMethod->" + strPwdEncMethod, EIGlobal.NivelLog.DEBUG);

				iRc = ValidateUser.validateEID(rd, strHost, iPort, strResource, session.getUserID8() + session.getUserID8(), true);
				//iRc = ValidateUser.validateEIDWithAuthentication(rd, strHost, iPort, strResource, session.getUserID8(), session.getUserID8(), strPwdEncMethod, strExtID, true);

				switch (iRc){
					case LOGIN_OK:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",guid=" + rd.getGuid() + ", type=", EIGlobal.NivelLog.INFO);
						break;
					case LOGIN_BAD:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Bad Login", EIGlobal.NivelLog.INFO);
						codigoErr += LOGIN_BAD;
						errorSingleSingOn = true;
						break;
					case BAD_PARAMETER:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Bad Login", EIGlobal.NivelLog.INFO);
						codigoErr += BAD_PARAMETER;
						errorSingleSingOn = true;
						break;
					case EID_LOOKUP_FAIL:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",External ID lookup fail", EIGlobal.NivelLog.INFO);
						codigoErr += EID_LOOKUP_FAIL;
						errorSingleSingOn = true;
						break;
					case PASSWORD_EXPIRED:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Password Expired", EIGlobal.NivelLog.INFO);
						codigoErr += PASSWORD_EXPIRED;
						errorSingleSingOn = true;
						break;
					case CONNECTION_ERROR:
						codigoErr += CONNECTION_ERROR;
						errorSingleSingOn = true;
						break;
					case RESOURCE_ERROR:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Connection Error", EIGlobal.NivelLog.INFO);
						codigoErr += RESOURCE_ERROR;
						errorSingleSingOn = true;
						break;
					case INVALID_PASSWORD:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Invalid password", EIGlobal.NivelLog.INFO);
						codigoErr += INVALID_PASSWORD;
						errorSingleSingOn = true;
						break;
					case ACCOUNT_LOCKED:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Account is locked", EIGlobal.NivelLog.INFO);
						codigoErr += ACCOUNT_LOCKED;
						errorSingleSingOn = true;
						break;
					default:
						EIGlobal.mensajePorTrace(RET_PACTAOPERBO_C + iRc + ",Unexpected result", EIGlobal.NivelLog.INFO);
						codigoErr += 999;
						errorSingleSingOn = true;
				}
				if(errorSingleSingOn){
					escribeBitacoraOper(req,
							BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA + codigoErr,
							BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA, referencia);
					paginaError(  "Estimado cliente le informamos que la contrataci&oacute;n del servicio <br>"
                   				+ "se encuentra en proceso, le pedimos por favor se ponga en contacto con <br>"
                   				+ "nuestro centro de atenci&oacute;n telef&oacute;nica: <br>"
                   				+ "En el D.F. al 51694343 del interior de la rep&uacute;blica al 018005095000.<br>"
                   				+ "Nuestros asesores especialistas con gusto le ayudar&aacute;n.", req, res);
					return;
				}else{
					if(dao.guardaContratoRET(session.getContractNumber(), session.getUserID8())){
						EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: Se actualizo el contrato en RET correctamente.", EIGlobal.NivelLog.INFO);
					}else{
						EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar:: NO se actualizo el contrato en RET.", EIGlobal.NivelLog.INFO);
					}
				}
			}
			catch (ConnectionException e)
			{
				escribeBitacoraOper(req,
						BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA + "9999",
						BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA, referencia);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciar::Connection Error: " + e.getMessage(), EIGlobal.NivelLog.INFO);
			}

			escribeBitacoraOper(req,
					BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA + "0000",
					BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA, referencia);
			sess.setAttribute(S_GUID, rd.getGuid());
	        req.setAttribute(S_GUID, rd.getGuid());
			sess.setAttribute(CONTRATO_RET, session.getContractNumber());
	        req.setAttribute(CONTRATO_RET, session.getContractNumber());
			req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
			req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
			req.setAttribute(S_ENCABEZADO, CreaEncabezado("Pactado de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Pactado de Operaciones","s26080h", req));
		}

	/**
	 * Metodo para manejar el negocio de la acci贸n iniciar para realizar el pactado
	 * de operaciones con la herramienta Flame
	 *
	 * @param req					request de la operaci贸n
	 * @param res					response de la operaci贸n
	 * @throws ServletException		Dispara ServletException
	 * @throws IOException			Dispara IOException
	 */
	public void iniciarFlame( HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Entrando a iniciarFlame.", EIGlobal.NivelLog.DEBUG);
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute(S_SESSION);
			ServicioTux tuxGlobal = new ServicioTux();
			RET_PactaOperDAO dao = new RET_PactaOperDAO();
			Hashtable hs = null;
			Integer referencia = 0 ;
			String codError = null;
			boolean errorSingleSingOn = false;
			int codigoErr = 9000;

			TokenResponse tokenResponse=null;

			//try{
				if(!esHorarioValido()){
					paginaError("<b>FUERA DE HORARIO PARA OPERAR.</b>", req, res);
					return;
				}

				try {
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Llamado al servicio SREFERENCIA",	EIGlobal.NivelLog.DEBUG);
					hs = tuxGlobal.sreferencia("901");;
					codError = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Codigo de Error llamado->"+ codError, EIGlobal.NivelLog.DEBUG);
					referencia = (Integer) hs.get("REFERENCIA");
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: FOLIO ENLA->"+ referencia, EIGlobal.NivelLog.DEBUG);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Problemas al obtener la FOLIO ENLA->"+e.getMessage(), EIGlobal.NivelLog.INFO);
					codError = null;
					referencia = 0;
				}

			sess.removeAttribute(S_GUID);

				//Se verifica el estatus de la conexion con el Security Service de Flame
			if (FLAME_LISTENER.isConnAlive()) {
					/*Se implementa la llamada al single sign on de FLAME*/
					//Parametros del canal
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame::PARAMETROS:::::: Usuario<"+session.getUserID8()+"> Contraro: <"+session.getContractNumber()+">", EIGlobal.NivelLog.DEBUG);
					//Se crea una LegalEntityUserIdentifier para identificar al usuario
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: 1. Se crea LegalEntityUserIdentifier Flame: " + Global.FLAME_LEGAL_ENTITY, EIGlobal.NivelLog.DEBUG);
					LegalEntityUserIdentifier userIdentifier = new LegalEntityUserIdentifier(session.getUserID8(), Global.FLAME_LEGAL_ENTITY);
					//Se solicita la generacion de token pasando numero de contrato
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: 2. Solicitando generacion de token Flame", EIGlobal.NivelLog.DEBUG);
					tokenResponse = singleSignOnClient.generateToken(userIdentifier, session.getContractNumber());

					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: 3. Resultado Single Sign On "+tokenResponse.getResult(), EIGlobal.NivelLog.DEBUG);
					if (tokenResponse.getResult()==null){
						EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Error", EIGlobal.NivelLog.INFO);
						codigoErr = ERROR_9999;
						errorSingleSingOn = true;
					} else {
						switch (tokenResponse.getResult()){
							case OK:
								req.getSession().setAttribute(FLAME_USER_IDENTIFIER, userIdentifier);
								EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: OK,token=" + tokenResponse.getToken(), EIGlobal.NivelLog.INFO);
								break;
							case INVALID_USER:
								EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: INVALID_USER, Usuario no valido", EIGlobal.NivelLog.INFO);
							codigoErr = ERROR_9001;
								errorSingleSingOn = true;
								break;
							case INVALID_ACCOUNT:
								EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: INVALID_ACCOUNT, Contrato no valido", EIGlobal.NivelLog.INFO);
							codigoErr = ERROR_9002;
								errorSingleSingOn = true;
								break;
							case CONNECTION_ERROR:
								EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: CONNECTION_ERROR", EIGlobal.NivelLog.INFO);
							codigoErr = ERROR_9003;
							errorSingleSingOn = true;
							break;
						default:
							EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Error", EIGlobal.NivelLog.INFO);
							codigoErr = ERROR_9999;
							errorSingleSingOn = true;
						}
					} 
				} else {
					EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: CONNECTION_ERROR", EIGlobal.NivelLog.INFO);
					codigoErr = ERROR_9003;
					errorSingleSingOn = true;
				}
				if(errorSingleSingOn){
					escribeBitacoraOper(req,
							BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA + codigoErr,
							BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA, referencia);
					switch (codigoErr){
						case ERROR_9001:
							paginaError("Estimado cliente, FX Online es nuestra plataforma para compra venta de divisas en tiempo real. "
									+ "Si quiere tener acceso o dar seguimiento a su solicitud, puede llamar a Superl&iacute;nea "
									+ "al tel&eacute;fono 018009114411, donde con gusto le podemos apoyar.", req, res);
							break;
						case ERROR_9002:
							paginaError("Estimado cliente, el contrato con el que desea acceder a nuestra plataforma FX Online no se encuentra habilitado. "
									+ "Puede llamar a Superl&iacute;nea al tel&eacute;fono 018009114411, donde con gusto le podemos apoyar.", req, res);
							break;
						case ERROR_9003:
							paginaError("Estimado cliente, el sistema est&aacute; moment&aacute;neamente fuera de servicio. Por favor, int&eacute;ntelo m&aacute;s tarde.", req, res);
							break;
						default:
							paginaError("Estimado cliente, el sistema est&aacute; moment&aacute;neamente fuera de servicio. Por favor, int&eacute;ntelo m&aacute;s tarde.", req, res);
					}
					return;
				}else{
				if(dao.guardaContratoRET(session.getContractNumber(), session.getUserID8())){
						EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: Se actualizo el contrato en RET correctamente.", EIGlobal.NivelLog.INFO);
				}else{
						EIGlobal.mensajePorTrace("RET_PactaOperBO::iniciarFlame:: NO se actualizo el contrato en RET.", EIGlobal.NivelLog.INFO);
				}
			}

			escribeBitacoraOper(req,
					BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA + "0000",
					BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA, referencia);
	        req.setAttribute(S_GUID, tokenResponse.getToken());
			req.setAttribute("FLAME_URL", mx.altec.enlace.utilerias.Global.FLAME_URL);
			req.setAttribute("EIGlobal", new mx.altec.enlace.utilerias.EIGlobal(null));
			sess.setAttribute(CONTRATO_RET, session.getContractNumber());
	        req.setAttribute(CONTRATO_RET, session.getContractNumber());
			req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
			req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
			req.setAttribute(S_ENCABEZADO, CreaEncabezado("Pactado de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Pactado de Operaciones","s26080h", req));
		}

	/**
	 * Metodo para manejar el negocio de la acci贸n regresar para realizar el pactado
	 * de operaciones con la herramienta Reuters
	 *
	 * @param req					request de la operaci贸n
	 * @param res					response de la operaci贸n
	 * @throws ServletException		Dispara ServletException
	 * @throws IOException			Dispara IOException
	 */
	public void regresar( HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::processRequest:: Entrando a regresar.", EIGlobal.NivelLog.INFO);
			// Inicia codificaci贸n Admin API para cerrar session del Applet.
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute(S_SESSION);
			req.setAttribute("direccion", "ConSaldos");
			req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
			req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
			req.setAttribute(S_ENCABEZADO, CreaEncabezado("Pactado de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Pactado de Operaciones","s26080h", req));
		}

	/**
	 * Muestra una pagina de error con la descripcion indicada
	 * @param mensaje			Mensaje de error presentado en pantalla
	 * @param request			request de la operaci贸n
	 * @param response			response de la operaci贸n
	 *
	 */
	public void paginaError(String mensaje,HttpServletRequest request, HttpServletResponse response){
		Exception ex = null;
		try{
			despliegaPaginaErrorURL(mensaje,
				"Pactado de Operaciones",
				"Transferencias FX Online &gt; ",
				"Pactado de Operaciones",
				request,
				response);
			} catch (IOException e) {
				ex=e;
			} catch (ServletException e) {
				ex=e;
			}
			if (ex!=null){
			StackTraceElement[] lineaError;
				lineaError = ex.getStackTrace();
				EIGlobal.mensajePorTrace("RET_PactaOperBO::paginaError:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + ex.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de desplegar la pagina de error
	 * por medio de una URL
	 *
	 * @param Error			Error a desplegar
	 * @param param1		Parametro del encabezado
	 * @param param2		Parametro del encabezado
	 * @param param3 		Parametro del encabezado
	 * @param request		Petici贸n de la aplicaci贸n
	 *
	 */
	private void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(S_SESSION);
		RequestDispatcher view = null;

		EIGlobal.mensajePorTrace("RET_PactaOperBO::despliegaPaginaErrorURL:: Entrando.", EIGlobal.NivelLog.DEBUG);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		String param4 = "s55205h";

		request.setAttribute( "FechaHoy", fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );
		request.setAttribute( "URL", "csaldo1?prog=0");

		request.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute( MENU_PRINCIPAL, session.getStrMenu() );
		request.setAttribute( S_ENCABEZADO, CreaEncabezado( param1, param2 + param3, param4, request ) );

		//evalTemplate("/jsp/EI_Mensaje.jsp", request, response);
		view = request.getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		view.forward(request,response);
	}

	/**
	 * Metodo para definir la fecha para el mensaje de error.
	 *
	 * @param strFecha			formato de la fecha
	 * @return Fecha Formato
	 */
	private String fechaHoy(String strFecha) {
		Date Hoy = new Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);

		String dd = "";
		String mm = "";
		String aa = "";
		String aaaa = "";
		String dt = "";
		String mt = "";
		String th = "";
		String tm = "";
		String ts = "";

		int indth = 0;
		int indtm = 0;
		int indts = 0;
		int inddd = 0;
		int indmm = 0;
		int indaaaa = 0;
		int indaa = 0;
		int indmt = 0;
		int inddt = 0;

		String[] dias = {
			"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
			"Sabado"
		};
		String[] meses = {
			"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
			"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
		};

		if (Cal.get(Calendar.DATE) <= 9){
			dd += "0" + Cal.get(Calendar.DATE);
		}else{
			dd += Cal.get(Calendar.DATE);
		}
		if (Cal.get(Calendar.MONTH) + 1 <= 9){
			mm += "0" + (Cal.get(Calendar.MONTH) + 1);
		}else{
			mm += (Cal.get(Calendar.MONTH) + 1);
		}
		if (Cal.get(Calendar.HOUR_OF_DAY) < 10){
			th += "0" + Cal.get(Calendar.HOUR_OF_DAY);
		}else{
			th += Cal.get(Calendar.HOUR_OF_DAY);
		}
		if (Cal.get(Calendar.MINUTE) < 10){
			tm += "0" + Cal.get(Calendar.MINUTE);
		}else{
			tm += Cal.get(Calendar.MINUTE);
		}
		if (Cal.get(Calendar.SECOND) < 10){
			ts += "0" + Cal.get(Calendar.SECOND);
		}else{
			ts += Cal.get(Calendar.SECOND);
		}
		aaaa += Cal.get(Calendar.YEAR);
		aa += aaaa.substring(aaaa.length() - 2, aaaa.length());
		dt += dias[Cal.get(Calendar.DAY_OF_WEEK) - 1];
		mt += meses[Cal.get(Calendar.MONTH)];

		while (
			indth >= 0 || indtm >= 0 || indts >= 0 || inddd >= 0
			|| indmm >= 0 || indaaaa >= 0 || indaa >= 0 || indmt >= 0
			|| inddt >= 0
			) {
			indth = strFecha.indexOf("th");
			if (indth >= 0)
				strFecha = strFecha.substring(0, indth) + th + strFecha.substring(indth + 2, strFecha.length());
			indtm = strFecha.indexOf("tm");
			if (indtm >= 0)
				strFecha = strFecha.substring(0, indtm) + tm + strFecha.substring(indtm + 2, strFecha.length());
			indts = strFecha.indexOf("ts");
			if (indts >= 0)
				strFecha = strFecha.substring(0, indts) + ts + strFecha.substring(indts + 2, strFecha.length());
			inddd = strFecha.indexOf("dd");
			if (inddd >= 0)
				strFecha = strFecha.substring(0, inddd) + dd + strFecha.substring(inddd + 2, strFecha.length());
			indmm = strFecha.indexOf("mm");
			if (indmm >= 0)
				strFecha = strFecha.substring(0, indmm) + mm + strFecha.substring(indmm + 2, strFecha.length());
			indaaaa = strFecha.indexOf("aaaa");
			if (indaaaa >= 0)
				strFecha = strFecha.substring(0, indaaaa) + aaaa + strFecha.substring(indaaaa + 4, strFecha.length());
			indaa = strFecha.indexOf("aa");
			if (indaa >= 0)
				strFecha = strFecha.substring(0, indaa) + aa + strFecha.substring(indaa + 2, strFecha.length());
			indmt = strFecha.indexOf("mt");
			if (indmt >= 0)
				strFecha = strFecha.substring(0, indmt) + mt + strFecha.substring(indmt + 2, strFecha.length());
			inddt = strFecha.indexOf("dt");
			if (inddt >= 0)
				strFecha = strFecha.substring(0, inddt) + dt + strFecha.substring(inddt + 2, strFecha.length());
		}
		return strFecha;
	}

	/**
	 * Metodo especial para bitacorizar el Pactado de Operaciones Reuters dentro de
	 * la bitacora de Operaciones
	 *
	 * @param req				Request de la acci贸n
	 * @param codError			C贸digo de error
	 * @param cveBitacora		Clave de Bitacora
	 * @param referencia		Numero de referencia
	 *
	 */
	private void escribeBitacoraOper(HttpServletRequest req, String codError,
			String cveBitacora, Integer referencia){
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(S_SESSION);

		EIGlobal.mensajePorTrace("RET_PactaOperBO::escribeBitacoraOper:: Entrando ...", EIGlobal.NivelLog.DEBUG);
		if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				if(getFormParameter(req,BitaConstants.FLUJO) != null){
					bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
				}else{
					bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				}
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				BitaTCTBean beanTCT = new BitaTCTBean ();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setTipoOperacion(cveBitacora);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
                        && ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
                                    .equals(BitaConstants.VALIDA)) {
                        bt.setIdToken(session.getToken().getSerialNumber());
                }

				bt.setIdErr(codError);
				beanTCT.setCodError(codError);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(cveBitacora);

				bt.setIdFlujo(cveBitacora);
				bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "01");

				BitaHandler.getInstance().insertBitaTransac(bt);
			    	BitaHandler.getInstance().insertBitaTCT(beanTCT);
				EIGlobal.mensajePorTrace("RET_PactaOperBO::escribeBitacoraOper:: bitacorizando " + cveBitacora, EIGlobal.NivelLog.DEBUG);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace("RET_PactaOperBO::escribeBitacoraOper:: Exception " + e.toString(), EIGlobal.NivelLog.DEBUG);
			}
		}
	}

	/**
	 * Metodo encargado de validar que el horario sea valido
	 *
	 * @return	-	true Horario Valido
	 * 			-	false Horario Invalido
	 */
	private boolean esHorarioValido(){
		Date dateHoy = new Date();
		int h = 0;
		int m = 0;
		try{
			h = Integer.parseInt(Global.HORA_FX_ONLINE);
			m = Integer.parseInt(Global.MINUTOS_FX_ONLINE);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Hora parametrizada:" + h, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Minuto parametrizado:" + m, EIGlobal.NivelLog.DEBUG);
			if(dateHoy.getHours()<h){
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Valido", EIGlobal.NivelLog.DEBUG);
				return true;
			}else if(dateHoy.getHours()==h && dateHoy.getMinutes()<=m){
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Valido", EIGlobal.NivelLog.DEBUG);
				return true;
			}else{
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Invalido", EIGlobal.NivelLog.DEBUG);
				return false;
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Problemas al validar el horario->"+e.getMessage(),
					EIGlobal.NivelLog.DEBUG);
		}
		return false;
	}

	/**
	 * Metodo que valida si el usuario es cliente FLAME
	 * @param numUsuario numero de cliente enlace
	 * @return clienteFlame respuesta cliente
	 */
	public boolean isFlameClient(String numUsuario){
		EIGlobal.mensajePorTrace("RET_PactaOperBO::isFlameClient::Validacion de cliente Enlace", EIGlobal.NivelLog.DEBUG);
		RET_PactaOperDAO dao = new RET_PactaOperDAO();
		boolean clienteFlame=false;
		clienteFlame=dao.isFlameClient(numUsuario);
		EIGlobal.mensajePorTrace("RET_PactaOperBO::isFlameClient::El cliente es flame: "+clienteFlame, EIGlobal.NivelLog.DEBUG);
		return clienteFlame;
	}

	/**
	 * Realiza la operacion indicada en el <code>request</code> para Flame
	 *
	 * @param request Request de la peticion
	 * @param response Response de la peticion
	 * @throws ServletException		Dispara ServletException
	 * @throws IOException			Dispara IOException
	 */
	public void doFlameTasks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String action = request.getParameter(ACCION);
		if (FLAME_SESSION_STATUS.equals(action)) {
			getFlameSessionStatus(request, response);
		} else if (FLAME_CLOSE_SESSION.equals(action)) {
			ejectFlameSession(request);
		}
	}

	/**
	 * Escribe en el <code>response</code> el estatus de la sesion FLAME
	 *
	 * @param request Request de la peticion
	 * @param response Response de la peticion
	 * @throws IOException Dispara IOException
	 */
	private void getFlameSessionStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		LegalEntityUserIdentifier userIdentifier = (LegalEntityUserIdentifier) request.getSession()
				.getAttribute("flameUserIdentifier");
		response.setContentType("application/json");
		if (userIdentifier != null) {
			boolean sessionStatus = singleSignOnClient.userHasSession(userIdentifier);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: Consultando estatus de sesion Flame.", EIGlobal.NivelLog.DEBUG);
			response.getWriter().write("{\"flameSessionAlive\":" + sessionStatus + "}");
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: Estatus de sesion Flame: " + sessionStatus, EIGlobal.NivelLog.DEBUG);
		} else {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: Consultando estatus de sesion Flame.", EIGlobal.NivelLog.DEBUG);
			response.getWriter().write("{\"flameSessionAlive\":false}");
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: No hay sesion Flame activa.", EIGlobal.NivelLog.DEBUG);
		}
	}


	/**
	 * Solicita a Flame cerrar la sesion
	 *
	 * @param request Request de la peticion
	 * @throws IOException Dispara IOException
	 */
	private void ejectFlameSession(HttpServletRequest request) throws IOException {
		LegalEntityUserIdentifier userIdentifier = (LegalEntityUserIdentifier) request.getSession()
				.getAttribute(FLAME_USER_IDENTIFIER);
		if (userIdentifier != null) {
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: Cerrando Sesion Flame...", EIGlobal.NivelLog.DEBUG);
			boolean status = singleSignOnClient.ejectUser(userIdentifier);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::estatusFlame:: Sesion Flame cerrada('"+status+"').", EIGlobal.NivelLog.DEBUG);
		}
	}


}
