package mx.altec.enlace.bo;

import java.io.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class MTI_Importar
 {
   public String cuentaCargo   = "";
   public String titular       = "";
   public String cuentaAbono   = "";
   String divisa        = "";
   String banco         = "";
   String pais          = "";
   String beneficiario  = "";
   String ciudad        = "";
   String claveABA      = "";
   String claveEspecial = "";
   String concepto      = "";
   String comproFiscal  = "";
   String RFC           = "";

   double importeIVA = 0;
   double importe = 0;

   int err = 0;

  public void formateaLinea(String linea)
   {
     try
      {
        linea=linea.trim();

        if(linea.length()>=280 && linea.length()<=292)
         {
		   EIGlobal.mensajePorTrace("MTI_Importar - MTI_Importar(): Separando campos.", EIGlobal.NivelLog.INFO);

           cuentaCargo  = linea.substring(0,20).trim();
           cuentaAbono  = linea.substring(20,40).trim();
           divisa       = linea.substring(40,44).trim();
           banco        = linea.substring(44,84).trim();
		   pais         = linea.substring(84,88).trim();
		   beneficiario = linea.substring(88,138).trim();
		   ciudad       = linea.substring(138,178).trim();
		   // importe ...
		   claveABA     = linea.substring(200,212).trim();
		   claveEspecial= linea.substring(212,224).trim();
		   concepto     = linea.substring(224,268).trim();
		   comproFiscal = linea.substring(268,269).trim();
		   RFC          = linea.substring(269,282).trim();
		   // importeIVA ...

		   String idimp = formateaImp(linea.substring(178,200).trim());
		   String idiva = formateaImp(linea.substring(282,290).trim());

           importe    = new Double(idimp).doubleValue();
		   importeIVA = new Double(idiva).doubleValue();

           err = 0;
         }
		else
		 err = linea.length()+1;
       } catch(Exception e)
         {
		   EIGlobal.mensajePorTrace("MTI_Importar - MTI_Importar(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
           err = linea.length()+1;
         }
   }

   boolean verificaPais(String[][] arrayPaises)
	{
      for(int indice=1;indice<=Integer.parseInt(arrayPaises[0][0]);indice++)
       {
         if(arrayPaises[indice][1].trim().equals(pais))
           return true;
       }
      return false;
    }

/*Modificacion para integracion
boolean verificaCuentaCargo(String[][] arrayCuentas)
    {
      for(int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
       {
         if(arrayCuentas[indice][1].trim().equals(cuentaCargo))
           {
             titular=arrayCuentas[indice][4];
             return true;
           }
       }
      titular="CUENTA NO TIENE";
      return false;
    }

   boolean verificaCuentaAbono(String[][] arrayCuentas,int total)
    {
      for(int id=0;id<total;id++)
       {
         if(arrayCuentas[id][0].trim().equals(cuentaAbono))
           return true;
       }
      return false;
    }
*/
   boolean verificaImporte()
    {
      if(importe<=0.0)
        return false;
      return true;
    }

   boolean verificaImporteIVA()
    {
      if(importeIVA<=0.0)
        return false;
      return true;
    }

   String formateaImp(String imp)
	{
	   StringBuffer formateado=new StringBuffer("");

	   if(imp.length()<4)
		 imp="0000"+imp;

	   formateado.append(imp.substring(0,imp.length()-2));
	   formateado.append(".");
	   formateado.append(imp.substring(imp.length()-2,imp.length()));
	   return formateado.toString();
	}
}
