// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.*;
import java.util.Vector;
import java.util.StringTokenizer;

/**<code><B><I>InterpretaPC46</I></B></code> es una clase que interpreta la trama de <BR>
 * consulta de l&iacute;neas de cr&eacute;dito <B>PC46</B>.  En caso de que la trama de <BR>
 * respuesta sea satisfatoria lee el archivo de cuentas y va construyendo una tabla<BR>
 * de HTML con los datos de la cuenta.
 * <P>
 * Fecha creaci&oacute;n: 25/04/2002 <BR>
 * Modificaciones: 26/04/2002: Se terminaron los
 * m&eacute;todos <I>InterpetaTrama</I> y los m&eacute;todos para regresar los
 * valores de los atributos.<BR>
 * 03/05/2002 Cambios en <I>leeArchivoRespuesta</I><BR>
 * 22/05/2002 Se agregaron m&eacute;todos para el manejar el color.<BR>
 * 27/05/2002 Se cambi&oacute; <I>interpretaTrama</I><BR>
 * 28/05/2002 Se cambi&oacute; <I>descomponeLinea</I><BR>
 * 30/05/2002 Se cambi&oacute; <I>interpretaTrama</I><BR>
 * 05/06/2002 Se modific&oacute; la alineaci&oacute;n de <I>cuenta</I> y <I>descripci&oacute;n</I>.<BR>
 * 14/06/2002 Se asigna un salto de carro para la <I>descripci&oacute;n</I> si el dato proporcionado es nulo o es una cadena vac�a.<BR>
 * 24/06/2002 En <I>leeArchivoRespuesta</I> asigna una salto de carro a la descripci�n si contiene s&oacute;lo blancos.<BR>
 * 09/06/2002 Verificaci&oacute;n supresi&oacute;n caracteres blancos en <I>descripci&oacute;n</I><BR>
 * 17/09/2002 Correcci�n sustituci&oacute;n campo vac&iacute;o por descripci&oacute;n.<BR>
 * 18/09/2002 Cambio condici&oacute;n para color de rengl&oacute;n.<BR>
 * 08/10/2002 Se cambi&oacute; <I>interpretaTrama</I> debido a que var&iacute;a el n&uacute;mero de espacios en la trama de respuesta de tuxedo.<BR>
 * 09/10/2002 Se agreg&oacute; el m&eacute;todo <I>conversionFechaJapones</I><BR>
 * 14/10/2002 Se convierte la fecha al formato DD/MM/YYYY en <B>todosLosDatos</B>,  el cual se utiliza para generar el archivo.<BR>
 * 25/10/2002 Se agreg&oacute; que indique los errores al leer el archivo.<BR>
 * 08/11/2002 Se da formato a las cantidades monetarias.<BR>
 * 28/11/2002 Se corrigi&oacute; que una variable sea reinicializada.<BR>
 * 03/12/2002 Se asegura que se cierre el archivo de l&iacute;neas en <B><I>leeArchivoRespuesta</I></B>.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.7.0
 */
public class InterpretaPC46 implements Serializable
{

   /**Versi&oacute;n
   */
   public static final String version="1.7.0";

/**Dos primeros caracteres de la trama de respuesta correcta.
*/
public static final String OK="OK";

/**C&oacute;digo de error que responde cuando la trama es correcta.
*/
public static final String ceros="0000";

/**Caracter "pipe".
*/
public static final String pipe="|";

/**Arreglo de enteros que indica las posiciones de cada parte de la trama de respuesta.
*/
public static final int[] posicionesFijas={2, 8, 14, 18};


/**Arreglo de enteros para analizar el c&oacute;digo de error y obtener el nombre del archivo de respuesta de tuxedo (CEDC).
*/
public static final int[] posicionesArchivo={4, 8};


/**Indica color claro.
*/
public static final int CLARO=0;

/**Indica que el rengl&oacute;n es obscuro.
*/
public static final int OBSCURO=1;

/**Formato de columna de fondo claro con alineaci&oacute;n a la derecha.
*/
public static String fondoClaro="<TD align=\"right\" class=\"textabdatcla\">";

/**Formato de columna de fondo obscuro con alineaci&oacute;n a la derecha.
*/
public static String fondoObscuro="<TD align=\"right\" class=\"textabdatobs\">";

/**Formato de columna de fondo claro con alineaci&oacute;n a la izquierda.
*/
public static String fondoClaroIzquierdo="<TD align=\"left\" class=\"textabdatcla\">";

/**Formato de columna de fondo obscuro con alineaci&oacute;n a la derecha.
*/
public static String fondoObscuroIzquierdo="<TD align=\"left\" class=\"textabdatobs\">";

/**Columna de cuatro posiciones sin datos con fondo claro.
*/
public static String fondoClaroSinDatos="<TD align=\"right\" class=\"textabdatcla\" colspan=\"4\">";

/**Columna de cuatro posiciones sin datos con fondo obscuro.
*/
public static String fondoObscuroSinDatos="<TD align=\"right\" class=\"textabdatobs\" colspan=\"4\">";

/**Rengl&oacute;n claro.
*/
public static String renglonClaro="<TR bgcolor=\"#EBEBEB\">";

/**Rengl&oacute;n obscuro
*/
public static String renglonObscuro="<TR bgcolor=\"#CCCCCC\">";

/**Salto de carro.
*/
public static String BR="<BR>";


/**Indica el lugar en la trama de respuesta (a partir de la posici&oacute;n cero) ocupado<BR>
* por caracteres <I>blancos</I>.
*/
public static final int posicionDatoBlanco=2;


/**Indica la posici&oacute;n de la fecha en el rengl&oacute;n le&iacute;do.
*/
private static final int posicionDatoFecha=3;


/**Indica las posiciones de los datos correspondientes a cantidades monetarias.
*/
private static final int[] posicionesMonetarias={4, 5, 6};


/**Nombre y ruta del archivo generado por tuxedo que contiene las l&iacute;neas de cr&eacute;dito.
*/
private String nombreArchivo="";

/**Folio de la trama de respuesta PC46
*/
private String folio=null;

/**Cuatro d&iacute;gitos con el c&oacute;digo generado de la respuesta de tuxedo.
*/
private String codigoError=null;

/**Descripci&oacute;n del error.
*/
private String error;

/**Guarda las l&iacute;neas le&iacute;das.
*/
private String lineas="";

/**
*/
private String renglones="";

/**Descripci&oacute;n de la cuenta.
*/
private String descripcion="";

/**Contiene todas las l&iacute;neas del archivo le&iacute;das.
*/
private Vector datosLineas=new Vector();

/**Guarda todos los renglones HTML con los datos de las l&iacute;neas de cr&eacute;dito.
*/
private String renglonesTabla=new String("");

/**Contiene todos los datos le&iacute;dos para generar el archivo de exportaci&oacute;n
*/
private String todosLosDatos=new String("");

/**Separador de campos.
*/
private String separador;

/**N&uacute;mero de columnas vac&iacute;as cuando una cuenta no tiene l&iacute;neas de cr&eacute;dito asociadas.
*/
private int datosVacios=4;

/**Contiene la descripci&oacute;n de los errores de la trama de respuesta.
*/
private String erroresTrama;

/**Clave del color que va a usar para renglones.
*/
private int color;

/**N&uacute;mero asignado para indicar la alternancia de colores.
*/
private int contadorInterno=0;

/**N&uacute;mero total de renglones le&iacute;dos.
*/
private int renglonesLeidos=0;

/**
*/
private String trama="";


/**Indica las posiciones del formato <I>japon&eacute;s</I>.
*/
private int[] posicionFecha={4, 6, 8, 10};


/**Separador de fecha de formato DD/MM/YYYY
*/
private String diagonal="/";


/**Contiene los errores que surgieron al leer el archivo.
*/
public String erroresArchivo="";

  /**
   * Constructor de la clase.
   */
  public InterpretaPC46()
  {
    separador=pipe;
    error=new String("");
    erroresTrama=new String();
    color=this.OBSCURO;
  } // Fin contructor


  /**<code><B><I>interpretaTrama</I></B></code> Procesa la trama de respuesta del servicio de tuxedo<BR>
  de l&iacute;neas de cr&eacute;dito <B>PC46</B>.<BR>
   * <P><B>Bien:</B> <I>OK 198668 CEES0000/tmp/1001851</I><BR>
   *                    OK 198404 CEES0000/tmp/1001851<BR>
   * <B>Mal:</B> <I>OK 198712 CEES9999|No hay registros</I>
   * </P>
   * <P>Regresa verdadero si la trama es una trama correcta y trae un nombre de archivo;
   * falso si la trama es de error, no trae nombre de archivo o no se ha asignado trama.
   * </P>
  */
  public boolean interpretaTrama()
  {
    String aux;
    erroresTrama=new String("");
    folio=null;
    codigoError=null;
    StringTokenizer st;

    if(trama==null || trama.length()<1)
    {
      erroresTrama+="No se recibi� trama. ";
      return false;
    } // Fin if nulo

    //trama=trama.trim();
    st=new StringTokenizer(trama, " ");

    if(st.countTokens()<3)
    {
      erroresTrama+="N�mero de elementos: "+st.countTokens();
      return false;
    } // Fin if numero elementos


    try
    {
      //aux=trama.substring(0, posicionesFijas[0]);
      aux=st.nextToken();

      if(!aux.equalsIgnoreCase(OK))
      {
        erroresTrama+="Respuesta de error: "+aux+" ";
        return false;
      } // Fin if OK

      //aux=trama.substring(posicionesFijas[0]+1, posicionesFijas[1]+1);
      aux=st.nextToken();


      if(aux!=null && aux.length()>0)
      {
        folio=aux;
      } //Fin if nulo
      else
      {
        erroresTrama+="No hay folio en la trama. ";
      } // Fin if

      //aux=trama.substring(posicionesFijas[2], posicionesFijas[3]);
      aux=st.nextToken();

      if(aux!=null && aux.length()>0)
      {
        //codigoError=aux;
        codigoError=aux.substring(posicionesArchivo[0], posicionesArchivo[1]);
      }
      else
      {
        erroresTrama+="No hay c�digo de error. ";
        return false;
      } // Fin nulo codigoError

      if(codigoError.equalsIgnoreCase(ceros))
      {
        //nombreArchivo=trama.substring(posicionesFijas[3]);
        nombreArchivo=aux.substring(posicionesArchivo[1]);
        //return true;
      }
      else
      {
        //error=trama.substring(posicionesFijas[3]+1, trama.length()-1);
        //OK      201656 CEES9999|No hay registros
        //if(trama.length()>=posicionesFijas[3])
        if(aux.length()>posicionesArchivo[1])
        {
          //error=trama.substring(posicionesFijas[3]+1);
          error=aux.substring(posicionesArchivo[1]+1);

          while(st.hasMoreTokens())
          {
            error+=" "+st.nextToken();
          } // Fin while
        }
        else
        {
            error="";
        } // Fin if-else

        erroresTrama+=" El codigo de error es distinto de ceros ";
        return false;
      } // Fin if codigoError

    }
    catch(ArrayIndexOutOfBoundsException aioob)
    {
      erroresTrama+="Error interpretando la trama: "+aioob.getMessage()+" ";
      return false;
    }


    return true;
  } // Fin m�todo interpretaTrama


   /**<code><B><I>leeArchivoRespuesta</I></B><code> recibe el nombre del archivo, lo lee l&iacute;nea por<BR>
   * l&iacute;nea, separando los datos y construyendo los renglones de la tabla HTML con los datos obtenidos.
   * <P>Regresa verdadero si ley&oacute; el archivo sin problemas; falso si <I>nombreArchivo</I> es nulo, es de<BR>
   * longitud cero, no encontr&oacute; al archivo o tuvo problemas al leerlo.
   * </P>
   * @param nombreArchivo01 Es el nombre con la ruta absoluta al mismo.
   */
  public boolean leeArchivoRespuesta(String nombreArchivo01)
  {
    File respuesta;
    BufferedReader bf=null;
    String linea;
    String datosLinea[];
    renglonesLeidos=0;
    String descripcionTabla=null;
    erroresArchivo="";
    FormatosMoneda escribeMoneda;
    todosLosDatos=new String("");
    boolean resultadoArchivo=false;



    if(nombreArchivo01==null || nombreArchivo01.length()<1)
    {
      erroresArchivo="No se dio nombre de archivo.";
      return false;
    }
    else
    {
      nombreArchivo=nombreArchivo01;
    } // Fin if-else

    if(descripcion.trim().length()>0)
    {
      descripcionTabla=descripcion.trim();
    }
    else
    {
      descripcionTabla=BR;
    } // Fin if-else

    respuesta=new File(nombreArchivo01);

    if(!respuesta.isFile())
    {
      erroresArchivo+="El archivo no existe: "+nombreArchivo01+" .  Ruta: "+respuesta.getAbsolutePath();
      return false;
    }

    if(respuesta.length()<1)
    {
      erroresArchivo+=" La longitud del archivo es menor a uno: "+respuesta.getName();
      return false;
    }

    try
    {
      bf=new BufferedReader(new FileReader(nombreArchivo01));
      escribeMoneda=new FormatosMoneda();

      while(bf.ready())
      {
        linea=bf.readLine();

        if(linea!=null && linea.length()>1)
        {
          lineas+=linea+"@";
          datosLinea=descomponeLinea(linea);
          renglonesLeidos++;

          /*
          todosLosDatos+=datosLinea[0];
          todosLosDatos+=separador;
          todosLosDatos+=datosLinea[1];
          todosLosDatos+=separador;
          todosLosDatos+=getDescripcion();
          todosLosDatos+=separador;
          todosLosDatos+=datosLines[2];
          */


          //if(datosLineas.size()%2==0)
          if(getContadorInterno()%2!=0)
          {
            //renglonesTabla+="<TR bgcolor=\"#CCCCCC\">";
            renglonesTabla+=renglonObscuro;
            //renglonesTabla+="<TD width=\"50\"><BR></TD>";
            renglonesTabla+=fondoObscuro+"<Input type=\"radio\" name=\"datosCuenta\" value=\""+datosLinea[0]+separador+datosLinea[1]+separador+descripcion+"\">";
            renglonesTabla+="</TD>";
            renglonesTabla+=fondoObscuroIzquierdo+datosLinea[0]+"</TD>";
            renglonesTabla+=fondoObscuro+datosLinea[1]+"</TD>";
            renglonesTabla+=fondoObscuroIzquierdo+descripcionTabla+"</TD>";
            renglonesTabla+=fondoObscuro+conversionFechaJapones(datosLinea[posicionDatoFecha])+"</TD>";
            renglonesTabla+=fondoObscuro+escribeMoneda.daMonedaConComas(datosLinea[4], true)+"</TD>";
            renglonesTabla+=fondoObscuro+escribeMoneda.daMonedaConComas(datosLinea[5], true)+"</TD>";
            renglonesTabla+=fondoObscuro+escribeMoneda.daMonedaConComas(datosLinea[6], true)+"</TD>";
            renglonesTabla+="</TR>";
          }
          else
          {
            //renglonesTabla+="<TR bgcolor=\"#EBEBEB\">";
            renglonesTabla+=renglonClaro;
            //renglonesTabla+="<TD width=\"50\"><BR></TD>";
            renglonesTabla+=fondoClaro+"<Input type=\"radio\" name=\"datosCuenta\" value=\""+datosLinea[0]+separador+datosLinea[1]+separador+descripcion+"\">";
            renglonesTabla+="</TD>";
            renglonesTabla+=fondoClaroIzquierdo+datosLinea[0]+"</TD>";
            renglonesTabla+=fondoClaro+datosLinea[1]+"</TD>";
            renglonesTabla+=fondoClaroIzquierdo+descripcionTabla+"</TD>";
            renglonesTabla+=fondoClaro+conversionFechaJapones(datosLinea[posicionDatoFecha])+"</TD>";
            renglonesTabla+=fondoClaro+escribeMoneda.daMonedaConComas(datosLinea[4], true)+"</TD>";
            renglonesTabla+=fondoClaro+escribeMoneda.daMonedaConComas(datosLinea[5], true)+"</TD>";
            renglonesTabla+=fondoClaro+escribeMoneda.daMonedaConComas(datosLinea[6], true)+"</TD>";
            renglonesTabla+="</TR>";
          } // Fin if

          datosLineas.addElement(linea);
          setContadorInterno(getContadorInterno()+1);

        }
      } // Fin while

      resultadoArchivo=true;

    }
    catch(IOException iox)
    {
      erroresArchivo+="Error al leer el archivo: "+nombreArchivo01+" "+iox.getMessage();
      //return false;
      resultadoArchivo=false;
    }
    finally
    {
      if(bf!=null)
      {
         try
         {
            bf.close();
         }
         catch(IOException iox)
         {
            erroresArchivo+="Error al cerrar el archivo: "+nombreArchivo01;
         } // Fin try-catch cerrar
      } // Fin if bf
    } // Fin try-catch-finally




    //return true;
    return resultadoArchivo;
  } // Fin leeArchivoRespuesta


   /**Da un rengl&oacute;n HTML con s&oacute;los la cuenta y la descripi&oacute;n.
   * Se usa para desplegar la cuenta que no tiene l&iacute;neas de cr&eacute;dito asociadas.
   * <P>Regresa una cadena con el rengl&oacute;n HTML con las columnas en el mismo orden<BR>
   * de la pantalla principal, pero no tiene bot&oacute;n de tipo "radio" para seleccionar esta<BR>
   * cuenta, ya que no tiene l&iacute;nea; las dem&aacute;s columnas aparecen vac&iacute;as.
   * </P>
   * @param contador Indica el n&uacute;mero de registro a desplegar, con el fin de que en la pantalla<BR>
   * aparezca en un colora alternado respecto al anterior y al siguiente.
   * @param cuenta Es el n&uacute;mero de la cuenta de cheques.
   * @param descripcion Es la descripci&oacute;n de la cuenta.
   */
  public String daRenglonSinLinea(int contador, String cuenta, String descripcion)
  {
    String nuevoRenglon=null;
    String descripcionTabla=null;
    todosLosDatos=new String("");

    if(descripcion.trim().length()>0)
    {
      descripcionTabla=descripcion.trim();
    }
    else
    {
      descripcionTabla=BR;
    } // Fin if-else descripcion

    if(contador%2==0)
    {
      nuevoRenglon=renglonClaro+InterpretaPC46.fondoClaro+"<BR></TD>"+InterpretaPC46.fondoClaroIzquierdo+cuenta+"</TD>"+InterpretaPC46.fondoClaroIzquierdo+"<BR></TD>"+InterpretaPC46.fondoClaroIzquierdo+descripcionTabla+"</TD>"+InterpretaPC46.fondoClaroSinDatos+"<BR></TD></TR>";
    }
    else
    {
      nuevoRenglon=renglonObscuro+InterpretaPC46.fondoObscuro+"<BR></TD>"+InterpretaPC46.fondoObscuroIzquierdo+cuenta+"</TD>"+InterpretaPC46.fondoObscuro+"<BR></TD>"+InterpretaPC46.fondoObscuroIzquierdo+descripcionTabla+"</TD>"+InterpretaPC46.fondoObscuroSinDatos+"<BR></TD></TR>";
    } // Fin if

    todosLosDatos+=cuenta;
    todosLosDatos+=getSeparador();
    todosLosDatos+=getSeparador();
    todosLosDatos+=descripcion;

    for(int j=0;j<datosVacios;j++)
    {
      todosLosDatos+=getSeparador();
    } // Fin for

    return nuevoRenglon;
  } // Fin daRenglonSinLinea


   /**<code><B><I>descomponeLinea</I></B></code> recibe una l&iacute;nea del archivo generado por tuxedo,<BR>
   * y lo divide.
   * <P>Regresa un arreglo de cadenas, cada una de ellas con un dato de la l&iacute;nea le&iacute;da.
   * </P>
   * @param datosTrama Es un rengl&oacute;n del archivo generado como resultado del servicio de tuxedo PC46.
   */
  public String[] descomponeLinea(String datosTrama)
  {
    int numeroElementos;
    String datos[]=null;

    FormatosMoneda fm=new FormatosMoneda();
    StringTokenizer st=new StringTokenizer(datosTrama, pipe);
    numeroElementos=st.countTokens();
    datos=new String[numeroElementos];
    //todosLosDatos=new String("");

    if(todosLosDatos.length()>0)
      todosLosDatos+="\n";

    for(int i=0;i<numeroElementos;i++)
    {

     if(i==posicionDatoBlanco)
     {
      st.nextToken();
      datos[posicionDatoBlanco]=getDescripcion().trim();
      todosLosDatos+=getDescripcion().trim();
      todosLosDatos+=getSeparador();
      continue;
     } // Fin if dato blanco

      datos[i]=(st.nextToken()).trim();

      if(datos[i]!=null && datos[i].length()>0)
      {
         if(i!=posicionDatoFecha)
         {
            if(i==posicionesMonetarias[0] || i==posicionesMonetarias[1] || i==posicionesMonetarias[2])
            {
               todosLosDatos+=fm.daMonedaConComas(datos[i], false);
            }
            else
            {
               todosLosDatos+=datos[i];
            }
         }
         else
         {
            todosLosDatos+=conversionFechaJapones(datos[posicionDatoFecha]);
         } // Fin if fecha
         todosLosDatos+=getSeparador();
      } // Fin if datos[i]

      /**
      if(i==1)
      {
        todosLosDatos+=getDescripcion().trim();
        todosLosDatos+=getSeparador();
      } // Fin if
      */
    } // Fin for


    return datos;
  } // Fin descomponeLinea

   /**<B><I><code>conversionFechaJapones</code></I></B><BR>
   * Convierte una fecha en formato japon&eacute;s (YYYY-MM-DD) y la devuelve como cadena sin importar<BR>
   * la validez de la misma en el formato DD/MM/YYYY.
   * @param fechaJapones Cadena de diez posiciones en el formato YYYY-MM-DD que se considera una fecha v&aacute;lida.<BR>
   * <P>
   * Regresa una cadena en el formato DD/MM/YYYY.
   * </P>
   */
   public String conversionFechaJapones(String fechaJapones)
   {
      String fechaResultado="";


      if(fechaJapones==null || fechaJapones.length()<10)
         return fechaResultado;

      fechaResultado=fechaJapones.substring(posicionFecha[2], posicionFecha[3]);
      fechaResultado+=diagonal+fechaJapones.substring(posicionFecha[0]+1, posicionFecha[1]+1);
      fechaResultado+=diagonal+fechaJapones.substring(0, posicionFecha[0]);

      return fechaResultado;
   } // Fin conversionFechaJapones

  public String getNombreArchivo()
  {
    return nombreArchivo;
  }


  public String getFolio()
  {
    return folio;
  }

  public String getCodigoError()
  {
    return codigoError;
  }


  public String getError()
  {
    return error;
  }

  public String getLineas()
  {
    return lineas;
  }


  public Vector getRenglones()
  {
    return datosLineas;
  }


   /**<code><B><I>setDescripcion</I></B><code> asigna la descripci&oacute;n de la cuenta; si &eacute;sta no<BR>
   * tiene descripci&oacute;n se asigna un salto de carro para la celda de la tabla.
   * @param descripcion Es la descripci&oacute;n de la cuenta.
   */
  public void setDescripcion(String descripcion)
  {
    if(descripcion!=null && descripcion.trim().length()>0)
    {
      this.descripcion=descripcion.trim();
    } // Fin if
  } // Fin setDescripcion

  public String getDescripcion()
  {
    return descripcion;
  }

  public String getRenglonesTabla()
  {
    return renglonesTabla;
  }

  public Vector getDatosLineas()
  {
    return datosLineas;
  }

  public String getTodosLosDatos()
  {
    return todosLosDatos;
  }

  public String getSeparador()
  {
    return separador;
  }

  public void setSeparador(String separador)
  {
    if(separador!=null)
      this.separador=separador;
  }

  public String getErroresTrama()
  {
    return erroresTrama;
  } //


   /**Crea todos los renglones de una tabla HTML con datos fijos para simulaci&oacute;n.
   * <P>Regresa una cadena con los renglones.
   * </P>
   */
  public String daRenglonesSimulados()
  {
    String datos="";

     datos+="<TR bgcolor=\"#EBEBEB\"><TD align=\"right\" class=\"textabdatcla\"><Input type=\"radio\" name=\"datosCuenta\" value=\"123|12444|fdfggasdf\"></TD><TD align=\"right\" class=\"textabdatcla\">49000000654</TD><TD align=\"right\" class=\"textabdatcla\">123</TD><TD align=\"right\" class=\"textabdatcla\">  GRUPO BANCA ELECTRONICA</TD><TD align=\"right\" class=\"textabdatcla\">30/05/2002</TD><TD align=\"right\" class=\"textabdatcla\">1000</TD><TD align=\"right\" class=\"textabdatcla\">1000</TD><TD align=\"right\" class=\"textabdatcla\">500</TD></TR>";
     datos+="<TR bgcolor=\"#CCCCCC\"><TD align=\"right\" class=\"textabdatobs\"><Input type=\"radio\" name=\"datosCuenta\" value=\"1dsf23|121321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatobs\">50000000068</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD><TD align=\"right\" class=\"textabdatobs\">  LORIA GRACIA ADOLFO</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#EBEBEB\"><TD align=\"right\" class=\"textabdatcla\"><Input type=\"radio\" name=\"datosCuenta\" value=\"454|121321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatcla\">51500315411</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD><TD align=\"right\" class=\"textabdatcla\">  GRUPO BANCA ELECTRONICA</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#CCCCCC\"><TD align=\"right\" class=\"textabdatobs\"><Input type=\"radio\" name=\"datosCuenta\" value=\"111|144321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatobs\">60501058867</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD><TD align=\"right\" class=\"textabdatobs\">  JIMENEZ RAMIREZ CARMEN EULALIA</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#EBEBEB\"><TD align=\"right\" class=\"textabdatcla\"><Input type=\"radio\" name=\"datosCuenta\" value=\"1015|121321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatcla\">57003439833</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD><TD align=\"right\" class=\"textabdatcla\">  CARMEN E JIMENEZ RAMIREZ</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#CCCCCC\"><TD align=\"right\" class=\"textabdatobs\"><Input type=\"radio\" name=\"datosCuenta\" value=\"13|121321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatobs\">65500097821</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD><TD align=\"right\" class=\"textabdatobs\">  B E DIVISION NOROESTE</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#EBEBEB\"><TD align=\"right\" class=\"textabdatcla\"><Input type=\"radio\" name=\"datosCuenta\" value=\"188|121321|fgasdf\"></TD><TD align=\"right\" class=\"textabdatcla\">51500206978</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD><TD align=\"right\" class=\"textabdatcla\">  B E DIVISION NOROESTE</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#CCCCCC\"><TD align=\"right\" class=\"textabdatobs\"><Input type=\"radio\" name=\"datosCuenta\" value=\"188|78787|fgasdf\"></TD><TD align=\"right\" class=\"textabdatobs\">50000010081</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD><TD align=\"right\" class=\"textabdatobs\">  BICOR DISENO CIENTIFICO SA DE CV</TD><TD align=\"right\" class=\"textabdatobs\"><BR></TD></TR>";
     datos+="<TR bgcolor=\"#EBEBEB\"><TD align=\"right\" class=\"textabdatcla\"><Input type=\"radio\" name=\"datosCuenta\" value=\"188|1111|fgasdf\"></TD><TD align=\"right\" class=\"textabdatcla\">82500065800</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD><TD align=\"right\" class=\"textabdatcla\">  DINSA ELECTRICAS Y CYMI SA DE CV</TD><TD align=\"right\" class=\"textabdatcla\"><BR></TD></TR>";

    return datos;
  } // Fin daRenglonesSimulados;

   /**Asigna el color al rengl�n
   */
  public void setColor(int valor)
  {
    if(valor==CLARO)
    {
      color=CLARO;
    }
    else
    {
      color=OBSCURO;
    } // Fin if-else

  } // Fin setColor


  public int getColor()
  {
    return color;
  } // Fin color

  public int getContadorInterno()
  {
    return contadorInterno;
  }

  public void setContadorInterno(int numerito)
  {
    if(numerito>=0)
      contadorInterno=numerito;
  }

  public int getRenglonesLeidos()
  {
      return renglonesLeidos;
  }


   public String getErroresArchivo()
   {
      return erroresArchivo;
   }



  public String getTrama()
  {
   return trama;
  }

  public void setTrama(String trama)
  {
   if(trama!=null && trama.trim().length()>0)
      this.trama=trama.trim();
  } // Fin setTrama

} // Fin clase InterpretaPC46

