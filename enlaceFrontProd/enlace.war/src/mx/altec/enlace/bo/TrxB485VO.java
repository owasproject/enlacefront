package mx.altec.enlace.bo;

import java.io.Serializable;

/**
 *  Clase bean para manejar el procedimiento para la transaccion B485
 *
 * @author Fernando Gurrola Herrera
 * @version 1.0 Sep 27, 2010
 */
public class TrxB485VO implements Serializable {
	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Variable cuenta
	 */
	private String cuenta = null;

	/**
	 * Variable divisa
	 */
	private String divisa = null;

	/**
	 * Variable clabe
	 */
	private String clabe = null;

	/**
	 * Obtener el valor de cuenta.
     *
     * @return cuenta valor asignado.
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
     * Asignar un valor a cuenta.
     *
     * @param cuenta Valor a asignar.
     */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * Obtener el valor de divisa.
     *
     * @return divisa valor asignado.
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
     * Asignar un valor a divisa.
     *
     * @param divisa Valor a asignar.
     */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * Obtener el valor de clabe.
     *
     * @return clabe valor asignado.
	 */
	public String getClabe() {
		return clabe;
	}

	/**
     * Asignar un valor a clabe.
     *
     * @param clabe Valor a asignar.
     */
	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	/**
	 * Override toString
	 */
	public String toString() {
		StringBuffer registro = new StringBuffer();
		registro.append("cuenta->" + getCuenta());
		registro.append("divisa->" + getDivisa());
		registro.append("clabe->" + getClabe());
		return registro.toString();
	}

}