/*
 * pdSucursalCompPorCve.java
 *
 * Created on 2 de mayo de 2002, 09:29 AM
 */

package mx.altec.enlace.bo;

/**
 *
 * @author  Rafael Martinez Montiel
 * @version 1.0
 */
public class pdSucursalCompPorCve implements java.io.Serializable, java.util.Comparator {

    /** Creates new pdSucursalCompPorCve */
    public pdSucursalCompPorCve() {
    }

    /** hace la comparacion de dos objetos de tipo pdSucursal
     * @param obj es el primer pdSucursal
     * @param obj1 es el segundo pdSucursal
     * @return -1 si obj es menor que obj1
     * 0 si los dos objetos son iguales
     * 1 si obj1 es menor que obj
     */
    public int compare(java.lang.Object obj, java.lang.Object obj1) {
        pdSucursal suc = (pdSucursal) obj;
        pdSucursal suc1 = (pdSucursal) obj1;
        int i = suc.getCveSucursal().compareTo(suc1.getCveSucursal());
        if(i != 0) return i;
        return suc.compareTo(suc1);
    }
}
