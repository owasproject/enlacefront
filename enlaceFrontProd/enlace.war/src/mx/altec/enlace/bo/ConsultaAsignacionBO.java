package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AsignacionBean;
import mx.altec.enlace.beans.ConsultaAsignacionTEResBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.ConsultaAsignacionDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;

public class ConsultaAsignacionBO extends BaseServlet {

	public ArrayList<ConsultaAsignacionTEResBean> consultaAsignacionTE(
			AsignacionBean consulta, HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		ConsultaAsignacionDAO dao = new ConsultaAsignacionDAO();
//***************************************************INICIA BITACORIZACIÓN***************************************************//
		EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Entrando ...", EIGlobal.NivelLog.INFO);
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);

				// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
				BitaTCTBean beanTCT = new BitaTCTBean ();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				//Datos Comunes
				bt.setNumBit(BitaConstants.ES_NOMINAN3_ASIGNACION_CONS_CONSULTA);
				bt.setIdErr(BitaConstants.ES_NOMINAN3_ASIGNACION_CONS + "0000");
				beanTCT.setCodError(BitaConstants.ES_NOMINAN3_ASIGNACION_CONS + "0000");

				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				ServicioTux tuxGlobal = new ServicioTux();
				Hashtable hs = null;
				Integer referencia = 0 ;
				String codError = null;
				hs = tuxGlobal.sreferencia("901");
				codError = hs.get("COD_ERROR").toString();
				referencia = (Integer) hs.get("REFERENCIA");
				if(codError.endsWith("0000") && referencia!=null){
					bt.setReferencia(referencia);
					beanTCT.setReferencia(referencia);
				}

				//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
				bt.setIdFlujo(BitaConstants.ES_NOMINAN3_ASIGNACION_CONS);
				bt.setEstatus("A");

				//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
				beanTCT.setTipoOperacion(BitaConstants.ES_NOMINAN3_ASIGNACION_CONS);

				//Inserto en Bitacoras
				BitaHandler.getInstance().insertBitaTransac(bt);
				BitaHandler.getInstance().insertBitaTCT(beanTCT);
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + BitaConstants.ES_NOMINAN3_ASIGNACION_CONS, EIGlobal.NivelLog.INFO);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
			}
		}
//*****************************************************FIN BITACORIZACIÓN***************************************************//
		return dao.consultaAsignacionTE(consulta);
	}

	/**
	 * Metodo bussiness para realizar la cancelación de un folio.
	 *
	 * @param numFolio
	 * @param numContrato
	 * @return
	 */
	public String cancela(String numFolio, String numContrato, HttpServletRequest req,
			HttpServletResponse res) {
		EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Entrando...", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		ConsultaAsignacionDAO dao = new ConsultaAsignacionDAO();
		String resultado = null;
		String estatusFolio = null;
		String cveBitacora = "INCF";

		try{
			EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Verificar si el folio se puede cancelar", EIGlobal.NivelLog.INFO);
			estatusFolio = dao.validaFolio(numFolio, numContrato);
			if(!estatusFolio.trim().equals("")){
				EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio existe", EIGlobal.NivelLog.INFO);
				if(estatusFolio.trim().equals("R")){
					EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio cumple con las validaciones", EIGlobal.NivelLog.INFO);
					if(dao.cancelaFolio(numFolio, numContrato)){
						EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El número de folio " + numFolio + " ha sido cancelado correctamente.", EIGlobal.NivelLog.INFO);
						resultado = "\"El número de folio " + numFolio + " ha sido cancelado correctamente.\",1";

						//***************************************************INICIA BITACORIZACIÓN***************************************************//
						EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Entrando ...", EIGlobal.NivelLog.INFO);
						if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try{
								BitaHelper bh = new BitaHelperImpl(req, session, sess);

								// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);

								// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
								BitaTCTBean beanTCT = new BitaTCTBean ();
								beanTCT = bh.llenarBeanTCT(beanTCT);

								//Datos Comunes
								bt.setNumBit(cveBitacora + "02");
								bt.setIdErr(cveBitacora + "0000");
								beanTCT.setCodError(cveBitacora + "0000");

								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
									beanTCT.setNumCuenta(session.getContractNumber().trim());
								}

								if (session.getUserID8() != null) {
									bt.setCodCliente(session.getUserID8().trim());
									beanTCT.setUsuario(session.getUserID8().trim());
									beanTCT.setOperador(session.getUserID8().trim());
								}

								ServicioTux tuxGlobal = new ServicioTux();
								Hashtable hs = null;
								Integer referencia = 0 ;
								String codError = null;
								hs = tuxGlobal.sreferencia("901");
								codError = hs.get("COD_ERROR").toString();
								referencia = (Integer) hs.get("REFERENCIA");
								if(codError.endsWith("0000") && referencia!=null){
									bt.setReferencia(referencia);
									beanTCT.setReferencia(referencia);
								}

								//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
								bt.setIdFlujo(cveBitacora);
								bt.setEstatus("A");

								//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
								beanTCT.setTipoOperacion(cveBitacora);

								//Inserto en Bitacoras
								BitaHandler.getInstance().insertBitaTransac(bt);
								BitaHandler.getInstance().insertBitaTCT(beanTCT);
								EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: bitacorizando " + cveBitacora, EIGlobal.NivelLog.INFO);

							} catch (Exception e) {
								EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
							}
						}
				//*****************************************************FIN BITACORIZACIÓN***************************************************//

					}else{
						EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Ocurrio un error al cancelar el número de folio " + numFolio, EIGlobal.NivelLog.INFO);
						resultado = "\"Ocurrio un error al cancelar el número de folio " + numFolio + "\"";
					}
				}else if(estatusFolio.trim().equals("D")){
					EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio " + numFolio + " ya fue cancelado.", EIGlobal.NivelLog.INFO);
					resultado = "\"El folio " + numFolio + " ya fue cancelado.\"";
				}else{
					EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio " + numFolio + " esta en estatus " + estatusFolio, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio " + numFolio + " no cumple con las condiciones para cancelar.", EIGlobal.NivelLog.INFO);
					resultado = "\"El folio " + numFolio + " no se puede cancelar, el archivo ya fue enviado para su proceso o ya fue procesado.\"";
				}
			}else{
				EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: El folio " + numFolio + " no existe.", EIGlobal.NivelLog.INFO);
				resultado = "\"El folio " + numFolio + " no existe.\"";
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Error->"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			resultado = "\"Ocurrio un error al cancelar el número de folio " + numFolio + "\"";
		}
		EIGlobal.mensajePorTrace("ConsultaAsignacionBO::cancela:: Saliendo...", EIGlobal.NivelLog.INFO);
		return resultado;
	}


}