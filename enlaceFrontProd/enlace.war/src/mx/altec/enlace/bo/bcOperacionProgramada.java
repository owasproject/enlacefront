/*
 * bcOperacionProgramada.java
 *
 * Created on 23 de abril de 2002, 04:02 PM
 */

package mx.altec.enlace.bo;

import java.util.*;
import java.text.ParseException;

/**
 * Contenedor para la una operacione programada de Bace Cero
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class bcOperacionProgramada implements java.io.Serializable, java.lang.Comparable {

    /** Holds value of property noCuenta. */
    private java.lang.String noCuenta;

    /** Holds value of property fechaIni.
     */
    private GregorianCalendar fechaIni;

    /** Holds value of property fechaFin.
     */
    private GregorianCalendar fechaFin;

    /** Holds value of property horaIni.
     */
    private GregorianCalendar horaIni;

    /** Holds value of property horaFin.
     */
    private GregorianCalendar horaFin;

    /** Holds value of property techoPresupuestal. */
    private float techoPresupuestal;

    /** Holds value of property usuario. */
    private java.lang.String usuario;

    /** Creates new bcOperacionProgramada */
    public bcOperacionProgramada() {
    }

    /** Getter for property noCuenta.
     * @return Value of property noCuenta.
     */
    public java.lang.String getNoCuenta() {
        return noCuenta;
    }

    /** Setter for property noCuenta.
     * @param noCuenta New value of property noCuenta.
     */
    public void setNoCuenta(java.lang.String noCuenta) {

//        System.err.println("noCuenta = " + noCuenta );
        this.noCuenta = noCuenta;
    }

    /** Getter for property fechaIni.
     * @return Value of property fechaIni.
     */
    public GregorianCalendar getFechaIni() {
        return fechaIni;
    }

    /** Setter for property fechaIni.
     * @param fechaIni New value of property fechaIni.
     */
    public void setFechaIni(GregorianCalendar fechaIni) {
        this.fechaIni = (GregorianCalendar) fechaIni.clone();
    }

    /** Getter for property fechaFin.
     * @return Value of property fechaFin.
     */
    public GregorianCalendar getFechaFin() {
        return fechaFin;
    }

    /** Setter for property fechaFin.
     * @param fechaFin New value of property fechaFin.
     */
    public void setFechaFin(GregorianCalendar fechaFin) {
        this.fechaFin = (GregorianCalendar) fechaFin.clone();
    }

    /** Getter for property horaIni.
     * @return Value of property horaIni.
     */
    public GregorianCalendar getHoraIni() {
        return horaIni;
    }

    /** Setter for property horaIni.
     * @param fechaIni New value of property horaIni.
     */
    public void setHoraIni(GregorianCalendar horaIni) {
        this.horaIni = (GregorianCalendar) horaIni.clone();
    }

    /** Getter for property horaFin.
     * @return Value of property horaFin.
     */
    public GregorianCalendar getHoraFin() {
        return horaFin;
    }

    /** Setter for property horaFin.
     * @param horaFin New value of property horaFin.
     */
    public void setHoraFin(GregorianCalendar horaFin) {
        this.horaFin = (GregorianCalendar) horaFin.clone();
    }

    /** Getter for property techoPresupuestal.
     * @return Value of property techoPresupuestal.
     */
    public float getTechoPresupuestal() {
        return techoPresupuestal;
    }

    /** Setter for property techoPresupuestal.
     * @param techoPresupuestal New value of property techoPresupuestal.
     */
    public void setTechoPresupuestal(float techoPresupuestal) {
        this.techoPresupuestal = techoPresupuestal;
    }

    /** Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }

    /** Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }

    /** <CODE>set</CODE>hace la lectura de una trama en el formato de respuesta de tuxedo
     * @param trama Es la trama de la cual se hara la lectura
     * @throws IOException si ocurre algun error de lectura
     * @throws ParseException si hay error en los datos de lectura
     */
    public void set(java.lang.String trama) throws java.io.IOException, ParseException {
        /**/
        mx.altec.enlace.bo.pdTokenizer tok = new mx.altec.enlace.bo.pdTokenizer(  trama,//trama que se va a tokenizar
                                                                "|",//delimitadores a utilizar
                                                                2);//utilizar los delimitadores como separadores
        if(tok.countTokens() < 7){
            throw new java.io.IOException("bcOperacionProgramada:set - tokens en trama incompletos...");
        }
        try{
            this.setNoCuenta( tok.nextToken() );
            this.setFechaIni( tok.nextToken() );
            this.setFechaFin( tok.nextToken() );
            this.setHoraIni( tok.nextToken() );
            this.setHoraFin( tok.nextToken() );
            this.setTechoPresupuestal( tok.nextToken() );
            this.setUsuario( tok.nextToken() );
        } catch (ParseException e){
            throw new java.text.ParseException("bcOperacionProgramada:set - " + e.getMessage(), e.getErrorOffset());
        } catch (Exception e){
            throw new java.text.ParseException("bcOperacionProgramada:set - " + e.getMessage(), 0 );
        }


    }


    /** setter para la fecha inicial
     * @param fechaIni fecha a utilizar para el atributo
     * @throws ParseException si hay error en el formato de los datos
     */
    public void setFechaIni(java.lang.String fechaIni) throws ParseException {
        try{
//            System.err.println("fechaIni = " + fechaIni );
            setFechaIni(this.parseDate(fechaIni));
        } catch (Exception e){
            throw new ParseException(e.getMessage(), 4);
        }
    }

    /** setter para le fecha final
     * @param fechaFin fecha a utlizar para el atributo
     * @throws ParseException En caso de error en los datos de lectura
     */
    public void setFechaFin(java.lang.String fechaFin) throws ParseException {
        try{
//            System.err.println("fechaFin = " + fechaFin );
            setFechaFin(this.parseDate(fechaFin));
        } catch (Exception e){
            throw new ParseException(e.getMessage(), 2);
        }
    }

    /** setter para la hora inicial
     * @param horaIni hora a utulizar para el atributo
     * @throws ParseException si hay error en los datos de lectura
     */
    public void setHoraIni(java.lang.String horaIni) throws ParseException {
        try{
//            System.err.println("horaIni = " + horaIni );
            setHoraIni(this.parseTime(horaIni));
        } catch (Exception e){
            throw new ParseException(e.getMessage(), 3);
        }
    }

    /** setter para la hora final
     * @param horaFin hora a utulizar para el atributo
     * @throws ParseException si hay error en los datos de lectura
     */
    public void setHoraFin(java.lang.String horaFin) throws ParseException {
        try{
//            System.err.println("horaFin = " + horaFin );
            setHoraFin(this.parseTime(horaFin));
        } catch (Exception e){
            throw new ParseException(e.getMessage(), 4);
        }
    }

    /** setter para el techo presupuestal
     * @param techoPresupuestal valor a utilizar como techo presupuestal
     * @throws ParseException si hay error en el formato de los datos...
     */
    public void setTechoPresupuestal(java.lang.String techoPresupuestal) throws ParseException {
        try{
            setTechoPresupuestal(Float.parseFloat(techoPresupuestal));
        } catch (NumberFormatException e){
            throw new ParseException("Error en el formato del Techo Presupuestal", 3);
        }
    }

    /** <CODE>parseDate</CODE>obiene un gregorian calendar a partir de un string que represente una fecha
     * @param fecha es la fecha de la cual se sacara el GregorianCalendar
     * @throws Exception si ocurre algun error
     * @return GregorianCalendar con la fecha indicada
     */
    protected GregorianCalendar parseDate(java.lang.String fecha) throws Exception {
        java.lang.StringBuffer s = new StringBuffer ("");
		s = new StringBuffer ("Error en el formato de la fecha");

        if(8 != fecha.length() && 10 != fecha.length() ){
            {s.append (" - Numero de caracteres ");}
            if( 8 > fecha.length() ) {s.append ("inferior al requerido");}
            else if(10 < fecha.length() ) {s.append ("superior al requerido");}
            throw new Exception(s + ", " + fecha);
        }

        if(8 == fecha.length()){//si esta en el formato ddmmaaaa
            GregorianCalendar cal = new GregorianCalendar();
            int c=0;
            try{
                cal.set(cal.DAY_OF_MONTH,Integer.parseInt(fecha.substring(0,2)));
                ++c;
                cal.set(cal.MONTH,Integer.parseInt(fecha.substring(2,4)));
                ++c;
                cal.set(cal.YEAR,Integer.parseInt(fecha.substring(4)));
                ++c;
            } catch (NumberFormatException e){
                s.append (" - error en el formato de ");
                if(0 == c) {s.append ("dia");}
                else if(c == 1) {s.append ("mes");}
                else  {s.append ("a�o");
                s.append (", deberia ser dd/mm/aaaa");}
                throw new Exception(s+ ", " + fecha);
            }
            return cal;
        }
        else if(10 == fecha.length()){//esta en el formato dd/mm/aaaa
            GregorianCalendar cal = new GregorianCalendar();
            java.util.StringTokenizer tok = new java.util.StringTokenizer(fecha,"/-");
            //new pdTokenizer(  fecha,//string a tokenizar
            //                                    "/-",//delimitadores a utilizar
            //                                    2);//utilizar los delimitadores como separadores
//            System.err.println("countTokens = " + tok.countTokens() );
            if(3 != tok.countTokens()){
                throw new Exception(s + " - deberia ser dd/mm/aaaa"+ ", " + fecha);
            }
            int c=0;
            try{
                cal.set(cal.DAY_OF_MONTH,Integer.parseInt(tok.nextToken()));
                ++c;
                cal.set(cal.MONTH,Integer.parseInt(tok.nextToken())-1);
                ++c;
                cal.set(cal.YEAR,Integer.parseInt(tok.nextToken()));
                ++c;
            } catch (NumberFormatException e){
                s.append (" - error en el formato de ");
                if(0 == c) {s.append ("dia");}
                else if(c == 1) {s.append ("mes");}
                else  {s.append ("a�o");
                s.append (", deberia ser dd/mm/aaaa");}
                throw new Exception(s+ ", " + fecha);
            }
            return cal;
        }
        throw new Exception("bcOperacionProgramada:parseDate - Unknown status"+ ", " + fecha);
    }

     /** <code>parseTime</code>obtiene un GregorianCalendar a partir de un string que represente un tiempo
      * @param time string del cual se generara el Gregorian calendar
      * @throws Exception si ocurre algun error
      * @return GregorianCalendar con la hora indicada
      */
    protected GregorianCalendar parseTime(java.lang.String time) throws Exception {
        StringBuffer s = new StringBuffer ("");
		s = new StringBuffer ("Error en el formato de la hora -");
        if(4 != time.length() && 5 != time.length() ){
            throw new Exception("Error en el numero de caracteres, deberia ser hhmm o hh:mm");
        } else if(4 == time.length() ){
            GregorianCalendar cal = new GregorianCalendar();
            int c = 0;
            try{
                cal.set(cal.HOUR_OF_DAY,Integer.parseInt(time.substring(0,2)));
                ++c;
                cal.set(cal.MINUTE,Integer.parseInt(time.substring(2)));
            } catch (NumberFormatException e){
                if(0 == c) {s.append (" Error en el formato de la hora");}
                else {s.append (" Error en el formato del minuto");}
                throw new Exception(s + "deberia ser hhmm");
            }
            return cal;
        } else if(5 == time.length() ){
            GregorianCalendar cal = new GregorianCalendar();
            pdTokenizer tok = new pdTokenizer(time,":", 2);
            if( 2 !=tok.countTokens() ) throw new Exception(s + " deberia ser hh:mm");
            int c=0;
            try{
                cal.set(cal.HOUR_OF_DAY,Integer.parseInt(tok.nextToken()));
                ++c;
                cal.set(cal.MINUTE,Integer.parseInt(tok.nextToken()));
            } catch (NumberFormatException e){
                if(0 == c) {s.append (" Error en el formato de la hora");}
                else {s.append (" Error en el formato del minuto");}
                throw new Exception(s + "deberia ser hh:mm");
            } catch (Exception e){
                throw e;
            }
            return cal;
        }
        throw new Exception("bcOperacionProgramada:parseTime - unknown status");
    }

    /** compara esta operacion contra otra
     * @param obj Objeto contra el qu hay que comparar
     * @return -1 si el obj es menor que este
     * 0 si los objetos son iguales
     * 1 si este objeto es menor que el parametro
     */
    public int compareTo(java.lang.Object obj) {
        bcOperacionProgramada op = (bcOperacionProgramada) obj;
        int c = this.noCuenta.compareTo(op.noCuenta);
        if( 0 != c) return c;
        c = this.fechaIni.getTime().compareTo(op.fechaIni.getTime());
        if( 0 != c) return c;
        return this.fechaFin.getTime().compareTo(op.fechaFin.getTime());
    }

   public String toString()
	 {
		StringBuffer cadena=new StringBuffer("");

		cadena.append("EnlaceMig.bcOperacionProgramada");
		cadena.append("[");
		cadena.append("noCuenta=");
		cadena.append(this.noCuenta );
		cadena.append(",");
		cadena.append("usuario=");
		cadena.append(this.usuario );
		cadena.append(",");
		cadena.append("techoPresupuestal=");
		cadena.append(this.techoPresupuestal );
		cadena.append(",");
		cadena.append("fechaIni=");
		cadena.append(this.fechaIni );
		cadena.append(",");
		cadena.append("fechaFin=");
		cadena.append(this.fechaFin );
		cadena.append(",");
		cadena.append("horaIni=");
		cadena.append(this.horaIni );
		cadena.append(",");
		cadena.append("horaFin=");
		cadena.append(this.horaFin );
		cadena.append("]");

		return cadena.toString();
     }

}