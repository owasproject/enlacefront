/**
 * 
 */
package mx.altec.enlace.bo;

/**
 * @author rgutierrez
 */
public class BusinessException extends Exception {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 6858218594743141419L;
	/**
	 * Constructor por default.
	 *
	 */
	public BusinessException() {
		
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 */
	public BusinessException(String message) {
		super(message);
	}
	/**
	 * Constructor heredado.
	 * @param cause causa
	 */
	public BusinessException(Throwable cause) {
		super(cause);
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 * @param cause causa
	 */
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}
}
