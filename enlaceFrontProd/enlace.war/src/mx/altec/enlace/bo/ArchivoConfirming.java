/********************************************************************************
Archivo:	ArchivoConfirming.java
Creado:		19-Junio-2002 05:26 PM
Autor:		Hugo S�nchez Ricardez
Version:	2.0
*********************************************************************************/
package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class ArchivoConfirming {

	protected String strArchivo = null;			// ruta y nombre del archivo donde se guardar� la info
	private Vector proveedores = new Vector();	// vector con los proveedores
    protected File Archivo;						// Archivo con el cual se esta trabajando

	// X-- Constructor -----------------------------------------------------------------
	public ArchivoConfirming() {	// Constructor para llamarlo como Bean desde el JSP
    }

	// X-- Constructor -----------------------------------------------------------------
	public ArchivoConfirming(String nombreArchivo)
		{strArchivo = nombreArchivo;}

	// X-- agrega un proveedor ---------------------------------------------------------
	public boolean agregaProveedor(Proveedor prov) {
		if(proveedores.indexOf(prov)!=-1) {
			return false;
		}
		proveedores.add(prov);
		return true;
	}

	// X-- borra un proveedor ----------------------------------------------------------
	public boolean borraProveedor(int numPos) {
		if(numPos<0 || numPos>=proveedores.size()) return false;
		proveedores.remove(numPos);
		return true;
	}

	// X-- borra un proveedor ----------------------------------------------------------
	public boolean borraProveedor(String clave) {
		int pos;

		Proveedor test = new Proveedor();
		test.setClaveProveedor(clave);
		if((pos=proveedores.indexOf(test))!=-1) {
			proveedores.remove(pos);
			return true;
		}
		return false;
	}

	// X-- borra un proveedor ----------------------------------------------------------
	public boolean borraProveedor(Proveedor prov)
		{return borraProveedor(prov.getClaveProveedor());}

	// X-- regresa el proveedor que est� en la pos. especificada -----------------------
	public Proveedor obtenProveedor(int pos) {
		if(pos<0 || pos>=proveedores.size()) return null;
		return (Proveedor)proveedores.get(pos);
	}

	// X-- regresa el proveedor con la clave especificada ------------------------------
	public Proveedor obtenProveedor(String clave) {
		int pos;
		Proveedor test = new Proveedor();
		test.setClaveProveedor(clave);

		if((pos=proveedores.indexOf(test))!=-1)
			{return (Proveedor)proveedores.get(pos);}

		return null;
	}

	// X-- indica si el proveedor ya est� registrado -----------------------------------
	public boolean tieneProveedor(String clave) {
		Proveedor test = new Proveedor();
		test.setClaveProveedor(clave);
		return (proveedores.indexOf(test)!=-1);
	}

	// X-- indica si el proveedor ya est� registrado -----------------------------------
	public boolean tieneProveedor(Proveedor prov) {
		return (proveedores.indexOf(prov)!=-1);
	}

	// X-- inserta el proveedor en la posici�n especificada ----------------------------
	public void insertaProveedor(Proveedor prov,int pos) {
		proveedores.set(pos,prov);
	}

	// X-- escribe el archivo a disco --------------------------------------------------
	public void escribe() throws IOException {
		FileWriter salida;
		int act, max;

		Archivo = new File(strArchivo);
		if(Archivo.exists()) Archivo.delete();
		Archivo.createNewFile();
		salida = new FileWriter(Archivo);

		max = proveedores.size();
		for(int a=0;a<max;a++) {
			salida.write(((Proveedor)proveedores.get(a)).lineaArchivo());
		}
		salida.close();
	}

	// X-- borra un archivo ------------------------------------------------------------
	public void borrar() {
		long NumRegistros = proveedores.size();

		if(NumRegistros > 0)
			{proveedores = new Vector();}
	}

	// X-- indica la longitud en registros del archivo ---------------------------------
	public int longitud()
		{return proveedores.size();}

	// X-- regresa la trama caracter�stica del archivo ---------------------------------
	public String trama() {
		int max = proveedores.size();
		StringBuffer tramaTemp = new StringBuffer("");

		tramaTemp.append(strArchivo);
		for(int a=0;a<max;a++) tramaTemp.append("^" + ((Proveedor)proveedores.get(a)).trama());
		return tramaTemp.toString();
	}

	// X-- crea un objeto ArchivoConfirming de una trama -------------------------------
	public static ArchivoConfirming creaDeTrama(String trama) {
		String nomArchivo = "";
		Vector provs = new Vector();
		ArchivoConfirming nuevo = null;

		try {
			StringTokenizer prov = new StringTokenizer(trama,"^");
			nomArchivo = prov.nextToken();

			while(prov.hasMoreTokens())
				{provs.add(Proveedor.creaDeTrama(prov.nextToken()));}
			nuevo = new ArchivoConfirming(nomArchivo);
			nuevo.proveedores = provs;
		}
		catch(Exception e) {nuevo = null;}

		return nuevo;
	}

	// X-- crea un objeto ArchivoConfirming de un archivo ------------------------------
	public static ArchivoConfirming creaDeNombreArchivo(String NombreArchivo) {
		ArchivoConfirming nuevo = null;
		File Archivo;
		try{
			Archivo = new File(NombreArchivo);
		}catch(Exception e)	{return null;}

		try {
			nuevo = creaDeArchivo(Archivo);}
		catch(Exception e) {nuevo = null;}
		finally { return nuevo; }
	}

	// X-- crea un objeto ArchivoConfirming de un archivo ------------------------------
	public static ArchivoConfirming creaDeArchivo(File archivo)	{
		BufferedReader entrada;
		String linea;
		ArchivoConfirming nuevo = null;
		Proveedor prov = null;

		if(!archivo.exists()){ return null;	}

		try	{
			entrada = new BufferedReader(new FileReader(archivo));
			nuevo = new ArchivoConfirming(archivo.getPath());
			prov = null;

			while((linea=entrada.readLine())!=null) {
				if((prov = Proveedor.creaDeLineaArchivo(linea))==null) { break;	}
				nuevo.agregaProveedor(prov);
			}
			entrada.close();
		}catch(Exception e) {prov = null;}
		finally
			{return (prov!=null)?nuevo:null;}
	}

	// X-- Crea la trama de regreso para presentarla en la parte de registro de proveedores
	public static ArchivoConfirming creaDeArchivoRegreso(String Archivo) throws IOException {
		String registroLeido = "";
		String NuevoProvedor = "";
		String TramaProvedor = "";
		ArchivoConfirming nuevo = new ArchivoConfirming();
		Proveedor ProvRec = new Proveedor();

		BufferedReader fileAmbiente = null;
		try	{
			FileReader arch = new FileReader(Archivo);
			fileAmbiente = new BufferedReader(arch);
		}catch(IOException e){
			EIGlobal.mensajePorTrace("Error al crear el Archivo de ambiente.", EIGlobal.NivelLog.INFO);
		}
		if(fileAmbiente == null)
			{return null;}
		else {
			do {
				try {
					registroLeido = fileAmbiente.readLine();
					if (registroLeido != null) {
						NuevoProvedor = ProvRec.SeleccionaTokensDeRegistro(registroLeido);
						System.out.println("<>< ����� NuevoProvedor: " + NuevoProvedor);
						if(NuevoProvedor != null) {
							nuevo.agregaProveedor(ProvRec.creaDeTrama(NuevoProvedor));
						}
					}
				}catch ( IOException e ) {
					EIGlobal.mensajePorTrace("Error al leer el archivo de ambiente.", EIGlobal.NivelLog.INFO);
					return null;
				}
			} while ( registroLeido != null );
		}
		return nuevo;
	}

	// X -- Valida que el Nombre del Archivo a Importar sea Correcto
	public boolean EsValidoNombreArchivo(String NombreArchivo) {
		int longitud	= NombreArchivo.length();
		int pos = NombreArchivo.indexOf(".");

		if(pos != -1){
			String Nombre = NombreArchivo.substring(0,pos);

			if(NombreArchivo.substring(pos,longitud).length() > 1) {
				String Extension = NombreArchivo.substring(pos + 1,longitud);
				if(Nombre.length() <= 12 && Extension.length() <= 3)
					{ return true;	}
			}
		}
		else {
			if(NombreArchivo.length() <= 12)
				{return true; }
		}
		return false;
	}

	// X-- cambia el nombre del archivo -------------------------------------------------
	public void setNombre(String nuevoNombre)
		{strArchivo = nuevoNombre;}

	// X-- obtiene el nombre -----------------------------------------------------------
	public String getNombre() {return strArchivo;}

	// X-- Establece el Archivo que actualmente se esta ocupando
	public void setArchivo(File _Archivo)
		{Archivo = _Archivo;}

	// X-- Obtiene el Archivo con el que se esta trabajando ----------------------------
    public File getArchivo() {return Archivo;}

	// X-- Valida si el caracter recibido corresponde a un caracter alfabetico
	public boolean Caracter_EsAlfa(char caracter)
	{
		boolean ValorRetorno = true;

		if ( !(caracter >= 'A' && caracter <= 'Z') ) //Si no es Es Alpha Mayusculas
			if ( !(caracter >= 'a' && caracter <= 'z') ) // Si no es Alpha Minusculas
				if (caracter != '�' &&  caracter !='�')
					if (caracter !='�' && caracter !='�' && caracter !='�' && caracter !='�'  && caracter !='�')
						if (caracter !=' ')			// Si no es un espacio
							ValorRetorno = false;
		return ValorRetorno;
	}

	// Valida si el caracter recibido corresponde a un caracter numerico
	public boolean Caracter_EsNumerico(char caracter)
	{
		boolean ValorRetorno = true;

		if( !(caracter >= '0' && caracter <= '9') ) //Si no es Es numerico
			ValorRetorno = false;

		return ValorRetorno;
	}

	// X-- Valida que la Cadena sea Alfa-Numerica
	public boolean EsAlfaNumerica(String cadena) {
		boolean retorno = true;
		int longitud	= cadena.length();
		char caracter;

		for(int a=0; a < longitud; a++) {
			caracter = cadena.charAt(a);
			if (!Caracter_EsAlfa(caracter))
				if (!Caracter_EsNumerico(caracter))
					retorno = false;
		}
		return retorno;
	}

	// X-- Valida que la cadena sea Numerica
	public boolean EsNumerica(String cadena)
	{
		boolean retorno = true;
		int longitud	= cadena.length();
		char caracter;

		for(int a=0; a < longitud; a++) {
			caracter = cadena.charAt(a);
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
		}
		return retorno;
	}

	// X-- valida el campo Clave de Proveedor del Archivo a Importar
	public String ValidaClaveProveedor(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Clave de Proveedor, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Clave de Proveedor, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Apellido Paterno del Archivo a Importar
	public String ValidaApellidoPaterno(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Apellido Paterno, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Apellido Paterno, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Apellido Materno del Archivo a Importar
	public String ValidaApellidoMaterno(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Apellido Materno, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Nombre del Archivo a Importar
	public String ValidaNombre(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Nombre, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Nombre, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Tipo de Sociedad del Archivo a Importar
	public String ValidaTipoSociedad(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Tipo de Sociedad, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Tipo de Sociedad, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo R.F.C. del Archivo a Importar
	public String ValidaRFC(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: R.F.C., es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: R.F.C., debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo HomoClave del Archivo a Importar
	public String ValidaHomoClave(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: HomoClave, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: HomoClave, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Persona de Contacto en la Empresa del Archivo a Importar
	public String ValidaPersonaContacto(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Persona de Contacto, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Numero de Cliente para Factoraje del Archivo a Importar
	public String ValidaNoClienteFactoraje(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: N&uacute;mero de Cliente para Factoraje, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Calle y Numero del Archivo a Importar
	public String ValidaCalleNumero(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Calle y N&uacute;mero, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Calle y N&uacute;mero, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Delegacion o Municipio del Archivo a Importar
	public String ValidaDeleg_Municipio(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Delegacion o Municipio, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Delegacion o Municipio, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Ciudad o Poblacion del Archivo a Importar
	public String ValidaCiudad_Poblacion(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Ciudad o Poblacion, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Ciudad o Poblacion, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Codigo Postal del Archivo a Importar
	public String ValidaCodigoPostal(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Codigo Postal, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Codigo Postal, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Clave Estado del Archivo a Importar
	public String ValidaClaveEstado(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Clave Estado, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Clave Estado, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Clave Pais del Archivo a Importar
	public String ValidaClavePais(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Clave Pa&iacute;s, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Clave Pa&iacute;s, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Clave Medio de Confirmacion del Archivo a Importar
	public String ValidaClaveMedioConf(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Clave Medio de Confirmaci&oacute;n, es Obligatorio.<BR>";
		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Clave Medio de Confirmaci&oacute;n, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo e-mail del Archivo a Importar
	public String ValidaEmail(String Campo)
	{	return "";	}

	// X-- valida el campo Lada del Archivo a Importar
	public String ValidaLada(String Campo)
	{
		String ErrorCampo = "";

		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Lada, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Telefono del Archivo a Importar
	public String ValidaTelefono(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Tel&eacute;fono, es Obligatorio.<BR>";
		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Tel&eacute;fono, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Extension de Telefono del Archivo a Importar
	public String ValidaExtTelefono(String Campo)
	{
		String ErrorCampo = "";

		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Extensi&oacute;n de Tel&eacute;fono, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Fax del Archivo a Importar
	public String ValidaFax(String Campo)
	{
		String ErrorCampo = "";

		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Fax, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Extension de Fax del Archivo a Importar
	public String ValidaExtFax(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Extensi&oacute;n de Fax, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Forma de Pago del Archivo a Importar
	public String ValidaClaveFormaPago(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Forma de Pago, es Obligatorio.<BR>";
		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Forma de Pago, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}
	// X-- valida el campo Cuenta CLABE del Archivo a Importar
	public String ValidaCuentaClabe(String Campo)
	{
		String ErrorCampo = "";

		if(Campo == "")
			ErrorCampo += "El Campo: Cuenta CLABE, es Obligatorio.<BR>";
		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Cuenta CLABE, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Clave Banco del Archivo a Importar
	public String ValidaClaveBanco(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Clave Banco, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Sucursal del Archivo a Importar
	public String ValidaSucursal(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Sucursal, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Clave Plaza del Archivo a Importar
	public String ValidaClavePlaza(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Clave Plaza, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Limite de Financiamiento del Archivo a Importar
	//-->>> cambiar a Numerico
	public String ValidaLimiteFinanciamiento(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: L&iacute;mite de Financiamiento, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Numero de Deudores del Archivo a Importar
	//-->>> cambiar a Numerico
	public String ValidaNoDeudores(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: N&uacute;mero de Deudores, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Facturacion Anual del Archivo a Importar
	public String ValidaFacturacionAnual(String Campo)
	{
		String ErrorCampo = "";

		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Facturaci&oacute;n Anual, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Promedio de Importe de Facturacion del Archivo a Importar
	public String ValidaPromedioImporteFacturacion(String Campo)
	{
		String ErrorCampo = "";

		if(!EsNumerica(Campo))
			ErrorCampo += "El Campo: Promedio de Importe de Facturaci&oacute;n, debe ser Num&eacute;rico.<BR>";

		return ErrorCampo;
	}

	// X-- valida el campo Codigo de Cliente del Archivo a Importar
	public String ValidaCodigoCliente(String Campo)
	{
		String ErrorCampo = "";

		if(!EsAlfaNumerica(Campo))
			ErrorCampo += "El Campo: Codigo de Cliente, debe ser AlfaNum&eacute;rico.<BR>";

		return ErrorCampo;
	}
	// X--
	public String reporteArchivo(String parTrama)
	{
		String strTabla = "<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";

		strTabla = strTabla +	"<tr><td colspan=7 align=center class=tabmovtex><font color = blue>Reporte De Proveedores</font></td></tr>"	+
								"<tr><td class=tittabdat align=center>Clave de prov.</td>"				+
								"<td class=tittabdat align=center>Nombre o Raz&oacute;n Social</td>"	+
								"<td class=tittabdat align=center>R.F.C.</td>"							+
								"<td class=tittabdat align=center>Homo</td>"							+
								"<td class=tittabdat align=center>C&oacute;digo de cliente</td>"		+
								"<td class=tittabdat align=center>Estatus</td>"							+
								"<td class=tittabdat align=center>Descripci&oacute;n de estatus</td></tr><tr>";

		ArchivoConfirming archivo = ArchivoConfirming.creaDeTrama(parTrama);

		String estilo = "";
		int max = archivo.longitud();
		for(int a=0;a<max;a++) {
			estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla";
			Proveedor prov = archivo.obtenProveedor(a);
			String Nombre_RazonSocial = prov.getNombre().trim() + " " + prov.getApellidoPaterno().trim();
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getClaveProveedor().trim().equals(""))? prov.getClaveProveedor():"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(Nombre_RazonSocial.equals(""))? Nombre_RazonSocial :"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getRFC().trim().equals(""))? prov.getRFC():"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getHomoclave().trim().equals(""))? prov.getHomoclave():"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getCodigoCliente().trim().equals(""))? prov.getCodigoCliente():"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getStatus().trim().equals(""))? prov.getStatus():"&nbsp;") + "</TD>";
			strTabla += "<TD class=" + estilo + ">" + (!(prov.getDescStatus().trim().equals(""))? prov.getDescStatus():"&nbsp;") + "</TD></TR>";
		}
		strTabla += "</table>";
		return strTabla;
	}
}