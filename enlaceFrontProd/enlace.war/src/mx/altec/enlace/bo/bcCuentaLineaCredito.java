/*
 * bcCuentaLineaCredito.java
 *
 * Created on 24 de abril de 2002, 12:45 PM
 */

package mx.altec.enlace.bo;

/**
 * Contiene la informacion de una cuenta y su linea de credito
 * @author  Rafael Martinez Monriel
 * @version 1.0
 */
public class bcCuentaLineaCredito implements java.io.Serializable {

    /** Holds value of property cuenta. */
    private java.lang.String cuenta;

    /** Holds value of property lineaCredito. */
    private java.util.ArrayList lineaCredito;

    /** Creates new bcCuentaLineaCredito */
    public bcCuentaLineaCredito() {
        lineaCredito = new java.util.ArrayList();
    }

    /** Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }

    /** Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }

    /** Indexed getter for property lineaCredito.
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public java.lang.String getLineaCredito(int index) {
        return (java.lang.String) lineaCredito.get(index);
    }

    /** Getter for property lineaCredito.
     * @return Value of property lineaCredito.
     */
    public java.util.ArrayList getLineaCredito() {
        return lineaCredito;
    }

    /** Indexed setter for property lineaCredito.
     * @param index Index of the property.
     * @param lineaCredito New value of the property at <CODE>index</CODE>.
     */
    public void setLineaCredito(int index, String lineaCredito) {
        this.lineaCredito.set(index,lineaCredito);
    }

    /** Setter for property lineaCredito.
     * @param lineaCredito New value of property lineaCredito.
     */
    public void setLineaCredito(java.util.ArrayList lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    /** A�ade una nueva linea de credito
     * @param lineaCredito nuevo valor a a�adir a lineaCredito
     */
    public void addLineaCredito(java.lang.String lineaCredito) {
        this.lineaCredito.add( lineaCredito );
    }
}
