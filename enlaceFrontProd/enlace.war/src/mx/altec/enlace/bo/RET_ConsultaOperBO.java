package mx.altec.enlace.bo;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.dao.RET_ConsultaOperDAO;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.Util;

/**
 *  Clase Business para realizar la consulta de operaciones con la herramienta Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 04, 2012
 */
public class RET_ConsultaOperBO extends BaseServlet{

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Metodo para manejar el negocio para iniciar la consulta de operaciones con
	 * la herramienta Reuters
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws Exception			Dispara Exception
	 */
	public void iniciar( HttpServletRequest req, HttpServletResponse res)
		throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::iniciar:: Entrando a iniciar.", EIGlobal.NivelLog.INFO);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String fechaHoy = "";

		String strInhabiles = armaDiasInhabilesJS();
		StringBuffer varFechaHoy = new StringBuffer();

		varFechaHoy.append("  /*Fecha De Hoy*/");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n\n  /*Fechas Seleccionadas*/");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO"));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA"));
		varFechaHoy.append("');");

		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
		fechaHoy = sdf.format(cal.getTime());

		sess.removeAttribute("controladorLista");
		req.setAttribute("DiasInhabiles",strInhabiles);
		req.setAttribute("VarFechaHoy",varFechaHoy.toString());
		req.setAttribute("DiaHoy",fechaHoy);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones","RetCon01", req));
	}

	/**
	 * Metodo para manejar el negocio para la consulta de operaciones con
	 * la herramienta Reuters
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws Exception			Dispara Exception
	 */
	public void consultar( HttpServletRequest req, HttpServletResponse res)
		throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: Entrando a consultar.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = new RET_ControlConsOper();
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String EstatusOper = null;
		String folioOperLiq = null;
		String fchConsIni = null;
		String fchConsFin = null;
		String numContrato = null;
		String Where = null;
		int indice = 0;

		EstatusOper = (String)getFormParameter(req,"EstatusOper");
		folioOperLiq = (String)getFormParameter(req,"folioOperLiq");
		fchConsIni = (String)getFormParameter(req,"fchConsIni");
		fchConsFin = (String)getFormParameter(req,"fchConsFin");
		numContrato = session.getContractNumber();

		try{
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: EstatusOper>" + EstatusOper, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: folioOperLiq>" + folioOperLiq, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: fchConsIni>" + fchConsIni, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: fchConsFin>" + fchConsFin, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: numContrato>" + numContrato, EIGlobal.NivelLog.DEBUG);

			Where = "Where a.contrato = '" + numContrato + "' ";

			if(folioOperLiq!=null && !folioOperLiq.trim().equals(""))
				Where += "And a.folio_oper_liq = '" + folioOperLiq + "'";

			if(EstatusOper!=null && !EstatusOper.trim().equals("X"))
				Where += "And a.estatus_operacion = '" + EstatusOper + "'";

			if(fchConsIni.equals(fchConsFin)){
				Where += "And trunc(a.fch_hora_pactado) = "
	                  + "to_date('" + fchConsIni + "','DD/MM/YYYY')";
			}else{
				Where += "And trunc(a.fch_hora_pactado) >= "
	                  + "to_date('" + fchConsIni + "','DD/MM/YYYY')"
	                  + " And trunc(a.fch_hora_pactado) <="
	                  + "to_date('" + fchConsFin + "','DD/MM/YYYY')";
			}

			indice = 1;
			masterDao.setLista(dao.consulta(Where, indice));
			masterDao.setNumPages(dao.getNumPages(Where));
			masterDao.setNumRecords(dao.getNumRecords(Where));
			masterDao.setIdPage(indice);
			masterDao.setNumRecordPageDesde(dao.getRecordDesde(indice)+1);
			masterDao.setNumRecordPageHasta(dao.getRecordHasta(indice
													, masterDao.getNumPages()
													, masterDao.getNumRecords()));
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: RET_ControlConsOper.MAX_REGISTROS_PAG->"+RET_ControlConsOper.MAX_REGISTROS_PAG, EIGlobal.NivelLog.DEBUG);
			masterDao.setMaxNumRecordsPage(RET_ControlConsOper.MAX_REGISTROS_PAG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: masterDao.getMaxNumRecordsPage()->"+masterDao.getMaxNumRecordsPage(), EIGlobal.NivelLog.DEBUG);
			masterDao.setWhereCondicion(Where);

			if(masterDao.getLista().size()>0){
				EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: Si hay registros de consulta y se procede a exportar", EIGlobal.NivelLog.DEBUG);
				exportar(dao.exporta(Where), req, res);
			}else{
				paginaError("<b>No hay registros asociados a su consulta.</b>", req, res);
			}

		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::consultar:: Error Controlado>" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}

		sess.setAttribute("controladorLista", masterDao);
		//req.setAttribute("mensaje","\"La consulta fue exitosa.\",1");
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones","RetCon02", req));
	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Siguiente
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @param detalle				detalle de la operación N - No detalle, S - Detalle
	 * @throws Exception			Dispara Exception
	 */
	public void siguiente(HttpServletRequest req, HttpServletResponse res, String detalle)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::siguiente:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = null;
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String condicionBeforeComun = null;
		int indiceSiguiente = 0;
		String ayuda = "RetCon02";

		if(detalle.equals("N")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
			indiceSiguiente = masterDao.getIdPage() + 1;
			condicionBeforeComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consulta(condicionBeforeComun, indiceSiguiente));
		}else if(detalle.equals("S")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorListaDetalle");
			indiceSiguiente = masterDao.getIdPage() + 1;
			condicionBeforeComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consultaDetalle(condicionBeforeComun, indiceSiguiente));
			ayuda = "RetCon03";
		}
		masterDao.setIdPage(indiceSiguiente);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceSiguiente)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceSiguiente
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::siguiente:: Bussines Siguiente exitoso.", EIGlobal.NivelLog.DEBUG);

		if(detalle.equals("N"))
			sess.setAttribute("controladorLista", masterDao);
		else if(detalle.equals("S"))
			sess.setAttribute("controladorListaDetalle", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones",ayuda, req));
	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Anterior
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @param detalle				detalle de la operación N - No detalle, S - Detalle
	 * @throws Exception			Dispara Exception
	 */
	public void anterior(HttpServletRequest req, HttpServletResponse res, String detalle)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::anterior:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = null;
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String condicionAfterComun = null;
		int indiceAnterior = 0;
		String ayuda = "RetCon02";

		if(detalle.equals("N")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
			indiceAnterior = masterDao.getIdPage() - 1;
			condicionAfterComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consulta(condicionAfterComun, indiceAnterior));
		}else if(detalle.equals("S")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorListaDetalle");
			indiceAnterior = masterDao.getIdPage() - 1;
			condicionAfterComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consultaDetalle(condicionAfterComun, indiceAnterior));
			ayuda = "RetCon03";
		}
		masterDao.setIdPage(indiceAnterior);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceAnterior)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceAnterior
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::anterior:: Bussines Anterior exitoso.", EIGlobal.NivelLog.DEBUG);

		if(detalle.equals("N"))
			sess.setAttribute("controladorLista", masterDao);
		else if(detalle.equals("S"))
			sess.setAttribute("controladorListaDetalle", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones",ayuda, req));

	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Ultimo
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @param detalle				detalle de la operación N - No detalle, S - Detalle
	 * @throws Exception			Dispara Exception
	 */
	public void ultimo(HttpServletRequest req, HttpServletResponse res, String detalle)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::ultimo:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = null;
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String condicionBeforeComun = null;
		int indiceUltimo = 0;
		String ayuda = "RetCon02";

		if(detalle.equals("N")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
			indiceUltimo = masterDao.getNumPages();
			condicionBeforeComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consulta(condicionBeforeComun, indiceUltimo));
		}else if(detalle.equals("S")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorListaDetalle");
			indiceUltimo = masterDao.getNumPages();
			condicionBeforeComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consultaDetalle(condicionBeforeComun, indiceUltimo));
			ayuda = "RetCon03";
		}
		masterDao.setIdPage(indiceUltimo);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceUltimo)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceUltimo
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::ultimo:: Bussines Ultimo exitoso.", EIGlobal.NivelLog.DEBUG);

		if(detalle.equals("N"))
			sess.setAttribute("controladorLista", masterDao);
		else if(detalle.equals("S"))
			sess.setAttribute("controladorListaDetalle", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones",ayuda, req));

	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Primero
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @param detalle				detalle de la operación N - No detalle, S - Detalle
	 * @throws Exception			Dispara Exception
	 */
	public void primero(HttpServletRequest req, HttpServletResponse res, String detalle)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::primero:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = null;
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String condicionAfterComun = null;
		int indicePrimero = 0;
		String ayuda = "RetCon02";

		indicePrimero = 1;
		if(detalle.equals("N")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
			condicionAfterComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consulta(condicionAfterComun, indicePrimero));
		}else if(detalle.equals("S")){
			masterDao = (RET_ControlConsOper) sess.getAttribute("controladorListaDetalle");
			condicionAfterComun = masterDao.getWhereCondicion();
			masterDao.setLista(dao.consultaDetalle(condicionAfterComun, indicePrimero));
			ayuda = "RetCon03";
		}
		masterDao.setIdPage(indicePrimero);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indicePrimero)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indicePrimero
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::primero:: Bussines Primero exitoso.", EIGlobal.NivelLog.DEBUG);

		if(detalle.equals("N"))
			sess.setAttribute("controladorLista", masterDao);
		else if(detalle.equals("S"))
			sess.setAttribute("controladorListaDetalle", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones",ayuda, req));
	}

	/**
	 * Metodo para manejar el negocio para exportar las operaciones
	 * reuters que se consultan.
	 *
	 * @param listaExport			Lista de exportación
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws Exception			Dispara Exception
	 */
	public void exportar(Vector listaExport, HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::exportar:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String linea = null;
		FileWriter fileExport = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + session.getUserID() + ".doc");
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);
		try{
			for(int i=0; i<listaExport.size(); i++){
				RET_OperacionVO beanExport = (RET_OperacionVO)listaExport.get(i);
				linea = "";
				linea += rellenar(beanExport.getFolioEnla(),20,' ','D')
					  +  rellenar(beanExport.getFolioRet(),20,' ','D')
					  +  rellenar(Util.muestraFolioOper(beanExport.getEstatusOperacion(), beanExport.getFolioOper()),20,' ','D')
					  +  rellenar(beanExport.getUsrRegistro(),18,' ','D')
					  +  rellenar(beanExport.getUsrAutoriza(),18,' ','D')
					  +  rellenar(beanExport.getCtaCargo(),30,' ','D')
					  +  rellenar(beanExport.getImporteTotOper(),30,' ','D')
					  +  rellenar(beanExport.getDivisaOperante(),30,' ','D')
					  +  rellenar(beanExport.getContraDivisa(),30,' ','D')
					  +  rellenar(Util.muestraTipoOperUser(beanExport.getTipoOperacion()),30,' ','D')
					  +  rellenar(beanExport.getTcPactado(),20,' ','D')
					  +  rellenar(beanExport.getConcepto(),100,' ','D')
					  +  rellenar(beanExport.getDescEstatusOperacion(),30,' ','D')
					  +  rellenar(beanExport.getFchHoraPactado(),30,' ','D')
					  +  rellenar(beanExport.getFchHoraComplemento(),30,' ','D')
					  +  rellenar(beanExport.getFchHoraLiberado(),30,' ','D');
			    fileExport.write(linea + "\n");
			}
			sess.setAttribute("fileExport","/Download/" + session.getUserID() + ".doc");
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::exportar:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::exportar:: Se cerro el archivo.", EIGlobal.NivelLog.DEBUG);
		}
	}

	/**
	 * Metodo para manejar el negocio para el detalle de una operaciones de
	 * la herramienta Reuters
	 *
	 * @param req					request de la operación
	 * @param res					response de la operación
	 * @throws Exception			Dispara Exception
	 */
	public void verDetalle( HttpServletRequest req, HttpServletResponse res)
		throws Exception{
		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: Entrando a verDetalle.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = new RET_ControlConsOper();
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		StringTokenizer token = null;
		String operSel = null;
		String folioEnla = null;
		String folioOper = null;
		String numContrato = null;
		String Where = null;
		String delimRegistro = null;
		int indice = 0;

		operSel = (String)getFormParameter(req,"operSel");
		delimRegistro = "|";
		token = new StringTokenizer(operSel, delimRegistro);
		folioEnla = token.nextToken().trim();
		folioOper = token.nextToken().trim();
		numContrato = session.getContractNumber();

		try{
			sess.removeAttribute("controladorListaDetalle");

			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: operSel>" + operSel, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: folioEnla>" + folioEnla, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: folioOper>" + folioOper, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: numContrato>" + numContrato, EIGlobal.NivelLog.DEBUG);

			Where = "Where a.contrato = '" + numContrato + "' ";

			if(folioEnla!=null && !folioEnla.trim().equals(""))
				Where += "And a.folio_enla = '" + folioEnla + "'";

			indice = 1;
			masterDao.setLista(dao.consultaDetalle(Where, indice));
			masterDao.setNumPages(dao.getNumPagesDetalle(Where));
			masterDao.setNumRecords(dao.getNumRecordsDetalle(Where));
			masterDao.setIdPage(indice);
			masterDao.setNumRecordPageDesde(dao.getRecordDesde(indice)+1);
			masterDao.setNumRecordPageHasta(dao.getRecordHasta(indice
													, masterDao.getNumPages()
													, masterDao.getNumRecords()));
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: RET_ControlConsOper.MAX_REGISTROS_PAG->"+RET_ControlConsOper.MAX_REGISTROS_PAG, EIGlobal.NivelLog.DEBUG);
			masterDao.setMaxNumRecordsPage(RET_ControlConsOper.MAX_REGISTROS_PAG);
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: masterDao.getMaxNumRecordsPage()->"+masterDao.getMaxNumRecordsPage(), EIGlobal.NivelLog.DEBUG);
			masterDao.setWhereCondicion(Where);

			if(masterDao.getLista().size()>0){
				EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: Si hay registros de consulta y se procede a exportar", EIGlobal.NivelLog.DEBUG);
				//exportar(dao.exporta(Where), req, res);
			}else{
				paginaError("<b>No hay registros asociados a su consulta.</b>", req, res);
			}

		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::verDetalle:: Error Controlado>" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}

		sess.setAttribute("controladorListaDetalle", masterDao);
		//req.setAttribute("folioPadre",operSel);
		req.setAttribute("folioPadre",folioOper);
		//req.setAttribute("mensaje","\"La consulta fue exitosa.\",1");
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Consulta de Operaciones","RetCon03", req));
	}

	/**
	 * Muestra una pagina de error con la descripcion indicada
	 * @param mensaje			Mensaje de error presentado en pantalla
	 * @param request			request de la operación
	 * @param response			response de la operación
	 *
	 */
	public void paginaError(String mensaje,HttpServletRequest request, HttpServletResponse response){
		try{
			despliegaPaginaErrorURL(mensaje,
				"Consulta de Operaciones",
				"Transferencias FX Online &gt; ",
				"Consulta de Operaciones",
				request,
				response);
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("RET_ConsultaOperBO::paginaError:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de desplegar la pagina de error
	 * por medio de una URL
	 *
	 * @param Error			Error a desplegar
	 * @param param1		Parametro del encabezado
	 * @param param2		Parametro del encabezado
	 * @param param3 		Parametro del encabezado
	 * @param request		Petición de la aplicación
	 *
	 */
	private void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RequestDispatcher view = null;

		EIGlobal.mensajePorTrace("RET_ConsultaOperBO::despliegaPaginaErrorURL:: Entrando.", EIGlobal.NivelLog.DEBUG);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		String param4 = "s55205h";

		request.setAttribute( "FechaHoy", fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2 + param3, param4, request ) );

		//evalTemplate("/jsp/EI_Mensaje.jsp", request, response);
		view = request.getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		view.forward(request,response);
	}

	/**
	 * Metodo para definir la fecha para el mensaje de error.
	 *
	 * @param strFecha			formato de la fecha
	 * @return Fecha Formato
	 */
	private String fechaHoy(String strFecha) {
		Date Hoy = new Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);

		String dd = "";
		String mm = "";
		String aa = "";
		String aaaa = "";
		String dt = "";
		String mt = "";
		String th = "";
		String tm = "";
		String ts = "";

		int indth = 0;
		int indtm = 0;
		int indts = 0;
		int inddd = 0;
		int indmm = 0;
		int indaaaa = 0;
		int indaa = 0;
		int indmt = 0;
		int inddt = 0;

		String[] dias = {
			"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
			"Sabado"
		};
		String[] meses = {
			"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
			"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
		};

		if (Cal.get(Calendar.DATE) <= 9)
			dd += "0" + Cal.get(Calendar.DATE);
		else
			dd += Cal.get(Calendar.DATE);

		if (Cal.get(Calendar.MONTH) + 1 <= 9)
			mm += "0" + (Cal.get(Calendar.MONTH) + 1);
		else
			mm += (Cal.get(Calendar.MONTH) + 1);

		if (Cal.get(Calendar.HOUR_OF_DAY) < 10)
			th += "0" + Cal.get(Calendar.HOUR_OF_DAY);
		else
			th += Cal.get(Calendar.HOUR_OF_DAY);

		if (Cal.get(Calendar.MINUTE) < 10)
			tm += "0" + Cal.get(Calendar.MINUTE);
		else
			tm += Cal.get(Calendar.MINUTE);

		if (Cal.get(Calendar.SECOND) < 10)
			ts += "0" + Cal.get(Calendar.SECOND);
		else
			ts += Cal.get(Calendar.SECOND);

		aaaa += Cal.get(Calendar.YEAR);
		aa += aaaa.substring(aaaa.length() - 2, aaaa.length());
		dt += dias[Cal.get(Calendar.DAY_OF_WEEK) - 1];
		mt += meses[Cal.get(Calendar.MONTH)];

		while (
			indth >= 0 || indtm >= 0 || indts >= 0 || inddd >= 0
			|| indmm >= 0 || indaaaa >= 0 || indaa >= 0 || indmt >= 0
			|| inddt >= 0
			) {
			indth = strFecha.indexOf("th");
			if (indth >= 0)
				strFecha = strFecha.substring(0, indth) + th + strFecha.substring(indth + 2, strFecha.length());
			indtm = strFecha.indexOf("tm");
			if (indtm >= 0)
				strFecha = strFecha.substring(0, indtm) + tm + strFecha.substring(indtm + 2, strFecha.length());
			indts = strFecha.indexOf("ts");
			if (indts >= 0)
				strFecha = strFecha.substring(0, indts) + ts + strFecha.substring(indts + 2, strFecha.length());
			inddd = strFecha.indexOf("dd");
			if (inddd >= 0)
				strFecha = strFecha.substring(0, inddd) + dd + strFecha.substring(inddd + 2, strFecha.length());
			indmm = strFecha.indexOf("mm");
			if (indmm >= 0)
				strFecha = strFecha.substring(0, indmm) + mm + strFecha.substring(indmm + 2, strFecha.length());
			indaaaa = strFecha.indexOf("aaaa");
			if (indaaaa >= 0)
				strFecha = strFecha.substring(0, indaaaa) + aaaa + strFecha.substring(indaaaa + 4, strFecha.length());
			indaa = strFecha.indexOf("aa");
			if (indaa >= 0)
				strFecha = strFecha.substring(0, indaa) + aa + strFecha.substring(indaa + 2, strFecha.length());
			indmt = strFecha.indexOf("mt");
			if (indmt >= 0)
				strFecha = strFecha.substring(0, indmt) + mt + strFecha.substring(indmt + 2, strFecha.length());
			inddt = strFecha.indexOf("dt");
			if (inddt >= 0)
				strFecha = strFecha.substring(0, inddt) + dt + strFecha.substring(inddt + 2, strFecha.length());
		}
		return strFecha;
	}

	/**
	 * rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 */
	public static String rellenar (String cad, int lon, char rel, char tip){
		String aux = "";
		if(cad.length()>lon) return cad.substring(0,lon);
		if(tip!='I' && tip!='D') tip = 'I';
		if( tip=='D' ) aux = cad;
		for (int i=0; i<(lon-cad.length()); i++) aux += ""+ rel;
		if( tip=='I' ) aux += ""+ cad;
		return aux;
	}

	/**
	 * Metodo encargado de generar las fechas que se mostraran en el calendario
	 *
	 * @param tipo			Tipo de dato a generar
	 * @return strFecha		Fecha generada
	 */
	private String generaFechaAnt(String tipo){
		String strFecha = "";
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();

		Cal.setTime(Hoy);

		while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7) {
			Cal.add(Calendar.DATE, -1);
	    }

		if(tipo.equals("FECHA")) {
			if(Cal.get(Calendar.DATE) <= 9)
				strFecha += "0" + Cal.get(Calendar.DATE) + "/";
			else
				strFecha += Cal.get(Calendar.DATE) + "/";

			if(Cal.get(Calendar.MONTH)+ 1 <= 9)
				strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
			else
				strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";

			strFecha+=Cal.get(Calendar.YEAR);
		}

		if(tipo.equals("DIA"))
			strFecha += Cal.get(Calendar.DATE);

		if(tipo.equals("MES"))
			strFecha += (Cal.get(Calendar.MONTH) + 1);

		if(tipo.equals("ANIO"))
	        strFecha += Cal.get(Calendar.YEAR);

		return strFecha;
	}

	/**
	 * Metodo para traer los dias inhabiles
	 * @return
	 * @throws Exception
	 */
	private String armaDiasInhabilesJS() throws Exception{
		RET_ConsultaOperDAO dao = new RET_ConsultaOperDAO();
		String resultado="";
		Vector diasInhabiles;

		int indice=0;
		try{
			diasInhabiles = dao.CargarDias(1);
			if(diasInhabiles!=null){
				resultado = diasInhabiles.elementAt(indice).toString();
				for(indice=1; indice<diasInhabiles.size(); indice++)
					resultado += ", " + diasInhabiles.elementAt(indice).toString();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resultado;
	}
}