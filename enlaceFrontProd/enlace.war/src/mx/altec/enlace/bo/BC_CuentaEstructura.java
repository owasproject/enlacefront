//11/04/02
//Esta clase nos permite Establecer adecuadamente lo valores de Cuenta y Descripci�n.
////////////////////Modificaci�n para integraci�n {BASE CERO (Mto Estructura)}///////////////////////////////////////
package mx.altec.enlace.bo;
import java.util.Vector;

import mx.altec.enlace.utilerias.EIGlobal;

public class BC_CuentaEstructura extends BC_Cuenta
	{
	// --- Campos del jsp Y campos adicionales de bcMtoEstructura.jsp//////////////////////////////////////////////////
	//private BC_Cuenta padre;
	private String lineaCred;
	private String horarioOper1;
	private String horarioOper2;
	private String vigencia1;
	private String vigencia2;
	private String techopre;
	private int tipo;
	private int nivel=nivel();

	//este campo se usa cuando la cuenta se crea de una trama
	private BC_Cuenta padre;
	public String posiblePadre;


	// --- Constructores e inicializadores ///////////////////////////////////////////
	public BC_CuentaEstructura(BC_Cuenta unaCuenta)
		{
		super(unaCuenta.getNumCta(),unaCuenta.getDescripcion());
		if(unaCuenta instanceof BC_CuentaEstructura)
			{
			lineaCred		=	((BC_CuentaEstructura)unaCuenta).lineaCred;
			horarioOper1	=	((BC_CuentaEstructura)unaCuenta).horarioOper1;
			horarioOper2	=	((BC_CuentaEstructura)unaCuenta).horarioOper2;
			vigencia1		=	((BC_CuentaEstructura)unaCuenta).vigencia1;
			vigencia2		=	((BC_CuentaEstructura)unaCuenta).vigencia2;
			techopre		=	((BC_CuentaEstructura)unaCuenta).techopre;
		//	tipo			=	((BC_CuentaEstructura)unaCuenta).tipo;
			}
		else
			inicializa();
		}

	public BC_CuentaEstructura(String numCuenta)
		{
		super(numCuenta);
		inicializa();
		}

	private void inicializa()
		{
		lineaCred = "";						//campos adicionales
		horarioOper1 = "";
		horarioOper2= "";
		vigencia1= "";
		vigencia2= "";
		techopre = "0.0";
		tipo =0;
		padre = null;
		posiblePadre = "";
		}

	///////////////Indica la linea de cr'dito asociado a una Cuenta
	public String getLineaCred()
		{
			return lineaCred;
		}
	/////////////Actualiza a la linea de cr'dito
	public void setLineaCred(String lc)
		{
			this.lineaCred = lc;
		}
	// --- trae el valor de horarioOper1
	public String getHorarioOper1()
		{return horarioOper1;}

	// ---coloca el valor de HorarioOper1
	public void setHorarioOper1(String xho1)
		{horarioOper1 = xho1;}

	// --trae el valor de horarioOper2

	public String getHorarioOper2()
		{return horarioOper2;}

	// ---coloca el valor de HorarioOper1
	public void setHorarioOper2(String xho2)
		{horarioOper2 = xho2;}

	// ---trae el valor de Vigencia1
	public String getVigencia1()
		{return vigencia1;}

	// ---coloca valor de vigencia1
	public void setVigencia1(String xv1)
		{vigencia1 = xv1;}

	// ---trae el valor de Vigencia12
	public String getVigencia2()
		{return vigencia2;}

	// ---coloca el valor de vigencia2
	public void setVigencia2(String xv2)
		{vigencia2 = xv2;}

	////////////////Devuelve el importe actual
	public String getTechopre()
		{return this.techopre;}

	///Setea al  importe correspondiente
	public void setTechopre(String tp)
		{techopre = tp;}

	// --- Indica el tipo para la cuenta -----------------------------------------------
	public void setTipo(int tipo)
		{this.tipo = tipo;}

//// --- Devuelve el nivel de la cuenta
	public int getNivel()
		{
		return this.nivel;
		}

	public void setNivel(int n)
	{
		this.nivel = n;
	}

// --- Devuelve el tipo de la cuenta -----------------------------------------------
	public int getTipo()
		{return this.tipo;}

	//Regresa el horario de operacion
	public String getHorarios()
		{
			return "De " + getHorarioOper1() + "A " + getHorarioOper2();
		}

	//Regresa la Vigencia es el par de fechas
	public String getVigencia()
		{
			return "Del " + getVigencia1() + "Al " + getVigencia2();
		}

	// --- Devuelve el padre de la cuenta ----------------------------------------------
	public BC_Cuenta getPadre()
		{return padre;}

	// --- Indica el nuevo padre -------------------------------------------------------
	public boolean setPadre(BC_Cuenta newPadre)
		{
		if (newPadre.getNumCta().equals(getNumCta())) return false;
		if (desciendeDe(newPadre)) return false;
		if (((BC_CuentaEstructura)newPadre).desciendeDe(this)) return false;
		padre = newPadre;
		return true;
		}

	// --- indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(String numCta)
		{
		BC_Cuenta cuenta = padre;
		boolean resultado = false;
		while(!resultado)
			{
			if(cuenta == null) break;
			if(numCta.equals(cuenta.getNumCta())) resultado = true;
			if(cuenta instanceof BC_CuentaEstructura)
				cuenta = ((BC_CuentaEstructura)cuenta).getPadre();
			else
				cuenta = null;
			}
		return resultado;
		}

	// --- Indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(BC_Cuenta posiblePadre)
		{
		return desciendeDe(posiblePadre.getNumCta());
		}

	////// Regresa el nivel de la cuenta  //////////////////////////////////////////////
	public int nivel()
		{
		BC_Cuenta cuenta = padre;
		int resultado = 1;
		while(true)
			{
			if(cuenta == null) break;
			if(cuenta instanceof BC_CuentaEstructura)
				cuenta = ((BC_CuentaEstructura)cuenta).getPadre();
			else
				cuenta = null;
			resultado++;
			}
		return resultado;
		}

	/////// Regresa una trama conteniendo datos de la cuenta ////////////////////////////

/*	public String trama()
		{
		String trama = "@" + getNumCta() + "|" + ((padre == null)?" ":padre.getNumCta())
					+ "|" + ((getDescripcion()=="")?" ":getDescripcion()) + "|"
					+ lineaCred + "|" + horarioOper1 + "|" + horarioOper2 + "|"
					+ vigencia1 + "|" + vigencia2 + "|" + techopre;
		return trama;
		}*/


	public String trama()
		{
		String trama = "@"+getNumCta() + "|" + ((padre == null)?" ":padre.getNumCta()) +
					 "|" + lineaCred + "|" + nivel + "|" + techopre + "|" + horarioOper1 + "|" + horarioOper2 + "|01/01/01|02/02/02";
		return trama;
		}


	///////Igual que el anterior pero tomando en cuenta a posiblePadre ////////////////////////
	public String tramaPosible()
		{
		String trama = "@" + getNumCta() + "|" + ((padre == null)?" ":padre.getNumCta())+
						"|" + lineaCred + "|" + nivel + "|" + techopre + "|" + horarioOper1 + "|" + horarioOper2 + "||01/01/01|02/02/02";
		return trama;
		}

	/////// METODO ESTATICO: Crea una nueva cuenta a partir de una trama /////////////////////

	public static BC_CuentaEstructura creaCuentaEstructura(String trama)
		{
		int car;
		String strAux, strcad;
		BC_CuentaEstructura cuenta = null;
		if(trama == null) return null;
		if(!trama.substring(0,1).equals("@")) return null;
		trama = trama.substring(1);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta = new BC_CuentaEstructura(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.posiblePadre = (strAux.equals(" "))?"":strAux;
	//	car = trama.indexOf("|"); if(car == -1) return null;
	//	strAux = trama.substring(0,car); trama = trama.substring(car+1);
	//	cuenta.setDescripcion(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setLineaCred(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setNivel(Integer.parseInt(strAux));
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setTechopre(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setHorarioOper1(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setHorarioOper2(strAux);
		cuenta.setVigencia1("01/01/01");
//		car = trama.indexOf("|"); if(car == -1) return null;
//		strAux = trama.substring(0,car); trama = trama.substring(car+1);
//		cuenta.setVigencia1(strAux);
//		car = trama.indexOf("|"); if(car == -1) return null;
//		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setVigencia2("01/01/01");
		return cuenta;
		}

//	}

// --- ESTATICO: Copia una cuenta pero con otro num de cuenta ----------------------
	public static BC_CuentaEstructura copiaCuenta(String neoNumCta, BC_CuentaEstructura cuenta)
		{
		BC_CuentaEstructura neoCuenta = new BC_CuentaEstructura(neoNumCta);

		neoCuenta.setDescripcion(cuenta.getDescripcion());
		neoCuenta.setLineaCred(cuenta.getLineaCred());
		neoCuenta.setHorarioOper1(cuenta.getHorarioOper1());
		neoCuenta.setHorarioOper2(cuenta.getHorarioOper2());
		neoCuenta.setVigencia1(cuenta.getVigencia1());
		neoCuenta.setVigencia2(cuenta.getVigencia2());
		neoCuenta.setTechopre(cuenta.getTechopre());
		//neoCuenta.setTipo(cuenta.getTipo());
		neoCuenta.posiblePadre = cuenta.posiblePadre;
		return neoCuenta;
		}

	// ------------------------
	public static void main(String args[])
		{
		EIGlobal.mensajePorTrace("<DEBUG BC_CuentaEstructura> Creando cuenta....", EIGlobal.NivelLog.INFO);
		BC_CuentaEstructura c1 = new BC_CuentaEstructura("111111111");
		c1.setDescripcion("Cuenta 1");
		c1.setLineaCred("Linea de cr'dito");
		c1.setHorarioOper1("Horario 1");
		c1.setHorarioOper2("Horario 2");
		c1.setVigencia1("Vigencia 1");
		c1.setVigencia2("Vigencia 2");
		c1.setTechopre("Techo");
		EIGlobal.mensajePorTrace("Cuenta creada...Probando trama...La trama de c1 es: " + c1.trama(), EIGlobal.NivelLog.INFO);
		String strAux = c1.trama();
		EIGlobal.mensajePorTrace("Creando cuenta a partir de trama...", EIGlobal.NivelLog.INFO);
		BC_CuentaEstructura c2 = BC_CuentaEstructura.creaCuentaEstructura(strAux);
		}
	}



/* Segun tramas reales que me envia Tuxedo...
num_contrato|Cuenta_cheque|Num_Cuenta_Credito|Nivel|Nivel_Global|Techa_Presupuestal
hora_Inicio|hora_Fin|Importe_Dispuesto|Usuario.
?? con el Nivel Global e  Importe_Dispuesto
me sirve nadamas :

num_contrato
Cuenta_cheque
Num_Cuenta_Credito
Nivel
Techa_Presupueatal
hora_Inicio
hora_Fin

*/

