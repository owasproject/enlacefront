/**
* ISBAN M�xico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BOOCE8.java
*
* Control de versiones:
*
* Version Date/Hour        By            Company    Description
* ------- ---------------- ------------- --------   ----------------------------
* 1.0     09-11-2011 19:00 Z225016       BSD        Creacion
*
*/
package mx.altec.enlace.bo;

import mx.altec.enlace.beans.BeanOCE8;
import mx.altec.enlace.dao.DAOOCE8;
import mx.altec.enlace.utilerias.EIGlobal;

public class BOOCE8 {
	public boolean bloqueado(String usuario, String contrato) throws Exception {
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::bloqueado() Inicio", EIGlobal.NivelLog.INFO);

		boolean result = false;

		DAOOCE8 daoOCE8 = new DAOOCE8();

		BeanOCE8 bean = new BeanOCE8();
		bean.setPerRepaginado(usuario);
		bean.setContrato(contrato);
		bean.setRepaginado("S");

		try {
			bean = daoOCE8.consultaOCE8(bean);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (bean.getEstBloqueo().equals("S")){
			result = true;
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::bloqueado() resultado usuario bloqueado = " + result, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::bloqueado() Fin", EIGlobal.NivelLog.INFO);
		return result;
	}
}
