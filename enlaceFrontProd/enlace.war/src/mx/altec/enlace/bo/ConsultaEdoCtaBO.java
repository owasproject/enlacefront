package mx.altec.enlace.bo;

import java.util.List;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.beans.ConsEdoCtaDetalleBean;
import mx.altec.enlace.beans.ConsEdoCtaResultadoBean;
import mx.altec.enlace.beans.ConsultaEdoCtaBean;
//import mx.altec.enlace.dao.ConfEdosCtaIndDAO;
import mx.altec.enlace.dao.ConsultaEdoCtaDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import org.apache.commons.lang.StringUtils;

public class ConsultaEdoCtaBO {
	
	/**
	 * edoCtaDao
	 */
	private final transient ConsultaEdoCtaDAO edoCtaDao = new ConsultaEdoCtaDAO();
	
	
	/**
	 * Realiza la coansulta de edo cta
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return List<ConsEdoCtaResultadoBean> : List<ConsEdoCtaResultadoBean>
	 */
	public List<ConsEdoCtaResultadoBean> consultaEdoCta(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		return edoCtaDao.consultaEdoCta(consultaEdoCtaBean);
	}
	
	/**
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return creado : creado
	 */
	public String consultaEdoCtaDetalleTotal(ConsultaEdoCtaBean consultaEdoCtaBean){
		
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaBO-> consultaEdoCtaDetalleTotal ", EIGlobal.NivelLog.DEBUG);
		
		StringBuilder linea = new StringBuilder();
		List<ConsEdoCtaDetalleBean> list = edoCtaDao.consultaEdoCtaDetalleTotal(consultaEdoCtaBean);
		
		if(list != null && !list.isEmpty()){
			//PYME 2015 FSW Indra se agregan encabezados para la exportacion de TDC 
			String tipoEdo = list.get(0).getTipoEdoCta();
			linea.append( "001".equals(tipoEdo) ? 
				"Secuencia de domicilio|Codigo Cliente|Cuenta|Descripcion de Cuenta|Suspension de envio a domicilio|Estatus|Descripcion de Estatus\r\n" :
				"Contrato|Codigo Cliente|Credito|Descripcion Credito|Suspension de envio a domicilio (Paperless)|Estatus|Descripcion de Estatus\r\n" );
			for(int i=0;i<list.size();i++){				
				ConsEdoCtaDetalleBean temp = (ConsEdoCtaDetalleBean)list.get(i);
				String auxTdc = (temp.getContratoTDC().length() == 20) ? 
						temp.getContratoTDC().substring(0,4)+"-"+temp.getContratoTDC().substring(4,8)+"-"+temp.getContratoTDC().substring(8,20) : temp.getContratoTDC().trim();
				linea				
					//PYME 2015 FSW Indra se valida para CTA o TDC
					.append( "001".equals(tipoEdo) ? (StringUtils.isBlank(temp.getNumSec())?"":temp.getNumSec().trim()) : auxTdc ).append("|")
					.append((StringUtils.isBlank(temp.getCodClte())?"":temp.getCodClte().trim())).append("|")
					.append((StringUtils.isBlank(temp.getNumCuenta())?"":temp.getNumCuenta().trim())).append("|")
                    .append((StringUtils.isBlank(temp.getDescCta())?"":temp.getDescCta().trim())).append("|")
                    .append((StringUtils.isBlank(temp.getPpls())?"":temp.getPpls().trim())).append("|")
                    .append((StringUtils.isBlank(temp.getEstatus())?"":temp.getEstatus().trim())).append("|")
                    .append((StringUtils.isBlank(temp.getDescError())?"":temp.getDescError().trim())).append("\r\n");
			}
		}else{
			linea.append("Error al Obtener la informacion");
		}
		return linea.toString();

	}
	
	/**
	 * Realiza la consulta del detalle
	 * @param consultaEdoCtaBean : consultaEdoCtaBean
	 * @return List<ConsEdoCtaDetalleBean> : List<ConsEdoCtaDetalleBean>
	 */
	public List<ConsEdoCtaDetalleBean> consultaEdoCtaDetalle(ConsultaEdoCtaBean consultaEdoCtaBean){

		return edoCtaDao.consultaEdoCtaDetalle(consultaEdoCtaBean);
	}
	
	/**
	 * Metodo para cosultar el estatus de la cuenta para edo de cta PDF
	 * @param cuenta La cuenta seleccionada
	 * @return ConfEdosCtaIndBean Bean de la informacion
	 */
	public ConfEdosCtaIndBean mostrarCuentasPDF(String cuenta) {
		
		return edoCtaDao.mostrarCuentasPDF(cuenta);
	}

}
