package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import mx.altec.enlace.dao.GenericDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase para invocar la transaccion LMPL
 * @author FSW-Indra
 * @sice 01/03/2015
 */
public class TrxLMPLBO extends GenericDAO {
	
	/**
	 * FRANQUICIA CANAL 
	 */
	private static final String CANAL_ENLACE = "9004";
	/**
	 * NUMERO DE TARJETA
	 */
	private String numeroTarjeta;
	/**
	 * IND IMPRESION
	 */
	private String indImpresion;	
	/**
	 * RESULTADO DE LA OPERACION 
	 */
	private boolean resultadoOperacion;	
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de le Transaccion
	 */
	private int codStatus;
	
	/**
	 * Metodo encargado de realizar la ejecución de la Transaccion LMPL.
	 * @since 01/03/2015
	 * @author FSW-Indra
	 */
	public void ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		
		
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxLMPLBO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", EIGlobal.NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxLMPLBO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",EIGlobal.NivelLog.INFO);
		resultadoOperacion=desentramaRespuesta(tramaRespuesta, tramaEntrada) ;
	
		
	}
	
	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 01/03/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void obtenerTramaEntrada(StringBuffer tramaEntrada) {
		
		tramaEntrada.append( armaCabeceraPS7("LMPL","NMPL",21,""));
		tramaEntrada.append( rellenar(numeroTarjeta, 16,'0','I'));
		tramaEntrada.append( rellenar(indImpresion, 1,'0','I'));
		tramaEntrada.append( rellenar(CANAL_ENLACE, 4,'0','I'));
		EIGlobal.mensajePorTrace("TrxLMPLBO: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
		
	}
	
	/**
	 * Metodo que desetrama la respuesta de la Trx LMPL
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param tramaRespuesta cadena con la respuesta de la Trx LMPL
	 * @param tramaEntrada cadena con trama Entrada para la trx
	 * @return boolean operacion exitosa o fallida
	 */
	private boolean desentramaRespuesta(String tramaRespuesta, StringBuffer tramaEntrada){
		boolean continua = false;
		if(tramaRespuesta.indexOf("LMA0019")!=-1 )  {
			EIGlobal.mensajePorTrace("OPERACION EXITOSA", EIGlobal.NivelLog.INFO);
			procesaEstatus(0, " OPERACION EXITOSA", tramaEntrada);
			continua = true;			
		}
		else if( tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO")>-1 ) {
			EIGlobal.mensajePorTrace("NO HAY TERMINALES 390 PARA ATENDER LA PETICION", EIGlobal.NivelLog.INFO);
			procesaEstatus(99, " NO HAY TERMINALES 390 PARA ATENDER LA PETICION", tramaEntrada);
			
		} else if( tramaRespuesta.indexOf("ABEND CICS")>-1 || tramaRespuesta.indexOf("failed with abend")>-1 ) {
			EIGlobal.mensajePorTrace("ABEND CICS", EIGlobal.NivelLog.INFO);
			procesaEstatus(99, " " + traeCodError(tramaRespuesta), tramaEntrada);
			
		} else if("".equals(tramaRespuesta.trim())) {
			EIGlobal.mensajePorTrace("Error 99 TIMEOUT: SIN RESPUESTA 390", EIGlobal.NivelLog.INFO);
			procesaEstatus(99, " TIMEOUT: SIN RESPUESTA 390", tramaEntrada);
		
		} else if(tramaRespuesta.indexOf("LME0110")>-1) {
			EIGlobal.mensajePorTrace("Error 110 CAMPO DEBE SER INFORMADO", EIGlobal.NivelLog.INFO);
			procesaEstatus(110, " CAMPO DEBE SER INFORMADO", tramaEntrada);
		
		} else if(tramaRespuesta.indexOf("LME0151")>-1) {
			EIGlobal.mensajePorTrace("Error 151 INDICADOR INVALIDO SOLO S,N", EIGlobal.NivelLog.INFO);
			procesaEstatus(151, " INDICADOR INVALIDO SOLO S,N", tramaEntrada);
			
		} else if(tramaRespuesta.indexOf("LME0199")>-1){
			EIGlobal.mensajePorTrace("Error 199 TARJETA CON INDICADOR INVALIDO PARA PAPERLESS", EIGlobal.NivelLog.INFO);
			procesaEstatus(199, " TARJETA CON INDICADOR INVALIDO PARA PAPERLESS", tramaEntrada);
			
		}	
		  else if(tramaRespuesta.indexOf("ERMPE0013") > -1) 
		  {
			  EIGlobal.mensajePorTrace("Error ERMPE0013 NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION", EIGlobal.NivelLog.INFO);
			  procesaEstatus(12, " NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION", tramaEntrada);
				
			}				
						
		 else if(tramaRespuesta.indexOf("ERMPE0128") > -1) 
		 {
			 EIGlobal.mensajePorTrace("Error ERMPE0128  OPCION  ERRONEO. VALORES PERMITIDOS 1/2", EIGlobal.NivelLog.INFO);
			 procesaEstatus(11, " OPCION  ERRONEO. VALORES PERMITIDOS 1/2", tramaEntrada);
		 }
		 else if(tramaRespuesta.indexOf("LME0198")>-1)
		 {
		     EIGlobal.mensajePorTrace("Error LME0198 NO EXISTE REGISTRO EN TABLA", EIGlobal.NivelLog.INFO);			
			 procesaEstatus(198, " NO EXISTE REGISTRO EN TABLA", tramaEntrada);
		 }
		
		return continua;
	}
	
	/**
	 * Metodo para realizar la extraccion del Estatus sobre el resultado de la consulta.
	 * @since 01/03/2015
	 * @author FSW-Indra
	 * @param codigoStatus de tipo int
	 * @param msgError de tipo String
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void procesaEstatus(int codigoStatus, String msgError, StringBuffer tramaEntrada) {
		codStatus = codigoStatus;
		msgStatus = "LMPL" + rellenar(String.valueOf(this.codStatus), 4, '0', 'I') + msgError;
		EIGlobal.mensajePorTrace("codStatus-->: [" + codStatus + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("msgStatus-->: [" + msgStatus + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para obtener el Codigo de Error sobre la respuesta de la LMPL.
	 * @since 01/03/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo String
	 * @return stringbuffer de tipo stringbuffer.toString
	 */
	private String traeCodError(String tramaEntrada)
	{		
		StringBuffer stringbuffer = new StringBuffer("");
		int i = tramaEntrada.indexOf("@ER");
		if(i < 0){
			i = tramaEntrada.indexOf("Transaction LMPL failed");
		}
		if(i >= 0){
			int j = i;
			boolean continua = true;
			do{
				char c = tramaEntrada.charAt(j);
				if(c < ' ' || c > '~'){
					continua = false;
				} else if(j == (tramaEntrada.length()-1)){
					continua = false;
				}
				if( continua ){
					stringbuffer = stringbuffer.append(String.valueOf(tramaEntrada.charAt(j)));
					j++;
				}					
			}while(continua);
		} else {
			stringbuffer = stringbuffer.append("LMPL0000 Ejecucion Exitosa");
		}
		return stringbuffer.toString();
	}	
	
	/**
	 * getMsgStatus de tipo String.
	 * @author FSW-Indra
	 * @return msgStatus de tipo String
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * setMsgStatus para asignar valor a msgStatus.
	 * @author FSW-Indra
	 * @param msgStatus de tipo String
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * getCodStatus de tipo int.
	 * @author FSW-Indra
	 * @return codStatus de tipo int
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * setCodStatus para asignar valor a codStatus.
	 * @author FSW-Indra
	 * @param codStatus de tipo int
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

	/**
	 * Metodo que obtiene el numero de tarjeta
	 * @author FSW-Indra
	 * @return String numeroTarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * Metodo que asigna el numero de tarjeta
	 * @author FSW-Indra
	 * @param numeroTarjeta a asignar
	 */
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	/**
	 * Obtiene el ind de impredion
	 * @author FSW-Indra
	 * @return String indImpresion
	 */
	public String getIndImpresion() {
		return indImpresion;
	}

	/**
	 * Metodo que asigna el ind impresion
	 * @author FSW-Indra
	 * @param indImpresion a asignar
	 */
	public void setIndImpresion(String indImpresion) {
		this.indImpresion = indImpresion;
	}

	/**
	 * Obtiene el resultado de la operacion
	 * @return boolean resultadoOperacion
	 */
	public boolean isResultadoOperacion() {
		return resultadoOperacion;
	}

	/**
	 * Asigna el resultado de la operacion
	 * @param resultadoOperacion a asignar
	 */
	public void setResultadoOperacion(boolean resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}
}