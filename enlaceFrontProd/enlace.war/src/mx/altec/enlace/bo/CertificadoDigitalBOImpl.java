/**
 ******************************************************************************
 * 
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CertificadoDigitalBOImpl.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	  By 		Company 	     Description
 * ======= =========== ========= =============== ==============================
 * 1.0 	    21/10/2013 FSW-Indra  Indra Company      Creacion
 *
 ******************************************************************************
 **/
package mx.altec.enlace.bo;


import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

import mx.altec.enlace.beans.CertificadoDigitalBean;
import mx.altec.enlace.dao.CertificadoDigitalDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import com.mx.isban.certificados.service.canal.GestorCertificadosCanal;
import com.mx.isban.certificados.service.cliente.GestorCertificadosCliente;
import com.mx.isban.certificados.ws.canal.DTOLlaveCanal;
import com.mx.isban.certificados.ws.cliente.DTOCertificado;
import com.mx.isban.crypto.util.AdminCertificate;

import com.mx.isban.certificados.service.exception.GestorCertificadosException;

public class CertificadoDigitalBOImpl implements CertificadoDigitalBO{
	
	
	/**
	 * Consulta de Certificado Digital
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @param valida : indica si se va a validar el certificado
	 * @return new CertificadoDigitalBean : Certificado Digital
	 * @throws BusinessException Excepcion generada
	 */
	public CertificadoDigitalBean consultaCertificado(String canal, String cliente, String contrato, boolean valida)throws BusinessException{
		EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl >>>>>>>>>>>>>>>>", EIGlobal.NivelLog.INFO);
		
		CertificadoDigitalBean datosCert = new CertificadoDigitalBean(); 
		GestorCertificadosCliente gestor = new GestorCertificadosCliente();
		DTOCertificado cert = null;
		try{
			cert = gestor.consultaCertificado(canal, cliente, contrato, valida);
		}catch(GestorCertificadosException e){
			EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl --> consultaCertificado ::: EXCEPCION" + e.getMessage(), EIGlobal.NivelLog.INFO);
			throw new BusinessException(e);
		}
		if(cert != null){
			datosCert.setExiste(!cert.isVacio());
			if(!cert.isVacio()){
				Date d = cert.getFechaCaducidad().toGregorianCalendar().getTime();
				DateFormat df = DateFormat.getDateInstance();
				String s = df.format(d);
				datosCert.setVigenciaCertificado(s);
				datosCert.setNumeroDeSerie(cert.getIdCertificado());
				datosCert.setCertificado(cert.getCertificado());
				datosCert.setEstatus(cert.getEstatusCertificado());
			}
		}
		EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl --> consultaCertificado ::: " +cert, EIGlobal.NivelLog.INFO);
		
		return datosCert;

	}


	/**
	 * Alta de Certificado Digital
	 * @param certificado : CertificadoDigitalBean : Certificado Digital
	 * @param canal : canal del cual se realiza la peticion
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @return boolean resultado 
	 * @throws GestorCertificadosException Excepcion generada
	 */
	public boolean altaCertificado(CertificadoDigitalBean certificado, String canal, String cliente, String contrato)throws GestorCertificadosException {
		EIGlobal.mensajePorTrace(">>>>>>>>>>>  CertificadoDigitalBOImpl --> altaCertificado ::: " , EIGlobal.NivelLog.INFO);
		GestorCertificadosCliente gestor = new GestorCertificadosCliente();
		boolean resultado = false, existe = false;
 
		resultado = gestor.altaCertificado(existe, canal, cliente , contrato, certificado.getCertificado());

		return resultado;		
	}

	/**
	 * Revocar Certificado Digital
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @return boolean resultado 
	 * @throws BusinessException Excepcion generada
	 */
	public boolean revocaCertificado(String canal, String cliente, String contrato)throws BusinessException {
		EIGlobal.mensajePorTrace(">>>>>>>>>>>  CertificadoDigitalBOImpl --> revocaCertificado ::: " , EIGlobal.NivelLog.INFO);
		GestorCertificadosCliente gestor = new GestorCertificadosCliente();
		boolean resultado = false;
		
		try{
			resultado = gestor.revocaCertificado(canal, cliente , contrato);
		}catch(GestorCertificadosException e){
			EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl --> revocaCertificado ::: EXCEPCION" + e.getMessage(), EIGlobal.NivelLog.INFO);
			throw new BusinessException(e);
		}
		return resultado;
	}
	
	/**
	 * Consulta de Certificado Digital del Canal
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @return new byte[] : Certificado Digital Canal
	 * @throws CertificateEncodingException Excepcion generada
	 * @throws BusinessException Excepcion generada
	 */
	public byte[] consultaCertificadoCanal(String canal) throws CertificateEncodingException, BusinessException{
		EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl >>>>>>>>>>>>>>>>", EIGlobal.NivelLog.INFO);
		
		GestorCertificadosCanal gestor = new GestorCertificadosCanal();
		DTOLlaveCanal cert = new DTOLlaveCanal();
		AdminCertificate getCert = new AdminCertificate();
		PublicKey publicKey  = null;
		X509Certificate clientCertificate  = null;
		
		try{
			cert = gestor.getLlaveCanal(canal);
		
		}catch(GestorCertificadosException e){
			EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl --> consultaCertificadoCanal ::: EXCEPCION" + e.getMessage(), EIGlobal.NivelLog.INFO);
			throw new BusinessException(e);
		}
		
		if(cert != null){
			clientCertificate  = getCert.getCertificate(cert.getLlaveCanal());
			publicKey = clientCertificate.getPublicKey();
		}
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> CertificadoDigitalBOImpl --> Certificado ::: Public Key Channel Certificate " + publicKey, EIGlobal.NivelLog.INFO);
		
		return clientCertificate.getEncoded();

	}
	
	/** {@inheritDoc}*/ 
	public boolean tieneServicioImportacion(String contrato, int idFlujo) throws SQLException {
		CertificadoDigitalDAO dao = new CertificadoDigitalDAO();
		return dao.tieneServicioImportacion(contrato, idFlujo);
	}


	/** {@inheritDoc}*/ 
	public boolean servicioActivo() throws SQLException {
		CertificadoDigitalDAO dao = new CertificadoDigitalDAO();
		return dao.servicioActivo();
	}
	

}
