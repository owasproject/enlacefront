/**
 ******************************************************************************
 * 
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CertificadoDigitalBO.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	  By 		Company 	     Description
 * ======= =========== ========= =============== ==============================
 * 1.0 	    21/10/2013 FSW-Indra  Indra Company      Creacion
 *
 ******************************************************************************
 **/
package mx.altec.enlace.bo;

import java.security.cert.CertificateEncodingException;
import java.sql.SQLException;

import mx.altec.enlace.beans.CertificadoDigitalBean;

import com.mx.isban.certificados.service.exception.GestorCertificadosException;

public interface CertificadoDigitalBO {
	
	/**
	 * Alta de Certificado Digital
	 * @param certificado : CertificadoDigitalBean : Certificado Digital
	 * @param canal : canal del cual se realiza la peticion
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @return boolean resultado 
	 * @throws GestorCertificadosException Excepcion generada
	 */
	public boolean altaCertificado(CertificadoDigitalBean certificado, String canal, String cliente, String contrato)throws GestorCertificadosException;
	
	/**
	 * Consulta de Certificado Digital del Canal
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @param valida : valida el certificado
	 * @return CertificadoDigitalBean : Certificado Digital Canal
	 * @throws BusinessException Excepcion generada
	 */
	public CertificadoDigitalBean consultaCertificado(String canal, String cliente, String contrato, boolean valida)throws BusinessException;
	
	/**
	 * Revocar Certificado Digital
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @param cliente : cliente que revoca certificado
	 * @param contrato : contrato del cliente
	 * @return boolean resultado 
	 * @throws BusinessException Excepcion generada
	 */
	public boolean revocaCertificado(String canal, String cliente, String contrato)throws BusinessException;
	
	/**
	 * Consulta de Certificado Digital del Canal
	 * @param canal : canal de la aplicación que revoca el certificado
	 * @return new byte[] : Certificado Digital Canal
	 * @throws CertificateEncodingException Excepcion generada
	 * @throws BusinessException Excepcion generada
	 */
	public byte[] consultaCertificadoCanal(String canal)throws CertificateEncodingException, BusinessException;
		
	
	/**
	 * Metodo que valida si el contrato tiene servicio de importacion
	 * @param contrato numero de contrato
	 * @param idFlujo idFlujo
	 * @return boolean true si tiene servicio
	 * @throws SQLException exception
	 */
	public boolean tieneServicioImportacion(String contrato, int idFlujo) throws SQLException;
	
	/**
	 * Metodo que valida si el servicio de cifrado esta activo
	 * @return boolean true si tiene servicio
	 * @throws SQLException exception
	 */
	public boolean servicioActivo() throws SQLException;

}
