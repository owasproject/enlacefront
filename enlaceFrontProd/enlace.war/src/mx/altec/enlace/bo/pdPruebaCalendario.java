package mx.altec.enlace.bo;

import java.util.*;
import java.text.*;

public class pdPruebaCalendario {

    public static void main (String args[]) {
	ArrayList p = new ArrayList ();
	GregorianCalendar festivo = new GregorianCalendar (2002, 4, 13);
	p.add (festivo);
	SimpleDateFormat sdFormat = new SimpleDateFormat ("dd/MM/yyyy");
	System.out.println ("Dia festivo: ---- " + sdFormat.format (festivo.getTime ()));
	System.out.println ("----- Antes de pedir la fecha de liberacion -----");
	GregorianCalendar fechaLib = pdCalendario.getSigDiaHabil (new GregorianCalendar (), p);
	System.out.println ("----- Despues de pedir la fecha de liberacion: " + sdFormat.format (fechaLib.getTime ()));
	System.out.println ("----- Antes de pedir el calendario -----");
	ArrayList cal = pdCalendario.getCalendario (new GregorianCalendar (), p);
	System.out.println ("Despues de pedir el calendario: " + cal);
	ListIterator liMeses = cal.listIterator ();
	ListIterator Dias;
	pdMes Mes;
	pdDiaHabil Dia;
	while (liMeses.hasNext ()) {
	    Mes = (pdMes) liMeses.next ();
	    System.out.println (Mes.getMes () + " " + Mes.getAnio ());
	    Dias = Mes.getDias ().listIterator ();
	    while (Dias.hasNext ()) {
		Dia = (pdDiaHabil) Dias.next ();
		System.out.print ("--" + Dia.getDia () + " " + Dia.getHabil () + " " + Dia.getNomDia (Dia.getNomDia ()));
	    }
	    System.out.println ();
	}
	GregorianCalendar hoy = new GregorianCalendar ();
	hoy.set (GregorianCalendar.MONTH, hoy.get (GregorianCalendar.MONTH) + 2);
	GregorianCalendar fechaLimite = pdCalendario.getAntDiaHabil (hoy, p);
    }
}
