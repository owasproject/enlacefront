package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;


public class Multiples {

      public Multiples () {
	}

//      public void dtLee ( String tipo, String persona) throws IOException {
        public Vector dtLee (String Archivo, String tipo) throws IOException {
			    char Tipo = tipo.charAt (0);
                String registroLeido = "";
                String campo = "";
				String nom = "";
                String patape = "";
                String materape = "";
				String temp = "";
				String cadaux="";
				String espa="";
				String punto="";
				Vector lista = new Vector();
			    RandomAccessFile fileAmbiente=null;

                try {
                        File pruebas = new File(Archivo);
						System.out.println(pruebas.getPath());
			            fileAmbiente= new RandomAccessFile(pruebas,"r");
                } catch ( IOException e ) {
                        System.out.println( "Error al leer el archivo de ambiente, e1" + e);
                }


                do {
                        try {
                                registroLeido = fileAmbiente.readLine();
                        } catch ( IOException e ) {
                                System.out.println( "Error al leer el archivo de ambiente, e2" + e);
                        }
                } while ( registroLeido != null && registroLeido.charAt (0) != Tipo  );


                try {
                        do {
                                if ( registroLeido.startsWith( tipo ) ) {
                                        if (tipo != "1") {
                                                campo = registroLeido.substring(
                                                        registroLeido.indexOf( ';' , 2 ) + 1 ,
                                                        registroLeido.lastIndexOf( ';' ) );
                                                lista.add(campo);
                                        } else {

                                                campo = registroLeido.substring(
                                                        registroLeido.indexOf( ';' , 12 ) + 1 ,
                                                        registroLeido.lastIndexOf( ';' ) - 2);

												for(int i=0; i<campo.length(); i++){
													while(campo.charAt(i) == ';'){
														campo=replace(campo,';', ": ");
													}
												}


												//System.out.println("cadena" + campo);
												//campo = campo.replace(';',' ');
                                                StringTokenizer st = new StringTokenizer(campo, ":");
													   while (st.hasMoreTokens()) {

														   nom = st.nextToken();
                                                           patape = st.nextToken();
                                                           materape  = st.nextToken();
														  // Multi mult = new Multi (nom, patape, materape);

														   //System.out.println("Nombre: " + nom + "  ApellidoP: " + patape + "  Apellido: " + materape);
                                                            lista.add(nom);
                                                       }
                                        }
                                }
                        } while ( ( registroLeido = fileAmbiente.readLine() ) != null );
                } catch ( IOException e ) {
                                System.out.println( "Error al leer los registros del archivo de ambiente" );
                }



				try {
                        fileAmbiente.close();
                } catch ( IOException e ) {
                        System.out.println( "Error al cerrar el archivo de ambiente" );
                }
				return(lista);

        }

	public String replace(String fuente,char oldChar,String newString){
		String toReturn="";
		int index=fuente.indexOf(oldChar);

		if (index!=-1){
		if (index!=0)
		toReturn+=fuente.substring(0,index);
		toReturn+=newString;
		toReturn+=fuente.substring(index+1,fuente.length());
		}
		return toReturn;
	}


}

