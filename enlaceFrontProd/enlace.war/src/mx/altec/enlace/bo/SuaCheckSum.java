package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;

//####################################################################
//
//      Modificacion: Cambio el algoritmo de checksum por modulo 9
//      Proyecto    : SUA 2005
//      Cod         : MX2005-21100
//      Programador : Carlos Aleman Rosas.
//		clave en cod: W3xx
//####################################################################

public class SuaCheckSum
{
	static int chksumSize   = 19;
	static int chksumModulo = 10;
	static int chksumModuloW3 = 9; //W3xx
	static int chksumOffset = 120;

	int    chksumLine[];
	int    chksumRead[];
	int    chksumResult;
	int    chksumIndex;
	int    chksumCount;
	String chksumObject;
	String chksumSource;

	boolean enProceso;
	boolean SuaCheckReturn;

	public SuaCheckSum()
	{
		chksumLine     = new int[chksumSize];
		chksumRead     = new int[chksumSize];
		chksumResult   = 0;
		chksumIndex    = 0;
		chksumCount    = 0;
		enProceso      = true;
		SuaCheckReturn = false;
		chksumSource   = "";
		chksumObject   = "";
	}

	public boolean aplicarCheckSum(String registro)
	{
		int    tipoRegistro;
		int    indice;
		int    modulo;
		int    ascii;
		char   dato;
		String swap;

		EIGlobal.mensajePorTrace( "***SuaCheckSum.class # aplicarCheckSum #", EIGlobal.NivelLog.INFO);

		try
		{
			tipoRegistro = Integer.parseInt(registro.substring(0, 2));

			EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Tipo Registro = >" +Integer.toString(tipoRegistro)+ "<", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Recorriendo registro #", EIGlobal.NivelLog.INFO);

			for(indice=1; indice<=295; indice++)
			{
				dato = registro.charAt(indice-1);
				ascii = (int)dato;

				//EIGlobal.mensajePorTrace( "***SuaCheckSum.class # DATO " + dato + " ASCII " + Integer.toString(ascii) + " #", EIGlobal.NivelLog.INFO);
				if(!((tipoRegistro==6)&&(indice==121)))
				{
					if(((ascii>=48)&&(ascii<=57))||((ascii>=65)&&(ascii<=90)))
					{
						modulo = (chksumIndex + ascii) % chksumSize;
						chksumLine[modulo]= ( chksumLine[modulo] + ascii) % chksumModulo;
						//chksumResult = (chksumResult + ascii) % chksumModulo;
						chksumResult = (chksumResult + ascii) % chksumModuloW3; //W3xx
						chksumIndex  = (chksumIndex + 1) % chksumSize;
						if(chksumIndex==0)
						{
							chksumLine[chksumCount]= ( chksumLine[chksumCount] + chksumResult ) % chksumModulo;
							chksumCount = (chksumCount+1) % chksumSize;
						}
					}
				} else {
					enProceso = false;
					break;
				}
			}


			  EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Termina recorrer registro CheckSum #", EIGlobal.NivelLog.INFO);

              if(!enProceso)
              {
                  for(indice=0; indice<chksumSize; indice++)
                  {
                      dato = registro.charAt(indice + chksumOffset);
                      ascii = Character.digit(dato, 10);
                      chksumRead[indice] = ascii;
                  }

                  for(indice=0; indice<chksumSize; indice++)
                  {
                       swap = Integer.toString(chksumLine[indice]);
                       swap.trim();
                       chksumSource = chksumSource.concat(swap);
                       swap = Integer.toString(chksumRead[indice]);
                       swap.trim();
                       chksumObject = chksumObject.concat(swap);
                  }

	              EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Objecto Leido" + chksumObject + "	#", EIGlobal.NivelLog.INFO);
				  EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Objeto Source" + chksumSource + " #", EIGlobal.NivelLog.INFO);

                  SuaCheckReturn = chksumObject.equals(chksumSource);
              }

          }catch(Exception E)
			{
	  		  EIGlobal.mensajePorTrace( "***SuaCheckSum.class Excepcion %aplicarCheckSum() >> "+ E.getMessage() + " <<", EIGlobal.NivelLog.INFO);
			}

	  EIGlobal.mensajePorTrace( "***SuaCheckSum.class # Termina aplicarCheckSum #", EIGlobal.NivelLog.INFO);

      return enProceso;
    }
}
