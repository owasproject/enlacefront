package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.util.ArrayList;
import java.util.Hashtable;

import mx.altec.enlace.beans.AdmLyMGL28;
import mx.altec.enlace.dao.AdmLyMValidacionDAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Util;

public class LYMValidador {

	private MQQueueSession mqsess;
	private static Hashtable<String, String> camposOperacion;
	private static Hashtable<String, String> camposResultado;
	private static ArrayList<String> opImpSinDec;

	static {
		camposOperacion = new Hashtable<String, String>();
		camposOperacion.put("TRAN", "10");	//cuando es archivo se valida desde servlet
		camposOperacion.put("TRTJ", "10");
		camposOperacion.put("PROG", "12");
		camposOperacion.put("PAGT", "10");
		camposOperacion.put("OCUR", "6,3");
		camposOperacion.put("DIBT", "13");	//cuando es archivo se valida desde servlet
		camposOperacion.put("DIPD", "6,7");
		camposOperacion.put("DITA", "6,7");
		camposOperacion.put("VTSI", "10");
		camposOperacion.put("CPSI", "10");
		camposOperacion.put("CPID", "8");
		camposOperacion.put("CPDI", "8");
		camposOperacion.put("PD15", "6,3");
		camposOperacion.put("RG03", "11");
		//camposOperacion.put("TICO", "0");	no existe en el front-end
		//camposOperacion.put("TIDI", "0");
		camposOperacion.put("SATP", "8");
		camposOperacion.put("PILC", "10");
		camposOperacion.put("RE03", "6,3");
		camposOperacion.put("SULC", "0");	//no implementado hasta que SULC se instale en Producción
		camposOperacion.put("IN04", "6,3");	//se llama desde servlet, no desde ServicioTux
		camposOperacion.put("PNIS", "6,3"); //se llama desde servlet, no desde ServicioTux
		camposOperacion.put("PNOS", "6,3"); //se llama desde servlet, no desde ServicioTux
		camposOperacion.put("INTE", "7");
		camposOperacion.put("INGA", "6,3");
		camposOperacion.put("INGC", "6,3");
		camposOperacion.put("AAOP", "6,3");	//ojo
		camposOperacion.put("ALOP", "6,4");
		camposOperacion.put("CFRP", "6,3");
		camposOperacion.put("SARA", "8");


		camposResultado = new Hashtable<String, String>();
		camposResultado.put("OCUR", "1");	//ojo
		camposResultado.put("SATP", "2");	//ojo

		opImpSinDec = new ArrayList<String>();
		opImpSinDec.add("SARA");
		opImpSinDec.add("INGA");
	}
	public LYMValidador() {
		try {
    		mqsess = new MQQueueSession(
    				NP_JNDI_CONECTION_FACTORY,
    				NP_JNDI_QUEUE_RESPONSE,
    				NP_JNDI_QUEUE_REQUEST);
		} catch (Exception x) {
		    EIGlobal.mensajePorTrace("LYMValidador, Error al abrir: [" + x +  "]", EIGlobal.NivelLog.ERROR);
    	}
	}


	public void cerrar() {
		try {
			mqsess.close();
		}catch(Exception x) {
			EIGlobal.mensajePorTrace("LYMValidador, Error al cerrar: [" + x +  "]", EIGlobal.NivelLog.ERROR);		}
}

	public String validaLyM(String tramaRedsrvr) {
		String trama[] = null;
		String patronBsq = "";
		String importe = "";
		try {
			EIGlobal.mensajePorTrace("LYMValidador - tramaRedsrvr:[" + tramaRedsrvr + "]", EIGlobal.NivelLog.INFO);
			trama = tramaRedsrvr.split("\\|");
			EIGlobal.mensajePorTrace("LYMValidador - trama:[" + trama.toString() + "]", EIGlobal.NivelLog.INFO);
			/*
			 * trama[1] usuario
			 * trama[2] cveOpe
			 * trama[3] contrato
			 */
			trama[1] = BaseServlet.convierteUsr7a8(trama[1]);
			EIGlobal.mensajePorTrace("LYMValidador - Usuario: [" + trama[1]+ "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LYMValidador - CveOpe:  [" + trama[2]+ "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LYMValidador - Contrato:[" + trama[3]+ "]", EIGlobal.NivelLog.INFO);

			patronBsq = camposOperacion.get(trama[2]);
			EIGlobal.mensajePorTrace("LYMValidador - patronBsq:[" + patronBsq + "]", EIGlobal.NivelLog.INFO);

			if(patronBsq == null) {
				return "ALYM0000";	//operacion no valida LyM;
			}
			else if (patronBsq.indexOf(',') > -1) {	//trama compleja, buscar con "@"
				String idx[] = patronBsq.split(",");
				importe = trama[Integer.parseInt(idx[0])].split("@")[Integer.parseInt(idx[1])];
			}
			else {

				importe = trama[Integer.parseInt(patronBsq)];
			}
			if(opImpSinDec.contains(trama[2])) {
				importe = Util.formateaImporte(importe);
			}
			importe = Util.rellenaCeros(importe, 17);
			EIGlobal.mensajePorTrace("Importe:[" + importe + "]", EIGlobal.NivelLog.INFO);
			AdmLyMValidacionDAO dao = new AdmLyMValidacionDAO();
			dao.setMqsess(mqsess);
			AdmLyMGL28 valida = dao.validarLyM(trama[3], trama[1], trama[2], importe);
			if(valida.isCodExito()) {
				return "ALYM0000";
			}
			else { // Validar variacion de respuesta en clave
				String patronRst = camposResultado.get(trama[2]);
				EIGlobal.mensajePorTrace("LYMValidador - patronRst:[" + patronRst + "]", EIGlobal.NivelLog.INFO);
				String codError = "";
				if(patronRst == null) {
					codError = "ALYM";
					codError += valida.getCodigoOperacion().substring(3);
					codError += "       ";//espacio para la referencia
					codError += valida.getMensError();
				} else if (patronRst.equals("1")) { //Pago Ocurre
					codError = "ERROR ";
					codError += "ALYM";
					codError += valida.getCodigoOperacion().substring(3) + ": ";
					//codError += "       ";//espacio para la referencia
					codError += valida.getMensError();
				} else if (patronRst.equals("2")) {
					codError = "ALYM";
					codError += valida.getCodigoOperacion().substring(3);
					codError += " ";
					codError += valida.getMensError();
				}
				EIGlobal.mensajePorTrace("LYMValidador - CodError:[" + codError + "]", EIGlobal.NivelLog.INFO);
				return codError;
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("LYMValidador - Error controlado al validar limites:[" + e + "]",
					EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("LYMValidador - tramaRedsrvr:[" + tramaRedsrvr + "]", EIGlobal.NivelLog.ERROR);
			if (trama != null) {
				EIGlobal.mensajePorTrace("LYMValidador - trama:[" + trama.toString() + "]", EIGlobal.NivelLog.ERROR);
				if (trama.length >= 3) {
					EIGlobal.mensajePorTrace("LYMValidador - Contrato:[" + trama[3]+ "]", EIGlobal.NivelLog.ERROR);
				} else if (trama.length >= 2) {
					EIGlobal.mensajePorTrace("LYMValidador - CveOpe:  [" + trama[2]+ "]", EIGlobal.NivelLog.ERROR);
				} else if (trama.length >= 1) {
					EIGlobal.mensajePorTrace("LYMValidador - Usuario: [" + trama[1]+ "]", EIGlobal.NivelLog.ERROR);
				}
			}
			EIGlobal.mensajePorTrace("LYMValidador - patronBsq:[" + patronBsq+ "]", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("Importe:[" + importe + "]", EIGlobal.NivelLog.ERROR);
			return "ALYM0000";	//operacion no valida LyM;
		}
	}


	/*
	 * Valida Limites y Montos a partir de los parametros directos para la transaccion
	 */

	public String validaLyM(String contrato, String usr,
			String clvOperacion, String importe) {

		String usuario = BaseServlet.convierteUsr7a8(usr);
		EIGlobal.mensajePorTrace("LYMValidador - validaLyMsinTrama (contrato:[" + contrato + "], " +
				"usuario:[" + usuario + "], clvOperacion:[" + clvOperacion + "], " +
				"importe:[" + importe+ "])", EIGlobal.NivelLog.DEBUG);

		if(opImpSinDec.contains(clvOperacion)) {
			importe = Util.formateaImporte(importe);
		}
		importe = Util.rellenaCeros(importe, 17);

		EIGlobal.mensajePorTrace("LYMValidador - validaLyMsinTrama - Importe:[" +
				importe + "]", EIGlobal.NivelLog.DEBUG);

		AdmLyMValidacionDAO dao = new AdmLyMValidacionDAO();
		dao.setMqsess(mqsess);
		AdmLyMGL28 valida = dao.validarLyM(contrato, usuario, clvOperacion, importe);
		if(valida.isCodExito()) {
			return "ALYM0000";
		} else { // Validar variacion de respuesta en clave
			String patronRst = camposResultado.get(clvOperacion);
			EIGlobal.mensajePorTrace("patronRst:[" + patronRst + "]", EIGlobal.NivelLog.DEBUG);
			String codError = "";
			if(patronRst == null) {
				codError = "ALYM";
				codError += valida.getCodigoOperacion().substring(3);
				codError += "       ";//espacio para la referencia
				codError += valida.getMensError();
			} /*else if (patronRst.equals("1")) { // Validar respuestas específicas
			} */

			EIGlobal.mensajePorTrace("LYMValidador - validaLyMsinTrama - " +
					"codError:[" + codError + "]", EIGlobal.NivelLog.DEBUG);

			return codError;
		}
	}


}