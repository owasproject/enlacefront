/*
 * ArchivoCfmg.java
 *
 * Created on 22 de enero de 2003, 05:43 PM
 */
/********************************************************************************************************/
/*Modificación:                                                                                         */
/*             Incidencia:IM19625                                                                       */
/*             Programador: Sergio A. Lopez Flores                                                      */
/*             Fecha: 2/12/2003                                                                         */
/********************************************************************************************************/

/*I05-0382520 Praxis
 *ivn Ma. Isabel Valencia Navarrete
 *Se modifica en la funcion leeImportacion(S,H,H) el uso de la variable errores
 *y errorLinea para mostrar en el front el error en la linea correcta*/

package mx.altec.enlace.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * @author  Rafael Martinez Montiel
 */
public class ArchivoCfmg extends BaseServlet implements Serializable
	{

	// Constantes para identificar el estado del archivo
	public static final int SIN_ENVIAR = 0;
	public static final int ENVIADO = 1;
	public static final int RECUPERADO = 2;
	public static final int IMPORTADO = 3;
	public static final int tamConcepto = 40;
	// ---
	private int estado = 0;
	private String nombre = "";
	private String numTransmision = "";
	private String fechaTransmision = "";
	private String folio_transmision = "";
	private long totalRegistros = 0;
	private long aceptados = 0;
	private long rechazados = 0;
	private long paraTransmitir = 0;
	private String importeTotal = "0";
	private String importeFacturasOtros = "0";
	private int numLineas = 0;
// -----------------------------------------------------------------------------------------
	private Vector doc = new Vector();
	//-------------- ??? OJO: son public
	public static String totImp = "";
	public static String totImpFac = "";
	public static int numLinea = 0;

// -----------------------------------------------------------------------------------------
	// jgarcia - 06/Oct/2009
	// Nuevos campos en la pantalla de alta de pagos. Tambien
	// se crearon sus respectivos metodos set y get.
	private double impAceptado;
	private double impRechazado;
	private double impTransmitir;


	/** Constructor */
	public ArchivoCfmg() {super();}

	// ----- Métodos get y set -------------------------------------------------
	public int getEstado() {return estado;}
	public void setEstado(int valor) {estado = valor;}

	public String getNombre() {return this.nombre;}
	public void setNombre (String nombre) {this.nombre = nombre;}

	public String getNumTransmision() {return numTransmision;}
	public void setNumTransmision(String num) {numTransmision = num;}

	public String getFechaTransmision() {return this.fechaTransmision;}
	public void setFechaTransmision(String fechaTransmision) {this.fechaTransmision = fechaTransmision;}

	public String getFolio_transmision() {return this.folio_transmision;}
	public void setFolio_transmision(String folio_transmision) {this.folio_transmision = folio_transmision;}

	public long getTotalRegistros() {return this.totalRegistros;}
	public long getTotalDocumentos() {return this.doc.size();}

	public long getAceptados() {if(estado == SIN_ENVIAR) return 0L; else return this.aceptados;}

	public long getRechazados() {if(estado == SIN_ENVIAR) return 0L; else return this.rechazados;}

	public long getParaTransmitir() {if(estado != SIN_ENVIAR && estado != IMPORTADO) return 0L; else return this.totalRegistros;}

	public String getImporteTotal() {return importeTotal;}

	public String getImporteFacturasOtros() {return importeFacturasOtros;}

	public int getNumLineas() {return numLineas;}

	public String getSumaParcial() {return importeFacturasOtros;}

	/** */
	public Documento getDoc(int index)
	{
		return (Documento)doc.get(index);
	}
	/** */
	public boolean setDoc(int index, Documento nuevo)
		{
		int a, total = doc.size();
		BigDecimal impViejo;
		BigDecimal impNuevo;
		Documento viejo;

		// validaciones, y se toma el doc viejo
		if(index>doc.size() || index<0) return false;
		viejo = (Documento)doc.get(index);
		if(nuevo.gettipo() != viejo.gettipo()) return false;

		// se calculan importes (el viejo se va a restar, por eso se multiplica x 1)
		impViejo = (new BigDecimal(viejo.getimporte_neto())).multiply(new BigDecimal("-1"));
		impNuevo = new BigDecimal(nuevo.getimporte_neto());

		// si lo que se cambia es una nota...
		if((nuevo.gettipo() == nuevo.NOTA_CARGO || nuevo.gettipo() == nuevo.NOTA_ABONO))
			return false;
		   {
			Documento factura;

			if(nuevo.getnaturaleza() == 'D') impNuevo = impNuevo.multiply(new BigDecimal("-1"));
			if(viejo.getnaturaleza() == 'D') impViejo = impViejo.multiply(new BigDecimal("-1"));

			for(a=0;a<doc.size();a++)
				{
				factura=(Documento)doc.get(a);
				if(factura.getnumdocto().equals(viejo.getfactura_asociada()) &&
					factura.getclaveProv().equals(viejo.getclaveProv()) &&
					factura.gettipo() == 1)
					factura.setimporte((new BigDecimal(factura.getimporte())).add(impViejo).toString());
				if(factura.getnumdocto().equals(nuevo.getfactura_asociada()) &&
					factura.getclaveProv().equals(nuevo.getclaveProv()) &&
					factura.gettipo() == 1)
					factura.setimporte((new BigDecimal(factura.getimporte())).add(impNuevo).toString());
				}
			}

		// si lo que se cambia es una factura u otro...
		if(nuevo.gettipo()==nuevo.FACTURA || nuevo.gettipo()==nuevo.OTRO)
 		    {
			Documento nota;
			BigDecimal diferencia = impViejo.add(impNuevo);
			BigDecimal importeNeteado = diferencia.add(new BigDecimal(viejo.getimporte()));
			String numFacVieja = viejo.getnumdocto();
			String numFacNueva = nuevo.getnumdocto();
			nuevo.setimporte(importeNeteado.toString());
			importeFacturasOtros = diferencia.add(new BigDecimal(importeFacturasOtros)).toString();

			if(nuevo.gettipo()==nuevo.FACTURA)
				{
				for(a=0; a < total; a++)
					{
					nota = (Documento)doc.get(a);
					if(nota.gettipo()>=3 && nota.getfactura_asociada().equals(numFacVieja) &&
						nota.getclaveProv().equals(nuevo.getclaveProv()))
						{
						nota.setfch_venc(nuevo.getfch_venc());
						nota.setfactura_asociada(numFacNueva);
						}
					}
				}
			}

		importeTotal = (new BigDecimal(importeTotal)).add(impNuevo).add(impViejo).toString();
		impTransmitir = Double.parseDouble(importeTotal);
		doc.set(index,nuevo);
		return true;
		}

	/** */
	public boolean agregaDoc(Documento nuevo)
		{
		BigDecimal importeNuevo = null;
		try{
			importeNuevo = new BigDecimal(nuevo.getimporte_neto());
		}catch(Exception e){
			importeNuevo = new BigDecimal("0");
			EIGlobal.mensajePorTrace("ArchivoCfmg::agregaDoc:: Manejando Proceso Parser importeNuevo", EIGlobal.NivelLog.INFO);
		}

			if(nuevo.gettipo() == nuevo.NOTA_CARGO || nuevo.gettipo() == nuevo.NOTA_ABONO)
				{
				Documento factura;
				int a;
				if(nuevo.getnaturaleza() == 'D') importeNuevo = importeNuevo.multiply(new BigDecimal("-1"));

				for(a=0;a<doc.size();a++)
					if((factura=(Documento)doc.get(a)).getnumdocto().equals(nuevo.getfactura_asociada()) && factura.gettipo() == 1 && factura.getclaveProv().equals(nuevo.getclaveProv()))
						{
						factura.setimporte((new BigDecimal(factura.getimporte())).add(importeNuevo).toString());
						nuevo.setfch_venc(factura.getfch_venc());
						break;
						}
				if(a==doc.size()) return false;
				numLineas++;
				}
			else
				{
				nuevo.setimporte(nuevo.getimporte_neto());
				importeFacturasOtros = importeNuevo.add(new BigDecimal(importeFacturasOtros)).toString();
				}

			importeTotal = importeNuevo.add(new BigDecimal(importeTotal)).toString();
			impTransmitir = Double.parseDouble(importeTotal);
			totalRegistros++;
			numLineas++;
	        doc.add(nuevo);
	        EIGlobal.mensajePorTrace("Acfmg-> tamaño: " + doc.size(), EIGlobal.NivelLog.DEBUG);
		return true;
		}

	/** */
	public boolean eliminaDoc(int index)
		{
		if(index>doc.size() || index<0) return false;
		int a;
		Documento viejo = (Documento)doc.get(index);
		Documento porModificar;
		BigDecimal impViejo;

		if(viejo.gettipo()==viejo.FACTURA || viejo.gettipo()==viejo.OTRO)
			{
			// factura
			// - buscar notas y eliminarlas
			for(a=0;a<doc.size();a++)
				{
				porModificar=(Documento)doc.get(a);
				if(porModificar.getfactura_asociada().equals(viejo.getnumdocto()) &&
					porModificar.getclaveProv().equals(viejo.getclaveProv()) &&
					porModificar.gettipo() >= 3)
					eliminaDoc(a);
				}
			impViejo = (new BigDecimal(viejo.getimporte_neto())).multiply(new BigDecimal("-1"));
			importeFacturasOtros = impViejo.add(new BigDecimal(importeFacturasOtros)).toString();
			}
		else
			{
			// nota
			// - buscar factura asociada y restar de su importe
			impViejo = new BigDecimal(viejo.getimporte_neto());
			if(viejo.getnaturaleza() == 'A') impViejo = impViejo.multiply(new BigDecimal("-1"));
			for(a=0;a<doc.size();a++)
				if((porModificar=(Documento)doc.get(a)).getnumdocto().equals(viejo.getfactura_asociada()) &&
					porModificar.gettipo() == 1 && porModificar.getclaveProv().equals(viejo.getclaveProv()))
					{
					porModificar.setimporte((new BigDecimal(porModificar.getimporte())).add(impViejo).toString());
					break;
					}
			numLineas--;
			}
		totalRegistros--;
		numLineas--;
		importeTotal = impViejo.add(new BigDecimal(importeTotal)).toString();
		impTransmitir = Double.parseDouble(importeTotal);
		doc.remove(index);
		return true;
		}

//---

	// ---
	public Vector tramasFacturas()
		{
		int a,total = doc.size();
		Documento x;
		Vector tramas = new Vector();
		for(a=0;a<total;a++)
			{
			x = (Documento)doc.get(a);
			if(x.gettipo() == x.FACTURA) tramas.add(x.getproveedor() + "|" + x.getnumdocto());
			}
		return tramas;
		}

	// ---
	public Vector tramasVencimientoFacturas()
		{
		int a,total = doc.size();
		Documento x;
		Vector tramas = new Vector();
		for(a=0;a<total;a++)
			{
			x = (Documento)doc.get(a);
			if(x.gettipo() == x.FACTURA) tramas.add(x.getproveedor() + "|" + x.getnumdocto() + "|" + x.getfch_venc());
			}
		return tramas;
		}

	/** Lee los datos del archivo especificado, el archivo es resultado una operacion de
	 *  recuperacion del servicio de tuxedo
	 */
	public void leeRecuperacion(String archivo, Hashtable claves, Hashtable nombres, HttpServletRequest req)
	{
		EIGlobal.mensajePorTrace("Entre a LeeRecuperacion", EIGlobal.NivelLog.DEBUG);

		Documento lineaDoc, facturaDoc;
		String ultLinea = "", aux;
		String factura, nota, proveedor, tipoNota;
		Vector notas = new Vector();
		BigDecimal total = new BigDecimal("0");
		int a, b, c, totalNeteo, totalDoc;

		try	{
			java.io.BufferedReader entrada = new java.io.BufferedReader(new java.io.FileReader(archivo));
			String linea = entrada.readLine();

			if(linea != null)
				fechaTransmision = linea.substring(linea.indexOf(";")+1);

			EIGlobal.mensajePorTrace("Fecha Transmision = >" +fechaTransmision+ "<", EIGlobal.NivelLog.DEBUG);
			// se leen lineas y se guardan documentos y neteos
			while((linea = entrada.readLine())!=null)
			{
				if(linea.trim().equals("")) continue;
				if(!linea.startsWith("3"))
			    {
					//System.out.println("El archivo no empieza con 3");
					lineaDoc = new Documento();
					EIGlobal.mensajePorTrace("Antes del llamado a lineaDoc.leeRecuperacion(linea)", EIGlobal.NivelLog.DEBUG);
					lineaDoc.leeRecuperacion(linea, req);
					lineaDoc.setclaveProv((String)claves.get(lineaDoc.getproveedor()));
					lineaDoc.setnombreProv((String)nombres.get(lineaDoc.getclaveProv()));
					EIGlobal.mensajePorTrace("Tipo = >" +lineaDoc.gettipo()+ "<", EIGlobal.NivelLog.DEBUG);
					if(lineaDoc.gettipo()>2)
						notas.add(lineaDoc);
					else
						agregaDoc(lineaDoc);
					if(lineaDoc.gettipo()<3)
						total = total.add(new BigDecimal(lineaDoc.getimporte()));

					EIGlobal.mensajePorTrace("Status = >" +lineaDoc.getcve_status()+ "<", EIGlobal.NivelLog.DEBUG);

					if(lineaDoc.getcve_status().equals("A")){
						aceptados++;
						impAceptado += Double.parseDouble(lineaDoc.getimporte());
					}else if(lineaDoc.getcve_status().equals("R")){
						rechazados++;
						impRechazado += Double.parseDouble(lineaDoc.getimporte());
					}
				 } else {
					aux = linea;

					for(a=0;a<2;a++)
						aux = aux.substring(aux.indexOf(";")+1);

					EIGlobal.mensajePorTrace("Sacando informacion", EIGlobal.NivelLog.DEBUG);

					proveedor = aux.substring(0,aux.indexOf(";"));
					aux = aux.substring(aux.indexOf(";")+1);
					factura = aux.substring(0,aux.indexOf(";"));
					aux = aux.substring(aux.indexOf(";")+1);
					nota = aux.substring(0,aux.indexOf(";"));

					EIGlobal.mensajePorTrace("Informacion obtenida", EIGlobal.NivelLog.DEBUG);

					for(a=0;a<2;a++)
						aux = aux.substring(aux.indexOf(";")+1);

					tipoNota = aux.substring(0,aux.indexOf(";"));
					EIGlobal.mensajePorTrace("factura: " + factura + " -- nota: " + nota + " -- proveedor: " + proveedor + " -- tipoNota: " + tipoNota, EIGlobal.NivelLog.DEBUG);
					totalNeteo = notas.size();
					lineaDoc = null;

					for(a=0;a<totalNeteo;a++)
					{
						lineaDoc = (Documento)notas.get(a);
						if(lineaDoc.getproveedor().equals(proveedor) && lineaDoc.getnumdocto().equals(nota) && tipoNota.equals("" + lineaDoc.gettipo()))
							break;
					}
					if(a==totalNeteo)
					{
						EIGlobal.mensajePorTrace(" se encontro nota ",EIGlobal.NivelLog.DEBUG);
					} else {
						EIGlobal.mensajePorTrace(" no se encontro nota ",EIGlobal.NivelLog.DEBUG);
                    }
					notas.remove(lineaDoc);
					lineaDoc.setfactura_asociada(factura);
					boolean xx = agregaDoc(lineaDoc);
					EIGlobal.mensajePorTrace(" nota agregada: " + xx, EIGlobal.NivelLog.DEBUG);
				}
				ultLinea = linea;
			}

			// se obtiene el número de transmision
			//totalNeteo = neteo.size();
			EIGlobal.mensajePorTrace(" doc.size() " + doc.size(), EIGlobal.NivelLog.DEBUG);

			totalRegistros = totalDoc = doc.size();

			EIGlobal.mensajePorTrace(" TotalRegistros " + totalRegistros, EIGlobal.NivelLog.DEBUG);

			if(totalDoc > 0)
			{
				numTransmision = ultLinea.substring(ultLinea.indexOf(";")+1);
				numTransmision = numTransmision.substring(0,numTransmision.indexOf(";"));
			}
			importeTotal = total.toString();
			estado = RECUPERADO;
			entrada.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error en ArchivoCfmg.java (1): " + e + "\n"+e.getMessage(),EIGlobal.NivelLog.ERROR);}
	}

	/** */
	public Vector leeImportacion(String archivo)
	{
		return leeImportacion(archivo, null, null, null);
	}

	/** */
	public Vector leeImportacion(String archivo, Hashtable codigosProvFiltro,  Hashtable nombresProv, HttpServletRequest req)
		{
		Vector errores = new Vector();
		Vector errorLinea = null;
		String linea;
		Documento doc;// = new Documento();
		Hashtable numDoctos = new Hashtable();
		Hashtable importeFacturas = new Hashtable();
		BigDecimal total = new BigDecimal("0");
		BigDecimal totalSinNotas = new BigDecimal("0");
		boolean existenErrores = false;
		boolean pasa;
		Hashtable tiposDoc = new Hashtable();
		Hashtable importesNotas = new Hashtable();
		Hashtable naturalezas = new Hashtable();
		String idDocumento, idFacAsociada;
		String formaPago = null;
		int i = 0;
		/*
		 * jgarcia
		 * Stefanini - Inicio
		 * Obtencion de los proveedores sin aplicar el filtro. Esta lista se utilizara para validar que el proveedor existe
		 * y la que se pasa como parametro para validar que el proveedor tiene la misma divisa que la cuenta de cargo.
		 */
		HttpSession sess = req.getSession();
		String divisaCliente = (String) sess.getAttribute("divisaProv");
		EIGlobal.mensajePorTrace("ArchivoCfmg - divisaCliente:" + divisaCliente, EIGlobal.NivelLog.DEBUG);
		servicioConfirming arch_combos = (servicioConfirming)sess.getAttribute ("arch_combos");
		EIGlobal.mensajePorTrace("Variable arc_combos:" + arch_combos.toString(),EIGlobal.NivelLog.DEBUG);
		Proveedor arch_p = new Proveedor();
		if(null == arch_combos)
			arch_combos = new servicioConfirming();
		Vector codProv = null;
		Vector cveProv = null;
		Vector nomProv = null;
		//jgarcia 14/Oct/2009 - Vector para los tipos de cuenta
		Vector vectorTipoCta = null;
		String ArchivoResp = null;
		Hashtable codigosProv = new Hashtable();
		Hashtable codigosTipoCta = new Hashtable();
		if(codigosProvFiltro == null){
			codigosProv = null;
		}else{
			try {
				ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");
				codProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 3);
				cveProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 2);
				nomProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 1);
				//jgarcia 14/Oct/2009 - Llenar el vector con los tipos de cuenta
				vectorTipoCta = arch_p.coMtoProv_Leer(ArchivoResp,"9", 4);
				if(codProv != null){
					for(int a = 0; a < cveProv.size(); a++){
						  codigosProv.put((String)cveProv.get(a),(String)codProv.get(a));
						  codigosTipoCta.put((String)cveProv.get(a),(String)codigosTipoCta.get(a));
					}
				}
			} catch(Exception e) {
				codigosProv = codigosProvFiltro;
			}
			codProv = null;
			cveProv = null;
			nomProv = null;
			vectorTipoCta = null;
		}
		/*
		 * Stefanini - Fin
		 */
		Connection conn = null;
		try
		{
			java.io.BufferedReader entrada;
			//java.io.FileWriter salida; // sobra

			/*
			primero se abre el archivo y se valida,
			si existen errores no se continua adelante,
			si todo está bien, se genera el archivo para
			mandar en el servicio de tuxedo de altas
			*/
			entrada = new java.io.BufferedReader(new java.io.FileReader(archivo));
			totalRegistros = 0;

			conn= createiASConn (Global.DATASOURCE_ORACLE );
			while((linea = entrada.readLine())!=null)
			{//while

			   doc = new Documento();
				if(linea.length() == 0)
				 continue;
// SI SON 1
				if(linea.startsWith("1"))
				{
//PRIMER LLAMADO A LEE IMPORTACION Y VA A ERROR LINEA (2)
				 errorLinea = doc.leeImportacion(linea); // regresa error si la linea comienza con 0,3,...
				 idDocumento = doc.getclaveProv() + "|" + doc.gettipo() + "|" + doc.getnumdocto();

//SI EL PROVEEDOR DECLARADO NO EXISTE EN LA LISTA SUMA ERROR A ERROR LINEA
  				if(codigosProv != null && codigosProv.get(doc.getclaveProv()) == null){
  				  errorLinea.add("La clave de proveedor no corresponde a la Divisa o no esta registrado: " + doc.getclaveProv());
  				}//jgarcia - Stefanini - Valida que el proveedor se encuentre entre los que corresponden a la divisa de cuenta de cargo
  				else if(codigosProvFiltro != null && codigosProvFiltro.get(doc.getclaveProv()) == null){
    				  errorLinea.add("La clave de proveedor no corresponde a la Divisa o no esta registrado: " + doc.getclaveProv());
  				}

  				formaPago = ObtenFormaPagoImport(conn, doc.getclaveProv());
  				if((formaPago.trim().equals("1") || formaPago.trim().equals("2")) 
  						&& !verificaFacultad("CCIMPARCHPA",req)){
  					errorLinea.add("Usuario sin facultad para el envio de pagos nacionales: " + doc.getclaveProv());
  				}

  				if(formaPago.trim().equals("5") &&
  						!verificaFacultad("CCIMPPGINTRNAL",req)){
  					errorLinea.add("Usuario sin facultad para el envio de pagos internacionales: " + doc.getclaveProv());
  				}


// SI EL DOCUMENTO NO EXISTIA
				 if(numDoctos.get(idDocumento)==null)
				 {
				  numDoctos.put(idDocumento,"");

//INICIALIZA VALORES A USAR MAS ADELANTE
				  tiposDoc.put(idDocumento,"" + doc.gettipo());
				  importesNotas.put(idDocumento,sinPuntoDecimal(doc.getimporte_neto()));
				  naturalezas.put(idDocumento,"" + doc.getnaturaleza());

				  if(errorLinea.size()==0)
				  {
// SI NO HAY ERRORES (3)
				    total = total.add(new BigDecimal(((doc.getnaturaleza() == 'D')?"-":"") + doc.getimporte_neto()));
                    totalSinNotas = totalSinNotas.add(new BigDecimal(doc.getimporte_neto()));
				  }
				  else
				   existenErrores = true;
				 }
				 else
//SI EXISTIA EL DOCUMENTO SUMA ERROR A ERROR LINEA
				  errorLinea.add("El numero de documento esta duplicado: " + doc.getnumdocto());

				}
// SI SON DIFERENTES DE 1
				else
//SI SON 3 O 2
				if(linea.startsWith("2")||linea.startsWith("3"))
				{
				 // NETEO
				 errorLinea = new Vector();
				 idDocumento = linea.substring(1,21).trim() + "|" + sinCerosAlaIzq(linea.substring(37,40)) + "|" + linea.substring(29,37).trim();
				 idFacAsociada = linea.substring(1,21).trim() + "|1|" + linea.substring(21,29).trim();

				 if(linea.length()!=62)
				 {
				  errorLinea.add("La longitud de la linea no es valida");
				  existenErrores = true;
				 }
				 else
				  if(codigosProv != null && codigosProv.get(linea.substring(1,21).trim()) == null)
				  {
				   errorLinea.add("El proveedor no esta registrado: " + linea.substring(1,21).trim());
				   existenErrores = true;
				  }
				  else //jgarcia - Stefanini - Valida que el proveedor
					  if(codigosProvFiltro != null && codigosProvFiltro.get(linea.substring(1,21).trim()) == null)
					  {
					   errorLinea.add("La Divisa de cuenta de cargo no corresponde a registro en l�nea " + linea.substring(1,21).trim());
					   existenErrores = true;
					  }
					  else
					   if(numDoctos.get(idFacAsociada) == null)
					   {
					    errorLinea.add("La factura no existe: " + linea.substring(21,24));
					    existenErrores = true;
					   }
					   else
					    if(numDoctos.get(idDocumento) == null)
					    {
					     errorLinea.add("La nota no existe: " + linea.substring(29,37));
					     existenErrores = true;
					    }
					    else
					     if(!((String)tiposDoc.get(idDocumento)).equals(sinCerosAlaIzq(linea.substring(37,40))))
					      errorLinea.add("El tipo de nota no concuerda en el neteo: " + linea.substring(37,40));
					     else
					      if(!((String)importesNotas.get(idDocumento)).equals(linea.substring(40,61).trim()))
					       errorLinea.add("El importe de la nota no concuerda con el neteo: " + linea.substring(40,61).trim());
					      else
					       if(!((String)naturalezas.get(idDocumento)).equals(linea.substring(61,62)))
						errorLinea.add("La naturaleza de la nota no concuerda con el neteo: " + linea.substring(61,62));
					       else
					        if(!numDoctos.get(idDocumento).equals(""))
						{
						 errorLinea.add("La nota tiene otra factura asociada");
						 existenErrores = true;
						}
						else
						 if(!validaImporte(linea.substring(40,61).trim()))
						 {
						  errorLinea.add("el importe no es valido: " + linea.substring(40,61).trim());
						  existenErrores = true;
						 }
						 else
						 {
						  if(!existenErrores)
						  {
						   BigDecimal impDoc = new BigDecimal(((linea.substring(61,62).equals("D"))?"-":"") + ponPuntoDecimal(linea.substring(40,61).trim()));
						   BigDecimal importe = (BigDecimal)importeFacturas.get(idDocumento);
						   if(importe == null)
						    importe = new BigDecimal("0");
						   importe = importe.add(impDoc);
						   importeFacturas.put(idDocumento,importe);
						   numDoctos.put(idDocumento,idFacAsociada);
						  }
						 }
				}else{
					errorLinea = new Vector();
					errorLinea.add("El formato de la linea no es valido.");
				}
				/*I05-0382520 se comentan las siguientes lineas
				 *errores.add(errorLinea);*/
				  if(errorLinea.size()>0)
				  existenErrores = true;

//PROCESAR MIENTRAS NO EXISTAN ERRORES
			if(!existenErrores)
			{
			  //doc = new Documento();
//			  errorLinea = new Vector();
//			  if(linea.length()==0)
//			   continue;
			  if(!linea.startsWith("3") && !linea.startsWith("2"))
			  {
//			   doc.leeImportacion(linea);
			   idDocumento = doc.getclaveProv() + "|" + doc.gettipo() + "|" + doc.getnumdocto();
			   idFacAsociada = (String)numDoctos.get(idDocumento);
			   idFacAsociada = idFacAsociada.substring(idFacAsociada.lastIndexOf("|")+1);
			   doc.setfactura_asociada(idFacAsociada);
			   doc.setnombreProv((String)nombresProv.get(doc.getclaveProv()));
			   doc.setproveedor((String)codigosProvFiltro.get(doc.getclaveProv()));
//			   jgarcia - Stefanini
			   doc.setDivisa(divisaCliente);
			   // jgarcia 14/Oct/2009 - Se asigna la Forma de pago
			   doc.setipoCuenta( (String)codigosTipoCta.get(doc.getclaveProv()) );
			   EIGlobal.mensajePorTrace("Agregando documento",EIGlobal.NivelLog.DEBUG);
			   pasa = agregaDoc(doc);
			   if(!pasa)
			    errorLinea.add("La nota no tiene asociada ninguna factura: " + doc.getnumdocto());
			   else //if(doc.gettipo() == doc.FACTURA)
			   {
			    // si el documento es una nota, buscar la factura asociada y verificar si importe
			    // if(doc.gettipo() == doc.NOTA_CARGO || doc.gettipo() == doc.NOTA_ABONO)
			    if(doc.gettipo() == doc.NOTA_CARGO || doc.gettipo() == doc.NOTA_ABONO)
			    {
			     String facturaPorBuscar = doc.getfactura_asociada();
			     String provPorBuscar = doc.getproveedor();
			     Documento facturaEncontrada = null, facturaPosible = null; // declaracion de variables
			     for(int a=0; a <this.doc.size();a++)
			     {
			      facturaPosible = (Documento)this.doc.get(a);
			      if(facturaPorBuscar.equals(facturaPosible.getnumdocto()) && provPorBuscar.equals(facturaPosible.getproveedor()) && facturaPosible.gettipo() == 1)
			      {
			       facturaEncontrada = (Documento)this.doc.get(a);
			       break;
			      }
			     }
			     if(facturaEncontrada != null && facturaEncontrada.getimporte().startsWith("-"))
			      errorLinea.add("El importe total de la factura resulta negativo: " + facturaEncontrada.getimporte());
			    }
			   }
			  }
			  /*I05-0382520 se comenta la siguiente linea
			   *errores.add(errorLinea);*/
//			 }// while
			totImp = "" + total;
			totImpFac = "" + totalSinNotas;


			}
			//I05-0382520 se agrega la siguiente linea
			i++;
			errores.add(errorLinea);

		}
			 entrada.close();

			 if(i == 0){
				 errorLinea = new Vector();
				 errorLinea.add("El formato de la linea no es valido.");
				 errores.add(errorLinea);
			 }
		}
		catch(Exception e)
		{
		 //EIGlobal.mensajePorTrace("Error en ArchivoCfmg.java (2): " + e, EIGlobal.NivelLog.ERROR);
		 //e.printStackTrace();
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace("Error al leer importaci�n", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("ArchivoCfmg::leeImportacion:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ e.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.ERROR);
		}finally{
			try{
				conn.close();
				}catch(SQLException e1){
					EIGlobal.mensajePorTrace("ArchivoCfmg::leeImportacion:: Problemas al cerrar conexi�n", EIGlobal.NivelLog.ERROR);
				}
			}
		if(errores.size() == 0 && i>0)
		 estado = IMPORTADO;
		return errores;
	}
	// ---
	public void archivoEnvio(String archivo)
	{
		EIGlobal.mensajePorTrace("ArchivoCfmg.java Archivo = >" +archivo+ "<", EIGlobal.NivelLog.INFO);

		int a, totalDoc;
		Documento lineaDoc;

		totalDoc = doc.size();
		try
		{
			java.io.FileWriter salida = new java.io.FileWriter(archivo);

			for(a=0;a<totalDoc;a++)
				salida.write(((Documento)doc.get(a)).tramaEnvio() + "\n");

			for(a=0;a<totalDoc;a++)
			{
				lineaDoc = (Documento)doc.get(a);

				if(lineaDoc.gettipo() > 2)
				{
					salida.write("3;" + lineaDoc.getproveedor() + ";" +
						lineaDoc.getfactura_asociada() + ";" +
						lineaDoc.getnumdocto() + ";" +
						"00" + lineaDoc.gettipo() + ";" +
						lineaDoc.getimporte_neto() + ";" +
						lineaDoc.getnaturaleza() + ";" +
						//HGCV Ley de Transparencia II
						lineaDoc.getreferencia() + ";" +
						lineaDoc.getconcepto() + ";" +
						lineaDoc.getaplicacion() + ";" + "\n");
						//HGCV Ley de Transparencia II
				}
			}
			salida.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error en ArchivoCfmg.java (3): " + e, EIGlobal.NivelLog.ERROR);
		}
	}
	/*
	 * jgarcia - Stefanini - Se duplica la funcion pero se le agrega parametro --> String cuentaCargo
	 */
	public void archivoEnvio(String archivo, String cuentaCargo)
	{
		EIGlobal.mensajePorTrace("ArchivoCfmg.java Archivo = >" +archivo + "< Cuenta de cargo = >" + cuentaCargo + "<", EIGlobal.NivelLog.DEBUG);

		int a, totalDoc;
		Documento lineaDoc;
		String divisaPago = "";

		totalDoc = doc.size();
		try
		{
			java.io.FileWriter salida = new java.io.FileWriter(archivo);

			if(totalDoc > 0){
				divisaPago = ((Documento)doc.get(0)).getClaveDivisa();
			}

			for(a=0;a<totalDoc;a++)
				salida.write(((Documento)doc.get(a)).tramaEnvio(cuentaCargo, divisaPago) + "\n");

			for(a=0;a<totalDoc;a++)
			{
				lineaDoc = (Documento)doc.get(a);

				if(lineaDoc.gettipo() > 2)
				{
					salida.write("3;" + lineaDoc.getproveedor() + ";" +
						lineaDoc.getfactura_asociada() + ";" +
						lineaDoc.getnumdocto() + ";" +
						"00" + lineaDoc.gettipo() + ";" +
						lineaDoc.getimporte_neto() + ";" +
						lineaDoc.getnaturaleza() + ";" +
						//HGCV Ley de Transparencia II
						lineaDoc.getreferencia() + ";" +
						lineaDoc.getconcepto() + ";" +
						lineaDoc.getaplicacion() + ";" +
						//HGCV Ley de Transparencia II
						"\n");
				}
			}
			salida.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error en ArchivoCfmg.java (3): " + e, EIGlobal.NivelLog.ERROR);
		}
	}
	// ---
	public void archivoExportacion(String archivo, String cuentaCargoEnvio)
	{
		int a, totalDoc;
		Documento lineaDoc;
		totalDoc = doc.size();

		try	{
			 java.io.FileWriter salida = new java.io.FileWriter(archivo);			 
			 for(a=0;a < totalDoc;a++)
			     salida.write(((Documento)doc.get(a)).tramaExportacion(cuentaCargoEnvio) + "\n");
			 
			 for(a=0;a < totalDoc; a++)
			 {
				lineaDoc = (Documento)doc.get(a);
				if(lineaDoc.gettipo() > 2)
				{
					salida.write("3" +
					rellena(lineaDoc.getclaveProv(),20,' ',false) +
					rellena(lineaDoc.getfactura_asociada(),8,' ',false) +
					rellena(lineaDoc.getnumdocto(),8,' ',false) +
					"00" + lineaDoc.gettipo() +
					rellena(sinPuntoDecimal(lineaDoc.getimporte_neto()),21,' ',true) +
					lineaDoc.getnaturaleza());
					//HGCV Ley de Transparencia II
					//rellena(lineaDoc.getreferencia(), 7,' ', false) +
					//rellena(lineaDoc.getconcepto(), 40, ' ', false) + "\n");
					//HGCV Ley de Transparencia II
				}
			 }
			 salida.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error en ArchivoCfmg.java (4): " + e, EIGlobal.NivelLog.ERROR);			
		}
	}

	// ---
	public void ordenaNombreProv()
		{
		int a, b, total = doc.size();
		Documento docA, docB;
		for(a=0;a<total-1;a++)
			for(b=a+1;b<total;b++)
				{
				docA = (Documento)doc.get(a);
				docB = (Documento)doc.get(b);
				if(docA.getnombreProv().compareTo(docB.getnombreProv()) > 0)
					{
					doc.set(a,docB);
					doc.set(b,docA);
					}
				}
		}

	/** Añade caracteres de relleno
	 * @param cadena Cadena a la que se le añanden caracreres de relleno
	 * @param lon longitud de la nueva cadena
	 * @param car caracter con el que se hace el relleno
	 * @param rellenaXizq indica por cual lado se hará el relleno: true
	 * rellena por izquierda y false por la derecha
	 * @return la nueva cadena con el relleno
	 */
	private String rellena(String cadena, int lon, char car, boolean rellenaXizq)
		{
		int a, total = lon - cadena.length();
		if(total<0) return cadena.substring(0,lon);
		StringBuffer retorno = new StringBuffer(cadena);
		if(rellenaXizq)
			for(a=0;a<total;a++) retorno.insert(0,car);
		else
			for(a=0;a<total;a++) retorno.append(car);
		return retorno.toString();
		}

	//
	private static String sinPuntoDecimal(String moneda)
		{
		int pos = moneda.indexOf(".");
		if(pos == -1) return moneda + "00";
		while(pos+3>moneda.length()) moneda += "0";
		while(pos+3<moneda.length()) moneda = moneda.substring(0,moneda.length()-1);
		moneda = moneda.substring(0,pos) + moneda.substring(pos+1);
		while(moneda.length()>1 && moneda.startsWith("0")) moneda = moneda.substring(1);
		return moneda;
		}

	//
	private static String ponPuntoDecimal(String moneda)
		{
		while(moneda.length()<3) moneda = "0" + moneda;
		int posicionPunto = moneda.length() - 2;
		return moneda.substring(0,posicionPunto) + "." + moneda.substring(posicionPunto);
		}

	//
	private static String sinCerosAlaIzq(String tipo)
		{
		while(tipo.startsWith("0")) tipo = tipo.substring(1);
		return tipo;
		}

	/** valida un importe sin puntos decimales */
	private static boolean validaImporte(String cadena)
		{
		String patron = "0123456789";
		int a, total;
		boolean valido = true;
		total = cadena.length();
		for(a=0;a<total;a++)
			if(patron.indexOf(cadena.substring(a,a+1))==-1)
				{valido = false; break;}
		if(cadena.indexOf(".")!=cadena.lastIndexOf(".")) valido = false;
		return valido;
		}

	// ---
	public static void main(String args[])
		{
		ArchivoCfmg x = new ArchivoCfmg();
		/*
		x.leeRecuperacion("pagos_recuperado_2");
		System.out.println(x.fechaTransmision);
		System.out.println(x.numTransmision);
		System.out.println("" + x.aceptados);
		System.out.println("" + x.rechazados);
		System.out.println("" + x.totalRegistros);
		System.out.println("" + x.importeTotal);
		x.archivoEnvio("orale20.txt");
		*/
		int a, b;
		Vector e;
		Vector err=x.leeImportacion(Global.DOWNLOAD_PATH + "PAGPRUE.TXT"); //RvvPagosImp.txt //Se valida /tmp/
		Documento d;
		System.out.println("lineas: " + err.size());
		for(a=0;a < err.size();a++)
			{
			e = (Vector)err.get(a);
			for(b=0;b < e.size();b++)
				System.out.println("Linea " + (a+1) +": " + e.get(b));
			}
		System.out.println("---------------------------------------------");
		for(a=0;a < x.doc.size();a++)
			{
			  d = (Documento)x.doc.get(a);
			  //System.out.println("> linea: " + d.tramaExportacion());
			}
//		x.archivoEnvio("/tmp/RvvPagosEnvio.txt");

//		System.out.println(x.sinCerosAlaIzq("008"));


/*
		int a;
		for(a=0;a<x.doc.size();a++)
			{
			System.out.println("\n" + a + "\n");
			((Documento)x.doc.get(a)).imprimePrueba();
			}
/*
		for(a=0;a<x.neteo.size();a++)
			{
			System.out.println("\n" + a + "\n");
			System.out.println((String)x.neteo.get(a));
			}
*/
		}
//SLF 28112003
      public void ReporteCorto()
		{
		  if (doc.size()>30)
		  {
    	   EIGlobal.mensajePorTrace("Son más de 30 registros", EIGlobal.NivelLog.DEBUG);
		   while(doc.size()>30)
			 {
			  doc.removeElementAt(doc.size()-1);
	  		  EIGlobal.mensajePorTrace("size: "+ doc.size(), EIGlobal.NivelLog.DEBUG);
			 }
		  }
		  EIGlobal.mensajePorTrace("Terminado reporte corto", EIGlobal.NivelLog.DEBUG);
		}

	public final double getImpAceptado() {
		if(estado == SIN_ENVIAR) return 0.0;
		else return redondear(impAceptado, 2);
	}

	public final void setImpAceptado(double impAceptado) {
		this.impAceptado = impAceptado;
	}

	public final double getImpRechazado() {
		if(estado == SIN_ENVIAR) return 0.0;
		else return redondear(impRechazado, 2);
	}

	public final void setImpRechazado(double impRechazado) {
		this.impRechazado = impRechazado;
	}

	public final double getImpTransmitir() {
		if(estado != SIN_ENVIAR && estado != IMPORTADO) return 0.0;
		else return redondear(impTransmitir, 2);
	}

	public final void setImpTransmitir(double impTransmitir) {
		this.impTransmitir = impTransmitir;
	}

	/**
	 * Metodo para redondear a un numero determinado de decimales
	 *
	 * @param numero		Numero a rendondear
	 * @param decimales		Precisi�n en decimales
	 * @return				Numero redondeado
	 */
	public double redondear( double numero, int decimales ) {
		return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	}

	/**
	 * Metodo encargado de regresar la cuenta eje
	 * @param conn 				Conexi�n de BDD
	 * @param cveProveedor 		Clave de proveedor
	 * @return datosCuenta 		Numero y Descripci�n de la Cuenta
	 */
	private String ObtenFormaPagoImport(Connection conn, String cveProveedor) {
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String formaPago = "";

		try {
			sDup = conn.createStatement();
			query = "Select a.CVE_FORMA_PAGO as cveFormaPago "
				  + "From cfrm_det_provee a, cfrm_proveedores b "
				  + "Where a.cve_proveedor ='" + cveProveedor.trim() + "' "
				  + "and a.CVE_PROVEEDOR = b.CVE_PROVEEDOR "
				  + "and a.NUM_PERSONA = b.NUM_PERSONA";

			EIGlobal.mensajePorTrace ("ArchivoCfmg::ObtenFormaPagoImport:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				formaPago = rs.getString("cveFormaPago")!=null?rs.getString("cveFormaPago"):"";
			}

			EIGlobal.mensajePorTrace ("ArchivoCfmg::ObtenFormaPagoImport:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("ArchivoCfmg::ObtenFormaPagoImport:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al obtener el la forma de pago en la importacion", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("ArchivoCfmg::ObtenFormaPagoImport:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"		               				
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return formaPago;
	}

	}