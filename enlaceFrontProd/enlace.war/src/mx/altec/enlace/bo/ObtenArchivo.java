package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class ObtenArchivo
{
	protected String Contrato;
	protected String Usuario;
	protected String Perfil;
	
	// <vc n meg 05122008 >
	protected ServicioTux CombosConf;
	// <vc n meg 05122008 >
    protected File recibe(String Nombre) {
        try {
        	EIGlobal.mensajePorTrace("ObtenArchivo:recibe - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II
        	Properties rs = new Properties();
            FileInputStream in = new FileInputStream(Global.ARCHIVO_CONFIGURACION);
            rs.load(in);

           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nombre, rs.getProperty("DIRECTORIO_LOCAL"))){
				
					EIGlobal.mensajePorTrace("*** coConsultas.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** coConsultas.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
         
            if (Respuestal) {
                File Archivo = new File(
                		Nombre);

                return Archivo;
                
           	}
       	   
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return null;
    }

public void setContrato (String contrato) {Contrato = contrato; System.out.println("******* Contrato = "+Contrato);}
public void setUsuario (String usuario) {Usuario = usuario; System.out.println("******* Usuario = "+Usuario);}
public void setPerfil (String perfil) {Perfil = perfil; System.out.println("******* Perfil = "+Perfil);}

public String envia_tux() {
        try {
            String Trama = "2EWEB|" + Usuario + "|CFAC|" + Contrato + "|"
                            + Usuario + "|" + Perfil + "|";
            File ArchivoResp;
            Hashtable ht = CombosConf.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "actua"));
            return (ArchivoResp.getPath());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error al leer el archivo de respuesta";
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
            return "Error con la comunicacion";
        }
    }

}
