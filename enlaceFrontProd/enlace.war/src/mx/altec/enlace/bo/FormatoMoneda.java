package mx.altec.enlace.bo;

import java.text.NumberFormat;
import java.util.Locale;

public final class FormatoMoneda {

	public static final String formateaMoneda( double valor )
	{
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		if(valor == 0)
			formato = "0.0";

		if (valor < 0)
		{
			try {
				formato = nf.format(valor);
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("(MX"))
		    			 formato ="$ -"+ formato.substring (4,formato.length () - 1);
		    		 else
		    			 if (formato.substring (0,3).equals ("-MX"))
		    				 formato ="$ -"+ formato.substring (4,formato.length ());
		    			 else	
		    				 formato ="$ -"+ formato.substring (2,formato.length ());
			} catch(NumberFormatException e) {formato="$0.00";}
		} else {
			try {
				formato = nf.format(valor);
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("MXN"))
		    			 formato ="$ "+ formato.substring (3,formato.length ());
		    		 else
		    			 formato ="$ "+ formato.substring (1,formato.length ());
			} catch(NumberFormatException e) {
				formato="$0.00";}
		} // del else
		if(formato==null)
			formato = "";
		return formato;
	}

}
