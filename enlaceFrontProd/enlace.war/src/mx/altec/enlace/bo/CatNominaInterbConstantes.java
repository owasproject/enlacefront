package mx.altec.enlace.bo;

public class CatNominaInterbConstantes {

	/********* VALORES TABLA EWEB_CAT_NOM_EMPL ***************/
	public static final String _CAT_NOM	= "EWEB_CAT_NOM_EXTERNA";
	
	public static final String _CONTRATO = "NUM_CUENTA2";
	public static final String _NUM_CTAE = "NUM_CUENTA_EXT";
	public static final String _CVE_INT = "CVE_INTERME";
	public static final String _ULT_VER = "U_VERSION";
	public static final String _NOM = "NOMBRE";
	public static final String _PZA_BAN = "PLAZA_BANXICO";
	public static final String _SUC = "SUCURSAL";
	public static final String _TPO_CTA = "TIPO_CUENTA";

	/********* VALORES TABLA EWEB_CAT_NOM_AUT_EXT ***************/
	public static final String _CAT_EXT	= "EWEB_CAT_NOM_AUT_EXT";
	
	public static final String _EXT_CNT = "NUM_CUENTA2";    
	public static final String _EXT_CTA = "NUM_CUENTA_EXT";
	public static final String _EXT_STS = "ESTATUS_OPER";   
	public static final String _EXT_TIP = "TIPO_SOLIC";     
	public static final String _EXT_SUC = "SUCURSAL";       
	public static final String _EXT_INT = "CVE_INTERME";    
	public static final String _EXT_PLZ = "PLAZA_BANXICO";  
	public static final String _EXT_FOLE = "FOLIO_ENV";
	public static final String _EXT_FOLA = "FOLIO_AUT";      
	public static final String _EXT_PRSE = "NUM_PERSONA_ENV";
	public static final String _EXT_PRSA = "NUM_PERSONA_AUT";
	public static final String _EXT_FCHE = "FCH_ENV";
	public static final String _EXT_FCHA = "FCH_AUT";
	public static final String _EXT_TIT = "TITULAR";        
	public static final String _EXT_OBS = "OBSERVACIONES";  
	public static final String _EXT_LOT = "ID_LOTE";     
	public static final String _SEQ_EXT = "EWEB_SEQ_CAT_NOM_EXT";
	public static final String _SEQ_LOT = "EWEB_SEQ_CAT_NOM_LOT";
	public static final String _LOTE = "LOTE";

	/*********VALORES TABLA COMU_INTERME_FIN ******************/
	public static final String _COMU_INTERME = "COMU_INTERME_FIN";
	public static final String _NOM_BANCO = "NOMBRE_CORTO";
	public static final String _NUM_BANCO = "NUM_CECOBAN";
	public static final String _INTERME = "CVE_INTERME";
	
	

	/********* VALORES  ***************/
	public static final String _INA	= "I";
	public static final String _ACT	= "A";
	public static final String _PEND	= "P";
	public static final String _TOTAL	= "TOTAL";
	public static final String _ERROR	= "ERRORVACIO";
	public static final String _NOREG	= "NOREGISTROS";
	public static final int    _TOTAL_COL	= 9;		// Datos que se presentan en el resultado de la busqueda
	public static final String _MODIF	= "MODIF";
	public static final String _BAJA	= "BAJA";
	public static final String _ALTA_INTERB	= "ALTA_INTERB";
	public static final String _CTA_INTERB	= "INTERB";
	public static final String _CTA_INTERNA	= "PROPIA";
	public static final String _BANCO_INTERNO	= "SANTANDER";
	public static final String _DESC_ACT = "ACTIVO";
	public static final String _DESC_PEND = "PENDIENTE POR ACTIVAR";


	public static final String _ARCHIVO_NOMINA = "nominaInt.doc";


}