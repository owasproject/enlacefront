// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * Esta clase es un <I>bean</I> que se encarga de interpretar la trama de respuesta de tuxedo<BR>
 * de <B><I>disposici&oacute;n de cr&eacute;dito electr&oacute;nico (PC51)</I></B>, as&iacute; como interacturar con el jsp<BR>
 * para el despliegue de los datos obtenidos.<BR>
 * <P>Modificaciones:<BR>
 * 04/06/2002 Retirar espacios en blanco al obtener datos de la trama.<BR>
 * 03/11/2002 Se reemplaz&oacute; el gui&oacute;n por la diagonal como separador de fecha.<BR>
 * 08/11/2002 Se agrega el atributo <B>mensajeError</B>.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.0.1
 */
public class InterpretaPD15 implements Serializable
{

/**Sirve para comparar con los dos primeros caracteres de la trama.
*/
private static final String OK="OK";

/**Cuatro d&iacute;gitos del c&oacute;digo de error cuando la trama es correcta.
*/
private static final String BIEN="0000";

/**N&uacute;mero de datos que debe ir en la trama despu&eacute;s del c&oacute;digo de error.
*/
private static final int elementos=4;

/**Arreglo de enteros que indican las posiciones de cada elemento de la trama.
*/
public static int[] posiciones={8, 15, 19, 23};

/**Folio o n&uacute;mero de referencia obtenido como respuesta de la consulta a tuxedo.
*/
private String numeroReferencia="";

/**C&oacute;digo de error obtenido en la trama de error.
*/
private String numeroError="";

/**Valor obtenido como confirmaci&oacute;n de la disposici&oacute;n.
*/
private String numeroDisposicion="";

/**N&uacute;mero de d&iacute;as del plazo de esta disposici&oacute;n
*/
private String plazo="";

/**Fecha de vencimiento de la disposici&oacute;n
*/
private String fechaVencimiento="";

/**Trama de respuesta de tuxedo del servicio <B>PD15</B>.
*/
private String respuestaPD15="";


private String mensajeError="";

  /**
   * Constructor vac&iacute;o
   */
  public InterpretaPD15()
  {
  } // Fin InterpretaPD15


   /**<code><B><I>interpretaRespuesta</I></B></code> revisa la trama de respuesta del servicio de tuxedo <B>PC51</B>.
   * <P>Regresa verdadero si la trama de respuesta no es una trama de error; regresa falso si la<BR>
   * trama es de error o no viene en el formato esperado.
   * </P>
   * @param datos Es la trama de respuesta de tuxedo del servicio <B>PC51</B>.
   */
  public boolean interpretaRespuesta(String datos)
  {
    String aux1;
    String aux2;
    StringTokenizer st;
    mensajeError="";

    if(datos==null || datos.length()<1)
      return false;

    try
    {
      aux1=datos.substring(0, posiciones[0]).trim();

      if(!aux1.equalsIgnoreCase(OK))
        return false;

      aux2=datos.substring(posiciones[0], posiciones[1]).trim();

      if(aux2!=null)
        numeroReferencia=aux2;

      aux1=datos.substring(posiciones[2], posiciones[3]);

      if(aux1==null)
         return false;

      numeroError=aux1;

      if(!aux1.equalsIgnoreCase(BIEN))
      {
        mensajeError=datos.substring(posiciones[3]+1);
        return false;
      } // Fin if



      st=new StringTokenizer(datos, "|");

      if(st.countTokens()!=elementos)
        return false;

      st.nextToken();
      numeroDisposicion=st.nextToken().trim();
      plazo=st.nextToken().trim();
      fechaVencimiento=st.nextToken().trim().replace('-', '/');




    }
    catch(ArrayIndexOutOfBoundsException aioobe)
    {
      return false;
    }
    catch(Exception e)
    {
      return false;
    } // Fin try-catch


    return true;
  } // Fin setRespuestaPD15


  public String getPlazo()
  {
    return plazo;
  }


  public String getNumeroDisposicion()
  {
    return numeroDisposicion;
  }

  public String getFechaVencimiento()
  {
    return fechaVencimiento;
  }

  public String getNumeroReferencia()
  {
    return numeroReferencia;
  }

  public String getMensajeError()
  {
   return mensajeError;
  }

} // Fin m�todo InterpretaPD15

