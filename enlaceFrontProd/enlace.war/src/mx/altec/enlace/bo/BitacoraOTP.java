/*
 * BitacoraOTP.java
 *
 * Created on 26 de octubre de 2006, 09:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mx.altec.enlace.bo;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Yesenia Huerta
 */
public class BitacoraOTP implements Serializable {
    private int referencia;
    private Date fecha;
    private int numConsec;
    private String numCuenta;
    private String dirIP;
    private String canal;
    private String transcaccion;
    private String usuario;
    private String idOperacion;
    private String descripcion;
    private String codCliente;
    private String cuentaDestino;
    private double monto;
    private int titulos;
    private String banco;
    private int tipoCambio;
    private Date fechaPro;
    private Date fechaApl;
    private int error;
    private String detalle;
    private String idSession;
    private String estatus;
    private String archivo;
    private String idSerie;
    private String instancia;
    private String hostName;
    private String hora;

    /** Creates a new instance of BitacoraOTP */
    public BitacoraOTP() {
    }

    public int getReferencia() {
        return referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getNumConsec() {
        return numConsec;
    }

    public void setNumConsec(int numConsec) {
        this.numConsec = numConsec;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getDirIP() {
        return dirIP;
    }

    public void setDirIP(String dirIP) {
        this.dirIP = dirIP;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getTranscaccion() {
        return transcaccion;
    }

    public void setTranscaccion(String transcaccion) {
        this.transcaccion = transcaccion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(String idOperacion) {
        this.idOperacion = idOperacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getTitulos() {
        return titulos;
    }

    public void setTitulos(int titulos) {
        this.titulos = titulos;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(int tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public Date getFechaPro() {
        return fechaPro;
    }

    public void setFechaPro(Date fechaPro) {
        this.fechaPro = fechaPro;
    }

    public Date getFechaApl() {
        return fechaApl;
    }

    public void setFechaApl(Date fechaApl) {
        this.fechaApl = fechaApl;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    public String getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(String idSerie) {
        this.idSerie = idSerie;
    }

    public String getInstancia() {
        return instancia;
    }

    public void setInstancia(String instancia) {
        this.instancia = instancia;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }



}
