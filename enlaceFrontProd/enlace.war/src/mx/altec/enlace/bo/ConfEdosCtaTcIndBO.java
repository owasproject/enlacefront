package mx.altec.enlace.bo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.beans.ConfEdosCtaTcIndBean;
import mx.altec.enlace.beans.TrxODB4Bean;
import mx.altec.enlace.dao.ConfEdosCtaIndDAO;
import mx.altec.enlace.dao.ConfEdosCtaTcIndDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import org.apache.commons.lang.StringUtils;

public class ConfEdosCtaTcIndBO {
	/** Instancia de clase del DAO **/
	transient ConfEdosCtaIndDAO confEdosCtaIndDAO = new ConfEdosCtaIndDAO();
	/** Instancia de clase del DAO **/
	transient ConfEdosCtaTcIndDAO confEdosCtaTcIndDAO = new ConfEdosCtaTcIndDAO();
	/** Instancia de clase para consumir la Trx LMPL**/
	transient TrxLMPLBO trxLmpl = new TrxLMPLBO();
	/**
	 * Metodo para cosultar el estatus de la tarjeta para edo de cta PDF
	 * @param tarjeta La tarjeta seleccionada
	 * @return ConfEdosCtaTcIndBean Bean de la informacion
	 */
	public ConfEdosCtaTcIndBean mostrarTarjetasPDF(String tarjeta) {
		
		EIGlobal.mensajePorTrace("Inicia ConfEdosCtaTcIndBO.mostrarTarjetasPDF", EIGlobal.NivelLog.INFO);
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		try{
		TrxODB4BO trxODB4BO = new TrxODB4BO();
		List<TrxODB4Bean> direcciones;
		confEdosCtaTcIndBean = consultarTrx(confEdosCtaTcIndBean, tarjeta);
		if(!confEdosCtaTcIndBean.isTieneProblemaDatos()){
			trxODB4BO.setNumeroCliente(confEdosCtaTcIndBean.getCodCliente());
			EIGlobal.mensajePorTrace("trxODB4BO.setNumeroCliente("+confEdosCtaTcIndBean.getCodCliente()+")", EIGlobal.NivelLog.DEBUG);
			direcciones = trxODB4BO.ejecuta();
			EIGlobal.mensajePorTrace("trxODB4BO.ejecuta()", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("getCodStatus()-->"+trxODB4BO.getCodStatus(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("getMsgStatus()-->"+trxODB4BO.getMsgStatus(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("confEdosCtaTcIndBean()-->"+confEdosCtaTcIndBean, EIGlobal.NivelLog.DEBUG);
			/*if(trxODB4BO.getCodStatus() != 0 && confEdosCtaTcIndBean!=null){
				confEdosCtaTcIndBean.setTieneProblemaDatos(true);
				confEdosCtaTcIndBean.setMensajeProblemaDatos(trxODB4BO.getMsgStatus());
				return confEdosCtaTcIndBean;
			}*/
			int longitud = direcciones.size();
			EIGlobal.mensajePorTrace("longitud"+longitud+" tarjeta"+tarjeta, EIGlobal.NivelLog.DEBUG);
			for( int i = 0 ; i < longitud ; i++ ){
				TrxODB4Bean trxODB4Bean = direcciones.get(i);
				EIGlobal.mensajePorTrace("trxODB4Bean.getNumeroTdc["+trxODB4Bean.getNumeroTdc()+"]", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("confEdosCtaTcIndBean.getContrato["+confEdosCtaTcIndBean.getContrato()+"]"+" trxODB4Bean.getNumCuenta["+trxODB4Bean.getNumeroTdc()+"]", EIGlobal.NivelLog.DEBUG);
				if(tarjeta.equals(trxODB4Bean.getNumeroTdc()) || confEdosCtaTcIndBean.getContrato().equals(trxODB4Bean.getNumCuenta())){
					confEdosCtaTcIndBean.setSecDomicilio(trxODB4Bean.getSecDomicilio());
					confEdosCtaTcIndBean.setDomCompleto(trxODB4Bean.getCalleYNumero() + ", " + trxODB4Bean.getDescComunidad() + ", " +
														trxODB4Bean.getDescLocalidad() + "C.P. " + trxODB4Bean.getCodigoPostal());
					if("S".equals(trxODB4Bean.getIndConsulta())){
						confEdosCtaTcIndBean.setSuscripPaperless(false);
					} else {
						confEdosCtaTcIndBean.setSuscripPaperless(true);
					}
					i = longitud;
				}
			}
			EIGlobal.mensajePorTrace("ConfEdosCtaTcIndBO[1]", EIGlobal.NivelLog.DEBUG);
			TrxMPPCBO trxMppc = new TrxMPPCBO();
			trxMppc.consultaTarjetas( confEdosCtaTcIndBean.getCodent().concat(confEdosCtaTcIndBean.getCentalt()).concat(confEdosCtaTcIndBean.getContrato()) );
			EIGlobal.mensajePorTrace("ConfEdosCtaTcIndBO[2]", EIGlobal.NivelLog.DEBUG);
			confEdosCtaTcIndBean.setTarjetas( trxMppc.getTarjetas() );
			confEdosCtaTcIndBean.setPeriodoDisp("");
			EIGlobal.mensajePorTrace("ConfEdosCtaTcIndBO[3]", EIGlobal.NivelLog.DEBUG);
			confEdosCtaTcIndBean.setFechaConfig(confEdosCtaTcIndDAO.consultaConfigPreviaTarjeta(tarjeta,
					confEdosCtaTcIndBean.getCodCliente()));
			EIGlobal.mensajePorTrace("ConfEdosCtaTcIndBO[4]", EIGlobal.NivelLog.DEBUG);
		}
		EIGlobal.mensajePorTrace("Fin ConfEdosCtaTcIndBO.mostrarTarjetasPDF", EIGlobal.NivelLog.INFO);
		}catch(Exception e)
		{EIGlobal.mensajePorTrace("ENTRO A EXCEPCION", EIGlobal.NivelLog.INFO);
			e.printStackTrace();}
		return confEdosCtaTcIndBean;
	}
	/**
	 * Metodo donde se ejecutan llamados a Trx
	 * @since 30/03/2015
	 * @author FSW-Indra
	 * @param confEdosCtaTcIndBean bean con la informacion
	 * @param tarjeta numero de tarjeta
	 * @return ConfEdosCtaTcIndBean bean con la info de respuesta.
	 */
	public ConfEdosCtaTcIndBean consultarTrx(ConfEdosCtaTcIndBean confEdosCtaTcIndBean, String tarjeta){
		TrxLMM8BO trxLMM8BO = new TrxLMM8BO();
		TrxMPB8BO trxMPB8BO = new TrxMPB8BO();
		TrxPE47BO trxPE47BO = new TrxPE47BO();
		trxLMM8BO.setNumeroTarjeta(tarjeta);
		EIGlobal.mensajePorTrace("trxLMM8BO.setNumeroTarjeta("+tarjeta+")", EIGlobal.NivelLog.DEBUG);
		trxLMM8BO.ejecuta();
		EIGlobal.mensajePorTrace("trxLMM8BO.ejecuta()", EIGlobal.NivelLog.DEBUG);
		confEdosCtaTcIndBean.setCodCliente(trxLMM8BO.getNumeroCliente());
		if(trxLMM8BO.getCodStatus() != 0){
			confEdosCtaTcIndBean.setTieneProblemaDatos(true);
			confEdosCtaTcIndBean.setMensajeProblemaDatos(trxLMM8BO.getMsgStatus());
			return confEdosCtaTcIndBean;
		}

		trxMPB8BO.setPanTarjeta(tarjeta);
		EIGlobal.mensajePorTrace("trxMPB8BO.setPanTarjeta("+tarjeta+")", EIGlobal.NivelLog.DEBUG);
		trxMPB8BO.ejecuta();
		EIGlobal.mensajePorTrace("trxMPB8BO.ejecuta()", EIGlobal.NivelLog.DEBUG);
		confEdosCtaTcIndBean.setContrato(trxMPB8BO.getCuentaTarjeta());
		confEdosCtaTcIndBean.setCodent(trxMPB8BO.getCodigoEntidad());
		confEdosCtaTcIndBean.setCentalt(trxMPB8BO.getCentroAltaCtaTarjeta());
		if(trxMPB8BO.getCodStatus() != 0){
			confEdosCtaTcIndBean.setTieneProblemaDatos(true);
			confEdosCtaTcIndBean.setMensajeProblemaDatos(trxMPB8BO.getMsgStatus());
			return confEdosCtaTcIndBean;
		}

		trxPE47BO.setNumeroCliente(confEdosCtaTcIndBean.getCodCliente());
		EIGlobal.mensajePorTrace("trxPE47BO.setNumeroClienta("+confEdosCtaTcIndBean.getCodCliente()+")", EIGlobal.NivelLog.DEBUG);
		trxPE47BO.ejecuta();
		EIGlobal.mensajePorTrace("trxPE47BO.ejecuta()", EIGlobal.NivelLog.DEBUG);
		if ("F".equals(trxPE47BO.getTipoPersona())) {
			confEdosCtaTcIndBean.setNombreComp(trxPE47BO.getApellidoPaterno() + " " + trxPE47BO.getApellidoMaterno() + ", " + trxPE47BO.getNombreTitular());
		} else if ("J".equals(trxPE47BO.getTipoPersona())) {
			confEdosCtaTcIndBean.setNombreComp(trxPE47BO.getNombreTitular());
		} else {
			confEdosCtaTcIndBean.setNombreComp("");
		}
		confEdosCtaTcIndBean.setNombre(trxPE47BO.getNombreTitular());
		confEdosCtaTcIndBean.setApPaterno(trxPE47BO.getApellidoPaterno());
		confEdosCtaTcIndBean.setApMaterno(trxPE47BO.getApellidoMaterno());
		confEdosCtaTcIndBean.setTipoPersona(trxPE47BO.getTipoPersona());
		return confEdosCtaTcIndBean;
	}

	/**
	 * Metodo para cancelar o suscribir paperless
	 * @param suscripPaperless Alta o Baja de Paperless
	 * @param seqDomicilio Secuencia de Domicilio
	 * @param tarjetaCuenta Tarjeta o Cuenta segun sea la operacion
	 * @param codCliente Codigo del Cliente
	 * @param codClientePersonas codigo del cliente en personas
	 * @param hdEdoCtaDisponible Disponibilidad del Estado de Cuenta
	 * @param hdSuscripPaperless Suscripcion de Paperless
	 * @param contrato Contrato
	 * @param contratoTdc contrato para la TDC
	 * @return Bean con la informacion
	 */
	public ConfEdosCtaTcIndBean configuraPaperless(String suscripPaperless,
			String seqDomicilio, String tarjetaCuenta, String codCliente, String codClientePersonas, String hdEdoCtaDisponible,
			String hdSuscripPaperless, String contrato, String contratoTdc) {
		EIGlobal.mensajePorTrace("Inicia ConfEdosCtaTcIndBO.configuraPaperless", EIGlobal.NivelLog.INFO);
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		suscripPaperless = suscripPaperless != null ? suscripPaperless : "N";
		String[] ctaTipo = tarjetaCuenta.split("@");
		confEdosCtaIndBean.setCodCliente(codCliente);
		confEdosCtaIndBean.setCodClientePersonas(codClientePersonas);
		confEdosCtaIndBean.setCuenta(ctaTipo[0]);
		confEdosCtaIndBean.setSecDomicilio(seqDomicilio);
		confEdosCtaTcIndBean.setCodCliente(codCliente);
		confEdosCtaTcIndBean.setCodClientePersonas(codClientePersonas);
		confEdosCtaTcIndBean.setTarjeta(ctaTipo[0]);
		confEdosCtaTcIndBean.setSecDomicilio(seqDomicilio);
		EIGlobal.mensajePorTrace("suscripPaperless->" + suscripPaperless, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("hdSuscripPaperless->" + hdSuscripPaperless, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ctaTipo[1]->" + ctaTipo[1], EIGlobal.NivelLog.INFO);
		//if( !StringUtils.equals(suscripPaperless, hdSuscripPaperless) ) {
			if( "003".equals(ctaTipo[1]) ){
				trxLmpl.setNumeroTarjeta(ctaTipo[0]);
				trxLmpl.setIndImpresion( "S".equals( suscripPaperless ) ? "N" : "S" );
				trxLmpl.ejecuta();
				confEdosCtaTcIndBean.setCodRetorno( trxLmpl.getCodStatus() == 0 ? "OK" : "NOOK" );
				
				confEdosCtaTcIndBean.setMensajeError(trxLmpl.getMsgStatus());
				EIGlobal.mensajePorTrace("Mensaje Error->" + confEdosCtaTcIndBean.getMensajeError(), EIGlobal.NivelLog.INFO);
				
			} else {
				confEdosCtaIndBean = confEdosCtaIndDAO.ejecutaODB2(confEdosCtaIndBean, suscripPaperless);
				confEdosCtaTcIndBean.setCodRetorno( confEdosCtaIndBean.getCodRetorno() );
				confEdosCtaTcIndBean.setDescRetorno( confEdosCtaIndBean.getDescRetorno() );
			}
		//}
		if("S".equals(suscripPaperless)) {
			confEdosCtaTcIndBean.setSuscripPaperless(true);
		}else {
			suscripPaperless = "N";
		}
		EIGlobal.mensajePorTrace("confEdosCtaIndBean.getCodRetorno()->" + confEdosCtaIndBean.getCodRetorno(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("trxLmpl.isResultadoOperacion()->" + trxLmpl.isResultadoOperacion(), EIGlobal.NivelLog.INFO);
		boolean operacionExitosa=false;
		if("003".equals(ctaTipo[1]) && trxLmpl.isResultadoOperacion())
		{
			operacionExitosa=true;
		}
		
		else if( "001".equals(ctaTipo[1]) && 
				(confEdosCtaIndBean.getCodRetorno()==null || confEdosCtaIndBean.getCodRetorno().contains("ODA") ||
				"OK".equals(confEdosCtaIndBean.getCodRetorno()) ||
				trxLmpl.isResultadoOperacion())) 
		{
			operacionExitosa=true;
		}
		
		if(operacionExitosa){
			EIGlobal.mensajePorTrace("suscripPaperless->" + suscripPaperless, EIGlobal.NivelLog.INFO);
			confEdosCtaIndBean.setContrato( contrato );
			confEdosCtaIndBean.setFolioOp( String.valueOf(confEdosCtaIndDAO.consultaFolioSeq()) );
			confEdosCtaTcIndBean.setContrato(contrato);
			confEdosCtaTcIndBean.setFolioOp( confEdosCtaIndBean.getFolioOp() );
			confEdosCtaIndDAO.insertaConfiguracion( confEdosCtaIndBean );
			if( "003".equals(ctaTipo[1]) ){
				confEdosCtaIndBean.setUso("TDC");
				confEdosCtaIndBean.setSecDomicilio(null);
			}
			confEdosCtaIndBean.setContrato( contratoTdc );
			confEdosCtaIndDAO.insertaDetalle(confEdosCtaIndBean, "", suscripPaperless);
		}
		EIGlobal.mensajePorTrace("Fin ConfEdosCtaTcIndBO.configuraPaperless", EIGlobal.NivelLog.INFO);
		return confEdosCtaTcIndBean;
	}

	/**
	 * Metodo para consultar si hay una configuracion previa de suscripcion a
	 * al servicio paperless ya sea cuenta o tarjeta
	 * @param seqDomicilio Secuencia de domicilio
     * @param codCliente Codigo del Cliente
     * @param tarjeta numero de la tarjeta de credito
	 * @return String Mensaje a retornar
	 */
	public String consultaConfigPreviaPaperless(String seqDomicilio,
			String codCliente, String tarjeta) {
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		final SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy",
				Locale.US);
		final String fechaHoy = sFormat.format(new Date());
		String fechaConfig = "";
		String mensaje = "";
		confEdosCtaTcIndBean = confEdosCtaTcIndDAO.consultaConfigPreviaEdoCta(
				seqDomicilio, codCliente, tarjeta);
		if (confEdosCtaTcIndBean.getFechaConfig() != null) {
			fechaConfig = sFormat.format(confEdosCtaTcIndBean.getFechaConfig());
		}
		if (StringUtils.equals(fechaHoy, fechaConfig)) {
			mensaje = "La cuenta ya ha sido parametrizada el d&iacute;a de hoy con el folio "
					.concat(confEdosCtaTcIndBean.getFolioOp())
					.concat(" y contrato ")
					.concat(confEdosCtaTcIndBean.getContrato()).concat(".");
		}
		return mensaje;
	}
}