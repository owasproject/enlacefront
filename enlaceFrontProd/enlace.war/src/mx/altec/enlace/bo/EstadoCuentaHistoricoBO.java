package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceException;

import mx.altec.enlace.beans.CuentaODD4Bean;
import mx.altec.enlace.beans.EstadoCuentaHistoricoBean;
import mx.altec.enlace.beans.ODD4Bean;
import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.cliente.ws.pdfondemand.RequestWS;
import mx.altec.enlace.cliente.ws.pdfondemand.ResponseWS;
import mx.altec.enlace.cliente.ws.pdfondemand.SOAPExceptionImpl;
import mx.altec.enlace.cliente.ws.pdfondemand.ValidaExistenciaPDFService;
import mx.altec.enlace.cliente.ws.pdfondemand.ValidaExistenciaPDFService_Service;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.EstadoCuentaHistoricoDAO;
import mx.altec.enlace.dao.ODD4DAO;
import mx.altec.enlace.dao.OW42DAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class EstadoCuentaHistoricoBO {

	/** Atributo static y final para guardar cadena constante (log). */
	private static final String PREFIJO_SOL_PREVIA = "EstadoCuentaHistoricoBO - existeSolicitudPrevia";
	/** Cadena para log **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: EstadoCuentaHistoricoBO :::";
	/** Objeto para realizar las operaciones en base de datos */
	private transient final EstadoCuentaHistoricoDAO estadoCuentaDAO = new EstadoCuentaHistoricoDAO();
	/** Objeto para realizar las operaciones de la transaccion ODD4 */
	private transient final ODD4DAO txODD4 = new ODD4DAO();
	/** Objeto para realizar las operaciones de la transaccion OW42 */
	private transient final OW42DAO txOW42 = new OW42DAO();

	/**
	 * Obtencion de la informacion del cliente
	 * @param numeroCuenta Cuenta de la que se desea obtener la informacion
	 * @return ejecucion de consulta TX ODD4
	 * @throws BusinessException En caso de un error con la operacion
	 * @throws ValidationException En caso de un error con la operacion
	 */
	public ODD4Bean obtenerInformacionCliente(String numeroCuenta)
			throws BusinessException {
		final ODD4Bean odd4Bean = new ODD4Bean();
		if(numeroCuenta.indexOf("BME") == 0){
			numeroCuenta = String.format("B%s", numeroCuenta.substring(3));
		}
		odd4Bean.setNumeroCuenta(numeroCuenta);
		return txODD4.consultarODD4(odd4Bean);
	}

	/**
	 * Obtencion de los periodos a traves de la transaccion OW42
	 * @param numeroCuenta Cuenta de la que se van a obtener los estados de cuenta
	 * @param numeroContrato Numero de contrato en sesion.
	 * @return Objeto que contiene los periodos de la cuenta
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public OW42Bean obtenerPeriodos(String numeroCuenta,String numeroContrato) throws BusinessException {
			return txOW42.consultarOW42(numeroCuenta,numeroContrato);
	}

	/**
	 * Realiza las peticiones de estado de cuenta
	 * @param periodosSeleccionados Periodos seleccionados de los que se desean obtener los estados de cuenta
	 * @param beanBase Objeto con la informacion de los estados de cuenta que se desean obtener
	 * @return solicitudes registradas
	 * @throws BusinessException En caso de un error con las operaciones
	 */
	public List<EstadoCuentaHistoricoBean> registrarSolicitudes (String[] periodosSeleccionados,
			EstadoCuentaHistoricoBean beanBase) throws BusinessException {

		final List<EstadoCuentaHistoricoBean> estadosCuenta = new ArrayList<EstadoCuentaHistoricoBean>();
		int secuenciaHistorica = 0;

		EIGlobal.mensajePorTrace(String.format("Periodos Seleccionados [%s]",periodosSeleccionados.length),
				EIGlobal.NivelLog.DEBUG);

		for(String periodoSeleccionado : periodosSeleccionados){
			EIGlobal.mensajePorTrace(String.format("Datos periodoSeleccionados [%s]",periodoSeleccionado),
					EIGlobal.NivelLog.DEBUG);
			final String[] periodoSeparado = periodoSeleccionado.split("@");
			EIGlobal.mensajePorTrace(String.format("Longitud Periodo Separado [%s]",periodoSeparado.length),
					EIGlobal.NivelLog.DEBUG);

			if(periodoSeparado.length == 4){

				final EstadoCuentaHistoricoBean estadoCuentaBean = new EstadoCuentaHistoricoBean();
				final String periodoConFormato = periodoSeparado[0];
				final String folio = periodoSeparado[1];
				final String tipoEDC = periodoSeparado[2];

				secuenciaHistorica=obtenerNumeroSecuencia();
				EIGlobal.mensajePorTrace(String.format("Secuencia Historica [%s]", secuenciaHistorica), EIGlobal.NivelLog.DEBUG);

				estadoCuentaBean.setCuentaEstadoCuenta(beanBase.getCuentaEstadoCuenta());
				estadoCuentaBean.setFormato(beanBase.getFormato());
				estadoCuentaBean.setPeriodo(periodoConFormato);
				estadoCuentaBean.setCliente(beanBase.getCliente());
				estadoCuentaBean.setFolioOndemand(folio);
				estadoCuentaBean.setFolioUUID(folio);
				estadoCuentaBean.setNumeroSecuenciaDomicilio(beanBase.getNumeroSecuenciaDomicilio());
				estadoCuentaBean.setUsuarioEnlace(beanBase.getUsuarioEnlace());
				estadoCuentaBean.setNumeroSecuenciaHistorica(secuenciaHistorica);
				estadoCuentaBean.setCuentaEnlace(beanBase.getCuentaEnlace());
				estadoCuentaBean.setTipoEDC(tipoEDC);

				estadosCuenta.add(estadoCuentaBean);
			}
		}

		return  estadoCuentaDAO.registrarPeticion(estadosCuenta);
	}

	/**
	 * Obtencion de un numero de secuencia utilizado en la solicitud de
	 * los estados de cuenta
	 * @return Un numero de secuencia
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public int obtenerNumeroSecuencia() throws BusinessException {
		return estadoCuentaDAO.obtenerSecuencia();
	}

	/**
	 * Metodo para consultar si la solicitud de periodo historico ya fue
	 * realizada anteriormente por el usuario.
	 *
	 * @param cuenta string con el numero de cuenta solicitado.
	 * @param periodo string con el periodo solicitado (yyyymm).
	 * @return boolean que indica si existe (true) o no existe (false) el
	 *         periodo solicitado en la tabla eweb_edo_cta_hist.
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public boolean existeSolicitudPrevia(String cuenta, String periodo)
			throws BusinessException {
		EIGlobal.mensajePorTrace("EstadoCuentaHistoricoBO :: Inicio de metodo existeSolicitudPrevia", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA + " :: cuenta: [" + cuenta + "]",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(PREFIJO_SOL_PREVIA + " :: periodo: [" + periodo + "]",
				EIGlobal.NivelLog.DEBUG);
		boolean existeSolicitudPrevia = false;

		if (cuenta != null && periodo != null) {
			existeSolicitudPrevia = estadoCuentaDAO.existeSolicitudPrevia(cuenta, periodo);
		} else {
			EIGlobal.mensajePorTrace(
					"EstadoCuentaHistoricoBO :: Fin de metodo existeSolicitudPrevia :: Valores incompletos",
					EIGlobal.NivelLog.INFO);
			throw new BusinessException("Se han recibido valores incompletos y no se pudo procesar la peticion");
		}
		EIGlobal.mensajePorTrace("EstadoCuentaHistoricoBO :: Fin de metodo existeSolicitudPrevia",
				EIGlobal.NivelLog.INFO);
		return existeSolicitudPrevia;
	}

	/**
	 * Llamado al WebService de PDF.
	 * @param tipoedc Tipo de Estado de Cuenta
	 * @param branch Branch
	 * @param foliood Folio OnDemand
	 * @param idProcesar Id por Procesar
	 * @param numcliente Num Cliente
	 * @param numcuenta Num Cuenta
	 * @param pais Pais
	 * @param periodoNumero Numero de Periodo
	 * @param rfc RFC
	 * @param tipo Tipo
	 * @return Respuesta de la llamada al WS
	 * @throws BusinessException En caso de un error con la operacion
	 */
	public String[] invocaWebServicePDF(String tipoedc,String branch, String foliood, String idProcesar,
			String numcliente,String numcuenta,String pais, String periodoNumero, String rfc, String tipo)
	throws BusinessException {
		String[] respuesta={"0","NA"};

		try {
			/** Creando instancia del WebService */
			EIGlobal.mensajePorTrace(String.format("%s URL de Web Service: [%s]", LOG_TAG, Global.validaExistenciaPDFWsdlLocation), NivelLog.DEBUG);
			ValidaExistenciaPDFService_Service service=new ValidaExistenciaPDFService_Service();
			EIGlobal.mensajePorTrace(String.format("%s Recuperando el puerto del servicio [%s]",LOG_TAG,service),NivelLog.INFO);
			ValidaExistenciaPDFService ports=service.getValidaExistenciaPDFServicePort();

			/** Llenado del Bean para envar al WebService **/
			RequestWS requestWS=new RequestWS();
			requestWS.setAplicacion(lpad(tipoedc,"0",3)); 		//"001"
			requestWS.setFolio(lpad(foliood,"0",8));			//"10000049"
			EIGlobal.mensajePorTrace("invocaWebServicePDF::tipoEdc::"+tipoedc+"::",NivelLog.INFO);
			EIGlobal.mensajePorTrace("invocaWebServicePDF::numcliente::"+numcliente+"::",NivelLog.INFO);
			if(tipoedc.equals("001")){//
				requestWS.setNumCliente(lpad(numcliente,"0",8));	//"00000372"
				EIGlobal.mensajePorTrace("invocaWebServicePDF::entro::cheques::",NivelLog.INFO);
			}else{
				requestWS.setNumCliente("0");	//"00000372"
				EIGlobal.mensajePorTrace("invocaWebServicePDF::entro::TDC::",NivelLog.INFO);
			}
			requestWS.setNumCuenta(numcuenta);					//"56722701244"
			requestWS.setPais(pais);							//"MX"
			requestWS.setPeriodo(periodoNumero);				//"201301"
			requestWS.setTipo(lpad(tipoedc,"0",8));				//"00000001"
			EIGlobal.mensajePorTrace(String.format("%s Llenado del Bean para envar al WS [%s]",LOG_TAG,requestWS.toString()),NivelLog.INFO);
			EIGlobal.mensajePorTrace(String.format("%s Aplicacion [%s]\nFolio [%s]\nNumCte [%s]\nNumTcta [%s]\nPais [%s]\nPeriodo [%s]\nTipo [%s]\n",
					LOG_TAG,requestWS.getAplicacion(),requestWS.getFolio(),requestWS.getNumCliente(),requestWS.getNumCuenta(),
					requestWS.getPais(),requestWS.getPeriodo(),requestWS.getTipo()),NivelLog.DEBUG);

			/** Llamando al WebService **/
			ResponseWS responseWS;
			try {
				responseWS=ports.validaExistencia(requestWS);
				EIGlobal.mensajePorTrace(String.format("%s Llamando al WS [%s]",LOG_TAG,responseWS.toString()),NivelLog.INFO);
				respuesta[0]=String.valueOf(responseWS.getCodigoRespuesta());
				respuesta[1]=responseWS.getDescripcionCodigo();
			} catch (SOAPExceptionImpl e) {
				EIGlobal.mensajePorTrace(String.format("%s BusinessException [%s]",LOG_TAG,e.getMessage()), NivelLog.INFO);
				/** throw new BusinessException(e.getMessage()); **/
			}
		} catch (WebServiceException e) {
			EIGlobal.mensajePorTrace(String.format("El Web Service definido [%s] no responde a la solicitud, se lanza excepcion controlada de negocio", Global.validaExistenciaPDFWsdlLocation), EIGlobal.NivelLog.DEBUG);
			throw new WebServiceException();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(String.format("Ocurrio un error inesperado al consumir el web service [%s], se lanza excepcion controlada de negocio", Global.validaExistenciaPDFWsdlLocation), EIGlobal.NivelLog.DEBUG);
			throw new WebServiceException();
		}
		return respuesta;
	}

	/**
	 * Metodo para rellenar 0 a la izquierda.
	 * @param cadena Cadena recibida
	 * @param caracter Caracter de division
	 * @param pos Posicion
	 * @return Cadena Nueva
	 */
	public String lpad(String cadena,String caracter,int pos){
		String cadNva="";
		int falta=0,longActual=0;
		if (cadena!=null && caracter!=null && pos>0 && pos>cadena.length()){
			longActual=cadena.length();
			falta=pos-longActual;
			for (int i=0;i<falta;i++) {
				cadNva=cadNva.concat(caracter);
			}cadNva=cadNva.concat(cadena);
		}else {
			cadNva=cadena;
		}

		return cadNva;
	}


	/**
	 * valida la cuenta del usuario
	 * @param numeroCuenta : numeroCuenta
	 * @param request request de la peticion
	 * @return  resultado boleano
	 * @throws ValidationException : ValidationException
	 */
	public boolean validaCuenta(String numeroCuenta, HttpServletRequest request) throws ValidationException {
		EIGlobal.mensajePorTrace(">>>>>>>>>>>>>>>>>>>>>Inicia validaCuenta", EIGlobal.NivelLog.DEBUG);
		//final EstadoCuentaHistoricoBO estadoCuentaBO = new EstadoCuentaHistoricoBO();
		final HttpSession httpSession = request.getSession();

		//final ODD4Bean odd4Bean = estadoCuentaBO.obtenerInformacionCliente(numeroCuenta);
		List<String> listaCuentas = (List<String>) httpSession.getAttribute("cuentasRelacionadas");
		for(String bean : listaCuentas){
			String cuenta1 = bean.trim();
			String cuenta2 = numeroCuenta;
			EIGlobal.mensajePorTrace(String.format("Compara %s == %s", cuenta1,cuenta2), EIGlobal.NivelLog.DEBUG);
			if(cuenta1.equalsIgnoreCase(cuenta2)){
				return true;
			}
		}

		return false;
	}


	/**
	 * Metodo para realizar la notificacion
	 * @param request : request
	 * @param session : session
	 * @param cuentaDest : cuentaDest
	 * @param periodos : periodos
	 * @param errores : errores
	 * @param tipoOp : tipo de operacion
	 */
    public void notifica(HttpServletRequest request, BaseResource session, String cuentaDest,
    		List<String> periodos, List<String> errores, String tipoOp) {
    	EmailSender emailSender = null;
		EmailDetails beanEmailDetails = null;

		emailSender = new EmailSender();
		beanEmailDetails = new EmailDetails();
		beanEmailDetails.setNumeroContrato(session.getContractNumber());
		beanEmailDetails.setRazonSocial(session.getNombreContrato());
		beanEmailDetails.setCuentaDestino(cuentaDest);
		beanEmailDetails.setListaFolios(periodos);
		beanEmailDetails.setListaErrores(errores);
		beanEmailDetails.setTipo_operacion(tipoOp);
		emailSender.sendNotificacion(request,IEnlace.SOLEDOCTAHIST,beanEmailDetails);
    }

	/**
	 * Agrega los datos comunes de las paginas
	 * @param request : request Objeto con la informacion de la peticion
	 * @param session : session Objeto con los datos de la sesion
	 */
    public void agregarDatosComunes(HttpServletRequest request, BaseResource session){
		request.setAttribute("NumContrato",
				(session.getContractNumber() == null) ? "" : session.getContractNumber());
		request.setAttribute("NomContrato",
				(session.getNombreContrato() == null) ? "" : session.getNombreContrato());
		request.setAttribute("NumUsuario",
				(session.getUserID8() == null) ? "" : session.getUserID8());
		request.setAttribute("NomUsuario",
				(session.getNombreUsuario() == null) ? "" : session.getNombreUsuario());
	}
}