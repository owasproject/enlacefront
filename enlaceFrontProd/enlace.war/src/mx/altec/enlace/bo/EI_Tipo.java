package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FavoritosConstantes;

public class EI_Tipo implements Serializable {
	public String strCuentas = "";
	public String strOriginal = "";
	public String[] registrosTabla;
	public String[][] camposTabla;

	public String chkBox = "";

	public String colTit = "class='textittab'";
	public String colEnc = "class='tittabdat'";
	public String colLin = "";

	public char separaCampo = '|';
	public char separaRegistro = '@';

	public int totalRegistros = 0;
	public int totalCampos = 0;

	// Modificación JPPM para poner links en las tablas 09/11/2004
	public boolean link = false; // atributo para indicar si existe un liga
	public int linkCol; // atributo para indicar en que columna esta la liga

	public static BaseServlet bsBase;

	public EI_Tipo() {
	}

	public EI_Tipo(BaseServlet bsTemp) {
		bsBase = bsTemp;
	}

	// Modificación para agregar un link a las tablas 12/11/2004
	public void iniciaObjeto(char a, char b, String c, boolean l, int col) {
		separaCampo = a;
		separaRegistro = b;
		strCuentas = c;
		strOriginal = c;
		link = l;
		linkCol = col;
		numeroRegistros();
		llenaArreglo();
		separaCampos();
	}

	public void iniciaObjeto(char a, char b, String c) {
		separaCampo = a;
		separaRegistro = b;
		strCuentas = c;
		strOriginal = c;
		numeroRegistros();
		llenaArreglo();
		separaCampos();
	}

	public void iniciaObjeto(String c) {
		strCuentas = c;
		strOriginal = c;
		numeroRegistros();
		llenaArreglo();
		separaCampos();
	}

	public void numeroRegistros() {
		totalRegistros = 0;
		for (int i = 0; i < strOriginal.length(); i++) {
			if (strOriginal.charAt(i) == separaRegistro)
				totalRegistros++;
		}
		EIGlobal.mensajePorTrace(
				"EI_Tipo - numeroRegistros(): Registros encontrados: >"
						+ Integer.toString(totalRegistros) + "<>"
						+ strOriginal.length() + "<", EIGlobal.NivelLog.INFO);
	}

	public void separaCampos() {
		String vartmp = "";
		String regtmp = "";

		int i, j;

		totalCampos = 0;
		if (totalRegistros >= 1) {
			regtmp += registrosTabla[0];
			for (i = 0; i < regtmp.length(); i++) {
				if (regtmp.charAt(i) == separaCampo)
					totalCampos++;
			}
			EIGlobal.mensajePorTrace(
					"EI_Tipo - separaCampos(): Total de campos : "
							+ Integer.toString(totalCampos),
					EIGlobal.NivelLog.INFO);
			camposTabla = new String[totalRegistros + 1][totalCampos + 2];
			for (i = 0; i < totalRegistros; i++) {
				regtmp = registrosTabla[i];
				for (j = 0; j < totalCampos; j++) {
					if (regtmp.charAt(0) == separaCampo) {
						vartmp = "";
						regtmp = regtmp.substring(1, regtmp.length());
					} else {
						vartmp = regtmp.substring(0,
								regtmp.indexOf(separaCampo));
						if (regtmp.indexOf(separaCampo) > 0)
							regtmp = regtmp.substring(
									regtmp.indexOf(separaCampo) + 1,
									regtmp.length());
					}
					camposTabla[i][j] = vartmp;
				}
				camposTabla[i][j] = regtmp;
			}
		}
	}

	public void llenaArreglo() {
		String vartmp = "";
		registrosTabla = new String[totalRegistros + 1];
		for (int i = 0; i < totalRegistros; i++) {
			vartmp = strCuentas
					.substring(0, strCuentas.indexOf(separaRegistro));
			registrosTabla[i] = vartmp;
			if (strCuentas.indexOf(separaRegistro) > 0)
				strCuentas = strCuentas.substring(
						strCuentas.indexOf(separaRegistro) + 1,
						strCuentas.length());
		}
	}

	/* Metodos para generar tablas  ARS Para cambios deploy*/

	/*
	 * String generaTablaCHK String arrCampos[] int arrDatos[] int arrValues[]
	 * int arrAlign[]
	 * 
	 * Genera Tabla sin titulo con arreglo de datos,valores y de alineacion
	 */
	public String generaTablaCHK(String arrCampos[], int arrDatos[],
			int arrValues[], int arrAlign[])// cambio
	{
		StringBuffer strTabla = new StringBuffer("");
		StringBuffer value;

		String check = "CHECKED";
		String[] align = { "left ", "center ", "right " };
		int val;

		/*
		 * for(int j=0;j<arrCampos.length;j++) EIGlobal.mensajePorTrace(
		 * j+" 1  "+arrCampos[j],5 ); for(int j=0;j<arrDatos.length;j++)
		 * EIGlobal.mensajePorTrace(j+" 1  "+arrDatos[j] ,5 ); for(int
		 * j=0;j<arrValues.length;j++)
		 * EIGlobal.mensajePorTrace(j+" 1  "+arrValues[j] ,5 ); for(int
		 * j=0;j<arrAlign.length;j++) EIGlobal.mensajePorTrace(
		 * j+" 1  "+arrAlign[j],5 );
		 */

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center width='100%' nowrap>");

			// Encabezado ...
			val = (arrDatos[1] != 0) ? 1 : 0;
			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><td ");
				strTabla.append(colTit);
				strTabla.append(" colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(" align=center>");
				strTabla.append(arrCampos[0]);
				strTabla.append("</td></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr bgcolor='#E3E3E3'>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();' CHECKED> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}
			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = " ";
				else
					colLin = "bgcolor='#E3E3E3'";

				value = new StringBuffer("");
				for (int a = 1; a <= arrValues[0]; a++) {
					value.append(camposTabla[i][arrValues[a]]);
					value.append("|");
				}
				value.append(" @");

				strTabla.append("\n<tr ");
				strTabla.append(colLin);
				strTabla.append(">");
				// radio ...
				if (arrDatos[1] == 1) {
					strTabla.append("<td class='texconte'><input type=radio name=radTabla value='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					strTabla.append("></td>");
				}

				// checkbox ...
				if (arrDatos[1] == 2) {
					if (chkBox.charAt(i) != '0') {
						strTabla.append("<td class='texconte' align=center><input type=checkbox name='");
						strTabla.append(value.toString());
						strTabla.append("' CHECKED ></td>");
					} else
						strTabla.append("<td class='texconte' align=center><br></td>");
				}

				// info ...
				for (int j = 0; j < arrDatos[0]; j++) {
					strTabla.append("<td class='texconte' align=");
					strTabla.append(align[arrAlign[j]]);
					strTabla.append("> &nbsp;");
					strTabla.append(camposTabla[i][arrDatos[j + 2]]);
					strTabla.append("&nbsp; <br></td>");
				}

				strTabla.append("\n</tr>");
				check = "";
			}
			strTabla.append("\n</table>");
		}
		return strTabla.toString();
	}

	// Genera Tabla sin titulo con arreglo de datos,valores y de alineacion
	// y fila de totales
	public String generaTabla(String arrCampos[], int arrDatos[],
			int arrValues[], int arrAlign[], int Fin)// cambio
	{
		/*
		 * Fin: 0 no tabla fin, checked 1 no tabla fin, no checked 2 tabla fin,
		 * checked 3 tabla fin, no checked
		 */

		StringBuffer strTabla = new StringBuffer("");
		StringBuffer value;

		String check = "";
		String valCheck = "";
		String[] align = { "left ", "center ", "right " };

		int val;

		EIGlobal.mensajePorTrace(
				"EI_Tipo -- generaTabla(): Generando Tabla Campos,Datos,Value,Align",
				EIGlobal.NivelLog.INFO);

		if (Fin == 0 || Fin == 2) {
			valCheck = "CHECKED";
			check = "CHECKED";
		}

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");

			// Encabezado ...
			val = (arrDatos[1] != 0) ? 1 : 0;
			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><td ");
				strTabla.append(colTit);
				strTabla.append(" colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(" align=center>");
				strTabla.append(arrCampos[0]);
				strTabla.append("</td></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();'> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}
			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = "class='textabdatobs'";
				else
					colLin = "class='textabdatcla'";
				value = new StringBuffer("");
				for (int a = 1; a <= arrValues[0]; a++) {
					value.append(camposTabla[i][arrValues[a]]);
					value.append("|");
				}
				value.append(" @");

				strTabla.append("\n<tr>");
				if (arrDatos[1] == 1) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("><input type=radio name=radTabla value='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					strTabla.append("></td>");
				}
				if (arrDatos[1] == 2) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=center><input type=checkbox name='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(valCheck);
					strTabla.append(" ></td>");
				}
				for (int j = 0; j < arrDatos[0]; j++) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=");
					strTabla.append(align[arrAlign[j]]);
					strTabla.append("> &nbsp;");
					strTabla.append(camposTabla[i][arrDatos[j + 2]]);
					strTabla.append("&nbsp; <br></td>");
				}
				strTabla.append("\n</tr>");
				check = "";
			}
			if (Fin > 1)
				strTabla.append("</table>");
		}
		return strTabla.toString();
	}

	/**************************************************************************************/
	// Genera Tabla sin titulo con arreglo de datos y con arreglo de valores.
	public String generaTabla(String arrCampos[], int arrDatos[],
			int arrValues[]) {
		StringBuffer strTabla = new StringBuffer("");
		StringBuffer value;

		String check = "CHECKED";

		int val;

		EIGlobal.mensajePorTrace(
				"EI_Tipo - generaTabla(): Generando Tabla Campos,Datos,Value",
				EIGlobal.NivelLog.INFO);

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");

			// Encabezado ...
			val = (arrDatos[1] != 0) ? 1 : 0;
			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><th ");
				strTabla.append(colTit);
				strTabla.append(" colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(">");
				strTabla.append(arrCampos[0]);
				strTabla.append("</th></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();'> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}

			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = "class='textabdatobs'";
				else
					colLin = "class='textabdatcla'";

				value = new StringBuffer("");
				for (int a = 1; a <= arrValues[0]; a++) {
					value.append(camposTabla[i][arrValues[a]]);
					value.append("|");
				}
				value.append(" @");

				strTabla.append("\n<tr>");
				if (arrDatos[1] == 1) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("><input type=radio name=radTabla value='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					strTabla.append("></td>");
				}
				if (arrDatos[1] == 2) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=center><input type=checkbox name='");
					strTabla.append(value.toString());
					strTabla.append("'></td>");
				}
				for (int j = 0; j < arrDatos[0]; j++) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("> &nbsp;");
					strTabla.append(camposTabla[i][arrDatos[j + 2]]);
					strTabla.append(" &nbsp;<br></td>");
				}
				strTabla.append("\n</tr>");
				check = "";
			}
			strTabla.append("\n</table>");
		}
		return strTabla.toString();
	}

	// Genera Tabla sin titulo y con arreglo de datos
	public String generaTabla(String arrCampos[], int arrDatos[]) {
		StringBuffer strTabla = new StringBuffer("");
		String check = "CHECKED";

		int val;

		EIGlobal.mensajePorTrace(
				"EI_Tipo - generaTabla(): Generando Tabla Campos,Datos",
				EIGlobal.NivelLog.INFO);

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");

			// Encabezado ...
			val = (arrDatos[1] != 0) ? 1 : 0;
			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><th ");
				strTabla.append(colTit);
				strTabla.append(" colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(">");
				strTabla.append(arrCampos[0]);
				strTabla.append("</th></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();'> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}
			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = "class='textabdatobs'";
				else
					colLin = "class='textabdatcla'";

				strTabla.append("\n<tr>");
				if (arrDatos[1] == 1) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("><input type=radio name=radTabla value=");
					strTabla.append(Integer.toString(i));
					strTabla.append(" ");
					strTabla.append(check);
					strTabla.append("></td>");
				}
				if (arrDatos[1] == 2) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=center><input type=checkbox name=chkBox");
					strTabla.append(Integer.toString(i));
					strTabla.append(" checked></td>");
				}
				for (int j = 0; j < arrDatos[0]; j++) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("> &nbsp;");
					strTabla.append(camposTabla[i][arrDatos[j + 2]]);
					strTabla.append(" &nbsp;<br></td>");
				}
				strTabla.append("\n</tr>");
				check = "";
			}
			strTabla.append("\n</table>");
		}
		return strTabla.toString();
	}

	// Genera Tabla sin titulo con arreglo de datos,valores y de alineacion
	public String generaTabla(String arrCampos[], int arrDatos[],
			int arrValues[], int arrAlign[]) {
		// Para el Interbancario -----------------------
		StringBuffer strTabla = new StringBuffer("");
		StringBuffer value;

		String check = "CHECKED";
		String[] align = { "left ", "center ", "right " };

		int val;
        boolean hayFavorito = validarColumnaFavoritos(arrCampos, arrCampos.length-1);
		EIGlobal.mensajePorTrace(
				"**> EI_Tipo - generaTabla(): Generando Tabla Campos,Datos,Value,Align"
						+ totalRegistros, EIGlobal.NivelLog.INFO);

		if (arrDatos[1] == 5) // No checar ningun radio ...
		{
			arrDatos[1] = 1;
			check = "";
		}

		if (arrDatos[1] == 6) // No checar ningun check ...
		{
			arrDatos[1] = 2;
			check = "";
		}

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");

			// Encabezado ...
			val = (arrDatos[1] != 0)? 1 : 0;
			val = hayFavorito?(val+2):val;

			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><td ");
				strTabla.append(colTit);
				strTabla.append("colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(" align=center>");
				strTabla.append(arrCampos[0]);
				strTabla.append("</td></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();'> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}
			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			if(hayFavorito){
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append(" colspan = \"2\" > &nbsp; ");
				strTabla.append(arrCampos[arrCampos.length-1]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = "class='textabdatobs'";
				else
					colLin = "class='textabdatcla'";
				value = new StringBuffer("");
				for (int a = 1; a <= arrValues[0]; a++) {
					value.append(camposTabla[i][arrValues[a]]);
					value.append("|");
				}
				value.append(" @");

				strTabla.append("\n<tr>");
				if (arrDatos[1] == 1) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("><input type=radio name=radTabla value='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					if(hayFavorito){
						strTabla.append(" onClick='seleccionarFavorito(this,"+(i+1)+");' ");
					}
					strTabla.append("></td>");
				}
				if (arrDatos[1] == 2) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=center><input type=checkbox name='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					if(hayFavorito){
						strTabla.append(" onClick='seleccionarFavorito(this,"+(i+1)+");' ");
					}
					strTabla.append("></td>");
				}
				EIGlobal.mensajePorTrace("------------------>El numero de datos es "+arrDatos[0], EIGlobal.NivelLog.INFO);
				int j = 0;
				for (j = 0; j < arrDatos[0]; j++) {
					// Modificación JPPM para agregar links a columnas de las
					// tablas
					EIGlobal.mensajePorTrace("--------->Datos:" +camposTabla[i][arrDatos[j + 2]], EIGlobal.NivelLog.INFO);
					if ((linkCol == j) && (link == true)) {
						strTabla.append("<td ");
						strTabla.append(colLin);
						strTabla.append(" align=");
						strTabla.append(align[arrAlign[j]]);
						strTabla.append("> &nbsp;");
						strTabla.append("<a href=\"javascript:funcionLink('");
						strTabla.append(value);
						strTabla.append("')\">");
						strTabla.append(camposTabla[i][arrDatos[j + 2]]);
						strTabla.append("</a>");
						strTabla.append("&nbsp; <br></td>");
					}
					else {
						strTabla.append("<td ");
						strTabla.append(colLin);
						strTabla.append(" align=");
						strTabla.append(align[arrAlign[j]]);
						strTabla.append("> &nbsp;");
						strTabla.append(camposTabla[i][arrDatos[j + 2]]);
						strTabla.append("&nbsp; <br></td>");
					}
					// EIGlobal.mensajePorTrace( ">>>>Campos de la tala>>>>-"+
					// camposTabla[i][arrDatos[j+2]] ,5 );
					if (arrDatos[1] == 1)
						check = "";
				}

				if(hayFavorito){
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=");
					strTabla.append(align[1]);
					strTabla.append("> &nbsp;");
					strTabla.append("<input type=\"checkbox\" name=\"favChb"
							+ (i + 1) + "\"  id=\"favChb"+(i + 1)+"\" onClick='modificarTxt(this,"+(i+1)+");' ></input></td>");
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=");
					strTabla.append(align[1]);
					strTabla.append("> &nbsp;");
					strTabla.append("<input type=\"text\" name=\"favTxt"
							+ (i + 1) + "\" size=\"15\" disabled=\"true\" maxlength=\"40\" id=\"favTxt"+(i + 1)+"\">");
					strTabla.append("</input></td>");
				}
				strTabla.append("\n</tr>");
				// check="";
			}
			strTabla.append("\n</table>");
		}
		return strTabla.toString();
	}

	/**
	 * Determina si existe la columna es es para favoritos.
	 *
	 * @param arrCampos
	 *            Lista de titulos
	 * @param ind
	 *            Indice actual.
	 * @return True si es la columna de favoritos.
	 */
	private boolean validarColumnaFavoritos(String[] arrCampos, int ind) {
		String favoritos = "";
		boolean esColumnaFav = false;
		if (arrCampos.length > ind) {
			favoritos = arrCampos[ind];
			EIGlobal.mensajePorTrace("--------->>>>>>> EL valor en favorito :"+favoritos+" en el indice "+ind , EIGlobal.NivelLog.INFO);
			if (FavoritosConstantes.TITULO_FAVORITOS.equals(favoritos)) {
				esColumnaFav = true;
			}
		}
		return esColumnaFav;
	}

	// SOLO PARA INTEGRACION TESORERIA INTELIGENTE ...
	// Genera Tabla sin titulo con arreglo de datos,valores y de alineacion
	public String generaTablaIntegra(String arrCampos[], int arrDatos[],
			int arrValues[], int arrAlign[]) {
		// Para el Interbancario -----------------------
		StringBuffer strTabla = new StringBuffer("");
		StringBuffer value;

		String check = "CHECKED";
		String[] align = { "left ", "center ", "right " };

		int val;

		EIGlobal.mensajePorTrace(
				"**> EI_Tipo - generaTabla(): Generando Tabla Campos,Datos,Value,Align"
						+ totalRegistros, EIGlobal.NivelLog.INFO);

		if (arrDatos[1] == 5) // No checar ningun radio ...
		{
			arrDatos[1] = 1;
			check = "";
		}
		if (arrDatos[1] == 6) // No checar ningun check ...
		{
			arrDatos[1] = 2;
			check = "";
		}

		if (totalRegistros >= 1) {
			strTabla.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");

			// Encabezado ...
			val = (arrDatos[1] != 0) ? 1 : 0;
			if (!(arrCampos[0].trim().equals(""))) {
				strTabla.append("\n<tr><td ");
				strTabla.append(colTit);
				strTabla.append("colspan=");
				strTabla.append((arrDatos[0] + val));
				strTabla.append(" align=center>");
				strTabla.append(arrCampos[0]);
				strTabla.append("</td></tr>");
			}

			// Titulos ...
			strTabla.append("\n<tr>\n");
			if (arrDatos[1] > 0)
				if (arrDatos[1] == 4) {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><input type=checkbox name='Todas' onClick='SeleccionaTodos();'> Todas</th>");
					arrDatos[1] = 2;
				} else {
					strTabla.append("<th ");
					strTabla.append(colEnc);
					strTabla.append("><br></th>");
				}
			for (int i = 1; i <= arrDatos[0]; i++) {
				strTabla.append("<th ");
				strTabla.append(colEnc);
				strTabla.append("> &nbsp; ");
				strTabla.append(arrCampos[i]);
				strTabla.append(" &nbsp; </th>");
			}
			strTabla.append("\n</tr>");

			for (int i = 0; i < totalRegistros; i++) {
				if ((i % 2) == 0)
					colLin = "class='textabdatobs'";
				else
					colLin = "class='textabdatcla'";
				value = new StringBuffer("");
				for (int a = 1; a <= arrValues[0]; a++) {
					value.append(camposTabla[i][arrValues[a]]);
					value.append("|");
				}
				value.append(" @");

				strTabla.append("\n<tr>");
				if (arrDatos[1] == 1) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append("><input type=radio name=radTabla value='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					strTabla.append(" onClick='verificaOpcion(this);'></td>");
				}
				if (arrDatos[1] == 2) {
					strTabla.append("<td ");
					strTabla.append(colLin);
					strTabla.append(" align=center><input type=checkbox name='");
					strTabla.append(value.toString());
					strTabla.append("' ");
					strTabla.append(check);
					strTabla.append(" onClick='verificaOpcion(this);'></td>");
				}
				for (int j = 0; j < arrDatos[0]; j++) {
					int img = 0;
					String strImg = "";

					if (arrDatos[j + 2] == 0) {
						img = Integer.parseInt(camposTabla[i][12]);
						strImg = "";
						for (int a = 1; a < img; a++)
							strImg += "<img src='/gifs/EnlaceMig/gbo25625.gif'>";
						if (tieneHijos(i))
							strImg += "<img src='/gifs/EnlaceMig/folder.gif'>&nbsp;&nbsp;";
						else
							strImg += "<img src='/gifs/EnlaceMig/hoja.gif'>&nbsp;&nbsp;";
						strTabla.append("<td ");
						strTabla.append(colLin);
						strTabla.append(" align=");
						strTabla.append(align[arrAlign[j]]);
						strTabla.append(">");
						strTabla.append(strImg);
						strTabla.append(camposTabla[i][arrDatos[j + 2]]);
						strTabla.append("&nbsp; <br></td>");
					} else {
						strTabla.append("<td ");
						strTabla.append(colLin);
						strTabla.append(" align=");
						strTabla.append(align[arrAlign[j]]);
						strTabla.append("> &nbsp;");
						strTabla.append(camposTabla[i][arrDatos[j + 2]]);
						strTabla.append("&nbsp; <br></td>");
					}
				}
				strTabla.append("\n</tr>");
				check = "";
			}
			strTabla.append("\n</table>");
		}
		EIGlobal.mensajePorTrace(
				"**> SALIENDO -------------------- EI_Tipo - generaTabla(): Generando Tabla Campos,Datos,Value,Align "
						+ totalRegistros, EIGlobal.NivelLog.INFO);
		return strTabla.toString();
	}

	boolean tieneHijos(int reg) {
		String cuenta = camposTabla[reg][0];
		for (int i = 0; i < totalRegistros; i++)
			if (camposTabla[i][7].equals(cuenta))
				return true;
		return false;
	}
}