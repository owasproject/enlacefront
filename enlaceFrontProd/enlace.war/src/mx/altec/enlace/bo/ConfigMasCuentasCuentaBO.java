package mx.altec.enlace.bo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/** import java.util.logging.Level;
import java.util.logging.Logger;*/

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaArchivoDatosBean;
import mx.altec.enlace.dao.ConfEdosCtaMasArchivoDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
/** import mx.altec.enlace.utilerias.Formateador;*/
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ConfigMasCuentasCuentaBO {	
	
	/**session**/
	private static final String BASE_RESOURCE = "session";
	/** objeto de servicio BO */
	//private static ConsultaCtasAsociadasBO consultaCtasAsociadasBO = new ConsultaCtasAsociadasBO(); 
	/** BaseServlet **/
	
	
	/**
	 * 
	 * @param userID Id del Usuario
	 * @param contractNumber Numero de Contrato
	 * @param userProfile Perfil de Usuario
	 * @param request Solicitud
	 * @return Lista de cuentas
	 */
	@SuppressWarnings("unchecked")
	public List<ConfEdosCtaArchivoBean> consultaCuentas(String userID,String contractNumber, 
			String userProfile, HttpServletRequest request){
		HttpSession httpSession	=request.getSession();
		boolean cambioContrato = cambioContrato(request);
		List<ConfEdosCtaArchivoBean> listaCuentasMasiva;
		ConfigModEdoCtaTDCBO configModEdoCtaTDCBO=new ConfigModEdoCtaTDCBO();
		
		if (httpSession.getAttribute("listaCuentasMasiva")==null || cambioContrato) {
			EIGlobal.mensajePorTrace("Se van a obtener las cuentas", NivelLog.DEBUG);
			listaCuentasMasiva=new ArrayList<ConfEdosCtaArchivoBean>();			
			listaCuentasMasiva=configModEdoCtaTDCBO.consultaTDCAdmCtr(userID,contractNumber,userProfile);
		}
		else {
			EIGlobal.mensajePorTrace("La lista de cuentas viene de la sesion", NivelLog.DEBUG);
			listaCuentasMasiva=(ArrayList<ConfEdosCtaArchivoBean>)httpSession.getAttribute("listaCuentasMasiva");
		}
		return listaCuentasMasiva;			
	}
	

	/**
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @param modulo : String
	 * @param archivo : String
	 * @param opcion : String
	 * @return List<ConfEdosCtaArchivoBean>
	 */
	public List<ConfEdosCtaArchivoBean> getCuentasTuxido(HttpServletRequest req, HttpServletResponse res, 
			String modulo, String archivo, String opcion) {
		
   		EIGlobal.mensajePorTrace("***Entrando a  lectura_cuentas ", EIGlobal.NivelLog.DEBUG);
   		
		BufferedReader entrada = null;
        String linea     = "";
		int separadores  = 0;
		boolean continua = true;
		int contador     = 1;
		StringBuffer trama = new StringBuffer();
        String datos[]   = null;
		HttpSession  sess    = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
        
        List<ConfEdosCtaArchivoBean> listaCuentasMasiva = new ArrayList<ConfEdosCtaArchivoBean>();
        ConfEdosCtaArchivoBean beanCuentas= null;

        try
		{
		    entrada = new BufferedReader( new FileReader( IEnlace.LOCAL_TMP_DIR + "/" + archivo));
			while ( (linea = entrada.readLine()) != null )
			{
				beanCuentas = new ConfEdosCtaArchivoBean();				
				separadores = cuentaseparadores(linea);
				continua=true;
				if( modulo.equals(IEnlace.MEnvio_pago_imp) ) {
					if (separadores!=17){
						continua=false;
					}
				}
				else if( modulo.equals(IEnlace.MDep_Inter_Abono) ) {
					if(separadores!=5){
						continua=false; 
					}
				}
                else if( separadores!=8 ) {
					continua=false;
                }
                if(continua && (contador <= Global.MAX_CTAS_MODULO) ) {
					datos = desentramaC(separadores+"@"+linea,'@');
					trama.append(contador).append('|').append(datos[1]).append("| |").append(datos[3]).append('|').append(datos[4]).append('|').append(datos[5]).append('|').append(datos[6]).append('|').append(datos[7]).append('|').append(datos[8]).append('@');
					beanCuentas.setNomCuenta(datos[1]);
					beanCuentas.setNombreTitular(datos[3]);
					listaCuentasMasiva.add(beanCuentas);			
					/** if (contador>Global.MAX_CTAS_MODULO){
					*	break;
					}**/
				}
	        }  //while
			//Modif PVA 03/12/2002 mejor en el finally entrada.close();
        }
            catch (FileNotFoundException ex) {
                        StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("cuentasSerfinSantander::lectura_cuentas_para_pantalla:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + ex.getMessage()
								   + "<DATOS GENERALES>"
								   + "Usuario->" + session.getUserID8()
								   + "Contrato->" + session.getContractNumber()
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.ERROR);
            } catch (IOException ex) {
                                       StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("cuentasSerfinSantander::lectura_cuentas_para_pantalla:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + ex.getMessage()
								   + "<DATOS GENERALES>"
								   + "Usuario->" + session.getUserID8()
								   + "Contrato->" + session.getContractNumber()
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.ERROR);
            }
        finally
		{
           try {
               entrada.close();
	 	   }   catch (IOException ex) {
                           	    	   StackTraceElement[] lineaError;
	    	   lineaError = ex.getStackTrace();
	    	   EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0]+
	    			   "  Error: " + ex.getMessage()
		               , EIGlobal.NivelLog.ERROR);
                       }

		}
   		return listaCuentasMasiva;
	}
	
	/**
	 * Metodo que ejecuta la clase inserterRegistros de ConfEdosCtaMasArchivoDAO
	 * @param bean : datos a registrar
	 * @return Map<String, String > respuesta de la operacion
	 */
	public Map<String, String > mandaAlmacenarDatos(ConfEdosCtaArchivoDatosBean bean) {
		ConfEdosCtaMasArchivoDAO dao = new ConfEdosCtaMasArchivoDAO();
		Map<String, String> resultado = new HashMap<String, String>();
		try {
			resultado = dao.insertarRegistros(bean);
			resultado.put("error", "none");
			return resultado;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("----------->ERROR: " + e.getStackTrace() + e.getErrorCode() + e.getErrorCode() +e.getCause() +e.getLocalizedMessage() +e.getMessage(), EIGlobal.NivelLog.ERROR);
			resultado.put("error", "fail");
			return resultado;
		}
	}
	
	/**
	 * @param session : BaseResource
	 * @param referencia : int
	 * @param folio : String
	 * @param request : HttpServletRequest
	 * @param datos : datos para envio de correo
	 */
	public void enviarNotificaciones(BaseResource session, int referencia, String folio, 
			HttpServletRequest request, String [] datos){
		EmailSender sender = new EmailSender();
		EIGlobal.mensajePorTrace("Entro a enviar correo", EIGlobal.NivelLog.ERROR);
		
			EmailDetails details = new EmailDetails();
			details.setNumeroContrato(session.getContractNumber());
			details.setRazonSocial(session.getNombreContrato());
			details.setUsuario(session.getUserID8());
			details.setNumRef(folio);
			details.setNumTransmision(String.valueOf(referencia));
			details.setFormatoArchivo(datos);
			EIGlobal.mensajePorTrace("Se envia informacion de correo", EIGlobal.NivelLog.ERROR);
			sender.sendNotificacion(request,IEnlace.CONFIGMASPORCUENTA, details);
		
                
                
/**                catch (Exception e) {
 *			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} */
	}
	
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @param servletName : string
	 * @return String
	 * @throws IOException : exception
	 */
	public static  String validaToken(HttpServletRequest request, HttpServletResponse response, String servletName) 
		throws IOException{
		
		EIGlobal.mensajePorTrace(
				"Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
				EIGlobal.NivelLog.DEBUG);
		
		//HttpSession ses = request.getSession();
		//BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(BASE_RESOURCE);
		
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);
		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& session.getToken().getStatus() == 1 && solVal) {
			EIGlobal.mensajePorTrace( "El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion",
					EIGlobal.NivelLog.DEBUG);
			ValidaOTP.guardaParametrosEnSession(request);
			//ValidaOTP.reestableceParametrosEnSessionEncoded(request);
			ValidaOTP.mensajeOTP(request,servletName);
			ValidaOTP.validaOTP(request, response,IEnlace.VALIDA_OTP_CONFIRM);
			
		} else {
			return "1";
		}
		return "";
	}
	
	/**
	 * Metodo para verificar si el cliente ha cambiado de contrato. Esto con la
	 * finalidad de determinar si es necesario volver a realizar la consulta de
	 * cuentas, ya que el catalogo vive en sesion y si el usuario cambia de
	 * contrato es necesario volver a consultarla en base de datos y guardarla
	 * nuevamente en sesion.
	 * 
	 * @param request objeto HttpServletRequest.
	 * @return boolean que indica si el cliente cambio de contrato. 
	 */
	private boolean cambioContrato(HttpServletRequest request) {
		EIGlobal.mensajePorTrace("ConCtasPdfXmlServlet :: Inicio de metodo cambioContrato" , EIGlobal.NivelLog.INFO);
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		String contratoAnterior = (String) request.getSession().getAttribute("contratoAnterior");
		String contratoActual = session.getContractNumber();
		boolean cambioContrato = false;
		EIGlobal.mensajePorTrace("ConCtasPdfXmlServlet :: contrato anterior: ["
				+ contratoAnterior + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConCtasPdfXmlServlet :: contrato actual: ["
				+ contratoActual + "]", EIGlobal.NivelLog.INFO);
		
		// La primer vez que se ingresa al metodo se setea contratoAnterior.
		if (contratoAnterior == null) {
			contratoAnterior = contratoActual;
			request.getSession().setAttribute("contratoAnterior", contratoAnterior);
			cambioContrato = true;
		} else if (!contratoAnterior.equals(contratoActual)) {
			// Cuando el contrato cambia, la variable contratoAnterior toma el
			// valor del contrato actual del cliente para posteriormente validar
			// con dicho valor.
			contratoAnterior = contratoActual;
			request.getSession().setAttribute("contratoAnterior", contratoAnterior);
			cambioContrato = true;
		}
		EIGlobal.mensajePorTrace("ConCtasPdfXmlServlet :: contratos distintos: ["
				+ cambioContrato + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ConCtasPdfXmlServlet :: Fin de metodo cambioContrato" , EIGlobal.NivelLog.INFO);
		// En caso de que el contrato no haya cambiado se retorna el valor default 'false'.
		return cambioContrato;
	}
	
	/********************************************/
	/** Metodo: cuentaseparadores
	 * @return int
	 * @param String -> linea
	 */
	/********************************************/
	     public int cuentaseparadores (String linea) {
		 int sep=0;
		 int pos=0;
		 int longi=linea.length ();
		 String linea_aux="";
		 while ((pos>-1)&&(pos<longi)) {
		     pos=linea.indexOf ("@");
		     if (pos>=0) {
			 sep++;
			 linea=linea.substring (linea.indexOf ("@")+1,linea.length ());
		     }
		 }
		 return sep;
	     }
	     
	     /********************************************/
	     /** Metodo: desentramaC
	      * @return String[]
	      * @param String -> buffer
	      * @param char -> Separador
	      */
	     /********************************************/
	     	public String[] desentramaC ( String buffer, char Separador ) {
	     	 String bufRest;
	     	 int intNoCont = Integer.parseInt ( buffer.substring ( 0, buffer.indexOf ( Separador ) ) );
	     	 String[] arregloSal = new String[ intNoCont + 1 ];
	     	 arregloSal[0] = buffer.substring ( 0, buffer.indexOf ( Separador ) );
	     	 bufRest = buffer.substring ( buffer.indexOf ( Separador ) + 1, buffer.length () );
	     	 for( int cont = 1; cont <= intNoCont; cont++ ) {
	     	     arregloSal[cont] = bufRest.substring ( 0, bufRest.indexOf ( Separador ) );
	     	     bufRest = bufRest.substring ( bufRest.indexOf ( Separador) + 1, bufRest.length () );
	     	 }
	     	 return arregloSal;
	          }
	          /**
	           * TraePerfil
	           */
}
