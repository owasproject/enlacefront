package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;


import mx.altec.enlace.dao.GenericDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase para invocar la transaccion PE47.
 * @author FSW-Indra
 * @sice 26/02/2015
 *
 */
public class TrxPE47BO extends GenericDAO {
	
	/** NOMBRE_TRANSACCION nombre de la transaccion PE47 */
	private static final String NOMBRE_TRANSACCION = "PE47";
	
	/**
	 * NUMERO DE PERSONA
	 */
	private String numeroCliente;
	/**
	 * NOMBRE DE FANTASIA
	 */
	private String nombreFantasia;
	/**
	 * PRIMER APELLIDO
	 */
	private String apellidoPaterno;
	/**
	 * SEGUNDO APELLIDO
	 */
	private String apellidoMaterno;
	/**
	 * NOMBRE PERSONA
	 */
	private String nombreTitular;
	/**
	 * TIPO DE PERSONA
	 */
	private String tipoPersona;
	
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de la Transaccion
	 */
	private int codStatus;

	
	/**
	 * Metodo encargado de realizar la ejecución de la Transaccion PE47.
	 * @since 02/03/2015
	 * @author FSW-Indra
	 */
	public void ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxPE47BO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", EIGlobal.NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxPE47BO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",EIGlobal.NivelLog.INFO);
		int bandera = 0;
		if(null == tramaRespuesta || ("").equals(tramaRespuesta.trim())) {
			codStatus = 99;
			msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " SIN RESPUESTA 390";
		} else if(tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO") > -1) {
			codStatus = 99;
			msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " NO HAY TERMINALES 390 PARA ATENDER LA PETICION";
		} else if(tramaRespuesta.indexOf("ABEND CICS") > -1 || tramaRespuesta.indexOf("failed with abend") > -1) {
            codStatus = 99;
            msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " TRANSACTION WITH ABEND";
        } else if(tramaRespuesta.indexOf("PERSONA INEXISTENTE") > -1 ) {
            msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " PERSONA INEXISTENTE";
            bandera++;
        } else if(tramaRespuesta.indexOf("NO EXISTE DOMICILIO PRINCIPAL") > -1 ) {
        	msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " NO EXISTE DOMICILIO PRINCIPAL";
        	bandera++;
        } else if(tramaRespuesta.indexOf("NO EXISTE TELEFONO PRINCIPAL") > -1 ) {
        	msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " NO EXISTE TELEFONO PRINCIPAL";
        	bandera++;
        } else if(tramaRespuesta.indexOf("NO EXISTE PERSONA CON ESE DOCUMENTO") > -1 ) {
        	msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " NO EXISTE PERSONA CON ESE DOCUMENTO";
        	bandera++;
        } else if(tramaRespuesta.indexOf("FALTA CAMPO OBLIGATORIO") > -1 ) {
        	msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " FALTA CAMPO OBLIGATORIO";
        	bandera++;
        } else if(tramaRespuesta.indexOf("USUARIO INVALIDO")>-1) {
        	msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " USUARIO INVALIDO";
            bandera++;
        }
		/**se setean los valores en vacio cuando la PE47 responde codigos de error que no son bloqueantes para el flujo**/
        if(bandera>0){
        	nombreFantasia  = ""; 
        	apellidoPaterno = ""; 
        	apellidoMaterno = ""; 
        	nombreTitular   = ""; 
        	tipoPersona     = ""; 
            return;
        }
        if(tramaRespuesta.indexOf("@DCPEM") > -1) {
        	desentrama(tramaRespuesta);
            msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " Ejecucion Exitosa con Socket.";
            return;
        }
	}
	
	/**
	 * Metodo para realizar el desentramado de la respuesta.
	 * @since 27/02/2015
	 * @author FSW-Indra
	 * @param tramaRespuesta de tipo String
	 */
	private void desentrama(String tramaRespuesta) {
		
		tramaRespuesta = tramaRespuesta.substring(tramaRespuesta.indexOf("@DCPEM"));
		tramaRespuesta = tramaRespuesta.replace("@DCPEM2650 P", "");

        nombreFantasia = tramaRespuesta.substring(8,38); // TIPO Y NUM DE DOC.
        apellidoPaterno = tramaRespuesta.substring(38,58); // APELLIDO PATERNO
        apellidoMaterno = tramaRespuesta.substring(58,78); // APELLIDO MATERNO
        nombreTitular = tramaRespuesta.substring(78,118); // NOMBRE
        tipoPersona = tramaRespuesta.substring(118,119); // Tipo de Persona
        
        EIGlobal.mensajePorTrace("-- Obtener Datos del cliente PE47 --", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("nombreFantasia [" + nombreFantasia + "]" + "apellidoPaterno [" + apellidoPaterno + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("apellidoMaterno [" + apellidoMaterno + "]" + "nombreTitular [" + nombreTitular + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("tipoPersona [" + tipoPersona + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 02/03/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void obtenerTramaEntrada(StringBuffer tramaEntrada) {
		
		  tramaEntrada.append(armaCabeceraPS7(NOMBRE_TRANSACCION, "[E47", 643, "00"));
	      tramaEntrada.append(rellenar(numeroCliente, 8,  ' ', 'D'));

		EIGlobal.mensajePorTrace("TrxMPB8BO: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
	}

	/**
	 * getNumeroCliente de tipo String.
	 * @author FSW-Indra
	 * @return numeroCliente de tipo String
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * setNumeroCliente para asignar valor a numeroCliente.
	 * @author FSW-Indra
	 * @param numeroCliente de tipo String
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * getNombreFantasia de tipo String.
	 * @author FSW-Indra
	 * @return nombreFantasia de tipo String
	 */
	public String getNombreFantasia() {
		return nombreFantasia;
	}

	/**
	 * setNombreFantasia para asignar valor a nombreFantasia.
	 * @author FSW-Indra
	 * @param nombreFantasia de tipo String
	 */
	public void setNombreFantasia(String nombreFantasia) {
		this.nombreFantasia = nombreFantasia;
	}

	/**
	 * getApellidoPaterno de tipo String.
	 * @author FSW-Indra
	 * @return apellidoPaterno de tipo String
	 */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * setApellidoPaterno para asignar valor a apellidoPaterno.
	 * @author FSW-Indra
	 * @param apellidoPaterno de tipo String
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
	 * getApellidoMaterno de tipo String.
	 * @author FSW-Indra
	 * @return apellidoMaterno de tipo String
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * setApellidoMaterno para asignar valor a apellidoMaterno.
	 * @author FSW-Indra
	 * @param apellidoMaterno de tipo String
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	/**
	 * getNombreTitular de tipo String.
	 * @author FSW-Indra
	 * @return nombreTitular de tipo String
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}

	/**
	 * setNombreTitular para asignar valor a nombreTitular.
	 * @author FSW-Indra
	 * @param nombreTitular de tipo String
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	/**
	 * getTipoPersona de tipo String.
	 * @author FSW-Indra
	 * @return tipoPersona de tipo String
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * setTipoPersona para asignar valor a tipoPersona.
	 * @author FSW-Indra
	 * @param tipoPersona de tipo String
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * getMsgStatus de tipo String.
	 * @author FSW-Indra
	 * @return msgStatus de tipo String
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * setMsgStatus para asignar valor a msgStatus.
	 * @author FSW-Indra
	 * @param msgStatus de tipo String
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * getCodStatus de tipo int.
	 * @author FSW-Indra
	 * @return codStatus de tipo int
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * setCodStatus para asignar valor a codStatus.
	 * @author FSW-Indra
	 * @param codStatus de tipo int
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}


}
