package mx.altec.enlace.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
//import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.dao.CatNominaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;

public class CatNominaAction {

	/*jp@everis*/
	/******************************************************************
	 *** M?todo que obtiene el numero de registros de la consulta 	***
	 *** y obtiene el numero de p?ginas								***
	 *** arr[0] = numero Paginas		 							***
	 *** arr[1] = total de Registros								***
	 *@param numcontrato 	String
	 *@param radioValue		String
	 *@param valorBusqueda 	String
	 *@param tamanoBloque	int
	 *@return String[] unknown
	 ******************************************************************/
	public String[] consultaPaginas( String numContrato, String radioValue, String valorBusqueda, int tamanoBloque )
	{
//		System.out.println("CatNominaAction - [consultaPaginas] $ numContrato = " + numContrato);
//		System.out.println("CatNominaAction - [consultaPaginas] $ radioValue = " + radioValue);
//		System.out.println("CatNominaAction - [consultaPaginas] $ valoBusqueda = " + valorBusqueda);
		CatNominaDAO dao = new CatNominaDAO();
		String filtroBusqueda = "";
		String tablasBusq = CatNominaConstantes._CAT_NOM + ", " + CatNominaConstantes._DET_CAT;
		filtroBusqueda = dao.generaFiltroBusqueda(numContrato, radioValue, valorBusqueda);
		int totalReg = 0;
		int numeroPaginas = 0;
		String pagReg[] = new String[2];

		totalReg = dao.cuentaQueryBusqueda(filtroBusqueda, tablasBusq);

		if(totalReg != -100)
		{
			if( totalReg <= tamanoBloque ){
				numeroPaginas = 0;
			}
			else{
				if(totalReg%tamanoBloque == 0){
					numeroPaginas = totalReg/tamanoBloque;
				}
				else{
					numeroPaginas = totalReg/tamanoBloque+1;
				}
			}
			pagReg[0]=String.valueOf(numeroPaginas);
			pagReg[1]=String.valueOf(totalReg);
//			System.out.println("CatNominaAction - [consultaPaginas] $ Numero Paginas: " + pagReg[0]);
//			System.out.println("CatNominaAction - [consultaPaginas] $ Total Registros: " + pagReg[1]);
		}
		else{
			pagReg[0] = "ERROR0000";
		}
		return pagReg;
	}



	/******************************************************************
	 *** M?todo que toma el resultset con el resultado del query 	***
	 *** y seprara los campos en varibles para construir la tabla	***
	 *** con los resultados de la consulta.
	 ******************************************************************/
	public String consultaEmpleados(
			String[][] arrDatosQuery,
			int inicioBloque,
			int finBloque,
			int tamanoBloque,
			int totalReg,
			int numPagina,
			int numPaginas)
	{
//		System.out.println("CatNominaAction - [consultaEmpleados] $ Inicio");
		EIGlobal.mensajePorTrace("CatNominaAction - [consultaEmpleados] $ Inicio", EIGlobal.NivelLog.INFO);
		CatNominaHTML html = new CatNominaHTML();
		boolean existReg = true;  // Bandera momentanea
//		System.out.println("CatNominaAction - consultaEmpleados $ Existen Reg " + existReg);
		String tablaEmpl = "";
		boolean checkOn = true;		// Bandera para activar primera opcion
	 	String[] arrDatosQueryRenglon = new String[11];
		tablaEmpl = html.generaEncabezadoBusqueda();

//		System.out.println("CatNominaAction - consultaEmpleados $ Tamano del Arreglo : " + arrDatosQuery.length);

		if(arrDatosQuery[0][0].equals(CatNominaConstantes._ERROR)){
			tablaEmpl = "";
		}
		else {
			//Es ultima Pagina?
			if( numPagina == numPaginas ){
				//El boque final es mayor que el numero de registros?
				if( finBloque > totalReg ){
					finBloque = totalReg;
				}
			}

			for(int i=inicioBloque;i<finBloque;i++)
			{
				for(int j=0;j<arrDatosQuery[0].length;j++)
				{
					arrDatosQueryRenglon[j] = arrDatosQuery[i][j];
				}
				tablaEmpl += html.generaTablaEmpleados(arrDatosQueryRenglon, checkOn);
				checkOn = false;
			}
			tablaEmpl += "</table>";
			tablaEmpl += "</center>";
		}
		EIGlobal.mensajePorTrace("CatNominaAction - [consultaEmpleados] $ FIN", EIGlobal.NivelLog.INFO);
		return tablaEmpl;
	}



	/***************************************************************
	 *** M?todo que se encarga de traer la informaci?n 			****
	 *** referente al empleado seleccionado	para realizar		****
	 *** la modificaci?n. 										****
	 **************************************************************/
	public boolean consultaDatosEmpleado (String numContrato, String numCuenta, HttpServletRequest request,HttpSession sessionHttp, String tipoCuenta)
	{
//		System.out.println("CatNominaAction - consultaDatosEmpleado $ Ingresamos a consultaDatosEmpleado");
		EIGlobal.mensajePorTrace("CatNominaAction - consultaDatosEmpleado $ Ingresamos a consultaDatosEmpleado", EIGlobal.NivelLog.INFO);
		boolean retorno = false;
		try
			{
		CatNominaDAO dao = new CatNominaDAO();
		int totalInternas = 0;
		String[] datosEmpl = dao.obtenDatosEmplModif(numContrato, numCuenta, tipoCuenta);
		String tipoModif = "PROPIA"; 	// PROPIA = el empl tiene al menos una cuenta interna INTERB = no tiene cuenta interna

				if(datosEmpl[0] != null)
				{

					if(!"ERROR0000".equals(datosEmpl[0]))
					{
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ Cuenta Abono " + datosEmpl[0]);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ Num Empl " + datosEmpl[1]);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ Num Depto " + datosEmpl[2]);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ sueldo " + datosEmpl[3]);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ nombre " + datosEmpl[4]);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ numContrato " + numContrato);
//						System.out.println("CatNominaAction - consultaDatosEmpleado $ tipoCuenta [" + tipoCuenta + "]");

						if(tipoCuenta.equals(CatNominaConstantes._CTA_INTERB)) 		//Si es interbancaria verificamos que el empl no tenga una cuenta interna asociada
						{
//							System.out.println("CatNominaAction - consultaDatosEmpleado $ Entramos porue tipoCuenta es igual a interb");
							totalInternas = dao.existeInternaEmplInterb(numCuenta);
//							System.out.println("CatNominaAction - consultaDatosEmpleado $ NUM DE INTERNAS = " + totalInternas);

							if(totalInternas != -100){
								if(totalInternas == 0)
								{
//									System.out.println("CatNominaAction - consultaDatosEmpleado $ Entramos porque no tiene una cuenta interna relacionada");
									tipoModif = "INTERB";
									request.setAttribute("rnombre", datosEmpl[4]);
									request.setAttribute("rapellPat", datosEmpl[5]);
									request.setAttribute("rapellMat", datosEmpl[6]);
									request.setAttribute("rRFC", datosEmpl[7]);
									request.setAttribute("rsexo", datosEmpl[8]);
									request.setAttribute("rcalleNum", datosEmpl[9]);
									request.setAttribute("rColonia", datosEmpl[10]);
									request.setAttribute("rdelegMun", datosEmpl[11]);
									request.setAttribute("rcveEdo", datosEmpl[12]);
									request.setAttribute("rciuPob", datosEmpl[13]);
									request.setAttribute("rcodPos", datosEmpl[14]);
									request.setAttribute("rcvePais", datosEmpl[15]);
									request.setAttribute("rprefPart", datosEmpl[16]);
									request.setAttribute("rnumPart", datosEmpl[17]);

									request.setAttribute("rcalleNumOf", datosEmpl[18]);
									request.setAttribute("rColoniaOf", datosEmpl[19]);
									request.setAttribute("rdelegMunOf", datosEmpl[20]);
									request.setAttribute("rcveEdoOf", datosEmpl[21]);
									request.setAttribute("rciuPobOf", datosEmpl[22]);
									request.setAttribute("rcodPosOf", datosEmpl[23]);
									request.setAttribute("rcvePaisOf", datosEmpl[24]);
									request.setAttribute("rDireccOf", datosEmpl[25]);
									request.setAttribute("rprefOf", datosEmpl[26]);
									request.setAttribute("rnumOf", datosEmpl[27]);
									request.setAttribute("rextOf", datosEmpl[28]);
									request.setAttribute("rtipoCuenta", datosEmpl[29]);
								}
//								System.out.println("CatNominaAction - consultaDatosEmpleado $ tiene cuentas relacionadas");
							}
							else{
								retorno = false;
							}
						}
						request.setAttribute("rcuentaAbono", datosEmpl[0]);
						request.setAttribute("rnoEmpleado", datosEmpl[1]);
						request.setAttribute("rdepto", datosEmpl[2]);
						request.setAttribute("ringreso", datosEmpl[3]);
						request.setAttribute("rcontrato", numContrato);
						request.setAttribute("rtipoCuenta", tipoModif);



					retorno = true;
				}

				else{
					retorno = false;
				}

			}
			else{
//				System.out.println("El resultado de la consulta es vacio");
				retorno = false;
			}
		}


		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			retorno = false;
		}


//		System.out.println("CatNominaAction - consultaDatosEmpleado $ Terminamos");
		EIGlobal.mensajePorTrace("CatNominaAction - consultaDatosEmpleado $ Terminamos", EIGlobal.NivelLog.INFO);
		return retorno;

	}



	/******************************************************************
	 *** M?todo que se encarga de modificar los datos del empleado 	***
	 *** con la nueva informaci?n que ha sido proporcionada			***
	 ******************************************************************/
	public boolean modificaEmpleado(String cuentaA, String usuario, HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("같같같같같같같 Entro CatNominaAction::modificaEmpleado", EIGlobal.NivelLog.INFO);
//		System.out.println("CatNominaAction - modificaEmpleado $ Ingresamos a modificaEmpleado");
//		System.out.println("CatNominaAction - modificaEmpleado $ " + cuentaA);
		String tipoCuenta = request.getParameter("rtipoCuenta");
//		System.out.println("CatNominaAction - TipoCuenta $ " + tipoCuenta);
		boolean retorno = false;
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaEmpleado empleado a modificar: cuentaA ->" + cuentaA, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaEmpleado tipoCuenta ->" + tipoCuenta, EIGlobal.NivelLog.INFO);

		/* jp@everis */
		String[] datosModif = null;
		CatNominaDAO dao = new CatNominaDAO();
		/* jp@everis */
		if("PROPIA".equals(tipoCuenta)){
			System.out.println("PROPIA");
			datosModif = new String[5];
			datosModif [0] = request.getParameter("NoEmpleado");
			datosModif [1] = request.getParameter("Depto");
			datosModif [2] = request.getParameter("Ingreso");
			datosModif [3] = request.getParameter("Contrato");
			datosModif [4] = cuentaA;
		}
		else{
			System.out.println("INTBANC");
			datosModif = new String[30];
			/* tabla: EWEB_DET_CAT_NOM */
			datosModif [0] = request.getParameter("NoEmpleado");
			datosModif [1] = request.getParameter("Depto");
			datosModif [2] = request.getParameter("Ingreso");
			datosModif [3] = request.getParameter("Contrato");

			/* tabla: EWEB_DET_CAT_NOM */
			datosModif [4] = request.getParameter("nombre");
			datosModif [5] = request.getParameter("apellidoP");
			datosModif [6] = request.getParameter("apellidoM");
			datosModif [7] = request.getParameter("rfc");
			datosModif [8] = request.getParameter("sexo");
			datosModif [9] = request.getParameter("calleNumero");
			datosModif [10] = request.getParameter("colonia");
			datosModif [11] = request.getParameter("delegMunicipio");
			datosModif [12] = request.getParameter("claveEstado");
			datosModif [13] = request.getParameter("ciudadPob");
			datosModif [14] = request.getParameter("codigoPostal");
			datosModif [15] = request.getParameter("clavePais");
			datosModif [16] = request.getParameter("prefTelCasa");
			datosModif [17] = request.getParameter("telCasa");

			/* tabla: EWEB_CONTR_CAT_NOM */
			datosModif [18] = request.getParameter("calleNumOfi");
			datosModif [19] = request.getParameter("coloniaOfi");
			datosModif [20] = request.getParameter("delegMunOfi");
			datosModif [21] = request.getParameter("claveEstadoOfi");
			datosModif [22] = request.getParameter("ciudadPobOfi");
			datosModif [23] = request.getParameter("codigoPostalOfi");
			datosModif [24] = request.getParameter("clavePaisOfi");
			datosModif [25] = request.getParameter("claveDireccion");
			datosModif [26] = request.getParameter("prefTelOfi");
			datosModif [27] = request.getParameter("telOfi");
			datosModif [28] = request.getParameter("extOfi");

			datosModif [29] = cuentaA;
		}
		/* jp@everis */

		try {
			retorno = dao.actualizaDatosEmpl(datosModif, usuario);
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			retorno = false;
		}
		return retorno;
	}


	/***************************************************************
	 *** Metodo que realiza la baja de cuentas de un empleado	****
	 *** rompiendo la relaci?n contrato-cuenta en l?nea			****
	 **************************************************************/
	public boolean bajaCtaEmplOnline (String numCuenta, String numContrato, String usuario)
	{
		EIGlobal.mensajePorTrace("같같같같같같같 Entro CatNominaAction::bajaCtaEmplOnline", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplOnline numCuenta ->" + numCuenta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplOnline numContrato ->" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplOnline usuario ->" + usuario, EIGlobal.NivelLog.INFO);
//		System.out.println("CatNominaAction - bajaCtaEmplOnline $ Inicio");
//		System.out.println("CatNominaAction - bajaCtaEmplOnline $ numeroCuenta: " + numCuenta);
//		System.out.println("CatNominaAction - bajaCtaEmplOnline $ numeroContrato " + numContrato);
		CatNominaDAO dao = new CatNominaDAO();
		boolean esBaja = false;

		try
		{
			int bajaCuenta = dao.bajaCuentaContrato(numContrato, numCuenta, usuario);

			if(bajaCuenta != -100){
				if (bajaCuenta > 0) {
					System.out.println("CatNominaAction - [bajaCtaEmplOnline] $ La baja fue exitosa");
					esBaja = true;
				}
			}
			else{
				esBaja = false;
			}
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			esBaja = false;
		}
		EIGlobal.mensajePorTrace("CatNominaAction - [bajaCtaEmplOnline] $ La baja fue Exitosa? : " + esBaja, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaAction - [bajaCtaEmplOnline] $ FIN", EIGlobal.NivelLog.INFO);
		return esBaja;
	}



	/**************************************************************************************/
	/** Metodo: bajaLogica
	 * @param String
	 * @description: Hace una consulta de los numeros de cuenta mandodos por medio de un arcivo
	 *               despues de consultarlos realiza un baja logica cambiando el status de las
	 *               cuentas
	 *
	/*************************************************************************************/
	public boolean bajaCtaEmplArchivo(String numContrato, String usuario, ArrayList listaBaja)
	{
		EIGlobal.mensajePorTrace("같같같같같같같 Entro CatNominaAction::bajaCtaEmplArchivo", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplArchivo numContrato ->" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplArchivo usuario ->" + usuario, EIGlobal.NivelLog.INFO);
//		System.out.println("CatNominaAction - bajaCtaEmplArchivo $ Inicio");
		CatNominaDAO dao = new CatNominaDAO();
		boolean bajaOK = false;
		int bajaCuenta = 0;
		int contador = 0;

		try
		{
//			System.out.println("CatNominaAction - bajaCtaEmplArchivo $ size lista baja " + listaBaja.size());
			EIGlobal.mensajePorTrace("CatNominaAction - bajaCtaEmplArchivo $ size lista baja " + listaBaja.size(), EIGlobal.NivelLog.INFO );
			for(int i=0; i < listaBaja.size() ; i++)
			{
				EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::bajaCtaEmplArchivo listaBaja["+ i + " ->" + listaBaja.get(i), EIGlobal.NivelLog.INFO);
//				System.out.println("CatNominaAction - bajaCtaEmplArchivo $ listaBaja Cuenta " + listaBaja.get(i));
				bajaCuenta = dao.bajaCuentaContrato(numContrato, (String)listaBaja.get(i), usuario);

				if(bajaCuenta != -100){

					if (bajaCuenta != 0){
//						System.out.println("CatNominaAction - [bajaCtaEmplOnline] $ Se realizo baja");
						EIGlobal.mensajePorTrace("CatNominaAction - [bajaCtaEmplOnline] $ Se realizo baja",EIGlobal.NivelLog.INFO);
					}
					else{
//						System.out.println("No se actualizo ningun dato1");
						EIGlobal.mensajePorTrace("No se actualizo ningun dato1", EIGlobal.NivelLog.INFO);
						contador = contador + 1;
					}
				}
				else{
					contador = contador + 1;
				}
			}
			if(contador == 0){
				bajaOK = true;
			}
			else{
				bajaOK = false;
			}
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
//			System.out.println(e.getMessage());
			bajaOK = false;
		}
//		System.out.println("CatNominaAction - bajaCtaEmplArchivo $ FIn");
		EIGlobal.mensajePorTrace("CatNominaAction - bajaCtaEmplArchivo $ FIn", EIGlobal.NivelLog.INFO);
		return bajaOK;
	}


	/*******************************************************************
	 *	M?todo que se encarga de generar una tabla con el detalle del
	 *	archivo que se est? importando que contiene las modificaciones
	 *	de datos de empleados para el nvo. Cat?logo de n?mina
	 *******************************************************************/
	public String creaDetalleImport(String numContrato, ArrayList listaRegistros, String tipoCarga)
	{
//		System.out.println("CatNominaAction - creaDetalleImportModif() - Inicio");
		EIGlobal.mensajePorTrace("CatNominaAction - creaDetalleImportModif() - Inicio",EIGlobal.NivelLog.INFO);
		CatNominaHTML html = new CatNominaHTML();
		String tablaDetalle = "";
		int totalReg = 0;

		tablaDetalle = html.encabezadoImportaciones(tipoCarga);

		Iterator it = listaRegistros.iterator();
		String dato = "";
		while (it.hasNext()) {
			dato = (String)it.next();
//			System.out.println("CatNominaAction - creaDetalleImportModif() - Entramos al next " + dato );

			if(tipoCarga.equals(CatNominaConstantes._MODIF)){
				tablaDetalle += separaCadenaModif(numContrato, dato);
			}
			else{
				tablaDetalle += html.generaTablaBaja(dato);
			}
		}
//		System.out.println("CatNominaAction - creaDetalleImportModif() $ termina iterator" );
		totalReg = (listaRegistros.size());

		tablaDetalle += html.generaFinalTablaModif(totalReg);

//		System.out.println("CatNominaAction - creaDetalleImportModif() $ FIN" );
		EIGlobal.mensajePorTrace("CatNominaAction - creaDetalleImportModif() $ FIN", EIGlobal.NivelLog.INFO );
		return tablaDetalle;
	}


	/**********************************************************
 	 * Metodo que lee el registro y realiza la actualizaci?n
 	 * en el nuevo cat?logo de n?mina
	 **********************************************************/
	public String separaCadenaModif(String numContrato, String registro)
	{
//		System.out.println("CatNominaAction - separaCadenaModif() $ Inicio");
//		System.out.println("CatNominaAction - separaCadenaModif() $ Registro " + registro);
		EIGlobal.mensajePorTrace("CatNominaAction - separaCadenaModif() $ Inicio", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaAction - separaCadenaModif() $ Registro " + registro, EIGlobal.NivelLog.INFO);
		String tablaDetalle = "";
		String enteros = "";
		String decimales = "";
		String[] arrDatosModif = new String[4];
		CatNominaDAO dao = new CatNominaDAO();
		CatNominaHTML html = new CatNominaHTML();
		//String[] arrDatosActual = new String[4];
		arrDatosModif[0] = (registro.substring(0, 18)).trim();  // Numero de Cuenta
		arrDatosModif[1] = (registro.substring(18, 25)).trim();	// NUm de Empleado
		arrDatosModif[2] = (registro.substring(25, 31)).trim();	// Num depto

		// Modificacion para Sueldos//
		enteros =  String.valueOf(registro.substring(31, 49)).trim();
		decimales =  String.valueOf(registro.substring(49, 51)).trim();
//		System.out.println("CatNominaAction - separaCadenaModif() $ Enteros" + enteros);
//		System.out.println("CatNominaAction - separaCadenaModif() $ Decimales"+ decimales);

		if("".equals(decimales))
		{
			arrDatosModif[3] = enteros + "." + "00";
//			System.out.println("CatNominaAction - separaCadenaModif() $ sueldo sin decimanles: " + arrDatosModif[3]);
		}
		else
		{
			arrDatosModif[3] = enteros + "." + decimales;
//			System.out.println("CatNominaAction - separaCadenaModif() $ sueldo con decimanles: " + arrDatosModif[3]);
		}
		// Fin Modificacion para Sueldos //

		tablaDetalle += html.generaTablaModif(arrDatosModif);

	//	System.out.println("CatNominaAction - separaCadenaModif() $ Table Detalle" + tablaDetalle);
//		System.out.println("CatNominaAction - separaCadenaModif() $ Fin");
		EIGlobal.mensajePorTrace("CatNominaAction - separaCadenaModif() $ Fin", EIGlobal.NivelLog.INFO);

		return tablaDetalle;
	}


	/***************************************************************
	 ***             modificaEmpleadoPorArchivo()
	 ***  M?todo encargado de leer cada registro del archivo
	 ***  y enviarlo a otros m?todos de modificaci?n
	 **************************************************************/
	public boolean modificaDatosArchivo(String numContrato, String usuario, ArrayList listaModificaciones)
	{
		EIGlobal.mensajePorTrace("같같같같같같같 Entro CatNominaAction::modificaDatosArchivo", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaDatosArchivo numContrato ->" + numContrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaDatosArchivo usuario ->" + usuario, EIGlobal.NivelLog.INFO);
//		System.out.println("CatNominaAction - modificaDatosArchivo $ Inicio");
		CatNominaDAO dao = new CatNominaDAO();
		String[] arrModificaciones = new String[5]; 	// Solo trae 5 campos
		String renglon = "";
		String enteros = "";
		String decimales = "";
		String[] arrDatosActual = new String[4];

		boolean retorno = false;

		try {
			Iterator it = listaModificaciones.iterator();
			while (it.hasNext()) {
				renglon = (String)it.next();

//				System.out.println("CatNominaAction - modificaDatosArchivo $ Renglon" + renglon);
				arrModificaciones[4] = (renglon.substring(0, 18)).trim(); // Cuenta abono
				arrModificaciones[0] = (renglon.substring(18, 25)).trim();// num empl
				arrModificaciones[1] = (renglon.substring(25, 31)).trim();// depto empl
				arrModificaciones[3] = numContrato;
				EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaDatosArchivo Cuenta abono ->" + arrModificaciones[4], EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaDatosArchivo num empl ->" + arrModificaciones[0], EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("같같같같같같같 CatNominaAction::modificaDatosArchivo depto empl ->" + arrModificaciones[1], EIGlobal.NivelLog.INFO);

				// Modificacion para Sueldos//
				enteros =  String.valueOf(renglon.substring(31, 49)).trim();
				decimales =  String.valueOf(renglon.substring(49, 51)).trim();
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ Enteros" + enteros);
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ Decimales"+ decimales);

				if("".equals(decimales))
				{
					arrModificaciones[2] = enteros + "." + "00";
//					System.out.println("CatNominaAction - modificaDatosArchivo() $ sueldo sin decimanles: " + arrModificaciones[2]);
				}
				else
				{
					arrModificaciones[2] = enteros + "." + decimales;
//					System.out.println("CatNominaAction - modificaDatosArchivo() $ sueldo con decimanles: " + arrModificaciones[2]);
				}
				//arrModificaciones[2] = (renglon.substring(31, 51)).trim();
				// Fin Modificacion para Sueldos //


				//Cambio para enviar valores not null //
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ Valores que vienen del archivo");
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ 0antes: " + arrModificaciones[0]);
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ 1antes: " + arrModificaciones[1]);
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ 2antes: " + arrModificaciones[2]);
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ 3antes: " + arrModificaciones[3]);
//				System.out.println("CatNominaAction - modificaDatosArchivo() $ 4antes: " + arrModificaciones[4]);

				String tipoCuenta = dao.obtenTipoCuenta(numContrato, arrModificaciones[4]);  // nuevo Cambio

				if(!"ERROR0000".equals(tipoCuenta)) {

					arrDatosActual = dao.obtenDatosEmplModif(numContrato, arrModificaciones[4], tipoCuenta);//CtaAbono, NumEmpl, DeptoEmpl, Sueldo, []


					if(!"ERROR0000".equals(arrDatosActual))
					{

//						System.out.println("CatNominaAction - separaCadenaModif() $ Everis Cambio para tabla: " + arrDatosActual.length);

					/*	for(int i=0;i<arrDatosActual.length; i++)
						{
							System.out.println("CatNominaAction - separaCadenaModif() $ Everis Trae datos de Oracle: " + arrDatosActual[i]);
						}*/

						int i=0;	//Posicion Arreglo
						int indiceActuales = 1;

						// NUM EMPL
						if(null == arrModificaciones[i] || "".equals(arrModificaciones[i]))
						{
							if( arrDatosActual[indiceActuales] == null ){
								arrDatosActual[indiceActuales] = "";
							}
							arrModificaciones[i] = arrDatosActual[indiceActuales];
						}
						i++;
						indiceActuales++;

						//depto_empl
						if(null == arrModificaciones[i] || "".equals(arrModificaciones[i]))
						{
							if(arrDatosActual[indiceActuales] == null){
								arrDatosActual[indiceActuales] = "";
							}
							arrModificaciones[i] = arrDatosActual[indiceActuales];
						}
						i++;
						indiceActuales++;

						//sueldo
						if(null == arrModificaciones[i] || "".equals(arrModificaciones[i]))
						{
							if(arrDatosActual[indiceActuales] == null){
								arrDatosActual[indiceActuales] = "";
							}
							arrModificaciones[i] = arrDatosActual[indiceActuales];
						}





						retorno = dao.actualizaDatosEmpl(arrModificaciones, usuario);

					 //retorno = true;


					} //Fin del if if(arrDatosActual = dao.obtenDatosEmplModif(numContrato, arrModificaciones[4], tipoCuenta))

					else{
						retorno = false;
						}
				}
				else{
					retorno = false;
				}

			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			retorno = false;

		}
		//System.out.println("CatNominaAction - modificaDatosArchivo $ Fin");
		//return true;
		return retorno;
	}



	public boolean realizaAltaEmplInterb(CatNominaBean bean)
	{
//		System.out.println("CatNominaAction - realizaAltaEmplInterb $ Inicio ");
		EIGlobal.mensajePorTrace("CatNominaAction - realizaAltaEmplInterb $ Inicio ", EIGlobal.NivelLog.INFO);
		boolean altaExitosa = false;
		int respAlta = 0;
		int cuentaReg = 0;

		CatNominaDAO dao = new CatNominaDAO();

		respAlta = dao.altaInterbOnlineCatNom(bean);

		if(respAlta != -100){

			cuentaReg = dao.buscaContrato(bean.getNumContrato());

			if(cuentaReg != -100){

				if(cuentaReg == 0){
					respAlta = dao.altaInterbOnlineContrCat(bean);
					if(respAlta == -100){
						altaExitosa = false;
					}
				}


				respAlta = dao.altaInterbOnlineDetCat(bean);

				//System.out.println("CatNominaAction - realizaAltaEmplInterb $ respAlta:  " + respAlta);

				if(respAlta != -100){

					/****** Env?o de Notificaci?n *****/
					ArrayList cuentas = new ArrayList();
					cuentas.add(bean.getNumCuenta());
					notificaAltaCtaInterb(cuentas, bean.getNumContrato(), bean.getDescContrato(), bean.getCveUsuario());
					cuentas.clear();
					cuentas = null;

					/****** Fin Env?o de Notificaci?n *****/

					if(respAlta>0){
						altaExitosa = true;

					//System.out.println("CatNominaAction - realizaAltaEmplInterb $ se realizo alta = "+ altaExitosa );
					//System.out.println("CatNominaAction - realizaAltaEmplInterb $ Fin " );
					}
				}
				else{
					altaExitosa = false;
				}
			}
			else{
				altaExitosa = false;
			}

		}
		else{
			altaExitosa = false;
		}

		return altaExitosa;
	}


	/**************************************************************************************/
	/** Metodo: validaLineaModif
	 * @param String, int
	 * @description: Hace la validaci?n del tamano de la linea del archivo
	 * 				 verifica que el tamano de la linea sea solo de 11 ? 18 numeros
	/*************************************************************************************/
	public CatNominaBean validaArchivoModif(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
//		System.out.println("CatNominaAction - validaArchivoModif $ Inicio ");

		EIGlobal.mensajePorTrace("CatNominaAction - validaArchivoModif $ Inicio ", EIGlobal.NivelLog.INFO);
		CatNominaBean bean = new CatNominaBean();
		String linea = "";
		ArrayList listaErrorCuentas = new ArrayList();
		ArrayList listaErrorLong = new ArrayList();
		ArrayList listaErrorDatos = new ArrayList();
		ArrayList listaErrores = new ArrayList();
		ArrayList listaRegistros = new ArrayList();
		ArrayList listaErrorCampo = new ArrayList();


		RandomAccessFile archivoModif;
		int i = 0;

		try {
			File f = new File(archivoCargado);
			archivoModif = new RandomAccessFile(f, "r");
			String registroLeido = "";
			String numCta = "";
			String numEmpl = "";
			String deptoEmpl = "";
			String ingresoMensual = "";
			long posicion = 0;
			int existeRelacion = 0;

			listaErrorLong.add(new String("Long"));
			listaErrorCuentas.add(new String("Cuentas"));
			listaErrorDatos.add(new String("Datos"));

			listaErrorCampo.add(new String("Campo"));


			if (archivoModif.length() > 0) {
				archivoModif.seek(0);
				do {
					i++;
					registroLeido = archivoModif.readLine();

					//Validamos que la l?nea tenga la longitud permitida de 51 caracteres

				if (registroLeido.length() == 51) {
//						System.out.println("CatNominaAction - validaArchivoModif $ Longitud Correcta ");
						CatNominaValidaImport valida = new CatNominaValidaImport();
						numEmpl = registroLeido.substring(18, 25).trim();
						deptoEmpl = registroLeido.substring(25, 31).trim();
						ingresoMensual = registroLeido.substring(31, 51).trim();

						if (valida.validaCampoTexto(deptoEmpl) && valida.validaCampoTexto(numEmpl) && valida.validaCampoNumero(ingresoMensual))
						{

							if (!"".equals(numEmpl)){

//								System.out.println("CatNominaAction - validaArchivoModif $ Datos Correctos ");

								if("".equals(ingresoMensual)){
									listaErrorCampo.add(new String("Salario@" + i ));
								}


//								System.out.println("CatNominaAction - validaArchivoModif $ Datos Correctos ");
								numCta = registroLeido.substring(0, 18).trim();
//								System.out.println("CatNominaAction - validaArchivoModif $ Num Cuenta a validar = " + numCta);

								if (!"".equals(numCta)){

									CatNominaDAO dao = new CatNominaDAO();
									existeRelacion = dao.existeRelacion(numContrato, numCta);

									if(existeRelacion>0){
//										System.out.println("CatNominaValidaImport - validaLineaModif $ Relaci?n Existente - Linea VALIDA " + registroLeido );
										listaRegistros.add(new String(registroLeido));
									}
									else if (existeRelacion == 0){
//										System.out.println("CatNominaAction - validaArchivoModif $No existe relacion ");
										listaErrorCuentas.add(new Integer(i));
									}
									else{
										bean.setFalloInesperado(true);
									}

							    }

							   else{
//									System.out.println("*******CatNominaValidaInterb - validaArchivoAlta $ Datos Obligatotios******* ");
									listaErrorCampo.add(new String("Cuenta abono@" + i ));

								}

						     }
							else{
//								System.out.println("*******CatNominaValidaInterb - validaArchivoAlta $ Datos Obligatotios******* ");
							      listaErrorCampo.add(new String("Numero de Empleado@" + i ));
							}


						}
						else {
//							System.out.println("CatNominaAction - validaArchivoModif $ Datos incorrectos ");
							listaErrorDatos.add(new Integer(i));
						}
					}
					else {
//						System.out.println("CatNominaAction - validaArchivoModif $ Longitud INCorrecta ");
						listaErrorLong.add(new Integer(i));
					}
					posicion = archivoModif.getFilePointer();

				} while (posicion < archivoModif.length());
				bean.setTotalRegistros(i);
				bean.setListaRegistros(listaRegistros);
			}
//			System.out.println("CatNominaValidaImport - validaLineaModif $ Longitud " + listaErrorLong.size());
//			System.out.println("CatNominaValidaImport - validaLineaModif $ Datos " + listaErrorDatos.size());
//			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuentas " + listaErrorCuentas.size());
			EIGlobal.mensajePorTrace("CatNominaValidaImport - validaLineaModif $ Longitud " + listaErrorLong.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CatNominaValidaImport - validaLineaModif $ Datos " + listaErrorDatos.size(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CatNominaValidaImport - validaLineaModif $ Cuentas " + listaErrorCuentas.size(), EIGlobal.NivelLog.INFO);
		archivoModif.close();
		f.delete();
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

//		System.out.println("CatNominaValidaImport - validaLineaModif $ errores " + linea);
		EIGlobal.mensajePorTrace("CatNominaValidaImport - validaLineaModif $ errores " + linea, EIGlobal.NivelLog.INFO);
		if(listaErrorLong.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorLong;
		}

		else if(listaErrorDatos.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorDatos;
		}

		else if(listaErrorCampo.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorCampo;
		}

		else if(listaErrorCuentas.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorCuentas;
		}

		bean.setListaErrores(listaErrores);

		return bean;
	}



	/**************************************************************************************/
	/** Metodo: validaLineaModif
	 * @param String, int
	 * @description: Hace la validaci?n del tamano de la linea del archivo
	 * 				 verifica que el tamano de la linea sea solo de 11 ? 18 numeros
	/*************************************************************************************/
	public CatNominaBean validaArchivoBaja(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
//		System.out.println("CatNominaAction - validaArchivoBaja $ Inicio ");
		EIGlobal.mensajePorTrace("CatNominaAction - validaArchivoBaja $ Inicio ", EIGlobal.NivelLog.INFO);

		File f = new File(archivoCargado);
		RandomAccessFile raf = new RandomAccessFile(f, "r");
		String lineaActual = "";			// Contenido de la linea
		int numLinea = 0;					// num de Linea en el archivo
		boolean lineaValida = false;
		CatNominaValidaImport valida = new CatNominaValidaImport();
		CatNominaDAO dao = new CatNominaDAO();
		String ctaAbono = "";
		int existRelac = 0;
		ArrayList listaRegistrosBaja = new ArrayList();
		ArrayList listaErrorLong = new ArrayList();
		ArrayList listaErrorFormato = new ArrayList();
		ArrayList listaErrorCuenta = new ArrayList();
		ArrayList listaErrores = new ArrayList();
		listaErrorCuenta.add(new String ("Cuenta"));
		listaErrorLong.add(new String ("Long"));
		listaErrorFormato.add(new String ("Formato"));
		lineaActual = raf.readLine();
		CatNominaBean bean = new CatNominaBean();

		while (lineaActual != null)
		{
			numLinea++;
//			System.out.println("CatNominaAction - validaArchivoBaja - Num Linea = "+ numLinea);
//			System.out.println("CatNominaAction - validaArchivoBaja - Linea = "+ lineaActual);

			if (lineaActual.length() == 10 || lineaActual.length() == 11 || lineaActual.length() == 18) {
//				System.out.println("CatNominaAction - validaArchivoBaja - La longuitud es correcta");
				lineaValida = valida.validaCampoNumero(lineaActual);

				if (lineaValida)
				{
					ctaAbono = lineaActual.trim();
//					System.out.println("CatNominaAction - validaArchivoBaja - La linea es Valida");
					existRelac = dao.existeRelacion(numContrato, ctaAbono);

					if(existRelac > 0){
						listaRegistrosBaja.add(new String(ctaAbono));
					}
					else if(existRelac == 0){
//						System.out.println("CatNominaAction - validaArchivoBaja - Cuenta no Existente Linea: " + numLinea);
						listaErrorCuenta.add(new Integer(numLinea));
					}
					else{
						bean.setFalloInesperado(true);
					}

				}
				else {
//					System.out.println("CatNominaAction - validaArchivoBaja - Problema en formato linea " + numLinea);
					listaErrorFormato.add(new Integer(numLinea));
				}
			}
			else {
//				System.out.println("CatNominaAction - validaArchivoBaja - Problema en longitud linea " + numLinea);
				listaErrorLong.add(new Integer(numLinea));
			}
			lineaActual = raf.readLine();
//			System.out.println("CatNominaAction - validaArchivoBaja $  Nueva Linea a leer " + lineaActual);
		}
		raf.close();
		f.delete();

		bean.setTotalRegistros(numLinea);
		bean.setListaRegistros(listaRegistrosBaja);
//		System.out.println("CatNominaAction - validaArchivoBaja $ se cerro el archivo");

		if(listaErrorLong.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorLong;
		}

		else if(listaErrorFormato.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorFormato;
		}

		else if(listaErrorCuenta.size() > 1) {
			if(listaErrores != null) {
				listaErrores.clear();
			}
			listaErrores = listaErrorCuenta;
		}

		bean.setListaErrores(listaErrores);
//		System.out.println("CatNominaAction - validaArchivoBaja $ Fin");
		EIGlobal.mensajePorTrace("CatNominaAction - validaArchivoBaja $ Fin", EIGlobal.NivelLog.INFO);
		return bean;
	}



	/*******************************************************************
	 *	M?todo que se encarga de generar una tabla con el detalle del
	 *	archivo que se est? importando para dar de Alta en el nvo. Cat?logo de n?mina
	 *******************************************************************/
	public String creaDetalleAltaImp(String numContrato, ArrayList listaRegistros, String tipoCarga)
	{

		CatNominaHTML html = new CatNominaHTML();
		//CatNominaConstantes cons = new CatNominaConstantes();
		String tablaDetalle = "";
		int totalReg = 0;

		tablaDetalle = html.encabezadoImporAltaInterb(tipoCarga);

		Iterator it = listaRegistros.iterator();
		String dato = "";
		while (it.hasNext()) {
			dato = (String)it.next();

			if(tipoCarga.equals(CatNominaConstantes._ALTA_INTERB)){
				tablaDetalle += separaCadenaAlta(numContrato, dato);
			}

		}
		//System.out.println("CatNominaAction - creaDetalleAltaImp() $ termina iterator" );
		totalReg = (listaRegistros.size());

		tablaDetalle += html.generaTablaFinalAlta(totalReg);

		return tablaDetalle;
	}


	/**********************************************************
 	 * Metodo que lee el registro y los agrega
 	 * en el nuevo cat?logo de n?mina
	 **********************************************************/
	public String separaCadenaAlta(String numContrato, String registro)
	{

//		System.out.println("CatNominaAction - separaCadenaAlta() $ Registro " + registro);
		EIGlobal.mensajePorTrace("CatNominaAction - separaCadenaAlta() $ Registro " + registro, EIGlobal.NivelLog.INFO);
		String tablaDetalle = "";
		String enteros = "";
		String decimales = "";
		String[] arrDatosAlta = new String[29];
		CatNominaDAO dao = new CatNominaDAO();
		CatNominaHTML html = new CatNominaHTML();
		String[] arrDatosActual = new String[27];

		arrDatosAlta[0] = (registro.substring(0, 18)).trim();
		arrDatosAlta[1] = (registro.substring(18, 25)).trim();
		arrDatosAlta[2] = (registro.substring(25, 31)).trim();

		//arrDatosAlta[3] = String.valueOf((registro.substring(31, 51)).trim());



		// Modificacion para Sueldos//
		enteros =  String.valueOf(registro.substring(31, 49)).trim();
		decimales =  String.valueOf(registro.substring(49, 51)).trim();

		if("".equals(decimales))
		{
			arrDatosAlta[3] = enteros + "." + "00";
//			System.out.println("CatNominaAction - separaCadenaAlta() $ sueldo sin decimanles: " + arrDatosAlta[3]);
		}
		else
		{
			arrDatosAlta[3] = enteros + "." + decimales;
//			System.out.println("CatNominaAction - separaCadenaAlta() $ sueldo con decimanles: " + arrDatosAlta[3]);
		}
		// Fin Modificacion para Sueldos //

		arrDatosAlta[4] = (registro.substring(51, 81)).trim();
		arrDatosAlta[5] = (registro.substring(81, 111)).trim();
		arrDatosAlta[6] = (registro.substring(111, 141)).trim();
		arrDatosAlta[7] = (registro.substring(141, 154)).trim();
		arrDatosAlta[8] = (registro.substring(154, 155)).trim();
		arrDatosAlta[9] = (registro.substring(155, 215)).trim();
		arrDatosAlta[10] = (registro.substring(215, 245)).trim();
		arrDatosAlta[11] =	(registro.substring(245, 280)).trim();
		arrDatosAlta[12] = (registro.substring(280, 284)).trim();
		arrDatosAlta[13] =	(registro.substring(284, 319)).trim();
		arrDatosAlta[14] = (registro.substring(319, 324)).trim();
		arrDatosAlta[15] =	(registro.substring(324, 328)).trim();
		arrDatosAlta[16] = (registro.substring(328, 340)).trim();
		arrDatosAlta[17] = String.valueOf((registro.substring(340, 352)).trim());
		arrDatosAlta[18] = (registro.substring(352, 412)).trim();
		arrDatosAlta[19] =	(registro.substring(412, 442)).trim();
		arrDatosAlta[20] = (registro.substring(442, 477)).trim();
		arrDatosAlta[21] =	(registro.substring(477, 481)).trim();
		arrDatosAlta[22] = (registro.substring(481, 516)).trim();
		arrDatosAlta[23] =	(registro.substring(516, 521)).trim();
		arrDatosAlta[24] =	(registro.substring(521, 525)).trim();
		arrDatosAlta[25] = String.valueOf((registro.substring(525, 532)).trim());
		arrDatosAlta[26] = String.valueOf((registro.substring(532, 544)).trim());
		arrDatosAlta[27] = String.valueOf((registro.substring(544, 556)).trim());
		arrDatosAlta[28] = String.valueOf((registro.substring(556, 568)).trim());



	/*	for(int x=0; x<arrDatosAlta.length; x++)
		{
			System.out.println("CatNominaAction - separaCadenaAlta() $ Datos separados FINALES " + arrDatosAlta[x] );
		}*/
		tablaDetalle += html.generaTablaAlta(arrDatosAlta);

		//System.out.println("CatNominaAction - separaCadenaAlta() $ Table Detalle" + tablaDetalle);

		return tablaDetalle;

	}


	/***************************************************************
	 ***             altaDatosArchivo()
	 ***  M?todo encargado de leer cada registro del archivo
	 ***  y enviarlo a otros m?todos de modificaci?n
	 **************************************************************/

	public boolean altaDatosArchivo(String numContrato, String cveUsr, String descContrato, ArrayList listaAlta)
	{
//		System.out.println("CatalogoNomina - altaDatosArchivo $ Inicio");
		EIGlobal.mensajePorTrace("CatalogoNomina - altaDatosArchivo $ Inicio", EIGlobal.NivelLog.INFO);
		CatNominaDAO dao = new CatNominaDAO();
		String[] arrAlta = new String[29]; 	// Trae todos los campos
		String renglon = "";
		String enteros = "";
		String decimales = "";
		boolean retorno = false;

		ArrayList numCuentas = new ArrayList();

		try
		{
			Iterator it = listaAlta.iterator();
			while (it.hasNext()) {
				renglon = (String)it.next();
//				System.out.println("CatNominaAction - altaDatosArchivo $ Renglon" + renglon);

				arrAlta[0] = (renglon.substring(0, 18)).trim();
				arrAlta[1] = (renglon.substring(18, 25)).trim();
				arrAlta[2] = (renglon.substring(25, 31)).trim();

				// Modificacion para Sueldos//
				enteros =  String.valueOf(renglon.substring(31, 49)).trim();
				decimales =  String.valueOf(renglon.substring(49, 51)).trim();

				if("".equals(decimales))
				{
					arrAlta[3] = enteros + "." + "00";
//					System.out.println("CatNominaAction - altaDatosArchivo() $ sueldo sin decimanles: " + arrAlta[3]);
				}
				else
				{
					arrAlta[3] = enteros + "." + decimales;
//					System.out.println("CatNominaAction - altaDatosArchivo() $ sueldo con decimanles: " + arrAlta[3]);
				}
				// Fin Modificacion para Sueldos //


				arrAlta[4] = (renglon.substring(51, 81)).trim();
				arrAlta[5] = (renglon.substring(81, 111)).trim();
				arrAlta[6] = (renglon.substring(111, 141)).trim();
				arrAlta[7] = (renglon.substring(141, 154)).trim();
				arrAlta[8] = (renglon.substring(154, 155)).trim();
				arrAlta[9] = (renglon.substring(155, 215)).trim();
				arrAlta[10] = (renglon.substring(215, 245)).trim();
				arrAlta[11] = (renglon.substring(245, 280)).trim();
				arrAlta[12] = (renglon.substring(280, 284)).trim();
				arrAlta[13] = (renglon.substring(284, 319)).trim();
				arrAlta[14] = (renglon.substring(319, 324)).trim();
				arrAlta[15] = (renglon.substring(324, 328)).trim();
				arrAlta[16] = (renglon.substring(328, 340)).trim();
				arrAlta[17] = cambiaNum(renglon.substring(340, 352)).trim();
				arrAlta[18] = (renglon.substring(352, 412)).trim();
				arrAlta[19] = (renglon.substring(412, 442)).trim();
				arrAlta[20] = (renglon.substring(442, 477)).trim();
				arrAlta[21] = (renglon.substring(477, 481)).trim();
				arrAlta[22] = (renglon.substring(481, 516)).trim();
				arrAlta[23] = (renglon.substring(516, 521)).trim();
				arrAlta[24] = (renglon.substring(521, 525)).trim();


				arrAlta[25] = cambiaNum(renglon.substring(525, 532)).trim();
				arrAlta[26] = cambiaNum(renglon.substring(532, 544)).trim();
				arrAlta[27] = cambiaNum(renglon.substring(544, 556)).trim();
				arrAlta[28] = cambiaNum(renglon.substring(556, 568)).trim();

				retorno = dao.altaInterbArchivo(arrAlta, numContrato, cveUsr);

//				System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaAction) " + retorno);

				if(retorno){
					numCuentas.add(arrAlta[0]);
				}
			}

			if(retorno)
			{
				/****** Env?o de Notificaci?n *****/
				notificaAltaCtaInterb(numCuentas, numContrato, descContrato, cveUsr);
				if(numCuentas != null) {
					numCuentas.clear();
					numCuentas = null;
				}
			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
//			System.out.println("*** CatalogoAlta - Retorno de Alta por Archivo (CatNominaAction) 1 = false");
			retorno=false;
		}
		//return true;
		return retorno;
	}





	/**************************************************************************************/
	/** Metodo: cambiaNum
	 * @param String, String
	 * @description: Obtiene el valor de un entero
	/*************************************************************************************/
	public String cambiaNum(String campo)
	{
		 try{
			if("".equals(campo)){

				return "0";
			}
			else{
				return campo;
			}
		}
		catch(Exception e){
			return "0";
		}
	}



	/*******************************************************************
	 *	M?todo que se encarga de generar una tabla con el detalle del
	 *	archivo que se est? importando que contiene las modificaciones
	 *	de datos de empleados con cuenta Interbancaria para el nvo. Cat?logo de n?mina
	 *******************************************************************/
	public String creaDetalleModifImport(String numContrato, ArrayList listaRegistros, String tipoCarga)
	{
//		System.out.println("CatNominaAction - creaDetalleModifImport() $ Inicio");
		EIGlobal.mensajePorTrace("CatNominaAction - creaDetalleModifImport() $ Inicio", EIGlobal.NivelLog.INFO);
		CatNominaHTML html = new CatNominaHTML();
		String tablaDetalle = "";
		int totalReg = 0;

		tablaDetalle = html.encabezadoImporModifInterb(tipoCarga);
//		System.out.println("**********CatNominaAction - creaDetalleModifImport() - Tama?o de ArrayList " + listaRegistros.size());

		Iterator it = listaRegistros.iterator();
		String dato = "";
		while (it.hasNext()) {
			dato = (String)it.next();

			if(tipoCarga.equals(CatNominaConstantes._MODIF)){
				tablaDetalle += separaCadenaModifInterb(numContrato, dato);
			}
		}
//		System.out.println("CatNominaAction - creaDetalleModifImport() $ termina iterator" );
		totalReg = (listaRegistros.size());

		tablaDetalle += html.generaFinalTablaModifInterb(totalReg);
//		System.out.println("CatNominaAction - creaDetalleModifImport() $ Fin");
		EIGlobal.mensajePorTrace("CatNominaAction - creaDetalleModifImport() $ Fin", EIGlobal.NivelLog.INFO);
		return tablaDetalle;
	}



	/**********************************************************
 	 * Metodo que lee el registro y realiza la actualizaci?n
 	 * en el nuevo cat?logo de n?mina para cuentas Interbancarias
	 **********************************************************/
	public String separaCadenaModifInterb(String numContrato, String registro)
	{
//		System.out.println("CatNominaAction - separaCadenaModif() $ Registro " + registro);
		EIGlobal.mensajePorTrace("CatNominaAction - separaCadenaModif() $ Registro " + registro, EIGlobal.NivelLog.INFO);
		String tablaDetalle = "";
		String enteros = "";
		String decimales = "";
		String[] arrDatosModifInterb = new String[25];
		CatNominaDAO dao = new CatNominaDAO();
		CatNominaHTML html = new CatNominaHTML();
		String[] arrDatosActual = new String[6];

		arrDatosModifInterb[0] = (registro.substring(0, 18)).trim();   // Num Cuenta
		arrDatosModifInterb[1] = (registro.substring(18, 25)).trim();	// Num Empleado
		arrDatosModifInterb[2] = (registro.substring(25, 31)).trim();	// Depto Empl

		// Modificacion para Sueldos//
		enteros =  String.valueOf(registro.substring(31, 49)).trim();
		decimales =  String.valueOf(registro.substring(49, 51)).trim();

		if("".equals(decimales)) {
			arrDatosModifInterb[3] = enteros + "." + "00";		//Ingreso Mensual
//			System.out.println("CatNominaAction - separaCadenaModifInterb() $ sueldo sin decimanles: " + arrDatosModifInterb[3]);
		}
		else {
			arrDatosModifInterb[3] = enteros + "." + decimales;		// Ingreso Mensual
//			System.out.println("CatNominaAction - separaCadenaModifInterb() $ sueldo con decimanles: " + arrDatosModifInterb[3]);
		}
		// Fin Modificacion para Sueldos //

		arrDatosModifInterb[4] = (registro.substring(51, 52)).trim();
		arrDatosModifInterb[5] = (registro.substring(52, 112)).trim();
		arrDatosModifInterb[6] = (registro.substring(112, 142)).trim();
		arrDatosModifInterb[7] = (registro.substring(142, 177)).trim();
		arrDatosModifInterb[8] = (registro.substring(177, 181)).trim();
		arrDatosModifInterb[9] = (registro.substring(181, 216)).trim();
		arrDatosModifInterb[10] = (registro.substring(216, 221)).trim();
		arrDatosModifInterb[11] =	(registro.substring(221, 225)).trim();
		arrDatosModifInterb[12] = (registro.substring(225, 237)).trim();
		arrDatosModifInterb[13] = String.valueOf((registro.substring(237, 249)).trim());
		arrDatosModifInterb[14] = (registro.substring(249, 309)).trim();
		arrDatosModifInterb[15] =	(registro.substring(309, 339)).trim();
		arrDatosModifInterb[16] = (registro.substring(339, 374)).trim();
		arrDatosModifInterb[17] = (registro.substring(374, 378)).trim();
		arrDatosModifInterb[18] = (registro.substring(378, 413)).trim();
		arrDatosModifInterb[19] =	(registro.substring(413, 418)).trim();
		arrDatosModifInterb[20] = (registro.substring(418, 422)).trim();
		arrDatosModifInterb[21] = String.valueOf((registro.substring(422, 429)).trim());
		arrDatosModifInterb[22] = String.valueOf((registro.substring(429, 441)).trim());
		arrDatosModifInterb[23] = String.valueOf((registro.substring(441, 453)).trim());
		arrDatosModifInterb[24] = String.valueOf((registro.substring(453, 465)).trim());

		//tablaDetalle += html.generaTablaModifInterb(arrDatosModifInterb);
		//tablaDetalle += html.generaTablaModifInterb(arrDatosActual);
		tablaDetalle += html.generaTablaModifInterb(arrDatosModifInterb);
//		System.out.println("CatNominaAction - separaCadenaModif() $ Fin");
		return tablaDetalle;
	}



	/***************************************************************
	 ***             modificaDatosArchInterb()
	 ***  M?todo encargado de leer cada registro del archivo
	 ***  y enviarlo a otros m?todos de modificaci?n
	 **************************************************************/
	public boolean modificaDatosArchInterb(String numContrato, String cveUsr, ArrayList listaModificaciones)
	{
//		System.out.println("CatNominaAction - modificaDatosArchInterb - Inicio");
		CatNominaDAO dao = new CatNominaDAO();
		String[] arrModifArchInterb = new String[25]; 	// Solo trae 5 campos
		String renglon = "";
		String enteros = "";
		String decimales = "";
		boolean retorno = false;

		try
		{
			Iterator it = listaModificaciones.iterator();
			while (it.hasNext()) {
				renglon = (String)it.next();
//				System.out.println("CatNominaAction - modificaDatosArchInterb $ Renglon" + renglon);

				arrModifArchInterb[0] = (renglon.substring(0, 18)).trim();
				arrModifArchInterb[1] = (renglon.substring(18, 25)).trim();
				arrModifArchInterb[2] = (renglon.substring(25, 31)).trim();

				// Modificacion para Sueldos//
				enteros =  String.valueOf(renglon.substring(31, 49)).trim();
				decimales =  String.valueOf(renglon.substring(49, 51)).trim();

				if("".equals(decimales)) {
					arrModifArchInterb[3] = enteros + "." + "00";
//					System.out.println("CatNominaAction - modificaDatosArchInterb() $ sueldo sin decimanles: " + arrModifArchInterb[3]);
				}
				else {
					arrModifArchInterb[3] = enteros + "." + decimales;
//					System.out.println("CatNominaAction - modificaDatosArchInterb() $ sueldo con decimanles: " + arrModifArchInterb[3]);
				}
				// Fin Modificacion para Sueldos //

				arrModifArchInterb[4] = (renglon.substring(51, 52)).trim();
				arrModifArchInterb[5] = (renglon.substring(52, 112)).trim();
				arrModifArchInterb[6] = (renglon.substring(112, 142)).trim();
				arrModifArchInterb[7] = (renglon.substring(142, 177)).trim();
				arrModifArchInterb[8] = (renglon.substring(177, 181)).trim();
				arrModifArchInterb[9] = (renglon.substring(181, 216)).trim();
				arrModifArchInterb[10] = (renglon.substring(216, 221)).trim();
				arrModifArchInterb[11] = (renglon.substring(221, 225)).trim();
				arrModifArchInterb[12] = (renglon.substring(225, 237)).trim();
				arrModifArchInterb[13] = (renglon.substring(237, 249)).trim();
				arrModifArchInterb[14] = (renglon.substring(249, 309)).trim();
				arrModifArchInterb[15] = (renglon.substring(309, 339)).trim();
				arrModifArchInterb[16] = (renglon.substring(339, 374)).trim();
				arrModifArchInterb[17] = (renglon.substring(374, 378)).trim();
				arrModifArchInterb[18] = (renglon.substring(378, 413)).trim();
				arrModifArchInterb[19] = (renglon.substring(413, 418)).trim();
				arrModifArchInterb[20] = (renglon.substring(418, 422)).trim();
				arrModifArchInterb[21] = (renglon.substring(422, 429)).trim();
				arrModifArchInterb[22] = (renglon.substring(429, 441)).trim();
				arrModifArchInterb[23] = (renglon.substring(441, 453)).trim();
				arrModifArchInterb[24] = (renglon.substring(453, 465)).trim();

				retorno = dao.actualizaDatosArchInterb(arrModifArchInterb, numContrato, cveUsr);
			}
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			retorno = false;
		}
		return retorno;
	}



	/* EVERIS 22/02/2008 - INICIA metodo envio mail */
	/**
	 * Metodo encargado de enviar la notificacion de alta de cuenta interbancaria
	 * al mail del usuario y del contrato
	 * @param emailUser String
	 * @param emailContract String
	 * @return siempre devuelte true
	 * @deprecated Este m&eacute;todo ya no realiza ninguna de sus funciones,
	 * &uacute;nicamente devuelve un valor en <i>true</i>
	 * @throws Exception exception
	 */
	@Deprecated
	public boolean notificaAltaCtaInterb(ArrayList detalleCuentasInterb, String numContrato, String descContrato, String numUsuario)
	{
//		System.out.println("CatNominaAction - notificAltaCtaInterb $ Inicio");
		EIGlobal.mensajePorTrace("CatNominaAction - notificAltaCtaInterb $ Inicio", EIGlobal.NivelLog.INFO);
		boolean envioNotif = false;
		EmailSender emailSender=new EmailSender();
		// emailSender.confirmaEmailInterb(new Vector(detalleCuentasInterb), numContrato, descContrato, numUsuario);
		envioNotif = true;
//		System.out.println("CatNominaAction - notificAltaCtaInterb $ envioNotif = " + envioNotif);
//		System.out.println("CatNominaAction - notificAltaCtaInterb $ Fin");
		EIGlobal.mensajePorTrace("CatNominaAction - notificAltaCtaInterb $ Fin", EIGlobal.NivelLog.INFO);
		return envioNotif;
	}
	/* EVERIS 22/02/2008 - FIN Metodo envia mail*/
}