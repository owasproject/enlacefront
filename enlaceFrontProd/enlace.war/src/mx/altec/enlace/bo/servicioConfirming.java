/********************************************************************************
Archivo:	servicioConfirming.java
Creado:		19-Agosto-2002 04:09 PM
Autor:		Hugo Sánchez Ricardez
Modificado: 29/11/2002
Version:	5.0
********************************************************************************/
package mx.altec.enlace.bo;

import java.util.*;
import java.io.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class servicioConfirming implements Serializable{

	protected ServicioTux Servicio;		// Servicio de Tuxedo

	protected String Contrato;			// Numero de contrato
    protected String Usuario;			// Clave de Usuario
    protected String Perfil;			// Perfil del usuario

    // X-- Constructor ( Crea un nuevo servicioConfirming ) ----------------------------
    public servicioConfirming() {
        Servicio = new ServicioTux();
    }

	//---------		Metodos set() y get()		---------------
	// X-- Obtiene el Servicio de Tuxedo ------------------------------------------------
    public ServicioTux getServicioTux() {return Servicio;}

    // X-- Establece el numero de contrato ----------------------------------------------
    public void setContrato (String _contrato) {Contrato = _contrato;}

    // X-- Establece el usuario ---------------------------------------------------------
    public void setUsuario (String _usuario) {Usuario = _usuario;}

	// X-- Establece el perfil del usuario ----------------------------------------------
	public void setPerfil (String perfil) {Perfil = perfil;}

	// -------		Metodos Generales	----------------------

    // X-- Envia un Archivo a Tuxedo --------------------------------------------------
    protected boolean envia(String NombreArchivo) {
        try {
        	EIGlobal.mensajePorTrace("servicioConfirming:envia - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II
            
			int Respuesta = 1;           
            Runtime rt = Runtime.getRuntime();

            boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();
           
            
            if ( !envArch.copiaLocalARemoto( NombreArchivo, Global.DIRECTORIO_REMOTO) )
			{
				EIGlobal.mensajePorTrace( "***pdServicio.class & envia: No se realizo el envio del archivo. ", EIGlobal.NivelLog.DEBUG);
				Respuestal = false;
				
			} else {
				EIGlobal.mensajePorTrace( "***SpdServicio.class & envia: Se realizo el envio del archivo.", EIGlobal.NivelLog.DEBUG);
			}
         
            if (Respuestal == true){
                return true;
			}

        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.INFO);
            ex.printStackTrace(System.err);
        }
        return false;
    }

    // X-- Recibe un Archivo de Tuxedo --------------------------------------------------
    protected File recibe(String Nombre) {
        try {
        	EIGlobal.mensajePorTrace("servicioConfirming:recibe - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II PRUEBA DE VALIDACION
            File Archivo = new File(Nombre);
           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nombre, Global.DIRECTORIO_LOCAL)){
				
					EIGlobal.mensajePorTrace("*** servicioConfirming.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** servicioConfirming.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
           
        	if (Respuestal) {

              //Archivo.setReadOnly();
              return Archivo;
           	}
         
        } catch (Exception ex) {
			EIGlobal.mensajePorTrace("Error al obtener el archivo de respuesta.", EIGlobal.NivelLog.INFO);

        }

        return null;
    }

	// X-- Envia una Trama a Tuxedo
	public String EnviaTrama(String Trama) {
	    try {
			Hashtable ht	= Servicio.web_red(Trama);
			EIGlobal.mensajePorTrace("ALERTA - ALERTA ALERTA El buffer retornado :"+(String) ht.get("BUFFER"), EIGlobal.NivelLog.INFO);
//----------------------------
			try
				{
				FileWriter x = new FileWriter(Global.DOWNLOAD_PATH + "adrv.txt",true);
				x.write("trama: " + Trama + "\n");
				x.write("retorno: " + ((String) ht.get("BUFFER")) + "\n");
				x.close();
				}
			catch(Exception e)
				{
				EIGlobal.mensajePorTrace("Error en pruebas: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
				}
//----------------------------
			return ((String) ht.get("BUFFER"));
	    }
	    catch (Exception e){ return "Error"; }
	}


	// X-- Obtiene una cadena de respuesta de acuerdo al servicio que se solicito
	public String coMtoProv_Servicio(int TipoTrama, String TipoServicio, String TipoEnvio, String comando) {
		String Buffer		= "";
		String Trama =	TipoEnvio		+ "|" +		Usuario		+ "|" +
						TipoServicio	+ "|" +		Contrato	+ "|" +
						Usuario			+ "|" +		Perfil		+ "|";

		if(TipoTrama == 2) {		// Service: CFDP Consulta de datos del Proveedor
			Trama = Trama + comando;
			EIGlobal.mensajePorTrace("ALERTA!!!ALERTA!!!ALERTA!!!ALERTA!!!ALERTA!!!ALERTA!!!ALERTA!!!ALERTA!!! :"+Trama, EIGlobal.NivelLog.INFO);
			Buffer = EnviaTrama(Trama);
			EIGlobal.mensajePorTrace("Buffer de la consulta de datos :"+Buffer, EIGlobal.NivelLog.INFO);
			if(!Buffer.substring(0,2).equals("OK"))
				{Buffer = null;}
		}
		else if(TipoTrama == 3) {	// Service: CFPB Baja de proveedores
			Trama = Trama + comando;
			Buffer = EnviaTrama(Trama);
			if(!Buffer.substring(0,2).equals("OK"))
				{Buffer = null;}
		}
		else if(TipoTrama == 4) {	// Service: CFMR Modificacion de un Proveedor
			Trama = Trama + comando;
			Buffer = EnviaTrama(Trama);
			EIGlobal.mensajePorTrace("Buffer de la modificacion de un proveedor:"+Buffer, EIGlobal.NivelLog.INFO);
			if(!Buffer.substring(0,2).equals("OK"))
				{Buffer = null;}
		}

		return Buffer;
	}

	// X-- Obtiene un Archivo de Respuesta de Tuxedo de acuerdo al servicio que se solicito
	public String coMtoProv_envia_tux(int TipoTrama, String TipoServicio, String TipoEnvio, String comando, String trama) {
		int longitud = 0;
		String Buffer = "";
		/*
	     * jgarcia
	     * Stefanini
	     * Se agrega a la trama de envio la cuenta de cargo, se concatena al contrato.
	     */
		String Trama  =	TipoEnvio		+ "|" +		Usuario		+ "|" +
						TipoServicio	+ "|" +
						Contrato	+ (TipoTrama == 1 && "CFAC".equals(TipoServicio) && "2EWEB".equals(TipoEnvio)?"#" + trama + "#":"") + "|" +
						Usuario			+ "|" +		Perfil		+ "|";

		if(TipoTrama == 1) {	// Service: CFAC Actualizacion Catálogos Confirming

		    EIGlobal.mensajePorTrace("ACTUALIZACION DE CATALOGOS", EIGlobal.NivelLog.INFO);
			Buffer = EnviaTrama(Trama);
			
            
			if (Buffer.equals(Global.DIRECTORIO_REMOTO + "/" + Usuario)) {

				try {

					File ArchivoResp = recibe(Buffer);
					
					if (ArchivoResp.equals(null)) {

						EIGlobal.mensajePorTrace("Error al recibir el archivo de respuesta.", EIGlobal.NivelLog.INFO);return null;}

					return (ArchivoResp.getPath());
				} catch (Exception ex) {

					EIGlobal.mensajePorTrace("Error al intentar leer el archivo de respuesta.", EIGlobal.NivelLog.INFO);	return null; }
			}
		}
		else if(TipoTrama == 5)	{	//Service:	CFER Recuperación de Archivo de Proveedores

			Trama = Trama + comando;
			Buffer = EnviaTrama(Trama);
			longitud = Buffer.length();
			Buffer = Buffer.substring(0,longitud-1);
			if(Buffer.equals(Global.DIRECTORIO_REMOTO + "/" + Usuario)) {
				try {
					File ArchivoResp = recibe(Buffer);
					if (ArchivoResp.equals(null)) {EIGlobal.mensajePorTrace("Error al recibir el archivo de respuesta.", EIGlobal.NivelLog.INFO);return null;}
					return (ArchivoResp.getPath());
				} catch (Exception ex) {EIGlobal.mensajePorTrace("Error al intentar leer el archivo de respuesta.", EIGlobal.NivelLog.INFO);	return null; }
			}
		}
		else if(TipoTrama == 6)	{	//Service:	CFRR Envio de Archivo de proveedores

		    EIGlobal.mensajePorTrace("ENVIO DE ARCHIVO DE PROVEEDORES", EIGlobal.NivelLog.INFO);
			String path = Global.DIRECTORIO_LOCAL + "/" + Usuario + ".coMtoProv";
			if(CreaArchivoProveedor(path,trama)) {	// se crea el Archivo de envio
				if(envia(Usuario + ".coMtoProv")) {	// se hace la copia Remota
					Trama = Trama + path + comando;
					Buffer = EnviaTrama(Trama);
					EIGlobal.mensajePorTrace("BUFFER RECIBIDO DE LA TRAMA DE ENVIO :"+Buffer, EIGlobal.NivelLog.INFO);
					longitud = Buffer.length();
					String Ruta_T = Global.DIRECTORIO_REMOTO + "/" + Usuario;
					if (longitud!=Ruta_T.length())
					{
						Buffer = Buffer.substring(0,longitud-1);
					}
					if(Buffer.equals(Global.DIRECTORIO_REMOTO + "/" + Usuario)) {
						try {
							Proveedor prov = new Proveedor();
							File ArchivoResp = recibe(Buffer);
							if (ArchivoResp.equals(null)) {EIGlobal.mensajePorTrace("Error al recibir el archivo de respuesta.", EIGlobal.NivelLog.INFO);return null;}
							return(prov.LeerDatos_RegresoDeEnvio(ArchivoResp.getPath()));
							} catch (Exception ex) {EIGlobal.mensajePorTrace("Error al intentar leer el archivo de respuesta.", EIGlobal.NivelLog.INFO);	return null; }
					}
				}
			}
		}

		return null;
	}

	// X-- Crea el Archivo que sera enviado a Tuxedo para dar de Alta
	public boolean CreaArchivoProveedor(String path, String trama) {

		RandomAccessFile fileAmbiente = null;
		String NombreArchivo	= "";

		boolean valorRegreso = false;
		Proveedor pr = new Proveedor();

		try {
			File archivo = new File(path);
			if (archivo.exists()){archivo.delete();}
			fileAmbiente = new RandomAccessFile(archivo,"rw");
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("Error al leer el archivo de ambiente.", EIGlobal.NivelLog.INFO);
		}
		try	{
			StringTokenizer prov = new StringTokenizer(trama,"^");
			NombreArchivo = prov.nextToken();
			while(prov.hasMoreTokens())
				{ fileAmbiente.writeBytes( pr.creaTramaEnvio(prov.nextToken(),"|") + "\r\n"); }
			valorRegreso = true;
		} catch(IOException e ) {
			EIGlobal.mensajePorTrace("Error al leer el archivo de ambiente.", EIGlobal.NivelLog.INFO);
		}
		return valorRegreso;
	}
}