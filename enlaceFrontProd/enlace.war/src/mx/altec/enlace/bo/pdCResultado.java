/*
 * pdCResultado.java
 *
 * Created on 10 de abril de 2002, 11:37 AM
 */

package mx.altec.enlace.bo;

import java.util.*;
/**
 *
 * @author  Rafael Martinez Montiel
 * @version 1.0
 */
public class pdCResultado implements java.io.Serializable {

    /** Holds value of property noSec. */
    private Long noSec;

    /** Creates new pdCResultado */
    public pdCResultado() {
    }


    /** Getter for property noSec.
     * @return Value of property noSec.
     */
    public Long getNoSec() {
        return noSec;
    }

    /** Setter for property noSec.
     * @param noSec New value of property noSec.
     */
    public void setNoSec(Long noSec) {
        this.noSec = noSec;
    }

    /** lee desde un archivo los resultados de la consulta previamente hecha, construlle los objetos necesarios y devuelve la coleccion
     * @param file es el archivo que contiene los resultados de la consulta
     * @return ArrayList que contiene los resultados de la consulta, cada registro de la consulta esta en un objeto pdCPago
     * @throws IOException Si ocurre algun error de IO
     * @throws Exception en cualquier otro caso de error
     */
    public ArrayList read(java.io.BufferedReader file)
                        throws java.io.IOException, Exception{
        String trama ="";
        ArrayList al = new ArrayList();
        while(null!= (trama = file.readLine())){
            String subToken ="";
            if(trama.equals(""))continue;

            mx.altec.enlace.bo.pdCPago pago = new pdCPago();
            pdTokenizer tokenizer = new pdTokenizer(trama,"@", 2);

            pago.setNoSec (tokenizer.nextToken());

            pago.setNoCta(tokenizer.nextToken());
            pago.setNoOrden(tokenizer.nextToken());
            pago.setCveBenef(tokenizer.nextToken());
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("") ){
                pago.setFechaRecepcion(getCalendar(subToken));
            }
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaAplic(getCalendar(subToken));
            }
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaPago(getCalendar(subToken));
            }
            pago.setInstrucciones(tokenizer.nextToken());
            pago.setImporte(Double.parseDouble(tokenizer.nextToken()));
            subToken = tokenizer.nextToken();
            pago.setFmaPago(subToken.length()>0 ? subToken.charAt(0):'\b');
            subToken = tokenizer.nextToken();
            pago.setStatus(subToken.length()>0 ? subToken.charAt(0):'\b');
            pago.setStatusDesc(tokenizer.nextToken());
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaLimPago(getCalendar(subToken));
            }
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaCancelacion(getCalendar(subToken));
            }
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaVencimiento(getCalendar(subToken));
            }
            subToken = tokenizer.nextToken();
            if( ! subToken.equals("")){
                pago.setFechaModificacion(getCalendar(subToken));
            }
            pago.setCvePagoEnTodaSucursal(tokenizer.nextToken());
            pago.setCveRegion(tokenizer.nextToken());
            pago.setCvePlaza(tokenizer.nextToken());
            pago.setCveSucursal(tokenizer.nextToken());
            pago.setRefOperacion(tokenizer.nextToken());
            pago.setNombreBenefNoRegistrado(tokenizer.nextToken());

            al.add(pago);
        }
        return al;
    }

    /** convierte una fecha del formato dd/mm/aaaa a un GregorianCalendar
     * @param date es la fecha a convertir
     * @return GregorianCalendar con la fecha
     * @throws Exception si ocurre cualquier error
     */
    protected GregorianCalendar getCalendar(String date)
                    throws Exception{
        pdTokenizer tokenizer =  new pdTokenizer(date,"/", 2);

        String dia = tokenizer.nextToken();
        String mes = tokenizer.nextToken();
        String anio = tokenizer.nextToken();

        GregorianCalendar cal= new GregorianCalendar();
        cal.set(cal.DAY_OF_MONTH,Integer.parseInt(dia));
        cal.set(cal.MONTH,Integer.parseInt(mes)-1);
        cal.set(cal.YEAR,Integer.parseInt(anio));

        return cal;
    }

}