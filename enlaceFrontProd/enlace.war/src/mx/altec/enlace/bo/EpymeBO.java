package mx.altec.enlace.bo;

import mx.altec.enlace.dao.EpymeDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class EpymeBO {
	/**
	 * instancia - se realiza el llamado al EpymeDAO
	 */
	private static EpymeDAO iNSTANCIA=new EpymeDAO();
	/**
	 * Metodo generado para realizar la consulta de la existencia del contrato en la tabla EWEB_MX_MAE_PYME y del contrato-usuario en la tabla EWEB_PYME_CONTR
	 * @param Contrato contrato a consultar
	 * @param Usuario usuario a consultar
	 * @return esMenuReducido boolean
	 */
	public boolean esMenuReducido(String Contrato, String Usuario) {
		String estatus = "";
		boolean esMenuReducido=false;
		estatus=iNSTANCIA.esContratoEpymeNew(Contrato, Usuario);
		EIGlobal.mensajePorTrace("EpymeBO::esMenuReducido::Estatus EWEB_PYME_CONTR del contrato " + Contrato + " y el usuario "+Usuario+"::" + estatus + "::", EIGlobal.NivelLog.INFO);
		if("A".equals(estatus)) {
			esMenuReducido=true;
		}else if("B".equals(estatus)) {
			esMenuReducido=false;
		}else {
			estatus=iNSTANCIA.esContratoEpymeOld(Contrato);
			EIGlobal.mensajePorTrace("EpymeBO::esMenuReducido::Estatus EWEB_MX_MAE_PYME del contrato " + Contrato + "::" + estatus + "::", EIGlobal.NivelLog.INFO);
			if("A".equals(estatus)) {
				esMenuReducido=true;
			}else if("B".equals(estatus)) {
				esMenuReducido=false;
			}else {
				esMenuReducido=false;
			}
		}
		return esMenuReducido;
	}
	/**
	 * Metodo generado para realizar la actualizacion cuando el registro ya sea existente en la tabla EWEB_PYME_CONTR o insertarlo cuando este no exista
	 * @param contrato contrato a actualizar/insertar
	 * @param usuario usuario a actualizar/insertar
	 * @param tipoMenu estatus a evaluar
	 * @return estatus final despues de las validaciones
	 */
	public String actualizaMenu(String contrato, String usuario, String tipoMenu){
		EIGlobal.mensajePorTrace("EpymeBO::actualizaMenu::Entrando::", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("EpymeBO::actualizaMenu::tipoMenu::"+tipoMenu+"::", EIGlobal.NivelLog.INFO);
		String estatus="";
		String estatusOld="";
		String estatusFinal="";
		estatus=iNSTANCIA.esContratoEpymeNew(contrato, usuario);
		if("A".equals(estatus)) {
			iNSTANCIA.actualizaMenuMostrar(contrato, usuario, "B");
			estatusFinal="B";
		}else if("B".equals(estatus)){
			iNSTANCIA.actualizaMenuMostrar(contrato, usuario, "A");
			estatusFinal="A";
		}else{
			estatusOld=iNSTANCIA.esContratoEpymeOld(contrato);
			EIGlobal.mensajePorTrace("EpymeBO::actualizaMenu::estatusOld::"+estatusOld+"::", EIGlobal.NivelLog.INFO);
			if("A".equals(estatusOld)){
				iNSTANCIA.insertaMenuMostrar(contrato, usuario, "B");
				estatusFinal="B";
			}else if("B".equals(estatusOld)){
				iNSTANCIA.insertaMenuMostrar(contrato, usuario, "A");
				estatusFinal="A";
			}else{
				iNSTANCIA.insertaMenuMostrar(contrato, usuario, "A");
				estatusFinal="A";
			}
		}
		EIGlobal.mensajePorTrace("EpymeBO::actualizaMenu::estatusFinal::"+estatusFinal+"::", EIGlobal.NivelLog.INFO);
		return estatusFinal;
	}
	
}
