package mx.altec.enlace.bo;

import java.util.ArrayList;

import mx.altec.enlace.dao.FacPerfilDao;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;
import mx.altec.enlace.gwt.adminusr.shared.PerfilPrototipo;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;

public class GuardaGruposPerf {

	public ArrayList<String> procesaEdoInicial(ArrayList<GrupoPerfiles> total, ArrayList<String> edoFinal) {
		String dato, dato2;
		ArrayList<String> respuesta = new ArrayList<String>();
		ArrayList<String> cvesFinales = new ArrayList<String>();
		convierte(edoFinal, cvesFinales);
		for(GrupoPerfiles grupo: total) {
			for(PerfilPrototipo perf: grupo.getPerfiles()) {
				for(Facultad fac: perf.getFacultades()) {
					dato=grupo.getCveGrupo() + "&" + grupo.getDescripcion() + "@" + perf.getCvePerfil() + "&" + perf.getDescripcion() + "@" + fac.getCveFacultad() + "&" + fac.getDescripcion();
					dato2=grupo.getCveGrupo() + "&" + perf.getCvePerfil() + "&" + fac.getCveFacultad();
					if(!cvesFinales.contains(dato2)) {
						respuesta.add(dato);
					}
				}
			}
		}
		return respuesta;
	}

	private void convierte(ArrayList<String> edoFinal, ArrayList<String> cvesFinales) {
		String separado[] = null;
		String dato = null;
		for(String input: edoFinal) {
			separado = input.split("@");
			dato = separado[0].split("&")[0] + "&" + separado[1].split("&")[0] + "&" + separado[2].split("&")[0];
			cvesFinales.add(dato);
		}
	}

	// Genera la clave de perfil a utilizar tomando la primera disponible
	// con el formato cve_usuario + "nn", donde "nn" es un consecutivo que
	// puede ir de "00" a "99"
	private String generaPerfil(String usuario, ArrayList<String> perfiles) {
		String perfil = usuario;
		if (perfiles.size() == 0) {
			return perfil + "00";
		} else {
			for (int idx = 0; idx < 100; idx++) {
				String idxStr = "" + idx;
				if (idx < 10){
					idxStr = "0" + idxStr;
				}
				perfil = usuario + idxStr;
				if (perfiles.indexOf(perfil) == -1 ){ //Si no lo encuentra
					break;
				}
			}
		}
		return perfil;
	}

	public int guardaPerfilacion(ArrayList<String> edoInicial, ArrayList<String> edoFinal,
			ArrayList<String> extIniciales, ArrayList<String> extFinales, String contrato,
			String cvePerfil, String usuario, String nombre, String operacion) {
		FacPerfilDao dao = new FacPerfilDao();
		ArrayList<String> perfilesUsados = new ArrayList<String>();
		ArrayList<GrupoPerfiles> lista = InicializaGruposPerfiles.obtenUniverso();
		int respuesta = 0;
		EIGlobal.mensajePorTrace("Inicial:", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(edoInicial.toString(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Final:", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(edoFinal.toString(), EIGlobal.NivelLog.DEBUG);
		if(!operacion.equals("cambio")) {
			if(cvePerfil == null || cvePerfil.equals("")) {
				ArrayList<String> perfilesExistentes = dao.getPerfilesSimilaresExistentes(usuario);
				cvePerfil = generaPerfil(usuario, perfilesExistentes);
				EIGlobal.mensajePorTrace("Guarda Grupos Perf - guardaPerfilacion() " +
						"Perfil Generado: [" + cvePerfil + "}", EIGlobal.NivelLog.DEBUG);
				//cvePerfil = usuario + "SU";
			}
			respuesta = dao.altaPerfil(cvePerfil, nombre);
			if(respuesta != 1) {
				dao.rollback();
				dao.cierraConexion();
				return 1;	//error en alta de perfil.
			}
			respuesta = dao.altaPerfil(cvePerfil, BaseServlet.convierteUsr8a7(usuario), contrato);
			if(respuesta != 1) {
				dao.rollback();
				dao.cierraConexion();
				return 6;	//error en alta de perfil en segu_usrperftele.
			}
		}

		for(GrupoPerfiles grp: lista) {
			for(PerfilPrototipo perf: grp.getPerfiles()) {
				for(Facultad fac: perf.getFacultades()) {
					respuesta = dao.bajaFacultad(cvePerfil, fac.getCveFacultad());
					if(respuesta < 0) {
						dao.rollback();
						dao.cierraConexion();
						return 2;	//error en baja de facultaad
					}
				}
				respuesta = dao.bajaPerfProt(cvePerfil, perf.getCvePerfil());
				if(respuesta < 0) {
					dao.rollback();
					dao.cierraConexion();
					return 3;	//error en baja de perfilProt
				}

			}
		}
		ArrayList<String> perfilesAsignados = new ArrayList<String>();
		ArrayList<String> facultadesAsignadas = new ArrayList<String>();
		for(String itemFinal: edoFinal) {
			String test[] = itemFinal.split("@");
			String cveGrp1 = test[0].split("&")[0];
			String cvePerf1 = test[1].split("&")[0];
			if(!perfilesUsados.contains(cvePerf1) && !perfilesAsignados.contains(cvePerf1)) {
				respuesta = dao.altaPerfProt(cvePerfil, cvePerf1);
				if(respuesta != 1) {
					dao.rollback();
					dao.cierraConexion();
					return 4;	//error en alta de perfilProt
				}
				else {
					perfilesAsignados.add(cvePerf1);
				}

			}
			for(String itemInicial: edoInicial) {
				String test2[] = itemInicial.split("@");
				String cveGrp2 = test2[0].split("&")[0];
				String cvePerf2 = test2[1].split("&")[0];
				String cveFac2 = test2[2].split("&")[0];

				/*EIGlobal.mensajePorTrace("Se comparan los strings:", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Inicial: " + itemInicial, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Final:   " + itemFinal, EIGlobal.NivelLog.DEBUG);*/
				if(cveGrp2.equals(cveGrp1) && cvePerf2.equals(cvePerf1) && !facultadesAsignadas.contains(cveFac2)) {
					dao.altaFacExtraord(cvePerfil, cveFac2, "-");
					facultadesAsignadas.add(cveFac2);
					if(respuesta != 1) {
						dao.rollback();
						dao.cierraConexion();
						return 5;	//error en alta de facextraord por perfil.
					}

				}
			}
		}
		for(String item: extFinales) {
			String facextra[] = item.split("&");
			if(dao.altaFacExtraord(cvePerfil, facextra[0], "+") != 1) {
				dao.rollback();
				dao.cierraConexion();
				return 7;	//error en alta de facextraord individual.
			}
		}
		dao.commit();
		dao.cierraConexion();
		return 0;
	}


	public int borraPerfilacion(ArrayList<Facultad> listaFacultadesEx, String contrato, String cvePerfil, String usuario, String operacion) {
		FacPerfilDao dao = new FacPerfilDao();
		ArrayList<GrupoPerfiles> lista = InicializaGruposPerfiles.obtenUniverso();
		if(cvePerfil == null) {
			return -1;	//se debe validar previamente para la alta que existe y se haya creado
		}
		for(GrupoPerfiles grp: lista) {
			for(PerfilPrototipo perf: grp.getPerfiles()) {
				for(Facultad fac: perf.getFacultades()) {
					if(dao.bajaFacultad(cvePerfil, fac.getCveFacultad()) < 0) {
						dao.rollback();
						dao.cierraConexion();
						return 1;	//error en baja facultad
					}
				}
				if(dao.bajaPerfProt(cvePerfil, perf.getCvePerfil()) < 0) {
					dao.rollback();
					dao.cierraConexion();
					return 2;	//error en baja perfil prot
				}
			}
		}
		for(Facultad fac: listaFacultadesEx) {
			if(dao.bajaFacultad(cvePerfil, fac.getCveFacultad()) < 0) {
				dao.rollback();
				dao.cierraConexion();
				return 5;	//error en baja fac extraord individual
			}
		}
		if(operacion.equals("baja")) {
			if(dao.bajaUsuario(cvePerfil, BaseServlet.convierteUsr8a7(usuario), contrato) < 0) {
				dao.rollback();
				dao.cierraConexion();
				return 3;	//error en baja usuario
			}
			if(dao.bajaPerfil(cvePerfil) < 0) {
				dao.rollback();
				dao.cierraConexion();
				return 4;	//error en baja perfil
			}
		}
		dao.commit();
		dao.cierraConexion();
		return 0;
	}

}