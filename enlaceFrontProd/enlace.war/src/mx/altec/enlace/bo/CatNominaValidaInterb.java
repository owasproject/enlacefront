package mx.altec.enlace.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.dao.CatNominaDAO;


public class CatNominaValidaInterb {

	public int validaCuentaInterb(String numContrato, String numCuenta)
	{
		System.out.println("");
		System.out.println("CatNominaValidaInterb - validaCuentaInterb $ Inicio");
		System.out.println("CatNominaValidaInterb - validaCuentaInterb $ NumContrato: " + numContrato);
		System.out.println("CatNominaValidaInterb - validaCuentaInterb $ numCuenta: " + numCuenta);
		int usaCuenta = 0;

		CatNominaDAO dao = new CatNominaDAO();
		int totalRegistros = dao.buscaCuenta(numContrato, numCuenta);

		return totalRegistros;
	}



	//******ALTA ARCHIVO INTERB****************


	/**************************************************************************************/
	/** Metodo: validaLineaAlta
	 * @param String, int
	 * @description: Hace la validaci�n del tamano de la linea del archivo
	/*************************************************************************************/
	public CatNominaBean validaArchivoAlta(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
		System.out.println("");
		System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Inicio ");
		CatNominaBean bean = new CatNominaBean();

		String linea = "";
		ArrayList listaErrorCuentas = new ArrayList();
		ArrayList listaErrorLong = new ArrayList();
		ArrayList listaErrorDatos = new ArrayList();
		ArrayList listaErrores = new ArrayList();
		ArrayList listaRegistros = new ArrayList();
		ArrayList listaErrorCampo = new ArrayList();
		ArrayList listaErrorCuenta = new ArrayList();
		ArrayList listaErrorSantander = new ArrayList(); //Cambio para no permitir interbancaria Santander



		RandomAccessFile archivoAlta;

		int i = 0;

		try {
			archivoAlta = new RandomAccessFile(archivoCargado, "rw");
			String registroLeido = "";
			String numCta = "";
			String numEmpl = "";
			String apellidoP = "";
			String nombre = "";
			String rfc = "";
			String ingresoMensual = "";
			String deptoEmpl = "";
			String apellidoM = "", sexo = "", calle = "", colonia = "", delegacion = "", cveEstado = "", ciudad = "", codigoPostal = "", clavePais = "", prefTelPart = "", telPart = "";
			String calleOfi = "", coloniaOfi = "", delegOfi = "", cveEstadoOfi = "", ciudadOfi = "", codPostalOfi = "", paisOfi = "", cveDireccionOfi = "", prefTelOfi = "", telOfi = "", extOfi = "";
			String cuentaInterb = ""; //Cambio para no permitir interbancaria Santander

			long posicion = 0;
			int existeRelacion = 0;

			listaErrorLong.add(new String("Long"));
			listaErrorCuentas.add(new String("Cuentas"));
			listaErrorDatos.add(new String("Datos"));
			listaErrorCampo.add(new String("Campo"));
			listaErrorCuenta.add(new String ("Cuenta Abono"));
			listaErrorSantander.add(new String ("CodBancario"));//Cambio para no permitir interbancaria Santander



			if (archivoAlta.length() > 0) {
				archivoAlta.seek(0);
				do {
					i++;
					registroLeido = archivoAlta.readLine();
					int x = registroLeido.length();

					System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Longitud Correcta ," +  x);
					System.out.println("\nCatNominaValidaInterb - validaArchivoAlta $ Longitud Correcta ," +  registroLeido);


					if (registroLeido.length() == 568) {

						//Cambio para no permitir interbancaria Santander
						cuentaInterb = registroLeido.substring(0, 18).trim();

						CatNominaValidaImport valida = new CatNominaValidaImport();

						if(valida.validaCampoNumero(cuentaInterb))
						{

								int a = cuentaInterb.length();
								System.out.println("CatNominaAction - validaArchivoAlta $ Longitud de Num Cuenta  = " + a);

								if (a == 18){
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Valido cuenta abono ");
								}
								else{
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Cuenta abono incorrecta ");
									listaErrorCuenta.add(new Integer(i));

								}

								if (!cuentaInterb.equals("")){

									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Num Cuenta a validar = " + cuentaInterb);
									CatNominaDAO dao = new CatNominaDAO();
									//existeRelacion = dao.existeRelacion(numContrato, numCta);
									existeRelacion = dao.existeRegistro(numContrato, cuentaInterb);

									if(existeRelacion != -100)
									{
											if(existeRelacion==0){
											System.out.println("CatNominaValidaImport - validaLineaAlta $ Registro no existe en tablas - Linea VALIDA " + registroLeido );
											listaRegistros.add(new String(registroLeido));
										}
										else{
											System.out.println("CatNominaValidaInterb - validaArchivoAlta $Ya existe registro");
											listaErrorCuentas.add(new Integer(i));
										}

											String codigoBancario = cuentaInterb.substring(0,3);
											System.out.println("CatNominaAction - validaArchivoAlta $ Codigo Bancario = " + codigoBancario);

											if(!codigoBancario.equals("014")) // Codigo Bancario Santander
											{

												numEmpl = registroLeido.substring(18, 25).trim();
												deptoEmpl = registroLeido.substring(25, 31).trim();
												ingresoMensual = registroLeido.substring(31, 51).trim();
												apellidoP = registroLeido.substring(51, 81).trim();
												apellidoM = registroLeido.substring(81, 111).trim();
												nombre = registroLeido.substring(111, 141).trim();
												rfc = registroLeido.substring(141, 154).trim();
												sexo = registroLeido.substring(154, 155).trim();
												calle = registroLeido.substring(155, 215).trim();
												colonia = registroLeido.substring(215, 245).trim();
												delegacion = registroLeido.substring(245, 280).trim();
												cveEstado = registroLeido.substring(280, 284).trim();
												ciudad = registroLeido.substring(284, 319).trim();
												codigoPostal = registroLeido.substring(319, 324).trim();
												clavePais = registroLeido.substring(324, 328).trim();
												prefTelPart = registroLeido.substring(328, 340).trim();
												telPart = registroLeido.substring(340, 352).trim();
												calleOfi = registroLeido.substring(352, 412).trim();
												coloniaOfi  = registroLeido.substring(412, 442).trim();
												delegOfi  = registroLeido.substring(442, 477).trim();
												cveEstadoOfi  = registroLeido.substring(477, 481).trim();
												ciudadOfi  = registroLeido.substring(481, 516).trim();
												codPostalOfi  = registroLeido.substring(516, 521).trim();
												paisOfi  = registroLeido.substring(521, 525).trim();
												cveDireccionOfi  = registroLeido.substring(525, 532).trim();
												prefTelOfi  = registroLeido.substring(532, 544).trim();
												telOfi  = registroLeido.substring(544, 556).trim();
												extOfi  = registroLeido.substring(564, 568).trim();



													if (valida.validaCampoTexto(numEmpl) && valida.validaCampoTexto(deptoEmpl) && valida.validaCampoNumero(ingresoMensual) && valida.validaCampoTexto(apellidoP) && valida.validaCampoTexto(apellidoM) && valida.validaCampoTexto(nombre)
														&& valida.validaCampoTexto(rfc) && valida.validaCampoTexto(sexo) && valida.validaCampoTexto(calle) && valida.validaCampoTexto(colonia) && valida.validaCampoTexto(delegacion) && valida.validaCampoTexto(cveEstado) && valida.validaCampoTexto(ciudad) && valida.validaCampoTexto(codigoPostal)
														&& valida.validaCampoTexto(clavePais) && valida.validaCampoTexto(prefTelPart) && valida.validaCampoNumero(telPart) && valida.validaCampoTexto(calleOfi) && valida.validaCampoTexto(coloniaOfi) && valida.validaCampoTexto(delegOfi) && valida.validaCampoTexto(cveEstadoOfi)
														&& valida.validaCampoTexto(ciudadOfi) && valida.validaCampoTexto(codPostalOfi) && valida.validaCampoTexto(paisOfi) && valida.validaCampoNumero(cveDireccionOfi) && valida.validaCampoNumero(prefTelOfi) && valida.validaCampoNumero(telOfi) && valida.validaCampoNumero(extOfi))

													{


														if (!numEmpl.equals("")){

															if(apellidoP.equals("")){
																listaErrorCampo.add(new String("Apellido Paterno@" + i ));
															}else if(nombre.equals("")){
																listaErrorCampo.add(new String("Nombre@" + i ));
															}else if(rfc.equals("")){
																listaErrorCampo.add(new String("RFC@" + i ));
															}else if(ingresoMensual.equals("")){
																listaErrorCampo.add(new String("Salario@" + i ));
															}



														}
														else{
															System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Datos Obligatotios ");
														      listaErrorCampo.add(new String("Numero de Empleado@" + i ));
														}


													}
													else
													{
														System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Datos incorrectos ");
														listaErrorDatos.add(new Integer(i));
													}

												}	// FIN if - Valida codigo bancario
												else
												{
													System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Codigo Bancario Incorrecto ");
													listaErrorSantander.add(new Integer(i));
												}
										}
										else
										{
											bean.setFalloInesperado(true);
										}

								   }
								   else{
										System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Datos Obligatotios ");
										listaErrorCampo.add(new String("Cuenta abono@" + i ));

									}


							}
							else {
								System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Datos incorrectos ");
								listaErrorDatos.add(new Integer(i));
							}



						}
						else {
							System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Longitud Incorrecta ");
							listaErrorLong.add(new Integer(i));
						}
						posicion = archivoAlta.getFilePointer();


				} while (posicion < archivoAlta.length());
				bean.setTotalRegistros(i);
				bean.setListaRegistros(listaRegistros);
			}
			System.out.println("CatNominaValidaImport - validaLineaAlta $ Longitud " + listaErrorLong.size());
			System.out.println("CatNominaValidaImport - validaLineaAlta $ Datos " + listaErrorDatos.size());
			System.out.println("CatNominaValidaImport - validaLineaAlta $ Cuentas " + listaErrorCuentas.size());
			System.out.println("CatNominaValidaImport - validaLineaAlta $ Campo " + listaErrorCampo.size());
			System.out.println("CatNominaValidaImport - validaLineaAlta $ Cuenta Abono " + listaErrorCuenta.size());





			archivoAlta.close();
		} catch (Exception e) {
			e.printStackTrace();
			bean.setFalloInesperado(true);
		}

		System.out.println("CatNominaValidaImport - validaLineaAlta $ errores " + linea);
		if(listaErrorLong.size() > 1)
		{
			listaErrores = listaErrorLong;
		}
		else if(listaErrorSantander.size() > 1)
		{
			listaErrores = listaErrorSantander;
		}
		else if(listaErrorDatos.size() > 1)
		{
			listaErrores = listaErrorDatos;
		}
		else if(listaErrorCuentas.size() > 1)
		{
			listaErrores = listaErrorCuentas;
		}
		else if(listaErrorCampo.size() > 1)
		{
			listaErrores = listaErrorCampo;
		}
		else if (listaErrorCuenta.size() > 1)
		{
			listaErrores = listaErrorCuenta;
		}
		bean.setListaErrores(listaErrores);
		return bean;
	}








	//******MODIFICA ARCHIVO INTERB****************

	/**************************************************************************************/
	/** Metodo: validaLineaModif
	 * @param String, int
	 * @description: Hace la validaci�n del tamano de la linea del archivo
	 * 				 verifica que el tamano de la linea sea solo de 11 � 18 numeros
	/*************************************************************************************/
	public CatNominaBean validaArchModifInterb(String archivoCargado, String numContrato, HttpServletRequest request) throws IOException
	{
		System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Inicio ");
		CatNominaBean bean = new CatNominaBean();
		String linea = "";
		ArrayList listaErrorCuentas = new ArrayList();
		ArrayList listaErrorLong = new ArrayList();
		ArrayList listaErrorDatos = new ArrayList();
		ArrayList listaErrores = new ArrayList();
		ArrayList listaRegistros = new ArrayList();
		ArrayList listaErrorCampo = new ArrayList();
		ArrayList listaErrorCuenta = new ArrayList();
		ArrayList listaErrorCtaInterna = new ArrayList();

		//VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA
		String[] arrDatosActual = new String[6];
		RandomAccessFile archivoModif;
		int i = 0;

		try {
			archivoModif = new RandomAccessFile(archivoCargado, "rw");
			String registroLeido = "";
			String numCta = "";
			String numEmpl = "";
			String deptoEmpl = "";
			String ingresoMensual = "";
			String sexo = "", calle = "", colonia = "", delegacion = "", cveEstado = "", ciudad = "", codigoPostal = "", clavePais = "", prefTelPart = "", telPart = "";
			String calleOfi = "", coloniaOfi = "", delegOfi = "", cveEstadoOfi = "", ciudadOfi = "", codPostalOfi = "", paisOfi = "", cveDireccionOfi = "", prefTelOfi = "", telOfi = "", extOfi = "";

			long posicion = 0;
			int existeRelacion = 0;

			listaErrorLong.add(new String("Long"));
			listaErrorCuentas.add(new String("Cuentas"));
			listaErrorDatos.add(new String("Datos"));
			listaErrorCampo.add(new String("Campo"));
			listaErrorCtaInterna.add(new String ("Cuenta Interna"));
			listaErrorCuenta.add(new String ("Cuenta Abono"));

			if (archivoModif.length() > 0) {
				archivoModif.seek(0);
				do {
					i++;
					registroLeido = archivoModif.readLine();

					if (registroLeido.length() == 465) {
						System.out.println("CatNominaValidaInterbn - validaArchModifInterb $ Longitud Correcta ");
						CatNominaValidaImport valida = new CatNominaValidaImport();
						numEmpl = registroLeido.substring(18, 25).trim();
						deptoEmpl = registroLeido.substring(25, 31).trim();
						ingresoMensual = registroLeido.substring(31, 51).trim();
						sexo = registroLeido.substring(51, 52).trim();
						calle = registroLeido.substring(52, 112).trim();
						colonia = registroLeido.substring(112, 142).trim();
						delegacion = registroLeido.substring(142, 177).trim();
						cveEstado = registroLeido.substring(177, 181).trim();
						ciudad = registroLeido.substring(181, 216).trim();
						codigoPostal = registroLeido.substring(216, 221).trim();
						clavePais = registroLeido.substring(221, 225).trim();
						prefTelPart = registroLeido.substring(225, 237).trim();
						telPart = registroLeido.substring(237, 249).trim();
						calleOfi = registroLeido.substring(249, 309).trim();
						coloniaOfi  = registroLeido.substring(309, 339).trim();
						delegOfi  = registroLeido.substring(339, 374).trim();
						cveEstadoOfi  = registroLeido.substring(374, 378).trim();
						ciudadOfi  = registroLeido.substring(378, 413).trim();
						codPostalOfi  = registroLeido.substring(413, 418).trim();
						paisOfi  = registroLeido.substring(418, 422).trim();
						cveDireccionOfi  = registroLeido.substring(422, 429).trim();
						prefTelOfi  = registroLeido.substring(429, 441).trim();
						telOfi  = registroLeido.substring(441, 453).trim();
						extOfi  = registroLeido.substring(453, 465).trim();

						if (valida.validaCampoTexto(numEmpl) && valida.validaCampoTexto(deptoEmpl) && valida.validaCampoNumero(ingresoMensual) && valida.validaCampoTexto(sexo) && valida.validaCampoTexto(calle)
								&& valida.validaCampoTexto(colonia) && valida.validaCampoTexto(delegacion) && valida.validaCampoTexto(cveEstado) && valida.validaCampoTexto(ciudad) && valida.validaCampoTexto(codigoPostal)
								&& valida.validaCampoTexto(clavePais) && valida.validaCampoTexto(prefTelPart) && valida.validaCampoNumero(telPart) && valida.validaCampoTexto(calleOfi) && valida.validaCampoTexto(coloniaOfi) && valida.validaCampoTexto(delegOfi) && valida.validaCampoTexto(cveEstadoOfi)
								&& valida.validaCampoTexto(ciudadOfi) && valida.validaCampoTexto(codPostalOfi) && valida.validaCampoTexto(paisOfi) && valida.validaCampoNumero(cveDireccionOfi) && valida.validaCampoNumero(prefTelOfi) && valida.validaCampoNumero(telOfi) && valida.validaCampoNumero(extOfi))
						{
							if (!numEmpl.equals(""))
							{
								if(ingresoMensual.equals("")){
										listaErrorCampo.add(new String("Salario@" + i ));
								}

								numCta = registroLeido.substring(0, 18).trim();

								if(valida.validaCampoNumero(numCta))
								{
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Correcto ");
								}
								else {
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos incorrectos ");
									listaErrorDatos.add(new Integer(i));
								}
								int a = numCta.length();
								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ LONGITUD CTA ABONO " + a);

								if (a == 18){
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Valido cuenta abono ");
								}
								else{
									System.out.println("CatNominaValidaInterb - validaArchivoAlta $ Cuenta abono incorrecta ");
									listaErrorCuenta.add(new Integer(i));
								}

								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Num Cuenta a validar = " + numCta);

								if(!numCta.equals("")){
									System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");

									CatNominaDAO dao = new CatNominaDAO();
									existeRelacion = dao.existeRegistro(numContrato, numCta);

									if(existeRelacion != -100)
									{
										if(existeRelacion>0){
											System.out.println("CatNominaValidaImport - validaLineaModif $ Relaci�n Existente - Linea VALIDA " + registroLeido );
											listaRegistros.add(new String(registroLeido));

												/*
											 * VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA **** inicio
											 * */

											arrDatosActual = dao.obtenDatosEmplModifInterb(numContrato, numCta);

											if(!arrDatosActual[0].equals("ERROR0000"))
											{
												int Detalle = dao.existeRelacionCtaInterna(arrDatosActual, numContrato, numCta);

												if(Detalle == 0){
													System.out.println("CatNominaValidaInterb - validaLineaModif $ Relaci�n Existente - Linea VALIDA " + registroLeido );
												}
												else{
													System.out.println("CatNominaValidaInterb - validaArchModifInterb $No existe relacion ");
													listaErrorCtaInterna.add(new Integer(i));
												}
											}
											else{
												bean.setFalloInesperado(true);
											}
												/*
												 * VALIDACION DE EXISTENCIA DE RELACION CON UNA CUENTA INTERNA **** fin
												 * */
										}
										else{
											System.out.println("CatNominaValidaInterb - validaArchModifInterb $No existe relacion ");
											listaErrorCuentas.add(new Integer(i));
										}
									}
									else{
										bean.setFalloInesperado(true);
										}
								    }
									else{
										System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");
										listaErrorCampo.add(new String("Cuenta abono@" + i));
									}

							}
							else{
								System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos Obligatotios ");
							      listaErrorCampo.add(new String("Numero de Empleado@" + i ));
							}

						}
						else
						{
							System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Datos incorrectos ");
							listaErrorDatos.add(new Integer(i));
						}
					}
					else {
						System.out.println("CatNominaValidaInterb - validaArchModifInterb $ Longitud INCorrecta ");
						listaErrorLong.add(new Integer(i));
					}
					posicion = archivoModif.getFilePointer();

				} while (posicion < archivoModif.length());
				bean.setTotalRegistros(i);
				bean.setListaRegistros(listaRegistros);
			}
			System.out.println("CatNominaValidaImport - validaLineaModif $ Longitud " + listaErrorLong.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Datos " + listaErrorDatos.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuentas " + listaErrorCuentas.size());

			System.out.println("CatNominaValidaImport - validaLineaModif $ Campo " + listaErrorCampo.size());

			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuenta Abono " + listaErrorCuenta.size());
			System.out.println("CatNominaValidaImport - validaLineaModif $ Cuenta Interna " + listaErrorCtaInterna.size());

			archivoModif.close();
		} catch (Exception e) {
			e.printStackTrace();
			bean.setFalloInesperado(true);
		}

		System.out.println("CatNominaValidaImport - validaLineaModif $ errores " + linea);
		if(listaErrorLong.size() > 1)
		{
			listaErrores = listaErrorLong;
		}
		else if(listaErrorDatos.size() > 1)
		{
			listaErrores = listaErrorDatos;
		}
		else if(listaErrorCuentas.size() > 1)
		{
			listaErrores = listaErrorCuentas;
		}
		else if (listaErrorCampo.size() > 1)
		{
			listaErrores = listaErrorCampo;
		}
		else if (listaErrorCuenta.size() > 1)
		{
			listaErrores = listaErrorCuenta;
		}
		else if (listaErrorCtaInterna.size() > 1)
		{
			listaErrores = listaErrorCtaInterna;
		}

		bean.setListaErrores(listaErrores);
		return bean;
	}
}