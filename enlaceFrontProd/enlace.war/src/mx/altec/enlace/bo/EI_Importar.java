package mx.altec.enlace.bo;

import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.MultipartRequest;

import java.io.*;
import java.util.zip.*;

public class EI_Importar implements Serializable
 {
	public String nombreArchivo="";

	public String getNombre()
	 {
	   if(nombreArchivo.indexOf("/")>=0)
		  return nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
	   return nombreArchivo;
	 }

	public String getNombreDeArchivo()
	 {
		return nombreArchivo;
	 }

	public String getExtension()
	 {
		if(nombreArchivo.indexOf(".")>=0)
		 return nombreArchivo.substring(0,nombreArchivo.indexOf("."));
		return "";
	 }

	public String getPath()
	 {
		if(nombreArchivo.indexOf("/")>=0)
		  return nombreArchivo.substring(0,nombreArchivo.lastIndexOf("/"));
	   return "";
	 }

 	public String importaArchivo(HttpServletRequest req, HttpServletResponse res ) throws IOException
	 {
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");

		String sFile = "";

		boolean errorEnZip=false;

		try
		  {
			EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():Tam�o del archivo antes: "+req.getContentLength(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():Parseando objeto multipart ", EIGlobal.NivelLog.INFO);

			// Asignacion dinamica de espacio
			MultipartRequest mprFile = new MultipartRequest ( req, IEnlace.LOCAL_TMP_DIR,20971520   );
			//MultipartRequest mprFile = new MultipartRequest ( req, IEnlace.LOCAL_TMP_DIR,req.getContentLength() );
			java.io.FileInputStream entrada;
			java.util.Enumeration files = mprFile.getFileNames ();

			// Se obtiene el nombre del archivo enviado
			if( files.hasMoreElements () )
			 {
				String name = ( String ) files.nextElement ();
				sFile = mprFile.getFilesystemName ( name );
			 }
			EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():Fin Parser - Archivo:  "+ sFile, EIGlobal.NivelLog.INFO);

			// Si el archivo es .zip se descomprime
			if (sFile.toLowerCase().endsWith(".zip"))
			  {
				EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():El archivo viene en zip", EIGlobal.NivelLog.INFO);

				final int BUFFER = 2048;
				BufferedOutputStream dest = null;
				FileInputStream fis = new FileInputStream(IEnlace.LOCAL_TMP_DIR+"/" + sFile);
				ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
				ZipEntry entry;
				entry = zis.getNextEntry();

				EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():entry: "+entry, EIGlobal.NivelLog.INFO);
				if(entry==null)
				 {
				   EIGlobal.mensajePorTrace("EI_Importar - importaArchivo():Entry fue null, archivo no es zip o esta da�ando ", EIGlobal.NivelLog.INFO);
				   errorEnZip=true;
				 }
			   else
				 {
					EIGlobal.mensajePorTrace( "EI_Importar - importaArchivo():Extrayendo "  + entry, EIGlobal.NivelLog.INFO);

					int count;
					byte data[] = new byte[BUFFER];

					// Se escriben los archivos a disco
					// Nombre del archivo zip
					File archivocomp = new File( IEnlace.LOCAL_TMP_DIR +"/" + sFile );
					// Nombre del archivo que incluye el zip
					sFile = entry.getName();

					FileOutputStream fos = new FileOutputStream(IEnlace.LOCAL_TMP_DIR + "/"+sFile);
					dest = new BufferedOutputStream(fos, BUFFER);
					while((count = zis.read(data, 0, BUFFER)) != -1)
					  dest.write(data, 0, count);
					dest.flush();
					dest.close();

					// Borra el archivo comprimido
					if (archivocomp.exists())
					  archivocomp.delete();
					EIGlobal.mensajePorTrace( "EI_Importar - importaArchivo():Fin extraccion de: "  + entry, EIGlobal.NivelLog.INFO);
				 }
			   zis.close();
			 }

		   if(!errorEnZip)
			 nombreArchivo=IEnlace.LOCAL_TMP_DIR +"/"+ sFile;
		   else
			 nombreArchivo="ZIPERROR";

		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "EI_Importar - importaArchivo():Recibe Archivo", EIGlobal.NivelLog.INFO);
			e.printStackTrace();
		}

	   return nombreArchivo;
	 }

   public boolean cambiaNombreArchivo(String nombre)
	{
	    boolean cambio=true;
		EIGlobal.mensajePorTrace( "EI_Importar - cambiaNombreArchivo():Cambiar nombre al archivo" + nombre, EIGlobal.NivelLog.INFO);
		try
		 {
			EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);
			// Renombra el archivo descargado
			File archivo = new File( nombreArchivo );
			if (archivo.exists())
			  {
				EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Se renombra de: "+ nombreArchivo + " a "+nombre, EIGlobal.NivelLog.INFO);
				archivo.renameTo(new File(nombre));
			  }
			else
			 {
			   EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():El archivo no existe.", EIGlobal.NivelLog.INFO);
			   cambio=false;
			   // throw file not found exception
			 }
		  } catch(Exception e)
			{
			  EIGlobal.mensajePorTrace("EI_Importar - cambiaNombreArchivo():Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);
			  cambio=false;
			}
	     return cambio;
	}
   public boolean cambiaNombreArchivo(String nombreOri , String nombreNuevo)

   {
      nombreArchivo = nombreOri;
      return cambiaNombreArchivo(nombreNuevo);    
   }
 }