/**
 * 
 */
package mx.altec.enlace.bo;

/**
 * @author rgutierrez
 */
public class ValidationException extends Exception {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 6858218594743141419L;
	/**
	 * Constructor por default.
	 *
	 */
	public ValidationException() {
		
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 */
	public ValidationException(String message) {
		super(message);
	}
	/**
	 * Constructor heredado.
	 * @param cause causa
	 */
	public ValidationException(Throwable cause) {
		super(cause);
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 * @param cause causa
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
