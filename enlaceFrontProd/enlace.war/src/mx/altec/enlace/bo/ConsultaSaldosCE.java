// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;
import java.util.Date;

/**
 * Esta clase se encarga de interpreta y guardar los valores de la trama de respuesta de consulta<BR>
 * de saldos SDCT.
 * <P>
 * Trama saldo respuesta:
 * OK         61152         25.86          0.00         25.86          0.00
 * OK         20184    1008429.99          0.00    1008429.99          0.00
 * </P><P>
 * 26/04/2002: Se implant� la interfase Serializable<BR>
 * 24/07/2002: Se agreg&oacute; la verificaci&oacute;n de tiempo.<BR>
 * 25/07/2002: Se corrigi&oacute; la comparaci&oacute;n por tiempos.<BR>
 * 23/08/2002: Se eliminan posibles espacios en <B>saldo</B> y se quita el signo de pesos.<BR>
 * 12/10/2002: Se convirti&oacute; esta clase en un JavaBean.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.2
 */
public class ConsultaSaldosCE implements Serializable
{

/**Define tiempo m&aacute;ximo para considerar los datos como actuales.
*/
public static final long TIEMPO_LIMITE=150000;

/**Cadena correspondiente al n&uacute;mero de cuenta.
*/
private String cuenta="";

/**Descripci&oacute;n de la cuenta.
*/
private String descripcion="";

/**Folio o referencia obtenida como comprobante del servicio de tuxedo.
*/
private String referencia;

/**Saldo de la cuenta de cheques.
*/
private String saldo;

/**Guarda la trama de respuesta.
*/
private String respuestaSDCT=null;

/**Indica si la trama fue interpretada correctamente o no.
*/
private boolean esCorrecta=false;

/**Arreglo de enteros que indican la posici&oacute;n de cada elemento de la trama.
*/
public static int[] posiciones={8, 8, 14};

/**Sirve para comparar los dos primeros caracteres de la trama.
*/
public static final String OK="OK";

/**Indica el tama&ntilde;o m&iacute;nimo de la trama de respuesta.
*/
public static final int tamanoDatosSDCT=30;


/**Indica cu&aacute;ndo guard&oacute; los valores
*/
private Date fechaGeneracion=null;


  /**
   * Constructor de esta clase.  Asigna la cuenta y la descripci&oacute;n.
   */
   /*
  public ConsultaSaldosCE(String cuenta, String descripcion)
  {
    if(cuenta!=null)
      this.cuenta=cuenta;


    if(descripcion!=null)
      this.descripcion=descripcion;

  } // Fin constructor
  */


   /**Nuevo constructor sin par&aactue;metros.
   */
   public ConsultaSaldosCE()
   {
   } // Fin constructor sin par�metros


   /**<code><B><I>descomponeTrama</I></B></code> interpreta la trama de respuesta de consulta de saldos.
   * <P>Regresa verdadero si la trama es de respuesta positiva y trae datos.
   * </P>
   * @param respuestaSDCT Trama de respuesta del servicio de tuxedo SDCT
   */
  public boolean descomponeTrama(String respuestaSDCT)
  {
    //BaseServlet bs=new BaseServlet();
    String aux;


    if(respuestaSDCT==null)
      return false;

    if(respuestaSDCT.length()<tamanoDatosSDCT)
      return false;

    try
    {
      aux=respuestaSDCT.substring(0, posiciones[0]).trim();



      if(!aux.equalsIgnoreCase(OK))
        return false;


      aux=respuestaSDCT.substring(posiciones[0], posiciones[0]+posiciones[1]).trim();



      if(aux==null || aux.length()<1)
        return false;

      referencia=aux;


      aux=respuestaSDCT.substring(posiciones[0]+posiciones[1], posiciones[0]+posiciones[1]+posiciones[2]).trim();
    }
    catch(ArrayIndexOutOfBoundsException aioob)
    {
      return false;
    } // Fin try-catch


    if(aux==null || aux.trim().length()<1)
      return false;

    saldo=aux.trim();

    esCorrecta=true;
    fechaGeneracion=new Date();

    return esCorrecta;
  } // Fin descomponeTrama


  public String getCuenta()
  {
    return cuenta;
  } // Fin m�todo getCuenta


  public String getDescripcion()
  {
    return descripcion;
  } // Fin getDescripcion


  public String getSaldo()
  {
    return saldo;
  } // Fin getSaldo


  public String getReferencia()
  {
    return referencia;
  } // Fin getReferencia


  public String getRespuestaSDCT()
  {
    return respuestaSDCT;
  } // Fin getRespuestaSDCT


  public boolean getEsCorrecta()
  {
    return esCorrecta;
  } // Fin getEsCorrecta


   /**<code><B><I>esCaduco</I></B></code>
   *  <P>Regresa verdadero si los datos los ha obtenido en un lapso menor a <B>TIEMPO_LIMITE</B><BR>
   *  </P>
   */
   public boolean esCaduco()
   {
      int comparacion;
      Long l1;
      Long l2;

      if(fechaGeneracion==null)
         return true;

      l1=new Long((new Date()).getTime()-fechaGeneracion.getTime());
      l2=new Long(TIEMPO_LIMITE);



      comparacion=l1.compareTo(l2);



      //l3=l1-l2;

      if(comparacion<0)
      {
         return false;
      }
      else
      {
         return true;
      } // Fin if-else
   } // Fin esCaduco

   public void setCuenta(String cuenta)
   {
      if(cuenta!=null && cuenta.length()>0)
         this.cuenta=cuenta;
   } // Fin setCuenta


   public void setDescripcion(String descripcion)
   {
      if(descripcion!=null && descripcion.length()>0)
         this.descripcion=descripcion;

   } // Fin if setDescripcion

} // Fin clase ConsultaSaldosCE

