package mx.altec.enlace.bo;

import mx.altec.enlace.beans.PagoMicrositioRefBean;
import mx.altec.enlace.utilerias.EIGlobal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class PagoMicrositioRefBo {

	/** Algoritm SHA1 */
	final static String SHA1 = "SHA-1";
	
	/** Solo Numeros */
	final static Pattern IS_NUMERIC = Pattern.compile("[\\d]+");
	
	/** Es alfanumerica */
	final static Pattern IS_ALPHANUM = Pattern.compile("[\\w]+");
	
	/**
     * Valida el hash recibido a traves de pago referenciado micrositio
	 * @param bean contiene los datos recibidos
	 * @return res hash valido
	 */
	public boolean validaHash(PagoMicrositioRefBean bean)
	{
		boolean res = false;
		StringBuffer cadena = new StringBuffer();
		StringBuffer msgError = new StringBuffer();
		String hash = "";

		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaHash() :: validando hash", EIGlobal.NivelLog.DEBUG);

		cadena.append(bean.getTipoPersona());
		cadena.append(' ');
		cadena.append(bean.getLineaCaptura());
		cadena.append(' ');
		cadena.append(bean.getImporteSat());
		cadena.append(' ');
		cadena.append(bean.getRfc());

		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: cadena a cifrar: "+ cadena, EIGlobal.NivelLog.DEBUG);

		hash = getSha(cadena.toString());

		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: cadenas a comparar: --Original-- "+ bean.getDatosHash() + "--Cifrada--"+ hash, EIGlobal.NivelLog.DEBUG);

		if(hash.equals(bean.getDatosHash()))
		{
			res = true;
		}
		else
		{
			msgError.append("Los datos para realizar el pago son incorrectos, favor de validar sus datos e intentar nuevamente.");
			bean.setMsgError(msgError);
		}

		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaHash() :: validando hash: "+ res, EIGlobal.NivelLog.DEBUG);

		return res;
	}
	
	
	/**
	 * Genera el sha1 en binario
	 * @param cadena contine los datos a cifrar
	 * @return cadena hash
	 */
	
	public static String getSha(String cadena){

		try{
    		MessageDigest md    = MessageDigest.getInstance(SHA1);
			byte [] hash        = md.digest(cadena.getBytes());

			return toHexadecimal(hash);
		}
		catch(NoSuchAlgorithmException e){
			EIGlobal.mensajePorTrace("PagoMicrositioBo - NoSuchAlgorithmException - Exception ["+ e + "]", EIGlobal.NivelLog.INFO);
		}
		return "";
	}
	
	/**
	 * Conevierte el hash a exadecimal
	 * @param digest hash binario
	 * @return hash en exadecimal
	 */
	public static String toHexadecimal (byte[] digest)
	{
		StringBuffer hash = new StringBuffer();
		for (byte aux : digest)
		{
			int b = aux & 0xff;
			if(Integer.toHexString(b).length() == 1){
				hash.append("0");
			}
			hash.append(Integer.toHexString(b));
		}
		return hash.toString();
	}
	
	/**
	 * Valida si algun dato viene vacio
	 * @param bean continene los datos recibidos de la URL
	 * @return res verdadero o falso
	 */
	
	public boolean validaDatos(PagoMicrositioRefBean bean)
	{
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Inicio validando datos generales", EIGlobal.NivelLog.DEBUG);
		boolean res = true;
		StringBuffer msgError = new StringBuffer();;
		
		msgError.append("<br> El campo<strong> ");
		
		//Inician validaciones Generales
		if(null == bean.getIdSat() || "".equals(bean.getIdSat()))
		{
			msgError.append("ID, ");
			res = false;
		}
		
		if(null == bean.getTipoPersona() || "".equals(bean.getTipoPersona()))
		{
			msgError.append("Tipo persona, ");
			res = false;
		}
		
		if(null == bean.getLineaCaptura() || "".equals(bean.getLineaCaptura()))
		{
			msgError.append("l&iacute;nea de captura, ");
			res = false;
		}
		
		if(null == bean.getImporteSat() || "".equals(bean.getImporteSat()))
		{
			msgError.append("importe, ");
			res = false;
		}
		
		if(null == bean.getRfc() || "".equals(bean.getRfc()))
		{
			msgError.append("RFC, ");
			res = false;
		}
		if (!res)
		{
			msgError.append("</strong> no contiene informaci&oacute;n. Favor de validar e intentar nuevamente.");
			bean.setMsgError(msgError);
			return res;
		}
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Fin validando datos generales"+ res, EIGlobal.NivelLog.DEBUG);
		//Inicia validación importe
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Inicio validando datos importe", EIGlobal.NivelLog.DEBUG);
		if(!valFormatoCampo(bean.getImporteSat(),1))
		{
			res = false;
			msgError =  new StringBuffer();
			msgError.append("El Importe es incorrecto, contiene centavos.");
			bean.setMsgError(msgError);
			return res;
		}
		if("0".equals(bean.getImporteSat()))
		{
			res = false;
			msgError =  new StringBuffer();
			msgError.append("El Importe debe ser mayor a cero.");
			bean.setMsgError(msgError);
			return res;
		}
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Fin validando datos importe"+res, EIGlobal.NivelLog.DEBUG);
		//Inicia validación linea de captura
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Inicio validando datos LC", EIGlobal.NivelLog.DEBUG);
		if("00000000000000000000".equals(bean.getLineaCaptura()))
		{
			res = false;
			msgError =  new StringBuffer();
			msgError.append("La Línea de captura no es v&aacute;lida.");
			bean.setMsgError(msgError);
			return res;
		}
		
		if(!valFormatoCampo(bean.getLineaCaptura(),2))
		{
			res = false;
			msgError =  new StringBuffer();
			msgError.append("La l&iacute;nea de captura no es v&aacute;lida, esta no debe contener caracteres especiales o espacios en blanco.");
			bean.setMsgError(msgError);
			return res;
		}
		
		if(bean.getLineaCaptura().trim().length() != 20)
		{
			res = false;
			msgError =  new StringBuffer();
			msgError.append("La l&iacute;nea de captura debe ser de 20 posiciones.");
			bean.setMsgError(msgError);
			return res;
		}
		EIGlobal.mensajePorTrace("PagoMicrositioRefBo.java :: validaDatos() :: Fin validando datos LC"+res, EIGlobal.NivelLog.DEBUG);

		return res;
	}
	
	/**
	 * Valida el formato de los campos importe y linea de captura
	 *
	 * @param parametro cadena a validar
	 * @param opcion tipo de validación
	 * @return true, Si la cadena es valida
	 */
	 private static boolean valFormatoCampo(String parametro, int opcion) {
		boolean result = false;

		Pattern p = null;

		if(opcion == 1) { //Numerico
			p = IS_NUMERIC;
		}
		if(opcion == 2) { //Alfanumerico
			p = IS_ALPHANUM;
		}

		result = p.matcher(parametro).matches();
		return result ;
	}
}