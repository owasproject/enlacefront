package mx.altec.enlace.bo;

import mx.altec.enlace.dao.WSCnfLinCapDAO;

/**
 * Mejoras de Linea de Captura Clase que consume las operaciones
 * (PagoReferenciadoSatDAO) relacionadas a Pago Referenciado SAT Se obtiene
 * llave de pago, numero de operacion, cuenta, razon social.
 * 
 * @author RRR - Indra Marzo 2014
 */
public class WSCnfLinCapBO {

	/**
	 * Se ejecuta el select para obtener el estado de la bandera para el empleo del Webservice de Validaci�n de L�nea de Captura
	 * 
	 * @return Boolean el estado de la bandera
	 */
	public Boolean consBanderaLC() {
		WSCnfLinCapDAO wsCnfLCDao = new WSCnfLinCapDAO();
//		WSCnfLinCapBean wsCnfLCBean = new WSCnfLinCapBean();
		return wsCnfLCDao.consBndWSLinCap();
	}

	/**
	 * Se ejecuta el select para saber si la cuenta existe en el cat�logo para el empleo del Webservice de Validaci�n de L�nea de Captura
	 * 
	 * @param cuenta
	 *            Cuenta de Abono que se validar� en el cat�logo
	 * @return Boolean existe la cuenta de abono en el cat�logo
	 */
	public Boolean existeCtaLC(String cuenta) {
		WSCnfLinCapDAO wsCnfLCDao = new WSCnfLinCapDAO();
//		WSCnfLinCapBean wsCnfLCBean = new WSCnfLinCapBean();
		return wsCnfLCDao.consCtaWSLinCap(cuenta);
	}

	/**
	 * Se ejecuta el select para obtener la URL del Webservice de Validaci�n de L�nea de Captura
	 * 
	 * @return String URL del Webservice de Validaci�n de L�nea de Captura
	 */
	public String consURLLC() {
		WSCnfLinCapDAO wsCnfLCDao = new WSCnfLinCapDAO();
//		WSCnfLinCapBean wsCnfLCBean = new WSCnfLinCapBean();
		return wsCnfLCDao.consURLWSLinCap();
	}

	/**
	 * Se ejecuta el select para obtener el Timeout para el llamado del Webservice de Validaci�n de L�nea de Captura
	 * 
	 * @return int Timeout para el llamado del Webservice de Validaci�n de L�nea de Captura
	 */
	public Long consTmoLC() {
		WSCnfLinCapDAO wsCnfLCDao = new WSCnfLinCapDAO();
//		WSCnfLinCapBean wsCnfLCBean = new WSCnfLinCapBean();
		return wsCnfLCDao.consTmoWSLinCap();
	}

}
