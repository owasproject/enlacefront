package mx.altec.enlace.bo;

import java.util.List;

import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.dao.CtasMovilesDAO;

public class ConsultaCtasMovilesBO {
	
	/**
	 * ctaMovilDao
	 */
	private final transient CtasMovilesDAO ctaMovilDao = new CtasMovilesDAO();
	
	
	/**
	 * Realiza la coansulta de ctas moviles
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return List<ConsEdoCtaResultadoBean> : List<ConsEdoCtaResultadoBean>
	 */
	public List<CuentaMovilBean> consultaCtasMoviles(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		
		return ctaMovilDao.consultaCtasMoviles(consultaCtasMovilesBean);
	}
	
}