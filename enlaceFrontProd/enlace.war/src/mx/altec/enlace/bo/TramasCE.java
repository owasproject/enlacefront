// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.*;

import mx.altec.enlace.utilerias.IEnlace;

/**<code><B><I>TramasCE</I></B></code> contiene la definici&oacute;n de tramas de consulta de cr&eacute;dito electr&oacute;nico.
 * <P>
 * Fecha de &uacute;ltima modificaci&oacute;n: <I>24/04/2002</I><BR><BR>
 * Se agreg&oacute; trama PC51 (tasas)<BR>
 * 29/04/2002: Se agregaron los m&eacute;todos <I>consultaCobroAdeudos</I><BR>
 * <I>consultaCotizacionDisposicion</I> y <I>tramaConsultaDisposicion</I><BR>
 * 20/05/2002 Se agreg&oacute; el m&eacute;todo <I>quitaPunto</I><BR>
 * 30/05/2002 Se corrigi&oacute; <I>tramaConsultaTasas</I><BR>
 * 06/08/2002 Se corrigi&oacute; tipo de operaci&oacute;n <I>cobroAdeudo</I>.<BR>
 * 23/08/2002 Estandarizaci&oacute;n de variables.<BR>
 * 03/10/2002 Se corrigi&oacute; el tipo de operaci&oacute; en un m&eacute;todo sobrecargado de consulta de saldos.<BR>
 * 08/10/2002 Verificaci&oacute;n tipo de operaci&oacute;n.<BR>
 * 10/10/2002 Correcci&oacute;n tipo de operaci&oacute;n.<BR>
 * 12/10/2002 Se agreg&oacute; formato a la tasa para <B><I>tramaConsultaDisposicion</I></B>.
 * 06/11/2002 Se cambia el formato de la tasa para la trama de disposici&oacute;n<BR>
 * 08/11/2002 Se coloca el concepto de cotizaci&oacute;n como cadena vac&iacute;a si es nula.
 * 10/12/2002 Revisi&oacute;n avisos cr&eacute;dito electr&oacute;nico.
 * </P>
 * <p>Modificado por: Hugo Ru&iacute;z Zepeda</p>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.3.0
 */
public class TramasCE implements Serializable
{

  //private transient IEnlace enlace;
  /**Identifica a las cuentas de tipo personal.
  */
  public static final String PERSONAL="P";

  /**Conector o "pipe".
  */
  public static final String CONECTOR="|";

  /**Caracter arroba.
  */
  public static final String ARROBA="@";

  /**Espacio o "blanco".
  */
  public static final String ESPACIO=" ";

  /**Define los tipos de medio de entrega, trama o archivo, respectivamente.
  */
  //public static final String[] MEDIOS_WEB={"1EWEB", "2EWEB"};
  public static final String[] MEDIOS_WEB={IEnlace.medioEntrega1, IEnlace.medioEntrega2};

  /**Medio de entrega de trama.
  */
  public static final int TRAMA=0;

  /**Medio de entrega de archivo.
  */
  public static final int ARCHIVO=1;

  /**Tipo de operaci&oacute;n de consulta de saldos.
  */
  public static final String SDCT="SDCT";

  /**Tipo de operaci&oacute;n de consulta de l&iacute;neas de cr&eacute;dito
  */
  public static final String CEDC="CEDC";

  /**Tipo de operaci&oacute;n para consultas de posici&oacute;n
  */
  public static final String PC49="PC49";

  /**Tipo de operaci&oacute;n para consulta de tasas.
  */
  public static final String PC51="PC51";

  /**Tipo de operaci&oacute;n de deuda pendiente.
  */
  public static final String PD78="PD78";

  /**Tipo de operaci&oacute;n para disposici&oacute;n
  */
  public static final String PD15="PD15";

  /**Tipo de operaci&oacute;n para cobro de adeudo.
  */
  public static final String PD31="PD31";


  /**Tipo de operaci&oacute;n cotizaci&oacute;n de disposici&oacute;n
  */
  public static final String CECT="CECT";

  /**Tipo de operaci&oacute;n para avisos de cr&eacute;dito electr&oacute;nico.
  */
  public static final String CEAV="CEAV";

  /**Separador de campos
  */
  private String pipe;

  /**Sirve como separador de registros.
  */
  private String separadorRegistro;

  /**Sirve para definir el medio de entrega en todas las tramas.
  */
  private String medioEntrega;

   /**Caracter "blanco" o espacio.
   */
  private String blanco;

  /*
  private String contrato;
  private String empleado;
  private String perfil;
  private String medio;
  private String cuenta;
  private String lineaCredito;
  private String relacion;
  */


  /**
   * Constructor vac&iacute;o<BR>
   * Asigna el medio de entrega y los caracteres por defecto.
   */
  public TramasCE()
  {
    medioEntrega=MEDIOS_WEB[TRAMA];
    pipe=CONECTOR;
    separadorRegistro=ARROBA;
    blanco=ESPACIO;
  } // Fin contructor



  public String getMedioEntrega()
  {
    return medioEntrega;
  } // Fin getMedioEntrega


   /**<code><B><I>setMedioEntrega</I></B></code> cambia el medio por el cual tuxedo entega los<Br>
   * datos (archivo o trama).
   * @param medio Es la clave del medio.
   */
  public void setMedioEntrega(int medio)
  {
    if(medio<MEDIOS_WEB.length && medio>-1)
    {
      medioEntrega=MEDIOS_WEB[medio];
    }
    else
    {
      medioEntrega=MEDIOS_WEB[TRAMA];
    }
  } // Fin setMedioEntrega


   /**<code><B><I>setMedioEntrega</I></B></code> cambia el medio por el cual tuxedo entega los<Br>
   * datos (archivo o trama).
   * @param medio Cadena del medio con la que construye las tramas.
   */
  public void setMedioEntrega(String medio)
  {
    if(medio!=null && medio.length()>0)
    {
      medioEntrega=medio;
    }
    else
    {
      medioEntrega=MEDIOS_WEB[TRAMA];
    } // Fin if
  } // Fin setMedioEntrega


  /**<code><B><I>tramaConsultaSaldos</I></B></code> genera la cadena (trama) para efecuar la consulta<BR>
   * de consulta de saldos a trav&eacute;s de Tuxedo (<B><I>SDCT</I></B>).
   * @param empleado Atributo de sesi&oacute;n que corresponde a la clave del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param tipoOperacion Clave con la que se define esta operaci&oacute;n.
   * @param contrato Atributo de sesi&oacute;n que corresponde a la clave del contrato de la apliaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n que corresponde a la cerfil del usuario o empleado.
   * @param cuenta N&uacute;mero de cuenta a consultar.
   * @param relacion Tipo de relaci&oacute;n de la cuenta.
   */
  public String tramaConsultaSaldos(String empleado, String contrato,
  String perfil, String cuenta, String relacion)
  {
     return tramaConsultaSaldos(empleado, SDCT, contrato, perfil, cuenta, relacion);
  } // Fin m�todo tramaConsultaSaldos 5 par�metros


  /**<code><B><I>tramaConsultaSaldos</I></B></code> genera la cadena (trama) para efecuar la consulta<BR>
   * de consulta de saldos a trav&eacute;s de Tuxedo (<B><I>SDCT</I></B>).
   * @param empleado Atributo de sesi&oacute;n que corresponde a la clave del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param tipoOperacion Clave con la que se define esta operaci&oacute;n.
   * @param contrato Atributo de sesi&oacute;n que corresponde a la clave del contrato de la apliaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n que corresponde a la cerfil del usuario o empleado.
   * @param cuenta N&uacute;mero de cuenta a consultar.
   * @param relacion Tipo de relaci&oacute;n de la cuenta.
   */
  public String tramaConsultaSaldos(String empleado, String tipoOperacion,
  String contrato, String perfil, String cuenta, String relacion)
  {
    String trama=new String(medioEntrega);
    trama+=pipe;


    if(empleado==null || empleado.length()<1)
      empleado=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=SDCT;

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(cuenta==null || cuenta.length()<1)
      cuenta=" ";

    if(relacion==null || relacion.length()<1)
      relacion=PERSONAL;

      trama+=empleado;
      trama+=pipe;
      trama+=tipoOperacion;
      trama+=pipe;
      trama+=contrato;
      trama+=pipe;
      trama+=empleado;
      trama+=pipe;
      trama+=perfil;
      trama+=pipe;
      trama+=cuenta;
      trama+=pipe;
      trama+=relacion;

    return trama;
  } // Fin m�todo tramaConsultaSaldos 6 par�metros


  /**<code><B><I>tramaConsultaLineasCredito</I></B></code>
   * <P>Genera la cadena (trama) para efectuar la consulta de l&iacute;neas de<BR>
   * cr&eacute;dito a trav&eacute;s de Tuxedo (<B><I>PC46</I></B>).
   * </P>
   * @param empleado Atributo de sesi&oacute;n que corresponde a la clave del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param contrato Atributo de sesi&oacute;n que corresponde al contrato de la apliaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n que corresponde al perfil del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param cuenta N&uacute;mero de cuenta de la cual se quiere obtener sus l&iacute;neas de cr&eacute;dito.
   */
  public String tramaConsultaLineasCredito(String empleado, String contrato,
  String perfil, String cuenta)
  {
    return tramaConsultaLineasCredito(empleado, contrato, perfil, cuenta, " ");
  } // Fin m�todo tramaConsultaLineasCredito


  /**<code><B><I>tramaConsultaLineasCredito</I></B></code>
   * <P>Genera la cadena (trama) para efectuar la consulta de l&iacute;neas de<BR>
   * cr&eacute;dito a trav&eacute;s de Tuxedo (<B><I>PC46</I></B>).
   * </P>
   * @param empleado Atributo de sesi&oacute;n que corresponde a la clave del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param contrato Atributo de sesi&oacute;n que corresponde al contrato de la apliaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n que corresponde al perfil del usuario con que ingres&oacute; a la apliaci&oacute;n.
   * @param cuenta N&uacute;mero de cuenta de la cual se quiere obtener sus l&iacute;neas de cr&eacute;dito.
   */
  public String tramaConsultaLineasCredito(String empleado, String contrato,
  String perfil, String cuenta, String lineaCredito)
  {
    String trama=new String(medioEntrega);

    if(empleado==null || empleado.length()<1)
      empleado=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(cuenta==null || cuenta.length()<1)
      cuenta=" ";

    if(lineaCredito==null || lineaCredito.length()<1)
      lineaCredito=" ";

    trama+=pipe;
    trama+=empleado;
    trama+=pipe;
    trama+=CEDC;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=empleado;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=contrato;
    trama+=separadorRegistro;
    trama+=cuenta;
    trama+=separadorRegistro;
    trama+=lineaCredito;
    trama+=separadorRegistro;
    trama+=empleado;
    trama+=pipe;

    return trama;
  } // Fin tramaConsultaLineaCredito


  /**<code><B><I>consultaPosicionLinea</I></B></code> conatruye la trama para consultas de posici&oacute;n<BR>
  * de l&iacute;neas de cr&eacute;dito electr&oacute;nico <B>PC49</B>.
  * <P>Regresa la cadena de consulta para disponer del servicio de tuxedo.
  * </P>
  * @param empleado Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
  * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
  * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
  * @param lineaCredito N&uacute;mero de l&iacute;nea de cr&eacute;dito de la cual se consulta.
  */
  public String consultaPosicionLinea(String empleado, String contrato,
  String perfil, String lineaCredito)
  {
    return consultaPosicionLinea(empleado, PC49, contrato, perfil, lineaCredito);
  } // Fin m�todo consultaPosicionLinea 4 par�metros


  /**<code><B><I>consultaPosicionLinea</I></B></code> conatruye la trama para consultas de posici&oacute;n<BR>
  * de l&iacute;neas de cr&eacute;dito electr&oacute;nico <B>PC49</B>.
  * <P>Regresa la cadena de consulta para disponer del servicio de tuxedo.
  * </P>
  * @param empleado Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
  * @param tipoOperacion Define el tipo de consulta que se solicita a tuxedo.
  * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
  * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
  * @param lineaCredito N&uacute;mero de l&iacute;nea de cr&eacute;dito de la cual se consulta.
  */
  public String consultaPosicionLinea(String empleado, String tipoOperacion,
  String contrato, String perfil, String lineaCredito)
  {
    String trama=new String(medioEntrega);
    trama+=pipe;

    if(empleado==null || empleado.length()<1)
      empleado=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(lineaCredito==null || lineaCredito.length()<1)
      lineaCredito=" ";


    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=PC49;

    trama+=empleado;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=empleado;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=lineaCredito;
    trama+=pipe;

    return trama;
  } // Fin m�todo consultaPosicionLinea 5 par�metros


   /**<code><B><I>tramaConsultaTasas</I></B></code> construye la trama de consulta de tasas <B>PC51</B>.
   * <P> Regresa la trama de consulta de este servicio de tuxedo como una cadena.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito con la que se solicita este servicio.
   */
  public String tramaConsultaTasas(String usuario, String contrato, String perfil, String lineaCredito)
  {
    return tramaConsultaTasas(usuario, PC51, contrato, perfil, lineaCredito);
  } // Fin m�todo tramaConsultaTasas cuatro par�metros


   /**<code><B><I>tramaConsultaTasas</I></B></code> construye la trama de consulta de tasas <B>PC51</B>.
   * <P> Regresa la trama de consulta de este servicio de tuxedo como una cadena.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Cadena que identifica al servicio de tuxedo.
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito con la que se solicita este servicio.
   */
  public String tramaConsultaTasas(String usuario, String tipoOperacion, String contrato, String perfil, String lineaCredito)
  {
    String trama =new String(medioEntrega);

    if(usuario==null || usuario.length()<1)
      usuario=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=PC51;

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(lineaCredito==null || lineaCredito.length()<1)
      lineaCredito=" ";

    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=usuario;
    trama+=separadorRegistro;
    trama+=lineaCredito;
    trama+=pipe;

    return trama;
  } // Fin m�todo tramaConsultaTasas cinco par�metros


   /**<code><B><I>tramaConsultaDeudaPendiente</I></B></code> construye la trama de consulta de deuda pendiente <B>PD78</B>.<BR>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param tipoOperacion Cadena con la que se identifica al servicio de tuxedo.
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n.
   * @param numeroDisposicion Clave con la que se identifica a la disposci&oacute;n  que se consulta.
   */
  public String tramaConsultaDeudaPendiente(String usuario, String contrato,
  String tipoOperacion, String perfil, String numeroDisposicion)
  {
    String trama=new String(medioEntrega);

    if(usuario==null || usuario.length()<1)
      usuario=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(numeroDisposicion==null || numeroDisposicion.length()<1)
      numeroDisposicion=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=PD78;

    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=numeroDisposicion;
    trama+=pipe;

    return trama;
  } // Fin m�todo tramaConsultaDeudasPendientes 5 par�metros


   /**<code><B><I>tramaConsultaDeudaPendiente</I></B></code> construye la trama de consulta de deuda pendiente <B>PD78</B>.<BR>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n.
   * @param numeroDisposicion Clave con la que se identifica a la disposci&oacute;n  que se consulta.
   */
  public String tramaConsultaDeudaPendiente(String usuario, String contrato,
  String perfil, String numeroDisposicion)
  {
    return tramaConsultaDeudaPendiente(usuario, contrato, PD78, perfil, numeroDisposicion);
  } //Fin tramaConsultaDeudaPendiente


   /**<code><B><I>tramaConsultaDisposicion</I></B></code> construye la trama para efecuar la consulta<BR>
   * de disposici&oacute;n <B>PD15</B> a tuxedo.
   * <P>Regresa una cadena con la trama para efectuar la disposici&oacute;n
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Cadena que identifica a cada servicio de tuxedo.
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito de la cual se consulta.
   * @param importe Monto de la disposici&oacute;n
   * @param plazo N&uacute;mero de d&iacute;as de la disposici&oacute;n antes del vencimiento de la misma.
   * @param tasa Tasa de inter&eacute;s de la disposici&oacute;n
   * @param origenTransaccion Define el origen de la cuenta de la disposici&oacute;n.
   */
  public String tramaConsultaDisposicion(String usuario, String tipoOperacion, String contrato, String perfil, String lineaCredito, String importe, String plazo, String tasa, String origenTransaccion)
  {
    String trama=new String(medioEntrega);
    FormatosMoneda fm=new FormatosMoneda();

    if(usuario==null || usuario.length()<1)
      usuario=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=PD15;

    if(lineaCredito==null || lineaCredito.length()<1)
      lineaCredito=" ";

    if(importe==null || importe.length()<1)
      importe=" ";

    if(plazo==null || plazo.length()<1)
      plazo=" ";

    if(tasa==null || tasa.length()<1)
      tasa=" ";

    if(origenTransaccion==null || origenTransaccion.length()<1)
      origenTransaccion="P";

    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=contrato;
    trama+=separadorRegistro;
    trama+=lineaCredito;
    trama+=separadorRegistro;
    trama+=importe;
    trama+=separadorRegistro;
    trama+=plazo;
    trama+=separadorRegistro;
    trama+=quitaPunto(fm.daNuevaTasaFormateada(tasa));
    trama+=separadorRegistro;
    trama+=origenTransaccion;
    trama+=pipe;


    return trama;
  } // Fin de tramaConsultaDisposicion


   /**<code><B><I>tramaConsultaDisposicion</I></B></code> construye la trama para efecuar la consulta<BR>
   * de disposici&oacute;n <B>PD15</B> a tuxedo.
   * <P>Regresa una cadena con la trama para efectuar la disposici&oacute;n
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito de la cual se consulta.
   * @param importe Monto de la disposici&oacute;n
   * @param plazo N&uacute;mero de d&iacute;as de la disposici&oacute;n antes del vencimiento de la misma.
   * @param tasa Tasa de inter&eacute;s de la disposici&oacute;n
   * @param origenTransaccion Define el origen de la cuenta de la disposici&oacute;n.
   */

  public String tramaConsultaDisposicion(String usuario, String contrato, String perfil, String lineaCredito, String importe, String plazo, String tasa, String origenTransaccion)
  {
    return tramaConsultaDisposicion(usuario, PD15, contrato, perfil, lineaCredito,
    importe, plazo, tasa, origenTransaccion);
  } // Fin tramaConsultaDisposicion



   /**<code><B><I>tramaConsultaDisposicion</I></B></code> construye la trama para efecuar la consulta<BR>
   * de disposici&oacute;n <B>PD15</B> a tuxedo.
   * <P>Regresa una cadena con la trama para efectuar la disposici&oacute;n
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Cadena que identifica a cada servicio de tuxedo.
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito de la cual se consulta.
   * @param importe Monto de la disposici&oacute;n
   * @param plazo N&uacute;mero de d&iacute;as de la disposici&oacute;n antes del vencimiento de la misma.
   * @param origenTransaccion Define el origen de la cuenta de la disposici&oacute;n.
   */

  public String tramaConsultaDisposicion(String usuario, String contrato, String perfil, String lineaCredito, String importe, String plazo, String tasa)
  {
    return tramaConsultaDisposicion(usuario, PD15, contrato, perfil,
    lineaCredito, importe, plazo, tasa, "P");
  } // Fin tramaConsultaDisposicion


   /**<code><B><I>consultaCobroAdeudos</I></B></code> arma la consulta de cobro de adeudos <B>PC31</B>.<BR>
   * <P>Regresa una cadena con la trama de consulta.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Define la trama de consulta a tuxedo.
   * @param contrato Atributo de sesi&oacute;n de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n de la aplicaci&oacute;n
   * @param disposicion dato de la disposic&oacute;n
   */
  public String consultaCobroAdeudos(String usuario, String tipoOperacion, String contrato, String perfil, String disposicion)
  {
    String trama=new String(medioEntrega);

    if(usuario==null || usuario.length()<1)
      usuario=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=PD31;

    if(disposicion==null || disposicion.length()<1)
      disposicion=" ";

    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=disposicion;
    trama+=pipe;


    return trama;
  } // Fin consultaCobroAdeudos

   /**<code><B><I>consultaCobroAdeudos</I></B></code> es el m&eacute;todo de cuatro par&aacute;metros.<BR>
   * <P>Llama al m&eacute;todo de cinco par&aacute;metros<BR>
   * Regresa una cadena con la trama de consulta.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Define la trama de consulta a tuxedo.
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n.
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param disposicion dato de la disposic&oacute;n
   */
    public String consultaCobroAdeudos(String usuario, String contrato, String perfil, String disposicion)
    {
        return consultaCobroAdeudos(usuario, PD31, contrato, perfil, disposicion);
    } // Fin m�todo consuotaCobroAdeudos


   /**<code><B><I>consultaCotizacionDisposicion</I></B></code> construye las tramas de cotizaci&oacute;n de diposici&oacute;n<BR>
   * correspondientes al m&oacute;dulo de <B>Disposici&oacute;n Vista</B>
   * <P>El tipo de operaci�n es la <B>CECT</B>, pero la funci&oacute;n equivalente en el m&oacute;dulo<BR>
   * <I>modCreditoElectronico</I> de Visual Basic se llama <I>ArmaTramaPC47</I>. Hay m&oacute;dulos y funciones en Visual Basic<BR>
   * con nombres parecidos, pero el usuado es el mencionado anteriormente.<BR><BR>
   * Regresa la trama de consulta como una cadena.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param tipoOperacion Identifica al tipo de servicio que se solicita a tuxedo.
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param cuentaCheques N&uacute;mero de la cuenta de cheques.
   * @param lineaCredito N&uacute;mero de la l&iacute;mero de cr&eacute;dito.
   * @param importe Monto de la cotizaci&oacute;n.
   * @param plazo N&uacute;mero de d&iacute;as de plazo de la disposici&oacute;n antes del vencimiento.
   * @param concepto Descripci&oacute;n de la cotizaci&oacute;n
   */
  public String consultaCotizacionDisposicion(String usuario, String tipoOperacion,
      String contrato, String perfil, String cuentaCheques, String lineaCredito,
      String importe, String plazo, String concepto)
  {
    String trama=new String(medioEntrega);

    if(usuario==null || usuario.length()<1)
      usuario=" ";

    if(contrato==null || contrato.length()<1)
      contrato=" ";

    if(perfil==null || perfil.length()<1)
      perfil=" ";

    if(cuentaCheques==null || cuentaCheques.length()<1)
      cuentaCheques=" ";

    if(lineaCredito==null || lineaCredito.length()<1)
      lineaCredito=" ";

    if(tipoOperacion==null || tipoOperacion.length()<1)
      tipoOperacion=CECT;

    if(importe==null || importe.length()<1)
      importe=" ";

    if(plazo==null || plazo.length()<1)
      plazo=" ";

    if(concepto==null)
      concepto="";

    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=tipoOperacion;
    trama+=pipe;
    trama+=contrato;
    trama+=pipe;
    trama+=usuario;
    trama+=pipe;
    trama+=perfil;
    trama+=pipe;
    trama+=contrato;
    trama+=separadorRegistro;
    trama+=cuentaCheques;
    trama+=separadorRegistro;
    trama+=lineaCredito;
    trama+=separadorRegistro;
    trama+=importe;
    trama+=separadorRegistro;
    trama+=plazo;
    trama+=separadorRegistro;
    trama+=concepto;
    trama+=pipe;

    return trama;
    } // Fin m�todo consultaCotizacionDisposicion


   /**<code><B><I>consultaCotizacionDisposicion</I></B></code> arma la trama para cotizar una disposici&oacute;n
   * <P>Regresa la trama de consulta como cadena.
   * </P>
   * @param usuario Atributo de sesi&oacute;n con la que ingresa el usuario de la aplicaci&oacute;n
   * @param contrato Atributo de sesi&oacute;n del contrato de la aplicaci&oacute;n
   * @param perfil Atributo de sesi&oacute;n del perfil del usuario de la aplicaci&oacute;n
   * @param cuentaCheques N&uacute;mero de la chequera.
   * @param lineaCredito N&uacute;mero de la l&iacute;nea de cr&eacute;dito asociada a la cuenta de cheques.
   * @param importe Cantidad monetaria de la disposici&oacute;n
   * @param plazo N&uacute;mero de d&iacute;as de la cotizaci&oacute;n
   * @param concepto de la disposici&oacute;n
   */
  public String consultaCotizacionDisposicion(String usuario,
      String contrato, String perfil, String cuentaCheques, String lineaCredito,
      String importe, String plazo, String concepto)
  {
    return consultaCotizacionDisposicion(usuario, CECT, contrato,perfil, cuentaCheques, lineaCredito, importe, plazo, concepto);
  } // Fin consulta

   /**<B><I><code>consultaAvisosCE</code></I></B> construye la trama para saber si hay disposiciones vencidas.
   * <P>Regresa una cadena no nula ni vac&iacute;a, con la trama de consulta a este servicio de tuxedo.
   * </P>
   * @param tipoOperacion <B>String</B> Indica c&oacute;mo se llama el servicio de tuxedo.
   * @param contrato <B>String</B> N&uacute;mero de contrato.
   * @param usuario <B>String</B> Usuario con que ingres&oacute; a la aplicaci&oacute;n.
   * @param perfil <B>String</B> Perfil del usuario.
   */
   public String consultaAvisosCE(String tipoOperacion, String contrato, String usuario, String perfil)
   {
      String tramaConsulta=new String(medioEntrega);

      if(tipoOperacion==null || tipoOperacion.length()<1)
         tipoOperacion=CEAV;

      if(contrato==null)
         contrato="";

      if(usuario==null)
         usuario="";

      if(perfil==null)
         perfil="";

      tramaConsulta+=pipe;
      tramaConsulta+=usuario;
      tramaConsulta+=pipe;
      tramaConsulta+=tipoOperacion;
      tramaConsulta+=pipe;
      tramaConsulta+=contrato;
      tramaConsulta+=pipe;
      tramaConsulta+=usuario;
      tramaConsulta+=pipe;
      tramaConsulta+=perfil;
      tramaConsulta+=pipe;
      tramaConsulta+=contrato;
      tramaConsulta+=separadorRegistro;
      tramaConsulta+=usuario;
      tramaConsulta+=pipe;

      return tramaConsulta;
   } // Fin m�todo consultaAvisosCE


   /**<B><I><code>consultaAvisosCE</code></I></B> construye la trama para saber si hay disposiciones vencidas.
   * <P>Regresa una cadena no nula ni vac&iacute;a, con la trama de consulta a este servicio de tuxedo.
   * </P>
   * @param contrato <B>String</B> N&uacute;mero de contrato.
   * @param usuario <B>String</B> Usuario con que ingres&oacute; a la aplicaci&oacute;n.
   * @param perfil <B>String</B> Perfil del usuario.
   */
   public String consultaAvisosCE(String contrato, String usuario, String perfil)
   {
      return consultaAvisosCE(CEAV, contrato, usuario, perfil);
   } // Fin m�todo consultaAvisosCE

   /**<code><B><I>verificaConcatena</I></B></code> une todos los elementos de un arreglo de cadenas en una<BR>
   * sola cadena, separandos por un caracter.
   * @param datos Arreglo de cadenas.
   */
  public String verificaConcatena(String[] datos)
  {
    String cadenaDatos=new String("");

    for(int i=0;i<datos.length;i++)
    {
      if(datos[i]==null || datos[i].length()<1)
        {
          datos[i]=blanco;
        } // Fin if

        cadenaDatos+=pipe;
        cadenaDatos+=datos[i];

    } // Fin for i

    return cadenaDatos;
  } // Fin verificaDatos

  public String getPipe()
  {
    return pipe;
  } // Fin m�todo getPipe


   /**<code><B><I>setPipe</I></B></code> indica qu&eacute; caracter debe usarse como separador de campos.
   * @param caracter Nuevo separador.
   */
  public void setPipe(String caracter)
  {
    if(caracter!=null && caracter.length()>0)
    {
      pipe=caracter;
    } // Fin if
  } // Fin m�todo setPipe


  public String getSeparadorRegistro()
  {
    return separadorRegistro;
  } // Fin m�todo separador registro


   /**<code><B><I>setSeparadorRegistro</I></B></code> indica cual debe ser el nuevo caracter que<BR>
   * debe ser usado como separador de registros, tal como la arroba.
   * @param sr Cadena no vac&iacute;a
   */
  public void setSeparadorRegistro(String sr)
  {
    if(sr!=null && sr.length()>0)
    {
      separadorRegistro=sr;
    } // Fin if
  } // Fin m�todo getSeparadorRegistro


  public String getBlanco()
  {
    return blanco;
  } // Fin m�todo getBlanco


   /**<code><B><I>setBlanco</I></B></code> permite cambiar el caracter usado como blanco o espacio.
   * @param caracter Caracter o cadena a usar como espacio.
   */
  public void setBlanco(String caracter)
  {
    if(caracter!=null && caracter.length()>0)
      blanco=caracter;
  } // Fin m�todo setBlanco


   /**<code><B><I>quitaPunto</I></B></code> retira todos los caracteres "." de una cadena.
   * <P>Regresa una nueva cadena sin los puntos.
   * </P>
   * @param cadenaNumero Es una cadena a la cual se le quita el punto decimal para construir una trama de consulta para tuxedo.
   */
  public String quitaPunto(String cadenaNumero)
  {
    StringBuffer sb=new StringBuffer(cadenaNumero);
    char c;

    for(int i=0;i<sb.length();i++)
    {
        c=sb.charAt(i);

        if(c=='.')
          sb.deleteCharAt(i);
    } // Fin for

    return new String(sb);
  } // Fin cadenaNumero

} // Fin clase TramasCE

