package mx.altec.enlace.bo;

/**
 *  Clase bean para manejar el procedimiento para la transaccion GP93
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 22, 2011
 */
public class TrxGP93VO {
	
	/**
	 * Clave de la divisa o del pais
	 */
	private String OVarCod = null;
	
	/**
	 * Descripción de la divisa o del pais
	 */
	private String OVarDes = null;
	
	/**
	 * Obtener el valor de OVarCod.
     *
     * @return OVarCod valor asignado.
	 */
	public String getOVarCod() {
		return OVarCod;
	}
	
	/**
     * Asignar un valor a OVarCod.
     *
     * @param OVarCod Valor a asignar.
     */
	public void setOVarCod(String OVarCod) {
		this.OVarCod = OVarCod;
	}

	/**
	 * Obtener el valor de OVarDes.
     *
     * @return OVarDes valor asignado.
	 */
	public String getOVarDes() {
		return OVarDes;
	}

	/**
     * Asignar un valor a OVarDes.
     *
     * @param OVarDes Valor a asignar.
     */
	public void setOVarDes(String OVarDes) {
		this.OVarDes = OVarDes;
	}	
	
	/**
	 * Override toString
	 */
	public String toString() {
		StringBuffer registro = new StringBuffer();		
		registro.append("OVarCod->" + getOVarCod());
		registro.append("OVarDes->" + getOVarDes());
		return registro.toString();
	}
	
}
