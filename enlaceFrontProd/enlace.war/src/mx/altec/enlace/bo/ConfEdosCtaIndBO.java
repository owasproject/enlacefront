package mx.altec.enlace.bo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.dao.ConfEdosCtaIndDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import org.apache.commons.lang.StringUtils;

/**
 * @author Stefanini
 *
 */
public class ConfEdosCtaIndBO {

	/** Constante codigo para PDF **/
	public static final String FORMATO_PDF = "1";
	/** Instancia de clase del DAO **/
	transient ConfEdosCtaIndDAO confEdosCtaIndDAO = new ConfEdosCtaIndDAO();

	/**
	 * Metodo para cosultar el estatus de la cuenta para edo de cta PDF
	 * @param cuenta La cuenta seleccionada
	 * @return ConfEdosCtaIndBean Bean de la informacion
	 */
	public ConfEdosCtaIndBean mostrarCuentasPDF(String cuenta) {
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		confEdosCtaIndBean = confEdosCtaIndDAO.ejecutaODD4(cuenta);
		EIGlobal.mensajePorTrace("confEdosCtaIndDAO.ejecutaODD4("+cuenta+").getCodRetorno() = ["+confEdosCtaIndBean.getCodRetorno()+"]",
				EIGlobal.NivelLog.DEBUG);
		if("ODA0002".equals(confEdosCtaIndBean.getCodRetorno()) ||
				("ODE0065".equals(confEdosCtaIndBean.getCodRetorno()) && confEdosCtaIndBean.getCuentas().size() > 0)) {
			confEdosCtaIndBean.setDomCompleto(confEdosCtaIndBean.getCalle().trim().concat(" #").concat(confEdosCtaIndBean.getNoExterior().trim())
					.concat("-").concat(confEdosCtaIndBean.getNoInterior().trim()).concat(" COL. ").concat(confEdosCtaIndBean.getColonia().trim())
					.concat(" ").concat(confEdosCtaIndBean.getDelegMunic().trim()).concat(", ").concat(confEdosCtaIndBean.getEstado().trim()));
			confEdosCtaIndBean.setPeriodoDisp("");
			confEdosCtaIndBean.setFechaConfig(confEdosCtaIndDAO.consultaConfigPreviaSeqDom(confEdosCtaIndBean.getSecDomicilio(),
					confEdosCtaIndBean.getCodCliente()));

			if ("ODE0065".equals(confEdosCtaIndBean.getCodRetorno())) {
				confEdosCtaIndBean.setTieneProblemaDatos(true);
				confEdosCtaIndBean.setMensajeProblemaDatos("Por incongruencia de datos en la secuencia de domicilio no fue posible obtener el total de cuentas");
			}
		}
		return confEdosCtaIndBean;
	}

	/**
	 *
	 * @param suscripPaperless Alta o Baja de Paperless
	 * @param seqDomicilio Secuencia de Domicilio
	 * @param cuenta Cuenta
	 * @param codCliente Codigo del Cliente
	 * @param hdEdoCtaDisponible Disponibilidad del Estado de Cuenta
	 * @param hdSuscripPaperless Suscripcion de Paperless
	 * @param contrato Contrato
	 * @return Bean con la informacion
	 */
	public ConfEdosCtaIndBean configuraPDF(String suscripPaperless,
			String seqDomicilio, String cuenta, String codCliente, String codClientePersonas, String hdEdoCtaDisponible,
			String hdSuscripPaperless, String contrato) {
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		String mensaje = "Error";
		suscripPaperless = suscripPaperless!=null ? suscripPaperless : "N";
		confEdosCtaIndBean.setCodCliente(codCliente);
		confEdosCtaIndBean.setCodClientePersonas(codClientePersonas);
		confEdosCtaIndBean.setCuenta(cuenta);
		confEdosCtaIndBean.setSecDomicilio(seqDomicilio);
		EIGlobal.mensajePorTrace("suscripPaperless->" + suscripPaperless, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("hdSuscripPaperless->" + hdSuscripPaperless, EIGlobal.NivelLog.INFO);
		if(!StringUtils.equals(suscripPaperless, hdSuscripPaperless) ) {
			confEdosCtaIndBean.setFolioOp(String.valueOf(confEdosCtaIndDAO.consultaFolioSeq()));
			confEdosCtaIndBean = confEdosCtaIndDAO.ejecutaODB2(confEdosCtaIndBean, suscripPaperless);
		}
		mensaje = confEdosCtaIndBean.getDescRetorno();
		if("S".equals(suscripPaperless)) {
			confEdosCtaIndBean.setSuscripPaperless(true);
		}else {
			suscripPaperless = "N";
		}
		// Si no hubo error en la respuesta recibida de la transaccion continua flujo normal.
		if(confEdosCtaIndBean.getCodRetorno()==null || confEdosCtaIndBean.getCodRetorno().contains("ODA") ||
				"OK".equals(confEdosCtaIndBean.getCodRetorno())) {
			EIGlobal.mensajePorTrace("suscripPaperless->" + suscripPaperless, EIGlobal.NivelLog.INFO);
			confEdosCtaIndBean.setContrato(contrato);
			confEdosCtaIndBean.setFolioOp(confEdosCtaIndBean.getCodRetorno()==null ? String.valueOf(confEdosCtaIndDAO.consultaFolioSeq())
					: confEdosCtaIndBean.getFolioOp());
			confEdosCtaIndDAO.insertaConfiguracion(confEdosCtaIndBean);
			confEdosCtaIndDAO.insertaDetalle(confEdosCtaIndBean, "", suscripPaperless);
		} else {
			// En caso de error se termina metodo para retornar control al
			// servlet e informar el error al cliente.
			return confEdosCtaIndBean;
		}
		confEdosCtaIndBean.setDescRetorno(mensaje);
		return confEdosCtaIndBean;
	}

	/**
	 * Metodo para consultar la disponibilidad de edo de cta para nueva cuenta
	 * @param cuentaNva Cuenta nueva a consultar
	 * @param cuenta Cuenta previa
	 * @param codCliente Codigo del cliente
	 * @param listCuentas Lista de cuentas pertenecientes a la secuencia
	 * @return ConfEdosCtaIndBean Bean de regreso
	 */
	public ConfEdosCtaIndBean consultarNuevaCta(String cuentaNva, String cuenta, String codCliente, List<String> listCuentas) {
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();

		if (StringUtils.equals(cuentaNva, cuenta)) {
			confEdosCtaIndBean.setCodRetorno("ERROR");
			confEdosCtaIndBean.setDescRetorno("Favor de introducir un numero de cuenta distinto para proceder con la configuracion");
		}else if(listCuentas!=null && !listCuentas.isEmpty()) {
			for(String cuentaAux : listCuentas) {
				if(StringUtils.equals(cuentaNva, cuentaAux)) {
					EIGlobal.mensajePorTrace("Petenece a la secuencia ->" + cuentaNva, EIGlobal.NivelLog.INFO);
					confEdosCtaIndBean.setPeriodoDisp("");
					confEdosCtaIndBean.setCodCliente(codCliente);
					/*confEdosCtaIndBean = confEdosCtaIndDAO.ejecutaDY01(confEdosCtaIndBean, "C", cuentaNva, FORMATO_PDF);*/
					/** break; **/
				}else {
					confEdosCtaIndBean.setCodRetorno("ERROR");
					confEdosCtaIndBean.setDescRetorno("El numero de cuenta debe formar parte de las cuentas relacionadas a la secuencia " +
							"de domicilio. Para consultar estas cuentas de clic en el link del numero de secuencia de la pantalla");
				}
			}
		}else {
			confEdosCtaIndBean.setCodRetorno("ERROR");
			confEdosCtaIndBean.setDescRetorno("El numero de cuenta debe formar parte de las cuentas relacionadas a la secuencia " +
				"de domicilio. Para consultar estas cuentas de clic en el link del numero de secuencia de la pantalla");
		}
		return confEdosCtaIndBean;
	}

	/**
	 * Metodo para consultar si hay una configuracion previa de PDF
	 * @param seqDomicilio Secuencia de domicilio
         * @param codCliente Codigo del Cliente
	 * @return String Mensaje a retornar
	 */
	public String consultaConfigPreviaPDF(String seqDomicilio,String codCliente) {
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		final SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy", Locale.US);
		final String fechaHoy = sFormat.format(new Date());
		String fechaConfig = "";
		String mensaje = "";
		confEdosCtaIndBean = confEdosCtaIndDAO.consultaConfigPreviaEdoCta(seqDomicilio,codCliente);
		if(confEdosCtaIndBean.getFechaConfig() != null) {
			fechaConfig = sFormat.format(confEdosCtaIndBean.getFechaConfig());
		}
		if(StringUtils.equals(fechaHoy, fechaConfig)) {
			mensaje = "La cuenta ya ha sido parametrizada el d&iacute;a de hoy con el folio ".concat(confEdosCtaIndBean.getFolioOp())
				.concat(" y contrato ").concat(confEdosCtaIndBean.getContrato()).concat(".");
		}
		return mensaje;
	}

}