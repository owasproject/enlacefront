/**
 * 
 */
package mx.altec.enlace.bo;

import mx.altec.enlace.beans.TrxOD13Bean;
import mx.altec.enlace.dao.TrxOD13DAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase Bussines para manejar el procedimiento para la transaccion OD13
 * @author Armando Montoya Hernandez
 * @since 29/07/2014
 *
 */
public class TrxOD13BO {
	
	/**Objeto contenedor de tipo TrxOD13Bean*/
	private TrxOD13Bean bean;
	
	/**
	 * @return the bean
	 */
	public TrxOD13Bean getBean() {
		return bean;
	}

	/**
	 * @param bean the bean to set
	 */
	public void setBean(TrxOD13Bean bean) {
		this.bean = bean;
	}

	/**
	 * Verifica que exista una relaci&oacute;n del n&uacute;mero
	 * m&oacute;vil tenga relaci&oacute;n en la tabla de Personas,
	 * el m&eacute;todo s&oacute;lo deber&iacute;a ser aplicado
	 * para las Cuentas de N&uacute;meros M&oacute;viles del Mismo
	 * Banco (Santander). 
	 * 
	 * @param numeroMovil 	El n&uacute;mero m&oacute;vil que se 
	 * 						pretende verificar.
	 * @return 				<i>true</i> en caso de que se haya encontrado la 
	 * 						cuenta, <i>false</i> en caso contrario.
	 * */
	public boolean validateNumeroMovil(String numeroMovil){
		EIGlobal.mensajePorTrace("TrxOD13BO::validateNumeroMovil:: Entrando....", EIGlobal.NivelLog.DEBUG);
		boolean returnValue 		= false;
		bean = new TrxOD13DAO().ejecutaConsulta(numeroMovil);
		if(null == bean){
			EIGlobal.mensajePorTrace("TrxOD13BO::validateNumeroMovil:: No se devolvio un bean>>", EIGlobal.NivelLog.ERROR);
		}
		else{
			/**Invertir el valor relativo al error, ya que el metodo deberá
			 * devolver <i>true</i> en caso que no haya errores.*/
			returnValue = bean.isError()^true;
			EIGlobal.mensajePorTrace("TrxOD13BO::validateNumeroMovil:: Verificando validez de la cuenta >>["+(bean.isError()^true)+"]", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("TrxOD13BO::validateNumeroMovil:: Mensaje del bean >>["+bean.getErrorMessage()+"]", EIGlobal.NivelLog.DEBUG);
		}
		EIGlobal.mensajePorTrace("TrxOD13BO::validateNumeroMovil:: Saliendo....", EIGlobal.NivelLog.DEBUG);
		return returnValue;
	}
}
