/*
Encabezado
Tipo de registro|Clave Emisora|Cod. Operacion|Fecha de Presentación|    |Num. Registros|Importe Total     |Uso Futuro|
0                99001            60           20040312              01       000149     000000004483802000
Registro
Tipo de registro|Num. Secuencia|Fecha de Aplicación|Cod. Operación|Banco|Importe Aplicar|CTA. Tesofe|Folio               |Num. Orden Inicial1|Num. Orden Inicial1|Beneficiario                  |RFC          |Uso Futuro|
1                000001               20040314         60          40014 000000000377700 65500032917 00000000000000000000 3221000024                              CAZARES CAZARES BLANCA CECILIA CACB770103U19
099001602004031301000149000000004483802000
100000120030315604001400000000037770065500032917000000000000000000003221000024                    CAZARES CAZARES BLANCA CECILIA                                                  CACB770103U19
*/
package mx.altec.enlace.bo;

import java.io.*;
import java.lang.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class MTE_Registro
{
  public int tipoRegistro;
  public int numeroSecuencia;
  public GregorianCalendar fechaAplicacion;
  public int codigoOperacion;
  public int banco;
  public double importeAplicar;
  public String cuentaTesofe;
  public long folio;
  public long referencia;
  public long numeroOrdenFin;
  public String beneficiario;
  public String RFC;
  public String usoFuturo;

  public int err;
  public Vector listaErrores=new Vector();

  public EI_Formato val=new EI_Formato();

  public void MTE_Registro()
	{
	  //val=new EI_Formato();
	  tipoRegistro=0;
	  numeroSecuencia=0;
	  fechaAplicacion=new GregorianCalendar();
	  codigoOperacion=0;
	  banco=0;
	  importeAplicar=0.0;
	  cuentaTesofe="";
	  folio=0;
	  referencia=0;
	  numeroOrdenFin=0;
	  beneficiario="";
	  RFC="";
	  usoFuturo="";

	  err=0;
	}

  public int parse(String linea)
	{
	    log("MTE_Registro - parse(): Separando datos ...", EIGlobal.NivelLog.INFO);

		if(linea.length()>=191 && linea.length()<=215)
		 {
		   setTipoRegistro(linea.substring(0,1).trim());
		   setNumeroSecuencia(linea.substring(1,7).trim());
		   setFechaAplicacion(linea.substring(7,15).trim());
		   setCodigoOperacion(linea.substring(15,17).trim());
		   setBanco(linea.substring(17,22).trim());

		   setImporteAplicar(linea.substring(22,37).trim());

		   setCuentaTesofe(linea.substring(37,48).trim());
		   setFolio(linea.substring(48,68).trim());
		   setReferencia(linea.substring(68,83).trim());
		   setNumeroOrdenFin(linea.substring(83,98).trim());
		   setBeneficiario(linea.substring(98,178).trim());
		   if(linea.length()>191)
			{
		      setRFC(linea.substring(178,191).trim());
		      setUsoFuturo(linea.substring(191).trim());
			}
		   else
			setRFC(linea.substring(178).trim());
		 }
		else
		 {
		   log("MTE_Registro - parse(): Error en el formato de la linea.", EIGlobal.NivelLog.INFO);
		   listaErrores.add("Longitud de linea erronea: "+ linea.length());
		   err=1;
		 }
		return err;
	}

  public GregorianCalendar getFechaString(String fecha)
	{
	  //log("MTE_Header - getFechaString(): Fecha: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==8)
			{
			  int Anio = Integer.parseInt(fecha.substring(0,4));
			  int Mes = Integer.parseInt(fecha.substring(4,6))-1;
			  int Dia = Integer.parseInt(fecha.substring(6));
			  return new GregorianCalendar(Anio, Mes, Dia);
			}
		  else
			{
			  log("MTE_Registro - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Longitud de fecha de aplicacion erronea: "+ fecha);
			  err+=1;
			}

		} catch (Exception e)
		 {
			log("MTE_Registro - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de fecha de aplicacion erroneo: "+ fecha);
			err+=1;
	     }
	   return null;
     }

  public GregorianCalendar getFechaStringDiagonal (String fecha)
	{
	  //log("MTE_Registro - getFechaString(): Fecha: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==10)
			{
			  if( fecha.charAt(2)!='/' || fecha.charAt(5)!='/')
				{
				  log("MTE_Registro - getFechaString(): Error en el formato de la fecha.", EIGlobal.NivelLog.INFO);
				  listaErrores.add("Formato de fecha de aplicacion erroneo: "+ fecha);
				  err+=1;
				}
			  else
				{
				  int Dia = Integer.parseInt (fecha.substring(0,2));
				  int Mes = Integer.parseInt (fecha.substring(3,5))-1;
				  int Anio = Integer.parseInt (fecha.substring(6));
				  //log("MTE_Registro - getFechaString(): Fecha: " + Dia + "-" + Mes + "-" + Anio, EIGlobal.NivelLog.INFO);
				  return new GregorianCalendar (Anio, Mes, Dia);
				}
			}
		  else
			{
			  log("MTE_Registro - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Longitud de fecha de aplicacion erronea: "+ fecha);
			  err+=1;
			}

		} catch (Exception e)
		 {
			log("MTE_Registro - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de fecha de aplicacion erroneo: "+ fecha);
			err+=1;
	     }
	   return null;
     }

  public void setTipoRegistro(String cad)
	{
	   try
		{
	      tipoRegistro = new Integer(cad).intValue();
		  if(tipoRegistro!=1)
			{
			  log("MTE_Registro - setTipoRegistro(): Error en el tipo de registro.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Tipo de Registro incorrecto: "+ cad);
			  err+=1;
			}
		}catch(Exception e)
		 {
			log("MTE_Registro - setTipoRegistro(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de tipo de Registro erroneo: "+ cad);
			err+=1;
		 }
	}

  public void setNumeroSecuencia(String cad)
	{
	  try
		{
	      numeroSecuencia = new Integer(cad).intValue();
		}catch(Exception e)
		 {
			log("MTE_Registro - setClaveEmisora(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de numero de secuencia erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setCodigoOperacion(String cad)
	{
	  try
		{
	      codigoOperacion = new Integer(cad).intValue();
		}catch(Exception e)
		 {
			log("MTE_Registro - setCodigoOperacion(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de codigo de operacion erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setBanco(String cad)
	{
	  try
		{
	      banco = new Integer(cad).intValue();
		}catch(Exception e)
		 {
			log("MTE_Registro - setNumeroRegistros(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato del codigo de banco erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setImporteAplicar(String cad)
	{
	  try
		{
	      importeAplicar = new Double(cad).doubleValue();
		}catch(Exception e)
		 {
			log("MTE_Registro - setImporteAplicar(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de importe erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setFechaAplicacion(String cad)
	{
	   fechaAplicacion=getFechaString(cad);
	}

     public void setCuentaTesofe(String cad)
	{
	  cuentaTesofe=cad;
	}

  public void setFolio(String cad)
	{
	   try
		{
	      folio = new Long(cad).longValue();
		  if(folio<0)
			{
			  log("MTE_Registro - setFolio(): Error en el formato del folio.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Numero de Folio incorrecto: "+ cad);
			  err+=1;
			}
		}catch(Exception e)
		 {
			log("MTE_Registro - setFolio(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato del folio incorrecto: "+ cad);
			err+=1;
		 }
	}

  public void setReferencia(String cad)
	{
	  	try
		{
	      referencia = new Long(cad).longValue();
		  if(referencia<0)
			{
			  log("MTE_Registro - setReferencia(): Error en el formato de la referencia.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Numero de Referencia incorrecto: "+ cad);
			  err+=1;
			}
		}catch(Exception e)
		 {
			log("MTE_Registro - setReferencia(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de la Referencia incorrecto: "+ cad);
			err+=1;
		 }
	}

  public void setNumeroOrdenFin(String cad)
	{
	    if(!cad.trim().equals(""))
		 {
			try
			{
			  numeroOrdenFin = new Long(cad).longValue();
			  if(numeroOrdenFin<0)
				{
				  log("MTE_Registro - setNumeroOrdenFin(): Error en el formato del  NumeroOrdenFin.", EIGlobal.NivelLog.INFO);
				  listaErrores.add("Numero de Orden incorrecto: "+ cad);
				  err+=1;
				}
			}catch(Exception e)
			 {
				log("MTE_Registro - setNumeroOrdenFin(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				listaErrores.add("Formato del numero de orden incorrecto: "+ cad);
				err+=1;
			 }
		 }
	}

  public void setBeneficiario(String cad)
	{
	  beneficiario=cad;
	}

  public void setRFC(String cad)
	{
	  RFC=cad;
	  if(
		  (RFC.length()!=13 && RFC.length()!=10 && RFC.length()!=12 && RFC.length()!=9) ||
		  !val.validaRFC(RFC)
		)
		{
		  log("MTE_Registro - setRFC(): Error en formato del RFC.", EIGlobal.NivelLog.INFO);
		  listaErrores.add("RFC no cumple con el formato: "+ cad);
		  err+=1;
		}
	}
   public void setUsoFuturo(String cad)
	{
      usoFuturo=cad.trim();
	}


   public void log(String a, EIGlobal.NivelLog b)
	{
	   EIGlobal.mensajePorTrace(a, b);
	}
}