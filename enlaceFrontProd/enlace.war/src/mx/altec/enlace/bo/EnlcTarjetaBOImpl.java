package mx.altec.enlace.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.beans.TrxLM1JBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.EnlcTarjetaDAO;
import mx.altec.enlace.dao.EnlcTarjetaDAOImpl;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * @author ESC
 * Clase que realiza las regals de negocio para la vinculacion de tarjetas.
 */
public class EnlcTarjetaBOImpl implements EnlcTarjetaBO {

	/**
	 * formateo de fecha
	 */
	protected static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "MX"));
	/**
	 * Log
	 */
	private static final String TXTLOG = EnlcTarjetaBOImpl.class.getName();
	/**
	 * MensajeLogin01
	 */
	private static final String MENSAJELOGIN01 = "MensajeLogin01";
	/**
	 * session
	 */
	private static final String SESSION = "session";
	/**
	 * dialogo1
	 */
	private static final String DIALOGO1 = " cuadroDialogo(\"Se gener&oacute; un error al recibir el archivo.\", 1);";

	/**
	 * dialogo2
	 */
	private static final String DIALOGO2 = "Se gener&oacute; un error al recibir el archivo.";


	/**
	 * Valida los datos de las tarjetas
	 * @param request peticion
	 * @param tarjeta numero
	 * @param contrato numero
	 * @return TarjetaContrato tarjeta
	 * @throws DaoException excepcion
	 */
	public TarjetaContrato validarDatosTarjetas(HttpServletRequest request, String tarjeta, String contrato) throws DaoException {
		EIGlobal.mensajePorTrace(TXTLOG + "validarDatosTarjetas BO", EIGlobal.NivelLog.INFO);

		TarjetaContrato tarjetaOBJ;
		tarjetaOBJ = new TarjetaContrato();
		tarjetaOBJ.setCodExito(true);

		FiltroConsultaBean filtros;
		filtros = new FiltroConsultaBean();
		filtros.setTarjeta(tarjeta);
		filtros.setContrato(contrato);
		List<TarjetaContrato> tarjetas;
		tarjetas = consultarDatosTarjetas(filtros, request);
		EIGlobal.mensajePorTrace(TXTLOG + "validarDatosTarjetas BO " + tarjetas.size(), EIGlobal.NivelLog.INFO);
		if(!tarjetas.isEmpty()) {
			tarjetaOBJ.setCodExito(false);
			tarjetaOBJ.setMensError("La tarjeta " + tarjeta + " ya fue agregada anteriormente");
		} else {
			TrxLM1JBean bean = new TrxLM1JBean();
			bean.setTarjeta(tarjeta);
			EnlcTarjetaDAO tarjTrxBO;
			tarjTrxBO = new EnlcTarjetaDAOImpl();
			bean = tarjTrxBO.consultaLM1J(bean);
			if(!bean.isCodExito()){
				tarjetaOBJ.setCodExito(false);
				tarjetaOBJ.setMensError("No es posible dar de alta una tarjeta que no est&aacute; asignada o activa.");
			}
		}
		return tarjetaOBJ;
	}

	/**
	 * Consulta las tarjetas de acuerdo a los filtros
	 * @param filtros filtros
	 * @param request peticion
	 * @return ArrayList arreglo de contratos
	 * @throws DaoException excepcion
	 */
	public List<TarjetaContrato> consultarDatosTarjetas(FiltroConsultaBean filtros, HttpServletRequest request) throws DaoException{
		EIGlobal.mensajePorTrace(TXTLOG + "consultarDatosTarjetas BO", EIGlobal.NivelLog.INFO);
		EnlcTarjetaDAO tarjetaDao;
		tarjetaDao = new EnlcTarjetaDAOImpl();
		List<TarjetaContrato> tarRes;
		tarjetaDao.initTransac();
		tarRes = tarjetaDao.consultarDatosTarjetas(filtros);
		tarjetaDao.closeTransac();
		String clvOper;
		//clvOper = "ESECTV";
		
		/********estatus**********/
		for(TarjetaContrato tarjeta : tarRes){
			
			if(tarjeta.getEstatus() == null || "".equals(tarjeta.getEstatus())) {
				tarjeta.setEstatus("P");
			}
			
			BitaTransacBean bt;
			bt = new BitaTransacBean();
			bt.setCctaOrig(tarjeta.getTarjeta());
			bt.setEstatus(tarjeta.getEstatus());
			bitacoriza(request, BitaConstants.ES_EN_CONS_TARJETA_VINCULACION, "ESECTV00", bt);
		}
		/****************************************************/

		return tarRes;
	}

	/**
	 * Eliminar lista de tarjetas asociadas a un contrato
	 * @param tarjetasReq lista de tarjetas
	 * @param request peticion
	 * @throws DaoException excepcion
	 */
	public void eliminarTarjetas(String[] tarjetasReq, HttpServletRequest request) throws DaoException{
		EIGlobal.mensajePorTrace(TXTLOG + "eliminarTarjetas BO", EIGlobal.NivelLog.INFO);
		ArrayList<TarjetaContrato> tarjetas;
		tarjetas = new ArrayList<TarjetaContrato>();
		EnlcTarjetaDAO tarjetaDao;
		tarjetaDao = new EnlcTarjetaDAOImpl();
		String separado[] = null;
		

		for (String tarjCon : tarjetasReq) {
			separado = tarjCon.split("@");
			TarjetaContrato tarjeta;
			String estatus = "";
			tarjeta = new TarjetaContrato();
			tarjeta.setTarjeta(separado[0]);
			tarjeta.setContrato(separado[1]);
			tarjetas.add(tarjeta);
			
			try{
				tarjetaDao.eliminarTarjeta(tarjeta);
				estatus = "B";
			} catch (DaoException e) {
				EIGlobal.mensajePorTrace(TXTLOG + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(TXTLOG + "No se elimino la tarjeta " + tarjeta.getTarjeta()
						+ " Asociada al contrato " + tarjeta.getContrato(), EIGlobal.NivelLog.INFO);
				estatus = "P";
			}
			BitaTransacBean bt;
			bt = new BitaTransacBean();
			bt.setCctaOrig(separado[0]);
			bt.setEstatus(estatus);
			bitacoriza(request, BitaConstants.ES_EN_BAJA_TARJETA_VINCULACION, "ESEBTV00", bt);
		}

		
	}

	/**
	 * Eliminar lista de tarjetas asociadas a un contrato
	 * @return Long secuencia
	 * @throws DaoException excepcion
	 */
	public Long obtenerSecTarjetas() throws DaoException{
		EIGlobal.mensajePorTrace(TXTLOG + "obtenerSecTarjetas BO", EIGlobal.NivelLog.INFO);
		EnlcTarjetaDAO tarjetaDao;
		tarjetaDao = new EnlcTarjetaDAOImpl();
		Long idLote = null;

		//Id Lote
		idLote = tarjetaDao.getIdLoteAltaSequence();

		return idLote;
	}

	/**
	 * Agregar tarjeta a la BD
	 * @param tarjetas lista de tarjetas
	 * @param request peticion
	 * @throws DaoException excepcion
	 */
	public void agregarTarjeta(List<TarjetaContrato> tarjetas, HttpServletRequest request) throws DaoException{
		EIGlobal.mensajePorTrace(TXTLOG + "agregarTarjeta BO", EIGlobal.NivelLog.INFO);
		HttpSession sess;
		sess = request.getSession();
		Calendar cal;
		cal = Calendar.getInstance();
		cal.setTime(new Date());
		String[] regVincTarj;
		regVincTarj = new String[3];
		regVincTarj[0] = String.valueOf(tarjetas.size());
		Long idLote = null;
		try {
			idLote = obtenerSecTarjetas();
		} catch (DaoException e1) {
			sess.setAttribute(MENSAJELOGIN01, " cuadroDialogo(\"No se pudo obtener el lote de carga.\", 1);");
			throw new DaoException(e1);
		}
		recorreTarjetas(tarjetas, sess, idLote, request, regVincTarj);
		request.getSession().removeAttribute("tarjetasRes");
		request.getSession().setAttribute("tarjetasRes", tarjetas);
	}

	/**
	 * recorre y registra las tarjetas
	 * @param tarjetas lista
	 * @param sess sesion
	 * @param idLote lote
	 * @param request peticion
	 * @param regVincTarj registros
	 */
	public void recorreTarjetas(List<TarjetaContrato> tarjetas, HttpSession sess, Long idLote, HttpServletRequest request, String[] regVincTarj){
		EnlcTarjetaDAO tarjetaDao;
		tarjetaDao = new EnlcTarjetaDAOImpl();
		int exitosos = 0;
		int fallidos = 0;
		for(TarjetaContrato tarjeta : tarjetas){
			if(tarjeta.getEstatus() == null || "".equals(tarjeta.getEstatus())) {
				tarjeta.setEstatus("P");
			}
			BaseResource session1;
			session1 = (BaseResource) sess.getAttribute(SESSION);
			if(tarjeta.getContrato() == null || "".equals(tarjeta.getContrato())) {
				String contrato;
				contrato = session1.getContractNumber();
				tarjeta.setContrato(contrato);
			}

			if(tarjeta.getUsuarioReg() == null || "".equals(tarjeta.getUsuarioReg())) {
				String usuarioReg;
				usuarioReg = session1.getUserID8();
				tarjeta.setUsuarioReg(usuarioReg);
			}
			try {
				tarjeta.setIdLote(String.valueOf(idLote));
				tarjetaDao.agregarTarjetaContrato(tarjeta);
				tarjeta.setCodExito(true);
				exitosos++;
			} catch(DaoException e){
				tarjeta.setCodExito(false);
				fallidos++;
				tarjeta.setMensError("No se pudo guardar la tarjeta");
			}
			
			String clvOper;
			//clvOper = "ESEATV";
			BitaTransacBean bt;
			bt = new BitaTransacBean();
			bt.setCctaOrig(tarjeta.getTarjeta());
			bt.setEstatus(tarjeta.getEstatus());
			EIGlobal.mensajePorTrace("Nombre Archivo101  ::  " + tarjeta.getArchivo(), EIGlobal.NivelLog.DEBUG);
			bt.setNombreArchivo(tarjeta.getArchivo());
			bitacoriza(request, BitaConstants.ES_EN_ALTA_TARJETA_VINCULACION, "ESEATV00", bt);
		}
		
		regVincTarj[1] = String.valueOf(exitosos);
		//regVincTarj[2] = String.valueOf(fallidos);
		enviarNotificacion(request, regVincTarj, idLote);
	}

	/**
	 * Valida el archivo de tarjetas
	 * @param request peticion multipart
	 * @throws BusinessException excepcion
	 */
	public void validarArchivo(HttpServletRequest request) throws BusinessException{
		EIGlobal.mensajePorTrace(TXTLOG + "validarArchivo BO", EIGlobal.NivelLog.INFO);
		HttpSession sess;
		sess = request.getSession();
		BaseResource session1;
		session1 = (BaseResource) sess.getAttribute(SESSION);
		FileItemFactory file_factory;
		file_factory = new DiskFileItemFactory();
		ServletFileUpload servlet_up;
		servlet_up = new ServletFileUpload(file_factory);
		ArrayList<TarjetaContrato> tarjetas;
		tarjetas = new ArrayList<TarjetaContrato>();
		isMultipartContent(request, sess);
		Iterator it;
		it = obtenerParseRequest(servlet_up, request, sess);
		
		String fieldName2=null;
		String nomArchivo = "";

		String noContrato;
		noContrato = session1.getContractNumber();
		
		
		while(it.hasNext()){
			FileItem item;
			item = (FileItem) it.next();
			fieldName2=item.getName();
			
			if(!(fieldName2 == null)){
				
				EIGlobal.mensajePorTrace("Nombre Archivo3  ::  " + fieldName2, EIGlobal.NivelLog.DEBUG);
				
				/****************/
				
				String [] arch;
		        arch = fieldName2.split("/");
		        int numobj = arch.length;
		        String nomArch = arch [numobj - 1];
		        EIGlobal.mensajePorTrace("Nombre Archivo4  ::  " + nomArch, EIGlobal.NivelLog.DEBUG);
		        /****************/
				
				
				nomArchivo = nomArch;
				
			}
			
			if (! item.isFormField()){
				Calendar cal;
				cal = Calendar.getInstance();
				cal.setTime(new Date());
				File archivo_server;
				archivo_server = new File(IEnlace.LOCAL_TMP_DIR + "/" + cal.getTimeInMillis() + item.getName());
				try{
					item.write(archivo_server);
				} catch (Exception e){
					EIGlobal.mensajePorTrace(TXTLOG + "validarArchivo BO No se puede escribir", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(TXTLOG + "validarArchivo BO"+e, EIGlobal.NivelLog.ERROR);
					sess.setAttribute(MENSAJELOGIN01, DIALOGO1);
					throw new BusinessException(e);
				}

				int count;
				count = recorreArchivo(archivo_server, request, sess, noContrato, tarjetas, nomArchivo);

				if(count == 0) {
					sess.setAttribute(MENSAJELOGIN01, " cuadroDialogo(\"El archivo es vacio.\", 1);");
					throw new BusinessException(DIALOGO2);
				}
			}
		}
		
		request.getSession ().setAttribute("tarjetasRes", tarjetas);
	}

	/**
	 * recorre archivo para validar
	 * @param archivo_server archivo
	 * @param request peticion
	 * @param sess sesion
	 * @param noContrato contrato
	 * @param tarjetas lista
	 * @return int count
	 * @throws BusinessException excepcion
	 */
	public int recorreArchivo(File archivo_server, HttpServletRequest request, HttpSession sess, String noContrato, List<TarjetaContrato> tarjetas, String nomArchivo) throws BusinessException{
		FileReader fr = null;
		BufferedReader br = null;
		int count = 0;
		try {
			fr = new FileReader(archivo_server);
			br = new BufferedReader(fr);
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				if(count > 1000) {
					sess.setAttribute(MENSAJELOGIN01, " cuadroDialogo(\"El archivo excede de 1000 registros.\", 1);");
					throw new BusinessException("El archivo excede de 1000 registros.");
				}
				String tarjeta = "";
				String usuarioReg = "";
				TarjetaContrato tarjetaOBJ;
				tarjetaOBJ = new TarjetaContrato();
				tarjetaOBJ.setCodExito(true);
				try{
					tarjeta = strLine.substring(0, 16);
					tarjeta.trim();
					EIGlobal.mensajePorTrace("Dato en tarjeta  ::  " + tarjeta, EIGlobal.NivelLog.DEBUG);
					if(tarjeta.length() != 16) {
						tarjetaOBJ.setCodExito(false);
						guardaError(tarjetaOBJ, noContrato, tarjetas, count, "Longitud incorrecta de n&uacute;mero de tarjeta");
						count++;
						EIGlobal.mensajePorTrace("Longitud de tarjeta  ::  " + tarjeta.length(), EIGlobal.NivelLog.DEBUG);
					}
					try{
						Long.parseLong(tarjeta);

					} catch(NumberFormatException e) {
						guardaError(tarjetaOBJ, noContrato, tarjetas, count, "El n&uacute;mero de la tarjeta es inv&aacute;lido");
						count++;
					}
					if(tarjetaOBJ.isCodExito()){
						tarjetaOBJ.setTarjeta(tarjeta);
					}
				} catch(StringIndexOutOfBoundsException e){
					guardaError(tarjetaOBJ, noContrato, tarjetas, count, "El n&uacute;mero de la tarjeta es inv&aacute;lido");
					count++;
				}
				
				if(tarjetaOBJ.isCodExito()){
					try{
					   
						usuarioReg = strLine.substring(16, 23);
						usuarioReg.trim();
						EIGlobal.mensajePorTrace("Usuario Reg  ::  " + usuarioReg, EIGlobal.NivelLog.DEBUG);
						if(usuarioReg.length() != 7) {
							tarjetaOBJ.setCodExito(false);
							guardaError(tarjetaOBJ, noContrato, tarjetas, count, "El n&uacute;mero de empleado es inv&aacute;lido");
							count++;
						    EIGlobal.mensajePorTrace("Longitud de numero empleado  ::  " + usuarioReg.length(), EIGlobal.NivelLog.DEBUG);
						}
						try{
							Integer.parseInt(usuarioReg);
						} catch(NumberFormatException e) {
							guardaError(tarjetaOBJ, noContrato, tarjetas, count, "El n&uacute;mero de empleado es inv&aacute;lido");
							count++;
						}
						if(tarjetaOBJ.isCodExito()){
							tarjetaOBJ.setUsuarioReg(usuarioReg);
						}
					} catch(StringIndexOutOfBoundsException e){
						guardaError(tarjetaOBJ, noContrato, tarjetas, count, "El campo n&uacute;mero de empleado es requerido");
						count++;
					}
				}
				
				if(tarjetaOBJ.isCodExito()){
					guardaTarjetas(request, tarjetaOBJ, noContrato, tarjetas, tarjeta, nomArchivo);
					count++;
				}
				
			}
			
		} catch (FileNotFoundException e1) {
			sess.setAttribute(MENSAJELOGIN01, DIALOGO1);
			throw new BusinessException(e1);
		} catch (IOException e) {
			sess.setAttribute(MENSAJELOGIN01, DIALOGO1);
			throw new BusinessException(e);
		} finally {
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(TXTLOG + "eliminarTarjetas BO", EIGlobal.NivelLog.DEBUG);
			}
			archivo_server.delete();
		}

		return count;
	}

	/**
	 * Guarda el resultado de la tarjetas
	 * @param request peticion
	 * @param tarjetaOBJ tarjeta objeto
	 * @param noContrato contrato
	 * @param tarjetas lista de tarjetas
	 * @param tarjeta numero de tarjeta
	 */
	public void guardaTarjetas(HttpServletRequest request, TarjetaContrato tarjetaOBJ, String noContrato,
			List<TarjetaContrato> tarjetas, String tarjeta, String nomArchivo){
		TarjetaContrato tarjetaRes;
		try {
			tarjetaRes = validarDatosTarjetas(request, tarjeta, noContrato);
			
			EIGlobal.mensajePorTrace("Nombre Archivo99  ::  " + nomArchivo, EIGlobal.NivelLog.DEBUG);
			
		} catch (DaoException e) {
			tarjetaRes = new TarjetaContrato();
			tarjetaRes.setCodExito(false);
			tarjetaRes.setMensError("No se pudo validar datos");
		}
		tarjetaOBJ.setContrato(noContrato);
		tarjetaOBJ.setArchivo(nomArchivo);
		tarjetaOBJ.setCodExito(tarjetaRes.isCodExito());
		tarjetaOBJ.setMensError(tarjetaRes.getMensError());
		tarjetas.add(tarjetaOBJ);
		
		
		
	}

	/**
	 * Guarda error de las validaciones
	 * @param tarjetaOBJ tarjeta
	 * @param noContrato contrato
	 * @param tarjetas tarjetas lista
	 * @param count conteo
	 * @param mensajeError mensaje
	 */
	public void guardaError(TarjetaContrato tarjetaOBJ, String noContrato, List<TarjetaContrato> tarjetas,
			int count, String mensajeError){
		tarjetaOBJ.setCodExito(false);
		tarjetaOBJ.setMensError(mensajeError);
		tarjetaOBJ.setContrato(noContrato);
		tarjetas.add(tarjetaOBJ);
		count++;
	}

	/**
	 * valid multipart
	 * @param request peticion
	 * @param sess sesion
	 * @throws BusinessException excepcion
	 */
	public void isMultipartContent(HttpServletRequest request, HttpSession sess) throws BusinessException{
		if(!ServletFileUpload.isMultipartContent(request))
	    {
			EIGlobal.mensajePorTrace(TXTLOG + "validarArchivo BO No es multipart", EIGlobal.NivelLog.ERROR);
			sess.setAttribute(MENSAJELOGIN01, DIALOGO1);
			throw new BusinessException(DIALOGO2);
	    }
	}

	/**
	 * obtiene el parse request
	 * @param servlet_up file servlet
	 * @param request peticion
	 * @param sess sesion
	 * @return Iterator it
	 * @throws BusinessException excepcion
	 */
	public Iterator obtenerParseRequest(ServletFileUpload servlet_up, HttpServletRequest request,
			HttpSession sess) throws BusinessException{
		List items = null;
		Iterator it;
		try {
			items = servlet_up.parseRequest(request);
		} catch (FileUploadException e) {
			EIGlobal.mensajePorTrace(TXTLOG + "validarArchivo BO No es un archivo valido", EIGlobal.NivelLog.ERROR);
			sess.setAttribute(MENSAJELOGIN01, DIALOGO1);
			throw new BusinessException(e);
		}

		it = items.iterator();
		return it;
	}

	/**
	 * Ejecuta bitacora
	 * @param request peticion
	 * @param servicio que ejecuta
	 * @param codError codigo
	 * @param bt bitacora
	 */
	private static void bitacoriza(HttpServletRequest request, String servicio, String codError, BitaTransacBean bt) {
		EIGlobal.mensajePorTrace(TXTLOG + "bitacoriza BO", EIGlobal.NivelLog.INFO);
		/*String idFlujo;
		idFlujo=request.getParameter(BitaConstants.FLUJO);*/
		if("ON".equals(Global.USAR_BITACORAS.trim())) {

			HttpSession session1;
			session1 = request.getSession();
			BaseResource baseResource;
			baseResource = (BaseResource) session1.getAttribute(SESSION);

			BitaTCTBean beanTCT = new BitaTCTBean();
			ServicioTux tuxGlobal = new ServicioTux();
			Hashtable hs = null;
			Integer referencia = 0 ;
			bt.setContrato(baseResource.getContractNumber());
			bt.setUsr(baseResource.getUserID8());
			Calendar cal;
			cal = Calendar.getInstance();
			cal.setTime(new Date());
			bt.setFecha(cal.getTime());
			bt.setHora(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
			bt.setNumBit("ENTV00");
			bt.setIdToken(baseResource.getToken().getSerialNumber());
			BitaHelperImpl bh;
			bh = new BitaHelperImpl(request, baseResource, session1);
			bh.llenarBean(bt);
			bt.setIdFlujo(servicio);
			if(servicio.trim().equals(BitaConstants.ES_EN_ALTA_TARJETA_VINCULACION)) {
				bt.setNumBit(servicio + "01");
				beanTCT.setConcepto(BitaConstants.ES_EN_ALTA_TARJETA_VINCULACION_MSG);
			}else if(servicio.trim().equals(BitaConstants.ES_EN_CONS_TARJETA_VINCULACION)) {
				bt.setNumBit(servicio + "02");
				beanTCT.setConcepto(BitaConstants.ES_EN_CONS_TARJETA_VINCULACION_MSG);
			}else if(servicio.trim().equals(BitaConstants.ES_EN_BAJA_TARJETA_VINCULACION)) {
				bt.setNumBit(servicio + "03");
				beanTCT.setConcepto(BitaConstants.ES_EN_BAJA_TARJETA_VINCULACION_MSG);
			}
				
			if(codError!=null) {
				bt.setIdErr(codError);
			}
			try {
				//INICIO BITACORA DE OPERACIONES
				beanTCT = bh.llenarBeanTCT(beanTCT);
				try {
					EIGlobal.mensajePorTrace("EnlcTarjetaBOImpl::bitacoriza:: Llamado al servicio SREFERENCIA",	EIGlobal.NivelLog.DEBUG);
					hs = tuxGlobal.sreferencia("901");;
					codError = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("EnlcTarjetaBOImpl::bitacoriza:: Codigo de Error llamado->"+ codError, EIGlobal.NivelLog.DEBUG);
					referencia = (Integer) hs.get("REFERENCIA");				
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("EnlcTarjetaBOImpl::bitacoriza:: Problemas al obtener la referencia->"+e.getMessage(), EIGlobal.NivelLog.INFO);
					codError = null;
					referencia = 0;
				}				
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(servicio);
				beanTCT.setCodError(codError);
				beanTCT.setUsuario(baseResource.getUserID8());
				BitaHandler.getInstance().insertBitaTCT(beanTCT);
				//FIN BITACORA DE OPERACIONES
				
				bt.setReferencia(referencia);
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(TXTLOG + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(TXTLOG + e.getMessage(), EIGlobal.NivelLog.ERROR);			}
		}
	}

	/**
	 * Metodo que realiza el envio de notificacion de alta vinculacion tarjeta
	 * @param request peticion
	 * @param regVincTarj detalle de alta
	 */
	private void enviarNotificacion(HttpServletRequest request, String[] regVincTarj, Long idLote){
		EIGlobal.mensajePorTrace(TXTLOG + "enviarNotificacion BO", EIGlobal.NivelLog.INFO);

		EmailSender emailSender;
		emailSender=new EmailSender();
		EmailDetails emailDetails;
		emailDetails = new EmailDetails();
		HttpSession sess;
		sess = request.getSession();
		BaseResource session1;
		session1 = (BaseResource) sess.getAttribute(SESSION);
		String numContrato;
		numContrato = session1.getContractNumber();
		String nombreContrato;
		nombreContrato = session1.getNombreContrato();
		emailDetails.setNumeroContrato(numContrato);
		emailDetails.setRazonSocial(nombreContrato);
		emailDetails.setIdLote(String.valueOf(idLote));
		emailDetails.setRegVincTarj(regVincTarj);

		emailSender.sendNotificacion(request,IEnlace.SERV_TARJETA_VINCULA,emailDetails);
	}
}
