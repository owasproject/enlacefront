package mx.altec.enlace.bo;

import java.io.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class EI_Exportar implements Serializable{
	String nombreArchivo = "";
	File drvCon;	
	RandomAccessFile fileCon;
	private boolean addLine = false;

	public EI_Exportar( String str ) {
		nombreArchivo = str;
	}

	public boolean borraArchivo() {
		boolean error=true;

		if( drvCon.delete() ) {
			error=true;
			EIGlobal.mensajePorTrace("EI_Exportar - borraArchivo(): Archivo Eliminado", EIGlobal.NivelLog.INFO);
		} else {
			EIGlobal.mensajePorTrace("EI_Exportar - borraArchivo(): No se puede Borrar el Archivo", EIGlobal.NivelLog.INFO);
			error=false;
		}
		return error;
	}

	public boolean recuperaArchivo() {
		boolean error=true;		

		try	{
			drvCon = new File( nombreArchivo );
			if( drvCon.exists() ) {
				EIGlobal.mensajePorTrace("EI_Exportar - recuperaArchivo(): Archivo ya existe.", EIGlobal.NivelLog.INFO);
			}
			fileCon = new RandomAccessFile( drvCon, "rw" );
			EIGlobal.mensajePorTrace("EI_Exportar - recuperaArchivo(): Archivo abierto.", EIGlobal.NivelLog.INFO);
			error=true;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - recuperaArchivo(): Excepcion, Error al crear el archivo. "+e.getMessage(), EIGlobal.NivelLog.INFO);
			error=false;
		}
		return error;
	}

	public boolean creaArchivo() {
		boolean error=true;

		try {
			drvCon = new File(nombreArchivo);
			if ( drvCon.exists() ) {
				EIGlobal.mensajePorTrace("EI_Exportar - creaArchivo(): El archivo ya existe.", EIGlobal.NivelLog.INFO);
				if ( borraArchivo() )
					EIGlobal.mensajePorTrace( "EI_Exportar - creaArchivo(): Ahora se crea nuevo.", EIGlobal.NivelLog.INFO);
				else {
					EIGlobal.mensajePorTrace( "EI_Exportar - creaArchivo(): " +
							" El archivo no se pudo borrar.", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("EI_Exportar - creaArchivo(): " +
							"Se agregaran los registros al ya existente.", EIGlobal.NivelLog.INFO);
				}
			}
			fileCon = new RandomAccessFile(drvCon, "rw");
			EIGlobal.mensajePorTrace("EI_Exportar - creaArchivo(): Archivo creado. ", EIGlobal.NivelLog.INFO);
			error = true;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - creaArchivo(): Excepcion," +
					" Error al crear el archivo. " + e.getMessage(), EIGlobal.NivelLog.ERROR );
			error=false;
		}
		return error;
	}

	public boolean abreArchivoLectura()
	{
		boolean error=true;
		try
		{
			drvCon = new File( nombreArchivo );
			EIGlobal.mensajePorTrace("El archivo se llama "+nombreArchivo, EIGlobal.NivelLog.INFO);
			if ( drvCon.exists() )
			{
				fileCon = new RandomAccessFile( drvCon, "r" );
				EIGlobal.mensajePorTrace("EI_Exportar - abreArchivoLectura():  Archivo encontrado para leer.", EIGlobal.NivelLog.INFO);
				/*
				EIGlobal.mensajePorTrace("EI_Exportar - abreArchivoLectura():  Longitud = " + Long.toString(fileCon.length()), EIGlobal.NivelLog.INFO);
				if(fileCon.length()<5)
				error=false;
				else
				*/
				error=true;
			} else
				error=false;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - abreArchivoLectura(): Excepcion,  Error al abrir el archivo. "+e.getMessage(), EIGlobal.NivelLog.INFO);
			error=false;
		}
		return error;

	}

	public boolean abreArchivo() {
		boolean error=true;

		try {
			drvCon = new File(nombreArchivo);
			if ( drvCon.exists() ) {
				fileCon=new RandomAccessFile(drvCon, "rw");
				EIGlobal.mensajePorTrace("EI_Exportar - abreArchivo(): Archivo encontrado.", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("EI_Exportar - abreArchivo(): Longitud =" +
						Long.toString(fileCon.length()), EIGlobal.NivelLog.INFO);
				if(fileCon.length()<5)
					error=false;
				else
					error=true;
			} else
				error=false;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - creaArchivo(): Excepcion, Error al abrir el archivo. "+e.getMessage(), EIGlobal.NivelLog.INFO);
			error=false;
		}
		return error;
	}

	public void cierraArchivo()
	{
		try
		{
			fileCon.close();
			EIGlobal.mensajePorTrace("EI_Exportar - cierraArchivo(): Archivo cerrado."+nombreArchivo, EIGlobal.NivelLog.INFO);
		} catch ( IOException e )
		{
			EIGlobal.mensajePorTrace("EI_Exportar - cierraArchivo(): Excepcion, No se pudo cerrar Archivo ("+nombreArchivo+"). "+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	public boolean escribeLinea(String Linea) {
		boolean error=true;

		try	{
		fileCon.writeBytes(Linea);
		if(addLine) {
			fileCon.writeChars("\n");	
		}
		
		error=true;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - escribeLinea(): Excepcion, Error al escribir en archivo. "+e.getMessage(), EIGlobal.NivelLog.INFO);
			error=false;
		}
		return error;
	}

	public String leeLinea()
	{
		boolean error = true;
		String Linea = "";
		try
		{
			Linea = fileCon.readLine();
			if ( ( Linea == null ) || ( Linea.equals( "" ) ) )
				return "ERROR";
			error = true;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace( "EI_Exportar - leeLinea(): Excepcion,  Error al leer el archivo. " + e.getMessage(), EIGlobal.NivelLog.ERROR );
			e.printStackTrace();
			error=false;
		}
		if( error )
			return Linea;
		return "ERROR";
	}

	public boolean eof() {
		try {
			if ( fileCon.readBoolean() ) {
				fileCon.seek( fileCon.getFilePointer() - 1 );
				return false;
			}
		} catch ( EOFException e ) {
			EIGlobal.mensajePorTrace("EI_Exportar - eof(): Excepcion, Fin de Archivo. " +
				e.getMessage(), EIGlobal.NivelLog.ERROR );
			return true;
		} catch ( IOException e ) {
			EIGlobal.mensajePorTrace( "EI_Exportar - eof(): Excepcion, Error... " +
				e.getMessage(), EIGlobal.NivelLog.ERROR );
			return true;
		}
		return true;
	}

	public String formateaCampo( String campo, int tamanio )
	{
		String spc = "                                                                      ";
		String campoFormateado = "";
		campo=campo.trim();

		if ( campo.length() < tamanio )
			campoFormateado = campo + spc.substring( 0, tamanio - campo.length() );

		if ( campo.length() > tamanio )
			campoFormateado=campo.substring(0,tamanio);

		if ( campo.length() == tamanio )
			campoFormateado=campo;

		return campoFormateado;
	}
	
	/**
	 * Meotodo para formatear los campos de alta de cuentas internacionales
	 * Indra 2016
	 * @param campo String Campo a formatear
	 * @param tamanio int Longitud del campo
	 * @return String campo formateado
	 */
	public String formateaCampoCtaInt( String campo, int tamanio )
	{
		String spc = "                                                                      " +
				"                                                                      ";
		String campoFormateado = "";
		campo=campo.trim();
		
		EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Longitud campo: " + campo.length(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Longitud spc: " + spc.length(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Tamanio: " + tamanio, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Dif tamanio - campo: " + (tamanio - campo.length()), EIGlobal.NivelLog.INFO);

		if ( campo.length() < tamanio ){
			try {
				EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Contenido spc: " + spc.replace(' ', '-'), EIGlobal.NivelLog.INFO);
				campoFormateado = campo + spc.substring( 0, tamanio - campo.length() );
				EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Campo formateado: " + campoFormateado.replace(' ', '+'), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Contenido spc en error: " + spc.replace(' ', '-'), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Error al realizar substring " + e.getMessage(), EIGlobal.NivelLog.INFO);
				e.printStackTrace();
			}
		}

		if ( campo.length() > tamanio )
			campoFormateado=campo.substring(0,tamanio);

		if ( campo.length() == tamanio )
			campoFormateado=campo;

		EIGlobal.mensajePorTrace("EI_Exportar - formateaCampoCtaInt(String, int): Campo formateado Final: " + campoFormateado, EIGlobal.NivelLog.INFO);
		
		return campoFormateado;
	}

	public String formateaCampoDerecha( String campo, int tamanio ) {
		String spc = "                                                                      ";
		String campoFormateado = "";
		campo=campo.trim();

		if ( campo.length() < tamanio )
			campoFormateado = spc.substring( 0, tamanio - campo.length() ) + campo;

		if( campo.length() > tamanio )
			campoFormateado = campo.substring( 0, tamanio );

		if ( campo.length() == tamanio )
			campoFormateado = campo;

		return campoFormateado;
	}

	public String formateaCampoImporte(String campo,int tamanio)
	{
      String spc="                                                                      ";
	  String num="0000000000000000000000000000000000000000000000000000000";
      String campoFormateado="";
	  String campoTmp="";

	  campoTmp=campo.trim();
	  campo="";

	  for(int i=0;i<campoTmp.length();i++)
	   {
		  if( campoTmp.charAt(i)!='$' && campoTmp.charAt(i)!=',')
		    campo += campoTmp.charAt(i);
	   }
	  campo=campo.trim();
	  if(campo.indexOf(".")>=0)
	   {
		 campoFormateado=campo.substring(0,campo.indexOf("."));

		 if((campo.substring(campo.indexOf(".")+1,campo.length()) ).length()>=2)
		   campoFormateado+=campo.substring(campo.indexOf(".")+1,campo.indexOf(".")+3);
		 else
		  if((campo.substring(campo.indexOf(".")+1,campo.length()) ).length()<2)
			campoFormateado+=campo.substring(campo.indexOf(".")+1,campo.length())+num.substring(0,2-(campo.substring(campo.indexOf(".")+1,campo.length()) ).length());
	   }
	  else
		campoFormateado=campo+"00";

	  if(campoFormateado.length()<tamanio)
	    campoFormateado=num.substring(0,tamanio-campoFormateado.length())+campoFormateado;

	  return campoFormateado;
	}

	public void setAddLine(boolean addLine) {
		this.addLine = addLine;
	}
}