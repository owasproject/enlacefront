package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

public class TICuentaArchivo
 {
	Vector Cuentas=new Vector();
	int total=0;

	public EI_Tipo Todas=new EI_Tipo();

	public void insertaCuenta(String trama)
	 {
	   EI_Tipo Aux1=new EI_Tipo();
	   EI_Tipo Aux2=new EI_Tipo();

	   int cta=buscaCuenta(trama);
	   if(cta==-1)
		 {
		   Cuentas.add(trama);
		   total=Cuentas.size();
		 }
	   else
		 {
		   System.out.println("#### Error cuenta ya existe");
		 }
	 }

	public void agregaCuenta(String trama)
	 {
	   EI_Tipo Aux1=new EI_Tipo();
	   EI_Tipo Aux2=new EI_Tipo();

	   int cta=buscaCuenta(trama);
	   if(cta==-1)
		 {
		   System.out.println("#### Error cuenta no existe");
		 }
	   else
		 {
		   Aux1.iniciaObjeto(Cuentas.get(cta).toString());
		   Aux2.iniciaObjeto(trama);
		   if(Aux1.camposTabla[0][3].trim().equals(""))
		     Aux1.camposTabla[0][3]+=Aux2.camposTabla[0][3];
		   else
			 Aux1.camposTabla[0][3]+=", "+Aux2.camposTabla[0][3];
		   Aux1.camposTabla[0][0]=Aux2.camposTabla[0][0];
		   Aux1.camposTabla[0][2]=Aux2.camposTabla[0][2];
		   Aux1.camposTabla[0][4]=Aux2.camposTabla[0][4];
		   Aux1.camposTabla[0][5]=Aux2.camposTabla[0][5];
		   Aux1.camposTabla[0][6]=Aux2.camposTabla[0][6];
		   Aux1.camposTabla[0][12]=Aux2.camposTabla[0][12];
		   Cuentas.remove(cta);
		   Cuentas.add(nueva(Aux1));
		 }
	 }

	public void actualizaCuenta(String trama)
	 {
	   int cta=buscaCuenta(trama);

	   if(cta==-1)
	     insertaCuenta(trama);
	   else
		 agregaCuenta(trama);
	 }

    String nueva(EI_Tipo Aux)
	 {
		String trama="";
		int i=0;

		for(i=0;i<Aux.totalCampos;i++)
		  trama+=Aux.camposTabla[0][i]+"|";
		trama+=Aux.camposTabla[0][i]+="@";
		return trama;
	 }

	public int buscaCuenta(String trama)
	 {
	   String cuenta1="";
	   String cuenta2="";

	   cuenta2=trama.substring(0,trama.indexOf("|")).trim();

	   for(int i=0;i<total;i++)
		{
		  cuenta1=Cuentas.get(i).toString().trim();
		  cuenta1=cuenta1.substring(0,cuenta1.indexOf("|"));
		  if(cuenta1.equals(cuenta2))
		    return i;
		}
	   return -1;
	 }

	public void cadenaCuentas()
	 {
	   String tramaCtas="";

	   for(int i=0;i<total;i++)
		 tramaCtas+=Cuentas.get(i).toString();
	   Todas.iniciaObjeto(tramaCtas);
	 }

   public boolean esCero(String num)
	{
	  double saldo=0;

	  saldo=new Double(num).doubleValue();
	  if(saldo>0)
		return false;
	  return true;
	}
 }