package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;

public class CatNominaHTML {

	/**
	 * codigoHTML
	 */
	String codigoHTML = "";
	/**
	 * codigoT
	 */
	String codigoT = "";
	/**
	 * sbCodigoT
	 */
	StringBuilder sbCodigoT = new StringBuilder();
	/**
	 * sbcodigoHTML
	 */
	StringBuilder sbcodigoHTML = new StringBuilder();
	/***************************************************************************
	 *** Método que construye la tabla de resultados con los datos de los   ****
	 *** empleados. 														****
	 *** Ultima Modificacion: 04 Jun 2008 									****
	 *** Autor:Emmanuel Sanchez Castillo 				 					****
	 *** Modificación:se agregaron los campos tipo cuenta, estado
	 * @return String
	 **************************************************************************/
	public String generaEncabezadoBusqueda()
	{
		// EVERIS - CARGA DE DATOS EN TABLAS

		sbcodigoHTML.append( "<center>")
		.append( "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>")
		.append( "<tr>")
		.append( "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO SOLICITADO APARECEN A CONTINUACI&Oacute;N</th>")
		.append( "</tr>")
		.append( "<tr>")

		.append( "<th class=\"tittabdat\" align=center></th>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de<br>Empleado</th>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de<br>Departamento</th>")
		.append( "<th class=\"tittabdat\" align=center>Apellido<br>Paterno</th>")
		.append( "<th class=\"tittabdat\" align=center>Apellido<br>Materno</th>")
		.append( "<th class=\"tittabdat\" align=center>Nombre</th>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero<br>de Cuenta/M&oacute;vil</th>")
		.append( "<th class=\"tittabdat\" align=center>Banco</th>") //VC ESC
		.append( "<th class=\"tittabdat\" align=center>RFC</th>")
		.append( "<th class=\"tittabdat\" align=center>Estado</th>")   //VC ESC
		.append( "<th class=\"tittabdat\" align=center>Ingreso<br> Mensual</th>")
		.append( "</tr>");
		codigoHTML = sbcodigoHTML.toString();
		return codigoHTML;
	}

	/**
	 * @param tipoCarga
	 * @return String
	 */
	public String encabezadoImportaciones(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//EVERIS - ENCABEZADO DE TABLA DE MODIFICACION DE EMPLEADOS POR ARCHIVO

		sbCodigoT.append("<br></br>")
		.append( "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>")
		.append( "<tr>")
		.append( "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO</th>")
		.append( "</tr>")
		.append( "<tr>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta/M&oacute;vil</th>");
		if(tipoCarga.equals(CatNominaConstantes._MODIF))
		{
			sbCodigoT.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th>")
			.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Departamento</th>")
			.append( "<th class=\"tittabdat\" align=center>Ingreso mensual</th>");
		}
		codigoT = sbCodigoT.toString();
		return codigoT;
	}

	/***************************************************************************
	 *** Método que construye la tabla de resultados con los datos de los   ****
	 *** empleados. 														****
	 *** Ultima Modificacion: 04 Jun 2008 									****
	 *** Autor:Emmanuel Sanchez Castillo 				 					****
	 *** Modificación:se agregaron los campos tipo cuenta, estado
	 * @return String****
	 **************************************************************************/
	//public String generaTablaEmpleados(String cuentaRS,String numEmplRS,String deptoRS,String apePatRS,String apeMatRS,String nombreRS,String numTarjetaRS,String rfcRS,String ingresoRS, boolean checkOn)
	public String generaTablaEmpleados(String[] datosQuery, boolean checkOn)
	{
		EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaEmpleados $ Ingresamos a generaTablaEmpleados",EIGlobal.NivelLog.INFO);
		String codigoHTML = "";
		StringBuilder sb = new StringBuilder();
		/*EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[0] HTML = " + datosQuery[0]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[1] = " + datosQuery[1]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[2] = " + datosQuery[2]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[3] = " + datosQuery[3]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[4] = " + datosQuery[4]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[5] = " + datosQuery[5]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[6] = " + datosQuery[6]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[7] = " + datosQuery[7]);
		EIGlobal.mensajePorTrace("CatNominaAction - consultaEmpleados $ ARR[8] = " + datosQuery[8]);*/

		String nombreRadio = datosQuery[8] + datosQuery[0]; //nuevo cambio
		EIGlobal.mensajePorTrace("CatNominaAction - generaTablaEmpleados $ nombreRadio = " + nombreRadio, EIGlobal.NivelLog.DEBUG); //nuevo cambio

		sb.append( "<tr>");

		if(checkOn)
			sb.append( "<td class=\"textabdatobs\"><input type=\"radio\" name=\"cuentaSelec\" value=\""+nombreRadio+"\" checked=\"checked\"> </td>"); // Num cuenta
		else
			sb.append( "<td class=\"textabdatobs\"><input type=\"radio\" name=\"cuentaSelec\" value=\""+nombreRadio+"\"> </td>");   // Num cuenta

		sb.append( "<td class=\"textabdatobs\">"+datosQuery[1]+" </td>"); // numEmpl
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[2]+" </td>"); // Depto
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[3]+" </td>"); // Apellido Paterno
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[4]+"</td>");  // Apelldio Materno
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[5]+"</td>");  // Nombre
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[0]+"</td>");	// Num Cuenta
		sb.append( "<td class=\"textabdatobs\">" +datosQuery[10]+"</td>");  	// Banco // VC ESC
		sb.append( "<td class=\"textabdatobs\">"+datosQuery[6]+"</td>");	// RFC
		sb.append( "<td class=\"textabdatobs\">" +(CatNominaConstantes._ACT.equals(datosQuery[9]) ? CatNominaConstantes._DESC_ACT: CatNominaConstantes._PEND.equals(datosQuery[9]) ? CatNominaConstantes._DESC_PEND : "" )+"</td>");  	// Estado	   // VC ESC
		sb.append( "<td class=\"textabdatobs\" align=\"right\"> $ "+datosQuery[7]+"</td>");  // ingreso mensual
		sb.append( "</tr>");
		codigoHTML = sb.toString();
		return codigoHTML;
	}

	/**
	 * @param arrDatosModif
	 * @return String
	 */
	public String generaTablaModif(String[] arrDatosModif)
	{
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModif$ ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModif$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();

	/*	for(int i=0; i<arrDatosModif.length;i++)
		{
			EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModif$ " + arrDatosModif[i]);

		}*/
		sb.append("<tr>")
		.append("<td class=\"textabdatcla\">" + arrDatosModif[0] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosModif[1] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosModif[2] + "</td>")
		.append("<td class=\"textabdatcla\" align=\"right\">" + arrDatosModif[3] + "</td>")
		.append("</tr>");

	//	EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModif$ Tabla con dATOS " + tablaHTML);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModif$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}

	/**
	 * @param cuentaAbono
	 * @return String
	 */
	public String generaTablaBaja(String cuentaAbono)
	{
		EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaBaja $ Inicio",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaBaja $ Cuenta Abono " + cuentaAbono,EIGlobal.NivelLog.DEBUG);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>")
		.append("<td class=\"textabdatcla\">" + cuentaAbono + "</td>")
		.append("</tr>");

		//EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaBaja $ Tabla con dATOS " + tablaHTML);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaBaja $ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}

	/**
	 * @param totalReg
	 * @return String
	 */
	public String generaFinalTablaModif(int totalReg)
	{
		EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaFinalTablaModif$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();
		sb.append("</table>")
		.append("<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>")
		.append("<tr><td>&nbsp;</td></tr>")
		.append("<tr>")
		.append("<td class=texfootpagneg align=center bottom=\"middle\">")
		.append("Total de registros: " + totalReg + " <br><br>")
		.append("</td>")
		.append("</tr>")
		.append("</table>");
		EIGlobal.mensajePorTrace("CatNominaHTML - generaFinalTablaModif$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}


//******ALTA INTERB*********

	/**
	 * @param tipoCarga
	 * @return String
	 */
	public String encabezadoImporAltaInterb(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//ENCABEZADO DE TABLA DE ALTA DE EMPLEADOS CON CTA. INTERBANCARIA POR ARCHIVO
		sbCodigoT.append( "<br></br>")
		.append( "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>")
		.append( "<tr>")
		.append( "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO PARA GENERAR EL ALTA</th>")
		.append( "</tr>")
		.append( "<tr>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta</th>");
		if(tipoCarga.equals(CatNominaConstantes._ALTA_INTERB))
		{
			sbCodigoT.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th>")
			.append( "<th class=\"tittabdat\" align=center>Nombre</th>")
			.append( "<th class=\"tittabdat\" align=center>Apellido Paterno</th>")
			.append( "<th class=\"tittabdat\" align=center>R.F.C del Empleado</th>")
			.append( "<th class=\"tittabdat\" align=center>Ingreso mensual</th>");
		}
		codigoT = sbCodigoT.toString();
		return codigoT;
	}

	/**
	 * @param arrDatosAlta
	 * @return String
	 */
	public String generaTablaAlta(String[] arrDatosAlta)
	{
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaAlta$ ",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaAlta$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();

	/*	for(int i=0; i<arrDatosAlta.length;i++)
		{
			EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaAlta$ " + arrDatosAlta[i]);

		}*/
		sb.append("<tr>")
		.append("<td class=\"textabdatcla\">" + arrDatosAlta[0] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosAlta[1] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosAlta[6] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosAlta[4] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosAlta[7] + "</td>")
		.append("<td class=\"textabdatcla\" align=\"right\">" + arrDatosAlta[3] + "</td>")
		.append("</tr>");

		//EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaAlta$ Tabla con dATOS " + tablaHTML);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaAlta$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}



	/**
	 * @param totalReg
	 * @return String
	 */
	public String generaTablaFinalAlta(int totalReg)
	{
		EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaFinalAlta$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();
		sb.append("</table>")
		.append("<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>")
		.append("<tr><td>&nbsp;</td></tr>")
		.append("<tr>")
		.append("<td align=center class=texfootpagneg bottom=\"middle\">")
		.append("El total de registros dados de Alta: " + totalReg + " <br><br>")
		.append("</td>")
		.append("</tr>")
		.append("</table>");
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaFinalAlta$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}

//*******Modificación Interb*******

	/**
	 * @param tipoCarga
	 * @return String
	 */
	public String encabezadoImporModifInterb(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//EVERIS - ENCABEZADO DE TABLA DE MODIFICACION DE EMPLEADOS POR ARCHIVO INTERBANCARIA
		sbCodigoT.append( "<br></br>")
		.append( "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>")
		.append( "<tr>")
		.append( "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO A MODIFICAR</th>")
		.append( "</tr>")
		.append( "<tr>")
		.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta</th>");
		if(tipoCarga.equals(CatNominaConstantes._MODIF))
		{
			sbCodigoT.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th")
			.append( "<th class=\"tittabdat\" align=center>N&uacute;mero de Depto</th>")
			//.append( "<th class=\"tittabdat\" align=center>Apellido Paterno</th>")
			//.append( "<th class=\"tittabdat\" align=center>R.F.C del Empleado</th>")
			.append( "<th class=\"tittabdat\" align=center>Ingreso mensual</th>");
		}
		codigoT = sbCodigoT.toString();
		return codigoT;
	}



	/**
	 * @param arrDatosModifInterb
	 * @return String
	 */
	public String generaTablaModifInterb(String[] arrDatosModifInterb)
	{
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModifInterb$ ",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModifInterb$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<arrDatosModifInterb.length;i++)
		{
			EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModifInterb$ " + arrDatosModifInterb[i],EIGlobal.NivelLog.DEBUG);

		}
		sb.append("<tr>")
		.append("<td class=\"textabdatcla\">" + arrDatosModifInterb[0] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosModifInterb[1] + "</td>")
		.append("<td class=\"textabdatcla\">" + arrDatosModifInterb[2] + "</td>")
		.append("<td class=\"textabdatcla\" align=\"right\">" + arrDatosModifInterb[3] + "</td>")
		//.append("<td class=\"textabdatcla\">" + arrDatosModifInterb[5] + "</td>")
		//.append("<td class=\"textabdatcla\">" + arrDatosModifInterb[2] + "</td>")
		.append("</tr>");

		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModifInterb$ Tabla con dATOS " + tablaHTML,EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaModifInterb$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}


	/**
	 * @param totalReg
	 * @return String
	 */
	public String generaFinalTablaModifInterb(int totalReg)
	{
		EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNominaHTML - generaFinalTablaModifInterb$ Inicio",EIGlobal.NivelLog.INFO);
		String tablaHTML = "";
		StringBuilder sb = new StringBuilder();
		sb.append("</table>")
		.append("<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>")
		.append("<tr><td>&nbsp;</td></tr>")
		.append("<tr>")
		.append("<td align=center class=texfootpagneg bottom=\"middle\">")
		.append("El total de registros a modificar son: " + totalReg + " <br><br>")
		.append("</td>")
		.append("</tr>")
		.append("</table>");
		EIGlobal.mensajePorTrace("CatNominaHTML - generaFinalTablaModifInterb$ Fin",EIGlobal.NivelLog.INFO);
		tablaHTML = sb.toString();
		return tablaHTML;
	}



}