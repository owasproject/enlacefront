package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class Archivos {
	
	public Archivos () {
	}


	//*****************  Para Leer solo facturas *********************/

	public Vector sickLee ( String Archivo, String tipo) {
		
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		String NomTempo="";
		Vector lista = new Vector();
		BufferedReader fileAmbiente = null;
		String campo1="";
        String campo2="", campo3="",campo4="",campo5="",campo6="",
						   campo7="",campo8="",campo9="",campo10="",campo11="",
						   campo12="",campo13="",campo14="",campo15="",campo16="",
						   campo17="",campo18="",campo19="",campo20="";

		try{
			  File nombre = new File(Archivo);
              fileAmbiente= new BufferedReader(new FileReader (nombre));

				} catch ( IOException e ) {
                        System.out.println( "Error al leer el archivo de ambiente, e1" + e);
						}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");
		System.out.println (" Registros "+registroLeido);

		try {
			do {
				if(registroLeido == null || ((registroLeido.substring(0,3)).trim()).equals("Err")  || ((registroLeido.substring(0,3)).trim()).equals("ERR") ){
					lista.add("vacio");
				}

				else if ( registroLeido.startsWith( tipo ) ){

											campo = registroLeido.substring(
                                            registroLeido.indexOf( ';' , 1 ) + 1 ,
                                            registroLeido.lastIndexOf( ';' ) - 1);

											for(int i=0; i<campo.length(); i++){
												while(campo.charAt(i) == ';'){
													campo=replace2(campo,';', ": ");
						                        //if ( registroLeido.indexOf( persona ) != -1 )
												//lista.add(campo);
												}
											}
											StringTokenizer st = new StringTokenizer(campo, ":");
											while (st.hasMoreTokens()) {
												campo1 = st.nextToken();
												lista.add(campo1);
												campo2 = st.nextToken();
												lista.add(campo2);
												campo3 = st.nextToken();
												lista.add(campo3);
												campo4 = st.nextToken();
												lista.add(campo4);

												campo5 = st.nextToken();
												NomTempo =NumClave(campo5);
												lista.add(NomTempo);

												campo6 = st.nextToken();
												lista.add(campo6);
												campo7 = st.nextToken();
												lista.add(campo7);
												campo8 = st.nextToken();
												lista.add(campo8);
												campo9 = st.nextToken();
												lista.add(campo9);
												campo10 = st.nextToken();
												lista.add(campo10);
												campo11 = st.nextToken();
												lista.add(campo11);
												campo12 = st.nextToken();
												lista.add(campo12);
												campo13 = st.nextToken();
												lista.add(campo13);
												campo14 = st.nextToken();
												lista.add(campo14);
												campo15 = st.nextToken();
												lista.add(campo15);
												campo16 = st.nextToken();
												lista.add(campo16);
												campo17 = st.nextToken();
												lista.add(campo17);
												campo18 = st.nextToken();
												lista.add(campo18);
												campo19 = st.nextToken();
												lista.add(campo19);
												campo20 = st.nextToken();
												lista.add(campo20);
											}

				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);
	}


	//*****************  Para Leer las claves *********************/

	public Vector mLee ( String Archivo, String tipo){
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		Vector lista = new Vector();
		BufferedReader fileAmbiente = null;

		try {
			File arch = new File(Archivo);
			fileAmbiente= new BufferedReader(new FileReader (arch));
		} catch ( IOException e ) {
			System.out.println( "Error al leer el archivo de ambiente, e1 " + e);
		}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");

		try {
			do {
				if(registroLeido == null || ((registroLeido.substring(0,3)).trim()).equals("Err")  || ((registroLeido.substring(0,3)).trim()).equals("ERR")){
					lista.add("vacio");
				}
				else{
						if ( registroLeido.startsWith( tipo ) ) {

							if (tipo != "9") {
								campo =	registroLeido.substring(
									registroLeido.indexOf( ';' , 2 ) + 1 ,
								registroLeido.lastIndexOf( ';' ) );
								lista.add(campo);
							} else {
								campo = registroLeido.substring(
									registroLeido.indexOf( ';' , 12 ) + 1 ,
								registroLeido.lastIndexOf( ';' ) - 2);
								campo = campo.replace(';',' ') ;
								lista.add(campo);
							}
					}
				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);
	}



	//*****************  Para Leer solo Notas *********************/

	public Vector noteLee ( String Archivo, String tipo) {
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		Vector lista = new Vector();
		BufferedReader fileAmbiente = null;
		 String campo1="";
         String campo2="", campo3="",campo4="",campo5="",campo6="",
						   campo7="",campo8="",campo9="",campo10="",campo11="", campo12="", campo13="";

		try{
			  File nombre = new File(Archivo);
              fileAmbiente= new BufferedReader(new FileReader (nombre));

				} catch ( IOException e ) {
                        System.out.println( "Error al leer el archivo de ambiente, e1" + e);
						}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");
		System.out.println (" Registros "+registroLeido);

		try {
			do {

				if(registroLeido == null  || ((registroLeido.substring(0,3)).trim()).equals("Err") || ((registroLeido.substring(0,3)).trim()).equals("ERR")){
					lista.add("vacio");
				}

				else if ( registroLeido.startsWith( tipo ) ){

											campo = registroLeido.substring(
                                            registroLeido.indexOf( ';' , 1 ) + 1 ,
                                            registroLeido.lastIndexOf( ';' ) - 1);

											for(int i=0; i<campo.length(); i++){
												while(campo.charAt(i) == ';'){
													campo=replace2(campo,';', ": ");
						                        //if ( registroLeido.indexOf( persona ) != -1 )
												//lista.add(campo);
												}
											}
											StringTokenizer st = new StringTokenizer(campo, ":");
											while (st.hasMoreTokens()) {
												campo1 = st.nextToken();
												lista.add(campo1);
												campo2 = st.nextToken();
												lista.add(campo2);
												campo3 = st.nextToken();
												lista.add(campo3);
												campo4 = st.nextToken();
												lista.add(campo4);
												campo5 = st.nextToken();
												lista.add(campo5);
												campo6 = st.nextToken();
												lista.add(campo6);
												campo7 = st.nextToken();
												lista.add(campo7);
												campo8 = st.nextToken();
												lista.add(campo8);
												campo9 = st.nextToken();
												lista.add(campo9);
												campo10 = st.nextToken();
												lista.add(campo10);
												campo11 = st.nextToken();
												lista.add(campo11);
												campo12 = st.nextToken();
												lista.add(campo12);
												campo13 = NumClave(campo3);
												lista.add(campo13);

											}


				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);
	}


	//*****************  Para Leer solo Documentos *********************/


	public Vector docLee ( String Archivo, String tipo) {
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		Vector lista = new Vector();
		BufferedReader fileAmbiente = null;
		 String campo1="";
         String campo2="", campo3="",campo4="",campo5="", campo6="", campo7 ="", campo8="", campo9="";

		try{
			  File nombre = new File(Archivo);
              fileAmbiente= new BufferedReader(new FileReader (nombre));

				} catch ( IOException e ) {
                        System.out.println( "Error al leer el archivo de ambiente, e1" + e);
						}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");
		System.out.println (" Registros "+registroLeido);

		try {
			do {
				if(registroLeido == null || ((registroLeido.substring(0,3)).trim()).equals("Err") || ((registroLeido.substring(0,3)).trim()).equals("ERR")){
					lista.add("vacio");
				}

				else if ( registroLeido.startsWith( tipo ) ){

											campo = registroLeido.substring(
                                            registroLeido.indexOf( ';' , 1 ) + 1 ,
                                            registroLeido.lastIndexOf( ';' ) - 1);

											for(int i=0; i<campo.length(); i++){
												while(campo.charAt(i) == ';'){
													campo=replace2(campo,';', ": ");
						                        //if ( registroLeido.indexOf( persona ) != -1 )
												//lista.add(campo);
												}
											}
											StringTokenizer st = new StringTokenizer(campo, ":");
											while (st.hasMoreTokens()) {
												campo1 = st.nextToken();
												lista.add(campo1);
												campo2 = st.nextToken();
												lista.add(campo2);
												campo3 = st.nextToken();
												lista.add(campo3);
												campo4 = st.nextToken();
												lista.add(campo4);
												campo5 = st.nextToken();
												lista.add(campo5);
												campo6 = st.nextToken();
												lista.add(campo6);
												campo7 = st.nextToken();
												lista.add(campo7);
												campo8 = NumClave(campo3);
												lista.add(campo8);

											}
				}

			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return (lista);
	}


	public String replace2(String fuente,char oldChar,String newString){
		StringBuffer toReturn = new StringBuffer ("");/*<----*/
		int index=fuente.indexOf(oldChar);

		if (index!=-1){
		if (index!=0){
		toReturn.append (fuente.substring(0,index));
		toReturn.append (newString);
		toReturn.append (fuente.substring(index+1,fuente.length()));
		}
		}
		return toReturn.toString();
	}

	public Vector filtro1(Vector ls_1, Vector Filtro)
	{
		int k;
		String tempo="";
		Vector regreso = new Vector();
		System.out.println("-------->>Dentro de la funcion filtro1");
		for (k=0; k<Filtro.size(); k++)
		{
			tempo = (String)Filtro.elementAt(k);
			int j=0;

			while (j<ls_1.size())
			{

				if (tempo.trim().equals(((String)ls_1.elementAt(j+18)).trim()))
				{
					System.out.println("-------->>Valor de variable Vector"+ls_1.elementAt(j+18));
					System.out.println("-------->>Valor de varibale Filtro"+tempo);
					for(int i=j; i<j+20; i++)
					regreso.add(ls_1.elementAt(i));
				}
				j+=20;
			}

		}System.out.println("-------->>Saliendo de la funcion filtro1");
		if(regreso.size()==0) regreso.add("vacio");
		return(regreso);

	}


	public Vector filtro2(Vector ls_2, Vector Filtro)
	{
		int k;
		String tempo="";
		Vector regreso2 = new Vector();
		System.out.println("-------->>Dentro de la funcion filtro2");
		for (k=0; k<Filtro.size(); k++)
		{
			tempo = (String)Filtro.elementAt(k);
			int j=0;
			while (j<ls_2.size())
			{
				if (tempo.trim().equals(((String)ls_2.elementAt(j+10)).trim()))
				{
					System.out.println("-------->>Valor de variable Vector"+ls_2.elementAt(j+10));
					System.out.println("-------->>Valor de varibale Filtro"+tempo);
					for(int i=j; i<j+13; i++)
					regreso2.add(ls_2.elementAt(i));
				}
				j+=13;
			}
		}System.out.println("-------->>Saliendo de la funcion filtro2");
		if(regreso2.size()==0) regreso2.add("vacio");
		return(regreso2);

	}

	public String NumClave(String clave)throws IOException{

		claveTrans cvtrans = new claveTrans();

		Vector Vec1 = new Vector();
		Vector Vec2 = new Vector();
		String regreso = "";

		Vec1 = cvtrans.mLee(Global.DOWNLOAD_PATH+"1001851.prov", "9");
		Vec2 = mLee(Global.DOWNLOAD_PATH+"1001851.prov", "9");

		System.out.println("-------->>Vectores llenos");
		for(int i =0; i<Vec2.size(); i++){
				if(clave.trim().equals(((String)Vec1.elementAt(i)).trim())){
					regreso= (String)Vec2.elementAt(i);
				}

		}
		if(regreso.equals("")){regreso = "&nbsp;";}
		System.out.println("-------->>Saliendo de la funcion NumClave");
		return (regreso);
	}


	public String exportar(String nombre, Vector ls_1, Vector ls_2) {
        try {
            File Exportado = new File(Global.DIRECTORIO_LOCAL + "/" + nombre + ".doc");
            boolean Creado = Exportado.createNewFile();
            if (!Creado) {
                Exportado.delete();
                Exportado.createNewFile();
            }
            FileWriter writer = new FileWriter(Exportado);
            StringBuffer Linea = new StringBuffer ("");
			Vector tmp = new Vector();
			Vector tmp2 = new Vector();
			int j = 0;
			int i = 0;
			if(ls_1.size() !=1){
				tmp = rellena(ls_1);
				while (j<tmp.size()) {
					Linea = new StringBuffer ((String)tmp.elementAt(j));
					Linea.append ((String)tmp.elementAt(j+1));
					Linea.append ((String)tmp.elementAt(j+2));
					Linea.append (" ");
					Linea.append ((String)tmp.elementAt(j+3));
					Linea.append ((String)tmp.elementAt(j+4));
					Linea.append ((String)tmp.elementAt(j+5));
					Linea.append ((String)tmp.elementAt(j+6));
					Linea.append ((String)tmp.elementAt(j+7));
					Linea.append ((String)tmp.elementAt(j+8));
					Linea.append ((String)tmp.elementAt(j+9));
					Linea.append ("\n");

					System.out.println(Linea.toString());
					writer.write(Linea.toString());
					j+=10;
				}

            }
			if(ls_2.size() !=1){
				tmp2 = rellenaOtros(ls_2);
				while (i<tmp2.size()) {
					Linea = new StringBuffer ((String)tmp2.elementAt(i));
					Linea.append ((String)tmp2.elementAt(i+1));
					Linea.append ((String)tmp2.elementAt(i+2));
					Linea.append ((String)tmp2.elementAt(i+3));
					Linea.append ((String)tmp2.elementAt(i+4));
					Linea.append ((String)tmp2.elementAt(i+5));
					Linea.append ((String)tmp2.elementAt(i+6));
					Linea.append ((String)tmp2.elementAt(i+7));
					Linea.append ((String)tmp2.elementAt(i+8));
					Linea.append ("\n");


					System.out.println(Linea.toString());
					writer.write(Linea.toString());
					i+=9;
				}

            }
			writer.flush ();
            writer.close ();

            if (envia (Exportado)) {
                return Exportado.getName ();
            } else {
                return "Error al enviar el archivo";
            }
        } catch (FileNotFoundException ex) {
            System.err.println (ex.getMessage ());
            ex.printStackTrace (System.err);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return "Error al escribir el archivo";
    }


     // Metodo para enviar un archivo al servidor web

    public static boolean envia (File Archivo) {
       try {
    	   
    	    EIGlobal.mensajePorTrace("Archivos - envia - Subiendo archivo a servidor web",EIGlobal.NivelLog.DEBUG);
    	    //IF PROYECTO ATBIA1 (NAS) FASE II

           	boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();
           
           	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB")){
				
					EIGlobal.mensajePorTrace("*** Archivos.envia  no se pudo copiar archivo remoto_web:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** Archivos.envia archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);
				    
				}
           
           	if (Respuestal == true){
                return true;
			}
       	     
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return false;
    }


	//Metodos Auxiliares

    public static String rellenar
        //parametros
        (
            String cad, //Cadena a rellenar
            int    lon, //Longitud a rellenar
            char   rel, //Caracter de relleno
            char   tip  //Tipo de relleno {I=Izq,D=Der}
        )
    {
        if( cad.length()>lon ) return cad.substring(0,lon);
        if( tip!='I' && tip!='D' ) tip = 'I';
			StringBuffer aux = new StringBuffer ("");/*<--*/
        if( tip=='D' ) {aux = new StringBuffer (cad);}
		 for (int i=0; i<(lon-cad.length()); i++)
		{ aux.append ("");
			 aux.append (rel);}
        if( tip=='I' ) {aux.append ("");
			           aux.append (cad);}
		return aux.toString();
    }

	public String CambiaFechas(String Fecha){
		 if ((null == Fecha) || (Fecha.equals (""))) return null;
		StringTokenizer st = new StringTokenizer (Fecha, "/");
        String Dia = st.nextToken ();
        String Mes = st.nextToken ();
        String Anio = st.nextToken ();
		String NewFecha = Mes+Dia+Anio;
        return (NewFecha);
    }

	public String QuitaPuntos(String Importe){
		int p = 0;
		String temp =" ";
		 if ((null == Importe) || (Importe.equals (""))) return null;
		else{
					p = Importe.indexOf('.');
		}
		String regreso = Importe.substring(0, p)+"00";
		return (regreso);
	}

	public Vector rellena(Vector ls_1){
		String vacio = "";
		Vector regreso = new Vector();
		int i = 0 ;
      		String numpersona;
			String nombrepersona;
			String cvedocumento;
			String nudocumentos;
			String importe;
			String fechavencimiento;
			String fechaproceso;
			String estatuspago;
			String referencia;
			String observaciones;
		while(i<ls_1.size()){
			numpersona = ((String)ls_1.elementAt(i+2)).trim();
			nombrepersona =(String)ls_1.elementAt(i+4);
			cvedocumento = ((String)ls_1.elementAt(i)).trim();
			nudocumentos = ((String)ls_1.elementAt(i+3)).trim();
			importe = ((String)ls_1.elementAt(i+6)).trim();
			fechavencimiento =((String)ls_1.elementAt(i+9)).trim();
			fechaproceso = ((String)ls_1.elementAt(i+8)).trim();
			estatuspago = ((String)ls_1.elementAt(i+18)).trim();
			referencia = ((String)ls_1.elementAt(i+16)).trim();
			observaciones = (String)ls_1.elementAt(i+14);


			if(vacio.equals(numpersona)){ numpersona = rellenar(numpersona, 20, '0', 'D');}
			else {numpersona = rellenar(numpersona, (int)20-numpersona.length(), ' ', 'D');}

			if(vacio.equals(nombrepersona)){nombrepersona = rellenar(nombrepersona, 80, '0', 'D');}
			else {nombrepersona = rellenar(nombrepersona, (int)80-nombrepersona.length(), ' ', 'D');}

			if(vacio.equals(cvedocumento)){cvedocumento = rellenar(cvedocumento, 3, '0', 'D');}
			else {cvedocumento = "00"+cvedocumento;}

			if(vacio.equals(nudocumentos)){nudocumentos = rellenar(nudocumentos, 8, '0', 'D');}
			else {nudocumentos = rellenar(nudocumentos, (int)8-nudocumentos.length(), ' ', 'D');}

			if(vacio.equals(importe)){importe = rellenar(importe, 21, '0','D');}
			else {importe = QuitaPuntos(importe);
				importe = rellenar(importe, (int)21-importe.length(), '0', 'I');}

			if(vacio.equals(fechavencimiento)){fechavencimiento = rellenar(fechavencimiento, 8, '0','D');}
			else {fechavencimiento = CambiaFechas(fechavencimiento);}

			if(vacio.equals(fechaproceso)){fechaproceso = rellenar(fechaproceso, 8, '0', 'D');}
			else {fechaproceso = CambiaFechas(fechaproceso);}

			if (vacio.equals(estatuspago)){estatuspago = "0";}

			if(vacio.equals(referencia)){referencia = rellenar(referencia, 12, '0', 'D');}
			else {referencia = rellenar(referencia, (int)12-referencia.length(), '0', 'I');}

			if(vacio.equals(observaciones)){observaciones = rellenar(observaciones, 80, '0', 'D');}

			regreso.add(numpersona);
			regreso.add(nombrepersona);
			regreso.add(cvedocumento);
			regreso.add(nudocumentos);
			regreso.add(importe);
			regreso.add(fechavencimiento);
			regreso.add(fechaproceso);
			regreso.add(estatuspago);
			regreso.add(referencia);
			regreso.add(observaciones);
			i+=20;
		}//Fin del while

		return (regreso);
	}

	public Vector rellenaOtros(Vector ls_2){
		String vacio = "";
		Vector regreso = new Vector();
		int i = 0 ;
			String numpersona;
			String nombrepersona;
			String cvedocumento;
			String nudocumentos;
			String importe;
			String fechavencimiento;
			String fechaproceso;
			String estatuspago;
			String referencia;
			String observaciones;
		while(i<ls_2.size()){
			numpersona = ((String)ls_2.elementAt(i+2)).trim();
			nombrepersona =(String)ls_2.elementAt(i+12);
			cvedocumento = ((String)ls_2.elementAt(i)).trim();
			nudocumentos = ((String)ls_2.elementAt(i+5)).trim();
			importe = ((String)ls_2.elementAt(i+4)).trim();
			fechaproceso = ((String)ls_2.elementAt(i+6)).trim();
			estatuspago = ((String)ls_2.elementAt(i+10)).trim();
			referencia = ((String)ls_2.elementAt(i+9)).trim();
			observaciones = (String)ls_2.elementAt(i+8);

			if(vacio.equals(numpersona)){ numpersona = rellenar(numpersona, 20, '0', 'D');}
			else {numpersona = rellenar(numpersona, (int)20-numpersona.length(), ' ', 'D');}

			if(vacio.equals(nombrepersona)){nombrepersona = rellenar(nombrepersona, 80, '0', 'D');}
			else {nombrepersona = rellenar(nombrepersona, (int)80-nombrepersona.length(), ' ', 'D');}

			if(vacio.equals(cvedocumento)){cvedocumento = rellenar(cvedocumento, 3, '0', 'D');}
			else {cvedocumento = "00"+cvedocumento;}

			if(vacio.equals(nudocumentos)){nudocumentos = "0";}

			if(vacio.equals(importe)){importe = rellenar(importe, 21, '0','D');}
			else {importe = QuitaPuntos(importe);
				importe = rellenar(importe, (int)21-importe.length(), '0', 'I');}

			if(vacio.equals(fechaproceso)){fechaproceso = rellenar(fechaproceso, 8, '0', 'D');}
			else {fechaproceso = CambiaFechas(fechaproceso);}

			if (vacio.equals(estatuspago)){estatuspago = "0";}

			if(vacio.equals(referencia)){referencia = rellenar(referencia, 12, '0', 'D');}
			else {referencia = rellenar(referencia, (int)12-referencia.length(), '0', 'I');}

			if(vacio.equals(observaciones)){observaciones = rellenar(observaciones, 80, '0', 'D');}

			regreso.add(numpersona);
			regreso.add(nombrepersona);
			regreso.add(nudocumentos);
			regreso.add(cvedocumento);
			regreso.add(importe);
			regreso.add(fechaproceso);
			regreso.add(estatuspago);
			regreso.add(referencia);
			regreso.add(observaciones);
			i=i+13;
		}//Fin del while

		return (regreso);
	}


	public Vector provLee ( String Archivo, String tipo, String clave) {
		System.out.println("Entrando a provLee 1.0");
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		String NomTempo="";
		Vector lista = new Vector();
		System.out.println("Entrando a provLee 1.1");
		BufferedReader fileAmbiente = null;
		String campo1="";
        String campo2="", campo3="",campo4="",campo5="",campo6="",
						   campo7="",campo8="",campo9="",campo10="",campo11="",
						   campo12="",campo13="",campo14="",campo15="",campo16="",
						   campo17="",campo18="",campo19="",campo20="";

		System.out.println("Entrando a provLee 1.2");
		try{
			  File nombre = new File(Archivo);
              fileAmbiente= new BufferedReader(new FileReader (nombre));

		} catch ( IOException e ) {
                        System.out.println( "Error al leer el archivo de ambiente, e1" + e);
		}

		System.out.println("Entrando a provLee 1.3");
		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e3 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer registros y antes de crear lista ");

		System.out.println("Entrando a provLee 1.4");
		try {
			do {
				System.out.println (" Registros "+registroLeido);

				if(registroLeido == null || ((registroLeido.substring(0,3)).trim()).equals("Err") || ((registroLeido.substring(0,3)).trim()).equals("ERR")){
					lista.add("vacio");
				}

				else if ( registroLeido.startsWith( tipo ) ){

											campo = registroLeido.substring(
                                            registroLeido.indexOf( ';' , 1 ) + 1 ,
                                            registroLeido.lastIndexOf( ';' ) - 1);

											for(int i=0; i<campo.length(); i++){
												while(campo.charAt(i) == ';'){
													campo=replace2(campo,';', ": ");
												}
											}
											StringTokenizer st = new StringTokenizer(campo, ":");
											while (st.hasMoreTokens()) {

												if( ((tipo).trim()).equals("1")){
													campo1 = st.nextToken();
													campo2 = st.nextToken();
													campo3 = st.nextToken();
													campo4 = st.nextToken();
													campo5 = st.nextToken();  //Le corresponde el nombre
													NomTempo =NumClave(campo5);
													campo6 = st.nextToken();
													campo7 = st.nextToken();
													campo8 = st.nextToken();
													campo9 = st.nextToken();
													campo10 = st.nextToken();
													campo11 = st.nextToken();
													campo12 = st.nextToken();
													campo13 = st.nextToken();
													campo14 = st.nextToken();
													campo15 = st.nextToken();
													campo16 = st.nextToken();
													campo17 = st.nextToken();
													campo18 = st.nextToken();
													campo19 = st.nextToken();
													campo20 = st.nextToken();

													if(((campo3).trim()).equals(clave)){
														lista.add(campo1);
														lista.add(campo2);
														lista.add(campo3);
														lista.add(campo4);
														lista.add(NomTempo);
														lista.add(campo6);
														lista.add(campo7);
														lista.add(campo8);
														lista.add(campo9);
														lista.add(campo10);
														lista.add(campo11);
														lista.add(campo12);
														lista.add(campo13);
														lista.add(campo14);
														lista.add(campo15);
														lista.add(campo16);
														lista.add(campo17);
														lista.add(campo18);
														lista.add(campo19);
														lista.add(campo20);
													}
												}

											if( ((tipo).trim()).equals("2")){
												campo1 = st.nextToken();
												campo2 = st.nextToken();
												campo3 = st.nextToken();
												campo4 = st.nextToken();
												campo5 = st.nextToken();
												campo6 = st.nextToken();
												campo7 = st.nextToken();
												campo8 = st.nextToken();
												campo9 = st.nextToken();
												campo10 = st.nextToken();
												campo11 = st.nextToken();
												campo12 = st.nextToken();
												campo13 = NumClave(campo3);

												if(((campo3).trim()).equals(clave)){
													lista.add(campo1);
													lista.add(campo2);
													lista.add(campo3);
													lista.add(campo4);
													lista.add(campo5);
													lista.add(campo6);
													lista.add(campo7);
													lista.add(campo8);
													lista.add(campo9);
													lista.add(campo10);
													lista.add(campo11);
													lista.add(campo12);
													lista.add(campo13);
												}
											}

											if( ((tipo).trim()).equals("3")){
												campo1 = st.nextToken();
												campo2 = st.nextToken();
												campo3 = st.nextToken();
												campo4 = st.nextToken();
												campo5 = st.nextToken();
												campo6 = st.nextToken();
												campo7 = st.nextToken();
												campo8 = NumClave(campo3);

												if(((campo3).trim()).equals(clave)){
													lista.add(campo1);
													lista.add(campo2);
													lista.add(campo3);
													lista.add(campo4);
													lista.add(campo5);
													lista.add(campo6);
													lista.add(campo7);
													lista.add(campo8);
												}
											}

										}//fin del tokenizer

				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		if(lista.size()==0) lista.add("vacio");
		return (lista);
	}
}

