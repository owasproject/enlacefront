package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.dao.NomPreTarjetaDAO;

public class NomPreBusquedaAction {
	
	public StringBuffer armaTablaFiltros(String fechaHoy){
		StringBuffer sbTabla= new StringBuffer();			
		sbTabla.append("\n<tr>");
		sbTabla.append("\n <td align=center>");
		sbTabla.append("\n <table cellspacing=2 cellpadding=4 border=0 width=500 class=textabdatcla>");
		sbTabla.append("\n <tr><td colspan=4 class=tittabdat> Capture los datos para la consulta de Empleados<br/></td></tr>");
		sbTabla.append("\n <tr><td colspan=4 width=150 class=tabmovtex>Ingrese criterio de b&uacute;squeda: </td></tr>");
		sbTabla.append("\n <tr>");
		sbTabla.append("\n   <td class=tabmovtex>");
		sbTabla.append("\n   <input type=radio value='NE' name=rbTipoFiltro checked=checked onclick='getTipoFiltro();'/>No. Empleado </td>");
		sbTabla.append("\n   <td class=tabmovtex>");
		sbTabla.append("\n   <input type=radio value='NT' name=rbTipoFiltro onclick='getTipoFiltro();'/>No Tarjeta");
		sbTabla.append("\n   </td>");
		sbTabla.append("\n   <td rowspan=2 colspan=2 width=270 class=CeldaContenido>");
		sbTabla.append("\n   <input type=text onchange='convierteMayus(this);' class=tabmovtex value='' maxlength=30 size=30 name=valCriterio onblur='validaDatos(this);'/>");			
		sbTabla.append("\n </td></tr>");						
		sbTabla.append("\n <tr>");
		sbTabla.append("\n   <td rowspan=2> <input type=radio value='NO'  name=rbTipoFiltro onclick='getTipoFiltro();'/>Nombre </td>");
		sbTabla.append("\n   <td class=CeldaContenido><input type=text value='' name=valNombre maxlength=30 size=30 onblur='validaAlfanumerico(this);'></td>");
		sbTabla.append("\n   <td class=CeldaContenido><input type=text value='' name=valPaterno maxlength=30 size=30 onblur='validaAlfanumerico(this);'></td>");
		sbTabla.append("\n   <td class=CeldaContenido><input type=text value='' name=vaMaterno maxlength=30 size=30 onblur='validaAlfanumerico(this);'></td>");
		sbTabla.append("\n </tr>");
		sbTabla.append("\n <tr>");
		sbTabla.append("\n <td class=textabdatcla>Nombre</td>");
		sbTabla.append("\n <td class=textabdatcla> Paterno</td>");
		sbTabla.append("\n <td class=textabdatcla> Materno </td>");
		sbTabla.append("\n </tr>");			
		sbTabla.append("\n <tr><td> <input type=radio value='FE' name=rbTipoFiltro onclick='getTipoFiltro();'/>Fecha </td></tr>");
		sbTabla.append("\n <td colspan=3>");
		sbTabla.append("\n <input type=text name=FechaIni value='"+fechaHoy+"' size=10 height=14 align=absmiddle class=textabdatcla onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
		sbTabla.append("\n </td>");			
		sbTabla.append("\n <tr><td><input type=radio value='TO' name=rbTipoFiltro onclick='getTipoFiltro();'/>Todos</td> <td colspan=3></td></tr>");			
		sbTabla.append("\n </table><br>");
		sbTabla.append("\n <table height=22 cellspacing=0 cellpadding=0 border=0 bgcolor='ffffff' width=166>");
		sbTabla.append("\n   <tr>");
		sbTabla.append("\n     <td height=22 align=right width=90 valign=top>");
		sbTabla.append("\n     <a href=\"javascript:consulta();\" border=0 > <img  src=\"/gifs/EnlaceMig/gbo25220.gif\" border=0></a>");
		sbTabla.append("\n     </td>");
		sbTabla.append("\n     <td height=22 align=left width=76 valign=middle>");		
		sbTabla.append("\n     <a href=\"\" border=0 > <img  src=\"/gifs/EnlaceMig/gbo25250.gif\" border=0></a>");
		sbTabla.append("\n     </td>");			
		sbTabla.append("\n   </tr>");
		sbTabla.append("\n </table>");
		sbTabla.append("\n </td>");
		sbTabla.append("\n</tr>");
		return sbTabla;
	}
	/**************************************************************************/
	/******************************************************** fecha Consulta  */
	/**************************************************************************/
		public String generaFechaAnt(String tipo) {
		    String strFecha = "";
		    java.util.Date Hoy = new java.util.Date();
		    GregorianCalendar Cal = new GregorianCalendar();

		    Cal.setTime(Hoy);

		    do {
		        Cal.add(Calendar.DATE, -1);
	        }while(Cal.get(Calendar.DAY_OF_WEEK) == 1 || Cal.get(Calendar.DAY_OF_WEEK) == 7);

		    if(tipo.equals("FECHA")) {
		        if(Cal.get(Calendar.DATE) <= 9)
		            strFecha += "0" + Cal.get(Calendar.DATE) + "/";
		        else
		            strFecha += Cal.get(Calendar.DATE) + "/";

		        if(Cal.get(Calendar.MONTH)+ 1 <= 9)
		            strFecha += "0" + (Cal.get(Calendar.MONTH) + 1) + "/";
		        else
		            strFecha += (Cal.get(Calendar.MONTH) + 1) + "/";

		        strFecha+=Cal.get(Calendar.YEAR);
	        }

		    if(tipo.equals("DIA"))
		        strFecha += Cal.get(Calendar.DATE);

		    if(tipo.equals("MES"))
		        strFecha += (Cal.get(Calendar.MONTH) + 1);

		    if(tipo.equals("ANIO"))
		        strFecha += Cal.get(Calendar.YEAR);
		    return strFecha;
	    } 
		
		public NomPreLM1D obtenRelacionTarjetaEmpleado(String noContrato, String noTarjeta, Date fecha, NomPreEmpleado empleado) {
			
			NomPreTarjetaDAO dao = new NomPreTarjetaDAO();
			NomPreLM1D lm1d = null;
			NomPreLM1D result = null;
			String paginacion = "";									
			
			do {						
				
				lm1d = dao.consultaRelacionEmpleado(noContrato, noTarjeta, fecha, empleado, paginacion);
				
				if (lm1d == null) break;
				
				if (result == null) {
					result = new NomPreLM1D();					
				}
				
				result.setCodExito(lm1d.isCodExito());
				result.setCodigoOperacion(lm1d.getCodigoOperacion());
				
				if (lm1d.getDetalle() != null) {
					
					if (result.getDetalle() == null) {
						
						result.setDetalle(new ArrayList<Relacion>());
					}
					
					result.getDetalle().addAll(lm1d.getDetalle());
				}
				
				paginacion = lm1d.getNoEmpleadoPag();
				
			} while (paginacion != null && paginacion.length() > 0); 
			
			return result;
		}

}
