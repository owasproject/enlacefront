package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Clase para invocar la transaccion MPPC
 * @author FSW-Indra
 * @sice 26/02/2015
 *
 */
public class TrxMPPCBO extends CamposTrxMPPCBO {
	/** Constante para Log */
	private static final String LOGTAG = "[EDC-TDC] ::: TrxMPPCBO ::: ";
	
	/** Constante Mensaje OK Operacion Efectuada **/
	private static final String OK_OP_EFECTUADA = "AVMPA0166";
	
	/** Constante **/
	private static final String HAY_MAS_REGISTROS = "AVMPA8001";
	
	/** Constante formato Salida MPM0467 **/
	private static final String FOR_SAL_DCMPM0467 = "DCMPM0467";
	
	/** Constante Error MPE0128**/
	private static final String ERROR_MPE0128 = "ERMPE0128";
	
	/** Constante de Error MPE0013 **/
	private static final String ERROR_MPE0013 = "ERMPE0013";
	
	/** Constante para la etiqueta de @ E R*/
	private static final String ER = "@ER";
	
	/** Constante para separar tramas */
	private static final String SEPARADOR_TRAMAS = "@";
	/**
	 * ABEND_CICS ABEND CICS
	 */
	private static final String ABEND_CICS = "ABEND CICS";
	/**
	 * FAILED_WITH_ABEND cuando falle ABEND
	 */
	private static final String FAILED_WITH_ABEND = "failed with abend";
	/**
	 * tarjetaRespuesta de tipo String
	 */
	private String tarjetaRespuesta;
	/**
	 * CODIGO DE ENTIDAD
	 */
	private String codigoEntidad;
	/**
	 * CENTRO DE ALTA CUENTA
	 */
	private String centroAltaCuenta;
	/**
	 * CUENTA DE LA TARJETA 
	 */
	private String cuentaTdc;
	/**
	 * COD.MARCA TARJETA 
	 */
	private String codigoMarcaTarjeta;
	/**
	 * DESCR.DE LA MARCA
	 */
	private String descripcionMarca;
	
	/**
	 * Metodo encargado de realizar la ejecuci�n de la Transaccion MPPC.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 */
	public void ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxMPPCBO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxMPPCBO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",NivelLog.INFO);
		int codError = 0;
		String msjeError = "";
		if (tramaRespuesta.indexOf(SEPARADOR_TRAMAS+HAY_MAS_REGISTROS) > -1) { //"@AVMPA0166"
			try {
				tramaRespuesta = tramaRespuesta.substring( tramaRespuesta.indexOf("@DCMPM0467 P"), tramaRespuesta.length());
				tarjetaRespuesta = tramaRespuesta.substring(56,72);
			} catch (IndexOutOfBoundsException  e) {
				msjeError = " SE HA PRESENTADO ERROR EN LA RESPUESTA DCMPM0467 P ";
				return;
			}
		} else {
			codError = 99 ;
			if( tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO")>-1 ) {				
				msjeError = " NO HAY TERMINALES 390 PARA ATENDER LA PETICION" ;
				return;
			} else if( tramaRespuesta.indexOf(ABEND_CICS)>-1 || tramaRespuesta.indexOf(FAILED_WITH_ABEND)>-1 ) {
				msjeError = " TRANSACTION WITH ABEND";
				return;
			} else if(tramaRespuesta.indexOf(ER) > -1) {
				if(tramaRespuesta.indexOf(ERROR_MPE0128) > -1) { //"ERMPE0128"
					codError = 11 ;
					msjeError = " OPCION  ERRONEO. VALORES PERMITIDOS 1/2";
					return;
				} else if(tramaRespuesta.indexOf(ERROR_MPE0013) > -1) { //"ERMPE0013"
					codError = 12 ;
					msjeError = " NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION";
					return;
				} else {
					codError = 2;
					msjeError = " ERROR EN ServicioMPPC ";
					return;
				}
			} else if("".equals(tramaRespuesta.trim())) {
				msjeError = " TIMEOUT: SIN RESPUESTA 390";
				return;
			} else if(tramaRespuesta.indexOf(ABEND_CICS) > -1 || tramaRespuesta.indexOf(FAILED_WITH_ABEND) > -1) {
				msjeError = " " + traeCodError(tramaRespuesta);
				return;
			}
		}
		procesaEstatus(codError, msjeError , tramaEntrada);
	}
	
	/**
	 * Metodo privado que implmenta la invocaci�n a la transaccion, llena tramaRespuesta con el valor de la trama
	 * Regresa Verdadero si hay mas registros y si se procesaron y ya no hay mas registros.
	 * 		   Falso 	 si hubo un error al invocar la Trx.
	 * @author FSW - Indra
	 * @param tramaRespuesta trama de la invocacion anterior
	 * @return boolean : result
	 */
	private boolean invocaTrx(StringBuffer tramaRespuesta){
		StringBuffer tramaEntrada = new StringBuffer();
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxMPPCBO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", NivelLog.INFO);
		tramaRespuesta.append(invocarTransaccion(tramaEntrada.toString()));
		EIGlobal.mensajePorTrace("TrxMPPCBO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",NivelLog.INFO);
		int codError = 0;
		String msjeError = "";
		if (tramaRespuesta.indexOf("@AVMPA0166") > -1 || tramaRespuesta.indexOf("@AVMPA8001") > -1 ) {
			EIGlobal.mensajePorTrace(String.format("%s RESPONDIO LA TRANSACCION", LOGTAG), NivelLog.INFO);
			msjeError = " OPERACION EFECTUADA Ejecucion Exitosa con Socket";
			return true;
		} else {
			codError = 99;
			if( tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO")>-1 ) {
				msjeError = " NO HAY TERMINALES 390 PARA ATENDER LA PETICION";			
				return false;
			} else if( tramaRespuesta.indexOf(ABEND_CICS)>-1 || tramaRespuesta.indexOf(FAILED_WITH_ABEND)>-1 ) {
				msjeError = " TRANSACTION WITH ABEND";
				return false;
			} else if(tramaRespuesta.indexOf(ER) > -1) {
				if(tramaRespuesta.indexOf(ERROR_MPE0128) > -1) { //"ERMPE0128"
					codError = 11;
					msjeError = " OPCION  ERRONEO. VALORES PERMITIDOS 1/2";
					return false;
				} else if(tramaRespuesta.indexOf(ERROR_MPE0013) > -1) { //"ERMPE0013"
					codError = 12;
					msjeError = " NO EXISTE REGISTRO PARA EL CRITERIO DE SELECCION";
					return false;
				} else {
					codError = 2;
					msjeError = " ERROR EN ServicioMPPC ";
					return false;
				}
			} else if( "".equals(tramaRespuesta.toString().trim()) ) {
				msjeError = " TIMEOUT: SIN RESPUESTA 390";
				return false;
			} else if(tramaRespuesta.indexOf(ABEND_CICS) > -1 || tramaRespuesta.indexOf(FAILED_WITH_ABEND) > -1) {
				msjeError = " " + traeCodError(tramaRespuesta.toString());
				return false;
			}
		}		
		procesaEstatus(codError, msjeError, tramaEntrada);
		return false;
	}
	
	/**
	 * Metodo que sirve para obtener las Tarjetas relacionadas al contrato PAMPA
	 * @author FSW - Indra
	 * @param contrato20Pos : contrato20Pos
	 */
	public void consultaTarjetas(String contrato20Pos) {
		EIGlobal.mensajePorTrace(String.format("%s consultaTarjetas - Inicio", 
				LOGTAG ), NivelLog.DEBUG);
		
		StringBuffer tramaRespuesta = new StringBuffer();
		String[] tramaSeparada;
		String formatoSalida;
		String codigoOperacion=OK_OP_EFECTUADA;
		List<String> l = new ArrayList<String>();
		
		EIGlobal.mensajePorTrace(String.format("%s contrato20Pos:[%s]",
				LOGTAG,contrato20Pos),NivelLog.DEBUG);
		
		setCodigoEntidad( contrato20Pos.substring(0,4) );
		setCentroAltaCuenta( contrato20Pos.substring(4,8) );
		setCuentaTdc( contrato20Pos.substring(8,20) );
		setTipoCuenta("1");
		
		boolean bandera = true;
		while ( bandera && invocaTrx(tramaRespuesta) ) {
			EIGlobal.mensajePorTrace(String.format("%s while(invocaTrx) tramaRespuesta:[%s]"
					, LOGTAG,tramaRespuesta.toString()), NivelLog.DEBUG);
			tramaSeparada=tramaRespuesta.toString().substring(0, tramaRespuesta.length()-1).split("@");
			for (int i=0;i<tramaSeparada.length;i++) {
				formatoSalida = (tramaSeparada[i].length() > 10) ? tramaSeparada[i].substring(0,9) : tramaSeparada[i];
				if (OK_OP_EFECTUADA.equalsIgnoreCase(formatoSalida) || HAY_MAS_REGISTROS.equalsIgnoreCase(formatoSalida) ) {
					codigoOperacion=formatoSalida;
					EIGlobal.mensajePorTrace(String.format("%s tramaSeparada[%s]:[%s] CodigoOperacion:[%s] FormatoSalida:[%s]",
							LOGTAG,String.valueOf(i),tramaSeparada[i],codigoOperacion,formatoSalida), NivelLog.DEBUG);
				} else if (/*"DCMPM0467"*/FOR_SAL_DCMPM0467.equalsIgnoreCase(formatoSalida)) {					
					tarjetaRespuesta=tramaSeparada[i].substring(55,77);
					setFechaBaja( tramaSeparada[i].substring(147,157) );
					EIGlobal.mensajePorTrace(String.format("%s tarjetaRespuesta:[%s] fechaBaja:[%s]",
							LOGTAG,tarjetaRespuesta,getFechaBaja()), NivelLog.DEBUG);
					if ("0001-01-01".equals(getFechaBaja()) && i<(tramaSeparada.length-1)) {
						EIGlobal.mensajePorTrace(LOGTAG+"Se agrega tarjeta a la lista", NivelLog.DEBUG);
						l.add(tarjetaRespuesta);
					}					
				}	
			}
			//Almacena el ultimo numeroTDC para el rellamado
			setNumeroTDC( tarjetaRespuesta );
			//Limpia StringBuffer para el rellamado
			tramaRespuesta.setLength(0);
			
			if ("AVMPA0166".equalsIgnoreCase(codigoOperacion)){
				EIGlobal.mensajePorTrace(String.format("%s Se rompe ciclo; codigoOperacion:[%s]",
						LOGTAG,codigoOperacion), NivelLog.DEBUG);
				bandera = false;
			}
		}
		setTarjetas(l);
	} 
	
	
	
	/**
	 * Metodo para realizar la extraccion del Estatus sobre el resultado de la consulta.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param codigoStatus de tipo int
	 * @param msgError de tipo String
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void procesaEstatus(int codigoStatus, String msgError, StringBuffer tramaEntrada) {
		setCodStatus( codigoStatus );
		setMsgStatus( "MPPC" + rellenar(String.valueOf(getCodStatus()), 4, '0', 'I') + msgError );
		EIGlobal.mensajePorTrace(LOGTAG+"TrxMPPCBO.procesaEstatus: Enviando la trama: [" + tramaEntrada.toString() + "]", NivelLog.INFO);
		
		EIGlobal.mensajePorTrace(
			String.format("%s,codigoEntidad:[%s] centroAltaCuenta:[%s] cuentaTdc:[%s] codigoMarcaTarjeta:[%s] descripcionMarca:[%s] indicadorTipoTDC:[%s]\n"+
				"descripcionTipoReduc:[%s] numeroTDC:[%s] fechaAlta:[%s] indicadorSituacion:[%s] descripcionSituacion:[%s] nombreEstampacion:[%s]\n"+
				"codigoBloqueo:[%s] fechaBaja:[%s] descripcionBloqueo:[%s] tipoCuenta:[%s] numeroCliente:[%s]\n"+
				"codigoStatus:[%s], mensajeEstatus[%s], tarjetaRespuesta:[%s] ",
				LOGTAG,getCodigoEntidad(),getCentroAltaCuenta(),getCuentaTdc(),getCodigoMarcaTarjeta(),getDescripcionMarca(),getIndicadorTipoTDC()
				,getDescripcionTipoReduc(),getNumeroTDC(),getFechaAlta(),getIndicadorSituacion(),getDescripcionSituacion(),getNombreEstampacion()
				,getCodigoBloqueo(),getFechaBaja(),getDescripcionBloqueo(),getTipoCuenta(),getNumeroCliente()
				,getCodStatus(),getMsgStatus(),getTarjetaRespuesta() ) ,NivelLog.DEBUG);		
	}
	
	/**
	 * Metodo para obtener el Codigo de Error sobre la respuesta de la MOOC.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo String
	 * @return stringbuffer de tipo stringbuffer.toString
	 */
	private String traeCodError(String tramaEntrada)
	{
			StringBuffer stringbuffer = new StringBuffer("");
			if(null==tramaEntrada||("").equals(tramaEntrada)||tramaEntrada.indexOf(ER)==-1||tramaEntrada.indexOf("Transaction MPPC failed")==-1){
				return null;
			}
			int i = tramaEntrada.indexOf(ER);
			if(i < 0){
				i = tramaEntrada.indexOf("Transaction MPPC failed"); }
			if(i >= 0)
			{
				int j = i;
				boolean bandera = true;
				while(bandera){
					char c = tramaEntrada.charAt(j);
					if(c < ' ' || c > '~'){
						bandera=false; 
					} else if(j == (tramaEntrada.length()-1)){
						bandera=false;
					}
					stringbuffer = stringbuffer.append(String.valueOf(tramaEntrada.charAt(j)));
					j++;
				}
			} else
			{
				stringbuffer = stringbuffer.append("MPPC0000 Ejecucion Exitosa");
			}
			return stringbuffer.toString();
	}
	

	
	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	public void obtenerTramaEntrada(StringBuffer tramaEntrada) {
		
		tramaEntrada.append( armaCabeceraPS7("MPPC", "[PPC", 165, "00"));
		tramaEntrada.append( rellenar(getCodigoEntidad(),         4, ' ', 'I'));
		tramaEntrada.append( rellenar(getCentroAltaCuenta(),      4, ' ', 'I'));
		tramaEntrada.append( rellenar(getCuentaTdc(),            12, '0', 'I'));
		tramaEntrada.append( rellenar(getCodigoMarcaTarjeta(),    2, ' ', 'D'));
		tramaEntrada.append( rellenar(getDescripcionMarca(),     10, ' ', 'I'));
		tramaEntrada.append( rellenar(getIndicadorTipoTDC(),      2, ' ', 'D'));
		tramaEntrada.append( rellenar(getDescripcionTipoReduc(), 10, ' ', 'I'));
		tramaEntrada.append( rellenar(getNumeroTDC(),            22, ' ', 'I'));
		tramaEntrada.append( rellenar(getFechaAlta(),            10, ' ', 'I'));
		tramaEntrada.append( rellenar(getIndicadorSituacion(),    2, ' ', 'D'));
		tramaEntrada.append( rellenar(getDescripcionSituacion(), 30, ' ', 'I'));
		tramaEntrada.append( rellenar(getNombreEstampacion(),    26, ' ', 'I'));
		tramaEntrada.append( rellenar(getCodigoBloqueo(),         2, ' ', 'D'));
		tramaEntrada.append( rellenar(getFechaBaja(),            10, ' ', 'I'));
		tramaEntrada.append( rellenar(getDescripcionBloqueo(),   10, ' ', 'I'));
		tramaEntrada.append( rellenar(getTipoCuenta(),            1, ' ', 'I'));
		tramaEntrada.append( rellenar(getNumeroCliente(),         8, ' ', 'I'));

		EIGlobal.mensajePorTrace("TrxMPPCBO: Enviando la trama: [" + tramaEntrada.toString() + "]", NivelLog.INFO);
		
	}

	/**
	 * getTarjetaRespuesta de tipo String.
	 * @author FSW-Indra
	 * @return tarjetaRespuesta de tipo String
	 */
	public String getTarjetaRespuesta() {
		return tarjetaRespuesta;
	}

	/**
	 * setTarjetaRespuesta para asignar valor a tarjetaRespuesta.
	 * @author FSW-Indra
	 * @param tarjetaRespuesta de tipo String
	 */
	public void setTarjetaRespuesta(String tarjetaRespuesta) {
		this.tarjetaRespuesta = tarjetaRespuesta;
	}
	/**
	 * getCodigoEntidad de tipo String.
	 * @author FSW-Indra
	 * @return codigoEntidad de tipo String
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * setCodigoEntidad para asignar valor a codigoEntidad.
	 * @author FSW-Indra
	 * @param codigoEntidad de tipo String
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/**
	 * getCentroAltaCuenta de tipo String.
	 * @author FSW-Indra
	 * @return centroAltaCuenta de tipo String
	 */
	public String getCentroAltaCuenta() {
		return centroAltaCuenta;
	}
	/**
	 * setCentroAltaCuenta para asignar valor a centroAltaCuenta.
	 * @author FSW-Indra
	 * @param centroAltaCuenta de tipo String
	 */
	public void setCentroAltaCuenta(String centroAltaCuenta) {
		this.centroAltaCuenta = centroAltaCuenta;
	}
	/**
	 * getCuentaTdc de tipo String.
	 * @author FSW-Indra
	 * @return cuentaTdc de tipo String
	 */
	public String getCuentaTdc() {
		return cuentaTdc;
	}
	/**
	 * setCuentaTdc para asignar valor a cuentaTdc.
	 * @author FSW-Indra
	 * @param cuentaTdc de tipo String
	 */
	public void setCuentaTdc(String cuentaTdc) {
		this.cuentaTdc = cuentaTdc;
	}
	/**
	 * getCodigoMarcaTarjeta de tipo String.
	 * @author FSW-Indra
	 * @return codigoMarcaTarjeta de tipo String
	 */
	public String getCodigoMarcaTarjeta() {
		return codigoMarcaTarjeta;
	}
	/**
	 * setCodigoMarcaTarjeta para asignar valor a codigoMarcaTarjeta.
	 * @author FSW-Indra
	 * @param codigoMarcaTarjeta de tipo String
	 */
	public void setCodigoMarcaTarjeta(String codigoMarcaTarjeta) {
		this.codigoMarcaTarjeta = codigoMarcaTarjeta;
	}
	/**
	 * getDescripcionMarca de tipo String.
	 * @author FSW-Indra
	 * @return descripcionMarca de tipo String
	 */
	public String getDescripcionMarca() {
		return descripcionMarca;
	}
	/**
	 * setDescripcionMarca para asignar valor a descripcionMarca.
	 * @author FSW-Indra
	 * @param descripcionMarca de tipo String
	 */
	public void setDescripcionMarca(String descripcionMarca) {
		this.descripcionMarca = descripcionMarca;
	}
}
