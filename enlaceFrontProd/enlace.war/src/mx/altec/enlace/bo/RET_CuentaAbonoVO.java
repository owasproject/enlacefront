package mx.altec.enlace.bo;

/**
 *  Clase Bean para las Operaciones de abono de RR
 *
 * @author Daniel Hern�ndez P�rez
 * @version 1.0 Apr 02, 2012
 */

public class RET_CuentaAbonoVO implements java.io.Serializable{

	/**
	 * Serial Id Version
	 */
	private static final long serialVersionUID = 4935924544871021267L;

	/**
	 * Constante para indicar que la cuenta es Mismo Banco
	 */
	public static final String CUENTA_MISMO_BANCO = "MB";

	/**
	 * Constante para indicar que la cuenta es Internacional
	 */
	public static final String CUENTA_INTERNACIONAL = "IB";

	/**
	 * Tipo de cuenta de abono (misnmo banco, interbacaria(triangulada), internacionales)
	 */
	private String tipoCuenta = null;

	/**
	 * Descripci�n Tipo de cuenta de abono (misnmo banco, interbacaria(triangulada), internacionales)
	 */
	private String descTipoCuenta = null;

	/**
	 * Cuenta de abono
	 */
    private String numCuenta = null;

    /**
	 * Clave ABA o SWIFT para cuentas internacionales
	 */
    private String cveAbaSwift = null;

    /**
	 * Importe del abono a la cuenta
	 */
    private String importeAbono =  null;

    /**
	 * Nombre del beneficiario
	 */
    private String benefCuenta = null;

    /**
	 * Nombre del beneficiario
	 */
    private int idRegistro = 0;

    /**
	 * Folio de Operaci�n Cambios
	 */
    private String folioOperAbono = null;

    /**
	 * Folio de operaci�n Transfer
	 */
    private String folioTransfer = null;

    /**
	 * Estatus de la operaci�n
	 */
    private String estatusOperacion = null;

    /**
	 * Descripci�n del Estatus de la operaci�n
	 */
    private String descEstatusOperacion = null;

    /**
	 * Observaciones del envio de la operaci�n
	 */
    private String observacionesEnvio = null;

    /**
	 * Variable para indicar la baja Logica del registro
	 */
    private boolean bajaLogica = false;

    /**
	 * Obtener el valor de folioOperAbono.
     *
     * @return folioOperAbono valor asignado.
	 */
    public String getFolioOperAbono() {
		return folioOperAbono;
	}

	/**
     * Asignar un valor a folioOperAbono.
     *
     * @param folioOperAbono Valor a asignar.
     */
	public void setFolioOperAbono(String folioOperAbono) {
		this.folioOperAbono = folioOperAbono;
	}

    /**
	 * Obtener el valor de folioTransfer.
     *
     * @return folioTransfer valor asignado.
	 */
	public String getFolioTransfer() {
		return folioTransfer;
	}

	/**
     * Asignar un valor a folioTransfer.
     *
     * @param folioTransfer Valor a asignar.
     */
	public void setFolioTransfer(String folioTransfer) {
		this.folioTransfer = folioTransfer;
	}

    /**
	 * Obtener el valor de estatusOperacion.
     *
     * @return estatusOperacion valor asignado.
	 */
	public String getEstatusOperacion() {
		return estatusOperacion;
	}

	/**
     * Asignar un valor a estatusOperacion.
     *
     * @param estatusOperacion Valor a asignar.
     */
	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}

	/**
     * Asignar un valor a tipoCuenta.
     *
     * @param tipoCuenta Valor a asignar.
     */
    public void setTipoCuenta(String tipCta){
    	this.tipoCuenta = tipCta;
    }

    /**
	 * Obtener el valor de tipoCuenta.
     *
     * @return tipoCuenta valor asignado.
	 */
    public String getTipoCuenta(){
    	return this.tipoCuenta;
    }

    /**
     * Asignar un valor a numCuenta.
     *
     * @param numCuenta Valor a asignar.
     */
    public void setNumCuenta(String numCta){
    	this.numCuenta = numCta;
    }

    /**
	 * Obtener el valor de numCuenta.
     *
     * @return numCuenta valor asignado.
	 */
    public String getNumCuenta(){
    	return this.numCuenta;
    }

    /**
     * Asignar un valor a cveAbaSwift.
     *
     * @param cveAbaSwift Valor a asignar.
     */
    public void setCveAbaSwift(String abaSwift){
    	this.cveAbaSwift = abaSwift;
    }

    /**
	 * Obtener el valor de cveAbaSwift.
     *
     * @return cveAbaSwift valor asignado.
	 */
    public String getCveAbaSwift(){
    	return this.cveAbaSwift;
    }

    /**
     * Asignar un valor a importeAbono.
     *
     * @param importeAbono Valor a asignar.
     */
    public void setImporteAbono(String impAbon){
    	this.importeAbono = impAbon;
    }

    /**
	 * Obtener el valor de importeAbono.
     *
     * @return importeAbono valor asignado.
	 */
    public String getImporteAbono(){
    	return this.importeAbono;
    }

    /**
     * Asignar un valor a benefCuenta.
     *
     * @param benefCuenta Valor a asignar.
     */
    public void setBenefCuenta(String benef){
    	this.benefCuenta = benef;
    }

    /**
	 * Obtener el valor de benefCuenta.
     *
     * @return benefCuenta valor asignado.
	 */
    public String getBenefCuenta(){
    	return this.benefCuenta;
    }

    /**
     * Asignar un valor a idRegistro.
     *
     * @param idRegistro Valor a asignar.
     */
    public void setIdRegistro(int idRegistro){
    	this.idRegistro = idRegistro;
    }

    /**
	 * Obtener el valor de idRegistro.
     *
     * @return idRegistro valor asignado.
	 */
    public int getIdRegistro(){
    	return this.idRegistro;
    }

    /**
	 * Obtener el valor de observacionesEnvio.
     *
     * @return observacionesEnvio valor asignado.
	 */
	public String getObservacionesEnvio() {
		return observacionesEnvio;
	}

    /**
     * Asignar un valor a observacionesEnvio.
     *
     * @param observacionesEnvio Valor a asignar.
     */
	public void setObservacionesEnvio(String observacionesEnvio) {
		this.observacionesEnvio = observacionesEnvio;
	}

    /**
	 * Obtener el valor de bajaLogica.
     *
     * @return bajaLogica valor asignado.
	 */
	public boolean getBajaLogica() {
		return bajaLogica;
	}

    /**
     * Asignar un valor a bajaLogica.
     *
     * @param bajaLogica Valor a asignar.
     */
	public void setBajaLogica(boolean bajaLogica) {
		this.bajaLogica = bajaLogica;
	}

	/**
     * Asignar un valor a descEstatusOperacion.
     *
     * @param descEstatusOperacion Valor a asignar.
     */
	public String getDescEstatusOperacion() {
		if(estatusOperacion.trim().equals("P")){
			return "PACTADO";
		}else if(estatusOperacion.trim().equals("C")){
			return "COMPLEMENTADO";
		}else if(estatusOperacion.trim().equals("L")){
			return "ENVIADO";
		}else if(estatusOperacion.trim().equals("D")){
			return "CANCELADO";
		}else if(estatusOperacion.trim().equals("N")){
			return "ENVIADO CON ERRORES";
		}else if(estatusOperacion.trim().equals("O")){
			return "CONFIRMADO";
		}
		return "";
	}

    /**
	 * Obtener el valor de descTipoCuenta.
     *
     * @return descTipoCuenta valor asignado.
	 */
	public String getDescTipoCuenta() {
		if(tipoCuenta.trim().equals(CUENTA_MISMO_BANCO)){
			return "Mismo Banco";
		}else if(tipoCuenta.trim().equals(CUENTA_INTERNACIONAL)){
			return "Internacional";
		}
		return "";
	}

	/**
	 * Override toString
	 */
	public String toStringConDet(int color, String folioPadre) {
		StringBuffer registro = new StringBuffer();
		String rellenoHtml = "";
		String colorFila = "textabdatobs";

		if(color==1)
			colorFila = "textabdatcla";

		if(getEstatusOperacion().trim().equals("L") || getEstatusOperacion().trim().equals("O") ||
				getEstatusOperacion().trim().equals("D")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;" + folioPadre + "&nbsp;" );
			registro.append("<br></td>");
		}else{
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;&nbsp;&nbsp;" );
			registro.append("<br></td>");
		}
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFolioOperAbono() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getFolioTransfer() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getNumCuenta() + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + aMoneda(getImporteAbono()) + "&nbsp;" );
		registro.append("<br></td>");
		registro.append("<td class = \'"+ colorFila + "\' align=center>");
		registro.append("&nbsp;" + getDescTipoCuenta() + "&nbsp;" );
		registro.append("<br></td>");
		if(getEstatusOperacion().trim().equals("N")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("<font color=red>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("</font>");
			registro.append("<br></td>");
		}else if(getEstatusOperacion().trim().equals("L") || getEstatusOperacion().trim().equals("O")){
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("<font color=green>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("</font>");
			registro.append("<br></td>");
		}else{
			registro.append("<td class = \'"+ colorFila + "\' align=center>");
			registro.append("&nbsp;" + getDescEstatusOperacion() + "&nbsp;" );
			registro.append("<br></td>");
		}

		return registro.toString();
	}

	/**
	 * Metodo para formatear una cantidad a moneda
	 *
	 * @param num	-	Cadena con el numero a formatear
	 * @return		-	Formato de Cadena
	 */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}
}