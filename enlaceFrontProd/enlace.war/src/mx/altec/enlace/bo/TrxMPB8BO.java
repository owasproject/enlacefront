package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;


import mx.altec.enlace.dao.GenericDAO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * Clase para invocar la transaccion MPB8
 * @author FSW-Indra
 * @sice 26/02/2015
 *
 */
public class TrxMPB8BO extends GenericDAO {
	
	/**
	 * NOMBRE DE TRANSACCION
	 */
	private static final String NOMBRE_TRANSACCION = "MPB8";
	
	/**
	 * CODIGO DE ENTIDAD
	 */
	private String codigoEntidad;
	/**
	 * CENTRO DE ALTA CUENT  
	 */
	private String centroAltaCtaTarjeta;
	/**
	 * CUENTA DE LA TARJETA  
	 */
	private String cuentaTarjeta;
	/**
	 * NUMERO DE TARJETA     
	 */
	private String panTarjeta;
	/**
	 * TIPO DE CONSULTA    
	 */
	private String indicadorConsulta;
	/**
	 * CODIGO DE BLOQUEO  
	 */
	private String codigoBloqueo;
	/**
	 * IND.SITUACION TJT 
	 */
	private String indicadorSitTarjeta;
	/**
	 * FECHA DE BAJA
	 */
	private String fechaBaja;
	/**
	 * Mensaje de Estatus de la Transaccion
	 */
	private String msgStatus;
	/**
	 * Codigo de Estatus de le Transaccion
	 */
	private int codStatus;
	/**
	 * tarjetaRespuesta de tipo String
	 */
	private String tarjetaRespuesta;

	
	/**
	 * Metodo encargado de realizar la ejecución de la Transaccion MPB8.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 */
	public void ejecuta() {
		StringBuffer tramaEntrada = new StringBuffer();
		String tramaRespuesta;
		
		obtenerTramaEntrada(tramaEntrada);
		EIGlobal.mensajePorTrace("TrxMPB8BO - TRAMA DE ENVIO(): [" +tramaEntrada.toString()+"]", EIGlobal.NivelLog.INFO);
		tramaRespuesta = invocarTransaccion(tramaEntrada.toString());
		EIGlobal.mensajePorTrace("TrxMPB8BO - TRAMA DE RESPUESTA(): [" + tramaRespuesta+"]",EIGlobal.NivelLog.INFO);
		
		if(tramaRespuesta.indexOf("@DCMPM0405") > -1) {
			desentrama(tramaRespuesta);
			msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " Ejecucion Exitosa con Socket.";
		} else {
			if("".equals(tramaRespuesta.trim())) {
				codStatus = 99;
				msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " SIN RESPUESTA 390";
			}
			if(tramaRespuesta.indexOf("NO EXISTE  TERMINAL ACTIVO") > -1) {
				codStatus = 99;
				msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " NO HAY TERMINALES 390 PARA ATENDER LA PETICION";
			}
			if(tramaRespuesta.indexOf("ABEND CICS") > -1 || tramaRespuesta.indexOf("failed with abend") > -1) {
				codStatus = 99;
				msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " TRANSACTION WITH ABEND";
			}
			if(tramaRespuesta.indexOf("@ER") > -1) {
				if(tramaRespuesta.indexOf("EXISTEN ERRORES DE VALIDACION") > -1) {
					codStatus = 14;
					msgStatus = "EXISTEN ERRORES DE VALIDACION";
				}
				if(tramaRespuesta.indexOf("TARJETA INEXISTENTE") > -1) {
					codStatus = 34;
					msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " EL NUMERO DE TARJETA ES ERRONEO";
				}
				if(tramaRespuesta.indexOf("ERROR DE SISTEMA") > -1) {
					codStatus = 37;
					msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " TRANSACTION WITH ABEND";
				} else { 
					codStatus = 38;
					msgStatus = "Error al consultar Trx";
				}
			} else if(tramaRespuesta.indexOf("Transaction MPB8 failed") > -1) {
				codStatus = 37;
				msgStatus = NOMBRE_TRANSACCION + rellenar(String.valueOf(codStatus), 4, '0', 'I') + " TRANSACTION WITH ABEND";
			}
			procesaEstatus(codStatus, msgStatus);
		}
	}
	
	/**
	 * Metodo para realizar el desentramado de la respuesta.
	 * @since 27/02/2015
	 * @author FSW-Indra
	 * @param tramaRespuesta de tipo String
	 */
	private void desentrama(String tramaRespuesta) {
		
		int posIni = tramaRespuesta.indexOf("@DCMPM0405") + 12;
        codigoEntidad = subCadena(tramaRespuesta, posIni, 4);
        centroAltaCtaTarjeta = subCadena(tramaRespuesta, posIni + 4, 4);
        cuentaTarjeta = subCadena(tramaRespuesta, posIni + 8, 12);
        indicadorSitTarjeta = subCadena(tramaRespuesta, posIni + 86, 2);
        codigoBloqueo = subCadena(tramaRespuesta, posIni + 143, 2);
        fechaBaja = subCadena(tramaRespuesta, posIni + 197, 6);
        
        EIGlobal.mensajePorTrace("-- Obtener Datos tarjeta MPB8 --", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("codigoEntidad [" + codigoEntidad + "]" + "centroAltaCtaTarjeta [" + centroAltaCtaTarjeta + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("cuentaTarjeta [" + cuentaTarjeta + "]" + "indicadorSitTarjeta [" + indicadorSitTarjeta + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("codigoBloqueo [" + codigoBloqueo + "]" + "fechaBaja [" + fechaBaja + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para obtener la trama de Entrada.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param tramaEntrada de tipo StringBuffer
	 */
	private void obtenerTramaEntrada(StringBuffer tramaEntrada) {
		
		  tramaEntrada.append(armaCabeceraPS7("MPB8", "[PB8", 640, "00"));
	      tramaEntrada.append(rellenar("", 4,  ' ', 'D')).append(rellenar("", 4,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 12, ' ', 'D')).append(rellenar("", 12, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 6,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D')).append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar(panTarjeta, 22, ' ', 'D'));
	  	  tramaEntrada.append(rellenar("", 22, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D')).append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D')).append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 30, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 30, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 6,  ' ', 'D')).append(rellenar("", 6,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 6,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 3,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 40, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 20, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D')).append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D')).append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 4,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 26, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 30, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 2,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D')).append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D')).append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D')).append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 8,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 3,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 40, ' ', 'D')).append(rellenar("", 40, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 3,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 1,  ' ', 'D')).append(rellenar("", 1,  ' ', 'D'));
	      tramaEntrada.append(rellenar("", 26, ' ', 'D')).append(rellenar("", 26, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 19, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 19, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 10, ' ', 'D'));
	      tramaEntrada.append(rellenar("", 19, ' ', 'D'));

		EIGlobal.mensajePorTrace("TrxMPB8BO: Enviando la trama: [" + tramaEntrada.toString() + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para realizar la extraccion del Estatus sobre el resultado de la consulta.
	 * @since 27/02/2015
	 * @author FSW-Indra
	 * @param codigoStatus de tipo String
	 * @param msgError de tipo String
	 */
	private void procesaEstatus(int codigoStatus, String msgError) {
		msgStatus = "MPB8" + rellenar(String.valueOf(codigoStatus), 4, '0', 'I') + msgError;
		EIGlobal.mensajePorTrace("TrxMPB8BO.procesaEstatus: Enviando la trama: [" + msgStatus + "]", EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * Metodo para realizar un subString a una cadena.
	 * @since 27/02/2015
	 * @author FSW-Indra
	 * @param cadOrigen de tipo String
	 * @param posIni posicion Inicial
	 * @param tamano tamanio de la cadena a obtener
	 * @return respuesta de tipo String
	 */
	private String subCadena(String cadOrigen, int posIni, int tamano) {
        return cadOrigen.substring(posIni, posIni + tamano);
    }

	/**
	 * getCodigoEntidad de tipo String.
	 * @author FSW-Indra
	 * @return codigoEntidad de tipo String
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}

	/**
	 * setCodigoEntidad para asignar valor a codigoEntidad.
	 * @author FSW-Indra
	 * @param codigoEntidad de tipo String
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}

	/**
	 * getCentroAltaCtaTarjeta de tipo String.
	 * @author FSW-Indra
	 * @return centroAltaCtaTarjeta de tipo String
	 */
	public String getCentroAltaCtaTarjeta() {
		return centroAltaCtaTarjeta;
	}

	/**
	 * setCentroAltaCtaTarjeta para asignar valor a centroAltaCtaTarjeta.
	 * @author FSW-Indra
	 * @param centroAltaCtaTarjeta de tipo String
	 */
	public void setCentroAltaCtaTarjeta(String centroAltaCtaTarjeta) {
		this.centroAltaCtaTarjeta = centroAltaCtaTarjeta;
	}

	/**
	 * getCuentaTarjeta de tipo String.
	 * @author FSW-Indra
	 * @return cuentaTarjeta de tipo String
	 */
	public String getCuentaTarjeta() {
		return cuentaTarjeta;
	}

	/**
	 * setCuentaTarjeta para asignar valor a cuentaTarjeta.
	 * @author FSW-Indra
	 * @param cuentaTarjeta de tipo String
	 */
	public void setCuentaTarjeta(String cuentaTarjeta) {
		this.cuentaTarjeta = cuentaTarjeta;
	}

	/**
	 * getPanTarjeta de tipo String.
	 * @author FSW-Indra
	 * @return panTarjeta de tipo String
	 */
	public String getPanTarjeta() {
		return panTarjeta;
	}

	/**
	 * setPanTarjeta para asignar valor a panTarjeta.
	 * @author FSW-Indra
	 * @param panTarjeta de tipo String
	 */
	public void setPanTarjeta(String panTarjeta) {
		this.panTarjeta = panTarjeta;
	}

	/**
	 * getIndicadorConsulta de tipo String.
	 * @author FSW-Indra
	 * @return indicadorConsulta de tipo String
	 */
	public String getIndicadorConsulta() {
		return indicadorConsulta;
	}

	/**
	 * setIndicadorConsulta para asignar valor a indicadorConsulta.
	 * @author FSW-Indra
	 * @param indicadorConsulta de tipo String
	 */
	public void setIndicadorConsulta(String indicadorConsulta) {
		this.indicadorConsulta = indicadorConsulta;
	}

	/**
	 * getCodigoBloqueo de tipo String.
	 * @author FSW-Indra
	 * @return codigoBloqueo de tipo String
	 */
	public String getCodigoBloqueo() {
		return codigoBloqueo;
	}

	/**
	 * setCodigoBloqueo para asignar valor a codigoBloqueo.
	 * @author FSW-Indra
	 * @param codigoBloqueo de tipo String
	 */
	public void setCodigoBloqueo(String codigoBloqueo) {
		this.codigoBloqueo = codigoBloqueo;
	}

	/**
	 * getIndicadorSitTarjeta de tipo String.
	 * @author FSW-Indra
	 * @return indicadorSitTarjeta de tipo String
	 */
	public String getIndicadorSitTarjeta() {
		return indicadorSitTarjeta;
	}

	/**
	 * setIndicadorSitTarjeta para asignar valor a indicadorSitTarjeta.
	 * @author FSW-Indra
	 * @param indicadorSitTarjeta de tipo String
	 */
	public void setIndicadorSitTarjeta(String indicadorSitTarjeta) {
		this.indicadorSitTarjeta = indicadorSitTarjeta;
	}

	/**
	 * getFechaBaja de tipo String.
	 * @author FSW-Indra
	 * @return fechaBaja de tipo String
	 */
	public String getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * setFechaBaja para asignar valor a fechaBaja.
	 * @author FSW-Indra
	 * @param fechaBaja de tipo String
	 */
	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * getMsgStatus de tipo String.
	 * @author FSW-Indra
	 * @return msgStatus de tipo String
	 */
	public String getMsgStatus() {
		return msgStatus;
	}

	/**
	 * setMsgStatus para asignar valor a msgStatus.
	 * @author FSW-Indra
	 * @param msgStatus de tipo String
	 */
	public void setMsgStatus(String msgStatus) {
		this.msgStatus = msgStatus;
	}

	/**
	 * getCodStatus de tipo int.
	 * @author FSW-Indra
	 * @return codStatus de tipo int
	 */
	public int getCodStatus() {
		return codStatus;
	}

	/**
	 * setCodStatus para asignar valor a codStatus.
	 * @author FSW-Indra
	 * @param codStatus de tipo int
	 */
	public void setCodStatus(int codStatus) {
		this.codStatus = codStatus;
	}

	/**
	 * getTarjetaRespuesta de tipo String.
	 * @author FSW-Indra
	 * @return tarjetaRespuesta de tipo String
	 */
	public String getTarjetaRespuesta() {
		return tarjetaRespuesta;
	}

	/**
	 * setTarjetaRespuesta para asignar valor a tarjetaRespuesta.
	 * @author FSW-Indra
	 * @param tarjetaRespuesta de tipo String
	 */
	public void setTarjetaRespuesta(String tarjetaRespuesta) {
		this.tarjetaRespuesta = tarjetaRespuesta;
	}

}
