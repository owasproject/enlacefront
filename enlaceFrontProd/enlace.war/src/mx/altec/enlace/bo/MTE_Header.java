/*
Encabezado
Tipo de Header|Clave Emisora|Cod. Operacion|Fecha de Presentaci�n|    |Num. Headers|Importe Total     |Uso Futuro|
0                99001            60           20040312              01       000149     000000004483802000
Header
Tipo de Header|Num. Secuencia|Fecha de Aplicaci�n|Cod. Operaci�n|Banco|Importe Aplicar|CTA. Tesofe|Folio               |Num. Orden Inicial1|Num. Orden Inicial1|Beneficiario                  |RFC          |Uso Futuro|
1                000001               20040314         60          40014 000000000377700 65500032917 00000000000000000000 3221000024                              CAZARES CAZARES BLANCA CECILIA CACB770103U19
099001602004031301000149000000004483802000
100000120030315604001400000000037770065500032917000000000000000000003221000024                    CAZARES CAZARES BLANCA CECILIA                                                  CACB770103U19
*/

package mx.altec.enlace.bo;

import java.io.*;
import java.lang.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class MTE_Header
{
  public int tipoRegistro;
  public int claveEmisora;
  public int codigoOperacion;
  public GregorianCalendar fechaPresentacion;
  public long numeroRegistros;
  public double importeTotal;
  public String usoFuturo;

  public int err;
  public Vector listaErrores=new Vector();

  public void MTE_Header()
	{
	  tipoRegistro=0;
	  claveEmisora=0;
	  codigoOperacion=0;
	  fechaPresentacion=new GregorianCalendar();
	  numeroRegistros=0;
	  importeTotal=0.0;
	  usoFuturo="";

	  err=0;
	}

  public int parse(String linea)
	{
	    log("MTE_Header - parse(): Separando datos ...", EIGlobal.NivelLog.INFO);

		if(linea.length()>=42 && linea.length()<=215)
		 {
		   setTipoRegistro(linea.substring(0,1).trim());
		   setClaveEmisora(linea.substring(1,6).trim());
		   setCodigoOperacion(linea.substring(6,8).trim());
		   setFechaPresentacion(linea.substring(8,16).trim()); // +2 (sin uso).
		   setNumeroRegistros(linea.substring(18,24).trim());

		   setImporteTotal(linea.substring(24,42).trim());

		   setUsoFuturo(linea.substring(42).trim());
		 }
		else
		 {
		   log("MTE_Header - parse(): Error en el tama�o de la linea.", EIGlobal.NivelLog.INFO);
		   listaErrores.add("Longitud de linea erronea: "+ linea.length());
		   err=1;
		 }
		return err;
	}

  public GregorianCalendar getFechaStringDiagonal (String fecha)
	{
	  //log("MTE_Header - getFechaString(): Fecha: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==10)
			{
			  if( fecha.charAt(2)!='/' || fecha.charAt(5)!='/')
				{
				  log("MTE_Header - getFechaString(): Error en el formato de la fecha.", EIGlobal.NivelLog.INFO);
				  listaErrores.add("Formato de fecha de presentacion erroneo: "+ fecha);
				  err+=1;
				}
			  else
				{
				  int Dia = Integer.parseInt (fecha.substring(0,2));
				  int Mes = Integer.parseInt (fecha.substring(3,5))-1;
				  int Anio = Integer.parseInt (fecha.substring(6));
				  //log("MTE_Header - getFechaString(): Fecha: " + Dia + "-" + Mes + "-" + Anio, EIGlobal.NivelLog.INFO);
				  return new GregorianCalendar (Anio, Mes, Dia);
				}
			}
		  else
			{
			  log("MTE_Header - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Longitud de fecha de presentacion erronea: "+ fecha);
			  err+=1;
			}

		} catch (Exception e)
		 {
			log("MTE_Header - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de fecha de presentacion erroneo: "+ fecha);
			err+=1;
	     }
	   return null;
     }

    public GregorianCalendar getFechaString(String fecha)
	{
	  //log("MTE_Header - getFechaString(): Fecha: " + fecha, EIGlobal.NivelLog.INFO);
      try
		{
		  if(fecha.length()==8)
			{
			  int Anio = Integer.parseInt (fecha.substring(0,4));
			  int Mes = Integer.parseInt (fecha.substring(4,6))-1;
			  int Dia = Integer.parseInt (fecha.substring(6));
			  return new GregorianCalendar (Anio, Mes, Dia);
			}
		  else
			{
			  log("MTE_Header - getFechaString(): Error en la longitud de la fecha.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Longitud de fecha de presentacion erronea: "+ fecha);
			  err+=1;
			}

		} catch (Exception e)
		 {
			log("MTE_Header - getFechaString(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de fecha de presentacion erroneo: "+ fecha);
			err+=1;
	     }
	   return null;
     }

  public void setTipoRegistro(String cad)
	{
	   try
		{
	      tipoRegistro = new Integer(cad).intValue();
		  if(tipoRegistro!=0)
			{
			  log("MTE_Header - setTipoRegistro(): Error en el tipo de Registro.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("Tipo de Registro incorrecto: "+ cad);
			  err+=1;
			}
		}catch(Exception e)
		 {
			log("MTE_Header - setTipoRegistro(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de tipo de Registro erroneo: "+ cad);
			err+=1;
		 }
	}

  public void setClaveEmisora(String cad)
	{
	  try
		{
	      claveEmisora = new Integer(cad).intValue();
		}catch(Exception e)
		 {
			log("MTE_Header - setClaveEmisora(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de clave emisora erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setCodigoOperacion(String cad)
	{
	  try
		{
	      codigoOperacion = new Integer(cad).intValue();
		}catch(Exception e)
		 {
			log("MTE_Header - setCodigoOperacion(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de codigo de operacion erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setNumeroRegistros(String cad)
	{
	  try
		{
	      numeroRegistros = new Long(cad).longValue();
		}catch(Exception e)
		 {
			log("MTE_Header - setNumeroRegistros(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de numero de Registros erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setFechaPresentacion(String cad)
	{
	   fechaPresentacion=getFechaString(cad);
	}

   public void setImporteTotal(String cad)
	{
	   try
		{
	      importeTotal = new Double(cad).doubleValue();
		}catch(Exception e)
		 {
			log("MTE_Header - setImporteTotal(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			listaErrores.add("Formato de importe total erroneo: "+ cad);
			err+=1;
		 }
	}

   public void setUsoFuturo(String cad)
	{
      usoFuturo=cad.trim();
	}


   public void log(String a, EIGlobal.NivelLog b)
	{
	   EIGlobal.mensajePorTrace(a, b);
	}
}