package mx.altec.enlace.bo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.smartgwt.client.docs.Date;

import mx.altec.enlace.beans.ArchivoNominaBean;
import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.NominaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * 
 * @author FSW Vector
 * @since Marzo 2015 Clase Bussines Object para manejo de pagos
 */
public class NominaPagosBO {
	/**
	 * Constante texto log
	 */
	public static final String TEXTO_LOG = "NomimaPagosBO :: generaArchivoPagos :: ";
	/**
	 * Constante texto log
	 */
	public static final String TEXTOLOG = "Error al consultasr las cuentas de catalogo";
	/**
	 * Constante texto log
	 */
	public static final String TXT_ERROR = "Error ";
	
	
	/**
	 * 
	 * @param nombreArchivo
	 *            nombre el archivo a crear
	 * @param archivoNominaBean
	 *            Bean de
	 * @return boolean v/f
	 */

	public boolean generaArchivoPagos(String nombreArchivo,
			ArchivoNominaBean archivoNominaBean) {

		boolean resultado = false;
		FileWriter archivoTmp = null;
		BufferedWriter archivoEscritura = null;
		File archivo = null;
		LinkedHashMap<String, ArchivoNominaDetBean> detalleArchivo = null;
		ArchivoNominaDetBean detalleBean = null;
		Iterator<ArchivoNominaDetBean> it = null;
		BufferedWriter writer = null;
		StringBuffer linea = null;
		double impTotal = 0;
		int regTotal = 0;
		int numLinea = 1;
		long importe390;
		String[] fecha;
		String fchRecepcion;
		String fchEnvio;
		Date hoy = null;

		EIGlobal.mensajePorTrace(
				"NomimaPagosBO :: generaArchivoPagos ::  - Entrando",
				EIGlobal.NivelLog.INFO);

		if (archivoNominaBean != null) {
			if (archivoNominaBean.getDetalleArchivo() != null) {
				try {

					EIGlobal.mensajePorTrace(
							TEXTO_LOG
									+ " - Archivo + [" + nombreArchivo + "]",
							EIGlobal.NivelLog.INFO);

					archivo = new File(nombreArchivo);
					// si el archivo existe se borra
					if (archivo.exists()) {
						archivo.delete();
					}

					archivoTmp = new FileWriter(nombreArchivo);
					archivoEscritura = new BufferedWriter(archivoTmp);

					fecha = archivoNominaBean.getFechaEnvio().split("/");
					fchEnvio = fecha[1] + fecha[0] + fecha[2];

					linea = new StringBuffer();
					/* Generando Encabezado */
					linea.append('1');
					linea.append(rellenar(Integer.toString(numLinea), 5, '0',
							'I'));
					linea.append('E');
					linea.append(rellenar(fchEnvio, 8, ' ', 'D'));
					linea.append(rellenar(archivoNominaBean.getCtaCargo(), 16,
							' ', 'D'));
					linea.append(rellenar(fchEnvio, 8, ' ', 'D'));
					linea.append("\r\n");

					EIGlobal.mensajePorTrace(
							TEXTO_LOG
									+ " - Encabezado + [" + linea.toString()
									+ "]", EIGlobal.NivelLog.INFO);
					archivoEscritura.write(linea.toString());

					linea = null;

					detalleArchivo = archivoNominaBean.getDetalleArchivo();
					it = detalleArchivo.values().iterator();
					numLinea++;
					while (it.hasNext()) {
						detalleBean = it.next();
						importe390 = (long) (Math.rint(detalleBean.getImporte() * 100));

						linea = new StringBuffer();
						linea.append('2');
						linea.append(rellenar(Integer.toString(numLinea), 5,
								'0', 'I'));
						linea.append(rellenar(detalleBean.getIdEmpleado(), 7,
								' ', 'D'));
						linea.append(rellenar(detalleBean.getAppPaterno(), 30,
								' ', 'D'));
						linea.append(rellenar(detalleBean.getAppMaterno(), 20,
								' ', 'D'));
						linea.append(rellenar(detalleBean.getNombre(), 30, ' ',
								'D'));
						linea.append(rellenar(detalleBean.getCuentaAbono(), 16,
								' ', 'D'));
						linea.append(rellenar(Long.toString(importe390), 18,
								'0', 'I'));
						linea.append(rellenar(detalleBean.getTipoPago(), 2,
								'0', 'I'));
						linea.append("\r\n");

						EIGlobal.mensajePorTrace(
								TEXTO_LOG
										+ " - Detalle + [" + linea.toString()
										+ "]", EIGlobal.NivelLog.INFO);

						archivoEscritura.write(linea.toString());
						regTotal++;
						impTotal += detalleBean.getImporte();
						numLinea++;
						linea = null;
					}
					linea = new StringBuffer();
					importe390 = (long) (Math.rint(impTotal * 100));
					linea.append('3');
					linea.append(rellenar(Integer.toString(numLinea), 5, '0',
							'I'));
					linea.append(rellenar(Integer.toString(regTotal), 5, '0',
							'I'));
					linea.append(rellenar(Long.toString(importe390), 18, '0',
							'I'));
					linea.append("\r\n");

					EIGlobal.mensajePorTrace(
							TEXTO_LOG
									+ " - Sumario + [" + linea.toString() + "]",
							EIGlobal.NivelLog.INFO);
					archivoEscritura.write(linea.toString());
				} catch (NumberFormatException e) {
					resultado = false;
					EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
				} catch (FileNotFoundException e) {
					resultado = false;
					EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
				} catch (IOException e) {
					resultado = false;
					EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
				} finally {
					try {
						archivoEscritura.close();
					} catch (IOException e) {
						EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
					try {
						archivoTmp.close();
					} catch (IOException e) {
						EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
				}
			}
		}

		EIGlobal.mensajePorTrace(
				"NomimaPagosBO :: generaArchivoPagos ::  - Saliendo",
				EIGlobal.NivelLog.INFO);

		return resultado;
	}
	/**
	 * Metodo para consultar
	 * @param contrato contrato
	 * @return HashMap<String, ArchivoNominaDetBean> mapa consulta
	 * @throws BusinessException excepcion a manejar
	 */
	public HashMap<String, ArchivoNominaDetBean> consultaCtasCatalogo(
			String contrato) throws BusinessException {
		NominaDAO dao = new NominaDAO();
		HashMap<String, ArchivoNominaDetBean> ctasCatalogo = null;

		try {
			ctasCatalogo = dao.consultaCtasCatalogo(contrato);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(
					TEXTOLOG);
		}

		return ctasCatalogo;

	}

	/**
	 * Consulta de empleados
	 *
	 * @since 18/04/2015
	 * @param contrato
	 *            contrato en sesion
	 * @param cuenta
	 *            filtro cuenta cargo
	 * @param descripcion
	 *            descripcion cuenta
	 * @return Listado de cuentas empleado que coincidan con el filtro.
	 * @throws BusinessException
	 *             Excepcion de negocio
	 */
	public List<ArchivoNominaDetBean> consultaCtasCatalogo(String contrato,
			String cuenta, String descripcion) throws BusinessException {
		NominaDAO dao = new NominaDAO();
		List<ArchivoNominaDetBean> ctasCatalogo = null;

		try {
			ctasCatalogo = dao.consultaCtasCatalogo(contrato, cuenta,
					descripcion);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(TXT_ERROR+e.getMessage(), EIGlobal.NivelLog.ERROR);
			throw new BusinessException(
					TEXTOLOG);
		}

		return ctasCatalogo;

	}
	/**
	 * Metodo para crear pista
	 * @param request peticion
	 * @param idFlujo flujo
	 * @param idPaso numero de paso
	 * @return BitaTransacBean bean con informacion a registrar
	 */
	public BitaTransacBean creaPista(HttpServletRequest request,
			String idFlujo, String idPaso) {
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess
				.getAttribute("session");

		BitaHelper bh = new BitaHelperImpl(request, session, sess);

		bh.incrementaFolioFlujo(idFlujo);
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean) bh.llenarBean(bt);
		bt.setNumBit(idPaso);
		bt.setContrato(session.getContractNumber());
		bt.setIdFlujo(idFlujo);
		if (null != session.getToken().getSerialNumber()) {
			bt.setIdToken(session.getToken().getSerialNumber());
		}

		return bt;

	}
	/**
	 * Metodo para registrar pista
	 * @param bt bean informacion
	 */
	public void registraPista(BitaTransacBean bt) {

		if (("ON").equals(Global.USAR_BITACORAS.trim())) {
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"***NominaPagosBO.class &Error de excepcion SQL" + e,
						EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(
						"***NominaPagosBO.class &Error de excepcion " + e,
						EIGlobal.NivelLog.INFO);
			}
		}
	}
	/**
	 * Metodo para rellenar
	 * @param cad cadena
	 * @param lon longitud
	 * @param rel rel
	 * @param tip tip
	 * @return String cadena modificada
	 */
	public static String rellenar(String cad, int lon, char rel, char tip) {

		cad = cad == null ? "" : cad;

		if (cad.length() > lon)
			return cad.substring(0, lon);
		if (tip != 'I' && tip != 'D')
			tip = 'I';
		String aux = "";
		if (tip == 'D')
			aux = cad;
		for (int i = 0; i < (lon - cad.length()); i++)
			aux += "" + rel;
		if (tip == 'I')
			aux += "" + cad;
		return aux;
	}
	/**
	 * Metodo para generar combo pagos
	 * @param tiposPago tipos pago
	 * @param tipoSel tipo sel
	 * @return String cad
	 */
	public static String generaComboPagos(HashMap<String, String> tiposPago,
			String tipoSel) {

		Iterator it = null;
		String combo = "";

		if (tiposPago != null) {

			StringBuffer bf = new StringBuffer();
			it = tiposPago.values().iterator();

			while (it.hasNext()) {
				EIGlobal.mensajePorTrace("S", EIGlobal.NivelLog.INFO);
			}

		}

		return combo;

	}

	/**
	 * Lee el archivo de nomina y lo ordena dentro de un bean
	 *
	 * @param urlArchivo
	 *            URL del archivo
	 * @return BEan con la informacion del archivo.
	 */
	public static ArchivoNominaBean generarInfoArchivo(String urlArchivo) {

		EIGlobal.mensajePorTrace("Se va a pasar la infor del archivo: "
				+ urlArchivo + " a un bean", EIGlobal.NivelLog.INFO);
		ArchivoNominaBean archivoBean = new ArchivoNominaBean();
		String linea = "";
		String aux = "";
		int id = -1;
		int conta = 0;
		BufferedReader lector = null;
		try {
			lector = new BufferedReader(new FileReader(urlArchivo));

			while ((linea = lector.readLine()) != null) {
				conta++;
				aux = linea.substring(0, 1);
				id = Integer.parseInt(aux.trim());

				switch (id) {
				case 1: {
					obtenerInfoEncabezado(archivoBean, linea);
					break;
				}
				case 2: {
					obtenerInfoDetalle(archivoBean, linea, conta);
					break;
				}
				case 3: {
					obtenerInfoResumen(archivoBean, linea);
					break;
				}
				}

			}

		} catch (FileNotFoundException e) {
			EIGlobal.mensajePorTrace("Error al abrir el archivo:" + urlArchivo
					+ " con el siguiente mensaje: " + e.getMessage(),
					EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("Error al leer el archivo:" + urlArchivo
					+ " se tiene el siguiente mensaje: " + e.getMessage(),
					EIGlobal.NivelLog.INFO);
		} finally {
			if (lector != null) {
				try {
					lector.close();
				} catch (IOException e) {
					EIGlobal.mensajePorTrace(
							"Error al cerrar el archivo de lectura: "
									+ e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return archivoBean;

	}

	/**
	 * Obtiene la info resumen del archivo
	 *
	 * @param archivoBean
	 *            Bean con la informacion del archivo.
	 * @param linea
	 *            Linea del archivo.
	 */
	private static void obtenerInfoResumen(ArchivoNominaBean archivoBean,
			String linea) {
		String aux = "";
		EIGlobal.mensajePorTrace(
				"Se va a insertar la info de detalle siguiente:" + linea,
				EIGlobal.NivelLog.INFO);

		aux = linea.substring(6, 11);
		archivoBean.setNumRegistros(Integer.parseInt(aux.trim()));
		aux = linea.substring(11, 29);
		archivoBean.setImpTotal((Double.parseDouble(aux.trim()) / 100));

	}

	/**
	 * Obtiene la informacion de los archivos de detalle, los registros tipo 2
	 *
	 * @param archivoBean
	 *            bean con la info del archivo
	 * @param linea
	 *            Linea del archivo.
	 * @param numLinea
	 *            numero de Linea
	 */
	private static void obtenerInfoDetalle(ArchivoNominaBean archivoBean,
			String linea, int numLinea) {
		String aux = "";
		ArchivoNominaDetBean detalleBean = null;
		LinkedHashMap<String, ArchivoNominaDetBean> detalle = archivoBean
				.getDetalleArchivo();
		;
		EIGlobal.mensajePorTrace(
				"Se va a insertar la info de detalle siguiente:" + numLinea+"-"+ linea,
				EIGlobal.NivelLog.INFO);

		if (detalle == null) {
			detalle = new LinkedHashMap<String, ArchivoNominaDetBean>();
			archivoBean.setDetalleArchivo(detalle);
		}

		detalleBean = new ArchivoNominaDetBean();

		aux = linea.substring(1, 6);
		detalleBean.setSecuencia(Integer.parseInt(aux.trim()));
		aux = linea.substring(6, 13);
		detalleBean.setIdEmpleado(aux.trim());
		aux = linea.substring(13, 43);
		detalleBean.setAppPaterno(aux);
		aux = linea.substring(43, 63);
		detalleBean.setAppMaterno(aux);
		aux = linea.substring(63, 93);
		detalleBean.setNombre(aux);
		aux = linea.substring(93, 109);
		detalleBean.setCuentaAbono(aux.trim());
		aux = linea.substring(109, 127);
		EIGlobal.mensajePorTrace(
				"Importe DETALLE:" + aux,
				EIGlobal.NivelLog.INFO);
		detalleBean.setImporte((Double.parseDouble(aux.trim()) / 100));
		if (linea.length() == 129) {
			aux = linea.substring(127, 129);
		} else {
			aux = "";
		}
		detalleBean.setTipoPago(aux);

		detalle.put(""+numLinea, detalleBean);

		EIGlobal.mensajePorTrace("Terminando de procesar el archivo:"
				+ detalleBean.getSecuencia() + " --- de " + detalle.size(),
				EIGlobal.NivelLog.INFO);

	}

	/**
	 * Obtiene la informacion del encabezado del archivo y la inserta en el
	 * bean.
	 *
	 * @param archivoBean
	 *            Bean a llenar con la informacion del archivo.
	 * @param linea
	 *            Linea del archivo con la informacion del encabezado.
	 */
	private static void obtenerInfoEncabezado(ArchivoNominaBean archivoBean,
			String linea) {

		String aux = "";
		EIGlobal.mensajePorTrace(
				"Se va a insertar la info de encabezado siguiente:" + linea,
				EIGlobal.NivelLog.INFO);

		if (archivoBean == null) {
			archivoBean = new ArchivoNominaBean();
		}

		aux = linea.substring(7, 15);
		archivoBean.setFechaEnvio(aux.trim());
		aux = linea.substring(15, 31);
		archivoBean.setCtaCargo(aux.trim());
		aux = linea.substring(linea.length() - 8, linea.length());
		archivoBean.setFecha(aux.trim());

	}

}