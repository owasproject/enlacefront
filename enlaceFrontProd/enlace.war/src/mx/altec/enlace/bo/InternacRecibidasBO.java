package mx.altec.enlace.bo;

import java.io.FileWriter;
import java.io.IOException;

import mx.altec.enlace.beans.InterbancariasRecibidasBean;
import mx.altec.enlace.beans.InternacRecibidasBean;
import mx.altec.enlace.dao.InternacRecibidasDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 * Class InternacRecibidasBO.
 */
public class InternacRecibidasBO {


	/** The Constant COMA. */
	private static final String COMA = ",";


	/**
	 * Funcion para Exportar las Operaciones Internacionales Recibidas.
	 *
	 * @param bean Bean con la informacion a consultar
	 * @param String El usuario
	 * @return InternacRecibidasBean 
	 */
	public InternacRecibidasBean exportarRecibidas(String fecha, String cuenta, String referencia, String user){

		EIGlobal.mensajePorTrace("***InternacRecibidasBO :: consultarExportarRecibidas :: Inicio", EIGlobal.NivelLog.INFO);
		InternacRecibidasBean recibidasBean=null;

  		try{
  			InternacRecibidasDAO interDAO = new InternacRecibidasDAO();

			recibidasBean = new InternacRecibidasBean();

			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportarRecibidas :: Set del BEAN", EIGlobal.NivelLog.INFO);
			recibidasBean = interDAO.consultarRecibidas(fecha, cuenta, referencia);

			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportarRecibidas :: DATOS CONSULTA <"+recibidasBean.getLstTransferRecibidas().get(0).getReferencia()+">", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportarRecibidas :: CODIGO  <"+recibidasBean.getLstTransferRecibidas().get(0).getCodRespuesta()+">", EIGlobal.NivelLog.INFO);

			if("OK".equals(recibidasBean.getLstTransferRecibidas().get(0).getCodRespuesta())) {
				exportarDatos(recibidasBean.getLstTransferRecibidas().get(0), user);
			}else{
				EIGlobal.mensajePorTrace("***InternacRecibidasBO :: consultarExportarRecibidas :: Sin resultado", EIGlobal.NivelLog.INFO);
			}

  		}catch (IOException e) {
  			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: consultarExportarRecibidas:: Error al crear archivo "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return recibidasBean;
	}


	/**
	 * Genera el archivo con la informacion de la operacion.
	 *
	 * @param recibidasBean Informacion de entrada
	 * @param user Usuario
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void exportarDatos(InternacRecibidasBean recibidasBean, String user)
	throws IOException{

		EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportar :: Inicio ", EIGlobal.NivelLog.DEBUG);

		StringBuffer linea = null;
		FileWriter fileExport = new FileWriter(Global.DIRECTORIO_REMOTO_WEB + "/" + user + ".doc");
		EIGlobal.mensajePorTrace("***InternacRecibidasBO ::exportar:: Se creo el archivo.", EIGlobal.NivelLog.DEBUG);
		try{
			linea = new StringBuffer();

			linea.append(encabezadoExport());

			fileExport.write(linea.toString());
			for(int i=0; i<recibidasBean.getLstTransferRecibidas().size(); i++){
				InternacRecibidasBean beanExport = (InternacRecibidasBean)recibidasBean.getLstTransferRecibidas().get(i);
				linea = new StringBuffer();
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getReferencia(),20,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getCuentaOrdenante(),35,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getNombreOrdenante(),140,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getBancoOrdenante(), 140, ' ', 'D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getPaisOrdenante(), 19, ' ', 'D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getCiudadOrdenante(), 35, ' ', 'D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getImporte(),17,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getDivisa(),6,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getTipoCambio(),20,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getDescripcionEstatus(),20,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getFechaHora(),29,' ','D'));
				linea.append(COMA);
				linea.append(InterbancariasRecibidasBO.rellenar(beanExport.getConceptoPago(),210,' ','D'));
				linea.append(COMA);
				linea.append('\n');
			    fileExport.write(linea.toString());
			}
	
		}catch (IOException e2) {
			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportar :: IOException-> " + e2.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			fileExport.close();
			EIGlobal.mensajePorTrace("***InternacRecibidasBO :: exportar :: Archivo Finalizado.", EIGlobal.NivelLog.DEBUG);
		}
	}


	/**
	 * Encabezado export.
	 *
	 * @return the string
	 */
	private String encabezadoExport(){
		StringBuffer lineaEnca = null;

		lineaEnca = new StringBuffer();
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("NUMERO DE REFERENCIA", 20, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("CUENTA ORDENANTE", 35, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("NOMBRE DEL ORDENANTE", 140, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("BANCO ORDENANTE", 140, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("PAIS DEL ORDENANTE", 19, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("CIUDAD DEL ORDENANTE", 35, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("IMPORTE", 17, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("DIVISA", 6, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("TIPO DE CAMBIO", 20, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("ESTATUS", 20, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("FECHA Y HORA DE CONFIRMACION", 29, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append(InterbancariasRecibidasBO.rellenar("CONCEPTO DEL PAGO/TRANSFERENCIA", 210, ' ', 'D'));
		lineaEnca.append(COMA);
		lineaEnca.append('\n');

		return lineaEnca.toString();

	}
}