package mx.altec.enlace.bo;

import java.util.HashMap;
import java.util.Iterator;

import mx.altec.enlace.beans.NomPreLM1I;
import mx.altec.enlace.dao.NomPreConfiguracionDAO;
import mx.altec.enlace.utilerias.EIGlobal;


public class NomPreConfiguracionAction {
	
	public Double obtieneBin(String bin) {
		
		Double result = null;
								
		NomPreLM1I lm1i = obtenConfiguracionBeans();
		
		if (lm1i.getDetalle() != null) {
			
			result = Double.parseDouble(lm1i.getDetalle().get(bin));
		}
				
		return result;
	}
	
	public NomPreLM1I obtenConfiguracionBeans() 
	{
		NomPreConfiguracionDAO dao = new NomPreConfiguracionDAO();
		NomPreLM1I lm1i = null;
		NomPreLM1I result = null;
		String paginacion = "";									
		
		do{								
			lm1i = dao.obtenerMontoBean(paginacion);
			
			if (lm1i == null)  break;
			
			if (result == null) {
				result = new NomPreLM1I();				
			}
			
			result.setCodExito(lm1i.isCodExito());
			result.setCodigoOperacion(lm1i.getCodigoOperacion());
			
			if (lm1i.getDetalle() != null) {
				
				if(result.getDetalle() == null) {
					
					result.setDetalle(new HashMap<String, String>());
				}
				
				result.getDetalle().putAll(lm1i.getDetalle());
			}
			
			paginacion = lm1i.getBeanPaginacion();
			
		}while (paginacion != null && paginacion.length() > 0); 
		return result;
	}
	
	public Double obtieneMonto(String bin) {
		
		Double result = null;
		
		NomPreLM1I lm1i = obtenMontoBeans();
		
		if (lm1i.getDetalle() != null) {
			try{
				
				String valor = null;
				
				Iterator it = lm1i.getDetalle().keySet().iterator();
				
				while (it.hasNext()) {
					valor = (String) it.next();
					break;			
				}
				
				result = Double.parseDouble(valor.substring(0,15) + "." + valor.substring(15,17));
				String signo = valor.substring(17,18);
				if ("-".equals(signo)) {
					result = result * -1;
				}
				
			}catch(Exception e)
			{
				EIGlobal.mensajePorTrace("Eror " + e.getMessage(), EIGlobal.NivelLog.INFO);
				
				return 0.00;
			}
		}
				
		return result;
	}
	
	public NomPreLM1I obtenMontoBeans() 
	{
		NomPreConfiguracionDAO dao = new NomPreConfiguracionDAO();
		NomPreLM1I lm1i = null;
		NomPreLM1I result = null;
		String paginacion = "";									
		
		do{								
			lm1i = dao.obtenerMontoDisp(paginacion);
			
			if (lm1i == null)  break;
			
			if (result == null) {
				result = new NomPreLM1I();				
			}
			
			result.setCodExito(lm1i.isCodExito());
			result.setCodigoOperacion(lm1i.getCodigoOperacion());
			
			if (lm1i.getDetalle() != null) {
				
				if(result.getDetalle() == null) {
					
					result.setDetalle(new HashMap<String, String>());
				}
				
				result.getDetalle().putAll(lm1i.getDetalle());
			}
			
			paginacion = lm1i.getBeanPaginacion();
			
		}while (paginacion != null && paginacion.length() > 0); 
		return result;
	}
}