/**
 *
 */
package mx.altec.enlace.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.EmpleadoBean;
import mx.altec.enlace.beans.LMXC;
import mx.altec.enlace.beans.LMXE;
import mx.altec.enlace.beans.RemesasBean;
import mx.altec.enlace.beans.TarjetasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.AsignacionEmplDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.ServicioMensajesUSR;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


/**
 * @author Isban
 *
 */
public class AsignacionEmplBO extends BaseServlet {

	Map<Object, Object> mapa = new HashMap<Object, Object>();
	/**
	 * Tipo de nomina Nivel 3.
	 */
	private static String TIPO_NOMINA = "N3";

	/**
	 * Alta Individual.
	 */
	private static String ALTA_INDIVIDUAL = "I";
	/**
	 * Estatus con el que se guarda en BD.
	 */
	private static String ESTATUS_R = "R";
	/**
	 * Constante.
	 */
	private static Integer UN_REGISTRO = 1;
	private static String ALTA_ARCHIVO = "A";
	public static final String MEXICO = "MEXI";
	public static String MENSAJE=null;

	/**
	 * Almacena Numero de Tarjetas
	 * para validacion de Tarjetas Existentes
	 */
	static List<String> numTarjetas=null;
	static String []campoEncabezado={"Tipo de registro", "N&uacute;mero de secuencia", "Sentido", "Fecha de generaci&oacute;n"};
	static String []campoDetalle={"Tipo de registro", "N&uacute;mero de secuencia",
		"N&uacute;mero de empleado", "Apellido paterno del empleado", "Apellido materno del empleado",
		"Nombre del empleado", "Rfc del empleado", "Homoclave", "Tipo de identificaci&oacute;n",
		"N&uacute;mero de identificaci&oacute;n", "Sexo del empleado", "Fecha de nacimiento", "Clave de nacionalidad",
		"Pa&iacute;s de nacimiento", "Entidad federativa de personas nacidas en M&eacute;xico", "Entidad federativa de personas nacidas en el extranjero",
		"Estado civil", "Calle", "N&uacute;mero exterior",	"N&uacute;mero interior", "Colonia", "C&oacute;digo postal", "Prefijo del tel. de casa", "Tel&eacute;fono de casa",
		"Cp. centro de trabajo", "Sucursal tutora", 	"N&uacute;mero de tarjeta"};
	static String []campoSumario={"Tipo de registro", "N&uacute;mero de secuencia", "Total de registros"};
	static String[] tipoDocumento={"IF","CP","FM","ID","CM","IS","LC","LU","P","TM","SM","SS"};
	static String[] paises = {"MEXI", "USA", "CANA", "ESPA", "FRAN"};
	static String[] entidadesMex={"AS", "BC", "BS", "CC", "CH", "CL", "CM", "CS", "DF", "DG", "GR",
					"GT", "HG", "JC", "MC", "MN", "MS", "NL", "NT", "OC", "PL", "QR", "QT", "SL",
					"SP", "SR", "TC", "TL", "TS", "VZ", "YN", "ZS"};
	static String[] edoCivil = {"S", "C", "V", "U", "D"};

	private static int totalLineas=0;
	/**
	 * Lista de Tarjetas Validas.
	 */
	private static List<String> listaTarjetas = new ArrayList<String>();

	/**
	 * Relacion Tarjeta-Remesa
	 */
	private static HashMap<String, String> listaTarjetaRemesa = new HashMap<String, String>();





	public static String validaArchivo(File fArchivo, String contrato){
		try {
			MENSAJE="";//Se limpia la variable mensaje
			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaArchivo :: entra a validaArchivo(File fArchivo)", EIGlobal.NivelLog.DEBUG);
			BufferedReader br = new BufferedReader(new FileReader(fArchivo));
			String strEncabezado=null;
			String strDetalle=null;
			try {
				/*************************************************************************
				 *			VALIDACION REGISTRO DE ENCABEZADO
				 *************************************************************************/
				EIGlobal.mensajePorTrace("AsignacionEmplaBO:: validaArchivo :: Validando ENCABEZADO", EIGlobal.NivelLog.DEBUG);
				strEncabezado=br.readLine();

				if(strEncabezado == null){
					armaMensaje(0, "encabezado", "Error el archivo se encuentra vac&iacute;o", 1);
					EIGlobal.mensajePorTrace("AsignacionEmplaBO::ValidaArchivo:: el archivo esta vacio ", EIGlobal.NivelLog.DEBUG);
					return MENSAJE;
				}else {
					EIGlobal.mensajePorTrace("AsignacionEmplaBO::ValidaArchivo:: el archivo esta vacio y se brinca el if", EIGlobal.NivelLog.DEBUG);
				}

				if(strEncabezado.length() == 15){//Longitud encabezado
					validaEncabezado(strEncabezado);
				}else{
					armaMensaje(0, "encabezado", "Error en la longitud del registro.", 1);
				}

				/*************************************************************************
				 *			VALIDACION REGISTRO DE DETALLE
				 *************************************************************************/
				EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  validaArchivo  :: Validando DETALLE", EIGlobal.NivelLog.DEBUG);
				BufferedReader br2 = new BufferedReader(new FileReader(fArchivo));
				String longitud=null;
				int count=0;
				//Obtiene total de registros del archivo
				while ((longitud = br2.readLine()) != null) {
					if(longitud.length()!=0)
						count++;
				}

				int secuencia=2;
				totalLineas = count;
				int control=count-2;
				numTarjetas = new ArrayList<String>(); //Crea nueva Lista de Tarjetas para cada archivo

				while( control-- > 0 ){
					strDetalle=br.readLine();
					if(strDetalle.length() == 331){//Valida longitud del registro
						validaDetalle(strDetalle, secuencia, contrato);
					}else{
						armaMensaje(0, "detalle", "Error en la longitud del registro.", secuencia);
					}
					secuencia++;
				}

				/************************************************************************
				 *			VALIDACION REGISTRO DE SUMARIO
				 ************************************************************************/
				EIGlobal.mensajePorTrace("AsignacionEmplaBO::  validaArchivo :: Validando SUMARIO", EIGlobal.NivelLog.DEBUG);
				String strSumario=br.readLine();
				if(strSumario.length() == 11){//Longitud encabezado
					validaRegistroSumario(strSumario, count);
				}else{
					armaMensaje(0, "sumario", "Error en la longitud del registro.", count);
				}
				EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  validaArchivo :: Errores de archivo: "+MENSAJE.replaceAll("<br>", "\n"), EIGlobal.NivelLog.DEBUG);

			} catch (IOException e) {
				EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  validaArchivo :: Error en el archivo: "+e, EIGlobal.NivelLog.DEBUG);
			}
		} catch (FileNotFoundException e) {
			EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  validaArchivo  :: Error en el archivo: "+e, EIGlobal.NivelLog.DEBUG);
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  validaArchivo  :: Error"+e, EIGlobal.NivelLog.DEBUG);
		}


		return MENSAJE;
	}


	/**
	 * Enlace
	 * Metodo que devuelve la lista de Empleados
	 */
	public static List<EmpleadoBean> obtenerListEmpl(File file){
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: obtenerListEmpl :: entra a obtenerListEmpl(File file)", EIGlobal.NivelLog.DEBUG);
		AsignacionEmplBO asgEmpl = new AsignacionEmplBO();
		List<EmpleadoBean> lstEmpleados=new ArrayList<EmpleadoBean>();
		String strDetalle = null;
		EmpleadoBean empleadoBean = null;
		int control=totalLineas-2;

		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			br.readLine();//Se lee la primera linea para iterar el detalle

			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: obtenerListEmpl :: llenando empleadoBean", EIGlobal.NivelLog.DEBUG);
			while( control-- > 0 ){//Se iteran las lineas del detalle
				strDetalle=br.readLine();
				if(strDetalle.length() == 331){
					empleadoBean = new EmpleadoBean();
				    strDetalle = strDetalle.toUpperCase();

					empleadoBean.setTipoRegistro(strDetalle.substring(0, 1).trim());//[1]TIPO DE REGISTRO
					empleadoBean.setSecuencia(strDetalle.substring(1, 6).trim());//[2]NUMERO SECUENCIA
					empleadoBean.setNoEmpleado(strDetalle.substring(6, 13).trim());//[3]NUMERO EMPLEADO
					empleadoBean.setAppPaterno(strDetalle.substring(13, 33).trim());//[4]APE PATERNO
					empleadoBean.setAppMaterno(strDetalle.substring(33, 53).trim());//[5]APE MATERNO
					empleadoBean.setNombreEmpleado(strDetalle.substring(53, 93).trim());//[6]NOMBRE(S) DEL EMPLEADO
					empleadoBean.setRfc(strDetalle.substring(93, 103).trim());//[7]RFC EMPLEADO
					empleadoBean.setHomoclave(strDetalle.substring(103, 106).trim());//[8]HOMOCLAVE
					empleadoBean.setTipoDocto(strDetalle.substring(106, 108).trim());//[9]TIPO IDENTIFICACION
					empleadoBean.setNoIdentificacion(strDetalle.substring(108, 128).trim());//[10]NUMERO DE IDENTIFICACION
					 empleadoBean.setSexo(strDetalle.substring(128, 129).trim());//[11]SEXO
					 empleadoBean.setFechaNac(strDetalle.substring(129, 139).trim());//[12]FECHA DE NACIMIENTO
					 empleadoBean.setCveNacionalidad(strDetalle.substring(139, 143).trim());//[13]CLAVE DE NACIONALIDAD
					 empleadoBean.setPaisNacimiento(strDetalle.substring(143, 147).trim());//[14]PAIS DE NACIMIENTO

					 if (!empleadoBean.getPaisNacimiento().equals("MEXI")) {
							empleadoBean.setEntidadExtranjera(strDetalle.substring(149, 179).trim());//[16]ENTIDAD FEDERATIVA Personas nacidas en el Extranjero
							empleadoBean.setCveEstado("NE");

						} else {
							empleadoBean.setCveEstado(strDetalle.substring(147, 149).trim());//[15]ENTIDAD FEDERATIVA Personas nacidas en Mexico
							empleadoBean.setEntidadExtranjera(strDetalle.substring(149, 179).trim());
						}

					 empleadoBean.setEdoCivil(strDetalle.substring(179, 180).trim());//[17]ESTADO CIVIL
					 empleadoBean.setCalle(strDetalle.substring(180, 230).trim());//[18]CALLE
					 empleadoBean.setNoExterior(strDetalle.substring(230, 245).trim());//[19]NUMERO EXTERIOR
					 empleadoBean.setNoInterior(strDetalle.substring(245, 260).trim());//[20]NUMERO INTERIOR
					 empleadoBean.setColonia(strDetalle.substring(260, 290).trim());//[21]COLONIA
					 empleadoBean.setCodPostal(strDetalle.substring(290, 295).trim());//[22]CODIGO POSTAL
					 empleadoBean.setLada(strDetalle.substring(295, 298).trim());//[23]PREFIJO DE TEL DE CASA
					 empleadoBean.setTelefono(strDetalle.substring(298, 306).trim());//[24]TELEFONO DE CASA
					 empleadoBean.setCodPostalTrab(strDetalle.substring(306, 311).trim());//[25]CP CENTRO DE TRABAJO
					 empleadoBean.setSucursal(strDetalle.substring(311, 315).trim());//[26]SUCURSAL TUTORA
					 empleadoBean.setNumTarjeta(strDetalle.substring(315, 331).trim());//[27]NUMERO DE TARJETA
					 String remesa=asgEmpl.obtenerRemesa(empleadoBean.getNumTarjeta());
					 EIGlobal.mensajePorTrace("AsignacionEmplaDAO :: obtenerListEmpl :: REMESA ::::::::::::::"+remesa, EIGlobal.NivelLog.DEBUG);
					 empleadoBean.setRemesa(remesa); //NUMERO DE REMESA
					 empleadoBean.setEstatus("R"); //ESTATUS
					 lstEmpleados.add(empleadoBean);
				}
			}

			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaEncabezado :: fin de obtenerListEmpl(File file)", EIGlobal.NivelLog.DEBUG);

		} catch (IOException e) {
			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: obtenerListEmpl :: Error en el archivo: "+e, EIGlobal.NivelLog.DEBUG);
		}
		return lstEmpleados;
	}
	/**
	 * Enlace
	 * Metodo que valida el encabezado del archivo
	 */
	public static void validaEncabezado(String encabezado){
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: obtenerListEmpl :: entra a validaEncabezado(String encabezado)", EIGlobal.NivelLog.DEBUG);
		String encabezadoUpper = encabezado.toUpperCase();
		String strucEncabezado[] =new String[4];
		strucEncabezado[0] = encabezadoUpper.substring(0, 1); //TIPO REGISTRO
		strucEncabezado[1] = encabezadoUpper.substring(1, 6); //NUMERO SECUENCIA
		strucEncabezado[2] = encabezadoUpper.substring(6, 7); //SENTIDO
		strucEncabezado[3] = encabezadoUpper.substring(7, 15); //FECHA GENERACION

		/********* INICIO DE VALIDACIONES DE CAMPOS DE ENCABEZADO ************/
		for(int i=0; i<strucEncabezado.length; i++){//Valida campos requeridos
			if(strucEncabezado[i].trim().equals("")){
				armaMensaje(i+1, "encabezado", campoEncabezado[i]+", es requerido", 1);
			}
		}

		if(!strucEncabezado[0].trim().equals("") && !strucEncabezado[0].equals("1")){//Valida tipo de registro
			armaMensaje(1, "encabezado", "Error en el primer Caracter del encabezado, se esperaba 1.", 1);
		}if(!strucEncabezado[1].trim().equals("") && !strucEncabezado[1].equals("00001")){//Valida secuencia
			armaMensaje(2, "encabezado", "Error en el n&uacute;mero secuencial del encabezado, se esperaba 1.", 1);
		}if(!strucEncabezado[2].trim().equals("") && !strucEncabezado[2].equals("E")){//Valida sentido
			armaMensaje(3, "encabezado", "Error en el caracter de sentido del encabezado, se esperaba E.", 1);
		}
		String fechaInvertida=strucEncabezado[3].substring(2,4)+strucEncabezado[3].substring(0,2)+strucEncabezado[3].substring(4,8);
		if(!strucEncabezado[3].trim().equals("") && fechaValida(fechaInvertida)==false){//Valida  fecha
			armaMensaje(4, "encabezado", "Error en la fecha de generacion del archivo.", 1);
		}
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaEncabezado :: fin de validaEncabezado(String encabezado)", EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Enlace
	 * Metodo que valida el encabezado del archivo
	 */
	public static void validaDetalle(String strCamposDetalle, int secuencia,  String contrato){
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaDetalle :: entra a validaDetalle(String strCamposDetalle, int secuencia,  String contrato)", EIGlobal.NivelLog.DEBUG);
		AsignacionEmplDAO daoConsulta = AsignacionEmplDAO.getSingleton();

		try {
			daoConsulta.initTransac();

			String strDetalle = strCamposDetalle.toUpperCase();
			String detalle[]=new String[27];
			detalle[0] = strDetalle.substring(0, 1).trim();//[1]TIPO DE REGISTRO
			detalle[1] = strDetalle.substring(1, 6).trim();//[2]NUMERO SECUENCIA
			detalle[2] = strDetalle.substring(6, 13).trim();//[3]NUMERO EMPLEADO
			detalle[3] = strDetalle.substring(13, 33).trim();//[4]APE PATERNO
			detalle[4] = strDetalle.substring(33, 53).trim();//[5]APE MATERNO
			detalle[5] = strDetalle.substring(53, 93).trim();//[6]NOMBRE(S) DEL EMPLEADO
			detalle[6] = strDetalle.substring(93, 103).trim();//[7]RFC EMPLEADO
			detalle[7] = strDetalle.substring(103, 106).trim();//[8]HOMOCLAVE
			detalle[8] = strDetalle.substring(106, 108).trim();//[9]TIPO IDENTIFICACION
			detalle[9] = strDetalle.substring(108, 128).trim();//[10]NUMERO DE IDENTIFICACION
			detalle[10] = strDetalle.substring(128, 129).trim();//[11]SEXO
			detalle[11] = strDetalle.substring(129, 139).trim();//[12]FECHA DE NACIMIENTO
			detalle[12] = strDetalle.substring(139, 143).trim();//[13]CLAVE DE NACIONALIDAD
			detalle[13] = strDetalle.substring(143, 147).trim();//[14]PAIS DE NACIMIENTO
			detalle[14] = strDetalle.substring(147, 149).trim();//[15]ENTIDAD FEDERATIVA Personas nacidas en Mexico
			detalle[15] = strDetalle.substring(149, 179).trim();//[16]ENTIDAD FEDERATIVA Personas nacidas en el Extranjero
			detalle[16] = strDetalle.substring(179, 180).trim();//[17]ESTADO CIVIL
			detalle[17] = strDetalle.substring(180, 230).trim();//[18]CALLE
			detalle[18] = strDetalle.substring(230, 245).trim();//[19]NUMERO EXTERIOR
			detalle[19] = strDetalle.substring(245, 260).trim();//[20]NUMERO INTERIOR
			detalle[20] = strDetalle.substring(260, 290).trim();//[21]COLONIA
			detalle[21] = strDetalle.substring(290, 295).trim();//[22]CODIGO POSTAL
			detalle[22] = strDetalle.substring(295, 298).trim();//[23]PREFIJO DE TEL DE CASA
			detalle[23] = strDetalle.substring(298, 306).trim();//[24]TELEFONO DE CASA
			detalle[24] = strDetalle.substring(306, 311).trim();//[25]CP CENTRO DE TRABAJO
			detalle[25] = strDetalle.substring(311, 315).trim();//[26]SUCURSAL TUTORA
			detalle[26] = strDetalle.substring(315, 331).trim();//[27]NUMERO DE TARJETA

			/********* INICIO DE VALIDACIONES DE CAMPOS DE DETALLE ************/
			/**************************** VALIDA CAMPOS REQUERIDOS *****************************/
			boolean reqMexi=false;//Identificara si ya se mostro mensaje de requerido para nacidas en mexico
			for(int campo=0; campo<detalle.length; campo++){//Valida campos requeridos
				if(detalle[campo].equals("") && campo!=7 && campo!=2 && campo!=14 && campo!=15 && campo!=19 ){//Se descarta registro 7 y 2 debido a que no son requeridos
					armaMensaje(campo+1, "detalle", campoDetalle[campo]+", es requerido", secuencia);
				}
				if(campo==13 && detalle[13].equals("MEXI") && detalle[14].equals("")){//Validar persona nacida en Mexico
					armaMensaje(campo+2, "detalle", campoDetalle[campo+1]+", es requerido", secuencia);
					reqMexi=true;//controla si es requerido ya no muestra otro mensaje para el campo personas nacidas en mexico
				}
				if(campo==13 && !detalle[13].equals("MEXI") && detalle[15].equals("")){//Validar persona nacida en Extranjero
					armaMensaje(campo+3, "detalle", campoDetalle[campo+2]+", es requerido", secuencia);
				}
				EIGlobal.mensajePorTrace("["+campo+"]" + " DETALLE["+detalle[campo]+"]["+campoDetalle[campo]+"]", EIGlobal.NivelLog.DEBUG);
			}

			/**************************** VALIDACIONES ESPECIFICAS DE CAMPO *****************************/
			if(!detalle[0].trim().equals("") && !detalle[0].equals("2")){//Valida tipo de registro
				armaMensaje(1, "detalle", "Error en el primer caracter del detalle, se esperaba 2.", 1);
			}
			if (!detalle[1].trim().equals("") && (validaDigito(detalle[1])==false || Integer.parseInt(detalle[1]) != secuencia) ){//valida secuencia
				armaMensaje(2, "detalle", "Error en el n&uacute;mero secuencial del detalle.", secuencia);
			}
			if (validaDigito(detalle[2])==false ){//Valida numero de empleado
				armaMensaje(3, "detalle", "Error en el "+campoDetalle[2]+", debe ser num&eacute;rico", secuencia);
			}
			if (!detalle[3].trim().equals("") && !validaCampoTexto(detalle[3]) ){//Valida apellido paterno
				armaMensaje(4, "detalle", "Error en el "+campoDetalle[3]+", s&oacute;lo se permiten caracteres A-Z, la � no est� permitida", secuencia);
			}
			if (!detalle[4].trim().equals("") && !validaCampoTexto(detalle[4]) ){//Valida apellido materno
				armaMensaje(5, "detalle", "Error en el "+campoDetalle[4]+", s&oacute;lo se permiten caracteres A-Z, la � no est� permitida", secuencia);
			}
			if (!detalle[5].trim().equals("") && !validaCampoTexto(detalle[5]) ){//Valida nombre
				armaMensaje(6, "detalle", "Error en el "+campoDetalle[5]+", s&oacute;lo se permiten caracteres A-Z, la � no est� permitida", secuencia);
			}
			if(!detalle[6].trim().equals("") && !validaRFC(detalle[5], detalle[3], detalle[4], detalle[6]) ){
				armaMensaje(7, "detalle", "Error en el "+campoDetalle[6], secuencia);
			}
			if ( !detalle[7].equals("") && (!validaCampoAlfanumerico(detalle[7]) || detalle[7].length()!= 3) ){//Valida homoclave
				armaMensaje(8, "detalle", "Error en la "+campoDetalle[7]+", debe ser alfanum&eacute;rico, sin caracteres especiales y a 3 posiciones", secuencia);
			}
			Arrays.sort(tipoDocumento);//Ordenar al array para usar el binarySearch
			if (!detalle[8].trim().equals("") && (detalle[8].length()>2 || Arrays.binarySearch(tipoDocumento,detalle[8]) < 0)){//Valida tipo de identificacion
				armaMensaje(9, "detalle", "Error en el "+campoDetalle[8], secuencia);
			}
			if (!validaCampoAlfanumerico(detalle[9])){//Valida numero de identificacion
				armaMensaje(10, "detalle", "Error en el "+campoDetalle[9]+", debe ser alfanum&eacute;rico y sin caracteres especiales", secuencia);
			}
			if ( !detalle[10].trim().equals("") && (!detalle[10].equals("H") && !detalle[10].equals("M")) ){//Valida Sexo
				armaMensaje(11, "detalle", "Error en el "+campoDetalle[10]+", debe ser H o M", secuencia);
			}
			//Valida fecha de nacimiento
			try{
				if(!detalle[11].trim().equals("")){
					String fechaNaci[] = detalle[11].split("-");
					SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
					String strDate = sdf.format(new Date());
					String fechaAct[] = strDate.split("/");
					if (!detalle[11].trim().equals("") && fechaValida(fechaNaci[1]+fechaNaci[2]+fechaNaci[0])==false){
						armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", es inv&aacute;lida", secuencia);
					}else if(calculaEdad(fechaNaci[2]+"/"+fechaNaci[1]+"/"+fechaNaci[0]) < 16){
						armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", el empleado es menor a 16 a�os", secuencia);
					}else if(Integer.parseInt(fechaAct[2]) - Integer.parseInt(fechaNaci[0]) > 100){
						armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", la diferencia del a�o actual y el a�o de nacimiento no debe ser superior a 100", secuencia);
					}else if(!comparaFechas(detalle[11])){
						armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", la fecha de nacimiento no debe ser mayor a la fecha actual", secuencia);
					}else if(!validaFechNaciRfc(detalle[11], detalle[6])){
						armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", no coincide con el rfc", secuencia);
					}
				}
			}catch (Exception e) {
				armaMensaje(12, "detalle", "Error en la "+campoDetalle[11]+", es incorrecta", secuencia);
			}
			Arrays.sort(paises);//Ordenar al array para usar el binarySearch
			if (!detalle[12].trim().equals("") && Arrays.binarySearch(paises,detalle[12]) < 0){//Valida Nacionalidad
				armaMensaje(13, "detalle", "Error en la "+campoDetalle[12], secuencia);
			}
			Arrays.sort(paises);//Ordenar al array para usar el binarySearch
			if (!detalle[13].trim().equals("") && Arrays.binarySearch(paises,detalle[13]) < 0){//Valida Pais de Nacimiento
				armaMensaje(14, "detalle", "Error en el "+campoDetalle[13], secuencia);
			}
			Arrays.sort(entidadesMex);//Ordenar al array para usar el binarySearch || la variable reqMexi identifica ya se mostro mensaje de requerido
			if( detalle[13].equals("MEXI") && (Arrays.binarySearch(entidadesMex,detalle[14]) < 0 && reqMexi==false)  ){//Valida entidad federativa para personas nacidas en mexico
				armaMensaje(15, "detalle", "Error en el "+campoDetalle[14], secuencia);
			}
			if( (detalle[13].equals("CANA") || detalle[13].equals("USA") || detalle[13].equals("ESPA")
					|| detalle[13].equals("FRAN")) && !detalle[14].equals("NE") ){//Valida entidad federativa para personas nacidas en el extranjero
				armaMensaje(15, "detalle", "Error en la entidad federativa, debe ser NE", secuencia);
			}
			if (!detalle[15].trim().equals("") && !validaCampoTexto(detalle[15]) ){//alida entidad federativa para personas nacidas en el extranjero
				armaMensaje(6, "detalle", "Error en el "+campoDetalle[15]+", ssss&oacute;lo se permiten caracteres A-Z, la � no est� permitida", secuencia);
			}
			Arrays.sort(edoCivil);//Ordenar al array para usar el binarySearch
			if(!detalle[16].trim().equals("") && Arrays.binarySearch(edoCivil,detalle[16]) < 0  ){//Valida estao civil
				armaMensaje(17, "detalle", "Error en "+campoDetalle[16]+", se esperaba S = Soltero, C = Casado, V = Viudo, U = Uni&oacute;n libre, D = Divorciado", secuencia);
			}
			if ( (!detalle[17].trim().equals("") && !validaCampoAlfanumerico(detalle[17]) )
				|| detalle[17].equals("DOMICILIO CONOCIDO") ){//Valida calle
				armaMensaje(18, "detalle", "Error en el "+campoDetalle[17]+", s&oacute;lo se permiten caracteres A-Z de 0 a 9 y la � no est� permitida", secuencia);
			}
			if( !detalle[18].trim().equals("") && !validaCampoAlfanumerico(detalle[18]) ){//Valida numero exterior
				armaMensaje(19, "detalle", "Error en el "+campoDetalle[18]+", s&oacute;lo se permiten caracteres A-Z de 0 a 9 y la � no est� permitida", secuencia);
			}
			if( !validaCampoAlfanumerico(detalle[19]) ){//Valida numero interior
				armaMensaje(20, "detalle", "Error en el "+campoDetalle[19]+", s&oacute;lo se permiten caracteres A-Z de 0 a 9 y la � no est� permitida", secuencia);
			}
			if( !detalle[20].trim().equals("") && !validaCampoAlfanumerico(detalle[20]) ){//Valida colonia
				armaMensaje(21, "detalle", "Error en el "+campoDetalle[20]+", s&oacute;lo se permiten caracteres A-Z de 0 a 9 y la � no est� permitida", secuencia);
			}
			if( !detalle[21].trim().equals("") &&  (validaDigito(detalle[21])==false || detalle[21].length() > 5) ){//Valida codigo postal
				armaMensaje(22, "detalle", "Error en el "+campoDetalle[21]+", debe ser num&eacute;rico y no mayor a 5 posiciones.", secuencia);
			}
			if( !detalle[22].trim().equals("") && validaDigito(detalle[22])==false ){//Valida prefijo de tel de casa
				armaMensaje(23, "detalle", "Error en el "+campoDetalle[22]+", debe ser num&eacute;rico.", secuencia);
			}
			if(!detalle[23].trim().equals("") &&  (validaDigito(detalle[23])==false || !validaTelefono(detalle[23])) ){
				armaMensaje(24, "detalle", "Error en el "+campoDetalle[23]+", debe ser num&eacute;rico y no debe tener 7 veces el mismo n&uacute;mero.", secuencia);
			}
			if( !detalle[24].trim().equals("") &&  (validaDigito(detalle[24])==false || detalle[24].length() > 5) ){//Valida codigo postal trabajo
				armaMensaje(25, "detalle", "Error en el "+campoDetalle[24]+", debe ser num&eacute;rico y no mayor a 5 posiciones.", secuencia);
			}
			if( !detalle[25].trim().equals("") &&  validaDigito(detalle[25])==false ){//Valida sucursal tutora
				armaMensaje(26, "detalle", "Error en la "+campoDetalle[25]+", debe ser num&eacute;rico y no mayor a 5 posiciones.", secuencia);
			}
			
			boolean hayError=false;
			if( !detalle[26].trim().equals("") &&  (validaDigito(detalle[26])==false || detalle[26].length() != 16) ){//Valida tarjeta
				armaMensaje(27, "detalle", "Error en el "+campoDetalle[26]+", debe ser num&eacute;rico y no mayor a 5 posiciones.", secuencia);
				hayError=true;
			}
			//if( !detalle[26].trim().equals("") &&  !numTarjetas.contains(detalle[26]) ){//Valida si la tarjeta se encuentra repetida en el archivo
			if( numTarjetas.contains(detalle[26]) ){//Valida si la tarjeta se encuentra repetida en el archivo
				armaMensaje(27, "detalle", "Error en el "+campoDetalle[26]+", La tarjeta se encuentra repetida", secuencia);
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::validaDetalle:: La tarjeta esta repetida " + detalle[26]
				                                                     								+ " NO se puede asignar.--->"
				                                                     								+ numTarjetas.contains(detalle[26])
				                                                     								, EIGlobal.NivelLog.DEBUG);
				hayError=true;
			}else{
				numTarjetas.add(detalle[26]); //En caso contrario se almacena para proximas busquedas
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::validaDetalle:: La tarjeta no esta repetida " + detalle[26]
				                                                     								+ " Se puede asignar.--->" 
				                                                     								+ numTarjetas.contains(detalle[26])
				                                                     								, EIGlobal.NivelLog.DEBUG);
			}

			//INICIO DE VALIDACIONES INTEGRIDAD TARJETA
			if(Global.VALIDA_NOMINA_INMEDIATA.trim().equals("ON") && hayError==false){
				if(listaTarjetas.size()>0 ){
					if(listaTarjetas.contains(detalle[26])){
						EIGlobal.mensajePorTrace("AsignacionEmplServlet::valiARchivo:: La tarjeta " + detalle[26]
								+ " se puede asignar."
								, EIGlobal.NivelLog.DEBUG);
						//emp.setRemesa((String)this.listaTarjetaRemesa.get(detalle[26]));//FIXME Ya no se requiere porque se setea en el metodo obtenerListEmpl(fArchivo)
					}else{
						EIGlobal.mensajePorTrace("AsignacionEmplServlet::valiARchivo:: La tarjeta " + detalle[26]
								+ " NO se puede asignar."
								, EIGlobal.NivelLog.DEBUG);
						armaMensaje(27, "detalle", "Error en la "+campoDetalle[26]+", No es posible asignar la tarjeta, no esta disponible.", secuencia);
						hayError = true;
					}
				}else{
					armaMensaje(27, "detalle", "Error en la "+campoDetalle[26]+", No es posible asignar la tarjeta, no esta disponible.", secuencia);
					hayError = true;
				}

				if(!hayError){
					if(daoConsulta.verificaTarjetaAsignacion(contrato, detalle[26])){
						EIGlobal.mensajePorTrace("AsignacionEmplServlet::valiARchivo:: La tarjeta ya se encuentra Enviada"
								, EIGlobal.NivelLog.DEBUG);
						armaMensaje(27, "detalle", "Error en el "+campoDetalle[26]+", La tarjeta ya fue enviada el d�a de hoy.", secuencia);
						hayError = true;
					}
				}
			}
			//FIN DE VALIDACIONES INTEGRIDAD TARJETA

			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaDetalle :: fin de validaDetalle(String strCamposDetalle, int secuencia,  String contrato)", EIGlobal.NivelLog.DEBUG);
		}catch (Exception e) {
			armaMensaje(0, "archivo", "Error al validar el archivo.", secuencia);
			EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaEncabezado :: Error al validar el archivo.....", EIGlobal.NivelLog.DEBUG);
		}finally{
			daoConsulta.closeTransac();
		}
	}

	/**
	 * Enlace
	 * Metodo que valida el registro sumario del archivo
	 */
	public static void validaRegistroSumario(String sumario, int count){
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: validaRegistroSumario :: entra a validaRegistroSumario(String sumario, int count)", EIGlobal.NivelLog.DEBUG);
		String sumarioUpper = sumario.toUpperCase();
		String strucSumario[] =new String[3];
		strucSumario[0] = sumarioUpper.substring(0, 1); //REGISTRO SUMARIO
		strucSumario[1] = sumarioUpper.substring(1, 6); //NUMERO SECUENCIA
		strucSumario[2] = sumarioUpper.substring(6, 11); //TOTAL DE REGISTROS

		/********* INICIO DE VALIDACIONES DE CAMPOS DE ENCABEZADO ************/
		for(int i=0; i<strucSumario.length; i++){//Valida campos requeridos
			if(strucSumario[i].trim().equals("")){
				armaMensaje(i+1, "encabezado", campoSumario[i]+", es requerido", 1);
			}
		}

		if(!strucSumario[0].trim().equals("") && !strucSumario[0].equals("3")){//Valida tipo de registro
			armaMensaje(1, "sumario", "Error en el primer Caracter del sumario, se esperaba 3.", count);
		}if (!strucSumario[1].trim().equals("") && (validaDigito(strucSumario[1])==false || Integer.parseInt(strucSumario[1]) != count) ){//valida secuencia
			armaMensaje(2, "sumario", "Error en el n&uacute;mero secuencial del sumario.", count);
		}if(!strucSumario[2].trim().equals("")  && Integer.parseInt(strucSumario[2])!=count-2){//Valida total de registros
			armaMensaje(3, "sumario", "Error en el total de registros, se esperaba "+(count-2)+".", count);
		}

		EIGlobal.mensajePorTrace("AsignacionEmplBO :: validaRegistroSumario :: fin de validaRegistroSumario(String sumario, int count)", EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Enlace
	 * Metodo arma el mensaje de errores
	 */
	public static void armaMensaje(int campo, String seccion, String error, int registro){
		EIGlobal.mensajePorTrace("AsignacionEmplaBO :: armaMensaje :: entra a armaMensaje(int campo, String seccion, String error, int registro)", EIGlobal.NivelLog.DEBUG);
		MENSAJE += (campo==0?"":"Campo " +campo)+". 'Registro de "+seccion+"'"
			+ " "+error+" Registro No. "+registro
			+ "<br>";
	}

	/**************************************************************
	 *** Metodo encargado de validar una cadena de caracteres *** de tipo
	 * alfanumerico ***
	 **************************************************************/
	public static boolean validaCampoTexto(String campo) {

		String caracteresPermitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + " ";
		boolean campoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < campo.length(); i++) {
			letra = campo.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == campo.length())
			campoValido = true;

		return campoValido;

	}

	/**************************************************************
	 *** Metodo encargado de validar una cadena de caracteres *** de tipo
	 * alfanumerico ***
	 **************************************************************/
	public static boolean validaCampoAlfanumerico(String campo) {

		String caracteresPermitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" + " ";
		boolean campoValido = false;
		int carValido = 0;
		char letra;

		for (int i = 0; i < campo.length(); i++) {
			letra = campo.charAt(i);
			for (int j = 0; j < caracteresPermitidos.length(); j++) {
				if (letra == caracteresPermitidos.charAt(j))
					carValido++;
			}
		}

		if (carValido == campo.length())
			campoValido = true;

		return campoValido;

	}

	/**************************************************************
	 *** Metodo encargado de validar una cadena de caracteres *** de tipo
	 * alfanumerico ***
	 **************************************************************/
	static boolean validaTelefono(String num){
		boolean valid = true;
		for(int i=0; i<num.length(); i++){
			int count = 1;
			for(int j=0; j<num.length(); j++){
				if(num.charAt(i) == num.charAt(j) && i!=j)
					count +=1;
			}
			if(count >= 7)
				valid =  false;
		}
		return valid;
	}

	/*
     * El metodo comparaFechas(String strFecha)
     * Valida que la fecha del parametro no sea mayor a la fecha actual
     */
	static boolean comparaFechas(String strFecha){
		Date dateCurrent = new Date();
		 SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		 Date fecha = null;
		 try {
		     fecha = formatoDelTexto.parse(strFecha);
		 } catch (ParseException ex) {
		     ex.printStackTrace();
		 }
		 return dateCurrent.compareTo(fecha) == -1 ? false : true;

	}

	/*
     * El metodo validaDigito(String cadena)
     * Valida que la cadena sea digito
     */
	public static boolean validaDigito(String cadena){
			//EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo validaDigito&", EIGlobal.NivelLog.INFO);
		String caracteresValidos="0123456789";
		char chr = '0';

	 for (int i = 0; i<cadena.length(); i++) {
			chr = cadena.charAt(i);
			if (caracteresValidos.indexOf(chr) == -1) {
				//EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &El concepto contiene caracteres no validos&", EIGlobal.NivelLog.INFO);
				return false;
			}
		}
		return true;
	} // fin del metodo validaDigito

    private static boolean validaRFC(String strNombre, String strApPaterno, String strApMaterno, String strRfc)
    {
    	int a, anio, mes, dia;
        char car;
        boolean correcto = true;
        int let = 4;

	        StringBuffer resultado = new StringBuffer();
	        String nombre = strNombre.toUpperCase();
	        String pat = strApPaterno.toUpperCase();
	        String mat = strApMaterno.toUpperCase();
	        String rfc1 = strRfc.toUpperCase().substring(0, 4);
	        StringBuffer aux = new StringBuffer();
	        String token;
	        if(mat.equals("")) mat = strRfc.substring(2, 3);
	        nombre=quitaPrep(nombre);
	        pat=quitaPrep(pat);
	        mat=quitaPrep(mat);
	        if (pat.length()==0 || nombre.length()==0 || mat.length()==0) return false;
	        resultado.append(pat.charAt(0));
	        for (a=1;a<pat.length();a++)
	            if (pat.charAt(a)== 'A' || pat.charAt(a)=='E' || pat.charAt(a)=='I' || pat.charAt(a)=='O' || pat.charAt(a)=='U' || pat.charAt(a)=='&')
	                aux.append(pat.charAt(a));
	        if (aux.length()==0)
	            {if (pat.length()>1) resultado.append(pat.charAt(1)); else resultado.append(rfc1.charAt(1));}
	        else
	            resultado.append(aux.charAt(0));
	        resultado.append(mat.charAt(0));
	        if(nombre.trim().indexOf(" ") == -1)
	            {
	            resultado.append(nombre.charAt(0));
	            }
	        else
	            {
	            aux = new StringBuffer();
	            try
	                {
	                StringTokenizer tokens = new StringTokenizer(nombre);
	                while(tokens.hasMoreTokens())
	                    {
	                    token = tokens.nextToken();
	                    if (!(token.equals("MARIA") || token.equals("MA") || token.equals("MA.") || token.equals("JOSE")))
	                        aux.append(" " + token);
	                    }
	                }
	            catch(Exception e)
	                {
	                //EIGlobal.mensajePorTrace("Error en ProveedorConf.java: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
	                }
	            if(aux.length()>0) resultado.append(aux.charAt(1)); else resultado.append(strRfc.charAt(3));
	            }
	        token = resultado.toString();
	        if (token.equals("BUEI") || token.equals("BUEY") || token.equals("CACA") ||
	            token.equals("CACO") || token.equals("CAGA") || token.equals("CAGO") ||
	            token.equals("CAKA") || token.equals("CAKO") || token.equals("COGE") ||
	            token.equals("COJA") || token.equals("COJE") || token.equals("COJI") ||
	            token.equals("COJO") || token.equals("CULO") || token.equals("FETO") ||
	            token.equals("GUEY") || token.equals("JOTO") || token.equals("KACA") ||
	            token.equals("KACO") || token.equals("KAGA") || token.equals("KAGO") ||
	            token.equals("KOGE") || token.equals("KOJO") || token.equals("KAKA") ||
	            token.equals("KULO") || token.equals("LOCA") || token.equals("LOCO") ||
	            token.equals("LOKA") || token.equals("LOKO") || token.equals("MAME") ||
	            token.equals("MAMO") || token.equals("MEAR") || token.equals("MEAS") ||
	            token.equals("MEON") || token.equals("MION") || token.equals("MOCO") ||
	            token.equals("MULA") || token.equals("PEDA") || token.equals("PEDO") ||
	            token.equals("PENE") || token.equals("PUTA") || token.equals("PUTO") ||
	            token.equals("QULO") || token.equals("RATA") || token.equals("RUIN"))
	            resultado.replace(3,4,"X");
	        correcto = (resultado.toString().equals(rfc1));

	    try
	        {
	        anio = 1900 + Integer.parseInt(strRfc.substring(let,let+2));
	        mes = Integer.parseInt(strRfc.substring(let+2,let+4)) - 1;
	        dia = Integer.parseInt(strRfc.substring(let+4,let+6));
	        if(mes < 0 || mes > 11) correcto = false;
	        GregorianCalendar fecha = new GregorianCalendar(anio,mes, 4);
			if(mes==02 || mes==2){
				if(dia > (fecha.getActualMaximum(Calendar.DAY_OF_MONTH)+1) ){
					correcto = false;
				}
			}else  if(dia > fecha.getActualMaximum(Calendar.DAY_OF_MONTH)){
				correcto = false;
			}
	        }
	    catch(Exception e)
	        {correcto = false;}
	    return correcto;
    }

    private static String quitaPrep(String cadena)
    {
	    StringBuffer buffer = new StringBuffer();
	    String token;
	    //EIGlobal.mensajePorTrace("ProveedorConf.class - Modif. Quita Prep. vers 1.0 4 de Marzo 2005 .", EIGlobal.NivelLog.INFO);
	    try
	        {
	        StringTokenizer tokens = new StringTokenizer(cadena);
	        while(tokens.hasMoreTokens())
	            {
	            token = tokens.nextToken();
	            if (!(token.equals("") || token.equals("DEL") || token.equals("DE") || token.equals("LA") ||
	                token.equals("LOS") || token.equals("LAS") || token.equals("Y") || token.equals("MC") ||
	                token.equals("MAC") || token.equals("VON") && token.equals("VAN")))
	                buffer.append(" " + token);
	            }
	        }catch(Exception e){
	        //EIGlobal.mensajePorTrace("Error en ProveedorConf.java: " + e.getMessage(),EIGlobal.NivelLog.DEBUG);
	        }
	      if (buffer.length()>1)
		  {
		    return buffer.toString().substring(1);
		  }
	      else {
		   return "";
		  }
	}


    public static boolean fechaValida(String fecha){
		EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class &Entrando al metodo fechaValida&"+ fecha, EIGlobal.NivelLog.INFO);
	    String fechaParaValidar;
		int month=0;
	    int day=0;
		int year=0;
	    boolean diaValido, mesValido, yearValido;
		boolean fechaCorrecta =false;
	    fechaParaValidar= fecha;

	    yearValido=true;

	    try{
		    month=Integer.parseInt(fechaParaValidar.substring(0,2));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
			mesValido=false;
	    }
		try{
			day=Integer.parseInt(fechaParaValidar.substring(2,4));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
			diaValido=false;
	    }
		try{
			year=Integer.parseInt(fechaParaValidar.substring(4,8));
	    }
		catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("***ValidaArchivoPagos.class Ha ocurrido una excepcion en: %fechaValida()"+e, EIGlobal.NivelLog.INFO);
			yearValido=false;
	    }

	/*	java.util.Date midia = new java.util.Date();
		System.out.println("ValidaArchivoPagos.class "+((int)(midia.getYear()+1900)));
        if (  year <(midia.getYear()+1900) ||year !=(midia.getYear()+1900)  ){
			System.out.println("La fecha fue Igual o mayor ok");
            yearValido= false;
        }
*/
/*
        if (year<1980 || year > 2002){// quitar esto para tomar cualquier a�o
            yearValido= false;        //
        }                             //
        else{*/
//            yearValido=true;
        //}
        if (month<1 || month>12){
            mesValido=false;
        }
        else{
            mesValido=true;
        }

        if(day < 1 || day > regresaDiaMes(month, year)){
        	diaValido=false;
        }else{
        	diaValido=true;
        }

        if (yearValido && mesValido && diaValido){
            fechaCorrecta= true;
        }
        else
            fechaCorrecta =false;

		if (fechaCorrecta)
			return true;
		else
			return false;
	 }//fin del metodo fechaValida

    /**
	 * GAE
	 * @param month		Mes a probar
	 * @param year		A�o a probar
	 * @return numDias  Metodo que regresa el dia final de un mes en cualquier
	 * a�o, tomando en cuenta que el a�o sea bisiesto
	 */
	public static int regresaDiaMes(int month, int year) {
		int numDias = 0;
		if ((year % 4) == 0) {
			switch (month) {
				case 2:
					numDias = 29;
					break;
				case 4:
					numDias = 30;
					break;
				case 6:
					numDias = 30;
					break;
				case 9:
					numDias = 30;
					break;
				case 11:
					numDias = 30;
					break;
				default:
					numDias = 31;
					break;
			}
		} else {
			switch (month) {
				case 2:
					numDias = 28;
					break;
				case 4:
					numDias = 30;
					break;
				case 6:
					numDias = 30;
					break;
				case 9:
					numDias = 30;
					break;
				case 11:
					numDias = 30;
					break;
				default:
					numDias = 31;
					break;
			}
		}
		return numDias;
	}

	/**
	 * Metodo para generar la informaci�n de las tarjetas de PAMPA
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void generaInfoTarjetas( HttpServletRequest req, HttpServletResponse res)
			throws Exception{
			EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas :: Entrando a generaInfoTarjetas.", EIGlobal.NivelLog.INFO);

			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			LMXC entradaLmxc = new LMXC();
			LMXC salidaLmxc  = new LMXC();
			LMXE entradaLmxe = new LMXE();
			LMXE salidaLmxe  = new LMXE();
			AdmonRemesasBO service = new AdmonRemesasBO();
			RemesasBean remesa = new RemesasBean();

			try{
				entradaLmxc.setContratoEnlace(session.getContractNumber());
				EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas:: Contrato->" + entradaLmxc.getContratoEnlace(), EIGlobal.NivelLog.INFO);

				salidaLmxc = service.consultaRemesas(entradaLmxc, true);
				if (salidaLmxc != null && salidaLmxc.getListaRemesas() != null && salidaLmxc.getListaRemesas().size() > 0) {
					EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas:: Numero de remesas->" + salidaLmxc.getListaRemesas().size(), EIGlobal.NivelLog.INFO);
					Iterator<RemesasBean> it = salidaLmxc.getListaRemesas().iterator();
					while (it.hasNext()){
						remesa = it.next();
						entradaLmxe.setContratoEnlace(session.getContractNumber());
						entradaLmxe.setCtroDistribucion(remesa.getCentroDistribucion());
						entradaLmxe.setNumeroRemesa(remesa.getNumRemesa());
						entradaLmxe.setEstadoTarjeta("R");
						entradaLmxe.setTarjeta("");
						salidaLmxe = service.consultaDetalleRemesa(entradaLmxe, true);
						if(salidaLmxe.getListaTarjetas()!=null){
							for(int i=0; i<salidaLmxe.getListaTarjetas().size(); i++){
								this.listaTarjetas.add(((TarjetasBean)salidaLmxe.getListaTarjetas().get(i)).getTarjeta());
								this.listaTarjetaRemesa.put(((TarjetasBean)salidaLmxe.getListaTarjetas().get(i)).getTarjeta(), "" + Integer.parseInt(remesa.getNumRemesa()));
							}
						}
					}
				}
				EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas:: Numero de tarjetas->" + this.listaTarjetas.size(), EIGlobal.NivelLog.INFO);

			}catch(Exception e){
				EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas:: Hubo un error al generar la informacion->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
			EIGlobal.mensajePorTrace("AsignacionEmplBO::generaInfoTarjetas:: Saliendo de generaInfoTarjetas", EIGlobal.NivelLog.INFO);
		}

	public String obtenerRemesa(String numTarjeta){
		return this.listaTarjetaRemesa.get(numTarjeta);
	}

	/**
	 * Metodo encargado de validar e invocar la inserci�n en BD.
	 *
	 * @param empleadoBean
	 *            bean
	 * @param descContrato
	 *            descripcion del contrato
	 * @return
	 */
	public String invocaAsignacionTarjeta(EmpleadoBean empleadoBean,
			String descContrato, HttpServletRequest req, HttpServletResponse res) {

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("AsignacionEmplBO  invocaAsignacionTarjeta "
				+ empleadoBean.getNombreEmpleado(), NivelLog.INFO);

		List<String> lstNombre = new ArrayList<String>();
		String nombreCompleto = empleadoBean.getAppPaterno() + " "
				+ empleadoBean.getAppMaterno() + " "
				+ empleadoBean.getNombreEmpleado() ;

		if (empleadoBean.getSexo().equals("Masculino")) {
			empleadoBean.setSexo("H");
		} else {
			empleadoBean.setSexo("M");
		}

		if (!empleadoBean.getPaisNacimiento().equals(MEXICO)) {
			empleadoBean.setEntidadExtranjera(empleadoBean
					.getColoniaExtranjero());
			empleadoBean.setCveEstado("NE");

		} else {
			empleadoBean.setEntidadExtranjera(" ");
		}

		if(empleadoBean.getNoEmpleado()!=null
				|| !empleadoBean.getNoEmpleado().trim().equals("")){
			EIGlobal.mensajePorTrace("AsignacionEmplBO::invocaAsignacionTarjeta:: Num Empleado CAPTURADO->"
					+ empleadoBean.getNoEmpleado().trim() + "<"
					, NivelLog.DEBUG);
		}else{
			empleadoBean.setNoEmpleado(" ");
		}

		empleadoBean.setCveUsuario((String) session.getUserID8());
		empleadoBean.setEstatus(ESTATUS_R);
		AsignacionEmplDAO asignacionEmplDAO = AsignacionEmplDAO.getSingleton();

		Map<String, String> mapExito = asignacionEmplDAO.altaEmpleado(
				empleadoBean, empleadoBean.getUsuarioSession(),
				ALTA_INDIVIDUAL, TIPO_NOMINA, UN_REGISTRO, UN_REGISTRO);

		if (mapExito.get("mensaje").equals("EXITO")) {
			EIGlobal.mensajePorTrace("AsignacionEmplBO  Envia Mail "
					+ empleadoBean.getNombreEmpleado(), NivelLog.INFO);
			lstNombre.add(nombreCompleto);
			EmailDetails mensaje = new EmailDetails(empleadoBean.getContrato(),
					descContrato, mapExito.get("idSecuencia"), lstNombre);
			ServicioMensajesUSR me = new ServicioMensajesUSR();

			me.enviaEmailContrato(empleadoBean.getContrato(), empleadoBean
					.getCorreoContrato(), mensaje
					.msgTextAsignacionTarjetaEmpleado().toString());

			//me.enviaEmailContrato(empleadoBean.getContrato(), empleadoBean,
					//.getCorreoContrato(), mensaje.msgTypeN3().toString());


//***************************************************INICIA BITACORIZACI�N***************************************************//
			EIGlobal.mensajePorTrace("AsignacionEmplBO::invocaAsignacionTarjeta:: Entrando ...", EIGlobal.NivelLog.INFO);
			if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);

					// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);

					// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
					BitaTCTBean beanTCT = new BitaTCTBean ();
					beanTCT = bh.llenarBeanTCT(beanTCT);

					//Datos Comunes
					bt.setNumBit(BitaConstants.ES_NOMINAN3_ASIGNACION_IND_ALTA);
					bt.setIdErr(BitaConstants.ES_NOMINAN3_ASIGNACION_IND + "0000");
					beanTCT.setCodError(BitaConstants.ES_NOMINAN3_ASIGNACION_IND + "0000");

					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
						beanTCT.setNumCuenta(session.getContractNumber().trim());
					}

					if (session.getUserID8() != null) {
						bt.setCodCliente(session.getUserID8().trim());
						beanTCT.setUsuario(session.getUserID8().trim());
						beanTCT.setOperador(session.getUserID8().trim());
					}

					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
					}

					ServicioTux tuxGlobal = new ServicioTux();
					Hashtable hs = null;
					Integer referencia = 0 ;
					String codError = null;
					hs = tuxGlobal.sreferencia("901");
					codError = hs.get("COD_ERROR").toString();
					referencia = (Integer) hs.get("REFERENCIA");
					if(codError.endsWith("0000") && referencia!=null){
						bt.setReferencia(referencia);
						beanTCT.setReferencia(referencia);
					}

					//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					bt.setIdFlujo(BitaConstants.ES_NOMINAN3_ASIGNACION_IND);
					bt.setEstatus("A");

					//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
					beanTCT.setTipoOperacion(BitaConstants.ES_NOMINAN3_ASIGNACION_IND);

					//Inserto en Bitacoras
					BitaHandler.getInstance().insertBitaTransac(bt);
					BitaHandler.getInstance().insertBitaTCT(beanTCT);
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + BitaConstants.ES_NOMINAN3_ASIGNACION_IND, EIGlobal.NivelLog.INFO);

				} catch (Exception e) {
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
				}
			}
//*****************************************************FIN BITACORIZACI�N***************************************************//

		}
		return mapExito.get("idSecuencia");
	}

	/**
	 * Obtiene el catalogo de paises.
	 *
	 * @return String
	 */
	public String ObtenCatalogo() {
		EIGlobal.mensajePorTrace("AsignacionEmplBO -->  ObtenCatalogo ",
				NivelLog.INFO);
		AsignacionEmplDAO asignacionEmplDAO = AsignacionEmplDAO.getSingleton();
		return asignacionEmplDAO.ObtenCatalogo();
	}


	/**
	 * Metodo encargado de insertar la carga masiva de empleados
	 *
	 * @param lstEmpleados
	 * @param descSesion
	 * @param totalReg
	 * @param numContrato
	 * @param cveUsuario
	 * @param req
	 * @param res
	 * @return
	 */
	public String insertarArchivo(List<EmpleadoBean> lstEmpleados,
			String descSesion, String totalReg, String numContrato,
			String cveUsuario, HttpServletRequest req, HttpServletResponse res) {

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		AsignacionEmplDAO asignacionEmplDAO = AsignacionEmplDAO.getSingleton();
		int reg = Integer.parseInt(totalReg.trim());
		List<EmpleadoBean> empleados = new ArrayList<EmpleadoBean>();

		for (EmpleadoBean empleado : lstEmpleados) {

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date convertedDate = format.parse(empleado.getFechaNac());
				String fechaNuevo = dateFormat.format(convertedDate);
				empleado.setFechaNac(fechaNuevo);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace("<<<<<<<Excepcion>>>>>>>>" + e,
						NivelLog.ERROR);
			}

			empleado.setContrato(numContrato);
			empleado.setUsuarioSession(cveUsuario);
			empleado.setCveUsuario((String) session.getUserID8());
			empleados.add(empleado);

		}

		Map<String, String> mapExito = asignacionEmplDAO.altaEmpleadoMasiva(
				empleados, cveUsuario, ALTA_ARCHIVO, TIPO_NOMINA, reg, reg,
				numContrato);
		List<String> lstNombre = new ArrayList<String>();
		if (mapExito.get("mensaje").equals("EXITO")) {
			for (EmpleadoBean e : lstEmpleados) {
				String nombreCompleto = "";
				nombreCompleto = e.getAppPaterno()
						+ " " + e.getAppMaterno()
						+ " " + e.getNombreEmpleado();
				lstNombre.add(nombreCompleto);

			}

			EmailDetails mensaje = new EmailDetails(lstEmpleados.get(0)
					.getContrato(), descSesion, mapExito.get("idSecuencia"),
					lstNombre);
			ServicioMensajesUSR me = new ServicioMensajesUSR();
			me.enviaEmailContrato(lstEmpleados.get(0).getContrato(),
					lstEmpleados.get(0).getCorreoContrato(), mensaje
							.msgTextAsignacionTarjetaEmpleado().toString());

			//me.enviaEmailContrato(lstEmpleados.get(0).getContrato(),
					//lstEmpleados.get(0).getCorreoContrato(), mensaje.msgTypeN3().toString());

//***************************************************INICIA BITACORIZACI�N***************************************************//
			EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Entrando ...", EIGlobal.NivelLog.INFO);
			if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);

					// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);

					// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
					BitaTCTBean beanTCT = new BitaTCTBean ();
					beanTCT = bh.llenarBeanTCT(beanTCT);

					//Datos Comunes
					bt.setNumBit(BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH_ALTA);
					bt.setIdErr(BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH + "0000");
					beanTCT.setCodError(BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH + "0000");


					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
						beanTCT.setNumCuenta(session.getContractNumber().trim());
					}

					if (session.getUserID8() != null) {
						bt.setCodCliente(session.getUserID8().trim());
						beanTCT.setUsuario(session.getUserID8().trim());
						beanTCT.setOperador(session.getUserID8().trim());
					}

					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
					}

					ServicioTux tuxGlobal = new ServicioTux();
					Hashtable hs = null;
					Integer referencia = 0 ;
					String codError = null;
					hs = tuxGlobal.sreferencia("901");
					codError = hs.get("COD_ERROR").toString();
					referencia = (Integer) hs.get("REFERENCIA");
					if(codError.endsWith("0000") && referencia!=null){
						bt.setReferencia(referencia);
						beanTCT.setReferencia(referencia);
					}

					//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
					bt.setIdFlujo(BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH);
					bt.setEstatus("A");
					bt.setNombreArchivo((String)req.getSession().getAttribute("nombreArchivoBita"));

					//Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
					beanTCT.setTipoOperacion(BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH);

					//Inserto en Bitacoras
					BitaHandler.getInstance().insertBitaTransac(bt);
					BitaHandler.getInstance().insertBitaTCT(beanTCT);
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: bitacorizando " + BitaConstants.ES_NOMINAN3_ASIGNACION_ARCH, EIGlobal.NivelLog.INFO);

				} catch (Exception e) {
					EIGlobal.mensajePorTrace("AdmonRemesasServlet::recibirRemesa:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
				}
			}
//*****************************************************FIN BITACORIZACI�N***************************************************//
		}
		return mapExito.get("idSecuencia");

	}

	/***
	 * Metodo para bitacorizar
	 *
	 * @param req
	 * @param res
	 * @param codError
	 * @param cveBitacora
	 */
	private void escribeBitacoraOper(HttpServletRequest req,
			HttpServletResponse res, String codError, String cveBitacora,
			Integer referencia) {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace(
				"AsignacionEmplaBO::escribeBitacoraOper:: Entrando ...",
				EIGlobal.NivelLog.DEBUG);

		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")) {
			try {

				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				if (getFormParameter(req, BitaConstants.FLUJO) != null) {
					bh.incrementaFolioFlujo((String) getFormParameter(req,
							BitaConstants.FLUJO));
				} else {
					bh.incrementaFolioFlujo((String) req.getSession()
							.getAttribute(BitaConstants.SESS_ID_FLUJO));
				}

				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean) bh.llenarBean(bt);

				BitaTCTBean beanTCT = new BitaTCTBean();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setTipoOperacion(cveBitacora);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				bt.setIdErr(codError);
				beanTCT.setCodError(codError);
				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(cveBitacora);

				BitaHandler.getInstance().insertBitaTransac(bt);
				BitaHandler.getInstance().insertBitaTCT(beanTCT);
				EIGlobal.mensajePorTrace(
						"RET_PactaOperBO::escribeBitacoraOper:: bitacorizando "
								+ cveBitacora, EIGlobal.NivelLog.INFO);

			} catch (Exception e) {
				e.printStackTrace();
				EIGlobal.mensajePorTrace("error" + e, EIGlobal.NivelLog.INFO);
			}
		}

	}
	public static boolean fileIsEmpty(File fArchivo){
		boolean flag = false;
		try{
			BufferedReader br = new BufferedReader(new FileReader(fArchivo));
			String strFirstLine;
			EIGlobal.mensajePorTrace("AsignacionEmplaBO:: fileIsEmpty :: Validando Contenido de Archivo", EIGlobal.NivelLog.DEBUG);
			strFirstLine=br.readLine();

			if(strFirstLine == null){
				EIGlobal.mensajePorTrace("AsignacionEmplaBO::fileIsEmpty:: El archivo esta vacio ", EIGlobal.NivelLog.DEBUG);
				flag = true;
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("AsignacionEmplaBO ::  fileIsEmpty :: Error en el archivo: "+e, EIGlobal.NivelLog.DEBUG);
		}
		return flag;
	}

	/**
	 *
	 * @param fecha_nac		fecha de nacimiento a calcular
	 * @return anos  devuelve la edad a partir de la fecha de nacimiento
	 */
	public static int calculaEdad(String fecha_nac) {     //fecha_nac debe tener el formato dd/MM/yyyy

	    Date fechaActual = new Date();
	    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	    String hoy = formato.format(fechaActual);
	    String[] fnacimiento = fecha_nac.split("/");
	    String[] factual = hoy.split("/");
	    int anos = Integer.parseInt(factual[2]) - Integer.parseInt(fnacimiento[2]);
	    int mes = Integer.parseInt(factual[1]) - Integer.parseInt(fnacimiento[1]);

	    if(mes < 0){
	    	anos=anos-1;
	    }else if(mes == 0){
	    	int dia = Integer.parseInt(factual[0]) - Integer.parseInt(fnacimiento[0]);
	    	if(dia < 0 ){
	    		anos=anos-1;
	    	}
	    }
	    return anos;
	  }

	/**
	 *
	 * @param fechaNaci, rfc - Valida que la fecha de nacimiento coincida con el rfc
	 * @return false si encuentra diferencia entre fecha y rfc
	 */
	public static boolean validaFechNaciRfc(String fechaNaci, String rfc){

		String[] fecha=fechaNaci.split("-");

		boolean flag = true;
	       try{
	              String strFecNaci = fecha[0].substring(2,4) + fecha[1] + fecha[2];
	              String strFecNacRfc = rfc.substring(4,10);

	              if(!strFecNaci.equals(strFecNacRfc))
	                   flag = false;

	       }catch(Exception e){
	             flag = false;
	       }
	      return flag;
	}
}