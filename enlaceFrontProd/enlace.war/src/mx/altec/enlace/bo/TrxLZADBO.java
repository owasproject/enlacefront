/** 
*   Isban Mexico
*   Clase: TrxLZADBO.java
*   Descripcion: clase de respuesta para almacenamiento y transporte de registros.
*
*   Control de Cambios:
*   1.0 Marzo 2016 FSW Everis 
*/
package mx.altec.enlace.bo;

import mx.altec.enlace.beans.TrxLZADBean;
import mx.altec.enlace.dao.TrxLZADDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class TrxLZADBO {
    
    /**Objeto contenedor de tipo TrxLZADBean*/
    private TrxLZADBean bean;
    
    /**
     * @return the bean
     */
    public TrxLZADBean getBean() {
        return bean;
    }

    /**
     * @param bean the bean to set
     */
    public void setBean(TrxLZADBean bean) {
        this.bean = bean;
    }

    /**
     * Valida transaccion a 390
     * @param tipoRegMP tipo de registro AFD
     * @param ctaOrigen cuenta origen
     * @param ctaDestino cuenta destino 
     * @return returnValue
     */
    public boolean validaTrxLZAD(String tipoRegMP, String ctaOrigen, String ctaDestino){
        EIGlobal.mensajePorTrace("TrxLZADBO::validaTrxLZAD:: Entrando....", EIGlobal.NivelLog.DEBUG);
        boolean returnValue         = false;
        bean = new TrxLZADDAO().ejecutaConsulta(tipoRegMP, ctaOrigen, ctaDestino);
        if(null == bean && !bean.getCodError().isEmpty()){
            EIGlobal.mensajePorTrace("TrxLZADBO::validaTrxLZAD:: No se devolvio un bean>>", EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTrace("TrxLZADBO::validaTrxLZAD:: Mensaje del bean >>["+bean.getMensajeError()+"]", EIGlobal.NivelLog.DEBUG);
        }
        else{
            returnValue         = true;
            EIGlobal.mensajePorTrace("TrxLZADBO () validaTrxLZAD : Se obtuvieron los datos para trx390", EIGlobal.NivelLog.DEBUG);
            
        }
        EIGlobal.mensajePorTrace("TrxLZADBO::validaTrxLZAD:: Saliendo....", EIGlobal.NivelLog.DEBUG);
        return returnValue;
    }
}
