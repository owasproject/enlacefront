/*
 * Facultades.java
 *
 * Created on 11 de octubre de 2002, 01:03 PM
 */

package mx.altec.enlace.bo;

import java.util.*;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 */
public class Facultades implements java.io.Serializable {
    private ArrayList facultades;

    /** Creates a new instance of Facultades */
    public Facultades () {
        facultades = new ArrayList ();
    }

    public boolean getFacultad (String fac) {
        return facultades.contains (fac);
    }

    public void setFacultad (String fac) {
        facultades.add (fac);
    }

}
