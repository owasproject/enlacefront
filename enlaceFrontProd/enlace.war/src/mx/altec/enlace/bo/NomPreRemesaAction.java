package mx.altec.enlace.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.altec.enlace.beans.NomPreLM1A;
import mx.altec.enlace.beans.NomPreLM1B;
import mx.altec.enlace.beans.NomPreLM1C;
import mx.altec.enlace.beans.NomPreRemesa;
import mx.altec.enlace.beans.NomPreTarjeta;
import mx.altec.enlace.dao.NomPreRemesaDAO;
import mx.altec.enlace.dao.NomPreTarjetaDAO;
import static mx.altec.enlace.utilerias.NomPreUtil.logInfo;

public class NomPreRemesaAction {
		
	public NomPreLM1C confirmarDetalleRemesa(String noContrato, String noRemesa, List<NomPreTarjeta> aceptadas, List<NomPreTarjeta> rechazadas) { 
		
		NomPreTarjetaDAO dao = new NomPreTarjetaDAO();
		NomPreLM1C lm1c = null;
		String[][] aprobar = null;
		String[][] rechazar = null;
		int renglones = 0;
		int i = 0;
		int y = 0;
		
		if (aceptadas != null && aceptadas.size() > 0) {
			
			renglones = (aceptadas.size() / 50) + (aceptadas.size() % 50 != 0 ? 1 : 0);						
			aprobar = new String[renglones][50];
			
			for (NomPreTarjeta tarjeta : aceptadas) {								
				
				aprobar[y][i] = tarjeta.getNoTarjeta();				
				i++;
				
				if(i  == 50) {
					i = 0;
					y++;
				}										
			}
		}				
		
		i = 0;
		y = 0;
		
		if (rechazadas != null && rechazadas.size() > 0) {
			
			renglones = (rechazadas.size() / 50) + (rechazadas.size() % 50 != 0 ? 1 : 0);						
			rechazar = new String[renglones][50];
			
			for (NomPreTarjeta tarjeta : rechazadas) {
								
				rechazar[y][i] = tarjeta.getNoTarjeta();				
				i++;
				
				if(i == 50) {
					i = 0;
					y++; 
				}
			}
		}				
		
		if (aprobar != null) {						
			
			for (y = 0; y < aprobar.length; y ++) {
				
				lm1c = dao.aprobarTarjetas(noContrato, noRemesa, aprobar[y]);
				
				if (!lm1c.isCodExito()) {
					
					return lm1c;
				}
			}
		}
		
		if (rechazar != null) {
	
			for (y = 0; y < rechazar.length; y ++) {
				
				lm1c = dao.rechazarTarjetas(noContrato, noRemesa, rechazar[y]);
	
				if (!lm1c.isCodExito()) {
										
					return lm1c;
				}
			}
		}	
		
		return lm1c;
	}
	
	public NomPreLM1C confirmarRemesa(String noContrato,  String noRemesa) {
		
		return new NomPreRemesaDAO().aprobarRemesa(noContrato, noRemesa);
	}
	
	public NomPreLM1C rechazarRemesa(String noContrato,  String noRemesa) {
		
		return new NomPreRemesaDAO().rechazarRemesa(noContrato, noRemesa);
	}
	
	public NomPreLM1A consultarRemesas(String noContrato) {
		
		NomPreRemesaDAO dao = new NomPreRemesaDAO();		
		NomPreLM1A lm1a = null;
		NomPreLM1A result = null;
		
		String paginacion = "";
		
		do {
		
			lm1a = dao.consultarRemesas(noContrato, "", "", paginacion);
			
			if(lm1a == null) break;
			
			if (result == null) {				
				result = new NomPreLM1A();				
			}
			
			result.setTotalRemesas(lm1a.getTotalRemesas());
			result.setCodExito(lm1a.isCodExito());
			result.setCodigoOperacion(lm1a.getCodigoOperacion());
			
			if (lm1a.getDetalle() != null) {
				
				if (result.getDetalle() == null) {
					
					result.setDetalle(new ArrayList<NomPreRemesa>());
				}
				
				result.getDetalle().addAll(lm1a.getDetalle());
			}
			
			paginacion = lm1a.getNoRemesaPag(); 
			
		} while (paginacion != null && paginacion.length() > 0);		
		
		return result;
	}
	
	public NomPreLM1B buscarTarjetas(String noContrato, String noRemesa) {
		
		NomPreTarjetaDAO dao = new NomPreTarjetaDAO();
		NomPreLM1B lm1b = null;
		NomPreLM1B result = null;
		String paginacion = "";
		
		do {						
			
			lm1b = dao.obtenerTarjetas(noContrato, noRemesa, paginacion);
			
			if (lm1b == null) break;
			
			if (result == null) {
				result = new NomPreLM1B();				
			}
			
			result.setRemesa(lm1b.getRemesa());			
			result.setCodigoOperacion(lm1b.getCodigoOperacion());
			result.setCodExito(lm1b.isCodExito());
			
			if (lm1b.getDetalle() != null) {
				
				if (result.getDetalle() == null) {
					
					result.setDetalle(new ArrayList<NomPreTarjeta>());
				}
				
				result.getDetalle().addAll(lm1b.getDetalle());
			}
			
			paginacion = lm1b.getNoTarjetaPag();
			
		} while (paginacion != null && paginacion.length() > 0); 
		
		return result;
	}
	
	public NomPreLM1B buscaTarjetaPorEstado(String noContrato, String noRemesa, String noTarjeta, String... estados) {
		
		
		NomPreLM1B result = null;
		NomPreLM1B lm1b = buscarTarjetas(noContrato, noRemesa);
		boolean existeTar = noTarjeta != null && noTarjeta.trim().length() > 0;
		boolean existeEst = estados != null && estados.length > 0; 
		
		if (lm1b == null || lm1b.getDetalle() == null || (!existeEst && !existeTar)) {
			
			result = lm1b;
			
		} else {
			
			result = new NomPreLM1B();
			result.setRemesa(lm1b.getRemesa());
			result.setDetalle(new ArrayList<NomPreTarjeta>());
									
			logInfo("ExisteTar: [" + existeTar + "] existeEst [" + existeEst + "] noTarjeta [" + noTarjeta.trim() + "] estados [" + Arrays.toString(estados) + "]");
			
			for (NomPreTarjeta tarjeta: lm1b.getDetalle()) {											
				
				if (existeTar && existeEst) {					
					
					if (tarjeta.getNoTarjeta().trim().equals(noTarjeta.trim())) {
					
						for (String estado : estados) {
							
							if (tarjeta.getEstadoTarjeta().trim().equals(estado)) {
								
								result.getDetalle().add(tarjeta);
								break;
							}
						}
					}
					
				} else if (existeTar) {
					
					if (tarjeta.getNoTarjeta().trim().equals(noTarjeta.trim())) {

						result.getDetalle().add(tarjeta);				
					}
					
				} else if (existeEst) {				
					
					for (String estado : estados) {												
						
						if (tarjeta.getEstadoTarjeta().trim().equals(estado.trim())) {
							
							result.getDetalle().add(tarjeta);
							break;
						}
					}
				}												
			}
			
			if (result.getRemesa() != null) {
				
				result.getRemesa().setTotalTarjetas(result.getDetalle().size());
			}
			
		}
		
		return result;
	}
}
