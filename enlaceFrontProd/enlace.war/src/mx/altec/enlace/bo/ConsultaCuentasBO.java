package mx.altec.enlace.bo;

import java.util.List;

import mx.altec.enlace.beans.CuentaBean;
import mx.altec.enlace.dao.ConsultaCuentasDAO;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * The Class ConsultaCuentasBO.
 */
public class ConsultaCuentasBO {

	/** The Constant OPCION_UNO. */
	private static final String OPCION_UNO = "1";

	/** The Constant OPCION_DOS. */
	private static final String OPCION_DOS = "2";

	/** The Constant OPCION_TRES. */
	private static final String OPCION_TRES = "3";

	/** The Constant OPCION_CUATRO. */
	private static final String OPCION_CUATRO = "4";

	/** The Constant OPCION_CINCO. */
	private static final String OPCION_CINCO = "5";

	/** The Constant MDisposicion_Cargo. */
	private static final String MDISPOSICION_CARGO = "43@";

	/** The Constant MDisposicion_Abono. */
	private static final String MDISPOSICION_ABONO = "44@";

	/**
	 * L�gica de validaciones para identificar el modulo a consultar.
	 * 
	 * @param opcion
	 *            the opcion
	 * @param modulo
	 *            the moduloconsultar
	 * @return the string
	 */
	public String consultaModulo(String opcion, String modulo) {
		EIGlobal.mensajePorTrace(
				"* :: ConsultaCuentasBO :: inicia consulta de modulo:"+ modulo +" opcion:"+ opcion,
				EIGlobal.NivelLog.DEBUG);

		if (null == modulo || OPCION_CUATRO.equals(opcion)) {
			modulo = IEnlace.MMant_gral_ctas;
			opcion = OPCION_CUATRO;
		}

		if (IEnlace.MCargo_transf_pesos.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MAbono_transf_pesos;
		} else if (IEnlace.MCargo_transf_dolar.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MAbono_transf_dolar;
		} else if (IEnlace.MCargo_transf_pesos.equals(modulo)
				&& OPCION_TRES.equals(opcion)) {
			modulo = IEnlace.MAbono_tarjeta_cred;
		} else if (IEnlace.MDep_inter_cargo.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MDep_Inter_Abono;
		} else if (IEnlace.MMant_gral_ctas.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MCons_Baja_Cta;
		} else if (IEnlace.MMant_gral_ctas.equals(modulo)
				&& OPCION_TRES.equals(opcion)) {
			modulo = IEnlace.MAbono_TI;
		} else if (IEnlace.MCargo_cambio_pesos.equals(modulo)
				&& OPCION_UNO.equals(opcion)) {
			modulo = IEnlace.MCargo_cambio_pesos + IEnlace.MCargo_cambio_dolar;
		} else if (IEnlace.MCargo_cambio_pesos.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MAbono_cambios_pesos + IEnlace.MAbono_cambio_dolar;
		} else if (IEnlace.MCargo_TI_pes_dolar.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MAbono_TI;
		}
		/** Validaci�n para ventana de muestra de cuentas para m�vil */
		else if (IEnlace.MMant_gral_ctas.equals(modulo)
				&& OPCION_CINCO.equals(opcion)) {
			modulo = IEnlace.MCons_Ctas_movil;
		} else if (MDISPOSICION_CARGO.equals(modulo)
				&& OPCION_TRES.equals(opcion)) {
			modulo = MDISPOSICION_ABONO;
		} else if (IEnlace.MAltaProveedor_abono.equals(modulo)
				&& OPCION_UNO.equals(opcion)) {
			modulo = IEnlace.MAbono_transf_pesos;
		} else if (IEnlace.MAltaProveedor_abono.equals(modulo)
				&& OPCION_DOS.equals(opcion)) {
			modulo = IEnlace.MDep_Inter_Abono;
		}//ARS VECTOR SPID
		else if (IEnlace.MDep_inter_cargo.equals(modulo)
				&& OPCION_CINCO.equals(opcion)) {
			modulo = IEnlace.MAbono_cambio_dolar;
		}

		EIGlobal.mensajePorTrace("* :: ConsultaCuentasBO :: modulo: " + modulo,
				EIGlobal.NivelLog.DEBUG);
		return modulo;
	}

	/**
	 * BO de Consulta cuentas.
	 * 
	 * @param archivo
	 *            the archivo
	 * @param modulo
	 *            the modulo
	 * @param argParametrosNoContemplados
	 *            de tipo String[], en la posicion 0 esta la Facultad DEPSINCAR
	 * @return the list
	 */
	public List<CuentaBean> consultaCuentas(String archivo, String modulo,
			String[] argParametrosNoContemplados) {
		EIGlobal.mensajePorTrace("* :: ConsultaCuentasBO :: consultaCuentas",
				EIGlobal.NivelLog.DEBUG);
		ConsultaCuentasDAO cuentasDAO = new ConsultaCuentasDAO();

		List<CuentaBean> cuentas = null;
		try {
			cuentas = cuentasDAO.consultaCuentas(archivo, modulo,
					argParametrosNoContemplados);
		} catch (DaoException e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return cuentas;
	}
}
