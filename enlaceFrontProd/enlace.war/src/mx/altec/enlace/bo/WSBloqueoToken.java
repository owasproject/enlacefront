package mx.altec.enlace.bo;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWS;
import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWSService;
import mx.altec.enlace.cliente.ws.bloqueotoken.CanalesWSServiceLocator;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

/**
 * @author ESC
 * Clase que se encarga de invocar los WS de Token Manager
 *
 */
public class WSBloqueoToken {

	/**
	 * C�digo de la aplicaci�n Enlace
	 */
	public static final String APPNAME = "E";
	/**
	 * C�digo de bloqueo por intentos fallidos
	 */
	public static final String COD_MOTIVO = "5";
	/**
	 * C�digo de bloqueo por intentos fallidos
	 */
	public static final String COD_LANG = "SP";
	/**
	 * Variable para distinguir en log las lineas de la clase
	 */
	private static final String logBloqueoToken = "WSBloqueoToken - bloqueoToken() - "; 

	/**
	 * Metodo que invoca el cliente para realizar el bloqueo de token por intentos fallidos.
	 * @param codigoCliente cliente
	 * @return String[] result cadena con el resultado que nos regresa el WS de bloqueo
	 */
	public String[] bloqueoToken(String codigoCliente) {
		String[] result = null;
        try {

        	final CanalesWSService service = new CanalesWSServiceLocator();
        	final CanalesWS token = service.getCanalesWS();
            EIGlobal.mensajePorTrace(
            		logBloqueoToken + "URL: [" + Global.URL_WS_BLOQ_TOKEN + "]",
            		EIGlobal.NivelLog.DEBUG
            );

            EIGlobal.mensajePorTrace(
            		logBloqueoToken + "Parametros de Entrada: ["
					+ new String[] {codigoCliente, APPNAME, COD_LANG, COD_MOTIVO}
					+ "]",
					EIGlobal.NivelLog.INFO
			);

            result = token.bloquearToken(codigoCliente, APPNAME, COD_LANG, COD_MOTIVO);

			EIGlobal.mensajePorTrace(
					logBloqueoToken + "result: [" + result + "]",
					EIGlobal.NivelLog.INFO
			);
        } catch (ServiceException se) {
			EIGlobal.mensajePorTrace(
					logBloqueoToken + "Error Controlado: ["
					+ new Formateador().formatea(se) + "]",
					EIGlobal.NivelLog.ERROR
			);
        } catch (RemoteException re) {
			EIGlobal.mensajePorTrace(
					logBloqueoToken + "Error Controlado: ["
					+ new Formateador().formatea(re) + "]",
					EIGlobal.NivelLog.ERROR
			);
		}
        return result;
	}

	/**
	 * M&eacute;todo para realizar el bloqueo de token por intentos fallidos.
	 * @param codigoCliente C&oacute;digo de cliente al que se realizar&aacute;
	 * el bloqueo de token.
	 * @param motivoBloqueo N&uacute;mero para informar el tipo de bloqueo.
	 * @return String[] result Cadena con el resultado que nos regresa el WS de
	 * bloqueo de token.
	 */
	public String[] bloqueoToken(String codigoCliente, String motivoBloqueo) {
		String[] result = null;
        try {

        	final CanalesWSService service = new CanalesWSServiceLocator();
        	final CanalesWS token = service.getCanalesWS();
            EIGlobal.mensajePorTrace(
            		logBloqueoToken + "URL: [" + Global.URL_WS_BLOQ_TOKEN + "]",
            		EIGlobal.NivelLog.DEBUG
            );

            EIGlobal.mensajePorTrace(
            		logBloqueoToken + "Parametros de Entrada: ["
					+ new String[] {codigoCliente, APPNAME, COD_LANG, motivoBloqueo}
            		+ "]",
            		EIGlobal.NivelLog.INFO
            );

            result = token.bloquearToken(codigoCliente, APPNAME, COD_LANG, motivoBloqueo);

			EIGlobal.mensajePorTrace(
					logBloqueoToken + "result: [" + result + "]",
					EIGlobal.NivelLog.INFO
			);
        } catch (ServiceException se) {
			EIGlobal.mensajePorTrace(
					logBloqueoToken + "Error Controlado: ["
					+ new Formateador().formatea(se) + "]", 
					EIGlobal.NivelLog.ERROR
			);
        } catch (RemoteException re) {
			EIGlobal.mensajePorTrace(
					logBloqueoToken + "Error Controlado: [" 
					+ new Formateador().formatea(re) + "]",
					EIGlobal.NivelLog.ERROR
			);
		}
        return result;
	}
}