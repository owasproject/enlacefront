/*
 * pdServicio.java
 *
 * Created on 3 de abril de 2002, 11:24 AM
 */

/* EVERIS
 * 28 Mayo 2007
 * Art. 52 Solucion de raiz
 * Jose Antonio Rodriguez Alba
 * Adicion de metodos para servicios tuxedo: generador de folio,
 * controlador de registros y dispersor de transacciones.
 *
 **/

package mx.altec.enlace.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;


//Cambio
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 *
 * @author  Horacio Oswaldo Ferro Daz
 * @version 1.0
 */
public class pdServicio {

    /** Servico de Tuxedo */
    protected ServicioTux Servicio;
    /** Numero de contrato */
    public String Contrato;
    /** Clave de Usuario */
    protected String Usuario;
    /** Perfil del usuario */
    protected String Perfil;
    /** Numero de secuencia */
    protected long numSecuencia;

    // EVERIS 28 Mayo 2007 Numero De Folio
    protected String FolioArchivo;

	private HttpServletRequest req;

	public HttpServletRequest getRequest(){
		return req;
	}

	public void setRequest(HttpServletRequest request){
		this.req=request;
	}

    /** Creates new pdServicio */
    public pdServicio() {
        Servicio = new ServicioTux();
        numSecuencia = -1;
    }

    /**
     * Metodo para obtener el servicio de Tuxedo
     */
    public ServicioTux getServicioTux() {return Servicio;}

    /**
     * Metodo para generar la trama de tuxedo
     * @return Trama de Tuxedo
     */
    public String generaTrama(String MedioEnt, String TipoOper,
        String Secuencia, String TipoResp, String queryRealizar ) {

		if(Perfil==null)
		 {
			EIGlobal.mensajePorTrace ("Perfil no enviado, usuario", EIGlobal.NivelLog.DEBUG);
			Perfil=Usuario;
		 }

        StringBuffer trama = new StringBuffer("");
        String contrato = quitaGuiones(Contrato);

		trama.append(TipoResp );
		trama.append(MedioEnt );
		trama.append("|" );
		trama.append(Usuario );
		trama.append("|" );
		trama.append(TipoOper);

        trama.append("|" );
		trama.append(contrato );
		trama.append("|" );
		trama.append(Usuario );
		trama.append("|" );
		trama.append(Perfil );
		trama.append("|");

        if (TipoOper.equals("SEOP"))
		 {
            trama.append(contrato );
			trama.append("@1@" );
			trama.append(Usuario );
			trama.append("@");
         }
		else if ( TipoOper.equals("CBOP") ||
                    TipoOper.equals("CROP"))
		 {
            trama.append(contrato );
			trama.append("@");
         }
		else if (TipoOper.equals("COOP") ||
                    TipoOper.equals("CAOP") ||
                    TipoOper.equals("OAOP") ) {/*Rafa. Aadi un parametro para
                                              *poder realizar cualquier query*/
            //trama = trama + "num_cuenta2 = '" + contrato + "' AND num_secuencia = " + Secuencia + "@";
            trama.append(queryRealizar );
			trama.append("@");
         }
		else if (TipoOper.equals ("ALOP"))
		 {
            trama.append(contrato );
			trama.append("@" );
			trama.append(Secuencia );
			trama.append("@" );
			trama.append(queryRealizar);
         }
        EIGlobal.mensajePorTrace ("Nueva trama metodo 1:" + trama.toString(), EIGlobal.NivelLog.INFO);
        return trama.toString();
    }

    /**
     * Metodo para generar la trama de tuxedo para enviar un archivo
     * @return Trama generada
     */
    public String generaTrama(String MedioEnt, String TipoOper,
        String Secuencia, String TipoResp,
        String Registros, String Path, char tipo) {
        StringBuffer trama = new StringBuffer("");
		String contrato = quitaGuiones(Contrato);

		if(Perfil==null)
		 {
			EIGlobal.mensajePorTrace ("Perfil no enviado, usuario", EIGlobal.NivelLog.DEBUG);
			Perfil=Usuario;
		 }

		trama.append(TipoResp );
		trama.append(MedioEnt );
		trama.append("|" );
		trama.append(Usuario );
		trama.append("|" );
		trama.append(TipoOper);

        trama.append("|" );
		trama.append(contrato );
		trama.append("|" );
		trama.append(Usuario );
		trama.append("|" );
		trama.append(Perfil );
		trama.append("|");

		EIGlobal.mensajePorTrace ("Enviando perfil a la trama AAOP", EIGlobal.NivelLog.DEBUG);

        if (tipo == 'M')
		 {
            trama.append(contrato );
			trama.append("@" );
			trama.append(Secuencia );
			trama.append("@" );
			trama.append(Registros );
			trama.append("@M@" );
			trama.append(Perfil );
			trama.append("@" );
			trama.append(Path );
			trama.append("@");
         }
		else
		 {
            trama.append(contrato );
			trama.append("@" );
			trama.append(Secuencia );
			trama.append("@" );
			trama.append(Registros );
			trama.append("@@" );
			trama.append(Perfil );
			trama.append("@" );
			trama.append(Path );
			trama.append("@");
         }
		EIGlobal.mensajePorTrace ("Nueva trama metodo 2: " + trama.toString(), EIGlobal.NivelLog.INFO);
        return trama.toString();
    }

    /**
     * Metodo para formatear numeros de contrato y cuentas
     * @param Guiones String a formatear
     * @return String formateado
     */
    public String quitaGuiones(String Guiones) {
        String Temp = "";
        StringTokenizer stGuiones =
            new StringTokenizer(Guiones, "-");
        int Tokens = stGuiones.countTokens();
        int Count = 0;
        while (Count < Tokens) {
            Temp = Temp + stGuiones.nextToken();
            Count++;
        }
        return Temp;
    }

    /**
     * Metodo para enviar un archivo por rcp
     * @param Archivo Archivo que sera mandado a Tuxedo
     * @return true si el archivo fue enviado correctamente, de lo contrario false
     */
    protected boolean envia(File Archivo) {
        try {
        	EIGlobal.mensajePorTrace("pdServicio:envia - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II

            boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();

            if ( !envArch.copiaLocalARemoto( Archivo.getName(), Global.DIRECTORIO_REMOTO) )
			{
				EIGlobal.mensajePorTrace( "***pdServicio.class & envia: No se realizo el envio del archivo. ", EIGlobal.NivelLog.DEBUG);
				Respuestal = false;
				
			} else {
				EIGlobal.mensajePorTrace( "***SpdServicio.class & envia: Se realizo el envio del archivo.", EIGlobal.NivelLog.DEBUG);
			}

            if (Respuestal){
                return true;
			}

        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
        }
        return false;
    }

    /**
     * Metodo para recibir un archivo de tuxedo
     * @param String Nombre del archivo a transmitir
     * @return true si se recibio correctamente, de lo contrario false
     */
    protected File recibe(String Nombre) {
        try {
        	EIGlobal.mensajePorTrace("pdServicio:recibe - Inicia", EIGlobal.NivelLog.INFO);
        	//IF PROYECTO ATBIA1 (NAS) FASE II
 
               	boolean Respuestal = true;
                ArchivoRemoto recibeArch = new ArchivoRemoto();
               
               	if(!recibeArch.copiaCUENTAS(Nombre, Global.DIRECTORIO_LOCAL)){
    				
    					EIGlobal.mensajePorTrace("*** pdServicio.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
    					Respuestal = false;
    					
    				}
    				else {
    				    EIGlobal.mensajePorTrace("*** pdServicio.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
    				    
    				}
               
               	if (Respuestal) {
                    File Archivo = new File(
                    		Nombre);
                    return Archivo;
                    
               	}
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
        }
        return null;
    }

    /**
     * Metodo para establecer el usuario
     * @param usuario Clave de usuario
     */
    public void setUsuario (String usuario) {Usuario = usuario;}

    /**
     * Metodo para establecer el numero de contrato
     * @param contrato Numero de contrato
     */
    public void setContrato (String contrato) {Contrato = contrato;}

    /**
     * Metodo para enviar un archivo de pago directo a Tuxedo
     * @param Archivo Apuntador al archivo que sera mandado a Tuxedo
     * @param Registros Numero de registros guardados en el archivo
     * @return Respuesta de tuxedo o mensaje de error
     */
    public File envia_tux(File Archivo, long Registros, char tipo) throws Exception {
        try {
            String Trama = generaTrama("EWEB", "SEOP", "", "1","");
            String NumeroSecuencia;
            StringTokenizer st;
            File ArchivoResp;
            Hashtable ht = Servicio.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            if (!Buffer.substring(0, 2).equals("OK"))
                throw new Exception ("Error en la conexion");
            st = new StringTokenizer(Buffer, "@");
            st.nextToken();
            st.nextToken();
            NumeroSecuencia = st.nextToken();
            try {
                numSecuencia = Long.parseLong (NumeroSecuencia);
            } catch (Exception ex) {
                throw new Exception ("Numero de secuencia erroneo");
            }
            if (!envia(Archivo)) throw new Exception ("Error al mandar el archivo");
            Trama = generaTrama ("EWEB", "AAOP", NumeroSecuencia, "2",
                Long.toString (Registros), Archivo.getPath (), tipo);
            ht = Servicio.web_red(Trama);
            Buffer = (String) ht.get("BUFFER");
            if (Buffer == "") throw new Exception ("Error en la transmision");
            ArchivoResp = recibe(Buffer);
            //System.out.println("*** NVS --> ArchivoResp -->" + ArchivoResp.getAbsolutePath());
            if (ArchivoResp == null) throw new Exception ("Error al recibir el archivo de respuesta");
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "pdreg"));
            //ArchivoResp.deleteOnExit ();
            return ArchivoResp;
        } catch (IOException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error al leer el archivo de respuesta");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error con la comunicacion");
        }
    }

  	// Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - inicio
	/**
	 * Metodo para enviar un archivo de pago directo a Tuxedo.
	 * @param Archivo Apuntador al archivo que sera mandado a Tuxedo.
	 * @param Registros Numero de registros guardados en el archivo
	 * @return ArchivoResp Respuesta de tuxedo o mensaje de error.
	 */
	public File envia_tux_aux(File Archivo, long Registros, char tipo, String NumeroSecuencia) throws Exception {
        try {
            String Trama;
            StringTokenizer st;
            File ArchivoResp;
            Hashtable ht;
            String Buffer;
            EIGlobal.mensajePorTrace("Recibiendo numero de secuencia = @" + NumeroSecuencia + "@", EIGlobal.NivelLog.INFO);
			if(NumeroSecuencia.equals("")){
				Trama = generaTrama("EWEB", "SEOP", "", "1","");
            	ht = Servicio.web_red(Trama);
	            Buffer = (String) ht.get("BUFFER");
    	        if (!Buffer.substring(0, 2).equals("OK"))
        	        throw new Exception ("Error en la conexion");
            	st = new StringTokenizer(Buffer, "@");
	            st.nextToken();
    	        st.nextToken();
        	    NumeroSecuencia = st.nextToken();
            	try {
                	numSecuencia = Long.parseLong (NumeroSecuencia);
	            } catch (Exception ex) {
    	            throw new Exception ("Numero de secuencia erroneo");
        	    }
        	}
            if (!envia(Archivo)) throw new Exception ("Error al mandar el archivo");
            Trama = generaTrama ("EWEB", "AAOP", NumeroSecuencia, "2",
                Long.toString (Registros), Archivo.getPath (), tipo);
            ht = Servicio.web_red(Trama);
            Buffer = (String) ht.get("BUFFER");
            if (Buffer == "") throw new Exception ("Error en la transmision");
            ArchivoResp = recibe(Buffer);
            if (ArchivoResp == null) throw new Exception ("Error al recibir el archivo de respuesta");
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "pdreg"));
            //ArchivoResp.deleteOnExit ();
            return ArchivoResp;
        } catch (IOException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error al leer el archivo de respuesta");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error con la comunicacion");
        }
    }



	// EVERIS 28 Mayo 2007
	/**
	 * Metodo para el controlador de registros.
	 * @param Archivo Apuntador al archivo que sera mandado a Tuxedo.
	 * @param Registros Numero de registros guardados en el archivo
	 * @return boolean false si no ocurrio error, true si ocurrio un error Valor para cuando ha terminado de enviar las tramas.
	 */
	public boolean envia_datos_tux_aux(File Archivo, long Registros, char tipo, String NumeroSecuencia) throws Exception {
        try {
            String Trama;
            StringTokenizer st;
            Hashtable ht;
            String Buffer;
            boolean errorEnvio = true;
            String Result = "";
            String mensajeErrorEnviar = "";
            if(NumeroSecuencia.equals(""))
			{
				Trama = generaTrama("EWEB", "SEOP", "", "1","");
            	ht = Servicio.web_red(Trama);
	            Buffer = (String) ht.get("BUFFER");

				if (!Buffer.substring(0, 2).equals("OK"))
        	        throw new Exception ("Error en la conexion");

				st = new StringTokenizer(Buffer, "@");
	            st.nextToken();
    	        st.nextToken();
        	    NumeroSecuencia = st.nextToken();

				try
				{
                	numSecuencia = Long.parseLong (NumeroSecuencia);
	            }
				catch (Exception ex)
				{
    	            throw new Exception ("Numero de secuencia erroneo");
        	    }
        	}

            if (!envia(Archivo))
            	throw new Exception ("Error al mandar el archivo");
            String gzipTrama = Archivo.getName() +"_" + FolioArchivo + ".tram.gz";
			Trama = generaTrama ("EWEB", "AAOP", NumeroSecuencia, "2", Long.toString (Registros), Archivo.getPath () + "_"+FolioArchivo+".tram", tipo);
			String tramaParaEnviar = Usuario + "|" + Perfil + "|" + Contrato + "|" + FolioArchivo + "|PAGD|" + IEnlace.REMOTO_TMP_DIR + "/"+ gzipTrama + "|" +
				 " " +"|" + Trama + "|" + IEnlace.REMOTO_TMP_DIR + "/" + Usuario + "_" + FolioArchivo;

			ht = Servicio.envia_datos(tramaParaEnviar);
			Result = (String) ht.get("BUFFER") +"";

			if(Result==null || Result.equals("null") ){
				Result="ERROR 	   Error en el servicio.";
				errorEnvio=true;
				throw new Exception ("Error en la transmision");
			}

			EIGlobal.mensajePorTrace("pdServicio -  CONTROLADOR DE REGISTROS  : Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
			String codError=Result.substring(0,Result.indexOf("|"));

			if(!codError.trim().equals("TRSV0000"))
				{
				mensajeErrorEnviar = Result.substring(Result.indexOf("|")+1,Result.length() );
				EIGlobal.mensajePorTrace ("EVERIS No se enviaron los registros al controlador de registros Mensaje: " +mensajeErrorEnviar , EIGlobal.NivelLog.DEBUG);
				errorEnvio=true;
				throw new Exception ("Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde. Error en el envio de registros. ");
				}
			else
				{
				mensajeErrorEnviar = Result.substring(Result.indexOf("|")+1,Result.length() );
				EIGlobal.mensajePorTrace ("EVERIS Se realizo el envio al controlador de registros: " + mensajeErrorEnviar , EIGlobal.NivelLog.DEBUG);
				if (mensajeErrorEnviar.equals("OK") )
					{
					errorEnvio = false;
					}
				}
            return errorEnvio;
        }
		catch (IOException ex)
		{
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error al leer el archivo de respuesta");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error con la comunicacion");
        }
    }


	// EVERIS 28 Mayo 2007
	/**
	 * Metodo para solicitar un folio al servicio tuxedo.
	 * @param TramaParaFolio trama que sera enviada al servicio tuxedo
	 * @return HashTable que contiene la respuesta del servicio
	 */

	public Hashtable obtiene_folio_tux(String TramaParaFolio) throws Exception {
		Hashtable ht = null;
		try {
			ht = Servicio.alta_folio(TramaParaFolio);
			return ht;
			}
		catch (IOException ex)
		{
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error al leer el archivo de respuesta");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error con la comunicacion");
        }
	}

	// EVERIS 28 Mayo 2007
	 /**
     * Metodo para establecer el folio del Archivo
     * @param folioArchivo Folio de Archivo
     */
    public void setFolioArchivo (String folioArchivo) {FolioArchivo = folioArchivo;}


	// EVERIS 28 Mayo 2007
	/**
	 * Metodo para el llamado al dispersor de registros.
	 * @return boolean
	 */
	public boolean dispersor_tux_aux() throws Exception {
		boolean errorEnvio = true;
		String Result = null;
        String mensajeErrorDispersor = "";
        Hashtable ht = null;
        String codError = null;

        try {
					String tramaParaDispersor = Usuario + "|" + Perfil + "|" + Contrato + "|" + FolioArchivo + "|PAGD|";
					ht = Servicio.inicia_proceso(tramaParaDispersor);
					Result= (String) ht.get("BUFFER") +"";
					if(Result==null || Result.equals("null") ){
						Result="ERROR 	   Error en el servicio.";
						errorEnvio=true;
						throw new Exception ("Error en la transmision");
						}
					EIGlobal.mensajePorTrace("MDI_Enviar - iniciaEnviar() DISPERSOR DE REGISTROS  : Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
					codError=Result.substring(0,Result.indexOf("|"));

					if(!codError.trim().equals("TRSV0000"))
						{
						mensajeErrorDispersor = Result.substring(Result.indexOf("|")+1,Result.length() );
						EIGlobal.mensajePorTrace ("EVERIS No se realizo la dispersion de registros Mensaje: " +mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
						errorEnvio=true;
						throw new Exception ("Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.Error en la dispersion de registros. ");
						}
					else
						{
						mensajeErrorDispersor = Result.substring(Result.indexOf("|")+1,Result.length() );
						EIGlobal.mensajePorTrace ("EVERIS Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
						errorEnvio = false;
						}
            return errorEnvio;
        }
		catch (IOException ex)
		{
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error al leer el archivo de respuesta");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
            throw new Exception ("Error con la comunicacion");
        }
	}

  	// Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - fin
    /**
     * Metodo para actualizar el estado de un pago directo por archivo
     * @param Numero de referencia del pago
     * @return Objeto con los pagos actualizados
     */
    public pdArchivoTemporal actualizaPago(String NumSec) {
        try {
            File ArchivoResp;
            pdArchivoTemporal ArchTemp = new pdArchivoTemporal();
            BufferedReader reader;
            pdPago pag;
            pdTokenizer tokenizer;
            String Trama = generaTrama("EWEB", "COOP", NumSec, "2", "num_cuenta2 = '" + Contrato + "' AND num_secuencia = " + NumSec );
            String Linea;
            Hashtable ht = Servicio.web_red(Trama);
            String Buffer = (String) ht.get("BUFFER");
            if (Buffer == "") return null;
            ArchivoResp = recibe(Buffer);
            ArchivoResp.renameTo (new File (ArchivoResp.getName () + "actualiza"));
            ArchivoResp.deleteOnExit ();
            if (ArchivoResp == null) return null;
            reader = new BufferedReader (
                new FileReader (ArchivoResp));
            try {
                Linea = reader.readLine();
                // todo: Checar que onda con la primer linea del archivo
                while (null != (Linea = reader.readLine())) {
                    EIGlobal.mensajePorTrace("pdServicio Linea=" + Linea, EIGlobal.NivelLog.ERROR);
                    tokenizer = new pdTokenizer (Linea, "@", 2);
                    pag = new pdPago();
                    pag.setNumeroReferencia (tokenizer.nextToken ());
                    pag.setCuentaCargo(tokenizer.nextToken());
                    pag.setNoPago(tokenizer.nextToken());
                    pag.setClaveBen(tokenizer.nextToken());
                    ArchTemp.setFechaActualizacion(parseFecha(tokenizer.nextToken()));
                    pag.setFechaLib(parseFecha(tokenizer.nextToken()));
                    tokenizer.nextToken ();
                    pag.setConcepto(tokenizer.nextToken());
					pag.setImporte(Double.parseDouble(tokenizer.nextToken()));
                    pag.setFormaPago(tokenizer.nextToken().charAt(0));
                    pag.setEstatusPago(tokenizer.nextToken().charAt(0));
                    pag.setDescripcionEstatus(tokenizer.nextToken());
                    pag.setFechaPago(parseFecha(tokenizer.nextToken()));
                    pag.setFechaVencimiento(parseFecha(tokenizer.nextToken()));
                    pag.setFechaVencio(parseFecha(tokenizer.nextToken()));
                    pag.setFechaModificacion(parseFecha(tokenizer.nextToken()));
                    tokenizer.nextToken ();
                    pag.setClaveRegion(tokenizer.nextToken());
                    pag.setClavePlaza(tokenizer.nextToken());
                    pag.setClaveSucursal(tokenizer.nextToken());
                    tokenizer.nextToken(); //se salta el campo de referencia
					pag.setNomBen(tokenizer.nextToken());
                    //System.out.println("LAM --> nombre del beneficiario = " + pag.getNomBen());
                    ArchTemp.agrega(pag);
                }
            } catch (IOException ex) {
                EIGlobal.mensajePorTrace (ex.getMessage (), EIGlobal.NivelLog.ERROR);
                //ex.printStackTrace (System.err);
                return null;
            }finally{
                if(null != reader){
                    try{
                        reader.close();
                    }catch(java.io.IOException ex){
                    }
                }
            }
            return ArchTemp;
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
        }
	return null;
    }

    /** <code>getCuentas</code> Obtiene las cuentas asociadas al contrato
     * @return ArrayList con las cuentas, null si ocurrio algun error
     */
    public ArrayList getCuentas(){
        ArrayList aCuentas= new ArrayList();
        BufferedReader rFile = null;
        try{
            String trama= generaTrama("EWEB","CROP","","2","");
            /*Enviar trama a Tuxedo*/
            Hashtable ht = Servicio.web_red(trama);

            /*Obtener Path de Archivo de respuesta*/
            String arch = (String) ht.get("BUFFER");
            if(arch.equals("")){
                return null;
            }
            File file = recibe(arch);
            file.deleteOnExit();
            if(file == null ){
                return null;
            }
            rFile = new BufferedReader (new FileReader (file));
            trama = rFile.readLine();

            /*obtener respuesta*/
            if(  trama.substring(0,2).equals("OK") ){
                /*obtener las cuentas*/
                while((trama = rFile.readLine() ) != null){
                    if(trama.trim().equals("")){ continue;}
                    pdCuenta cuenta= new pdCuenta();

                    StringTokenizer st = new StringTokenizer(trama,"@");
                    //si no estan los tres campos
                     if(st.countTokens() < 3){//el archivo esta incompleto
                        throw new IOException("Archivo Incompleto");
                    }
                    cuenta.setNoCta(st.nextToken());
                    cuenta.setFechaAlta(st.nextToken());
                    cuenta.setNoRef(st.nextToken());

                    aCuentas.add(cuenta);
                }
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
                return aCuentas;
            }

        } catch (IOException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);

        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);

        }finally{
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
        }
        return null;
    }

    /** <CODE>getBeneficiarios</CODE>obtiene los beneficiarios asociados a un contrato
     * @return ArrayList con los beneficiarios, c/u encapsulado en un objeto pdbeneficiario
     *        null si ocurre algun error
     */
    public ArrayList getBeneficiarios(){
        ArrayList aBenef= new ArrayList();
        BufferedReader rFile = null;
        try{

            String trama= generaTrama("EWEB","CBOP","","2","");
            /*Enviar trama a Tuxedo*/
            Hashtable ht = Servicio.web_red(trama);

            /*Obtener Path de Archivo de respuesta*/
            String arch = (String) ht.get("BUFFER");
            if(arch.equals("")){
                return null;
            }
            File file = recibe(arch);
            file.deleteOnExit();
            if(file == null ){
                return null;
            }
            rFile = new BufferedReader (new FileReader (file));
            trama = rFile.readLine();
            /*obtener respuesta*/
            if(trama.substring(0,2).equals("OK")){
                /*obtener las beneficiarios*/

                while((trama = rFile.readLine() ) != null){
                    if(trama.trim().equals("")){ continue;}
                    /*obtener solo el no de cuenta*/
                    StringTokenizer st = new StringTokenizer(trama,"@");
                    /*si no se obtienen los tres campos, el archivo esta truncado u
                     ocurrio algun error*/
                    if(st.countTokens() < 3){
                        throw new IOException("Archivo Incompleto");
                    }
                    pdBeneficiario benef = new pdBeneficiario();

                    benef.setID(st.nextToken());
                    benef.setNombre(st.nextToken());
                    benef.setFechaAlta(st.nextToken());

                    aBenef.add(benef);
                }
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
                return aBenef;
            }

        } catch (IOException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);

        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);

        }   finally{
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
        }

        return null;
    }

    /**
     * Metodo para construir una nueva fecha en base a un string
     * @param Fecha String con la fecha
     * @return GregorianCalendar con la fecha del string
     */
    protected GregorianCalendar nuevaFecha (String Fecha) {
        if ((null == Fecha) || (Fecha.equals (""))) return null;
        StringTokenizer st = new StringTokenizer (Fecha, "/");
        int Dia = Integer.parseInt (st.nextToken ());
        int Mes = Integer.parseInt (st.nextToken ());
        int Anio = Integer.parseInt (st.nextToken ());
        GregorianCalendar retValue = new GregorianCalendar (/*Anio, Mes - 1, Dia*/);
        retValue.set( Anio, Mes - 1, Dia);
        retValue.setLenient(false);
        return retValue;
    }

    /**
     * Metodo para construir una nueva fecha en base a un string
     * @param Fecha String con la fecha
     * @return GregorianCalendar con la fecha del string
     */
    public GregorianCalendar parseFecha(String fecha){
        GregorianCalendar retValue = null;
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        java.util.Date date = null;
        try{
            date = sdf.parse(fecha);
        }catch(java.text.ParseException ex){
            EIGlobal.mensajePorTrace("pdServicio.parseFecha: " + ex.getMessage() +", retValue=" +retValue,EIGlobal.NivelLog.ERROR  );
            return retValue;
        }
        if(null != date){
            retValue = new GregorianCalendar();
            retValue.setTime(date);

        }
        return retValue;
    }

    /**
     * Metodo para obtener a los beneficiarios en un HashMap
     * @param beneficiarios Lista con los beneficiarios
     * @return Mapa con los beneficiarios
     */
    public HashMap getBeneficiarios (ArrayList beneficiarios) {
        HashMap MapaBeneficiarios = new HashMap ((beneficiarios != null) ? beneficiarios.size () : 1);
        if (beneficiarios == null) return MapaBeneficiarios;
        ListIterator liBeneficiarios = beneficiarios.listIterator ();
        String Llave;
        pdBeneficiario Ben;
        while (liBeneficiarios.hasNext ()) {
            Ben = (pdBeneficiario) liBeneficiarios.next ();
            Llave = new String (Ben.getID ());
            MapaBeneficiarios.put (Llave, Ben);
        }
        return MapaBeneficiarios;
    }

    /**
     * Metodo para obtener a los beneficiarios en un HashMap
     * @return Mapa con los beneficiarios
     */
    public HashMap getBeneficiariosMapa () {
        return getBeneficiarios (getBeneficiarios ());
    }
    /** Obtiene los resultados de la consulta, de acuerdo a los parametros en filtro
     * @return Mapa con el resultado
     * @param filtro es el grupo de filtros a apicar en el query
     */
    public ArrayList getConsulta(pdCFiltro filtro){
        ArrayList al = new ArrayList();
        BufferedReader rFile = null;
        try{
            String trama = generaTrama("EWEB","COOP","","2","num_cuenta2 = '" +
                                                        Contrato + "'" +
                                                    filtro.getQuery());

            /*Enviar trama a Tuxedo*/
            Hashtable ht = Servicio.web_red(trama);

            /*Obtener Path de Archivo de respuesta*/
            String arch = (String) ht.get("BUFFER");
            if(arch.equals("")){
                return null;
            }
            EIGlobal.mensajePorTrace ("pdServicio:getConsulta - BUFFER =" + arch, EIGlobal.NivelLog.INFO);
            File file = recibe(arch);
            file.deleteOnExit();
            if(file == null ){
                return null;
            }
            rFile = new BufferedReader (new FileReader(file));
            trama = rFile.readLine();
            /*obtener respuesta*/
            EIGlobal.mensajePorTrace ("pdServicio:getConsulta - iniciando lectura de archivo", EIGlobal.NivelLog.DEBUG);
            if(trama.substring(0,2).equals("OK")){
                pdCResultado res = new pdCResultado();
                try{
                    al =  res.read(rFile);
                    return al;
                } catch (IOException e){
                    EIGlobal.mensajePorTrace ("pdServicio::getConsulta - Error en la lectura", EIGlobal.NivelLog.ERROR);
                    //e.printStackTrace();
                } catch ( Exception e){
                    EIGlobal.mensajePorTrace ("pdServicio::getConsulta - Error en el formato de los datos", EIGlobal.NivelLog.ERROR);
                    //e.printStackTrace();
                } finally {
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
            }
            else{
                EIGlobal.mensajePorTrace ("pdServicio:getConsulta() - Error", EIGlobal.NivelLog.ERROR);
            }
        } catch (IOException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException e){
                    }
                }
            return null;
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace(System.err);
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException e){
                    }
                }
            return null;
        }finally{
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
        }
        return null;
    }

    /**
     * Metodo para enviar registros de pago en linea
     * @param pago Pago a ser enviado
     * @return Respuesta del servicio de tuxedo
     */
    public String regLinea (pdPago pago, String Estatus) {
        try {
            String Trama1 = generaTrama ("EWEB", "SEOP", "", "1", "");
            Hashtable ht;
            String Buffer;
            StringTokenizer st;
            String Secuencia;
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("ddMMyyyy");
            ht = Servicio.web_red (Trama1);
            Buffer = (String) ht.get ("BUFFER");
            if (Buffer == null || Buffer.indexOf ('@') == -1) return "Error en la comunicacion";
            st = new StringTokenizer (Buffer, "@");
            st.nextToken ();
            st.nextToken ();
            Secuencia = st.nextToken ();

			StringBuffer Trama=new StringBuffer("");


            Trama.append( pago.getCuentaCargo () );
			Trama.append("@" );
			Trama.append(EIGlobal.eliminaCerosImporte(pago.getNoPago()));
			Trama.append("@" );
			Trama.append(pago.getImporte () );
			Trama.append("@");
            Trama.append(pago.getClaveBen () );
			Trama.append("@" );
			Trama.append(pago.getNomBen () );
			Trama.append("@" );
			Trama.append(pago.getFormaPago () );
			Trama.append("@");
            Trama.append(sdf.format (pago.getFechaLib ().getTime ()) );
			Trama.append("@");
            Trama.append(sdf.format (pago.getFechaPago ().getTime ()) );
			Trama.append("@");
            if (pago.getTodasSucursales ())
                Trama.append("S@");
            else
                Trama.append("@");
            Trama.append(pago.getClaveSucursal () );
			Trama.append("@@@" );
			Trama.append(pago.getConcepto () );
			Trama.append("@" );
			Trama.append(Perfil );
			Trama.append("@");
            if (Estatus.equals ("")) {
                Trama.append("@");
            } else {
                Trama.append(Estatus );
				Trama.append("@");
            }
            Trama1 = generaTrama ("EWEB", "ALOP", Secuencia, "1", Trama.toString());
            ht = Servicio.web_red (Trama1);
            return (String) ht.get ("BUFFER");
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace  (ex.getMessage (), EIGlobal.NivelLog.ERROR);
            //ex.printStackTrace (System.err);
            return "Error con la comunicacin";
        }
    }

    /**
     * Metodo para establecer el perfil del usuario
     * @param perfil Perfil del usuario
     */
    public void setPerfil (String perfil) { Perfil = perfil;}

    /** cancela  pagos
     * @param pagos arraylist con los pagos
     * @return array list con las respuestas de cada cancelacion
     */
    public ArrayList cancelaPagos (ArrayList pagos){
        ArrayList ret =new ArrayList();
        ArrayList message = new ArrayList();
        String trama = "";
        int cancelados = 0;
        int noCancelados = 0;
       java.text.NumberFormat nf = new java.text.DecimalFormat();
        nf.setMinimumIntegerDigits(1);
        nf.setMaximumIntegerDigits(12);
        nf.setGroupingUsed (false);

        pdCPago pago = new pdCPago();

        for(int i = 0; i < pagos.size(); ++i){
            try{
                pago = (pdCPago) pagos.get(i);
                char status = pago.getStatus();
                if(status == 'I' || status == 'R'){
                    try{
                        trama = this.Contrato + "@" +
                                pago.getNoCta() + "@" +
                                pago.getNoOrden();
                    } catch (NumberFormatException e){
                        EIGlobal.mensajePorTrace ("pdServicio::cancelaPagos - Error en el formato de la orden:" +
                                            pago.getNoOrden(), EIGlobal.NivelLog.DEBUG);
                        ++noCancelados;
                        continue;
                    }
                    trama = generaTrama("EWEB","CAOP","","1",trama);

                    Hashtable ht = Servicio.web_red(trama);
                    trama = (String) ht.get("BUFFER");
                    EIGlobal.mensajePorTrace ("pdServicio:cancelaPagos - trama =" + trama, EIGlobal.NivelLog.DEBUG);

                    if(trama.length() <= 0){
                        throw new Exception("Error en la comunicacion...");
                    } else if ( trama.substring (0,8).trim().equals ("OK") ) {
                        trama = trama.substring ( 16 );
                        EIGlobal.mensajePorTrace ("trama =" + trama, EIGlobal.NivelLog.INFO);
                        if( trama.substring ( 0, 8).trim ().equals ( "ORPA0000" ) ) {
                            //message.add("Pago no:" + pago.getNoOrden() + " trama:" + trama );
                            ++cancelados;
                            // TODO: BIT CU 4251, Cancelacion de pagos
							/*
							 * VSWF ARR -I
							 */
            				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
            				 	if(this.req != null){

								  try{
										java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
										BitaHelper bh = new BitaHelperImpl(req, (BaseResource) req.getSession ().getAttribute ("session"),req.getSession());
										BitaTransacBean bt = new BitaTransacBean();
										bt = (BitaTransacBean)bh.llenarBean(bt);
										bt.setNumBit(BitaConstants.ES_PAGO_DIR_CONS_MODIF_REALIZA_CANC_PAGO_DIR);
										bt.setContrato(this.Contrato);
										bt.setServTransTux("ORPA");
										bt.setIdErr("ORPA0000");

										if(pago.getNoCta()!=null || !pago.getNoCta().equals("") )
											bt.setCctaOrig(pago.getNoCta());

										bt.setImporte(pago.getImporte());

										bt.setFechaProgramada(Utilerias.MdyToDate(sdf.format(pago.getFechaCancelacion().getTime())));
										BitaHandler.getInstance().insertBitaTransac(bt);
								  }catch (SQLException e){
										//e.printStackTrace();
								  }catch(Exception e){
										//e.printStackTrace();
								  }
								  }
								}
							/*
							 * VSWF ARR -F
              				 */

                        } else {
                            message.add("Pago no:" + pago.getNoOrden() + " trama:" + trama );
                            ++noCancelados;
                        }
                    } else {
                        throw new Exception ( trama.substring(16) + "\n");
                    }
                    /*
                    if(trama.length() <= 0){
                        throw new Exception("Error en la comunicacion...");
                    } else if(trama.substring(0,8).trim().equals("ORDPA0000")){
                        message.add("Pago no:" + pago.getNoOrden() + " status:" + trama.substring(16));
                        ++cancelados;
                    } else {
                        throw new Exception ( trama.substring(16) + "\n");
                    } */
                }
            } catch (Exception e){

                message.add("Pago no:" + pago.getNoOrden() + " status:" + e.getMessage());
                ++noCancelados;
                continue;
            }
        }
        if(cancelados > 0 ){
            ret.add("Se cancelaron " + cancelados + " pagos.");
        }
        if(noCancelados > 0){
            ret.add("No se cancelaron " + noCancelados + " pagos.");
        }
        ret.addAll(ret.size(), message);
        return ret;
    }
    /**metodo para obtener las sucursales
     *@return ArrayList con las sucursales
     */
    public ArrayList getSucursales(){
        ArrayList al = new ArrayList();
        BufferedReader rFile = null;
        try{
            String trama= generaTrama("EWEB","OAOP","","2","7");

            Hashtable ht = Servicio.web_red(trama);

            /*Obtener Path de Archivo de respuesta*/
            String arch = (String) ht.get("BUFFER");

            if(null == arch || arch.equals("")){
                return null;
            }
            File file = recibe(arch);
            file.deleteOnExit();
            if(file == null ){
                return null;
            }
            rFile = new BufferedReader (new FileReader (file));
            trama = rFile.readLine();
            /*obtener respuesta*/
            if(null!= trama && trama.substring(0,2).equals("OK")){
                while(null != (trama = rFile.readLine())){
                    try {
                        pdSucursal sucursal = new pdSucursal();
                        sucursal.fill( trama );
                        al.add( sucursal );
                    } catch (Exception e){
                        EIGlobal.mensajePorTrace ("pdServicio:getSucursales - archivo incompleto...", EIGlobal.NivelLog.ERROR);
                    }
                }
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
                return al;
            }
        } catch (Exception e){
           EIGlobal.mensajePorTrace ("pdSucursales - " + e.getMessage (), EIGlobal.NivelLog.ERROR);
            //e.printStackTrace (System.err);

        } finally{
                if(null!= rFile){
                    try{
                        rFile.close();
                        rFile = null;
                    }catch(java.io.IOException ex){
                    }
                }
        }
        return null;
    }

    public long getNumSecuencia () {return numSecuencia;}

}