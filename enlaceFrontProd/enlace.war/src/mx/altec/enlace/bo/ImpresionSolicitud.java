/*
 * Contrato.java
 *
 * Created on 3 de Mayo de 2007
 *
 * Clase encarga de generar el documento de solicitud
 * en formato PDF
 */

package mx.altec.enlace.bo;

//import //EnlaceMig.Global;

import java.io.ByteArrayOutputStream;

import java.util.GregorianCalendar;


import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 *
 * @author Iv�n Cano Ortega.Vision
 */
public class ImpresionSolicitud {

	 private Locale _currentLocale;
	 private Calendar _calIni; // calendario global de la fecha inicial se
		  // recarga en el metodo fiGetDaysOn2
     private Calendar _calFin = null; // calendario global de la fecha final se
				 // recarga en el metodo fiGetDaysOn2
     private boolean bDebug = false;


	private String codigocliente = "";
	private String nombre = "";
	private String direccion = "";
	private String correo = "";
	private String telefono = "";
	private String telefonoII="";
	private String paterno="";
	private String materno="";
	private String folio;
	private String fecha;
	private String dia;
	private String mes;
	private String ano;
	private String diaCaduco;
	private String mesCaduco;
	private String anoCaduco;
	private String areacode;
	private String areacodealt;
	private String contrato;
      private String razonsocial;
      private String clientecontrato;

	//private float tam = 0;
   /* public static void main(String[] args) {
		System.out.println("Empieza la Generaci�n de PDF");
		Contrato2 contrato = new Contrato2();
		//contrato.tam = Float.parseFloat(args[0]);
		//contrato.generaContrato("12345678","DAVID AGUILAR GOMEZ.");

	}*/

/** El m�todo generaContrato(String, String, String, String) genera el PDF
*   @param contrato  numero de contrato
*   @param direccion direccion de la empresa
*   @param empresa nombre de la empresa
*   @param nombre nombre del representante
*/
	public ByteArrayOutputStream generaSolicitud(){
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
		System.out.println("M�todo Contrato.generaContrato");
		try{
			// String ruta=/productos/ias60/ias/APPS/enlaceMig/enlaceMig/Contrato.pdf
			//PdfReader reader = new PdfReader("C:/Solicitud.pdf");
			//PdfReader reader = new PdfReader("/proarchivapp/ias/APPS/enlaceMig/enlaceMig/pdf/Solicitud.pdf");
			//PdfReader reader = new PdfReader("/proarchivapp/ias/APPS/enlaceMig/enlaceMig/WEB-INF/pdf/Solicitud.pdf");
			//PdfReader reader = new PdfReader("/productos/ias60/ias/APPS/enlaceMig/enlaceMig/Contrato.pdf");
			EIGlobal.mensajePorTrace("RUTA PDF SOLICITUD: " + Global.RUTA_PDF_SOLICITUD, EIGlobal.NivelLog.DEBUG);
			PdfReader reader = new PdfReader(Global.RUTA_PDF_SOLICITUD);



			System.out.println("Pags: " + reader.getNumberOfPages());

			Document documento = new Document(PageSize.LETTER,1,1,1, 4);
			PdfWriter writer = PdfWriter.getInstance(documento, baos);
			//PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream("Contrato.pdf"));
			documento.open();

			PdfContentByte cb = writer.getDirectContent();

			PdfImportedPage page = writer.getImportedPage(reader, 4);
			cb.addTemplate(page, 0, 0);

			cb.beginText();
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);


 			 java.util.Date dt = new java.util.Date();
				Calendar cal = Calendar.getInstance();
				Date date = cal.getTime();
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);


				String auxmes = getmesenletra( dt.getMonth() + 1 );
				setFechaMes( String.valueOf( dt.getMonth() + 1 ) );
				//int anio = dt.getYear()+1900;
				setFechaDia( String.valueOf( dt.getDate() ) );
				setFechaAno( String.valueOf(dt.getYear()+1900) );

				String leyenda="";

			// diaCaduco =(String) ( Integer.parseInt( desentramaS( fecha +"/", '/',1) ) ) ;
            // mesCaduco= (String) ( Integer.parseInt( desentramaS( fecha +"/", '/',2) ) ) ;
            // anoCaduco=(String) ( Integer.parseInt( desentramaS( fecha +"/", '/',3) ) ) ;




	        //System.out.println("Fecha Final Habil="+FechaHabil);







				cb.setFontAndSize(bf, 9);//// EL TAMA�O
				cb.setTextMatrix(165, 674);/// EJE X, EJE Y

				cb.showText( clientecontrato);
				cb.setTextMatrix(50, 628);/// EJE X, EJE Y

				cb.showText( codigocliente);


				cb.endText();
				cb.addTemplate(page, 0, 0);
				cb = writer.getDirectContent();
				cb.beginText();
				 //con.setFolio("00000001");
		          int incremento =376;
	       	    //con.setFecha("15/12/2003");
				cb.setTextMatrix(50, 600);
				cb.showText( folio);

 		      	cb.setTextMatrix(280, 663-incremento);
		      	cb.showText( razonsocial);
			      cb.setTextMatrix(280, 674);
		      	cb.showText( razonsocial);

				cb.setTextMatrix(275, 628);
				cb.showText( nombre + " " + paterno + " " + materno);
//				cb.setTextMatrix(96, 552);
//				cb.showText( direccion);

				cb.setTextMatrix(340, 600);
				cb.showText( correo);
				cb.setTextMatrix(50, 590 - incremento);
				cb.showText( folio);
				cb.setTextMatrix(50, 617-incremento);/// EJE X, EJE Y
				cb.showText( codigocliente);
				cb.setTextMatrix(165, 663-incremento);/// EJE X, EJE Y
				cb.showText( clientecontrato);


			      cb.setTextMatrix(67, 514);
			      cb.showText( nombre + " " + paterno + " " + materno);
			      cb.setTextMatrix(67, 120);
			      cb.showText( nombre + " " + paterno + " " + materno);


				cb.setTextMatrix(275, 617- incremento);
				cb.showText( nombre + " " + paterno + " " + materno);
//				cb.setTextMatrix(96, 552);
//				cb.showText( direccion);

				cb.setTextMatrix(340, 590- incremento);
				cb.showText( correo);

	/*			cb.setTextMatrix(68, 530);
	                     if ( (areacodealt.equals("")== true)  || (telefonoII.equals("")== true)  ){
				cb.showText( areacode +" "+telefono );
	                     }else{
	                     	cb.showText( areacode +" "+telefono + " y " + areacodealt +" "+ telefonoII);

	                          }*/
				cb.setFontAndSize(bf, 7);
				cb.setTextMatrix(434, 324);
				cb.showText(dia );
				cb.setTextMatrix(489, 324);
                           mes=getmesenletra( Integer.parseInt(mes) );
        			cb.showText(mes );
				//cb.showText("SEPTIEMBRE" );
				cb.setTextMatrix(553, 324);
				cb.showText(ano );

				cb.setTextMatrix(434, 334+incremento);
				cb.showText(dia );
				cb.setTextMatrix(489, 334+incremento);
                              // mes=getmesenletra( Integer.parseInt(mes) );
				cb.showText(mes );
				//cb.showText("SEPTIEMBRE" );
				cb.setTextMatrix(553, 334+incremento);
				cb.showText(ano );

				 Calendar calendario=new GregorianCalendar();

					String minutos_ = String.valueOf(calendario.get(Calendar.MINUTE));

					if(minutos_.trim().length()==1)
						minutos_ = "0"+minutos_;
					String mihora =calendario.get(Calendar.HOUR_OF_DAY )+":"+minutos_;

					cb.setTextMatrix(553, 323+incremento);
					cb.showText( mihora);
					cb.setTextMatrix(553, 313);
					cb.showText( mihora);



				       if (fecha.equals("today")){
                                           setFechaMes( String.valueOf( dt.getMonth() + 1 ) );
	                                   if (mes.length()==1){
                                              mes= "0" + mes;
                                           }
	                                   if (dia.length()==1){
                                               dia= "0" + dia;
                                           }


				           setFecha( FechHabil( dia +"/"+ mes +"/"+ ano ,30).trim() );
					       //dia =  String.valueOf( Integer.parseInt( desentramaS( fecha +"/", '/',1) ));

					       dia =  desentramaS( fecha +"/", '/', 4);
				           //mes =  String.valueOf( Integer.parseInt( desentramaS( fecha +"/", '/',2) ));
                                        mes =  desentramaS( fecha +"/", '/', 2);
				           //ano =  String.valueOf( Integer.parseInt( desentramaS( fecha +"/", '/',3) ));
                                         ano =  desentramaS( fecha +"/", '/', 3);
                                       int g=0;
                                        g=Integer.parseInt(mes);

				           String auxmesCaduco = getmesenletra(g);

				            leyenda= "Esta solicitud caduca el d�a "+ dia +" de "+auxmesCaduco + " del " +ano;




				        }else{
				   		    //System.out.println("la fecha para imprimir recibida es : "+ fecha);
					         diaCaduco = desentramaS( fecha +"/", '/', 4);
                                             //System.out.println("la fecha para imprimir recibida es : "+ diaCaduco);
				            mesCaduco = desentramaS( fecha +"/", '/', 2);

				            anoCaduco = desentramaS( fecha +"/", '/', 3);
				               String hola=diaCaduco +"/"+ mesCaduco + "/" + anoCaduco;
                                            hola= hola.trim();
				            setFecha( FechHabil( hola ,30) );


					         diaCaduco =  desentramaS( fecha +"/", '/',1).trim();

                                             //System.out.println("la fecha para imprimir recibida2 es : "+ diaCaduco);

                                              mesCaduco =  desentramaS( fecha +"/", '/',2).trim();
 //System.out.println("la fecha para imprimir recibida2 mes= : "+ mesCaduco);
                                       int g=0;
                                        g=Integer.parseInt(mesCaduco);

				            anoCaduco =  String.valueOf( Integer.parseInt( desentramaS( fecha +"/", '/',3) ));

				            String auxmesCaduco = getmesenletra(g);
				             leyenda= "Esta solicitud caduca el d�a "+ diaCaduco +" de "+auxmesCaduco + " del " +anoCaduco;




				           }

			                    cb.setFontAndSize(bf, 7);

				cb.setTextMatrix(400, 454);
				cb.showText(leyenda );
				cb.setTextMatrix(400, 61);
				cb.showText(leyenda );

				              cb.setFontAndSize(bf, 9);

						cb.setTextMatrix(50, 674);
						cb.showText(contrato );

						cb.setTextMatrix(50, 287);
						cb.showText(contrato );












			cb.endText();

			documento.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return baos;
	}

	public void setCodigoCliente(String string)
	{
			codigocliente = string;
	}
	public void setNombre(String string)
	{
			nombre = string;
	}
	public void setDireccion(String string)
	{
			direccion = string;
	}
	public void setTelefono(String string)
	{
			telefono = string;
	}
	public void setCorreo(String string)
	{
			correo = string;
	}

	public void setPaterno(String string)
	{
			paterno = string;
	}
	public void setMaterno(String string)
	{
			materno = string;
	}
	public void setFolio(String string)
	{
			folio = string;
	}

	public void setFecha(String string)
	{
			fecha = string;
	}
	public void setFechaDia(String string)
	{
			dia = string;
	}
	public void setFechaMes(String string)
	{
			mes = string;
	}
	public void setFechaAno(String string)
	{
			ano = string;
	}
	public void setFechaDiaCaduca(String string)
	{
			diaCaduco = string;
	}
	public void setFechaMesCaduca(String string)
	{
			mesCaduco = string;
	}
	public void setFechaAnoCaduca(String string)
	{
			anoCaduco = string;
	}


	public void setTelefono2(String string)
	{
			telefonoII = string;
	}
	public void setCodArea(String string)
	{
			areacode = string;
	}
	public void setCodAreaAlter(String string)
	{
		areacodealt = string;
	}
	public void setcontrato(String string)
	{
		contrato = string;
	}

	public void setrazonsocial(String string)
	{
		razonsocial = string;
	}

	public void setclientecontrato(String string)
	{
		clientecontrato = string;
	}

	 public String FechHabil(String FechaIn, int Dias)
     {
           int iNumeroDias = 0;

         int iDiasAsueto = 0;
         int iDiasHabiles = 0;
         int iCountSabDom = 0;
         int TotalDias=0;
         TotalDias=getDateHabiles(FechaIn, Dias);//numero de dias finales

         int iYear = Integer.parseInt(FechaIn.substring(6, 10));
         int iMonth = Integer.parseInt(FechaIn.substring(3, 5));
         int iDay = Integer.parseInt(FechaIn.substring(0, 2));
         Calendar cldInicio = Calendar.getInstance(_currentLocale);
         cldInicio.setFirstDayOfWeek(cldInicio.MONDAY);
         cldInicio.set(iYear, iMonth - 1, iDay);

         int iDiaSemana = cldInicio.get(cldInicio.DAY_OF_WEEK);
         int iDInicio = cldInicio.get(cldInicio.DAY_OF_YEAR);

         Calendar cldFin = Calendar.getInstance(_currentLocale);
         cldFin.setFirstDayOfWeek(cldFin.MONDAY);
         cldFin.set(iYear, iMonth - 1, iDay);

         cldFin.add(cldFin.DATE, TotalDias);

         int iDiaSemanaFin = cldFin.get(cldFin.DAY_OF_WEEK);
         int iDFin = cldFin.get(cldFin.DAY_OF_YEAR);

         DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM,_currentLocale);
         Date d = cldFin.getTime();

         return df.format(d);

     }

	   public int getDateHabiles(String FechaIni, int Dias){
	         int countd=0;
	         int contapasadas=0;

	         int iYear = Integer.parseInt(FechaIni.substring(6, 10));
	         int iMonth = Integer.parseInt(FechaIni.substring(3, 5));
	         int iDay = Integer.parseInt(FechaIni.substring(0, 2));
     _currentLocale = new Locale("es", "MX");
         SimpleDateFormat dfFormat = new SimpleDateFormat("dd/MM/yyyy",_currentLocale);
	         Calendar cldInicio = Calendar.getInstance(_currentLocale);
	         cldInicio.setFirstDayOfWeek(cldInicio.MONDAY);
	         int iDiaSemana =0;
	         while(Dias>=countd)
	         {
	                  contapasadas++;
	                  cldInicio.set(iYear, iMonth - 1, iDay);
	                  iDiaSemana=cldInicio.get(cldInicio.DAY_OF_WEEK);
	                  if(iDiaSemana==1 || iDiaSemana== 7)
	                  {
	                      System.out.println("Entro en el if");
	                      countd++;
	                  }else{

	                  countd++;

	                  }
	                 iDay++;

	         }


	       return contapasadas-1;


	    }

	   public  String desentramaS(String cadena, char caracter, int x)
	     {
	      String vartmp=" ";
	         String regtmp=" ";

	         int y=0;
	         String camposTabla="";
	         regtmp = cadena;
	         for (int i = 0; i < regtmp.length(); i++)
	      {

	        if (regtmp.charAt(i) == caracter){

	        	y=y+1;


	           if (y==x){
	        	 //System.out.println("retorna " + vartmp);
	        	 camposTabla= vartmp;
	        	  break;
	        	}
	            if (y==1 || y==2 ){
	            	vartmp="";
	            }
	        }else
	        	if (x!=y){
	        		vartmp=vartmp + regtmp.charAt(i);
	        	    }

	      }
	        if (camposTabla.length() == 1){

	         	 camposTabla= "0" + camposTabla;
	         }
	      return camposTabla;
	     }

	public String getmesenletra(int x){

		String mes="";
		switch ( x )
		{
			case  1: mes = "Enero";		 break;
			case  2: mes = "Febrero";	 break;
			case  3: mes = "Marzo";		 break;
			case  4: mes = "Abril";		 break;
			case  5: mes = "Mayo";       break;
			case  6: mes = "Junio";		 break;
			case  7: mes = "Julio";		 break;
			case  8: mes = "Agosto";	 break;
			case  9: mes = "Septiembre"; break;
			case 10: mes = "Octubre";	 break;
			case 11: mes = "Noviembre";	 break;
			case 12: mes = "Diciembre";  break;
			default: mes = "Enero";
		}
		return mes;
	}




}

