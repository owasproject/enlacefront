
package mx.altec.enlace.bo;
import java.util.Vector;

import mx.altec.enlace.utilerias.EIGlobal;

public class bc_CuentaProgFechas extends TI_Cuenta
	{
	private String numCta;
	private String fechaIni;
	private String fechaFin;
	private String horaIni;
	private String horaFin;
	private String techo;
	private String usr;
	private int periodicidad;		//0 = diario, 2-6 = luneas-viernes
	private String lineaCred;
	private String horarioOper1;
	private String horarioOper2;
	private String vigencia1;
	private String vigencia2;
	private String techopre;
	private int tipo;
	private Vector horarios;
	private double importe;

	//este campo se usa cuando la cuenta se crea de una trama
	private TI_Cuenta padre;
	public String posiblePadre;


	// --- Constructores e inicializadores
	public bc_CuentaProgFechas(TI_Cuenta unaCuenta)
		{
		super(unaCuenta.getNumCta(),unaCuenta.getDescripcion());
		if(unaCuenta instanceof bc_CuentaProgFechas)
			{
			numCta =		((bc_CuentaProgFechas)unaCuenta).numCta;
			fechaIni =		((bc_CuentaProgFechas)unaCuenta).fechaIni;
			fechaFin =		((bc_CuentaProgFechas)unaCuenta).fechaFin;
			horaIni =		((bc_CuentaProgFechas)unaCuenta).horaIni;
			horaFin =		((bc_CuentaProgFechas)unaCuenta).horaFin;
			techo = 		((bc_CuentaProgFechas)unaCuenta).techo;
			usr =			((bc_CuentaProgFechas)unaCuenta).usr;
			horarios =		(Vector)((bc_CuentaProgFechas)unaCuenta).horarios.clone();
			lineaCred =		((bc_CuentaProgFechas)unaCuenta).lineaCred;
			horarioOper1 =	((bc_CuentaProgFechas)unaCuenta).horarioOper1;
			horarioOper2 =	((bc_CuentaProgFechas)unaCuenta).horarioOper2;
			vigencia1 =		((bc_CuentaProgFechas)unaCuenta).vigencia1;
			vigencia2 =		((bc_CuentaProgFechas)unaCuenta).vigencia2;
			techopre =		((bc_CuentaProgFechas)unaCuenta).techopre;
			}
		else
			inicializa();
		}

	public bc_CuentaProgFechas(String numCuenta)
		{
		super(numCuenta);
		inicializa();
		}

	private void inicializa()
		{
		numCta = "";
		fechaIni = "";
		fechaFin = "";
		horaIni = "";
		horaFin = "";
		techo = "";
		usr = "";
		horarios = new Vector();
		lineaCred = "";						//campos adicionales
		horarioOper1 = "";
		horarioOper2= "";
		vigencia1= "";
		vigencia2= "";
		techopre = "0.0";

		padre = null;
		posiblePadre = "";
		}

	// --- Indica y regresa la cuenta ------------------------------------------------
	public void setNumCta(String numCta)
		{
		this.numCta = numCta;
		}

	public String getNumCta()
		{
		return numCta;
		}
	// --- Indica y regresa Fecha de Inicio ------------------------------------------------
	public void setFechaIni(String numCta)
		{
		this.fechaIni = fechaIni;
		}

	public String getFechaIni()
		{
		return fechaIni;
		}
	// --- Indica y regresa Fecha final ------------------------------------------------
	public void setFechaFin(String numCta)
		{
		this.fechaFin = fechaFin;
		}

	public String getFechaFin()
		{
		return fechaFin;
		}

	// --- indica y regresa Horario Inicio
	public void setHoraIni(String horaIni){
		this.horaIni = horaIni;
	}

	public String getHoraIni(){
		return horaIni;
	}

	// --- indica y regresa Horario Fin
	public void setHoraFin(String horaFin){
		this.horaFin = horaFin;
	}

	public String getHoraFin(){
		return horaFin;
	}

	// --- indica y regresa Techo
	public void setTecho(String techo){
		this.techo = techo;
	}

	public String getTecho(){
		return techo;
	}

	// --- indica y regresa Usuario
	public void setUsr(String usr){
		this.usr = usr;
	}

	public String getUsr(){
		return usr;
	}

	// --- Indica la nueva periodicidad ------------------------------------------------
	public boolean setPeriodicidad(int per)
		{
		if(per<2 && per>6 && per!=0) return false;
		periodicidad = per;
		return true;
		}

	// --- Indica el nuevo importe -----------------------------------------------------
	public boolean setImporte(double importe)
		{
		if(importe < 0.01) return false;
		importe = ((double)(long)(importe*100))/100;
		this.importe = importe;
		return true;
		}

	// --- Devuelve el importe ---------------------------------------------------------
	public double getImporte()
		{return importe;}

	// --- Indica la linea de cr�dito asociado a una Cuenta
	public String getLineaCred()
		{
			return lineaCred;
		}
	// --- Actualiza a la linea de cr�dito
	public void setLineaCred(String lc)
		{
			this.lineaCred = lc;
		}
	// ---
	public String getHorarioOper1()
		{return horarioOper1;}

	// ---
	public void setHorarioOper1(String xho1)
		{horarioOper1 = xho1;}

	// ---
	public String getHorarioOper2()
		{return horarioOper2;}

	// ---
	public void setHorarioOper2(String xho2)
		{horarioOper2 = xho2;}

	// ---
	public String getVigencia1()
		{return vigencia1;}

	// ---
	public void setVigencia1(String xv1)
		{vigencia1 = xv1;}

	// ---
	public String getVigencia2()
		{return vigencia2;}

	// ---
	public void setVigencia2(String xv2)
		{vigencia2 = xv2;}


	// --- Devuelve el importe actual
	public String getTechopre()
		{return this.techopre;}

	// --- Setea al  importe correspondiente
	public void setTechopre(String tp)
		{techopre = tp;}

	// --- Indica el tipo para la cuenta -----------------------------------------------
	public void setTipo(int tipo)
		{this.tipo = tipo;}

	// --- Devuelve el tipo de la cuenta -----------------------------------------------
	public int getTipo()
		{return tipo;}

	// --- Regresa el horario de operacion
	//public Vector getHorarios()
	//	{return (Vector)horarios.clone();}
	public String getHorarios()
		{return "De " + getHorarioOper1() + " a " + getHorarioOper2();}

	// --- Regresa la Vigencia es el par de fechas

	public boolean agregaHorario(String horario)
		{

		//PENDIENTE: validar el formato del horario

		if(horarios.size()==0) {horarios.add(horario); return true;}
		int indice = 0;
		while(indice < horarios.size())
			{
			if(horario.compareTo((String)horarios.get(indice))<0)
				{horarios.add(indice,horario); return true;}
			else if(horario.compareTo((String)horarios.get(indice)) == 0)
				{return true;}
			indice++;
			}
		horarios.add(horario);
		return true;
		}

	public String getVigencia()
		{
			return "Del  " + getVigencia1() + "  a  " + getVigencia2();
		}

	// --- Devuelve el padre de la cuenta ----------------------------------------------
	public TI_Cuenta getPadre()
		{return padre;}

	// --- Indica el nuevo padre -------------------------------------------------------
	public boolean setPadre(TI_Cuenta newPadre)
		{
		if (newPadre.getNumCta().equals(getNumCta())) return false;
		if (desciendeDe(newPadre)) return false;
		if (((bc_CuentaProgFechas)newPadre).desciendeDe(this)) return false;
		padre = newPadre;
		return true;
		}

	// --- indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(String numCta)
		{
		TI_Cuenta cuenta = padre;
		boolean resultado = false;
		while(!resultado)
			{
			if(cuenta == null) break;
			if(numCta.equals(cuenta.getNumCta())) resultado = true;
			if(cuenta instanceof bc_CuentaProgFechas)
				cuenta = ((bc_CuentaProgFechas)cuenta).getPadre();
			else
				cuenta = null;
			}
		return resultado;
		}

	// --- Indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(TI_Cuenta posiblePadre)
		{
		return desciendeDe(posiblePadre.getNumCta());
		}

	// --- Regresa el nivel de la cuenta
	public int nivel()
		{
		TI_Cuenta cuenta = padre;
		int resultado = 1;
		while(true)
			{
			if(cuenta == null) break;
			if(cuenta instanceof bc_CuentaProgFechas)
				cuenta = ((bc_CuentaProgFechas)cuenta).getPadre();
			else
				cuenta = null;
			resultado++;
			}
		return resultado;
		}

	// --- Regresa una trama conteniendo datos de la cuenta
	public String trama()
		{
		StringBuffer trama = new StringBuffer ("");
					 trama = new StringBuffer ("@");
					 trama.append (getNumCta());						//Cuenta
					 trama.append ("|");
					 trama.append (vigencia1);							//Fecha Inicio
					 trama.append ("|");
					 trama.append (vigencia2);							//Fecha Fin
					 trama.append ("|");
					 trama.append (horarioOper1);						//Hora Inicio
					 trama.append ("|");
					 trama.append (horarioOper2);						//Hora Fin
					 trama.append ("|");
					 trama.append (techopre);							//Techo
					 trama.append ("|");
					 trama.append (((getUsr()=="")?" ":getUsr()));		//Usuario
					 trama.append ("|");
					 trama.append (((getDescripcion()=="")?" ":getDescripcion())); //Descripcion
		return trama.toString();
		}


	// --- Igual que el anterior pero tomando en cuenta a posiblePadre
	public String tramaPosible()
		{
		StringBuffer trama = new StringBuffer ("");
		trama = new StringBuffer ("@");
		trama.append (getNumCta());						//Cuenta
		trama.append ("|");
		trama.append (vigencia1);							//Fecha Inicio
		trama.append ("|");
		trama.append (vigencia2);							//Fecha Fin
		trama.append ("|");
		trama.append (horarioOper1);						//Hora Inicio
		trama.append ("|");
		trama.append (horarioOper2);						//Hora Fin
		trama.append ("|");
		trama.append (techopre);							//Techo
		trama.append ("|");
		trama.append (((getUsr()=="")?" ":getUsr()));		//Usuario
		trama.append ("|");
		trama.append (((getDescripcion()=="")?" ":getDescripcion())); //Descripcion
		return trama.toString();
		}

	// --- METODO ESTATICO: Crea una nueva cuenta a partir de una trama
	public static bc_CuentaProgFechas creaCuentaFechas(String trama)
		{
		int car;
		String strAux;
		bc_CuentaProgFechas cuenta = null;

		if(trama == null) return null;
		if(!trama.substring(0,1).equals("@")) return null;
		trama = trama.substring(1);  // quitamos el @
		car = trama.indexOf("|"); if(car == -1) return null;  // Posicion 1 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Cuenta
		cuenta = new bc_CuentaProgFechas(strAux);  // inicializa variables
		cuenta.setNumCta(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 2 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Fecha inicio
		cuenta.setVigencia1(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 3 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Fecha fin
		cuenta.setVigencia2(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 4 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Hora inicio
		cuenta.setHorarioOper1(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 5 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Hora fin
		cuenta.setHorarioOper2(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 6 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Techo
		cuenta.setTechopre(strAux);
		car = trama.indexOf("|"); if(car == -1) return null; // Posicion 7 de: |
		strAux = trama.substring(0,car); trama = trama.substring(car+1);	// Usuario
		cuenta.setUsr(strAux);
		strAux = trama;
		cuenta.setDescripcion(strAux);						 // Posicion 8 Descripcion*/
		return cuenta;
		}

//	}

// --- ESTATICO: Copia una cuenta pero con otro num de cuenta ----------------------
	public static bc_CuentaProgFechas copiaCuenta(String neoNumCta, bc_CuentaProgFechas cuenta)
		{
		bc_CuentaProgFechas neoCuenta = new bc_CuentaProgFechas(neoNumCta);

		neoCuenta.setDescripcion(cuenta.getDescripcion());
		neoCuenta.setLineaCred(cuenta.getLineaCred());
		neoCuenta.setHorarioOper1(cuenta.getHorarioOper1());
		neoCuenta.setHorarioOper2(cuenta.getHorarioOper2());
		neoCuenta.setVigencia1(cuenta.getVigencia1());
		neoCuenta.setVigencia2(cuenta.getVigencia2());
		neoCuenta.setTechopre(cuenta.getTechopre());
		neoCuenta.posiblePadre = cuenta.posiblePadre;
		return neoCuenta;
		}

	// ------------------------
	public static void main(String args[])
		{
		EIGlobal.mensajePorTrace("Creando cuenta...", EIGlobal.NivelLog.INFO);
		bc_CuentaProgFechas c1 = new bc_CuentaProgFechas("111111111");
		c1.setDescripcion("Cuenta 1");
		c1.setLineaCred("Linea de cr�dito");
		c1.setHorarioOper1("Horario 1");
		c1.setHorarioOper2("Horario 2");
		c1.setVigencia1("Vigencia 1");
		c1.setVigencia2("Vigencia 2");
		c1.setTechopre("Techo");
		EIGlobal.mensajePorTrace("Cuenta creada...", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Probando trama...", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("La trama de c1 es: " + c1.trama(), EIGlobal.NivelLog.INFO);
		String strAux = c1.trama();
		EIGlobal.mensajePorTrace("Creando cuenta a partir de trama...", EIGlobal.NivelLog.INFO);
		bc_CuentaProgFechas c2 = bc_CuentaProgFechas.creaCuentaFechas(strAux);
		EIGlobal.mensajePorTrace("Todo hecho.", EIGlobal.NivelLog.INFO);
		}
	}
