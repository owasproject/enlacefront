package mx.altec.enlace.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConsultaCuentasBean;
import mx.altec.enlace.beans.CuentaBean;
import mx.altec.enlace.dao.ConsultaCtasAsociadasDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ConfigModEdoCtaTDCBO {
	

		/**
		 * Metodo para obtener las TDC asociadas al contrato para el modulo de Administracion y Control.
		 * @since 27/02/2015
		 * @author FSW-Indra
		 * @param userID de tipo String
		 * @param contractNumber de tipo String
		 * @param userProfile de tipo String
		 * @return listaCuentasMasiva de tipo List<ConfEdosCtaArchivoBean>
		 */
		public List<ConfEdosCtaArchivoBean> consultaTDCAdmCtr(String userID, String contractNumber, String userProfile) {
			List<ConfEdosCtaArchivoBean> listaCuentasMasiva;
			List<ConsultaCuentasBean> listaTarjetas;
			ConfEdosCtaArchivoBean confEdosCtaArchivoBean;
			ConsultaCtasAsociadasDAO clase = new ConsultaCtasAsociadasDAO();
			EIGlobal.mensajePorTrace("ConfigModEdoCtaTDCBO.consultaTDCAdmCtr Se van a obtener las cuentas", NivelLog.INFO);
			listaCuentasMasiva = new ArrayList<ConfEdosCtaArchivoBean>();
			try {
				listaTarjetas = clase.getCuentasTotales(contractNumber, userProfile, userID);
				for (ConsultaCuentasBean consultaCuentasBean : listaTarjetas) {
					confEdosCtaArchivoBean = new ConfEdosCtaArchivoBean();
					confEdosCtaArchivoBean.setNomCuenta(consultaCuentasBean.getNumCuenta());
					confEdosCtaArchivoBean.setNombreTitular(consultaCuentasBean.getNDescripcion());
					confEdosCtaArchivoBean.setTipoCuenta(consultaCuentasBean.getTipoRelacCtas());
					listaCuentasMasiva.add(confEdosCtaArchivoBean);
				}					                         
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("ConfigModEdoCtaTDCBO.consultaTDCAdmCtr ["+e.getMessage()+"]", NivelLog.ERROR);
			}
			return listaCuentasMasiva;
		}
		
		/**
		 * Metodo para obtener las TDC asociadas al contrato para el modulo de Administracion y Control.
		 * @since 27/02/2015
		 * @author FSW-Indra
		 * @param userID de tipo String
		 * @param contractNumber de tipo String
		 * @param userProfile de tipo String
		 * @return listaCuentasMasiva de tipo List<ConfEdosCtaArchivoBean>
		 */
		public List<CuentaBean> consultaTarjetasCredito(String userID, String contractNumber, String userProfile) {
			List<CuentaBean> listaCuentasMasiva;
			List<ConsultaCuentasBean> listaTarjetas;			
			ConsultaCtasAsociadasDAO clase = new ConsultaCtasAsociadasDAO();
			EIGlobal.mensajePorTrace("ConfigModEdoCtaTDCBO.consultaTDCAdmCtr Se van a obtener las cuentas", NivelLog.INFO);
			listaCuentasMasiva = new ArrayList<CuentaBean>();
			try {
				listaTarjetas = clase.getTarjetasCredito(contractNumber, userProfile, userID);
				for (ConsultaCuentasBean consultaCuentasBean : listaTarjetas) {
					
					

					CuentaBean beanCuentas = new CuentaBean();
					beanCuentas.setNumCuenta(consultaCuentasBean.getNumCuenta());
					beanCuentas.setTipoCuenta(consultaCuentasBean.getTipoRelacCtas());
					beanCuentas.setDescripcion(consultaCuentasBean.getNDescripcion());
					beanCuentas.setTitular(consultaCuentasBean.getNDescripcion());
					beanCuentas.setTipoProd("5");
					
					/*confEdosCtaArchivoBean = new ConfEdosCtaArchivoBean();
					confEdosCtaArchivoBean.setNomCuenta(consultaCuentasBean.getNumCuenta());
					confEdosCtaArchivoBean.setNombreTitular(consultaCuentasBean.getNDescripcion());
					confEdosCtaArchivoBean.setTipoCuenta(consultaCuentasBean.getTipoRelacCtas());
					*/
					listaCuentasMasiva.add(beanCuentas);
				}					                         
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("ConfigModEdoCtaTDCBO.consultaTDCAdmCtr ["+e.getMessage()+"]", NivelLog.ERROR);
			}
			return listaCuentasMasiva;
		}
		

}
