/*
 * confProveedor.java
 *
 * Created on 2 de mayo de 2002, 12:47 PM
 */

package mx.altec.enlace.bo;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class confProveedor {

    protected String claveProveedor;
    protected String nombreProveedor;

    /** Creates new confProveedor */
    public confProveedor () {
    }

    /**
     * Metodo para crear conProveedor
     * @param clave Clave del provedor
     * @param nombre Nombre completo del proveedor
     */
    public confProveedor (String clave, String nombre) {
        claveProveedor = clave;
        nombreProveedor = nombre;
    }

    public void setClaveProveedor (String clave) {claveProveedor = clave;}
    public void setNombreProveedor (String nombre) {nombreProveedor = nombre;}

    public String getClaveProveedor () {return claveProveedor;}
    public String getNombreProveedor () {return nombreProveedor;}

}
