package mx.altec.enlace.bo;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.NominaLineaBean;
import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.dao.NominaLineaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.ServicioMensajesUSR;

/**
 * 
 * @author Z712236
 *
 */
public class NominaLineaBO {

	/**
	 * TITULO_NOTIFICACION titulo de notificacion
	 */
	private final String TITULO_NOTIFICACION = "Notificaci" + (char)243 + "n a usuario";
	/**
	 * titulo del Mail
	 */
	private String subject;
	/**
	 *  estatusSMS para indicar las operaciones mancomunadas
	 */
	private String estatusSMS="";
	
	/**
	 * Consulta de archivos con paginacion
	 * @param consulta bean con filtros de la consulta
	 * @param req para obtener y pasar parametros de consulta
	 * @param res para enviar respuesta de la consulta
	 * @return NominaLineaBean devuelve el resultado de la consulta
	 * @throws SQLException control de excepciones sql
	 */
	public NominaLineaBean consultarArchivos(
			NominaLineaBean consulta, HttpServletRequest req,
			HttpServletResponse res) throws SQLException {
		
		EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: Iniciando ConsultaPaginacion", EIGlobal.NivelLog.INFO);
		NominaLineaBean resultados=new NominaLineaBean();
		
		/****************** PAGINACION CONSULTA *************************/
		/**
		 * maxPagina cantidad de registros por pagina
		 */
		Integer maxPagina = 0;
		try {
			final String registrosPagina = Global.registrosPaginanln;//registros por pagina
			maxPagina=Integer.parseInt(registrosPagina);
			final Integer maxQuery = consulta.getPaginacion().getPagina() * maxPagina;
			final Integer minQuery = maxQuery - (maxPagina-1);

			EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: maxQuery: "+maxQuery, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: minQuery: "+minQuery, EIGlobal.NivelLog.INFO);
			
			final NominaLineaDAO dao=new NominaLineaDAO();
			resultados = dao.consultarArchivos(consulta, minQuery, maxQuery);
			EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: consulta ejecutada: "+minQuery, EIGlobal.NivelLog.INFO);
			
			float respuesta =0;
			
			//Procedimiento para consultar un bloque de registros de n a n, segun la pagina
			respuesta = resultados.getPaginacion().getTotalRegs();
			resultados.getPaginacion().setPaginasTotales( Math.round((Math.round((respuesta / maxPagina))>= respuesta / maxPagina ? 
					Math.abs((respuesta / maxPagina)):(respuesta / maxPagina)+1)));
			EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: paginasTotales: "+resultados.getPaginacion().getPaginasTotales(), EIGlobal.NivelLog.INFO);
			resultados.getPaginacion().setPagina(consulta.getPaginacion().getPagina());//Se setea en el resultado la pagina
			
			//Procedimiento para pintar el encabezado Del n - Al n (opcional, no afecta la paginacion)
			int maximo = 0;
			if(resultados.getPaginacion().getPaginasTotales() == consulta.getPaginacion().getPagina()){
				final int total = Integer.parseInt(registrosPagina)*resultados.getPaginacion().getPaginasTotales();
				final int val = total - resultados.getPaginacion().getTotalRegs();
				maximo = total - val;
				req.getSession().setAttribute("sesDel", minQuery);
				req.getSession().setAttribute("sesAl", maximo);
			}else{
				req.getSession().setAttribute("sesDel", minQuery);
				req.getSession().setAttribute("sesAl", maxQuery);
			}
			EIGlobal.mensajePorTrace("NominaLineaBO.java :: consultarArchivos :: Registros del: "+minQuery+" al: "+maximo, EIGlobal.NivelLog.INFO);
		}catch (NumberFormatException e) {
			EIGlobal.mensajePorTrace("NominaLineaBo::consultarArchivos::NumberFormatException " + e.toString(), EIGlobal.NivelLog.INFO);
		}
		/**************** FIN PAGINACION CONSULTA ***********************/

		return resultados;
	}
	
	/**
	 * Inserta datos del archivo de Nomina en Linea
	 * @param bean contiene los datos a registrar
	 * @return insert bandera de ejecusion de la operacion
	 */
	public boolean registraNomlinArch(NominaLineaBean bean){
		/**
		 * insert bandera de ejecusion
		 */
		EIGlobal.mensajePorTrace("NominaLineaBO.java::inicio registraNomlinArch", EIGlobal.NivelLog.DEBUG);
		boolean insert=false;
		
		NominaLineaDAO dao=new NominaLineaDAO();
		insert=dao.registraNomlinArch(bean);
		EIGlobal.mensajePorTrace("NominaLineaBO.java::fin registraNomlinArch :: " + insert, EIGlobal.NivelLog.DEBUG);
		return insert;
	}
	
	/**
	 * Envio de notificaciones
	 * @param request respuesta del servidor
	 * @param operRealizada operarion que se realiza
	 * @param bean parametro de datos
	 */
	public void sendNotificacion(HttpServletRequest request, NominaLineaBean bean) {
		EIGlobal.mensajePorTrace("NominaLineaBO.java :: inicio sendNotificacion", EIGlobal.NivelLog.DEBUG);
			try{
					setSubject(TITULO_NOTIFICACION);
					bean.setEstatus(obtenDescripcionEstatus(bean.getEstatus()));
					String mensajeCorreo="";
					String mensajeSMS="";
					StringBuffer mensajeInicio = new StringBuffer();
					StringBuffer mensajeSmsBuffer = new StringBuffer();
					String divisa="";

					Date fechaActual = new Date();
					SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:MM");
				    String hoy = formato.format(fechaActual);
				    
				    String fecha[]=hoy.split("/");
					
					EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_NOMINA", EIGlobal.NivelLog.DEBUG);
					mensajeCorreo=msgTextNotificacionPagoNomina(bean).toString();
					mensajeSmsBuffer
						.append(" Programacion pago Nomina en Linea ").append(estatusSMS)
						.append(bean.getCtaCargo().substring(bean.getCtaCargo().length()-4, bean.getCtaCargo().length())).append(", ")
						.append(bean.getNumRegistros())
						.append(" registros por ").append(bean.getImporteAplicado());//La fecha se concatena en ServicioMensajeUSR.java

					mensajeInicio.append("Santander Enlace ").append(bean.getContrato().substring(bean.getContrato().length()-4, bean.getContrato().length()));
					mensajeSMS = mensajeInicio.toString().concat(" ").concat(mensajeSmsBuffer.toString());
					BaseResource session = (BaseResource) request.getSession().getAttribute("session");
					MensajeUSR mensajeUSR = session.getMensajeUSR();

					if(mensajeUSR !=null){
					ServicioMensajesUSR servicioMensajesUSR=new ServicioMensajesUSR();
					EIGlobal.mensajePorTrace("SMS[" + mensajeSMS+"]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("CORREO[" + mensajeCorreo+"]", EIGlobal.NivelLog.DEBUG);
				    servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), mensajeSMS, mensajeCorreo, this.subject);

					}

			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}

		EIGlobal.mensajePorTrace("EmailSender::Fin sendConfirmacionEmail", EIGlobal.NivelLog.DEBUG);

	}
	
	/**
	 * setSubject Subject del correo
	 * @param subject asunto de correo
	 */
	public void setSubject(String subject){
		this.subject = subject;
	}

	/**
	 * getSubject obtener subject de correo
	 * @return subject asunto de correo
	 */
	public String getSubject(){
		return this.subject;
	}
	
	/**
	 * msgTextNotificacionPagoNomina arma cuerpo del correo
	 * @param bean parametros de entrada
	 * @return StringBuffer cadena con cuerpo de correo
	 */
	public StringBuffer msgTextNotificacionPagoNomina(NominaLineaBean bean) {
		EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);

		StringBuffer bodyNotificacion = new StringBuffer();


		bodyNotificacion.append("<HTML>");
		bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
		bodyNotificacion.append("<BODY> ");
		bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

		bodyNotificacion.append("<br>");
		bodyNotificacion.append("<br>");
		bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
		bodyNotificacion.append("</B> de <B>" + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
		bodyNotificacion.append("</B> de <B>" + obtenCalendario().get(Calendar.YEAR) + "</B>");
		bodyNotificacion.append(", en su contrato Enlace <B>"+bean.getContrato().substring(bean.getContrato().length()-4, bean.getContrato().length())+"</B> a nombre de <B>"+bean.getRazonSocial()+"</B> se realiz� el env�o de su archivo de <B>Pago de N�mina en L�nea");
		bodyNotificacion.append(" </B> con folio(s) <B>"+bean.getNumRef()+"</B>, conforme a lo siguiente:<br><br>");
		bodyNotificacion.append("<CENTER><TABLE border = 1>");
		bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+bean.getCtaCargo().substring(bean.getCtaCargo().length()-4, bean.getCtaCargo().length())+"&nbsp;&nbsp;</B></td></tr>");
		bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+bean.getNumTransmision()+"&nbsp;&nbsp;</B></td></tr>");
		bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Enviados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+bean.getNumRegistros()+"&nbsp;&nbsp;</B></td></tr>");
		bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+bean.getImporteAplicado()+"&nbsp;&nbsp;</B></td></tr>");
		bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
		bodyNotificacion.append("<B>El sistema se encuentra validando sus datos y una vez que concluya puede revisar el estatus de la dispersi�n ingresando a la ruta: ");
		
		bodyNotificacion.append(" Servicios / N�mina / N�mina en L�nea  / Consulta. ");

		bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
		bodyNotificacion.append("<br>");
		bodyNotificacion.append("<br>");
		bodyNotificacion.append("</FONT>");

		bodyNotificacion.append("</BODY></HTML> ");

		EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);
		return bodyNotificacion;
	}
	
	/**
	 * Metodo encargado de crear el Calendario que se imprimira en el mail.
	 * @return calendar			Calendario de trabajo
	 */
	public static Calendar obtenCalendario(){
		// Obteniendo GMT-08:00 (hora estandar pacifica)
		 String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
		 // Si no regresa ids , algo esta mal
		 if (ids.length == 0)
			 EIGlobal.mensajePorTrace("Algo esta mal", EIGlobal.NivelLog.ERROR);

		 // Creando el timeZone para la hora standard del pacifico
		 SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);

		 // Poniendo las reglas de cambio de horario
		 pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
		 pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

		 // Creando un GregorianCalendar con las especificaciones del pacifico
		 Calendar calendar = new GregorianCalendar(pdt);
		 Date trialTime = new Date();
		 calendar.setTime(trialTime);

		 return calendar;
	}
	
	/**
	 * Metodo para obtenet el nombre del mes de la notificacion del mail
	 *
	 * @param mes		Numero entero con el mes
	 * @return 	String	Regresa el nombre del mes
	 */
	public static String obtenNombreMes(int mes){
		switch(mes){
		case 0: return "Enero";
		case 1: return "Febrero";
		case 2: return "Marzo";
		case 3: return "Abril";
		case 4: return "Mayo";
		case 5: return "Junio";
		case 6: return "Julio";
		case 7: return "Agosto";
		case 8: return "Septiembre";
		case 9: return "Octubre";
		case 10: return "Noviembre";
		case 11: return "Diciembre";
		default: return "";
		}
	}
	
	/**
	 * obtenDescripcionEstatus obtiene descricpion de estatus
	 * @param codigo_error respuesta de descripcion
	 * @return String cadena con cod error
	 */
	public String obtenDescripcionEstatus(String codigo_error)
	{
		 if(codigo_error.length()>9 && codigo_error.indexOf("-")>-1)
			 return obtenDescripcionEstatusMultiple( codigo_error);

		 else
			  return obtenDescripcionEstatusUnico( codigo_error);


	}
	
	/**
	 * obtenDescripcionEstatusMultiple descripcion de estatus multiple
	 * @param codigo_error codigo de error
	 * @return String cod error
	 */
	private String obtenDescripcionEstatusMultiple(String codigo_error)
	{EIGlobal.mensajePorTrace("EmailSender::codError-->"+codigo_error, EIGlobal.NivelLog.DEBUG);
		String  estatus		= "";
		try{
			String	tipo_error	= "";
			String	terminacion	= "";
			int operManc=0;
			int operProg=0;
			int operTrans=0;
			int operAcep=0;
			int operRechazada=0;

			String[] codError=codigo_error.split("-");

			EIGlobal.mensajePorTrace("EmailSender::codError.length-->"+codError.length, EIGlobal.NivelLog.DEBUG);
			for(int i=0;i<codError.length;i++){
					EIGlobal.mensajePorTrace("EmailSender::codError["+i+"]-->"+codError[i], EIGlobal.NivelLog.DEBUG);
					if(codError[i]!=null && codError[i].length()==8){

						tipo_error	=  codError[i].substring( 0, 4);
						terminacion =  codError[i].substring( 4,8 );

						if ( !tipo_error.equals( "MANC" ) ){
							if ( !tipo_error.equals( "PROG" ) ){
								if ( codError[i].equals( "TRANSITO" ) )
									{	//estatus = "T"; // en tr�nsito
										operTrans++;

									}
								else if ( !terminacion.equals( "0000" ) )
								{
									//estatus = "R"; // rechazado
									operRechazada++;
								}
								else{
									//estatus = "A"; // aceptado
									operAcep++;
									}
							}else{

									if ( !terminacion.equals( "0000" ) )
									{
										//estatus = "R";
										operRechazada++;
									}
									else{
										//estatus = "P"; // programado
										operProg++;
									}

							} // if ( PROG )
						}else{
							//estatus = "M"; // mancomunada
							operManc++;
						} // if ( MANC )



				}

			}
			 if(operAcep>0)
					estatus += " "+operAcep+" Aceptada ";
				 if(operManc>0){
					 estatus += " "+operManc+" Mancomunada ";
					 estatusSMS=" Mancomunada ";
				 }
				 if(operRechazada>0)
					 estatus += " "+operRechazada+" Rechazada ";
				 if(operProg>0)
					 estatus += " "+operProg+" Programada ";


		}
		catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
			EIGlobal.mensajePorTrace("EmailSender::ESTATUS["+codigo_error+"]-->"+estatus, EIGlobal.NivelLog.DEBUG);
		return estatus;
	}
	
	/**
	 * obtenDescripcionEstatusUnico obtiene desc de estatus unico
	 * @param codigo_error codigo de error
	 * @return String cod error
	 */
	private String obtenDescripcionEstatusUnico(String codigo_error)
	{
      String  estatus		= "";
		try{
			String	tipo_error	= "";
			String	terminacion	= "";

			EIGlobal.mensajePorTrace("EmailSender::codigo_error["+codigo_error+"]", EIGlobal.NivelLog.DEBUG);
			tipo_error	= codigo_error.substring( 0, 4);
			terminacion = codigo_error.substring( 4,8 );
			EIGlobal.mensajePorTrace("EmailSender::tipo_error["+tipo_error+"]", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("EmailSender::terminacion["+terminacion+"]", EIGlobal.NivelLog.DEBUG);

			if ( !tipo_error.equals( "MANC" ) ){
				if ( !tipo_error.equals( "PROG" ) ){
					if ( codigo_error.equals( "TRANSITO" ) )
						estatus = "T"; // en tr�nsito
					else if ( !terminacion.equals( "0000" ) )
						estatus = "R"; // rechazado
					else
						estatus = "A"; // aceptado
				} else {
					if ( !terminacion.equals( "0000" ) )
						estatus = "R";
					else
						estatus = "P"; // programado
				} // if ( PROG )
			} else{
				if (tipo_error.equals("ADMU") || tipo_error.equals("MLUS")) {
					 if ( !terminacion.equals( "0000") ){
						 estatus = "";
					 } else {
						 estatus = "X";
					 }
				} else {
					estatus = "M"; // mancomunada
				}
			} // if ( MANC )

			// Estatus
			if      ( estatus.equals( "A" ) ) estatus = "Aceptada";
			else if ( estatus.equals( "R" ) ) estatus = "Rechazada";
			else if ( estatus.equals( "M" ) ){ estatus = "Mancomunada"; estatusSMS=" Mancomunada ";}
			else if ( estatus.equals( "P" ) ) estatus = "Programada";
		//	else if ( estatus.equals( "N" ) ) estatus = "Pendiente";
			else estatus = "No catalogado";

		}
		catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
			EIGlobal.mensajePorTrace("EmailSender::ESTATUS["+codigo_error+"]-->"+estatus, EIGlobal.NivelLog.DEBUG);
		return estatus;
	}

	/**
	 * validaDuplicadoNomlinArch valida archivos duplicados 
	 * con base al contrato, num_registros e importe
	 * @param bean parametros de entrada
	 * @return duplicado archivos duplicados
	 */
	public boolean validaDuplicadoNomlinArch(NominaLineaBean bean){
		EIGlobal.mensajePorTrace("NominaLineaBO.java - validaDuplicadoNomlinArch() :: Entrando al metodo...", EIGlobal.NivelLog.ERROR);
		boolean duplicado=false;
		NominaLineaDAO dao = new NominaLineaDAO();
		try {
			final SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
			final Date fecha=new Date();
			final String fchCarga=f.format(fecha);	
			bean.setFchCarga(fchCarga);
			
			duplicado=dao.validaDuplicadoNomlinArch(bean);
			EIGlobal.mensajePorTrace("NominaLineaBO.java - validaDuplicadoNomlinArch() :: duplicado: "+duplicado, EIGlobal.NivelLog.ERROR);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("NominaLineaBO.java - validaDuplicadoNomlinArch() - "+ "Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
		}
		return duplicado;
	}
	
}
