package mx.altec.enlace.bo;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.AsignacionBean;
import mx.altec.enlace.beans.ConsultaAsignacionTEResBean;
import mx.altec.enlace.beans.LMXF;
import mx.altec.enlace.beans.LMXC;
import mx.altec.enlace.beans.LMXD;
import mx.altec.enlace.beans.LMXE;
import mx.altec.enlace.beans.RemesasBean;
import mx.altec.enlace.beans.TarjetasBean;
import mx.altec.enlace.dao.AdmonRemesasDAO;
import mx.altec.enlace.dao.ConsultaAsignacionDAO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * @author ESC Clase que realiza las regals de negocio en la
 *         administraci&oacute;n y consulta de remesas y ejecuta el llamado al
 *         DAO.
 */
public class AdmonRemesasBO {

	private static final String textoLog = AdmonRemesasBO.class.getName();

	/**
	 * Consulta remesas por contrato y ctro distribuci&oacute;n
	 * 
	 * @param bean
	 *            LMXC
	 * @return LMXC
	 */
	public LMXC consultaRemesas(LMXC bean, boolean isExcel) {
		EIGlobal.mensajePorTrace(textoLog + "consultaRemesas BO",
				EIGlobal.NivelLog.DEBUG);
		LMXC resultado = null;
		ArrayList<RemesasBean> listaRem = new ArrayList<RemesasBean>();
		MQQueueSession mqsess = null;
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		try {
			mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			dao.setMqsess(mqsess);
			if (isExcel) {
				boolean hayMasDatos = true;

				while (hayMasDatos) {
					resultado = dao.consultaRemesas(bean);
					listaRem.addAll(resultado.getListaRemesas());
					hayMasDatos = resultado.isExistenMasRegistros();
					bean.setEntidad(resultado.getEntidad());
					bean.setContratoEnlace(resultado.getContratoEnlace());
					bean.setCtroDistribucion(resultado.getCtroDistribucion());
					bean.setNumeroRemesa(resultado.getNumeroRemesa());
					bean.setEstadoRemesa(resultado.getEstadoRemesa());
					bean.setFechaInicio(resultado.getFechaInicio());
					bean.setFechaFinal(resultado.getFechaFinal());
					bean.setCtoEnlacePag(resultado.getCtoEnlacePag());
				}
				resultado.setListaRemesas(listaRem);
			} else {
				resultado = dao.consultaRemesas(bean);				
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + " Error " + e,
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				mqsess.close();
			} catch (Exception x) {
				EIGlobal.mensajePorTrace(textoLog + "Error: [" + x + "]",
						EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}

	/**
	 * Consulta remesas por contrato, ctro distribuci&oacute;n, numero de
	 * remesa, fecha inicio, fecha final y consultar por los estatus
	 * seleccionados ademas de ejecutar el llamado al DAO
	 * 
	 * @param bean
	 *            LMXC
	 * @return LMXC
	 */
	public LMXC consultaRemesasVarias(LMXC bean, String tipoFecha,
			String estatus, String fecha1, String fecha2) {
		EIGlobal.mensajePorTrace(textoLog
				+ "##################333333consultaRemesasVarias BO",
				EIGlobal.NivelLog.DEBUG);
		LMXC resultado = new LMXC();
		MQQueueSession mqsess = null;
		ArrayList<RemesasBean> listaRemesas = new ArrayList<RemesasBean>();
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		try {
			mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);

			dao.setMqsess(mqsess);

			if (estatus != null && !estatus.equals("")) {
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::consultaRemesasVarias:: ENTRO 1", EIGlobal.NivelLog.DEBUG);
				bean.setEstadoRemesa(estatus);
				bean.setFechaInicio(fecha1.replaceAll("/", "-"));
				bean.setFechaFinal(fecha2.replaceAll("/", "-"));
				boolean existenMasDatos = true;
				while (existenMasDatos) {
					LMXC res = dao.consultaRemesas(bean);

					if (res.getListaRemesas() != null) {
						listaRemesas.addAll(res.getListaRemesas());
					} else {
						listaRemesas = new ArrayList<RemesasBean>();
					}
					existenMasDatos = res.isExistenMasRegistros();

					resultado.setCodExito(res.isCodExito());
					resultado.setCodigoOperacion(res.getCodigoOperacion());
					EIGlobal.mensajePorTrace("########REGRESO CODIGO"
							+ res.getCodigoOperacion() + " error"
							+ res.getMensError() + "exito=" + res.isCodExito(),
							EIGlobal.NivelLog.DEBUG);
					bean.setCtoEnlacePag(bean.getContratoEnlace());
					bean.setCtroDistribucionPag(bean.getCtroDistribucion());
					if(listaRemesas!=null && listaRemesas.size()>0)
						bean.setNumRemesaPag(listaRemesas.get(listaRemesas.size() - 1).getNumRemesa());					
				}	

			} else {
				EIGlobal.mensajePorTrace("AdmonRemesasServlet::consultaRemesasVarias:: ENTRO 2", EIGlobal.NivelLog.DEBUG);
				boolean isFinDatos = false;
				boolean consultaEfectuada = false;
				while (!isFinDatos && !consultaEfectuada) {
					LMXC res = dao.consultaRemesas(bean);
					if (res.getListaRemesas() != null) {
						listaRemesas.addAll(res.getListaRemesas());
					} else {
						listaRemesas = new ArrayList<RemesasBean>();
					}
					isFinDatos = res.isFinDatos();
					consultaEfectuada = res.isConsultaEfectuada();
					resultado.setCodExito(res.isCodExito());
					resultado.setCodigoOperacion(res.getCodigoOperacion());
					EIGlobal.mensajePorTrace("########REGRESO CODIGO"
							+ res.getCodigoOperacion() + " error"
							+ res.getMensError() + "exito=" + res.isCodExito(),
							EIGlobal.NivelLog.DEBUG);
					bean.setCtoEnlacePag(bean.getContratoEnlace());
					bean.setCtroDistribucionPag(bean.getCtroDistribucion());
					bean.setNumRemesaPag(listaRemesas.get(
							listaRemesas.size() - 1).getNumRemesa());
				}
			}
			resultado.setListaRemesas(listaRemesas);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + " Se controlan problemas " + e.getMessage(),
					EIGlobal.NivelLog.INFO);
			//e.printStackTrace();
		} finally {
			try {
				mqsess.close();
			} catch (Exception x) {
				EIGlobal.mensajePorTrace(textoLog + " Se controlan problemas " + x.getMessage(),
						EIGlobal.NivelLog.INFO);
			}
		}
		return resultado;
	}

	/**
	 * Realiza el llamado al DAO para cambiar el estatus de la remesa.
	 * 
	 * @param bean
	 *            LMXD
	 * @return LMXD
	 */
	public LMXD recibirRemesa(LMXD bean) {
		EIGlobal.mensajePorTrace(textoLog + "recibirRemesa BO",
				EIGlobal.NivelLog.DEBUG);
		LMXD resultado = null;
		MQQueueSession mqsess = null;
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		try {
			mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			dao.setMqsess(mqsess);
			resultado = dao.recibirRemesa(bean);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + " Error " + e,
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				mqsess.close();
			} catch (Exception x) {
				EIGlobal.mensajePorTrace(textoLog + "Error: [" + x + "]",
						EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}

	/**
	 * Consulta el detalle de una remesa mediante el DAO.
	 * 
	 * @param bean
	 *            LMXE
	 * @return LMXE
	 */
	public LMXE consultaDetalleRemesa(LMXE bean, boolean isExcel) {
		EIGlobal.mensajePorTrace(textoLog + "consultaDetalleRemesa BO",
				EIGlobal.NivelLog.DEBUG);
		LMXE resultado = null;
		MQQueueSession mqsess = null;
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		ArrayList<TarjetasBean> listaTar = new ArrayList<TarjetasBean>();
		try {
			mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			dao.setMqsess(mqsess);
			if (isExcel) {
				boolean isFinDatos = false;
				while (!isFinDatos) {
					resultado = dao.consultaDetalleRemesa(bean);
					if (resultado.isExistenMasDatos()) {
						bean.setTarjeta(resultado.getListaTarjetas().get(
								resultado.getListaTarjetas().size() - 1)
								.getTarjeta());
						resultado.getListaTarjetas().remove(
								resultado.getListaTarjetas().size() - 1);

					}
					listaTar.addAll(resultado.getListaTarjetas());
					isFinDatos = resultado.isFinDatos();
					EIGlobal.mensajePorTrace(
							"Entro a traer los datos de las tarjeta###########################"
									+ "hay mas="
									+ resultado.isExistenMasDatos(),
							EIGlobal.NivelLog.ERROR);

				}
				resultado.setListaTarjetas(listaTar);
			} else {
				resultado = dao.consultaDetalleRemesa(bean);
				resultado.setTarjeta(resultado.getListaTarjetas().get(
						resultado.getListaTarjetas().size() - 1).getTarjeta());
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + " Error " + e,
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				mqsess.close();
			} catch (Exception x) {
				EIGlobal.mensajePorTrace(textoLog + "Error: [" + x + "]",
						EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}

	public void cancelarRemesa() {

	}

	/**
	 * Cancela la tarjeta mediante la transaccion LMX8
	 * 
	 * @param bean
	 *            LMX8
	 * @return LMX8
	 */
	public LMXF cancelarTarjeta(LMXF bean) {
		EIGlobal.mensajePorTrace(textoLog + "recibirRemesa BO",
				EIGlobal.NivelLog.DEBUG);
		LMXF resultado = null;
		MQQueueSession mqsess = null;
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		try {
			mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			dao.setMqsess(mqsess);
			resultado = dao.cancelarTarjeta(bean);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(textoLog + " Error " + e,
					EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				mqsess.close();
			} catch (Exception x) {
				EIGlobal.mensajePorTrace(textoLog + "Error: [" + x + "]",
						EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}
	
	public int cancelartarjetaEnlace(
			AsignacionBean consulta, HttpServletRequest req,
			HttpServletResponse res) throws Exception {
		AdmonRemesasDAO dao = new AdmonRemesasDAO();
		
		return dao.cancelartarjetaEnlace(consulta);
	}
}
