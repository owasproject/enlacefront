package mx.altec.enlace.bo;

import java.util.List;


import mx.altec.enlace.beans.ConsEdoCtaDetalleHistBean;
import mx.altec.enlace.beans.ConsultaEdoCtaHistBean;
import mx.altec.enlace.dao.ConsultaEdoCtaHistDAO;
import mx.altec.enlace.utilerias.EIGlobal;

import org.apache.commons.lang.StringUtils;

public class ConsultaEdoCtaHistBO {
	
	/**
	 * edoCtaHistDao
	 */
	private final transient ConsultaEdoCtaHistDAO edoCtaHistDao = new ConsultaEdoCtaHistDAO();
	
	
	/**
	 * @param consultaEdoCtaHistBean : consultaEdoCtaHistBean
	 * @return creado : creado
	 */
	public String consultaEdoCtaDetalleTotalHist(ConsultaEdoCtaHistBean consultaEdoCtaHistBean){
		
		EIGlobal.mensajePorTrace( "ConsultaEdoCtaHistBO-> consultaEdoCtaHistDetalleTotalHist ", EIGlobal.NivelLog.DEBUG);
		
		StringBuilder linea = new StringBuilder();
		List<ConsEdoCtaDetalleHistBean> list = edoCtaHistDao.consultaEdoCtaDetalleHistTotal(consultaEdoCtaHistBean);
		
		if(list != null && !list.isEmpty()){
			linea.append("Folio de Solicitud|Estatus|Numero de cuenta / Tarjeta|Periodo|Descripcion de Cuenta / Tarjeta|Secuencia de Domicilio de la Cuenta|Fecha solicitud|Usuario Solicitante").append("\r\n");
			for(int i=0;i<list.size();i++){
				
				ConsEdoCtaDetalleHistBean temp = (ConsEdoCtaDetalleHistBean)list.get(i);
				linea.append((StringUtils.isBlank(temp.getIdEdoCtaHist())?"":temp.getIdEdoCtaHist().trim())).append("|")
				.append((StringUtils.isBlank(temp.getEstatus())?"":temp.getEstatus().trim())).append("|")
				.append((StringUtils.isBlank(temp.getNumCuenta())?"":temp.getNumCuenta().trim())).append("|")
				.append((StringUtils.isBlank(temp.getPeriodo())?"":temp.getPeriodo().trim())).append("|")
				.append((StringUtils.isBlank(temp.getDescCta())?"":temp.getDescCta().trim())).append("|")
				.append((StringUtils.isBlank(temp.getNumSec())?"":temp.getNumSec().trim())).append("|")
				.append((StringUtils.isBlank(temp.getFchSol())?"":temp.getFchSol().trim())).append("|")
				.append((StringUtils.isBlank(temp.getIdUsuario())?"":temp.getIdUsuario().trim())).append("\r\n");
				
			}
		}else{
			linea.append("Error al Obtener la informacion");
		}
		return linea.toString();

	}
	
	/**
	 * Realiza la consulta del detalle
	 * @param consultaEdoCtaHistBean : consultaEdoCtaHistBean
	 * @return List<ConsEdoCtaDetalleHistBean> : List<ConsEdoCtaDetalleHistBean>
	 */
	public List<ConsEdoCtaDetalleHistBean> consultaEdoCtaHistDetalle(ConsultaEdoCtaHistBean consultaEdoCtaHistBean){

		return edoCtaHistDao.consultaEdoCtaHistDetalle(consultaEdoCtaHistBean);
	}
}
