package mx.altec.enlace.bo;

/**
 *
 * @
 * @
 */
public class Multi {

    protected String Nombre;
    protected String Apellido;
	protected String ApellidoM;

    /** Creates new Multi */
    public Multi () {
    }

    /**
     * Metodo para crear conProveedor
     * @param clave Clave del provedor
     * @param nombre Nombre completo del proveedor
     */
    public Multi (String Nom, String Ape1, String Ape2) {
        Nombre = Nom;
        Apellido = Ape1;
		ApellidoM = Ape2;
    }

    public void setNombre (String Nom) {Nombre = Nom;}
    public void setApellidoP (String Ape1) {Apellido = Ape1;}
	public void setApellidoM (String Ape2) {ApellidoM = Ape2;}

    public String getNombre () {return Nombre;}
    public String getApellidoP () {return Apellido;}
	public String getApellidoM () {return ApellidoM;}

}
