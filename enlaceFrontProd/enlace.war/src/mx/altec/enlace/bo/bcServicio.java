/*
 * bcServicio.java
 *
 * Created on 23 de abril de 2002, 03:24 PM
 */

package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 * Realiza la coneccion con tuxedo y regresa las respuestas necesarias  para cada peticion especifica
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class bcServicio {

    /** Tipo de respuesta a utilizar
     */
    protected static final java.lang.String MEDIO_ENTREGA = "EWEB";

    /** Holds value of property servicio. */
    private ServicioTux servicio;

    /** Holds value of property contrato. */
    private java.lang.String contrato;

    /** Holds value of property usuario. */
    private java.lang.String usuario;

    /** Holds value of property clavePerfil. */
    private java.lang.String clavePerfil;

    /** Holds value of property cuenta. */
    private String cuenta;

    /** Creates new bcServicio */
    public bcServicio() {
        servicio = new ServicioTux();
    }

    /** Getter for property servicio.
     * @return Value of property servicio.
     */
    public ServicioTux getServicio() {
        return servicio;
    }


    /** Setter for property contrato.
     * @param contrato New value of property contrato.
     */
    public void setContrato(java.lang.String contrato) {
        this.contrato = contrato;
    }


    /** Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /** Setter for property clavePerfil.
     * @param clavePerfil New value of property clavePerfil.
     */
    public void setClavePerfil(java.lang.String clavePerfil) {
        this.clavePerfil = clavePerfil;
    }

    /** <code>getOperacionesProgramadas</code> hace la consulta por medio de tuxedo
     * @return ArrayList con los resultados
     * @throws Exception si hay error en las propiedades de servicio
     */
    public java.util.ArrayList getOperacionesProgramadas()
                                throws mx.altec.enlace.bo.bcConsultaException, Exception    {
        verificaNulos(true);
        java.util.ArrayList al = new java.util.ArrayList();

        String trama = "N" + "@" + this.cuenta + "| |";

        trama = generaTrama("2","B0PC", trama);
		EIGlobal.mensajePorTrace(getClass().getName() + ": tramaSalida =" + trama , EIGlobal.NivelLog.INFO);
        File file = null;
        java.io.BufferedReader rFile = null;
        try{
            java.util.Hashtable ht = servicio.web_red(trama);

            if( null == ht ) throw new mx.altec.enlace.bo.bcConsultaException("No se pudo hacer la coneccion a la base de datos");

            trama = (java.lang.String) ht.get("BUFFER");
			EIGlobal.mensajePorTrace(getClass().getName() + ": tramaEntrada =" + trama , EIGlobal.NivelLog.INFO);
            if(null == trama || trama.equals("")) throw new mx.altec.enlace.bo.bcConsultaException("No se pudo hacer la coneccion a la base de datos");

            file = this.recibe( trama );
            if( null == file) throw new java.io.IOException("bcServicio:getOperacionesProgramadas - No se pudo obtener el archivo");
            /* NORMAL CODE */
            rFile = new java.io.BufferedReader(new java.io.FileReader(file));

            /* DEBUG CODE
            EIGlobal.mensajePorTrace(getClass ().getName () + " ARCHIVO DE PRUEBAS <debug code> " + "/tmp/1001851.txt.tmp", EIGlobal.NivelLog.INFO);
            java.io.RandomAccessFile rFile = new java.io.RandomAccessFile("/tmp/1001851.txt.tmp","r");
            */
            trama = rFile.readLine();
            EIGlobal.mensajePorTrace( getClass ().getName () + " " + trama , EIGlobal.NivelLog.INFO);
            if( (null == trama) ||
                (! trama.startsWith("OK")) ){
                EIGlobal.mensajePorTrace("bcServicio:getOperacionesProgramadas - " + trama.substring(16) , EIGlobal.NivelLog.INFO);
                throw new mx.altec.enlace.bo.bcConsultaException(trama.substring(16));
            }
                /*
OK      00082291BASE0001No existen programaciones para la cuenta 65500144939
                 */

            while(null != (trama = rFile.readLine() ) ){
                bcOperacionProgramada operacion = new bcOperacionProgramada();
                EIGlobal.mensajePorTrace( getClass ().getName () + " " + trama , EIGlobal.NivelLog.INFO);
                operacion.set(trama);
                al.add( operacion );
            }

        } catch (java.io.IOException e) {
            throw new Exception ("bcServicio:getRelacionCuentas - Error en la lectura de Operaciones\n" + e.getMessage());
        }catch (bcConsultaException e){
            throw e;
        }catch (Exception e) {
            EIGlobal.mensajePorTrace("bcServicio:getRelacion de cuentas - " + e.getMessage(), EIGlobal.NivelLog.INFO);
            throw e;
        } finally {
            // 20021203 RMM
            if(null != rFile){
                try{
                    rFile.close();
                    rFile = null;
                }catch(java.io.IOException ex){
                }
            }
            if(null != file){
                file = null;
            }
        }

        return al;
    }

    /** verifica si los campos son nulos o vacios
     * @param all indica si se debe revisar todos los campos
     * @throws Exception en caso de que haya errores en los campos
     *
     */
    protected void verificaNulos( boolean all )
                    throws Exception{
        if ( null == clavePerfil ){
            throw new Exception("bcServicio:verificaNulos - clavePerfil es null, asignar un valor previamente...");
        } else if ( null == contrato ){
            throw new Exception("bcServicio:verificaNulos - contrato es null, asignar un valor previamente...");
        } else if ( null == usuario ){
            throw new Exception("bcServicio:verificaNulos - usuario es null, asignar un valor previamente...");
        } else if ( all &&  (null == cuenta) ){
            throw new Exception("bcServicio:verificaNulos - cuenta es null, asignar un valor previamente...");
        } else if ( clavePerfil.equals("") ){
            throw new Exception("bcServicio:verificaNulos - clavePerfil = \"\", asignar un valor valido previamente...");
        } else if ( contrato.equals("") ){
            throw new Exception("bcServicio:verificaNulos - contrato = \"\", asignar un valor valido previamente...");
        } else if ( usuario.equals("") ){
            throw new Exception("bcServicio:verificaNulos - usuario = \"\", asignar un valor valido previamente...");
        } else if ( all && (cuenta.equals("")) ){
            throw new Exception("bcServicio:verificaNulos - cuenta = \"\", asignar un valor valido previamente...");
        }
    }

    /** Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    /** Obtiene la relacion de cuentas y lieas de credito
     * @return un Array list con las cuentas, y por cada cuenta un arraylist para las lineas de credito
     * @throws Exception si hay algun error
     * @throws bcConsultaException Si la consulta no se pudo realizar, aqui esta el mensaje recuperado de tuxedo
     */
    public java.util.ArrayList getRelacionDeCuentas()
                        throws mx.altec.enlace.bo.bcConsultaException, Exception{
        EIGlobal.mensajePorTrace("bcServicio:getRelacionDeCuentas - Inicia", EIGlobal.NivelLog.INFO);
        verificaNulos(false);

        java.util.ArrayList al = new java.util.ArrayList();
        java.lang.String trama = generaTrama("2","B0AM");
		EIGlobal.mensajePorTrace(getClass().getName() + ": tramaSalida =" + trama , EIGlobal.NivelLog.INFO);

        File file = null;
        java.io.BufferedReader rFile = null;
        try{
            //Enviar a tuxedo
            EIGlobal.mensajePorTrace("bcServicio:getRelacionDeCuentas - envia a tuxedo", EIGlobal.NivelLog.INFO);
            java.util.Hashtable ht = servicio.web_red(trama);

            //obtener el path del archivo
            EIGlobal.mensajePorTrace("bcServicio:getRelacionDeCuentas - obteniendo el path del archivo", EIGlobal.NivelLog.INFO);
            trama = (java.lang.String) ht.get("BUFFER");
            EIGlobal.mensajePorTrace(getClass().getName() + ": tramaEntrada =" + trama , EIGlobal.NivelLog.INFO);

            if(null == trama || trama.equals("")) throw new Exception("bcServicio:getRelacionDeCuentas - No se pudo obtener el path del archivo");

            //obtener el archivo
            file = this.recibe( trama );
            if( null == file) throw new java.io.IOException("bcServicio:getRelacionDeCuentas - No se pudo obtener el archivo");

            rFile = new java.io.BufferedReader(new java.io.FileReader(file));

            trama = rFile.readLine();
            EIGlobal.mensajePorTrace( getClass ().getName () + " " + trama , EIGlobal.NivelLog.INFO);
            if(null == trama ||
                (! trama.startsWith("OK")) ){
                EIGlobal.mensajePorTrace("bcServicio:getrelacionDeCuentas - " + trama.substring(16) , EIGlobal.NivelLog.INFO);
                throw new mx.altec.enlace.bo.bcConsultaException(trama.substring(16));
            }


            while(null != ( trama = rFile.readLine() )){//leer cada una de las cuentas
                EIGlobal.mensajePorTrace( getClass ().getName () + " " + trama , EIGlobal.NivelLog.INFO);
                pdTokenizer tok = new pdTokenizer(trama,"|", 2);
                bcCuentaLineaCredito cuenta = new bcCuentaLineaCredito();
                try{
                    cuenta.setCuenta(tok.nextToken());
                    while(0 < tok.countTokens() ){
                        cuenta.addLineaCredito( tok.nextToken());
                    }
                } catch (Exception e) {
                    EIGlobal.mensajePorTrace("bcServicio:getRelacionCuentas - Error en la lectura de cuenta", EIGlobal.NivelLog.INFO);
                }
                al.add(cuenta);
            }

        } catch (java.io.IOException e) {
            throw new Exception ("bcServicio:getRelacionCuentas - Error en la lectura de cuentas\n" + e.getMessage());
        }catch (bcConsultaException e){
            throw e;
        } catch (Exception e) {
            EIGlobal.mensajePorTrace("bcServicio:getRelacion de cuentas - " + e.getMessage(), EIGlobal.NivelLog.INFO);
            throw e;
        } finally {
            if (null != rFile) {
                try{
                    rFile.close();
                    rFile = null;
                }catch(java.io.IOException ex){
                }
            }
            if(null != file){
                file = null;
            }
        }
        EIGlobal.mensajePorTrace( getClass ().getName () + " Cuentas" + al.size () , EIGlobal.NivelLog.INFO);
        return al;
    }

    /** verifica si los campos son nulos o vacios
     * @param tipoRespuesta es el tipo de respuesta que se espera 1, para trama, 2 para archivo
     * @param tipoOperacion es el tipo de operacion a utilizar
     * @return String con la trama a enviar
     */
    protected java.lang.String generaTrama(java.lang.String tipoRespuesta,java.lang.String tipoOperacion){
        java.lang.String trama = tipoRespuesta + MEDIO_ENTREGA + "|";
        if( tipoOperacion.equals("B0AM") ||
            tipoOperacion.equals("B0PB") ||
            tipoOperacion.equals("B0PC") ){
            trama +=    this.usuario + "|" +
                        tipoOperacion + "|" +
                        this.contrato + "|" +
                        this.usuario + "|" +
                        this.clavePerfil + "|";

        }

        return trama;
    }

    /** obtiene un archivo desde un directorio remoto.
     * @return File con el resultado
     * @param Nombre el nombre del archivo a recibir
     */
    protected File recibe(String Nombre) {
        EIGlobal.mensajePorTrace("bcServicio:recibe - Inicia", EIGlobal.NivelLog.INFO);
        try {
        	
        	//IF PROYECTO ATBIA1 (NAS) FASE II
            File Archivo = new File(
                    Nombre);
           	boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();
           
           	if(!envArch.copiaCUENTAS(Nombre, Global.DIRECTORIO_LOCAL)){
				
					EIGlobal.mensajePorTrace("*** bcServicio.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** bcServicio.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
           	
           	if (Respuestal) {
                
                //Archivo.setReadOnly();
                return Archivo;
                
           	}
           	
        } catch (Exception ex) {
            EIGlobal.mensajePorTrace("bcServicio:recibe - " + ex.getMessage(), EIGlobal.NivelLog.INFO);
            ex.printStackTrace(System.err);
        }
        EIGlobal.mensajePorTrace("bcServicio:recibe - sale con error", EIGlobal.NivelLog.INFO);
        return null;
    }

    protected java.lang.String generaTrama(java.lang.String tipoRespuesta, java.lang.String tipoOperacion, java.lang.String tramaUsuario) {
        java.lang.String trama = generaTrama(tipoRespuesta, tipoOperacion);
        if( tipoOperacion.equals("B0PB") ||
            tipoOperacion.equals("B0PC") ){
            trama += tramaUsuario;
        }
        return trama;
    }

    public void cancelaOp(mx.altec.enlace.bo.bcOperacionProgramada operacion) throws Exception {
        verificaNulos(false);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("ddMMyyyy");
        String trama = operacion.getNoCuenta() + "@" +
                        "N" + "@" +
                        sdf.format(operacion.getFechaIni().getTime()) + "@" +
                        sdf.format(operacion.getFechaFin().getTime()) + "@" +
                        "| |";

        trama = generaTrama("1", "B0PB", trama);
        EIGlobal.mensajePorTrace(getClass().getName() + ": tramaSalida =" + trama , EIGlobal.NivelLog.INFO);
        java.util.Hashtable ht = servicio.web_red(trama);
        if(null == ht)throw new Exception("No se logro eliminar la programacion");
        trama = (java.lang.String) ht.get("BUFFER");
        EIGlobal.mensajePorTrace("bcServicio:cancelaOp - resultado = " + trama, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(getClass().getName() + ": tramaEntrada =" + trama , EIGlobal.NivelLog.INFO);
        if (null == trama || trama.substring(0,8).trim().equals("")) throw new Exception("Problemas Con la comunicacion");


/*
OK          8246BASE0000Transaccion Exitosa
*/
        if(!trama.startsWith("OK") ) throw new Exception (trama.substring(16));
		else throw new Exception(trama.substring(24));


    }

}