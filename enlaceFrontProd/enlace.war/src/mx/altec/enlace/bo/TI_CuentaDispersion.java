package mx.altec.enlace.bo;
import mx.altec.enlace.utilerias.EIGlobal;
import java.util.Vector;

public class TI_CuentaDispersion extends TI_Cuenta
	{

	// --- Campos ----------------------------------------------------------------------
	private int periodicidad;		//0 = diario, 2-6 = luneas-viernes
	private Vector horarios;
	private double importe;
	private boolean loConcentrado;
	private TI_Cuenta padre;
	private boolean concentracion;
	private int tipo;

	//este campo se usa cuando la cuenta se crea de una trama
	public String posiblePadre;

	// --- Constructores e inicializadores ---------------------------------------------
	public TI_CuentaDispersion(TI_Cuenta cuenta)
		{
		super(cuenta.getNumCta(),cuenta.getDescripcion());
		if(cuenta instanceof TI_CuentaDispersion)
			{
			periodicidad = ((TI_CuentaDispersion)cuenta).periodicidad;
			horarios = (Vector)((TI_CuentaDispersion)cuenta).horarios.clone();
			importe = ((TI_CuentaDispersion)cuenta).importe;
			loConcentrado = ((TI_CuentaDispersion)cuenta).loConcentrado;
			padre = ((TI_CuentaDispersion)cuenta).padre;
			concentracion = ((TI_CuentaDispersion)cuenta).concentracion;
			tipo = ((TI_CuentaDispersion)cuenta).tipo;
			}
		else
			inicializa();
		}

	public TI_CuentaDispersion(String numCuenta)
		{
		super(numCuenta);
		inicializa();
		}

	private void inicializa()
		{
		periodicidad = 0;
		horarios = new Vector();
		importe = 0.0;
		loConcentrado = false;
		padre = null;
		concentracion = false;
		tipo = 0;
		posiblePadre = "";
		}

	// --- Indica la nueva periodicidad ------------------------------------------------
	public boolean setPeriodicidad(int per)
		{
			EIGlobal.mensajePorTrace("PERIODICIDAD A GUARDAR:"+per, EIGlobal.NivelLog.INFO);
		if(per<2 && per>6 && per!=0) return false;
		periodicidad = per;
		return true;
		}

	// --- Devuelve la periodicidad ----------------------------------------------------
	public int getPeriodicidad()
		{return periodicidad;}

	// --- Devuelve una cadena con la periodicidad -------------------------------------
			//YHG Modiifca r dias
	public String getStrPeriodicidad()
		{
		String retorno ="diario";
		switch(periodicidad)
			{
			case 1: retorno = "Lunes"; break;
			case 2: retorno = "Martes"; break;
			case 3: retorno = "Mi&eacute;rcoles"; break;
			case 4: retorno = "Jueves"; break;
			case 5: retorno = "Viernes"; break;
			case 6: retorno = "S&aacute;bado"; break;
			case 7: retorno = "Domingo"; break;
			}
		return retorno;
		}

	// --- Indica el nuevo importe -----------------------------------------------------
	public boolean setImporte(double importe)
		{
		if(importe < 0.01) return false;
		importe = ((double)(long)(importe*100))/100;
		this.importe = importe;
		return true;
		}

	// --- Devuelve el importe ---------------------------------------------------------
	public double getImporte()
		{return importe;}

	// --- Devuelve una cadena con el importe ------------------------------------------
	public String getStrImporte()
		{
		if(loConcentrado) return "Lo concentrado";
		String cadena = "" + importe;
		if(cadena.indexOf("E")!=-1)
			{
			int orden = Integer.parseInt(cadena.substring(cadena.indexOf("E")+1));
			cadena = cadena.substring(0,cadena.indexOf("E"));
			while(cadena.length() < orden + 3) cadena += "0";
			cadena = cadena.substring(0,1) + cadena.substring(2);
			cadena = cadena.substring(0,orden + 1) + "." + cadena.substring(orden + 1);
			}
		if(cadena.indexOf(".") == cadena.length() - 2) cadena += "0";
		for(int a=cadena.length()-6;a>0;a-=3) cadena = cadena.substring(0,a) + "," + cadena.substring(a);
		return "$" + cadena;
		}

	// --- Indica si se dispersa lo concentrado ----------------------------------------
	public void setLoConcentrado(boolean val)
		{loConcentrado = val;}

	// --- Devuelve loConcentrado ------------------------------------------------------
	public boolean getLoConcentrado()
		{return loConcentrado;}

	// --- Indica el nuevo padre -------------------------------------------------------
	public boolean setPadre(TI_Cuenta newPadre)
		{
		if (newPadre.getNumCta().equals(getNumCta())) return false;
		if (desciendeDe(newPadre)) return false;
		if (((TI_CuentaDispersion)newPadre).desciendeDe(this)) return false;
		padre = newPadre;
		return true;
		}

	// --- Devuelve el padre de la cuenta ----------------------------------------------
	public TI_Cuenta getPadre()
		{return padre;}

	// --- indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(String numCta)
		{
		TI_Cuenta cuenta = padre;
		boolean resultado = false;
		while(!resultado)
			{
			if(cuenta == null) break;
			if(numCta.equals(cuenta.getNumCta())) resultado = true;
			if(cuenta instanceof TI_CuentaDispersion)
				cuenta = ((TI_CuentaDispersion)cuenta).getPadre();
			else
				cuenta = null;
			}
		return resultado;
		}

	// --- Indica si la cuenta desciende de la especificada ----------------------------
	public boolean desciendeDe(TI_Cuenta posiblePadre)
		{
		return desciendeDe(posiblePadre.getNumCta());
		}

	// --- Indica los nuevos horarios --------------------------------------------------
	public void setHorarios(Vector newHorarios)
		{horarios = (Vector)newHorarios.clone();}

	// --- Devuelve un vector con los horarios -----------------------------------------
	public Vector getHorarios()
		{return (Vector)horarios.clone();}

	// --- Agrega un horario -----------------------------------------------------------
	public boolean agregaHorario(String horario)
		{

		//PENDIENTE: validar el formato del horario

		if(horarios.size()==0) {horarios.add(horario); return true;}
		int indice = 0;
		while(indice < horarios.size())
			{
			if(horario.compareTo((String)horarios.get(indice))<0)
				{horarios.add(indice,horario); return true;}
			else if(horario.compareTo((String)horarios.get(indice)) == 0)
				{return true;}
			indice++;
			}
		horarios.add(horario);
		return true;
		}

	// --- Quita un horario ------------------------------------------------------------
	public void quitaHorario(String horario)
		{
		for(int a=0;a<horarios.size();a++)
			{
			if(horario.equals((String)horarios.get(a))) {horarios.remove(a); break;}
			}
		}

	// --- Indica el tipo para la cuenta -----------------------------------------------
	public void setTipo(int tipo)
		{this.tipo = tipo;}

	// --- Devuelve el tipo de la cuenta -----------------------------------------------
	public int getTipo()
		{return tipo;}

	// --- Indica si la cuenta pertenece a la estructura de concentracion --------------
	public void setConcentracion(boolean val)
		{concentracion = val;}

	// --- Indica si la cuenta pertenece a la estructura de concentracion --------------
	public boolean estaEnConcentracion()
		{return concentracion;}

	// --- Regresa el nivel de la cuenta -----------------------------------------------
	public int nivel()
		{
		TI_Cuenta cuenta = padre;
		int resultado = 1;
		while(true)
			{
			if(cuenta == null) break;
			if(cuenta instanceof TI_CuentaDispersion)
				cuenta = ((TI_CuentaDispersion)cuenta).getPadre();
			else
				cuenta = null;
			resultado++;
			}
		return resultado;
		}

	// --- Regresa una trama conteniendo datos de la cuenta ----------------------------
	public String trama()
	 {
		StringBuffer trama = new StringBuffer("");
		trama.append("@");
		trama.append(getNumCta());
		trama.append("|");
		trama.append(((padre == null)?" ":padre.getNumCta()) );
		trama.append("|");
		trama.append( ((getDescripcion()=="")?" ":getDescripcion()) );
		trama.append("|");
		trama.append(periodicidad);
		trama.append("|");
		trama.append(importe);
		trama.append("|");
		trama.append( ((loConcentrado)?"S":"N") );
		trama.append("|");
		trama.append( ((concentracion)?"S":"N") + "|" );
		trama.append(tipo);
		for (int a=0;a<horarios.size();a++)
	     {
		   trama.append( "|");
		   trama.append((String)horarios.get(a) );
		 }
		return trama.toString();
	 }

	// --- Igual que el anterior pero tomando en cuenta a posiblePadre -----------------
	public String tramaPosible()
	 {
		StringBuffer trama = new StringBuffer("");
		trama.append("@");
		trama.append(getNumCta());
		trama.append("|");
		trama.append( ((posiblePadre.equals(""))?" ":posiblePadre) );
		trama.append("|");
		trama.append( ((getDescripcion()=="")?" ":getDescripcion()) );
		trama.append("|");
		trama.append(periodicidad);
		trama.append("|");
		trama.append(importe);
		trama.append("|");
		trama.append(((loConcentrado)?"S":"N"));
		trama.append("|");
		trama.append( ((concentracion)?"S":"N") );
		trama.append("|");
		trama.append(tipo);
		for (int a=0;a<horarios.size();a++)
		 {
		   trama.append("|");
		   trama.append( (String)horarios.get(a) );
		 }
		return trama.toString();
	 }

	// --- ESTATICO: Crea una nueva cuenta a partir de una trama -----------------------
	public static TI_CuentaDispersion creaCuentaDispersion(String trama)
		{
		int car;
		String strAux;
		TI_CuentaDispersion cuenta = null;

		if(trama == null) return null;
		if(!trama.substring(0,1).equals("@")) return null;
		trama = trama.substring(1);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta = new TI_CuentaDispersion(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.posiblePadre = (strAux.equals(" "))?"":strAux;
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setDescripcion(strAux);
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setPeriodicidad(Integer.parseInt(strAux));
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setImporte(Double.parseDouble(strAux));
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setLoConcentrado(strAux.equals("S"));
		car = trama.indexOf("|"); if(car == -1) return null;
		strAux = trama.substring(0,car); trama = trama.substring(car+1);
		cuenta.setConcentracion(strAux.equals("S"));
		car = trama.indexOf("|"); if(car == -1) car = trama.length();
		strAux = trama.substring(0,car); trama = trama.substring((car == trama.length())?car:car+1);
		cuenta.setTipo(Integer.parseInt(strAux));
		while(!trama.equals(""))
			{
			car = trama.indexOf("|"); if(car == -1) car = trama.length();
			strAux = trama.substring(0,car);
			trama = trama.substring((car == trama.length())?car:car+1);
			cuenta.agregaHorario(strAux);
			}
		return cuenta;
		}

	// --- ESTATICO: Copia una cuenta pero con otro num de cuenta ----------------------
	public static TI_CuentaDispersion copiaCuenta(String neoNumCta, TI_CuentaDispersion cuenta)
		{
		TI_CuentaDispersion neoCuenta = new TI_CuentaDispersion(neoNumCta);
		neoCuenta.setDescripcion(cuenta.getDescripcion());
		neoCuenta.setPeriodicidad(cuenta.getPeriodicidad());
		neoCuenta.setHorarios(cuenta.getHorarios());
		neoCuenta.setImporte(cuenta.getImporte());
		neoCuenta.setLoConcentrado(cuenta.getLoConcentrado());
		if(cuenta.getPadre() != null) neoCuenta.setPadre(cuenta.getPadre());
		neoCuenta.setConcentracion(cuenta.estaEnConcentracion());
		neoCuenta.setTipo(cuenta.getTipo());
		neoCuenta.posiblePadre = cuenta.posiblePadre;
		return neoCuenta;
		}

	}