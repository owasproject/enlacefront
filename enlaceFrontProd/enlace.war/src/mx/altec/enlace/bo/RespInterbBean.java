package mx.altec.enlace.bo;

import java.util.Vector;

public class RespInterbBean {
	private int enviados;
	private int autorizados;
	private int pendientes;
	private int rechazados;
	private long lote;
	private String nombreArchivo;
	private Vector detalleCuentasAlta;

	public RespInterbBean(int enviados, int autorizados, int pendientes,
			int rechazados, long lote, String nombreArchivo, Vector detalleCuentasAlta) {
		super();
		this.enviados = enviados;
		this.autorizados = autorizados;
		this.pendientes = pendientes;
		this.rechazados = rechazados;
		this.lote = lote;
		this.nombreArchivo = nombreArchivo;
		this.detalleCuentasAlta = detalleCuentasAlta;
	}

	public RespInterbBean(int enviados, int autorizados, int pendientes,
			int rechazados, long lote, String nombreArchivo) {
		super();
		this.enviados = enviados;
		this.autorizados = autorizados;
		this.pendientes = pendientes;
		this.rechazados = rechazados;
		this.lote = lote;
		this.nombreArchivo = nombreArchivo;
		this.detalleCuentasAlta = new Vector();
	}

	public RespInterbBean() {
		enviados = 0;
		autorizados = 0;
		pendientes = 0;
		rechazados = 0;
		nombreArchivo = "";
		detalleCuentasAlta = new Vector();
	}

	public int getEnviados() {
		return enviados;
	}

	public void setEnviados(int enviados) {
		this.enviados = enviados;
	}

	public int getAutorizados() {
		return autorizados;
	}

	public void setAutorizados(int autorizados) {
		this.autorizados = autorizados;
	}

	public int getPendientes() {
		return pendientes;
	}

	public void setPendientes(int pendientes) {
		this.pendientes = pendientes;
	}

	public int getRechazados() {
		return rechazados;
	}

	public void setRechazados(int rechazados) {
		this.rechazados = rechazados;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public long getLote() {
		return lote;
	}

	public void setLote(long lote) {
		this.lote = lote;
	}


	public Vector getDetalleCuentasAlta() {
		return detalleCuentasAlta;
	}


	public void setDetalleCuentasAlta(Vector detalleCuentasAlta) {
		this.detalleCuentasAlta = detalleCuentasAlta;
	}

	protected void finalize() throws Throwable {
		nombreArchivo = null;
		if(detalleCuentasAlta != null) {
			detalleCuentasAlta.clear();
			detalleCuentasAlta = null;
		}
	}

}