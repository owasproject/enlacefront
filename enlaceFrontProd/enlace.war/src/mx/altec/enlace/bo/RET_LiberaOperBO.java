package mx.altec.enlace.bo;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.RET_LiberaOperDAO;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.CatDivisaPais;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 *  Clase Business para realizar la liberaci�n de operaciones con la herramienta Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 04, 2012
 */
public class RET_LiberaOperBO extends BaseServlet{

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Objeto Sender para el envio de notificaciones
	 */
	EmailSender sender = new EmailSender();

	/**
	 * Objeto Details para el envio de notificaciones
	 */
	EmailDetails details = new EmailDetails();

	/**
	 * Metodo para manejar el negocio de la acci�n iniciar para realizar el pactado
	 * de operaciones con la herramienta Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void iniciar( HttpServletRequest req, HttpServletResponse res)
			throws Exception{
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: Entrando a iniciar.", EIGlobal.NivelLog.INFO);

			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			RET_ControlConsOper masterDao = new RET_ControlConsOper();
			RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
			String fchHrPactado = null;
			String estatus = null;
			String contrato = null;
			String Where = null;
			int indice = 0;

			try{
				sess.removeAttribute("controladorLista");
				GregorianCalendar cal = new GregorianCalendar();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", new java.util.Locale("es","mx"));
				fchHrPactado = sdf.format(cal.getTime());
				contrato = session.getContractNumber();

				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: fchHrPactado>" + fchHrPactado, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: estatus>" + estatus, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: contrato>" + contrato, EIGlobal.NivelLog.DEBUG);

				Where = "Where a.CONTRATO = '" + contrato.trim() + "' "
				      + "And   a.ESTATUS_OPERACION in ('P','C') "
				      + "And   trunc(a.FCH_HORA_PACTADO) = to_date('" + fchHrPactado + "','YYYYMMDD')";

				indice = 1;
				masterDao.setLista(dao.consulta(Where, indice));
				masterDao.setNumPages(dao.getNumPages(Where));
				masterDao.setNumRecords(dao.getNumRecords(Where));
				masterDao.setIdPage(indice);
				masterDao.setNumRecordPageDesde(dao.getRecordDesde(indice)+1);
				masterDao.setNumRecordPageHasta(dao.getRecordHasta(indice
														, masterDao.getNumPages()
														, masterDao.getNumRecords()));
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: RET_ControlConsOper.MAX_REGISTROS_PAG->"+RET_ControlConsOper.MAX_REGISTROS_PAG, EIGlobal.NivelLog.DEBUG);
				masterDao.setMaxNumRecordsPage(RET_ControlConsOper.MAX_REGISTROS_PAG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: masterDao.getMaxNumRecordsPage()->"+masterDao.getMaxNumRecordsPage(), EIGlobal.NivelLog.DEBUG);
				masterDao.setWhereCondicion(Where);

				if(masterDao.getLista().size()>0){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: Si hay registros de consulta.", EIGlobal.NivelLog.DEBUG);
				}else{
					paginaError("<b>No hay operaciones pactadas para liquidar.</b>", req, res);
				}

			}catch(Exception e){
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::iniciar:: Error Controlado>" + e.getMessage(), EIGlobal.NivelLog.INFO);
			}

			sess.removeAttribute("beanOperacionRET");
			sess.setAttribute("controladorLista", masterDao);
			//req.setAttribute("mensaje","\"La consulta fue exitosa.\",1");
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp01", req));
		}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Siguiente
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void siguiente(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::siguiente:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		String condicionBeforeComun = null;
		int indiceSiguiente = 0;

		indiceSiguiente = masterDao.getIdPage() + 1;
		condicionBeforeComun = masterDao.getWhereCondicion();
		masterDao.setLista(dao.consulta(condicionBeforeComun, indiceSiguiente));
		masterDao.setIdPage(indiceSiguiente);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceSiguiente)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceSiguiente
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::siguiente:: Bussines Siguiente exitoso.", EIGlobal.NivelLog.DEBUG);

		sess.setAttribute("controladorLista", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp01", req));
	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Anterior
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void anterior(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::anterior:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		String condicionAfterComun = null;
		int indiceAnterior = 0;

		indiceAnterior = masterDao.getIdPage() - 1;
		condicionAfterComun = masterDao.getWhereCondicion();
		masterDao.setLista(dao.consulta(condicionAfterComun, indiceAnterior));
		masterDao.setIdPage(indiceAnterior);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceAnterior)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceAnterior
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::anterior:: Bussines Anterior exitoso.", EIGlobal.NivelLog.DEBUG);

		sess.setAttribute("controladorLista", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp01", req));

	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Ultimo
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void ultimo(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::ultimo:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		String condicionBeforeComun = null;
		int indiceUltimo = 0;

		indiceUltimo = masterDao.getNumPages();
		condicionBeforeComun = masterDao.getWhereCondicion();
		masterDao.setLista(dao.consulta(condicionBeforeComun, indiceUltimo));
		masterDao.setIdPage(indiceUltimo);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indiceUltimo)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indiceUltimo
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::ultimo:: Bussines Ultimo exitoso.", EIGlobal.NivelLog.DEBUG);

		sess.setAttribute("controladorLista", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp01", req));

	}

	/**
	 * Metodo que se encarga del negocio del Reporte de beneficiarios para la accion
	 * Primero
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void primero(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::primero:: Entrando...", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_ControlConsOper masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		String condicionAfterComun = null;
		int indicePrimero = 0;

		indicePrimero = 1;
		condicionAfterComun = masterDao.getWhereCondicion();
		masterDao.setLista(dao.consulta(condicionAfterComun, indicePrimero));
		masterDao.setIdPage(indicePrimero);
		masterDao.setNumRecordPageDesde(dao.getRecordDesde(indicePrimero)+1);
		masterDao.setNumRecordPageHasta(
				dao.getRecordHasta(indicePrimero
						,masterDao.getNumPages()
						,masterDao.getNumRecords()));
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::primero:: Bussines Primero exitoso.", EIGlobal.NivelLog.DEBUG);

		sess.setAttribute("controladorLista", masterDao);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp01", req));
	}

	/**
	 * Metodo para manejar el negocio de la acci�n complementar para realizar el pactado
	 * de operaciones con la herramienta Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void complementar( HttpServletRequest req, HttpServletResponse res)
			throws Exception{
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Entrando a complementar.", EIGlobal.NivelLog.INFO);

			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			RET_ControlConsOper masterDao = (RET_ControlConsOper) sess.getAttribute("controladorLista");
			RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
			RET_OperacionVO bean = null;
			String operSel = null;
			String idRecord = null;
			String tablaValDatos = null;

			String divisaCargo = null;
			String divisaAbono = null;
			Vector operacionesRET = null;

			try{

				operSel = (String)getFormParameter(req,"operSel");
				operacionesRET = (Vector) masterDao.getLista();
				idRecord = operSel.trim();
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: idRecord a complementar->"+idRecord, EIGlobal.NivelLog.DEBUG);

				bean = (RET_OperacionVO)operacionesRET.get(Integer.parseInt(idRecord));
				dao.obtenDetalleAbono(bean);
				if(bean.getDetalleAbono()!=null && bean.getDetalleAbono().size()>0){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Numero de abonos->"+bean.getDetalleAbono().size(), EIGlobal.NivelLog.DEBUG);
					tablaValDatos = dibujaTablaAbonos(bean);

				}else
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Numero de abonos->0", EIGlobal.NivelLog.DEBUG);

				divisaCargo = obtenNaturalezaDivisa(bean.getDivisaOperante(),
													bean.getContraDivisa(),
													bean.getTipoOperacion(), "cargo");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Divisa de Cargo->"+divisaCargo, EIGlobal.NivelLog.DEBUG);

				divisaAbono = obtenNaturalezaDivisa(bean.getDivisaOperante(),
													bean.getContraDivisa(),
													bean.getTipoOperacion(), "abono");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Divisa de Abono->"+divisaAbono, EIGlobal.NivelLog.DEBUG);
			}catch(Exception e){
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::complementar:: Hubo un error al complementar la operaci�n->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
				paginaError("<b>Problemas al tratar de complementar la operaci&oacute;n.</b>", req, res);
				return;
			}

			session.setModuloConsultar(IEnlace.MCargo_TI_pes_dolar);
			session.setDivisaCuentaInter(obtenNaturalezaDivisa(bean.getDivisaOperante(), bean.getContraDivisa(),
										 bean.getTipoOperacion(), "abono"));
			sess.setAttribute("beanOperacionRET", bean);
			req.setAttribute("divisaCargo", divisaCargo);
			req.setAttribute("divisaAbono", divisaAbono);

			req.setAttribute("tablaValDatos", tablaValDatos);
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp02", req));
		}

	/**
	 * Muestra una pagina de error con la descripcion indicada
	 * @param mensaje			Mensaje de error presentado en pantalla
	 * @param request			request de la operaci�n
	 * @param response			response de la operaci�n
	 *
	 */
	public void paginaError(String mensaje,HttpServletRequest request, HttpServletResponse response){
		try{
			despliegaPaginaErrorURL(mensaje,
				"Pactado de Operaciones",
				"Transferencias FX Online &gt; ",
				"Pactado de Operaciones",
				request,
				response);
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::paginaError:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de desplegar la pagina de error
	 * por medio de una URL
	 *
	 * @param Error			Error a desplegar
	 * @param param1		Parametro del encabezado
	 * @param param2		Parametro del encabezado
	 * @param param3 		Parametro del encabezado
	 * @param request		Petici�n de la aplicaci�n
	 *
	 */
	private void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RequestDispatcher view = null;

		EIGlobal.mensajePorTrace("RET_LiberaOperBO::despliegaPaginaErrorURL:: Entrando.", EIGlobal.NivelLog.DEBUG);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		String param4 = "s55205h";

		request.setAttribute( "FechaHoy", fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );
		request.setAttribute( "URL", "csaldo1?prog=0");

		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2 + param3, param4, request ) );

		//evalTemplate("/jsp/EI_Mensaje.jsp", request, response);
		view = request.getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		view.forward(request,response);
	}

	/**
	 * Metodo para definir la fecha para el mensaje de error.
	 *
	 * @param strFecha			formato de la fecha
	 * @return Fecha Formato
	 */
	private String fechaHoy(String strFecha) {
		Date Hoy = new Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);

		String dd = "";
		String mm = "";
		String aa = "";
		String aaaa = "";
		String dt = "";
		String mt = "";
		String th = "";
		String tm = "";
		String ts = "";

		int indth = 0;
		int indtm = 0;
		int indts = 0;
		int inddd = 0;
		int indmm = 0;
		int indaaaa = 0;
		int indaa = 0;
		int indmt = 0;
		int inddt = 0;

		String[] dias = {
			"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes",
			"Sabado"
		};
		String[] meses = {
			"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
			"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
		};

		if (Cal.get(Calendar.DATE) <= 9)
			dd += "0" + Cal.get(Calendar.DATE);
		else
			dd += Cal.get(Calendar.DATE);

		if (Cal.get(Calendar.MONTH) + 1 <= 9)
			mm += "0" + (Cal.get(Calendar.MONTH) + 1);
		else
			mm += (Cal.get(Calendar.MONTH) + 1);

		if (Cal.get(Calendar.HOUR_OF_DAY) < 10)
			th += "0" + Cal.get(Calendar.HOUR_OF_DAY);
		else
			th += Cal.get(Calendar.HOUR_OF_DAY);

		if (Cal.get(Calendar.MINUTE) < 10)
			tm += "0" + Cal.get(Calendar.MINUTE);
		else
			tm += Cal.get(Calendar.MINUTE);

		if (Cal.get(Calendar.SECOND) < 10)
			ts += "0" + Cal.get(Calendar.SECOND);
		else
			ts += Cal.get(Calendar.SECOND);

		aaaa += Cal.get(Calendar.YEAR);
		aa += aaaa.substring(aaaa.length() - 2, aaaa.length());
		dt += dias[Cal.get(Calendar.DAY_OF_WEEK) - 1];
		mt += meses[Cal.get(Calendar.MONTH)];

		while (
			indth >= 0 || indtm >= 0 || indts >= 0 || inddd >= 0
			|| indmm >= 0 || indaaaa >= 0 || indaa >= 0 || indmt >= 0
			|| inddt >= 0
			) {
			indth = strFecha.indexOf("th");
			if (indth >= 0)
				strFecha = strFecha.substring(0, indth) + th + strFecha.substring(indth + 2, strFecha.length());
			indtm = strFecha.indexOf("tm");
			if (indtm >= 0)
				strFecha = strFecha.substring(0, indtm) + tm + strFecha.substring(indtm + 2, strFecha.length());
			indts = strFecha.indexOf("ts");
			if (indts >= 0)
				strFecha = strFecha.substring(0, indts) + ts + strFecha.substring(indts + 2, strFecha.length());
			inddd = strFecha.indexOf("dd");
			if (inddd >= 0)
				strFecha = strFecha.substring(0, inddd) + dd + strFecha.substring(inddd + 2, strFecha.length());
			indmm = strFecha.indexOf("mm");
			if (indmm >= 0)
				strFecha = strFecha.substring(0, indmm) + mm + strFecha.substring(indmm + 2, strFecha.length());
			indaaaa = strFecha.indexOf("aaaa");
			if (indaaaa >= 0)
				strFecha = strFecha.substring(0, indaaaa) + aaaa + strFecha.substring(indaaaa + 4, strFecha.length());
			indaa = strFecha.indexOf("aa");
			if (indaa >= 0)
				strFecha = strFecha.substring(0, indaa) + aa + strFecha.substring(indaa + 2, strFecha.length());
			indmt = strFecha.indexOf("mt");
			if (indmt >= 0)
				strFecha = strFecha.substring(0, indmt) + mt + strFecha.substring(indmt + 2, strFecha.length());
			inddt = strFecha.indexOf("dt");
			if (inddt >= 0)
				strFecha = strFecha.substring(0, inddt) + dt + strFecha.substring(inddt + 2, strFecha.length());
		}
		return strFecha;
	}

	/**
	 * Metodo especial para bitacorizar el Complemento y Liberaci�n de Operaciones Reuters dentro de
	 * la bitacora de Operaciones
	 *
	 * @param req				Request de la acci�n
	 * @param res				Response de la acci�n
	 * @param RET_OperacionVO	Objeto contenedor de tipo RET_OperacionVO
	 * @param cveBitacora		Clave de Bitacora
	 *
	 */
	private void escribeBitacoraOper(HttpServletRequest req, HttpServletResponse res, RET_OperacionVO bean,
									 String codError, String cveBitacora){
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("RET_LiberaOperBO::escribeBitacoraOper:: Entrando ...", EIGlobal.NivelLog.INFO);
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				if(getFormParameter(req,BitaConstants.FLUJO) != null){
					bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
				}else{
					bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				}
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				BitaTCTBean beanTCT = new BitaTCTBean ();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setTipoOperacion(cveBitacora);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
                        && ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).equals(BitaConstants.VALIDA)
                        && !cveBitacora.trim().equals(BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA)){
					bt.setIdToken(session.getToken().getSerialNumber());
                }

				bt.setIdErr(codError);
				beanTCT.setCodError(codError);

				bt.setReferencia(Integer.parseInt(bean.getFolioEnla()));
				beanTCT.setReferencia(Integer.parseInt(bean.getFolioEnla()));

				bt.setCctaOrig(bean.getCtaCargo());
				beanTCT.setCuentaOrigen(bean.getCtaCargo());

				bt.setImporte(Double.parseDouble(bean.getImporteTotOper()));
				beanTCT.setImporte(Double.parseDouble(bean.getImporteTotOper()));

				bt.setTipoCambio(Double.parseDouble(bean.getTcPactado()));
				beanTCT.setTipoOperacion(cveBitacora);

				bt.setIdFlujo(cveBitacora);
				bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "02");

				BitaHandler.getInstance().insertBitaTransac(bt);
				BitaHandler.getInstance().insertBitaTCT(beanTCT);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::escribeBitacoraOper:: bitacorizando " + cveBitacora, EIGlobal.NivelLog.INFO);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::escribeBitacoraOper:: Exception " + e.toString(), EIGlobal.NivelLog.INFO);
			}
		}
	}

	/**
	 * Metodo encargado de administrar las cuentas insertadas por parte del usuario de
	 * Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void insertarCuenta(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Entrando.....", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		TrxB485BO b485 = new TrxB485BO();
		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		RET_CuentaAbonoVO beanAbono = null;
		String cuentaCargo = null;
		String desCuentaCargo = null;
		String concepto = null;
		String cuentaAbonoMB = null;
		String desCuentaAbonoMB = null;
		String cuentaAbonoInter = null;
		String desCuentaAbonoInter = null;
		String divCuentaAbonoInter = null;
		String abaCuentaAbonoInter = null;
		String swiftCuentaAbonoInter = null;
		String tipoCuenta = null;
		String tablaValDatos = null;
		String impTotalAbono = null;
		String divisaAbono = null;
		String divisaAbonoOper = null;
		String divisaCargoOper = null;
		String mensajeError = null;

		Vector detCuentasAbono = null;
		int idRegistroAbono = 0;
		boolean valExitosa = true;

		try{
			beanAbono = new RET_CuentaAbonoVO();
			cuentaCargo = (String)getFormParameter(req,"selCuentaCargo");
			desCuentaCargo = (String)getFormParameter(req,"selDesCuentaCargo");
			concepto = (String)getFormParameter(req,"concepto");
			cuentaAbonoMB = (String)getFormParameter(req,"selCtaMB");
			desCuentaAbonoMB = (String)getFormParameter(req,"selDesCtaMB");
			cuentaAbonoInter = (String)getFormParameter(req,"selCtaInter");
			desCuentaAbonoInter = (String)getFormParameter(req,"selDesCtaInter");
			divCuentaAbonoInter = (String)getFormParameter(req,"selDivCtaInter");
			abaCuentaAbonoInter = (String)getFormParameter(req,"selABACtaInter");
			swiftCuentaAbonoInter = (String)getFormParameter(req,"selSWIFTCtaInter");
			tipoCuenta = (String)getFormParameter(req,"tipoCuenta");
			impTotalAbono = (String)getFormParameter(req,"impTotalAbono");



			try{
				//idRegistroAbono = Integer.parseInt((String)getFormParameter(req,"idRegistroAbono"));
				idRegistroAbono = bean.getDetalleAbono() != null ? bean.getDetalleAbono().size() - 1 : 0;

			}catch(NumberFormatException e){
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Primer Abono a insertar", EIGlobal.NivelLog.DEBUG);
			}
			impTotalAbono = (impTotalAbono!=null)?impTotalAbono:"0";

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Existen ->"+(idRegistroAbono+1)+" Abonos en esta operacion.", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: cuentaCargo->"+cuentaCargo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: desCuentaCargo->"+desCuentaCargo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: concepto->"+concepto, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: tipoCuenta->"+tipoCuenta, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: impTotalAbono->"+impTotalAbono, EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Inician validaciones....", EIGlobal.NivelLog.DEBUG);
			divisaCargoOper = obtenNaturalezaDivisa(bean.getDivisaOperante(), bean.getContraDivisa(), bean.getTipoOperacion(), "cargo");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Divisa de Cargo de la operacion->"+divisaCargoOper, EIGlobal.NivelLog.DEBUG);

			divisaAbonoOper = obtenNaturalezaDivisa(bean.getDivisaOperante(), bean.getContraDivisa(), bean.getTipoOperacion(), "abono");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Divisa de Abono de la operacion->"+divisaAbonoOper, EIGlobal.NivelLog.DEBUG);

			if(tipoCuenta.trim().equals("1")){
				divisaAbono = b485.obtenDivisaCta(cuentaAbonoMB);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Cuenta a insertar Mismo Banco con Divisa->"+divisaAbono, EIGlobal.NivelLog.DEBUG);

				if(divisaAbono != null && !divisaAbono.trim().equals(divisaAbonoOper.trim())){
						valExitosa = false;
						mensajeError = "La Divisa de la Cuenta de Abono no es correcta";
				}
			}

			if(bean.getDetalleAbono()!=null){
				for(int i=0; i<bean.getDetalleAbono().size() && valExitosa; i++){
					if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta().equals(cuentaAbonoMB.trim())){
						req.setAttribute("mensaje", "\"La cuenta de abono Mismo Banco " + cuentaAbonoMB + " ya ha sido registrada.\",11");
						valExitosa = false;
					}

					if(valExitosa && ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta().equals(cuentaAbonoInter.trim())){
						req.setAttribute("mensaje", "\"La cuenta de abono Internacional " + cuentaAbonoInter + " ya ha sido registrada.\",11");
						valExitosa = false;
					}
				}
			}else{
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: No hay cuentas de abono registradas.", EIGlobal.NivelLog.DEBUG);
			}

			if(valExitosa){
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Las validaciones sobre las cuentas fueron exitosas.", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Se inicia el guardado de datos sobre el Bean de la operaci�n.", EIGlobal.NivelLog.DEBUG);
				if(!cuentaCargo.trim().equals("") && !cuentaCargo.trim().equals(bean.getCtaCargo())){
					bean.setCtaCargo(cuentaCargo.trim());
					bean.setDesCtaCargo(desCuentaCargo.trim());
				}
				bean.setConcepto(concepto.trim());
				bean.setImporteTotAbono(impTotalAbono);
				/*for(int i=0; i<idRegistroAbono; i++){
					for(int j=0; j<idRegistroAbono; j++){
						EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: ((RET_CuentaAbonoVO)bean.getDetalleAbono().get("+i+")).getIdRegistro()->"
								+ ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: impIdRegistroAbono"+j+"->"
								+ ((String)getFormParameter(req,"impIdRegistroAbono"+j)), EIGlobal.NivelLog.DEBUG);
						if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro()==j)
							((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setImporteAbono((String)getFormParameter(req,"impIdRegistroAbono"+j));
					}
				}*/

				if(bean.getDetalleAbono() != null && bean.getDetalleAbono().size() > 0)
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(idRegistroAbono)).setImporteAbono((String)getFormParameter(req,"impIdRegistroAbono" + idRegistroAbono));

				if(tipoCuenta.equals("1")){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: El tipo de cuenta de abono que se desea insertar es Mismo Banco", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: cuentaAbonoMB->"+cuentaAbonoMB, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: desCuentaAbonoMB->"+desCuentaAbonoMB, EIGlobal.NivelLog.DEBUG);
					if(!bean.getExistenCtaMB())
						bean.setExistenCtaMB(true);
					beanAbono.setNumCuenta(cuentaAbonoMB.trim());
					beanAbono.setBenefCuenta(desCuentaAbonoMB.trim());
					beanAbono.setTipoCuenta(RET_CuentaAbonoVO.CUENTA_MISMO_BANCO);
				}else if(tipoCuenta.equals("2")){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: El tipo de cuenta de abono que se desea insertar es Internacional", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: cuentaAbonoInter->"+cuentaAbonoInter, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: desCuentaAbonoInter->"+desCuentaAbonoInter, EIGlobal.NivelLog.DEBUG);
					if(!bean.getExistenCtaIB())
						bean.setExistenCtaIB(true);
					beanAbono.setNumCuenta(cuentaAbonoInter.trim());
					beanAbono.setBenefCuenta(desCuentaAbonoInter.trim());
					beanAbono.setTipoCuenta(RET_CuentaAbonoVO.CUENTA_INTERNACIONAL);
					if(divCuentaAbonoInter.equals("USA")){
						EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: se registra un ABA->"+abaCuentaAbonoInter, EIGlobal.NivelLog.DEBUG);
						beanAbono.setCveAbaSwift(abaCuentaAbonoInter.trim());
					}else{
						EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: se registra un SWIFT->"+swiftCuentaAbonoInter, EIGlobal.NivelLog.DEBUG);
						beanAbono.setCveAbaSwift(swiftCuentaAbonoInter.trim());
					}
				}
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: El id del Abono sera->"+idRegistroAbono, EIGlobal.NivelLog.DEBUG);
				beanAbono.setIdRegistro(bean.getDetalleAbono().size());
				beanAbono.setImporteAbono("0.00");

				if(bean.getDetalleAbono()!=null){
					bean.getDetalleAbono().add(beanAbono);
				}else{
					detCuentasAbono = new Vector<RET_CuentaAbonoVO>();
					detCuentasAbono.add(beanAbono);
					bean.setDetalleAbono(detCuentasAbono);
				}
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Se termina el guardado de datos sobre el Bean de la operaci�n.", EIGlobal.NivelLog.DEBUG);
				tablaValDatos = dibujaTablaAbonos(bean);
				idRegistroAbono += 1;
			}else{
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Hubo errores en las validaciones.", EIGlobal.NivelLog.DEBUG);
				if(!cuentaCargo.trim().equals("") && !cuentaCargo.trim().equals(bean.getCtaCargo())){
					bean.setCtaCargo(cuentaCargo.trim());
					bean.setDesCtaCargo(desCuentaCargo.trim());
				}
				bean.setImporteTotAbono(impTotalAbono);
				bean.setConcepto(concepto.trim());
				tablaValDatos = dibujaTablaAbonos(bean);
				req.setAttribute("mensaje", "\"" + mensajeError + ".\",11");
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Problemas al insertar el abono->"+e.getMessage(), EIGlobal.NivelLog.INFO);

			for (StackTraceElement element: e.getStackTrace())
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::insertarCuenta:: Problemas al insertar el abono->"+ element.getClassName() + element.getLineNumber(), EIGlobal.NivelLog.INFO);

			req.setAttribute("mensaje", "\"Cuenta Invalida, intente nuevamente por favor.\",11");
			if(!cuentaCargo.trim().equals("") && !cuentaCargo.trim().equals(bean.getCtaCargo())){
				bean.setCtaCargo(cuentaCargo.trim());
				bean.setDesCtaCargo(desCuentaCargo.trim());
			}
			bean.setConcepto(concepto.trim());

		}

		actualizaMontos(req, res);
		tablaValDatos = dibujaTablaAbonos(bean);

		sess.setAttribute("beanOperacionRET", bean);
		req.setAttribute("divisaCargo", divisaCargoOper);
		req.setAttribute("divisaAbono", divisaAbonoOper);

		req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute("idRegistroAbono", idRegistroAbono);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp02", req));

	}

	/**
	 * Metodo para dibujar la tabla de datos de la cuenta de abono
	 * @param bean 		 - bean de datos con lo correspondiente al detalle de la operaci�n
	 * @return	String con la tabla
	 */
	private String dibujaTablaAbonos(RET_OperacionVO bean){
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::dibujaTablaAbonos:: Entrando a dibujaTabla.", EIGlobal.NivelLog.DEBUG);
		StringBuffer sb = new StringBuffer("");
		boolean tituloTablaMB = false;
		boolean tituloTablaIB = false;
		boolean colorFilaMB = false;
		boolean colorFilaIB = false;
		String txtColorFilaMB = "#CCCCCC";
		String txtColorFilaIB = "#CCCCCC";

		DecimalFormat format = new DecimalFormat("0.00");

		sb.append("<table align=\"center\" width=\"660\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\">");
		sb.append("<tr>");

		sb.append("<td align=\"center\" valign=\"TOP\" width=\"350\">");
		if(bean.getExistenCtaMB())
			sb.append("<table align=\"center\">");
		for(int i=0; i<bean.getDetalleAbono().size(); i++){
			if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getTipoCuenta().equals(RET_CuentaAbonoVO.CUENTA_MISMO_BANCO)){
				if(!((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBajaLogica()){
					if(!tituloTablaMB){
						sb.append("<tr bgcolor=#A4BEE4>");
						sb.append("<td align=\"center\" class=tittabdat>");
						sb.append("Seleccione");
						sb.append("</td>");
						sb.append("<td align=\"center\" colspan=2 class=tittabdat>");
						sb.append("Cuenta Abono Mismo Banco");
						sb.append("</td>");
						sb.append("<td align=\"center\" class=tittabdat>");
						sb.append("Importe");
						sb.append("</td>");
						sb.append("</tr>");
						tituloTablaMB = true;
					}
					sb.append("<tr bgcolor="+txtColorFilaMB+">");
					sb.append("<td class=textabdatobs>");
					sb.append("<input type=checkbox name=chkCuentaMB value=\""+ ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta() + "\" >");
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta());
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta());
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append("<input type=text name=impIdRegistroAbono"+ i
							+ " id=impIdRegistroAbono"+ i
							+ " value=\"" + format.format(Double.parseDouble(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getImporteAbono()))
							+ "\" size=\"15\" maxlength=\"15\"  onChange=\"return valImporteAbono(this);\" >");
					sb.append("</td>");
					sb.append("</tr>");

					if(colorFilaMB)colorFilaMB = false;txtColorFilaMB = "#CCCCCC";
					if(!colorFilaMB)colorFilaMB = true;txtColorFilaMB = "#EBEBEB";
				}else{
					sb.append("<input type=\"hidden\" name=impIdRegistroAbono"+ i
							+ " id=impIdRegistroAbono"+ i
							+ " value=\"" + format.format(Double.parseDouble(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getImporteAbono())) + "\">");
				}
			}
		}
		if(bean.getExistenCtaMB())
			sb.append("</table>");
		sb.append("</td>");

		sb.append("<td align=\"center\" valign=\"TOP\" width=\"0\">");
		if(bean.getExistenCtaIB())
			sb.append("<table align=\"center\">");
		for(int i=0; i<bean.getDetalleAbono().size(); i++){
			if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getTipoCuenta().equals(RET_CuentaAbonoVO.CUENTA_INTERNACIONAL)){
				if(!((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBajaLogica()){
					if(!tituloTablaIB){
						sb.append("<tr bgcolor=#A4BEE4>");
						sb.append("<td align=\"center\" class=tittabdat>");
						sb.append("Seleccione");
						sb.append("</td>");
						sb.append("<td align=\"center\" colspan=2 class=tittabdat>");
						sb.append("Cuenta Abono Internacional");
						sb.append("</td>");
						sb.append("<td align=\"center\" class=tittabdat>");
						sb.append("Importe");
						sb.append("</td>");
						sb.append("</tr>");
						tituloTablaIB = true;
					}
					sb.append("<tr bgcolor="+txtColorFilaIB+">");
					sb.append("<td class=textabdatobs>");
					sb.append("<input type=checkbox name=chkCuentaIB value=\""+ ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta() + "\" >");
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta());
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta());
					sb.append("</td>");
					sb.append("<td class=textabdatobs>");
					sb.append("<input type=text name=impIdRegistroAbono"+ i
							+ " id=impIdRegistroAbono"+ i
							+ " value=\"" + format.format(Double.parseDouble(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getImporteAbono()))
							+ "\" size=\"15\" maxlength=\"15\"  onChange=\"return valImporteAbono(this);\" >");
					sb.append("</td>");
					sb.append("</tr>");

					if(colorFilaIB)colorFilaIB = false;txtColorFilaIB = "#CCCCCC";
					if(!colorFilaIB)colorFilaIB = true;txtColorFilaIB = "#EBEBEB";
				}else{
					sb.append("<input type=\"hidden\" name=impIdRegistroAbono"+ i
							+ " id=impIdRegistroAbono"+ ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro()
							+ " value=\"" + format.format(Double.parseDouble(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getImporteAbono())) + "\">");
				}
			}
		}
		if(bean.getExistenCtaIB())
			sb.append("</table>");
		sb.append("</td>");

		sb.append("</tr>");
		sb.append("</table>");

		sb.append("<table border=0 align=\"center\" bgcolor=#EBEBEB>");
		sb.append("<tr bgcolor=#EBEBEB>");
		sb.append("<td class=textabdatcla>");
		sb.append("Total Abono:");
		sb.append("</td>");
		sb.append("<td class=textabdatcla>");
		sb.append("<input type=text name=impTotalAbono VALUE=\"" + format.format(Double.parseDouble(bean.getImporteTotAbono())) + "\""
				+ " size=40 maxlength=40 readonly=\"readonly\">");
		sb.append("<input type=\"hidden\" name=\"numAbonos\" value=\" " + bean.getDetalleAbono().size()+ " \">");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");

		if(bean.getDetalleAbono().size() < 1)sb.delete(0, sb.length());

		EIGlobal.mensajePorTrace("RET_LiberaOperBO::dibujaTablaAbonos:: Saliendo de dibujaTabla."+ sb.toString(), EIGlobal.NivelLog.DEBUG);
		return sb.toString();
	}

	/**
	 * Metodo encargado de limpiar las instrucciones de liquidaci�n realizadas en
	 * Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void limpiar(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::limpiar:: Entrando a limpiar.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		String divisaCargo = null;
		String divisaAbono = null;
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();


		try{
			bean.setCtaCargo("");
			bean.setDesCtaCargo("");
			bean.setConcepto("");
			bean.setImporteTotAbono("");
			bean.setExistenCtaMB(false);
			bean.setExistenCtaIB(false);

			dao.initTransac();
			dao.limpiaAbonos(bean,session.getContractNumber());
			dao.execTransac();
			dao.closeTransac();

			bean.getDetalleAbono().removeAllElements();

			divisaCargo = obtenNaturalezaDivisa(bean.getDivisaOperante(),
												bean.getContraDivisa(),
												bean.getTipoOperacion(), "cargo");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::limpiar:: Divisa de Cargo->"+divisaCargo, EIGlobal.NivelLog.DEBUG);

			divisaAbono = obtenNaturalezaDivisa(bean.getDivisaOperante(),
												bean.getContraDivisa(),
												bean.getTipoOperacion(), "abono");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::limpiar:: Divisa de Abono->"+divisaAbono, EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			dao.closeTransac();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::limpiar:: Problemas al limpiar->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		sess.setAttribute("beanOperacionRET", bean);
		req.setAttribute("divisaCargo", divisaCargo);
		req.setAttribute("divisaAbono", divisaAbono);

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp02", req));
	}

	/**
	 * Metodo encargado de dar de baja un registro en especifico sobre las instrucciones
	 * de liquidaci�n realizadas en Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void bajaRegistro(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Entrando a bajaRegistro.", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		StringTokenizer operTokMB = null;
		StringTokenizer operTokIB = null;
		String operSelMB = null;
		String operSelIB = null;
		String idRegistroTokMB = null;
		String idRegistroTokIB = null;
		String delimRegistro = null;
		String tablaValDatos = null;
		String divisaCargo = null;
		String divisaAbono = null;
		String cuentaCargo = null;
		String desCuentaCargo = null;
		Double importeTotOper = 0.0;
		int countMB=0;
		int countIB=0;
		int idRegistroAbono = 0;



		try{
			idRegistroAbono = bean.getDetalleAbono().size();

			cuentaCargo = (String)getFormParameter(req,"selCuentaCargo");
			desCuentaCargo = (String)getFormParameter(req,"selDesCuentaCargo");
			operSelMB = (String)getFormParameter(req,"operSelMB");
			operSelIB = (String)getFormParameter(req,"operSelIB");
			delimRegistro = "|";

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Existen ->"+bean.getDetalleAbono().size()+" Abonos en esta operacion.", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Trama operSelMB->"+operSelMB, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Trama operSelIB->"+operSelIB, EIGlobal.NivelLog.DEBUG);

			/*for(int i=0; i<idRegistroAbono; i++){
				for(int j=0; j<idRegistroAbono; j++){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: ((RET_CuentaAbonoVO)bean.getDetalleAbono().get("+i+")).getIdRegistro()->"
							+ ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: impIdRegistroAbono"+j+"->"
							+ ((String)getFormParameter(req,"impIdRegistroAbono"+j)), EIGlobal.NivelLog.DEBUG);
					if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro()==j)
						((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setImporteAbono((String)getFormParameter(req,"impIdRegistroAbono"+j));
				}
			}*/

			dao.initTransac();
			operTokMB = new StringTokenizer(operSelMB, delimRegistro);
			while(operTokMB.hasMoreElements()){
				idRegistroTokMB = operTokMB.nextToken().trim();
				for(int i=0; i<bean.getDetalleAbono().size(); i++){
					if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta().equals(idRegistroTokMB)){
						//((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setBajaLogica(true);
						//((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setImporteAbono("0");
						dao.limpiaAbonos(bean,session.getContractNumber());
						bean.getDetalleAbono().remove(i);
						countMB++;
					}
				}
			}
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Se eliminaron " + countMB + " Cuentas Mismo Banco", EIGlobal.NivelLog.DEBUG);

			operTokIB = new StringTokenizer(operSelIB, delimRegistro);
			while(operTokIB.hasMoreElements()){
				idRegistroTokIB = operTokIB.nextToken().trim();
				for(int j=0; j<bean.getDetalleAbono().size(); j++){
					if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(j)).getNumCuenta().equals(idRegistroTokIB)){
						//((RET_CuentaAbonoVO)bean.getDetalleAbono().get(j)).setBajaLogica(true);
						//((RET_CuentaAbonoVO)bean.getDetalleAbono().get(j)).setImporteAbono("0");
						dao.limpiaAbonos(bean,session.getContractNumber());
						bean.getDetalleAbono().remove(j);
						countIB++;
					}
				}
			}
			dao.execTransac();
			dao.closeTransac();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Calculando el total del importe", EIGlobal.NivelLog.DEBUG);

			for(int k=0; k<bean.getDetalleAbono().size(); k++){
				importeTotOper += Double.parseDouble(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(k)).getImporteAbono());
			}

			bean.setImporteTotAbono("" + importeTotOper);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Nuevo Importe Total de Abono->" + bean.getImporteTotOper(), EIGlobal.NivelLog.DEBUG);

			if(cuentaCargo!=null && !cuentaCargo.trim().equals("")){
				bean.setCtaCargo(cuentaCargo);
				bean.setDesCtaCargo(desCuentaCargo);
			}
			divisaCargo = obtenNaturalezaDivisa(bean.getDivisaOperante(),
												bean.getContraDivisa(),
												bean.getTipoOperacion(), "cargo");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Divisa de Cargo->"+divisaCargo, EIGlobal.NivelLog.DEBUG);

			divisaAbono = obtenNaturalezaDivisa(bean.getDivisaOperante(),
												bean.getContraDivisa(),
												bean.getTipoOperacion(), "abono");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Divisa de Abono->"+divisaAbono, EIGlobal.NivelLog.DEBUG);

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Se eliminaron " + countIB + " Cuentas Internacionales", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Se vuelve a dibujar la tabla con los registros actualizados", EIGlobal.NivelLog.DEBUG);
			tablaValDatos = dibujaTablaAbonos(bean);

		}catch(Exception e){
			dao.closeTransac();
			for (StackTraceElement element: e.getStackTrace())
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::bajaRegistro:: Problemas al dar de baja el registro->"+ element.getClassName() + element.getLineNumber(), EIGlobal.NivelLog.INFO);
		}

		sess.setAttribute("beanOperacionRET", bean);
		req.setAttribute("divisaCargo", divisaCargo);
		req.setAttribute("divisaAbono", divisaAbono);

		req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp02", req));
	}

	/**
	 * Metodo encargado validar un registro en especifico sobre las instrucciones
	 * de liquidaci�n realizadas en Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public boolean validaDatos(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: Entrando a validaDatos.", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		TrxB485BO b485 = new TrxB485BO();
		String tablaValDatos = null;
		String divisaCargo = null;
		String divisaCargoOper = null;
		String divisaAbonoOper = null;
		String textCuentaCargo = null;
		String concepto = null;
		boolean valExitosa = true;

		try{
			textCuentaCargo = (String)getFormParameter(req,"textCuentaCargo");
			concepto = (String)getFormParameter(req,"concepto");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: textCuentaCargo->"+textCuentaCargo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: concepto->"+concepto, EIGlobal.NivelLog.DEBUG);

			bean.setCtaCargo(textCuentaCargo.substring(0, 11).trim());
			bean.setDesCtaCargo(textCuentaCargo.substring(12).trim());
			bean.setConcepto(concepto);

			divisaCargo = b485.obtenDivisaCta(bean.getCtaCargo());
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: Divisa de la cuenta de cargo a registrar->"+divisaCargo, EIGlobal.NivelLog.DEBUG);
			divisaCargoOper = obtenNaturalezaDivisa(bean.getDivisaOperante(),
													bean.getContraDivisa(),
													bean.getTipoOperacion(), "cargo");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: Divisa de cargo de la operacion->"+divisaCargoOper, EIGlobal.NivelLog.DEBUG);

			divisaAbonoOper = obtenNaturalezaDivisa(bean.getDivisaOperante(),
													bean.getContraDivisa(),
													bean.getTipoOperacion(), "abono");
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: Divisa de abono de la operacion->"+divisaAbonoOper, EIGlobal.NivelLog.DEBUG);

			if(!divisaCargo.trim().equals(divisaCargoOper.trim())){
				req.setAttribute("mensaje", "\"La Divisa de la Cuenta de Cargo no es correcta.\",11");
				valExitosa = false;
			}
			tablaValDatos = dibujaTablaAbonos(bean);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::validaDatos:: Problemas al validar datos->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}

		sess.setAttribute("beanOperacionRET", bean);

		req.setAttribute("divisaCargo", divisaCargoOper);
		req.setAttribute("divisaAbono", divisaAbonoOper);
		req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp02", req));
		return valExitosa;
	}

	/**
	 * Metodo encargado registrar un registro en especifico sobre las instrucciones
	 * de liquidaci�n realizadas en Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void registrar(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Entrando a registrar.", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable hs = null;
		Integer referencia = 0 ;
		String codError = null;
		String tablaValDatos = null;
		String numContrato = null;
		String mensaje = null;
		String etiquetaDivisa = null;
		int countOk = 0;
		int[] regsBatch = null;

		try{
			GregorianCalendar cal = new GregorianCalendar();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", new java.util.Locale("es","mx"));
			bean.setFchHoraComplemento(sdf.format(cal.getTime()));
			numContrato = session.getContractNumber();
			actualizaMontos(req, res);

			dao.initTransac();
			if(dao.limpiaAbonos(bean, numContrato))
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se limpian abonos correctamente", EIGlobal.NivelLog.DEBUG);
			else{
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ejecutando rollback problemas al limpiar abonos.", EIGlobal.NivelLog.DEBUG);
				dao.closeTransacError();
				paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
				return;
			}

			try {
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Llamado al servicio SREFERENCIA",	EIGlobal.NivelLog.DEBUG);
				hs = tuxGlobal.sreferencia("901");;
				codError = hs.get("COD_ERROR").toString();
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Codigo de Error llamado->"+ codError, EIGlobal.NivelLog.DEBUG);
				referencia = (Integer) hs.get("REFERENCIA");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: FOLIO ENLA->"+ referencia, EIGlobal.NivelLog.DEBUG);
				if(bean.getFolioEnla()!=null && !bean.getFolioEnla().trim().equals(""))
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ya se tiene folio Enla de la Operaci�n", EIGlobal.NivelLog.DEBUG);
				else{
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Primer complemento se guarda folio Enla de la operaci�n", EIGlobal.NivelLog.DEBUG);
					bean.setFolioEnla(referencia.toString());
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Problemas al obtener la FOLIO ENLA->"+e.getMessage(), EIGlobal.NivelLog.INFO);
				codError = null;
				referencia = 0;
			}

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se inicia la conexi�n exitosamente.", EIGlobal.NivelLog.DEBUG);
			if(dao.registraCargoOper(bean, numContrato))
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se actualiza el cargo", EIGlobal.NivelLog.DEBUG);
			else{
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ejecutando rollback problemas en el cargo", EIGlobal.NivelLog.DEBUG);
				dao.closeTransacError();
				paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
				return;
			}

			for(int i=0; i<bean.getDetalleAbono().size(); i++){
				if(dao.registraAbonoOper(bean, numContrato, i))
					countOk++;
				else{
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ejecutando rollback problemas en el abono", EIGlobal.NivelLog.DEBUG);
					dao.closeTransacError();
					paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
					return;
				}
			}
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se registran " + countOk + " abonos", EIGlobal.NivelLog.DEBUG);

			regsBatch = dao.execTransac();
			for(int j=0; j<regsBatch.length; j++){
				EIGlobal.mensajePorTrace("regsBatch["+j+"]->"+regsBatch[j], EIGlobal.NivelLog.DEBUG);
				if(j>0 && regsBatch[j]<=0){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ejecutando Rollback.", EIGlobal.NivelLog.DEBUG);
					dao.closeTransacError();
					paginaError("Problemas al realizar la operaci&oacute;n<BR>intente m&aacute;s tarde.", req, res);
					return;
				}
			}

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: La operacion fue exitosa", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Ejecutando Commit", EIGlobal.NivelLog.DEBUG);
			dao.closeTransac();
			tablaValDatos = dibujaTablaAbonos(bean);

			if(verificaFacultad("LIBPFXREUTER",req)){
				req.setAttribute("muestraTransferir", "true");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Con facultad para transferir", EIGlobal.NivelLog.DEBUG);
			}else{
				req.setAttribute("muestraTransferir", "false");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Sin facultad para transferir", EIGlobal.NivelLog.DEBUG);
			}

			if(bean.getTipoOperacion().equals("CPA"))
				etiquetaDivisa = "MXP";
			else
				etiquetaDivisa = obtenNaturalezaDivisa(bean.getDivisaOperante(), bean.getContraDivisa(), bean.getTipoOperacion(), "abono");

			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: etiquetaDivisa->"+etiquetaDivisa, EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Problemas al registrar->"+e.getMessage(), EIGlobal.NivelLog.INFO);
			dao.closeTransacError();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n erronea", EIGlobal.NivelLog.DEBUG);
			escribeBitacoraOper(req, res, bean,
					           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA + "9999",
					           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA);
			paginaError("Problemas al realizar la operaci&oacute;n<BR>intente m&aacute;s tarde.", req, res);
			return;
		}
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n exitosa", EIGlobal.NivelLog.DEBUG);
		escribeBitacoraOper(req, res, bean,
				           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA + "0000",
				           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA);
		mensaje = "\"Se guardo el complemento correctamente.\",1";

		sess.setAttribute("beanOperacionRET", bean);
		req.setAttribute("etiquetaDivisa", etiquetaDivisa);
		req.setAttribute("mensaje", mensaje);
		req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetComp03", req));
	}

	/**
	 * Metodo encargado transferir un registro en especifico sobre las instrucciones
	 * de liquidaci�n realizadas en Reuters
	 *
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws Exception			Dispara Exception
	 */
	public void transferir(HttpServletRequest req, HttpServletResponse res)
	throws Exception{
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Entrando a transferir.", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		LYMValidador valida = new LYMValidador();
		RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		RET_LiberaOperDAO dao = new RET_LiberaOperDAO();
		TrxGP72BO busGP72 = new TrxGP72BO();
		TrxPE80BO busPE80 = new TrxPE80BO();
		TrxGPF2BO busGPF2;
		TrxGP72VO beanGP72 = new TrxGP72VO();
		TipoCambioEnlaceDAO daoCI = new TipoCambioEnlaceDAO();
		ServicioTux tuxGlobal = new ServicioTux();
		Vector listaBusquedaBancos = null;
		Hashtable hs = null;
		Vector paises = null;
		String producto = null;
		String[] datosCtaInter = null;
		String codError = null;
		String paisBenef = null;
		String bancoCorresponsal = null;
		String numUsuario = null;
		String numContrato = null;
		String nomContrato = null;
		String mensaje = null;
		String tipoAbono = "S";
		String folioMultiabono = "";
		String nombreBenef = null;
		String apePaternoBenef = null;
		String apeMaternoBenef = null;
		int[] regsBatch = null;
		boolean errorAbono = false;

		try{
			if(!esHorarioValido()){
				paginaError("<b>FUERA DE HORARIO PARA OPERAR.</b>", req, res);
				return;
			}

			numUsuario = session.getUserID8();
			numContrato = session.getContractNumber();
			nomContrato = session.getNombreContrato();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se inicia validaci�n de limites y montos.", EIGlobal.NivelLog.DEBUG);
	        String valLyM = valida.validaLyM(numContrato, numUsuario,
	        		                         BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_VALIDA_LIMITES_MONTOS,
	        		                         bean.getImporteTotOper());
	        valida.cerrar();//CSA
	        
	        if(!valLyM.startsWith("ALYM0000")) {
	        	EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: No paso validaci�n de limites y montos", EIGlobal.NivelLog.DEBUG);
	        	EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Codigo de Error->" + valLyM.substring(0, 8), EIGlobal.NivelLog.DEBUG);
				paginaError(valLyM.substring(8), req, res);
				return;
	        }
	        EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se paso validaci�n de limites y montos correctamente", EIGlobal.NivelLog.DEBUG);
	        if(bean.getDetalleAbono().size()>1)
				tipoAbono = "M";

			dao.initTransac();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se inicia la conexi�n exitosamente.", EIGlobal.NivelLog.DEBUG);
			for(int i=0; i<bean.getDetalleAbono().size(); i++){
				if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getTipoCuenta().equals(RET_CuentaAbonoVO.CUENTA_MISMO_BANCO)){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: CUENTA MISMO BANCO, iter="+i, EIGlobal.NivelLog.DEBUG);
					producto = "CHDO";
					bancoCorresponsal = "";
					paisBenef = "";
				}else if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getTipoCuenta().equals(RET_CuentaAbonoVO.CUENTA_INTERNACIONAL)){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: CUENTA INTERNACIONAL, iter="+i, EIGlobal.NivelLog.DEBUG);
					producto = "TRAN";

					// Obtenemos el pais internacional
					try{
						datosCtaInter = daoCI.obtenDatosCtaInter(session.getContractNumber(), ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta());
					    paises = CatDivisaPais.getInstance().getListaPaises();
					    EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir::Pais a buscar->"+datosCtaInter[0].trim()+"<", EIGlobal.NivelLog.DEBUG);
				    	for(int l=0; l<paises.size(); l++){
				    		if(((TrxGP93VO)paises.get(l)).getOVarCod().substring(4).trim().equals(datosCtaInter[0].trim())){
				    			paisBenef = ((TrxGP93VO)paises.get(l)).getOVarCod().substring(0,3).trim();
				    		    break;
				    		}
				    	}
					}catch(Exception e){
						paisBenef = "";
						EIGlobal.mensajePorTrace("RET_LiberaOperBO:: transferir:: Problemas al obtener los datos del pais internacional", EIGlobal.NivelLog.DEBUG);
					}

					// Obtenemos el banco corresponsal internacional
					try{
						busGPF2 = new TrxGPF2BO();
						listaBusquedaBancos = busGPF2.obtenCatalogoBancos((paisBenef.trim().equals("001")?datosCtaInter[1]:datosCtaInter[2]),
																		   paisBenef, "",
									                                       (paisBenef.trim().equals("001")?"A":"S"));
						if(listaBusquedaBancos.size()==1){
							bancoCorresponsal = ((TrxGPF2VO)listaBusquedaBancos.get(0)).getOIdBaCo();
							paisBenef = ((TrxGPF2VO)listaBusquedaBancos.get(0)).getOCtry();
						}else{
							bancoCorresponsal = "";
							paisBenef = "";
						}
					}catch(Exception e){
						bancoCorresponsal = "";
						EIGlobal.mensajePorTrace("RET_LiberaOperBO:: transferir:: Problemas al obtener los datos de la cuenta internacional", EIGlobal.NivelLog.DEBUG);
					}
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: El Banco Corresponsal es " + bancoCorresponsal, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: El Pais es " + paisBenef, EIGlobal.NivelLog.DEBUG);

					// Obtenemos el beneficiario de la cuenta de abono
					try{
						if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length()>40){
							nombreBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().substring(0, 40);
							if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length()<60){
								apePaternoBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().
													substring(40, ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length());
								apeMaternoBenef = "";
							}else if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length()>60
									&& ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length()<80){
								apePaternoBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().substring(40, 60);
								apeMaternoBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().
													substring(60, ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length());
							}else if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().length()>80){
								apePaternoBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().substring(40, 60);
								apeMaternoBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta().substring(60, 80);
							}
						}else{
							nombreBenef = ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getBenefCuenta();
							apePaternoBenef = "";
							apeMaternoBenef = "";
						}
					}catch(Exception e){
						EIGlobal.mensajePorTrace("RET_LiberaOperBO:: transferir:: Problemas al sacar los datos del beneficiario Internacional", EIGlobal.NivelLog.DEBUG);
						nombreBenef = "";
						apePaternoBenef = "";
						apeMaternoBenef = "";
					}
				}

				// Ejecutamos transacci�n de liberaci�n
				beanGP72 = busGP72.ejecutaLiquidacionRET(folioMultiabono, bean.getCveTcOper(), tipoAbono, bean.getCtaCargo(), bean.getDivisaOperante(),
										   				 bean.getContraDivisa(), bean.getImporteTotOper(), bean.getTcPactado(),
										                 bean.getTcBase(), bean.getConcepto(), producto,
										                 ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta(),
										                 ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getImporteAbono(),
										                 nombreBenef, apePaternoBenef, apeMaternoBenef, paisBenef, bancoCorresponsal);
				if(beanGP72.getErroresReintento()){
					dao.closeTransacError();
					paginaError(beanGP72.getMensajeError(), req, res);

					EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n de reintento.", EIGlobal.NivelLog.DEBUG);
		            		escribeBitacoraOper(req, res, bean,
		            		BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "0080",
		            		BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA);

					return;
				}

				if(beanGP72.getOperacionExitosa()){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se libero correctamente el abono", EIGlobal.NivelLog.DEBUG);
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setFolioOperAbono(beanGP72.getFolioOperacion());
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setFolioTransfer(beanGP72.getFolioTransfer());
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setEstatusOperacion("L");
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setObservacionesEnvio("Abono Correcto");
					folioMultiabono = beanGP72.getFolioOperacionPadre();
				}else{
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se libero con error el abono", EIGlobal.NivelLog.DEBUG);
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setEstatusOperacion("N");
					((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setObservacionesEnvio(beanGP72.getMensajeError());
					errorAbono = true;
				}

				if(dao.liberaAbonoOper(beanGP72, bean.getFolioEnla(), numContrato, ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta())){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se guardo la transferencia correctamente", EIGlobal.NivelLog.DEBUG);
				}else{
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Ejecutando rollback problemas en el abono", EIGlobal.NivelLog.DEBUG);
					dao.closeTransacError();
					paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
					return;
				}

			}
			if(folioMultiabono!=null && !folioMultiabono.trim().equals(""))
				bean.setFolioOper(folioMultiabono);

			GregorianCalendar cal = new GregorianCalendar();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", new java.util.Locale("es","mx"));
			bean.setFchHoraLiberado(sdf.format(cal.getTime()));
			bean.setUsrAutoriza(numUsuario);
			if(dao.liberaCargoOper(bean, numContrato, errorAbono))
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se actualiza el cargo", EIGlobal.NivelLog.DEBUG);
			else{
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Ejecutando rollback problemas en el cargo", EIGlobal.NivelLog.DEBUG);
				dao.closeTransacError();
				paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
				return;
			}

			regsBatch = dao.execTransac();
			for(int j=0; j<regsBatch.length; j++){
				if(regsBatch[j]<=0){
					EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Ejecutando Rollback.", EIGlobal.NivelLog.DEBUG);
					dao.closeTransacError();
					paginaError("Problemas al realizar la operaci&oacute;n<BR>intente m&aacute;s tarde.", req, res);
					return;
				}
			}
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: La operacion fue exitosa", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Ejecutando Commit", EIGlobal.NivelLog.DEBUG);
			dao.closeTransac();
			if(!errorAbono){
				bean.setEstatusOperacion("L");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n exitosa", EIGlobal.NivelLog.DEBUG);
				escribeBitacoraOper(req, res, bean,
						           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "0000",
						           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA);
			}else{
				bean.setEstatusOperacion("N");
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n exitosa", EIGlobal.NivelLog.DEBUG);
				escribeBitacoraOper(req, res, bean,
						           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "0080",
						           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA);
			}
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Se procede a realizar la notificaci�n..", EIGlobal.NivelLog.DEBUG);
			lanzaEmail(req, numContrato, nomContrato, bean);

		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Problemas al transferir la operaci�n->"+e.getMessage(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::transferir:: Ejecutando Rollback.", EIGlobal.NivelLog.DEBUG);
			dao.closeTransacError();
			EIGlobal.mensajePorTrace("RET_LiberaOperBO::registrar:: Se procede a bitacorizar operaci�n erronea", EIGlobal.NivelLog.DEBUG);
			escribeBitacoraOper(req, res, bean,
					           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA + "9999",
					           BitaConstants.ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA);
			paginaError("Problemas al realizar la operaci&oacute;n<BR>intente m&aacute;s tarde.", req, res);
			return;
		}
		mensaje = "\"Se libero correctamente la operaci�n.\",1";

		sess.setAttribute("beanOperacionRET", bean);
		//req.setAttribute("mensaje", mensaje);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Complemento y Liberaci�n de Operaciones","Transferencias &gt; Transferencias FX Online &gt; Liberaci�n de Operaciones","RetLib01", req));
	}

	/**
	 * Metodo para obtener una divisa de cargo o abono
	 *
	 * @param divisaOperante	-	Divisa Operante de RET
	 * @param contraDivisa		- 	ContraDivisa de RET
	 * @param tipoOperacion		-	Tipo de Operaci�n RET
	 * @param naturaleza		-	Cargo o Abono
	 * @return
	 */
	public String obtenNaturalezaDivisa(String divisaOperante, String contraDivisa,
			String tipoOperacion, String naturaleza) {
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::getDivisa:: Entrando a getDivisa", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::getDivisa:: divisaOperante->"+divisaOperante, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::getDivisa:: contraDivisa->"+contraDivisa, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::getDivisa:: tipoOperacion->"+tipoOperacion, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("RET_LiberaOperBO::getDivisa:: naturaleza->"+naturaleza, EIGlobal.NivelLog.DEBUG);
		if(tipoOperacion.trim().equals("VTA") && naturaleza.trim().equals("cargo"))
			return contraDivisa;
		else if(tipoOperacion.trim().equals("CPA") && naturaleza.trim().equals("cargo"))
			return divisaOperante;
		else if(tipoOperacion.trim().equals("VTA") && naturaleza.trim().equals("abono"))
			return divisaOperante;
		else if(tipoOperacion.trim().equals("CPA") && naturaleza.trim().equals("abono"))
			return contraDivisa;
		return "";
	}

	/**
	 * Metodo encargado de disparar la rutina de confirmaci�n via Email
	 *
	 * @param req					Request del sistema
	 * @param numContrato			Numero de Contrato
	 * @param nomContrato			Nombre de Contrato
	 * @param RET_OperacionVO		Objeto contenedor de RET_OperacionVO
	 */
	private void lanzaEmail(HttpServletRequest req, String numContrato, String nomContrato,
			                RET_OperacionVO bean){

		DecimalFormat format = new DecimalFormat("#,###,##0.00");

		String codErrorOper = "REUT0000";
		String detAbonos = "";
		String etiquetaDiv = "";
		if(bean.getTipoOperacion().trim().equals("CPA"))
			etiquetaDiv = format.format(Double.parseDouble(bean.getImporteTotOper())) + " " + bean.getDivisaOperante();
		else if(bean.getTipoOperacion().trim().equals("VTA"))
			etiquetaDiv = "" + format.format((Double.parseDouble(bean.getImporteTotOper()) * Double.parseDouble(bean.getTcPactado()))) + " " + bean.getContraDivisa();

		if(sender.enviaNotificacion(codErrorOper)){
			try{
				details.setEstatusActual(bean.getDescEstatusOperacion());
				details.setNumeroContrato(numContrato);
				details.setRazonSocial(nomContrato);
				details.setNumRegImportados(bean.getDetalleAbono().size());
				details.setNumRef(bean.getFolioEnla());
				details.setImpTotal(etiquetaDiv);
				details.setNumCuentaCargo(bean.getCtaCargo());
				for(int i=0; i<bean.getDetalleAbono().size(); i++){
					detAbonos += "<tr>"
							  +  "<td>" + obtenSubCadena(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getNumCuenta(), 4) + "</td>"
							  +  "<td>" + ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getObservacionesEnvio() + "</td>"
							  +  "</tr>";
				}
				details.setDetalle_Alta("<center><table border=1>"
						               + "<tr>"
						               + "<td>Cuenta de Abono</td>"
									   + "<td>Estatus Abono</td>"
									   + "</tr>"
									   + detAbonos
									   + "</table></center>");

				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getEstatusActual()   -> " + details.getEstatusActual(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getNumeroContrato()  -> " + details.getNumeroContrato(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getRazonSocial()     -> " + details.getRazonSocial(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getNumRegImportados()-> " + details.getNumRegImportados(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getNumRef()          -> " + details.getNumRef(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getImpTotal()        -> " + details.getImpTotal(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getNumCuentaCargo()  -> " + details.getNumCuentaCargo(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: details.getDetalle_Alta()    -> " + details.getDetalle_Alta(), EIGlobal.NivelLog.DEBUG);

				sender.sendNotificacion(req, IEnlace.NOT_TRANSFERENCIAS_FX_ONLINE, details);
			}catch (Exception e) {
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("RET_LiberaOperBO::lanzaEmail:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
									   + "<DATOS GENERALES>"
									   + "Linea de truene->" + lineaError[0]
						               , EIGlobal.NivelLog.INFO);
			}
		}
	}

	/**
	 * Metodo encargado de validar que el horario sea valido
	 *
	 * @return	-	true Horario Valido
	 * 			-	false Horario Invalido
	 */
	private boolean esHorarioValido(){
		Date dateHoy = new Date();
		int h = 0;
		int m = 0;
		try{
			h = Integer.parseInt(Global.HORA_FX_ONLINE_LIQ);
			m = Integer.parseInt(Global.MINUTOS_FX_ONLINE_LIQ);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Hora parametrizada:" + h, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Minuto parametrizado:" + m, EIGlobal.NivelLog.DEBUG);
			if(dateHoy.getHours()<h){
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Valido", EIGlobal.NivelLog.DEBUG);
				return true;
			}else if(dateHoy.getHours()==h && dateHoy.getMinutes()<=m){
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Valido", EIGlobal.NivelLog.DEBUG);
				return true;
			}else{
				EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Horario Invalido", EIGlobal.NivelLog.DEBUG);
				return false;
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_PactaOperBO::esHorarioValido:: Problemas al validar el horario->"+e.getMessage(),
					EIGlobal.NivelLog.DEBUG);
		}
		return false;
	}

	/**
	 * Metodo utileria para pasar los ultimos caracteres de una cadena
	 *
	 * @param cadena	-	Cadena a truncar
	 * @param nCarac	-	Numero de caracteres a truncar
	 * @return			-	Cadena formateada
	 */
    private String obtenSubCadena(String cadena, int nCarac){
    	String subString = "";
    	cadena = cadena.trim();

    	if (!cadena.equalsIgnoreCase("SIN CUENTA")) {
	    	if(cadena != null && cadena.length() > nCarac){
	    		subString = cadena.substring(cadena.length() - nCarac);
	    		EIGlobal.mensajePorTrace("RET_LiberaOperBO::obtenSubCadena:: La cadena obtenida es: " + subString, EIGlobal.NivelLog.DEBUG);
	    	} else {
	    		EIGlobal.mensajePorTrace("RET_LiberaOperBO::obtenSubCadena:: La cadena para substring esta vacia o es null", EIGlobal.NivelLog.DEBUG);
	    	}
    	} else {
    		subString = cadena;
    	}
    	return subString;
    }

    /**
     *
     * @param req
     * @param res
     */
    private void actualizaMontos(HttpServletRequest req, HttpServletResponse res){
        HttpSession sess = req.getSession();
        RET_OperacionVO bean = (RET_OperacionVO) sess.getAttribute("beanOperacionRET");
		/*  int idRegistroAbono = 0;

		  try{
		        idRegistroAbono = Integer.parseInt((String)getFormParameter(req,"idRegistroAbono"));
		  }catch(NumberFormatException e){
		        EIGlobal.mensajePorTrace("RET_LiberaOperBO::actualizaMontos:: Primer Abono a insertar",EIGlobal.NivelLog.DEBUG);
		  }
		  if(idRegistroAbono==0 && bean.getDetalleAbono().size()>0)
			  idRegistroAbono = bean.getDetalleAbono().size();

	  /*for(int i=0; i<idRegistroAbono; i++){
	        for(int j=0; j<idRegistroAbono; j++){
	              EIGlobal.mensajePorTrace("RET_LiberaOperBO::actualizaMontos:: ((RET_CuentaAbonoVO)bean.getDetalleAbono().get("+i+")).getIdRegistro()->"
	                    + ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro(), EIGlobal.NivelLog.DEBUG);
	              EIGlobal.mensajePorTrace("RET_LiberaOperBO::actualizaMontos:: impIdRegistroAbono"+j+"->"
	                    + ((String)getFormParameter(req,"impIdRegistroAbono"+j)), EIGlobal.NivelLog.DEBUG);
	              if(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).getIdRegistro()==j)
	                    ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i)).setImporteAbono((String)getFormParameter(req,"impIdRegistroAbono"+j));
	        }
	  }*/
		  for(int i=0; i<bean.getDetalleAbono().size(); i++){
			  ((RET_CuentaAbonoVO)bean.getDetalleAbono().get(i))
			  	.setImporteAbono(getFormParameter(
			  			req,"impIdRegistroAbono" + i) != null ? (String)getFormParameter(req,"impIdRegistroAbono" + i):"0.00");
		  }
	}
}