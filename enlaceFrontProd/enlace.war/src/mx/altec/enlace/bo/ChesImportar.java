package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class ChesImportar{

   String strLinea              = "";
   public String cuentaCargo    = "";
   public String numCheque             = "";
   public String cveBeneficiario       = "";
   public String nombreBeneficiario    = "";
   public String importe               = "";
   public String fechaLibramiento      = "";
   public String fechaLimite           = "";

   public Calendar calFechaLibramiento;
   public Calendar calFechaLimite;

   Calendar hoy      = new GregorianCalendar();
   Calendar fechaHoy = new GregorianCalendar( hoy.get(hoy.YEAR), hoy.get(hoy.MONTH), hoy.get( hoy.DAY_OF_MONTH) );

   Vector diasNoHabiles;

   public int err = 0;
   public StringBuffer msg_error = new StringBuffer ("");   /* <--- */


  /** El metodo ChesImportar() es el constructor.
  *
  */
  public ChesImportar(String linea, Vector paramDias)
   {

     diasNoHabiles = paramDias;
     try
      {
        System.out.println("linea.length() "+linea.length());

        if(linea.length() == 132)
        {
            strLinea           = linea;
			cuentaCargo        = linea.substring(0,16).trim();
            numCheque          = linea.substring(16,23).trim();
            cveBeneficiario    = linea.substring(23,36).trim();
            nombreBeneficiario = linea.substring(36,96).trim();
            importe            = linea.substring(96,112).trim();
            fechaLibramiento   = linea.substring(112,122).trim();
            fechaLimite        = linea.substring(122,132).trim();
			System.out.println("cuentaCargo "+cuentaCargo);
			System.out.println("numCheque "+numCheque);
			System.out.println("cveBeneficiario "+cveBeneficiario);
			System.out.println("nombreBeneficiario "+nombreBeneficiario);
			System.out.println("importe  "+importe);
			System.out.println("fechaLibramiento "+fechaLibramiento);
			System.out.println("fechaLimite "+fechaLimite);
            err = 0;
        }
        else
           err = linea.length();
      } catch(Exception e)
         {
		   EIGlobal.mensajePorTrace( "***ChesImportar.class Excepcion %ChesImportar() >> "+ e.toString() + " <<", EIGlobal.NivelLog.INFO);
           err = linea.length();
         }
    System.out.println("Salir de ChesImportar");

   }


   /** El metodo validaCaracteres() determina si el registro tiene
   *   caracteres invalidos.
   */
    public boolean validaCaracteres()
	{
       System.out.println("validaCaracteres");
       msg_error = new StringBuffer ("");   /* <--- */
	   int     i     = 0;
	   int     letra = 0;
	   boolean valida = true;
      System.out.println("antes del for");
	   for(i=0; i<strLinea.length(); i++)
		{
		   letra = strLinea.charAt(i);
		   //System.out.println("letra i"+letra+" "+i);
	        if( !( (letra==32) || (letra==45) || (letra>=47 && letra <=57) || (letra>=65 && letra <=90) || (letra>=97 && letra<=122) || (letra==209) || (letra==241) ) )
		    {
			   msg_error.append ("<br><DD><LI>Caracter no v&aacute;lido en la posici&oacute;n : ");
			   msg_error.append (Integer.toString(i+1));
			   System.out.println("mensaje :"+msg_error);

			   valida = false;
			}
		}
      System.out.println("despues del for");

		return valida;

	}


   /** El metodo validaCuentaCargo() determina si la cuenta del registro
   *   es una cuenta registrada para Cheque Seguridad.
   */
    public boolean validaCuentaCargo(Vector arrayCuentas)
    {
      System.out.println("Entrando a validacuentaCargo");
      msg_error = new StringBuffer (""); /* <--- */
	  boolean result=true;

	  if (cuentaCargo.length() == 0)
       {
          msg_error = new StringBuffer ("La Cuenta de Cargo es obligatoria.");
          result=false;
       }
      else if(!esNumerico(cuentaCargo))
       {
           msg_error = new StringBuffer ("La Cuenta de Cargo debe ser num&eacute;rica: ");
		   msg_error.append (cuentaCargo);
           result=false;
       }
       //modificacion para integracion
	  /*else
	   {
            for(int indice=0;indice<arrayCuentas.size();indice++)
            {
			   EIGlobal.mensajePorTrace( "***ChesImportar.class & " + "cta_archivo: " + cuentaCargo + "|cta_servicio: " + arrayCuentas.elementAt(indice) + " &", EIGlobal.NivelLog.INFO);
               if(cuentaCargo.trim().equals(arrayCuentas.elementAt(indice)))
                  return true;
            }
		   msg_error += "La Cuenta de Cargo es inv&aacute;lida: " + cuentaCargo;
           return false;
	   }*/
      return result;
    }


  /** El metodo validaNumCheque() verifica que el n�mero de cheque del registro
  *   no sea vac�o y que sea num�rico.
  */
  public boolean validaNumCheque()
   {
       msg_error = new StringBuffer ("");   /* <---*/
       if(numCheque.length() == 0)
        {
          msg_error = new StringBuffer ("El N&uacute;mero de Cheque es obligatorio.");
          return false;
        }

       try
         {
            Long valor = new Long(numCheque);
         }catch( Exception exceptionLeer )
           {
              msg_error = new StringBuffer ("El N&uacute;mero de Cheque debe ser num&eacute;rico: ");
			  msg_error.append (numCheque);
			  EIGlobal.mensajePorTrace( "***ChesImportar.class Excepcion %validaNumCheque() >> "+ exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
              return false;
           }

       return true;
   }

  /** El metodo validaCveBeneficiario() verifica que la clave del beneficiario
  *   no sea vac�a y que sea num�rico.
  */
   public boolean validaCveBeneficiario(String FacBenefNoReg)
   {
      msg_error = new StringBuffer ("");   /* <--- */
      if (cveBeneficiario.length() == 0)
       {
          if(!FacBenefNoReg.equals("true"))
           {
             msg_error = new StringBuffer ("La Clave del Beneficiario es obligatoria.");
             return false;
           }
       }

      return true;
   }

  /** El metodo validaNombreBeneficiario() verifica que el nombre del beneficiario
  *   no sea vac�o y tenga caracteres v�lidos.
  */
  public  boolean validaNombreBeneficiario()
   {
     msg_error = new StringBuffer ("");   /* <--- */
     if(nombreBeneficiario.length() == 0)
      {
        msg_error = new StringBuffer ("<br><DD><LI>El Nombre del Beneficiario es obligatorio.");
        return false;
      }
	 else if(!validaCaracteresNombre())
	   {
		 return false;
	   }

     return true;

   }

  /** El metodo validaCaracteresNombre() determina si el nombre del beneficiario
  *   tiene caracteres v�lidos.
  */
   public boolean validaCaracteresNombre()
	{
	   int     i     = 0;
	   int     letra = 0;
	   boolean valida = true;

       msg_error = new StringBuffer ("");   /* <--- */
	   for(i=0; i<nombreBeneficiario.length(); i++)
		{
		   letra = nombreBeneficiario.charAt(i);
		   if( !( (letra==32) || (letra>=65 && letra <=90) || (letra>=97 && letra<=122) || (letra==209) || (letra==241) ) )
		    {
			   msg_error.append ("\n<br><DD><LI>Caracter no v&aacute;lido para el Nombre del Beneficiario en la posici&oacute;n : ");
			   msg_error.append (Integer.toString(i+1));
			   valida = false;
			}
		}

		return valida;

	}


  /** El metodo validaImporte() verifica que el importe
  *   no sea vac�o y que sea num�rico.
  */
  public boolean validaImporte()
   {
      msg_error = new StringBuffer ("");   /* <--- */

      if (importe.length() == 0)
       {
          msg_error = new StringBuffer ("El Importe es obligatorio.");
          return false;
       }
      else
       {
          if(!esNumerico(importe))
           {
             msg_error = new StringBuffer ("El Importe debe ser num&eacute;rico: ");
			 msg_error.append (importe);
             return false;
           }
       }

      return true;
   }

  /** El metodo validaFechaLibramiento() verifica que la fecha de libramiento
  *   sea una fecha v�lida.
  */
   public  boolean validaFechaLibramiento()
   {
      msg_error = new StringBuffer ("");
      boolean resultado = false;
      Calendar fechaLibramientoFin;

      if (esFecha(fechaLibramiento))
        {
          calFechaLibramiento = toCalendar(fechaLibramiento);
          if (!calFechaLibramiento.before(fechaHoy))
           {
             fechaLibramientoFin = new GregorianCalendar( fechaHoy.get(fechaHoy.YEAR), fechaHoy.get(fechaHoy.MONTH), fechaHoy.get( fechaHoy.DAY_OF_MONTH) );
             fechaLibramientoFin.add( Calendar.MONTH, 3 );
             fechaLibramientoFin = evaluaDiaHabil(fechaLibramientoFin);

             if (!calFechaLibramiento.after(fechaLibramientoFin))
              {
               resultado = true;
              }
             else
              {
                 msg_error = new StringBuffer ("La Fecha de Libramiento no puede ser mayor al ");
				 msg_error.append (EIGlobal.formatoFecha(fechaLibramientoFin, "dd/mm/aaaa"));
              }
           }
          else
           {
             msg_error = new StringBuffer ("La Fecha de Libramiento no puede ser menor al ");
			 msg_error.append (EIGlobal.formatoFecha(fechaHoy, "dd/mm/aaaa"));
           }
        }
       else
        {
          msg_error = new StringBuffer ("La Fecha de Libramiento no es una fecha v&aacute;lida.");
        }

      return resultado;
   }

  /** El metodo validaFechaLimite() verifica que la fecha de l�mite
  *   sea una fecha v�lida.
  */
  public boolean  validaFechaLimite()
   {
      boolean resultado = false;
      Calendar fechaLimiteFin;

      if (esFecha(fechaLimite))
        {
          calFechaLimite = toCalendar(fechaLimite);
          if (!calFechaLimite.before(calFechaLibramiento))
           {
             fechaLimiteFin = new GregorianCalendar( calFechaLibramiento.get(calFechaLibramiento.YEAR), calFechaLibramiento.get(calFechaLibramiento.MONTH), calFechaLibramiento.get( calFechaLibramiento.DAY_OF_MONTH));
             fechaLimiteFin.add( Calendar.MONTH, 6 );
             fechaLimiteFin = evaluaDiaHabil(fechaLimiteFin);
             if (!calFechaLimite.after(fechaLimiteFin))
              {
               resultado = true;
              }
             else
              {
                 msg_error = new StringBuffer ("La Fecha Limite no puede ser mayor al ");
				 msg_error.append (EIGlobal.formatoFecha(fechaLimiteFin, "dd/mm/aaaa"));
              }
           }
          else
           {
             msg_error = new StringBuffer ("La Fecha Limite no puede ser menor al ");
			 msg_error.append (fechaLibramiento);
           }

        }
      else
        {
          msg_error = new StringBuffer ("La Fecha Limite no es una fecha v&aacute;lida.");
        }


      return resultado;
   }


 /** El metodo evaluaDiaHabil() determina si la variable fecha es inhabil,
 *   y devuelve el d�a habil siguiente.
 */
 public Calendar evaluaDiaHabil(Calendar fecha)
   {
      int      indice;
      int      diaDeLaSemana;
      boolean  encontrado;

             if(diasNoHabiles!=null)
             {
               fecha.add( fecha.DAY_OF_MONTH, -1);
               do{
                   fecha.add( fecha.DAY_OF_MONTH,  1);

                   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
                   encontrado = false;
                   for(indice=0; indice<diasNoHabiles.size(); indice++)
                   {
                      String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
                      String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
                      String anio = Integer.toString( fecha.get( fecha.YEAR) );
                      String strFecha = dia.trim() + "/" + mes.trim() + "/" + anio.trim();
                      if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
                      {
                         System.out.println("Dia es inhabil");
                         encontrado = true;
                         break;
                      }
                   }
               }while( (diaDeLaSemana == Calendar.SUNDAY)   ||
                       (diaDeLaSemana == Calendar.SATURDAY) ||
                       encontrado );
             }

         return fecha;
      }


 /** El metodo esFecha() determina si la variable strFecha
 *   es una fecha v�lida.
 */
 public boolean esFecha(String strFecha)
  {
    Calendar fecha;
    String separador = "/";

     try
      {

        if (strFecha.substring(2, 3).equals(separador) && strFecha.substring(5, 6).equals(separador))
          {
            int dia  = Integer.parseInt(strFecha.substring(0, 2));
            int mes  = Integer.parseInt(strFecha.substring(3, 5)) - 1;
            int anio = Integer.parseInt(strFecha.substring(6, 10));
            fecha = new GregorianCalendar(anio, mes, dia);
            return true;
          }
        else
          return false;

      } catch(Exception e)
          {
		   EIGlobal.mensajePorTrace( "***ChesImportar.class Excepcion %esFecha() >> "+ e.toString() + " <<", EIGlobal.NivelLog.INFO);
           return false;
          }

  }


 /** El metodo toCalendar() convierte un String en un Calendar.
 *
 */
public  Calendar toCalendar(String strFecha)
  {
    Calendar fecha;
    String separador = "/";

     try
      {
        if (strFecha.substring(2, 3).equals(separador) && strFecha.substring(5, 6).equals(separador))
          {
            int dia  = Integer.parseInt(strFecha.substring(0, 2));
            int mes  = Integer.parseInt(strFecha.substring(3, 5)) - 1;
            int anio = Integer.parseInt(strFecha.substring(6, 10));
            fecha = new GregorianCalendar(anio, mes, dia);
         }
        else
          fecha = null;

      } catch(Exception e)
          {
		   EIGlobal.mensajePorTrace( "***ChesImportar.class Excepcion %toCalendar() >> "+ e.toString() + " <<", EIGlobal.NivelLog.INFO);
           fecha = null;
          }

      return fecha;

  }


 /** El metodo esNumerico() determina si la variable dato es num�rica.
 *
 */
 public boolean esNumerico(String dato)
   {
          try
           {
             Long valor = new Long(dato.trim());
           }catch( Exception exceptionLeer )
              {
				 EIGlobal.mensajePorTrace( "***ChesImportar.class Excepcion %esNumerico() >> "+ exceptionLeer.getMessage() + " <<", EIGlobal.NivelLog.INFO);
                 return false;
              }

         return true;
   }


}
