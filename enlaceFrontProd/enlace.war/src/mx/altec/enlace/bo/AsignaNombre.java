package mx.altec.enlace.bo;

import java.io.*;
import java.util.*;

import mx.altec.enlace.dao.CombosConf;

public class AsignaNombre {

	public AsignaNombre () {
	}

//	public void mLee ( String tipo, String persona) throws IOException {
    public String  asigno( String Clave) {
		String tipo="9";
		String NombreProveedor = new String();
		CombosConf archivo1 = new CombosConf();
		String Archivo = archivo1.envia_tux();
		String clave = Clave;
		char Tipo = tipo.charAt (0);
		String registroLeido = "";
		String campo = "";
		Vector lista = new Vector();
		RandomAccessFile fileAmbiente=null;

		try {
			File archclv = new File(Archivo);
			System.out.println(archclv.getPath());
			System.out.println("********** El archivo : ****"+Archivo);
			fileAmbiente= new RandomAccessFile(archclv,"r");
		} catch ( IOException e ) {
			System.out.println( "Error al leer el archivo de ambiente, e1 " + e);
		}


		System.out.println (" Antes de leer registros ");
		do {
			try {
				registroLeido = fileAmbiente.readLine();
			} catch ( IOException e ) {
				System.out.println( "Error al leer el archivo de ambiente, e2 " + e);
			}
		} while ( registroLeido != null && registroLeido.charAt (0) != Tipo );
		System.out.println (" Despues de leer  los registros y antes de crear lista ");

		try {
			do {
				if ( registroLeido.startsWith( tipo ) ) {
					if (tipo != "9") {
						    campo =	registroLeido.substring(
							registroLeido.indexOf( ';' , 2 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) );
						    lista.add(campo);
						} else {
						campo = registroLeido.substring(4,
                                registroLeido.indexOf( ';', 6));
                        if (campo.equals(clave))
						{
							campo = registroLeido.substring(
							registroLeido.indexOf( ';' , 12 ) + 1 ,
							registroLeido.lastIndexOf( ';' ) - 2);
						    campo = campo.replace(';',' ') ;
							NombreProveedor = campo;
   						}
							lista.add(campo);
					}
				}
			} while ( ( registroLeido = fileAmbiente.readLine() ) != null );
		} catch ( IOException e ) {
				System.out.println( "Error al leer los registros del archivo de ambiente" );
		}
		System.out.println (" Lista creada, antes de cerrar el archivo ");

		try {
			fileAmbiente.close();
		} catch ( IOException e ) {
			System.out.println( "Error al cerrar el archivo de ambiente" );
		}
		System.out.println (" Archivo cerrado ");
		return NombreProveedor;
	}
}