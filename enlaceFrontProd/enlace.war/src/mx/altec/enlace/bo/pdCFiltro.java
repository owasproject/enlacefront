/*
 * pdCFiltro.java
 *
 * Created on 8 de abril de 2002, 08:48 AM
 */

package mx.altec.enlace.bo;

import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.utilerias.EIGlobal;


/**
 *
 * @author Rafael Martinez
 * @version 1.0
 */
public class pdCFiltro implements java.io.Serializable {
    final int REGISTRADOS   = 0;
    final int PENDIENTES    = 1;
    final int CANCELADOS    = 2;
    final int LIQUIDADOS    = 3;
    final int VENCIDOS      = 4;
    final int MODIFICADOS   = 5;

    /** Holds value of property fechaIni. */
    private GregorianCalendar fechaIni;

    /** Holds value of property fechaFin. */
    private GregorianCalendar fechaFin;

    /** Holds value of property pagoIni. */
    private String pagoIni="";

    /** Holds value of property pagoFin. */
    private String pagoFin="";

    /** Holds value of property cuenta. */
    private String cuenta="";

    /** Holds value of property beneficiario. */
    private String beneficiario="";

    /** Holds value of property importe. */
    private String importe="";

    /** Holds value of property opciones. */
    private String[] opciones;

    /** Holds value of property mensaje. */
    private String mensaje="";

    /** Query especifico con este set de filtros...
     */
    private String query="";

	/** Creates new pdCFiltro */
    public pdCFiltro() {

    }

    /** Getter for property fechaIni.
     * @return Value of property fechaIni.
     */
    public GregorianCalendar getFechaIni() {
        return fechaIni;
    }

    /** Setter for property fechaIni.
     * @param fechaIni New value of property fechaIni.
     */
    public void setFechaIni(String fechaIni) {
        this.fechaIni = new GregorianCalendar();
        if(fechaIni != null){
            try{
                this.fechaIni.set(this.fechaIni.DAY_OF_MONTH,
                                    Integer.parseInt (fechaIni.substring (0,2)));
                this.fechaIni.set(this.fechaIni.MONTH,
                                    Integer.parseInt (fechaIni.substring (3,5)) - 1 );
                this.fechaIni.set(this.fechaIni.YEAR,
                                    Integer.parseInt (fechaIni.substring (6)));
            } catch (NumberFormatException e){
                this.fechaIni = new GregorianCalendar();

            }

        } else {
               EIGlobal.mensajePorTrace ("pdCFiltro:setFechaIni - null assignment attempted", EIGlobal.NivelLog.INFO);
        }
    }

    /** Setter for property fechaIni.
     * @param fechaIni valor para la nueva fecha inicial
     */
    public void setFechaIni(GregorianCalendar fechaIni){
        this.fechaIni = new GregorianCalendar();
        if(null != fechaIni){
            this.fechaIni.set(  fechaIni.get(fechaIni.YEAR),
                                fechaIni.get(fechaIni.MONTH),
                                fechaIni.get(fechaIni.DAY_OF_MONTH));
        } else {
            EIGlobal.mensajePorTrace ("pdCFiltro:setFechaIni - null assignment attempted", EIGlobal.NivelLog.INFO);
        }
    }

    /** Getter for property fechaFin.
     * @return Value of property fechaFin.
     */
    public GregorianCalendar getFechaFin() {
        return fechaFin;
    }

    /** Setter for property fechaFin.
     * @param fechaFin New value of property fechaFin.
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = new GregorianCalendar();
        if(fechaFin != null){
            try{
                this.fechaFin.set(this.fechaFin.DAY_OF_MONTH,
                                    Integer.parseInt (fechaFin.substring (0,2)));
                this.fechaFin.set(this.fechaFin.MONTH,
                                    Integer.parseInt (fechaFin.substring (3,5)) - 1 );
                this.fechaFin.set(this.fechaFin.YEAR,
                                    Integer.parseInt (fechaFin.substring (6)));
            } catch (NumberFormatException e){
                this.fechaFin = new GregorianCalendar();
            }

        } else {
               EIGlobal.mensajePorTrace ("pdCFiltro:setFechaFin - null assignment attempted", EIGlobal.NivelLog.INFO);
        }
    }

    /** setter para la fecha final
     * @param fechaFin valor apara la nueva fecha final
     */
    public void setFechaFin(GregorianCalendar fechaFin){
        this.fechaFin = new GregorianCalendar();
        if(null != fechaFin){
            this.fechaFin.set(  fechaFin.get(fechaFin.YEAR),
                                fechaFin.get(fechaFin.MONTH),
                                fechaFin.get(fechaFin.DAY_OF_MONTH));
        } else {

            EIGlobal.mensajePorTrace ("pdCFiltro:setFechaFin - null assignment attempted", EIGlobal.NivelLog.INFO);
        }
    }

    /** Getter for property pagoIni.
     * @return Value of property pagoIni.
     */
    public String getPagoIni() {
        return pagoIni;
    }

    /**
	 * Setter for property pagoIni.
	 *
	 * @param pagoIni
	 *            New value of property pagoIni.
	 * */
	public void setPagoIni(String pagoIni) {
		if (pagoIni != null) {
			this.pagoIni = pagoIni.trim();

			try {
				double i = Double.parseDouble(this.pagoIni);
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::setPagoIni() PagoIni [" + i + "]",
						EIGlobal.NivelLog.DEBUG);
				if (i <= 0) {
					this.pagoIni = "";
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::setPagoIni() Valor incorrecto, se dejara vacio para traer todos los campos",
						EIGlobal.NivelLog.DEBUG
				);
				this.pagoIni = "";
			}
		} else {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::setPagoIni() Valor nulo, se dejara vacio para traer todos los campos",
					EIGlobal.NivelLog.DEBUG
			);
			this.pagoIni = "";
		}
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::setPagoIni()::Fin con valor [" + this.pagoIni + "]",
				EIGlobal.NivelLog.INFO
		);
	}

    /** Getter for property pagoFin.
     * @return Value of property pagoFin.
     */
    public String getPagoFin() {
        return pagoFin;
    }

    /**
	 * Setter for property pagoFin.
	 *
	 * @param pagoFin
	 *            New value of property pagoFin.
	 * */
	public void setPagoFin(String pagoFin) {
		if (pagoFin != null) {
			this.pagoFin = pagoFin.trim();

			try {
				Double i = Double.parseDouble(this.pagoFin);
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::setPagoFin() PagoFin [" + i + "]",
						EIGlobal.NivelLog.DEBUG
				);
				if (i <= 0) {
					this.pagoFin = "";
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::setPagoFin() Valor incorrecto, se dejara vacio para traer todos los campos",
						EIGlobal.NivelLog.DEBUG
				);
				this.pagoFin = "";
			}
		} else {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::setPagoFin() Valor nulo, se dejara vacio para traer todos los campos",
					EIGlobal.NivelLog.DEBUG
			);
			this.pagoFin = "";
		}
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::setPagoFin()::Fin con valor [" + this.pagoFin + "]",
				EIGlobal.NivelLog.INFO
		);
	}

    /** Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public String getCuenta() {
        return cuenta;
    }

    /** Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(String cuenta) {
        if(cuenta != null){
            this.cuenta = cuenta;
        } else {
               this.cuenta = "";
        }
    }

    /** Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public String getBeneficiario() {
        return beneficiario;
    }

    /** Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(String beneficiario) {
        if(beneficiario != null){
            this.beneficiario = beneficiario;
        } else {
               this.beneficiario = "";
        }
    }

    /** Getter for property importe.
     * @return Value of property importe.
     */
    public String getImporte() {
        return importe;
    }

    /** Setter for property importe.
     * @param importe New value of property importe.
     */
    public void setImporte(String importe) {
        if(importe != null){

            this.importe = importe.trim();
        } else {
               EIGlobal.mensajePorTrace ("pdCFiltro:setImporte - null assignment attempted", EIGlobal.NivelLog.INFO);
               this.importe = "";
        }
    }

    /** Indexed getter for property opciones.
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public String getOpciones(int index) {
        return opciones[index];
    }

    /** Getter for property opciones.
     * @return Value of property opciones.
     */
    public String[] getOpciones() {
        return opciones;
    }

    /** Indexed setter for property opciones.
     * @param index Index of the property.
     * @param opciones New value of the property at <CODE>index</CODE>.
     */
    public void setOpciones(int index, String opciones) {
        if(opciones != null){
            this.opciones[index] = opciones;
        } else {
            EIGlobal.mensajePorTrace ("pdCFiltro:setOpciones(index,val) - null assignment attempted", EIGlobal.NivelLog.INFO);
            this.opciones[index] = "";
        }
    }

    /** Setter for property opciones.
     * @param opciones New value of property opciones.
     */
    public void setOpciones(String[] opciones) {
        if(opciones != null){
            this.opciones = opciones;
        } else {
               String [] tmp= {"0","1","2","3","4","5"};
               this.opciones= tmp;

        }
    }

    /** <CODE>fill</CODE>obtiene el contenido desde los parametros del request
     * @param req el HttpServletRequest del cual se obtendran los datos
     */
    public void fill(HttpServletRequest req) {

        try{
            setFechaIni( (String) req.getParameter("fechaIni"));
            setFechaFin( (String) req.getParameter("fechaFin"));

            if(null == fechaIni && null == fechaFin ){
                pdCFiltro filtro = (pdCFiltro) req.getSession().getAttribute("pdcFiltro");
                if(null != filtro){
                    this.beneficiario = filtro.beneficiario;
                    this.cuenta = filtro.cuenta;
                    this.fechaFin = filtro.fechaFin;
                    this.fechaIni = filtro.fechaIni;
                    this.importe = filtro.importe;
                    this.mensaje = filtro.mensaje;
                    this.pagoFin = filtro.pagoFin;
                    this.pagoIni = filtro.pagoIni;
                    this.query = filtro.query;
                    this.opciones = (String[]) filtro.opciones.clone();
                    return;
                }
            }

            setOpciones( (String []) req.getParameterValues("opciones"));

            setCuenta( (String) req.getParameter("txtCuenta"));

            setImporte( (String) req.getParameter("importe"));
            setBeneficiario( (String) req.getParameter("beneficiario"));
            setPagoIni( (String) req.getParameter("pagoIni"));
            setPagoFin( (String) req.getParameter("pagoFin"));
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
	 * <code>validate</code> Verifica los datos contenidos, en caso de cualquier
	 * error, pone el error en mensaje
	 *
	 * @return true si los datos son correctos, false de lo contrario
	 */
	public boolean validate() {
		mensaje = "";

		if (null == beneficiario || null == cuenta || null == importe
				|| null == pagoIni || null == pagoFin || null == fechaIni
				|| null == fechaFin || null == opciones) {
			mensaje = "Hay datos nulos";
			return false;
		}

		if (opciones.length > 0) {
			boolean hasLimits = false;
			for (int i = 0; i < opciones.length; ++i) {
				switch (Integer.parseInt(opciones[i])) {
				case REGISTRADOS: {
					break;
				}
				case PENDIENTES: {
					break;
				}
				case CANCELADOS: {
					break;
				}
				case LIQUIDADOS: {
					hasLimits = true;
					break;
				}
				case VENCIDOS: {
					hasLimits = true;
					break;
				}
				case MODIFICADOS: {
					hasLimits = true;
					break;
				}
				}
			}
			if (hasLimits) {
				if (fechaFin.before(fechaIni)) {
					mensaje += "Fecha final anterior a Fecha inicial. ";
				}
			}

			if (null != pagoIni && null != pagoFin) {
				try {
					if ((!pagoIni.equals("")) && (!pagoFin.equals(""))) {
						if (Double.parseDouble(pagoFin) < Double
								.parseDouble(pagoIni)) {
							mensaje += "Pago Inicial debe ser menor o igual a Pago Final. ";
						}
					}
				} catch (NumberFormatException e) {
					mensaje += "Numero de pago: introducir un valor";
				}
			} else {
				mensaje += "Numero de pago: introducir valores";
			}
		}
		if (mensaje.equals("")) {
			return true;
		} else {
			EIGlobal.mensajePorTrace(
					getClass().getName()
					+ "::validate()::Mensaje [" + mensaje + "]",
					EIGlobal.NivelLog.DEBUG
			);
			return false;
		}
	}

    /** Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public String getMensaje() {
        return mensaje;
    }

    /** Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
	 * Obtiene un query con las caracteristicas requeridas
	 *
	 * @return query
	 */
	public String getQuery() {
		query = "";
		if (!cuenta.equals("")) {
			query += " AND num_cuenta = '" + cuenta + "'";
		}
		if (!beneficiario.equals("")) {
			query += " AND beneficiario = '" + beneficiario + "'";
		}
		if (opciones.length > 0) {
			query += " AND ( ";
			for (int i = 0; i < opciones.length; ++i) {
				if (i > 0) {
					query += " OR";
				}

                switch(Integer.parseInt(opciones[i])) {
	                case REGISTRADOS:{
	                    query += "( status = 'R' AND (TRUNC(fch_recepcion) >= '" +
	                                fechaOracle(fechaIni) + "' AND TRUNC(fch_recepcion) <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";
	                    break;
	                }
	                case PENDIENTES:{
	                    query += "( status = 'I' AND (TRUNC(fch_recepcion) >= '" +
	                                fechaOracle(fechaIni) + "' AND TRUNC(fch_recepcion) <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";
	                    break;
	                }
	                case CANCELADOS:{
	                    query += "( status = 'C' AND (fch_cancelacion >=  '" +
	                                fechaOracle(fechaIni) + "' AND fch_cancelacion <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";
	                    break;
	                }
	                case LIQUIDADOS:{
	                    query += "( status = 'L' AND (fch_pago >= '" +
	                                fechaOracle(fechaIni) + "' AND fch_pago <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";
	                    break;
	                }
	                case VENCIDOS:{
	                    query += "( status = 'V' AND (fch_vencio >=  '" +
	                                fechaOracle(fechaIni) + "' AND fch_vencio <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";
	                    break;
	                }
	                case MODIFICADOS:{
	                    query += "( status LIKE 'M%' AND (fch_modif >= '" +
	                                fechaOracle(fechaIni) + "' AND fch_modif <= '" +
	                                fechaOracle(fechaFin) + "' ) ) ";

	                    break;
	                }
                }
			}
			query += " )";
		}
		if (null != pagoIni && null != pagoFin)
			if (!pagoIni.equals("") && !pagoFin.equals("")) {
				try {
					Double.parseDouble(pagoIni);
					Double.parseDouble(pagoFin);

                    query += " AND ( num_orden >= " +
                    		pagoIni + " AND num_orden <= " +
                    		pagoFin +" ) ";

				} catch (NumberFormatException e) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::getQuery()::Numero de Pago Inicial o final invalido, no sera incluido en filtro",
							EIGlobal.NivelLog.DEBUG
					);
				}
			}
		if (null != importe)
			if (!importe.equals("")) {
				try {
					Double.parseDouble(importe);
					query += " AND IMPORTE='" + importe + "' ";
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::getQuery()::Importe invalido, no sera incluido en filtro",
							EIGlobal.NivelLog.DEBUG
					);
				}
			}
		return query;
	}

    /** convierte un string de la forma dd/mm/aaaa a el formato de oracle
     * @param fecha es la fecha a convertir
     * @return un string con la fecha en en el formato de oracle
     */
    protected String fechaOracle(String fecha){
        String dia = fecha.substring(0, 2);
        String mes = fecha.substring(3, 3);
        String anio = fecha.substring(6);
        if(mes.equals("01")) mes = "jan";
        else if(mes.equals("02")) mes = "feb";
        else if(mes.equals("03")) mes = "mar";
        else if(mes.equals("04")) mes = "apr";
        else if(mes.equals("05")) mes = "may";
        else if(mes.equals("06")) mes = "jun";
		 		 else if(mes.equals("07")) mes = "jul";
        else if(mes.equals("08")) mes = "aug";
    		 else if(mes.equals("09")) mes = "sep";
		 		 else if(mes.equals("10")) mes = "oct";
        else if(mes.equals("11")) mes = "nov";
        else if(mes.equals("12")) mes = "dec";

        String res = dia + "-" + mes + "-" + anio;

        return res;
    }

    /** Conviere una fecha al formato de oracle
     * @param fecha es la facha a convertir al formato de oracle
     * @return un string con la fecha en el formato de oracle
     */
    protected String fechaOracle(GregorianCalendar fecha){

        String dia = fecha.get(fecha.DAY_OF_MONTH) < 10 ? "0" : "";
        dia += String.valueOf(fecha.get(fecha.DAY_OF_MONTH));
        String mes = "";

        switch( fecha.get(fecha.MONTH) ){
            case 0:{mes = "jan";break;}
            case 1:{mes = "feb";break;}
            case 2:{mes = "mar";break;}
            case 3:{mes = "apr";break;}
            case 4:{mes = "may";break;}
            case 5:{mes = "jun";break;}
            case 6:{mes = "jul";break;}
            case 7:{mes = "aug";break;}
            case 8:{mes = "sep";break;}
            case 9:{mes = "oct";break;}
            case 10:{mes = "nov";break;}
            case 11:{mes = "dec";break;}
            default:{
                mes = "ERROR";
                EIGlobal.mensajePorTrace ("pdCFiltro:fechaOracle - Error mes=" + fecha.get(fecha.MONTH), EIGlobal.NivelLog.INFO);

            }
        }
        String anio = String.valueOf(fecha.get(fecha.YEAR));
        return dia + "/" + mes + "/" + anio;


    }
}