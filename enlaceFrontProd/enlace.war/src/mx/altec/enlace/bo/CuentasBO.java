package mx.altec.enlace.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mx.altec.enlace.beans.CuentasBean;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.utilerias.EIGlobal;

public class CuentasBO {

	public List<String> consultaCuentasRel(String contrato){
		
		ArrayList<String> listaRegistros = null;
		List<CuentasBean> listaCtas = null;
		CuentasDAO ctasDao = null;
		Iterator<CuentasBean> it = null;
		CuentasBean ctaBean = null;
		String registro = "";
		
		EIGlobal.mensajePorTrace("Entra CuentasBO.consultaCuentasRel ->" + contrato, EIGlobal.NivelLog.INFO);
		
		listaRegistros = new ArrayList<String>();
		if(contrato == null || "".equals(contrato)){
			listaRegistros.add("ERROR");
		}else{
			ctasDao = new CuentasDAO();
			
			try {
				listaCtas = ctasDao.consultaCuentasRel(contrato,  "");
				
				EIGlobal.mensajePorTrace("CuentasBO.consultaCuentasRel -> Registros " + listaCtas.size(), EIGlobal.NivelLog.DEBUG);
				
				
				it = listaCtas.iterator();
				
				while(it.hasNext()){
					ctaBean = it.next();				
									
					registro = ctaBean.toString();				
					EIGlobal.mensajePorTrace("CuentasBO.consultaCuentasRel -> registro " + registro, EIGlobal.NivelLog.DEBUG);

					listaRegistros.add(registro);
					
				}			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				listaRegistros.add("ERROR");
				e.printStackTrace();
			}	
		}
		EIGlobal.mensajePorTrace("Sale CuentasBO.consultaCuentasRel ->" + listaRegistros.size(), EIGlobal.NivelLog.INFO);
		
		return listaRegistros;
	}
}
