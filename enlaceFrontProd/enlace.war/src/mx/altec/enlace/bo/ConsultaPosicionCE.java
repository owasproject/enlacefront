// Copyright (c) 2001 Santander
package mx.altec.enlace.bo;

import java.io.Serializable;
import java.util.*;
//import java.rmi.*;


/**
 * Esta clase es un <I>bean</I> que interpreta la trama de respuesta de <I>consulta de posiciones</I><BR>
 * e interact&uacute;a con el jsp para desplegar los datos de dicha trama.
 * <P>
 * Ejemlo trama correcta PC49:<BR>
 * OK      203848 CEES0000|1000.00|500.00|0.00|500.00|400.00|0.00|2400.00|
 * <P>
 * Fecha creaci&oacute;n: 16/06/2002<BR>
 * &Uacute;ltimo cambio<BR>
 * 17/04/2002: Constuctor sin par�metros.<BR>
 * 18/04/2002: Se agregaron nuevos atributos y m&eacute;todos.<BR>
 * 19/04/2002: Se modific� el m&eacute;todo setLinea.<BR>
 * 26/04/2002: Se implant� la interfase Serializable.<BR>
 * 24/07/2002: Se agregar&oacute; la verificaci&oacute;n de tiempo.<BR>
 * 25/07/2002: Se corrigi&oacute; la comparaci&oacute;n por tiempos.<BR>
 * 23/08/2002: Se eliminan posibles espacios en los datos de la trama.<BR>
 * 17/10/2002: Se agreg&oacute; el m&eacute;todo de instrospecci&oacute;n de lectura <B>esCorrecta</B>.<BR>
 * 25/11/2002: Se agreg&oacute; el atributo <B>mensajeError</B> y se modific&oacute; el m&eacute;todo <B><I>interpretaTramaPC49</I></B>.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.3.0
 */
public class ConsultaPosicionCE implements Serializable
{

/**Define tiempo m&aacute;ximo para considerar los datos como actuales.
*/
public static final long TIEMPO_LIMITE=150000;

/**Guarda el n&uacute;mero de contrato que obtiene un atributo de sesi&oacute;n
*/
private String contrato="";

/**Guarda el n&uacute;mero de usuario que obtiene de la sesi&oacute;n.
*/
private String usuario="";

/**Guarda el perfil del usuario.
*/
private String perfil="";

/**Guarda el par&aacute;metro <I>cuenta</I> para los servicios de consulta de posici&oacute;n o detalle disposici&oacute;n.
*/
private String cuenta="";

/**Guarda el par&aacute;metro <I>cuenta</I> para los servicios de consulta de posici&oacute;n o detalle disposici&oacute;n.
*/
private String linea="";

/**Indica la cantidad monetaria del capital vencido de la l&iacute;nea de cr&eacute;dito.
*/
private String vencido="";

/**Indica la cantidad monetaria del capital vigente de la l&iacute;nea de cr&eacute;dito.
*/
private String vigente="";

/**Cantidad monetaria de inter&eacute;s no exigible de la l&iacute;nea de cr&eacute;dito.
*/
private String noExigible="";

/**Cantidad monetaria de inter&eacute;s exigible de la l&iacute;nea de cr&eacute;dito.
*/
private String exigible="";

/**Intereses moratorios de la l&iacute;nea de cr&eacute;dito.
*/
private String moratorios="";

/**Adeudos jur&iacute;dicos de la l&iacute;nea de cr&eacute;dito.
*/
private String juridicos="";

/**Adeudos del seguro de la l&iacute;nea de cr&eacute;dito.
*/
private String seguro="";

/**Adeudo del seguro de la l&iacute;nea de cr&eacute;dito.
*/
private String otros="";

/**Suma de todos los adeudos de la l&iacute;nea de cr&eacute;dito.
*/
private String total="";

/**Guarda el n&uacute;mero de l&iacute;neas de cr&eacute;dito
*/
private String noLineas="";

/**Datos de la consulta de posici&oacute;n PC
*/
private String datos="";

/**Posici&oacute;n del arreglo de l&iacute;neas.
*/
private int posicion=-1;

/**Direcci&oacute;n que toma para moverse (atr&aacute;s o adelante) en el arreglo.
*/
private int direccion=0;

/**Arreglo de las l&iacute;neas de cr&eacute;dito.
*/
private String lineas[]=null;

/**Almacena todas las tramas de respuesta de todas la l&iacute;neas de cr&eacute;dito.
*/
private String tramasRespuesta[]=null;

/**Arreglo de enteros que indican la posici&oacute;n de cada elemento de la trama.
*/
public static int[] posiciones={8, 7, 4, 4};

/**Tama&ntilde;o m&iacute;nimo de la trama.
*/
public static final int tamanoPC49=23;

/**Caracter "pipe".
*/
public static String pipe="|";

/**Sirve para comparar los dos primeros caracteres de la trama de respuesta.
*/
public static final String OK="OK";

/**C&oacute;digo de error cuando la trama tiene el formato correcto.
*/
public static final String BIEN="0000";

/**C&oacute;digo de error cuando la trama no tiene el formato correcto.
*/
public static final String MAL="9999";

/**Cadena de la trama de consulta de posici&oacute;n PC49
*/
private String tramaPC49="";

/**Trama de respuesta de tuxedo del servicio PC49
*/
private String respuestaPC49="";

/**C&oacute;digo de error que obtiene de la trama.
*/
private String codigoError="";

/**Almacena los errores al interpretar la trama.
*/
private Vector errores=new Vector();
//private ServletConfig configuracion=null;

/**Indica si la interpretaci&oacute;n de la trama es correcta.
*/
private boolean esCorrecta=false;


/**Contiene el mensaje o descripci&oacute;n del error.
*/
private String mensajeError="";


/**Indica cu&aacute;ndo guard&oacute; los valores
*/
private Date fechaGeneracion=null;

  /**
   * Constructor vac&iacute;o
   */
  public ConsultaPosicionCE()
  {
  } // Fin contructor

   /**<code><B><I>interpretaTramaPC49</I></B></code> analiza la trama de respuesta del servicio de<BR>
   * consulta de posici&oacute;n <B>PC49</B> de tuxedo.
   * <P>Regresa verdadero si la trama corresponde la formato de la trama; regresa falso si<BR>
   * la trama es de error, nula o no tiene datos.
   * </P>
   */
  public boolean interpretaTramaPC49(String respuestaPC49)
  {
    StringTokenizer st;
    String aux;

    borrarDatos();

    if(respuestaPC49==null)
      return false;

    if(respuestaPC49.length()<tamanoPC49)
      return false;

    try
    {
      aux=respuestaPC49.substring(0, posiciones[0]).trim();

      if(!aux.equalsIgnoreCase(OK))
        return false;

      aux=respuestaPC49.substring(posiciones[0]+posiciones[1]+posiciones[2], posiciones[0]+posiciones[1]+posiciones[2]+posiciones[3]).trim();

      if(aux==null)
        return false;

      codigoError=aux;
      st=new StringTokenizer(respuestaPC49, pipe);

      if(!codigoError.equalsIgnoreCase(BIEN))
      {
        if(st.countTokens()>1)
        {
         st.nextToken();
         mensajeError=st.nextToken();
        } // Fin if tokens

        esCorrecta=false;
        return false;
      } // Fin codigoError

      st=new StringTokenizer(respuestaPC49, pipe);

      st.nextToken();
      vencido=st.nextToken().trim();
      vigente=st.nextToken().trim();
      noExigible=st.nextToken().trim();
      exigible=st.nextToken().trim();
      moratorios=st.nextToken().trim();
      juridicos=st.nextToken().trim();
      seguro=st.nextToken().trim();
      otros=st.nextToken().trim();
      total=st.nextToken().trim();

      esCorrecta=true;
    }
    catch(ArrayIndexOutOfBoundsException aioobe)
    {
      errores.addElement(aioobe.getMessage()+"\n");
      return false;
    }
    catch(NoSuchElementException nsee)
    {
      esCorrecta=true;
    } // Fin try-catch


    return esCorrecta;
  } // Fin interpretaTramaSDCT


  /*
  public void ejecutaPC49()
  {
    TramasCE tce=new TramasCE();

    tramaPC49=tce.consultaPosicionLinea(usuario, contrato, perfil, linea);
    respuestaPC49=ejecutaPC49(tramaPC49);

  } // Fin ejecutaPC49
  */


  /*
  public String ejecutaPC49(String trama)
  {
    Hashtable ht=null;
    String respuesta=null;

    try
    {
      ServicioTux tuxGlobal=new ServicioTux();
      tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext)configuracion.getServletContext()).getContext());
      ht=tuxGlobal.web_red(trama);
      respuesta=(String)ht.get("BUFFER");
      esCorrecta=true;
    }
    catch(RemoteException re)
    {
      errores.addElement(re.getMessage());
    }
    catch(Exception e)
    {
      errores.addElement(e.getMessage());
    } // Fin try-catch

    return respuesta;
  } // Fin m�todo ejecuta PC49
  */



   /**<code><B><I>arreglaLineas</I></B><code> despliega los elementos de <I>noLineas</I>.
   */
  public void arreglaLineas()
  {
    int numeroLineas=-1;

    if(noLineas!=null)
    {
      StringTokenizer st=new StringTokenizer(noLineas, "|");
      numeroLineas=st.countTokens();

      if(numeroLineas>2)
      {
        lineas=new String[numeroLineas-2];
        st.nextToken();
        st.nextToken();

        for(int i=0;i<numeroLineas-2;i++)
          lineas[i]=st.nextToken();
      } // Fin if tokens 2
    } // Fin if noLineas
  } // Fin m�todo arreglaLineas


  public void setContrato(String contrato)
  {
    if(contrato!=null)
      this.contrato=contrato;
  }

  public String getContrato()
  {
    return contrato;
  }

  public void setUsuario(String usuario)
  {
    if(usuario!=null)
      this.usuario=usuario;
  }

  public String getUsuario()
  {
    return usuario;
  }

  public void setPerfil(String perfil)
  {
    if(perfil!=null)
      this.perfil=perfil;
  }

  public String getPerfil()
  {
    return perfil;
  }

  public String getCuenta()
  {
    return cuenta;
  }

  public void setCuenta(String cuenta)
  {
    if(cuenta!=null)
      this.cuenta=cuenta;
  }

  public String getLinea()
  {
    return linea;
  }


   /**<code><B><I>getLineaEn</I></B></code> da el n&uacute;mero de l&iacute;nea en la posici&oacute;n solicitada.
   * <P>Regresa una cadena que corresponde al n&uacute;mero.  Nulo si no hay l&iacute;nea en tal posici&oacute;n
   * </P>
   * @param numero Posici&oacute;n de la l&iacute;nea solicitada.
   */
  public String getLineaEn(int numero)
  {
    String encontrada=null;

    if(lineas!=null && numero>-1 && numero<lineas.length)
    {
        encontrada=lineas[numero];
    } // Fin if lineas

    return encontrada;
  } // Fin m�todo getLineaEn



   /**<code><B><I>setLinea</I></B></code> obtiene la l&iacute;nea del par&aacute;metro recibido del<Br>
   * cual se va a efectuar la consulta, y en consecuencia se desplegar&aacute; los datos de la misma.
   * @param linea Indica el n&uacute;mero de l&iacute;nea del cual se va a efectuar la consulta.<BR>
   */
  public void setLinea(String linea)
  {
    if(linea!=null && lineas!=null)
    {


      for(int i=0;i<lineas.length;i++)
      {
        if(linea.equalsIgnoreCase(lineas[i]))
        {
          posicion=i;
          this.linea=linea;

          if(tramasRespuesta!=null && tramasRespuesta.length>posicion)
          {
            interpretaTramaPC49(tramasRespuesta[posicion]);
          }

          break;
        } //Fin fi linea
      } // Fin for
    } // Fin if linea
  } // Fin m�todo setLinea

  public String getVencido()
  {
    return vencido;
  }

  public String getVigente()
  {
    return vigente;
  }

  public String getNoExigible()
  {
    return noExigible;
  }

  public String getExigible()
  {
    return exigible;
  }

  public String getMoratorios()
  {
    return moratorios;
  }

  public String getJuridicos()
  {
    return juridicos;
  }

  public String getSeguro()
  {
    return seguro;
  }

  public String getOtros()
  {
    return otros;
  }

  public String getTotal()
  {
    return total;
  }

  public String getTramaPC49()
  {
    return tramaPC49;
  }

  public void setTramaPC49(String tramaPC49)
  {
    if(tramaPC49!=null)
      this.tramaPC49=tramaPC49;
  }

  public String getRespuestaPC49()
  {
    return respuestaPC49;
  }

  public void setRespuestaPC49(String respuestaPC49)
  {
    if(respuestaPC49!=null)
      this.respuestaPC49=respuestaPC49;
  }

   /**<code><B><I>getErrores</I></B></code> regresa los errores del vector <I>errores</I> como un<BR>
   * arreglo de cadenas.
   */
  public String[] getErrores()
  {
    String arreglo[]=new String[errores.size()];

    for(int i=0;i<errores.size();i++)
      arreglo[i]=(String)errores.elementAt(i);

    return arreglo;
  } // Fin m�todo getErrores

  /*
  public void setConfiguracion(ServletConfig configuracion)
  {
    if(configuracion!=null)
      this.configuracion=configuracion;
  }
  */


  public void setNoLineas(String noLineas)
  {
    if(noLineas!=null)
    {
      this.noLineas=noLineas;
      arreglaLineas();
    } // Fin if noLineas
  } // Fin setnoLineas


  public String getNoLineas()
  {
    return noLineas;
  }


  public int getPosicion()
  {
    return posicion;
  }


   /**<code><B><I>getPosicion</I></B></code> indica la posici&oacute;n del arreglo de l&iacute;neas en<BR>
   * el cual se encuentra actualmente.
   * <P>Regresa un entero con la posici&oacute;n, o -1 si no existe dicha l&iacute;nea
   * </P>
   * @param cual Cadena que contiene el n&uacute;mero de la l&iacute;nea.
   */
  public int getPosicion(String cual)
  {
    int pos=-1;

    if(lineas!=null && cual!=null)
    {
      for(int i=0;i<lineas.length;i++)
      {
        if(lineas[i].equalsIgnoreCase(cual))
        {
          posicion=i;
          break;
        } // Fin if
      } // Fin for
    } // Fin if linea


    return posicion;
  } // Fin get Posicion


   /**<code><B><I>setPosicion</I></B></code> cambia la posici&oacute;n del arreglo de l&iacute;neas y<BR>
   * de los datos desplegados. Si dicho n&uacute;mero es negativo o mayor que el n&uacute;mero de<BR>
   * l&iacute;neas existentes.
   * @param posicion N&uacute;mero de la posici&oacute;n.
   */
  public void setPosicion(int posicion)
  {
    if(posicion>-1 && lineas!=null && posicion<lineas.length && tramasRespuesta!=null && posicion<tramasRespuesta.length)
    {
      this.posicion=posicion;
      //setLinea(lineas[posicion]);
      linea=lineas[posicion];
      interpretaTramaPC49(tramasRespuesta[posicion]);
    } // Fin if
  } // Fin setPosicion


   /**<code><B><I>setDatos</I></B></code> Asigna los datos obtenidos de la consulta de posici&oacute;n
   * @param datos Es la cadena con los datos, donde cada registro est&aacute; separado por una arroba.
   */
  public void setDatos(String datos)
  {
    int numeroDatos=-1;

    if(datos!=null)
    {
      this.datos=datos;
      fechaGeneracion=new Date();

      StringTokenizer st=new StringTokenizer(datos, "@");
      numeroDatos=st.countTokens();

      if(numeroDatos>0)
      {
        tramasRespuesta=new String[numeroDatos];

        for(int j=0;j<numeroDatos;j++)
          tramasRespuesta[j]=st.nextToken();
      } // Fin if tokens

    } // Fin if datos
  } // Fin m�todo setDatos


  public String getDatos()
  {
    return datos;
  }

  public String[] getTramasRespuesta()
  {
    return tramasRespuesta;
  }

  public String[] getLineas()
  {
    return lineas;
  }


  public int getDireccion()
  {
    return direccion;
  }


  public boolean esCorrecta()
  {
   return esCorrecta;
  }


  public String getMensajeError()
  {
   return mensajeError;
  }


   /**<code><B><I>setDireccion</I></B></code> indica si avanza o retrocede en el arreglo de l&iacute;neas a consultar.
   * @param direccion Si tiene valor -1 retrocede, 1 que avanza; con otros no hace ning&uacute;n cambio.
   */
  public void setDireccion(int direccion)
  {

    if(direccion>=-1 && direccion<=1)
    {
      this.direccion=direccion;
      setPosicion(getPosicion()+this.direccion);
    } // Fin if
  } // Fin set Direccion


   /**<code><B><I>esCaduco</I></B></code>
   *  <P>Regresa verdadero si los datos los ha obtenido en un lapso menor a <B>TIEMPO_LIMITE</B><BR>
   *  </P>
   */
   public boolean esCaduco()
   {
      int comparacion;
      Long l1;
      Long l2;

      if(fechaGeneracion==null)
         return true;

      l1=new Long((new Date()).getTime()-fechaGeneracion.getTime());
      l2=new Long(TIEMPO_LIMITE);


      comparacion=l1.compareTo(l2);



      //l3=l1-l2;

      if(comparacion<0)
      {
         return false;
      }
      else
      {
         return true;
      } // Fin if-else
   } // Fin esCaduco

   private void borrarDatos()
   {
      codigoError="";
      errores.clear();
      esCorrecta=false;
      exigible="";
      noExigible="";
      juridicos="";
      moratorios="";
      seguro="";
      vencido="";
      vigente="";
      otros="";
      total="";
      mensajeError="";

   } // Fin m�todo borrar datos

} // Fin ConsultaPosicionCE

