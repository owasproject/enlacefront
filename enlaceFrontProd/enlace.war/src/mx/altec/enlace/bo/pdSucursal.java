/*
 * pdSucursal.java
 *
 * Created on 25 de abril de 2002, 03:21 PM
 */

package mx.altec.enlace.bo;

/**
 *
 * @author  Rafael Martinez Montiel
 * @version 1.0
 */
public class pdSucursal implements Comparable, java.io.Serializable {

    /** Holds value of property cveSucursal. */
    private String cveSucursal;

    /** Holds value of property nombre. */
    private String nombre;

    /** Holds value of property calleNum. */
    private String calleNum;

    /** Holds value of property colonia. */
    private String colonia;

    /** Holds value of property ciudad. */
    private String ciudad;

    /** Holds value of property estado. */
    private String estado;

    /** Holds value of property delegacion. */
    private String delegacion;

    /** Holds value of property cp. */
    private String cp;

    /** Holds value of property tel. */
    private String tel;

    /** Creates new pdSucursal */
    public pdSucursal() {
    }

    /** Getter for property cveSucursal.
     * @return Value of property cveSucursal.
     */
    public String getCveSucursal() {
        return cveSucursal;
    }

    /** Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre() {
        return nombre;
    }

    /** Getter for property calleNum.
     * @return Value of property calleNum.
     */
    public String getCalleNum() {
        return calleNum;
    }

    /** Getter for property colonia.
     * @return Value of property colonia.
     */
    public String getColonia() {
        return colonia;
    }

    /** Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public String getCiudad() {
        return ciudad;
    }

    /** Getter for property estado.
     * @return Value of property estado.
     */
    public String getEstado() {
        return estado;
    }

    /** Getter for property delegacion.
     * @return Value of property delegacion.
     */
    public String getDelegacion() {
        return delegacion;
    }

    /** Getter for property cp.
     * @return Value of property cp.
     */
    public String getCp() {
        return cp;
    }

    /** Getter for property tel.
     * @return Value of property tel.
     */
    public String getTel() {
        return tel;
    }
    /** obtiene el contenido de una sucursal a partir de una trama
     *@param trama es la trama de la que se sacaran los datos
     *@throws Exception si la trama esta incompleta
     */
    public void fill(java.lang.String trama) throws Exception {
        pdTokenizer tok = new pdTokenizer(trama,"@", 2);
        cveSucursal = tok.nextToken();
        this.nombre = tok.nextToken();
        this.calleNum = tok.nextToken();
        this.colonia = tok.nextToken();
        this.ciudad = tok.nextToken();
        this.estado = tok.nextToken();
        this.delegacion = tok.nextToken();
        this.cp = tok.nextToken();
        this.tel = tok.nextToken();
    }

    /** regresa un String en el formato para exprtar en archivo
     *@return String en el formato del archivo de sucursales
     */
    public String toFileExp(){
        String ret = "";
        char [] space = {' '};
        ret += cveSucursal + this.addPadding(4 - cveSucursal.length());//new String( space, 0, 4).substring( 0, 4 - cveSucursal.length());
        ret += nombre +  this.addPadding(40 - nombre.length());//new String( space, 0, 40).substring( 0, 40 - nombre.length());
        ret += calleNum +  this.addPadding(60 - calleNum.length());//new String( space, 0, 60).substring(0, 60 - calleNum.length());
        ret += colonia +  this.addPadding(40 - colonia.length());//new String( space, 0, 40).substring( 0, 40 - colonia.length());
        ret += delegacion +  this.addPadding(36 - delegacion.length());//new String( space, 0, 36).substring( 0, 36 - delegacion.length());
        ret += ciudad +  this.addPadding(35 - ciudad.length());//new String( space, 0, 35).substring( 0, 35 - ciudad.length());
        ret += estado +  this.addPadding(5 - estado.length());//new String( space, 0, 6).substring( 0, 5 - estado.length());
        ret += cp +  this.addPadding(5 - cp.length());//new String( space, 0, 5).substring( 0, 5 - cp.length());
        ret += tel +  this.addPadding(10 - tel.length());//new String( space, 0, 10).substring( 0, 10 - tel.length());

        return ret;
    }

    /** compara esta sucursal contra otra
     * @param obj es la sucursal contra la cual hay que comparar
     * @return 0 si las sucursales son iguales
     * < 0 si esta sucursal es menor a el parametro
     * > 0 si el parametro es menor a esta sucursal
     */
    public int compareTo(java.lang.Object obj) {
        pdSucursal suc = (pdSucursal) obj;
        int i = 0;
        if ( 0!=( i = this.cveSucursal.compareTo(suc.cveSucursal))) return i;
        else if( 0!=( i = this.nombre.compareTo(suc.nombre)))return i;
        else if( 0!=( i = this.calleNum.compareTo(suc.calleNum)))return i;
        else if( 0!=( i = this.delegacion.compareTo(suc.delegacion)))return i;
        else if( 0!=( i = this.ciudad.compareTo(suc.ciudad)))return i;
        else if( 0!=( i = this.estado.compareTo(suc.estado)))return i;
        else if( 0!=( i = this.cp.compareTo(suc.cp)))return i;

        return 0;
    }

    public String addPadding(long spaces) {
        String ret = "";
        long c = 0;
        while ( c < spaces ) {
            ret = ret + ' ';
            c++;
        }
        return ret;
    }
}

