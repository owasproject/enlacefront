package mx.altec.enlace.bo;

import mx.altec.enlace.utilerias.EIGlobal;

public class CatNominaInterbHTML {

	String codigoHTML = "";
	String codigoT = "";
	/***************************************************************************
	 *** M�todo que construye la tabla de resultados con los datos de los   ****
	 *** empleados. 														****
	 *** Ultima Modificacion: 04 Jun 2008 									****
	 *** Autor:Emmanuel Sanchez Castillo 				 					****
	 *** Modificaci�n:se agregaron los campos tipo cuenta, estado 			****
	 **************************************************************************/
	public String generaEncabezadoBusqueda()
	{
		// EVERIS - CARGA DE DATOS EN TABLAS
		codigoHTML += "<center>";
		codigoHTML += "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		codigoHTML += "<tr>";
		codigoHTML += "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO SOLICITADO APARECEN A CONTINUACI&Oacute;N</th>";
		codigoHTML += "</tr>";
		codigoHTML += "<tr>";

		//codigoHTML += "<th class=\"tittabdat\" align=center></th>";
		/*
		 * 		codigoHTML += "<td class=\"textabdatobs\"><input type=\"checkbox\" name=\"cuentaSelec\" value=\""+nombreRadio+"\"> </td>";   // Num cuenta

		 */
		
		codigoHTML += "<th class=\"tittabdat\"><input type=\"checkbox\" name=\"Todas\" onClick=\"SeleccionaTodos();\"></th>";
		codigoHTML += "<th class=\"tittabdat\" align=center>N&uacute;mero<br>de Cuenta</th>";
		codigoHTML += "<th class=\"tittabdat\" align=center>Nombre</th>";
		codigoHTML += "<th class=\"tittabdat\" align=center>Banco</th>"; //VC ESC
		/*codigoHTML += "<th class=\"tittabdat\" align=center>Estado</th>";   //VC ESC  */
		codigoHTML += "</tr>";

		return codigoHTML;
	}

	public String encabezadoImportaciones(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//EVERIS - ENCABEZADO DE TABLA DE MODIFICACION DE EMPLEADOS POR ARCHIVO
		codigoT += "<br></br>";
		codigoT += "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		codigoT += "<tr>";
		codigoT += "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO</th>";
		codigoT += "</tr>";
		codigoT += "<tr>";
		codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta</th>";
		if(tipoCarga.equals(CatNominaConstantes._MODIF))
		{
			codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th>";
			codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Departamento</th>";
			codigoT += "<th class=\"tittabdat\" align=center>Ingreso mensual</th>";
		}

		return codigoT;
	}

	/***************************************************************************
	 *** M�todo que construye la tabla de resultados con los datos de los   ****
	 *** empleados. 														****
	 *** Ultima Modificacion: 04 Jun 2008 									****
	 *** Autor:Emmanuel Sanchez Castillo 				 					****
	 *** Modificaci�n:se agregaron los campos tipo cuenta, estado 			****
	 **************************************************************************/
	//public String generaTablaEmpleados(String cuentaRS,String numEmplRS,String deptoRS,String apePatRS,String apeMatRS,String nombreRS,String numTarjetaRS,String rfcRS,String ingresoRS, boolean checkOn)
	public String generaTablaEmplInt(String[] datosQuery)
	{
	    EIGlobal.mensajePorTrace("CatNominaHTML - generaTablaEmpleados $ Ingresamos a generaTablaEmpleados",EIGlobal.NivelLog.DEBUG);

		String codigoHTML = "";
		/*System.out.println("CatNominaAction - consultaEmpleados $ ARR[0] HTML = " + datosQuery[0]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[1] = " + datosQuery[1]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[2] = " + datosQuery[2]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[3] = " + datosQuery[3]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[4] = " + datosQuery[4]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[5] = " + datosQuery[5]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[6] = " + datosQuery[6]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[7] = " + datosQuery[7]);
		System.out.println("CatNominaAction - consultaEmpleados $ ARR[8] = " + datosQuery[8]);*/

//		String nombreRadio = datosQuery[8] + datosQuery[0]; //nuevo cambio
		String nombreRadio = datosQuery[1].trim(); //nuevo cambio
		System.out.println("CatNominaAction - generaTablaEmpleados $ nombreRadio = " + nombreRadio); //nuevo cambio

		codigoHTML += "<tr>";

		codigoHTML += "<td class=\"textabdatobs\"><input type=\"checkbox\" name=\"cuentaSelec\" value=\""+nombreRadio+"\"> </td>";   // Num cuenta

		codigoHTML += "<td class=\"textabdatobs\">"+datosQuery[1]+" </td>"; // numCtaExt
		codigoHTML += "<td class=\"textabdatobs\">"+datosQuery[4]+" </td>"; // Nombre
		codigoHTML += "<td class=\"textabdatobs\">" +datosQuery[8]+"</td>";  	// Banco // VC ESC
		/*codigoHTML += "<td class=\"textabdatobs\">" +(CatNominaInterbConstantes._ACT.equals(datosQuery[7]) ? 
				CatNominaInterbConstantes._DESC_ACT: CatNominaInterbConstantes._PEND.equals(datosQuery[7]) ? 
				CatNominaInterbConstantes._DESC_PEND : "" )+"</td>";  	// Estado	   // VC ESC */
		codigoHTML += "</tr>";
/*		codigoHTML += "<INPUT TYPE=\"hidden\" NAME=\"cuentas\">";
		codigoHTML += "<INPUT TYPE=\"hidden\" NAME=\"tot_cuentas\">"; */
		

		return codigoHTML;
	}
public String generaTablaModif(String[] arrDatosModif)
	{
		System.out.println("CatNominaHTML - generaTablaModif$ ");
		System.out.println("CatNominaHTML - generaTablaModif$ Inicio");
		String tablaHTML = "";

	/*	for(int i=0; i<arrDatosModif.length;i++)
		{
			System.out.println("CatNominaHTML - generaTablaModif$ " + arrDatosModif[i]);

		}*/
		tablaHTML += "<tr>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModif[0] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModif[1] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModif[2] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\" align=\"right\">" + arrDatosModif[3] + "</td>";
		tablaHTML += "</tr>";

	//	System.out.println("CatNominaHTML - generaTablaModif$ Tabla con dATOS " + tablaHTML);
		System.out.println("CatNominaHTML - generaTablaModif$ Fin");
		return tablaHTML;
	}

	public String generaTablaBaja(String cuentaAbono)
	{
		System.out.println("");
		System.out.println("CatNominaHTML - generaTablaBaja $ Inicio");
		System.out.println("CatNominaHTML - generaTablaBaja $ Cuenta Abono " + cuentaAbono);
		String tablaHTML = "";

		tablaHTML += "<tr>";
		tablaHTML += "<td class=\"textabdatcla\">" + cuentaAbono + "</td>";
		tablaHTML += "</tr>";

		//System.out.println("CatNominaHTML - generaTablaBaja $ Tabla con dATOS " + tablaHTML);
		System.out.println("CatNominaHTML - generaTablaBaja $ Fin");
		return tablaHTML;
	}

	public String generaFinalTablaModif(int totalReg)
	{
		System.out.println("");
		System.out.println("CatNominaHTML - generaFinalTablaModif$ Inicio");
		String tablaHTML = "";
		tablaHTML += "</table>";
		tablaHTML += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
		tablaHTML += "<tr><td>&nbsp;</td></tr>";
		tablaHTML += "<tr>";
		tablaHTML += "<td class=texfootpagneg align=center bottom=\"middle\">";
		tablaHTML += "Total de registros: " + totalReg + " <br><br>";
		tablaHTML += "</td>";
		tablaHTML += "</tr>";
		tablaHTML += "</table>";
		System.out.println("CatNominaHTML - generaFinalTablaModif$ Fin");
		return tablaHTML;
	}


//******ALTA INTERB*********

	public String encabezadoImporAltaInterb(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//ENCABEZADO DE TABLA DE ALTA DE EMPLEADOS CON CTA. INTERBANCARIA POR ARCHIVO
		codigoT += "<br></br>";
		codigoT += "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		codigoT += "<tr>";
		codigoT += "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO PARA GENERAR EL ALTA</th>";
		codigoT += "</tr>";
		codigoT += "<tr>";
		codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta</th>";
		if(tipoCarga.equals(CatNominaConstantes._ALTA_INTERB))
		{
			codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th>";
			codigoT += "<th class=\"tittabdat\" align=center>Nombre</th>";
			codigoT += "<th class=\"tittabdat\" align=center>Apellido Paterno</th>";
			codigoT += "<th class=\"tittabdat\" align=center>R.F.C del Empleado</th>";
			codigoT += "<th class=\"tittabdat\" align=center>Ingreso mensual</th>";
		}

		return codigoT;
	}

	public String generaTablaAlta(String[] arrDatosAlta)
	{
		System.out.println("CatNominaHTML - generaTablaAlta$ ");
		System.out.println("CatNominaHTML - generaTablaAlta$ Inicio");
		String tablaHTML = "";

	/*	for(int i=0; i<arrDatosAlta.length;i++)
		{
			System.out.println("CatNominaHTML - generaTablaAlta$ " + arrDatosAlta[i]);

		}*/
		tablaHTML += "<tr>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosAlta[0] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosAlta[1] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosAlta[6] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosAlta[4] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosAlta[7] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\" align=\"right\">" + arrDatosAlta[3] + "</td>";
		tablaHTML += "</tr>";

		//System.out.println("CatNominaHTML - generaTablaAlta$ Tabla con dATOS " + tablaHTML);
		System.out.println("CatNominaHTML - generaTablaAlta$ Fin");
		return tablaHTML;
	}



	public String generaTablaFinalAlta(int totalReg)
	{
		System.out.println("");
		System.out.println("CatNominaHTML - generaTablaFinalAlta$ Inicio");
		String tablaHTML = "";
		tablaHTML += "</table>";
		tablaHTML += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
		tablaHTML += "<tr><td>&nbsp;</td></tr>";
		tablaHTML += "<tr>";
		tablaHTML += "<td align=center class=texfootpagneg bottom=\"middle\">";
		tablaHTML += "El total de registros dados de Alta: " + totalReg + " <br><br>";
		tablaHTML += "</td>";
		tablaHTML += "</tr>";
		tablaHTML += "</table>";
		System.out.println("CatNominaHTML - generaTablaFinalAlta$ Fin");
		return tablaHTML;
	}

//*******Modificaci�n Interb*******

	public String encabezadoImporModifInterb(String tipoCarga)
	{
		//CatNominaConstantes cons = new CatNominaConstantes();

		//EVERIS - ENCABEZADO DE TABLA DE MODIFICACION DE EMPLEADOS POR ARCHIVO INTERBANCARIA
		codigoT += "<br></br>";
		codigoT += "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		codigoT += "<tr>";
		codigoT += "<th colspan=\"12\" class=\"tittabdat\">LOS DATOS DEL EMPLEADO A MODIFICAR</th>";
		codigoT += "</tr>";
		codigoT += "<tr>";
		codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Cuenta</th>";
		if(tipoCarga.equals(CatNominaConstantes._MODIF))
		{
			codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Empleado</th>";
			codigoT += "<th class=\"tittabdat\" align=center>N&uacute;mero de Depto</th>";
			//codigoT += "<th class=\"tittabdat\" align=center>Apellido Paterno</th>";
			//codigoT += "<th class=\"tittabdat\" align=center>R.F.C del Empleado</th>";
			codigoT += "<th class=\"tittabdat\" align=center>Ingreso mensual</th>";
		}

		return codigoT;
	}



	public String generaTablaModifInterb(String[] arrDatosModifInterb)
	{
		System.out.println("CatNominaHTML - generaTablaModifInterb$ ");
		System.out.println("CatNominaHTML - generaTablaModifInterb$ Inicio");
		String tablaHTML = "";

		for(int i=0; i<arrDatosModifInterb.length;i++)
		{
			System.out.println("CatNominaHTML - generaTablaModifInterb$ " + arrDatosModifInterb[i]);

		}
		tablaHTML += "<tr>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModifInterb[0] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModifInterb[1] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModifInterb[2] + "</td>";
		tablaHTML += "<td class=\"textabdatcla\" align=\"right\">" + arrDatosModifInterb[3] + "</td>";
		//tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModifInterb[5] + "</td>";
		//tablaHTML += "<td class=\"textabdatcla\">" + arrDatosModifInterb[2] + "</td>";
		tablaHTML += "</tr>";

		System.out.println("CatNominaHTML - generaTablaModifInterb$ Tabla con dATOS " + tablaHTML);
		System.out.println("CatNominaHTML - generaTablaModifInterb$ Fin");
		return tablaHTML;
	}


	public String generaFinalTablaModifInterb(int totalReg)
	{
		System.out.println("");
		System.out.println("CatNominaHTML - generaFinalTablaModifInterb$ Inicio");
		String tablaHTML = "";
		tablaHTML += "</table>";
		tablaHTML += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
		tablaHTML += "<tr><td>&nbsp;</td></tr>";
		tablaHTML += "<tr>";
		tablaHTML += "<td align=center class=texfootpagneg bottom=\"middle\">";
		tablaHTML += "El total de registros a modificar son: " + totalReg + " <br><br>";
		tablaHTML += "</td>";
		tablaHTML += "</tr>";
		tablaHTML += "</table>";
		System.out.println("CatNominaHTML - generaFinalTablaModifInterb$ Fin");
		return tablaHTML;
	}



}