/*
 * pdMes.java
 *
 * Created on 5 de abril de 2002, 01:37 PM
 */

package mx.altec.enlace.bo;

import java.util.ArrayList;

/**
 *
 * @author  Horacio Oswaldo Ferro D�az
 * @version
 */
public class pdMes implements java.io.Serializable {

    /** Mes del a�o */
    protected String Mes;

    /** A�o */
    protected int Anio;

    /** Lista de dias */
    protected ArrayList Dias;

    /** Creates new pdMes */
    public pdMes () {
        Dias = new ArrayList ();
    }

    /**
     * Metodo para crear un nuevo objeto pdMes
     * @param mes Nombre del mes actual
     * @param anio A�o
     * @param dias Lista con los dias del mes
     */
    public pdMes (String mes, int anio, ArrayList dias) {
        Mes = mes;
        Anio = anio;
        Dias = dias;
    }

    /**
     * Metodo para crear un nuevo objeto pdMes
     * @param mes Numero del mes
     * @param anio A�o
     * @param dias Lista con los dias del mes
     */
    public pdMes (int mes, int anio, ArrayList dias) {
        Mes = getMes (mes);
        Anio = anio;
        Dias = dias;
    }

    /**
     * Metodo para establecer el mes
     * @param mes Nombre del mes
     */
    public void setMes (String mes) {Mes = mes;}

    /**
     * Metodo para establecer el mes
     * @param mes Numero del mes
     */
    public void setMes (int mes) {Mes = getMes (mes);}

    /**
     * Metodo para establecer el a�o
     * @param anio A�o actual
     */
    public void setAnio (int anio) {Anio = anio;}

    /**
     * Metodo para establecer los dias
     * @param dias
     */
    public void setDias (ArrayList dias) {Dias = dias;}

    /**
     * Metodo para agregar un dia
     * @param dia Dia a agregar
     */
    public void agregaDia (pdDiaHabil dia) {Dias.add (dia);}

    /**
     * Metodo para obtener el mes
     * @return Mes en un string
     */
    public String getMes () {return Mes;}

    /**
     * Metodo para obtener el a�o
     * @return A�o
     */
    public int getAnio () {return Anio;}

    /**
     * Metodo para obtener los dias del mes
     * @return Lista con los dias del mes
     */
    public ArrayList getDias () {return Dias;}

    /**
     * Metodo para obtener el nombre del mes
     * @param mes Numero del mes
     * @return String con el nombre del mes
     */
    protected String getMes (int mes) {
        String nom = "";
        switch (mes) {
            case 0: {
                nom = "Enero";
                break;
            }
            case 1: {
                nom = "Febrero";
                break;
            }
            case 2: {
                nom = "Marzo";
                break;
            }
            case 3: {
                nom = "Abril";
                break;
            }
            case 4: {
                nom = "Mayo";
                break;
            }
            case 5: {
                nom = "Junio";
                break;
            }
            case 6: {
                nom = "Julio";
                break;
            }
            case 7: {
                nom = "Agosto";
                break;
            }
            case 8: {
                nom = "Septiembre";
                break;
            }
            case 9: {
                nom = "Octubre";
                break;
            }
            case 10: {
                nom = "Noviembre";
                break;
            }
            case 11: {
                nom = "Diciembre";
                break;
            }
        }
        return nom;
    }

}
