package mx.altec.enlace.bo;

import java.io.*;

import mx.altec.enlace.utilerias.EIGlobal;

public class MDI_Importar
 {
	/**
	 * Almacena la cuenta cargo
	 */
   public String cuentaCargo="";
   /**
    * Almacena la cuenta abono
    */
   public String cuentaAbono="";
   /**
    * Almacena el concepto
    */
   public String concepto = "";

   /**
    * Almacena el titular
    */
   public String titular="";

   /**
    * Almacena la plaza
    */
   public String plaza="";

   /**
    * Almacena el banco
    */
   public String banco="";

   /**
    * Almacena el beneficiario
    */
   public String beneficiario="";

   /**
    * Almacena el comprobante fiscal
    */
   public String comproFiscal="";

   /**
    * Almacena el RFC
    */
   public String RFC="";

   /**
    * Almacena la referencia
    */
   public String referencia="";
   public String formaAplica="1";
   public String strImporteIVA="";

   public double importeIVA=0;
   public double importe=0;

   public int longitudLinea=0;
   public String sucursal="";
   public int err = 0;
   public boolean nuevoFormato=false;
   public boolean indicadorFormaAplica=false;
   public boolean conReferenciaInter=false;
   /** Variable para almacenar valor de tipo de pago ctas dolares*/
   public transient String tipoPago = "";
   /** Variable bandera para operaciones en Dolares*/
   public transient boolean indicadorOperDolares = false;

  public void formateaLinea(String linea)
   {
	 indicadorFormaAplica=false;
	 conReferenciaInter=false;

     try
      {

		switch(linea.length())
		 {
			   case 274:	//nuevo con RFC + 1 caracter forma aplicacion (opcional) + 2 caracteres de Tipo de pago
							conReferenciaInter=true;
							longitudLinea=274;
							log("MDI_Importar - MDI_Importar(): Nuevo layout con rfc", EIGlobal.NivelLog.DEBUG);
							break;
				case 242:case 243:	//nuevo sin RFC + 1 caracter forma aplicacion (opcional)
							conReferenciaInter=true;
							longitudLinea=242;
							log("MDI_Importar - MDI_Importar(): Nuevo layout sin rfc", EIGlobal.NivelLog.DEBUG);
							break;
				case 254:   // viejo con RFC
				            longitudLinea=254;
				 			log("MDI_Importar - MDI_Importar(): Viejo layout con rfc", EIGlobal.NivelLog.DEBUG);
				 			break;
				case 225:   // viejo sin RFC
				            longitudLinea=225;
				            log("MDI_Importar - MDI_Importar(): Viejo layout sin rfc", EIGlobal.NivelLog.DEBUG);
				            break;

		 }

		log("MDI_Importar - MDI_Importar(): Formato RFC: " + nuevoFormato, EIGlobal.NivelLog.DEBUG);
		log("MDI_Importar - MDI_Importar(): Longitud de linea "+longitudLinea, EIGlobal.NivelLog.DEBUG);
		log("MDI_Importar - MDI_Importar(): Longitud de registro en archivo ["+linea.length() + "]", EIGlobal.NivelLog.DEBUG);
		log("MDI_Importar - MDI_Importar(): imprime linea: [" + linea + "]", EIGlobal.NivelLog.DEBUG);
		if(conReferenciaInter)
		  {
			 if(linea.length()==(longitudLinea+1))
			  {
				log("MDI_Importar - MDI_Importar(): Registro contiene indicador", EIGlobal.NivelLog.DEBUG);
				formaAplica=linea.substring(linea.length()-1);
				linea=linea.substring(0,linea.length()-1);
				indicadorFormaAplica=true;
			  }else if (linea.length()== 274){ // Tamanhio del nuevo layout operArchivo Dolares
			      formaAplica=linea.substring(linea.length()-3, linea.length()-2);
			      log("MDI_Importar - MDI_Importar(): formaAplica |" +  formaAplica + "|", EIGlobal.NivelLog.DEBUG);
			      if(formaAplica.isEmpty()){
			          formaAplica = "1";
			          log("MDI_Importar - MDI_Importar(): formaAplica " +  formaAplica, EIGlobal.NivelLog.DEBUG);
			      }
	              tipoPago = linea.substring(linea.length()-2);
	              indicadorFormaAplica=true;
			  }
			 else{
			  log("MDI_Importar - MDI_Importar(): Registro no contiene indicador", EIGlobal.NivelLog.DEBUG);
			 }
		  }
		log("MDI_Importar - MDI_Importar(): tama�o linea" + linea.length(), EIGlobal.NivelLog.DEBUG);
		log("MDI_Importar - MDI_Importar(): imprime linea" + linea, EIGlobal.NivelLog.DEBUG);
		// Verificar longitud de linea
		if(
		    (nuevoFormato && conReferenciaInter && linea.length()!=274) ||   /*formatoConReferencia con RFC*/
		    (!nuevoFormato && conReferenciaInter && linea.length()!=242) ||  /*formatoConReferencia sin RFC*/
		    (nuevoFormato && !conReferenciaInter && linea.length()!=254)  || /*formatoSinReferencia con RFC*/
		    (!nuevoFormato && !conReferenciaInter && linea.length()!=225)    /*formatoSinReferencia sin RFC*/
		  )
		  {
		     log("MDI_Importar - MDI_Importar(): Registro no cumple con el formato", EIGlobal.NivelLog.ERROR);
		     err=linea.length()+1;
		     return;
		  }

        if(linea.length()>=109 && linea.length()<=longitudLinea)
         {
           cuentaCargo    = linea.substring(0,16).trim();
           cuentaAbono    = linea.substring(16,36).trim();
           banco          = linea.substring(36,41).trim();
           beneficiario   = linea.substring(41,81).trim();
           log("MDI_Importar - MDI_Importar(): Antes de obtener el registro de la sucursal", EIGlobal.NivelLog.ERROR);
           if( null == linea.substring(81,85).trim() || ("").equals(linea.substring(81,85).trim()) || ("0000").equals(linea.substring(81,85).trim())){
        	   sucursal = "0001";
        	   log("MDI_Importar - MDI_Importar(): Se coloca la sucursal por default 0001", EIGlobal.NivelLog.ERROR);
           }else{
        	   sucursal = linea.substring(81,85).trim();
        	   log("MDI_Importar - MDI_Importar(): Se coloca la sucursal que se informa en el archivo " + sucursal, EIGlobal.NivelLog.ERROR);
           }
		   log("MDI_Importar - MDI_Importar(): Despues de obtener el registro de la sucursal", EIGlobal.NivelLog.ERROR);
		   log("MDI_Importar - MDI_Importar(): Revisando Importe", EIGlobal.NivelLog.DEBUG);
		   String idimp   = formateaImp(linea.substring(85,100).trim());
		   importe        = new Double(idimp).doubleValue();
           plaza          = linea.substring(100,105);
           if( null == linea.substring(100,105).trim() || ("").equals(linea.substring(100,105).trim()) || ("00000").equals(linea.substring(100,105).trim())){
        	   plaza = "01001";
        	   log("MDI_Importar - MDI_Importar(): Se coloca la plaza por default 01001", EIGlobal.NivelLog.ERROR);
           }else{
        	   plaza = linea.substring(100,105);
        	   log("MDI_Importar - MDI_Importar(): Se coloca la plaza que se informa en el archivo " + plaza, EIGlobal.NivelLog.ERROR);
           }
		   if(nuevoFormato)
			 {
			   if(conReferenciaInter)
				 {
					log("MDI_Importar - MDI_Importar(): CON REF INTER", EIGlobal.NivelLog.DEBUG);
					comproFiscal=linea.substring(235,236).trim();
					log("MDI_Importar - MDI_Importar(): comproFiscal " + comproFiscal, EIGlobal.NivelLog.DEBUG);
					RFC         =linea.substring(236,249).trim();
					//String idiva=formateaImp(linea.substring(249,264).trim());
					strImporteIVA = linea.substring(249,264).trim();
					if(indicadorOperDolares && strImporteIVA.isEmpty()){
					    strImporteIVA = "000000000000.00";
					}
					importeIVA=new Double(strImporteIVA).doubleValue();
                 }
			   else
				 {
				    log("MDI_Importar - MDI_Importar(): SIN REF INTER", EIGlobal.NivelLog.DEBUG);
					comproFiscal=linea.substring(225,226).trim();
					log("MDI_Importar - MDI_Importar(): comproFiscal " + comproFiscal, EIGlobal.NivelLog.DEBUG);
					RFC         =linea.substring(226,239).trim();
					//String idiva=formateaImp(linea.substring(239,254).trim());
					strImporteIVA = linea.substring(239,254).trim();
					importeIVA=new Double(strImporteIVA).doubleValue();
				 }
			  }

			log("MDI_Importar - MDI_Importar(): Buscando concepto.", EIGlobal.NivelLog.DEBUG);
			concepto = linea.substring(105,145);
			try
			{
			  if(conReferenciaInter && nuevoFormato)
			    referencia = linea.substring(264, 271).trim();
			  else
			  if(conReferenciaInter && !nuevoFormato)
			    referencia = linea.substring(235).trim();
			  log("MDI_Importar - MDI_Importar(): dato ref: " + referencia, EIGlobal.NivelLog.DEBUG);
			}
			catch(StringIndexOutOfBoundsException e)
			{
			  log("MDI_Importar - MDI_Importar(): Archivo con layout anterior - sin referencia", EIGlobal.NivelLog.DEBUG);
			  referencia = " ";
			}
           err = 0;

           log("MDI_Importar - MDI_Importar(): cta cargo: "+cuentaCargo, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): cta abono: "+cuentaAbono, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): concepto: "+concepto.trim(), EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): titular: "+titular, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): plaza: "+plaza, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): importe: "+importe, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): sucursal: "+sucursal, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): banco: "+banco, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): benef: "+beneficiario, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): rfc: "+RFC, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): iva: "+importeIVA, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): referencia interbancaria: "+referencia, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): forma aplicacion: "+formaAplica, EIGlobal.NivelLog.DEBUG);
           log("MDI_Importar - MDI_Importar(): Tipo de pago: "+ tipoPago, EIGlobal.NivelLog.DEBUG);
         }
		else
		 {
		 	err=linea.length()+1;
		 	log("MDI_Importar - MDI_Importar(): El registro no cumple con el formato", EIGlobal.NivelLog.DEBUG);
		 }

       } catch(Exception e)
         {
		   log("MDI_Importar - MDI_Importar(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.ERROR);
           err = linea.length()+1;
         }
   }

   public boolean verificaCuentaCargo(String[][] arrayCuentas)
    {
      for(int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
       {
         if(arrayCuentas[indice][1].trim().equals(cuentaCargo))
           {
             titular=arrayCuentas[indice][4];
             return true;
           }
       }
      titular="CUENTA NO TIENE";
      return false;
    }

   public boolean verificaCuentaAbono(String[][] arrayCuentas,int total)
    {
      for(int id=0;id<total;id++)
       {
         if(arrayCuentas[id][0].trim().equals(cuentaAbono))
           return true;
       }
      return false;
    }

   public boolean verificaSucursal()
    {
	   try{
	      if(Integer.parseInt(sucursal)<0){
	       return false;
	      }else{
	      return true;
	      }
	   }catch(NumberFormatException e){
    	  return false;
      }
    }

   public boolean verificaImporte()
    {
      if(importe<=0.0)
        return false;
      return true;
    }

   public String formateaImp(String imp)
	{
	   String formateado="";

	   if(imp.length()<4)
		 imp="0000"+imp;

	   if(imp.indexOf(".")>=0)
		 return imp;

	   formateado+=imp.substring(0,imp.length()-2);
	   formateado+=".";
	   formateado+=imp.substring(imp.length()-2,imp.length());
	   return formateado;
	}


   public boolean verificaConcepto() {
	   char tst;
	   for(int i = 0; i < concepto.length(); i++) {
		   tst = concepto.charAt(i);
		   if(tst < ' ' || (tst > ' ' && tst < '0') || (tst > '9' && tst < 'A') || (tst > 'Z' && tst < 'a') || tst > 'z') {
			   return false;
		   }
	   }
	   return true;
   }

   public boolean verificaReferencia() {
	   char tst;
	   for(int i = 0; i < referencia.length(); i++) {
		   tst = referencia.charAt(i);
		   if(tst < '0' || tst > '9') {
			   return false;
		   }
	   }
	   return true;

   }

  public boolean verificaFormaAplica()
   {

      if(indicadorOperDolares && "1".equals(formaAplica.trim())){
         return true;
      }
	  if(!indicadorOperDolares && (formaAplica.trim().equals("1") ||
		  formaAplica.trim().equals("2")) )
		 return true;
	  return false;
   }

  public String getFormaAplica()
   {
	  return (formaAplica.trim().equals("1"))?"H":"2";
   }

 public String getDescAplica()
   {
	  return (formaAplica.trim().equals("1"))?"Mismo d&iacute;a":"D&iacute;a Siguiente";
   }

 public void log(String t,EIGlobal.NivelLog a)
   {
 	  EIGlobal.mensajePorTrace(t,a);
   }


 	/**
	 * GAE
	 * Metodo encargado de validar que la cantidad del Iva contenga punto
	 * decimal
	 *
	 * @return		true  - Si contiene punto
	 * 				false - Si no contiene punto
	 */
	public boolean existePuntoImporteIva(){
		int indPunto = 0;
		String importeIvaTmp = null;
		try{
			importeIvaTmp = "" + strImporteIVA;
			System.out.println("MDI_Importar::existePuntoImporteIva:: "
					+ "ImporteIvaTmp= " + importeIvaTmp);
			indPunto = importeIvaTmp.indexOf(".");
			System.out.println("MDI_Importar::existePuntoImporteIva:: "
					+ "Posici�n Punto decimal= " + indPunto);
			if(indPunto>-1)
				return true;
			else
				return false;

		}catch(Exception e){
			System.out.println("MDI_Importar::existePuntoImporteIva:: Error->"
					+ e.getMessage());
		}
		return false;
	}


	/**
	 * GAE
	 * Metodo encargado de validar el formato del importe de Iva
	 *
	 * @return		true  - Si es correcto
	 * 				false - Si no es correcto
	 */
	public boolean validaFormatoImporteIva(){
		int indPunto = 0;
		String importeIvaTmp = null;
		try{
			importeIvaTmp = "" + strImporteIVA;

	         if(indicadorOperDolares && strImporteIVA.isEmpty()){
	                return true;
	            }
			System.out.println("MDI_Importar::validaFormatoImporteIva:: "
					+ "ImporteIvaTmp= " + importeIvaTmp);
			indPunto = importeIvaTmp.indexOf(".");
			System.out.println("MDI_Importar::validaFormatoImporteIva:: "
					+ "Posici�n Punto decimal= " + indPunto);
			if(indPunto>12)
				return false;
			return true;
		}catch(Exception e){
			System.out.println("MDI_Importar::validaFormatoImporteIva:: Error->"
					+ e.getMessage());
		}
		return false;
	}

	/**
	 * Valida que el beneficiario sea alfanumerico
	 *
	 * @return			true  - Si el beneficiario es correcto
	 * 					false - Si el beneficiario es incorrecto
	 */
	public boolean beneficiarioValido(){
	    boolean resultado = true;
	    String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789& ";
	    int a, total = beneficiario.length();
	    for(a=0;a<total && resultado;a++) if(patron.indexOf(beneficiario.substring(a,a+1))==-1) resultado = false;
	    return resultado;
    }

    /**
     * Valida que el tipo de pago sea correcto
     *
     * @return          true  - Si el beneficiario es correcto
     *                  false - Si el beneficiario es incorrecto
     */
    public boolean verificaTipoPago(){
        EIGlobal.mensajePorTrace(" Inicia verificaTipoPago ", EIGlobal.NivelLog.DEBUG);
        String tipoValido = "0[1-8]";
        boolean result = tipoPago.matches(tipoValido);
        if(result){
            String valor = TipoPagoCod.obtenerCodTipoPag(Integer.valueOf(tipoPago)).toString();
            tipoPago = valor;
            EIGlobal.mensajePorTrace(" El codigo de TipoPago es= " + tipoPago, EIGlobal.NivelLog.DEBUG);
        }

        return result;
    }


    /**
	 * Funcion para validar si el texto es un RFC valido
	 * @return true si es valido
	 */
	public boolean validaRFC(){
		EIGlobal.mensajePorTrace("INICIA - validaRFC", EIGlobal.NivelLog.DEBUG);

		boolean isValid = true;

		if(null == RFC ){
			EIGlobal.mensajePorTrace("validaRFC: El campo RFC debe venir informado", EIGlobal.NivelLog.ERROR);
			return false;
		}

		RFC = RFC.toUpperCase();

		switch(RFC.length()){
		case 13:
		case 10:
		case 12:
		case 9:
			isValid = true;
			break;
		default:
			EIGlobal.mensajePorTrace("validaRFC: El campo RFC tiene longitud invalida", EIGlobal.NivelLog.ERROR);
			isValid = false;
			break;
		}


		if (isValid) {
			isValid = (RFC.length() == 13 || RFC.length() == 10)
					? validaRFCPersonasFisicas() : validaRFCPersonasMorales();
		}

		EIGlobal.mensajePorTrace("TERMINA - validaRFC: " + isValid, EIGlobal.NivelLog.DEBUG);
	 	return isValid;
	}

	/**
	 * Valida que un texto cuente con una expresion regular
	 * @param texto       el texto a validar
	 * @param expRegular  la expresion regular a utilizar
	 * @return true si es valido
	 */
	private boolean validaTextoConExpReg(String texto, String expRegular){

		if(null == texto || null == expRegular){
			return false;
		}

		return texto.matches(expRegular);
	}

	/**
	 * Valida que el mes y el dia vengan informados correctamente
	 * @param fecNac el texto en formato yymmdd
	 * @return resultado de la validacion
	 */
	private boolean validaFecha(String fecNac){
		boolean respuesta = true;

		int mes=Integer.parseInt(fecNac.substring(2,4));
	    int dia=Integer.parseInt(fecNac.substring(4,6));


	    if((dia >0 && dia<=31) && (mes >0 && mes<=12) )
         {
           if(mes==2 && dia >=30){
        	   respuesta = false;
           }
         }
       else{
    	   respuesta = false;
       }
	    return respuesta;
	}

	/**
	 * Realiza la validacion del RFC para personas fisicas
	 * @return el resultado de la validacion
	 */
	private boolean validaRFCPersonasFisicas(){
		boolean isValid = true;

		//Valida nombre RFC
		if(!validaTextoConExpReg(RFC.substring(0,4),"[A-Z\u00D1]+")){
			EIGlobal.mensajePorTrace("validaRFC: Las inciales del nombre en el RFC deben de ser Letras", EIGlobal.NivelLog.ERROR);
			isValid = false;
		}

	    if(isValid && !validaTextoConExpReg(RFC.substring(4,10), "[0-9]+")){
			EIGlobal.mensajePorTrace("validaRFC: La fecha de nacimiento del RFC debe de ser num\u00E9rica", EIGlobal.NivelLog.ERROR);
			isValid = false;
	 	}

	    if(isValid && !validaFecha(RFC.substring(4,10))){
	    	EIGlobal.mensajePorTrace("validaRFC: La fecha no contiene formato correcto", EIGlobal.NivelLog.ERROR);
	    	isValid = false;
	    }

		//Valida Homoclave RFC
		if(isValid && RFC.length() == 13 && !"".equals(RFC.substring(10,13).trim()) &&
				!validaTextoConExpReg(RFC.substring(10,13), "[A-Z\u00D10-9]+")){
			EIGlobal.mensajePorTrace("validaRFC: La homoclave del RFC debe de ser alfanum\u00E9rica", EIGlobal.NivelLog.ERROR);
			isValid = false;
	 	}

		return isValid;
	}

	/**
	 * Realiza la validacion del RFC para personas morales
	 * @return resultado de la validacion
	 */
	private boolean validaRFCPersonasMorales(){
		boolean isValid = true;

		//Valida nombre RFC
		if(!validaTextoConExpReg(RFC.substring(0,3),"[A-Z\u00D1]+")){
			EIGlobal.mensajePorTrace("validaRFC: Las inciales del nombre en el RFC deben de ser Letras", EIGlobal.NivelLog.ERROR);
			isValid = false;
		}
		//Valida Fecha de nacimiento RFC
		if(isValid && !validaTextoConExpReg(RFC.substring(3,9), "[0-9]+")){
			EIGlobal.mensajePorTrace("validaRFC: La fecha de nacimiento del RFC debe de ser num\u00E9rica", EIGlobal.NivelLog.ERROR);
			isValid = false;
	 	}

		if(isValid && !validaFecha(RFC.substring(3,9))){
	    	EIGlobal.mensajePorTrace("validaRFC: La fecha no contiene formato correcto", EIGlobal.NivelLog.ERROR);
	    	isValid = false;
	    }

		//Valida Homoclave RFC
		if(isValid && RFC.length() == 12 && !"".equals(RFC.substring(9,12).trim()) &&
				!validaTextoConExpReg(RFC.substring(9,12), "[A-Z\u00D10-9]+")){
			EIGlobal.mensajePorTrace("validaRFC: La homoclave del RFC debe de ser alfanum\u00E9rica", EIGlobal.NivelLog.ERROR);
			isValid = false;
	 	}
	return isValid;
	}
	/**
	* Declaraci?n del enum compleo el mismo no puede ser declarado como
	* privado(private) o protegido (protected)
	*
	*/
	enum TipoPagoCod {
	    /** Liquidaci?n de operaci?n con valores*/
	    TTLIQVAL(1),
	    /** Liquidaci?n de operaci?n derivada*/
	    TTLIQDER(2),
	    /** Liquidaci?n de operaci?n cambiaria*/
	    TTLIQCAM(3),
	    /** Pago de servicios*/
	    TTPAGSER(4),
	    /** Pago de bienes*/
	    TTPAGBIE(5),
	    /** Otorgamiento de pr?stamo*/
	    TTOTOPRE(6),
	    /** Pago de pr?stamo*/
	    TTPAGPRE(7),
	    /** Otros*/
	    TTOTROS(8);
		/**
		* Declaracion de la instancia de variable a usar por cada valor del enum
		* **/
		private int idTipoPago;

	/**
	 * Establece el orden del valor del enum
	 * @param idTipoPago el orden del valor del enum
	 */
	private TipoPagoCod(int idTipoPago){
	this.idTipoPago = idTipoPago;
	}
	/**
	* M?todo que recupera el orden del valor del enum
	* @return int.
	* **/
	public int getIdTipoPago(){
	return idTipoPago;
	}

	/**
	 * Obtener Enum por id.
	 *
	 * @param id identificador de tipo de pago a buscar
	 * @return Enum encontrado
	 */
	public static TipoPagoCod obtenerCodTipoPag(int id){
	    for (TipoPagoCod dato : TipoPagoCod.values()) {
	        if (dato.idTipoPago == id) {
	            return dato;
	        }
	    }
	    return null;
	}

	}//Fin Enum

}