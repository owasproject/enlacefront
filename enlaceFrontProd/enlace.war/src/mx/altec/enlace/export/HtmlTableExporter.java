package mx.altec.enlace.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.IllegalArgumentException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.export.exception.ExportarException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;


/**
 * Exporta a los formatos TXT y CSV.
 *
 * @author Stefanini
 *
 */
public final class HtmlTableExporter {

	/**
	 * Constante para indicar que se va a enviar a archivo de texto.
	 */
	public static final String TIPO_EXPORT_TXT = "TXT";

	/** Variable de apoyo para log. */
	private static final Logger LOG = Logger.getLogger(HtmlTableExporter.class);

    /**
     * Mime type para texto plano.
     */
    private static final String CONTENT_TYPE_TXT = "text/plain";

    /**
     * Mime type para CSV.
     */
    private static final String CONTENT_TYPE_CSV = "application/vnd.ms-excel";

    /**
     * Extension para archivo de texto plano.
     */
    private static final String FILE_EXTENSION_TXT = "txt";

    /**
     * Extension para archivo csv.
     */
    private static final String FILE_EXTENSION_CSV = "csv";

    /**
     * Constructor privado.
     */
    private HtmlTableExporter() {}

    /**
     * Exporta los datos de la lista de beans en el tipo de archivo indicado,
     * la exportaci贸n es como un adjunto de forma que se descargue del navegador.
     *
     * @param tipo el tipo de archivo TXT o CSV
     * @param em el modelo de exportacion
     * @param beans lista de beans a ser exportados
     * @param response el response de la servlet
     *
     * @throws IOException si falla la exportaci&oacute;n
     * @throws ExportarException si falla la exportaci&oacute;n
     */
    public static void export(String tipo, ExportModel em, List<?> beans, HttpServletResponse response) throws IOException, ExportarException {

    	//LOG.debug("Ingresa a export bean, tipo: " + tipo);

    	if (beans == null) {
    		//LOG.error("La lista de beans a exportar es nula");
    		throw new ExportarException("La lista de beans a exportar es nula");
    	}

    	//LOG.debug("beans.size(): " + beans.size());

    	String fileExtension = null;

        Exportable exporter = null;

        if (TIPO_EXPORT_TXT.equalsIgnoreCase(tipo)) {
        	exporter = new TextExporter(em, response.getOutputStream());
        	response.setContentType(CONTENT_TYPE_TXT);
        	fileExtension = FILE_EXTENSION_TXT;
        } else {
        	exporter = new CSVExporter(em, response.getOutputStream());
        	response.setContentType(CONTENT_TYPE_CSV);
        	fileExtension = FILE_EXTENSION_CSV;
        }

        response.setHeader("Content-disposition", "attachment; filename=" + em.getFileName() + "." + fileExtension);

        if (em.isOcultarTitulos()) {
        	for (Column c : em.getColumns()) {
        		exporter.writeColumn(c, c.getTitle());
        	}
        }

        exporter.nextRow();

       // LOG.debug("Exportando lista de beans.");

        for (Object bean : beans) {

            for (Column col : em.getColumns()) {
                try {
                    Object ret = PropertyUtils.getSimpleProperty(bean, col.getFieldName());
                    Format fmtr = getFormatter(col, ret);

                    if (fmtr != null) {

                    	if (isNumber(ret)) {
                    		ret = new BigDecimal(ret.toString().trim().replaceAll(",", ""));
                    		ret = recorrerPuntoDecimal((BigDecimal)ret, col);
                    	}

                        if (tieneValor(ret)) {
                            ret = fmtr.format(ret);
                        }
                    }
                    exporter.writeColumn(col, ret);
                } catch (IllegalAccessException e) {
                	exporter.writeColumn(col, null);
                } catch (IllegalArgumentException e) {
                	exporter.writeColumn(col, null);
                } catch (InvocationTargetException e) {
                	exporter.writeColumn(col, null);
                } catch (NoSuchMethodException e) {
                	exporter.writeColumn(col, null);
                } catch (IOException e) {
                	//LOG.debug("Error al escribir al servlet de salida: " + e.getMessage());
                }
            }

            exporter.nextRow();
        }
        exporter.close();

       // LOG.debug("Termina proceso de exportar lista de beans.");
    }

	/**
	 * Recorrer punto decimal.
	 *
	 * @param numero El objeto: numero
	 * @param col El objeto: col
	 * @return Objeto big decimal
	 */
	private static BigDecimal recorrerPuntoDecimal(BigDecimal numero, Column col) {
		int posiciones = col.getPosPuntoDec();
		int divisor = 1;
		if (posiciones==0) {
			return numero;
		}
		for (int i = 0; i < posiciones; i++) {
			divisor *= 10;
		}
		return numero.divide(new BigDecimal(divisor));
	}

	/**
	 * Gets the formatter.
	 *
	 * @param col Columna con meta informaci&oacute;n
	 * @param ret Objeto de apoyo al formateo del valor
	 * @return Instancia del formateador
	 */
	private static Format getFormatter(Column col, Object ret) {
        if (col.getFormat() != null && col.getFormatter() == null) {

            if (ret instanceof Date) {
                col.setFormatter(new SimpleDateFormat(col.getFormat(), new Locale("es","MX")));
            } else if (ret instanceof Number) {
                col.setFormatter(new DecimalFormat(col.getFormat()));
            } else if (isNumber(ret)) {
        		col.setFormatter(new DecimalFormat(col.getFormat()));
        	}
        }
        return col.getFormatter();
	}

	/**
     * Exporta los datos del archivo en el tipo de archivo indicado,
     * la exportaci&uacute;n es como un adjunto de forma que se descargue del navegador.
     *
     * @param tipo el tipo de archivo TXT o CSV
     * @param em el modelo de exportacion
     * @param filePath ruta absoluta del archivo que contiene la informacion a exportar
     * @param response el response de la servlet
     *
     * @throws IOException si falla la exportaci贸n
     */
    public static void export(String tipo, ExportModel em, String filePath, HttpServletResponse response) throws IOException {
    	if ("txt".equals(tipo) && em.isArchivoExistente()) {
    		copyFile(null, filePath, response);
    	} else {
    		if (em.isUseColumnSeparatorFile()) {
    			exportSeparatorBased(tipo, em, filePath, response);
    		} else {
    			exportPositionBase(tipo, em, filePath, response);
    		}
        }
    }
	/**
     * Exporta los datos del archivo en el tipo de archivo indicado,
     * la exportaci贸n es como un adjunto de forma que se descargue del navegador.
     *
     * @param tipo el tipo de archivo TXT o CSV
     * @param em el modelo de exportacion
     * @param filePath ruta absoluta del archivo que contiene la informacion a exportar
     * @param response el response de la servlet
     *
     * @throws IOException si falla la exportaci贸n
     */
    public static void exportPositionBase(String tipo, ExportModel em, String filePath, HttpServletResponse response) throws IOException {

    	//LOG.debug("Ingresa a export por posici髇, tipo: " + tipo);

        String fileExtension = null;

        Exportable exporter = null;
        BufferedReader br = null;

        try {


            if (TIPO_EXPORT_TXT.equalsIgnoreCase(tipo)) {
                exporter = new TextExporter(em, response.getOutputStream());
                response.setContentType(CONTENT_TYPE_TXT);
                fileExtension = FILE_EXTENSION_TXT;
            } else {
                exporter = new CSVExporter(em, response.getOutputStream());
                response.setContentType(CONTENT_TYPE_CSV);
                fileExtension = FILE_EXTENSION_CSV;
            }

            response.setHeader("Content-disposition", "attachment; filename=" + em.getFileName() + "." + fileExtension);

            if (em.isOcultarTitulos()) {
            	for (Column c : em.getColumns()) {
                    exporter.writeColumn(c, c.getTitle());
                }
            }

            exporter.nextRow();

            br = new BufferedReader(new FileReader(filePath));

            String line = null;

            while ( (line = br.readLine()) != null ) {

                for (Column col : em.getColumns()) {
                    try {
                        Object ret = readColumn(col, line);

                        Format fmtr = getFormatter(col, ret);

                        if (fmtr != null) {


                        	if (isNumber(ret)) {
                        		ret = new BigDecimal(ret.toString().trim().replaceAll(",", ""));
                        		ret = recorrerPuntoDecimal((BigDecimal)ret, col);
                        		ret = fmtr.format(ret);
                        	}


                        }
                        exporter.writeColumn(col, ret);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        exporter.writeColumn(col, null);
                    } catch (IOException e) {
                    	//LOG.debug("Error al escribir al servlet de salida: " + e.getMessage());
                    }
                }

                exporter.nextRow();
            }
        } finally {

            if (exporter != null) {
                exporter.close();
            }
            if (br != null) {
                br.close();
            }
        }

        //LOG.debug("Termina proceso de exportar por posici髇.");
    }
    /**
     * Descarga el archivo base con extension TXT.
     * @since 16/04/2015
     *
     * @param em modelo de exportacion.
     * @param filePath ruta del archivo a descargar.
     * @param response objeto response del servlet.
     * @throws IOException error en exportacion.
     */
    public static void copyFile(ExportModel em, String filePath, HttpServletResponse response) throws IOException {
    	 File downloadFile = new File(filePath);
         FileInputStream inStream = new FileInputStream(downloadFile);
         response.setContentType("text/plain");
         response.setHeader("Content-disposition", "attachment; filename=" + downloadFile.getName().replaceAll("[.]{1}.*", ".txt"));

         OutputStream outStream = response.getOutputStream();
         byte[] buffer = new byte[4096];
         int bytesRead = -1;

         while ((bytesRead = inStream.read(buffer)) != -1) {
             outStream.write(buffer, 0, bytesRead);
         }

         inStream.close();
         outStream.close();
    }

    /**
     * Exporta los datos del archivo en el tipo de archivo indicado,
     * la exportaci贸n es como un adjunto de forma que se descargue del navegador.
     *
     * @param tipo el tipo de archivo TXT o CSV
     * @param em el modelo de exportacion
     * @param filePath ruta absoluta del archivo que contiene la informacion a exportar
     * @param response el response de la servlet
     *
     * @throws IOException si falla la exportaci贸n
     */
    public static void exportSeparatorBased(String tipo, ExportModel em, String filePath, HttpServletResponse response) throws IOException {

    	//LOG.debug("Ingresa a export por separador, tipo: " + tipo);

        String fileExtension = null;

        Exportable exporter = null;
        BufferedReader br = null;

        try {

        	StringBuffer separator = new StringBuffer();
        	separator.append(em.getOriginalFileColumnSeparator());


            if (TIPO_EXPORT_TXT.equalsIgnoreCase(tipo)) {
                exporter = new TextExporter(em, response.getOutputStream());
                response.setContentType(CONTENT_TYPE_TXT);
                fileExtension = FILE_EXTENSION_TXT;
            } else {
                exporter = new CSVExporter(em, response.getOutputStream());
                response.setContentType(CONTENT_TYPE_CSV);
                fileExtension = FILE_EXTENSION_CSV;
            }

            response.setHeader("Content-disposition", "attachment; filename=" + em.getFileName() + "." + fileExtension);

            if (em.isOcultarTitulos()) {
            	for (Column c : em.getColumns()) {
                    exporter.writeColumn(c, c.getTitle());
                }
            }

            //exporter.nextRow();

            br = new BufferedReader(new FileReader(filePath));

            String line = null;

            while ( (line = br.readLine()) != null ) {
            	if ("|".equals(separator.toString())) {
            		separator.append("\\");
            	}

                String[] colValues = line.split(separator.toString()); //FSW STEFANINI se actualiza para dividir linea

                for (Column col : em.getColumns()) {
                    try {
                        Object ret = colValues[col.getColPos()];

                        Format fmtr = getFormatter(col, ret);

                        if (fmtr != null) {


                        	if (isNumber(ret)) {
                        		String objetoString = ret.toString().replaceAll(",", "");
                        		objetoString = remplazaSignoPesos(objetoString);
                        		ret = new BigDecimal(objetoString.trim());
                        	}

                            if (tieneValor(ret)) {
                                ret = fmtr.format(ret);
                            }
                        }
                        exporter.writeColumn(col, ret);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        exporter.writeColumn(col, null);
					} catch (IllegalArgumentException e) {
                        //LOG.debug("Error al escribir al servlet de salida: " + e.getMessage());
                    }
                     catch (IOException e) {
                    	//LOG.debug("Error al escribir al servlet de salida: " + e.getMessage());
                    }
                }

                exporter.nextRow();
            }
        } finally {

            if (exporter != null) {
                exporter.close();
            }
            if (br != null) {
                br.close();
            }
        }

       // LOG.debug("Termina proceso de exportar por separador.");
    }

    /**
     * Metodo para evitar el sonar.
     * @since 27/03/2015
     * @author FSW-Stefanini
     * @param objetoString de tipo String
     * @return objetoString de tipo String
     */
    private static String remplazaSignoPesos(String objetoString){
    	return objetoString.replaceAll("\\$", "");
    }

    /**
     * Lee una columna del archivo.
     *
     * @param col Columna con meta informaci&oacute;n
     * @param line Registro del archivo
     * @return Valor de columna
     */
    private static String readColumn(Column col, String line) {

        String columnValue = null;

        if (("".equals(line.trim()) || line == null) ||
                (col.getPosIni() > line.length()) ) {
            columnValue = "";
        } else {

            int posIni = col.getPosIni();
            int posFin = col.getPosFin()+1;

            if (col.getPosFin() >= line.length()) {
                posFin = line.length();
            }

            columnValue = line.substring(posIni, posFin);
        }

        return columnValue;
    }

    /**
     * Metodo para validar si es numerico.
     * @since 27/03/2015
     * @author FSW-Stefanini
     * @param o de tipo Object
     * @return true false de tipo booleano
     */
    private static boolean isNumber(Object o) {

    	try {
    		String objetoString = o.toString().replaceAll(",", "");
    		objetoString = remplazaSignoPesos(objetoString);
    		new BigDecimal(objetoString.trim());
    		return true;
    	} catch (Exception e) {
    		return false;
    	}

    }

    /**
     * Valida si el objeto tiene valor significativo
     * @param o objeto a validar
     * @return true si tiene valor, false de lo contrario
     */
    private static boolean tieneValor(Object o) {
        //Si es null no tiene valor
        if (o == null) {
            return false;
        }

        if (o instanceof String) {
            //Si es string y tiene algo, tiene valor
            return o.toString().trim().length() > 0;
        }

        //Si es otro tiepo (date, numer, etc) tiene valor
        return true;
    }
}