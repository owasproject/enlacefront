/**
 * 
 */
package mx.altec.enlace.export;

import java.io.OutputStream;

/**
 * Exporta un archivo en formato separado por comas.
 * 
 * @author Stefanini
 *
 */
public class CSVExporter extends TextExporter {
    
    
    /**
     * Crea una instancia del exportador.
     * 
     * @param em la meta informacion del archivo a exportar
     * @param out el stream de salida donde se escribira el archivo
     */
    public CSVExporter(ExportModel em,  OutputStream out) {
        super(em, out);
        columnSeparator = ',';
    }

}