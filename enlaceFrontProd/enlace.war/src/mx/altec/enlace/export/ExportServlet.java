package mx.altec.enlace.export;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.export.exception.ExportarException;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * Servlet que exporta el achivo en el formato indicado para cuando la 
 * informacion exportar esta en un archivo con columnas separadas por 
 * posiciones.
 * 
 * @author Alberto Rodriguez
 *
 */
public class ExportServlet extends HttpServlet {
    
	/** Variable de apoyo para log. */
	private static final Logger LOG = Logger.getLogger(ExportServlet.class);

    /**
     * El Id para la serializacion. 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Nombre del parametro que indica el tipo de archivo a exportar.
     */
    private static final String PARAM_TIPO = "tipoArchivo";
    
    /**
     * Nombre del parametro contiene el nombre del atributo se sesion donde se 
     * almacena la ruta del archivo a exportar.
     */
    private static final String PARAM_ATRIBUTO_FILE = "af"; 
    
    /**
     * Nombre del parametro contiene el nombre del atributo se sesion donde se 
     * almacena el modelo de exportacion.
     */
    private static final String PARAM_ATRIBUTO_EXPORT_MODEL = "em";
    
    /**
     * Tipo de Exportacion utilizada Bean, SubString, Separados.
     */
    private static final String PARAM_TIPO_EXPORT = "metodoExportacion";
    
    /**
     * Nombre del Bean para identificar el objeto con la informacion a exportar.
     */
    private static final String NOMBRE_BEAN_EXPORT = "nombreBeanExport";
    
    /**
     * Tipo de archivo a exportar como texto plano.
     */
    private static final String TIPO_TXT = "txt";
    
    /**
     * Tipo de archivo a exportar como separado por comas.
     */
    private static final String TIPO_CSV = "csv";

    /**
     * Metodo get para procesar las peticiones de exportacion.
     *
     * @param request Objeto request
     * @param response Objeto response
     * @throws ServletException Exception en el proceso
     * @throws IOException Excepción para el tratamiento de archivos.
     */
    protected void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String paramTipo = request.getParameter(PARAM_TIPO);
        String paramEM = request.getParameter(PARAM_ATRIBUTO_EXPORT_MODEL);
        String paramTipoExport = request.getParameter(PARAM_TIPO_EXPORT);
        String tipoExportar = TIPO_TXT;
        LOG.debug("ServletExporter - Tipo: " + paramTipo + ", Metodo: " + paramTipoExport);        

        checkValue(response, PARAM_TIPO, paramTipo);
        
        checkValue(response, PARAM_ATRIBUTO_EXPORT_MODEL, paramEM);
        checkValue(response, PARAM_TIPO_EXPORT, paramTipoExport);
        
        
        
        ExportModel em = (ExportModel) request.getSession().getAttribute(paramEM);
        
        
        checkValue(response, "Export Model", em);

        if (TIPO_CSV.equals(paramTipo)) {
        	tipoExportar = TIPO_CSV;
        }
        
        if("BEAN".equals(paramTipoExport.toUpperCase().trim())) {
        	LOG.debug("Exportación de tipo bean");
        	String nombreBeanExport = request.getParameter(NOMBRE_BEAN_EXPORT);
        	checkValue(response, NOMBRE_BEAN_EXPORT, nombreBeanExport);
        	List<Object> lstBean = (List<Object>) request.getSession().getAttribute(nombreBeanExport);
        	
        	try {
        		HtmlTableExporter.export(tipoExportar, em, lstBean, response);
			} catch (ExportarException e) {
				new ServletException(e.getMessage());
			}
        } else {
        	LOG.debug("Exportación por archivo");
        	String paramAf = request.getParameter(PARAM_ATRIBUTO_FILE);
        	checkValue(response, PARAM_ATRIBUTO_FILE, paramAf);
        	String filePath = (String) request.getSession().getAttribute(paramAf);
        	checkValue(response, "File Path", filePath);
        	
        	LOG.debug("ServletExporter - EM.existente" + em.isArchivoExistente());
        	if (TIPO_TXT.equals(paramTipo) && em.isArchivoExistente()) {
        		HtmlTableExporter.copyFile(em, filePath, response);
        	} else {
        		HtmlTableExporter.export(tipoExportar, em, filePath, response);
        	}
        }
        
        LOG.debug("ServletExporter - termina exportación");
    }
    
    /**
     * Check value.
     *
     * @param response the response
     * @param paramName the param name
     * @param paramValue the param value
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void checkValue(HttpServletResponse response, String paramName, Object paramValue) throws IOException {
    	if (paramValue == null) {
    		response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parametro invalido: " + paramName);
    	}
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	
    			defaultAction(req, resp);
    }
    
    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	defaultAction(req, resp);
    }

}