/**
 * 
 */
package mx.altec.enlace.export;

import java.io.IOException;
import java.io.OutputStream;  

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Exportador de archivo en formato de texto plano.
 * 
 * @author Stefanini
 *
 */
public class TextExporter implements Exportable {

    /**
	 * Caracteres de fin de linea.
	 */
    private static final String EOL = "\r\n";
    
    /**
     * El separador de columna a usar.
     */
    protected transient char columnSeparator;
    
    /**
     * La posicion de la columna actual.
     */
    private transient long currentColumn;
    /**
     * La posicion del renglon actual.
     */
    private transient long currentRow;
    /**
     * Indica la ultima posicion del renglon.
     */
    private transient long lastRow;
    
    /**
     * El stream de salida donde se escribira el archivo
     */
    private transient final OutputStream out;
    
    
    
    /**
     * Crea una instancia del exportador.
     * @param em la meta informacion del archivo a exportar
     * @param out el stream de salida donde se escribira el archivo
     */
    public TextExporter(ExportModel em,  OutputStream out) {
        this.out = out;
        this.columnSeparator = em.getColumnSeparator();
    }

    
    /**
     * Indica al exportador que hay que moverse al siguiente renglon.
     * @throws IOException si falla el movimiento al siguiente renglon
     */
    public void nextRow() throws IOException {
        currentColumn = 0;
        lastRow = currentRow;
        currentRow++;
        
    }

    /**
     * Indica al exportador que hay que escribir la siguiente columna.
     * @param col la meta informacion de la columna a exportar
     * @param data la informacion a exportar
     * @throws IOException si falla la escritura de la columna
     */
    public void writeColumn(Column col, Object data) throws IOException {
        
        if (currentColumn == 0 && currentRow > lastRow) {
            out.write(EOL.getBytes());
        }
        
        if (currentColumn > 0) {
        	out.write(columnSeparator);
        }
        
    	EIGlobal.mensajePorTrace("data ["+data+"]", NivelLog.DEBUG);
		
    	
        if (data != null) {
			String campo=data.toString().replaceAll("\\$","").trim();
        	if (','==columnSeparator){				
				if(!esDecimal(campo)){
					out.write("\"\t".getBytes());
				}
							  		
        	}
            out.write(data.toString().replaceAll("\\$","").trim().getBytes());			
            if (','==columnSeparator){
				if(!esDecimal(campo)){
					out.write("\"\t".getBytes());
				}
        	}						
        }
        out.flush();
        
        currentColumn++;
        
    }

    /**
     * Indica al exportador que se cierre y que ha terminado la exportacion. 
     * @throws IOException si falla el cierre del exportador
     */
    public void close() throws IOException {
    	out.write(EOL.getBytes());
        out.flush();
    }
	
	
	
	public boolean esDecimal(String cad){
		if(cad.indexOf('.')>-1){
			return true;
		}else{
			return false;
		}
		
	}

}
