package mx.altec.enlace.export;

import java.util.ArrayList;
import java.util.List;
 

/**
 * Contiene la meta informacion para la exportacion de la informacion.
 * 
 * @author Stefanini
 *
 */
public class ExportModel {

    /**
     * Nombre del archivo a exportar, sin extension.
     */
    private transient String fileName;
    
    /**
     * Separador de columna para archivos de texto.
     */
    private transient char columnSeparator;
    
    /**
     * Separador de columna para el archivo de texto de donse se obtiene la 
     * informacion.
     */
    private transient String originalFileColumnSeparator;
    
    /**
     * Indica si el origen sera un archivo con separador de columnas.
     */
    private transient boolean useColumnSeparatorFile;
    
    
    /**
     * Lista con la informacion acerca de las columnas a exportar.
     */
    private transient List<Column> columns;

    /**
     * Archivo Existente
     */
    private transient boolean archivoExistente;
    
    /**
     * Ocultar Titulos
     */
    private transient boolean ocultarTitulos = true;
    
    /**
     * Constructor.
     * 
     * @param fileName el nombre del archivo a exportar, sin extension 
     * @param columSeparator el separador de columna para archivos de texto
     */
    public ExportModel(String fileName, char columSeparator) {
        super();
        columns = new ArrayList<Column>();
        this.fileName = fileName;
        this.columnSeparator = columSeparator;
        this.archivoExistente = false;
    }
    
    /**
     * Constructor.
     * 
     * @param fileName el nombre del archivo a exportar, sin extension 
     * @param columSeparator el separador de columna para archivos de texto
     * @param archivoExistente de tipo boolean
     */
    public ExportModel(String fileName, char columSeparator, boolean archivoExistente) {
        super();
        columns = new ArrayList<Column>();
        this.fileName = fileName;
        this.columnSeparator = columSeparator;
        this.archivoExistente = archivoExistente;
    }

    /**
     * Constructor.
     * 
     * @param fileName el nombre del archivo a exportar, sin extension 
     * @param columSeparator el separador de columna para archivos de texto
     * @param originalFileColumnSeparator el separador de columna para el 
     *            archivo de texto de donse se obtiene la informacion 
     */
    public ExportModel(String fileName, char columSeparator, String originalFileColumnSeparator) {
        this(fileName,columSeparator);
        this.originalFileColumnSeparator = originalFileColumnSeparator;
        useColumnSeparatorFile = true;
        this.archivoExistente = false;
    }
    
    /**
     * Constructor.
     * @param fileName el nombre del archivo a exportar, sin extension 
     * @param columSeparator el separador de columna para archivos de texto
     * @param originalFileColumnSeparator el separador de columna para el 
     *            archivo de texto de donse se obtiene la informacion 
     * @param archivoExistente si la exportacion ya exisiste regresar archivo existente
     */
    public ExportModel(String fileName, char columSeparator, String originalFileColumnSeparator, boolean archivoExistente) {
        this(fileName,columSeparator);
        this.originalFileColumnSeparator = originalFileColumnSeparator;
        useColumnSeparatorFile = true;
        this.archivoExistente = archivoExistente;
    }
    
    
    
    
    /**
     * Agrega una columna a la lista de columnas a exportar.
     * 
     * @param col la columna a agregar
     * 
     * @return la instancia de esta clase
     */
    public ExportModel addColumn(Column col) {
        columns.add(col);
        return this;
    }

    
    /**
     * Agrega una lista de columnas a la lista de columnas a exportar.
     * 
     * @param colums las columnas con la meta informacion
     * 
     * @return la instancia de esta clase
     */
    public ExportModel addColumns(List<Column> colums) {
        columns.addAll(columns);
        return this;
    }


    /**
     * Regresa la propiedad fileName.
     *
     * @return la propiedad fileName
     */
    protected String getFileName() {
        return fileName;
    }


    /**
     * Regresa la propiedad columnSeparator.
     *
     * @return la propiedad columnSeparator
     */
    protected char getColumnSeparator() {
        return columnSeparator;
    }


    /**
     * Regresa la propiedad columns.
     *
     * @return la propiedad columns
     */
    protected List<Column> getColumns() {
        return columns;
    }

	/**
	 * Regresa la propiedad originalFileColumnSeparator.
	 * 
	 * @return la propieadad originalFileColumnSeparator
	 */
    protected String getOriginalFileColumnSeparator() {
		return originalFileColumnSeparator;
	}

	/**
	 * Regresa la propiedad useColumnSeparatorFile.
	 * 
	 * @return la propiedad useColumnSeparatorFile
	 */
    protected boolean isUseColumnSeparatorFile() {
		return useColumnSeparatorFile;
	}

	/**
	 * isArchivoExistente de tipo boolean.
	 *
	 * @return archivoExistente de tipo boolean
	 */
	public boolean isArchivoExistente() {
		return archivoExistente;
	}

	/**
	 * isOcultarTitulos de tipo boolean.
	 * @author FSW-Stefanini
	 * @return ocultarTitulos de tipo boolean
	 */
	public boolean isOcultarTitulos() {
		return ocultarTitulos;
	}

	/**
	 * setOcultarTitulos para asignar valor a ocultarTitulos.
	 * @author FSW-Stefanini
	 * @param ocultarTitulos de tipo boolean
	 */
	public void setOcultarTitulos(boolean ocultarTitulos) {
		this.ocultarTitulos = ocultarTitulos;
	}
    
    

}