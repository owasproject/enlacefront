package mx.altec.enlace.export;

import java.text.Format;


/**
 * Almacena la informacion necesario para generar una columna en el archivo
 * exportado.
 *
 * @author Stefanini
 *
 */
public class Column {

    /**
     * Nombre del campo a exportar.
     */
    private transient String fieldName;
    /**
     * Formato del campo a exportar.
     */
    private transient String format;
    /**
     * El titulo de la columna a exportar.
     */
    private transient String title;

    /**
     * El formateador de la información de la columna.
     */
    private transient Format formatter;

    /**
     * Indica la posición inicial para cuando las columnas son separadas por posiciones.
     */
    private transient int posIni;
    /**
     * Indica la posición final para cuando las columnas son separadas por posiciones.
     */
    private transient int posFin;

    /**
     * Indica la posicion dela columna dentro del archivo cuando es separado por
     * algun caracter.
     */
    private transient int colPos;
    
    /** Indica cuantas posiciones se recorrera el punto decimal a la izquierda.
     * Ejemplo:
     * valor = 123;
     * Si posPuntoDec = 2 
     * Por lo tanto el valor nuevo sera:
     * valor = 1.23*/
    private transient int posPuntoDec;

    /**
     * Constructor para cuando es lista de beans y hay que formatear la columna.
     *
     * @param fieldName el nombre del campo en la lista de beans
     * @param title el titulo de la columna
     * @param format el formato de la columna
     */
    public Column(String fieldName, String title, String format) {
        super();
        this.fieldName = fieldName;
        this.title = title;
        this.format = format;
    }
    
    /**
     * Constructor para cuando es lista de beans y hay que formatear la columna.
     *
     * @param fieldName el nombre del campo en la lista de beans
     * @param title el titulo de la columna
     * @param format el formato de la columna
     * @param posPuntoDec el numero de posiciones para recorrer el punto decimal a la izquierda
     */
    public Column(String fieldName, String title, String format, int posPuntoDec) {
    	super();
    	this.fieldName = fieldName;
    	this.title = title;
    	this.format = format;
        this.posPuntoDec = posPuntoDec;
    }

    /**
     * Constructor para cuando es lista de beans.
     * @param fieldName el nombre del campo en la lista de beans
     * @param title el titulo de la columna
     */
    public Column(String fieldName, String title) {
        super();
        this.fieldName = fieldName;
        this.title = title;
    }

    /**
     * Constructor para cuando las columnas son por posiciones y hay que
     * formatear la columna.
     *
     * @param posIni la posición inicial de la columna
     * @param posFin la posición final de la columna
     * @param title el titulo de la columna
     * @param format el formato de la columna
     */
    public Column(int posIni, int posFin, String title, String format) {
        super();
        this.posIni = posIni;
        this.posFin = posFin;
        this.title = title;
        this.format = format;
    }

    /**
     * Constructor para cuando las columnas son por posiciones.
     *
     * @param posIni la posición inicial de la columna
     * @param posFin la posición final de la columna
     * @param title el titulo de la columna
     */
    public Column(int posIni, int posFin, String title) {
        super();
        this.posIni = posIni;
        this.posFin = posFin;
        this.title = title;
    }

    /**
     * Constructor para cuando las columnas son por posiciones y hay que
     * formatear la columna.
     *
     * @param colPos la posición de la columna
     * @param title el titulo de la columna
     * @param format el formato de la columna
     */
    public Column(int colPos, String title, String format) {
        super();
        this.colPos = colPos;
        this.title = title;
        this.format = format;
    }
    
    /**
     * Constructor para cuando las columnas son por posiciones y hay que
     * formatear la columna.
     *
     * @param colPos la posición de la columna
     * @param title el titulo de la columna
     * @param posPuntoDec el numero de posiciones para recorrer el punto decimal a la izquierda
     */
    public Column(int colPos, String title, String format, int posPuntoDec) {
    	super();
    	this.colPos = colPos;
    	this.title = title;
    	this.format = format;
        this.posPuntoDec = posPuntoDec;
    }

    /**
     * Constructor para cuando las columnas son por posiciones.
     *
     * @param colPos la posición de la columna
     * @param title el titulo de la columna
     */
    public Column(int colPos, String title) {
        super();
        this.colPos = colPos;
        this.title = title;
    }

    /**
     * Constructor para cuando las columnas son por posiciones y hay que
     * formatear la columna.
     *
     * @param posIni la posición inicial de la columna
     * @param posFin la posición final de la columna
     * @param title el titulo de la columna
     * @param format el formato de la columna
     * @param posPuntoDec el numero de posiciones para recorrer el punto decimal a la izquierda
     */
    public Column(int posIni, int posFin, String title, String format, int posPuntoDec) {
        super();
        this.posIni = posIni;
        this.posFin = posFin;
        this.title = title;
        this.format = format;
        this.posPuntoDec = posPuntoDec;
    }

	/**
     * Regresa la propiedad fieldName.
     *
     * @return la propiedad fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Asigna la propiedad fieldName.
     * @param fieldName la propiedad a asignar.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Regresa la propiedad format.
     *
     * @return la propiedad format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Asigna la propiedad format.
     * @param format la propiedad a asignar.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Regresa la propiedad title.
     *
     * @return la propiedad title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Asigna la propiedad title.
     * @param title la propiedad a asignar.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Regresa la propiedad formatter.
     *
     * @return la propiedad formatter
     */
    public Format getFormatter() {
        return formatter;
    }

    /**
     * Asigna la propiedad formatter.
     * @param formatter la propiedad a asignar.
     */
    public void setFormatter(Format formatter) {
        this.formatter = formatter;
    }

    /**
     * Regresa la propiedad posIni.
     *
     * @return la propiedad posIni
     */
    public int getPosIni() {
        return posIni;
    }

    /**
     * Asigna la propiedad posIni.
     * @param posIni la propiedad a asignar.
     */
    public void setPosIni(int posIni) {
        this.posIni = posIni;
    }

    /**
     * Regresa la propiedad posFin.
     *
     * @return la propiedad posFin
     */
    public int getPosFin() {
        return posFin;
    }

    /**
     * Asigna la propiedad posFin.
     * @param posFin la propiedad a asignar.
     */
    public void setPosFin(int posFin) {
        this.posFin = posFin;
    }

    /**
     * Regresa la propiedad colPos.
     *
     * @return la propiedad colPos
     */
    public int getColPos() {
		return colPos;
	}

    /**
     * Regresa la propiedad posPuntoDec.
     *
     * @return la propiedad posPuntoDec
     */
	public int getPosPuntoDec() {
		return posPuntoDec;
	}
	
	/**
     * Asigna la propiedad posPuntoDec.
     * @param posPuntoDec la propiedad a asignar.
     */
	public void setPosPuntoDec(int posPuntoDec) {
		this.posPuntoDec = posPuntoDec;
	}

}