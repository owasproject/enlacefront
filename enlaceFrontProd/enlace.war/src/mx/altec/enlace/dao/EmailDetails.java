/** 
*   Isban Mexico
*   Clase: EmailDetails.java
*   Descripcion: Clase encargada de obtener el detalle del mail.
*   Control de Cambios: 
*   1.0 22/11/2007 EVERIS - Se crean nuevos metodos para  configurar notificacion por   
*   cambio de correo electronico 
*         
*   1.1 27/11/2013 EVERIS - Se crean mensajes para EdosCta   de notificaciones     
*   
*   1.2 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid). 
*/

package mx.altec.enlace.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.Vector;

import mx.altec.enlace.bo.DatosManc;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;




/**
 *  Clase encargada de obtener el detalle del mail
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Feb 26, 2007
 */
public class EmailDetails extends BaseServlet{
    /**
     * Constante para el html
     */
    public static final String CADHTML = "<center><table border=1 cellpadding=\"0\" cellspacing=\"0\">";
    /**
     * Constante para la etiqueta td
     */
    public static final String ETIQUETA_TD = "<td>";
    /**
     * Constante para la etiqueta /td
     */
    public static final String ETIQUETA_CIERRA_TD = "</td>";
    /**
     * Constante para la literal "conforme a lo siguiente:"
     */
    public static final String CONFORME_A_LO_SIGUIENTE = "conforme a lo siguiente:";
    /**
     * Constante para la literal "null"
     */
    public static final String TXT_NULL = "null";

    /**
     * Constante para la literal <br>
     */
    public static final String BR = "<BR></BR>";
    /**
     * Constante para la literal <tr>
     */
    public static final String TR = "<tr>";
    /**
     * Constante para la literal </tr>
     */
    public static final String DIAG_TR = "</tr>";
    /**
     * Constante que indica el maximo de cuentas que se veran en el mail
     */
    public static final int MAX_DETAIL = 10;
    /**
     * Abre tds
     */
    private static final String ABRETDS = "<td>&nbsp;&nbsp;";
    /**
     * Cierra tds
     */
    private static final String CIERRATDS = "&nbsp;&nbsp;</td>";
    /**
     * Constante que indica el salto de linea
     */
    private static final String SALTO_DE_LINEA = "\n";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_ABRE_B = "<B>";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_NOMBRE_DE = " a nombre de ";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_CIERRA_B = "</B>";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_COMA_ESPACIO = ", ";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String COD_ERROR = "0000";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_DOBLE_BR = "<BR></BR><BR></BR>";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_ABRE_TR_TD = "<TR><TD>";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_CIERRA_TR_TD = "</TD></TR>";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_ESPACIO_BLANCO = "&nbsp;";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TABLE_CIERRA = "</TABLE>";
    
    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String TAG_NEGRITA_DE = "</B> de <B>";
   
    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String LADA_SIN_COSTO = "Lada sin costo 01 800 509 5000.</B><br>";

    /* EVERIS 29/11/2007 - INICIA constructor por defecto */
    /**
     ** Variable para el arrayList con detlle de cuentas interbancarias
     */
    private static ArrayList detalleCuentasInterb = null;

    /**
     * Numero de Contrato
     */
    private static String numContratoSinTrunc = null;

    /**
     * Lotes en los cuales se encuentran las cuentas
     */
    private static String lotes = null;

    /**
    /* EVERIS 29/11/2007 - INICIA constructor por defecto */
    /**
     ** Constante para el nuevo mail
     ** Constante para el tipo de Dato
     */
    private String mailNuevo = "";
    /**
     * mailViejo
     */
    private String mailViejo = "";
    /**
     * tipoDato
     */
    private String tipoDato = "";

    /**
     * numRegImportados
     */
    private int numRegImportados;
    /**
     * impTotal
     */
    private String impTotal;
    /**
     * estatusActual
     */
    private String estatusActual;
    /**
     * numRef
     */
    private String numRef;
    /**
     * razonSocial
     */
    private String razonSocial;
    /**
     * idLote
     */
    private String idLote;

    /**
     * tipoPagoNomina
     */
    private String tipoPagoNomina;
    /**
     * tipoPagoProveedor
     */
    private String tipoPagoProveedor;
    /**
     * numTransmision
     */
    private String numTransmision;
    /**
     * numCuentaCargo
     */
    private String numCuentaCargo;
    /**
     * tipoPagoImp
     */
    private String tipoPagoImp;
    /**
     * tipoTransferenciaUni
     */
    private String tipoTransferenciaUni;
    /**
     * cuentaDestino
     */
    private String cuentaDestino;
    /**
     * tipo_operacion
     */
    private String tipoOperacion;
    /**
     * estatusFinal
     */
    private String estatusFinal;
    /**
     * impTotalUSD
     */
    private String impTotalUSD;
    /**
     * impTotalMXN
     */
    private String impTotalMXN;
    //Consys 20.07.2010 variable para alta de ceuntas notificacion
    /**
     * detalle_Alta
     */
    private String detalleAlta;
    //Consys 22.07.2010 variable para alta o baja de cuentas notificacion
    /**
     * tipoOpCuenta
     */
    private String tipoOpCuenta;
    /**
     * esPAGT
     */
    private boolean esPAGT = false;
    /**
     * esMayorA100Reg
     */
    private boolean esMayorA100Reg = false;

    //Lista de operaciones de Limites y Montos
    /**
     * listaOperLYM
     */
    private ArrayList<LimitesOperacion> listaOperLYM = new ArrayList<LimitesOperacion>();
    /**
     * regVincTarj
     */
    private String[] regVincTarj = new String[3];

    /**
     * Detalle de las cuentas a autorizar
     */
    private Vector detalleCuentas = null;

    /**
     * Numero de Contrato
     */
    private String numContrato = null;

    /**
     * Numero de Contrato
     */
    private String secuencia = null;

    /**
     * Numero de Contrato
     */
    private List<String> nombres = null;

    /**
     * Descripcion del Contrato
     */
    private String descContrato = null;

    /**
     * Detalle de las cuentas a autorizar
     */
    private boolean attachment = false;

    /**
     * Archivo adjunto al mail
     */
    private String fileAttachmentName;

        /**
     * numero de usuario
     */
    private String usuario;

    /**
     *formato del archivo
     */
    private String[] formatoArchivo;

    /**
     * Convenio Micrositio
     */
    private String convenio;

    /**
     * Nombre Convenio Micrositio
     */
    private String nombreConvenio;

        /**
     * Lista 1 EDC Historicos
     */
    private List<String> listaFolios;

    /**
     * lista 2 EDC Historicos
     */
    private List<String> listaErrores;

    /* EVERIS 22/11/2007 - INICIA constructor por defecto */
    /**
     * Constructor por defecto
     */
    public EmailDetails(){
    }

    /* EVERIS 22/11/2007 - FIN constructor por defecto */
    /**
     * Constructor que inicializa el detalle del mail
     *
     * @param numContrato
     *            numero de contrato
     * @param nombreContrato
     *            nombre del contrato
     * @param secuencia
     *            secuencia de la remesa
     * @param nombres
     *            nombres de los empleados
     */
    public EmailDetails(String numContrato, String nombreContrato,
            String secuencia, List<String> nombres) {
        this.numContrato = numContrato;
        this.nombres = nombres;
        this.secuencia = secuencia;
        this.descContrato = nombreContrato;
        if(nombres.size() > 10){
            this.attachment = true;
        }
    }
    /* EVERIS 22/02/2008 - INICIA constructor para Interb */
    /**
     * Constructor que inicializa el detalle del mail
     *
     * @param detalleCuentasInterb      Detalle de las cuentas a informar
     * @param numContrato               Numero de Contrato
     * @param descContrato              Descripcion del Contrato
     */
    public EmailDetails(ArrayList detalleCuentasInterb, String numContrato,
            String descContrato) {
        this.numContrato = numContrato.substring(6);
        this.numContratoSinTrunc = numContrato;
        this.descContrato = descContrato;
        this.detalleCuentasInterb = detalleCuentasInterb;

    }
    /* EVERIS 22/02/2008 - FIN constructor por defecto */

    /**
     * Constructor que inicializa el detalle del mail
     *
     * @param detalleCuentas        Detalle de las cuentas a autorizar
     * @param numContrato           Numero de Contrato
     * @param descContrato          Descripcion del Contrato
     * @param lotes                 Id del lote final de carga masiva
     */
    public EmailDetails(Vector detalleCuentas, String numContrato,
            String descContrato, String lotes) {
        this.numContrato = numContrato.substring(6);
        this.numContratoSinTrunc = numContrato;
        this.descContrato = descContrato;
        this.lotes = lotes;
        this.detalleCuentas = detalleCuentas;
        this.detalleCuentasInterb = new ArrayList(detalleCuentas);
        if(this.detalleCuentas.size()> MAX_DETAIL){
            this.attachment = true;
        }
    }

    /**
     * Constructor que inicializa el detalle del mail usado en alta de empleados de nomina
     *
     * @param nomArch       Nombre del Archivo a manejar
     * @param numContrato   Numero de Contrato
     * @param descContrato  Descripcion del Contrato
     * @param secuencia     numero de secuencia del archivo procesado
     * @param registros     cantidad de registros contenidos en el archivo
     */
    public EmailDetails(String nomArch, String numContrato, String descContrato, String secuencia, int registros) {
        this.numContrato = numContrato.substring(6);
        this.numContratoSinTrunc = numContrato;
        this.descContrato = descContrato;
        this.lotes = secuencia;
        this.fileAttachmentName = nomArch;
        if(registros > 10){
            this.attachment = true;
        }
        this.numRegImportados = registros;
    }

    /**
     * getFormatoArchivo
     * @return formatoArchivo : String[]
     */
    public String[] getFormatoArchivo() {
        return formatoArchivo.clone();
    }

    /**
     * setFormatoArchivo
     * @param formatoArchivo : String[]
     */
    public void setFormatoArchivo(String[] formatoArchivo) {
        this.formatoArchivo = formatoArchivo;
    }

    /**
     * @return String : idLote
     */
    public String getIdLote() {
        return idLote;
    }

    /**
     * @param idLote : idLote


     */
    public void setIdLote(String idLote) {
        this.idLote = idLote;

    }

    /**
     * @return String : usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario : usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * getSecuencia
     * @return secuencia String
     */
    public String getSecuencia() {
        return secuencia;
    }

    /**
     * setSecuencia
     * @param secuencia String
     */
    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * getNombres
     * @return nombres List<String>
     */
    public List<String> getNombres() {
        return nombres;
    }

    /**
     * setNombres
     * @param nombres List<String>
     */
    public void setNombres(List<String> nombres) {
        this.nombres = nombres;
    }

    /**
     * Obtener el valor de attachment.
     * @return attachment valor asignado.
     */
    public boolean isAttachment() {
        return attachment;
    }

    /**
     * Asignar un valor a attachment.
     * @param attachment Valor a asignar.
     */
    public void setAttachment(boolean attachment) {
        this.attachment = attachment;
    }

    /**
     * Asignar un valor a fileAttachmentName.
     * @param fileAttachmentName Valor a asignar.
     */
    public void setFileAttachmentName(String fileAttachmentName) {
        this.fileAttachmentName = fileAttachmentName;
    }

    /**
     * Obtener el valor de fileAttachmentName.
     * @return fileAttachmentName valor asignado.
     */
    public String getFileAttachmentName() {
        return fileAttachmentName;
    }
    /**
     * getRegVincTarj
     * @return String[] regVincTarj
     */
    public String[] getRegVincTarj() {
        return regVincTarj.clone();
    }
    /**
     * setRegVincTarj
     * @param regVincTarj detalle
     */
    public void setRegVincTarj(String[] regVincTarj) {
        this.regVincTarj = regVincTarj;
    }

    /**
     * isMayorA100Reg
     * @return boolean esMayorA100Reg
     */
    public boolean isMayorA100Reg() {
        return esMayorA100Reg;
    }

    /**
     * isMayorA100Reg
     * @param esMayorA100Reg boolean
     */
    public void setMayorA100Reg(boolean esMayorA100Reg) {
        this.esMayorA100Reg = esMayorA100Reg;
    }

    /**
     * getImpTotalUSD
     * @return String impTotalUSD
     */
    public String getImpTotalUSD() {
        return impTotalUSD;
    }

    /**
     * setImpTotalUSD
     * @param impTotalUSD String
     */
    public void setImpTotalUSD(String impTotalUSD) {
        this.impTotalUSD = impTotalUSD;
    }

    /**
     * getImpTotalMXN
     * @return String impTotalMXN
     */
    public String getImpTotalMXN() {
        return impTotalMXN;
    }

    /**
     * setImpTotalMXN
     * @param impTotalMXN String
     */
    public void setImpTotalMXN(String impTotalMXN) {
        this.impTotalMXN = impTotalMXN;
    }
    /**
     * isPAGT
     * @return boolean esPAGT
     */
    public boolean isPAGT() {
        return esPAGT;
    }

    /**
     * setPAGT
     * @param esPAGT boolean
     */
    public void setPAGT(boolean esPAGT) {
        this.esPAGT = esPAGT;
    }

    /**
     * getTipo_operacion
     * @return tipo_operacion String
     */
    public String getTipo_operacion() {
        return tipoOperacion;
    }

    /**
     * setTipo_operacion
     * @param tipoOperacion String
     */
    public void setTipo_operacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    /**
     * getEstatusFinal
     * @return String estatusFinal
     */
    public String getEstatusFinal() {
        return estatusFinal;
    }

    /**
     * setEstatusFinal
     * @param estatusFinal String
     */
    public void setEstatusFinal(String estatusFinal) {
        this.estatusFinal = estatusFinal;
    }
    /**
     * getTipoOpCuenta
     * @return tipoOpCuenta String
     */
    public String getTipoOpCuenta() {
        return tipoOpCuenta;
    }

    /**
     * setTipoOpCuenta
     * @param tipoOpCuenta String
     */
    public void setTipoOpCuenta(String tipoOpCuenta) {
        this.tipoOpCuenta = tipoOpCuenta;
    }

    /**
     * getDetalle_Alta
     * @return detalle_Alta String
     */
    public String getDetalle_Alta() {
        return detalleAlta;
    }

    /**
     * setDetalle_Alta
     * @param detalleAlta String
     */
    public void setDetalle_Alta(String detalleAlta) {
        this.detalleAlta = detalleAlta;
    }

    /**
     * getNumCuentaCargo
     * @return numCuentaCargo String
     */
    public String getNumCuentaCargo() {
        return numCuentaCargo;
    }

    /**
     * setNumCuentaCargo
     * @param numCuentaCargo String
     */
    public void setNumCuentaCargo(String numCuentaCargo) {
        this.numCuentaCargo = numCuentaCargo;
    }

    /**
     * getCuentaDestino
     * @return cuentaDestino String
     */
    public String getCuentaDestino() {
        return cuentaDestino;
    }

    /**
     * setCuentaDestino
     * @param cuentaDestino String
     */
    public void setCuentaDestino(String cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    /**
     * getTipoTransferenciaUni
     * @return tipoTransferenciaUni String
     */
    public String getTipoTransferenciaUni() {
        return tipoTransferenciaUni;
    }

    /**
     * setTipoTransferenciaUni
     * @param tipoTransferenciaUni String
     */
    public void setTipoTransferenciaUni(String tipoTransferenciaUni) {
        this.tipoTransferenciaUni = tipoTransferenciaUni;
    }

    /**
     * getTipoPagoImp
     * @return tipoPagoImp String
     */
    public String getTipoPagoImp() {
        return tipoPagoImp;
    }

    /**
     * setTipoPagoImp
     * @param tipoPagoImp String
     */
    public void setTipoPagoImp(String tipoPagoImp) {
        this.tipoPagoImp = tipoPagoImp;
    }

    /**
     * getEstatusActual
     * @return estatusActual String
     */
    public String getEstatusActual() {
        return estatusActual;
    }

    /**
     * setEstatusActual
     * @param estatusActual String
     */
    public void setEstatusActual(String estatusActual) {
        this.estatusActual = estatusActual;
    }

    /**
     * getImpTotal
     * @return impTotal String
     */
    public String getImpTotal() {
        return impTotal;
    }

    /**
     * setImpTotal
     * @param impTotal String
     */
    public void setImpTotal(String impTotal) {
        this.impTotal = impTotal;
    }

    /**
     * getNumRef
     * @return numRef String
     */
    public String getNumRef() {
        return numRef;
    }

    /**
     * setNumRef
     * @param numRef String
     */
    public void setNumRef(String numRef) {
        this.numRef = numRef;
    }

    /**
     * getNumRegImportados
     * @return int numRegImportados
     */
    public int getNumRegImportados() {
        return numRegImportados;
    }

    /**
     * setNumRegImportados
     * @param numRegImportados int
     */
    public void setNumRegImportados(int numRegImportados) {
        this.numRegImportados = numRegImportados;
    }

    /**
     * getNumTransmision
     * @return numTransmision String
     */
    public String getNumTransmision() {
        return numTransmision;
    }

    /**
     * setNumTransmision
     * @param numTransmision String
     */
    public void setNumTransmision(String numTransmision) {
        this.numTransmision = numTransmision;
    }

    /**
     * getRazonSocial
     * @return razonSocial String
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * setRazonSocial
     * @param razonSocial String
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * getTipoPagoNomina
     * @return tipoPagoNomina String
     */
    public String getTipoPagoNomina() {
        return tipoPagoNomina;
    }

    /**
     * setTipoPagoNomina
     * @param tipoPagoNomina String
     */
    public void setTipoPagoNomina(String tipoPagoNomina) {
        this.tipoPagoNomina = tipoPagoNomina;
    }

    /**
     * getTipoPagoProveedor
     * @return tipoPagoProveedor String
     */
    public String getTipoPagoProveedor() {
        return tipoPagoProveedor;
    }

    /**
     * setTipoPagoProveedor
     * @param tipoPagoProveedor String
     */
    public void setTipoPagoProveedor(String tipoPagoProveedor) {
        this.tipoPagoProveedor = tipoPagoProveedor;
    }

    /**
     * Metodo principal que regresa el contenido del mensaje para el alta de
     * empleados de nomina.
     * @param fileName archivo
     * @return StringBuffer Contenido del Email
     */
    public StringBuffer msgType(String fileName){
        String direction = null;
        if(isAttachment()){
            try {
                setFileAttachmentName(createFile(fileName));
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
            }
            direction = "que se detallan en el archivo adjunto."
                      + BR
                      + BR;
        }else{
            String table = null;
            table = CADHTML
                  + TR
                  + "<td valign=\"top\">Nombre</td>"
                  + "<td valign=\"top\">N�mero de Cuenta</td>"
                  + DIAG_TR;

            try{
                //System.out.println("fileName = " + fileName);
                BufferedReader file = new BufferedReader(new FileReader(fileName));
                String actualLine = "";

                while(true){
                    actualLine = (String) file.readLine();
                    if(actualLine == null){
                        break;
                    }

                    String strContrato = actualLine.substring(0, 11);
                    String strEmpleado = actualLine.substring(11,actualLine.length());

                    if ( "".equals(strContrato.trim()) ){
                        strContrato = "En tr�mite";
                    }

                    //System.out.println("strContrato = " + strContrato + " strEmpleado = " + strEmpleado);

                    table = table
                        + "<tr>\n"
                        + ETIQUETA_TD + strEmpleado + ETIQUETA_CIERRA_TD
                        + ETIQUETA_TD + strContrato + ETIQUETA_CIERRA_TD
                        + "</tr>\n";
                }
                file.close();
            }
            catch (java.io.FileNotFoundException fnfe){
                EIGlobal.mensajePorTrace(new Formateador().formatea(fnfe), EIGlobal.NivelLog.DEBUG);
            }
            catch (java.io.IOException ioe){
                EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.DEBUG);
            }

            //System.out.println("EmailDetails :: Saliendo de ciclo");
            table = table + "</table></center>";
            //System.out.println("EmailDetails :: Termina creacion de tabla");
            direction = CONFORME_A_LO_SIGUIENTE
                      + BR
                      + BR
                      + table
                      + BR
                      + BR;
        }
        //System.out.println("EmailDetails :: Termina con exito");
        return msgTextAltaEmpl(direction);
    }

    /**
     * Metodo principal que regresa el contenido del mensaje para el alta de
     * empleados de nomina.
     * @return StringBuffer Contenido del Email
     *
    public StringBuffer msgTypeN3(){
        String direction = null;
        if(isAttachment()){
            try {
                setFileAttachmentName(createFileN3());
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
            }
            direction = "que se detallan en el archivo adjunto."
                      + BR
                      + BR;
        }else{
            String table = null;
            table = CADHTML
                  + TR
                  + "<td valign=\"top\">Nombre</td>"
                  + "<td valign=\"top\">N�mero de Cuenta</td>"
                  + DIAG_TR;

            for (String nombre : this.nombres) {
                table += "<tr><td align='center'>" + nombre
                      + "</td><td align='center'>En tr�mite</td></tr>";
            }

            //System.out.println("EmailDetails :: Saliendo de ciclo");
            table = table + "</table></center>";
            //System.out.println("EmailDetails :: Termina creacion de tabla");
            direction = CONFORME_A_LO_SIGUIENTE
                      + BR
                      + BR
                      + table
                      + BR
                      + BR;
        }
        //System.out.println("EmailDetails :: Termina con exito");
        return msgTextAsignacionTarjetaEmpleado(direction);
    }*/

    /**
     * Metodo principal que regresa el contenido del mensaje
     * @return StringBuffer     Contenido del Email
     */
    public StringBuffer msgType(){
        String direction = null;
        if(isAttachment()){
            try {
                setFileAttachmentName(createFile());
            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
            }
            direction = "que se detallan en el archivo adjunto."
                      + BR
                      + BR;
        }else{
            String table = null;
            table = CADHTML
                  + TR
                  + "<td valign=\"top\">Numero de Cuenta</td>"
                  + "<td valign=\"top\">Descripci�n</td>";
                  if(lotes!=null && !"".equals(lotes) && !lotes.equals(TXT_NULL) && !"0".equals(lotes)){
                      table += "<td valign=\"top\">Lote</td>";
                  }
                  table += DIAG_TR;
            for(int i=0; i<this.detalleCuentas.size(); i++){
                table = table
                      + TR
                      + ETIQUETA_TD + ((EmailContainer)this.detalleCuentas.get(i)).getNumCuenta() + ETIQUETA_CIERRA_TD
                      + ETIQUETA_TD + ((EmailContainer)this.detalleCuentas.get(i)).getTitular() + ETIQUETA_CIERRA_TD;
                      if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
                        table += ETIQUETA_TD + lotes + ETIQUETA_CIERRA_TD;
                      }
                      table += DIAG_TR;
            }
            table = table + "</table></center>";
            direction = CONFORME_A_LO_SIGUIENTE
                      + BR
                      + BR
                      + table
                      + BR
                      + BR;
        }
        return msgText(direction);
    }

    /**
     * Metodo encargado de crear el archivo adjunto en caso de que este se
     * requiera
     *
     * @return fileName         Nombre del archivo
     * @throws Exception        Error de tipo Exception
     */
    private String createFile() throws Exception{

        GregorianCalendar cal = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", new java.util.Locale("es","mx"));
        EmailContainer cuentasRecord = new EmailContainer();
        String path = null;
        String fileName = null;
        String head = null;
        String line = null;

        try {
            path = Global.DOWNLOAD_PATH;
            fileName = path + "adj_" + sdf.format(cal.getTime()) + ".txt";
            File diskFile = new File(fileName);
            RandomAccessFile file = new RandomAccessFile(diskFile, "rw");
            file.setLength(0); //para truncar el archivo en caso que exista
            if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
                head = dameFormato("Numero de Cuenta", 1)
                     + dameFormato("Descripci�n", 2)
                     + dameFormato("Lote", 3);
            }else{
                head = dameFormato("Numero de Cuenta", 1)
                     + dameFormato("Descripci�n", 2);
            }

            file.writeBytes(head + SALTO_DE_LINEA);
            for(int i=0; i<this.detalleCuentas.size(); i++) {
                cuentasRecord = (EmailContainer)this.detalleCuentas.get(i);
                line = dameFormato(cuentasRecord.getNumCuenta(), 1)
                     + dameFormato(cuentasRecord.getTitular(), 2);
                     if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
                         line += dameFormato(lotes, 3);
                     }
                     line += SALTO_DE_LINEA;
                file.writeBytes(line);
            }
            file.close();
        }catch (FileNotFoundException e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }catch (Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        return fileName;
    }

    /**
     * Metodo encargado de crear el archivo adjunto en caso de que este se
     * requiera para el alta de empleados de nomina.
     * 
     * @param fileName Nombre del archivo
     * @return Nuevo nombre del archivo
     * @throws Exception Error de tipo Exception
     */
    private String createFile(String fileName) throws Exception{

        GregorianCalendar cal = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", new java.util.Locale("es","mx"));
        String newFileName = null;
        String head = null;
        String line = null;

        try {
            newFileName = Global.DOWNLOAD_PATH+"adj_" + sdf.format(cal.getTime()) + ".txt";
            head = "Cuenta      " + "Nombre";
            line = head + SALTO_DE_LINEA;

            BufferedWriter newFile = new BufferedWriter(new FileWriter(newFileName));
            BufferedReader file = new BufferedReader(new FileReader(fileName));
            String actualLine = "";

            while(true){
                // Convert String to Bytes array
                actualLine = (String) file.readLine();

                if(actualLine == null){
                    break;
                }

                String strContrato = actualLine.substring(0, 11);
                String strEmpleado = actualLine.substring(11,actualLine.length());

                if ("".equals(strContrato.trim())){
                    strContrato = "Pendiente  ";
                }
                else{
                    while(strContrato.length()<13){
                        strContrato = strContrato.concat(" ");
                    }
                }

                //System.out.println("strContrato = [" + strContrato + "] strEmpleado = [" + strEmpleado + "]");

                line += strContrato + strEmpleado + SALTO_DE_LINEA;
                newFile.write(line);
                line = "";
            }
            file.close();
            newFile.close();
        }catch (java.io.FileNotFoundException fnfe){
            EIGlobal.mensajePorTrace(new Formateador().formatea(fnfe), EIGlobal.NivelLog.INFO);
        }catch (java.io.IOException ioe){
            EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.INFO);
        }catch (Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        return newFileName;
    }

    /**
     * Metodo encargado de crear el archivo adjunto en caso de que este se
     * requiera para el alta de empleados de nomina.
     *
     * @return fileName         Nombre del archivo
     * @throws Exception        Error de tipo Exception
     */
     /*
    private String createFileN3() throws Exception{

        GregorianCalendar cal = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", new java.util.Locale("es","mx"));
        String newFileName = null;
        String head = null;
        String line = null;

        try {
            newFileName = Global.DOWNLOAD_PATH + "adj_" + sdf.format(cal.getTime()) + ".txt";
            BufferedWriter newFile = new BufferedWriter(new FileWriter(newFileName));

            line = "Nombre, N�mero de Cuenta" + SALTO_DE_LINEA;
            newFile.write(line);
            line = "";

            for (String nombre : this.nombres) {
                line = nombre + ", En tr�mite" + SALTO_DE_LINEA;
                newFile.write(line);
                line = "";
            }

            newFile.close();
        }catch (java.io.FileNotFoundException fnfe){
            EIGlobal.mensajePorTrace(new Formateador().formatea(fnfe), EIGlobal.NivelLog.INFO);
        }catch (java.io.IOException ioe){
            EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.INFO);
        }catch (Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        return newFileName;
    }
    */

    /**
     * Anexo del mail
     * 
     * @param direction Direccion de correo
     * @return  Se refiere al texto dentro del mail
     */
    private StringBuffer msgText(String direction){
        StringBuffer body = new StringBuffer();
        int valParam = getValorParam();
        body.append("<HTML>");
        body.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        body.append("<BODY> ");
        body.append("<FONT FACE=\"arial\" SIZE=3 COLOR=black>");
        body.append("Por este medio le informamos que el d�a <B>");
        body.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        body.append(TAG_NEGRITA_DE);
        body.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        body.append(TAG_NEGRITA_DE);
        body.append(obtenCalendario().get(Calendar.YEAR));
        body.append("</B>, se ha solicitado el registro  de alta de cuentas dentro de su contrato ENLACE con terminaci�n ");
        body.append(TAG_ABRE_B);
        body.append(numContrato);
        body.append(TAG_CIERRA_B);
        body.append(TAG_NOMBRE_DE);
        body.append(TAG_ABRE_B);
        body.append(descContrato).append(", ");
        body.append(TAG_CIERRA_B);
        if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
            body.append("y con folio(s) de carga masiva <B>" + lotes + TAG_CIERRA_B + TAG_COMA_ESPACIO);
        }
        body.append(direction);
        body.append("La solicitud se encuentra actualmente con el estatus de \"Pendiente de Activar\". ");
        body.append("El estatus de la solicitud cambiar� a \"Activa\" dentro de los ");
        body.append(valParam).append(" minutos siguientes a la autorizaci�n.");
        body.append(BR);
        body.append(BR);
        body.append("En caso de no estar de acuerdo con esta solicitud de alta de cuentas y de contar con facultades para dar de baja cuentas ");
        body.append("dentro de ENLACE, es necesario accesar al servicio en la opci�n \"Administraci�n y Control\", \"Cuentas\", \"Autorizaci�n y Cancelaci�n\" y llevar a ");
        body.append("cabo la baja respectiva.");
        body.append(BR);
        body.append(BR);
        //if(lotes!=null && !lotes.equals("") && !lotes.equals(TXT_NULL)){
            //body.append("Ahora bien, en caso de que no este de acuerdo con el alta en general ponga el (los) numero(s) de folio de lote de carga masiva que desee eliminar");
            //body.append(" <B>" + lotes + "</B>, para realizar la baja respectiva.");
            //body.append(BR);
            //body.append(BR);
        //}
        body.append("Con el objetivo de brindarle un mejor servicio, si por alguna raz�n usted no puede realizar la baja de alguna cuenta o ");
        body.append("solicitud desde ENLACE, tambi�n puede solicitar la baja reenviando este correo a banca_electronica@santander.com.mx .");
        body.append(BR);
        body.append(BR);
        body.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 ");
        body.append("o Lada sin costo 01 800 509 5000.");
        body.append(BR);
        body.append(BR);
        body.append(" </BODY></HTML> ");
        return body;
    }

    /**
     *  Anexo del Mail para Alta de Empleados de Nomina.
     *  
     * @param direction direccion de correo electronico
     * @return Se refiere al texto dentro del mail
     */
    private StringBuffer msgTextAltaEmpl(String direction){
        StringBuffer body = new StringBuffer();



        body.append("<HTML>");
        body.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        body.append("<BODY> ");
        body.append("<FONT FACE=\"arial\" SIZE=3 COLOR=black>");
        //body.append("Estimado Cliente:");
        body.append(BR).append(BR);
        body.append("Por este medio le informamos que el d�a <B>");
        body.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        body.append(TAG_NEGRITA_DE);
        body.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        body.append(TAG_NEGRITA_DE);
        body.append(obtenCalendario().get(Calendar.YEAR));
        body.append("</B>, se ha solicitado el registro de empleados de n�mina dentro de su contrato ENLACE con terminaci�n ");
        body.append(TAG_ABRE_B);
        body.append(numContrato);
        body.append(TAG_CIERRA_B);
        body.append(TAG_NOMBRE_DE);
        body.append(TAG_ABRE_B);
        body.append(descContrato).append(", ");
        body.append(TAG_CIERRA_B);
        if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
            body.append("y con n�mero de secuencia <B>")
                .append(lotes)
                .append(TAG_CIERRA_B)
                .append(TAG_COMA_ESPACIO);
        }
        body.append(direction);
        body.append("La solicitud se encuentra actualmente en proceso y se podr� realizar dispersi�n a los empleados ");

        Calendar cal = Calendar.getInstance();

        //System.out.println("cal.get(Calendar.HOUR_OF_DAY) = " + cal.get(Calendar.HOUR_OF_DAY));
        if (cal.get(Calendar.HOUR_OF_DAY)< 15){
            body.append("a partir del proximo d�a h�bil.");
        }
        else{
            body.append("a partir del segundo d�a h�bil.");
        }
        body.append(BR).append(BR);
        body.append("En caso de no estar de acuerdo con esta solicitud y de contar con facultades para realizar cancelaciones ");
        body.append("dentro de ENLACE, es necesario accesar al servicio en la opci�n \"Servicios\", \"N�mina\", \"Empleados\", \"Consulta de Proceso de Alta\" y llevar a ");
        body.append("cabo la cancelaci�n respectiva.");
        body.append(BR).append(BR);
        body.append("Con el objetivo de brindarle un mejor servicio, si por alguna raz�n usted no puede realizar la cancelaci�n ");
        body.append("desde ENLACE, puede solicitar la cancelaci�n reenviando este correo a banca_electronica@santander.com.mx .");
        body.append(BR).append(BR);
        body.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 ");
        body.append("o Lada sin costo 01 800 509 5000.");
        body.append(BR).append(BR);
        //body.append("Atentamente,");
        body.append("</FONT>");
        body.append(BR);
        //body.append("<FONT FACE=\"arial\" SIZE=5 COLOR=red>");
        //body.append("<U>");
        //body.append("E N L A C E");
        //body.append("</U>");
        //body.append("</FONT>");
        body.append(BR).append(BR);
        body.append(" </BODY></HTML> ");

        //System.out.println("*******************************************************************************");
        //System.out.println("BODY COMPLETO A ENVIAR ********************************************************");
        //System.out.println(body);
        //System.out.println("*******************************************************************************");

        return body;
    }/**/

    /**
     * Metodo para obtenet el nombre del mes de la notificacion del mail
     *
     * @param mes       Numero entero con el mes
     * @return          Regresa el nombre del mes
     */
    public static String obtenNombreMes(int mes){
        switch(mes){
        case 0: return "Enero";
        case 1: return "Febrero";
        case 2: return "Marzo";
        case 3: return "Abril";
        case 4: return "Mayo";
        case 5: return "Junio";
        case 6: return "Julio";
        case 7: return "Agosto";
        case 8: return "Septiembre";
        case 9: return "Octubre";
        case 10: return "Noviembre";
        case 11: return "Diciembre";
        default: return "";
        }
    }

    /**
     * Metodo encargado de crear el Calendario que se imprimira en el mail.
     * @return calendar         Calendario de trabajo
     */
    public static Calendar obtenCalendario(){
        // Obteniendo GMT-08:00 (hora estandar pacifica)
         String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
         // Si no regresa ids , algo esta mal
         if (ids.length == 0){
             EIGlobal.mensajePorTrace("Algo esta mal", EIGlobal.NivelLog.ERROR);
        }

         // Creando el timeZone para la hora standard del pacifico
         SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);

         // Poniendo las reglas de cambio de horario
         pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
         pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

         // Creando un GregorianCalendar con las especificaciones del pacifico
         Calendar calendar = new GregorianCalendar(pdt);
         Date trialTime = new Date();
         calendar.setTime(trialTime);

         /** Imprimiendo cosas de Debug
         System.out.println("ERA: " + calendar.get(Calendar.ERA));
         System.out.println("YEAR: " + calendar.get(Calendar.YEAR));
         System.out.println("MONTH: " + calendar.get(Calendar.MONTH));
         System.out.println("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
         System.out.println("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
         System.out.println("DATE: " + calendar.get(Calendar.DATE));
         System.out.println("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
         System.out.println("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
         System.out.println("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
         System.out.println("DAY_OF_WEEK_IN_MONTH: " + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));
         System.out.println("AM_PM: " + calendar.get(Calendar.AM_PM));
         System.out.println("HOUR: " + calendar.get(Calendar.HOUR));
         System.out.println("HOUR_OF_DAY: " + calendar.get(Calendar.HOUR_OF_DAY));
         System.out.println("MINUTE: " + calendar.get(Calendar.MINUTE));
         System.out.println("SECOND: " + calendar.get(Calendar.SECOND));
         System.out.println("MILLISECOND: " + calendar.get(Calendar.MILLISECOND));
         System.out.println("ZONE_OFFSET: " + (calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000)));
         System.out.println("DST_OFFSET: " + (calendar.get(Calendar.DST_OFFSET)/(60*60*1000)));
         System.out.println("Current Time, with hour reset to 3");
         calendar.clear(Calendar.HOUR_OF_DAY); // so doesn't override
         calendar.set(Calendar.HOUR, 3);
         System.out.println("ERA: " + calendar.get(Calendar.ERA));
         System.out.println("YEAR: " + calendar.get(Calendar.YEAR));
         System.out.println("MONTH: " + calendar.get(Calendar.MONTH));
         System.out.println("WEEK_OF_YEAR: " + calendar.get(Calendar.WEEK_OF_YEAR));
         System.out.println("WEEK_OF_MONTH: " + calendar.get(Calendar.WEEK_OF_MONTH));
         System.out.println("DATE: " + calendar.get(Calendar.DATE));
         System.out.println("DAY_OF_MONTH: " + calendar.get(Calendar.DAY_OF_MONTH));
         System.out.println("DAY_OF_YEAR: " + calendar.get(Calendar.DAY_OF_YEAR));
         System.out.println("DAY_OF_WEEK: " + calendar.get(Calendar.DAY_OF_WEEK));
         System.out.println("DAY_OF_WEEK_IN_MONTH: " + calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH));
         System.out.println("AM_PM: " + calendar.get(Calendar.AM_PM));
         System.out.println("HOUR: " + calendar.get(Calendar.HOUR));
         System.out.println("HOUR_OF_DAY: " + calendar.get(Calendar.HOUR_OF_DAY));
         System.out.println("MINUTE: " + calendar.get(Calendar.MINUTE));
         System.out.println("SECOND: " + calendar.get(Calendar.SECOND));
         System.out.println("MILLISECOND: " + calendar.get(Calendar.MILLISECOND));
         System.out.println("ZONE_OFFSET: " + (calendar.get(Calendar.ZONE_OFFSET)/(60*60*1000))); // in hours
         System.out.println("DST_OFFSET: " + (calendar.get(Calendar.DST_OFFSET)/(60*60*1000))); // in hours
         */
         return calendar;
    }

    /**
     * Metodo encargado de obtener el valor parametrizado de activacion de
     * Cuentas
     * @return valorParam               Valor parametrizado de activacion
     */
    private int getValorParam(){
        ResultSet rs = null;
        String query = null;
        int valorParam = 0;
        Connection conn = null;
        Statement sDup = null;
        try{
            conn= createiASConn (Global.DATASOURCE_ORACLE );
            sDup = conn.createStatement();

            query  = "select nvl(param_enlace, 0) as valorParam"
                   + " from eweb_ctas_param"
                   + " where num_cuenta2 = '" + numContratoSinTrunc + "'";

            rs = sDup.executeQuery(query);

            if(rs.next()){
                valorParam = rs.getInt("valorParam");
            }

            rs.close();


            if(valorParam == 0) {
                query  = "select valor_entero as valorParam "
                       + "from eweb_parametros "
                       + "where nombre_param='tiempoCTA'";
                EIGlobal.mensajePorTrace ("EmailDAO::getMailContract:: -> Query:" + query, EIGlobal.NivelLog.DEBUG);
                rs = sDup.executeQuery(query);

                if(rs.next()){
                    valorParam = rs.getInt("valorParam");
                }
            }

            rs.close();
            sDup.close();
            conn.close();
        } catch(Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        } finally {
            try{
                if (rs != null) {
                    rs.close();
                }
                if (sDup != null) {
                    sDup.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }
        }
        return valorParam;
    }



    /**
     * Metodo encargado de generar el formato indicado para cada columna del
     * archivo
     *
     * @param linea                     Linea a formatear
     * @param col                       Numero de columna del archivo
     * @return LineaFinal               Linea ya formateada
     * @throws AdmonCtrlException       Error de tipo AdmonCtrlException
     */
    private String dameFormato(String linea, int col){
        String space = " ";
        if(col!=2){
            for(int i=linea.length(); i<25; i++){
                space += " ";
            }
            linea += space;
        }else{
            for(int i=linea.length(); i<50; i++){
                space += " ";
            }
            linea += space;
        }

        return linea;
    }

/* EVERIS 22/11/2007 - INICIA metodos para el armado del cuerpo */
/**
 * Metodo encargado de armar el tipo del mensaje de confirmacion
 * @return body con el cuerpo del mensaje a enviar
 */
    public StringBuffer msgTypeConfirmacion() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTypeConfirmacion", EIGlobal.NivelLog.DEBUG);
        StringBuffer body=msgTextConfirmacion();

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTypeConfirmacion", EIGlobal.NivelLog.DEBUG);
        return body;
    }

    /**
     *  Metodo que genera el cuerpo del mensaje de notificacion de cambio de mail
     * @return bodyNotificacion     Se refiere al texto dentro del mail
     */
    private StringBuffer msgTextConfirmacion() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextConfirmacion", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("EmailDetails::inicio mailNuevo" + mailNuevo, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("EmailDetails::inicio tipoDato" + tipoDato, EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Por este medio le informamos que en apego a la forma de notificaci�n reconocida por las partes en el Contrato de ENLACE con terminaci�n ");
        bodyNotificacion.append(TAG_ABRE_B);
        bodyNotificacion.append(this.numContrato.substring(this.numContrato.length() - 5, this.numContrato.length()));
        bodyNotificacion.append(TAG_CIERRA_B);
        bodyNotificacion.append(TAG_NOMBRE_DE);
        bodyNotificacion.append(TAG_ABRE_B);
        bodyNotificacion.append(this.descContrato);
        bodyNotificacion.append(", ");
        bodyNotificacion.append(TAG_CIERRA_B);
        bodyNotificacion.append("en virtud de la solicitud realizada a trav�s del uso de los medios de identificaci�n pactados entre las partes ");
        bodyNotificacion.append("en sustituci�n de la firma aut�grafa en el contrato se ha operado exitosamente el cambio de los datos personales ");
        bodyNotificacion.append("anteriormente registrados, conforme a lo siguiente: ");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Datos anteriores");
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<center>");
        bodyNotificacion.append(this.mailViejo);
        bodyNotificacion.append("</center>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Datos actuales a partir del d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<center>");
        bodyNotificacion.append(this.mailNuevo);
        bodyNotificacion.append("</center>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("En tal virtud se entender� por consentido el cambio respectivo por parte de EL CLIENTE y en caso de cambio de domicilio ");
        bodyNotificacion.append("EL BANCO tiene por convalidada la omisi�n del aviso por escrito con 30 d�as de anticipaci�n a que alude el contrato de referencia. ");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("En caso de no estar de acuerdo con los cambios indicados y de contar con facultades para ello dentro de ENLACE, ");
        bodyNotificacion.append("es necesario accesar al servicio en la opci�n de \"Administraci�n y Control\", ");
        bodyNotificacion.append("\"Mantenimiento de Datos Personales\" y cambiarlo nuevamente. ");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Con el objetivo de brindarle un mejor servicio, si por alguna raz�n usted no puede cambiar el correo, tambi�n ");
        bodyNotificacion.append("puede solicitar el cambio reenviando este correo a banca_electronica@santander.com.mx .");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 ");
        bodyNotificacion.append("o Lada sin costo 01 800 509 5000.");
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(" </BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextConfirmacion", EIGlobal.NivelLog.INFO);
        return bodyNotificacion;
    }

     /** Obtiene el numero del Contrato.
      * 
     * @return Valor del numero de contrato
     */
    public String getNumeroContrato () {
        return this.numContrato;
    }

    /**
     *Asigna el numero del contrato.
     * 
     * @param numContrato el numero de Contrato
     */
    public void setNumeroContrato (String numContrato) {
        this.numContrato = numContrato;
        //System.out.println("%%%%%% el valor del numero de contrato es = " + this.numContrato);
    }

    /** Obtiene la descripcion del contrado.
     * 
     * @return decripcion del contrato
     */
    public String getDescContrato () {
        return this.descContrato;
    }

    /**
     * Asigna la descripcion del contrato.
     * 
     * @param descContrato descripcion del contratro
     */
    public void setDescContrato (String descContrato) {
        this.descContrato = descContrato;
        //System.out.println("%%%%%% el valor del desc de contrato es = " + this.descContrato);
    }

    /** Asigna el mail nuevo.
     * 
     * @param mailNuevo mail nuevo
     */
    public void setMailNuevo (String mailNuevo) {
        this.mailNuevo = mailNuevo;
        //System.out.println("%%%%%% el valor del mailNuevo = " + this.mailNuevo);
    }

    /** Obtiene el mail nuevo.
     * 
     * @return valor del mail nuevo
     */
    public String getMailNuevo () {
        return this.mailNuevo;
    }

    /** Asigna  el mail viejo.
     * 
     * @param mailViejo el mail viejo
     */
    public void setMailViejo(String mailViejo) {
        this.mailViejo = mailViejo;
        //System.out.println("%%%%%% el valor del mailViejo = " + this.mailViejo);
    }

    /** Obtiene para el mail viejo.
     * 
     * @return valor del mail viejo
     */
      public String getMailViejo () {
        return this.mailViejo;
    }

    /** Obtiene el tipo de dato
     * @return valor del tipo de dato.
     */
    public String getTipoDato () {
        return this.tipoDato;
    }

    /** Asigna el tipo de Dato
     * @param tipoDato el tipo de dato
     */
    public void setTipoDato (String tipoDato) {
        this.tipoDato = tipoDato;
        //System.out.println("%%%%%% el valor del tipoDato = " + this.tipoDato);
    }
    /* EVERIS 22/11/2007 - FIN metodos para el armado del cuerpo del mail*/

    /* EVERIS 22/02/2008 - INICIA metodos para el armado del cuerpo para cuenta interbancaria */
    /**
     * Metodo encargado de armar el tipo del mensaje de confirmacion
     * @return body con el cuerpo del mensaje a enviar
     */
    public StringBuffer msgTypeConfirmacionInterb() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTypeConfirmacionInterb", EIGlobal.NivelLog.DEBUG);
        StringBuffer mailBody= new StringBuffer();
        String directionInterb = null;
        if(isAttachment()){
            directionInterb = "que se detallan en el archivo adjunto."
                      + BR
                      + BR;
        }else{
            String table = null;
            table = CADHTML
                  + TR
                  + "<td valign=\"top\">Numero de Cuenta</td>"
                  + "<td valign=\"top\">Descripci�n</td>";
              if(lotes!=null && !"".equals(lotes) && !TXT_NULL.equals(lotes) && !"0".equals(lotes)){
                  table += "<td valign=\"top\">Lote</td>";
              }
              table += DIAG_TR;
            for(int i=0; i<this.detalleCuentasInterb.size(); i++){
                table = table
                      + TR
                      + ETIQUETA_TD + ((EmailContainer)this.detalleCuentasInterb.get(i)).getNumCuenta() + ETIQUETA_CIERRA_TD
                      + ETIQUETA_TD + ((EmailContainer)this.detalleCuentasInterb.get(i)).getTitular() + ETIQUETA_CIERRA_TD;
                  if(lotes!=null && !"".equals(lotes) && !lotes.equals(TXT_NULL) && !"0".equals(lotes)){
                    table += ETIQUETA_TD + lotes + ETIQUETA_CIERRA_TD;
                  }
                  table += DIAG_TR;
            }
            table = table + "</table></center>";
            directionInterb = CONFORME_A_LO_SIGUIENTE
                  + BR
                  + BR
                  + table
                  + BR
                  + BR;
        }


            EIGlobal.mensajePorTrace("EmailDetails::Fin msgTypeConfirmacionInterb", EIGlobal.NivelLog.DEBUG);
            mailBody = msgTextConfirmacionInterb(directionInterb);
        return mailBody;
    }

    /**
     *  Metodo que genera el cuerpo del mensaje de notificacion de cambio de mail
     * @return bodyNotificacion     Se refiere al texto dentro del mail
     */
    private StringBuffer msgTextConfirmacionInterb(String directionInterb) {
            EIGlobal.mensajePorTrace("EmailsSender - msgTextConfirmacionInterb::inicio msgTextConfirmacionInterb", EIGlobal.NivelLog.DEBUG);
            StringBuffer body = new StringBuffer();
            int valParam = getValorParam();
            body.append("<HTML>");
            body.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
            body.append("<BODY> ");
            body.append("<FONT FACE=\"arial\" SIZE=3 COLOR=black>");
            body.append("Por este medio le informamos que el d�a <B>");
            body.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
            body.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
            body.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR));
            body.append("</B>, se ha solicitado el registro de alta de empleados con cuentas interbancarias dentro de su contrato ENLACE con terminaci�n ");
            body.append(TAG_ABRE_B);
            EIGlobal.mensajePorTrace("EmailsSender - msgTextConfirmacionInterb::inicio Esto vale el contrato" + numContrato, EIGlobal.NivelLog.DEBUG);
            body.append(this.numContrato.substring(this.numContrato.length() - 5, this.numContrato.length()));
            body.append(TAG_CIERRA_B);
            body.append(TAG_NOMBRE_DE);
            body.append(TAG_ABRE_B);
            EIGlobal.mensajePorTrace("EmailsSender - msgTextConfirmacionInterb::inicio Esto vale el contrato" + descContrato, EIGlobal.NivelLog.DEBUG);
            body.append(descContrato);
            body.append(", ");
            body.append(TAG_CIERRA_B);
            body.append(directionInterb);
            body.append("La solicitud se encuentra actualmente con el estatus de \"Pendiente por Activar\". ");
            body.append("El estatus de la solicitud cambiar� a \"Activa\" dentro de los ");
            body.append(valParam);
            body.append(" minutos siguientes.");
            body.append(BR);
            body.append(BR);
            body.append("En caso de no estar de acuerdo con esta solicitud de alta de cuentas interbancarias y de contar con facultades para dar de baja cuentas ");
            body.append("dentro de ENLACE, es necesario accesar al servicio en la opci�n \"Servicios\", \"N�mina Interbancaria\", \"Registro de Cuentas\", \"Baja\" y llevar a ");
            body.append("cabo la baja respectiva.");
            body.append(BR);
            body.append(BR);
            body.append("Con el objetivo de brindarle un mejor servicio, si por alguna raz�n usted no puede realizar la baja de alguna cuenta o ");
            body.append("solicitud desde ENLACE, tambi�n puede solicitar la baja reenviando este correo a banca_electronica@santander.com.mx .");
            body.append(BR);
            body.append(BR);
            body.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 ");
            body.append("o Lada sin costo 01 800 509 5000.");
            body.append(" </BODY></HTML> ");
            return body;
    }
    /* EVERIS 22/02/2008 - FIN metodos para el armado del cuerpo del mail*/








    public StringBuffer msgTextNotificacionTransferenciasMas() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionTransferenciasMas", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace ")
            .append(obtenSubCadena(this.numContrato, 4) )
            .append(TAG_NOMBRE_DE)
            .append(TAG_ABRE_B)
            .append(this.razonSocial)
            .append("</B> se realiz� la transferencia masiva ");

        if(this.esPAGT){
            bodyNotificacion.append("<B> Pago de Tarjeta de Cr�dito </B>");
        }
        bodyNotificacion.append("con folio(s) <B>").append(this.numRef).append("</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");

        if(this.tipoTransferenciaUni != null){
            if(this.tipoTransferenciaUni.equals(" transferencia Multiple ")){
                bodyNotificacion.append("<tr><td>N�mero de Cuenta Cargo</td><td><B>").append(obtenSubCadena(this.numCuentaCargo, 4)).append("</B></td></tr>");
            }
        }

        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;")
                .append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;")
            .append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");

        if(this.esMayorA100Reg){
            bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de EN PROCESO (Utilice el n�mero de Folio para consultar las referencias y estado de las transferencias, en Seguimiento de Transferencias). ");
        }else{
            bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de ")
                .append(this.estatusActual).append(". ");
        }
        bodyNotificacion.append(" Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciasMas", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionAutCancOperManc(java.util.ArrayList listaNotifica) {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAutCancOperManc", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4)).append("</B> a nombre de <B>")
            .append(this.razonSocial).append("</B> se realiz� una <B>");
        bodyNotificacion.append(this.tipoOperacion).append("</B> de operaciones mancomunadas conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");


        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Total de Operaciones Nacionales&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;")
            .append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td><B>&nbsp;&nbsp;Producto&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;Importe&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;Estatus&nbsp;&nbsp;</B></td></tr>");


        String operacion="";
        String cuentaCargo ="";
        String importe = "";
        String codErrorOper="";
        String estatus = "";
        //itera la lista de operaciones a notificar
        for (int j = 1; j < listaNotifica.size(); j++) {
            EIGlobal.mensajePorTrace(j+"----codErrorOper-->" + codErrorOper, EIGlobal.NivelLog.DEBUG);
                    estatus = "Autorizada";//emailSender.obtenDescripcionEstatus(codErrorOper);

                        EIGlobal.mensajePorTrace("----elemento a notificar-->" + listaNotifica.get(j).toString(), EIGlobal.NivelLog.DEBUG);

                        operacion = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 5);
                        EIGlobal.mensajePorTrace("----operacion-->" + operacion, EIGlobal.NivelLog.DEBUG);
                        operacion = DatosManc.Description(operacion);
                        EIGlobal.mensajePorTrace("----operacion-->" + operacion, EIGlobal.NivelLog.DEBUG);
                        cuentaCargo = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 6);
                        EIGlobal.mensajePorTrace("----cuentaCargo-->" + cuentaCargo, EIGlobal.NivelLog.DEBUG);
                        importe = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 7);
                        EIGlobal.mensajePorTrace("----importe-->" + importe, EIGlobal.NivelLog.DEBUG);
                        codErrorOper = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 2);

                        EIGlobal.mensajePorTrace("----codErrorOper-->" + codErrorOper, EIGlobal.NivelLog.DEBUG);
                    if(codErrorOper!=null && codErrorOper.contains(COD_ERROR))
                        {
                        EIGlobal.mensajePorTrace("----estatus-->" + estatus, EIGlobal.NivelLog.DEBUG);
                        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;"+operacion+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+obtenSubCadena(cuentaCargo, 4)+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+ValidaOTP.formatoNumero(importe)+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+estatus+"&nbsp;&nbsp;</td></tr>");

                        }
                    EIGlobal.mensajePorTrace("----termino iterrar listaNotifica-->", EIGlobal.NivelLog.DEBUG);
                    }

        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAutCancOperManc", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }
    
    /**
     * Metodo que lanza el mensaje de notificacion automatica.
     * 
     * @param listaNotifica lista de notificaciones automaticas
     * @return Cuerpo de la notificacion
     */
    public StringBuffer msgTextNotificacionAutCancOperMancInter(java.util.ArrayList listaNotifica) {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAutCancOperManc", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4))
            .append("</B> a nombre de <B>")
            .append(this.razonSocial).append("</B> se realiz� una <B>");
        bodyNotificacion.append(this.tipoOperacion).append( "</B> de operaciones mancomunadas conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");


        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Total de Operaciones Internacionales&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;")
                .append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td><B>&nbsp;&nbsp;Producto&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;Importe&nbsp;&nbsp;</B></td><td><B>&nbsp;&nbsp;Estatus&nbsp;&nbsp;</B></td></tr>");


        String operacion="";
        String cuentaCargo ="";
        String codErrorOper="";
        String estatus = "";
        String divisa="";
        String importe;
        //itera la lista de operaciones a notificar
        for (int j = 1; j < listaNotifica.size(); j++) {

                              EIGlobal.mensajePorTrace("----elemento a notificar-->" + listaNotifica.get(j).toString(), EIGlobal.NivelLog.ERROR);
                                estatus = "Autorizada";//emailSender.obtenDescripcionEstatus(codErrorOper);
                                operacion = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 5);
                                operacion = DatosManc.Description(operacion);
                                cuentaCargo = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 6);
                                divisa=ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 8);
                                if(divisa!=null){
                                    divisa=divisa.trim();
                                }else{
                                    divisa="";
                                }
                                importe = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 7);
                                codErrorOper = ValidaOTP.cortaCadena(listaNotifica.get(j).toString(),'/', 2);
                                EIGlobal.mensajePorTrace(j+"----codErrorOper-->" + codErrorOper, EIGlobal.NivelLog.DEBUG);

                            if(codErrorOper!=null && codErrorOper.contains(COD_ERROR))
                                    {
                                bodyNotificacion.append("<tr><td>&nbsp;&nbsp;"+operacion+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+obtenSubCadena(cuentaCargo, 4)+"&nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+ValidaOTP.formatoNumero(importe)+" "+divisa+" &nbsp;&nbsp;</td><td>&nbsp;&nbsp;"+estatus+"&nbsp;&nbsp;</td></tr>");
                                    }
                    }

        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAutCancOperMancInter", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Notificacion del pago de nomina
     * 
     * @return cuerpo de la notificacion.
     */
    public StringBuffer msgTextNotificacionPagoNomina() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(this.numContrato.substring((this.numContrato.length())-4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se realiz� el env�o de su archivo de Pago de N�mina <B>");
        bodyNotificacion.append(this.tipoPagoNomina).append("</B> con folio(s) <B>").append(this.numRef).append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numCuentaCargo.substring((this.numCuentaCargo.length())-4)).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numTransmision).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>El sistema se encuentra validando sus datos y una vez que concluya puede revisar el estatus de la dispersi�n ingresando a la ruta: ");

        if(this.tipoPagoNomina!=null && this.tipoPagoNomina.equals("Interbancaria")){
            bodyNotificacion.append(" Servicios / N�mina Interbancaria / Pagos. ");
        }else{
            if(this.tipoPagoNomina!=null && (this.tipoPagoNomina.contains("Inmediata"))){
                bodyNotificacion.append(" Servicios / Tarjeta de Pagos Santander  / Pagos / Consulta. ");//YHG NPRE
            }else{
                bodyNotificacion.append(" Servicios / N�mina  / Pagos / Consulta. ");
            }
        }

        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    public StringBuffer msgTextNotificacionPagoNominaPre() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se realiz� el el env�o de su archivo de Pago de <B>");
        bodyNotificacion.append(this.tipoPagoNomina).append("</B> con folio(s) <B>").append(this.numRef).append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(obtenSubCadena(this.numCuentaCargo, 4)).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numTransmision).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>El sistema se encuentra validando sus datos y una vez que concluya puede revisar el estatus de la dispersi�n ingresando a la ruta: ");
        bodyNotificacion.append(" Servicios/ Tarjeta de Pagos Santander / Pagos / Consulta. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoNomina", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    public StringBuffer msgTextNotificacionPagoImp() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoImp", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(this.numContrato.substring((this.numContrato.length())-4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se realiz� el Pago de <B>").append(this.tipoPagoImp);
        bodyNotificacion.append("</B> con folio(s) ").append(this.numRef).append(" conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numCuentaCargo.substring((this.numCuentaCargo.length())-4)).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de ").append(this.estatusActual).append(". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoImp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    //Sipare 07/2012
    public StringBuffer msgTextNotificacionPagoImpLC() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoImpLC", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace con terminaci�n <B>").append(this.numContrato.substring((this.numContrato.length())-4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se envi� un Pago de <B>").append(this.tipoPagoImp);
        bodyNotificacion.append("</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;L�nea de Captura&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numCuentaCargo).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Estatus&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.estatusActual).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoImpLC", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }



    public StringBuffer msgTextNotificacionPagoSUALineaCaptura() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoSUA", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD>");
        bodyNotificacion.append("<BODY>");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(this.numContrato.substring((this.numContrato.length())-4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se acaba de capturar una operaci�n de Pago de <B>").append(this.tipoPagoImp);
        bodyNotificacion.append("</B> con folio(s) ").append(this.numRef).append(" conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numCuentaCargo.substring((this.numCuentaCargo.length())-4)).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de ").append(this.estatusActual).append(". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML>");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoImp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }



    public StringBuffer msgTextNotificacionPagoProv() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoProv", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");


        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(this.numContrato.substring((this.numContrato.length())-4)).append("</B> a nombre de <B>").append(this.razonSocial).append("</B> se realiz� el env�o de su archivo de Pago a Proveedores <B>").append(this.tipoPagoProveedor);
        bodyNotificacion.append("</B> con folio(s) <B>").append(this.numRef).append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numCuentaCargo.substring((this.numCuentaCargo.length())-4)).append("</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numTransmision).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.numRegImportados).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;").append(this.impTotal).append("&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de ").append(this.estatusActual).append(". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoProv", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionAltaProv() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAltaProv", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE+ obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado el registro de Alta de Proveedores dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+this.numContrato.substring((this.numContrato.length())-4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, con el n�mero de Alta Masiva <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");

        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numTransmision+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");

        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusActual+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaProv", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionTransferenciasUni() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial+"</B> se acaba de realizar la transferencia <B>"+this.tipoTransferenciaUni);
        bodyNotificacion.append("</B> con folio(s) <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.numCuentaCargo, 4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Destino&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.cuentaDestino, 4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.impTotal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusActual+". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * msgTextNotificacionPagoMicrositio
     * Detalle de Notificacion de Pagos desde el Micrositio
     * JGAL
     * 30/07/2013
     * @return
     */

    public StringBuffer msgTextNotificacionPagoMicrositio() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoMicrositio", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" +
                obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace con terminaci�n <B>" +
                obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+
                this.razonSocial);
        if("0093".equals(this.convenio.trim())) {
            bodyNotificacion.append("</B> se realiz� un Pago Referenciado SAT por Micrositio Enlace ");
        } else {
            bodyNotificacion.append("</B> se realiz� un Pago por Micrositio Enlace ");
        }
        bodyNotificacion.append(" al convenio <B>" + this.convenio +" </B> a nombre de <B>" +
                this.nombreConvenio + "</B>, por un importe de <B>" + this.impTotal);
        bodyNotificacion.append("</B> con n�mero de referencia <B>" + this.numRef +
                "</B> <br><br>");

        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el " +
                "estatus de " + this.estatusActual+ ". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a " +
                "S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo " +
                "01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Atentamente:<br><br>");
        bodyNotificacion.append("<b>Enlace</b><br>");
        bodyNotificacion.append("</font></P><br><P ALIGN=\"JUSTIFY\"><FONT SIZE=\"2\">");
        bodyNotificacion.append("******************PRIVACIDAD DE ESTE MENSAJE********************** Este mensaje est&aacute; dirigido exclusivamente ");
        bodyNotificacion.append("a las personas que tienen las direcciones de correo electr&oacute;nico especificadas en los destinatarios dentro ");
        bodyNotificacion.append("de su encabezado. Si por error usted ha recibido este mensaje, por ning&uacute;n motivo debe revelar su contenido, ");
        bodyNotificacion.append("copiarlo, distribuirlo o utilizarlo. Le solicitamos por favor comunique del error a la direcci&oacute;n de correo ");
        bodyNotificacion.append("electr&oacute;nico remitente y elimine dicho mensaje junto con cualquier documento adjunto que pudiera contener. ");
        bodyNotificacion.append("Los derechos de privacidad y confidencialidad de la informaci&oacute;n en este mensaje no deben perderse por el hecho ");
        bodyNotificacion.append("de haberse trasmitido err&oacute;neamente o por causas de interferencias en el funcionamiento de los sistemas de ");
        bodyNotificacion.append("correo y canales de comunicaci&oacute;n. Toda opini&oacute;n que se expresa en este mensaje pertenece a la persona ");
        bodyNotificacion.append("remitente por lo que no debe entenderse necesariamente como una opini&oacute;n del Grupo Financiero Santander y/o ");
        bodyNotificacion.append("de las entidades que lo integran, a menos que el remitente este autorizado para hacerlo o expresamente lo diga en el ");
        bodyNotificacion.append("mismo mensaje. En consideraci&oacute;n a que los mensajes enviados de manera electr&oacute;nica pueden ser interceptados ");
        bodyNotificacion.append("y manipulados, el Grupo Financiero Santander y las entidades que lo integran no se hacen responsables si los mensajes ");
        bodyNotificacion.append("llegan con demora, incompletos, eliminados o con alg&uacute;n programa malicioso denominado como virus inform&aacute;tico. ");
        bodyNotificacion.append("Este mensaje no debe interpretarse, por ning&uacute;n motivo como una oferta de venta o de compra de valores ni de ");
        bodyNotificacion.append("instrumentos financieros relacionados. Los acentos en la leyenda de confidencialidad se han suprimido para una mejor lectura.");

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionPagoMicrositio", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * msgTextNotificacionPAGOOCURRE
     * PAGO OCURRE FISICA Y MORAL
     * MAAM
     * 10/07/2010
     * @return
     */
    public StringBuffer msgTextNotificacionPagoOcurre() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionPagoOcurre", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));//MAAM 12/JUL/2010
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial+"</B> se liber&oacute;(aron) <B> &oacute;rden(es) de pago(s) Ocurre </B> con folio(s) <B>"+this.numRef+" </B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Ordenes Pago ocurre&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.impTotal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusActual+". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * msgTextNotificacionManGenFolio
     * mancomunidad: geenracion de folio
     * 10/07/2010
     * @return
     */

    public StringBuffer msgTextNotificacionManGenFolio() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+this.numContrato.substring((this.numContrato.length())-4)+"</B> a nombre de <B>"+this.razonSocial+"</B> se acaba de realizar la generaci�n del folio <B>"+this.numRef);
        bodyNotificacion.append("</B> para la operaci�n <B>"+Description(this.tipoOperacion)+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numCuentaCargo.substring((this.numCuentaCargo.length())-4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.impTotal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusFinal+". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionCuentas() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCuentas", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        String[] detalleAltaCuentas = this.getDetalle_Alta().split(",");

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);

        bodyNotificacion.append(", se ha solicitado el registro de <B>" + this.tipoOpCuenta + " DE CUENTAS</B> dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, con el n�mero de "+ this.tipoOpCuenta +" Masiva <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");

        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Enviados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaCuentas[0] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizaci�n Pendiente&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaCuentas[1] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Rechazados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaCuentas[2] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");

        bodyNotificacion.append("<B>El estatus de las cuentas enviadas puede consultarlo ingresando a <B>Consultas/ Bit�cora</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCuentas", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionCambioPaswd() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCambioPaswd", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado Cambio de Contrase�a dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial+"</B>.<br><br>");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCambioPaswd", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * msgTextNotificacionAsignacionTarjeta
     * Asignacion de tarjeta
     * 10/07/2010
     * @return
     */
    public StringBuffer msgTextNotificacionAsignacionTarjeta() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAsignacionTarjeta", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial+"</B> se acaba de realizar la asignaci�n de ");
        bodyNotificacion.append(TAG_ABRE_B+this.numRegImportados+"</B> tarjetas, con folio <B>"+this.numRef+"</B><br><br>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusActual+". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciasUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    public StringBuffer msgTextNotificacionAltaEmp() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAltaEmp", EIGlobal.NivelLog.INFO);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado el registro de Alta de Empleados dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, con el n�mero de Alta Masiva <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");

        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Transmisi�n&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numTransmision+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");

        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusActual+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaEmp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }



    public StringBuffer msgTextNotificacionCambioDivisas() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCambioDivisas", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado el Cambio de Divisas(Transferencia) dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B> con folio(s) <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");

        String[] importes;
        if( this.impTotal != null && this.impTotal != "" ){
            importes = this.impTotal.split("-");
        } else {

            importes = new String [2];

            importes[0] = "0";
            importes[1] = "0";
        }

        if( this.numRegImportados > 1 ){

            bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + this.numRegImportados + "&nbsp;&nbsp;</B></td></tr>");


        } else {

            bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + obtenSubCadena(this.numCuentaCargo, 4) + "&nbsp;&nbsp;</B></td></tr>");
            bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Abono&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + obtenSubCadena(this.cuentaDestino, 4)+ "&nbsp;&nbsp;</B></td></tr>");
        }

        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Dlls.&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + importes[0] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe M.N.&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + importes[1] + "&nbsp;&nbsp;</B></td></tr>");

        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusActual+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaEmp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }


    /**
     * Metodo para armar el mensaje para Modificacion de Empleados por Archivo
     * @return
     */
    public StringBuffer msgTextNotificacionModEmp() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionModEmp", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado la Modificaci�n de Empleados dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Referencia&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRef+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusFinal+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionModEmp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para armar el mensaje para Baja de Empleados por Archivo
     * @return
     */
    public StringBuffer msgTextNotificacionBajaEmp() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionBajaEmp", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado la Baja de Empleados dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Referencia&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRef+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusFinal+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionBajaEmp", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para armar el mensaje para Credito Prepago
     * @return
     */
    public StringBuffer msgTextNotificacionCreditoPrepago() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCreditoPrepago", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial+"</B> se realiz� el Prepago de Cr�dito <B>"); //YHG NPRE
        bodyNotificacion.append("</B> con folio(s) <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Cuenta Cargo&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.numCuentaCargo, 4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.impTotal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusActual+". ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCreditoPrepago", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para armar el mensaje para Modificacion de Empleados en Linea
     * @return
     */
    public StringBuffer msgTextNotificacionModEmpUni() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionModEmpUni", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado la Modificaci�n de Empleados dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Referencia&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRef+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Cuenta Modificada&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.numCuentaCargo, 4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusFinal+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionModEmpUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para armar el mensaje para Baja de Empleados en Linea
     * @return
     */
    public StringBuffer msgTextNotificacionBajaEmpUni() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionBajaEmpUni", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado la Baja de Empleados dentro de ");
        bodyNotificacion.append("su contrato Enlace con terminaci�n <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B>, conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Referencia&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRef+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Cuenta dada de Baja&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.numCuentaCargo, 4)+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La solicitud se encuentra actualmente con el estatus de <B>"+this.estatusFinal+"</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionBajaEmpUni", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

        public StringBuffer msgTextNotificacionAdminUsuarios() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B> se realiz� el <B>"+tipoOperacion.toUpperCase()+" DE USUARIO </B>, con folio <B>" + this.numTransmision + "</B> ");
        bodyNotificacion.append("conforme a lo siguiente:<br><br>");

        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td><LEFT>&nbsp;&nbsp;C�digo de Cliente &nbsp;</td><td><B>&nbsp;&nbsp;"+obtenSubCadena(this.numRef, 4)+"&nbsp;&nbsp;</B></td></tr>");
        if (this.estatusActual.equals("Aceptada")){
            this.estatusActual = "X";
            this.estatusFinal = "";
        } else {
            this.estatusActual = "";
            this.estatusFinal = "X";
        }
        bodyNotificacion.append("<tr><td><LEFT>&nbsp;&nbsp;N�mero de Registro Aceptado &nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.estatusActual+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td><LEFT>&nbsp;&nbsp;N�mero de Registro Rechazado &nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.estatusFinal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");

        bodyNotificacion.append("<B>El estatus de los usuarios enviados puede consultarlo ingresando a Administraci�n y Control / Usuarios. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    public StringBuffer msgTextNotificacionLyM() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
/*      bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append("</B> de <B>" + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append("</B> de <B>" + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha realizado una modificacion en limites y montos " + tipo_operacion + " dentro de ");*/

        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        if (!tipoOperacion.trim().equals("")) {
            bodyNotificacion.append("</B> se realiz� el <B> ESTABLECIMIENTO DE LIMITES Y MONTOS "+ tipoOperacion +"</B>, con folio <B>" + this.numTransmision + "</B> <br><br>");
        } else {
            bodyNotificacion.append("</B> se realiz� el <B> ESTABLECIMIENTO DE LIMITES Y MONTOS</B>, con folio(s) <B>" + this.numTransmision + "</B> ");
            bodyNotificacion.append("conforme a lo siguiente: <br><br>");
            //Crea encabezado de tabla
            bodyNotificacion.append("<CENTER><TABLE border=1>");
            bodyNotificacion.append("<tr><td>&nbsp;&nbsp; USUARIO &nbsp;</td><td>&nbsp;&nbsp; PRODUCTO(S) &nbsp;&nbsp;</td><td>&nbsp;&nbsp; MONTO &nbsp;&nbsp;</td></tr>");

            //Itera para mostrar el detalle de las operaciones
            for (LimitesOperacion operacion : this.listaOperLYM) {
                bodyNotificacion.append("<tr><td>&nbsp;&nbsp;" + operacion.getUsuario()+ "&nbsp;</td>" +
                        ABRETDS + operacion.getDescripcionOperacion() + CIERRATDS +
                        ABRETDS + operacion.getLimiteOperacion() + "&nbsp;&nbsp;</td></tr>");
            }
            bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        }

        bodyNotificacion.append("<B>El estatus de las modificaciones enviadas puede consultarlo ingresando a Administraci�n y Control / " +
                "Administraci�n Super Usuario / Establecimiento de L�mites y Montos. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para realizar la notificaci�n por email de autorizaci�n
     * de beneficiarios pago ocurre
     *
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextNotificacionAutBenefPagOcurre() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAutBenefPagOcurre", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        String[] detalleAltaBenefOcurre = this.getDetalle_Alta().split(",");

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Estimado Cliente");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);

        bodyNotificacion.append(", en su su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append(" se realizo el registro de beneficiarios " + this.numRegImportados + " para el producto de Pago Ocurre ");
        bodyNotificacion.append("con folio(s) <B>" + this.numRef + "</B> conforme a lo siguiente:<br><br>");

        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Enviados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[0] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizaci�n Pendiente&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[1] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Rechazados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[2] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");

        bodyNotificacion.append("<B>El estatus de los beneficiarios enviados puede consultarlo ingresando a <B>Transferencias/ Ordenes de Pago Ocurre</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAutBenefPagOcurre", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para realizar la notificaci�n por email de autorizaci�n
     * de beneficiarios pago directo
     *
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextNotificacionAutBenefPagDirecto() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAutBenefPagDirecto", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        String[] detalleAltaBenefOcurre = this.getDetalle_Alta().split(",");

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append("Estimado Cliente");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);

        bodyNotificacion.append(", en su su contrato Enlace con terminaci�n <B>"+obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append(" se realizo el registro de beneficiarios " + this.numRegImportados + " para el producto de Pago Directo ");
        bodyNotificacion.append("con folio(s) <B>" + this.numRef + "</B> conforme a lo siguiente:<br><br>");

        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Enviados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[0] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Autorizaci�n Pendiente&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[1] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Rechazados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;" + detalleAltaBenefOcurre[2] + "&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");

        bodyNotificacion.append("<B>El estatus de los beneficiarios enviados puede consultarlo ingresando a <B>Servicios/ Pago Directo/ Mantenimiento de Beneficiarios</B>. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);

        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAutBenefPagDirecto", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para realizar la notificaci�n por email de una
     * transferencia reuters
     *
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextNotificacionTransferenciaReuters() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionTransferenciaReuters", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace "+obtenSubCadena(this.numContrato, 4)+ TAG_NOMBRE_DE + TAG_ABRE_B +this.razonSocial+"</B> se realiz� la transferencia ");
        bodyNotificacion.append("<B> FX Online </B>");
        bodyNotificacion.append("con folio(s) <B>"+this.numRef+"</B> conforme a lo siguiente:<br><br>");
        bodyNotificacion.append("<CENTER><TABLE border = 1>");
        bodyNotificacion.append("<tr><td>N�mero de Cuenta Cargo</td><td><B>"+obtenSubCadena(this.numCuentaCargo, 4)+"</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Importados&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.numRegImportados+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;Importe Total&nbsp;&nbsp;</td><td><B>&nbsp;&nbsp;"+this.impTotal+"&nbsp;&nbsp;</B></td></tr>");
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("<B>La operaci�n se encuentra actualmente con el estatus de "+this.estatusActual+", este es el detalle del alta.<BR><BR> ");
        bodyNotificacion.append(this.detalleAlta);
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append(" Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionTransferenciaReuters", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para formar el cuerpo del correo asignacion de tarjetas
     * @return StringBuffer mensaje de cuerpo de correo para Asignacion de tarjetas
     */
    public StringBuffer msgTextAsignacionTarjetaEmpleado() {
        EIGlobal
                .mensajePorTrace(
                        "EmailsSender - msgTextAsignacionTarjetaEmpleado::inicio msgTextAsignacionTarjetaEmpleado",
                        EIGlobal.NivelLog.DEBUG);
        StringBuffer body;
        body = new StringBuffer();
        StringBuffer tableDirec;
        tableDirec = new StringBuffer();
        String direction = null;
        tableDirec.append(CADHTML);
        tableDirec.append(TR);
        tableDirec.append("<td valign=\"top\">Nombre</td>");
        tableDirec.append("<td valign=\"top\">N�mero de Cuenta</td>");
        tableDirec.append(DIAG_TR);

        for (String nombre : this.nombres) {
            tableDirec.append("<tr><td align='center'>" + nombre
                  + "</td><td align='center'>En tr�mite</td></tr>");
        }

        //System.out.println("EmailDetails :: Saliendo de ciclo");
        tableDirec.append("</table></center>");
        //System.out.println("EmailDetails :: Termina creacion de tabla");
        direction = CONFORME_A_LO_SIGUIENTE
                  + BR
                  + BR
                  + tableDirec.toString()
                  + BR
                  + BR;

        body.append("<HTML>");
        body.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        body.append("<BODY> ");
        body.append("<FONT FACE=\"arial\" SIZE=3 COLOR=black>");

        body.append("Por este medio le informamos que el d�a <B>"
                + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        body.append(TAG_NEGRITA_DE
                + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        body.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR));
        body
                .append("</B>, se ha solicitado el Alta de empleados de Cuenta Inmediata dentro de su contrato ENLACE con terminaci�n ");
        body.append(TAG_ABRE_B);
        EIGlobal.mensajePorTrace(
                "EmailsSender - msgTextConfirmacionInterb::inicio Esto vale el contrato"
                        + numContrato, EIGlobal.NivelLog.DEBUG);
        body.append(this.numContrato
                .substring(this.numContrato.length() - 5, this.numContrato.length()));
        body.append(TAG_CIERRA_B);
        body.append(TAG_NOMBRE_DE);
        body.append(TAG_ABRE_B);
        EIGlobal.mensajePorTrace(
                "EmailsSender - msgTextConfirmacionInterb::inicio Esto vale el contrato"
                        + descContrato, EIGlobal.NivelLog.DEBUG);
        body.append(this.descContrato + ", y con numero de secuencia " + secuencia
                + TAG_CIERRA_B + TAG_COMA_ESPACIO);
        body.append(direction);


        body
                .append("La solicitud se encuentra actualmente en proceso y se podr� realizar dispersi�n a los empleados a partir del pr�ximo d�a h�bil.");
        body.append(BR);
        body.append(BR);
        body
                .append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.");
        body.append(BR);
        body.append(BR);

        body.append(" </BODY></HTML> ");
        return body;
    }
    /**
     * Metodo que genera el cuerpo del correo para el alta de ctas nivel 1
     * @return StringBuffer cuerpo correo
     */
    public StringBuffer msgTextAltaCtaNivel1() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);
        StringBuffer bodyNotificacion;
        bodyNotificacion = new StringBuffer();
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH) + TAG_CIERRA_B);
        bodyNotificacion.append(" de <B>" + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)) + TAG_CIERRA_B);
        bodyNotificacion.append(" de <B>" + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado el registro de <B>VINCULACION DE TARJETAS</B> ");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append("dentro de su contrato Enlace con terminaci�n <B>"+ obtenSubCadena(this.numContrato, 4)+"</B> a nombre de <B>"+this.razonSocial);
        bodyNotificacion.append("</B> con el n�mero de carga masiva <B>XXX</B>, conforme a lo siguiente:");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Enviados&nbsp;</td>" +
                    ABRETDS + this.regVincTarj[0] + CIERRATDS);
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Activaci�n Pendiente&nbsp;</td>" +
                    ABRETDS + this.regVincTarj[1] + CIERRATDS);
        bodyNotificacion.append("<tr><td>&nbsp;&nbsp;N�mero de Registros Rechazados&nbsp;</td>" +
                    ABRETDS + this.regVincTarj[2] + CIERRATDS);
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append("El estatus de las tarjetas enviadas puede consultarlo ingresando a  <B>Servicios / Tarjeta de Pagos Santander " +
                "/ Vinculaci�n tarjeta - contrato / Consulta y Baja</B>.");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 " +
                "o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");
        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionAltaUsuarios", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para realizar la notificaci�n por email de una
     * transferencia reuters
     *
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextBloqTokenIntFallidos() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextBloqueoTokenIntentos", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" + obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE + obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", su usuario con terminaci�n "+obtenSubCadena(this.numContrato, 2)+" </B> a nombre de <B>"+this.razonSocial+"</B> se realiz� el bloqueo del token por Intentos Fallidos: ");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append(" <B>Para desbloquearlo, puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000 o acudir a cualquiera de nuestras sucursales para solicitar un Token nuevo.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextBloqueoTokenIntentos", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para armar el mensaje de texto empleado en la notificacion de la
     * desvinculacion de dispositivos.
     *
     * @return StringBuffer con texto de la notificacion.
     */
    public StringBuffer msgTextNotificacionDesvinculacion() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionDesvinculacion", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<p align='justify' >");
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>");
        bodyNotificacion.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(" </B> de <B>");
        bodyNotificacion.append(obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append("</B> , su usuario con terminac�on  <B>");
        bodyNotificacion.append( this.usuario.substring(6,8));
        bodyNotificacion.append("</b> a nombre de <B>");
        bodyNotificacion.append(this.razonSocial);
        bodyNotificacion.append("</b> se realiz� la ");
        bodyNotificacion.append("Desvinculaci�n de Dispositivos, por lo que le pedimos que acceda nuevamente al sistema con el nuevo dispositivo para poder nuevamente enrolarlo. <br> ");
        bodyNotificacion.append("<br> Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente, ");
        bodyNotificacion.append("contrase�a de Enlace y contrase�as din�micas generadas por su Token.<B> Para cualquier duda puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o  ");
        bodyNotificacion.append(LADA_SIN_COSTO);
        bodyNotificacion.append("</p>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionDesvinculacion", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * @return StringBuffer : mensaje de correo para cambio imagen
     */
    public StringBuffer msgTextNotificacionCambioImag() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCambioImagen", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<p align='justify' >");
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" );
        bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append("</B> de <B> " );
        bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append( "</B>, su usuario con terminaci�n <B> ");
        bodyNotificacion.append(this.usuario.substring(6,8));
        bodyNotificacion.append("</b> a nombre de <B>");
        bodyNotificacion.append(this.razonSocial);
        bodyNotificacion.append("</b> se realiz� la asignaci�n ");
        bodyNotificacion.append("de una Imagen que le permita dar mayor seguridad a las operaciones que haga en la Banca Electr�nica. <br/> </p> ");
        bodyNotificacion.append("<br><p>Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente,  <br>");
        bodyNotificacion.append("contrase�a de Enlace y contrase�as din�micas generadas por su Token.<B> Para cualquier duda puede comunicarse a S�per L�nea  <br>");
        bodyNotificacion.append("Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B><br>");
        bodyNotificacion.append(" </p>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");

        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionDesvinculacion", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * @return StringBuffer : mensaje de correo para cambio pregunta
     */
    public StringBuffer  msgTextNotificacionEnrolamientoRSA() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionEnrolamiento", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<p align='justify'>");
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" );
        bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(" </B> de <B>");
        bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append( "</B>, su usuario con terminaci�n <B> ");
        bodyNotificacion.append(this.usuario.substring(6,8));
        bodyNotificacion.append("</b> a nombre de <B>");
        bodyNotificacion.append(this.razonSocial);
        bodyNotificacion.append("</b> se realiz� ");
        bodyNotificacion.append("el enrolamiento de sus dispositivos electr�nicos donde opera su Banca Electr�nica. <br/> ");
        bodyNotificacion.append("<br>Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente,  <br>");
        bodyNotificacion.append("contrase�a de Enlace y contrase�as din�micas generadas por su Token. <B>Para cualquier duda puede comunicarse a S�per L�nea  <br>");
        bodyNotificacion.append("Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B><br>");
        bodyNotificacion.append("</p> ");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCambioPreg", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * @return StringBuffer : mensaje de correo para enrolamiento
     */
    public StringBuffer msgTextNotificacionCambioPreg() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCambioPregunta", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();


        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        //bodyNotificacion.append("Estimado Cliente:");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("<p align='justify' >");
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>" );
        bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append("</B> de <B> " );
        bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append( "</B>, su usuario con terminaci�n <B> ");
        bodyNotificacion.append(this.usuario.substring(6,8));
        bodyNotificacion.append("</b> a nombre de <B>");
        bodyNotificacion.append(this.razonSocial);
        bodyNotificacion.append("</b> se realiz� la asignaci�n ");
        bodyNotificacion.append("de una Pregunta Secreta que le permita dar mayor seguridad a las operaciones que haga en la Banca Electr�nica. <br/> </p> ");
        bodyNotificacion.append("<br><p>Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente,  <br>");
        bodyNotificacion.append("contrase�a de Enlace y contrase�as din�micas generadas por su Token.<B> Para cualquier duda puede comunicarse a S�per L�nea  <br>");
        bodyNotificacion.append("Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B><br>");
        bodyNotificacion.append(" </p>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionDesvinculacion", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Msg text notificacion review analista.
     *
     * @return the string buffer
     */
    public StringBuffer msgTextNotificacionReviewAnalista() {
       EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       StringBuffer bodyNotificacion = new StringBuffer();

       bodyNotificacion.append("<HTML><HEAD><TITLE>INFORMATIVO</TITLE></HEAD><BODY>");
       bodyNotificacion.append("<P ALIGN=\"JUSTIFY\"><font face=\"Arial\"><br><br>");
       //bodyNotificacion.append("Estimado Analista de Riesgos:<br><br>");
       bodyNotificacion.append("Por este medio le informamos que el d&iacute;a <B>");
       bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
       bodyNotificacion.append(TAG_NEGRITA_DE);
       bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
       bodyNotificacion.append("</B> de <B>" );
       bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR) );
       bodyNotificacion.append("</B>, el usuario ");
       bodyNotificacion.append(this.usuario);
       bodyNotificacion.append(TAG_NOMBRE_DE);
       bodyNotificacion.append(TAG_ABRE_B);
       bodyNotificacion.append(this.razonSocial);
       bodyNotificacion.append("</B> el motor de riesgos determin&oacute; una situaci&oacute;n anormal con estatus (<B>Mandar a un analista de riesgos</B>) ");
       bodyNotificacion.append("por lo que pedimos el apoyo de su &aacute;rea para revisar la situaci&oacute;n del usuario y en dado caso de ser ");
       bodyNotificacion.append("un \"Falso Positivo\", Favor de ingresar al motor de riesgos a desbloquearlo.<br>");
       //bodyNotificacion.append("Atentamente:<br><br>");
       //bodyNotificacion.append("<b>Banco Santander, S.A.</b><br>");
       bodyNotificacion.append("</font></P><br></BODY></HTML>");

       EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       return bodyNotificacion;
    }

    /**
     * Msg text notificacion review cliente.
     *
     * @return the string buffer
     */
    public StringBuffer msgTextNotificacionRSACliente() {
       EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       StringBuffer bodyNotificacion = new StringBuffer();

       bodyNotificacion.append("<HTML><HEAD><TITLE>INFORMATIVO</TITLE></HEAD><BODY>");
       bodyNotificacion.append("<P ALIGN=\"JUSTIFY\"><font face=\"Arial\">");
       bodyNotificacion.append("Por este medio le informamos que el d&iacute;a <B>");
       bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
       bodyNotificacion.append(TAG_NEGRITA_DE);
       bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
       bodyNotificacion.append("</B> de <B>" );
       bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR) );
       bodyNotificacion.append("</B>, el usuario con terminaci�n ");
       bodyNotificacion.append(TAG_ABRE_B);
       bodyNotificacion.append(this.usuario.substring(6,8));
       bodyNotificacion.append(TAG_CIERRA_B);
       bodyNotificacion.append(TAG_NOMBRE_DE);
       bodyNotificacion.append(TAG_ABRE_B);
       bodyNotificacion.append(this.razonSocial);
       bodyNotificacion.append("</B> nuestro motor de riesgos detect� una situaci�n anormal, por lo que pedimos comun�quese a <B>S�per L�nea Empresarial a los tel�fonos 5169 4343, ");
       bodyNotificacion.append("Lada sin costo 01 800 509 5000</B> o con su ejecutivo de cuenta, para darle seguimiento a su situaci�n.<br><br>");
       //bodyNotificacion.append("Atentamente:<br><br>");
       //bodyNotificacion.append("<b>Banco Santander, S.A.</b><br>");
       bodyNotificacion.append("</font></P><br></BODY></HTML>");

       EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       return bodyNotificacion;
    }

    /**
     * Msg text notificacion deny analista.
     *
     * @return the string buffer
     */
    public StringBuffer msgTextNotificacionDenyAnalista() {
       EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       StringBuffer bodyNotificacion = new StringBuffer();

       bodyNotificacion.append("<HTML><HEAD><TITLE>INFORMATIVO</TITLE></HEAD><BODY>");
       bodyNotificacion.append("<P ALIGN=\"JUSTIFY\"><font face=\"Arial\"><br><br>");
       //bodyNotificacion.append("Estimado Analista de Riesgos:<br><br>");
       bodyNotificacion.append("Por este medio le informamos que el d&iacute;a <B>");
       bodyNotificacion.append( obtenCalendario().get(Calendar.DAY_OF_MONTH));
       bodyNotificacion.append(TAG_NEGRITA_DE);
       bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
       bodyNotificacion.append("</B> de <B>" );
       bodyNotificacion.append( obtenCalendario().get(Calendar.YEAR) );
       bodyNotificacion.append("</B>, el usuario <B>");
       bodyNotificacion.append(this.usuario);
       bodyNotificacion.append("</B> a nombre de <B>");
       bodyNotificacion.append(this.razonSocial);
       bodyNotificacion.append("</B> el motor de riesgos determin&oacute; una situaci&oacute;n anormal con estatus (<B>Detener</B>) ");
       bodyNotificacion.append("por lo que pedimos el apoyo de su &aacute;rea para revisar la situaci&oacute;n del usuario y en dado caso de ser ");
       bodyNotificacion.append("un \"Falso Positivo\", Favor de ingresar al motor de riesgos a desbloquearlo.<br>");
       //bodyNotificacion.append("Atentamente:<br><br>");
       //bodyNotificacion.append("<b>Banco Santander, S.A.</b><br>");
       bodyNotificacion.append("</font></P><br></BODY></HTML>");

       EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionReviewAnalista", EIGlobal.NivelLog.DEBUG);
       return bodyNotificacion;
    }


    /**
     * Metodo para realizar la notificaci�n por email de una
     * configuracion de edo de cta individual
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextConfigEdoCtaIndPDF() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextConfigEdoCtaIndPDF", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <b>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado <b>").append("true".equals(this.estatusFinal) ? "el alta" : "la baja").append("</b> ");
        bodyNotificacion.append("individual del servicio de Paperless dentro de su contrato Enlace con terminaci&oacute;n <b>");
        bodyNotificacion.append(obtenSubCadena(this.numContrato, 4)).append("</b> a nombre de <b>").append(this.razonSocial).append(TAG_CIERRA_B).append(TAG_COMA_ESPACIO);
        /**
         * Se agrega validacion para cuando es Cuenta o Tarjeta
         * Configuracion Paperless Edo Cta Individual
         * FSW Indra 03/03/2015 PYME
         */
        bodyNotificacion.append("para la ").append( tipoOperacion.contains("001") ? "cuenta <b>" : "Tarjeta <b>").append(obtenSubCadena(this.cuentaDestino, 4)).append("</B>, por lo que se <B>");
        bodyNotificacion.append("true".equals(this.estatusFinal) ? "cancelar&aacute; el env&iacute;o de" : "enviar&aacute; al domicilio registrado");
        bodyNotificacion.append("</B> su estado de cuenta impreso.");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append("<b>Para cualquier duda o aclaraci&oacute;n puede comunicarse a S&uacute;per L�nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.</b>");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append("</font>");
        bodyNotificacion.append("</body></html> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextConfigEdoCtaIndPDF", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

    /**
     * Metodo para realizar la notificaci�n por email de una
     * configuracion de edo de cta individual
     * @return bodyNotificacion     Cuerpo del mensaje
     */
    public StringBuffer msgTextConfigEdoCtaIndXML() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextConfigEdoCtaIndXML", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR)).append(TAG_CIERRA_B);
        bodyNotificacion.append(", se ha solicitado la configuracion de  <B>ESTADOS DE CUENTA</B> dentro de su contrato Enlace ");
        bodyNotificacion.append("con terminaci�n ").append(obtenSubCadena(this.numContrato, 4)).append(TAG_NOMBRE_DE).append(TAG_ABRE_B).append(this.razonSocial).append(TAG_CIERRA_B).append(TAG_COMA_ESPACIO);
        bodyNotificacion.append("con el n�mero de FOLIO Individual XML ").append(this.numRef).append(" conforme a lo siguiente:");
        bodyNotificacion.append("<CENTER><TABLE border=1>");
        bodyNotificacion.append("<tr><td align=\"center\">Cuenta</td>");
        bodyNotificacion.append("<td align=\"center\">Descripci�n</td></tr>");
        bodyNotificacion.append(TAG_ABRE_TR_TD).append(obtenSubCadena(this.cuentaDestino, 4)).append(ETIQUETA_CIERRA_TD);
        bodyNotificacion.append(ETIQUETA_TD).append(this.descContrato).append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append("</TABLE></CENTER><BR><BR>");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append("<B>El estatus de las cuentas enviadas puede consultarlo ingresando a Consulta de Solicitud de Configuraci�n. ");
        bodyNotificacion.append("Para cualquier duda o aclaraci�n puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextConfigEdoCtaIndXML", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }

        /**
         * Metodo para realizar la notificaci�n por email de una
         * configuraci�n de Estatus de Envio de Estados de Cuenta Masiva
         * @return bodyNotificacion     Cuerpo del mensaje
         */
        public StringBuffer msgTextConfigMasPorCuenta() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionCambioPreg", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
        bodyNotificacion.append("<table border=\"0\" style=\"width:auto;\">");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>");
        bodyNotificacion.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append("</B>, se ha solicitado la configuraci&oacute;n masiva de estatus de env&iacute;o del ");
        bodyNotificacion.append("Estado de Cuenta en su contrato Enlace con terminaci&oacute;n <b>");
        bodyNotificacion.append(this.numContrato.substring(this.numContrato.length()-4,this.numContrato.length()));
        bodyNotificacion.append("</b> a nombre de <b>" +  this.razonSocial + "</b>,");
        bodyNotificacion.append(" con el n&uacute;mero de FOLIO <b>");
        bodyNotificacion.append(this.getNumRef());
        bodyNotificacion.append("</b> conforme a lo siguiente:");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append(TAG_ABRE_TR_TD);
        bodyNotificacion.append("<center><table cellspacing=\"2\" cellpadding=\"3\" border=\"1\" >");
        bodyNotificacion.append("<tr><td valign=\"top\"> N&uacute;mero de Registros Enviados</td><td valign=\"top\" >");
        bodyNotificacion.append(this.getFormatoArchivo()[1]+TAG_CIERRA_TR_TD);
        bodyNotificacion.append("</table></center><br/><br/>");
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append("<tr><td style=\"text-align:justify;\">");
        bodyNotificacion.append("<span><b><br><br>El estatus de las cuentas enviadas puede consultarlo Ingresando al menu \"Estados de Cuenta &gt; Modificar ");
        bodyNotificacion.append("estado de env&iacute;o del Estado de Cuenta Impreso &gt; Consulta\" de Enlace. Para cualquier duda o aclaraci&oacute;n puede ");
        bodyNotificacion.append("comunicarse a S&uacute;per Linea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.</bb></span>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");
        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCambioPreg", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }
        /**
         * Generacion de notificacion email configuracion masiva TC.
         * @since 27/02/2015
         * @author FSW-Indra
         * @return Texto correo.
         */
        public String msgTextConfigMasPorTC() {
            EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextConfigMasPorTC", EIGlobal.NivelLog.DEBUG);

            StringBuffer bodyNotificacion = new StringBuffer();

            bodyNotificacion.append("<HTML>");
            bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
            bodyNotificacion.append("<BODY> ");
            bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
            bodyNotificacion.append("<TABLE border=\"0\" style=\"width:auto;\">");
            bodyNotificacion.append(TAG_ABRE_TR_TD);
            bodyNotificacion.append(TAG_DOBLE_BR);
            bodyNotificacion.append("Por este medio le informamos que el d&iacute;a <B>");
            bodyNotificacion.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
            bodyNotificacion.append(TAG_NEGRITA_DE);
            bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
            bodyNotificacion.append(TAG_NEGRITA_DE);
            bodyNotificacion.append(obtenCalendario().get(Calendar.YEAR));
            bodyNotificacion.append("</B>, se ha solicitado la configuraci&oacute;n masiva de estatus de env&iacute;o del ");
            bodyNotificacion.append("Estado de Cuenta en su contrato Enlace con terminaci&oacute;n <B>");
            bodyNotificacion.append(this.numContrato.substring(this.numContrato.length()-4,this.numContrato.length()));
            bodyNotificacion.append("</B> a nombre de <B>" +  this.razonSocial + "</B>,");
            bodyNotificacion.append(" con el n&uacute;mero de FOLIO <B>");
            bodyNotificacion.append(this.getNumRef());
            bodyNotificacion.append("</B> conforme a lo siguiente:");
            bodyNotificacion.append(TAG_DOBLE_BR);
            bodyNotificacion.append(TAG_CIERRA_TR_TD);
            bodyNotificacion.append(TABLE_CIERRA);
            bodyNotificacion.append("<CENTER><TABLE cellspacing=\"2\" cellpadding=\"3\" border=\"1\" >");
            bodyNotificacion.append("<TR><TD valign=\"top\"> N&uacute;mero de Registros Enviados</TD><TD valign=\"top\" >");
            bodyNotificacion.append(this.getFormatoArchivo()[1]+TAG_CIERRA_TR_TD);
            bodyNotificacion.append(TABLE_CIERRA);
            bodyNotificacion.append("</CENTER>");
            bodyNotificacion.append(BR);
            bodyNotificacion.append("<TABLE border=\"0\" style=\"width:auto;\">");
            bodyNotificacion.append("<TR><TD style=\"text-align:justify;\">");
            bodyNotificacion.append("<SPAN><B>");
            bodyNotificacion.append(BR);
            bodyNotificacion.append("El estatus de las cuentas enviadas puede consultarlo Ingresando al men&uacute; \"Estados de Cuenta &gt; Modificar ");
            bodyNotificacion.append("estado de env&iacute;o del Estado de Cuenta Impreso &gt; Consulta\" de Enlace. Para cualquier duda o aclaraci&oacute;n puede ");
            bodyNotificacion.append("comunicarse a S&uacute;per L&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.</B></SPAN>");
            bodyNotificacion.append(BR);
            bodyNotificacion.append(TAG_CIERRA_TR_TD);
            bodyNotificacion.append(TABLE_CIERRA);
            bodyNotificacion.append(BR);
            bodyNotificacion.append("</FONT>");
            bodyNotificacion.append("</BODY></HTML> ");
            bodyNotificacion.append(BR);
            EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextConfigMasPorTC", EIGlobal.NivelLog.DEBUG);
            return bodyNotificacion.toString();
        }
    /**
    * Metodo para realizar la notificaci�n por email de una
    * solicitud de estados de cuenta Historicos
    * @return bodyNotificacion      Cuerpo del mensaje
    */
    public StringBuffer msgTextSolicitaPeriodosHistoricos() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextSolicitaPeriodosHistoricos", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();

        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");
        bodyNotificacion.append("<table border=\"0\" style=\"width:auto;\"><tr><td>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>");
        bodyNotificacion.append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE);
        bodyNotificacion.append(obtenCalendario().get(Calendar.YEAR));
        bodyNotificacion.append("</B>, se ha solicitado la disposici�n de Estados de Cuenta en formato PDF de ");
        bodyNotificacion.append("periodos hist�ricos, dentro de su contrato Enlace con terminaci&oacute;n <b> ");
        bodyNotificacion.append(this.numContrato.substring(this.numContrato.length()-4,this.numContrato.length()));
        if("003".equals(getTipo_operacion())){
            bodyNotificacion.append("</b> a nombre de <b>" +  this.razonSocial + "</b>, para la tarjeta <b>");
        }else{
            bodyNotificacion.append("</b> a nombre de <b>" +  this.razonSocial + "</b>, para la cuenta <b>");
        }
        bodyNotificacion.append(this.cuentaDestino.substring(this.cuentaDestino.length()-4,this.cuentaDestino.length()));
        bodyNotificacion.append("</b> conforme a lo siguiente:");
        bodyNotificacion.append(TAG_DOBLE_BR);
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append(TAG_ABRE_TR_TD);
        bodyNotificacion.append("<center><table cellspacing=\"2\" cellpadding=\"3\" border=\"1\" >");
        bodyNotificacion.append("<tr><td valign=\"top\">Periodo</td><td valign=\"top\">Folio</td></tr>");
        if (this.listaFolios.isEmpty()) {
            bodyNotificacion.append("<tr><td>NA</td><td>NA</td></tr>");
        }else {
            for (String pf : this.listaFolios ) {
                EIGlobal.mensajePorTrace("Periodo_Folio "+pf, EIGlobal.NivelLog.DEBUG);
                String[] v=pf.split("\\|");
                EIGlobal.mensajePorTrace("vsize "+v.length, EIGlobal.NivelLog.DEBUG);
                String v0=(v.length>=0) ? String.format("%s %s",obtenNombreMes(Integer.valueOf((v[0].length()==6) ? v[0].substring(4,6) : "00")-1)
                          ,(v[0].length()==6) ? v[0].substring(0,4) : COD_ERROR) : TAG_ESPACIO_BLANCO;
                String v1=(v.length>0) ? v[1] : TAG_ESPACIO_BLANCO;
                bodyNotificacion.append(TAG_ABRE_TR_TD+v0+"</td><td>"+v1+TAG_CIERRA_TR_TD);
            }
        }
        bodyNotificacion.append("</table><center><br><br>");
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        if (!this.listaErrores.isEmpty()) {
            bodyNotificacion.append("<tr><td><br><br>Se presentaron errores en las solicitudes de Estado de Cuenta Historicos para los siguientes periodos:<br>");
            bodyNotificacion.append("<center><table cellspacing=\"2\" cellpadding=\"3\" border=\"1\" >");
            bodyNotificacion.append("<tr><td valign=\"top\">Periodo</td><td valign=\"top\">Error</td><td valign=\"top\">Descripcion</td></tr>");

            for (String pf : this.listaErrores) {
                EIGlobal.mensajePorTrace("error "+pf, EIGlobal.NivelLog.DEBUG);
                String[] v=pf.split("\\|");
                EIGlobal.mensajePorTrace("vsize "+v.length, EIGlobal.NivelLog.DEBUG);
                String v0=(v.length>=0)? String.format("%s %s",obtenNombreMes(Integer.valueOf((v[0].length()==6) ? v[0].substring(4,6) : "00")-1)
                          ,(v[0].length()==6) ? v[0].substring(0,4) : COD_ERROR) : TAG_ESPACIO_BLANCO;
                String v1=(v.length>0) ? v[1] : TAG_ESPACIO_BLANCO;
                String v2=(v.length>1) ? v[2] : TAG_ESPACIO_BLANCO;
                bodyNotificacion.append(TAG_ABRE_TR_TD+v0+"</td><td>"+v1+"</td><td>"+v2+TAG_CIERRA_TR_TD);
            }
            bodyNotificacion.append("</table></td></tr>");
        }
        bodyNotificacion.append("<tr><td style=\"text-align:justify;\">");
        bodyNotificacion.append("<span><b><br><br>El estatus de Estados de Cuenta solicitados puede consultarlo ingresando al menu \"Estados de Cuenta &gt; ");
        bodyNotificacion.append("Solicitud de Periodos Hist&oacute;ricos formato PDF &gt; Consulta\" de Enlace. Para cualquier duda o aclaraci&oacute;n puede ");
        bodyNotificacion.append("comunicarse a S&uacute;per Linea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.</b></span>");
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(TAG_CIERRA_TR_TD);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");
        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionCambioPreg", EIGlobal.NivelLog.DEBUG);
        return bodyNotificacion;
    }
    /**
     * Metodo que obtiene la subcadena.
     * 
     * @param cadena Cadena obtenida
     * @param nCarac numero de caracteres
     * @return Tama�o de la cadena
     */
    public String obtenSubCadena(String cadena, int nCarac){

        String subString = "";
        cadena = cadena.trim();

        if (!cadena.equalsIgnoreCase("SIN CUENTA")) {
            if(cadena != null && cadena.length() > nCarac){
                subString = cadena.substring(cadena.length() - nCarac);
                EIGlobal.mensajePorTrace(" EmailDetails -> La cadena obtenida es: " + subString, EIGlobal.NivelLog.DEBUG);
            } else {
                EIGlobal.mensajePorTrace(" EmailDetails -> La cadena para substring esta vacia o es null", EIGlobal.NivelLog.DEBUG);
            }
        } else {
            subString = cadena;
        }

        return subString;
    }
    
    /**
     * Se envia correo para notificar las operacion para dar de baja la FIEL.
     * @return el resultado de la notificacion de las operaciones de la FIEL.
     */
    public StringBuffer msgTextNotificacionFielBaja() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionFielBaja", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append (obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append( obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4)).append("</B> a nombre de <B>").append( this.razonSocial) 
        				.append( "</B> se realiz� baja de la FIEL para la cuenta *****<B>");
        bodyNotificacion.append(this.cuentaDestino.substring(this.cuentaDestino.length()-4,this.cuentaDestino.length()));
        bodyNotificacion.append("</B> con folio(s) <B>"+this.numRef+"</B>");
		bodyNotificacion.append(BR);
        bodyNotificacion.append("<br> Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente, ");
        bodyNotificacion.append("contrase&ntilde;a de Enlace y contrase&ntilde;as din�micas generadas por su Token.");
		bodyNotificacion.append(BR);
		bodyNotificacion.append("Para cualquier duda puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o  ");
        bodyNotificacion.append(LADA_SIN_COSTO);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionFielBaja", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Resultado: " + bodyNotificacion.toString(), EIGlobal.NivelLog.DEBUG);        
        return bodyNotificacion;
    }
    
    
    /**
     * Se envia correo para notificar las operacion para dar de alta FIEL.
     * @return el resultado de la notificacion de las operaciones de la FIEL.
     */
    public StringBuffer msgTextNotificacionFielAlta() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionFielAlta", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4)).append("</B> a nombre de <B>").append(this.razonSocial) 
                               .append("</B> se realiz� el registro de la FIEL para la cuenta *****<B>");
        bodyNotificacion.append(this.cuentaDestino.substring(this.cuentaDestino.length()-4,this.cuentaDestino.length()));
        bodyNotificacion.append("</B> con folio(s) <B>"+this.numRef+"</B>");
		bodyNotificacion.append(BR);
        bodyNotificacion.append("<br> Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente, ");
        bodyNotificacion.append("contrase&ntilde;a de Enlace y contrase&ntilde;as din�micas generadas por su Token.");
		bodyNotificacion.append(BR);
		bodyNotificacion.append("Para cualquier duda puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o  ");
        bodyNotificacion.append(LADA_SIN_COSTO);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionFielAlta", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Resultado: " + bodyNotificacion.toString(), EIGlobal.NivelLog.DEBUG);        
        return bodyNotificacion;
    }
    
    /**
     * Se envia correo para notificar las operacion para modificar la FIEL.
     * @return el resultado de la notificacion de las operaciones de la FIEL.
     */
    public StringBuffer msgTextNotificacionFielModificacion() {
        EIGlobal.mensajePorTrace("EmailDetails::inicio msgTextNotificacionFielModificacion", EIGlobal.NivelLog.DEBUG);

        StringBuffer bodyNotificacion = new StringBuffer();
        
        bodyNotificacion.append("<HTML>");
        bodyNotificacion.append("<HEAD><TITLE>Informativo</TITLE></HEAD> ");
        bodyNotificacion.append("<BODY> ");
        bodyNotificacion.append("<FONT FACE=\"arial\" SIZE=\"3\" COLOR=\"black\">");

        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("Por este medio le informamos que el d�a <B>").append(obtenCalendario().get(Calendar.DAY_OF_MONTH));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenNombreMes(obtenCalendario().get(Calendar.MONTH)));
        bodyNotificacion.append(TAG_NEGRITA_DE).append(obtenCalendario().get(Calendar.YEAR) + TAG_CIERRA_B);
        bodyNotificacion.append(", en su contrato Enlace <B>").append(obtenSubCadena(this.numContrato, 4) + "</B> a nombre de <B>" + this.razonSocial 
                                 + "</B> se realiz� modificaci�n de la FIEL para la cuenta *****<B>");
        bodyNotificacion.append(this.cuentaDestino.substring(this.cuentaDestino.length()-4,this.cuentaDestino.length()));
        bodyNotificacion.append("</B> con folio(s) <B>").append(this.numRef).append(TAG_CIERRA_B);
		bodyNotificacion.append(BR);
        bodyNotificacion.append("<br> Le informamos que en Santander por ning�n motivo enviamos correos electr�nicos con ligas para solicitarle su c�digo de cliente, ");
        bodyNotificacion.append("contrase&ntilde;a de Enlace y contrase&ntilde;as din�micas generadas por su Token.");
		bodyNotificacion.append(BR);
		bodyNotificacion.append("Para cualquier duda puede comunicarse a S�per L�nea Empresarial a los tel�fonos 5169 4343 o  ");
        bodyNotificacion.append(LADA_SIN_COSTO);
        bodyNotificacion.append(BR);
        bodyNotificacion.append(BR);
        bodyNotificacion.append("</FONT>");
        bodyNotificacion.append("</BODY></HTML> ");

        EIGlobal.mensajePorTrace("EmailDetails::Fin msgTextNotificacionFielModificacion", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("Resultado: " + bodyNotificacion.toString(), EIGlobal.NivelLog.DEBUG);        
        return bodyNotificacion;
    }
    
    
    public ArrayList<LimitesOperacion> getListaOperLYM() {
        return listaOperLYM;
    }

    public void setListaOperLYM(ArrayList<LimitesOperacion> listaOperLYM) {
        this.listaOperLYM = listaOperLYM;
    }

    /**
     * @return the convenio
     */
    public String getConvenio() {
        return convenio;
    }

    /**
     * @param convenio the convenio to set
     */
    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    /**
     * @return the nombreConvenio
     */
    public String getNombreConvenio() {
        return nombreConvenio;
    }

    /**
     * @param nombreConvenio the nombreConvenio to set
     */
    public void setNombreConvenio(String nombreConvenio) {
        this.nombreConvenio = nombreConvenio;
    }

    /**
     * @param listaFolios el listaFolios a establecer
     */
    public void setListaFolios(List listaFolios) {
        this.listaFolios = listaFolios;
    }

    /**
     * @return el listaFolios
     */
    public List getListaFolios() {
        return listaFolios;
    }

    /**
     * @param listaErrores el listaErrores a establecer
     */
    public void setListaErrores(List listaErrores) {
        this.listaErrores = listaErrores;
    }

    /**
     * @return el listaErrores
     */
    public List getListaErrores() {
        return listaErrores;
    }
}