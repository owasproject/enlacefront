package mx.altec.enlace.dao;

import java.sql.*;

import mx.altec.enlace.beans.DetBajaUsuarioBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class DetBajaUsuarioDAO 
{
		private PreparedStatement statement =null;
		private Connection conn=null;
		String query = "";

		private ResultSet Consulta()throws Exception{
			ResultSet rs = null;
			rs = statement.executeQuery();
			return rs;
		}

		private int Inserta()throws Exception{
			int resultado = 0;
			resultado = statement.executeUpdate();
			return resultado;
		}

		/******************************************************************
		 *** Metodo que registra la baja en la tabla EWEB_DET_BAJAS
		 *
		 * @throws Exception 
		 *****************************************************************/
		
		public void registrarBaja(DetBajaUsuarioBean usr) {
			EIGlobal.mensajePorTrace ("DetBajaUsuarioDAO - registraBaja() - " +
					"el Usuario es: [" + usr.getUsuario() + "]", EIGlobal.NivelLog.INFO);
			try {
	    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE);
				
	    		query = "INSERT INTO EWEB_DET_BAJAS VALUES (" +
	    		"'" + usr.getContrato() + "', " +
	    		"'" + usr.getUsuario() + "', " +
	    		"'" + usr.getSerieToken() + "', " +
	    		"'" + usr.getEstadoToken() + "', " +
	    		"'" + usr.getEstadoNIP() + "', " +
	    			  usr.getFechaBaja() + ", " +
  	    		"'" + usr.getEstadoOperacion() + "', " +
	    		"'" + usr.getOrigen() + "')";

	    		EIGlobal.mensajePorTrace ("DetBajaUsuarioDAO - registraBaja() -" +
						" query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
				statement = conn.prepareStatement (query.toString());
				Inserta();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("DetBajaUsuarioDAO - registraBaja() " +
						" Error: [" + e + "]", 
						EIGlobal.NivelLog.INFO);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					//bean = null;
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("DetBajaUsuarioDAO - registraBaja() - " +
							"Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.INFO);
				}
				EIGlobal.mensajePorTrace( "DetBajaUsuarioDAO - registraBaja() - fin"
						, EIGlobal.NivelLog.INFO);
			}
		}
		
}