package mx.altec.enlace.dao;

import java.io.File;
import java.util.Map;

import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class NomPreAltaMasivaDAO {
	
	private String ejecutaServicio(String trama) {        
		
		String tramaSalida = null;
		Map<String, String> htResult = null;
		
		try {
			
			EIGlobal.mensajePorTrace("NomPreAltaMasiva > Entrada: [" + trama + "]", EIGlobal.NivelLog.INFO);
			
			htResult = (Map<String, String>) new ServicioTux().web_red(trama);
			
			tramaSalida = (String) htResult.get("BUFFER");
			
		} catch (Exception e) {
	        	
			e.printStackTrace();
		}	 

		EIGlobal.mensajePorTrace("NomPreAltaMasiva > Salida: [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);

		return tramaSalida;
	}
	
	public String enviarDetalleCorreo(String idUsuario, String contrato, String perfil, String secuencia, String correo) {
		
		StringBuffer sb = new StringBuffer()
			.append("1EWEB")
			.append("|")
			.append(idUsuario)
			.append("|")
			.append("NPEA")
			.append("|")
			.append(contrato)
			.append("|")
			.append(idUsuario)
			.append("|")
			.append(perfil)
			.append("|")
			.append(contrato)
			.append("@")
			.append(secuencia)
			.append("@")
			.append(correo)
			.append("@");
		
		return ejecutaServicio(sb.toString());		
	}
	
	public String cancelarAltaMasiva(String idUsuario, String contrato, String perfil, String secuencia, String fechaRecepcion) {
		
		StringBuffer sb = new StringBuffer()
			.append("1EWEB")
			.append("|")
			.append(idUsuario)
			.append("|")
			.append("CAAM")
			.append("|")
			.append(contrato)
			.append("|")
			.append(idUsuario)
			.append("|")
			.append(perfil)
			.append("|")
			.append(contrato)
			.append("@")
			.append(secuencia)
			.append("@")
			.append(fechaRecepcion)
			.append("@");
		
		return ejecutaServicio(sb.toString());
	}
	
	public String obtenerDetalleArchivos(String contrato, String idUsuario, String perfil, String idSesion, String fechaRecepcion, String fechaAplicacion, String secuencia, String... estados) {	
		
		StringBuffer sb = new StringBuffer()
			.append("1EWEB")
			.append("|")
			.append(idUsuario)
			.append("|")
			.append("NPCT")
			.append("|")
			.append(contrato)
			.append("|")
			.append(idUsuario)
			.append("|")
			.append(perfil)
			.append("|")
			.append("NO_CONTRATO ='")
			.append(contrato)	
			.append("'");
	
		if (estados != null && estados.length > 0) {
			
			sb.append(" AND ESTATUS IN (");
			
			for(int i = 0; i < estados.length; i++) {
				
				sb.append("'").append(estados[i]).append("'");
				
				if (i != estados.length - 1) {
					
					sb.append(",");
				}
			}
			
			sb.append(")");
		}
		
		if (fechaRecepcion != null && fechaRecepcion.length() > 0) {
			
			sb.append(" AND  FECHA_RECEP = TO_DATE('").append(fechaRecepcion).append("','dd/mm/yyyy')");			
		}
		
		if (fechaAplicacion != null && fechaAplicacion.length() > 0) {
			
			sb.append(" AND FECHA_APLIC = TO_DATE('").append(fechaAplicacion).append("','dd/mm/yyyy')");			
		}
		
		if (secuencia != null && secuencia.length() > 0) {
			
			sb.append(" AND SECUENCIA = '").append(secuencia).append("'");			
		}		
		
		sb.append("@").append(new File(Global.DIRECTORIO_REMOTO_INTERNET, idSesion).getAbsolutePath()).append("@");				        
		
		return ejecutaServicio(sb.toString());						
	}
	
	
}

