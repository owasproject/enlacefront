package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.NominaLineaBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

/**
 *
 * @author Z712236
 *
 */
public class NominaLineaDAO {
	/**
	 * statement
	 */
	private static PreparedStatement statement = null;
	/**
	 * fchRecep fecha de recepcion
	 */
	static final String fchRecep="F_RECEP";
	/**
	 * fchAplic fecha de aplicacion
	 */
	static final String fchAplic="F_APLIC";
	/**
	 * Realiza consulta de archivos por rango (pagina)
	 * @param consulta bean de la consulta
	 * @param ini      		fecha inicio
	 * @param fin      		fecha fin
	 * @return resultado 	Lista dentro del bean
	 * @throws SQLException	Control de excepciones oracle
	 */
	public NominaLineaBean consultarArchivos(
			NominaLineaBean consulta, Integer ini, Integer fin ) throws SQLException {
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: consultarArchivos() :: Iniciando Consulta Archivos", EIGlobal.NivelLog.INFO);
		/**
		 * NominaLineaBean resultado
		 */
		final NominaLineaBean resultado=new NominaLineaBean();
		/**
		 * coneccion bd
		 */
		Connection conn = null;
		/**
		 * coneccion bd
		 */
		ResultSet rs = null;
		/**
		 * fecha
		 */
		final String fecha = "";
		try {
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			/*****************QUERY TOTAL REGISTROS*******************/
			final String queryCount = armarqueryCount(consulta);
			statement = conn.prepareStatement(queryCount);
			EIGlobal.mensajePorTrace("Query total regs:: " + queryCount, EIGlobal.NivelLog.INFO);
			String totalRegistros="";
			rs = statement.executeQuery();
			if(rs.next()){
				totalRegistros=rs.getString("TOTAL") != null ? rs.getString("TOTAL") : "0";
				resultado.getPaginacion().setTotalRegs(Integer.parseInt(totalRegistros));
			}
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: consultarArchivos() :: totalRegistros: "+totalRegistros, EIGlobal.NivelLog.INFO);
			/*****************QUERY CONSULTA REGISTROS*******************/
			final String query = armarqueryConsulta(consulta, ini, fin);
			statement = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace("Query armado consulta:: " + query, EIGlobal.NivelLog.INFO);
			//Se llama funcion para obtener los datos del ResultSet
			final List<NominaLineaBean> listaNomina=obtenerDatosResultado(rs);
			resultado.setLstConsulta(listaNomina);
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: consultarArchivos() :: Lista generada"+totalRegistros, EIGlobal.NivelLog.INFO);
		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
		} catch (IllegalStateException e) {
			EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion.:. [" + e + "]", EIGlobal.NivelLog.ERROR);
		}catch (Exception e2) {
			EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion.:. ", EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion::: [" +new Formateador().formatea(e1) + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion::: "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("NominaLineaDAO - saliendo de consultarArchivos():::", EIGlobal.NivelLog.INFO);
		}
		return resultado;
	}

	/**
	 * Arma el query de consulta
	 * @param consulta bean de la consulta
	 * @param ini fecha de inicio
	 * @param fin fecha de fin
	 * @return query Resultado Consulta
	 */
	private String armarqueryConsulta(NominaLineaBean consulta, Integer ini, Integer fin) {
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryConsulta() :: fchInicio: "+consulta.getFchIni(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryConsulta() :: fchFin: "+consulta.getFchFin(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryConsulta() :: ctaCargo : "+consulta.getCtaCargo(), EIGlobal.NivelLog.INFO);

		/**
		 * query a ejecutar
		 */
		/*Stefanini - Modificacion del query para consultar la tabla de solicitudes de informes en PDF*/
		final StringBuffer query = new StringBuffer();
		query.append( "SELECT * FROM ( SELECT ROWNUM AS FILA, CONSULTA.* FROM ( " );
		query.append( " SELECT A.SEC_ARCH AS SEC_ARCH, TO_CHAR(A.FCH_APLIC, 'dd/mm/yyyy') AS FCH_APLICACION, A.CTA_CARGO AS CTA_CARGO, A.NUM_REG AS NUM_REGS," );
		query.append( " LTRIM(TO_CHAR(A.IMPORTE_REC, '999,999,999,999.00')) AS IMPORT_APLIC, A.ESTATUS_ARCH AS ESTATUS,  to_char(A.FCH_RECEP,'YYYY-MM-DD') AS FCH_RECEP ,");
		query.append( " NVL(B.COD_ESTAT,'N')  AS ESTATUS_SOL , NVL(B.DSC_OBSER, '') AS OBSERVACIONES ");
		query.append( " FROM EWEB_NOMLIN_CTRL A , EWEB_MX_PRC_SOL_NOM B " );
		query.append( " WHERE A.SEC_ARCH  = B.NUM_SEC_PK(+) AND trim(A.NUM_CUENTA2) = B.COD_CONTR_PK(+) AND 'L' = B.COD_TIPO_NOM(+) " );
		query.append( " AND A.NUM_CUENTA2='" );
		query.append( consulta.getContrato() );
		query.append( "'" );

		if(fchRecep.equals(consulta.getTipoFecha())){
			query.append( " AND trunc(A.FCH_RECEP) >= TO_DATE('" );
			query.append( consulta.getFchIni() );
			query.append( "', 'dd/mm/yyyy')" );
			query.append( " AND trunc(A.FCH_RECEP) <= TO_DATE('" );
			query.append( consulta.getFchFin() );
			query.append( "', 'dd/mm/yyyy') " );
		}
		if(fchAplic.equals(consulta.getTipoFecha())){
			query.append( " AND trunc(A.FCH_APLIC) >= TO_DATE('" );
			query.append( consulta.getFchIni() );
			query.append( "', 'dd/mm/yyyy')" );
			query.append( " AND trunc(A.FCH_APLIC) <= TO_DATE('" );
			query.append( consulta.getFchFin() );
			query.append( "', 'dd/mm/yyyy') " );
		}
		if (!"".equals(consulta.getCtaCargo())) {
			query.append( " AND A.CTA_CARGO = '" );
			query.append( consulta.getCtaCargo() );
			query.append( "'" );
		}
		query.append( " ORDER BY A.FCH_APLIC, A.SEC_ARCH ASC )CONSULTA)" );
		query.append( " WHERE FILA >= " );
		query.append( ini );
		query.append( " AND FILA <= " );
		query.append( fin );
		return query.toString();
	}

	/**
	 * Cuenta registros encontrados
	 * @param consulta bean de la consulta
	 * @return query Query de la consulta
	 */
	private String armarqueryCount(NominaLineaBean consulta) {
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: fchInicio: "+consulta.getFchIni(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: fchFin: "+consulta.getFchFin(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: ctaCargo : "+consulta.getCtaCargo(), EIGlobal.NivelLog.INFO);

		/**
		 * query a ejecutar
		 */
		final StringBuffer query = new StringBuffer();
		query.append( "SELECT COUNT(1) AS TOTAL " );
		query.append( " FROM EWEB_NOMLIN_CTRL" );
		query.append( " WHERE NUM_CUENTA2='" );
		query.append( consulta.getContrato() );
		query.append( "'" );

		if("F_RECEP".equals(consulta.getTipoFecha())){
			query.append( " AND trunc(FCH_RECEP) >= TO_DATE('" );
			query.append( consulta.getFchIni() );
			query.append( "', 'dd/mm/yyyy')" );
			query.append( " AND trunc(FCH_RECEP) <= TO_DATE('" );
			query.append( consulta.getFchFin() );
			query.append( "', 'dd/mm/yyyy') " );
		}
		if("F_APLIC".equals(consulta.getTipoFecha())){
			query.append( " AND trunc(FCH_APLIC) >= TO_DATE('" );
			query.append( consulta.getFchIni() );
			query.append( "', 'dd/mm/yyyy')" );
			query.append( " AND trunc(FCH_APLIC) <= TO_DATE('" );
			query.append( consulta.getFchFin() );
			query.append( "', 'dd/mm/yyyy') " );
		}
		if (!"".equals(consulta.getCtaCargo())) {
			query.append( " AND CTA_CARGO = '" );
			query.append( consulta.getCtaCargo() );
			query.append( "' " );
		}
		return query.toString();
	}

	/**
	 * Recupera datos del ResultSet
	 * @param rs Resultado de la consulta
	 * @return listaNomina devuelve la lista de archivos
	 * @throws SQLException control de excepciones sql
	 */
	private List<NominaLineaBean> obtenerDatosResultado(ResultSet rs) throws SQLException{
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: Entrando al metodo obtenerDatosResultado()", EIGlobal.NivelLog.INFO);
		/**
		 * listaNomina
		 */
		final List<NominaLineaBean> listaNomina = new ArrayList<NominaLineaBean>();

		rs = statement.executeQuery();
		try {
			while (rs.next()) {
				final NominaLineaBean r = new NominaLineaBean();
				r.setSecArchivo(rs.getString("SEC_ARCH") != null ? rs.getString("SEC_ARCH") : "");
				r.setFchAplicacion(rs.getString("FCH_APLICACION") != null ? rs.getString("FCH_APLICACION") : "");
				r.setCtaCargo(rs.getString("CTA_CARGO") != null ? rs.getString("CTA_CARGO") : "");
				r.setNumRegistros(rs.getString("NUM_REGS") != null ? rs.getString("NUM_REGS") : "");
				r.setImporteAplicado(rs.getString("IMPORT_APLIC") != null ? rs.getString("IMPORT_APLIC") : "");
				r.setEstatus(rs.getString("ESTATUS"));
				r.setFchCarga(rs.getString("FCH_RECEP") != null ? rs.getString("FCH_RECEP") : "");

				final String estatus = rs.getString("ESTATUS");
				String estat = "";
				if (estatus != null ) {
						if ("R".equals(estatus)){
							estat = "Recibido";
						}if ("E".equals(estatus)){
							estat = "En proceso";
						}if ("P".equals(estatus)){
							estat = "Procesado";
						}if ("T".equals(estatus)){
							estat = "Mancomunado";
						}if ("C".equals(estatus)){
							estat = "Cancelado";
						}if ("X".equals(estatus)){
							estat = "No procesado";
						}if ("G".equals(estatus)){
							estat = "Preparado";
						}
				}

				/*FSW - Nuevos campos de nomina */
				r.setObservaciones(rs.getString("OBSERVACIONES"));
				r.setEstatus(estat);

				final String estatusSol = rs.getString("ESTATUS_SOL");
				String desEstatusSol = "";
				if( null != estatusSol && "P".equals(estatus) ){
					//Solo mostar estatus para la nominas procesadas
					if("S".equals(estatusSol)){
						desEstatusSol = "Solicitado";
					}if("P".equals(estatusSol)){
						desEstatusSol = "Procesado";
					}if("R".equals(estatusSol)){
						desEstatusSol = "Rechazado";
					}if("N".equals(estatusSol)){
						desEstatusSol = " ";
					}if("D".equals(estatusSol)){
						desEstatusSol = "Disponible";
					}
				}
				r.setEstatusSol(desEstatusSol);

				listaNomina.add(r);
			}

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: Se controla detalle al obtener los datos del ResulSet ", EIGlobal.NivelLog.INFO);
		}catch (IllegalStateException e) {
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: Se controla detalle al obtener los datos del ResulSet... ", EIGlobal.NivelLog.INFO);
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryCount() :: Se controla detalle al obtener los datos del ResulSet.... ", EIGlobal.NivelLog.INFO);
		}finally {
			try {
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion:: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e2) {
				EIGlobal.mensajePorTrace("NominaLineaDAO - consultarArchivos() - "+ "Se controla detalle al cerrar conexion::", EIGlobal.NivelLog.ERROR);
			}
		}

		return listaNomina;
	}

	/**
	 * Inserta datos del archivo de Nomina en Linea
	 * @param bean contiene los datos a registrar
	 * @return exito bandera de ejecusion de la operacion
	 */
	public boolean registraNomlinArch(NominaLineaBean bean){
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: registraNomlinArch() :: Entrando al metodo registraNomlinArch()", EIGlobal.NivelLog.INFO);
		Connection conn = null;
    	boolean exito = true;

    	try {
			StringBuffer query = new StringBuffer("");
			query.append("INSERT INTO EWEB_NOMLIN_ARCH (CONTRATO, NOM_ARCH, NUM_REG, IMPORTE_REC, FCH_CARGA, ESTATUS_CARGA, BIATUX_ORIGEN, ID_TIPO_CARGO)");
			query.append(" VALUES(");
			query.append("'");
			query.append(bean.getContrato());
			query.append("','");
			query.append(bean.getNomArchivo());
			query.append("','");
			query.append(bean.getNumRegistros());
			query.append("','");
			query.append(bean.getImporteRecibido());
			query.append("',");
			//query.append("to_date('" + bean.getFchCarga() + "','DD-MM-YYYY HH24:MI')");
			query.append("SYSDATE");
			query.append(",'");
			query.append("0");
			query.append("','");
			query.append(bean.getServidorBiatux());
			query.append("','");
			query.append(bean.getTipoCargoArch());
			query.append("')");

			EIGlobal.mensajePorTrace("NominaLineaDAO :: registraNomlinArch :: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			statement = conn.prepareStatement(query.toString());
			statement.executeQuery();
			conn.commit();
			EIGlobal.mensajePorTrace("NominaLineaDAO :: registraNomlinArch :: Saliendo... "+exito,EIGlobal.NivelLog.DEBUG);

    	}catch (SQLException e){
    		EIGlobal.mensajePorTrace("NominaLineaDAO :: registraNomlinArch :: Se controlo problema al insertar al batch "
    				+ " tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
    		exito = false;
    	}catch (Exception e) {
    		EIGlobal.mensajePorTrace("NominaLineaDAO :: registraNomlinArch :: Se controlo problema al insertar al batch ",EIGlobal.NivelLog.INFO);
		}finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("ConsultaAsignacionDAO - registraNomlinArch() - "+ "Se proboco error al cerrar conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e2) {
				EIGlobal.mensajePorTrace("ConsultaAsignacionDAO - registraNomlinArch() - "+ "Se proboco detalle al cerrar conexion::", EIGlobal.NivelLog.ERROR);
			}
		}
    	return exito;
    }

	/**
	 * validaDuplicadoNomlinArch valida archivos duplicados
	 * con base al contrato, num_registros e importe
	 * @param bean parametros de entrada
	 * @return duplicado archivos duplicados
	 * @throws SQLException Excepciones de SQL
	 */
	public boolean validaDuplicadoNomlinArch(
			NominaLineaBean bean ) throws SQLException {
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: validaDuplicadoNomlinArch() :: Iniciando validacion archivos duplicados...", EIGlobal.NivelLog.INFO);
		/**
		 * duplicados regs duplicados
		 */
		boolean duplicado=false;
		/**
		 * coneccion bd
		 */
		Connection conn = null;
		/**
		 * coneccion bd
		 */
		ResultSet rs = null;
		try {
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			/*****************QUERY REGISTROS DUPLICADOS *******************/
			final String queryCount = armarqueryDuplicados(bean);
			statement = conn.prepareStatement(queryCount);
			EIGlobal.mensajePorTrace("Query duplicados :: " + queryCount, EIGlobal.NivelLog.INFO);
			String totalRegistros="";
			rs = statement.executeQuery();
			if(rs.next()){
				totalRegistros=rs.getString("TOTAL") != null ? rs.getString("TOTAL") : "0";
				if(Integer.parseInt(totalRegistros) > 0){
					duplicado=true;
				}

			}
			EIGlobal.mensajePorTrace("NominaLineaDAO.java :: validaDuplicadoNomlinArch() :: totalDuplicados: "+totalRegistros, EIGlobal.NivelLog.INFO);
		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace("NominaLineaDAO.java - validaDuplicadoNomlinArch() - "+ "Error al tratar de cerrar la conexion: [" + e1 + "]", EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("NominaLineaDAO.java - validaDuplicadoNomlinArch() - "+ "Error al tratar de cerrar la conexion::", EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (rs != null){
					rs.close();
				}
				if (statement != null) {
					statement.close();
					statement = null;
				}if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("NominaLineaDAO.java - validaDuplicadoNomlinArch() - "+ "Error al tratar de cerrar la conexion: [" + new Formateador().formatea(e1) + "]", EIGlobal.NivelLog.ERROR);
			}catch (Exception e2) {
				EIGlobal.mensajePorTrace("NominaLineaDAO.java - validaDuplicadoNomlinArch() - "+ "detalles al tratar de cerrar la conexion:: "+ new Formateador().formatea(e2), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("NominaLineaDAO.java - validaDuplicadoNomlinArch() :: duplicado: "+duplicado, EIGlobal.NivelLog.ERROR);
		return duplicado;
	}

	/**
	 * armarqueryDuplicados valida registros duplicados
	 * @param consulta datos de entrada
	 * @return query cadena con query
	 */
	private String armarqueryDuplicados(NominaLineaBean consulta) {
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryDuplicados() :: contrato: "+consulta.getContrato(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryDuplicados() :: num_regs: "+consulta.getNumRegistros(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("NominaLineaDAO.java :: armarqueryDuplicados() :: importe : "+consulta.getImporteRecibido(), EIGlobal.NivelLog.INFO);

		/**
		 * query a ejecutar
		 */
		final StringBuffer query = new StringBuffer();
		query.append( "SELECT COUNT(1) AS TOTAL " );
		query.append( " FROM EWEB_NOMLIN_ARCH" );
		query.append( " WHERE CONTRATO='" );
		query.append( consulta.getContrato() );
		query.append( "'" );
		query.append( " AND NUM_REG=" );
		query.append( consulta.getNumRegistros() );
		query.append( " AND IMPORTE_REC=" );
		query.append( consulta.getImporteRecibido() );
		query.append( " AND trunc(FCH_CARGA) >= TO_DATE('" );
		query.append( consulta.getFchCarga() );
		query.append( "', 'dd/mm/yyyy')" );

		return query.toString();
	}

}