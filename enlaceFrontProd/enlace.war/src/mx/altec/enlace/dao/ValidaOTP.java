package mx.altec.enlace.dao;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraOTP;
import mx.altec.enlace.bo.DatosManc;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;


/*
 * ValidaOTP.java
 *
 * Created on 7 de octubre de 2006, 12:20 AM
 * @author Lic. Alexander Villalobos Yadr�
 *
 */

public class ValidaOTP {
	/**
	 * Constante para la literal bitacora
	 */
	public static final String BITACORA = "bitacora";
	/**
	 * Constante para la literal parametros
	 */
	public static final String PARAMETROS = "parametros";
	/**
	 * Constante para la literal atributos
	 */
	public static final String ATRIBUTOS = "atributos";
	/**
	 * Constante para la literal fechaActual
	 */
	public static final String FECHACT = "fechaActual";
	/**
	 * Constante para la literal "con un monto total de"
	 */
	public static final String MONTOTOTAL = " con un monto total de ";
	/**
	 * Constante para la literal "�Desea cancelar en mancomunidad "
	 */
	public static final String CANCELAMANC = "�Desea cancelar en mancomunidad ";
	/**
	 * Constante para la literal "FolioParam"
	 */
	public static final String FOLIOPARAM = "FolioParam";
	/**
	 * Constante para la literal " registro con un importe de "
	 */
	public static final String CONIMPORTE = 	" registro con un importe de ";
	/**
	 * Constante para la literal "session"
	 */
	public static final String SESSION = 		"session";
	/**
	 * Constante para la literal "vacio"
	 */
	public static final String VACIO = 			"vacio";
	/**
	 * Constante para la literal "cuenta"
	 */
	public static final String CUENTA = "cuenta";
	/**
	 * Constante para la literal " con un valor de "
	 */
	public static final String CONVALOR = " con un valor de ";
	/**
	 * Constante para la literal "importe";
	 */
	public static final String IMPORTE = "importe";
	/**
	 * Constante para la literal " registros con un importe de ";
	 */
	public static final String SCONIMPORTE = " registros con un importe de ";
	/**
	 * Constante para la literal "�Desea autorizar a mancomunidad ";
	 */
	public static final String AUTORIZARMANC = "�Desea autorizar a mancomunidad ";
	/**
	 * Constante para la literal "arregloctas_destino2_string";
	 */
	public static final String ARRCTASDESTSTR = "arregloctas_destino2_string";
	/**
	 * Select para consultar constrao
	 */
	public static String sSQLCONTRATO = "Select VALOR from EWEB_OTP_ADMINISTRACION where ID_TRANSACCION = ? and NUM_CUENTA2 = ?";
	/**
	 * Select para obtener valor por default
	 */
	public static String sSQLGENERAL = "Select VDEFAULT from EWEB_OTP_PARAM_TRANS where ID_TRANSACCION = ?";
	/**
	 * Select para consultar referencia
	 */
	public static String sSQLCONSULTAREFERENCIA = "Select MAX(REFERENCIA) from TCT_BITACORA where TRIM(NUM_CUENTA) = ? and FECHA = ? and TRIM(USUARIO) = ? and REFERENCIA not in (Select REFERENCIA from TCT_BITACORA_AUDIT where FECHA = ? and REFERENCIA is not null)";
	/**
	 * Insert para registrar en bitacora
	 */
	public static String sSQLINSERTBITACORA = "insert into TCT_BITACORA_AUDIT(FECHA,HORA,IDSESSION,NUM_CUENTA,DIRECCION_IP,CANAL,USUARIO,ID_OPERACION,HOSTNAME,IDSERIE,DESCRIPCION,TRANSACCION,NUM_CONSEC,INSTANCIA) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Update para bitacora
	 */
	public static String sSQLUPDATEBITACORA = "update TCT_BITACORA_AUDIT set REFERENCIA = ?,TRANSACCION = ? where FECHA = ? and HORA = ? and IDSESSION = ?";

	/**
	 * Constante con el valor importe transferencia
	 */
	public static final String IMPORTE_TRAN = "importeTran";
	
	/**
	 * Metodo para guardar parametros en sesion
	 * @param request peticion del clinte
	 */
	public static void guardaParametrosEnSession (HttpServletRequest request) {
		String template= request.getServletPath();
		request.getSession().setAttribute("plantilla",template);
		request.getSession().removeAttribute(BITACORA);
		HttpSession session = request.getSession(false);

		if(session != null){
			Map tmp = new HashMap();
			Enumeration enumer = request.getParameterNames();

			while(enumer.hasMoreElements()) {
				String name = (String)enumer.nextElement();
				tmp.put(name,request.getParameter(name));
			}

			session.setAttribute(PARAMETROS,tmp);
			tmp = new HashMap();
			enumer = request.getAttributeNames();

			while(enumer.hasMoreElements()){
				String name = (String)enumer.nextElement();
				tmp.put(name,request.getAttribute(name));
			}
			session.setAttribute(ATRIBUTOS,tmp);
		}
	}
	/**
	 * Metodo para reestablecer los parametros en sesion
	 * @param request peticion del cliente
	 * @return String param
	 */
	public static String reestableceParametrosEnSession(HttpServletRequest request){
		String param = "";
		HttpSession session = request.getSession(false);

		if(session != null){
			Map tmp = (Map)session.getAttribute(ATRIBUTOS);
			Iterator it = null;
			if(tmp != null && tmp.keySet() != null){
				it = tmp.keySet().iterator();
				while(it.hasNext()){
					String key = (String)it.next();
					request.setAttribute(key,tmp.get(key));
				}
			}
			tmp = (Map)session.getAttribute(PARAMETROS);
			if(tmp != null && tmp.keySet() != null){
				it = tmp.keySet().iterator();
				while(it.hasNext()){
					String key = (String)it.next();
					param += key + "=" + (String)tmp.get(key) +"&" ;
				}
			}
		}

		session.removeAttribute(ATRIBUTOS);
		session.removeAttribute(PARAMETROS);
		return param;
	}
	/**
	 * Metodo para reestablecer parametros en sesion encode
	 * @param request peticion del cliente
	 * @return String cadena param
	 */
	public static String reestableceParametrosEnSessionEncoded(HttpServletRequest request){
		StringBuffer param = new StringBuffer();
		HttpSession session = request.getSession(false);
//VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace
		String enc="UTF-8";
//VSWF RRG 08-Dic-2008 F

		if(session != null){
			Map tmp = (Map)session.getAttribute(ATRIBUTOS);
			Iterator it = null;
			if(tmp != null && tmp.keySet() != null){
				it = tmp.keySet().iterator();
				while(it.hasNext()){
					String key = (String)it.next();
					request.setAttribute(key,tmp.get(key));
				}
			}
			tmp = (Map)session.getAttribute(PARAMETROS);
			if(tmp != null && tmp.keySet() != null){
				it = tmp.keySet().iterator();
				while(it.hasNext()){
					String key = (String)it.next();
//					VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace
					try {
						EIGlobal.mensajePorTrace("Valor key: "+ tmp.get(key), EIGlobal.NivelLog.INFO);
						if(tmp.get(key)!=null){
							param.append(key);
							param.append('=');
							param.append(java.net.URLEncoder.encode((String)tmp.get(key),enc));
							param.append('&');
//							+= key + "=" + java.net.URLEncoder.encode((String)tmp.get(key),enc) +"&" ;
						}else{
							param.append(key);
							param.append('=');
							param.append((String)tmp.get(key));
							param.append('&');
//							param += key + "=" + (String)tmp.get(key) +"&" ;
						}
					} catch (UnsupportedEncodingException e) {
						// TODO Bloque catch generado autom�ticamente
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
//					VSWF RRG 08-Dic-2008 F
				}
			}
		}

		session.removeAttribute(ATRIBUTOS);
		session.removeAttribute(PARAMETROS);
		return param.toString();
	}

	/**
	 * Metodo validaOTP
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @param path de String
	 * @return boolean siempre verdadero
	 * @throws IOException en caso de error
	 */
	public static boolean validaOTP(HttpServletRequest request, HttpServletResponse response, String path) throws IOException{

		EIGlobal.mensajePorTrace(" Entro a validaOTP->" , EIGlobal.NivelLog.DEBUG);
		HttpSession session = request.getSession();
		BaseResource br = (BaseResource)session.getAttribute(SESSION);
		session.setAttribute(SESSION,br);
		/**
		 * STEFANINI AGOSTO 2015 
		 */
	//	String tipoCargo =  (String) request.getAttribute("TipoCargoIG");
		
 		//EIGlobal.mensajePorTrace( "ValidaOTP  - TIPO CARGO: "+tipoCargo, EIGlobal.NivelLog.INFO ); 
 						
		//Global.mensajePorTrace( "VALIDA OTP " +tipoCargo , EIGlobal.NivelLog.INFO );

		EIGlobal.mensajePorTrace(" Salio a validaOTP->" , EIGlobal.NivelLog.DEBUG);
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
		if ("/jsp/MMC_AltaArchivo.jsp".equals(path)
				|| "/jsp/transferencia_val_duplicados.jsp".equals(path)
				|| "/jsp/MDI_Cotizar.jsp".equals(path)
				|| "/jsp/MantenimientoNominaLn.jsp".equals(path)
				|| "/jsp/MantenimientoNomina.jsp".equals(path)) {
			try {
				EIGlobal.mensajePorTrace("3 === PYME", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("path::"+path+"::", EIGlobal.NivelLog.INFO);
				if( "/jsp/MMC_AltaArchivo.jsp".equals(path) ){
					request.setAttribute("urlCancelar", "/Enlace/"+Global.WEB_APPLICATION+"/MMC_Alta?Modulo=0&" +
							BitaConstants.FLUJO +"=" + BitaConstants.EA_CUENTAS_ALTA);
					EIGlobal.mensajePorTrace("ValidaOTP::MMC_AltaArchivo.jsp::urlCancelar::"+request.getAttribute("urlCancelar")+"::", EIGlobal.NivelLog.INFO);
				} else if( "/jsp/MDI_Cotizar.jsp".equals(path) ){
					request.setAttribute("urlCancelar", "/Enlace/"+Global.WEB_APPLICATION+"/MDI_Interbancario?Modulo=0&" +
							BitaConstants.FLUJO +"=" + BitaConstants.ET_INTERBANCARIAS);
				} else if( path.indexOf("MantenimientoNomina") != -1 ){
					request.setAttribute("urlCancelar", "/Enlace/"+Global.WEB_APPLICATION+"/InicioNominaLn?Modulo=0&" +
							BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINALN_PAGOS_IMP_ENVIO + "&submodulo=2");
				} else if( "/jsp/transferencia_val_duplicados.jsp".equals(path) && "0".equals(request.getParameter("trans")) ){
					request.setAttribute("urlCancelar", "/Enlace/"+Global.WEB_APPLICATION+"/transferencia?ventana=0&" +
							BitaConstants.FLUJO +"=" + BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA);
				} else if( "/jsp/transferencia_val_duplicados.jsp".equals(path) && "2".equals(request.getParameter("trans")) ){
					request.setAttribute("urlCancelar", "/Enlace/"+Global.WEB_APPLICATION+"/transferencia?ventana=3&" +
							BitaConstants.FLUJO +"=" + BitaConstants.ET_TARJETA_CREDITO_PAGO);
				}
				request.getRequestDispatcher(path).forward(request, response);
			} catch (ServletException e) {
				EIGlobal.mensajePorTrace(" validaOTP->".concat(e.getMessage()) , EIGlobal.NivelLog.DEBUG);
			} catch(IllegalStateException e){
				EIGlobal.mensajePorTrace(" validaOTP->".concat(e.getMessage()) , EIGlobal.NivelLog.DEBUG);
			}
		} else {		
			response.sendRedirect(response.encodeRedirectURL("/Enlace/"+path));
			
		}
		/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */

		return true;
	}

	/**
	 * Metodo validaOTPConfirm
	 * @param request de tipo HttpServletRequest
	 * @param response de tipo HttpServletResponse
	 * @param path de String
	 * @return boolean siempre verdadero
	 * @throws IOException en caso de error
	 */
	public static boolean validaOTPConfirm(HttpServletRequest request, HttpServletResponse response, String path) throws IOException{

		HttpSession session = request.getSession();
		BaseResource br = (BaseResource)session.getAttribute(SESSION);
		session.setAttribute(SESSION,br);
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		return true;
	}

	/**
	 * Metodo formatoNumero
	 * @param importeS de tipo String
	 * @return String importeS
	 */
	public static String formatoNumero (String importeS) {

		DecimalFormatSymbols separadores = new DecimalFormatSymbols();
		separadores.setDecimalSeparator('.');
		separadores.setGroupingSeparator(',');

		DecimalFormat formato = new DecimalFormat("$###,###,###,###,###.00", separadores);
		importeS = importeS.replaceAll("@", "");
		importeS = importeS.replaceAll(",", "");

		try {
			double importe = Double.parseDouble(importeS.trim());
			importeS = formato.format(importe);
		} catch (Exception e) {

		}

		return importeS;
	}

	/**
	 * Metodo para cortar cadena
	 * @param cadena de tipo String
	 * @param car de tipo char
	 * @param cant de tipo int
	 * @return String envio
	 */
	public static String cortaCadena (String cadena, char car, int cant) {
	int ini = 0;
	String envio = "";
		try {
			int result = 0, pos = 0;
			for (int i = 0; i < cant; i++) {
				result = cadena.indexOf(car, pos);
				pos = result + 1;
				if (i == cant - 2){
					ini = pos;
				}
			}
			envio = cadena.substring(ini, result);
		} catch (Exception e) {

		}
		return envio;
	}
	/**
	 * Metodo para formatear numero en cantidad
	 * @param importe a dar formato moneda
	 * @return String cadena num formateada
	 */
	public static String formatoNumero (double importe) {

		DecimalFormatSymbols separadores = new DecimalFormatSymbols();
		separadores.setDecimalSeparator('.');
		separadores.setGroupingSeparator(',');

		DecimalFormat formato = new DecimalFormat("$###,###,###,###,###.00", separadores);

		return formato.format(importe);
	}

	/**
	 * Metodo para obtener formato de fecha, de valor numerico a cadena de
	 * texto.
	 *
	 * @param mes valor numerico indicando el mes.
	 * @param anio cadena de texto con el anio correspondiente al mes.
	 * @return String con la fecha formateada.
	 */
	public static String formatoFecha (int mes,String anio){
		String mesTexto="";
		switch (mes) {
			case 1 : mesTexto="ENE";break;
			case 2 : mesTexto="FEB";break;
			case 3 : mesTexto="MAR";break;
			case 4 : mesTexto="ABR";break;
			case 5 : mesTexto="MAY";break;
			case 6 : mesTexto="JUN";break;
			case 7 : mesTexto="JUL";break;
			case 8 : mesTexto="AGO";break;
			case 9 : mesTexto="SEP";break;
			case 10: mesTexto="OCT";break;
			case 11: mesTexto="NOV";break;
			case 12: mesTexto="DIC";break;
		}
		return String.format("%s-%s",mesTexto,anio);
	}
	/**
	 * Metodo para definir el mensaje a mostrar dependiendo del servlet
	 * @param request peticion del cliente
	 * @param servlet parametro que identifica al servlet
	 * @return String mensaje
	 */
	public static String mensajeOTP(HttpServletRequest request, String servlet) {

		HttpSession session = request.getSession(false);

		String mensaje = VACIO;
		StringBuffer sbMensaje = new StringBuffer();

		try {

			if(session != null){
				Map tmpParametros = (HashMap) session.getAttribute(PARAMETROS);
				Map tmpAtributos = (HashMap) session.getAttribute(ATRIBUTOS);

				session.setAttribute("parametrosBitacora", tmpParametros);
				session.setAttribute("atributosBitacora", tmpAtributos);

				if("OrdenPago".equals(servlet)){

					String cadena = tmpAtributos.get("Arreglo_Importes_string").toString();

					if(cadena.split("@").length > 1){

						String arregloImportes[] = cadena.split("@");
						double importeTotal = 0;

						for(int nImporte = 0; nImporte < arregloImportes.length; nImporte++){
							importeTotal = importeTotal + Double.parseDouble(arregloImportes[nImporte]);
						}

						mensaje = "Le recordamos que la comisi&oacute;n e IVA generada por la aplicaci�n de sus transacciones se cobrar&aacute;n en l&iacute;nea. "
									.concat("�Desea realizar ").concat(Integer.toString(arregloImportes.length)).concat(" transferencias con un monto total de ").concat(formatoNumero(importeTotal)).concat("?");

					} else {

						mensaje = "Le recordamos que la comisi&oacute;n e IVA generada por la aplicaci�n de su transacci&oacute;n se cobrar&aacute; en l&iacute;nea. "
									.concat("�Desea transferir ").concat(formatoNumero(cadena)).concat(" de la cuenta ").concat(tmpAtributos.get("Arreglo_Cuentas_Cargo_string").toString()).concat("?");
					}
					mensaje = mensaje.replaceAll("@", "");
				}

				if("ImpProv".equals(servlet)){
					String cuentaS = "";
					String importeS = "";

					if (tmpAtributos.get(CUENTA) != null) {
						cuentaS = tmpAtributos.get(CUENTA).toString();
					} else {
						cuentaS = tmpParametros.get(CUENTA).toString();
					}

					String cuenta = cuentaS.substring(0, cuentaS.indexOf('|'));

					if (tmpAtributos.get("paramImp") != null) {
						importeS = tmpAtributos.get("paramImp").toString();
					} else {
						importeS = tmpParametros.get("paramImp").toString();
					}

					String importe = importeS.substring(importeS.indexOf("Total efectivamente pagado:"));
					importe = importe.substring(27,importe.indexOf('|'));

					mensaje = "�Desea realizar un Pago de Impuesto de la cuenta ".concat(cuenta).concat(CONVALOR).concat(formatoNumero(importe)).concat(" ?");
				}

				if("ImpLC".equals(servlet)){
					String lineaCap = tmpParametros.get("concepto").toString();

					String importe = tmpParametros.get(IMPORTE).toString();

					mensaje = "�Desea realizar un Pago de Impuesto con L�nea de Captura ".concat(lineaCap).concat(CONVALOR).concat(formatoNumero(importe)).concat(" ?");
				}

				//SIPARE
				if("SipareLC".equals(servlet)){

					String importe = tmpParametros.get(IMPORTE).toString();

					mensaje = "�Desea realizar el pago de SUA L�nea de Captura con un monto total de ".concat(formatoNumero(importe)).concat(" MN?");

				}

				if("PagoNomina".equals(servlet)){
					String registros = "";
					String importe = "";

					if (session.getAttribute("noRegistrosNomina") != null) {
						registros = session.getAttribute("noRegistrosNomina").toString();
					}

					if (session.getAttribute("sumTmpSession") != null) {
						importe = session.getAttribute("sumTmpSession").toString();
					}

					mensaje = "�Desea programar el archivo de Pago de N�mina con ";

					if ("1".equals(registros.trim())) {
						mensaje = mensaje.concat(" 1 registro con un valor de ").concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = mensaje.concat(registros).concat(" registros con un valor de ").concat(formatoNumero(importe)).concat("?");
					}
				}

				if("PrePagoNomina".equals(servlet)){
					String registros = "";
					String importe = "";

					if (session.getAttribute("importePrePagoS") != null) {
						importe = session.getAttribute("importePrePagoS").toString().trim();
					}

					if (session.getAttribute("registrosPrePagoS") != null) {
						registros = session.getAttribute("registrosPrePagoS").toString().trim();
					}

					mensaje = "�Desea programar el archivo de Tarjeta de Pagos Santander  con ";//YHG NPRE

					if ("1".equals(registros.trim())) {
						mensaje = mensaje.concat(" 1 registro con un valor de ").concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = mensaje.concat(registros).concat(" registros con un valor de ").concat(formatoNumero(importe)).concat("?");
					}
				}

				if("NominaInter".equals(servlet)){

					String importe = "";
					String cuenta = "";
					String registros = "";

					cuenta = tmpAtributos.get("textcuenta").toString();
					importe = session.getAttribute("importeNISes").toString().trim();

					if (tmpAtributos.get("importeNIReq")!=null && tmpAtributos.get("registrosNIReq")!=null) {
						registros = tmpAtributos.get("registrosNIReq").toString();
					} else {
						registros = session.getAttribute("registrosNISes").toString();
					}

					registros = String.valueOf(Integer.parseInt(registros.trim()));

					mensaje = "�Desea programar el archivo de Pago de N�mina Interbancaria con ";

					if ("1".equals(registros)) {
						mensaje = mensaje.concat("1 registro con un valor de ").concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = mensaje.concat(registros).concat(" registros con un valor de ").concat(formatoNumero(importe)).concat("?");
					}

				}

				if("PagoIndividual".equals(servlet)){

					String importe = session.getAttribute("importe1").toString().trim();
					String cuenta = session.getAttribute("num_cuenta").toString().trim();

					mensaje = "�Desea programar el pago individual desde la cuenta ".concat(cuenta).concat(CONVALOR).concat(formatoNumero(importe)).concat("?");
				}

				if("ChesLinea".equals(servlet)){
					String importe = tmpParametros.get("txtImporte").toString();
					String cuenta = tmpParametros.get("textcboCuentaCargo").toString();

					if (importe.indexOf('.') < 0){
						importe = importe.concat(".0");
					}

					mensaje = "�Desea registrar el cheque con cuenta ".concat(cuenta).concat(CONVALOR).concat(formatoNumero(importe)).concat("?");
				}

				if("ChesArchivo".equals(servlet)){

					String importe = session.getAttribute("importeCheqR").toString().trim();
					String registros = session.getAttribute("registrosCheqR").toString().trim();

					String importe1 = importe.substring(0, importe.length()-2);
					String importe2 = importe.substring(importe.length()-2);

					importe = importe1.concat(".").concat(importe2);

					if ("1".equals(registros)) {
						mensaje = "�Desea registrar 1 cheque con un valor de ".concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = "�Desea registrar ".concat(registros).concat(" cheques con un valor de ").concat(formatoNumero(importe)).concat("?");
					}

				}

				if("ManGeneraFolios".equals(servlet)){
					String importe = tmpParametros.get(IMPORTE).toString();
					String cuenta = tmpParametros.get(CUENTA).toString();;
					String operacion = tmpParametros.get("desc_operacion").toString();

					if (importe.indexOf('.') < 0){
						importe = importe.concat(".0");
					}

					mensaje = "�Desea generar el Folio de ".concat(operacion).concat(" con cuenta ").concat(cuenta).concat(CONVALOR).concat(formatoNumero(importe)).concat("?");
				}
				if("AutorizaManc_A_T".equals(servlet)){
					String noRegistros = (String) request.getSession().getAttribute("regManc");
					String importeS = (String) request.getSession().getAttribute("impManc");;

					double importe = Double.parseDouble(importeS);

					if (Integer.parseInt(noRegistros) > 1){
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(SCONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(CONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					}
				}

				if("AutorizaManc_A".equals(servlet)){
					String registros[] = (String []) session.getAttribute(FOLIOPARAM);
					int noRegistros = registros.length;
					double importe = 0.0;

					List listaResManc = new ArrayList(registros.length);
					List resultadosMancomunidad = (ArrayList)session.getAttribute("ResultadosMancomunidad");

					for (int x = 0; x < registros.length; x++) {
						try {
							int temp = Integer.parseInt(registros[x]);
							listaResManc.add(resultadosMancomunidad.get(temp));
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}

					for (int aux=0; aux<registros.length; aux++) {
						DatosManc folioParamTmp = (DatosManc) listaResManc.get(aux);
						importe = importe + Double.parseDouble(folioParamTmp.getImporte().trim());
					}

					if (noRegistros > 1){
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(SCONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(CONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					}
				}

				if("AutorizaManc_C".equals(servlet)){
					String registros[] = (String []) session.getAttribute(FOLIOPARAM);
					int noRegistros = registros.length;
					double importe = 0.0;

					List listaResManc = new ArrayList(registros.length);
					List resultadosMancomunidad = (ArrayList)session.getAttribute("ResultadosMancomunidad");

					for (int x = 0; x < registros.length; x++) {
						try {
							int temp = Integer.parseInt(registros[x]);
							listaResManc.add(resultadosMancomunidad.get(temp));
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}

					for (int aux=0; aux<registros.length; aux++) {
						DatosManc folioParamTmp = (DatosManc) listaResManc.get(aux);
						importe = importe + Double.parseDouble(folioParamTmp.getImporte().trim());
					}

					if (noRegistros > 1){
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(SCONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(CONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					}
				}
				if("AutorizaManc_A_SA".equals(servlet)){

			    	String[] registros=(String[])request.getSession().getAttribute(FOLIOPARAM);

					int noRegistros = registros.length;
					double importe = 0.0;

					List listaResManc = new ArrayList(registros.length);
					List resultadosMancomunidad = (ArrayList) request.getSession().getAttribute("ResultadosMancDetalle");

					for (int x = 0; x < registros.length; x++) {
						try {
							int temp = Integer.parseInt(registros[x]);
							listaResManc.add(resultadosMancomunidad.get(temp));
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}

					for (int aux=0; aux<registros.length; aux++) {
						DatosMancSA folioParamTmp = (DatosMancSA) listaResManc.get(aux);
						importe = importe + Double.parseDouble(folioParamTmp.getImporte().trim());
					}

					if (noRegistros > 1){
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(SCONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(CONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					}
				}

				if("AutorizaManc_C_SA".equals(servlet)){
			    	String[] registros=(String[])request.getSession().getAttribute(FOLIOPARAM);
					int noRegistros = registros.length;
					double importe = 0.0;

					List listaResManc = new ArrayList(registros.length);
					List resultadosMancomunidad = (ArrayList) request.getSession().getAttribute("ResultadosMancDetalle");

					for (int x = 0; x < registros.length; x++) {
						try {
							int temp = Integer.parseInt(registros[x]);
							listaResManc.add(resultadosMancomunidad.get(temp));
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					}

					for (int aux=0; aux<registros.length; aux++) {
						DatosMancSA folioParamTmp = (DatosMancSA) listaResManc.get(aux);
						importe = importe + Double.parseDouble(folioParamTmp.getImporte().trim());
					}

					if (noRegistros > 1){
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(SCONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					} else {
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(CONIMPORTE)
									.concat(formatoNumero(importe)).concat("?");
					}
				}
				if("AutorizaManc_A_SA_A".equals(servlet)){

					ArrayList listResultArch = ((ArrayList) request.getSession().getAttribute("ResultadosMancomunidadSA"));
					DatosMancSA datosArchivo = null;
					int indiceInt = -1;

					EIGlobal.mensajePorTrace("�������������������� extraeOperaciones indiceArchivoSession ->" + request.getSession().getAttribute("indiceArchivoSession"), EIGlobal.NivelLog.DEBUG);

					if (request.getSession().getAttribute("indiceArchivoSession")!=null) {
						indiceInt = Integer.parseInt((String) request.getSession().getAttribute("indiceArchivoSession"));
					}

					datosArchivo = (DatosMancSA) listResultArch.get(indiceInt);

					if (Integer.parseInt(datosArchivo.getRegistros()) > 1){
						mensaje = AUTORIZARMANC.concat(datosArchivo.getRegistros()).concat(SCONIMPORTE)
									.concat(formatoNumero(datosArchivo.getImporte())).concat("?");
					} else {
						mensaje = AUTORIZARMANC.concat(datosArchivo.getRegistros()).concat(CONIMPORTE)
									.concat(formatoNumero(datosArchivo.getImporte())).concat("?");
					}
				}

				if("Manc_Op_Inter_A".equals(servlet)){
					String noRegistros = session.getAttribute("registrosMancOI").toString().trim();

					if ("1".equals(noRegistros)) {
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(" operaci�n internacional?");
					} else {
						mensaje = AUTORIZARMANC.concat(String.valueOf(noRegistros)).concat(" operaciones internacionales?");
					}
				}

				if("Manc_Op_Inter_C".equals(servlet)){
					String noRegistros = session.getAttribute("registrosMancOI").toString().trim();

					if ("1".equals(noRegistros)) {
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(" operaci�n internacional?");
					} else {
						mensaje = CANCELAMANC.concat(String.valueOf(noRegistros)).concat(" operaciones internacionales?");
					}
				}

				if("AltaCuenta".equals(servlet)){
					/** CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia tmpParametros por tmpAtributos*/
					String registros="";
					/**inicio de implementacion de validacion para que el modulo de alta de cuentas no truene**/
					try {
						Class.forName("mx.altec.enlace.dao.ConsultaCtasDAO");
						registros = tmpAtributos.get("totalRegistros").toString();
						EIGlobal.mensajePorTrace("ValidaOTP AltaCuenta - Nuevo", EIGlobal.NivelLog.DEBUG);
					} catch (ClassNotFoundException e) {
						registros = tmpParametros.get("totalRegistros").toString();
						EIGlobal.mensajePorTrace("ValidaOTP AltaCuenta - Anterior", EIGlobal.NivelLog.DEBUG);
					}
					/**fin de implementacion de validacion para que el modulo de alta de cuentas no truene**/
					if ("1".equals(registros)) {
						mensaje = "�Desea enviar el archivo de Alta de Cuentas con ".concat(registros).concat(" registro?");
					} else {
						mensaje = "�Desea enviar el archivo de Alta de Cuentas con ".concat(registros).concat(" registros?");
					}
				}

				if("PagoProv".equals(servlet)){
					String noRegistros = "";
					String importeTotal = "";

					noRegistros = session.getAttribute("registrosCfmg").toString().trim();
					importeTotal = session.getAttribute("importeCfmg").toString().trim();

					if ("1".equals(noRegistros)){
						mensaje = "�Desea enviar el archivo de Pago de Proveedores con ".concat(noRegistros).concat(" registro con un importe total de ").concat(formatoNumero(importeTotal)).concat("?");
					} else {
						mensaje = "�Desea enviar el archivo de Pago de Proveedores con ".concat(noRegistros).concat(" registros con un importe total de ").concat(formatoNumero(importeTotal)).concat("?");
					}
				}

				if("PagoDirLinea".equals(servlet)){
					String ctaCargo = tmpParametros.get("CuentaCargo").toString();
					String importeTotal = tmpParametros.get("Importe").toString();

					if (importeTotal.indexOf('.') < 0){
						importeTotal = importeTotal.concat(".0");
					}

					mensaje = "�Desea realizar el Pago Directo en Linea de la cuenta ".concat(ctaCargo).concat(" con un importe total de ").concat(formatoNumero(importeTotal)).concat("?");
				}

				if("PagoDirArchivo".equals(servlet)){
					String aceptados = session.getAttribute("registrosAceptados").toString();
					String importeTotal = session.getAttribute("importeTotal").toString();

					if (importeTotal.indexOf('.') < 0){
						importeTotal = importeTotal.concat(".0");
					}

					if ("1".equals(aceptados)) {
						mensaje = "�Desea enviar el archivo de Pago Directo con ".concat(aceptados).concat(" registro con un importe total de ").concat(formatoNumero(importeTotal)).concat("?");
					} else {
						mensaje = "�Desea enviar el archivo de Pago Directo con ".concat(aceptados).concat(" registros con un importe total de ").concat(formatoNumero(importeTotal)).concat("?");
					}
				}

				if("TarjetaPago".equals(servlet)){
					/**
					 * PYME FSW INDRA Unificacion Token
					 */
					int numRegistros = Integer.parseInt( session.getAttribute("registrosTran") == null ? session.getAttribute("RegTotalPagoTarjeta").toString().trim() : session.getAttribute("registrosTran").toString().trim() );
					double total = Double.parseDouble( session.getAttribute(IMPORTE_TRAN) == null ? session.getAttribute("ImporteTotalPagoTarjetaToken").toString().trim() : session.getAttribute(IMPORTE_TRAN).toString().trim() );

					if (!"".equals(tmpAtributos.get(ARRCTASDESTSTR).toString())) {
						numRegistros = tmpAtributos.get(ARRCTASDESTSTR).toString().split("@").length;
					}

					if(numRegistros > 1){

						mensaje = "�Desea realizar ".concat(String.valueOf(numRegistros)).concat(" Pagos a Tarjeta de Cr�dito ").concat(MONTOTOTAL).concat(formatoNumero(total)).concat("?");

					} else {
						String cta_destino = tmpAtributos.get(ARRCTASDESTSTR).toString();
						cta_destino = cta_destino.substring(0, cta_destino.indexOf('|'));

						mensaje = "�Desea realizar el Pago a Tarjeta de Cr�dito a la cuenta ".concat(cta_destino).concat(MONTOTOTAL).concat(formatoNumero(total)).concat("?");
					}
					mensaje = mensaje.replaceAll("@", "");
				}

				if("TarjetaDisposicion".equals(servlet)){

					String importe = tmpParametros.get("montostring").toString();
					String cta_destino = tmpParametros.get("destino").toString();
					cta_destino = cta_destino.substring(0, cta_destino.indexOf('|'));

					mensaje = "�Desea realizar la transferencia de Disposici�n de Tarjeta de Cr�dito a la cuenta ".concat(cta_destino).concat(MONTOTOTAL).concat(formatoNumero(importe)).concat("?");
				}

				if("SUA_LC".equals(servlet)){
					String importe = tmpParametros.get(IMPORTE).toString();
					String cta_cargo = tmpParametros.get("cargo").toString();

					mensaje = "�Desea realizar la transferencia SUA Linea de Captura de la cuenta ".concat(cta_cargo).concat(MONTOTOTAL).concat(formatoNumero(importe)).concat("?");
				}

				if("SUA_Disco".equals(servlet)){
					String importe = tmpParametros.get("total_a_pagar").toString();
					String cta_cargo = tmpParametros.get("cboCuentaCargo").toString();

					cta_cargo = cta_cargo.substring(0, cta_cargo.indexOf('|'));

					mensaje = "�Desea realizar el pago SUA de la cuenta ".concat(cta_cargo).concat(MONTOTOTAL).concat(formatoNumero(importe)).concat("?");
				}

				if("PagoSAR".equals(servlet)){

					String importe = tmpParametros.get("txtTotal").toString().trim();
					String cuenta = tmpParametros.get(CUENTA).toString().trim();

					importe.replaceAll("$", "");
					importe.replaceAll(",", "");

					cuenta = cuenta.substring(0, cuenta.indexOf('|'));

					mensaje = "�Desea realizar el pago SAR de la cuenta ".concat(cuenta).concat(MONTOTOTAL).concat(formatoNumero(importe)).concat("?");
				}





				if("MisTransferenciaMoneda Nacional".equals(servlet)){

					int numRegistros = Integer.parseInt(session.getAttribute("registrosTran").toString().trim());
					double total = Double.parseDouble(session.getAttribute(IMPORTE_TRAN).toString().trim());

					if (!"".equals(tmpAtributos.get(ARRCTASDESTSTR).toString())) {
						numRegistros = tmpAtributos.get(ARRCTASDESTSTR).toString().split("@").length;
					}

					if( numRegistros > 1 ){
						mensaje = "�Desea realizar una Transferencia en Linea de ".concat(String.valueOf(numRegistros)).concat(" registros con un valor total de ");
					} else {
						String ctaDestino = tmpAtributos.get(ARRCTASDESTSTR).toString();
						ctaDestino = ctaDestino.substring(0,ctaDestino.indexOf('|'));
						String strFechaEnviada = tmpAtributos.get("arreglofechas2_string").toString().replaceAll("@", "");
						String strFechaHoy = "";

						if (session.getAttribute(FECHACT) != null) {
							strFechaHoy = session.getAttribute(FECHACT).toString();
						} else {
							strFechaHoy = strFechaEnviada;
						}

						if (!strFechaEnviada.equals(strFechaHoy)) {
							mensaje = "�Desea realizar una Transferencia Programada a la cuenta ".concat(ctaDestino).concat(MONTOTOTAL);
						} else {
							mensaje = "�Desea realizar una Transferencia en Linea a la cuenta ".concat(ctaDestino).concat(MONTOTOTAL);
						}
					}

					mensaje = mensaje.concat(formatoNumero(total)).concat(" MN ?");
				}

				if("MisTransferenciaDolares".equals(servlet)){

					int numRegistros = Integer.parseInt(session.getAttribute("registrosTran").toString().trim());
					double total = Double.parseDouble(session.getAttribute(IMPORTE_TRAN).toString().trim());

					if (!"".equals(tmpAtributos.get(ARRCTASDESTSTR).toString())) {
						numRegistros = tmpAtributos.get(ARRCTASDESTSTR).toString().split("@").length;
					}

					if( numRegistros > 1 ){
						mensaje = "�Desea realizar una Transferencia en Linea de ".concat(String.valueOf(numRegistros)).concat(" registros con un valor total de ");
					} else {
						String ctaDestino = tmpAtributos.get(ARRCTASDESTSTR).toString();
						ctaDestino = ctaDestino.substring(0,ctaDestino.indexOf('|'));
						String strFechaEnviada = tmpAtributos.get("arreglofechas2_string").toString().replaceAll("@", "");
						String strFechaHoy = "";

						if (session.getAttribute(FECHACT) != null) {
							strFechaHoy = session.getAttribute(FECHACT).toString();
						} else {
							strFechaHoy = strFechaEnviada;
						}

						if (!strFechaEnviada.equals(strFechaHoy)) {
							mensaje = "�Desea realizar una Transferencia Programada a la cuenta ".concat(ctaDestino).concat(MONTOTOTAL);
						} else {
							mensaje = "�Desea realizar una Transferencia en Linea a la cuenta ".concat(ctaDestino).concat(MONTOTOTAL);
						}
					}
					mensaje = mensaje.concat(formatoNumero(total)).concat(" USD ?");
				}

				if("TransferenciaMultiple".equals(servlet)){

					int numRegistros = 0;
					String importes = tmpAtributos.get("arregloimportes2_string").toString();

					String[] importes2 = importes.split("@");
					String ctas_Destino = tmpAtributos.get(ARRCTASDESTSTR).toString();
					String ctaDestino = ctas_Destino.substring(0,ctas_Destino.indexOf('|'));

					numRegistros = ctas_Destino.split("@").length;

					double total = 0;

					for(int n = 0; n < importes2.length; n++){
						total = total + Double.parseDouble(importes2[n]);
					}

					if( numRegistros > 1 ){
						mensaje = "�Desea realizar una Transferencia M�ltiple de ".concat(String.valueOf(numRegistros)).concat(" registros con un valor total de ");
					} else {
						mensaje = "�Desea realizar una Transferencia M�ltiple a la cuenta ".concat(ctaDestino).concat(MONTOTOTAL);
					}

					mensaje = mensaje.concat(formatoNumero(total)).concat("?");

				}

				if("TransInternac".equals(servlet)){

					String cuentaCargo = tmpParametros.get("Cuentas").toString();
					String montoDivisa = tmpParametros.get("ImporteT").toString();
					String divisa = tmpParametros.get("CveDivisa").toString();

					montoDivisa = montoDivisa.substring(montoDivisa.indexOf(':')+1).trim();

					if(cuentaCargo.contains("|")){
						cuentaCargo = cuentaCargo.substring(0,cuentaCargo.indexOf('|'));
					}

					mensaje = "�Desea realizar una Transferencia Internacional de la cuenta ".concat(cuentaCargo).concat(" con un monto de ").concat(formatoNumero(montoDivisa)+" ").concat(divisa).concat("?");
				}

				if("TransFxOnline".equals(servlet)){

					String cuentaCargo = tmpParametros.get("Cuentas").toString();
					String montoDivisa = tmpParametros.get("ImporteT").toString();
					String divisa = tmpParametros.get("CveDivisa").toString();

					montoDivisa = montoDivisa.substring(montoDivisa.indexOf(':')+1).trim();

					if(cuentaCargo.contains("|")){
						cuentaCargo = cuentaCargo.substring(0,cuentaCargo.indexOf('|'));
					}

					mensaje = "�Desea realizar una Transferencia FX Online de la cuenta ".concat(cuentaCargo).concat(" por un importe divisa de ").concat(formatoNumero(montoDivisa)+" ").concat(divisa).concat("?");
				}

				if("InitFxOnline".equals(servlet)){
					mensaje = "Validaci�n de contrase�a dinamica para entrar a la compra y venta de divisas FX Online";
				}

				if("TransInterBanc".equals(servlet)){
				    EIGlobal.mensajePorTrace( "-- ValidaOTP.mensajeOTP- TransInterBanc ***", EIGlobal.NivelLog.DEBUG);
					/** CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token se cambia tmpParametros por tmpAtributos*/
					String importe = tmpAtributos.get("Importe").toString();
					EIGlobal.mensajePorTrace( "-- ValidaOTP.mensajeOTP- importe ***" + importe, EIGlobal.NivelLog.DEBUG);
					String totalTrans = tmpAtributos.get("TotalTrans").toString();
					EIGlobal.mensajePorTrace( "-- ValidaOTP.mensajeOTP- totalTrans ***" + totalTrans, EIGlobal.NivelLog.DEBUG);
					String cuentaDestino = "";
					/* VECTOR 06-2016: SPID */
					String tipoDivisaOper = session.getAttribute("DivisaMsjOper") ==  null 
					        ? String.valueOf(request.getAttribute("DivisaMsjOper")): 
					            session.getAttribute("DivisaMsjOper").toString().trim();
					
					EIGlobal.mensajePorTrace( "------------ValidaOTP.mensajeOTPmensajeOTP - 1076"
					        + "TipoDivisaOper " + tipoDivisaOper 
					        + "totalTrans" + totalTrans, EIGlobal.NivelLog.DEBUG);
					
					if(importe.contains(":")){
						importe = importe.substring(importe.indexOf(':'));
					}

					if("Archivo de Exportacion".equals(totalTrans)){

						String lineas = (String) tmpParametros.get("Lineas");

						mensaje = "�Desea enviar el archivo de Transferencia Interbancaria con ".concat(lineas);

						if("1".equals(lineas.trim())){
							mensaje = mensaje.concat(" registro ");
						}else{
							mensaje = mensaje.concat(" registros ");
						}
						/* VECTOR 06-2016: SPID */
						mensaje = mensaje.concat(CONVALOR).concat(formatoNumero(importe)).concat(" ").concat(tipoDivisaOper).concat(" ?");

					} else {
						String importeTotal = session.getAttribute("importeTIB").toString();
						String registros = session.getAttribute("registrosTIB").toString().trim();
						EIGlobal.mensajePorTrace( "------------ValidaOTP.mensajeOTPmensajeOTP - 1102"
						        + " TipoDivisaOper " + tipoDivisaOper 
						        + " importeTotal " + importeTotal 
						        + " registros " + registros , EIGlobal.NivelLog.DEBUG);

						if ("1".equals(registros)) {
							cuentaDestino = cortaCadena(totalTrans,'|', 3);

							mensaje = "�Desea realizar una Transferencia Interbancaria a la cuenta ".concat(cuentaDestino);
							/* VECTOR 06-2016: SPID */
							mensaje = mensaje.concat(" con un importe de ").concat(formatoNumero(importeTotal)).concat(" ").concat(tipoDivisaOper).concat(" ?");

						} else {
							/* VECTOR 06-2016: SPID */
							mensaje = "�Desea realizar una Transferencia Interbancaria de "
									.concat(registros)
									.concat(" registros con un importe total de ")
									.concat(formatoNumero(importeTotal))
									.concat(" ").concat(tipoDivisaOper)
									.concat(" ?");
						}
					}
				}

				if("ConfigEdoCuenta".equals(servlet)) {
					/**
					 * Se agrega validacion para cuando es Cuenta o Tarjeta
					 * Configuracion Paperless Edo Cta Individual
					 * FSW Indra 03/03/2015 PYME
					 */
					if( "003".equals(request.getParameter("tipoOp").toString()) ){
						mensaje = "�Desea modificar el estatus de env�o del Estado de Cuenta Impreso de la Tarjeta "+request.getParameter("hdTarjeta")+"?";
					} else {
						mensaje = "�Desea modificar el estatus de env�o del Estado de Cuenta Impreso de la cuenta "+request.getParameter("hdCuenta")+"?";
					}
				}

				if("ConfigMasPorCuentaServlet".equals(servlet)) {
					int ctasAfectadas=Integer.parseInt(request.getParameter("elementos").toString());
					EIGlobal.mensajePorTrace("Elementos ["+ctasAfectadas+"]", EIGlobal.NivelLog.DEBUG);
					sbMensaje.append("�Desea modificar de forma masiva el estatus de env�o de Estado de Cuenta impreso para ");
					if (ctasAfectadas == 1) {
						sbMensaje.append("la cuenta seleccionada? ");
					}
					else {
						sbMensaje.append("las ").append(ctasAfectadas).append(" cuentas seleccionadas?");
					}
					mensaje = sbMensaje.toString();
				}

				// Configuracion masiva tarjeta de credito
				// Febrero 2015
				// FSW Indra
				if("ConfigMasPorTCServlet".equals(servlet)) {
					int ctasAfectadas=Integer.parseInt(request.getParameter("elementos").toString());
					EIGlobal.mensajePorTrace("Elementos ["+ctasAfectadas+"]", EIGlobal.NivelLog.DEBUG);
					sbMensaje.append("�Desea modificar de forma masiva el estatus de env�o de Estado de Cuenta impreso para ");
					if (ctasAfectadas == 1) {
						sbMensaje.append("la tarjeta seleccionada? ");
					}
					else {
						sbMensaje.append("las ").append(ctasAfectadas).append(" tarjetas seleccionadas?");
					}
					mensaje = sbMensaje.toString();
				}
				// Fin Configuracion masiva tarjeta de credito

				if ("DescargaEdoCtaPDFServlet".equals(servlet)) {
					String cuenta=(String)request.getAttribute("cuentaOTP");
					String periodo=(String)request.getAttribute("periodoOTP");
					mensaje = cuenta.length() == 16 ? String.format("�Desea realizar la descarga del Estado de Cuenta seleccionado para la cuenta %s y el periodo %s?"
							,cuenta
							,formatoFecha(Integer.valueOf(periodo.substring(4,6)), periodo.substring(0,4))) : String.format("�Desea realizar la descarga del Estado de Cuenta seleccionado para la cuenta %s y el periodo %s?"
							,cuenta
							,formatoFecha(Integer.valueOf(periodo.substring(4,6)), periodo.substring(0,4)));
				}

				if("DescargaEdoCtaXMLServlet".equals(servlet)) {
					String cuenta=(request.getParameter("textCuentas") != null) ?
									((String)request.getParameter("textCuentas")).split(" ")[0] : " ";
					String periodo=(request.getParameter("slcPeriodos") != null ) ?
									((String)request.getParameter("slcPeriodos")).split("@")[1] : " ";
					String tipoXML=(request.getParameter("tipoXML") != null ) ? (String)request.getParameter("tipoXML") : " ";
					tipoXML=tipoXML.replace("I", "Ingreso");
					tipoXML=tipoXML.replace("E", "Egreso");
					mensaje = cuenta.length() == 16 ? String.format("�Desea realizar la descarga del Estado de Cuenta de %s seleccionado para la tarjeta %s y el periodo %s?",
							tipoXML,
							cuenta,
							formatoFecha(Integer.valueOf(periodo.substring(4,6)), periodo.substring(0,4))) : String.format("�Desea realizar la descarga del Estado de Cuenta de %s seleccionado para la cuenta %s y el periodo %s?",
							tipoXML,
							cuenta,
							formatoFecha(Integer.valueOf(periodo.substring(4,6)), periodo.substring(0,4)));
				}

				if("EstadoCuentaHistoricoServlet".equals(servlet)) {
					String cuenta=request.getParameter("cuentaTXT");
					int nperiodos=(request.getParameter("periodosSoliciar")==null) ? 0
							: request.getParameter("periodosSoliciar").substring(0, request.getParameter("periodosSoliciar").length()-1).split("\\|").length;
					String leyenda = (nperiodos <= 1)
										? "del Estado de Cuenta del periodo seleccionado"
										: String.format("de los Estados de Cuenta para los %s periodos seleccionados", nperiodos);
					mensaje = "003".equals(request.getParameter("tipoOp").toString()) ? String.format("�Desea realizar la solicitud %s para la tarjeta %s?",leyenda,cuenta)
						: String.format("�Desea realizar la solicitud %s para la cuenta %s?",leyenda,cuenta);
				}

				if("ConfigMedioNot".equals(servlet)){

					mensaje = "�Desea modificar su informaci�n para notificaciones?";
				}
				if("DesvinculacionDispositivosServlet".equals(servlet)){

					mensaje = "�Desea desvincular los dispositivos asociados a su perfil?";
				}
				if("CambioImagenServlet".equals(servlet)){

					mensaje = "�Desea cambiar la imagen asociada a su perfil?";
				}
				if("CambioPreguntaServlet".equals(servlet)){

					mensaje = "�Desea cambiar la pregunta secreta asociada a su perfil?";
				}

			}
		} catch (Exception e) {
			mensaje = VACIO;
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		if (!VACIO.equals(mensaje)) {
			session.setAttribute("mensajeSession", mensaje);
		}
		EIGlobal.mensajePorTrace("Mensaje en token ->" + mensaje, EIGlobal.NivelLog.DEBUG);

		return mensaje;
	}

	/**
	 * Metodo solicitaValidacion
	 * @param contrato de tipo String
	 * @param numMod de tipo int
	 * @return boolean que indica el resultado de la validadion
	 */
	public static boolean solicitaValidacion(String contrato, int numMod){
		Statement stmt = null;

		try{
			InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( Global.DATASOURCE_ORACLE );
			java.sql.Connection conn = ds.getConnection ();
			//System.out.println("Conexion por medio de DATASOURCE_ORACLE:" + Global.DATASOURCE_ORACLE);
			stmt = conn.createStatement();
			java.sql.ResultSet rs =  stmt.executeQuery("Select VALOR from EWEB_OTP_ADMINISTRACION where ID_TRANSACCION = " + numMod + " and TRIM(NUM_CUENTA2) = '" + contrato.trim() + "'");

			if(rs.next()){
				int valor = rs.getInt(1);
				if(valor == 1) {
					return true;
				}
				//else if(valor == 0)
				//	return false;
			}

			stmt = conn.createStatement();
			rs =  stmt.executeQuery("Select VDEFAULT from EWEB_OTP_PARAM_TRANS where ID_TRANSACCION = " + numMod);

			if(rs.next()){
				int valor = rs.getInt(1);
				if(valor == 1) {
					return true;
				}
				else if(valor == 0) {
					return false;
				}
			}

			rs.close();
			stmt.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally {
			try {
				stmt.close();
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("OperacionesProgramadasDAO.altaOperacionProgramada-" + e1.getMessage() + e1 + "]", EIGlobal.NivelLog.ERROR);
			}
		}
		return false;
	}

	/**
	 * Metodo para bitacorizar en tct_bitacora
	 * @param request de tipo HttpServletRequest
	 * @param numeroReferencia de tipo int
	 * @param cuenta de tipo String
	 * @param importe de tipo double
	 * @param tipo_operacion de tipo String
	 * @param cuenta2 de tipo String
	 * @param dispositivo2 de tipo String
	 * @param concepto de tipo String
	 * @param cant_titulos de tipo
	 * @return String con el mensaje
	 */
	public static String bitacora_Confirma_OTP(HttpServletRequest request,  int  numeroReferencia,String cuenta, double importe,
		String tipo_operacion, String cuenta2,String dispositivo2, String concepto, int cant_titulos) {

		HttpSession sessionHttp = request.getSession();
		BaseResource session = (BaseResource) sessionHttp.getAttribute(SESSION);
		String mensaje = VACIO;
		EIGlobal.mensajePorTrace( "Entro  Bitacorizacion bitacora_Confirma_OTP ####", EIGlobal.NivelLog.DEBUG);
		try {
			BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
		  	BitaTCTBean bean = new BitaTCTBean ();
		  	bean = bh.llenarBeanTCT(bean);
		  	bean.setReferencia(numeroReferencia);
		  	bean.setCuentaOrigen(cuenta);
			bean.setCuentaDestinoFondo(cuenta2);
			bean.setImporte(importe);
			bean.setConcepto("");
			if( "PROG".equals(tipo_operacion) ){
				bean.setCodError("PROG0000");
				bean.setConcepto(concepto);
			}
		  	bean.setTipoOperacion(tipo_operacion);
		  	bean.setDispositivo2(dispositivo2);
		  	EIGlobal.mensajePorTrace( "Antes insert ####", EIGlobal.NivelLog.DEBUG);
		    BitaHandler.getInstance().insertBitaTCT(bean);
		    EIGlobal.mensajePorTrace( "Despues insert ####", EIGlobal.NivelLog.DEBUG);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace( "Termino Bitacorizacion############################", EIGlobal.NivelLog.DEBUG);
		return mensaje;
	}

	/**
	 * Metodo para guardar en bitacora
	 * @param listBitacora de tipo List
	 * @param transaccion de tipo String
	 */
    public static void guardaBitacora(List listBitacora,String transaccion){

        /*try{

            if(listBitacora != null && listBitacora.size() > 0){
                BitacoraOTP bOTP = (BitacoraOTP)listBitacora.get(0);
                int referencia = -1;
                if(bOTP != null){
                    InitialContext ic = new InitialContext ();
                    DataSource ds = ( DataSource ) ic.lookup ( Global.DATASOURCE_ORACLE );

                    java.sql.Connection conn = ds.getConnection ();
                    java.sql.PreparedStatement ps = conn.prepareStatement(sSQLCONSULTAREFERENCIA);

                    ps.setString(1,bOTP.getNumCuenta());
                    ps.setDate(2,bOTP.getFecha());
                    ps.setString(3,bOTP.getUsuario());
                    ps.setDate(4,bOTP.getFecha());

                    java.sql.ResultSet rs = ps.executeQuery();

                    if(rs.next()){
                        referencia = rs.getInt(1);
                    }
                    rs.close();
                    if(referencia >= 0){
                        for (int i = 0; i < listBitacora.size(); i++){
                            bOTP = (BitacoraOTP)listBitacora.get(i);
                            ps = conn.prepareStatement(sSQLUPDATEBITACORA);
                            EIGlobal.mensajePorTrace(bOTP.getHora().substring(0,2) + bOTP.getHora().substring(3,5), EIGlobal.NivelLog.INFO);
                            ps.setInt(1,referencia);
                            ps.setString(2,transaccion);
                            ps.setDate(3,bOTP.getFecha());
                            ps.setString(4,bOTP.getHora());
                            ps.setString(5,bOTP.getIdSession());
                            ps.execute();

                        }
                    }
                    ps.close();
                    conn.close();
                }

								EIGlobal.mensajePorTrace( bOTP.getDescripcion() + "  Transacci�n ejecutada.|" +
									bOTP.getFecha() + "|" +
									bOTP.getHora() + "|" +
									bOTP.getNumCuenta() + "|" +
									"|" +
									bOTP.getIdSession() + "|" +
									bOTP.getDirIP() + "|" +
              		bOTP.getUsuario() + "|" +
              		"ENLACE|"+
              		referencia + "|" +
              		transaccion + "|" +
              		bOTP.getIdSerie() + "|" +
              		bOTP.getInstancia() + "|" +
              		bOTP.getHostName() + "|", EIGlobal.NivelLog.INFO);
            }

        }catch(Exception e){
            e.printStackTrace();
        }*/
    }

	/**
	 * Metodo para guardar registro en bitacora
	 * @param req de tipo HttpServletRequest
	 * @param mensaje de tipo String
	 */
    public static void guardaRegistroBitacora( HttpServletRequest req, String mensaje){

        BitacoraOTP bitacora = new BitacoraOTP();
        List listBitacora = null;
        BaseResource br = (BaseResource) req.getSession ().getAttribute (SESSION);

        java.sql.Date date = new java.sql.Date(new java.util.Date().getTime() );
        Format formatter = new SimpleDateFormat("HH:mm:ss", new Locale("la", "MX"));
        String hora = formatter.format(new Date());

        if(req.getSession().getAttribute(BITACORA) != null){
            listBitacora = (List)req.getSession().getAttribute(BITACORA);
        }else{
            listBitacora = new ArrayList();
        }

        String usr = br.getUserID8().trim();
        Token tok = br.getToken();

        int numConsec = "Token valido.".equalsIgnoreCase(mensaje)? getNumeroIntento(usr) + 1:getNumeroIntento(usr) ;

        bitacora.setNumConsec(numConsec);
        bitacora.setFecha(date);
        bitacora.setUsuario(usr.trim());
        bitacora.setIdSession(req.getSession().getId());
        if(tok != null && tok.getSerialNumber() != null) {
        	bitacora.setIdSerie(tok.getSerialNumber().substring(2));
        } else {
          bitacora.setIdSerie("N/A");
        }
        bitacora.setDirIP(req.getRemoteAddr());
        bitacora.setCanal("Enlace");
        bitacora.setNumCuenta(br.getContractNumber().trim());
        bitacora.setIdOperacion("Validacion de OTP.");
        bitacora.setHostName(req.getServerName());
        bitacora.setDescripcion(mensaje);
        bitacora.setHora(hora);
        bitacora.setInstancia(Global.SERVER_INSTANCIA);

				EIGlobal.mensajePorTrace( bitacora.getDescripcion() + "|" +
				bitacora.getFecha() + "|" +
				bitacora.getHora() + "|" +
				bitacora.getNumCuenta() + "|" +
				"|" +
				bitacora.getIdSession() + "|" +
				bitacora.getDirIP() + "|" +
				bitacora.getUsuario() + "|" +
				"ENLACE|" +
				bitacora.getIdSerie() + "|" +
				bitacora.getHostName() + "|", EIGlobal.NivelLog.DEBUG);

        try{

            InitialContext ic = new InitialContext ();
            DataSource ds = ( DataSource ) ic.lookup ( Global.DATASOURCE_ORACLE );
            java.sql.Connection conn = ds.getConnection ();
            java.sql.PreparedStatement ps = conn.prepareStatement(sSQLINSERTBITACORA);
            ps.setDate(1,bitacora.getFecha());
            ps.setString(2,bitacora.getHora());
            ps.setString(3,bitacora.getIdSession());
            ps.setString(4,bitacora.getNumCuenta());
            ps.setString(5,bitacora.getDirIP());
            ps.setString(6,bitacora.getCanal());
            ps.setString(7,bitacora.getUsuario());
            ps.setString(8,bitacora.getIdOperacion());
            ps.setString(9,bitacora.getHostName());
            ps.setString(10,bitacora.getIdSerie());
            ps.setString(11,bitacora.getDescripcion());
            ps.setString(12,"N/A");
            ps.setInt(13,bitacora.getNumConsec());
            ps.setString(14,bitacora.getInstancia());
            ps.execute();
            ps.close();
            conn.close();

        }catch(Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }

        listBitacora.add(bitacora);

        req.getSession().setAttribute(BITACORA,listBitacora);
    }

	/**
	 * Metodo para obtener facultad de validaci�n de OTP
	 * @param request de tipo HttpServletRequest
	 * @return boolean indicando si tiene o no la facultad
	 * @throws IOException en caso de error
	 */
	public static boolean getFacValOTP(HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession();
        BaseResource baseResource = (BaseResource)session.getAttribute(SESSION);
        return baseResource.getFacultad(baseResource.FAC_VAL_OTP);
    }

	/**
	 * Metodo para obtener el numero de intento
	 * @param usuario usuario
	 * @return int valor del nunero de intento
	 */
	public static int getNumeroIntento(String usuario){		//se elimina l�gica de negocio pero se mantiene m�todo por atenci�n de incidente.
		return 0;
	}
}