/*
 * pdImportarArchivo.java
 *
 * Created on 11 de abril de 2002, 11:04 AM
 */
package mx.altec.enlace.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Properties;
import java.util.StringTokenizer;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.pdArchivoTemporal;
import mx.altec.enlace.bo.pdBeneficiario;
import mx.altec.enlace.bo.pdCalendario;
import mx.altec.enlace.bo.pdError;
import mx.altec.enlace.bo.pdPago;
import mx.altec.enlace.bo.pdTokenizer;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * @author Horacio Oswaldo Ferro D�az
 * @version 1.0
 */
public class pdImportarArchivo {

	/** Nombre del archivo */
	protected String Nombre;

	/** Arreglo con las lineas separadas */
	protected String[] LineasSep;

	/** N�mero de lineas en el archivo */
	protected long numLineas;

	/** Lista de dias inhabiles */
	protected ArrayList diasInhabiles;

	/** Lista de errores generados en el archivo */
	protected ArrayList listaErrores;

	/** Mapa de beneficiarios */
	protected HashMap Beneficiarios;

	/** Lista de cuentas */
	protected ArrayList Cuentas;

	/** Numero de lineas correctas */
	protected long lineasCorrectas;

	/** Contrato */
	private String contrato;

	private String usuario;

	/** Longitudes de campos para layouts de alta y baja de pagos directos */
	public static HashMap<String, int[]> layouts = new HashMap<String, int[]>();

	/** Layout para alta y baja de archivos de Pago Directo a 12 posiciones */
	public static int[] layoutRef12 = {// Layout a 12 posiciones
	16, // Cuenta de Cargo
			28, // Numero de Orden
			38, // Fecha de Aplicacion
			48, // Fecha Limite de Pago
			61, // Clave del Beneficiario
			121, // Nombre del Beneficiario
			122, // Clave de todas las sucursales
			126, // Clave de Sucursal
			127, // Tipo de Pago
			143, // Importe
			203 // Concepto
	};

	/** Layout para alta y baja de archivos de Pago Directo a 20 posiciones */
	public static int[] layoutRef20 = {// Layout a 20 posiciones
	16, // Cuenta de Cargo
			36, // Numero de Orden
			46, // Fecha de Aplicacion
			56, // Fecha Limite de Pago
			69, // Clave del Beneficiario
			129, // Nombre del Beneficiario
			130, // Clave de todas las sucursales
			134, // Clave de Sucursal
			135, // Tipo de Pago
			151, // Importe
			211 // Concepto
	};

	/**
	 * Funci�n que realiza la carga de los arreglos que contienen las longitudes
	 * de los campos de cada layout manejado por altas y bajas de pago directo,
	 * tanto en l�nea como por archivo.
	 */
	public static void cargaLayout() {
		layouts.put("1", layoutRef12);
		layouts.put("2", layoutRef20);
	}

	/**
	 * Obtiene por medio del primer parametro un arreglo de enteros que cuyos
	 * valores representan las longitudes de los campos del archivo de pagos
	 * directos utilizado en el modulo de importacion de pagos.<br>
	 *
	 * @param layout <br>
	 *            1 para layout de referencia a 12 posiciones. <br>
	 *            2 para layout de referencia a 20 posiciones.
	 * @param field <br>
	 *            El numero del campo a buscar.
	 * @return Longitud del campo
	 */
	public static int getFieldSize(String layout, int field) {
		int[] fieldArray = null;
		int fieldVal = 0;

		try {
			fieldArray = layouts.get(layout);
			fieldVal = fieldArray[field];
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(
					"mx.altec.enlace.dao.pdImportarArchivo::getFieldSize()::Error al obtener tama�o de campo.",
					EIGlobal.NivelLog.ERROR
			);
		}
		return fieldVal;
	}

	/** Clase para manejar pagos */
	protected class pdCuentaTemp implements java.io.Serializable {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		/** Cuenta en altair */
		protected String CuentaAltair;

		/** Cuenta de Serfin */
		protected String CuentaSerfin;

		/** Descripcion de la cuenta */
		protected String Descripcion;

		/**
		 * Construye un nuevo pdPago
		 *
		 * @param ca
		 *            Cuenta de Altair
		 * @param cs
		 *            Cuenta de Serfin
		 * @param d
		 *            Descripcion de la cuenta
		 */
		public pdCuentaTemp(String ca, String cs, String d) {
			CuentaAltair = ca;
			CuentaSerfin = cs;
			Descripcion = d;
		}

		/**
		 * Metodo para obtener la cuenta de altair
		 *
		 * @return Cuenta de Altair
		 */
		public String getCuentaAltair() {
			return CuentaAltair;
		}

		/**
		 * Metodo para obtener la cuenta de Serfin
		 *
		 * @return Cuenta de Serfin
		 */
		public String getCuentaSerfin() {
			return CuentaSerfin;
		}

		/**
		 * Metodo para obtener descripcion de la cuenta
		 *
		 * @return Descripcion de la cuenta
		 */
		public String getDescripcion() {
			return Descripcion;
		}
	}

	/** Creates new pdImportarArchivo */
	public pdImportarArchivo() {
		LineasSep = null;
		numLineas = 0;
		listaErrores = new ArrayList();
		Cuentas = new ArrayList();
	}

	/**
	 * Crea un nuevo pdImportarArchivo con el request del servlet
	 *
	 * @param req
	 *            Request del servlet
	 */
	public pdImportarArchivo(
			byte[] archivoBytes,
			String nom,
			ArrayList Inhabiles,
			HashMap mapaBen
	) {
		LineasSep = null;
		numLineas = 0;
		Nombre = nom + "_pdImport";
		listaErrores = new ArrayList();
		Cuentas = new ArrayList();
		Beneficiarios = mapaBen;
		diasInhabiles = Inhabiles;
		formateaLineas(archivoBytes);
	}

	/**
	 * Metodo para formatear las lineas
	 */
	protected void formateaLineas(byte[] archivoBytes) {
		String Temp = new String(archivoBytes);
		StringTokenizer st = new StringTokenizer(Temp, "\n\r");
		numLineas = st.countTokens();
		LineasSep = new String[(int) numLineas];
		for (int x = 0; x < numLineas; x++) {
			LineasSep[x] = st.nextToken();
		}
	}

	/**
	 * Metodo para obtener los errores generados por el archivo
	 *
	 * @return Lista con los errores generados
	 */
	public ArrayList getListaErrores() {
		return listaErrores;
	}

	/**
	 * Metodo para importar el archivo
	 *
	 * @return pdArchivoTemporal Con los datos del archivo de texto
	 * @deprecated Reemplazado por:
	 *             {@link mx.altec.enlace.dao.pdImportarArchivo#nuevaImportacion(String)
	 *             nuevaImportacion}<br>
	 *             AMX-11-01416-0A / RFC-31377<br>
	 *             Se detecta que la presente funcion ya no es utilizada en la
	 *             actualidad, sin embargo no se remueve el codigo por la
	 *             urgencia del mantenimiento que se atiende y asi evitar algun
	 *             impacto que coprometa el cambio.<br>
	 *             Se recomienda que en la reingenieria sea eliminada la funcion
	 *             y renombrada la vigente.
	 */
	@Deprecated
	public pdArchivoTemporal importa() {
		long numRegistrosProc = 0;
		long RegistrosCorrectos = 0;
		Properties prop = new Properties();
		pdArchivoTemporal Archivo = new pdArchivoTemporal();// (Nombre);
		File temp = null;
		pdPago pag;
		LYMValidador valida = new LYMValidador();
		while (numRegistrosProc < numLineas) {
			pag = parsePago(numRegistrosProc, valida);
			if (pag != null) {
				if (Archivo.agrega(pag))
					++RegistrosCorrectos;
				else
					agregaError(
							"El n&uacute;mero de pago " +
							pag.getNoPago() +
							" ya existe en el archivo",
							numRegistrosProc
					);
			}
			++numRegistrosProc;
		}
		valida.cerrar();
		lineasCorrectas = RegistrosCorrectos;
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::importa()::Registros procesados: " +
				numRegistrosProc,
				EIGlobal.NivelLog.DEBUG
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::importa()::Registros aceptados: " +
				lineasCorrectas,
				EIGlobal.NivelLog.DEBUG
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::importa()::Registros: " + numLineas +
				" Correctos: " + RegistrosCorrectos,
				EIGlobal.NivelLog.INFO
		);
		if (RegistrosCorrectos == numLineas) {
			if (!Archivo.crea(Nombre)) {
				try {
					FileInputStream in = new FileInputStream(
							Global.ARCHIVO_CONFIGURACION);
					prop.load(in);
					temp = new File(
							prop.getProperty("DIRECTORIO_LOCAL") + Nombre
					);
				} catch (Exception ex) {
					EIGlobal.mensajePorTrace(
							getClass().getName() + "::importa()::Error en funcion Nombre: " + Nombre,
							EIGlobal.NivelLog.ERROR
					);					
					temp = new File(Global.DOWNLOAD_PATH + Nombre);
				}
				temp.delete();
				if (!Archivo.crea(Nombre))
					return null;
				else
					return Archivo;
			}
			return Archivo;
		} else {
			return null;
		}
	}

	/**
	 * Metodo para obtener un pago de la l�nea del archivo.
	 *
	 * @param long N�mero de linea del archivo.
	 * @param LYMValidador
	 *            Objeto para validacion de limites y montos ocupado por la
	 *            funcion
	 *            {@link mx.altec.enlace.bo.LYMValidador#validaLyM(String, String, String, String)
	 *            validaLyM}
	 * @return pdPago Objeto con la informaci�n del pago si la linea es correcta
	 *         o de lo contrario regresa null.
	 * @deprecated Reemplazado por:
	 *             {@link mx.altec.enlace.dao.pdImportarArchivo#nuevoParsePago(pdPago, long, LYMValidador)
	 *             nuevoParsePago}<br>
	 *             AMX-11-01416-0A / RFC-31377<br>
	 *             Se detecta que la presente funcion ya no es utilizada en la
	 *             actualidad, sin embargo no se remueve el codigo por la
	 *             urgencia del mantenimiento que se atiende y asi evitar algun
	 *             impacto que coprometa el cambio.<br>
	 *             Se ajusta en el presente mantenimiento para homologar con
	 *             nueva funcion, sin embargo se recomienda que en la
	 *             reingenieria sea eliminada la funcion y renombrada la
	 *             vigente.
	 */
	@Deprecated
	public pdPago parsePago(long linea, LYMValidador valida) {
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePago()::Inicio",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePago()::Linea  [" + linea + "]",
				EIGlobal.NivelLog.INFO
		);

		String Linea = LineasSep[(int) linea];
		pdPago Temp = new pdPago();
		String regis = "";
		boolean bError = false;

		/*
		 * Se modificaron las posiciones de los substring a partir de Num Pago
		 * de 12 a 20, y Linea.length se aumento 8 (de 203 a 211)
		 */
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePago()::Longitud de linea = " + Linea.length(),
				EIGlobal.NivelLog.DEBUG
		);
		if (Linea.length() == 211 || Linea.length() == 203) {

			/*
			 * Se declara variable para seleccion de layout a trabajar (1 para
			 * 12 referencia a posiciones o 2 para referencia a 20 posiciones).
			 */
			String numLayout = "1";
			if (Linea.length() == 211)
				numLayout = "2";
			cargaLayout();

			// Cuenta Cargo 16 caracteres
			Temp.setCuentaCargo(
					Linea.substring(0, getFieldSize(numLayout, 0)).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Cuenta de Cargo [" + Temp.getCuentaCargo() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Num Pago 12 o 20 caracteres
			String numPago = Linea.substring(
					getFieldSize(numLayout, 0),
					getFieldSize(numLayout, 1)
			).trim();

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Numero de Pago conCeros [" + numPago + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Num Pago 12 o 20 caracteres
			Temp.setNoPago(EIGlobal.eliminaCerosImporte(numPago));

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Numero de Pago String [" + Temp.getNoPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Fecha Lib 10 caracteres
			Temp.setFechaLib(
					Linea.substring(
							getFieldSize(numLayout, 1),
							getFieldSize(numLayout, 2)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Fecha Lib [" + Temp.getFechaLib() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Fecha Pago 10 caracteres
			Temp.setFechaPago(
					Linea.substring(
							getFieldSize(numLayout, 2),
							getFieldSize(numLayout, 3)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Fecha de Pago [" + Temp.getFechaPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Beneficiario 13 caracteres
			Temp.setClaveBen(
					Linea.substring(
							getFieldSize(numLayout, 3),
							getFieldSize(numLayout, 4)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Clave Benef [" + Temp.getClaveBen() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if (Beneficiarios.containsKey(regis)) {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::parsePago()::Clave Registrada",
						EIGlobal.NivelLog.DEBUG
				);
				Temp.setClaveBen(regis);
			} else {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::parsePago()::Clave NO Registrada",
						EIGlobal.NivelLog.DEBUG
				);
				Temp.setClavenoReg(regis);
			}

			// Nombre Beneficiario 60 caracteres
			Temp.setNomBen(
					Linea.substring(
							getFieldSize(numLayout, 4),
							getFieldSize(numLayout, 5)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Nombre de Beneficiario [" + Temp.getNomBen() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Todas las sucursales 1 caracter
			Temp.setTSucursales(
					Linea.charAt(getFieldSize(numLayout, 5))
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Todas las sucursales [" + Temp.getTSucursales() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Clave de sucursal 4 caracteres
			Temp.setClaveSucursal(
					Linea.substring(
							getFieldSize(numLayout, 6),
							getFieldSize(numLayout, 7)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Clave de sucursal [" + Temp.getClaveSucursal() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Forma de Pago 1 caracter
			Temp.setFormaPago(
					Linea.charAt(getFieldSize(numLayout, 7))
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Forma de Pago [" + Temp.getFormaPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Importe 16 caracteres
			Temp.setImporteSin(
					Linea.substring(
							getFieldSize(numLayout, 8),
							getFieldSize(numLayout, 9)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Importe [" + Temp.getImporte() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Concepto 60 caracteres
			Temp.setConcepto(
					Linea.substring(
							getFieldSize(numLayout, 9)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Concepto [" + Temp.getConcepto() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if (pagoValido(Temp, linea, valida)) {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::parsePago()::Pago Correcto",
						EIGlobal.NivelLog.INFO
				);
				return Temp;
			} else {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::parsePago()::Pago Incorrecto",
						EIGlobal.NivelLog.ERROR
				);
				return null;
			}
		} else {
			EIGlobal.mensajePorTrace(
					getClass().getName() + "::parsePago()::Linea con longitud incorrecta",
					EIGlobal.NivelLog.ERROR
			);
			agregaError("La linea no tiene la longitud correcta", linea);
			bError = true;
		}

		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePago()::Fin",
				EIGlobal.NivelLog.INFO
		);
		if (bError)
			return null;
		else
			return Temp;
	}

	/**
	 * Metodo para verificar la validez de un pago
	 *
	 * @param pago
	 *            Pago a verificar
	 * @return true si el pago es correcto de lo contrario false
	 */
	public boolean pagoValido(pdPago pago, long linea, LYMValidador valida) {
		boolean Regresa = true;
		String s;

		GregorianCalendar actual = new GregorianCalendar();
		int d = actual.get(Calendar.DATE);
		int m = actual.get(Calendar.MONTH);
		int a = actual.get(Calendar.YEAR);
		GregorianCalendar f = new GregorianCalendar(a, m, d);

		if ((s = validaCuenta(pago.getCuentaCargo())) == null) {
			Regresa = false;
			agregaError("El n&uacute;mero de cuenta es incorrecto.", linea);
		} else {
			pago.setCuentaCargo(s);
		}
		if (pago.getNoPago().equals("-1")) {
			Regresa = false;
			agregaError("El n&uacute;mero de pago no es v&aacute;lido.", linea);
		}
		if (-1 == pago.getImporte()) {
			Regresa = false;
			agregaError("El importe es incorrecto.", linea);
		}
		if (null == pago.getFechaLib()) {
			Regresa = false;
			agregaError("La fecha de liberaci&oacute;n no es correcta.", linea);
		}
		if (Regresa) {
			if (!pago.getFechaLib().equals(
					pdCalendario.getSigDiaHabil(
							pago.getFechaLib(),
							diasInhabiles)
					)
			) {
				Regresa = false;
				agregaError("La fecha de liberaci&oacute;n no es un dia h&aacute;bil.", linea);
			}
		}
		if (null == pago.getFechaPago()) {
			Regresa = false;
			agregaError("La fecha l&iacute;mite no es correcta.", linea);
		}
		if (Regresa) {
			if (!pago.getFechaPago().equals(
					pdCalendario.getSigDiaHabil(
							pago.getFechaPago(),
							diasInhabiles)
					)
			) {
				Regresa = false;
				agregaError("La fecha l&iacute;mite no es un d&iacute;a h&aacute;bil.", linea);
			}
			if (pago.getFechaPago().before(pago.getFechaLib())) {
				Regresa = false;
				agregaError("La fecha l&iacute;mite no puede ser menor a la fecha de liberaci&oacute;n", linea);
			}
			if (pago.getFechaLib().before(f)) {
				Regresa = false;
				agregaError("La fecha de liberaci&oacute;n debe ser igual o mayor al d&iacute;a actual", linea);
			}
		}
		if (pago.getClaveBen().equals("") && pago.getClavenoReg().equals("")) {
			Regresa = false;
			agregaError("Debe especificar una clave de beneficiario.", linea);
		}
		if ("" == pago.getNomBen().trim()) {
			Regresa = false;
			agregaError("Debe proporcionar el nombre del beneficiario.", linea);
		}
		if ("" == pago.getClavenoReg()) {
			if (
					!Beneficiarios.containsKey(pago.getClaveBen()) ||
					!(((pdBeneficiario) Beneficiarios.get(pago.getClaveBen())).getNombre().equals(pago.getNomBen()))) {
				Regresa = false;
				agregaError("El nombre del beneficiario no concuerda con la clave.", linea);
			}
		}

		if (pago.getTSucursales() == 'S')
			EIGlobal.mensajePorTrace(
					getClass().getName() + "::pagoValido()::linea [" + linea +
					"] Todas las sucursales ",
					EIGlobal.NivelLog.DEBUG
			);
		if (!pago.getClaveSucursal().equals(""))
			EIGlobal.mensajePorTrace(
					getClass().getName() + "::pagoValido()::linea [" + linea +
					"] La sucursal [" + pago.getClaveSucursal() + "]",
					EIGlobal.NivelLog.DEBUG
			);
		if (pago.getTSucursales() == ' ' && pago.getClaveSucursal().equals("")) {
			Regresa = false;
			agregaError("No existe el campo \"Todas Sucursales\" y \"Cve_Sucursal\". Debe existir uno de los dos en el registro.", linea);
		}
		if (pago.getTSucursales() == 'S' && !pago.getClaveSucursal().equals("")) {
			Regresa = false;
			agregaError("Error en el campo \"Todas Sucursales\" y \"Cve_Sucursal\". No pueden existir ambos en el mismo registro.", linea);
		}

		String contratoTraspaso = "";
		Connection connContrato = null;
		try {
			connContrato = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			Statement stmtContrato = connContrato.createStatement();
			ResultSet rs = stmtContrato.executeQuery("select cta_cheques from cata_cheq_enlace where cve_chequera = 'LOPT' and sucursal_region = '99' and u_version = '9'");
			if (rs.next()) {
				contratoTraspaso = rs.getString(1);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::pagoValido()::Error al intentar crear la conexion.",
					EIGlobal.NivelLog.ERROR
			);
		} finally {
			try {
				if (connContrato != null) {
					connContrato.close();
				}
			} catch (SQLException sqle) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::pagoValido()::Error al intentar cerrar la conexcion.",
						EIGlobal.NivelLog.ERROR
				);
			}

		}

		if (contrato.trim().equals(contratoTraspaso.trim())) {
			if (pago.getFormaPago() != 'C' && pago.getFormaPago() != 'E'
					&& pago.getFormaPago() != 'T') {
				Regresa = false;
				agregaError("Debe especificar una forma v&aacute;lida de pago.", linea);
			}
		} else {
			if (pago.getFormaPago() != 'C' && pago.getFormaPago() != 'E') {
				Regresa = false;
				agregaError("Debe especificar una forma v&aacute;lida de pago.", linea);
			}

		}

		if (null == pago.getConcepto() || "" == pago.getConcepto()
				|| pago.getConcepto().equals("")) {
			Regresa = false;
			agregaError("Debe especificar un concepto.", linea);
		}

		String valLyM = valida.validaLyM(contrato, usuario, "AAOP", ""
				+ pago.getImporte());
		valida.cerrar();//CSA
		if (!valLyM.startsWith("ALYM0000")) {
			Regresa = false;
			agregaError(valLyM.substring(8), linea);
		}

		return Regresa;
	}

	/**
	 * Metodo para agregar errores a la lista de errores
	 *
	 * @param error
	 *            String con la descripci+on del error
	 */
	protected void agregaError(String error, long linea) {
		pdError Error = new pdError(linea + 1, error);
		listaErrores.add(Error);
	}

	/**
	 * Metodo para construir la lista de cuentas a partir del archivo enviado
	 *
	 * @param nombre
	 *            Nombre del archivo del que se obtendran las cuentas
	 */
	public void generaCuentas(String nombre) {
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::generaCuentas()::Inicio",
				EIGlobal.NivelLog.DEBUG
		);
		BufferedReader input = null;
		try {
			EIGlobal.mensajePorTrace(
					getClass().getName() + "::generaCuentas()::Esta en el try",
					EIGlobal.NivelLog.DEBUG
			);
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(
					Global.ARCHIVO_CONFIGURACION
			);
			prop.load(in);
			File arch = new File(prop.getProperty("DIRECTORIO_LOCAL", Global.DOWNLOAD_PATH) + "/" + nombre);
			arch.deleteOnExit();
			input = new BufferedReader(new FileReader(arch));
			pdCuentaTemp temp;
			String Linea;
			pdTokenizer tokenizer;
			while ((Linea = input.readLine()) != null) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::generaCuentas()::La linea en el while es = " + Linea,
						EIGlobal.NivelLog.DEBUG
				);
				tokenizer = new pdTokenizer(Linea, "@", 2);
				temp = new pdCuentaTemp(
						tokenizer.nextToken(),
						tokenizer.nextToken(),
						tokenizer.nextToken()
				);
				Cuentas.add(temp);
			}
			input.close();
		} catch (FileNotFoundException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaCuentas()::Error FNFE Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaCuentas()::Error I/O Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaCuentas()::Error Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::generaCuentas()::Error en close I/O Ex[" +
						e.getMessage() + "]",
						EIGlobal.NivelLog.ERROR
				);
				e.printStackTrace();
			}
		}
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::generaCuentas()::Fin",
				EIGlobal.NivelLog.DEBUG
		);
	}

	/**
	 * Metodo para validar cuentas
	 *
	 * @param numero
	 *            Numero de cuenta a validar
	 */
	protected String validaCuenta(String numero) {
		pdCuentaTemp temp;
		ListIterator liCuentas = Cuentas.listIterator();
		while (liCuentas.hasNext()) {
			temp = (pdCuentaTemp) liCuentas.next();
			if (numero.equals(temp.getCuentaAltair()) || numero.equals(temp.getCuentaSerfin()))
				return temp.getCuentaAltair();
		}
		// return numero; //hardcode para pruebas
		return null;
	}

	/**
	 * Metodo para obtener el numero de lineas en el archivo
	 *
	 * @return Numero de lineas
	 */
	public long getNumLineas() {
		return numLineas;
	}

	/**
	 * Metodo para obtener el numero de lineas correctas
	 *
	 * @return Lineas correctas en el archivo
	 */
	public long getLineasCorrectas() {
		return lineasCorrectas;
	}

	/** importe total */
	protected double impTotal;

	/**
	 * Agrega el mensaje de error cuando el archivo no es un zip valido
	 *
	 * @param void
	 */
	public void zipError() {
		agregaError(
				"El archivo esta da�ado o no esta comprimido correctamente.", 0);
	}

	/**
	*
	*/
	public double getImporteTotal() {
		return impTotal;
	}

	/**
	 * Crea un nuevo pdImportarArchivo con el nombre del archivo importado
	 *
	 * @param req
	 *            Request del servlet
	 */
	public pdImportarArchivo(String nom, ArrayList Inhabiles, HashMap mapaBen) {
		LineasSep = null;
		numLineas = 0;
		lineasCorrectas = 0;
		impTotal = 0.0;
		Nombre = nom;
		nombreArchivoExportacion = "";
		listaErrores = new ArrayList();
		Cuentas = new ArrayList();
		Beneficiarios = mapaBen;
		diasInhabiles = Inhabiles;
	}

	/**
	 * @deprecated No se reemplaza esta funcion.<br>
	 *             AMX-11-01416-0A / RFC-31377<br>
	 *             Se detecta que la presente funcion ya no es utilizada en la
	 *             actualidad, sin embargo no se remueve el codigo por la
	 *             urgencia del mantenimiento que se atiende y asi evitar algun
	 *             impacto que coprometa el cambio.<br>
	 */
	@Deprecated
	public boolean nuevaImportacion(String nombreArchivo, String contrato) {
		return nuevaImportacion(nombreArchivo);
	}

	/**
	 * Abre el archivo y realiza las validaciones trabajando directo a disco.
	 *
	 * @param String
	 *            nombreArchivo
	 * @return boolean<br>
	 *         true - si el archivo se importa correctamente.<br>
	 *         false - si el archivo contiene errores.
	 */
	public boolean nuevaImportacion(String nombreArchivo) {
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::nuevaImportacion()::Inicio",
				EIGlobal.NivelLog.INFO
		);

		String lineaLeida = "";
		long tamanioArchivo = 0;
		boolean erroresEnArchivo = false;
		boolean archivoExp = false;

		pdPago Temp = new pdPago();
		impTotal = 0.0;

		RandomAccessFile archivoTrabajo = null;

		try {
			archivoTrabajo = new RandomAccessFile(new File(nombreArchivo), "r");
			tamanioArchivo = archivoTrabajo.length();
			archivoTrabajo.seek(0);

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevaImportacion()::Tama�o Archivo [" +
					tamanioArchivo + "]",
					EIGlobal.NivelLog.DEBUG
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevaImportacion()::Total de Lineas [" +
					(int) (tamanioArchivo / 211) + "]",
					EIGlobal.NivelLog.DEBUG
			);

			numLineas = 0;
			archivoExp = creaArchivoParaExportar(Nombre, ".doc");
			LYMValidador valida = new LYMValidador();
			while ((lineaLeida = archivoTrabajo.readLine()) != null) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::nuevaImportacion()::lineaLeida [" + numLineas +
						"][" + lineaLeida + "]",
						EIGlobal.NivelLog.DEBUG
				);
				if ((numLineas % 100) == 0)
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::nuevaImportacion()::numLineas [" + numLineas + "]",
							EIGlobal.NivelLog.DEBUG
					);
				Temp = nuevoParsePago(lineaLeida, numLineas, valida);
				if (Temp != null) {
					impTotal += Temp.getImporte();
					if (archivoExp)
						escribeLineaEnArchivo(Temp);
					lineasCorrectas++;
				} else
					erroresEnArchivo = true;
				numLineas++;

			}
			valida.cerrar();

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevaImportacion()::Registros procesados [" + numLineas +
					"]",
					EIGlobal.NivelLog.INFO
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevaImportacion()::Registros aceptados  [" +
					lineasCorrectas + "]",
					EIGlobal.NivelLog.INFO
			);

			if (archivoExp)
				cierraArchivoExportacion();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevaImportacion()::funcion finalizando con errores [" +
					e + "]",
					EIGlobal.NivelLog.ERROR
			);
			return false;
		} finally {
			if (null != archivoTrabajo) {
				try {
					archivoTrabajo.close();
					archivoTrabajo = null;
				} catch (java.io.IOException ex) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::nuevaImportacion()::problema con cierre de archivo de trabajo",
							EIGlobal.NivelLog.ERROR
					);
				}
			}
		}

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::nuevaImportacion()::Lineas Correctas [" +
				lineasCorrectas + "]",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::nuevaImportacion()::Lineas Totales   [" + numLineas + "]",
				EIGlobal.NivelLog.INFO
		);

		if (erroresEnArchivo)
			return false;
		EIGlobal.mensajePorTrace(
				getClass().getName()
				+ "::nuevaImportacion()::Fin",
				EIGlobal.NivelLog.INFO
		);
		return true;

	}

	/**
	 * Metodo nuevo para obtener un pago de la l�nea del archivo
	 *
	 * @param Linea
	 *            N�mero de linea del archivo
	 * @param numero
	 * @param valida
	 * @return Pago si la linea es correcta de lo contrario null
	 */
	public pdPago nuevoParsePago(String Linea, long numero, LYMValidador valida) {
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::nuevoParsePago()::Inicio 2",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::nuevoParsePago()::Linea  [" +
				Linea + "]",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::nuevoParsePago()::numero [" +
				numero + "]",
				EIGlobal.NivelLog.INFO
		);

		pdPago Temp = new pdPago();
		String regis = "";
		boolean bError = false;

		/*
		 * Se modificaron las posiciones de los substring a partir de setNoPago
		 * que cambio de 12 a 20, y Linea.length aumento 203 a 211)
		 */
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::nuevoParsePago()::Longitud de linea [" + Linea.length() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		if (Linea.length() == 211 || Linea.length() == 203) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Longitud correcta",
					EIGlobal.NivelLog.DEBUG
			);

			// Se declara variable para seleccion de layout a trabajar
			// (1 para 12 referencia a posiciones o 2 para referencia a 20
			// posiciones)
			String numLayout = "1";
			if (Linea.length() == 211)
				numLayout = "2";
			cargaLayout();

			// Cuenta Cargo 16 caracteres
			Temp.setCuentaCargo(
					Linea.substring(0, getFieldSize(numLayout, 0)).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Cuenta de Cargo [" + Temp.getCuentaCargo() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Num Pago 12 o 20 caracteres
			String numPago = Linea.substring(
					getFieldSize(numLayout, 0),
					getFieldSize(numLayout, 1)
			).trim();

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Numero de Pago conCeros [" + numPago + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Num Pago 12 o 20 caracteres
			//CSA

			String tmpNumPag = "-1";
			try{
				tmpNumPag = EIGlobal.eliminaCerosImporte(numPago);
				Double.parseDouble(tmpNumPag);// Solo es para validar que sea un numero
			}catch(Exception e){
				tmpNumPag = "-1";
			}
			
			Temp.setNoPago(tmpNumPag);
			
			//*****************************************************
			
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Numero de Pago String [" + Temp.getNoPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Fecha Lib 10 caracteres
			Temp.setFechaLib(
					Linea.substring(
							getFieldSize(numLayout, 1),
							getFieldSize(numLayout, 2)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Fecha Lib [" + Temp.getFechaLib() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Fecha Pago 10 caracteres
			Temp.setFechaPago(
					Linea.substring(
							getFieldSize(numLayout, 2),
							getFieldSize(numLayout, 3)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Fecha de Pago [" + Temp.getFechaPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Beneficiario 13 caracteres
			regis = Linea.substring(
							getFieldSize(numLayout, 3),
							getFieldSize(numLayout, 4)
					).trim();
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::clave Beneficiario [" + regis + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if (Beneficiarios.containsKey(regis)) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::nuevoParsePago()::Clave Registrada",
						EIGlobal.NivelLog.DEBUG
				);
				Temp.setClaveBen(regis);
			} else {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::nuevoParsePago()::Clave NO Registrada",
						EIGlobal.NivelLog.DEBUG
				);
				Temp.setClavenoReg(regis);
			}

			// Nombre Beneficiario 60 caracteres
			Temp.setNomBen(
					Linea.substring(
							getFieldSize(numLayout, 4),
							getFieldSize(numLayout, 5)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Nombre de Beneficiario [" + Temp.getNomBen() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Todas las sucursales 1 caracter
			Temp.setTSucursales(Linea.charAt(getFieldSize(numLayout, 5)));
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Todas las sucursales [" + Temp.getTSucursales() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Clave de sucursal 4 caracteres
			Temp.setClaveSucursal(
					Linea.substring(
							getFieldSize(numLayout, 6),
							getFieldSize(numLayout, 7)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Clave de sucursal [" + Temp.getClaveSucursal() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Forma de Pago 1 caracter
			Temp.setFormaPago(
					Linea.charAt(getFieldSize(numLayout, 7)));
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Forma de Pago [" + Temp.getFormaPago() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Importe 16 caracteres
			Temp.setImporteSin(
					Linea.substring(
							getFieldSize(numLayout, 8),
							getFieldSize(numLayout, 9)
					).trim()
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Importe [" + Temp.getImporte() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			// Concepto 60 caracteres
			Temp.setConcepto(Linea.substring(getFieldSize(numLayout, 9)).trim());
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::nuevoParsePago()::Concepto [" + Temp.getConcepto() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if (pagoValido(Temp, numero, valida)) {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::nuevoParsePago()::Pago Correcto",
						EIGlobal.NivelLog.INFO
				);
				return Temp;
			} else {
				EIGlobal.mensajePorTrace(
						getClass().getName() + "::nuevoParsePago()::Pago Incorrecto",
						EIGlobal.NivelLog.ERROR
				);
				return null;
			}
		} else {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::parsePago()::Linea con longitud incorrecta",
					EIGlobal.NivelLog.ERROR
			);
			agregaError("La linea no tiene la longitud correcta", numero);
			bError = true;
		}

		EIGlobal.mensajePorTrace(
				getClass().getName() + "::nuevoParsePago()::Fin",
				EIGlobal.NivelLog.INFO
		);
		if (bError)
			return null;
		else
			return Temp;
	}

	/** Nombre del archivo */
	protected String nombreArchivoExportacion;

	/** Archivo de exportacion */
	FileWriter writer = null;

	/**
	 * Metodo para obtener el nombre del archivo exportado
	 *
	 * @return nombre de archivo exportado
	 */
	public String getNombreExportar() {
		return nombreArchivoExportacion;
	}

	/**
	 * Metodo para poner el numero de lineas correctas
	 *
	 * @return None
	 */
	public void setNombreExportar(String nombreArc) {
		nombreArchivoExportacion = nombreArc;
	}

	/**
	 * Metodo para completar un registro con espacios
	 *
	 * @return String con espacios para completar el registro
	 */
	public String agregaEspacios(String Rel, long Espacios) {
		String Relleno = "";
		long Aux = 0;
		while (Aux < Espacios) {
			Relleno = Relleno + Rel;
			Aux++;
		}
		return Relleno;
	}

	/**
	 * Metodo para formatear numeros de contrato y cuentas
	 *
	 * @param Guiones
	 *            String a formatear
	 * @return String formateado
	 */
	public String quitaGuiones(String Guiones) {
		String Temp = "";
		StringTokenizer stGuiones = new StringTokenizer(Guiones, "-");
		int Tokens = stGuiones.countTokens();
		int Count = 0;
		while (Count < Tokens) {
			Temp = Temp + stGuiones.nextToken();
			Count++;
		}
		return Temp;
	}

	/**
	 * Metodo para crear el archivo de exportacion en formato txt
	 *
	 * @return true si tuvo exito
	 */
	public boolean creaArchivoParaExportar(String nombre, String ext) {
		try {
			File Exportado = new File(Global.DIRECTORIO_LOCAL + "/" + nombre + ext);
			boolean Creado = Exportado.createNewFile();
			if (!Creado) {
				Exportado.delete();
				Exportado.createNewFile();
			}
			writer = new FileWriter(Exportado);
			setNombreExportar(Exportado.getName());

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::creaArchivoParaExportar()::Se genero el archivo de exportacion [" +
					getNombreExportar() + "]",
					EIGlobal.NivelLog.DEBUG
			);

			return true;
		} catch (FileNotFoundException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::creaArchivoParaExportar()::Error FNFE Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::creaArchivoParaExportar()::Error I/O Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		}
		return false;
	}

	/**
	 * Metodo para escribir una linea en el archivo
	 *
	 * @param Temp
	 *            pdPago
	 * @return true si tuvo exito
	 */
	public boolean escribeLineaEnArchivo(pdPago Temp) {
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
		java.text.DecimalFormat nf = new java.text.DecimalFormat();
		nf.applyPattern("############0.00");
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		String impor = nf.format(Temp.getImporte()).toString();
		String Linea = "";

		try {
			/*if (impor.indexOf(".") > -1)
				impor = impor.substring(0, impor.indexOf('.')) +
						impor.substring(
								impor.indexOf('.') + 1,
								impor.length()
						);*/
			Linea = Linea + quitaGuiones(Temp.getCuentaCargo()) +
					agregaEspacios(" ", 16 - Temp.getCuentaCargo().length());
			Linea = Linea +
					agregaEspacios("0", 10 - Temp.getNoPago().length()) +
					Temp.getNoPago();
			if (null == Temp.getFechaLib()) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::escribeLineaEnArchivo()::Temp.fechaLib=" +
						Temp.getFechaLib(),
						EIGlobal.NivelLog.DEBUG
				);
				Linea += "          ";
			} else
				Linea += df.format(Temp.getFechaLib().getTime());
			if (null == Temp.getFechaPago()) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::escribeLineaEnArchivo()::Temp.fechaPago=" +
						Temp.getFechaPago(),
						EIGlobal.NivelLog.DEBUG
				);
				Linea += "          ";
			} else
				Linea += df.format(Temp.getFechaPago().getTime());
			if (!Temp.getClaveBen().equals("") || null != Temp.getClaveBen()) {
				Linea = Linea +
						agregaEspacios(" ", 13 - Temp.getClaveBen().length()) +
						Temp.getClaveBen();
			} else {
				if (null == Temp.getClavenoReg()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::la clave del no registrado es null" +
							Temp.getClavenoReg(),
							EIGlobal.NivelLog.DEBUG
					);
				} else {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::clave del no registrado",
							EIGlobal.NivelLog.DEBUG
					);
					Linea = Linea +
							agregaEspacios(" ", 13 - Temp.getClavenoReg().length()) +
							Temp.getClavenoReg();
				}
			}
			Linea = Linea + Temp.getNomBen() +
					agregaEspacios(" ", 60 - Temp.getNomBen().length());
			if (Temp.getTodasSucursales())
				Linea = Linea + "S" + "                                        ";
			else
				Linea = Linea + " " +
						agregaEspacios(" ", 40 - Temp.getClaveSucursal().length()) +
						Temp.getClaveSucursal();
			Linea = Linea + Temp.getFormaPago();
			Linea = Linea + agregaEspacios("0", 16 - impor.length()) + impor;
			if (Temp.getEstatusPago() == 'L') {
				if (null == Temp.getFechaPagado()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::Temp.fechaPagado=" +
							Temp.getFechaPagado(),
							EIGlobal.NivelLog.DEBUG
					);
					Linea += "          ";
				} else
					Linea += df.format(Temp.getFechaPagado().getTime());
			} else if (Temp.getEstatusPago() == 'C') {
				if (null == Temp.getFechaCancelacion()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::Temp.fechaCancelacion=" +
							Temp.getFechaCancelacion(),
							EIGlobal.NivelLog.DEBUG
					);
					Linea += "          ";
				} else
					Linea += df.format(Temp.getFechaCancelacion().getTime());
			} else if (Temp.getEstatusPago() == 'V') {
				if (null == Temp.getFechaVencimiento()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::Temp.fechaVencimiento=" +
							Temp.getFechaVencimiento(),
							EIGlobal.NivelLog.DEBUG
					);
					Linea += "          ";
				} else
					Linea += df.format(Temp.getFechaVencimiento().getTime());
			} else if (Temp.getEstatusPago() == 'M') {
				if (null == Temp.getFechaModificacion()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::Temp.fechaModificacion=" +
							Temp.getFechaModificacion(),
							EIGlobal.NivelLog.DEBUG
					);
					Linea += "          ";
				} else
					Linea += df.format(Temp.getFechaModificacion().getTime());
			} else {
				if (null == Temp.getFechaPago()) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::escribeLineaEnArchivo()::Temp.fechaPago=" +
							Temp.getFechaPago(),
							EIGlobal.NivelLog.DEBUG
					);
					Linea += "          ";
				} else
					Linea += df.format(Temp.getFechaPago().getTime());
			}
			Linea = Linea +
					agregaEspacios(" ", 8 - Temp.getNumeroReferencia().length()) +
					Temp.getNumeroReferencia();
			Linea = Linea + " " + Temp.getEstatusPago() + " ";
			Linea = Linea + Temp.getDescripcionEstatus() +
					agregaEspacios(" ", 40 - Temp.getDescripcionEstatus().length());
			Linea = Linea + Temp.getConcepto() +
					agregaEspacios(" ", 60 - Temp.getConcepto().length()) + "\n";

			/* Escribe la linea en el archivo */
			writer.write(Linea);
		} catch (FileNotFoundException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivo()::Error FNFE Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
			return false;
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivo()::Error I/O Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
			return false;
		}
		return true;
	}

	/**
	 * Metodo para cerrar el archivo de exportacion
	 *
	 * @param None
	 */
	public void cierraArchivoExportacion() {
		try {
			writer.flush();
			writer.close();
		} catch (FileNotFoundException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::cierraArchivoExportacion()()::Error FNFE Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::cierraArchivoExportacion()()::Error I/O Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		} finally {
			if (null != writer) {
				try {
					writer.close();
					writer = null;
				} catch (java.io.IOException ex) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::cierraArchivoExportacion()()::Error I/O en cierre",
							EIGlobal.NivelLog.ERROR
					);
				}
			}
		}
	}

	/**
	 * Metodo para enviar un archivo al servidor web
	 *
	 * @param Archivo
	 *            Objeto File del archivo a ser enviado
	 * @return true si el envio se realizo correctamente, de lo contrario false
	 */
	public static boolean envia(File Archivo) {
		try {
			EIGlobal.mensajePorTrace("pdImportarArchivo:envia - Inicia", EIGlobal.NivelLog.INFO);
			//IF PROYECTO ATBIA1 (NAS) FASE II
		
           	boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();

           	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB")){
				
					EIGlobal.mensajePorTrace("*** pdImportarArchivo.actualizaCLABE  no se pudo copiar archivo remoto_web:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** pdImportarArchivo.actualizaCLABE  archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);
				    
				}

           	if (Respuestal){
                return true;
			}
                
          
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(
					"mx.altec.enlace.dao.pdImportarArchivo"+ 
					"::envia()::Problema con envio de archivo\n" +
					ex.getMessage(), 
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
		}
		return false;
	}

	/****************** Para el Envio del Archivo Importado ******************/

	/** Fecha de Transmision */
	protected GregorianCalendar fechaTransmision;

	/** Fecha de actualizacion */
	protected GregorianCalendar fechaActualizacion;

	/** numero de transmision */
	protected long numeroTransmision;

	/** registros rechazados */
	protected long registrosRechazados;

	/** registros rechazados */
	protected long registrosAceptados;

	/** nombre de archivo que se importo */
	protected String nombreArchivoImportado;

	/** nombre de archivo que se enviara a tuxedo */
	protected String nombreArchivoTuxedo;

	/**
	 * Metodos para obtener datos de envio
	 */
	public long getNumeroTransmision() {
		return numeroTransmision;
	}

	public long getRegistrosRechazados() {
		return registrosRechazados;
	}

	public long getRegistrosAceptados() {
		return registrosAceptados;
	}

	public GregorianCalendar getFechaTransmision() {
		return fechaTransmision;
	}

	public GregorianCalendar getFechaActualizacion() {
		return fechaActualizacion;
	}

	public String getNombreArchivoTuxedo() {
		return Global.DIRECTORIO_LOCAL + "/" + nombreArchivoTuxedo;
	}

	/**
	 * Crea un nuevo pdImportarArchivo para enviarlo.
	 */
	public pdImportarArchivo(long nLineas, String nArchivo, String nTuxedo) {
		numLineas = nLineas;
		nombreArchivoImportado = nArchivo;
		nombreArchivoTuxedo = nTuxedo;
	}

	/**
	 * Metodo para generar el archivo que es enviado al servicio Tuxedo.
	 *
	 * @return <br>
	 *         true - Si el archivo pudo generarse de forma correcta. <br>
	 *         false - Si existe algun error en la generacion del archivo.
	 */
	public boolean generaArchivoTuxedo() {
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::generaArchivoTuxedo()::Inicio",
				EIGlobal.NivelLog.INFO
		);
		fechaActualizacion = new GregorianCalendar();
		pdPago Temp = new pdPago();
		boolean archivoTux = false;
		String lineaLeida = "";

		RandomAccessFile archivoTrabajo = null;

		try {
			archivoTrabajo = new RandomAccessFile(new File(nombreArchivoImportado), "r");
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaArchivoTuxedo()::Se abre archivo para crear el de tuxedo [" +
					nombreArchivoImportado + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if ((archivoTux = creaArchivoParaExportar(nombreArchivoTuxedo, ".ParaTuxedo"))) {
				EIGlobal.mensajePorTrace(
						getClass().getName() +
						"::generaArchivoTuxedo()::Se empiezan a generar los datos lineas [" +
						numLineas + "]",
						EIGlobal.NivelLog.DEBUG
				);

				lineasCorrectas = 0;
				for (int i = 0; i < numLineas; i++) {
					lineaLeida = archivoTrabajo.readLine();
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::generaArchivoTuxedo()::Se leyo del archivo [" +
							lineaLeida + "]",
							EIGlobal.NivelLog.DEBUG
					);

					Temp = parsePagoTuxedo(lineaLeida, i);

					if (Temp == null)
						EIGlobal.mensajePorTrace(
								getClass().getName() +
								"::generaArchivoTuxedo()::No se pudo generar objeto Temp",
								EIGlobal.NivelLog.DEBUG
						);
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::generaArchivoTuxedo()::Se genero el objeto Temp",
							EIGlobal.NivelLog.DEBUG
					);

					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::generaArchivoTuxedo()::Intentando escribir en el archivo",
							EIGlobal.NivelLog.DEBUG
					);

					if (!escribeLineaEnArchivoTuxedo(Temp))
						return false;
					else
						EIGlobal.mensajePorTrace(
								getClass().getName() +
								"::generaArchivoTuxedo()::Se escribio la linea en el archivo para Tuxedo",
								EIGlobal.NivelLog.DEBUG
						);

					lineasCorrectas++;
				}
			} else
				return false;

			if (archivoTux)
				cierraArchivoExportacion();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaArchivoTuxedo()::Ha ocurrido una excepcion",
					EIGlobal.NivelLog.DEBUG
			);
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::generaArchivoTuxedo()::\n" + e.getMessage(),
					EIGlobal.NivelLog.DEBUG
			);
			return false;
		} finally {
			if (null != archivoTrabajo) {
				try {
					archivoTrabajo.close();
					archivoTrabajo = null;
				} catch (java.io.IOException ex) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::generaArchivoTuxedo()::Problema al cerrar el archivo",
							EIGlobal.NivelLog.ERROR
					);
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::generaArchivoTuxedo()\n" + ex.getMessage(),
							EIGlobal.NivelLog.DEBUG
					);
				}
			}
		}

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::generaArchivoTuxedo()::Registros procesados [" +
				numLineas + "]",
				EIGlobal.NivelLog.DEBUG
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::generaArchivoTuxedo()::Registros aceptados [" +
				lineasCorrectas + "]",
				EIGlobal.NivelLog.DEBUG
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::generaArchivoTuxedo()::Fin",
				EIGlobal.NivelLog.INFO
		);
		return true;
	}

	/**
	 * Metodo para escribir una linea en el archivo de Tuxedo
	 */
	public boolean escribeLineaEnArchivoTuxedo(pdPago Temp) {

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::escribeLineaEnArchivoTuxedo()::Inicio",
				EIGlobal.NivelLog.INFO
		);

		String Linea = "";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("ddMMyyyy");

		try {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Formateando los datos 1",
					EIGlobal.NivelLog.DEBUG
			);

			Linea = quitaGuiones(Temp.getCuentaCargo()) + "@";
			Linea = Linea + Temp.getNoPago() + "@";
			Linea = Linea + Temp.getImporte() + "@";

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Formateando los datos 2",
					EIGlobal.NivelLog.DEBUG
			);

			/*
			 * IM323100 - Norma Moreno pidio que al enviar pagos directos por
			 * archivo acepte el pago aunque el beneficiario no este registrado;
			 * el orpasrvr acepta el pago siempre y cuando venga el nombre, por
			 * eso se quitaron las l�neas comentadas abajo y se dejan �stas que
			 * siempre env�an clave y nombre. Fecha: 11/10/2004 Autor: Martin
			 * Schmiedel Despacho: Praxis Sistemas
			 */
			if (Temp.getClaveBen().equals("")) {
				Linea = Linea + Temp.getClavenoReg() + "@";
			} else {
				Linea = Linea + Temp.getClaveBen() + "@";
			}

			Linea = Linea + Temp.getNomBen() + "@";

			/*
			 * if(Temp.getClaveBen().equals("")) Linea = Linea +
			 * Temp.getClavenoReg() +"@" + Temp.getNomBen() + "@"; else Linea =
			 * Linea + Temp.getClaveBen() + "@@";
			 */

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Formateando los datos 3",
					EIGlobal.NivelLog.DEBUG
			);

			Linea = Linea + Temp.getFormaPago() + "@";
			Linea = Linea + sdf.format(Temp.getFechaLib().getTime()) + "@";
			Linea = Linea + sdf.format(Temp.getFechaPago().getTime()) + "@";

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Formateando los datos 4",
					EIGlobal.NivelLog.DEBUG
			);

			if (Temp.getTodasSucursales())
				Linea = Linea + "S@";
			else
				Linea = Linea + "@";

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Formateando los datos 5",
					EIGlobal.NivelLog.DEBUG
			);

			Linea = Linea + Temp.getClaveSucursal() + "@";
			Linea = Linea + "@@" + Temp.getConcepto() + "@@\n";

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Linea a mandar en archivo a el servicio\n[" + Linea + "]",
					EIGlobal.NivelLog.DEBUG
			);

			/* Escribe la linea en el archivo */
			writer.write(Linea);
		} catch (FileNotFoundException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Error FNFE Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
			return false;
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::escribeLineaEnArchivoTuxedo()::Error I/O Ex[" +
					ex.getMessage() + "]",
					EIGlobal.NivelLog.ERROR
			);
			ex.printStackTrace(System.err);
			return false;
		}
		return true;
	}

	/**
	 * Metodo nuevo para obtener un pago de la l�nea del archivo
	 *
	 * @return Pago si la linea es correcta de lo contrario null
	 */
	public pdPago parsePagoTuxedo(String Linea, long numero) {
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Inicio",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Linea  [" +
				Linea + "]",
				EIGlobal.NivelLog.INFO
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::numero [" +
				numero + "]",
				EIGlobal.NivelLog.INFO
		);

		pdPago Temp = new pdPago();

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Longitud de linea [" +
				Linea.length() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		/*
		 * Se declara variable para seleccion de layout a trabajar (1 para 12
		 * referencia a posiciones o 2 para referencia a 20 posiciones).
		 */
		String numLayout = "1";
		if (Linea.length() == 211)
			numLayout = "2";
		cargaLayout();

		// Cuenta Cargo 16 caracteres
		Temp.setCuentaCargo(
				Linea.substring(0, getFieldSize(numLayout, 0)).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Cuenta de Cargo [" +
				Linea.substring(0, getFieldSize(numLayout, 0)).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Num Pago 12 o 20 caracteres
		String numPago = Linea.substring(
				getFieldSize(numLayout, 0),
				getFieldSize(numLayout, 1)
		).trim();

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Numero de Pago conCeros [" + numPago + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Num Pago 12 o 20 caracteres
		Temp.setNoPago(EIGlobal.eliminaCerosImporte(numPago));

		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Numero de Pago String [" + Temp.getNoPago() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Fecha Lib 10 caracteres
		Temp.setFechaLib(
				Linea.substring(
						getFieldSize(numLayout, 1),
						getFieldSize(numLayout, 2)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Fecha Lib [" +
				Linea.substring(
						getFieldSize(numLayout, 1),
						getFieldSize(numLayout, 2)
				).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Fecha Pago 10 caracteres
		Temp.setFechaPago(
				Linea.substring(
						getFieldSize(numLayout, 2),
						getFieldSize(numLayout, 3)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Fecha de Pago [" +
				Linea.substring(
						getFieldSize(numLayout, 2),
						getFieldSize(numLayout, 3)
				).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Beneficiario 13 caracteres
		Temp.setClaveBen(
				Linea.substring(
						getFieldSize(numLayout, 3),
						getFieldSize(numLayout, 4)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Clave Benef [" +
				Linea.substring(
						getFieldSize(numLayout, 3),
						getFieldSize(numLayout, 4)
				).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Nombre Beneficiario 60 caracteres
		Temp.setNomBen(
				Linea.substring(
						getFieldSize(numLayout, 4),
						getFieldSize(numLayout, 5)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Nombre de Beneficiario [" +
				Linea.substring(
						getFieldSize(numLayout, 4),
						getFieldSize(numLayout, 5)
				).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Todas las sucursales 1 caracter
		Temp.setTSucursales(Linea.charAt(getFieldSize(numLayout, 5)));
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Todas las sucursales [" +
				Linea.charAt(getFieldSize(numLayout, 5)) + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Clave de sucursal 4 caracteres
		Temp.setClaveSucursal(
				Linea.substring(
						getFieldSize(numLayout, 6),
						getFieldSize(numLayout, 7)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() +
				"::parsePagoTuxedo()::Clave de sucursal [" +
				Linea.substring(
						getFieldSize(numLayout, 6),
						getFieldSize(numLayout, 7)
				).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Forma de Pago 1 caracter
		Temp.setFormaPago(Linea.charAt(getFieldSize(numLayout, 7)));
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Forma de Pago [" +
				Linea.charAt(getFieldSize(numLayout, 7)) + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Importe 16 caracteres
		Temp.setImporteSin(
				Linea.substring(
						getFieldSize(numLayout, 8),
						getFieldSize(numLayout, 9)
				).trim()
		);
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Importe [" +
				Temp.getImporte() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		// Concepto 60 caracteres
		Temp.setConcepto(Linea.substring(getFieldSize(numLayout, 9)).trim());
		EIGlobal.mensajePorTrace(
				getClass().getName() + "::parsePagoTuxedo()::Concepto [" +
				Linea.substring(layouts.get(numLayout)[9]).trim() + "]",
				EIGlobal.NivelLog.DEBUG
		);

		return Temp;
	}

	/**
	 * Metodo para destramar la respuesta de tuxedo
	 *
	 * @param trama
	 *            Trama de la respuesta de Tuxedo
	 * @return Mensaje formateado con la respuesta
	 */
	public String getRespuesta(File respuesta) {
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(respuesta));

			String trama = reader.readLine();
			String Respuesta;
			String Referencia;

			if (trama.indexOf("OK") == -1) {
				String respuesta_err = "";
				int linea = 1;
				if (trama.indexOf("ORPA0002") == -1)
					do {
						respuesta_err += "<b>Linea " + linea + " : " + trama
								+ "</b></br>";
						linea++;
					} while ((trama = reader.readLine()) != null);
				else
					while ((trama = reader.readLine()) != null) {
						if (trama.indexOf("Exitosa") == -1) {
							if (trama.indexOf("Beneficiario no existe") == -1) {
								respuesta_err += "<b>Linea "
										+ linea
										+ " : "
										+ trama.substring(0, trama.indexOf('@'));
							} else {
								respuesta_err += "<b>Linea " + linea + " : "
										+ "Beneficiario no existe";
							}
							trama = trama.substring(trama.indexOf('@') + 1,
									trama.length());
							trama = trama.substring(trama.indexOf('@') + 1,
									trama.length());
							respuesta_err += " - "
									+ trama.substring(0, trama.indexOf('@'))
									+ "</b></br>";
						}
						linea++;
					}

				return respuesta_err;// SLF 323100
			}

			fechaTransmision = new GregorianCalendar();
			Referencia = trama.substring(2, 12).trim();
			Respuesta = "Su n&uacute;mero de referencia es <b>";
			Respuesta += Referencia + "</b><br>";
			Respuesta += trama.substring(12).trim();

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::getRespuesta()::Referencia [" + Referencia + "]",
					EIGlobal.NivelLog.DEBUG
			);

			if (null != reader) {
				try {
					reader.close();
				} catch (java.io.IOException ex) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::getRespuesta()()::Error I/O en cierre de buffer",
							EIGlobal.NivelLog.ERROR
					);
				}
			}

			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::getRespuesta()::Respuesta [" + Respuesta + "]",
					EIGlobal.NivelLog.DEBUG
			);

			return Respuesta;
		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(
					getClass().getName() +
					"::getRespuesta()::Error I/O Ex[" + ex.getMessage() + "]",
					EIGlobal.NivelLog.DEBUG
			);
			ex.printStackTrace(System.err);
			if (null != reader) {
				try {
					reader.close();
				} catch (java.io.IOException e) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::getRespuesta()::Error I/O en cierre de buffer",
							EIGlobal.NivelLog.DEBUG
					);
				}
			}
			return "Error al leer el archivo de respuesta";
		} catch (Exception ex) {
			if (null != reader) {
				try {
					reader.close();
				} catch (java.io.IOException e) {
					EIGlobal.mensajePorTrace(
							getClass().getName() +
							"::getRespuesta()::Error Ex en cierre de buffer",
							EIGlobal.NivelLog.DEBUG
					);
				}
			}
			return ex.getMessage();
		}
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}