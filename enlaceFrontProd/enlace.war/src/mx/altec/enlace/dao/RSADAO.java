package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class RSADAO extends GenericDAO {

	public static int consultaVersionRSA() throws SQLException {
		
		Connection conn = null;
		Statement st = null;		
		ResultSet rs = null;
		String query = "";
		int version = 0;
		
		query = "SELECT valor_entero FROM EWEB_PARAMETROS WHERE nombre_param = \'rsaFlag\'";
		
		EIGlobal.mensajePorTrace("RSADAO :: consultaVersionRSA :: Query  ["+ query +"]" , EIGlobal.NivelLog.INFO);
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			st = conn.createStatement();
			rs = st.executeQuery(query);
			
			if(rs.next()) {
				version = rs.getInt("valor_entero");
			}
			
		} catch (SQLException e) {					
			EIGlobal.mensajePorTrace(
					"RSADAO :: consultaVersionRSA :: Error SQL al consultar la version de RSA " + e.getMessage(),
					EIGlobal.NivelLog.ERROR);
			throw e;
		} finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(st != null){
					st.close();
				}
				if(conn != null){
					conn.close();
					conn = null;
				}				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"RSADAO :: consultaVersionRSA :: Error al cerrar las conexiones --",
						EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("RSADAO :: consultaVersionRSA :: Resultado  ["+ version +"]" , EIGlobal.NivelLog.INFO);
		
		return version;
	}
	
}
