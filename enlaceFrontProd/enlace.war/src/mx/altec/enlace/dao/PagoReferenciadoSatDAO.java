package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mx.altec.enlace.beans.PagoReferenciadoSatBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Mejoras de Linea de Captura Clase de acceso a datos creada para las
 * operaciones relacionadas a Pago Referenciado SAT Se obtiene llave de pago,
 * numero de operacion, cuenta, razon social.
 * 
 * @author RRR - Indra Marzo 2014
 */
public class PagoReferenciadoSatDAO extends GenericDAO {

	/**
	 * Constante para encabezado de PS7 para la trx LZCP
	 */
	public static final String HEADER_LZCP = "LZCP00761123451O00N2";
	/**
	 * Constante con leyenda para el log
	 */
	public static final String LEYENDA_LOG = "PagoReferenciadoSatDAO:LZCP:tramaLZCPEnvio[";
	/**
	 * Select para obtener el numero de operacion
	 */
	private static final String SELECT_NUM_OPE = new StringBuilder().append(
			"select PROTECCION1 %n").append("from %s %n").append(
			"where REFERENCIA = %s and CUENTA_ORIGEN = '%s' %n").append(
			"and NUM_CUENTA = '%s' ").append(
			"and trunc(FECHA) = to_date('%s','dd/mm/yyyy') ").toString();

	/**
	 * Select para obtener la cuenta Cargo o cuenta origen
	 */
	private static final String SELECT_CTA_CARGO = new StringBuilder().append(
			"select TRIM(N_DESCRIPCION) N_DESCRIPCION %n").append(
			"from NUCL_RELAC_CTAS %n").append("where NUM_CUENTA2 = '%s' %n")
			.append("and NUM_CUENTA = '%s' ").toString();

	/**
	 * Constante para mensaje de error al cerrar conexion
	 * 
	 */
	private static final String ERROR_CONEXION = "Error al cerrar conexion: [";

	/**
	 * Se ejecuta el select para obtener el numero de operacion
	 * 
	 * @param pagoRefSatBean
	 *            Bean con los filtros para el select
	 * @return Bean con el numero de operacion
	 */
	public PagoReferenciadoSatBean consultaNumeroDeOperacion(
			PagoReferenciadoSatBean pagoRefSatBean) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String tabla = "TCT_BITACORA";

		if (!fechaIgual(pagoRefSatBean.getFecha())) {
			tabla = "tct_Bitacora_his";
		}

		String selectFull = String.format(SELECT_NUM_OPE, tabla, pagoRefSatBean
				.getReferencia(), pagoRefSatBean.getCuentaCargo(),
				pagoRefSatBean.getContrato(), pagoRefSatBean.getFecha());
		EIGlobal.mensajePorTrace(
				"PagoReferenciadoSatDAO - consultaNumeroDeOperacion - query ["
						+ selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format(
				"PagoReferenciadoSatDAO consultaNumeroDeOperacion - QRY [%s]",
				selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("Numero de operacion -> "
						+ rs.getLong(1), NivelLog.DEBUG);
				pagoRefSatBean.setNumOpe(String.valueOf(rs.getLong(1)));
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace(
						"PagoReferenciadoSatDAO - consultaNumeroDeOperacion() - "
								+ ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return pagoRefSatBean;
	}

	/**
	 * Compara la fecha de la operacion con la fecha actual
	 * 
	 * @param fch
	 *            fecha de la operacion
	 * @return true/false si son iguales
	 */
	public boolean fechaIgual(String fch) {
		GregorianCalendar calHoy = new GregorianCalendar();
		int anio1 = Integer.parseInt(fch.substring(6));
		int anio2 = calHoy.get(Calendar.YEAR);
		int mes1 = Integer.parseInt(fch.substring(3, 5));
		int mes2 = calHoy.get(Calendar.MONTH) + 1;
		int dia1 = Integer.parseInt(fch.substring(0, 2));
		int dia2 = calHoy.get(Calendar.DATE);
		if (anio1 == anio2 && mes1 == mes2 && dia1 == dia2) {
			return true;
		}
		return false;
	}
	
	/**
	 * Metodo que consulta la razon social de la cuenta cargo
	 * para el comprobante
	 * @param pagoRefSatBean contiene la cuenta cargo y el contrato
	 * @return bean con la razon social de la cta cargo
	 */
	public PagoReferenciadoSatBean consultaRazonSocCtaCargo(
			PagoReferenciadoSatBean pagoRefSatBean) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String selectFull = String.format(SELECT_CTA_CARGO, pagoRefSatBean
				.getContrato(), pagoRefSatBean.getCuentaCargo());
		EIGlobal.mensajePorTrace(
				"PagoReferenciadoSatDAO - consultaRazonSocCtaCargo - query ["
						+ selectFull + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format(
				"PagoReferenciadoSatDAO consultaRazonSocCtaCargo - QRY [%s]",
				selectFull), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectFull);
			if (rs.next()) {
				EIGlobal.mensajePorTrace("Razon Social de Cuenta Cargo -> "
						+ rs.getString(1), NivelLog.DEBUG);
				pagoRefSatBean.setRazonSocCtaCargo(rs.getString(1));
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		} finally {
			try {
				rs.close();
				stmt.close();
				conn.close();
				if (conn != null) {
					cierraConexion(conn);
				}
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace(
						"PagoReferenciadoSatDAO - consultaRazonSocCtaCargo() - "
								+ ERROR_CONEXION + e1 + "]", NivelLog.ERROR);
			}
		}
		return pagoRefSatBean;
	}

	/**
	 * Metodo que consume la transaccion LZCP para obtener la llave de pago
	 * 
	 * @param pagRefSatBean
	 *            bean con campos de entrada para la LZCP
	 * @return Bean con la llave de pago
	 */
	public PagoReferenciadoSatBean ejecutaLZCP(
			PagoReferenciadoSatBean pagRefSatBean) {
		StringBuilder tramaLZCP = new StringBuilder();
		String respuestaLZCP = "";
		EIGlobal.mensajePorTrace("PagoReferenciadoSatDAO:LZCP:lineaDeCaptura->"
				+ pagRefSatBean.getLineaDeCaptura(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"PagoReferenciadoSatDAO:LZCP:numeroDeOperacion->"
						+ pagRefSatBean.getNumOpe(), NivelLog.DEBUG);

		tramaLZCP.append(HEADER_LZCP)
				.append(rellenar(pagRefSatBean.getLineaDeCaptura(), 20, ' ', 'D'))
				.append(rellenar(pagRefSatBean.getNumOpe(), 9, '0', 'I'))
				.append(rellenar(" ", 10, ' ', 'I'));
		EIGlobal.mensajePorTrace(LEYENDA_LOG
				+ tramaLZCP.toString() + "]", NivelLog.DEBUG);
		respuestaLZCP = invocarTransaccion(tramaLZCP.toString());
			
		return desentramaLZCP(respuestaLZCP);
	}

	/**
	 * Metodo que desentrama la respuesta de la transaccion LZCP
	 * 
	 * @param respuesta
	 *            trama de respuesta de la transaccion LZCP
	 * @return Bean con la llave de pago
	 */
	private PagoReferenciadoSatBean desentramaLZCP(String respuesta) {
		PagoReferenciadoSatBean pagoRefSat = new PagoReferenciadoSatBean();
		String[] arrayTrama = null;
		EIGlobal.mensajePorTrace(String.format(
				"PagoReferenciadoSatDAO:desentramaLZCP tramaRespuesta[%s]",
				respuesta), NivelLog.DEBUG);
		
		if( respuesta.length() > 0 ){
			arrayTrama = respuesta.split("[@]");
		}
		
		if( arrayTrama.length > 2 ){
			if( arrayTrama[2].indexOf("DCLZMMLCP") >= 0 ){
				pagoRefSat.setCodError(arrayTrama[2].substring(0, 9));
				pagoRefSat.setDescError("LLAVE DE PAGO RECUPERADA");
				pagoRefSat.setLlaveDePago(arrayTrama[2].substring(40,50));//Llave de pago
				EIGlobal.mensajePorTrace(String.format(
						"PagoReferenciadoSatDAO:desentramaLZCP Llave de Pago[%s]",
						pagoRefSat.getLlaveDePago()), NivelLog.DEBUG);
				pagoRefSat.setDescError("HORA DE PAGO RECUPERADA");
				pagoRefSat.setHoraLZCP(arrayTrama[2].substring(50,55));//Hora de pago
				EIGlobal.mensajePorTrace(String.format(
						"PagoReferenciadoSatDAO:desentramaLZCP HORA de Pago[%s]",
						pagoRefSat.getHoraLZCP()), NivelLog.DEBUG);
			} else {
				EIGlobal.mensajePorTrace(String.format(
						"PagoReferenciadoSatDAO:desentramaLZCP tramaRespuesta Codigo de Error[%s]",
						arrayTrama[2].substring(0, 9)), NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(String.format(
						"PagoReferenciadoSatDAO:desentramaLZCP tramaRespuesta Mensaje de Error[%s]",
						arrayTrama[2].substring(10)), NivelLog.DEBUG);
				pagoRefSat.setLlaveDePago("X");
				pagoRefSat.setHoraLZCP("");
			}
		} else {
			pagoRefSat.setLlaveDePago("X");
			pagoRefSat.setHoraLZCP("");
		}
		
		
		return pagoRefSat;
	}
}
