package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;

import mx.altec.enlace.beans.DY02Bean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;

public class DY02DAO {
	/** Almacena la lista de los codigos de operacion y su descripcion */
	public static final Map<String, String> CODIGOS_OPERACION = crearMapaCodigos();
	/** Id de la operacion exitosa */
	private static final String OPERACION_EXITOSA = "DYA0001";
	
	/**
	 * Crea un mapa con los posibles codigos de error de la transaccion DY02
	 * @return Mapa con los codigos de error de la DY02
	 */
	private static Map<String, String> crearMapaCodigos(){
		Map<String, String> codigos = new HashMap<String, String>();
		
		codigos.put("DYE0010", "El c&oacute;digo del cliente esta mal informado");
		codigos.put("DYE0011", "El c&oacute;digo de cuenta esta mal informado");
		codigos.put("DYE0012", "El formato esta mal informado");
		codigos.put("DYE0013", "El tipo de cuenta no es valido");
		codigos.put("DYE0014", "El folio debe ser informado");
		codigos.put("DYE0015", "El canal no esta informado");
		codigos.put("DYE0016", "El canal informado no es valido");
		codigos.put("DYE0017", "Opci&oacute;n informada no es valida");
		codigos.put("DYE0019", "El registro que intenta dar de alta ya existe");
		codigos.put("DYE0020", "El regisro que se intenta dar de baja no existe");
		codigos.put("DYE0022", "No se ha proporcionado toda la informaci&oacute;n");
		codigos.put("DYE0023", "El periodo esta mal informado, el formato es AAAAMM");
		codigos.put("DYE0031", "Ya existe un petici&oacute;n para el periodo dado");
		codigos.put("DYE0032", "Ya existe un petici&oacute;n para el periodo dado");
		codigos.put("DYE0090", "Error de acceso a la informaci&oacute;n");
		codigos.put("DYA0001", "Alta exitosa");
		return Collections.unmodifiableMap(codigos);
	}
	
	/**
	 * Ejecuta una consulta a la transaccion DY02
	 * @param bean Objeto con la informacion necesaria para ejecutar la transaccion DY02
	 * @return Objeto con la informacion obtenida de la ejecucion de la transaccion DY02
	 * @throws BusinessException En caso de que exista un error con la operacion
	 */
	public DY02Bean ejecutarDY02(DY02Bean bean) throws BusinessException {
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY02() Inicio", EIGlobal.NivelLog.INFO);
		boolean ocurrioUnError = false;
		
		try {
			final MQQueueSession ejecutor = 
					new MQQueueSession(
							NP_JNDI_CONECTION_FACTORY, 
							NP_JNDI_QUEUE_RESPONSE, 
							NP_JNDI_QUEUE_REQUEST);		
			EIGlobal.mensajePorTrace( String.format("%s:::consultaDY02() Trama de envio::%s", 
									  this.getClass().getName(),bean.generarMensajeEntrada()), 
									  EIGlobal.NivelLog.INFO);
			final String respuesta = ejecutor.enviaRecibeMensaje(bean.generarMensajeEntrada());
			EIGlobal.mensajePorTrace(
					String.format("%s:::consultaDY02() Trama de respuesta::%s", this.getClass().getName(), respuesta), 
					EIGlobal.NivelLog.INFO);
			final String[] respuestaSeparada = respuesta.split("@");
			final String codigoRespuesta = respuestaSeparada[2].substring(2, 9);
			final String descripcionRespuesta = respuestaSeparada[2].substring(10,respuestaSeparada[2].length());
			bean.setCodigoRespuesta(codigoRespuesta);
			bean.setDescripcionRespuesta(descripcionRespuesta);
			
			if(!OPERACION_EXITOSA.equals(codigoRespuesta)){
				ocurrioUnError = true;
				throw new BusinessException(codigoRespuesta);
			}
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaDY02() Fin", EIGlobal.NivelLog.INFO);
		} catch (NamingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			ocurrioUnError = true;
		} catch (JMSException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			ocurrioUnError = true;
		} catch (ValidationException e) {
			EIGlobal.mensajePorTrace("--------->Error: " + e.getCause() + e.getLocalizedMessage(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			ocurrioUnError = true;
		} catch (MQQueueSessionException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			ocurrioUnError = true;
		}

		if(ocurrioUnError){
			throw new BusinessException("SECHDY2");
		}
		return bean;
	}
}