/**
* ISBAN Mexico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* ModuloActivoDAO.java
*
* Control de versiones:
*
* Version	Date/Hour		By				Company		Description
* -------	--------------	-------------	--------	-----------
* 1.0		03-12-13 18:02 	Ricardo Perez	Isban		Creacion
*
*/
package mx.altec.enlace.dao;


/**
 * Clase de acceso a datos la cual se conecta a GFI para obtener el valor
 * de la bandera que indica si se muestra o no el contenido de un menu 
 * en particula.
 */
public class ModuloActivoDAO extends GenericDAO {
	
	
}