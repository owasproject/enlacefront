package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class CertificadoDigitalDAO {
	
	/**statemant**/
	private transient PreparedStatement statement =null;
	/**conn**/
	private transient Connection conn=null;
	/**query**/
	private transient String query = "";


	/**
	 * Metodo que valida si el contrato tiene servicio de importacion
	 * @param contrato : contrato enlace
	 * @param idFlujo : id del flujo
	 * @return boolean
	 * @throws SQLException : exception  
	 */
	public boolean tieneServicioImportacion(String contrato, int idFlujo)throws SQLException {
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - tieneServicioImportacion() - " +
				"Cliente: [" + contrato + "]", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		boolean res = false;
		try {
    		//conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE);
			//conn = GenericDAO.createiASConnStatic(Global.DATASOURCE_ORACLE3);
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			
			query = creaQueryServicioExp(contrato);
			
			statement = conn.prepareStatement (query);
			
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - tieneServicioImportacion() - " +
					"query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
			
			rs = statement.executeQuery();
			
			if(rs.next()) {
				res = valida(rs, idFlujo);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - tieneServicioImportacion() " +
					"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
			throw new SQLException (e);
		} finally {
			try{
				if(statement!=null){
					statement.close();
					statement=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
				if(rs!=null){
					rs.close();
					rs=null;}
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace ("ChequeDigitalDAO - tieneChequeDigital() - " +
					"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("ChequeDigitalDAO - tieneChequeDigital() - fin"
					, EIGlobal.NivelLog.INFO);
		}
		
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - tieneServicioImportacion() --------> " + res, EIGlobal.NivelLog.INFO);
		
		return res;
	}

	
	/**
	 * Validacion de servicio activo
	 * @return boolean
	 * @throws SQLException : exception
	 */
	public boolean servicioActivo() throws SQLException {
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - servicioActivo() - ", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		Integer valEnt = 0;
		boolean resultado = false;
		
		try {
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE);
    		
			query = "select  valor_entero  from EWEB_PARAMETROS " +
			"where nombre_param = 'CONTIARCIF'";
			
			statement = conn.prepareStatement (query);
			
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - servicioActivo() - " +
					"query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
			
			rs = statement.executeQuery();
			
			if(rs.next()) {
				valEnt = rs.getInt("valor_entero");
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - servicioActivo() " +
					"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
			throw new SQLException (e);
		} finally {
			try{
				if(statement!=null){
					statement.close();
					statement=null;}
				if(conn!=null){
					conn.close();
					conn=null;}
				if(rs!=null){
					rs.close();
					rs=null;}
			}catch( SQLException e1){
				EIGlobal.mensajePorTrace ("ChequeDigitalDAO - servicioActivo() - " +
					"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace("ChequeDigitalDAO - servicioActivo() - fin"
					, EIGlobal.NivelLog.INFO);
		}
		
		if( valEnt == 0 ){
			resultado = true;
		}
		
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - servicioActivo() --------> " + resultado, EIGlobal.NivelLog.INFO);
		
		return resultado;
	}
	
	/**
	 * Validacion de servicio importacion
	 * @param rs Result set para iterar registro
	 * @param flujo flujo que se busca
	 * @return boolean
	 * @throws SQLException : exception
	 */
	private boolean valida(ResultSet rs, int flujo) throws SQLException {
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - valida() - ", EIGlobal.NivelLog.INFO);
		String estatus = "", servicios = "";
		boolean resultado = false;
		estatus = rs.getString("ESTATUS");
		servicios = rs.getString("SERVICIOS");
		
		if("A".equals(estatus)){
			resultado = buscaEnCadena(servicios, flujo);
		}else{
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - valida() --------> no tiene el estatus A ", EIGlobal.NivelLog.INFO);
		}
		
		return resultado;
	}
	
	/**
	 * buscaEnCadena el servicio que se evalua
	 * @param servicios : servicios activos
	 * @param flujo : flujo que se busca en los servicios
	 * @return boolean
	 * @throws SQLException : exception
	 */
	private boolean buscaEnCadena(String servicios, int flujo) throws SQLException {
		EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - buscaEnCadena() - ", EIGlobal.NivelLog.INFO);
		boolean resultado = false;
		String [] arregloServ = servicios.split(",");
		for(String valor : arregloServ){
			if(valor.equals(String.valueOf(flujo))){
				resultado = true;
			}
		}
		if(!resultado){
			EIGlobal.mensajePorTrace ("CertificadoDigitalDAO - buscaEnCadena() --------> no tiene el servicio ", EIGlobal.NivelLog.INFO);
		}
		return resultado;
	}
	
	/**
	 * Genera query
	 * @param contrato : contrato
	 * @return String query
	 */
	private String creaQueryServicioExp(String contrato) {
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT * ");
		query.append(" FROM EWEB_CONTRATOS_ARCH_CIF ");
		query.append(" WHERE num_cuenta = '");
		query.append(contrato);
		query.append("'");
		
		return query.toString();
		
	}

}
