package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import mx.altec.enlace.beans.ConfEdosCtaTcIndBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;


public class ConfEdosCtaTcIndDAO extends GenericDAO {
	/** Constante para mensaje de query */
	private static final String QUERY = " query: [";
	/** Constante para mensaje de error al cerrar conexion */
	private static final String ERROR_CONEXION = "Error al cerrar conexion: [";
	/** LOG TAG**/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfEdosCtaTcIndDAO ::: ";
	/**
	 * Constante para armar el select para la configuracion previa
	 */
	private static final String SELECTENCABEZADOCONFPREVIA = new StringBuilder(
			"SELECT A.FCH_ENVIO, A.ID_EDO_CTA_CTRL, A.NUM_CUENTA2 FROM EWEB_EDO_CTA_CTRL A")
			.append(" INNER JOIN EWEB_EDO_CTA_DET B ON A.ID_EDO_CTA_CTRL = B.ID_EDO_CTA_CTRL WHERE")
			.toString();
	/**
	 * Constante para armar el where del select para la configuracion previa
	 */
	private static final String WHERECONFPREVIA = new StringBuilder(
			" AND A.ESTATUS = 'P' AND B.ESTATUS = 'A' AND A.FCH_ENVIO = TRUNC(SYSDATE)")
			.toString();
	
	/**
	 * Metodo para consultar si ya existe una configuracion el mismo dia para la
	 * cuenta o tarjeta
	 * Se agrega metodo 03/03/2015 PYME FSW INDRA
	 * @param seqDomicilio seqDomicilio
	 * @param codCliente codigo del ciente
	 * @param tarjeta numero de la tarjeta de credito
	 * @return confEdosCtaTcIndBean Bean de la informacion
	 */
	public ConfEdosCtaTcIndBean consultaConfigPreviaEdoCta(String seqDomicilio,String codCliente, String tarjeta) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		StringBuilder where = new StringBuilder();
		if( tarjeta != null ){
			where.append(" B.NUM_CUENTA = '")
				.append(tarjeta).append("'");
		} else {
			where.append(" B.NUM_SEC = '")
				.append(seqDomicilio).append("' AND B.COD_CLTE = '")
				.append(codCliente).append("'");
		}
		String selectComplet = new StringBuilder(SELECTENCABEZADOCONFPREVIA)
			.append(where).append(WHERECONFPREVIA).toString();
		EIGlobal.mensajePorTrace(String.format(
				"ConfEdosCtaTcIndDAO consultaConfigPreviaEdoCta - QRY [%s]",
				selectComplet), NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(selectComplet);
			if(rs.next()) {
				EIGlobal.mensajePorTrace(
						"Fecha de Config de la cuenta ->" + rs.getDate(1),
						NivelLog.DEBUG);
				confEdosCtaTcIndBean.setFechaConfig(rs.getDate(1));
				confEdosCtaTcIndBean.setFolioOp(String.valueOf(rs.getInt(2)));
				confEdosCtaTcIndBean.setContrato(rs.getString(3));
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				stmt.close();
				conn.close();
				rs.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
						"ConfEdosCtaTcIndDAO - consultaConfigPreviaEdoCta() - Error al cerrar conexion: ["
								+ e1 + "]",
						NivelLog.ERROR);
			}
		}
		return confEdosCtaTcIndBean;
	}
	
	/**
	 * Metodo para consultar si ya existe una configuracion el mismo dia de PDF para una Tarjeta
	 * 
	 * @param tarjeta : Numero de tarjeta
	 * @param codCliente : Codigo de cliente a consultar
	 * @return Date : Fecha de la configuracion previa.
	 */
	public Date consultaConfigPreviaTarjeta(String tarjeta, String codCliente) {
		//StringBuilder query = new StringBuilder();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Date fechaConfig = null;
		String query=String.format("SELECT A.FCH_ENVIO, A.ID_EDO_CTA_CTRL, A.NUM_CUENTA2" +
				" FROM EWEB_EDO_CTA_CTRL A" +
				" INNER JOIN EWEB_EDO_CTA_DET B ON A.ID_EDO_CTA_CTRL = B.ID_EDO_CTA_CTRL" +
				" WHERE B.NUM_CUENTA = '%s'" +
				" AND B.COD_CLTE = '%s'" +
				" AND A.ESTATUS = 'P' AND B.ESTATUS = 'A'" +
				" AND A.FCH_ENVIO = TRUNC(SYSDATE)",tarjeta,codCliente);	
		EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - consultaConfigPreviaTarjeta -" + QUERY
						+ query + "]", NivelLog.INFO);
		EIGlobal.mensajePorTrace(String.format("%s consultaConfigPreviaSeqDom - QRY [%s]",LOG_TAG,query),NivelLog.DEBUG);
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if(rs.next()) {
				EIGlobal.mensajePorTrace("setFechaConfig ->" + rs.getDate(1),
						NivelLog.DEBUG);
				fechaConfig = rs.getDate(1);
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e),
					NivelLog.INFO);
		}finally {
			try{
				rs.close();
				stmt.close();
				conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace("ConfEdosCtaIndDAO - consultaConfigPreviaSeqDom() - "+ERROR_CONEXION + e1 + "]",
						NivelLog.ERROR);
			}
		}
		return fechaConfig;
	}	
}