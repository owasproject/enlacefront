/** 
*   Isban Mexico
*   Clase: DAOClienteFiel.java
*   Descripcion: Clase de servicio encargada de consumir el servicio web de FIEL .
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.dao;

import javax.xml.ws.WebServiceException;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.cliente.ws.etransfernal.BeanReqConsultaCertificado;
import mx.altec.enlace.cliente.ws.etransfernal.BeanResConsultaCertificado;
import mx.altec.enlace.cliente.ws.etransfernal.ConsultaCertificadoService;
import mx.altec.enlace.cliente.ws.etransfernal.WSConsultaCertificado;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;

/**
 * Objeto de acceso a datos para datos devueltos por Banxico
 * 
 * @author FSW Everis
 *
 */
public class DAOClienteFiel {

	 	/**
		 * Metodo para buscar fiel
		 * @param beanDetalle DTO para construir la entrada del servicio web
		 * @return DTO de resultado
		 */
		public BeanAdminFiel buscaFiel(BeanAdminFiel beanDetalle) {
			EIGlobal.mensajePorTrace(new StringBuilder("Inicio buscaFiel").toString(), EIGlobal.NivelLog.INFO);
			BeanReqConsultaCertificado wsIiput = new BeanReqConsultaCertificado();
			wsIiput.setNumCertificado(beanDetalle.getFiel());
			ConsultaCertificadoService service = null;
	        WSConsultaCertificado wsImpl = null;
			BeanResConsultaCertificado response = null;
			
			try {
				service = new ConsultaCertificadoService();
		        wsImpl = service != null ? service.getWSConsultaCertificadoImplPort() : null;
		        response = wsImpl != null ? wsImpl.consultaCertificado(wsIiput) : null;
			} catch (WebServiceException e) {
				EIGlobal.mensajePorTrace("buscaFiel WebServiceException", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTraceExcepcion(e);
			}
			
			if (response != null) {
				beanDetalle.setDetalleConsulta(response);
				beanDetalle.setCodError(FielConstants.COD_ERRROR_OK);
				
				EIGlobal.mensajePorTrace("***** DATOS DEVUELTOS POR WS-BANXICO *****"
						+ "\ncodigo Respuesta|" + beanDetalle.getDetalleConsulta().getCodRespuesta() + FielConstants.BARRA
						+ "\nmensaje|" + beanDetalle.getDetalleConsulta().getMensaje() + FielConstants.BARRA
						+ "\nCodigoPostal|" + beanDetalle.getDetalleConsulta().getDatosTitular().getCodPostal() + FielConstants.BARRA
						+ "\nCURP|" + beanDetalle.getDetalleConsulta().getDatosTitular().getCurp() + FielConstants.BARRA
						+ "\nDireccion|" + beanDetalle.getDetalleConsulta().getDatosTitular().getDireccion() + FielConstants.BARRA
						+ "\nEmail|" + beanDetalle.getDetalleConsulta().getDatosTitular().getEmail() + FielConstants.BARRA
						+ "\nEstado|" + beanDetalle.getDetalleConsulta().getDatosTitular().getEstado() + FielConstants.BARRA
						+ "\nLocalidad|" + beanDetalle.getDetalleConsulta().getDatosTitular().getLocalidad() + FielConstants.BARRA
						+ "\nNombreOrganizacion|" + beanDetalle.getDetalleConsulta().getDatosTitular().getNombreOrganizacion() + FielConstants.BARRA
						+ "\nNombreTitular|" + beanDetalle.getDetalleConsulta().getDatosTitular().getNombreTitular() + FielConstants.BARRA
						+ "\nNumPasaporteIfe|" + beanDetalle.getDetalleConsulta().getDatosTitular().getNumPasaporteIfe() + FielConstants.BARRA
						+ "\nPais|" + beanDetalle.getDetalleConsulta().getDatosTitular().getPais() + FielConstants.BARRA
						+ "\nRfc|" + beanDetalle.getDetalleConsulta().getDatosTitular().getRfc() + FielConstants.BARRA
						+ "\nEstadoCertificado|" + beanDetalle.getDetalleConsulta().getEstadoCertificado() + FielConstants.BARRA
						+ "\nFchEmision|" + beanDetalle.getDetalleConsulta().getFchEmision() + FielConstants.BARRA
						+ "\nFchExpiracion|" + beanDetalle.getDetalleConsulta().getFchExpiracion() + FielConstants.BARRA
						+ "\nNumSerie|" + beanDetalle.getDetalleConsulta().getNumSerie() + FielConstants.BARRA
						, EIGlobal.NivelLog.INFO);
			} else {
				beanDetalle.setCodError(FielConstants.COD_ERRROR_KO);
			}

			EIGlobal.mensajePorTrace(new StringBuilder("Fin buscaFiel codError|").append(beanDetalle.getCodError()).append(FielConstants.BARRA).toString(), EIGlobal.NivelLog.INFO);
			return beanDetalle;
		}
}
