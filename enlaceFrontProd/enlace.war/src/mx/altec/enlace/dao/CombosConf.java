package mx.altec.enlace.dao;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.reflect.*;
import java.sql.*;

import java.text.*;
import javax.sql.*;
import javax.naming.*;
import java.math.BigDecimal.*;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.File;
import java.io.IOException;

public class CombosConf implements Serializable
	{

/* Q1346 JEV Inicio Getronics CP*/
     public Connection createiASConn ( String dbName )
     throws SQLException {

	 Connection conn = null;
	 try {
	     InitialContext ic = new InitialContext ();
	     DataSource ds = ( DataSource ) ic.lookup ( dbName );
	     conn = ds.getConnection ();
	 } catch ( NamingException ne ) {

	 }
	 return conn;
     }
/* Q1346 JEV FIN Getronics CP */

	/** Servico de Tuxedo */
	protected ServicioTux CombosConf;

	/** Numero de contrato */
	protected String Contrato;

	/** Clave de Usuario */
	protected String Usuario;

	/** Perfil del usuario */
	protected String Perfil;

	/** Creates new pdCombosConf */
	public CombosConf() {CombosConf = new ServicioTux();}

	/**
	* Metodo para obtener el servicio de Tuxedo
	*/
	public ServicioTux getServicioTux() {return CombosConf;}

	//
	protected File recibe(String Nombre)
		{
		try
			{
			EIGlobal.mensajePorTrace("CombosConf:recibe - Inicia", EIGlobal.NivelLog.INFO);
			//IF PROYECTO ATBIA1 (NAS) FASE II
			File Archivo = new File(Nombre);
           	
			if (Global.HOST_LOCAL.equals (Global.HOST_REMOTO)) return new File (Nombre);
			
			//Version JSCH, Proyecto CNBV
           	boolean Respuestal = true;
            ArchivoRemoto recibeArch = new ArchivoRemoto();
           
           	if(!recibeArch.copiaCUENTAS("/" + Nombre, Global.DIRECTORIO_LOCAL)){
				
					EIGlobal.mensajePorTrace("*** CombosConf.recibe  No se realizo la copia remota:" + Nombre, EIGlobal.NivelLog.ERROR);
					Respuestal = false;
					
				}
				else {
				    EIGlobal.mensajePorTrace("*** CombosConf.recibe archivo remoto copiado exitosamente:" + Nombre, EIGlobal.NivelLog.DEBUG);
				    
				}
 
           	if (Respuestal) {
        	
                //Archivo.setReadOnly();
                return Archivo;
                
           	}
            }
		catch (Exception ex)
			{
			//EIGlobal.mensajePorTrace (ex.getMessage(), EIGlobal.NivelLog.INFO);
			//ex.printStackTrace(System.err);
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error al recibir archivo", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("CombosConf::recibe:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			}
		return null;
		}


	//
	/* Q1346 JEV Inicio Getronics CP */
		public String envia_tux_Cancelacion(String usr)
		throws ServletException, IOException
		{
			String cancela="";
			String linea="";
			String Error="SUCCESS";
			Connection conn = null;
			String nombre="";

		ResultSet cancelaResult = null;
		int totales = 0;

		EIGlobal.mensajePorTrace("CombosConf - execute(): Entrando Cancelacion de los proeveedores.", EIGlobal.NivelLog.INFO);

		BufferedWriter Arch =null;


		try
			{

		  Arch= new BufferedWriter(new FileWriter(Global.DIRECTORIO_LOCAL + "/" + Usuario + ".actua"));


	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
			if(conn == null)
			 {
				EIGlobal.mensajePorTrace("CombosConf - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			 /****** consulta *******/
 			EIGlobal.mensajePorTrace("Ejecutando el Query: ", EIGlobal.NivelLog.ERROR);
			cancela = "SELECT nvl(num_persona,' '), nvl(cve_proveedor,' '), nvl(nombre_razon_soc,' '),nvl(apell_paterno,' '),nvl(apell_materno,' '),nvl(persona_juridica,' '),nvl(estatus,' ') FROM cfrm_proveedores WHERE num_cuenta2 = '"+ Contrato +"'";
			EIGlobal.mensajePorTrace("El Query es: " + cancela, EIGlobal.NivelLog.ERROR);
			PreparedStatement cancelaQuery = conn.prepareStatement(cancela);
			if(cancelaQuery == null)
			 {
				EIGlobal.mensajePorTrace("CombosConf - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }
			cancelaResult = cancelaQuery.executeQuery();
			if(cancelaResult==null)
			  {
				EIGlobal.mensajePorTrace("CombosConf- execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
				Error="NOSUCCESS";
			 }


/********************/
			if(Error.equals("SUCCESS"))
			 {
					while (cancelaResult.next())
					{
						//linea=cancelaQuery.getString(1).trim();
						cancela="9;"+cancelaResult.getString(6)+";"+cancelaResult.getString(1)+";"+cancelaResult.getString(2)+";"+cancelaResult.getString(3)+";"+cancelaResult.getString(4)+";"+cancelaResult.getString(5)+";"+cancelaResult.getString(7)+";"+"\n";
						Arch.write( cancela );
					}
				   nombre=Global.DIRECTORIO_LOCAL + "/" + Usuario + ".actua";
			 }
			else
			 nombre="Error en la comunicacion con la base de datos";
		 }catch( SQLException sqle )
		 {//sqle.printStackTrace(); 
			 nombre="Error en la generacion de archivo";
				StackTraceElement[] lineaError;
				lineaError = sqle.getStackTrace();
				EIGlobal.mensajePorTrace("Error en la generacion de archivo", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("CombosConf::envia_tux_Cancelacion:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ sqle.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
		 }
		finally
		  {
			try
			{
				Arch.close();
				cancelaResult.close();
				conn.close();
			 }catch(Exception ex)
				{nombre= "Error en la comunicacion";}
		  }
	  EIGlobal.mensajePorTrace("CombosConf ... > El nombre del archivo: "+nombre, EIGlobal.NivelLog.ERROR);
	  return nombre;

	}

	/* Q1346 JEV FIN Getronics CP */
	///
	public String envia_tux(String divisa)
		{
		EIGlobal.mensajePorTrace("----->Divisa prueba 1 ----------- "+divisa, EIGlobal.NivelLog.INFO);
		  if(divisa.equals("")){divisa="";

			EIGlobal.mensajePorTrace("----->Divisa prueba 2 ----------- "+divisa, EIGlobal.NivelLog.INFO);
		  }
		try
			{
			EIGlobal.mensajePorTrace ("<DEBUG ******entrando y ejecutando envia_tux> ", EIGlobal.NivelLog.INFO);
			String Trama = "2EWEB|" + Usuario + "|CFAC|" + Contrato +"#" +divisa +"#" + "|" + Usuario + "|" + Perfil + "|";
			EIGlobal.mensajePorTrace("----->Trama ----------- "+Trama, EIGlobal.NivelLog.INFO);
			File ArchivoResp;
			Hashtable ht = CombosConf.web_red(Trama); debug("trama enviada: " + Trama);
			String Buffer = (String) ht.get("BUFFER"); debug("trama devuelta: " + Buffer);
			ArchivoResp = recibe(Buffer); debug("ArchivoResp: " + ArchivoResp);


			if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
		boolean RR = ArchivoResp.renameTo (new File(ArchivoResp.getPath() + ".actua"));
		debug("RR: " + RR); debug("ArchivoResp: " + ArchivoResp);
			return (ArchivoResp.getPath() + ".actua");
			}
		catch (IOException ex)
			{
			//debug("Error: " + ex);
			//ex.printStackTrace(System.err);
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error al leer el archivo de respuesta", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("CombosConf::envia_tux:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
			return "Error al leer el archivo de respuesta";
			}
		catch (Exception ex)
			{
			//debug("Error: " + ex);
			//ex.printStackTrace(System.err);
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error con la comunicacion", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("CombosConf::envia_tux:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);			
			return "Error con la comunicacion";
			}
		}

	//
	public String envia_tux()
	{


	try
		{
		EIGlobal.mensajePorTrace ("<DEBUG ******entrando y ejecutando envia_tux> ", EIGlobal.NivelLog.INFO);
		String Trama = "2EWEB|" + Usuario + "|CFAC|" + Contrato +"##"+ "|" + Usuario + "|" + Perfil + "|";

		File ArchivoResp;
		Hashtable ht = CombosConf.web_red(Trama); debug("trama enviada: " + Trama);
		String Buffer = (String) ht.get("BUFFER"); debug("trama devuelta: " + Buffer);
		ArchivoResp = recibe(Buffer); debug("ArchivoResp: " + ArchivoResp);


		if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
	boolean RR = ArchivoResp.renameTo (new File(ArchivoResp.getPath() + ".actua"));
	debug("RR: " + RR); debug("ArchivoResp: " + ArchivoResp);
		return (ArchivoResp.getPath() + ".actua");
		}
	catch (IOException ex)
		{
		//debug("Error: " + ex);
		//ex.printStackTrace(System.err);
		StackTraceElement[] lineaError;
		lineaError = ex.getStackTrace();
		EIGlobal.mensajePorTrace("Error al leer el archivo de respuesta", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("CombosConf::envia_tux:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ ex.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.ERROR);				
		return "Error al leer el archivo de respuesta";
		}
	catch (Exception ex)
		{
		//debug("Error: " + ex);
		//ex.printStackTrace(System.err);
		StackTraceElement[] lineaError;
		lineaError = ex.getStackTrace();
		EIGlobal.mensajePorTrace("Error con la comunicacion", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("CombosConf::envia_tux:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ ex.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.ERROR);			
		return "Error con la comunicacion";
		}
	}

//
	public String actualiza_grafica(String fechaIni, String fechaLim)
		{
		try
			{
			String Trama = "2EWEB|" + Usuario + "|CFAE|" + Contrato + "|" + Usuario + "|"
				+ Perfil + "|2@" + fechaIni + "@" + fechaLim + "@|";
			File ArchivoResp;
			Hashtable ht = CombosConf.web_red (Trama); debug("trama enviada: " + Trama);
			String Buffer = (String) ht.get ("BUFFER"); debug("trama devuelta: " + Buffer);
			ArchivoResp = recibe(Buffer); debug("ArchivoResp: " + ArchivoResp);
			if (ArchivoResp == null) return "Error al recibir el archivo de respuesta";
		boolean RR = ArchivoResp.renameTo (new File (ArchivoResp.getPath() + ".graf"));
		debug("RR: " + RR); debug("ArchivoResp: " + ArchivoResp);
			return (ArchivoResp.getPath() + ".graf");
			}
		catch (IOException ex)
			{
			//debug("Error: " + ex);
			//ex.printStackTrace(System.err);
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error al leer el archivo de respuesta", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("CombosConf::actualiza_grafica:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);				
			return "Error al leer el archivo de respuesta";
			}
		catch (Exception ex)
			{
			//debug("Error: " + ex);
			//ex.printStackTrace(System.err);
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error con la comunicacion", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("CombosConf::actualiza_grafica:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);			
			return "Error con la comunicacion";
			}
		}

	//
	public void setContrato (String contrato) {Contrato = contrato;}

	//
	public void setUsuario (String usuario) {Usuario = usuario;}

	//
	public void setPerfil (String perfil) {Perfil = perfil;}

	//
	private void debug(String mensaje)
		{
		EIGlobal.mensajePorTrace ("<DEBUG CombosConf.java> " + mensaje, EIGlobal.NivelLog.INFO);
		}
	}