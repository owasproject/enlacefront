package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import mx.altec.enlace.beans.NominaArchBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class NominaArchDAO {
	
	/**
	 * Inserta datos del archivo de Nomina
	 * @param bean contiene los datos a registrar
	 * @return exito bandera de ejecucion de la operacion
	 */
	public boolean registraNominaArch(NominaArchBean bean){
		EIGlobal.mensajePorTrace("NominaArchDAO.java :: registraNominaArch() :: Entrando al metodo registraNominaArch()", EIGlobal.NivelLog.INFO);
		Connection conn = null;
    	boolean exito = true;
    	PreparedStatement statement = null;

    	try {
			StringBuffer query = new StringBuffer("");
			query.append("INSERT INTO EWEB_NOMCONT_ARCH (FCH_CARGA, CONTRATO, NOM_ARCH, NUM_REG, IMPORTE_REC, ESTATUS_CARGA, BIATUX_ORIGEN, TIPO_ARCHIVO)");
			query.append(" VALUES(");
			query.append("to_date('");
			query.append(bean.getFechaCarga());
			query.append("','DD-MM-YYYY HH24:MI'),'");
			query.append(bean.getContrato());
			query.append("','");
			query.append(bean.getNombreArchivo());
			query.append("',");
			query.append(bean.getNumRegistros());
			query.append(",");
			query.append(bean.getImporte());
			query.append(",'");
			query.append(bean.getEstatusCarga());
			query.append("','");
			query.append(bean.getBiatuxOrigen());
			query.append("','");
			query.append(bean.getTipoArchivo());
			query.append("')");
			
			EIGlobal.mensajePorTrace("NominaArchDAO :: registraNominaArch :: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE3);
			statement = conn.prepareStatement(query.toString());
			statement.executeQuery();
			conn.commit();
			EIGlobal.mensajePorTrace("NominaArchDAO :: registraNominaArch :: Saliendo... "+exito,EIGlobal.NivelLog.DEBUG);

    	}catch (Exception e){
    		EIGlobal.mensajePorTrace("NominaArchDAO :: registraNominaArch :: Se controlo problema al insertar al batch "
    				+ " tipo de error->" + e.getMessage(),EIGlobal.NivelLog.ERROR);
    		exito = false;
    	}finally {
			try {
				if (statement != null) {
					statement.close();
    	}
				if(conn!=null){
					conn.close();
					conn=null;
			    }
			}catch(SQLException e){		
				EIGlobal.mensajePorTrace("NominaArchDAO :: registraNominaArch :: Error al cerrar la conexion "
	    				+ " tipo de error->" + e.getMessage(),EIGlobal.NivelLog.ERROR);
			}	
		}
    	return exito;
    }

}