/**
 * 
 */
package mx.altec.enlace.dao;

/**
 * @author rgutierrez
 */
public class DaoException extends Exception {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 6858218594743141419L;
	/**
	 * Constructor por default.
	 *
	 */
	public DaoException() {
		
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 */
	public DaoException(String message) {
		super(message);
	}
	/**
	 * Constructor heredado.
	 * @param cause causa
	 */
	public DaoException(Throwable cause) {
		super(cause);
	}
	/**
	 * Constructor heredado.
	 * @param message mensaje
	 * @param cause causa
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
