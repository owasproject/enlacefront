package mx.altec.enlace.dao;

import java.io.*;
import java.sql.*;
import javax.sql.*;

import java.math.*;
import java.util.*;
import java.text.*;
import javax.naming.*;

import mx.altec.enlace.utilerias.Global;


import java.security.*;


/*
 * EI_Query.java
 *
 * Created on 12 de mayo de 2005, 05:54 PM
 */

/**
 *
 * @author Administrador
 */
public class EI_Query implements Serializable
  {

    private int         numColumnas;
    private Hashtable   resultado;

    Hashtable  result;
	Vector lista;
    Hashtable  params   =   new Hashtable();

    String dbName=Global.DATASOURCE_ORACLE;

    public EI_Query()
	 {
        resultado      = null;
        numColumnas    = 0;
     }

//*************************************************************************************************
//*************************************************************************************************

    public  Hashtable  ejecutaQuery(String query)
    {
        System.out.println("Entre a ejecutaquery");
		System.out.println("Query: "+query);
        System.out.println("dbName "+dbName);

        resultado = new Hashtable();

        Connection  connection=null;
        Statement   st = null;
        ResultSet   rs = null;

        int i=0;
        int contador=0;

        try
        {
            InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( dbName );
			connection = ds.getConnection ();

            try
             {
                st = connection.createStatement();
                rs = st.executeQuery(query);
             }
            catch(Exception e)
             {
               e.printStackTrace();
             }

            ResultSetMetaData  datosquery=rs.getMetaData();
            numColumnas = datosquery.getColumnCount();
            while (rs.next())
            {
                String cols[]= new String[numColumnas];
                for(i=1;i<=numColumnas;i++)
                {
                    try
                    {
                        if(rs.getString(i)!=null)
                            cols[i-1] = rs.getString(i).trim();
                        else cols[i-1] = "";
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                        System.out.println("Error de BD: "+e.toString());
                    }
                }

                resultado.put(Integer.toString(contador),(String [])cols);
                contador++;
            }
        }
        catch (SQLException e)
        {
           System.out.println("No se pudo ejecutar query....");
           e.printStackTrace();
           return null;
        }
        catch (Exception e)
        {
           System.out.println("Error de BD: "+e.toString());
           e.printStackTrace();
           return null;
        }
        finally
        {
          try
		  {
				connection.close();
				rs.close();
		  } catch(SQLException ex)
		   {}
			  catch(Exception e)
			  {}
			}
        return resultado;
    }

//*************************************************************************************************
//*************************************************************************************************
	public  Vector ejecutaQueryCampo(String query)
    {
        System.out.println("Entre a ejecutaquery");
		System.out.println("Query: "+query);
        System.out.println("dbName "+dbName);

        lista = new Vector();

        Connection  connection=null;
        Statement   st = null;
        ResultSet   rs = null;

        int i=0;
        int contador=0;

        try
        {
            InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( dbName );
			connection = ds.getConnection ();

            try
             {
                st = connection.createStatement();
                rs = st.executeQuery(query);
             }
            catch(Exception e)
             {
               e.printStackTrace();
             }

            ResultSetMetaData  datosquery=rs.getMetaData();
            while (rs.next())
            {
				try
				{
					if(rs.getString(1)!=null)
					   lista.add(rs.getString(1).trim());
					else
						lista.add("");
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Error de BD: "+e.toString());
				}
            }
        }
        catch (SQLException e)
        {
           System.out.println("No se pudo ejecutar query....");
           e.printStackTrace();
           return null;
        }
        catch (Exception e)
        {
           System.out.println("Error de BD: "+e.toString());
           e.printStackTrace();
           return null;
        }
        finally
        {
          try
		  {
				connection.close();
				rs.close();
		  } catch(SQLException ex)
		   {}
			  catch(Exception e)
			  {}
			}
        return lista;
    }

//*************************************************************************************************
//*************************************************************************************************

    public  String[] ejecutaQueryNum(String query)
    {

        Connection connection;
        Statement st = null;
        ResultSet rs = null;
        String cols[]=null;

        int i=0;

        try
        {
            InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( dbName );
			connection = ds.getConnection ();

            st = connection.createStatement();
            rs = st.executeQuery(query);
            ResultSetMetaData  datosquery=rs.getMetaData();
            numColumnas = datosquery.getColumnCount();

            cols= new String[numColumnas];
            rs.next();
            for(i=1;i<=numColumnas;i++)
            {
                cols[i-1] = rs.getString(i).trim();
            }
        }
        catch (Exception e)
        {
            System.out.println("Error de BD: ");
            e.printStackTrace();
            return null;
        }
        return cols;
    }

//*************************************************************************************************
//*************************************************************************************************

    public  int  actualizaQuery(String query)
    {
        System.out.println("Entre actualizaquery ");
        System.out.println("dbname "+dbName);

        int retorno=0;

        Connection connection;
        Statement st = null;

        try
        {
            InitialContext ic = new InitialContext ();
			DataSource ds = ( DataSource ) ic.lookup ( dbName );
			connection = ds.getConnection ();

            st = connection.createStatement();
            retorno = st.executeUpdate(query);
        }
        catch (Exception e)
        {
            System.out.println("Error de BD: "+e.toString());
            e.printStackTrace();
            return 0;
        }
        return retorno;
    }

//*************************************************************************************************
//*************************************************************************************************

    public Hashtable getResultado()
    {
        return this.resultado;
    }

//*************************************************************************************************
//*************************************************************************************************

    public int getNumeroColumnas()
    {
        return numColumnas;
    }


    public static void cierraResultSet(ResultSet object) {
    	try {
    		if (object != null) {
    			object.close();
    		}
		} catch (SQLException e) {
			System.out.println("Error al Cerrar el RS EI_Query.cierraConexion: " + e.toString());
			e.printStackTrace();
		}
    }

    public static void cierraConnection(Connection object) {
    	try {
    		if (object != null) {
    			object.close();
    		}
		} catch (SQLException e) {
			System.out.println("Error al Cerrar el CN EI_Query.cierraConexion: " + e.toString());
			e.printStackTrace();
		}
    }

    public static void cierraStatement(Statement object) {
    	try {
    		if (object != null) {
    			object.close();
    		}
		} catch (SQLException e) {
			System.out.println("Error al Cerrar el ST EI_Query.cierraConexion: " + e.toString());
			e.printStackTrace();
		}
    }

    /**
     * Metodo Comun para manejar el Cierre de Conexion, Statement y ResultSet
     * @author Edgar Adri�n R�driguez S�nchez
     * @since 09/12/2013
     * @param conn de tipo Connection
     * @param sDup de tipo Statement
     * @param rs de tipo ResultSet
     */
    public static void cierraConexion(ResultSet rs, Statement st, Connection conn){
    	cierraResultSet(rs);
    	cierraStatement(st);
    	cierraConnection(conn);
    }

}