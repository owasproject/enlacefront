package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.ConsEdoCtaDetalleHistBean;
import mx.altec.enlace.beans.ConsultaEdoCtaHistBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class ConsultaEdoCtaHistDAO extends GenericDAO {
	
	/**
	 * SELECT_DETALLE
	 */
	private static final String SELECT_DETALLE = new StringBuilder().append("SELECT A.ID_EDO_CTA_HIST, A.ESTATUS,") 
												 .append("A.NUM_CUENTA, A.PERIODO, DECODE(NVL(A.TIPO_EDO_CTA,'000'),'001',TO_CHAR(NVL(A.NUM_SEC,0)),'003',' ',' ') NUM_SEC, ")
												 .append("TO_CHAR(A.FCH_SOL,'DD/MM/YYYY') FCH_SOL, A.ID_USUARIO, ")
												 .append(" NVL(RELAC.N_DESCRIPCION,'Sin descripci&oacute;n') N_DESCRIPCION ")
												 .append("FROM  EWEB_EDO_CTA_HIST A ")
												 .append("LEFT OUTER JOIN NUCL_RELAC_CTAS RELAC ON RELAC.NUM_CUENTA = A.NUM_CUENTA ")
												 .append(" AND RELAC.NUM_CUENTA2 = '%s' ")
												 .append("		  %s %n").toString();

	/**
	 * SELECT_DET_PAGINADO
	 */

	private static final String SELECT_DET_PAGINADO = new StringBuilder(
	                            "SELECT REG.*, TOTAL FROM %n").append(
                                "(SELECT ROWNUM AS INDICE, EDOCTAHIST.* %n").append(
                                "    FROM ( %n").append(
                                "         SELECT A.ID_EDO_CTA_HIST, A.ESTATUS,  %n").append(
                                "		     	 A.NUM_CUENTA, A.PERIODO, DECODE(NVL(A.TIPO_EDO_CTA,'000'),'001',TO_CHAR(NVL(A.NUM_SEC,0)),'003',' ',' ') NUM_SEC, %n").append(      		
                                "		  		 TO_CHAR(A.FCH_SOL,'DD/MM/YYYY') FCH_SOL, A.ID_USUARIO, %n").append(
                                "				 NVL(RELAC.N_DESCRIPCION,'Sin descripci&oacute;n') N_DESCRIPCION %n").append(		
                                "           FROM  EWEB_EDO_CTA_HIST A %n").append(
                                "			LEFT OUTER JOIN NUCL_RELAC_CTAS RELAC ON RELAC.NUM_CUENTA = A.NUM_CUENTA %n").append(
                                "			AND RELAC.NUM_CUENTA2 = '%s' %n").append(	
                                "		  %s %n").append(			
                                "         ) EDOCTAHIST %n").append(
                                "    WHERE ROWNUM <= %s %n").append(
                                ") REG, (SELECT COUNT(*) AS TOTAL FROM EWEB_EDO_CTA_HIST A %s) TODOS %n").append( 
                                "WHERE INDICE > %s").toString();
	
	/**
	 * ESTATUS
	 */
	private static final String ESTATUS = "ESTATUS";
	

	/**
	 * Se encarga de armar los criterios de busqueda
	 * @param ConsultaEdoCtaHistBean : ConsultaEdoCtaHistBean
	 * @return String : where
	 */
	private String armaCriterioDet(ConsultaEdoCtaHistBean ConsultaEdoCtaHistBean){
		
		StringBuilder where = new StringBuilder();
		where.append("WHERE A.NUM_CUENTA2 = '").append(ConsultaEdoCtaHistBean.getContrato()).append("'");
		
		if(ConsultaEdoCtaHistBean.getFolio()!= null && !"".equals(ConsultaEdoCtaHistBean.getFolio())){
			where.append(" AND A.ID_EDO_CTA_HIST = ").append(ConsultaEdoCtaHistBean.getFolio());
		}
		
		if(ConsultaEdoCtaHistBean.getEstatus() != null && !"".equals(ConsultaEdoCtaHistBean.getEstatus())){
			where.append(" AND A.ESTATUS = '").append(ConsultaEdoCtaHistBean.getEstatus()).append("'");
		}
		
		if(ConsultaEdoCtaHistBean.getUsuarioSolicitante() != null && !"".equals(ConsultaEdoCtaHistBean.getUsuarioSolicitante())){
			where.append(" AND A.ID_USUARIO = '").append(ConsultaEdoCtaHistBean.getUsuarioSolicitante()).append("'");
		}
		
		if(ConsultaEdoCtaHistBean.getFechaInicio() != null && !"".equals(ConsultaEdoCtaHistBean.getFechaInicio())){
			where.append(" AND A.FCH_SOL >= TO_DATE('").append(ConsultaEdoCtaHistBean.getFechaInicio()).append("','DD/MM/YYYY')")
			.append(" AND A.FCH_SOL <= TO_DATE('").append(ConsultaEdoCtaHistBean.getFechaFin()).append("','DD/MM/YYYY')");
		}
		
		where.append(" ORDER BY ID_EDO_CTA_HIST DESC");
		
		return where.toString();
		
	}
	
	/**
	 * Realiza la consulta del detalle de edo cta
	 * @param ConsultaEdoCtaHistBean : ConsultaEdoCtaHistBean
	 * @return detalle : detalle
	 */
	public List<ConsEdoCtaDetalleHistBean> consultaEdoCtaHistDetalle(ConsultaEdoCtaHistBean ConsultaEdoCtaHistBean){
		
		List<ConsEdoCtaDetalleHistBean> detalle = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		String where;
		
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			if(conn != null){
			
				detalle  = new ArrayList<ConsEdoCtaDetalleHistBean>();
				where = armaCriterioDet(ConsultaEdoCtaHistBean);
      		    query = String.format(SELECT_DET_PAGINADO,ConsultaEdoCtaHistBean.getContrato(),
		    		                       where, ConsultaEdoCtaHistBean.getRegFin(),
		    		                       where, ConsultaEdoCtaHistBean.getRegIni());		
//				
		        EIGlobal.mensajePorTrace("QUERY PAGINADO A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
						
				 while (result.next ()) {
					 ConsEdoCtaDetalleHistBean resultado = new ConsEdoCtaDetalleHistBean();
					 
					 resultado.setIdEdoCtaHist(result.getString("ID_EDO_CTA_HIST"));
					 resultado.setEstatus("D".equals(result.getString(ESTATUS))?"Disponible":
		  				                  "P".equals(result.getString(ESTATUS))?"Pendiente":
		  				                  "I".equals(result.getString(ESTATUS))?"Inexistente":"");
					 resultado.setNumCuenta(result.getString("NUM_CUENTA"));
                     resultado.setPeriodo(result.getString("PERIODO"));
                     resultado.setDescCta(result.getString("N_DESCRIPCION"));
                     resultado.setNumSec(result.getString("NUM_SEC"));
                     resultado.setFchSol(result.getString("FCH_SOL"));
                     resultado.setIdUsuario(result.getString("ID_USUARIO"));
                     resultado.setTotalRegistros(result.getInt("TOTAL"));

					 detalle.add(resultado);
					                      			 
				 }
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		
		return detalle;
		
	}
	
	/**
	 * Realiza la consulta del detalle de edo cta
	 * @param ConsultaEdoCtaHistBean : ConsultaEdoCtaHistBean
	 * @return detalle : detalle
	 */
	public List<ConsEdoCtaDetalleHistBean> consultaEdoCtaDetalleHistTotal(ConsultaEdoCtaHistBean ConsultaEdoCtaHistBean){
		
		List<ConsEdoCtaDetalleHistBean> detalle = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		String where;
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			if(conn != null){
			
				detalle  = new ArrayList<ConsEdoCtaDetalleHistBean>();
				where = armaCriterioDet(ConsultaEdoCtaHistBean);
				query = String.format(SELECT_DETALLE,
			    		              ConsultaEdoCtaHistBean.getContrato(), where);
	    
				EIGlobal.mensajePorTrace("QUERY TOTALIZADOR A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
						
				 while (result.next ()) {
					 ConsEdoCtaDetalleHistBean resultado = new ConsEdoCtaDetalleHistBean();
					 resultado.setIdEdoCtaHist(result.getString("ID_EDO_CTA_HIST"));
					 resultado.setEstatus("D".equals(result.getString(ESTATUS))?"Disponible":
		  				                  "P".equals(result.getString(ESTATUS))?"Pendiente":
		  				                  "I".equals(result.getString(ESTATUS))?"Inexistente":"");
					 resultado.setNumCuenta(result.getString("NUM_CUENTA"));
					 resultado.setPeriodo(result.getString("PERIODO"));
					 resultado.setDescCta(result.getString("N_DESCRIPCION"));
					 resultado.setNumSec(result.getString("NUM_SEC"));
					 resultado.setFchSol(result.getString("FCH_SOL"));
					 resultado.setIdUsuario(result.getString("ID_USUARIO"));
					 					 
					 detalle.add(resultado);
				 }
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return detalle;
	}
}
