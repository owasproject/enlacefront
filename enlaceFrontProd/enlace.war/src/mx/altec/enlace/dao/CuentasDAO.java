/** 
 *   Isban Mexico
 *   Clase: CuentasDAO
 *   Descripcion: Clase de acceso a datos de cuentas
 *
 *   Control de Cambios:
 *   1.0 Agosto del 2015 FSW Everis
 */
package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.altec.enlace.beans.CuentasBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/** 
 * Clase cuentas DAO.
 */
public class CuentasDAO {
	
	/**
	 * Constante QUERY_MSG para imprimir la cadena de texto en log del resultado de los queries
	 */
	public static final String QUERY_MSG = "Query <";
	
	/**
	 * 
	 * @param contrato cadena que contiene el contrato para filtrar consulta
	 * @param tipoProd cadena que contiene el tipo de producto de la cuenta
	 * @return Lista con cuentas relacionadas a un contrato.
	 * @throws SQLException Apunta que ocurrio una excepcion de tipo SQL.
	 * @throws Exception Apunta que ocurrio una excepcion.
	 */
	public List<CuentasBean> consultaCuentasRel(String contrato, String tipoProd) throws SQLException,Exception{	
		

		Connection con = null;
		ResultSet rs = null;
		CuentasBean ctaBean= null;
		ArrayList<CuentasBean> listaCuentas = null;
		PreparedStatement psDao =  null;
		
		EIGlobal.mensajePorTrace("Entra CuentasDAO.ConsultaCuentas ->" + contrato, EIGlobal.NivelLog.INFO);
		
		String query = "SELECT trim(a.num_cuenta), a.tipo_relac_ctas, nvl(a.n_tipo_cuenta,'1'), nvl(a.n_per_jur,'F'), nvl(a.n_product,''), a.n_descripcion, '0000000', 'SIN TITULAR',"+
					    "' ' num_cuenta_rel, nvl(decode((SELECT cve_prod_enl FROM tct_prod_enlace WHERE CVE_PROD_ORG = a.n_product), null, (decode(a.n_tipo_cuenta,'7',(decode(a.n_per_jur,'F',2,3))," + 
					    "(SELECT cve_prod_enl FROM tct_prod_enlace2 WHERE CVE_PROD_ORG=a.n_tipo_cuenta AND CVE_PROD_ORG<>'7'))), e.cve_prod_enl),'0') as tipo_producto FROM nucl_relac_ctas a, tct_prod_enlace e," + 
					    "(select CVE_PROD_ORG, cve_prod_enl from tct_prod_enlace2 where CVE_PROD_ORG<>'7') f ";
		String filtro =  "WHERE  a.num_cuenta2 = '" + contrato + "' and a.n_product = e.CVE_PROD_ORG(+) and a.n_tipo_cuenta=f.CVE_PROD_ORG(+)";
		
		
		query  = query + filtro;
		
		EIGlobal.mensajePorTrace(QUERY_MSG + query + ">", EIGlobal.NivelLog.DEBUG);
		listaCuentas = new ArrayList<CuentasBean>();
		
		try {
			con = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			psDao = con.prepareStatement(query);			
			//psDao.setString(1, contrato);
			rs = psDao.executeQuery();
			
			EIGlobal.mensajePorTrace("Antes de Lista ->" + contrato, EIGlobal.NivelLog.INFO);
			while(rs.next()){
				
				ctaBean = new CuentasBean();
				ctaBean.setNumCuenta(rs.getString(1));
				ctaBean.setTipoRel(rs.getString(2));
				ctaBean.setTipoCuenta(rs.getString(3));
				ctaBean.setTipoPersona(rs.getString(4));
				ctaBean.setCveProducto(rs.getString(5));
				ctaBean.setDescripcion(rs.getString(6));
				ctaBean.setClave(rs.getString(7));
				ctaBean.setTitular(rs.getString(8));
				ctaBean.setNumCuentaRel(rs.getString(9));
				ctaBean.setTipoProd(rs.getString(10));				
				listaCuentas.add(ctaBean);				
			}
			EIGlobal.mensajePorTrace("despues de Lista ->" + listaCuentas.size(), EIGlobal.NivelLog.INFO);
		}finally{
			if( rs != null){
                rs.close();
			}
			if(psDao != null){
                psDao.close();
			}
			if(con != null){
                con.close();
			}
		}	
		EIGlobal.mensajePorTrace("Sale CuentasDAO.ConsultaCuentas ->" + listaCuentas.size(), EIGlobal.NivelLog.INFO);
		return listaCuentas;
	}	
	
	/**
	 * Metodo para consultar el id del cliente asociado al contrato
	 * @param contrato para filtrar consulta y obtener el id de cliente asociado al contrato
	 * @return cadena con id del cliente asociado al contrato
	 */
    public static String consultarIdUsuarioContrato(String contrato) {
        EIGlobal.mensajePorTrace("Entra CuentasDAO.getIdUsuarioCta ->" + contrato, EIGlobal.NivelLog.INFO);
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement psDao = null;
        String idUsrCta = "";
        StringBuilder query = new StringBuilder("SELECT num_persona as numPersona FROM tct_cuentas_tele WHERE num_cuenta='").append(contrato).append("'");
        EIGlobal.mensajePorTrace(QUERY_MSG + query + ">", EIGlobal.NivelLog.DEBUG);
        try {
            con = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
            psDao = con.prepareStatement(query.toString());
            rs = psDao.executeQuery();
            if (rs.next()) {
                idUsrCta = rs.getString("numPersona");
            }
            EIGlobal.mensajePorTrace("Resultado de result-> " + idUsrCta, EIGlobal.NivelLog.INFO);
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace("Error a SQLException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar ResultSet: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (psDao != null) {
                try {
                    psDao.close();
                    psDao = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar PreparedStatement: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar Connection: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
        }
        EIGlobal.mensajePorTrace("Sale CuentasDAO.getIdUsuarioCta ->", EIGlobal.NivelLog.INFO);
        return idUsrCta;
    } 
    
    
    /**
     * Metodo para consultar identificador de convenio en base al numero de cuenta
     * 
     * @param cuenta para filtrar consulta
     * @return idConv identificador de convenio
     */
    public static String consultarCuentaConv(String cuenta) {
        EIGlobal.mensajePorTrace("Entra CuentasDAO ->" + cuenta, EIGlobal.NivelLog.INFO);
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement psDao = null;
        String ctaConv = "";
        
        StringBuilder query = new StringBuilder("SELECT NUM_CTA FROM ENL_MX_MAE_CONV_DRO WHERE (NUM_CTA= '")
                .append(cuenta).append("' OR COD_CVE_CONV = '").append(cuenta).append("') AND FLG_STAT = 'S'");
        EIGlobal.mensajePorTrace(QUERY_MSG + query + ">", EIGlobal.NivelLog.DEBUG);
        try {
            con = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE3);
            psDao = con.prepareStatement(query.toString());
            rs = psDao.executeQuery();
            if (rs.next()) {
                ctaConv = rs.getString("NUM_CTA").trim();
            }
            EIGlobal.mensajePorTrace("Resultado de id convenio|" + ctaConv + "|", EIGlobal.NivelLog.INFO);
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace("Error a SQLException: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar ResultSet: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (psDao != null) {
                try {
                    psDao.close();
                    psDao = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar PreparedStatement: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error a cerrar Connection: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
        }
        EIGlobal.mensajePorTrace("Sale CuentasDAO ->", EIGlobal.NivelLog.INFO);
        return ctaConv;
    }

    /**
     * Consulta los valores restantes para completar el mapeo de campos de MonitorPlus
     * @param contrato valor de cuenta o contrato para filtrar la consulta
     * @return Mapa con los valores obtenidos
     */
    public static Map<Integer, String>  consultarDatosMP(String contrato) {
        EIGlobal.mensajePorTrace("Entra consultarDatosMP" + contrato, EIGlobal.NivelLog.INFO);
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement psDao = null;
        Map<Integer, String> map = new HashMap<Integer, String>();
        StringBuilder query = new StringBuilder("SELECT CTA_COMIS_GRALES as ctaEje, CVE_SUPERUSUARIO as superUs FROM tct_cuentas_tele WHERE num_cuenta='").append(contrato).append("'");
        EIGlobal.mensajePorTrace(QUERY_MSG + query + ">", EIGlobal.NivelLog.DEBUG);
        try {
            con = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
            psDao = con.prepareStatement(query.toString());
            rs = psDao.executeQuery();
            if (rs.next()) {
                map.put(1, rs.getString("ctaEje"));
                map.put(2, rs.getString("superUs"));
                
            }
            EIGlobal.mensajePorTrace("Resultado de result cta EJE |" + rs.getString("ctaEje") 
            		+ "|" + "| Super Usuario |" + rs.getString("superUs"), EIGlobal.NivelLog.INFO);
        } catch (SQLException e) {
            EIGlobal.mensajePorTrace("Error a cerrar ResultSet: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    rs = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error  ResultSet: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (psDao != null) {
                try {
                    psDao.close();
                    psDao = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error PreparedStatement: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
            if (con != null) {
                try {
                    con.close();
                    con = null;
                } catch (SQLException e) {
                    EIGlobal.mensajePorTrace("Error Connection: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
                }
            }
        }
        EIGlobal.mensajePorTrace("Sale CuentasDAO.consultarDatosMP", EIGlobal.NivelLog.INFO);
        return map;
    } 

}
