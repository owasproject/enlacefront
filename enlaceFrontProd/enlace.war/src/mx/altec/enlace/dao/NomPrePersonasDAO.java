package mx.altec.enlace.dao;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1A;
import mx.altec.enlace.beans.NomPreLM1C;
import mx.altec.enlace.beans.NomPrePE68;
import static mx.altec.enlace.utilerias.NomPreUtil.*;
import static mx.altec.enlace.beans.NomPreRemesa.LNG_NO_REMESA;

public class NomPrePersonasDAO extends AdmonUsuariosMQDAO {
		
	
	//TRANS: PE68
	public NomPrePE68 consultarPersona(String numeroPersona) {	
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(numeroPersona, 8)); 										
		
		logInfo("PE68 - Consulta de Datos Basico: " + sb.toString());
	
		return consulta(NomPrePE68.HEADER, sb.toString(), NomPrePE68.getFactoryInstance());		
	}
	
}
