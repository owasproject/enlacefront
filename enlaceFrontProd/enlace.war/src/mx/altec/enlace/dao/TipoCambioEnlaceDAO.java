package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.Ps7;
import mx.altec.enlace.servicios.ServicioTux;
import  mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.IEnlace;

import javax.sql.*;
import java.sql.*;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class TipoCambioEnlaceDAO extends BaseServlet{

	/**
	 * STFQRO - OBTIENE EL TIPO DE CAMBIO EN VENTANILLA (GPA0)
	 * @param tipoOperacion <STRONG>CPA</STRONG> = COMPRA, <STRONG>VTA</STRONG> = VENTA
	 * @param divisa
	 * @return
	 */
	public String verificaCuenta(String cuenta, String tipoOperacion, String divisa, String divisaCruce, String instrumento){

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.verificaCuenta",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();
		String resMQ = "";

		MQQueueSession mqTux = null;
		
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("EN", 2) ) //CANAL
			.append( Ps7.rellenar("", 3 ) )  //SEGMENTO
			.append( Ps7.rellenar(cuenta, 12, '0', 'I') )  //CUENTA
			.append( Ps7.rellenar(instrumento, 5)  )  //INSTRUMENTO
			.append( Ps7.rellenar(tipoOperacion, 3)) //TIPO DE OPERACION CPA O VTA
			.append( Ps7.rellenar("0981", 4)  )  //CENTRO DE COSTOS
			.append( Ps7.rellenar("M", 1)  ) //INDICADOR DE VALORACION
			.append( Ps7.rellenar("H", 2)  ) //TRAMO EN EL QUE SE REQUIERE LA OPERACION
			.append( Ps7.rellenar(divisa, 3)) //DIVISAS
			.append( Ps7.rellenar(divisaCruce, 3)  );//DIVISA DE VAL. CRUZADA

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GPA0", "/GPAO", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("******ps7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST); 
			resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("***SALIENDO DE TipoCambioEnlaceDAO.verificaCuenta",
				EIGlobal.NivelLog.INFO);

		return resMQ;
	}

	/**
	 * STFQRO - OBTIENE EL TIPO DE CAMBIO EN VENTANILLA (GPA0)
	 * @param tipoOperacion <STRONG>CPA</STRONG> = COMPRA, <STRONG>VTA</STRONG> = VENTA
	 * @param divisa
	 * @return
	 */
	public String getCambioBase(String cuenta, String tipoOperacion, String divisa, String divisaCruce, String instrumento){
		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getCambioBase(String cuenta, String tipoOperacion, String divisa)***",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();
		MQQueueSession mqTux = null;

		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("EN", 2) ) //CANAL
			.append( Ps7.rellenar("", 3 ) )  //SEGMENTO
			.append( Ps7.rellenar(cuenta, 12, '0', 'I') )  //CUENTA
			.append( Ps7.rellenar(instrumento, 5)  )  //INSTRUMENTO
			.append( Ps7.rellenar(tipoOperacion, 3)) //TIPO DE OPERACION CPA O VTA
			.append( Ps7.rellenar("0981", 4)  )  //CENTRO DE COSTOS
			.append( Ps7.rellenar("M", 1)  ) //INDICADOR DE VALORACION
			.append( Ps7.rellenar("H", 2)  ) //TRAMO EN EL QUE SE REQUIERE LA OPERACION
			.append( Ps7.rellenar(divisa, 3)) //DIVISAS
			.append( Ps7.rellenar(divisaCruce, 3)  );//DIVISA DE VAL. CRUZADA

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GPA0", "/GPAO", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("******ps7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.substring(53,63).trim();//BASE

			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::getCambioBase:: Tipo de Cambio->" +resultado, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("***SALIENDO DE TipoCambioEnlaceDAO.getCambioBase(String tipoOperacion, String divisa)***",
				EIGlobal.NivelLog.INFO);

		return resultado;
	}

	/**
	 * STFQRO - OBTIENE EL TIPO DE CAMBIO EN VENTANILLA (GPA0)
	 * @param tipoOperacion <STRONG>CPA</STRONG> = COMPRA, <STRONG>VTA</STRONG> = VENTA
	 * @param divisa
	 * @return
	 */
	public String getCambioVentanilla(String cuenta, String tipoOperacion, String divisa, String divisaCruce, String instrumento){
		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getCambioVentanilla(String cuenta, String tipoOperacion, String divisa)***",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();

		MQQueueSession mqTux = null;
		
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("EN", 2) ) //CANAL
			.append( Ps7.rellenar("", 3 ) )  //SEGMENTO
			.append( Ps7.rellenar(cuenta, 12, '0', 'I') )  //CUENTA
			.append( Ps7.rellenar(instrumento, 5)  )  //INSTRUMENTO
			.append( Ps7.rellenar(tipoOperacion, 3)) //TIPO DE OPERACION CPA O VTA
			.append( Ps7.rellenar("0981", 4)  )  //CENTRO DE COSTOS
			.append( Ps7.rellenar("M", 1)  ) //INDICADOR DE VALORACION
			.append( Ps7.rellenar("H", 2)  ) //TRAMO EN EL QUE SE REQUIERE LA OPERACION
			.append( Ps7.rellenar(divisa, 3)) //DIVISAS
			.append( Ps7.rellenar(divisaCruce, 3)  );//DIVISA DE VAL. CRUZADA

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GPA0", "/GPAO", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("******ps7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.substring(73,83).trim();//VENTANILLA

			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::getCambioVentanilla:: Tipo de Cambio->" +resultado, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("***SALIENDO DE TipoCambioEnlaceDAO.getCambioVentanilla(String tipoOperacion, String divisa)***",
				EIGlobal.NivelLog.INFO);

		return resultado;
	}


	/**
	 * STFQRO - OBTIENE EL TIPO DE CAMBIO SUGERIDO
	 * @param tipoOperacion <STRONG>CPA</STRONG> = COMPRA, <STRONG>VTA</STRONG> = VENTA
	 * @return
	 */
	public String getCambioSugerido(String tipoOperacion, String instrumento){
		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getCambioSugerido(String tipoOperacion)***",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();

		MQQueueSession mqTux = null;
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("EN", 2) ) //CANAL
			.append( Ps7.rellenar("", 3 ) )  //SEGMENTO
			.append( Ps7.rellenar("", 12) )  //CUENTA
			.append( Ps7.rellenar(instrumento, 5)  )  //INSTRUMENTO
			.append( Ps7.rellenar(tipoOperacion, 3)) //TIPO DE OPERACION CPA O VTA
			.append( Ps7.rellenar("0981", 4)  )  //CENTRO DE COSTOS
			.append( Ps7.rellenar("M", 1)  ) //INDICADOR DE VALORACION
			.append( Ps7.rellenar("H", 2)  ) //TRAMO EN EL QUE SE REQUIERE LA OPERACION
			.append( Ps7.rellenar("USD", 3)) //DIVISAS
			.append( Ps7.rellenar("", 3)  );//DIVISA DE VAL. CRUZADA

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GPA0", "/GPAO", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("******ps7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.substring(63,73).trim();//SUGERIDO

			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::getCambioSugerido:: Tipo de Cambio->" +resultado, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("***SALIENDO DE TipoCambioEnlaceDAO.getCambioSugerido(String tipoOperacion)***",
				EIGlobal.NivelLog.INFO);

		return resultado;
	}

	/**
	 * STFQRO - OBTIENE EL TIPO DE CAMBIO SUGERIDO
	 * @param tipoOperacion <STRONG>CPA</STRONG> = COMPRA, <STRONG>VTA</STRONG> = VENTA
	 * @return
	 */
	public String getCambioSugerido(String cuenta, String tipoOperacion, String divisa, String divisaCruce, String instrumento){
		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getCambioSugerido(String tipoOperacion)***",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();

		MQQueueSession mqTux = null;
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("EN", 2) ) //CANAL
			.append( Ps7.rellenar("", 3 ) )  //SEGMENTO
			.append( Ps7.rellenar(cuenta , 12, '0', 'I'))  //CUENTA
			.append( Ps7.rellenar(instrumento, 5)  )  //INSTRUMENTO
			.append( Ps7.rellenar(tipoOperacion, 3)) //TIPO DE OPERACION CPA O VTA
			.append( Ps7.rellenar("0981", 4)  )  //CENTRO DE COSTOS
			.append( Ps7.rellenar("M", 1)  ) //INDICADOR DE VALORACION
			.append( Ps7.rellenar("H", 2)  ) //TRAMO EN EL QUE SE REQUIERE LA OPERACION
			.append( Ps7.rellenar(divisa, 3)) //DIVISAS
			.append( Ps7.rellenar(divisaCruce, 3)  );//DIVISA DE VAL. CRUZADA

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GPA0", "/GPAO", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("******ps7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.substring(63,73).trim();//SUGERIDO

			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::getCambioSugerido:: Tipo de Cambio->" +resultado, EIGlobal.NivelLog.INFO);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace("***SALIENDO DE TipoCambioEnlaceDAO.getCambioSugerido(String tipoOperacion)***",
				EIGlobal.NivelLog.INFO);

		return resultado;
	}


	/**
	 * Transaccion <STRONG>GP07</STRONG> - TRANSFERENCIAS INTERNACIONALES
	 * @param cuentaCargo
	 * @param divisa
	 * @param montoDivisa
	 * @param tipoCambio
	 * @param equivalenteMN
	 * @param refClave
	 * @param NombreOrdenante
	 * @param apPatOrdenante
	 * @param apMatOrdenante
	 * @param PABenef
	 * @param NombreBenef
	 * @param apPatBenef
	 * @param apMatBenef
	 * @param RFCBenef
	 * @param cuentaBenef
	 * @param bancoCorresponsal
	 * @param detalleOp
	 * @param centroOperante
	 * @param CentroOrigen
	 * @param centroDestino
	 * @param ObsCargo
	 * @param ObsAbono
	 * @return
	 */
	public String getTransfInternacionales(
			String cuentaCargo,	    String divisa,
			String montoDivisa,	    String tipoCambio,
			String equivalenteMN,   String refClave,
			String NombreOrdenante,	String apPatOrdenante,
			String apMatOrdenante,  String PABenef,
			String NombreBenef,		String apPatBenef,
			String apMatBenef, 		String RFCBenef,
			String cuentaBenef,		String bancoCorresponsal,
			String detalleOp, 		String centroOperante,
			String CentroOrigen, 	String centroDestino,
			String ObsCargo, 		String ObsAbono){

		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getTransfInternacionales",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();
		
		MQQueueSession mqTux = null;
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("1", 1)  )  //Tipo de cuenta cargo
			.append( Ps7.rellenar(cuentaCargo, 12, '0', 'I') )  //Cuenta de cargo
			.append( Ps7.rellenar(divisa, 3)   )  //Divisa
			.append( Ps7.rellenar(montoDivisa, 17, '0', 'I')  )  //Monto Divisa
			.append( Ps7.rellenar(tipoCambio, 10, '0', 'I')  )  //Tipo de cambio
			.append( Ps7.rellenar(equivalenteMN, 17, '0','I')  )  //Equivalente en Moneda Nacional
			.append( Ps7.rellenar(refClave, 7, ' ', 'D')   )  //Referencia de la clave
			.append( Ps7.rellenar(NombreOrdenante, 40, ' ', 'D')  )  //Nombre del ordenante
			.append( Ps7.rellenar(apPatOrdenante, 20, ' ', 'D')  )  //Apellido paterno ordenante
			.append( Ps7.rellenar(apMatOrdenante, 20, ' ', 'D')  )  //Apellido materno ordenante
			.append( Ps7.rellenar(PABenef, 3)   )  //Pa�s del beneficiario
			.append( Ps7.rellenar(NombreBenef, 40, ' ', 'D')  )  //Nombre del beneficiario
			.append( Ps7.rellenar(apPatBenef, 20, ' ', 'D')  )  //Apellido paterno beneficiario
			.append( Ps7.rellenar(apMatBenef, 20, ' ', 'D')  )  //Apellido materno beneficiario
			.append( Ps7.rellenar(RFCBenef, 16, ' ', 'D')  )  //RFC del beneficiario
			.append( Ps7.rellenar(cuentaBenef, 20)  )  //Cuenta del beneficiario
			.append( Ps7.rellenar(bancoCorresponsal, 11, ' ', 'D')  )  //Banco corresponsal
			.append( Ps7.rellenar(detalleOp, 80)  )  //Detalle de la operaci�n
			.append( Ps7.rellenar(centroOperante, 4)   )  //Centro operante
			.append( Ps7.rellenar(CentroOrigen, 4)   )  //Centro origen
			.append( Ps7.rellenar(centroDestino, 4)   )  //Centro Destino
			.append( Ps7.rellenar(ObsCargo, 30)  )  //Observaci�n de Cargo
			.append( Ps7.rellenar(ObsAbono, 30)  )  //Observaci�n de Abono
			.append( Ps7.rellenar("EN", 2) ) //Identificaci�n de Canal
			.append( Ps7.rellenar(" ", 10)  )  //FOLIO CANCELACION
			.append( Ps7.rellenar(" ", 8) ); //Campo clave de usuario

			EIGlobal.mensajePorTrace( "TRANSACCION LENGTH: " + transaccion.length() , EIGlobal.NivelLog.INFO);

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GP07", "/GP07", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("***** PS7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ= mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.toString();

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return resultado;
	}

	/**
	 * Transaccion <STRONG>GP02</STRONG> - TRANSPASO ENTRE CUENTAS
	 * @param cuentaCargo
	 * @param divisa
	 * @param montoDivisa
	 * @param tipoCambio
	 * @param equivalenteMN
	 * @param equivalenteDLLS
	 * @param cuentaAbono
	 * @param refClave
	 * @param folioOPCancel
	 * @param centroOperante
	 * @param CentroOrigen
	 * @param centroDestino
	 * @param ObsCargo
	 * @param ObsAbono
	 * @return
	 */
	public String getTranspasoCuentas(
			String cuentaCargo,	    String divisa,
			String montoDivisa,	    String tipoCambio,
			String equivalenteMN,   String equivalenteDLLS,
			String cuentaAbono,     String refClave,
			String folioOPCancel,   String centroOperante,
			String CentroOrigen, 	String centroDestino,
			String ObsCargo, 		String ObsAbono){

		String resultado = "";

		EIGlobal.mensajePorTrace("***ENTRANDO A TipoCambioEnlaceDAO.getTranspasoCuentas",
				EIGlobal.NivelLog.INFO);

		StringBuffer transaccion = new StringBuffer();

		MQQueueSession mqTux = null;
		try {
			/* ARMANDO TRAMA COMPRA*/
			transaccion
			.append( Ps7.rellenar("1", 1)  )  //Tipo de cuenta cargo
			.append( Ps7.rellenar(cuentaCargo, 12, '0', 'I' ) )  //Cuenta de cargo
			.append( Ps7.rellenar(divisa, 3)   )  //Divisa
			.append( Ps7.rellenar(montoDivisa, 17, '0', 'I')  )  //Monto Divisa
			.append( Ps7.rellenar(tipoCambio, 10, '0', 'I')  )  //Tipo de cambio
			.append( Ps7.rellenar(equivalenteMN, 17, '0','I')  )  //Equivalente en Moneda Nacional
			.append( Ps7.rellenar(equivalenteDLLS, 17, '0','I')  )  //Equivalente en Moneda DLLS
			.append( Ps7.rellenar("1", 1)  )  //Tipo de cuenta abono
			.append( Ps7.rellenar(cuentaAbono, 12, '0', 'I')  )  //cuenta abono
			.append( Ps7.rellenar(refClave, 7, ' ', 'D')   )  //Referencia de la clave
			.append( Ps7.rellenar(folioOPCancel, 10)   )  //Folio Operacion cancelacion
			.append( Ps7.rellenar(centroOperante, 4)   )  //Centro operante
			.append( Ps7.rellenar(CentroOrigen, 4)   )  //Centro origen
			.append( Ps7.rellenar(centroDestino, 4)   )  //Centro Destino
			.append( Ps7.rellenar(ObsCargo, 30)  )  //Observaci�n de Cargo
			.append( Ps7.rellenar(ObsAbono, 30)  )  //Observaci�n de Abono
			.append( Ps7.rellenar("EN", 2) ) //Identificaci�n de Canal
			.append( Ps7.rellenar(" ", 8) ); //Campo clave de usuario

			EIGlobal.mensajePorTrace( "TRANSACCION LENGTH: " + transaccion.length() , EIGlobal.NivelLog.INFO);

			String ps7 = Ps7.armaCabeceraPS7("MQ", "GP02", "/GP02", transaccion.length(), "00");
			EIGlobal.mensajePorTrace("***** PS7+transaccion:" +ps7+transaccion.toString(), EIGlobal.NivelLog.INFO);

			mqTux = new MQQueueSession(NP_JNDI_CONECTION_FACTORY, NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
			String resMQ = mqTux.enviaRecibeMensaje(ps7 + transaccion.toString());

			resultado = resMQ.toString();

		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally{//CSA
			if(mqTux!=null){
				try{
					mqTux.close();
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ERROR AL CERRAR CONEXION: "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}
			}
		}

		return resultado;
	}


	/**
	 * Query para insertar en la tabla  EWEB_BITA_INTER
	 * @param contrato
	 * @param tpo_orden
	 * @param num_orden
	 * @param ref
	 * @param edocargo
	 * @param edoabono
	 * @param tpo_per
	 * @param cve_pais_rec
	 * @param cvdiv_abon
	 * @param cvdiv_cargo
	 * @param instr
	 * @param usr
	 * @param cve_aba
	 * @param cve_esp
	 * @param cta_cargo
	 * @param cta_abono
	 * @param folio_liq_cargo
	 * @param folio_liq_abono
	 * @param rtcargo
	 * @param rtabono
	 * @param imp_pesos
	 * @param imp_dolares
	 * @param imp_divisa
	 * @param tpo_campes
	 * @param tpo_camdol
	 * @param fch_env
	 * @param fch_aplic
	 * @param nom_bcorec
	 * @param suc_rec
	 * @param cd_rec
	 * @param concepto
	 * @param beneficiario
	 * @param refe_meca
	 * @return
	 */
	public void insertEWEBBITA(
			String contrato, 		String tpo_orden,
			String num_orden, 		Integer ref,
			String edocargo, 		String edoabono,
			String tpo_per, 		String cve_pais_rec,
			String cvdiv_abon, 		String cvdiv_cargo,
			String instr, 			String usr,
			String cve_aba,			String cve_esp,
			String cta_cargo,		String cta_abono,
			String folio_liq_cargo,	String folio_liq_abono,
			int rtcargo,			int rtabono,
			String imp_pesos,		String imp_dolares,
			String imp_divisa,		String tpo_campes,
			String tpo_camdol,		String fch_env,
			String fch_aplic,		String nom_bcorec,
			String suc_rec,			String cd_rec,
			String concepto,		String beneficiario,
			String refe_meca ){
		EIGlobal.mensajePorTrace("*** ENTRANDO a TipoCambioEnlaceDAO.insertEWEBBITA ***",
				EIGlobal.NivelLog.INFO);

		StringBuilder query = new StringBuilder();
		Connection connbd = null;
		PreparedStatement ps = null;
		int nOrden = 0;
		int fliqAbon = 0;
		int fliqCarg = 0;

		try {
			BaseServlet bs = new BaseServlet();
			connbd = bs.createiASConn(Global.DATASOURCE_ORACLE3);

			if(connbd == null) {
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::insertEWEBBITA():: Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
			}else{

				try{
					nOrden = Integer.parseInt(num_orden);
					fliqAbon = Integer.parseInt(folio_liq_abono);
					fliqCarg = Integer.parseInt(folio_liq_cargo);
				}catch(Exception e){}


			query.append(" INSERT INTO EWEB_BITA_INTER(contrato, tpo_orden, num_orden, ref, ")
			.append(" edocargo, edoabono, tpo_per, cve_pais_rec, cvdiv_abon, cvdiv_cargo, instr, ")
			.append(" usr, cve_aba, cve_esp, cta_cargo, cta_abono, ")
			.append(" folio_liq_cargo, folio_liq_abono, rtcargo, rtabono, ")
			.append(" imp_pesos, imp_dolares, imp_divisa, tpo_campes, tpo_camdol, ")
			.append(" fch_env, fch_aplic, nom_bcorec, suc_rec, cd_rec, ")
			.append(" concepto, beneficiario, refe_meca) ")
			.append(" VALUES('" + contrato.trim() + "','" + tpo_orden.trim() + "'," + nOrden + "," + ref + ",'" + edocargo.trim() + "','" + edoabono.trim() + "','")
			.append(tpo_per.trim() + "','" + cve_pais_rec.trim() + "','" + cvdiv_abon.trim() + "','" + cvdiv_cargo.trim() + "','"+ instr.trim() + "','" + usr.trim() + "','" + cve_aba.trim() + "','" + cve_esp.trim() + "','")
			.append(cta_cargo.trim() + "','" + cta_abono.trim() + "'," + fliqCarg + "," + fliqAbon + "," + rtcargo + "," + rtabono + ",")
			.append(imp_pesos.trim() + "," + imp_dolares.trim() + "," + imp_divisa.trim() + "," + tpo_campes.trim() + "," + tpo_camdol.trim() + ",to_date('" + fch_env.trim() + "','ddMMyy'),")
			.append("to_date('" + fch_aplic.trim() + "','ddMMyy'),'" + nom_bcorec.trim() + "','" + suc_rec.trim() + "','" + cd_rec.trim() + "','" + concepto.trim() + "','" + beneficiario.trim() + "','" + refe_meca.trim() + "')");

			ps = connbd.prepareStatement(query.toString());

			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::insertEWEBBITA():: Query["+ query.toString() + "]", EIGlobal.NivelLog.INFO);

			int result = ps.executeUpdate();

			if(result==1){
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::insertEWEBBITA():: SE INSERTARON REGISTROS, result: " + result,EIGlobal.NivelLog.INFO);
			}
			else {
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::insertEWEBBITA():: NO SE INSERTARON REGISTROS, result: " + result,EIGlobal.NivelLog.INFO);
			}
			}
		} catch (Exception e) {
			EIGlobal.mensajePorTrace( "ERROR::TipoCambioEnlaceDAO.insertEWEBBITA" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
		}finally{
			try {
				ps.close();
				//rs.close();
				connbd.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace( "ERROR::TipoCambioEnlaceDAO.insertEWEBBITA" + e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);
				e.printStackTrace();
			}

		}
	}

	/**
	 * Metodo encargado de realizar la operaci�n por Transfer
	 *
	 * @param referencia		Referencia propia del Medio de Entrega
	 * @param cuentaCargo		N�mero de cuenta del ordenante
	 * @param cuentaAbono		N�mero de cuenta receptora
	 * @param beneficiario		Nombre del receptor
	 * @param importeDA			Importe de cargo
	 * @param banco				Nombre del banco receptor
	 * @param sucursal			Sucursal del banco receptor
	 * @param pais				Clave de pa�s del banco receptor
	 * @param ciudad			Ciudad del banco receptor
	 * @param claveABA			Clave ABA
	 * @param concepto			Concepto de la operaci�n
	 * @param rfcBeneficiario	RFC Beneficiario
	 * @return resultados		Resultados de la operaci�n que se envio a transfer
	 */
	public String[] ejecutaServicioTransfer(Integer referencia, String cuentaCargo, String titular, String cuentaAbono,
			String beneficiario, String importeDA, String banco, String sucursal, String pais, String ciudad,
			String claveABA, String concepto, String rfcBeneficiario, String usuarioCap, String usuarioAut) {
		Hashtable hs = null;
		ServicioTux tuxGlobal = new ServicioTux();
		String tramaEntrada = "";
		String tramaSalida = "";
		StringTokenizer token = null;
		String[] resultados = new String[3];
		int i = 0;
		StringTokenizer tokenConcepto = new StringTokenizer(concepto,"+");
  		try{
  			tramaEntrada = "BME|0981|EWEB|" + referencia.toString() + "|" + usuarioCap + "|" + usuarioAut
  				  + "|104|BCACOMER|C|BANME|" + cuentaCargo.trim() + "||USD|" + titular.trim()
  				  + "||" + cuentaAbono.trim() + "|||USD|" + beneficiario.trim() + "|" + importeDA + "|"+ importeDA
  				  + "|1.00|" + banco + "|" + sucursal + "|" + pais + "|" + ciudad + "|" + claveABA + "|" + tokenConcepto.nextToken().trim() + "|||";
  				  //+ "|" + rfcBeneficiario + "|";
  			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::ejecutaServicioTransfer:: Trama Entrada->"+ tramaEntrada, EIGlobal.NivelLog.INFO);
  			hs = tuxGlobal.tranTransfe(tramaEntrada, "TRAN_TRANSFE");

			if(hs != null) {
				tramaSalida = (String) hs.get("BUFFER");
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::ejecutaServicioTransfer:: Trama Salida->"+ tramaSalida, EIGlobal.NivelLog.INFO);
				token = new StringTokenizer(tramaSalida, "|");
				while(token.hasMoreTokens()){
					resultados[i] = token.nextToken();
					i++;
				}
			}else{
				resultados[0] = "";
				resultados[1] = "";
				resultados[2] = "";
			}

			if(!resultados[0].endsWith("0000"))
				resultados[2] = descErrorTransfer(resultados[0]);

  		}catch(Exception e){
  			EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::ejecutaServicioTransfer:: Error al crear la conexi�n "
  					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
			resultados[0] = "";
			resultados[1] = "";
			resultados[2] = "";
  		}
  		return resultados;
	}

	/**
	 * Metodo encargado de regresar el banco corresponsal
	 *
	 * @param contrato 			Numero de Contrato
	 * @param  cuenta 		    Numero de Cuenta
	 * @return datosCtaInter	Datos de la cuenta internacional
	 */
	public String[] obtenDatosCtaInter(String contrato, String cuenta) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String[] datosCtaInter = new String[5];

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE3 );
			sDup = conn.createStatement();
			query = "Select a.CVE_PAIS as cvePais, "
				  + "a.CVE_ABA as cveAba, "
				  + "a.CVE_SWIFT as cveSwift, "
				  + "a.DESC_BANCO as descBanco, "
				  + "a.DESC_CIUDAD as descCiudad "
				  + "From EWEB_CTAS_INTER a "
				  + "Where a.NO_CTA ='" + cuenta + "' "
				  + "And a.NO_CONTRATO = '" + contrato + "'";

			EIGlobal.mensajePorTrace ("OperacionesManc::obtenBcoCorresp:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				datosCtaInter[0] = rs.getString("cvePais")!=null?rs.getString("cvePais"):"";
				datosCtaInter[1] = rs.getString("cveAba")!=null?rs.getString("cveAba"):"";
				datosCtaInter[2] = rs.getString("cveSwift")!=null?rs.getString("cveSwift"):"";
				datosCtaInter[3] = rs.getString("descBanco")!=null?rs.getString("descBanco"):"";
				datosCtaInter[4] = rs.getString("descCiudad")!=null?rs.getString("descCiudad"):"";
			}

			EIGlobal.mensajePorTrace ("OperacionesManc::obtenBcoCorresp:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("OperacionesManc::obtenBcoCorresp:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OperacionesManc::obtenBcoCorresp:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return datosCtaInter;
	}

	/**
	 * Metodo encargado de actualizar el los datos del banco de la cuenta internacional
	 *
	 * @param numContrato		Numero de Contrato
	 * @param numCuenta			Numero de Cuenta
	 * @param cvePais			Clave de Pais
	 * @param cveAbaSwift		Clave ABA � SWIFT
	 * @param desBanco			Descripci�n de Banco
	 * @param desCiudad			Descripci�n de Ciudad
	 * @return 					TRUE - si la transacci�n es exitosa
	 * 							FALSE - si la transacci�n es NO exitosa
	 */
	public boolean actBancoCtaInter(String numContrato, String numCuenta, String cvePais, String cveAbaSwift,
			                        String desBanco, String desCiudad){
		Connection conn = null;
		Statement sDup = null;
		int i = 0;
		String query = null;
		String campoActualizar = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE3);
			sDup = conn.createStatement();

			if(cvePais.equals("001"))
				campoActualizar = "CVE_ABA = '" + cveAbaSwift + "', ";
			else
				campoActualizar = "CVE_SWIFT = '" + cveAbaSwift + "', ";

			query = "UPDATE EWEB_CTAS_INTER SET "
				  + campoActualizar
				  + "DESC_BANCO = '" + desBanco + "', "
				  + "DESC_CIUDAD = '" + desCiudad + "' "
				  + "WHERE NO_CONTRATO='" + numContrato + "' "
				  + "AND NO_CTA='" + numCuenta + "'";

			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::actBancoCtaInter:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			i = sDup.executeUpdate(query);

			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::actBancoCtaInter:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
			if(i>0)
				return true;
			else
				return false;

		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::actBancoCtaInter: -> Error->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}finally{
			try{
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::actBancoCtaInter: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return false;
	}

	/**
	 * Metodo encargado de actualizar el estatus de una operaci�n mancomunada
	 *
	 * @param contrato 			Numero de Contrato
	 * @param folioReg 		    Folio de regsitro
	 * @param estatus 		    Estatus a actualizar
	 * @return 					TRUE - si la transacci�n es exitosa
	 * 							FALSE - si la transacci�n es NO exitosa
	 */
	public boolean actEstatusOperManc(String contrato, String folioReg, String estatus) {
		Connection conn = null;
		Statement sDup = null;
		int i = 0;
		String query = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE);
			sDup = conn.createStatement();
			query = "UPDATE tct_manc_oper SET "
				  + "estatus ='"+ estatus + "' "
				  + "WHERE folio_registro='" + folioReg + "' "
				  + "AND contrato='" + contrato + "'";

			EIGlobal.mensajePorTrace ("OperacionesManc::actEstatusOperManc:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			i = sDup.executeUpdate(query);

			EIGlobal.mensajePorTrace ("OperacionesManc::actEstatusOperManc:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
			if(i>0)
				return true;
			else
				return false;

		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("OperacionesManc::actEstatusOperManc:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}finally{
			try{
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("OperacionesManc::actEstatusOperManc:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return false;
	}


	/**
	 * Metodo encargado de regresar la descripci�n del error en cambios al
	 * ejecutar las transacciones GP02 y GP07
	 *
	 * @param codigo		Codigo de Error generado por las transaciones GP02 y GP07
	 * @return				Descripci�n correcta del error
	 */
	public String descErrorTransfer(String codigo){
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String msgError = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE);
			sDup = conn.createStatement();
			query = "Select a.MSG_ERROR as msgError "
				  + "From COMU_ERROR a "
				  + "Where a.COD_ERROR ='" + codigo + "'";

			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::descErrorTransfer:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				msgError = rs.getString("msgError")!=null?rs.getString("msgError"):"";
			}

			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::descErrorTransfer:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("TipoCambioEnlaceDAO::descErrorTransfer:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("TipoCambioEnlaceDAO::descErrorTransfer:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return msgError;
	}

}