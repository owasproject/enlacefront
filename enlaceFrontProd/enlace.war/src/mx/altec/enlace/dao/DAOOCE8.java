/**
* ISBAN M�xico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* DAOOCE8.java
*
* Control de versiones:
*
* Version Date/Hour        By            Company    Description
* ------- ---------------- ------------- --------   ----------------------------
* 1.0     09-11-2011 19:00 Z225016       BSD        Creacion
*
*/
package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import mx.altec.enlace.beans.BeanOCE8;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

public class DAOOCE8 {

	private static final String CODTRAN_OK = "112345";
	private static final String CODERR_SINDATOS = "OC0999";
	private static final String MENSAJE_SINDATOS = "SIN DATOS DE RESPUESTA";
	private static final String TRANSACCION ="OCE8";
	private static final String FORMATO_SALIDA ="@DCOCME801 P";

	public BeanOCE8 consultaOCE8(BeanOCE8 bean)throws Exception {

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaOCE8() Inicio", EIGlobal.NivelLog.INFO);

		String respuesta = null;
		StringBuffer tramaBuf = null;
		String trama = null;

		try {
			tramaBuf = new StringBuffer();

			tramaBuf.append(rellenar(bean.getContrato(), 20))
			.append(rellenar(bean.getRepaginado(),1))
			.append(rellenar(bean.getPerRepaginado(),8))
			.append(rellenar(bean.getCveUsuario(),8));

			trama = creaTrama(NP_MQ_TERMINAL, ADMUSR_MQ_USUARIO, TRANSACCION, tramaBuf.toString());

			respuesta = new MQQueueSession(
					NP_JNDI_CONECTION_FACTORY,
					NP_JNDI_QUEUE_RESPONSE,
					NP_JNDI_QUEUE_REQUEST)
					.enviaRecibeMensaje(trama);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaOCE8() Error DAOOCE8: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}

		if (respuesta != null && respuesta.length() > 0) {
			parseTrama(respuesta, bean);
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::consultaOCE8() Fin", EIGlobal.NivelLog.INFO);
		return bean;
	}

	public void parseTrama(String trama,BeanOCE8 bean)throws Exception{

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() Inicio", EIGlobal.NivelLog.INFO);

		String []arrayTrama;
		String tramaDatos;

		if(trama != null){
			trama.trim();
			if(trama.equals("")){
				bean.setCodError(CODERR_SINDATOS);
				bean.setMsgError(MENSAJE_SINDATOS);
			}
			else {
				arrayTrama = trama.split("[@]");
				if(arrayTrama != null && arrayTrama.length >= 3){
					bean.setCodError(arrayTrama[2].substring(2,9));
					bean.setMsgError(arrayTrama[2].substring(10,arrayTrama[2].length()-1));

					EIGlobal.mensajePorTrace(
							this.getClass().getName() + "::parseTrama() "+
							"codError [" + bean.getCodError() + "] " +
							"msgError [" + bean.getMsgError() + "]"
							,EIGlobal.NivelLog.INFO
					);

					if(arrayTrama[1].indexOf(CODTRAN_OK) == 0){

						String arrayDatos [] = trama.split(FORMATO_SALIDA);

						/*	En ciclo si se desean obtener todos los usuarios
							relacionados al contrato, por el momento solamente
							se desea obtener el registro del usuario que esta
							ingresando en este momento
						*/
						//for(int i = 1; i < arrayDatos.length; i++){
							//tramaDatos = arrayDatos[i];
							tramaDatos = arrayDatos[1];
							bean.setCodCliente(tramaDatos.substring(0,8).trim());
							bean.setDesCliente(tramaDatos.substring(8,48).trim());
							bean.setSAM(tramaDatos.substring(48,51).trim());
							bean.setFchModificacion(tramaDatos.substring(51,61).trim());
							bean.setEstBloqueo(tramaDatos.substring(61,62).trim());								
							bean.setFchBloqueo(tramaDatos.substring(62,72).trim());

							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getCodCliente      [" + bean.getCodCliente() + "]",EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getDesCliente      [" + bean.getDesCliente() + "]",EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getSAM             [" + bean.getSAM() + "]",EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getFchModificacion [" + bean.getFchModificacion() + "]",EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getEstBloqueo      [" + bean.getEstBloqueo() + "]",EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() getFchBloqueo      [" + bean.getFchBloqueo() + "]",EIGlobal.NivelLog.INFO);
						//}
					}
				}else{
					EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() Trama no procesada [" + trama + "]", EIGlobal.NivelLog.INFO);
				}
			}
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::parseTrama() Fin", EIGlobal.NivelLog.INFO);
	}

	public String creaTrama(
			String terminal, String usuario, String transaccion, String trama
	)
	{
		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Inicio", EIGlobal.NivelLog.INFO);
		StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(terminal, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(usuario, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		EIGlobal.mensajePorTrace(this.getClass().getName() + "::creaTrama() Fin", EIGlobal.NivelLog.INFO);
		return tramaBuffer.toString();
	}


}