/**
 * Isban Mexico
 *   Clase: DescargaEstadoCuentaDAO.java
 *   Descripcion: 
 *
 *   Control de Cambios:
 *   1.0 13/06/2013 Stefanini - Creacion
 */
package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.DY03Bean;
import mx.altec.enlace.beans.EstadoCuentaBean;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import org.apache.commons.lang.StringUtils;

/**
 * The Class DescargaEstadoCuentaDAO.
 */
public class DescargaEstadoCuentaDAO extends GenericDAO {
	/** Constante **/
	private static final String DECXML201="DECXML201";
	/** Constante SQL Brinco **/
	private static final String SQL_INSERTA_BRINCO=
						"Insert Into stprod.eweb_edocta_segu t " +
						"(t.llave_descarga,t.num_cuenta,t.formato,t.periodo,t.pais,t.folio_uuid,t.tipo_edo_cta) "+
						"Values ('%s','%s','%s','%s','MX','%s','%s')";
	/** Constante SQL Brinco PDF **/
	private static final String SQL_INSERTA_BRINCO_PDF=
						"Insert Into stprod.eweb_edocta_segu t "+
						"(t.llave_descarga,t.cod_clte,t.num_cuenta,t.formato,t.periodo,t.tipo_edo_cta,t.pais,t.Folio_Ondemand) "+
						"Values ('%s','%s','%s','%s','%s','%s','%s',%s)";
	/** The qry ctas. */
	private static final String QRY_CTAS = 
	"SELECT * FROM (" +
		"select subquery1.num_cuenta, ctas.n_descripcion " + /* Consulta cuentas del periodo */
		"from nucl_relac_ctas ctas, (" +
			"select det.num_cuenta, ctrl.num_cuenta2 " +
			"from eweb_edo_cta_det det, eweb_edo_cta_ctrl ctrl " +
			"where det.id_edo_cta_ctrl = ctrl.id_edo_cta_ctrl " +
			"and det.tipo_frmt = '%s' " + //1 PDF/2 XML
			"and ctrl.estatus = 'P' " + //Procesados
			"and det.disp_line = 'S' " + //No rechazados
			"and ctrl.num_cuenta2 = '%s' " + // Contrato
			"UNION " +
			"select hist.num_cuenta, hist.num_cuenta2 " + /* Consulta cuentas en historico */
			"from eweb_edo_cta_hist hist " +
			"where hist.num_cuenta2 = '%s' " + // Contrato
			"and hist.formato = '%s' " + //1 PDF/2 XML
		") subquery1 " +
		"where ctas.num_cuenta = subquery1.num_cuenta " +
		"and ctas.num_cuenta2 = '%s' %s " + // Contrato - Filtro
		"order by subquery1.num_cuenta "+
	") WHERE rownum <= 50";
	/** Constante Para Log **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: DescargaEstadoCuentaDAO ::: ";
	
	
	/**
	 * Consulta estatus estado cuenta.
	 *
	 * @param estadoCuenta the estado cuenta
	 * @return the d y03 bean
	 * @throws BusinessException the business exception
	 */
	public DY03Bean consultaEstatusEstadoCuenta(EstadoCuentaBean estadoCuenta) throws BusinessException {
		EIGlobal.mensajePorTrace(String.format("%s Ejecuta DAO DY03",LOG_TAG), NivelLog.INFO);	
		DY03DAO dy03dao = new DY03DAO();
		DY03Bean dy03Bean = new DY03Bean();
		dy03Bean.setCodigoCliente(estadoCuenta.getCodigoCliente());
		dy03Bean.setCuenta(estadoCuenta.getCuenta());
		dy03Bean.setFormato(estadoCuenta.getFormato());
		dy03Bean.setFolio(StringUtils.EMPTY);
		dy03Bean.setPeriodo(StringUtils.EMPTY);
		dy03Bean = dy03dao.ejecutarDY03(dy03Bean);
		EIGlobal.mensajePorTrace(String.format("%s Termina metodo consultaEstatusEstadoCuenta()",LOG_TAG), NivelLog.INFO);
		return dy03Bean;
	}

	/**
	 * Consulta que tienen solicitud de estado de cuenta pendiente.
	 *
	 * @param contrato Contrato al que estan asociadas las cuentas
	 * @param formato Formato de estado de cuenta 1 PDF - 2 XML
	 * @param cuenta the cuenta
	 * @param descripcion the descripcion
	 * @return Listado de cuentas
	 * @throws BusinessException the business exception
	 */
	public List<CuentasDescargaBean> consultaCuentas(String contrato, String formato, String cuenta, String descripcion) throws BusinessException {
		EIGlobal.mensajePorTrace(String.format("%s Consulta cuentas",LOG_TAG), NivelLog.INFO);
		
		List<CuentasDescargaBean> ctas = null;
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pst = null;		
		try {
			conn = GenericDAO.createiASConnStatic(Global.DATASOURCE_ORACLE3);
			StringBuilder filtro = new StringBuilder("");
			if(StringUtils.isNotEmpty(cuenta)){
				filtro.append("and LOWER(subquery1.num_cuenta) like '%");
				filtro.append(cuenta);
				filtro.append("%' ");
			}
			if(StringUtils.isNotEmpty(descripcion)){
				filtro.append("and LOWER(ctas.n_descripcion) like '%");
				filtro.append(descripcion.toLowerCase());
				filtro.append("%' ");
			}
			String qryFmt = String.format(QRY_CTAS,formato,contrato,contrato,formato,contrato,filtro.toString());
			EIGlobal.mensajePorTrace(String.format("%s Query:%s",LOG_TAG,qryFmt), NivelLog.INFO);			
			pst = conn.prepareStatement(qryFmt);			
			ctas = new ArrayList<CuentasDescargaBean>();
			rs = pst.executeQuery();
			while(rs.next()) {
				CuentasDescargaBean cd = new CuentasDescargaBean();
				cd.setCuenta(rs.getString(1).trim());
				cd.setDescripcion(rs.getString(2).trim());
				EIGlobal.mensajePorTrace(String.format("%s Cta: %s",LOG_TAG,cd.toString()), NivelLog.INFO);
				ctas.add(cd);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s Ocurrio un error en consultaCuentas : %s",LOG_TAG,e.toString()),NivelLog.DEBUG);
			throw new BusinessException("EDOCTADAO002 - Operacion no disponible.", e);
		} finally {
			try {
				rs.close();
				pst.close();
				conn.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s - Error al cerrar objeto : [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
			}
		}
		EIGlobal.mensajePorTrace(String.format("%s Termina ejecucion de metodo consultaCuentas()",LOG_TAG), NivelLog.INFO);
		return ctas;
	}
	
	/**
	 * Inserta Registro Descarga PDF
	 * @param cuentasDescargaBean : cuentasDescargaBean 
	 * @return int : Numero de Registros insertados
	 * @throws BusinessException : Manejo de la Excepcion.
	 */
	public int insertaRegistroDescargaPDF(CuentasDescargaBean cuentasDescargaBean) throws BusinessException {
		int registrosInsertados=0;
		Connection conn=null;
		PreparedStatement pst=null;
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			conn.setAutoCommit(false);
			String query=String.format(SQL_INSERTA_BRINCO_PDF,cuentasDescargaBean.getLlaveDescarga(),
															cuentasDescargaBean.getCodigoCliente(),
															cuentasDescargaBean.getCuenta(),
															cuentasDescargaBean.getFormato(),
															cuentasDescargaBean.getPeriodoSeleccionado(),
															cuentasDescargaBean.getTipoEdoCta(),
															cuentasDescargaBean.getPais(),
															cuentasDescargaBean.getFolioSeleccionado());
			pst = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace(String.format("%s insertaBrincoPDF %s",LOG_TAG,query), NivelLog.INFO);
			registrosInsertados=pst.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s Ocurrio un error en insertaRegistroDescargaPDF : %s",LOG_TAG,e.toString()),NivelLog.DEBUG);
			throw new BusinessException(DECXML201,e);
		} finally {
			try {
				pst.close();
				conn.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s - Error al cerrar objeto : [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
		}
		}		
		EIGlobal.mensajePorTrace(String.format("%s Termina ejecucion de metodo insertaRegistroDescargaPDF()",LOG_TAG), NivelLog.INFO);
		return registrosInsertados;
	}
	
	/**
	 * Inserta Registro Descarga XML
	 * @param cuentasDescargaBean : cuentasDescargaBean
	 * @return int : Numero de Registros insertados
	 * @throws BusinessException : Envio del error a traves del metodo.
	 */
	public int insertaRegistroDescarga(CuentasDescargaBean cuentasDescargaBean) throws BusinessException {
		EIGlobal.mensajePorTrace(String.format("%s Se ejecuta metodo insertaRegistroDescarga()",LOG_TAG),NivelLog.INFO);
		
		Connection conn = null;
		PreparedStatement pst = null;
		int registrosInsertados=0;
		String divisaCuenta = null;
		if (cuentasDescargaBean.getSerie().contains("USD")) {
			EIGlobal.mensajePorTrace(String.format("%s Se concatena %s a la cuenta %s correspondiente al periodo seleccionado", LOG_TAG, cuentasDescargaBean.getSerie(), cuentasDescargaBean.getCuentaBita()), NivelLog.DEBUG);
			divisaCuenta = "USD" + cuentasDescargaBean.getCuentaBita();
		}					
		
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			conn.setAutoCommit(false);
			String query=String.format(SQL_INSERTA_BRINCO,cuentasDescargaBean.getLlaveDescarga(),
														  divisaCuenta != null ? divisaCuenta : cuentasDescargaBean.getCuentaBita(),
														  cuentasDescargaBean.getFormato(),
														  cuentasDescargaBean.getPeriodoSeleccionado(), 
														  cuentasDescargaBean.getFolioSeleccionado(),
														  cuentasDescargaBean.getTipoXML());
			pst = conn.prepareStatement(query);
			EIGlobal.mensajePorTrace(String.format("%s insertaBrinco %s",LOG_TAG,query), NivelLog.INFO);
			registrosInsertados=pst.executeUpdate();
			conn.commit();
			
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(String.format("%s Ocurrio un error en insertaRegistroDescarga : %s",LOG_TAG,e.toString()),NivelLog.DEBUG);
			throw new BusinessException(DECXML201,e);
		} finally {
			try {
				pst.close();
				conn.close();				
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(String.format("%s - Error al cerrar objeto : [%s]",LOG_TAG,e.toString()),NivelLog.DEBUG);
			}
		}
		EIGlobal.mensajePorTrace(String.format("%s Termina ejecucion de metodo insertaRegistroDescarga()",LOG_TAG), NivelLog.INFO); 		
		return registrosInsertados;
	}
	
	/**
	 * Obtiene el nombre corto del mes
	 * @param mesPeriodo : mesPeriodo
	 * @return String : Nombre corto del Mes.
	 */
	public String obtenerNombreCortoMes(int mesPeriodo){	
		switch(mesPeriodo){
			case 1: return "ENE";
			case 2: return "FEB";
			case 3: return "MAR";
			case 4: return "ABR";
			case 5: return "MAY";
			case 6: return "JUN";
			case 7: return "JUL";
			case 8: return "AGO";
			case 9: return "SEP";
			case 10: return "OCT";
			case 11: return "NOV";
			case 12: return "DIC";
			default: return String.valueOf(mesPeriodo);
		}
	}
	
	/**
	 * Formatea el numero de cuenta.
	 * @param numeroCuenta : numeroCuenta
	 * @return String : numeroCuenta formateado
	 */
	public String formatCuenta(String numeroCuenta) {
		EIGlobal.mensajePorTrace(String.format("%s >>>>> formatCuenta [%s] <<<<<",LOG_TAG,numeroCuenta), NivelLog.INFO);
		if(numeroCuenta.startsWith("00")) {
			EIGlobal.mensajePorTrace(String.format("%s >>>>> StartsWith 00 replaceFirst 00 with 0. <<<<<",LOG_TAG), NivelLog.DEBUG);
			return numeroCuenta.replaceFirst("00", "0");
		} else if(numeroCuenta.startsWith("0")) {
			EIGlobal.mensajePorTrace(String.format("%s >>>>> StartsWith 0 replaceFirst 0 with ''. <<<<<",LOG_TAG), NivelLog.DEBUG);
			return numeroCuenta.replaceFirst("0", "");
		} else if (numeroCuenta.startsWith("B") && !numeroCuenta.startsWith("BME")) {
			EIGlobal.mensajePorTrace(String.format("%s >>>>> StartsWith B replaceFirst B with BME. <<<<<",LOG_TAG), NivelLog.DEBUG);
			return numeroCuenta.replaceFirst("B", "BME");
		}
		EIGlobal.mensajePorTrace(String.format("%s Sin sustitucion, Regresa [%s]",LOG_TAG,numeroCuenta), NivelLog.INFO);
		return numeroCuenta;
	}
	
}