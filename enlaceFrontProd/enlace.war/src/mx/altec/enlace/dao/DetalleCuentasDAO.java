package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.beans.TSE1Trans;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.jms.mq.conexion.MQQueueSessionException;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

public class DetalleCuentasDAO {
	private Connection conn=null;
	private PreparedStatement ps =null;
	private ResultSet rs=null;
	private static final String ORIGEN_DEFAULT = "A"; //A - Archivo.
	private static final 	String ESTADO_CTA_DEFAULT = "PA"; //PA - Pendiente Por Autorizar.

	private static final String ACTUALIZA_CUENTA = "UPDATE EWEB_DETALLE_EMP SET ESTADO_CTA = ? WHERE CONTRATO = ? AND FOLIO = ? AND CUENTA = ? AND ESTADO_CTA = 'I'";
	private static final String DETALLE_CUENTAS = "SELECT * FROM EWEB_DETALLE_EMP WHERE CONTRATO = ? AND FOLIO = ? AND ESTADO_CTA = 'I' AND TRUNC(SYSDATE - FECHA) <= ? ORDER BY CUENTA";
	//CONSULTA DBLINKS
	private static final String CONSULTA_ARCHIVOS = "SELECT ECE.CONTRATO, ECE.FOLIO, ETP.NUM_REGS, ECE.FECHA, ETP.ESTADO, COUNT(EDE.CUENTA) AS NRECHAZADOS FROM EWEB_CONTROL_EMP@lwebgfss ECE INNER JOIN EWEB_DETALLE_EMP@lwebgfss EDE ON TRIM(ECE.CONTRATO) = TRIM(EDE.CONTRATO) AND ECE.FOLIO = EDE.FOLIO INNER JOIN EWEB_TRANSAC_PROC ETP ON ETP.FOLIO = ECE.FOLIO AND ETP.CONTRATO = ECE.CONTRATO WHERE TRIM(ECE.CONTRATO) = ? AND ECE.ORIGEN = 'N' AND EDE.ESTADO_CTA = 'I' AND TRUNC(SYSDATE - EDE.FECHA) <= ? GROUP BY ECE.CONTRATO, ECE.FOLIO, ETP.NUM_REGS, ECE.FECHA, ETP.ESTADO HAVING COUNT(EDE.CUENTA) > 0 ORDER BY FOLIO";


	public boolean existeCuenta(String cuenta, String contrato) throws SQLException{

		boolean existe=false;

		try{
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeCuenta -> Conexion creada = " + conn, EIGlobal.NivelLog.INFO);
			
			
			StringBuffer sb = new StringBuffer("");
		 	sb.append("SELECT CONTRATO AS CONTRATO, CUENTA_ABONO AS CUENTA FROM EWEB_DET_CAT_NOM WHERE CONTRATO = '" + contrato + "' AND CUENTA_ABONO = '" + cuenta +  "' AND ESTATUS_CUENTA = 'A'");
		 	
		 	
			ps = conn.prepareStatement(sb.toString());
			//ps.setString(1, contrato);
			//ps.setString(2, cuenta);

			EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeCuenta -> EJECUTANDO" + sb.toString() + "\n", EIGlobal.NivelLog.DEBUG);		
			
			rs = ps.executeQuery();
			EIGlobal.mensajePorTrace("SELECT OK\n", EIGlobal.NivelLog.INFO);
			 if (rs.next()){
				 existe = true;
			 }
		}catch (SQLException sqlEx){
			throw sqlEx;
		}finally{
			try{
				if (rs!=null)
					rs.close();
			}catch (SQLException rsException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeCuenta ->" +
						" Error al cerrar el resultset " + rsException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (ps!=null)
					ps.close();
			}catch (SQLException psException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeCuenta ->" +
						" Error al cerrar el preparedStatement " + psException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (conn!=null)
				if (!conn.isClosed()){
					conn.close();
				}
			}catch (SQLException connException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeCuenta ->" +
						" Error al cerrar la conexi�n " + connException.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		return existe;
	}

	public long insertarContrato(String contrato)
			throws SQLException{
		try{

			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			/*if (existeContrato(contrato, conn))
				return;
			*/


			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarContrato -> Conexion creada = " + conn, EIGlobal.NivelLog.INFO);
			conn.setAutoCommit(false);
			StringBuffer insert1= new StringBuffer("");
			insert1.append("INSERT INTO EWEB_CONTROL_EMP (CONTRATO, SECUENCIA, FOLIO, FECHA, ORIGEN) VALUES (?,?,?,?,?)");
			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarContrato -> EJECUTANDO" + insert1.toString() + "\n", EIGlobal.NivelLog.INFO);
			ps=conn.prepareStatement(insert1.toString());
			ps.setString(1, contrato);

			Long folioSecuencia = getSecuencia( conn);
			EIGlobal.mensajePorTrace("FOLIO SECUENCIA =>" + folioSecuencia + "\n", EIGlobal.NivelLog.INFO);


			ps.setString(1, contrato);
			ps.setNull(2, Types.NULL);
			ps.setLong(3, folioSecuencia);
			ps.setDate(4, getFechaInsercion());
			ps.setString(5, ORIGEN_DEFAULT);
			ps.execute();

			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarContrato -> INSERT EWEB_CONTROL_EMP OK\n", EIGlobal.NivelLog.INFO);
			conn.commit();

			return folioSecuencia;
		}catch(SQLException sqlEx){
			if (conn!=null)
				conn.rollback();
			throw sqlEx;
		}finally{
			try{
				if (rs!=null)
					rs.close();
			}catch (SQLException rsException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar el resultset " + rsException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (ps!=null)
					ps.close();
			}catch (SQLException psException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar el preparedStatement " + psException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (conn!=null)
				if (!conn.isClosed()){
					conn.close();
				}
			}catch (SQLException connException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar la conexi�n " + connException.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}

	public void insertarCuenta(DetalleCuentasBean cuentaBeanInsertar, String contrato, long folioContrato)
			throws SQLException{
		try{
			String cuentaActual = cuentaBeanInsertar.getCuenta();
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta -> Conexion creada = " + conn, EIGlobal.NivelLog.INFO);
			conn.setAutoCommit(false);

			StringBuffer insert2= new StringBuffer("");

			insert2.append("INSERT INTO EWEB_DETALLE_EMP( CONTRATO, FOLIO, CUENTA, FECHA, ESTADO_CTA, OBSERVACIONES_CTA, ESTADO_PLAN, OBSERVACIONES_PLAN, ORIGEN, CODIGO_CLIENTE) ");
			insert2.append(" VALUES (?,?,?,?,?,?,?,?,?,?)");

			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta -> EJECUTANDO" + insert2.toString() + "\n", EIGlobal.NivelLog.INFO);
			ps=conn.prepareStatement(insert2.toString());
			ps.setString(1, contrato);



			ps.setLong(2, folioContrato);
			ps.setString(3, cuentaActual);
			ps.setDate(4, getFechaInsercion());
			ps.setString(5, ESTADO_CTA_DEFAULT);
			ps.setNull(6, Types.NULL);
			ps.setNull(7, Types.NULL);
			ps.setNull(8, Types.NULL);
			ps.setString(9, ORIGEN_DEFAULT);
			ps.setNull(10, Types.NULL);
			ps.execute();
			EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta -> INSERT EWEB_DETALLE_EMP OK\n", EIGlobal.NivelLog.INFO);
			conn.commit();
		}catch(SQLException sqlEx){
			if (conn!=null)
				conn.rollback();
			throw sqlEx;
		}finally{
			try{
				if (rs!=null)
					rs.close();
			}catch (SQLException rsException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar el resultset " + rsException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (ps!=null)
					ps.close();
			}catch (SQLException psException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar el preparedStatement " + psException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (conn!=null)
				if (!conn.isClosed()){
					conn.close();
				}
			}catch (SQLException connException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::insertarCuenta ->" +
						" Error al cerrar la conexi�n " + connException.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}

	public List<DetalleCuentasBean> obtenDetalleCuentas(Connection conn,
			String contrato, int folio, int diasVigencia) throws SQLException {

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {

			pst = conn.prepareStatement(DETALLE_CUENTAS);

			pst.setString(1, contrato);
			pst.setInt(2, folio);
			pst.setInt(3, diasVigencia);

			rs = pst.executeQuery();

			if (!rs.next()) {
				return null;
			}

			List<DetalleCuentasBean> resultado = new ArrayList<DetalleCuentasBean>();

			do {

				DetalleCuentasBean dto = new DetalleCuentasBean();

				dto.setContrato(rs.getString("CONTRATO"));
				dto.setCuentaLiberar(rs.getString("CUENTA"));
				dto.setFolio(rs.getInt("FOLIO"));
				dto.setFecha(rs.getDate("FECHA"));
				dto.setDescripcion("PRUEBA");
				dto.setEstadoCta(rs.getString("ESTADO_CTA"));
				dto.setObservacionesCta(rs.getString("OBSERVACIONES_CTA"));
				dto.setEstadoPlan(rs.getString("ESTADO_PLAN"));
				dto.setObservacionesPlan(rs.getString("OBSERVACIONES_PLAN"));

				resultado.add(dto);

			} while (rs.next());

			return resultado;

		} finally {

			close(rs);
			close(pst);
		}
	}

	public boolean actualizaEstadoCuenta(Connection conn, String contrato,
			int folio, String cuenta, String estado) throws SQLException {

		PreparedStatement pst = null;

		try {

			pst = conn.prepareStatement(ACTUALIZA_CUENTA);

			pst.setString(1, estado);
			pst.setString(2, contrato);
			pst.setInt(3, folio);
			pst.setString(4, cuenta);

			boolean afectacion = pst.executeUpdate() >= 1;

			if (!afectacion) {
				EIGlobal.mensajePorTrace(
						"No se actualizo informacion para [ contrato: "
								+ contrato + ", cuenta: " + cuenta
								+ ", folio: " + folio + "]",
						EIGlobal.NivelLog.ERROR);
			}

			return afectacion;

		} finally {

			close(pst);
		}
	}

	//Metodo consulta con DBLINKS
	public List<DetalleCuentasBean> obtenArchivos(Connection conn,
			String contrato, int vigencia) throws SQLException {

		PreparedStatement pst = null;
		ResultSet rs = null;

		try {

			pst = conn.prepareStatement(CONSULTA_ARCHIVOS);

			pst.setString(1, contrato);
			pst.setInt(2, vigencia);

			rs = pst.executeQuery();

			if (!rs.next()) {
				return null;
			}

			List<DetalleCuentasBean> resultado = new ArrayList<DetalleCuentasBean>();

			do {

				DetalleCuentasBean dto = new DetalleCuentasBean();

				dto.setFolio(rs.getInt("FOLIO"));
				dto.setFecha(rs.getDate("FECHA"));
				dto.setNumeroRegistros(rs.getInt("NUM_REGS"));
				dto.setNoRegistrados(rs.getInt("NRECHAZADOS"));
				dto.setEstado(rs.getString("ESTADO"));

				resultado.add(dto);

			} while (rs.next());

			return resultado;

		} finally {

			close(rs);
			close(pst);
		}
	}

	public TSE1Trans obtenerDatosCuenta(MQQueueSession mqSession,
			String numeroContrato) throws MQQueueSessionException {

		TSE1Trans resultado = null;

		StringBuffer trama = new StringBuffer()
				.append(rellenar(NP_MQ_TERMINAL, 4))
				.append(rellenar(ADMUSR_MQ_USUARIO, 8))
				.append(TSE1Trans.HEADER)
				.append(TSE1Trans.tramaConsulta(numeroContrato));

		String respuesta = mqSession.enviaRecibeMensaje(trama.toString());

		resultado = TSE1Trans.BUILDER.build(respuesta);

		if (resultado == null || resultado.getRazonSocial() == null) {
			EIGlobal.mensajePorTrace("DetalleCuentasDAO::obtenerDatosCuenta [" + numeroContrato + ", " + respuesta + "]", EIGlobal.NivelLog.INFO);
		}

		return resultado;
	}

	private static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(
						"DetalleCuentasDAO:: Error al cerrar el resultset"
								+ ex.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}

	private static void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException ex) {
				EIGlobal.mensajePorTrace(
						"DetalleCuentasDAO:: Error al cerrar el statement"
								+ ex.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}

	private Long getSecuencia( Connection conn) throws SQLException{
		Long folioRegresar=1L;
		PreparedStatement psFolio = null;
		ResultSet rsFolio = null;
		try{
			StringBuffer sb = new StringBuffer("");
			sb.append("select EWEB_SEQ_CAT_NOM_LOT.nextval as folio from DUAL \n");
			psFolio=conn.prepareStatement(sb.toString());

			rsFolio = psFolio.executeQuery();

			if (rsFolio.next()){
				folioRegresar = rsFolio.getLong("folio");
			}
		}catch(SQLException sqlEx){

			throw sqlEx;
		}
		finally{
			try{
				if (rsFolio!=null)
					rsFolio.close();
			}catch (SQLException rsException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::getSecuencia ->" +
						" Error al cerrar el resultset " + rsException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (psFolio!=null)
					psFolio.close();
			}catch (SQLException psException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::getSecuencia ->" +
						" Error al cerrar el preparedStatement " + psException.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}


		return folioRegresar;
	}



	private boolean existeContrato(String contrato, Connection conn) throws SQLException{
		Long nRegistros=1L;
		boolean existe = false;
		PreparedStatement psExiste = null;
		ResultSet rsExiste = null;
		try{
			StringBuffer sb = new StringBuffer("");
			sb.append(" select count(1) as existe from EWEB_CONTROL_EMP where contrato = ?  \n");
			psExiste=conn.prepareStatement(sb.toString());
			psExiste.setString(1, contrato);
			rsExiste = psExiste.executeQuery();

			if (rsExiste.next()){
				nRegistros = rsExiste.getLong("existe");
			}
			if (nRegistros>0)
					existe=true;
		}catch(SQLException sqlEx){

			throw sqlEx;
		}
		finally{
			try{
				if (rsExiste!=null)
					rsExiste.close();
			}catch (SQLException rsException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeContrato ->" +
						" Error al cerrar el resultset " + rsException.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			try{
				if (psExiste!=null)
					psExiste.close();
			}catch (SQLException psException){
				EIGlobal.mensajePorTrace("DetalleCuentasDAO::existeContrato ->" +
						" Error al cerrar el preparedStatement " + psException.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}


		return existe;
	}

	private Date getFechaInsercion(){
		java.util.Date hoy = new java.util.Date();
		long tiempoInsercion = hoy.getTime();
		return new Date(tiempoInsercion);
	}

}