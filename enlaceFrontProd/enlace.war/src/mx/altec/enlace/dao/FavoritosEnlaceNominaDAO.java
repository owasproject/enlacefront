package mx.altec.enlace.dao;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import mx.altec.enlace.beans.ArchivoNominaBean;
import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.utilerias.EIGlobal;


/**
 * DAO para dar de alta favoritos de nomina de enlace.
 * 
 */
public class FavoritosEnlaceNominaDAO extends FavoritosEnlaceDAO {

	/**
	 * Query para consultar los favoritos de los archivos de nomina.
	 */
	private static final String QUERY_CONSULTA_FAVORITOS_NOMINA = "Select * from EWEB_MX_MAE_FAV_NOM where NUM_CONTR = ? ";
	//private static final String QUERY_CONSULTA_FAVORITOS_NOMINA = "Select * from EWEB_MX_MAE_FAV_NOM where NUM_CONTR = ? AND COD_CLNTE = ? ";

	/**
	 * Query para obtener el numero de favoritos.
	 */
	private static final String QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA = "Select count(1) from EWEB_MX_MAE_FAV_NOM where NUM_CONTR = ? ";
//		private static final String QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA = "Select count(1) from EWEB_MX_MAE_FAV_NOM where NUM_CONTR = ? AND COD_CLNTE = ? ";
	/**
	 * Query para obtener el valormaximo de favoritos en la tabla.
	 */
	private static final String QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA_TOTAL = "Select max(ID_FAV_PK) from EWEB_MX_MAE_FAV_NOM";

	/**
	 * Query para la consulta de favoritos paginado.
	 */
	private static final String QUERY_CONSULTA_FAVORITOS_PAGINADO_NOMINA = "select * from (select ROWNUM rnum , a.* from ( "
			+ QUERY_CONSULTA_FAVORITOS_NOMINA
			+ " ) a ) where rnum >= ? and rnum <= ? order by rnum,NUM_CTA_CARGO ";
	/**
	 * Query para actualizar un favorito.
	 */
	private static final String QUERY_UPDATE_FAVORITO_NOMINA = "UPDATE EWEB_MX_MAE_FAV_NOM SET DSC_FAV = ? WHERE NUM_CONTR = ? AND NUM_CTA_CARGO = ? AND ID_FAV_PK = ";
	//private static final String QUERY_UPDATE_FAVORITO_NOMINA = "UPDATE EWEB_MX_MAE_FAV_NOM SET DSC_FAV = ? WHERE NUM_CONTR = ? AND COD_CLNTE = ?  AND NUM_CTA_CARGO = ? AND ID_FAV_PK = ";

	/**
	 * Query para eliminar un favorito.
	 */
	private static final String QUERY_DELETE_FAVORITO_CTRL_NOM = "DELETE EWEB_MX_MAE_FAV_NOM WHERE NUM_CONTR = ? AND NUM_CTA_CARGO = ? AND DSC_FAV = ? AND ID_FAV_PK = ";
	//private static final String QUERY_DELETE_FAVORITO_CTRL_NOM = "DELETE EWEB_MX_MAE_FAV_NOM WHERE NUM_CONTR = ? AND COD_CLNTE = ? AND NUM_CTA_CARGO = ? AND DSC_FAV = ? AND ID_FAV_PK = ";

	/**
	 * Query para eliminar todos los detalles de los favorito.
	 */
	private static final String QUERY_DELETE_FAVORITO_DETALLE_NOM = "DELETE EWEB_MX_AUX_FAV_NOM WHERE ID_FAV_PK = ";
	/**
	 * Query para consultar la informacion del detalle de los favoritos de una
	 * nomina.
	 */
	private static final String QUERY_CONSULTA_DETALLE_NOMINA = "select * from EWEB_MX_AUX_FAV_NOM where ID_FAV_PK = ";
	/**
	 * Query para insertar un favorito de ctrl.
	 */
	private static final String QUERY_INSERT_FAVORITO_CTRL = "insert into EWEB_MX_MAE_FAV_NOM values(?,?,?,?,?,?,?,?)";
	/**
	 * Query para insertar un favorito de detalle.
	 */
	private static final String QUERY_INSERT_DETALLE_FAVORITO = "insert into EWEB_MX_AUX_FAV_NOM values (?,?,?,?,?,?,?,?,?)";

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.dao.FavoritosEnlaceDAO#consultarFavoritos(java.lang.String
	 * , java.lang.String, java.lang.String)
	 */
	@Override
	public List<FavoritosEnlaceBean> consultarFavoritos(String contrato,
			String codCliente, String idModulo) throws SQLException {
		FavoritosEnlaceBean favorito = null;
		List<FavoritosEnlaceBean> listaFavoritos = new ArrayList<FavoritosEnlaceBean>();
		boolean resultado = false;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Realizando la consulta con los siguientes datos -->"
						+ contrato + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);
		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_FAVORITOS_NOMINA);
				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					favorito = new FavoritosEnlaceBean();

					favorito.setIdFav(" " + resultSet.getInt("ID_FAV_PK"));
					favorito.setDescripcion(resultSet.getString("DSC_FAV"));
					favorito.setContrato(resultSet.getString("NUM_CONTR"));
					favorito.setCodCliente(resultSet.getString("COD_CLNTE"));
					favorito.setCuentaCargo(resultSet
							.getString("NUM_CTA_CARGO"));
					favorito.setDescCuentaCargo(resultSet
							.getString("DSC_CTA_CARGO"));
					favorito.setNumeroRegistros(resultSet.getInt("NUM_REG"));
					favorito.setImporte(resultSet.getDouble("IMP_FAV"));

					listaFavoritos.add(favorito);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: EL numero de favoritos son: "
								+ listaFavoritos.size(), EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_FAVORITOS_NOMINA + ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}

		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return listaFavoritos;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.dao.FavoritosEnlaceDAO#obtenerNumeroFavoritos(java.lang
	 * .String, java.lang.String, java.lang.String)
	 */
	@Override
	public int obtenerNumeroFavoritos(String contrato, String codCliente,
			String idModulo) throws SQLException {
		int numeroFavoritos = 0;

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va a obtener el numero de favoritos para la siguiente consulta -->"
						+ contrato + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);

		if (inicializarConexionBD()) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA);

				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					numeroFavoritos = resultSet.getInt(1);
				}

				EIGlobal.mensajePorTrace("EL numero de favoritos son: "
						+ numeroFavoritos, EIGlobal.NivelLog.INFO);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA
								+ ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				throw e;
			} finally {
				cerrarConexionBD();
			}

		}
		return numeroFavoritos;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.dao.FavoritosEnlaceDAO#obtenerFavoritosPaginados(java
	 * .lang.String, java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public List<FavoritosEnlaceBean> obtenerFavoritosPaginados(String contrato,
			String codCliente, String idModulo, int inicioPagina, int finPagina)
			throws SQLException {
		boolean resultado = false;
		FavoritosEnlaceBean favorito = null;
		List<FavoritosEnlaceBean> listaFavoritos = new ArrayList<FavoritosEnlaceBean>();

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va a obtener el numero de favoritos para la siguiente consulta -->"
						+ contrato + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);

		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_FAVORITOS_PAGINADO_NOMINA);

				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);
				preparedStatement.setInt(2, inicioPagina);
				preparedStatement.setInt(3, finPagina);

				EIGlobal.mensajePorTrace("Ejecutando el query: "
						+ QUERY_CONSULTA_FAVORITOS_PAGINADO_NOMINA,
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					favorito = new FavoritosEnlaceBean();

					favorito.setIdFav(" " + resultSet.getInt("ID_FAV_PK"));
					favorito.setDescripcion(resultSet.getString("DSC_FAV"));
					favorito.setContrato(resultSet.getString("NUM_CONTR"));
					favorito.setCodCliente(resultSet.getString("COD_CLNTE"));
					favorito.setCuentaCargo(resultSet
							.getString("NUM_CTA_CARGO"));
					favorito.setDescCuentaCargo(resultSet
							.getString("DSC_CTA_CARGO"));
					favorito.setNumeroRegistros(resultSet.getInt("NUM_REG"));
					favorito.setImporte(resultSet.getDouble("IMP_FAV"));

					listaFavoritos.add(favorito);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: EL numero de favoritos son: "
								+ listaFavoritos.size(), EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_FAVORITOS_PAGINADO_NOMINA
								+ ": " + e.getMessage(), EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}
		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return listaFavoritos;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.dao.FavoritosEnlaceDAO#modificarFavorito(mx.altec.enlace
	 * .beans.FavoritosEnlaceBean)
	 */
	@Override
	public void modificarFavorito(FavoritosEnlaceBean favorito) {
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va modificar un favorito para "
						+ favorito.getContrato() + " - "
						/*+ favorito.getCodCliente()*/ + " con las cuenatas "
						+ favorito.getCuentaCargo() + " descripcion "
						+ favorito.getDescripcion() + " id "
						+ favorito.getIdFav()+" "+QUERY_UPDATE_FAVORITO_NOMINA, EIGlobal.NivelLog.INFO);
		boolean resultado = inicializarConexionBD();

		if (resultado) {

			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_UPDATE_FAVORITO_NOMINA
								+ favorito.getIdFav());

				conexion.setAutoCommit(false);

				preparedStatement.setString(1, favorito.getDescripcion());
				preparedStatement.setString(2, favorito.getContrato());
				//preparedStatement.setString(3, favorito.getCodCliente());
				preparedStatement.setString(3, favorito.getCuentaCargo());

				preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit",
						EIGlobal.NivelLog.INFO);
				conexion.commit();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la modificacion de un favorito: "
								+ QUERY_UPDATE_FAVORITO_NOMINA + e.getMessage(),
						EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace(
							"Erro al aplica el rolleback" + e1.getMessage(),
							EIGlobal.NivelLog.INFO);
				}
			} finally {
				cerrarConexionBD();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * mx.altec.enlace.dao.FavoritosEnlaceDAO#eliminarFavorito(mx.altec.enlace
	 * .beans.FavoritosEnlaceBean)
	 */
	@Override
	public void eliminarFavorito(FavoritosEnlaceBean favorito) {
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va eliminar un favorito para "
						+ favorito.getContrato() + " - "
						/*+ favorito.getCodCliente()*/ + " con las cuenatas "
						+ favorito.getCuentaCargo() + " con un importe de $"
						+ favorito.getImporte() + " con el id "
						+ favorito.getIdFav()+" "+QUERY_DELETE_FAVORITO_CTRL_NOM, EIGlobal.NivelLog.INFO);
		boolean resultado = inicializarConexionBD();

		if (resultado) {

			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_DELETE_FAVORITO_DETALLE_NOM
								+ favorito.getIdFav());

				conexion.setAutoCommit(false);
				preparedStatement.execute();

				preparedStatement = conexion
						.prepareStatement(QUERY_DELETE_FAVORITO_CTRL_NOM
								+ favorito.getIdFav());
				preparedStatement.setString(1, favorito.getContrato());
				//preparedStatement.setString(2, favorito.getCodCliente());
				preparedStatement.setString(2, favorito.getCuentaCargo());
				preparedStatement.setString(3, favorito.getDescripcion());

				preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit",
						EIGlobal.NivelLog.INFO);
				conexion.commit();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la eliminacion de un favorito "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace("Error al aplica el rolleback"
							+ e1.getMessage(), EIGlobal.NivelLog.INFO);
				}
			} finally {
				cerrarConexionBD();
			}

		}
	}

	/**
	 * Obtiene el detalle de un favorito de nomina
	 *
	 * @param idFav
	 *            Id del favorito a buscar.
	 * @return Lista del detalle de un favorito de nomina.
	 * @throws SQLException
	 *             Excepcion SQL.
	 */
	public LinkedHashMap<String, ArchivoNominaDetBean> obtenerDetalleNomina(
			String idFav) throws SQLException {
		boolean resultado = false;
		LinkedHashMap<String, ArchivoNominaDetBean> detalle = new LinkedHashMap<String, ArchivoNominaDetBean>();
		ArchivoNominaDetBean detalleBean = null;
		int llave = 2;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va a obtener la informacion del detalle de -->"
						+ idFav, EIGlobal.NivelLog.INFO);

		resultado = inicializarConexionBD();

		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_DETALLE_NOMINA + idFav
								+ " order by NUM_SEC_PK");

				EIGlobal.mensajePorTrace("Ejecutando el query: "
						+ QUERY_CONSULTA_DETALLE_NOMINA, EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					detalleBean = new ArchivoNominaDetBean();

					//llave = resultSet.getInt("NUM_SEC_PK");
					detalleBean.setSecuencia(llave);
					detalleBean.setIdEmpleado(resultSet
							.getString("NUM_EMPL"));
					detalleBean.setCuentaAbono(resultSet
							.getString("NUM_CTA_ABONO"));
					detalleBean.setNombre(resultSet.getString("DSC_NOM_ABONO"));
					detalleBean.setAppPaterno(resultSet
							.getString("DSC_APA_ABONO"));
					detalleBean.setAppMaterno(resultSet
							.getString("DSC_AMA_ABONO"));
					detalleBean.setImporte(resultSet.getDouble("IMP_AUX_FAV"));
					detalleBean.setTipoPago("ID_CNPTO_FK");

					detalle.put("" + llave, detalleBean);
					llave++;
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: EL numero de favoritos son: "
								+ detalle.size(), EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_DETALLE_NOMINA
								+ ": " + e.getMessage(), EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}
		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return detalle;
	}

	/**
	 * Da de alta un favorito
	 *
	 * @param descFav
	 *            Descripcion que identifica a un favorito.
	 * @param usuario
	 *            Usuario que da de alta al favorito.
	 * @param datosFavoritoNom
	 *            Datos del favorito a dar de alta.
	 * @return Resultado del alta.
	 */
	public boolean agregarFavorito(String descFav, String usuario,
			ArchivoNominaBean datosFavoritoNom) {
		boolean resultado = false;
		int numeroFav = 0;
		LinkedHashMap<String, ArchivoNominaDetBean> detallesFav = null;
		ArchivoNominaDetBean auxDetalle = null;
		String llave = "";
		Iterator<String> iteDetalle = null;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va insertar un favorito para "
						+ datosFavoritoNom.getContrato() + " - " + usuario
						+ " con las cuenatas " + datosFavoritoNom.getCtaCargo()
						+ " a  con un importe de $"
						+ datosFavoritoNom.getImpTotal(),
				EIGlobal.NivelLog.INFO);

		try {
			numeroFav = obtenerNumeroFavoritosTOTAL() + 1;

			darDeAltaFavoritoCtrl(descFav, usuario, numeroFav, datosFavoritoNom);

			detallesFav = datosFavoritoNom.getDetalleArchivo();

			if (detallesFav != null) {
				iteDetalle = detallesFav.keySet().iterator();

				while (iteDetalle.hasNext()) {
					llave = iteDetalle.next();
					EIGlobal.mensajePorTrace("!!!!LLAVE:"+llave, EIGlobal.NivelLog.INFO);
					auxDetalle = detallesFav.get(llave);
					darDeAltaFavoritoDetalle(numeroFav, auxDetalle);
				}

			}

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Error al ejecutar la insercion de un favorito: "
							+ e.getMessage(), EIGlobal.NivelLog.INFO);
		}

		return resultado;
	}

	/**
	 * Da de alta el favorito de control.
	 *
	 * @param descFav
	 *            Descripcion correspondiente al favorito.
	 * @param usuario
	 *            Usuario quien dio de alta el favorit0
	 * @param idFav
	 *            Id del favorito
	 * @param datosFavoritoNom
	 *            Datos del favorito
	 * @throws SQLException
	 *             Excepcion SQL.
	 */
	private void darDeAltaFavoritoCtrl(String descFav, String usuario,
			int idFav, ArchivoNominaBean datosFavoritoNom) throws SQLException {

		String descCuenta = "";
		byte strDescFav[] = null;
		String dscFav = null;
		boolean resultado = false;

		try {

			descCuenta = consultarDescCuenta(datosFavoritoNom.getContrato(),
					datosFavoritoNom.getCtaCargo());

			resultado = inicializarConexionBD();

			if (resultado) {
				preparedStatement = conexion
						.prepareStatement(QUERY_INSERT_FAVORITO_CTRL);

				conexion.setAutoCommit(false);

				preparedStatement.setInt(1, idFav);
				strDescFav = descFav.getBytes();
				try {
					dscFav = new String(strDescFav, "UTF-8");
					EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Favorito sin Encode: "
							+ descFav , EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Favorito con Encode: "
							+ dscFav , EIGlobal.NivelLog.INFO);
				} catch (UnsupportedEncodingException e)
				{
					EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Excepcion con Encode: "
							+ e.getMessage() , EIGlobal.NivelLog.INFO);
				}
				preparedStatement.setString(2, dscFav);
				preparedStatement.setString(3, datosFavoritoNom.getContrato());
				preparedStatement.setString(4, usuario);
				preparedStatement.setString(5, datosFavoritoNom.getCtaCargo());
				preparedStatement.setString(6, descCuenta);
				preparedStatement.setInt(7, datosFavoritoNom.getNumRegistros());
				preparedStatement.setDouble(8, datosFavoritoNom.getImpTotal());

				resultado = preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit",
						EIGlobal.NivelLog.INFO);
				conexion.commit();
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Error al ejecutar la insercion de un favorito: "
							+ QUERY_INSERT_FAVORITO_CTRL + ": "
							+ e.getMessage(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Se aplica el rolleback",
					EIGlobal.NivelLog.INFO);
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				EIGlobal.mensajePorTrace("FavoritosEnlaceDAO: Error rollback "
						+ e1.getMessage(), EIGlobal.NivelLog.INFO);
			}
			throw e;
		} finally {
			cerrarConexionBD();
		}

	}

	/**
	 * Da de alta el favorito de detalle.
	 *
	 * @param idFav
	 *            Id del favorito.
	 * @param datosFavoritoNom
	 *            Datos del favorito a dar de alta.
	 * @throws SQLException
	 *             Excepcion SQL
	 */
	private void darDeAltaFavoritoDetalle(int idFav,
			ArchivoNominaDetBean datosFavoritoNom) throws SQLException {

		EIGlobal.mensajePorTrace("Se dara de alta el detalle de :"
				+ datosFavoritoNom.getIdEmpleado(), EIGlobal.NivelLog.INFO);
		boolean resultado = inicializarConexionBD();
		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_INSERT_DETALLE_FAVORITO);

				conexion.setAutoCommit(false);

				preparedStatement.setInt(1, idFav);
				preparedStatement.setInt(2, datosFavoritoNom.getSecuencia());
				preparedStatement
						.setString(3, datosFavoritoNom.getIdEmpleado());
				preparedStatement.setString(4,
						datosFavoritoNom.getCuentaAbono());
				preparedStatement.setString(5, datosFavoritoNom.getNombre());
				preparedStatement
						.setString(6, datosFavoritoNom.getAppPaterno());
				preparedStatement
						.setString(7, datosFavoritoNom.getAppMaterno());
				preparedStatement.setDouble(8,datosFavoritoNom.getImporte());
				preparedStatement.setString(9, datosFavoritoNom.getTipoPago());

				resultado = preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit",
						EIGlobal.NivelLog.INFO);
				conexion.commit();

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la insercion de un favorito: "
								+ QUERY_INSERT_FAVORITO_CTRL + ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace(
							"FavoritosEnlaceDAO: Error rollback "
									+ e1.getMessage(), EIGlobal.NivelLog.INFO);
				}
				throw e;
			} finally {
				cerrarConexionBD();
			}
		}

	}

	/**
	 * Obtiene el total de favoritos en la tabla
	 *
	 * @return Numero de favoritos en la tabla.
	 * @throws SQLException SQLException
	 */
	public int obtenerNumeroFavoritosTOTAL() throws SQLException {
		int numeroFavoritos = 0;

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceNominaDAO: Se va a obtener el numero de favoritos ",
				EIGlobal.NivelLog.INFO);

		if (inicializarConexionBD()) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA_TOTAL);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					numeroFavoritos = resultSet.getInt(1);
				}

				EIGlobal.mensajePorTrace("EL numero de favoritos son: "
						+ numeroFavoritos, EIGlobal.NivelLog.INFO);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceNominaDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_NUMERO_FAVORITOS_NOMINA
								+ ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				throw e;
			} finally {
				cerrarConexionBD();
			}

		}
		return numeroFavoritos;
	}
}