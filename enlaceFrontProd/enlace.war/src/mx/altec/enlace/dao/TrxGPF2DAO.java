package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmTrxGPF2;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase de conexiones para manejar el procedimiento para la transaccion GPF2
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 27, 2011
 */
public class TrxGPF2DAO extends AdmonUsuariosMQDAO{
	
	/**
	 * Metodo encargado de generar los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion PE80
	 * 
	 * @param numPersona		Numero de persona
	 * @param codEntidad		Codigo de entidad
	 * @param codOficina		Codigo de oficina
	 * @param numCuenta			Numero de cuenta
	 * @param codProducto		Codigo de producto
	 * @param codSubProducto	Codigo de Subproducto
	 * @param calPar			Cal.Par
	 * @param ordenPar			Orden de Participacion
	 * @param segLlamada		Seguimiento Llamada
	 * 
	 * @return AdmTrxGPF2		Instancia de tipo AdmTrxGPF2
	 */
	private AdmTrxGPF2 ejecutaGPF2(String idBanco, String descBanco,  String codPais, 
								   String ciudad,  String idBusqueda, String llavePag) {
		
		StringBuffer sb = new StringBuffer();
		sb.append(idBanco)					//CVE-GRUPO
		  .append(descBanco)
		  .append(codPais)
		  .append(ciudad)
		  .append(idBusqueda)
		  .append(llavePag);			
		
    	EIGlobal.mensajePorTrace("TrxGPF2DAO::ejecutaGPF2:: Armando trama:" + sb.toString()
    			, EIGlobal.NivelLog.DEBUG);
		
		return consulta(AdmTrxGPF2.HEADER, sb.toString(), AdmTrxGPF2.getFactoryInstance());
	}
	
	/**
	 * Metodo encargado de recibir los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion PE80
	 * 
	 * @param numPersona		Numero de persona
	 * @param codEntidad		Codigo de entidad
	 * @param codOficina		Codigo de oficina
	 * @param numCuenta			Numero de cuenta
	 * @param codProducto		Codigo de producto
	 * @param codSubProducto	Codigo de Subproducto
	 * @param calPar			Cal.Par
	 * @param ordenPar			Orden de Participacion
	 * @param segLlamada		Seguimiento Llamada
	 * 
	 * @return AdmTrxGPF2		Instancia de tipo AdmTrxGPF2
	 */
	public AdmTrxGPF2 consultaTrxGPF2(String idBanco, String descBanco,  String codPais, 
											String ciudad,  String idBusqueda, String llavePag) {
    	EIGlobal.mensajePorTrace("TrxGPF2DAO::consultaTrxGPF2:: Llegando a metodo catalogoCambBancoEU"
    			, EIGlobal.NivelLog.DEBUG);
		return ejecutaGPF2(idBanco, descBanco,  codPais, ciudad, idBusqueda, llavePag);			
	}

}
