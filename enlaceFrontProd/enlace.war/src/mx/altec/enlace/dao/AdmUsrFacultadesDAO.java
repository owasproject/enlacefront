package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmUsrGL31;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmUsrFacultadesDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL31
	private AdmUsrGL31 ejecutaGL31Base(String cveFacultad,
			String indSuperUsr, String nivel, String grupoFiltro, String indPag,
			String indRePag) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(cveFacultad, 16))					//CVE-GRUPO
			.append(rellenar(indSuperUsr, 2))
			.append(rellenar(nivel, 1))					//CVE-GRUPO
			.append(rellenar(grupoFiltro, 3))					//CVE-GRUPO
			.append(rellenar(indPag, 2))					//CVE-GRUPO
			.append(rellenar(indRePag, 22));					//CVE-GRUPO
		return consulta(AdmUsrGL31.HEADER, sb.toString(), AdmUsrGL31.getFactoryInstance());
	}
	
	public AdmUsrGL31 consultaFacultades(String cveFacultad,
			String indSuperUsr, String nivel, String grupoFiltro, String indPag,
			String indRePag) {
		logInfo("GL31 - Consulta de Facultades");
		return ejecutaGL31Base(cveFacultad, indSuperUsr, nivel, grupoFiltro, indPag,
				indRePag);			
	}

}
