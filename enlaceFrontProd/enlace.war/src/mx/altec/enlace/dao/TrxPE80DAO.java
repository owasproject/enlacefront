package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmTrxPE80;
import mx.altec.enlace.utilerias.EIGlobal;
import static mx.altec.enlace.utilerias.NomPreUtil.*;

/**
 *  Clase de conexiones para manejar el procedimiento para la transaccion PE80
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Sep 22, 2010
 */
public class TrxPE80DAO extends AdmonUsuariosMQDAO {
	
	/**
	 * Metodo encargado de generar los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion PE80
	 * 
	 * @param numPersona		Numero de persona
	 * @param codEntidad		Codigo de entidad
	 * @param codOficina		Codigo de oficina
	 * @param numCuenta			Numero de cuenta
	 * @param codProducto		Codigo de producto
	 * @param codSubProducto	Codigo de Subproducto
	 * @param calPar			Cal.Par
	 * @param ordenPar			Orden de Participacion
	 * @param segLlamada		Seguimiento Llamada
	 * 
	 * @return AdmTrxPE80		Instancia de tipo AdmTrxPE80
	 */
	private AdmTrxPE80 ejecutaPE80(String numPersona, String codEntidad,  String codOficina, 
								   String numCuenta,  String codProducto, String codSubProducto,
								   String calPar,     String ordenPar,    String segLlamada) {
		
		StringBuffer sb = new StringBuffer();
		sb.append(numPersona)					//CVE-GRUPO
		  .append(codEntidad)
		  .append(codOficina)
		  .append(numCuenta)
		  .append(codProducto)
		  .append(codSubProducto)
		  .append(calPar)
		  .append(ordenPar)
		  .append(segLlamada);			
		
    	EIGlobal.mensajePorTrace("TrxPE80DAO::ejecutaPE80:: Armando trama:" + sb.toString()
    			, EIGlobal.NivelLog.DEBUG);
		
		return consulta(AdmTrxPE80.HEADER, sb.toString(), AdmTrxPE80.getFactoryInstance());
	}
	
	/**
	 * Metodo encargado de recibir los atributos de conforman la trama de entrada para
	 * la ejecucion de la transaccion PE80
	 * 
	 * @param numPersona		Numero de persona
	 * @param codEntidad		Codigo de entidad
	 * @param codOficina		Codigo de oficina
	 * @param numCuenta			Numero de cuenta
	 * @param codProducto		Codigo de producto
	 * @param codSubProducto	Codigo de Subproducto
	 * @param calPar			Cal.Par
	 * @param ordenPar			Orden de Participacion
	 * @param segLlamada		Seguimiento Llamada
	 * 
	 * @return AdmTrxPE80		Instancia de tipo AdmTrxPE80
	 */
	public AdmTrxPE80 intervinientesContrato(String numPersona, String codEntidad,  String codOficina, 
											String numCuenta,  String codProducto, String codSubProducto,
											String calPar,     String ordenPar,    String segLlamada) {
    	EIGlobal.mensajePorTrace("TrxPE80DAO::intervinientesContrato:: Llegando a metodo intervinientesContrato"
    			, EIGlobal.NivelLog.DEBUG);
		return ejecutaPE80(numPersona, codEntidad, codOficina, numCuenta, codProducto,
				           codSubProducto, calPar, ordenPar, segLlamada);			
	}

}
