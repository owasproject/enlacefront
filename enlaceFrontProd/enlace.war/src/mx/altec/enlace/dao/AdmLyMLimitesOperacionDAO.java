package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmLyMGL26;
import mx.altec.enlace.beans.AdmLyMGL27;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmLyMLimitesOperacionDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL26
	private AdmLyMGL26 ejecutaGL26Base(String opcion, String contrato, 
			String usuario, String cveGrupo, String monto) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(opcion, 2))					//CVE-GRUPO
			.append(rellenar(contrato, 11))
			.append(rellenar(usuario, 8))
			.append(rellenar(cveGrupo, 2))
			.append(rellenar(monto, 17));
		return consulta(AdmLyMGL26.HEADER, sb.toString(), AdmLyMGL26.getFactoryInstance());
	}
	
	public AdmLyMGL26 mantenimientoLimitesOperacion(String opcion, String contrato, 
			String usuario, String cveGrupo, String monto) {
		logInfo("GL26 - Mantenimiento de Limites por Operacion");
		return ejecutaGL26Base(opcion, contrato, usuario, cveGrupo, monto);			
	}

	//TRANS: GL27
	private AdmLyMGL27 ejecutaGL27Base(String contrato, String usuario, 
			String cveGrupo) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(contrato, 11))
			.append(rellenar(usuario, 8))
			.append(rellenar(cveGrupo, 2));
		return consulta(AdmLyMGL27.HEADER, sb.toString(), AdmLyMGL27.getFactoryInstance());
	}
	
	public AdmLyMGL27 consultaLimitesOperacion(String contrato, String usuario, 
			String cveGrupo) {
		logInfo("GL27 - Consulta de Limites por Operacion");
		return ejecutaGL27Base(contrato, usuario, cveGrupo);			
	}

}
