package mx.altec.enlace.dao;

import java.io.*;
import java.lang.*;
import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/**
 * The Class MAC_Interb.
 */
public class MAC_Interb
{

/** The len bn. */
  public final int lenBN	=  83;
  
  /** The len nm. */
  public static final int LENNM	=  71; //lenNM
  
  /** The credito. */
  public static final int CREDITO	=  2;  /*10*/
 /** The clabe. */
  public static final int CLABE	= 40;
  /** The movil. */
  public static final int MOVIL	= 10;

 /** The ext. */
  public static final String EXT	= "EXTRNA";

/** The tipo registro. */
  public final String tipoRegistro = EXT;
 /** The cuenta. */
  public String cuenta;
 /** The nombre titular. */
  public String nombreTitular;
/** The cve plaza. */
  public String cvePlaza;
/** The cve banco. */
  public String cveBanco;
 /** The sucursal. */
  public int sucursal;
/** The tipo cuenta. */
  public int tipoCuenta;
 /** The cve interme. */
  public String cveInterme;



  /** The err. */
  public int err;
/** The lista errores. */
  public Vector listaErrores=new Vector();

/** The lista plazas. */
  public Vector listaPlazas=new Vector();
/** The lista bancos. */
  public Vector listaBancos=new Vector();

/** The valida. */
  public boolean valida=true;

 /**
	 * Inicializa.
	 */
  public void inicializa()
	{
	  cuenta="";
	  nombreTitular="";
	  cveBanco="";
	  cvePlaza="";
	  sucursal=0;
	  tipoCuenta=0;
	  err=0;
	}

  /**
	 * Parses the.
	 * 
	 * @param linea the linea
	 * @return the int
	 */
  public int parse(String linea)
	{
		log("MAC_Interb - parse(): Separando los datos ...", EIGlobal.NivelLog.INFO);
		inicializa();
        if(linea.length()==lenBN)
		  {
			setCuenta(linea.substring(6,26).trim());						/*20*/
			setNombreTitular(linea.substring(26,66).trim());				/*40*/
			setClaveBanco(linea.substring(66,71).trim());		/*5*/
			setClavePlaza(linea.substring(71,76).trim());		/*5*/
			setSucursal(linea.substring(76,81).trim());		/*5*/
			setTipoCuenta(linea.substring(81).trim());		/*2*/
			validaCuenta();
              }else if(linea.length()==LENNM){
               log("MAC_Interb - TRAMA CUENTA MOVIL :: "+linea, EIGlobal.NivelLog.INFO);
			  setCuenta(linea.substring(6,26).trim());
			  setNombreTitular(linea.substring(26,66).trim());
			  setClaveBanco(linea.substring(66,71).trim());			  
			  setClavePlaza(""); //Sin valor para alta de cta. Movil	
			  setSucursal("");	//Sin valor para alta de cta. Movil				  
			  setTipoCuenta("10");//No existe posicion en el archivo para tomar el valor del tipo de cta. 
              validaCuenta();
		  }else  {
			log("MAC_Interb - parse(): Error en el formato de la linea.", EIGlobal.NivelLog.INFO);
			listaErrores.add("Longitud de linea incorrecto: "+ linea.length());
			err=1;
		  }
		return err;
	}

  /**
	 * Campo vacio.
	 * 
	 * @param campo the campo
	 * @param msg the msg
	 */
  public void campoVacio(String campo,String msg)
	{
	  if(campo.equals(""))
		{
		  log("MAC_Interb - campoVacio(): "+msg+" no contiene datos.", EIGlobal.NivelLog.INFO);
		  listaErrores.add(msg + " no puede ser un campo vacio.");
		  err+=1;
		}
	}

  /**
	 * Sets the cuenta.
	 * 
	 * @param cad the new cuenta
	 */
  public void setCuenta(String cad)
	{
	  cuenta=cad;
	  campoVacio(cuenta,"La cuenta");
	}

  /**
	 * Sets the nombre titular.
	 * 
	 * @param cad the new nombre titular
	 */
  public void setNombreTitular(String cad)
	{
	  nombreTitular=cad;
	  campoVacio(nombreTitular,"La descripci&oacute;n o Titular de la cuenta");
	}
	
 /**
	 * *************************************************************************
	 * ***************.
	 * 
	 * @param cad the new clave banco
	 */
  /* Bancos Nacionales                                                                       */
  public void setClaveBanco(String cad)
	{
	  cveBanco=cad;
	  campoVacio(cveBanco,"La clave del banco");

	  if(valida && !cad.equals(""))
		{
		   if(listaBancos!=null && listaBancos.indexOf(cveBanco)<0)
			{
			  log("MAC_Registro - setClaveBanco(): banco no encontrado.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave del banco no existe en el cat&aacute;logo.");
			  err+=1;
			}
	    }
	}

  /**
	 * Sets the clave plaza.
	 * 
	 * @param cad the new clave plaza
	 */
  public void setClavePlaza(String cad)
	{
	  cvePlaza=cad;
	  /*campoVacio(cvePlaza,"La clave de la plaza");*/
	  if(valida && !cvePlaza.equals(""))
		{
		  if(cvePlaza.length()> 3 && listaPlazas!=null && listaPlazas.indexOf(cvePlaza)<0)
			{
			  log("MAC_Registro - setClavePlaza(): plaza no encontrada.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave de la plaza no existe en el cat&aacute;logo.");
			  err+=1;
			}
	    }
	}

  /**
	 * Sets the sucursal.
	 * 
	 * @param cad the new sucursal
	 */
  public void setSucursal(String cad)
	{
	  /*campoVacio(cad,"La sucursal");*/
	  if(!cad.equals(""))
		{
		  try
			{
			  sucursal = new Integer(cad).intValue();
			}catch(Exception e)
			 {
				log("MAC_Registro - setSucursal(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				listaErrores.add("Formato de numero de sucursal incorrecto: "+ cad);
				err+=1;
			 }
		} else {
			sucursal=0;
		}
  }

 /**
	 * Sets the tipo cuenta.
	 * 
	 * @param cad  the new tipo cuenta
	 */
  public void setTipoCuenta(String cad)
	{
	  if(cad.length()!=2)
		{
		  log("MAC_Interb - setTipoCuenta(): Longitud diferente de 2", EIGlobal.NivelLog.INFO);
		  listaErrores.add("El tipo de cuenta debe ser num&eacute;rico de 2 posiciones: "+ cad);
		  err+=1;
		}
	  else
		{
		  try
			{
			  tipoCuenta = new Integer(cad).intValue();
			}catch(Exception e)
			 {
				log("MAC_Interb - setTipoCuenta(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				listaErrores.add("El tipo de cuenta debe ser num&eacute;rico: "+ cad);
				err+=1;
			 }
		   if(/*tipoCuenta!=CHEQUES &&*/ /*tipoCuenta!=DEBITO &&*/ tipoCuenta!=CREDITO && tipoCuenta!=CLABE && tipoCuenta!=MOVIL)
			 {
				log("MAC_Interb - setTipoCuenta(): Tipo cuenta no reconocido ... ", EIGlobal.NivelLog.INFO);
				listaErrores.add("No se puede identificar el tipo de cuenta: "+ cad);
				err+=1;
			 }
		}
	}


  /**
	 * Valida cuenta.
	 */
  public void validaCuenta()
	{
	  switch(tipoCuenta)
		{
		  case CREDITO:
			  /*if(cuenta.length()==16)
				{
				  String prefijo=cuenta.substring(0, 4);
				  if( !prefijo.equals( "4931" ) && !prefijo.equals( "4547" ) && !prefijo.equals( "4915" ) &&
					  !prefijo.equals( "5453" ) && !prefijo.equals( "4941" ) && !prefijo.equals( "5471" ) &&
					  !prefijo.equals( "5470" ) && !prefijo.equals( "5474" ) && !prefijo.equals( "5408" ) &&
					  !prefijo.equals( "4913" ) && !prefijo.equals( "2728" ) && !prefijo.equals( "5549" ) )
					{
					  log("MAC_Registro - validaCuenta(): El formato de la cuenta no corresponde con los prefijos bancarios", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La cuenta "+cuenta+" no cumple con el formato requerido.");
					  err+=1;
					}
				}*/
			  if(cuenta.length()!=16)
				{
		          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto ", EIGlobal.NivelLog.INFO);
				  listaErrores.add("La longitud de la cuenta no es correcta ["+cuenta.length()+"].");
				  err+=1;
	            }
		      break;
		  case CLABE:
		      if(cuenta.trim().length()!=18)
		        {
		          log("MAC_Interb - validaCuenta(): longitud de cuenta incorrecto MB ", EIGlobal.NivelLog.INFO);
				  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado: claBE");
				  err+=1;
		        }
			  else
		        {
				  // JGAL - Activar
				  if(!validaDigitoVerificador())
				    {
					  log("MAC_Interb - validaCuenta(): longitud de cuenta incorrecto claBE", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La cuenta claBE no es v&aacute;lida: "+ cuenta);
					  err+=1;
					}
				}
		      break;
    case MOVIL: //Valida alta de cuentas moviles Julio 2014 -- Vector
			  if(cuenta.trim().length()!=10){
				  log("MAC_Interb - validaCuenta(): longitud de cuenta incorrecto NUMERO MOVIL ", EIGlobal.NivelLog.INFO);
				  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado: NUM MOVIL");
				  err+=1;
			  }
			  break;
		}
	}

  /**
	 * Sets the cve interme.
	 * 
	 * @param cad the new cve interme
	 */
  public void setCveInterme(String cad)
	{
	  cveInterme=cad;
	}

  /**
	 * Valida digito verificador.
	 * 
	 * @return true, if successful
	 */
  public boolean validaDigitoVerificador()
	{
	  String pesos="371371371371371371371371371";
	  String cuenta2=cuenta.substring(0,17);

	  int DC=0;

	  for(int i=0;i<cuenta2.length();i++)
        DC += (Integer.parseInt(cuenta2.substring(i,i+1))) * (Integer.parseInt(pesos.substring(i,i+1))) % 10;
	  DC = 10 - (DC % 10);
	  DC = (DC >= 10) ? 0 : DC;
	  cuenta2 += DC;

	  if(cuenta2.trim().equals(cuenta.trim()))
	    return true;
	  return false;
	}

  /**
	 * Log.
	 * 
	 * @param a the a
	 * @param b the b
	 */
   public void log(String a, EIGlobal.NivelLog b)
	{
	  //System.out.println(a + " <Tr> " + b);
	  EIGlobal.mensajePorTrace(a,b);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	protected void finalize() throws Throwable {
		super.finalize();
		cuenta = null;
		nombreTitular = null;
		cvePlaza = null;
		cveBanco = null;
		cveInterme = null;
		if(listaErrores != null) {
			listaErrores.clear();
			listaErrores = null;
		}
		if(listaPlazas != null) {
			listaPlazas.clear();
			listaPlazas = null;
		}
		if(listaBancos != null) {
			listaBancos.clear();
			listaBancos = null;
		}
	}

	
}