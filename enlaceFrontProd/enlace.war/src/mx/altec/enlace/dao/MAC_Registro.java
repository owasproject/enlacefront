/** 
*   Isban Mexico
*   Clase: MAC_Registro.java
*   Descripcion: Objeto de acceso a datos para la Registro.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid). 
*/
package mx.altec.enlace.dao;

import java.util.*;

import mx.altec.enlace.utilerias.EIGlobal;



public class MAC_Registro
{


  /**Longitud del layout del archivo para cuentas internas (propias y terceros)*/
  public static final int LEN_MB	=  66;
  
  /**Longitud del layout del archivo para cuentas externas Nacionales*/
  public static final int LEN_BN	=  83;
  
  /**Longitud del layout del archivo para cuentas internacionales*/
  public static final int LEN_BI	= 166;
  
  /**Longitud del layout del archivo para cuentas de numeros moviles*/
  public static final int LENNM	= 71;

  /**Constante para cuentas de mismo banco*/
  public static final String SAN	= "SANTAN";

  /**Longitud de una cuenta movil*/
  public static final int MOVIL = 10;


  /*public final int CHEQUES	=  1;*/  /* 4*/ /* Por disposicion oficial ya solo se capturan cuentas clabe
  /*public final int DEBITO	=  2;  Debito y credito se tratan sin distincion*/
  
  /**Constante num&eacute;rica para las Cuentas Externas Nacionales de Cr&eacute;dito*/
  public static final int CREDITO	=  2;  /*10*/
  
  /**Constante num&eacute;rica para las Cuentas Externas Nacionales indicadas por la CLABE*/
  public static final int CLABE	= 40;
  
  /**Constante para cuentas m&oacute;viles*/
  public static final String MOV    = "MOVIL ";
  
  /**Constante para cuentas externas nacionales (otros bancos)*/
  public static final String EXT	= "EXTRNA";
  
  /**Constante para cuentas internacionales*/
  public static final String INT	= "INTNAL";

  /** Holds value of property tipoRegistro. */
  public String tipoRegistro;
  
  /** Holds value of property cuenta. */
  public String cuenta;
  
  /** Holds value of property nombreTitular. */
  public String nombreTitular;

  /** Holds value of property cveBanco. */
  public String cveBanco;
  
  /** Holds value of property cvePlaza. */
  public String cvePlaza;

  /** Holds value of property sucursal. */
  public String sucursal;
  
  /** Holds value of property tipoCuenta. */
  public int tipoCuenta;

  /** Holds value of property cvePais. */
  public String cvePais;
  
  /** Holds value of property cveDivisa. */
  public String cveDivisa;
  
  /** Holds value of property ciudad. */
  public String ciudad;
  
  /** Holds value of property banco. */
  public String banco;

  /** Holds value of property cveABA. */
  public String cveABA;

  /** Holds value of property err. */
  public int err;
  
  /** Holds value of property listaErrores. */
  public Vector listaErrores=new Vector();

  /** Holds value of property listaPlazas. */
  public Vector listaPlazas=new Vector();
  
  /** Holds value of property listaBancos. */
  public Vector listaBancos=new Vector();
  
  /** Holds value of property listaPaises. */
  public Vector listaPaises=new Vector();
  
  /** Holds value of property listaDivisas. */
  public Vector listaDivisas=new Vector();

  /** Holds value of property valida. */
  public boolean valida=true;

  /**
   * Metodo que inicializa los atributos de la clase.
   * 
   */
  public void inicializa()
	{
	  tipoRegistro="";
	  cuenta="";
	  nombreTitular="";

	  cveBanco="";
	  cvePlaza="";
	  sucursal="";
	  tipoCuenta=0;
	  /*00  Mismo Banco
		99  Internacional*/
	  cvePais="";
	  ciudad="";
	  banco="";
	  cveABA="";
	  cveDivisa="";

	  err=0;
	}

  /**
   * Realiza el parsing de una l&iacute;nea proveniente del archivo de
   * @param linea Linea del archivo de texto (o registro) que va a parsear
   * @return numero de errores al intentar hacer el parseo
   * */
  public int parse(String linea)
	{
		log("MAC_Registro - parse(): Separando los datos ...", EIGlobal.NivelLog.INFO);

		inicializa();

        if(linea.length()==LEN_MB || linea.length()==LEN_BN || linea.length()==LEN_BI || linea.length()==LENNM)
		  {
			setTipoRegistro(linea.substring(0,6),linea.length());	/* 5*/
			setCuenta(linea.substring(6,26).trim());						/*20*/
			setNombreTitular(linea.substring(26,66).trim());				/*40*/

			switch (linea.length())
			 {
			  case LEN_MB:
				          setClaveBanco("BANME");
			              setClavePlaza("01001");
						  setSucursal("035");
				   break;
			  case LEN_BN:
				          setClaveBanco(linea.substring(66,71).trim());		/*5*/
			              setClavePlaza(linea.substring(71,76).trim());		/*5*/
			              asignaClaveDivisaBN(linea.substring(76,81).trim(), linea.substring(81).trim());		/*5*/
						  setTipoCuenta(linea.substring(81).trim());		/*2*/
				   break;
			  case LEN_BI:
				          setClavePais(linea.substring(66,70).trim());		/* 4*/
			              setClaveDivisa(linea.substring(70,74).trim());	/* 4*/
						  setCiudad(linea.substring(74,114).trim());		/*40*/
						  setBanco(linea.substring(114,154).trim());		/*40*/
						  setClaveABA(linea.substring(66,70).trim(),linea.substring(154).trim()); /*12*/
						  tipoCuenta=99;
				   break;
			  case LENNM:
				  		  StringBuilder movilValue = new StringBuilder("");
				  		  movilValue.append(MOVIL);
				  		  setTipoCuenta(movilValue.toString());
				  		  setClaveBanco(linea.substring(66,71).trim());
				  break;
			 }
			validaCuenta();
		  }
		else
		  {
			log("MAC_Registro - parse(): Error en el formato de la linea.", EIGlobal.NivelLog.INFO);
			listaErrores.add("Longitud de linea incorrecto: "+ linea.length());
			err=1;
		  }
		return err;
	}

  /**
   * Verifica si el campo indicado por el par&aacute;metro <strong>campo</strong> 
   * es vac&iacute;o, en cuyo caso agregar&aacute; el valor de <strong>msg</strong>
   * a la lista de errores.
   * @param	campo		valor a ser probado
   * @param msg			mensaje de error a reportar en caso que el <i>campo</i> est&eacute; vac&iacute;o
   * */
  public void campoVacio(String campo,String msg)
	{
	  if("".equals(campo))
		{
		  log("MAC_Registro - campoVacio(): "+msg+" no contiene datos.", EIGlobal.NivelLog.INFO);
		  listaErrores.add(msg + " no puede ser un campo vacio.");
		  err+=1;
		}
	}

  /*******************************************************************************************/
  /**
   * M&eacute;todo para establecer el tipo de cuenta y la longitud requerida por dicho tipo de cuenta
   * @param cad 	establece el tipo de registro que se va a procesar, los valores aceptados son:
   * 				{@link mx.altec.enlace.dao.MAC_Registro.SAN}, 
   * 				{@link mx.altec.enlace.dao.MAC_Registro.EXT},
   * 				{@link mx.altec.enlace.dao.MAC_Registro.INT} y 
   * 				{@link mx.altec.enlace.dao.MAC_Registro.MOV}
   * @param len 	indica la longitud requerida para la cadena proveniente de la trama
   * */
  public void setTipoRegistro(String cad,int len)
	{
	  tipoRegistro=cad;
	  if(!tipoRegistro.equals(SAN) && !tipoRegistro.equals(EXT) && !tipoRegistro.equals(INT) && !tipoRegistro.equals(MOV))
		{
		  log("MAC_Registro - setTipoRegistro(): Error en el tipo de registro.", EIGlobal.NivelLog.INFO);
		  listaErrores.add("Tipo de Cuenta no conocida: "+ cad);
		  err+=1;
		}
          log("tipo Registro  "  + tipoRegistro + " Longitud = "+ len , EIGlobal.NivelLog.INFO);
          if( (tipoRegistro.equals(SAN) && len!=LEN_MB) ||
              (tipoRegistro.equals(EXT) && len!=LEN_BN) ||
              (tipoRegistro.equals(INT) && len!=LEN_BI) ||
              (tipoRegistro.equals(MOV) && len!=LENNM)
            )
           {
			  log("MAC_Registro - setTipoRegistro(): Error en el tipo de registro y la longitud.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("El tipo de registro especificado,"+cad+", no cumple con la longitud esperada.");
			  err+=1;
	       }
	}

  /**
   * Sets the value of property Cuenta
   * @param cad 	el valor de la cuenta
   * */
  public void setCuenta(String cad)
	{
	  cuenta=cad;
	  campoVacio(cuenta,"La cuenta");
	}

  /**
   * Sets the value of property Nombre Titular.
   * Valida que la informaci&oacute;n no sea vac&iacute;a, de ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor del titular de la cuenta
   * */
  public void setNombreTitular(String cad)
	{
	  nombreTitular=cad;
	  campoVacio(nombreTitular,"La descripci&oacute;n o Titular de la cuenta");
	}

  /*******************************************************************************************/
  /* Bancos Nacionales                                                                       */
  /**
   * Sets the value of property Clave Banco.
   * Valida que la Clave de Banco exista, de no existir agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor de la Clave del Banco
   * */
  public void setClaveBanco(String cad)
	{
	  cveBanco=cad;
	  campoVacio(cveBanco,"La clave del banco");

	  if(valida && !"".equals(cad) && listaBancos!=null && listaBancos.indexOf(cveBanco)<0)
		{
			  log("MAC_Registro - setClaveBanco(): banco no encontrado.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave del banco no existe en el cat&aacute;logo.");
			  err+=1;
	    }
	}
  /**
   * Sets the value of property Clave Plaza.
   * Valida que la Clave de Plaza exista, de no existir agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor de la clave de plaza
   * */
  public void setClavePlaza(String cad)
	{
	  cvePlaza=cad;
	  /*campoVacio(cvePlaza,"La clave de la plaza");*/
	  if(valida && !"".equals(cvePlaza) && null != listaPlazas && listaPlazas.indexOf(cvePlaza)<0)
		{
			  log("MAC_Registro - setClavePlaza(): plaza no encontrada.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave de la plaza no existe en el cat&aacute;logo.");
			  err+=1;
	    }
	}
  
  /**
   * Sets the value of property Sucursal.
   * Valida que la Sucursal sea un valor num&eacute;rico, de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor del titular de la cuenta
   * */
  public void setSucursal(String cad)
	{
	  /*campoVacio(cad,"La sucursal");*/
	  if(!"".equals(cad))
		{
		  try
			{
			  sucursal = cad;
			}catch(Exception e)
			 {
				log("MAC_Registro - setSucursal(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				listaErrores.add("Formato de numero de sucursal incorrecto: "+ cad);
				err+=1;
			 }
		}
    }

  /**
   * Sets the value of property Tipo Cuenta.
   * Valida que el Tipo Cuenta sea un valor num&eacute;rico a dos posiciones, 
   * de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor del tipo de cuenta
   * */
  public void setTipoCuenta(String cad)
	{
	  if(cad.length()!=2)
		{
		  log("MAC_Registro - setTipoCuenta(): Longitud diferente de 2", EIGlobal.NivelLog.INFO);
		  listaErrores.add("El tipo de cuenta debe ser num&eacute;rico de 2 posiciones: "+ cad);
		  err+=1;
		}
	  else
		{
		  try
			{
			  tipoCuenta = new Integer(cad).intValue();
			}catch(Exception e)
			 {
				log("MAC_Registro - setTipoCuenta(): Excepcion, Error ... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				listaErrores.add("El tipo de cuenta debe ser num&eacute;rico: "+ cad);
				err+=1;
			 }
		   if(/*tipoCuenta!=CHEQUES &&*/ /*tipoCuenta!=DEBITO &&*/ tipoCuenta!=CREDITO && tipoCuenta!=CLABE && tipoCuenta!=MOVIL)
			 {
				log("MAC_Registro - setTipoCuenta(): Tipo cuenta no reconocido ... ", EIGlobal.NivelLog.INFO);
				listaErrores.add("No se puede identificar el tipo de cuenta: "+ cad);
				err+=1;
			 }
		}
	}

  /*******************************************************************************************/
  /* Internacionales                                                                         */
  /**
   * Sets the value of property Clave Pa&iacute;s.
   * Valida que la Clave Pa&iacute;s exista, de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor de la clave de pa&iacute;s
   * */
  public void setClavePais(String cad)
	{
	  cvePais=cad;
	  campoVacio(cvePais,"La clave del pa&iacute;s");
	  if(valida && !"".equals(cvePais) && null != listaPaises && listaPaises.indexOf(cvePais)<0)
		{
			  log("MAC_Registro - setClavePais(): La clave del pais no existe.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave del Pa&iacute;s no existe en el cat&aacute;logo.");
			  err+=1;
	    }
	}

  /**
   * Sets the value of property Clave Divisa.
   * Valida que la Clave Divisa no sea un valor vac&iacute;o y que exista, 
   * de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor de la clave de divisa
   * */
  public void setClaveDivisa(String cad)
	{
	  cveDivisa=cad;
	  campoVacio(cveDivisa,"La clave de la divisa");
	  if(valida && !"".equals(cveDivisa))
		{
		  if(listaDivisas!=null && listaDivisas.indexOf(cveDivisa)<0)
			{
			  log("MAC_Registro - setClaveDivisa(): La clave de la divisa no existe.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave de la Divisa no existe en el cat&aacute;logo.");
			  err+=1;
			}
	    }
	}

  /**
   * Valida que la Clave Divisa no sea un valor vacio y que exista, 
   * de no ser asi agrega una entrada al registro de errores.
   * @param cad el valor de la clave de divisa
   * @param tipoCue Tipo de cuenta
   */
  public void asignaClaveDivisaBN(String cad, String tipoCue)
	{
	  cveDivisa=cad;
	  campoVacio(cveDivisa,"La clave de la divisa");
	  
	  if(valida && !"".equals(cveDivisa))
		{
		  if(!"USD".equals(cveDivisa) && !"MXP".equals(cveDivisa)){
			  log("MAC_Registro - asignaClaveDivisaBN(): La clave de la divisa no existe.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave de la Divisa no existe.");
			  err+=1;
		  }
		  log("MAC_Registro - asignaClaveDivisaBN(): Clave divisa: "+cveDivisa+" Tipo cuenta: "+tipoCue+" Tam cuenta: "+cuenta.length(), EIGlobal.NivelLog.INFO);
		  if(("USD".equals(cveDivisa) && !"40".equals(tipoCue)) || ("USD".equals(cveDivisa) && 18!=cuenta.length())){
			  log("MAC_Registro - asignaClaveDivisaBN(): La clave de la divisa no coincide con el tipo de cuenta.", EIGlobal.NivelLog.INFO);
			  listaErrores.add("La clave de la Divisa no coincide con el Tipo de cuenta.");
			  err+=1;
		  }
			  
	    }  
	}
  /**
   * Sets the value of property Ciudad.
   * Valida que la informaci&oacute;n no sea vac&iacute;a, 
   * de ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor de la ciudad
   * */
  public void setCiudad(String cad)
	{
	  ciudad=cad;
	  campoVacio(ciudad,"La ciudad");
	}

  /**
   * Sets the value of property Banco.
   * Valida que la informaci&oacute;n no sea vac&iacute;a, 
   * de ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param cad 	el valor del banco
   * */
  public void setBanco(String cad)
	{
	  banco=cad;
	  campoVacio(ciudad,"El banco");
	}

  /**
   * Sets the value of property Clave ABA.
   * Valida que la informaci&oacute;n no est&eacute; vac&iacute;a y que exista, de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param pais	clave del pa&iacute;s
   * @param cad 	el valor de la ciudad
   * */
  public void setClaveABA(String pais,String cad){
    cveABA = cad;
	if("".equals(cad)){
		log("MAC_Registro - setClaveABA(): La clave ABA/SWIFT es requerida.", EIGlobal.NivelLog.INFO);
		listaErrores.add("La clave ABA/SWIFT es requerida.");
		 err+=1;
		}else{
			if(!existeClaveSwift(pais,cad)){
				log("MAC_Registro - setClaveABA(): No existe aba/swift en catalogo.", EIGlobal.NivelLog.INFO);
				listaErrores.add("La clave ABA/SWIFT especificada no existe: " + cad);
				err+=1;
			}
		}
	}

  /* ************************ Cambio - Clave SWIFT ******************/
  /**
   * Verifica .
   * Valida que la informaci&oacute;n no est&eacute; vac&iacute;a y que exista, 
   * de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * @param busCvePais 	la clave del pa&iacute;s
   * @param claveSwift	la clave SWIFT
   * @return <i>true</i> si la clave es v&aacute;lida, en caso contrario devolver&aacute; <i>false</i>
   * */
  private boolean existeClaveSwift(String busCvePais, String claveSwift){
      log(">>>>>>>>>>>>>>>>>>>>  " + busCvePais + " : " + claveSwift, EIGlobal.NivelLog.INFO);
      java.util.List paises 					= mx.altec.enlace.utilerias.CatDivisaPais.getInstance().getListaPaises();
      mx.altec.enlace.bo.TrxGPF2BO trxGPF2BO	= new mx.altec.enlace.bo.TrxGPF2BO();

      for(int i=0; i<paises.size(); i++){
        if(((mx.altec.enlace.bo.TrxGP93VO)paises.get(i)).getOVarCod().substring(4).trim().equals(busCvePais.trim())){
        	busCvePais = ((mx.altec.enlace.bo.TrxGP93VO)paises.get(i)).getOVarCod().substring(0,3).trim();
        	break;
        }
      }

      if(trxGPF2BO.obtenCatalogoBancos(claveSwift, busCvePais, "", "001".equals(busCvePais)?"A":"S").size() == 1){
    	  return true;
      }

        return false;
      }

  /************************* Cambio - Clave SWIFT ******************/

  /**
   * Valida que la cuenta ingresada a traves del m&eacute;todo <i>setCuenta()</i>
   * sea v&aacute;lida, de no ser as&iacute; agregar&aacute; una entrada al registro de errores.
   * */
  public void validaCuenta()
	{
	  switch(tipoCuenta)
		{
		  case 0: if(cuenta.length()==11)
					{
					  if("49".equals(cuenta.substring(0,2)))
						{
						   log("MAC_Registro - validaCuenta(): El formato de la cuenta es SAN CHEQ 49, error ", EIGlobal.NivelLog.INFO);
						   listaErrores.add("La cuenta ["+cuenta.length()+"] no cumple con el formato.");
						   err+=1;
						}
					}
				  else
				  if(cuenta.length()==16)
					{
					  String prefijo=cuenta.substring(0, 4);
					  if( !"4931".equals( prefijo ) && !"4547".equals( prefijo ) && !"4915".equals( prefijo ) &&
						  !"5453".equals( prefijo ) && !"4941".equals( prefijo ) && !"5471".equals( prefijo ) &&
						  !"5470".equals( prefijo ) && !"5474".equals( prefijo ) && !"5408".equals( prefijo ) &&
						  !"4913".equals( prefijo ) && !"2728".equals( prefijo ) && !"5549".equals( prefijo ) )
						{
						  log("MAC_Registro - validaCuenta(): El formato de la cuenta no corresponde con los prefijos bancarios", EIGlobal.NivelLog.INFO);
						  listaErrores.add("La cuenta "+cuenta+" no cumple con el formato requerido.");
						  err+=1;
						}
					}
				  else
					{
			          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto ", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La longitud de la cuenta no es correcta ["+cuenta.length()+"].");
					  err+=1;
		            }
			      break;
		  case CLABE:
			      if(cuenta.length()!=18)
			        {
			          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto MB ", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado: 01");
					  err+=1;
		            }
				  else
			        {
					  if(!validaDigitoVerificador())
					    {
						  log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto claBE", EIGlobal.NivelLog.INFO);
						  listaErrores.add("La cuenta claBE no es v&aacute;lida: "+ cuenta);
						  err+=1;
						}
					   /*  select NUM_CECOBAN from comu_interme_fin where num_cecoban<>0 and cve_interme_fin=cveBanco order by cve_interme;
					   cveBanco2=cuenta.substring(0, EIGlobal.NivelLog.DEBUG);
					   while(cveBanco2.startsWith("0"))
					     cveBanco=cveBanco2.substring(1,cveBanco2.length());
					   if(!numCECOBAN.equals(cveBanco2)	 )
						{
						  log("MAC_Registro - validaCuenta(): banco no corresponde con cuenta clabe", EIGlobal.NivelLog.INFO);
						  listaErrores.add("La cuenta claBE no corresponde al banco especificado: "+ cuenta);
						  err+=1;
						}*/
					}
			      break;
		  case CREDITO:
			      if( !(cuenta.length()==16 || cuenta.length()==15) )
			        {
			          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto ", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado: 02");
					  err+=1;
		            }
			      break;
		  /*case CHEQUES:
			      if(cuenta.length()!=11)
			        {
			          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto TDD ", EIGlobal.NivelLog.INFO);
					  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado: 04");
					  err+=1;
		            }
			      break;*/
			      
		  case MOVIL:
			  if( cuenta.length()!= 10 )
		        {
		          log("MAC_Registro - validaCuenta(): longitud de cuenta incorrecto ", EIGlobal.NivelLog.INFO);
				  listaErrores.add("La longitud de la cuenta ["+cuenta.length()+"] no corresponde con el tipo especificado");
				  err+=1;
	            }

			  break;
		  case 99:
			      break;
		}
	}

  /**
   * Calcula si el d&iacute;gito verificador de la <i>cuenta</i> es v&aacute;lido
   * @return <i>true</i> si el d&iacute;gito verificador es v&aacute;lida, en caso contrario devolver&aacute; <i>false</i>
   * */
  public boolean validaDigitoVerificador()
	{
	  String pesos="371371371371371371371371371";
	  String cuenta2=cuenta.substring(0,17);

	  int DC=0;

	  for(int i=0;i<cuenta2.length();i++)
        DC += (Integer.parseInt(cuenta2.substring(i,i+1))) * (Integer.parseInt(pesos.substring(i,i+1))) % 10;
	  DC = 10 - (DC % 10);
	  DC = (DC >= 10) ? 0 : DC;
	  cuenta2 += DC;

	  if(cuenta2.trim().equals(cuenta.trim()))
	    return true;
	  return false;
	}

  /**
   * Manda un mensaje a la respectiva bit&aacute;cora de la aplicaci&oacute;n
   * @param a		Mensaje a enviar
   * @param b		Nivel de severidad del mensaje del tipo {@link mx.altec.enlace.utilerias.EIGlobal.NivelLog}
   * */
   public void log(String a, EIGlobal.NivelLog b)
	{
	  //System.out.println(a + " <Tr> " + b);
	  EIGlobal.mensajePorTrace(a,b);
	}
}