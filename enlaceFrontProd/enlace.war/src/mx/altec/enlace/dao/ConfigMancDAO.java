package mx.altec.enlace.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import mx.altec.enlace.beans.CAMancomunidadSABean;
import mx.altec.enlace.beans.ConfigMancBean;
import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreTarjeta;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import oracle.jdbc.driver.OracleConnection;
import oracle.jdbc.driver.OraclePreparedStatement;



public class ConfigMancDAO
{
		/**
		 * Medio de referencia
		 */
		private static final String REFERENCIA_MEDIO = "REFERENCIA_MEDIO";
		/**
		 * Tipo destino de cuenta
		 */
		private static final String TIPO_DESTINO = "TIPO_DESTINO";
		/**
		 * timpo origen operacion 
		 */
		private static final String TIPO_ORIGEN = "TIPO_ORIGEN";
		/**
		 * plaza del contrato
		 */
		private static final String PLAZA = "PLAZA";
		/**
		 * titular del contrato
		 */
		private static final String TITULAR = "TITULAR";
		/**
		 * statement prepara una sentencia SQL
		 */
		private PreparedStatement statement =null;
		/**
		 * interna realiza una conexion SQL
		 */
		private Connection interna=null;
		/**
		 * query String sentencia SQL
		 */
		private String query = "";
		/**
		 * conn conexion oracle
		 */
		private OracleConnection conn = null;
		//private WSCallHelper helper = null;


		/**
		 * Ejecuta un sentencia SQL
		 * @return rs resultado
		 */
		private ResultSet Consulta()throws Exception{
			ResultSet rs = null;
			rs = statement.executeQuery();
			return rs;
		}
		
		/**
		 * Ejecuta una sentencia SQL de actualizacion 
		 * @return resultado resultado 
		 * @throws Exception exepcion generica
		 */
		private int Inserta()throws Exception{
			int resultado = 0;
			resultado = statement.executeUpdate();
			return resultado;
		}

		/**
		 * Valida si la operacion es mancomunada
		 * @param contrato contrato mancomunado
		 * @return res resultado
		 * @throws Exception excepcion generica
		 */
		public boolean esMancomunado(String contrato) throws Exception{
			EIGlobal.mensajePorTrace ("ConfigMancDAO - esMancomunado() - " +
					"Contrato: [" + contrato + "]", EIGlobal.NivelLog.INFO);
			ResultSet rs = null;
			boolean res = false;
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				query = "SELECT CONTRATO FROM TCT_MANC_CONTR " +
				"WHERE CONTRATO = '" + contrato + "'";

				statement = interna.prepareStatement(query);

				//OraclePreparedStatement ps2 = (OraclePreparedStatement) statement;

				//Pasar parametros al query
				//ps2.setFixedCHAR(1, contrato);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - esMancomunado() - " +
						"query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
				rs = statement.executeQuery();
				if(rs.next()) {
					res = true;
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - esMancomunado() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - esMancomunado() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - esMancomunado() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return res;
		}


		/**
		 * Invalida los usuarios 3 y 4
		 * @param folioArchivo se pasa el folio del archivo
		 * @return res resultado del query
		 * @throws Exception Excepcion Generica
		 */

		public boolean invalidaUsuarios(String folioArchivo) throws Exception{
			EIGlobal.mensajePorTrace ("ConfigMancDAO - invalidaUsuarios() - " +
					"Folio: [" + folioArchivo + "]", EIGlobal.NivelLog.INFO);
			boolean res = false;
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);

				query = "UPDATE TCT_MANC_ARCH SET USUARIO_AUT_2 = 'X', " +
						"USUARIO_AUT_3 = 'X', " +
						"USUARIO_AUT_4 = 'X' WHERE FOLIO_ARCHIVO = ?";

				statement = interna.prepareStatement(query);

				//Pasar parametros al query
				statement.setInt(1, Integer.parseInt(folioArchivo));

				EIGlobal.mensajePorTrace ("ConfigMancDAO - invalidaUsuarios() - " +
						"query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
				if(statement.executeUpdate() > 0) {
					res = true;
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - invalidaUsuarios() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - invalidaUsuarios() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - invalidaUsuarios() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return res;
		}


		/**
		 * Consulta configuracion de mancomunidad
		 * @param contrato mancomunado
		 * @param usuario usuario operante
		 * @return bean bean de datos
		 * @throws Exception excepcion generica
		 */

		public ConfigMancBean consultaConfigManc(String contrato,
				String usuario) throws Exception{
			EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc - " +
					"el Contrato es:" + contrato + ", usuario: " + usuario,
					EIGlobal.NivelLog.INFO);
			ResultSet rs = null;
			ConfigMancBean bean = new ConfigMancBean(contrato, usuario, "", "");
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				query = "SELECT TIPO_FIRMA, MONTO_AUTORIZADO FROM TCT_MANC_USU " +
				"WHERE CONTRATO = '" + contrato + "'  AND CVE_USUARIO = '" + usuario + "'";
				statement = interna.prepareStatement (query.toString());

				//OraclePreparedStatement ps2 = (OraclePreparedStatement) statement;

				//Pasar parametros al query
				//ps2.setFixedCHAR(1, contrato);
				//ps2.setFixedCHAR(2, usuario);


				EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc -" +
						" query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
				rs = Consulta();
				if(rs.next()) {
					bean.setFirma(rs.getString("TIPO_FIRMA"));
					bean.setLimite(rs.getString("MONTO_AUTORIZADO"));
					EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc, firma: "
							+ bean.getFirma() + ", limite: " + bean.getLimite(),
							EIGlobal.NivelLog.INFO);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc " +
						" Error: [" + e + "]",
						EIGlobal.NivelLog.INFO);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
					//bean = null;
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc - " +
							"Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.INFO);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - consultaConfigManc - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return bean;
		}


		/**
		 * Actualiza ConfigManc
		 * @param bean bean de datos
		 * @return inser actualizacion
		 * @throws Exception excepcion generica
		 */

		public int actualizaConfigManc(ConfigMancBean bean) throws Exception{
			EIGlobal.mensajePorTrace ("ConfigMancDAO - actualizaConfigManc - " +
					"el Contrato es:" + bean.getContrato() + ", usuario: " +
					bean.getUsuario(),
					EIGlobal.NivelLog.INFO);
			int inser = 0;
			try {
				query = "UPDATE TCT_MANC_USU SET TIPO_FIRMA = '" + bean.getFirma() + "' " +
				", MONTO_AUTORIZADO = '" + new BigDecimal(bean.getLimite()) + "'"  +
				" WHERE CONTRATO = '" + bean.getContrato() + "' AND CVE_USUARIO = '" + bean.getUsuario() + "'";
				EIGlobal.mensajePorTrace ("ConfigMancDAO - actualizaConfigManc -" +
						" query: ["	+ query + "]", EIGlobal.NivelLog.INFO);

				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				statement = interna.prepareStatement (query.toString());

				//OraclePreparedStatement ps2 = (OraclePreparedStatement) statement;

				//Pasar parametros al query
				//ps2.setFixedCHAR(1, bean.getFirma());
				//ps2.setBigDecimal(2, new BigDecimal(bean.getLimite()));
				//ps2.setFixedCHAR(3, bean.getContrato());
				//ps2.setFixedCHAR(4, bean.getUsuario());

				inser = Inserta();
				if (inser == 0) { // Valida si realiz� la actualizaci�n si no lo inserta
					query = "INSERT INTO TCT_MANC_USU VALUES (?, ?, ?, ?)";
					EIGlobal.mensajePorTrace ("ConfigMancDAO - actualizaConfigManc -" +
							" query: ["	+ query + "]", EIGlobal.NivelLog.INFO);
					statement = conn.prepareStatement (query.toString());

					OraclePreparedStatement ps3 = (OraclePreparedStatement) statement;

					//Pasar parametros al query
					ps3.setFixedCHAR(1, bean.getContrato());
					ps3.setFixedCHAR(2, bean.getUsuario());
					ps3.setFixedCHAR(3, bean.getFirma());
					ps3.setBigDecimal(4, new BigDecimal(bean.getLimite()));

					inser = Inserta();
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc - " +
						"Error : [" + e + "]",EIGlobal.NivelLog.INFO);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
					bean = null;
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - consultaConfigManc - " +
							"Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.INFO);
				}
			}
			return inser;
		}

		/**
		 * Elimina configuracion en mancomunidad
		 * @param contrato mancomunado
		 * @param usuario autorizador
		 * @throws Exception generica
		 */

		public void eliminaConfigManc(String contrato, String usuario) throws Exception{
			EIGlobal.mensajePorTrace ("ConfigMancDAO - eliminaConfigManc - " +
					"el Contrato es:" + contrato + ", usuario: " + usuario,
					EIGlobal.NivelLog.INFO);
			int inser = 0;
			try {
				query = "DELETE FROM TCT_MANC_USU WHERE CONTRATO = '" + contrato + "' AND CVE_USUARIO = '" + usuario + "'";
				EIGlobal.mensajePorTrace ("ConfigMancDAO - eliminaConfigManc -" +
						" query: ["	+ query + "]", EIGlobal.NivelLog.INFO);

				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				statement = interna.prepareStatement (query.toString());

				//OraclePreparedStatement ps2 = (OraclePreparedStatement) statement;

				//Pasar parametros al query
				//ps2.setFixedCHAR(1, contrato);
				//ps2.setFixedCHAR(2, usuario);

				inser = Inserta();
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - eliminaConfigManc - " +
						"Error : [" + e + "]",EIGlobal.NivelLog.INFO);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - eliminaConfigManc - " +
							"Error al cerrar conexion-> "+e1,EIGlobal.NivelLog.INFO);
				}
			}
		}

		/**
		 * Obtiene datos de archivos 
		 * @param mancomunidadSABean bean de operaciones mancomunadas
		 * @param contrato contrato operante de enlace
		 * @return listArchivos devuelve una lista de archivos
		 * @throws Exception Exepcion generica
		 */
		public ArrayList getDatosArchivo(CAMancomunidadSABean mancomunidadSABean, String contrato) throws Exception {
			ResultSet rs = null;
			ArrayList listArchivos = new ArrayList();
			Collection listNoRep = new HashSet();
			DatosMancSA beanTmp = null;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE3);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				query = "SELECT DISTINCT a.FECHA_REGISTRO, a.TIPO_OPERACION, a.FECHA_AUTORIZACION, a.FOLIO_ARCHIVO, a.REGISTROS, a.IMPORTE, " +
						"a.USUARIO_REG, a.USUARIO_AUT_2, a.USUARIO_AUT_3, a.USUARIO_AUT_4, a.ESTATUS, b.CTA_ORIGEN FROM TCT_MANC_ARCH a " +
						"INNER JOIN TCT_MANC_OPER b ON a.FOLIO_ARCHIVO = b.FOLIO_ARCHIVO " + filtroBusquedaArch(mancomunidadSABean, contrato);

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDatosArchivo() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);
				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();

					beanTmp.setFch_registro(rs.getDate("FECHA_REGISTRO")!=null ? formato.format(rs.getDate("FECHA_REGISTRO")) : "");
					beanTmp.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");
					beanTmp.setFch_autorizacion(rs.getDate("FECHA_AUTORIZACION")!=null ? formato.format(rs.getDate("FECHA_AUTORIZACION")) : "");
					beanTmp.setFolio_archivo(String.valueOf(rs.getInt("FOLIO_ARCHIVO")));
					beanTmp.setRegistros(String.valueOf(rs.getInt("REGISTROS")));
                    EIGlobal.mensajePorTrace ("1 importe Str->"+rs.getString("IMPORTE"),EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("1 importe Float->"+rs.getFloat("IMPORTE"),EIGlobal.NivelLog.INFO);
					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setUsrRegistro(rs.getString("USUARIO_REG")!=null ? rs.getString("USUARIO_REG") : "");
					beanTmp.setUsrAutoriza1(rs.getString("USUARIO_AUT_2")!=null ? rs.getString("USUARIO_AUT_2") : "");
					beanTmp.setUsrAutoriza2(rs.getString("USUARIO_AUT_3")!=null ? rs.getString("USUARIO_AUT_3") : "");
					beanTmp.setUsrAutoriza3(rs.getString("USUARIO_AUT_4")!=null ? rs.getString("USUARIO_AUT_4") : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS").trim() : "");
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN").trim() : "");

					if (listNoRep.add(beanTmp.getFolio_archivo())) {
						listArchivos.add(beanTmp);
					}
				}

				if (listArchivos.size() > 0) {

				}

			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDatosArchivo() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDatosArchivo() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDatosArchivo() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listArchivos;
		}
		
		/**
		 * Obtiene el detalle de un archivo de transferencias mismo banco
		 * @param folio folio de aarchivo
		 * @param contrato contato operante
		 * @return listDetalle de arhivo
		 * @throws Exception excepcion generica
		 */
		public ArrayList getDetalleTMB(String folio, String contrato) throws Exception {
			ResultSet rs = null;
			ArrayList listDetalle = new ArrayList();
			DatosMancSA beanTmp = null;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Collection listNoRep = new HashSet();

			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE3);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				//query = "SELECT DISTINCT a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.TIPO_OPERACION, a.CTA_ORIGEN, b.NOMBRE_TITULAR as TITULAR_ORIGEN, " +
				//		"a.CTA_DESTINO, c.NOMBRE_TITULAR as TITULAR_DESTINO, a.IMPORTE, a.FCH_AUTORIZAC, a.ESTATUS, a.FOLIO_ARCHIVO, " +
				query = "SELECT DISTINCT a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.TIPO_OPERACION, a.CTA_ORIGEN, b.N_DESCRIPCION as TITULAR_ORIGEN, " +
						"a.CTA_DESTINO, c.N_DESCRIPCION as TITULAR_DESTINO, a.IMPORTE, a.FCH_AUTORIZAC, a.ESTATUS, a.FOLIO_ARCHIVO, " +
						"a.TIPO_DESTINO, a.TIPO_ORIGEN, a.PLAZA, a.TITULAR, a.SUCURSAL, a.REFERENCIA_MEDIO, a.FCH_EJE_PROG, a.TIPO_OPER_PROG, " +
						"a.CONCEPTO, a.USUARIO1, a.USUARIO2, a.USUARIO3, a.USUARIO4 " +
						"FROM TCT_MANC_OPER a " +
						"inner join NUCL_RELAC_CTAS b on a.CTA_ORIGEN=b.NUM_CUENTA " +
						"inner join NUCL_RELAC_CTAS c on a.CTA_DESTINO=c.NUM_CUENTA " +
						"WHERE a.FOLIO_ARCHIVO = '" + folio + "' " +
						"AND b.NUM_CUENTA2 = '" + contrato + "' " +
						"AND c.NUM_CUENTA2 = '" + contrato + "' " +
						"ORDER BY a.FOLIO_REGISTRO ASC";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTMB() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();
					beanTmp.setFch_registro(rs.getDate("FCH_REGISTRO")!=null ? formato.format(rs.getDate("FCH_REGISTRO")) : "");
					beanTmp.setFolio_registro(String.valueOf(rs.getInt("FOLIO_REGISTRO")));
					beanTmp.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN") : "");
					beanTmp.setDescrip_cargo(rs.getString("TITULAR_ORIGEN")!=null ? rs.getString("TITULAR_ORIGEN") : "");
					beanTmp.setCta_abono(rs.getString("CTA_DESTINO")!=null ? rs.getString("CTA_DESTINO") : "");
					beanTmp.setDescrip_abono(rs.getString("TITULAR_DESTINO")!=null ? rs.getString("TITULAR_DESTINO") : "");
                    EIGlobal.mensajePorTrace ("2 importe Str->"+rs.getString("IMPORTE"),EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("2 importe Float->"+rs.getFloat("IMPORTE"),EIGlobal.NivelLog.INFO);

					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setFch_aplicacion(rs.getDate("FCH_AUTORIZAC")!=null ? formato.format(rs.getDate("FCH_AUTORIZAC")) : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS") : "");
					beanTmp.setTipo_destino(rs.getString(TIPO_DESTINO)!=null ? rs.getString(TIPO_DESTINO) : "");
					beanTmp.setTipo_origen(rs.getString(TIPO_ORIGEN)!=null ? rs.getString(TIPO_ORIGEN) : "");
					beanTmp.setPlaza(rs.getString(PLAZA)!=null ? rs.getString(PLAZA) : "");
					beanTmp.setTitular(rs.getString(TITULAR)!=null ? rs.getString(TITULAR) : "");
					beanTmp.setSucursal(rs.getString("SUCURSAL")!=null ? rs.getString("SUCURSAL") : "");
					beanTmp.setReferencia_medio(rs.getString(REFERENCIA_MEDIO)!=null ? rs.getString(REFERENCIA_MEDIO) : "");
					beanTmp.setFch_eje_prog(rs.getDate("FCH_EJE_PROG")!=null ? formato.format(rs.getDate("FCH_EJE_PROG")) : "");
					beanTmp.setTipo_oper_prog(rs.getString("TIPO_OPER_PROG")!=null ? rs.getString("TIPO_OPER_PROG") : "");
					beanTmp.setConcepto(rs.getString("CONCEPTO")!=null ? rs.getString("CONCEPTO") : "");
					beanTmp.setUsrRegistro(rs.getString("USUARIO1")!=null ? rs.getString("USUARIO1") : "");
					beanTmp.setUsrAutoriza1(rs.getString("USUARIO2")!=null ? rs.getString("USUARIO2") : "");
					beanTmp.setUsrAutoriza2(rs.getString("USUARIO3")!=null ? rs.getString("USUARIO3") : "");
					beanTmp.setUsrAutoriza3(rs.getString("USUARIO4")!=null ? rs.getString("USUARIO4") : "");

					if (listNoRep.add(beanTmp.getFolio_registro())) {
						listDetalle.add(beanTmp);
					}
				}


			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTMB() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTMB() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDetalleTMB() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listDetalle;
		}

		/**
		 * Detalle de un archivo de trasnferencias interbancarias
		 * @param folio de archivo
		 * @return listDetalle detalle de archivo
		 * @throws Exception exepcion generica
		 */
		public ArrayList getDetalleTI(String folio) throws Exception {
			ResultSet rs = null;
			ArrayList listDetalle = new ArrayList();
			DatosMancSA beanTmp = null;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Collection listNoRep = new HashSet();

			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE3);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				/*query = "SELECT FCH_REGISTRO, FOLIO_REGISTRO, CTA_ORIGEN, CTA_DESTINO, BENEFICIARIO, BANCO, IMPORTE, " +
						"CONCEPTO, INSTRUCCION, ESTATUS, TIPO_DESTINO, TIPO_ORIGEN, PLAZA, TITULAR, SUCURSAL, " +
						"REFERENCIA_MEDIO, FCH_EJE_PROG, TIPO_OPER_PROG, USUARIO1, USUARIO2, USUARIO3, USUARIO4 FROM TCT_MANC_OPER " +
						"WHERE FOLIO_ARCHIVO = '" + folio + "' ORDER BY FOLIO_REGISTRO ASC";*/

				query = "SELECT a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.CTA_ORIGEN, a.CTA_DESTINO, b.NOMBRE AS BENEFICIARIO, a.BANCO, a.IMPORTE, " +
				"a.CONCEPTO, a.INSTRUCCION, a.ESTATUS, a.TIPO_DESTINO, a.TIPO_ORIGEN, a.PLAZA, a.TITULAR, a.SUCURSAL, " +
				"a.REFERENCIA_MEDIO, a.FCH_EJE_PROG, a.TIPO_OPER_PROG, a.USUARIO1, a.USUARIO2, a.USUARIO3, a.USUARIO4 FROM TCT_MANC_OPER a " +
				"inner join NUCL_CTA_EXTERNA b on a.CTA_DESTINO=b.NUM_CUENTA_EXT AND a.CONTRATO = b.NUM_CUENTA " +
				"WHERE a.FOLIO_ARCHIVO = '" + folio + "' " +
				"ORDER BY a.FOLIO_REGISTRO ASC";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTI() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();
					beanTmp.setFch_registro(rs.getDate("FCH_REGISTRO")!=null ? formato.format(rs.getDate("FCH_REGISTRO")) : "");
					beanTmp.setFolio_registro(String.valueOf(rs.getInt("FOLIO_REGISTRO")));
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN") : "");
					beanTmp.setCta_abono(rs.getString("CTA_DESTINO")!=null ? rs.getString("CTA_DESTINO") : "");
					beanTmp.setBeneficiario(rs.getString("BENEFICIARIO")!=null ? rs.getString("BENEFICIARIO") : "");
					beanTmp.setBanco(rs.getString("BANCO")!=null ? rs.getString("BANCO") : "");
				    EIGlobal.mensajePorTrace ("3 importe Str->"+rs.getString("IMPORTE"),EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("3 importe Float->"+rs.getFloat("IMPORTE"),EIGlobal.NivelLog.INFO);
					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setConcepto(rs.getString("CONCEPTO")!=null ? rs.getString("CONCEPTO") : "");
					beanTmp.setRef_interbancaria(rs.getString("CONCEPTO")!=null ? rs.getString("CONCEPTO") : "");
					beanTmp.setForma_aplicacion(rs.getString("INSTRUCCION")!=null ? rs.getString("INSTRUCCION") : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS") : "");
					beanTmp.setTipo_destino(rs.getString(TIPO_DESTINO)!=null ? rs.getString(TIPO_DESTINO) : "");
					beanTmp.setTipo_origen(rs.getString(TIPO_ORIGEN)!=null ? rs.getString(TIPO_ORIGEN) : "");
					beanTmp.setPlaza(rs.getString(PLAZA)!=null ? rs.getString(PLAZA) : "");
					beanTmp.setTitular(rs.getString(TITULAR)!=null ? rs.getString(TITULAR) : "");
					beanTmp.setSucursal(rs.getString("SUCURSAL")!=null ? rs.getString("SUCURSAL") : "");
					beanTmp.setReferencia_medio(rs.getString(REFERENCIA_MEDIO)!=null ? rs.getString(REFERENCIA_MEDIO) : "");
					beanTmp.setFch_eje_prog(rs.getDate("FCH_EJE_PROG")!=null ? formato.format(rs.getDate("FCH_EJE_PROG")) : "");
					beanTmp.setTipo_oper_prog(rs.getString("TIPO_OPER_PROG")!=null ? rs.getString("TIPO_OPER_PROG") : "");

					beanTmp.setUsrRegistro(rs.getString("USUARIO1")!=null ? rs.getString("USUARIO1") : "");
					beanTmp.setUsrAutoriza1(rs.getString("USUARIO2")!=null ? rs.getString("USUARIO2") : "");
					beanTmp.setUsrAutoriza2(rs.getString("USUARIO3")!=null ? rs.getString("USUARIO3") : "");
					beanTmp.setUsrAutoriza3(rs.getString("USUARIO4")!=null ? rs.getString("USUARIO4") : "");
					beanTmp.setTipo_operacion("DIBT");

					if (listNoRep.add(beanTmp.getFolio_registro())) {
						listDetalle.add(beanTmp);
					}
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTI() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleTI() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDetalleTI() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listDetalle;
		}
		
		/**
		 * Arma el filtro de busqueda para archivos mancomunadoas
		 * @param bean bean de datos
		 * @param contrato contrato mancomunado
		 * @return cadenaFiltro filtro
		 */
		private String filtroBusquedaArch(CAMancomunidadSABean bean, String contrato) {
			StringBuffer sbFiltro = new StringBuffer();
			String cadenaFiltro = "";

			sbFiltro.append("WHERE B.CONTRATO = '" + contrato + "' AND ");

			if (!bean.getUsuario().equals("")) {
				sbFiltro.append("(b.USUARIO1 ='").append(bean.getUsuario()).append("' OR ");
				sbFiltro.append("b.USUARIO2 ='").append(bean.getUsuario()).append("' OR ");
				sbFiltro.append("b.USUARIO3 ='").append(bean.getUsuario()).append("' OR ");
				sbFiltro.append("b.USUARIO4 ='").append(bean.getUsuario()).append("') AND ");
			}
			if (!bean.getProducto().equals("0")) {
				sbFiltro.append("b.TIPO_OPERACION ='").append(bean.getProducto()).append("' AND ");
			}
			if (!bean.getImporte().equals("")) {
				sbFiltro.append("a.IMPORTE ='").append(bean.getImporte()).append("' AND ");
			}
			if (!bean.getChkArchivo().equals("") && !bean.getFolioArchivo().equals("")) {
				sbFiltro.append("a.FOLIO_ARCHIVO ='").append(bean.getFolioArchivo()).append("' AND ");
			}

			if (bean.getOpConsulta().equals("fchRegistro")) {
				sbFiltro.append("a.FECHA_REGISTRO >= TO_DATE('").append(bean.getFecha1()).append(" 00:00:00', 'dd/MM/yyyy HH24:MI:SS') ")
						.append("AND a.FECHA_REGISTRO <= TO_DATE('").append(bean.getFecha2()).append(" 23:59:59', 'dd/MM/yyyy HH24:MI:SS')");
			} else {
				sbFiltro.append("a.FECHA_AUTORIZACION >= TO_DATE('").append(bean.getFecha1()).append(" 00:00:00', 'dd/MM/yyyy HH24:MI:SS') ")
						.append("AND a.FECHA_AUTORIZACION <= TO_DATE('").append(bean.getFecha2()).append(" 23:59:59', 'dd/MM/yyyy HH24:MI:SS')");
			}
			if(!bean.getChkServicios().equals("")) {
				sbFiltro.append(" AND b.TIPO_OPERACION IN ('IN04','INTE','PNOS','PNLI')");
			}
			if(!bean.getCuenta().equals("")) {
				sbFiltro.append(" AND b.CTA_ORIGEN = '").append(bean.getCuenta()).append("'");
			}
			if (!bean.getChkAutorizadas().equals("") || !bean.getChkRechazadas().equals("") || !bean.getChkCanceladas().equals("") ||
				!bean.getChkMancomunada().equals("") || !bean.getChkVerificada().equals("") || !bean.getChkPendientes().equals("") ||
				!bean.getChkEjecutadas().equals("") || !bean.getChkNoEjecutadas().equals("") || !bean.getChkPreAutorizada().equals("")) {
				sbFiltro.append(" AND a.ESTATUS IN (");
			}

			if (!bean.getChkAutorizadas().equals("")) {
				sbFiltro.append("'A', ");
			}
			if (!bean.getChkRechazadas().equals("")) {
				sbFiltro.append("'R', ");
			}
			if (!bean.getChkCanceladas().equals("")) {
				sbFiltro.append("'C', ");
			}
			if (!bean.getChkMancomunada().equals("")) {
				sbFiltro.append("'M', ");
			}
			if (!bean.getChkVerificada().equals("")) {
				sbFiltro.append("'V', ");
			}
			if (!bean.getChkPendientes().equals("")) {
				sbFiltro.append("'P', ");
			}
			if (!bean.getChkEjecutadas().equals("")) {
				sbFiltro.append("'E', ");
			}
			if (!bean.getChkNoEjecutadas().equals("")) {
				sbFiltro.append("'N', ");
			}
			if (!bean.getChkPreAutorizada().equals("")) {
				sbFiltro.append("'U'");
			}

			cadenaFiltro = sbFiltro.toString().trim();

			if (cadenaFiltro.endsWith(",")) {
				cadenaFiltro = cadenaFiltro.substring(0, cadenaFiltro.length()-1);
			}
			if (cadenaFiltro.contains("ESTATUS IN")) {
				cadenaFiltro = cadenaFiltro.concat(")");
			}
//CSA 08/07/2016  HD1000000458951 
			cadenaFiltro = cadenaFiltro.concat(" AND A.USUARIO_REG in (SELECT CVE_USUARIO FROM SEGU_USRPERFTELE WHERE NUM_CUENTA = " +"'" + contrato + "')" 
			+" ORDER BY a.FOLIO_ARCHIVO ASC");

			return cadenaFiltro;
		}

		/**
		 * Obtiene los products a procesar
		 * @return listProductos lista de productos
		 */
		public ArrayList getProductos() {
			ResultSet rs = null;
			ArrayList listProductos = null;
			DatosMancSA beanDatos = null;

			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
				//conn = (OracleConnection) WSCallHelper.jdbcCall(OracleConnectionWrapper.class,interna,"unwrap",null,null);

				query = "SELECT DESCRIPCION, TIPO_OPERACION FROM TCT_TIPO_OPERACION WHERE TIPO_OPERACION IN('TRAN', 'DIBT', 'PAGT')";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getProductos() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				listProductos = new ArrayList();

				while(rs.next()) {
					beanDatos = new DatosMancSA();
					beanDatos.setEstatus(rs.getString("DESCRIPCION")!=null ? rs.getString("DESCRIPCION") : "");
					beanDatos.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");

					listProductos.add(beanDatos);
				}
				EIGlobal.mensajePorTrace("����������� listProductos.size ->" + listProductos.size(), EIGlobal.NivelLog.DEBUG);
			} catch (Exception e) {
				listProductos = null;
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getProductos() - " +
						"Error al cerrar conexion: ["+e + "]",EIGlobal.NivelLog.ERROR);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getProductos() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getProductos() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listProductos;
		}



	/**
	 * Consulta para saber si existen registros pendientes por autorizar.
	 * @param contrato - N&uacute;mero de contrato a buscar.
	 * @param fecha - Fecha de alta de los registros que se desean buscar.
	 * @param folio - Folio con el que fueron dados de alta los registros.
	 * @return true si existen registros por autorizar.
	 */
	public boolean getRegPorAut(String contrato, String fecha, String folio) {
		ResultSet rs = null;
		int cantidad = 0;
		boolean result = false;

		try {
			interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			query = "select count(1) as registros from tct_manc_oper where contrato = '" +
				contrato +
				"' and fch_registro  = to_date('" +
				fecha +
				"', 'dd/mm/yyyy')" +
				"and folio_archivo = '" +
				folio +
				"' and estatus in ('P','M','V','U')";

			statement = interna.prepareStatement(query);
			EIGlobal.mensajePorTrace (
					"ConfigMancDAO - getRegPorAut() - " +
					"query: ["	+ query + "]",
					EIGlobal.NivelLog.INFO
			);

			rs = Consulta();

			while(rs.next()) {
					cantidad = Integer.parseInt(rs.getString("registros"));
					if (cantidad > 0){
						result = true;
					}
			}

			EIGlobal.mensajePorTrace(
					"cantidad de registros [" + cantidad + "]",
					EIGlobal.NivelLog.DEBUG
			);
		}
		catch (Exception e) {
			EIGlobal.mensajePorTrace ("ConfigMancDAO - getRegPorAut() - " +
					"Error al cerrar conexion: ["+e + "]",EIGlobal.NivelLog.ERROR);
		}
		finally {
			try {
				if( statement!=null ) {
					statement.close();
					statement=null;
				}
				if(interna!=null){
					interna.close();
					interna=null;
				}
			}
			catch(Exception e1) {
				EIGlobal.mensajePorTrace (
						"ConfigMancDAO - getRegPorAut() - " +
						"Error al cerrar conexion: ["+e1 + "]",
						EIGlobal.NivelLog.ERROR);
			}
			EIGlobal.mensajePorTrace(
					"ConfigMancDAO - getRegPorAut() - fin",
					EIGlobal.NivelLog.INFO
			);
		}
		return result;
	}



		/**
		 * metodo para obtener el detalle de nomina Tradicional e inmediata
		 * Modificacion Mancomunidad Fase II LFER
		 * @param folio
		 * @return ArrayList
		 * @throws Exception
		 */
		public ArrayList<DatosMancSA> getDetalleNomInmTrad(String folio, String fecha) throws Exception {
			ResultSet rs = null;
			ArrayList<DatosMancSA> listDetalle = new ArrayList<DatosMancSA>();
			DatosMancSA beanTmp = null;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			NomPreBusquedaAction npbAction = new NomPreBusquedaAction();
			NomPreLM1D datosEmpleados = null;
			ArrayList resDatos = null;
			String beneficiario = "";
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);

/*				query = "SELECT a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.FOLIO_ARCHIVO,a.cta_origen,a.beneficiario,a.titular,a.cta_destino,a.importe," +
						" b.nombre_corto BANCO,b.num_banxico PLAZA,a.tipo_operacion,a.estatus, a.contrato from tct_manc_oper a "+
						",COMU_INTERME_FIN b where b.cve_interme(+)=a.banco "+
						"and a.FOLIO_ARCHIVO = '" + folio +
						"'and a.fch_registro  = to_date('" + fecha +	"', 'dd/mm/yyyy')" +
				"ORDER BY a.FOLIO_REGISTRO ASC";*/

				query = "SELECT b.NUM_EMPL, a.TITULAR, a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.FOLIO_ARCHIVO, a.cta_origen, " +
                        "a.cta_destino, a.importe, a.tipo_operacion, a.estatus, a.contrato, c.APELL_PATERNO, " +
                        "c.APELL_MATERNO, c.NOMBRE " +
                        "from tct_manc_oper a, EWEB_DET_CAT_NOM@LWEBGFSS b,EWEB_CAT_NOM_EMPL@LWEBGFSS c " +
                        "where b.CUENTA_ABONO(+) = a.cta_destino and c.CUENTA_ABONO(+) = a.cta_destino and  b.contrato(+) = a.contrato " +
                        "and a.FOLIO_ARCHIVO = '" + folio +"' and a.fch_registro  = to_date('" + fecha +"', 'dd/mm/yyyy') " +
                        "ORDER BY a.FOLIO_REGISTRO ASC";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNomInmTrad() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();
					beanTmp.setFch_registro(rs.getDate("FCH_REGISTRO")!=null ? formato.format(rs.getDate("FCH_REGISTRO")) : "");
					beanTmp.setFolio_registro(String.valueOf(rs.getInt("FOLIO_REGISTRO")));
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN") : "");

					if(rs.getString("APELL_PATERNO") != null && rs.getString("APELL_MATERNO")
					   != null && rs.getString("NOMBRE") != null)
						beneficiario = rs.getString("NOMBRE")+"@"+rs.getString("APELL_PATERNO")+"@"+rs.getString("APELL_MATERNO");
					else
						beneficiario = "";
					beanTmp.setBeneficiario(beneficiario);
					beanTmp.setTitular(rs.getString("NUM_EMPL")!=null ? rs.getString("NUM_EMPL") : "");
					beanTmp.setCta_abono(rs.getString("CTA_DESTINO")!=null ? rs.getString("CTA_DESTINO") : "");
					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS") : "");
					beanTmp.setFolio_archivo(rs.getString("FOLIO_ARCHIVO")!=null ? rs.getString("FOLIO_ARCHIVO") : "");
					beanTmp.setContrato(rs.getString("CONTRATO")!=null ? rs.getString("CONTRATO") : "");
					listDetalle.add(beanTmp);
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNomInmTrad() " +
							"beanTmp.getBeneficiario antes: [" + beanTmp.getBeneficiario() + "]", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNomInmTrad() " +
							"beanTmp.getBeneficiario replaceAll: [" + beanTmp.getBeneficiario().replaceAll("@", "").trim() + "]", EIGlobal.NivelLog.INFO);
					if (beanTmp.getTipo_operacion().equals("PNOS")) {
		                    EIGlobal.mensajePorTrace ("ConfigMancDAO - va por los nombres LM1D", EIGlobal.NivelLog.INFO);
		        			datosEmpleados = npbAction.obtenRelacionTarjetaEmpleado(beanTmp.getContrato(), null, null, null);
		        			EIGlobal.mensajePorTrace ("ConfigMancDAO - regresa con los nombres LM1D", EIGlobal.NivelLog.INFO);
		        			if (datosEmpleados.isCodExito() && datosEmpleados.getDetalle() != null) {
		        				resDatos = new ArrayList(datosEmpleados.getDetalle());
		        			}
		        			if (resDatos != null) {
		        				for (int i = 0; i < resDatos.size(); i ++){
		        					Relacion relacionTE = (Relacion)resDatos.get(i);
		        					NomPreTarjeta tarjetaRes = relacionTE.getTarjeta();
		        					if (tarjetaRes.getNoTarjeta().trim().equals(beanTmp.getCta_abono().trim())) { //Es la misma tarjeta
		        						NomPreEmpleado empleadoRes = relacionTE.getEmpleado();
		        						beanTmp.setTitular(empleadoRes.getNoEmpleadoFrm());
		        						beanTmp.setBeneficiario(empleadoRes.getNombreFrm() + "@" + empleadoRes.getPaternoFrm() + "@" + empleadoRes.getMaternoFrm());
		        						break;
		        					}
		        				}
		        			}
	        			EIGlobal.mensajePorTrace ("ConfigMancDAO - - getDetalleNomInmTrad()" +
	        					"descripcion de LM1D: [" + beanTmp.getBeneficiario() + "]", EIGlobal.NivelLog.INFO);
					}
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNomInmTrad() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNomInmTrad() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDetalleNomInmTrad() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listDetalle;
		}



		/**
		 * metodo para obtener el detalle de nomina Interbancaria
		 * Modificacion Mancomunidad Fase II LFER
		 * @param folio
		 * @return ArrayList
		 * @throws Exception
		 */
		public ArrayList<DatosMancSA> getDetalleInterbancaria(String folio, String fecha) throws Exception {
			ResultSet rs = null;
			ArrayList<DatosMancSA> listDetalle = new ArrayList<DatosMancSA>();
			DatosMancSA beanTmp = null;

			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);

				query = "SELECT a.FCH_REGISTRO, a.FOLIO_REGISTRO,a.FOLIO_ARCHIVO,a.cta_origen,a.beneficiario,a.tipo_destino,a.cta_destino,a.importe," +
						" b.nombre_corto BANCO,a.plaza PLAZA, a.tipo_operacion ,a.estatus"+
						" FROM tct_manc_oper a ,COMU_INTERME_FIN b "+
						" WHERE a.banco(+)=b.cve_interme"+
						" and a.FOLIO_ARCHIVO = '" + folio +
						"' and a.fch_registro  = to_date('" + fecha +	"', 'dd/mm/yyyy')" +
						"ORDER BY a.FOLIO_REGISTRO ASC";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleInterbancaria() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();
					beanTmp.setFch_registro(rs.getDate("FCH_REGISTRO")!=null ? formato.format(rs.getDate("FCH_REGISTRO")) : "");
					beanTmp.setFolio_registro(String.valueOf(rs.getInt("FOLIO_REGISTRO")));
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN") : "");
					beanTmp.setBeneficiario(rs.getString("BENEFICIARIO")!=null ? rs.getString("BENEFICIARIO") : "");
					beanTmp.setTipo_origen(rs.getString(TIPO_DESTINO)!=null ? rs.getString(TIPO_DESTINO) : "");
					beanTmp.setCta_abono(rs.getString("CTA_DESTINO")!=null ? rs.getString("CTA_DESTINO") : "");
					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setBanco(rs.getString("BANCO")!=null ? rs.getString("BANCO") : "");
					beanTmp.setPlaza(rs.getString(PLAZA)!=null ? rs.getString(PLAZA) : "");
					beanTmp.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS") : "");
					beanTmp.setFolio_archivo(rs.getString("FOLIO_ARCHIVO")!=null ? rs.getString("FOLIO_ARCHIVO") : "");
					listDetalle.add(beanTmp);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleInterbancaria() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleInterbancaria() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDetalleInterbancaria() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listDetalle;
		}

		/**
		 * metodo para obtener el detalle de nomina En linea de webgfss y gfi
		 * Modificacion Mancomunidad Fase II LFER
		 * @param folio
		 * @return ArrayList
		 * @throws Exception
		 */
		public ArrayList<DatosMancSA> getDetalleNominaLinea(String folio, String fecha) throws Exception {
			ResultSet rs = null;
			ArrayList<DatosMancSA> listDetalle = new ArrayList<DatosMancSA>();
			DatosMancSA beanTmp = null;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			String beneficiario = null;
			try {
				interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);

				query = "SELECT b.NUM_EMPL, a.FCH_REGISTRO, a.FOLIO_REGISTRO, a.FOLIO_ARCHIVO, a.CTA_ORIGEN, " +
		                "a.CTA_DESTINO, a.IMPORTE, a.TIPO_OPERACION, a.ESTATUS, a.contrato, c.APELL_PATERNO, " +
		                "c.APELL_MATERNO, c.NOMBRE " +
		                "from tct_manc_oper a, EWEB_DET_CAT_NOM@LWEBGFSS b,EWEB_CAT_NOM_EMPL@LWEBGFSS c " +
		                "where b.CUENTA_ABONO(+) = a.cta_destino and c.CUENTA_ABONO(+) = a.cta_destino and  b.contrato(+) = a.contrato " +
		                "and a.FOLIO_ARCHIVO = '" + folio +"' and a.fch_registro  = to_date('" + fecha +"', 'dd/mm/yyyy') " +
		                "ORDER BY a.FOLIO_REGISTRO ASC";

				statement = interna.prepareStatement(query);

				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNominaLinea() - " + "query: ["	+ query + "]",
						EIGlobal.NivelLog.INFO);

				rs = Consulta();

				while(rs.next()) {
					beanTmp = new DatosMancSA();
					beanTmp.setTitular(rs.getString("NUM_EMPL")!=null ? rs.getString("NUM_EMPL") : "");
					beanTmp.setFch_registro(rs.getDate("FCH_REGISTRO")!=null ? formato.format(rs.getDate("FCH_REGISTRO")) : "");
					beanTmp.setFolio_registro(String.valueOf(rs.getInt("FOLIO_REGISTRO")));
					beanTmp.setFolio_archivo(rs.getString("FOLIO_ARCHIVO")!=null ? rs.getString("FOLIO_ARCHIVO") : "");
					beanTmp.setCta_cargo(rs.getString("CTA_ORIGEN")!=null ? rs.getString("CTA_ORIGEN") : "");
					if(rs.getString("APELL_PATERNO") != null && rs.getString("APELL_MATERNO")
							   != null && rs.getString("NOMBRE") != null)
								beneficiario = rs.getString("NOMBRE")+"@"+rs.getString("APELL_PATERNO")+"@"+rs.getString("APELL_MATERNO");
							else
								beneficiario = "";
							beanTmp.setBeneficiario(beneficiario);
					beanTmp.setCta_abono(rs.getString("CTA_DESTINO")!=null ? rs.getString("CTA_DESTINO") : "");
					beanTmp.setImporte(rs.getString("IMPORTE"));
					beanTmp.setTipo_operacion(rs.getString("TIPO_OPERACION")!=null ? rs.getString("TIPO_OPERACION") : "");
					beanTmp.setEstatus(rs.getString("ESTATUS")!=null ? rs.getString("ESTATUS") : "");
					beanTmp.setContrato(rs.getString("CONTRATO")!=null ? rs.getString("CONTRATO") : "");
					listDetalle.add(beanTmp);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNominaLinea() " +
						"Error: [" + e + "]", EIGlobal.NivelLog.ERROR);
				throw new Exception (e);
			} finally {
				try{
					if(statement!=null){
						statement.close();
						statement=null;}
					if(interna!=null){
						interna.close();
						interna=null;}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("ConfigMancDAO - getDetalleNominaLinea() - " +
						"Error al cerrar conexion: ["+e1 + "]",EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace( "ConfigMancDAO - getDetalleNominaLinea() - fin"
						, EIGlobal.NivelLog.INFO);
			}
			return listDetalle;
		}
    /**
     * Actualiza el Estatus de Una operacion cuando la validacion de linea de captura es erronea
     * @param folioReg folio de registro
     * @param tipoOper tipo de operacion enviada
     * @param usrAut usuario que autoriza
     * @param msgErrorLC mensaje de erro de la linea de captura
     * @param contrato contrato en el que se autoriza la operacion 
     * @param fchRegistro fecha de registro de la operacion 
     * @param fchAut fecha de autorizacion 
     * @return true o false
     */
	public Boolean actEstatusOperacion(String folioReg, String  tipoOper, String usrAut, String msgErrorLC, String contrato, String fchRegistro, String fchAut) {
		int actualizados = 0;
		String nUsr;

		nUsr = getNUsr(folioReg, contrato, fchRegistro);

		try{
			interna = BaseServlet.createiASConn_static (Global.DATASOURCE_ORACLE);
			interna.setAutoCommit(true);
			String qryUpd = prepUpdMncOpr(fchAut, usrAut, "N", msgErrorLC, contrato, fchRegistro, folioReg, nUsr);
			EIGlobal.mensajePorTrace("Antes de ejecutar: " + qryUpd,EIGlobal.NivelLog.DEBUG);
			statement = interna.prepareStatement( qryUpd );
			actualizados = statement.executeUpdate();
			EIGlobal.mensajePorTrace("Registros Actualizados:" + actualizados,EIGlobal.NivelLog.DEBUG);
			
		} catch(SQLException e) {
			EIGlobal.mensajePorTrace("actEstatusOperacion Update Error >>"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("actEstatusOperacion Update Error:"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (statement!=null)
				{
					statement.close();
					statement=null;
				}
				if (interna!=null)
				{
					interna.close();
					interna=null;
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("actEstatusOperacion:"+e.getMessage(),EIGlobal.NivelLog.ERROR);
			}
		}
		if(actualizados > 0) {
			return true;
		} 
		
		return false;
		
	}
    
	/**
	 *  Obtiene el usuario que va a autorizar
	 * @param folReg folio de registro de la operacion
	 * @param contrato contrato donde se autoriza la operacion 
	 * @param fchReg fecha de registro
	 * @return nUsr nuemro de usuario
	 */
	public String getNUsr(String folReg, String contrato, String fchReg)
	{
		String nUsr = "";
		ResultSet rs = null;

		try{
			interna = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			String qrySel = prepSelUsrActMncOpr(folReg, contrato, fchReg);
			EIGlobal.mensajePorTrace("getNUsr Antes de ejecutar: " + qrySel,EIGlobal.NivelLog.DEBUG);
			statement = interna.prepareStatement(qrySel);
			rs = Consulta();
			if(rs.next()) {
				nUsr = rs.getString("USUARIO");
			}
			EIGlobal.mensajePorTrace("getNUsr Resultado: " + nUsr,EIGlobal.NivelLog.DEBUG);
			
		} catch(SQLException e) {
			EIGlobal.mensajePorTrace("getNUsr select Error >>"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("getNUsr select Error:"+e.getMessage(),EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (statement!=null)
				{
					statement.close();
					statement=null;
				}
				if (interna!=null)
				{
					interna.close();
					interna=null;
				}
				if(rs!=null)
				{
					rs.close();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("consultaCfrmMigUsr:"+e.getMessage(),EIGlobal.NivelLog.ERROR);
			}
		}
		return nUsr;
	}
	
	/**
	 * Prepara usuario que actualiza el estado de la operacion 
	 * @param folReg folio de registro de la operacion 
	 * @param contrato contrato de la operacion
	 * @param fchReg  fecha de registro de la operacion
	 * @return query resultado
	 */
	public String prepSelUsrActMncOpr(String folReg, String contrato, String fchReg)
	{
		StringBuffer query = new StringBuffer();

		query.append("select 'USUARIO' || to_char(")
				.append("(5 - (nvl2(USUARIO2,0,1) + nvl2(USUARIO3,0,1) + nvl2(USUARIO4,0,1) + DECODE(USUARIO3,'0000000',1,0) + DECODE(USUARIO4,'0000000',1,0)))) ")
				.append("as USUARIO ")
				.append("from  TCT_MANC_OPER ")
				.append("where CONTRATO = '").append(contrato).append("' ")
				.append("and FOLIO_REGISTRO = ").append(folReg)
				.append("and FCH_REGISTRO = to_date('").append(fchReg).append("','dd/mm/yyyy') ")
				.append("and rownum=1");

		return query.toString();
	}

	/**
	 * Prepara el Update para actualizacion de estatus de la operaci�n
	 * @param fchAut fecha autorizacion
	 * @param usuario usuario autiruzador
	 * @param estado estatis de operacion 
	 * @param msgErr mensaje de error
	 * @param contrato contrato en que se ejecuta la operacin
	 * @param fchReg fecha de registro de la operacion
	 * @param folReg folio de registro de la operacion
	 * @param nUsr numero de usuario autorizador
	 * @return query resultado
	 */
	public String prepUpdMncOpr(String fchAut, String usuario, String estado, String msgErr, String contrato, String fchReg, String folReg, String nUsr)
	{
		StringBuffer query = new StringBuffer();

		query.append("update TCT_MANC_OPER ")
				.append("set FCH_AUTORIZAC = to_date('").append(fchAut).append("','dd/mm/yyyy'), ")
				.append(nUsr).append(" = '").append(usuario).append("', ")
				.append("ESTATUS = '").append(estado).append("', ")
				.append("MSG_ERROR = '").append(msgErr).append("' ")
				.append("where CONTRATO = '").append(contrato).append("' ")
				.append("and FCH_REGISTRO = to_date('").append(fchReg).append("','dd/mm/yyyy') ")
				.append("and FOLIO_REGISTRO = ").append(folReg);

		return query.toString();
	}

}