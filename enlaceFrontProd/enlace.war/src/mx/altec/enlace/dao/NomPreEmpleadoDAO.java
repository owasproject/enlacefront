package mx.altec.enlace.dao;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1F;
import mx.altec.enlace.beans.NomPreLM1E;
import mx.altec.enlace.beans.NomPreCodPost;

import static mx.altec.enlace.utilerias.NomPreUtil.*;
import static mx.altec.enlace.beans.NomPreTarjeta.LNG_NO_TARJETA;
import static mx.altec.enlace.beans.NomPreEmpleado.LNG_NO_EMPLEADO;

public class NomPreEmpleadoDAO extends NomPreMQDAO {

	//TRANS: LM1E
	private NomPreLM1E ejecutaLM1EBase(String operacion, String noContrato,
			String noTarjeta, NomPreEmpleado empleado) {

		StringBuffer sb = new StringBuffer()
			.append(rellenar(operacion, 1))					//COD-ACCION
			.append(rellenar(noContrato, 11))				//NUM-CONTRATO
			.append(rellenar(noTarjeta, LNG_NO_TARJETA))	//NUM-TARJETA
			.append(empleado.getNombreFrm())				//NOM-EMPLEADO
			.append(empleado.getPaternoFrm())				//APE-PATERNO
			.append(empleado.getMaternoFrm())				//APE-MATERNO
			.append(empleado.getRfcFrm())					//COD-RFC
			.append(empleado.getHomoclaveFrm())				//COD-HOMOCLAVE
			.append(empleado.getNumIDFrm())					//NUM-ID VSWF ESC nuevos campos NP
			//.append(empleado.getTipoIDFrm())				//TIPO-ID VSWF ESC se comenta codigo tipo id
			.append(empleado.getNoEmpleadoFrm())			//COD-EMPLEADO
			.append(empleado.getFechaNacimientoFrm())		//FEC-NACIMIENTO
			.append(empleado.getSexoFrm())					//IND-SEXO
			.append(empleado.getNacionalidadFrm())			//COD-NACION
			.append(empleado.getPaisFrm())					//COD-PAIS VSWF ESC nuevos campos NP
			.append(empleado.getEstadoCivilFrm())			//COD-EST-CIVIL
			.append(empleado.getCorreoElectronicoFrm())		//DES-MAIL
			.append(empleado.getCalleFrm())					//DES-DOMICILIO
			.append(empleado.getNumeroFrm())				//NUM-EXTERIOR
			.append(empleado.getNumIntFrm())				//NUM-INTERIOR VSWF ESC nuevos campos NP
			.append(empleado.getColoniaFrm())				//DES-COLONIA
			.append(empleado.getDelegacionFrm())			//DES-DELEGACION
			.append(empleado.getCiudadFrm())				//DES-CIUDAD   
			.append(empleado.getEstadoFrm())				//COD-ESTADO   
			.append(empleado.getCodigoPostalFrm())			//COD-POSTAL  
			.append(empleado.getLadaFrm())					//COD-LADA
			.append(empleado.getTelefonoFrm());				//COD-TELEFONO

		return consulta(NomPreLM1E.HEADER, sb.toString(), NomPreLM1E.getFactoryInstance());
	}


	//TRANS: LM1E
	public NomPreLM1E actualizaDatosDemograficos(String noContrato,
			String noTarjeta, NomPreEmpleado empleado) {
		// M - Modificación de datos demográficos

		logInfo("LM1E/M - Actualiza datos demograficos");

		return ejecutaLM1EBase("M", noContrato, noTarjeta, empleado);
	}

	//TRANS: LM1E
	public NomPreLM1E agregarEmpleado(String noContrato, String noTarjeta,
			NomPreEmpleado empleado) {
		// A - Alta de Empleado

		logInfo("LM1E/A - Alta de empleado");

		return ejecutaLM1EBase("A", noContrato, noTarjeta, empleado);
	}

	//TRANS: LM1F
	public NomPreLM1F consultarDatosDemograficos(String noContrato, String noEmpleado, String noTarjeta) {

		StringBuffer sb = new StringBuffer()
			.append(rellenar(noContrato, 11))					//NUM-CONTRATO
			.append(rellenar(noEmpleado, LNG_NO_EMPLEADO))	//COD-EMPLEADO
			.append(rellenar(noTarjeta, LNG_NO_TARJETA));	//NUM-TARJETA

		logInfo("LM1F - Consulta de datos demograficos");

		return consulta(NomPreLM1F.HEADER, sb.toString(), NomPreLM1F.getFactoryInstance());
	}
	
	//VSWF ESC Consulta codigo postal 
	public NomPreCodPost consultarCodigoPostal(String codPostal) {	
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(codPostal, 8, '0', 'I'))				//NUM-CONTRATO
			.append("00001");										//NUMERO CONSECUTIVO  
		
		logInfo("TC73 - Consulta de codigo postal");
		
		return consulta(NomPreCodPost.HEADER, sb.toString(), NomPreCodPost.getFactoryInstance());
	}
}