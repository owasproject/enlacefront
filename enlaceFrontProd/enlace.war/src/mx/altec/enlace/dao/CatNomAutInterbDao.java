package mx.altec.enlace.dao;

import java.sql.*;
import javax.sql.*;
import java.io.*;
import java.util.ArrayList;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bo.CatNomAutIntbBean;

public class CatNomAutInterbDao
{
		private PreparedStatement statement =null;
		private Connection conn=null;
		private PreparedStatement statement2 =null;
		private Connection conn2=null;
		String query;


	public ArrayList<CatNomAutIntbBean> obtenSolicitudes(String contrato,
														 String usuario,
														 String tipoConsulta,
														 String fechaIni,
														 String fechaFin,
														 String folioReg,
														 String cuenta,
														 String tipoEstatus,
														 String operacion) {

		ArrayList<CatNomAutIntbBean> lista = new ArrayList<CatNomAutIntbBean>();
		query = "select ESTATUS_OPER, NUM_CUENTA_EXT, TITULAR, FCH_ENV, FCH_AUT, FOLIO_ENV, NUM_PERSONA_ENV, NUM_PERSONA_AUT, TIPO_SOLIC, to_number(substr(num_cuenta_ext, 1, 3)) as CVE_BANCO, CVE_INTERME, FOLIO_AUT, OBSERVACIONES FROM EWEB_CAT_NOM_AUT_EXT WHERE NUM_CUENTA2 = '" + contrato + "' ";

		if(usuario != null) {
			query += "AND NUM_PERSONA_ENV = '" + usuario + "' ";
		}
		if(tipoConsulta != null) {
			if(tipoConsulta.equals("R")) {
				query += "AND FCH_ENV BETWEEN to_date('" + fechaIni + "', 'yyyymmdd') AND to_date('" + fechaFin + "', 'yyyymmdd') + 1 ";
			}
			else {
				query += "AND FCH_AUT BETWEEN to_date('" + fechaIni + "', 'yyyymmdd') AND to_date('" + fechaFin + "', 'yyyymmdd') + 1 ";
			}
		}
		if(folioReg != null) {
			query += "AND FOLIO_ENV = '" + folioReg + "' ";
		}
		if(cuenta != null) {
			query += "AND NUM_CUENTA_EXT = '" + cuenta + "' ";
		}
		if(tipoEstatus != null && !tipoEstatus.trim().equals("")) {
			    query += "AND ESTATUS_OPER in (" + tipoEstatus + ") ";
		}
		if(operacion != null) {
			query += "AND TIPO_SOLIC = '" + operacion + "' ";
		}

		ResultSet rs = null;

		try
		{
			EIGlobal.mensajePorTrace( "CatNomAutInterbDao query: <" + query + ">", EIGlobal.NivelLog.INFO);
			Crear(query);

			rs = Consulta();
			CatNomAutIntbBean bean = null;
			CatNominaInterbDAO daoBanco= new CatNominaInterbDAO();
			while (rs.next()) {
				bean = new CatNomAutIntbBean();

				bean.setFolioAut(rs.getInt("FOLIO_AUT"));
				bean.setFolioReg(rs.getInt("FOLIO_ENV"));
				if (rs.getTimestamp("FCH_AUT") != null)
					bean.setFechaAut(rs.getTimestamp("FCH_AUT").getTime());
				if (rs.getTimestamp("FCH_ENV") != null)
					bean.setFechaReg(rs.getTimestamp("FCH_ENV").getTime());
				bean.setObservaciones(rs.getString("OBSERVACIONES"));
				//bean.setBanco(rs.getString("NOMBRE_CORTO"));
				bean.setBanco(daoBanco.obtenNombreBanco(rs.getString("CVE_INTERME")));
				//query anidado para obtener nombre corto
				if(rs.getString("NUM_PERSONA_AUT") != null) {
					bean.setUsrAut(rs.getString("NUM_PERSONA_AUT").trim());
				}
				if(rs.getString("NUM_PERSONA_ENV") != null) {
					bean.setUsrReg(rs.getString("NUM_PERSONA_ENV").trim());
				}
				bean.setNombre(rs.getString("TITULAR"));
				bean.setCuenta(rs.getString("NUM_CUENTA_EXT"));
				bean.setOperacion(rs.getString("TIPO_SOLIC").charAt(0));
				bean.setEstatus(rs.getString("ESTATUS_OPER").charAt(0));
				lista.add(bean);
				EIGlobal.mensajePorTrace( "Se agrego BEAN a la lista...", EIGlobal.NivelLog.DEBUG);
			}
			daoBanco = null;
			EIGlobal.mensajePorTrace( "Contenido de la lista: " + lista.toString(), EIGlobal.NivelLog.DEBUG);

		}
		catch (Exception e) {
			e.printStackTrace();
			lista = null;
			EIGlobal.mensajePorTrace( "Error en consulta de Oracle.", EIGlobal.NivelLog.ERROR);
		}
		finally{
				try{
					if(rs!=null){
						rs.close();
						rs=null;}
					if(statement!=null){
						statement.close();
						statement=null;}
					if(conn!=null){
						conn.close();
						conn=null;}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
					}
				}
		return lista;
	}


	public int[] autorizaSolicitudes(ArrayList<CatNomAutIntbBean> lista, String estatus, String contrato, String archErr) {
		CatNomAutIntbBean bean = null;
		int respuesta[] = new int[3];
		//0 exitosos, 1 errores, 2 totales
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(archErr));
			out.println("CUENTA;TIPO;MENSAJE;");
			for(int i = 0; i < lista.size(); i++) {
				bean = lista.get(i);
				respuesta[2]++;
				if(estatus.equals("P")) {	//AUTORIZACION
					switch(bean.getEstatus()) {
						case 'I':	//Pendiente por Autorizar
									if(bean.getOperacion() == 'A') {
										query = "UPDATE EWEB_CAT_NOM_AUT_EXT SET ESTATUS_OPER='" + estatus + "', FOLIO_AUT=EWEB_SEQ_CAT_NOM_EXT.NEXTVAL, FCH_AUT=sysdate, NUM_PERSONA_AUT='" + bean.getUsrAut() + "', OBSERVACIONES='SOLICITUD AUTORIZADA' " +
												"WHERE FOLIO_ENV='" + bean.getFolioReg() + "'";
												out.println(bean.getCuenta() + ";E;SOLICITUD AUTORIZADA;");
									}
									else {
										query = "UPDATE EWEB_CAT_NOM_AUT_EXT SET ESTATUS_OPER='A', FOLIO_AUT=EWEB_SEQ_CAT_NOM_EXT.NEXTVAL, FCH_AUT=sysdate, NUM_PERSONA_AUT='" + bean.getUsrAut() + "', OBSERVACIONES='CUENTA DADA DE BAJA' " +
												"WHERE FOLIO_ENV='" + bean.getFolioReg() + "'";
												out.println(bean.getCuenta() + ";E;CUENTA DADA DE BAJA;");
									}
									try {
										Crear(query);
										if(Ejecuta() == 1){
											respuesta[0]++;
											bean.setEstatus(estatus.charAt(0));
										}
										else {
											respuesta[1]++;
										}
									}
									catch (Exception e) {
										e.printStackTrace();
										respuesta[1]++;
										EIGlobal.mensajePorTrace( "Error en consulta de Oracle.", EIGlobal.NivelLog.ERROR);
									}
									finally{
										try {
											if(statement!=null){
												statement.close();
												statement=null;}
											if(conn!=null){
												conn.close();
												conn=null;}
										} catch(Exception e1){
												EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
										}
									}
									if(bean.getOperacion() == 'B') {
										query = "DELETE FROM EWEB_CAT_NOM_EXTERNA " +
												"WHERE NUM_CUENTA2='" + contrato + "' " +
												"AND NUM_CUENTA_EXT='" + bean.getCuenta() + "'";
										try {
											Crear(query);
											Ejecuta();
										}
										catch (Exception e) {
											e.printStackTrace();
											EIGlobal.mensajePorTrace( "Error en consulta de Oracle.", EIGlobal.NivelLog.ERROR);
										}
										finally{
											try {
												if(statement!=null){
													statement.close();
													statement=null;}
												if(conn!=null){
													conn.close();
													conn=null;}
											} catch(Exception e1){
													EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
											}
										}
									}
									break;
						case 'C':	//Cancelada
						case 'A':	//Activa
						case 'P':	//Pendiente por Activar
						default:	//Status Erroneo
									respuesta[1]++;
									break;
					}
				}
				else {	//CANCELACION
					switch(bean.getEstatus()) {
						case 'I':	//Pendiente por Autorizar
						case 'P':	//Pendiente por Activar
									query = "UPDATE EWEB_CAT_NOM_AUT_EXT SET ESTATUS_OPER='" + estatus + "', OBSERVACIONES='SOLICITUD CANCELADA' " +
											"WHERE FOLIO_ENV='" + bean.getFolioReg() + "'";
											out.println(bean.getCuenta() + ";E;SOLICITUD CANCELADA;");
									try {
										Crear(query);
										if(Ejecuta() == 1){
											respuesta[0]++;
											bean.setEstatus(estatus.charAt(0));
										}
										else {
											respuesta[1]++;
										}
									}
									catch (Exception e) {
										e.printStackTrace();
										respuesta[1]++;
										EIGlobal.mensajePorTrace( "Error en consulta de Oracle.", EIGlobal.NivelLog.ERROR);
									}
									finally{
										try {
											if(statement!=null){
												statement.close();
												statement=null;}
											if(conn!=null){
												conn.close();
												conn=null;}
										} catch(Exception e1){
												EIGlobal.mensajePorTrace ("CatNominaDAO - Error al cerrar conexi�n-> "+e1,EIGlobal.NivelLog.INFO);
										}
									}
									break;
						case 'C':	//Cancelada
						case 'A':	//Activa
						default:	//Status Erroneo
									respuesta[1]++;
									break;
					}
				}
			}
		}
		catch (IOException e) {
			EIGlobal.mensajePorTrace("Error en escritura de archivo", EIGlobal.NivelLog.ERROR);
		}
		finally {
			try {
				out.close();
			}
			catch(Exception e) {
				EIGlobal.mensajePorTrace("Error al cerrar archivo", EIGlobal.NivelLog.ERROR);
			}
		}
		return respuesta;
	}

	private void Crear(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion-----------PreparedStatament", EIGlobal.NivelLog.DEBUG);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.DEBUG);
    		conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE2, EIGlobal.NivelLog.DEBUG);
			statement = conn.prepareStatement (query);
    	}

	private void Crear2(String query)throws Exception{
    		EIGlobal.mensajePorTrace("--------- Creando Conexion 2-----------PreparedStatament", EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace(" ANTES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
    		conn2 = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
			EIGlobal.mensajePorTrace(" dESPUES ds: " + Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
			statement2 = conn2.prepareStatement (query);
			//statement = conn.preparedStatement();
    }


	private ResultSet Consulta()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement.executeQuery();
		return rs;
		}

	private ResultSet Consulta2()throws Exception{
		EIGlobal.mensajePorTrace("--------- Realizando Consulta 2 -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		ResultSet rs = null;
		rs = statement2.executeQuery();
		return rs;
		}

	private int Ejecuta() throws Exception {
		EIGlobal.mensajePorTrace("--------- Ejecutando Update -----------PreparedStatament", EIGlobal.NivelLog.INFO);
		int result = -1;
		result = statement.executeUpdate();
		return result;
	}

}