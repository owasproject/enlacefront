package mx.altec.enlace.dao;

import static mx.altec.enlace.beans.NomPreEmpleado.*;
import static mx.altec.enlace.beans.NomPreRemesa.*;
import static mx.altec.enlace.beans.NomPreTarjeta.*;
import static mx.altec.enlace.utilerias.NomPreUtil.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1B;
import mx.altec.enlace.beans.NomPreLM1C;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1E;
import mx.altec.enlace.beans.NomPreLM1H;

public class NomPreTarjetaDAO extends NomPreMQDAO {
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final String CADENA_TARJETAS_VACIO_1 = rellenar(" ", 118);
	private static final String CADENA_TARJETAS_VACIO_2 = rellenar(" ", 234);
	
	
	
	//TRANS: LM1E
	/**
	 * ESC 29/junio/2010 se agrega campo numero de identificion requerido para la transaccion lm1e
	 */
	public NomPreLM1E cancelaTarjeta(String noContrato, String noTarjeta, String noEmpleado) {
		//B - Baja
		
		StringBuffer sb = new StringBuffer()
			.append("B")									//COD-ACCION
			.append(rellenar(noContrato, 11))				//NUM-CONTRATO
			.append(rellenar(noTarjeta, LNG_NO_TARJETA))	//NUM-TARJETA
			.append(CADENA_TARJETAS_VACIO_1)				//RESTANTES			
			.append(rellenar(noEmpleado, LNG_NO_EMPLEADO))	//NUMEMPL
			.append(CADENA_TARJETAS_VACIO_2);				//RESTANTES
		
		logInfo("LM1E/B - Baja tarjeta");
		
		return consulta(NomPreLM1E.HEADER, sb.toString(), NomPreLM1E.getFactoryInstance());				
	}
	
	//TRANS: LM1E
	public NomPreLM1E bloqueaTarjeta(String noContrato, String noTarjeta, String noEmpleado) {
		//Q - Bloqueo
		
		StringBuffer sb = new StringBuffer()
			.append("Q")									//COD-ACCION
			.append(rellenar(noContrato, 11))				//NUM-CONTRATO
			.append(rellenar(noTarjeta, LNG_NO_TARJETA))	//NUM-TARJETA
			.append(CADENA_TARJETAS_VACIO_1)				//RESTANTES		
			.append(rellenar(noEmpleado, LNG_NO_EMPLEADO))	//NUMEMPL
			.append(CADENA_TARJETAS_VACIO_2);				//RESTANTES	
		
		logInfo("LM1E/Q - Bloqueo tarjeta");
		
		return consulta(NomPreLM1E.HEADER, sb.toString(), NomPreLM1E.getFactoryInstance());
	}
	
	//TRANS: LM1D
	public NomPreLM1D consultaRelacionEmpleado(String noContrato,
			String noTarjeta, Date fechaAlta, NomPreEmpleado empleado, String masDatos) {							
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(noContrato, 11) ) 														//NUM-CONTRATO
			.append(empleado == null ? rellenar(" ", LNG_NO_EMPLEADO) : empleado.getNoEmpleadoFrm())//NUM-EMPLEADO
			.append(rellenar(noTarjeta, LNG_NO_TARJETA))											//NUM-TARJETA
			.append(fechaAlta == null ? rellenar(" ", 10) : SDF.format(fechaAlta))					//FEC-ALTA 		
			.append(empleado == null ? rellenar(" ", LNG_PATERNO) : empleado.getPaternoFrm())		//APE-PATERNO
			.append(empleado == null ? rellenar(" ", LNG_MATERNO) : empleado.getMaternoFrm())		//APE-MATERNO
			.append(empleado == null ? rellenar(" ", LNG_NOMBRE) : empleado.getNombreFrm())			//NOM-EMPLEADO
			.append(rellenar(masDatos, LNG_NO_EMPLEADO));															//MAS-DATOS (PAG)
		
		logInfo("LM1D - Consulta relacion tarjeta empleado");
		
		return consulta(NomPreLM1D.HEADER, sb.toString(), NomPreLM1D.getFactoryInstance());
	}
	
	//TRANS: LM1E
	public NomPreLM1E asignarTarjeta(String noContrato, String noTarjeta, NomPreEmpleado empleado) {
		//T - Asignación de Tarjeta
		
		StringBuffer sb = new StringBuffer()
			.append("T")								//COD-ACCION
			.append(rellenar(noContrato, 11))			//NUM-CONTRATO 
			.append(rellenar(noTarjeta, LNG_NO_TARJETA))//NUM-TARJETA  
			.append(empleado.getNombreFrm())			//NOM-EMPLEADO
			.append(empleado.getPaternoFrm())			//APE-PATERNO
			.append(empleado.getMaternoFrm())			//APE-MATERNO
			.append(empleado.getRfcFrm())				//COD-RFC
			.append(empleado.getHomoclaveFrm())			//COD-HOMOCLAVE
			.append(empleado.getNoEmpleadoFrm())		//COD-EMPLEADO
			.append(empleado.getFechaNacimientoFrm())	//FEC-NACIMIENTO
			.append(empleado.getSexoFrm())				//IND-SEXO
			.append(empleado.getNacionalidadFrm())		//COD-NACION
			.append(empleado.getEstadoCivilFrm())		//COD-EST-CIVIL
			.append(empleado.getCorreoElectronicoFrm())	//DES-MAIL
			.append(empleado.getCalleFrm())				//DES-DOMICILIO
			.append(empleado.getNumeroFrm())			//NUM-DOMICILIO
			.append(empleado.getColoniaFrm())			//DES-COLONIA
			.append(empleado.getDelegacionFrm())		//DES-DELEGACION
			.append(empleado.getCiudadFrm())			//DES-CIUDAD
			.append(empleado.getEstadoFrm())			//COD-ESTADO
			.append(empleado.getCodigoPostalFrm())		//COD-POSTAL
			.append(empleado.getLadaFrm())				//COD-LADA
			.append(empleado.getTelefonoFrm());			//COD-TELEFONO		
		
		logInfo("LM1E/T - Asignar tarjeta");
		
		return consulta(NomPreLM1E.HEADER, sb.toString(), NomPreLM1E.getFactoryInstance());		
	}
	
	//TRANS: LM1H
	public NomPreLM1H reasignarTarjeta(String noContrato, String noEmpleado, String noTarjetaAnt, String noTarjetaNva) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(noContrato, 11)) 				//NUM-CONTRATO 	
			.append(rellenar(noEmpleado, LNG_NO_EMPLEADO))	//COD-EMPLEADO
			.append(rellenar(noTarjetaAnt, LNG_NO_TARJETA)) //NUM-TARJETA-ANT
			.append(rellenar(noTarjetaNva, LNG_NO_TARJETA));//NUM-TARJETA-ACT
		
		logInfo("LM1H - Reasignar tarjeta");
		return consulta(NomPreLM1H.HEADER, sb.toString(), NomPreLM1H.getFactoryInstance());
	}
		
	//TRANS: LM1B
	public NomPreLM1B obtenerTarjetas(String noContrato, String noRemesa, String paginacion) {				
				
		StringBuffer sb = new StringBuffer()
			.append(rellenar(noContrato, 11)) 			//NUM-CONTRATO
			.append(rellenar(noRemesa, LNG_NO_REMESA))	//NUM-REMESA
			.append(rellenar(paginacion, 22));			//TRJ-PAGINACION (PAG)
		
		logInfo("LM1B - Obtener tarjetas");
		
		return consulta(NomPreLM1B.HEADER, sb.toString(), NomPreLM1B.getFactoryInstance()); 
	}
	
	//TRANS: LM1C
	private NomPreLM1C ejecutaNomPreLM1CBase(String operacion, String noContrato, String noRemesa, String... tarjetas) {
		
		StringBuffer sb = new StringBuffer();
		StringBuffer sbt = new StringBuffer("");
		
		if (tarjetas.length > 0 && tarjetas.length <= 50) {						
			
			for(int i = 0; i < tarjetas.length; i++) {
				
				if (tarjetas[i] == null || tarjetas[i].length() == 0) {
					break;
				}
				
				sbt.append(rellenar(tarjetas[i], LNG_NO_TARJETA));
				
				//if (i != tarjetas.length - 1 && (i + 1) % 5 != 0) {
				
					sbt.append("@");
				//}
			}
		}
		
		sb.append(rellenar(operacion,2))				//COD-ACCION
			.append(rellenar(noContrato, 11))			//NUM-CONTRATO				
			.append(rellenar(noRemesa, LNG_NO_REMESA)) 	//NUM-REMESA
			.append(rellenar(sbt.toString(), 1150));	//NUM-TARJETAS-1/10											
		
		return consulta(NomPreLM1C.HEADER, sb.toString(), NomPreLM1C.getFactoryInstance());
	}
	
	//TRANS: LM1C
	public NomPreLM1C aprobarTarjetas(String noContrato, String noRemesa, String... tarjetas) {
		
		//AT - Aprobar Tarjetas		
		logInfo("LM1C/AT - Aprobar tarjetas");
		return ejecutaNomPreLM1CBase("AT", noContrato, noRemesa, tarjetas);
	}
	
	//TRANS: LM1C
	public NomPreLM1C rechazarTarjetas(String noContrato, String noRemesa, String... tarjetas) {
		
		//RT - Rechazar Tarjeta.		
		logInfo("LM1C/RT - Rechazar tarjetas");
		return ejecutaNomPreLM1CBase("RT", noContrato, noRemesa, tarjetas);							
	}

}
