package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmLyMGL28;


import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmLyMValidacionDAO extends AdmonUsuariosMQDAO {
	
	//TRANS: GL28
	private AdmLyMGL28 ejecutaGL28Base(String contrato, 
			String usuario, String cveOperacion, String importe) {
		
		StringBuffer sb = new StringBuffer()
			.append(rellenar(contrato, 11))
			.append(rellenar(usuario, 8))
			.append(rellenar(cveOperacion, 4))
			.append(rellenar(importe, 17));
		return consulta(AdmLyMGL28.HEADER, sb.toString(), AdmLyMGL28.getFactoryInstance());
	}
	
	public AdmLyMGL28 validarLyM(String contrato, 
			String usuario, String cveOperacion, String importe) {
		logInfo("GL28 - Validación de LyM");
		return ejecutaGL28Base(contrato, usuario, cveOperacion, importe);			
	}

}
