package mx.altec.enlace.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.naming.NamingException;
import javax.sql.DataSource;
import mx.altec.enlace.beans.TranInterbancaria;
import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.TranInterConstantes;

import java.io.UnsupportedEncodingException;
import org.apache.commons.lang.CharSet;

/**
 *
 * @author Z708764
 *
 */
public class TransferenciaInterbancariaDAO {

	/**
	 * INSTANCE instancia
	 */
	private static TransferenciaInterbancariaDAO INSTANCE = null;
	/**
	 * Metodo que realiza la creacion de la instancia
	 */
	private synchronized static void createInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TransferenciaInterbancariaDAO();
		}
	}
	/**
	 * Metodo que crea el get de la instancia
	 * @return
	 */
	public static TransferenciaInterbancariaDAO getInstance() {
		if (INSTANCE == null) {
			createInstance();
		}
		return INSTANCE;
	}
	/**
	 * Metodo que realiza la conexion a la Base de Datos
	 * @param dbName nombre de base de datos
	 * @return Connection conexion
	 * @throws SQLException Excepcion de Base de Datos
	 */
	public static Connection createiASConn_static ( String dbName )
		throws SQLException {
		Connection conn = null;
		ServiceLocator servLocator = ServiceLocator.getInstance();
		DataSource ds = null;
		try {
			 ds = servLocator.getDataSource(dbName);
		 }catch(NamingException e) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		 }
		 if(null == ds) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
			 return null;
		 }
		 conn = ds.getConnection ();
		 return conn;
	}
	/**
	 * Metodo que realiza el armado del query
	 * @param tranInterbancaria
	 * @return boolean actualiza datos
	 */
	public boolean registraTransferenciaInterbancaria(TranInterbancaria tranInterbancaria) {
		StringBuffer query= new StringBuffer();
		query.append("insert into EWEB_TRAN_INTERB (REFERENCIA,FCH_ENVIO,CONTRATO,CVE_USUARIO,CUENTA_CARGO,CUENTA_ABONO,");
		query.append("BENEFICIARIO,IMPORTE,CONCEPTO_PAGO,ESTATUS,FORMA_LIQ,FOLIO_MANCOMUN,REFERENCIA_INTERB,TITULAR,IVA,RFC,BANCO, ");
		query.append(" PLAZA,SUCURSAL,CUENTA_CLABE,CVE_DIVISA )");
		query.append(" values (");
		query.append(tranInterbancaria.getReferencia());
		query.append(TranInterConstantes.COMA);
		query.append("sysdate,");
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getContrato());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getUsuario());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getCuentaCargo());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getCuentaAbono());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getBeneficiario());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(tranInterbancaria.getImporte());
		query.append(TranInterConstantes.COMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getConceptoPago());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getEstatus());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getFormaLiquidacion());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(tranInterbancaria.getFolioMancomunado());
		query.append(TranInterConstantes.COMA);
		query.append(tranInterbancaria.getReferenciaInterb());
		query.append(TranInterConstantes.COMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getTitular());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(tranInterbancaria.getIvaNull());
		query.append(TranInterConstantes.COMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getRfc());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getBanco());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getPlaza());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getSucursal());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append(tranInterbancaria.getCuentaCLABE());
		query.append(TranInterConstantes.APOSTROFECOMA);
		query.append(TranInterConstantes.APOSTROFE);
		query.append("MXN".equals(tranInterbancaria.getTipoDivisa()) ? "MXP" : tranInterbancaria.getTipoDivisa());
		query.append(TranInterConstantes.APOSTROFE);
		query.append(")");
		return actualizaDatos(query.toString());
	}
	/**
	 * Metodo que realiza el armado de la lista con los datos de la consulta.
	 * @param contrato contrato
	 * @param fechaInicio fecha inicio
	 * @param fechaFin fecha fin
	 * @param tipoMoneda Tipo de Divisa para la consulta
	 * @return lista de beans
	 */
	public HashMap<Integer,TranInterbancaria> consultaTransferenciaInterbancaria(String contrato,String fechaInicio,String fechaFin, String tipoMoneda) {
		HashMap<Integer,TranInterbancaria> listaDatos= new HashMap<Integer,TranInterbancaria>();
		ResultSet result =null;
		Statement stmt = null;
		Connection conexion=null;
		try {
			StringBuffer query= new StringBuffer();
			query.append("SELECT REFERENCIA referencia,to_char(FCH_ENVIO,'dd/mm/yyyy') fchEnvio,CONTRATO  contrato,");
			query.append("CVE_USUARIO  usuario,CUENTA_CARGO  cuentaCargo,CUENTA_ABONO  cuentaAbono,");
			query.append("BENEFICIARIO beneficiario,IMPORTE importe,CONCEPTO_PAGO concepto,ESTATUS estatus,");
			query.append("FORMA_LIQ liquidacion,FOLIO_MANCOMUN folioManc,REFERENCIA_INTERB refInterb,");
			query.append("to_char(FCH_APLICACION,'dd/mm/yyyy hh24:mi:ss') fchAplica,CLAVE_RASTREO cveRastreo, REFERENCIA_DEVOL refDevol,");
			query.append("to_char(FCH_DEVOL,'dd/mm/yyyy') fchDevol,CVE_MOT_DEVOL cveDevol ,DESCRIP_DEVOL desDevol,TITULAR titular,IVA iva,RFC rfc ,BANCO banco, ");
			query.append(" PLAZA plaza,SUCURSAL sucursal,CUENTA_CLABE cuentaClabe ");
			query.append(" FROM EWEB_TRAN_INTERB ");
			query.append(" WHERE ");
			query.append("FCH_ENVIO >= TO_DATE('"+fechaInicio+"','ddmmyyyy')");
			query.append("AND FCH_ENVIO < TO_DATE('"+fechaFin+"','ddmmyyyy')+1 ");
			query.append("AND CONTRATO='"+contrato+"'");
			//ARS Vector SPID Inicio
			if ("MXN".equals(tipoMoneda)) {
				query.append(" AND CVE_DIVISA IN ('MXN','MXP') ");
			} else {
				query.append(" AND CVE_DIVISA='USD' ");
			}
			//ARS Vector SPID Fin
			conexion=createiASConn_static (Global.DATASOURCE_ORACLE2 );
			EIGlobal.mensajePorTrace("query: " + query,EIGlobal.NivelLog.DEBUG);
			stmt = conexion.createStatement();
			result = stmt.executeQuery(new String(query.toString().getBytes("ISO-8859-1"), "UTF-8"));
			int i=0;
			EIGlobal.mensajePorTrace(" EJECUTO QUERY",EIGlobal.NivelLog.DEBUG);
			while (result.next ()) {
				try{
					TranInterbancaria tranInterbancaria=extraeValores(result);
					listaDatos.put(i,tranInterbancaria);
					i++;
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(TranInterConstantes.ERROR+e.getMessage(),EIGlobal.NivelLog.ERROR);
					e.printStackTrace();
				}
			}
		}catch(Exception e) {
			EIGlobal.mensajePorTrace(TranInterConstantes.ERROR+e.getMessage(),EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		}finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			try {
				cierraConexion(conexion);
			}catch(SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return listaDatos;
	}
	/**
	 * Metodo que realiza la consulta de la clave del banco
	 * @param banco banco a consultar
	 * @return clave numerica del banco
	 */
	public Integer consultaClaveBanco(String banco) {
		int claveBanco=0;
		ResultSet result =null;
		Statement stmt = null;
		Connection conexion=null;
		try {
			String query="SELECT NUM_BANXICO FROM COMU_INTERME_FIN WHERE CVE_INTERME= '"+banco+"'";
			conexion=createiASConn_static (Global.DATASOURCE_ORACLE);
			stmt = conexion.createStatement();
			result = stmt.executeQuery(query);
			EIGlobal.mensajePorTrace("TransferenciaInterbancariaDAO::consultaClaveBanco::query:" + query,EIGlobal.NivelLog.DEBUG);
			while (result.next ()) {
				try {
					claveBanco=result.getInt("NUM_BANXICO");
				}catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				cierraConexion(conexion);
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			try{
				if(null!=conexion) {
					conexion.close();
					conexion=null;
				}
			}catch(SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return claveBanco;
	}
	/**
	 * Metodo que realiza el cierre de la conexion
	 * @param conexion
	 * @throws SQLException
	 */
	public void cierraConexion(Connection conexion)	throws SQLException{
		try{
			if(null!=conexion) {
				conexion.close();
				conexion=null;
			}
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}
	}
	/**
	 * Metodo que realiza la actualizacion de los datos
	 * @param queryActualiza query para actualizar
	 * @return boolean si actualiza o no los datos
	 */
	public boolean actualizaDatos(String queryActualiza){
		PreparedStatement preparedstatement = null;
		Connection conexion=null;
		int regActualiza=0;
		try {
			conexion=createiASConn_static (Global.DATASOURCE_ORACLE2);
			conexion.setAutoCommit(true);
			EIGlobal.mensajePorTrace("Antes de ejecutar: "+queryActualiza,EIGlobal.NivelLog.DEBUG);
			preparedstatement =  conexion.prepareStatement(new String(queryActualiza.toString().getBytes("ISO-8859-1"), "UTF-8"));
			regActualiza=preparedstatement.executeUpdate();
			EIGlobal.mensajePorTrace("Registros Actualizados:"+regActualiza,EIGlobal.NivelLog.DEBUG);
			preparedstatement.close();
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);			
		}catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);		
		} 
		finally {
			try {
				cierraConexion(conexion);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		if(regActualiza>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * metodo que realiza la extraccion de valores
	 * @param result
	 * @return
	 */
	private TranInterbancaria extraeValores(ResultSet result) {
		TranInterbancaria bean = new TranInterbancaria();
		try{
			bean.setReferencia(result.getLong("referencia"));
			bean.setFchEnvio(result.getString("fchEnvio"));
			bean.setContrato(result.getString("contrato"));
			bean.setUsuario(result.getString("usuario"));
			bean.setCuentaCargo(result.getString("cuentaCargo"));
			bean.setCuentaAbono(result.getString("cuentaAbono"));
			bean.setBeneficiario(result.getString("beneficiario"));
			bean.setImporte(result.getDouble("importe"));
			bean.setConceptoPago(result.getString("concepto"));
			bean.setEstatus(result.getString("estatus"));
			bean.setFormaLiquidacion(result.getString("liquidacion"));
			bean.setFolioMancomunado(result.getLong("folioManc"));
			bean.setReferenciaInterb(result.getLong("refInterb"));
			bean.setFchAplicacion(result.getString("fchAplica"));
			bean.setCveRastreo(result.getString("cveRastreo"));
			bean.setReferenciaDevol(result.getLong("refDevol"));
			bean.setFchDevol(result.getString("fchDevol"));
			bean.setCveMotivoDevol(result.getString("cveDevol"));
			bean.setDesMotivoDevol(result.getString("desDevol"));
			bean.setTitular(result.getString("titular"));
			bean.setIva(result.getString("iva"));
			bean.setRfc(result.getString("rfc"));
			bean.setBanco(result.getString("banco"));
			bean.setPlaza(result.getString("plaza"));
			bean.setSucursal(result.getString("sucursal"));
			bean.setCuentaCLABE(result.getString("cuentaClabe"));
			if(null != bean.getBeneficiario() && bean.getBeneficiario().indexOf("&")>-1) {
				bean.setBeneficiario(bean.getBeneficiario().replaceAll("&", " AND "));
			}
			String estatusDesc="",estatusExp="";
			if(("CO").equals(bean.getEstatus())) {
				estatusDesc = "<font color=green><b> ACEPTADA Y CONFIRMADA </b></font>";
				estatusExp="ACEPTADA Y CONFIRMADA";
			}else if(("DV").equals(bean.getEstatus())) {
				estatusDesc = "<font color=blue><b>DEVUELTA POR EL BANCO RECEPTOR EN EL SPEI </b></font>";
				estatusExp="DEVUELTA POR EL BANCO RECEPTOR EN EL SPEI";
			}else if(("CA").equals(bean.getEstatus())) {
				estatusDesc = "<font color=blue><b>RECHAZADA POR SANTANDER </b></font>";
				estatusExp="RECHAZADA POR SANTANDER";
			}else if(("EN").equals(bean.getEstatus())|| ("LI").equals(bean.getEstatus())) {
				estatusDesc = "<font color=blue><b>ACEPTADA </b></font>";
				estatusExp="ACEPTADA";
			}else if(("PV").equals(bean.getEstatus())|| ("PR").equals(bean.getEstatus()) || ("PA").equals(bean.getEstatus())) {
				estatusDesc = "<font color=blue><b>EN PROCESO DE VALIDACIÓN </b></font>";
				estatusExp="EN PROCESO DE VALIDACIÓN";
			}else {
				estatusDesc = "<font color=blue><b> EN PROCESO DE VALIDACIÓN </b></font>";
				estatusExp="EN PROCESO DE VALIDACIÓN";
			}
			if(null == bean.getFormaLiquidacion() || "".equals(bean.getFormaLiquidacion().trim())) {
				if(bean.getImporte()>50000) {
					bean.setFormaLiquidacion("H");
				}else {
					bean.setFormaLiquidacion("2");
				}
			}
			String formaAplicaDesc=("H".equals(bean.getFormaLiquidacion()))?"Mismo d&iacute;a":"D&iacute;a Siguiente";
			String formaAplicaDescExp=("H".equals(bean.getFormaLiquidacion()))?"Mismo dia":"Dia Siguiente";
			bean.setDescFormaAplic(formaAplicaDesc);
			bean.setDescEstatus(estatusDesc);
			bean.setExpFormaAplica(formaAplicaDescExp);
			bean.setExpEstatus(estatusExp);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return bean;
	}
}