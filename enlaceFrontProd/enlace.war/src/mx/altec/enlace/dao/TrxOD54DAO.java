/** 
*   Isban Mexico
*   Clase: TrxOD54DAO.java
*   Descripcion: Objeto de datos  que comprueba la existencia de informacion 
*   para el documento electronico relacionado con el numero de cliente en la 
*   transaccion OD54.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.dao;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;

public class TrxOD54DAO extends GenericDAO{
    
    /**Encabezado de la trama de env&iacute;o*/
    public static final String HEADEROD54         = "OD5401521123451O00N2";
    /**Caracter de inicio de trama para env&iacute;o*/
    public static final String INICIO0D54         = " ";
    
//    /**Indicador de error en la ejecuci&oacute;n de la trama*/
//    private static final String ERROR             = "ER";
//    /**Indicador de &eacute;xito en la ejecuci&oacute;n de la trama*/
//    private static final String EXITO             = "AV";

    
    /**
     * Verifica si existe la informacion del documento electronico relacionado con el numero de cliente
     * @param trama 	   trama con datos del documento a afectar
     * @return             cadena con datos resultantes de la transaccion
     */
    public String ejecutaConsulta(String trama){

        StringBuffer cadena = new StringBuffer().append(HEADEROD54).append(trama);

        String respuesta = FielConstants.EMPTY_STRING;

        EIGlobal.mensajePorTrace("TrxOD54DAO - ejecutaConsulta(): [" +cadena.toString()+"]", EIGlobal.NivelLog.INFO);
        respuesta = invocarTransaccion(cadena.toString());
        EIGlobal.mensajePorTrace("TrxOD54DAO - ejecutaConsulta() - RESULTADO TRAMA : [" + respuesta+"]",EIGlobal.NivelLog.INFO);
        return respuesta;
    }
    
    

}
