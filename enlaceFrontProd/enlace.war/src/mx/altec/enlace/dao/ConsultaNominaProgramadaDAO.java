/**
 * Isban Mexico
 *   Clase: ConsultaNominaProgramadaDAO.java
 *   Descripcion: Implementacion de ConsultaNominaProgramadaDAO, acceso a datos para la consulta
 *   a la tabla eweb_nomina
 *
 *   Control de Cambios:
 *   1.0 Mar 01, 2015 asanjuan - Creacion
 *   
 **/
package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.altec.enlace.beans.ConsultaPaginadaBean;
import mx.altec.enlace.beans.TablaEwebNominaBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import com.google.gwt.dev.util.collect.HashMap;

/**
 * Clase ConsultaNominaProgramadaDAO
 * @author Arturo
 * @version 1.0
 * 
 */
public class ConsultaNominaProgramadaDAO extends GenericDAO {
	
	/** LOG TAG**/
	private static final String LOG_TAG = ">>>>> ConfEdosCtaMasArchivoDAO ::: ";
	/**Connection**/
	private transient Connection conn=null;
	
	private static final String FORMATO_FECHA = "'dd/MM/yyyy'";
	private static final String FORMATO_FECHA2 = "'yyyymmdd'";

	
	/**
	 * Consulta de todos los registros de la nomina
	 * @param contrato : numero de contrato
	 * @return mapa con datos de la consulta
	 * @throws SQLException : exception
	 */
	public Map<String, Object> consultarNominaProgramada (String contrato) throws SQLException {
		
		//final BaseServlet utilerias = new BaseServlet();
		Map<String, Object> respuestasConsulta = new HashMap<String, Object>();
		List<TablaEwebNominaBean> respuestaConsulta = new ArrayList<TablaEwebNominaBean>();
		Double totalNomina = 0.0;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String queryConsulta = String.format("SELECT to_char(FCH_RECEP,"+FORMATO_FECHA+") AS FCH_RECEP, to_char(FCH_CARGO,"+FORMATO_FECHA+") AS FCH_CARGO, to_char(FCH_APLIC,"+FORMATO_FECHA+") AS FCH_APLIC, ID_HOR_DISP,  " +
				"CTA_CARGO, NOM_ARCH, SEC_PAGO, NUM_REG, IMPORTE_APLIC, ESTATUS " +
				"FROM EWEB_NOMINA WHERE num_cuenta2 = '%s' and estatus = 'R' and  to_number(to_char(fch_aplic, "+FORMATO_FECHA2+")) >= to_number(to_char(sysdate, "+FORMATO_FECHA2+"))", contrato);
				
		try {		
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			EIGlobal.mensajePorTrace( "***ConsultaNominaProgramadaDAO.class::consultarNominaProgramadaPag()", EIGlobal.NivelLog.INFO);
			
			pst = conn.prepareStatement(queryConsulta);
			rs = pst.executeQuery();
			while (rs.next()) {
				TablaEwebNominaBean dato = new TablaEwebNominaBean();
				dato.setFechaRecepcion( rs.getString("FCH_RECEP"));
				dato.setFechaCargo(rs.getString("FCH_CARGO"));
				dato.setFechaAplicacion(rs.getString("FCH_APLIC"));
				dato.setHoraAplicacion(rs.getString("ID_HOR_DISP"));
				dato.setCtaCargo(rs.getString("CTA_CARGO"));
				dato.setNombreArchivo(rs.getString("NOM_ARCH"));
				dato.setSecuencia(rs.getString("SEC_PAGO"));
				dato.setNumeroRegistros(rs.getString("NUM_REG"));
				dato.setImporteAplic(rs.getString("IMPORTE_APLIC"));
				totalNomina = totalNomina + Double.parseDouble(dato.getImporteAplic());
				dato.setEstatus(rs.getString("ESTATUS"));
				
				respuestaConsulta.add(dato);
			} 
			//return secuencia;
		} finally {
			try{
				if (rs!=null) {
					rs.close();
				}
				if (conn!=null){
					conn.close();
				}
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
					String.format("%s Error Cerrando Recursos %s",LOG_TAG,e1.toString()),NivelLog.ERROR);
			}			
		}
		respuestasConsulta.put("resultadoNomina", "OK");
		respuestasConsulta.put("listaDatosNomina", respuestaConsulta);
		respuestasConsulta.put("totalNomina", String.format("%.2f", totalNomina));
		
		return respuestasConsulta;
	}
	
	/**
	 * Consulta de la nomina paginada
	 * @param contrato : numero de contrato
	 * @param regInicio : registro de inicio
	 * @param regFin : registro de gin
	 * @return bean de la consulta paginada
	 * @throws SQLException : exception
	 */
	public ConsultaPaginadaBean consultarNominaProgramadaPag(String contrato, int regInicio, int regFin) throws SQLException {
		
		//final BaseServlet utilerias = new BaseServlet();
		Map<String, Object> respuestasConsulta = new HashMap<String, Object>();
		List<TablaEwebNominaBean> respuestaConsulta = new ArrayList<TablaEwebNominaBean>();
		ConsultaPaginadaBean consultaPaginada = new ConsultaPaginadaBean();
		Double totalNomina = 0.0;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		Map<String, Object> resultado= consultaNumeroRegistros(contrato);
		int numeroRegistros = (Integer)resultado.get("numeroRegistros");
		
		if(numeroRegistros > 0) {
			StringBuffer queryConsulta = new StringBuffer();
			queryConsulta.append("select num_cuenta2, sec_pago, estatus, cta_cargo, num_reg, importe_aplic, fch_recep, nom_arch, fch_cargo, id_hor_disp, fch_aplic from (select num_cuenta2, sec_pago, decode(estatus,'R','RECIBIDO') as estatus, cta_cargo, num_reg,importe_aplic, to_char(fch_recep,"+FORMATO_FECHA+") AS fch_recep, nom_arch, to_char(fch_cargo,"+FORMATO_FECHA+") AS fch_cargo, id_hor_disp, to_char(fch_aplic,"+FORMATO_FECHA+") AS fch_aplic, ").
				append("row_number() over(order by sec_pago) rn ").
				append("from EWEB_NOMINA where num_cuenta2 = '%s' and estatus = 'R' and  to_number(to_char(fch_aplic, "+FORMATO_FECHA2+")) >= to_number(to_char(sysdate, "+FORMATO_FECHA2+"))) " ).
				append("where rn between %s and %s ");
			
			try {		
				conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
				EIGlobal.mensajePorTrace( "***ConsultaNominaProgramadaDAO.class::consultarNominaProgramadaPag()&", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "***consultarNominaProgramadaPag()&" + String.format(queryConsulta.toString(), contrato, regInicio, regFin), EIGlobal.NivelLog.INFO);
				
				pst = conn.prepareStatement(String.format(queryConsulta.toString(), contrato, regInicio, regFin));
				rs = pst.executeQuery();
				while (rs.next()) {
					TablaEwebNominaBean dato = new TablaEwebNominaBean();
					dato.setFechaRecepcion(rs.getString("FCH_RECEP"));
					dato.setFechaCargo(rs.getString("FCH_CARGO"));
					dato.setFechaAplicacion(rs.getString("FCH_APLIC"));
					dato.setHoraAplicacion(rs.getString("ID_HOR_DISP"));
					dato.setCtaCargo(rs.getString("CTA_CARGO"));
					dato.setNombreArchivo(rs.getString("NOM_ARCH"));
					dato.setSecuencia(rs.getString("SEC_PAGO"));
					dato.setNumeroRegistros(rs.getString("NUM_REG"));
					dato.setImporteAplic(rs.getString("IMPORTE_APLIC"));
					totalNomina = totalNomina + Double.parseDouble(dato.getImporteAplic());
					dato.setEstatus(rs.getString("ESTATUS"));
					
					respuestaConsulta.add(dato);
				} 
				//return secuencia;
			} finally {
				try{
					if (rs!=null) {
						rs.close();
					}
					if (conn!=null){
						conn.close();
					}
				}catch(SQLException e1){
					EIGlobal.mensajePorTrace(
						String.format("%s Error Cerrando Recursos %s",LOG_TAG,e1.toString()),NivelLog.ERROR);
				}			
			}
		}
		respuestasConsulta.put("resultadoNomina", "OK");
		respuestasConsulta.put("listaDatosNomina", respuestaConsulta);
		respuestasConsulta.put("totalNomina", String.format("%.2f", totalNomina));
		respuestasConsulta.put("totalGlobalNomina", String.format("%.2f", (Double)resultado.get("importeTotal")));
		
		consultaPaginada.setTotalRegistros(numeroRegistros);
		consultaPaginada.setNumeroRegInicio(regInicio);
		consultaPaginada.setNumeroRegFin(regFin);
		consultaPaginada.setResultadoConsulta(respuestasConsulta);
		
		return consultaPaginada;
	}
	
	/**
	 * Numero de registros de la conlta de nomina
	 * @param contrato : numero de contrato
	 * @return numero de registros
	 * @throws SQLException : exception
	 */
	public Map<String, Object> consultaNumeroRegistros(String contrato) throws SQLException {
		
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		int numeroRegistros = 0;
		Double importeTotal = 0.0;
		Map<String, Object> resultado = new HashMap<String, Object>();
		
		String queryConsulta = String.format("SELECT COUNT(num_cuenta2) as num_registros, sum(importe_aplic) as importe_total FROM EWEB_NOMINA WHERE num_cuenta2 = '%s' AND estatus = 'R' and  to_number(to_char(fch_cargo, "+FORMATO_FECHA2+")) >= to_number(to_char(sysdate, "+FORMATO_FECHA2+"))", contrato);
		
		try {
			EIGlobal.mensajePorTrace( "***ConsultaNominaProgramadaDAO.class::consultaNumeroRegistros()&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***consultaNumeroRegistros()&"+String.format("SELECT COUNT(num_cuenta2) as num_registros FROM EWEB_NOMINA WHERE num_cuenta2 = '%s' AND estatus = 'R'", contrato), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***consultaNumeroRegistros()& Tipo de data source::: " + Global.DATASOURCE_ORACLE3, EIGlobal.NivelLog.INFO);
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			
			pst = conn.prepareStatement(queryConsulta);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				numeroRegistros = Integer.parseInt(rs.getString("num_registros"));
				importeTotal = rs.getString("importe_total") != null ? Double.parseDouble(rs.getString("importe_total")) : 0.0;
			} else {
				numeroRegistros = 0;
			}
			//return secuencia;
		} finally {
			try{
				if (rs!=null) {
					rs.close();
				}
				if (conn!=null){
					conn.close();
				}
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(
					String.format("%s Error Cerrando Recursos %s",LOG_TAG,e1.toString()),NivelLog.ERROR);
			}			
		}
		resultado.put("numeroRegistros", numeroRegistros);
		resultado.put("importeTotal", importeTotal);
		
		EIGlobal.mensajePorTrace( "***consultaNumeroRegistros()& Numero de registros::: " + numeroRegistros, EIGlobal.NivelLog.INFO);
		return resultado;
	}	
}