package mx.altec.enlace.dao;

import mx.altec.enlace.beans.AdmUsrGL37;

import static mx.altec.enlace.utilerias.NomPreUtil.*;

public class AdmUsrPerfilesDAO extends AdmonUsuariosMQDAO {

	//TRANS: GL37
	private AdmUsrGL37 ejecutaGL37Base(String cveGrupo, String cvePerfil,
			String indPaginacion) {

		StringBuffer sb = new StringBuffer()
			.append(rellenar(cveGrupo, 2))					//CVE-GRUPO
			.append(rellenar(cvePerfil, 8))                 //CVE-PERFIL
			.append(rellenar(indPaginacion, 2));			//IND-PAGINACION
		return consulta(AdmUsrGL37.HEADER, sb.toString(), AdmUsrGL37.getFactoryInstance());
	}

	public AdmUsrGL37 consultaPerfiles(String cveGrupo, String cvePerfil,
			String indPaginacion) {
		logInfo("GL37 - Consulta de Perfiles");
		return ejecutaGL37Base(cveGrupo, cvePerfil, indPaginacion);
	}

}