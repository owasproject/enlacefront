package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import java.math.BigDecimal;

import mx.altec.enlace.bo.RET_ControlConsOper;
import mx.altec.enlace.bo.RET_CuentaAbonoVO;
import mx.altec.enlace.bo.RET_OperacionVO;
import mx.altec.enlace.bo.TrxGP72VO;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 *  Clase para el negocio de acceso a base de datos para la consulta de operaciones
 *  Reuters pactadas del d�a
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 10, 2012
 */
public class RET_LiberaOperDAO extends BaseServlet{

	/**
	 * Numero id de version serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Variable Connection para uso transaccional
	 */
	private Connection conn = null;

	/**
	 * Variable Statement para realizar batch
	 */
	private Statement stmt = null;

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y poner commit en false.
	 *
	 * @return conn				Connection de la transacci�n.
	 */
	public Connection initTransac(){
		try{
			conn = createiASConn (Global.DATASOURCE_ORACLE2);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::initConnTransac:: Problemas al crear la conexi�n", EIGlobal.NivelLog.INFO);
		}
		return conn;
	}

	/**
	 * Metodo para ejecutar una transacci�n en batch
	 * la transacci�n y poner commit en false.
	 *
	 * @return ejecBatch				Arreglo con ejecuci�n del batch
	 */
	public int[] execTransac(){
		int[] ejecBatch = null;
		try{
			ejecBatch = this.stmt.executeBatch();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::initConnTransac:: Problemas al ejecutar el batch->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
		return ejecBatch;
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta commit.
	 *
	 */
	public void closeTransac(){
		try{
			this.conn.commit();
			this.stmt.close();
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta rollback.
	 *
	 */
	public void closeTransacError(){
		try{
			this.conn.rollback();
			this.stmt.close();
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::closeConnTransac:: Problemas al cerrar la conexi�n con errores->"+e.getMessage(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de regresar los dias validos a cargar.
	 *
	 * @param formato					Formato de Consulta
	 * @return diasInhabiles			Vector con dias inhabiles
	 * @throws Exception				Error de tipo SQLException
	 */
	public Vector CargarDias(int formato)throws Exception{
		String query = null;
		ResultSet rs = null;
		Vector diasInhabiles = new Vector();

		EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::CargarDias:: Entrando a CargaDias", EIGlobal.NivelLog.DEBUG);
		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE );
			query  = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') "
				   + "From comu_dia_inhabil "
				   + "Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2) ";
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			while(rs.next()){
				String dia  = rs.getString(1);
				String mes  = rs.getString(2);
				String anio = rs.getString(3);
				if(formato == 0){
	                  dia  =  Integer.toString( Integer.parseInt(dia) );
	                  mes  =  Integer.toString( Integer.parseInt(mes) );
	                  anio =  Integer.toString( Integer.parseInt(anio) );
				}
				String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
				diasInhabiles.addElement(fecha);
			}
			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::CargarDias:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return diasInhabiles;
	}

	/**
	 * Metodo para realizar la consulta de los beneficiarios que se podran autorizar o consultar
	 *
	 * @param Where				    Tipo de Consulta: Beneficiario, Lote de Carga, Fecha de Carga
	 * @return beneficiariosCons	Lista de beneficiarios a consultar
	 */
	public Vector consulta(String Where, int indice)
	throws Exception {
		String query = null;
		ResultSet rs = null;
		RET_OperacionVO bean = null;
		int ind = 0;
		int numRegistros = 0;
		Vector OperacionesRETCons = new Vector();

		EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::consulta:: Entrando a consulta", EIGlobal.NivelLog.DEBUG);
		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "Select a.FOLIO_ENLA as folioEnla,"
				   + "a.FOLIO_RET as folioRet,"
				   + "a.CVE_TC_OPER as cveTcOper,"
				   + "a.CTA_CARGO as ctaCargo,"
				   + "a.DESC_CTA_CARGO as descCtaCargo,"
				   + "a.CONTRATO as contrato,"
				   + "a.USR_REGISTRO as usrRegistro,"
				   + "a.IMPORTE_TOT_OPER as importeTotOper,"
				   + "a.DIVISA_OPERANTE as divisaOperante,"
				   + "a.CONTRA_DIVISA as contraDivisa,"
				   + "a.TIPO_OPERACION as tipoOperacion,"
				   + "a.TC_BASE as tcBase,"
				   + "a.TC_PACTADO as tcPactado,"
				   + "a.TC_BASE_DIV1 as tcBaseDiv1,"
				   + "a.TC_VENTANILLA_DIV1 as tcVentanillaDiv1,"
				   + "a.TC_BASE_DIV1 as tcBaseDiv2,"
				   + "a.TC_VENTANILLA_DIV1 as tcVentanillaDiv2,"
				   + "a.ESTATUS_OPERACION as estatusOperacion,"
				   + "a.CONCEPTO as concepto,"
				   + "to_char(a.FCH_HORA_PACTADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrPactado, "
				   + "to_char(a.FCH_HORA_COMPLEMENT, 'DD-MM-YYYY HH24:MI:SS') as fchHrComplement "
				   + "From EWEB_OPER_REUTERS a "
				   + Where
				   + " order by a.FCH_HORA_PACTADO asc";
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::consulta:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query,ResultSet.TYPE_SCROLL_INSENSITIVE,
	        		ResultSet.CONCUR_UPDATABLE);
			rs = sDup.executeQuery();
			ind = getRecordDesde(indice);
			if(ind>0){
				rs.absolute(ind);
			}

			while(rs.next() && numRegistros<RET_ControlConsOper.MAX_REGISTROS_PAG){
				bean = new RET_OperacionVO();
				bean.setFolioEnla(rs.getString("folioEnla")!=null?rs.getString("folioEnla"):"");
				bean.setFolioRet(rs.getString("folioRet")!=null?rs.getString("folioRet"):"");
				bean.setCveTcOper(rs.getString("cveTcOper")!=null?rs.getString("cveTcOper"):"");
				bean.setCtaCargo(rs.getString("ctaCargo")!=null?rs.getString("ctaCargo"):"");
				bean.setDesCtaCargo(rs.getString("descCtaCargo")!=null?rs.getString("descCtaCargo"):"");
				bean.setContrato(rs.getString("contrato")!=null?rs.getString("contrato"):"");
				bean.setUsrRegistro(rs.getString("usrRegistro")!=null?rs.getString("usrRegistro"):"");
				bean.setImporteTotOper(rs.getString("importeTotOper")!=null?rs.getString("importeTotOper"):"");
				bean.setDivisaOperante(rs.getString("divisaOperante")!=null?rs.getString("divisaOperante"):"");
				bean.setContraDivisa(rs.getString("contraDivisa")!=null?rs.getString("contraDivisa"):"");
				bean.setTipoOperacion(rs.getString("tipoOperacion")!=null?rs.getString("tipoOperacion"):"");
				bean.setTcBase(rs.getString("tcBase")!=null?rs.getString("tcBase"):"");
				bean.setTcPactado(rs.getString("tcPactado")!=null?rs.getString("tcPactado"):"");
				bean.setTcBaseDiv1(rs.getString("tcBaseDiv1")!=null?rs.getString("tcBaseDiv1"):"");
				bean.setTcVentanillaDiv1(rs.getString("tcVentanillaDiv1")!=null?rs.getString("tcVentanillaDiv1"):"");
				bean.setTcBaseDiv2(rs.getString("tcBaseDiv2")!=null?rs.getString("tcBaseDiv2"):"");
				bean.setTcVentanillaDiv2(rs.getString("tcVentanillaDiv2")!=null?rs.getString("tcVentanillaDiv2"):"");
				bean.setEstatusOperacion(rs.getString("estatusOperacion")!=null?rs.getString("estatusOperacion"):"");
				bean.setConcepto(rs.getString("concepto")!=null?rs.getString("concepto"):"");
				bean.setFchHoraPactado(rs.getString("fchHrPactado")!=null?rs.getString("fchHrPactado"):"");
				bean.setFchHoraComplemento(rs.getString("fchHrComplement")!=null?rs.getString("fchHrComplement"):"");

				bean.setIdRecord("" + numRegistros++);
				OperacionesRETCons.add(bean);
			}

			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::consulta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return OperacionesRETCons;
	}

	/**
	 * Metodo encargado de identificar el registro inicial por pagina
	 *
	 * @param indice			Identificador de la pagina
	 * @return 					El valor del registro inicial de la pagina
	 */
	public int getRecordDesde(int indice) {
		return ((indice-1) * RET_ControlConsOper.MAX_REGISTROS_PAG);
	}

	/**
	 * Metodo funcional encargado de obtener el registro final por pagina
	 *
	 * @param indice				Identificador de la pagina
	 * @param numTotalPags			Total de Paginas
	 * @param numTotalRegistros		Total de registros
	 * @return						El valor del registro final de la pagina
	 */
	public int getRecordHasta(int indice, int numTotalPags, int numTotalRegistros) {
		if(indice==numTotalPags){
			return numTotalRegistros;
		}else{
			return indice * RET_ControlConsOper.MAX_REGISTROS_PAG;
		}
	}

	   /**
     * Metodo encargado de determinar el numero total de paginas de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalPags 	Total de paginas del query
     */
    public int getNumPages(String where)
    throws Exception {

    	ResultSet rs = null;
    	String query = null;
    	double numTotalRegs = 0;
    	int numTotalPags = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_OPER_REUTERS a ";
			query = query + where;
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::getNumPages:: Query->" + query, EIGlobal.NivelLog.DEBUG);

			if(rs.next()){
				numTotalRegs = rs.getDouble("numTotalRegs");
				numTotalPags = new Double(Math.ceil(numTotalRegs
							  / RET_ControlConsOper.MAX_REGISTROS_PAG)).intValue();
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de paginas");
		}
		return numTotalPags;
    }

    /**
     * Metodo encargado de determinar el numero total de registros de un query
     * ejecutado
     *
     * @param bean				Objeto contenedor de ConDevTesofeVo
     * @param where 			Condicion de la consulta entrante
     * @return numTotalRegs 	Total de registros
     */
	public int getNumRecords(String where)
	throws Exception {

		ResultSet rs = null;
    	String query = null;
    	int numTotalRegs = 0;

    	try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query = "Select count(*) as numTotalRegs "
		          + "from EWEB_OPER_REUTERS a ";
			query = query + where;
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::getNumRecords:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			if(rs.next()){
				numTotalRegs = rs.getInt("numTotalRegs");
			}

    	}catch (Exception e) {
    		throw new Exception("Error: de Base de datos, total de registros");
		}
		return numTotalRegs;
    }

    /**
     * M�todo que realiza el registro de los atributos del cargo de una operaci�n reuters
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     * @param numContrato 		Numero de Contrato
     * @param folioEnla			Folio Operaci�n Enlace
     * @return exito			Bandera que indica que no hubo problemas con la actualizaci�n del registro
     */
	public boolean registraCargoOper(RET_OperacionVO bean, String numContrato) {
		boolean exito = true;

		try {
			StringBuffer query = new StringBuffer("");

			query.append("Update EWEB_OPER_REUTERS Set ");
			query.append("FOLIO_ENLA = '" + bean.getFolioEnla() + "',");
			query.append("USR_AUTORIZA = '" + bean.getUsrAutoriza() + "',");
			query.append("CTA_CARGO = '" + bean.getCtaCargo() + "',");
			query.append("DESC_CTA_CARGO = '" + bean.getDesCtaCargo() +  "',");
			query.append("CONCEPTO = '" + bean.getConcepto() + "',");
			query.append("ESTATUS_OPERACION = 'C',");
			query.append("FCH_HORA_COMPLEMENT = to_date('" + bean.getFchHoraComplemento() + "','DD-MM-YYYY HH24:MI:SS') ");
			query.append("Where FOLIO_RET = '" + bean.getFolioRet() + "' ");
			query.append("And CONTRATO = '" + numContrato.trim() + "' ");
			query.append("And trunc(FCH_HORA_PACTADO) = trunc(SYSDATE)");
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraCargoOper:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			this.stmt.addBatch(query.toString());
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraCargoOper:: Saliendo...", EIGlobal.NivelLog.DEBUG);

			}catch (Exception e){
	    		EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraCargoOper:: Se controlo problema al registrar la parte del cargo, "
	    				+ "tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
	    		exito = false;
	    	}
		return exito;
	}

    /**
     * M�todo que realiza la inserci�n de las instrucciones de liquidaci�n dadas para una
     * operaci�n de reuters
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     * @param numContrato 		Numero de Contrato
     * @param folioEnla			Folio Operaci�n Enlace
     * @param pos				Posici�n del abono en la colecci�n de objetos
     */
    public boolean limpiaAbonos(RET_OperacionVO bean, String numContrato){
    	boolean exito = true;

    	try {
			StringBuffer query = new StringBuffer("");
			query.append("DELETE FROM EWEB_MONTOS_REUTERS ");
			query.append("Where FOLIO_ENLA = '" + bean.getFolioEnla() + "' ");
			query.append("And CONTRATO = '" + numContrato.trim() + "' ");
			query.append("And trunc(FCH_HORA_COMPLEMENT) = trunc(SYSDATE)");
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::limpiaAbonos:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			this.stmt.addBatch(query.toString());
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::limpiaAbonos:: Saliendo...",EIGlobal.NivelLog.DEBUG);

    	}catch (Exception e){
    		EIGlobal.mensajePorTrace("RET_LiberaOperDAO::limpiaAbonos:: Se controlo problema al insertar al batch "
    				+ "la operacion RET, tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
    		exito = false;
    	}
    	return exito;
    }
    /**
     * M�todo que realiza la inserci�n de las instrucciones de liquidaci�n dadas para una
     * operaci�n de reuters
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     * @param numContrato 		Numero de Contrato
     * @param folioEnla			Folio Operaci�n Enlace
     * @param pos				Posici�n del abono en la colecci�n de objetos
     */
    public boolean registraAbonoOper(RET_OperacionVO bean, String numContrato, int pos){
    	boolean exito = true;

    	try {
			StringBuffer query = new StringBuffer("");
			query.append("Insert into EWEB_MONTOS_REUTERS (FOLIO_OPER_LIQ, FOLIO_ENLA, CONTRATO, CUENTA_ABONO, IMPORTE,");
			query.append(" TIPO_CUENTA, CVE_ABA_SWIFT, FCH_HORA_COMPLEMENT, DESC_TITULAR)");
			query.append(" Values(");
			query.append("'");
			query.append(bean.getCveTcOper());
			query.append("','");
			query.append(bean.getFolioEnla());
			query.append("','");
			query.append(numContrato);
			query.append("','");
			query.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(pos)).getNumCuenta());
			query.append("','");
			query.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(pos)).getImporteAbono());
			query.append("','");
			query.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(pos)).getTipoCuenta());
			query.append("','");
			query.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(pos)).getCveAbaSwift());
			query.append("',");
			query.append("to_date('" + bean.getFchHoraComplemento() + "','DD-MM-YYYY HH24:MI:SS')");
			query.append(",'");
			query.append(((RET_CuentaAbonoVO)bean.getDetalleAbono().get(pos)).getBenefCuenta());
			query.append("')");
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraAbonoOper:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			this.stmt.addBatch(query.toString());
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraAbonoOper:: Saliendo...",EIGlobal.NivelLog.DEBUG);

    	}catch (Exception e){
    		EIGlobal.mensajePorTrace("RET_LiberaOperDAO::registraAbonoOper:: Se controlo problema al insertar al batch "
    				+ "la operacion RET, tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
    		exito = false;
    	}
    	return exito;
    }

    /**
     * M�todo que realiza la actualizaci�n del resultado de la liberaci�n de la
     * operaci�n de reuters que notifica el sistema de cambios 390 en tabla
     * Detalle
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     * @param folioEnla			Folio de Operaci�n Enlace
     * @param numContrato 		Numero de Contrato
     * @param numCuenta         Numero de Cuenta
     */
    public boolean liberaAbonoOper(TrxGP72VO bean, String folioEnla, String numContrato, String numCuenta){
		boolean exito = true;
		try{
			StringBuffer query = new StringBuffer("");

			query.append("Update EWEB_MONTOS_REUTERS Set ");
			if(bean.getOperacionExitosa()){
				query.append("FOLIO_ENLA = '" + folioEnla + "',");
				query.append("FOLIO_OPER_LIQ = '" + bean.getFolioOperacionPadre() + "',");
				query.append("FOLIO_OPER_ABON = '" + bean.getFolioOperacion() + "',");
				query.append("FOLIO_TRANSFER = '" + bean.getFolioTransfer() + "',");
				query.append("CUENTA_ABONO = '" + numCuenta + "',");
				query.append("ESTATUS_OPERACION = 'L' ");
			}else{
				query.append("FOLIO_ENLA = '" + folioEnla + "',");
				query.append("ESTATUS_OPERACION = 'N',");
				query.append("CUENTA_ABONO = '" + numCuenta + "' ");
			}
			query.append("Where FOLIO_ENLA = '" + folioEnla + "' ");
			query.append("And CONTRATO = '" + numContrato.trim() + "' ");
			query.append("And CUENTA_ABONO = '" + numCuenta.trim() + "' ");
			query.append("And trunc(FCH_HORA_COMPLEMENT) = trunc(SYSDATE)");
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaAbonoOper:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			this.stmt.addBatch(query.toString());
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaAbonoOper:: Saliendo...", EIGlobal.NivelLog.DEBUG);

			}catch (Exception e){
	    		EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaAbonoOper:: Se controlo problema al registrar la parte del cargo, "
	    				+ "tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
	    		exito = false;
	    	}
		return exito;
    }

    /**
     * M�todo que realiza la actualizaci�n del resultado de la liberaci�n de la
     * operaci�n de reuters que notifica el sistema de cambios 390 en tabla
     * Maestro
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     * @param numContrato 		Numero de Contrato
     * @param referencia		Referencia de liberaci�n
     */
    public boolean liberaCargoOper(RET_OperacionVO bean, String numContrato, boolean errorAbono){
		boolean exito = true;
		try{
			StringBuffer query = new StringBuffer("");

			query.append("Update EWEB_OPER_REUTERS Set ");
			if(errorAbono){
				query.append("ESTATUS_OPERACION = 'N',");
				query.append("USR_AUTORIZA = '" + bean.getUsrAutoriza() + "',");
			}else{
				query.append("FOLIO_ENLA = '" + bean.getFolioEnla() + "',");
				query.append("FOLIO_OPER_LIQ = '" + bean.getFolioOper() + "',");
				query.append("ESTATUS_OPERACION = 'L',");
				query.append("USR_AUTORIZA = '" + bean.getUsrAutoriza() + "',");
			}
			query.append("FCH_HORA_LIBERADO = to_date('" + bean.getFchHoraLiberado() + "','DD-MM-YYYY HH24:MI:SS') ");
			query.append("Where FOLIO_ENLA = '" + bean.getFolioEnla() + "' ");
			query.append("And CONTRATO = '" + numContrato.trim() + "' ");
			query.append("And trunc(FCH_HORA_PACTADO) = trunc(SYSDATE)");
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaCargoOper:: query->" + query.toString(),EIGlobal.NivelLog.DEBUG);

			this.stmt.addBatch(query.toString());
			EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaCargoOper:: Saliendo...", EIGlobal.NivelLog.DEBUG);

			}catch (Exception e){
	    		EIGlobal.mensajePorTrace("RET_LiberaOperDAO::liberaCargoOper:: Se controlo problema al registrar la parte del cargo, "
	    				+ "tipo de error->" + e.getMessage(),EIGlobal.NivelLog.INFO);
	    		exito = false;
	    	}
		return exito;
    }

    /**
     * M�todo para obtener el detalle del abono del registro seleccionado de
     * operaci�n de reuters
     *
     * @param bean				Objeto Contenedor RET_OperacionVO
     */
	public void obtenDetalleAbono(RET_OperacionVO bean){
		EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::obtenDetalleAbono:: Entrando a obtenDetalleAbono", EIGlobal.NivelLog.DEBUG);
		RET_CuentaAbonoVO beanIn = new RET_CuentaAbonoVO();
		Vector regs = new Vector<RET_CuentaAbonoVO>();
		String query = null;
		ResultSet rs = null;
		int idReg = 0;
		BigDecimal suma = new BigDecimal(0);

		try{
			Connection conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "SELECT a.CUENTA_ABONO as cuentaAbono, "
				   + "a.DESC_TITULAR as descTitular, "
				   + "a.IMPORTE as importe, "
				   + "a.TIPO_CUENTA as tipoCuenta, "
				   + "a.CVE_ABA_SWIFT as cveAbaSwift "
				   + "FROM EWEB_MONTOS_REUTERS a "
				   + "WHERE a.CONTRATO ='" + bean.getContrato() + "' "
				   + "AND a.FOLIO_ENLA ='" + bean.getFolioEnla() + "' ";

			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::obtenDetalleAbono:: query->"+query, EIGlobal.NivelLog.DEBUG);
			PreparedStatement sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();
			while(rs.next()){
				beanIn = new RET_CuentaAbonoVO();
				beanIn.setNumCuenta(rs.getString("cuentaAbono")!=null?rs.getString("cuentaAbono"):"");
				beanIn.setBenefCuenta(rs.getString("descTitular")!=null?rs.getString("descTitular"):"");
				beanIn.setImporteAbono(rs.getString("importe")!=null?rs.getString("importe"):"");
				beanIn.setTipoCuenta(rs.getString("tipoCuenta")!=null?rs.getString("tipoCuenta"):"");
				beanIn.setCveAbaSwift(rs.getString("cveAbaSwift")!=null?rs.getString("cveAbaSwift"):"");
				if(beanIn.getTipoCuenta().trim().equals(RET_CuentaAbonoVO.CUENTA_MISMO_BANCO))
					bean.setExistenCtaMB(true);
				else if(beanIn.getTipoCuenta().trim().equals(RET_CuentaAbonoVO.CUENTA_INTERNACIONAL))
					bean.setExistenCtaIB(true);
				suma = new BigDecimal(beanIn.getImporteAbono()).add(suma);

				beanIn.setIdRegistro(idReg);
				idReg++;
				regs.add(beanIn);
			}
			bean.setImporteTotAbono(suma.toString());
			bean.setDetalleAbono(regs);
			rs.close();
			sDup.close();
			conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("RET_LiberaOperDAO::obtenDetalleAbono:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	}
}