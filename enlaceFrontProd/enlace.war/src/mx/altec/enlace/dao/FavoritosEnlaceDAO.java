package mx.altec.enlace.dao;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


/**
 * DAO que realiza la iteracion con la tabla EWEB_MX_MAE_FAV_TRAN en la BD de
 * GFI.
 *
 * @author
 *
 */
public class FavoritosEnlaceDAO {

	/**
	 * Mensaje de error.
	 */
	protected static final String MENSAJE_ERROR = "ERROR: No se puede consultar los Favoritos. Intente mas tarde.";
	/**
	 * Query para consultar los favoritos de un cliente de acuerdo al modulo
	 * especifico..
	 */
	private static final String QUERY_CONSULTA_FAVORITOS = "Select * from EWEB_MX_MAE_FAV_TRAN where NUM_CONTR_PK = ? AND ID_MOD_LLAM = ? ";
	/**
	 * Query para actualizar un favorito.
	 */
	private static final String QUERY_UPDATE_FAVORITO = "UPDATE EWEB_MX_MAE_FAV_TRAN SET DSC_FAV_PK = ? WHERE NUM_CONTR_PK = ? AND COD_CLIEN_PK = ? AND ID_MOD_LLAM = ? AND NUM_CTA_CARGO = ? AND NUM_CTA_ABONO = ? AND ID_FAV_PK = ";
	/**
	 * Query para eliminar un favorito.
	 */
	private static final String QUERY_DELETE_FAVORITO = "DELETE EWEB_MX_MAE_FAV_TRAN WHERE NUM_CONTR_PK = ? AND COD_CLIEN_PK = ? AND ID_MOD_LLAM = ? AND NUM_CTA_CARGO = ? AND NUM_CTA_ABONO = ? AND ID_FAV_PK = ";
	/**
	 * Query para obtener la informacion adicional para una cuenta
	 * interbancaria.
	 */
	private static final String QUERY_COMPLEMENTAR_CUENTA = "select CVE_INTERME,U_VERSION,PLAZA_BANXICO,SUCURSAL from nucl_cta_externa where num_cuenta = ? and num_cuenta_ext = ?";
	/**
	 * Query para la consulta de favoritos paginado.
	 */
	private static final String QUERY_CONSULTA_FAVORITOS_PAGINADO = "select * from (select ROWNUM rnum , a.* from ( "
			+ QUERY_CONSULTA_FAVORITOS
			+ " ) a ) where rnum >= ? and rnum <= ? order by rnum,FCH_ALTA,NUM_CTA_CARGO ";

	/**
	 * Query para obtener el numero de favoritos.
	 */
	private static final String QUERY_CONSULTA_NUMERO_FAVORITOS = "Select count(1) from EWEB_MX_MAE_FAV_TRAN where NUM_CONTR_PK = ? AND ID_MOD_LLAM = ? ";

	/**
	 * Query para obtener el numero de favoritos.
	 */
	private static final String QUERY_CONSULTA_NUMERO_FAVORITOS_TOTAL = "Select max(ID_FAV_PK) from EWEB_MX_MAE_FAV_TRAN";

	/**
	 * Query para insertar la informacion de un nuevo favorito.
	 */
	private static final String QUERY_INSERT_FAVORITOS = "Insert into EWEB_MX_MAE_FAV_TRAN values (?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate)";

	/**
	 * Objeto que realiza la conexion a la BD.
	 */
	protected Connection conexion;
	/**
	 * Objeto statement el cual ejecuta los queries hacia la BD.
	 *
	 */
	protected Statement statement;
	/**
	 * Objeto ResultSet que recuperara la informacion de la tabla
	 * FAVORITOS_ENLACE.
	 */
	protected ResultSet resultSet;

	/**
	 * Objeto statement el cual recibe parametros para la consulta.
	 */
	protected PreparedStatement preparedStatement;

	/**
	 * Se encarga de crear la conexion a la Base de datos.
	 *
	 * @return Resultado de la conexion.
	 */
	protected boolean inicializarConexionBD() {
		boolean inicializado = false;

		try {
			conexion = BaseServlet
					.createiASConn_static(Global.DATASOURCE_ORACLE3);
			conexion.setAutoCommit(false);
			inicializado = true;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Error al inicializar la conexion en FavoritosEnlaceDAO utilizando: "
							+ Global.DATASOURCE_ORACLE + e.getMessage(),
					EIGlobal.NivelLog.INFO);
		}

		return inicializado;
	}

	/**
	 * Cierra las conexiones a la BD
	 *
	 * @return resultado de la operacion.
	 */
	protected boolean cerrarConexionBD() {
		boolean cerrado = false;
		EIGlobal.mensajePorTrace("Cerrando la conexion", EIGlobal.NivelLog.INFO);
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (resultSet != null) {
				resultSet.close();
			}
			if (conexion != null) {
				conexion.close();
			}
			cerrado = true;
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"Error al cerrar la conexion en FavoritosEnlaceDAO utilizando: "
							+ Global.DATASOURCE_ORACLE + e.getMessage(),
					EIGlobal.NivelLog.INFO);
		}

		return cerrado;
	}

	/**
	 * Realiza la consulta de los favoritos de acuerdo a los parametros
	 * recibidos.
	 *
	 * @param contrato
	 *            Contrato del cliente.
	 * @param codCliente
	 *            Codigo de cliente de los favoritos a consultar.
	 * @param idModulo
	 *            Id de modulo al que pertenece el favorito.
	 * @return Lista de favoritos encontrados.
	 * @throws SQLException
	 *             Posible error en la consulta de Favoritos.
	 */
	public List<FavoritosEnlaceBean> consultarFavoritos(String contrato,
			String codCliente, String idModulo) throws SQLException {

		FavoritosEnlaceBean favorito = null;
		List<FavoritosEnlaceBean> listaFavoritos = new ArrayList<FavoritosEnlaceBean>();
		boolean resultado = false;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Realizando la consulta con los siguientes datos -->"
						+ contrato + " " + codCliente + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);
		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_FAVORITOS);
				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);
				preparedStatement.setString(2, idModulo);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {

					favorito = new FavoritosEnlaceBean();

					favorito.setContrato(resultSet.getString("NUM_CONTR_PK"));
					favorito.setCodCliente(resultSet.getString("COD_CLIEN_PK"));
					favorito.setDescripcion(resultSet.getString("DSC_FAV_PK"));
					favorito.setIdFav("" + resultSet.getInt("ID_FAV_PK"));
					favorito.setIdModulo(resultSet.getString("ID_MOD_LLAM"));
					favorito.setCuentaCargo(resultSet
							.getString("NUM_CTA_CARGO"));
					favorito.setCuentaAbono(resultSet
							.getString("NUM_CTA_ABONO"));
					favorito.setDescCuentaCargo(resultSet
							.getString("DSC_CTA_CARGO"));
					favorito.setDescCuentaAbono(resultSet
							.getString("DSC_CTA_ABONO"));
					favorito.setTipo(resultSet.getString("DSC_TIPO"));
					favorito.setImporte(resultSet.getDouble("IMP_FAV"));
					favorito.setConceptoOperacion(resultSet
							.getString("DSC_CONCP"));
					favorito.setTipoTransaccion(resultSet.getString("ID_TRANS"));
					favorito.setFechaAlta(resultSet.getDate("FCH_ALTA"));

					listaFavoritos.add(favorito);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: EL numero de favoritos son: "
								+ listaFavoritos.size(), EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_FAVORITOS + ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}

		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return listaFavoritos;
	}

	/**
	 * Se encarga de dar de alta un nuevo favorito.
	 *
	 * @param favorito
	 *            Favorito a dar de alta.
	 * @return Resultado del insert del favorito.
	 */
	public boolean agregarFavorito(FavoritosEnlaceBean favorito) {
		boolean resultado = false;
		int idFav = 0;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va insertar un favorito para "
						+ favorito.getContrato() + " - "
						+ favorito.getCodCliente() + " con las cuenatas "
						+ favorito.getCuentaCargo() + " a "
						+ favorito.getCuentaAbono() + " con un importe de $"
						+ favorito.getImporte(), EIGlobal.NivelLog.INFO);
		resultado = inicializarConexionBD();

		if (resultado) {
			try {

				idFav = obtenerNumeroFavoritosTOTAL() + 1;

				if (inicializarConexionBD()) {
					preparedStatement = conexion
							.prepareStatement(QUERY_INSERT_FAVORITOS);

					conexion.setAutoCommit(false);
					preparedStatement.setString(1, favorito.getContrato());
					preparedStatement.setString(2, favorito.getCodCliente());															
					preparedStatement.setInt(4, idFav);
					preparedStatement.setString(5, favorito.getIdModulo());
					preparedStatement.setString(6, favorito.getCuentaCargo());
					preparedStatement.setString(7, favorito.getCuentaAbono());					
					preparedStatement.setString(10, favorito.getTipo());
					preparedStatement.setDouble(11, favorito.getImporte());
					preparedStatement.setString(13,
							favorito.getTipoTransaccion());
					aplicarEncoding(favorito);
					resultado = preparedStatement.execute();

					EIGlobal.mensajePorTrace("Se aplica el commit",
							EIGlobal.NivelLog.INFO);
					conexion.commit();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la insercion de un favorito: "
								+ QUERY_INSERT_FAVORITOS + ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace(
							"FavoritosEnlaceDAO: Error rollback "
									+ e1.getMessage(), EIGlobal.NivelLog.INFO);
				}
			} finally {
				cerrarConexionBD();
			}
		}

		return resultado;
	}
	
	/**
	 * Metodo para modificar encoding
	 * @since 14/08/2015
	 * @author FSW-Indra
	 * @param favorito informacion para el favorito
	 */
	public void aplicarEncoding(FavoritosEnlaceBean favorito){
		EIGlobal.mensajePorTrace("Entro aplicarEncoding", EIGlobal.NivelLog.INFO);
		try{
			if( "TRAN".equals( favorito.getIdModulo() ) || "PAGT".equals( favorito.getIdModulo() ) ){
				preparedStatement.setString(
						3,
						favorito.getDescripcion() == null ? "" : new String(favorito.getDescripcion().getBytes("ISO-8859-1"),"UTF-8") );
				preparedStatement.setString(8,
						new String(favorito.getDescCuentaCargo().getBytes("ISO-8859-1"),"UTF-8") );
				preparedStatement.setString(9,
						new String(favorito.getDescCuentaAbono().getBytes("ISO-8859-1"),"UTF-8") );
				preparedStatement.setString(12,
						new String(favorito.getConceptoOperacion().getBytes("ISO-8859-1"),"UTF-8") );
			} else {
				preparedStatement.setString(
						3,
						favorito.getDescripcion() == null ? "" : favorito.getDescripcion());
				preparedStatement.setString(8,
						favorito.getDescCuentaCargo() );
				preparedStatement.setString(9,
						favorito.getDescCuentaAbono() );
				preparedStatement.setString(12,
						favorito.getConceptoOperacion() );
			}			
		} catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Error encoding "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(
					"FavoritosEnlaceDAO: Error encoding "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace("Salio aplicarEncoding", EIGlobal.NivelLog.INFO);
	}

	/**
	 * Obtiene el total de toda la tabla de favoritos
	 *
	 * @return Numero de favoritos insertados.
	 * @throws SQLException
	 *             Excepcion de SQL
	 */
	private int obtenerNumeroFavoritosTOTAL() throws SQLException {

		int numeroFavoritos = 0;

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va a obtener el numero de favoritos ->",
				EIGlobal.NivelLog.INFO);

		if (inicializarConexionBD()) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_NUMERO_FAVORITOS_TOTAL);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					numeroFavoritos = resultSet.getInt(1);
				}

				EIGlobal.mensajePorTrace("EL numero de favoritos son: "
						+ numeroFavoritos, EIGlobal.NivelLog.INFO);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_NUMERO_FAVORITOS
								+ ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				throw e;
			} finally {
				cerrarConexionBD();
			}

		}
		return numeroFavoritos;

	}

	/**
	 * Obtiene el numero de favoritos para una consulta especifica.
	 *
	 * @param contrato
	 *            Contrato del cual se va a consultar sus favoritos.
	 * @param codCliente
	 *            Cliente del contrato que dio de alta los favoritos.
	 * @param idModulo
	 *            Id. del modulo del que pertenecen los favoritos.
	 * @return Numero de favoritos encontrados
	 * @throws SQLException
	 *             Algun posible error al realizar la consulta.
	 */
	public int obtenerNumeroFavoritos(String contrato, String codCliente,
			String idModulo) throws SQLException {

		int numeroFavoritos = 0;

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va a obtener el numero de favoritos para la siguiente consulta -->"
						+ contrato + " " + codCliente + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);

		if (inicializarConexionBD()) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_NUMERO_FAVORITOS);

				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);
				preparedStatement.setString(2, idModulo);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Ejecutando el query",
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					numeroFavoritos = resultSet.getInt(1);
				}

				EIGlobal.mensajePorTrace("EL numero de favoritos son: "
						+ numeroFavoritos, EIGlobal.NivelLog.INFO);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_NUMERO_FAVORITOS
								+ ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				throw e;
			} finally {
				cerrarConexionBD();
			}

		}
		return numeroFavoritos;
	}

	/**
	 * Obtiene los favoritos consultados por pagina.
	 *
	 * @param contrato
	 *            Contrato al que pertenecen los favoritos.
	 * @param codCliente
	 *            Cliente al que pertenecen los favoritos.
	 * @param idModulo
	 *            Modulo al que corresponden los favoritos.
	 * @param inicioPagina
	 *            Valor de la posicion incial de la pagina.
	 * @param finPagina
	 *            Valor de la posicion final de la pagina.
	 * @return Lista de favoritos por pagina seleccionada.
	 * @throws SQLException
	 *             Error generado ocasionado por la BD.
	 */
	public List<FavoritosEnlaceBean> obtenerFavoritosPaginados(String contrato,
			String codCliente, String idModulo, int inicioPagina, int finPagina)
			throws SQLException {

		boolean resultado = false;
		String aux = "";
		FavoritosEnlaceBean favorito = null;
		List<FavoritosEnlaceBean> listaFavoritos = new ArrayList<FavoritosEnlaceBean>();

		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va a obtener el numero de favoritos para la siguiente consulta -->"
						+ contrato + " " + codCliente + " " + idModulo + "<--",
				EIGlobal.NivelLog.INFO);

		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_CONSULTA_FAVORITOS_PAGINADO);

				preparedStatement.setString(1, contrato);
				//preparedStatement.setString(2, codCliente);
				preparedStatement.setString(2, idModulo);
				preparedStatement.setInt(3, inicioPagina);
				preparedStatement.setInt(4, finPagina);
				EIGlobal.mensajePorTrace("ARS_Favoritos_ARS",
						EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Ejecutando el query: "
						+ QUERY_CONSULTA_FAVORITOS_PAGINADO,
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				while (resultSet.next()) {
					favorito = new FavoritosEnlaceBean();
					favorito.setContrato(resultSet.getString("NUM_CONTR_PK"));
					favorito.setCodCliente(resultSet.getString("COD_CLIEN_PK"));
					favorito.setDescripcion(resultSet.getString("DSC_FAV_PK"));
					favorito.setIdFav("" + resultSet.getInt("ID_FAV_PK"));
					favorito.setIdModulo(resultSet.getString("ID_MOD_LLAM"));
					favorito.setCuentaCargo(resultSet
							.getString("NUM_CTA_CARGO"));
					favorito.setCuentaAbono(resultSet
							.getString("NUM_CTA_ABONO"));
					favorito.setDescCuentaCargo(resultSet
							.getString("DSC_CTA_CARGO"));
					favorito.setDescCuentaAbono(resultSet
							.getString("DSC_CTA_ABONO"));
					favorito.setTipo(resultSet.getString("DSC_TIPO"));
					favorito.setImporte(resultSet.getDouble("IMP_FAV"));
					favorito.setConceptoOperacion(resultSet
							.getString("DSC_CONCP"));
					favorito.setTipoTransaccion(resultSet.getString("ID_TRANS"));
					favorito.setFechaAlta(resultSet.getDate("FCH_ALTA"));
					listaFavoritos.add(favorito);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: EL numero de favoritos son: "
								+ listaFavoritos.size(), EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la consulta de los favoritos utilizando: "
								+ QUERY_CONSULTA_FAVORITOS_PAGINADO
								+ ": "
								+ e.getMessage(), EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}
		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return listaFavoritos;
	}

	/**
	 * Modifica los datos de un favorito
	 *
	 * @param favorito
	 *            Favorito a modificar.
	 *
	 */
	public void modificarFavorito(FavoritosEnlaceBean favorito) {
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va modificar un favorito para "
						+ favorito.getContrato() + " - "
						+ favorito.getCodCliente() + " con las cuenatas "
						+ favorito.getCuentaCargo() + " a "
						+ favorito.getCuentaAbono() + " con un importe de $"
						+ favorito.getImporte() + " descripcion "
						+ favorito.getDescripcion() + " indice "
						+ favorito.getIdFav(), EIGlobal.NivelLog.INFO);
		boolean resultado = inicializarConexionBD();

		if (resultado) {

			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_UPDATE_FAVORITO
								+ favorito.getIdFav());

				conexion.setAutoCommit(false);

				preparedStatement.setString(1, favorito.getDescripcion());
				preparedStatement.setString(2, favorito.getContrato());
				preparedStatement.setString(3, favorito.getCodCliente());
				preparedStatement.setString(4, favorito.getIdModulo());
				preparedStatement.setString(5, favorito.getCuentaCargo());
				preparedStatement.setString(6, favorito.getCuentaAbono());

				preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit al query "
						+ QUERY_UPDATE_FAVORITO, EIGlobal.NivelLog.INFO);
				conexion.commit();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la modificacion de un favorito: "
								+ QUERY_UPDATE_FAVORITO + e.getMessage(),
						EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace(
							"Erro al aplica el rolleback" + e1.getMessage(),
							EIGlobal.NivelLog.INFO);
				}
			} finally {
				cerrarConexionBD();
			}

		}

	}

	/**
	 * Eliminar el favorito seleccionado.
	 *
	 * @param favorito
	 *            Favorito a eliminar.
	 */
	public void eliminarFavorito(FavoritosEnlaceBean favorito) {
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Se va eliminar un favorito para "
						+ favorito.getContrato() + " - "
						+ favorito.getCodCliente() + " con las cuenatas "
						+ favorito.getCuentaCargo() + " a "
						+ favorito.getCuentaAbono() + " con un importe de $"
						+ favorito.getImporte(), EIGlobal.NivelLog.INFO);
		boolean resultado = inicializarConexionBD();

		if (resultado) {

			try {
				preparedStatement = conexion
						.prepareStatement(QUERY_DELETE_FAVORITO
								+ favorito.getIdFav());

				conexion.setAutoCommit(false);

				preparedStatement.setString(1, favorito.getContrato());
				preparedStatement.setString(2, favorito.getCodCliente());
				preparedStatement.setString(3, favorito.getIdModulo());
				preparedStatement.setString(4, favorito.getCuentaCargo());
				preparedStatement.setString(5, favorito.getCuentaAbono());

				preparedStatement.execute();

				EIGlobal.mensajePorTrace("Se aplica el commit",
						EIGlobal.NivelLog.INFO);
				conexion.commit();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Error al ejecutar la eliminacion de un favorito: "
								+ QUERY_DELETE_FAVORITO + e.getMessage(),
						EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Se aplica el rolleback",
						EIGlobal.NivelLog.INFO);
				try {
					conexion.rollback();
				} catch (SQLException e1) {
					EIGlobal.mensajePorTrace("Error al aplica el rolleback"
							+ e1.getMessage(), EIGlobal.NivelLog.INFO);
				}
			} finally {
				cerrarConexionBD();
			}

		}

	}

	/**
	 * Obtiene los datos complementarios de una cuenta externa.
	 *
	 * @param contrato
	 *            Contrato al que pertenece la cuenta.
	 * @param cuenta
	 *            Cuenta externa
	 * @return Datos adicionales.
	 * @throws SQLException
	 *             Excepcion de algun posible error.
	 */
	public String consultarDatosCuenta(String contrato, String cuenta)
			throws SQLException {
		String signo = "$";
		StringBuffer datosCuenta = new StringBuffer();
		boolean resultado = false;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Realizando la consulta del complemente de la cuenta -->"
						+ contrato + " " + cuenta, EIGlobal.NivelLog.INFO);
		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				String query = "select CVE_INTERME,U_VERSION,PLAZA_BANXICO,SUCURSAL from nucl_cta_externa where num_cuenta = '"
						+ contrato + "' and num_cuenta_ext = '" + cuenta + "'";
				preparedStatement = conexion.prepareStatement(query);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Ejecutando el query+ " + query,
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				if (resultSet.next()) {
					EIGlobal.mensajePorTrace("Entrando por el dato",
							EIGlobal.NivelLog.INFO);

					datosCuenta.append(resultSet.getString(1));
					datosCuenta.append(resultSet.getString(2));
					datosCuenta.append(resultSet.getString(3));
					datosCuenta.append(signo);
					datosCuenta.append(resultSet.getString(4));

				} else {
					EIGlobal.mensajePorTrace(
							"No existen registros adicionales para la cuenta: "
									+ cuenta, EIGlobal.NivelLog.INFO);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: EL complemento de la cuenta es: "
								+ datosCuenta, EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al ejecutar el complemento: "
						+ QUERY_COMPLEMENTAR_CUENTA + ":" + e.getMessage(),
						EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}

		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return datosCuenta.toString();
	}

	/**
	 * Obtiene la descripcion de la cuenta
	 *
	 * @param contrato
	 *            Contrato.
	 * @param cuenta
	 *            Cuenta
	 * @return Descripcion de la cuenta
	 * @throws SQLException
	 *             Error de excepcion.
	 */
	public String consultarDescCuenta(String contrato, String cuenta)
			throws SQLException {

		StringBuffer datosCuenta = new StringBuffer();
		boolean resultado = false;
		EIGlobal.mensajePorTrace(
				"FavoritosEnlaceDAO: Realizando la consulta del complemente de la cuenta -->"
						+ contrato + " " + cuenta, EIGlobal.NivelLog.INFO);
		resultado = inicializarConexionBD();
		if (resultado) {
			try {
				String query = "select DESCRIPCION from nucl_cuentas where num_cuenta = '"
						+ cuenta+ "'";
				preparedStatement = conexion.prepareStatement(query);

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: Ejecutando el query+ " + query,
						EIGlobal.NivelLog.INFO);
				resultSet = preparedStatement.executeQuery();

				if (resultSet.next()) {
					EIGlobal.mensajePorTrace("Entrando por el dato",
							EIGlobal.NivelLog.INFO);

					datosCuenta.append(resultSet.getString(1));

				} else {
					EIGlobal.mensajePorTrace(
							"No existen registros adicionales para la cuenta: "
									+ cuenta, EIGlobal.NivelLog.INFO);
				}

				EIGlobal.mensajePorTrace(
						"FavoritosEnlaceDAO: EL complemento de la cuenta es: "
								+ datosCuenta, EIGlobal.NivelLog.INFO);
				resultado = true;

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al ejecutar el complemento: "
						+ e.getMessage(),
						EIGlobal.NivelLog.INFO);
				resultado = false;

			} finally {
				resultado = cerrarConexionBD();
			}

		}

		if (!resultado) {
			throw new SQLException(MENSAJE_ERROR);
		}

		return datosCuenta.toString();
	}

	/**
	 * Obtiene la conexion.
	 *
	 * @return conexion
	 */
	public Connection getConexion() {
		return conexion;
	}

	/**
	 * Establece la conexion.
	 *
	 * @param conexion
	 *            Conexion establecer.
	 */
	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	/**
	 * Obtiene el objeto statement.
	 *
	 * @return objeto statement
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * Establece el objeto statement.
	 *
	 * @param statement
	 *            Statement a establecer.
	 */
	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	/**
	 * Obtiene el resultSet.
	 *
	 * @return resultSet
	 */
	public ResultSet getResultSet() {
		return resultSet;
	}

	/**
	 * Establece el resultSet
	 *
	 * @param resultSet
	 *            Resultset a establecet.
	 */
	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}

	/**
	 * Obtiene el preparedStatement.
	 *
	 * @return preparedStatement
	 */
	public PreparedStatement getPreparedStatement() {
		return preparedStatement;
	}

	/**
	 * Establece el preparedStatement.
	 *
	 * @param preparedStatement
	 *            preparedStatement a establecer.
	 */
	public void setPreparedStatement(PreparedStatement preparedStatement) {
		this.preparedStatement = preparedStatement;
	}

}