package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;


import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 *  Clase para el negocio de acceso a base de datos para el envio de confirmación
 *  de Autorización de Email
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Feb 26, 2007
 */
public class EmailDAO extends BaseServlet{

	/**
	 * Numero id de version serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Metodo encargado de obtener la dirección electronica del contrato
	 *
	 * @param numContrato		Numero de contrato
	 * @return emailCtr			Email del usuario del contrato
	 * @throws Exception		Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	public String getMailContract(String numContrato) throws Exception{
		String query = null;
		String emailCtr = null;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select email as emailCtr "
				  + "from tct_cuentas_tele "
				  + "where num_cuenta='" + numContrato + "'";
			EIGlobal.mensajePorTrace ("EmailDAO::getMailContract:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				emailCtr = rs.getString("emailCtr");
			}
			EIGlobal.mensajePorTrace ("EmailDAO::getMailContract:: -> Email CTR:" + emailCtr, EIGlobal.NivelLog.INFO);

		}catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return emailCtr;
	}

	/**
	 * Metodo encargado de obtener los lotes que son incidentes en la autorización de
	 * las cuentas dadas
	 *
	 * @param detalleCuentas		Vector con cuentas seleccionadas
	 * @return lotes				Lotes incidentes
	 * @throws Exception			Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	public String getLotes(Vector detalleCuentas)  throws Exception{
		String query = null;
		String lotes = null;
		String cuentas = null;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;
		boolean coma = true;

		try {
			for(int i=0; i<detalleCuentas.size(); i++){
				if(i==0){
					cuentas = "'" + ((EmailContainer)detalleCuentas.get(i)).getNumCuenta() + "'";
					continue;
				}
				cuentas += ",'" + ((EmailContainer)detalleCuentas.get(i)).getNumCuenta() + "'";
			}

			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "select distinct(id_lote) as lotes "
				  + "from eweb_aut_operctas "
				  + "where num_cuenta in (" + cuentas + ")";
			EIGlobal.mensajePorTrace ("EmailDAO::getLotes:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			while(rs.next()){
				if(coma){
					lotes = rs.getString("lotes");
				}else{
					if(rs.isLast()){
						lotes = " y " + rs.getString("lotes");
					}else{
						lotes = "," + rs.getString("lotes");
					}
				}
			}
		}catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return lotes;
	}

	/**
	 * Metodo encargado de obtener settear los folios de las cuentas elegidas
	 *
	 * @param detalleCuentas		Vector con cuentas seleccionadas
	 * @return detalleCuentas		Vector con folios correspondientes a cuentas
	 * @throws Exception			Error de tipo Exception
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	public Vector loadLotes(Vector detalleCuentas)  throws Exception{
		String query = null;
		String idLote = null;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			for(int i=0; i< detalleCuentas.size(); i++){
				query = "select id_lote as idLote "
					  + "from eweb_aut_operctas "
					  + "where num_cuenta ='"
					  + ((EmailContainer)detalleCuentas.get(i)).getNumCuenta() + "'";
				EIGlobal.mensajePorTrace ("EmailDAO::loadLotes:: -> Query:" + query, EIGlobal.NivelLog.INFO);
				rs = sDup.executeQuery(query);

				if(rs.next()){
					((EmailContainer)detalleCuentas.get(i)).setIdLote(rs.getString("idLote"));
					System.out.println("Aqui!!!!!!!!!");
					if(((EmailContainer)detalleCuentas.get(i)).getIdLote()==null
							|| ((EmailContainer)detalleCuentas.get(i)).getIdLote().equals("null")){
						((EmailContainer)detalleCuentas.get(i)).setIdLote("N/A");
					}
				}else{
					((EmailContainer)detalleCuentas.get(i)).setIdLote("N/A");
				}
			}
		}catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return detalleCuentas;
	}

	public static final void main(String[] args){
		EmailDAO dao = new EmailDAO();
		Vector test = new Vector();
		EmailContainer obj = new EmailContainer();
		obj.setNumCuenta("80000407711");
		obj.setTitular("asdasdas");
		test.add(obj);
		try{
			System.out.println(dao.getLotes(test));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}