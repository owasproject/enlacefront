/**
 * 
 */
package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement; 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;




import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;


/**
 * @author Stefanino
 *
 */
public class EpymeDAO {

    /**
     * QUERY_EPYME_OLD Varible para ejecutar query de seleccion de contrato Epyme de la tabla EWEB_MX_MAE_PYME.
     */
	private  static final String QUERY_EPYME_OLD = "SELECT ID_ESTAT FROM EWEB_MX_MAE_PYME WHERE NUM_CONTR_PK = '%s'" ;
    /**
     * QUERY_EPYME_NEW Varible para ejecutar query de seleccion de contrato Epyme de la tabla EWEB_PYME_CONTR.
     */	
	private  static final String QUERY_EPYME_NEW = "SELECT ID_ESTAT FROM EWEB_PYME_CONTR WHERE NUM_CONTR_PK = %s AND COD_CLIENTE_PK = %s" ;
	/**
	 *  TRAZA_SQL - sql.conection.close 
	 */
	private static final String TRAZA_SQL = "sql.conection.close ";
	/**
	 * Metodo generado para consultar si el contrato existe en la tabla EWEB_MX_MAE_PYME
	 * @param contrato contrato a consultar
	 * @return estatus del contrato String
	 */
	public String esContratoEpymeOld(String contrato) {
	    String estatus = "";
	    Connection conexion = null;
	    PreparedStatement  ps = null;
	    ResultSet rs = null;
		EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeOld::Realizando la consulta a la tabla EWEB_MX_MAE_PYME del contrato->" + contrato+"<-", EIGlobal.NivelLog.INFO);
	    try {
			conexion = GenericDAO.createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			ps = conexion.prepareStatement(String.format(QUERY_EPYME_OLD,contrato));
			EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeOld::Consulta::" + String.format(QUERY_EPYME_OLD,contrato) + "::", EIGlobal.NivelLog.INFO);
			rs = ps.executeQuery();
			if(rs.next()) {
				estatus=rs.getString("ID_ESTAT");
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeOld::es contrato pyme ->" + contrato + "<-", EIGlobal.NivelLog.INFO);
			}else {
				estatus="";
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeOld::NO es contrato pyme ->" + contrato + "<-", EIGlobal.NivelLog.INFO);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("EpymeDAO : Error SQL al intentar realizar la consulta--> " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}catch (NullPointerException e){			
			EIGlobal.mensajePorTrace("EpymeDAO : Error  al intentar realizar la consulta--> " + new Formateador().formatea(e) + " " +  contrato, EIGlobal.NivelLog.ERROR);
		}finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(ps != null){
					ps.close();
				}
				if(conexion != null){
					conexion.close();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeOld::Error al cerrar las conexiones --> " + new Formateador().formatea(e) + " " +  contrato, EIGlobal.NivelLog.ERROR);
			}
		}	    
	    EIGlobal.mensajePorTrace("EpymeDAO : Estatus del Contrato Epyme --> " + estatus,EIGlobal.NivelLog.INFO);	    
	    return estatus;		
	}
	/**
	 * Metodo generado para validar el estatus del contrato-usuario en la tabla EWEB_PYME_CONTR
	 * @param contrato contrato a consultar
	 * @param usuario usuario a consultar
	 * @return estatus estatus del contrato-usuario
	 */
	public String esContratoEpymeNew(String contrato, String usuario) {
	    String estatus = "";
	    Connection conexion = null;
	    PreparedStatement  ps = null;
	    ResultSet rs = null;
		EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::Realizando la consulta a la tabla EWEB_PYME_CONTR del contrato->" + contrato+"<-usuario:->:"+usuario+"<-", EIGlobal.NivelLog.INFO);
	    try {
			conexion = GenericDAO.createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			ps = conexion.prepareStatement(String.format(QUERY_EPYME_NEW, contrato, usuario));
			EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::Consulta::" + String.format(QUERY_EPYME_NEW, usuario, contrato)+"::", EIGlobal.NivelLog.INFO);
			rs = ps.executeQuery();
			if(rs.next()){
				estatus=rs.getString("ID_ESTAT");
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::es contrato pyme ->" + contrato + "<-usuario:->:" + usuario + "<-", EIGlobal.NivelLog.INFO);
			}else{
				estatus="";
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::NO es contrato pyme ->" + contrato + "<-usuario:->:" + usuario + "<-", EIGlobal.NivelLog.INFO);
			}
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::Error SQL al intentar realizar la consulta--> " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally{
			try {
				if(rs != null){
					rs.close();
				}
				if(ps != null){
					ps.close();
				}
				if(conexion != null){
					conexion.close();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::Error al cerrar las conexiones --> " + new Formateador().formatea(e) + " " +  contrato, EIGlobal.NivelLog.ERROR);
			}
		}	    
	    EIGlobal.mensajePorTrace("EpymeDAO::esContratoEpymeNew::Estatus del Contrato Epyme --> " + estatus,EIGlobal.NivelLog.INFO);	    
	    return estatus;		
	}
	
	/**
	 * Metodo generado para insertar el contrato-usuario en la tabla EWEB_PYME_CONTR
	 * @param contrato contrato a insertar
	 * @param usuario usuario a insertar
	 * @param estatus estatus a insertar
	 */
	public void insertaMenuMostrar(String contrato, String usuario, String estatus ) {
		EIGlobal.mensajePorTrace("Entra a insertaMenuMostrar: ",EIGlobal.NivelLog.DEBUG);
		PreparedStatement stmt2 = null;
		Connection conexion = null;
		StringBuilder query_bloqueo_u = null;
		try {
			conexion=GenericDAO.createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			if(conexion != null) {
				query_bloqueo_u = new StringBuilder();
				query_bloqueo_u.append("INSERT INTO EWEB_PYME_CONTR (NUM_CONTR_PK,COD_CLIENTE_PK,ID_ESTAT,FCH_MODIF) VALUES('").append(contrato).append("','").append(usuario).append("','").append(estatus).append("',to_date(sysdate,'DD/MM/RR'))");
				EIGlobal.mensajePorTrace("EpymeDAO::insertaMenuMostrar::INSERT A EJECUTAR::" + query_bloqueo_u.toString(),EIGlobal.NivelLog.DEBUG);
				stmt2 = conexion.prepareStatement(query_bloqueo_u.toString());
				stmt2.executeQuery();
			}
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(" OCURRIO UN ERROR " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (stmt2 != null) {
					stmt2.close();
				}
				GenericDAO.cierraConexion(conexion);
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(TRAZA_SQL + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
	}
	/**
	 * Metodo generado para actualizar el estatus contrato-usuario en la tabla EWEB_PYME_CONTR
	 * @param contrato contrato a actualizar
	 * @param usuario usuario a actualizar
	 * @param estatus estatus a actualizar
	 */
	public void actualizaMenuMostrar(String contrato, String usuario,String estatus) {
		EIGlobal.mensajePorTrace("Entra a actualizaMenuMostrar: ",EIGlobal.NivelLog.DEBUG);
		PreparedStatement stmt2 = null;
		Connection conexion = null;
		StringBuilder query_bloqueo_u = null;
		try{
			conexion=GenericDAO.createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			if(conexion != null) {
				query_bloqueo_u = new StringBuilder();
				query_bloqueo_u.append("UPDATE EWEB_PYME_CONTR SET ID_ESTAT = '").append(estatus).append("' WHERE NUM_CONTR_PK = '").append(contrato).append("' AND COD_CLIENTE_PK = '").append(usuario).append("'");
				EIGlobal.mensajePorTrace("EpymeDAO::insertaMenuMostrar::UPDATE A EJECUTAR EWEB_PYME_CONTR::" + query_bloqueo_u.toString(),EIGlobal.NivelLog.DEBUG);
				stmt2 = conexion.prepareStatement(query_bloqueo_u.toString());
				stmt2.executeUpdate();
			}
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(" OCURRIO UN ERROR " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				if (stmt2 != null) {
					stmt2.close();
				}
				GenericDAO.cierraConexion(conexion);
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(TRAZA_SQL + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
	}
}