package mx.altec.enlace.dao;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.beans.FiltroConsultaBean;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.beans.TrxLM1JBean;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

/**
 * @author ESC
 * Clase que realiza la conexi&oacuten a la BD
 */
public class EnlcTarjetaDAOImpl extends NomPreMQDAO implements EnlcTarjetaDAO {
	
	/**
	 * Log
	 */
	private static final String TEXTOLOG = EnlcTarjetaDAOImpl.class.getName();
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * and
	 */
	private static final String AND = " AND ";
	/**
	 * COMILLA
	 */
	private static final String COMILLA = "'";
	
	/**
	 * Variable Connection para uso transaccional
	 */
	private Connection conn = null;
	
	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y poner commit en false.
	 *
	 * @return conn				Connection de la transacci�n.
	 */
	public Connection initTransac(){
		try{
			this.conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("AsignacionEmplDAO::initConnTransac:: Problemas al crear la conexi�n", EIGlobal.NivelLog.INFO);
		}
		return conn;
	}
	
	/**
	 * Metodo que crea una conexi�n de base de datos a oracle para inicializar
	 * la transacci�n y ejecuta commit.
	 *
	 */
	public void closeTransac(){
		try{
			this.conn.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("AsignacionEmplDAO::closeConnTransac:: Problemas al cerrar la conexi�n", EIGlobal.NivelLog.INFO);
		}
	}
	

	/**
	 * Metodo que consulta tarjetas en la BD EWEB_VINC_TARJETACON de acuerdo a los filtros
	 * @param filtros para ejecutar la consulta
	 * @return TarjetaContrato bean
	 * @throws DaoException controlada
	 */
	public List<TarjetaContrato> consultarDatosTarjetas(FiltroConsultaBean filtros)throws DaoException{
		EIGlobal.mensajePorTrace(TEXTOLOG + "consultarDatosTarjetas DAO", EIGlobal.NivelLog.INFO);
		String query = null;
		ResultSet rs = null;
		Statement sDup = null;		
		TarjetaContrato tajeta = null;
		ArrayList<TarjetaContrato> tarRes;
		tarRes = new ArrayList<TarjetaContrato>();
		try{
			
			sDup = this.conn.createStatement();
			query = generaQuery(filtros);
			EIGlobal.mensajePorTrace(TEXTOLOG + "consultarDatosTarjetas DAO " + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			while(rs.next()){
				tajeta = new TarjetaContrato();
				tajeta.setTarjeta(rs.getString("NUM_TARJETA"));
				tajeta.setContrato(rs.getString("CONTRATO"));
				tajeta.setEstatus(rs.getString("ESTATUS_OPERACION"));
				tajeta.setIdLote(rs.getString("ID_LOTE"));
				tajeta.setFechaUltAcceso(rs.getString("FCH_ULT_ACCESO"));
				tajeta.setUsuarioReg(rs.getString("USR_REGISTRO"));
				tarRes.add(tajeta);
			}
			
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::consultarDatosTarjetas:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			e.printStackTrace();
			throw new DaoException(e);
		} finally {
			try {
				rs.close();
				sDup.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::consultarDatosTarjetas:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
		}
		return tarRes;
	}
	
	/**
	 * Genera query de consulta
	 * @param filtros consulta
	 * @return Cadena query
	 */
	private String generaQuery(FiltroConsultaBean filtros){
		boolean bandera = false;
		StringBuffer sb;
		sb = new StringBuffer();
		sb.append("Select * from EWEB_VINC_TARJETACON WHERE ");
		
		if(filtros.getTarjeta() != null && !"".equals(filtros.getTarjeta())){
			sb.append("NUM_TARJETA = ").append(COMILLA).append(filtros.getTarjeta()).append(COMILLA);
			bandera = true;
		}
		if(filtros.getContrato() != null && !"".equals(filtros.getContrato())){
			if(bandera){
				sb.append(AND);
			}
			sb.append("CONTRATO = ").append(COMILLA).append(filtros.getContrato()).append(COMILLA);
			bandera = true;
		}
		if(filtros.getEstatus() != null && !"".equals(filtros.getEstatus()) && !"T".equals(filtros.getEstatus())){
			if(bandera){
				sb.append(AND);
			}
			sb.append("ESTATUS_OPERACION = ")
			.append(COMILLA).append(filtros.getEstatus()).append(COMILLA);
			bandera = true;
		}
		if(filtros.getIdLote() != null && !"".equals(filtros.getIdLote())){
			if(bandera){
				sb.append(AND);
			}
			sb.append("ID_LOTE = ")
			  .append(filtros.getIdLote());
			bandera = true;
		}
		if((filtros.getFechaInicio() != null && filtros.getFechaFin() != null) && 
				(!"".equals(filtros.getFechaInicio()) && !"".equals(filtros.getFechaFin()))){
			if(bandera){
				sb.append(AND);
			}
			sb.append("FCH_ULT_ACCESO BETWEEN ").append("to_date('").append(filtros.getFechaInicio().toString())
			  .append("', 'DD/MM/YYYY')").append(AND).append("(SELECT to_date('").append(filtros.getFechaFin().toString())
			  .append("', 'DD/MM/YYYY') + 1 FROM DUAL)");
			
		}
		return sb.toString();
	}
	
	/**
	 * Metodo que elimina tarjetas en la BD EWEB_VINC_TARJETACON de acuerdo a los parametros
	 * @param tarjeta a eliminar
	 * @return int entero del resultado de la eliminacion
	 * @throws DaoException controlada 
	 */
	public int eliminarTarjeta(TarjetaContrato tarjeta)throws DaoException{
		EIGlobal.mensajePorTrace(TEXTOLOG + "eliminarTarjeta DAO", EIGlobal.NivelLog.INFO);
		String query = null;
		Connection conn = null;
		Statement statement = null;
		int rs = 0;

		try{
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			StringBuffer sb;
			sb = new StringBuffer();
			sb.append("DELETE FROM EWEB_VINC_TARJETACON WHERE ")
			  .append("NUM_TARJETA = '")
			  .append(tarjeta.getTarjeta())
			  .append("' AND CONTRATO = '")
			  .append(tarjeta.getContrato())
			  .append(COMILLA);
			
			query = sb.toString();
			statement = conn.prepareStatement(query);
			rs = statement.executeUpdate(query);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::eliminarTarjeta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			throw new DaoException(e);
		} finally {
			try {
				statement.close();
				conn.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::eliminarTarjeta:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
		} 
		
		return rs;
	}
	
	
	/**
	 * Metodo que agrega una tarjeta a la BD EWEB_VINC_TARJETACON
	 * @param tarjeta que se agrega
	 * @return int resultado del insert
	 * @throws DaoException controlada
	 */
	public int agregarTarjetaContrato(TarjetaContrato tarjeta)throws DaoException{
		EIGlobal.mensajePorTrace(TEXTOLOG + "agregarTarjetaContrato DAO", EIGlobal.NivelLog.INFO);
		String query = null;
		Connection conn = null;
		Statement statement = null;
		int rs = 0;

		try{
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);

			StringBuffer sb;
			sb = new StringBuffer();
			sb.append("INSERT INTO EWEB_VINC_TARJETACON")
			  .append(" (CONTRATO, NUM_TARJETA, ESTATUS_OPERACION, ID_LOTE, FCH_ULT_ACCESO, USR_REGISTRO)")
			  .append(" VALUES ('")
			  .append(tarjeta.getContrato())
			  .append("', '")
			  .append(tarjeta.getTarjeta())
			  .append("', '")
			  .append(tarjeta.getEstatus())
			  .append("', ")
			  .append(tarjeta.getIdLote())
			  .append(", current_timestamp")
			  .append(", '")
			  .append(tarjeta.getUsuarioReg())
			  .append("')");
			
			query = sb.toString();
			statement = conn.prepareStatement(query);
			rs = statement.executeUpdate(query);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::agregarTarjetaContrato:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			throw new DaoException(e);
		} finally {
			try {
				statement.close();
				conn.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::agregarTarjetaContrato:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
		}
		return rs;
	}
	
	/**
	 * Metodo encargado de regresar el id de la secuencia de alta de cuentas
	 *
	 * @return idLote					Id del lote de carga
	 * @throws DaoException				Error de tipo DaoException
	 */
	public Long getIdLoteAltaSequence()throws DaoException{
		EIGlobal.mensajePorTrace(TEXTOLOG + "getIdLoteAltaSequence DAO", EIGlobal.NivelLog.INFO);
		String query = null;
		ResultSet rs = null;
		Long idLote = null;
		Connection conn = null;
		Statement sDup = null;

		try{
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			sDup = conn.createStatement();

			query  = "Select SEQ_VINC_NOMN1.nextval as idLote "
				   + "From dual ";
			rs = sDup.executeQuery(query);

			if(rs.next()){
				idLote = rs.getLong("idLote");
			}
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::getIdLoteAltaSequence:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			throw new DaoException(e);
		} finally {
			try {				
				rs.close();
				sDup.close();
				conn.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(TEXTOLOG + "EnlcTarjetaDAO::getIdLoteAltaSequence:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}
		}
		return idLote;
	}
	
	/**
	 * Metodo para generar la consulta de tarjetas
	 * @param bean para la transacci&oacute;n LM1J
	 * @return lista de tarjetas
	 */
	public TrxLM1JBean consultaLM1J(TrxLM1JBean bean) {
		EIGlobal.mensajePorTrace(TEXTOLOG + "consultaLM1J DAO", EIGlobal.NivelLog.INFO);
		StringBuffer sb;
		sb = new StringBuffer()
		.append(rellenar(bean.getTarjeta(), 22)) 			//TARJETA
		.append(rellenar(bean.getNombreEmpleado(), 30)) 	//NOMEMPL
		.append(rellenar(bean.getApellidoPat(), 30)) 		//APEPATE
		.append(rellenar(bean.getApellidoMat(), 30)) 		//APEMATE
		.append(rellenar(bean.getRfc(), 10)) 				//RFC
		.append(rellenar(bean.getHomoclave(), 3)) 			//HOMOCLA
		.append(rellenar(bean.getFechaNac(), 10)) 			//FECNACI
		.append(rellenar(bean.getTarjetaPag(), 22)); 		//NUMTARP
		EIGlobal.mensajePorTrace(TEXTOLOG + "TrxLM1JBean - Consulta LM1J", EIGlobal.NivelLog.INFO);
		return consulta(TrxLM1JBean.HEADER, sb.toString(), TrxLM1JBean.getFactoryInstance());
	}
}
