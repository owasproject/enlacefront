/**
*   Isban Mexico
*   Clase: ConsultaCtasDAO.java
*   Descripcion: Objeto de acceso a datospara el modulo de Administracion y control de Cuentas.
*
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid).
*/

package mx.altec.enlace.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.beans.CuentaInterbancariaBean;
import mx.altec.enlace.beans.CuentaInternacionalBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.beans.CuentasSantanderBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
/**
 * clase creada para el modulo de Administracion y control -> Cuentas -> Consulta
 * @author Z708764 JORGE OLALDE
 *
 */
public class ConsultaCtasDAO extends GenericDAO{

	/**
	 * NOMBRE: NOMBRE
	 */
	private static final String NOMBRE = "NOMBRE";
	/**
	 * NUMERO_CUENTA: CUENTA
	 */
	private static final String NUMERO_CUENTA = "CUENTA";
	/**
	 * BANCO: BANCO
	 */
	private static final String BANCO = "BANCO";
	/**
	 * CVE_BANCO: "CVE_BANCO"
	 */
	private static final String CVE_BANCO = "CVE_BANCO";
	/**
	 * SUCURSAL: SUCURSAL
	 */
	private static final String SUCURSAL = "SUCURSAL";
	/**
	 * PLAZA: PLAZA
	 */
	private static final String PLAZA = "PLAZA";
	/**
	 * SWIFT: SWIFT
	 */
	private static final String SWIFT = "SWIFT";
	/**
	 * PAIS: PAIS
	 */
	private static final String PAIS = "PAIS";
	/**
	 * variable DIVISA: DIVISA
	 */
	private static final String DIVISA = "DIVISA";
	/**
	 * ABA: ABA
	 */
	private static final String ABA = "ABA";
	/**
	 * CIUDAD: CIUDAD
	 */
	private static final String CIUDAD = "CIUDAD";
	/**
	 * PRODUCTO: PRODUCTO
	 */
	private static final String PRODUCTO="PRODUCTO";
	/**
	 * SUBPRODUCTO: SUBPRODUCTO
	 */
	private static final String SUBPRODUCTO="SUBPRODUCTO";
	/**
	 * PERSONA_JURIDICA: PERSONA_JURIDICA
	 */
	private static final String PERSONA_JURIDICA="PERSONA_JURIDICA";
	/**
	 * TIPO_RELAC_CUENTAS: TIPO_RELAC_CUENTAS
	 */
	private static final String TIPO_RELAC_CUENTAS="TIPO_RELAC_CUENTAS";
	/**
	 * DIVISA: DIVISA
	 */
	private static final String CVE_PROD_ENL = "CVE_PROD_ENL";
	/**
	 * TITULAR: "TITULAR"
	 */
	private static final String TITULAR = "TITULAR";
	/**
	 * TOTAL: "TOTAL"
	 */
	private static final String TOTAL = "TOTAL";
	/** DIR_BENEF */
	private static final String DIR_BENEF = "DIR_BENEF";
	/** CIUDAD_BENEF */
	private static final String CIUDAD_BENEF = "CIUDAD_BENEF";
	/** ID_CLIENTE_BENEF */
	private static final String ID_CLIENTE_BENEF = "ID_CLIENTE_BENEF";
	/** PAIS_BENEF */
	private static final String PAIS_BENEF = "PAIS_BENEF";
	/**
	 * NUMCUENTA "NumCuenta"
	 */
	private static final String NUMCUENTA="NumCuenta";
	/**
	 * PORCENTAJE = "'%'";
	 */
	private static final String PORCENTAJE = "'%'";
	/**
	 * NOMBREX "Nombre"
	 */
	private static final String NOMBREX="Nombre";
	/**
	 * ERROR "Error al obtener los datos ";
	 */
	private static final String ERROR="Error al obtener los datos ";
	/**
	 * ERRORFINAL "Error al finalizar "
	 */
	private static final String ERRORFINAL="Error al finalizar ";
	/**
	 * TMP ".tmp";
	 */
	private static final String TMP=".tmp";
	/**
	 * TRAZA "ConsultaCtasDAO::consultaCuentaSantanderBean -> No se puede cerrar el archivo:";
	 */
	private static final String TRAZA="ConsultaCtasDAO::consultaCuentaSantanderBean -> No se puede cerrar el archivo:";
	/**
	 * SELECT_CTAS_TOTALES_INICIO: SELECT tabla_general.* FROM (SELECT ROWNUM AS INDICE, tabla.* FROM(
	 */
	private static final String SELECT_CTAS_TOTALES_INICIO = new StringBuilder(
	"	SELECT tabla_general.* ").append(
	"	FROM (	SELECT ROWNUM AS INDICE, tabla.* ").append(
	"	FROM(").toString();
	/**
	 * SELECT_CTAS_TOTALES_FIN:  )tabla WHERE ROWNUM <= %s )tabla_general WHERE INDICE > %s
	 */
	private static final String SELECT_CTAS_TOTALES_FIN = new StringBuilder(
	"   )tabla WHERE ROWNUM <= %s ").append(
	"   )tabla_general WHERE INDICE > %s ").append(
	"   ").toString();
	/**
	 * SELECT_CTAS_INTERBANCARIAS_TOTAL: SELECT COUNT(*) AS TOTAL FROM NUCL_CTA_EXTERNA WHERE NUM_CUENTA = %s
	 */
	private static final String SELECT_CTAS_INTERBANCARIAS_TOTAL = new StringBuilder(
	"   SELECT COUNT(*) AS TOTAL FROM NUCL_CTA_EXTERNA A LEFT OUTER JOIN COMU_INTERME_FIN B ON(A.CVE_INTERME=B.CVE_INTERME) LEFT OUTER JOIN COMU_PLA_BANXICO C ON (A.PLAZA_BANXICO = C.PLAZA_BANXICO) WHERE  LENGTH(TRIM(A.NUM_CUENTA_EXT)) <> 10 and A.NUM_CUENTA = %s %n").toString();
	/**
	 * SELECT_CTAS_INTERNACIONALES_TOTAL: SELECT COUNT(*) AS TOTAL FROM EWEB_CTAS_INTER WHERE NO_CONTRATO = %s AND TIPO_MOVIMIENTO NOT IN ('B')
	 */
	private static final String SELECT_CTAS_INTERNACIONALES_TOTAL = new StringBuilder(
	"   SELECT COUNT(*) AS TOTAL FROM EWEB_CTAS_INTER WHERE NO_CONTRATO = %s AND TIPO_MOVIMIENTO NOT IN ('B')%n").toString();
	/**
	 * SELECT_CTAS_SANTANDER_TOTAL: SELECT COUNT(A.NUM_CUENTA) AS TOTAL FROM NUCL_RELAC_CTAS A INNER JOIN TCT_PROD_ENLACE B ON(A.N_PRODUCT=B.CVE_PROD_ORG(+)) %n WHERE A.NUM_CUENTA2 = %s AND LENGTH(TRIM(A.NUM_CUENTA)) <> 10
	 */
	private static final String SELECT_CTAS_SANTANDER_TOTAL = new StringBuilder(
	"   SELECT COUNT(A.NUM_CUENTA) AS TOTAL FROM NUCL_RELAC_CTAS A INNER JOIN TCT_PROD_ENLACE B ON(A.N_PRODUCT=B.CVE_PROD_ORG(+)) %n WHERE A.NUM_CUENTA2 = %s AND LENGTH(TRIM(A.NUM_CUENTA)) <> 10 %n").toString();
	/**
	 * SELECT_CTAS_MOVILES_TOTAL: SELECT NOMBRE, NUM_CUENTA_EXT, CVE_INTERME FROM EWEB_CAT_NOM_EXTERNA WHERE LENGTH(trim(NUM_CUENTA_EXT))=10 ORDER BY NUM_CUENTA_EXT
	 * SELECT count(*) as TOTAL FROM(
	 * SELECT a.num_cuenta as CUENTA, a.nombre_titular as TITULAR, cve_producto AS BANCO, cve_producto2 as CVE_BANCO
	 * from nucl_relac_ctas a
	 * where length(trim(a.num_cuenta)) = 10 and num_cuenta2 = %s
	 * AND a.Num_cuenta like %s AND a.Nombre_Titular like %s
	 * union
	 * select b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme
	 * from nucl_cta_externa b, comu_interme_fin c
	 * where b.cve_interme = c.cve_interme
	 * and length(trim(b.num_cuenta_ext)) = 10 and num_cuenta =  %s
	 * AND b.Num_cuenta_Ext like %s AND b.Nombre like %s
	 */
	private static final String SELECT_CTAS_MOVILES_TOTAL = new StringBuilder(
	"SELECT count(*) as TOTAL FROM( %n").append(
	"	SELECT a.num_cuenta as CUENTA, a.nombre_titular as TITULAR, cve_producto AS BANCO, cve_producto2 as CVE_BANCO %n").append(
	"	from nucl_relac_ctas a %n").append(
    "	where length(trim(a.num_cuenta)) = 10 and num_cuenta2 = %s %n").append(
    "   AND a.Num_cuenta like %s AND a.Nombre_Titular like %s %n").append(
    "   union %n").append(
    "   	select b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme %n").append(
    "			from nucl_cta_externa b left outer join comu_interme_fin c on(b.cve_interme = c.cve_interme)%n").append(
    "				where length(trim(b.num_cuenta_ext)) = 10 and num_cuenta =  %s %n").append(
    "				AND b.Num_cuenta_Ext like %s AND b.Nombre like %s %n").append(
    ")").toString();

	/**
	 * SELECT_CTAS_INTERBANCARIAS_PAGINADO: SELECT A.NUM_CUENTA_EXT AS CUENTA, B.NOMBRE_CORTO AS BANCO, A.NOMBRE, A.PLAZA_BANXICO AS PLAZA, A.SUCURSAL FROM NUCL_CTA_EXTERNA A INNER JOIN COMU_INTERME_FIN B ON(A.CVE_INTERME=B.CVE_INTERME) WHERE NUM_CUENTA = %s
	 */
	private static final String SELECT_CTAS_INTERBANCARIAS_PAGINADO = new StringBuilder(
			"   SELECT length(trim(NUM_CUENTA_EXT)) as longitud,A.NUM_CUENTA_EXT AS CUENTA, B.NOMBRE_CORTO AS BANCO, A.NOMBRE, C.DESCRIPCION AS PLAZA, A.SUCURSAL, A.CVE_DIVISA AS divisa %n").append(
			"	FROM NUCL_CTA_EXTERNA A LEFT OUTER JOIN COMU_INTERME_FIN B ON(A.CVE_INTERME=B.CVE_INTERME) LEFT OUTER JOIN COMU_PLA_BANXICO C ON (A.PLAZA_BANXICO = C.PLAZA_BANXICO) %n").append(
			"	WHERE  LENGTH(TRIM(A.NUM_CUENTA_EXT)) <> 10   AND NUM_CUENTA = %s %n").toString();
	/**
	 * SELECT_CTAS_INTERBANCARIAS_FILTRO_CUENTA: AND NUM_CUENTA_EXT like %s
	 */
	private static final String SELECT_CTAS_INTERBANCARIAS_FILTRO_CUENTA = new StringBuilder(
			" AND A.NUM_CUENTA_EXT like %s %n" ).toString();
	/**
	 * SELECT_CTAS_INTERBANCARIAS_FILTRO_NOMBRE: AND NOMBRE like %s
	 */
	private static final String SELECT_CTAS_INTERBANCARIAS_FILTRO_NOMBRE = new StringBuilder(
			" AND A.NOMBRE like %s %n" ).toString();
	/**
	 * SELECT_CTAS_INTERBANCARIAS_FILTRO_BANCO: AND CVE_INTERME LIKE %s
	 */
	private static final String SELECT_CTAS_INTERBANCARIAS_FILTRO_BANCO = new StringBuilder(
			" AND B.NOMBRE_CORTO LIKE %s %n" ).toString();
	/**
	 * SELECT_CTAS_INTERNACIONAL_PAGINADO: SELECT NO_CTA AS CUENTA, CVE_SWIFT AS SWIFT, CVE_PAIS AS PAIS, CVE_DIVISA AS DIVISA, CVE_ABA AS ABA, DESC_CIUDAD AS CIUDAD, DESC_BANCO AS BANCO, DESC_TITULAR AS NOMBRE, DIR_BEN_REC AS DIR_BENEF, CIUDAD_REC AS CIUDAD_BENEF, ID_CLIENTE_REC AS ID_CLIENTE_BENEF, CVE_PAIS_BENEF AS PAIS_BENEF FROM EWEB_CTAS_INTER WHERE NO_CONTRATO = %s AND TIPO_MOVIMIENTO NOT IN ('B')
	 */
	private static final String SELECT_CTAS_INTERNACIONAL_PAGINADO = new StringBuilder(
	"   SELECT length(trim(NO_CTA)) as longitud,NO_CTA AS CUENTA, CVE_SWIFT AS SWIFT, CVE_PAIS AS PAIS, CVE_DIVISA AS DIVISA, CVE_ABA AS ABA, DESC_CIUDAD AS CIUDAD, DESC_BANCO AS BANCO, DESC_TITULAR AS NOMBRE, DIR_BEN_REC AS DIR_BENEF, CIUDAD_REC AS CIUDAD_BENEF, ID_CLIENTE_REC AS ID_CLIENTE_BENEF, CVE_PAIS_BENEF AS PAIS_BENEF %n").append(
	"	FROM EWEB_CTAS_INTER %n").append(
	"	WHERE NO_CONTRATO = %s %n").append(
	"	AND TIPO_MOVIMIENTO NOT IN ('B') %n").toString();
	/**
	 * SELECT_CTAS_INTERNACIONAL_FILTRO_CUENTA: AND NO_CTA like %s
	 */
	private static final String SELECT_CTAS_INTERNACIONAL_FILTRO_CUENTA = new StringBuilder(
	" AND NO_CTA like %s %n" ).toString();
	/**
	 * SELECT_CTAS_INTERNACIONAL_FILTRO_NOMBRE: AND DESC_TITULAR like %s
	 */
	private static final String SELECT_CTAS_INTERNACIONAL_FILTRO_NOMBRE = new StringBuilder(
	" AND DESC_TITULAR like %s %n" ).toString();
	/**
	 * SELECT_CTAS_SANTANDER_PAGINADO: SELECT A.NUM_CUENTA AS CUENTA, A.N_DESCRIPCION AS NOMBRE, A.N_PRODUCT AS PRODUCTO, A.N_SUBPROD AS SUBPRODUCTO, A.N_PER_JUR AS PERSONA_JURIDICA, A.TIPO_RELAC_CTAS AS TIPO_RELAC_CUENTAS, B.CVE_PROD_ENL AS CVE_PROD_ENL FROM NUCL_RELAC_CTAS A INNER JOIN TCT_PROD_ENLACE B ON(A.N_PRODUCT=B.CVE_PROD_ORG(+)) WHERE A.NUM_CUENTA2 = %s AND LENGTH(TRIM(A.NUM_CUENTA)) <> 10
	 */
	private static final String SELECT_CTAS_SANTANDER_PAGINADO = new StringBuilder(
	"   SELECT length(trim(A.NUM_CUENTA)) as longitud,A.NUM_CUENTA AS CUENTA, A.N_DESCRIPCION AS NOMBRE, A.N_PRODUCT AS PRODUCTO, A.N_SUBPROD AS SUBPRODUCTO, A.N_PER_JUR AS PERSONA_JURIDICA, A.TIPO_RELAC_CTAS AS TIPO_RELAC_CUENTAS, B.CVE_PROD_ENL AS CVE_PROD_ENL %n").append(
	"	FROM NUCL_RELAC_CTAS A INNER JOIN TCT_PROD_ENLACE B ON(A.N_PRODUCT=B.CVE_PROD_ORG(+)) %n").append(
	"	WHERE A.NUM_CUENTA2 = %s  %n").append(
	"	AND LENGTH(TRIM(A.NUM_CUENTA)) <> 10 %n").toString();
	/**
	 * SELECT_CTAS_SANTANDER_FILTRO_CUENTA: AND A.NUM_CUENTA like %s
	 */
	private static final String SELECT_CTAS_SANTANDER_FILTRO_CUENTA = new StringBuilder(
	" AND A.NUM_CUENTA like %s %n" ).toString();
	/**
	 * SELECT_CTAS_SANTANDER_FILTRO_NOMBRE: AND A.N_DESCRIPCION like %s
	 */
	private static final String SELECT_CTAS_SANTANDER_FILTRO_NOMBRE = new StringBuilder(
	" AND A.N_DESCRIPCION like %s %n" ).toString();
	/**
	 * ORDENAMIENTO_MISMO_BANCO "ORDER BY longitud, A.NUM_CUENTA"
	 */
	private static final String ORDENAMIENTO_MISMO_BANCO = new StringBuilder(	"  ORDER BY longitud, A.NUM_CUENTA " ).toString();
	/**
	 * ORDENAMIENTO_INTERBANCARIO "ORDER BY longitud,NUM_CUENTA_EXT "
	 */
	private static final String ORDENAMIENTO_INTERBANCARIO = new StringBuilder(	"  ORDER BY longitud,NUM_CUENTA_EXT " ).toString();
	/**
	 * ORDENAMIENTO_INTERNACIONAL "  ORDER BY longitud,NO_CTA "
	 */
	private static final String ORDENAMIENTO_INTERNACIONAL = new StringBuilder(	"  ORDER BY longitud,NO_CTA " ).toString();




	/**
	 * SELECT_CTAS_PAGINADO:
	 * SELECT tabla_general.* FROM (
	 * SELECT ROWNUM AS INDICE, tabla.* FROM(
	 * SELECT a.num_cuenta AS CUENTA, a.nombre_titular AS TITULAR,
	 * cve_producto AS BANCO, cve_producto2 AS CVE_BANCO
	 * FROM nucl_relac_ctas a
	 * WHERE LENGTH(trim(a.num_cuenta)) = 10 AND num_cuenta2 = %s
	 * AND a.Num_cuenta like %s AND a.Nombre_Titular like %s
	 * UNION
	 * SELECT b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme
	 * FROM nucl_cta_externa b left outer join comu_interme_fin c on(b.cve_interme = c.cve_interme)
	 * WHERE  LENGTH(trim(b.num_cuenta_ext)) = 10 AND num_cuenta = %s
	 * AND b.Num_cuenta_Ext like %s AND b.Nombre like %s
	 * )tabla WHERE ROWNUM <=  %s
	 * )tabla_general WHERE INDICE >  %s
	 */
	private static final String SELECT_CTAS_MOVILES_PAGINADO = new StringBuilder(
			"SELECT tabla_general.* FROM ( %n").append(
			"SELECT ROWNUM AS INDICE, length(trim(CUENTA)) as longitud, tabla.* FROM( %n").append(
			"	SELECT a.num_cuenta AS CUENTA, a.nombre_titular AS TITULAR,  %n").append(
			"		cve_producto AS BANCO, cve_producto2 AS CVE_BANCO  %n").append(
			"	FROM nucl_relac_ctas a %n").append(
		    "	WHERE LENGTH(trim(a.num_cuenta)) = 10 AND num_cuenta2 = %s %n").append(
		    "   AND a.Num_cuenta like %s AND a.Nombre_Titular like %s %n").append(
		    "   UNION %n").append(
		    "   	SELECT b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme %n").append(
		    "			FROM nucl_cta_externa b left outer join comu_interme_fin c on(b.cve_interme = c.cve_interme) %n").append(
		    "				WHERE  LENGTH(trim(b.num_cuenta_ext)) = 10 AND num_cuenta = %s %n").append(
		    "				AND b.Num_cuenta_Ext like %s AND b.Nombre like %s %n").append(
		    "				 ORDER BY CUENTA )tabla WHERE ROWNUM <=  %s %n").append(
		    "				)tabla_general WHERE INDICE >  %s %n").toString();


	/**
	 * Metodo consultaCuentaInterbancariaBean realiza la consulta total o paginada de cuentas interbancarias
	 * @param consultaCuentaInterbancariaBean bean que contiene los parametros para la consulta
	 * @return listaResultado List<CuentaInterbancariaBean> lista de bean con el resultado de la consulta
	 */
	public List<CuentaInterbancariaBean> consultaCuentaInterbancariaBean(CuentaInterbancariaBean consultaCuentaInterbancariaBean){

		List<CuentaInterbancariaBean> listaResultado = null;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;
		String banco=PORCENTAJE;

		queryMaster.append(SELECT_CTAS_TOTALES_INICIO);
		contrato = armaCriterio(consultaCuentaInterbancariaBean);
		queryMaster.append(String.format(SELECT_CTAS_INTERBANCARIAS_PAGINADO, contrato));
		if (NUMCUENTA.equals(consultaCuentaInterbancariaBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_CUENTA, armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaInterbancariaBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_NOMBRE, armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(nombre);
		}else if ("Banco".equals(consultaCuentaInterbancariaBean.getCriterio())) {
			banco = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_BANCO,armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(banco);
		}
		queryMaster.append(ORDENAMIENTO_INTERBANCARIO);
		queryMaster.append(String.format(SELECT_CTAS_TOTALES_FIN,consultaCuentaInterbancariaBean.getRegFin(),consultaCuentaInterbancariaBean.getRegIni()));

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){
				listaResultado  = new ArrayList<CuentaInterbancariaBean>();
				EIGlobal.mensajePorTrace("QUERY SANTANDER A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				CuentaInterbancariaBean resultado = null;
				/***
				CONTRATO         bean
				NUM_CUENTA_EXT   CUENTA
				NOMBRE           NOMBRE
				CVE_INTERME      BANCO
				PLAZA_BANXICO    PLAZA
				SUCURSAL         SUCURSAL
				REGISTRO_FIN     bean
				REGISTRO_INICIO  bean
				**/
				 while (result.next ()) {
					 resultado = new CuentaInterbancariaBean();
					 resultado.setTotalRegistro(result.getFetchSize());
					 resultado.setnumeroInterbancaria(validaYFormateaCampo(result.getString(NUMERO_CUENTA)));
					 resultado.setdescripcion(validaYFormateaCampo(result.getString(NOMBRE)));
					 resultado.setbanco(validaYFormateaCampo(result.getString(BANCO)));
					 resultado.setplaza(validaYFormateaCampo(result.getString(PLAZA)));
					 resultado.setsucursal(validaYFormateaCampo(result.getString(SUCURSAL)));
					 resultado.setDivisa(validaYFormateaCampo(result.getString(DIVISA)));
					 listaResultado.add(resultado);
				 }
			}
			cierraConexion(conn);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas Santander terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return listaResultado;
	}

	/**
	 * Metodo consultaCuentaInternacionalBean realiza la consulta total o paginada de cuentas internacionales
	 * @param consultaCuentaInternacionalBean bean que contiene los parametros para realizar la consulta
	 * @return listaResultado List<CuentaInternacionalBean> lista de bean con el resultado de la consulta
	 */
	public List<CuentaInternacionalBean> consultaCuentaInternacionalBean(CuentaInternacionalBean consultaCuentaInternacionalBean){

		List<CuentaInternacionalBean> listaResultado = null;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;
		queryMaster.append(SELECT_CTAS_TOTALES_INICIO);
		contrato = armaCriterioInternacional(consultaCuentaInternacionalBean);
		queryMaster.append(String.format(SELECT_CTAS_INTERNACIONAL_PAGINADO, contrato));
		if (NUMCUENTA.equals(consultaCuentaInternacionalBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_INTERNACIONAL_FILTRO_CUENTA, armaFiltroInternacional(consultaCuentaInternacionalBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaInternacionalBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_INTERNACIONAL_FILTRO_NOMBRE, armaFiltroInternacional(consultaCuentaInternacionalBean));
			queryMaster.append(nombre);
		}
		queryMaster.append(ORDENAMIENTO_INTERNACIONAL);
		queryMaster.append(String.format(SELECT_CTAS_TOTALES_FIN,consultaCuentaInternacionalBean.getRegFin(),consultaCuentaInternacionalBean.getRegIni()));

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			if(conn != null){
				listaResultado  = new ArrayList<CuentaInternacionalBean>();

				EIGlobal.mensajePorTrace("QUERY INTERNACIONAL A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				CuentaInternacionalBean resultado = null;
				/***
				NO_CTA AS CUENTA
				CVE_SWIFT AS SWIFT
				CVE_PAIS AS PAIS
				CVE_DIVISA AS DIVISA
				CVE_ABA AS ABA
				DESC_CIUDAD AS CIUDAD
				DESC_BANCO AS BANCO
				DESC_TITULAR AS NOMBRE
				**/
				 while (result.next ()) {
					 resultado = new CuentaInternacionalBean();
					 resultado.setnumeroInternacional(validaYFormateaCampo(result.getString(NUMERO_CUENTA)));
					 resultado.setswift(validaYFormateaCampo(result.getString(SWIFT)));
					 resultado.setpais(validaYFormateaCampo(result.getString(PAIS)));
					 resultado.setdivisa(validaYFormateaCampo(result.getString(DIVISA)));
					 resultado.setaba(validaYFormateaCampo(result.getString(ABA)));
					 resultado.setciudad(validaYFormateaCampo(result.getString(CIUDAD)));
					 resultado.setbanco(validaYFormateaCampo(result.getString(BANCO)));
					 resultado.settitular(validaYFormateaCampo(result.getString(NOMBRE)));
					 resultado.setDireccionBenef(validaYFormateaCampo(result.getString(DIR_BENEF)));
					 resultado.setCiudadBenef(validaYFormateaCampo(result.getString(CIUDAD_BENEF)));
					 resultado.setPaisBenef(validaYFormateaCampo(result.getString(PAIS_BENEF)));
					 resultado.setIdClienteBenef(validaYFormateaCampo(result.getString(ID_CLIENTE_BENEF)));
					 listaResultado.add(resultado);
				 }
			}
			cierraConexion(conn);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas Internacional terminada exitosamente 2-----------> " , EIGlobal.NivelLog.DEBUG);
		return listaResultado;
	}
	/**
	 * Metodo consultaCuentaSantanderBean que realiza la consulta total o paginada de cuentas mismo banco (Santander)
	 * @param consultaCuentaSantanderBean bean que contiene los parametros para la consulta
	 * @return listaResultadoClabe List<CuentasSantanderBean> lista de beans que contiene el resultado de la consulta y las cuentas clabe
	 */
	public List<CuentasSantanderBean> consultaCuentaSantanderBean(CuentasSantanderBean consultaCuentaSantanderBean){

		List<CuentasSantanderBean> listaResultado = null;
		List<CuentasSantanderBean> listaResultadoClabe = null;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;
		queryMaster.append(SELECT_CTAS_TOTALES_INICIO);
		contrato = armaCriterioSantander(consultaCuentaSantanderBean);
		queryMaster.append(String.format(SELECT_CTAS_SANTANDER_PAGINADO, contrato));
		if (NUMCUENTA.equals(consultaCuentaSantanderBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_SANTANDER_FILTRO_CUENTA, armaFiltroSantander(consultaCuentaSantanderBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaSantanderBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_SANTANDER_FILTRO_NOMBRE, armaFiltroSantander(consultaCuentaSantanderBean));
			queryMaster.append(nombre);
		}


		queryMaster.append(ORDENAMIENTO_MISMO_BANCO);
		queryMaster.append(String.format(SELECT_CTAS_TOTALES_FIN,consultaCuentaSantanderBean.getRegFin(),consultaCuentaSantanderBean.getRegIni()));

		String fileName = consultaCuentaSantanderBean.getUSUARIO();
		String rutaArchivo = IEnlace.DOWNLOAD_PATH + fileName;
		EIGlobal.mensajePorTrace("Ruta de archivo Santander::"+rutaArchivo+TMP+"::" , EIGlobal.NivelLog.DEBUG);
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){
				listaResultado  = new ArrayList<CuentasSantanderBean>();

				EIGlobal.mensajePorTrace("QUERY INTERNACIONAL A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				CuentasSantanderBean resultado = null;
				FileWriter archivoTmp = null;
				BufferedWriter archivoEscritura = null;
				File archivo = null;
				StringBuffer linea = null;
				try {
					archivo = new File(rutaArchivo+TMP);
					// si el archivo existe se borra
					if (archivo.exists()) {
						archivo.delete();
					}
					archivoTmp = new FileWriter(rutaArchivo+TMP);
					archivoEscritura = new BufferedWriter(archivoTmp);
					linea = null;
				/***
				NUM_CUENTA AS CUENTA
				A.N_DESCRIPCION AS NOMBRE
				A.N_PRODUCT AS PRODUCTO
				A.N_SUBPROD AS SUBPRODUCTO
				A.N_PER_JUR AS PERSONA_JURIDICA
				A.TIPO_RELAC_CTAS AS TIPO_RELAC_CUENTAS
				B.CVE_PROD_ENL AS CVE_PROD_ENL
				**/
				 while (result.next ()) {
					 linea = new StringBuffer();
					 resultado = new CuentasSantanderBean();
					 resultado.setNumCuenta(validaYFormateaCampo(result.getString(NUMERO_CUENTA)));
					 resultado.setDescripcion(validaYFormateaCampo(result.getString(NOMBRE)));
					 resultado.setNProduct(validaYFormateaCampo(result.getString(PRODUCTO)));
					 resultado.setNSubprod(validaYFormateaCampo(result.getString(SUBPRODUCTO)));
					 resultado.setNPerJur(validaYFormateaCampo(result.getString(PERSONA_JURIDICA)));
					 resultado.setTIPO_RELAC_CUENTAS(validaYFormateaCampo(result.getString(TIPO_RELAC_CUENTAS)));
					 if (("P").equals(resultado.getTIPO_RELAC_CUENTAS())) {
						 resultado.setTipoRelacCtas("Propia");
			          } else if (("T").equals(resultado.getTIPO_RELAC_CUENTAS())) {
			        	 resultado.setTipoRelacCtas("Terceros");
			          } else {
			        	  resultado.setTipoRelacCtas("03");
			          }
					 resultado.setCVE_PROD_ENL(validaYFormateaCampo(result.getString(CVE_PROD_ENL)));
					 /**Escribe al archivo que se enviar al servidor tuxedo clabesrvr**/
					 if(resultado.getNumCuenta().length()==11){
						 linea.append(validaYFormateaCampo(result.getString(NUMERO_CUENTA)));
						 linea.append("\n");
						 archivoEscritura.write(linea.toString());
					 }
					 listaResultado.add(resultado);
				 }
				 archivoEscritura.close();
				 archivoTmp.close();
                 } catch (IOException e) {
                	 EIGlobal.mensajePorTrace("ConsultaCtasDAO::consultaCuentaSantanderBean -> Error al generar el archivo de para consulta de cuenta clabe:" +e.getMessage() +"::", EIGlobal.NivelLog.DEBUG);
                                 if(archivoTmp!=null){
                                                 try {
                                                	 archivoTmp.close();
                                                 } catch (IOException e1) {
                                                	 EIGlobal.mensajePorTrace(TRAZA +e1.getMessage() +"::", EIGlobal.NivelLog.DEBUG);
                                                 }
                                 }
                                 if(archivoEscritura!=null){
                                	 try{
                                		 archivoEscritura.close();
                                	 }catch(IOException e2){
                                		 EIGlobal.mensajePorTrace(TRAZA +e2.getMessage() +":::", EIGlobal.NivelLog.DEBUG);
                                	 }
                                 }
                 }finally{
                     if(archivoTmp!=null){
                         try {
                        	 archivoTmp.close();
                         } catch (IOException e1) {
                        	 EIGlobal.mensajePorTrace(TRAZA +e1.getMessage() +"::", EIGlobal.NivelLog.DEBUG);
                         }
                     }
                     if(archivoEscritura!=null){
                    	 try{
                    		 archivoEscritura.close();
                    	 }catch(IOException e2){
                    		 EIGlobal.mensajePorTrace(TRAZA +e2.getMessage() +":::", EIGlobal.NivelLog.DEBUG);
                    	 }
                     }
                 }
			}
			cierraConexion(conn);
			listaResultadoClabe=consultaCuentaSantanderClabe(listaResultado, rutaArchivo, consultaCuentaSantanderBean.getUSUARIO(), consultaCuentaSantanderBean.getContrato());
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas Internacional terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return listaResultadoClabe;
	}
	/**
	 * Valida y formatea campo.
	 *
	 * @param campo the campo
	 * @return the string
	 */
	private String validaYFormateaCampo(String campo){
		String valor = "";
		if (campo!=null) {
			valor = campo.trim();
		}
		return valor;
	}
	/**
	 * Se encarga de armar los criterios de busqueda de cuentas interbancarias
	 * @param consultaCuentaInterbancariaBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaCriterio(CuentaInterbancariaBean consultaCuentaInterbancariaBean){
		final StringBuilder contrato = new StringBuilder();
		contrato.append("'").append(consultaCuentaInterbancariaBean.getContrato()).append("'");
		return contrato.toString();
	}
	/**
	 * Se encarga de armar los criterios de busqueda de cuentas Internacionales
	 * @param consultaCuentaInternacionalBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaCriterioInternacional(CuentaInternacionalBean consultaCuentaInternacionalBean){
		final StringBuilder contrato = new StringBuilder();
		contrato.append("'").append(consultaCuentaInternacionalBean.getContrato()).append("'");
		return contrato.toString();
	}
	/**
	 * Se encarga de armar los criterios de busqueda de cuentas mismo banco (Santander)
	 * @param consultaCuentaSantanderBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaCriterioSantander(CuentasSantanderBean consultaCuentaSantanderBean){
		final StringBuilder contrato = new StringBuilder();
		contrato.append("'").append(consultaCuentaSantanderBean.getContrato()).append("'");
		return contrato.toString();
	}
	/**
	 * se encarga de armar el filtro de busqueda de cuentas interbancarias
	 * @param consultaCuentaInterbancariaBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaFiltro(CuentaInterbancariaBean consultaCuentaInterbancariaBean){
		final StringBuilder where = new StringBuilder();
		where.append("'%").append(consultaCuentaInterbancariaBean.getTextoBuscar()).append("%'");
		return where.toString();
	}
	/**
	 * se encarga de armar el filtro de busqueda de cuentas internacionales
	 * @param consultaCuentaInternacionalBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaFiltroInternacional(CuentaInternacionalBean consultaCuentaInternacionalBean){
		final StringBuilder where = new StringBuilder();
		where.append("'%").append(consultaCuentaInternacionalBean.getTextoBuscar()).append("%'");
		return where.toString();
	}
	/**
	 * se encarga de armar el filtro de busqueda de cuentas mismo banco(Santander)
	 * @param consultaCuentaSantanderBean bean que contiene los parametros a evaluar
	 * @return contrato contrato a utilizar en la consulta
	 */
	private String armaFiltroSantander(CuentasSantanderBean consultaCuentaSantanderBean){
		final StringBuilder where = new StringBuilder();
		where.append("'%").append(consultaCuentaSantanderBean.getTextoBuscar()).append("%'");
		return where.toString();
	}
	/**
	 * Metodo consultaCuentaSantanderClabe que consulta el servidor tuxedo clabesrvr para traer las cuentas clabe
	 * @param consultaCuentaSantanderBean bean que contiene los parametros para realizar la consulta
	 * @param rutaArchivo ruta de archivo
	 * @param usuario usuario due�o del contrato
	 * @param contrato contrato que utiliza el usuario
	 * @return List<CuentasSantanderBean> consultaCuentaSantanderBean lista de beans con el resultado de la consulta
	 */
	@SuppressWarnings("unchecked")
	public List<CuentasSantanderBean> consultaCuentaSantanderClabe(List<CuentasSantanderBean> consultaCuentaSantanderBean, String rutaArchivo, String usuario, String contrato){
		Map<String, CuentasSantanderBean> lista = new TreeMap<String, CuentasSantanderBean>();
		String tramaEntrada   = "";
		String tramaSalida    = "";
		String path_archivo   = "";
		String nombre_archivo = "";
		String fileOut        = "";
		String registro       = "";
		String cadAux         = "";
		@SuppressWarnings("unused")
		String elError        = "";
		tramaEntrada = "2EWEB|" + usuario + "|ECBE|" + contrato + "|" + usuario +	"|" + rutaArchivo + TMP + "|";
		ServicioTux tuxGlobal = new ServicioTux();
		EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class >> tecb :" + tramaEntrada + " <<", NivelLog.DEBUG);
		fileOut = usuario + TMP;
		ArchivoRemoto archR = new ArchivoRemoto();
		if(!archR.copiaLocalARemoto(fileOut)) {
			EIGlobal.mensajePorTrace("***consultaCuentaSantanderClabe.class No se pudo crear archivo para exportar claves", NivelLog.INFO);
		}
		try{
			Map hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
			path_archivo = tramaSalida;
			EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class trama_salida >>" + tramaSalida + "<<", NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re ){
			path_archivo = rutaArchivo;
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), NivelLog.INFO);
		} catch (Exception e) {
			path_archivo = rutaArchivo;
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
		}

		BufferedReader fileSua = null;
		try{
			nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());
			EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class & nombre_archivo_cb:" + nombre_archivo + " &", NivelLog.INFO);
			// Copia Remota
			archR = new ArchivoRemoto();
			if(!archR.copia(nombre_archivo)) {
				EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class & consultaClaves: No se realizo la copia remota. &", NivelLog.INFO);
			} else {
				EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class & consultaClaves: Copia remota OK. &", NivelLog.INFO);
			}
			path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo; // +"0";
			EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class & path_archivo_cb:" + path_archivo + " &", NivelLog.INFO);
			File drvSua = new File(path_archivo);
			fileSua = new BufferedReader(new FileReader(drvSua) );// Modificacion RMM 20021218
	 		int i = 0;
	 		String numeroCuenta=null;
	 		for ( CuentasSantanderBean b : consultaCuentaSantanderBean ) {
	 			b.setCLABE("");
	 			lista.put(b.getNumCuenta(), b);
	 		}
	 			while(null!=(registro = fileSua.readLine())){
	 				numeroCuenta=EIGlobal.BuscarToken(registro, '|', 1).trim();
	 				CuentasSantanderBean temp = lista.get(numeroCuenta);
	 				temp.setArchivo(nombre_archivo);
	 				temp.setTramaSalida(tramaSalida);
	 				EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class cuenta de archivo::"+(EIGlobal.BuscarToken(registro, '|', 1).trim())+"::", NivelLog.INFO);
	 				EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class cuenta de bean::"+temp.getNumCuenta()+"::", NivelLog.INFO);

					try{
	 				if(!"5".equals(temp.getNProduct()) && !"4".equals(temp.getNProduct())&&temp.getNumCuenta().trim().length()==11){//TODO
							EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class se cumple condicion::", NivelLog.INFO);
							cadAux = registro.substring(0, 6).trim();
							elError= registro.substring(16, registro.trim().length());
						}
						if (!(("CORR00").equals(cadAux) || ("ERROR3").equals(cadAux))) {
							try{

								Long.parseLong(EIGlobal.BuscarToken(registro, '|', 3));
								temp.setCLABE((!"5".equals(temp.getNProduct()) && !"4".equals(temp.getNProduct()) ? EIGlobal.BuscarToken(registro, '|', 3) : ""));
							}catch(NumberFormatException ex){
								temp.setCLABE("");
								EIGlobal.mensajePorTrace( "***consultaCuentaSantanderClabe.class valor dentro del NumberFormatException::"+EIGlobal.BuscarToken(registro, '|', 3)+"::", NivelLog.INFO);
							}
						}else {
							EIGlobal.mensajePorTrace( "***ContCtas_AL.class & consultaClavesExportar & Error al generar archivo de exportaci�n.", NivelLog.INFO);
						}
		 			EIGlobal.mensajePorTrace("Cuentas Santander = "+temp.getNumCuenta(), NivelLog.INFO);
		 			EIGlobal.mensajePorTrace("Propietario Cuentas Santander = "+temp.getDescripcion(), NivelLog.INFO);
					}catch(Exception e){

					}
		 			i++;
	 			}
	 		EIGlobal.mensajePorTrace("Total Registros Leidos::"+i+"::", NivelLog.INFO);

			fileSua.close();

		}catch(Exception ioeSua ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), NivelLog.INFO);
		}finally{
			if(null != fileSua){
				try{
					fileSua.close();
				}catch(IOException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				}
			}
			fileSua = null;
		}
		return consultaCuentaSantanderBean;

	}
	/**
	 * metodo consultaCuentaInterbancariaTotales que obtiene el total de cuentas interbancarias
	 * @param consultaCuentaInterbancariaBean bean que contiene los parametros para realizar la consulta
	 * @return Resultado total de registros de la consulta
	 */
	public int consultaCuentaInterbancariaTotales(CuentaInterbancariaBean consultaCuentaInterbancariaBean){
		int Resultado = 0;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;
		String banco=PORCENTAJE;

		contrato = armaCriterio(consultaCuentaInterbancariaBean);
		queryMaster.append(String.format(SELECT_CTAS_INTERBANCARIAS_TOTAL, contrato));
		if (NUMCUENTA.equals(consultaCuentaInterbancariaBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_CUENTA, armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaInterbancariaBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_NOMBRE, armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(nombre);
		}else if ("Banco".equals(consultaCuentaInterbancariaBean.getCriterio())) {
			banco = String.format(SELECT_CTAS_INTERBANCARIAS_FILTRO_BANCO,armaFiltro(consultaCuentaInterbancariaBean));
			queryMaster.append(banco);
		}

		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){
				EIGlobal.mensajePorTrace("QUERY COUNT INTERBANCARIAS A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				 while (result.next ()) {
					 Resultado = result.getInt(TOTAL);
					 EIGlobal.mensajePorTrace("Si existe informacion -----------> " , EIGlobal.NivelLog.DEBUG);
				 }
			}
			cierraConexion(conn);

		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas INTERBANCARIAS terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return Resultado;
	}
	/**
	 * metodo consultaCuentaInternacionalTotal que obtiene el total de cuentas internacionales
	 * @param consultaCuentaInternacionalBean  bean que contiene los parametros para realizar la consulta
	 * @return Resultado total de registros de la consulta
	 */
	public int consultaCuentaInternacionalTotal(CuentaInternacionalBean consultaCuentaInternacionalBean){
		int Resultado = 0;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;
		contrato = armaCriterioInternacional(consultaCuentaInternacionalBean);
		queryMaster.append(String.format(SELECT_CTAS_INTERNACIONALES_TOTAL, contrato));
		if (NUMCUENTA.equals(consultaCuentaInternacionalBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_INTERNACIONAL_FILTRO_CUENTA, armaFiltroInternacional(consultaCuentaInternacionalBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaInternacionalBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_INTERNACIONAL_FILTRO_NOMBRE, armaFiltroInternacional(consultaCuentaInternacionalBean));
			queryMaster.append(nombre);
		}
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE3);
			if(conn != null){
				EIGlobal.mensajePorTrace("QUERY INTERNACIONAL COUNT A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				EIGlobal.mensajePorTrace("TOTAL REGISTROS QUERY INTERNACIONAL::" + result.getFetchSize()+"::", EIGlobal.NivelLog.DEBUG);
				 while (result.next ()) {
					 Resultado = result.getInt(TOTAL);
					 EIGlobal.mensajePorTrace("Si existe informacion -----------> " , EIGlobal.NivelLog.DEBUG);
				 }
			}
			cierraConexion(conn);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas Internacional terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return Resultado;
	}
	/**
	 * metodo consultaCuentaSantanderTotal que obtiene el total de cuentas mismo banco (Santander)
	 * @param consultaCuentaSantanderBean bean que contiene los parametros para realizar la consulta
	 * @return Resultado total de registros de la consulta
	 */
	public int consultaCuentaSantanderTotal(CuentasSantanderBean consultaCuentaSantanderBean){

		int Resultado = 0;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		StringBuffer queryMaster=new StringBuffer();
		String contrato;
		String cuenta=PORCENTAJE;
		String nombre=PORCENTAJE;

		contrato = armaCriterioSantander(consultaCuentaSantanderBean);
		queryMaster.append(String.format(SELECT_CTAS_SANTANDER_TOTAL, contrato));
		if (NUMCUENTA.equals(consultaCuentaSantanderBean.getCriterio())) {
			cuenta = String.format(SELECT_CTAS_SANTANDER_FILTRO_CUENTA, armaFiltroSantander(consultaCuentaSantanderBean));
			queryMaster.append(cuenta);
		}else if (NOMBREX.equals(consultaCuentaSantanderBean.getCriterio())) {
			nombre = String.format(SELECT_CTAS_SANTANDER_FILTRO_NOMBRE, armaFiltroSantander(consultaCuentaSantanderBean));
			queryMaster.append(nombre);
		}
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){

				EIGlobal.mensajePorTrace("QUERY INTERNACIONAL COUNT A EJECUTAR ::" + queryMaster+"::", EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(queryMaster.toString());
				result = stmt.executeQuery();
				EIGlobal.mensajePorTrace("TOTAL REGISTROS QUERY INTERNACIONAL::" + result.getFetchSize()+"::", EIGlobal.NivelLog.DEBUG);
				while (result.next ()) {
					Resultado = result.getInt(TOTAL);
					EIGlobal.mensajePorTrace("Si existe informacion -----------> " , EIGlobal.NivelLog.DEBUG);
				}
			}
			cierraConexion(conn);
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas Internacional terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return Resultado;
	}

	/**
	 * Realiza la consulta ctas moviles
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return listaResultado : listaResultado
	 */
	public List<CuentaMovilBean> consultaCtasMoviles(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		List<CuentaMovilBean> listaResultado = null;
		ResultSet result =null;
		PreparedStatement stmt = null;
		Connection conn = null;
		String query;
		String where;
		String whereCta=PORCENTAJE;
		String whereTit=PORCENTAJE;
		where = armaCriterioMovil(consultaCtasMovilesBean);
		if (NUMCUENTA.equals(consultaCtasMovilesBean.getCriterio())) {
			whereCta = armaFiltroMovil(consultaCtasMovilesBean);
		}else if (NOMBREX.equals(consultaCtasMovilesBean.getCriterio())) {
			whereTit = armaFiltroMovil(consultaCtasMovilesBean);
		}
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){
				listaResultado  = new ArrayList<CuentaMovilBean>();
				query = String.format(SELECT_CTAS_MOVILES_PAGINADO,
											where,
											whereCta,
											whereTit,
											where,
											whereCta,
											whereTit,
											consultaCtasMovilesBean.getRegFin(),
											consultaCtasMovilesBean.getRegIni());
				query=query.concat("  ORDER BY CUENTA ");
				EIGlobal.mensajePorTrace("QUERY MOVIL PAGINADO A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				CuentaMovilBean resultado = null;
				 while (result.next ()) {
					 resultado = new CuentaMovilBean();
					 resultado.setNumeroMovil(validaYFormateaCampo(result.getString(NUMERO_CUENTA)));
					 resultado.setTitular(validaYFormateaCampo(result.getString(TITULAR)));
					 defineBancoYTipo(resultado, validaYFormateaCampo(result.getString(BANCO)).toUpperCase());
					 if (!("BANME".equals(resultado.getCveBanco()))) {
						 resultado.setCveBanco(validaYFormateaCampo(result.getString(CVE_BANCO)));
					 }
					 listaResultado.add(resultado);
				 }
			}
		} catch(SQLException e){
			EIGlobal.mensajePorTrace(ERROR+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace(ERRORFINAL+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas moviles terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return listaResultado;
	}
	/**
	 * Regresa el numero de registros totales de la consulta de cuentas moviles
	 * @param consultaCtasMovilesBean bean de cuentas moviles
	 * @return int total de cuentas moviles
	 */
	public int consultaCuentaMovilTotal(ConsultaCtasMovilesBean consultaCtasMovilesBean) {
		PreparedStatement stmt = null;
		ResultSet result= null;
		Connection conn = null;
		String where;
		String whereCta=PORCENTAJE;
		String whereTit=PORCENTAJE;

		where = armaCriterioMovil(consultaCtasMovilesBean);
		if (NUMCUENTA.equals(consultaCtasMovilesBean.getCriterio())) {
			whereCta = armaFiltroMovil(consultaCtasMovilesBean);
		}else if (NOMBREX.equals(consultaCtasMovilesBean.getCriterio())) {
			whereTit = armaFiltroMovil(consultaCtasMovilesBean);
		}
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			final String query = String.format(SELECT_CTAS_MOVILES_TOTAL,  where, whereCta, whereTit, where, whereCta, whereTit);
			EIGlobal.mensajePorTrace("QUERY MOVIL COUNT A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
			if(conn != null){
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				EIGlobal.mensajePorTrace("Query ejecutado de forma correcta " , EIGlobal.NivelLog.DEBUG);

				while (result.next()) {
					EIGlobal.mensajePorTrace("Registros totales en la consulta de cuentas moviles  -----------> " + result.getInt(TOTAL) , EIGlobal.NivelLog.DEBUG);
					return result.getInt(TOTAL);
					}
				}
			} catch(SQLException e){
				EIGlobal.mensajePorTrace("Error en count de cuentas moviles "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			} finally {
				try {
					if (result != null) {
						result.close();
					}
					if (stmt != null) {
						stmt.close();
					}
					if (conn != null) {
						conn.close();
					}
				}catch (SQLException e) {
					EIGlobal.mensajePorTrace("Error al finalizar conexiones en count "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}
		return 0;
	}
	/**
	 * Se encarga de armar los criterios de busqueda
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return where : where
	 */
	private String armaCriterioMovil(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		final StringBuilder where = new StringBuilder();
		where.append("'").append(consultaCtasMovilesBean.getContrato()).append("'");
		return where.toString();
	}
	/**
	 * Se encarga de armar los criterios del filtro
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return where : where
	 */
	private String armaFiltroMovil(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		final StringBuilder where = new StringBuilder();
		where.append("'%").append(consultaCtasMovilesBean.getTextoBuscar()).append("%'");
		return where.toString();
	}
	/**
	 * Define el tipo y descBanco de la cuenta apartir del banco
	 * @param resultado : cuenta
	 * @param banco : banco
	 */
	private void defineBancoYTipo(CuentaMovilBean resultado, String banco) {
		String tipo = "";
		String descBanco = "";
		 if("TCT".equals(banco)){
			 descBanco = "SANTANDER";
			 tipo = "Mismo banco";
			 resultado.setCveBanco("BANME");
		 }else{
			 descBanco = banco;
			 tipo = "Otros bancos";
		 }
		 resultado.setDescBanco(descBanco);
		 resultado.setTipoCuenta(tipo);
         EIGlobal.mensajePorTrace(BANCO + ": " + banco, EIGlobal.NivelLog.DEBUG);
         EIGlobal.mensajePorTrace("TIPO DE CUENTA: " + tipo, EIGlobal.NivelLog.DEBUG);
	}
}