package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class BloqueoUsuarioDAO {
	
	/**
	 * Valida si el usuario esta bloqueado
	 * @param contrato : contrato
	 * @param usuario : usuario
	 * @return bloqueado : bloqueado
	 */
	public boolean isBlockedUser(String contrato, String usuario){
		
		EIGlobal.mensajePorTrace("Entra a isBlockedUser: ",EIGlobal.NivelLog.DEBUG);
		
		ResultSet result =null;
		PreparedStatement stmt = null;	
		PreparedStatement stmt2 = null;
		PreparedStatement stmt3 = null;
		Connection conexion = null;
		boolean bloqueado = true;
		boolean existe = false;
		StringBuilder query_bloqueo_s = null;
		StringBuilder query_bloqueo_u = null;
		StringBuilder query_bloqueo_i = null;
		
		try{
				 
			conexion=createiASConnStatic (Global.DATASOURCE_ORACLE3 );
			
			if(conexion != null){
				
				query_bloqueo_s = new StringBuilder();
				query_bloqueo_s.append("SELECT BLOQUEADO FROM EWEB_USRCTA_BLOQ WHERE NUM_CUENTA2 = '").append(contrato)
					.append("' AND NUM_USUARIO = '").append(usuario).append("'");
				
				query_bloqueo_u = new StringBuilder();
				query_bloqueo_u.append("UPDATE EWEB_USRCTA_BLOQ SET FCH_ULT_ACCESO = SYSDATE WHERE NUM_CUENTA2 = '").append(contrato)
					.append("' AND NUM_USUARIO = '").append(usuario).append("'");
				
				query_bloqueo_i = new StringBuilder();
				query_bloqueo_i.append("INSERT INTO EWEB_USRCTA_BLOQ VALUES('").append(contrato.trim()).append("','")
					.append(usuario.trim()).append("', SYSDATE, SYSDATE, 'N')");
				
				EIGlobal.mensajePorTrace("QUERY A EJECUTAR query_bloqueo_s: " + query_bloqueo_s.toString(),EIGlobal.NivelLog.DEBUG);
								
				stmt = conexion.prepareStatement(query_bloqueo_s.toString());
				result = stmt.executeQuery();	
			   			
				while(result.next ()) {   
					existe = true;
					if(result.getString("BLOQUEADO")!= null && "N".equals(result.getString("BLOQUEADO").trim())){
						EIGlobal.mensajePorTrace("QUERY A EJECUTAR query_bloqueo_u: " + query_bloqueo_u.toString(),EIGlobal.NivelLog.DEBUG);
						stmt2 = conexion.prepareStatement(query_bloqueo_u.toString());
						stmt2.executeUpdate();
						bloqueado = false;
						
					}
 				 
				 }
				
				if(!existe){
					EIGlobal.mensajePorTrace("USUARIO INEXISTENTE, PROCEDO CON SU CREACION ",EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("QUERY A EJECUTAR query_bloqueo_i: " + query_bloqueo_i.toString(),EIGlobal.NivelLog.DEBUG);
					stmt3 = conexion.prepareStatement(query_bloqueo_i.toString());
					stmt3.executeUpdate();
					bloqueado = false;
				}
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace(" OCURRIO UN ERROR " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}finally {
			try {
				
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (stmt2 != null) {
					stmt2.close();
				}
				
			    if(conexion!=null){
			        conexion.close();
			        conexion=null;
				}
				
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("sql.conection.close " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}catch (Exception e) {
				EIGlobal.mensajePorTrace("sql.conection.close " + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		return bloqueado;
	}
	
	
	/**
	 * Realiza la conexion a BD
	 * @param dbName : dbName
	 * @return Connection : Connection
	 * @throws SQLException manejo de excepcion
	 */
	public static Connection createiASConnStatic ( String dbName )
    throws SQLException {

    	 Connection conn = null;
    	 ServiceLocator servLocator = ServiceLocator.getInstance();
    	 DataSource ds = null;
    	 try {
    		 ds = servLocator.getDataSource(dbName);
    	 } catch(NamingException e) {
    		 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
    	 }
    	 if(ds == null) {
    		 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
    		 return null;
    	 }
    	 conn = ds.getConnection ();
    	 return conn;
    }
	
}
