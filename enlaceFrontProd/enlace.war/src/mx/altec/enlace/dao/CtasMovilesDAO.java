package mx.altec.enlace.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List; 

import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

public class CtasMovilesDAO extends GenericDAO{
	
	/**
	 * Variable titular: campo de tabla
	 */
	private static final String TITULAR = "TITULAR";
	/**
	 * Variable NUMERO_MOVIL: campo de tabla
	 */
	private static final String NUMERO_MOVIL = "CUENTA";
	/**
	 * Variable BANCO: campo de tabla
	 */
	private static final String BANCO = "BANCO";
	/**
	 * Variable TOTAL: campo de tabla
	 */
	private static final String TOTAL = "TOTAL";
	
	/** The Constant CVE_BANCO. */
	private static final String CVE_BANCO = "CVE_BANCO";
 	
	/**
	 * SELECT_CTAS_PAGINADO
	 */
	private static final String SELECT_CTAS_PAGINADO = new StringBuilder(
			"SELECT tabla_general.* FROM ( %n").append(
			"SELECT ROWNUM AS INDICE, tabla.* FROM( %n").append(
			"	SELECT a.num_cuenta AS CUENTA, a.nombre_titular AS TITULAR,  %n").append(
			"		cve_producto AS BANCO, cve_producto2 AS CVE_BANCO  %n").append(		
			"	FROM nucl_relac_ctas a %n").append(
		    "	WHERE LENGTH(trim(a.num_cuenta)) = 10 AND num_cuenta2 = %s %n").append(
		    "   UNION %n").append(
		    "   	SELECT b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme %n").append( 
		    "			FROM nucl_cta_externa b, comu_interme_fin c %n").append(
		    "				WHERE b.cve_interme = c.cve_interme %n").append(
		    "				AND LENGTH(trim(b.num_cuenta_ext)) = 10 AND num_cuenta =  %s %n").append(
		    "				)tabla WHERE ROWNUM <=  %s %n").append(
		    "				)tabla_general WHERE INDICE >  %s %n").toString();
	
	/**
	 * SELECT_CTAS_TOTALES
	 */
	private static final String SELECT_CTAS_TOTALES = new StringBuilder(
	"SELECT count(*) as TOTAL FROM( %n").append(
	"	SELECT a.num_cuenta as CUENTA, a.nombre_titular as TITULAR, cve_producto AS BANCO, cve_producto2 as CVE_BANCO %n").append(
	"	from nucl_relac_ctas a %n").append(
    "	where length(trim(a.num_cuenta)) = 10 and num_cuenta2 = %s %n").append(
    "   union %n").append(
    "   	select b.num_cuenta_ext, b.nombre, c.nombre_corto, b.cve_interme %n").append( 
    "			from nucl_cta_externa b, comu_interme_fin c %n").append(
    "				where b.cve_interme = c.cve_interme %n").append(
    "				and length(trim(b.num_cuenta_ext)) = 10 and num_cuenta =  %s %n").append(
    ")").toString();
	
	
	/**
	 * Realiza la consulta ctas moviles
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return listaResultado : listaResultado
	 */
	public List<CuentaMovilBean> consultaCtasMoviles(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		
		List<CuentaMovilBean> listaResultado = null;
		ResultSet result =null;
		PreparedStatement stmt = null;		
		Connection conn = null;
		String query;
		String where;

		where = armaCriterio(consultaCtasMovilesBean);
		int totales = obtenerNumeroDeRegistros(where);
		
		try{
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			if(conn != null){
				listaResultado  = new ArrayList<CuentaMovilBean>();
				query = String.format(SELECT_CTAS_PAGINADO, 
											where, 
											where,
											consultaCtasMovilesBean.getRegFin(), 
											consultaCtasMovilesBean.getRegIni());
				
				EIGlobal.mensajePorTrace("QUERY PAGINADO A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
				
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				
				CuentaMovilBean resultado = null;
				 while (result.next ()) {
					 resultado = new CuentaMovilBean();
					 resultado.setTotalRegistros(totales);
					 	EIGlobal.mensajePorTrace("Si existe informacion -----------> " , EIGlobal.NivelLog.DEBUG);
					 resultado.setTitular(validaYFormateaCampo(result.getString(TITULAR)));
                     	EIGlobal.mensajePorTrace(TITULAR + ": " + resultado.getTitular(), EIGlobal.NivelLog.DEBUG);
					 resultado.setNumeroMovil(validaYFormateaCampo(result.getString(NUMERO_MOVIL)));
                     	EIGlobal.mensajePorTrace(NUMERO_MOVIL + ": " + resultado.getNumeroMovil(), EIGlobal.NivelLog.DEBUG);
					 defineBancoYTipo(resultado, validaYFormateaCampo(result.getString(BANCO)).toUpperCase());
					 if (!("BANME".equals(resultado.getCveBanco()))) {
						 resultado.setCveBanco(validaYFormateaCampo(result.getString(CVE_BANCO)));
					 }
					 listaResultado.add(resultado);
				 }
			}
	
		} catch(SQLException e){
			EIGlobal.mensajePorTrace("Error al obtener los datos "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			}catch (SQLException e) {
				EIGlobal.mensajePorTrace("Error al finalizar "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
		}
		EIGlobal.mensajePorTrace("Consulta de cuentas moviles terminada exitosamente -----------> " , EIGlobal.NivelLog.DEBUG);
		return listaResultado;
	}
	
	/**
	 * Valida y formatea campo.
	 *
	 * @param campo the campo
	 * @return the string
	 */
	private String validaYFormateaCampo(String campo){
		String valor = "";
		if (campo!=null) {
			valor = campo.trim();
		}
		return valor;
	}
	
	/**
	 * Define el tipo y descBanco de la cuenta apartir del banco
	 * @param resultado : cuenta
	 * @param banco : banco
	 */
	private void defineBancoYTipo(CuentaMovilBean resultado, String banco) {
		String tipo = "";
		String descBanco = "";
		 if("TCT".equals(banco)){
			 descBanco = "SANTANDER";
			 tipo = "Mismo banco";
			 resultado.setCveBanco("BANME");
		 }else{
			 descBanco = banco;
			 tipo = "Otros bancos";
		 }
		 resultado.setDescBanco(descBanco);
		 resultado.setTipoCuenta(tipo);
         EIGlobal.mensajePorTrace(BANCO + ": " + banco, EIGlobal.NivelLog.DEBUG);
         EIGlobal.mensajePorTrace("TIPO DE CUENTA: " + tipo, EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Regresa el numero de registros totales de la consulta
	 * @param where : contrato
	 * @return registrosTotales : registrosTotales
	 */
	private int obtenerNumeroDeRegistros(String where) {
		PreparedStatement stmt = null;
		ResultSet result= null;
		Connection conn = null;
		try {
			conn = createiASConnStatic(Global.DATASOURCE_ORACLE);
			final String query = String.format(SELECT_CTAS_TOTALES,  where, where);
			EIGlobal.mensajePorTrace("QUERY COUNT A EJECUTAR " + query, EIGlobal.NivelLog.DEBUG);
			if(conn != null){
				stmt = conn.prepareStatement(query);
				result = stmt.executeQuery();
				EIGlobal.mensajePorTrace("Query ejecutado de forma correcta " , EIGlobal.NivelLog.DEBUG);

				while (result.next()) {
					EIGlobal.mensajePorTrace("Registros totales en la consulta de cuentas moviles  -----------> " + result.getInt(TOTAL) , EIGlobal.NivelLog.DEBUG);
					return result.getInt(TOTAL);
					}
				}
			} catch(SQLException e){
				EIGlobal.mensajePorTrace("Error en count de cuentas moviles "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			} finally {
				try {
					if (result != null) {
						result.close();
					}
					if (stmt != null) {
						stmt.close();
					}
					if (conn != null) {
						conn.close();
					}
				}catch (SQLException e) {
					EIGlobal.mensajePorTrace("Error al finalizar conexiones en count "+new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
				}
			}
		return 0;
	}


	/**
	 * Se encarga de armar los criterios de busqueda
	 * @param consultaCtasMovilesBean : consultaCtasMovilesBean
	 * @return where : where
	 */
	private String armaCriterio(ConsultaCtasMovilesBean consultaCtasMovilesBean){
		
		final StringBuilder where = new StringBuilder();
		where.append("'").append(consultaCtasMovilesBean.getContrato()).append("'");
		
		return where.toString();	
	}
}