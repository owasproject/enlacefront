package mx.altec.enlace.beans;

// TODO: Auto-generated Javadoc
/**
 * Clase encargada de almacenar la informacion para la tabla TCT_PROGRAMACION.
 *
 * @author FSW-Indra
 * @sice 11/02/2015
 */
public class OperacionesProgramadasBean extends OperacionesProgramadasCtasBean{
	
	/**
	 * Variable para almacenar la referencia de Operacion Programada.
	 */
	private String referencia;
	/**
	 * Variable para almacenar la fechaAplicacion de Operacion Programada.
	 */
	private String fechaAplicacion;
	/**
	 * Variable para almacenar la referenciaBit de Operacion Programada.
	 */
	private String referenciaBit;
	/**
	 * Variable para almacenar el tipoOperacion de Operacion Programada.
	 */
	private String tipoOperacion;
	/**
	 * Variable para almacenar el numCuenta (contrato) de Operacion Programada.
	 */
	private String numCuenta;
	/**
	 * Variable para almacenar el usuario de Operacion Programada.
	 */
	private String usuario;
	/**
	 * Variable para almacenar la importe de Operacion Programada.
	 */
	private String importe;
	/**
	 * Variable para almacenar los titulos de Operacion Programada.
	 */
	private String titulos;
	/**
	 * Variable para almacenar la cvePerfil de Operacion Programada.
	 */
	private String cvePerfil;
	/**
	 * Variable para almacenar el concepto de Operacion Programada.
	 */
	private String concepto;
	/**
	 * Variable para almacenar la fchRegistro de Operacion Programada.
	 */
	private String fchRegistro;
	/**
	 * Variable para almacenar la forma de Aplicacion de Operacion Programada.
	 */
	private String formaAplicacion;
	/**
	 * Variable para almacenar la Referencia Interbancaria.
	 */
	private String refInterb;	
	/**
	 * Variable para almacenar la Referencia Interbancaria.
	 */
	private String rfc;
	
	/**
	 * Variable para almacenar la Referencia Interbancaria.
	 */
	private String iva;
	
	/**
	 * Variable para almacenar la divisa de la transferencia Interbancaria.
	 */
	private String divisa;
	
	/**
	 * Variable para almacenar la direccion IP de la transferencia Interbancaria.
	 */
	private String direccionIP;
	
	/**
	 * Variable para almacenar la clace de opracion de la transferencia Interbancaria.
	 */
	private String cveOperacionSPID;

	
	/**
	 * Gets the divisa.
	 *
	 * @return the divisa
	 */
	public String getDivisa() {
		return divisa;
	}

	/**
	 * Sets the divisa.
	 *
	 * @param divisa the new divisa
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	/**
	 * getReferencia de tipo String.
	 * @author FSW-Indra
	 * @return referencia de tipo String
	 */
	public String getRFC() {
		return rfc;
	}
		
	/**
	 * setReferencia para asignar valor a referencia.
	 *
	 * @author FSW-Indra
	 * @param rfc the new rfc
	 */
	public void setRFC(String rfc) {
		this.rfc = rfc;
	}
	
	/**
	 * getReferencia de tipo String.
	 * @author FSW-Indra
	 * @return referencia de tipo String
	 */
	public String getIva() {
		return iva;
	}
	
	/**
	 * setReferencia para asignar valor a referencia.
	 *
	 * @author FSW-Indra
	 * @param iva the new iva
	 */
	public void setIva(String iva) {
		this.iva = iva;
	}
	
	/**
	 * getReferencia de tipo String.
	 * @author FSW-Indra
	 * @return referencia de tipo String
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * setReferencia para asignar valor a referencia.
	 * @author FSW-Indra
	 * @param referencia de tipo String
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * getFechaAplicacion de tipo String.
	 * @author FSW-Indra
	 * @return fechaAplicacion de tipo String
	 */
	public String getFechaAplicacion() {
		return fechaAplicacion;
	}
	/**
	 * setFechaAplicaciona para asignar valor a fechaAplicacion.
	 * @author FSW-Indra
	 * @param fechaAplicacion de tipo String
	 */
	public void setFechaAplicacion(String fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
	/**
	 * getReferenciaBit de tipo String.
	 * @author FSW-Indra
	 * @return referenciaBit de tipo String
	 */
	public String getReferenciaBit() {
		return referenciaBit;
	}
	/**
	 * setReferenciaBit para asignar valor a referenciaBit.
	 * @author FSW-Indra
	 * @param referenciaBit de tipo String
	 */
	public void setReferenciaBit(String referenciaBit) {
		this.referenciaBit = referenciaBit;
	}
	/**
	 * getTipoOperacion de tipo String.
	 * @author FSW-Indra
	 * @return tipoOperacion de tipo String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * setTipoOperacion para asignar valor a tipoOperacion.
	 * @author FSW-Indra
	 * @param tipoOperacion de tipo String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * getNumCuenta de tipo String.
	 * @author FSW-Indra
	 * @return numCuenta de tipo String
	 */
	public String getNumCuenta() {
		return numCuenta;
	}
	/**
	 * setNumCuenta para asignar valor a numCuenta.
	 * @author FSW-Indra
	 * @param numCuenta de tipo String
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	/**
	 * getUsuario de tipo String.
	 * @author FSW-Indra
	 * @return usuario de tipo String
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * setUsuario para asignar valor a usuario.
	 * @author FSW-Indra
	 * @param usuario de tipo String
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * getImporte de tipo String.
	 * @author FSW-Indra
	 * @return importe de tipo String
	 */
	public String getImporte() {
		return importe;
	}
	/**
	 * setImporte para asignar valor a importe.
	 * @author FSW-Indra
	 * @param importe de tipo String
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}
	/**
	 * getTitulos de tipo String.
	 * @author FSW-Indra
	 * @return titulos de tipo String
	 */
	public String getTitulos() {
		return titulos;
	}
	/**
	 * setTitulos para asignar valor a titulos.
	 * @author FSW-Indra
	 * @param titulos de tipo String
	 */
	public void setTitulos(String titulos) {
		this.titulos = titulos;
	}
	/**
	 * getCvePerfil de tipo String.
	 * @author FSW-Indra
	 * @return cvePerfil de tipo String
	 */
	public String getCvePerfil() {
		return cvePerfil;
	}
	/**
	 * setCvePerfil para asignar valor a cvePerfil.
	 * @author FSW-Indra
	 * @param cvePerfil de tipo String
	 */
	public void setCvePerfil(String cvePerfil) {
		this.cvePerfil = cvePerfil;
	}
	/**
	 * getConcepto de tipo String.
	 * @author FSW-Indra
	 * @return concepto de tipo String
	 */
	public String getConcepto() {
		return concepto;
	}
	/**
	 * setConcepto para asignar valor a concepto.
	 * @author FSW-Indra
	 * @param concepto de tipo String
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	/**
	 * getFchRegistro de tipo String.
	 * @author FSW-Indra
	 * @return fchRegistro de tipo String
	 */
	public String getFchRegistro() {
		return fchRegistro;
	}
	/**
	 * setFchRegistro para asignar valor a fchRegistro.
	 * @author FSW-Indra
	 * @param fchRegistro de tipo String
	 */
	public void setFchRegistro(String fchRegistro) {
		this.fchRegistro = fchRegistro;
	}
	/**
	 * getFormaAplicacion de tipo String.
	 * @author FSW-Indra
	 * @return formaAplicacion de tipo String
	 */
	public String getFormaAplicacion() {
		return formaAplicacion;
	}
	/**
	 * setFormaAplicacion para asignar valor a formaAplicacion.
	 * @author FSW-Indra
	 * @param formaAplicacion de tipo String
	 */
	public void setFormaAplicacion(String formaAplicacion) {
		this.formaAplicacion = formaAplicacion;
	}
	/**
	 * getRefInterb de tipo String.
	 * @author FSW-Indra
	 * @return refInterb de tipo String
	 */
	public String getRefInterb() {
		return refInterb;
	}
	/**
	 * setRefInterb para asignar valor a refInterb.
	 * @author FSW-Indra
	 * @param refInterb de tipo String
	 */
	public void setRefInterb(String refInterb) {
		this.refInterb = refInterb;
	}

	/**
	 * Gets the direccion ip.
	 *
	 * @return the direccion ip
	 */
	public String getDireccionIP() {
		return direccionIP;
	}

	/**
	 * Sets the direccion ip.
	 *
	 * @param direccionIP the new direccion ip
	 */
	public void setDireccionIP(String direccionIP) {
		this.direccionIP = direccionIP;
	}

	/**
	 * Gets the cve operacion spid.
	 *
	 * @return the cve operacion spid
	 */
	public String getCveOperacionSPID() {
		return cveOperacionSPID;
	}

	/**
	 * Sets the cve operacion spid.
	 *
	 * @param cveOperacionSPID the new cve operacion spid
	 */
	public void setCveOperacionSPID(String cveOperacionSPID) {
		this.cveOperacionSPID = cveOperacionSPID;
	}
}