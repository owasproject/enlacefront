package mx.altec.enlace.beans;
/**
 * 
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */

import java.util.ArrayList;
import java.util.List;

public class GL52Tran extends GLBase {
	
	String contrato;
	String usuario;
	String cuenta;
	String facultad;	
	String indicadorPag;
	String cuentaPag;
	String facultadPag;
	String usuarioOper;
	
	ArrayList<GLFacultadCta> facultadesCta;

	
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getCuentaPag() {
		return cuentaPag;
	}
	public void setCuentaPag(String cuentaPag) {
		this.cuentaPag = cuentaPag;
	}
	public String getFacultad() {
		return facultad;
	}
	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}
	public String getFacultadPag() {
		return facultadPag;
	}
	public void setFacultadPag(String facultadPag) {
		this.facultadPag = facultadPag;
	}
	public String getIndicadorPag() {
		return indicadorPag;
	}
	public void setIndicadorPag(String indicadorPag) {
		this.indicadorPag = indicadorPag;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getUsuarioOper() {
		return usuarioOper;
	}
	public void setUsuarioOper(String usuarioOper) {
		this.usuarioOper = usuarioOper;
	}
	public ArrayList<GLFacultadCta> getFacultadesCta() {
		return facultadesCta;
	}
	public void setFacultadesCta(ArrayList<GLFacultadCta> facultadesCta) {
		this.facultadesCta = facultadesCta;
	}
	
	
}
