package mx.altec.enlace.beans;

public interface AdmUsrBuilder<T> {
	
	T build(String args);
}
