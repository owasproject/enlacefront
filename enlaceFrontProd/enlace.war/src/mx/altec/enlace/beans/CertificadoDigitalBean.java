/**
 *******************************************************************************
 * 
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CertificadoDigitalBean.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	  By 		Company 	     Description
 * ======= =========== ========= =============== ==============================
 * 1.0 	    21/10/2013 FSW-Indra  Indra Company      Creacion
 *
 *******************************************************************************
 **/
package mx.altec.enlace.beans;

public class CertificadoDigitalBean {
	
	/** Fecha de la vigencia del certificado digital **/
	private String vigenciaCertificado;
	
	/** Numero de serie del certificado digital **/
	private String numeroDeSerie;
	
	/** Certificado digital **/
	private String certificado;
	
	/** Indica si existe ya alg�n certificado para el usuario **/
	private boolean existe;
	
	/** Indica el estatus del certificado V = valido, C = caducado, R = revocado **/
	private String estatus;
	
	/**
	 * Getter de la fecha de la vigencia del certificado digital
	 * @return new String : Fecha de la vigencia del certificado digital
	 */
	public String getVigenciaCertificado() {
		return vigenciaCertificado;
	}
	
	/**
	 * Setter de la fecha de la vigencia del certificado digital
	 * @param vigenciaCertificado : String : vigencia del certificado digital
	 */
	public void setVigenciaCertificado(String vigenciaCertificado) {
		this.vigenciaCertificado = vigenciaCertificado;
	}
	
	/**
	 * Getter del numero de certificado
	 * @return new String : Numero del Certificado Digital
	 */
	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}
	
	/**
	 * Setter del numero de Serie
	 * @param numeroDeSerie : String : Numero de Serie
	 */
	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}
	
	/**
	 * Getter del Certificado
	 * @return new String : Certificado Digital
	 */
	public String getCertificado() {
		return certificado;
	}
	
	/**
	 * Setter del Certificado Digital
	 * @param certificado : String : Certificado Digital
	 */
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	/**
	 * Getter de existe
	 * @return new boolean : existe el certificado digital
	 */
	public boolean isExiste() {
		return existe;
	}

	/**
	 * Setter de existe
	 * @param existe : boolean : existe el certificado digital
	 */
	public void setExiste(boolean existe) {
		this.existe = existe;
	}
	
	/**
	 * Getter de estatus
	 * @return new String : estatus del certificado digital
	 */
	public String getEstatus() {
		return estatus;
	}
	
	/**
	 * Setter de estatus
	 * @param estatus : String : estatus del certificado digital
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}	
	
	

}
