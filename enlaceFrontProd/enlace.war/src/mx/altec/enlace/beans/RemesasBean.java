package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author ESC
 * Bean de remesas LMM0XC1  CONSULTA REMESAS
 */
public class RemesasBean implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * N&uacute;mero de remesa
	 */
	private String numRemesa;
	/**
	 * Estado de remesa
	 */
	private String estadoRemesa;
	/**
	 * Condici&oacute;n estampa de remesa
	 */
	private String condEstampa;
	/**
	 * Descripci&oacute;n condici&oacute;n estampa de remesa
	 */
	private String descCondEst;
	/**
	 * N&uacute;mero de tarjetas
	 */
	private String numTarjetas;
	/**
	 * Fecha alta remesa
	 */
	private String fechaAltaRem;
	/**
	 * Fecha recepci&oacute;n remesa
	 */
	private String fechaRecepRem;

	private String centroDistribucion;
	/**
	 * Fecha baja remesa
	 */
	private String fechaBajaRem;

	public String getNumRemesa() {
		return numRemesa;
	}
	public void setNumRemesa(String numRemesa) {
		this.numRemesa = numRemesa;
	}
	public String getEstadoRemesa() {
		return estadoRemesa;
	}

	public String getCentroDistribucion() {
		return centroDistribucion;
	}
	public void setCentroDistribucion(String centroDistribucion) {
		this.centroDistribucion = centroDistribucion;
	}
	public void setEstadoRemesa(String estadoRemesa) {
		if(estadoRemesa.equals("E")) {
			this.estadoRemesa = "ENVIADA";
		} else if(estadoRemesa.equals("R")) {
			this.estadoRemesa = "ASIGNADA";
		} else if(estadoRemesa.equals("D")) {
			this.estadoRemesa = "CANCELADA";
		} else if(estadoRemesa.equals("N")) {
			this.estadoRemesa = "NUEVA";
		} else {
			this.estadoRemesa = "BAJA";
		}
	}
	public String getCondEstampa() {
		return condEstampa;
	}
	public void setCondEstampa(String condEstampa) {
		this.condEstampa = condEstampa;
	}
	public String getDescCondEst() {
		return descCondEst;
	}
	public void setDescCondEst(String descCondEst) {
		this.descCondEst = descCondEst;
	}
	public String getNumTarjetas() {
		return numTarjetas;
	}
	public void setNumTarjetas(String numTarjetas) {
		this.numTarjetas = numTarjetas;
	}
	public String getFechaAltaRem() {
		return fechaAltaRem;
	}
	public void setFechaAltaRem(String fechaAltaRem) {
		this.fechaAltaRem = fechaAltaRem;
	}
	public String getFechaRecepRem() {
		return fechaRecepRem;
	}
	public void setFechaRecepRem(String fechaRecepRem) {
		this.fechaRecepRem = fechaRecepRem;
	}
	public String getFechaBajaRem() {
		return fechaBajaRem;
	}
	public void setFechaBajaRem(String fechaBajaRem) {
		this.fechaBajaRem = fechaBajaRem;
	}
}