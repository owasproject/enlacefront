package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author ESC
 * Bean de Tarjetas Contrato BD EWEB_VINC_TARJETACON 
 */
public class FiltroConsultaBean implements Serializable {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * N&uacute;mero de contrato
	 */
	private String contrato;	
	/**
	 * N&uacute;mero de tarjeta
	 */
	private String tarjeta;	
	/**
	 * Estatus de la tarjeta
	 */
	private String estatus;	
	/**
	 * Lote de carga
	 */
	private String idLote;	
	/**
	 * Fecha inicio consulta
	 */
	private String fechaInicio;
	/**
	 * Fecha fin consulta
	 */
	private String fechaFin;
	/**
	 * Usuario de registro
	 */
	private String usuarioReg;
	
	/**
	 * Obtiene contrato
	 * @return contrato
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * Setea contrato 
	 * @param contrato contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * Obtiene tarjeta
	 * @return tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}
	/**
	 * Setea tarjeta 
	 * @param tarjeta tarjeta
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	/**
	 * Obtiene estatus
	 * @return estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Setea estatus 
	 * @param estatus estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * Obtiene idLote
	 * @return idLote
	 */
	public String getIdLote() {
		return idLote;
	}
	/**
	 * Setea idLote 
	 * @param idLote idLote
	 */
	public void setIdLote(String idLote) {
		this.idLote = idLote;
	}
	/**
	 * Obtiene usuarioReg
	 * @return usuarioReg
	 */
	public String getUsuarioReg() {
		return usuarioReg;
	}
	/**
	 * Setea usuarioReg 
	 * @param usuarioReg usuarioReg
	 */
	public void setUsuarioReg(String usuarioReg) {
		this.usuarioReg = usuarioReg;
	}
	/**
	 * Obtiene fechaInicio
	 * @return fechaInicio
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * Setea fechaInicio 
	 * @param fechaInicio fechaInicio
	 */
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * Obtiene fechaFin
	 * @return fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}
	/**
	 * Setea fechaFin 
	 * @param fechaFin fechaFin
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}
