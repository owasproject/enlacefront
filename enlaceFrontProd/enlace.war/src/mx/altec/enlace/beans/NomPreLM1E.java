package mx.altec.enlace.beans;

import static mx.altec.enlace.beans.NomPreEmpleado.*;
import static mx.altec.enlace.beans.NomPreTarjeta.*;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

public class NomPreLM1E extends NomPreLM1Base implements Serializable {

	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "LM1E04601123451O00N2"; //VSWF ESC nuevos campos NP
	
	private static final String FORMATO_HEADER = "@DCLMMTAR0 P";

	private static final NomPreLM1EFactory FACTORY = new NomPreLM1EFactory();

	private String codigoAccion;
	private String contrato;
	private String noTarjeta;
	private NomPreEmpleado empleado;

	public String getCodigoAccion() {
		return codigoAccion;
	}

	public void setCodigoAccion(String codigoAccion) {
		this.codigoAccion = codigoAccion;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getNoTarjeta() {
		return noTarjeta;
	}

	public void setNoTarjeta(String noTarjeta) {
		this.noTarjeta = noTarjeta;
	}

	public NomPreEmpleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(NomPreEmpleado empleado) {
		this.empleado = empleado;
	}

	public static NomPreLM1EFactory getFactoryInstance() {
		return FACTORY;
	}

	private static class NomPreLM1EFactory implements NomPreBuilder<NomPreLM1E> {

		public NomPreLM1E build(String arg) {

			NomPreLM1E bean = new NomPreLM1E();

			if (isCodigoExito(arg)) {
				bean.setCodigoOperacion("LM1E0000");
				bean.setCodExito(true);

				int index = arg.indexOf(FORMATO_HEADER);

				if (index != -1) {

					index += FORMATO_HEADER.length();
					bean.setCodigoAccion(getValor(arg, index , 1));
					bean.setContrato(getValor(arg, index += 1, 11));
					bean.setNoTarjeta(getValor(arg, index += 11, LNG_NO_TARJETA));

					bean.setEmpleado(
							new NomPreEmpleado(
								getValor(arg, index + 115, LNG_NO_EMPLEADO),			//NO-EMPLEADO
								getValor(arg, index += 22, LNG_NOMBRE), 			//NOM-EMPLEADO
								getValor(arg, index += 30, LNG_PATERNO), 			//APE-PATERNO
								getValor(arg, index += 30, LNG_MATERNO), 			//APE-MATERNO
								getValor(arg, index += 20, LNG_RFC), 				//COD-RFC
								getValor(arg, index += 10, LNG_HOMOCLAVE), 			//COD-HOMOCLAVE
								getValor(arg, index += 20, LNG_NUM_ID), 				//NUM-ID VSWF ESC nuevos campos NP
								getValor(arg, index += 10, LNG_FECHA_NACIMIENTO),	//FEC-NACIMIENTO //*
								getValor(arg, index += 10, LNG_SEXO), 				//IND-SEXO
								getValor(arg, index += 1, LNG_NACIONALIDAD), 		//COD-NACION
								getValor(arg, index += 1, LNG_PAIS), 				//COD-PAIS VSWF ESC nuevos campos NP
								getValor(arg, index += 4, LNG_ESTADO_CIVIL), 		//COD-EST-CIVIL
								getValor(arg, index += 1, LNG_CORREO_ELECTRONICO), 	//DES-MAIL
								getValor(arg, index += 30, LNG_CALLE), 				//DES-DOMICILIO
								getValor(arg, index += 10, LNG_NUM_EXT), 			//NUM-DOMICILIO	VSWF ESC nuevos campos NP
								getValor(arg, index += 10, LNG_NUM_INT), 			//NUM-INT VSWF ESC nuevos campos NP
								getValor(arg, index += 10, LNG_COLONIA), 			//DES-COLONIA						
								getValor(arg, index += 30, LNG_DELEGACION), 		//DES-DELEGACION
								getValor(arg, index += 20, LNG_CIUDAD), 			//DES-CIUDAD
								getValor(arg, index += 30, LNG_ESTADO), 			//COD-ESTADO
								getValor(arg, index += 30, LNG_CODIGO_POSTAL), 		//COD-POSTAL
								getValor(arg, index += 5, LNG_LADA), 				//COD-LADA
								getValor(arg, index += 5, LNG_TELEFONO) 			//COD-TELEFONO
								)
							);
				}

			} else if (isCodigoError(arg))  {

				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}

			return bean;
		}

	}
}