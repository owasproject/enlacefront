package mx.altec.enlace.beans;

public class PagoReferenciadoSatBean{

	/**
	 * Fecha de la transferencia
	 */
	private String fecha;
	/**
	 * Numero de referencia
	 */
	private String referencia;
	/**
	 * Numero de la cuenta Cargo o Cuenta Origen
	 */
	private String cuentaCargo;
	/**
	 * Numero de contrato
	 */
	private String contrato;
	/**
	 * Numero de la operacion
	 */
	private String numOpe;
	/**
	 * Linea de Captura
	 */
	private String lineaDeCaptura;
	/**
	 * Llave de Pago
	 */
	private String llaveDePago;
	/**
	 * Razon Social de la Cuenta Cargo
	 */
	private String razonSocCtaCargo;
	/**
	 * Codigo de error de respuesta de la transaccion LZCP
	 */
	private String codError;
	/**
	 * Descripcion del Codigo de error de respuesta de la transaccion LZCP
	 */
	private String descError;

	/**
	 * HORA de la Operación, que viene de la respuesta de la trx LZCP
	 */
	private String horaLZCP;

	/**
	 * Obtiene la Fecha de la transferencia
	 * 
	 * @return String Fecha de la transferencia
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Asigna la Fecha de la transferencia
	 * 
	 * @param fecha
	 *            String Fecha de la transferencia
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * Obtiene Numero de referencia
	 * 
	 * @return String Numero de referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * Asigna Numero de referencia
	 * 
	 * @param referencia
	 *            String Numero de referencia
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	/**
	 * Obtiene Numero de la cuenta Cargo o Cuenta Origen
	 * 
	 * @return String Numero de la cuenta Cargo o Cuenta Origen
	 */
	public String getCuentaCargo() {
		return cuentaCargo;
	}

	/**
	 * Asigna Numero de la cuenta Cargo o Cuenta Origen
	 * 
	 * @param cuentaCargo
	 *            String Numero de la cuenta Cargo o Cuenta Origen
	 */
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}

	/**
	 * Obtiene Numero de contrato
	 * 
	 * @return String Numero de contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * Asigna Numero de contrato
	 * 
	 * @param contrato
	 *            String Numero de contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * Obtien Numero de la operacion
	 * 
	 * @return String Numero de la operacion
	 */
	public String getNumOpe() {
		return numOpe;
	}

	/**
	 * Asigna Numero de la operacion
	 * 
	 * @param numOpe
	 *            String Numero de la operacion
	 */
	public void setNumOpe(String numOpe) {
		this.numOpe = numOpe;
	}

	/**
	 * Obtiene Linea de Captura
	 * 
	 * @return String Linea de Captura
	 */
	public String getLineaDeCaptura() {
		return lineaDeCaptura;
	}

	/**
	 * Asigna Linea de Captura
	 * 
	 * @param lineaDeCaptura
	 *            String Linea de Captura
	 */
	public void setLineaDeCaptura(String lineaDeCaptura) {
		this.lineaDeCaptura = lineaDeCaptura;
	}

	/**
	 * Obtiene Llave de Pago
	 * 
	 * @return String Llave de Pago
	 */
	public String getLlaveDePago() {
		return llaveDePago;
	}

	/**
	 * Asigna Llave de Pago
	 * 
	 * @param llaveDePago
	 *            String Llave de Pago
	 */
	public void setLlaveDePago(String llaveDePago) {
		this.llaveDePago = llaveDePago;
	}

	/**
	 * Obtiene la Razon Social de la Cuenta Cargo
	 * 
	 * @return String Razon Social de la Cuenta Cargo
	 */
	public String getRazonSocCtaCargo() {
		return razonSocCtaCargo;
	}

	/**
	 * Asigna la Razon Social de la Cuenta Cargo
	 * 
	 * @param razonSocCtaCargo
	 *            String Razon Social de la Cuenta Cargo
	 */
	public void setRazonSocCtaCargo(String razonSocCtaCargo) {
		this.razonSocCtaCargo = razonSocCtaCargo;
	}

	/**
	 * Obtiene Codigo de error de respuesta de la transaccion LZCP
	 * 
	 * @return String Codigo de error de respuesta de la transaccion LZCP
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Asigna Codigo de error de respuesta de la transaccion LZCP
	 * 
	 * @param codError
	 *            String Codigo de error de respuesta de la transaccion LZCP
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Obtiene Descripcion del Codigo de error de respuesta de la transaccion
	 * LZCP
	 * 
	 * @return String Descripcion del Codigo de error de respuesta de la
	 *         transaccion LZCP
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * Asigna Descripcion del Codigo de error de respuesta de la transaccion
	 * LZCP
	 * 
	 * @param descError
	 *            String Descripcion del Codigo de error de respuesta de la
	 *            transaccion LZCP
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}

	/**
	 * Obtiene la HORA de la Operación, que viene de la respuesta de la trx LZCP
	 * 
	 * @return String HORA de la Operación de la Trx LZCP
	 */
	public String getHoraLZCP() {
		return horaLZCP;
	}

	/**
	 * Asigna la HORA de la Operación, que viene de la respuesta de la trx LZCP
	 * 
	 * @param horaLZCP
	 *            String HORA de la Operación, que viene de la respuesta de la trx LZCP
	 */
	public void setHoraLZCP(String horaLZCP) {
		this.horaLZCP = horaLZCP;
	}
}
