package mx.altec.enlace.beans;
/**
 * 
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */
public class GLFacultad {
	
	String claveFacultad;

	public String getClaveFacultad() {
		return claveFacultad;
	}

	public void setClaveFacultad(String claveFacultad) {
		this.claveFacultad = claveFacultad;
	}
	

}
