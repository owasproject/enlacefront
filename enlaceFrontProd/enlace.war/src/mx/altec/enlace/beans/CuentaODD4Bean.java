package mx.altec.enlace.beans;

import java.io.Serializable;

public class CuentaODD4Bean implements Serializable {

	/**
	 * Id generado automaticamente para la serializacion de la clase
	 */
	private static final long serialVersionUID = -8302727844832585472L;
	/**
	 * El numero de cuenta de la transaccion
	 */
	private String numeroCuenta;
	/**
	 * Codigo de entidad perteneciente a la cuenta
	 */
	private String codigoEntidad;
	/**
	 * Codigo de oficina perteneciente a la cuenta
	 */
	private String codigoOficina;
	/**
	 * Codigo de producto perteneciente a la cuenta
	 */
	private String codigoProducto;
	/**
	 * Codigo de subproducto perteneciente a la cuenta
	 */
	private String codigoSubproducto;
	/**
	 * Indicador del estatus de la cuenta
	 */
	private String indicadorEstatusCuenta;
	
	/**
	 * Obtiene el Numero de Cuenta
	 * @return El Numero de Cuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	/**
	 * Asigna el Numero de Cuenta
	 * @param numeroCuenta El Numero de Cuenta que se desea asignar
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	/**
	 * Obtiene el Codigo de Entidad
	 * @return El Codigo de Entidad
	 */
	public String getCodigoEntidad() {
		return codigoEntidad;
	}
	/**
	 * Asigna un valor para el Codigo de Entidad
	 * @param codigoEntidad El Codigo de Entidad que se desea asignar
	 */
	public void setCodigoEntidad(String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	/**
	 * Obtiene el Codigo de Oficina
	 * @return El Codigo de Oficina
	 */
	public String getCodigoOficina() {
		return codigoOficina;
	}
	/**
	 * Asigna un valor para el Codigo de Oficina
	 * @param codigoOficina El Codigo de Oficina que se va a asignar
	 */
	public void setCodigoOficina(String codigoOficina) {
		this.codigoOficina = codigoOficina;
	}
	/**
	 * Obtiene el Codigo del Producto
	 * @return El Codigo del Producto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	/**
	 * Asigna un valor al Codigo del Producto
	 * @param codigoProducto El Codigo del Producto que se desea asignar
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	/**
	 * Obtiene el Codigo del Subproducto
	 * @return El Codigo del Subproducto
	 */
	public String getCodigoSubproducto() {
		return codigoSubproducto;
	}
	/**
	 * Asigna un valor al Codigo del Subproducto
	 * @param codigoSubproducto El Codigo del Subproducto que se desea asignar
	 */
	public void setCodigoSubproducto(String codigoSubproducto) {
		this.codigoSubproducto = codigoSubproducto;
	}
	/**
	 * Obtiene el Indicador del Estatus de la Cuenta
	 * @return El Indicador del Estatus de la Cuenta
	 */
	public String getIndicadorEstatusCuenta() {
		return indicadorEstatusCuenta;
	}
	/**
	 * Asigna un valor a el Indicador del Estatus de la Cuenta
	 * @param indicadorEstatusCuenta El Indicador del Estatus de la Cuenta que se desea asignar
	 */
	public void setIndicadorEstatusCuenta(String indicadorEstatusCuenta) {
		this.indicadorEstatusCuenta = indicadorEstatusCuenta;
	}
}
