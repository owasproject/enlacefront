package mx.altec.enlace.beans;

import java.io.Serializable;

public class NomPreTarjeta implements Serializable {

	private static final long serialVersionUID = 7544873097755357216L;
	
	public static int LNG_NO_TARJETA_MIN = 16;

	public static int LNG_NO_TARJETA = 22;
	
	public static int LNG_FEC_VIGENCIA = 10;
	
	public static int LNG_COD_ESTADO = 1;
	
	public static int LNG_DESCRIPCION = 20;
	
	public static int LNG_OBSERVACIONES = 20;	
	
	public static int ESTADO_RECHAZADA = 6;
	
	public static int ESTADO_BAJA = 5;
	
	public static int ESTADO_ACTIVA = 4;
	
	public static int ESTADO_ASIGNADA = 3;
	
	public static int ESTADO_RECEPCION = 2;
	
	public static int ESTADO_ENVIADA = 1;
	
	private String noTarjeta;

	private String fechaVigencia;

	private String estadoTarjeta;

	private String descEstadoTarjeta;

	private String observaciones;				
	
	public NomPreTarjeta() {
	}

	public NomPreTarjeta(String noTarjeta, String estadoTarjeta) {
		this.noTarjeta = noTarjeta;
		this.estadoTarjeta = estadoTarjeta;
	}

	public NomPreTarjeta(String noTarjeta, String fechaVigencia,
			String estadoTarjeta, String descEstadoTarjeta) {
		this(noTarjeta, estadoTarjeta);
		this.fechaVigencia = fechaVigencia;
		this.descEstadoTarjeta = descEstadoTarjeta;
	}

	public NomPreTarjeta(String noTarjeta, String fechaVigencia,
			String estadoTarjeta, String descEstadoTarjeta, String observaciones) {
		this(noTarjeta, fechaVigencia, estadoTarjeta, descEstadoTarjeta);
		this.observaciones = observaciones;
	}

	public String getDescEstadoTarjeta() {
		return descEstadoTarjeta;
	}

	public void setDescEstadoTarjeta(String descEstadoTarjeta) {
		this.descEstadoTarjeta = descEstadoTarjeta;
	}

	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public String getNoTarjeta() {
		return noTarjeta;
	}

	public void setNoTarjeta(String noTarjeta) {
		this.noTarjeta = noTarjeta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
}
