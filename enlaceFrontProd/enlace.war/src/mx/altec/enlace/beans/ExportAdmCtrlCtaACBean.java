/** 
*   Isban Mexico
*   Clase: ExportAdmCtrlCtaACBean.java
*   Descripcion: Objeto de transporte de datos para administrar el control de cuentas.
*   
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Se agrega campo divisa. 
*/


package mx.altec.enlace.beans;


/**
 * La clase ExportAdmCtrlCtaACBean.
 */
public class ExportAdmCtrlCtaACBean {

	/**
	 * La clase ExportAdmCtrlCtaACBeanData.
	 */
	public static class ExportAdmCtrlCtaACBeanData {
		/**
		 * Variable para almacenar la fechaRegistro.
		 */
		private String fechaRegistro;
		/**
		 * Variable para almacenar el folioRegistro.
		 */
		private String folioRegistro;
		/**
		 * Variable para almacenar la fechaAtorizacion.
		 */
		private String fechaAtorizacion;
		/**
		 * Variable para almacenar el folioAutorizacion.
		 */
		private String folioAutorizacion;
		/**
		 * Variable para almacenar el usrRegistrado.
		 */
		private String usrRegistrado;
		/**
		 * Variable para almacenar el usrAutorizado.
		 */
		private String usrAutorizado;
		/**
		 * Variable para almacenar las observaciones.
		 */
		private String observaciones;

        /**
         * Obtiene el valor de fecha registro.
         *
         * @return fechaRegistro de tipo String
         */
        public String getFechaRegistro() {
            return fechaRegistro;
        }

        /**
         * Asigna la fecha registro.
         *
         * @param fechaRegistro de tipo String
         */
        public void setFechaRegistro(String fechaRegistro) {
            this.fechaRegistro = fechaRegistro;
        }

        /**
         * Obtiene el valor de folio registro.
         *
         * @return folioRegistro de tipo String
         */
        public String getFolioRegistro() {
            return folioRegistro;
        }

        /**
         * Asigna el folio registro.
         *
         * @param folioRegistro de tipo String
         */
        public void setFolioRegistro(String folioRegistro) {
            this.folioRegistro = folioRegistro;
        }

        /**
         * Obtiene el valor de fecha atorizacion.
         *
         * @return  fechaAtorizacion de tipo String
         */
        public String getFechaAtorizacion() {
            return fechaAtorizacion;
        }

        /**
         * Asigna la fecha atorizacion.
         *
         * @param fechaAtorizacion de tipo String
         */
        public void setFechaAtorizacion(String fechaAtorizacion) {
            this.fechaAtorizacion = fechaAtorizacion;
        }

        /**
         * Obtiene el valor de folio autorizacion.
         *
         * @return folioAutorizacion de tipo String
         */
        public String getFolioAutorizacion() {
            return folioAutorizacion;
        }

        /**
         * Asigna el folio autorizacion.
         *
         * @param folioAutorizacion de tipo String
         */
        public void setFolioAutorizacion(String folioAutorizacion) {
            this.folioAutorizacion = folioAutorizacion;
        }

        /**
         * Obtiene el valor de usr registrado.
         *
         * @return usrRegistrado de tipo String
         */
        public String getUsrRegistrado() {
            return usrRegistrado;
        }

        /**
         * Asigna el usr registrado.
         *
         * @param usrRegistrado de tipo String
         */
        public void setUsrRegistrado(String usrRegistrado) {
            this.usrRegistrado = usrRegistrado;
        }

        /**
         * Obtiene el valor de usr autorizado.
         *
         * @return usrAutorizado de tipo String
         */
        public String getUsrAutorizado() {
            return usrAutorizado;
        }

        /**
         * Asigna el usr autorizado.
         *
         * @param usrAutorizado de tipo String
         */
        public void setUsrAutorizado(String usrAutorizado) {
            this.usrAutorizado = usrAutorizado;
        }

        /**
         * Obtiene el valor de observaciones.
         *
         * @return observaciones de tipo String
         */
        public String getObservaciones() {
            return observaciones;
        }

        /**
         * Asigna las observaciones.
         *
         * @param observaciones de tipo String
         */
        public void setObservaciones(String observaciones) {
            this.observaciones = observaciones;
        }
	}
	
	/**
	 * Variable para almacenar el valor del check.
	 */
	private String check;
	/**
	 * Variable para almacenar el tipoOperacion.
	 */
	private String tipoOperacion;
	/**
	 * Variable para almacenar el tipoCuenta.
	 */
	private String tipoCuenta;
	/**
	 * Variable para almacenar la cuenta.
	 */
	private String cuenta;
	/**
	 * Variable para almacenar el titular.
	 */
	private String titular;
	/**
	 * Variable para almacenar el banco.
	 */
	private String banco;
	/**
	 * Variable para almacenar el estado.
	 */
	private String estado;
	/**
	 * Variable para almacenar la divisa.
	 */
	private String divisa;
	
	/** The detalle oper. */
	private ExportAdmCtrlCtaACBeanData detalleOper = new ExportAdmCtrlCtaACBeanData();

	/**
     * Gets the detalle oper.
     *
     * @return the detalleOper
     */
    public ExportAdmCtrlCtaACBeanData getDetalleOper() {
        return detalleOper;
    }

    /**
     * Sets the detalle oper.
     *
     * @param detalleOper the detalleOper to set
     */
    public void setDetalleOper(ExportAdmCtrlCtaACBeanData detalleOper) {
        this.detalleOper = detalleOper;
    }

	/**
	 * getCheck de tipo String.
	 * @author FSW-Indra
	 * @return check de tipo String
	 */
	public String getCheck() {
		return check;
	}
	/**
	 * setCheck para asignar valor a check.
	 * @author FSW-Indra
	 * @param check de tipo String
	 */
	public void setCheck(String check) {
		this.check = check;
	}
	/**
	 * getTipoOperacion de tipo String.
	 * @author FSW-Indra
	 * @return tipoOperacion de tipo String
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * setTipoOperacion para asignar valor a tipoOperacion.
	 * @author FSW-Indra
	 * @param tipoOperacion de tipo String
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * getTipoCuenta de tipo String.
	 * @author FSW-Indra
	 * @return tipoCuenta de tipo String
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * setTipoCuenta para asignar valor a tipoCuenta.
	 * @author FSW-Indra
	 * @param tipoCuenta de tipo String
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * getCuenta de tipo String.
	 * @author FSW-Indra
	 * @return cuenta de tipo String
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * setCuenta para asignar valor a cuentacuenta.
	 * @author FSW-Indra
	 * @param cuenta de tipo String
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	/**
	 * getTitular de tipo String.
	 * @author FSW-Indra
	 * @return titular de tipo String
	 */
	public String getTitular() {
		return titular;
	}
	/**
	 * setTitular para asignar valor a titular.
	 * @author FSW-Indra
	 * @param titular de tipo String
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}
	/**
	 * getBanco de tipo String.
	 * @author FSW-Indra
	 * @return banco de tipo String
	 */
	public String getBanco() {
		return banco;
	}
	/**
	 * setBanco para asignar valor a banco.
	 * @author FSW-Indra
	 * @param banco de tipo String
	 */
	public void setBanco(String banco) {
		this.banco = banco;
	}
	/**
	 * getFechaRegistro de tipo String.
	 * @author FSW-Indra
	 * @return fechaRegistro de tipo String
	 */
	public String getFechaRegistro() {
		return getDetalleOper().getFechaRegistro();
	}
	/**
	 * setFechaRegistro para asignar valor a fechaRegistro.
	 * @author FSW-Indra
	 * @param fechaRegistro de tipo String
	 */
	public void setFechaRegistro(String fechaRegistro) {
        this.getDetalleOper().setFechaRegistro(fechaRegistro);
	}
	/**
	 * getFolioRegistro de tipo String.
	 * @author FSW-Indra
	 * @return folioRegistro de tipo String
	 */
	public String getFolioRegistro() {
		return getDetalleOper().getFolioRegistro();
	}
	/**
	 * setFolioRegistro para asignar valor a folioRegistro.
	 * @author FSW-Indra
	 * @param folioRegistro de tipo String
	 */
	public void setFolioRegistro(String folioRegistro) {
        this.getDetalleOper().setFolioRegistro(folioRegistro);
	}
	/**
	 * getFechaAtorizacion de tipo String.
	 * @author FSW-Indra
	 * @return fechaAtorizacion de tipo String
	 */
	public String getFechaAtorizacion() {
		return getDetalleOper().getFechaAtorizacion();
	}
	/**
	 * setFechaAtorizacion para asignar valor a fechaAtorizacion.
	 * @author FSW-Indra
	 * @param fechaAtorizacion de tipo String
	 */
	public void setFechaAtorizacion(String fechaAtorizacion) {
        this.getDetalleOper().setFechaAtorizacion(fechaAtorizacion);
	}
	/**
	 * getFolioAutorizacion de tipo String.
	 * @author FSW-Indra
	 * @return folioAutorizacion de tipo String
	 */
	public String getFolioAutorizacion() {
		return getDetalleOper().getFolioAutorizacion();
	}
	/**
	 * setFolioAutorizacion para asignar valor a folioAutorizacion.
	 * @author FSW-Indra
	 * @param folioAutorizacion de tipo String
	 */
	public void setFolioAutorizacion(String folioAutorizacion) {
        this.getDetalleOper().setFolioAutorizacion(folioAutorizacion);
	}
	/**
	 * getUsrRegistrado de tipo String.
	 * @author FSW-Indra
	 * @return usrRegistrado de tipo String
	 */
	public String getUsrRegistrado() {
		return getDetalleOper().getUsrRegistrado();
	}
	/**
	 * setUsrRegistrado para asignar valor a usrRegistrado.
	 * @author FSW-Indra
	 * @param usrRegistrado de tipo String
	 */
	public void setUsrRegistrado(String usrRegistrado) {
        this.getDetalleOper().setUsrRegistrado(usrRegistrado);
	}
	/**
	 * getUsrAutorizado de tipo String.
	 * @author FSW-Indra
	 * @return usrAutorizado de tipo String
	 */
	public String getUsrAutorizado() {
		return getDetalleOper().getUsrAutorizado();
	}
	/**
	 * setUsrAutorizado para asignar valor a usrAutorizado.
	 * @author FSW-Indra
	 * @param usrAutorizado de tipo String
	 */
	public void setUsrAutorizado(String usrAutorizado) {
        this.getDetalleOper().setUsrAutorizado(usrAutorizado);
	}
	/**
	 * getEstado de tipo String.
	 * @author FSW-Indra
	 * @return estado de tipo String
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * setEstado para asignar valor a estado.
	 * @author FSW-Indra
	 * @param estado de tipo String
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * getDivisa de tipo String.
	 * @return divisa de tipo String
	 */
	public String getDivisa() {
		return divisa;
	}
	/**
	 * setDivisa para asignar valor a divisa.
	 * @param divisa de tipo String
	 */
	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}
	/**
	 * getObservaciones de tipo String.
	 * @author FSW-Indra
	 * @return observaciones de tipo String
	 */
	public String getObservaciones() {
		return getDetalleOper().getObservaciones();
	}
	/**
	 * setObservaciones para asignar valor a observaciones.
	 * @author FSW-Indra
	 * @param observaciones de tipo String
	 */
	public void setObservaciones(String observaciones) {
        this.getDetalleOper().setObservaciones(observaciones);
	}

}
