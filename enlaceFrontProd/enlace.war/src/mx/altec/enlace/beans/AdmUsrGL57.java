package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;

public class AdmUsrGL57 extends GLBase {

	public static final String HEADER = "GL5700821123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrGL57> BUILDER = new GL57Builder();

	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345";
	
	private static final String FMT_PAGINACION = "@DCGLM5730 P";  
	
	private static final String FMT_CABECERA = "@DCGLM5710 P";
	
	private static final String FTM_DETALLE = "@DCGLM5720 P";    
    		
	/////////////////////////////////////

	private int numeroRegistros;
	
	private GLPerfil perfil;
	
	private List<GLFacultadCta> facultades;
	
	private String facultadPaginacion;
	
	private AdmUsrGL57() {		
	}		
	
	public int getNumeroRegistros() {
		return numeroRegistros;
	}
	
	public GLPerfil getPerfil() {
		return perfil;
	}
	
	public List<GLFacultadCta> getFacultades() {
		return facultades;
	}	
	
	public String getFacultadPaginacion() {
		return facultadPaginacion;
	}
	
	/////////////////////////////////////
	
	public static String tramaConsulta(String clavePerfilPrototipo, String claveFacultad, String usuarioOperacion, String facultadPaginacion) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(clavePerfilPrototipo, 8));
		sb.append(rellenar(claveFacultad, 16));
		sb.append(facultadPaginacion == null || facultadPaginacion.length() == 0 ? "  " : "SI"); //2
		sb.append(rellenar(facultadPaginacion, 16));
		sb.append(rellenar(usuarioOperacion, 8));
		
		return sb.toString();
	}
	
	/////////////////////////////////////
	
	public static class GL57Builder implements AdmUsrBuilder<AdmUsrGL57> {

		public AdmUsrGL57 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrGL57 gl57 = new AdmUsrGL57();
			
			if (isCodigoError(string)) {
				
				gl57.setCodMsg(getCodigoError(string));	        
				gl57.setMensaje(getMensajeError(string));
	        	
				return gl57;
			}

	        if (string.startsWith(INDICADOR_EXITO)) {	        	        	    
	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<GLFacultadCta> facultades = new ArrayList<GLFacultadCta>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		GLFacultadCta facultad = new GLFacultadCta();
	        		facultad.setClaveFacultad(string.substring(ind, (ind += 8)));
	        		facultad.setDescripcion(string.substring(ind, (ind += 80)).trim());
	        		facultad.setNombre(string.substring(ind, (ind += 80)).trim());
	        		facultad.setVisible(string.substring(ind, (ind += 2)));
	        		facultad.setUso(string.substring(ind, (ind += 2)));
	        		facultad.setNivel(string.substring(ind, (ind += 1)));
	        		facultad.setGrupo(string.substring(ind, (ind += 3)));
	        		
	        		facultades.add(facultad);
	        	}      
	        	
	        	gl57.facultades = facultades;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		gl57.facultadPaginacion = string.substring(ind, (ind += 16));
	        	}
	        	
	        	//SE PROCESA LA INFORMACION DE CABECERA
	        	
	        	if ((ind = string.indexOf(FMT_CABECERA)) != -1) {
	        		
	        		ind += FMT_CABECERA.length();
	        			        			        		
	        		//Se salta el numero de registros
	        		gl57.numeroRegistros = Integer.parseInt(string.substring(ind, (ind += 3)));
	        		
	        		GLPerfil perfil = new GLPerfil();
	        		perfil.setClave(string.substring(ind, (ind += 8)));
	        		perfil.setDescripcion(string.substring(ind, (ind += 80)).trim());
	        		perfil.setNombreComercial(string.substring(ind, (ind += 40)).trim());
	        		
	        		gl57.perfil = perfil;
	        	}	        
	        }

	        return gl57;        	
		}	
	}	
}
