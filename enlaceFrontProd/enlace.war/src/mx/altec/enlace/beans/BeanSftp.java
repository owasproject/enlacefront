package mx.altec.enlace.beans;

import com.jcraft.jsch.*;

public class BeanSftp {
	private Session session;
	private ChannelSftp sftp;
	
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public ChannelSftp getSftp() {
		return sftp;
	}
	public void setSftp(ChannelSftp sftp) {
		this.sftp = sftp;
	}
	
	
}
