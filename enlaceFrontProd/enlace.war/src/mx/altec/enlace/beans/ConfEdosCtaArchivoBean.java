package mx.altec.enlace.beans;

/**
 * @author asanjuan
 *
 */

public class ConfEdosCtaArchivoBean {
	
	/**tipo de cuenta**/
	private String nombreTitular;
	/**tipo de cuenta**/
	private String tipoCuenta;
	/**numero de cuenta**/
	private String nomCuenta;
	/**formato**/
	private String formato;
	/**dispLinea**/
	private char dispLinea;
	/**paperless**/
	private char papaerless;
	/**numero linea error**/
	private int lineaError;
	/**mensaje de error**/
	private String error;
	/**mensaje de error**/
	private String tipoMasivo = "001";
	
	/**
	 * getTipoCuenta
	 * @return String : tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	/**
	 * setTipoCuenta
	 * @param tipoCuenta : tipoCuenta
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * getNomCuenta
	 * @return String 
	 */
	public String getNomCuenta() {
		return nomCuenta;
	}
	/**
	 * setNomCuenta
	 * @param nomCuenta : nomCuenta
	 */
	public void setNomCuenta(String nomCuenta) {
		this.nomCuenta = nomCuenta;
	}
	/**
	 * getFormato
	 * @return String
	 */
	public String getFormato() {
		return formato;
	}
	/**
	 * setFormato
	 * @param formato : formato
	 */
	public void setFormato(String formato) {
		this.formato = formato;
	}
	/**
	 * getDispLinea
	 * @return char
	 */
	public char getDispLinea() {
		return dispLinea;
	}
	/**
	 * setDispLinea
	 * @param dispLinea : dispLinea
	 */
	public void setDispLinea(char dispLinea) {
		this.dispLinea = dispLinea;
	}
	
	/**
	 * getPapaerless
	 * @return : char
	 */
	public char getPapaerless() {
		return papaerless;
	}
	/**
	 * setPapaerless
	 * @param papaerless : papaerless
	 */
	public void setPapaerless(char papaerless) {
		this.papaerless = papaerless;
	}
	
	/**
	 * getLineaError
	 * @return int
	 */
	public int getLineaError() {
		return lineaError;
	}
	/**
	 * setLineaError
	 * @param lineaError : lineaError
	 */
	public void setLineaError(int lineaError) {
		this.lineaError = lineaError;
	}
	/**
	 * getError
	 * @return String
	 */
	public String getError() {
		return error;
	}
	/**
	 * setError
	 * @param error error
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return String : nombre del titular
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}
	/**
	 * @param nombreTitular : nombre del titular
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	/**
	 * getTipoMasivo de tipo String.
	 * @author FSW-Indra
	 * @return tipoMasivo de tipo String
	 */
	public String getTipoMasivo() {
		return tipoMasivo;
	}
	/**
	 * setTipoMasivo para asignar valor a tipoMasivo.
	 * @author FSW-Indra
	 * @param tipoMasivo de tipo String
	 */
	public void setTipoMasivo(String tipoMasivo) {
		this.tipoMasivo = tipoMasivo;
	}
	
	
	
	
}
