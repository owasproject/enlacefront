package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author VSWF esoto
 *	Clase bean que contiene el layout referente a la consulta de codigo postal
 *	a la transaccion TC73
 */
public class NomPreCodPost extends NomPreLM1Base implements Serializable{
	
	private static final long serialVersionUID = -5384764057454682564L;
	
	public static String HEADER = "TC7300451123451O00N2";							   
	
	private static final String FORMATO_HEADER = "@DCTCM074  P";
	
	private static final NomPreCodPostFactory FACTORY = new NomPreCodPostFactory();
	
	public static NomPreCodPostFactory getFactoryInstance() {
		return FACTORY;
	}
	
	private String codPostal;
	private String numConsecutivo;
	private String codAplicacion;
	private String codComuna;
	private String colonia;
	private String nomComuna;
	private String ciudad;
	private String delegacion;
	private String estado;
	private List<NomPreCodPost> detalle;
	
	public String getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodAplicacion() {
		return codAplicacion;
	}

	public void setCodAplicacion(String codAplicacion) {
		this.codAplicacion = codAplicacion;
	}

	public String getCodComuna() {
		return codComuna;
	}

	public void setCodComuna(String codComuna) {
		this.codComuna = codComuna;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNomComuna() {
		return nomComuna;
	}

	public void setNomComuna(String nomComuna) {
		this.nomComuna = nomComuna;
	}

	public String getNumConsecutivo() {
		return numConsecutivo;
	}

	public void setNumConsecutivo(String numConsecutivo) {
		this.numConsecutivo = numConsecutivo;
	}

	public List<NomPreCodPost> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<NomPreCodPost> detalle) {
		this.detalle = detalle;
	}
	
	private static class NomPreCodPostFactory implements NomPreBuilder<NomPreCodPost> {

		public NomPreCodPost build(String arg) {
						
			NomPreCodPost bean = new NomPreCodPost();
			
			if (isCodigoExito(arg)) {
				bean.setCodigoOperacion("TC730000");
				bean.setCodExito(true);
				
				String[] detalles = getDetalles(arg, FORMATO_HEADER, 261);
				
				if (detalles != null && detalles.length > 0) {					
					
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new ArrayList<NomPreCodPost>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}				
						
						NomPreCodPost codPostales = new NomPreCodPost();
						
						codPostales.setCodPostal(getValor(detalle, 0, 8));
						codPostales.setNumConsecutivo(getValor(detalle, 8, 5));
						codPostales.setCodAplicacion(getValor(detalle, 13, 2));
						codPostales.setCodComuna(getValor(detalle, 15, 5));
						codPostales.setColonia(getValor(detalle, 20, 30));
						codPostales.setNomComuna(getValor(detalle, 50, 7));
						codPostales.setCiudad(getValor(detalle, 57, 30));
						codPostales.setDelegacion(getValor(detalle, 87, 50));
						codPostales.setEstado(getValor(detalle, 137, 40));
	
						bean.getDetalle().add(codPostales);
					}
				}
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}	
	
			return bean;										
		}	
	}
}
