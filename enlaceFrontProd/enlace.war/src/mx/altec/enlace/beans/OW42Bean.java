package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.Global.ADMUSR_MQ_USUARIO;
import static mx.altec.enlace.utilerias.Global.NP_MQ_TERMINAL;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Locale;

import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class OW42Bean implements Serializable {
	/** Id de version generado automaticamente para la serializacion de la clase **/
	private static final long serialVersionUID = -1238509348507987928L;
	/** Id de la trama **/
	private static final String ID_TRAMA = "OW42";
	/** Numero de periodos que se van a solicitar en la trama **/
	private static final String NUMERO_PERIODOS = "018";
	/** Tipo de estado de cuenta enviado a la trama **/
	private static final String TIPO_ESTADO_CUENTA = "  ";
	/** Pais enviado en la trama **/
	private static final String PAIS = "MX";
	/** Numero de cliente cuando se trate de un estado de cuenta XML **/
	private static final String NUMERO_CLIENTE_OW42 = "00000000";
	/** No existen periodos **/
	private static final String NO_EXISTEN_PERIODOS = "OWA0007";
	/** Tipo que define TDC **/
	private static final String TIPO_EDCCTA_TDC = "003";
	/** Constante para log del sistema **/
	private static final String LOGTAG = "[EdcTdc:::OW42Bean]-";
	
	/** Codigo de respuesta de la transaccion **/
	private String codigoRespuesta;
	/** Descripcion de la respuesta enviada por la transaccion **/
	private String descripcionRespuesta;
	/** Numero de cuenta del que se desean los periodos **/
	private String numeroCuenta;
	/** Tipo de Estado de Cuenta **/
	private String tipoEstadoCuenta;
	/** Numero de Periodos a consultar **/
	private String numPeriodos;
	/** Numero de Cliente **/
	private String numeroCliente;
	/** Pais **/
	private String codigoPais;
	/** Periodos devueltos por la transaccion **/
	private List<PeriodoOW42> periodos;

	/**
	 * Llena valores constantes en el BEAN
	 */
	public OW42Bean() {
		this.tipoEstadoCuenta=TIPO_ESTADO_CUENTA;
		this.numPeriodos=NUMERO_PERIODOS;
		this.numeroCliente=NUMERO_CLIENTE_OW42;
		this.codigoPais=PAIS;
	}

	/**
	 * Obtiene el codigo de respuesta
	 * @return El codigo de respuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	/**
	 * Asigna un valor al codigo de respuesta
	 * @param codigoRespuesta El codigo de respuesta que se desea asignar
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	/**
	 * Obtiene la descripcion de la respuesta
	 * @return La descripcion
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	/**
	 * Asigna un valor a la descripcion de la respuesta
	 * @param descripcionRespuesta La descripcion de la respuesta que se desea asignar
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}
	/**
	 * Obtiene el numero de cuenta
	 * @return El numero de cuenta
	 * @throws ValidationException En caso de que no se haya especificado un valor para el numero de cuenta
	 */
	public String getNumeroCuenta() throws ValidationException {
		if(numeroCuenta == null || numeroCuenta.length() == 0){
			throw new ValidationException("OW42E01");
		}
		
		return numeroCuenta;
	}
	/**
	 * Asigna un valor al numero de cuenta
	 * @param numeroCuenta El numero de cuenta que se desea asignar
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	
	/**
	 * @return el tipoEstadoCuenta
	 */
	public String getTipoEstadoCuenta() {
		return tipoEstadoCuenta;
	}
	/**
	 * @param tipoEstadoCuenta el tipoEstadoCuenta a establecer
	 */
	public void setTipoEstadoCuenta(String tipoEstadoCuenta) {
		this.tipoEstadoCuenta = tipoEstadoCuenta;
	}
	/**
	 * @return el numPeriodos
	 */
	public String getNumPeriodos() {
		return numPeriodos;
	}
	/**
	 * @param numPeriodos el numPeriodos a establecer
	 */
	public void setNumPeriodos(String numPeriodos) {
		this.numPeriodos = numPeriodos;
	}
	/**
	 * @return el numeroCliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}
	/**
	 * @param numeroCliente el numeroCliente a establecer
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}
	/**
	 * @return el pais
	 */
	public String getCodigoPais() {
		return codigoPais;
	}
	/**
	 * @param codigoPais el pais a establecer
	 */
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	/**
	 * Obtiene las parejas de los folios en el formato MMM-YYYY:::Folio
	 * @return Una lista la cual contiene las parejas de los folios
	 */
	public List<String> obtenerParejasPeriodoFolio(){
		List<String> parejaPeriodos = new ArrayList<String>();
		
		if(periodos != null){
			for(PeriodoOW42 periodoOW42 : periodos){
				parejaPeriodos.add(String.format("%s:::%s@%s@%s", 
						periodoOW42.getPeriodoConFormato(), 
						periodoOW42.getFolioOnDemand(), 
						formatTipoEDC(periodoOW42.getTipoEstadoCuenta()),
						periodoOW42.getPeriodo()
				));
			}
		}
		return parejaPeriodos;
	}
	
	/**
	 * Metodo para formatear cadena de tipoEDC, a 3 posiciones rellenando con 0 a la izq.
	 * @param cad Cadena a formatear.
	 * @return Cadena formateada.
	 */
	private String formatTipoEDC(String cad){
		if (cad.length()==3) {
			return cad;
		}
		else {
			switch (cad.length()) {
				case 1: 
					cad=String.format("00%s",cad);
					break;
				case 2:
					cad=String.format("0%s",cad);
					break;
				default:break;
			}
			return cad;
		}
	}
	
	/**
	 * Obtiene los periodos
	 * @return Los periodos
	 */
	public List<PeriodoOW42> getPeriodos() {
		return periodos;
	}
	/**
	 * Asigna un valor a los periodos
	 * @param periodos Los periodos que se desean asignar
	 */
	public void setPeriodos(List<PeriodoOW42> periodos) {
		this.periodos = periodos;
	}
	/**
	 * Agrega un periodos
	 * @param periodo El periodo que se desea asignar
	 */
	public void agregarPeriodo(PeriodoOW42 periodo){
		if(this.periodos == null){
			this.periodos = new ArrayList<PeriodoOW42>();
		}
		
		this.periodos.add(periodo);
	}
	
	/**
	 * Genera la trama de entrada con la informacion almacenada en la instancia de esta clase
	 * @return La trama de entrada
	 * @throws ValidationException En caso de que la informacion necesaria no este completa
	 */
	public String generarMensajeEntrada() throws ValidationException {
		final StringBuilder trama = new StringBuilder();
		
		trama
			.append(rellenar(getNumPeriodos(), 3))
			.append(rellenar(getNumeroCuenta(), 20, ' ', 'I'))
			.append(rellenar(getTipoEstadoCuenta(), 2))
			.append(rellenar(getCodigoPais(), 2))
			.append(rellenar(getNumeroCliente(), 8));
		
		return agregarEncabezadoPS7(ID_TRAMA, trama);
	}
	
	/**
	 * Genera el encabezado de entrada PS7, el cual es utilizado por las transacciones
	 * @param transaccion Transaccion que se desea ejecutar
	 * @param trama La trama de entrada
	 * @return Trama de entrada con el encabeazado PS7
	 */
	private String agregarEncabezadoPS7(String transaccion, StringBuilder trama) {
		EIGlobal.mensajePorTrace(LOGTAG + "::creaTrama() Inicio", EIGlobal.NivelLog.INFO);
		final StringBuffer tramaBuffer = new StringBuffer();
		tramaBuffer.append(rellenar(NP_MQ_TERMINAL, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(ADMUSR_MQ_USUARIO, 8, ' ', 'D'));
		tramaBuffer.append(rellenar(transaccion, 4, ' ', 'D'));
		tramaBuffer.append(rellenar(Integer.toString(trama.length() + 32), 4, '0', 'I'));
		tramaBuffer.append("1123451O00N2");
		tramaBuffer.append(trama);

		EIGlobal.mensajePorTrace(LOGTAG + "::creaTrama() Fin", EIGlobal.NivelLog.INFO);
		return tramaBuffer.toString();
	}
	
	/**
	 * Obtiene el mensaje de salida y asigna los valores a las variables de la clase
	 * @param mensajesRespuesta Mensaje de repuesta obtenido de la ejecucion de la trama OW42
	 * @throws BusinessException En caso de error al obtener los periodos
	 */
	public void procesarMensajeRespuesta(String[] mensajesRespuesta) throws BusinessException{
		
		SimpleDateFormat sFormat2 = new SimpleDateFormat("yyyyMM", Locale.US);
		Calendar calendar = Calendar.getInstance();
		
		for(int i = 0  ; i < 18 ;i++ ) {
			calendar.add(Calendar.MONTH, -1);
			String ultPeriodo = sFormat2.format(calendar.getTime());
			boolean existe = false;			
			EIGlobal.mensajePorTrace(LOGTAG + " --> Procesando Periodo [" + ultPeriodo + "] <--",EIGlobal.NivelLog.INFO);
			for(int contMsj = 3; contMsj < mensajesRespuesta.length; contMsj++){			
				final String mensajeRespuesta = mensajesRespuesta[contMsj];
				if(!NO_EXISTEN_PERIODOS.equals(mensajeRespuesta.substring(2, 9).trim())) {
					EIGlobal.mensajePorTrace(LOGTAG + "MsjRep ["+mensajeRespuesta+"] Longitud["+mensajeRespuesta.length()+"]", NivelLog.DEBUG);
					this.setCodigoRespuesta(mensajeRespuesta.substring(2, 9).trim());
					String per=lPad(mensajeRespuesta.substring(11,17),6,"0");
					String tec=lPad(mensajeRespuesta.substring(25,27),3,"0");
					String fod=(TIPO_EDCCTA_TDC.equals(tec)) ? lPad("",8,"0") : lPad(mensajeRespuesta.substring(17,25),8,"0");
					String cad=per+"@"+fod+"@"+tec;
					
					EIGlobal.mensajePorTrace(LOGTAG + " Compara Periodo ["+ultPeriodo+"] == ["+per,EIGlobal.NivelLog.INFO);
					if(ultPeriodo.equals(per)) {						
						
						final PeriodoOW42 periodo = new PeriodoOW42();
						periodo.setPeriodo(per);
						periodo.setFolioOnDemand(fod);
						periodo.setTipoEstadoCuenta(tec);
						periodo.setCadena(cad);
						agregarPeriodo(periodo);
						
						EIGlobal.mensajePorTrace(LOGTAG+":::Se agrega periodo::: "+
								" periodo["+periodo.getPeriodo()+"]"+
								" folioOD["+periodo.getFolioOnDemand()+"]"+
								" TipoEDC["+periodo.getTipoEstadoCuenta()+"]"+
								" Cadena["+periodo.getCadena()+"]"
								, NivelLog.DEBUG);
	
						existe = true;
						contMsj = mensajesRespuesta.length+1;
					}
				}
				
			}
			if(!existe) {
				EIGlobal.mensajePorTrace(LOGTAG + " No existe información para el periodo ["+
						ultPeriodo+"] Se carga vacio FolioOD["+lPad("",8,"0")+"] Tipo["+lPad("",3,"0")+"]", NivelLog.INFO);
				final PeriodoOW42 periodo = new PeriodoOW42();
				periodo.setPeriodo(ultPeriodo);
				periodo.setFolioOnDemand(lPad("",8,"0"));
				periodo.setTipoEstadoCuenta(lPad("",3,"0"));
				agregarPeriodo(periodo);
			}	
		}
	}
	
	/**
	 * Obtiene el mensaje de salida de una ejecucion erronea y asigna los valores a las variables de la clase
	 * @param mensajeRespuesta Mensaje de respuesta obtenido de la ejecucion de la trama OW42
	 */
	public void procesarMensajeRespuestaErronea(String mensajeRespuesta){
		EIGlobal.mensajePorTrace(LOGTAG + " ::: Mensaje Respuesta OW42 :: " + mensajeRespuesta, 
				EIGlobal.NivelLog.ERROR);
		this.setCodigoRespuesta(mensajeRespuesta.substring(0, 7).trim());
		this.setDescripcionRespuesta(mensajeRespuesta.substring(7, 57).trim());
	}
	/**
	 * Metodo para rellenar a la izquierda
	 * @author FSW-Indra
	 * @param string de tipo String
	 * @param length de tipo int
	 * @param padString de tipo String
	 * @return String
	 */
	public String lPad(String string,int length,String padString) {
	    return (string==null) ? "" 
	    		:(string.length()<=length) ? String.format("%"+length+"s",string).replace(" ", padString)
	    				:string.substring(0,length);
	}
}