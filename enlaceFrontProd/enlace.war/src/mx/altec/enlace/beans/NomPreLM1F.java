package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import static mx.altec.enlace.beans.NomPreEmpleado.*;
import static mx.altec.enlace.beans.NomPreTarjeta.*;

import java.io.Serializable;
public class NomPreLM1F extends NomPreLM1Base implements Serializable{

	private static final long serialVersionUID = -5384764057454682564L;

	public static String HEADER = "LM1F00721123451O00N2";

	private static final String FORMATO_HEADER = "@DCLMCCLI1 P";

	private static final NomPreLM1FFactory FACTORY = new NomPreLM1FFactory();

	private NomPreTarjeta tarjeta;

	private NomPreEmpleado empleado;

	public NomPreTarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(NomPreTarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public NomPreEmpleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(NomPreEmpleado empleado) {
		this.empleado = empleado;
	}

	public static NomPreLM1FFactory getFactoryInstance() {
		return FACTORY;
	}

	private static class NomPreLM1FFactory implements NomPreBuilder<NomPreLM1F> {

		public NomPreLM1F build(String arg) {

			NomPreLM1F bean = new NomPreLM1F();

			if (isCodigoExito(arg)) {
				bean.setCodigoOperacion("LM1F0000");
				bean.setCodExito(true);

				int index = arg.indexOf(FORMATO_HEADER);

				if (index != -1) {
					index += FORMATO_HEADER.length();
					bean.setTarjeta(
							new NomPreTarjeta(
									getValor(arg, index, LNG_NO_TARJETA), 		//NUM-TARJETA-S
									getValor(arg, index += 22, LNG_COD_ESTADO))	//COD-EST-TARJ-S
							);

					bean.setEmpleado(
							new NomPreEmpleado(
								null,
								getValor(arg, index += 1, LNG_NOMBRE), 				//NOM-EMPLEADO
								getValor(arg, index += 30, LNG_PATERNO), 			//APE-PATERNO
								getValor(arg, index += 30, LNG_MATERNO), 			//APE-MATERNO
								getValor(arg, index += 20, LNG_RFC), 				//COD-RFC
								getValor(arg, index += 10, LNG_HOMOCLAVE), 			//COD-HOMOCLAVE
								getValor(arg, index += 3, LNG_NUM_ID), 				//NUM-ID VSWF ESC nuevos campos NP
								getValor(arg, index += 25, LNG_FECHA_NACIMIENTO),	//FEC-NACIMIENTO //*
								getValor(arg, index += 10, LNG_SEXO), 				//IND-SEXO
								getValor(arg, index += 1, LNG_NACIONALIDAD), 		//COD-NACION
								getValor(arg, index += 4, LNG_PAIS), 				//COD-PAIS VSWF ESC nuevos campos NP
								getValor(arg, index += 25, LNG_ESTADO_CIVIL), 		//COD-EST-CIVIL
								getValor(arg, index += 1, LNG_CORREO_ELECTRONICO), 	//DES-MAIL
								getValor(arg, index += 30, LNG_CALLE), 				//DES-DOMICILIO
								getValor(arg, index += 50, LNG_NUM_EXT), 			//NUM-DOMICILIO	 VSWF ESC nuevos campos NP
								getValor(arg, index += 10, LNG_NUM_INT), 			//NUM-INT VSWF ESC nuevos campos NP
								getValor(arg, index += 10, LNG_COLONIA), 			//DES-COLONIA						
								getValor(arg, index += 30, LNG_DELEGACION), 		//DES-DELEGACION  
								getValor(arg, index += 20, LNG_CIUDAD), 			//DES-CIUDAD  
								getValor(arg, index += 30, LNG_ESTADO), 			//COD-ESTADO
								getValor(arg, index += 30, LNG_CODIGO_POSTAL), 		//COD-POSTAL
								getValor(arg, index += 5, LNG_LADA), 				//COD-LADA
								getValor(arg, index += 5, LNG_TELEFONO) 			//COD-TELEFONO
								)
							);
				}
			} else if (isCodigoError(arg))  {

				bean.setCodigoOperacion(getCodigoError(arg));
			}

			return bean;
		}
	}
}