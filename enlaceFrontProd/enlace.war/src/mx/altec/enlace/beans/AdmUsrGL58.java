package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;


import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import com.netscape.server.servlet.i18n.io.Serializable;

public class AdmUsrGL58 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL5800771123451O00N2";
	
	public static AdmUsrGL58Factory getFactoryInstance() {
		return FACTORY;
	}	

	
	private static final AdmUsrGL58Factory FACTORY = new AdmUsrGL58Factory();
	
	private static class AdmUsrGL58Factory implements AdmUsrBuilder<AdmUsrGL58> {

		public AdmUsrGL58 build(String arg) {
			
			AdmUsrGL58 bean = new AdmUsrGL58();
			
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GLA8000");
				bean.setCodExito(true);
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}			
			return bean;
		}
	}

}
