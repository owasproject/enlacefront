/**
 *
 */
package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * @author Praxis
 *
 */
public class EmpleadoBean implements Serializable {


	private static final long serialVersionUID = 1L;

	private String contrato;

	private boolean tieneError;

	private String error;

	private String secuencia;

	private String noSecuencia;

	private String noEmpleado;

	private String appPaterno;

	private String appMaterno;

	private String nombreEmpleado;

	private String rfc;

	private String homoclave;

	private String tipoDocto;

	private String noIdentificacion;

	private String sexo;

	private String fechaNac;

	private String cveNacionalidad;

	private String paisNacimiento;

	private String cveEstado;

	private String entidadExtranjera;

	private String edoCivil;

	private String calle;

	private String noExterior;

	private String noInterior;

	private String colonia;

	private String codPostal;

	private String codPostalTrab;

	private String lada;

	private String telefono;

	private String sucursal;

	private String sucursalControl;

	private String codigoCliente;

	private String numTarjeta;

	private String numCuenta;

	private String estatus;

	private String remesa;

	private String reactivacion;

	private String enviarCatalogo;

	private String usuarioSession;

	private String tipoRegistro;

	private String cveEstadoNombre;

	private String correoContrato;

	private String coloniaExtranjero;

	private String cveUsuario;





	public boolean isTieneError() {
		return tieneError;
	}

	public void setTieneError(boolean tieneError) {
		this.tieneError = tieneError;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	/**
	 * @return the contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * @param contrato the contrato to set
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * @return the secuencia
	 */
	public String getSecuencia() {
		return secuencia;
	}

	/**
	 * @param secuencia the secuencia to set
	 */
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

	/**
	 * @return the noSecuencia
	 */
	public String getNoSecuencia() {
		return noSecuencia;
	}

	/**
	 * @param noSecuencia the noSecuencia to set
	 */
	public void setNoSecuencia(String noSecuencia) {
		this.noSecuencia = noSecuencia;
	}

	/**
	 * @return the noEmpleado
	 */
	public String getNoEmpleado() {
		return noEmpleado;
	}

	/**
	 * @param noEmpleado the noEmpleado to set
	 */
	public void setNoEmpleado(String noEmpleado) {
		this.noEmpleado = noEmpleado;
	}

	/**
	 * @return the appPaterno
	 */
	public String getAppPaterno() {
		return appPaterno;
	}

	/**
	 * @param appPaterno the appPaterno to set
	 */
	public void setAppPaterno(String appPaterno) {
		this.appPaterno = appPaterno;
	}

	/**
	 * @return the appMaterno
	 */
	public String getAppMaterno() {
		return appMaterno;
	}

	/**
	 * @param appMaterno the appMaterno to set
	 */
	public void setAppMaterno(String appMaterno) {
		this.appMaterno = appMaterno;
	}



	/**
	 * @return the nombreEmpleado
	 */
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	/**
	 * @param nombreEmpleado the nombreEmpleado to set
	 */
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the homoclave
	 */
	public String getHomoclave() {
		return homoclave;
	}

	/**
	 * @param homoclave the homoclave to set
	 */
	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}

	/**
	 * @return the tipoDocto
	 */
	public String getTipoDocto() {
		return tipoDocto;
	}

	/**
	 * @param tipoDocto the tipoDocto to set
	 */
	public void setTipoDocto(String tipoDocto) {
		this.tipoDocto = tipoDocto;
	}

	/**
	 * @return the noIdentificacion
	 */
	public String getNoIdentificacion() {
		return noIdentificacion;
	}

	/**
	 * @param noIdentificacion the noIdentificacion to set
	 */
	public void setNoIdentificacion(String noIdentificacion) {
		this.noIdentificacion = noIdentificacion;
	}

	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	/**
	 * @return the fechaNac
	 */
	public String getFechaNac() {
		return fechaNac;
	}

	/**
	 * @param fechaNac the fechaNac to set
	 */
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}

	/**
	 * @return the cveNacionalidad
	 */
	public String getCveNacionalidad() {
		return cveNacionalidad;
	}

	/**
	 * @param cveNacionalidad the cveNacionalidad to set
	 */
	public void setCveNacionalidad(String cveNacionalidad) {
		this.cveNacionalidad = cveNacionalidad;
	}

	/**
	 * @return the paisNacimiento
	 */
	public String getPaisNacimiento() {
		return paisNacimiento;
	}

	/**
	 * @param paisNacimiento the paisNacimiento to set
	 */
	public void setPaisNacimiento(String paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}

	/**
	 * @return the cveEstado
	 */
	public String getCveEstado() {
		return cveEstado;
	}

	/**
	 * @param cveEstado the cveEstado to set
	 */
	public void setCveEstado(String cveEstado) {
		this.cveEstado = cveEstado;
	}

	/**
	 * @return the entidadExtranjera
	 */
	public String getEntidadExtranjera() {
		return entidadExtranjera;
	}

	/**
	 * @param entidadExtranjera the entidadExtranjera to set
	 */
	public void setEntidadExtranjera(String entidadExtranjera) {
		this.entidadExtranjera = entidadExtranjera;
	}

	/**
	 * @return the edoCivil
	 */
	public String getEdoCivil() {
		return edoCivil;
	}

	/**
	 * @param edoCivil the edoCivil to set
	 */
	public void setEdoCivil(String edoCivil) {
		this.edoCivil = edoCivil;
	}

	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}

	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}

	/**
	 * @return the noExterior
	 */
	public String getNoExterior() {
		return noExterior;
	}

	/**
	 * @param noExterior the noExterior to set
	 */
	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}

	/**
	 * @return the noInterior
	 */
	public String getNoInterior() {
		return noInterior;
	}

	/**
	 * @param noInterior the noInterior to set
	 */
	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}

	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return colonia;
	}

	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	/**
	 * @return the codPostal
	 */
	public String getCodPostal() {
		return codPostal;
	}

	/**
	 * @param codPostal the codPostal to set
	 */
	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	/**
	 * @return the codPostalTrab
	 */
	public String getCodPostalTrab() {
		return codPostalTrab;
	}

	/**
	 * @param codPostalTrab the codPostalTrab to set
	 */
	public void setCodPostalTrab(String codPostalTrab) {
		this.codPostalTrab = codPostalTrab;
	}

	/**
	 * @return the lada
	 */
	public String getLada() {
		return lada;
	}

	/**
	 * @param lada the lada to set
	 */
	public void setLada(String lada) {
		this.lada = lada;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}



	/**
	 * @return the sucursalControl
	 */
	public String getSucursalControl() {
		return sucursalControl;
	}

	/**
	 * @param sucursalControl the sucursalControl to set
	 */
	public void setSucursalControl(String sucursalControl) {
		this.sucursalControl = sucursalControl;
	}

	/**
	 * @return the codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}

	/**
	 * @param codigoCliente the codigoCliente to set
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}



	/**
	 * @return the numTarjeta
	 */
	public String getNumTarjeta() {
		return numTarjeta;
	}

	/**
	 * @param numTarjeta the numTarjeta to set
	 */
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	/**
	 * @return the numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * @param numCuenta the numCuenta to set
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the remesa
	 */
	public String getRemesa() {
		return remesa;
	}

	/**
	 * @param remesa the remesa to set
	 */
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}

	/**
	 * @return the reactivacion
	 */
	public String getReactivacion() {
		return reactivacion;
	}

	/**
	 * @param reactivacion the reactivacion to set
	 */
	public void setReactivacion(String reactivacion) {
		this.reactivacion = reactivacion;
	}

	/**
	 * @return the enviarCatalogo
	 */
	public String getEnviarCatalogo() {
		return enviarCatalogo;
	}

	/**
	 * @param enviarCatalogo the enviarCatalogo to set
	 */
	public void setEnviarCatalogo(String enviarCatalogo) {
		this.enviarCatalogo = enviarCatalogo;
	}

	/**
	 * @return the usuarioSession
	 */
	public String getUsuarioSession() {
		return usuarioSession;
	}

	/**
	 * @param usuarioSession the usuarioSession to set
	 */
	public void setUsuarioSession(String usuarioSession) {
		this.usuarioSession = usuarioSession;
	}

	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * @return the tipoRegistro
	 */
	public String getTipoRegistro() {
		return tipoRegistro;
	}

	/**
	 * @param tipoRegistro the tipoRegistro to set
	 */
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	/**
	 * @return the cveEstadoNombre
	 */
	public String getCveEstadoNombre() {
		return cveEstadoNombre;
	}

	/**
	 * @param cveEstadoNombre the cveEstadoNombre to set
	 */
	public void setCveEstadoNombre(String cveEstadoNombre) {
		this.cveEstadoNombre = cveEstadoNombre;
	}

	/**
	 * @return the correoContrato
	 */
	public String getCorreoContrato() {
		return correoContrato;
	}

	/**
	 * @param correoContrato the correoContrato to set
	 */
	public void setCorreoContrato(String correoContrato) {
		this.correoContrato = correoContrato;
	}

	/**
	 * @return the coloniaExtranjero
	 */
	public String getColoniaExtranjero() {
		return coloniaExtranjero;
	}

	/**
	 * @param coloniaExtranjero the coloniaExtranjero to set
	 */
	public void setColoniaExtranjero(String coloniaExtranjero) {
		this.coloniaExtranjero = coloniaExtranjero;
	}

	/**
	 * @return the cveUsuario
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}

	/**
	 * @param cveUsuario the cveUsuario to set
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}




}