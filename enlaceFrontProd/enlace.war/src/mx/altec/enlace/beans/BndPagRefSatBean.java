package mx.altec.enlace.beans;

public class BndPagRefSatBean{

	/**
	 * Estado Bandera de Pago de Impuestos Provisional
	 */
	private String bndProvis;

	/**
	 * Estado Bandera de Pago de Impuestos del Ejercicio
	 */
	private String bndEjerci;

	/**
	 * Estado Bandera de Pago de Impuestos de Entidades Federativas
	 */
	private String bndEntFed;

	/**
	 * Obtiene Estado de la Bandera de Pago de Impuestos Provisional
	 * 
	 * @return String Estado de la Bandera de Pago de Impuestos Provisional
	 */
	public String getBndProvis() {
		return bndProvis;
	}

	/**
	 * Asigna Estado de la Bandera de Pago de Impuestos Provisional
	 * 
	 * @param bndProvis
	 *            String Estado de la Bandera de Pago de Impuestos Provisional
	 */
	public void setBndProvis(String bndProvis) {
		this.bndProvis = bndProvis;
	}

	/**
	 * Obtiene Estado de la Bandera de Pago de Impuestos del Ejercicio
	 * 
	 * @return String Estado de la Bandera de Pago de Impuestos del Ejercicio
	 */
	public String getBndEjerci() {
		return bndEjerci;
	}

	/**
	 * Asigna Estado de la Bandera de Pago de Impuestos del Ejercicio
	 * 
	 * @param bndEjerci
	 *            String Estado de la Bandera de Pago de Impuestos del Ejercicio
	 */
	public void setBndEjerci(String bndEjerci) {
		this.bndEjerci = bndEjerci;
	}

	/**
	 * Obtiene Estado de la Bandera de Pago de Impuestos de Entidades Federativas
	 * 
	 * @return String Estado de la Bandera de Pago de Impuestos de Entidades Federativas
	 */
	public String getBndEntFed() {
		return bndEntFed;
	}

	/**
	 * Asigna Estado de la Bandera de Pago de Impuestos de Entidades Federativas
	 * 
	 * @param bndEntFed
	 *            String Estado de la Bandera de Pago de Impuestos de Entidades Federativas
	 */
	public void setBndEntFed(String bndEntFed) {
		this.bndEntFed = bndEntFed;
	}

}
