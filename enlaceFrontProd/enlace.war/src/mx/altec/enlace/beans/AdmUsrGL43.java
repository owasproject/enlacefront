package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;

//TODO Paginacion
public class AdmUsrGL43 extends GLBase {

	public static final String HEADER = "GL4300811123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrGL43> BUILDER = new GL43Builder();

	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345"; 
	
	private static final String FMT_PAGINACION = "@DCGLM4302 P";  
	
	private static final String FTM_DETALLE = "@DCGLM4301 P";    
    		
	/////////////////////////////////////

	
	private List<GLContrato> contratos;
	
	private String contratoRepaginado = "";
	private String indRepaginado = "";
	
	public String getIndRepaginado() {
		return indRepaginado;
	}

	public void setIndRepaginado(String indRepaginado) {
		this.indRepaginado = indRepaginado;
	}

	private AdmUsrGL43() {		
	}		
	
	public List<GLContrato> getContratos() {
		return contratos;
	}	
	
	public String getContratoRepaginado() {
		return contratoRepaginado;
	}
	
	/////////////////////////////////////
	
	public static String tramaConsulta( String cveUsuario,String contrato,String indPag,String contraPag ) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(cveUsuario,  8));		 
		sb.append(rellenar(contrato,   20));
		sb.append(rellenar(indPag, 		1));
		sb.append(rellenar(contraPag,  20));
		
		return sb.toString();
	}
	
	/////////////////////////////////////
	
	public static class GL43Builder implements AdmUsrBuilder<AdmUsrGL43> {

		public AdmUsrGL43 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrGL43 gl43 = new AdmUsrGL43();

	        if (string.startsWith(INDICADOR_EXITO)) {	        	        	    
	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<GLContrato> contratos = new ArrayList<GLContrato>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		GLContrato contrato = new GLContrato();
	        		contrato.setContrato(string.substring(ind, (ind += 11)));	        			        			        		
	        		contratos.add(contrato);
	        	}      
	        	
	        	gl43.contratos = contratos;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		gl43.contratoRepaginado = string.substring(ind, (ind += 20));
	        		gl43.indRepaginado = "S";
	        	}	        	
	        		        	
	        } else {
	        	
	        	gl43.setCodMsg(getCodigoError(string));
	        	gl43.setMensaje(getMensajeError(string));
	        }

	        return gl43;        	
		}	
	}	
}
