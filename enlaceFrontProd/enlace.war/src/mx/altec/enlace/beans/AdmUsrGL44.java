package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;


import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import com.netscape.server.servlet.i18n.io.Serializable;

public class AdmUsrGL44 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL4400851123451O00N2";
	
	public static AdmUsrGL44Factory getFactoryInstance() {
		return FACTORY;
	}	

	
	private static final AdmUsrGL44Factory FACTORY = new AdmUsrGL44Factory();
	
	private static class AdmUsrGL44Factory implements AdmUsrBuilder<AdmUsrGL44> {

		public AdmUsrGL44 build(String arg) {
			
			AdmUsrGL44 bean = new AdmUsrGL44();
			
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GLA0001");
				bean.setCodExito(true);
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}			
			return bean;
		}
	}

}
