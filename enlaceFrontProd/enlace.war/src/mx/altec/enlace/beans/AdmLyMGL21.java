package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.GrupoOperacion;


import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmLyMGL21 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2100341123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM211  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM212  P";
	
	private static final AdmLyMGL21Factory FACTORY = new AdmLyMGL21Factory();
	
	private ArrayList<GrupoOperacion> grupos;
	
	public static AdmLyMGL21Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL21Factory implements AdmUsrBuilder<AdmLyMGL21> {

		public AdmLyMGL21 build(String arg) {
			
			AdmLyMGL21 bean = new AdmLyMGL21();
			ArrayList<GrupoOperacion> grpList = new ArrayList<GrupoOperacion>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL210000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							

				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						EIGlobal.mensajePorTrace("==> numRegs: [" + numRegs + "]", EIGlobal.NivelLog.INFO);
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									GrupoOperacion goBean = new GrupoOperacion();
									goBean.setClave(getValor(arg, index, 2));
									EIGlobal.mensajePorTrace("==> clave: [" + goBean.getClave() + "]", EIGlobal.NivelLog.INFO);
									goBean.setDescripcion(getValor(arg, index += 2, 40));
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
									index += 41;
									grpList.add(goBean);
								}
							}
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmLyMGL21 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setGrupos(grpList);
			return bean;
		}
		
	}

	public ArrayList<GrupoOperacion> getGrupos() {
		return grupos;
	}

	public void setGrupos(ArrayList<GrupoOperacion> grupos) {
		this.grupos = grupos;
	}



}
