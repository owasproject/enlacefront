package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean de datos para Nomina en Linea
 * @author Z712236 ferruzca
 * Modificaciones: Tactico Epyme  - Modificaciones Nomina En Linea
 *     @author : FSW - Vector
 *     Descricion: Se agregan variables al Bean para estatus y observaciones de la solicitud
 */
public class NominaLineaBean implements Serializable {

	/**
	 * Serial de version
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * tipoCargoArch Tipo Cargo seleccionado
	 */
	private String tipoCargoArch;
	/***** Datos de entrada ******/
	/**
	 * contrato del cliente
	 */
	private String contrato;
	/**
	 * razonSocial descripcion de contrato
	 */
	private String razonSocial;
	/**
	 * ctaCargo del cliente
	 */
	private String ctaCargo;
	/**
	 * fchIni inicio fecha de aplicacion
	 */
	private String fchIni;
	/**
	 * fchFin fin fecha aplicacion
	 */
	private String fchFin;
	/**
	 * secArchivo Folio del archivo
	 */
	private String secArchivo;
	/**
	 * fchAplicacion fecha de aplicacion
	 */
	private String fchAplicacion;
	/**
	 * numRegistros Numero de registros
	 */
	private String numRegistros;
	/**
	 * importeAplicado Importe que se aplica
	 */
	private String importeAplicado;
	/**
	 * estatus Estatus del archivo
	 */
	private String estatus;
	/**
	 * pagina Numero de pagina al consultar
	 */
	private Integer pagina;
	/**
	 * numRef numero de referencia
	 */
	private String numRef;
	/**
	 * numTransmision numero de transmision
	 */
	private String numTransmision;
	/**
	 * tipoFecha tipo de fecha recepcion aplicacion
	 */
	private String tipoFecha;
	/**
	 * nomArchivo nombre de archivo
	 */
	private String nomArchivo;
	/**
	 * fchCarga fecha carga
	 */
	private String fchCarga;
	/**
	 * importeRecibido importe recibido
	 */
	private String importeRecibido;

	/***** Datos de salida ******/
	/**
	 * lstConsulta Resultado de cunsulta
	 */
	/*VECTOR - Modificacion de Nomina Epyme - Estatus Solicitud*/
	/**
	 * Estatus de la solicitud
	 */
	private String estatusSol;
	/**
	 * Observaciones Solicitud
	 */
	private String observaciones; 
	/**
	 * Obtiene estatusSol
	 * @since 27/05/2015
	 * @return String estatus de la solicitud
	 */
	public String getEstatusSol() {
		return estatusSol;
	}
	/**
	 * Asigna Estatus
	 * @since 27/05/2015
	 * @param estatusSol valor a asignar
	 */
	public void setEstatusSol(String estatusSol) {
		this.estatusSol = estatusSol;
	}
	/**
	 * Obtiene observaciones
	 * @since 27/05/2015
	 * @return String observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/**
	 * Asigna observaciones
	 * @since 27/05/2015
	 * @param observaciones asignadas
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}



	private List<NominaLineaBean> lstConsulta=new ArrayList<NominaLineaBean>();
	/**
	 * paginacion Bean de paginacion
	 */
	private PaginacionBean paginacion=new PaginacionBean();
	/**
	 * servidorBiatux servidor biatux
	 */
	private String servidorBiatux;
	/***** geters and seters de entrada ******/
	/**
	 * Obtener contrato del cliente
	 * @return contrato devuelve cadena contrato
	 */
	public String getContrato() {
		return contrato;
	}
	/**
	 * Asignar valor contrato cliente
	 * @param contrato cadena contrato del cliente
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	/**
	 * Obtener cuenta cargo
	 * @return ctaCargo devuelve cadena cuenta cargo del cliente
	 */
	public String getCtaCargo() {
		return ctaCargo;
	}
	/**
	 * Asignar cuenta cargo
	 * @param ctaCargo cadena cuenta cargo del cliente
	 */
	public void setCtaCargo(String ctaCargo) {
		this.ctaCargo = ctaCargo;
	}
	/**
	 * Obtener fecha inicio
	 * @return fchIni devuelve cadena fecha inicio de aplicacion
	 */
	public String getFchIni() {
		return fchIni;
	}
	/**
	 * Asingar fecha inicio
	 * @param fchIni cadena fecha inicio de aplicacion
	 */
	public void setFchIni(String fchIni) {
		this.fchIni = fchIni;
	}
	/**
	 * Obtener fecha fin
	 * @return fchFin devuelve cadena fecha fin de aplicacion
	 */
	public String getFchFin() {
		return fchFin;
	}
	/**
	 * Asignar fecha fin
	 * @param fchFin cadena fecha fin de aplicacion
	 */
	public void setFchFin(String fchFin) {
		this.fchFin = fchFin;
	}
	/**
	 * Obtener secuencia de archivo
	 * @return secArchivo cadena folio del archivo
	 */
	public String getSecArchivo() {
		return secArchivo;
	}
	/**
	 * Asignar valor secuencia de archivo
	 * @param secArchivo cadena folio del archivo
	 */
	public void setSecArchivo(String secArchivo) {
		this.secArchivo = secArchivo;
	}
	/**
	 * Obtener valor fecha de aplicacion
	 * @return fchAplicacion cadena fecha de aplicacion
	 */
	public String getFchAplicacion() {
		return fchAplicacion;
	}
	/**
	 * Asignar valor fecha de aplicacion
	 * @param fchAplicacion devuelve cadena fecha de aplicacion
	 */
	public void setFchAplicacion(String fchAplicacion) {
		this.fchAplicacion = fchAplicacion;
	}
	/**
	 * Obtener numero de registros
	 * @return numRegistros devuelve cantidad de registros
	 */
	public String getNumRegistros() {
		return numRegistros;
	}
	/**
	 * Asignar cantidad de registros
	 * @param numRegistros asigna total de registros
	 */
	public void setNumRegistros(String numRegistros) {
		this.numRegistros = numRegistros;
	}
	/**
	 * Obtener valor de importe aplicado
	 * @return importeAplicado devuelve el total de importe aplicado
	 */
	public String getImporteAplicado() {
		return importeAplicado;
	}
	/**
	 * Asignar valor de importe aplicado
	 * @param importeAplicado asigna valor de importe aplicado
	 */
	public void setImporteAplicado(String importeAplicado) {
		this.importeAplicado = importeAplicado;
	}
	/**
	 * Obtener estatus del archivo
	 * @return estatus devuelve valor del estatus del archivo
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Asignar calor del estatus del archivo
	 * @param estatus asigna valor del estatus del archivo
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * Obtener pagina de la consulta
	 * @return pagina devuelve la pagina de la consulta
	 */
	public Integer getPagina() {
		return pagina;
	}
	/**
	 * Asignar valor de pagina
	 * @param pagina Asigna la pagina que se consulta
	 */
	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}
	
	/***** geters and seters de salida ******/
	/**
	 *Obtener lista de archivos de consulta
	 * @return lstConsulta devuelve los archivos encontrados en la consulta
	 */
	public List<NominaLineaBean> getLstConsulta() {
		return lstConsulta;
	}
	/**
	 * Asignar lista de archivos de la consulta
	 * @param lstConsulta Asigna los archivos encontrados en consulta
	 */
	public void setLstConsulta(List<NominaLineaBean> lstConsulta) {
		this.lstConsulta = lstConsulta;
	}
	/**
	 * Obtener bean de paginacion
	 * @return paginacion obtiene datos de la paginacion
	 */
	public PaginacionBean getPaginacion() {
		return paginacion;
	}
	/**
	 * Asignar bean de paginacion
	 * @param paginacion se asignan datos de la paginacion
	 */
	public void setPaginacion(PaginacionBean paginacion) {
		this.paginacion = paginacion;
	}
	/**
	 * Razon social
	 * @return razonSocial obtiene razon social
	 */
	public String getRazonSocial() {
		return razonSocial;
	}
	/**
	 * Razon social
	 * @param razonSocial setea razon social
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	/**
	 * Numero de referencia
	 * @return numRef numero de referencia
	 */
	public String getNumRef() {
		return numRef;
	}
	/**
	 * Numero de referencia
	 * @param numRef numero de referencia
	 */
	public void setNumRef(String numRef) {
		this.numRef = numRef;
	}
	/**
	 * Numero de transmision
	 * @return numTransmision numero de transmision
	 */
	public String getNumTransmision() {
		return numTransmision;
	}
	/**
	 * Numero de transmision
	 * @param numTransmision Numero de transmision 
	 */
	public void setNumTransmision(String numTransmision) {
		this.numTransmision = numTransmision;
	}
	/**
	 * getTipoFecha tipo de fecha
	 * @return tipoFecha tipo de fecha
	 */
	public String getTipoFecha() {
		return tipoFecha;
	}
	/**
	 * setTipoFecha tipo de fecha
	 * @param tipoFecha tipoFecha
	 */
	public void setTipoFecha(String tipoFecha) {
		this.tipoFecha = tipoFecha;
	}
	/**
	 * getNomArchivo nombre de archivo
	 * @return nomArchivo nomArchivo
	 */
	public String getNomArchivo() {
		return nomArchivo;
	}
	/**
	 * setNomArchivo poner nombre de archivo
	 * @param nomArchivo nombre de archivo
	 */
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	/**
	 * getFchCarga obtener fecha carga
	 * @return fchCarga fecha carga
	 */
	public String getFchCarga() {
		return fchCarga;
	}
	/**
	 * setFchCarga setear fecha carga
	 * @param fchCarga fecha carga
	 */
	public void setFchCarga(String fchCarga) {
		this.fchCarga = fchCarga;
	}
	/**
	 * getImporteRecibido obtener importe recibido
	 * @return importeRecibido importe recibido
	 */
	public String getImporteRecibido() {
		return importeRecibido;
	}
	/**
	 * setImporteRecibido setear importe recibido
	 * @param importeRecibido importe recibido
	 */
	public void setImporteRecibido(String importeRecibido) {
		this.importeRecibido = importeRecibido;
	}
	/**
	 * getServidorBiatux obtiene servidor biatux
	 * @return servidor biatux
	 */
	public String getServidorBiatux() {
		return servidorBiatux;
	}
	/**
	 * recibe valor servidor biatux
	 * @param servidorBiatux buatux
	 */
	public void setServidorBiatux(String servidorBiatux) {
		this.servidorBiatux = servidorBiatux;
	}
	/**
	 * getTipoCargoArch obtiene tipo de cargo
	 * @return tipoCargoArch
	 */
	public String getTipoCargoArch() {
		return tipoCargoArch;
	}
	/**
	 * recibe valor tipo cargo
	 * @param tipoCargoArch tipo cargo
	 */
	public void setTipoCargoArch(String tipoCargoArch) {
		this.tipoCargoArch = tipoCargoArch;
	}
}