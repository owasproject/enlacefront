package mx.altec.enlace.beans;
/**
 * 
 * @author Z096114 Emmanuel Sanchez Castillo
 * @version 1.0
 * @Fecha 12 Noviembre 2010
 * @proyecto 201001500 - EBEE (Estrategia de banca electronica Enlace
 *
 */
import java.util.ArrayList;
import java.util.List;

public class GL51Tran extends GLBase {
	
	String contrato;
	String usuario;
	String nivel;
	String agrupacion;
	String indicadorPag;
	String facultadPag;
	String usuarioOper;
	
	ArrayList<GLFacultad> facultades;
	
	public String getAgrupacion() {
		return agrupacion;
	}
	public void setAgrupacion(String agrupacion) {
		this.agrupacion = agrupacion;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getFacultadPag() {
		return facultadPag;
	}
	public void setFacultadPag(String facultadPag) {
		this.facultadPag = facultadPag;
	}
	public String getIndicadorPag() {
		return indicadorPag;
	}
	public void setIndicadorPag(String indicadorPag) {
		this.indicadorPag = indicadorPag;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getUsuarioOper() {
		return usuarioOper;
	}
	public void setUsuarioOper(String usuarioOper) {
		this.usuarioOper = usuarioOper;
	}
	public ArrayList<GLFacultad> getFacultades() {
		return facultades;
	}
	public void setFacultades(ArrayList<GLFacultad> facultades) {
		this.facultades = facultades;
	}

}
