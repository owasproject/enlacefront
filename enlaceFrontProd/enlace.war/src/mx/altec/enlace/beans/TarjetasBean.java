package mx.altec.enlace.beans;

import java.io.Serializable;

public class TarjetasBean implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * N&uacute;mero PAN de tarjeta
	 */
	private String tarjeta;
	/**
	 * Condici&oacute;n estampa de remesa
	 */
	private String condEstampa;
	/**
	 * Estado de tarjeta
	 */
	private String estadoTarjeta;
	/**
	 * Fecha de caducidad
	 */
	private String fechaCaducidad;
	/**
	 * Fecha de asignacion
	 */
	private String fechaAsignacion;
	/**
	 * Fecha ultima modificacion
	 */
	private String fechaBaja;
	/**
	 * Fecha ultima modificacion
	 */
	private String fechaUltimaMod;

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getCondEstampa() {
		return condEstampa;
	}

	public void setCondEstampa(String condEstampa) {
		this.condEstampa = condEstampa;
	}

	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	public void setEstadoTarjeta(String estadoTarjeta) {
		if (estadoTarjeta.equals("E")) {
			this.estadoTarjeta = "ENVIADA";
		} else if (estadoTarjeta.equals("A")) {
			this.estadoTarjeta = "ASIGNADA";
		} else if (estadoTarjeta.equals("D")) {
			this.estadoTarjeta = "CANCELADA";
		} else if (estadoTarjeta.equals("N")) {
			this.estadoTarjeta = "NUEVA";
		} else if (estadoTarjeta.equals("R")) {
			this.estadoTarjeta = "RECIBIDA";
		} else {
			this.estadoTarjeta = "NO DEFINIDO";
		}
	}

	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getFechaUltimaMod() {
		return fechaUltimaMod;
	}

	public void setFechaUltimaMod(String fechaUltimaMod) {
		this.fechaUltimaMod = fechaUltimaMod;
	}

	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getFechaBaja() {
		return fechaBaja;
	}
}