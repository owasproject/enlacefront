package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;



import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmUsrGL31 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL3100781123451O00N2";
					
	private static final String FORMATO_HEADER = "@DCGLM311  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM312  P";
	private static final String FORMATO_HEADER_PAG = "@DCGLM313  P";
	
	private static final AdmUsrGL31Factory FACTORY = new AdmUsrGL31Factory();
		
	private ArrayList<Facultad> facultades;
	
	public static AdmUsrGL31Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmUsrGL31Factory implements AdmUsrBuilder<AdmUsrGL31> {

		public AdmUsrGL31 build(String arg) {
			
			AdmUsrGL31 bean = new AdmUsrGL31();
			ArrayList<Facultad> facList = new ArrayList<Facultad>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL310000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							
				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									Facultad facBean = new Facultad();
									facBean.setCveFacultad(getValor(arg, index, 16));
									EIGlobal.mensajePorTrace("==> Facultad: [" + facBean.getCveFacultad() + "]", EIGlobal.NivelLog.ERROR);
									facBean.setDescripcion(getValor(arg, index += 16, 80));
									EIGlobal.mensajePorTrace("==> Desc: [" + facBean.getDescripcion() + "]", EIGlobal.NivelLog.ERROR);
									String nombreComercial = getValor(arg, index+= 80 , 80);
									EIGlobal.mensajePorTrace("==> nombreComercial: [" + nombreComercial + "]", EIGlobal.NivelLog.ERROR);
									String estado = getValor(arg, index += 80, 2);
									EIGlobal.mensajePorTrace("==> estado: [" + estado + "]", EIGlobal.NivelLog.ERROR);
									String descEstado = getValor(arg, index += 2, 10);
									EIGlobal.mensajePorTrace("==> des estado: [" + descEstado + "]", EIGlobal.NivelLog.ERROR);
									String uso = getValor(arg, index += 10, 2);
									EIGlobal.mensajePorTrace("==> uso: [" + uso + "]", EIGlobal.NivelLog.ERROR);
									String factSU = getValor(arg, index += 2, 2);
									EIGlobal.mensajePorTrace("==> factSU: [" + factSU + "]", EIGlobal.NivelLog.ERROR);
									String nivel = getValor(arg, index += 2, 1);
									EIGlobal.mensajePorTrace("==> nivel: [" + nivel + "]", EIGlobal.NivelLog.ERROR);
									String grupo = getValor(arg, index += 1, 3);
									EIGlobal.mensajePorTrace("==> grupo: [" + grupo + "]", EIGlobal.NivelLog.ERROR);
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
									index += 4;
									facList.add(facBean);
								}
							}
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmUsrGL31 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setFacultades(facList);
			return bean;
		}
		
	}

	public ArrayList<Facultad> getFacultades() {
		return facultades;
	}

	public void setFacultades(ArrayList<Facultad> facultades) {
		this.facultades = facultades;
	}


}
