package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 * @author ESC Bean referente a la transacci&oacute;n LMXC CONSULTA REMESAS
 */
public class LMXC extends AdmUsrGL35Base implements Serializable {

	/**
	 * Header de entrada para la transaccion LMXC
	 */
	public static String HEADER = "LMXC01171123451O00N2";
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "@DCLMMCXC2 P";

	/**
	 * Identificador de detalle
	 */
	private static final String FORMATO_DETALLE = "@DCLMMCXC2 P";

	private static final String FORMATO_PAGINACION = "@DCLMMCXC1 P";

	private static final String EXISTEN_MAS_DATOS = "EXISTEN MAS REGISTROS A CONSULTAR";

	/**
	 * Identificador de fin de datos
	 */
	private static final String FIN_DATOS = "FIN DATOS";

	private static final String CONSULTA_EFECTUADA = "CONSULTA EFECTUADA";

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidad de las remesas
	 */
	private String entidad = "0014";
	/**
	 * Contrato enlace de las remesas
	 */
	private String contratoEnlace;
	/**
	 * Contrato distribuci&oacute;n de las remesas
	 */
	private String ctroDistribucion;
	/**
	 * N&uacute;mero de las remesas
	 */
	private String numeroRemesa;

	private String numeroRemesa2;
	public String getNumeroRemesa2() {
		return numeroRemesa2;
	}

	public void setNumeroRemesa2(String numeroRemesa2) {
		this.numeroRemesa2 = numeroRemesa2;
	}

	/**
	 * Estado de las remesas
	 */
	private String estadoRemesa;
	/**
	 * Fecha inicio de las remesas
	 */
	private String fechaInicio;
	/**
	 * Fecha Fin de las remesas
	 */
	private String fechaFinal;
	/**
	 * Fecha Fin de las remesas
	 */
	private String nombre;

	private String tipoFecha;

	public String getTipoFecha() {
		return tipoFecha;
	}

	public void setTipoFecha(String tipoFecha) {
		this.tipoFecha = tipoFecha;
	}

	/**
	 * Lista de remesas
	 */
	private ArrayList<RemesasBean> listaRemesas;
	/**
	 * Indicador si la transaccion es exitosa
	 */
	private boolean codExito;

	/**
	 * Cadena con el codigo de la operacion
	 */
	private String codigoOperacion;
	/**
	 * Cadena con el mensaje de error de la operacion
	 */
	private String mensError;
	/**
	 * Indicador si la transaccion es exitosa
	 *
	 */
	private boolean finDatos;

	private boolean consultaEfectuada;

	private boolean existenMasRegistros;

	private String numRegistros;

	private String numRegistrosAnt;

	public String getNumRegistros() {
		return numRegistros;
	}

	public void setNumRegistros(String numRegistros) {
		this.numRegistros = numRegistros;
	}

	public String getNumRegistrosAnt() {
		return numRegistrosAnt;
	}

	public void setNumRegistrosAnt(String numRegistrosAnt) {
		this.numRegistrosAnt = numRegistrosAnt;
	}

	public boolean isExistenMasRegistros() {
		return existenMasRegistros;
	}

	public void setExistenMasRegistros(boolean existenMasRegistros) {
		this.existenMasRegistros = existenMasRegistros;
	}

	public boolean isConsultaEfectuada() {
		return consultaEfectuada;
	}

	public void setConsultaEfectuada(boolean consultaEfectuada) {
		this.consultaEfectuada = consultaEfectuada;
	}

	/**
	 * Contrato enlace de las remesas
	 */
	private String ctoEnlacePag;
	/**
	 * Contrato distribuci&oacute;n de las remesas
	 */
	private String ctroDistribucionPag;
	/**
	 * N&uacute;mero de las remesas
	 */
	private String numRemesaPag;

	/**
	 * Obtiene el indicador de exito de la operacion
	 *
	 * @return Boolean
	 */
	public boolean isCodExito() {
		return codExito;
	}

	/**
	 * Asigna el indicador de exito de la operacion
	 *
	 * @param codExito
	 *            Boolean
	 */
	public void setCodExito(boolean codExito) {
		this.codExito = codExito;
	}

	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	public String getMensError() {
		return mensError;
	}

	public void setMensError(String mensajeError) {
		this.mensError = mensajeError;
	}

	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPreLMXCFactory FACTORY = new NomPreLMXCFactory();

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setContratoEnlace(String contratoEnlace) {
		this.contratoEnlace = contratoEnlace;
	}

	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public String getCtroDistribucion() {
		return ctroDistribucion;
	}

	public void setCtroDistribucion(String ctroDistribucion) {
		this.ctroDistribucion = ctroDistribucion;
	}

	public String getNumeroRemesa() {
		return numeroRemesa;
	}

	public void setNumeroRemesa(String numeroRemesa) {
		this.numeroRemesa = numeroRemesa;
	}

	public String getEstadoRemesa() {
		return estadoRemesa;
	}

	public void setEstadoRemesa(String estadoRemesa) {
		this.estadoRemesa = estadoRemesa;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public static NomPreLMXCFactory getFactoryInstance() {
		return FACTORY;
	}

	public void setListaRemesas(ArrayList<RemesasBean> listaRemesas) {
		this.listaRemesas = listaRemesas;
	}

	public ArrayList<RemesasBean> getListaRemesas() {
		return listaRemesas;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setFinDatos(boolean finDatos) {
		this.finDatos = finDatos;
	}

	public boolean isFinDatos() {
		return finDatos;
	}

	public void setCtoEnlacePag(String ctoEnlacePag) {
		this.ctoEnlacePag = ctoEnlacePag;
	}

	public String getCtoEnlacePag() {
		return ctoEnlacePag;
	}

	public void setCtroDistribucionPag(String ctroDistribucionPag) {
		this.ctroDistribucionPag = ctroDistribucionPag;
	}

	public String getCtroDistribucionPag() {
		return ctroDistribucionPag;
	}

	public void setNumRemesaPag(String numRemesaPag) {
		this.numRemesaPag = numRemesaPag;
	}

	public String getNumRemesaPag() {
		return numRemesaPag;
	}

	/**
	 * Clase usada para generar un elemento LMXC a partir de la cadena de salida
	 * de la transaccion
	 *
	 * @author ESC
	 */
	private static class NomPreLMXCFactory implements AdmUsrBuilder<LMXC> {

		/**
		 * Componente encargado de construir el objeto LMXC a partir de una
		 * cadena de entrada
		 *
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg
		 *            Cadena de entrada
		 */
		public LMXC build(String arg) {

			LMXC bean = new LMXC();
			bean.setFinDatos(false);
			ArrayList<RemesasBean> listaRemesas = new ArrayList<RemesasBean>();
			if (isCodigoExito(arg)) {

				bean.setCodExito(true);
				bean.setCodigoOperacion("LMXC0000");
				int index = arg.indexOf(FORMATO_HEADER);

				if (index != -1) {

					index += FORMATO_HEADER.length();
				}

				if (arg.indexOf(EXISTEN_MAS_DATOS) != -1) {
					bean.setExistenMasRegistros(true);
				} else {
					bean.setExistenMasRegistros(false);
				}

				if (arg.indexOf(FIN_DATOS) != -1) {
					bean.setFinDatos(true);
				}

				if (arg.indexOf(CONSULTA_EFECTUADA) != -1) {
					bean.setConsultaEfectuada(true);
				} else {
					bean.setConsultaEfectuada(false);
				}

				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 112);

				if (detalles != null && detalles.length > 0) {

					for (String detalle : detalles) {

						if (detalle.length() == 0) {
							continue;
						}
						// EIGlobal.mensajePorTrace("String detalle en la LMXC##########"+detalle,
						// EIGlobal.NivelLog.INFO);
						RemesasBean remesa = new RemesasBean();
						remesa.setFechaBajaRem(getValor(detalle, 0, 4));
						remesa.setFechaBajaRem(getValor(detalle, 4, 12));
						remesa.setCentroDistribucion(getValor(detalle, 16, 6));// CENTRO
						// DISTRIBUCION
						remesa.setNumRemesa(getValor(detalle, 22, 12)); // NUM-REMESA
						remesa.setEstadoRemesa(getValor(detalle, 34, 1)); // ESTADO-REMESA
						remesa.setCondEstampa(getValor(detalle, 35, 3)); // COND-ESTAMPA
						remesa.setDescCondEst(getValor(detalle, 38, 35)); // DESC-CONDICION
						remesa.setNumTarjetas(getValor(detalle, 73, 9)); // NUM-TARJETAS
						remesa.setFechaAltaRem(getValor(detalle, 82, 10)); // FECHA-ALTA
						remesa.setFechaRecepRem(getValor(detalle, 92, 10)); // FECHA-RECEPCION
						remesa.setFechaBajaRem(getValor(detalle, 102, 10)); // FECHA-BAJA
						listaRemesas.add(remesa);
					}
					bean.setListaRemesas(listaRemesas);
					if(arg.indexOf(EXISTEN_MAS_DATOS) != -1 && arg.indexOf(FORMATO_PAGINACION) != -1){
						String paginacion = arg.substring(arg.indexOf(FORMATO_PAGINACION));
						EIGlobal.mensajePorTrace("LMXC::NomPreLMXCFactory::build:: Paginacion:"+paginacion,EIGlobal.NivelLog.DEBUG);
						bean.setEntidad(getValor(paginacion, 12, 4));
						bean.setContratoEnlace(getValor(paginacion, 16, 12));
						bean.setCtroDistribucion(getValor(paginacion, 28, 6));
						bean.setNumeroRemesa(getValor(paginacion, 34, 12));
						bean.setEstadoRemesa(getValor(paginacion, 46, 1));
						bean.setFechaInicio(getValor(paginacion, 47, 10));
						bean.setFechaFinal(getValor(paginacion, 57, 10));
						bean.setCtoEnlacePag(getValor(paginacion, 67, 12));
					}
				}

			} else if (isCodigoError(arg)) {
				bean.setCodExito(false);
				bean.setCodigoOperacion(getCodigoError(arg));
			}

			return bean;
		}
	}
}