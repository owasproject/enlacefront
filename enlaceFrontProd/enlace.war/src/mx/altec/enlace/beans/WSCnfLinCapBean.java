package mx.altec.enlace.beans;

public class WSCnfLinCapBean{

	/**
	 * Bandera de Validaci�n de L�nea de Captura
	 */
	private Boolean bndLinCap = false;
	/**
	 * Variable que indica si es una cuenta del Cat�logo de L�nea de Captura
	 */
	private Boolean esCtaLinCap = false;
	/**
	 * TimeOut del Web Service de validaci�n de la L�nea de Captura
	 */
	private Long tmoWSLinCap = Long.valueOf(1000);
	/**
	 * URL del Web Service de validaci�n de la L�nea de Captura
	 */
	private String urlWSLinCap = "";

	/**
	 * Asigna el valor de la Bandera de Validaci�n de la L�nea de Captura
	 * 
	 * @param bndLinCap Boolean Campo Libre1 de la respuesta
	 */
	public void setBndLinCap(Boolean bndLinCap) {
		this.bndLinCap = bndLinCap;
	}

	/**
	 * Obtiene el valor de la Bandera de Validaci�n de la L�nea de Captura
	 * 
	 * @return Boolean valor de la Bandera de Validaci�n de la L�nea de Captura
	 */
	public Boolean getBndLinCap() {
		return bndLinCap;
	}

	/**
	 * Almacena el valor verdadero o falso dependiendo de si la cuenta existe en el cat�logo de Validaci�n de la L�nea de Captura
	 * 
	 * @param esCtaLinCap Boolean valor verdadero o falso dependiendo de si la cuenta existe en el cat�logo de Validaci�n de la L�nea de Captura
	 */
	public void setEsCtaLinCap(Boolean esCtaLinCap) {
		this.esCtaLinCap = esCtaLinCap;
	}

	/**
	 * Obtiene el valor de la Bandera de Validaci�n de la L�nea de Captura
	 * 
	 * @return Boolean valor verdadero o falso dependiendo de si la cuenta existe en el cat�logo de Validaci�n de la L�nea de Captura
	 */
	public Boolean getEsCtaLinCap() {
		return esCtaLinCap;
	}

	/**
	 * Asigna el valor del TimeOut para el Web Service de Validaci�n de la L�nea de Captura
	 * 
	 * @param tmoWSLinCap int valor del TimeOut para el Web Service de Validaci�n de la L�nea de Captura
	 */
	public void setTmoWSLinCap(Long tmoWSLinCap) {
		this.tmoWSLinCap = tmoWSLinCap;
	}

	/**
	 * Obtiene el valor del TimeOut para el Web Service de Validaci�n de la L�nea de Captura
	 * 
	 * @return int valor del TimeOut para el Web Service de Validaci�n de la L�nea de Captura
	 */
	public Long getTmoWSLinCap() {
		return tmoWSLinCap;
	}

	/**
	 * Asigna el valor de la URL del Web Service de Validaci�n de la L�nea de Captura
	 * 
	 * @param urlWSLinCap String valor de la URL del Web Service de Validaci�n de la L�nea de Captura
	 */
	public void setUrlWSLinCap(String urlWSLinCap) {
		this.urlWSLinCap = urlWSLinCap;
	}

	/**
	 * Obtiene el valor de la URL del Web Service de Validaci�n de la L�nea de Captura
	 * 
	 * @return String valor de la URL del Web Service de Validaci�n de la L�nea de Captura
	 */
	public String getUrlWSLinCap() {
		return urlWSLinCap;
	}

}
