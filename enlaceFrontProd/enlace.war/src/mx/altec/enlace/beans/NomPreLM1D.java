package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.beans.NomPreEmpleado.*;
import static mx.altec.enlace.beans.NomPreTarjeta.*;

public class NomPreLM1D extends NomPreLM1Base implements Serializable{

	private static final long serialVersionUID = 2917572976150884580L;

	public static String HEADER = "LM1D01691123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCLMLTAR1 P";
	
	private static final String FORMATO_PAGINADO = "@DCLMLTAR2 P";
	
	private static final NomPreLM1DFactory FACTORY = new NomPreLM1DFactory();
	
	private String noEmpleadoPag;
	
	private List<Relacion> detalle;
	
	public String getNoEmpleadoPag() {
		return noEmpleadoPag;
	}
	
	public void setNoEmpleadoPag(String noEmpleadoPag) {
		this.noEmpleadoPag = noEmpleadoPag;
	}
	
	public List<Relacion> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<Relacion> detalle) {
		this.detalle = detalle;
	}		
	
	public static NomPreLM1DFactory getFactoryInstance() {
		return FACTORY;
	}
	
	private static class NomPreLM1DFactory implements NomPreBuilder<NomPreLM1D> {
			
		public NomPreLM1D build(String arg) {
		
			NomPreLM1D bean = new NomPreLM1D();;
			
			if (isCodigoExito(arg)) {															
				bean.setCodigoOperacion("LM1D0000");
				bean.setCodExito(true);
				
				int indexPag = arg.indexOf(FORMATO_PAGINADO);
				
				if (indexPag != -1) {
					
					indexPag += FORMATO_PAGINADO.length();
					
					bean.setNoEmpleadoPag(getValor(arg, indexPag + 130, 7)); //NUM-REMESA-PAG
				}
				
				String[] detalles = getDetalles(arg, FORMATO_HEADER, 140);
	
				if (detalles != null && detalles.length > 0) {					
					
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new ArrayList<Relacion>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}					
						
						int index = 0;
											
						NomPreEmpleado empleado = 
							new NomPreEmpleado(
								getValor(detalle, index, LNG_NO_EMPLEADO),			//NUM-EMPLEADO-S
								getValor(detalle, index += 7, LNG_NOMBRE), 			//NOM-EMPLEADO
								getValor(detalle, index += 30, LNG_PATERNO), 		//APE-PATERNO
								getValor(detalle, index += 30, LNG_MATERNO));		//APE-MATERNO						
						
						NomPreTarjeta tarjeta =
								new NomPreTarjeta(
										getValor(detalle, index +=20 , LNG_NO_TARJETA), //NUM-TARJETA-S
										null,
										getValor(detalle, index += 22, LNG_COD_ESTADO),	//COD-EST-TARJ-S
										getValor(detalle, index += 1, LNG_DESCRIPCION));//DES-EST-TAR-S												
	
						bean.getDetalle().add(new Relacion(tarjeta, empleado));
					}
				}
				
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}	
	
			return bean;
		}
	}
	public static class Relacion implements Serializable {

		private static final long serialVersionUID = -9220444918215172380L;
		
		private NomPreTarjeta tarjeta;
		private NomPreEmpleado empleado;
		
		public Relacion(NomPreTarjeta tarjeta, NomPreEmpleado empleado) {
			this.tarjeta = tarjeta;
			this.empleado = empleado;
		}
		public NomPreTarjeta getTarjeta() {			
			return tarjeta;
		}

		public NomPreEmpleado getEmpleado() {			
			return empleado;
		}	
	}
}
