package mx.altec.enlace.beans;

import java.util.ArrayList;

public class NomPreAlta 
{
	private ArrayList listaRegistros;
	private ArrayList listaErrores;
	private int totalRegistros;
	private String numContrato;
	private String descContrato;
	private String numTarjeta;
	private String apellidoM;
	private String apellidoP;
	private String nombre;
	private String rfc;	
	private String numEmpl;
	private String homoclave;
	private String fechNacimiento;
	private String edoCivil;
	private String nacionalidad;
	private String sexo;
	private String delegacion;
	private String colonia;
	private String calle;
	private int numero;
	private String codigoPostal;
	private String cveEstado;
	private String ciudad;
	private int telPart;
	private int prefTelPart;
	private boolean existenReg = true;
	private boolean falloInesperado = false;
	/**
	 * @return el apellidoM
	 */
	public String getApellidoM() {
		return apellidoM;
	}
	/**
	 * @param apellidoM el apellidoM a establecer
	 */
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	/**
	 * @return el apellidoP
	 */
	public String getApellidoP() {
		return apellidoP;
	}
	/**
	 * @param apellidoP el apellidoP a establecer
	 */
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	/**
	 * @return el calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * @param calle el calle a establecer
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * @return el ciudad
	 */
	public String getCiudad() {
		return ciudad;
	}
	/**
	 * @param ciudad el ciudad a establecer
	 */
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	/**
	 * @return el codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 * @param codigoPostal el codigoPostal a establecer
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * @return el colonia
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * @param colonia el colonia a establecer
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	/**
	 * @return el cveEstado
	 */
	public String getCveEstado() {
		return cveEstado;
	}
	/**
	 * @param cveEstado el cveEstado a establecer
	 */
	public void setCveEstado(String cveEstado) {
		this.cveEstado = cveEstado;
	}
	/**
	 * @return el delegacion
	 */
	public String getDelegacion() {
		return delegacion;
	}
	/**
	 * @param delegacion el delegacion a establecer
	 */
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}
	/**
	 * @return el descContrato
	 */
	public String getDescContrato() {
		return descContrato;
	}
	/**
	 * @param descContrato el descContrato a establecer
	 */
	public void setDescContrato(String descContrato) {
		this.descContrato = descContrato;
	}
	/**
	 * @return el edoCivil
	 */
	public String getEdoCivil() {
		return edoCivil;
	}
	/**
	 * @param edoCivil el edoCivil a establecer
	 */
	public void setEdoCivil(String edoCivil) {
		this.edoCivil = edoCivil;
	}
	/**
	 * @return el existenReg
	 */
	public boolean isExistenReg() {
		return existenReg;
	}
	/**
	 * @param existenReg el existenReg a establecer
	 */
	public void setExistenReg(boolean existenReg) {
		this.existenReg = existenReg;
	}
	/**
	 * @return el falloInesperado
	 */
	public boolean isFalloInesperado() {
		return falloInesperado;
	}
	/**
	 * @param falloInesperado el falloInesperado a establecer
	 */
	public void setFalloInesperado(boolean falloInesperado) {
		this.falloInesperado = falloInesperado;
	}
	/**
	 * @return el fechNacimiento
	 */
	public String getFechNacimiento() {
		return fechNacimiento;
	}
	/**
	 * @param fechNacimiento el fechNacimiento a establecer
	 */
	public void setFechNacimiento(String fechNacimiento) {
		this.fechNacimiento = fechNacimiento;
	}
	/**
	 * @return el homoclave
	 */
	public String getHomoclave() {
		return homoclave;
	}
	/**
	 * @param homoclave el homoclave a establecer
	 */
	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}
	/**
	 * @return el listaErrores
	 */
	public ArrayList getListaErrores() {
		return listaErrores;
	}
	/**
	 * @param listaErrores el listaErrores a establecer
	 */
	public void setListaErrores(ArrayList listaErrores) {
		this.listaErrores = listaErrores;
	}
	/**
	 * @return el listaRegistros
	 */
	public ArrayList getListaRegistros() {
		return listaRegistros;
	}
	/**
	 * @param listaRegistros el listaRegistros a establecer
	 */
	public void setListaRegistros(ArrayList listaRegistros) {
		this.listaRegistros = listaRegistros;
	}
	/**
	 * @return el nacionalidad
	 */
	public String getNacionalidad() {
		return nacionalidad;
	}
	/**
	 * @param nacionalidad el nacionalidad a establecer
	 */
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return el numContrato
	 */
	public String getNumContrato() {
		return numContrato;
	}
	/**
	 * @param numContrato el numContrato a establecer
	 */
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}
	/**
	 * @return el numEmpl
	 */
	public String getNumEmpl() {
		return numEmpl;
	}
	/**
	 * @param numEmpl el numEmpl a establecer
	 */
	public void setNumEmpl(String numEmpl) {
		this.numEmpl = numEmpl;
	}
	/**
	 * @return el numero
	 */
	public int getNumero() {
		return numero;
	}
	/**
	 * @param numero el numero a establecer
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}
	/**
	 * @return el numTarjeta
	 */
	public String getNumTarjeta() {
		return numTarjeta;
	}
	/**
	 * @param numTarjeta el numTarjeta a establecer
	 */
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	/**
	 * @return el prefTelPart
	 */
	public int getPrefTelPart() {
		return prefTelPart;
	}
	/**
	 * @param prefTelPart el prefTelPart a establecer
	 */
	public void setPrefTelPart(int prefTelPart) {
		this.prefTelPart = prefTelPart;
	}
	/**
	 * @return el rfc
	 */
	public String getRfc() {
		return rfc;
	}
	/**
	 * @param rfc el rfc a establecer
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/**
	 * @return el sexo
	 */
	public String getSexo() {
		return sexo;
	}
	/**
	 * @param sexo el sexo a establecer
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	/**
	 * @return el telPart
	 */
	public int getTelPart() {
		return telPart;
	}
	/**
	 * @param telPart el telPart a establecer
	 */
	public void setTelPart(int telPart) {
		this.telPart = telPart;
	}
	/**
	 * @return el totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}
	/**
	 * @param totalRegistros el totalRegistros a establecer
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	
}
