package mx.altec.enlace.beans;

import java.io.Serializable;

public class ConsultaAsignacionTEResBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 *
	 */

	private String folio;
	private String fecha;
	private String remesa;
	private String buc;
	private String bucAsterisc;
	private String numEmpleado;
	private String nombre;
	private String tarjeta;
	private String cuenta;
	private String estatus;
	private String usuario;
	private String usuarioAsterisc;

	public String getUsuarioAsterisc() {
		return usuarioAsterisc;
	}
	public void setUsuarioAsterisc(String usuarioAsterisc) {
		this.usuarioAsterisc = usuarioAsterisc;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getBucAsterisc() {
		return bucAsterisc;
	}
	public void setBucAsterisc(String bucAsterisc) {
		this.bucAsterisc = bucAsterisc;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getRemesa() {
		return remesa;
	}
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}
	public String getBuc() {
		return buc;
	}
	public void setBuc(String buc) {
		this.buc = buc;
	}
	public String getNumEmpleado() {
		return numEmpleado;
	}
	public void setNumEmpleado(String numEmpleado) {
		this.numEmpleado = numEmpleado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}