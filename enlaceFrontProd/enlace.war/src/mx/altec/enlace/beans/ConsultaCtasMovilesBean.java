package mx.altec.enlace.beans;

public class ConsultaCtasMovilesBean {
	
	/**
	 * variable titular
	 */
	private String titular;
	
	/**
	 * variable numeroMovil
	 */
	private String numeroMovil;
	
	/**
	 * variable tipoCuenta
	 */
	private String tipoCuenta;

	/**
	 * variable cveBanco
	 */
	private String cveBanco;
	
	/**
	 * variable descBanco
	 */
	private String descBanco;
        
     /**
	 * variable descripcionError
	 */
	private String descErr;
	
	/**
	 * variable contrato
	 */
	private String contrato;
	
	/**
	 * variable regIni
	 */
	private int regIni;
	
	/**
	 * variable regFin
	 */
	private int regFin;
	
	/**
	 * variable criterio
	 */
	private String criterio;
	
	/**
	 * variable textoBuscar
	 */
	private String textoBuscar;

	/**
	 * obtiene titular
	 * @return titular : titular
	 */	
	public String getTitular() {
		return titular;
	}

	/**
	 * asigna titular
	 * @param titular : titular
	 */
	public void setTitular(String titular) {
		this.titular = titular;
	}
	/**
	 * obtiene numeroMovil
	 * @return numeroMovil : numeroMovil
	 */
	public String getNumeroMovil() {
		return numeroMovil;
	}

	/**
	 * asigna numeroMovil
	 * @param numeroMovil : numeroMovil
	 */
	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}

	/**
	 * obtiene tipoCuenta
	 * @return tipoCuenta : tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * asigna tipoCuenta
	 * @param tipoCuenta : tipoCuenta
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * obtiene cveBanco
	 * @return cveBanco : cveBanco
	 */
	public String getCveBanco() {
		return cveBanco;
	}

	/**
	 * asigna cveBanco
	 * @param cveBanco : cveBanco
	 */
	public void setCveBanco(String cveBanco) {
		this.cveBanco = cveBanco;
	}

	/**
	 * obtiene descBanco
	 * @return descBanco : descBanco
	 */
	public String getDescBanco() {
		return descBanco;
	}

	/**
	 * asigna descBanco
	 * @param descBanco : descBanco
	 */
	public void setDescBanco(String descBanco) {
		this.descBanco = descBanco;
	}
	
	/**
	 * obtiene descErr
	 * @return descErr : descErr
	 */
	public String getDescErr() {
		return descErr;
	}

	/**
	 * asigna descErr
	 * @param descErr : descErr
	 */
	public void setDescErr(String descErr) {
		this.descErr = descErr;
	}

	/**
	 * obtiene contrato
	 * @return contrato : contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * asigna contrato
	 * @param contrato : contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * obtiene regIni
	 * @return regIni : regIni
	 */
	public int getRegIni() {
		return regIni;
	}
	
	/**
	 * asigna regIni
	 * @param regIni : regIni
	 */
	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}

	/**
	 * obtiene regFin
	 * @return regFin : regFin
	 */
	public int getRegFin() {
		return regFin;
	}

	/**
	 * asigna regFin
	 * @param regFin : regFin
	 */
	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}

	/**
	 * @param criterio el criterio a establecer
	 */
	public void setCriterio(String criterio) {
		this.criterio = criterio;
	}

	/**
	 * @return el criterio
	 */
	public String getCriterio() {
		return criterio;
	}

	/**
	 * @param textoBuscar el textoBuscar a establecer
	 */
	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}

	/**
	 * @return el textoBuscar
	 */
	public String getTextoBuscar() {
		return textoBuscar;
	}

}