package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean de datos para Consulta de Interbancarias Recibidas
 * @author Z712236 ferruzca
 *
 */
public class InterbancariasRecibidasBean implements Serializable {

	/**
	 * Serial de version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * codError codigo de error
	 */
	private String codError;
	/**
	 * fchTrasnfer fecha transferencia
	 */
	private String fchTrasnfer;
	/**
	 * cuenta del cliente
	 */
	private String cuenta;
	/**
	 * referencia de operacion
	 */
	private String referencia;
	/**
	 * cveRastreo clave de rastreo
	 */
	private String cveRastreo;
	/**
	 * numReferencia numero de referencia
	 */
	private String numReferencia;
	/**
	 * refInterbancaria referencia interbancaria
	 */
	private String refInterbancaria;
	/**
	 * cuentaOrdenante cuenta del ordenante
	 */
	private String cuentaOrdenante;
	/**
	 * nombreOrdenante nombre del ordenante
	 */
	private String nombreOrdenante;
	/**
	 * rfcOrdenante rfc del ordenante
	 */
	private String rfcOrdenante;
	/**
	 * bancoOrdenante banco ordenante
	 */
	private String bancoOrdenante;
	/**
	 * importe de operacion
	 */
	private String importe;
	/**
	 * estatus de operacion
	 */
	private String estatus;
	/**
	 * fechaHrConfirmacion fecha y hora de confirmacion
	 */
	private String fechaHrConfirmacion;
	/**
	 * conceptoPagoTransfer concepto pago de transferencia
	 */
	private String conceptoPagoTransfer;
	/**
	 * URL que contiene los parametros para enviar a banxico
	 */
	private String URL;
	/**
	 * lstInterbancarias lista de interbancarias recibidas
	 */
	private List<InterbancariasRecibidasBean> lstInterbancarias=new ArrayList<InterbancariasRecibidasBean>();


	/**
	 * Obtener codigo de error
	 * @return codError codigo de error
	 */
	public String getCodError() {
		return codError;
	}
	/**
	 * Asignar valor codigo error
	 * @param codError codigo error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	/**
	 * Obtener fecha de transferencia
	 * @return fchTrasnfer devuelve fecha transferencia
	 */
	public String getFchTrasnfer() {
		return fchTrasnfer;
	}
	/**
	 * Asignar valor fecha transfer
	 * @param fchTrasnfer fecha transferencia
	 */
	public void setFchTrasnfer(String fchTrasnfer) {
		this.fchTrasnfer = fchTrasnfer;
	}


	/**
	 * Obtener cuenta del cliente
	 * @return cuenta del cliente
	 */
	public String getCuenta() {
		return cuenta;
	}
	/**
	 * Asignar valor cuenta
	 * @param cuenta cuenta del cliente
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}


	/**
	 * Obtener valor referencia
	 * @return referencia de operacion
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * Asignar valor referencia
	 * @param referencia de operacion
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	/**
	 * Obtener clave de rastreo
	 * @return cveRastreo clave de rastreo
	 */
	public String getCveRastreo() {
		return cveRastreo;
	}
	/**
	 * Asignar clave de rastreo
	 * @param cveRastreo clave de rastreo
	 */
	public void setCveRastreo(String cveRastreo) {
		this.cveRastreo = cveRastreo;
	}


	/**
	 * Obtener numero de referencia
	 * @return numReferencia numero de referencia
	 */
	public String getNumReferencia() {
		return numReferencia;
	}
	/**
	 * Asignar numero de referencia
	 * @param numReferencia numero de referencia
	 */
	public void setNumReferencia(String numReferencia) {
		this.numReferencia = numReferencia;
	}


	/**
	 * Obtener referencia interbancaria
	 * @return refInterbancaria referencia interbancaria
	 */
	public String getRefInterbancaria() {
		return refInterbancaria;
	}
	/**
	 * Asignar referencia interbancaria
	 * @param refInterbancaria referencia interbancaria
	 */
	public void setRefInterbancaria(String refInterbancaria) {
		this.refInterbancaria = refInterbancaria;
	}


	/**
	 * Obtener cuenta ordenante
	 * @return cuentaOrdenante cuenta ordenante
	 */
	public String getCuentaOrdenante() {
		return cuentaOrdenante;
	}
	/**
	 * Asignar cuenta ordenante
	 * @param cuentaOrdenante cuenta ordenante
	 */
	public void setCuentaOrdenante(String cuentaOrdenante) {
		this.cuentaOrdenante = cuentaOrdenante;
	}


	/**
	 * Obtener nombre ordenante
	 * @return nombreOrdenante nombre ordenante
	 */
	public String getNombreOrdenante() {
		return nombreOrdenante;
	}
	/**
	 * Asignar nombre ordenante
	 * @param nombreOrdenante nombre ordenante
	 */
	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}


	/**
	 * Obtener rfc ordenante
	 * @return rfcOrdenante rfc ordenante
	 */
	public String getRfcOrdenante() {
		return rfcOrdenante;
	}
	/**
	 * Asignar rfc ordenante
	 * @param rfcOrdenante rfc ordenante
	 */
	public void setRfcOrdenante(String rfcOrdenante) {
		this.rfcOrdenante = rfcOrdenante;
	}


	/**
	 * Obtener banco ordenante
	 * @return bancoOrdenante banco ordenante
	 */
	public String getBancoOrdenante() {
		return bancoOrdenante;
	}
	/**
	 * Asignar banco ordenante
	 * @param bancoOrdenante banco ordenante
	 */
	public void setBancoOrdenante(String bancoOrdenante) {
		this.bancoOrdenante = bancoOrdenante;
	}
	
	
	/**
	 * importe de operacion
	 * @return importe de operacion
	 */
	public String getImporte() {
		return importe;
	}
	/**
	 * Asignar importe de operacion
	 * @param importe de operacion
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}


	/**
	 * Obtener estaus de transferencia
	 * @return estaus de transferencia
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * Asignar estatus de tranferencia
	 * @param estatus de transferencia
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * Obtener fecha hora de confirmacion
	 * @return fechaHrConfirmacion fecha y hora de confirmacion
	 */
	public String getFechaHrConfirmacion() {
		return fechaHrConfirmacion;
	}
	/**
	 * Asignar fecha y hora de confirmacion
	 * @param fechaHrConfirmacion fecha y hora de confirmacion
	 */
	public void setFechaHrConfirmacion(String fechaHrConfirmacion) {
		this.fechaHrConfirmacion = fechaHrConfirmacion;
	}


	/**
	 * Obtener concepto pago de transferencia
	 * @return conceptoPagoTransfer concepto pago de transferencia
	 */
	public String getConceptoPagoTransfer() {
		return conceptoPagoTransfer;
	}
	/**
	 * Asignar concepto pago de transferencia
	 * @param conceptoPagoTransfer concepto pago de transferencia
	 */
	public void setConceptoPagoTransfer(String conceptoPagoTransfer) {
		this.conceptoPagoTransfer = conceptoPagoTransfer;
	}
	
	
	/**
	 * Obtener lista de interbancarias recibidas
	 * @return lstInterbancarias lista de interbancarias recibidas
	 */
	public List<InterbancariasRecibidasBean> getLstInterbancarias() {
		return lstInterbancarias;
	}
	/**
	 * Asignar lista de interbancarias recibidas
	 * @param lstInterbancarias lista de interbancarias recibidas
	 */
	public void setLstInterbancarias(
			List<InterbancariasRecibidasBean> lstInterbancarias) {
		this.lstInterbancarias = lstInterbancarias;
	}
	/**
	 * Parametro para retornar la URL
	 * @return
	 */
	public String getURL() {
		return URL;
	}
	/**
	 * Parametro para asignar la URL
	 * @param uRL
	 */
	public void setURL(String uRL) {
		URL = uRL;
	}
	
	
}
