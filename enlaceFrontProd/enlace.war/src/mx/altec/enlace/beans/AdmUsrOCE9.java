package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;


import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import com.netscape.server.servlet.i18n.io.Serializable;

public class AdmUsrOCE9 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "OCE900731123451O00N2";
	
	public static AdmUsrOCE9Factory getFactoryInstance() {
		return FACTORY;
	}	

	
	private static final AdmUsrOCE9Factory FACTORY = new AdmUsrOCE9Factory();
	
	private static class AdmUsrOCE9Factory implements AdmUsrBuilder<AdmUsrOCE9> {

		public AdmUsrOCE9 build(String arg) {
			
			AdmUsrOCE9 bean = new AdmUsrOCE9();
			
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("OCA0000");
				bean.setCodExito(true);
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}			
			return bean;
		}
	}

}
