package mx.altec.enlace.beans;
/**
 * 
 * @author Z087104 Cecilio Maldonado Avalos
 * @version 1.0
 * @Fecha 15 Julio 2011
 * @proyecto 201170301 - EBEE Contratación y perfilación
 *
 */

public class GLContrato {
	
	String contrato;
	String numPersona;
	String descContrato;
	String estatusCta;
	String fechaAlta;

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getNumPersona() {
		return numPersona;
	}

	public void setNumPersona(String numPersona) {
		this.numPersona = numPersona;
	}
	
	public String getDesContrato() {
		return descContrato;
	}

	public void setDesContrato(String descContrato) {
		this.descContrato = descContrato;
	}

	public String getEstatusCta() {
		return estatusCta;
	}

	public void setEstatusCta(String estatusCta) {
		this.estatusCta = estatusCta;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

}
