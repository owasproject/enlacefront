package mx.altec.enlace.beans;

public class EnvioEmailBean {
	
	private String requestId;
	private String template;
	private String appId;
	private String from;
	private String fromName;
	private String to;
	private String toName;
	private String subject;
	private String msgBody;
	private String sentDate;
	private String bounceAddress;
	private String codigoCliente;
	private String centrCostos;
	private String URLAttachment;
	private String attachmentName;
	
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getBounceAddress() {
		return bounceAddress;
	}
	public void setBounceAddress(String bounceAddress) {
		this.bounceAddress = bounceAddress;
	}
	public String getCentrCostos() {
		return centrCostos;
	}
	public void setCentrCostos(String centrCostos) {
		this.centrCostos = centrCostos;
	}
	public String getCodigoCliente() {
		return codigoCliente;
	}
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getMsgBody() {
		return msgBody;
	}
	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getSentDate() {
		return sentDate;
	}
	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getURLAttachment() {
		return URLAttachment;
	}
	public void setURLAttachment(String attachment) {
		URLAttachment = attachment;
	}

}