package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminusr.shared.PerfilPrototipo;


import java.io.Serializable;
import java.util.ArrayList;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmUsrGL37 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL3700441123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM371  P";
	private static final String FORMATO_HEADER_DET = "@DCGLM372  P";
	private static final String FORMATO_HEADER_PAG = "@DCGLM373  P";
	
	private static final AdmUsrGL37Factory FACTORY = new AdmUsrGL37Factory();
		
	
	
	private ArrayList<PerfilPrototipo> perfiles;
	
	public static AdmUsrGL37Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmUsrGL37Factory implements AdmUsrBuilder<AdmUsrGL37> {

		public AdmUsrGL37 build(String arg) {
			
			AdmUsrGL37 bean = new AdmUsrGL37();
			ArrayList<PerfilPrototipo> prfList = new ArrayList<PerfilPrototipo>();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL370000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);							

				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						int numRegs = Integer.parseInt(getValor(arg, index , 3));
						EIGlobal.mensajePorTrace("==> numRegs: [" + numRegs + "]", EIGlobal.NivelLog.ERROR);
						if (numRegs > 0) {
							index = arg.indexOf(FORMATO_HEADER_DET);
							if (index != -1) {
								EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
								for (int i = 0; i < numRegs; i ++) { // lee el detalle
									index += FORMATO_HEADER_DET.length();
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
									PerfilPrototipo ppBean = new PerfilPrototipo();
									String grupo = getValor(arg, index, 2);
									EIGlobal.mensajePorTrace("==> grupo: [" + grupo + "]", EIGlobal.NivelLog.ERROR);
									String descGpo = getValor(arg, index += 2, 40);
									String stsGpo = getValor(arg, index+= 40 , 2);
									String dscStsGpo = getValor(arg, index += 2, 10);
									ppBean.setCvePerfil(getValor(arg, index += 10, 8));
									EIGlobal.mensajePorTrace("==> Perfil: [" + ppBean.getCvePerfil() + "]", EIGlobal.NivelLog.ERROR);
									ppBean.setDescripcion(getValor(arg, index += 8, 80));
									EIGlobal.mensajePorTrace("==> Desc: [" + ppBean.getDescripcion() + "]", EIGlobal.NivelLog.ERROR);
									ppBean.setNombreComercial(getValor(arg, index += 80, 40));
									EIGlobal.mensajePorTrace("==> NomCom: [" + ppBean.getNombreComercial() + "]", EIGlobal.NivelLog.ERROR);
									String estatus = getValor(arg, index+= 40 , 2);
									String dscStatus = getValor(arg, index += 2, 10);
									EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.ERROR);
									index += 11;
									prfList.add(ppBean);
								}
							}
						}
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmUsrGL37 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
				
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setPerfiles(prfList);
			return bean;
		}
		
	}

	public ArrayList<PerfilPrototipo> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(ArrayList<PerfilPrototipo> perfiles) {
		this.perfiles = perfiles;
	}


}
