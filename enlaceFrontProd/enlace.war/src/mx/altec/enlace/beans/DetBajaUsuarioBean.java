package mx.altec.enlace.beans;

import java.sql.Date;

/**
 * Bean para almacenar los datos del mantenimiento a l�mites y Montos  
 * @author 
 *
 */
public class DetBajaUsuarioBean {

	
	private String contrato;
	private String usuario;
	private String serieToken;
	private String estadoToken;
	private String estadoNIP;
	private Date fechaBaja; 
	private String estadoOperacion;
	private String origen;
	
	
	public DetBajaUsuarioBean() {
		super();
		this.contrato = "";
		this.usuario = "";
		this.serieToken = "";
		this.estadoToken = "";
		this.estadoNIP = "";
		this.fechaBaja = new java.sql.Date(new java.util.Date().getTime());;
		this.estadoOperacion = "";
		this.origen = "";
	}
	
	
	public DetBajaUsuarioBean(String contrato, String usuario, String serieToken,
			String estadoToken, String estadoNIP, Date fechaBaja, 
			String estadoOperacion, String origen) {
		super();
		this.contrato = contrato;
		this.usuario = usuario;
		this.serieToken = serieToken;
		this.estadoToken = estadoToken;
		this.estadoNIP = estadoNIP;
		this.fechaBaja = fechaBaja;
		this.estadoOperacion = estadoOperacion;
		this.origen = origen;
	}


	public String getContrato() {
		return contrato;
	}


	public void setContrato(String contrato) {
		this.contrato = contrato;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getSerieToken() {
		return serieToken;
	}


	public void setSerieToken(String serieToken) {
		this.serieToken = serieToken;
	}


	public String getEstadoToken() {
		return estadoToken;
	}


	public void setEstadoToken(String estadoToken) {
		this.estadoToken = estadoToken;
	}


	public String getEstadoNIP() {
		return estadoNIP;
	}


	public void setEstadoNIP(String estadoNIP) {
		this.estadoNIP = estadoNIP;
	}


	public Date getFechaBaja() {
		return fechaBaja;
	}


	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}


	public String getEstadoOperacion() {
		return estadoOperacion;
	}


	public void setEstadoOperacion(String estadoOperacion) {
		this.estadoOperacion = estadoOperacion;
	}


	public String getOrigen() {
		return origen;
	}


	public void setOrigen(String origen) {
		this.origen = origen;
	}
	
}
