package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;
import mx.altec.enlace.bo.TrxGP93VO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase administradora para manejar el procedimiento para la transaccion GP93
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 22, 2011
 */
public class AdmTrxGP93 extends AdmUsrGL35Base implements Serializable{
	
	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constantes para codigo exito
	 */
	private static final String CODIGO_EXITO = "@1";
	
	/**
	 * Constantes para codigo error
	 */
	private static final String CODIGO_ERROR = "@2";
	
	/**
	 * Constante para la cabecera de la trama
	 */
	public static String HEADER = "GP9300391000011O00N2";
	
	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_HEADER = "@DCGPM0931 P";
	
	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_PAGINACION = "@AVGPA0002";
	
	/**
	 * Constante para reconocer la instancia de la clase
	 */
	private static final TrxGP93VOFactory FACTORY = new TrxGP93VOFactory();	
	
	/**
	 * Obtener el valor de FACTORY.
     *
     * @return FACTORY valor asignado.
	 */
	public static TrxGP93VOFactory getFactoryInstance() {
		return FACTORY;
	}
	
	/**
	 * Objeto contenedor de tipo TrxGP93VO
	 */
	private TrxGP93VO GP93Bean;
	
	/**
	 * Objeto contenedor de beans de tipo TrxGP93VO
	 */
	private Vector catGP93Bean;
	
	/**
	 * Variable para indicar si existen mas registros
	 */
	private boolean masPaginacion;
	
	/**
	 * Variable para indicar la llave de paginacion
	 */
	private String llavePaginacion;
	
	/**
	 * Obtener el valor de GP93Bean.
     *
     * @return GP93Bean valor asignado.
	 */	
	public TrxGP93VO getGP93bean() {
		return GP93Bean;
	}

	/**
     * Asignar un valor a GP93Bean.
     *
     * @param GP93Bean Valor a asignar.
     */
	public void setGP93bean(TrxGP93VO beanGP93) {
		this.GP93Bean = beanGP93;
	}	
	
	/**
	 * Obtener el valor de catGP93Bean.
     *
     * @return catGP93Bean valor asignado.
	 */	
	public Vector getCatGP93Bean() {
		return catGP93Bean;
	}

	/**
     * Asignar un valor a catGP93Bean.
     *
     * @param catGP93Bean Valor a asignar.
     */
	public void setCatGP93Bean(Vector catGP93Bean) {
		this.catGP93Bean = catGP93Bean;
	}

	/**
	 * Obtener el valor de masPaginacion.
     *
     * @return masPaginacion valor asignado.
	 */	
	public boolean getMasPaginacion() {
		return masPaginacion;
	}

	/**
     * Asignar un valor a masPaginacion.
     *
     * @param masPaginacion Valor a asignar.
     */
	public void setMasPaginacion(boolean masPaginacion) {
		this.masPaginacion = masPaginacion;
	}	
	
	/**
	 * Obtener el valor de llavePaginacion.
     *
     * @return llavePaginacion valor asignado.
	 */	
	public String getLlavePaginacion() {
		return llavePaginacion;
	}
	
	/**
     * Asignar un valor a llavePaginacion.
     *
     * @param llavePaginacion Valor a asignar.
     */
	public void setLlavePaginacion(String llavePaginacion) {
		this.llavePaginacion = llavePaginacion;
	}

	/**
	 * Inner Class que implementa la interfaz AdmUsrBuilder
	 * Metodos a implementar: 	build
	 *
	 */
	private static class TrxGP93VOFactory implements AdmUsrBuilder<AdmTrxGP93> {
		
		/**
		 * Metodo implementado build para el trato de la trama de salida
		 */
		public AdmTrxGP93 build(String arg) {
	    	EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build:: Entrando..."
	    			, EIGlobal.NivelLog.DEBUG);
			AdmTrxGP93 bean = new AdmTrxGP93();
			TrxGP93VO beanGP93 = null;
			StringTokenizer tramaSalida = null;
			String token = "";
			String llavePag = "";
			Vector regsbeanGP93 = new Vector();
			if(verificaCodigo(arg)){				
		    	EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build:: Codigo exitoso"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion("GP930000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_PAGINACION);
				if(index != -1){
					bean.setMasPaginacion(true);
				}else{
					bean.setMasPaginacion(false);
				}
				
				index = arg.indexOf(FORMATO_HEADER);
				if(index != -1){
					tramaSalida = new StringTokenizer(arg.substring(arg.indexOf(FORMATO_HEADER), arg.length()),"@");
					EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build:: Cuantos tokens son:"+tramaSalida.countTokens(), EIGlobal.NivelLog.DEBUG);
					while(tramaSalida.hasMoreTokens()){						
						try{
							beanGP93 = new TrxGP93VO();
							index = 0;
							token = tramaSalida.nextToken();
							index += FORMATO_HEADER.length()-1;
							beanGP93.setOVarCod(getValor(token, index, 8));
							beanGP93.setOVarDes(getValor(token, index += 8, 30));				
							
					    	EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build:: Resultados..." +
					    			beanGP93.toString(), EIGlobal.NivelLog.DEBUG);
					    	regsbeanGP93.add(beanGP93);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("AdmTrxGP93 - Error en la cantidad de registros: ["
									+ e + "]", EIGlobal.NivelLog.ERROR);
						}
					}
				}
			}else{
		    	EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build:: Codigo erroneo"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			//bean.setGP93bean(beanGP93);
			llavePag = ((TrxGP93VO)regsbeanGP93.lastElement()).getOVarCod();
			EIGlobal.mensajePorTrace("AdmTrxGP93::TrxGP93VOFactory::build::Llave siguiente iteracion->"+llavePag, EIGlobal.NivelLog.DEBUG);
			bean.setLlavePaginacion(llavePag);
			bean.setCatGP93Bean(regsbeanGP93);
			return bean;
		}		
	}
	
	/**
	 * Metodo para verificar el codigo de error
	 * @param resultado
	 * @return
	 */
	public static boolean verificaCodigo(String resultado){
		if (resultado != null && resultado.indexOf(CODIGO_EXITO) != -1)
			return true;
		else if (resultado != null && resultado.indexOf(CODIGO_ERROR) != -1)
			return false;
		return false;
	}

}
