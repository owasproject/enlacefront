package mx.altec.enlace.beans;

public class ConsEdoCtaResultadoBean {
	
	/**
	 * idEdoCta
	 */
	private String idEdoCta;
	
	/**
	 * contrato
	 */
	private String contrato;
	
	/**
	 * usuarioSolicitante
	 */
	private String usuarioSolicitante;
	
	/**
	 * fechaEnvio
	 */
	private String fechaEnvio;
	
	/**
	 * estatus
	 */
	private String estatus;
	
	/**
	 * nomArch
	 */
	private String nombreArchivo;
	
	/**
	 * variable totalRegistros
	 */
	private int totalRegistros;

	/**
	 * obtiene idEdoCta
	 * @return idEdoCta : idEdoCta
	 */
	public String getIdEdoCta() {
		return idEdoCta;
	}

	/**
	 * asigna idEdoCta
	 * @param idEdoCta : idEdoCta
	 */
	public void setIdEdoCta(String idEdoCta) {
		this.idEdoCta = idEdoCta;
	}

	/**
	 * obtiene contrato
	 * @return contrato : contrato
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * asigna contrato
	 * @param contrato : contrato
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * obtiene usuarioSolicitante
	 * @return usuarioSolicitante : usuarioSolicitante
	 */
	public String getUsuarioSolicitante() {
		return usuarioSolicitante;
	}

	/**
	 * asigna usuarioSolicitante
	 * @param usuarioSolicitante : usuarioSolicitante
	 */
	public void setUsuarioSolicitante(String usuarioSolicitante) {
		this.usuarioSolicitante = usuarioSolicitante;
	}

	/**
	 * obtiene fechaEnvio
	 * @return fechaEnvio : fechaEnvio
	 */
	public String getFechaEnvio() {
		return fechaEnvio;
	}

	/**
	 * asigna fechaEnvio
	 * @param fechaEnvio : fechaEnvio
	 */
	public void setFechaEnvio(String fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	/**
	 * obtiene estatus
	 * @return estatus : estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * asigna estatus
	 * @param estatus : estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * obtiene nombreArchivo
	 * @return nombreArchivo : nombreArchivo
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * asigna nombreArchivo
	 * @param nombreArchivo : nombreArchivo
	 */
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	/**
	 * Obtitne totalRegistros
	 * @return totalRegistros : totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}

	/**
	 * Asigna totalRegistros
	 * @param totalRegistros : totalRegistros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
}
