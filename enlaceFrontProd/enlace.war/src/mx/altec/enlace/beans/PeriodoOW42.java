package mx.altec.enlace.beans;

import java.io.Serializable;

import mx.altec.enlace.bo.BusinessException;

public class PeriodoOW42 implements Serializable {
	
	/** Id de version generado automaticamente para la serializacion del objeto */
	private static final long serialVersionUID = -8886416941806903687L;

	/** Codigo de repuesta */
	private String codigoRespuesta;
	/** Descripcion de la respuesta */
	private String descripcionRespuesta;
	/** Periodo en formato YYYYDD */
	private String periodo;
	/** Folio Ondemand */
	private String folioOnDemand;
	/**  Tipo de Estado de Cuenta */
	private String tipoEstadoCuenta;
	/** Cadena con Periodo@FolioOD@TipoEDC */
	private String cadena;

	/** Mes del periodo asignado */
	private transient int mesPeriodo;
	/** Anio del periodo asignado */
	private transient String anioPeriodo;
	
	
	/**
	 * Obtiene el codigo de respuesta
	 * @return El codigo de respuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	/**
	 * Asigna un valor al codigo de respuesta
	 * @param codigoRespuesta El nuevo codigo de respuesta
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	/**
	 * Obtiene la descripcion de la respuesta
	 * @return La descripcion de la respuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	/**
	 * Asigna un valor a la descripcion de la respuesta
	 * @param descripcionRespuesta La nueva descripcion de la respuesta
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}
	/**
	 * Obtiene el periodo
	 * @return El periodo
	 */
	public String getPeriodo() {
		return periodo;
	}
	/**
	 * Obtiene el periodo con el formato MMM-YYYY
	 * @return El periodo con formato
	 */
	public String getPeriodoConFormato(){
		return String.format("%s-%s", obtenerNombreCortoMes(), anioPeriodo);
	}
	/**
	 * Asigna un valor al periodo
	 * @param periodo El nuevo periodo
	 * @throws BusinessException En caso de no exista 
	 * informacion o la longitud sea incorrecta
	 */
	public void setPeriodo(String periodo) throws BusinessException {
		if(periodo == null || periodo.length() != 6){
			throw new BusinessException("No existen periodos");
		} else {
			try {
				mesPeriodo = Integer.parseInt(periodo.substring(4));
			} catch(NumberFormatException e) {
				mesPeriodo = 0;
			}
			anioPeriodo = periodo.substring(0, 4);
		}
		
		this.periodo = periodo;
	}
	/**
	 * Obtiene el folio Ondemand
	 * @return El folio Ondemand
	 */
	public String getFolioOnDemand() {
		return folioOnDemand;
	}
	/**
	 * Asigna un valor para el folio Ondemand
	 * @param folioOnDemand El nuevo folio Ondemand
	 */
	public void setFolioOnDemand(String folioOnDemand) {
		this.folioOnDemand = folioOnDemand;
	}
	
	/**
	 * @param tipoEstadoCuenta el tipoEstadoCuenta a establecer
	 */
	public void setTipoEstadoCuenta(String tipoEstadoCuenta) {
		this.tipoEstadoCuenta = tipoEstadoCuenta;
	}
	/**
	 * @return el tipoEstadoCuenta
	 */
	public String getTipoEstadoCuenta() {
		return tipoEstadoCuenta;
	}
	/**
	 * @param cadena el cadena a establecer
	 */
	public void setCadena(String cadena) {
		this.cadena = cadena;
	}
	/**
	 * @return el cadena
	 */
	public String getCadena() {
		return cadena;
	}
	/**
	 * Obtiene el nombre corto del mes
	 * @return EL nombre corto del mes
	 */
	private String obtenerNombreCortoMes(){
		String mesCorto;
		
		switch(mesPeriodo){
		case 1:
			mesCorto = "ENE";
		break;
		case 2:
			mesCorto = "FEB";
		break;
		case 3:
			mesCorto = "MAR";
		break;
		case 4:
			mesCorto = "ABR";
		break;
		case 5:
			mesCorto = "MAY";
		break;
		case 6:
			mesCorto = "JUN";
		break;
		case 7:
			mesCorto = "JUL";
		break;
		case 8:
			mesCorto = "AGO";
		break;
		case 9:
			mesCorto = "SEP";
		break;
		case 10:
			mesCorto = "OCT";
		break;
		case 11:
			mesCorto = "NOV";
		break;
		case 12:
			mesCorto = "DIC";
		break;
		default:
			mesCorto = String.valueOf(mesPeriodo);
		}
		
		return mesCorto;
	}
}