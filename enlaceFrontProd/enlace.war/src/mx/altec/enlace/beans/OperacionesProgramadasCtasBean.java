package mx.altec.enlace.beans;

/**
 * Clase encargada de almacenar la informacion para la tabla TCT_PROGRAMACION
 * @author FSW-Indra
 * @sice 11/02/2015
 *
 */
public class OperacionesProgramadasCtasBean{

	/**
	 * Variable para almacenar la cuenta1 de Operacion Programada.
	 */
	private String cuenta1;
	/**
	 * Variable para almacenar la cuenta2 de Operacion Programada.
	 */
	private String cuenta2;
	/**
	 * Variable para almacenar el tipoCtaOrigen de Operacion Programada.
	 */
	private String tipoCtaOrigen;
	/**
	 * Variable para almacenar el tipoCtaDestino de Operacion Programada.
	 */
	private String tipoCtaDestino;
	/**
	 * Variable para almacenar la descripcionCtaOrigen de Operacion Programada.
	 */
	private String descripcionCtaOrigen;
	/**
	 * Variable para almacenar el descripcionCtaDestino de Operacion Programada.
	 */
	private String descripcionCtaDestino;
	/**
	 * Variable para almacenar la Clave del Banco de Operacion Programada.
	 */
	private String cveBancoCtaDestino;
	/**
	 * Variable para almacenar la plaza de Operacion Programada.
	 */
	private String plazaCtaDestino;
	/**
	 * Variable para almacenar la sucursal de Operacion Programada.
	 */
	private String sucursalCtaDestino;

	/**
	 * getCuenta1 de tipo String.
	 * @author FSW-Indra
	 * @return cuenta1 de tipo String
	 */
	public String getCuenta1() {
		return cuenta1;
	}
	/**
	 * setCuenta1 para asignar valor a cuenta1.
	 * @author FSW-Indra
	 * @param cuenta1 de tipo String
	 */
	public void setCuenta1(String cuenta1) {
		this.cuenta1 = cuenta1;
	}
	/**
	 * getCuenta2 de tipo String.
	 * @author FSW-Indra
	 * @return cuenta2 de tipo String
	 */
	public String getCuenta2() {
		return cuenta2;
	}
	/**
	 * setCuenta2 para asignar valor a cuenta2.
	 * @author FSW-Indra
	 * @param cuenta2 de tipo String
	 */
	public void setCuenta2(String cuenta2) {
		this.cuenta2 = cuenta2;
	}
	/**
	 * getTipoCtaOrigen de tipo String.
	 * @author FSW-Indra
	 * @return tipoCtaOrigen de tipo String
	 */
	public String getTipoCtaOrigen() {
		return tipoCtaOrigen;
	}
	/**
	 * setTipoCtaOrigen para asignar valor a tipoCtaOrigen.
	 * @author FSW-Indra
	 * @param tipoCtaOrigen de tipo String
	 */
	public void setTipoCtaOrigen(String tipoCtaOrigen) {
		this.tipoCtaOrigen = tipoCtaOrigen;
	}
	/**
	 * getTipoCtaDestino de tipo String.
	 * @author FSW-Indra
	 * @return tipoCtaDestino de tipo String
	 */
	public String getTipoCtaDestino() {
		return tipoCtaDestino;
	}
	/**
	 * setTipoCtaDestino para asignar valor a tipoCtaDestino.
	 * @author FSW-Indra
	 * @param tipoCtaDestino de tipo String
	 */
	public void setTipoCtaDestino(String tipoCtaDestino) {
		this.tipoCtaDestino = tipoCtaDestino;
	}
	/**
	 * getDescripcionCtaOrigen de tipo String.
	 * @author FSW-Indra
	 * @return descripcionCtaOrigen de tipo String
	 */
	public String getDescripcionCtaOrigen() {
		return descripcionCtaOrigen;
	}
	/**
	 * setDescripcionCtaOrigen para asignar valor a descripcionCtaOrigen.
	 * @author FSW-Indra
	 * @param descripcionCtaOrigen de tipo String
	 */
	public void setDescripcionCtaOrigen(String descripcionCtaOrigen) {
		this.descripcionCtaOrigen = descripcionCtaOrigen;
	}
	/**
	 * getDescripcionCtaDestino de tipo String.
	 * @author FSW-Indra
	 * @return descripcionCtaDestino de tipo String
	 */
	public String getDescripcionCtaDestino() {
		return descripcionCtaDestino;
	}
	/**
	 * setDescripcionCtaDestino para asignar valor a descripcionCtaDestino.
	 * @author FSW-Indra
	 * @param descripcionCtaDestino de tipo String
	 */
	public void setDescripcionCtaDestino(String descripcionCtaDestino) {
		this.descripcionCtaDestino = descripcionCtaDestino;
	}
	/**
	 * getCveBancoCtaDestino de tipo String.
	 * @author FSW-Indra
	 * @return cveBancoCtaDestino de tipo String
	 */
	public String getCveBancoCtaDestino() {
		return cveBancoCtaDestino;
	}
	/**
	 * setCveBancoCtaDestino para asignar valor a cveBancoCtaDestino.
	 * @author FSW-Indra
	 * @param cveBancoCtaDestino de tipo String
	 */
	public void setCveBancoCtaDestino(String cveBancoCtaDestino) {
		this.cveBancoCtaDestino = cveBancoCtaDestino;
	}
	/**
	 * getPlazaCtaDestino de tipo String.
	 * @author FSW-Indra
	 * @return plazaCtaDestino de tipo String
	 */
	public String getPlazaCtaDestino() {
		return plazaCtaDestino;
	}
	/**
	 * setPlazaCtaDestino para asignar valor a plazaCtaDestino.
	 * @author FSW-Indra
	 * @param plazaCtaDestino de tipo String
	 */
	public void setPlazaCtaDestino(String plazaCtaDestino) {
		this.plazaCtaDestino = plazaCtaDestino;
	}
	/**
	 * getSucursalCtaDestino de tipo String.
	 * @author FSW-Indra
	 * @return sucursalCtaDestino de tipo String
	 */
	public String getSucursalCtaDestino() {
		return sucursalCtaDestino;
	}
	/**
	 * setSucursalCtaDestino para asignar valor a sucursalCtaDestino.
	 * @author FSW-Indra
	 * @param sucursalCtaDestino de tipo String
	 */
	public void setSucursalCtaDestino(String sucursalCtaDestino) {
		this.sucursalCtaDestino = sucursalCtaDestino;
	}
}