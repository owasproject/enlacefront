package mx.altec.enlace.beans;

import java.util.Date;

public class ConsultaAsignacionTarjetasBean {

	private String folio;
	private Date fechaIni;
	private Date fechaFin;
	private String remesa;
	private String numEmpleado;
	private Boolean recibida;
	private Boolean cancelada;
	private Boolean enProceso;
	private Boolean procesada;


	public Boolean getRecibida() {
		return recibida;
	}
	public void setRecibida(Boolean recibida) {
		this.recibida = recibida;
	}
	public Boolean getCancelada() {
		return cancelada;
	}
	public void setCancelada(Boolean cancelada) {
		this.cancelada = cancelada;
	}
	public Boolean getEnProceso() {
		return enProceso;
	}
	public void setEnProceso(Boolean enProceso) {
		this.enProceso = enProceso;
	}
	public Boolean getProcesada() {
		return procesada;
	}
	public void setProcesada(Boolean procesada) {
		this.procesada = procesada;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getRemesa() {
		return remesa;
	}
	public void setRemesa(String remesa) {
		this.remesa = remesa;
	}
	public String getNumEmpleado() {
		return numEmpleado;
	}
	public void setNumEmpleado(String numEmpleado) {
		this.numEmpleado = numEmpleado;
	}


}