package mx.altec.enlace.beans;

public class ConsEdoCtaDetalleBean {
	
	/**
	 * variable idEdoCtaDet
	 */
	private String idEdoCtaDet;
	
	/**
	 * variable idEdoCtaCtrl
	 */
	private String idEdoCtaCtrl;
	
	/**
	 * variable numCuenta
	 */
	private String numCuenta;
	
	/**
	 * variable numContrato
	 */
	private String numContrato;
	
	/**
	 * variable numSec
	 */
	private String numSec;
	
	/**
	 * variable estatus
	 */
	private String estatus;
	
	/**
	 * variable descError
	 */
	private String descError;

	/**
	 * variable ppls
	 */
	private String ppls;
	
	/**
	 * variable codClte
	 */
	private String codClte;
	
	/**
	 * variable tipoEdoCta
	 */
	private String tipoEdoCta;
	
	/**
	 * atributo contratoTDC
	 */
	private String contratoTDC;
	
	/**
	 * atributo beneficiario
	 */
	private String beneficiario;
	
	/**
	 * variable descCta
	 */
	private String descCta;
	
	/**
	 * variable totalRegistros
	 */
	private int totalRegistros;
	
	/**
	 * obtener idEdoCtaDet
	 * @return idEdoCtaDet : idEdoCtaDet
	 */
	public String getIdEdoCtaDet() {
		return idEdoCtaDet;
	}

	/**
	 * Asigna idEdoCtaDet
	 * @param idEdoCtaDet : idEdoCtaDet
	 */
	public void setIdEdoCtaDet(String idEdoCtaDet) {
		this.idEdoCtaDet = idEdoCtaDet;
	}

	/**
	 * Obtiene idEdoCtaCtrl
	 * @return idEdoCtaCtrl : idEdoCtaCtrl
	 */
	public String getIdEdoCtaCtrl() {
		return idEdoCtaCtrl;
	}

	/**
	 * asigna idEdoCtaCtrl
	 * @param idEdoCtaCtrl : idEdoCtaCtrl
	 */
	public void setIdEdoCtaCtrl(String idEdoCtaCtrl) {
		this.idEdoCtaCtrl = idEdoCtaCtrl;
	}

	/**
	 * obtiene numCuenta
	 * @return numCuenta : numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * asigna numCuenta
	 * @param numCuenta : numCuenta
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * asigna numContrato
	 * @param numContrato : numContrato
	 */
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}

	/**
	 * Obtiene numContrato
	 * @return numContrato : numContrato
	 */
	public String getNumContrato() {
		return numContrato;
	}

	/**
	 * obtiene numSec
	 * @return numSec : numSec
	 */
	public String getNumSec() {
		return numSec;
	}

	/**
	 * asigna numSec
	 * @param numSec : numSec
	 */
	public void setNumSec(String numSec) {
		this.numSec = numSec;
	}

	/**
	 * obtiene estatus
	 * @return estatus : estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * asigna estatus
	 * @param estatus : estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * obtiene descError
	 * @return descError : descError
	 */
	public String getDescError() {
		return descError;
	}

	/**
	 * asigna descError
	 * @param descError : descError
	 */
	public void setDescError(String descError) {
		this.descError = descError;
	}

	/**
	 * obtiene ppls
	 * @return ppls : ppls
	 */
	public String getPpls() {
		return ppls;
	}

	/**
	 * asigna ppls
	 * @param ppls : ppls
	 */
	public void setPpls(String ppls) {
		this.ppls = ppls;
	}

	/**
	 * obtiene codClte
	 * @return codClte : codClte
	 */
	public String getCodClte() {
		return codClte;
	}

	/**
	 * asigna codClte
	 * @param codClte : codClte
	 */
	public void setCodClte(String codClte) {
		this.codClte = codClte;
	}

	/**
	 * obtiene tipoEdoCta
	 * @return tipoEdoCta : tipoEdoCta
	 */
	public String getTipoEdoCta() {
		return tipoEdoCta;
	}

	/**
	 * asigna tipoEdoCta
	 * @param tipoEdoCta : tipoEdoCta
	 */
	public void setTipoEdoCta(String tipoEdoCta) {
		this.tipoEdoCta = tipoEdoCta;
	}

	/**
	 * asigna contratoTDC
	 * @param contratoTDC el contratoTDC a establecer
	 */
	public void setContratoTDC(String contratoTDC) {
		this.contratoTDC = contratoTDC;
	}

	/**
	 * obtiene contratoTDC
	 * @return el contratoTDC
	 */
	public String getContratoTDC() {
		return contratoTDC;
	}

	/**
	 * @param beneficiario el beneficiario a establecer
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return el beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * obtiene descCta
	 * @return descCta : descCta
	 */
	public String getDescCta() {
		return descCta;
	}

	/**
	 * asigna descCta
	 * @param descCta : descCta
	 */
	public void setDescCta(String descCta) {
		this.descCta = descCta;
	}

	/**
	 * Obtitne totalRegistros
	 * @return totalRegistros : totalRegistros
	 */
	public int getTotalRegistros() {
		return totalRegistros;
	}

	/**
	 * Asigna totalRegistros
	 * @param totalRegistros : totalRegistros
	 */
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

}
