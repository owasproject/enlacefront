package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

import mx.altec.enlace.bo.TrxB485VO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase administradora para manejar el procedimiento para la transaccion B485
 *
 * @author Fernando Gurrola Herrera
 * @version 1.0 Sep 27, 2010
 */
public class AdmTrxB485 extends AdmUsrGL35Base implements Serializable {

	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = -6316061958563478941L;

	/**
	 * Constantes para codigo exito
	 */
	private static final String CODIGO_EXITO = "@1";

	/**
	 * Constantes para codigo error
	 */
	private static final String CODIGO_ERROR = "@2";

	/**
	 * Constante para la cabecera de la trama
	 */
	public static String HEADER = "B48500551000011O00N2";

	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_HEADER = "@DCBGM4851 P";

	/**
	* Constante para reconocer la instancia de la clase
	 */
	private static final TrxB485VOFactory FACTORY = new TrxB485VOFactory();

	/**
	 * Obtener el valor de FACTORY.
     *
     * @return FACTORY valor asignado.
	 */
	public static TrxB485VOFactory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Objeto contenedor de tipo TrxB485VO
	 */
	private TrxB485VO b485Bean;

	/**
	 * Obtener el valor de b485Bean.
     *
     * @return b485Bean valor asignado.
	 */
	public TrxB485VO getB485bean() {
		return b485Bean;
	}

	/**
     * Asignar un valor a b485Bean.
     *
     * @param b485Bean Valor a asignar.
     */
	public void setB485bean(TrxB485VO beanB485) {
		this.b485Bean = beanB485;
	}

	/**
	 * Inner Class que implementa la interfaz AdmUsrBuilder
	 * Metodos a implementar: 	build
	 *
	 */
	private static class TrxB485VOFactory implements AdmUsrBuilder<AdmTrxB485> {

		/**
		 * Metodo implementado build para el trato de la trama de salida
		 */
		public AdmTrxB485 build(String arg) {
	    	EIGlobal.mensajePorTrace("AdmTrxB485::TrxB485VOFactory::build:: Entrando..."
	    			, EIGlobal.NivelLog.DEBUG);
			AdmTrxB485 bean = new AdmTrxB485();
			TrxB485VO beanB485 = new TrxB485VO();
			if(verificaCodigo(arg)){
		    	EIGlobal.mensajePorTrace("AdmTrxB485::TrxB485VOFactory::build:: Codigo exitoso"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion("B4850000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);
				if(index != -1){
					index += FORMATO_HEADER.length();
					try{
						beanB485.setCuenta(getValor(arg, index, 20));
						beanB485.setDivisa(getValor(arg, index += 20, 3));
						beanB485.setClabe(getValor(arg, index += 3, 18));


				    	EIGlobal.mensajePorTrace("AdmTrxB485::TrxB485VOFactory::build:: Resultados..." +
				    			beanB485.toString(), EIGlobal.NivelLog.DEBUG);
					}catch(Exception e){
						EIGlobal.mensajePorTrace("AdmLyMGL24 - Error en la cantidad de registros: ["
								+ e + "]", EIGlobal.NivelLog.ERROR);
					}
				}
			}else{
		    	EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Codigo erroneo"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setB485bean(beanB485);
			return bean;
		}
	}

	/**
	 * Metodo para verificar el codigo de error
	 * @param resultado
	 * @return
	 */
	public static boolean verificaCodigo(String resultado){
		if (resultado != null && resultado.indexOf(CODIGO_EXITO) != -1)
			return true;
		else if (resultado != null && resultado.indexOf(CODIGO_ERROR) != -1)
			return false;
		return false;
	}

}