/**
* ISBAN M�xico - (c) Banco Santander Central Hispano
* Todos los derechos reservados
* BeanOCE8.java
*
* Control de versiones:
*
* Version Date/Hour        By            Company    Description
* ------- ---------------- ------------- --------   ----------------------------
* 1.0     09-11-2011 19:00 Z225016       BSD        Creacion
*
*/
package mx.altec.enlace.beans;

import java.io.Serializable;

/**
 * Clase BeanOCE8 que almacena los parametros para la transaccion OCE8. 
 * OCE8 - CONSULTA DE USUARIOS VINCULADOS A UN CONTRATO
 */
public class BeanOCE8  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * Codigos y mensajes de error ---------------------------------------------
	 */
	public final static String COD_ERR_SIN_RESPUESTA_390		= "EROC8886";
	public final static String MSG_ERR_SIN_RESPUESTA_390		="No se obtuvo respuesta alguna al realizar la transaccion.";

	public final static String COD_ERR_INSTANCE_DATAACCESS		= "EROC8887"; 
	public final static String MSG_ERR_INSTANCE_DATAACCESS		= "No fue posible crear instancia de DataAccess.";

	public final static String COD_ERR_TIMEOUT					= "EROC8885";
	public final static String MSG_ERR_TIMEOUT					= "No se pudo realizar la transaccion. Tiempo de espera agotado.";

	public final static String COD_ERR_CONSULTA_DATOS			= "EROC8884";
	public final static String MSG_ERR_CONSULTA_DATOS			= "No fue posible ejecutar la transaccion {0}.";

	public final static String COD_ERR_SIN_DATOS				= "EROC8882";
	public final static String MSG_ERR_SIN_DATOS				= "No se obtuvieron datos de la respuesta obtenida de la transaccion.";

	public final static String COD_NO_SE_ENCONTRARON_DATOS		= "OCA0008";
	public final static String MSG_CODE_NO_SE_ENCONTRARON_DATOS	= "NO SE ENCONTRARON DATOS";

	public final static String COD_CTA_ANT_O_NO_EXISTE			= "OCE1108";
	public final static String MSG_CTA_ANT_O_NO_EXISTE			= "CUENTA ANTIGUA NO EXISTE O NO TIENE CUENTA NUEVA";

	public final static String COD_NUM_PER_NO_INFORMADO			="OCE0354";
	public final static String MSG_NUM_PER_NO_INFORMADO			="CAMPO NUMERO DE PERSONA NO INFORMADO";

	public final static String COD_CTA_ERRONEA					="CONTRATOOCE1110";
	public final static String MSG_CTA_ERRONEA					="CUENTA ERRONEA: TECLEE 11 O 20 DIGITOS";	

	public final static String COD_ERR_NO_INICIALIZADO			= "CONTRATO9999999";
	public final static String MSG_ERR_NO_INICIALIZADO 			= "ERROR AL INICIALIZAR EL SERVICIO";
	
	//Avisos
	public final static String COD_MAS_DATOS 					= "CONTRATOOCA8001";
	public final static String MSG_MAS_DATOS 					= "8001 HAY MAS DATOS";
	
	public final static String COD_NO_MAS_DATOS 				= "CONTRATOOCA8000";
	public final static String MSG_NO_MAS_DATOS 				= "NO HAY MAS DATOS";

	/*
	 * Variables para leer y escribir los datos de entrada ---------------------
	 */
	
	private String contrato;
	private String repaginado;
	private String perRepaginado;
	private String cveUsuario;

	/**
	 * Obtiene el numero de contrato (INUMCON).
	 * @return String el numero de contrato.
	 */
	public String getContrato() {
		return contrato;
	}

	/**
	 * Almacena el numero de contrato (INUMCON).
	 * @param String el numero de contrato.
	 */
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	/**
	 * Obtiene el repaginado (IREPGEM).
	 * @return String del repaginado.
	 */
	public String getRepaginado() {
		return repaginado;
	}

	/**
	 * Almacena el repaginado (IREPGEM).
	 * @param String el repaginado.
	 */
	public void setRepaginado(String repaginado) {
		this.repaginado = repaginado;
	}

	/**
	 * Obtiene del numero de usuario para repaginado (INUMPER).
	 * @return String el numero de usuario para repaginado.
	 */
	public String getPerRepaginado() {
		return perRepaginado;
	}

	/**
	 * Almacena del numero de usuario para repaginado (INUMPER).
	 * @param String el numero de usuario para repaginado.
	 */
	public void setPerRepaginado(String perRepaginado) {
		this.perRepaginado = perRepaginado;
	}

	/**
	 * Obtiene la clave de usuario (IUSUARI).
	 * @return String la clave de usuario.
	 */
	public String getCveUsuario() {
		return cveUsuario;
	}

	/**
	 * Almacena la clave de usuario (IUSUARI).
	 * @param String la clave de usuario.
	 */
	public void setCveUsuario(String cveUsuario) {
		this.cveUsuario = cveUsuario;
	}

	/*
	 * Variables para leer y escribir los datos de salida ----------------------
	 */
	private String msgError;
	private String codError;
	private String codCliente;
	private String desCliente;
	private String sam;
	private String fchModificacion;
	private String estBloqueo;
	private String fchBloqueo;

	/**
	 * Obtiene el codigo de error.
	 * @return String el codigo de error.
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * Almacena el mensaje de error.
	 * @param String con el mensaje de error.
	 */
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	/**
	 * Obtiene el mensaje de error.
	 * @return String con el mensaje de error.
	 */
	public String getMsgError() {
		return msgError;
	}

	/**
	 * Almacena el codigo de error.
	 * @param String con el codigo de error.
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * Obtiene el numero de persona (ONUMPER).
	 * @return String el numero de persona.
	 */
	public String getCodCliente() {
		return codCliente;
	}

	/**
	 * Almacena el numero de persona (ONUMPER).
	 * @param String con el numero de persona.
	 */
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	/**
	 * Obtiene la descripcion del cliente (ODESCRI).
	 * @return String la descripcion del cliente.
	 */
	public String getDesCliente() {
		return desCliente;
	}

	/**
	 * Almacena la descripcion del cliente (ODESCRI).
	 * @param String con la descripcion del cliente.
	 */
	public void setDesCliente(String desCliente) {
		this.desCliente = desCliente;
	}

	/**
	 * Obtiene el Indicador de Alta en SAM (OINDSAM).
	 * @return String el Indicador de Alta en SAM.
	 */
	public String getSAM() {
		return sam;
	}

	/**
	 * Almacena el Indicador de Alta en SAM (OINDSAM).
	 * @param String con el Indicador de Alta en SAM.
	 */
	public void setSAM(String sam) {
		this.sam = sam;
	}

	/**
	 * Obtiene la fecha de ultima modificacion (OFECMOD).
	 * @return String la fecha de ultima modificacion.
	 */
	public String getFchModificacion() {
		return fchModificacion;
	}

	/**
	 * Almacena la fecha de ultima modificacion (OFECMOD).
	 * @param String la fecha de ultima modificacion.
	 */
	public void setFchModificacion(String fchModificacion) {
		this.fchModificacion = fchModificacion;
	}

	/**
	 * Obtiene el Estatus de Bloqueo (OSTATUS).
	 * @return String el Estatus de Bloqueo.
	 */
	public String getEstBloqueo() {
		return estBloqueo;
	}

	/**
	 * Almacena el Estatus de Bloqueo (OSTATUS).
	 * @param String con el Estatus de Bloqueo.
	 */
	public void setEstBloqueo(String estBloqueo) {
		this.estBloqueo = estBloqueo;
	}

	/**
	 * Obtiene la fecha de bloqueo (OFECBLO).
	 * @return String la fecha de bloqueo .
	 */
	public String getFchBloqueo() {
		return fchBloqueo;
	}

	/**
	 * Almacena la fecha de bloqueo (OFECBLO).
	 * @param String con la fecha de bloqueo .
	 */
	public void setFchBloqueo(String fchBloqueo) {
		this.fchBloqueo = fchBloqueo;
	}

}
