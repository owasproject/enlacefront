package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

import mx.altec.enlace.bo.TrxGPF2VO;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase administradora para manejar el procedimiento para la transaccion GPF2
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 28, 2011
 */
public class AdmTrxGPF2 extends AdmUsrGL35Base implements Serializable{

	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = -5928166932906317486L;
	
	/**
	 * Constantes para codigo exito
	 */
	private static final String CODIGO_EXITO = "@1";
	
	/**
	 * Constantes para codigo error
	 */
	private static final String CODIGO_ERROR = "@2";
	
	/**
	 * Constante para la cabecera de la trama
	 */
	public static String HEADER = "GPF201981000011O00N2";
	
	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_HEADER = "@DCGPM0F21 P";
	
	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_PAGINACION = "@AVGPA0002";
	
	/**
	 * Constante para reconocer la instancia de la clase
	 */
	private static final TrxGPF2VOFactory FACTORY = new TrxGPF2VOFactory();	
	
	/**
	 * Obtener el valor de FACTORY.
     *
     * @return FACTORY valor asignado.
	 */
	public static TrxGPF2VOFactory getFactoryInstance() {
		return FACTORY;
	}
	
	/**
	 * Objeto contenedor de tipo TrxGPF2VO
	 */
	private TrxGPF2VO gpf2Bean;
	
	/**
	 * Objeto contenedor de beans de tipo TrxGPF2VO
	 */
	private Vector catGpf2Bean;
	
	/**
	 * Variable para indicar si existen mas registros
	 */
	private boolean masPaginacion;
	
	/**
	 * Variable para indicar la llave de paginacion
	 */
	private String llavePaginacion;
	
	/**
	 * Obtener el valor de gpf2Bean.
     *
     * @return gpf2Bean valor asignado.
	 */	
	public TrxGPF2VO getGpf2bean() {
		return gpf2Bean;
	}

	/**
     * Asignar un valor a gpf2Bean.
     *
     * @param gpf2Bean Valor a asignar.
     */
	public void setGpf2bean(TrxGPF2VO beanGpf2) {
		this.gpf2Bean = beanGpf2;
	}	
	
	/**
	 * Obtener el valor de catGpf2Bean.
     *
     * @return catGpf2Bean valor asignado.
	 */	
	public Vector getCatGpf2Bean() {
		return catGpf2Bean;
	}

	/**
     * Asignar un valor a catGpf2Bean.
     *
     * @param catGpf2Bean Valor a asignar.
     */
	public void setCatGpf2Bean(Vector catGpf2Bean) {
		this.catGpf2Bean = catGpf2Bean;
	}

	/**
	 * Obtener el valor de masPaginacion.
     *
     * @return masPaginacion valor asignado.
	 */	
	public boolean getMasPaginacion() {
		return masPaginacion;
	}

	/**
     * Asignar un valor a masPaginacion.
     *
     * @param masPaginacion Valor a asignar.
     */
	public void setMasPaginacion(boolean masPaginacion) {
		this.masPaginacion = masPaginacion;
	}	
	
	/**
	 * Obtener el valor de llavePaginacion.
     *
     * @return llavePaginacion valor asignado.
	 */	
	public String getLlavePaginacion() {
		return llavePaginacion;
	}
	
	/**
     * Asignar un valor a llavePaginacion.
     *
     * @param llavePaginacion Valor a asignar.
     */
	public void setLlavePaginacion(String llavePaginacion) {
		this.llavePaginacion = llavePaginacion;
	}

	/**
	 * Inner Class que implementa la interfaz AdmUsrBuilder
	 * Metodos a implementar: 	build
	 *
	 */
	private static class TrxGPF2VOFactory implements AdmUsrBuilder<AdmTrxGPF2> {
		
		/**
		 * Metodo implementado build para el trato de la trama de salida
		 */
		public AdmTrxGPF2 build(String arg) {
	    	EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build:: Entrando..."
	    			, EIGlobal.NivelLog.DEBUG);
			AdmTrxGPF2 bean = new AdmTrxGPF2();
			TrxGPF2VO beanGpf2 = null;
			StringTokenizer tramaSalida = null;
			String token = "";
			String llavePag = "";
			Vector regsbeanGpf2 = new Vector();
			if(verificaCodigo(arg)){				
		    	EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build:: Codigo exitoso"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion("GPF20000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_PAGINACION);
				if(index != -1){
					bean.setMasPaginacion(true);
				}else{
					bean.setMasPaginacion(false);
				}
				
				index = arg.indexOf(FORMATO_HEADER);
				if(index != -1){
					tramaSalida = new StringTokenizer(arg.substring(arg.indexOf(FORMATO_HEADER), arg.length()),"@");
					EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build:: Cuantos tokens son:"+tramaSalida.countTokens(), EIGlobal.NivelLog.DEBUG);
					while(tramaSalida.hasMoreTokens()){						
						try{
							beanGpf2 = new TrxGPF2VO();
							index = 0;
							token = tramaSalida.nextToken();
							index += FORMATO_HEADER.length()-1;
							beanGpf2.setOIdBaCo(getValor(token, index, 11));
							beanGpf2.setODesCor(getValor(token, index += 11, 105));
							beanGpf2.setOCtry(getValor(token, index += 105, 3));
							beanGpf2.setODesCtry(getValor(token, index += 3, 15));
							beanGpf2.setOTwn(getValor(token, index += 15, 35));						
							
					    	EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build:: Resultados..." +
					    			beanGpf2.toString(), EIGlobal.NivelLog.DEBUG);
					    	regsbeanGpf2.add(beanGpf2);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("AdmTrxGPF2 - Error en la cantidad de registros: ["
									+ e + "]", EIGlobal.NivelLog.ERROR);
						}
					}
				}
			}else{
		    	EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build:: Codigo erroneo"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			//bean.setGpf2bean(beanGpf2);
			llavePag = ((TrxGPF2VO)regsbeanGpf2.lastElement()).getOIdBaCo();
			EIGlobal.mensajePorTrace("AdmTrxGPF2::TrxGPF2VOFactory::build::Llave siguiente iteracion->"+llavePag, EIGlobal.NivelLog.DEBUG);
			bean.setLlavePaginacion(llavePag);
			bean.setCatGpf2Bean(regsbeanGpf2);
			return bean;
		}		
	}
	
	/**
	 * Metodo para verificar el codigo de error
	 * @param resultado
	 * @return
	 */
	public static boolean verificaCodigo(String resultado){
		if (resultado != null && resultado.indexOf(CODIGO_EXITO) != -1)
			return true;
		else if (resultado != null && resultado.indexOf(CODIGO_ERROR) != -1)
			return false;
		return false;
	}

}
