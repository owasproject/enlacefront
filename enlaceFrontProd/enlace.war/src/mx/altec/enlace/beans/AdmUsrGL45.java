package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import mx.altec.enlace.utilerias.Util;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;

//TODO Paginacion
public class AdmUsrGL45 extends GLBase {

	public static final String HEADER = "GL4500771123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrGL45> BUILDER = new GL45Builder();

	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345"; 
	
	private static final String FMT_PAGINACION = "@DCGLM4502 P";  
	
	private static final String FTM_DETALLE = "@DCGLM4501 P";    
    		
	/////////////////////////////////////

	
	private List<ConfigMancBean> usuarios;
	
	public List<ConfigMancBean> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<ConfigMancBean> usuarios) {
		this.usuarios = usuarios;
	}

	public String getUsuarioRepaginado() {
		return usuarioRepaginado;
	}

	public void setUsuarioRepaginado(String usuarioRepaginado) {
		this.usuarioRepaginado = usuarioRepaginado;
	}

	private String usuarioRepaginado = "";
	private String indRepaginado = "";
	
	public String getIndRepaginado() {
		return indRepaginado;
	}

	public void setIndRepaginado(String indRepaginado) {
		this.indRepaginado = indRepaginado;
	}

	private AdmUsrGL45() {		
	}		
	
	public static String tramaConsulta( String cveUsuario,String contrato,String usuario ,String indPag,String usuarioRepag ) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(cveUsuario,    8));		 
		sb.append(rellenar(contrato,     20));
		sb.append(rellenar(usuario,       8));
		sb.append(rellenar(indPag, 		  1));
		sb.append(rellenar(usuarioRepag,  8));
		
		return sb.toString();
	}
	
	/////////////////////////////////////
	
	public static class GL45Builder implements AdmUsrBuilder<AdmUsrGL45> {

		public AdmUsrGL45 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrGL45 gl45 = new AdmUsrGL45();

	        if (string.startsWith(INDICADOR_EXITO)) {	        	        	    
	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<ConfigMancBean> usuarios = new ArrayList<ConfigMancBean>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		ConfigMancBean usuario = new ConfigMancBean();
	        		usuario.setUsuario(string.substring(ind, (ind += 8)));	        			        			        		
	        		usuario.setFirma(string.substring(ind, (ind += 1)));
	        		usuario.setLimite(Util.formateaImporte(string.substring(ind, (ind += 17))));
	        		usuarios.add(usuario);
	        	}      
	        	
	        	gl45.usuarios= usuarios;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		gl45.usuarioRepaginado = string.substring(ind, (ind += 8));
	        		gl45.indRepaginado = "S";
	        	}	        	
	        		        	
	        } else {
	        	
	        	gl45.setCodMsg(getCodigoError(string));
	        	gl45.setMensaje(getMensajeError(string));
	        }

	        return gl45;        	
		}	
	}	
}
