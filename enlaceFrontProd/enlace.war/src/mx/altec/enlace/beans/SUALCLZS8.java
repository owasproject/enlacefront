package mx.altec.enlace.beans;
/**
 * bean principal para la transaccion LZS8
 * @author lespinosa
 * <VC> 21-Ago-2009
 */
public class SUALCLZS8{
	
	private String codErr;
	private String msjErr;
	private String lineaCap;
	private String nombreContrato;
	private String cuentaCargo;
	private String nombreCuentaCargo;
	private String titular;
	private String fecha;
	
	private String estatus;
	private String Referencia;
	
	private String folioSua;
	private String regPatronal;
	private String periodoPago;
	private String importeTotal;
	private String codOperacion;
	private String horaOperacion;
	private String fechaOperacion;
	private String medioPresentacion;
	private boolean validarToken=true;
	private boolean mancomunidad=false;
	private String origenArchivo;
	private String fechaVencimiento;
	private String importeImss;
	private String importeRCV;
	private String importeVivienda;
	private String importeACV;
	
	public boolean getValidarToken() {
		return validarToken;
	}
	public void setValidarToken(boolean validarToken) {
		this.validarToken = validarToken;
	}
	public String getCodOperacion() {
		return codOperacion;
	}
	public void setCodOperacion(String codOperacion) {
		this.codOperacion = codOperacion;
	}
	public String getPeriodoPago() {
		return periodoPago;
	}
	public void setPeriodoPago(String periodoPago) {
		this.periodoPago = periodoPago;
	}
	public String getFolioSua() {
		return folioSua;
	}
	public void setFolioSua(String folioSua) {
		this.folioSua = folioSua;
	}
	public String getCodErr() {
		return codErr;
	}
	public void setCodErr(String codErr) {
		this.codErr = codErr;
	}

	public String getMsjErr() {
		return msjErr;
	}
	public void setMsjErr(String msjErr) {
		this.msjErr = msjErr;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getLineaCap() {
		return lineaCap;
	}
	public void setLineaCap(String lineaCap) {
		this.lineaCap = lineaCap;
	}
	public String getNombreContrato() {
		return nombreContrato;
	}
	public void setNombreContrato(String nombreContrato) {
		this.nombreContrato = nombreContrato;
	}
	public String getReferencia() {
		return Referencia;
	}
	public void setReferencia(String referencia) {
		Referencia = referencia;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getRegPatronal() {
		return regPatronal;
	}
	public void setRegPatronal(String regPatronal) {
		this.regPatronal = regPatronal;
	}
	public String getHoraOperacion() {
		return horaOperacion;
	}
	public void setHoraOperacion(String horaOperacion) {
		this.horaOperacion = horaOperacion;
	}
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public String getMedioPresentacion() {
		return medioPresentacion;
	}
	public void setMedioPresentacion(String medioPresentacion) {
		this.medioPresentacion = medioPresentacion;
	}
	public boolean getMancomunidad() {
		return mancomunidad;
	}
	public void setMancomunidad(boolean mancomunidad) {
		this.mancomunidad = mancomunidad;
	}
	public String getCuentaCargo() {
		return cuentaCargo;
	}
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}
	public String getNombreCuentaCargo() {
		return nombreCuentaCargo;
	}
	public void setNombreCuentaCargo(String nombreCuentaCargo) {
		this.nombreCuentaCargo = nombreCuentaCargo;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getImporteACV() {
		return importeACV;
	}
	public void setImporteACV(String importeACV) {
		this.importeACV = importeACV;
	}
	public String getImporteImss() {
		return importeImss;
	}
	public void setImporteImss(String importeImss) {
		this.importeImss = importeImss;
	}
	public String getImporteRCV() {
		return importeRCV;
	}
	public void setImporteRCV(String importeRCV) {
		this.importeRCV = importeRCV;
	}
	public String getImporteVivienda() {
		return importeVivienda;
	}
	public void setImporteVivienda(String importeVivienda) {
		this.importeVivienda = importeVivienda;
	}
	public String getOrigenArchivo() {
		return origenArchivo;
	}
	public void setOrigenArchivo(String origenArchivo) {
		this.origenArchivo = origenArchivo;
	}
	 

}
