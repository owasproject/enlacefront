package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;

//TODO Paginacion
public class AdmUsrOCE7 extends GLBase {

	public static final String HEADER = "OCE700691123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrOCE7> BUILDER = new OCE7Builder();

	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345"; 
	
	private static final String FMT_PAGINACION = "@DCOCME702 P";  
	
	private static final String FTM_DETALLE = "@DCOCME701 P";    
    		
	/////////////////////////////////////

	
	private List<GLContrato> contratos;
	
	private String contratoRepaginado;
	
	private AdmUsrOCE7() {		
	}		
	
	public List<GLContrato> getContratos() {
		return contratos;
	}	
	
	public String getContratoRepaginado() {
		return contratoRepaginado;
	}
	
	/////////////////////////////////////
	
	public static String tramaConsulta(String numPersona, String cveUsuario, 
			String numContratoRep) {
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(numPersona, 8));
		sb.append(numContratoRep == null || numContratoRep.length() == 0 ? "N" : "S"); 
		sb.append(rellenar(numContratoRep, 20));
		sb.append(rellenar(cveUsuario, 8));
		
		return sb.toString();
	}
	
	/////////////////////////////////////
	
	public static class OCE7Builder implements AdmUsrBuilder<AdmUsrOCE7> {

		public AdmUsrOCE7 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrOCE7 oce7 = new AdmUsrOCE7();

	        if (string.startsWith(INDICADOR_EXITO)) {	        	        	    
	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<GLContrato> contratos = new ArrayList<GLContrato>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		GLContrato contrato = new GLContrato();
	        		contrato.setContrato(string.substring(ind, (ind += 11)));
	        		contrato.setNumPersona(string.substring(ind, (ind += 8)).trim());
	        		contrato.setDesContrato(string.substring(ind, (ind += 40)).trim());
	        		contrato.setEstatusCta(string.substring(ind, (ind += 1)));
	        		contrato.setFechaAlta(string.substring(ind, (ind += 10)));
	        			        		
	        		contratos.add(contrato);
	        	}      
	        	
	        	oce7.contratos = contratos;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		oce7.contratoRepaginado = string.substring(ind, (ind += 20));
	        	}
	        	
	        		        	
	        } else {
	        	
	        	oce7.setCodMsg(getCodigoError(string));
	        	oce7.setMensaje(getMensajeError(string));
	        }

	        return oce7;        	
		}	
	}	
}
