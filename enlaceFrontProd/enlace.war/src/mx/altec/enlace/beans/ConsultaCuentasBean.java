package mx.altec.enlace.beans;

public class ConsultaCuentasBean {

	/**
	 * cveModulo int 
	 */ 
	private int cveModulo;
	
	/**
	 * tipoRecuperacion int 
	 */
	private String tipoRecuperacion;
	
	/**
	 * restric int 
	 */
	private String restric;
	
	/**
	 * cveFacultad String 
	 */
	
	private String cveFacultad;
	
	/**
	 * numCuenta
	 */
	private String numCuenta;
	
	/**
	 * nDescripcion
	 */
	private String nDescripcion;
	
	/**
	 * nProduct
	 */
	private String nProduct;
	
	/**
	 * nSubprod
	 */
	private String nSubprod;
	
	/**
	 * nPerJur
	 */
	private String nPerJur;
	
	/**
	 * tipoRelacCtas
	 */
	private String tipoRelacCtas;
	
	/**
	 * numCuentaSerfin
	 */
	private String numCuentaSerfin;

	/**
	 *  numContrato
	 */
	private String numContrato;
	
	
	
	/**
	 * Metodo para obtener numero de contrato.
	 * @return valor de numero de Contrato
	 */
	public String getNumContrato() {
		return numContrato;
	}
	/**
	 * Metodo para asignar el numero de contrato.
	 * @param numContrato : numContrato
	 */
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}
	
	/**
	 * @return valor de clave modulo 
	 */
	public int getCveModulo() {
		return cveModulo;
	}

	/**
	 * @param cveModulo : valor de clave modulo
	 */
	public void setCveModulo(int cveModulo) {
		this.cveModulo = cveModulo;
	}

	/**
	 * @return valor del tipo de Recuperacion
	 */
	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	/**
	 * @param tipoRecuperacion : valor del tipo de Recuperacion
	 */
	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	/**
	 * @return Valor de la restriccion en el query
	 */
	public String getRestric() {
		return restric;
	}

	/**
	 * @param restric : Valor de la restriccion en el query
	 */
	public void setRestric(String restric) {
		this.restric = restric;
	}

	/**
	 * @return valor de la clave facultad consultada
	 */
	public String getCveFacultad() {
		return cveFacultad;
	}

	/**
	 * @param cveFacultad : valor de la clave facultad consultada
	 */
	public void setCveFacultad(String cveFacultad) {
		this.cveFacultad = cveFacultad;
	}

	/**
	 * @return valor del n�mero de cuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
	 * @param numCuenta : valor del n�mero de cuenta
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * @return descripci�n de la cuenta
	 */
	public String getNDescripcion() {
		return nDescripcion;
	}

	/**
	 * @param descripcion : descripci�n de la cuenta
	 */
	public void setNDescripcion(String descripcion) {
		nDescripcion = descripcion;
	}

	/**
	 * @return valor del producto
	 */
	public String getNProduct() {
		return nProduct;
	}

	/**
	 * @param product : valor del producto
	 */
	public void setNProduct(String product) {
		nProduct = product;
	}

	/**
	 * @return valor del subproducto
	 */
	public String getNSubprod() {
		return nSubprod;
	}

	/**
	 * @param subprod : valor del subproducto
	 */
	public void setNSubprod(String subprod) {
		nSubprod = subprod;
	}

	/**
	 * @return valor del perJur
	 */
	public String getNPerJur() {
		return nPerJur;
	}

	/**
	 * @param perJur : valor del perJur
	 */
	public void setNPerJur(String perJur) {
		nPerJur = perJur;
	}

	/**
	 * @return valor del tipo relaci�n de Cuentas
	 */
	public String getTipoRelacCtas() {
		return tipoRelacCtas;
	}

	/**
	 * @param tipoRelacCtas : valor del tipo relaci�n de Cuentas
	 */
	public void setTipoRelacCtas(String tipoRelacCtas) {
		this.tipoRelacCtas = tipoRelacCtas;
	}

	/**
	 * @return valor del n�mero de Cuenta Serfin
	 */
	public String getNumCuentaSerfin() {
		return numCuentaSerfin;
	}

	/**
	 * @param numCuentaSerfin : valor del n�mero de Cuenta Serfin
	 */
	public void setNumCuentaSerfin(String numCuentaSerfin) {
		this.numCuentaSerfin = numCuentaSerfin;
	}
	
}
