package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getDetalles;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static mx.altec.enlace.beans.NomPreTarjeta.*;

/**
 * @author cgodinez
 *
 */
public class NomPreLM1B extends NomPreLM1Base implements Serializable {
	
	private static final long serialVersionUID = -136727832706803039L;

	/**
	 * Header de entrada para la transaccion LM1B
	 */
	public static  String HEADER = "LM1B00851123451O00N2";
	
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "@DCLMLRET1 P";

	/**
	 * Identificador de detalle
	 */
	private static final String FORMATO_DETALLE = "@DCLMLRET2 P";
	
	/**
	 * Identificador de paginado
	 */
	private static final String FORMATO_PAGINADO = "@DCLMLRET3 P";
	
	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPreLM1BFactory FACTORY = new NomPreLM1BFactory();
	
	/**
	 * Remesa de la que se obtiene el detalle
	 */
	private NomPreRemesa remesa;
	
	/**
	 * Detalle de tarjetas asignadas a la remesa
	 */
	private List<NomPreTarjeta> detalle;
	
	/**
	 * Tarjeta para paginacion
	 */
	private String noTarjetaPag;
	
	/**
	 * Obtiene la remesa
	 * @return Remesa
	 */
	public NomPreRemesa getRemesa() {
		return remesa;
	}

	/**
	 * Asigna la remesa
	 * @param remesa Remesa
	 */
	public void setRemesa(NomPreRemesa remesa) {
		this.remesa = remesa;
	}

	/**
	 * Obtiene el detalle de las tarjetas asignadas a la remesa
	 * @return Lista de tarjetas
	 */
	public List<NomPreTarjeta> getDetalle() {
		return detalle;
	}

	/**
	 * Asigna el detalle de las tarjetas asignadas a la remesa 
	 * @param detalle Lista de tarjetas
	 */
	public void setDetalle(List<NomPreTarjeta> detalle) {
		this.detalle = detalle;
	}
	
	/**
	 * Obtiene el numero de tarjeta con el que se paginara
	 * @return Numero de tarjeta
	 */
	public String getNoTarjetaPag() {
		return noTarjetaPag;
	}
	
	/**
	 * Asigna el siguiente numero de tarjeta
	 * @param noTarjetaPag Numero de tarjeta
	 */
	public void setNoTarjetaPag(String noTarjetaPag) {
		this.noTarjetaPag = noTarjetaPag;
	}
	
	/** Obtiene la instancia del constructor de la transaccion
	 * @return NomPreBuilder
	 */
	public static NomPreLM1BFactory getFactoryInstance() {
		return FACTORY;
	}
	
	/**
	 * Clase usada para generar un elemento NomPreLM1B a partir de la cadena de salida
	 * de la transaccion
	 * @author cgodinez
	 *
	 */
	private static class NomPreLM1BFactory implements NomPreBuilder<NomPreLM1B> { 
	
		/**
		 * Componente encargado de construir el objeto NomPreLM1B a partir de una cadena de entrada
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg Cadena de entrada
		 */
		public NomPreLM1B build(String arg) {
		
			NomPreLM1B bean = new NomPreLM1B();
			
			if (isCodigoExito(arg)) { 
				bean.setCodigoOperacion("LM1B0000");
				bean.setCodExito(true);
				
				int index = arg.indexOf(FORMATO_HEADER);
													
				if (index != -1) {
						
					index += FORMATO_HEADER.length();
					bean.setRemesa(new NomPreRemesa(null, getValor(arg, index + 6, 30), null, Integer.parseInt(getValor(arg, index, 6)))); //TOT-REMESAS				
				}
				
				index = arg.indexOf(FORMATO_PAGINADO);
				
				if (index != -1) {
					
					index += FORMATO_PAGINADO.length();
					
					bean.setNoTarjetaPag(getValor(arg, index + 31, LNG_NO_TARJETA)); //NUM-TARJETA-R
				}
				
				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 53);
	
				if (detalles != null && detalles.length > 0) {					
					
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new ArrayList<NomPreTarjeta>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}												
						
						NomPreTarjeta remesa = new NomPreTarjeta(																			
							getValor(detalle, 0, LNG_NO_TARJETA), 		//NUM-TARJETA-S
							getValor(detalle, 22, LNG_FEC_VIGENCIA), 	//FEC-VIGENCIA-S
							getValor(detalle, 32, LNG_COD_ESTADO), 		//COD-ESTADO-TAR-S
							getValor(detalle, 33, LNG_DESCRIPCION)); 	//DES-ESTADO-TAR-S
	
						bean.getDetalle().add(remesa);
					}
				}
				
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}
	
			return bean;
		}
	}		
}
