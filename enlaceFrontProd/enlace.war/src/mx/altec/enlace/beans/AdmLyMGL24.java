package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.gwt.adminlym.shared.LimitesContrato;


import java.io.Serializable;

import mx.altec.enlace.utilerias.EIGlobal;

public class AdmLyMGL24 extends AdmUsrGL35Base implements Serializable {
	
	private static final long serialVersionUID = -6786737407659700111L;

	public static String HEADER = "GL2400651123451O00N2";
	
	private static final String FORMATO_HEADER = "@DCGLM241 P";
	
	private static final AdmLyMGL24Factory FACTORY = new AdmLyMGL24Factory();
	
	private LimitesContrato limites;
	
	public static AdmLyMGL24Factory getFactoryInstance() {
		return FACTORY;
	}	
	
	private static class AdmLyMGL24Factory implements AdmUsrBuilder<AdmLyMGL24> {

		public AdmLyMGL24 build(String arg) {
			
			AdmLyMGL24 bean = new AdmLyMGL24();
			LimitesContrato lymBean = new LimitesContrato();
			if (isCodigoExito(arg)) {				
				bean.setCodigoOperacion("GL240000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);							
				if (index != -1) {
					index += FORMATO_HEADER.length();
					try {
						EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
						lymBean.setOpcion(getValor(arg, index, 2));
						EIGlobal.mensajePorTrace("==> opcion: [" + lymBean.getOpcion() + "]", EIGlobal.NivelLog.INFO);
						lymBean.setTipoOperacion(getValor(arg, index += 2, 1));
						EIGlobal.mensajePorTrace("==> tipo operacion: [" + lymBean.getTipoOperacion() + "]", EIGlobal.NivelLog.INFO);
						lymBean.setGrupo(getValor(arg, index += 1, 2));
						EIGlobal.mensajePorTrace("==> grupo: [" + lymBean.getGrupo() + "]", EIGlobal.NivelLog.INFO);
						lymBean.setContrato(getValor(arg, index += 2, 11));
						EIGlobal.mensajePorTrace("==> contrato: [" + lymBean.getContrato() + "]", EIGlobal.NivelLog.INFO);
						lymBean.setMonto(getValor(arg, index += 11, 17));
						EIGlobal.mensajePorTrace("==> monto: [" + lymBean.getMonto() + "]", EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("---------------------------------------", EIGlobal.NivelLog.INFO);
					} catch (NumberFormatException nfe) {
						EIGlobal.mensajePorTrace("AdmLyMGL24 - Error en la cantidad de registros: ["
								+ nfe + "]", EIGlobal.NivelLog.ERROR);
					}
				}
			} else if (isCodigoError(arg))  {
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setLimites(lymBean);
			return bean;
		}
		
	}

	public LimitesContrato getLimites() {
		return limites;
	}

	public void setLimites(LimitesContrato limites) {
		this.limites = limites;
	}


}
