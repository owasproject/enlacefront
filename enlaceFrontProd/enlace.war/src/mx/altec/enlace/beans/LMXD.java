package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;

import java.io.Serializable;

/**
 * @author ESC
 * Bean referente a la transacci&oacute;n LMXD RECEPCION REMESA
 */
public class LMXD extends AdmUsrGL35Base implements Serializable {

	/**
	 * Header de entrada para la transaccion LMXC
	 */
	public static  String HEADER = "LMXD00671123451O00N2";
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "LMA0019 OK";

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidad de las remesas
	 */
	private String entidad = "0014";
	/**
	 * Contrato enlace de las remesas
	 */
	private String contratoEnlace;
	/**
	 * Contrato distribuci&oacute;n de las remesas
	 */
	private String ctroDistribucion;
	/**
	 * N&uacute;mero de las remesas
	 */
	private String numeroRemesa;
	/**
	 * Estado de las remesas
	 */
	private String estadoRemesa;
	/**
	 * Constructor de salida de transaccion
	 */
	private static final NomPreLMXDFactory  FACTORY = new NomPreLMXDFactory ();

	public LMXD() {
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public void setContratoEnlace(String contratoEnlace) {
		this.contratoEnlace = contratoEnlace;
	}

	public String getCtroDistribucion() {
		return ctroDistribucion;
	}

	public void setCtroDistribucion(String ctroDistribucion) {
		this.ctroDistribucion = ctroDistribucion;
	}

	public String getNumeroRemesa() {
		return numeroRemesa;
	}

	public void setNumeroRemesa(String numeroRemesa) {
		this.numeroRemesa = numeroRemesa;
	}

	public String getEstadoRemesa() {
		return estadoRemesa;
	}

	public void setEstadoRemesa(String estadoRemesa) {
		this.estadoRemesa = estadoRemesa;
	}

	public static NomPreLMXDFactory getFactoryInstance() {
		return FACTORY;
	}

	/**
	 * Clase usada para generar un elemento LMXC a partir de la cadena de salida
	 * de la transaccion
	 * @author ESC
	 */
	private static class NomPreLMXDFactory  implements AdmUsrBuilder<LMXD> {

		/**
		 * Componente encargado de construir el objeto LMXC a partir de una cadena de entrada
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg Cadena de entrada
		 */
		public LMXD build(String arg) {

			LMXD bean = new LMXD();
			if (isCodigoExito(arg)) {
				if (arg.contains(FORMATO_HEADER)) {
					bean.setCodExito(true);
					bean.setCodigoOperacion("LMXD0000");
				} else {
					bean.setCodExito(false);
					bean.setCodigoOperacion("LMXD0001");
				}
			} else if (isCodigoError(arg))  {
				bean.setCodExito(false);
				bean.setCodigoOperacion(getCodigoError(arg));
			}

			return bean;
		}
	}
}