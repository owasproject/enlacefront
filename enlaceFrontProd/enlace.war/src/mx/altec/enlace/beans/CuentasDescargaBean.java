/**
 * Isban Mexico
 *   Clase: CuentasDescargaBean.java
 *   Descripcion: 
 *
 *   Control de Cambios:
 *   1.0 12/06/2013 Stefanini - Creacion
 *   1.1 11/11/2013 IRF - Modificacion Elementos Lista.
 */
package mx.altec.enlace.beans;

import java.io.Serializable;
import java.util.List;

import mx.altec.enlace.cliente.ws.cfdiondemand.PeriodCFDI;

/**
 * Bean CuentasDescargaBean
 */
public class CuentasDescargaBean implements Serializable {

	/** Serial **/
	private static final long serialVersionUID = 1L;
	/** The property tipoXML for the bean **/
	private String tipoXML;
	/** The property cuenta for the bean **/
	private String cuenta;
	/** The property cuentaBita for the bean **/
	private String cuentaBita;
	/** The property codigoCliente for the bean **/
	private String codigoCliente;
	/** The property formato for the bean **/
	private int formato;
	/** The property folioSeleccionado for the bean **/
	private String folioSeleccionado;
	/** The property periodoSeleccionado for the bean **/
	private String periodoSeleccionado;
	/** The property serie for the bean **/
	private String serie;
	/** The property tipoEdoCta for the bean **/
	private String tipoEdoCta;
	/** The property pais for the bean **/
	private String pais;
	/** The property branch of the bean **/
	private String branch;
	/** The property rfc for the bean **/
	private String rfc;
	/** The property tipo for the bean **/
	private String tipo;
	/** The property descripcion for the bean **/
	private String descripcion;
	/** The property numPeridos for the bean **/
	private Integer numPeridos;
	/** The property codigoRespuesta for the bean **/
	private Integer codigoRespuesta;
	/** The property descripcionCodigo for the bean **/
	private String descripcionCodigo;
	/** The property llaveDescarga for the bean **/
	private String llaveDescarga;
	/** The String List for the Bean **/
	private List<String> periodosAnteriores;
	/** The periodoCFDI List for the Bean **/
	private List<PeriodCFDI> periodCFDI;
	
	/**
	 * @return el numero de cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta el numero de cuenta a establecer
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return el numero de cuentaBita
	 */
	public String getCuentaBita() {
		return cuentaBita;
	}

	/**
	 * @param cuentaBita el numero de cuentaBita a establecer
	 */
	public void setCuentaBita(String cuentaBita) {
		this.cuentaBita = cuentaBita;
	}

	/**
	 * @return el codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}

	/**
	 * @param codigoCliente el codigoCliente a establecer
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	/**
	 * @return el formato
	 */
	public int getFormato() {
		return formato;
	}

	/**
	 * @param formato el formato a establecer
	 */
	public void setFormato(int formato) {
		this.formato = formato;
	}

	/**
	 * @return el folioSeleccionado
	 */
	public String getFolioSeleccionado() {
		return folioSeleccionado;
	}

	/**
	 * @param folioSeleccionado el folioSeleccionado a establecer
	 */
	public void setFolioSeleccionado(String folioSeleccionado) {
		this.folioSeleccionado = folioSeleccionado;
	}

	/**
	 * @return el periodoSeleccionado
	 */
	public String getPeriodoSeleccionado() {
		return periodoSeleccionado;
	}

	/**
	 * @param periodoSeleccionado el periodoSeleccionado a establecer
	 */
	public void setPeriodoSeleccionado(String periodoSeleccionado) {
		this.periodoSeleccionado = periodoSeleccionado;
	}

	/**
	 * @return el serie
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * @param serie el serie a establecer
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * @return el tipoEdoCta
	 */
	public String getTipoEdoCta() {
		return tipoEdoCta;
	}

	/**
	 * @param tipoEdoCta el tipoEdoCta a establecer
	 */
	public void setTipoEdoCta(String tipoEdoCta) {
		this.tipoEdoCta = tipoEdoCta;
	}

	/**
	 * @return el pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais el pais a establecer
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return el branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch el branch a establecer
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return el rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc el rfc a establecer
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return el tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo el tipo a establecer
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return el numPeridos
	 */
	public Integer getNumPeridos() {
		return numPeridos;
	}

	/**
	 * @param numPeridos el numPeridos a establecer
	 */
	public void setNumPeridos(Integer numPeridos) {
		this.numPeridos = numPeridos;
	}

	/**
	 * @return el codigoRespuesta
	 */
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * @param codigoRespuesta el codigoRespuesta a establecer
	 */
	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * @return el descripcionCodigo
	 */
	public String getDescripcionCodigo() {
		return descripcionCodigo;
	}

	/**
	 * @param descripcionCodigo el descripcionCodigo a establecer
	 */
	public void setDescripcionCodigo(String descripcionCodigo) {
		this.descripcionCodigo = descripcionCodigo;
	}

	/**
	 * @return el llaveDescarga
	 */
	public String getLlaveDescarga() {
		return llaveDescarga;
	}

	/**
	 * @param llaveDescarga el llaveDescarga a establecer
	 */
	public void setLlaveDescarga(String llaveDescarga) {
		this.llaveDescarga = llaveDescarga;
	}

	/**
	 * @return el periodosAnteriores
	 */
	public List<String> getPeriodosAnteriores() {
		return periodosAnteriores;
	}

	/**
	 * @param periodosAnteriores el periodosAnteriores a establecer
	 */
	public void setPeriodosAnteriores(List<String> periodosAnteriores) {
		this.periodosAnteriores = periodosAnteriores;
	}

	/**
	 * @return el periodCFDI
	 */
	public List<PeriodCFDI> getPeriodCFDI() {
		return periodCFDI;
	}

	/**
	 * @param periodCFDI el periodCFDI a establecer
	 */
	public void setPeriodCFDI(List<PeriodCFDI> periodCFDI) {
		this.periodCFDI = periodCFDI;
	}

	/**
	 * @return el tipoXML
	 */
	public String getTipoXML() {
		return tipoXML;
	}		
	
	/**
	 * @param tipoXML el tipoXML a establecer
	 */
	public void setTipoXML(String tipoXML) {
		this.tipoXML = tipoXML;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CuentasDescargaBean {"+"tipoXML ["+tipoXML+"]"+
		"\ncuenta ["+cuenta+"]"+
		"\ncuentaBita ["+cuentaBita+"]"+
		"\ncodigoCliente ["+codigoCliente+"]"+
		"\nformato ["+formato+"]"+
		"\nfolioSeleccionado ["+folioSeleccionado+"]"+
		"\nperiodoSeleccionado ["+periodoSeleccionado+"]"+
		"\nserie ["+serie+"]"+
		"\ntipoEdoCta ["+tipoEdoCta+"]"+
		"\npais ["+pais+"]"+
		"\nbranch ["+branch+"]"+
		"\nrfc ["+rfc+"]"+
		"\ntipo ["+tipo+"]"+
		"\ndescripcion ["+descripcion+"]"+
		"\nnumPeridos ["+numPeridos+"]"+
		"\ncodigoRespuesta ["+codigoRespuesta+"]"+
		"\ndescripcionCodigo ["+descripcionCodigo+"]"+
		"\nllaveDescarga ["+llaveDescarga+"]"+"}";
	}
}