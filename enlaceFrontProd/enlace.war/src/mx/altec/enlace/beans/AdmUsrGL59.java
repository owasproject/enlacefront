package mx.altec.enlace.beans;

import java.util.ArrayList;
import java.util.List;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;

public class AdmUsrGL59 extends GLBase {
	
	public static final String HEADER = "GL5900861123451O00N2";
	
	public static final AdmUsrBuilder<AdmUsrGL59> BUILDER = new GL59Builder();
	
	/////////////////////////////////////
	
	private static final String INDICADOR_EXITO = "@112345";
	
	private static final String FMT_PAGINACION = "@DCGLM5930 P";  
	
	private static final String FMT_CABECERA = "@DCGLM5910 P";
	
	private static final String FTM_DETALLE = "@DCGLM5920 P";    
	
	/////////////////////////////////////
	
	private int numeroRegistros;
	
	private String contratoEnlace;
	
	private String usuarioEnlace;	
					
	private List<GLPerfil> perfiles;
	
	private String perfilPaginacion;
	
	private AdmUsrGL59() {		
	}
			
	public String getContratoEnlace() {
		return contratoEnlace;
	}

	public int getNumeroRegistros() {
		return numeroRegistros;
	}

	public String getPerfilPaginacion() {
		return perfilPaginacion;
	}

	public String getUsuarioEnlace() {
		return usuarioEnlace;
	}

	public List<GLPerfil> getPerfiles() {
		return perfiles;
	}
	
	/////////////////////////////////////
	
	public static String tramaConsulta(String contratoEnlace, String usuarioEnlace, String clavePerfilPrototipo, String usuarioOperacion, String perfilPaginacion) {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(rellenar(contratoEnlace, 20));
		sb.append(rellenar(usuarioEnlace, 8));
		sb.append(rellenar(clavePerfilPrototipo, 8));
		sb.append(perfilPaginacion == null || perfilPaginacion.length() == 0 ? "  " : "SI");
		sb.append(rellenar(perfilPaginacion, 8));
		sb.append(rellenar(usuarioOperacion, 8));
		
		return sb.toString();
	}	

	/////////////////////////////////////
	
	public static class GL59Builder implements AdmUsrBuilder<AdmUsrGL59> {

		public AdmUsrGL59 build(String string) {
			
			if (string == null || string.length() == 0) {
	            throw new IllegalArgumentException();
	        }

			AdmUsrGL59 gl59 = new AdmUsrGL59(); 

			if (isCodigoError(string)) {
				
				gl59.setCodMsg(getCodigoError(string));	        	
				gl59.setMensaje(getMensajeError(string));
	        	
				return gl59;
			}

	        if (string.startsWith(INDICADOR_EXITO)) {
	        	        	
	        	//SE PROCESA EL DETALLE
	        	
	        	int ind = 0;        
	        	
	        	List<GLPerfil> perfiles = new ArrayList<GLPerfil>();
	        	
	        	while ((ind = string.indexOf(FTM_DETALLE, ind)) != -1) {
	        		
	        		ind += FTM_DETALLE.length();
	        		
	        		GLPerfil perfil = new GLPerfil();
	        		perfil.setClave(string.substring(ind, (ind += 8)));
	        		perfil.setDescripcion(string.substring(ind, (ind += 80)).trim());
	        		perfil.setNombreComercial(string.substring(ind, (ind += 40)).trim());
	        		
	        		perfiles.add(perfil);
	        	}      
	        	
	        	gl59.perfiles = perfiles;
	        	
	        	//SE PROCESA LA INFORMACION DE PAGINACION
	        	
	        	if ((ind = string.indexOf(FMT_PAGINACION)) != -1) {
	        		
	        		ind += FMT_PAGINACION.length();
	        		
	        		gl59.perfilPaginacion = string.substring(ind, (ind += 8));
	        	}
	        	
	        	//SE PROCESA LA INFORMACION DE CABECERA
	        	
	        	if ((ind = string.indexOf(FMT_CABECERA)) != -1) {
	        		
	        		ind += FMT_CABECERA.length();	        	
	        		
	        		gl59.numeroRegistros = Integer.parseInt(string.substring(ind, (ind += 3)));	        			
	        		gl59.contratoEnlace = string.substring(ind, (ind += 11));
	        		gl59.usuarioEnlace = string.substring(ind, (ind += 8));
	        	}	        
	        }

	        return gl59;        	
		}
	}
}
