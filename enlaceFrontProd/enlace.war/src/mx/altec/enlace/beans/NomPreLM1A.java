package mx.altec.enlace.beans;

import static mx.altec.enlace.beans.NomPreRemesa.LNG_COD_ESTADO;
import static mx.altec.enlace.beans.NomPreRemesa.LNG_DESCRIPCION;
import static mx.altec.enlace.beans.NomPreRemesa.LNG_NO_REMESA;
import static mx.altec.enlace.utilerias.NomPreUtil.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cgodinez
 *
 */
public class NomPreLM1A extends NomPreLM1Base implements Serializable {
	
	private static final long serialVersionUID = 584784739676339828L;

	
	/**
	 * Header de entrada para la transaccion LM1A
	 */
	public static  String HEADER = "LM1A00841123451O00N2";
	
	/**
	 * Identificador de header
	 */
	private static final String FORMATO_HEADER = "@DCLMLREM1 P";
			
	/**
	 * Identificador de detalle
	 */
	private static final String FORMATO_DETALLE = "@DCLMLREM2 P";
	
	/**
	 * Identificador de paginado
	 */
	private static final String FORMATO_PAGINADO = "@DCLMLREM3 P";

	/**
	 * Constructor de salida de transaccion 
	 */
	private static final NomPreLM1AFactory FACTORY = new NomPreLM1AFactory();
		
	/**
	 * Total de remesas
	 */
	private int totalRemesas;

	/**
	 * Numero de remesa para paginar
	 */
	private String noRemesaPag;
	
	/**
	 * Detalle de remesas
	 */
	private List<NomPreRemesa> detalle;	
	
	/**
	 * Obtiene el total de remesas
	 * @return int
	 */
	public int getTotalRemesas() {
		return totalRemesas;
	}
	
	/**
	 * Asigna el numero total de remesas
	 * @param totalRemesas Total de remesas
	 */
	public void setTotalRemesas(int totalRemesas) {
		this.totalRemesas = totalRemesas;
	}

	/**
	 * Obtiene la lista con el detalle de remesas
	 * @return Lista de remesas
	 */
	public List<NomPreRemesa> getDetalle() {
		return detalle;
	}

	/**
	 * Asigna la lista con detalle de remesas
	 * @param detalle Listado de remesas
	 */
	public void setDetalle(List<NomPreRemesa> detalle) {
		this.detalle = detalle;
	}		
	
	/**
	 * Obtiene la remesa a usar en el paginado
	 * @return Numero de remesa
	 */
	public String getNoRemesaPag() {
		return noRemesaPag;
	}

	/**
	 * Asigna el numero de remesa a paginar 
	 * @param noRemesaPag Numero de remesa
	 */
	public void setNoRemesaPag(String noRemesaPag) {
		this.noRemesaPag = noRemesaPag;
	}

	/**
	 * Obtiene la instancia del constructor de la transaccion
	 * @return NomPreBuilder
	 */
	public static NomPreLM1AFactory getFactoryInstance() {
		return FACTORY;
	}
	
	/**
	 * Clase usada para generar un elemento NomPreLM1A a partir de la cadena de salida
	 * de la transaccion
	 * @author cgodinez
	 */
	private static class NomPreLM1AFactory implements NomPreBuilder<NomPreLM1A> {
				
		/** 
		 * Componente encargado de construir el objeto NomPreLM1A a partir de una cadena de entrada
		 * @see mx.altec.enlace.beans.NomPreBuilder#build(java.lang.String)
		 * @param arg Cadena de entrada
		 */
		public NomPreLM1A build(String arg) {
	
			NomPreLM1A bean = new NomPreLM1A();
	
			if (isCodigoExito(arg)) {
								
				bean.setCodExito(true);
				bean.setCodigoOperacion("LM1A0000");
				int index = arg.indexOf(FORMATO_HEADER);
				
				if (index != -1) {
						
					index += FORMATO_HEADER.length();
					bean.setTotalRemesas(Integer.parseInt(getValor(arg, index, 6))); //TOT-REMESAS
				}
				
				index = arg.indexOf(FORMATO_PAGINADO);
				
				if (index != -1) {
					
					index += FORMATO_PAGINADO.length();
					
					bean.setNoRemesaPag(getValor(arg, index + 32, LNG_NO_REMESA)); //NUM-REMESA-PAG
				}
				
				String[] detalles = getDetalles(arg, FORMATO_DETALLE, 41);
	
				if (detalles != null && detalles.length > 0) {
					
					if (bean.getDetalle() == null) {
						
						bean.setDetalle(new ArrayList<NomPreRemesa>());
					}
					
					for (String detalle : detalles) {
						
						if (detalle.length() == 0) {
							continue;
						}			
						
						NomPreRemesa remesa = new NomPreRemesa(
							getValor(detalle, 0, LNG_NO_REMESA), 	//NUM-REMESA-S
							getValor(detalle, 20, LNG_COD_ESTADO), 	//COD-ESTADO-REM-S
							getValor(detalle, 21, LNG_DESCRIPCION), //DES-ESTADO-REM-S
							0);
	
						bean.getDetalle().add(remesa);
					}
				}
				
			} else if (isCodigoError(arg))  {
				
				bean.setCodigoOperacion(getCodigoError(arg));
			}
	
			return bean;
		}
	}		
}
