package mx.altec.enlace.beans;

public class WSLinCapBean{

	/**
	 * L�nea de Captura de la consulta (sin espacios)
	 */
	private String cnsLinCap = "";
	/**
	 * Monto del Pago de la consulta
	 */
	private String cnsMontPag = "";
	/**
	 * Forma de Pago de la consulta
	 */
	private String cnsFormPag = "";
	/**
	 * Id del Canal de la consulta
	 */
	private String cnsIdCanal = "";
	/**
	 * Fecha de la consulta (DD-MM-YYYY sin espacios)
	 */
	private String cnsFecha = "";
	/**
	 * Hora de la consulta (HH:MM:SS sin espacios)
	 */
	private String cnsHora = "";
	/**
	 * Referencia del Canal de la consulta
	 */
	private String cnsRef = "";
	/**
	 * N�mero de Cta Abono de la consulta
	 */
	private String cnsNumCtaAb = "";
	/**
	 * Meses sin Intereses de la consulta
	 */
	private String cnsMesSinInt = "";
	/**
	 * Campo Libre1 de la consulta
	 */
	private String cnsLibre1 = "";

	/**
	 * Estatus de L�nea de Captura de respuesta
	 */
	private String rspEstatusLin = "";
	/**
	 * Nombre del Contribuyente de la respuesta
	 */
	private String rspNomCont = "";
	/**
	 * RFC del Contribuyente de la respuesta
	 */
	private String rspRFCCont = "";
	/**
	 * Campo Libre1 de la respuesta
	 */
	private String rspLibre1 = "";

	/**
	 * Asigna la L�nea de Captura de la consulta
	 * 
	 * @param cnsLinCap String L�nea de Captura de la consulta
	 */
	public void setCnsLinCap(String cnsLinCap) {
		this.cnsLinCap = cnsLinCap;
	}

	/**
	 * Obtiene la L�nea de Captura de la consulta
	 * 
	 * @return String L�nea de Captura de la consulta
	 */
	public String getCnsLinCap() {
		return cnsLinCap;
	}

	/**
	 * Asigna el Monto del Pago de la consulta
	 * 
	 * @param cnsMontPag String Monto del Pago de la consulta
	 */
	public void setCnsMontPag(String cnsMontPag) {
		this.cnsMontPag = cnsMontPag;
	}

	/**
	 * Obtiene el Monto del Pago de la consulta
	 * 
	 * @return String Monto del Pago de la consulta
	 */
	public String getCnsMontPag() {
		return cnsMontPag;
	}

	/**
	 * Asigna la Forma de Pago de la consulta
	 * 
	 * @param cnsFormPag String Forma de Pago de la consulta
	 */
	public void setCnsFormPag(String cnsFormPag) {
		this.cnsFormPag = cnsFormPag;
	}

	/**
	 * Obtiene la Forma de Pago de la consulta
	 * 
	 * @return String Forma de Pago de la consulta
	 */
	public String getCnsFormPag() {
		return cnsFormPag;
	}

	/**
	 * Asigna el Id del Canal de la consulta
	 * 
	 * @param cnsIdCanal String Id del Canal de la consulta
	 */
	public void setCnsIdCanal(String cnsIdCanal) {
		this.cnsIdCanal = cnsIdCanal;
	}

	/**
	 * Obtiene el Id del Canal de la consulta
	 * 
	 * @return String Id del Canal de la consulta
	 */
	public String getCnsIdCanal() {
		return cnsIdCanal;
	}

	/**
	 * Asigna la Fecha de la consulta
	 * 
	 * @param cnsFecha String Fecha de la consulta
	 */
	public void setCnsFecha(String cnsFecha) {
		this.cnsFecha = cnsFecha;
	}

	/**
	 * Obtiene la Fecha de la consulta
	 * 
	 * @return String Fecha de la consulta
	 */
	public String getCnsFecha() {
		return cnsFecha;
	}

	/**
	 * Asigna la Hora de la consulta
	 * 
	 * @param cnsHora String Hora de la consulta
	 */
	public void setCnsHora(String cnsHora) {
		this.cnsHora = cnsHora;
	}

	/**
	 * Obtiene la Hora de la consulta
	 * 
	 * @return String Hora de la consulta
	 */
	public String getCnsHora() {
		return cnsHora;
	}

	/**
	 * Asigna la Referencia del Canal de la consulta
	 * 
	 * @param cnsRef String Referencia del Canal de la consulta
	 */
	public void setCnsRef(String cnsRef) {
		this.cnsRef = cnsRef;
	}

	/**
	 * Obtiene la Referencia del Canal de la consulta
	 * 
	 * @return String Referencia del Canal de la consulta
	 */
	public String getCnsRef() {
		return cnsRef;
	}

	/**
	 * Asigna el N�mero de Cta Abono de la consulta
	 * 
	 * @param cnsNumCtaAb String N�mero de Cta Abono de la consulta
	 */
	public void setCnsNumCtaAb(String cnsNumCtaAb) {
		this.cnsNumCtaAb = cnsNumCtaAb;
	}

	/**
	 * Obtiene el N�mero de Cta Abono de la consulta
	 * 
	 * @return String N�mero de Cta Abono de la consulta
	 */
	public String getCnsNumCtaAb() {
		return cnsNumCtaAb;
	}

	/**
	 * Asigna los Meses sin Intereses de la consulta
	 * 
	 * @param cnsMesSinInt String Meses sin Intereses de la consulta
	 */
	public void setCnsMesSinInt(String cnsMesSinInt) {
		this.cnsMesSinInt = cnsMesSinInt;
	}

	/**
	 * Obtiene los Meses sin Intereses de la consulta
	 * 
	 * @return String Meses sin Intereses de la consulta
	 */
	public String getCnsMesSinInt() {
		return cnsMesSinInt;
	}

	/**
	 * Asigna el Campo Libre1 de la consulta
	 * 
	 * @param cnsLibre1 String Campo Libre1 de la consulta
	 */
	public void setCnsLibre1(String cnsLibre1) {
		this.cnsLibre1 = cnsLibre1;
	}

	/**
	 * Obtiene el Campo Libre1 de la consulta
	 * 
	 * @return String Campo Libre1 de la consulta
	 */
	public String getCnsLibre1() {
		return cnsLibre1;
	}

	/**
	 * Asigna el Estatus de L�nea de Captura de respuesta
	 * 
	 * @param rspEstatusLin String Estatus de L�nea de Captura de respuesta
	 */
	public void setRspEstatusLin(String rspEstatusLin) {
		this.rspEstatusLin = rspEstatusLin;
	}

	/**
	 * Obtiene el Estatus de L�nea de Captura de respuesta
	 * 
	 * @return String Estatus de L�nea de Captura de respuesta
	 */
	public String getRspEstatusLin() {
		return rspEstatusLin;
	}

	/**
	 * Asigna el Nombre del Contribuyente de la respuesta
	 * 
	 * @param rspNomCont String Nombre del Contribuyente de la respuesta
	 */
	public void setRspNomCont(String rspNomCont) {
		this.rspNomCont = rspNomCont;
	}

	/**
	 * Obtiene el Nombre del Contribuyente de la respuesta
	 * 
	 * @return String Nombre del Contribuyente de la respuesta
	 */
	public String getRspNomCont() {
		return rspNomCont;
	}

	/**
	 * Asigna el RFC del Contribuyente de la respuesta
	 * 
	 * @param rspRFCCont String RFC del Contribuyente de la respuesta
	 */
	public void setRspRFCCont(String rspRFCCont) {
		this.rspRFCCont = rspRFCCont;
	}

	/**
	 * Obtiene el RFC del Contribuyente de la respuesta
	 * 
	 * @return String RFC del Contribuyente de la respuesta
	 */
	public String getRspRFCCont() {
		return rspRFCCont;
	}

	/**
	 * Asigna el Campo Libre1 de la respuesta
	 * 
	 * @param rspLibre1 String Campo Libre1 de la respuesta
	 */
	public void setRspLibre1(String rspLibre1) {
		this.rspLibre1 = rspLibre1;
	}

	/**
	 * Obtiene el Campo Libre1 de la respuesta
	 * 
	 * @return String Campo Libre1 de la respuesta
	 */
	public String getRspLibre1() {
		return rspLibre1;
	}

}
