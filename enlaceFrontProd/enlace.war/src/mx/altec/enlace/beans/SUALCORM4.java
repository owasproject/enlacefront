package mx.altec.enlace.beans;

import java.util.List;

/**
 * bean principal para la transaccion LZS8
 * @author lespinosa
 * <VC> 21-Ago-2009
 */
public class SUALCORM4{
	
	public static final int PAGINACION = 40;
	private String codErr;
	private String msjErr;
	private String fechaIni;
	private String fechFin;
	private String opcion;
	private String linSua;
	
	private List<SUALCORM4> consulta;
	//datos de salida
	private String cuentaCargo;
	private String regPatronal;
	private String folioSua;
	private String periodoPago;
	private String lineaCaptura;
	private String importeTotal;
	private String numeroMovimiento;
	private String usuarioRegPago;
	private String timestamp;
	private boolean pagina=false;
	
	public boolean getPagina() {
		return pagina;
	}
	public void setPagina(boolean pagina) {
		this.pagina = pagina;
	}
	public String getCodErr() {
		return codErr;
	}
	public void setCodErr(String codErr) {
		this.codErr = codErr;
	}
	public List<SUALCORM4> getConsulta() {
		return consulta;
	}
	public void setConsulta(List<SUALCORM4> consulta) {
		this.consulta = consulta;
	}
	public String getCuentaCargo() {
		return cuentaCargo;
	}
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}
	public String getFechFin() {
		return fechFin;
	}
	public void setFechFin(String fechFin) {
		this.fechFin = fechFin;
	}
	public String getFolioSua() {
		return folioSua;
	}
	public void setFolioSua(String folioSua) {
		this.folioSua = folioSua;
	}
	public String getImporteTotal() {
		return importeTotal;
	}
	public void setImporteTotal(String importeTotal) {
		this.importeTotal = importeTotal;
	}
	public String getLineaCaptura() {
		return lineaCaptura;
	}
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}
	public String getLinSua() {
		return linSua;
	}
	public void setLinSua(String linSua) {
		this.linSua = linSua;
	}
	public String getMsjErr() {
		return msjErr;
	}
	public void setMsjErr(String msjErr) {
		this.msjErr = msjErr;
	}
	public String getNumeroMovimiento() {
		return numeroMovimiento;
	}
	public void setNumeroMovimiento(String numeroMovimiento) {
		this.numeroMovimiento = numeroMovimiento;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getPeriodoPago() {
		return periodoPago;
	}
	public void setPeriodoPago(String periodoPago) {
		this.periodoPago = periodoPago;
	}
	public String getRegPatronal() {
		return regPatronal;
	}
	public void setRegPatronal(String regPatronal) {
		this.regPatronal = regPatronal;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getUsuarioRegPago() {
		return usuarioRegPago;
	}
	public void setUsuarioRegPago(String usuarioRegPago) {
		this.usuarioRegPago = usuarioRegPago;
	}
	
}
