package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.isCodigoExito;
import mx.altec.enlace.bo.TrxPE80VO;
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

import mx.altec.enlace.utilerias.EIGlobal;

/**
 *  Clase administradora para manejar el procedimiento para la transaccion PE80
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Sep 23, 2010
 */
public class AdmTrxPE80 extends AdmUsrGL35Base implements Serializable {		

	/**
	 * Serial Id de la clase
	 */
	private static final long serialVersionUID = -8010833219323594156L;
	
	/**
	 * Constantes para codigo exito
	 */
	private static final String CODIGO_EXITO = "@1";
	
	/**
	 * Constantes para codigo error
	 */
	private static final String CODIGO_ERROR = "@2";

	/**
	 * Constante para la cabecera de la trama
	 */
	public static String HEADER = "PE8000721000011O00N2";
	
	/**
	 * Constante para reconocer el formato de salida de la transaccion
	 */
	private static final String FORMATO_HEADER = "@DCPEM8010 P";
	
	/**
	 * Constante para reconocer la instancia de la clase
	 */
	private static final TrxPE80VOFactory FACTORY = new TrxPE80VOFactory();	
	
	/**
	 * Obtener el valor de FACTORY.
     *
     * @return FACTORY valor asignado.
	 */
	public static TrxPE80VOFactory getFactoryInstance() {
		return FACTORY;
	}
	
	/**
	 * Objeto contenedor de tipo TrxPE80VO
	 */
	private TrxPE80VO pe80Bean;	
	
	/**
	 * Obtener el valor de pe80Bean.
     *
     * @return pe80Bean valor asignado.
	 */	
	public TrxPE80VO getPe80bean() {
		return pe80Bean;
	}

	/**
     * Asignar un valor a pe80Bean.
     *
     * @param pe80Bean Valor a asignar.
     */
	public void setPe80bean(TrxPE80VO beanPe80) {
		this.pe80Bean = beanPe80;
	}	
		
	/**
	 * Inner Class que implementa la interfaz AdmUsrBuilder
	 * Metodos a implementar: 	build
	 *
	 */
	private static class TrxPE80VOFactory implements AdmUsrBuilder<AdmTrxPE80> {

		/**
		 * Metodo implementado build para el trato de la trama de salida
		 */
		public AdmTrxPE80 build(String arg) {
	    	EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Entrando..."
	    			, EIGlobal.NivelLog.DEBUG);
			AdmTrxPE80 bean = new AdmTrxPE80();
			TrxPE80VO beanPe80 = new TrxPE80VO();
			StringTokenizer tramaSalida = null;
			String token = "";
			EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Trama Salida->" + arg
	    			, EIGlobal.NivelLog.DEBUG);
			if(verificaCodigo(arg)){				
		    	EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Codigo exitoso"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion("PE800000");
				bean.setCodExito(true);
				int index = arg.indexOf(FORMATO_HEADER);							
				if(index != -1){
					tramaSalida = new StringTokenizer(arg.substring(arg.indexOf(FORMATO_HEADER), arg.length()),"@");
					EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Cuantos tokens son:"+tramaSalida.countTokens(), EIGlobal.NivelLog.DEBUG);
					while(tramaSalida.hasMoreTokens()){
						try{ 
							index = 0;
							token = tramaSalida.nextToken();
							index += FORMATO_HEADER.length()-1;
							if(token.substring(19, 21).trim().equals("TI")){
								beanPe80.setPeNumPer(getValor(token, index, 8));
								beanPe80.setPeCalPar(getValor(token, index += 8, 2));
								beanPe80.setPeOrdPar(getValor(token, index += 2, 3));
								beanPe80.setPeCodPro(getValor(token, index += 3, 2));
								beanPe80.setPeCodSub(getValor(token, index += 2, 4));
								beanPe80.setFecBaja (getValor(token, index += 4, 10));
								beanPe80.setPeEstRel(getValor(token, index += 10, 1));
								beanPe80.setPeResInt(getValor(token, index += 1, 5));
								beanPe80.setPeMarPaq(getValor(token, index += 5, 1));
								beanPe80.setPeMotBaj(getValor(token, index += 1, 2));
								beanPe80.setPeTipDoc(getValor(token, index += 2, 2));
								beanPe80.setPeNumDoc(getValor(token, index += 2, 11));
								beanPe80.setPeSecDoc(getValor(token, index += 11, 2));
								beanPe80.setPeNomFan(getValor(token, index += 2, 30));
								beanPe80.setPePriApe(getValor(token, index += 30, 20));
								beanPe80.setPeSegApe(getValor(token, index += 20, 20));
								beanPe80.setPeNomPer(getValor(token, index += 20, 40));
								beanPe80.setPeFecNac(getValor(token, index += 40, 10));
								beanPe80.setPeTipPer(getValor(token, index += 10, 1));
								beanPe80.setPeEstPer(getValor(token, index += 1, 3));
								beanPe80.setPeConPer(getValor(token, index += 3, 3));
								beanPe80.setPeNivAcc(getValor(token, index += 3, 1));
								beanPe80.setPeIndRel(getValor(token, index += 1, 1));
								beanPe80.setPeIndGru(getValor(token, index += 1, 1));
								beanPe80.setPeIndN01(getValor(token, index += 1, 1));
								beanPe80.setPeIndN02(getValor(token, index += 1, 1));
								beanPe80.setPeIndN03(getValor(token, index += 1, 1));
								beanPe80.setPeIndN04(getValor(token, index += 1, 1));
								beanPe80.setPeIndN05(getValor(token, index += 1, 1));
								beanPe80.setPeTipVi(getValor(token, index += 1, 2));
								beanPe80.setPeNomCa(getValor(token, index += 2, 50));
								beanPe80.setPeObse1(getValor(token, index += 50, 100));
								beanPe80.setPeObse2(getValor(token, index += 100, 100));
								beanPe80.setPeNumBl(getValor(token, index += 100, 10));
								beanPe80.setPeNomLo(getValor(token, index += 10, 7));
								beanPe80.setPeNomCo(getValor(token, index += 7, 5));
								beanPe80.setPeCodPo(getValor(token, index += 5, 8));
								beanPe80.setPeRutCa(getValor(token, index += 8, 9));
								beanPe80.setCodPrv(getValor(token, index += 9, 2));
								beanPe80.setPeCodPa(getValor(token, index += 2, 3));
								beanPe80.setPeMarNo(getValor(token, index += 3, 1));
								beanPe80.setPeHStam(getValor(token, index += 1, 26));
								beanPe80.setPeNumTc(getValor(token, index += 26, 16));
								beanPe80.setPeHStamPEDT207(getValor(token, index += 16, 26));		
							}							
					    	EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Resultados..." +
					    			beanPe80.toString(), EIGlobal.NivelLog.DEBUG);
						}catch(Exception e){
							EIGlobal.mensajePorTrace("AdmLyMGL24 - Error en la cantidad de registros: ["
									+ e + "]", EIGlobal.NivelLog.ERROR);
						}
					}
				}
			}else{
		    	EIGlobal.mensajePorTrace("AdmTrxPE80::TrxPE80VOFactory::build:: Codigo erroneo"
		    			, EIGlobal.NivelLog.DEBUG);
				bean.setCodigoOperacion(getCodigoError(arg));
				bean.setMensError(getMensajeError(arg));
			}
			bean.setPe80bean(beanPe80);
			return bean;
		}		
	}
	
	/**
	 * Metodo para verificar el codigo de error
	 * @param resultado
	 * @return
	 */
	public static boolean verificaCodigo(String resultado){
		if (resultado != null && resultado.indexOf(CODIGO_EXITO) != -1)
			return true;
		else if (resultado != null && resultado.indexOf(CODIGO_ERROR) != -1)
			return false;
		return false;
	}
}
