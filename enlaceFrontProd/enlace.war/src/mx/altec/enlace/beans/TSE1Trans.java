package mx.altec.enlace.beans;

import static mx.altec.enlace.utilerias.NomPreUtil.rellenar;
import static mx.altec.enlace.utilerias.NomPreUtil.getCodigoError;
import static mx.altec.enlace.utilerias.NomPreUtil.getMensajeError;

public class TSE1Trans extends GLBase {

	public static final String HEADER = "TSE100521123451O00N2";

	public static final AdmUsrBuilder<TSE1Trans> BUILDER = new TSE1TransBuilder();

	// ///////////////////////////////////

	private static final String INDICADOR_EXITO = "@112345";

	private static final String FTM_DETALLE = "@DCTSME11  P";

	// ///////////////////////////////////

	private String numeroPersona;
	private String claveTransferenciaInterbancaria;
	private String codigoMoneda;
	private String codigoProducto;
	private String codigoSubproducto;
	private String descripcionCuenta;
	private String personaJuridica;
	private String estatusCuenta;
	private String activoInactivo;
	private String razonSocial;

	// ///////////////////////////////////

	public String getNumeroPersona() {
		return numeroPersona;
	}

	public String getClaveTransferenciaInterbancaria() {
		return claveTransferenciaInterbancaria;
	}

	public String getCodigoMoneda() {
		return codigoMoneda;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public String getCodigoSubproducto() {
		return codigoSubproducto;
	}

	public String getDescripcionCuenta() {
		return descripcionCuenta;
	}

	public String getPersonaJuridica() {
		return personaJuridica;
	}

	public String getEstatusCuenta() {
		return estatusCuenta;
	}

	public String getActivoInactivo() {
		return activoInactivo;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	// ///////////////////////////////////

	public static String tramaConsulta(String numeroContrato) {

		StringBuilder sb = new StringBuilder();
		sb.append(rellenar(numeroContrato, 20));
		return sb.toString();
	}

	// ///////////////////////////////////

	public static class TSE1TransBuilder implements AdmUsrBuilder<TSE1Trans> {

		public TSE1Trans build(String string) {

			if (string == null || string.length() == 0) {
				throw new IllegalArgumentException();
			}

			TSE1Trans tse1 = new TSE1Trans();

			if (!string.startsWith(INDICADOR_EXITO)) {

				tse1.setCodMsg(getCodigoError(string));

				tse1.setMensaje(getMensajeError(string));

				return tse1;
			}

			// SE PROCESA EL REGISTRO RETORNADO

			int ind = -1;

			// SE PROCESA LA INFORMACION DE CABECERA

			if ((ind = string.indexOf(FTM_DETALLE)) != -1) {

				ind += FTM_DETALLE.length();
				
				tse1.numeroPersona = string.substring(ind, (ind += 8));
				tse1.claveTransferenciaInterbancaria = string.substring(ind, (ind += 18));
				tse1.codigoMoneda = string.substring(ind, (ind += 3));
				tse1.codigoProducto = string.substring(ind, (ind += 2));
				tse1.codigoSubproducto = string.substring(ind, (ind += 4));
				tse1.descripcionCuenta = string.substring(ind, (ind += 25)).trim();
				tse1.personaJuridica = string.substring(ind, (ind += 1));
				tse1.estatusCuenta = string.substring(ind, (ind += 1));
				tse1.activoInactivo = string.substring(ind, (ind += 1));
				tse1.razonSocial = string.substring(ind, (ind += 82)).trim();							
			}

			return tse1;
		}
	}
}
