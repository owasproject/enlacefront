package mx.altec.enlace.nuevopagoimpuestos;

//import java.io.IOException;

/*
 * Proyecto : Mejoras de L�nea de Captura
 * Modific� : Sadda� Germ�n Salazar Cu�llar (SGSC)
 * Fecha	: Febrero 2014
 * Objetivo : Redirecciona a la pantalla de "AvisoImpuestos.jsp" el pago de impuestos de:
 *				Provivional: identificado por la "opcion" = "0"
 */
import java.io.*;
import javax.servlet.http.*;
import mx.altec.enlace.bo.BndPagRefSatBO;
/*
 * SGSC
 */

import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.IEnlace;

/*
 * Proyecto : Mejoras de L�nea de Captura
 * Modific� : Sadda� Germ�n Salazar Cu�llar (SGSC)
 * Fecha	: Febrero 2014
 * Objetivo : Redirecciona a la pantalla de "AvisoImpuestos.jsp" el pago de impuestos de:
 *				Provivional: identificado por la "opcion" = "0"
 */
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.utilerias.Global;
/*
 * SGSC
 */

public class ValidaRFC extends BaseServlet{

/**
 * Process the HTTP doGet request.
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @throws IOException returns the Input/Output Exception
 * @throws ServletException returns the exception that caused this servlet exception
 */	
	public void doGet( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}

/**
 * Process the HTTP doPost request.
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @throws IOException returns the Input/Output Exception
 * @throws ServletException returns the exception that caused this servlet exception
 */	
	public void doPost( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		defaultAction( req, res );
	}

/**
 * defaultAction :
 * metodo principal de la clase
 * @param req HttpServletRequest
 * @param res HttpServletResponse
 * @throws ServletException returns the exception that caused this servlet exception
 * @throws IOException returns the Input/Output Exception
 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		
		boolean sesionvalida = SesionValida(req, res);
		
		if (sesionvalida) {

/*
 * Proyecto : Mejoras de L�nea de Captura
 * Modific� : Sadda� Germ�n Salazar Cu�llar (SGSC)
 * Fecha	: Febrero 2014
 * Objetivo : Redirecciona a la pantalla de "AvisoImpuestos.jsp" el pago de impuestos de:
 *				Provivional: identificado por la "opcion" = "0"
 */
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			if(session!=null) {
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu",	session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Pago Provisional de Impuestos","Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; Provisional","s60010h",req) );
				req.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);
			}
/*
 * SGSC
 */

			String opcion =  ( String ) req.getParameter("OPCION");
//			if (opcion.equals("0")) {
			if ("0".equals(opcion)) {
/*
 * Proyecto : Mejoras de L�nea de Captura
 * Modific� : Sadda� Germ�n Salazar Cu�llar (SGSC)
 * Fecha	: Febrero 2014
 * Objetivo : Redirecciona a la pantalla de "AvisoImpuestos.jsp" el pago de impuestos de:
 *				Provisional: identificado por la "opcion" = "0"
 */
				BndPagRefSatBO bndPRSBo = new BndPagRefSatBO();

				if (!("1".equals(bndPRSBo.consBndPagRefSAT("001"))))
				{
					evalTemplate("/jsp/nuevopagoimpuestos/AvisoImpuestos.jsp", req, res);
				} else {
					despliegaPaginaError("Estimado cliente por disposici�n oficial a partir del d�a 1� de Febrero 2012, la SHCP determin� que los pagos que realizaban las personas morales por NEPE deber�n ser presentados por l�nea de captura.<BR><BR><BR>", "Pago de Impuestos Federales","Servicios &gt; Impuestos federales &gt; Pagos", "s00600h",req, res);
					evalTemplate("/jsp/nuevopagoimpuestos/ValidaRFC.jsp", req, res);
				}

/*
 * SGSC
 */			}
		}
		else {
			return;
		}
	}
}
