/*************************************************************************
 * Fecha de �ltima modificacin : 22 - Dic - 05
 * Proyecto:200525200 Recaudacin de Impuestos Federales V4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: Septiembre 2005
 * Modificacin: Adaptaciones correspondientes a las
 * especificaciones t�cnicas del SAT versin 4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificacin: Se elimin el cdigo muerto
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificacin: Se modifico la funcin ordenaDiSub
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 04 - Abr - 06
 * Modificacin: Se ordena la trama para Diesel Marino y Subsidios,
 * antes de guardarse en la base de datos.
 ************************************************************************/

package mx.altec.enlace.nuevopagoimpuestos;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BndPagRefSatBO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.sql.SQLException;

import java.io.IOException;
import java.util.*;

public class NuevoPagoImpuestos extends mx.altec.enlace.servlets.BaseServlet {

    /** EOF_PIPE M/021556 Constante que evita repetir cadena */
    static final String EOF_PIPE = "EOF|";

    /** INDEX_02 M/021556 Constante que evita repetir cadena */
    static final String INDEX_02 = "02=";

    /** INDEX_35 M/021556 Constante que evita repetir cadena */
    static final String INDEX_35 = "35=";

    /** INDEX_36 M/021556 Constante que evita repetir cadena */
    static final String INDEX_36 = "36=";

    /** INDEX_37 M/021556 Constante que evita repetir cadena */
    static final String INDEX_37 = "37=";

    private String tipoOperacion = "";

    //********************CSA*****************************
    //Variables creadas para controlar la codificacion UTF-8 e ISO-8859-1
    //para controlar los problemas con las � y otros caracteres
    private static final String nomAttTramaDes = "tramaDesordenadaCod";
    private static final String nomAttParam = "paramCod";
    private static final String nomAttParamDat = "paramDatosImpCod";
    private static final Character[] caracteres = {'�','�','�','�','�','�','�','�','�','�','�','�'};
    private static final String UTF8 = "UTF-8";
    private static final String ISO = "ISO-8859-1";
    //**********************************

    public static final String CLASS_NAME = NuevoPagoImpuestos.class.getName();
	  String cadOriginal; //VSWF se guarda la cadena original para imprimirse en el recibo de pago
	  String cadXML;

    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {
        defaultAction(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {
        defaultAction(req, res);
    }

    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {

    	boolean sesionvalida = SesionValida(req, res);
        String contrato = "";
        String usuario = "";
        String clavePerfil = "";
        short sucOpera = (short) 981;
        String mensaje = "";
        GregorianCalendar calHoy = new GregorianCalendar();
        javax.servlet.http.HttpSession sess = req.getSession();

        BaseResource session = (BaseResource) sess.getAttribute("session");



    	//-----------------CSA-------------------
        //----------------------------------------
        String param = null;
        String paramDatosImp = null;
        String tramaDesordenada = null;
        inicializarParametros(req,res,sess);
        param = sess.getAttribute(nomAttParam) == null?null:sess.getAttribute(nomAttParam).toString();
        paramDatosImp = sess.getAttribute(nomAttParamDat)==null?null:sess.getAttribute(nomAttParamDat).toString();
        tramaDesordenada = sess.getAttribute(nomAttTramaDes)==null?null:sess.getAttribute(nomAttTramaDes).toString();
    	EIGlobal.mensajePorTrace("El valor de param desde sesion: "+ param, EIGlobal.NivelLog.INFO);
    	EIGlobal.mensajePorTrace("El valor de paramDatosImp desde sesion: "+ paramDatosImp, EIGlobal.NivelLog.INFO);
    	EIGlobal.mensajePorTrace("El valor de tramaDesordenada desde sesion: "+ tramaDesordenada, EIGlobal.NivelLog.INFO);
    	//-------------------------------------------------------------
    	//-------------------------------------------------------------
		// VSWF - recupera la trama enviada por el applet
	    if(req.getParameter("paramXML")!=null && !"".equals(req.getParameter("paramXML")) && !"null".equals(req.getParameter("paramXML"))){
	    	this.cadXML = req.getParameter("paramXML");
	    }



        String tipopago = "";
        tipopago = req.getParameter("tipopago");
        if (tipopago == null) {
            tipopago = "";
        }

		/*
		 * Proyecto	: Mejoras de L�nea de Captura
		 * Modific�	: Sadda� Germ�n	Salazar	Cu�llar	(SGSC)
		 * Fecha	: Febrero 2014
		 * Objetivo	: Redirecciona a la	pantalla de	"AvisoImpuestos.jsp" los pagos de impuestos	de:
		 *				Del	Ejercicio: identificado	por	el "tipopago" =	"005"
		 *				Entidades Federativas: identificado	por	el "tipopago" =	"012"
		 */
		if (sesionvalida)
		{
			if(session!=null) {
				BndPagRefSatBO bndPRSBo = new BndPagRefSatBO();

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu",	session.getFuncionesDeMenu());
				req.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);

				if("005".equals(tipopago)){
					if (!("1".equals(bndPRSBo.consBndPagRefSAT(tipopago))))
					{
						req.setAttribute("Encabezado", CreaEncabezado("Pago de Impuestos del Ejercicio","Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; Del Ejercicio","s60010h",req) );
						evalTemplate("/jsp/nuevopagoimpuestos/AvisoImpuestos.jsp", req,	res);
					}
				}
				else if("012".equals(tipopago)){
					if (!("1".equals(bndPRSBo.consBndPagRefSAT(tipopago))))
					{
						req.setAttribute("Encabezado", CreaEncabezado("Pago de Impuestos de Entidades Federativas","Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; Entidades Federativas","s60010h",req) );
						evalTemplate("/jsp/nuevopagoimpuestos/AvisoImpuestos.jsp", req,	res);
					}
				}
			}
		}
		/*
		 * SGSC
		 */

        /*
         * VSWF
         */
        BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
        BitaTransacBean bt = new BitaTransacBean();
        /*
         * VSWF
         */


        /******************************************Inicia validacion OTP**************************************/
		String valida = req.getParameter ( "valida" );
		if(validaPeticion( req,  res,session,sess,valida)){
			EIGlobal.mensajePorTrace("\n\n\n EN validacion OTP \n\n\n", EIGlobal.NivelLog.INFO);
		}
		/******************************************Termina validacion OTP**************************************/
		else
		{
			// VSWF Sube las tramas a sesin y las almacena en variables en lugar de los request, solo para DPA's
  			if("010".equals(tipopago)){

  				if(sess.getAttribute("cadParam") == null || (sess.getAttribute("cadParam") != null && "null".equals(sess.getAttribute("cadParam"))) || (sess.getAttribute("cadParam") != null && "".equals(sess.getAttribute("cadParam")))){
  					if(param!= null && !"null".equals(param)  && !"".equals(param)){
  						sess.setAttribute("cadParam", param);
  					}
  				}

  				if(sess.getAttribute("cadParamImp") == null || (sess.getAttribute("cadParamImp") != null && "null".equals(sess.getAttribute("cadParamImp"))) || (sess.getAttribute("cadParamImp") != null && "".equals(sess.getAttribute("cadParamImp")))){
  					if(paramDatosImp != null && !"null".equals(paramDatosImp)  && !"".equals(paramDatosImp)){
  						sess.setAttribute("cadParamImp", paramDatosImp);
  					}
  				}


  				if(sess.getAttribute("cadTramaDesordenada") == null || (sess.getAttribute("cadTramaDesordenada") != null && "null".equals(sess.getAttribute("cadTramaDesordenada"))) || (sess.getAttribute("cadTramaDesordenada") != null && "".equals(sess.getAttribute("cadTramaDesordenada")))){
  					if(tramaDesordenada != null && !"null".equals(tramaDesordenada)  && !"".equals(tramaDesordenada)){
  						sess.setAttribute("cadTramaDesordenada", tramaDesordenada);
  					}
  				}
  			}
  			// VSWF

	        EIGlobal insEIGlobal = new EIGlobal(session, getServletContext(), this); //EIGlobal(BaseResource
	        // bs,
	        // ServletContext
	        // con,
	        // BaseServlet
	        // basS)
	        if (sesionvalida) {
	            String ventana = req.getParameter("ventana");
	            if (ventana == null)
	                ventana = " ";
	            //System.out.println("NuevoPagoImpuestos: ventana = " + ventana);
	            contrato = session.getContractNumber();
	            sucOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
	            usuario = session.getUserID8();
	            clavePerfil = session.getUserProfile();
	            if (clavePerfil == null)
	                clavePerfil = usuario;
	            //System.out.println("NuevoPagoImpuestos: tipopago = " + tipopago);
	            if ("001".equals(tipopago)) {
	                //System.out.println("NuevoPagoImpuestos: Pago Provicional Execute obteniendo parametros");
	                //System.out.println("NuevoPagoImpuestos: Pago Provicional Execute contrato usuario clavePerfil");
	                //System.out.println("NuevoPagoImpuestos: Pago Provicional Execute " + contrato + " " + usuario + " " + clavePerfil);
	            } else {
	                //System.out.println("NuevoPagoImpuestos: Pago del Ejercicio Execute obteniendo parametros");
	                //System.out.println("NuevoPagoImpuestos: Pago del ejercicio Execute " + contrato + " " + usuario + " " + clavePerfil);
	            }
//          TODO: BIT CU 4051,4061,4071,4081. EL cliente entra al flujo.
            /*
     		 * VSWF ARR -I
     		 */
	        if ("ON".equals(Global.USAR_BITACORAS) && req.getParameter(BitaConstants.FLUJO)!=null){
            try{
    			bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
    			bt = (BitaTransacBean)bh.llenarBean(bt);
    			if ((req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
    		        bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_ENTRA);
    			else
    				if((req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
    					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_ENTRA);
    				else
    					if((req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
    						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_ENTRA);
    					else
    						if((req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
    							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_ENTRA);
    			bt.setContrato(contrato);
    			BitaHandler.getInstance().insertBitaTransac(bt);
    			}catch (SQLException e){
    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    			}catch(Exception e){
    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    			}

	        }
     		/*
     		 * VSWF ARR -F
     		 */
	            if ("0".equals(ventana)) {
	                //Presentamos los frames y el Applet del Sat para enviar la
	                // trama
	                session.setModuloConsultar(IEnlace.MSua_envio);
	                sess.setAttribute("tipopago", tipopago);
	                /*
	                 * Modificado ECamacho 13/10/2003 Si satTipoPago tiene un valor
	                 * previo, lo remueve SINAPSIS
	                 */
	                if (req.getSession().getAttribute("satTipoPago") != null)
	                    req.getSession().removeAttribute("satTipoPago");
	                /* FINALIZA C�DIGO SINAPSIS */
	                req.getSession().setAttribute("satTipoPago", tipopago);
	                presentaApplet(req, res);
	            } else if ("1".equals(ventana))
				{
	                /*
					 * Modificado ECamacho 13/10/2003 Si tipoPago tiene un valor
					 * previo, lo remueve SINAPSIS
					 */
	                if (sess.getAttribute("tipopago") != null)
	                    sess.removeAttribute("tipopago");
	                /* FINALIZA C�DIGO SINAPSIS */
	                sess.setAttribute("tipopago", tipopago);
	                // Aqui Inicia el Codigo Q1073 Getronics
	                //System.out.println("NuevoPagoimpuestos: TipoPago = :" + tipopago);
	                //System.out.println("NuevoPagoimpuestos: ventana = " + ventana);
	                //System.out.println("NuevoPagoimpuestos: cuenta = " + req.getParameter("cuenta"));
	                //System.out.println("NuevoPagoimpuestos: param = " + req.getParameter("param"));
	                //System.out.println("NuevoPagoimpuestos: paramImp = " + req.getParameter("paramImp"));
	                //System.out.println("NuevoPagoimpuestos: tramaDesordenada = " + req.getParameter("tramaDesordenada"));
	                //System.out.println("NuevoPagoimpuestos: refmancomunidad = " + req.getParameter("refmancomunidad"));
	                //System.out.println("NuevoPagoimpuestos: importetotal = " + req.getParameter("importetotal"));

					EIGlobal.mensajePorTrace("NuevoPagoimpuestos --> defaultAction - Trama desordenada 2: [" + tramaDesordenada + "]", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("NuevoPagoimpuestos --> verificado: [" + req.getParameter("verificado") + "]", EIGlobal.NivelLog.INFO);
	                if (req.getParameter("verificado") == null
	                        || (req.getParameter("verificado") != null && !"1".equals(req.getParameter("verificado")))) {
	                    req.setAttribute("ventana", ventana);
	                    req.setAttribute("cuenta",
	                            req.getParameter("cuenta") != null ? req
	                                    .getParameter("cuenta") : "");
						req.setAttribute("param",
								param != null ? param : "");


						//CSA ************************************************
						String paramImp = paramDatosImp;//req.getParameter("paramImp");
						if(paramImp!= null){
							//EIGlobal.mensajePorTrace("CSA-Extrayendo 1era vez :"+paramImp, EIGlobal.NivelLog.INFO);
							//EIGlobal.mensajePorTrace("CSA-Extrayendo UTF-8 - UTF-8:"+new String(paramImp.getBytes("UTF-8"),"UTF-8"), EIGlobal.NivelLog.INFO);
							//EIGlobal.mensajePorTrace("CSA-Extrayendo 1ISO-8  - UTF-8:"+new String(paramImp.getBytes("ISO-8859-1"),"UTF-8"), EIGlobal.NivelLog.INFO);
							//EIGlobal.mensajePorTrace("CSA-Extrayendo UTF-8 - ISO:"+new String(paramImp.getBytes("UTF-8"),"ISO-8859-1"), EIGlobal.NivelLog.INFO);
							//EIGlobal.mensajePorTrace("CSA-Extrayendo 1ISO-8  - ISO:"+new String(paramImp.getBytes("ISO-8859-1"),"ISO-8859-1"), EIGlobal.NivelLog.INFO);

							if(esUTF8(paramImp)){
								paramImp = convertirUTF8ISO(paramImp);//new String(paramImp.getBytes("ISO-8859-1"),"UTF-8");//convertirEnieUTF8ISO(paramImp);
							}
						}else{
							paramImp = "";
						}
						req.setAttribute("paramImp",paramImp);
						//CSA****************************

						if ((req.getParameter("encoded") != null) && (new Boolean((String)req.getParameter("encoded")).booleanValue())) {
							//CSA *************************************
							if(esUTF8(tramaDesordenada)){
								req.setAttribute("tramaDesordenada", convertirUTF8ISO(tramaDesordenada)); //tramaDesordenada != null ?
										//new String(tramaDesordenada.getBytes("ISO-8859-1"),"UTF-8") : "");
							}else{
							req.setAttribute("tramaDesordenada", tramaDesordenada != null ?tramaDesordenada : "");
							}
							//****************************************
						} else {
		                    req.setAttribute("tramaDesordenada", tramaDesordenada != null ?tramaDesordenada : "");
						}


	                    req.setAttribute("refmancomunidad", req
	                            .getParameter("refmancomunidad") != null ? req
	                            .getParameter("refmancomunidad") : "");
	                    req.setAttribute("importetotal", req
	                            .getParameter("importetotal") != null ? req
	                            .getParameter("importetotal") : "");
	                    req.setAttribute("tipopago", tipopago);
	                    // Validaci?n contra Tuxedo

						String tramaBackEnd = "";
						if ((req.getParameter("encoded") != null) && (new Boolean((String)req.getParameter("encoded")).booleanValue())) {
							//CSA ******************************
								tramaBackEnd = armaTramaBackEnd(esUTF8(param)?convertirUTF8ISO(param):param,esUTF8(tramaDesordenada)?convertirUTF8ISO(tramaDesordenada):tramaDesordenada,tipopago);
								//new String (param.getBytes("ISO-8859-1"),"UTF-8"), new String (tramaDesordenada.getBytes("ISO-8859-1"),"UTF-8"), tipopago);
							//CSA --------------------------------
						} else {
							tramaBackEnd = armaTramaBackEnd(param, tramaDesordenada, tipopago);
						}
						EIGlobal.mensajePorTrace("NuevoPagoimpuestos - defaultAction - tramaBackEnd: [" + tramaBackEnd + "]", EIGlobal.NivelLog.INFO);
	            		/*
						* VSWF - LPG
						* 04 - Abr - 2006
						* Se ordena la trama para Diesel Marino y Subsidios, antes de guardarse en la base de datos.
						*/
						String tipoPago = req.getParameter("tipopago");
						if(!"004".equals(tipoPago) && !"010".equals(tipoPago)) {
							tramaBackEnd = ordenaDiSub(tramaBackEnd);
	                    }

	                    //System.out.println("NuevoPagoImpuestos.defaultAction: tramaBackEnd = " + tramaBackEnd);
	                    String codError = "";
	                    if (contrato != null && usuario != null
	                            && req.getParameter("cuenta") != null) {
	                        codError = llamaServicioVerificaPago(contrato, usuario,
	                                clavePerfil, sess, tramaBackEnd, tipopago, req,
	                                res);
	                        EIGlobal.mensajePorTrace( "-----------METODO llamaServicioVerificaPago--a->"+ codError, EIGlobal.NivelLog.INFO);
	                    }
	                    if (codError.startsWith("IMPV0000"))
	                        req.setAttribute("estatus", "1");
	                    else if (codError.startsWith("IMPV9999")) {
	                        req.setAttribute("estatus", "2");
	                        req.setAttribute("MenuPrincipal", session.getStrMenu());
	                        req.setAttribute("newMenu", session
	                                .getFuncionesDeMenu());
	                        req
	                                .setAttribute(
	                                        "Encabezado",
	                                        CreaEncabezado(
	                                                "Nuevo Esquema de Pago de Impuestos",
	                                                "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
	                                                "s25570h", req));
                        //TODO: BIT CU 4051,4061,4071,4081 C4
                        /*
                 		 * VSWF ARR -I
                 		 */
	                        if ("ON".equals(Global.USAR_BITACORAS)){

                        	try{
                        		String importe =req.getParameter("importetotal");
                        		if(importe==null||"".equals(importe));
                        			importe ="0.00";

                			bt = (BitaTransacBean)bh.llenarBean(bt);
                			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
                				bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
                			else
                				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
                					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
                				else
                					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
                						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
                					else
                						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
                							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);

                			bt.setContrato(contrato);
                			if(codError!=null){
                				 if("OK".equals(codError.substring(0,2))){
                					   bt.setIdErr("IMPV0000");
                				 }else if(codError.length()>8){
                					  bt.setIdErr(codError.substring(0,8));
                				 }else{
                					  bt.setIdErr(codError.trim());
                				 }
                				}



                			String cuenta = (String)req.getParameter("cuenta");

                			if (cuenta!=null){
                				cuenta = cuenta.trim();
                				int posPipe = cuenta.indexOf("|");
                				if (posPipe>0){
                					cuenta = cuenta.substring(0, posPipe);
                				}

                				if (cuenta.length()<20){
                					bt.setCctaOrig(cuenta);
                				}else{
                					bt.setCctaOrig(cuenta.substring(0,20));
                				}

                			}

                			bt.setImporte(Double.parseDouble(importe));
                			if (tipoOperacion!=null){
                				bt.setServTransTux(tipoOperacion);
                			}else{
								bt.setServTransTux("VAPS");
							}

                			BitaHandler.getInstance().insertBitaTransac(bt);
                			}catch(SQLException e){
                				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                			}catch (Exception e){
                				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                			}

	                        }
                 		/*
                 		 * VSWF ARR -F
                 		 */
	                        evalTemplate("/jsp/PMVerificadorPI.jsp", req, res);
	                        return;
	                    } else {
	                        req.setAttribute("estatus", "0");
	                        req.setAttribute("MenuPrincipal", session.getStrMenu());
	                        req.setAttribute("newMenu", session
	                                .getFuncionesDeMenu());
	                        req
	                                .setAttribute(
	                                        "Encabezado",
	                                        CreaEncabezado(
	                                                "Nuevo Esquema de Pago de Impuestos",
	                                                "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
	                                                "s25570h", req));
//                      TODO: BIT CU 4051,4061,4071,4081 C4
                        /*
                 		 * VSWF ARR -I
                 		 */
	                        if ("ON".equals(Global.USAR_BITACORAS)){
                        try{
                        	String importe =req.getParameter("importetotal");
                    		if(importe==null||importe=="")
                    			importe ="0.00";

            			bt = (BitaTransacBean)bh.llenarBean(bt);
            			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
            				bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
            			else
            				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
            					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
            				else
            					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
            						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
            					else
            						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
            							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);
            			bt.setContrato(session.getContractNumber());
            			if(codError!=null){
           				 if("OK".equals(codError.substring(0,2))){
           					   bt.setIdErr("IMPV0000");
           				 }else if(codError.length()>8){
           					  bt.setIdErr(codError.substring(0,8));
           				 }else{
           					  bt.setIdErr(codError.trim());
           				 }
           				}


            			String cuenta = (String)req.getParameter("cuenta");

            			if (cuenta!=null){
            				cuenta = cuenta.trim();
            				int posPipe = cuenta.indexOf("|");
            				if (posPipe>0){
            					cuenta = cuenta.substring(0, posPipe);
            				}

            				if (cuenta.length()<20){
            					bt.setCctaOrig(cuenta);
            				}else{
            					bt.setCctaOrig(cuenta.substring(0,20));
            				}

            			}

            			bt.setImporte(Double.parseDouble(importe));

            			if (tipoOperacion!=null){
                				bt.setServTransTux(tipoOperacion.trim());
                			}else{
								bt.setServTransTux("VAPS");
							}

            			BitaHandler.getInstance().insertBitaTransac(bt);
            			}catch(SQLException e){
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}catch (Exception e){
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}
	                        }
                 		/*
                 		 * VSWF ARR -F
                 		 */
	                        evalTemplate("/jsp/PMVerificadorPI.jsp", req, res);
	                        return;
	                    }
	                }
	                // Aqui Termina el Codigo Q1073 Getronics
	                
	                ///////////////////--------------challenge--------------------------
					String validaChallenge = req.getAttribute("challngeExito") != null 
						? req.getAttribute("challngeExito").toString() : "";
					
					EIGlobal.mensajePorTrace("--------------->NuevoPagoImpuestos >validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);
					
					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
						EIGlobal.mensajePorTrace("--------------->NuevoPagoImpuestos > Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
						ValidaOTP.guardaParametrosEnSession(req);
						
						validacionesRSA(req, res);
						return;
					} 
					
					//Fin validacion rsa para challenge
					///////////////////-------------------------------------------------
	                
	                
	                boolean valBitacora = true;
					if(valida == null) {

					boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber(),IEnlace.PAGO_IMPUESTOS);

					if( session.getValidarToken() &&
						//session.getFacultad(session.FAC_VAL_OTP) &&
						ValidaOTP.getFacValOTP(req) &&
						session.getToken().getStatus() == 1 &&
						solVal)

					{
						//VSWF-HGG -- Bandera para guardar el token en la bit�cora
						req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
						EIGlobal.mensajePorTrace("\n\n\n Solicit la validacin. Se guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
						ValidaOTP.guardaParametrosEnSession(req);

						EIGlobal.mensajePorTrace("\n\n\n Recupera parametros----> \n\n\n", EIGlobal.NivelLog.INFO);

						req.getSession().removeAttribute("mensajeSession");
						ValidaOTP.mensajeOTP(req, "ImpProv");

						ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP_CONFIRM);
					}
						else {
							valida="1";
							valBitacora = false;
						}
					}

					//retoma el flujo
					if( valida!=null && "1".equals(valida))
					{

						if (valBitacora)
						{

							try{
								EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

								HttpSession sessionBit = req.getSession(false);

								int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								String claveOperacion = " ";
								String concepto = " ";
								double importeDouble = 0;
								String cuenta = "0";
								String token = "0";
								if (req.getParameter("token") != null && !"".equals(req.getParameter("token")));
								{token = req.getParameter("token");}

								Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
								Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");

								if (tmpParametros!=null ) {
									Enumeration enumer = req.getParameterNames();
									while(enumer.hasMoreElements()) {
										String name = (String)enumer.nextElement();
									}
								}

								String datosCuenta = "";
								String datosImporte = "";
								String datosTipoImpuesto = "";
								String datosTipoImpuesto2 = "";

								if (tmpAtributos.get("cuenta") != null) {
									datosCuenta = tmpAtributos.get("cuenta").toString();
								} else {datosCuenta = tmpParametros.get("cuenta").toString();}

								cuenta = datosCuenta.substring(0, datosCuenta.indexOf("|"));

								if (tmpAtributos.get("paramImp") != null) {
									datosImporte = tmpAtributos.get("paramImp").toString();
								} else {
									datosImporte = tmpParametros.get("paramImp").toString();
								}

								String importe = datosImporte.substring(datosImporte.indexOf("Total efectivamente pagado:"));
								importe = importe.substring(27,importe.indexOf("|"));
								importeDouble = Double.valueOf(importe.replace(",", "")).doubleValue();

								if (tmpAtributos.get("paramXML") != null)
									{datosTipoImpuesto = tmpAtributos.get("paramXML").toString();}
								if (tmpParametros.get("paramXML") != null)
									{datosTipoImpuesto = tmpParametros.get("paramXML").toString();}
								if (tmpAtributos.get("paramImp") != null)
									{datosTipoImpuesto2 = tmpAtributos.get("paramImp").toString();}
								if (tmpParametros.get("paramImp") != null)
									{datosTipoImpuesto2 = tmpParametros.get("paramImp").toString();}

								if (datosTipoImpuesto != null && datosTipoImpuesto != "")
								{
									if (datosTipoImpuesto.indexOf("PROVISIONALES") > -1)
									{claveOperacion = BitaConstants.CTK_PAGO_IMP_PROV;
									 concepto =  BitaConstants.CTK_CONCEPTO_PAGO_IMP_PROV;}
			            			else
			            				if (datosTipoImpuesto.indexOf("EJERCICIO") > -1)
			            					{claveOperacion = BitaConstants.CTK_PAGO_IMP_EJER;
			            					 concepto = BitaConstants.CTK_CONCEPTO_PAGO_IMP_EJER;}
			            				else
			            					if (datosTipoImpuesto.indexOf("ENTIDADES") > -1)
			            						{claveOperacion = BitaConstants.CTK_PAGO_IMP_FEDER;
			            						 concepto = BitaConstants.CTK_CONCEPTO_PAGO_IMP_FEDER;}
			            					if (datosTipoImpuesto.indexOf("DERECHOS") > -1)
											{claveOperacion = BitaConstants.CTK_PAGO_IMP_DER_PROD_APR;
											 concepto = BitaConstants.CTK_CONCEPTO_PAGO_IMP_DER_PROD_APR;}
								}
								if (datosTipoImpuesto2 != null && datosTipoImpuesto2 != "")
								{
									if (datosTipoImpuesto2.indexOf("Derechos, Productos y Aprovechamientos") > -1)
									{claveOperacion = BitaConstants.CTK_PAGO_IMP_DER_PROD_APR;
									 concepto = BitaConstants.CTK_CONCEPTO_PAGO_IMP_DER_PROD_APR;}}

								sessionBit.removeAttribute("atributosBitacora");
								sessionBit.removeAttribute("parametrosBitacora");

								EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

								String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
							} catch(Exception e) {
								EIGlobal.mensajePorTrace("ENTRA  BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.WARN);
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

							}
						}


//				    	TODO: BIT CU 4051. Se envia la trama para el pago de impuesto
				    	/*
                 		 * VSWF ARR -I
                 		 */
						if ("ON".equals(Global.USAR_BITACORAS)){
				    	try{
				    		String importe =req.getParameter("importetotal");
                    		if(importe==null||"".equals(importe))
                    			importe ="0.00";

            			bt = (BitaTransacBean)bh.llenarBean(bt);



            			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
            				bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
            			else
            				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
            					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
            				else
            					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
            						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
            					else
            						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
            							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);
            			bt.setContrato(session.getContractNumber());


            			String cuenta = (String)req.getParameter("cuenta");



            			if (cuenta!=null){
            				cuenta = cuenta.trim();
            				int posPipe = cuenta.indexOf("|");
            				if (posPipe>0){
            					cuenta = cuenta.substring(0, posPipe);
            				}

            				if (cuenta.length()<20){
            					bt.setCctaOrig(cuenta);
            				}else{
            					bt.setCctaOrig(cuenta.substring(0,20));
            				}

            			}



            			try{
            				bt.setImporte(Double.parseDouble(importe));
            			}catch (NumberFormatException e){
							bt.setImporte(0);
						}
            			if (tipoOperacion!=null){
                				bt.setServTransTux(tipoOperacion);
                			}

            			BitaHandler.getInstance().insertBitaTransac(bt);
            			}catch(SQLException e){
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}catch (Exception e){
            				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            			}
						}
                 		/*
                 		 * VSWF ARR -F
                 		 */
		                ejecutaPagoImpuesto(req, res, contrato, usuario, clavePerfil, tipopago);
		                ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipoOperacion);
		            }
		        }
	        } //fin de session valida
		}
    }

    /***
     * Se encarga de identificar los parametros con la informacion de la operacion, identifica en que formato vienen y
     * realiza la codificacion si es necesario, esto con la finalidad de controlar que los cacteres especiales como la �
     * se muestren correctamente.
     * @param req Peticion
     * @param res Respuesta
     * @param sess Sesion
     */
    private void inicializarParametros(HttpServletRequest req,
			HttpServletResponse res, HttpSession sess) {
    	//CSA - Correccion variables sesion
    	String tramaDesordenada;
    	String param;
    	String paramDatosImp;

        String tramaDesSesion = null;
        String paramDeSesion = null;
        String paramDatSesion = null;
        //Obtengo los parametros con la informacion
    	 tramaDesordenada = req.getParameter("tramaDesordenada");
    	 param = req.getParameter("param");
    	 paramDatosImp = req.getParameter("paramImp");//paramDatosImp");

    	EIGlobal.mensajePorTrace("tramaDesordenada  desde la peticion:"+tramaDesordenada, EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace(" param desde la peticion :"+param, EIGlobal.NivelLog.INFO);
	    EIGlobal.mensajePorTrace("paramImp desde la peticion :"+paramDatosImp, EIGlobal.NivelLog.INFO);

    	 //Significa que es la primer llamada asi que inicializo los atributos correspondientes en la sesion
    	if((tramaDesordenada == null && param == null && paramDatosImp == null)){
    		//Inicializa los atributos para evitar que se queden valores de una ejecucion anterior
    		sess.setAttribute(nomAttTramaDes, null);
    		sess.setAttribute(nomAttParam, null);
    		sess.setAttribute(nomAttParamDat, null);
    	}else{
    		//Cuando es otro llamado cuando ya se capturo informacion

    		//Obtiene los valores de la sesion
	    	tramaDesSesion = (String) sess.getAttribute(nomAttTramaDes);
	    	paramDeSesion = (String) sess.getAttribute(nomAttParam);
	    	paramDatSesion = (String)sess.getAttribute(nomAttParamDat);

	    	EIGlobal.mensajePorTrace("la tramaDesordenada en sesion:"+tramaDesSesion, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("el param en sesion:"+paramDeSesion, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("el paramDatSesion en sesion:"+paramDatSesion, EIGlobal.NivelLog.INFO);
	    	//Si todos los valores de la sesion son null, quiere decir que es cuando apenas se
	    	//dio aceptar en el front
	    	if(tramaDesSesion == null && paramDeSesion == null && paramDatSesion == null){



		    	if(esUTF8(tramaDesordenada)){
		    		EIGlobal.mensajePorTrace("tramaDesordenada es UTF8", EIGlobal.NivelLog.INFO);
		    		tramaDesordenada = convertirUTF8ISO(tramaDesordenada);
		    	}

		    	if(esUTF8(param)){
		    		EIGlobal.mensajePorTrace("param es UTF8", EIGlobal.NivelLog.INFO);
		    		param = convertirUTF8ISO(param);
		    	}

		    	if(esUTF8(paramDatosImp)){
		    		EIGlobal.mensajePorTrace("paramImp es UTF8", EIGlobal.NivelLog.INFO);
		    		paramDatosImp = convertirUTF8ISO(paramDatosImp);
		    	}

	    		//guarda los valores en la sesion
	    		sess.setAttribute(nomAttTramaDes, tramaDesordenada);
	    		sess.setAttribute(nomAttParam, param);
	    		sess.setAttribute(nomAttParamDat, paramDatosImp);


	    	}//else{
	    		//Si ya hay informacion en session, tomo esos valores, ya que con eso garantizo que se codifiquen
	    		//doble vez cuando entren al token
	    		//sess.setAttribute(nomAttTramaDes, tramaDesordenada);
	    		//sess.setAttribute(nomAttParam, param);
	    		//sess.setAttribute(nomAttParamDat, paramDatosImp);

	    	//}
    	}

    	EIGlobal.mensajePorTrace("al final de las validaciones tramaDesordenada :"+tramaDesordenada, EIGlobal.NivelLog.INFO);
    	EIGlobal.mensajePorTrace("al final de las validaciones param :"+param, EIGlobal.NivelLog.INFO);
    	EIGlobal.mensajePorTrace("al final de las validaciones paramDatosImp :"+paramDatosImp, EIGlobal.NivelLog.INFO);


	}

	/**
     * CSA:  Metodo que identifica si una cadena esta en UTF - 8
     *
     * @param trama Trama a evaluar
     * @return true si es UTF-8
     */
    private boolean esUTF8(String trama) {
		EIGlobal.mensajePorTrace("valida codificacion de la trama : "+trama, EIGlobal.NivelLog.INFO);
		byte[] b = trama.getBytes();

		boolean esUTF = false;
		int numBytes = b.length;

		try{
			for(int i=0;i<caracteres.length;i++){
				Character aux = caracteres[i];
				String carac = new String(aux.toString().getBytes(UTF8));
				if(trama.contains(carac)){
					esUTF = true;
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		EIGlobal.mensajePorTrace("la trama era UTF-8: "+esUTF, EIGlobal.NivelLog.INFO);
		return esUTF;
	}
    /**
     * Convierte cadena fomrmato UTF-8 a ISO-8859-1
     * @param  trama trama con formato UTF-8
     * @return trama con formato ISO-8859-1
     */
    private static String convertirUTF8ISO(String trama) {

		EIGlobal.mensajePorTrace("Antes de convertir la cadena "+trama, EIGlobal.NivelLog.INFO);
		try {

			for (int i = 0; i < caracteres.length; i++) {
				Character aux = caracteres[i];
				String caracUTF = new String(aux.toString().getBytes(UTF8));
				String caracISO = new String(aux.toString().getBytes(ISO));

				trama = trama.replace(caracUTF,caracISO);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace("Despues de convertir la cadena a ISO-8859-1: "+trama, EIGlobal.NivelLog.INFO);
		return trama;

	}
    /**
     * Metodo ejeuta Impuestos
     * @param req 		  Peticion
     * @param res 		  Respuesta
     * @param contrato 	  Contrato
     * @param usuario 	  Usuario ejecuta
     * @param clavePerfil Perfil usuario
     * @param tipopago 	  tipo de pago de impuesto
     * @return 			  ejecucion de pagos
     * @throws ServletException Excepcion servlet
     * @throws IOException      Excepcion entrada salida
     */

    public int ejecutaPagoImpuesto(HttpServletRequest req,
            HttpServletResponse res, String contrato, String usuario,
            String clavePerfil, String tipopago) throws ServletException,
            IOException {

        javax.servlet.http.HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
        String namePdf = "";
        String paramDatos = "";
        String paramDatosImp = "";
        String cuenta_cargo = "";
        String selloDigital = "";
        boolean sesionvalida = false;
        String tipo_cuenta = "";
        boolean mancomunado = false;
        String imptot = "";
        String refman = "";
        String totalImporte = "";
        String num_operacion = ""; // --> jbg 060802
        int salida = 0;
        Hashtable vDatos = null;
        String[] mensaje = new String[1];
        String tipoPago = tipopago; //vcl
        mensaje[0] = "";
        String tramaBackEnd = "";
        String tramaDesordenada = "";

		// vswf ini
        String llavePago = "";
		// vswf fin

        //CSA ----------------------------
        String param = sess.getAttribute(nomAttParam) == null?null:sess.getAttribute(nomAttParam).toString();
       //---------------------------------------

        cuenta_cargo = req.getParameter("cuenta");
        try {
            cuenta_cargo = cuenta_cargo.substring(0, cuenta_cargo.indexOf("|"));
        } catch (Exception e) {
            //System.out.println("NuevoPagoImpuestos: cuenta_cargo exception " + e);
        }
        try {

            // VSWF Recupera las tramas de sesin y las almacena en variables en lugar de los request, solo para DPA's
        	EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - tipopago: [" + tipopago + "]", EIGlobal.NivelLog.INFO);
            if("010".equals(tipopago)){
                String s1 = param;
                String s2 = sess.getAttribute("cadParam").toString();

                if(s2.length() > s1.length()){
                    paramDatos = sess.getAttribute("cadParam").toString();

                }
                else{
                    paramDatos = param;
                }

                s1 = sess.getAttribute(nomAttParamDat).toString();//req.getParameter("paramImp");
                s2 = sess.getAttribute("cadParamImp").toString();

                if(s2.length() > s1.length()){
                    paramDatosImp = sess.getAttribute("cadParamImp").toString();
                }
                else{
                    paramDatosImp = sess.getAttribute(nomAttParamDat).toString();//req.getParameter("paramImp");
                }

                s1 = sess.getAttribute(nomAttTramaDes).toString();
                s2 = sess.getAttribute("cadTramaDesordenada").toString();

                if(s2.length() > s1.length()){
                    tramaDesordenada = sess.getAttribute("cadTramaDesordenada").toString();
                }
                else{
                    tramaDesordenada = sess.getAttribute(nomAttTramaDes).toString();//req.getParameter("tramaDesordenada");

                }
                EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatos 60A: [" + paramDatos + "]", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatosImp 60A: [" + paramDatosImp + "]", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - Trama desordenada 60A: [" + tramaDesordenada + "]", EIGlobal.NivelLog.INFO);

            }
            else{
    				    paramDatos = param;
    				    paramDatosImp = sess.getAttribute(nomAttParamDat).toString();//req.getParameter("paramImp");
						tramaDesordenada = sess.getAttribute(nomAttTramaDes).toString();//req.getParameter("tramaDesordenada");
				    }

      			sess.removeAttribute("cadParam");
      			sess.removeAttribute("cadParamImp");
      			sess.removeAttribute("cadTramaDesordenada");
      			// VSWF

            //System.out.println("NuevoPagoImpuestos: tramaDesordenada = " + tramaDesordenada);
            String temp = "";
            int donde = 0;

            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatos antes: [" + paramDatos + "]", EIGlobal.NivelLog.INFO);
            //CSA
            if(esUTF8(paramDatos)){
            	//paramDatos = new String(paramDatos.getBytes("ISO-8859-1"),"UTF-8");
            	paramDatos = convertirUTF8ISO(paramDatos);
            }
            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatos: [" + paramDatos + "]", EIGlobal.NivelLog.INFO);

            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatosImp antes: [" + paramDatosImp + "]", EIGlobal.NivelLog.INFO);
			if ((req.getParameter("encoded") != null) && (new Boolean((String)req.getParameter("encoded")).booleanValue())) {
				//CSA
				if(esUTF8(paramDatosImp)){
					paramDatosImp = convertirUTF8ISO(paramDatosImp);
				}
				//paramDatosImp = new String(paramDatosImp.getBytes("ISO-8859-1"),"UTF-8");
			}
            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - paramDatosImp: [" + paramDatosImp + "]", EIGlobal.NivelLog.INFO);

            donde = paramDatos.indexOf("10017=");
            temp = paramDatos.substring(donde);
            temp = temp.substring(temp.indexOf("=") + 1, temp.indexOf("|"));
            imptot = temp;
            //System.out.println("NuevoPagoImpuestos: paramDatos = " + paramDatos);
            //System.out.println("NuevoPagoImpuestos: paramDatosImp = " + paramDatosImp);
            //System.out.println("NuevoPagoImpuestos: tramaDesordenada = " + tramaDesordenada);
            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - Trama desordenada 7: [" + tramaDesordenada + "]", EIGlobal.NivelLog.INFO);
			if ((req.getParameter("encoded") != null) && (new Boolean((String)req.getParameter("encoded")).booleanValue())) {
				//CSA ----
				if(esUTF8(tramaDesordenada)){
					tramaDesordenada = convertirUTF8ISO(tramaDesordenada);//new String(this.tramaDesordenada.getBytes("ISO-8859-1"),"UTF-8");
				}
			}
            EIGlobal.mensajePorTrace("NuevoPagoimpuestos - ejecutaPagoImpuesto - Trama desordenada 7A: [" + tramaDesordenada + "]", EIGlobal.NivelLog.INFO);
            tramaBackEnd = armaTramaBackEnd(paramDatos, tramaDesordenada, tipopago);

            /*
             * VSWF - LPG
             * 04 - Abr - 2006
             * Se ordena la trama para Diesel Marino y Subsidios, antes de guardarse en la base de datos.
             */
				    if(!"004".equals(tipoPago) && !"010".equals(tipoPago)){
                tramaBackEnd = ordenaDiSub(tramaBackEnd);
            }

            // VSWF Eliminar cuando las cadenas se ordenen en el applet

            //System.out.println("NuevoPagoImpuestos.ejecutaPagoImpuesto: tramaBackEnd = " + tramaBackEnd);
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            //System.out.println("NuevoPagoImpuestos: ejecutaPagoImpuesto exception = " + e);
        }
        //imptot = valIn.getValString("importetotal");
        refman = req.getParameter("refmancomunidad");
        totalImporte = req.getParameter("importetotal");
        //System.out.println("NuevoPagoImpuestos: imptot = " + imptot);
        if (imptot != null && !"".equals(imptot.trim()))
            if (refman != null && !"".equals(refman.trim()))
                mancomunado = true;
        if (contrato != null && usuario != null & cuenta_cargo != null) {
            //selloDigital = llamaServicioPagoImpuestos (paramDatos,
            // cuenta_cargo, contrato, usuario, mancomunado,refman,imptot);
            vDatos = llamaServicioPagoImpuestos(paramDatos, cuenta_cargo,
                    contrato, usuario, mancomunado, refman, imptot,
                    clavePerfil, mensaje, sess, tramaBackEnd, tipopago);
            if (vDatos == null) {
                /*
                 * Este link no lleva prog=0 en produccion quitarselo en cuanto
                 * se suba a produccion despliegaPaginaErrorURL
                 * (mensaje[0],"Nuevo Esquema de Pago de Impuestos
                 * ","Pagos","s26170h",
                 * "/NASApp/"+Global.WEB_APPLICATION+"/csaldo1" ,req, res );
                 */
//            	<vswf:meg cambio de NASApp por Enlace 08122008>
                despliegaPaginaErrorURL(mensaje[0],
                        "Nuevo Esquema de Pago de Impuestos ",
                        "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
                        "s26170h", "/Enlace/" + Global.WEB_APPLICATION
                                + "/csaldo1?prog=0", req, res);

                //despliegaPaginaError(mensaje,"Nuevo Esquema de Pago de
                // Impuestos ", "Pagos");
                return 1;
            }
            selloDigital = (String) vDatos.get("cert");

			// vswf ini
            llavePago = (String)vDatos.get("llavePago");
			// vswf fin

        }
        if (selloDigital != null)
            EIGlobal.mensajePorTrace("NuevoPagoImpuestos: selloDigital = " + selloDigital, EIGlobal.NivelLog.INFO);
        else {
            namePdf = usuario + cuenta_cargo + "otro.pdf";
            selloDigital = "Sello Digital";
        }

        try {
            String tramaImprimir = "";
            //	40002 FECHA pres PAGO
            //	40003 HORA pres PAGO
            //	20002 Numero de operacion
            //	20008 Plaza Bancaria
            //	30003 Numero serie certificado digital
            //Modificacion para el nuevo tipo de pago 010 vcl

            if ("010".equals(tipoPago)){

				        // vswf se agreg? el par�metro llavePago
                tramaImprimir = ArmaTramaImprimir(tipoPago, tramaBackEnd,
                        "20002", (String) vDatos.get("noper"), "40002",
                        (String) vDatos.get("fecha"), "40003", (String) vDatos
                                .get("hora"), "30003", (String) vDatos
                                .get("serial"), session.getClaveBanco(), llavePago);
            }
            else{
				        // vswf se agreg? el par�metro llavePago
				        if(!"004".equals(tipoPago)){
    				        paramDatos = paramDatos.substring(2) + "40006=|EOF|";
    				        String sini = paramDatos.substring(0, paramDatos.indexOf("30003=|30004=|"));
    				        String sfin = paramDatos.substring(paramDatos.indexOf("30003=|30004=|") + "30003=|30004=|".length());
    				        paramDatos = sini + sfin;
    				        paramDatos = ordenaDiSub(paramDatos);
    				        paramDatos = "||" + paramDatos.substring(0, paramDatos.indexOf("40006")) + "30003=|30004=|";

				        }

                tramaImprimir = ArmaTramaImprimir(tipoPago, paramDatos,
                        "20002", (String) vDatos.get("noper"), "40002",
                        (String) vDatos.get("fecha"), "40003", (String) vDatos
                        .get("hora"), "30003", (String) vDatos
                        .get("serial"), session.getClaveBanco(), llavePago);

            }
            //tramaImprimir = ArmaTramaImprimir ( paramDatos,"20002",
            // (String)vDatos.get ("noper"), "40002", (String)vDatos.get
            // ("fecha"), "40003", (String)vDatos.get ("hora"), "30003",
            // (String)vDatos.get ("serial"), session.getClaveBanco ());

            CreacomprobantePDF pdf = new CreacomprobantePDF();

						// vswf ini
						pdf.setCanal("enlace");
						pdf.setLlavePago(llavePago);


						if(this.cadOriginal != null && this.cadOriginal.length() > 0 && this.cadOriginal.indexOf("40008") < 0){
                int iHora = this.cadOriginal.indexOf("40003=");
								String stemp = this.cadOriginal.substring(iHora);
								int itemp = stemp.indexOf("|");
                String strIni = this.cadOriginal.substring(0, iHora + itemp);
                String strFin = this.cadOriginal.substring(iHora + itemp);
                this.cadOriginal = strIni + "|40008=" + llavePago + strFin;
						}

      			pdf.setCadOriginal(this.cadOriginal);

			// vswf fin

            if(paramDatosImp.indexOf("RFC") > -1) {
		            String sRFC = paramDatosImp.substring(paramDatosImp.indexOf(':') + 1, paramDatosImp.indexOf("|"));

		            if (!(sRFC.charAt(3) < 'A') && !(sRFC.charAt(3) > 'Z')) {
		                pdf.setMorales(false);
		            } else {
		                pdf.setMorales(true);
		            }
          	}
          	else{
          			if(paramDatosImp.indexOf("Denominaci") > -1){
		                pdf.setMorales(true);
		            } else {
		                pdf.setMorales(false);
		            }
          	}


            //EL PAGO ES DEL EJERCICIO SE LO ENVIAMOS AL COMPROBANTE PARA QUE
            // SEPA QUE DEBE DE HACER

            pdf.setProvisional(false);
            pdf.setTipoPago((String) req.getSession().getAttribute(
                    "satTipoPago"));
            //System.out.println("NuevoPagoImpuestos: paramDatosImp = " + paramDatosImp);

            pdf.setTramaDatos(paramDatosImp);
            pdf.setSelloDigital(selloDigital);
            pdf.setTramaDatosImprimir(tramaImprimir);
            pdf.setFechaPago((String) vDatos.get("fecha"));
            pdf.setHoraPago((String) vDatos.get("hora"));
            pdf.setNumOperacion((String) vDatos.get("noper"));
            pdf.setPlaza((String) vDatos.get("plaza"));
            pdf.setNumSerie((String) vDatos.get("serial"));
            num_operacion = (String) vDatos.get("noper"); // --> jbg 060802
            try{
            	   EmailSender emailSender = new EmailSender();


	            	   String coderrorEnv = (String)vDatos.get("coderror");
	            	   String coderrorOper= (String)vDatos.get("coderrorOper");
	            	   EIGlobal.mensajePorTrace("*********************coderrorEnv: " + coderrorEnv, EIGlobal.NivelLog.INFO);
	            	   EIGlobal.mensajePorTrace("*********************coderrorOper: " + coderrorOper, EIGlobal.NivelLog.INFO);

	              if(emailSender.enviaNotificacion(coderrorOper)){
	            	  // if(coderrorEnv != null && coderrorEnv.length()>= 2){

	            		  // if("OK".equals(coderrorEnv.substring(0,2))){


				     		   EmailDetails EmailDetails = new EmailDetails();
				     		  tipopago=tipopago.trim();

					             String tipoImpuesto ="";
					             EIGlobal.mensajePorTrace("CONTRATO: " + session.getContractNumber().toString(), EIGlobal.NivelLog.INFO);
					             EIGlobal.mensajePorTrace("NOMBRE CONTRATO: " + session.getNombreContrato().toString(), EIGlobal.NivelLog.INFO);
					             String NumOper =((String) vDatos.get("noper"));
					             EIGlobal.mensajePorTrace("REFERENCIA: " + NumOper, EIGlobal.NivelLog.INFO);

					             EIGlobal.mensajePorTrace("TIPO PAGO IMPUESTO--->" + tipopago+"<---", EIGlobal.NivelLog.INFO);
					             EIGlobal.mensajePorTrace("CUENTA CARGO: " + cuenta_cargo, EIGlobal.NivelLog.INFO);

					            if("005".equals(tipopago))
					            	 tipoImpuesto = "Impuestos del Ejercicio";
								else if("010".equals(tipopago))
									 tipoImpuesto = "Impuestos Derechos, Productos y Aprovechamiento";
								else if("012".equals(tipopago))
									  tipoImpuesto = "Impuestos Entidades Federativas";
								else if("001".equals(tipopago))
									  tipoImpuesto = "Impuestos Provisionales";


					             EIGlobal.mensajePorTrace("*********************TIPO IMPUESTO: " + tipoImpuesto, EIGlobal.NivelLog.INFO);
					             String importe = paramDatosImp.substring(paramDatosImp.indexOf("Total efectivamente pagado:"));
								 importe = importe.substring(27,importe.indexOf("|"));




					             EmailDetails.setNumeroContrato(session.getContractNumber().toString());
					             EmailDetails.setRazonSocial(session.getNombreContrato().toString());
					             EmailDetails.setNumRef(NumOper);
					             EmailDetails.setNumCuentaCargo(cuenta_cargo);
					             EmailDetails.setImpTotal("$".concat(importe));
					             EmailDetails.setTipoPagoImp(tipoImpuesto);
//					         	if (mancomunado) {
//					         		EmailDetails.setEstatusActual("Mancomunada");
//								} else {
									//EmailDetails.setEstatusActual("Realizada");
									EmailDetails.setEstatusActual(coderrorOper);

//								}

								try {

									emailSender.sendNotificacion(req,IEnlace.NOT_PAGO_IMPUESTOS, EmailDetails);
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}

	            	   }
	              // }}

			} catch(Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);


			}


            String comprobante = "";
            /** *********************************************************************************************** */
            /* Integracion SAT */
            /** *********************************************************************************************** */
            comprobante = "comprobante";

            pdf.setGifComprobante(comprobante);
            pdf.desentramaDatos();
            namePdf = pdf.nombrePDF();
            mx.altec.enlace.bo.ArchivoRemoto archRemoto = new mx.altec.enlace.bo.ArchivoRemoto();
            //System.out.println("NuevoPagoImpuestos: namePDF = " + namePdf);
            if (null == namePdf || "".equals(namePdf.trim())) {
                //System.out.println("NuevoPagoImpuestos: Error al crear el archivo del comprobante.");
            }
            if (archRemoto.copiaLocalARemoto(namePdf, "WEB")) {
                //request.getSession ().setAttribute ( MOD_ARCHIVO_ARCHIVO_EXP,
                // archivoWeb.getName () );
                //req.getSession ().setAttribute ("sessNamePDF", namePdf);
                req.removeAttribute("sessNamePDF");
                req.setAttribute("sessNamePDF", namePdf);
            } else {
                //System.out.println("NuevoPagoImpuestos: Error al copiar el archivo.");
            }
            totalImporte = EliminaComas(pdf.obtieneImporteTotal());
            req.removeAttribute("referencia");
            req.setAttribute("referencia", num_operacion);
            req.getSession().setAttribute("FechaHoy", ObtenFecha(false));
            req.getSession().setAttribute("ContUser", ObtenContUser(req));
            req.getSession().setAttribute("MenuPrincipal", session.getStrMenu());
            req.getSession().setAttribute("Fecha", ObtenFecha(true));
            req.getSession().setAttribute("newMenu", session.getFuncionesDeMenu());
            req
                    .getSession()
                    .setAttribute(
                            "encabezado",
                            CreaEncabezado(
                                    "Nuevo Esquema de Pago de Impuestos",
                                    "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
                                    "s25630h", req));
            //res.sendRedirect( "/NASApp/" + Global.WEB_APPLICATION
            // + "/jsp/nuevopagoimpuestos/exitopagoimpuestos.jsp" );
            //evalTemplate ("/jsp/nuevopagoimpuestos/exitopagoimpuestos.jsp",
            // req, res );

				// vswf ini>
				try {
	                String sXML = this.cadXML;
	                sXML = insertaPlazaXML(sXML, (String) vDatos.get("plaza"));
									sXML = insertaSucursalXML(sXML, "0981");
	                sXML = insertaFechaHoraXML(sXML, (String) vDatos.get("fecha"), (String) vDatos.get("hora"));
	                sXML = insertaNumOperacionXML(sXML, (String) vDatos.get("noper"));
	                sXML = insertaLlavePagoXML(sXML, llavePago);
	                EIGlobal.mensajePorTrace("--< NuevoPagoImpuestos - ejecutaPagoImpuesto - sXML [" + sXML + "]", EIGlobal.NivelLog.INFO);


              	  char chChars[]={'�', '�', '�', '�', '�', '�', '�', '?', '', '�', '�', '�', '(', ')', '[', ']', '{', '}', '&', '#', '/', '@', '_',  '�', '!', '�', '?', '�', '$', '\"','\\', '�'};

                  String sample = "";
									if("010".equals(tipoPago)){
	                		sample = this.cadOriginal;
	                }
	                else{
	                		sample = tramaImprimir;
	                }


                 char chChars3[]=sample.toCharArray();
                 StringBuffer str1 = new StringBuffer("");
                 int indice=0;
                 String cadBase="�, �, �, �, �, �, �, ?, , �, �, �, (, ), [, ], {, }, &, #, /, @, _, �, !, �, ?, �, $, \",\\, �";


                  for(int i=0;i<chChars3.length;i++){
                    indice=cadBase.indexOf(String.valueOf(chChars3[i]));
                    if(indice >-1){
                      str1.append(ObtenHex(chChars3[i]));
                    }
                    else{
                    	str1.append(chChars3[i]);
                    }
                  }

                  	EIGlobal.mensajePorTrace("--< NuevoPagoImpuestos - ejecutaPagoImpuesto - str1 [" + str1 + "]", EIGlobal.NivelLog.INFO);
	                sXML = insertaTramaImprimirXML(sXML, str1.toString());
                  	// - TEST sXML = insertaTramaImprimirXML(sXML, sample);

	                EIGlobal.mensajePorTrace("--< NuevoPagoImpuestos - ejecutaPagoImpuesto - selloDigital [" + selloDigital + "]", EIGlobal.NivelLog.INFO);
	                sXML = insertaSelloDigitalXML(sXML, selloDigital);

                  sess.removeAttribute("sessXML");
	                sess.setAttribute("sessXML", sXML);

				}
				catch(Exception e){
					//System.out.println("Error al crear el comprobante XML " + e);
					EIGlobal.mensajePorTrace("--< NuevoPagoImpuestos - ejecutaPagoImpuesto - Mensaje controlado [" + e + "]", EIGlobal.NivelLog.ERROR);
				}
				// vswf fin

//		      TODO: BIT CU 4051, 4061, 4071, 4081, A11
		        /*
		         * VSWF ARR -I
		         */
		        if ("ON".equals(Global.USAR_BITACORAS)){




		        try{
		        	if(imptot==null || "".equals(imptot))
		        		imptot="0.00";

				String referenc =(String) vDatos.get("noper");
				String coderror = (String)vDatos.get("coderror");

		        BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
		        BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
					bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_NUEVO_PAGO_IMP);
				else
					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
						bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_NUEVO_PAGO_IMP);
					else
						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
							bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_NUEVO_PAGO_IMP);
						else
							if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
								bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_NUEVO_PAGO_IMP);

				bt.setContrato(contrato);

				bt.setUsr(usuario);

				try{
					    bt.setImporte(Double.parseDouble(imptot));
					}catch(NumberFormatException nef){
						bt.setImporte(0);
					}


				if (referenc!=null && !"".equals(referenc.trim())){
					try{
						bt.setReferencia(Long.parseLong(referenc));
					}catch(NumberFormatException nef){
						bt.setReferencia(0);
					}

				}
				//VC autor ="ESC" fecha ="22/05/2008" descripcion ="se agrega codigo de error"
				if(coderror != null && coderror.length()>= 2)
				{
					if("OK".equals(coderror.substring(0,2)))
					{
						bt.setIdErr("SATP0000");
					}else{
					    bt.setIdErr(coderror.length() > 8 ? coderror.substring(0,8) : coderror);
					}
				}
				bt.setServTransTux("SATP");
				bt.setCctaOrig(cuenta_cargo);
				if(namePdf!=null && !"".equals(namePdf))
					bt.setNombreArchivo(namePdf);

					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
					&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
							.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}else{
					log("VSWF: no hubo bandera de token");
				}
					//System.out.println("VSWF 6");
				req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}catch (Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

		        }
				/*
				 * VSWF ARR -F
				 */
            req.getRequestDispatcher("/jsp/nuevopagoimpuestos/exitopagoimpuestos.jsp").forward(req, res);
            //System.out.println("NuevoPagoImpuestos: salida = " + salida);
        } catch (Exception e) {
            //System.out.println("NuevoPagoImpuestos: ejecutaPagoImpuesto exception " + e);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }

        return 1;
    }


	// vswf ini


	/**
	 Descripcion : Obtiene el caracter a imprimir en formato hexadecimal
	 Datos Entrada : caracter a examinar
	 Datos Salidas : caracter modificado
	 Autor: VSWF - LP
	**/
  public String ObtenHex(char cValor){
   char chChars[]={'�', '�', '�', '�', '�', '�', '�', '?', '', '�', '�', '�', '(', ')', '[', ']', '{', '}', '&', '#', '/', '@', '_', '�', '!', '�', '?', '�', '$', '\"','\\', '�'};
   String chChars2[]={"&#xC1;","&#xC9;","&#xCD;","&#xD3;","&#xDA;","&#xE1;","&#xE9;","&#xED;","&#xF3;","&#xFA;","&#xD1;","&#xF1;","&#x28;","&#x29;","&#x5B;","&#x5D;","&#x7B;","&#x7D;","&#x26;","&#x23;","&#x2F;","&#x40;","&#x5F;","&#xA1;","&#x21;","&#xBF;","&#x3F;","&#xBA;","&#x24;","&#x22;","&#x27;","&#xd1;"};
   String sValor="";
    if(String.valueOf(cValor).trim().length()>0){
        for(int j=0;j<chChars[j];j++){
         if(String.valueOf(chChars[j]).equals(String.valueOf(cValor))){
            sValor=chChars2[j];
            break;
          }
         }
    }//fin if cValor no es espacio
    else{ sValor=" ";}
   return sValor;
  }

  	/**
     * VSWF LP 15 - Oct - 05
     * @param sXML, sPlaza Inserta el bloque correspondiente a la plaza en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaPlazaXML(String sXML, String sPlaza){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>20008</CLAVE>");

		if(inserta == -1) {
			String sRFC = "<CONCEPTO><CLAVE>10001";
			String sCURP = "<CONCEPTO><CLAVE>10002";
			String sRazonSocial = "<CONCEPTO><CLAVE>10006";
			String sApPaterno = "<CONCEPTO><CLAVE>10003";

			int iRFC = sXML.indexOf(sRFC);
			int iCURP = sXML.indexOf(sCURP);
			int iRazonSocial = sXML.indexOf(sRazonSocial);
			int iApPaterno = sXML.indexOf(sApPaterno);

			String sIni="";
			String sFin ="";
			if(iRFC > -1){
			   sIni = sXML.substring(0, iRFC);
			   sFin = sXML.substring(iRFC);
			}
			else if(iCURP > -1){
			   sIni = sXML.substring(0, iCURP);
			   sFin = sXML.substring(iCURP);
			}
			else if(iRazonSocial > -1){
			   sIni = sXML.substring(0, iRazonSocial);
			   sFin = sXML.substring(iRazonSocial);
			}
			else if(iApPaterno > -1){
			   sIni = sXML.substring(0, iApPaterno);
			   sFin = sXML.substring(iApPaterno);
			}

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>20008</CLAVE>" +
  					"<DESCRIPCION>Plaza</DESCRIPCION>" +
  					"<VALOR>" + sPlaza + "</VALOR>" +
  					"<VALORIMPRESION>" + sPlaza + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML, sSucursal Inserta el bloque correspondiente a la sucursal en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaSucursalXML(String sXML, String sSucursal){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>20009</CLAVE>");

		if(inserta == -1) {
			String sRFC = "<CONCEPTO><CLAVE>10001";
			String sCURP = "<CONCEPTO><CLAVE>10002";
			String sRazonSocial = "<CONCEPTO><CLAVE>10006";
			String sApPaterno = "<CONCEPTO><CLAVE>10003";

			int iRFC = sXML.indexOf(sRFC);
			int iCURP = sXML.indexOf(sCURP);
			int iRazonSocial = sXML.indexOf(sRazonSocial);
			int iApPaterno = sXML.indexOf(sApPaterno);

			String sIni="";
			String sFin ="";
			if(iRFC > -1){
			   sIni = sXML.substring(0, iRFC);
			   sFin = sXML.substring(iRFC);
			}
			else if(iCURP > -1){
			   sIni = sXML.substring(0, iCURP);
			   sFin = sXML.substring(iCURP);
			}
			else if(iRazonSocial > -1){
			   sIni = sXML.substring(0, iRazonSocial);
			   sFin = sXML.substring(iRazonSocial);
			}
			else if(iApPaterno > -1){
			   sIni = sXML.substring(0, iApPaterno);
			   sFin = sXML.substring(iApPaterno);
			}

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>20009</CLAVE>" +
  					"<DESCRIPCION>Sucursal</DESCRIPCION>" +
  					"<VALOR>" + sSucursal + "</VALOR>" +
  					"<VALORIMPRESION>" + sSucursal + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML, sFecha, sHora Inserta el bloque correspondiente a la fecha y hora en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaFechaHoraXML(String sXML, String sFecha, String sHora){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>40002</CLAVE>");

		if(inserta == -1) {

			String sTotalEP = "<CONCEPTO><CLAVE>10017";
			int iTotalEP = sXML.indexOf(sTotalEP);

			String sIni = sXML.substring(0, iTotalEP);
			String sFin = sXML.substring(iTotalEP);

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>40002</CLAVE>" +
  					"<DESCRIPCION>Fecha de pago</DESCRIPCION>" +
  					"<VALOR>" + sFecha + "</VALOR>" +
  					"<VALORIMPRESION>" + sFecha + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
					"<CONCEPTO>" +
  					"<CLAVE>40003</CLAVE>" +
  					"<DESCRIPCION>Hora de pago</DESCRIPCION>" +
  					"<VALOR>" + sHora + "</VALOR>" +
  					"<VALORIMPRESION>" + sHora + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML, sNumOperacion Inserta el bloque correspondiente al n�mero de operacin en la cadena
     * @return cadenaModificada con la que se formar� el recibo con formato XML
     */
	private String insertaNumOperacionXML(String sXML, String sNumOperacion){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>20002</CLAVE>");

		if(inserta == -1) {

			String sTotalEP = "<CONCEPTO><CLAVE>10017";
			int iTotalEP = sXML.indexOf(sTotalEP);

			String sIni = sXML.substring(0, iTotalEP);
			String sFin = sXML.substring(iTotalEP);

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>20002</CLAVE>" +
  					"<DESCRIPCION>N&#xfa;mero de operaci&#xf3;n</DESCRIPCION>" +
  					"<VALOR>" + sNumOperacion + "</VALOR>" +
  					"<VALORIMPRESION>" + sNumOperacion + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML, sLlavePago Inserta el bloque correspondiente a la llave de pago en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaLlavePagoXML(String sXML, String sLlavePago){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>40008</CLAVE>");

		if(inserta == -1) {

			String sTotalEP = "<CONCEPTO><CLAVE>10017";
			int iTotalEP = sXML.indexOf(sTotalEP);

			String sIni = sXML.substring(0, iTotalEP);
			String sFin = sXML.substring(iTotalEP);

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>40008</CLAVE>" +
  					"<DESCRIPCION>Llave de pago</DESCRIPCION>" +
  					"<VALOR>" + sLlavePago + "</VALOR>" +
  					"<VALORIMPRESION>" + sLlavePago + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML, tramaImprimir Inserta el bloque correspondiente a la cadena original en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaTramaImprimirXML(String sXML, String tramaImprimir){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>30002</CLAVE>");

		if(inserta == -1) {

			String sConceptos = "</CONCEPTOS>";
			int iConceptos = sXML.lastIndexOf(sConceptos);
			String sIni = sXML.substring(0, iConceptos);
			String sFin = sXML.substring(iConceptos);
			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>30002</CLAVE>" +
  					"<DESCRIPCION>Cadena original</DESCRIPCION>" +
  					"<VALOR>" + tramaImprimir + "</VALOR>" +
  					"<VALORIMPRESION>" + tramaImprimir + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}
		else cadenaModificada=sXML;

		return cadenaModificada;
	}

    /**
     * VSWF LP 15 - Oct - 05
     * @param sXML,selloDigital Inserta el bloque correspondiente al sello digital en la cadena con la que se
     * @return cadenaModificada formar� el recibo con formato XML
     */
	private String insertaSelloDigitalXML(String sXML, String selloDigital){

		String cadenaModificada = null;

		int inserta = sXML.indexOf("<CLAVE>30001</CLAVE>");

		if(inserta == -1) {

			String sConceptos = "</CONCEPTOS>";
			int iConceptos = sXML.lastIndexOf(sConceptos);
			String sIni = sXML.substring(0, iConceptos);
			String sFin = sXML.substring(iConceptos);

			cadenaModificada =
					sIni +
					"<CONCEPTO>" +
  					"<CLAVE>30001</CLAVE>" +
  					"<DESCRIPCION>Sello digital</DESCRIPCION>" +
  					"<VALOR>" + selloDigital + "</VALOR>" +
  					"<VALORIMPRESION>" + selloDigital + "</VALORIMPRESION>" +
  					"</CONCEPTO>" +
  					sFin;
		}
		else cadenaModificada=sXML;

		return cadenaModificada;
	}

	// vswf fin



    public Hashtable llamaServicioPagoImpuestos(String trama,
            String cuenta_cargo, String contrato, String clave_usuario,
            boolean mancomunada, String referenciaman, String importe_total,
            String clavePerfil, String[] mensaje,
            javax.servlet.http.HttpSession sess, String tramaBackEnd,
            String tipopago) {
    	EIGlobal.mensajePorTrace("NuevoPagoImpuestos - llamaServicioPagoImpuestos - tramaBackEnd [" + tramaBackEnd + "]", EIGlobal.NivelLog.INFO);
        String sello = "  ";
        String coderror = "  ";
        String importeTotal = null;
        StringBuffer tramaarchivo = new StringBuffer();
        StringBuffer tramaWebRed = new StringBuffer();
        String clave40002 = ""; //FECHA pres PAGO
        String clave40003 = ""; //HORA pres PAGO
        String clave20002 = ""; //Numero de operacion
        String clave20008 = ""; //Plaza Bancaria
        String clave30003 = ""; //Numero serie certificado digital
        String certDigital = ""; //Certificado Digital
        String coderrorOper="";//Codigo de error de la bitacora de operaciones

		// vswf ini
        String clave40008 = "";
		// vswf fin

        Hashtable retorno = new Hashtable();
        mx.altec.enlace.bo.ArchivoRemoto archRem = new mx.altec.enlace.bo.ArchivoRemoto();
        if (cuenta_cargo == null || "".equals(cuenta_cargo.trim()))
            cuenta_cargo = " ";
        if (importe_total == null || "".equals(importe_total.trim()))
            importe_total = " ";
        tipoOperacion = "SATP";
        tramaWebRed.append("2EWEB|");
        tramaWebRed.append(clave_usuario);
        tramaWebRed.append("|" + tipoOperacion + "|");
        tramaWebRed.append(contrato);
        tramaWebRed.append("|");
        tramaWebRed.append(clave_usuario);
        tramaWebRed.append("|");
        tramaWebRed.append(clavePerfil); //Ver que perfil tiene el usuario y
        // enviarlo
        tramaWebRed.append("|");
        if (mancomunada) {
            tramaWebRed.append(referenciaman);
            tramaWebRed.append("|");
            tramaWebRed.append(cuenta_cargo);
            tramaWebRed.append("|");
            tramaWebRed.append(importe_total + "|");
        } else {
            // tramaWebRed.append(" | | |"); ---> jbg 050802
            tramaWebRed.append(" |");
            tramaWebRed.append(cuenta_cargo);
            tramaWebRed.append("|");
            tramaWebRed.append(importe_total + "|");
        }
        tramaarchivo.append(clave_usuario);
        tramaarchivo.append("@");
        tramaarchivo.append("EWEB"); //Medio Entrega
        tramaarchivo.append("@");
        tramaarchivo.append(contrato);
        tramaarchivo.append("@");
        tramaarchivo.append(cuenta_cargo);
        tramaarchivo.append("@");
        tramaarchivo.append("_"); //No. Operacion
        tramaarchivo.append("@");
        tramaarchivo.append("981"); //sucursal operante
        tramaarchivo.append("@");
        tramaarchivo.append("_"); //Fecha PAgo
        tramaarchivo.append("@");
        tramaarchivo.append("_"); //Hora Pago
        tramaarchivo.append("@");
        tramaarchivo.append("0"); //Reverso
        tramaarchivo.append("@");
        //tramaarchivo.append ( ( (String)sess.getAttribute ("satTipoPago") )
        // );
        tramaarchivo.append(tipopago);
        tramaarchivo.append("@");
        tramaarchivo.append(tramaBackEnd); //trama de la applet
        tramaarchivo.append("@");
        // Adicion RMM 20021217
        tramaWebRed.append(checkSum(tramaarchivo.toString()) + "|");
        // fin Adicion RMM 20021217

        //System.out.println("NuevoPagoImpuestos: Se guarda el archivo, se envia al web red.");
        //System.out.println("NuevoPagoImpuestos: tramaarchivo = " + tramaarchivo);
        //System.out.println("NuevoPagoImpuestos: tramaWebRed = " + tramaWebRed);

        if (!creaArchivo(clave_usuario + ".tmp", Global.DIRECTORIO_LOCAL, tramaarchivo.toString())) {
            mensaje[0] = IEnlace.MSG_PAG_NO_DISP;
            return null;
        }
        boolean res_ = false;
        try {
            res_ = archRem.copiaLocalARemoto(((String) clave_usuario + ".tmp"),
                    Global.DIRECTORIO_LOCAL);
        } catch (Exception e) {
            //System.out.println("NuevoPagoImpuestos: res_ exception " + e);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        //System.out.println("NuevoPagoImpuestos: Regresa la copia del archivo temporal al remoto. res_ = " + res_);
        if (!res_) {
            //System.out.println("NuevoPagoImpuestos: No se realizo la copia remota. res_ = " + res_);
            mensaje[0] = "Error  no se pudo crear archivo remoto para realizar el pago";
            return null;
        }

        ///INICIO YHG
        EIGlobal.mensajePorTrace("NuevoPagoImpuestos - Valida Cuenta(): Trama entrada: "+cuenta_cargo, EIGlobal.NivelLog.INFO);
        ValidaCuentaContrato valCtas = null;
        String datoscta[]=null;
        try
         {

            BaseResource session = (BaseResource) sess.getAttribute("session");
			valCtas = new ValidaCuentaContrato();
     	   EIGlobal.mensajePorTrace("NuevoPagoImpuestos - cuentaValidar: "+cuenta_cargo, EIGlobal.NivelLog.INFO);

			  datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
															  session.getUserID8(),
													          session.getUserProfile(),
													          cuenta_cargo.trim(),
													          IEnlace.MSua_envio);
				 EIGlobal.mensajePorTrace("NuevoPagoImpuestos - datoscta: "+datoscta, EIGlobal.NivelLog.INFO);




     	  if(datoscta==null)
           {

         	       	mensaje[0] ="Error H701:Cuenta Inv&aacute;lida";
         	      return null;
           }


         }

        catch (Exception e) {e.printStackTrace();}
	    finally
		{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}


	    }
        ////FIN YHG

        try {
            mx.altec.enlace.servicios.ServicioTux tuxGlobal = new mx.altec.enlace.servicios.ServicioTux();
            /*tuxGlobal
                    .setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext())
                            .getContext());*/
            Hashtable hs = tuxGlobal.web_red(tramaWebRed.toString());
            //System.out.println("NuevoPagoImpuestos: tramaWebRed = " + tramaWebRed);
            coderror = (String) hs.get("BUFFER");
            coderrorOper =  hs.get("COD_ERROR")+"";
            System.out.println("NuevoPagoImpuestos: coderror = " + coderror);
            System.out.println("NuevoPagoImpuestos: coderrorOper = " + coderrorOper);

        } catch (java.rmi.RemoteException re) {
            //System.out.println("NuevoPagoImpuestos: remote exception " + re);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        if (coderror == null)
            coderror = "";
        String ok = "";
        String error = "";
        //String mensaje = "";
        String elarchivo = "";
        String lareferencia = "";

        //Cap X - LYM - JGAL
        EIGlobal.mensajePorTrace("Nvo Pag Imp (LyM) coderror: [" + coderror + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Nvo Pag Imp (LyM) coderrorOper: [" + coderrorOper + "]", EIGlobal.NivelLog.INFO);
        if (coderrorOper!=null && coderrorOper.startsWith("ALYM")) {
            mensaje[0] = "Error  " + coderror + " en el servicio al ejecutar el pago";
            return null;
        }

        if (coderror.length() > 53) {
            /*
             * asi esta actualmente ok = coderror.substring (0, EIGlobal.NivelLog.ERROR); lareferencia =
             * coderror.substring (11,15); error = coderror.substring (16,24);
             * elarchivo = coderror.substring (24);
             */
            ok = coderror.substring(0, 2);
            lareferencia = coderror.substring(3, coderror.indexOf("SATS"));
            error = coderror.substring(coderror.indexOf("SATS"), coderror
                    .indexOf("/"));
            elarchivo = coderror.substring(coderror.indexOf("/"), coderror
                    .length());
            //System.out.println("NuevoPagoImpuestos: Ref. = " + lareferencia);
            //System.out.println("NuevoPagoImpuestos: Error. = " + error);
            //System.out.println("NuevoPagoImpuestos: Archivo = " + elarchivo);
        } else {
            elarchivo = coderror;
            //System.out.println("NuevoPagoImpuestos: coderror incorrecto");
        }
        if (error == null)
            error = "           ";
        //if( error.equals("SATS0000") ) {
        archRem.copia(clave_usuario);
        //}
        try {
            java.io.BufferedReader fileDec = null;
            fileDec = new java.io.BufferedReader(new java.io.FileReader(
                    elarchivo.trim()));
            String tramaSalida = "";
            tramaSalida = fileDec.readLine();

			EIGlobal.mensajePorTrace("FILEDEC: " + tramaSalida,EIGlobal.NivelLog.INFO);

            if (null != tramaSalida) {
                //System.out.println("NuevoPagoImpuestos: tramaSalida = " + tramaSalida);
            } else {
                //System.out.println("NuevoPagoImpuestos: tramaSalida = null");
            }

            //System.out.println("El ultimo valor de coderror es = " + coderror);

            if (tramaSalida == null || coderror == "")
                tramaSalida = "SATS0005   10812ERROR AL EJECUTAR EL SERVICIO DE PAGO DE IMPUESTOS";
            String retCode = tramaSalida.substring(0, 8).trim();

            //System.out.println("NuevoPagoImpuestos: coderror = " + coderror);
            if (coderror.length() < 53) {
                lareferencia = tramaSalida.substring(11, 16);
                error = tramaSalida.substring(16);
            }
            String registro = "";
            fileDec.close();
            //System.out.println("NuevoPagoImpuestos: retCode = " + retCode);
            if ("SATS0000".equals(retCode) || "SATS0000".equals(error)) {
                registro = tramaSalida;


				// vswf ini
                String stemp = "";
                int itemp = 0;

                itemp = tramaSalida.indexOf("40002");
                stemp = tramaSalida.substring(itemp);
                clave40002 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                itemp = tramaSalida.indexOf("40003");
                stemp = tramaSalida.substring(itemp);
                clave40003 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                itemp = tramaSalida.indexOf("20002");
                stemp = tramaSalida.substring(itemp);
                clave20002 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                itemp = tramaSalida.indexOf("20008");
                stemp = tramaSalida.substring(itemp);
                clave20008 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                itemp = tramaSalida.indexOf("30003");
                stemp = tramaSalida.substring(itemp);
                clave30003 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                itemp = tramaSalida.indexOf("40008");
                stemp = tramaSalida.substring(itemp);
                clave40008 = stemp.substring(stemp.indexOf("=") + 1, stemp.indexOf("@"));
                registro = registro.substring(registro.indexOf("@") + 1);

                sello = registro.substring(0, registro.lastIndexOf("@"));

				// vswf ini


            } else {
                mensaje[0] = "Error  " + error
                        + " en el servicio al ejecutar el pago";
                return null;
            }
        } catch (Exception e) {
            //System.out.println("NuevoPagoImpuestos: exception " + e);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            return null;
        }
        retorno.put("fecha", clave40002);
        retorno.put("hora", clave40003);
        retorno.put("noper", clave20002);
        retorno.put("plaza", clave20008);
        retorno.put("serial", clave30003);
        retorno.put("cert", sello);

        //VC autor = "ESC" fecha ="22/05/2008" descripcio="se agrega codigo error"
        retorno.put("coderror",coderror);
		// vswf ini
        retorno.put("llavePago", clave40008);
		// vswf fin
        retorno.put("coderrorOper", coderrorOper);


        return retorno;
    }

    public boolean creaArchivo(String nombreArchivo, String rutaArchivo,
            String registro) {


        java.io.File outf = null;
        java.io.FileWriter out = null;
        //System.out.println("NuevoPagoImpuestos: nombreArchivo = " + nombreArchivo);
        //System.out.println("NuevoPagoImpuestos: rutaArchivo = " + rutaArchivo);
        //System.out.println("NuevoPagoImpuestos: registro = " + registro);
        java.io.BufferedWriter writer = null;
        boolean retValue = false;
        try {
            out = new java.io.FileWriter(new java.io.File(rutaArchivo,
                    nombreArchivo));
            writer = new java.io.BufferedWriter(out);
            writer.write(registro);
            writer.flush();
            retValue = true;
        } catch (IOException e) {
            //System.out.println("NuevoPagoImpuestos.creaArchivo (catch) " + e);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    //System.out.println("NuevoPagoImpuestos.creaArchivo (finally) " + e);
                }
            }
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    //System.out.println("NuevoPagoImpuestos: creaArchivo exception " + e);
                }
            }
        }
        //System.out.println("NuevoPagoImpuestos: retValue = " + retValue);
        return retValue;
    }

    public String EliminaComas(String sCadena) {
        String sResultado = "";
        String c = "";
        if (sCadena == null)
            sCadena = "  ";
        for (int i = 0; i < sCadena.length(); i++) {
            c = sCadena.substring(i, i + 1);
            if (",".equals(c) == true)
                c = "";
            sResultado += c;
        }
        return (sResultado);
    }

    public static final String FIN_TRAMA = "|30003=|30004=||";


    /**
     * Quita los espacios en blanco en una cadena Ver?nica Cervantes Lara (vcl)
     * @param cadena cadena a procesar
     * @return cadena sin espacios
     */
    public String QuitarEspacios(String cadena) {
        String cadNueva = "";
        StringTokenizer token = new StringTokenizer(cadena);
        while (token.hasMoreTokens()) {
            cadNueva += token.nextToken(" ");
            //System.out.println("NuevoPagoImpuestos: cadNueva = " + cadNueva);
        }
        return cadNueva;
    }

    /***************************************************************************
     * Arma la trama que se ira a imprimir en el comprobante Ver?nica Cervantes
     * Lara (vcl)
     */

	// vswf se agreg? el par�metro llavePago
    public String ArmaTramaImprimir(String tipoPago, String tramaApplet,
            String cvNoOp, String noOp, String cvFecP, String fecP,
            String cvHrP, String hrP, String cvSer, String ser, String cveBanco, String llavePago) {
        String tramaImp = "";
        String s4 = "";
        String s5 = "";
        String s6 = "";
        int s = 0;
        /***********************************************************************
         * Si no tiene RFC y es un pago DPA, la cadena de impresion se armara
         * con el nombre completo del contribuyente (sin espacios), el importe a
         * pagar (100017), las claves que inicien con 147xx, clave del banco y
         * los datos propios del impuesto a pagar
         **********************************************************************/
        if (tramaApplet.indexOf("10001=SIN_RFC") > -1 && "010".equals(tipoPago)) {
            if (tramaApplet.indexOf("|10003=") > -1)
                s4 = tramaApplet.substring(tramaApplet.indexOf("|10003=") + 1,
                        tramaApplet.indexOf("|10017=")); //nombre persona
            // fisica
            if (tramaApplet.indexOf("10006=") > -1)
                s4 = tramaApplet.substring(tramaApplet.indexOf("|10006=") + 1,
                        tramaApplet.indexOf("|10017=")); //nombre persona moral
            s4 = QuitarEspacios(s4);
            s5 = tramaApplet.substring(tramaApplet.indexOf("10017="));
            s6 = tramaApplet.substring(tramaApplet.indexOf("10017="),
                    tramaApplet.length());
            s = s6.indexOf("|") + 1;
            s5 = s5.substring(0, s5.indexOf("|"));

            //vswf Secci?n en donde se ordenan los datos a salir de la cadena original en el recibo PDF
            int i1=0;
            int i2=0;
            String aux120="";
            String aux147="";
            String sTmp=tramaApplet;
            //se recorre la cadena original paraa extraer los conceptos de DPA e IVA en orden
            while(i1>-1 || i2 > -1){
             if(i1>-1){
               if(i1==0) i1 = sTmp.indexOf("|12002");
                 if(i1 > -1){ //si se pago iva se procede a ordenarlos
                     aux120+=sTmp.substring(sTmp.indexOf("|12002",i1),sTmp.indexOf("|", sTmp.indexOf("|12020",i1)+1 ));
                     i1=sTmp.indexOf("|12002",i1+1);
                  }
              }//fin if i1>-1
             if(i2>-1){
               if(i2==0) i2 = sTmp.indexOf("|147");
               else i2= sTmp.indexOf("|147",i2);
               if(i2>-1){
                    aux147+=sTmp.substring(sTmp.indexOf("|147",i2),sTmp.indexOf("|", sTmp.indexOf("|14734",i2)+1 ));
                  i2 = sTmp.indexOf("|", sTmp.indexOf("|14734",i2)+1 );
                 }
              }//fin if i2>-1
            }//fin while
            //vswf


            int pos = tramaApplet.indexOf("|120");
            if (pos < 0)
                pos = tramaApplet.indexOf("|147");
            s6 = tramaApplet.substring(pos + 1, tramaApplet.indexOf("|40006="));
            // JVL MX-2004-120 PROYECTO UNICO 03-Ene-2005
            /*
             * tramaImp = "||" + s4 + "|" + s5 + "|20001=40" + cveBanco + "|" +
             * cvNoOp + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP +
             * "=" + hrP + "|" + s6 + "|" + cvSer + "=" + ser + "||";
             */
            tramaImp = "||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + "|" + s6 + "|" + cvSer + "=" + ser + "||";

						if(aux120.length()>0){ // si existen iva's se anexan junto con los DPA
								this.cadOriginal="||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + "|" + aux120.substring(1,aux120.length())+aux147 + "|" + cvSer + "=" + ser + "||";
						}
						else{ // caso contrario , solo se anexan pagos de DPA
								this.cadOriginal="||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + aux147 + "|" + cvSer + "=" + ser + "||";
							}

            // FIN JVL MX-2004-120 PROYECTO UNICO
        }
        /***********************************************************************
         * Si tiene RFC y es un pago DPA, la cadena de impresion se armara con
         * el rfc del contribuyente, el importe a pagar (100017), las claves que
         * inicien con 147xx, clave del banco y los datos propios del impuesto a
         * pagar
         **********************************************************************/
        if (tramaApplet.indexOf("10001=SIN_RFC") < 0 && "010".equals(tipoPago)) {
            s4 = tramaApplet.substring(0, tramaApplet.indexOf("|"));
            s5 = tramaApplet.substring(tramaApplet.indexOf("10017="));
            s6 = tramaApplet.substring(tramaApplet.indexOf("10017="),
                    tramaApplet.length());
            s = s6.indexOf("|") + 1;
            s5 = s5.substring(0, s5.indexOf("|"));

            //vswf Secci?n en donde se ordenan los datos a salir de la cadena original en el recibo PDF
            int i1=0;
            int i2=0;
            String aux120="";
            String aux147="";
            String sTmp=tramaApplet;
            //se recorre la cadena original paraa extraer los conceptos de DPA e IVA en orden
            while(i1>-1 || i2 > -1){
             if(i1>-1){
               if(i1==0) i1 = sTmp.indexOf("|12002");
                 if(i1 > -1){ //si se pago iva se procede a ordenarlos
                     aux120+=sTmp.substring(sTmp.indexOf("|12002",i1),sTmp.indexOf("|", sTmp.indexOf("|12020",i1)+1 ));
                     i1=sTmp.indexOf("|12002",i1+1);
                  }
              }//fin if i1>-1
             if(i2>-1){
               if(i2==0) i2 = sTmp.indexOf("|147");
               else i2= sTmp.indexOf("|147",i2);
               if(i2>-1){
                    aux147+=sTmp.substring(sTmp.indexOf("|147",i2),sTmp.indexOf("|", sTmp.indexOf("|14734",i2)+1 ));
                  i2 = sTmp.indexOf("|", sTmp.indexOf("|14734",i2)+1 );
                 }
              }//fin if i2>-1
            }//fin while
            //vswf

            int pos = tramaApplet.indexOf("|120");
            if (pos < 0)
                pos = tramaApplet.indexOf("|147");
            s6 = tramaApplet.substring(pos + 1, tramaApplet.indexOf("|40006="));
            // JVL MX-2004-120 PROYECTO UNICO 03-Ene-2005

            /*
             * tramaImp = "||" + s4 + "|" + s5 + "|20001=40" + cveBanco + "|" +
             * cvNoOp + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP +
             * "=" + hrP + "|" + s6 + "|" + cvSer + "=" + ser + "||";
             */
            tramaImp = "||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + "|" + s6 + "|" + cvSer + "=" + ser + "||";


						if(aux120.length()>0){ // si existen iva's se anexan junto con los DPA
								this.cadOriginal="||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + "|" + aux120.substring(1,aux120.length())+aux147 + "|" + cvSer + "=" + ser + "||";
						}
						else{ // caso contrario , solo se anexan pagos de DPA
								this.cadOriginal="||" + s4 + "|" + s5 + "|20001=40014" + "|" + cvNoOp
                    + "=" + noOp + "|" + cvFecP + "=" + fecP + "|" + cvHrP
                    + "=" + hrP + aux147 + "|" + cvSer + "=" + ser + "||";
						}


            // Fin JVL MX-2004-120 PROYECTO UNICO
        }
        /***********************************************************************
         * Si no es un pago DPA, la cadena de impresion se armara rfc,el importe
         * a pagar (100017), las claves que inicien con 147xx, clave del banco y
         * los datos propios del impuesto a pagar
         **********************************************************************/
        if (!"010".equals(tipoPago)) {
            int i = tramaApplet.indexOf("10001=");
            int j = tramaApplet.indexOf("|", i);
            String rfc = tramaApplet.substring(i, j);
            String total = tramaApplet.substring(tramaApplet.indexOf("10017=")); // Modificacion
            String resto = tramaApplet.substring(tramaApplet.indexOf("40003="),
                    tramaApplet.length());
            int inicio = 0;
            inicio = resto.indexOf("|") + 1;
            total = total.substring(0, total.indexOf("|"));
            resto = resto.substring(inicio, resto.length());

            tramaImp = "||" + rfc + "|" + total + "|" + "20001" + "=" + "40014"
                    + "|" + cvNoOp + "=" + noOp + "|" + cvFecP + "=" + fecP
                    + "|" + cvHrP + "=" + hrP + "|"
                    + resto.substring(0, resto.length() - FIN_TRAMA.length() + 1)
                    + "|" + cvSer + "=" + ser + "||";


            // Fin JVL MX-2004-120 PROYECTO UNICO

			//vswf
                String cveLlave = "40008=";
                int iLlave = tramaImp.indexOf(cveLlave);
                String strIni = tramaImp.substring(0, iLlave);
                String strFin = tramaImp.substring(iLlave + "40008=".length());
                tramaImp = strIni + cveLlave + llavePago + strFin;
			//vswf

        }
        //System.out.println("NuevoPagoImpuestos: tramaImp = " + tramaImp);
        return tramaImp;
    }

    public int presentaApplet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        String fecha_hoy = "";
        String sFechaServer = "";
        String smes = "";
        String anio = "";
        String sdia = "";
        int ianio;
        int imes = 0;
        int idia = 0;
        int salida = 0;
        java.util.Date hoy = new java.util.Date();
        GregorianCalendar cal = new GregorianCalendar();
        fecha_hoy = ObtenFecha();
        cal.setTime(hoy);
        ianio = cal.get(Calendar.YEAR);
        anio = Integer.toString(ianio);
        imes = cal.get(Calendar.MONTH);
        imes++;
        smes = ((imes < 10) ? "0" : "") + imes;
        idia = cal.get(Calendar.DATE);
        sdia = ((idia < 10) ? "0" : "") + idia;
        sFechaServer = sdia + "/" + smes + "/" + anio;
        javax.servlet.http.HttpSession ses = req.getSession();
        BaseResource session = (BaseResource) ses.getAttribute("session");
        if (ses.getAttribute("fechaServer") != null)
            ses.removeAttribute("fechaServer");
        ses.setAttribute("fechaServer", fechaApplet());
        req.setAttribute("fechaServer", sFechaServer); //migra
        req.setAttribute("newMenu", session.getFuncionesDeMenu()); //migra
        req.setAttribute("MenuPrincipal", session.getStrMenu()); //migra
        req.setAttribute("Encabezado", CreaEncabezado(
                "Nuevo Esquema de Pago de Impuestos",
                "Servicios &gt; Nuevo Esquema de Pago de Impuestos", "s25630h",
                req)); //migra
        evalTemplate("/jsp/pagoimpuestos.jsp", req, res);
        return 1;
    }

    String fechaApplet() {
        String fecha_hoy = "";
        String ctas = "";
        int ianio;
        java.util.Date hoy = new java.util.Date();
        GregorianCalendar cal = new GregorianCalendar();
        int imes = 0;
        String smes = "";
        String anio = "";
        int idia = 0;
        String sdia = "";
        int salida = 0;
        fecha_hoy = ObtenFecha();
        cal.setTime(hoy);
        ianio = cal.get(Calendar.YEAR);
        anio = Integer.toString(ianio);
        imes = cal.get(Calendar.MONTH);
        imes++;
        smes = ((imes < 10) ? "0" : "") + imes;
        idia = cal.get(Calendar.DATE);
        sdia = ((idia < 10) ? "0" : "") + idia;
        String sFechaServer = "";
        sFechaServer = sdia + "/" + smes + "/" + anio;
        return sFechaServer;
    }

    // Metodo a�adido RMM 20021217
    /**
     * Calcula el CheckSum segun dise�o de Marco Balderas
     *
     * @param texto
     *            es el texto del cual se obtendr� el CheckSum
     * @return String con la cadena del CheckSum con longitud de 6 caracteres
     */
    public static String checkSum(String texto) {
        //System.out.println("NuevoPagoImpuestos: texto = " + texto);
        String retValue = "";
        String debugStr = " checkSum\n";
        StringBuffer buff = new StringBuffer(6);
        long sum = 0;
        long prod = 0;
        char c = '\0';
        for (int i = 0; i < texto.length(); ++i) {
            c = texto.charAt(i);
            if ('\0' == c || '\n' == c) {
                continue;
            }
            //c = UnicodeToAscii.unicodeToAscii (c);
            sum += c;
            prod += (i + 1) * c;
        }
        debugStr += " sum =" + String.valueOf(sum) + " prod ="
                + String.valueOf(prod) + "\n";
        char d3 = (char) (48 + sum - Math.round(sum / 16) * 16);
        sum /= 16;
        char d2 = (char) (48 + sum - Math.round(sum / 16) * 16);
        sum /= 16;
        char d1 = (char) (48 + sum - Math.round(sum / 16) * 16);
        char d6 = (char) (48 + prod - Math.round(prod / 16) * 16);
        prod /= 16;
        char d5 = (char) (48 + prod - Math.round(prod / 16) * 16);
        prod /= 16;
        char d4 = (char) (48 + prod - Math.round(prod / 16) * 16);
        debugStr += "\td1 = " + String.valueOf(d1) + " hex:"
                + Long.toHexString(d1) + "\n";
        debugStr += "\td2 = " + String.valueOf(d2) + " hex:"
                + Long.toHexString(d2) + "\n";
        debugStr += "\td3 = " + String.valueOf(d3) + " hex:"
                + Long.toHexString(d3) + "\n";
        debugStr += "\td4 = " + String.valueOf(d4) + " hex:"
                + Long.toHexString(d4) + "\n";
        debugStr += "\td5 = " + String.valueOf(d5) + " hex:"
                + Long.toHexString(d5) + "\n";
        debugStr += "\td6 = " + String.valueOf(d6) + " hex:"
                + Long.toHexString(d6);

        //System.out.println("NuevoPagoImpuestos: debugStr = " + debugStr);

        buff.append((char) d1);
        buff.append((char) d2);
        buff.append((char) d3);
        buff.append((char) d4);
        buff.append((char) d5);
        buff.append((char) d6);
        retValue = buff.toString();

        //System.out.println("NuevoPagoImpuestos: retValue = " + retValue);

        return retValue;
    }

    protected String armaTramaBackEnd(String tramaOrdenada,
            String tramaDesordenada, String tipoPago) {
        StringBuffer retValue = new StringBuffer();
        EIGlobal.mensajePorTrace("NuevoPagoimpuestos --> armaTramaBackEnd - tramaOrdenada :: [" + tramaOrdenada + "]", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("NuevoPagoimpuestos --> armaTramaBackEnd - Trama desordenada :: [" + tramaDesordenada + "]", EIGlobal.NivelLog.INFO);
        int i = 0, j = 0;
        String rfc = "";
        String nombre = "";
        if ("010".equals(tipoPago)) // trama para Derechos,Productos y
        // Aprovechamientos (DPA) vcl
        {
            // si no se encuentra el rfc en la trama se le concatenara SIN_RFC
            // para identificar este faltante (vcl)
            if (tramaDesordenada.indexOf("10001=") < 0) {
                rfc = "10001=SIN_RFC|";
                tramaDesordenada = rfc + tramaDesordenada;
            } else
                rfc = tramaDesordenada.substring(tramaDesordenada
                        .indexOf("10001="), tramaDesordenada.indexOf("10017="));
            if ("010".equals(tipoPago)) {
                if (tramaDesordenada.indexOf("10001=") < 0) //no se encuentra
                    // el rfc
                    nombre = tramaDesordenada.substring(0, tramaDesordenada
                            .length());
                else
                    nombre = tramaDesordenada.substring(tramaDesordenada
                            .indexOf("|") + 1, tramaDesordenada.length());
            } else {
                nombre = tramaDesordenada.substring(tramaDesordenada
                        .indexOf("|") + 1, tramaDesordenada.indexOf("|10017="));
            }

            String cadenanueva = "";
            //System.out.println("NuevoPagoImpuestos: RFC = " + rfc);
            // La cadena nueva = la cadena del applet + la version (vcl)
            cadenanueva = rfc + nombre.substring(0, nombre.length() - 4); //para
            // 010
            String cadena = tramaDesordenada.substring(0, tramaDesordenada
                    .length() - 4); //quita el EOF| para despues insertar la
            // version
            retValue.append(cadena);
        } else //flujo actual vcl
        {
            // a�adir el rfc
            // int i = tramaOrdenada.indexOf ( "10001" );// Modificacion RMM
            // 20030331 commented out
            i = tramaOrdenada.indexOf("10001="); // Modificacion RMM 20030331
            // add
            // int j = tramaOrdenada.indexOf ( "10017" );// Modificacion RMM
            // 20030331 commented out
            j = tramaOrdenada.indexOf("10017="); // Modificacion RMM 20030331
            // add
            retValue.append(tramaOrdenada.substring(i, j));
            //a�adir los datos del contribuyente
            //i = tramaDesordenada.indexOf ( "10001" );// Modificacion RMM
            // 20030331 commented out
            i = tramaDesordenada.indexOf("10001="); // Modificacion RMM 20030331
            // add
            i = tramaDesordenada.indexOf('|', i) + 1;
            //j = tramaDesordenada.indexOf ("10017");// Modificacion RMM
            // 20030331 commented out
            j = tramaDesordenada.indexOf("10017="); // Modificacion RMM 20030331
            // add
            retValue.append(tramaDesordenada.substring(i, j));
            //a�adir el importe total y saltar los siguientes elementos que
            // vienen vacios
            //i = tramaOrdenada.indexOf ("10017");// Modificacion RMM 20030331
            // commented out
            i = tramaOrdenada.indexOf("10017="); // Modificacion RMM 20030331
            // add
            j = tramaOrdenada.indexOf('|', i) + 1;
            retValue.append(tramaOrdenada.substring(i, j));
            //a�adir el resto de la trama ordenada excepto los ultimos
            // elementos que venen vacios
            //i = tramaOrdenada.indexOf ( "40003" );// Modificacion RMM
            // 20030331 commented out
            i = tramaOrdenada.indexOf("40003="); // Modificacion RMM 20030331
            // add
            i = tramaOrdenada.indexOf('|', i) + 1;
            // j = tramaOrdenada.indexOf ( "30003" );// Modificacion RMM
            // 20030331 commented out
            j = tramaOrdenada.indexOf("30003="); // Modificacion RMM 20030331
            // add
            retValue.append(tramaOrdenada.substring(i, j));
            /*
             * Modificado por Miguel Cortes Arellano Fecha 13/09/2003 Empieza
             * codigo sinapsis Proposito mandar la version en la trama para el
             * procesamiento en redsrvr
             */
        }
//        if (tipoPago.equals("001")) { //Provisionales
        if ("001".equals(tipoPago)) { //Provisionales

            //retValue.append (tramaOrdenada.substring ( i,j )+"40006=11003|");
            //retValue.append("40006=11004|").append("EOF|");

			// vswf ini
			retValue.append("40006=11005|").append(EOF_PIPE);
			// vswf fin

//        } else if (tipoPago.equals("005")) { //Ejercicios
        } else if ("005".equals(tipoPago)) { //Ejercicios

            //retValue.append (tramaOrdenada.substring ( i,j )+"40006=12002|");
            //retValue.append("40006=12003|").append("EOF|");

			// vswf ini
			retValue.append("40006=12005|").append(EOF_PIPE);
			// vswf fin

//        } else if (tipoPago.equals("004")) { //Creditos Fiscales
        } else if ("004".equals(tipoPago)) { //Creditos Fiscales

            //retValue.append (tramaOrdenada.substring ( i,j )+ "40006=13001|");
            //retValue.append("40006=13001|").append("EOF|");

			// vswf ini
			retValue.append("40006=13002|").append(EOF_PIPE);
			// vswf fin

//        } else if (tipoPago.equals("012")) { //Entidades Federativas
        } else if ("012".equals(tipoPago)) { //Entidades Federativas

            //retValue.append (tramaOrdenada.substring ( i,j ) + "40006=14001|");
            //retValue.append("40006=14002|").append("EOF|");

			// vswf ini
			retValue.append("40006=14003|").append(EOF_PIPE);
			// vswf fin

        }
        //	Se agrega la versi?n para el nuevo tipo de pago Derechos, Productos y Aprovechamientos --- vcl
//        else if (tipoPago.equals("010")) {
        else if ("010".equals(tipoPago)) {

            //retValue.append("40006=15001|").append("EOF|");

			// vswf ini
			retValue.append("40006=15003|").append(EOF_PIPE);
			// vswf fin

        }
        //Finaliza Codigo Synapsis
        //retValue.append ( tramaOrdenada.substring ( i,j)).append ( "EOF|" );
        //System.out.println("NuevoPagoImpuestos: retValue = " + retValue.toString());
        return retValue.toString();
    }

    public String llamaServicioVerificaPago(String contrato,
            String clave_usuario, String clavePerfil,
            javax.servlet.http.HttpSession sess, String tramaBackEnd,
            String tipopago, HttpServletRequest req, HttpServletResponse res) {
    	EIGlobal.mensajePorTrace("NuevoPagoImpuestos - llamaServicioVerificaPago - tramaBackEnd [" + tramaBackEnd + "]", EIGlobal.NivelLog.INFO);

        String coderror = "  ";
        StringBuffer tramaarchivo = new StringBuffer();
        StringBuffer tramaWebRed = new StringBuffer();
        mx.altec.enlace.bo.ArchivoRemoto archRem = new mx.altec.enlace.bo.ArchivoRemoto();
        boolean mancomunada = false;
        String paramDatos = sess.getAttribute(nomAttParam).toString();//param;
        String cuenta_cargo = req.getParameter("cuenta");
        String referenciaman = req.getParameter("refmancomunidad");
        String importe_total = "";
        String[] mensaje = new String[1];
        mensaje[0] = "";
        try {
            cuenta_cargo = cuenta_cargo.substring(0, cuenta_cargo.indexOf('|'));
            String temp = "";
            int donde = 0;
            donde = paramDatos.indexOf("10017=");
            temp = paramDatos.substring(donde);
            temp = temp.substring(temp.indexOf('=') + 1, temp.indexOf('|'));
            importe_total = temp;
        } catch (Exception e) {
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
//        if (importe_total != null && !importe_total.trim().equals("")){
        if (importe_total != null && !"".equals(importe_total.trim())){
//            if (referenciaman != null && !referenciaman.trim().equals("")){
            if (referenciaman != null && !"".equals(referenciaman.trim())){
                mancomunada = true;
			}
		}
//        if (cuenta_cargo == null || cuenta_cargo.trim().equals("")){
        if (cuenta_cargo == null || "".equals(cuenta_cargo.trim())){
            cuenta_cargo = " ";
		}
//        if (importe_total == null || importe_total.trim().equals("")){
        if (importe_total == null || "".equals(importe_total.trim())){
            importe_total = " ";
		}
        tipoOperacion = "VAPS";
        tramaWebRed.append("1EWEB|").append(clave_usuario).append("|" + tipoOperacion + "|")
                .append(contrato).append('|');
        tramaWebRed.append(clave_usuario).append('|').append(clavePerfil); //Ver
        // que
        // perfil
        // tiene
        // el
        // usuario
        // y
        // enviarlo
        tramaWebRed.append('|');
        if (mancomunada) {
            tramaWebRed.append(referenciaman).append('|').append(cuenta_cargo)
                    .append('|').append(importe_total + "|");
        } else {
            //System.out.println("NuevoPagoImpuestos: La cuenta no es mancomunada");
            tramaWebRed.append(" |").append(cuenta_cargo).append('|').append(importe_total + "|");
        }
        tramaarchivo.append(clave_usuario).append('@').append("EWEB"); //Medio
        // Entrega
        tramaarchivo.append('@').append(contrato).append('@').append(
                cuenta_cargo);//96056100911
        tramaarchivo.append('@').append('_'); //No. Operacion
        tramaarchivo.append('@').append("981"); //sucursal operante
        tramaarchivo.append('@').append('_'); //Fecha PAgo
        tramaarchivo.append('@').append('_'); //Hora Pago
        tramaarchivo.append('@').append('0'); //Reverso
        tramaarchivo.append('@').append(tipopago).append('@').append(
                tramaBackEnd); //trama de la applet
        tramaarchivo.append('@');
        tramaWebRed.append(checkSum(tramaarchivo.toString()) + "|");

        //System.out.println("NuevoPagoImpuestos: Se guarda el archivo, se envia al web red.");
        //System.out.println("NuevoPagoImpuestos: tramaarchivo = " + tramaarchivo);
        //System.out.println("NuevoPagoImpuestos: tramaWebRed = " + tramaWebRed);

        if (!creaArchivo(clave_usuario + ".tmp", Global.DIRECTORIO_LOCAL,
                tramaarchivo.toString())) {
            mensaje[0] = IEnlace.MSG_PAG_NO_DISP;
            try {
//            	<vswf:meg cambio de NASApp por Enlace 08122008>
                despliegaPaginaErrorURL(mensaje[0],
                        "Nuevo Esquema de Pago de Impuestos ",
                        "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
                        "s26170h", "/Enlace/" + Global.WEB_APPLICATION
                                + "/csaldo1?prog=0", req, res);
            } catch (Exception e) {
            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }
            return null;
        }
        boolean res_ = false;
        try {
            res_ = archRem.copiaLocalARemoto(((String) clave_usuario + ".tmp"),
                    Global.DIRECTORIO_LOCAL);
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }

        //System.out.println("NuevoPagoImpuestos: Regresa la copia del archivo temporal al remoto. res_ = " + res_);

        if (!res_) {
            //System.out.println("NuevoPagoImpuestos: No se realizo la copia remota. res_ = " + res_);
            mensaje[0] = "Error  no se pudo crear archivo remoto para validar el pago";
            try {
            	//<vswf:meg cambio de NASApp por Enlace 08122008>
                despliegaPaginaErrorURL(mensaje[0],
                        "Nuevo Esquema de Pago de Impuestos ",
                        "Servicios &gt; Nuevo Esquema de Pago de Impuestos",
                        "s26170h", "/Enlace/" + Global.WEB_APPLICATION
                                + "/csaldo1?prog=0", req, res);
            } catch (Exception e) {
            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }
            return null;
        }
        try {
            mx.altec.enlace.servicios.ServicioTux tuxGlobal = new mx.altec.enlace.servicios.ServicioTux();
            /*tuxGlobal
                    .setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext())
                            .getContext());*/
            Hashtable hs = tuxGlobal.web_red(tramaWebRed.toString());

            //System.out.println("NuevoPAgoImpuestos: tramaWebRed = " + tramaWebRed);

            coderror = (String) hs.get("BUFFER");
            //System.out.println("NuevoPAgoImpuestos: coderror = " + coderror);
        } catch (java.rmi.RemoteException re) {
            //System.out.println("NuevoPagoImpuestos: exception " + re);
        	EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
        } catch (Exception e) {
        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
        if (coderror == null){
            coderror = "";
		}
        return coderror;
    }

    /**
	 * Autor: VSWF - Rodrigo S�nchez  Descripcion : Ordena los conceptos de Diesel Marino y Subsidios
	 * @param sTmp  trama con los conceptos Diesel Marino y Subsidios
	 * @return conceptos Diesel Marino y Subsidios ordenados
     */

  String ordenaDiSub(String sTmp){
  //vswf Seccin en donde se ordenan los datos a salir de la cadena original en el recibo PDF
      int i=0;
      int i1=0;
      int i2=0;
      int i3=0;
      StringBuffer sTmp1=new StringBuffer("");
      StringBuffer sTmp2=new StringBuffer("");
      StringBuffer sTmp3=new StringBuffer("");
      StringBuffer sTmp4=new StringBuffer("");
      String strTemp="";
      //se recorre la cadena original para ordenar cantidad a pagar con diesel y subsidios
      while(i1>-1){
         i1 = sTmp.indexOf(INDEX_02);
         if(i1 > -1){
              strTemp=sTmp.substring(i, sTmp.indexOf("|",sTmp.indexOf(INDEX_02,i1)+1 )+1 );
              sTmp3.append(sTmp.substring(strTemp.length()) );
              i2=strTemp.indexOf(INDEX_35);
              if(i2>-1){
                sTmp2.append(sTmp.substring(strTemp.indexOf(INDEX_35,i2)-3,  strTemp.indexOf("|", strTemp.indexOf(INDEX_35,i2)+1 ))+"|");
              }
              i2=strTemp.indexOf(INDEX_36);
              if(i2>-1){
                sTmp2.append(sTmp.substring(strTemp.indexOf(INDEX_36,i2)-3,  strTemp.indexOf("|", strTemp.indexOf(INDEX_36,i2)+1 ))+"|");
              }
              i2=strTemp.indexOf(INDEX_37);
              if(i2>-1){
                sTmp2.append(sTmp.substring(strTemp.indexOf(INDEX_37,i2)-3,  strTemp.indexOf("|", strTemp.indexOf(INDEX_37,i2)+1 ))+"|");
              }
              strTemp=elimElto(strTemp,sTmp2.toString());
              //vamos armando la cadena final, se extrae la subcadena sin diesel y/o subsidios si vienen
              //ahora se el final del pago para meter los conceptos a ordenar
              sTmp4.append(strTemp.substring(i, strTemp.lastIndexOf("|",strTemp.lastIndexOf(INDEX_02,i1)+1 )+1 ));
              i3=strTemp.lastIndexOf("|",strTemp.lastIndexOf(INDEX_02,i1)+1 )+1 ;
              sTmp1.append(sTmp4+sTmp2.toString()+strTemp.substring(i3,strTemp.length()));
              sTmp4.delete(0,sTmp4.length());
              sTmp2.delete(0,sTmp2.length());
              sTmp=sTmp3.toString();
              i1=sTmp.indexOf(INDEX_02);
              if(i1>-1){
				  sTmp3.delete(0,sTmp3.length());
			  }
              i1=sTmp.indexOf(INDEX_02);
				  }//fin i1>-1
        }//fin while
      sTmp2.append(ordDiSubFin(sTmp3.toString()));
      sTmp1.append(sTmp2.toString());
      strTemp=null;
      sTmp=null;
      sTmp2=null;
      sTmp3=null;
      sTmp4=null;
    return sTmp1.toString();

  }


  /**
   * Descripcion: Ordena los datos de la parte final del pago para diesel marino y subsidios
   * @param cad   Datos de entrada : cadena con la parte final del pago
   * @return      Datos de salida : cadena ya ordenada
   */
  String ordDiSubFin(String cad){
   int i2=0;
   StringBuffer sTmp=new StringBuffer("");
     i2=cad.indexOf(INDEX_35);
     if(i2>-1){
        sTmp.append(cad.substring(cad.indexOf(INDEX_35,i2)-3,  cad.indexOf("|", cad.indexOf(INDEX_35,i2)+1 ))+"|");
      }
     i2=cad.indexOf(INDEX_36);
     if(i2>-1){
        sTmp.append(cad.substring(cad.indexOf(INDEX_36,i2)-3,  cad.indexOf("|", cad.indexOf(INDEX_36,i2)+1 ))+"|");
      }
     i2=cad.indexOf(INDEX_37);
     if(i2>-1){
        sTmp.append(cad.substring(cad.indexOf(INDEX_37,i2)-3,  cad.indexOf("|", cad.indexOf(INDEX_37,i2)+1 ))+"|");
      }
  cad=elimElto(cad,sTmp.toString());
  int i4=cad.lastIndexOf("|",cad.lastIndexOf("40006=")+1 )+1 ;
  return (cad.substring(0, cad.lastIndexOf("|",cad.lastIndexOf("40006=")+1 )+1 )+sTmp.toString()+cad.substring(i4,cad.length()));
  }

  /**
   * Descripcion : Elimina elementos de la cadena
   * @param sTmp  Datos Entrada: cadena a modificar
   * @param sTmp2 y la subcadena a eliminar
   * @return      cadena modificada
   */
  String elimElto(String sTmp,String sTmp2){
      	  int index1=sTmp.indexOf(sTmp2);
      	  StringBuffer str1=new StringBuffer("");
      	  if(index1>-1){
       	   str1.append(sTmp.substring(0,index1)+ sTmp.substring(index1+sTmp2.length()));
       	   return str1.toString();
      	  }
      	  else{
			  return sTmp;
		  }
    	 }

}