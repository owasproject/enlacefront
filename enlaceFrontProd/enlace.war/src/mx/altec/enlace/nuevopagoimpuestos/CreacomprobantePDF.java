/**
 * PROYECTO: Recaudaci�n de Impuestos Federales VERSION: 4.0
 * FECHA: Septiembre 2005
 *
 * Vision Software Factory VSWF
 *
 * Descripci�n: Comprobante PDF en base a los datos solicitados, al SAT de un
 * recibo bancario.
 *
 * @author Griselda Ram�rez Espinosa GRE
 * @version 1.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: Septiembre 2005
 * Modificaci�n: Adaptaciones correspondientes a las
 * especificaciones t�cnicas del SAT versi�n 4.0
 *
 * Nombre del programador: Leonel Popoca
 * Fecha: 22 - Dic - 05
 * Modificaci�n: Se elimin� el c�digo muerto
 *
 */

package mx.altec.enlace.nuevopagoimpuestos;
import java.io.*;
import java.util.*;//AVG
import java.awt.Color;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import javax.servlet.ServletOutputStream;

import mx.altec.enlace.utilerias.Global;

public class CreacomprobantePDF {

    public static final String CLASS_NAME = CreacomprobantePDF.class.getName();

    public static final String FDCL_ID = "Fecha de Declaraci�n del Ejercicio:";

    public static final String NORS_ID = "No. de Operaci�n de recibido en el SAT:";

    String concepto = null;

    String conDictamen = "NO";

    String curp = "";

    String fecha = "";

    String hora = "";

    String importeTotal = "";

    String nombre = "";

    String namePDF = "";

    String numOperacion = "";

    String numSerie = "";

    String plantilla = "";

    String plaza = "";

    String rfc = "";

    String sello = "";

    String tipoPersona = "";

    String tipoPago = "";

    String trama = "";

    String tramaImp = "";

	  String cadOriginal=""; //VSWF variable para vaciar la cadena original a imprimir en el recibo

    String TotalEfecPag = "";
    
    String dependencia = "";

    private boolean provisional;

    private boolean morales;

    boolean masConceptos = true;

    // VSWF GRE 20 - Sep - 05

    int numDatos;

    int numOpera;

    int pageNumber;

    String UrlWaterMark;

    ByteArrayOutputStream bytesPDF = new ByteArrayOutputStream();

    ServletOutputStream output;

    PdfTemplate template;

    BaseFont bf = null;

    PdfWriter writer;

    private String nors = null;

    private String fdcl = null;

    private String ptu = null;

    private String nombreClase = "";

    private String stringbuffer = "";

    private String llavePago = "";

    public void setLlavePago(String llave) {

        this.llavePago = llave;
    }
   // VSWF GRE 20 -Sep - 05 para agregar la llave de pago al pdf y la cadena original para impresion
    public String getLlavePago() {

        return this.llavePago;
    }

    public void setCadOriginal(String cadOrig) {

        this.cadOriginal = cadOrig;
    }

    // VSWF 27 - Oct - 05 se agrega el canal de procedencia

    private String canal;

    public void setCanal(String s) {

        this.canal = s;
    }

    public String getCanal() {

        return this.canal;
    }

    /**
     * VSWF GRE 20 - Sep - 05 M�todos correspondientes a los archivos de Enlace
     * Internet y SuperNet a los cuales no se les realizo alguna modificaci�n.
     */
    public void setTramaDatos(String pTrama) {

        this.trama = pTrama;
    }

    public void setTramaDatosImprimir(String pTramaImp) {

        this.tramaImp = pTramaImp;
    }

    public void setSelloDigital(String pSello) {

        this.sello = pSello;
    }

    public void setFechaPago(String pFechaPago) {

        this.fecha = pFechaPago;
    }

    public void setHoraPago(String pHoraPago) {

        this.hora = pHoraPago;
    }

    public void setNumOperacion(String pNumOperacion) {

        this.numOperacion = pNumOperacion;
    }

    public void setPlaza(String pPlaza) {

        this.plaza = pPlaza;
    }

    public void setNumSerie(String pNumSerie) {

        this.numSerie = pNumSerie;
    }

    public void setGifComprobante(String pGifComprobante) {

        this.plantilla = pGifComprobante;
    }

    public void setTipoPago(String tipoPago) {

        this.tipoPago = tipoPago;
    }

    public void setConDictamen(String dictamen) {

        this.conDictamen = dictamen;
    }

    public ByteArrayOutputStream getBytesPDF() {

        return this.bytesPDF;
    }

    public String obtieneImporteTotal() {

        return TotalEfecPag;
    }

    public String nombrePDF() {

        return namePDF;
    }

    /**
     * VSWF GRE 20 - Sep - 05 M�todos que corresponden al archivo Enlace
     * internet.
     */
    public void setProvisional(boolean provisional) {

        this.provisional = provisional;
    }

    public void setMorales(boolean morales) {

        this.morales = morales;
    }

    public boolean isProvisional() {

        return this.provisional;
    }

    public boolean isMorales() {

        return this.morales;
    }

    /**
     * VSWF GRE 20 - Sep - 05 M�todos que corresponden al archivo SuperNet.
     */
    private int getPageNumber() {

        return pageNumber;
    }

    private void setPageNumber(int pagNumber) {

        pageNumber = pagNumber;
    }

    /**
     * VSWF GRE 22 - Sep - 05 Funci�n que obtiene el RFC del arreglo de argumentos de Enlace Internet.
     * @return s Retorna el RFC para Enlace Internet
     */

    public String obtieneRFC() {

        String s = "";
        if (((trama.indexOf("RFC:")) != -1)
                || ((trama.indexOf("R.F.C.:")) != -1)) {

            int rfc_ini = trama.indexOf("RFC:");
            if (rfc_ini == -1) {
                rfc_ini = trama.indexOf("R.F.C.:") + 7;
            } else {
                rfc_ini = rfc_ini + 4;
            }

            int rfc_fin = trama.indexOf('|', rfc_ini);

            if (rfc_ini > 0) {
                s = trama.substring(rfc_ini, rfc_fin);
            }
        }
        System.out.println("CreacomprobantePDF: RFC = " + s);
        return s;
    }

    /**
     * VSWF GRE 22 - Sep - 05 Funcion que obtiene el RFC del arreglo de argumentos SuperNet.
     * @return sRFC para SuperNet 
     */

    public String obtieneRFC(String[] args) {
        String sRFC = "";
        if (args[0].indexOf("RFC") > -1 || args[0].indexOf("R.F.C.") > -1) {
            sRFC = args[0]
                    .substring(args[0].indexOf(':') + 1, args[0].length());
        }
        return sRFC;
    }

    /**
     * VSWF GRE 22 - Sep - 05 Funcion que obtiene el CURP del arreglo de argumentos Enlace Internet.
     * @return Retorna el CURP Enlace Internet
     */
    public String obtieneCURP() {
        String strCURP = "";
        if ((trama.indexOf("CURP:")) != -1) {
            int curp_ini = trama.indexOf("CURP:") + 5;
            int curp_fin = trama.indexOf('|', curp_ini);
            if (curp_ini > 0) {
                strCURP = trama.substring(curp_ini, curp_fin);
            }
        }
        System.out.println("CreacomprobantePDF: CURP = " + strCURP);
        return strCURP;
    }

    /**
     * VSWF GRE 22 - Sep - 05 Funcion que obtiene el CURP del arreglo de argumentos SuperNet.
     * @param args Recibe un arreglo
     * @return Retorna el curp para SuperNet 
     */
    public String obtieneCURP(String[] args) {
        String sCURP = "";
        for (int i = 0; i < args.length; i++) {
            if (args[i].indexOf("CURP") > -1) {
                sCURP = args[i].substring(args[i].indexOf(':') + 1, args[i]
                        .length());
                break;
            }
        }
        return sCURP;
    }

    /**
     * VSWF GRE 22 - Sep - 05 Metodo correspondiente a los archivos Enlace
     * @param args Internet y SuperNet. Funcion que obtiene el nombre completo ya sea
     * @return persona f�sica o moral del arreglo de argumentos.
     */
    public String obtieneNombre(String[] args) {
        String sApPaterno = "";
        String sApMaterno = "";
        String sNombre = "";
        String sNombreCompleto = "";
        String nom = "";
        String apm = "";
        int posicion = 0;
        nom = (args[posicion].substring(0, args[posicion].indexOf(':'))).trim();
        while (!nom.equals("Apellido paterno")
                && !nom.equals("Apellido Paterno")
                && nom.indexOf("Denominaci") < 0) {
            posicion++;
            nom = (args[posicion].substring(0, args[posicion].indexOf(':')))
                    .trim();
        }
        nom = args[posicion].substring(0, args[posicion].indexOf(':'));
		if(args[posicion + 1].length()> 1 ){
			apm = args[posicion + 1].substring(0, args[posicion + 1].indexOf(':'));
		}
		else{
			apm=" ";
		}

        System.out.println("CreacomprobantePDF: Dato[" + posicion + "] = " + nom);

        if (nom.trim().equals("Apellido Paterno")
                || nom.trim().equals("Apellido paterno")) {
            sApPaterno = args[posicion].substring(
                    args[posicion].indexOf(':') + 1, args[posicion].length());
            if (apm.trim().equals("Apellido Materno")
                    || apm.trim().equals("Apellido materno")) {
                sApMaterno = args[posicion + 1].substring(args[posicion + 1]
                        .indexOf(':') + 1, args[posicion + 1].length());
                sNombre = args[posicion + 2].substring(args[posicion + 2]
                        .indexOf(':') + 1, args[posicion + 2].length());
            } else {
                if (apm.trim().equals("Nombre")) {
                    sNombre = args[posicion + 1].substring(args[posicion + 1]
                            .indexOf(':') + 1, args[posicion + 1].length());
                }
            }
            // ISBAN IOV Se cambia el orden de concatenaci�n para el nombre a Ap, Am y Nom
            sNombreCompleto = sApPaterno + " " + sApMaterno + " " + sNombre;
            System.out.println("CreacomprobantePDF: nombre completo = " + sNombreCompleto);
            tipoPersona = "F";
        } else {
            sNombre = args[posicion].substring(args[posicion].indexOf(':') + 1,
                    args[posicion].length());
            sNombreCompleto = sNombre;
            tipoPersona = "M";
        }
        return sNombreCompleto;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * @param args Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @return Funcion que obtiene el total pagado para personas fisicas del arreglo de
     * argumentos
     */
    public String obtieneTotalPagado(String[] args) {
        String sTotal = "";
        int posicion = 0;
        String nom = (args[posicion].substring(0, args[posicion].indexOf(':')))
                .trim();
        while (nom.indexOf("Total") < 0) {
            posicion++;
            nom = (args[posicion].substring(0, args[posicion].indexOf(':')))
                    .trim();
        }
        sTotal = args[posicion].substring(args[posicion].indexOf(':') + 1,
                args[posicion].length());
        System.out.println("CreacomprobantePDF: Total[" + posicion + "] = " + sTotal);
        return sTotal;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * @param args Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @return Funcion que obtiene el total pagado para personas morales del arreglo de argumentos
     */
    public String obtieneTotalPagadoMoral(String[] args) {
        String sTotal = "";
        int posicion = 0;
        String nom = (args[posicion].substring(0, args[posicion].indexOf(':')))
                .trim();
        while (nom.indexOf("Total") < 0) {
            posicion++;
            nom = (args[posicion].substring(0, args[posicion].indexOf(':')))
                    .trim();
        }
        sTotal = args[posicion].substring(args[posicion].indexOf(':') + 1,
                args[posicion].length());
        System.out.println("CreacomprobantePDF: args[" + posicion + "] = " + sTotal);
        return sTotal;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @param trama
     * @return
     */
    public String obtienePTU(String trama) {
        String sPTU = null;
        String sTramo = null;
        try {
            if (trama.indexOf("P.T.U.:") < 0) {
                return null;
            }
            sTramo = trama.substring(trama.indexOf("P.T.U.:") + 7);
            System.out.println("CreacomprobantePDF: PTU = " + sTramo);
            sPTU = sTramo.substring(0, sTramo.indexOf("|"));
            return sPTU;
        } catch (Exception exception) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
        }

        return null;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @param trama
     * @retun 
     */
    public String obtieneFDCL(String trama) {
        String sFDCL = null;
        String sTramo = null;
        try {
            if (trama.indexOf(FDCL_ID) < 0) {
                return null;
            }
            sTramo = trama.substring(trama.indexOf(FDCL_ID) + FDCL_ID.length());
            System.out.println("CreacomprobantePDF: FDCL = " + sTramo);
            sFDCL = sTramo.substring(0, sTramo.indexOf("|"));
            return sFDCL;
        } catch (Exception exception) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
        }
        return null;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @param trama
     * @return
     */
    public String obtieneNORS(String trama) {
        String sNORS = null;
        String sTramo = null;
        try {
            if (trama.indexOf(NORS_ID) < 0) {
                return null;
            }
            sTramo = trama.substring(trama.indexOf(NORS_ID) + NORS_ID.length());
            System.out.println("CreacomprobantePDF: NORS = " + sTramo);
            sNORS = sTramo.substring(0, sTramo.indexOf("|"));
            return sNORS;
        } catch (Exception exception) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
        }

        return null;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     * @param campos Obtiene los campos de la trama y los pone en una Hashtable AVG
     * @return 
     */
    public Hashtable getCampos(String[] campos) {
        Hashtable hstElementos = new Hashtable();
        int iCampos = 0;
        boolean bIva = false;
        boolean bDPA = false;
        boolean bDependencia = false;
        boolean bCadDependencia = false;

        for (iCampos = 0; iCampos < campos.length; iCampos++) {
            if (campos[iCampos].equals("IVA actos accidentales")) {
                bIva = true;
                break;
            }
        }
        if (bIva) {
            String stKeyIVA0 = campos[iCampos];
            String stValueIVA0 = "";
            System.out.println("CreacomprobantePDF: stKeyIVA0 = " + stKeyIVA0);
            System.out.println("CreacomprobantePDF: stValueIVA0 = " + stValueIVA0);
            hstElementos.put(stKeyIVA0, stValueIVA0);
            iCampos++;
            for (; iCampos < campos.length; iCampos++) {
                int iPos = campos[iCampos].indexOf(":");
                if (iPos == -1)
                    break;//vswf se valida que se haya recorrido todo sin
                          // contar el EOF
                if (campos[iCampos]
                        .equals("Derechos, Productos y Aprovechamientos")) {
                    break;
                }
                String stKey = campos[iCampos].substring(0, iPos) + "IVA";
                String stValue = campos[iCampos].substring(iPos + 1);
                System.out.println("Creacomprobante: Llave = " + stKey);
                System.out.println("Creacomprobante: Valor = " + stValue);
                hstElementos.put(stKey, stValue);
            }
        }

        for (iCampos = 0; iCampos < campos.length; iCampos++) {
            if (campos[iCampos]
                    .equals("Derechos, Productos y Aprovechamientos")) {
                bDPA = true;
                break;
            }
        }
        if (bDPA) {
            String stKeyDPA0 = new String(campos[iCampos]);
            String stValueDPA0 = new String("");
            System.out.println("CreacomprobantePDF: stKeyDPA0 = " + stKeyDPA0);
            System.out.println("CreacomprobantePDF: stValueDPA0 = " + stValueDPA0);
            hstElementos.put(stKeyDPA0, stValueDPA0);
            iCampos++;
            for (; iCampos < campos.length - 1; iCampos++) {
                int iPos = campos[iCampos].indexOf(":");
                if (iPos == -1)
                    break;//vswf se valida que se haya recorrido todo sin
                          // contar el EOF
                String stKey = campos[iCampos].substring(0, iPos) + "DPA";
                String stValue = campos[iCampos].substring(iPos + 1);
                System.out.println("CreacomprobantePDF: Llave = " + stKey);
                System.out.println("CreacomprobantePDF: Valor = " + stValue);
                hstElementos.put(stKey, stValue);
            }

        }
        for (iCampos = 0; iCampos < campos.length; iCampos++) {
            int ipos = campos[iCampos].indexOf(":"); //vswf se valida la
                                                     // busqueda de la etiqueta
                                                     // Dependencia
            if (ipos > -1) { //vswf se recorre hasta el EOF
                if ((campos[iCampos].substring(0, campos[iCampos].indexOf(":")))
                        .equals("Dependencia")) {
                    bDependencia = true;
                    break;
                }
            }
        }
        if (bDependencia) {
            String stKeyD = campos[iCampos].substring(0, campos[iCampos]
                    .indexOf(":"));
            String stValueD = campos[iCampos].substring(campos[iCampos]
                    .indexOf(":") + 1);
            System.out.println("CreacomprobantePDF: stKeyD = " + stKeyD);
            System.out.println("CreacomprobantePDF: stValueD = " + stValueD);
            hstElementos.put(stKeyD, stValueD);
        }
        for (iCampos = 0; iCampos < campos.length; iCampos++) {
            int ipos = campos[iCampos].indexOf("Cadena de la Dependencia");
            if (ipos > -1) {
                if ((campos[iCampos].substring(0, campos[iCampos].indexOf(":")))
                        .equals("Cadena de la Dependencia")) {
                    bCadDependencia = true;
                    break;
                }
            }
        }
        if (bCadDependencia) {
            String stKeyCD = campos[iCampos].substring(0, campos[iCampos]
                    .indexOf(":"));
            String stValueCD = campos[iCampos].substring(campos[iCampos]
                    .indexOf(":") + 1);
            System.out.println("CreacomprobantePDF: stKeyCD = " + stKeyCD);
            System.out.println("CreacomprobantePDF: stValueCD = " + stValueCD);
            hstElementos.put(stKeyCD, stValueCD);
        }
        return hstElementos;
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     */
    public void LeeHashtable(Hashtable hsLeer) {
        Enumeration eKeys = hsLeer.keys();
        while (eKeys.hasMoreElements()) {
            String stKey = (String) eKeys.nextElement();
            String stValue = (String) hsLeer.get(stKey);
            System.out.println("CreacomprobantePDF: stKey = " + stKey);
            System.out.println("CreacomprobantePDF: stValue = " + stValue);
        }
    }

    /**
     * VSWF GRE 22 - Sep - 05 M�todo correspondiente a los archivos de Enlace
     * Internet y SuperNet y al cual no se le realizo alguna modificaci�n.
     */
    public String getRazonSocial(String args[]) {
        String sRazonS = "";
        for (int i = 0; i < args.length; i++) {

            if (args[i].indexOf("Denominaci�n o raz�n social") > -1) {
                sRazonS = args[i].substring(args[i].indexOf(':') + 1, args[i]
                        .length());
                break;
            }
        }
        return sRazonS;
    }

    /**
     * VSWF GRE 23 - Sep - 05 Esta clase es auxiliar para poder mantener los
     * eventos generados por la creacion del comprobante PDF correspondiente a
     * los archivos de Enlace Internet y SuperNet.
     */
    private class MyPageEvents extends PdfPageEventHelper {

        private String nombre;

        private String rfc;

        private String curp;

        private String plaza;

        private String fecha;

        private String numOperacion;

        private String TotalEfecPag;

        private String sRazon = "";

        private boolean masConceptos;

        private boolean bolCloseDocument = false;

        private int lastPage = 0;

        private int pageN = 0;

        // VSWF GRE 25 - Sep - 05 para agregar la llave de pago al pdf
        private String llavePago = "";
        
        private String dependencia = "";

        BaseFont bf = null;

        MyPageEvents(String nombre, String rfc, String curp, String plaza,
                String fecha, String numOperacion, String TotalEfecPag,
                boolean masConceptos, String llave, String dependencia) {
            this.nombre = nombre;
            this.rfc = rfc;
            this.curp = curp;
            this.plaza = plaza;
            this.fecha = fecha;
            this.numOperacion = numOperacion;
            this.TotalEfecPag = TotalEfecPag;
            this.masConceptos = masConceptos;
            this.llavePago = llave;
            this.dependencia = dependencia;
        }

        MyPageEvents(String nombre, String rfc, String curp, String plaza,
                String fecha, String numOperacion, String TotalEfecPag,
                boolean masConceptos, String sRazon, String llave, String dependencia) {
            this.nombre = nombre;
            this.rfc = rfc;
            this.curp = curp;
            this.plaza = plaza;
            this.fecha = fecha;
            this.sRazon = sRazon;
            this.numOperacion = numOperacion;
            this.TotalEfecPag = TotalEfecPag;
            this.masConceptos = masConceptos;
            this.llavePago = llave;
            this.dependencia = dependencia;
        }

        PdfContentByte cb;

        PdfTemplate template;

        public void setEventStatus(boolean actualStatus) {
            this.bolCloseDocument = actualStatus;
        }

        public boolean getEventStatus() {
            return bolCloseDocument;
        }

        public void onStartPage(PdfWriter writer, Document document) {
            try {
                PdfPTable aHTable = new PdfPTable(2);
                aHTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                aHTable.getDefaultCell().setPadding(3);
                aHTable.getDefaultCell().setHorizontalAlignment(
                        Element.ALIGN_LEFT);
                PdfPTable headerSTable = new PdfPTable(2);
                headerSTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                headerSTable.getDefaultCell().setPadding(3);
                headerSTable.getDefaultCell().setHorizontalAlignment(
                        Element.ALIGN_RIGHT);
                headerSTable.addCell(" ");
                Phrase titulo = null;
                Phrase frase = null;
                titulo = new Phrase("Plaza :  " + plaza, new Font(
                        Font.HELVETICA, 10, Font.BOLD));
                headerSTable.addCell(titulo);
                headerSTable.addCell(" ");

                // VSWF GRE 23 - Sep - 05
                // Valida el canal para conocer la sucursal
                if (canal.equals("enlace")) {
                    titulo = new Phrase("Sucursal :  0981", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                } else {
                    titulo = new Phrase("Sucursal :  0890", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                }

                headerSTable.addCell(titulo);
                document.add(headerSTable);
                document.add(new Paragraph(" "));
                PdfPTable id = new PdfPTable(2);
                id.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                id.getDefaultCell().setPadding(3);
                id.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
                id.addCell(" ");
                id.addCell(" ");
                frase = new Phrase("R.F.C. : ", new Font(Font.HELVETICA, 10,
                        Font.BOLD));
                id.addCell(frase);
                frase = new Phrase(rfc, new Font(Font.HELVETICA, 10,
                        Font.NORMAL));
                id.addCell(frase);

                if (!curp.equals("")) {// VSWF GRE 26 - Sep - 05 .......
                    //Valida CURP basaso en el archivo de SN
                    frase = new Phrase("CURP. : ", new Font(Font.HELVETICA, 10,
                            Font.BOLD));
                    id.addCell(frase);
                    frase = new Phrase(curp, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    id.addCell(frase);
                }
                if (sRazon.equals("")) {
                    frase = new Phrase("Nombre : ", new Font(Font.HELVETICA,
                            10, Font.BOLD));
                    id.addCell(frase);
                    frase = new Phrase(nombre, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    id.addCell(frase);
                } else {
                    frase = new Phrase("Denominaci�n o raz�n social : ",
                            new Font(Font.HELVETICA, 10, Font.BOLD));
                    id.addCell(frase);
                    frase = new Phrase(sRazon, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    id.addCell(frase);
                }
                //ISBAN IOV se Cambia la leyenda a Fecha y Hora del Pago
                frase = new Phrase("Fecha y Hora del Pago : ", new Font(Font.HELVETICA,
                        10, Font.BOLD));
                id.addCell(frase);
                frase = new Phrase(formatFecha(fecha.replace('-', '/').trim()) + "  " + hora +" hrs.",
                        new Font(Font.HELVETICA, 10, Font.NORMAL));
                id.addCell(frase);
                frase = new Phrase("N�mero de Operaci�n :  " + numOperacion,
                        new Font(Font.HELVETICA, 10, Font.BOLD));
                id.addCell(frase);

                // VSWF GRE 26 - Sep - 05 para agregar la llave de pago al pdf
                frase = new Phrase("Llave de pago :  " + llavePago, new Font(
                        Font.HELVETICA, 10, Font.BOLD));
                id.addCell(frase);

                frase = new Phrase("Total Efectivamente Pagado :   $ "
                        + TotalEfecPag, new Font(Font.HELVETICA, 10, Font.BOLD));
                id.addCell(frase);
                
                frase = new Phrase("", new Font(Font.HELVETICA, 10, Font.BOLD));
                id.addCell(frase);
                
                if (tipoPago.equals("010")) {
	                frase = new Phrase("Dependencia : ", new Font(Font.HELVETICA, 10, Font.BOLD));
	                id.addCell(frase);
	                
	                frase = new Phrase(dependencia, new Font(Font.HELVETICA, 10, Font.BOLD));
	                id.addCell(frase);
                }
                
                if (masConceptos) {
                    frase = new Phrase("Por los conceptos siguientes : ",
                            new Font(Font.HELVETICA, 10, Font.BOLD));
                    id.addCell(frase);
					id.addCell(" ");
                }

				id.addCell(" ");
				id.addCell(" ");

				document.add(id);
                //document.add(new Paragraph(" "));
                masConceptos = false;
                System.out.println("CreacomprobantePDF: getEventStatus = " + getEventStatus());
                if (getEventStatus() == true) {
                    onCloseDocument(writer, document);
                }
            } catch (Exception exception) {
                System.out.println("CreacomprobantePDF: Excepci�n onStartPage =" + exception);
            }
        }

        public void onEndPage(PdfWriter writer, Document document) {
            if (bolCloseDocument == true) {
                lastPage = pageN + 1;
                pageN = lastPage;
            } else {
                pageN = writer.getPageNumber();
                lastPage = pageN + 1;
            }
            String text = "Pagina " + pageN + " de ";
            float len = bf.getWidthPoint(text, 12);
            cb.beginText();
            cb.setFontAndSize(bf, 12);
            cb.setTextMatrix(180, 30);
            cb.showText(text);
            cb.endText();
            cb.addTemplate(template, 180 + len, 30);
        }

        public void onOpenDocument(PdfWriter writer, Document document) {
            try {
                bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252,
                        BaseFont.NOT_EMBEDDED);
                cb = writer.getDirectContent();
                template = cb.createTemplate(100, 50);
            } catch (DocumentException de) {
				System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + de);
            } catch (IOException ioe) {
				System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + ioe);
            }
        }

        public void onCloseDocument(PdfWriter writer, Document document) {
            onEndPage(writer, document);
            template.beginText();
            template.setFontAndSize(bf, 12);
            if (bolCloseDocument == false) {
                template.showText(String.valueOf(writer.getPageNumber() - 1));
            } else {
                template.showText(String.valueOf(lastPage));
            }
            template.endText();
        }
    }

    /**
     * VSWF GRE 26 - Sep - 05 M�todo desentramaDatos correspondiente al archivo
     * SuperNet.
     */
    public void desentramaDatos() {
        //VSWF GRE 26 - Sep - 05
        //Asignar null antes de la validaci�n tipoPersona al arreglo de datos
        String[] datos = null;
		trama = toASCII(trama);
        if (tipoPago.equals("005")) {
            this.nors = obtieneNORS(trama);
            this.fdcl = obtieneFDCL(trama);
            this.ptu = obtienePTU(trama);
        }

        // VSWF GRE 26 - Sep - 05 Valida canal.
        if (canal.equals("supernet")) {
            System.out.println("CreacomprobantePDF: Trama en desentramaDatos = " + trama);
            int indice = trama.indexOf('|');
            int numSeparador = 0;
            for (int k = 0; k < trama.length(); k++) {
                if (trama.charAt(k) == '|') {
                    numSeparador++;
                }
            }
            datos = new String[numSeparador];
            String temp = "";
            String temp1 = "";
            String fecha_temp = "";
            for (int i = 0; i < numSeparador || trama.length() < 0; i++) {
                temp = trama.substring(0, indice);
                if (temp.trim().indexOf("Cantidad a pagar") > -1) {
                    temp1 = "Cantidad pagada:"
                            + temp.substring(temp.indexOf(":") + 1);
                    datos[i] = temp1;
                } else {
                    if (temp.trim().indexOf(
                            "Fecha del monto pagado con anterioridad") > -1) {
                        fecha_temp = "Fecha del monto pagado con anterioridad:"
                                + temp.substring(temp.indexOf(":") + 1);
                        i--;
                    } else if (temp.trim().indexOf(
                            "Monto pagado con Anterioridad") > -1) {
                        datos[i] = fecha_temp;
                        i++;
                        datos[i] = trama.substring(0, indice);
                    } else {
                        datos[i] = trama.substring(0, indice);
                    }
                }
                trama = trama.substring(indice + 1);
                indice = trama.indexOf('|');
                numDatos = i;
            }
        }

        try {

            /**
             * VSWF GRE 26 - Sep - 05 Valida el canal donde los parametros
             * varian para Enlace Internet y SuperNet.
             */
            if (canal.equals("supernet")) {
                generarReciboPDF(datos);
            } else {
                generarReciboPDF(obtieneDatos(trama));
            }
        } catch (Exception exception) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
        }
    }

 /**
   *Descripcion : Convierte caracteres en Hexadecimal a ASCII para la salida del comprobante PDF
   *@param sVal Recibe la cadena con caracteres a modificar
   *@return Retorna la Cadena modificada
   */
  public String toASCII(String sVal){
  	char chChars[]={'�', '�', '�', '�', '�','�', '�', '�', '�', '�'};
  	String chChars2[]={"&#225;","&#233;","&#237;","&#243;","&#250;","&#193;","&#201;","&#205;","&#211;","&#218;"};
    StringBuffer str1 = new StringBuffer("");
    StringBuffer str2 = new StringBuffer("");
	int index=0;
	str1.append(sVal);
	for(int i=0;i<chChars2.length;i++){
      index=0;
      while(index >-1){
        index=sVal.indexOf(chChars2[i],index+1);
        if(index>-1){
         str1.replace(index,(index+6), String.valueOf(chChars[i]));
         sVal=null;
         sVal=str1.toString();
        }
      }
	 }
    return str1.toString();
  }
  
  /**
    * VSWF GRE 26 - Sep - 05 M�todo generarReciboPDF . de Enlace Internet.
    * @param campos genera el recibo PDF
    */
    public void generarReciboPDF(String[] campos) {
        if (tipoPago.equals("010")) {
            generarReciboPDF_DPA(campos);
        } else {
            Document document = new Document(PageSize.LETTER.rotate(), 36, 36, 36, 72);
            int ultimaLinea = 0;
            java.io.FileOutputStream outStream = null;

            try {
                // VSWF GRE 26 - Sep - 05
                // Valida el canal de acuerdo a EI y SN.
                if (canal.equals("enlace")) {
                    rfc = obtieneRFC();
                    curp = obtieneCURP();
                    namePDF = "ReciboSat" + fecha + numOperacion + ".pdf";
                    java.io.File tmpFile = new java.io.File(
                            Global.DIRECTORIO_LOCAL, namePDF);
                    System.out.println("CreacomprobantePDF: tmpFile = " + tmpFile.getAbsolutePath());
                    outStream = new java.io.FileOutputStream(tmpFile);
                } else {
                    rfc = obtieneRFC(campos);
                    curp = obtieneCURP(campos);
                }

                nombre = obtieneNombre(campos);
                if (tipoPersona.equals("F")) {
                    TotalEfecPag = obtieneTotalPagado(campos);
                } else {
                    TotalEfecPag = obtieneTotalPagadoMoral(campos);
                }

                // VSWF GRE 27 - Sep - 05
                // Valida tipo de persona de acuerdo a EI y SN.
                //PDFWriter writer = null;
                if (canal.equals("enlace")) {
                    writer = PdfWriter.getInstance(document, outStream);
                } else {
                    namePDF = "Recibo" + rfc + fecha + numOperacion + ".pdf";
                    writer = PdfWriter.getInstance(document, bytesPDF);
                }

				String sRazonS = "";
				sRazonS = getRazonSocial(campos);

				MyPageEvents events = new MyPageEvents(nombre, rfc, curp,
                        plaza, fecha, numOperacion, TotalEfecPag, true, sRazonS, this
                                .getLlavePago(), dependencia);
                writer.setPageEvent(events);
                //UrlWaterMark = "/ias/ias60/ias/APPS/enlaceMig/enlaceMig/jsp/nuevopagoimpuestos/";
                UrlWaterMark = Global.PATH_PNG;
                System.out.println("CreacomprobantePDF: WATERMARK PATH = " + UrlWaterMark);
                Watermark watermark = new Watermark(Image
                        .getInstance(UrlWaterMark + plantilla + ".png"), 2, 2);
                document.add(watermark);
                Phrase pagina = new Phrase("", new Font(Font.HELVETICA, 9,
                        Font.NORMAL));
                HeaderFooter footer = new HeaderFooter(pagina, false);
                footer.setBorder(Rectangle.NO_BORDER);
                if (canal.equals("enlace")) {
                    footer.setAlignment(Element.ALIGN_RIGHT);
                }
                document.setFooter(footer);
                document.open();
                Phrase frase = null;
                PdfPTable aTable = new PdfPTable(2);
                aTable = new PdfPTable(2);
                aTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
                aTable.getDefaultCell().setPadding(3);
                aTable.getDefaultCell().setHorizontalAlignment(
                        Element.ALIGN_LEFT);
                System.out.println("CreacomprobantePDF: numOpera = " + numOpera);
                System.out.println("CreacomprobantePDF: numDatos:" + numDatos);
                for (int m = 0; m < numDatos; m++) {
                    System.out.println("CreacomprobantePDF: Campos[" + m + "] = " + campos[m]);
                    if (campos[m].indexOf("Total efectivamente") > -1) {
                        numOpera = m + 1;
                        System.out.println("CreacomprobantePDF: numOpera = " + numOpera);
                        break;
                    }
                }

                /*
                 * VSWF Sustituye la cadena "A Cargo" por la cadena "Impuesto a Cargo"
                 * Sustituye la cadena "A Favor" por la cadena "Impuesto a Favor"
                 */
                for(int i=0; i<campos.length; i++){
                    if(campos[i].indexOf("A Cargo") > -1){
                        campos[i] = "Impuesto a Cargo" + campos[i].substring(campos[i].indexOf(":"));
                    }
                    if(campos[i].indexOf("A Favor") > -1){
                        campos[i] = "Impuesto a Favor" + campos[i].substring(campos[i].indexOf(":"));
                    }
                }

                /*
                 * VSWF - Mueve la cadena "Entidad Federativa de ubicaci�n del
                 * terreno y/o construcci�n" una posici�n despues de la cadena
                 * "ISR enajenaci�n de terrenos y/o construcciones" en pagos
                 * coordinados con entidades federativas
                 */
                if(tipoPago.equals("012")){
                    for(int i=campos.length-1; i>=0; i--){
                        if(campos[i].indexOf("Entidad Federativa de ubicaci�n del terreno y/o construcci�n") > -1){
                            while(campos[i - 1].indexOf("ISR enajenaci�n de terrenos y/o construcciones") == -1){
                                String tmp = campos[i];
                                campos[i] = campos[i - 1];
                                campos[i - 1] = tmp;
                                i--;
                            }
                            break;
                        }
                    }
                }

				for (int j = numOpera; j < numDatos; j++) {
                    ultimaLinea = j;
                    System.out.println("CreacomprobantePDF: campos[" + j + "] = " + campos[j]);

                    if (!(campos[j].indexOf("Total de Contribuciones") > -1)) {
                        if (!(campos[j].indexOf("Total de aplicaciones") > -1)) {
                            if (campos[j].indexOf(':') < 0) {
                                aTable.addCell(" ");
                                aTable.addCell(" ");
                                frase = new Phrase(campos[j], new Font(
                                        Font.HELVETICA, 10, Font.BOLD));
                                aTable.addCell(frase);
                                aTable.addCell(" ");
                            } else if (-1 < campos[j].indexOf("P.T.U.")
                                    || -1 < campos[j].indexOf(NORS_ID)
                                    || -1 < campos[j].indexOf(FDCL_ID)) {
                            } else {

                                // VSWF GRE 26 - Sep - 05
                                // valida el canal.

                                if (canal.equals("supernet")) {
                                    if (tipoPago.equals("010")) {
                                        if (!(campos[j].indexOf("Clave Contable") > -1)) {
                                            frase = new Phrase(campos[j].substring(0, campos[j].indexOf(':')) + " :", new Font(Font.HELVETICA, 10, Font.NORMAL));
                                            aTable.addCell(frase);
                                            frase = new Phrase(campos[j].substring(campos[j].indexOf(':') + 1, campos[j].length()), new Font(Font.HELVETICA, 8, Font.NORMAL));
                                            aTable.addCell(frase);
                                        }
                                    }
                                    else {
                                        // VSWF - No debe de aparecer en el recibo "N�mero de Operaci�n de recibido en el SAT"
                                        if(campos[j].indexOf("de recibido en el SAT") < 0){
                                            frase = new Phrase(campos[j].substring(0, campos[j].indexOf(':')) + " :", new Font(Font.HELVETICA, 10, Font.NORMAL));
                                            aTable.addCell(frase);
                                            frase = new Phrase(campos[j].substring(campos[j].indexOf(':') + 1, campos[j].length()), new Font(Font.HELVETICA, 8, Font.NORMAL));
                                            aTable.addCell(frase);
                                        }
                                        // VSWF
                                    }

                                } else {
                                    // VSWF - No debe de aparecer en el recibo "N�mero de Operaci�n de recibido en el SAT"
                                    if(campos[j].indexOf("de recibido en el SAT") < 0){
                                        frase = new Phrase(campos[j].substring(0, campos[j].indexOf(':')) + " :", new Font(Font.HELVETICA, 10, Font.NORMAL));
                                        aTable.addCell(frase);
                                        frase = new Phrase(campos[j].substring(campos[j].indexOf(':') + 1, campos[j].length()), new Font(Font.HELVETICA, 8, Font.NORMAL));
                                        aTable.addCell(frase);
                                    }
                                    // VSWF
                                }

                            }
                            if (j == numOpera) {
                                importeTotal = campos[j - 1].substring(
                                        campos[j - 1].indexOf(':') + 1,
                                        campos[j - 1].length());
                                System.out.println("CreacomprobantePDF: importeTotal =  " + importeTotal);
                            }
                        }
                    }
                }
                document.add(aTable);
                masConceptos = false;
                document.add(new Paragraph(" "));
                Table bTable = new Table(1);
                bTable.setBorderColor(new java.awt.Color(255, 255, 255));
                bTable.setBorder(Rectangle.NO_BORDER);
                Cell cell = null;

                /*
                if (tipoPago.equals("005")) {
                    if (this.ptu != null) {
                        frase = new Phrase(
                                "Participaci�n de los Trabajadores en las "
                                        + "Utilidades de la Empresa "
                                        + this.ptu, new Font(Font.HELVETICA,
                                        10, Font.BOLD));
                        cell = new Cell(frase);
                        cell.setBorderWidth(0.0f);
                        bTable.addCell(cell);
                    }
                    if (this.nors != null && this.fdcl != null) {
                        cell = new Cell(" ");
                        cell.setBorderWidth(0.0f);
                        bTable.addCell(cell);
                        frase = new Phrase(
                                "Manifiesto bajo protesta de decir verdad,"
                                        + " que el pago realizado, corresponde a la "
                                        + "informaci�n presentada ante el SAT con fecha"
                                        + this.fdcl
                                        + " y registrada con el No. de operaci�n "
                                        + this.nors, new Font(Font.HELVETICA,
                                        10, Font.NORMAL));
                        cell = new Cell(frase);
                        cell.setBorderWidth(0.0f);
                        bTable.addCell(cell);
                    }
                    cell = new Cell(" ");
                    cell.setBorderWidth(0.0f);
                    bTable.addCell(cell);
                }
                */

                // VSWF GRE 24 - Sep - 05

                if(tipoPago.equals("005")){
                    String fecha = getFecha(campos);
                    String numop = getNumOp(campos);

                    if(!fecha.equals("EOF") && !numop.equals("EOF")){
                    	String leyenda = "Manifiesto bajo protesta de decir verdad que el pago realizado, corresponde a la informaci�n presentada ante el SAT con fecha " + fecha + " y registrada con el No. de operaci�n " + numop;
                        frase = new Phrase(leyenda, new Font(Font.HELVETICA, 10,
                                Font.NORMAL));
                        cell = new Cell(frase);
                        cell.setBorderWidth(0.0f);
                        bTable.addCell(cell);
                        cell = new Cell(" ");
                        cell.setBorderWidth(0.0f);
                        bTable.addCell(cell);
                    }
                }

                // valida el canal.
                if (canal.equals("supernet")) {
                    this.tramaImp = obtenTramaImp(this.tramaImp);
                }

                frase = new Phrase("Cadena Original : ", new Font(Font.HELVETICA,
                        10, Font.BOLD));
                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);

                frase = new Phrase(" ", new Font(Font.HELVETICA, 10,
                        Font.NORMAL));
                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);

				frase = new Phrase(this.tramaImp, new Font(Font.HELVETICA, 10,
                        Font.NORMAL));

                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);
                cell = new Cell(" ");
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);

                frase = new Phrase("Sello Digital : ", new Font(Font.HELVETICA,
                        10, Font.BOLD));
                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);
                frase = new Phrase(" ", new Font(Font.HELVETICA, 10,
                        Font.NORMAL));
                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);
                frase = new Phrase("||" + sello + "||", new Font(
                        Font.HELVETICA, 10, Font.NORMAL));
                cell = new Cell(frase);
                cell.setBorderWidth(0.0f);
                bTable.addCell(cell);
                if (!writer.fitsPage(bTable)) {
                    document.newPage();
                }
                document.add(bTable);
                events.setEventStatus(true);
                events.onCloseDocument(writer, document);
                writer.setPageEvent(null);
                document.close();
            } catch (DocumentException de) {
				System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + de);
            } catch (IOException ioe) {
				System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + ioe);
            } finally {
                if (canal.equals("enlace")) {
                    if (null != outStream) {
                        try {
                            outStream.close();
                        } catch (Exception exception) {
							System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
                        }
                        outStream = null;
                    }
                }
            }
        }
    }


    /**
     * VSWF Obtiene la "Fecha de Declaraci�n" del arreglo Campos
     * @param campos
     * @return Retorna la fecha de declaraci�n
     */
     public String getFecha(String[] campos){

        String sfecha = "";

        for(int i=0; i<campos.length; i++){
            sfecha = campos[i];
            if(sfecha.indexOf("Fecha de Declaraci") > -1){
                sfecha = sfecha.substring(sfecha.indexOf(":") + 1);
                break;
            }
        }

        return sfecha;
     }


    /**
     * VSWF Obtiene el "N�mero de Operaci�n de recibido en el SAT" del arreglo Campos
     * @param campos
     * @return Regresa el n�mero de operacion del sat
     */
     public String getNumOp(String[] campos){

        String snumop = "";

        for(int i=0; i<campos.length; i++){
            snumop = campos[i];
            if(snumop.indexOf("de recibido en el SAT") > -1){
                snumop = snumop.substring(snumop.indexOf(":") + 1);
                break;
            }
        }

        return snumop;
     }
     
     /**
      * Metodo para formatear la fecha de pago
      * @param fecha Recibe la fecha que se va a formatear
      * @return	    Regresa la fecha con el formato dd/mm/aaaa
      **/
      public String formatFecha(String fecha){

    	  StringBuffer sfecha = new StringBuffer();
         	
         if (fecha.length() > 8 && (fecha.indexOf('-') > 0 ||  fecha.indexOf('/') > 0))
	 		{
	 			sfecha.append(fecha.substring (8,10))
	 				  .append('/').append(fecha.substring (5,7))
	 			      .append('/').append(fecha.substring(0,4));
	 		}
	 		else
	 		{
	 			sfecha.append(fecha.substring (6,8))
	 			      .append('/').append(fecha.substring (4,6))
	 			      .append('/').append(fecha.substring(0,4));
	 		}
	         return sfecha.toString();
      }

      /**
       * Metodo para formatear la fecha de pago
       * @param trama Recibe la fecha que se va a formatear
       * @return	    Regresa la fecha con el formato dd/mm/aaaa
       **/
      
      public String obtieneDependencia() {

    	  String strDep = "";
          if ((trama.indexOf("Dependencia:")) != -1) {
              int dep_ini = trama.indexOf("Dependencia:") + 12;
              int dep_fin = trama.indexOf('|', dep_ini);
              if (dep_ini > 0) {
                  strDep = trama.substring(dep_ini, dep_fin);
              }
          }
          System.out.println("CreacomprobantePDF: Dependencia = " + strDep);
          return strDep;
      }

    /**
     * VSWF GRE 26 - Sep - 05 M�todo generarReciboPDF_DPA . de Enlace Internet.
     * @param campos 
     */
    public void generarReciboPDF_DPA(String[] campos) {

        // vswf - Arreglo de conceptos pertenecientes a varios pagos de DPA's
        String[] camposDPA = campos;

        Document document = new Document(PageSize.LETTER.rotate(), 36, 36, 36, 72);
        int ultimaLinea = 0;
        java.io.FileOutputStream outStream = null;

        try {

            // VSWF GRE 26 - Sep - 05
            // Valida tipo de persona de acuerdo a EI y SN.
            if (canal.equals("enlace")) {
                rfc = obtieneRFC();
                curp = obtieneCURP();
                namePDF = "ReciboSat" + fecha + numOperacion + ".pdf";
                dependencia = obtieneDependencia();
                java.io.File tmpFile = new java.io.File(
                        Global.DIRECTORIO_LOCAL, namePDF);
                System.out.println("CreacomprobantePDF: tmpFile = " + tmpFile.getAbsolutePath());
                outStream = new java.io.FileOutputStream(tmpFile);
            } else {
                rfc = obtieneRFC(campos);
                curp = obtieneCURP(campos);
            }
            String sRazonS = "";
            sRazonS = getRazonSocial(campos);
            nombre = "";
            if (sRazonS.equals("")) {
                nombre = obtieneNombre(campos);
            }
            if (tipoPersona.equals("F")) {
                TotalEfecPag = obtieneTotalPagado(campos);
            } else {
                TotalEfecPag = obtieneTotalPagadoMoral(campos);
            }

            // VSWF GRE 25 - Sep - 05
            // Valida tipo de persona de acuerdo a EI y SN.
            PdfWriter writer = null;
            if (canal.equals("enlace")) {
                writer = PdfWriter.getInstance(document, outStream);
            } else {
                namePDF = "Recibo" + rfc + fecha + numOperacion + ".pdf";
                writer = PdfWriter.getInstance(document, bytesPDF);

                System.out.println("CreacomprobantePDF: namePDF = " + namePDF);
            }

            MyPageEvents events = new MyPageEvents(nombre, rfc, curp, plaza,
                    fecha, numOperacion, TotalEfecPag, true, sRazonS, this
                            .getLlavePago(),dependencia);
            writer.setPageEvent(events);
            //UrlWaterMark = "/ias/ias60/ias/APPS/enlaceMig/enlaceMig/jsp/nuevopagoimpuestos/";
            UrlWaterMark = Global.PATH_PNG;
            System.out.println("CreacomprobantePDF: WATERMARK PATH = " + UrlWaterMark);
            Watermark watermark = new Watermark(Image.getInstance(UrlWaterMark
                    + plantilla + ".png"), 2, 2);
            document.add(watermark);
            Phrase pagina = new Phrase("", new Font(Font.HELVETICA, 9,
                    Font.NORMAL));
            HeaderFooter footer = new HeaderFooter(pagina, false);
            footer.setBorder(Rectangle.NO_BORDER);

            if (canal.equals("enlace")) {
                footer.setAlignment(Element.ALIGN_RIGHT);
            }
            document.setFooter(footer);
            document.open();
            Phrase frase = null;
            PdfPTable aTable = new PdfPTable(2);
            aTable = new PdfPTable(2);
            aTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            aTable.getDefaultCell().setPadding(3);
            aTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
            Hashtable hsDatDisplay = getCampos(campos);
            LeeHashtable(hsDatDisplay);

            // VSWF GRE 25 - Sep - 05 Inicia dependencia

            /* VSWF Modificaci�n que permite mostrar mas de de dos pagos DPA's
             * en el comprobante de pago
             */
            for (int i = 0; i < camposDPA.length; i++)
                System.out.println("CreacomprobantePDF: camposDPA[" + i + "] = " + camposDPA[i]);

						// VSWF - Ordena los conceptos del pago DPA conforme a la especificaci�n
            int iDPA = 0;
            int iClaveDPA = 0;

            // Recorre todo el arreglo
            for(int i = 0; i < camposDPA.length; i++){

	            String conceptoArr = "";
	            if (camposDPA[i].indexOf(":") > -1){
	            		conceptoArr = camposDPA[i].substring(0, camposDPA[i].indexOf(":"));
	            } else {
	            		conceptoArr = camposDPA[i];
	            }

            	if(conceptoArr.equals("Derechos, Productos y Aprovechamientos")){
            			iDPA = i;
            	}

            	// Ordena la cadena "Clave de Referencia del DPA"
            	if(conceptoArr.equals("Clave de Referencia del DPA")){
									for(int j = i; j > iDPA + 1; j--){
											String tmp = camposDPA[j - 1];
											camposDPA[j - 1] = camposDPA[j];
											camposDPA[j] = tmp;
									}
            			iClaveDPA = iDPA + 1;
            	}

            	// Ordena la cadena "Cadena de la dependencia"
            	if(conceptoArr.equals("Cadena de la dependencia")){
									for(int j = i; j > iClaveDPA + 1; j--){
											String tmp = camposDPA[j - 1];
											camposDPA[j - 1] = camposDPA[j];
											camposDPA[j] = tmp;
									}
            	}
            }


						// Intercambia las cadenas "Dependencia" y "Derechos, Productos y Aprovechamientos"
						boolean b = true;
            for(int i = 0; i < camposDPA.length && b; i++){

	            String conceptoArr = "";
	            if (camposDPA[i].indexOf(":") > -1){
	            		conceptoArr = camposDPA[i].substring(0, camposDPA[i].indexOf(":"));
	            } else {
	            		conceptoArr = camposDPA[i];
	            }

            	if(conceptoArr.equals("Dependencia") && b){
									String tmp = camposDPA[i];
									camposDPA[i] = camposDPA[i + 1];
									camposDPA[i + 1] = tmp;
									b = false;
							}
            }

						// Crea el comprobante de DPA's
            String pago = "";
            for (int i = 0; i < camposDPA.length; i++) {

                String valor = "";
                String conceptoArr = "";
                if (camposDPA[i].indexOf(":") > -1) {
                    conceptoArr = camposDPA[i].substring(0, camposDPA[i]
                            .indexOf(":"));
                } else {
                    conceptoArr = camposDPA[i];
                }

                if (conceptoArr.equals("Derechos, Productos y Aprovechamientos")) {
                    pago = "Importe :";
            		    aTable.addCell(" ");
            		    aTable.addCell(" ");
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
            				frase = new Phrase("Derechos, Productos y Aprovechamientos",
            								new Font(Font.HELVETICA, 10, Font.BOLD));
            				aTable.addCell(frase);
            				aTable.addCell(" ");
            		}
                


                if (conceptoArr.equals("Clave de Referencia del DPA")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Clave de Referencia del DPA :",
                            new Font(Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Cadena de la dependencia")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Cadena de la dependencia :", new Font(Font.HELVETICA,
                            10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("IVA actos accidentales")) {
                    pago = "Impuesto a Cargo :";
                    aTable.addCell(" ");
                    aTable.addCell(" ");
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("IVA actos accidentales", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    aTable.addCell("");
                }

                if (conceptoArr.substring(0, 3).equals("Per")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Periodo :", new Font(Font.HELVETICA, 10,
                            Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Ejercicio")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Ejercicio :", new Font(Font.HELVETICA,
                            10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Importe")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase(pago, new Font(Font.HELVETICA, 10,
                            Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }
                else if(conceptoArr.equals("Saldo a Favor"))
                {
                	valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Saldo a Favor :", new Font(Font.HELVETICA, 10,
                            Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Cantidad Pagada")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Cantidad Pagada :", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Parte Actualizada")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Parte Actualizada :", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Recargos")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Recargos :", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }

                if (conceptoArr.equals("Multa por Correcci�n")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Multa por Correcci�n :", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }
                if (conceptoArr.equals("Compensaciones")) {
                    valor = camposDPA[i]
                            .substring(camposDPA[i].indexOf(":") + 1);
                    frase = new Phrase("Compensaciones :", new Font(
                            Font.HELVETICA, 10, Font.BOLD));
                    aTable.addCell(frase);
                    System.out.println("Valor compensaciones"+ valor.length());
                    frase = new Phrase(valor, new Font(Font.HELVETICA, 10,
                            Font.NORMAL));
                    aTable.addCell(frase);
                }
            }

            document.add(aTable);
            masConceptos = false;
            document.add(new Paragraph(" "));
            Table bTable = new Table(1);
            bTable.setBorderColor(new java.awt.Color(255, 255, 255));
            bTable.setBorder(Rectangle.NO_BORDER);

            Cell cell = null;

            // VSWF GRE 27 - Sep - 05 Valida el canal
            /*if (canal.equals("supernet")) {
                this.tramaImp = obtenTramaImp(this.tramaImp);
            }
            frase = new Phrase(this.tramaImp, new Font(Font.HELVETICA, 10,
                    Font.NORMAL));*/

			//VSWF se agrega la cadena original ordenada

			frase = new Phrase("Cadena Original : ", new Font(Font.HELVETICA, 10, Font.BOLD));
            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);

            frase = new Phrase(" ", new Font(Font.HELVETICA, 10, Font.NORMAL));
            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);

            frase = new Phrase(this.cadOriginal, new Font(Font.HELVETICA, 10,
                    Font.NORMAL));

            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);
            cell = new Cell(" ");
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);
            frase = new Phrase("Sello Digital : ", new Font(Font.HELVETICA, 10,
                    Font.BOLD));
            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);
            frase = new Phrase(" ", new Font(Font.HELVETICA, 10, Font.NORMAL));
            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);
            frase = new Phrase("||" + sello + "||", new Font(Font.HELVETICA,
                    10, Font.NORMAL));
            cell = new Cell(frase);
            cell.setBorderWidth(0.0f);
            bTable.addCell(cell);
            if (!writer.fitsPage(bTable)) {
                document.newPage();
            }
            document.add(bTable);
            events.setEventStatus(true);
            events.onCloseDocument(writer, document);
            writer.setPageEvent(null);
            document.close();
        } catch (DocumentException de) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + de);
        } catch (IOException ioe) {
			System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + ioe);
        } finally {
            if (canal.equals("enlace")) {
                if (null != outStream) {
                    try {
                        outStream.close();
                    } catch (Exception exception) {
						System.out.println("CreacomprobantePDF - ERROR AL EJECUTAR EL SERVICIO DE TUXEDO PAGOIMPUESTOS: " + exception);
                    }
                    outStream = null;
                }
            }
        }
    }

    /**
     * VSWF GRE 24 - Sep - 05 M�todo que corresponde al archivo Enlace internet.
     * @param trama 
     * @return
     */
    public String[] obtieneDatos(String trama) {

        int indice = trama.indexOf('|');
        int numSeparador = 0;
        for (int k = 0; k < trama.length(); k++) {
            if (trama.charAt(k) == '|') {
                numSeparador++;
            }
        }
        String temp = "";
        String temp1 = "";
        java.util.ArrayList list = new java.util.ArrayList(numSeparador);
        System.out.println("CreacomprobantePDF: numSeparador = " + numSeparador);
        System.out.println("CreacomprobantePDF: trama.length = " + trama.length());
        String fechaPagoAnterioridad = null;
        for (int i = 0; i < numSeparador || trama.length() < 0; i++) {
            temp = trama.substring(0, indice);
            if (temp.trim().indexOf("Cantidad a pagar") > -1) {
                temp1 = "Cantidad pagada:"
                        + temp.substring(temp.indexOf(":") + 1);
                list.add(temp1);
            } else if ((-1 != temp.trim().indexOf(
                    "Fecha del monto pagado con anterioridad"))
                    && (null == fechaPagoAnterioridad)) {

                System.out.println("CreacomprobantePDF: skiping and holding = " + temp);
                fechaPagoAnterioridad = temp;
            } else if ((-1 != temp.trim().indexOf(
                    "Monto pagado con Anterioridad"))
                    && (null != fechaPagoAnterioridad)) {

                System.out.println("CreacomprobantePDF: inserting = " + fechaPagoAnterioridad);
                list.add(fechaPagoAnterioridad);
                list.add(temp);
                fechaPagoAnterioridad = null;
            } else if ((-1 != temp.trim().indexOf(
                    "Fecha del monto pagado con anterioridad"))
                    || (-1 != temp.trim().indexOf(
                            "Monto pagado con Anterioridad"))) {

                System.out.println("CreacomprobantePDF: ERROR. Caso no contemplado, continuando de manera normal");
                System.out.println("CreacomprobantePDF: ERROR. Insertando " + temp);
                list.add(temp);
            } else {
                list.add(temp);
            }
            trama = trama.substring(indice + 1);
            indice = trama.indexOf('|');
            numDatos = i;
        }
        Object[] array = list.toArray();
        String[] retValue = new String[array.length];
        for (int i = 0; i < array.length; ++i) {
            retValue[i] = (String) array[i];
        }
        
        return retValue;
    }

    /**
     * VSWF GRE 24 - Sep - 05 M�todo que corresponde al archivo SuperNet.
     * @param Trama recibe la trama para supernet
     * @return Retorna la trama Final
     */
    public String obtenTramaImp(String Trama) {

        System.out.println("CreacomprobantePDF: Trama = " + Trama);
        int indexVersion = Trama.indexOf("40006") - 1;
        int indexSerie = Trama.indexOf("|30003");

        String tempTrama = Trama.substring(0, indexVersion);
        String tempFinalTrama = Trama.substring(indexSerie);

        return tempTrama + tempFinalTrama;
    }

} /* Fin de la clase CreacomponentePDF */
