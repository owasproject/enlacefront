package mx.altec.enlace.servicios;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.CuentaBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaCuentasBO;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Clase con el manejo de conexiones a base de datos para
 * la validación de cuentas existentes
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Nov 03, 2009
 */
public class ValidaCuentaContrato extends BaseServlet{

	/**
	 * Id Serial Version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Objeto de Conexion MQ
	 */
	private MQQueueSession objMQ = null;

	/**
	 * Constante para identificar un modulo que no es transferencia Mismo
	 * Banco e Interbancaria
	 */
	//public static final String DEFAULT = "0";

	/**
	 * Constante para identificar Transferencias Mismo Banco
	 */
	public static final String TRANSFERENCIA_MISMO_BANCO = "1";

	/**
	 * Constante para identificar Transferencias Interbancaria
	 */
	//public static final String TRANSFERENCIA_INTERBANCARIA = "4";

	/**
	 * Constante para identificar Transferencias Interbancaria
	 */
	public static final String SERVICIO_TUXEDO = "VALCTA_TR";

	/**
	 * Constructor donde se inicializa la conexión que
	 * se utilizara para las transacciones
	 *
	 */
	public ValidaCuentaContrato(){
		try{
			objMQ = new MQQueueSession(Global.JNDI_CONECTION_FACTORY,Global.JNDI_QUEUE_REQUEST,Global.JNDI_QUEUE_RESPONSE);
			objMQ.open();
		}catch(Exception e){
			EIGlobal.mensajePorTrace("ArchivoPagoDAO::getConnectionTransaction:: Error al crear la conexión "
					+ e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
	}

	/**
	 * Metodo encargado de cerrar la conexi?n para varias transacciones
	 *
	 */
	public void closeTransaction() {
		try{
			if(this.objMQ != null){
				objMQ.close();
				this.objMQ = null;
		    }
		}catch(Exception e1){
			StackTraceElement[] lineaError;
			lineaError = e1.getStackTrace();
			EIGlobal.mensajePorTrace("Error generico al cerrar la conexion transaccional.", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("ValidaCuentaContrato::closeConnectionTransaction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e1.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		}catch (Throwable e2) {
			StackTraceElement[] lineaError;
			lineaError = e2.getStackTrace();
			EIGlobal.mensajePorTrace("Error generico al cerrar la conexion transaccional.", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("ValidaCuentaContrato::closeConnectionTransaction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e2.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea de truene->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
		}
	}

	/**
	 * Metodo encargado de generar la trama para validar la cuenta
	 * @param numContrato		Numero de Contrato
	 * @param usr 				Numero de usuario
	 * @param usrPerfil 		Perfil de usuario
	 * @param numCuenta 		Numero de Cuenta
	 * @param modulo 			Modulo de Consulta
	 * @param tipo				Tipo de consulta a ejecutar
	 * @return result			Arreglo que contiene la trama de requerida
	 */
	public String[] obtenDatosValidaCuenta(String numContrato, String numUsuario, String usrPerfil,String numCuenta,
										   String modulo) {
		ServicioTux tuxGlobal = new ServicioTux ();
		StringTokenizer token = new StringTokenizer(modulo, "@");
		int moduloLenght = 0;
		String nuevoModulo = "";
	    Hashtable hs = null;
	    String tramaEntrada = null;
	    String tramaSalida = null;
		String CodError = "";
		String[] datos = null;
		String[] result = null;
		int registro=8;
	 try{
			moduloLenght = token.countTokens();
			for(int i=0; i<moduloLenght; i++)
				nuevoModulo += token.nextToken() + "_";

			tramaEntrada =// "VALC" + "@" +
						   numContrato + "@"
						 + numUsuario + "@"
						 + usrPerfil + "@"
						 + numCuenta + "@"
						 + nuevoModulo + "@";
						 //+ tipo + "@";

			//hs =  tuxGlobal.validaTransferencias(tramaEntrada, "VALREG_TR");
			hs = tuxGlobal.validaCuentasMax(tramaEntrada, SERVICIO_TUXEDO, this.objMQ);

			if(hs != null) {
				CodError = (String) hs.get("COD_ERROR");
				tramaSalida = (String) hs.get("BUFFER");
			}

			if(modulo.equals(IEnlace.MDep_Inter_Abono))
				registro=5;

			if(CodError.equals("VALF0000") && tramaSalida!=null){
				result = new String[9];
				datos = desentramaC(registro + "@" + tramaSalida,'@');
				if (modulo.equals (IEnlace.MDep_Inter_Abono)) {
				     result=datos;
				}else{
					result[1]=datos[1]; //numero cta
					result[2]=datos[8]; //propia o de terceros
					result[3]=datos[7]; //tipo cta
					result[4]=datos[3]; //descripcion
					result[5]=datos[2]; //cuenta hogan
					result[6]=datos[6]; //FM
					result[7]=datos[4]; //producto
					result[8]=datos[5]; //subproducoto
				}
            if(hs != null) hs.clear();
			return result;
			}
		}catch(Exception e){
			if(hs != null) hs.clear();
			try{
				closeTransaction();
			}catch(Exception e1){
				EIGlobal.mensajePorTrace("Error al cerrar la conexión a MQ", EIGlobal.NivelLog.ERROR);
			}
			try{
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("transferencia::obtenDatosValidaCuenta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
			               				+ "Cuenta->" + numCuenta
			               				+ "Usuario->" + numUsuario
			               				+ "Contrato->" + numContrato
			               				+ "Modulo a consultar->" + modulo
							 			+ "Linea de truene->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}catch(Exception e2){
				EIGlobal.mensajePorTrace("transferencia::obtenDatosValidaCuenta::"
						                + "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
						                + e2.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		return result;
	}

protected void finalize() throws Throwable {
	super.finalize();
	closeTransaction();
}
/**
 * Metodo para validar cuentas en Dolares
 * @param numeroCuenta Numero de cuenta cargo o abono
 * @param moduloconsultar para pesos o dolares de cuenta cargo o abono
 * @param req peticion 
 * @return la cuenta encontrada 
 */
public String [] obtenDatosValidaCtaDolares(String numeroCuenta, 
         String moduloconsultar, HttpServletRequest req){
    
    EIGlobal.mensajePorTrace("obtenDatosValidaCtaDolares - Inicia -----> MOD : "
                                + moduloconsultar , EIGlobal.NivelLog.INFO);
    HttpSession sess = req.getSession();
    BaseResource session = (BaseResource) sess.getAttribute("session");
    String chogan = "D";
    List<CuentaBean> cuentas = null;
    String[] result = null;
    ConsultaCuentasBO ccbo = new ConsultaCuentasBO();
    String descripcionCuenta= "";
    //identificar la Consulta de Cuentas en Dolares
    String nombreArchivo = session.getUserID8() + ".ctas";
    boolean resultadoConsulta = llamado_servicio50CtasInteg(moduloconsultar, numeroCuenta, chogan, descripcionCuenta, nombreArchivo, req);
    //CuentaBean cuenta = null;
    if(resultadoConsulta) {
        String archivo = session.getUserID8() + ".ctas";
        boolean facultadSinCuenta = false;
        facultadSinCuenta = (session.getFacultad("DEPSINCAR"))?true:false;
        String[] argParametrosNoContempladosDao = new String[1];
        if(!"".equals(numeroCuenta)) {
            argParametrosNoContempladosDao[0] = "";
        }else{
            argParametrosNoContempladosDao[0] = String.valueOf(facultadSinCuenta);
        }

        cuentas = ccbo.consultaCuentas(archivo, moduloconsultar, argParametrosNoContempladosDao);
        for(CuentaBean obj : cuentas){
            EIGlobal.mensajePorTrace("obtenDatosValidaCtaDolares - REGISTRO_CTA::: "+obj.getDescripcion(), EIGlobal.NivelLog.INFO);
        }
        
        for (int i = 0; i < cuentas.size(); i++) {
            result = new String[9];
            CuentaBean cuenta = cuentas.get(i);
            result[1] = cuenta.getNumCuenta();
            result[2] = cuenta.getTipoCuenta();
            result[4] = cuenta.getDescripcion();
            EIGlobal.mensajePorTrace("obtenDatosValidaCtaDolares - --- > RESULTADO"
                                        + " NumCuenta " + cuenta.getNumCuenta()
                                        + " Divisa " 
                                        + (!IEnlace.MCargo_transf_dolar.equals(moduloconsultar) ? cuenta.getDivisa() : "No Informada")
                                     , EIGlobal.NivelLog.INFO);
        }
        
        EIGlobal.mensajePorTrace("obtenDatosValidaCtaDolares - [Saliendo-----> RESULT |" + Arrays.toString(result) + "|", EIGlobal.NivelLog.INFO);
    }
    return result;

}

}