package mx.altec.enlace.servicios;
import mx.altec.enlace.utilerias.Global;

public class ServiceMain {
	public static void main(String[] args) {
		Global.configura(Global.ARCHIVO_CONFIGURACION);
		try {
			switch (args[0].charAt(0)) {
				case 'C':System.out.println(NIPManagerServices.consultaEstado(args[1],args[2]));break;
				case 'S':System.out.println(NIPManagerServices.solicitud(args[1],args[2],args[3],args[4],args[5]));break;
				case 'A':System.out.println(NIPManagerServices.activar(args[1],args[2],args[3]));break;
				case 'B':System.out.println(NIPManagerServices.bloquear(args[1],args[2],args[3]));break;
				case 'F':System.out.println(NIPManagerServices.solFolioSeguridad(args[1],args[2]));break;
				case 'E':StringEncrypter.encrypt2File(args[1]);break;
				case 'D':System.out.println(new StringEncrypter(StringEncrypter.DESEDE_ENCRYPTION_SCHEME).decrypt(args[1]));
			}
		} catch (NIPManagerServiceException e) {
			e.printStackTrace();
		} catch (EncryptionException e) {
			e.printStackTrace();
		}
	}
}