package mx.altec.enlace.servicios;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.io.IOException;
import javax.xml.soap.*;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.WSCnfLinCapBO;
import mx.altec.enlace.beans.WSCnfLinCapBean;
import mx.altec.enlace.beans.WSLinCapBean;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.IEnlace;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.utilerias.Formateador;
import java.net.URLConnection;
import java.net.URL;
import mx.altec.enlace.utilerias.HttpTimeoutHandler;
import java.net.MalformedURLException;

public class WSLinCapService {

	/** OKNM Constante para definir el nombre del par�metro para obtener la bandera de la l�nea de captura */
	private static final String OKNM = "0000";

  	/**
	 * Realiza la validaci�n de la l�nea de captura.
	 *
	 * @param request HttpServletRequest request del sistema
	 * @param linCap String linea de captura de la operaci�n
	 * @param montPag String importe de la operaci�n
	 * @param formPag String forma de pago de la operaci�n
	 * @param idCanal String id del canal de la operaci�n
	 * @param fecha String fecha de validaci�n de la operaci�n
	 * @param hora String hora de validaci�n de la operaci�n
	 * @param ref String referencia de la operaci�n
	 * @param numCtaAb String cuenta de abono de la operaci�n
	 * @param mesSinInt String meses sin intereses de la operaci�n
	 * @param libre1 String campo libre de la operaci�n
	 * @param ctaCargo String cuenta de cargo de la operaci�n
	 * @param tipoOper String tipo de la operaci�n
	 * @param convenio String convenio de la operaci�n
	 * @return String Respuesta de la validaci�n.
	 * @throws MalformedURLException si la URL es incorrecta
	 */
	public String validaLineaCaptura(HttpServletRequest request, String linCap, String montPag, String formPag,
				String idCanal, String fecha, String hora, String ref, String numCtaAb, String mesSinInt,
				String libre1, String ctaCargo, String tipoOper, String convenio) throws MalformedURLException{
		String respuesta = "", sreferencia = "";
		WSCnfLinCapBO wsCnfLCBo = new WSCnfLinCapBO();
		WSCnfLinCapBean wsCnfLCBean = new WSCnfLinCapBean();
		WSLinCapBean wsLCBean = new WSLinCapBean();
		SOAPConnectionFactory soapConnectionFactory = null;
		SOAPConnection soapConnection = null;
		URL endpoint = null;
		HttpTimeoutHandler tmo = null;
		BaseServlet bs = new BaseServlet();
		double importe = 0.0;
		wsCnfLCBean.setBndLinCap(wsCnfLCBo.consBanderaLC());
		String tipoOperacion = convierteTipoOper(tipoOper);
		if (wsCnfLCBean.getBndLinCap())
		{
			wsCnfLCBean.setEsCtaLinCap(wsCnfLCBo.existeCtaLC(numCtaAb));
			if (wsCnfLCBean.getEsCtaLinCap())
			{
				wsCnfLCBean.setUrlWSLinCap(wsCnfLCBo.consURLLC());
				sreferencia = String.format("%8d", bs.obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE))); 
				EIGlobal.mensajePorTrace("Referencia para bitacora de proceso ---> " + sreferencia, EIGlobal.NivelLog.DEBUG);
				if (!"".equals(wsCnfLCBean.getUrlWSLinCap()))
				{

					// Create SOAP Connection
					try {
						soapConnectionFactory = SOAPConnectionFactory.newInstance();
					} catch (UnsupportedOperationException e) {
						// TODO Auto-generated catch block
						EIGlobal.mensajePorTrace("WSLinCapService - validaLineaCaptura: Error (UnsupportedOperationException) al tratar de crear la conexi�n " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
					} catch (SOAPException e) {
						// TODO Auto-generated catch block
						EIGlobal.mensajePorTrace("WSLinCapService - validaLineaCaptura: Error (SOAPException) al tratar de crear la conexi�n " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
					}

					try {
						soapConnection = soapConnectionFactory.createConnection();
					} catch (SOAPException e) {
						// TODO Auto-generated catch block
						EIGlobal.mensajePorTrace("WSLinCapService - validaLineaCaptura: Error (SOAPException) al tratar de realizar la conexi�n " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
					}
					importe = Double.parseDouble(montPag);
					EIGlobal.mensajePorTrace("Cuenta abono "+numCtaAb, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Importe "+ formatoNumero(importe), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Linea Captura "+linCap, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("Cuenta Carggo "+ctaCargo, EIGlobal.NivelLog.DEBUG);
					wsCnfLCBean.setTmoWSLinCap(wsCnfLCBo.consTmoLC());
					wsLCBean.setCnsLinCap(linCap);
					wsLCBean.setCnsMontPag(formatoNumero(importe));
					wsLCBean.setCnsFormPag(formPag);
					wsLCBean.setCnsIdCanal(idCanal);
					wsLCBean.setCnsFecha(fecha);
					wsLCBean.setCnsHora(hora);
					wsLCBean.setCnsRef(ref);
					wsLCBean.setCnsNumCtaAb(numCtaAb);
					wsLCBean.setCnsMesSinInt(mesSinInt);
					wsLCBean.setCnsLibre1(libre1);
					try {
						//Establece el tiempo de respuesta
						tmo = new HttpTimeoutHandler(wsCnfLCBean.getTmoWSLinCap().intValue());
						endpoint = new URL(null,wsCnfLCBean.getUrlWSLinCap(),tmo);
						EIGlobal.mensajePorTrace("Inicia LLAMADO A WEB SERVICE", EIGlobal.NivelLog.DEBUG);
						wsLCBean = getSOAPResponse(soapConnection.call(createSOAPRequest(wsLCBean), endpoint),wsLCBean);
						EIGlobal.mensajePorTrace("TERMINA LAAMADO A WEB SERVICE", EIGlobal.NivelLog.DEBUG);
					}
					catch (SOAPException e)
					{
						// TODO Auto-generated catch block
						EIGlobal.mensajePorTrace("WSLinCapService - validaLineaCaptura: Error (SOAPException) al tratar de llamar el web service " + new Formateador().formatea(e),	EIGlobal.NivelLog.ERROR);
					}
					catch (Exception ce)
					{
						EIGlobal.mensajePorTrace("WSLinCapService - validaLineaCaptura: Error (ConnectException) al tratar de llamar el web service "+ new Formateador().formatea(ce),	EIGlobal.NivelLog.ERROR);
					}

					if ("".equals(wsLCBean.getRspEstatusLin()))
					{ /* TIMEOUT o ERROR de interno de WEB SERVICE */
						if ("".equals(wsLCBean.getRspLibre1()))
						{ /* TIMEOUT*/
							respuesta = tipoOperacion+OKNM+sreferencia+"Time Out (" + wsCnfLCBean.getTmoWSLinCap() + "mseg) alcanzado.";
						} else { /* ERROR de interno de WEB SERVICE*/
							respuesta = tipoOperacion+OKNM+sreferencia+ wsLCBean.getRspLibre1() + ". [TMO]";
						}
					} else {
						if ("001".equals(wsLCBean.getRspEstatusLin()))
						{
							respuesta = tipoOperacion+OKNM+sreferencia+"L�nea de Captura V�lida.";
						} else {
							respuesta = tipoOperacion+"0" + wsLCBean.getRspEstatusLin() +sreferencia+ wsLCBean.getRspLibre1() + ".";
						}
					}
					
					if("WPMI".equals(tipoOperacion))
					{
						bitacorizaProceso(request,sreferencia, respuesta, tipoOperacion , wsLCBean.getCnsMontPag(),convenio, ctaCargo);
					}
					else
					{
						bitacorizaProceso(request,sreferencia, respuesta, tipoOperacion , wsLCBean.getCnsMontPag(), wsLCBean.getCnsNumCtaAb(), ctaCargo);
					}
					
				}
				else
				{ /* No se encuentra definida la URL del WebService de L�nea de Captura*/
					respuesta = tipoOperacion+OKNM+sreferencia+"URL de WebService No Definida. [TMO]";
					if("WPMI".equals(tipoOperacion))
					{
						bitacorizaProceso(request,sreferencia, respuesta, tipoOperacion , montPag,convenio, ctaCargo);
					}
					else
					{
						bitacorizaProceso(request,sreferencia, respuesta, tipoOperacion , montPag, numCtaAb, ctaCargo);
					}
				}
			}
			else
			{ /* La Cuenta de Abono no esta en el cat�logo de cuentas de L�nea de Captura*/
				respuesta = tipoOperacion+OKNM+sreferencia+"Cuenta de Abono NO pertenece al cat�logo de cuentas del Web Service.";
			}
		}
		else
		{ /* Bandera de L�nea de Captura apagada*/
			respuesta = tipoOperacion+OKNM+sreferencia+"Bandera de Validaci�n de L�nea de Captura APAGADA.";
		}
		
		return respuesta;

	}

  	/**
	 * Genera el mensage de solicitud.
	 *
	 * @param inWSLCBean WSLinCapBean BEAN en el que contiene la infromaci�n para generar el mensaje de solicitud.
	 * @return SOAPMessage Mensaje de solicitud.
	 */
	private	static SOAPMessage createSOAPRequest(WSLinCapBean inWSLCBean) {
		MessageFactory messageFactory = null;
		SOAPMessage soapMessage = null;
		SOAPEnvelope envelope = null;
		SOAPBody soapBody = null;

		try {
			messageFactory = MessageFactory.newInstance();
		} catch (SOAPException e) {
			// TODO	Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de hacer newInstance " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}
		try {
			soapMessage = messageFactory.createMessage();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de hacer createMessage " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String serverURI = "http://interfaces.wslc.isban.mx/";

		// SOAP Envelope
		try {
			envelope = soapPart.getEnvelope();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de hacer getEnvelope " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}
		try {
			envelope.addNamespaceDeclaration("int", serverURI);
		} catch (SOAPException e) {
			// TODO	Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de hacer addNamespaceDeclaration " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}

		/*
		<soapenv:Envelope	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"	xmlns:int="http://interfaces.wslc.isban.mx/">
			<soapenv:Header/>
			<soapenv:Body>
				<int:validaLC>
					<!--Optional:-->
					<beanDatosLineaCaptura>
						<!--Optional:-->
						<fecha>19122014</fecha>
						<!--Optional:-->
						<formPag>001</formPag>
						<!--Optional:-->
						<hora>06:05:05</hora>
						<!--Optional:-->
						<idCanal>0001</idCanal>
						<!--Optional:-->
						<libre1>0000000000000</libre1>
						<!--Optional:-->
						<linCap>JASDII46546546asd56asd</linCap>
						<!--Optional:-->
						<mesSinInt>?</mesSinInt>
						<!--Optional:-->
						<montPag>15.00</montPag>
						<!--Optional:-->
						<numCtaAb>65501153679</numCtaAb>
						<!--Optional:-->
						<ref>983749823</ref>
					</beanDatosLineaCaptura>
				</int:validaLC>
			</soapenv:Body>
		</soapenv:Envelope>
		 */

		// SOAP Body
		try {
			soapBody = envelope.getBody();
			SOAPElement soapBodyElem = soapBody.addChildElement("validaLC", "int");
			SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("beanDatosLineaCaptura");
			SOAPElement soapBodyElem11 = soapBodyElem1.addChildElement("fecha");
			soapBodyElem11.addTextNode(inWSLCBean.getCnsFecha()); //"24-12-2014"
			SOAPElement soapBodyElem12 = soapBodyElem1.addChildElement("formPag");
			soapBodyElem12.addTextNode(inWSLCBean.getCnsFormPag()); //"001"
			SOAPElement soapBodyElem13 = soapBodyElem1.addChildElement("hora");
			soapBodyElem13.addTextNode(inWSLCBean.getCnsHora()); //"06:05:05"
			SOAPElement soapBodyElem14 = soapBodyElem1.addChildElement("idCanal");
			soapBodyElem14.addTextNode(inWSLCBean.getCnsIdCanal()); //"0001"
			SOAPElement soapBodyElem15 = soapBodyElem1.addChildElement("libre1");
			soapBodyElem15.addTextNode(inWSLCBean.getCnsLibre1()); //"0000000000000"
			SOAPElement soapBodyElem16 = soapBodyElem1.addChildElement("linCap");
			soapBodyElem16.addTextNode(inWSLCBean.getCnsLinCap().trim()); //"002476266622191206"
			SOAPElement soapBodyElem17 = soapBodyElem1.addChildElement("mesSinInt");
			soapBodyElem17.addTextNode(inWSLCBean.getCnsMesSinInt()); //"123"
			SOAPElement soapBodyElem18 = soapBodyElem1.addChildElement("montPag");
			soapBodyElem18.addTextNode(inWSLCBean.getCnsMontPag()); //"1.00"
			SOAPElement soapBodyElem19 = soapBodyElem1.addChildElement("numCtaAb");
			soapBodyElem19.addTextNode(inWSLCBean.getCnsNumCtaAb()); //"50235987457"
			SOAPElement soapBodyElem1A = soapBodyElem1.addChildElement("ref");
			soapBodyElem1A.addTextNode(inWSLCBean.getCnsRef()); //"983749823"
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de crear el contenido del mensaje de solicitud " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", serverURI  + "validaLC");

		try {
			soapMessage.saveChanges();
		} catch (SOAPException e) {
			// TODO	Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de guardar los cambios en el mensaje de solicitud " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}

		/* Print the request message */
		try {
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (SOAPException) al tratar de imprimir el mensaje de solicitud " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			EIGlobal.mensajePorTrace("WSLinCapService - createSOAPRequest: Error (IOException) al tratar de imprimir el mensaje de solicitud " + new Formateador().formatea(e),	EIGlobal.NivelLog.INFO);
		}

		return soapMessage;
	}

  	/**
	 * Obtiene los valores de la respuesta y los almacena en el BEAN.
	 *
	 * @param inSOAPResponse SOAPMessage mensaje que contiene la respuesta
	 * @param outWSLCBean WSLinCapBean BEAN en el que se almacenar�n la infromaci�n de respuesta.
	 * @return WSLinCapBean BEAN con la informaci�n de la respuesta.
	 */
	private static WSLinCapBean getSOAPResponse(SOAPMessage inSOAPResponse, WSLinCapBean outWSLCBean) throws Exception {
		try {
			inSOAPResponse.writeTo(System.out);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			//EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("WSLinCapService - Error (SOAPException) al tratar de imprimir el mensaje de respuesta " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("WSLinCapService - Error (IOException) al tratar de imprimir el mensaje de respuesta " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		SOAPBody soapBody = null;
		try {

//			String nombreMetodo = "";
//			String nombreBean = "";
			String nombreNodo = "", valorNodo = "";
			SOAPElement soapNode = null, soapMetodo = null,  soapBean = null;

			soapBody = inSOAPResponse.getSOAPBody();
			EIGlobal.mensajePorTrace("WSLinCapService - Nombre soapBody : " + soapBody.getNodeName() + "   \tValor soapBody : " + soapBody.getValue(), EIGlobal.NivelLog.INFO);
			Iterator iteratorA = soapBody.getChildElements();
			if (! iteratorA.hasNext()) {
				EIGlobal.mensajePorTrace("WSLinCapService - NO HAY hijos en Body", EIGlobal.NivelLog.INFO);
			} else {
//				EIGlobal.mensajePorTrace("WSLinCapService - SI HAY hijos en Body", EIGlobal.NivelLog.INFO);
				while (iteratorA.hasNext()){
					soapMetodo = (SOAPBodyElement)iteratorA.next();
					EIGlobal.mensajePorTrace("WSLinCapService - Nombre soapMetodo : " + soapMetodo.getNodeName() + "   \tValor soapMetodo : " + soapMetodo.getValue(), EIGlobal.NivelLog.INFO);
					Iterator iteratorB = soapMetodo.getChildElements();
					if (! iteratorB.hasNext()) {
						EIGlobal.mensajePorTrace("WSLinCapService - NO HAY hijos en M�todo", EIGlobal.NivelLog.INFO);
					} else {
//						EIGlobal.mensajePorTrace("WSLinCapService - SI HAY hijos en M�todo", EIGlobal.NivelLog.INFO);
						while (iteratorB.hasNext()){
							soapBean = (SOAPElement)iteratorB.next();
							EIGlobal.mensajePorTrace("WSLinCapService - Nombre soapBean : " + soapBean.getNodeName() + "   \tValor soapBean : " + soapBean.getValue(), EIGlobal.NivelLog.INFO);
							Iterator iteratorC = soapBean.getChildElements();
							if (! iteratorC.hasNext()) {
								EIGlobal.mensajePorTrace("WSLinCapService - NO HAY hijos en Bean", EIGlobal.NivelLog.INFO);
							} else {
//								EIGlobal.mensajePorTrace("WSLinCapService - SI HAY hijos en Bean", EIGlobal.NivelLog.INFO);
								while (iteratorC.hasNext()) {
									soapNode = (SOAPElement)iteratorC.next();
									nombreNodo = soapNode.getNodeName();
									valorNodo = soapNode.getValue();
									EIGlobal.mensajePorTrace("WSLinCapService - Nombre soapNode : " + nombreNodo + "   \tValor soapNode : " + valorNodo, EIGlobal.NivelLog.INFO);

									if ("estatusLin".equals(nombreNodo.trim())) {
										outWSLCBean.setRspEstatusLin(valorNodo);
									} else {
										if ("libre1".equals(nombreNodo.trim())) {
											outWSLCBean.setRspLibre1(valorNodo);
										} else {
											if ("nomCont".equals(nombreNodo.trim())) {
												outWSLCBean.setRspNomCont(valorNodo);
											} else {
												if ("rfcCont".equals(nombreNodo.trim())) {
													outWSLCBean.setRspRFCCont(valorNodo);
												}
											}
										}
									}
								}
								EIGlobal.mensajePorTrace("***outWSLCBean.estatusLin->" + outWSLCBean.getRspEstatusLin() + "\r\n***outWSLCBean.getRspLibre1->" + outWSLCBean.getRspLibre1() + "\r\n***outWSLCBean.getRspNomCont>" + outWSLCBean.getRspNomCont() + "\r\n***outWSLCBean.getRspRFCCont->" + outWSLCBean.getRspRFCCont(), EIGlobal.NivelLog.DEBUG);
							}
						}
					}
				}
			}
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
				EIGlobal.mensajePorTrace("WSLinCapService - Error al obtener mensje de respuesta " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return outWSLCBean;
  }

  	/**
	 * Bitacoriza la operaci�n.
	 *
	 * @param request HttpServletRequest
	 * @param sreferencia String referencia de la operaci�n.
	 * @param codError String c�digo de error obtenido.
	 * @param tipoOper String tipo de operaci�n.
	 * @param importe String importe de la operaci�n.
	 * @param ctaAbono String cuenta de abono de la operaci�n.
	 * @param ctaCargo String cuenta de cargo de la operaci�n.
	 */
  private void bitacorizaProceso(HttpServletRequest request, String sreferencia, String codError, String tipoOper, String importe, String ctaAbono, String ctaCargo) {
		EIGlobal.mensajePorTrace("***ENTRO AL METODO BITACORIZA PROCESO***", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***SREFERENCIA: "+sreferencia, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***CODERROR: "+codError.substring(0,8), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***TIPO OPERACION: "+ tipoOper, EIGlobal.NivelLog.DEBUG);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		BitaHelper bh = new BitaHelperImpl(request, session, sess);
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			try {
 				BitaTCTBean beanTCT = new BitaTCTBean ();

 				beanTCT = bh.llenarBeanTCT(beanTCT);
 				beanTCT.setImporte(Double.parseDouble(importe));
 				beanTCT.setCuentaOrigen(ctaCargo);
 				beanTCT.setCuentaDestinoFondo(ctaAbono);
				beanTCT.setReferencia(Integer.parseInt(sreferencia.trim()));
				beanTCT.setTipoOperacion(tipoOper);

				beanTCT.setCodError(codError.substring(0,8));
				beanTCT.setUsuario(session.getUserID8());
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("WSLinCapService - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("WSLinCapService - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		EIGlobal.mensajePorTrace("***SALIO DEL METODO BITACORIZA PROCESO***", EIGlobal.NivelLog.DEBUG);
  }

  	/**
	 * Convierte el tipo de operaci�n.
	 *
	 * @param key String llave para obtener la conversi�n.
	 * @return String operaci�n convertida.
	 */
  public static String convierteTipoOper (String key) {

		 HashMap GetTipoOper = new HashMap ();

		 if (key == null) {
		     return "";
		 }

			 GetTipoOper.put ("TRAN", "WTRA");
			 GetTipoOper.put ("PMIC", "WPMI");

		 String regreso = "";
		 regreso = (String) GetTipoOper.get (key);

		 if(regreso == null) {
		     regreso = key;
		 }
		 return regreso;
  }

  	/**
	 * Se formatea n�mero a decimal.
	 *
	 * @param importe double n�mero a ser formateado.
	 * @return String n�mero formateado.
	 */
public static String formatoNumero (double importe) {

		DecimalFormatSymbols separadores = new DecimalFormatSymbols();
		separadores.setDecimalSeparator('.');
		separadores.setGroupingSeparator(',');

		DecimalFormat formato = new DecimalFormat("##################.00", separadores);
		return formato.format(importe).toString();
	}
}