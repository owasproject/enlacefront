/**Banco Santander Mexicano
   Create Date: 03-2002
	 Modify Date: 11-04-2002
   @author Daniel Cazares Beltran. (ASTECI)
   @version 1.01


   08-11-2002 Update
   Daniel C�zares Beltr�n

   Se insert� el m�todo comboCuentas(HttpServletRequest req)
   que filtra las cuentas de Base Cero que se van a consultar.
   �nicamente devuelve las cuentas hijas, en forma de una cadena de
   <OPTION VALUE="numeroCuenta>Cuenta-Descripci�n</OPTION>

*/

package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.Map;


/**
** bcConSaldos is a blank servlet to which you add your own code.
*/
public class bcConSaldos extends BaseServlet{
    //Class Fields.
    private String strNotWorkingDates = null;
    private short sucOpera = (short)787;
    private String strMsgError = "";

    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
        defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
        defaultAction(req, res);
    }

    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException{
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");



        //Characters for Java Script code.
	final char chrBreak = '\n';
	final char chrSemiColon = ';';
	final char chrComilla = '"';

        EIGlobal.mensajePorTrace("++++++++++++++++++++++++++", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("BEGIN Consulta Saldos Base Cero v1.1.10", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("++++++++++++++++++++++++++", EIGlobal.NivelLog.INFO);



	/*Parameter that stores events to  let
	  know which page must be displayed
	*/
	String strEvent = req.getParameter("event");
	String usuario = null;
	String contrato = null;
	String clavePerfil   = null;
	String strMsgErrorDays = "";
	String strJspPath = null;

        final String StrServletPath = "/NASApp/enlaceMig/bcConSaldos";


        boolean enlaceSession = SesionValida(req,res);


	if(enlaceSession){
	   sucOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

           //Validation of events
           if(strEvent == null || strEvent.equals("initSaldos") ||
	      strEvent.equals("")){

	      try{
	    	  EIGlobal.mensajePorTrace("valida facultades", EIGlobal.NivelLog.INFO);
		 validateFaculties(new Boolean(session.getFacultad(session.FAC_BASCONSAL_BC)).toString());
		//Loads not working dates for current year.
		strNotWorkingDates = diasInhabilesAnt();

		/*if(strNotWorkingDates == null){
		   strMsgErrorDays = "Error al generar calendario";
                }else{
                    if(strNotWorkingDates.equals("")){
                        strNotWorkingDates = " ";
                    }
                }*/

                if(strNotWorkingDates == null || strNotWorkingDates.equals("")){
		   strMsgErrorDays = "Error al generar calendario";
                }


                req.setAttribute("msgErrorDays",strMsgErrorDays + chrComilla +
                                 chrSemiColon + chrBreak);

                req.setAttribute("ComboMtoEst",comboCuentas(req));
   			 //TODO: Bitacora Inicio de flujo
       		 /*
       		  * VSWF ARR -I
       		  */
       		 				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
       		 				try{
       		 					 EIGlobal.mensajePorTrace("bitacorizacion", EIGlobal.NivelLog.INFO);
       		 				BitaHelper bh = new BitaHelperImpl(req, session, sess);
       		 				if (req.getParameter(BitaConstants.FLUJO)!=null){
       		 				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
       		 				  }else{
       		 				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
       		 				  }
       		 				BitaTransacBean bt = new BitaTransacBean();
       		 				bt = (BitaTransacBean)bh.llenarBean(bt);
       		 				bt.setNumBit(BitaConstants.ER_ESQ_BASE0_CONS_SALDO_ENTRA);
       		 				bt.setContrato(session.getContractNumber());

       		 				BitaHandler.getInstance().insertBitaTransac(bt);
       		 				}catch (SQLException ex){
       		 					ex.printStackTrace();
       		 				}catch(Exception e){

       		 					e.printStackTrace();
       		 				}
       		 				}
       		 	    		/*
       		 	    		 * VSWF ARR -F*/

                strJspPath = initReqState(req,res);

                evalTemplate(strJspPath,req,res);
             }catch(Exception e){
                despliegaPaginaErrorURL(e.getMessage(),
                                        "Consulta de Saldos de Base Cero",
                                        "Tesorer�a &gt; Esquema Base Cero &gt; " +
                                            "Consulta de Saldos",
                                        "s33020h",StrServletPath,req,res);


             }
          }else{
             if(strEvent.equals("query")){
                //Balance Page
                try{
		   strJspPath = getBalance(req,res);
                   evalTemplate(strJspPath,req,res);
                }catch(Exception e){

                   despliegaPaginaErrorURL(e.getMessage(),
                                        "Consulta de Saldos de Base Cero",
                                        "Tesorer�a &gt; Esquema Base Cero &gt; " +
                                            "Consulta de Saldos",
                                        "s33020h",StrServletPath,req,res);


                }//end catch
             }
          }
       }//end if(enlaceSession)


    }//end defaultAction

    /**
     *Validates faculties.
     *@param StrFac is a String with facultie value. true if user has faculty.
     *@exception java.lang.Exception generated when user has not faculties.
    */
    private void validateFaculties(String StrFac)
       throws Exception{

       final String StrMsgErrorFac ="No tiene facultad para consulta de saldos.";

       EIGlobal.mensajePorTrace("VALOR DE FACULTAD: "+ StrFac, EIGlobal.NivelLog.INFO);



       if (null == StrFac || StrFac.equals("false")){
          throw new Exception(StrMsgErrorFac);
       }
    }

    /**Initial state.
     *@return String representing the path to a JSP.
    */
    private String initReqState(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException{

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


        //Some chars for Java Script code.
        final char chrBreak = '\n';
	final char chrSemiColon = ';';
	final char chrComilla = '"';
	//Limit date
	String strLimitDate = "";
	/***End of declarations***/

	req.setAttribute("newMenu",session.getFuncionesDeMenu());

        //Modificacion para integraci�n. de las cuentas
	session.setModuloConsultar(IEnlace.MMant_estruc_TI);



	//Creates header for request page
	req.setAttribute("Encabezado",
        CreaEncabezado("Consulta de Saldos de Base Cero",
                       "Tesorer�a &gt; Esquema Base Cero &gt; " +
                       "Consulta de Saldos", "s33020h",req));
        req.setAttribute("MenuPrincipal",session.getStrMenu());


        //3 months in past
        strLimitDate = getLimitDate(3,strNotWorkingDates);

        req.setAttribute("limitDate",strLimitDate);
        req.setAttribute("notWorkingDates",strNotWorkingDates + chrComilla +
                          chrSemiColon + chrBreak);
        req.setAttribute("day",ObtenDia());
        req.setAttribute("month",ObtenMes());
        req.setAttribute("year",ObtenAnio());
        return new String("/jsp/bcConSaldos.jsp");
    }//end method


    /**
     *Rolls a date (today) paramNumMonths down.
     *@param parmNumMonths is the months number.
     *@param parmNotWorkingDates is a String with not working dates.
     *@return String is a date paramNumMonths in the past.
    */

    private String getLimitDate(int parmNumMonths,String parmNotWorkingDates){
        String strNotWorkingDates = parmNotWorkingDates;
        int years = 0;
        int monthsDiff = 0;
        int daysInMonth = 0;
        int daysDiff = 0;
        boolean notWorkingDate = false;
        String strDate = "";
        final char chrSlash = '/';
        final int DAY_OF_MONTH = GregorianCalendar.DAY_OF_MONTH;
        final int DAY_OF_WEEK = GregorianCalendar.DAY_OF_WEEK;
        final int MONTH = GregorianCalendar.MONTH;
        final int YEAR = GregorianCalendar.YEAR;
        GregorianCalendar gcCurrentDate = new GregorianCalendar();
        /***End of declarations***/

        if(parmNumMonths > 0){
            years = parmNumMonths / 12;
            monthsDiff = parmNumMonths % 12;

            //if its necesary to roll down one year
            if(parmNumMonths > gcCurrentDate.get(MONTH) &&
               years == 0){

               gcCurrentDate.roll(YEAR,-1);
            }else{
               gcCurrentDate.roll(YEAR,years * -1);
            }

            //rolls down monthsDiff
            gcCurrentDate.roll(MONTH,monthsDiff * -1);
            daysInMonth = gcCurrentDate.getActualMaximum(DAY_OF_MONTH);
            daysDiff = daysInMonth - gcCurrentDate.get(DAY_OF_MONTH);
            strDate = formatDate(gcCurrentDate);

            //if it is not working date
            if(strNotWorkingDates.indexOf(strDate) == -1){
                notWorkingDate = false;
            }else{
                notWorkingDate = true;
            }

            while(gcCurrentDate.get(DAY_OF_WEEK) == Calendar.SATURDAY ||
                  gcCurrentDate.get(DAY_OF_WEEK) == Calendar.SUNDAY ||
                  notWorkingDate == true){

                   //If its end of month
                   if(daysDiff <= 0){
                       gcCurrentDate.roll(DAY_OF_MONTH,1);

                       //if its end of year
                       if(gcCurrentDate.get(MONTH) == 11){
                           gcCurrentDate.roll(YEAR,1);
                       }

                       gcCurrentDate.roll(MONTH,1);
                   }else{
                       gcCurrentDate.roll(DAY_OF_MONTH,1);
                   }

                   //calculate new Date again
                   daysInMonth = gcCurrentDate.getActualMaximum(DAY_OF_MONTH);
                   daysDiff = daysInMonth - gcCurrentDate.get(DAY_OF_MONTH);
                   strDate = formatDate(gcCurrentDate);

                   //if it is not working date
                   if(strNotWorkingDates.indexOf(strDate) == -1){
                       notWorkingDate = false;
                   }else{
                       notWorkingDate = true;
                   }
            }//end while
        }//end if

        return strDate;
    }//end getLimitDate


    /**
     *It takes a date and returns a string in dd/mm/yyyy format.
     *@param date is the date to be formatted.
     *@return String is the date in "dd/mm/yyyy" format.
    */
    private String formatDate(GregorianCalendar date){
        String strReturn = "";
        String day = "";
        char chrSlash = '/';

        if(date.get(GregorianCalendar.DAY_OF_MONTH)  <= 9){
            strReturn = strReturn + "0";
        }

        strReturn = strReturn +
        Integer.toString(date.get(GregorianCalendar.DAY_OF_MONTH)) + chrSlash;

        if((date.get(GregorianCalendar.MONTH) + 1) <= 9){
            strReturn = strReturn + "0";
        }

        strReturn = strReturn +
                    Integer.toString(date.get(GregorianCalendar.MONTH) + 1) +
                    chrSlash +
                    Integer.toString(date.get(GregorianCalendar.YEAR));

        return strReturn;
    }//end formatDate


    /**

     *Obtains Base Cero Balance.

     *@return String representing the path to a JSP.

     */

    private String getBalance(HttpServletRequest req, HttpServletResponse res)
            throws Exception{

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


       final char chrSlash = '/';

       //Balance's List
       List lstBalances = null;

       //Error messages.
       final String msgCopyError = "Error en la copia de datos.<BR>";
       //final String msgNull = "No existen datos.<BR>";
       //final String msgRecovError = "Error al recuperar los datos.<BR>";

       //This String stores buffer sent Tuxedo.
       String strData = "";

       //Stores response sent from Tuxedo
       String strRespTux = "";
       //Next JSP
       String strJspPath = "/jsp/bcDetSaldos.jsp";

       //Objects to process file
       String strFile = "";
       ArchivoRemoto RemoteFile;
       //Stores export file path.
       String strExportPath = "";

       //Gets user id
       String strUser = session.getUserID8();  



       String strAccount = req.getParameter("Account");

       //***End of declarations***

       strData = buildData(req);

       EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("TRAMA ENTRADA: " + "\"" + strData + "\"", EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);



       strRespTux = sendData(strData);

       EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("TRAMA SALIDA: " +  "\"" + strRespTux + "\"", EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("**************************", EIGlobal.NivelLog.INFO);



       if(strRespTux == null){
           throw new Exception(this.strMsgError);
       }else{
           //Copies file from Tux Server to App Server
           strFile = strRespTux.substring(strRespTux.lastIndexOf(chrSlash)+1,
                     strRespTux.length());

           RemoteFile = new ArchivoRemoto();
           if(!RemoteFile.copia(strFile)){
               throw new Exception(msgCopyError);
           }

           strFile = Global.DIRECTORIO_LOCAL + chrSlash + strFile;



           //loads balances records on a List.
           lstBalances = readBalances(strFile);

           if(lstBalances == null){
               throw new Exception(this.strMsgError);
           }else{
               req.setAttribute("newMenu",session.getFuncionesDeMenu());
               //Creates header for request page
               req.setAttribute("Encabezado",
               CreaEncabezado("Consulta de Saldos de Base Cero",
                              "Tesorer�a &gt; Esquema Base Cero &gt; " +
                              "Consulta de Saldos", "s33030h",req));
               req.setAttribute("MenuPrincipal",session.getStrMenu());
               req.setAttribute("ExportFaculty",
                   new Boolean(
                       session.getFacultad(
                           session.FAC_BASEXPORT_BC)).toString());
               req.setAttribute("DetailFaculty",
                   new Boolean(
                       session.getFacultad(
                           session.FAC_BASCONDEV_BC)).toString());



               //export
               strExportPath = export(strUser,strAccount,lstBalances);
               //TODO
               /*
	            * VSWF ARR -I
	            */
	           if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	           try{
				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit("ERBC02");
				bt.setContrato(session.getContractNumber());
				if(strRespTux!=null){
				 if(strRespTux.substring(0,2).equals("OK")){
					   bt.setIdErr("B0CS" + "0000");
				 }else if(strRespTux.length()>8){
					  bt.setIdErr(strRespTux.substring(0, 8));
				 }else{
					  bt.setIdErr(strRespTux);
				 }
				}
				if(strAccount!=null && !strAccount.equals("")){
					if(strAccount.length()<20){
						bt.setCctaOrig(strAccount);
					}else{
							bt.setCctaOrig(strAccount.substring(0,20));
					}
				}
				bt.setServTransTux("B0CS");
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
	           }
	   		/*
	   		 * VSWF ARR -F
	   		 */
               req.setAttribute("ExportPath",strExportPath);
               req.setAttribute("BalancesList",lstBalances);
           }
       }

       return strJspPath;
    }//END getBalance


    /**Removes slash to strDate.

     *@param strDate Date in dd/mm/yyyy style.

     *@return String representing a date in ddmmyyyy style.

     */

    private String delSlashToDate(String strDate){

        int firstSlash = strDate.indexOf("/");

        int lastSlash = strDate.lastIndexOf("/");

        String strReturn = strDate.substring(0,firstSlash) +
                           strDate.substring(firstSlash+1,lastSlash) +
                           strDate.substring(lastSlash+1,strDate.length());


        return strReturn;

    }//end delSlashToDate


    /**Gives data to Tux Server
     *@param strData is the String sent to server.
     *@return String with response.
     */
    private String sendData(String strData){
        //Mensajes de error.
        final String msgSendError = "Error en el env�o de datos.";


        ServicioTux servTx = new ServicioTux();


        String strRespTux = "";
        Map mp = null;

        /*servTx.setContext(
            ((com.netscape.server.servlet.platformhttp.PlatformServletContext)
            getServletContext()).getContext());*/


        try{
            mp = servTx.web_red(strData);
            strRespTux = (String) (mp.get("BUFFER"));



        }catch(java.rmi.RemoteException re){
            strRespTux = null;
            this.strMsgError = msgSendError;

        }catch(Exception e){
            strRespTux = null;
            this.strMsgError = msgSendError;
        }finally{
            return strRespTux;
        }
    }//end sendData


    /**Builds all data sent Tux Server.
     *The order of data is acording to Balance transaction.
     *@return String with data.
     */
    public String buildData(HttpServletRequest req){
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


        String strData = "";

        //Fields separators
        final char chrPipe = '|';
        final char chrAt = '@';
        final char chrSpace = ' ';
        final char chrSlash = '/';
        //Some default values to transact.
        final String strDeliverWay = IEnlace.medioEntrega2;
        //how data is delivered
        final String strService = "B0CS"; //transaction's name.

        //Gets User, Profile and Contract from session object.
        String strUser = session.getUserID8();  
        String strProfile = session.getUserProfile();
        String strContract = session.getContractNumber();



        /*
         *Data structure is:
         *
         *"MedioEntrega|Empleado|Servicio|Contrato|Usuario|Perfil|Cuenta@
			    FechaInicio@FechaFin| | |"
         */

        //Gets account and range for the Balance query.

        String strAccount = req.getParameter("Account");


        String strFirstDate = req.getParameter("FirstDate");
        String strLastDate = req.getParameter("LastDate");
        strFirstDate = delSlashToDate(strFirstDate);
        strLastDate = delSlashToDate(strLastDate);

        strData = strDeliverWay + chrPipe +  strUser + chrPipe + strService +
                  chrPipe + strContract + chrPipe +  strUser + chrPipe +
                  strProfile + chrPipe + strAccount + chrAt + strFirstDate +
                  chrAt + strLastDate + chrPipe + chrSpace + chrPipe + chrSpace
                  + chrPipe;

        return strData;
    }//end buildData


    /**Reads a file and copies records on a List.
     *@param strFile is the path and name of the file.
     *@return List is the list with all the balances.
     *
     *The response file has in its first line:
     *+++++++++++++++++++++++++++++++++++++++++++++
     *Position       Field
     *+++++++++++++++++++++++++++++++++++++++++++++
     *[1-8]       Mensaje de Respuesta
     *[9-16]      Numero de Referencia
     *[17]        Separador
     *[18-25]     Nombre del servicio
     *
     *For instance: "OK      00097932BASE0000Transaccion Exitosa"
     *
     *Other type of message is:
     *"FML BUFFER contiene trama no valida (pipes consecutivos)"
     *
     *Another type is:
     *"ERROR3     38279Su transaccion no puede ser atendida en este momento."
     *
     *For the next lines, the structure in the file is as follows:
     *
     *"Numero de Contrato|Cuenta de Cheques|Fecha|Credito|Techo Asignado|
     *Total Devoluciones|Techo Real|Monto a Disponer|Saldo Chequera|
     *Credito Dispuesto|Monto Operado|Horario Operacion|Estatus de Fondeo|"
     *
     *
     */
    private List readBalances(String strFile){
        //Error messages.
        final String msgReadError = "Error al leer datos.";
        final String msgDataError = "Error en el servicio solicitado.";
        final String msgNotKnown = "Error inesperado.";
        final String msgNoRecords = "No existen datos por consultar.";
        final String msgOpenError = "Error en acceso a datos.";
        final String msgNoDataSep = "Error al procesar los datos.";
        final String msgNullData = "Los datos son inv�lidos.";
        final String msgStructError = "La estructura de los datos es inv�lida.";

        //Objects to process File.
        EI_Exportar LocalFile;
        String strLine = "";

        //Balances List.
        List lstReturn = null;

        //list Index.
        int idx = 0;
        int intFirstIdx = 0;
        int intLastIdx = 0;

        //Data separator.
        final String strDataSep = "|";

        //Breaks a loop.
        boolean boolLoopError = false;
        LocalFile = new EI_Exportar(strFile);

        //Process File on App Server
        try{
            if(LocalFile.abreArchivoLectura()){
                strLine = LocalFile.leeLinea();

                if(strLine.substring(0,8).trim().equals("OK") &&
                   strLine.substring(16,24).trim().equals("BASE0000")){

                   lstReturn = new ArrayList();
                   idx = 0;
                   boolLoopError = false;

                   while(!LocalFile.eof() && boolLoopError == false){
                       strLine = LocalFile.leeLinea();
                       EIGlobal.mensajePorTrace("Linea no formateada: " + strLine, EIGlobal.NivelLog.INFO);

                       strLine = formatData(strLine);

                       EIGlobal.mensajePorTrace("Linea formateada: " + strLine, EIGlobal.NivelLog.INFO);

                       if(strLine != null){
                           //Contract and Account are removed from String
                           for(int i = 0; i < 2; i++){
                               intLastIdx = strLine.indexOf(strDataSep);
                               if(intLastIdx != -1){
                                   strLine = strLine.substring(intLastIdx + 1,
					     strLine.length());
                               }else{
                                   boolLoopError = true;
                               }
                           }

                           //Removes the last data separator on String
                           strLine = strLine.substring(0,strLine.length() - 1);

                           //Index is set in Date first character.
                           intFirstIdx = 0;
                           intLastIdx = strLine.indexOf(strDataSep);

                           if(intLastIdx == -1){
                               //data separator was not found.
                               lstReturn = null;
                               this.strMsgError = msgNoDataSep;
                               boolLoopError = true;
                           }else{
                               while(intLastIdx != -1){
                                   //Copies each field on List as received
                                   //from Tuxedo.
                                   lstReturn.add(idx,
                                    strLine.substring(intFirstIdx,intLastIdx));

                                   intFirstIdx = intLastIdx + 1;
                                   strLine =
                                      strLine.substring(intFirstIdx,
                                                        strLine.length());

                                   intFirstIdx = 0;
                                   intLastIdx = strLine.indexOf(strDataSep);
                                   idx++;
                               }

                               //Copies the last field on list as received from
                               //Tuxedo
                               lstReturn.add(idx,
                                             strLine.substring(intFirstIdx,
					     strLine.length()));
                               idx++;

                           }//end if
                       }//end if
                   }//end while

                   if(idx == 0){
                       //If no record was copied to Balance's List then.
                       lstReturn = null;
                       this.strMsgError = msgNoRecords;
                   }else{

                       EIGlobal.mensajePorTrace("**INICIA ORDENADO LISTA***", EIGlobal.NivelLog.INFO);



		       lstReturn = orderData(lstReturn);

                       EIGlobal.mensajePorTrace("**TERMINA ORDENADO LISTA**", EIGlobal.NivelLog.INFO);




                   }
                }else{
                    if(strLine.substring(0,8).trim().equals("ERROR") &&
                       strLine.substring(16,24).trim().equals("BASE0001")){

                           this.strMsgError = strLine.substring(24,
                                                              strLine.length());
                    }else{
                        if(strLine.substring(0,8).trim().equals("OK") &&
                           strLine.substring(16,24).trim().equals("BASE0001")){

                               this.strMsgError = strLine.substring(24,
                                                              strLine.length());
                        }else{
                            if(strLine.substring(0,5).equals("ERROR")){
                                this.strMsgError = strLine.substring(16,strLine.length());
                            }else{
                                this.strMsgError = strLine;
                            }
                        }
                    }
                    //lstReturn = null;
                }

		LocalFile.cierraArchivo();

		/*if(null == lstReturn){
                   this.strMsgError = msgNullData;
		}*/
            } else{
                this.strMsgError = msgOpenError;
                lstReturn = null;
            }
        }catch(Exception e){
            lstReturn = null;
            this.strMsgError = msgReadError;
        }finally{
            return lstReturn;
        }
    }//end readBalances

    /**
     *Orders ParmList because Tuxedo does not send each Balance in the display order.
     *@param ParmList is the disordered list
     *@return List is the ordered List (Output)
     */
    private List orderData(List ParmList){
        String StrTime = null;
        List RetList = null;
        int iRows = 0;
        int iCols = 0;

        /*
         ************************************
         ************************************
         *Change this value when Balance's Table increase or decrease
         *its number of columns
         ************************************
         ************************************
        */
        final int iRowLength = 11;

        //Time Row Absolute Position
        int iTimeAbsPos = 0;
        //Time Row Relative Position
        final int iTimeRelPos = 9;
        //Credito Disponible Row Relative Position
        final int iCredDispRelPos = 8;
        //Cr�dito Row Relative Position
        final int iCredRelPos = 1;
        //Current pointer list position
        int iCurrAbsPos = 0;
        //Next pointer list position
        int iNextPos = 0;


        if(null == ParmList){
            RetList = null;
        }else{
            EIGlobal.mensajePorTrace("****INPUT LIST****", EIGlobal.NivelLog.INFO);


            printList(ParmList);

            //rows loop
            for(iRows=0; iRows < (ParmList.size() / iRowLength); iRows++){
               iTimeAbsPos = iTimeRelPos + (iRows * iRowLength);
               StrTime = (String) ParmList.get(iTimeAbsPos);

               //cols loop
               for(iCols = iCredDispRelPos; iCols >= iCredRelPos; iCols--){
                   iCurrAbsPos = iCols + (iRows * iRowLength);
                   iNextPos = iCurrAbsPos + 1;
                   ParmList.set(iNextPos,ParmList.get(iCurrAbsPos));
               }//end cols loop

               ParmList.set(iCurrAbsPos,StrTime);
            }//end rows loop

            RetList = ParmList;

            EIGlobal.mensajePorTrace("***OUTPUT LIST***", EIGlobal.NivelLog.INFO);


            printList(RetList);

        }//end elseif(null == ParmList)

        return RetList;
    }//end orderList

    private void printList(List ParmList){

        //Imprime input list
        for(int i = 0; i < ParmList.size(); i++){
            EIGlobal.mensajePorTrace(i + ":" + ParmList.get(i), EIGlobal.NivelLog.DEBUG);


        }
    }

    /**
     *Formats all data.
     *@param LstNotFormated List not formated.
     * The order of data is as follows:
     *
     *"Numero de Contrato|Cuenta de Cheques|Fecha|Credito|Techo Asignado|
     *Total Devoluciones|Techo Real|Monto a Disponer|Saldo Chequera|
     *Credito Dispuesto|Monto Operado|Horario Operacion|Estatus de Fondeo|"
     *
     *@return String Returns formatted String. The method uses currency format
     *
     */

    private String formatData(String StrNotFormatted){
	String StrReturn = "";
        int intNumFields = 13;
        int intInitPos = 0;
        int intEndPos = 0;
        String StrDataSep = "|";
        String StrAuxField = null;
        int intSpaceIdx = 0;
        final String StrSpace = " ";

        for(int i = 0; i < intNumFields; i++){

            intEndPos = StrNotFormatted.indexOf(StrDataSep,intInitPos);

            if(intEndPos != -1){

                StrAuxField = StrNotFormatted.substring(intInitPos,intEndPos);

                switch(i){
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        StrAuxField = FormatoMoneda(StrAuxField);
                        System.out.println("cifra antes de quitar espacio "+ StrAuxField);
                        //Quita el espacio entre la cifra y el signo de moneda($)
                        StrAuxField.trim();
                        intSpaceIdx =  StrAuxField.indexOf(StrSpace);

                        //Si hay espacio
                        if(intSpaceIdx != -1){
                            StrAuxField = StrAuxField.substring(0,intSpaceIdx) +
                                        StrAuxField.substring((intSpaceIdx + 1),
                                                        StrAuxField.length());
                        }

                        System.out.println("cifra despues de quitar espacio "+ StrAuxField);

                        break;
                    default:
                        break;
                }//end switch(i)

                StrReturn = StrReturn + StrAuxField + StrDataSep;

                intInitPos = intEndPos + 1;

            }else{
                 StrReturn = StrNotFormatted;
                 break;
            }
        }

        return StrReturn;

    }//end formatData


    /**
     *Exports balances into a text file.
     *@return strReturn is the path of the text file.
     */
    private String export(String parmUsrID, String parmAccount,
                          List parmLstBalances){

       String strReturn = "";
       //Export File
       EI_Exportar ExportFile;
       //Error messages
       final String msgCreateError = "Error al crear archivo de exportaci�n.";
       final String msgWriteError = "Error al escribir en archivo de exportaci�n.";
       final String msgExportError = "Error al exportar archivo de consulta de saldos.";
       final char TAB = (char)(9);
       final char PAGE_BREAK = (char)(13);
       String strFilePath = parmUsrID + ".doc";

       ExportFile = new EI_Exportar(Global.DIRECTORIO_LOCAL + "/" +
                                    strFilePath);



       String strLine = "";
       Iterator it = parmLstBalances.iterator();

       //Columns counter
       int cols = 0;
       ArchivoRemoto RemoteFile = null;
       /***End of declarations***/

       try{
           if(ExportFile.creaArchivo()){
               strLine = parmAccount + TAB;

               while(it.hasNext()){
                   //go forward next object in List
                   strLine = strLine + (String)(it.next()) + TAB;
                   cols++;

                   if((cols % 11) == 0){
                       //if its last column in row
                       strLine = strLine + PAGE_BREAK;

                       //Writes in text file
                       if(!ExportFile.escribeLinea(strLine)){
                           //Error
                           this.strMsgError = msgWriteError;
                           break;
                       }

                       //Begin of next line
                       strLine = parmAccount + TAB;
                   }
               }//end while

               ExportFile.cierraArchivo();

               RemoteFile = new ArchivoRemoto();

               if(!RemoteFile.copiaLocalARemoto(strFilePath,"WEB")){
                   strReturn = null;
                   this.strMsgError = msgExportError;
               }else{
                   //OK
                   strReturn = strFilePath;
               }



           }else{
               strReturn = null;
               this.strMsgError = msgCreateError;
           }
       }catch(Exception e){
           strReturn = null;
           this.strMsgError = msgWriteError;
       }

       return strReturn;
    }//END export








    /**
       Retrieves Base Cero children Accounts as  a <option> tags set
       inserted in a String.
      @param req is the HttpRequest.
      @return String is the children accounts set.
    */
    public String comboCuentas(HttpServletRequest req){
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

        Hashtable ht;
        String trama = "";
        Vector cuentasMto = new Vector();
        String valorFinal = "";
        ServicioTux tuxedo = new ServicioTux();
        ArchivoRemoto archRemoto = new ArchivoRemoto();


        Hashtable infoCuentas = new Hashtable();
		String infoArchivo = "infoCtas.mto";
		String infoLinea = "";
		String infoClave, infoContenido;
		BufferedReader infoEntrada;
		int n=0;

		try
		{
			// Hacer consulta a tuxedo
			trama = "2EWEB|"+session.getUserID8()+"|B0AC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile(); // <--- aqu� va la trama de consulta, de la estructura de Marco (Cuentas padre)
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama);
			trama = (String)ht.get("BUFFER");

			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... comboCuentas, trama regresada por Tuxedo= " + trama, EIGlobal.NivelLog.INFO);
			//mientras
			/*FileWriter salida = new FileWriter("/tmp/1001851.tmpAMT");
			salida.write("x\n");
			salida.write("x\n");
			salida.write("1000001 -- Cuenta1|xxxx\n");
			salida.write("1000002 -- Cuenta2|xxxx\n");
			salida.write("1000003 -- Cuenta3|xxxx\n");
			salida.close();*/

			// PROVISIONAL 2 linea para el despliegue de las cuentas hijas de Manteniemiento a Estructura
			//trama = "/tmp/1001851.tmpAMT1";

			// realizar copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
			if(archRemoto.copia(trama))
				{
				EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... comboCuentas, copia remota realizada (Tuxedo a local), trama= " + trama, EIGlobal.NivelLog.INFO);
				trama = Global.DIRECTORIO_LOCAL + "/" + trama;
				}
			else
				{
				EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
				String mensError = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				return "";
				//return null;
				}

			// Leer datos
			BufferedReader entrada = new BufferedReader(new FileReader(trama));
			String linea = entrada.readLine();

			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java> linea 1: " + linea, EIGlobal.NivelLog.INFO);

			if(linea != null) linea = entrada.readLine(); else return "";
			if(linea == null) return "";

			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java>linea 2: " + linea, EIGlobal.NivelLog.INFO);
			n = 2;
			while((linea = entrada.readLine()) != null)
				{
				n++;
				EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java> linea " + n + ": " + linea, EIGlobal.NivelLog.INFO);
				cuentasMto.add(linea.substring(0,linea.indexOf("|"))); //+descripcion);
				}

			//  Realizar consulta para obtener archivo de cuentas
			//  Obtener descripciones de las cuentas
			if(llamado_servicioCtasInteg("7@", " ", " ", " ",infoArchivo, req))
				{
				//- Hacer un parser del archivo y guardar la info en un Hastable: clave: cuenta, valor: descripci�n
				infoEntrada = new BufferedReader(new FileReader(Global.DOWNLOAD_PATH + infoArchivo));
				//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... comboCuentas: infoEntrada "+infoEntrada, EIGlobal.NivelLog.INFO);
				while((infoLinea = infoEntrada.readLine())!= null)
					{
					//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... comboCuentas: infoLinea "+infoLinea, EIGlobal.NivelLog.INFO);
					infoClave = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoContenido = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoCuentas.put(infoClave,infoContenido);
					}
				}

			// Construir c�digo HTML con la info
			while(cuentasMto.size()>0)
				{
				//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... comboCuentas: punto1", EIGlobal.NivelLog.INFO);
				//Hace coincidir la cuenta que nos interesa en el Hashtable de todas las cuentas para obtener la descripcion
				infoContenido = (String)infoCuentas.get((String)cuentasMto.get(0));
				if(infoContenido == null) infoContenido = "";
				valorFinal += "<OPTION value=\"" + (String)cuentasMto.get(0) + "\">" + (String)cuentasMto.remove(0) + " -- " + infoContenido + "</OPTION>";
				}
		}
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java> Error: " + e, EIGlobal.NivelLog.INFO);
			}
		EIGlobal.mensajePorTrace("<DEBUG bcProgFechas.java> valorFinal: " + valorFinal, EIGlobal.NivelLog.INFO);
		return valorFinal;
		//return cuentasMto;
	} // Fin comboCuentas

}//end class