/*
    Desarrollado por: Francisco Serrato Jimenez (fsj)
    Consultoria:      Getronics CP Mexico
    Proyecto:         Migracion de Fondos a 390(MX-2002-257)
**/

package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.lang.reflect.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

// TODO: Auto-generated Javadoc
//02/ENE/07

/**
 * La Clase BE_Posicion.
 */
public class BE_Posicion extends BaseServlet


{
	
	/** Variable Final EXT_AMBCI. */
	private static final String EXT_AMBCI = ".ambci";
	
	/** Variable Final MSJ_CARTERA. */
	private static final String MSJ_CARTERA = "-!?Cartera[";
	
	/** Variable Final MSJ_DISPONIBLE. */
	private static final String MSJ_DISPONIBLE = "] Disponible[";
	
	/** Variable Final MSJ_ACTUAL. */
	private static final String MSJ_ACTUAL = "] Actual[";
	
	/** Variable Final MSJ_HRS. */
	private static final String MSJ_HRS = "] 24Hrs.[";
	
	/** Variable Final MSJ_TITULAR. */
	private static final String MSJ_TITULAR = "] Titular[";
	
	/** Variable Final MSJ_DINERO. */
	private static final String MSJ_DINERO = "] Mesa Dinero[";
	
	/** Variable Final TD_CLASS. */
	private static final String TD_CLASS = "   <td class=\"";
	
	/** Variable Final CIERRA_TD. */
	private static final String CIERRA_TD = "</td>\n";
	
	/** Variable Final CIERRA_TR. */
	private static final String CIERRA_TR = "  </tr>\n";
	
	/** Variable Final CODIGO. */
	private static final String CODIGO = "s25040h";
	
	/** Variable Final TD_ALING. */
	private static final String TD_ALING = "   <td align=\"right\" class=\"";
	
	/** Variable tipoConsulta. */
	private String tipoConsulta = ""; 
	
	/** Variable fileOut. */
	private String fileOut="";
	

	    
	/**
	 * doGet.
	 * 
	 * @param request El HttpServletRequest obtenido de la vista
	 * @param response El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
    public void doGet(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
        defaultAction(request,response);
    }

	/**
	 * doPost.
	 * 
	 * @param request El HttpServletRequest enviado a la vista
	 * @param response El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
        defaultAction(request,response);
    }

    /**
     * Default action.
     *
     * @param request the request
     * @param response the response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void defaultAction(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException
    {
    	
    	EIGlobal.mensajePorTrace("BE_Posicion --> VERSION < v.1.0> ", EIGlobal.NivelLog.INFO);
    	
    	String tipoArchivo = request.getParameter("archivoExportacion");
    	tipoConsulta = (request.getParameter("tipoConsulta") != null ? request.getParameter("tipoConsulta") : "");
		
		EIGlobal.mensajePorTrace("BE_Posicion --> OBTIENE EXTENSION <"+tipoArchivo+"> ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("BE_Posicion --> OBTIENE soloPosicion <"+tipoConsulta+"> ", EIGlobal.NivelLog.INFO);
		
    	 if(tipoArchivo!=null&&!("".equals(tipoArchivo))&&"soloPosicion".equals(tipoConsulta)){
    		 EIGlobal.mensajePorTrace("BE_Posicion --> INICIA EXPORTACION <-- "+fileOut, EIGlobal.NivelLog.INFO);
    		 
    		 HttpSession exportSess = request.getSession();
    		 BaseResource sessiones = (BaseResource) exportSess.getAttribute("session");
//    		 String rutaArchivo = IEnlace.DOWNLOAD_PATH + fileOut;
    		 
    		 String rutaArchivo = IEnlace.DOWNLOAD_PATH + sessiones.getUserID8() + "be.doc";
    		 
    		 EIGlobal.mensajePorTrace("BE_Posicion --> EXPORTA RUTA <"+IEnlace.DOWNLOAD_PATH+"> ARCHIVO <"+fileOut+">", EIGlobal.NivelLog.INFO);
    		 
//    		 String rutaArchivo =  IEnlace.DOWNLOAD_PATH + "/"+ sessiones.getUserID8() + "be.doc";

			 EIGlobal.mensajePorTrace("BE_Posicion --> INICIADO EXPORTACION RUTA  "+rutaArchivo, EIGlobal.NivelLog.INFO);
			 exportarArchivo(tipoArchivo, rutaArchivo, response);	
    	 } else{
    	 
 	        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Comenzar[defaultAction();]", EIGlobal.NivelLog.INFO);
 	        HttpSession sess = request.getSession();
 	        BaseResource session = (BaseResource) sess.getAttribute("session");
 	        boolean sesionvalida = SesionValida(request,response);
 	                
 	        String cta_saldo    = ( request.getParameter("cta_saldo")!=null )   ? request.getParameter("cta_saldo")    : ""; 	       
	        String tipoConsulta = ( request.getParameter("tipoConsulta")!=null )? request.getParameter("tipoConsulta") : "";
 	        String m            = ( request.getParameter("j")!=null )           ? request.getParameter("j")            : "";
 	        EIGlobal.mensajePorTrace("-!?cta_Saldo["+ cta_saldo +"] tipoCta["+ tipoConsulta +"] m["+ m +"]", EIGlobal.NivelLog.INFO);
 	        String[][] arrayCuentas = null;
 	        String[]   CarregloAux     = desentramaC(m +","+ cta_saldo,',');
			String[]   Carreglo = new String[CarregloAux.length];
			
	        String [][] arrayCuentasTmp = (String[][]) sess.getAttribute("CUENTAS_POSICION");
			String indAux = "";
			String ctaAux = "";
			int indCta  = -1;
			 cta_saldo="";
			 Carreglo[0] = CarregloAux[0];
			 
			for(int i=1;i<CarregloAux.length;i++){
					// POST -----
	
					indCta = -1;
					indAux = CarregloAux[i];
					EIGlobal.mensajePorTrace("Se va a obtener de sesion las cuentas del indice:"+indAux, EIGlobal.NivelLog.INFO);
					
					if((indAux!=null)&&(!"".equals(indAux))){
						indCta = Integer.parseInt(indAux);
					}
					
					if((arrayCuentasTmp==null)||((indCta<1)||(indCta>=arrayCuentasTmp.length)))
					{
						despliegaPaginaErrorURL("Operaci&oacute;n no autorizada","Consulta de Posici&oacute;n","Consultas &gt; Posici&oacute;n", "s25100h", " javascript:history.back(); ", request, response);
						return;
					}else{
					
						ctaAux = arrayCuentasTmp[indCta][1];
					}
					cta_saldo+=ctaAux+",";
					EIGlobal.mensajePorTrace("Valor de la cuenta"+cta_saldo, EIGlobal.NivelLog.INFO);
					Carreglo[i]=ctaAux;
	        //POST ----
	        } 	        
   		
 	        if( sesionvalida && session.getFacultad(session.FAC_CONSULTA_SALDO) )
 	        {
 	            EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Sesion Valida", EIGlobal.NivelLog.INFO);
 	            llamado_servicioCtasInteg(IEnlace.MConsulta_Saldos_BE," "," "," ",session.getUserID8()+EXT_AMBCI,request);
 	            int contctas = getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR +"/"+ session.getUserID8() +EXT_AMBCI);
 	            arrayCuentas = ObteniendoCtasInteg(0,contctas,IEnlace.LOCAL_TMP_DIR +"/"+ session.getUserID8() +EXT_AMBCI);
 	            String[][] cuentasEnvio = new String[Carreglo.length][5];
 	            for(int i=1; i<Carreglo.length; i++ )
 	            {
 	                for(int j=1; j<arrayCuentas.length; j++)
 	                {
 	                    if( Carreglo[i].equals(arrayCuentas[j][1]) )
 	                        for(int k=0; k<5 ;k++)
 	                            cuentasEnvio[i-1][k] = arrayCuentas[j][k];
 	                }//fin for j
 	            }//fin for i
 		            //
 	            if("soloPosicion".equals(tipoConsulta)){	                        
 	                ConsultaPosicion(request,response,session,cuentasEnvio,Carreglo,cta_saldo);
 	            }
 	            else{
 	                ConsultaSaldos(request,response,session,cuentasEnvio,Carreglo);
 	            }													  	
 				
 	        }else {
 	            EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Sesion NO Valida", EIGlobal.NivelLog.INFO);
 	            request.setAttribute("MenuPrincipal",session.getstrMenu());
 	            request.setAttribute("Fecha"        ,ObtenFecha());
 	            request.setAttribute("ContUser"     ,ObtenContUser(request));
 	            request.setAttribute("MsgError"     ,IEnlace.MSG_PAG_NO_DISP);
 	            evalTemplate(IEnlace.ERROR_TMPL,request,response);
 			}
    	 }
    	 	        
	        //fin else
	        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Terminar[defaultAction();]", EIGlobal.NivelLog.INFO);	        				        
				   
    }

    /**
     * Consulta saldos.
     *
     * @param request the request
     * @param response the response
     * @param session the session
     * @param cuentasEnvio the cuentas envio
     * @param Carreglo the carreglo
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void ConsultaSaldos
        (
            HttpServletRequest request,
            HttpServletResponse response,
            BaseResource session,
            String[][] cuentasEnvio,
            String[] Carreglo
        )
            throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Comenzar[ConsultaSaldos();]", EIGlobal.NivelLog.INFO);
        int        elementos = 0;
        String     color     = "";
        String     numCta    = "";
        String     tipOper   = "";
        String     tipRela   = "";
        String     tablaHTML = "";
        String[]   respSaldo = null;
        BigDecimal saldoCart = new BigDecimal("0");
        BigDecimal saldoDisp = new BigDecimal("0");
        BigDecimal saldoActu = new BigDecimal("0");
        BigDecimal saldo24Hr = new BigDecimal("0");
        BigDecimal saldoMesa = new BigDecimal("0");
        BigDecimal saldoSuma = new BigDecimal("0");
        BigDecimal totalCart = new BigDecimal("0");
        BigDecimal totalDisp = new BigDecimal("0");
        BigDecimal totalActu = new BigDecimal("0");
        BigDecimal total24Hr = new BigDecimal("0");
        BigDecimal totalMesa = new BigDecimal("0");
        BigDecimal totalCol1 = new BigDecimal("0");
        BigDecimal totalCol2 = new BigDecimal("0");
        BigDecimal totalCol3 = new BigDecimal("0");
        try
        {
            String tramaEnvio           = "";
            String regresoServicios     = "";
            String tramaRegresoServicio = "";
            for(int indx=1; indx<=Integer.parseInt(Carreglo[0].trim()) ;indx++)
            {
                saldoCart = new BigDecimal("0");
                saldoDisp = new BigDecimal("0");
                saldoActu = new BigDecimal("0");
                saldo24Hr = new BigDecimal("0");
                saldoMesa = new BigDecimal("0");
                saldoSuma = new BigDecimal("0");
                ServicioTux tuxGlobal = new ServicioTux();
                //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                numCta  = cuentasEnvio[indx-1][1];
                tipRela = cuentasEnvio[indx-1][2];
                
                if("4".equals(cuentasEnvio[indx-1][3]))   
                	tipOper = "POSI";
                else if("5".equals(cuentasEnvio[indx-1][3]))
                	tipOper = "SDCR";
                else
                	tipOper = "SDCT";
                

                EIGlobal.mensajePorTrace("-!?tipOper["+ tipOper +"]", EIGlobal.NivelLog.INFO);
                
                if(!"".equals(tipOper) && !"".equals(tipRela))
                    tramaEnvio = "2EWEB|"+ session.getUserID8() +"|"+ tipOper +"|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ numCta +"|"+ tipRela;
                EIGlobal.mensajePorTrace("-!?Trama Enviar["+ tramaEnvio +"]", EIGlobal.NivelLog.INFO);
                try
                {
                    Hashtable hs = tuxGlobal.web_red(tramaEnvio);
                    if(hs != null){
                        tramaRegresoServicio = (String)hs.get("BUFFER");                    	
                    }    
                    EIGlobal.mensajePorTrace("-!?Trama Recibe["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.INFO);
                }catch (Exception e1){ EIGlobal.mensajePorTrace("-!?Exception Consulta Saldos["+ e1.toString() +"]", EIGlobal.NivelLog.INFO); }
                String nombreArchivo = Global.DIRECTORIO_LOCAL + tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/'));
                ArchivoRemoto archRem = new ArchivoRemoto();
                archRem.copia(tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/')+1));
                RandomAccessFile archivoRespuesta= new RandomAccessFile(nombreArchivo,"rw");
                String datSaldosArch = obtenerSaldosArchivo(archivoRespuesta);
                EIGlobal.mensajePorTrace("-!?Servicio Regreso["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("-!?    Trama Saldos["+ datSaldosArch +"]", EIGlobal.NivelLog.INFO);
                boolean ctaSaldos = true;
                if( !datSaldosArch.startsWith("OK ") )
                {
                    EIGlobal.mensajePorTrace("-!?servicio <No> responde...", EIGlobal.NivelLog.INFO);
                    request.setAttribute("errorEnPosicion","alert('Servicio de posicion no disponible');");
                    ctaSaldos = false;
                }
                else
                {
                    EIGlobal.mensajePorTrace("-!?servicio <Si> responde...", EIGlobal.NivelLog.INFO);
                    respSaldo = desentrama(datSaldosArch,';');
                    if( respSaldo.length>5 )
                    {
                        EIGlobal.mensajePorTrace(MSJ_CARTERA+ respSaldo[1] +MSJ_DISPONIBLE+ respSaldo[2] +MSJ_ACTUAL+ respSaldo[3] +MSJ_HRS+ respSaldo[4] +MSJ_TITULAR+ respSaldo[5] +MSJ_DINERO+ respSaldo[6] +"]", EIGlobal.NivelLog.INFO);
                        saldoCart = new BigDecimal( quitarComas(respSaldo[1]) );
                        saldoDisp = new BigDecimal( quitarComas(respSaldo[2]) );
                        saldoActu = new BigDecimal( quitarComas(respSaldo[3]) );
                        saldo24Hr = new BigDecimal( quitarComas(respSaldo[4]) );
                        saldoMesa = new BigDecimal( quitarComas(respSaldo[6]) );
                        saldoSuma = saldoSuma.add( saldoCart ).add( saldoMesa );
                    }
                }//fin else if
                if( (indx%2)==0 )color="textabdatobs";
                else             color="textabdatcla";
                if( ctaSaldos && Carreglo[indx].length()>0 && respSaldo.length>5 )
                {
                    EIGlobal.mensajePorTrace(MSJ_CARTERA+ saldoCart.toString() +MSJ_DISPONIBLE+ saldoDisp.toString() +MSJ_ACTUAL+ saldoActu.toString() +MSJ_HRS+ saldo24Hr.toString() +MSJ_TITULAR+ respSaldo[5] +MSJ_DINERO+ saldoMesa.toString() +"]", EIGlobal.NivelLog.INFO);
                    tablaHTML += "  <tr>\n";
                    tablaHTML += TD_CLASS+ color +"\" align=\"center\"><input type=\"radio\" name=\"radioSel\" value=\""+ numCta +"|"+ saldoActu.toString() +"|"+ saldo24Hr.toString() +"|"+ saldoDisp.toString() +"#\" onclick=\"document.tsaldos.cta_posi.value='2|"+ numCta +"|"+ respSaldo[5] +"|"+ tipRela +"|'\"></td>\n";
                    tablaHTML += TD_CLASS+ color +"\" align=\"center\">&nbsp; "+ numCta                            +CIERRA_TD;
                    tablaHTML += TD_CLASS+ color +"\" align=\"left\"  >&nbsp; "+ respSaldo[5]                      +CIERRA_TD;
                    tablaHTML += TD_CLASS+ color +"\" align=\"right\" >&nbsp; "+ formatear(saldoMesa.toString(),2) +CIERRA_TD;
                    tablaHTML += TD_CLASS+ color +"\" align=\"right\" >&nbsp; "+ formatear(saldoCart.toString(),2) +CIERRA_TD;
                    tablaHTML += TD_CLASS+ color +"\" align=\"right\" >&nbsp; "+ formatear(saldoSuma.toString(),2) +CIERRA_TD;
                    tablaHTML += CIERRA_TR;

                    totalCart = totalCart.add( saldoCart );
                    totalDisp = totalDisp.add( saldoDisp );
                    totalActu = totalActu.add( saldoActu );
                    total24Hr = total24Hr.add( saldo24Hr );
                    totalMesa = totalMesa.add( saldoMesa );
                    totalCol1 = totalCol1.add( saldoMesa );
                    totalCol2 = totalCol2.add( saldoCart );
                    totalCol3 = totalCol3.add( saldoSuma );
                }
                else
                {
                    tablaHTML += "  <tr>\n";
                    tablaHTML += TD_CLASS+ color +"\" align=\"center\"><input type=\"radio\" name=\"radioSel\" value=\""+ numCta +"|||#\" onclick=\"document.tsaldos.cta_posi.value='2|"+ numCta +"'|||\"></td>\n";
                    tablaHTML += TD_CLASS+ color +"\" align=\"center\">&nbsp; "+ numCta +CIERRA_TD;
                    tablaHTML += TD_CLASS+ color +"\" align=\"center\" colspan=\"5\">Servicio No Disponible</td>\n";
                    tablaHTML += CIERRA_TR;

                }//fin else
                regresoServicios = regresoServicios +"@"+ numCta +"@"+ datSaldosArch;
                EIGlobal.mensajePorTrace("-!?regresoServicios ["+ regresoServicios+"]", EIGlobal.NivelLog.INFO);
                elementos ++;
            	//TODO TODO BIT CU1021, CU1061
    	        /*
    			 * VSWF-BMB-I
    			 */
                if ("ON".equals(Global.USAR_BITACORAS.trim())){
                try{
    	            HttpSession sess = request.getSession();
    		        BitaHelper bh = new BitaHelperImpl(request, session, sess);
    				BitaTransacBean bt = new BitaTransacBean();
    				bt = (BitaTransacBean)bh.llenarBean(bt);
    				if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.EC_SALDO_CUENTA_BANCA)){
    					bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_BANCA_CONS_SALDO_CUENTA);
    				}
    				if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
    						equals(BitaConstants.EC_POSICION_BANCA)){
    					bt.setNumBit(BitaConstants.EC_POSICION_BANCA_CONS_SALDOS);
    				}
    				if(session.getContractNumber()!=null){
    					bt.setContrato(session.getContractNumber());
    				}
    				if(numCta!=null){
    					if(numCta.length()>20){
    						bt.setCctaOrig(numCta.substring(0,20));
    					}else{
    						bt.setCctaOrig(numCta.trim());
    					}
    				}
    				if(tipOper!=null){
    					bt.setServTransTux(tipOper.trim());
    				}
    				if(tramaRegresoServicio!=null){
		    			if("OK".equals(tramaRegresoServicio.substring(0,2))){
		    				bt.setIdErr(tipOper.trim()+"0000");
		    			}else if(tramaRegresoServicio.length()>8){
	    					bt.setIdErr(tramaRegresoServicio.substring(0,8));
	    				}else{
	    					bt.setIdErr(tramaRegresoServicio.trim());
	    				}
    				}
    				bt.setImporte(saldoSuma.doubleValue());
    				BitaHandler.getInstance().insertBitaTransac(bt);
    			}catch (NumberFormatException e){
    				
    			}catch(SQLException e){
                	EIGlobal.mensajePorTrace(">> BE_Posicion --> Excepcion SQL Inserta Bitacora --> "+e.getMessage(), EIGlobal.NivelLog.INFO);

    			}catch(Exception e){
    				EIGlobal.mensajePorTrace(">> BE_Posicion --> Excepcion Inserta Bitacora --> "+e.getMessage(), EIGlobal.NivelLog.INFO);

    			}
                }
    	        /*
    			 * VSWF-BMB-F
    			 */
            }//fin for idnx
            tablaHTML += "  <tr>\n";
            tablaHTML += "   <td> &nbsp; </td>\n";
            tablaHTML += "   <td> &nbsp; </td>\n";
            tablaHTML += "   <td class=\"tittabdat\" align=\"right\">Totales</td>\n";
            tablaHTML += "   <td class=\"tittabdat\" align=\"right\">"+ formatear(totalCol1.toString(),2) +CIERRA_TD;
            tablaHTML += "   <td class=\"tittabdat\" align=\"right\">"+ formatear(totalCol2.toString(),2) +CIERRA_TD;
            tablaHTML += "   <td class=\"tittabdat\" align=\"right\">"+ formatear(totalCol3.toString(),2) +CIERRA_TD;
            tablaHTML += CIERRA_TR;

            session.setBEPosiciones(regresoServicios);
            session.setElementos(String.valueOf(elementos));
            request.setAttribute("MenuPrincipal",session.getStrMenu());
            request.setAttribute("newMenu"      ,session.getFuncionesDeMenu());
            request.setAttribute("Encabezado"   ,CreaEncabezado("Consulta de saldos por cuenta de Banca especializada","Consultas &gt; Saldos &gt; Por cuenta &gt; Banca Especializada",CODIGO,request));
            request.setAttribute("Fecha"        ,ObtenFecha());
            request.setAttribute("ContUser"     ,ObtenContUser(request));
            request.setAttribute("cta"          ,tablaHTML);

            evalTemplate(IEnlace.BE_TRAESALDO_TMPL,request,response);
            EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Comenzar[ConsultaSaldos();]", EIGlobal.NivelLog.INFO);
            return;
        }catch(Exception e){ EIGlobal.mensajePorTrace("-!?Exception ConsultaSaldos["+ e.toString() +"]", EIGlobal.NivelLog.INFO);
            }
        despliegaPaginaError("Servicio de Consulta de Saldos no disponible","Consulta de Saldos por cuenta de Banca especializada", "Consultas &gt; Saldos &gt; Por cuenta &gt; Banca Especializada","s25040h", request,response);
        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Terminar[ConsultaSaldos();]", EIGlobal.NivelLog.INFO);
    }

    /**
     * Consulta posicion.
     *
     * @param request the request
     * @param response the response
     * @param session the session
     * @param cuentasEnvio the cuentas envio
     * @param Carreglo the carreglo
     * @param cta_saldo the cta_saldo
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void ConsultaPosicion
        (
            HttpServletRequest request,
            HttpServletResponse response,
            BaseResource session,
            String[][] cuentasEnvio,
            String[] Carreglo,
            String cta_saldo
        )
            throws ServletException, IOException
    {
        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Comenzar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
        int        recordsOnFile = 0;
        String     color         = "";
        String     numCta        = "";
        String     tipOper       = "";
        String     tipRela       = "";
        String     tablaHTML     = "";
        String[]   respSaldo     = null;
        BigDecimal sldCartera    = new BigDecimal("0");
        BigDecimal saldoCart     = new BigDecimal("0");
        BigDecimal saldoDisp     = new BigDecimal("0");
        BigDecimal saldoActu     = new BigDecimal("0");
        BigDecimal saldo24Hr     = new BigDecimal("0");
        BigDecimal saldoMesa     = new BigDecimal("0");
        HttpSession sess = request.getSession();
        cta_saldo = cta_saldo.substring(0,cta_saldo.length()-1);
        ServicioTux tuxGlobal = new ServicioTux();
        //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        
       
		fileOut = session.getUserID8() + "be.doc";
		boolean grabaArchivo = true;
		
		 EIGlobal.mensajePorTrace("-*** BE_Posicion ARCHIVO = "+fileOut, EIGlobal.NivelLog.INFO);
		
		String valorarchivo = "";
        
        RandomAccessFile archivoRespuesta;
        
        if("4".equals(cuentasEnvio[0][3]))
        	tipOper="POSI";
        else if("5".equals(cuentasEnvio[0][3]))
        	tipOper="SDCR";
        else
        	tipOper="SDCT";
        

        EIGlobal.mensajePorTrace("-!?***tipOper["+ tipOper +"]", EIGlobal.NivelLog.INFO);
        tipRela = cuentasEnvio[0][2];
        String tramaEnvio = "2EWEB|"+ session.getUserID8() +"|"+ tipOper +"|"+ session.getContractNumber() +"|"+ session.getUserID8() +"|"+ session.getUserProfile() +"|"+ cta_saldo +"|"+ tipRela;
        EIGlobal.mensajePorTrace("-!?***Trama Enviar["+ tramaEnvio +"]", EIGlobal.NivelLog.INFO);
        String tramaRegresoServicio = "";
        try
        {
            Hashtable hs = tuxGlobal.web_red(tramaEnvio);
            if(hs != null){
            	tramaRegresoServicio = (String) hs.get("BUFFER");
            }
            EIGlobal.mensajePorTrace("-!?***Trama Recibe["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.DEBUG);
        }catch(Exception e1){
        	EIGlobal.mensajePorTrace(">> BE_Posicion --> ConsultaPosicion -- Recupera Trama --> "+e1.getMessage(), EIGlobal.NivelLog.ERROR);
        }
        String nombreArchivo = Global.DIRECTORIO_LOCAL+ tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/'));
        ArchivoRemoto archRem = new ArchivoRemoto();
        archRem.copia(tramaRegresoServicio.substring(tramaRegresoServicio.lastIndexOf('/')+1));
        try
        {
            archivoRespuesta = new RandomAccessFile(nombreArchivo,"rw");
            String datSaldosArch = obtenerSaldosArchivo(archivoRespuesta);
            EIGlobal.mensajePorTrace("-!?***Servicio Regreso["+ tramaRegresoServicio +"]", EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("-!?***    Trama Saldos["+ datSaldosArch +"]", EIGlobal.NivelLog.INFO);
            
            
        	EI_Exportar ArcSal;
			ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			ArcSal.creaArchivo();
                        
            if( datSaldosArch.startsWith("OK ") )
            {
                tablaHTML += "  <tr align=\"center\">\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Emisora       </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">T&iacute;tulos</td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Costo Promedio</td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Precio        </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">Fondos        </td>\n";
                tablaHTML += "   <td class=\"tittabdat\" width=\"90\">% Cartera     </td>\n";
                tablaHTML += CIERRA_TR;

                Vector datPosicionArch = obtenerPosicionArchivo(archivoRespuesta);
                EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Registros["+ datPosicionArch.size() +"]", EIGlobal.NivelLog.INFO);
                if( datPosicionArch.isEmpty() )
                {
                    tablaHTML += "  <tr align=\"center\">\n";
                    tablaHTML += "   <td class=\"textabdatobs\" colspan=\"6\">&nbsp;  El contrato no tiene Posici&oacute;n &nbsp;</td>\n";
                    tablaHTML += CIERRA_TR;

                }//fin if
                else
                {
                    respSaldo = desentrama(datSaldosArch,';');
                    if( respSaldo.length>5 )
                    {
                        EIGlobal.mensajePorTrace(MSJ_CARTERA+ respSaldo[1] +MSJ_DISPONIBLE+ respSaldo[2] +MSJ_ACTUAL+ respSaldo[3] +MSJ_HRS+ respSaldo[4] +MSJ_TITULAR+ respSaldo[5] +MSJ_DINERO+ respSaldo[6] +"]", EIGlobal.NivelLog.INFO);
                        saldoCart = new BigDecimal( quitarComas(respSaldo[1]) );
                        saldoDisp = new BigDecimal( quitarComas(respSaldo[2]) );
                        saldoActu = new BigDecimal( quitarComas(respSaldo[3]) );
                        saldo24Hr = new BigDecimal( quitarComas(respSaldo[4]) );
                        saldoMesa = new BigDecimal( quitarComas(respSaldo[6]) );
                    }//fin if
                    EIGlobal.mensajePorTrace(MSJ_CARTERA+ saldoCart.toString() +MSJ_DISPONIBLE+ saldoDisp.toString() +MSJ_ACTUAL+ saldoActu.toString() +MSJ_HRS+ saldo24Hr.toString() +MSJ_DINERO+ saldoMesa.toString() +"]", EIGlobal.NivelLog.INFO);
                    Vector regPosi = new Vector();
                    for(int i=0,indx=0; i<datPosicionArch.size(); i++,indx+=4)
                    {
                        String[] colPosi = new String[5];
                        colPosi[0] = ((String[])datPosicionArch.elementAt(i))[0];   //Emisora
                        colPosi[1] = ((String[])datPosicionArch.elementAt(i))[1];   //Titulos
                        colPosi[2] = ((String[])datPosicionArch.elementAt(i))[2];   //Promedio
                        colPosi[3] = ((String[])datPosicionArch.elementAt(i))[3];   //Precio
                        try
                        {   //Obtener Importe
                            BigDecimal tmp1 = new BigDecimal(colPosi[1]);
                            BigDecimal tmp2 = new BigDecimal(colPosi[3]);
                            BigDecimal tmp3 = new BigDecimal("0");
                            tmp3 = tmp1.multiply(tmp2);
                            colPosi[4] = tmp3.setScale(2,BigDecimal.ROUND_DOWN).toString();     //Importe
                        }catch(Exception e1){ colPosi[4] = "0"; }
                        EIGlobal.mensajePorTrace("-!?Emisora["+ colPosi[0] +"]\tTitulos["+ colPosi[1] +"]\tPromedio["+ colPosi[2] +"]\tPrecio["+ colPosi[3] +"]\tImporte["+ colPosi[4] +"]", EIGlobal.NivelLog.INFO);
                        sldCartera = sldCartera.add( new BigDecimal(colPosi[4]) );
                        regPosi.addElement(colPosi);
                    }//fin for i
                    for(int i=0; i<regPosi.size() ;i++)
                    {
                        String porCartera = "0";
                        try
                        {   //Obtener Porcentaje
                            BigDecimal tmp1 = new BigDecimal( ((String[])regPosi.elementAt(i))[4] );    //Importe
                            BigDecimal tmp2 = tmp1.multiply(new BigDecimal("100"));
                            tmp2 = tmp2.divide(sldCartera, 3);
                            porCartera = tmp2.setScale(2,BigDecimal.ROUND_DOWN).toString();     //Porcentaje
                        }catch(Exception e1){  }
                        if( (i%2)==0 ) color="textabdatobs";
                        else           color="textabdatcla";
                        tablaHTML += "  <tr>";
                        tablaHTML += "   <td align=\"left\"  class=\""+ color +"\">&nbsp; "+            ((String[])regPosi.elementAt(i))[0]    +"&nbsp;</td>\n";
                        tablaHTML += TD_ALING+ color +"\">&nbsp; "+ formatear( ((String[])regPosi.elementAt(i))[1],0) +"&nbsp;</td>\n";
                        tablaHTML += TD_ALING+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[2],8) +"&nbsp;</td>\n";
                        tablaHTML += TD_ALING+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[3],8) +"&nbsp;</td>\n";
                        tablaHTML += TD_ALING+ color +"\">&nbsp;$"+ formatear( ((String[])regPosi.elementAt(i))[4],2) +"&nbsp;</td>\n";
                        tablaHTML += TD_ALING+ color +"\">&nbsp; "+ formatear( porCartera                         ,2) +"&nbsp;</td>\n";
                        tablaHTML += CIERRA_TR;
                        
                        valorarchivo = ((String[])regPosi.elementAt(i))[0] +";"+formatear( ((String[])regPosi.elementAt(i))[1],0)+";"+
                        		formatear( ((String[])regPosi.elementAt(i))[2],8)+";"+formatear( ((String[])regPosi.elementAt(i))[3],8)+";"+
                        		formatear( ((String[])regPosi.elementAt(i))[4],2)+";"+formatear( porCartera                         ,2);
                        
                        if(grabaArchivo){
							ArcSal.escribeLinea(valorarchivo + "\r\n");
							EIGlobal.mensajePorTrace("FSWV ---> BE_Posicion --> Linea Exportar  "+valorarchivo, EIGlobal.NivelLog.DEBUG);
						}
                        
                        
                        ArchivoRemoto archR = new ArchivoRemoto();
                        
                        archR.copiaLocalARemoto(fileOut,"WEB");
                        

                    }//fin for
                    if(grabaArchivo){
                    	ArcSal.cierraArchivo();
                    }
    					
                }//fin else
                request.setAttribute("MenuPrincipal",session.getStrMenu());
                request.setAttribute("newMenu"      ,session.getFuncionesDeMenu());
                request.setAttribute("Encabezado"   ,CreaEncabezado("Consulta de Posici&oacute;n por cuenta de Banca especializada","Consultas &gt; Posici&oacute;n &gt; Por cuenta &gt; Banca Especializada",CODIGO,request));
                request.setAttribute("count"        ,cta_saldo);
                request.setAttribute("totalMesa"    ,formatear(saldoMesa.toString(),2));
                request.setAttribute("totalCart"    ,formatear(saldoCart.toString(),2));
                request.setAttribute("tabla"        ,tablaHTML);
                // TODO TODO BIT CU1061
		        /*
				 * 02/ENE/07
				 * VSWF-BMB-I
				 */
                if ("ON".equals(Global.USAR_BITACORAS.trim())){
                try{
			        BitaHelper bh = new BitaHelperImpl(request, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							equals(BitaConstants.EC_POSICION_BANCA)){
						bt.setNumBit(BitaConstants.EC_POSICION_BANCA_CONS_POS_SALDOS);
					}
					if((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
							equals(BitaConstants.EC_SALDO_CUENTA_BANCA)){
						bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_BANCA_CONS_SOLO_POS_CUENTA);
					}
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					if(cta_saldo!=null){
						if(cta_saldo.length()>20){
							bt.setCctaOrig(cta_saldo.substring(0,20));
						}else{
							bt.setCctaOrig(cta_saldo.trim());
						}
					}
					if(tipOper!=null){
						bt.setServTransTux(tipOper);
					}
					bt.setImporte(saldoDisp.doubleValue());
					if(tramaRegresoServicio!=null){
		    			if("OK".equals(tramaRegresoServicio.substring(0,2))){
		    				bt.setIdErr(tipOper.trim()+"0000");
		    			}else if(tramaRegresoServicio.length()>8){
							bt.setIdErr(tramaRegresoServicio.substring(0,8));
						}else{
							bt.setIdErr(tramaRegresoServicio.trim());
						}
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (NumberFormatException e){}
                catch(SQLException e){
					 EIGlobal.mensajePorTrace(">> BE_Posicion --> ConsultaPosicion --> SQLException "+e.getMessage(), EIGlobal.NivelLog.ERROR);

				}catch (Exception e){
					EIGlobal.mensajePorTrace(">> BE_Posicion --> ConsultaPosicion --> Excepcion "+e.getMessage(), EIGlobal.NivelLog.ERROR);

				}
                }
		        /*
		         * VSWF-BMB-F
				 */
                evalTemplate(IEnlace.BE_POSICION_TMPL,request,response);
                EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Terminar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
                return;
            }//fin if
        }catch(Exception e){
        	EIGlobal.mensajePorTrace("-!?Excepcion ConsultaPosicion["+ e +"]", EIGlobal.NivelLog.ERROR); 
        }
        despliegaPaginaError("Servicio de Consulta de Posici&oacute;n no disponible","Consulta de Posici&oacute;n por cuenta de Banca especializada", "Consultas &gt; Posici&oacute;n &gt; Por cuenta &gt; Banca Especializada",CODIGO, request,response);
        EIGlobal.mensajePorTrace("-!?***BE_Posicion.class Terminar[ConsultaPosicion();]", EIGlobal.NivelLog.INFO);
    }

    /**
     * Obtener saldos archivo.
     *
     * @param archivoSalida the archivo salida
     * @return the string
     */
    public String obtenerSaldosArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct    = 0;
        long   lngArch   = 0;
        String tramaArch = "";
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch += archivoSalida.readLine();
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch && numLineas<2 );
        }catch(Exception e){  }
        return tramaArch;
    }

    /**
     * Obtener posicion archivo.
     *
     * @param archivoSalida the archivo salida
     * @return the vector
     */
    public Vector obtenerPosicionArchivo(RandomAccessFile archivoSalida)
    {
        long   posAct    = 0;
        long   lngArch   = 0;
        String tramaArch = "";
        Vector regPosi   = new Vector();
        try
        {
            int numLineas = 0;
            archivoSalida.seek(0);
            lngArch = archivoSalida.length();
            do
            {
                tramaArch = archivoSalida.readLine();
                if( numLineas>1 )
                    regPosi.addElement(desentrama(tramaArch,';'));
                posAct = archivoSalida.getFilePointer();
                numLineas ++;
            }while( posAct<lngArch );
        }catch(Exception e){  }
        return regPosi;
    }

    /**
     * Desentrama.
     *
     * @param trama the trama
     * @param delimitador the delimitador
     * @return the string[]
     */
    private String[] desentrama(String trama,char delimitador)
    {
        int i = 0;
        String as[] = null;
        for(int j=0; j<trama.length(); j++)
            if( trama.toCharArray()[j]==delimitador )
                i ++;
        try
        {
            as = new String[i];
            for(int k=0; k<=i ;k++)
            {
                as[k] = trama.substring(0,trama.indexOf(delimitador)).trim();
                trama = trama.substring(trama.indexOf(delimitador)+1,trama.length());
            }//fin for k
        }
        catch(Exception e){  }
        return as;
    }

    /**
     * Formatear.
     *
     * @param format the format
     * @param decimal the decimal
     * @return the string
     */
    private String formatear(String format,int decimal)
    {
        try
        {
            BigDecimal importe = new BigDecimal(format).setScale(decimal,BigDecimal.ROUND_DOWN);
            NumberFormat formateo = NumberFormat.getNumberInstance();
            formateo.setMaximumFractionDigits(decimal);
            formateo.setMinimumFractionDigits(decimal);
            format = formateo.format(importe);
        }catch(Exception e){  }
        return format;
    }

    /**
     * Quitar comas.
     *
     * @param cad the cad
     * @return the string
     */
    private String quitarComas(String cad)
    {
        String aux = "";
        try
        {
            for(int i=0; i<cad.length() ;i++)
                if( cad.toCharArray()[i]!=',' )
                    aux += ""+ cad.toCharArray()[i];
        }catch(Exception e){  }
        return aux;
    }
    
	/**
	 * Exportar archivo.
	 *
	 * @param tipoArchivo El tipo de archivo a exportar
	 * @param nombreArchivo La ruta del archivo de donde se obtienen los datos
	 * @param res El request de la pagina
	 */
    private void exportarArchivo(String tipoArchivo, String nombreArchivo, HttpServletResponse res)
    {
      EIGlobal.mensajePorTrace("BE_Posicion -> ARCHIVO DE ENTRADA  -->" + nombreArchivo + "<--", EIGlobal.NivelLog.DEBUG);

      ExportModel em = new ExportModel("bEspecializada", '|', ";")
      	.addColumn(new Column(0, "Emisora"))
      	.addColumn(new Column(1, "Titulos","$#########################0.00"))
		.addColumn(new Column(2, "Costo Promedio","$#########################0.00"))
		.addColumn(new Column(3, "Precio","$#########################0.00"))
		.addColumn(new Column(4, "Fondos","$#########################0.00"))
		.addColumn(new Column(5, "% Cartera","$#########################0.00"));
      try
      {
        EIGlobal.mensajePorTrace("BE_Posicion -> Generando Exportacion --> ", EIGlobal.NivelLog.DEBUG);
        HtmlTableExporter.export(tipoArchivo, em, nombreArchivo, res);
      } catch (IOException ex) {
        EIGlobal.mensajePorTrace("BE_Posicion -> ERROR --> Exportar Archivo -> " + ex.getMessage(), EIGlobal.NivelLog.ERROR);
      }
    }
    
}

/*fsj*/