

package mx.altec.enlace.servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import java.sql.*;
import java.io.UnsupportedEncodingException;
import org.apache.commons.lang.CharSet;

// TODO: Auto-generated Javadoc
/**
 * The Class MDI_Comprobante.
 */
public class MDI_Comprobante extends BaseServlet{

	/**
	 * DoGet.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * DoPost.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
try{
	   int indice=0;
	   EI_Tipo Env=new EI_Tipo(this);

	   EIGlobal.mensajePorTrace("MDI_Comprobante - execute(): Entrando a Comprobante de Dep. Inter.", EIGlobal.NivelLog.INFO);
       String cadena=(String) req.getParameter("TransEnv");
	   //String cadena=new String(req.getParameter("TransEnv").toString().getBytes("ISO-8859-1"), "UTF-8");
       EIGlobal.mensajePorTrace("MDI_Comprobante - cadena. " + cadena, EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("MDI_Comprobante - cadena. Encoding " + new String(req.getParameter("TransEnv").toString().getBytes("ISO-8859-1"), "UTF-8"), EIGlobal.NivelLog.INFO);
//       if(req.getParameter("origenComprobante") != null){
//    	   cadena=new String(req.getParameter("TransEnv").toString().getBytes("UTF-8"), "ISO-8859-1");
//       }
	   Env.iniciaObjeto(cadena);
	   indice=Integer.parseInt((String) req.getParameter("Indice"));
	   EIGlobal.mensajePorTrace("MDI_Comprobante - execute(): Elemento no. " + indice, EIGlobal.NivelLog.INFO);
	   iniciaComprobante(Env,indice, req, res );

	   EIGlobal.mensajePorTrace("MDI_Comprobante - execute(): Saliendo de Comprobante de Dep. Inter.", EIGlobal.NivelLog.INFO);
}
catch(Exception e)
  {e.printStackTrace();
  EIGlobal.mensajePorTrace("MDI_Comprobante - error. " + e.getMessage(), EIGlobal.NivelLog.INFO);
	}
	}


	/**
	 * Inicia comprobante.
	 *
	 * @param Env the env
	 * @param indice the indice
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void iniciaComprobante(EI_Tipo Env,	int indice, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	  String totales="";
	  String banco="";
	  String plaza="";
	  String strMen="";
	  StringBuilder strTabla = new StringBuilder();
	  strTabla.delete(0, strTabla.length());


        String[] titulo= {  "N&uacute;mero de Referencia",
                            "Cuenta Cargo",
                            "Cuenta CLABE",
                            "Titular",
                            "Cuenta Abono/M&oacute;vil",
                            "Beneficiario",
                            "Importe",
                            "Concepto  del Pago / Transferencia",
                            "Referencia Interbancaria",
                            "Banco",
                            "Plaza",
                            "Sucursal",
                            "Estatus",
                            "Fecha",
                            "Clave de Rastreo",
                            "RFC Beneficiario",
                            "Importe IVA",
                            "Forma Aplicaci&oacute;n",
                            "Fecha y Hora de Aplicaci&oacute;n",
                            "Motivo de devoluci&oacute;n"};
        
      int total_campos = 20;
      boolean existeCLABE=false; // 13/05/2004 SPEI-Cta ordenante v1 jbg

	  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Total de Campos:" + total_campos, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Total de Campos en objeto: "+ Env.totalCampos, EIGlobal.NivelLog.INFO);

      if(Env.camposTabla[0][2]!=null && Env.camposTabla[0][2].trim().length()==18)
        {
            existeCLABE=true;
			EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): SI hay CLABE:", EIGlobal.NivelLog.INFO);

        }

	  for(int i=0;i<Env.totalCampos;i++)
		  EIGlobal.mensajePorTrace("MDI_Comprobante =======> titulo["+i+"] :"  + titulo[i] + " = " + Env.camposTabla[0][i], EIGlobal.NivelLog.INFO);

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	  Connection conn=null;
	  PreparedStatement contQuery=null;
	  ResultSet contResult=null;

	  Connection conn1=null;
	  PreparedStatement contQuery1=null;
	  ResultSet contResult1=null;

	  try
	   {
		 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Verificando elementos", EIGlobal.NivelLog.INFO);
		 if(Env.camposTabla[0][6]!=null && Env.camposTabla[0][6].trim().length()>0) //Importe
		 EnlaceGlobal.formateaImporte(Env,(6)); //Importe
		 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Importe", EIGlobal.NivelLog.INFO);

		 //if(Env.totalCampos>=(total_campos-3))  //IVA
		   if(Env.camposTabla[0][16]!=null && Env.camposTabla[0][16].trim().length()>0) //IVA
			 EnlaceGlobal.formateaImporte(Env,(16));//IVA
		   EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): IVA", EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Conversion Datos", EIGlobal.NivelLog.INFO);


		 for(int i=0;i<total_campos;i++)
		  {

            if ((!existeCLABE) && (i==2)) i++;  // No hay cuenta clabe
            strTabla.append("\n <tr><td class='tittabcom' align='right' >");
            strTabla.append(titulo[i]).append(":</td>");
            strTabla.append("\n<td class='textabcom' nowrap>");

			EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Armando datos.", EIGlobal.NivelLog.INFO);

			if(i==9)//Banco
			 {
			   try
				{
		  		  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Intentando conexion A.", EIGlobal.NivelLog.INFO);
				  conn = createiASConn(Global.DATASOURCE_ORACLE);
				  if(conn==null)
					  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear la conexion.", EIGlobal.NivelLog.ERROR);
				  else
				   {
					  totales="Select nombre_corto from comu_interme_fin where num_cecoban<>0 and cve_interme='"+Env.camposTabla[indice][i]+"'";
					  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Query." + totales, EIGlobal.NivelLog.INFO);
					  contQuery = conn.prepareStatement(totales);
					  if(contQuery==null)
						EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
					  else
					   {
						 contResult=contQuery.executeQuery();
						 if(contResult==null)
						   EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
						 else
						   {
							 contResult.next();
							 banco=contResult.getString(1);
							 if(banco!=null)
							   Env.camposTabla[indice][i]=banco.trim();
						   }
					   }
				   }
				}catch(Exception e)
				 {EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Exception, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);}
				finally{
					if(contResult!=null)
						contResult.close();
					if(conn!=null)
					 conn.close();
				}

			 }

			EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Buscando elemento siguiente.", EIGlobal.NivelLog.INFO);

			if(i==10)//Plaza
			 {
			   try
				{
				  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Intentando conexion. B", EIGlobal.NivelLog.INFO);
				  conn1 = createiASConn(Global.DATASOURCE_ORACLE);
				  if(conn==null){
					  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear la conexion.", EIGlobal.NivelLog.ERROR);
				  }else
				   {
					  totales="Select descripcion from comu_pla_banxico where plaza_banxico='"+Env.camposTabla[indice][i]+"'";
					  EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Query." + totales, EIGlobal.NivelLog.INFO);
					  contQuery1=conn1.prepareStatement(totales);
					  if(contQuery1==null)
						EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
					  else
					   {
						 contResult1=contQuery1.executeQuery();
						 if(!contResult1.next())
						   EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
						 else
						  {
							plaza=contResult1.getString(1);
							if(plaza!=null){
							  Env.camposTabla[indice][i]=plaza.trim();
							}
						  }
					   }
					}
				}catch(Exception e)
				 {EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Exception, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);}
				finally{
					if(contResult1!=null)
						contResult1.close();
					if(conn1!=null)
					 conn1.close();
				}
			 }

			EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Informacion: "+Env.camposTabla[indice][i]+" no. "+i, EIGlobal.NivelLog.INFO);
			strTabla.append(Env.camposTabla[indice][i]).append("</td>");
			strTabla.append("</tr>");
				
			//Inicia modificación FSW TCS
			if( i==5 && "USD".equals(req.getParameter("tipoMoneda"))){ // Leyenda debajo del Beneficiario solo en divisas USD TCS FSW
		    	EIGlobal.mensajePorTrace("TCS ::::::::::::::::  MDI_Comporobante - Leyenda - Beneficiario "+i,EIGlobal.NivelLog.INFO);
		    	strTabla.append("\n <tr><td class='tittabcom' align='right' >");
		    	strTabla.append("</td>");
		    	strTabla.append("\n<td class='textabcom' nowrap style='font-style:italic'>");
		    	strTabla.append("Dato no verificado por esta instituci&oacute;n");
		    	strTabla.append("</td>");
		    	strTabla.append("</tr>");		
			}
			//FIN modificación FSW TCS
			
		    if(i==6 && req.getParameter("tipoMoneda")!= null){ // Importe ARS VECTOR SPID
		    	EIGlobal.mensajePorTrace("MDI_Comporobante - Campo Divisa "+i,EIGlobal.NivelLog.INFO);
		    	strTabla.append("\n <tr><td class='tittabcom' align='right' >");
		    	strTabla.append("Divisa:</td>");
		    	strTabla.append("\n<td class='textabcom' nowrap>");
		    	strTabla.append(req.getParameter("tipoMoneda"));
		    	strTabla.append("</td>");
		    	strTabla.append("</tr>");		
			}
		  }
		}catch( SQLException sqle ){
			sqle.printStackTrace();
			
			strTabla.append("<tr><td class='tittabcom' align='right'>");
			strTabla.append("<br><font color=red>Error</font> en la b&uacute;squeda de los datos. Intente nuevamente.<br><br><br>");
			strTabla.append("</td></tr>");
		}

	 String ncontrato_ = "";
     String contrato_   = "";
	 String nusuario_ = "";
     String usuario_   = "";
	 String cvebanco_ = "";

	 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Contrato y nombre ", EIGlobal.NivelLog.INFO);

	 if(req.getParameter("NumContrato")!= null)
		contrato_ = (String)req.getParameter("NumContrato");
	 if(req.getParameter("NomContrato")!= null)
		ncontrato_ = (String) req.getParameter("NomContrato");
	 if(req.getParameter("NumUsuario")!= null)
		nusuario_ = (String)req.getParameter("NumUsuario");
	 if(req.getParameter("NomUsuario")!= null)
		usuario_ = (String) req.getParameter("NomUsuario");
	 if(req.getParameter("ClaveBanco")!= null)
		cvebanco_ = (String) req.getParameter("ClaveBanco");

	 req.setAttribute("NomContrato",ncontrato_);
	 req.setAttribute("NumContrato",contrato_);
	 req.setAttribute("NumUsuario",nusuario_);
	 req.setAttribute("NomUsuario",usuario_);
	 req.setAttribute("ClaveBanco",cvebanco_);
	 EIGlobal.mensajePorTrace("MDI_Comprobante - strTabla(): strTabla["+strTabla+"]", EIGlobal.NivelLog.INFO);
	 req.setAttribute("Tabla",strTabla.toString());
	 EIGlobal.mensajePorTrace("MDI_Comprobante - strTabla(): strTabla Encoding["+new String(strTabla.toString().getBytes("ISO-8859-1"), "UTF-8")+"]", EIGlobal.NivelLog.INFO);
	 req.setAttribute("Mensaje",strMen);

	 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Finalizando", EIGlobal.NivelLog.INFO);


	 //TODO BIT CU3131 Envia a generar comprobante

	 	/**
		 * VSWF - FVC - I
		 * 17/Enero/2007
		 */
	 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try {
			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();

			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(mx.altec.enlace.bita.BitaConstants.ER_TESO_INTER_TRANSFER_INTER_CONSULTA_DB);
			bt.setContrato((contrato_ == null)?" ":contrato_);

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	 }
		/**
		 * VSWF - FVC - F
		 */

	 evalTemplate("/jsp/MDI_Comprobante.jsp", req, res);
	}
}