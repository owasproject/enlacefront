package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;



/**
 * The Class MCL_Posicion.
 */
public class MCL_Posicion extends BaseServlet
{
	
	/** The pipe. */
	private static final String PIPE="|";

	/**  Encabezado Consulta Posicion. */
	private static final String ENCABEZADO_POSICION = "Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito";
	
	/**  Encabezado Consulta Posicion Linea de Credito. */
	private static final String ENCABEZADO_POSICION_LDC = "Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito";
	
	/**  Numero Contrato. */
	private static final String NUM_CONTRATO = "NumContrato";
	
	/**  Numero de contrato. */
	private static final String NOM_CONTRATO = "NomContrato";
	
	/**  Numero Usuario. */
	private static final String NUM_USUARIO = "NumUsuario";
	
	/**  Numero de Usuario. */
	private static final String NOM_USUARIO = "NomUsuario";
	
	/**  Mensaje de Error. */
	private static final String ERRORES = "Errores";
	
	/**  Mensaje estatus error. */
	private static final String ESTATUS_ERROR = "EstatusError";
	
	/**  Exportar. */
	private static final String EXPORTAR = "Exportar";

	/**  Fecha hoy. */
	private static final String FECHA_HOY = "FechaHoy";
	
	/**  Nuevo Menu. */
	private static final String NEW_MENU = "newMenu";
	
	/**  Menu Principal. */
	private static final String MENU_PRINCIPAL = "MenuPrincipal";
	
	/**  Encabezado. */
	private static final String ENCABEZADO = "Encabezado";
	
	/**  Caracter Arroba. */
	private static final String ARROBA = "@";
	
	/**  Espacio en blanco. */
	private static final String ESPACIO = " ";

	/**
	 * doGet.
	 * 
	 * @param request El HttpServletRequest obtenido de la vista
	 * @param response El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}


	/**
	 * doPost.
	 * 
	 * @param request El HttpServletRequest enviado a la vista
	 * @param response El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	/**
	 * Default action.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {
	 
	EIGlobal.mensajePorTrace("MCL_Posicion - PRUEBA CONSULTA TDC 270515", EIGlobal.NivelLog.INFO);

	 EIGlobal.mensajePorTrace("MCL_Posicion - execute(): Entrando a Posicion Lineas-Tarjetas.", EIGlobal.NivelLog.INFO);

		String contrato     = "";
		String usuario      = "";
		String clavePerfil  = "";
		String tipoArchivoExport	= request.getParameter("tipoArchivoExport");

		EI_Tipo Seleccion	 = new EI_Tipo(this);
		EI_Tipo FacArchivo	 = new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        boolean sesionvalida=SesionValida( request, response );
        if(sesionvalida)
		 {
		   //Variables de sesion ...
		   contrato=session.getContractNumber();
		   usuario=session.getUserID8();
		   clavePerfil=session.getUserProfile();
		   
		   EIGlobal.mensajePorTrace("MCL_Posicion - VALOR ARCHIVO <"+tipoArchivoExport+">", EIGlobal.NivelLog.INFO);
		   
		   if (tipoArchivoExport==null) {		
			   EIGlobal.mensajePorTrace("MCL_Posicion -DESENTRAMA ", EIGlobal.NivelLog.INFO);
			   // Cuenta|Descripcion|tipoRelacion|tipoCuenta| @
			   Seleccion.iniciaObjeto(request.getParameter("radTabla"));
			   // Registros de facultades (7)
			   FacArchivo.iniciaObjeto(';','@',request.getParameter("FacArchivo"));
			   //Seleccion.camposTabla[0][3].equals("5")
		   }
		   
		   if (tipoArchivoExport!=null) {
			   String nombreArchivo = IEnlace.DOWNLOAD_PATH+usuario+".doc";
			   EIGlobal.mensajePorTrace("MCL_Posicion - Exportando archivo: "+nombreArchivo , EIGlobal.NivelLog.INFO);
			   exportarArchivo(tipoArchivoExport, nombreArchivo, response);
		   } 
		   
		   if("5".equals(Seleccion.camposTabla[0][3])){			   
        	   iniciaTablaPosicionTarjeta( FacArchivo,request, response, contrato, usuario, clavePerfil );  
        	   EIGlobal.mensajePorTrace("MCL_Posicion - MANDA LLAMAR EN 5.", EIGlobal.NivelLog.INFO);
           } else{
        	   iniciaTablaPosicionLinea( FacArchivo,Seleccion, request, response, contrato, usuario, clavePerfil );  
        	   EIGlobal.mensajePorTrace("MCL_Posicion - MANDA LLAMAR VALOR DIFERENTE DE 5.", EIGlobal.NivelLog.INFO);
           }             
           
         }
		else
		 {
           request.setAttribute("Servicio no disponible por el momento", IEnlace.MSG_PAG_NO_DISP);

           evalTemplate(IEnlace.ERROR_TMPL, request, response );
		 }
	   EIGlobal.mensajePorTrace("MCL_Posicion - execute(): Saliendo de Posicion Lineas-Tarjetas.", EIGlobal.NivelLog.INFO);
    }

/**
 * **********************************************************************************.
 *
 * @param FacArchivo the fac archivo
 * @param request the request
 * @param response the response
 * @param contrato the contrato
 * @param usuario the usuario
 * @param clavePerfil the clave perfil
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
/*************************************************************************************/
/*************************************************************************************/
    public void iniciaTablaPosicionTarjeta(EI_Tipo FacArchivo,HttpServletRequest request, HttpServletResponse response, String contrato, String usuario, String clavePerfil )
		throws ServletException, IOException
	{

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		String errores      = "";
		String estatusError = "";
		String tipoRelacion	= "";

//		String Trama		= "";
		StringBuffer trama = new StringBuffer("");
		String Result		= "";

		Hashtable htResult  = null;

		boolean existenRegistros=false;

		EI_Tipo Cuentas=new EI_Tipo();
		EI_Tipo Posicion=new EI_Tipo();

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

        EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionTarjeta(): La cuenta es tarjeta de credito. TRAMA CONSULTA <"+request.getParameter("strCuentas")+">", EIGlobal.NivelLog.INFO);

		Cuentas.iniciaObjeto(request.getParameter("strCuentas"));

           for(int i=0;i<Cuentas.totalRegistros;i++)
            {
              if(Cuentas.camposTabla[i][2].trim().equals("Tarjeta"))
               {
                 tipoRelacion=Cuentas.camposTabla[i][1];
                 if(
					 (
					   Cuentas.camposTabla[i][1].trim().equals("P") &&
					    (
					     tieneFacultad(FacArchivo,tipoRelacion,Cuentas.camposTabla[i][0],"CONSPOSICION",request) ||
					     tieneFacultad(FacArchivo,tipoRelacion,Cuentas.camposTabla[i][0],"CONSPOSIPROP",request)
					    )
					  )
					 ||
                      (
					    Cuentas.camposTabla[i][1].trim().equals("T") &&
					     (
					       tieneFacultad(FacArchivo,tipoRelacion,Cuentas.camposTabla[i][0],"CONSPOSICIONT",request) ||
					       tieneFacultad(FacArchivo,tipoRelacion,Cuentas.camposTabla[i][0],"CONSPOSITER",request)
					     )
					  )
					)
                  {
                    existenRegistros=true;

                    //POST---------------
                    String cuenta = Cuentas.camposTabla[i][0];
                    boolean resEva = evaluarCuenta(cuenta,session.getContractNumber());

                    if(!resEva){
                    	  despliegaPaginaError( "Operaci&oacute;n no autorizada", "Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito",
                          		"Consultas &gt; Posici&oacute;n &gt; Tarjeta de Cr&eacute;dito ", "s25130h",request, response );
                    	return;
                    }

                    // POST----------------
                    
                    trama.delete(0, trama.length());
                    
                    trama.append(IEnlace.medioEntrega1).append(PIPE)
                    .append(usuario).append(PIPE)
                    .append("SDCR").append(PIPE)
                    .append(contrato).append(PIPE)
                    .append(usuario).append(PIPE)
                    .append(clavePerfil).append(PIPE)
                    .append(Cuentas.camposTabla[i][0].trim()).append(PIPE)
                    .append(Cuentas.camposTabla[i][1].trim()).append(PIPE)
                    .append("9999");
                    

                    EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionTarjeta(): Trama entrada 1: "+trama.toString(), EIGlobal.NivelLog.DEBUG);

                    try {
				    	htResult = tuxGlobal.web_red( trama.toString() );
				    } catch ( java.rmi.RemoteException re ) {
				    	re.printStackTrace();
				    }catch ( Exception e ){}
                    Result=  ( String ) htResult.get( "BUFFER" );

                    EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionTarjeta(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

					if(Result==null || Result.equals("null"))
					  Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";

                    if(!formateaResult(Result).trim().equals("ERROR"))
					 {
                       Posicion.strCuentas+=Cuentas.camposTabla[i][0]+PIPE;
                       Posicion.strCuentas+=Cuentas.camposTabla[i][3]+PIPE;
                       Posicion.strCuentas+=EnlaceGlobal.fechaHoy("dd/mm/aa")+PIPE;
                       Posicion.strCuentas+=EnlaceGlobal.fechaHoy("th:tm:ts")+PIPE;
                       Posicion.strCuentas+=formateaResult(Result);
                     }
                    else
                     {
                       estatusError+="Cuenta "+Cuentas.camposTabla[i][0]+" "+Result.substring(16,Result.length())+"<br>";
                       //errores+="\nalert('Error en la cuenta: " + Cuentas.camposTabla[i][0]+".\\n"+Result.substring(16,Result.length())+"');";
					   errores += "\ncuadroDialogo('Error en la cuenta: " + Cuentas.camposTabla[i][0]+".\\n"+Result.substring(16,Result.length())+"', 3);";
                     }
                    //TODO CU3061 se realiza operaci�n SDCR
                    /**
                     * VSWF - FVC - I
                     * 15/Enero/2007
                     */
                    if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                    try {
                 	   BitaHelper bh = new BitaHelperImpl(request, session, sess);
         				BitaTransacBean bt = new BitaTransacBean();
         				bt = (BitaTransacBean)bh.llenarBean(bt);
         			if ((request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
         					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_CDCR);
         			   }
         			//VSWF-BMB-I
         			else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals(BitaConstants.EC_POSICION_TARJETA)){
         				bt.setNumBit(BitaConstants.EC_POSICION_TARJETA_ENTRA_CONS_POS_TARJETA);
         			}else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
         					(BitaConstants.EC_SALDO_CUENTA_TARJETA)){
         				bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_TARJETA_CONS_POS_TARJETA);
         			}else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
							(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
						bt.setNumBit("ERCS07");
					}
         			bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
         			bt.setCctaOrig((Cuentas.camposTabla[i][0] == null)?" ":Cuentas.camposTabla[i][0].trim());
         			if(Result!=null){
		    			if(Result.substring(0,2).equals("OK")){
		    				bt.setIdErr("SDCR0000");
		    			}else if(Result.length()>8){
	         			bt.setIdErr(Result.substring(0,8));
	         			}else{
	         				bt.setIdErr(Result.trim());
	         			}
         			}
         			bt.setServTransTux("SDCR");
         			if(Result != null)
         				if(Result.length()>=30)
         					bt.setImporte(Double.parseDouble(Result.substring(16,30).trim()));
         			bt.setNombreArchivo(usuario+".doc");
//         			VSWF-BMB-F
         			BitaHandler.getInstance().insertBitaTransac(bt);
         		} catch(NumberFormatException e){}
                    catch (SQLException e) {
         			e.printStackTrace();
         		} catch (Exception e) {
         			e.printStackTrace();
         		}
                    /**
                     * VSWF - FVC - F
                     */
                  }

                  }
               }
            }

          generaTablaPosicionTarjeta(existenRegistros,Posicion, request, response, usuario, errores, estatusError );
    }

    /**
     * Evalua que la cuenta este dada de alta en la lista grobal de cuentas del contrato.
     * @param cuenta Cuenta a buscar.
     * @param cuentasContrato Cuentasdel contrato.
     * @return True si existe la cuenta en la relacion de cuentas del contrato.
     */
	private boolean evaluarCuenta(String cuenta, EI_Tipo cuentasContrato) {
		boolean resultado =false;
		String cuentaAux ="";
		String[][] cuentas = cuentasContrato.camposTabla;
		EIGlobal.mensajePorTrace("Evaluando la cuenta: "+cuenta +" dentro de un universo de: "+cuentas.length, EIGlobal.NivelLog.INFO);

		for(int i =0;i<cuentas.length;i++){
			cuentaAux = cuentas[i][1];
			EIGlobal.mensajePorTrace("Evaluando la cuentaAux: "+cuentaAux, EIGlobal.NivelLog.INFO);
			if(cuentaAux!=null){
			if(cuentaAux.equals(cuenta)){
				resultado = true;
				break;
			}
			}
		}

		return resultado;
	}
    
    /**
     * Inicia tabla posicion linea.
     *
     * @param cuenta the cuenta
     * @param cuentasContrato the cuentas contrato
     * @return true, if successful
     */
	private boolean evaluarCuentaDir(String cuenta, EI_Tipo cuentasContrato) {
		boolean resultado =false;
		String cuentaAux ="";
		String[][] cuentas = cuentasContrato.camposTabla;
		EIGlobal.mensajePorTrace("Evaluando la cuenta: "+cuenta +" dentro de un universo de: "+cuentas.length, EIGlobal.NivelLog.INFO);

		for(int i =0;i<cuentas.length;i++){
			cuentaAux = cuentas[i][0];

			EIGlobal.mensajePorTrace("Evaluando la cuentaAux: "+cuentaAux, EIGlobal.NivelLog.INFO);
			if(cuentaAux!=null){
				if(cuentaAux.equals(cuenta)){
					resultado = true;
					break;
				}
			}
		}

		return resultado;
	}

/**
 * **********************************************************************************.
 *
 * @param FacArchivo the fac archivo
 * @param Seleccion the seleccion
 * @param request the request
 * @param response the response
 * @param contrato the contrato
 * @param usuario the usuario
 * @param clavePerfil the clave perfil
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
/*********************************************************** tabla Posicion Linea    */
/*************************************************************************************/
    public void iniciaTablaPosicionLinea(EI_Tipo FacArchivo,EI_Tipo Seleccion, HttpServletRequest request, HttpServletResponse response, String contrato, String usuario, String clavePerfil)
			throws ServletException, IOException {

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		String errores      = "";
		String estatusError = "";
		String tipoRelacion = "";
		
		
		StringBuffer trama = new StringBuffer("");

//		String Trama		= "";
		String Result		= "";
	    Hashtable htResult  = null;

		boolean existenRegistros=false;

		EI_Tipo Posicion=new EI_Tipo();

        EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionLinea(): La cuenta es linea de credito.", EIGlobal.NivelLog.INFO);

        tipoRelacion = Seleccion.camposTabla[0][2];
		if(tipoRelacion.trim().equals("P"))
			existenRegistros=true;
        if(tieneFacultadNueva(FacArchivo,Seleccion.camposTabla[0][0].trim(),"TECREPOSICIOC",request) && tipoRelacion.trim().equals("P") )
         {
           
        	EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionLinea(): Si tiene facultad TECREPOSICIOC para"+ Seleccion.camposTabla[0][0].trim() + " y es propia", EIGlobal.NivelLog.INFO);

        	trama.delete(0, trama.length());
        	
           trama.append("");
           trama.append(IEnlace.medioEntrega2).append(PIPE)
           .append(usuario).append(PIPE)
           .append("RG01").append(PIPE)
           .append(contrato).append(PIPE)
           .append(usuario).append(PIPE)
           .append(clavePerfil).append(PIPE)
           .append(Seleccion.camposTabla[0][0].trim().substring(0,2)).append(ARROBA)
           .append(Seleccion.camposTabla[0][0].trim().substring(2,10)).append(ARROBA)
           .append(Seleccion.camposTabla[0][0].trim().substring(10,11)).append(ARROBA);
           
//		   Trama="";
//           Trama+=IEnlace.medioEntrega2  +PIPE;
//           Trama+=usuario                +PIPE;
//           Trama+="RG01"                 +PIPE;
//           Trama+=contrato               +PIPE;
//           Trama+=usuario                +PIPE;
//           Trama+=clavePerfil            +PIPE;
//           Trama+=Seleccion.camposTabla[0][0].trim().substring(0,2)   + "@";
//           Trama+=Seleccion.camposTabla[0][0].trim().substring(2,10)  + "@";
//           Trama+=Seleccion.camposTabla[0][0].trim().substring(10,11) + "@";

           EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionLinea(): Trama entrada 2: "+trama.toString(), EIGlobal.NivelLog.DEBUG);


		   try {
		   	htResult = tuxGlobal.web_red( trama.toString() );
		   } catch ( java.rmi.RemoteException re ) {
		   	re.printStackTrace();
		   }catch ( Exception e ){}
           Result = ( String ) htResult.get( "BUFFER" );

		   if(Result==null || Result.equals("null"))
			 Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";
		   else
			 Result=formateaResultLinea(usuario).trim();

           EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionLinea(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

           if(Result.substring(0,8).equals("RGOS0000"))
             {
               String adeudo=calculaAdeudoTotal(Result);
               String disponible=calculaCapitalDisponible(Result,adeudo);

               Posicion.strCuentas =Result;
               Posicion.strCuentas+=adeudo+PIPE;
               Posicion.strCuentas+=disponible+PIPE;
               Posicion.strCuentas+=Seleccion.camposTabla[0][0]+PIPE;
               Posicion.strCuentas+=Seleccion.camposTabla[0][1]+PIPE;
               Posicion.strCuentas+=Seleccion.camposTabla[0][2]+PIPE;
               Posicion.strCuentas+=EnlaceGlobal.fechaHoy("dd-mm-aaaa")+PIPE;
               Posicion.strCuentas+=EnlaceGlobal.fechaHoy("th:tm:ts")+" @";
             }
           else
             {
               if(Result.equals("OPENFAIL"))
                {
                  estatusError+="Su transacci&oacute;n no puede ser atendida en este momento. Cuenta "+Seleccion.camposTabla[0][0]+"<br>";
                  //errores+="\nalert('Su transacci&oacute;n no puede ser atendida en este momento. \\nCuenta: " + Seleccion.camposTabla[0][0]+"');";
				  errores+="\ncuadroDialogo('Su transacci&oacute;n no puede ser atendida en este momento. \\nCuenta: " + Seleccion.camposTabla[0][0]+"', 3);";
                }
               else
                {
                  estatusError+="Cuenta "+Seleccion.camposTabla[0][0]+" "+Result.substring(16,Result.length())+"<br>";
                  //errores+="\nalert('Error en la cuenta: " + Seleccion.camposTabla[0][0]+".\\n"+Result.substring(16,Result.length())+"');";
				  errores+="\ncuadroDialogo('Error en la cuenta: " + Seleccion.camposTabla[0][0]+".\\n"+Result.substring(16,Result.length())+"', 3);";
                }
             }
           
           EIGlobal.mensajePorTrace("MCL_Posicion - iniciaTablaPosicionLinea(): DATOS PARA TABLA: "+Result, EIGlobal.NivelLog.DEBUG);
           

           generaTablaPosicionLinea(existenRegistros,Posicion, Result, request, response, errores , estatusError);
         } else
         	despliegaPaginaError( "No tiene Facultad para revisar Posici&oacute;n.",
         			"Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito",
         			"Consultas &gt; Posici&oacute;n &gt; L&iacute;nea de Cr&eacute;dito", "s26270h",request, response );
      }


    /**
     * Genera tabla posicion tarjeta.
     *
     * @param existenRegistros the existen registros
     * @param Posicion the posicion
     * @param request the request
     * @param response the response
     * @param usuario the usuario
     * @param errores the errores
     * @param estatusError the estatus error
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void generaTablaPosicionTarjeta(boolean existenRegistros, EI_Tipo Posicion, HttpServletRequest request, HttpServletResponse response, String usuario, String errores, String estatusError )
			throws ServletException, IOException {
    	
    	EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): Inicio Tabla <"+request.getAttribute("Tabla")+">", EIGlobal.NivelLog.DEBUG);
  
      EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): Despues Limpiar Tabla <"+request.getAttribute("Tabla")+">", EIGlobal.NivelLog.DEBUG);
       String strTabla     = "";
       String exportar     = "";
	   String arcLinea     = "";
       String[] titulos    = {"",
                              "Tarjeta",
                              "Descripcion",
                              //"Fecha",
                              //"Hora",
                              "Saldo Corte",
                              "Saldo Deudor",
                              "Pag. M&iacute;nimo",
							  "Pago Fijo",
                              "Fecha L&iacute;mite"};
       //int[] datos={8,0,0,1,2,3,6,5,7,8,8};  Fecha y hora
	   int[] datos = {7,0,0,1,6,5,7,11,8};   // NUMERO DE ELEMENTOS Y CAMPOS A PRESENTAR
	   int[] value = {2,0,1,2};              // VALORES QUE SE ASIGNADOS EN CASO DE UNA TABLA DE CHECKS O RADIOS
	   int[] align = {1,0,2,2,2,2,1};        // ALINEACION DE CADA CAMPO


	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = request.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

/*************************************** Generar Archivo de Exportacion *********/
       Posicion.iniciaObjeto(Posicion.strCuentas);
       if(Posicion.totalRegistros>=1)
        {
          EI_Exportar ArcSal;
          String nombreArchivo=usuario+".doc";
          ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

		  EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): Generando Tabla Posicion.", EIGlobal.NivelLog.INFO);
		  //CSA
		  formateaImporte(Posicion,7);
		  formateaImporte(Posicion,5);
		  formateaImporte(Posicion,6);
          strTabla+=Posicion.generaTabla(titulos,datos,value,align);

          EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): Iniciando la Exportacion.", EIGlobal.NivelLog.INFO);

          if(ArcSal.creaArchivo())
           {
             int c[]={ 0, 1, 2, 3, 6, 5, 7,11, 8};  // Indice del elemento deseado PAGOFIJO
             int b[]={16,40, 8, 5,15,15,15,15,12};  // Longitud maxima del campo para el elemento

             for(int i=0;i<Posicion.totalRegistros;i++)
              {
                arcLinea="";
                for(int j=0; j<9; j++)
                  arcLinea+=ArcSal.formateaCampo(Posicion.camposTabla[i][c[j]],b[j]);
                arcLinea+="\n";
                if(!ArcSal.escribeLinea(arcLinea))
				  EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
              }

             ArcSal.cierraArchivo();
			 // Boton de Exportacion de Archivo.
			 //*************************************************************
			 ArchivoRemoto archR = new ArchivoRemoto();
			 if(archR.copiaLocalARemoto(nombreArchivo,"WEB"))
//			   exportar+="<a href='/Download/"+nombreArchivo+"'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
			 exportar+= "<a id=\"Exportar\"  onclick=\"Exportar();\" href = \"#\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a>";
           }
          else
           {
             //Exportar+="<b>Exportacion no disponible<b>";
             EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionTarjeta(): No se pudo crear archivo de exportacion", EIGlobal.NivelLog.INFO);
           }
/*************************************** Fin de Exportacion de Archivo *********/

		  request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          request.setAttribute("Errores",errores);
		  request.setAttribute("EstatusError",estatusError);
          request.setAttribute("Exportar",exportar);
          request.setAttribute("Tabla",strTabla);
          request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
		  request.setAttribute("newMenu", session.getFuncionesDeMenu());
          request.setAttribute("MenuPrincipal", session.getStrMenu());
		  request.setAttribute("Encabezado", CreaEncabezado("Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito","Consultas &gt; Posici&oacute;n &gt; Tarjeta de Cr&eacute;dito ","s25140h",request));
/////////////////////////////////////////////////////////////////////////////////////////////
  	      HttpSession ses = request.getSession();

		  ses.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  ses.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  ses.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  ses.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          ses.setAttribute("Errores",errores);
		  ses.setAttribute("EstatusError",estatusError);
          ses.setAttribute("Exportar",exportar);
          ses.setAttribute("Tabla",strTabla);
          ses.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
		  ses.setAttribute("newMenu", session.getFuncionesDeMenu());
          ses.setAttribute("MenuPrincipal", session.getStrMenu());
		  ses.setAttribute("Encabezado", CreaEncabezado("Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito","Consultas &gt; Posici&oacute;n &gt; Tarjeta de Cr&eacute;dito ","s25140h",request));

          evalTemplate( "/jsp/MCL_Posicion.jsp", request, response );
        }
       else
        {
		  if(estatusError.trim().equals(""))
			 estatusError="Error en la ejecuci&oacute;n de la consulta.";

          if(existenRegistros)
            despliegaPaginaError( estatusError, "Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito",
            		"Consultas &gt; Posici&oacute;n &gt; Tarjeta de Cr&eacute;dito ", "s25130h",request, response );
          else
            despliegaPaginaError( "No se encontraron Tarjetas de Cr&eacute;dito Asociadas.",
            		"Consultas de Posici&oacute;n de Tarjeta de cr&eacute;dito",
            		"Consultas &gt; Posici&oacute;n &gt; Tarjeta de Cr&eacute;dito","s25130h", request, response );
        }
     }


    /**
     * Genera tabla posicion linea.
     *
     * @param existenRegistros the existen registros
     * @param Posicion the posicion
     * @param Result the result
     * @param request the request
     * @param response the response
     * @param errores the errores
     * @param estatusError the estatus error
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void generaTablaPosicionLinea(boolean existenRegistros,EI_Tipo Posicion, String Result, HttpServletRequest request, HttpServletResponse response, String errores, String estatusError )
		    throws ServletException, IOException {

       String exportar     = "";
       String chequera     = "";

       double IVACon1 = 0;
       double IVACon2 = 0;
       double IVACon3 = 0;
       double IVACon4 = 0;
       double IVACon5 = 0;

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = request.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionLinea(): ERROR" + errores, EIGlobal.NivelLog.INFO);

	   Posicion.iniciaObjeto(Posicion.strCuentas);

	   EIGlobal.mensajePorTrace("MCL_Posicion - generaTablaPosicionLinea(): Se llego con " + Posicion.totalRegistros+" Registros", EIGlobal.NivelLog.INFO);

       if(Posicion.totalRegistros>=1)
        {
          chequera+=Posicion.camposTabla[0][35]+"-";
          chequera+=Posicion.camposTabla[0][36]+"-";
          chequera+=Posicion.camposTabla[0][37];

          request.setAttribute("Credito",Posicion.camposTabla[0][50]);
          request.setAttribute("Fecha",Posicion.camposTabla[0][53]);
          request.setAttribute("Hora",Posicion.camposTabla[0][54]);
          request.setAttribute("Chequera",chequera);
          request.setAttribute("SaldoChq",Posicion.camposTabla[0][29]); //29
          request.setAttribute("Moneda",Posicion.camposTabla[0][32]); //32
          request.setAttribute("FechaUltVen",Posicion.camposTabla[0][4]);
          request.setAttribute("FechaVenAnt",Posicion.camposTabla[0][5]);
          request.setAttribute("FechaAju",Posicion.camposTabla[0][6]);
          request.setAttribute("NuevoLim",Posicion.camposTabla[0][30]);

          // Capital ...
          request.setAttribute("Vigente",Posicion.camposTabla[0][16]);
          request.setAttribute("Vencido",Posicion.camposTabla[0][13]);
          request.setAttribute("ProgDes",Posicion.camposTabla[0][33]);

          //Intereses ...
          request.setAttribute("Ordinario",Posicion.camposTabla[0][17]);
          request.setAttribute("IVAOrd",Posicion.camposTabla[0][18]);
          request.setAttribute("Vencidos",Posicion.camposTabla[0][14]);
          request.setAttribute("IVAVenc",Posicion.camposTabla[0][15]);
          request.setAttribute("Moratorios",Posicion.camposTabla[0][19]);
          request.setAttribute("IVAMor",Posicion.camposTabla[0][20]);

          //Otros Adeudos ...
          request.setAttribute("Seguros",Posicion.camposTabla[0][21]);
          request.setAttribute("PenaConv",Posicion.camposTabla[0][25]);
          request.setAttribute("IVAPena",Posicion.camposTabla[0][26]);
          request.setAttribute("Otros",Posicion.camposTabla[0][27]);
          request.setAttribute("IVAOtros",Posicion.camposTabla[0][28]);
          request.setAttribute("GJuridicos",Posicion.camposTabla[0][24]);

          //Totales ...
          IVACon1 = new Double(Posicion.camposTabla[0][18]).doubleValue();
          IVACon2 = new Double(Posicion.camposTabla[0][15]).doubleValue();
          IVACon3 = new Double(Posicion.camposTabla[0][20]).doubleValue();
          IVACon4 = new Double(Posicion.camposTabla[0][26]).doubleValue();
          IVACon5 = new Double(Posicion.camposTabla[0][28]).doubleValue();

          request.setAttribute("IVACon",Double.toString(IVACon1+IVACon2+IVACon3+IVACon4+IVACon5));
          request.setAttribute("Adeudo",calculaAdeudoTotal(Result));

          /*********************************************************/
		  request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          request.setAttribute("Errores",errores);
          request.setAttribute("EstatusError",estatusError);
          request.setAttribute("Exportar",exportar);
          request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
		  request.setAttribute("Encabezado", CreaEncabezado("Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito","Consultas &gt; Posici&oacute;n &gt; L&iacute;nea de Cr&eacute;dito ","s26280h",request));
		  request.setAttribute("newMenu", session.getFuncionesDeMenu());
          request.setAttribute("MenuPrincipal", session.getStrMenu());
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   	      HttpSession ses = request.getSession();
          ses.setAttribute("Credito",Posicion.camposTabla[0][50]);
          ses.setAttribute("Fecha",Posicion.camposTabla[0][53]);
          ses.setAttribute("Hora",Posicion.camposTabla[0][54]);
          ses.setAttribute("Chequera",chequera);
          ses.setAttribute("SaldoChq",Posicion.camposTabla[0][29]);  //29
          ses.setAttribute("Moneda",Posicion.camposTabla[0][32]);  //34
          ses.setAttribute("FechaUltVen",Posicion.camposTabla[0][4]);
          ses.setAttribute("FechaVenAnt",Posicion.camposTabla[0][5]);
          ses.setAttribute("FechaAju",Posicion.camposTabla[0][6]);
          ses.setAttribute("NuevoLim",Posicion.camposTabla[0][30]);

          // Capital ...
          ses.setAttribute("Vigente",Posicion.camposTabla[0][16]);
          ses.setAttribute("Vencido",Posicion.camposTabla[0][13]);
          ses.setAttribute("ProgDes",Posicion.camposTabla[0][33]);

          //Intereses ...
          ses.setAttribute("Ordinario",Posicion.camposTabla[0][17]);
          ses.setAttribute("IVAOrd",Posicion.camposTabla[0][18]);
          ses.setAttribute("Vencidos",Posicion.camposTabla[0][14]);
          ses.setAttribute("IVAVenc",Posicion.camposTabla[0][15]);
          ses.setAttribute("Moratorios",Posicion.camposTabla[0][19]);
          ses.setAttribute("IVAMor",Posicion.camposTabla[0][20]);

          //Otros Adeudos ...
          ses.setAttribute("Seguros",Posicion.camposTabla[0][21]);
          ses.setAttribute("PenaConv",Posicion.camposTabla[0][25]);
          ses.setAttribute("IVAPena",Posicion.camposTabla[0][26]);
          ses.setAttribute("Otros",Posicion.camposTabla[0][27]);
          ses.setAttribute("IVAOtros",Posicion.camposTabla[0][28]);
          ses.setAttribute("GJuridicos",Posicion.camposTabla[0][24]);


		  ses.setAttribute("IVACon",Double.toString(IVACon1+IVACon2+IVACon3+IVACon4+IVACon5));
          ses.setAttribute("Adeudo",calculaAdeudoTotal(Result));

		  ses.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  ses.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  ses.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  ses.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          ses.setAttribute("Errores",errores);
          ses.setAttribute("EstatusError",estatusError);
          ses.setAttribute("Exportar",exportar);
          ses.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
		  ses.setAttribute("Encabezado", CreaEncabezado("Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito","Consultas &gt; Posici&oacute;n &gt; L&iacute;nea de Cr&eacute;dito ","s26280h",request));
		  ses.setAttribute("newMenu", session.getFuncionesDeMenu());
          ses.setAttribute("MenuPrincipal", session.getStrMenu());

          //TODO CU3061 se realiza operaci�n RG01
          /**
           * VSWF - FVC - I
           * 15/Enero/2007
           */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
          try {
       	   	BitaHelper bh = new BitaHelperImpl(request, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
			if (((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals
					(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION)){
					bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RG01);
			}
			/*BMB-I*/
			else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
					(BitaConstants.EC_POSICION_TARJETA)){
				bt.setNumBit(BitaConstants.EC_POSICION_TARJETA_ENTRA_CONS_POS_LINEA_CREDITO);
			}else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
					(BitaConstants.EC_SALDO_CUENTA_TARJETA)){
				bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_TARJETA_CONS_POS_LINEA_CREDITO);
			}else if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).equals
					(BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO)){
				bt.setNumBit("ERCS08");
			}


			bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
			try{
				bt.setCctaOrig((chequera == null)?" ":chequera);//deber�a ir el N�mero de L�nea de Cr�dito
			}catch (Exception e){
				bt.setCctaOrig(" ");
			}
			if(Result!=null){
    			if(Result.substring(0,2).equals("OK")){
    				bt.setIdErr("RG010000");
    			}else if(Result.length()>8){
				bt.setIdErr(Result.substring(0,8));
				}else{
					bt.setIdErr(Result.trim());
				}
			}
			bt.setServTransTux("RG01");
			/*BMB-F*/
			bt.setImporte(Double.parseDouble((Result == null)?"0":calculaAdeudoTotal(Result)));
			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch(NumberFormatException e){}
          catch (SQLException e) {
        	  EIGlobal.mensajePorTrace(">>MCL_Posicion -> generaTablaPosicionLinea -> Inserta Bitacoras --> " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(">>MCL_Posicion -> generaTablaPosicionLinea --> " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		}
          }
          /**
           * VSWF - FVC - F
           */
       	  evalTemplate("/jsp/MCL_PosicionLinea.jsp", request, response );
        }
       else
        {
		  boolean facultadPOS=false;
		  if( session.getFacultad("TECREPOSICIOC") )
			 facultadPOS=true;

		  EIGlobal.mensajePorTrace("MCL_Posicion - No hay registros y facultad es " +facultadPOS, EIGlobal.NivelLog.INFO);

		  if(estatusError.trim().equals("") )
			{
			  if((existenRegistros && facultadPOS) || !facultadPOS)
				 estatusError="No tiene facultad.";
			}
		   else
			   estatusError="No se puedo realizar la consulta.";


          if(existenRegistros)
            despliegaPaginaError(estatusError,"Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito",
            		"Consultas &gt; Posici&oacute;n &gt; L&iacute;nea de Cr&eacute;dito ", "s26270h", request, response );
          else
		despliegaPaginaError("No se encontraron L&iacute;neas de Cr&eacute;dito Asociadas.",
			                 "Consultas de Posici&oacute;n de L&iacute;nea de Cr&eacute;dito",
			                 "Consultas &gt; Posici&oacute;n &gt; L&iacute;nea de Cr&eacute;dito ", "s26270h", request, response );
        }
     }


    /**
     * Formatea result.
     *
     * @param Result the result
     * @return the string
     */
    String formateaResult(String Result)
     {
	   StringBuffer formateado = new StringBuffer("");
	   String tipoPago = "";
       double caut = 0;
       double atot = 0;
       double cdis = 0;

       if(Result.length()>=70)
        if(Result.substring(0,7).trim().equals("OK"))
         {
		   try{
				tipoPago = Result.substring( 87, 89).trim();  // OBTIENE EL TIPO DE PAGO PARA PRESENTAR VALORES EN COLUMNAS
		   }catch(Exception e){}
           formateado.append(Result.substring( 8,16).trim()); // REFERENCIA
           formateado.append(PIPE);
           formateado.append(Result.substring(16,30).trim()); // SALDO ACTUAL
           formateado.append(PIPE);
           formateado.append(Result.substring(30,44).trim()); // SALDO AL CORTE
		   formateado.append(PIPE);
		   if(!tipoPago.equals("02"))
		   {
	           formateado.append(Result.substring(44,58).trim()); // PAGO MINIMO
		   }
		   else
		   {
			   formateado.append(" ");
		   }
		   formateado.append(PIPE);
           formateado.append(Result.substring(58,69).trim()); // FECHA LIMITE DE PAGO
		   formateado.append(PIPE);
           formateado.append(Result.substring(69,83).trim()); // LIMITE DE CREDITO
		   formateado.append(PIPE);
           caut=new Double(Result.substring(69,83).trim()).doubleValue(); // LIMITE DE CREDITO
           atot=new Double(Result.substring(16,30).trim()).doubleValue(); // SALDO ACTUAL
           cdis=caut-atot;  // CREDITO DISPONIBLE = LIMITE DE CREDITO - SALDO ACTUAL
           formateado.append(Double.toString(cdis)); // CREDITO DISPONIBLE
		   formateado.append(PIPE);
		   if (tipoPago.equals("02"))
		   {
				formateado.append(Result.substring(103,117).trim()); // CARGO MENSUAL
		   }
		   else
		   {
				formateado.append(" ");
		   }
		   formateado.append("@");

           return formateado.toString();
         }
       return "ERROR";
     }


	/**
	 * Calcula adeudo total.
	 *
	 * @param Result the result
	 * @return the string
	 */
	String calculaAdeudoTotal(String Result)
		{
		long atotal = 0;
		EI_Tipo ResultTmp=new EI_Tipo(this);

		ResultTmp.iniciaObjeto(Result+"@");

		long capitalExigible      = cantidadX10000(ResultTmp.camposTabla[0][13]);
		long interesExigible      = cantidadX10000(ResultTmp.camposTabla[0][14]);
		long ivaInteresExigible   = cantidadX10000(ResultTmp.camposTabla[0][15]);

		long capitalNoExigible    = cantidadX10000(ResultTmp.camposTabla[0][16]);
		long interesNoExigible    = cantidadX10000(ResultTmp.camposTabla[0][17]);
		long ivaInteresNoExigible = cantidadX10000(ResultTmp.camposTabla[0][18]);

		long interesMoratorio     = cantidadX10000(ResultTmp.camposTabla[0][19]);
		long ivaInteresMoratorio  = cantidadX10000(ResultTmp.camposTabla[0][20]);
		long seguro               = cantidadX10000(ResultTmp.camposTabla[0][21]);

		long gastosJuridicos      = cantidadX10000(ResultTmp.camposTabla[0][24]);
		long penaConvencional     = cantidadX10000(ResultTmp.camposTabla[0][25]);
		long ivaPenaConvencional  = cantidadX10000(ResultTmp.camposTabla[0][26]);

		long otros                = cantidadX10000(ResultTmp.camposTabla[0][27]);
		long ivaOtros             = cantidadX10000(ResultTmp.camposTabla[0][28]);

		// C�lculo
		atotal=capitalExigible + interesExigible + ivaInteresExigible;
		atotal+=capitalNoExigible + interesNoExigible + ivaInteresNoExigible;
		atotal+=interesMoratorio + ivaInteresMoratorio + seguro + gastosJuridicos;
		atotal+=penaConvencional + ivaPenaConvencional + otros + ivaOtros;

		// Redondeo
		atotal += ((atotal % 100) > 50)?100:0;

		// Formateo
		String strTotal = "" + atotal;
		while(strTotal.length()<5) strTotal = "0" + strTotal;
		strTotal = strTotal.substring(0,strTotal.length()-4) + "." + strTotal.substring(strTotal.length()-4,strTotal.length()-2);

		return strTotal;
		}

	// ------------------------------
	/**
	 * Cantidad x10000.
	 *
	 * @param cantidad the cantidad
	 * @return the long
	 */
	long cantidadX10000(String cantidad)
		{
		int posPunto;

		if ((posPunto = cantidad.indexOf(".")) == -1) {
			posPunto = cantidad.length();
			cantidad += ".0000";
		}
		if(posPunto + 5 > cantidad.length()) cantidad += "0000";
		cantidad = cantidad.substring(0,posPunto) + cantidad.substring(posPunto+1,posPunto+5);
		return Long.parseLong(cantidad);
		}



    /**
     * Calcula capital disponible.
     *
     * @param Result the result
     * @param atotal the atotal
     * @return the string
     */
    String calculaCapitalDisponible(String Result, String atotal)
     {
       double cdisponible = 0;
       double adeudoTotal = new Double(atotal).doubleValue();

       EI_Tipo ResultTmp = new EI_Tipo(this);

       Result+=" @";
	   ResultTmp.iniciaObjeto(Result);

       double capitalExigible   = new Double(ResultTmp.camposTabla[0][13]).doubleValue();
       double capitalNoExigible = new Double(ResultTmp.camposTabla[0][16]).doubleValue();
       double capitalAutorizado = new Double(ResultTmp.camposTabla[0][40]).doubleValue();

       if(adeudoTotal>capitalAutorizado || capitalExigible>0)
         cdisponible=0;
       else
         cdisponible=capitalAutorizado-capitalNoExigible;

       return Double.toString(cdisponible);
     }


    /**
     * Formatea result linea.
     *
     * @param usuario the usuario
     * @return the string
     */
    String formateaResultLinea(String usuario)
     {
       String nombreArchivo = IEnlace.LOCAL_TMP_DIR+"/"+usuario;
       String arcLinea      = "";

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR  = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 EIGlobal.mensajePorTrace("MCL_Posicion - formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
        {
          boolean noError=true;
          arcLinea=ArcEnt.leeLinea();
          if(arcLinea.substring(0,2).equals("OK"))
           {
             arcLinea=ArcEnt.leeLinea();
             if(arcLinea.equals("ERROR"))
               return "OPENFAIL";
           }
		  ArcEnt.cierraArchivo();
        }
       else
        return "OPENFAIL";
       return arcLinea;

       //############ Cambiar ....
    }


   /**
    * Tiene facultad.
    *
    * @param FacArchivo the fac archivo
    * @param tipoRelacion the tipo relacion
    * @param credito the credito
    * @param facultad the facultad
    * @param request the request
    * @return true, if successful
    */
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if(tipoRelacion.trim().equals("P"))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    /**
     * Facultad asociada.
     *
     * @param FacArchivo the fac archivo
     * @param credito the credito
     * @param facultad the facultad
     * @param signo the signo
     * @return true, if successful
     */
    boolean facultadAsociada(EI_Tipo FacArchivo,String credito,String facultad,String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
			  {
			    EIGlobal.mensajePorTrace("MCL_Posicion - facultadAsociada(): Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
                return true;
			  }
		 }
       EIGlobal.mensajePorTrace("MCL_Posicion - facultadAsociada(): Facultad asociada regresa false: Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
       return false;
     }


   /**
    * Tiene facultad nueva.
    *
    * @param FacArchivo the fac archivo
    * @param credito the credito
    * @param facultad the facultad
    * @param request the request
    * @return true, if successful
    */
   boolean tieneFacultadNueva(EI_Tipo FacArchivo,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
		 else
		   FacultadAsociadaACuenta=false;
       }
	  EIGlobal.mensajePorTrace("MCL_Posicion - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

	/**
    * Formatea importe.
    *
    * @param Tipo the tipo
    * @param ind the ind
    */
   public  void formateaImporte( EI_Tipo Tipo, int ind )
	  {
		 for(int i=0;i<Tipo.totalRegistros;i++)
		 {
			if(! (Tipo.camposTabla[i][ind].trim().equals("")) )
			{
				// importe=new Float(Tipo.camposTabla[i][ind]).floatValue();
				// Tipo.camposTabla[i][ind]=FormatoMoneda(Float.toString(importe));
				 Tipo.camposTabla[i][ind]=FormatoMoneda(Tipo.camposTabla[i][ind]);
			}
		  }
	   }


	/**
	 * Formato moneda.
	 * 
	 * @param cantidad La cantidad a la que se le aplicara el formato
	 * @return formato Cadena con el formato de moneda
	 */
	public String FormatoMoneda(String cantidad)
	{
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		if(cantidad ==null ||cantidad.equals(""))
			cantidad="0.0";

		double importeTemp = 0.0;
		importeTemp = new Double(cantidad).doubleValue();
		if (importeTemp < 0)
		{
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("(MX"))
		    			 formato ="$ -"+ formato.substring (4,formato.length () - 1);
		    		 else
		    			 formato ="$ -"+ formato.substring (4,formato.length ());
			} catch(NumberFormatException e) {formato="$0.00";}
		} else {
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
		    		 if (formato.substring (0,3).equals ("MXN"))
		    			 formato ="$ "+ formato.substring (3,formato.length ());
		    		 else
		    			 formato ="$ "+ formato.substring (1,formato.length ());
			} catch(NumberFormatException e) {
				formato="$0.00";}
		} // del else
		if(formato==null)
			formato = "";
		return formato;
	}
	
	
	/**
	 * Exportar archivo.
	 *
	 * @param tipoArchivo El tipo de archivo a exportar
	 * @param nombreArchivo La ruta del archivo de donde se obtienen los datos
	 * @param res El request de la pagina
	 */
	private void exportarArchivo(String tipoArchivo, String nombreArchivo, HttpServletResponse res){
		EIGlobal.mensajePorTrace(">>Export MCL_Posicion -> ARCHIVO DE ENTRADA  -->"+nombreArchivo+"<--",EIGlobal.NivelLog.DEBUG);
		   ExportModel em = new ExportModel("cSaldoPosicion", '|', true)
		   .addColumn(new Column(0,15, "Tarjeta"))//16
		   .addColumn(new Column(16,55, "Descripcion"))//40
		   .addColumn(new Column(56,63, "Fecha"))//8
		   .addColumn(new Column(64,68, "Hora"))//5
		   .addColumn(new Column(69,83, "Saldo Corte"))//15
		   .addColumn(new Column(84,98, "Saldo Deudor"))//15
		   .addColumn(new Column(99,113,"Pag. Minimo"))//15
		   .addColumn(new Column(114,128,"Pago Fijo"))//15
		   .addColumn(new Column(129,140,"Fecha Limite"));	//12

		   try{
			   EIGlobal.mensajePorTrace(">>Export MCL_Posicion -> Generando Exportacion --> ",EIGlobal.NivelLog.DEBUG);
	
			   HtmlTableExporter.export(tipoArchivo, em, nombreArchivo, res);
			   
		   }catch(IOException ex){
			   EIGlobal.mensajePorTrace(">>Export MCL_Posicion -> ERROR --> Exportar Archivo -> " + ex.getMessage(),EIGlobal.NivelLog.ERROR);
		   }
	}
	
	/**
	 * Metodo que se encarga de validar si la cuenta esta relacionada al contrato.
	 * @param cuenta Cuenta a validar
	 * @param contrato Contrato a validar
	 * @return Regresa true si la cuenta pertenece al contrato.
	 */
	private boolean evaluarCuenta(String cuenta, String contrato) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer("");
        boolean resultado = false;

        if((cuenta==null)||("".equals(cuenta.trim()))){
          return true;
        }


        try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query.append("Select num_cuenta From nucl_relac_ctas Where num_cuenta2 ='");
			query.append(contrato);
			query.append("' And num_cuenta = '");
			query.append(cuenta);
			query.append("' ");

			EIGlobal.mensajePorTrace ("Validando la cuenta:: -> Query:" + query.toString(), EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query.toString());

			if(rs.next()){
				resultado=true;
			}

			EIGlobal.mensajePorTrace ("La cuenta existe:"+resultado, EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace (" Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace("SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}
}
/*2005.1*/