/** 
*   Isban Mexico
*   Clase: ConsultaBitacora.java
*   Descripcion:Servlet que obtiene y despliega los movimientos de la bitacora.
*   
*   Control de Cambios:
*   1.0 19/04/2001 Jorge Barbosa - modificaci�n de archivos filtro 
*   
*   1.1 22/06/2016  FSW. Everis - Implementacion permite la gestion de transferencia en dolares(Spid). 
*/

package mx.altec.enlace.servlets;
import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.SUAGenCompPDF;
import mx.altec.enlace.bo.SUAConsultaPagoAction;
import mx.altec.enlace.bo.RET_CuentaAbonoVO;
import mx.altec.enlace.bo.RET_OperacionVO;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.sql.*;			//HELG-Getronics
import java.text.DecimalFormat;	//HELG-Getronics
import java.text.NumberFormat;


/**
 * Class ConsultaBitacora.
 */
public class ConsultaBitacora extends BaseServlet{

	/**  Clave para pago Referenciado por micrositio. */
	private static final String ES_PAGO_SATMICROSITIO_MANC = "PMRF";

	/**  Clave para pago SUA LC. */
	private static final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";

	/**  Clave MS en mancomunidad. */
	private static final String ES_PAGO_MICROSITIO_MANC = "PMIC";

	/**  Constante para el valor session. */
	private static final String SESSION = "session";

	/**  Constante para el valor Fecha. */
	private static final String FECHA = "Fecha";

	/**  Constante para el valor FechaaTrabajar. */
	private static final String FECHA_TRABAJAR = "FechaaTrabajar";

	/**  Constante para el valor Detalle. */
	private static final String DETALLE = "Detalle";

	/**  Constante para el valor MenuPrincipal. */
	private static final String MENU_PRINCIPAL = "MenuPrincipal";

	/**  Constante para el valor MsgError. */
	private static final String MSG_ERROR = "MsgError";

	/**  Constante para el valor newMenu. */
	private static final String NEW_MENU = "newMenu";

	/**  Constante para el valor Consulta de Bit&aacute;cora. */
	private static final String CONSULTA_DE_BITACORA = "Consulta de Bit&aacute;cora";

	/**  Constante para el valor Consultas &gt; Bit&aacute;cora. */
	private static final String CONSULTAS_BITACORA = "Consultas &gt; Bit&aacute;cora";

	/**  Constante para el valor Encabezado. */
	private static final String ENCABEZADO = "Encabezado";

	/**  Constante para el valor s25320h. */
	private static final String S25320H = "s25320h";

	/**  Constante para el valor Mensaje. */
	private static final String MENSAJE = "Mensaje";
	/** Constante para el valor /jsp/EI_Mensaje2.jsp */
	private static final String EI_MENSAJEDOS = "/jsp/EI_Mensaje2.jsp";

	/**  Encabezado bitacora historica. */
	private static final String ENC_BIT_HIS = "Consulta de Bit&aacute;cora hist&oacute;rica";

	/**  Contrato. */
	private static final String CONTRATO = "contrato";

	/**  Substring ceros. */
	private static final String SUB_CEROS = "0000";

	/** La constante VACIO. */
	private static final String VACIO = "";

	/** La constante SIGNO_PESOS $. */
	private static final String SIGNO_PESOS = "$";

	/** La constante COMA ','. */
	private static final String COMA = ",";

	/** La constante SPACE ' '. */
	private static final String SPACE = " ";


	/**  Instancia para objeto con que convierte usuario de 8 a 7 pos. */
	private Convertidor con = new Convertidor();
	//=============================================================================================
	/**
	 * doGet.
	 *
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

    /**
     * doPost.
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws ServletException excepcion a manejar
     * @throws IOException excepcion a manejar
     */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * defaultAction.
	 *
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &Inicia clase&", EIGlobal.NivelLog.DEBUG);
		boolean sesionValida = SesionValida( req, res);

		if(sesionValida) {
			HttpSession sess = req.getSession();
			  BaseResource session = (BaseResource) sess.getAttribute(SESSION);

			String mMenup = "";
			//if ( sesionValida && session.getFacCBitacora().trim().length()!=0 ) {

			if (req.getParameter(FECHA)!=null){						//HELG-Getronics
				req.setAttribute( FECHA_TRABAJAR, req.getParameter(FECHA));	//HELG-Getronics
			}

			if (session.getFacultad(session.FAC_CONSULTA_BITACORA)) {

				if (req.getParameter("opcionLC") == null) {
					mMenup = session.getstrMenu();
					if(mMenup==null){
						mMenup = "";
					}

					req.setAttribute( MENU_PRINCIPAL, mMenup );
					req.setAttribute( "ContUser",      ObtenContUser(req) );
					req.setAttribute( FECHA,         ObtenFecha() );


		//***************** Inicia codigo HELG-Getronics
					if(req.getParameter(DETALLE)!=null && "3".equals(req.getParameter(DETALLE))){
						try{
							detalle_reutersfx(req, res);
						}catch(Exception e){
							EIGlobal.mensajePorTrace ("ConsultaBitacora::defaultAction:: -> Fallo al traer detalle reuters", EIGlobal.NivelLog.DEBUG);
						}
					}else if(req.getParameter(DETALLE)!=null && "2".equals(req.getParameter(DETALLE)))
					{

						detalle_internacional( req, res );
					}
					else if(req.getParameter(DETALLE)!=null && ("1".equals(req.getParameter(DETALLE)))){
						detalle_divisa( req, res );
					}
					else{
		//***************** Termina codigo HELG-Getronics

					inicio( req, res ); // m�todo principal
		//***************** Inicia codigo DAG-Getronics
					}
		//***************** Termina codigo DAG-Getronics

				} else {
					generaComprobanteSUALC(req, res);
				}



			}else {
				req.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
				req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
				req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
				req.setAttribute(MENSAJE, "No cuenta con facultades para Consulta de Bit&aacute;cora.");
				evalTemplate( EI_MENSAJEDOS, req, res);
			}
		}
		else {
			req.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}

		EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &Termina clase&", EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * generaComprobanteSUALC.
	 *
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	private void generaComprobanteSUALC ( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);


		EIGlobal.mensajePorTrace("Consulta Bitacora - generaComprobanteSUALC", EIGlobal.NivelLog.DEBUG);
		SUAConsultaPagoAction action = new SUAConsultaPagoAction();
		String folioSUA = req.getParameter("folioSUA");
		String lc = req.getParameter("lc");
		/*
		String fechaSUA =req.getParameter("fechaSUA");
		String horaSUA  =req.getParameter("horaSUA");
		String importeSUA  =req.getParameter("importeSUA");
		SUALCORM4 beanDao = action.anterioresSUALC("", folioSUA, fechaSUA, fechaSUA, "", datos.get(0).getLineaCaptura(), 2);
		  */

		EIGlobal.mensajePorTrace("VAO Consulta Bitacora -> "+lc+" --"+folioSUA, EIGlobal.NivelLog.DEBUG);
		//SUALCORM4 anterioresSUALC(ctaCargo,  folioSua,  fechIni,   fechFin,  regPatronal,  lc,int opcion){
		SUALCORM4 beanDao = action.anterioresSUALC("", folioSUA, "", "", "", lc, 2);

		if (!"Operacion Exitosa".equals(beanDao.getCodErr())){
			EIGlobal.mensajePorTrace("VAO Consulta sin LC -> ", EIGlobal.NivelLog.DEBUG);
			beanDao = action.anterioresSUALC("", folioSUA, "", "", "", "", 2); // VAO por si no funciona la consulta por lc
		}

		if (beanDao != null){
			List lista = beanDao.getConsulta();
			if ("Operacion Exitosa".equals(beanDao.getCodErr())) {
				if (lista.size() > 0) {
					SUALCORM4 detalle = (SUALCORM4) lista.get(0);
					double importeTotal;
					try{

						importeTotal=Double.valueOf(detalle.getImporteTotal());
						importeTotal=(importeTotal/100);//por los decimales
					}catch(NumberFormatException exception){
						importeTotal=0;
					}
					String periodopagMes=null;
					String periodopagAnio=null;
					if(detalle.getPeriodoPago()!=null && detalle.getPeriodoPago().length()>0){
						try{
							periodopagAnio=detalle.getPeriodoPago().substring(0,4);
							periodopagMes=detalle.getPeriodoPago().substring(4,detalle.getPeriodoPago().length());
						}catch(IndexOutOfBoundsException ibf){
							periodopagAnio="";
							periodopagMes="";
						}
					}else{
						periodopagAnio="";
						periodopagMes="";
					}
					SUAGenCompPDF comp;
					comp = new SUAGenCompPDF(
						session.getContractNumber(),
						session.getNombreContrato(),
					session.getUserID(),
					session.getNombreUsuario(),
					detalle.getFolioSua(),
					detalle.getCuentaCargo(),
					"",
					detalle.getRegPatronal(),
					periodopagMes,
					periodopagAnio,
					detalle.getLinSua(),
					importeTotal,
					detalle.getNumeroMovimiento(),
					detalle.getTimestamp().substring(0, 10),
					detalle.getTimestamp().substring(11, 19).replace('.', ':'),
					"INTERNET",
					"EXITO"
					,detalle.getMsjErr());

					pantCompSUA(req, res, comp.generaComprobanteBAOS());
				} else {
					req.setAttribute( MENU_PRINCIPAL, "");
					String encabezado = ENC_BIT_HIS;
					req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,CONSULTAS_BITACORA,S25320H,req) );
					req.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
					evalTemplate( IEnlace.ERROR_TMPL, req, res );
				}
			}else {
				req.setAttribute( MENU_PRINCIPAL, "");
				String encabezado = ENC_BIT_HIS;
				req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,CONSULTAS_BITACORA,S25320H,req) );
				req.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate( IEnlace.ERROR_TMPL, req, res );
			}
		}else {
				req.setAttribute( MENU_PRINCIPAL, "");
				String encabezado = ENC_BIT_HIS;
				req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,CONSULTAS_BITACORA,S25320H,req) );
				req.setAttribute(MSG_ERROR, IEnlace.MSG_PAG_NO_DISP);
				evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}
	}

	/**
	 * Generacion del comprobante PDF.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @param archivo El objeto: archivo
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	private void  pantCompSUA(HttpServletRequest req, HttpServletResponse res, ByteArrayOutputStream archivo) throws ServletException, IOException
	{

		//StringBuffer codigo=new StringBuffer("");
		//codigo.append("<FRAME SRC=\"../../Download/" + archivo + "\"");
		//codigo.append("NAME=\"comprobante\"  SCROLLING=\"SI\"   marginheight=\"0\" marginwidth=\"0\">");
		//req.setAttribute("codigo",codigo.toString());
		res.setHeader("Expires", "0");
		res.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		res.setHeader("Pragma", "public");
		// setting the content type
		res.setContentType("application/pdf");
		// the contentlength is needed for MSIE!!!
		res.setContentLength(archivo.size());
		// write ByteArrayOutputStream to the ServletOutputStream
		ServletOutputStream out = res.getOutputStream();
		archivo.writeTo(out);
		out.flush();
		out.close();

	}

	/**
	 * Metodo de inicio.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto int
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public int inicio( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &inicia m�todo inicio()&", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		String mMenu = "";

		req.setAttribute( FECHA, ObtenFecha() );

		mMenu = session.getstrMenu();
		if (mMenu==null){
			mMenu = "";
		}
		req.setAttribute( MENU_PRINCIPAL,	mMenu );

		String archivoServicio	= "";
		String archivoServicioOrdenado = "";
		String archivoFiltro	= IEnlace.LOCAL_TMP_DIR + "/"+ session.getUserID8() + ".bit"; //Se elimina ruta HardCode /tmp
		String salida			= "";
		int llamadaServicio	= 0;
		int totalRegistros		= 0;
		int parametros			= 0;
		int exportar			= 0;
		int filtro				= 0;

		String nombreArchivo	= "";
		String archivoExportar	= "";
		String tipoArchivoExport	= req.getParameter("tipoArchivoExport");
		String SEMAFORO[]		= new String[12];
		String FECHAFIN			= "";
		String FECHAINI			= "";
		String contrato			= "";
		String NOMCONTRATO			= "";

		String MISPARAMETROS	= "";
		String [] REFERENCIA	= new String [1];
		String importeDe		= "";
		String importeA			= "";
		String USUARIO			= "";
		String USUARIO1_Selec	= "";
		String CUENTA			= "";
		String [] ERROR			= new String [1];
		String PREV				= "";
		String NEXT				= "";
		//String TOTAL			= "";
		boolean TRANSFERENCIAS	= false;
		boolean INVERSIONES		= false;
		boolean INTERBANCARIAS	= false;
		boolean LINCAPSUA 		= false;
		boolean ACEPTADAS		= false;
		boolean RECHAZADAS		= false;
		boolean PROGRAMADAS		= false;
		boolean PENDIENTES		= false;
		boolean [] regreso		= new boolean [8];
		//String linea = "";
		//String [] contenidoArch = null;
		String [] lineatmp = null;
		//String contenidoFinal = "";

		PREV  = req.getParameter( "prev" );
		NEXT  = req.getParameter( "next" );
		//TOTAL = ( String) req.getParameter( "total" );
		for ( int i = 0 ; i < 12  ; i++ ) {
			SEMAFORO[i] = "0"; // 0 0 0 0 0000000
		}

		// log( "Inicializando m�scara de filtros : "+SEMAFORO[0]+SEMAFORO[1]+SEMAFORO[2]+SEMAFORO[3]+SEMAFORO[4]+SEMAFORO[5]+SEMAFORO[6]+SEMAFORO[7]+SEMAFORO[8]+SEMAFORO[9]+SEMAFORO[10] );

		ERROR[0] = "No hay registros asociados a su consulta";

		// para la primer ventana
		if(tipoArchivoExport!=null&&!("".equals(tipoArchivoExport))){
			exportarBitacora(req, res);
		}else if( PREV == null && NEXT == null ){
			String hoy		= ( String) req.getParameter( "deldia" );

			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &primera ventana ...&", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &obteniendo par�metros del ambiente ...&", EIGlobal.NivelLog.DEBUG);

			CUENTA		  = ( String) req.getParameter( "cuenta" );
			USUARIO1_Selec= ( String) req.getParameter( "usuario" );
			contrato	  = ( String) req.getParameter( CONTRATO );
			importeDe	= ( String) req.getParameter( "ImporteDe" );
			importeA	= ( String) req.getParameter( "ImporteA" );
			REFERENCIA [0]	= ( String) req.getParameter( "Refe" );
			FECHAINI  	  = ( String) req.getParameter( "fecha1" );
			FECHAFIN	  = ( String) req.getParameter( "fecha2" );
			try{
			String el_ = (String) req.getAttribute(CONTRATO);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &obteniendo par�metros del ambiente ...&"+ e, EIGlobal.NivelLog.DEBUG);
			}

			//System.otu.println();
			//req.setAttribute("contrato", CONTRATO);

			if ( USUARIO1_Selec.trim().length() > 1 ){
				SEMAFORO[0]	= "1";
			}else{
				USUARIO1_Selec="0";
			}

			if ( CUENTA.length() > 1 ){
				SEMAFORO[1]	= "1";
			}
			if ( importeDe.length() > 0 ){
				SEMAFORO[2]	= "1";
			}
			if ( REFERENCIA[0].length() >= 1 ){
				SEMAFORO[3]	= "1";
			}

			// POST -------------------
			if(!evaluarUsuario(USUARIO1_Selec,sess)|(!evaluarCuenta(CUENTA,session.getContractNumber()))){

                        EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &erroren la consulta&", EIGlobal.NivelLog.DEBUG);
						req.setAttribute( "Output1","<h3>Operaci&oacute;n no autorizada.</h3>");
						req.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
						req.setAttribute( MENU_PRINCIPAL, session.getStrMenu());
						req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
						req.setAttribute( MENSAJE, "Error inseperado. Operaci&oacute;n no autorizada." );
						evalTemplate( EI_MENSAJEDOS, req, res);
			}
			
			// POST -------------------------------
			
			//if ( USUARIO.length() < 2 ) USUARIO	= session.getUserID8();
			USUARIO	= session.getUserID8();
			if ( contrato==null )
			 {
				contrato	    = session.getContractNumber();
			  // JVL 04/01/2006 Obtengo la Descripcion del contrato ya que se utilizara en el JSP (CompNvoPILC.jsp)
                          NOMCONTRATO   = session.getNombreContrato();
			  // JVL 04/01/2006 fin modificacion
			 }
			nombreArchivo   = contrato + USUARIO + ".bit.doc";
			archivoExportar = IEnlace.DOWNLOAD_PATH + nombreArchivo;
			// filtros de la consulta
			if(( String) req.getParameter("ChkTranf")!= null)         {	TRANSFERENCIAS	=true; SEMAFORO[4] ="1"; }
			if(( String) req.getParameter("ChkInversiones") != null)  {	INVERSIONES		=true; SEMAFORO[5] ="1"; }
			if(( String) req.getParameter("ChkInterbancario") != null){ INTERBANCARIAS	=true; SEMAFORO[6] ="1"; }
			if(( String) req.getParameter("ChkSUA") != null)		  { LINCAPSUA		=true; SEMAFORO[7] ="1"; }
			if(( String) req.getParameter("ChkAceptadas") != null)	  {	ACEPTADAS		=true; SEMAFORO[8] ="1"; }
			if(( String) req.getParameter("ChkRechazadas") != null)   {	RECHAZADAS		=true; SEMAFORO[9] ="1"; }
			if(( String) req.getParameter("ChkProgramadas") != null)  {	PROGRAMADAS		=true; SEMAFORO[10] ="1"; }
			if(( String) req.getParameter("ChkPendientes") != null)   {	PENDIENTES		=true; SEMAFORO[11]="1"; }

			session.setfechaini( FECHAINI );
			session.setfechafin( FECHAFIN );
			//aqui
			llamadaServicio = actualizaBitacora(USUARIO,contrato, req, res);  // llamada al servicio e inicializaci�n de archivo

			// Error en el servicio
			if ( llamadaServicio != 0 ){
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &error en el servicio&", EIGlobal.NivelLog.ERROR);
				req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
				req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
				req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
				req.setAttribute(MENSAJE, "Por el momento no es posible atender su consulta, intente m&aacute;s tarde!");
				evalTemplate( EI_MENSAJEDOS,req,res);
				return 1;
			}else{
				// servicio OK

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &servicio OK ...&", EIGlobal.NivelLog.DEBUG);

				archivoServicio = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();
				archivoServicioOrdenado = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8()+ "Ord";

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class: " + archivoServicioOrdenado, EIGlobal.NivelLog.DEBUG);

				// n�mero de lineas del archivo
				totalRegistros = totalRegistros(ERROR, archivoServicio );
				String  contenidoArch[] = new String [totalRegistros];
				String  contenidoArchtmp[] = new String [totalRegistros];
				File archivo= new File(archivoServicio);

/*				BufferedReader archivoTrabajo		= new BufferedReader( new FileReader( archivoServicio ) );
				BufferedWriter archivoTrabajotmp	= new BufferedWriter( new FileWriter( archivoServicioOrdenado ) );
					//archivoTrabajo = new RandomAccessFile(archivoServicio,"rw");


				for( int i = 0; i < totalRegistros; i++ ){
					linea = archivoTrabajo.readLine();
					if( linea.trim().equals(""))
						linea = archivoTrabajo.readLine();
					if( linea == null )
						break;
					contenidoArch[i]  = linea + "";
				//	contenidoArch  = desentrama( linea , ';' );
					}
					archivoTrabajo.close();
					archivo.delete();


					//contenidoArchtmp = bubbleSort(contenidoArch);
					Arrays.sort(contenidoArch);

					for(int i = 0; i < contenidoArch.length; i++ ){
					contenidoFinal = contenidoArch [i] + "\r\n";
					archivoTrabajotmp.write( contenidoFinal );
					}
					archivoTrabajotmp.close();


*/

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &total de registros: "+totalRegistros+"&", EIGlobal.NivelLog.INFO);

				session.settotal( new Integer( totalRegistros ).toString() ); // se guardan en el ambiente

				// existen registros
				if( totalRegistros >0 ){


					EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &filtrando archivo ...&", EIGlobal.NivelLog.DEBUG);
					//aqui
					filtro = filtrarArchivo(INTERBANCARIAS,CUENTA,USUARIO1_Selec,importeDe,importeA,REFERENCIA,SEMAFORO,archivoServicio, archivoFiltro, totalRegistros );


					if(filtro == 0)// filtro del archivo OK
					{
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &archivo filtro OK ...&", EIGlobal.NivelLog.DEBUG);

						session.setArchivoBit( archivoFiltro ); // se guarda en el ambiente

						totalRegistros = totalRegistros(ERROR, session.getArchivoBit() );
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &registros filtrados: "+totalRegistros+"&", EIGlobal.NivelLog.DEBUG);

						session.settotal( new Integer( totalRegistros ).toString() ); // Se guarda total de registros
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &creando archivo de exportaci�n ...&", EIGlobal.NivelLog.DEBUG);
						exportar = exportarArchivo(nombreArchivo, session.getArchivoBit() , archivoExportar, totalRegistros, req, res );

						req.setAttribute( CONTRATO, contrato);

			                        // JVL 04/01/2006 Coloco la descripcion del contrato en NOMCONTRATO, se utilizara en el JSP (CompNvoPILC.jsp)
						req.setAttribute( "NOMCONTRATO", NOMCONTRATO.replace(' ','+'));
			                        // JVL 04/01/2006 fin modificacion
						if ( exportar != 0 ){
							return exportar;
						}
						if ( totalRegistros>1000 )// demasiados registros
						{
							String botonExportar = "";
							String opcionesExportar = "";
							salida += "		<form name =\"frmbit\" method = \"POST\">\n";
							salida += "			<input type=\"Hidden\" name=\"total\" value= \"" + session.gettotal() + "\">\n";
							salida += "			<input type=\"Hidden\" name=\"prev\" value= \"" + 0 + "\">\n";
							salida += "			<input type=\"Hidden\" name=\"next\" value= \"" + 0 + "\">\n";
							salida += "			Existen m&aacute;s de 1000 movimientos\n";
							salida += "		</form>\n";
							opcionesExportar += "<a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('txt');\">Exporta en TXT <input id=\"extTxt\" type=\"radio\" name=\"tipoExportacion\" ></a>\n";
							opcionesExportar += "<br><a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('csv');\">Exporta en XLS <input id=\"extCsv\" type=\"radio\" name=\"tipoExportacion\" checked='true'></a><br><br>\n";
							opcionesExportar += "<input id=\"tipoArchivoExportacion\" name=\"tipoArchivoExportacion\" type=\"hidden\" value='csv'/>\n";
							botonExportar += "<a id=\"bExport\" href=\"javascript:exportaBitacora();\">";
							botonExportar += "<img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" width=85 height=22 alt=Exportar></a>\n";

							req.setAttribute( "Output1",salida);
							req.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
							req.setAttribute( MENU_PRINCIPAL, session.getStrMenu());
							req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
							req.setAttribute( MENSAJE, salida );
							req.setAttribute( "Boton", botonExportar );
							req.setAttribute( "opcionesExportar", opcionesExportar );
							//aqui
							req.setAttribute( CONTRATO, contrato);

					                // JVL 04/01/2006 Coloco la descripcion del contrato en NOMCONTRATO, se utilizara en el JSP (CompNvoPILC.jsp)
						        req.setAttribute( "NOMCONTRATO", NOMCONTRATO.replace(' ','+'));
					                // JVL 04/01/2006 Fin modificacion

							evalTemplate( "/jsp/EI_Mensaje1000.jsp", req, res);
						}else if( totalRegistros >= Global.NUM_REGISTROS_PAGINA ) {
							regreso[7] = TRANSFERENCIAS;
							regreso[6] = INVERSIONES;
							regreso[5] = INTERBANCARIAS;
							regreso[4] = LINCAPSUA;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;
							//aqui
							muestraBitacora(importeDe,importeA,USUARIO,CUENTA,regreso,REFERENCIA,contrato,FECHAINI,FECHAFIN, session.getArchivoBit(), 0, Global.NUM_REGISTROS_PAGINA ,req, res);
							}

						else if( totalRegistros < Global.NUM_REGISTROS_PAGINA && totalRegistros >=1 ) {
							regreso[7] = TRANSFERENCIAS;
							regreso[6] = INVERSIONES;
							regreso[5] = INTERBANCARIAS;
							regreso[4] = LINCAPSUA;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;
							//aqui
							muestraBitacora(importeDe,importeA,USUARIO,CUENTA,regreso,REFERENCIA,contrato,FECHAINI,FECHAFIN, session.getArchivoBit(), 0, totalRegistros, req, res );
							}

						else{
							req.setAttribute( "Output1","<h3>No hay registros asociados a su consulta !");
							req.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
							req.setAttribute( MENU_PRINCIPAL, session.getStrMenu());
							req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
							req.setAttribute( MENSAJE, "No hay registros asociados a su consulta !"  );
							evalTemplate( EI_MENSAJEDOS, req, res);
						}
					}else{
						// error en el archivo filtro
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &error en archivo filtro ...&", EIGlobal.NivelLog.DEBUG);
						req.setAttribute( "Output1","<h3>Error inesperado !");
						req.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
						req.setAttribute( MENU_PRINCIPAL, session.getStrMenu());
						req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
						req.setAttribute( MENSAJE, "Error inseperado !." );
						evalTemplate( EI_MENSAJEDOS, req, res);
					}
				}else
				{
					// no hay registros
					req.setAttribute("Output1","<h3>" + ERROR[0] + "!" );
					req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
					req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
					req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
					req.setAttribute(MENSAJE,ERROR[0]);
					evalTemplate( EI_MENSAJEDOS, req, res);
				} // if( totalRegistros >0 )

			} // if ( llamadaServicio )
		}else{

			// no es la primer ventana
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &ventana siguiente ...&", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &obteniendo par�metros ...&", EIGlobal.NivelLog.DEBUG);

			MISPARAMETROS	= ( String) req.getParameter( "parametros" );



			String [] elimportetemp = new String [2];
			elimportetemp[0] = importeDe;
			elimportetemp[1] = importeA;
			String USUARIO_ [] = new String [1];
			USUARIO_[0] = USUARIO;

			String CONTRATO_ [] = new String [1];
			CONTRATO_[0] = contrato;

			String CUENTA_ [] = new String [1];
			CUENTA_[0] = CUENTA;
			regreso[7] = TRANSFERENCIAS;
			regreso[6] = INVERSIONES;
			regreso[5] = INTERBANCARIAS;
			regreso[4] = LINCAPSUA;
			regreso[3] = ACEPTADAS;
			regreso[2] = RECHAZADAS;
			regreso[1] = PROGRAMADAS;
			regreso[0] = PENDIENTES;

			String fechas[] = new String[2];
			fechas[0] ="";
			fechas[1] ="";

			parametros = obtenParametros(fechas, regreso,CUENTA_,USUARIO_,elimportetemp,REFERENCIA,MISPARAMETROS,CONTRATO_,SEMAFORO,req);
			FECHAINI	= fechas[0];
			FECHAFIN	= fechas[1];


			importeDe = elimportetemp [0];
			importeA = elimportetemp [1];
			USUARIO = USUARIO_[0];
			contrato = CONTRATO_[0];
			CUENTA = CUENTA_[0];

			TRANSFERENCIAS =regreso[7];
			INVERSIONES=regreso[6];
			INTERBANCARIAS=regreso[5];
			LINCAPSUA = regreso[4];
			ACEPTADAS=regreso[3];
			RECHAZADAS=regreso[2];
			PROGRAMADAS=regreso[1];
			PENDIENTES=regreso[0];





//FECHAINI FECHAFIN REFERENCIA
			if ( parametros == 0 ){
				// par�metros OK

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &par�metros OK ...&", EIGlobal.NivelLog.DEBUG);

				if ( contrato.length() < 2 ){
					contrato	= session.getContractNumber();
				}
				if ( USUARIO.length() < 2 ){
					USUARIO	= session.getUserID8();
				}

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &registros del "+PREV+" al "+NEXT+"&", EIGlobal.NivelLog.INFO);
				regreso[7] = TRANSFERENCIAS;
				regreso[6] = INVERSIONES;
				regreso[5] = INTERBANCARIAS;
				regreso[4] = LINCAPSUA;
			regreso[3] = ACEPTADAS;
			regreso[2] = RECHAZADAS;
			regreso[1] = PROGRAMADAS;
			regreso[0] = PENDIENTES;

				muestraBitacora(importeDe,importeA,USUARIO,CUENTA,regreso,REFERENCIA,contrato, FECHAINI, FECHAFIN, session.getArchivoBit(), Integer.parseInt(PREV), Integer.parseInt(NEXT) , req, res);

			}else{
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &error en par�metros ...&", EIGlobal.NivelLog.DEBUG);
				req.setAttribute("Output1","<h3>Error inesperado !");
				req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
				req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
				req.setAttribute(ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA,S25320H,req) );
				req.setAttribute( MENSAJE, "Error inesperado !." );
				evalTemplate( EI_MENSAJEDOS, req, res);
			} // if ( par�metros )

		} // if ( PREV )

		return 1;
	} // m�todo
	
	/**
	 * Evalua si el usuario tiene facultades para el contrato
	 * @param usuario Usuario a validar.
	 * @param sess Objeto sesion
	 * @return Regresa true si es un usuario valido.
	 */
	private boolean evaluarUsuario(String usuario, HttpSession sess) {
		boolean resultado = false;
		String[][] usuarios = (String[][]) sess.getAttribute("listaUsuarios");
		String usuAux ="";

		EIGlobal.mensajePorTrace("validando el usuario "+usuario, EIGlobal.NivelLog.INFO);

		if((usuario==null)||("".equals(usuario.trim()))||("0".equals(usuario.trim()))||(usuarios==null)){
			return true;
		}

		for(int i=0;i<usuarios.length;i++){
			usuAux = usuarios[i][1];
			if(usuAux!=null){
				EIGlobal.mensajePorTrace("validando el usuario "+usuAux, EIGlobal.NivelLog.INFO);
				if(usuAux.equals(usuario)){
					resultado=true;
					break;
				}
			}
		}

		EIGlobal.mensajePorTrace("El resultado de la consulta es: "+resultado, EIGlobal.NivelLog.INFO);
		return resultado;
	}
	/**
	 * Metodo que se encarga de validar si la cuenta esta relacionada al contrato.
	 * @param cuenta Cuenta a validar
	 * @param contrato Contrato a validar
	 * @return Regresa true si la cuenta pertenece al contrato.
	 */
	private boolean evaluarCuenta(String cuenta, String contrato) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer();
        boolean resultado = false;

        if((cuenta==null)||("".equals(cuenta.trim()))){
          return true;
        }


        try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query.append("Select num_cuenta From nucl_relac_ctas Where num_cuenta2 ='");
			query.append(contrato);
			query.append("' And num_cuenta = '");
			query.append(cuenta);
			query.append("' ");

			EIGlobal.mensajePorTrace ("Validando la cuenta:: -> Query:" + query.toString(), EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query.toString());

			if(rs.next()){
				resultado=true;
			}

			EIGlobal.mensajePorTrace ("La cuenta existe:"+resultado, EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace (" Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace("SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
		return resultado;
	}

	/**
	 * Actualiza la consita de bitacora.
	 *
	 * @param usuario El objeto: usuario
	 * @param contrato El objeto: contrato
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto int
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public int actualizaBitacora(String usuario, String contrato,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		int salida			= 0; // por default OK
		String tramaSalida		= "";
		String tramaEntrada	= "";
		String tipoOperacion	= "ABIT";  // prefijo del servicio ( actualizaci�n de la bit�cora )
		String cabecera			= "2EWEB"; // medio de entrega ( regresa un archivo )
		//String facultad_consulta = session.getFacCBitacora(); // facultad para actualizar

		//boolean facultad_consulta = session.getFacultad(session.FAC_CONSULTA_BITACORA);
		String fechaInicial	 = ( String) req.getParameter("fecha1");
		String fechaFinal		 = ( String) req.getParameter("fecha2");

		String perfil			 = "";

		perfil = session.getUserProfile();

		try {
			fechaInicial	= fechaInicial.substring( 3,5 )
							+ fechaInicial.substring( 0,2 )
							+ fechaInicial.substring( 8,10 );  // mmddaa

			fechaFinal		= fechaFinal.substring( 3,5 )
							+ fechaFinal.substring( 0,2 )
							+ fechaFinal.substring( 8,10 );  // mmddaa
		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "Error en parseo de fecha_inicial: " + fechaInicial , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace( "Error en parseo de fecha_final: " + fechaFinal , EIGlobal.NivelLog.ERROR);
			salida = 1;	//error
			return salida;
		}
		//TuxedoGlobal tuxedoGlobal;

		//if ( facultad_consulta.trim().equals("true") || facultad_consulta.trim().length()!=0 ){
		if (session.getFacultad(session.FAC_CONSULTA_BITACORA)) {
			try{
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.DEBUG);
				tramaEntrada	=       cabecera + "|"
								+        usuario + "|"
								+ tipoOperacion + "|"
								+       contrato + "|"
								+        usuario + "|"
								+        perfil  + "|"
								+  fechaInicial + "|"
								+  fechaFinal;

				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

				try{
					//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo() ;
					ServicioTux tuxedoGlobal = new ServicioTux();
					//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					Hashtable hs = tuxedoGlobal.web_red( tramaEntrada );
					tramaSalida = (String) hs.get("BUFFER");

				}catch(java.rmi.RemoteException re){
					EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &remote shell ...&"+re, EIGlobal.NivelLog.DEBUG);
				}catch (Exception e) {}
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);

				if ( tramaSalida.equals( IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() ) ){
					// Copia Remota
					EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &remote shell ...&", EIGlobal.NivelLog.DEBUG);
					ArchivoRemoto archivoRemoto = new ArchivoRemoto();

					if(!archivoRemoto.copia( session.getUserID8() ) ){
						salida = 1;	// error al copiar
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &error de  remote shell ...&", EIGlobal.NivelLog.ERROR);
					}else{
						salida=0; // OK
						EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &remote shell OK ...&", EIGlobal.NivelLog.DEBUG);
					}
				}else{
					salida=1; // error
				}
			}catch(Exception ioe ){
				salida = 1; // error
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Excepci�n en actualizaBitacora() >>"+ioe.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
			} // try
			//  BIT CU1151
			/*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
		  if ( "ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim()) ){
				try{
		        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_BITACORA_CONSULTA_BIT_ACTUALIZA);
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					if(tipoOperacion!=null){
						bt.setServTransTux(tipoOperacion.trim());
					}
					if(tramaSalida!=null){
						if(tramaSalida.length() > 2 && "OK".equals(tramaSalida.substring(0,2))){
							bt.setIdErr(tipoOperacion.trim()+SUB_CEROS);
						}else if(tipoOperacion.length()>8){
							bt.setIdErr(tipoOperacion.substring(0,8));
						}else{
							bt.setIdErr(tipoOperacion.trim());
						}
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &actualizaBitacora 1 ...&"+ e, EIGlobal.NivelLog.DEBUG);
				}catch(Exception e){
					EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &actualizaBitacora 2 ...&"+ e, EIGlobal.NivelLog.DEBUG);
				}
			}
			/*
			 * 02/ENE/07
			 * VSWF-BMB-F
			 */

		}else{
			// no se tiene facultad
			salida = 1;
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &facultad de consulta NO v�lida ...&", EIGlobal.NivelLog.DEBUG);
		}
		return salida;
	}


	/**
	 * Filtra archivos.
	 *
	 * @param interbancarias El objeto: interbancarias
	 * @param cuenta El objeto: cuenta
	 * @param usuario1Selec El objeto: usuario1 selec
	 * @param importeDe El objeto: importe de
	 * @param importeA El objeto: importe a
	 * @param referencia El objeto: referencia
	 * @param semaforo El objeto: semaforo
	 * @param archivoEntrada El objeto: archivo entrada
	 * @param archivoSalida El objeto: archivo salida
	 * @param numeroLineas El objeto: numero lineas
	 * @return Objeto int
	 */
	public int filtrarArchivo(boolean interbancarias,String cuenta,String usuario1Selec,String importeDe,String importeA,String [] referencia,
			String semaforo[], String archivoEntrada, String archivoSalida, int numeroLineas ){
		BufferedReader entrada;
		BufferedWriter salida;

		int i = 0;
		int renglonValido = 0;

		String[] renglon = null;
		String linea	 = "";
		String estatus	= "";

		try{
			entrada	= new BufferedReader( new FileReader( archivoEntrada ) );
			salida	= new BufferedWriter( new FileWriter( archivoSalida ) );
			linea  = entrada.readLine();
			linea += "\r\n";
			salida.write( linea ,0 ,linea.length() );

			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &creando archivo filtro ...&", EIGlobal.NivelLog.DEBUG);
			// EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &n�mero delineas :"+numeroLineas+"&", EIGlobal.NivelLog.INFO);

			for( i = 0; i < numeroLineas; i++ ){
				linea = entrada.readLine();
				if( "".equals(linea.trim())){
					linea = entrada.readLine();
				}
				if( linea == null ){
					break;
				}
				 //EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &registro : "+linea+"&", EIGlobal.NivelLog.INFO);
				renglon = desentrama( linea , ';' );
				estatus = obtenEstatus( renglon[0], renglon[10] ); // [0]tipo de operaci�n y [10]c�digo de error
				renglonValido = aplicaFiltro(interbancarias,cuenta,usuario1Selec,importeDe,importeA,referencia,semaforo, renglon, estatus ); // filtro por cada registro

				if ( renglonValido == 0 )// pas� el filtro
				{
					linea = estatus + ";" + linea + "\r\n";
					salida.write( linea, 0, linea.length() );
					salida.flush();
				} // if ( renglonValido )
			} // for ( i )
			salida.close();
			entrada.close();
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Excepci�n en filtrarArchivo() >>"+e.getMessage()+"<<2", EIGlobal.NivelLog.ERROR);

		} // try
		return 0;
	} // m�todo

	//=============================================================================================
	/**
	 * obtenEstatus.
	 *
	 * @param tipoOperacion El objeto: tipo operacion
	 * @param codigoError El objeto: codigo error
	 * @return Objeto string
	 */
	String obtenEstatus ( String tipoOperacion, String codigoError ){
		String	tipoError	= "";
		String	terminacion	= "";
		String  estatus		= "";

		tipoError	= codigoError.substring( 0, 4);
		terminacion = codigoError.substring( 4,8 );
		estatus		= "";

		if ( !"MANC".equals( tipoError ) ){
			if ( !"PROG".equals( tipoError ) ){

				if ( !"WTRA".equals( tipoError ) ){

					if ( "TRANSITO".equals( codigoError ) ){
						estatus = "T"; // en tr�nsito
					}else if ( !SUB_CEROS.equals( terminacion ) ){
						estatus = "R"; // rechazado
					}else{
						estatus = "A"; // aceptado
					}

				} else {
					if (SUB_CEROS.equals( terminacion ) ){
						estatus = "A";
					}else{
						estatus = "R";
					}
				}
			}else{
				if ( "ABIT".equals( tipoOperacion ) )// actualizaci�n a la bitacora
				{
					if ( !SUB_CEROS.equals( terminacion ) ){
						estatus = "R";
					}else{
						estatus = "A";
					}
				}else{
					if ( !SUB_CEROS.equals( terminacion ) ){
						estatus = "R";
					}else{
						estatus = "P"; // programado
					}
				} // if ( ABIT )
			} // if ( PROG )
		}else{
			if ("0001".equals( terminacion ) || "0002".equals( terminacion ) ||
					"0003".equals( terminacion ) || "0004".equals( terminacion ) ||
					"0007".equals( terminacion ) || "0008".equals( terminacion ) ||
					"0009".equals( terminacion ) || "0010".equals( terminacion ) ||
					"0011".equals( terminacion ) || "0014".equals( terminacion ) ||
					"0015".equals( terminacion ) || "0017".equals( terminacion ) ||
					"0018".equals( terminacion ) || "0019".equals( terminacion ) ||
					"0020".equals( terminacion ) || "0024".equals( terminacion ) ||
					"0025".equals( terminacion ) || "0026".equals( terminacion ) ||
					"0027".equals( terminacion ) || "0028".equals( terminacion )) {
				estatus = "R"; // Rechazada por limites de importes - JGAL
			} else {
				if ("0022".equals( terminacion )) {
					estatus = "V"; //Verificada
				} else {
					if ("0023".equals( terminacion )) {
						estatus = "T"; //Pre Autorizada
					} else {
						if (SUB_CEROS.equals( terminacion) && ("CAMA".equals( tipoOperacion)
								|| "COMA".equals(tipoOperacion ))) {
							estatus = "A";
						} else {
							estatus = "M"; // mancomunada
						}
					}
				}
			}
		} // if ( MANC )
		/** Se validan operaciones provenientes de Archivos Cifrados - CertificadoDigitalServlet | UploadServlet **/
		if (SUB_CEROS.equals(terminacion) && ("IMCD".equals(tipoOperacion) || "RCCE".equals(tipoOperacion) || "DCBC".equals(tipoOperacion)  || "CCCE".equals(tipoOperacion) ||
				"ACTN".equals(tipoOperacion) ||
				"ACTD".equals(tipoOperacion) ||
				"ACTB".equals(tipoOperacion) ||
				"ACPT".equals(tipoOperacion) ||
				"ACAE".equals(tipoOperacion) ||
				"ACPN".equals(tipoOperacion) ||
				"ACNI".equals(tipoOperacion) ||
				"ACAP".equals(tipoOperacion) ||
				"ACPP".equals(tipoOperacion) ||
				"ACPD".equals(tipoOperacion) ||
				"ACOF".equals(tipoOperacion) ||
				"ACOM".equals(tipoOperacion) ||
				"ACCH".equals(tipoOperacion) ||
				"ACTE".equals(tipoOperacion) ||
				"ACPI".equals(tipoOperacion) ||
				"ACAN".equals(tipoOperacion) ||
				"ACBN".equals(tipoOperacion) ||
				"ACMN".equals(tipoOperacion) ||
				"ACAT".equals(tipoOperacion) ||
				"ACAI".equals(tipoOperacion) ||
				"ACAC".equals(tipoOperacion) ||
				"ACNL".equals(tipoOperacion)) ) {
			estatus = "A";
		} else if (!SUB_CEROS.equals(terminacion) && ("IMCD".equals(tipoOperacion) || "RCCE".equals(tipoOperacion) || "DCBC".equals(tipoOperacion) || "CCCE".equals(tipoOperacion) ||
				"ACTN".equals(tipoOperacion) ||
				"ACTD".equals(tipoOperacion) ||
				"ACTB".equals(tipoOperacion) ||
				"ACPT".equals(tipoOperacion) ||
				"ACAE".equals(tipoOperacion) ||
				"ACPN".equals(tipoOperacion) ||
				"ACNI".equals(tipoOperacion) ||
				"ACAP".equals(tipoOperacion) ||
				"ACPP".equals(tipoOperacion) ||
				"ACPD".equals(tipoOperacion) ||
				"ACOF".equals(tipoOperacion) ||
				"ACOM".equals(tipoOperacion) ||
				"ACCH".equals(tipoOperacion) ||
				"ACTE".equals(tipoOperacion) ||
				"ACPI".equals(tipoOperacion) ||
				"ACAN".equals(tipoOperacion) ||
				"ACBN".equals(tipoOperacion) ||
				"ACMN".equals(tipoOperacion) ||
				"ACAT".equals(tipoOperacion) ||
				"ACAI".equals(tipoOperacion) ||
				"ACAC".equals(tipoOperacion) ||
				"ACNL".equals(tipoOperacion) ) ){
			estatus = "R";
		}
		return estatus;
	} // m�todo

	/**
	 * Valida si son letras.
	 *
	 * @param usuario usuario
	 * @return boolean si son letras
	 */
	public boolean is_letter(String usuario)
	{
		int longitud = usuario.length();
		for(int i = 0; i < longitud; i++) {
			if(usuario.charAt(i)>= 65 && usuario.charAt(i) <= 90){
				return true;
			}

		}

		return false;
	}

	//=============================================================================================
	/**
	 * aplicaFiltro.
	 *
	 * @param INTERBANCARIAS El objeto: interbancarias
	 * @param CUENTA El objeto: cuenta
	 * @param USUARIO1_Selec El objeto: USUARI o1_ selec
	 * @param importeDe El objeto: importe de
	 * @param importeA El objeto: importe a
	 * @param REFERENCIA El objeto: referencia
	 * @param SEMAFORO El objeto: semaforo
	 * @param registro El objeto: registro
	 * @param estatus_temp El objeto: estatus_temp
	 * @return Objeto int
	 */
	public int aplicaFiltro(boolean INTERBANCARIAS, String CUENTA, String USUARIO1_Selec,String importeDe,String importeA,
			String [] REFERENCIA, String SEMAFORO[],String[] registro, String estatus_temp ){
		int filtro = 0;
//antes del foltro
//  ABIT;0;0;000000000.00;24/09/2002;00/00/0000;1226;0000;97352;1001851;PROG0000;  ;EWEB;
//despues del foltro
//A;ABIT;0;0;000000000.00;24/09/2002;00/00/0000;1226;0000;97352;1001851;PROG0000;  ;EWEB;


		String tipoOperacion	= "";
		String usuarioRegistro = "";
		String usuarioFiltro	= "";
		String cuentaRetiro	= "";
		String cuentaDeposito	= "";
		String cuenta_filtro	= "";
		String importeRegistro	= "";
		String refRegistro		= "";
		String semaforo_filtro[]= new String[12];

		int ninguno				=0;
		int tmp_usuario			=0;

		//Modificacion del cliente_7_t08 15/03/2004 cgc.
                //tmp_usuario =Integer.parseInt(USUARIO1_Selec);
		tmp_usuario =Integer.parseInt(con.cliente_7To8(USUARIO1_Selec.trim()));
                //fin de modificacion cgc.



		tipoOperacion	= registro[0];
		//Modificacion del cliente_7_t08 15/03/2004 cgc.
		//usuarioRegistro= registro[9];

		//CSA
		if(is_letter(registro[9].trim())) usuarioRegistro = registro[9].trim();  //Para aquellas operaciones que se registraron con el Perfil
		else usuarioRegistro= new Integer(con.cliente_7To8(registro[9].trim())).toString(); // Operaciones bien registradas

		//fin de modificacion cgc.

		usuarioFiltro	= new Integer(tmp_usuario).toString();
		cuentaRetiro	= registro[1];
		cuentaDeposito	= registro[2];
		importeRegistro= registro[3];
		refRegistro	= registro[8];

		/*if(tipoOperacion.equals(ES_PAGO_SUA_LINEA_CAPTURA) || (tipoOperacion.indexOf("SULC")>=0) ){
			if(registro[13] != null){
				refRegistro = (registro[13]).trim();
			}else{
				refRegistro	= registro[8];
			}
		}else{
			refRegistro	= registro[8];
		}*/

		cuenta_filtro	= FormateaContrato( CUENTA );

		for ( int i = 0 ; i < 12  ; i++ ) semaforo_filtro[i] = "0"; // 0 0 0 0 000 0000

		filtro = 1; // no aplica el filtro

		ninguno = Integer.parseInt( SEMAFORO[0] )
				+ Integer.parseInt( SEMAFORO[1] )
				+ Integer.parseInt( SEMAFORO[2] )
				+ Integer.parseInt( SEMAFORO[3] )
				+ Integer.parseInt( SEMAFORO[4] )
				+ Integer.parseInt( SEMAFORO[5] )
				+ Integer.parseInt( SEMAFORO[6] )
				+ Integer.parseInt( SEMAFORO[7] )
				+ Integer.parseInt( SEMAFORO[8] )
				+ Integer.parseInt( SEMAFORO[9] )
				+ Integer.parseInt( SEMAFORO[10] )
				+ Integer.parseInt( SEMAFORO[11] );


		// existe un filtro por lo menos
		if ( ninguno > 0 ){
			//EIGlobal.mensajePorTrace( "*** VAO >> Entro a filtrar: "+ ninguno  + ">>", EIGlobal.NivelLog.INFO);
			// filtro de usuario
			if ( ( usuarioFiltro.trim().length() > 1) && ( usuarioRegistro.equals( usuarioFiltro ) ) ){ 
				semaforo_filtro[0] = "1";
			}
			// filtro de cuenta
			if ( ( cuenta_filtro.length() > 1 ) && ( cuentaRetiro.equals( cuenta_filtro ) ||
					cuentaDeposito.equals( cuenta_filtro ) ) ){
				semaforo_filtro[1] = "1";
			}

			// filtro de importe
			if ( importeDe.length() > 0 ){
				float impR = obtenerFloat(importeRegistro);
				float impDe = obtenerFloat(importeDe);
				float impA = obtenerFloat(importeA);
			//	if ( importeRegistro.equals( FormatoMoneda( importeDe ) ) ) semaforo_filtro[2] = "1";
				if ( impR >= impDe && impR <= impA ){
					semaforo_filtro[2] = "1";
				}
			}

			// filtro de referencia
			//if ( ( REFERENCIA[0].length() > 1 ) && ( refRegistro.equals( REFERENCIA[0] ) ) ) semaforo_filtro[3] = "1";  kitar descomentar
			if ( ( REFERENCIA[0].length() >= 1 )
					&& ( refRegistro.trim().equals( REFERENCIA[0] ) ) ){
				semaforo_filtro[3] = "1";
				}

			/*if ( ( REFERENCIA[0].length() >= 1 )
					&& ( registro[13].trim().equals( REFERENCIA[0].trim()) ) &&
					(tipoOperacion.equals(ES_PAGO_SUA_LINEA_CAPTURA) || (tipoOperacion.indexOf("SULC")>=0) ))
				{semaforo_filtro[3] = "1";
				EIGlobal.mensajePorTrace( "*** VAO >> Es Linea de Captura: "+ semaforo_filtro[3] + ">>", EIGlobal.NivelLog.INFO);

				}*/


			// filtro de transferencias

			if ( "TRAN".equals(tipoOperacion) ||
					"RETJ".equals(tipoOperacion) ||
					"PAGT".equals(tipoOperacion) ||
					"TRTJ".equals(tipoOperacion) ||
					"PIMP".equals(tipoOperacion) ||  /* Linea de Captura */
					"PILC".equals(tipoOperacion) ||
					"OCUR".equals(tipoOperacion) ||
					"RG03".equals(tipoOperacion) ||
				 //Modificacion Mancomunidad Fase II LFER
					"IN04".equals(tipoOperacion) ||
					"PNOS".equals(tipoOperacion) ||
					"PNIS".equals(tipoOperacion) ||
			     tipoOperacion.equals(ES_PAGO_MICROSITIO_MANC) ||
			     tipoOperacion.equals(ES_PAGO_SATMICROSITIO_MANC)
			     //Modificacion Mancomunidad Fase II LFER
				 ){
					semaforo_filtro[4] = "1";
				 }



			// filtro de inversiones
			if ( "CPSI".equals(tipoOperacion) ||
				 "VTSI".equals(tipoOperacion) ||
				 "CPDI".equals(tipoOperacion) ||
				 "CPDV".equals(tipoOperacion) ||
				 "CPID".equals(tipoOperacion) ||
				 "CPVD".equals(tipoOperacion) ||
				 "DIDV".equals(tipoOperacion) ||
				 "CPCI".equals(tipoOperacion)  ){
				semaforo_filtro[5] = "1";
			}

			// filtro de interbancario  DIBC = cotizacion Interbancaria DIBA = Consulta Interbancaria
			// INTE = Cotizaci�n Internacional
			// falta PAGT
			if ( ( INTERBANCARIAS ) && ( "CAIB".equals(tipoOperacion) ||
										 "DIBT".equals(tipoOperacion) ||
										 "INTE".equals(tipoOperacion) ||
										 "DIBC".equals(tipoOperacion) ) ){
				semaforo_filtro[6] = "1";
			}

			//VSWF - JGAL
			// Filtro por l�nea de captura SUA
			if ( /*( LINCAPSUA ) &&*/ ( tipoOperacion.equals(ES_PAGO_SUA_LINEA_CAPTURA))){
				semaforo_filtro[7] = "1";
			}

			// filtro de estatus de operaciones

			if ( "A".equals( estatus_temp ) ){
				semaforo_filtro[8] = "1"; // aceptado
			}
			if ( "R".equals( estatus_temp ) ){
				semaforo_filtro[9] = "1"; // rechazado
			}
			if ( "P".equals( estatus_temp ) ){
				semaforo_filtro[10] = "1"; // programada
			}
			if ( "N".equals( estatus_temp ) ){
				semaforo_filtro[11] = "1"; // pendientes
			}

			// log("m�scara del registro :"+semaforo_filtro[0]+semaforo_filtro[1]+semaforo_filtro[2]+semaforo_filtro[3]+semaforo_filtro[4]+semaforo_filtro[5]+semaforo_filtro[6]+semaforo_filtro[7]+semaforo_filtro[8]+semaforo_filtro[9]+semaforo_filtro[10]);

			// ( usuario ) y ( cuenta ) y ( transf o inver o inter ) y ( acep o rech o prog o pend )
			if ( ( ( semaforo_filtro[0].equals( SEMAFORO[0] ) && "1".equals(semaforo_filtro[0]) ) || "0".equals(SEMAFORO[0]) ) &&
				 ( ( semaforo_filtro[1].equals( SEMAFORO[1] ) && "1".equals(semaforo_filtro[1]) ) || "0".equals(SEMAFORO[1]) ) &&
				 ( ( semaforo_filtro[2].equals( SEMAFORO[2] ) && "1".equals(semaforo_filtro[2]) ) || "0".equals(SEMAFORO[2]) ) &&
				 ( ( semaforo_filtro[3].equals( SEMAFORO[3] ) && "1".equals(semaforo_filtro[3]) ) || "0".equals(SEMAFORO[3]) ) &&
				 ( ( semaforo_filtro[4].equals( SEMAFORO[4] ) && "1".equals(semaforo_filtro[4]) ) ||
				   ( semaforo_filtro[5].equals( SEMAFORO[5] ) && "1".equals(semaforo_filtro[5]) ) ||
				   ( semaforo_filtro[6].equals( SEMAFORO[6] ) && "1".equals(semaforo_filtro[6]) ) ||
				   ( semaforo_filtro[7].equals( SEMAFORO[7] ) && "1".equals(semaforo_filtro[7]) ) ||
				   ( "0".equals(SEMAFORO[4]) && "0".equals(SEMAFORO[5]) && "0".equals(SEMAFORO[6]) && "0".equals(SEMAFORO[7]) ) ) &&
				 ( ( semaforo_filtro[8].equals( SEMAFORO[8] ) && "1".equals(semaforo_filtro[8]) ) ||
				   ( semaforo_filtro[9].equals( SEMAFORO[9] ) && "1".equals(semaforo_filtro[9]) ) ||
				   ( semaforo_filtro[10].equals( SEMAFORO[10] ) && "1".equals(semaforo_filtro[10]) ) ||
				   ( semaforo_filtro[11].equals( SEMAFORO[11] ) && "1".equals(semaforo_filtro[11]) ) ||
				   ( "0".equals(SEMAFORO[8]) && "0".equals(SEMAFORO[9]) && "0".equals(SEMAFORO[10]) && "0".equals(SEMAFORO[11]) ) ) )
			{
				filtro = 0; // pas� el filtro
				//EIGlobal.mensajePorTrace( "*** VAO >> Salida filtro:  "+ filtro  + ">>", EIGlobal.NivelLog.INFO);
			}
		}else{
			// no se seleccion� ningun filtro
			filtro =0;
		}

		return filtro;
	} // m�todo

	/**
	 * Obtener float.
	 *
	 * @param str El objeto que contiene un numero
	 * @return Objeto float correspondiente al numero
	 */
	private float obtenerFloat(String str) {
		float numero = 0;
		try{
			if(str==null||str.isEmpty()){
				numero = 0;
			}else{
				str = str.replace(SIGNO_PESOS, VACIO);
				str = str.replace(COMA, VACIO);
				str = str.replace(SPACE, VACIO);
				numero = Float.parseFloat(str);
			}
		}catch(NumberFormatException e){
			EIGlobal.mensajePorTrace("Error parsear numero " + str+": "+e.getMessage(), EIGlobal.NivelLog.ERROR);
			numero = 0;
		}
		return numero;
	}

	/**
	 * Muestra la bitacora.
	 *
	 * @param importeDe El objeto: importe de
	 * @param importeA El objeto: importe a
	 * @param USUARIO El objeto: usuario
	 * @param CUENTA El objeto: cuenta
	 * @param regreso El objeto: regreso
	 * @param REFERENCIA El objeto: referencia
	 * @param CONTRATO El objeto: contrato
	 * @param FECHAINI El objeto: fechaini
	 * @param FECHAFIN El objeto: fechafin
	 * @param archivo_fuente El objeto: archivo_fuente
	 * @param inicio El objeto: inicio
	 * @param fin El objeto: fin
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto int
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public int muestraBitacora(String importeDe,String importeA,String USUARIO,String CUENTA, boolean [] regreso, String [] REFERENCIA,String CONTRATO, String FECHAINI,
									String FECHAFIN, String archivo_fuente, int inicio, int fin,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &desplegando datos en pantalla ...&", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		StringBuilder tabla		= new StringBuilder();
		String encabezado	= "";


	boolean TRANSFERENCIAS	= false;
	boolean INVERSIONES		= false;
	boolean INTERBANCARIAS	= false;
	boolean LINCAPSUA		= false;
	boolean ACEPTADAS		= false;
	boolean RECHAZADAS		= false;
	boolean PROGRAMADAS		= false;
	boolean PENDIENTES		= false;



		/*Date hoy	= new Date();
		Date day1	= new Date();
		Date day2	= new Date();*/

		int delimitador = 0;

		String menu_p = "";
		menu_p        = session.getstrMenu();

		if(menu_p == null){
			menu_p = "";
		}
		req.setAttribute( MENU_PRINCIPAL,	menu_p );
		req.setAttribute( FECHA,			ObtenFecha() );
		req.setAttribute( "ContUser",		ObtenContUser(req) );

		delimitador	= 15;

		String arreglo_registros[][] = obtenRegistrosArchivo( archivo_fuente, ";", delimitador, inicio, fin );
		String fecha_inicio			 = "";
		String fecha_fin			 = "";

		// se inicializa la tabla de datos
		tabla.append("<Table border=0 cellspacing=2 cellpadding=0 width=750 >\n");
		tabla.append( "	<tr align=left>\n");
		if (CUENTA.trim().length()>1 )tabla.append( "		<td class=texenccon align=left colspan=10>Cuenta : ").append(CUENTA).append("</td>\n");
		else tabla.append( "		<td align=left colspan=10>&nbsp;</td>\n");
		tabla.append( "	</tr>\n");

		/*comentado por pablo***************************************************
		**ya que no se usan las sig lineas*************************************
		************************************************************************
		day1.setDate(  Integer.parseInt( session.getfechaini().substring( 0,2 ) ) );
		day1.setMonth( Integer.parseInt( session.getfechaini().substring( 3,5 ) )-1 );
		day1.setYear(  Integer.parseInt( session.getfechaini().substring( 6,10 ) ) );
		day2.setDate(  Integer.parseInt( session.getfechafin().substring( 0,2 ) ) );
		day2.setMonth( Integer.parseInt( session.getfechafin().substring( 3,5 ) )-1 );
		day2.setYear(  Integer.parseInt( session.getfechafin().substring( 6,10 ) ) );



		//   rango de fechas
		if(	( hoy.getDate()  == day1.getDate() && hoy.getDate()	 == day2.getDate() ) &&
			( hoy.getMonth() == day1.getMonth()&& hoy.getMonth() == day2.getMonth() )&&
			( ( hoy.getYear() + 1900 ) == day1.getYear() ) &&
			( ( hoy.getYear() + 1900 ) == day2.getYear() ) )
		{
			//tabla += "	<tr>\n";
			//tabla += "		<td align=right colspan=10><b>Bit&aacute;cora del d&iacute;a&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;:&nbsp;" + session.gettotal() + "</b></td>\n";
			//tabla += "	</tr>\n";
		}
		else
		{
			//tabla += "	<tr>\n";
			//tabla += "		<td align=right colspan=10><b>Bit&aacute;cora del&nbsp;" + FECHAINI;
			//tabla +=		"&nbsp;al&nbsp;" + FECHAFIN + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;:&nbsp;" + session.gettotal() + "</b></td>";
			//tabla += "	</tr>\n";
		}
		*comentado por pablo***************************************************
		***********************************************************************
		***********************************************************************/
		// total de operaciones
		tabla.append("	<tr>\n");
		//tabla += "		<td align=left class=texenccon colspan=10>Contrato" + session.getNombreContrato() + "</td>\n";
		tabla.append("		<td align=left class=texenccon colspan=10>Total de operaciones:&nbsp;").append( session.gettotal()).append( "</td>\n");
		tabla.append( "	</tr>\n");
		tabla.append( "</table>\n");

        tabla.append("<table width=750 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>\n");

		// encabezados de columna
        tabla.append( "	<tr>\n");
        tabla.append( "		<td width=50  class=tittabdat align=center>Fecha</td>\n");
        tabla.append( "		<td width=30  class=tittabdat align=center>Hora</td>\n");
        tabla.append( "		<td width=155 class=tittabdat align=center>Descripci&oacute;n</td>\n");
        tabla.append( "		<td width=100 class=tittabdat align=center>Cuenta de cargo</td>\n");
        tabla.append( "		<td width=125 class=tittabdat align=center>Cuenta de abono/M&oacute;vil</td>\n");
        tabla.append( "		<td width=66  class=tittabdat align=center>Importe</td>\n");
        tabla.append( "		<td width=65  class=tittabdat align=center>Referencia</td>\n");
        tabla.append( "		<td width=55  class=tittabdat align=center>Usuario</td>\n");
        tabla.append( "		<td width=55  class=tittabdat align=center>Estatus</td>\n");
        tabla.append( "      <td width=50  class=tittabdat align=center>Origen Oper.</td>\n");
        tabla.append( "	</tr>\n");

		// contenido de la tabla
EIGlobal.mensajePorTrace( "***HELG Antes de armaRenglones", EIGlobal.NivelLog.DEBUG);

		tabla.append( armaRenglones(USUARIO, arreglo_registros, fin - inicio ,req));

		tabla.append( "</table>\n");

		// env�a par�metros
		tabla.append("<table width=750 border=0 cellspacing=2 cellpadding=0>\n");
		tabla.append("	<tr align=left>\n");
		tabla.append("		<td class=texenccon>Si desea obtener el comprobante de cualquier ");
        tabla.append(		"operaci&oacute;n, d&eacute; click en el n&uacute;mero de referencia ");
		tabla.append(		"subrayado.</td>\n");
		tabla.append("	</tr>\n");
		tabla.append("</table>");
		tabla.append("<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>\n");
		tabla.append("	<tr><td>&nbsp;</td></tr>\n");
		tabla.append("	<tr>\n");
		tabla.append("		<td align=center class=texfootpagneg>\n");
		tabla.append("			<Form name =\"frmbit\" method = \"POST\" ACTION=\"ConsultaBitacora\">\n");
		tabla.append("              <input type=\"Hidden\" name=\"banco\" value= \"" ).append( session.getClaveBanco()).append( "\">\n");
		tabla.append("				<input type=\"Hidden\" name=\"total\" value= \"" ).append( session.gettotal() ).append( "\">\n");
		tabla.append("				<input type=\"Hidden\" name=\"prev\" value= \"" ).append( new Integer(inicio).toString() ).append( "\">\n");
		tabla.append("				<input type=\"Hidden\" name=\"next\" value= \"" ).append( new Integer(fin).toString() ).append( "\">\n");
		tabla.append("				<input type=\"Hidden\" name=\"parametros\" value=\"" ).append( CONTRATO ).append(";") ;

		if( USUARIO.trim().length() == 0 ) tabla .append( "0;");
		else							   tabla .append( USUARIO + ";");

		if( CUENTA.trim().length() == 0 )  tabla .append( "0;");
		else					           tabla .append( CUENTA + ";");

		if( importeDe.trim().length() == 0 ) tabla .append( "0;");
		else					           tabla .append( importeDe + ";");

		if( importeA.trim().length() == 0 ) tabla .append( "0;");
		else					           tabla .append( importeA + ";");

		if(REFERENCIA[0].trim().length()==0)  tabla .append( "0;");
		else						       tabla .append( REFERENCIA[0] + ";");

		tabla .append( new Boolean( TRANSFERENCIAS ).toString()).append( ";")
			   .append( new Boolean( INVERSIONES ).toString()).append( ";")
			   .append( new Boolean( INTERBANCARIAS ).toString()).append( ";")
			   .append( new Boolean( LINCAPSUA ).toString() ).append( ";")
			   .append( new Boolean( ACEPTADAS ).toString() ).append( ";")
			   .append( new Boolean( RECHAZADAS ).toString() ).append( ";")
			   .append( new Boolean( PROGRAMADAS ).toString() ).append( ";")
			   .append( new Boolean( PENDIENTES ).toString() ).append( ";")
			   .append( FECHAINI ).append( ";")
			   .append( FECHAFIN ).append( ";\">\n");

		tabla .append( "Movimientos : " ).append( new Integer( inicio+1 ).toString() ).append( " - "
				).append( new Integer( fin ).toString() ).append( " de " ).append( session.gettotal() ).append( "<br><br>\n");


		if( inicio > 0 ){
			tabla .append( "<a href=\"javascript:atras();\"> Anteriores ").append( Global.NUM_REGISTROS_PAGINA  ).append( "</a>&nbsp;");
		}

		if( fin < Integer.parseInt( session.gettotal() ) )
		{
			if(	( fin + Global.NUM_REGISTROS_PAGINA ) <= Integer.parseInt( session.gettotal() ) ) //class=texfootpaggri
			tabla.append( "<A HREF=\"javascript:adelante();\">Siguientes " ).append( new Integer(fin - inicio).toString() ).append( " ></A>\n");
			else
			tabla.append( "<a href=\"javascript:adelante();\">Siguientes " ).append( new Integer(Integer.parseInt( session.gettotal() ) - fin ).toString() ).append( " ></a>\n");
		}
/*
		if( inicio > 0 ) tabla += "<a href=\"javascript:history.back();\" class=texfootpaggri>< Anteriores 30</a>&nbsp;\n";

		if( fin < Integer.parseInt( session.gettotal() ) )
		{
			if(	( fin + 30 ) <= Integer.parseInt( session.gettotal() ) )
				tabla += "<nobr><a href=\"javascript:adelante();\" class=texfootpaggri>Siguientes " + new Integer( fin - inicio ).toString() + " ></a>\n";
			else
				tabla += "<nobr><a href=\"javascript:adelante();\" class=texfootpaggri>Siguientes " + new Integer(Integer.parseInt( session.gettotal() ) - fin ).toString() + " ></a>\n";
		}
*/

		tabla.append( "				<br/><br/><a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('txt');\">Exporta en TXT <input id=\"extTxt\" type=\"radio\" name=\"tipoExportacion\"></a>\n");
		tabla.append( "				<br><a style=\"cursor: pointer;\" class='tabmovtex11' onclick=\"cambiaArchivoExport('csv');\">Exporta en XLS <input id=\"extCsv\" type=\"radio\" name=\"tipoExportacion\" checked='true'></a><br>\n");
		tabla.append( "				<br><a id=\"bExport\" href=\"javascript:exportaBitacora();\"><img border=0 name=imageField4 src=\"/gifs/EnlaceMig/gbo25230.gif\" width=85 height=22 alt=Exportar></a><nobr>\n");
		tabla.append( "				<a href = \"javascript:scrImpresion();\"><img border=0 name=imageField42 src=\"/gifs/EnlaceMig/gbo25240.gif\" width=83 height=22 alt=\"Imprimir\"></a>\n");
		tabla.append( "			</Form>\n ");
		tabla.append( "			<input id=\"tipoArchivoExportacion\" name=\"tipoArchivoExportacion\" type=\"hidden\" value='csv'/>");
		tabla.append( "		</td>\n");
		tabla.append( "	</tr>\n");
		tabla.append( "</table>\n");

		if ( !FECHAINI.trim().equals( FECHAFIN.trim() ) ) {
			encabezado = ENC_BIT_HIS;
			req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,CONSULTAS_BITACORA,S25320H,req) );
		}else {
			encabezado	= "Consulta de Bit&aacute;cora general del d&iacute;a";
			req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,CONSULTAS_BITACORA,S25320H,req) );// s25290h historica
		}
		req.setAttribute("Output1",tabla.toString());
		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		req.setAttribute("varpaginacion", Global.NUM_REGISTROS_PAGINA);
		req.setAttribute( "CONTRATO", CONTRATO);

	       // JVL 04/01/2006 Coloco la descripcion del contrato en NOMCONTRATO, se utilizara en el JSP (CompNvoPILC.jsp)
		req.setAttribute( "NOMCONTRATO", session.getNombreContrato().replace(' ','+'));
	       // JVL 04/01/2006 Fin Modificacion
		evalTemplate(IEnlace.BITACORA_TMPL, req, res);

							regreso[7] = TRANSFERENCIAS;
							regreso[6] = INVERSIONES;
							regreso[5] = INTERBANCARIAS;
							regreso[4] = LINCAPSUA;
							regreso[3] = ACEPTADAS;
							regreso[2] = RECHAZADAS;
							regreso[1] = PROGRAMADAS;
							regreso[0] = PENDIENTES;
		return 0;
	}


	/**
	 * Obtiene registros de archivo.
	 *
	 * @param filename El objeto: filename
	 * @param delim El objeto: delim
	 * @param Ndelim El objeto: ndelim
	 * @param b_ini El objeto: b_ini
	 * @param b_end El objeto: b_end
	 * @return Objeto string[][]
	 */
	public String[][] obtenRegistrosArchivo(String filename,String delim,int Ndelim,int b_ini,int b_end){

        EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Inicia obtenRegistrosArchivo()...&", EIGlobal.NivelLog.DEBUG);

		BufferedReader entrada;
		StringTokenizer t ;
		String linea;

		linea     = null;
		int i     = 0;
		int i_ini = b_ini;
		int i_end = b_end;
		int j     = 0;
		int k     = 0;

		String[][] salida = new String[i_end - i_ini][Ndelim];
		try
		{
			entrada = new BufferedReader( new FileReader( filename ) );
			linea   = entrada.readLine();

			if( i_ini > 0 )
			{
				for( j=0 ; j <i_ini ; j++ ) linea = entrada.readLine();
				/*{
					linea = entrada.readLine();
					/////
					if(linea==null||linea.trim().equals(""))
						linea = entrada.readLine();
					if( linea == null )
						continue;
						//break;
				}*/
			}

			for( j=0 ; j<( i_end - i_ini ) ; j++ ){
				linea = entrada.readLine();
				//////
				/*if( linea.trim().equals(""))
					linea = entrada.readLine();
				if( linea == null )
					break;*/
				for( k=0 ; k < Ndelim ; k++ )
				{
					try
					{
						salida[j][k] = EIGlobal.BuscarToken(linea , ';' , k+1  );
					}
					catch(Exception e)
					{
						salida[j][k]=" ";
           			}
				}
			}//Fin for
			entrada.close();
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Excepci�n en obtenRegistrosArchivo() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
		}
		String[][] straux = new String[i_end - i_ini][Ndelim];

		for( j = 0; j < (i_end - i_ini); j++ )
		{
			for( k = 0; k < Ndelim ; k++ )
			{
				straux[j][k] = salida[j][k];
			}
		}
		return straux;
	}

	/**
	 * Arma los renglones de la bitacora.
	 *
	 * @param USUARIO El objeto: usuario
	 * @param registros El objeto: registros
	 * @param numero_registros El objeto: numero_registros
	 * @param req El objeto: req
	 * @return Objeto string
	 */
	public String armaRenglones (String USUARIO, String[][] registros ,int numero_registros,HttpServletRequest req )
	{
		String hora				   = "";
		String renglon			   = "";
		String estilo			   = "";
		String estatus			   = "";
		String origen              = "";
		String descripcion		   = "";
		String comprobante		   = "";
		String lc		   			= "";
		String[] datos_comprobante = new String[14];

		StringBuffer renglon_ = new StringBuffer();

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		double Cargo_Abono = 0.0;


		for( int j = 0; j < numero_registros; j++)
		{
			if( ( j % 2 ) != 0 )	estilo = "		<td class=textabdatcla ";
			else					estilo = "		<td class=textabdatobs ";

			//renglon += "		<tr>\n";
			renglon_.append("		<tr>\n");

			// fecha
			if( registros[j][5].trim().length() > 0 )
			{
				//renglon += estilo + "nowrap align=center>" + registros[j][5] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("nowrap align=center>");
				renglon_.append(registros[j][5]);
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "nowrap align=center>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("nowrap align=center>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[0] = "6";
			datos_comprobante[5] = registros[j][5];

			// Hora
			if( registros[j][7].trim().length()>0 )
			{
				if ( registros[j][7].trim().length() == 3 )
					hora = "0" + registros[j][7].substring( 0,1 ) + ":" + registros[j][7].substring ( 1, 3);
				else
					hora = registros[j][7].substring( 0,2 ) + ":" + registros[j][7].substring ( 2, 4);

					//renglon += estilo + "align=center>" + hora + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center>");
					renglon_.append(hora);
					renglon_.append("</td>\n");
			}else
			{
				//renglon += estilo + "align=center>&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center>&nbsp;&nbsp;</td>\n");

			}

			datos_comprobante[6] = hora ;
			// Descripci�n
			if (registros[j][12].trim().length() > 0 )
			{
				descripcion = registros[j][12].trim();
				EIGlobal.mensajePorTrace( "[>-----------------------------------------------------------------------]", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "ConsultaBitacora - armaRenglones - descripcion: [" + descripcion + "]", EIGlobal.NivelLog.INFO);

				//renglon += estilo + "algin=left>" + descripcion + "</td>\n";

				// JVL-23/12/2005 Modifico para que siempre ponga la descripcion de la operacion
				// tomandola del Base Servlet, solo para el tipo de Operacion PIMP-Pago de Impuestos
				// Indistintamente de que el campo Descripcion (Concepto) traiga algo, solo para la pantalla
				if("PIMP".equals(registros[j][1].trim()) || "PILC".equals(registros[j][1].trim()) ||
				   "ACRR".equals(registros[j][1].trim()) || "CTCR".equals(registros[j][1].trim()) ||
				   "LOPC".equals(registros[j][1].trim()) || "SULC".equals(registros[j][1].trim()))
				 {
 				  descripcion = Description( registros[j][1].trim() );
				  if(descripcion.length()  >  0)
				   {
				    renglon_.append(estilo);
				    renglon_.append("nowrap align=left>");
				    renglon_.append(descripcion);
				    renglon_.append("</td>\n");
				   }
				  else
				   {
				    renglon_.append(estilo);
				    renglon_.append("nowrap align=left>&nbsp;&nbsp;&nbsp;</td>\n");
				   }
				 }
				else if (ES_PAGO_MICROSITIO_MANC.equals(registros[j][1].trim()) || "MMIC".equals(registros[j][1].trim()) ||
				   "VMIC".equals(registros[j][1].trim()) || "AMIC".equals(registros[j][1].trim()))
				{
					 descripcion = Description( registros[j][1].trim() );
					  if(descripcion.length()  >  0)
					   {
					    renglon_.append(estilo);
					    renglon_.append("nowrap align=left>");
					    renglon_.append(descripcion);
					    renglon_.append("</td>\n");
					   }
					  else
					   {
					    renglon_.append(estilo);
					    renglon_.append("nowrap align=left>&nbsp;&nbsp;&nbsp;</td>\n");
					   }
				}
				// JVL-23/12/2005 Fin cambio
				else if (ES_PAGO_SATMICROSITIO_MANC.equals(registros[j][1].trim()) || "MMRF".equals(registros[j][1].trim()) ||
						   "VMRF".equals(registros[j][1].trim()) || "PARF".equals(registros[j][1].trim()))
						{
							 descripcion = Description( registros[j][1].trim() );
							  if(descripcion.length()  >  0)
							   {
							    renglon_.append(estilo);
							    renglon_.append("nowrap align=left>");
							    renglon_.append(descripcion);
							    renglon_.append("</td>\n");
							   }
							  else
							   {
							    renglon_.append(estilo);
							    renglon_.append("nowrap align=left>&nbsp;&nbsp;&nbsp;</td>\n");
							   }
						}
				else
				{
				  renglon_.append(estilo);
				  renglon_.append("algin=left>");
				  renglon_.append(descripcion);
				  renglon_.append("</td>\n");
			    }
			}
			else  // si la referencia esta vacia
			{
				descripcion = Description( registros[j][1].trim() );
	            if(ES_PAGO_SUA_LINEA_CAPTURA.equals(registros[j][1].trim())){
	            	descripcion = "Pago S.U.A. l&iacute;nea de Captura";
	            }
				if(descripcion.length()  >  0)	{
				//renglon += estilo + "align=left>" + descripcion + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("nowrap align=left>");
					renglon_.append(descripcion);
					//renglon_.append("nowrap align=left>&nbsp;&nbsp;&nbsp;</td>\n");
					renglon_.append("</td>\n");
				}
				else
				{
					//descripcion = registros[j][1].trim();
					//renglon += estilo + "align=left>" + descripcion + "</td>\n";
					renglon_.append(estilo);
					//renglon_.append("nowrap align=left>");
					//renglon_.append(descripcion);
					renglon_.append("nowrap align=left>&nbsp;&nbsp;&nbsp;</td>\n");
					//renglon_.append("</td>\n");
				}
			}
			EIGlobal.mensajePorTrace( "[-->---------------------------------------------------------------------]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ConsultaBitacora - armaRenglones - descripcionSalir: [" + descripcion + "]", EIGlobal.NivelLog.INFO);


 		       // JVL-28/12/2005 Regreso la descripcion (concepto) original de tablas para el comprobante
                       // Solo para la operacion PIMP
            if("PIMP".equals(registros[j][1].trim())
            		|| "PILC".equals(registros[j][1].trim())
            		|| ES_PAGO_MICROSITIO_MANC.equals(registros[j][1].trim())
            		|| ES_PAGO_SATMICROSITIO_MANC.equals(registros[j][1].trim()) ){
            	 descripcion = registros[j][12].trim();
            }
 		       // JVL-28/12/2005 Fin cambio
			EIGlobal.mensajePorTrace( "[-------------------------------------------------------------<----------]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ConsultaBitacora - armaRenglones - descripcionRegreso: [" + descripcion + "]", EIGlobal.NivelLog.INFO);

			descripcion = descripcion.replace( ' ','+' );
			datos_comprobante[9] = descripcion; //jgal
			EIGlobal.mensajePorTrace( "[-----------------------------------<------------------------------------]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "ConsultaBitacora - armaRenglones - datos_comprobante[9]: [" + datos_comprobante[9] + "]", EIGlobal.NivelLog.INFO);

			datos_comprobante[10]= registros[j][1].trim();

			// Cuenta Retiro
			if( registros[j][2].trim().length() >0 )
			{
				//renglon += estilo + "align=left nowrap>" + registros[j][2] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>");
				renglon_.append(registros[j][2]);
				renglon_.append("</td>\n");
			}
			else
			{
				 //renglon += estilo + "align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[3] = registros[j][2];

			//Modificaci�n para la incidencia Q5616 JPPM
			if ("SIN CUENTA".equals(datos_comprobante[3]))
			{
				datos_comprobante[3]= "SIN+CUENTA";
			}


			// Cuenta Dep�sito
			if(registros[j][3].trim().length()>0)
			{
				//renglon += estilo + "align=left nowrap>" + registros[j][3] + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>");
				renglon_.append(registros[j][3]);
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=left nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[4] = registros[j][3];

			// Importe
			if(registros[j][4].trim().length()>0)
			{
				//renglon += estilo + "align=right nowrap>" + FormatoMoneda( registros[j][4].trim() ) + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=right nowrap>");
				try{
				renglon_.append(FormatoMoneda( registros[j][4].trim() ));
				} catch(Exception e){}
				renglon_.append("</td>\n");
			}
			else
			{
				//renglon += estilo + "align=right nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=right nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			datos_comprobante[2] = registros[j][4].trim();

			// Referencia
			if(registros[j][9].trim().length()>0)
			{
				datos_comprobante[1] = registros[j][9];
				datos_comprobante[8] = ObtenNombreUsuario(USUARIO,true,req);

				datos_comprobante[11]= registros[j][0];

   		                // JVL-28/12/2005 Formateo el usuario, para la operacion PIMP
   		                // para generar el comprobante
                if("PIMP".equals(registros[j][1].trim()) || "PILC".equals(registros[j][1].trim()) || ES_PAGO_SATMICROSITIO_MANC.equals(registros[j][1].trim())){
			         datos_comprobante[7] = con.cliente_7To8(registros[j][10].trim());
		                // JVL-28/12/2005 Fin cambio
                }
		        else
 				 datos_comprobante[7] = USUARIO;

                if(ES_PAGO_SUA_LINEA_CAPTURA.equals(registros[j][1].trim())){
                	datos_comprobante[12] = registros[j][14].trim();
                	datos_comprobante[13] = registros[j][12].trim();  // carga la linea de captura que treae en el concepto
                } else {
                	datos_comprobante[12] = "";
                }

				comprobante = armaComprobante( datos_comprobante );  // se arma el link para enviar el comprobante
				// VAO  18-09-20112
				//Si es Linea de Captura y esta mancomunada,  quita el Link para ajecutar la operacion
				if(ES_PAGO_SUA_LINEA_CAPTURA.equals(registros[j][1].trim())&& "M".equals( registros[j][0] ) ){ 
					comprobante = "";
				}

				if ( comprobante.length() > 1 ) {
					//renglon += estilo + "align=center nowrap>" + comprobante + registros[j][9].trim() + "</a></td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center nowrap>");
					renglon_.append(comprobante);
					if(ES_PAGO_SUA_LINEA_CAPTURA.equals(registros[j][1].trim())){
						if (registros[j].length > 14) {
							renglon_.append(registros[j][14].trim());
						} else {
							renglon_.append(registros[j][9].trim());
						}

					} else {
					renglon_.append(registros[j][9].trim());
					}
					renglon_.append("</a></td>\n");
				}
				else
				{
					//renglon += estilo + "align=center nowrap>" + registros[j][9].trim() + "</td>\n";
					renglon_.append(estilo);
					renglon_.append("align=center nowrap>");
					//renglon_.append(registros[j][9].trim());

				     //********************** Empieza codigo nuevo HELG-Getronics
					if("DIPD".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
					    renglon_.append("<A  href=\"javascript:fDetalle_divisa('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
					}else if("DITA".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
					    renglon_.append("<A  href=\"javascript:fDetalle_internacional('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
					}else if("LOPC".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
						renglon_.append("<A  href=\"javascript:fDetalle_reutersfx('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
					}else{
					    renglon_.append(registros[j][9].trim());
					}//********************** Termina Codigo nuevo HELG

					renglon_.append("</td>\n");
				}
			}else
			{
				//renglon += estilo + "align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("nowrap align=center>");
				//renglon_.append("align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			     //********************** Empieza codigo nuevo HELG-Getronics
				if("DIPD".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
				    renglon_.append("<A  href=\"javascript:fDetalle_divisa('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
				}else if("DITA".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
				    renglon_.append("<A  href=\"javascript:fDetalle_internacional('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
				}else if("LOPC".equals(registros[j][1].trim()) && "A".equals( registros[j][0] )){
					renglon_.append("<A  href=\"javascript:fDetalle_reutersfx('").append(registros[j][5].trim()).append("','").append(registros[j][9].trim()).append("');\">").append(registros[j][9].trim()).append("</A>");
				}else{
				    renglon_.append(registros[j][9].trim());
				}	//********************** Termina Codigo nuevo HELG
				renglon_.append("</td>\n");
			}

			// Usuario
			if(registros[j][10].trim().length()>0) {
				//renglon += estilo + "align=center nowrap>" + registros[j][10].trim() + "</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				//Modificacion de cliente7t08 15/03/2004 cgc.
				//renglon_.append(registros[j][10].trim());
				if("99999999".equals(registros[j][10].trim())){
					renglon_.append("Contingencia");
				}else{
					renglon_.append(con.cliente_7To8(registros[j][10].trim()));
				}
				//renglon_.append(con.cliente_7To8(registros[j][10].trim()));
				//fin de la modificacion cgc.
				renglon_.append("</td>\n");

			}
			else
			{
				//renglon += estilo + "align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>&nbsp;&nbsp;&nbsp;</td>\n");
			}

			// Estatus
			if      ( "A".equals( registros[j][0] ) ){ estatus = "Aceptado";}
			else if ( "R".equals( registros[j][0] ) ){ estatus = "Rechazado";}
			else if ( "M".equals( registros[j][0] ) ){ estatus = "Mancomunado";}
			else if ( "P".equals( registros[j][0] ) ){ estatus = "Programado";}
			else if ( "N".equals( registros[j][0] ) ){ estatus = "Pendiente";}
			else if ( "V".equals( registros[j][0] ) ){ estatus = "Verificado";}
			else if ( "T".equals( registros[j][0] ) ){ estatus = "Pre Autorizado";}
			else estatus = "No catalogado";
			//renglon += estilo + "align=center nowrap>" + estatus + "</td>\n";

				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(estatus);
				renglon_.append("</td>\n");

            // Origen de operaci�n
			try{
				if(registros[j][13] ==null){
					registros[j][13] = "";
				}
			} catch(Exception e) {

				origen = "No catalogado"; //cambio
				//renglon += estilo + "align=center nowrap>" + origen + "</td>\n"; //cambio
				//renglon += "	</tr>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(origen);
				renglon_.append("</td>\n");
				renglon_.append("	</tr>\n");
				continue;
			}
             if      ( "EWEB".equals( registros[j][13].trim() ) ){ 
            	 origen = "Web";//cambio
             }
             else if ( "TCT".equals( registros[j][13].trim() ) ) {
            	 origen = "Tradicional"; //cambio
             } else if ( "MOVI".equals( registros[j][13].trim() ) )  origen = "M�vil"; //cambio
	  //      if      ( registros[j][13].trim().equals( "EWEB" ) ) origen = "Enlace";//cambio
      //      else if ( registros[j][13].trim().equals( "TCT" ) )  origen = "Enlace"; //cambio
            else { 
            	origen = "No catalogado"; //cambio
            }
            //renglon += estilo + "align=center nowrap>" + origen + "</td>\n"; //cambio
			//renglon += "	</tr>\n";
				renglon_.append(estilo);
				renglon_.append("align=center nowrap>");
				renglon_.append(origen);
				renglon_.append("</td>\n");
				renglon_.append("	</tr>\n");

			if(ES_PAGO_SUA_LINEA_CAPTURA.equals(registros[j][1].trim())){
	           datos_comprobante[12] = registros[j][14].trim();
	           datos_comprobante[13] = registros[j][12].trim();  // carga la linea de captura que treae en el concepto
	           EIGlobal.mensajePorTrace("VAO >>Carga LC >>ConsultaBitacora.armaRenglones>>"+datos_comprobante[13]+"--",EIGlobal.NivelLog.INFO);
	        }
//				  BIT CU1031
				/*
				 * 02/ENE/07
				 * VSWF-BMB-I
				 */
				  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
					try{
			        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EC_BITACORA_CONSULTA_BIT_OPERACIONES);
						if(session.getContractNumber()!=null){
							bt.setContrato(session.getContractNumber());
						}
						if(session.getContractNumber()!=null && session.getUserID8()!=null){
							bt.setNombreArchivo(session.getContractNumber()+session.getUserID8()+ "bit.doc");
						}
						if(registros[j][1]!=null && !registros[j][1].equals("")){
							bt.setServTransTux(registros[j][1].trim());
						}
						if(registros[j][9]!=null&&registros[j][9].trim().length()>0&&(Long.parseLong(registros[j][9].trim()))>=0){
							bt.setReferencia(Long.parseLong(registros[j][9].trim()));
						}
						if(registros[j][4]!=null&&registros[j][4].trim().length()>0){
							bt.setImporte(Double.parseDouble(registros[j][4].trim()));
						}
						if(datos_comprobante[3]!=null){
							if(datos_comprobante[3].length()>20){
								bt.setCctaOrig((datos_comprobante[3]).substring(0,20));
							}else{
								bt.setCctaOrig(datos_comprobante[3].trim());
							}
						}
						if(datos_comprobante[4]!=null){
							if(datos_comprobante[4].length()>20){
								bt.setCctaDest((datos_comprobante[4]).substring(0,20));
							}else {
								bt.setCctaDest(datos_comprobante[4].trim());
							}
						}
						if(registros[j][0]!=null&& !registros[j][0].equals("")){
							bt.setEstatus(String.valueOf(registros[j][0]));
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						EIGlobal.mensajePorTrace("VAO >>Carga LC >>ConsultaBitacora.armaRenglones>>"+e.getMessage(),EIGlobal.NivelLog.INFO);

					}catch(Exception e){
						EIGlobal.mensajePorTrace("VAO >>Carga LC >>ConsultaBitacora.armaRenglones>>"+e.getMessage(),EIGlobal.NivelLog.INFO);

					}
				  }
				/*
				 * 02/ENE/07
				 * VSWF-BMB-F
				 */
		} // for ( j )
		return renglon_.toString();
		//return renglon;
	}


	/**
	 * Genera los comprobantes.
	 *
	 * @param registro El objeto: registro
	 * @return Objeto string
	 */
	public String armaComprobante( String[] registro )
	{
		String link	= " ";
		// ### Modificacion para el estado fiscal ...
		String concepto = "";
		String rfc      = "";
		String iva      = "";


		int rfcIn = -1;
		int ivaIn = -1;
		EIGlobal.mensajePorTrace( "[>>-----------------------------------------------------------------------]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConsultaBitacora - armaComprobante - registro[9]: [" + registro[9] + "]", EIGlobal.NivelLog.INFO);
		// tipo de operaci�n y estatus . descipci�n de cheques seguridad INGC y para archivo INGA tipo operaci�n = CHSE
		if ( ( "DIBT".equals(registro[10]) || "TRAN".equals(registro[10]) || "PAGT".equals(registro[10]) ||
				FielConstants.BITA_FIEL_ALTA.equals(registro[10]) || FielConstants.BITA_FIEL_MODIFICA.equals(registro[10]) || FielConstants.BITA_FIEL_BAJA.equals(registro[10]) ||
			   "CPDI".equals(registro[10]) || "CPID".equals(registro[10]) || "TRTJ".equals(registro[10]) ||
			   "CPDV".equals(registro[10]) || "RG03".equals(registro[10]) || ES_PAGO_MICROSITIO_MANC.equals(registro[10]) )
			   && "A".equals( registro[11] ) ) //Agregado RG03 para IM314336 - NVS - Praxis - 050518
		{
			concepto = registro[9];
			rfcIn=concepto.lastIndexOf("RFC");
			ivaIn=concepto.lastIndexOf("IVA");

			if(rfcIn>=30)
			 {
			  if(ivaIn>=30) {
				  try {
				  	rfc=concepto.substring(concepto.lastIndexOf("RFC")+4,concepto.lastIndexOf("IVA")).trim();
				   	iva=concepto.substring(concepto.lastIndexOf("IVA")+4,concepto.length()).trim();
					//concepto = concepto.substring(0,concepto.indexOf(","));
				  } catch(StringIndexOutOfBoundsException e) {
					EIGlobal.mensajePorTrace( "Error al procesar IVA o RFC para armaComprobante, Cadena que genero excepcion = <" + concepto + ">", EIGlobal.NivelLog.ERROR);
					rfc = "";
					iva = "";
				  }
			 }
			 else
				{
				  try {
				    rfc=concepto.substring(concepto.lastIndexOf("RFC")+4,concepto.length()).trim();
				    iva="";
			      } catch(StringIndexOutOfBoundsException e) {
					EIGlobal.mensajePorTrace("Error al procesar RFC para armaComprobante, Cadena que genero excepcion = <" + concepto + ">", EIGlobal.NivelLog.ERROR);
					rfc = "";
					iva = "";
				  }
				}
				 concepto=concepto.substring(0,concepto.lastIndexOf("RFC"));
			}
			link ="<a href=\"javascript:VentanaComprobante(";
			link += "'"+ registro[0] + "',"; // tipo
			link += "'"+ registro[1] + "',"; // referencia
			link += "'"+ registro[2] + "',"; // importe
			link += "'"+ registro[3] + "',"; // cuenta origen
			link += "'"+ registro[4] + "',"; // cuenta destino
			link += "'"+ registro[5] + "',"; // fecha
			link += "'"+ registro[6] + "',"; // hora
			link += "'"+ registro[7] + "',"; // usuario
			link += "'"+ registro[8] + "',"; // descripci�n de usuario
			link += "'"+ concepto + "',";    // descripci�n de operaci�n

			link += "'"+ rfc + "',";         // descripci�n de usuario
			link += "'"+ iva + "');";        // descripci�n de usuario

			//link += "'"+ registro[9] + "');";// descripci�n de operaci�n
			link += "\">";
		}

                // JVL-28/12/2005 Agrego condicion para que la referencia de la operacion PIMP
                // tambien tenga link para generar comprobante
		if( ("PIMP".equals(registro[10]) || "PILC".equals(registro[10]) || ES_PAGO_SATMICROSITIO_MANC.equals(registro[10])) && "A".equals(registro[11]) )
		{
			link ="<a href=\"javascript:VenComNvoPI(";
			/*
			  * Mejoras de Linea de Captura
			  * Se modifica la posicion que se regresa ante 1 ahora 10, para
			  * despu�s indicar que comprobante se tomara
			  * Inicia RRR - Indra Marzo 2014
			  */
			link += "'"+ registro[10] + "',"; // tipo
			/*
		      * Fin RRR - Indra Marzo 2014
		      */
			link += "'"+ registro[1] + "',"; // referencia
			link += "'"+ FormatoMoneda(registro[2]).replace(' ','+') + "',"; // importe
			link += "'"+ registro[3] + "',"; // cuenta origen
			link += "'"+ registro[5] + "',"; // fecha
			link += "'"+ registro[6] + "',"; // hora
			link += "'"+ registro[7] + "',"; // usuario
			link += "'"+ registro[8] + "',"; // descripci�n de usuario
			link += "'"+ registro[9] + "');";// descripci�n de operaci�n
			link += "\">";
		}
                // JVL-28/12/2005 Fin cambio

		// VSWF - JGAL - SIPARE 03/09/2009 Se agrega comprobante Linea de Captura SUA
		if( ES_PAGO_SUA_LINEA_CAPTURA.equals(registro[10]))
		{
			EIGlobal.mensajePorTrace("VAO Consultabitacora.armaComprobante LC>>"+registro[13]+"<<" ,EIGlobal.NivelLog.INFO);
			//Poner campo proteccion1 una vez que sea devuelto por el back
			link ="<a href=\"javascript:VentanaComprobanteSUALC(";
			link += "'"+ registro[12].trim() + "','"+ registro[13]+"')"; // referencia -
			link += "\">";
			/*Se cambia para que envie Todos los parametros de consulta Referencia, fecha, hora, importe*/
		/*
			link = "<a href=\"javascript:window.open('ConsultaBitacora?opcionLC=1&folioSUA=" + registro[1] + "','Impresion'," +
					"'toolbar=no,location=no,directories=no,status=no,menubar=no," +
					"scrollbars=yes,resizable=no,width=440,height=290\');\">"; */
		}

/*			link ="<a href=\"javascript:VenComNvoPI(";
			link += "'"+ registro[0] + "',"; // tipo
			link += "'"+ registro[1] + "',"; // referencia
			link += "'"+ FormatoMoneda(registro[2]).replace(' ','+') + "',"; // importe
			link += "'"+ registro[3] + "',"; // cuenta origen
			link += "'"+ registro[5] + "',"; // fecha
			link += "'"+ registro[6] + "',"; // hora
			link += "'"+ registro[7] + "',"; // usuario
			link += "'"+ registro[8] + "',"; // descripci�n de usuario
			link += "'"+ registro[9] + "');";// descripci�n de operaci�n
			link += "\">";
		}

		/*
		 *   window.open("SUALineaCapturaServlet?opcionLC=2","Impresion","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
		 */

        // JVL-28/12/2005 Fin cambio

		return link ;
	}


/**
 * Obtiene los parametros de la consulta.
 *
 * @param fechas El objeto: fechas
 * @param regreso El objeto: regreso
 * @param CUENTA El objeto: cuenta
 * @param USUARIO El objeto: usuario
 * @param impo El objeto: impo
 * @param REFERENCIA El objeto: referencia
 * @param MISPARAMETROS El objeto: misparametros
 * @param CONTRATO El objeto: contrato
 * @param SEMAFORO El objeto: semaforo
 * @param req El objeto: req
 * @return Objeto int
 */
	public int obtenParametros(String [] fechas,boolean [] regreso,String [] CUENTA,String [] USUARIO,String [] impo,
										String [] REFERENCIA,String MISPARAMETROS	,String [] CONTRATO, String SEMAFORO[],HttpServletRequest req){
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
			int salida					= 0;
			int size	= 16;
		String parametros[]			= new String[size];
        StringTokenizer parametro	= new StringTokenizer(MISPARAMETROS,";");
		String FECHAFIN ="";
		String FECHAINI ="";

	boolean TRANSFERENCIAS_	= false;
	boolean INVERSIONES_	= false;
	boolean INTERBANCARIAS_	= false;
	boolean LINCAPSUA_		= false;
	boolean ACEPTADAS_		= false;
	boolean RECHAZADAS_		= false;
	boolean PROGRAMADAS_	= false;
	boolean PENDIENTES_		= false;

		try{
			for( int k=0 ; k<size ; k++ ) parametros[k] = parametro.nextToken();

			CONTRATO [0]		= parametros[0];
			USUARIO [0]		= parametros[1];
			CUENTA[0]			= parametros[2];
			//IMPORTE			= parametros[3];
			impo [0]			= parametros[3];
			impo [1]			= parametros[4];
			REFERENCIA [0]	= parametros[5];
			if( "0".equals(CUENTA[0]) ){
				CUENTA[0] = " ";
			}

			if( !"false".equals(parametros[6] ) ) { TRANSFERENCIAS_ =true; SEMAFORO[1] = "1"; }
			if( !"false".equals(parametros[7] ) ) { INVERSIONES_ =true; SEMAFORO[2] = "1"; }
			if( !"false".equals(parametros[8] ) ) { INTERBANCARIAS_ =true; SEMAFORO[3] = "1"; }
			if( !"false".equals(parametros[9] ) ) { LINCAPSUA_ =true; SEMAFORO[4] = "1"; }
			if( !"false".equals(parametros[10] ) ) { ACEPTADAS_ =true; SEMAFORO[5] = "1"; }
			if( !"false".equals(parametros[11] ) ) { RECHAZADAS_ =true; SEMAFORO[6] = "1"; }
			if( !"false".equals(parametros[12] )) { PROGRAMADAS_ =true; SEMAFORO[7] = "1"; }
			if( !"false".equals(parametros[13] )) { PENDIENTES_ =true; SEMAFORO[8] = "1"; }


			regreso[7] = TRANSFERENCIAS_;
			regreso[6] = INVERSIONES_;
			regreso[5] = INTERBANCARIAS_;
			regreso[4] = LINCAPSUA_;
			regreso[3] = ACEPTADAS_;
			regreso[2] = RECHAZADAS_;
			regreso[1] = PROGRAMADAS_;
			regreso[0] = PENDIENTES_;



			FECHAINI		= parametros[14];
			FECHAFIN		= parametros[15];

			fechas[0] =FECHAINI;
			fechas[1] =FECHAFIN;

			session.setfechaini( FECHAINI );
			session.setfechafin( FECHAFIN );

		}catch ( Exception ioe ){
			salida = 1; // error en los datos
		}
		return salida;
	}


	/**
	 * Retorna el total de registros.
	 *
	 * @param ERROR El objeto: error
	 * @param filename El objeto: filename
	 * @return Objeto int
	 */
	public int totalRegistros(String [] ERROR, String filename )
	{
		int registros = 0;
		String linea = null;

		try{
			BufferedReader entrada= new BufferedReader(new FileReader(filename));
			linea = entrada.readLine();

			if ( linea.startsWith( "OK" ) ){
				while( ( linea = entrada.readLine() )!=null ){
					if( "".equals(linea.trim())||linea==null){
						linea = entrada.readLine();
					}
					if( linea == null ){
						//break;
						continue;
					  }
					registros++;
				}
			}else{
				// archivo sin registros
				ERROR[0] = linea.substring(16);
				EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &no hay registros en el archivo ...&", EIGlobal.NivelLog.DEBUG);
			}
			entrada.close();
		}catch(Exception e){
			return 0; // error
		}
		return registros;
	}

	/**
	 * Formatea Contrato.
	 *
	 * @param contrato_entrada El objeto: contrato_entrada
	 * @return Objeto string
	 */
	String FormateaContrato (String contrato_entrada)
	{
		String salida	= "";
		String caracter	= "";

		for ( int i = 0 ; i < contrato_entrada.length() ; i++ ){
			caracter = contrato_entrada.substring( i, i+1 );
			if ( !"-".equals(caracter) ){
				salida += caracter;
			}
		}
		return salida;
	}


	/**
	 * Exporta Resultado de la consulta.
	 *
	 * @param nombreArchivo El objeto: nombre archivo
	 * @param archivo_entrada El objeto: archivo_entrada
	 * @param archivoSalida El objeto: archivo salida
	 * @param numero_lineas El objeto: numero_lineas
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @return Objeto int
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public int exportarArchivo(String nombreArchivo, String archivo_entrada, String archivoSalida, int numero_lineas,
			HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		BufferedReader entrada;

		int i = 0;

		String[] renglon		= null;
		String linea			= "";
		String linea_exportar	= "";

		EI_Exportar archivo_exportar = new EI_Exportar( archivoSalida );
		ArchivoRemoto archR=new ArchivoRemoto();
		archivo_exportar.creaArchivo();

		try{
			entrada	= new BufferedReader( new FileReader( archivo_entrada ) );
			linea	= entrada.readLine();

			for( i = 0; i < numero_lineas; i++ ){
				linea = entrada.readLine();

				if( "".equals(linea.trim())){
					linea = entrada.readLine();
				}
				if( linea == null ){
					break;
				}
				renglon = desentrama( linea, ';' );
				linea_exportar = armaRenglonExportar( renglon );
				linea_exportar = formateaAcentos(linea_exportar);
				archivo_exportar.escribeLinea( linea_exportar );

			} // for ( i )
			archivo_exportar.cierraArchivo();
			entrada.close();

			// Doble remote shell
			archR.copiaLocalARemoto( nombreArchivo ,"WEB");
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &Ejecutando copia local a remoto...&", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  HELG &Ejecutando copia local a remoto...&", EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Excepci�n en exportarArchivo() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
			req.setAttribute( "Output1","<h3>No hay registros asociados a su consulta !");
			req.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
			req.setAttribute( MENU_PRINCIPAL, session.getStrMenu());
			req.setAttribute( ENCABEZADO, CreaEncabezado( CONSULTA_DE_BITACORA,CONSULTAS_BITACORA, S25320H,req) );
			req.setAttribute( MENSAJE, "No hay registros asociados a su consulta !"  );
			evalTemplate( EI_MENSAJEDOS, req, res );
			return 1;
		} // try
		return 0;
	} // m�todo


	/**
	 * Formatea acentos.
	 *
	 * @param linea El objeto: linea
	 * @return Objeto string
	 */
	private String formateaAcentos(String linea) {
		linea = linea.replace("&aacute;", "�");
		linea = linea.replace("&eacute;", "�");
		linea = linea.replace("&iacute;", "�");
		linea = linea.replace("&oacute;", "�");
		linea = linea.replace("&uacute;", "�");
		linea = linea.replace("&Aacute;", "�");
		linea = linea.replace("&Eacute;", "�");
		linea = linea.replace("&Iacute;", "�");
		linea = linea.replace("&Oacute;", "�");
		linea = linea.replace("&Uacute;", "�");
		return linea;
	}

	/**
	 * Arma los renglones a exportar.
	 *
	 * @param registro registro
	 * @return String renglones
	 */
	public String armaRenglonExportar ( String[] registro ){
		String hora			= "";
		String renglon		= "";
        String origen      = "";//cambio
		String estatus		= "";
		String descripcion	= "";


/*
try{

for(int r=0;r<registro.length;r++)
}catch(Exception e) {e.printStackTrace();}
*/

			// fecha
			if( registro[5].trim().length() > 0 ){
				renglon += registro[5] + ";";
			}
			else{
				renglon += " ;";
			}

			// Hora

			if( registro[7].trim().length()>0 ){
				if ( registro[7].trim().length() == 3 ){
					hora = "0" + registro[7].substring( 0,1 ) + ":" + registro[7].substring ( 1, 3);
				}
				else{
					hora = registro[7].substring( 0,2 ) + ":" + registro[7].substring ( 2, 4);
				}
				renglon += hora + ";";
			}
			else{ 
				renglon += " ;";
			}

			// Descripci�n
			if(registro.length>12)
				if (registro[12].trim().length() > 0 )
				{
				 // JVL-23/12/2005 Modifico para que siempre ponga la descripcion de la operacion
				 // tomandola del Base Servlet, solo para el tipo de Operacion PIMP-Pago de Impuestos
				 // Indistintamente de que el campo Descripcion (Concepto) traiga algo, solo para la pantalla
				 if ("PIMP".equals(registro[1].trim()) || "PILC".equals(registro[1].trim()) || ES_PAGO_SATMICROSITIO_MANC.equals(registro[1].trim()))
				  {
				   descripcion = Description( registro[1].trim() );
				   if(descripcion != null)
				    renglon += descripcion + ";";
				   else
				    renglon += registro[1].trim() + ";";
				  }
				 // JVL-23/12/2005 Fin cambio
				 else
				  {
				   renglon += registro[12] + ";";
				   descripcion = registro[12];
				  }
				}
				else
				{
				  descripcion = Description( registro[1].trim() );
				  if(descripcion != null)
				   renglon += descripcion + ";";
				  else
				   renglon += registro[1].trim() + ";";
				}

			// Cuenta Retiro
			if( registro[2].trim().length() >0 ){
			registro[2] =registro[2] + completaCampos (registro[2].length(), 20);
			renglon += registro[2] + ";";
			} else{
				renglon += "                    ;";
			}


			// Cuenta Dep�sito
			if(registro[3].trim().length()>0) {
			registro[3] = registro[3] + completaCampos (registro[3].length(), 20);
			renglon += registro[3] + ";";
			}	else{
				renglon += "                    ;";
			}


			// Importe
			if(registro[4].trim().length()>0){
				renglon += FormatoMoneda( registro[4].trim() ) + ";";
			}
			else{
				renglon += " ;";
			}

			// Referencia
			if(registro[9].trim().length()>0){
				renglon += registro[9].trim() + ";";
			}
			else{
				renglon += " ;";
			}

			// Usuario
			if(registro[10].trim().length()>0){
			        //Modificaci�n para usuario 7to8 15/03/2004 cgc.
				//renglon += registro[10].trim() + ";";
				renglon +=con.cliente_7To8(registro[10].trim()) + ";";
				//fin de la modificaci�n.
			}
			else{
				renglon += " ;";
			}

			// Estatus
			if ( "A".equals( registro[0] ) ){ estatus = "Aceptado;";}
			else if ( "R".equals( registro[0] ) ){ estatus = "Rechazado;";}
			else if ( "M".equals( registro[0] ) ){ estatus = "Mancomunado;";}
			else if ( "P".equals( registro[0] ) ){ estatus = "Programado;";}
			else if ( "N".equals( registro[0] ) ){ estatus = "Pendiente;";}
			else if ( "V".equals( registro[0] ) ){ estatus = "Verificado";}
			else if ( "T".equals( registro[0] ) ){ estatus = "Pre Autorizado";}
			else {estatus = "No catalogado;";}
			renglon += estatus;

            // Origen de operaci�n
			if(registro.length>13) {
				if      ( "EWEB".equals( registro[13].trim() ) ) {
					origen = "Web;"; //cambio
				}
				else if ( "TCT".equals( registro[13].trim() ) ){
					origen = "Tradicional;"; //cambio
				}
				else if ( "MOVI".equals( registro[13].trim() ) ){
				    origen = "M�vil;"; //cambio
				}
				else{
					origen = "No catalogado;"; //cambio
				}
			} else{
				origen = "No catalogado";
			}
            renglon += origen ; //cambio
            if (ES_PAGO_SUA_LINEA_CAPTURA.equals(registro[1].trim())
            		&& registro.length > 14) {
            	if (!"".equals(registro[14].trim())) {
            		if (renglon.charAt(renglon.length()-1)!=';') {
            			renglon += ";" + registro[14].trim();
            		} else {
            			renglon += registro[14].trim();
            		}
            	}
            } else {
            	renglon += ";";
            }
			renglon += "\r\n";

		return renglon;
	} // m�todo

	/**
	 * Formate el importe a formato moneda.
	 *
	 * @param cantidad importe
	 * @return String importe en moneda
	 */
	public String FormatoMoneda ( String cantidad ) {
	 String language = "la"; // ar
	 String country = "MX";  // AF
	 Locale local = new Locale (language,  country);
	 NumberFormat nf = NumberFormat.getCurrencyInstance (local);
	 String formato = "";
	 if(cantidad ==null ||"".equals (cantidad)){
		 cantidad="0.0";
	 }
	 double importeTemp = 0.0;
	 importeTemp = new Double (cantidad).doubleValue ();
	 if (importeTemp < 0) {
		 try {
		 formato = nf.format (new Double (cantidad).doubleValue ());
		 if (!("$".equals (formato.substring (0,1))))
			 if ("MXN".equals (formato.substring (0,3))){
				 formato ="$ -"+ formato.substring (4,formato.length ());
			 }
			 else{
				 formato ="$ -"+ formato.substring (5,formato.length ());
			 }
		 } catch(NumberFormatException e) {formato="$0.00";}
	 } else {
		 try {
		 formato = nf.format (new Double (cantidad).doubleValue ());
		 if (!("$".equals (formato.substring (0,1))))
			 if ("MXN".equals (formato.substring (0,3))){
				 formato ="$ "+ formato.substring (4,formato.length ());
			 }
			 else{
				 formato ="$ "+ formato.substring (1,formato.length ());
				 }
		 } catch(NumberFormatException e) {
		 formato="$0.00";}
	 } // del else
	 if(formato==null)
		 formato = "";
	 return formato;
	}

/**
 * completa campos.
 *
 * @param longitudActual longitudActual
 * @param longitudRequerida longitudRequerida
 * @return String valor
 */
public String completaCampos(int longitudActual, int longitudRequerida){
		//EIGlobal.mensajePorTrace("***.class &Entrando al metodo completaCampos()&", EIGlobal.NivelLog.INFO);
	    String blancos =" ";
	    int faltantes= longitudRequerida-longitudActual;
	    for (int i=1; i<faltantes; i++){
		 blancos=blancos+" ";
	    }
		if(faltantes>0)
		    return blancos;
		else
			return "";
	}//fin del metodo completaCampos()


/**
 * Obtiene el nombre usuario operante.
 *
 * @param NumUsuario El objeto: num usuario
 * @param EspacioMas El objeto: espacio mas
 * @param req El objeto: req
 * @return Objeto string
 */
	public String ObtenNombreUsuario(String NumUsuario, boolean EspacioMas,HttpServletRequest req)
	{
		String NumUsuario2 = NumUsuario;
		try {

			String[][] ArregloUsuarios = TraeUsuarios(req);
			String Nombre = "";
			for(int i=1;i<=Integer.parseInt(ArregloUsuarios[0][0]);i++)
				if(ArregloUsuarios[i][1].equals(NumUsuario2))
				{
					Nombre = ArregloUsuarios[i][2].trim();
					break;
				}
			if(EspacioMas) Nombre = Nombre.replace(' ','+');
			EIGlobal.mensajePorTrace("Entrando al metodo ObtenNombreUsuario - Nombre: "+ Nombre, EIGlobal.NivelLog.INFO);
			return Nombre;
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error al traer Nombre de Usuario para " + NumUsuario, EIGlobal.NivelLog.ERROR);
			return "";
		}
    }

/**
 * Desaentrama datos.
 *
 * @param auxCad El objeto: aux cad
 * @param Separador El objeto: separador
 * @return Objeto string[]
 */
	public String[] desentrama(String auxCad, char Separador)
	{
		int NumSep = 0; // Variable que contiene el numero de separadores
		String[] Salida = null; // Arreglo de String donde se guardan los valores
								// Obtener n�mero de separadores

		if (auxCad.charAt((auxCad.length() - 1)) != Separador)
			auxCad += Separador;

			for ( int i = 0; i < auxCad.length(); i++ )
				if (auxCad.toCharArray()[i] == Separador)  NumSep++;

		// Almacenar valores en el arreglo de Strings
		try{
			Salida = new String[NumSep];
			for (int i = 0; i <= NumSep; i++){
				Salida[i] = auxCad.substring(0, auxCad.indexOf(Separador));
				auxCad = auxCad.substring((auxCad.indexOf(Separador) + 1), auxCad.length());
			}
		}catch (Exception e){
			//EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  Excepci�n en desentrama() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
		}
		return Salida;
	}

/**
 * Genera el calendario.
 *
 * @param strFecha strFecha
 * @return Calendar Calendario
 */
Calendar toCalendar(String strFecha) //cambio
  {
    Calendar fecha;
    String separador = "/";

     try
      {
        if (strFecha.substring(2, 3).equals(separador) && strFecha.substring(5, 6).equals(separador))
          {
            int dia  = Integer.parseInt(strFecha.substring(0, 2));
            int mes  = Integer.parseInt(strFecha.substring(3, 5)) - 1;
            int anio = Integer.parseInt(strFecha.substring(6, 10));
            fecha = new GregorianCalendar(anio, mes, dia);
         }
        else
          fecha = null;

      } catch(Exception e)
          {
           EIGlobal.mensajePorTrace( "***ChesConsulta.class Excepcion %toCalendar() >> "+ e.toString() + " <<", EIGlobal.NivelLog.INFO);
           fecha = null;
          }

      return fecha;

  }



/**
 * Obtiene divisa.
 *
 * @param req El objeto: req
 * @param res El objeto: res
 * @return Objeto int
 * @throws ServletException La servlet exception
 * @throws IOException Una excepcion de I/O ha ocurrido.
 */
	public int detalle_divisa(HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
	    EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &desplegando detalle ...&", EIGlobal.NivelLog.DEBUG);
	    HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
	    String usu_desc = "";
	    String usu_hora = "";
	    String fecha_mov = "";
	    String fecHoy = "";

	    String  query_realizar ="";
	    StringBuffer tabla = new StringBuffer();
	    ResultSet TestResult = null;
	    Statement TestQuery = null;
	    Connection connbd = null;
	    DecimalFormat formateo = new DecimalFormat("$ #,##0.00");

	    try{

  	      fecHoy = ObtenFecha(true);
	      if (req.getAttribute( FECHA_TRABAJAR) != null)
			fecha_mov = (String) req.getAttribute( FECHA_TRABAJAR);
	      fecha_mov = fecha_mov.substring( 0,2 ) + fecha_mov.substring( 3,5 ) + fecha_mov.substring( 6,10 );
	      fecHoy = fecHoy.substring( 0,2 ) + fecHoy.substring( 3,5 ) + fecHoy.substring( 6,10 );

		connbd = createiASConn(Global.DATASOURCE_ORACLE);
		if(connbd==null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
		    return 0;
		}

		query_realizar=" SELECT a.CONTRATO AS CONTRATO, a.USR AS USR, to_char(a.FCH_ENV,'dd/mm/yyyy') AS FCH_ENV, b.HORA AS HORA_ENV, " +
				 " a.NUM_ORDEN AS NUM_ORDEN, a.REF AS REF, a.EDOABONO AS EDOABONO, a.CONCEPTO AS CONCEPTO, a.CTA_CARGO AS CTA_CARGO, " +
				 " a.CTA_ABONO AS CTA_ABONO, a.TPO_CAMPES AS TPO_CAMPES, a.IMP_PESOS AS IMP_PESOS, a.IMP_DOLARES AS IMP_DOLARES " +
				 " FROM eweb_bita_inter a, tct_bitacora b " +
				 " WHERE a.REF = "+ req.getParameter("Ref") +
				 " AND a.FCH_ENV  >= to_date ('"+fecha_mov+"' || ' 000000','DDMMYYYY HH24MISS') " +
				 " AND a.FCH_ENV  <= to_date ('"+fecha_mov+"' || ' 235959','DDMMYYYY HH24MISS') " +
				 " AND b.REFERENCIA=a.REF";
				 if (!fecha_mov.equals(fecHoy))
				 {
				 	query_realizar +=	" AND b.FECHA  >= to_date ('"+fecha_mov+"' || ' 000000','DDMMYYYY HH24MISS') " +
				 	//" AND b.FECHA  <= to_date ('"+fecha_mov+"' || ' 235959','DDMMYYYY HH24MISS')";
				 	 " AND b.FECHA  <= to_date ('"+fecha_mov+"' || ' 235959','DDMMYYYY HH24MISS')" +
					 " AND      b.referencia = a.ref" +
					 " AND a.FCH_ENV = b.fecha " +
					 " AND  a.CONTRATO = '" + session.getContractNumber() + "'";

				 }

		EIGlobal.mensajePorTrace("ConsultaBitacora - detalle()-  query: [" + query_realizar + "]",EIGlobal.NivelLog.ERROR);
		TestQuery  = connbd.createStatement();
		if(TestQuery == null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Cannot Create Query",EIGlobal.NivelLog.ERROR);
		    return 0;
		}
		TestResult = TestQuery.executeQuery(query_realizar);
		if(TestResult == null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Cannot Create ResultSet",EIGlobal.NivelLog.ERROR);
		    return 0;
		}
		if(TestResult.next()){
		    tabla.append("<table width=380 border=0 cellspacing=2 cellpadding=3 background=\"/gifs/EnlaceMig/gau25010.gif\">\n");

		    //CONTRATO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right width=0>Contrato:</td>\n");
		    tabla.append("		<td class=textabcom align=left nowrap>").append(TestResult.getString(CONTRATO)!=null?TestResult.getString(CONTRATO):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //USUARIO
		    if (TestResult.getString("usr")!=null)
		    	usu_desc = TestResult.getString("usr") + " " + ObtenNombreUsuario(session.getUserID8(),false,req) +"</td>\n";
		    else
			usu_desc = "&nbsp;</td>\n";
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Usuario:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(usu_desc);
		    tabla.append("	</tr>\n");

		    //FECHA Y HORA
		    if (TestResult.getString("hora_env")!=null)
		    {
			if (TestResult.getString("hora_env").trim().length()==0)
				usu_hora = " 00:00 hrs.";
			else if (TestResult.getString("hora_env").trim().length()==1)
				usu_hora = " 00:0" + TestResult.getString("hora_env").trim() + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==2)
				usu_hora = " 00:" + TestResult.getString("hora_env").trim() + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==3)
				usu_hora = " 0" + TestResult.getString("hora_env").trim().substring(0,1) + ":" + TestResult.getString("hora_env").trim().substring(1,3) + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==4)
				usu_hora = " " + TestResult.getString("hora_env").trim().substring(0,2) + ":" + TestResult.getString("hora_env").trim().substring(2,4) + " hrs.";
		    }

		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Fecha y hora:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("fch_env")!=null?TestResult.getString("fch_env"):"&nbsp;").append(usu_hora).append("</td>\n");
		    tabla.append("	</tr>\n");

		    //NUMERO DE ORDEN
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right width=0>No. de Orden:</td>\n");
		    tabla.append("		<td class=textabcom align=left nowrap>").append(TestResult.getString("num_orden")!=null?TestResult.getString("num_orden"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //REFERENCIA
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Referencia:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("ref")!=null?TestResult.getString("ref"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //CONCEPTO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Concepto:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("concepto")!=null?TestResult.getString("concepto"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //CUENTA DE CARGO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Cuenta de cargo:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cta_cargo")!=null?TestResult.getString("cta_cargo"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //CUENTA DE ABONO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Cuenta abono/M&oacute;vil:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cta_abono")!=null?TestResult.getString("cta_abono"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //RFC BENEFICIARIO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>RFC Beneficiario:</td>\n");
		    tabla.append("	</tr>\n");

		    //TIPO DE CAMBIO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Tipo de Cambio:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("tpo_campes")!=null?TestResult.getString("tpo_campes"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IMPORTE MN
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Importe MN:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("imp_pesos")!=null?formateo.format(TestResult.getDouble("imp_pesos")):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IMPORTE USD
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Importe USD:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("imp_dolares")!=null?formateo.format(TestResult.getDouble("imp_dolares")):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IVA
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>IVA:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append("$ 0.00").append("</td>\n");
		    tabla.append("	</tr>\n");

			//ESTATUS Q6973 Getronics
			tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Estatus:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(Consulta_status(TestResult.getString("edoabono"))).append("</td>\n");
		    tabla.append("	</tr>\n");



		    tabla.append("</table>\n");
			//  BIT CU1151
			/*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
		    if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				try{
		        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_BITACORA_CONSULTA_BIT_DETALLE_DIV);
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					if((TestResult.getString("ref")!=null)&&
							(!"".equals(TestResult.getString("ref")))){
						bt.setReferencia(Long.parseLong(TestResult.getString("ref")));
					}
					if((TestResult.getString("tpo_campes")!=null)&&
							(!TestResult.getString("tpo_campes").equals(""))){
						bt.setTipoCambio(Double.parseDouble(TestResult.getString("tpo_campes")));
					}
					if((TestResult.getString("imp_pesos")!=null)&&
							(!TestResult.getString("imp_pesos").trim().equals(""))){
						bt.setImporte(Double.parseDouble(TestResult.getString("imp_pesos")));
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
		    }
			/*
			 * 02/ENE/07
			 * VSWF-BMB-F
			 */
		}

	    }catch(SQLException sqle){
		EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	    }finally{
		try{
		    TestResult.close();
		    TestQuery.close();
		    connbd.close();
		}catch(Exception e) {}
	    }
	    req.setAttribute( "tabla", tabla);
	    req.setAttribute( "ClaveBanco", session.getClaveBanco());
	    //evalTemplate(IEnlace.BITACORA_TMPL, req, res);
	    evalTemplate("/jsp/Detalle_Bita_Inter.jsp", req, res);
	    return 0;
	}




/**
 * Obtiene el detalle de operaciones internacionales.
 *
 * @param req El objeto: req
 * @param res El objeto: res
 * @return Objeto int
 * @throws ServletException La servlet exception
 * @throws IOException Una excepcion de I/O ha ocurrido.
 */
	public int detalle_internacional(HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
	    EIGlobal.mensajePorTrace( "***ConsultaBitacora.class  &desplegando detalle ...&", EIGlobal.NivelLog.DEBUG);
	    HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
	    String usu_desc = "";
	    String usu_hora = "";
	    String fecha_mov = "";
	    String fecHoy = "";

	    String  query_realizar ="";
	    StringBuffer tabla = new StringBuffer();
	    ResultSet TestResult = null;
	    Statement TestQuery = null;
	    Connection connbd = null;
	    DecimalFormat formateo = new DecimalFormat("$ #,##0.00");

	    try{
  	      fecHoy = ObtenFecha(true);
	      if (req.getAttribute( FECHA_TRABAJAR) != null)
			fecha_mov = (String) req.getAttribute( FECHA_TRABAJAR);
	      fecha_mov = fecha_mov.substring( 0,2 ) + fecha_mov.substring( 3,5 ) + fecha_mov.substring( 6,10 );
	      fecHoy = fecHoy.substring( 0,2 ) + fecHoy.substring( 3,5 ) + fecHoy.substring( 6,10 );

		connbd = createiASConn(Global.DATASOURCE_ORACLE);
		if(connbd==null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Error al intentar crear la conexion",EIGlobal.NivelLog.ERROR);
		    return 0;
		}
		query_realizar="SELECT a.CONTRATO AS CONTRATO, a.USR AS USR, to_char(FCH_ENV,'dd/mm/yyyy') AS FCH_ENV, b.HORA AS HORA_ENV, "+
			       "	a.NUM_ORDEN as NUM_ORDEN,a.EDOABONO AS EDOABONO,('098121' || a.FOLIO_LIQ_ABONO) AS FOLIO_LIQ_ABONO, a.CONCEPTO AS CONCEPTO, a.CTA_CARGO AS CTA_CARGO, "+
			       "	a.CTA_ABONO AS CTA_ABONO, a.BENEFICIARIO AS BENEFICIARIO, a.NOM_BCOREC AS NOM_BCOREC, "+
			       "	a.CVE_PAIS_REC AS CVE_PAIS_REC, a.CD_REC AS CD_REC, a.CVDIV_ABON AS CVDIV_ABON, "+
			       "	a.TPO_CAMPES AS TPO_CAMPES, a.IMP_DOLARES AS IMP_DOLARES, a.IMP_DIVISA AS IMP_DIVISA"+
			       " FROM	EWEB_BITA_INTER a, TCT_BITACORA b"+
			       " WHERE	a.REF = "+ req.getParameter("Ref") +
				 " AND      a.FCH_ENV  >= to_date ('"+fecha_mov+"' || ' 000000','DDMMYYYY HH24MISS') " +
				 " AND      a.FCH_ENV  <= to_date ('"+fecha_mov+"' || ' 235959','DDMMYYYY HH24MISS') " +
				 //" AND      b.referencia=a.ref ";
				 //jgal - Se agregar condiciones para romper producto cartesiano
				 " AND      b.referencia = a.ref" +
				 " AND a.FCH_ENV = b.fecha " +
				 " AND  a.CONTRATO = '" + session.getContractNumber() + "'";


				 if (!fecha_mov.equals(fecHoy))
				 {
				 	query_realizar +=	" AND b.FECHA  >= to_date ('"+fecha_mov+"' || ' 000000','DDMMYYYY HH24MISS') " +
								" AND b.FECHA  <= to_date ('"+fecha_mov+"' || ' 235959','DDMMYYYY HH24MISS')";
				 }

		EIGlobal.mensajePorTrace("ConsultaBitacora - detalle() -- query: [" + query_realizar + "]",EIGlobal.NivelLog.ERROR);
		TestQuery  = connbd.createStatement();
		if(TestQuery == null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Cannot Create Query",EIGlobal.NivelLog.ERROR);
		    return 0;
		}
		TestResult = TestQuery.executeQuery(query_realizar);
		if(TestResult == null){
		    EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Cannot Create ResultSet",EIGlobal.NivelLog.ERROR);
		    return 0;
		}
		if(TestResult.next()){
		    tabla.append("<table width=380 border=0 cellspacing=2 cellpadding=3 background=\"/gifs/EnlaceMig/gau25010.gif\">\n");

		    //CONTRATO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right width=0>Contrato:</td>\n");
		    tabla.append("		<td class=textabcom align=left nowrap>").append(TestResult.getString(CONTRATO)!=null?TestResult.getString(CONTRATO):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //USUARIO
		    if (TestResult.getString("usr")!=null)
		    	usu_desc = TestResult.getString("usr") + " " + ObtenNombreUsuario(session.getUserID8(),false,req) +"</td>\n";
		    else
			usu_desc = "&nbsp;</td>\n";
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Usuario:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(usu_desc);
		    tabla.append("	</tr>\n");

		    //FECHA Y HORA
		    if (TestResult.getString("hora_env")!=null)
		    {
			if (TestResult.getString("hora_env").trim().length()==0)
				usu_hora = " 00:00 hrs.";
			else if (TestResult.getString("hora_env").trim().length()==1)
				usu_hora = " 00:0" + TestResult.getString("hora_env").trim() + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==2)
				usu_hora = " 00:" + TestResult.getString("hora_env").trim() + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==3)
				usu_hora = " 0" + TestResult.getString("hora_env").trim().substring(0,1) + ":" + TestResult.getString("hora_env").trim().substring(1,3) + " hrs.";
			else if (TestResult.getString("hora_env").trim().length()==4)
				usu_hora = " " + TestResult.getString("hora_env").trim().substring(0,2) + ":" + TestResult.getString("hora_env").trim().substring(2,4) + " hrs.";
		    }

		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Fecha y hora:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("fch_env")!=null?TestResult.getString("fch_env"):"&nbsp;").append(usu_hora).append("</td>\n");
		    tabla.append("	</tr>\n");

		    //REFERENCIA DE CARGO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Ref. de Cargo:</td>\n");
		    tabla.append("		<td class=textabcom align=left>Por Confirmar.</td>\n");
		    tabla.append("	</tr>\n");

		    //NUMERO DE ORDEN
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>No. de Orden:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("num_orden")!=null?TestResult.getString("num_orden"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //REFERENCIA DE RASTREO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Ref. de Rastreo:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("folio_liq_abono")!=null?TestResult.getString("folio_liq_abono"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //CONCEPTO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Concepto:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("concepto")!=null?TestResult.getString("concepto"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //ORDENANTE
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Ordenante:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cta_cargo")!=null?TestResult.getString("cta_cargo"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //BENEFICIARIO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Beneficiario:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cta_abono")!=null?TestResult.getString("cta_abono"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //RFC BENEFICIARIO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>RFC Beneficiario:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("beneficiario")!=null?TestResult.getString("beneficiario"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //BANCO RECEPTOR
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Banco Receptor:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("nom_bcorec")!=null?TestResult.getString("nom_bcorec"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //PAIS
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Pais:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cve_pais_rec")!=null?TestResult.getString("cve_pais_rec"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //CIUDAD
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Ciudad:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cd_rec")!=null?TestResult.getString("cd_rec"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //DIVISA
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Divisa:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("cvdiv_abon")!=null?TestResult.getString("cvdiv_abon"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //TIPO DE CAMBIO
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Tipo de Cambio:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("tpo_campes")!=null?TestResult.getString("tpo_campes"):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IMPORTE DLLS
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Importe Dlls:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("imp_dolares")!=null?formateo.format(TestResult.getDouble("imp_dolares")):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IMPORTE DIVISA
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Importe Divisa:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(TestResult.getString("imp_divisa")!=null?formateo.format(TestResult.getDouble("imp_divisa")):"&nbsp;").append("</td>\n");
		    tabla.append("	</tr>\n");

		    //IMPORTE IVA
		    tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Importe IVA:</td>\n");
		    tabla.append("		<td class=textabcom align=left nowrap>").append("&nbsp;</td>\n");
		    tabla.append("	</tr>\n");

			//ESTATUS Q6973 Getronics
			tabla.append("	<tr>\n");
		    tabla.append("		<td class=tittabcom align=right>Estatus:</td>\n");
		    tabla.append("		<td colspan='3' class=textabcom align=left>").append(Consulta_status(TestResult.getString("edoabono"))).append("</td>\n");
		    tabla.append("	</tr>\n");


		    tabla.append("</table>\n");
			//  BIT CU1151
			/*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
		    if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
				try{
		        	BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_BITACORA_CONSULTA_BIT_DETALLE_INT);
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
		    }
			/*
			 * 02/ENE/07
			 * VSWF-BMB-F
			 */
		}
	    }catch(SQLException sqle){
		EIGlobal.mensajePorTrace("ConsultaBitacora - detalle(): Excepcion Error... "+sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	    }finally{
		try{
		    TestResult.close();
		    TestQuery.close();
		    connbd.close();
		}catch(Exception e) {}
	    }
	    req.setAttribute( "tabla", tabla);
	    req.setAttribute( "ClaveBanco", session.getClaveBanco());
	    //evalTemplate(IEnlace.BITACORA_TMPL, req, res);
	    evalTemplate("/jsp/Detalle_Bita_Inter.jsp", req, res);
	    return 0;
	}

	/**
	 * Consulta estatus de las operaciones.
	 *
	 * @param campo campo
	 * @return String estatus
	 */
public String Consulta_status(String campo) //Q6973 Getronics
{
	/*
	PV- Por verificar
	LI- Listo
	EN- Enviado
	CO- Confirmado
	CA- Cancelado
	ER- Error
	CP- Capturada/ no enviada
	RT- Orden con restricci�n
	MA- Confirmaci�n manual
	AU- Autorizada
	*/
		if ("CP".equals(campo) || "RT".equals(campo))
			return "RECIBIDA";
		if ("EN".equals(campo) || "PV".equals(campo) || "LI".equals(campo))
			return "ENVIADA";
		if ("CO".equals(campo) || "MA".equals(campo) || "AU".equals(campo))
			return "CONFIRMADA";
		if ("CA".equals(campo))
			return "CANCELADA";
		if ("ER".equals(campo))
			return "ERROR";
	return "";
}

    /** El objeto _default template. */
    static protected String _defaultTemplate ="EnlaceMig/movimientos";

    /**
     * Metodo para obtener el detalle correspondiente al comprobante de una
     * operaci�n reuters fx.
     *
     * @param req El objeto: req
     * @param res El objeto: res
     * @return Objeto int
     * @throws Exception La exception
     */
    public int detalle_reutersfx(HttpServletRequest req, HttpServletResponse res )
    throws Exception{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		Connection conn = null;
		PreparedStatement sDup = null;
		ResultSet rs = null;
		RET_OperacionVO bean = null;
		RET_CuentaAbonoVO beanAbono = null;
		Vector detAbonos = new Vector();
		String fecha_mov = null;
		String query = null;
		int ind = 0;
		int numRegistros = 0;

		EIGlobal.mensajePorTrace ("ConsultaBitacora::detalle_reutersfx:: Entrando a traer registro", EIGlobal.NivelLog.DEBUG);
		try{
			sess.removeAttribute("beanOperacionRET");
			if (req.getAttribute( FECHA_TRABAJAR) != null)
				fecha_mov = (String) req.getAttribute( FECHA_TRABAJAR);
			fecha_mov = fecha_mov.substring( 0,2 ) + fecha_mov.substring( 3,5 ) + fecha_mov.substring( 6,10 );

			conn = createiASConn (Global.DATASOURCE_ORACLE2);
			query  = "Select a.FOLIO_ENLA as folioEnla,"
				   + "a.FOLIO_OPER_LIQ as folioOperLiq,"
				   + "a.TIPO_OPERACION as tipoOperacion,"
				   + "a.DIVISA_OPERANTE as divisaOperante,"
				   + "a.CONTRA_DIVISA as contraDivisa,"
				   + "a.TC_PACTADO as tcPactado,"
				   + "a.IMPORTE_TOT_OPER as importeTotOper,"
				   + "a.CTA_CARGO as ctaCargo,"
				   + "a.USR_REGISTRO as usrRegistro,"
				   + "a.USR_AUTORIZA as usrAutoriza,"
				   + "a.ESTATUS_OPERACION as estatusOperacion,"
				   + "a.CONCEPTO as concepto,"
				   + "to_char(a.FCH_HORA_PACTADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrPactado,"
				   + "to_char(a.FCH_HORA_COMPLEMENT, 'DD-MM-YYYY HH24:MI:SS') as fchHrComplement,"
				   + "to_char(a.FCH_HORA_LIBERADO, 'DD-MM-YYYY HH24:MI:SS') as fchHrLiberado "
				   + "From EWEB_OPER_REUTERS a "
				   + "Where a.CONTRATO = '" + session.getContractNumber() + "' "
				   + "And a.FOLIO_ENLA = '" + req.getParameter("Ref") + "' "
				   + "And trunc(a.FCH_HORA_LIBERADO) = to_date('" + fecha_mov + "','DDMMYYYY')";
			EIGlobal.mensajePorTrace ("ConsultaBitacora::detalle_reutersfx:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			while(rs.next()){
				bean = new RET_OperacionVO();
				bean.setFolioEnla(rs.getString("folioEnla")!=null?rs.getString("folioEnla"):"");
				bean.setFolioOper(rs.getString("folioOperLiq")!=null?rs.getString("folioOperLiq"):"");
				bean.setTipoOperacion(rs.getString("tipoOperacion")!=null?rs.getString("tipoOperacion"):"");
				bean.setDivisaOperante(rs.getString("divisaOperante")!=null?rs.getString("divisaOperante"):"");
				bean.setContraDivisa(rs.getString("contraDivisa")!=null?rs.getString("contraDivisa"):"");
				bean.setTcPactado(rs.getString("tcPactado")!=null?rs.getString("tcPactado"):"");
				bean.setImporteTotOper(rs.getString("importeTotOper")!=null?rs.getString("importeTotOper"):"");
				bean.setCtaCargo(rs.getString("ctaCargo")!=null?rs.getString("ctaCargo"):"");
				bean.setUsrRegistro(rs.getString("usrRegistro")!=null?rs.getString("usrRegistro"):"");
				bean.setUsrAutoriza(rs.getString("usrAutoriza")!=null?rs.getString("usrAutoriza"):"");
				bean.setEstatusOperacion(rs.getString("estatusOperacion")!=null?rs.getString("estatusOperacion"):"");
				bean.setConcepto(rs.getString("concepto")!=null?rs.getString("concepto"):"");
				bean.setFchHoraPactado(rs.getString("fchHrPactado")!=null?rs.getString("fchHrPactado"):"");
				bean.setFchHoraComplemento(rs.getString("fchHrComplement")!=null?rs.getString("fchHrComplement"):"");
				bean.setFchHoraLiberado(rs.getString("fchHrLiberado")!=null?rs.getString("fchHrLiberado"):"");
			}

			query  = "Select a.CUENTA_ABONO as ctaAbono,"
				   + "a.IMPORTE as importe "
				   + "From EWEB_MONTOS_REUTERS a "
				   + "Where a.CONTRATO = '" + session.getContractNumber() + "' "
				   + "And a.FOLIO_ENLA = '" + req.getParameter("Ref") + "' "
				   + "And a.FOLIO_OPER_LIQ='" + bean.getFolioOper() + "' "
				   + "And trunc(a.FCH_HORA_COMPLEMENT) = to_date('" + fecha_mov + "','DDMMYYYY')";
			EIGlobal.mensajePorTrace ("ConsultaBitacora::detalle_reutersfx:: Query->" + query, EIGlobal.NivelLog.DEBUG);
			sDup = conn.prepareStatement(query);
			rs = sDup.executeQuery();

			while(rs.next()){
				beanAbono = new RET_CuentaAbonoVO();
				beanAbono.setNumCuenta(rs.getString("ctaAbono")!=null?rs.getString("ctaAbono"):"");
				beanAbono.setImporteAbono(rs.getString("importe")!=null?rs.getString("importe"):"");
				detAbonos.add(beanAbono);
			}
			bean.setDetalleAbono(detAbonos);

			sess.setAttribute("beanOperacionRET", bean);
			rs.close();
			sDup.close();
			conn.close();

		}catch(Exception e){
			EIGlobal.mensajePorTrace ("ConsultaBitacora::detalle_reutersfx:: -> Problema controlado:" + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	    evalTemplate("/jsp/RET_comprobante.jsp", req, res);
	    return 0;
    }

	/**
	 * Exportar bitacora.
	 *
	 * @param request El objeto: request
	 * @param response El objeto: response
	 */
	private void exportarBitacora(HttpServletRequest request,
			HttpServletResponse response) {
		BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION);
		String contrato = session.getContractNumber();
		String usuario = session.getUserID8();
		String nombreArchivo   = contrato + usuario + ".bit";
		String archivoOrigen = IEnlace.DOWNLOAD_PATH + nombreArchivo +".doc";
		String tipoArchivo	= request.getParameter("tipoArchivoExport");
		EIGlobal.mensajePorTrace( "Exportando archivoOrigen: "+archivoOrigen, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "Exportando nombreArchivo: "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "Exportando tipoArchivo: "+tipoArchivo, EIGlobal.NivelLog.DEBUG);
		ExportModel em = new ExportModel(nombreArchivo, '|', ";",true)
	    .addColumn(new Column(0, "FECHA"))
	    .addColumn(new Column(1, "HORA"))
	    .addColumn(new Column(2, "DESCRIPCION"))
	    .addColumn(new Column(3, "CUENTA DE CARGO"))
		.addColumn(new Column(4, "CUENTA DE ABONO/MOVIL"))
		.addColumn(new Column(5, "IMPORTE","$#########################0.00"))
		.addColumn(new Column(6, "REFERENCIA"))
		.addColumn(new Column(7, "USUARIO"))
		.addColumn(new Column(8, "ESTATUS"))
		.addColumn(new Column(9, "ORIGEN DE OPERACION"))
		.addColumn(new Column(10, "PAGO LC"));
		try {
			HtmlTableExporter.export(tipoArchivo, em, archivoOrigen, response);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace( "Error durante la exportacion del la bitacora: "+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
	}

} // clase
/*2.0*/