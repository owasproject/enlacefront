/**Banco Santander Mexicano
* Clase OpcionesNominaInter, exhibe la plantilla necesaria para dar mantenimiento al archivo,
						asimismo, envio y recuperacion de archivo
* @
* @version 1.1
* fecha de creacion:
* responsable: Ruben Fregoso V.
* descripcion: en el caso de ejecutar la opcion altas, se exhibira una plantilla para capturar un nuevo registro,
				en el caso de bajas y modificaciones, muestra al usuario los campos que integran al registro,
				estas operaciones (altas, bajas y cambios) usan la misma plantilla.
				Cuando el usuario ha seleccionado la opcion envio, prepara los datos que han de ser enviados
				al servicio IN04,IN05 e IN06 de WEB_RED.
				Cuanto el usuario selecciona recuperacion, el servicio es IN10, tambien a traves de WEB_RED
                // Modificacion RMM 20021218 cerrado de archivos, no se puede eliminar RandomAcessFile por usar metodo especifico
*/

package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.sql.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosNominaInter;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import java.text.SimpleDateFormat;

public class OpcionesNominaInter extends BaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
    			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean transaccionExitosa=true;
		boolean bandera=true;

		GregorianCalendar fechaHoy = null;
		String laFechaHoy = null;
		Vector diasNoHabiles = null;
		//Vector diasNoHabilesJS;
		CalendarNomina nominaCal= new CalendarNomina();

		//diasNoHabilesJS = new Vector();
		String numDevuelto;
		numDevuelto		= "";
		String Sin_duplicados = "NO";

		if ( sesionvalida ) {

			/******************************************Inicia validacion OTP**************************************/
			String valida = getFormParameter(request, "valida" );
			if ( validaPeticion( request, response,session,sess,valida))
			{
				  EIGlobal.mensajePorTrace("\n\n ENTR� A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
			}
			/******************************************Termina validacion OTP**************************************/
			else
			{
				//*************************************************
				//Determinar las cuentas que corresponden al modulo
	            //*************************************************
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nom_IN);
				EIGlobal.mensajePorTrace("***OpcionesNominaInter.class &Entrando al metodo execute de OpcionesNominaInter...&, Vers 1.2 23 Feb 2005", EIGlobal.NivelLog.DEBUG);
				//request.setAttribute("MenuPrincipal", session.getstrMenu());
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser",ObtenContUser(request));
				fechaHoy	= new GregorianCalendar();
				laFechaHoy	= EIGlobal.formatoFecha(fechaHoy,"aaaammdd");
				request.setAttribute("fechaHoy",laFechaHoy);
				String secuencia_rec	= "";
				String VarFechaHoy		= "";
				String strInhabiles = "";

				try
				{
				  diasNoHabiles = nominaCal.CargarDias();
				  strInhabiles = nominaCal.armaDiasInhabilesJS();
				}catch(ArrayIndexOutOfBoundsException e)
				 {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					strInhabiles ="";
				 }

				Calendar fechaFin	= nominaCal.calculaFechaFin();
				VarFechaHoy			= nominaCal.armaArregloFechas();
				String strFechaFin  = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
				String Cuentas		= "";
				String tipo_cuenta	= "";
				//***********************************************
				//modificaci�n para integraci�n pva 07/03/2002
	            //***********************************************
	 			//String[][] arrayCuentas = ContCtasRelac(session.getContractNumber());
				String nombre_arch_salida	= "";

				boolean banderaTransaccion = true;  // variable l�gica de sesion IM199738

				 //***********************************************
				//modificaci�n para integraci�n pva 07/03/2002
	            //***********************************************
	            /*
				for ( int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++ ) {
					tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
					if ( arrayCuentas[indice][2].equals("P") &&
							( !tipo_cuenta.equals( "BM" ) ) &&
							( !tipo_cuenta.equals( "SI" ) ) && ( !tipo_cuenta.equals( "49" ) ) )
						Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" +
								arrayCuentas[indice][1] + "\n";
				}
				*/
				String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
				datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";			datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
                datesrvr = datesrvr + "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
				String cuentaCargo = getFormParameter(request, "textcuenta");
			    EIGlobal.mensajePorTrace("***OpcionesNominaInter.class & cuentaCargo"+cuentaCargo, EIGlobal.NivelLog.INFO);
	           //String cuentaCargo = session.getCuentaCargo();
			   if (cuentaCargo == null || cuentaCargo.equals(""))
			      cuentaCargo = "";
				request.setAttribute( "textcuenta",cuentaCargo);
				//request.setAttribute( "Cuentas",cuentasCargo);
				request.setAttribute( "VarFechaHoy",           VarFechaHoy);
				request.setAttribute( "fechaFin",              strFechaFin);
				request.setAttribute( "diasInhabiles",         strInhabiles);
				request.setAttribute( "Fechas",                 ObtenFecha(true));
				request.setAttribute( "FechaHoy",              ObtenFecha(false) + " - 06" );
				request.setAttribute("botLimpiar","");

				EIGlobal.mensajePorTrace("***OpcionesNominaInter.class & Asignacion de elementos", EIGlobal.NivelLog.DEBUG);


				String strOperacion = getFormParameter(request, "operacion");
				if (strOperacion == null || strOperacion.equals(""))
			        strOperacion = "";
				String strRegistro = getFormParameter(request, "registro");
				if (strRegistro == null || strRegistro.equals(""))
			        strRegistro = "";
				long longRegistro	= 0;
				request.setAttribute("operacion",strOperacion);

				EIGlobal.mensajePorTrace("***OpcionesNominaInter.class & Elementos Asignados", EIGlobal.NivelLog.DEBUG);

				String	tipoArchivotmp = getFormParameter(request, "tipoArchivo");
				if (tipoArchivotmp == null || tipoArchivotmp.equals(""))
			        tipoArchivotmp = "";

				EIGlobal.mensajePorTrace("***OpcionesNominaInter.class & Revisando tipo de operacion "+ strOperacion, EIGlobal.NivelLog.INFO);

				//VSWF RRG I 08-May-2008 Se agrego esta variable para el beneficiario
				String beneficiario="";
				//VSWF RRG F 08-May-2008
				if ( strOperacion.equals( "baja" ) || strOperacion.equals( "modificacion" ) )
				   {
					  try {
							longRegistro = Long.parseLong( strRegistro );
						  } catch( NumberFormatException e )
							 {
							  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							 }

					String elArchivoNomina	  = "";//nomArchNomina
					String elArchivo		  = "";//nombreOriginal
					//clabe **********
					String valorCuentaClabe   = "<input type=radio value=0 name=TipoCuenta >Cuenta CLABE";
					String valorCuentaTarjeta = "<input type=radio value=0 name=TipoCuenta >Tarjeta de D&eacute;bito";
					//***************
					//File nominaFile= new File(session.getNameFileNomina());
					//File nominaFile			= new File((String) sess.getAttribute("nombreArchivo"));
					EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Se va a crear el archivo _1", EIGlobal.NivelLog.DEBUG);
					ArchivosNominaInter archivo2 = new ArchivosNominaInter((String)sess.getAttribute("nombreArchivo"),request);
					int result=archivo2.leeRegistro(longRegistro,tipoArchivotmp);

					EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Se va a crear el archivo _1", EIGlobal.NivelLog.INFO);
					String variablesJS=archivo2.creaVariablesJS();
					EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class variable js:"+variablesJS, EIGlobal.NivelLog.INFO);



					if (strOperacion.equals("baja")){
						request.setAttribute("soloLectura","blur()"); //Variable para controlar los campos de solo lectura en el caso de la baja
						request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25540.gif");
						request.setAttribute("altImagen","Borrar");
					}

					if (strOperacion.equals( "modificacion" ) ) {
						request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25510.gif");
						request.setAttribute("altImagen","Modificar");
					}

					request.setAttribute("NombreEmpleado",archivo2.NombreCompleto.trim());
					request.setAttribute("NumeroCuenta",archivo2.NumeroCuenta.trim());
					//clabe *********************

					if (archivo2.NumeroCuenta.trim().length () == 18 || archivo2.NumeroCuenta.trim().length () == 11)
						valorCuentaClabe   = "<input type=radio value=0 name=TipoCuenta checked>Cuenta CLABE";
						else
						valorCuentaTarjeta = "<input type=radio value=0 name=TipoCuenta checked>Tarjeta de D&eacute;bito";

					request.setAttribute("CuentaClabe",valorCuentaClabe);
					request.setAttribute("CuentaTarjeta" ,valorCuentaTarjeta);

					//***************************
					archivo2.Importe=agregarPunto(archivo2.Importe);
					request.setAttribute("Importe",archivo2.Importe);
					String CveBanco = traeBancocve(archivo2.ClaveBanco.trim());
					String bank     = traeBanco(archivo2.ClaveBanco.trim());

					String plazaBank=traePlaza(archivo2.PlazaBanxico.trim());
					request.setAttribute("comboBancos", colocaBanco(bank,CveBanco));
					//request.setAttribute("comboBancos", colocaBanco(bank,archivo2.ClaveBanco.trim()));
					request.setAttribute("comboPlazas", colocaPlaza(plazaBank));
					request.setAttribute("anteriorImporte",archivo2.Importe);
					request.setAttribute("posicionRegistro",strRegistro);
					request.setAttribute("CuentasArchivo", session.getCuentasArchivo());
					//Q05-0044906 - Praxis NVS - 051117
					// <meg vswf 05122008>
					//request.setAttribute("Concepto", archivo2.Concepto);
					//request.setAttribute("Referencia", archivo2.Referencia2);
					request.setAttribute("Concepto", " ");
					request.setAttribute("Referencia"," ");
					// <meg vswf 05122008>
					//request.setAttribute("NumerosEmpleado", session.getNumerosEmpleado());
					if (sess.getAttribute("numeroEmp") != null)
					{
					request.setAttribute("NumerosEmpleado", sess.getAttribute("numeroEmp"));
					}
					if ( result == 1 ) {
						request.setAttribute("variables",variablesJS);
						request.setAttribute("ContenidoArchivo","");
					} else {
						request.setAttribute("ContenidoArchivo","Error leyendo Archivo: ");
					}
					//se colocan los putString para el menu y el encabezado
					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					if(strOperacion.equals("baja"))
						request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Bajas", "s25830h",request));
					else if(strOperacion.equals("modificacion"))
						request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Modificaciones", "s25830h",request));


					/**
					* Facultades Nomina Interbancaria
					*
					*/
					if(sess.getAttribute("facultadesN") != null)
					  request.setAttribute( "facultades", sess.getAttribute("facultadesN") );


					request.setAttribute("textcuenta",cuentaCargo);
					String uso_cta_cheques = Global.USO_CTA_CHEQUE; //se obtiene de global
					request.setAttribute("uso_cta_clabe", uso_cta_cheques);
					request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
					evalTemplate(IEnlace.NOMINADATOSINTER_TMPL, request, response);
				} else
				{

		            if ( strOperacion.equals( "valida" ) )
					{
						if(request.getSession().getAttribute("Recarga_Opc_Nomina")!=null && verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false))
						{
							EIGlobal.mensajePorTrace( "OpcionesNominaInter El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
							banderaTransaccion =false;
						}
						else
							if(request.getSession().getAttribute("Recarga_Opc_Nomina")!=null)
							{
								EIGlobal.mensajePorTrace("OpcionesNominaInter Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
								request.getSession().removeAttribute("Recarga_Opc_Nomina");
								banderaTransaccion=false;
							}
							else

								if(verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false) )
								{
									EIGlobal.mensajePorTrace( "OpcionesNominaInter El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
									banderaTransaccion = false;
								}
								else

									if(request.getSession().getAttribute("Recarga_Opc_Nomina")==null && !verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false))
						            {
									    EIGlobal.mensajePorTrace( "OpcionesNominaInter Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
										banderaTransaccion = true;
									}

						if (banderaTransaccion) //IM199738. Entra si no es recarga de la misma sesion (evitar reenvio misma transferencia)
						{

						ArchivoRemoto archR = new ArchivoRemoto();

						ArchivoRemoto archR2 = new ArchivoRemoto();	// Para la copia de Exportar Archivo.


						String folio = getFormParameter(request, "folio");
						request.setAttribute("folio", folio);
						EIGlobal.mensajePorTrace( "***Visualizar folio: "+ folio, EIGlobal.NivelLog.INFO);


						String nombre_archivo = (String)sess.getAttribute("nombreArchivo");
						String nA			= nombre_archivo.substring(nombre_archivo.lastIndexOf('/')+1,nombre_archivo.length());
						String ruta			= "/gfi/desa/web/internet/EnlaceMig/Download";
						File archivoEnvio	= new File(Global.DOWNLOAD_PATH+session.getUserID8()+".nib");
						File archivoRecibido = new File(Global.DOWNLOAD_PATH+session.getUserID8()+".nibr");
						String totalRegistros	= "";
						String fechaCargo		= "";
						String importeCargo		= "";
						String resultadoTransmision = "";
		                RandomAccessFile archivoEnvioPago = null;// Modificacion RMM 20021218 no se puede eliminar randomacces por usar metodo especifico

						try{
							if(archivoEnvio.exists())
								archivoEnvio.delete();


							/*JEV*/
							archivoEnvioPago = new RandomAccessFile(Global.DOWNLOAD_PATH+session.getUserID8()+".nib", "rw");// Modificacion RMM 20021218
							/*JEV*/

							EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Se va a crear el archivo _2", EIGlobal.NivelLog.DEBUG);
							ArchivosNominaInter archivoP	  = new ArchivosNominaInter(Global.DOWNLOAD_PATH+nA,request);

							String encabezadoArchivoImportado = archivoP.leeEncabezadoImportado();
							EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class encabezadoArchivo: " +encabezadoArchivoImportado, EIGlobal.NivelLog.INFO);

							String[] detalleArchivoImportado  = archivoP.leeDetalleImportado();
							String contratoCargo              = session.getContractNumber();
							totalRegistros                    = session.getTotalRegsNom();
							importeCargo					  = agregarPunto(session.getImpTotalNom().trim());
							fechaCargo=getFormParameter(request, "txtFechaLimite");
							String cuentaCargotxt = cuentaCargo;

							String encabezadoArchivoPago	  = contratoCargo+"|"+totalRegistros+"|"+importeCargo+"|"+fechaCargo+"|"+cuentaCargotxt+"|\r\n";
							archivoEnvioPago.writeBytes(encabezadoArchivoPago);
							String cuentaAbono = "", importeAbono="", bancoAbono="", plazaAbono="", nombreAbono="", conceptoAbono = "", referenciaAbono="";
							String detalleArchivoPago = cuentaAbono+importeAbono+bancoAbono+plazaAbono+nombreAbono;
							String[] registrosArchivoPagos	  = new String[detalleArchivoImportado.length];

							EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class detalle del archivo 001", EIGlobal.NivelLog.DEBUG);

							double importeNI = 0;
							for(int i=0; i<detalleArchivoImportado.length;i++){
							    int fixlength = 0;
							    if (detalleArchivoImportado[i].length() == 154){
							        fixlength = 2;
							    }
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" long: "+detalleArchivoImportado[i].length(), EIGlobal.NivelLog.INFO);
								cuentaAbono		= detalleArchivoImportado[i].substring(61,81);
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" cuenta abono: "+cuentaAbono, EIGlobal.NivelLog.INFO);
								//Q05-0044906 - Praxis NVS - 051117
								importeAbono	= detalleArchivoImportado[i].substring(81,99 - fixlength);
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
								importeAbono	= importeAbono.trim();
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
								importeAbono	= agregarPunto(importeAbono);
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
								bancoAbono		= detalleArchivoImportado[i].substring(99 - fixlength,104 - fixlength);
								EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" bancoAbono: "+bancoAbono, EIGlobal.NivelLog.INFO);
								//Q05-0044906 - Praxis NVS - 051117
								//if(detalleArchivoImportado[i].length()>109)
								plazaAbono		= detalleArchivoImportado[i].substring(104 - fixlength,109 - fixlength);
								//else
								//plazaAbono		= detalleArchivoImportado[i].substring(104);
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" plazaAbono: "+plazaAbono, EIGlobal.NivelLog.INFO);
								//***************************
								nombreAbono		= detalleArchivoImportado[i].substring(6,56).trim();
								//VSWF RRG I 08-May-2008 Se carga el beneficiario
								beneficiario=nombreAbono;
								//VSWF RRG I 08-May-2008
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" nombreAbono: "+nombreAbono, EIGlobal.NivelLog.INFO);
								//Q05-0044906 - Praxis NVS - 051117
								if(detalleArchivoImportado[i].length() > 109) {
		                                                conceptoAbono = detalleArchivoImportado[i].substring(109 - fixlength, 149 - fixlength).trim();
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+conceptoAbono, EIGlobal.NivelLog.INFO);
								referenciaAbono = detalleArchivoImportado[i].substring(149 - fixlength, 156 - fixlength).trim();
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+referenciaAbono, EIGlobal.NivelLog.INFO);
		                                                }
		                                                else {
		                                                conceptoAbono = "";
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+conceptoAbono, EIGlobal.NivelLog.INFO);
								referenciaAbono = "";
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+referenciaAbono, EIGlobal.NivelLog.INFO);
		                                                }

								detalleArchivoPago=cuentaAbono+"|"+importeAbono+"|"+bancoAbono+"|"+plazaAbono+"|"+nombreAbono+"|"+conceptoAbono+"|"+referenciaAbono+"|\r\n";
								registrosArchivoPagos[i]=detalleArchivoPago;
								//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Enviando al archivo.", EIGlobal.NivelLog.INFO);
								archivoEnvioPago.writeBytes(detalleArchivoPago);
								try {
									importeNI = importeNI + Double.parseDouble(importeAbono.trim());
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}
							request.setAttribute("registrosNIReq",session.getTotalRegsNom());
							sess.setAttribute("registrosNISes",session.getTotalRegsNom());
							EIGlobal.mensajePorTrace("��������������������registrosAceptados nomina interbancaria ->" + session.getTotalRegsNom(), EIGlobal.NivelLog.INFO);
							archivoEnvioPago.close();
						}

						catch(IOException e){
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}finally{// Modificacion RMM 20021218 inicio
		                    if(null != archivoEnvioPago){
		                        try{
		                        	archivoEnvioPago.close();
		                        }catch(Exception e){
		                        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		                        }
		                        archivoEnvioPago = null;
		                    }
		                }// Modificacion RMM 20021218 fin

						EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class se obtuvo detalle.", EIGlobal.NivelLog.DEBUG);

						File oldFile=new File(Global.DOWNLOAD_PATH+nA);

						EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class otro archivo nA: "+ nA, EIGlobal.NivelLog.INFO);

						sess.setAttribute("NomArchOrigen",nA );


						if(archR.copiaLocalARemoto(session.getUserID8()+".nib")){
						//if(archR.copiaLocalARemoto("1001851.nib")){
						   EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(a) el archivo .nib se copio correctamente", EIGlobal.NivelLog.DEBUG);
						}
						else{
						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(b) el archivo nib no se copio correctamente", EIGlobal.NivelLog.INFO);
						}

						String tramaEnviada ="";
						tramaEnviada="1EWEB|"+session.getUserID8()+"|VALF|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|3|";

				 //MSD Q05-0029909 inicio lineas agregadas
				 String IP = " ";
				 String fechaHr = "";												//variables locales al m�todo
				 IP = request.getRemoteAddr();											//ObtenerIP
				 java.util.Date fechaHrAct = new java.util.Date();
				 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
				 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
				 //MSD Q05-0029909 fin lineas agregada


						EIGlobal.mensajePorTrace(fechaHr+"***OpcionesNominaInter.class La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);

						ServicioTux tuxGlobal = new ServicioTux();
						//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
						Hashtable htResult=null;
						try{
							htResult=tuxGlobal.web_red(tramaEnviada);
						}
						catch(java.rmi.RemoteException re){
							EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
						} catch(Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}

						String CodError = "";

						if(htResult != null) {
							CodError = ( String ) htResult.get( "BUFFER" );
							htResult.clear();
							htResult = null;
						}


						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class El servicio regreso CodError: "+CodError, EIGlobal.NivelLog.DEBUG);

						int NumeroError = Integer.parseInt(CodError.substring(4,8));

						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Numero Error="+ NumeroError, EIGlobal.NivelLog.DEBUG);


						if ((NumeroError==0) && ( CodError.trim().lastIndexOf("/")>0))
						{
							long posicionReg = 0;
							long residuo	 = 0;

							//String[][] arrayDetails;
							String nombreArchRegreso=CodError.substring(CodError.lastIndexOf('/')+1);

							if(archR.copia(nombreArchRegreso))
								EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo nib se copio correctamente", EIGlobal.NivelLog.DEBUG);
							else
								EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo nib1 no se copio correctamente", EIGlobal.NivelLog.INFO);


							String contenidoArchivoStr = "";
							contenidoArchivoStr="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
											        "<tr> "+
													"<td class=\"textabref\">Pago de N&oacute;mina Interbancaria</td>"+
													"</tr>"+
													"</table>"+
													"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
													"<tr> "+
													"<td width=\"70\" class=\"tittabdat\" align=\"center\">N&uacute;mero de Cuenta</td>"+
													"<td width=\"100\" class=\"tittabdat\" align=\"center\">Clave del Banco</td>"+
													"<td width=\"70\" class=\"tittabdat\" align=\"center\">Importe</td>"+
													"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de Recepci&oacute;n</td>"+
													"<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de Aplicaci&oacute;n</td>"+
													"</tr>";
							String aux_nombreArchRegreso = nombreArchRegreso;

							nombreArchRegreso=Global.DIRECTORIO_LOCAL+"/"+nombreArchRegreso;
							BufferedReader arch	= new BufferedReader( new FileReader(nombreArchRegreso) );
							String linea;

							//Se trata de generar la copia remota para Exportacion del Archivo
							if ( archR2.copiaLocalARemoto( nombreArchRegreso,"WEB") )
							{
								EIGlobal.mensajePorTrace("OpcionesNominaInter - execute(): Copia Exportacion. "+ aux_nombreArchRegreso, EIGlobal.NivelLog.INFO);
								session.setRutaDescarga( "/Download/" + aux_nombreArchRegreso );
								request.setAttribute("DescargaArchivo",session.getRutaDescarga());
							}
							else
							{
								EIGlobal.mensajePorTrace("OpcionesNominaInter - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
								request.setAttribute("DescargaArchivo",""); //Poner vacio para l�gica en JSP
							}
							//Termina generaci�n copia Remota Exportacion Archivo.


							while ((linea = arch.readLine()) !=null)
						    {

								StringTokenizer separador = new StringTokenizer(linea, "|");
								posicionReg++;
								residuo=posicionReg % 2 ;
								String bgcolor="textabdatobs";
								if (residuo == 0){
									bgcolor="textabdatcla";
								}

							  if ( posicionReg <=30) // Control despliegue hasta 30 regs.
							  {
								contenidoArchivoStr=contenidoArchivoStr+"<tr>"; //Se traslado linea Dom31Oct
								while (separador.hasMoreTokens())
								{
									contenidoArchivoStr=contenidoArchivoStr+"<td class=\""+bgcolor+"\" nowrap align=\"center\">"+separador.nextToken()+"&nbsp;</td>";
								}
									contenidoArchivoStr=contenidoArchivoStr+"</tr>";

							  } //if posicionReg <=30 Regs



							}


							contenidoArchivoStr=contenidoArchivoStr+"</table>";
							request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);


		                    /* Se deshabilita este mensaje para manejo por No. regs. duplicados
							String infoUser= "<br>Esta  transmisi�n contiene registros  que se han enviado  anteriormente, " +
											 "si desea realizar  �l envi� de cualquier manera, presione el bot�n enviar    " +
											 "ubicado en la parte inferior de esta pantalla. ";
							*/

							//Se generan mensajes de acuerdo al numero de regs. duplicados
							String infoUser="";
							if (posicionReg <=30)
							{
								infoUser= "Esta transmisi�n contiene registros  que se han enviado  anteriormente, " +
										"si desea realizar  �l envi� de cualquier manera, presione el bot�n Enviar " +
										"ubicado en la parte inferior de esta pantalla, o puede elegir el bot�n Exportar para " +
									    "la exportaci�n del archivo de los registros duplicados";
							}
							else
							{
								infoUser= "Esta transmisi�n contiene registros  que se han enviado  anteriormente, " +
										"se muestran los primeros 30, pero el n�mero es mayor. <br>Puede elegir el bot�n"+
										"Exportar para ver completo el archivo de registros duplicados, "+
										"<br>o bien, Si desea realizar  �l envi� de cualquier manera, presione el bot�n transferir " +
										"ubicado en la parte inferior de esta pantalla. ";
							}
							//Termina



							infoUser="cuadroDialogoMedidas(\""+ infoUser +"\",1,380,220)";
			      			request.setAttribute("infoUser", infoUser);

							request.setAttribute("CuentasArchivo", session.getCuentasArchivo());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("textcuenta", "" );
							//request.setAttribute("Encabezado", CreaEncabezado("Registros dados de alta con anterioridad","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s25800h",request));
							request.setAttribute("Encabezado", CreaEncabezado("Registros dados de alta con anterioridad","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s55250",request));
							request.setAttribute("textcuenta",cuentaCargo);
							request.setAttribute("operacion","valida");
							request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
							request.setAttribute("tipoArchivo",tipoArchivotmp);
							request.setAttribute("txtFechaLimite", fechaCargo);
							evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
							//return;
							//evalTemplate("/jsp/DuplicadosNominaInter.jsp", request, response );
						}


						if ((NumeroError==0) && (CodError.trim().lastIndexOf("-")>0))
						{
							EIGlobal.mensajePorTrace("OpcionesNominaInter Entra caso No hay Errores Duplicados", EIGlobal.NivelLog.DEBUG);
							if(archivoRecibido.exists())
								archivoRecibido.delete();
//							TODO: BIT CU 4161, E2
							/*
				    		 * VSWF
				    		 */
							 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
							try{
							BitaHelper bh = new BitaHelperImpl(request, session,sess);
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ES_NOMINA_INTERBANCARIA_VALIDACION_ENVIO);
							bt.setContrato(session.getContractNumber());
							bt.setNombreArchivo(nombre_archivo);

							if(CodError!=null){
								 if(CodError.substring(0,2).equals("OK")){
									   bt.setIdErr("VALF0000");
								 }else if(CodError.length()>8){
									  bt.setIdErr(CodError.substring(0,8));
								 }else{
									  bt.setIdErr(CodError.trim());
								 }
								}
							bt.setServTransTux("VALF");
							bt.setCctaOrig(cuentaCargo);
							//VSWF RRG I  08-May-2008 Agrego el beneficiario a la bitacora
							bt.setBeneficiario(beneficiario);
							//VSWF RRG F  08-May-2008
							BitaHandler.getInstance().insertBitaTransac(bt);
							}catch (SQLException e){
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}catch(Exception e){
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
							 }
				    		/*
				    		 * VSWF
				    		 */
							// String infoUser= "<br>Validaci�n exitosa, presione el bot�n enviar para concluir la transacci�n";
							// infoUser="cuadroDialogo (\""+ infoUser +"\",1)";

							Sin_duplicados = "SI";

						}


					}

				}  //cerrando if (banderaTransaccion)


/*********************** Termina codigo Operacion Validar IM199738 **********************************************************************************************/


						String result = "";
EIGlobal.mensajePorTrace( "OperacionNominaInter LaBanderaTransaccion= " +banderaTransaccion, EIGlobal.NivelLog.INFO);


						if ( strOperacion.equals( "envio" ) || Sin_duplicados.equals("SI") || !banderaTransaccion )
						{
							//Sin_duplicados= SI, permite ejecuci�n transferencia si no hubo duplicados. IM199738
							Sin_duplicados ="NO";  //Reiniciar variable.

							/*********************************** Construir variables sesion. ****************/

							boolean banderaTransaccion2 = true;

							if(request.getSession().getAttribute("Recarga_Opc_Nomina2")!=null && verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false))
							{
								EIGlobal.mensajePorTrace( "OpcionesNominaInter El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
								banderaTransaccion2 =false;
							}
							else
								if(request.getSession().getAttribute("Recarga_Opc_Nomina2")!=null)
								{
									EIGlobal.mensajePorTrace("OpcionesNominaInter Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
									request.getSession().removeAttribute("Recarga_Opc_Nomina2");
									banderaTransaccion2=false;
								}
								else

									if(verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false) )
									{
										EIGlobal.mensajePorTrace( "OpcionesNominaInter El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
										banderaTransaccion2 = false;
									}
									else

										if(request.getSession().getAttribute("Recarga_Opc_Nomina2")==null && !verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",false))
							            {
										    EIGlobal.mensajePorTrace( "OpcionesNominaInter Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
											banderaTransaccion2 = true;
										}

							/*********************************** Termina construir variables sesion ********************************/

							EIGlobal.mensajePorTrace( "OperacionNominaInter BanderaTransaccion1: " +banderaTransaccion + "BanderaTransaccion2: " +banderaTransaccion2 , EIGlobal.NivelLog.INFO);

							if (banderaTransaccion && banderaTransaccion2) //IM199738. Entra si no es recarga de la misma sesion (evitar reenvio misma transferencia)
							{
								EIGlobal.mensajePorTrace("\n\n\n Entr� en banderas transaccion ... SAN\n\n\n", EIGlobal.NivelLog.DEBUG);

								///////////////////--------------challenge--------------------------
								String validaChallenge = request.getAttribute("challngeExito") != null
									? request.getAttribute("challngeExito").toString() : "";

								EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

								if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
									EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
									//ValidaOTP.guardaParametrosEnSession(request);
									guardaParametrosEnSession(request);
									validacionesRSA(request, response);
									return;
								}

								//Fin validacion rsa para challenge
								///////////////////-------------------------------------------------

								//interrumpe la transaccion para invocar la validaci�n
								boolean valBitacora = true;
								if(  valida==null && (!Sin_duplicados.equals("SI")) )
								{
									EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

									boolean solVal=ValidaOTP.solicitaValidacion(
												session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

										if( session.getValidarToken() &&
										    session.getFacultad(session.FAC_VAL_OTP) &&
										    session.getToken().getStatus() == 1 &&
										    solVal)
									{
										//VSWF-HGG -- Bandera para guardar el token en la bit�cora
										request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
										EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
										guardaParametrosEnSession(request);
			                            request.getSession().removeAttribute("mensajeSession");
			                            EIGlobal.mensajePorTrace("�������������������� importe total Nomina Interbancaria ->" + agregarPunto(session.getImpTotalNom().trim()), EIGlobal.NivelLog.INFO);
										sess.setAttribute("importeNISes", agregarPunto(session.getImpTotalNom().trim()));
										ValidaOTP.mensajeOTP(request, "NominaInter");
										ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
									}
								    else{
			                            ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
									    valida="1";
									    valBitacora = false;
			                        }
								}
								//retoma el flujo
							    if( valida!=null && valida.equals("1")) {

									if (valBitacora)
									{
						        		try{
						        			EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);
						        			HttpSession sessionHttp = request.getSession();
											HttpSession sessionBit = request.getSession(false);
											Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");
						        			int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
						        			String claveOperacion = BitaConstants.CTK_PAGO_NOMINA_INTERB;
						        			String concepto = BitaConstants.CTK_CONCEPTO_PAGO_NOMINA_INTERB;
						        			String cuenta ="0";
						        			String importeStr = "0";
						        			double importeDouble = 0;
						        			String token = "0";
						        			if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
											{token = request.getParameter("token");}
											if (session.getImpTotalNom() != null && !session.getImpTotalNom().equals(""))
											{importeStr =  agregarPunto(session.getImpTotalNom().trim());
											EIGlobal.mensajePorTrace( "*******----------importe $$$$$$$$$$--a->"+ importeStr, EIGlobal.NivelLog.INFO);
											importeDouble = Double.valueOf(importeStr).doubleValue();
											}
											if (getFormParameter(request, "textcuenta") != null && !getFormParameter(request, "textcuenta").equals(""))
											{cuenta = getFormParameter(request, "textcuenta");
						        			EIGlobal.mensajePorTrace( "*******----------cuenta $$$$$$$$$$--a->"+ cuenta, EIGlobal.NivelLog.INFO);}

						    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
						        			EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
						        			EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
						        			EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
						        			EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
						        			EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

						        			String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
						        		} catch(Exception e) {
						        			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						        		}
									}
									EI_Exportar arcTmp=new EI_Exportar(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses");
									if(arcTmp.creaArchivo())
										EIGlobal.mensajePorTrace( "OperacionNominaInter El archivo.ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);

									request.getSession().setAttribute("Recarga_Opc_Nomina","TRUE");



									ArchivoRemoto archR = new ArchivoRemoto();
									String nombre_archivo = (String)sess.getAttribute("nombreArchivo");
									String nA			= nombre_archivo.substring(nombre_archivo.lastIndexOf('/')+1,nombre_archivo.length());
									//String nA			= session.getNameFileNomina().substring(session.getNameFileNomina().lastIndexOf('/')+1,session.getNameFileNomina().length());
									String ruta			= "/gfi/desa/web/internet/EnlaceMig/Download";
									File archivoEnvio	= new File(Global.DOWNLOAD_PATH+session.getUserID8()+".tmp");
									String totalRegistros	= "";
									String fechaCargo		= "";
									String importeCargo		= "";
									String resultadoTransmision = "";
					                RandomAccessFile archivoEnvioPago = null;// Modificacion RMM 20021218 no se puede eliminar randomacces por usar metodo especifico
									try {
										if(archivoEnvio.exists())
										archivoEnvio.delete();
										archivoEnvioPago = new RandomAccessFile(Global.DOWNLOAD_PATH+session.getUserID8()+".tmp", "rw");// Modificacion RMM 20021218


										/*JPPM*/ log("se creo el archivo"+ Global.DOWNLOAD_PATH + session.getUserID8() + ".tmp");
										/*JEV
										archivoEnvioPago = new RandomAccessFile("/tmp/"+session.getUserID8()+".rnt", "rw");// Modificacion RMM 20021218
										JEV*/

										EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Se va a crear el archivo _2", EIGlobal.NivelLog.DEBUG);
										ArchivosNominaInter archivoP	  = new ArchivosNominaInter(Global.DOWNLOAD_PATH+nA,request);

										String encabezadoArchivoImportado = archivoP.leeEncabezadoImportado();
										EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class encabezadoArchivo: " +encabezadoArchivoImportado, EIGlobal.NivelLog.INFO);

										String[] detalleArchivoImportado  = archivoP.leeDetalleImportado();
										String contratoCargo              = session.getContractNumber();
										totalRegistros                    = session.getTotalRegsNom();
										importeCargo					  = agregarPunto(session.getImpTotalNom().trim());
										fechaCargo = getFormParameter(request, "txtFechaLimite");
										String cuentaCargotxt = cuentaCargo.trim();

										fechaCargo = fechaCargo.substring(0,10);

										String encabezadoArchivoPago	  = contratoCargo+"|"+totalRegistros+"|"+importeCargo+"|"+fechaCargo+"|"+cuentaCargotxt+"|\r\n";
										archivoEnvioPago.writeBytes(encabezadoArchivoPago);
										//Q05-0044906 - Praxis NVS - 051117
										String cuentaAbono = "", importeAbono="", bancoAbono="", plazaAbono="", nombreAbono="", conceptoAbono="", referenciaAbono="";
										String detalleArchivoPago = cuentaAbono+importeAbono+bancoAbono+plazaAbono+nombreAbono;
										String[] registrosArchivoPagos	  = new String[detalleArchivoImportado.length];

										// clabe ******************
										// detalleArchivoImportado = obtiene los registros del archivo importado � nuevo

										EIGlobal.mensajePorTrace( "*** NVS --> OpcionesNominaInter.class detalle del archivo 002", EIGlobal.NivelLog.DEBUG);

										for(int i=0; i<detalleArchivoImportado.length;i++){
										    //Q05-0044906 - Praxis NVS - 051117
										    int fixlength = 0;
										    if (detalleArchivoImportado[i].length() == 154){
										        fixlength = 2;
										    }
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" long: "+detalleArchivoImportado[i].length(), EIGlobal.NivelLog.INFO);
											cuentaAbono		= detalleArchivoImportado[i].substring(61,81);
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" cuenta abono: "+cuentaAbono, EIGlobal.NivelLog.INFO);
											importeAbono	= detalleArchivoImportado[i].substring(81,99 - fixlength);
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
											importeAbono	= importeAbono.trim();
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
											importeAbono	= agregarPunto(importeAbono);
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" importeAbono: "+importeAbono, EIGlobal.NivelLog.INFO);
											bancoAbono		= detalleArchivoImportado[i].substring(99 - fixlength,104 - fixlength);
											//if(detalleArchivoImportado[i].length()>109)
											 plazaAbono		= detalleArchivoImportado[i].substring(104 - fixlength,109 - fixlength);
											//else
											//plazaAbono		= detalleArchivoImportado[i].substring(104);
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" plazaAbono: "+plazaAbono, EIGlobal.NivelLog.INFO);
											//***************************
											nombreAbono		= detalleArchivoImportado[i].substring(6,56).trim();
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" nombreAbono: "+nombreAbono, EIGlobal.NivelLog.INFO);
											//Q05-0044906 - Praxis NVS - 051117
											if(detalleArchivoImportado[i].length() > 109) {
					                                                conceptoAbono = detalleArchivoImportado[i].substring(109 - fixlength, 149 - fixlength).trim();
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+conceptoAbono, EIGlobal.NivelLog.INFO);
											referenciaAbono = detalleArchivoImportado[i].substring(149 - fixlength, 156 - fixlength).trim();
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+referenciaAbono, EIGlobal.NivelLog.INFO);
				                            } else {
												conceptoAbono = "";
												//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+conceptoAbono, EIGlobal.NivelLog.INFO);
												referenciaAbono = "";
												//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Linea: "+i+" conceptoAbono: "+referenciaAbono, EIGlobal.NivelLog.INFO);
											}
											detalleArchivoPago=cuentaAbono+"|"+importeAbono+"|"+bancoAbono+"|"+plazaAbono+"|"+nombreAbono+"|"+conceptoAbono+"|"+referenciaAbono+"|\r\n";
											registrosArchivoPagos[i]=detalleArchivoPago;
											//EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class Enviando al archivo.", EIGlobal.NivelLog.INFO);
											archivoEnvioPago.writeBytes(detalleArchivoPago);
										}
										archivoEnvioPago.close();
									} catch(IOException e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									} finally{// Modificacion RMM 20021218 inicio
					                    if(null != archivoEnvioPago) {
											try{
												archivoEnvioPago.close();
											}catch(Exception e){
												EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											}
											archivoEnvioPago = null;
					                    }
									}// Modificacion RMM 20021218 fin

									EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class se obtuvo detalle.", EIGlobal.NivelLog.DEBUG);

									File oldFile=new File(Global.DOWNLOAD_PATH+nA);

									EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class otro archivo nA: "+ nA, EIGlobal.NivelLog.INFO);

									if(archR.copiaLocalARemoto(session.getUserID8()+".tmp")){
										EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo se copio correctamente", EIGlobal.NivelLog.DEBUG);
									}
									else{
										EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo no se copio correctamente", EIGlobal.NivelLog.INFO);
									}

									//String cuenta	   = request.getParameter("cuenta");
									//*******************Modificaci�n 21-02-2002
									String folio = getFormParameter(request, "folio");

									EIGlobal.mensajePorTrace( "***Visualizando folio: "+ folio, EIGlobal.NivelLog.INFO);

									if(folio==null||folio.trim().equals(""))
										folio = " ";
									String tramaEnviada ="";

									if( folio==null||folio.trim().equals("") ){
										tramaEnviada="2EWEB|"+session.getUserID8()+"|INTE|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"| |" + agregarPunto(session.getImpTotalNom().trim()) + "| |";
									} else {
										/*Modificaci�n (EIC) req.AMX-06-00213-0A. Formateo de Cuenta cargo y folio
										tramaEnviada="2EWEB|"+session.getUserID8()+"|INTE|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+request.getParameter("textcuenta")+"|"+agregarPunto(session.getImpTotalNom().trim())+"|"+folio+"|"; */
										tramaEnviada="2EWEB|"+session.getUserID8()+"|INTE|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+getFormParameter(request, "textcuenta").trim()+"|"+agregarPunto(session.getImpTotalNom().trim())+"|"+folio.trim()+"|";
										//tramaEnviada="2EWEB|"+session.getUserID8()+"|INTE|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|" + cuenta + "|"+agregarPunto(session.getImpTotalNom().trim())+"|"+folio+"|";
									}
									//*********************************
									//MSD Q05-0029909 inicio lineas agregadas
									String IP = " ";
									String fechaHr = "";												//variables locales al m�todo
									IP = request.getRemoteAddr();											//ObtenerIP
									java.util.Date fechaHrAct = new java.util.Date();
									SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
									fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
									//MSD Q05-0029909 fin lineas agregada

									EIGlobal.mensajePorTrace(fechaHr+"***OpcionesNominaInter.class La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
									ServicioTux tuxGlobal = new ServicioTux();
									//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
									Hashtable htResult=null;
									try{
										htResult=tuxGlobal.web_red(tramaEnviada);
									} catch(java.rmi.RemoteException re) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
									} catch(Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									}
									String CodError = "";

									String cod_error = "";
									if(htResult != null) {
										CodError = ( String ) htResult.get( "BUFFER" );
										cod_error = (String) htResult.get("COD_ERROR");
										htResult.clear();
										htResult = null;
									}

									EIGlobal.mensajePorTrace("***OpcionesNominaInter.class El servicio regreso CodError: "+CodError, EIGlobal.NivelLog.DEBUG);

									if(!( CodError.trim().lastIndexOf("/")>0)||!(CodError.trim().indexOf(session.getUserID8().trim())>0) ||  CodError.equals ("null") ||  CodError.equals ("")  ) {
										String mmensaje = "";

										if(cod_error.equals("MANC0018")) {
											mmensaje = CodError.substring(16);
											EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Mensaje de retorno Mancomunidad: "+mmensaje, EIGlobal.NivelLog.DEBUG);
										} else if(CodError.length()>8) {
											mmensaje = CodError.substring(8);
										} else{
											mmensaje = CodError;
										}

										String infoUser=mmensaje+"<br>revise y transmita nuevamente el archivo";
										infoUser="cuadroDialogo (\""+ infoUser +"\",1)";
										request.setAttribute("infoUser", infoUser);
										/*request.setAttribute("MenuPrincipal", session.getStrMenu());
										request.setAttribute("newMenu", session.getFuncionesDeMenu());
										request.setAttribute("textcuenta", "" );
										request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s25800h",request));
										request.setAttribute("textcuenta",cuentaCargo);
										evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL,request, response);*/
										/*EIGlobal.mensajePorTrace("***OpcionesNomina.class - No se obtuvo informacion del servicio IN04>>", EIGlobal.NivelLog.DEBUG);
										despliegaPaginaError("Servicio de Pago de N&oacute;mina no disponible","Pagos de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25830h", request, response);*/
										bandera=false;
										transaccionExitosa = false;
										//out.close();
									}

									EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(2) CodError.lastIndexOf('/') --" + CodError.lastIndexOf('/') + "--", EIGlobal.NivelLog.INFO);
									String nombreArchRegreso = "";
									String contenido = "";

									if (CodError.lastIndexOf('/') > 0) {
										nombreArchRegreso = CodError.substring(CodError.lastIndexOf('/')+1);
										if(archR.copia(nombreArchRegreso))
											EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo se copio correctamente", EIGlobal.NivelLog.DEBUG);
										else
										EIGlobal.mensajePorTrace("***OpcionesNominaInter.class el archivo no se copio correctamente", EIGlobal.NivelLog.INFO);

										nombreArchRegreso=Global.DIRECTORIO_LOCAL+"/"+nombreArchRegreso;
										ArchivosNominaInter archivoRegreso= new ArchivosNominaInter(nombreArchRegreso,request);
										//boolean transExitosa=false;
										 contenido = archivoRegreso.lecturaArchivoRegreso();
										try{
											if (contenido.equals("")){
												resultadoTransmision= "Servicio de Pago de N&oacute;mina no disponible.";
												transaccionExitosa = false;
											} else if((contenido.startsWith("OK"))&&(contenido.substring(16,24).equals("INTN0000"))){
											EIGlobal.mensajePorTrace("***OpcionesNominaInter.class El pago se ha realizado correctamente&", EIGlobal.NivelLog.DEBUG);
												resultadoTransmision=contenido.substring(24,contenido.indexOf('.'));
												transaccionExitosa = true;
												//transExitosa=true;
											} else if(contenido.startsWith("MANC")){
											EIGlobal.mensajePorTrace("***OpcionesNominaInter.class El usuario esta mancumunado&", EIGlobal.NivelLog.DEBUG);
													resultadoTransmision  = contenido.substring(16,contenido.length());
													transaccionExitosa = false;
											        // resultadoTransmision = contenido.substring(0,contenido.indexOf('.'));
							 		        } else if(!(contenido.startsWith("OK"))&&(!contenido.substring(16,24).equals("INTN0000")) || contenido.indexOf("Errores") > 1 ){
												EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(1) El pago no se ha realizado correctamente", EIGlobal.NivelLog.INFO);
												resultadoTransmision=contenido.substring(24,contenido.length());
												//resultadoTransmision=contenido;
												//resultadoTransmision = contenido.substring(24,contenido.indexOf('.'));

												transaccionExitosa = false;

												//transExitosa=false;
											} else if (contenido.indexOf("No existe folio") > 0){
											EIGlobal.mensajePorTrace("\n\n***Error, operacion no sera exitosa no existe folio***\n", EIGlobal.NivelLog.INFO);
													resultadoTransmision = "No existe folio.";
													transaccionExitosa =false;
													//transExitosa=false;
											} else if ((contenido.startsWith("OK"))&&(!contenido.substring(16,24).equals("INTN0000")) ) {
											EIGlobal.mensajePorTrace("\n\n***Error, operacion no sera exitosa pago incorrecto***\n", EIGlobal.NivelLog.INFO);
													 EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(2) El pago no se ha realizado correctamente", EIGlobal.NivelLog.INFO);
													resultadoTransmision=contenido.substring(24,contenido.length());
													transaccionExitosa = false;
											}
										} catch(Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											transaccionExitosa = false;
											//transExitosa=false;
										}
									} else {
										EIGlobal.mensajePorTrace("***OpcionesNominaInter.class(2) No se genero un archivo de respuesta", EIGlobal.NivelLog.INFO);
										transaccionExitosa = false;
									}

									//************************
									//if(transExitosa){
									if(transaccionExitosa ) {

										//************************
										String resumenTransaccion="";

										String importeTotaltmp="";
										String comisionTransacciontmp="";
										String ivaComisiontmp="";
										String totalPagotmp="";

										String importeTotal="";
										String comisionTransaccion="";
										String ivaComision="";
										String totalPago="";

										String tablaTotales="<tr>"+
											"		<td>"+
											"<table width=\"460\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\">"+
											"<tr> "+
											"                  <td class=\"tittabdat\" colspan=\"2\"> Totales</td>"+
											"                </tr>"+
											"                <tr align=\"center\"> "+
											"                  <td class=\"textabdatcla\" valign=\"top\" colspan=\"2\"> "+
											"                    <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
											"                      <tr valign=\"top\"> "+
											"                        <td width=\"270\" align=\"right\"> "+
											"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap>Importe de n&oacute;mina:</td>"+
											"                            </tr>"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap> "+
											"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+importeTotal+" NAME=\"cargoNomina\" onFocus='blur();' >"+
											"                              </td>"+
											"                            </tr>"+
											"                          </table>"+
											"                        </td>"+
											"                        <td width=\"180\" align=\"right\"> "+
											"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap>Comisi&oacute;n:</td>"+
											"                            </tr>"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap> "+
											"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+comisionTransaccion+" NAME=\"comision\" onFocus='blur();' >"+
											"                              </td>"+
											"                            </tr>"+
											"                          </table>"+
											"                        </td>"+
											"                        <td width=\"180\" align=\"right\"> "+
											"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap>Iva de comisi&oacute;n:</td>"+
											"                            </tr>"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap> "+
											"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+ivaComision+" NAME=\"iva\" onFocus='blur();' >"+
											"                              </td>"+
											"                            </tr>"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap>Total del cargo:</td>"+
											"                            </tr>"+
											"                            <tr> "+
											"                              <td class=\"tabmovtex\" nowrap> "+
											"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+totalPago+" NAME=\"total\" onFocus='blur();' >"+
											"                              </td>"+
											"                            </tr>"+
											"                          </table>"+
											"                        </td>"+
											"                      </tr>"+
											"                    </table>"+
											"                  </td>"+
											"                </tr>"+
											"              </table>"+
											"            </td>"+
											"            <td align=\"center\" valign=\"bottom\">&nbsp;</td>"+
											"          </tr>"+
											"        </table>";


										//if(transExitosa){
										resumenTransaccion=contenido.substring(contenido.lastIndexOf('/')+5,contenido.length());
										String numSecuencia = "";
										String registrosAceptados = "";
										String regsRechazados = "";
										try {
											numSecuencia=resumenTransaccion.substring(0,resumenTransaccion.indexOf('|'));
											registrosAceptados=resumenTransaccion.substring(resumenTransaccion.indexOf('|')+1,posCar(resumenTransaccion,'|',2));

											importeTotaltmp=resumenTransaccion.substring(posCar(resumenTransaccion,'|',2)+1,posCar(resumenTransaccion,'|',3));
											comisionTransacciontmp=resumenTransaccion.substring(posCar(resumenTransaccion,'|',3)+1,posCar(resumenTransaccion,'|',4));
											ivaComisiontmp=resumenTransaccion.substring(posCar(resumenTransaccion,'|',4)+1,posCar(resumenTransaccion,'|',5));
											totalPagotmp=resumenTransaccion.substring(posCar(resumenTransaccion,'|',5)+1,posCar(resumenTransaccion,'|',6));
											 importeTotal = importeTotaltmp.substring(0,importeTotaltmp.indexOf(".")+ 3);
											 comisionTransaccion = comisionTransacciontmp.substring(0,comisionTransacciontmp.indexOf(".")+ 3);
											 ivaComision = ivaComisiontmp.substring(0,ivaComisiontmp.indexOf(".")+ 3);
											 totalPago = totalPagotmp.substring(0,totalPagotmp.indexOf(".")+ 3);

											regsRechazados=Integer.toString((Integer.parseInt(totalRegistros)-Integer.parseInt(registrosAceptados)));
										} catch(Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}
										tablaTotales="<tr>"+
															"		<td>"+
															"<table width=\"460\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\">"+
															"<tr> "+
															"                  <td class=\"tittabdat\" colspan=\"2\"> Totales</td>"+
															"                </tr>"+
															"                <tr align=\"center\"> "+
															"                  <td class=\"textabdatcla\" valign=\"top\" colspan=\"2\"> "+
															"                    <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
															"                      <tr valign=\"top\"> "+
															"                        <td width=\"270\" align=\"right\"> "+
															"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap>Importe de n&oacute;mina:</td>"+
															"                            </tr>"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap> "+
															"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+importeTotal+" NAME=\"cargoNomina\" onFocus='blur();' >"+
															"                              </td>"+
															"                            </tr>"+
															"                          </table>"+
															"                        </td>"+
															"                        <td width=\"180\" align=\"right\"> "+
															"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap>Comisi&oacute;n:</td>"+
															"                            </tr>"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap> "+
															"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+comisionTransaccion+" NAME=\"comision\" onFocus='blur();' >"+
															"                              </td>"+
															"                            </tr>"+
															"                          </table>"+
															"                        </td>"+
															"                        <td width=\"180\" align=\"right\"> "+
															"                          <table width=\"150\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap>Iva de comisi&oacute;n:</td>"+
															"                            </tr>"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap> "+
															"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+ivaComision+" NAME=\"iva\" onFocus='blur();' >"+
															"                              </td>"+
															"                            </tr>"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap>Total del cargo:</td>"+
															"                            </tr>"+
															"                            <tr> "+
															"                              <td class=\"tabmovtex\" nowrap> "+
															"                                <input type=\"text\" size=\"22\" class=\"tabmovtex\" VALUE="+totalPago+" NAME=\"total\" onFocus='blur();' >"+
															"                              </td>"+
															"                            </tr>"+
															"                          </table>"+
															"                        </td>"+
															"                      </tr>"+
															"                    </table>"+
															"                  </td>"+
															"                </tr>"+
															"              </table>"+
															"            </td>"+
															"            <td align=\"center\" valign=\"bottom\">&nbsp;</td>"+
															"          </tr>"+
															"        </table>";

										request.setAttribute("transmision",numSecuencia);
										String nombreArchivo = (String)sess.getAttribute("nombreArchivo");
										request.setAttribute("archivo_actual",	nombre_archivo.substring(nombre_archivo.lastIndexOf('/')+1,nombre_archivo.length()));
										//request.setAttribute("archivo_actual",	session.getNameFileNomina().substring(session.getNameFileNomina().lastIndexOf('/')+1,session.getNameFileNomina().length()));
										//request.setAttribute("Cuentas","<Option value = " + session.getCuentaCargo() + ">" + session.getCuentaCargo());
										//request.setAttribute("cuenta",session.getCuentaCargo() );
										request.setAttribute("textcuenta",cuentaCargo);
//					request.setAttribute("textcuenta", session.getCuentaCargo()) ;
						                request.setAttribute("regsEnviados",totalRegistros);
							            request.setAttribute("regsAceptados",registrosAceptados);
								        request.setAttribute("regsRechazados",regsRechazados);
									    request.setAttribute("fechaTransmision",fechaCargo);
										request.setAttribute("importeEnviado",importeCargo);
					                    request.setAttribute("importeAceptado",importeTotal);
					                    request.setAttribute("comision",comisionTransaccion);
					                    request.setAttribute("iva",ivaComision);
						                request.setAttribute("total",totalPago);

										request.setAttribute("TablaTotales",tablaTotales);

										String infoUser=resultadoTransmision+"<br> Se enviaron "+registrosAceptados+" registros, se aceptaron " + registrosAceptados + " registros, se rechazaron "+ regsRechazados+ " se enviaron $"+ importeTotal.substring(0,importeTotal.indexOf('.')+3) + " se aceptaron $"+importeTotal.substring(0,importeTotal.indexOf('.')+3);
									    infoUser="cuadroDialogoMedidas (\""+ infoUser +"\",1,380,200)";
							            request.setAttribute("infoUser", infoUser);


							            int regAceptadosInt = 0;
							            try {
							            	regAceptadosInt = Integer.parseInt(registrosAceptados.trim());
							            } catch (Exception e) {
							            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}

							            try {
						            	EmailSender emailSender = new EmailSender();
						    			EmailDetails beanEmailDetails = new EmailDetails();
							    		if (emailSender.enviaNotificacionArchivo(cod_error)) {
							    			int noRegistros = 0;

							    			try {
							    				noRegistros = Integer.parseInt(totalRegistros.trim());
							    			} catch (Exception e) {
							    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											}

							    			EIGlobal.mensajePorTrace("�������������������� Notificacion en OpcionesNominaInter", EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� Se debe enviar notificacion? " + emailSender.enviaNotificacionArchivo(cod_error), EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� CuentaCargo ->" + cuentaCargo, EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� Importe ->" + totalPago, EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� No de Transmision ->" + numSecuencia, EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� Registros Importados ->" + noRegistros, EIGlobal.NivelLog.INFO);
							    			EIGlobal.mensajePorTrace("�������������������� Estatus ->" + cod_error, EIGlobal.NivelLog.INFO);

											if(cuentaCargo == null){
												cuentaCargo = "";
											}
							    			beanEmailDetails.setNumeroContrato(session.getContractNumber());
							    			beanEmailDetails.setRazonSocial(session.getNombreContrato());
							    			beanEmailDetails.setNumCuentaCargo(cuentaCargo.trim());
							    			beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(totalPago));
							    			beanEmailDetails.setNumRef(numSecuencia);
							    			beanEmailDetails.setNumTransmision(numSecuencia);
							    			beanEmailDetails.setNumRegImportados(noRegistros);
							    			beanEmailDetails.setEstatusActual(cod_error);
							    			beanEmailDetails.setTipoPagoNomina("Interbancaria");


							    				emailSender.sendNotificacion(request, IEnlace.PAGO_NOMINA_INTER, beanEmailDetails);

							            }
							            } catch (Exception e) {
						    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}

//							          TODO: BIT CU 4161
										/*
							    		 * VSWF ARR -I
							    		 */
							            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
										try{
										BitaHelper bh = new BitaHelperImpl(request, session,sess);
										BitaTransacBean bt = new BitaTransacBean();
										bt = (BitaTransacBean)bh.llenarBean(bt);
										bt.setNumBit(BitaConstants.ES_NOMINA_INTERBANCARIA_IMPORTAR_ARCHIVO);
										bt.setContrato(session.getContractNumber());
										if(cuentaCargo!=null && !cuentaCargo.equals(""))
											bt.setCctaOrig(cuentaCargo);
										bt.setNombreArchivo(nombre_archivo.substring((nombre_archivo.lastIndexOf("\\")+1),nombre_archivo.length()));
										if(CodError!=null){
											 if(CodError.substring(0,2).equals("OK")){
												   bt.setIdErr("INTE0000");
											 }else if(CodError.length()>8){
												  bt.setIdErr(CodError.substring(0,8));
											 }else{
												  bt.setIdErr(CodError.trim());
											 }
											}
										if(importeTotal!=null && !importeTotal.equals(""))
											bt.setImporte(Double.parseDouble(importeTotal));
										bt.setServTransTux("INTE");
										if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
											&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
													.equals(BitaConstants.VALIDA)){
											bt.setIdToken(session.getToken().getSerialNumber());
										}
										//VSWF RRG I  08-May-2008 Agrego el beneficiario a la bitacora
										bt.setBeneficiario(beneficiario);
										//VSWF RRG F  08-May-2008
										BitaHandler.getInstance().insertBitaTransac(bt);
										}catch (SQLException e){
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}catch(Exception e){
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}
							            }
							    		/*
							    		 * VSWF ARR -F
							    		 */
										/**
										* Facultades Nomina Interbancaria
										*
										*/
										if (sess.getAttribute("facultadesN") != null)
											request.setAttribute( "facultades", sess.getAttribute("facultadesN") );
										request.setAttribute("tipoArchivo","enviado");
									} else {  //transaccionExitosa <-- false
										// IM199738. borrar variables de control de recarga por que transaccion no se completo OK.
										/***********************limpieza de variables de Sesion*****************************/

										verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",true);

										if(request.getSession().getAttribute("Recarga_Opc_Nomina")!=null)
										{
											request.getSession().removeAttribute("Recarga_Opc_Nomina");
											EIGlobal.mensajePorTrace( "OpcionesNominaInter Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
										}
										else
											EIGlobal.mensajePorTrace( "OpcionesNominaInter La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);

										/**********************Termina limpieza de variables de Sesion**********************/

										if (bandera){
											String infoUser=resultadoTransmision;//+"<br>Revise y transmita nuevamente el archivo";
											infoUser="cuadroDialogo (\""+ infoUser +"\",1)";
											request.setAttribute("infoUser", infoUser);
											request.setAttribute("CuentasArchivo", session.getCuentasArchivo());
											request.setAttribute("textcuenta", "" );
										} // bandera

									} // else transaccion Exitosa

									request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s25800h",request));
									request.setAttribute("CuentasArchivo", session.getCuentasArchivo());
									evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL,request, response);
					             	transaccionExitosa=true;

								} //Termina else de la validaci�n del OTP
							}//fin de la condicion banderaTransaccion IM199738
							else
							{
								EIGlobal.mensajePorTrace("***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);

								String infoUser= "<br>Su transacci&oacute;n ya est&aacute; en proceso, Verifique posteriormente resultados";
								infoUser="cuadroDialogo (\""+ infoUser +"\",1)";
								request.setAttribute("infoUser", infoUser);
								request.setAttribute("operacion","tranopera");  //antes "valida"

							    String nombre_archivo = (String)sess.getAttribute("nombreArchivo");
							    String nA			= nombre_archivo.substring(nombre_archivo.lastIndexOf('/')+1,nombre_archivo.length());
								request.setAttribute("nombreArchivo",nA);

								request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());

								request.setAttribute("MenuPrincipal", session.getStrMenu());
								request.setAttribute("newMenu", session.getFuncionesDeMenu());
								request.setAttribute("textcuenta", "" );
								//request.setAttribute("Encabezado", CreaEncabezado("Transacci&oacute;n en Proceso","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s25800h",request));
								request.setAttribute("Encabezado", CreaEncabezado("Transacci&oacute;n en Proceso","Servicios &gt; N&oacute;mina &gt; Pago de N&oacute;mina Interbancaria", "s55260",request));
								request.setAttribute("textcuenta",cuentaCargo);
								//Agregado IM199738 atributo de fecha para no duplicados
								request.setAttribute("txtFechaLimite", "");
								evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
							}


						}//fin de la condicion operacion=envio



				}//fin del else

				if( strOperacion.equals("cancelar")){
					session.setCuentasArchivo(getFormParameter(request, "CuentasArchivo"));
					//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado"));
					sess.setAttribute("numeroEmp",getFormParameter(request, "NumerosEmpleado"));
					leerArchivo( request, response, tipoArchivotmp, nominaCal, laFechaHoy );
				}

				if ( strOperacion.equals( "borrar" ) ) {
					try { //Si ya existe el archivo lo borra
						//File archivo=new File(session.getNameFileNomina() );
						File archivo=new File((String)sess.getAttribute("nombreArchivo") );
						archivo.delete();
					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
					diasNoHabiles = nominaCal.CargarDias();
					VarFechaHoy  = nominaCal.armaArregloFechas();

	                //Modificacion para integracion
					/*
					for ( int indice = 1; indice <= Integer.parseInt( arrayCuentas[0][0] ); indice++ ) {
						tipo_cuenta = arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
						if ( arrayCuentas[indice][2].equals( "P" ) &&
								( !tipo_cuenta.equals( "BM" ) ) &&
								( !tipo_cuenta.equals( "SI" ) ) && ( !tipo_cuenta.equals( "49" ) ) )
							Cuentas += "<OPTION value = " + arrayCuentas[indice][1] + ">" +
									arrayCuentas[indice][1] + "\n";
					}*/
					/*String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
					datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
					datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";*/
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("Encabezado",
							CreaEncabezado( "Pago de N&oacute;mina Interbancaria",
									"Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h",request ) );
					//request.setAttribute("Cuentas",
					//		"<Option value = >Seleccione una cuenta " + Cuentas);
					//String cuentaCargotxt				  = request.getParameter("textcuenta");
					//request.setAttribute("textcuenta",	cuentaCargotxt);
					request.setAttribute("textcuenta",cuentaCargo);
					request.setAttribute("VarFechaHoy", VarFechaHoy);
					request.setAttribute("fechaFin", strFechaFin);
					request.setAttribute("diasInhabiles", strInhabiles);
					request.setAttribute("Fechas", ObtenFecha(true));
					request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06" );
					request.setAttribute("ContenidoArchivo", "");

					/**
					* Facultades Nomina Interbancaria
					*
					*/
					if (sess.getAttribute("facultadesN") != null)
					   request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

					request.setAttribute("totRegs", "" );
					sess.setAttribute("numeroEmp",getFormParameter(request, "NumerosEmpleado"));
					//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado") );
					evalTemplate("/jsp/MantenimientoNominaInter.jsp", request, response );
				}

/************************************************************** ALTA ****************/
				if ( ( transaccionExitosa == false && strOperacion.equals( "envio" ) ) ||
						strOperacion.equals( "alta" ) ) {
					// La operaci�n es ALTA, ENVIO
				    //clabe Aqui se obtiene el valor de la variable uso_cta_cheques nota: falta ponerla en la session y colocarla cuando sea una modificaci�n.
					EIGlobal.mensajePorTrace("OpcionesNominaInter operacion Alta ------------------> ", EIGlobal.NivelLog.DEBUG);

					String uso_cta_cheques = Global.USO_CTA_CHEQUE; //se obtiene de global
					request.setAttribute("uso_cta_clabe", uso_cta_cheques);

					//clabe ********************
					String valorCuentaClabe   = "<input type=radio value=0 name=TipoCuenta checked>Cuenta CLABE";
					String valorCuentaTarjeta = "<input type=radio value=0 name=TipoCuenta >Tarjeta de D&eacute;bito";
					request.setAttribute("CuentaClabe",valorCuentaClabe);
					request.setAttribute("CuentaTarjeta" ,valorCuentaTarjeta);
					//*************************

					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("Encabezado",
							CreaEncabezado( "Pago de N&oacute;mina Interbancaria",
									"Servicios &gt; N&oacute;mina &gt; Altas", "s25810h",request ) );
					request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25480.gif");
					request.setAttribute("altImagen","Alta");
					request.setAttribute("CuentasArchivo", session.getCuentasArchivo());
					//request.setAttribute("NumerosEmpleado", session.getNumerosEmpleado());
					if (sess.getAttribute("numeroEmp") != null)
					{
					request.setAttribute("NumerosEmpleado", sess.getAttribute("numeroEmp") );
					}

					/**
					* Facultades Nomina Interbancaria
					*
					*/
					if (sess.getAttribute("facultadesN") != null)
					  request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

					request.setAttribute("comboBancos", comboBancos(request));
					request.setAttribute("comboPlazas", comboPlazas());
					request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
					request.setAttribute("botLimpiar","<td align=left valign=top width=76><a href=javascript:js_limpiar();><img border=0 name=boton src=/gifs/EnlaceMig/gbo25250.gif width=76 height=22 alt=Limpiar></a></td>");
					EIGlobal.mensajePorTrace("Mando todos los parametros en alta -----------------------------> ", EIGlobal.NivelLog.DEBUG);
	   				evalTemplate( IEnlace.NOMINADATOSINTER_TMPL, request, response );
				}


/************************************ RECUPERAR ARCHIVO ************************************/
				if ( strOperacion.equals( "recuperar" ) )
				 {
					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class & La operacion fue recuperar.", EIGlobal.NivelLog.DEBUG);
					String empleado=session.getUserID8();
					String contrato=session.getContractNumber();
					secuencia_rec = getFormParameter(request, "secuencia" );

					ServicioTux tuxGlobal = new ServicioTux();
					//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					String perfil= session.getUserProfile();
					Hashtable htResult = null;
					//el servicio IN10 es uasdo para la recuperacion dearchivos dentro del servidor, este
					//servicio regresa un archivo que tiene como nombre el perfil usado
					String trama_entrada="2EWEB|"+empleado+"|INTA|"+contrato+"|"+empleado+"|"+perfil+"|"+contrato+"@"+secuencia_rec+"@";

					 //MSD Q05-0029909 inicio lineas agregadas
					 String IP = " ";
					 String fechaHr = "";												//variables locales al m�todo
					 IP = request.getRemoteAddr();											//ObtenerIP
					 java.util.Date fechaHrAct = new java.util.Date();
					 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
					 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
					 //MSD Q05-0029909 fin lineas agregada


				 EIGlobal.mensajePorTrace(fechaHr+"OpcionesNominaInter - Trama"+trama_entrada, EIGlobal.NivelLog.DEBUG);


					try {
						htResult = tuxGlobal.web_red( trama_entrada );
					} catch ( java.rmi.RemoteException re ) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

					String CodError3 = "";
					if(htResult != null) {
						CodError3 = ( String ) htResult.get( "BUFFER" );
						htResult.clear();
						htResult = null;
					}
					//String CodError3="/tmp/rada.txt";
					EIGlobal.mensajePorTrace("OpcionesNominaInter - El servicio Regreso: " +  CodError3, EIGlobal.NivelLog.INFO);

					if(CodError3 == null)
						CodError3=Global.DOWNLOAD_PATH +"ErrorEnServicio";
					//INTN0013NO SE PUDO ABRIR BASE DE DATOS

					//***********************modificacion
					if(CodError3.indexOf("@")>0)
						nombre_arch_salida = CodError3.substring(CodError3.lastIndexOf('/'),posCar(CodError3,'@',3));
					else
						nombre_arch_salida = CodError3.substring(CodError3.lastIndexOf('/')).trim();
					//************************

					String nombreExportar=nombre_arch_salida.replace('/',' ').trim()+".enom";

					ArchivoRemoto archR = new ArchivoRemoto();
					archR.copia( nombre_arch_salida );
					nombre_arch_salida = Global.DIRECTORIO_LOCAL + nombre_arch_salida;

					ArchivosNominaInter archSalida = new ArchivosNominaInter(  nombre_arch_salida,request );

					EIGlobal.mensajePorTrace("OpcionesNominaInter - Se esta analizando la informacion recibida", EIGlobal.NivelLog.DEBUG);
					String contenidoArchtmp = archSalida.lecturaArchivoSalida(request);
					EIGlobal.mensajePorTrace("OpcionesNominaInter - Se recibio: " + contenidoArchtmp , EIGlobal.NivelLog.INFO);

					String contenidoArch = "";
					String cantidadReg   = "";

					if (contenidoArchtmp != null && contenidoArchtmp.length() > 1 )
					 {
					   contenidoArch = contenidoArchtmp.substring(0,contenidoArchtmp.indexOf("|"));
	  				   cantidadReg = contenidoArchtmp.substring(contenidoArchtmp.indexOf("|")+1,contenidoArchtmp.indexOf("||"));
					 }
					//if (contenidoArch.equals("") || contenidoArch.indexOf("Error")  >= 0){
					if (contenidoArch.trim().equals("") || contenidoArch.indexOf("Error ") >= 0 || contenidoArch.startsWith("Error") )
					 {
					    EIGlobal.mensajePorTrace("***OpcionesNominaInter.class La lectura del archivo no regreso datos", EIGlobal.NivelLog.INFO);
						//despliegaPaginaError("Datos erroneos en el n&uacute;mero de secuencia","Pagos de N&oacute;mina Interbancaria", "Servicios &gt; N&oacute;mina &gt; Pagos &gt; Recuperaci&oacute;n de Archivos", request, response);

						//request.setAttribute("mensaje_js","cuadroDialogo (\"No se encontraron datos para la secuencia:"+ secuencia_rec"\" ,1)");
						//evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
						despliegaPaginaError("No se encontraron datos para la secuencia: "+secuencia_rec ,"Pagos de N&oacute;mina Interbancaria", "Servicios &gt; N&oacute;mina &gt; Pagos &gt; Recuperaci&oacute;n de Archivos", request, response);
						//request.setAttribute("mensaje_js","alert (\"No se encontraron datos para la secuencia: "+secuencia_rec+"\")");
					} else {

							//if(Integer.parseInt(cantidadReg) <= Global.NUM_REGISTROS_PAGINA){
		 					if(Integer.parseInt(cantidadReg) <= 30){
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h",request));
							request.setAttribute("transmision",secuencia_rec);
							request.setAttribute("regsEnviados",archSalida.asTotalRegistros);
							request.setAttribute("r_portran","0");
							request.setAttribute("regsAceptados",archSalida.asAceptados);
							request.setAttribute("regsRechazados",archSalida.asRechazados);
							request.setAttribute("fechaTransmision",archSalida.asFechaTransmision);
							request.setAttribute("ContenidoArchivo",contenidoArch);
							//request.setAttribute("Cuentas","<Option value = " + session.getCuentaCargo() + ">" + session.getCuentaCargo());
							request.setAttribute("textcuenta",cuentaCargo);
							//request.setAttribute("textcuenta",session.getCuentaCargo());
							request.setAttribute("tipoArchivo","recuperado");
							request.setAttribute("archivo_actual",	nombre_arch_salida.substring(nombre_arch_salida.lastIndexOf('/')+1));

							//String archivoName=session.getNameFileNomina();
							String archivoName=(String)sess.getAttribute("nombreArchivo");
							//ArchivoRemoto archR = new ArchivoRemoto();
							String nombreArchivoOriginal=archivoName.substring(archivoName.lastIndexOf("/")+1);
							archR.copiaLocalARemoto(nombreArchivoOriginal,"WEB");

							/*************************************/
							/*************** Exportacion ... 1  **/
							/*************************************/
							//if (session.getRutaDescarga() != null)
							//	request.setAttribute("DescargaArchivo",session.getRutaDescarga());
							if(generaArchivoParaExportar(nombre_arch_salida, session.getUserID8())==0)
							  request.setAttribute("DescargaArchivo","/Download/"+nombreExportar);

							/**
							* Facultades Nomina Interbancaria
							*
							*/
							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

							request.setAttribute("cantidadDeRegistrosEnArchivo",cantidadReg);
							evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
							}// del <=30
							//else if( Integer.parseInt(cantidadReg ) > Global.NUM_REGISTROS_PAGINA ) {
							else if( Integer.parseInt(cantidadReg ) > 30 )
							{
							contenidoArch="<table width=\"320\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">";
							contenidoArch=contenidoArch+"<tr align=\"center\"><td class=\"tittabdat\" align=\"center\">Pago de Nomina Interbancaria</td>";
							contenidoArch=contenidoArch+"<tr><td class=\"textabdatobs\" nowrap align=\"center\">Use una aplicacion local para ver y editar el archivo</td>";
							contenidoArch=contenidoArch+"</table>";


						/*	contenidoArch="<table width=\"320\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" align=\"center\">";
							contenidoArch=contenidoArch+"<tr align=\"center\"><td width=\"80\" class=\"tittabdat\" align=\"center\">Pago de N&oacute;mina Interbancaria</td>";
							contenidoArch=contenidoArch+"<tr><td class=\"textabdatobs\" nowrap align=\"center\">Use una aplicacion local para ver y editar el archivo</td>";
							contenidoArch=contenidoArch+"</table>";
						*/
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h",request));
							request.setAttribute("transmision",secuencia_rec);
							request.setAttribute("regsEnviados",archSalida.asTotalRegistros);
							request.setAttribute("r_portran","0");
							request.setAttribute("regsAceptados",archSalida.asAceptados);
							request.setAttribute("regsRechazados",archSalida.asRechazados);
							request.setAttribute("fechaTransmision",archSalida.asFechaTransmision);
							request.setAttribute("ContenidoArchivo",contenidoArch);
							//request.setAttribute("Cuentas","<Option value = " + session.getCuentaCargo() + ">" + session.getCuentaCargo());
							//request.setAttribute("textcuenta",session.getCuentaCargo());
							request.setAttribute("textcuenta",cuentaCargo);
							request.setAttribute("tipoArchivo","recuperado");
							request.setAttribute("archivo_actual",	nombre_arch_salida.substring(nombre_arch_salida.lastIndexOf('/')+1));

							//String archivoName=session.getNameFileNomina();
							String archivoName=(String)sess.getAttribute("nombreArchivo");
							//ArchivoRemoto archR = new ArchivoRemoto();
							String nombreArchivoOriginal=archivoName.substring(archivoName.lastIndexOf("/")+1);
							archR.copiaLocalARemoto(nombreArchivoOriginal,"WEB");

							/*************************************/
							/*************** Exportacion ... 2  **/
							/*************************************/
							//if (session.getRutaDescarga() != null)
							//	request.setAttribute("DescargaArchivo",session.getRutaDescarga());
							if(generaArchivoParaExportar(nombre_arch_salida,session.getUserID8())==0)
							  request.setAttribute("DescargaArchivo","/Download/"+nombreExportar);

							/**
							* Facultades Nomina Interbancaria
							*
							*/
							if (sess.getAttribute("facultadesN") != null)
							  request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

							request.setAttribute("cantidadDeRegistrosEnArchivo",cantidadReg);
							evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );

							}
						}
				}
/********************************** RECUPERAR ARCHIVO FIN **********************************/

				String arcRecuperado  = "";
				String tmpTipoArchivo = "";
				String registrosEnArchivo = "";
				if(strOperacion.equals("reporte")){
					request.setAttribute("archivo", "Interbancario");
					arcRecuperado=getFormParameter(request, "tipoArchivo");
					registrosEnArchivo=getFormParameter(request, "cantidadDeRegistrosEnArchivo");

					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Operacion Reporte.", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Archivo recuperado: "+arcRecuperado, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Registros en archivo: "+registrosEnArchivo, EIGlobal.NivelLog.INFO);

					arcRecuperado.trim();
					if (arcRecuperado.equals(""))
						arcRecuperado = "nuevo";
					tmpTipoArchivo = arcRecuperado.substring(0, 4);

					if(tmpTipoArchivo.equals("r") ){
						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class tmpTipoArchivo ="+tmpTipoArchivo, EIGlobal.NivelLog.INFO);
						nombre_arch_salida=Global.DOWNLOAD_PATH +getFormParameter(request, "archivo_actual");
					    String secuencia_recu = getFormParameter(request, "transmision" );
						ArchivosNominaInter archRecuperado =new ArchivosNominaInter(nombre_arch_salida,request);
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
						request.setAttribute("MenuPrincipal", session.getStrMenu());
						request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Altas", "s25810h",request));
						request.setAttribute("etiquetaNameFile", "Nombre del archivo: Archivo Recuperado");
						request.setAttribute("etiquetaNumTransmision", "Numero de Transmision: "+secuencia_recu);
						request.setAttribute("tipoArchivo", arcRecuperado);
						request.setAttribute("cantidadDeRegistrosEnArchivo",registrosEnArchivo);
						request.setAttribute("ContenidoArchivo", archRecuperado.reporteArchivoSalida());
					} else {
						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class tmpTipoArchivo ="+tmpTipoArchivo, EIGlobal.NivelLog.INFO);
						//String archivoName=session.getNameFileNomina();
						String archivoName=(String)sess.getAttribute("nombreArchivo");
						String nombreArchivoOriginal=archivoName.substring(archivoName.lastIndexOf("/")+1);

						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class archivoName: "+archivoName, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("***OpcionesNominaInter.class nombreArchivoOriginal: "+nombreArchivoOriginal, EIGlobal.NivelLog.INFO);

						//ArchivosNominaInter archNvoImportado=new ArchivosNominaInter(session.getNameFileNomina(),request);
						ArchivosNominaInter archNvoImportado=new ArchivosNominaInter((String)sess.getAttribute("nombreArchivo"),request);

						request.setAttribute("newMenu", session.getFuncionesDeMenu());
						request.setAttribute("MenuPrincipal", session.getStrMenu());
						request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Altas", "s25810h",request));
						request.setAttribute("etiquetaNameFile", "Nombre del archivo: "+nombreArchivoOriginal);
						request.setAttribute("etiquetaNumTransmision", "Numero de Transmision: Archivo no transmitido");
						request.setAttribute("tipoArchivo", arcRecuperado);
						request.setAttribute("cantidadDeRegistrosEnArchivo",registrosEnArchivo);
						request.setAttribute("ContenidoArchivo", archNvoImportado.reporteArchivo());
					}

					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Se finalizo el Reporte 1", EIGlobal.NivelLog.DEBUG);
					evalTemplate(IEnlace.REPORTE_NOMINA_TMPL, request, response );
					EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Se finalizo el Reporte 2", EIGlobal.NivelLog.DEBUG);

				}
			} //Termina else de valida OTP
		} else if ( sesionvalida ) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}
	  EIGlobal.mensajePorTrace("***OpcionesNominaInter.class Se finalizo el Reporte 3", EIGlobal.NivelLog.DEBUG);
	  if(diasNoHabiles != null) {
		  diasNoHabiles.clear();
		  diasNoHabiles = null;
	  }
	  return;
	}

	//metodo agregarPunto que exhibe en el template el importe con decimales


	 public String  agregarPunto(String importe)
		{
		String importeConPunto	= "";

		if(importe.length()<3)
		  importe="000"+importe;

		String cadena1			= importe.substring(0,importe.length()-2);
		cadena1					= cadena1.trim();
		String cadena2			= importe.substring(importe.length()-2, importe.length());
		importe					= cadena1+" "+cadena2;
		importeConPunto			= importe.replace(' ','.');
		return importeConPunto;
		}

	//metodo posCar que devuelve la posicion de un caracter en un String
	public int posCar(String cad,char car,int cant){
		int result=0,pos=0;
		for (int i=0;i<cant;i++){
			result=cad.indexOf(car,pos);
			pos=result+1;
			}
		return result;
	}

	public void leerArchivo( HttpServletRequest request, HttpServletResponse response, String tipoArchivo, CalendarNomina nominaCal, String laFechaHoy )
			throws IOException, ServletException {
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    Vector diasNoHabiles = null;
        String VarFechaHoy   = "";
        String strInhabiles  = "";

		try
			{
			  diasNoHabiles = nominaCal.CargarDias();
			  strInhabiles = nominaCal.armaDiasInhabilesJS();
			}catch(ArrayIndexOutOfBoundsException e)
			 {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				strInhabiles ="";
			 }

	    Calendar fechaFin    = nominaCal.calculaFechaFin();
        VarFechaHoy          = nominaCal.armaArregloFechas();


        String strFechaFin   = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
		String tmpTipoArchivo = "";
		String contenidoArch  = "";
		String cantidadReg	  = "";
	    String cuentaCargo=session.getCuentaCargo();
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/**
		* Facultades Nomina Interbancaria
		*
		*/
		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h",request));
		if (session.getTotalRegsNom() != null)
		request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
		request.setAttribute("VarFechaHoy",           VarFechaHoy);
        request.setAttribute("fechaFin",              strFechaFin);
        request.setAttribute("diasInhabiles",         strInhabiles);
        request.setAttribute("Fechas",                 ObtenFecha(true));
        request.setAttribute("FechaHoy",              ObtenFecha(false) + " - 06" );
		String systemHour=ObtenHora();
		request.setAttribute("horaSistema", systemHour);
		//String archivoName=session.getNameFileNomina();
		String archivoName	= (String)sess.getAttribute("nombreArchivo");
		//ArchivoRemoto archR = new ArchivoRemoto();
        String nombreArchivoOriginal=archivoName.substring(archivoName.lastIndexOf("/")+1);
		//archR.copiaLocalARemoto(nombreArchivoOriginal,"WEB");
		if(session.getRutaDescarga() != null)
		request.setAttribute("DescargaArchivo",session.getRutaDescarga());
		request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
		if (sesionvalida) {                                                  // servidor para faciultad a Mancomunidad

			String nombreArchivoEmp="",contenidoArchivoStr="";
			String operacion=getFormParameter(request, "operacion");
			String registrosEnArchivo=getFormParameter(request, "cantidadDeRegistrosEnArchivo");
			//nombreArchivoEmp=session.getNameFileNomina();
			nombreArchivoEmp=(String)sess.getAttribute("nombreArchivo");
			ArchivosNominaInter archivo1 = new ArchivosNominaInter(nombreArchivoEmp,request);

			tipoArchivo.trim();

			if (tipoArchivo.equals(""))
				tipoArchivo = "nuevo";
				tmpTipoArchivo = tipoArchivo.substring(0, 4);
				if(tmpTipoArchivo.equals("r") ){
				  contenidoArchivoStr+= archivo1.lecturaArchivoSalida(request);
					if (contenidoArchivoStr != null && contenidoArchivoStr.length() > 1 ){
					  contenidoArch = contenidoArchivoStr.substring(0,contenidoArchivoStr.indexOf("|"));
					  //cantidadReg   = contenidoArchivoStr.substring(contenidoArchivoStr.indexOf("|")+1,contenidoArchivoStr.indexOf("||"));
					 }
				}else {
						contenidoArch+= archivo1.lecturaArchivo();
				}

				request.setAttribute("cantidadDeRegistrosEnArchivo",registrosEnArchivo);
				request.setAttribute("fechaHoy",laFechaHoy);
				request.setAttribute("ContenidoArchivo",contenidoArch);

				/**
				* Facultades Nomina Interbancaria
				*
				*/
				if (sess.getAttribute("facultadesN") != null)
				  request.setAttribute( "facultades", sess.getAttribute("facultadesN") );

				request.setAttribute("textcuenta",cuentaCargo);
				evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
			//}
        } else {
            request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
            evalTemplate(IEnlace.ERROR_TMPL, request, response );
        }
		if(diasNoHabiles != null) {
			diasNoHabiles.clear();
			diasNoHabiles = null;
		}
	}

	public String comboBancos(HttpServletRequest request){
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		Connection conn = null;
	    PreparedStatement contQuery = null;
	    ResultSet contResult = null;
		String comboBanco	= "";
		String totales		= "";
    	int totalBancos;
		String claveBanco   = "";
		String banco       = "";
		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
				return "";
			}
			totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";
			contQuery=conn.prepareStatement(totales); // MODIFICAR
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.INFO);
				return "";
			}
			contResult=contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.INFO);
				return "";
			}
		    // MODIFICAR
			if( !contResult.next() ){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No existe registro.", EIGlobal.NivelLog.INFO);
				return "";
			}
			totalBancos=Integer.parseInt(contResult.getString(1));
			contResult.close(); // MODIFICAR
			EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

			/************************************************** Traer Bancos ... */
			String sqlbanco="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
			PreparedStatement bancoQuery=conn.prepareStatement(sqlbanco);

			if(bancoQuery==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.INFO);
				return "";
			}
			ResultSet bancoResult=bancoQuery.executeQuery(); // MODIFICAR
			if(bancoResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.INFO);
				return "";
			}
			if(!bancoResult.next()){
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): ResultSet regreso sin valor.", EIGlobal.NivelLog.INFO);
					return "";
				}
				comboBanco+="\n		 <SELECT NAME=Banco    class='tabmovtex'>";
				comboBanco+="\n		  <option value=0   class='tabmovtex'> Seleccione un Banco </OPTION>";

				// validaci�n del banco SERFIN, SANTANDER
				/**************************************************************************************************************/
			    /* Proyecto Unico  */
				/*
				 claveBanco   = session.getClaveBanco();
				 if (claveBanco.equals("003"))
				 banco = "SERFIN";
					 else if (claveBanco.equals("014"))
				          banco = "SANTANDER";

				 String bancoTmp = "";
				 for(int i=0; i<totalBancos; i++)
				 {
				    //bancoTmp = bancoResult.getString(2);
					// if ( bancoTmp.indexOf(banco) >= 0 ) {
					//}else{
					comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
					//	comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"\n";
					//	}
		          }
				  */
				  EIGlobal.mensajePorTrace("Se busca que los bancos no sean banme o serfi.", EIGlobal.NivelLog.INFO);
				  for(int i=0; i<totalBancos; i++)
				   {
					  if( !(bancoResult.getString(1).trim().equals("SERFI")) )
					     comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
					  else
						EIGlobal.mensajePorTrace("Se enconto y no se muestra: >"+ bancoResult.getString(1) + "<", EIGlobal.NivelLog.INFO);
					  bancoResult.next();
				   }
                  /**************************************************************************************************************/

				comboBanco+="\n		 </SELECT>";
				bancoResult.close();

		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		}finally {
        	  EI_Query.cierraConexion(contResult, contQuery, conn);
          }
          

		return comboBanco;
		/******************************************************************************/

	}

	public String comboPlazas(){
		String comboPlazas="";
		comboPlazas+="\n		 <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas+="\n		 <input type=text size=25 name=PlazaBanxico value='MEXICO, DF' onFocus='blur();'>";
		comboPlazas+="\n		 <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String traeBanco(String claveB){
		String sqlbanco		= "";
		String comboBanco	= "";
		String totales		= "";
		String banco		= "";
		Connection conn = null;
		PreparedStatement contQuery = null;
		ResultSet contResult = null;
		PreparedStatement bancoQuery = null;
		ResultSet bancoResult = null;
		int totalBancos;

		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
//			contQuery=createQuery(); // MODIFICAR
			try{

				totales="Select nombre_corto from comu_interme_fin where num_cecoban<>0 and cve_interme='"+claveB+"'";
				contQuery=conn.prepareStatement(totales);

				if(contQuery == null){
					EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): No se pudo crear Query.", EIGlobal.NivelLog.INFO);
				}

				contResult=contQuery.executeQuery();

				if(contResult==null){
					EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
				}

				if(contResult.next())
					banco=contResult.getString(1);
		  				contResult.close();

				}
				catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		} finally {
        	  EI_Query.cierraConexion(contResult, contQuery, conn);
          }

		return banco;
	}


	public String traeBancocve(String claveB){
		String sqlbanco		= "";
		String comboBanco	= "";
		String totales		= "";
		String banco		= "";
		Connection conn = null;
		PreparedStatement contQuery = null;
		ResultSet contResult = null;
		int totalBancos;

		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
//			contQuery=createQuery(); // MODIFICAR
			try{
						//="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
				totales="Select cve_interme,num_cecoban from comu_interme_fin where num_cecoban<>0 and cve_interme='"+claveB+"'";
				contQuery=conn.prepareStatement(totales);

				if(contQuery == null){
					EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): No se pudo crear Query.", EIGlobal.NivelLog.INFO);
				}

				contResult=contQuery.executeQuery();

				if(contResult==null){
					EIGlobal.mensajePorTrace("OpcionesNominaInter - traeBanco(): No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
				}

				if(contResult.next())
				//comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
					banco=contResult.getString(1) +"-"+contResult.getString(2);
		  			contResult.close();
					conn.close();

				}
				catch(Exception e){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		}finally {
        	  EI_Query.cierraConexion(contResult, contQuery, conn);
          }

		return banco;
	}

	public String traePlaza(String claveP){
		EIGlobal.mensajePorTrace("OpcionesNominaInter - traePlaza(): ", EIGlobal.NivelLog.INFO);
		String sqlbanco		= "";
		String comboBanco	= "";
		String totales		= "";
		String plaza		= "";
		Connection conn = null;
		PreparedStatement contQuery = null;
		ResultSet contResult = null;
		int totalBancos;
		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - traePlaza(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR

		   try{

			  totales="Select descripcion from comu_pla_banxico where plaza_banxico='"+claveP+"'";
			  contQuery	= conn.prepareStatement(totales);

			  if(contQuery == null){
				 EIGlobal.mensajePorTrace("OpcionesNominaInter - traePlaza(): No se pudo crear Query.", EIGlobal.NivelLog.INFO);
			  }
		  	  contResult=contQuery.executeQuery();
			  if(contResult==null){
				 EIGlobal.mensajePorTrace("OpcionesNominaInter - traePlaza(): No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO);
			  }
		  if(contResult.next())
		  plaza=contResult.getString(1);
			  
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
} finally {
        	  EI_Query.cierraConexion(contResult, contQuery, conn);
          }
		return plaza;
		}

	public String colocaBanco(String bank, String claveB){
		EIGlobal.mensajePorTrace("OpcionesNominaInter - colocaBanco(): claveB>> "+ claveB, EIGlobal.NivelLog.INFO);
		String sqlbanco		= "";
		String comboBanco	= "";
		String totales		= "";
		Connection conn = null;
		PreparedStatement contQuery = null;
		ResultSet contResult = null;
		PreparedStatement bancoQuery = null;
		ResultSet bancoResult = null;
		int totalBancos;
		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR
			totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";

			contQuery=conn.prepareStatement(totales); // MODIFICAR
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
			contResult=contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
		    // MODIFICAR
			if(contResult.next())
			totalBancos=Integer.parseInt(contResult.getString(1));
			else
			totalBancos =0;

			contResult.close(); // MODIFICAR
			EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

			/************************************************** Traer Bancos ... */
//			bancoQuery=createQuery(); // MODIFICAR
			sqlbanco="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
			bancoQuery=conn.prepareStatement(sqlbanco);

			if(bancoQuery==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
			bancoResult=bancoQuery.executeQuery(); // MODIFICAR
			if(bancoResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.INFO);
				return "NOSUCCESS";
			}
			/*****************************************************************************/
			/******************************************************************************/
			comboBanco+="\n		 <SELECT NAME='Banco'    class='tabmovtex'>";
			comboBanco+="\n		  <option value='"+claveB+"'   class='tabmovtex'>"+ bank +"</OPTION>\n";

				while(bancoResult.next()) {
				comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
				//comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"\n";
			    }


/*				bancoResult.next();
				for(int i=0; i<totalBancos; i++)
				{
					comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"</OPTION>\n\n";
					bancoResult.next(); // MODIFICAR
				} */


				comboBanco+="\n		 </SELECT>";

		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraStatement(bancoQuery);
      	  	EI_Query.cierraResultSet(bancoResult);
        	  EI_Query.cierraConexion(contResult, contQuery, conn);

          }

		return comboBanco;
	}
	public String colocaPlaza(String plazaBank){
		String comboPlazas="";
		comboPlazas+="\n		 <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas+="\n		 <input type=text size=25 name=PlazaBanxico value='"+plazaBank+"' onFocus='blur();'>";
		comboPlazas+="\n		 <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public int generaArchivoParaExportar(String archivo,String usuario)
	  {
		 /*
		 0     Todo salio bien
		 1     Error en el archivo no OK.
		 2     No se pudo abrir el archivo de entrada.
		 3     No se pudo realizar la copia remota
		 4     No se pudo crear el archivo
		 */

		 /*
		     ArchivoEntrada
			 OK;fecha 1;;no registros;-;-;fecha 2;-;-;-;cuenta;-;-;-;-;-;-;
			 cuenta;importe;-;referencia;-;-;clavebanco;-;-;-;-;
		 */

		 String nombreArchivo=archivo.substring(archivo.lastIndexOf("/")+1)+".enom";
		 String arcLinea="";
		 String cadenaRegistros="";
		 String head="";
		 String fecha1="";
		 String fecha2="";

		 boolean status=false;
		 int totalLineas=0;

		 EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
		 EI_Exportar ArcEnt=new EI_Exportar(archivo);

		 EI_Tipo Registros=new EI_Tipo();
		 EI_Tipo Header=new EI_Tipo();

		 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

		 if(ArcEnt.abreArchivoLectura())
		  {
			boolean noError=true;
			do
			 {
			   arcLinea=ArcEnt.leeLinea();
			   if(totalLineas==0)
				if( arcLinea.substring(0,2).equals("OK") )
				 {
				   status=true;
				   head=arcLinea+"@";
				 }
			   if(!arcLinea.equals("ERROR"))
				{
				  if(arcLinea.substring(0,5).trim().equals("ERROR"))
					break;
				  totalLineas++;
				  if(totalLineas>=2)
					cadenaRegistros+=arcLinea.substring(0,arcLinea.length())+"@";
				}
			   else
				 noError=false;
			 }while(noError);
			ArcEnt.cierraArchivo();

			if(arcLinea.substring(0,5).trim().equals("ERROR") && totalLineas<=1 && !status)
				return 0;
		  }
		 else
			return 1;


		 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Armando los objetos con la informacion del archivo", EIGlobal.NivelLog.DEBUG);
		 Header.iniciaObjeto(';','@',head);
		 Registros.iniciaObjeto(';','@',cadenaRegistros);

		 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): cadenaRegistros = *-*-*" + cadenaRegistros + "*-*-*", EIGlobal.NivelLog.INFO);

		 if(ArcSal.creaArchivo())
		  {
			arcLinea="";
			//Q05-0044906 - Praxis NVS - 051117
			int c[]={8,0,1,6,7,3,5,9,10,11};
			//Indice del elemento deseado
			//nombre,cuenta,importe,clave banco,plaza,referencia,mensaje,fecha,concepto,referencia_ley
			//Q05-0044906 - Praxis NVS - 051117
			int b[]={50,20,18,5,5,8,40,10,40,7};
			// Longitud maxima del campo para el elemento

			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Exportando Registros.", EIGlobal.NivelLog.DEBUG);

			//formateando fechas (eliminar las /)
			fecha1=Header.camposTabla[0][1];
			fecha2=Header.camposTabla[0][5];
			Header.camposTabla[0][1]=fecha1.substring(0,2)+fecha1.substring(3,5)+fecha1.substring(6);
			Header.camposTabla[0][5]=fecha2.substring(0,2)+fecha2.substring(3,5)+fecha2.substring(6);

			//Registro encabezado ...
			arcLinea="11    S";
			arcLinea+=ArcSal.formateaCampo(Header.camposTabla[0][1],8); //fecha generacion
			arcLinea+=ArcSal.formateaCampo(Header.camposTabla[0][9],16); //cuenta cargo
			arcLinea+=ArcSal.formateaCampo(Header.camposTabla[0][5],8); //fecha aplicacion
			arcLinea+="\n";
			if(!ArcSal.escribeLinea(arcLinea))
			   EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Algo salio mal escribiendo "+arcLinea, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Se genero el encabezado del archivo", EIGlobal.NivelLog.DEBUG);
			//Registro Detalle ...
			for(int i=0;i<Registros.totalRegistros;i++)
			 {
			   arcLinea="";
			   arcLinea+="2"+ArcSal.formateaCampo(Integer.toString(i+2), 3);
//Q05-0044906 - Praxis NVS - 051117
			   for(int j=0;j<10;j++)
				{
			       EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): [i]=" + i + " [j]=" + j, EIGlobal.NivelLog.INFO);
			       EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Registros.camposTabla[i][c[j]],b[j]=|" + Registros.camposTabla[i][c[j]]+ "|", EIGlobal.NivelLog.INFO);

				  if(c[j]==1)
					arcLinea+=ArcSal.formateaCampoImporte(Registros.camposTabla[i][c[j]],b[j]);
				  else
					arcLinea+=ArcSal.formateaCampo(Registros.camposTabla[i][c[j]],b[j]);

				  if(j==0)
					{
					  if(Registros.camposTabla[i][0].trim().length()==18)
					    arcLinea+="40   ";
					  else
					  if(Registros.camposTabla[i][0].trim().length()==16)
						arcLinea+="02   ";
					  else
						arcLinea+="01   ";
					}
				}
			   arcLinea+="\n";
			   EIGlobal.mensajePorTrace("***OpcionesNominaInter.class - generaArchivoParaExportar(): arcLinea = " + arcLinea + "|" , EIGlobal.NivelLog.INFO);
			   if(!ArcSal.escribeLinea(arcLinea))
				 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Algo salio mal escribiendo "+arcLinea, EIGlobal.NivelLog.INFO);
			 }

			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Se genero el detalle del archivo", EIGlobal.NivelLog.DEBUG);

			//Registro sumario ...
			arcLinea="3"+ArcSal.formateaCampo(Integer.toString(Registros.totalRegistros+2), 3);  //secuencia
			arcLinea+=ArcSal.formateaCampo(Integer.toString(Registros.totalRegistros), 3); //registros
			arcLinea+=ArcSal.formateaCampoImporte(Header.camposTabla[0][7],18); //importe total
			arcLinea+="\n";
			if(!ArcSal.escribeLinea(arcLinea))
				 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Algo salio mal escribiendo "+arcLinea, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Se genero el sumario del archivo", EIGlobal.NivelLog.DEBUG);
			ArcSal.cierraArchivo();

			//*************************************************************
			ArchivoRemoto archR = new ArchivoRemoto();
			if(!archR.copiaLocalARemoto(nombreArchivo,"WEB"))
			 return 3;
			else
			 EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Se genero la copia remota", EIGlobal.NivelLog.DEBUG);
			//**************************************************************
			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): Se llevo a cabo la exportacion", EIGlobal.NivelLog.DEBUG);
		  }
		 else
		  {
			EIGlobal.mensajePorTrace("OpcionesNominaInter - generaArchivoParaExportar(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
			return 4;
		  }

		 return 0;
	 }


	// Funcionalidad para verificar Archivos del control de recarga sesion IM199738.
	public boolean verificaArchivos(String arc,boolean eliminar)
		throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
		File archivocomp = new File( arc );
		if (archivocomp.exists())
		 {
		   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.DEBUG);
		   if(eliminar)
			{
		      archivocomp.delete();
			  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
			}
		   return true;
		 }
		EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.DEBUG);
		return false;
	}

	public  void guardaParametrosEnSession (HttpServletRequest request){

          String template= request.getServletPath();

          request.getSession().setAttribute("plantilla",template);

        HttpSession session = request.getSession(false);

        session.removeAttribute("bitacora");


        if(session != null){

            Map tmp = new HashMap();

			tmp.put("textcuenta",getFormParameter(request, "textcuenta" ));
			tmp.put("operacion",getFormParameter(request, "operacion" ));
			tmp.put("registro",getFormParameter(request, "registro" ));
			tmp.put("tipoArchivo",getFormParameter(request, "tipoArchivo" ));
			tmp.put("cuenta",getFormParameter(request, "cuenta" ));
			tmp.put("folio",getFormParameter(request, "folio" ));
			tmp.put("txtFechaLimite",getFormParameter(request, "txtFechaLimite" ));
			tmp.put("secuencia",getFormParameter(request, "secuencia" ));
			tmp.put("CuentasArchivo",getFormParameter(request, "CuentasArchivo" ));
			tmp.put("NumerosEmpleado",getFormParameter(request, "NumerosEmpleado" ));
			tmp.put("cantidadDeRegistrosEnArchivo",getFormParameter(request, "cantidadDeRegistrosEnArchivo" ));
			tmp.put("archivo_actual",getFormParameter(request, "archivo_actual" ));
			tmp.put("transmision",getFormParameter(request, "transmision" ));
			tmp.put("txtCveBeneficiario",getFormParameter(request, "txtCveBeneficiario" ));
			tmp.put("txtBeneficiario",getFormParameter(request, "txtBeneficiario" ));
			tmp.put("txtFechaAlta",getFormParameter(request, "txtFechaAlta" ));

			session.setAttribute("parametros",tmp);
            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }

    }

} //Fin de la clase