/*
 * ChesConsulta.java
 *
 * Created on 7 de enero de 2003, 11:18 AM
 */

package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.ListIterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.ches.ChesArchivoImportado;
import mx.altec.enlace.ches.ChesArchivoImportadoConsFormat;
import mx.altec.enlace.ches.ChesBeneficiario;
import mx.altec.enlace.ches.ChesCheque;
import mx.altec.enlace.ches.ChesChequeConsultaFormat;
import mx.altec.enlace.ches.ChesChequeExpFormat;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//VSWF


/** Realiza la consulta de Cheques, Archivos y Detallada de Archivos, asi como desplegar el calendario de Ches
 * @author Rafael Martinez Montiel
 * @version 2 se rehizo todo el codigo como parte del proyecto Chequera de segurudad simplificada
 */
public class ChesConsulta extends BaseServlet {
    /** Nombre de la clase para rapida referencia
     */
    static final String CLASS_NAME = ChesConsulta.class.getName ();
    /** Formateador para las fechas
     */
    public static SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
    /** Formateador de fechas para reconocer los parametros para el despliegue de calendario
     */
    public static SimpleDateFormat sdfNoDelim = new SimpleDateFormat ("ddMMyyyy");
    /** Formateador para fechas para la trama de consulta de cheques
     */
    public static SimpleDateFormat sdfTramaCheques = new SimpleDateFormat ("yyyy-MM-dd");
    /** Formateador de fechas para la trama de consulta de archivos
     */
    public static SimpleDateFormat sdfTramaArchivos = new SimpleDateFormat ("dd-MM-yyyy");
    /** Formateador para los numeros
     */
    public static java.text.DecimalFormat def = new java.text.DecimalFormat ("####0.00");

    /** Identificador para consulta de archivos
     */
    public static final  String CONSULTA_ARCHIVOS = "archivos";
    /** Identificador para consulta de Cheques
     */
    public static  final String CONSULTA_CHEQUES = "cheques";
    /** Identificador para el despliegue de calendario
     */
    public static  final String CALENDARIO = "calendario";
    /** Identificador para el despliegue de resultados
     */
    public static  final String RESULTADOS = "resultados";

    /** identificado para despliegue de pagina de Macros
     * TODO: ELIMINAR EN CUANTO SE TENGA UNSERVLET ESPECIFICO PARA ESTO
     */
    public static final String DESPLIEGA_MACROS = "macros";


    /** plantilla de respuesta para la descarga de macros
     *
     */
    public static final String MOD_MACROS_DEF_TEMPLATE = "/jsp/downloadMacros.jsp";

    /** Identificador para consulta detallada de archivo
     */
    public static  final String CONSULTA_DETALLE_ARCHIVO = "detalleArch";

    /** identifiador para copia local hacia remoto
     */
    public static final String LOCAL_A_REMOTO = "1";
    /** identificador para copia de remoto a local
     */
    public static final String REMOTO_A_LOCAL = "0";
    /** llave para identificar los beneficiarios en la sesion
     */
    public static final String CHES_BENEF = "chesBenef";
    /** Cabecera para el modulo
     */
    public static  final String S_CHEQUERA_CABECERA = "Servicios &gt; Chequera Seguridad ";
    /** Nombre del Modulo
     */
    public static  final String S_CHEQUERA_SEGURIDAD = "Chequera seguridad";

    /** llave para el parametro del modulo deseado
     */
    public static  final String PAR_MODULO = "modulo";
    /** llave para el parametro de opcion
     */
    public static  final String PAR_OPCION = "opcion";


    /** despachador por defecto para la consulta por archivo
     */
    public static  final String MOD_ARCHIVO_DEFAULT_DISPATCHER = "/jsp/ches/ChesConsultaArchFiltro.jsp";
    /** despachador para la respuesta de consulta de archivos
     */
    public static  final String MOD_ARCHIVO_RESPUESTA_DISPATCHER = "/jsp/ches/ChesConsultaArchRes.jsp";
    /** Cadena para el encabezado en la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_S_MODULO = "Chequera seguridad, Consulta Archivos";
    /** Cadena de cabecera para la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_S_CABECERA = "Servicios &gt; Chequera Seguridad &gt; Consultas &gt; Archivos";
    /** Ayuda inicial para la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_S_HLP_INICIAL = "s34010h";
    /** ayuda para resultado de consulta de archivos
     */
    public static  final String MOD_ARCHIVO_S_HLP_RESULTADO = "s34011h";
    /** ayuda para consulta de archivos procesados
     */
    public static  final String MOD_ARCHIVO_S_HLP_RESULTADO_PROCESADOS = "s34012h";
    public static  final String EMAIL = "email";


    /** llave para la fecha inicial de la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_FECHA_INICIAL = "fechaInicial";
    /** llave para la fecha final de la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_FECHA_FINAL = "fechaFinal";
    /** llave para el numero de secuencia en la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_SECUENCIA = "secuencia";
    /** llave para el estatus en la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_STATUS = "status";
    /** estatus de recibido
     */
    public static final String MOD_ARCHIVO_STATUS_RECIBIDO = "R";
    /** estatus de enviado
     */
    public static final String MOD_ARCHIVO_STATUS_ENVIADO = "E";
    /** estatus de procesado
     */
    public static final String MOD_ARCHIVO_STATUS_PROCESADO = "P";
    /** llave para los archivos en la sesion dentro de la consutla de archivos
     */
    public static  final String MOD_ARCHIVO_ARCHIVOS = "chesArchivos";
    /** llave para un mensaje extra en el resultado en la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_MSG = "chesMSG";

    /** llave para el archivo exportado en la consulta de archivos
     */
    public static  final String MOD_ARCHIVO_ARCHIVO_EXP = "chesArchivoExp";

    /** llave para el tipo de archivo que se esta consultando
     */
    public static final String MOD_ARCHIVO_TIPO_ARCHIVO = "chesTipoArchivo";

    /** despachador por defecto en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_DEFAULT_DISPATCHER = "/jsp/ches/ChesConsultaCheqFiltro.jsp";
    /** despachador para la respuesta en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_RESPUESTA_DISPATCHER = "/jsp/ches/ChesConsultaCheqRes.jsp";
    /** Nombre del modulo para los encabezados de consutla de cheques
     */
    public static  final String MOD_CHEQUE_S_MODULO = "Chequera seguridad, Consulta Cheques";
    /** cabecera para el modulo de consulta de cheques
     */
    public static  final String MOD_CHEQUE_S_CABECERA = "Servicios &gt; Chequera Seguridad &gt; Consultas &gt; Cheques";
    /** ayuda inicial para la consulta de cheques
     */
    public static  final String MOD_CHEQUE_S_HLP_INICIAL = "s34020h";
    /** ayuda para respuesta de consulta de archivos transmitidos y programados
     */
    public static  final String MOD_CHEQUE_S_HLP_RESPUESTA_TRANS_PROG = "s34021h";
    /** ayuda para respuesta de cheques vencidos liquidados y cancelados
     *
     */
    public static  final String MOD_CHEQUE_S_HLP_RESPUESTA_VENC_LIQ_CAN = "s34022h";
    /** ayuda para la resupesta de consulta de cheques
     */
    public static final String MOD_CHEQUE_S_HLP_RESPUESTA = "s34023h";
    /** llave para el tipo de cheque
     */
    public static final String MOD_CHEQUE_TIPO_CHEQUE = "chesTipoCheque";

    /** estatus de pendiente
     */
    public static final String MOD_CHEQUE_STATUS_PENDIENTE = "PE";
    /** estatus de liquidado
     */
    public static final String MOD_CHEQUE_STATUS_LIQUIDADO = "LI";
    /** estatus de cancelado
     */
    public static final String MOD_CHEQUE_STATUS_CANCELADO = "CA";
    /** estatus de vencido
     */
    public static final String MOD_CHEQUE_STATUS_VENCIDO = "VE";

    /** mensaje de retorno
     */
    public static final String MOD_CHEQUE_MSG_RETORNO = "chesMsgRet";
    /** encabezado para la trama consulta de cheques
     */
    public static  final String HEAD_TRAMA_CONS_CHEQ = "2EWEB";
    /** operacion para la trama de consulta de cheques
     */
    public static  final String OP_TRAMA_CONS_CHEQ = "COCH";

    /** llave para el numero de cuenta en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_NUM_CUENTA = "ctaCargo";
    /** llave para el importe en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_IMPORTE = "importe";
    /** llave para el cheque inicial en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_CHK_DESDE = "cheqDesde";
    /** llave para el cheque final en la consutla de cheques
     */
    public static  final String MOD_CHEQUE_CHK_HASTA = "cheqHasta";
    /** llave para el estatus en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_STATUS = "status";

    public static final String MOD_CHEQUE_EMAIL = "cheqMail";
    /** llave para los cheques en la sesion dentro de la consutla de cheques
     */
    public static  final String MOD_CHEQUE_CHEQUES = "chesConsCheques";
    /** llave para el nombre del archivo exportado en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_ARCHIVO_EXP = "chesArchivoExp";


    public static final String MOD_CHEQUE_USE_LIB = "useLib";
    /** llave para la fecha inicial de liberacion en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_LIB_DESDE = "libDesde";
    /** llave para la fecha final de liberacion en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_LIB_HASTA = "libHasta";

    public static final String MOD_CHEQUE_USE_LIQ = "useLiq";
    /** llave para la fecha inicial de liquidacion en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_LIQ_DESDE = "liqDesde";
    /** llave para la fecha final de liquidacion en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_LIQ_HASTA = "liqHasta";

    public static final String MOD_CHEQUE_USE_VIG = "useVig";
    /** llave para la fecha inicial de vigencia en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_VIG_DESDE = "vigDesde";
    /** llave para la fecha final de vigencia en la consulta de cheques
     */
    public static  final String MOD_CHEQUE_VIG_HASTA = "vigHasta";

//    /** Despachador por defecto para el despliegue de resultados
//     */
//    public static final String MOD_RESULTADOS_DEF_TEMPLATE = "/jsp/ches/ChesResultados.jsp";
//
//    /** Cabecera para el modulo de consulta de resultados
//     */
//    public static final String MOD_RESULTADOS_S_CABECERA = "Servicios &gt; Chequera Seguridad &gt; Consultas &gt; Resultados";
//
//    /** Nombre del modulo de resultados
//     */
//    public static final String MOD_RESULTADOS_S_MODULO = "Chequera Seguridad, Resultados";
//    /** ayuda para el modulo de resultados
//     */
//    private static final String MOD_RESULTADOS_S_AYUDA = "123hsh";
//
//    //TODO:definir la ruta por defecto
//    /** llave para el directorio base don de se realizara la busqueda de archivos
//     */
//    private static final String MOD_RESULTADOS_BASE_DIR_KEY = "enlaceMig.ches.consulta.resultados.path";
//    /** directorio por defecto donde se realizará la busqueda
//     */
//    private static final String MOD_RESULTADOS_BASE_DIR_DEF = "/transfer/EnlaceInternet/chequera/masivo";

    /** Despachador por defecto en el desplieguq de calendario
     */
    public static  final String CAL_DISPATCHER = "/jsp/ches/ChesCalendario.jsp";
    /** llave para la fecha maxima en el despliegue de calendario
     */
    public static  final String CAL_FECHA_MAX = "fechaMax";
    /** llave para la fecha minima en el despliegue de calendario
     */
    public static  final String CAL_FECHA_MIN = "fechaMin";
    /** llave para la fecha maxima desplegable en el despliegue de calendario
     */
    public static  final String CAL_FECHA_MAX_DISP = "fechaMaxDisp";
    /** llave para identificar si se incluye en el despliegue la fecha maxima
     */
    public static  final String CAL_INCLUSIVE_END = "inclusiveEnd";

    /** llave para identificar los dias inhabiles en la sesion para el despliegue del calendario
     */
    public static  final String CHES_INHABILES = "chesInhabiles";

    // TODO: fin

    /** Initializes the servlet.
     * @param config per spec
     * @throws ServletException per spec
     */
    public void init (javax.servlet.ServletConfig config) throws ServletException {
        super.init (config);
    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EIGlobal.mensajePorTrace (CLASS_NAME + ": entrando processRequest()", EIGlobal.NivelLog.DEBUG);
        if(! SesionValida ( request,response) ){
            EIGlobal.mensajePorTrace (CLASS_NAME + ": saliendo processRequest()", EIGlobal.NivelLog.DEBUG);
            return;
        }
        BaseResource baseResource = (BaseResource)request.getSession ().getAttribute ("session");
        if(null == baseResource ){
            EIGlobal.mensajePorTrace (CLASS_NAME + ": baseResource="+ baseResource, EIGlobal.NivelLog.DEBUG);
            request.setAttribute ("Error",IEnlace.MSG_PAG_NO_DISP);
            request.getRequestDispatcher ("/jsp/EI_Error.jsp").forward (request,response) ;
            EIGlobal.mensajePorTrace (CLASS_NAME + ": saliendo processRequest()", EIGlobal.NivelLog.DEBUG);
            return;
        }
        request.setAttribute ("baseResource", baseResource);
        baseResource.setModuloConsultar ( IEnlace.MAlta_ctas_cheq_seg );
        String requestDispatcher = null;
        String modulo = request.getParameter ( PAR_MODULO );
        EIGlobal.mensajePorTrace ( CLASS_NAME + ": modulo:" +modulo, EIGlobal.NivelLog.DEBUG);
        if(null == modulo){
            EIGlobal.mensajePorTrace (CLASS_NAME + ": modulo no definido, " + modulo , EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP,S_CHEQUERA_SEGURIDAD,S_CHEQUERA_CABECERA,request,response);
        }else if(modulo.equals ( CONSULTA_ARCHIVOS) ){
            requestDispatcher = consultaArchivo ( request, response );
        }else if(modulo.equals (CONSULTA_CHEQUES) ){
        	//TODO: BIT CU 4191, EL cliente entra al flujo

        	/*
    		 * VSWF ARR -I
    		 */
        	if (Global.USAR_BITACORAS.trim().equals("ON")){
	        	try{
				BitaHelper bh = new BitaHelperImpl(request, baseResource,request.getSession());
				if (request.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_CHEQUES_CANC_ENTRA);
				bt.setContrato(baseResource.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
        	}
    		/*
    		 * VSWF ARR -F
    		 */

            requestDispatcher = consultaCheques ( request, response );
        }else if(modulo.equals ( CALENDARIO ) ){
            requestDispatcher = verCalendario ( request, response );
        }else if(modulo.equals (CONSULTA_DETALLE_ARCHIVO)){
            requestDispatcher = consultaDetalle (request,response);
        }else if(modulo.equals ( DESPLIEGA_MACROS )){
            requestDispatcher = despliegaMacros (request,response);
//        } else if(modulo.equals ( RESULTADOS )){
//            requestDispatcher = despliegaResultados (request,response);
        }else{
            EIGlobal.mensajePorTrace (CLASS_NAME + ": modulo no definido, " + modulo , EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP,S_CHEQUERA_SEGURIDAD,S_CHEQUERA_CABECERA,request,response);
        }
        if(null != requestDispatcher ){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": requestDispatcher=" + requestDispatcher , EIGlobal.NivelLog.DEBUG);
            request.getRequestDispatcher ( requestDispatcher).forward ( request, response );
        }
        EIGlobal.mensajePorTrace (CLASS_NAME + ": saliendo processRequest()", EIGlobal.NivelLog.DEBUG);
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     * @return short description
     */
    public String getServletInfo () {
        return "realiza consultas de archivos y cheques";
    }

    /** realiza la consulta de archivos
     * @param request per spec
     * @param response per spec
     * @throws IOException per spec
     * @throws ServletException per spec
     * @return String con el request dispatcher a utilizar o null si ya se realizo algun forward
     */
    protected String consultaArchivo ( HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{

        String requestDispatcher = MOD_ARCHIVO_DEFAULT_DISPATCHER;

        String encabezado = CreaEncabezado (MOD_ARCHIVO_S_MODULO,
        MOD_ARCHIVO_S_CABECERA,
        MOD_ARCHIVO_S_HLP_INICIAL, request);
        request.setAttribute ("encabezado",encabezado);
        /**
         * VSWF ARR
         */
        BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
        /*
         * VSWF ARR
         * */
        int opcion = -1;

        try{
            opcion = Integer.parseInt ( request.getParameter ( PAR_OPCION ) );
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": opcion incorrecta, debe ser numerica", EIGlobal.NivelLog.DEBUG);
        }

        switch (opcion){
            default:{// Mostrar la pagina de filtro
                if(null == request.getSession ().getAttribute ( CHES_INHABILES ) ){
                    Set diasInhabiles = diasInhabiles (  );
                    request.getSession ().setAttribute (CHES_INHABILES, diasInhabiles );
                }
//            	TODO: BIT CU 4201, EL cliente entra al flujo

            	/*
        		 * VSWF ARR -I
        		 */
            	if (Global.USAR_BITACORAS.trim().equals("ON")){
    	        	try{
    				BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
    				if (request.getParameter(BitaConstants.FLUJO)!=null){
    				       bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
    				  }else{
    				   bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
    				  }
    				BitaTransacBean bt = new BitaTransacBean();
    				bt = (BitaTransacBean)bh.llenarBean(bt);
    				bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_ARCHIVOS_ENTRA);
    				bt.setContrato(session.getContractNumber());
    				BitaHandler.getInstance().insertBitaTransac(bt);
    				}catch (SQLException e){
    					e.printStackTrace();
    				}catch(Exception e){
    					e.printStackTrace();
    				}
            	}
        		/*
        		 * VSWF ARR -F
        		 */
                break;
            }
            case 1:{//realizar consulta

                requestDispatcher = obtenerConsultaArchivos (request,response);
                break;
            }
            case 2:{//desplazar consulta
                try{
                    Integer start = new Integer (request.getParameter ("start"));
                    request.setAttribute ("start", start);
                    String status = (String)request.getSession ().getAttribute ( MOD_ARCHIVO_TIPO_ARCHIVO );
                    String encab = CreaEncabezado (MOD_ARCHIVO_S_MODULO,
                    MOD_ARCHIVO_S_CABECERA,
                    MOD_ARCHIVO_S_HLP_RESULTADO, request);
                    if( status.equals ("P")){
                        encab = CreaEncabezado (MOD_ARCHIVO_S_MODULO,
                        MOD_ARCHIVO_S_CABECERA,
                        MOD_ARCHIVO_S_HLP_RESULTADO_PROCESADOS, request);
                    }

                    request.setAttribute ("encabezado",encab);
                    requestDispatcher = MOD_ARCHIVO_RESPUESTA_DISPATCHER;
                    break;
                }catch (NumberFormatException ex){
                    despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
                    requestDispatcher = null;
                }
            }
        }


        return requestDispatcher;
    }

    /** Realiza la consulta de cheques
     * @param request per spec
     * @param response per spec
     * @throws IOException per spec
     * @throws ServletException per spec
     * @return String con el RequestDispatcher a utilizar, o null si ya se realizo algun forward
     */
    protected String consultaCheques ( HttpServletRequest request, HttpServletResponse response)
    throws IOException,ServletException{
        String requestDispatcher = MOD_CHEQUE_DEFAULT_DISPATCHER;

        String encabezado = CreaEncabezado (MOD_CHEQUE_S_MODULO,
        MOD_CHEQUE_S_CABECERA,
        MOD_CHEQUE_S_HLP_INICIAL, request);
        request.setAttribute ("encabezado",encabezado);

        int opcion = -1;

        try{
            opcion = Integer.parseInt ( request.getParameter ( PAR_OPCION ) );
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": opcion incorrecta, debe ser numerica", EIGlobal.NivelLog.DEBUG);
        }

        switch (opcion){
            case 1:{// realizar consulta
                requestDispatcher = obtenerConsultaCheques ( request,response );
                break;
            }
            case 2:{// desplazar resultado
                try{
                    Integer start = new Integer (request.getParameter ("start"));
                    request.setAttribute ("start", start);
                    //                    String tipoCheque = (String) request.getSession ().getAttribute ( MOD_CHEQUE_TIPO_CHEQUE );
                    encabezado = CreaEncabezado (MOD_CHEQUE_S_MODULO,
                    MOD_CHEQUE_S_CABECERA
                   //, MOD_CHEQUE_S_HLP_RESPUESTA_VENC_LIQ_CAN
                    //,MOD_CHEQUE_S_HLP_RESPUESTA_TRANS_PROG
                    ,MOD_CHEQUE_S_HLP_RESPUESTA
                    , request);
                    //                    if(tipoCheque.equals ( "PE" )){
                    //                        encabezado = CreaEncabezado (MOD_CHEQUE_S_MODULO,
                    //                        MOD_CHEQUE_S_CABECERA,
                    //                        MOD_CHEQUE_S_HLP_RESPUESTA_TRANS_PROG, request);
                    //                    }
                    request.setAttribute ("encabezado",encabezado);
                    requestDispatcher = MOD_CHEQUE_RESPUESTA_DISPATCHER;
                    break;
                }catch (NumberFormatException ex){
                    despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP, MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,request,response);
                }
            }
            default:{
                GregorianCalendar cal = new GregorianCalendar ();
                cal.add (cal.MONTH,-3);
                EIGlobal.mensajePorTrace (CLASS_NAME + ": " + CAL_FECHA_MIN + "=" + sdf.format ( cal.getTime () ) , EIGlobal.NivelLog.INFO);
                request.setAttribute ( CAL_FECHA_MIN,cal);
                cal = new GregorianCalendar ();
                cal.add (cal.MONTH, 3);
                EIGlobal.mensajePorTrace (CLASS_NAME + ": " + CAL_FECHA_MAX_DISP + "=" + sdf.format ( cal.getTime () ) , EIGlobal.NivelLog.INFO);
                request.setAttribute ( CAL_FECHA_MAX_DISP , cal);
                cal = new GregorianCalendar ();
                cal.add (cal.MONTH, 3);
                cal.add (cal.DAY_OF_MONTH, 4);
                EIGlobal.mensajePorTrace (CLASS_NAME + ": " + CAL_FECHA_MAX + "=" + sdf.format ( cal.getTime () ) , EIGlobal.NivelLog.INFO);
                request.setAttribute ( CAL_FECHA_MAX, cal);

                if(null == request.getSession ().getAttribute ( CHES_INHABILES ) ){
                    Set diasInhabiles = diasInhabiles (  );
                    request.getSession ().setAttribute (CHES_INHABILES, diasInhabiles );
                }
                // obtener beneficiarios
                //                java.util.Map beneficiarios = getBeneficiarios (MOD_CHEQUE_S_MODULO,MOD_CHEQUE_S_CABECERA,request,response);
                //                if(null == beneficiarios){
                //                    return null;
                //                }
                //                request.getSession ().setAttribute ( CHES_BENEF, beneficiarios );

                break;
            }
        }

        return requestDispatcher;
    }

    /** Obtiene los dias inhabiles
     * @return Set con los dias inhabiles contenidos en un GregorianCalendar
     */
    protected Set diasInhabiles () {

        Set retValue = new java.util.HashSet ();

        String inh = diasInhabilesAnt ();
        if((null != inh )&&(!inh.trim ().equals (""))){
            StringTokenizer tokenizer = new StringTokenizer ( inh, ", ");
            while(tokenizer.hasMoreTokens ()){
                try{
                    java.util.Date date = sdf.parse (tokenizer.nextToken () );
                    GregorianCalendar cal = new GregorianCalendar ();
                    cal.setTime ( date );
                    retValue.add ( cal );
                }catch(ParseException ex){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": no se reconoce el formato de fecha, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
                }
            }
        }

        inh = diasInhabilesDesp ();
        if((null != inh )&&(!inh.trim ().equals (""))){
            StringTokenizer tokenizer = new StringTokenizer ( inh, ", ");
            while(tokenizer.hasMoreTokens ()){
                try{
                    java.util.Date date = sdf.parse (tokenizer.nextToken () );
                    GregorianCalendar cal = new GregorianCalendar ();
                    cal.setTime ( date );
                    retValue.add ( cal );
                }catch(ParseException ex){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": no se reconoce el formato de fecha, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
                }
            }
        }
        return( retValue );
    }

    /** Obtiene los beneficiarios
     * @param modulo modulo a utilizar
     * @param cabecera cabecera a utilizar
     * @param request per spec
     * @param response per spec
     * @throws IOException per spec
     * @throws ServletException per spec
     * @return mapa con los beneficiarios {@link mx.altec.enlace.ches.ChesBeneficiario}
     */
    protected java.util.Map getBeneficiarios (String modulo, String cabecera, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        java.util.Map retValue = new java.util.HashMap ();
        ServicioTux servicio = new ServicioTux ();
        //servicio.setContext ( ((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext () );
        BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");


        BufferedReader reader = null;
        try{
            String contrato = baseResource.getContractNumber ();
            String clavePerfil = baseResource.getUserProfile ();
            String usuario = baseResource.getUserID8 ();
            String tipoOperacion = "CBEN";
            String trama = "2EWEB|" + usuario + "|" + tipoOperacion + "|" + contrato + "|" + usuario + "|" + clavePerfil + "|"
            + contrato + "@";
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama salida=" + trama , EIGlobal.NivelLog.DEBUG);
            java.util.Hashtable ret = servicio.web_red ( trama );
            if(null == ret){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }

            String tramaRetorno = (String) ret.get ("BUFFER");
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama retorno=\n" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
            if(null == tramaRetorno){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }
            File archivoRetorno = new File ( tramaRetorno );
            File archivoLocal = new File ( IEnlace.LOCAL_TMP_DIR, archivoRetorno.getName () );
            File archivoBen = new File ( IEnlace.LOCAL_TMP_DIR, request.getRequestedSessionId ());
            ArchivoRemoto archRemoto = new ArchivoRemoto ( );
            if( !archRemoto.copia (archivoRetorno.getName () ,archivoRetorno.getParent (),REMOTO_A_LOCAL  ) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR no se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            }
            archivoLocal.renameTo ( archivoBen );
            reader = new BufferedReader ( new java.io.FileReader (archivoBen) );

            String linea = reader.readLine ();
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama en archivo:" + linea, EIGlobal.NivelLog.INFO);
            if(null == linea || !(linea.startsWith ("OK" ) ) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR en la transaccion, " + linea, EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            }
            while( null != ( linea = reader.readLine () ) ){
                StringTokenizer tokenizer = new StringTokenizer ( linea , ";" );
                if ( tokenizer.countTokens () < 2 ) {
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": tokens = " + tokenizer.countTokens () , EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR al leer el beneficiario, " + linea, EIGlobal.NivelLog.DEBUG);
                    throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
                }
                mx.altec.enlace.ches.ChesBeneficiario ben = new mx.altec.enlace.ches.ChesBeneficiario ();
                ben.setCveBenef ( tokenizer.nextToken () );
                ben.setNombre ( tokenizer.nextToken () );
                //ben.setFechaAlta ( tokenizer.nextToken () );
                retValue.put ( ben.getCveBenef (),ben );
            }
        }catch(IOException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + ex.toString (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP ,modulo, cabecera, request,response);
            retValue = null;
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": "  + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            despliegaPaginaError ( ex.getMessage (),modulo,cabecera, request,response);
            retValue = null;
        }finally{
            if(null != reader){
                try{
                    reader.close ();
                }catch(IOException e){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + e.toString (), EIGlobal.NivelLog.DEBUG);
                    e.printStackTrace ();
                }
                reader = null;
            }
        }

        return retValue;
    }

    /** Despliega el calendario
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return String con el request dispatcher a utilizar
     */
    protected String verCalendario (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String requestDispatcher = CAL_DISPATCHER;
        if(null == request.getSession ().getAttribute ( CHES_INHABILES ) ){
            Set inhabiles = diasInhabiles ();
            request.getSession ().setAttribute ( CHES_INHABILES , inhabiles );
        }

        GregorianCalendar cal = null;
        try{
            boolean inclusiveEnd = Boolean.valueOf (request.getParameter (CAL_INCLUSIVE_END) ).booleanValue ();
            cal = new GregorianCalendar ();
            cal.setTime ( sdfNoDelim.parse ( request.getParameter ( CAL_FECHA_MAX ) ) );
            if( inclusiveEnd ){
                cal.add ( cal.DAY_OF_MONTH, 4);
            }
            EIGlobal.mensajePorTrace (CLASS_NAME + ": " + CAL_FECHA_MAX + "=" + sdf.format ( cal.getTime () ) , EIGlobal.NivelLog.INFO);
            request.setAttribute ( CAL_FECHA_MAX, cal );

            cal = new GregorianCalendar ();
            cal.setTime ( sdfNoDelim.parse ( request.getParameter ( CAL_FECHA_MIN ) ) );
            EIGlobal.mensajePorTrace (CLASS_NAME + ": " + CAL_FECHA_MIN + "=" + sdf.format ( cal.getTime () ) , EIGlobal.NivelLog.INFO);
            request.setAttribute ( CAL_FECHA_MIN, cal );
        }catch(ParseException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": no se reconoce el parametro como fecha, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            request.setAttribute ( "error", IEnlace.MSG_PAG_NO_DISP );
        }
        return requestDispatcher;
    }

    /** Hace la peticion de la consulta de cheques
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return String con el request dispatcher a utilizar
     */
    protected String obtenerConsultaCheques (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String requestDispatcher = null;
        EIGlobal.mensajePorTrace ( CLASS_NAME + "obtenerConsultaCheques", EIGlobal.NivelLog.DEBUG);
        request.getSession ().removeAttribute ( MOD_CHEQUE_CHEQUES );
        request.getSession ().removeAttribute ( MOD_CHEQUE_ARCHIVO_EXP ) ;
        //        request.getSession ().removeAttribute ( MOD_CHEQUE_TIPO_CHEQUE );
        request.getSession ().removeAttribute ( MOD_CHEQUE_NUM_CUENTA );

        BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");
        String trama = generaTramaConsCheq ( request,response );
        if(null == trama ){
            return null;
        }

        ServicioTux servicio = new ServicioTux ();
        try{
            //servicio.setContext ( ((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext () );
            java.util.Hashtable ret = servicio.web_red ( trama );
            if(null == ret){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }
            String tramaRespuesta = (String) ret.get ( "BUFFER" );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": tramaRespuesta="+ tramaRespuesta, EIGlobal.NivelLog.DEBUG);
            if(null == tramaRespuesta
            || !tramaRespuesta.startsWith ("OK") ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al realizar la consulta, " + tramaRespuesta, EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            } else if( -1 == tramaRespuesta.indexOf ( '@' ) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama respuesta no cumple formato", EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            }else {
                StringTokenizer tok = new StringTokenizer ( tramaRespuesta,"@" );
                String msg = tok.nextToken ();
                if( !msg.endsWith ("0000") ){
                    msg = tok.nextToken ();
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg, EIGlobal.NivelLog.INFO);
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": el servicio no respondo de manera adecuada", EIGlobal.NivelLog.DEBUG);
                    throw new Exception (  msg );
                }else if( tok.hasMoreTokens () ){
                    tok.nextToken ();
                    if( tok.hasMoreTokens () ){
                        msg = tok.nextToken ();
                        EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg , EIGlobal.NivelLog.INFO);
                        request.setAttribute (MOD_CHEQUE_MSG_RETORNO, msg );
                    }
                }else{
                    request.setAttribute (MOD_CHEQUE_MSG_RETORNO, "" );
                }
            }
            //TODO: BIT CU 4191, B3
            /*
    		 * VSWF ARR -I
    		 */
            if (Global.USAR_BITACORAS.trim().equals("ON")){
	            try{
	            	String cuentaBit=(String)request.getSession().getAttribute ( MOD_CHEQUE_NUM_CUENTA );
				BitaHelper bh = new BitaHelperImpl(request, baseResource,request.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_CHEQUES_CANC_GENERA_TRAMA_CHEQ);
				bt.setContrato(baseResource.getContractNumber());
				bt.setServTransTux(OP_TRAMA_CONS_CHEQ);
				if(cuentaBit!=null &&  !cuentaBit.equals(""))
					bt.setCctaOrig(cuentaBit);

				if(tramaRespuesta!=null){
					 if(tramaRespuesta.substring(0,2).equals("OK")){
						   bt.setIdErr(OP_TRAMA_CONS_CHEQ+"0000");
					 }else if(tramaRespuesta.length()>8){
						  bt.setIdErr(tramaRespuesta.substring(0,8));
					 }else{
						  bt.setIdErr(tramaRespuesta.trim());
					 }
					}
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
            }
    		/*
    		 * VSWF ARR F
    		 */
            File archivoRemoto = new File ( Global.DIRECTORIO_REMOTO_INTERNET ,request.getRequestedSessionId () );
            ArchivoRemoto archRemoto = new ArchivoRemoto ();
            if( ! archRemoto.copia ( archivoRemoto.getName (),archivoRemoto.getParent (), REMOTO_A_LOCAL ) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": No se pudo copiar el archivo de respuesta", EIGlobal.NivelLog.DEBUG);
                throw new Exception (IEnlace. MSG_PAG_NO_DISP );
            }
            File archivoRespuesta = new File ( Global.DIRECTORIO_LOCAL, archivoRemoto.getName () );

            requestDispatcher = validaRespuestaCheque ( archivoRespuesta,request, response );
            if(null == requestDispatcher){
                return requestDispatcher;
            }
            request.setAttribute ("start", new Integer (0) );
            request.getSession ().setAttribute ("despl", new Integer ( Global.NUM_REGISTROS_PAGINA ) );
            //            //String tipoCheque = (String) request.getSession ().getAttribute ( MOD_CHEQUE_TIPO_CHEQUE );
            String encabezado = CreaEncabezado (MOD_CHEQUE_S_MODULO,
            MOD_CHEQUE_S_CABECERA
            //    , MOD_CHEQUE_S_HLP_RESPUESTA
            //, MOD_CHEQUE_S_HLP_RESPUESTA_TRANS_PROG
            ,MOD_CHEQUE_S_HLP_RESPUESTA
                , request);
            //            if(tipoCheque.startsWith ( "P" )){
            //                encabezado = CreaEncabezado (MOD_CHEQUE_S_MODULO,
            //                MOD_CHEQUE_S_CABECERA,
            //                MOD_CHEQUE_S_HLP_RESPUESTA_TRANS_PROG, request);
            //            }
            request.setAttribute ("encabezado",encabezado);
        }catch(java.util.NoSuchElementException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Trama no cumple con el formato," + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            despliegaPaginaErrorURL ( IEnlace.MSG_PAG_NO_DISP, MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,MOD_CHEQUE_S_HLP_INICIAL, "ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_CHEQUES,request,response);
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": "+ex.getClass ().getName ()
            +":"+ ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            if(ex instanceof RuntimeException){
                ex.printStackTrace ();
            }
            despliegaPaginaErrorURL ( ex.getMessage (),MOD_CHEQUE_S_MODULO,MOD_CHEQUE_S_CABECERA,MOD_CHEQUE_S_HLP_INICIAL, "ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_CHEQUES, request,response);
        }

        //Exportar archivo
        if( null != requestDispatcher ){
            exportarArchivoCheques (request,response);
        }

        return requestDispatcher;
    }

    /** genera la trama para la consulta de cheques
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return trama para realizar la consulta
     */
    protected String generaTramaConsCheq ( HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
        StringBuffer retValue = new StringBuffer ();

        BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");

        GregorianCalendar libDesde = null;
        GregorianCalendar libHasta = null;
        GregorianCalendar liqDesde = null;
        GregorianCalendar liqHasta = null;
        GregorianCalendar vigDesde = null;
        GregorianCalendar vigHasta = null;
        String numCuenta = request.getParameter ( MOD_CHEQUE_NUM_CUENTA );
        request.getSession ().setAttribute ( MOD_CHEQUE_NUM_CUENTA, numCuenta );

        EIGlobal.mensajePorTrace ( CLASS_NAME + ": numCuenta=" + numCuenta, EIGlobal.NivelLog.INFO);
        String [] status = request.getParameterValues ( MOD_CHEQUE_STATUS );
        if (null == status){
            status = new String[]{

            };
        }
        EIGlobal.mensajePorTrace ( CLASS_NAME + ": status=" + status, EIGlobal.NivelLog.INFO);
        String email = request.getParameter ( MOD_CHEQUE_EMAIL );
        if(null == email || email.trim ().equals ("") ){
            email = " ";
        }else{
            String user = email.substring (0,email.indexOf ('@') );
            String dominio = email.substring ( email.indexOf ('@') +1);
            email = user.trim () + "[]" + dominio.trim ();
        }
        EIGlobal.mensajePorTrace ( CLASS_NAME + ": email=" + email, EIGlobal.NivelLog.INFO);
        //        // en caso de que 390 se adecue para contener el beneficiario
        //        String benef = request.getParameter ( MOD_CHEQUE_BENEF );
        //        EIGlobal.mensajePorTrace( CLASS_NAME + ": benef=" + benef, EIGlobal.NivelLog.INFO);
        double importe = Double.NaN;
        long chkDesde = -1;
        long chkHasta = -1;
        try{
            libDesde = new GregorianCalendar ();
            libDesde.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_LIB_DESDE ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": libDesde=" + sdf.format ( libDesde.getTime () ), EIGlobal.NivelLog.INFO);
            libHasta = new GregorianCalendar ();
            libHasta.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_LIB_HASTA ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": libDesde=" + sdf.format ( libDesde.getTime () ), EIGlobal.NivelLog.INFO);
            liqDesde = new GregorianCalendar ();
            liqDesde.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_LIQ_DESDE ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": liqDesde=" + sdf.format ( liqDesde.getTime () ), EIGlobal.NivelLog.INFO);
            liqHasta = new GregorianCalendar ();
            liqHasta.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_LIQ_HASTA ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": liqHasta=" + sdf.format ( liqHasta.getTime () ), EIGlobal.NivelLog.INFO);
            vigDesde = new GregorianCalendar ();
            vigDesde.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_VIG_DESDE ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": vigDesde=" + sdf.format ( vigDesde.getTime () ), EIGlobal.NivelLog.INFO);
            vigHasta = new GregorianCalendar ();
            vigHasta.setTime ( sdf.parse ( request.getParameter ( MOD_CHEQUE_VIG_HASTA ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": vigHasta=" + sdf.format ( vigHasta.getTime () ), EIGlobal.NivelLog.INFO);
            String tmp = request.getParameter ( MOD_CHEQUE_IMPORTE);
            if( null != tmp && !tmp.trim ().equals ("") ){
                importe = Double.parseDouble ( tmp );
            }
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": importe=" + importe, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": importeFormat=" + def.format ( importe ), EIGlobal.NivelLog.INFO);
            tmp = request.getParameter ( MOD_CHEQUE_CHK_DESDE);
            if( null != tmp && !tmp.trim ().equals ("") ){
                chkDesde = Long.parseLong ( tmp );
            }
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": chkDesde=" + chkDesde, EIGlobal.NivelLog.INFO);
            tmp = request.getParameter ( MOD_CHEQUE_CHK_HASTA);
            if( null != tmp && !tmp.trim ().equals ("") ){
                chkHasta = Long.parseLong ( tmp );
            }
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": chkHasta=" + chkHasta, EIGlobal.NivelLog.INFO);
        }catch(ParseException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al dar formato al los parametros, " + ex.getMessage () , EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_CHEQUE_S_MODULO,MOD_CHEQUE_S_CABECERA,request,response);
            return null;
        }catch(NumberFormatException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al dar formato al los parametros, " + ex.getMessage () , EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_CHEQUE_S_MODULO,MOD_CHEQUE_S_CABECERA,request,response);
            return null;
        }

        retValue.append ( HEAD_TRAMA_CONS_CHEQ ).append ( "|" )
        .append ( baseResource.getUserID8 () ).append ( "|" )
        .append ( OP_TRAMA_CONS_CHEQ ).append ( "|" )
        .append ( baseResource.getContractNumber () ).append ( "|" )
        .append ( baseResource.getUserID8 () ).append ( "|" )
        .append ( baseResource.getUserProfile () ).append ( "|" )
        .append ( numCuenta ).append ( "@" );
        if( null != request.getParameter ( MOD_CHEQUE_USE_LIB )
        && request.getParameter ( MOD_CHEQUE_USE_LIB ).equals ( Boolean.TRUE.toString () ) ){
            EIGlobal.mensajePorTrace( CLASS_NAME + " insertando fechas libramiento",  EIGlobal.NivelLog.DEBUG  );
            retValue.append ( sdfTramaCheques.format ( libDesde.getTime () ) ).append ( "@" );
            retValue.append ( sdfTramaCheques.format ( libHasta.getTime () ) ).append ( "@" );
        } else{
            EIGlobal.mensajePorTrace( CLASS_NAME + " saltando fechas libramiento",  EIGlobal.NivelLog.DEBUG );

            retValue.append ( " @ @" );
        }
        if( null != request.getParameter ( MOD_CHEQUE_USE_VIG )
        && request.getParameter ( MOD_CHEQUE_USE_VIG ).equals ( Boolean.TRUE.toString () ) ){
            EIGlobal.mensajePorTrace( CLASS_NAME + " insertando fechas vigencia",  EIGlobal.NivelLog.DEBUG );

            retValue.append ( sdfTramaCheques.format ( vigDesde.getTime () ) ).append ( "@" );
            retValue.append ( sdfTramaCheques.format ( vigHasta.getTime () ) ).append ( "@" );
        } else{
            EIGlobal.mensajePorTrace( CLASS_NAME + " saltando fechas vigencia",  EIGlobal.NivelLog.DEBUG );

            retValue.append ( " @ @" );
        }
        if( null != request.getParameter ( MOD_CHEQUE_USE_LIQ )
        && request.getParameter ( MOD_CHEQUE_USE_LIQ ).equals ( Boolean.TRUE.toString () ) ){
            EIGlobal.mensajePorTrace( CLASS_NAME + " insertando fechas liquidacion",  EIGlobal.NivelLog.DEBUG );

            retValue.append ( sdfTramaCheques.format ( liqDesde.getTime () ) ).append ( "@" );
            retValue.append ( sdfTramaCheques.format ( liqHasta.getTime () ) ).append ( "@" );

        } else{
            EIGlobal.mensajePorTrace( CLASS_NAME + " saltando fechas liquidacion",  EIGlobal.NivelLog.DEBUG );

            retValue.append ( " @ @" );
        }
        retValue.append ((chkDesde > 0)? String.valueOf ( chkDesde ): " " ).append ( "@" );
        retValue.append  ((chkHasta > 0)? String.valueOf ( chkHasta ): " " ) .append ( "@" );
        retValue.append  (( !Double.isNaN ( importe ) )? def.format ( importe ) : " " ) .append ( "@" );
        retValue.append  ( chequeStatus ( status ) ).append ( "@" )
        .append ( new File ( Global.DIRECTORIO_REMOTO_INTERNET, request.getRequestedSessionId () ).getPath () ).append ( "@" )
        //          Se comenta para eliminar la utlilizacion se servicios de
        // correo en la aplicacion
                .append ( email ).append ("@")
        .append ( baseResource.getContractNumber () ).append ( "@" );

        EIGlobal.mensajePorTrace ( CLASS_NAME + " trama=" + retValue.toString () , EIGlobal.NivelLog.DEBUG);
        return retValue.toString ();
    }
    /** Obtiene la sub cadena para la trama con los status del filtro de cheques
     * @param status arreglo con los status que se van a filtrar
     * @return cadena con los status o <code>" @ @ @ "</code>
     */
    protected String chequeStatus ( String[] status ){
        StringBuffer retValue = new StringBuffer ();
        java.util.Set set = new java.util.HashSet ();
        for(int i = 0; i < status.length;++i){
            set.add ( status[i] );
        }
        if ( set.contains ( MOD_CHEQUE_STATUS_PENDIENTE ) ){
            retValue.append ( MOD_CHEQUE_STATUS_PENDIENTE );
        }else{
            retValue.append ( " " );
        }
        retValue.append ( "@" );
        if ( set.contains ( MOD_CHEQUE_STATUS_LIQUIDADO ) ){
            retValue.append ( MOD_CHEQUE_STATUS_LIQUIDADO );
        }else{
            retValue.append ( " " );
        }
        retValue.append ( "@" );
        if ( set.contains ( MOD_CHEQUE_STATUS_CANCELADO ) ){
            retValue.append ( MOD_CHEQUE_STATUS_CANCELADO );
        }else{
            retValue.append ( " " );
        }
        retValue.append ( "@" );
        if ( set.contains ( MOD_CHEQUE_STATUS_VENCIDO ) ){
            retValue.append ( MOD_CHEQUE_STATUS_VENCIDO );
        }else{
            retValue.append ( " " );
        }
        EIGlobal.mensajePorTrace ( CLASS_NAME + ":status=" + retValue.toString () , EIGlobal.NivelLog.INFO);
        return retValue.toString ();
    }

    /** realiza la validacion de la respuesta obtenida de tuxedo en la consulta de cheques
     * @param archivoRespuesta archivo de respuesta de la consulta
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return String con el request dispatcher a utilizar
     */
    protected String validaRespuestaCheque ( File archivoRespuesta, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
        EIGlobal.mensajePorTrace ( CLASS_NAME + ": validaRespuestaCheque", EIGlobal.NivelLog.DEBUG);
        String requestDispatcher = null;
        BufferedReader reader = null;
        try{
            reader = new BufferedReader ( new java.io.FileReader ( archivoRespuesta ) );
            String trama = null;
            java.util.List cheques = new java.util.LinkedList ();
            try{
                while ( null != (trama = reader.readLine () ) ){
                    mx.altec.enlace.ches.ChesCheque cheque = mx.altec.enlace.ches.ChesChequeConsultaFormat.getInstance ().parse ( trama );
                    cheques.add ( cheque );
                }
                if( cheques.size () > 0){
                    request.getSession ().setAttribute ( MOD_CHEQUE_CHEQUES, cheques );
                    //                    request.getSession ().setAttribute ( MOD_CHEQUE_TIPO_CHEQUE, request.getParameter (MOD_CHEQUE_STATUS) );
                    requestDispatcher = MOD_CHEQUE_RESPUESTA_DISPATCHER;
                }else if ( null != request.getAttribute (MOD_CHEQUE_MSG_RETORNO) &&
                ! ( (String) request.getAttribute (MOD_CHEQUE_MSG_RETORNO)).equals ("") ) {
                    despliegaPaginaErrorURL ( (String) request.getAttribute (MOD_CHEQUE_MSG_RETORNO) ,MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,MOD_CHEQUE_S_HLP_INICIAL,"ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_CHEQUES, request,response);
                }else{
                    despliegaPaginaErrorURL ("No se encontraron resultados para la consulta",MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,MOD_CHEQUE_S_HLP_INICIAL,"ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_CHEQUES, request,response);
                }
            }catch(ParseException ex){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al obtener el cheque,"  +  ex.getMessage () + "linea: " + cheques.size () + 1 +" car:" + ex.getErrorOffset () , EIGlobal.NivelLog.DEBUG);
                despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,request,response);
            }

        }catch(IOException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al leer el archivo de respuesta", EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_CHEQUE_S_MODULO, MOD_CHEQUE_S_CABECERA,request,response);
        }finally{
            if ( null != reader ){
                try {
                    reader.close ();
                } catch ( IOException e ){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al cerrar el reader", EIGlobal.NivelLog.DEBUG);
                }
                reader= null;
            }
        }
        return requestDispatcher;
    }

    /** Realiza la peticion de consulta de archivos
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return String con el request dispatcher a utilizar
     */
    protected String obtenerConsultaArchivos (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String requestDispatcher = null;
        BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");
        request.getSession ().removeAttribute ( MOD_ARCHIVO_ARCHIVO_EXP );
        request.getSession ().removeAttribute ( MOD_ARCHIVO_TIPO_ARCHIVO );
        try{
            GregorianCalendar fechaInicial = new GregorianCalendar ();
            fechaInicial.setTime ( sdf.parse ( request.getParameter ( MOD_ARCHIVO_FECHA_INICIAL ) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + MOD_ARCHIVO_FECHA_INICIAL + "=" + sdf.format ( fechaInicial.getTime ()) , EIGlobal.NivelLog.INFO);
            GregorianCalendar fechaFinal = new GregorianCalendar ();
            fechaFinal.setTime ( sdf.parse ( request.getParameter ( MOD_ARCHIVO_FECHA_FINAL) ) );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": fechaFinal=" + sdf.format ( fechaFinal.getTime () ), EIGlobal.NivelLog.INFO);
            long secuencia = -1;
            if( (null != request.getParameter (MOD_ARCHIVO_SECUENCIA)) &&
            ( !request.getParameter (MOD_ARCHIVO_SECUENCIA).trim ().equals ("") ) ){
                secuencia = Long.parseLong ( request.getParameter ( MOD_ARCHIVO_SECUENCIA ) );
            }
            String []status = request.getParameterValues (MOD_ARCHIVO_STATUS);
            if(null == status){
                status = new String[]{
                };
            }

            File archivoRemoto = new File ( Global.DIRECTORIO_REMOTO_INTERNET, request.getRequestedSessionId () );
            File archivoRespuesta = new File ( Global.DIRECTORIO_LOCAL, archivoRemoto.getName () );

            String tramaSalida = "2EWEB|"
            + baseResource.getUserID8 () + "|"
            + "CHCA" + "|"
            + baseResource.getContractNumber () + "|"
            + baseResource.getUserID8 () + "|"
            + baseResource.getUserProfile () + "|"
            + baseResource.getContractNumber () + "@"
            + sdfTramaArchivos.format ( fechaInicial.getTime () ) + "@"
            + sdfTramaArchivos.format ( fechaFinal.getTime () ) + "@"
            + ((secuencia>0)? Long.toString (secuencia):" ") + "@"
            + archStatus ( status ) + "@"
            + archivoRemoto.getPath () + "@";

            EIGlobal.mensajePorTrace ( CLASS_NAME + ": tramaSalida=" + tramaSalida , EIGlobal.NivelLog.DEBUG);

            ServicioTux servicio = new ServicioTux ();
            //servicio.setContext ( ((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext () );

            java.util.Hashtable ret = servicio.web_red ( tramaSalida );
            if(null == ret){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }
            String tramaRespuesta = (String) ret.get ( "BUFFER" );
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": tramaRespuesta="+ tramaRespuesta, EIGlobal.NivelLog.DEBUG);
            if(null == tramaRespuesta ||
            !tramaRespuesta.startsWith ("OK") ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al realizar la consulta, " + tramaRespuesta, EIGlobal.NivelLog.DEBUG);
                throw new Exception (IEnlace.MSG_PAG_NO_DISP );
            }else if(-1 == tramaRespuesta.indexOf ( '@' ) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al realizar la consulta, " + tramaRespuesta, EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            }else {
                StringTokenizer tok = new StringTokenizer ( tramaRespuesta, "@" );
                String msg = tok.nextToken ();

                if( !msg.endsWith ("0000") ){
                    msg = tok.nextToken ();
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg, EIGlobal.NivelLog.DEBUG);
                    throw new Exception ( IEnlace.MSG_PAG_NO_DISP
                    + " " + msg );
                }
                msg = tok.nextToken ();
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg, EIGlobal.NivelLog.INFO);

            }

            ArchivoRemoto archRemoto = new ArchivoRemoto ();

            if (!archRemoto.copia ( archivoRemoto.getName (),archivoRemoto.getParent (),REMOTO_A_LOCAL) ){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR no se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
                throw new Exception ( IEnlace.MSG_PAG_NO_DISP );
            }

            request.getSession ().setAttribute ( MOD_ARCHIVO_TIPO_ARCHIVO, request.getParameter ( MOD_ARCHIVO_STATUS) );

            requestDispatcher = validaRespuestaArchivo (archivoRespuesta,request,response);
            File archivoWeb = new File (archivoRespuesta.getParent (),archivoRespuesta.getName () + ".doc" );
            archivoRespuesta.renameTo ( archivoWeb );
            if(archRemoto.copiaLocalARemoto ( archivoWeb.getName (), "WEB") ){// Modificacion RMM 20030130
                request.getSession ().setAttribute ( MOD_ARCHIVO_ARCHIVO_EXP, archivoWeb.getName () );
            }
            String encab = CreaEncabezado (MOD_ARCHIVO_S_MODULO,
            MOD_ARCHIVO_S_CABECERA,
            MOD_ARCHIVO_S_HLP_RESULTADO, request);
            if( status.equals ("P")){
                encab = CreaEncabezado (MOD_ARCHIVO_S_MODULO,
                MOD_ARCHIVO_S_CABECERA,
                MOD_ARCHIVO_S_HLP_RESULTADO_PROCESADOS, request);
            }
            //TODO: BIT CU 4201, B1
						/*
						 * VSWF ARR -I
						 */
				if (Global.USAR_BITACORAS.trim().equals("ON")){


					try{
						java.util.List listaArchivos= (java.util.List)request.getSession ().getAttribute (MOD_ARCHIVO_ARCHIVOS);
						ListIterator li = null;
						if(listaArchivos != null) {
							li = listaArchivos.listIterator();
						}

						while (li != null && li.hasNext()) {
							mx.altec.enlace.ches.ChesArchivoImportado archivo = (mx.altec.enlace.ches.ChesArchivoImportado) li.next();


									BitaHelper bh = new BitaHelperImpl(request, baseResource,request.getSession());
									BitaTransacBean bt = new BitaTransacBean();
									bt = (BitaTransacBean)bh.llenarBean(bt);
									bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_ARCHIVOS_CONS_ARCH_CHEQ);



									bt.setContrato(baseResource.getContractNumber());
									bt.setServTransTux("CHCA");
									if(tramaRespuesta!=null){
										 if(tramaRespuesta.substring(0,2).equals("OK")){
											   bt.setIdErr("CHCA0000");
										 }else if(tramaRespuesta.length()>8){
											  bt.setIdErr(tramaRespuesta.substring(0,8));
										 }else{
											  bt.setIdErr(tramaRespuesta.trim());
										 }
										}
									bt.setNombreArchivo(archivo.getFileName ());

									if (archivo.getStatus()!=null && !archivo.getStatus().trim().equals("")){
										bt.setEstatus(archivo.getStatus().trim().substring(0,1));
									}


									BitaHandler.getInstance().insertBitaTransac(bt);
						}
									}catch (SQLException e){
										e.printStackTrace();
									}catch(Exception e){
										e.printStackTrace();
									}

				}
						/*
						 * VSWF ARR -F
						 */


            request.setAttribute ("encabezado",encab);
        }catch(ParseException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al dar formato a la fecha," +  ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            //            ex.printStackTrace ();
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
        }catch ( NumberFormatException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al dar formato al numero, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            //            ex.printStackTrace ();
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
        }catch (java.util.NoSuchElementException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Trama no tiene el formato adecuado, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            //            ex.printStackTrace ();
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
        }catch (Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": "  + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            //            ex.printStackTrace ();
            despliegaPaginaError ( ex.getMessage (),MOD_ARCHIVO_S_MODULO,MOD_ARCHIVO_S_CABECERA, request,response);

        }

        return requestDispatcher;
    }
    /** @TODO: cambiar implemente cion por la verdadera de arch*/
    protected String archStatus ( String[] status ){
        StringBuffer retValue = new StringBuffer ();
        java.util.Set set = new java.util.HashSet ();
        for(int i = 0; i < status.length;++i){
            set.add ( status[i] );
        }
        if ( set.contains ( MOD_ARCHIVO_STATUS_RECIBIDO ) ){
            retValue.append ( MOD_ARCHIVO_STATUS_RECIBIDO );
        }else{
            retValue.append ( " " );
        }
        retValue.append ( "@" );
        if ( set.contains ( MOD_ARCHIVO_STATUS_ENVIADO ) ){
            retValue.append ( MOD_ARCHIVO_STATUS_ENVIADO );
        }else{
            retValue.append ( " " );
        }
        retValue.append ( "@" );
        if ( set.contains ( MOD_ARCHIVO_STATUS_PROCESADO ) ){
            retValue.append ( MOD_ARCHIVO_STATUS_PROCESADO );
        }else{
            retValue.append ( " " );
        }

        EIGlobal.mensajePorTrace ( CLASS_NAME + ":status=" + retValue.toString () , EIGlobal.NivelLog.INFO);
        return retValue.toString ();
    }


    /** Realiza la validacion de la respuesta obtenida de la consulta de archivos
     * @param archivoRespuesta archivo de respuesta de tuxedo
     * @param request pre spec
     * @param response pre spec
     * @throws IOException pre spec
     * @throws ServletException pre spec
     * @return String con el request dispatcher a utilizar
     */
    protected String validaRespuestaArchivo ( File archivoRespuesta, HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException{
        String requestDispatcher = null;
        java.util.List archivos = new java.util.LinkedList ();
        BufferedReader reader = null;
        request.getSession ().removeAttribute ( MOD_ARCHIVO_MSG );

        try{
            reader = new BufferedReader ( new java.io.FileReader ( archivoRespuesta) );
            String trama = null;

            while (null != (trama  = reader.readLine ())){
                mx.altec.enlace.ches.ChesArchivoImportado archivo = mx.altec.enlace.ches.ChesArchivoImportadoConsFormat.getInstance ().parse ( trama );
                archivos.add ( archivo );
            }
            if( archivos.size () > 0){
                request.getSession ().setAttribute (MOD_ARCHIVO_ARCHIVOS,archivos);
                request.setAttribute ("start", new Integer (0) );
                request.getSession ().setAttribute ("despl", new Integer ( Global.NUM_REGISTROS_PAGINA ) );
                requestDispatcher = MOD_ARCHIVO_RESPUESTA_DISPATCHER;
            }else{
                despliegaPaginaErrorURL ("No se encontraron resultados para la consulta",MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,MOD_ARCHIVO_S_HLP_INICIAL,"ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_ARCHIVOS, request,response);
            }
        }catch(ParseException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al recuperar los cheques del archivo, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            if( archivos.isEmpty () ){
                despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
            }else {
                request.getSession ().setAttribute (MOD_ARCHIVO_ARCHIVOS,archivos);
                request.setAttribute ("start", new Integer (0) );
                request.getSession ().setAttribute ("despl", new Integer ( Global.NUM_REGISTROS_PAGINA ) );
                request.getSession ().setAttribute (MOD_ARCHIVO_MSG, "No se pudo obtener el resto de los archivos");
                requestDispatcher = MOD_ARCHIVO_RESPUESTA_DISPATCHER;
            }
        }catch(IOException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al leer el archivo de respuesta", EIGlobal.NivelLog.DEBUG);
            despliegaPaginaError ( IEnlace.MSG_PAG_NO_DISP,MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
        }finally{
            if ( null != reader ){
                try {
                    reader.close ();
                } catch ( IOException e ){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al cerrar el lector," + e.getMessage (), EIGlobal.NivelLog.DEBUG);
                }
                reader= null;
            }
        }


        return requestDispatcher;
    }

    /** Realiza la exportacion de los cheques en la consulta de cheques
     * @param request per spec
     * @param response per spec
     */
    protected void exportarArchivoCheques ( HttpServletRequest request, HttpServletResponse response) {
        File archivoExportar = new File ( Global.DIRECTORIO_LOCAL, request.getRequestedSessionId ()  + ".doc");
        java.io.BufferedWriter writer = null;
        try {
            writer = new java.io.BufferedWriter ( new java.io.FileWriter ( archivoExportar ) );
            java.util.List cheques = (java.util.List) request.getSession ().getAttribute ( MOD_CHEQUE_CHEQUES );
            java.util.ListIterator li = cheques.listIterator ();
            while (li.hasNext ()){
                writer.write ( mx.altec.enlace.ches.ChesChequeExpFormat.getInstance ().format ( (mx.altec.enlace.ches.ChesCheque)li.next () ) );
                writer.newLine ();
            }
            writer.flush ();
            writer.close ();
            writer = null;

            ArchivoRemoto archRemoto = new ArchivoRemoto ();
            //            if(! archRemoto.copiaLocalARemoto ( archivoExportar.getName (), Global.DIRECTORIO_REMOTO_EXP , LOCAL_A_REMOTO ) ){// Modificacion RMM 20030130
            if(! archRemoto.copiaLocalARemoto ( archivoExportar.getName (), "WEB" ) ){
                throw new Exception ("No se pudo enviar el archivo Exportado");
            }
            request.getSession ().setAttribute ( MOD_CHEQUE_ARCHIVO_EXP, archivoExportar.getName () );
        }catch (IOException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al escribir el archivo de exportacion", EIGlobal.NivelLog.DEBUG);
        }catch (Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
        }finally{
            if ( null != writer ){
                try {
                    writer.close ();
                } catch ( IOException e ){
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": error al cerrar el writer", EIGlobal.NivelLog.DEBUG);
                }
                writer= null;
            }
        }
    }

    /** Realiza la consulta detallada de archivos
     * @return request dispatcher a utilizar
     * @param request per spec
     * @param response per spec
     * @throws IOException per spec
     * @throws ServletException per spec */
    protected String consultaDetalle (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String email = request.getParameter ( EMAIL );

        try{

            if(null == email || email.trim ().equals ("")){
                email = " ";
            }else{
                String user = email.substring (0,email.indexOf ('@') );
                String dominio = email.substring ( email.indexOf ('@') +1);
                email = user.trim () + "[]" + dominio.trim ();
            }
            ServicioTux servicio = new ServicioTux ();
            //servicio.setContext ( ((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext () );
            BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");

            String numSec = request.getParameter ( "reg" );
            if( null == numSec || numSec.trim ().equals ("") ){
                throw new Exception ("El parametro no existe");
            }
            String trama = "1EWEB|"
            + baseResource.getUserID8 () + "|"
            + "CHDA" + "|"
            + baseResource.getContractNumber () + "|"
            + baseResource.getUserID8 () + "|"
            + baseResource.getUserProfile () + "|"
            + numSec + "@"
            + email + "@";

            EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama=" + trama, EIGlobal.NivelLog.DEBUG);
            java.util.Hashtable ret = servicio.web_red ( trama );
            if(null == ret){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }

            String tramaRetorno = (String) ret.get ("BUFFER");
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama retorno=\n" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
            if(null == tramaRetorno){
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR el servicio no respondio de manera correcta, retorno null" , EIGlobal.NivelLog.DEBUG);
                throw new Exception ("Error al llamar al servicio. " + IEnlace.MSG_PAG_NO_DISP );
            }

            if(!tramaRetorno.startsWith ("OK") ){
                throw new Exception ( "Trama no esperada, " + tramaRetorno);
            }else if(-1 == tramaRetorno.indexOf ( '@' ) ){
                throw new Exception ( "Trama no corresponde con el formato esperado, " + tramaRetorno);
            }else{
                StringTokenizer tok = new StringTokenizer ( tramaRetorno, "@" );
                String msg = tok.nextToken ();
                EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg, EIGlobal.NivelLog.INFO);
                if(! msg.endsWith ("0000") ){
                    msg = tok.nextToken ();
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al realizar la operacion msg=" + msg, EIGlobal.NivelLog.DEBUG);
                    despliegaPaginaError ( msg, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
                }else{
                    msg = tok.nextToken ();
                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": msg=" + msg, EIGlobal.NivelLog.INFO);
                    despliegaPaginaError ( msg, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,request,response);
                }
            }
            //TODO: BIT CU 4201, A7
            /*
    		 * VSWF ARR -I
    		 */
            if (Global.USAR_BITACORAS.trim().equals("ON")){
	            try{
	    		BitaHelper bh = new BitaHelperImpl(request, baseResource,request.getSession());
	    		BitaTransacBean bt = new BitaTransacBean();
	    		bt = (BitaTransacBean)bh.llenarBean(bt);
	    		bt.setNumBit(BitaConstants.ES_CH_SEG_CONS_ARCHIVOS_CONS_DET_ARCH_CHEQ);
	    		bt.setContrato(baseResource.getContractNumber());
	    		bt.setServTransTux("CHDA");
	    		if(tramaRetorno!=null){
					 if(tramaRetorno.substring(0,2).equals("OK")){
						   bt.setIdErr("CHDA0000");
					 }else if(tramaRetorno.length()>8){
						  bt.setIdErr(tramaRetorno.substring(0,8));
					 }else{
						  bt.setIdErr(tramaRetorno.trim());
					 }
					}
	    		BitaHandler.getInstance().insertBitaTransac(bt);
	    		}catch (SQLException e){
	    			e.printStackTrace();
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}
            }
    		/*
    		 * VSWF ARR -F
    		 */
        }catch(IndexOutOfBoundsException ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": el email no tiene el formato adecuado, email=" + email, EIGlobal.NivelLog.DEBUG);
        }catch(Exception ex){
            EIGlobal.mensajePorTrace ( CLASS_NAME + ": Error al hacer la peticion de reporte por correo, " + ex.getMessage (), EIGlobal.NivelLog.DEBUG);
            ex.printStackTrace ();
            despliegaPaginaErrorURL ( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,MOD_ARCHIVO_S_HLP_INICIAL, "ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_ARCHIVOS,request,response);
        }

        return null;
    }
    public static final String MOD_MACROS_S_MODULO = "Generaci&oacute;n de Archivos - Descarga de Aplicaci&oacute;n";
    public static final String MOD_MACROS_S_CABECERA = "Administraci&oacute;n y Control &gt; Generaci&oacute;n de Archivos - Descarga de Aplicaci&oacute;n";
    public static final String MOD_MACROS_S_HLP_INICIAL = "s35000h";
    /** Descpliega la pantalla de macros
     * TODO: ELIMINAR Y OVER A UN SERVLET ESPECIFICO CUANDO SE TENGA UNO
     */
    protected String despliegaMacros ( HttpServletRequest request, HttpServletResponse response )
    throws  IOException , ServletException{
        String retValue = MOD_MACROS_DEF_TEMPLATE;
        String encabezado = CreaEncabezado (MOD_MACROS_S_MODULO,
        MOD_MACROS_S_CABECERA,
        MOD_MACROS_S_HLP_INICIAL, request);
		//TODO BIT CU5111, inicio de flujo generación manual de archivos
		/*VSWF-HGG-I*/
        if (Global.USAR_BITACORAS.trim().equals("ON")){

			BaseResource session = (BaseResource) request.getSession().getAttribute("session");
			BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());

			bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_GEN_MANUAL_ARCHIVOS_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
		/*VSWF-HGG-F*/

        request.setAttribute ("encabezado",encabezado);
        return retValue;
    }

//    /** Controlador para el modulo de consulta de resultados
//     * @param request per spec
//     * @param response per spec
//     * @throws IOException per spec
//     * @throws ServletException per spec
//     * @return null o el path del request dispatcher a utilizar
//     */
//    public String despliegaResultados (HttpServletRequest request,
//    HttpServletResponse response)
//    throws IOException, ServletException{
//        String retValue = null;
//        String encabezado = CreaEncabezado (MOD_RESULTADOS_S_MODULO,
//        MOD_RESULTADOS_S_CABECERA,
//        MOD_RESULTADOS_S_AYUDA, request);
//        request.setAttribute ("encabezado",encabezado);
//
//        int opcion = 0;
//
//        try{
//            opcion = Integer.parseInt ( request.getParameter ( PAR_OPCION ) );
//        }catch(Exception ex){
//            EIGlobal.mensajePorTrace ( CLASS_NAME + ": opcion incorrecta, debe ser numerica", EIGlobal.NivelLog.DEBUG);
//        }
//
//        switch (opcion){
//            case 1:{
//                java.io.File[] archivos = (java.io.File[])request.getSession ()
//                .getAttribute ("chesArchivosResultado");
//                if(null == archivos){
//                    despliegaPaginaError ("No hay resultados para su consulta"
//                    ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//                    ,request,response);
//                    break;
//                }
//
//                try{
//                    java.io.File archivo = archivos[Integer
//                    .parseInt (request.getParameter ("archivo"))];
//                    retValue = servirArchivo (archivo, "application/x-gzip",request,response);
//                }catch(NumberFormatException e){
//                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + e, EIGlobal.NivelLog.DEBUG);
//                    despliegaPaginaError ("No hay resultados para su consulta"
//                    ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//                    ,request,response);
//                    break;
//                }catch(java.lang.ArrayIndexOutOfBoundsException e){
//                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": " + e, EIGlobal.NivelLog.DEBUG);
//                    break;
//                }
//
//                break;
//            }
//            case 0:
//            default:{
//                retValue = obtenerResultados (request,response);
//                break;
//            };
//        };
//        return retValue;
//    }


    //    public class ResultadosFilter implements java.io.FilenameFilter {
    //        private String contrato;
    //        public ResultadosFilter( String contrato) {
    //            this.contrato = contrato;
    //        }
    //        public boolean accept (java.io.File file, String str) {
    //            return str.startsWith ( contrato + "_" );
    //        }
    //
    //    }

//    /** realiza la busqueda de archivos por contrato
//     * @param request per spec
//     * @param response per spec
//     * @throws IOException per spec
//     * @throws ServletException per spec
//     * @return request dispatcher a utilizar o null
//     */
//    public String obtenerResultados (HttpServletRequest request,
//    HttpServletResponse response)
//    throws IOException, ServletException{
//        String retValue = MOD_RESULTADOS_DEF_TEMPLATE;
//        BaseResource baseResource = (BaseResource) request.getAttribute ("baseResource");
//        String baseDir = Global.getProperty (MOD_RESULTADOS_BASE_DIR_KEY
//        ,MOD_RESULTADOS_BASE_DIR_DEF);
//        EIGlobal.mensajePorTrace ( CLASS_NAME + ": baseDir:" + baseDir, 5 );
//
//        java.io.File baseFile = new java.io.File (baseDir);
//        if(null == baseFile){
//            despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP
//            ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//            ,request,response);
//            return null;
//        }
//        java.io.FilenameFilter filter = new EnlaceMig.ches.ChesConsResFileFilter ( baseResource.getContractNumber () );
//        java.io.File [] archivos = baseFile.listFiles ( filter );
//        if (null == archivos){
//            despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP
//            ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//            ,request,response);
//            retValue = null;
//        }else if( archivos.length > 0){
//            request.getSession ().setAttribute ("chesArchivosResultado",archivos);
//        }else {
//            despliegaPaginaError ("No hay resultados para su consulta"
//            ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//            ,request,response);
//            retValue = null;
//        }
//
//        return retValue;
//    }

//    public String servirArchivo (java.io.File archivo,String contentType, HttpServletRequest request, HttpServletResponse response)
//    throws IOException,ServletException{
//        EIGlobal.mensajePorTrace ( CLASS_NAME + ": sirviendo archivo:" + archivo, EIGlobal.NivelLog.INFO);
//        String retValue = null;
//        java.io.BufferedInputStream inStream = null;
//        java.io.BufferedOutputStream outStream = null;
//        try{
//            byte[] buff = new byte[1024];
//            inStream = new java.io.BufferedInputStream (new java.io.FileInputStream ( archivo ));;
//            response.setContentType ( contentType );
//            int charsRead = -1;
//            outStream = new java.io.BufferedOutputStream (response.getOutputStream ());
//            while (-1 != (charsRead = inStream.read (buff,0,buff.length) ) ){
//                outStream.write (buff,0,charsRead);
//            }
//
//        }catch(IOException e){
//            EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR:" + e.getMessage (), EIGlobal.NivelLog.DEBUG);
//            e.printStackTrace ();
//            despliegaPaginaError (IEnlace.MSG_PAG_NO_DISP
//            ,MOD_RESULTADOS_S_MODULO,MOD_RESULTADOS_S_CABECERA
//            ,request,response);
//            retValue = null;
//        }finally{
//            if (null != inStream){
//                try{
//                    inStream.close ();
//                }catch(IOException ex){
//                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR AL CERRAR EL FLUJO DE ENTRADA: " + ex, EIGlobal.NivelLog.DEBUG);
//                    ex.printStackTrace ();
//                }
//                inStream = null;
//            }
//            if(null != outStream){
//                try{
//                    outStream.close ();
//                }catch(IOException ex){
//                    EIGlobal.mensajePorTrace ( CLASS_NAME + ": ERROR AL CERRAR EL FLUJO DE SALIDA:" + ex, EIGlobal.NivelLog.DEBUG);
//                    ex.printStackTrace ();
//                }
//                outStream = null;
//            }
//        }
//        return retValue;
//    }

//    public String consultaDetalleDownload (HttpServletRequest request,
//    HttpServletResponse response)
//    throws IOException, ServletException{
//        despliegaPaginaErrorURL( IEnlace.MSG_PAG_NO_DISP, MOD_ARCHIVO_S_MODULO, MOD_ARCHIVO_S_CABECERA,MOD_ARCHIVO_S_HLP_INICIAL, "ChesConsulta?" +PAR_MODULO + "=" + CONSULTA_ARCHIVOS,request,response);
//        return null;
//    }
}