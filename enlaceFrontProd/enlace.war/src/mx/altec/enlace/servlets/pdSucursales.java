package mx.altec.enlace.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.pdArchivoTemporal;
import mx.altec.enlace.bo.pdServicio;
import mx.altec.enlace.bo.pdSucursal;
import mx.altec.enlace.bo.pdSucursalCompPorCP;
import mx.altec.enlace.bo.pdSucursalCompPorCiu;
import mx.altec.enlace.bo.pdSucursalCompPorCve;
import mx.altec.enlace.bo.pdSucursalCompPorDel;
import mx.altec.enlace.bo.pdSucursalCompPorDom;
import mx.altec.enlace.bo.pdSucursalCompPorEdo;
import mx.altec.enlace.bo.pdSucursalCompPorNom;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;



/**
 * pdSucursales is a blank servlet to which you add your own code.
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class pdSucursales extends BaseServlet
{

    /** cadena para identifiacar el parametro de inicio
     */
    public static final String INICIO = "inicio";
    /** cadena para identifiacar el parametro de exportar
     */
    public static final String EXPORTAR = "exportar";
    /** cadena para identifiacar el parametro de consultar
     */
    public static final String CONSULTAR = "consultar";

    /** cadena para identificar el tipo de ordenamiento, por clave
     */
     public static final String POR_CLAVE = "cve";
     /** cadena para identificar el tipo de ordenamiento, por nombre
      */
     public static final String POR_NOMBRE = "nombre";
     /** cadena para identificar el tipo de ordenamiento, por domicilio
      */
     public static final String POR_DOMICILIO = "domicilio";
     /** cadena para identificar el tipo de ordenamiento, por Delegacion o municipio
      */
     public static final String POR_DELEGACION = "del";
     /** cadena para identificar el tipo de ordenamiento, por ciudad
      */
     public static final String POR_CIUDAD = "ciudad";
     /** cadena para identificar el tipo de ordenamiento, por estado
      */
     public static final String POR_EDO = "edo";
     /** cadena para identificar el tipo de ordenamiento, por codigo postal
      */
     public static final String POR_CP = "cp";

     public static final String DESPLAZAR = "desplazar";

    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }



    /**
     * <code>displayMessage</code> allows for a short message to be streamed
     * back to the user.  It is used by various NAB-specific wizards.
     * @param req the HttpServletRequest
     * @param res the HttpServletResponse
     * @param messageText the message-text to stream to the user.
     * @throws ServletException per spec
     * @throws IOException per spec
     */
    public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException
    {
        res.setContentType("text/html");
        java.io.PrintWriter out = res.getWriter();
        out.println(messageText);
    }


    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {
        if(! SesionValida( req, res ) ) return;
        /* Modificacion Rafael Martinez Montiel para eliminar la variable de instancia session
         */
        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        if(null == session ){
            EIGlobal.mensajePorTrace ("pdSucursales:defaultAction() session" + session, EIGlobal.NivelLog.INFO);
            req.setAttribute("Error",IEnlace.MSG_PAG_NO_DISP);
            return;
        }

        req.removeAttribute("Error");


        req.setAttribute("newMenu", session.getFuncionesDeMenu());
        req.setAttribute("MenuPrincipal", session.getStrMenu());
        /*Debug Code
         Copy Paste...*/String archivoAyuda = "s31050h";
        req.setAttribute("Encabezado",CreaEncabezado( "Cat&aacute;logo de Sucursales",
                         "Servicios &gt; Pago Directo &gt; Cat&aacute;logo de Sucursales",
                          archivoAyuda,req));

        if( ! session.getFacultad (session.FAC_PADIRACATSU_PD) ) {
        //!Boolean.getBoolean (session.getFAC_PADIRACATSU_PD ())){
            req.setAttribute("Error","No tiene facultad para ver el cat&aacute;logo de sucursales");
            evalTemplate("/jsp/EI_Error.jsp",req,res);
            return;
        } else {
            java.util.TreeSet ts = new java.util.TreeSet();
            if( session.getFacultad( session.FAC_PADIRACATSU_PD) ) ts.add("PADIRACATSU");
            if( session.getFacultad( session.FAC_PADIREXPARC_PD ) ) ts.add("PADIREXPARC");
            if(session.getFacultad( session.FAC_PADIRIMPREP_PD ) ) ts.add("PADIRIMPREP");
            req.setAttribute("pdSucPermisos",ts);
        }


//      TODO: BIT CU 4311, EL cliente entra al flujo
        /*
		 * VSWF ARR -I
		 */
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
        try{

        BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
        if (req.getParameter(BitaConstants.FLUJO)!=null){
		       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
		  }else{
		   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
		  }
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_PAGO_DIR_CAT_SUCURSALES_ENTRA);
		bt.setContrato(session.getContractNumber());

		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch (SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
        }
		/*
		 * VSWF ARR -F
		 */

        String opcion = req.getParameter("opcion");
        if( null == opcion || opcion.equals("") ){
            opcion = INICIO;
        }

        if(opcion.equals(INICIO)){
            consultar(req,res);
        } else {
            archivoAyuda = "s31050h";
            req.setAttribute("Encabezado",CreaEncabezado( "Cat&aacute;logo de Sucursales",
                         "Servicios &gt; Pago Directo &gt; Cat&aacute;logo de Sucursales",
                          archivoAyuda,req));
            if(opcion.equals(CONSULTAR)){
                consultar(req,res);
            } else if(opcion.equals(DESPLAZAR)){
                desplazar(req,res);
            }
        }

    }

    /**
     *inicia las operaciones del servle, obtiene las sucursales y envia los resultados sin formato a un jsp
     *@param req HttpServletRequest per spec
     *@param res HttpServletResponse per spec
     *@throws ServletException per spec
     *@throws IOException per spec
     */
    protected void iniciar(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
        evalTemplate("/jsp/pdSucursales.jsp",req,res);
    }

    /** exporta el resultado de la consulta a un archivo
     *@param sucursales es una Colection con las sucursales
     *@return String con el path del archivo
     *@throws IOException si ocurre algun error de IO
     */
    protected String exportar(java.util.Collection sucursales,HttpServletRequest req) throws IOException{
        /* Modificacion Rafael Martinez Montiel para eliminar la variable de instancia session
        */
        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        java.io.FileInputStream in = null;
        java.io.File Exportado = null;
        java.io.FileWriter writer = null;
        try{
            in = new java.io.FileInputStream ( Global.ARCHIVO_CONFIGURACION );
            java.util.Properties prop = new java.util.Properties ();
            prop.load (in);
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
            Exportado = new java.io.File(prop.getProperty ("DIRECTORIO_LOCAL") + "/" + session.getUserID8() + ".doc");
            boolean Creado = Exportado.createNewFile();
            if (!Creado) {
                Exportado.delete();
                Exportado.createNewFile();
            }
            writer = new java.io.FileWriter(Exportado);
            java.util.Iterator it = sucursales.iterator();
            while(it.hasNext()){
                pdSucursal sucursal = (pdSucursal) it.next();
                writer.write(sucursal.toFileExp() + "\n" );
            }
            writer.flush();
            writer.close();
            EIGlobal.mensajePorTrace  ( getClass ().getName () + " Exportado " + Exportado.getAbsolutePath (), EIGlobal.NivelLog.INFO);
            if( pdArchivoTemporal.envia( Exportado ))
                return Exportado.getName();
            else
                EIGlobal.mensajePorTrace  ( getClass ().getName () + " No se pudo enviar el Archivo al WebServer", EIGlobal.NivelLog.DEBUG);


        } catch (java.io.FileNotFoundException ex) {
            EIGlobal.mensajePorTrace (ex.getMessage (), EIGlobal.NivelLog.INFO);
            ex.printStackTrace ();
        }finally{
            if(null != in){
                try{
                    in.close();
                    in = null;
                }catch(java.io.IOException ex){
                }
            }
            if(null != writer){
                try{
                    writer.close();
                    writer = null;
                }catch(java.io.IOException ex){
                }
            }
            if(null != Exportado){
                Exportado = null;
            }
        }

        throw new IOException("No se pudo enviar el archivo");
    }

    /**
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException  */
    protected void consultar(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException{
        /* Modificacion Rafael Martinez Montiel para eliminar la variable de instancia session
        */
        BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        if(null == session ){
            EIGlobal.mensajePorTrace ("pdSucursales:consultar() session=" + session, EIGlobal.NivelLog.INFO);
            req.setAttribute("Error",IEnlace.MSG_PAG_NO_DISP);
            evalTemplate ("/jsp/EI_Error.jsp",req,res);
            return;
        }

        pdServicio servicio = new pdServicio();
        //servicio.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

        servicio.setContrato(session.getContractNumber());
        servicio.setUsuario(session.getUserID8());
		servicio.setPerfil(session.getUserProfile());

        java.util.ArrayList al = new java.util.ArrayList();
        String orden = req.getParameter("orden");

        java.util.Comparator comparator = new mx.altec.enlace.bo.pdSucursalCompPorCve();
        if(null != orden && !orden.equals("")){
            if(orden.equals(this.POR_CIUDAD)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorCiu();
            else if(orden.equals(this.POR_CP)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorCP();
            else if(orden.equals(this.POR_DELEGACION)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorDel();
            else if(orden.equals(this.POR_DOMICILIO)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorDom();
            else if(orden.equals(this.POR_EDO)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorEdo();
            else if(orden.equals(this.POR_NOMBRE)) comparator = new mx.altec.enlace.bo.pdSucursalCompPorNom();
            else comparator = new mx.altec.enlace.bo.pdSucursalCompPorCve();
        }
        java.util.TreeSet ts = new java.util.TreeSet(comparator);

        try{
            al = servicio.getSucursales();

        } catch (Exception e){
        }

        if(null == al ){
            req.setAttribute("Error","no se pudo obtener las sucursales");
            req.setAttribute("URL","pdSucursales");
            evalTemplate("/jsp/EI_Error.jsp",req,res);
            return;
        } else {

            ts.addAll(al);

            //Exportar archivo
            req.removeAttribute ("pdSucursalesFile");
            req.getSession ().removeAttribute ("pdSucursalesFile");
            if( session.getFacultad ( session.FAC_PADIREXPARC_PD ) ){
                try{
                    String fileName = this.exportar(ts,req);
                    req.getSession ().setAttribute("pdSucursalesFile",fileName);
                } catch(IOException e){
                    EIGlobal.mensajePorTrace("pdSucursales:consultar - " + e.getMessage(), EIGlobal.NivelLog.INFO);
                    req.getSession ().removeAttribute("pdSucursalesFile");
                }
            }

            req.setAttribute("start",new Integer(0));
        }

        req.getSession().setAttribute("pdSucursales", ts);
        //TODO BIT CU 4311, A5
        /*
		 * VSWF ARR -I
		 */
        if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
        try{
        BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_PAGO_DIR_CAT_SUCURSALES_OBT_LISTA_SUCURS);
		bt.setContrato(servicio.Contrato);
		bt.setServTransTux("OAOP");
		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch (SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
        }
		/*
		 * VSWF ARR -F
		 */
        evalTemplate("/jsp/pdSucursales.jsp",req,res);
    }

    public void desplazar(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        Integer start = new Integer(0);

        String Start = req.getParameter("start");

        if(null != Start && ! Start.equals("")){
            try{
                start = new Integer(Start);
            } catch (NumberFormatException e){
                EIGlobal.mensajePorTrace("pdSucursales:desplazar - NumberFormatException", EIGlobal.NivelLog.INFO);
            }
        }
        req.setAttribute("start",start);

        evalTemplate("/jsp/pdSucursales.jsp",req,res);
    }

}