/**
 *   Isban Mexico
 *   Clase: FielRegistro.java
 *   Descripcion: Servlet que gestiona el flujo de Registro fiel.
 *
 *   Control de Cambios:
 *   1.0 22/06/2016  FSW. Everis
 */
package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;
import mx.altec.enlace.utilerias.FielUtils;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilAdminFIEL;

/**
 * Servlet que gestiona el flujo de Registro fiel.
 *
 * @author FSW Everis.
 */
public class FielRegistro extends BaseServlet {

    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = 8680017998612528718L;
    /**
     * Titulo del encabezado de la pagina.
     */
    private static final String PAG_ENCAB_TITULO = "Registro de FIEL";
    /**
     * Ruta base del encabezado de la pagina.
     */
    private static final String PAG_ENCAB_RUTA = "Administraci&oacute;n y control &gt; FIEL &gt; Registro";
    /**
     * Cadena base de registro en Log.
     */
    private static final String LOG_MODULO = "FielRegistro - ";

    /**
     * Metodo que responde a peticiones GET
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que responde a peticiones POST
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que atiende peticiones de Registro FIEL
     *
     * @param req objeto inherente a la peticion
     * @param res objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        EIGlobal.mensajePorTrace("defaultAction - Inicia FielRegistro", EIGlobal.NivelLog.INFO);
        HttpSession ses = req.getSession();
        BaseResource attrSession = (BaseResource) ses.getAttribute("session");

        if (SesionValida(req, res)) {
			attrSession.setModuloConsultar(IEnlace.MDep_inter_cargo); // Limpiar modulo de busueda de cuentas
            String valida = FielUtils.validaRecToken(req, this);
            if (validaPeticion(req, res, attrSession, req.getSession(), valida)) {
                EIGlobal.mensajePorTrace("\n\nENTRO A VALIDAR LA PETICION\n\n",
                        EIGlobal.NivelLog.INFO);
            } else {
                String mod = getFormParameter(req, FielConstants.PARAM_MODULO);
                EIGlobal.mensajePorTrace("Modulo - " + mod, EIGlobal.NivelLog.INFO);
                if (mod == null || mod.isEmpty()) {
                    mod = FielConstants.INICIA_REGISTRO;
                }

                // Accion por modulo
                String pagResultado;
                switch (mod.charAt(0)) {
                    case '1': // Validacion de registro
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Validacion", EIGlobal.NivelLog.INFO);
                        pagResultado = registroValidacion(req, res, ses, attrSession, valida);
                        break;
                    case '2': // Confirmacion
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Confirmacion", EIGlobal.NivelLog.INFO);
                        FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO,
                                PAG_ENCAB_RUTA + " &gt; Confirmacion", FielConstants.AYUDA_FIEL_REG);
                        pagResultado = FielConstants.JSP_FIEL_RESULTADO;
                        break;
                    case '3': // Resultado
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Resultado", EIGlobal.NivelLog.INFO);
                        pagResultado = FielConstants.JSP_FIEL_REGISTRO_COMPROBANTE;
                        break;
                    default: // Inicia registro fiel
                        EIGlobal.mensajePorTrace(LOG_MODULO + "Inicio", EIGlobal.NivelLog.INFO);
                        FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO, PAG_ENCAB_RUTA, FielConstants.AYUDA_FIEL_REG);
                        pagResultado = FielConstants.JSP_FIEL_REGISTRO_INIT;
                        ses.removeAttribute(FielConstants.PARAM_DETALLE_FIEL);
                        req.getSession().removeAttribute(FielConstants.PARAM_FIEL_RESULT);
                        FielUtils.grabaPistas(req, attrSession, FielConstants.BITA_FIEL_ALTA_ENTRA,
                                null, null, null, false, null);
                        break;
                }
                EIGlobal.mensajePorTrace(LOG_MODULO + "pagResultado ***" + pagResultado + " MODAT- " + mod.charAt(0), EIGlobal.NivelLog.INFO);

                if (pagResultado != null) {
                    evalTemplate(pagResultado, req, res);
                }
            }
        }

        EIGlobal.mensajePorTrace("FielRegistro - Saliendo.", EIGlobal.NivelLog.INFO);
    }

    /**
     * Metodo que valida los datos de la FIEL
     *
     * @param req Objeto inherente a la solicitud
     * @param res Objeto inherente a la reapuesta
     * @param ses Objeto de sesion
     * @param attrSession Objeto de sesion
     * @param valida repuesta de Token
     * @return la pantalla a mostrar
     * @throws IOException Excepcion generica
     */
    private String registroValidacion(HttpServletRequest req, HttpServletResponse res,
            HttpSession ses, BaseResource attrSession, String valida) throws IOException {
        EIGlobal.mensajePorTrace("INICIA registroValidacion", EIGlobal.NivelLog.INFO);
        String paginaRetorno = FielConstants.JSP_FIEL_REGISTRO_INIT;

        String detalleCuenta = getFormParameter(req, "comboCuenta");
        String cuenta = null;
        if (detalleCuenta != null) {
            if (detalleCuenta.indexOf('|') >= 0) {
                cuenta = detalleCuenta.substring(0, detalleCuenta.indexOf('|'));
            } else {
                cuenta = detalleCuenta;
            }
        }
        // valida WS Banxico
        BeanAdminFiel detalleFiel = (BeanAdminFiel) ses.getAttribute(FielConstants.PARAM_DETALLE_FIEL);
        if (detalleFiel == null) {
            detalleFiel = new BeanAdminFiel();
            detalleFiel.setFiel(getFormParameter(req, "fiel"));
            detalleFiel.setCuenta(cuenta);
            detalleFiel = UtilAdminFIEL.buscaFiel(detalleFiel);
        }

        if (FielConstants.COD_ERRROR_OK.equals(detalleFiel.getCodError())) {
            paginaRetorno = guardarFiel(req, res, ses, attrSession, valida,
                    paginaRetorno, cuenta, detalleFiel);

        } else {
            req.setAttribute(FielConstants.PARAM_MODULO, FielConstants.INICIA_REGISTRO);
            req.setAttribute(FielConstants.ETIQUETA_MSG_ERROR, detalleFiel.getMsgError());
            FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO,
                    PAG_ENCAB_RUTA , FielConstants.AYUDA_FIEL_REG);

        }
        return paginaRetorno;
    }

    /**
     * Metodo que valida y guarda la fiel
     *
     * @param req Objeto inherente a la peticion
     * @param res Objeto inherente a la respuesta
     * @param ses Objeto inherente a la sesion
     * @param attrSession objeto de sesion
     * @param valida Token valido
     * @param paginaRetorno Pantalla de retorno
     * @param cuenta Cuenta
     * @param detalleFiel DTO con informacion de la fiel
     * @return paginaRetorno Pagina de retorno
     */
    private String guardarFiel(HttpServletRequest req, HttpServletResponse res,
            HttpSession ses, BaseResource attrSession, String valida,
            String paginaRetorno, String cuenta,
            BeanAdminFiel detalleFiel) {
        ses.setAttribute(FielConstants.PARAM_DETALLE_FIEL, detalleFiel);
        if (FielUtils.validaRSA(req, res, ses, this, "/enlaceMig/FielRegistro")) {
            paginaRetorno = null;
        } else {
            EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.INFO);
            if (valida == null) {
                boolean solVal = ValidaOTP.solicitaValidacion(attrSession.getContractNumber(),
                        IEnlace.FIEL_OTP_REGISTRO);
                if (solVal && attrSession.getValidarToken()
                        && attrSession.getFacultad(BaseResource.FAC_VAL_OTP)
                        && attrSession.getToken().getStatus() == 1) {
                    req.setAttribute(FielConstants.DATOS_FIEL, FielConstants.DATOS_FIEL);
                    paginaRetorno = FielUtils.validaToken(req, res, attrSession, this,
                            paginaRetorno, PAG_ENCAB_TITULO,
                            PAG_ENCAB_RUTA + " &gt; Confirmaci&oacute;n", FielConstants.AYUDA_FIEL_REG,
                            FielConstants.CONTEXTO_ENLACE + Global.WEB_APPLICATION + "/FielRegistro?modulo=0&"
                            + BitaConstants.FLUJO + "=" + FielConstants.BITA_FIEL_ALTA,
                            "&#191;Desea registrar la FIEL a la cuenta " + cuenta + "&#63;");
                } else {
                    EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: valida NO es null", EIGlobal.NivelLog.DEBUG);
                    ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado.");
                    valida = FielConstants.UNO_STR;
                }
            }
        }
        EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: continua Valida >" + valida, EIGlobal.NivelLog.INFO);
        if (valida != null && FielConstants.UNO_STR.equals(valida)) {
            int referencia = obten_referencia_operacion(
                    Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
            detalleFiel.setReferencia(String.valueOf(referencia));
            detalleFiel.setIndicadorOperacion("A");
            req.setAttribute(FielConstants.ETIQUETA_NOMBRE_MODULO_FIEL, "REGISTRO FIEL");
            paginaRetorno = FielConstants.JSP_FIEL_RESULTADO;
            detalleFiel = UtilAdminFIEL.administraFiel(detalleFiel);
            req.getSession().setAttribute(FielConstants.PARAM_FIEL_RESULT, detalleFiel);
            FielUtils.realizarOperaciones(req, attrSession,
                    referencia, cuenta,
                    FielConstants.BITA_FIEL_ALTA_EJEC,
                    FielConstants.BITA_FIEL_ALTA,
                    detalleFiel.getCodError(),
                    "Registro", Description(FielConstants.BITA_FIEL_ALTA));
        } else {
            EIGlobal.mensajePorTrace(LOG_MODULO + "NO RETOMA EL FLUJO", EIGlobal.NivelLog.INFO);
        }

        if (paginaRetorno != null) {
            FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO,
                    PAG_ENCAB_RUTA + " &gt; Resultado", FielConstants.AYUDA_FIEL_REG);
        }
        return paginaRetorno;
    }

}
