package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.RET_ConsultaOperBO;
import mx.altec.enlace.utilerias.EIGlobal;


/**
 *  Clase Servlet para la consulta de operaciones Reuters
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Apr 28, 2012
 */
public class RET_ConsultaOper extends BaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2520607286426728727L;

	/**
	 * Objeto manejador RET_ConsultaOperBO
	 */
	RET_ConsultaOperBO manejador = new RET_ConsultaOperBO();

    /**
     * Constante accion iniciar
     */
    private static final String ACCION_INICIAR = "iniciar";

    /**
     * Constante accion consultar
     */
    private static final String ACCION_CONSULTAR = "consultar";

    /**
     * Constante accion siguiente
     */
    private static final String ACCION_SIGUIENTE = "siguiente";

    /**
     * Constante accion anterior
     */
    private static final String ACCION_ANTERIOR = "anterior";

    /**
     * Constante accion siguiente
     */
    private static final String ACCION_ULTIMO = "ultimo";

    /**
     * Constante accion anterior
     */
    private static final String ACCION_PRIMERO = "primero";

    /**
     * Constante accion ver detalle
     */
    private static final String ACCION_VER_DETALLE = "verDetalle";

	/**
	 * DoGet
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * DoPost
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		processRequest( req, res );
	 }

	/**
	 * Metodo principal para el manejo del alta de beneficiarios de pago
	 * ocurre
	 * @param req					request de la operaci�n
	 * @param res					response de la operaci�n
	 * @throws ServletException		Exception de tipo ServletException
	 * @throws IOException			Exception de tipo IOException
	 */
	public void processRequest( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException{

		EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Entrando a la Consulta de Operaciones Reuters.", EIGlobal.NivelLog.DEBUG);
		String opcion = null;
		String detalle = null;
		opcion = (String) getFormParameter(req,"opcion");
		if (opcion == null) {
			opcion = ACCION_INICIAR;
			EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Opcion null, se asigna inicio" , EIGlobal.NivelLog.ERROR);
		}
		EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Opcion: " + opcion, EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if(SesionValida(req,res)){
			//*****************************************Inicia validacion OTP****************
			String valida = getFormParameter(req,"valida" );
			if(validaPeticion( req, res,session,sess,valida)){
				EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Entro a validar peticion", EIGlobal.NivelLog.INFO);
			}else{//*****************************************Termina validacion OTP**************************************
				EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Leyendo facultades.", EIGlobal.NivelLog.INFO);

				if(!verificaFacultad("COMPFXREUTER",req) &&
				   !verificaFacultad("LIBPFXREUTER",req)){
					manejador.paginaError("<p align=\"left\">Por el momento no cuenta con Facultades para operar <font color=green>FX Online</font>,<br>"
							+ ",le pedimos se ponga en contacto con su Ejecutivo de cuenta,<br>" 
							+ "su Ejecutivo Cash &oacute; con su Asesor de Inversi&oacute;n," 
							+ "<br>quienes lo podr�n apoyar para contratar <font color=green>FX Online</font>.</p>", req, res);
				}else{

					/**
					 * Opcion Iniciar
					 */
					if(opcion.equals(ACCION_INICIAR)){
					   	try {
							manejador.iniciar(req, res);
					        EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando iniciar.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_consultaOper.jsp", req, res);
						} catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Consultar
					 */
					if(opcion.equals(ACCION_CONSULTAR)){
						try{
							manejador.consultar( req, res);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando consultar.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_consultaOperRes.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Siguiente
					 */
					if(opcion.equals(ACCION_SIGUIENTE)){
						try{
							detalle = (String)getFormParameter(req,"detalle");
							manejador.siguiente(req, res, detalle);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando siguiente.", EIGlobal.NivelLog.INFO);
							if(detalle.equals("N"))
								evalTemplate("/jsp/RET_consultaOperRes.jsp", req, res);
							else if(detalle.equals("S"))
								evalTemplate("/jsp/RET_consultaOperDet.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Anterior
					 */
					if(opcion.equals(ACCION_ANTERIOR)){
						try{
							detalle = (String)getFormParameter(req,"detalle");
							manejador.anterior(req, res, detalle);
							if(detalle.equals("N"))
								evalTemplate("/jsp/RET_consultaOperRes.jsp", req, res);
							else if(detalle.equals("S"))
								evalTemplate("/jsp/RET_consultaOperDet.jsp", req, res);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando anterior.", EIGlobal.NivelLog.INFO);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Primero
					 */
					if(opcion.equals(ACCION_PRIMERO)){
						try{
							detalle = (String)getFormParameter(req,"detalle");
							manejador.primero(req, res, detalle);
							if(detalle.equals("N"))
								evalTemplate("/jsp/RET_consultaOperRes.jsp", req, res);
							else if(detalle.equals("S"))
								evalTemplate("/jsp/RET_consultaOperDet.jsp", req, res);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando primero.", EIGlobal.NivelLog.INFO);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Ultimo
					 */
					if(opcion.equals(ACCION_ULTIMO)){
						try{
							detalle = (String)getFormParameter(req,"detalle");
							manejador.ultimo(req, res, detalle);
							if(detalle.equals("N"))
								evalTemplate("/jsp/RET_consultaOperRes.jsp", req, res);
							else if(detalle.equals("S"))
								evalTemplate("/jsp/RET_consultaOperDet.jsp", req, res);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando ultimo.", EIGlobal.NivelLog.INFO);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}

					/**
					 * Opcion Ver Detalle
					 */
					if(opcion.equals(ACCION_VER_DETALLE)){
						try{
							manejador.verDetalle( req, res);
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Finalizando ver detalle.", EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/RET_consultaOperDet.jsp", req, res);
						}catch (Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
													+ e.getMessage()
						               				+ "<DATOS GENERALES>"
										 			+ "Linea encontrada->" + lineaError[0]
										 			, EIGlobal.NivelLog.INFO);
							manejador.paginaError("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde.", req, res);
						}
					}
				}
			}
		EIGlobal.mensajePorTrace("RET_ConsultaOper::processRequest:: Saliendo de la Consulta de Operaciones Reuters.", EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * Metodo encargado de guardar los parametros en la session
	 *
	 * @param request			Petici�n de entrada
	 */
	public void guardaParametrosEnSession(HttpServletRequest request){
		EIGlobal.mensajePorTrace("RET_ConsultaOper::guardaParametrosEnSession::Dentro de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("RET_ConsultaOper::guardaParametrosEnSession:: opcion-> " + getFormParameter(request,"opcion"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_ConsultaOper::guardaParametrosEnSession:: benefSel-> " + getFormParameter(request,"benefSel"), EIGlobal.NivelLog.INFO);

			String value;

			value = (String) getFormParameter(request,"opcion");
			if(value != null && !value.equals(null)) {
				tmp.put("opcion",getFormParameter(request,"opcion"));
			}

			value = (String) getFormParameter(request,"benefSel");
			if(value != null && !value.equals(null)) {
            	tmp.put("benefSel",getFormParameter(request,"benefSel"));
			}

			value = (String) getFormParameter(request,"nombreArchivoImp");
			if(value != null && !value.equals(null)) {
            	tmp.put("nombreArchivoImp",getFormParameter(request,"nombreArchivoImp"));
			}

			value = (String) getFormParameter(request,"totalRegistros");
			if(value != null && !value.equals(null)) {
            	tmp.put("totalRegistros",getFormParameter(request,"totalRegistros"));
			}

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
        EIGlobal.mensajePorTrace("RET_ConsultaOper::guardaParametrosEnSession:: Saliendo de guardaParametrosEnSession ", EIGlobal.NivelLog.INFO);
    }


 }