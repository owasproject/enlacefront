package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.bo.ConsultaCtasAsociadasBO;
import mx.altec.enlace.bo.DescargaEstadoCuentaBO;
import mx.altec.enlace.cliente.ws.cfdiondemand.PeriodCFDI;
import mx.altec.enlace.cliente.ws.cfdiondemand.SOAPExceptionImpl;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
/**
 * Servlet implementation class DescargarEdoCtaXMLServlet
 */
/**
 * @author C109836
 *
 */
public class DescargaEdoCtaXMLServlet extends BaseServlet {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
    /** The Constant STR_ESTADOS_CUENTA. */
    private static final String STR_ESTADOS_CUENTA = "Estados de Cuenta";
    /** The Constant STR_ESTADOS_CUENTA_DESCARGA_MENU. */
	private static final String STR_ESTADOS_CUENTA_DESCARGA_MENU = "Estados de Cuenta &gt; Descarga de Estados de Cuenta &gt; XML";
	/** The Constant STR_MSG_SINFACULTADES **/
	private static final String STR_MSG_SINFACULTADES =  "<br>Usuario sin facultades para operar con este servicio<br>";
	/** The Constant STR_MSG_SERVICIO_NO_DISPONIBLE **/
	private static final String STR_MSG_SERVICIO_NO_DISPONIBLE =  "El servicio CFDI no se encuentra disponible";
	/** The Constant STR_MSG_ERROR_LLAMADO_WS **/
	private static final String STR_MSG_ERROR_LLAMADO_WS = "Hubo un problema al recuperar los periodos disponibles, Intente Nuevamente</br>";
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_DescargaXML";
	/** The Constant SESSION_NAME. */
	private static final String SESSION_NAME = "session";
	/** The Constant VENTANA_INICIAL. */
	private static final int VENTANA_INICIAL = 0;
	/** The Constant VENTANA_DESCARGA. */
	private static final int VENTANA_CONSULTA_WS = 1;
	/** The Constant VENTANA_CUENTAS. */
	private static final int VENTANA_DESCARGA_EDOCTA = 2;
	/** The Constant VENTANA_ERROR_DESCARGA. */
	private static final int VENTANA_ERROR_DESCARGA = 3;
	/** The Constant URL_SERVLET. */
	private static final String URL_SERVLET = "DescargaEdoCtaXMLServlet";
	/** Constante para log **/
	private static final String LOG_TAG ="[EDCPDFXML:::DescargaEdoCtaXMLServlet]:";

	/** The Constant REQ_PARAM_TEXTCUENTAS Atributo del request. */
	private static final String REQ_PARAM_TEXTCUENTAS = "textCuentas";
	/** The Constant REQ_PARAM_CUENTASELEC Atributo del request. */
	private static final String REQ_PARAM_CUENTASELEC = "cuentaSeleccionada";
	/** The Constant REQ_PARAM_URLEdoCtaXML Atributo del request. */
	private static final String REQ_PARAM_URLEDOCTAXML = "URLEdoCtaXML";
	/** The Constant REQ_PARAM_TIPOXML Atributo del request. */
	private static final String REQ_PARAM_TIPOXML = "tipoXML";
	/** The Constant PARAM_XML Atributo del request. */
	private static final String REQ_PARAM_XML = "XML";
	//"ECTA0000"
	/** The Constant PARAM_XML Atributo del request. */
	private static final String REQ_ESTADO_CUENTA = "ECTA0000";
	/** Constante para WS de CFDI Tipo Cuenta **/
	private static final String WS_TIPO_CUENTA_CHEQUES = "001";
	/** Constante para WS de CFDI Tipo Cuenta TDC**/
	private static final String WS_TIPO_CUENTA_TDC = "003";
	/** Constante para WS de CFDI Numero de Peridos **/
	private static final int WS_NUM_PERIODOS = 19;
	/** Constante para la literal opcion **/
	private static final String OPCION = "opcion";
	/** Constante para la literal problemaWS **/
	private static final String PROBLEMAWS = "problemaWS";

	/** BusinessObject descargaEstadoCuentaBO **/
	private final transient DescargaEstadoCuentaBO descargaEstadoCuentaBO = new DescargaEstadoCuentaBO();

	/** Bean para bitacora **/
	private final transient BitaAdminBean bean = new BitaAdminBean();

	/**
	 * Metodo que procesa las peticiones de tipo GET.
	 *
	 * @param	request		El HttpServletRequest donde envian los parametros al servlet.
	 * @param	response	El HttpServletResponse para devolver la salida del servlet.
	 * @throws	ServletException	Excepcion arrogada del servlet.
	 * @throws	IOException			Excepcion de Entrada o Salida.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		cargaEncabezados(request);
		descargaEdoCtaXML(request,response);
	}

	/**
	 * Metodo que procesa las peticiones de tipo POST.
	 *
	 * @param	request		El HttpServletRequest donde envian los parametros al servlet.
	 * @param	response	El HttpServletResponse para devolver la salida del servlet.
	 * @throws	ServletException	Excepcion del Servlet.
	 * @throws	IOException			Excepcion de Entrada o Salida.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 		throws ServletException, IOException {
		cargaEncabezados(request);
		descargaEdoCtaXML(request,response);
	}

	/**
	 * Metodo inicial del flujo de descarga de Estados de cuenta XML.
	 *
	 * @param	request		El HttpServletRequest donde envian los parametros al servlet.
	 * @param	response	El HttpServletResponse para devolver la salida del servlet.
	 * @throws	ServletException	Excepcion del Servlet.
	 * @throws	IOException			Excepcion de Entrada o Salida.
	 */
	protected void descargaEdoCtaXML(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EIGlobal.mensajePorTrace(String.format("%s <---Inicia Flujo de Descarga de Estado de Cuenta XML Yes",LOG_TAG)
				,NivelLog.INFO);
		BaseResource session=(BaseResource)request.getSession().getAttribute(SESSION_NAME);
		Integer ventana = (request.getParameter("ventana")!=null) ?
				Integer.valueOf(request.getParameter("ventana").toString()) : VENTANA_INICIAL;
		EIGlobal.mensajePorTrace(String.format("%s Ventana -> %s",LOG_TAG,ventana)
				,NivelLog.INFO);

		if (SesionValida(request, response)) {
			if ( session.getFacultad(BaseResource.FAC_EDO_CTA_DESCARGA) ) {
				switch (ventana) {
					case VENTANA_INICIAL :
						iniciaConsulta(request,response);
						break;
					case VENTANA_CONSULTA_WS :
						consultaPeriodoWS(request,response);
						break;
					case VENTANA_DESCARGA_EDOCTA :
						iniciaDescarga(request,response);
						break;
					case VENTANA_ERROR_DESCARGA :
						break;
					default :
						despliegaPaginaErrorURL(STR_MSG_SINFACULTADES,
								STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
						break;
				}
			}else {
				despliegaPaginaErrorURL(STR_MSG_SINFACULTADES,
						STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA, null, request, response);
			}
		} else {
			despliegaPaginaErrorURL("<br>Session no Valida<br>",
					STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
		}

	}

	/**
	 * Metodo iniciaConsulta, metodo inicial que envia la pantalla principal del flujo de descarga.
	 *
	 * @param	request		El HttpServletRequest que contiene los parametros enviados al Servlet.
	 * @param	response	El HttpServletResponse al que se envia la salida del Servlet.
	 * @throws	ServletException	Excepcion del Servlet.
	 * @throws	IOException			Excepcion de Entrada o Salida.
	 */
	private void iniciaConsulta(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s Metodo --->%s",LOG_TAG,"iniciaConsulta")
				,NivelLog.INFO);
		HttpSession httpSession = request.getSession();
		if ( request.getParameter("filtro")==null ) {
			/** PISTA_AUDITORA [DEDC-03] Entra a pantalla de descarga de Estados de Cuenta - XML **/
			EIGlobal.mensajePorTrace(String.format("%s Bitacoriza [DEDC-03] Entra a pantalla de descarga de Estados de Cuenta - XML.", LOG_TAG)
					, NivelLog.DEBUG);
	    	bean.setTipoOp(" ");
	    	bean.setIdTabla(" ");
	    	bean.setServTransTux("DY05-PeriodsCFDI");
	    	bean.setReferencia(100000);
			BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC03, bean, false);
			EIGlobal.mensajePorTrace(String.format("%s Bitacorizo",LOG_TAG),NivelLog.DEBUG);
		}
		/***************************** Carga Cuentas ***************************/
		request.setAttribute("ventana","0");
		request.setAttribute(OPCION,"");
		ctasTarjetas(request, response);


		EIGlobal.mensajePorTrace(String.format("%s evalTemplate -> /jsp/descargaEdoCtaXML.jsp",LOG_TAG),NivelLog.DEBUG);
		evalTemplate("/jsp/descargaEdoCtaXML.jsp", request, response );
	}

	/**
	 * Metodo para consultar via WebService de CFDIOndemand los periodos para los cuales esta dispoinble el
	 * estado de Cuenta en XML y por cada uno que encuentre, obtener el FolioUUID, para la descarga del mismo
	 * desde ondemand.
	 *
	 * @param	request		El HttpServletRequest que contiene los parametros enviados al servlet.
	 * @param	response	El HttpServletResponse para enviar la salida del servlet.
	 * @throws	ServletException	Excepcion del Servlet.
	 * @throws	IOException			Excepcion de Entrada o Salida.
	 */
	private void consultaPeriodoWS(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		String cuentaSeleccionada="";
		String tipoXML=(request.getParameter(REQ_PARAM_TIPOXML)!=null) ? request.getParameter(REQ_PARAM_TIPOXML) : "";
		CuentasDescargaBean cuentasDescargaBean=new CuentasDescargaBean();
		EIGlobal.mensajePorTrace(String.format("%s ::::: Inicia consultaPeriodoWS :::::",LOG_TAG)
				,NivelLog.INFO);

		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource)httpSession.getAttribute(SESSION_NAME);

		request.removeAttribute(PROBLEMAWS);
		cuentaSeleccionada=(request.getParameter(REQ_PARAM_TEXTCUENTAS)!=null) ? ((String)request.getParameter(REQ_PARAM_TEXTCUENTAS)).split(" ")[0] : "";
		cuentasDescargaBean.setTipoXML(tipoXML);
		session.setModuloConsultar(IEnlace.MConsulta_Saldos);
		EIGlobal.mensajePorTrace(String.format("%s Se carga la cuenta [%s]", LOG_TAG,cuentaSeleccionada), NivelLog.DEBUG);



		cuentasDescargaBean.setCuentaBita(cuentaSeleccionada);
		cuentasDescargaBean.setNumPeridos(WS_NUM_PERIODOS);
		if (cuentaSeleccionada.startsWith("BME") && cuentaSeleccionada.length()==14) {
			cuentasDescargaBean.setSerie(cuentaSeleccionada.substring(0,3));
			cuentasDescargaBean.setCuenta(cuentaSeleccionada.substring(3,cuentaSeleccionada.length()) );
			cuentasDescargaBean.setTipoEdoCta(WS_TIPO_CUENTA_CHEQUES);
			EIGlobal.mensajePorTrace(String.format("%s Serie BME --> Cargado Cuenta 11d [%s] Serie 3d [%s].",LOG_TAG,
					cuentasDescargaBean.getCuenta(),cuentasDescargaBean.getSerie()), NivelLog.DEBUG);
		}else if (cuentaSeleccionada.length()==11) {
			cuentasDescargaBean.setCuenta(cuentaSeleccionada);
			cuentasDescargaBean.setTipoEdoCta(WS_TIPO_CUENTA_CHEQUES);
			EIGlobal.mensajePorTrace(String.format( "%s Cargado Cuenta 11d[%s]/tipoEDC[%s]",LOG_TAG,
					cuentasDescargaBean.getCuenta(),cuentasDescargaBean.getTipoEdoCta() ), NivelLog.DEBUG);
		}else {
			cuentasDescargaBean.setCuenta(cuentaSeleccionada);
			cuentasDescargaBean.setTipoEdoCta(WS_TIPO_CUENTA_TDC);
			EIGlobal.mensajePorTrace(String.format( "%s Cargado Cuenta 16d[%s]/tipoEDC[%s]",LOG_TAG,
					cuentasDescargaBean.getCuenta(),cuentasDescargaBean.getTipoEdoCta() ), NivelLog.DEBUG);
		}

		boolean respondioWS=false;
		try {
			EIGlobal.mensajePorTrace(String.format("%s Llamado a WS para la cuenta [%s] el bean [%s]"
					,LOG_TAG,cuentasDescargaBean.getCuenta(),cuentasDescargaBean), NivelLog.INFO);
			cuentasDescargaBean=descargaEstadoCuentaBO.invocaCFDIPeriodsWS(cuentasDescargaBean);
			respondioWS=true;
		}catch (SOAPExceptionImpl se) {
			respondioWS=false;
			request.setAttribute(PROBLEMAWS, STR_MSG_ERROR_LLAMADO_WS);
			EIGlobal.mensajePorTrace(String.format("%sError en llamada a WebService: %s::%s",LOG_TAG,se.getMessage(),se.getLocalizedMessage()),
					NivelLog.ERROR);
		} catch (BusinessException e) {
			respondioWS=false;
			request.setAttribute(PROBLEMAWS, STR_MSG_ERROR_LLAMADO_WS+STR_MSG_SERVICIO_NO_DISPONIBLE);
			EIGlobal.mensajePorTrace(String.format(STR_MSG_ERROR_LLAMADO_WS+STR_MSG_SERVICIO_NO_DISPONIBLE,
					Global.periodsCFDIServiceWsdlLocation), NivelLog.ERROR);
		}
		List<String> listaPA=descargaEstadoCuentaBO.obtenPeriodosAnteriores();
		List<String> listaPF=new ArrayList<String>();
		if (respondioWS) {
			if (cuentasDescargaBean.getCodigoRespuesta()==100) {
				String periodoFolio="";
				for (String periodoAnt : listaPA) {
					EIGlobal.mensajePorTrace(String.format("%s Validando yy periodo[%s]",LOG_TAG,periodoAnt),NivelLog.DEBUG);
					boolean compara=true;
					int i=0;
					while (compara && cuentasDescargaBean.getPeriodCFDI().size()>i ){
						EIGlobal.mensajePorTrace(String.format("Match->periodoCFDI[%s] >> En el Ciclo.",LOG_TAG,i),NivelLog.DEBUG);
						PeriodCFDI periodCFDI=cuentasDescargaBean.getPeriodCFDI().get(i);
						if ( periodCFDI.getPeriodo().equalsIgnoreCase(periodoAnt.split(":::")[0]) ){
							EIGlobal.mensajePorTrace(String.format("%s Match->periodoCFDI[%s] periodoAnt[%s] >> Rompiendo el Ciclo.",LOG_TAG,periodCFDI.getPeriodo(),periodoAnt),
									NivelLog.DEBUG);
							periodoFolio=String.format("%s@%s@%s:::%s",periodCFDI.getFolioUUID(),periodoAnt.split(":::")[0], periodCFDI.getSerie() + ".", periodoAnt.split(":::")[1]);
							EIGlobal.mensajePorTrace(String.format("%s Cadena armada para combo [%s]", LOG_TAG, periodoFolio), NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(String.format("%s Serie [%s]", LOG_TAG, periodoFolio), NivelLog.DEBUG);
							compara=false;
						}else {

							periodoFolio=String.format("%s",periodoAnt);
							EIGlobal.mensajePorTrace(String.format("Match->periodoFolio[%s] >>",LOG_TAG,periodoFolio),NivelLog.DEBUG);
							i++;
						}
					}
					EIGlobal.mensajePorTrace(String.format("%s Se agrega a la lista %s",LOG_TAG,periodoFolio),
							NivelLog.DEBUG);
					listaPF.add(periodoFolio);
				}
			}else {
				String wsErrorMsg="";
				switch (cuentasDescargaBean.getCodigoRespuesta()){
					case 201 : wsErrorMsg=STR_MSG_ERROR_LLAMADO_WS+"Par&aacute;metros Nulos o Vacios.";
							break;
					case 202 : wsErrorMsg=STR_MSG_ERROR_LLAMADO_WS+"Par&aacute;metros Con Formato Incorrecto.";
							break;
					case 301 : wsErrorMsg="No Se Encontraron Resultados.";
							break;
					case 401 : wsErrorMsg=STR_MSG_ERROR_LLAMADO_WS+"Error en la Base de Datos.";
							break;
					case 501 : wsErrorMsg=STR_MSG_ERROR_LLAMADO_WS+"Error General.";
							break;
					default : wsErrorMsg=STR_MSG_ERROR_LLAMADO_WS+"Error No Identificado.";
							break;
				}
				listaPF=listaPA;
				if (cuentasDescargaBean.getCodigoRespuesta()!=301) {
					request.setAttribute(PROBLEMAWS, wsErrorMsg);
				}
				EIGlobal.mensajePorTrace(wsErrorMsg, NivelLog.ERROR);
			}
		}else {
			listaPF=listaPA;
		}
		cuentasDescargaBean.setPeriodosAnteriores(listaPF);
		request.setAttribute(REQ_PARAM_CUENTASELEC, request.getParameter(REQ_PARAM_TEXTCUENTAS));
		request.setAttribute("periodosRecientes", cuentasDescargaBean.getPeriodosAnteriores());
		request.setAttribute(REQ_PARAM_TIPOXML, request.getParameter(REQ_PARAM_TIPOXML));
		httpSession.setAttribute("cuentasDescargaBean", cuentasDescargaBean);
		EIGlobal.mensajePorTrace(String.format("%sSaliendo del metodo consultaPeriodoWS - cuentaSeleccionada [%s]",
				LOG_TAG,request.getAttribute(REQ_PARAM_CUENTASELEC)), NivelLog.INFO);
		evalTemplate("/jsp/descargaEdoCtaXML.jsp", request, response );
	}

	/**
	 * Metodo para descargar el estado de cuenta.
	 *
	 * @param request	El HttpServletRequest que contiene los parametros enviados al servlet.
	 * @param response	El HttpServletResponse para enviar la salida del servlet.
	 * @throws ServletException	Excepcion del Servlet.
	 * @throws IOException		Excepcion de Entrada o Salida.
	 */
	protected void iniciaDescarga(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {

		EIGlobal.mensajePorTrace(String.format("%s ::::: Inicia descarga :::::",LOG_TAG),NivelLog.DEBUG);

		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource)httpSession.getAttribute(SESSION_NAME);
		String tipoXML=(request.getParameter(REQ_PARAM_TIPOXML)!=null) ? request.getParameter(REQ_PARAM_TIPOXML) : "";
		String cuentaSeleccionada=(request.getParameter(REQ_PARAM_TEXTCUENTAS)!=null) ? ((String)request.getParameter(REQ_PARAM_TEXTCUENTAS)).split(" ")[0] : "";
		/** PISTA_AUDITORA [DEDC-04] Selecciona Cuenta y Periodo para Descarga - XML */
    	bean.setTipoOp(" ");
    	bean.setIdTabla(cuentaSeleccionada);
    	bean.setServTransTux(" ");
    	bean.setReferencia(100000);
		BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC04, bean, false);


		String valida = request.getParameter("valida");
		if (valida == null) {
			valida = validaToken(request, response, "DescargaEdoCtaXMLServlet");
		}

		if (valida != null && "1".equals(valida)) {
			CuentasDescargaBean cuentasDescargaBean=(CuentasDescargaBean)httpSession.getAttribute("cuentasDescargaBean");
			DescargaEstadoCuentaBO descargaEstadoCuentaBO=new DescargaEstadoCuentaBO();

			/** PISTA_AUDITORA [CDEC-01 o CDEC-02] Confirma Descarga de Estados de Cuenta **/
			EIGlobal.mensajePorTrace(String.format("%s [CDEC-01 o CDEC-02] Confirma Descarga de Estados de Cuenta - XML",LOG_TAG)
					, NivelLog.DEBUG);
	    	bean.setTipoOp(" ");
	    	bean.setServTransTux(" ");
	    	bean.setIdTabla(" ");
	    	bean.setReferencia(100000);

	    	if ("I".equals(tipoXML.trim())){
	    	   	BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.DESC_EDOCTA_CDEC01, bean, true);
	    	}
	    	if ("E".equals(tipoXML.trim())){
	    	   	BitacoraBO.bitacorizaAdmin(request, httpSession, EdoCtaConstantes.DESC_EDOCTA_CDEC02, bean, true);
	    	}

			/** BITACORA_OPERACION [CDEC-01 o CDEC-02] Confirma Descarga de Estados de Cuenta **/
			EIGlobal.mensajePorTrace(
					String.format("%s BITACORA DE OPERACION ::: [CDEC-01 o CDEC-02] Confirma Descarga de Estados de Cuenta - XML",LOG_TAG),
					NivelLog.DEBUG);
			if ("I".equals(tipoXML.trim())){
			BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.CONF_DESC_EDOCTA_CDEC,
					obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuentaBita(), REQ_PARAM_XML, REQ_ESTADO_CUENTA, EdoCtaConstantes.TOKEN_VALIDO_DESCARGA_XML_INGRESO);
			}
			if ("E".equals(tipoXML.trim())){

				BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.CONF_DESC_EDOCTA_CDEC,
						obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuentaBita(), REQ_PARAM_XML, REQ_ESTADO_CUENTA, EdoCtaConstantes.TOKEN_VALIDO_DESCARGA_XML_EGRESO);
			}
			String periodoSeleccionado= (request.getParameter("slcPeriodos")!=null) ? request.getParameter("slcPeriodos") : "";
			String folioSAT="";
			String url="";
			EIGlobal.mensajePorTrace(String.format("%s periodoSeleccionado [%s]", LOG_TAG,periodoSeleccionado), NivelLog.DEBUG);
			if (periodoSeleccionado.split("@").length == 3) {
				folioSAT=periodoSeleccionado.split("@")[0];
				EIGlobal.mensajePorTrace(String.format("%s folioSAT [%s]", LOG_TAG,folioSAT), NivelLog.DEBUG);

				EIGlobal.mensajePorTrace(String.format("%s Generando llave Decarga",LOG_TAG),NivelLog.DEBUG);
				String key=descargaEstadoCuentaBO.genKey();
				EIGlobal.mensajePorTrace(String.format("%s Llave de Descarga [%s]",LOG_TAG,key), NivelLog.DEBUG);

				//Carga Datos para Insertar Descarga.
				cuentasDescargaBean.setLlaveDescarga(key);
				cuentasDescargaBean.setFolioSeleccionado(folioSAT);
				cuentasDescargaBean.setPeriodoSeleccionado(periodoSeleccionado.split("@")[1]);
				cuentasDescargaBean.setSerie(periodoSeleccionado.split("@")[2]);
				cuentasDescargaBean.setFormato(2);
				cuentasDescargaBean.setTipoXML(tipoXML);

				try {
	                               //POST ----------------------
					String cuenta = request.getParameter(REQ_PARAM_TEXTCUENTAS);
					String cuentaEnPeticion = (cuenta!=null) ? cuenta.trim().split(" ")[0] : "";

					// POST ------------------------

						EIGlobal.mensajePorTrace("Cuenta en peticion cuentaEnPeticion-->"+cuentaEnPeticion+"<--",NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Cuenta en peticion cuentaBean-->"+cuentasDescargaBean.getCuenta()+"<--",NivelLog.DEBUG);

						EIGlobal.mensajePorTrace(String.format("%s Tipo XML [%s]", LOG_TAG,tipoXML), NivelLog.DEBUG);
	                                       //POST ---------------------
						if(!(cuentaEnPeticion.trim()).endsWith(cuentasDescargaBean.getCuenta().trim())){
							despliegaPaginaErrorURL("<br>Operación no autorizada. <br>",
									STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
							return;
						}else{
					if (descargaEstadoCuentaBO.insertaRegistroDescarga(cuentasDescargaBean)) {
						String urlDescargaEdoCta = "../../../EdoEnlace/edoCtaEnlace/descargaEdoCta.do";
						url = String.format("%s?cadena=%s", urlDescargaEdoCta, key);
						request.setAttribute(REQ_PARAM_URLEDOCTAXML, url);
						EIGlobal.mensajePorTrace(String.format("urlDescargaEdoCta [XML]:  [%s]", request.getAttribute(REQ_PARAM_URLEDOCTAXML)), NivelLog.INFO);
						EIGlobal.mensajePorTrace(String.format("urlDescargaEdoCta [XML]:",request.getAttribute(REQ_PARAM_URLEDOCTAXML)),
								NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(String.format("%sEnviando URL para descarga \n%s",LOG_TAG,request.getAttribute(REQ_PARAM_URLEDOCTAXML)),
								NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(String.format("%s Cargando valores en Request.", LOG_TAG), NivelLog.DEBUG);
						session.setModuloConsultar(IEnlace.MConsulta_Saldos);
						request.setAttribute("periodosRecientes", (cuentasDescargaBean!=null) ? cuentasDescargaBean.getPeriodosAnteriores() : descargaEstadoCuentaBO.obtenPeriodosAnteriores() );
						request.setAttribute(REQ_PARAM_CUENTASELEC, request.getParameter(REQ_PARAM_TEXTCUENTAS));
						request.setAttribute("periodoSeleccionado",periodoSeleccionado);
						request.setAttribute(REQ_PARAM_TIPOXML, request.getParameter(REQ_PARAM_TIPOXML));
						EIGlobal.mensajePorTrace(String.format("%sAtributos Cargados \n periodosRecientes %s\n cuentaSeleccionada %s\n" +
								"periodoSeleccionado %s\n tipoXML %s\n",LOG_TAG,request.getAttribute(""),request.getAttribute(""),request.getAttribute(""),
								request.getAttribute(""))
								,NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(String.format("%s Regresando flujo a --> descargaEdoCtaXML.jsp.", LOG_TAG), NivelLog.DEBUG);
						evalTemplate("/jsp/descargaEdoCtaXML.jsp", request, response );

					}else {
						despliegaPaginaErrorURL("<br>Error al Crear Registro en Tabla para Descarga <br>",
								STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
					}
					}

				} catch (BusinessException e) {
					EIGlobal.mensajePorTrace("Ocurrio un error en : " + e, NivelLog.INFO);
					despliegaPaginaErrorURL("<br>Error "+e+" <br>",
							STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
				} finally {
					/** PISTA_AUDITORA [DEDC-05 o DEDC-06] Inicia proceso de Descarga de Estado de Cuenta - XML**/
					EIGlobal.mensajePorTrace(String.format("%s [DEDC-05 o DEDC-06] Inicia proceso de Descarga de Estado de Cuenta - XML",LOG_TAG)
							, NivelLog.DEBUG);
			    	bean.setCampo(cuentaSeleccionada);
			    	bean.setServTransTux(" ");
			    	bean.setTipoOp("S");
			    	bean.setIdTabla(" ");
			    	bean.setReferencia(100000);
			    	if ("I".equals(tipoXML.trim())){
			    		BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC05, bean, false);
			    	}

			    	if ("E".equals(tipoXML.trim())){
			    		BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC06, bean, false);
			    	}
					/** BITACORA_OPERACION [DEDC-05] Inicia proceso de Descarga de Estado de Cuenta **/
					EIGlobal.mensajePorTrace(
							String.format("%s BITACORA DE OPERACION ::: [CDEC-05] Descarga de Estados de Cuenta - XML",LOG_TAG),
							NivelLog.DEBUG);
					if ("I".equals(tipoXML.trim())){
						BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC,
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuentaBita(), REQ_PARAM_XML, REQ_ESTADO_CUENTA, EdoCtaConstantes.CONCEPTO_DESCARGA_XML_INGRESO);
					}
					if ("E".equals(tipoXML.trim())){
						BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC,
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuentaBita(), REQ_PARAM_XML, REQ_ESTADO_CUENTA, EdoCtaConstantes.CONCEPTO_DESCARGA_XML_EGRESO);
					}
				}
			}else {
				despliegaPaginaErrorURL("<br>Error en los parametros del periodo - No contiene folio de descarga<br>",
						STR_ESTADOS_CUENTA,	STR_ESTADOS_CUENTA_DESCARGA_MENU, ARCHIVO_AYUDA,URL_SERVLET + "?ventana=0", request, response);
			}
		}
	}

	/**
	 * Metodo encargado de realizar las operaciones de validacion de token.
	 *
	 * @param request	Contiene la informacion de la peticion al servlet.
	 * @param response	Contiene la informacion de la respuesta del servlet.
	 * @param servletName	Nombre del servlet con el cual se esta realizando la peticion.
	 * @return				Validacion del token, cadena vacia en caso de validacion pendiente.
	 * @throws IOException	En caso de un error con la operacion.
	 */
	private  String validaToken(HttpServletRequest request, HttpServletResponse response, String servletName)
	throws IOException{
		final HttpSession ses = request.getSession();
		final BaseResource session = (BaseResource) ses.getAttribute(SESSION_NAME);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);

		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& session.getToken().getStatus() == 1 && solVal) {
			EIGlobal.mensajePorTrace( "El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion",
					NivelLog.DEBUG);

			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request,servletName);
			ValidaOTP.validaOTP(request, response,IEnlace.VALIDA_OTP_CONFIRM);
		} else {
			return "1";
		}
		return "";
	}


    /**
     * Metodo para cargar Encabezados en el Request.
     *
     * @param request	El HttpServletRequest que contiene los parametros enviados al Servlet.
     */
	private void cargaEncabezados(HttpServletRequest request) {
        BaseResource session = (BaseResource) request.getSession().getAttribute(SESSION_NAME);

        EIGlobal enlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		List<ConfEdosCtaArchivoBean> lista = (List<ConfEdosCtaArchivoBean>)request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS);
		request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, lista);

        request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado(STR_ESTADOS_CUENTA,STR_ESTADOS_CUENTA_DESCARGA_MENU,ARCHIVO_AYUDA,request));
        request.setAttribute("FechaHoy", enlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
        request.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
        request.setAttribute("web_application", Global.WEB_APPLICATION);
        request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
        request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
        request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
        request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
        request.setAttribute("banderaDescarga", "");

        EIGlobal.mensajePorTrace(String.format("%s DescargaEstadoCuenta -> Enviando fechas y encabezados",LOG_TAG), NivelLog.DEBUG);
    }

	/**
	 * Metodo para consultar las cuentas y tarjetas a presentar en el combo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void ctasTarjetas(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		EIGlobal.mensajePorTrace(String.format("%s Inicia ctasTarjeta",
				LOG_TAG), NivelLog.INFO);
		//ConsultaCtasAsociadasBO consultaCtasAsociadasBO = new ConsultaCtasAsociadasBO();
		ConfigModEdoCtaTDCBO configModEdoCtaTDCBO=new ConfigModEdoCtaTDCBO();
		
		List<ConfEdosCtaArchivoBean> cuentas =null;
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		//ConCtasPdfXmlServlet pdfXml = new ConCtasPdfXmlServlet();
		//boolean cambioContrato = pdfXml.cambioContrato(request);
		boolean cambioContrato =ConfigMasPorTCAux.cambiaContrato(request.getSession(),  session.getContractNumber());

		String opcion = request.getParameter(OPCION) != null
			&& !"".equals(request.getParameter(OPCION)) ? request.getParameter(OPCION) : "";
		EIGlobal.mensajePorTrace(this + "-----> opcion: " + opcion, EIGlobal.NivelLog.INFO);
		if (("".equals(opcion) && request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null)
				|| cambioContrato) {
			cuentas = configModEdoCtaTDCBO.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()),
					session.getContractNumber(), session.getUserProfile());
			EIGlobal.mensajePorTrace("Longitud de la lista: "+ cuentas.size(),
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			request.getSession().setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
		} else {
			EIGlobal.mensajePorTrace(String.format("%s pdfXml.filtrarDatos", LOG_TAG),NivelLog.DEBUG);
			ConfigMasPorTCAux.filtrarDatos(request, response);
		}
		cuentas = (List<ConfEdosCtaArchivoBean>) request.getAttribute(EdoCtaConstantes.LISTA_CUENTAS);
		if( cuentas.size() > 25 ){
			List<ConfEdosCtaArchivoBean> cuentasTarjetasAux = new ArrayList<ConfEdosCtaArchivoBean>();
			for( int i = 0 ; i < 25 ; i++ ){
				cuentasTarjetasAux.add( cuentas.get(i) );
			}
			cuentas = cuentasTarjetasAux;
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			request.setAttribute("msgOption", "La consulta gener&oacute; m&aacute;s de 25 resultados, debe ser m&aacute;s espec&iacute;fico en el filtro");
			request.setAttribute("msgExcede", "-1");
		}

	}

}