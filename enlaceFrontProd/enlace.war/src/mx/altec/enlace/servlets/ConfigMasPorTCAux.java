package mx.altec.enlace.servlets;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.EmailSender;

public class ConfigMasPorTCAux {

	/**
	 * Separa tarjetas de credito para presentar en pantalla.
	 * @param cuentas Bean de Cuentas
	 * @author FSW-Indra
	 * @return lista de tarjetas de credito.
	 */
	public static List<ConfEdosCtaArchivoBean> cuentasTarjetasCredito(List<ConfEdosCtaArchivoBean> cuentas) {
		List<ConfEdosCtaArchivoBean> res = new ArrayList<ConfEdosCtaArchivoBean>();
		for (ConfEdosCtaArchivoBean consultaCuentasBean : cuentas) {
			if(consultaCuentasBean.getNomCuenta().length() == 16) {  
				res.add(consultaCuentasBean);
			}
		}		
		return res;
	} 
	
	
	/**
	 * Cambia contrato.
	 * @since 26/02/2015
	 * @author FSW-Indra
	 * @param session Session actual
	 * @param contratoActual Contrato actual
	 * @return Verdadero si hubo un cambio de contrato en la sesion del usuario
	 */
	public static boolean cambiaContrato(HttpSession session, String contratoActual){
		String contratoAnterior = (String) session.getAttribute("contratoAnterior");
		EIGlobal.mensajePorTrace("ConfigMasPorTCAux :: Inicio de metodo cambioContrato" , EIGlobal.NivelLog.INFO);
		boolean cambioContrato = false;
		
		EIGlobal.mensajePorTrace("ConfigMasPorTCAux :: contrato anterior: ["
				+ contratoAnterior + "]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConfigMasPorTCAux :: contrato actual: ["
				+ contratoActual + "]", EIGlobal.NivelLog.INFO);
		
		
		if (contratoAnterior == null) {
			contratoAnterior = contratoActual;			
			cambioContrato = true;
			session.setAttribute("contratoAnterior", contratoAnterior);
		} else if (!contratoAnterior.equals(contratoActual)) {
			// Cuando el contrato cambia, la variable contratoAnterior toma el
			// valor del contrato actual del cliente para posteriormente validar
			// con dicho valor.
			contratoAnterior = contratoActual;
			cambioContrato = true;
			session.removeAttribute("listaTDC_CMov");
			session.removeAttribute("listaCuentasEdoCta");
			session.setAttribute("contratoAnterior", contratoAnterior);
			
		}
		EIGlobal.mensajePorTrace("ConfigMasTCCuentaBO :: contratos distintos: ["
				+ cambioContrato + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ConfigMasTCCuentaBO :: Fin de metodo cambioContrato--->"+cambioContrato , EIGlobal.NivelLog.INFO);

		
		return cambioContrato;				
	}
	
	/**
	 * metodo para filtrar las cuentas
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 */
	public static void filtrarDatos(HttpServletRequest request, HttpServletResponse response) {
		String cuenta = request.getParameter("filtroCuenta") != null ? request.getParameter("filtroCuenta") : "";
		String descripcion = request.getParameter("filtroDesc") != null ? request.getParameter("filtroDesc") : "";
		List<ConfEdosCtaArchivoBean> cuentasTMP = new ArrayList<ConfEdosCtaArchivoBean>();
		List<ConfEdosCtaArchivoBean> cuentasNueva = new ArrayList<ConfEdosCtaArchivoBean>();
		cuentasTMP = (List<ConfEdosCtaArchivoBean>)request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS);
		
		EIGlobal.mensajePorTrace( "ConfigMasPorTCAux-----> cuenta: " + cuenta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConfigMasPorTCAux-----> descripcion: " + descripcion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "ConfigMasPorTCAux-----> cuenta length: " + cuenta.length(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ConfigMasPorTCAux-----> descripcion: length" + descripcion.length(), EIGlobal.NivelLog.INFO);
		
		if (cuenta.length() > 0 || descripcion.length() > 0) {
			EIGlobal.mensajePorTrace("ConfigMasPorTCAux-----> con filtro..." , EIGlobal.NivelLog.INFO);
			for(int i = 0; i < cuentasTMP.size(); i++) {
				
				// Busqueda unicamente por cuenta.
				if((("".equals(descripcion.trim())) && (cuenta.length() > 0)) && (cuentasTMP.get(i).getNomCuenta().contains(cuenta))) {
						EIGlobal.mensajePorTrace("ConfigMasPorTCAux-----> con filtro cuenta" , EIGlobal.NivelLog.INFO);
						cuentasNueva.add(cuentasTMP.get(i));
						/** continue;*/
				}
				// Busqueda unicamente por descripcion.
				if((("".equals(cuenta.trim())) && (descripcion.length() > 0)) && (cuentasTMP.get(i).getNombreTitular().contains(descripcion))) {
						EIGlobal.mensajePorTrace("ConfigMasPorTCAux-----> con filtro descripcion" , EIGlobal.NivelLog.INFO);
						cuentasNueva.add(cuentasTMP.get(i));
						/** continue;*/
				}
				// Busqueda por cuenta y por descripcion.
				if((cuenta.length() > 0 && descripcion.length() > 0) && (cuentasTMP.get(i).getNomCuenta().contains(cuenta) && 
						cuentasTMP.get(i).getNombreTitular().contains(descripcion))) {
							EIGlobal.mensajePorTrace( "ConfigMasPorTCAux-----> con filtro cuenta y descripcion" , EIGlobal.NivelLog.INFO);
							cuentasNueva.add(cuentasTMP.get(i));
							/** continue;*/
				}
			}
			
			if ((cuentasNueva.size())==0) {
				request.setAttribute("MensajeCtas01", "No se encontraron coincidencias.");
			}
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasNueva);
		} else {
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTMP);
		}
	}
	/**
	 * Envio de notificaciones de correo electronico y SMS.
	 * @since 27/02/2015
	 * @author FSW-Indra
	 * @param session Session activa
	 * @param referencia numero referencia
	 * @param folio numero referencia
	 * @param request request actual
	 * @param datos datos para anexar al correo
	 */
	public static void enviarNotificaciones(BaseResource session, int referencia, String folio, 
			HttpServletRequest request, String [] datos){
		EmailSender sender = new EmailSender();
		EIGlobal.mensajePorTrace("Entro a enviar correo", EIGlobal.NivelLog.ERROR);
		
			EmailDetails details = new EmailDetails();
			details.setNumeroContrato(session.getContractNumber());
			details.setRazonSocial(session.getNombreContrato());
			details.setUsuario(session.getUserID8());
			details.setNumRef(folio);
			details.setNumTransmision(String.valueOf(referencia));
			details.setFormatoArchivo(datos);
			EIGlobal.mensajePorTrace("Se envia informacion de correo", EIGlobal.NivelLog.ERROR);
			sender.sendNotificacion(request,EdoCtaConstantes.CONFIGMASPORTC, details);
	}
}
