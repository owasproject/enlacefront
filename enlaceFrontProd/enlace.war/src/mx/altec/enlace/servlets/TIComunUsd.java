package mx.altec.enlace.servlets;

import java.util.*;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.IEnlace;


/**
 *TIComunUsd
 */
public class TIComunUsd extends  BaseServlet
{
	public boolean cuentasUsd (String str)
   	{
      return ( str.startsWith ("82") || str.startsWith ("83") ||
               str.startsWith ("49"));
   	}

    public void cuentasContrato(HttpServletRequest req, HttpServletResponse res)
    	throws ServletException, IOException
    {
    	HttpSession sess = req.getSession();
  		int prev=0,next=0;
		int indice = 1;

		BaseResource session = (BaseResource) sess.getAttribute("session");

		//traer 50 cuentas de muestreo
		next=50;
		String[][] arrayCuentas = ObteniendoCtasInteg (prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+".ambci");
		//***********************************************
		sess.setAttribute("ctasUSD", "");
		sess.setAttribute("ctasMN", "");

		for (int indice2 = 1;indice2<=Integer.parseInt (arrayCuentas[0][0]);indice2++){
			System.out.println("***->cuenta" + arrayCuentas[indice2][1]);
			if(cuentasUsd(arrayCuentas[indice2][1]))
			{
				sess.setAttribute("ctasUSD", "Existe");
			}
			else
			{
				sess.setAttribute("ctasMN", "Existe");
			}
		}//for
		System.out.println("ctasUSD" + sess.getAttribute("ctasUSD"));
		System.out.println("ctasMN" + sess.getAttribute("ctasMN"));
   }

}