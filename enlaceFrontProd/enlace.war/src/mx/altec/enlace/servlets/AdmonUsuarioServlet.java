/**
 * Isban Mexico
 *   Clase: AdmonUsuarioServlet.java
 *   Descripci�n:
 *
 *   Control de Cambios:
 *   1.0 4/07/2013 Stefanini - Creaci�n
 */
package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AdmUsrGL31;
import mx.altec.enlace.beans.ConfigMancBean;
import mx.altec.enlace.beans.NomPrePE68;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BloqueoFactAut;
import mx.altec.enlace.bo.GuardaGruposPerf;
import mx.altec.enlace.bo.InicializaGruposPerfiles;
import mx.altec.enlace.bo.WSBloqueoToken;
import mx.altec.enlace.dao.AdmUsrFacultadesDAO;
import mx.altec.enlace.dao.ConfigMancDAO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.FacPerfilDao;
import mx.altec.enlace.dao.NomPrePersonasDAO;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.gwt.adminusr.shared.BeanAdminUsr;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.VinculacionMonitorPlusUtils;

/**
 * The Class AdmonUsuarioServlet.
 */
public class AdmonUsuarioServlet extends BaseServlet{


	/** The Constant VISTA_MOSTRAR_STRING. */
	private static final String VISTA_MOSTRAR_STRING = "VistaMostrar";

    /** The Constant ERROR_STRING. */
    private static final String ERROR_STRING = "Error";

    /** The Constant DESTINO_STRING. */
    private static final String DESTINO_STRING = "Destino";

    /** The Constant ORIGEN_STRING. */
    private static final String ORIGEN_STRING = "Origen";

    /** The Constant OPERACION_STRING. */
	private static final String OPERACION_STRING = "Operacion";
	
	/** The Constant OPERACION_STRING. */
	private static final String CADENA_VACIA = "";

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8671088696208518991L;

	/** The bool login. */
	private static boolean boolLogin = false;

    
	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

	
	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
  	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		  if(!SesionValida(request, response)) {
			return;
		}

		javax.servlet.http.HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String lstrOperacion = request.getParameter(OPERACION_STRING);
	    String lstrOrigen    = request.getParameter(ORIGEN_STRING);
	    String lstrDestino   = request.getParameter(DESTINO_STRING);
	    String lstrError     = request.getParameter(ERROR_STRING);
	    String lstrVista     = request.getParameter(VISTA_MOSTRAR_STRING)==null?"":request.getParameter(VISTA_MOSTRAR_STRING);
		if (request.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
			if (request.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
				request.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
						request.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
			} else if (getFormParameter(request,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
				request.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
						getFormParameter(request,EnlaceMonitorPlusConstants.DATOS_BROWSER));
			}
		}

	    if(lstrOperacion == null) {
	    	String lstrOperacionTMP = request.getSession().getAttribute(OPERACION_STRING) !=null ? request.getSession().getAttribute(OPERACION_STRING).toString() : "";
		    String lstrOrigenTMP    = request.getSession().getAttribute(ORIGEN_STRING) !=null ? request.getSession().getAttribute(ORIGEN_STRING).toString() : "";
		    String lstrDestinoTMP   = request.getSession().getAttribute(DESTINO_STRING) !=null ? request.getSession().getAttribute(DESTINO_STRING).toString() : "";
		    String lstrErrorTMP     = request.getSession().getAttribute(ERROR_STRING) !=null ? request.getSession().getAttribute(ERROR_STRING).toString() : "";
		    String lstrVistaTMP     = request.getSession().getAttribute(VISTA_MOSTRAR_STRING) !=null ? request.getSession().getAttribute(VISTA_MOSTRAR_STRING).toString() : "";

		    System.out.println("------> valor tmp lstrDestino:  " + lstrDestinoTMP);
		    System.out.println("------> valor  lstrDestino:  " + lstrDestino);

	    	lstrOperacion = lstrOperacion == null  ? lstrOperacionTMP : lstrOperacion;
	    	lstrOrigen = lstrOrigen == null ? lstrOrigenTMP : lstrOrigen;
	    	lstrDestino = lstrDestino == null ? lstrDestinoTMP : lstrDestino;
	    	lstrError = lstrError == null ? lstrErrorTMP : lstrError;
	    	lstrVista = lstrVista == null ? lstrVistaTMP : lstrVista;

	    	System.out.println("------> valor  Destino:  " + lstrDestino);
	    }

	    if("alta".equals(lstrOperacion)) {
	    	if(!session.getFacultad(BaseResource.FAC_ALTA_SUSU)) {
	    		String laDireccion = "document.location='SinFacultades'";
	    		request.setAttribute( "plantillaElegida", laDireccion);
	    		request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
	            return;
	    	}
	    }
	    else if("cambio".equals(lstrOperacion)) {
	    	if(!session.getFacultad(BaseResource.FAC_MOD_SUSU)) {
	    		String laDireccion = "document.location='SinFacultades'";
	    		request.setAttribute( "plantillaElegida", laDireccion);
	    		request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
	            return;
	    	}
	    }
	    else if("baja".equals(lstrOperacion) && !session.getFacultad(BaseResource.FAC_BAJA_SUSU)) {
	    		String laDireccion = "document.location='SinFacultades'";
	    		request.setAttribute( "plantillaElegida", laDireccion);
	    		request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
	            return;
	    }

	    if("".equals(lstrOrigen) && lstrDestino.indexOf("/jsp/adminusr/admin") > -1) {	//acceso inicial, se borran par�metros de sesi�n.
	    	borraParamSesion(request);
	    }

	    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - Recibe: " +
	    		"	\nOperacion: [" + lstrOperacion +  "]" +
	    		"	\nOrigen: [" + lstrOrigen +  "]" +
	    		"	\nDestino: [" + lstrDestino +  "]" +
	    		"	\nError: [" + lstrError +  "]" +
	    		"	\nVistaMostrar: [" + lstrVista +  "]",
	    		EIGlobal.NivelLog.DEBUG);

        if (request.getAttribute("MenuPrincipal") == null){
            request.setAttribute("MenuPrincipal", session.getStrMenu());
            request.setAttribute("newMenu", session.getFuncionesDeMenu());
        }
        String titulo = "Alta Usuario";

        String cliente = request.getParameter("codCliente");
        if(cliente != null) {
        	sess.setAttribute("cliente", cliente);
        }
        /*if (cliente == null) {
        	cliente = (String) sess.getAttribute("cliente");
        }*/
        String cvePerfil = "";
		String error = null;
		String titConf = "";
        if (lstrOperacion != null) {
        	if ("baja".equals(lstrOperacion)) {
            	titulo = "Baja Usuario";
        	} else if ("cambio".equals(lstrOperacion)) {
            	titulo = "Modificaci&oacute;n Usuario";
        	}
            sess.setAttribute("titulo", titulo);
        }
        if (lstrOrigen.contains("detalle")
            	|| lstrOrigen.contains("adminBajaUsr")) {
            	titConf = " Confirmaci&oacute;n de ";
            }
            if (request.getAttribute("Encabezado") == null){
                request.setAttribute("Encabezado",
                        CreaEncabezado("Administraci&oacute;n Super Usuario - " + titConf + titulo,
                                "Administraci&oacute;n y Control &gt; Administraci&oacute;n Super Usuario - "
                        		+ titConf + titulo, "s25570h", request));
            }


		GuardaGruposPerf daoGuardar = new GuardaGruposPerf();
        if (lstrOperacion != null) {
        	if("alta".equals(lstrOperacion) && "/jsp/adminusr/adminAltaUsr.jsp".equals(lstrOrigen)) {
        		if(cliente == null || "".equals(cliente)) {
        			error = "El c&oacute;digo de cliente es obligatorio.";
        		}
        		else if(cliente.length() != 8) {
        			error = "El c&oacute;digo de cliente debe tener 8 posiciones.";
        		}
        		else {
        			try {
        				Integer.parseInt(cliente);
        			} catch(Exception e) {
        				error = "El c&oacute;digo de cliente debe ser num&eacute;rico.";
        			}
        		}
        	}


    		if(cliente != null && !"".equals(cliente) && lstrOrigen.indexOf("adminusr") >= 0) {
    			EIGlobal.mensajePorTrace("Entrando a validar el perfil...", EIGlobal.NivelLog.DEBUG);
    			FacPerfilDao daoPerf = new FacPerfilDao();
    			cvePerfil = daoPerf.getPerfilUsuario(cliente, session.getContractNumber());
    			daoPerf.cierraConexion();
    			daoPerf = null;
    		}


    		if(error == null && cliente != null) {
    			System.out.println("Entrando a validar PE68...");
    			MQQueueSession mqsess = null;
    			NomPrePersonasDAO persDAO = new NomPrePersonasDAO();
	    		NomPrePE68 pe68 = new NomPrePE68();

    			try {
    	    		mqsess = new MQQueueSession(
    	    				NP_JNDI_CONECTION_FACTORY,
    	    				NP_JNDI_QUEUE_RESPONSE,
    	    				NP_JNDI_QUEUE_REQUEST);
    	    		persDAO.setMqsess(mqsess);
    	    		pe68 = persDAO.consultarPersona(cliente);
    			} catch (Exception x) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - PE68 -" +
				    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
			    	} finally {
			    		try {
			    			mqsess.close();
			    		}catch(Exception x) {
						    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - PE68 -" +
						    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
			    		}
			    	}

    			if(pe68 != null && pe68.getEstadoPersona() != null) {
    				EIGlobal.mensajePorTrace ("Estatus Cliente: <" + pe68.getEstadoPersona() + ">", EIGlobal.NivelLog.DEBUG);
    				EIGlobal.mensajePorTrace ("Condici Cliente: <" + pe68.getCondicionPersona() + ">", EIGlobal.NivelLog.DEBUG);
    				if("ACL".equals(pe68.getCondicionPersona()) && !"baja".equals(lstrOperacion)) {
    					error = "Este usuario no se puede vincular, <br>por favor consulte con su ejecutivo de cuenta.";
    				}
        			sess.setAttribute("nombre", pe68.getNombre() + " " + pe68.getPaterno() + " " + pe68.getMaterno());        			//c�digo Y456
    			}
    			else {
    				error = "No existe el c&oacute;digo de cliente.";
    			}
    			if(error != null) {
    				EIGlobal.mensajePorTrace ("Mensaje de Error: <" + error + ">", EIGlobal.NivelLog.INFO);
    			}
        		EIGlobal.mensajePorTrace ("Perfil: <" + cvePerfil + ">", EIGlobal.NivelLog.INFO);

        		if("alta".equals(lstrOperacion) && cvePerfil.length() > 0) {
        			error = "El usuario ya est&aacute; asociado a su contrato.";
        		}
    		}

    		if(("cambio".equals(lstrOperacion) || "baja".equals(lstrOperacion)) && cliente != null && !"".equals(cliente) && cvePerfil.length() == 0 && lstrOrigen.indexOf("adminusr") >= 0) {	//valida que el usuario exista
    			error = "El usuario ya fue dado de baja.";
        		String[][] arrayUsuarios = TraeUsuarios(request);
        		for(int z=1;z<=Integer.parseInt(arrayUsuarios[0][0].trim());z++){
        			System.out.println( "Valor del USUARIO ANTES: "+arrayUsuarios[z][1]+"&");
        			arrayUsuarios[z][1] = convierteUsr7a8(arrayUsuarios[z][1]);
        			System.out.println( "Valor del  DESPUES: "+arrayUsuarios[z][1]+"&");
        		}
        		request.setAttribute("usuarios", arrayUsuarios);

    		}
    		if(error != null) {
    			 request.setAttribute("error", error);
    			 evalTemplate (lstrOrigen, request, response);
    			 return;
    		}

    		///////////////////--------------challenge--------------------------
            String validaChallenge = request.getAttribute("challngeExito") != null
                ? request.getAttribute("challngeExito").toString() : "";

            EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

//            if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge) && lstrOrigen.contains("detalle")) {
            if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge) && (
                    lstrOrigen.contains("detalle") ||
                    ("baja".equals(lstrOperacion) && lstrDestino.contains("desgloseBajaUsr"))
                )) {

                EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
                ValidaOTP.guardaParametrosEnSession(request);

                System.out.println("-------------->dentro challenge lstrDestino:" + lstrDestino);

                request.getSession().setAttribute(OPERACION_STRING, lstrOperacion);
        	    request.getSession().setAttribute(ORIGEN_STRING, lstrOrigen);
        	    request.getSession().setAttribute(DESTINO_STRING, lstrDestino);
        	    request.getSession().setAttribute(ERROR_STRING, lstrError);
        	    request.getSession().setAttribute(VISTA_MOSTRAR_STRING, lstrVista);

                validacionesRSA(request, response);
                return;
            } else {
                boolean solVal=ValidaOTP.solicitaValidacion(
                        session.getContractNumber(),IEnlace.ADM_SUPERUSR);

                if( session.getValidarToken() && session.getToken().getStatus() == 1 && solVal) {
                    request.getSession().setAttribute("valOTP", "true");
                }
            }
            //Fin validacion rsa para challenge
            ///////////////////-------------------------------------------------



        	if("baja".equals(lstrOperacion) || "cambio".equals(lstrOperacion)) {
        		System.out.println("Opcion nula");
        		String[][] arrayUsuarios = TraeUsuarios(request);
        		for(int z=1;z<=Integer.parseInt(arrayUsuarios[0][0].trim());z++){
        			System.out.println( "Valor del USUARIO ANTES: "+arrayUsuarios[z][1]+"&");
        			arrayUsuarios[z][1] = convierteUsr7a8(arrayUsuarios[z][1]);
        			System.out.println( "Valor del  DESPUES: "+arrayUsuarios[z][1]+"&");
        		}
        		request.setAttribute("usuarios", arrayUsuarios);
        	}

        } else {
        	titulo = (String) sess.getAttribute("titulo");
        }

    	if("".equals(lstrOrigen)){
	    	evalTemplate (lstrDestino, request, response);
	    } else {
		    if("detalle".equals(lstrVista) || ("desglose".equals(lstrVista) && "baja".equals(lstrOperacion))){
		    	System.out.println("Generando BeanAdminUsr");
		    	BeanAdminUsr bean = new BeanAdminUsr();
		    	ConfigMancDAO mancDAO = new ConfigMancDAO();
	    		try {
	    			bean.setMostrarMancomunidad(mancDAO.esMancomunado(session.getContractNumber()));
				} catch (Exception e) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
				    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
				}

		    	InicializaGruposPerfiles test = new InicializaGruposPerfiles();
		    	ArrayList <GrupoPerfiles> res = test.obtenUniverso();
		    	bean.setTodosGrupoPerfiles(res);

		    	AdmUsrFacultadesDAO facDAO = new AdmUsrFacultadesDAO();
		    	MQQueueSession mqsess = null;
		    	AdmUsrGL31 fact = null;
		    	try {
		    		mqsess = new MQQueueSession(
		    				NP_JNDI_CONECTION_FACTORY,
		    				NP_JNDI_QUEUE_RESPONSE,
		    				NP_JNDI_QUEUE_REQUEST);
		    		facDAO.setMqsess(mqsess);
		    		fact = facDAO.consultaFacultades("", "SI", "", "", "NO", "");
		    	} catch (Exception x) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
			    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    	} finally {
		    		try {
		    			mqsess.close();
		    		}catch(Exception x) {
					    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
					    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    		}
		    	}
		    	bean.setTodasFacultadesExt(fact.getFacultades());

		    	ArrayList<String> seleccionEx = new ArrayList<String>();
				boolean result = false;
				//String coderror = "";
		    	//ServicioTux servicio = new ServicioTux();
				FacPerfilDao daoFac = new FacPerfilDao();
		    	for(Facultad fac : fact.getFacultades()) {
					result = false;
					result = daoFac.tieneFacultadEx(cvePerfil, fac.getCveFacultad(), "+");
					if(result) {
						//seleccionEx.add(fac.getCveFacultad());
						seleccionEx.add(fac.getCveFacultad() + "&" + fac.getDescripcion());
						EIGlobal.mensajePorTrace ("CargaFacExtraord()-> [" + fac.getCveFacultad() + "]",EIGlobal.NivelLog.DEBUG);
					}
		    	}
		    	daoFac.cierraConexion();

		    	//usuario -> cliente
		    	//contrato -> session.getContractNumber()

		    	//ConfigMancDAO mancDAO = new ConfigMancDAO();
		    	ConfigMancBean mancDTO = new ConfigMancBean();
		    	//Listo
		    	bean.setFirma("");
		    	bean.setMonto("");
		    	try {
					mancDTO = mancDAO.consultaConfigManc(session.getContractNumber(),
							convierteUsr8a7(cliente));
			    	bean.setFirma(mancDTO.getFirma());
			    	bean.setMonto(mancDTO.getLimite());

				} catch (Exception e) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
				    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
				}
		    	bean.setFacultadesUsuario(test.seleccionActual(cliente, cvePerfil));
		    	test.cierraConexiones();
		    	bean.setFacultadesExtUsuario(seleccionEx);
		    	if("baja".equals(lstrOperacion)) {
		    		request.getSession().setAttribute("BeanAdminUsrFin", bean);
		    	}
		    	else {
		    		request.getSession().setAttribute("BeanAdminUsr", bean);
		    	}
		    }
			EmailSender sender = new EmailSender();
	    	BeanAdminUsr beanOri = (BeanAdminUsr) request.getSession().getAttribute("BeanAdminUsr");
	    	BeanAdminUsr beanFin = (BeanAdminUsr) request.getSession().getAttribute("BeanAdminUsrFin");


	    	if(("alta".equals(lstrOperacion) || "cambio".equals(lstrOperacion)) &&
		    		("confirmacion".equals(lstrVista))) {
        		//Registrar datos (altas, bajas, cambios)
		    	String cveOpe = "";
		    	cliente = (String) sess.getAttribute("cliente");

                if(validaToken(request) != 0) {
                    evalTemplate (lstrOrigen, request, response);
                    return;
                }

		    	int respGrd = -1;
		    	if (beanOri != null && beanFin != null) {
  	        		if("cambio".equals(lstrOperacion)) {
  	        			FacPerfilDao daoPerf = new FacPerfilDao();
  	        			cvePerfil = daoPerf.getPerfilUsuario(cliente, session.getContractNumber());
  	        			daoPerf.cierraConexion();
  	        			daoPerf = null;
  	        			respGrd = daoGuardar.borraPerfilacion(beanFin.getTodasFacultadesExt(), session.getContractNumber(), cvePerfil, cliente, lstrOperacion);
  	        		}
  	        		if("alta".equals(lstrOperacion)) {
  	        			FacPerfilDao daoPerf = new FacPerfilDao();
  	        			if(!daoPerf.usuarioEnOtroContrato(cliente, session.getContractNumber())) {
  	        				daoPerf.altaUsuario(cliente);
  	        				daoPerf.commit();
  	        			}
  	        			daoPerf.cierraConexion();
  	        			daoPerf = null;
  	        		}
  	        		if("alta".equals(lstrOperacion) || respGrd == 0){
  	        			respGrd = daoGuardar.guardaPerfilacion(daoGuardar.procesaEdoInicial(beanOri.getTodosGrupoPerfiles(), beanFin.getFacultadesUsuario()), beanFin.getFacultadesUsuario(),
  		        				beanOri.getFacultadesExtUsuario(), beanFin.getFacultadesExtUsuario(), session.getContractNumber(), cvePerfil, cliente, (String) sess.getAttribute("nombre"), lstrOperacion);
  	        		}
  	        		if(beanFin.getMostrarMancomunidad() && ("alta".equals(lstrOperacion)
  	        				|| "cambio".equals(lstrOperacion))){
  	  	        		ConfigMancDAO cmDAO = new ConfigMancDAO();
  	  	        		try {
  	  	        			cmDAO.actualizaConfigManc(new ConfigMancBean(session.getContractNumber(),
  	  	        					convierteUsr8a7(cliente), beanFin.getMonto(), beanFin.getFirma()));
  	  	        		} catch (Exception e) {
  	  	        			EIGlobal.mensajePorTrace("AdmonUsuarioServlet - actualizaMancomuunidad -" +
  					    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
  	  	        		}
  	        			cmDAO = null;
  	        		}

		    	}

			    request.setAttribute("usuario", cliente);
			    request.setAttribute("nombre", sess.getAttribute("nombre"));
			    if("alta".equals(lstrOperacion)) {
			    	request.setAttribute("operacion", "Alta");
			    	cveOpe = "AUSS";
			    }
			    else if("cambio".equals(lstrOperacion)) {
			    	request.setAttribute("operacion", "Modificaci&oacute;n");
			    	cveOpe = "MFUS";
			    }
			    request.setAttribute("fecha", ObtenFecha());
	    		//Hashtable referencia = null;
			    int numRef = 0;
			    switch(respGrd) {
			    	case 0:

			    		//request.setAttribute("estatus", "Operaci\u00f3n exitosa");
						request.setAttribute("estatus", "La operaci\u00f3n  fue realizada exitosamente, " +
								"para visualizar el cambio es necesario que reinicie su sesi\u00f3n de Enlace");

			    		/*try {
							ServicioTux st = new ServicioTux();
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							Integer numRef = (Integer)referencia.get("REFERENCIA");
							st.sbitacora(numRef, "ADMU0000", session.getContractNumber(), session.getUserID(), cliente, 0, cveOpe, "", 0, "");
				    		request.setAttribute("referencia", "" + numRef);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
						}*/
						try{
					        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION MTTO USR",EIGlobal.NivelLog.DEBUG);
					        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
							EIGlobal.mensajePorTrace("Referencia " + numRef  ,EIGlobal.NivelLog.INFO);
						  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
						  	BitaTCTBean bean = new BitaTCTBean ();
						  	bean = bh.llenarBeanTCT(bean);
						  	bean.setReferencia(numRef);
						  	bean.setTipoOperacion(cveOpe);
						  	bean.setCuentaOrigen(cliente);
						  	bean.setConcepto(this.Description(cveOpe));
						  	bean.setCodError("ADMU0000");
						    BitaHandler.getInstance().insertBitaTCT(bean);
						    
						    // Se envian parametros para MonitorPlus
						    EIGlobal.mensajePorTrace("<----- SE ENVIA PARAMETROS PARA MAPEO A ** MONITORPLUS ** >",
						    		EIGlobal.NivelLog.DEBUG);						    
							VinculacionMonitorPlusUtils.enviarInclusionUsuarioContrato(request, numRef, IEnlace.SUCURSAL_OPERANTE, session);
				    	}catch(SQLException e){
				    		e.printStackTrace();
				   		}catch(Exception e){
				   			e.printStackTrace();
				   		}
				   		request.setAttribute("referencia", CADENA_VACIA + numRef);

						try {
							EmailDetails details = new EmailDetails();
							details.setNumeroContrato(session.getContractNumber());
							details.setRazonSocial(session.getNombreContrato());
							details.setEstatusActual("ADMU0000");
							details.setNumRef(cliente);
							details.setNumTransmision(CADENA_VACIA + numRef);

							if(cveOpe!=null && "AUSS".equals(cveOpe))
								details.setTipo_operacion("ALTA");
							if(cveOpe!=null && cveOpe.equals("MFUS"))
								details.setTipo_operacion("MODIFICACI�N");

							sender.sendNotificacion(request,IEnlace.NOT_ADMIN_USUARIOS, details);


						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}




			    		break;
			    	case -1:
			    		request.setAttribute("estatus", "Ha ocurrido un error.");
			    		request.setAttribute("referencia", "----");
  	        			EIGlobal.mensajePorTrace("Error en mto usuario " + cliente, EIGlobal.NivelLog.ERROR);

			    		break;
			    	default:
			    		request.setAttribute("estatus", "Ha ocurrido un error, cod: ADMU000" + respGrd);
						try {
							/*ServicioTux st = new ServicioTux();
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							Integer numRef = (Integer)referencia.get("REFERENCIA");
							st.sbitacora(numRef, "ADMU000" + respGrd, session.getContractNumber(), session.getUserID(), cliente, 0, cveOpe, "", 0, "");*/
							numRef = 0;
							try{
						        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION MTTO USR",EIGlobal.NivelLog.DEBUG);
						        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + numRef ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean = bh.llenarBeanTCT(bean);
							  	bean.setReferencia(numRef);
							  	bean.setTipoOperacion(cveOpe);
							  	bean.setCuentaOrigen(cliente);
							  	//bean.setConcepto(this.Description(cveOpe));
							  	bean.setCodError("ADMU000" + respGrd);
							    BitaHandler.getInstance().insertBitaTCT(bean);
					    	}catch(SQLException e){
					    		e.printStackTrace();
					   		}catch(Exception e){
					   			e.printStackTrace();
					   		}
				    		request.setAttribute("referencia", CADENA_VACIA + numRef);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
						}
			    		break;



			    }
			    borraParamSesion(request);	//limpiamos al terminar
		    }
		    else if(lstrOperacion.equals("baja") && lstrVista.equals("confirmacion")) {
		    	cliente = (String) sess.getAttribute("cliente");

                if(validaToken(request) != 0) {
                    evalTemplate (lstrOrigen, request, response);
                    return;
                }

		    	FacPerfilDao daoPerf = new FacPerfilDao();
		    	cvePerfil = daoPerf.getPerfilUsuario(cliente, session.getContractNumber());


		    	int respGrd = 0;
		    	if(!daoPerf.perfilEnOtroUsuario(cvePerfil, cliente)) {	//se borra la perfilaci�n completa del usuario.
		    		respGrd = daoGuardar.borraPerfilacion(beanFin.getTodasFacultadesExt(), session.getContractNumber(), cvePerfil, cliente, lstrOperacion);
		    	}
		    	else {	//s�lo se desvincula por existir su clave de perfil.
		    		daoPerf.bajaUsuario(cvePerfil, BaseServlet.convierteUsr8a7(cliente), session.getContractNumber());
		    		daoPerf.commit();
		    	}
		    	daoPerf.cierraConexion();
		    	// Borra Mancomunidad
	        		if(beanFin.getMostrarMancomunidad()){
  	  	        		ConfigMancDAO cmDAO = new ConfigMancDAO();
  	  	        		try {
  	  	        			cmDAO.eliminaConfigManc(session.getContractNumber(), convierteUsr8a7(cliente));
  	  	        		} catch (Exception e) {
  	  	        			EIGlobal.mensajePorTrace("AdmonUsuarioServlet - eliminaMancomunidad -" +
  	  					    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
  	  	        		}
  	        			cmDAO = null;
  	        		}
// -> bloqueo (inicio)
	        	// Bloquea Factores de Autenticacion
				EIGlobal.mensajePorTrace("-----> jgal [Empieza registro de baja]", EIGlobal.NivelLog.DEBUG);
	        	BloqueoFactAut bloqBO = new BloqueoFactAut();
		    	try {
					bloqBO.bloqueaFactores(cliente, session.getContractNumber(),
							beanFin.getTodasFacultadesExt(), cvePerfil, request);
				} catch (Exception e1) {
	        		EIGlobal.mensajePorTrace("AdmonUsuarioServlet - bloqueaFactores -" +
	  				    		"Error: [" + e1 +  "]", EIGlobal.NivelLog.ERROR);
				}

// -> bloqueo (fin)
		    	request.setAttribute("usuario", cliente);
			    request.setAttribute("nombre", sess.getAttribute("nombre"));
			    if(lstrOperacion.equals("baja")) {
			    	request.setAttribute("operacion", "Baja");
			    }
			    request.setAttribute("fecha", ObtenFecha());
	    		//Hashtable referencia = null;
	    		int numRef = 0;
			    switch(respGrd) {
			    	case 0:
						try {
						EmailDetails details = new EmailDetails();
						details.setNumeroContrato(session.getContractNumber());
						details.setRazonSocial(session.getNombreContrato());
						details.setNumRef(cliente);
						details.setEstatusActual("ADMU0000");
						details.setTipo_operacion("Baja");
						sender.sendNotificacion(request,IEnlace.NOT_ADMIN_USUARIOS, details);




					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
			    		//request.setAttribute("estatus", "Operaci\u00f3n exitosa");
						request.setAttribute("estatus", "La operaci\u00f3n  fue realizada exitosamente, " +
								"para visualizar el cambio es necesario que reinicie su sesi\u00f3n de Enlace");

						try {
							/*ServicioTux st = new ServicioTux();
							referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							Integer numRef = (Integer)referencia.get("REFERENCIA");
							st.sbitacora(numRef, "ADMU0000", session.getContractNumber(), session.getUserID(), cliente, 0, "BUSS", "", 0, "");*/

							try{
						        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION MTTO USR",EIGlobal.NivelLog.DEBUG);
						        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + numRef ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean = bh.llenarBeanTCT(bean);
							  	bean.setReferencia(numRef);
							  	bean.setTipoOperacion("BUSS");
							  	bean.setCuentaOrigen(cliente);
							  	//bean.setConcepto("Baja Usuario");
							  	bean.setCodError("ADMU0000");
							    BitaHandler.getInstance().insertBitaTCT(bean);
					    	}catch(SQLException e){
					    		e.printStackTrace();
					   		}catch(Exception e){
					   			e.printStackTrace();
					   		}
				    		request.setAttribute("referencia", CADENA_VACIA + numRef);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
						}
			    		break;
			    	case -1:
			    		request.setAttribute("estatus", "Ha ocurrido un error.");
			    		request.setAttribute("referencia", "----");
  	        			EIGlobal.mensajePorTrace("Error en mto usuario " + cliente, EIGlobal.NivelLog.ERROR);
			    		break;
			    	default:
			    		request.setAttribute("estatus", "Ha ocurrido un error, cod: ADMU000" + respGrd);
						try {
							//ServicioTux st = new ServicioTux();
							//referencia = st.sreferencia(IEnlace.SUCURSAL_OPERANTE);
							//Integer numRef = (Integer)referencia.get("REFERENCIA");
							//st.sbitacora(numRef, "ADMU000" + respGrd, session.getContractNumber(), session.getUserID(), cliente, 0, "BUSS", "", 0, "");
							numRef = 0;
							try{
						        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION MTTO USR",EIGlobal.NivelLog.DEBUG);
						        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + numRef  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean = bh.llenarBeanTCT(bean);
							  	bean.setReferencia(numRef);
							  	bean.setTipoOperacion("BUSS");
							  	bean.setCuentaOrigen(cliente);
							  	bean.setConcepto("Baja Usuario");
							  	bean.setCodError("ADMU000" + respGrd);
							    BitaHandler.getInstance().insertBitaTCT(bean);
					    	}catch(SQLException e){
					    		e.printStackTrace();
					   		}catch(Exception e){
					   			e.printStackTrace();
					   		}
				    		request.setAttribute("referencia", CADENA_VACIA + numRef);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
						}
			    		break;
			    }
			    borraParamSesion(request);	//limpiamos al terminar el flujo

		    }
		    EIGlobal.mensajePorTrace("AdmonUsuarioServlet Reenviando a [" + lstrDestino +  "]",
		    		EIGlobal.NivelLog.INFO);
			evalTemplate (lstrDestino, request, response);
		  }

	    }

	/**
	 * Borra param sesion.
	 *
	 * @param request the request
	 */
	private void borraParamSesion(HttpServletRequest request) {
		request.getSession().removeAttribute("titulo");
		request.getSession().removeAttribute("cliente");
		request.getSession().removeAttribute("nombre");
		request.getSession().removeAttribute("error");
		request.getSession().removeAttribute("valOTP");
		request.getSession().removeAttribute("BeanAdminUsr");
		request.getSession().removeAttribute("BeanAdminUsrFin");
		request.getSession().removeAttribute("challngeExito");

		request.getSession().removeAttribute(OPERACION_STRING);
	    request.getSession().removeAttribute(ORIGEN_STRING);
	    request.getSession().removeAttribute(DESTINO_STRING);
	    request.getSession().removeAttribute(ERROR_STRING);
	    request.getSession().removeAttribute(VISTA_MOSTRAR_STRING);
	}

	/* (non-Javadoc)
	 * @see mx.altec.enlace.servlets.BaseServlet#intentoFallido(mx.altec.enlace.dao.Token)
	 */
	public void intentoFallido(Token tok) {
		Connection conn = null;
	 try {
			conn= createiASConn ( Global.DATASOURCE_ORACLE );
			tok.incrementaIntentos(conn);
		} catch(SQLException e) {
                             EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally {
			EI_Query.cierraConnection(conn);
		}
	}

	
	/**
	 * Limpia intento fallido.
	 *
	 * @param tok the tok
	 */
	public void limpiaIntentoFallido(Token tok) {
		Connection conn = null;
	try {
		conn= createiASConn ( Global.DATASOURCE_ORACLE );
		tok.reinicializaIntentos(conn);
	} catch(SQLException e) {
                         EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}finally {
		EI_Query.cierraConnection(conn);
	}
}

	/**
	 * Valida token.
	 *
	 * @param request the request
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int validaToken(HttpServletRequest request )
	throws ServletException, IOException{

		  HttpSession sess = request.getSession();
		  BaseResource session = (BaseResource) sess.getAttribute("session");
	      String error = null;
		  String usr = session.getUserID8();

		  if(request.getSession().getAttribute("valOTP") != null) {
	    		Token tok = session.getToken();
	    		Connection conn = null;
	    		boolean result = false;
				try {
					conn = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
					result = tok.validaToken(conn, request.getParameter("token"));
				} catch (SQLException e) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - validaToken -" +
				    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
				} finally {
					EI_Query.cierraConnection(conn);
				}

	    		if(result) {
				    ValidaOTP.guardaRegistroBitacora(request,"Token valido.");
	    			limpiaIntentoFallido(tok);
	    			EIGlobal.mensajePorTrace( "------------TOKEN VALIDO---->", EIGlobal.NivelLog.DEBUG);
					return 0;
				}
				else{
					if(tok.getIntentos() >= 4) {
					//llama al WS de bloqueo de Token
					WSBloqueoToken bloqueo = new WSBloqueoToken();
					String[] resultado = bloqueo.bloqueoToken(usr);
					//Realizar envio de notificaci�n

					EmailSender emailSender=new EmailSender();
					EmailDetails emailDetails = new EmailDetails();

					String numContrato = session.getUserID();
					String nombreContrato = session.getNombreUsuario();

					emailDetails.setNumeroContrato(numContrato);
					emailDetails.setRazonSocial(nombreContrato);

					emailSender.sendNotificacion(request,IEnlace.BLOQUEO_TOKEN_INTENTOS,emailDetails);
					ValidaOTP.guardaRegistroBitacora(request,"BTIF Bloqueo de token por intentos fallidos.");
					BitaTCTBean beanTCT = new BitaTCTBean ();
					BitaHelper bh =	new BitaHelperImpl(request,session,request.getSession(false));

					beanTCT = bh.llenarBeanTCT(beanTCT);
					beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					beanTCT.setTipoOperacion("BTIF");
					beanTCT.setCodError("BTIF0000");
					beanTCT.setOperador(session.getUserID8());
					beanTCT.setConcepto("Bloqueo de token por intentos fallidos");

					try {
						BitaHandler.getInstance().insertBitaTCT(beanTCT);
					}catch(Exception e) {
						EIGlobal.mensajePorTrace("AdmonUsuarioServlet - Error al insertar en bitacora intentos fallidos",EIGlobal.NivelLog.ERROR);

					}

					bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setIdFlujo("BTIF");
					bt.setNumBit("000000");
					bt.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					bt.setIdToken(tok.getSerialNumber());

					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace (" - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace (" - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
					intentoFallido(tok);
					request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�perl�nea Empresarial para desbloquearlo.\", 3);" );
					request.setAttribute ( "Fecha", ObtenFecha () );
					sess.setAttribute("session", session);
					sess.invalidate();
					HttpServletResponse response = null;
					evalTemplate ( IEnlace.LOGIN, request, response );
					return 1;
				}
				intentoFallido(tok);
				boolLogin = false;
				ValidaOTP.guardaRegistroBitacora(request,"Token no valido.");
				EIGlobal.mensajePorTrace( "------------TOKEN INVALIDO--REPITE-->", EIGlobal.NivelLog.DEBUG);
				error = "Contrase&ntilde;a incorrecta.";
	    		request.setAttribute("error", error);
				}
				return 1;
	    	}
		/*try{
		login( false, request, response );
		} catch(Exception e) {
			response.setContentType("text/html");
			evalTemplate( IEnlace.LOGIN, request, response );
		}*/
		  return 0;
	  }
}