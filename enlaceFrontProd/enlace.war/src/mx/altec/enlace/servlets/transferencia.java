package mx.altec.enlace.servlets;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.MDI_Importar;
import mx.altec.enlace.bo.transferenciaUPL;
import mx.altec.enlace.cliente.ws.csd.BeanDatosOperMasiva;
import mx.altec.enlace.cliente.ws.csd.ConstantesSD;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.servicios.WSLinCapService;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceLynxConstants;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.EnlaceLynxUtils;
import mx.altec.enlace.utilerias.FavoritosConstantes;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.SelloDigitalUtils;
import mx.altec.enlace.utilerias.TransferenciasMonitorPlusUtils;
import mx.isban.conector.lynx.ConectorLynx;
import mx.isban.conector.lynx.dto.RespuestaLynxDTO;
import mx.isban.conector.lynx.util.LynxConstants;


/**
 * Clase trasnferencia.
 */
public class transferencia extends BaseServlet
{
	/**
	 * Constante para la literal "TRAN"
	 */
	public static final String CAD_TIP_OP_TRAN = "TRAN";
	/**
	 * Constante para la literal "PAGT"
	 */
	public static final String CAD_TIP_OP_PAGT = "PAGT";
	/**
	 * Constante para la literal " align=center nowrap>"
	 */
	public static final String CENTR_NOWRP = " align=center nowrap>";
	/**
	 * Constante para la literal " align=right nowrap>-------</TD>"
	 */
	public static final String RIGHT_NOWRAP = " align=right nowrap>-------</TD>";
	
	/** ESPACIO_BLANCO espacio en blanco para el campo observaciones  */
	private static final String ESPACIO_BLANCO = " align=right nowrap>&nbsp;</TD>";
	
	/** Constante MONEDA */
	private static final String MONEDA = "moneda";
	
	/** The fac disp tarj. */
	private static final String FAC_DISP_TARJ = "DISPTARJCRED";

	/** Variable tipo_operacion. */
	private static  String tipo_operacion = CAD_TIP_OP_TRAN;
	
	/** Constante CADENA_VACIA.*/
	private static final String CADENA_VACIA = "";

	/** Constante CADENA_VACIA.*/
	private static final char SEPARADOR_CHR_ARROBA = '@';
	
	/** Constante CADENA_VACIA.*/
	private static final String SEPARADOR_STR_ARROBA = "@";


	/** Constante CADENA_VACIA.*/
	private static final char SEPARADOR_PIPE = '|';

	
	
	/** Variable strDebug. */
	String strDebug 			= "";

	/** Variable nmaxoper. */
	int    nmaxoper;

	/** Variable strInhabiles. */
	String strInhabiles	    = "";
	

	/**
	 * Do get.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}

	/**
	 * Do post.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	{
		defaultAction( req, res );
	}

	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("*********** TRANSFERENCIAS **********", EIGlobal.NivelLog.DEBUG);
		String contrato 			= "";
		String usuario				= "";
		String clave_perfil			= "";
		boolean cadena_getFacCOpProg;

		@SuppressWarnings("unused")
		String strDivisa			= "";
		String strTrans 			= "";

		@SuppressWarnings("unused")
		String menuSesion			= "";
		EIGlobal.mensajePorTrace("***transferencia.defaultAction()->Entrando", EIGlobal.NivelLog.DEBUG);
		boolean sesionvalida = SesionValida( req, res );

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if ( !sesionvalida ) { return; }
		/******************************************Inicia validacion OTP**************************************/
        /**
		 * Inicio PYME 2015 Unificacion Token Contrasena incorrecta
		 */
        String valida = "";
		if( (req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull"))) &&
			(req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar"))) ){
			valida = null;
			EIGlobal.mensajePorTrace( "------------valida---->"+ valida, EIGlobal.NivelLog.DEBUG);
		} else {
			valida = getFormParameter(req, "valida" );
			if( ( valida == null || "".equals( valida ) ) &&
				req.getAttribute("valida2") != null ){
				valida = req.getAttribute("valida2").toString();
			}
		}
		/**
		 * Fin PYME 2015 Unificacion Token
		 */
		if(validaPeticion( req,  res,session,sess,valida))
		{
			  EIGlobal.mensajePorTrace("\n\n ENTR A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
		}
		/******************************************Termina validacion OTP**************************************/
        else	if( session.getFacultad(session.FAC_CARGO_CHEQUES) || session.getFacultad(FAC_DISP_TARJ))
		{
			if (req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
				if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
				} else if (getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER));
				}
			}
			contrato = session.getContractNumber();

			usuario = session.getUserID8();
			clave_perfil = session.getUserProfile();

			menuSesion = session.getstrMenu();
			cadena_getFacCOpProg = session.getFacultad(session.FAC_PROGRAMA_OP);

			nmaxoper = Global.MAX_REGISTROS;
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			String funcion_llamar	= getFormParameter(req,"ventana");
			/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Se agregan favoritos
			 * se valida si viene de consultar favoritos, para ahora cargar
			 * la informacion del favorito */
			if(identificarEsFavorito(req)){
				EIGlobal.mensajePorTrace("Si viene de favoritos", EIGlobal.NivelLog.INFO);
				funcion_llamar = iniciaCargaDeFavorito( sess, req );
			}
			/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
			if (funcion_llamar == null) {
				EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> funcion_llamar ven����a en null, se le asigna \"0\"", EIGlobal.NivelLog.ERROR);
				funcion_llamar = "0";
			}
			String tipo_trans	= getFormParameter(req,"trans");
			EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> funcion_llamar = " +funcion_llamar+ "<", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> tipo_trans = " +tipo_trans+ "<", EIGlobal.NivelLog.INFO);


			strInhabiles =diasInhabilesDesp();

			if( "".equals(strInhabiles.trim()) ){
				strInhabiles="01/01/2002,21/03/2002";
			}
			if (strInhabiles.trim().length()==0)
			{
				EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> no se encontraron dias inhabiles", EIGlobal.NivelLog.DEBUG);

				if (funcion_llamar.equals("0")){
					despliegaPaginaError("Servicio no disponible por el momento","Transferencias entre chequeras en M.N.","Transferencias &gt; Moneda Nacional","s25360h",req,res);
				} else if  (funcion_llamar.equals("3")) {
					despliegaPaginaError("Servicio no disponible por el momento","Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito","s25520h",req,res);
				} else if  (funcion_llamar.equals("6")) {
					despliegaPaginaError("Servicio no disponible por el momento","Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n","s40010h",req,res);
				} else if (funcion_llamar.equals("10")) {
					despliegaPaginaError("Servicio no disponible por el momento","Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  D&oacute;lares","s25400h",req,res);
				}
				return ;
			}
			if ( "0".equals(funcion_llamar) ){
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias Moneda Nacional","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea",req));
				sess.setAttribute(MONEDA, "Moneda Nacional");

			}
			//RMV se altero, se agrego el equals("8")
			else if (("1".equals(funcion_llamar))||("2".equals(funcion_llamar)) ||("8".equals(funcion_llamar))) {
				 req.setAttribute("Encabezado", CreaEncabezado("Transferencias ","Transferencias ",req));
			}
			else if ("3".equals(funcion_llamar)){
				sess.removeAttribute(MONEDA);
				req.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito",req));
			}
			else if ("6".equals(funcion_llamar)){
				sess.removeAttribute(MONEDA);
				req.setAttribute("Encabezado",CreaEncabezado("Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n",req));
			}
			else if ("4".equals(funcion_llamar)){
				req.setAttribute("Encabezado",CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  D&oacute;lares",req));


				sess.setAttribute(MONEDA, "Dolares");

			}
			else if ("10".equals(funcion_llamar)){
				req.setAttribute("Encabezado",CreaEncabezado("Transferencia por archivo","Transferencias ",req));
			}
			boolean tieneFacultades = false;

			if ( "6".equals(funcion_llamar) )
			{
				if (session.getFacultad(FAC_DISP_TARJ))
				{
					tieneFacultades = true;
					EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> TIENE FACULTAD PARA DISPOSICION DE TARJETA", EIGlobal.NivelLog.INFO);
				}
			}
			else
			{
				if (session.getFacultad(session.FAC_ABONO_CHEQUES))
				{
					tieneFacultades = true;
					EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> TIENE FACULTAD PARA ABONO A CHEQUES", EIGlobal.NivelLog.INFO);
				}
			}
			if (tieneFacultades)
			{
				boolean dispocion = req.getParameter("nombreFlujo")!= null && req.getParameter("nombreFlujo").toString().equals("tarjetaDisposicion") ? true : false;
				if ( "0".equals(funcion_llamar) )
					{
						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try {
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								if(req.getParameter(BitaConstants.FLUJO) != null){
									bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								}else{
									bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								}

								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_ENTRA);
								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						 }


							pantalla_inicial_transferencias_pesos(cadena_getFacCOpProg, req, res );    // Cuentas Propias Moneda Nacional
					}
					else if ( "1".equals(funcion_llamar) )
					{

							strDivisa	= getFormParameter(req,"divisa");//VSWF 05/12/2009 ESC Validar parametros de campos multipart

							strTrans	= getFormParameter(req,"trans");//VSWF 05/12/2009 ESC Validar parametros de campos multipart
							pantalla_tabla_transferencias(strTrans,clave_perfil,usuario, contrato, req, res );
					}
					/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Se agrega validacion al if para la
					 * unificacion del token para MN funcion_llamar = 2 y tipo_trans = 0 */
				else if ("8".equals(funcion_llamar)
						|| ("2".equals(funcion_llamar) && ("0"
								.equals(tipo_trans) || "2".equals(tipo_trans))) && !dispocion) {
						String [] arregloctas_origen2_array = req.getParameter("arregloctas_origen2").toString().split("@");
						String [] arregloctas_destino2_array = req.getParameter("arregloctas_destino2").toString().split("@");
						String [] arregloimportes2_array = req.getParameter("arregloimportes2").toString().split("@");
						String [] arregloestatus2_array = req.getParameter("arregloestatus2").toString().split("@");
						String [] arreglofechas2_array = req.getParameter("arreglofechas2").toString().split("@");

						String arregloctas_origen2_string = "";
						String arregloctas_destino2_string = "";
						String arregloimportes2_string = "";
						String arregloestatus2_string = "";
						String arreglofechas2_string = "";

						for (int aux=0; aux<arregloestatus2_array.length; aux++) {
							if (arregloestatus2_array[aux].equals("1")) {
								arregloctas_origen2_string = arregloctas_origen2_string.concat(arregloctas_origen2_array[aux]).concat("@");
								arregloctas_destino2_string = arregloctas_destino2_string.concat(arregloctas_destino2_array[aux]).concat("@");
								arregloimportes2_string = arregloimportes2_string.concat(arregloimportes2_array[aux]).concat("@");
								arregloestatus2_string = arregloestatus2_string.concat(arregloestatus2_array[aux]).concat("@");
								arreglofechas2_string = arreglofechas2_string.concat(arreglofechas2_array[aux]).concat("@");
							}
						}

						req.setAttribute("arregloctas_origen2_string", arregloctas_origen2_string);
						req.setAttribute("arregloctas_destino2_string", arregloctas_destino2_string);
						req.setAttribute("arregloimportes2_string", arregloimportes2_string);
						req.setAttribute("arregloestatus2_string", arregloestatus2_string);
						req.setAttribute("arreglofechas2_string", arreglofechas2_string);

						strTrans	= getFormParameter(req,"trans");



						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : CADENA_VACIA;
						EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
							ValidaOTP.guardaParametrosEnSession(req);
							/**
							 * PYME 2015 Unificacion Token para contrase�a incorrecta
							 */
							req.setAttribute("vieneDe", "/enlaceMig/transferencia");
							validacionesRSA(req, res);
							return;
						}


						//interrumpe la transaccion para invocar la validacin
						boolean valBitacora = true;
						if(  valida==null)
						{
							EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccin est parametrizada para solicitar la validacin con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

							boolean solVal=ValidaOTP.solicitaValidacion(
										session.getContractNumber(),IEnlace.TRANS_INTERNAS);

						if( session.getValidarToken() &&
						    session.getFacultad(session.FAC_VAL_OTP) &&
						    session.getToken().getStatus() == 1 &&
							    solVal)
							{

								//VSWF-HGG -- Bandera para guardar el token en la bitcora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit la validacin. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
								/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
								 * Se agrega validacion para cuando es moneda nacional y pago TDC
								 */
								if( "0".equals(strTrans) || ("2".equals(strTrans)  && !("ARCHIVO".equals((String) getFormParameter(req,"TIPOTRANS")))) ){
									pantalla_valida_regs_duplicados(strTrans, req, res );
								}
								// para no mostrar nuevamente mensaje de los duplicados cuando sea nip incorrecto en token
								if( (req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull"))) &&
									(req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar"))) ){
									req.setAttribute("InfoUser", null);
									EIGlobal.mensajePorTrace( "------------Viene de Token Incorrecto---->", EIGlobal.NivelLog.DEBUG);
								}
								if( "2".equals(strTrans) &&
										"ARCHIVO".equals((String) getFormParameter(req,"TIPOTRANS")) &&
										getFormParameter(req,"contador2") != null ){
									if( Integer.parseInt(getFormParameter(req,"contador2")) <= Global.MAX_REGISTROS ){
										EIGlobal.mensajePorTrace("Entro a Carga de Favorito por Archivo para TDC", EIGlobal.NivelLog.INFO);
										favoritosParaArchivo(req, res, strTrans);
									}
								}
								/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
								ValidaOTP.guardaParametrosEnSession(req);
								sess.removeAttribute("mensajeSession");
								EIGlobal.mensajePorTrace("������������������������������ trans" + getFormParameter(req,"trans"), EIGlobal.NivelLog.INFO);
								if (sess.getAttribute(MONEDA) != null) {
									sess.setAttribute("fechaActual", ObtenDia() + "/" + ObtenMes() + "/" + ObtenAnio());
									EIGlobal.mensajePorTrace("�������������������������������� Fecha del dia ->" + sess.getAttribute("fechaActual"), EIGlobal.NivelLog.INFO);
									String tipoMoneda = (String) sess.getAttribute("moneda");
									EIGlobal.mensajePorTrace("������������������������������ Entro a MisTransferencia" + tipoMoneda, EIGlobal.NivelLog.INFO);
									System.out.println("------------------------------------------> validando tipo de moneda en transferencias : "+tipoMoneda);
									ValidaOTP.mensajeOTP(req, "MisTransferencia".concat(tipoMoneda));
								} else {
									EIGlobal.mensajePorTrace("������������������������������ Entro a TarjetaPago", EIGlobal.NivelLog.INFO);
									ValidaOTP.mensajeOTP(req, "TarjetaPago");
								}
								/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
								 * Se modica jsp al que se envia para cuando es MN y pago TDC*/
								if( "0".equals(strTrans) || ("2".equals(strTrans) && !("ARCHIVO".equals((String) getFormParameter(req,"TIPOTRANS"))))){
									ValidaOTP.validaOTP(req,res, "/jsp/transferencia_val_duplicados.jsp");
                                    								if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
									EIGlobal.mensajePorTrace(
											"\n\n ENTR BROWSER PARAMETER 453*************** \n\n"
													+ req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER),
											EIGlobal.NivelLog.DEBUG);

									req.getSession()
											.setAttribute(
													EnlaceMonitorPlusConstants.DATOS_BROWSER,
													req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
								}

								} else {
									ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
								}
								/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
							} else {
								valida="1";
								ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
								valBitacora = false;
							}
						}
						//retoma el flujo
						if( valida!=null && valida.equals("1"))
						{

							if (valBitacora)
							{
								try{

									EIGlobal.mensajePorTrace( "*************************Entro c´digo bitacorizaci´n--->", EIGlobal.NivelLog.INFO);

									HttpSession sessionBit = req.getSession(false);
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = "0";
									String concepto = " ";
									String tipoMoneda = "";
									double importeDouble = 0;
									String token = "0";
									if (req.getParameter("token") != null && !req.getParameter("token").equals(""))
									{token = req.getParameter("token");}

									if (sess.getAttribute(MONEDA) != null)
									{
										importeDouble = Double.parseDouble(sessionBit.getAttribute("importeTran").toString().trim());

										tipoMoneda = (String) sess.getAttribute(MONEDA);

										if (tipoMoneda.equals("Moneda Nacional"))
										{claveOperacion = BitaConstants.CTK_TRANSFERENCIA_MN;
										 concepto = BitaConstants.CTK_CONCEPTO_TRANSFERENCIA_MN;}

										if (tipoMoneda.equals("Dolares"))
										{claveOperacion = BitaConstants.CTK_TRANSFERENCIA_DOLARES;
										 concepto = BitaConstants.CTK_CONCEPTO_TRANSFERENCIA_DOLARES;}
									}
									else
									{
										importeDouble = Double.parseDouble(sessionBit.getAttribute("importeTran").toString().trim());
										claveOperacion = BitaConstants.CTK_PAGO_TARJETA_CREDITO;
										concepto = BitaConstants.CTK_CONCEPTO_PAGO_TARJETA_CREDITO;
									}

									sessionBit.removeAttribute("atributosBitacora");

									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------contrato--b->"+ contrato, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------usuario--b->"+ usuario, EIGlobal.NivelLog.INFO);	//
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									@SuppressWarnings("unused")
									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",importeDouble,claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

								}

							}
							else
							{
								EIGlobal.mensajePorTrace("NO ENTRA CODIGO BITACORIZACION, USUARIO SIN TOKEN -->",EIGlobal.NivelLog.ERROR);
							}
							if ((getFormParameter(req,"imptabla")).equals("NO"))
							{
								ejecucion_operaciones_transferencias_sin_tabla(strTrans,clave_perfil,usuario,	contrato, req, res );	  // Ejecuci? de transferencia
							}
							else
							{
								ejecucion_operaciones_transferencias_con_tabla(strTrans,clave_perfil,usuario, contrato, req, res );
							}
                                                    ValidaOTP.guardaBitacora((List)req.getSession().getAttribute("bitacora"),tipo_operacion);
						}

					}
					else if ( "3".equals(funcion_llamar) )
					{
							//BIT CU2071, Inicio flujo Tarjeta de Crdito Pago
						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try {
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								if(req.getParameter(BitaConstants.FLUJO) != null){
									bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								}else{
									bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								}

								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_ENTRA);
								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						 }
						 pantalla_inicial_transferencias_tarjeta(cadena_getFacCOpProg, req, res );  // Pago de Tarjeta de Crdito
					}
					else if ( "6".equals(funcion_llamar) )
					{
							//BIT CU2081, Inicio flujo Tarjeta de Crdito Disposicin

						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try {
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								if(req.getParameter(BitaConstants.FLUJO) != null){
									bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								}else{
									bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								}

								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_ENTRA);
								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						 }

						   pantalla_inicial_transferencias_tarjeta_cheques(cadena_getFacCOpProg, req, res );  // Transferencia de Tarjeta de Crdito a Cheques
					}
					else if ( "4".equals(funcion_llamar) )
					{
							//BIT CU2031, Inicio flujo Transferencias Dolares

						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try {
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								if(req.getParameter(BitaConstants.FLUJO) != null){
									bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								}else{
									bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								}

								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_ENTRA);
								if (session.getContractNumber() != null) {
									bt.setContrato(session.getContractNumber().trim());
								}

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						 }

							pantalla_inicial_transferencias_dolares(cadena_getFacCOpProg, req, res );  // Cuentas propias d?ares
					}
					else if ( "10".equals(funcion_llamar) )
					{
							EIGlobal.mensajePorTrace( "-----------Entra a funcion_llamar 10--a->", EIGlobal.NivelLog.INFO);
							pantalla_resultado_operaciones_por_archivo(getFormParameter(req,"trans"), req, res );
					}

					else if ( "2".equals(funcion_llamar) )
					{

						if (req.getParameter("nombreFlujo")!= null && req.getParameter("nombreFlujo").toString().equals("tarjetaDisposicion")) {

							String validaChallenge = req.getAttribute("challngeExito") != null
								? req.getAttribute("challngeExito").toString() : CADENA_VACIA;

							EIGlobal.mensajePorTrace("--------------->validaChallenge 2: " + validaChallenge, EIGlobal.NivelLog.DEBUG);

							if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
								EIGlobal.mensajePorTrace("--------------->Entrando challenge....2" + validaChallenge, EIGlobal.NivelLog.DEBUG);
								ValidaOTP.guardaParametrosEnSession(req);

								validacionesRSA(req, res);
								return;
							}



							boolean valBitacora = true;
								if(valida == null){


									boolean solVal=ValidaOTP.solicitaValidacion(
											session.getContractNumber(),IEnlace.TRANS_INTERNAS);


									if( session.getValidarToken() &&
											session.getFacultad(session.FAC_VAL_OTP) &&
											session.getToken().getStatus() == 1 &&
											solVal )
										{
											//VSWF-HGG -- Bandera para guardar el token en la bit­cora
											req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
											EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit´ la validaci´n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);

											ValidaOTP.guardaParametrosEnSession(req);

											req.getSession().removeAttribute("mensajeSession");
											ValidaOTP.mensajeOTP(req, "TarjetaDisposicion");

											ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);

										}else{
	                                        ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                        valida="1";
	                                        valBitacora = false;
	                                    }
								}

								//retoma el flujo
								if( valida!=null && valida.equals("1"))
								{

									if (valBitacora)
									{
										try{
											EIGlobal.mensajePorTrace( "*************************Entro c´digo bitacorizaci´n--->", EIGlobal.NivelLog.INFO);

                                            HttpSession sessionBit = req.getSession(false);
											Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
											int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
											String claveOperacion = BitaConstants.CTK_DISP_TARJETA_CREDITO;
											String concepto = BitaConstants.CTK_CONCEPTO_DISP_TARJETA_CREDITO;
											double importeDouble = 0;
											String cta_destino="0";
											String cta_cargo="0";
											String token = "0";
											if (req.getParameter("token") != null && !req.getParameter("token").equals(""))
											{token = req.getParameter("token");}

											if (tmpParametros!=null) {

												String importe = tmpParametros.get("montostring").toString();
												importeDouble = Double.valueOf(importe).doubleValue();
												 cta_destino = tmpParametros.get("destino").toString();
												 cta_destino = cta_destino.substring(0, cta_destino.indexOf(SEPARADOR_PIPE));
												 cta_cargo = tmpParametros.get("arregloctas_origen2").toString();
												 cta_cargo = cta_cargo.substring(0, cta_cargo.indexOf(SEPARADOR_PIPE));
											}
											sessionBit.removeAttribute("parametrosBitacora");

											EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "-----------cta_destino--b->"+ cta_destino, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace( "-----------cta_cargo--b->"+ cta_cargo, EIGlobal.NivelLog.INFO);

											@SuppressWarnings("unused")
											String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cta_cargo,importeDouble,claveOperacion,cta_destino,token,concepto,0);
										} catch(Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

										}

									}

									EIGlobal.mensajePorTrace("\n\nEjecutar Transaccion\n\n", EIGlobal.NivelLog.DEBUG);

									strTrans	= getFormParameter(req,"trans");
									if ( "2".equals(strTrans) ) {
										if ((getFormParameter(req,"imptabla")).equals("NO")) {
											ejecucion_operaciones_transferencias_sin_tabla(strTrans,clave_perfil,usuario,	contrato, req, res );     // Ejecuci? de transferencia
										}else
											if (req.getParameter("TDC2CHQ") == null || !(getFormParameter(req,"TDC2CHQ")).equals("1")) {

												pantalla_valida_regs_duplicados(strTrans, req, res );
											} else {
												ejecucion_operaciones_transferencias_con_tabla(strTrans,clave_perfil,usuario, contrato, req, res );
											}
									}
									else
									{
										pantalla_valida_regs_duplicados(strTrans, req, res );
									}
								}

						} else {

							EIGlobal.mensajePorTrace("\n\nEjecutar Transaccion\n\n", EIGlobal.NivelLog.DEBUG);


							strTrans	= getFormParameter(req,"trans");
							if ( strTrans.equals("2") )
							{
								if ((getFormParameter(req,"imptabla")).equals("NO"))
								{
									ejecucion_operaciones_transferencias_sin_tabla(strTrans,clave_perfil,usuario,	contrato, req, res );
								}
								else if (req.getParameter("TDC2CHQ") == null || !(getFormParameter(req,"TDC2CHQ")).equals("1"))
								{

									pantalla_valida_regs_duplicados(strTrans, req, res );
								}
								else
								{
									ejecucion_operaciones_transferencias_con_tabla(strTrans,clave_perfil,usuario, contrato, req, res );
								}
							}
							else
							{

								pantalla_valida_regs_duplicados(strTrans, req, res );
							}
						}
					}
			}
			else
			{

		String laDireccion = "document.location='SinFacultades'";
		req.setAttribute( "plantillaElegida", laDireccion);
		req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
	    }
		}
		else
			{
				EIGlobal.mensajePorTrace("***transferencia.defaultAction()-> NO TIENE FACULTAD PARA ABONO A CHEQUES NI DISPOSICION", EIGlobal.NivelLog.INFO);
		String laDireccion = "document.location='SinFacultades'";
		req.setAttribute( "plantillaElegida", laDireccion);
		req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
	    }
	}


	/**
	 * Pantalla_inicial_transferencias_pesos.
	 *
	 * @param cadena_getFacCOpProg the cadena_get fac c op prog
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int pantalla_inicial_transferencias_pesos(boolean cadena_getFacCOpProg, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***transferencia.pantalla_inicial_transferencias_pesos()-> Entrando", EIGlobal.NivelLog.DEBUG);

		boolean fac_programadas;

		String campos_fecha	= "";

		String strInhabiles = "";
		String fecha_hoy	= "";

		String NoRegistradas= "";
		String divisa		= "";

		boolean facCtasNoRegCheq;


		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");

		verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",true);

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			EIGlobal.mensajePorTrace( "Transferencias Internas - Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
        }
		else{
			EIGlobal.mensajePorTrace( "Transferencias Internas - La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);}




		facCtasNoRegCheq = session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG);


		sess.setAttribute("TIPO_DIVISA", "PESOS");



		strInhabiles = armaDiasInhabilesJS();
		fac_programadas = cadena_getFacCOpProg;

		//jgal - 10-11-2010-Se eliminda validacion de facultad de No Registradas
		NoRegistradas		= cuentasNoRegistradas(false);
		campos_fecha		= poner_campos_fecha(fac_programadas);
		fecha_hoy			= ObtenFecha();


			req.setAttribute("strDebug", strDebug);
			req.setAttribute("NoRegistradas", NoRegistradas);
			req.setAttribute("diasInhabiles", strInhabiles);
			req.setAttribute("tipoDivisa", divisa );
			req.setAttribute("arregloctas_origen1","");
			req.setAttribute("arregloctas_destino1","");
			req.setAttribute("arregloimportes1","");
			req.setAttribute("arreglofechas1","");
			req.setAttribute("arregloconceptos1","");
		    req.setAttribute("arreglorfcs1","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas1",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus1","");
			req.setAttribute("arregloctas_origen2","");
			req.setAttribute("arregloctas_destino2","");
			req.setAttribute("arregloimportes2","");
			req.setAttribute("arreglofechas2","");
			req.setAttribute("arregloconceptos2","");
			req.setAttribute("arreglorfcs2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas2",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus2","");
			req.setAttribute("contador1","0");
			req.setAttribute("contador2","0");
			req.setAttribute("tabla","");
			req.setAttribute("fecha_hoy",fecha_hoy);
			//req.setAttribute("ContUser",ObtenContUser());
			req.setAttribute("fecha1",ObtenFecha(true));
			req.setAttribute("campos_fecha",campos_fecha);
			req.setAttribute("calendario_normal", poner_calendario_programadas(fac_programadas,"fecha_completa","WindowCalendar()"));
			req.setAttribute("TIPOTRANS","LINEA");
			req.setAttribute("INICIAL","SI");
			req.setAttribute("IMPORTACIONES","0");
			req.setAttribute("NMAXOPER"," "+nmaxoper);

			req.setAttribute("divisa","Pesos");
			req.setAttribute("trans","0");

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras en M.N.","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s25360h",req));


			if (fac_programadas==false){
				req.setAttribute("fac_programadas1","0");}
			else{
				req.setAttribute("fac_programadas1","1");}
			session.setModuloConsultar(IEnlace.MCargo_transf_pesos);


			evalTemplate(IEnlace.MIS_TRANS_TMPL, req, res);

		return 1;
	}  // fin mtodo

	/**
	 * Pantalla_inicial_transferencias_dolares.
	 *
	 * @param cadena_getFacCOpProg the cadena_get fac c op prog
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int pantalla_inicial_transferencias_dolares(boolean cadena_getFacCOpProg,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace("***transferencia.pantalla_inicial_transferencias_dolares()->Entrando", EIGlobal.NivelLog.DEBUG);


		boolean fac_programadas;

		String campos_fecha		= "";

		String strInhabiles		= "";
		String fecha_hoy		="";

		String divisa			= "" ;

		String NoRegistradas	= "";

		boolean facCtasNoRegCheq	;


		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");

		verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",true);

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			EIGlobal.mensajePorTrace( "Transferencias Internas - Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
        }
		else{
			EIGlobal.mensajePorTrace( "Transferencias Internas - La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);}


		facCtasNoRegCheq = session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG);

		strInhabiles = armaDiasInhabilesJS();

		fac_programadas=cadena_getFacCOpProg;


		fecha_hoy=ObtenFecha();
		campos_fecha=poner_campos_fecha(fac_programadas);


		NoRegistradas		= cuentasNoRegistradas(false);

			req.setAttribute("strDebug", strDebug);
			req.setAttribute("NoRegistradas", NoRegistradas);
			req.setAttribute("diasInhabiles", strInhabiles);
			req.setAttribute("tipoDivisa", divisa );
		req.setAttribute("arregloctas_origen1","");
		req.setAttribute("arregloctas_destino1","");
		req.setAttribute("arregloimportes1","");
			req.setAttribute("arreglofechas1","");
			req.setAttribute("arregloconceptos1","");
			req.setAttribute("arreglorfcs1",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas1","");//*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus1","");
			req.setAttribute("arregloctas_origen2","");
			req.setAttribute("arregloctas_destino2","");
			req.setAttribute("arregloimportes2","");
			req.setAttribute("arreglofechas2","");
			req.setAttribute("arreglorfcs2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas2",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arregloconceptos2","");
			req.setAttribute("arregloestatus2","");
			req.setAttribute("contador1","0");
			req.setAttribute("contador2","0");
			req.setAttribute("tabla","");
			req.setAttribute("fecha_hoy",fecha_hoy);
			//req.setAttribute("ContUser",ObtenContUser());
			req.setAttribute("fecha1",ObtenFecha(true));
			req.setAttribute("campos_fecha",campos_fecha);
			req.setAttribute("calendario_normal", poner_calendario_programadas(fac_programadas,"fecha_completa","WindowCalendar()"));
			req.setAttribute("TIPOTRANS","LINEA");
			req.setAttribute("INICIAL","SI");
			req.setAttribute("IMPORTACIONES","0");
			req.setAttribute("NMAXOPER"," "+nmaxoper);
			req.setAttribute("divisa","Dolares");
			req.setAttribute("trans","1");

			//ESC VULNERABILIDADES ENLACE
			sess.setAttribute("TIPO_DIVISA", "DOLARES");

			if (fac_programadas==false){
				req.setAttribute("fac_programadas1","0");}
			else{
				req.setAttribute("fac_programadas1","1");}



			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  D&oacute;lares","s25400h",req));

	   session.setModuloConsultar(IEnlace.MCargo_transf_dolar);

			evalTemplate(IEnlace.MIS_TRANS_TMPL, req, res);
		return 1;
	}


	/**
	 * Pantalla_inicial_transferencias_tarjeta.
	 *
	 * @param cadena_getFacCOpProg the cadena_get fac c op prog
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int pantalla_inicial_transferencias_tarjeta(boolean cadena_getFacCOpProg, HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("***transferencia.pantalla_inicial_transferencias_tarjeta()-> Entrando", EIGlobal.NivelLog.DEBUG);

		boolean fac_programadas;
		String campos_fecha  = "";
		String divisa	     = "";

		String strInhabiles  = "";
		String fecha_hoy     = "";

		String NoRegistradas = "";
		boolean  facTarNoReg;

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",true);

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			EIGlobal.mensajePorTrace( "Transferencias Internas - Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
        }
		else{
			EIGlobal.mensajePorTrace( "Transferencias Internas - La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);}


		facTarNoReg = session.getFacultad(session.FAC_TRANSF_TARJ_NOREG);
		strInhabiles = armaDiasInhabilesJS();
		fac_programadas=cadena_getFacCOpProg;


		fecha_hoy=ObtenFecha();

		campos_fecha=poner_campos_fecha(fac_programadas);
		NoRegistradas		= cuentasNoRegistradas(facTarNoReg);

			req.setAttribute("strDebug", strDebug);
			req.setAttribute("NoRegistradas", NoRegistradas);
			req.setAttribute("diasInhabiles", strInhabiles);
			req.setAttribute("tipoDivisa", divisa );
			req.setAttribute("arregloctas_origen1","");
			req.setAttribute("arregloctas_destino1","");
			req.setAttribute("arregloimportes1","");
			req.setAttribute("arreglofechas1","");
			req.setAttribute("arregloconceptos1","");
		    req.setAttribute("arreglorfcs1","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas1",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus1","");
			req.setAttribute("arregloctas_origen2","");
			req.setAttribute("arregloctas_destino2","");
			req.setAttribute("arregloimportes2","");
			req.setAttribute("arreglofechas2","");
			req.setAttribute("arregloconceptos2","");
			req.setAttribute("arreglorfcs2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus2","");
			req.setAttribute("contador1","0");
			req.setAttribute("contador2","0");
			req.setAttribute("tabla","");
			req.setAttribute("fecha_hoy",fecha_hoy + " - 06");
			req.setAttribute("ContUser",ObtenContUser(req));
			req.setAttribute("fecha1",ObtenFecha(true));
			req.setAttribute("campos_fecha",campos_fecha);
			req.setAttribute("calendario_normal", poner_calendario_programadas(fac_programadas,"fecha_completa","WindowCalendar()"));
			req.setAttribute("TIPOTRANS","LINEA");
			req.setAttribute("INICIAL","SI");
			req.setAttribute("IMPORTACIONES","0");
			req.setAttribute("NMAXOPER"," "+nmaxoper);
			req.setAttribute("divisa","Pesos");
			req.setAttribute("trans","2");

			if (fac_programadas==false){
				req.setAttribute("fac_programadas1","0");}
			else{
				req.setAttribute("fac_programadas1","1");}



			req.setAttribute("MenuPrincipal", session.getStrMenu());
		    req.setAttribute("newMenu", session.getFuncionesDeMenu());
		    req.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito","s25520h",req));

	    session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

			evalTemplate(IEnlace.MIS_TRANS_TARJETA_TMPL, req, res);

		return 1;
	} // mtodo

	/**
	 * Pantalla_inicial_transferencias_tarjeta_cheques.
	 *
	 * @param cadena_getFacCOpProg the cadena_get fac c op prog
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public int pantalla_inicial_transferencias_tarjeta_cheques(boolean cadena_getFacCOpProg, HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("***transferencia.pantalla_inicial_transferencias_tarjeta_cheques()-> Entrando", EIGlobal.NivelLog.DEBUG);
		boolean fac_programadas;
		String campos_fecha  = "";
		String divisa	     = "";
		String strInhabiles  = "";
		String fecha_hoy     = "";

		String NoRegistradas = "";
		boolean  facTarNoReg;
		//Modif PVA 10/10/2002
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",true);

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			EIGlobal.mensajePorTrace( "Transferencias Internas - Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
        }
		else{
			EIGlobal.mensajePorTrace( "Transferencias Internas - La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);}


		facTarNoReg = session.getFacultad(session.FAC_TRANSF_TARJ_NOREG);
		strInhabiles = armaDiasInhabilesJS();
		fac_programadas=cadena_getFacCOpProg;
		fac_programadas=false;

		fecha_hoy=ObtenFecha();

		campos_fecha=poner_campos_fecha(fac_programadas);
		NoRegistradas		= cuentasNoRegistradas(facTarNoReg);

			req.setAttribute("strDebug", strDebug);
			req.setAttribute("NoRegistradas", NoRegistradas);
			req.setAttribute("diasInhabiles", strInhabiles);
			req.setAttribute("tipoDivisa", divisa );
			req.setAttribute("arregloctas_origen1","");
			req.setAttribute("arregloctas_destino1","");
			req.setAttribute("arregloimportes1","");
			req.setAttribute("arreglofechas1","");
			req.setAttribute("arregloconceptos1","");
		    req.setAttribute("arreglorfcs1","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas1",""); //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus1","");
			req.setAttribute("arregloctas_origen2","");
			req.setAttribute("arregloctas_destino2","");
			req.setAttribute("arregloimportes2","");
			req.setAttribute("arreglofechas2","");
			req.setAttribute("arregloconceptos2","");
			req.setAttribute("arreglorfcs2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arreglorivas2","");  //*****Cambio para comprobante fiscal
			req.setAttribute("arregloestatus2","");
			req.setAttribute("contador1","0");
			req.setAttribute("contador2","0");
			req.setAttribute("tabla","");
			req.setAttribute("fecha_hoy",fecha_hoy + " - 06");
			req.setAttribute("ContUser",ObtenContUser(req));
			req.setAttribute("fecha1",ObtenFecha(true));
			req.setAttribute("campos_fecha",campos_fecha);
			req.setAttribute("calendario_normal", poner_calendario_programadas(fac_programadas,"fecha_completa","WindowCalendar()"));
			req.setAttribute("TIPOTRANS","LINEA");
			req.setAttribute("INICIAL","SI");
			req.setAttribute("IMPORTACIONES","0");
			req.setAttribute("NMAXOPER"," "+nmaxoper);
			req.setAttribute("divisa","Pesos");
			req.setAttribute("trans","2");
			req.setAttribute("TDC2CHQ","1");

			if (fac_programadas==false){
				req.setAttribute("fac_programadas1","0");}
			else{
				req.setAttribute("fac_programadas1","1");}



			req.setAttribute("MenuPrincipal", session.getStrMenu());
		    req.setAttribute("newMenu", session.getFuncionesDeMenu());
		    req.setAttribute("Encabezado",CreaEncabezado("Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n","s40010h",req));

	    session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

			evalTemplate(IEnlace.MIS_TRANS_TARJETA_TMPL, req, res);

		return 1;
	} // mtodo

	/**
	 * S formatea importe.
	 *
	 * @param importe the importe
	 * @return the string
	 */
String sFormateaImporte( String importe)
{
	return importe;
}



/**
 * Pantalla_tabla_transferencias.
 *
 * @param strTrans the str trans
 * @param clave_perfil the clave_perfil
 * @param usuario the usuario
 * @param contrato the contrato
 * @param req the req
 * @param res the res
 * @return the int
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
public int pantalla_tabla_transferencias(String strTrans,String clave_perfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
throws ServletException, IOException
{
	String tdc2chq = "";
	if ( getFormParameter(req,"TDC2CHQ")!=null )
	{
		tdc2chq = getFormParameter(req,"TDC2CHQ");
	}

	EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->Entrandos", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->TDC2CHQ["+tdc2chq+"]", EIGlobal.NivelLog.DEBUG);
	StringBuffer tabla = new StringBuffer();
	String	arrctas_origen		= "";
	String	arrctas_destino 	= "";
	String	arrimporte			= "";
	String	arrconceptos		= "";
	String	arrestatus			= "";
	String	arrfechas			= "";
	String	cadena_cta_origen	= "";
	String	cadena_cta_destino	= "";
	String	cta_origen			= "";
	String	cta_destino			= "";
	String	scontador			= "";
	String	simporte			= "";
	String	concepto			= "";
	String	fecha				= "";
	String	nombre_campo		= "";
	String	fecha_hoy			= "";
	String	valor				= "";
	String	sfac_programadas	= "";
	String	campos_fecha		= "";
	String	strArchivo			= "";
	String	chkvalor			= "";

	String	arrrfcs 	    = "";
	String	arrivas 	    = "";
	String	iva		    = "";
	String	rfc		    = "";

	boolean facCtasNoReg;
	double importetabla = 0.0;
	int	contador			= 0;
	//int	salida				= 0;
	int	indice				= 0;
	boolean fac_programadas;
	transferenciaUPL mitrans;
	boolean archivo_sin_error=true;
	double importearchivo=0.0;


		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");
	String fileOut="";
	fileOut = session.getUserID8() + "tr.doc";
	boolean grabaArchivo = true;
	String cadena = "";

	EI_Exportar ArcSalT;
	ArcSalT = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
	EIGlobal.mensajePorTrace( "***Se verificar­ que el archivo no exista en caso contrario se borrara y crear­ uno nuevo", EIGlobal.NivelLog.DEBUG);
	ArcSalT.creaArchivo();


		String desctanoreg="";

		if ( strTrans.equals("0")|| strTrans.equals("1")){
		  facCtasNoReg	    = session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG);}
		else{
		  facCtasNoReg		= session.getFacultad(session.FAC_TRANSF_TARJ_NOREG);}


	//ESC 02/JUN/2010 VALIDACION DE CUENTAS NO REGISTRADAS
	sess.removeAttribute("TIPO_CUENTA_TRAN");
	sess.removeAttribute("FAC_CUENTA_NR");

		chkvalor = getFormParameter(req,"ChkOnline");//VSWF ESC 05/02/2008 Validar con formularios multipart
		if(chkvalor.trim().equals("OnNoReg"))
		{
			cadena_cta_origen  =getFormParameter(req,"origen");
			cadena_cta_destino = getFormParameter(req,"destinoNoReg");
			desctanoreg=ValidaCuentaNoReg(usuario,clave_perfil,contrato,cadena_cta_destino, req);
		if(cadena_cta_destino.length() == 16 && !esCuentaValida(trimCuenta(cadena_cta_destino))) {
			req.setAttribute("ArchivoErr","cuadroDialogo('Cuenta inv&aacute;lida', 3);");
		}
		else if (desctanoreg.length()>0) {
		    cadena_cta_destino=cadena_cta_destino+"|NR|"+desctanoreg+"|";
			//ESC 02/JUN/2010 VALIDACION DE CUENTAS NO REGISTRADAS
			sess.setAttribute("TIPO_CUENTA_TRAN", "NR");
			EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->FACULTAD CUENTAS NO REGISTRADAS " + facCtasNoReg, EIGlobal.NivelLog.INFO);
			if(facCtasNoReg){
				sess.setAttribute("FAC_CUENTA_NR", "SI");
			}else{
				sess.setAttribute("FAC_CUENTA_NR", "NO");
			}

		}

			simporte	   = getFormParameter(req,"monto");
			concepto	   = getFormParameter(req,"concepto");
			fecha		   = getFormParameter(req,"fecha_completa");
		}
	else if(chkvalor.trim().equals("OnTdC")) //MPP 25052004 Q1364
	{
		cadena_cta_origen  = getFormParameter(req,"origen");

			cadena_cta_destino = getFormParameter(req,"TdC");
		simporte	   = getFormParameter(req,"monto");
		concepto	   = getFormParameter(req,"concepto");
		fecha		   = getFormParameter(req,"fecha_completa");
	}
	if (chkvalor.trim().equals("OnLine")) //MPP 25052004 Q1364
		{
			cadena_cta_origen  = getFormParameter(req,"origen");
			cadena_cta_destino = getFormParameter(req,"destino");
			simporte	   = getFormParameter(req,"monto");
			concepto	   = getFormParameter(req,"concepto");
			fecha		   = getFormParameter(req,"fecha_completa");
	}   //MPP 25052004 Q1364 Fin

	concepto=concepto.toUpperCase();

		arrctas_origen	   = getFormParameter(req,"arregloctas_origen1");
		arrctas_destino    = getFormParameter(req,"arregloctas_destino1");
		arrimporte	   = getFormParameter(req,"arregloimportes1");
		arrconceptos	   = getFormParameter(req,"arregloconceptos1");
		arrrfcs 	   = getFormParameter(req,"arreglorfcs1");
		arrivas 	   = getFormParameter(req,"arregloivas1");

		if(arrrfcs==null){	arrrfcs="";}
		if(arrivas==null){	arrivas="";}

	if (getFormParameter(req,"CHRCF").equals("1"))
	    {
			iva=getFormParameter(req,"IVA");
		     rfc=getFormParameter(req,"RFC").toUpperCase();

	}
		else
		{
	     iva="";
	     rfc="";
	}

		req.setAttribute("arregloivas1","\""+arrivas+"\"");
		req.setAttribute("arregloivas2","\""+arrivas+"\"");
		req.setAttribute("arreglorfcs1","\""+arrrfcs+"\"");
		req.setAttribute("arreglorfcs2","\""+arrrfcs+"\"");

		arrfechas	   = getFormParameter(req,"arreglofechas1");
		arrestatus	   = getFormParameter(req,"arregloestatus1");

		EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->marca 0", EIGlobal.NivelLog.DEBUG);


		sfac_programadas   = getFormParameter(req,"fac_programadas1");

		try
		{
			strArchivo	 = new String(getFileInBytes( req ));
		}
		catch(Exception e)
		{EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			strArchivo = "";
		}

		    if(chkvalor.trim().equals("OnNoReg") && concepto.trim().length()!=0) {
			String tipoDEST = ObtenTipoCuenta2(cadena_cta_destino, strTrans, true, session, req);
			  if(tipoDEST != null && tipoDEST.equals("TARJCRED")) {
			       	  cadena_cta_destino = trimCuenta(cadena_cta_destino)+"|NR||";
			  }
		}

		//AQUI VALIDAR TIPOS DE CUENTA//
		if (concepto.trim().length()==0)
		{
		    if	 ((chkvalor.trim().equals("OnNoReg"))|| (chkvalor.trim().equals("OnLine"))) {
		      String tipoDEST = ObtenTipoCuenta2(cadena_cta_destino, strTrans, true, session, req);

			  if(tipoDEST != null && tipoDEST.equals("TARJCRED")) {
			       concepto="PAGO TARJETA DE CREDITO";
			       if(chkvalor.trim().equals("OnNoReg")) {
			       	  cadena_cta_destino = trimCuenta(cadena_cta_destino)+"|NR||";
				   }
			  }
			  else {
			       concepto="TRANSFERENCIA EN EFECTIVO";
			   }
			}
			else{ concepto="PAGO TARJETA DE CREDITO";}
		}


		EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->marca 1", EIGlobal.NivelLog.DEBUG);



		if ( strTrans.equals("0")){  //Cuentas en pesos
			session.setModuloConsultar(IEnlace.MCargo_transf_pesos);}
		else if (strTrans.equals("1")){ //cuentas en dolar
			session.setModuloConsultar(IEnlace.MCargo_transf_dolar);}
		else if (strTrans.equals("2")){ //cargo para pago de tarjeta
			session.setModuloConsultar(IEnlace.MCargo_transf_pesos);}


		EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->marca 2", EIGlobal.NivelLog.DEBUG);

		if (!((String) getFormParameter(req,"TIPOTRANS")).equals("ARCHIVO"))
		{
				//*******************************************************************************");
				//*******************************ES TRANSFERENCIA EN LINEA *****************");
				//*******************************************************************************");
				EIGlobal.mensajePorTrace("transferencia.pantalla_tabla_transferencias(): Es transferencia en linea", EIGlobal.NivelLog.DEBUG);
				scontador=getFormParameter(req,"contador1");
				contador=Integer.parseInt(scontador);
				if(chkvalor.trim().equals("OnNoReg"))
				{

					if (desctanoreg.length()>0)
					{
					  arrctas_origen=arrctas_origen+cadena_cta_origen+"@";
					  arrctas_destino=arrctas_destino+cadena_cta_destino+"@";
					  arrconceptos=arrconceptos+concepto+"@";
					  arrimporte=arrimporte+simporte+"@";
					  	if(arrivas!=null && arrivas != ""){
					  arrivas	   +=iva+"@";}
					if(arrrfcs!=null && arrrfcs != ""){
					  	arrrfcs	   +=rfc+"@";}
					  arrfechas=arrfechas+fecha+"@";
					  arrestatus=arrestatus+"1@";
					  contador++;
					}
					else{
					   req.setAttribute("ArchivoErr","cuadroDialogo('Cuenta no est&aacute; registrada', 3);");}
				}
				else
				{
					  arrctas_origen=arrctas_origen+cadena_cta_origen+"@";
					  arrctas_destino=arrctas_destino+cadena_cta_destino+"@";
					  arrconceptos=arrconceptos+concepto+"@";
					  arrimporte=arrimporte+simporte+"@";

					  if(arrivas!=null && arrivas != ""){
						  arrivas	   +=iva+"@";}
					  if(arrrfcs!=null && arrrfcs != ""){
						  arrrfcs	   +=rfc+"@";}
					  arrfechas=arrfechas+fecha+"@";
					  arrestatus=arrestatus+"1@";
					  contador++;
				}


				String arrconceptosprueba="\""+arrconceptos+"\"";
				String arrctas_origenprueba="\""+arrctas_origen+"\"";
				String arrctas_destinoprueba="\""+arrctas_destino+"\"";

				req.setAttribute("arregloctas_origen1",arrctas_origenprueba);
				req.setAttribute("arregloctas_destino1",arrctas_destinoprueba);
				req.setAttribute("arregloimportes1",arrimporte);
				req.setAttribute("arregloconceptos1",arrconceptosprueba);
				req.setAttribute("arreglofechas1",arrfechas);
				req.setAttribute("arregloestatus1",arrestatus);
				req.setAttribute("contador1",Integer.toString(contador));
				req.setAttribute("contador2",Integer.toString(contador));
		if(arrivas!=null && arrivas != "")
				if(arrivas!=null&&!arrivas.trim().equals(""))	req.setAttribute("arregloivas1","\""+arrivas+"\"");
		if(arrrfcs!=null && arrrfcs != "")
				if(arrrfcs!=null&&!arrrfcs.trim().equals(""))	req.setAttribute("arreglorfcs1","\""+arrrfcs+"\"");




		}
		else
		{
				//*******************************************************************************");
				//*******************************ES TRANSFERENCIA POR ARCHIVO	 ***************");
				//*******************************************************************************");

			EIGlobal.mensajePorTrace("transferencia.pantalla_tabla_transferencias(): Es transferencia por archivo", EIGlobal.NivelLog.INFO);

			StringBuffer arrctas_origen2=new StringBuffer("");
			StringBuffer arrctas_destino2=new StringBuffer("");
			StringBuffer arrconceptos2=new StringBuffer("");
			StringBuffer arrimporte2=new StringBuffer("");
			StringBuffer arrfechas2=new StringBuffer("");
			StringBuffer arrivas2=new StringBuffer("");
			StringBuffer arrrfcs2=new StringBuffer("");
			StringBuffer arrestatus2=new StringBuffer("");

			String tipo_cta_abono	= "";
			String tipo_prod_cargo	= "";



			String tipo_cta_cargo	= "";
			String tipo_prod_abono	= "";

			String datoscta[]=null;

			String Cargo01 ="";
			String Abono01 ="";
			boolean ctavalida=true;

			int transerr	= 0;
			String linea	= "";
			int cont = 0;


			BufferedReader strOut =null;
			ValidaCuentaContrato valCtas = null;
			try
			{
				//****************** INICIA TCS FSW 12/16 ******************** 
				String sFile = getFormParameter2(req,"fileName");
				String extension = null;
				EIGlobal.mensajePorTrace("TCSFSW ::::::::  Archivo a validar... -----------> .: " + sFile, EIGlobal.NivelLog.INFO);
				
				int i = sFile.lastIndexOf('.');
				if (i >= 0) {
					extension = sFile.substring(i+1);
				}
				
				EIGlobal.mensajePorTrace("TCSFSW ::::::::  Extencion del archivo... ----------->" + extension, EIGlobal.NivelLog.INFO);
				if (!("txt").equals(extension)) {
					EIGlobal.mensajePorTrace("TCSFSW ::::::::  Extencion diferente a txt....-----------> " + extension, EIGlobal.NivelLog.INFO);
					throw new Exception (" Extencion diferente a txt....");
				} 
				//****************** FIN TCS FSW 12/16 ***********************
			
			
			
				strOut = new BufferedReader(new StringReader(strArchivo));
				valCtas = new ValidaCuentaContrato();

				while((linea = strOut.readLine()) != null)
				{

					if( linea.trim().equals("")){
						linea = strOut.readLine();}
					if( linea == null ){
						break;}

					cont++;
					mitrans = new transferenciaUPL(linea,getFormParameter(req,"CHRCF"));

					EIGlobal.mensajePorTrace("transferencia.pantalla_tabla_transferencias(): CHRCF ="+getFormParameter(req,"CHRCF"), EIGlobal.NivelLog.DEBUG);

					tipo_cta_cargo = "";
					tipo_cta_abono = "";
					Cargo01 = new Long(mitrans.Cuenta_Cargo).toString();
					Abono01 = new Long(mitrans.Cuenta_Abono).toString();

					EIGlobal.mensajePorTrace("***transferencia.Pantalla_tabla_transferencias:  mitrans.ImporteCadena ="+mitrans.ImporteCadena, EIGlobal.NivelLog.DEBUG);

				importearchivo=importearchivo+new Double(mitrans.Importe).doubleValue();

					if (mitrans.err == 0)
					{
			tipo_cta_abono="";
						tipo_cta_cargo="";
						tipo_prod_abono="";
						tipo_prod_cargo="";
						EIGlobal.mensajePorTrace("transferencia.pantalla_tabla_transferencias()->inicio Modificacion para integracion", EIGlobal.NivelLog.DEBUG);

					if (strTrans.equals("0") || strTrans.equals("2")){
						datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																  session.getUserID8(),
														          session.getUserProfile(),
														          Cargo01.trim(),
														          IEnlace.MCargo_transf_pesos);
					}else  if  (strTrans.equals("1")){
						datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
														  		  session.getUserID8(),
														          session.getUserProfile(),
														          Cargo01.trim(),
														          IEnlace.MCargo_transf_dolar);
					}

String cuentaC = "";

						if(datoscta!=null)
						{
							Cargo01=datoscta[1];
							mitrans.Cuenta_Cargo=Long.parseLong(datoscta[1]);
							cuentaC = datoscta[4]; //MSD
			    tipo_cta_cargo=datoscta[2];
							tipo_prod_cargo=datoscta[3];

			}

					if (strTrans.equals("0") || strTrans.equals("2")){
						datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																  session.getUserID8(),
														          session.getUserProfile(),
														          Abono01.trim(),
														          IEnlace.MAbono_transf_pesos);
					}else  if  (strTrans.equals("1")){
						datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																  session.getUserID8(),
														          session.getUserProfile(),
														          Abono01.trim(),
														          IEnlace.MAbono_transf_dolar);
					}

String tipoABONO = "DESCONOCIDO";
String cuentaA = "";
						EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()->valor de facCtasNoReg:"+facCtasNoReg, EIGlobal.NivelLog.INFO);
						if(datoscta!=null)
						{
							tipoABONO = datoscta[7];
							Abono01=datoscta[1];
							mitrans.Cuenta_Abono=Long.parseLong(datoscta[1]);
							cuentaA = datoscta[4]; //MSD
			    			tipo_cta_abono=datoscta[2];
							tipo_prod_abono=datoscta[3];

			}

						else if(facCtasNoReg)
			{
							ctavalida=false;

						EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()-> FACULTA ABONO NO REG CTA: " + Abono01, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()-> sigo validando porque tengo facultad a abono no reg", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()-> Proyecto unico integracion Santander-serfin .", EIGlobal.NivelLog.DEBUG);

							if (Abono01.trim().length() == 11 && validaDigitoCuenta(Abono01.trim()))
							   {
 							     EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()-> la cuenta es altair", EIGlobal.NivelLog.DEBUG);
							     ctavalida=true;

							   }
							else if(Abono01.trim().length() == 16 && esCuentaValida(Abono01.trim())) {
								String tipoDEST = ObtenTipoCuenta2(Abono01.trim(), strTrans, false, session, req);
								if(tipoDEST != null) {
									ctavalida = true;
									tipoABONO = tipoDEST;
								}

							}
							else {
 								transerr++;
								archivo_sin_error=false;
								req.setAttribute("ArchivoErr","cuadroDialogo('Cuenta inv&aacute;lida', 3);");
							}


							if(ctavalida)
			    {

							   tipo_cta_abono="NR";
							   tipo_prod_abono=Abono01.substring(0, 2);
							   if (((tipo_prod_abono.equals("82")) ||
				    (tipo_prod_abono.equals("83")))||
								   ((tipo_prod_abono.equals("49"))&&(Abono01.trim().length()==11))){
								  tipo_prod_abono="6";}
							   else{
				  tipo_prod_abono="1";}

						    }
						}
						else {

									   transerr++;
									   archivo_sin_error=false;
									req.setAttribute("ArchivoErr",
											"cuadroDialogo(\"Cuenta de abono/M&oacute;vil inv&aacute;lida, l&iacute;nea:"
													+ cont + "\", 3);");
									   break;
						}

						EIGlobal.mensajePorTrace("***transferencia..pantalla_tabla_transferencias()->fin Modificacion para integracion", EIGlobal.NivelLog.DEBUG);

						int fecha_correcta=checar_fecha(mitrans.Fecha);
						if(tipo_cta_abono.trim().length() != 0 && tipo_cta_cargo.trim().length() != 0 && !Abono01.trim().equals(Cargo01.trim()))
						{
							if  ((fecha_correcta!=-1)&&(fecha_correcta!=-2))
			    {

								if (strTrans.equals("1")) // si se trata de cuentas en d?ares
								{
								   // es cuenta en d?ares
								   if ( tipo_prod_cargo.equals("6") && tipo_prod_abono.equals("6") )
								   {

										arrctas_origen2.append(mitrans.Cuenta_Cargo + " " + cuentaC);
										arrctas_origen2.append("|");	
										arrctas_origen2.append(tipo_cta_cargo); 	
										arrctas_origen2.append("|@");

										arrctas_destino2.append(mitrans.Cuenta_Abono + " " + cuentaA);	
										arrctas_destino2.append("|");	
										arrctas_destino2.append(tipo_cta_abono);	
										arrctas_destino2.append("|@");

										arrconceptos2.append(mitrans.Concepto); 
										arrconceptos2.append(SEPARADOR_CHR_ARROBA);

										arrimporte2.append(mitrans.ImporteCadena); 
										arrimporte2.append(SEPARADOR_CHR_ARROBA);

									    arrfechas2.append(mitrans.Fecha); 
									    arrfechas2.append(SEPARADOR_CHR_ARROBA);

									  try{

										  float ftemp = Float.parseFloat(mitrans.iva.trim());

										  arrivas2.append(String.valueOf(ftemp)); 
										  arrivas2.append(SEPARADOR_CHR_ARROBA);
									 }catch (Exception e) {
											arrivas2.append(""); 
											arrivas2.append(SEPARADOR_CHR_ARROBA);
											EIGlobal.mensajePorTrace("transferencia..pantalla_tabla_transferencias()->PROBLEMA AL INTENTAR LEER EL IVA",EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}


									  arrrfcs2.append(mitrans.rfc.trim()); 
									  arrrfcs2.append(SEPARADOR_CHR_ARROBA);

									  arrestatus2.append("1@");

								    }
									else
									{
									   transerr++;
									   archivo_sin_error=false;
									   req.setAttribute("ArchivoErr","cuadroDialogo(\"S&oacute;lo se aceptan cuentas en d&oacute;lares, l&iacute;nea:"+cont+"\", 3);");
									   break;
								    }
							    }
								else
								{

				    boolean esTDC = false;

				    if (tipoABONO.indexOf("TARJCRED") != -1)
					esTDC = true;

									if(strTrans.equals("2") && !esTDC) {
									   transerr++;
									   archivo_sin_error=false;
									   req.setAttribute("ArchivoErr","cuadroDialogo(\"S&oacute;lo se aceptan pagos de TDC, l&iacute;nea:"+cont+"\", 3);");
									   break;
									}

									else if (((!tipo_prod_cargo.equals("6")) && (!tipo_prod_abono.equals("6"))))
									{

										arrctas_origen2.append(mitrans.Cuenta_Cargo + " " + cuentaC);	
										arrctas_origen2.append("|");	
										arrctas_origen2.append(tipo_cta_cargo); 
										arrctas_origen2.append("|@");

										arrctas_destino2.append(mitrans.Cuenta_Abono + " " + cuentaA);	
										arrctas_destino2.append("|");	
										arrctas_destino2.append(tipo_cta_abono); 
										arrctas_destino2.append("|");	
										arrctas_destino2.append(tipoABONO);	
										arrctas_destino2.append("|@");

										arrconceptos2.append(mitrans.Concepto); 
										arrconceptos2.append(SEPARADOR_CHR_ARROBA);

										arrimporte2.append(mitrans.ImporteCadena); 
										arrimporte2.append(SEPARADOR_CHR_ARROBA);

										arrfechas2.append(mitrans.Fecha); 
										arrfechas2.append(SEPARADOR_CHR_ARROBA);

										try {

											float ftemp = Float.parseFloat(mitrans.iva.trim());

											arrivas2.append(String.valueOf(ftemp)); arrivas2.append(SEPARADOR_CHR_ARROBA);
										} catch (Exception e) {
											arrivas2.append(""); arrivas2.append(SEPARADOR_CHR_ARROBA);
											EIGlobal.mensajePorTrace("transferencia..pantalla_tabla_transferencias()->PROBLEMA AL INTENTAR LEER IVA",EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}


										arrrfcs2.append(mitrans.rfc.trim()); arrrfcs2.append(SEPARADOR_CHR_ARROBA);

										arrestatus2.append("1@");

									}
									else
									{
									   transerr++;
									   archivo_sin_error=false;
									   req.setAttribute("ArchivoErr","cuadroDialogo(\"S&oacute;lo se aceptan cuentas en pesos, l&iacute;nea:"+cont+"\", 3);");
									   break;
								    }
								 }
			    }
							else
			    {
								transerr++;
								archivo_sin_error=false;
								req.setAttribute("ArchivoErr","cuadroDialogo(\"Fecha inv&aacute;lida,  l&iacute;nea:"+cont+"\", 3);");
								break;
							}
						}
						else if (Abono01.trim().equals(Cargo01.trim()))
						{
							transerr++;

							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Cuenta de cargo igual a cuenta de abono, l&iacute;nea:"+cont+"\", 3);");
							break;
						}
						else if(tipo_cta_cargo.trim().length() == 0)
			{
						transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Cuenta de cargo inv&aacute;lida, l&iacute;nea:"+cont+"\", 3);");
							break;
						}
						else if(tipo_cta_abono.trim().length() == 0)
						{
							transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr",
									"cuadroDialogo(\"Cuenta de abono/M&oacute;vil inv&aacute;lida, l&iacute;nea:"
											+ cont + "\", 3);");
							break;
						} //fin de if
						else
						{
							transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error en archivo, l&iacute;nea:"+cont+"\", 3);");
							break;
						} //fin de if
					}else if (mitrans.err == -2)
					{		transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, el punto decimal no se encuentra en la posici&oacute;n correcta, l&iacute;nea:"+cont+"\", 3);");
							break;

					}
					else if (mitrans.err == -1)
					{		transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, longitud de l&iacute;nea "+cont+"\", 3);");
							break;
					}

					else if (mitrans.err == -3)
					{		transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, en fecha de aplicaci&oacute;n de la transferencia, l&iacute;nea:"+cont+"\", 3);");
							break;
					}
    				else if (mitrans.err == -4)
					{		transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, longitud de cuenta de cargo, l&iacute;nea:"+cont+"\", 3);");
							break;
					}
    				else if (mitrans.err == -5)
					{		transerr++;
							archivo_sin_error=false;
						req
								.setAttribute(
										"ArchivoErr",
										"cuadroDialogo(\"Error de formato en archivo, longitud de cuenta de abono/M&oacute;vil, l&iacute;nea:"
												+ cont + "\", 3);");
							break;
					}

					else if(mitrans.err == -6)
					{
							transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, El importe IVA debe tener punto decimal, l&iacute;nea:"+cont+"\", 3);");
							break;
					}
					else if(mitrans.err == -7)
					{
							transerr++;
							archivo_sin_error=false;
							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, Solo se permiten 12 Enteros en el importe IVA, l&iacute;nea:"+cont+"\", 3);");
							break;
					}

					else{
							transerr += mitrans.err;

							req.setAttribute("ArchivoErr","cuadroDialogo(\"Error de formato en archivo, revise layout de archivo; l&iacute;nea:"+cont+"\", 3);");
							archivo_sin_error=false;
							break;
					}
				}
				strOut.close();

				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try {
					BitaHelper bh = new BitaHelperImpl(req, session, sess);

					BitaTransacBean bt = new BitaTransacBean();;
					bt = (BitaTransacBean)bh.llenarBean(bt);
					if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
							equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA)){
						bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_IMPORTAR_ARCHIVO);
						bt.setTipoMoneda("MN");
					}
					if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
							equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES)){
						bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_IMPORTAR_ARCHIVO);
						bt.setTipoMoneda("USD");
					}
					if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
							equals(BitaConstants.ET_TARJETA_CREDITO_PAGO)){
						bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_IMPORTAR_ARCHIVO);
						bt.setTipoMoneda("MN");
					}
					if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
							equals(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION)){
						bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_IMPORTAR_ARCHIVO);
						bt.setTipoMoneda("MN");
					}
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if (req.getSession().getAttribute("ArchImporta") != null) {
						bt.setNombreArchivo(((String) req.getSession().getAttribute("ArchImporta")).trim());
					}
					bt.setBancoDest("SANTANDER");
					if(!archivo_sin_error){
						bt.setIdErr("Error");
					}

					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				 }


			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			   req.setAttribute("ArchivoErr","cuadroDialogo('Archivo de Importaci&oacute;n inv&aacute;lido', 3);");
			   archivo_sin_error=false;
			}
	    finally
		{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	    					}

	    	if ( null != strOut ){

	       try
			   {
		  strOut.close();
			   }
		   catch(Exception e)
			   {
			   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	       }

	    		strOut = null;
	    	}


	    }


			if(transerr == 0)
			{
				arrctas_origen = arrctas_origen2.toString();
				arrctas_destino = arrctas_destino2.toString();
				arrconceptos = arrconceptos2.toString();
				arrimporte = arrimporte2.toString();
				arrfechas = arrfechas2.toString();
				arrivas   = arrivas2.toString();
				arrrfcs   = arrrfcs2.toString();
				arrestatus = arrestatus2.toString();

				contador = cont;
			}
			req.setAttribute("contador2",Integer.toString(contador));
		}

		fecha_hoy=ObtenFecha();

		if (sfac_programadas.equals("0")){
			fac_programadas=false;}
		else{
			fac_programadas=true;}

		campos_fecha=poner_campos_fecha(fac_programadas);



		req.setAttribute("diasInhabiles", strInhabiles);
		req.setAttribute("fecha_hoy",fecha_hoy);
		req.setAttribute("campos_fecha",campos_fecha);
		req.setAttribute("fac_programadas1",sfac_programadas);
		req.setAttribute("calendario_normal", poner_calendario_programadas(fac_programadas,"fecha_completa","WindowCalendar()"));


		req.setAttribute("NoRegistradas",cuentasNoRegistradas(/*facCtasNoReg*/false));

		req.setAttribute("INICIAL","NO");
		req.setAttribute("TIPOTRANS", (String) getFormParameter(req,"TIPOTRANS"));
		req.setAttribute("IMPORTACIONES","1");
		req.setAttribute("NMAXOPER"," "+nmaxoper);
		req.setAttribute("divisa",(String) getFormParameter(req,"divisa"));
		req.setAttribute("trans",(String) getFormParameter(req,"trans"));
		req.setAttribute("TDC2CHQ", tdc2chq);

	    if ( tdc2chq.equals("1") ) {req.setAttribute("Encabezado",CreaEncabezado("Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n","s40020h",req));}
		else if (strTrans.equals("0"))	{	req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt	Moneda Nacional &gt En l&iacute;nea","s25370h",req));}
		else if (strTrans.equals("1"))	 {req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt	D&oacute;lares","s25410h",req));}
		else if (strTrans.equals("2"))	 {req.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito","s25530h",req));}
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());




		boolean imptabla=true;
	    if( ( ( (String) getFormParameter(req,"TIPOTRANS") ).equals("ARCHIVO") ) && (contador>=nmaxoper) )

		   imptabla=false;



		tabla = new StringBuffer("");

		if (imptabla)
		{

			EIGlobal.mensajePorTrace("***transferencia.pantalla_tabla_transferencias()->imptabla = true... scontador ="+scontador, EIGlobal.NivelLog.DEBUG);

			String arrconceptosprueba="\""+arrconceptos+"\"";
			String arrctas_origenprueba="\""+arrctas_origen+"\"";
			String arrctas_destinoprueba="\""+arrctas_destino+"\"";

			req.setAttribute("arregloctas_origen2",arrctas_origenprueba);
			req.setAttribute("arregloctas_destino2",arrctas_destinoprueba);
			req.setAttribute("arregloimportes2",arrimporte);
			req.setAttribute("arregloconceptos2",arrconceptosprueba);
			req.setAttribute("arreglofechas2",arrfechas);
			req.setAttribute("arregloestatus2",arrestatus);
			if(arrivas!=null && arrivas != "")
			if(arrivas!=null&&!arrivas.trim().equals(""))	req.setAttribute("arregloivas2","\""+arrivas+"\"");
			if(arrrfcs!=null && arrrfcs != "")
			if(arrrfcs!=null&&!arrrfcs.trim().equals(""))	req.setAttribute("arreglorfcs2","\""+arrrfcs+"\"");

			String[] Ctas_origen  = desentramaC(Integer.toString(contador)+"@"+arrctas_origen,'@');
			String[] Ctas_destino = desentramaC(Integer.toString(contador)+"@"+arrctas_destino,'@');
			String[] Importes     = desentramaC(Integer.toString(contador)+"@"+arrimporte,'@');
			String[] Fechas       = desentramaC(Integer.toString(contador)+"@"+arrfechas,'@');
			String[] Conceptos    = desentramaC(Integer.toString(contador)+"@"+arrconceptos,'@');
			String[] estatus      = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');

			/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 se agrega columna para favoritos*/
		   if (archivo_sin_error){
				tabla.append( "<TR><TD width=26></TD><TD class=tittabdat align=center width=200>Cuenta cargo</TD><TD class=tittabdat align=center colspan=2>" + ("PESOS".equals(sess.getAttribute("TIPO_DIVISA")) ? "Cuenta abono&nbsp;/&nbsp;M&oacute;vil" : "Cuenta abono") + "</TD><TD class=tittabdat align=center width=90>Importe</TD><TD class=tittabdat align=center width=70>Fecha aplicaci&oacute;n</TD><TD class=tittabdat align=center>Concepto</TD>" );
				if( "0".equals( strTrans ) || "2".equals( strTrans ) ){
					tabla.append("<td class=tittabdat align=center colspan = \"2\">Favoritos</td>");
				} else {
					tabla.append("</TR>");
				}
		   }
		   /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
		   int residuo=0;

		   String clase="";
		   for (indice=1;indice<=contador;indice++)
		   {
			  cadena_cta_origen  = Ctas_origen[indice];
			  cadena_cta_destino = Ctas_destino[indice];

			  cta_origen=cadena_cta_origen.substring(0,cadena_cta_origen.indexOf('|'));
			  if( ((String) getFormParameter(req,"TIPOTRANS")).equals("LINEA"))
			  {
			    cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length());
			cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length()-1);
		  }
			  else{
			cadena_cta_origen="";}


			  if (cadena_cta_destino.indexOf((int)'|') == -1 )
			  {
				 cta_destino = cadena_cta_destino;
				 cadena_cta_destino="";
			  }
			  else
			  {
				  cta_destino=cadena_cta_destino.substring(0,cadena_cta_destino.indexOf('|'));
				  if( ((String) getFormParameter(req,"TIPOTRANS")).equals("LINEA"))
				  {
					  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length());
			  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length()-1);
				  }
				  else{
					  cadena_cta_destino="";}
			  }

			  residuo=indice%2;
			  if (residuo==0){
				clase="textabdatobs";}
			  else{
				clase="textabdatcla";}

		      tabla.append( "<TR>" );
			  valor=estatus[indice];
			  nombre_campo="CH"+indice;
			  if ( ((String) getFormParameter(req,"TIPOTRANS")).equals("LINEA"))
			  {

			    if (valor.equals("0")){
				  tabla.append( "<TD align=center> <INPUT TYPE =CHECKBOX NAME=")
				  	.append( nombre_campo ).append(" VALUE =").append( valor )
				  	.append( "></TD>" );}
			    else{
				  tabla.append( "<TD align=center> <INPUT TYPE =CHECKBOX NAME=" )
				  	.append( nombre_campo ).append( " VALUE =" )
				  	.append( valor ).append( " checked ></TD>" );}
		  }
			  else {tabla.append( "<TD></TD>" );}

		  importetabla+=new Double(Importes[indice]).doubleValue();

		  tabla.append( "<TD class=" ).append( clase ).append( " align=left  nowrap >" )
		  	.append( cta_origen ).append( ' ' ).append( cadena_cta_origen )
		  	.append( "</TD>" )
		    .append( "<TD class=" ).append( clase ).append( " nowrap align=left colspan=2 >" )
		  	.append( cta_destino ).append( ' ' ).append( cadena_cta_destino )
		  	.append( "</TD>" )
		    .append( "<TD class=" ).append( clase ).append( " align=right nowrap >" )
		  	.append( FormatoMoneda(Importes[indice]) ).append( "</TD>" )
		    .append( "<TD class=" ).append( clase ).append( " align=right nowrap >" )
		  	.append( Fechas[indice] ).append( "</TD>" );


			  if (Conceptos[indice].trim().length()>0){
			    tabla.append( "<TD class=" ).append( clase )
			    	.append( " align=left nowrap>" ).append( Conceptos[indice] )
			    	.append( "</TD>" );}
			  else{
				tabla.append( "<TD class=" ).append( clase )
					.append( " align=left nowrap>&nbsp;</TD>" );
			  }
			  /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 se agrega columna para favoritos */
			  if( "0".equals( strTrans ) || "2".equals( strTrans ) ){
				  tabla.append("<td class=" ).append( clase )
				  	.append(" align=left nowrap> &nbsp;")
					.append("<input type=\"checkbox\" name=\"favChb" )
					.append( indice ).append( "\" id=\"favChb" )
					.append( indice ).append( "\" onClick='modificarTxt(this,")
					.append( indice ).append(");' ></input></td>")
					.append("<td class=" ).append( clase )
				  	.append(" align=left nowrap> &nbsp;" )
					.append("<input type=\"text\" name=\"favTxt" )
					.append( indice ).append( "\" size=\"15\" disabled=\"true\" maxlength=\"40\" id=\"favTxt" )
					.append( indice ).append( "\">" )
					.append( "</input></td>" );
			  } else {
					tabla.append(" </TR> ");
			  }
			  /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 se agrega columna para favoritos */
				if(grabaArchivo){
					cadena = cta_origen.concat(" ").concat(cadena_cta_origen).concat(" \t")
							.concat(cta_destino).concat(" ").concat(cadena_cta_destino).concat(" \t")
									.concat(FormatoMoneda(Importes[indice])).concat(" \t")
									.concat(Fechas[indice]).concat(" \t").concat(Conceptos[indice]).concat(" \t");

					ArcSalT.escribeLinea(cadena + "\r\n");
					EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  "+cadena, EIGlobal.NivelLog.DEBUG);
				}

		   }


  			if( grabaArchivo ) {
  				ArcSalT.cierraArchivo();
  			}

  			ArchivoRemoto archR = new ArchivoRemoto();
  			if(!archR.copiaLocalARemoto(fileOut,"WEB")) {
  				EIGlobal.mensajePorTrace("***transferencia.class No se pudo crear archivo para exportar transferencias mismo banco", EIGlobal.NivelLog.ERROR);
  			}

		   tabla.append( "<TR>" )
		   		.append( "<td>&nbsp;</td>" )
		   		.append( "<td align=right nowrap>&nbsp;</td>" )
		   		.append( "<td width=160 nowrap>&nbsp;</td>" )
		   		.append( "<td class=tittabdat align=center width=40 nowrap>Total</td>")
		   		.append( "<td align=right class=tittabdat nowrap >" ).append( FormatoMoneda(CADENA_VACIA+importetabla) ).append( "</td>" )
		   		.append( "<td align=right nowrap>&nbsp;</td>" )
		   		.append( "<td align=right>&nbsp;</td>" )
		   		.append( "</TR>" );
		if (!((String) getFormParameter(req,"TIPOTRANS")).equals("ARCHIVO")){
		     tabla.append( "<tr><td>&nbsp;</td><td colspan=6 class=textabref>Elimine la marca de las operaciones que no desea realizar </td></tr>" );
		}
		   req.setAttribute("imptabla","SI");
		   // FSW INDRA PyMES .. Marzo 2015
		   if (strTrans.equals("0") || strTrans.equals("2")) {
		   tabla.append( "<tr><td>&nbsp;</td><td colspan=")
		   	.append( "0".equals( strTrans ) ? "8" : "6" )
		   	.append( " align=center class=textabref><a style=\"cursor: pointer;\" >Exporta en TXT <input id=\"tipoExportacion\" type=\"radio\" value ='txt' name=\"tipoExportacion\"></a>\n</td></tr>" );
		   tabla.append( "<tr><td>&nbsp;</td><td colspan=")
		   	.append( "0".equals( strTrans ) ? "8" : "6" )
		   	.append( " align=center class=textabref><a style=\"cursor: pointer;\" >Exporta en XLS <input id=\"tipoExportacion\" type=\"radio\" value ='csv' checked name=\"tipoExportacion\"></a>\n</td></tr>" );

		   }
		   ExportModel em = new ExportModel("sepfileexp", '|',"\t", true)
		   .addColumn(new Column(0, "Cuenta Cargo"))
		   .addColumn(new Column(1, "Cuenta abono / Movil"))
		   .addColumn(new Column(2, "Importe", "$####################0.00"))
		   .addColumn(new Column(3, "Fecha Aplicacion"))
		   .addColumn(new Column(4, "Concepto"));
		   String archExportar = IEnlace.DOWNLOAD_PATH.concat(fileOut);
		   sess.setAttribute("af", archExportar);
		   sess.setAttribute("ExportModel", em);

		   // FSW INDRA PyMES .. Marzo 2015
		   if (!archivo_sin_error)
		   {
		     req.setAttribute("tabla","");
			 req.setAttribute("botontransferir","");
			 req.setAttribute("contadoroperaciones","");

		   }
		   else
		   {
				 if (contador>0)
				 {
					   req.setAttribute("contadoroperaciones","Total de  operaciones:"+contador+" por "+ FormatoMoneda(CADENA_VACIA+importetabla));
					   req.setAttribute("tabla",tabla.toString());


					   if ( strTrans.equals("0") ) {
							req.setAttribute("botontransferir","<td align=right><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25222.gif border=0 ></a></td>" +
																"<td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>" +
		                   										"<td align=left><a href = \"javascript:exportarArchivoPlano();\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>");
					   } else if (strTrans.equals("1")) {
						   req.setAttribute("botontransferir","<td align=right><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25222.gif border=0 ></a></td>" +
									"<td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>" );
					   }
					   else {
						   if (strTrans.equals("2")) {
							  req.setAttribute("botontransferir","<td align=right width=\"166\"><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25222.gif border=0 ></a></td><td align=left width=\"85\"><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td><td align=left width=\"166\"><a href = \"javascript:exportarArchivoPlano();\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>");
						   } else {
							 req.setAttribute("botontransferir","<td align=right><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=left><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>");
						   }
					   }

				 }
		   }


		 if ( ((String) getFormParameter(req,"TIPOTRANS")).equals("LINEA")) {


		   if(strTrans.equals("0") || strTrans.equals("1")) {
		       evalTemplate(IEnlace.MIS_TRANS_TMPL, req, res);
		   }
		   else if(strTrans.equals("2")) {
		       evalTemplate(IEnlace.MIS_TRANS_TARJETA_TMPL, req, res);
			}



		}
		else if (archivo_sin_error==true)
			{
					evalTemplate(IEnlace.MIS_TRANS_ARCH_TMPL, req, res);
			}
		   else
			{

			   if(strTrans.equals("0") || strTrans.equals("1")) {
					evalTemplate(IEnlace.MIS_TRANS_TMPL, req, res);
			   }
			   else if(strTrans.equals("2")) {
			       evalTemplate(IEnlace.MIS_TRANS_TARJETA_TMPL, req, res);
				}


			}
		}

		else
		{
						EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"Datos" );

						if(!ArcSal.creaArchivo())
						{
							EIGlobal.mensajePorTrace("transferencia.pantalla_tabla_transferencias(): Error de creaci&oacute;n de archivo: "+session.getUserID8()+"Datos", EIGlobal.NivelLog.ERROR);
							req.setAttribute("ArchivoErr","cuadroDialogo(' Error de creaci&oacute;n de archivo', 3);");

						}
						else
						{
							try{	//es muy importante conservar el orden en que se escriben las lineas pues es el orden en que se leen al procesar el archivo.
								ArcSal.escribeLinea(Integer.toString(contador)+"\n");
								ArcSal.escribeLinea(arrctas_origen+"\n");
								ArcSal.escribeLinea(arrctas_destino+"\n");
								ArcSal.escribeLinea(arrimporte+"\n");
								ArcSal.escribeLinea(arrconceptos+"\n");
								ArcSal.escribeLinea(arrivas+"\n");
								ArcSal.escribeLinea(arrrfcs+"\n");
								ArcSal.escribeLinea(arrfechas+"\n");
								ArcSal.escribeLinea(arrestatus+"\n");
								ArcSal.escribeLinea(fecha_hoy+"\n");
								ArcSal.escribeLinea(campos_fecha+"\n");
								ArcSal.escribeLinea(sfac_programadas);


								}catch (Exception e)
								{
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
									req.setAttribute("ArchivoErr","cuadroDialogo('Error de escritura a archivo', 3);");
									ArcSal.cierraArchivo();
								}
								ArcSal.cierraArchivo();
						}

						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
						try {
							BitaHelper bh = new BitaHelperImpl(req, session, sess);

							BitaTransacBean bt = new BitaTransacBean();;
							bt = (BitaTransacBean)bh.llenarBean(bt);
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA)){
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_EXPORTAR_ARCHIVO);
								bt.setTipoMoneda("MN");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES)){
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_EXPORTAR_ARCHIVO);
								bt.setTipoMoneda("USD");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_TARJETA_CREDITO_PAGO)){
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_EXPORTAR_ARCHIVO);
								bt.setTipoMoneda("MN");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION)){
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_EXPORTAR_ARCHIVO);
								bt.setTipoMoneda("MN");
							}
							if (session.getContractNumber() != null) {
								bt.setContrato(((BaseResource) req.getSession().getAttribute("session")).getContractNumber().trim());
							}
							bt.setBancoDest("SANTANDER");
							if (session.getUserID8() != null) {
								bt.setNombreArchivo(session.getUserID8() + "Datos");
							}

							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
						 }



		   tabla = new StringBuffer( "<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=3 CELLSPACING=2  width=300 >" )
		   	.append( "<TR><TD class=tittabdat align=center COLSPAN=2 >Confirmaci&oacute;n de transferencias por archivo</TD></TR>" )
		   	.append( "<TR><TD class=textabdatcla>Numero de registros:</TD><TD class=textabdatcla ALIGN=RIGHT>" ).append( contador ).append( "</TD></TR>" )
		   	.append( "<TR><TD class=textabdatobs>Importe total:</TD><TD class=textabdatobs ALIGN=RIGHT>" ).append( FormatoMoneda(CADENA_VACIA+importearchivo) ).append( "</TD></TR>" )
		   	.append( "</TABLE>" );
		   req.getSession().setAttribute("ImporteTotalPagoTarjeta", FormatoMoneda(CADENA_VACIA+importearchivo));
		   req.getSession().setAttribute("ImporteTotalPagoTarjetaToken", importearchivo); //PYME FSW INDRA Unifiacion Token
		   req.getSession().setAttribute("RegTotalPagoTarjeta", contador);
		   req.setAttribute("imptabla","NO");
		   req.setAttribute("MenuPrincipal", session.getStrMenu());
		   req.setAttribute("newMenu", session.getFuncionesDeMenu());
		   req.setAttribute("Encabezado",CreaEncabezado("Transferencias por archivo ","Transferencias &gt  Transferencias Archivo","s25360h",req));
		   if (archivo_sin_error)
		   {
			   req.setAttribute("tabla",tabla.toString());

				if (strTrans.equals("0") || strTrans.equals("1") )
			   {
					req.setAttribute("botontransferir","<td align=right><input type=image border=0 name=Transferir src=/gifs/EnlaceMig/gbo25222.gif alt=Transferir></td><td align=left><a href=\"javascript:regresar();\"><img  border=0 src=/gifs/EnlaceMig/gbo25190.gif></a></td>");

					String arrconceptosprueba="\""+arrconceptos+"\"";
					String arrctas_origenprueba="\""+arrctas_origen+"\"";
					String arrctas_destinoprueba="\""+arrctas_destino+"\"";

					req.setAttribute("arregloctas_origen2",arrctas_origenprueba);
					req.setAttribute("arregloctas_destino2",arrctas_destinoprueba);
					req.setAttribute("arregloimportes2",arrimporte);
					req.setAttribute("arregloconceptos2",arrconceptosprueba);
					req.setAttribute("arreglofechas2",arrfechas);
					req.setAttribute("arregloestatus2",arrestatus);
                    if(arrivas!=null&&!arrivas.trim().equals(""))	req.setAttribute("arregloivas2","\""+arrivas+"\"");
			        if(arrrfcs!=null&&!arrrfcs.trim().equals(""))	req.setAttribute("arreglorfcs2","\""+arrrfcs+"\"");

			   }

				else{
			   req.setAttribute("botontransferir","<td align=right><input type=image border=0 name=Transferir src=/gifs/EnlaceMig/gbo25360.gif alt=Transferir></td><td align=left><a href=\"javascript:regresar();\"><img  border=0 src=/gifs/EnlaceMig/gbo25190.gif></a></td>");}

			   evalTemplate(IEnlace.MIS_TRANS_ARCH_TMPL, req, res);
		   }
		   else
		   {
			  req.setAttribute("tabla","");
			  req.setAttribute("botontransferir","");

			    if(strTrans.equals("0") || strTrans.equals("1")) {
		  evalTemplate(IEnlace.MIS_TRANS_TMPL, req, res);
			    }

			    else if(strTrans.equals("2")) {
				evalTemplate(IEnlace.MIS_TRANS_TARJETA_TMPL, req, res);
				}
		   }
		}
		return 1;
	}



/**
 * Genera archivo de transferencias.
 *
 * @param strTrans the str trans
 * @param clave_perfil the clave_perfil
 * @param usuario the usuario
 * @param contrato the contrato
 * @param req the req
 * @param res the res
 * @return the int
 */
public int generaArchivoDeTransferencias(String strTrans, String clave_perfil,String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
{
	int contador =0;
	String scontador="";

	String cta_origen="";
	String tipo_cta_origen="";

	String cta_destino="";
	String tipo_cta_destino="";

	String concepto="";
	String[] arrCuentas=new String[4]; //Numero de cuentas	X 3 (cuenta|tipo|descripcion|)

	EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias: Entrando ", EIGlobal.NivelLog.DEBUG);

	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");


	EI_Exportar ArcDatos=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"Datos" );
	EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"TramasTransfer" );
	ArrayList <String>respLYM = new ArrayList<String>();

	if(!ArcDatos.abreArchivoLectura())
	{
		EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias: Error al abrir archivo de Datos ", EIGlobal.NivelLog.ERROR);
		return -1;
	}
	else
	{		//es muy importante conservar el orden en que se leen las lineas pues es el orden en que se escribieron al crear el archivo.
			String Cadena=ArcDatos.leeLinea();
			scontador=Cadena;
			Cadena=ArcDatos.leeLinea();
			String[] Ctas_origen  = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] Ctas_destino = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] Importes     = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] Conceptos    = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] Ivas	      = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] RFCs	      = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			String[] Fechas       = desentramaC(scontador+"@"+Cadena,'@');
			Cadena=ArcDatos.leeLinea();
			@SuppressWarnings("unused")
			String[] Estatus      = desentramaC(scontador+"@"+Cadena,'@');

			ArcDatos.cierraArchivo();


			short tipo_fecha=0;

			String conceptocompleto="";
			String trama_entrada="";
			String sfecha_programada="";


				contador=Integer.parseInt(scontador);

				if(!ArcSal.creaArchivo())
				{
					EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias: Error de creaci? de archivo ", EIGlobal.NivelLog.ERROR);
					return -1;
				}
				else
				{
						LYMValidador valida = new LYMValidador();
						for (int indice1=1;indice1<=contador;indice1++)
						{

								concepto			= Conceptos[indice1];


								if (concepto.trim().length()==0)
								{
								    if	 ((strTrans.equals("0"))|| (strTrans.equals("1"))){
									 concepto="TRANSFERENCIA EN EFECTIVO";}
								    else if (strTrans.equals("2")){
									 concepto="PAGO TARJETA DE CREDITO";}
								}

								conceptocompleto="";
								if (concepto.length()>40){
									conceptocompleto=concepto.substring(0,40);}
								else
								{
									conceptocompleto=concepto;
									int longi=concepto.length();
									for(int j=0;j<(40-longi);j++)
										conceptocompleto+=" ";
								}



								/*****************incluye la cadena de rfc e iva en el concepto    *****************/
								if  ((RFCs[indice1].length()!=0)&&(Ivas[indice1].length()!=0))
									conceptocompleto+="RFC "+RFCs[indice1]+" IVA "+Ivas[indice1];


								/*****************determinando cuenta y tipo de cuenta de cargo y abono **************/
								arrCuentas			= desentramaC("2|" + Ctas_origen[indice1],'|');
								cta_origen			= trimCuenta(arrCuentas[1]).trim();
								tipo_cta_origen 	= arrCuentas[2];

								if (Ctas_destino[indice1].indexOf((int)'|') == -1 ){
									arrCuentas	= desentramaC("2|" + Ctas_destino[indice1] + "|NR||" ,'|');}
								else{
									arrCuentas	= desentramaC("2|" + Ctas_destino[indice1]  ,'|');}

								cta_destino = trimCuenta(arrCuentas[1]).trim();
								tipo_cta_destino=arrCuentas[2];



								if   ((strTrans.equals("0"))|| (strTrans.equals("1")))	{
									if(cta_destino.length() == 16) {
										String tipoDEST = ObtenTipoCuenta2(cta_destino, strTrans, true, session, req);
										if(tipoDEST.equals("TARJCRED")) {
											tipo_operacion=CAD_TIP_OP_PAGT;
										}
										else {
											tipo_operacion = CAD_TIP_OP_TRAN;
										}
									}
									else {
										tipo_operacion = CAD_TIP_OP_TRAN;
									}
								}
								else if (strTrans.equals("2")) {
									tipo_operacion=CAD_TIP_OP_PAGT;
								}

								EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias-> tipo operacion= "+tipo_operacion, EIGlobal.NivelLog.DEBUG);


								tipo_fecha=checar_fecha(Fechas[indice1]);
								if (tipo_fecha==0){
									trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
														cta_destino+"|"+tipo_cta_destino+"|"+Importes[indice1]+"|"+conceptocompleto+"| |";}//*****Cambio para comprobante fiscal
								else
								{
									sfecha_programada=Fechas[indice1].substring(3,5)+Fechas[indice1].substring(0,2)+Fechas[indice1].substring(8,Fechas[indice1].length());
									trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
														cta_destino+"|"+tipo_cta_destino+"|"+Importes[indice1]+"|0|"+conceptocompleto+"| |";//*****Cambio para comprobante fiscal
								}



								String IP = " ",fechaHr = "";
								IP = req.getRemoteAddr();
								java.util.Date fechaHrAct = new java.util.Date();
	        					SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
								fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";				//asignaci? de fecha y hora

								EIGlobal.mensajePorTrace(fechaHr+"***transferencia.generaArchivoDeTransferencias->Trama = ["+indice1+"]="+trama_entrada, EIGlobal.NivelLog.DEBUG);



								String resLYM = valida.validaLyM(trama_entrada);
								if(!resLYM.equals("ALYM0000")) {
									respLYM.add(indice1 + "|" + resLYM);
								}



								if(!ArcSal.escribeLinea(trama_entrada+"\n"))
								{
									EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias: Error de escritura a archivo ", EIGlobal.NivelLog.ERROR);
									ArcSal.cierraArchivo();
									if(valida != null) {
										valida.cerrar();
									}
									return -1;

								}

						}
						if(valida != null) {
							valida.cerrar();
						}
				}
	}

	EIGlobal.mensajePorTrace("***transferencia.generaArchivoDeTransferencias->Saliendo", EIGlobal.NivelLog.DEBUG);
	ArcSal.cierraArchivo();
	if(respLYM.size() > 0) {
		req.getSession().setAttribute("erroresLYM", respLYM);
	}
	return respLYM.size();
}




/**
 * Ejecuta transferencias.
 *
 * @param req the req
 * @param res the res
 * @return true, if successful
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
public boolean EjecutaTransferencias(HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException
{

	String coderror="";
	String VectCodError="";

	String sreferencia=null;
	@SuppressWarnings("unused")
	String strFormatCantidad="";
	String error="";
	@SuppressWarnings("unused")
	String edoCF="";

	String scontador="";
	String cta_origen="";
	String cta_destino="";
	String[] arrCuentas=new String[4]; //Numero de cuentas	X 3 (cuenta|tipo|descripcion|)


	String Result = null;
	String codError = null;
	@SuppressWarnings("unused")
	String mensajeErrorFolio = null;
	String mensajeErrorEnviar = null;
	String mensajeErrorDispersor = null;
	String folioArchivo = null;
	int numRegistrosOk = 0;


	int contador =0;
	int contadorok=0;
	int contadorprog=0;
	int contadormanc=0;
	int contadorerror=0;
	@SuppressWarnings("unused")
	short tipo_fecha=0;


		EIGlobal.mensajePorTrace("***transferencia.EjecutaTransferencias->Entrando", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		BitaHelper bh = new BitaHelperImpl(req, session, sess);


		EI_Exportar ArcDatos=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"Datos" );
		EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"CodErrTransfer.doc" );
		EI_Exportar ArcTramas=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	session.getUserID8()+"TramasTransfer" );

	// Creacion de Archivo con datos de la Bitacora
		String archivoBitacora = null;
		EI_Exportar ArcBit = null;
		boolean archivoBitCreado = false;
		String tramaBitacora = "";
		ServicioTux TuxGlobal = new ServicioTux();


	// Obtencion del numero de lineas sin error
		if (!ArcDatos.abreArchivoLectura()) {
				EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Error al abrir archivo de Datos ", EIGlobal.NivelLog.ERROR);

				ArcDatos.cierraArchivo();

				return false;
			}
		else {
			String Cadena = ArcDatos.leeLinea();
			scontador = Cadena;
			numRegistrosOk = Integer.parseInt(scontador);
			ArcDatos.cierraArchivo();
			}


		String tramaParaFolio = session.getUserID8() + "|" + session.getUserProfile() + "|" + session.getContractNumber() + "|TRAN|" + numRegistrosOk +"|";
		try{
			Hashtable hs = TuxGlobal.alta_folio(tramaParaFolio);
		 	Result= (String) hs.get("BUFFER") +"";
		 	}
		catch( java.rmi.RemoteException re ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		 	}
		catch(Exception e) {}

		if (Result == null || Result.equals("null"))
			Result = "ERROR            Error en el servicio.";

		EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: GENERADOR DE FOLIO : Trama salida: " + Result, EIGlobal.NivelLog.DEBUG);
		codError = Result.substring(0, Result.indexOf(SEPARADOR_PIPE));

		if (!codError.trim().equals("TRSV0000")) {
			mensajeErrorFolio = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
			}
		else {
			folioArchivo = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
			folioArchivo = folioArchivo.trim();
			if ( folioArchivo.length() < 12)
				{
				do{
					folioArchivo = "0" + folioArchivo;
					}while(folioArchivo.length() < 12);
				}
			if ( ( folioArchivo.length() != 12 ) || folioArchivo.equals("000000000000") )
				{
				mensajeErrorFolio = Result ;
				}
			else{
				session.setFolioArchivo(folioArchivo);
				req.setAttribute("folioArchivo", folioArchivo);

				if (!mx.altec.enlace.utilerias.GzipArchivo.zipFile(IEnlace.DOWNLOAD_PATH + session.getUserID8()
						+ "TramasTransfer", IEnlace.DOWNLOAD_PATH + session.getUserID8() + "_" + session.getFolioArchivo()	+ ".tram")){
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Error: No se reallizo el gzip del archivo "+ IEnlace.DOWNLOAD_PATH + session.getUserID8()
						+ "TramasTransfer", EIGlobal.NivelLog.DEBUG);}
				else{
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Se realizo el gzip: "
						+ IEnlace.DOWNLOAD_PATH + session.getUserID8() + "_"	+ session.getFolioArchivo() + ".tram.gz", EIGlobal.NivelLog.DEBUG);}


				if(!ArcDatos.abreArchivoLectura())
					{
					EIGlobal.mensajePorTrace("transferencia.generaArchivoDeTransferencias: Error al abrir archivo de Datos ", EIGlobal.NivelLog.ERROR);

					ArcDatos.cierraArchivo();

					return false;
					}
				else
					{
					String Cadena=ArcDatos.leeLinea();
					scontador=Cadena;
					contador=Integer.parseInt(scontador);

					Cadena=ArcDatos.leeLinea();
					String[] Ctas_origen  = desentramaC(scontador+"@"+Cadena,'@');

					Cadena=ArcDatos.leeLinea();
					String[] Ctas_destino = desentramaC(scontador+"@"+Cadena,'@');

					Cadena=ArcDatos.leeLinea();
					String[] Importes     = desentramaC(scontador+"@"+Cadena,'@');

					Cadena=ArcDatos.leeLinea();

					String [] sConceptos = desentramaC(scontador+"@"+Cadena,'@');


					Cadena=ArcDatos.leeLinea();
					String[] Ivas	      = desentramaC(scontador+"@"+Cadena,'@');

					Cadena=ArcDatos.leeLinea();
					String[] RFCs	      = desentramaC(scontador+"@"+Cadena,'@');

					Cadena=ArcDatos.leeLinea();
					String[] Fechas       = desentramaC(scontador+"@"+Cadena,'@');

					ArcDatos.cierraArchivo();

					if(!ArcTramas.abreArchivoLectura())
						{
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  al abrir archivo de tramas ", EIGlobal.NivelLog.ERROR);

						ArcTramas.cierraArchivo();

						return false;
						}
					else
						{
						if(!ArcSal.creaArchivo())
							{
							EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  de creaci? de archivo ", EIGlobal.NivelLog.ERROR);

							ArcSal.cierraArchivo();

							return false;
							}
						else
							{
							// Creacion de las tramas del archivo de la bitacora
                            archivoBitacora = session.getUserID8() + "_"+folioArchivo +"TIN.bita";
                            ArcBit = new EI_Exportar(IEnlace.DOWNLOAD_PATH + archivoBitacora);
                            if (ArcBit.creaArchivo()) archivoBitCreado = true;

							if (!archivoBitCreado) {
								EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  de creaci? de archivo Bitacora ", EIGlobal.NivelLog.ERROR);
								return false;
								}
							else {

								EIGlobal.mensajePorTrace("***transferencia.EjecutaTransferencias->Entrando al FOR", EIGlobal.NivelLog.DEBUG);
								for (int indice1=1;indice1<= contador ;indice1++)
									{

									arrCuentas			= desentramaC("2|" + Ctas_origen[indice1],'|');
									cta_origen			= arrCuentas[1];

									if (Ctas_destino[indice1].indexOf((int)'|') == -1 ){
										arrCuentas	= desentramaC("2|" + Ctas_destino[indice1]+ "|NR||",'|');}
									else{
										arrCuentas	= desentramaC("2|" + Ctas_destino[indice1],'|');}

									cta_destino = arrCuentas[1];

									tipo_fecha=checar_fecha(Fechas[indice1]);

									if  ((RFCs[indice1].length()!=0)&&(Ivas[indice1].length()!=0)){
										edoCF="S";}
									else {edoCF="N";}

									// No se realiza la transferencia, van campos vacios al archivo de salida
									sreferencia=" ";
									strFormatCantidad=" ";
									error=" ";

									String cuentaOrigen = cta_origen.substring(0, 11);
									String descripcionCO = cta_origen.substring(12);
									cuentaOrigen = formatoSalida(cuentaOrigen, 16);
									descripcionCO = formatoSalida(descripcionCO, 30);

									String cuentaDestino = cta_destino.substring(0, 11);
									String descripcionCD = cta_destino.substring(12);
									cuentaDestino = formatoSalida(cuentaDestino, 16);
									descripcionCD = formatoSalida(descripcionCD, 30);

									String fecha = Fechas[indice1];
									String nuevaFecha = "";
									int anterior = 0;
									for (int i=2; i<=3; i++){
										nuevaFecha += fecha.substring(anterior, fecha.indexOf("/", i) );
										anterior = fecha.indexOf("/", i)+1;
										}
									nuevaFecha += fecha.substring(anterior, fecha.length() );

									String importe = FormatoMoneda(Importes[indice1]);
									String valor = importe.substring( importe.indexOf("$")+1,importe.length());
									valor = valor.trim();
									String entero = valor.substring(0, valor.indexOf("."));
									if ( entero.length() < 12){
										do{
											entero = "0" + entero;
											}while(entero.length() < 12);
										}
									String decimal = valor.substring(valor.indexOf(".")+1, valor.length());

									String estatus = error;
									estatus = formatoSalida(estatus, 40);

									String referencia = sreferencia;
									if ( referencia.length() < 8){
										do{
											referencia = "0" + referencia;
											}while(referencia.length() < 8);
										}
									String varios = sConceptos[indice1];
									varios = formatoSalida(varios, 40);

									VectCodError = cuentaOrigen + descripcionCO + cuentaDestino + descripcionCD + nuevaFecha + entero + decimal + estatus + referencia + varios;

									if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
										try {
											BitaTransacBean bt = new BitaTransacBean();
											bt = (BitaTransacBean) bh.llenarBean(bt);
											if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
												equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA)){
												bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_AGREGAR_REGISTRO_BIT);
												bt.setTipoMoneda("MN");
												}
											if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
												equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES)){
												bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_AGREGAR_REGISTRO_BIT);
												bt.setTipoMoneda("USD");
												}
											if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
												equals(BitaConstants.ET_TARJETA_CREDITO_PAGO)){
												bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_AGREGAR_REGISTRO_BIT);
												bt.setTipoMoneda("MN");
												}
											if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
												equals(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION)){
												bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_AGREGAR_REGISTRO_BIT);
												bt.setTipoMoneda("MN");
												}
											if (session.getContractNumber() != null) {
												bt.setContrato(((BaseResource) req.getSession().getAttribute("session")).getContractNumber().trim());
												}
											if (cta_origen != null) {
												if (cta_origen.length() > 11) {
													bt.setCctaOrig(cta_origen.substring(0,11));
													}
												else {
													bt.setCctaOrig(cta_origen.trim());
													}
												}
											if(cta_destino != null){
												if(cta_destino.length() > 20){
													bt.setCctaDest(cta_destino.substring(0,20));
													}
												else{
													bt.setCctaDest(cta_destino.trim());
													}
												}
											if (Importes[indice1].trim() != null) {
												bt.setImporte(Double.parseDouble(Importes[indice1].trim()));
												}

											//No se realiza la transferencia, van campos vacios al archivo de salida
											bt.setReferencia(0);
											if (coderror != null) {
												if (coderror.length() > 8) {
													bt.setIdErr(coderror.substring(0,8));
												} else {
													bt.setIdErr(coderror.trim());
												}
											}


											bt.setBancoDest("SANTANDER");
											if (session.getUserID8() != null) {
												bt.setNombreArchivo(session.getUserID8()+ "CodErrTransfer.doc");
												}
											if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
												&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
												.equals(BitaConstants.VALIDA)) {
												bt.setIdToken(session.getToken().getSerialNumber());
												}
											else{
												log("VSWF: no hubo bandera");
												}



											if ( bt.getServTransTux() == null )
												 bt.setServTransTux(CAD_TIP_OP_TRAN);

											String arregloDatos[] = {
												String.valueOf(bt.getFolioBit()),
												String.valueOf(bt.getFolioFlujo()),
												bt.getIdFlujo(),
												bt.getNumBit(),
												String.valueOf( (bt.getFecha()==null)? bt.getFecha() : new java.sql.Date( bt.getFecha().getTime() ) ),
												String.valueOf( (bt.getFechaProgramada()==null)? bt.getFechaProgramada() :new java.sql.Date( bt.getFechaProgramada().getTime() ) ),
												bt.getTipoMoneda(),
												String.valueOf(bt.getNumTit()),
												String.valueOf(bt.getReferencia()),
												String.valueOf(bt.getImporte()),
												String.valueOf(bt.getTipoCambio()),
												bt.getIdErr(),
												bt.getHora(),
												bt.getDirIp(),
												bt.getCodCliente(),
												bt.getIdToken(),
												bt.getBancoDest(),
												bt.getServTransTux(),
												bt.getCanal(),
												BaseServlet.convierteUsr8a7(bt.getUsr()),
												bt.getContrato(),
												bt.getIdWeb(),
												bt.getIdSesion(),
												( bt.getCctaOrig() ).substring(0, 11),
												( bt.getCctaDest() ).substring(0, 11),
												bt.getNombreHostWeb(),
												String.valueOf( (bt.getFechaAplicacion()==null)? bt.getFechaAplicacion() : new java.sql.Date( bt.getFechaAplicacion().getTime() ) ),
												bt.getEstatus(),
												bt.getNombreArchivo() };
											tramaBitacora = "";
											for (int i = 0; i < arregloDatos.length; i++) {
												if (arregloDatos[i] == null	|| arregloDatos[i] == "" || arregloDatos[i].equals("null"))
													arregloDatos[i] = " ";
												if (i == arregloDatos.length - 1){
													tramaBitacora += arregloDatos[i];}
												else{
													tramaBitacora += arregloDatos[i] + "|";}
												}


											if (!ArcBit.escribeLinea(tramaBitacora + "\n")) {
												EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Algo salio mal escribiendo en el archivo de la bitacora: " + tramaBitacora, EIGlobal.NivelLog.ERROR);
												ArcBit.cierraArchivo();
												return false;
												}

											EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:Descripci���nCD"+descripcionCD, EIGlobal.NivelLog.INFO);
											if(descripcionCD!=null && descripcionCD!=""){
												bt.setBeneficiario(descripcionCD.trim());
											}
											BitaHandler.getInstance().insertBitaTransac(bt);

											}
										catch (Exception e) {
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											}
										}

									if(!ArcSal.escribeLinea(VectCodError+"\n"))
										{
										EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  de escritura a archivo de resultados", EIGlobal.NivelLog.ERROR);
										ArcSal.cierraArchivo();
										return false;
										}

									}
								req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

								// Cierre y compresion del archivo con tramas para bitacora
								ArcBit.cierraArchivo();
								if (!mx.altec.enlace.utilerias.GzipArchivo.zipFile(IEnlace.DOWNLOAD_PATH + archivoBitacora,
									IEnlace.DOWNLOAD_PATH + session.getUserID8() + "_" + session.getFolioArchivo() + ".bita")){
										EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: : No se reallizo el gzip del archivo " + IEnlace.DOWNLOAD_PATH + archivoBitacora, EIGlobal.NivelLog.DEBUG);}
								else{
									EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Se realizo el gzip: "
										+ IEnlace.DOWNLOAD_PATH + session.getUserID8() + "_" + session.getFolioArchivo() + ".bita.gz", EIGlobal.NivelLog.DEBUG);}
								ArcBit.borraArchivo();

								}

							}
						}
					}

     			ArcSal.cierraArchivo();
     			ArcTramas.cierraArchivo();

				// compresion del archivo de salida
				if (!mx.altec.enlace.utilerias.GzipArchivo.zipFile( IEnlace.DOWNLOAD_PATH + session.getUserID8() + "CodErrTransfer.doc",IEnlace.DOWNLOAD_PATH + session.getUserID8() + "CodErrTransfer.doc_" + session.getFolioArchivo() )){
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: : No se reallizo el gzip del archivo " +IEnlace.DOWNLOAD_PATH + session.getUserID8() + "CodErrTransfer.doc", EIGlobal.NivelLog.DEBUG);}
				else{
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Se realizo el gzip: " + IEnlace.DOWNLOAD_PATH + session.getUserID8() + "CodErrTransfer.doc_" + session.getFolioArchivo() + ".gz", EIGlobal.NivelLog.DEBUG);}

				// Realizando RCP de los archivos gzip
				ArchivoRemoto archRemoto = new ArchivoRemoto();
				String gzipTrama = session.getUserID8() + "_" + session.getFolioArchivo() + ".tram.gz";
				String gzipBita = session.getUserID8() + "_" + session.getFolioArchivo() + ".bita.gz";
				String gzipResu = session.getUserID8() + "CodErrTransfer.doc_" + session.getFolioArchivo() + ".gz";

				boolean rcpRealizado = false;
				if ( (new File(IEnlace.DOWNLOAD_PATH + gzipTrama)).exists() &&
				 	(new File(IEnlace.DOWNLOAD_PATH + gzipBita)).exists() &&
				 	(new File(IEnlace.DOWNLOAD_PATH + gzipResu)).exists() ) {

					if (archRemoto.copiaLocalARemoto(gzipTrama)) {
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  El archivo " + gzipTrama + " se copio correctamente", EIGlobal.NivelLog.DEBUG);
						if (archRemoto.copiaLocalARemoto(gzipBita)) {
							EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  El archivo " + gzipBita + " se copio correctamente", EIGlobal.NivelLog.DEBUG);
							if(archRemoto.copiaLocalARemoto( gzipResu )){
								rcpRealizado = true;
								EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  El archivo "+gzipResu+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
								}
							else{
								EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  Error al copiar el archivo " +gzipResu, EIGlobal.NivelLog.ERROR);}
							}
						else
							{EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Error al copiar el archivo " + gzipBita, EIGlobal.NivelLog.ERROR);}
						}
					else{
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: Error al copiar el archivo " + gzipTrama, EIGlobal.NivelLog.ERROR);
						}
					}
				else {
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se encontraron archivos para copiar por rcp.", EIGlobal.NivelLog.ERROR);
					}


				if (!rcpRealizado) {
					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  No se realizo copia por rcp.", EIGlobal.NivelLog.ERROR);
					return false;
					}
				else {// Llamado a Servicio Tuxedo controlador de registros
					String tramaParaEnviar = session.getUserID8() + "|"
						+ session.getUserProfile() + "|"
						+ session.getContractNumber() + "|"
						+ session.getFolioArchivo() + "|TRAN|"
						+ IEnlace.REMOTO_TMP_DIR + "/" + gzipTrama + "|"
						+ IEnlace.REMOTO_TMP_DIR + "/" + gzipBita + "|"
						+ IEnlace.REMOTO_TMP_DIR + "/" + gzipResu ;
					try{
						Hashtable hs = TuxGlobal.envia_datos(tramaParaEnviar);
						Result= (String) hs.get("BUFFER") + CADENA_VACIA;

						}
					catch( java.rmi.RemoteException re )
						{
						EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				 		}
					catch(Exception e) {EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}

					if (Result == null || Result.equals("null"))
						Result = "ERROR            Error en el servicio.";

					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: CONTROLADOR DE REGISTROS  : Trama salida: " + Result, EIGlobal.NivelLog.DEBUG);
					codError = Result.substring(0, Result.indexOf(SEPARADOR_PIPE));

					if (!codError.trim().equals("TRSV0000")) {
						mensajeErrorEnviar = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se enviaron los registros al controlador de registros Mensaje: " +mensajeErrorEnviar, EIGlobal.NivelLog.ERROR);
						return false;
						}
					else {
						mensajeErrorEnviar = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
						if (mensajeErrorEnviar.equals("OK"))
							{ // Llamado a Servicio Tuxedo dispersor de transacciones
							String tramaParaDispersor = session.getUserID8() + "|"
								+ session.getUserProfile() + "|"
								+ session.getContractNumber() + "|"
								+ session.getFolioArchivo() + "|TRAN|";
							try{
								if (Global.LYNX_INICIALIZADO && "TRAN".equals(tipo_operacion) && "Moneda Nacional".equals(req.getSession().getAttribute(MONEDA))) {
									EnlaceLynxUtils.llamadoConectorLynxTranferenciaArchivo(req,
											edoCF, cta_origen, cta_destino,
											session);
								}
						 		Hashtable hs = TuxGlobal.inicia_proceso(tramaParaDispersor);
						 		Result= (String) hs.get("BUFFER") + CADENA_VACIA;
						 						 		}
						 	catch( java.rmi.RemoteException re )
						 		{
						 		EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
						 		}
						 	catch(Exception e) {EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}

							if (Result == null || Result.equals("null"))
								Result = "ERROR            Error en el servicio.";

							EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  DISPERSOR DE REGISTROS  : Trama salida: " + Result, EIGlobal.NivelLog.DEBUG);

							codError = Result.substring(0, Result.indexOf(SEPARADOR_PIPE));

							if (!codError.trim().equals("TRSV0000")) {
								mensajeErrorDispersor = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
								EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se realizo la dispersion de registros", EIGlobal.NivelLog.DEBUG);
								return false;
								}
							else {
								mensajeErrorDispersor = Result.substring(Result.indexOf(SEPARADOR_PIPE) + 1, Result.length() );
								EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
								TransferenciasMonitorPlusUtils.enviarTMBArchivo(req, session,IEnlace.SUCURSAL_OPERANTE, IEnlace.SUCURSAL_OPERANTE, codError.trim());
								}
							}//FIN Llamado a Servicio Tuxedo dispersor de transacciones
						}
					}//FIN Llamado a Servicio Tuxedo controlador de registros
				}// FIN ELSE condicional de folio correcto
			}// FIN ELSE condicional de recepcion de folio

	EIGlobal.mensajePorTrace("***transferencia.EjecutaTransferencias->Saliendo", EIGlobal.NivelLog.DEBUG);
	sess.setAttribute("contadorok", Integer.toString(contadorok));
	sess.setAttribute("contadorprog", Integer.toString(contadorprog));
	sess.setAttribute("contadormanc", Integer.toString(contadormanc));
	sess.setAttribute("contadorerror", Integer.toString(contadorerror));
	sess.setAttribute("codError", codError);// Regresa codgo de error


	return true;
}


/**
 * Ejecucion_operaciones_transferencias_sin_tabla.
 *
 * @param strTrans the str trans
 * @param clave_perfil the clave_perfil
 * @param usuario the usuario
 * @param contrato the contrato
 * @param req the req
 * @param res the res
 * @return the int
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
public int ejecucion_operaciones_transferencias_sin_tabla(String strTrans, String clave_perfil,String usuario, String contrato,HttpServletRequest req, HttpServletResponse res)
throws ServletException, IOException
{
		EIGlobal.mensajePorTrace("******************************************************************************", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Entrando", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("******************************************************************************", EIGlobal.NivelLog.DEBUG);

		String mensaje_salida="";


		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");

		/***********************************Req. Q1466 Construccion variables sesion p/control recarga ****************/
		boolean banderaTransaccion = true;

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null && verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
		{
			EIGlobal.mensajePorTrace( "Transferencias Internas El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion =false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			EIGlobal.mensajePorTrace("Transferencias Internas Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			banderaTransaccion=false;
		}
		else
		if(verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false) )
		{
			EIGlobal.mensajePorTrace( "Transferencias Internas El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Internas")==null && !verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
        {
		    EIGlobal.mensajePorTrace( "Transferencias Internas Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = true;
		}

		if (banderaTransaccion || strTrans.equals("2") ) // Procesar si no es recarga de la pantalla ejecucion
		{
			//Se crea el archivo para control de recarga sesion, y puesta de var. Recarga
			EI_Exportar arcTmp=new EI_Exportar(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses");
			if(arcTmp.creaArchivo())
				EIGlobal.mensajePorTrace( "Transferencias Internas - El archivo.ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);
			req.getSession().setAttribute("Recarga_Ses_Internas","TRUE");

			arcTmp.cierraArchivo();

		int resArch = generaArchivoDeTransferencias(strTrans, clave_perfil, usuario, contrato, req, res);
		if (resArch < 0)
		{
			EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Error en la generacion de archivo de transferencias", EIGlobal.NivelLog.DEBUG);
			req.setAttribute("ArchivoErr","cuadroDialogo(\"Error en generacion de archivo de transferencias\", 3);");
			return 0;
		}
		else if(resArch > 0) {

			EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Existen operaciones que exceden LYM", EIGlobal.NivelLog.DEBUG);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias Moneda Nacional","Transferencias &gt  Validaci&oacute;n de L&iacute;mites y Montos",req));

			evalTemplate("/jsp/transferencia_val_lym.jsp", req, res);

			return 0;
		}
		else
		{
			if (!EjecutaTransferencias(req, res))
			{
				EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Error en la ejecucion de transferencias", EIGlobal.NivelLog.DEBUG);
				req.setAttribute("ArchivoErr","cuadroDialogo(\"Error en ejecucion de transferencias\", 3);");
				return 0;
			}
			else
			{
						ArchivoRemoto archR = new ArchivoRemoto();

						if(!archR.copiaLocalARemoto(session.getUserID8()+"CodErrTransfer.doc","WEB")){
							EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> No se creo archivo de exportacion", EIGlobal.NivelLog.DEBUG);}
						else{
							EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Se creo archivo de exportacion", EIGlobal.NivelLog.DEBUG);}


						mensaje_salida="<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=3 CELLSPACING=2  width=300>";
						// Mostrar el numero de folio
						mensaje_salida += "<TR><td class=tittabcom align=center><br><i>Las transferencias estan siendo efectuadas.</i><br>";
			   			mensaje_salida += "El n&uacute;mero de Folio correspondiente a esta transferencia es: <br>";
			   			mensaje_salida += "<b><font color=red>" + req.getAttribute("folioArchivo") +"</font></b><br> ";
			   			mensaje_salida += "Utilice este n&uacute;mero de Folio para consultar las <b><font color=blue>referencias</font></b> y<br>";
			   			mensaje_salida += "<font color=green><b>estado</b></font> de las transferencias, en <br>";
			   			mensaje_salida += "<b>Seguimiento de Transferencias</b>.</td> </TR>";
			   			mensaje_salida+="</TABLE>";


						req.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
						sess.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");


						req.setAttribute("strDebug",strDebug);
						req.setAttribute("mensaje_salida",mensaje_salida);
						req.setAttribute("MenuPrincipal", session.getStrMenu());
						req.setAttribute("newMenu", session.getFuncionesDeMenu());

						req.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
						req.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
						req.setAttribute("banco",session.getClaveBanco());

						sess.setAttribute("strDebug",strDebug);
						sess.setAttribute("mensaje_salida",mensaje_salida);
						sess.setAttribute("MenuPrincipal", session.getStrMenu());
						sess.setAttribute("newMenu", session.getFuncionesDeMenu());
						sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
						sess.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
						if (strTrans.equals("0"))
						{
							req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt	Moneda Nacional &gt En l&iacute;nea","s25380h",req ));
							sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras	en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s25380h",req ));

							req.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
							sess.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
						}
						else if (strTrans.equals("1"))
						{
							req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s25420h",req));
							sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt	d&oacute;lares","s25420h",req));
						}

						req.setAttribute("web_application",Global.WEB_APPLICATION);
						sess.setAttribute("web_application",Global.WEB_APPLICATION);
						sess.setAttribute("banco",session.getClaveBanco());

						EIGlobal.mensajePorTrace("******************************************************************************", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_sin_tabla-> Saliendo", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("******************************************************************************", EIGlobal.NivelLog.DEBUG);


						evalTemplate(IEnlace.MI_OPER_TMPL, req, res);

			}



			EmailSender sender = new EmailSender();
			String codErrorOper=(String)sess.getAttribute("codError");
			if(sender.enviaNotificacion(codErrorOper)){
				try {
				EmailDetails details = new EmailDetails();

				details.setEstatusActual(codErrorOper);
				details.setNumeroContrato(session.getContractNumber());
				details.setRazonSocial(session.getNombreContrato());
				details.setNumRef((String)req.getAttribute("folioArchivo"));

				details.setMayorA100Reg(true);
				details.setPAGT(false);

				EIGlobal.mensajePorTrace("<><><><><><><>[Pago de tarjeta] -> Numero de Registros: " + req.getSession().getAttribute("RegTotalPagoTarjeta"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("<><><><><><><>[Pago de tarjeta] -> Importe: " + req.getSession().getAttribute("ImporteTotalPagoTarjeta"), EIGlobal.NivelLog.INFO);

				details.setNumRegImportados((Integer)req.getSession().getAttribute("RegTotalPagoTarjeta"));
				details.setImpTotal((String)req.getSession().getAttribute("ImporteTotalPagoTarjeta"));
					if(tipo_operacion != null && tipo_operacion.equals(CAD_TIP_OP_PAGT)){
						EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><> Pago de tarjeta de credito <><><><><><><><><><><><><>" , EIGlobal.NivelLog.INFO);

						details.setPAGT(true);
					}


				EIGlobal.mensajePorTrace(".................Transferencias > 100 registros ----> num contrato   : " + details.getNumeroContrato(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(".................Transferencias > 100 registros ----> nom contrato   : " + details.getRazonSocial(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(".................Transferencias > 100 registros ----> num referencia : " + details.getNumRef(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(".................Transferencias > 100 registros ----> setImpTotal    : " + details.getImpTotal(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(".................Transferencias > 100 registros ----> num regImp     : " + details.getNumRegImportados(), EIGlobal.NivelLog.DEBUG);


				sender.sendNotificacion(req,IEnlace.MAS_TRANS_MISMO_BANCO, details);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			}
		}

		}
		else
		{   //Generar variables para pantalla aviso de Transaccion en Proceso.
			EIGlobal.mensajePorTrace("***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);
			String InfoUser= "<br>Su transacci&oacute;n ya est&aacute; en proceso, Verifique posteriormente resultados";
			InfoUser="cuadroDialogo (\""+ InfoUser +"\",1)";
			req.setAttribute("InfoUser", InfoUser);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("trans",strTrans);
			if (strTrans.equals("0"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55280",req ));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55280",req ));
			}
			else if (strTrans.equals("1"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55280a",req));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55280a",req));
			}

			req.setAttribute("operacion","tranopera");  //antes "valida"
			evalTemplate("/jsp/TransInternasTranenProceso.jsp", req, res);
		}
		sess.removeAttribute("registrosTran");
		sess.removeAttribute("importeTran");
		return 1;
}

	/**
	* Pantalla_valida_regs_duplicados.
	*
	* @param strTrans the str trans
	* @param req the req
	* @param res the res
	* @return the int
	* @throws ServletException the servlet exception
	* @throws IOException Signals that an I/O exception has occurred.
	*/
	public int pantalla_valida_regs_duplicados(String strTrans, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

		String tdc2chq = (req.getParameter("TDC2CHQ")!=null)?(String)req.getParameter("TDC2CHQ"):"";
		EIGlobal.mensajePorTrace("***transferencia.pantalla_valida_regs_duplicados -> Entrando", EIGlobal.NivelLog.DEBUG);
	 	EIGlobal.mensajePorTrace("***transferencia.pantalla_valida_regs_duplicados -> TDC2CHQ ["+tdc2chq+"]", EIGlobal.NivelLog.DEBUG);

		tipo_operacion=CAD_TIP_OP_TRAN;
		String cta_origen="";
		String cta_destino="";


		String importe_string="";
		double importe=0.0;
		
		String concepto="";
		String sfecha_programada="";

		Double importedbl;
		@SuppressWarnings("unused")
		String tipo_cta_origen="";
		@SuppressWarnings("unused")
		String tipo_cta_destino="";

		String cadena_cta_origen="";
		String cadena_cta_destino="";
		
		String arreglo_ctas_origen="";
		String arreglo_ctas_destino="";
		
		String arreglo_importes="";
		String arreglo_fechas="";
		String arreglo_conceptos="";
		String arreglo_rfcs="";
		String arreglo_ivas="";

		String arreglo_estatus="";
		int contador =0;
		
		String scontador="";
		@SuppressWarnings("unused")
		int indice=0;
		String fecha="";
		String bandera_estatus="";
	
		String imptabla = "";


        String TipodeTrans="";

		String RFC = "";


		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


		String pmuser      = session.getUserID8();
		String devicePrint = (String)req.getParameter ("pmdevice")==null?"":(String)req.getParameter ("pmdevice");
		String FSOanterior = (String)req.getParameter ("fsoantes")==null?"":(String)req.getParameter ("fsoantes");
		String ipuser      = (String)req.getRemoteAddr();

		double importeOTP = 0.0;

		if(Global.ACTIVAR_PASSMARK.equals("1"))
		{

		}
		else
		{
			FSOanterior = null;
		}


	    //Declaracion Objeto para la copia remota a desabsm y del nombre de Archivo
		ArchivoRemoto archR = new ArchivoRemoto();
		ArchivoRemoto archR2 = new ArchivoRemoto();	// Para la copia de Exportar Archivo.
	    String nombreArchivo= session.getUserID8()+".trn";

		String[] arrCuentas=new String[4]; //Numero de cuentas  X 3 (cuenta|tipo|descripcion|)

		scontador=getFormParameter(req,"contador2");

		contador=Integer.parseInt(scontador);
		arreglo_ctas_origen= getFormParameter(req,"arregloctas_origen2");
		arreglo_ctas_destino= getFormParameter(req,"arregloctas_destino2");
		arreglo_importes= getFormParameter(req,"arregloimportes2");
		arreglo_fechas= getFormParameter(req,"arreglofechas2");
		arreglo_conceptos= getFormParameter(req,"arregloconceptos2");
		arreglo_ivas=req.getParameter("arregloivas2");
		arreglo_rfcs=req.getParameter("arreglorfcs2");

		// Se incluyen los parmetros con la lista de ivas y rfcs para trasladarlos a siguiente pantalla
		if (contador > Global.MAX_REGISTROS){
			EIGlobal.mensajePorTrace("everis contador mayor a" +Global.MAX_REGISTROS +" removiendo atributos de rfc e ivas", EIGlobal.NivelLog.INFO);
			req.removeAttribute("arregloivas2");
			req.removeAttribute("arreglorfcs2");
		}else{
			req.setAttribute("arregloivas2", arreglo_ivas);
			req.setAttribute("arreglorfcs2", arreglo_rfcs);
		}

		arreglo_estatus=getFormParameter(req,"arregloestatus2");
		imptabla=getFormParameter(req,"imptabla");

		//parametro empleado en ejecuci? de operaciones con tabla.
		TipodeTrans=req.getParameter ("TIPOTRANS");
		req.setAttribute("TIPOTRANS", TipodeTrans);

		// Parametro que identifica el valor de transaccion, si es pesos, dolares, etc.
		req.setAttribute("trans",(String) req.getParameter ("trans"));

        //Desarmar arreglos para obtener solo los datos
		String[] Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');
        String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
        String[] Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
        String[] Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
        String[] Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');
        String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');
		String[] RFCs		  = desentramaC(scontador+"@"+arreglo_rfcs,'@');

        String arreglodecuentas_origen="\""+arreglo_ctas_origen+"\"";
        String arreglodecuentas_destino="\""+arreglo_ctas_destino+"\"";
        String arreglode_importes="\""+arreglo_importes+"\"";
        String arreglode_fechas="\""+arreglo_fechas+"\"";
        String arreglode_conceptos="\""+arreglo_conceptos+"\"";

//Codificacion para Armar el Archivo de Envio de Validaci? de registros Duplicados.

		EI_Exportar ArcSalida=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
		EIGlobal.mensajePorTrace("TransferenciasInternas pantalla_valida_regs_dup...(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

		String LineaArchivo ="";
		/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 se agregan variables para
		 * identificar los favoritos */
		String esFavorito = "";
		String[] losFavoritos = null;
		if( "0".equals( strTrans ) || "2".equals( strTrans ) ){
			esFavorito = req.getParameter("strFav");
			if( esFavorito != null && !"".equals( esFavorito ) ){
				EIGlobal.mensajePorTrace("esFavorito["+esFavorito+"]", EIGlobal.NivelLog.INFO);
				losFavoritos = esFavorito.split("@");
			}
		}
		/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
		if(ArcSalida.creaArchivo())
		{
			boolean llamaServicio = true;
			int indice1=1;
			for (indice1=1; indice1<=contador; indice1++)
			{

				LineaArchivo = "";  //Limpiar para cargar cada l?nea del Archivo.
				bandera_estatus=Estatus[indice1];

				if (bandera_estatus.equals("1"))
				{

					cadena_cta_origen  = Ctas_origen[indice1];
					cadena_cta_destino = Ctas_destino[indice1];
					importe_string     = Importes[indice1];
					concepto           = Conceptos[indice1];
					fecha              = Fechas[indice1];
					RFC				   = RFCs[indice1];

					String desc_cta_origen = "";
					if(TipodeTrans.equals("LINEA")){
						arrCuentas= desentramaC("3|" + cadena_cta_origen,'|');
						desc_cta_origen = arrCuentas[3]; // PYME FSW - INDRA MARZO 2015 se agrega variable
					} else {
						arrCuentas= desentramaC("2|" + cadena_cta_origen,'|');
						desc_cta_origen = arrCuentas[1].substring( arrCuentas[1].substring( 0, arrCuentas[1].indexOf(" ")).length() );
					}

					cta_origen = arrCuentas[1];
					tipo_cta_origen=arrCuentas[2];


                    if (cadena_cta_destino.indexOf((int)'|') == -1 ){
						cadena_cta_destino = cadena_cta_destino + "|NR||" ;
                    }
                    String desc_cta_destino = "";
					if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA")){
						arrCuentas= desentramaC("3|" + cadena_cta_destino,'|');
						desc_cta_destino = arrCuentas[3]; // PYME FSW - INDRA MARZO 2015 se agrega variable
					} else {
						arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');
						desc_cta_destino = arrCuentas[1].substring( arrCuentas[1].substring( 0, arrCuentas[1].indexOf(" ")).length() );
					}

                    cta_destino = arrCuentas[1];
					tipo_cta_destino=arrCuentas[2];
					tipo_operacion=ObtenTipoCuenta2(cta_destino, strTrans, llamaServicio, session, req);
					llamaServicio = false;
					if(tipo_operacion.equals("TARJCRED")) {
					  tipo_operacion=CAD_TIP_OP_PAGT;
				  	}
				    else {
					  tipo_operacion=CAD_TIP_OP_TRAN;
	  			  	}

					importedbl = new Double (importe_string);
					importe = importedbl.doubleValue();
					sfecha_programada=fecha.substring(0,2)+'/'+fecha.substring(3,5)+'/'+fecha.substring(6,fecha.length());

					//Linea para archivo.
					LineaArchivo+= ArcSalida.formateaCampo(trimCuenta(cta_origen).trim(),11) + "     ";
					LineaArchivo+= ArcSalida.formateaCampo(trimCuenta(cta_destino).trim(),16) + "     ";
					LineaArchivo+= ArcSalida.formateaCampo(importe_string,13);
					LineaArchivo+= ArcSalida.formateaCampo(concepto,40) + "          ";
					LineaArchivo+= ArcSalida.formateaCampo(sfecha_programada,10)+" ";
					LineaArchivo+= ArcSalida.formateaCampo(tipo_operacion, 4);
					LineaArchivo+= ArcSalida.formateaCampo(RFC, 13);

					//Visualizar los campos desarmados previos a armar el archivo
					EIGlobal.mensajePorTrace("***Cta de Origen:  "+cta_origen, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Cta de Destino: "+cta_destino, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Importe:        "+importe, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Fecha:          "+sfecha_programada, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Tip de Op:     "+sfecha_programada, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***RFC:     		"+RFC, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***LineaArchivo:   '"+LineaArchivo+"'", EIGlobal.NivelLog.INFO);


					if(Global.ACTIVAR_PASSMARK.equals("1") && ((String)req.getParameter ("TIPOTRANS")).equals("LINEA") && ((String)req.getParameter ("trans")).equals("0")  )
					{
						String FSOnuevo = CallPassmark(pmuser,devicePrint,ipuser,FSOanterior,importe_string);
						FSOanterior=FSOnuevo;
					}
					/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 dar de alta favoritos
					 * se obtienen datos para armar bean */
					if( losFavoritos != null && ( "0".equals( strTrans ) || "2".equals( strTrans ) ) ){
						int l = losFavoritos.length;
						EIGlobal.mensajePorTrace("losFavoritos.length["+losFavoritos.length+"]", EIGlobal.NivelLog.INFO);
						for( int r = 0 ; r < l ; r+=2 ){
							EIGlobal.mensajePorTrace("losFavoritos["+r+"]["+losFavoritos[r]+"]", EIGlobal.NivelLog.INFO);
							if( Integer.parseInt( losFavoritos[r] ) == indice1 ){
								// este registro si es un favorito, insertar

								if( !"LINEA".equals(TipodeTrans) ){
									cta_origen = cta_origen.substring( 0, cta_origen.indexOf(" ") );
								}
								if( !"LINEA".equals((String) req.getParameter ("TIPOTRANS")) ){
									cta_destino = cta_destino.substring( 0, cta_destino.indexOf(" ") );
								}
								String datos = new StringBuffer( session.getContractNumber() ).append(SEPARADOR_CHR_ARROBA)
									.append( session.getUserID8() ).append(SEPARADOR_CHR_ARROBA)
									.append( cta_origen ).append(SEPARADOR_CHR_ARROBA)
									.append( desc_cta_origen.trim() ).append(SEPARADOR_CHR_ARROBA)
									.append( cta_destino ).append(SEPARADOR_CHR_ARROBA)
									.append( desc_cta_destino.trim() ).append(SEPARADOR_CHR_ARROBA)
									.append( concepto ).append(SEPARADOR_CHR_ARROBA)
									.append( importe ).append(SEPARADOR_CHR_ARROBA)
									.append( losFavoritos[r+1] ).toString();
								EIGlobal.mensajePorTrace("datos["+datos+"]", EIGlobal.NivelLog.INFO);
								if( "0".equals( strTrans ) ){
									darDeAltaFavorito(datos, CAD_TIP_OP_TRAN);
								} else if(  "2".equals( strTrans )  ){
									darDeAltaFavorito(datos, CAD_TIP_OP_PAGT);
								}

								r = l;
							}
						}
					}
					/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 dar de alta favoritos */
					LineaArchivo+="\n";
					if(!ArcSalida.escribeLinea(LineaArchivo)){ //Se graba L?nea al Archivo de Salida de validaci?.
					    EIGlobal.mensajePorTrace("TransferenciasInternas pantalla_valida_regs_dup...(): Algo salio mal escribiendo ", EIGlobal.NivelLog.ERROR);
					    ArcSalida.cierraArchivo();
					}
					importeOTP = importeOTP + importe;
				} //Termina if(bandera_estatus)
			} //Termina for(indice1 ....)

			EIGlobal.mensajePorTrace("�������������������������������� importeOTP ->" + importeOTP, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("�������������������������������� indice1 ->" + indice1, EIGlobal.NivelLog.INFO);
			sess.setAttribute("importeTran", importeOTP);
			sess.setAttribute("registrosTran", indice1-1);

		}

		//Cerrar el Archivo de validaci?.
		ArcSalida.cierraArchivo();

		if(FSOanterior!=null)
			req.setAttribute("_fsonuevotrx",FSOanterior);


		if(archR.copiaLocalARemoto(session.getUserID8()+".trn"))
		{
			EIGlobal.mensajePorTrace("Transferencias Internas - pantalla_valida_regs_dup....(): el archivo .trn se copio correctamente", EIGlobal.NivelLog.DEBUG);
		}
		else
		{
			EIGlobal.mensajePorTrace("Transferencias Internas - pantalla_valida_regs_dup....(): error al copiar el archivo .trn", EIGlobal.NivelLog.ERROR);
		}

//Codigo para petici? servicio validaci? registros duplicados via Tuxedo.

		tipo_operacion = "VALF";
		String tramaEnviada="1EWEB|"+session.getUserID8()+"|" + tipo_operacion + "|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|1|";

		//ivn Q05-0001087 inicio lineas agregadas
		String IP = " ",fechaHr = "";										//variables locales al mtodo
		IP = req.getRemoteAddr();											//ObtenerIP
		java.util.Date fechaHrAct = new java.util.Date();
		SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
		fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";				//asignaci? de fecha y hora



		EIGlobal.mensajePorTrace(fechaHr+"Transferencias Internas - pantalla_valida_regs_dup....(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);

		ServicioTux tuxGlobal = new ServicioTux();

		Hashtable htResult=null;

		try{
			htResult=tuxGlobal.web_red(tramaEnviada);
		}
		catch(java.rmi.RemoteException re){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

		}

//Codigo para la recepcion de la respuesta del Servicio Tuxedo de validaci? registros Duplicados
		   String CodError = null;
		   if (htResult != null) {
			   CodError = ( String ) htResult.get( "BUFFER" );
		   }

		EIGlobal.mensajePorTrace("Transferencias Internas - pantalla_valida_regs_dup....(): El servicio regreso CodError: "+CodError, EIGlobal.NivelLog.DEBUG);

		if(CodError == null) {
			CodError = "VALF9999@Error al obtener respuesta.";
		}

		StringTokenizer separador = new StringTokenizer(CodError, "@");
		String[] result = new String[3];
		int i =0;
		while (separador.hasMoreTokens())
		{
			EIGlobal.mensajePorTrace("Dentro del ciclo For ", EIGlobal.NivelLog.DEBUG);
			result[i]= separador.nextToken();
			EIGlobal.mensajePorTrace("Transferencias Internas - pantalla_valida_regs_dup....(): Elementito " + i + " es : "+ result[i], EIGlobal.NivelLog.DEBUG);
			i++;
		}

		int NumeroError = Integer.parseInt(result[0].substring(4,8));
		// Validacin de Registros Duplicados

		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try {
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();;
			bt = (BitaTransacBean)bh.llenarBean(bt);
			if (((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA)){
				bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_VALIDA_DUPLICADOS);
				bt.setTipoMoneda("MN");
			}
			if (((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES)){
				bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_VALIDA_DUPLICADOS);
				bt.setTipoMoneda("USD");
			}
			if (((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_TARJETA_CREDITO_PAGO)){
				bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_VALIDA_DUPLICADOS);
				bt.setTipoMoneda("MN");
			}
			if (((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION)){
				bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_VALIDA_DUPLICADOS);
				bt.setTipoMoneda("MN");
			}
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			bt.setServTransTux("VALF");

			if(result[0]!=null){
    			if(result[0].substring(0,2).equals("OK")){
    				bt.setIdErr("VALF0000");
    			}else if(result[0].length()>8){
	    			bt.setIdErr(result[0].substring(0,8));
	    		}else{
	    			bt.setIdErr(result[0].trim());
	    		}
    		}

			bt.setBancoDest("SANTANDER");
			if(req.getAttribute("divisa") != null){
				bt.setTipoMoneda(req.getAttribute("divisa").toString());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			log(e.getMessage());
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			log(e.getMessage());
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		 }

		if ((NumeroError==0) && !(result[2].trim().equals("-")))
		{

			long posicionReg = 0;
			long residuo	 = 0;

			String nombreArchRegreso = result[2].substring(result[2].lastIndexOf('/')+1);

			MDI_Importar ArchImpRegreso = new MDI_Importar();
			ArchImpRegreso.nuevoFormato=false;


			BufferedReader arch	= null;


			try
			{
				if(archR.copia(nombreArchRegreso)){
					EIGlobal.mensajePorTrace("***Transferencias Internas, el archivo trnr se copio correctamente", EIGlobal.NivelLog.DEBUG);}
				else{
					EIGlobal.mensajePorTrace("***Transferencias Internas, el archivo trnr no se copio correctamente", EIGlobal.NivelLog.ERROR);}

// Se arma la cabecera de la tabla a Desplegar en el Jsp de Regs Duplicados
				String contenidoArchivoStr = "";
				contenidoArchivoStr="<table width=\"641\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style='margin-left:5%; margin-right:auto; border: 0px;'>"+//FSWI Se modifica para centrar tabla
							        "<tr> "+
									"<td class=\"textabref\">Transferencias Internas</td>"+
									"</tr>"+

									"<tr>"+
									"<td align=center class=\"titpag\">Listado de Registros Duplicados</td>"+
									"</tr>"+


									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\" style='margin-left:5%; margin-right:auto; border: 0px;'>"+
									"<tr> "+

									"<td align=center width=200 class=tittabdat>Cuenta cargo</td>"+

									"<td align=center width=200 class=tittabdat>" + ("PESOS".equals(sess.getAttribute("TIPO_DIVISA")) ? "Cuenta abono&nbsp;/&nbsp;M&oacute;vil" : "Cuenta abono") + "</td>"+
									"<td align=center width=90 class=tittabdat>Importe</td>"+
									"<td align=center class=tittabdat>Concepto</td>"+
									"<td align=center width=60 class=tittabdat>Fecha aplicaci&oacute;n</td>"+
									"</tr>";

// Extraer los registros del archivo y depositarlos en la tabla.

				String aux_nombreArchRegreso = nombreArchRegreso;

				nombreArchRegreso=Global.DIRECTORIO_LOCAL+"/"+nombreArchRegreso;

				arch	= new BufferedReader( new FileReader(nombreArchRegreso) );

				String linea;
				@SuppressWarnings("unused")
				String cad_decimal="";
				boolean envia_descarga= true;

				//Se trata de generar la copia remota del Archivo
				if ( archR2.copiaLocalARemoto( aux_nombreArchRegreso,"WEB") )
				{
					EIGlobal.mensajePorTrace("Transferencias Internas - execute(): Copia Exportacion. "+ aux_nombreArchRegreso, EIGlobal.NivelLog.DEBUG);
					session.setRutaDescarga( "/Download/" + aux_nombreArchRegreso );
					req.setAttribute("DescargaArchivo",session.getRutaDescarga());
					envia_descarga = true;
				}
				else
				{
					EIGlobal.mensajePorTrace("Transferencias Internas - execute(): No se pudo copiar el archivo."+ aux_nombreArchRegreso, EIGlobal.NivelLog.ERROR);

					envia_descarga = false;
				}

				/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015
				 * Exportaci�n de duplicados
				 */
				ExportModel em = new ExportModel("ctasAutorizacionCancelacion", '|');
				em.addColumn(new Column(0, 11, "Cuenta cargo"));
				em.addColumn(new Column(16, 32, "PESOS".equals(sess.getAttribute("TIPO_DIVISA")) ? "Cuenta abono / M�vil" : "Cuenta abono"));
				em.addColumn(new Column(37, 49, "Importe", "$####################0.00"));
				em.addColumn(new Column(50, 80, "Concepto"));
				em.addColumn(new Column(100, 110, "Fecha aplicaci�n"));

				req.setAttribute("em", "em");
			    req.setAttribute("af", "af");
			    req.setAttribute("metodoExportacion", "ARCHIVO");
			    String archExportar = "/proarchivapp/WebSphere8/enlace/estatico/Download/".concat(aux_nombreArchRegreso);
			    EIGlobal.mensajePorTrace("Transferencias Internas - Exportaci�n: Ruta de archivo. ["+ archExportar+"]", EIGlobal.NivelLog.DEBUG);
				sess.setAttribute("af", archExportar);
				sess.setAttribute("em", em);

				String tablaExportar = "<table width=800px cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" style=\"margin-left:5%; margin-right:auto; border: 0px;\">"
				.concat("<tbody><tr><td width=\"35%\"><td class=\"textabref\" style=\"text-align:left;\" colspan=\"2\">Exporta en TXT <input type=\"radio\" name=\"tipoArchivo\" value=\"txt\"></td></tr>")
				.concat("<tr><td width=\"35%\"><td class=\"textabref\" style=\"text-align:left;\" colspan=\"2\">Exporta en XLS <input type=\"radio\" checked=\"\" name=\"tipoArchivo\" value=\"csv\"><br><br></td></tr>")
				.concat("<tr><td width=\"1%\"></td><td align=\"left\"><a border=\"0\" href=\"javascript:exportacion()\"><img border=\"0\" alt=\"Exportar\" src=\"/gifs/EnlaceMig/gbo25230.gif\"></a></td></tr></tbody></table>");
				req.setAttribute("botonesD", tablaExportar);

				/** FIN CAMBIO PYME FSW - INDRA MARZO 2015
				 * Exportaci�n de duplicados
				 */

//Bloque tal cual se hace en Nomina Interbancaria
				while ((linea = arch.readLine()) !=null)
				{

					indice = -1;
					cad_decimal = "";
					if( linea.trim().equals(""))
						linea = arch.readLine();
					if( linea == null )
						break;
					EIGlobal.mensajePorTrace("Transferencias Internas linea: "+linea, EIGlobal.NivelLog.DEBUG);

					posicionReg++;
					residuo=posicionReg % 2 ;
					String bgcolor="textabdatobs";
					if (residuo == 0){
						bgcolor="textabdatcla";
					}
					if ( posicionReg <=30)
					{

						contenidoArchivoStr=contenidoArchivoStr+"<tr>";
						contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(0,11).trim()+"&nbsp;</td>"; // Cuenta de Cargo
						contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(16,32).trim()+"&nbsp;</td>"; // Cuenta de Abono
						//contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(37,50).trim()+"&nbsp;</td>"; // Importe
						//PYME 2015 INDRA se modifica para dar formato al importe
						contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+FormatoMoneda(linea.substring(37,50).trim())+"&nbsp;</td>"; // Importe
						contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(50,80).trim()+"&nbsp;</td>"; // Concepto

						contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(100,102).trim()+'/'+linea.substring(103,105).trim()+'/'+linea.substring(106,109).trim()+"&nbsp;</td>"; // Fecha de Aplicacion
						contenidoArchivoStr=contenidoArchivoStr+"</tr>";

					}
				}


				arch.close();


				contenidoArchivoStr=contenidoArchivoStr+"</table>"; // Cerrar tabla.


				req.setAttribute("ContenidoArchivoStr",contenidoArchivoStr);

				String mensajeHTML="\n  <br>RESULTADO<br>";
				mensajeHTML+="\n <br>";
				mensajeHTML+="\n  VALIDACION : EXISTEN REGISTROS DUPLICADOS.<br>";

				String InfoUser="";
				if (posicionReg <=30)
				{

					InfoUser= "<br>Esta  transmisi&oacute;n contiene registros duplicados, " +
							"si desea realizar el envi&oacute; de cualquier manera,"+
								"capture su contrase�a din&aacute;mica y presione Aceptar";
				}
				else
				{
					InfoUser= "<br>Esta  transmisi&oacute;n contiene registros duplicados, " +
							"se muestran solo los primeros 30 registros, pero el n&uacute;mero puede ser mayor."+
							"Exporte la informaci&oacute;n para ver el total de registros duplicados."+
							"S&iacute; desea enviarlo(s) de cualquier manera, capture su contrase�a din&aacute;mica y presione Aceptar";
				}

				InfoUser="cuadroDialogoMedidas(\""+ InfoUser +"\",1,380,220)";
   				req.setAttribute("InfoUser", InfoUser);

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
		//Manejo del tipo de Encabezado sean pesos o dolares.
		if (strTrans.equals("0"))
		{
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55270",req ));
			sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55270",req ));
		}
		else if (strTrans.equals("1"))
		{

			req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares. Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55270a",req));
			sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares. Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55270a",req));
		}
		else if (strTrans.equals("2"))
		{

			req.setAttribute("Encabezado", CreaEncabezado("Pago a Tarjeta de Crdito. Existen Registros Duplicados","Transferencias &gt  Pago a Tarjeta de Crdito","s55270a",req));
			sess.setAttribute("Encabezado", CreaEncabezado("Pago a Tarjeta de Crdito. Existen Registros Duplicados","Transferencias &gt  Pago a Tarjeta de Crdito","s55270a",req));
		}

		req.setAttribute("Mensaje",despliegaMensaje(mensajeHTML));

         //Tomados del Envio original
		if ( contador > Global.MAX_REGISTROS ){
			EIGlobal.mensajePorTrace("everis D contador mayor a "+Global.MAX_REGISTROS+" registros removiendo atributos", EIGlobal.NivelLog.DEBUG);
			req.removeAttribute("arregloctas_origen2");
			req.removeAttribute("arregloctas_destino2");
			req.removeAttribute("arregloimportes2");
			req.removeAttribute("arregloconceptos2");
			req.removeAttribute("arreglofechas2");
			req.removeAttribute("arregloestatus2");
			EIGlobal.mensajePorTrace("\n everis Borrando valores FIN \n", EIGlobal.NivelLog.DEBUG);
		}
		else{
			req.setAttribute("arregloctas_origen2", arreglodecuentas_origen);
			req.setAttribute("arregloctas_destino2", arreglodecuentas_destino);
			req.setAttribute("arregloimportes2", arreglode_importes);
			req.setAttribute("arregloconceptos2", arreglode_conceptos);
			req.setAttribute("arreglofechas2", arreglode_fechas);
			req.setAttribute("arregloestatus2", arreglo_estatus);
		}
		req.setAttribute("contador2",Integer.toString(contador));
		req.setAttribute("imptabla", imptabla);
		req.setAttribute("TDC2CHQ", tdc2chq);

		if ( envia_descarga == true ){
			req.setAttribute("botontransferir","<td align=right><a href=\"javascript:ejecutar();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:regresar();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25320.gif></a></td><td align=left><a href='/Download/"+aux_nombreArchRegreso+"'><img border=0  src=/gifs/EnlaceMig/gbo25230.gif></a></td>");}
		else{
			req.setAttribute("botontransferir","<td align=right><a href=\"javascript:ejecutar();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:regresar();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25320.gif></a></td><td align=left><a href=\"javascript:js_exportar_aviso();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25230.gif></a></td>");}
		/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
		 * ahora continuara haciendo la validacion del token y despues a mostrar
		 * el jsp comentado*/
		if( !"0".equals( strTrans ) && !"2".equals( strTrans )){

			evalTemplate("/jsp/transferencia_val_duplicados.jsp", req, res);
		}
		/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */

		req.setAttribute("web_application",Global.WEB_APPLICATION);
		req.setAttribute("banco",session.getClaveBanco());



		}catch(Exception e) {}


		finally{
			if ( null != arch ){
				try{
					arch.close();
				}catch (Exception e) {

				}
				arch = null;
			}
		}


		}
	    else if ((NumeroError==0) && (result[2].trim().equals("-")))
			//Corresponde al Caso donde no hay registros Duplicados.
		{
	    	EIGlobal.mensajePorTrace("Transferencias Internas linea: Caso donde no hay registros Duplicados", EIGlobal.NivelLog.DEBUG);
			String mensajeHTML="\n  <br>RESULTADO<br>";
			mensajeHTML+="\n <br>";
			mensajeHTML+="\n  VALIDACION EXITOSA, NO EXISTEN REGISTROS DUPLICADOS.<br>";
			if ( "0".equals(strTrans) || "1".equals(strTrans) ){
				String InfoUser= "<br>Validaci&oacute;n Exitosa:  No existen registros duplicados. " +
				 "Ahora puede enviar las operaciones colocando su contrase�a din&aacute;mica" +
				 " y presionando Aceptar. ";

				InfoUser="cuadroDialogoMedidas(\""+ InfoUser +"\",1,380,220)";
				req.setAttribute("InfoUser", InfoUser);
			}

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());

			//Manejo del tipo de Encabezado sean pesos o dolares.
			if (strTrans.equals("0"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. No Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55270",req ));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. No Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55270",req ));
			}
			else if (strTrans.equals("1"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares. No Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55270a",req));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares. No Existen Registros Duplicados","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55270a",req));
			}
			else if (strTrans.equals("2"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Pago a Tarjeta de Cr&eacute;dito. No Existen Registros Duplicados","Transferencias &gt  Pago a Tarjeta de Cr&eacute;dito","s55270a",req));
				sess.setAttribute("Encabezado", CreaEncabezado("Pago a Tarjeta de Cr&eacute;dito. No Existen Registros Duplicados","Transferencias &gt  Pago a Tarjeta de Cr&eacute;dito","s55270a",req));
			}


			req.setAttribute("Mensaje",despliegaMensaje(mensajeHTML));

			if ( contador > Global.MAX_REGISTROS ){
				EIGlobal.mensajePorTrace("\neveris ND contador mayor a "+Global.MAX_REGISTROS+" registros removiendo atributos", EIGlobal.NivelLog.DEBUG);
				req.removeAttribute("arregloctas_origen2");
				req.removeAttribute("arregloctas_destino2");
				req.removeAttribute("arregloimportes2");
				req.removeAttribute("arregloconceptos2");
				req.removeAttribute("arreglofechas2");
				req.removeAttribute("arregloestatus2");
				EIGlobal.mensajePorTrace("everis Borrando valores FIN \n", EIGlobal.NivelLog.DEBUG);
			}
			else{
		    	EIGlobal.mensajePorTrace("Transferencias Internas linea: contador < Global.MAX_REGISTROS", EIGlobal.NivelLog.DEBUG);
				req.setAttribute("arregloctas_origen2", arreglodecuentas_origen);
				req.setAttribute("arregloctas_destino2", arreglodecuentas_destino);
				req.setAttribute("arregloimportes2", arreglode_importes);
				req.setAttribute("arregloconceptos2", arreglode_conceptos);
				req.setAttribute("arreglofechas2", arreglode_fechas);
				req.setAttribute("arregloestatus2", arreglo_estatus);
			}
			req.setAttribute("contador2",Integer.toString(contador));
			req.setAttribute("imptabla", imptabla);
			req.setAttribute("TDC2CHQ", tdc2chq);
			req.setAttribute("botontransferir","<td align=right><a href=\"javascript:ejecutar();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=left><a href=\"javascript:regresar();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25320.gif></a></td>");
			/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
			 * Se agrega el if para que no afecte con el funcionamiento normal
			 * de operaciones que sean diferentes de 0 (MN)y 2 (Pago TDC),
			 * es decir siga mostrando los botones de transferir sin el token*/
			if( !"0".equals( strTrans ) && !"2".equals( strTrans )){

				evalTemplate("/jsp/transferencia_val_duplicados.jsp", req, res);
			}
			/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
			req.setAttribute("web_application",Global.WEB_APPLICATION);
            req.setAttribute("banco",session.getClaveBanco());


		}
		else if (NumeroError>0)
		{
			despliegaPaginaErrorURL(result[1],"Transferencias Mismo Banco","Transferencias > Mismo Banco" ,"", req, res);
		}


		return 1;

	}
	/**
	 * Metodo para dar de alta favoritos cuando es importacion por archivo y sean menos registros
	 * que el maximo
	 * @since 12/05/2015
	 * @author FSW-Indra
	 * @param req req informacion
	 * @param res respuesta
	 * @param strTrans tipo de transaccion
	 */
	public void favoritosParaArchivo( HttpServletRequest req, HttpServletResponse res, String strTrans ){
		String cta_origen="";
		String cta_destino="";
		String importe_string="";
		double importe=0.0;

		String concepto="";
		Double importedbl;

		String tipo_cta_origen="";
		String tipo_cta_destino="";

		String cadena_cta_origen="";
		String cadena_cta_destino="";

		String arreglo_ctas_origen="";
		String arreglo_ctas_destino="";

		String arreglo_importes="";
		String arreglo_fechas="";

		String arreglo_conceptos="";
		String arreglo_rfcs="";
		String arreglo_ivas="";

		String arreglo_estatus="";

		int contador =0;
		String scontador="";
		int indice=0;
		String fecha="";

		String bandera_estatus="";
        String TipodeTrans="";
		String RFC = "";
		double importeOTP = 0.0;
		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		String[] arrCuentas=new String[4];

		scontador=getFormParameter(req,"contador2");

		contador=Integer.parseInt(scontador);
		arreglo_ctas_origen= getFormParameter(req,"arregloctas_origen2");
		arreglo_ctas_destino= getFormParameter(req,"arregloctas_destino2");
		arreglo_importes= getFormParameter(req,"arregloimportes2");
		arreglo_fechas= getFormParameter(req,"arreglofechas2");
		arreglo_conceptos= getFormParameter(req,"arregloconceptos2");
		arreglo_ivas=req.getParameter("arregloivas2");
		arreglo_rfcs=req.getParameter("arreglorfcs2");
		if (contador > Global.MAX_REGISTROS){
			req.removeAttribute("arregloivas2");
			req.removeAttribute("arreglorfcs2");
		}else{
			req.setAttribute("arregloivas2", arreglo_ivas);
			req.setAttribute("arreglorfcs2", arreglo_rfcs);
		}

		arreglo_estatus=getFormParameter(req,"arregloestatus2");

		TipodeTrans=req.getParameter ("TIPOTRANS");
		req.setAttribute("trans",(String) req.getParameter ("trans"));
		String[] Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');
        String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
        String[] Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
        String[] Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
        String[] Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');
        String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');
		String[] RFCs		  = desentramaC(scontador+"@"+arreglo_rfcs,'@');
		String esFavorito = "";
		String[] losFavoritos = null;
		if( "0".equals( strTrans ) || "2".equals( strTrans ) ){
			esFavorito = req.getParameter("strFav");
			if( esFavorito != null && !"".equals( esFavorito ) ){
				EIGlobal.mensajePorTrace("esFavorito["+esFavorito+"]", EIGlobal.NivelLog.INFO);
				losFavoritos = esFavorito.split("@");
			}
		}
		int indice1=1;
		for (indice1=1; indice1<=contador; indice1++)
		{
			bandera_estatus=Estatus[indice1];
			if (bandera_estatus.equals("1"))
			{
				cadena_cta_origen  = Ctas_origen[indice1];
				cadena_cta_destino = Ctas_destino[indice1];
				importe_string     = Importes[indice1];
				concepto           = Conceptos[indice1];
				fecha              = Fechas[indice1];
				RFC				   = RFCs[indice1];

				String desc_cta_origen = "";
				if(TipodeTrans.equals("LINEA")){
					arrCuentas= desentramaC("3|" + cadena_cta_origen,'|');
					desc_cta_origen = arrCuentas[3];
				} else {
					arrCuentas= desentramaC("2|" + cadena_cta_origen,'|');
					desc_cta_origen = arrCuentas[1].substring( arrCuentas[1].substring( 0, arrCuentas[1].indexOf(" ")).length() );
				}

				cta_origen = arrCuentas[1];
				tipo_cta_origen=arrCuentas[2];

                if (cadena_cta_destino.indexOf((int)'|') == -1 ){
					cadena_cta_destino = cadena_cta_destino + "|NR||" ;
                }
                String desc_cta_destino = "";
				if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA")){
					arrCuentas= desentramaC("3|" + cadena_cta_destino,'|');
					desc_cta_destino = arrCuentas[3];
				} else {
					arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');
					desc_cta_destino = arrCuentas[1].substring( arrCuentas[1].substring( 0, arrCuentas[1].indexOf(" ")).length() );
				}
                cta_destino = arrCuentas[1];
				importedbl = new Double (importe_string);
				importe = importedbl.doubleValue();

				if( losFavoritos != null && ( "0".equals( strTrans ) || "2".equals( strTrans ) ) ){
					int l = losFavoritos.length;
					EIGlobal.mensajePorTrace("losFavoritos.length["+losFavoritos.length+"]", EIGlobal.NivelLog.INFO);
					for( int r = 0 ; r < l ; r+=2 ){
						EIGlobal.mensajePorTrace("losFavoritos["+r+"]["+losFavoritos[r]+"]", EIGlobal.NivelLog.INFO);
						if( Integer.parseInt( losFavoritos[r] ) == indice1 ){
							if( !"LINEA".equals(TipodeTrans) ){
								cta_origen = cta_origen.substring( 0, cta_origen.indexOf(" ") );
							}
							if( !"LINEA".equals((String) req.getParameter ("TIPOTRANS")) ){
								cta_destino = cta_destino.substring( 0, cta_destino.indexOf(" ") );
							}
							String datos = new StringBuffer( session.getContractNumber() ).append(SEPARADOR_CHR_ARROBA)
								.append( session.getUserID8() ).append(SEPARADOR_CHR_ARROBA)
								.append( cta_origen ).append(SEPARADOR_CHR_ARROBA)
								.append( desc_cta_origen.trim() ).append(SEPARADOR_CHR_ARROBA)
								.append( cta_destino ).append(SEPARADOR_CHR_ARROBA)
								.append( desc_cta_destino.trim() ).append(SEPARADOR_CHR_ARROBA)
								.append( concepto ).append(SEPARADOR_CHR_ARROBA)
								.append( importe ).append(SEPARADOR_CHR_ARROBA)
								.append( losFavoritos[r+1] ).toString();
							EIGlobal.mensajePorTrace("datos["+datos+"]", EIGlobal.NivelLog.INFO);
							darDeAltaFavorito(datos, CAD_TIP_OP_PAGT);
							r = l;
						}
					}
				}
			}
			importeOTP = importeOTP + importe;
		}
		sess.setAttribute("importeTran", importeOTP);
		sess.setAttribute("registrosTran", indice1-1);
	}

	/**
	* Ejecucion_operaciones_transferencias_con_tabla.
	*
	* @param strTrans the str trans
	* @param clave_perfil the clave_perfil
	* @param usuario the usuario
	* @param contrato the contrato
	* @param req the req
	* @param res the res
	* @return the int
	* @throws ServletException the servlet exception
	* @throws IOException Signals that an I/O exception has occurred.
	*/
	public int ejecucion_operaciones_transferencias_con_tabla(String strTrans,String clave_perfil,String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		String tdc2chq = (req.getParameter("TDC2CHQ")!=null)?(String)req.getParameter("TDC2CHQ"):CADENA_VACIA;
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> Entrando", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> TDC2CHQ ["+tdc2chq+"]", EIGlobal.NivelLog.DEBUG);
		StringBuffer mensaje_salida = new StringBuffer("");
		String coderror="";
		String CodErrorOper="";
		tipo_operacion=CAD_TIP_OP_TRAN;
		String cta_origen="";
		String cta_destino="";

		String des_cta_origen="";
		String des_cta_destino="";
		
		String importe_string="";
		double importe=0.0;
		
		String concepto="";
		String sfecha_programada="";

		Double importedbl;
		String tipo_cta_origen="";
		String tipo_cta_destino="";
		
		String cadena_cta_origen="";
		String cadena_cta_destino="";
		
		String arreglo_ctas_origen="";
		String arreglo_ctas_destino="";
		
		String arreglo_importes="";
		String arreglo_fechas="";
		
		String arreglo_conceptos="";
		String arreglo_rfcs="";
		String arreglo_ivas="";

		String arreglo_estatus="";

		int contador =0;
		String scontador="";
		String fecha="";

		String bandera_estatus="";
		String imptabla = "";
		String tramaok="";

		String error="";

		String moduloOrigen="";
		String moduloDestino="";

		boolean  par=false;
		int contadorok=0;
		String trama_entrada="";

		String sreferencia="";
		String clase="";
		short tipo_fecha=0;

		String conceptocompleto="";
		int longi=0;
		int j=0;

		String estatusTrans = "";
		String folios = "";
		String estatus = "";


		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");

		BitaHelper bh = new BitaHelperImpl(req, session, sess);


		/***********************************Req. Q1466 Construccion variables sesion p/control recarga ****************/
		boolean banderaTransaccion = true;

		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null && verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
		{
			EIGlobal.mensajePorTrace( "Transferencias Internas El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion =false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Internas")!=null)
		{
			EIGlobal.mensajePorTrace("Transferencias Internas Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
			req.getSession().removeAttribute("Recarga_Ses_Internas");
			banderaTransaccion=false;
		}
		else
		if(verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false) )
		{
			EIGlobal.mensajePorTrace( "Transferencias Internas El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Internas")==null && !verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
        {
		    EIGlobal.mensajePorTrace( "Transferencias Internas Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
			banderaTransaccion = true;
		}


		if (banderaTransaccion || strTrans.equals("2") )
		{
			//Se crea el archivo para control de recarga sesion, y puesta de var. Recarga
			EI_Exportar arcTmp=new EI_Exportar(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses");
			if(arcTmp.creaArchivo())
				EIGlobal.mensajePorTrace( "Transferencias Internas - El archivo.ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);
			req.getSession().setAttribute("Recarga_Ses_Internas","TRUE");

			arcTmp.cierraArchivo();



		String[] arrCuentas=new String[4]; //Numero de cuentas	X 3 (cuenta|tipo|descripcion|)

		scontador=getFormParameter(req,"contador2");
		contador=Integer.parseInt(scontador);

		String encoded = getFormParameter(req,"encoded");
		if (encoded == null){
			encoded = "false";
		}


			EIGlobal.mensajePorTrace("*** jgal transferencia.ejecucion_operaciones_transferencias_con_tabla-> encoded: [" + encoded + "]", EIGlobal.NivelLog.DEBUG);
			if (encoded == null || encoded.equals("false")) {
				arreglo_ctas_origen=getFormParameter(req,"arregloctas_origen2");
				EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> arreglo_ctas_origen 1: [" + arreglo_ctas_origen + "]", EIGlobal.NivelLog.DEBUG);
				arreglo_ctas_destino=getFormParameter(req,"arregloctas_destino2");
				EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> arregloctas_destino 1: [" + arreglo_ctas_destino + "]", EIGlobal.NivelLog.DEBUG);
			} else {
				arreglo_ctas_origen = new String(getFormParameter(req,"arregloctas_origen2").getBytes("ISO-8859-1"),"UTF-8");
				EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> arreglo_ctas_origen 2: [" + arreglo_ctas_origen + "]", EIGlobal.NivelLog.DEBUG);
				arreglo_ctas_destino = new String(getFormParameter(req,"arregloctas_destino2").getBytes("ISO-8859-1"),"UTF-8");
				EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> arregloctas_destino2 2: [" + arreglo_ctas_destino + "]", EIGlobal.NivelLog.DEBUG);
			}
			arreglo_importes=getFormParameter(req,"arregloimportes2");
			arreglo_fechas=getFormParameter(req,"arreglofechas2");
			arreglo_conceptos=getFormParameter(req,"arregloconceptos2");
			arreglo_estatus=getFormParameter(req,"arregloestatus2");
			imptabla=getFormParameter(req,"imptabla");
			arreglo_ivas=getFormParameter(req, "arregloivas2");
			arreglo_rfcs=getFormParameter(req, "arreglorfcs2");


		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> imptabla "+imptabla, EIGlobal.NivelLog.DEBUG);

		String[] Ctas_origen	= {""};
		String[] Ctas_destino	= {""};
		String[] Importes		= {""};
		String[] Fechas			= {""};
		String[] Conceptos		= {""};

		try {
			Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');
			Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
			Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
			Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
			Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');
		} catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(" desentramaC con scontador "+scontador, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_ctas_origen "+arreglo_ctas_origen, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_ctas_destino "+arreglo_ctas_destino, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_importes "+arreglo_importes, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_fechas "+arreglo_fechas, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_conceptos "+arreglo_conceptos, EIGlobal.NivelLog.ERROR);
		}

		String[] Ivas	      = {""};
		String[] RFCs	      = {""};

		try{
				EIGlobal.mensajePorTrace("-------------------------------CSA-------------------------------", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("transferencia.ejecucion_operaciones_transferencias_con_tabla RFCs: "+arreglo_rfcs, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("transferencia.ejecucion_operaciones_transferencias_con_tabla Ivas: "+arreglo_ivas, EIGlobal.NivelLog.DEBUG);
				Ivas	     = desentramaC(scontador+"@"+arreglo_ivas,'@'); //*****Cambio para comprobante fiscal
				RFCs	     = desentramaC(scontador+"@"+arreglo_rfcs,'@'); //*****Cambio para comprobante fiscal
		} catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("desentramaC con scontador "+scontador, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_ivas "+arreglo_ivas, EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("desentramaC con arreglo_rfcs "+arreglo_rfcs, EIGlobal.NivelLog.ERROR);
		}

		String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');


		mensaje_salida = new StringBuffer("<table width=641 border=0 cellspacing=2 cellpadding=3 style='margin-left:5%; margin-right:auto; border: 0px;'>");//FSWI Centrar tablao
		mensaje_salida.append( "<tr><td class=textabref colspan=9>Este es el resultado de "+(tdc2chq.equals("1")?"su operaci&oacute;n":"sus operaciones")+"</td></tr>")
		.append("<TR>")
		.append("<TD align=left class=tittabdat width=200>"+(tdc2chq.equals("1")?"Tarjeta":"Cuenta")+" cargo</TD>")
		.append("<TD align=left class=tittabdat width=200>" + ("PESOS".equals(sess.getAttribute("TIPO_DIVISA")) ? "Cuenta abono&nbsp;/&nbsp;M&oacute;vil" : "Cuenta abono") + "</TD>")
		.append("<TD class=tittabdat align=center width=85 >Importe</TD>")
		.append("<TD class=tittabdat align=center width=75 >Fecha de Aplicaci&oacute;n</TD>")
		.append("<TD class=tittabdat align=center width=230>Concepto</TD>")
		.append("<TD class=tittabdat align=center width=60>Estatus</TD>")
		.append("<TD class=tittabdat align=center width=60>Referencia</TD>");

		if (!tdc2chq.equals("1"))
		{
			mensaje_salida.append("<TD class=tittabdat align=center width=80>Saldo actualizado</TD>")
				.append("<TD class=tittabdat align=center width=80>Observaciones</TD>");
		}
		mensaje_salida.append("</TR>");

		  boolean llamaServicio = true;
		  //ESC Vulnerabilidad Enlace Inicio
		  ValidaCuentaContrato validaCuentas = null;
		  validaCuentas = new ValidaCuentaContrato();

		String fileOut="";
		fileOut = session.getUserID8() + "tr.doc";
		boolean grabaArchivo = true;
		String cadena = "";
		String estatusExp = "";
		String saldoExp = "";
		EI_Exportar ArcSal=null;
		String codErrorOper="";
		double sumaImporte=0.0;
		try{

		ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
		EIGlobal.mensajePorTrace( "***Se verificar que el archivo no exista en caso contrario se borrara y crea uno nuevo", EIGlobal.NivelLog.DEBUG);
		ArcSal.creaArchivo();
		}catch(Exception e){
			EIGlobal.mensajePorTrace( "***Se verificar que el archivo->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);

		}

			int contTransRechazadas = 0;
			// Se crea mapa para envio a conector lynx
			Map<Integer, String> map = new HashMap<Integer, String>();
			if (Global.LYNX_INICIALIZADO) {
				// Se cargan valores por default para el mensaje
				EnlaceLynxConstants.cargaValoresDefaultPB(map, req);
		        map.put(7, CuentasDAO.consultarIdUsuarioContrato(contrato)); // Identificador del cliente dueno de la cuenta
				EIGlobal.mensajePorTrace("La configuracion de Lynx esta incializada y se cargaron valores default de trama PB", EIGlobal.NivelLog.DEBUG);
			}
			List<BeanDatosOperMasiva> operMasivas = null;
			boolean flgOperValidaCSD = false;
			StringBuilder cadenaOperCSD = new StringBuilder();
		  for (int indice1=1;indice1<=contador;indice1++)
		  {
			    bandera_estatus=Estatus[indice1];
				if (bandera_estatus.equals("1"))
				{
						cadena_cta_origen  = new String(Ctas_origen[indice1].toString().getBytes("ISO-8859-1"), "UTF-8");
						cadena_cta_destino  = new String(Ctas_destino[indice1].toString().getBytes("ISO-8859-1"), "UTF-8");
						importe_string	   = Importes[indice1];
						concepto	   = Conceptos[indice1];
						fecha		   = Fechas[indice1];

						if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA")){
							arrCuentas= desentramaC("3|" + cadena_cta_origen,'|');}
						else{
							arrCuentas= desentramaC("2|" + cadena_cta_origen,'|');}

						cta_origen = arrCuentas[1];
						tipo_cta_origen=arrCuentas[2];



						if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA")){
							des_cta_origen=arrCuentas[3];}
						else if(cta_origen.indexOf(' ') != -1) {
							des_cta_origen = cta_origen.substring(cta_origen.indexOf(" "));
							cta_origen = cta_origen.substring(0, cta_origen.indexOf(" "));
						}
						else {
							des_cta_origen = "";
						}


						if (cadena_cta_destino.indexOf((int)'|') == -1 )
							 cadena_cta_destino = cadena_cta_destino + "|NR||" ;

						if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA")){
							arrCuentas= desentramaC("3|" + cadena_cta_destino,'|');}
						else{
							arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');}

						cta_destino = arrCuentas[1];
						tipo_cta_destino=arrCuentas[2];


						if(((String) req.getParameter ("TIPOTRANS")).equals("LINEA"))
						{
							if(arrCuentas[3]!=null)
								des_cta_destino=arrCuentas[3];
						}
						else if(cta_destino.indexOf(' ') != -1) {
							des_cta_destino = cta_destino.substring(cta_destino.indexOf(" "));
							cta_destino = cta_destino.substring(0, cta_destino.indexOf(" "));
						}
						else {
							des_cta_destino = "";
						}


						importedbl = new Double (importe_string);
						importe = importedbl.doubleValue();
						sumaImporte+=importe;
						sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());
						tipo_fecha=checar_fecha(fecha);
						mensaje_salida.append("<TR>");
						if (par==false)
						{
							par=true;
							clase="textabdatobs";
						}
						else
						{
							par=false;
							clase="textabdatcla";
						}



			    EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> strTrans ="+strTrans, EIGlobal.NivelLog.INFO);
			    if	 ((strTrans.equals("0"))|| (strTrans.equals("1"))) {

					String tipoDEST = ObtenTipoCuenta2(cta_destino, strTrans, llamaServicio, session, req);
					llamaServicio = false;
					if(tipoDEST.equals("TARJCRED"))
					{
						tipo_operacion=CAD_TIP_OP_PAGT;
					    moduloOrigen= IEnlace.MCargo_transf_pesos;
						moduloDestino= IEnlace.MAbono_tarjeta_cred;
					}
				    else
				    {
					  tipo_operacion=CAD_TIP_OP_TRAN;

					  String tipoDivisa = (String) sess.getAttribute("TIPO_DIVISA");
					  if("PESOS".equals(tipoDivisa)){
						  moduloOrigen= IEnlace.MCargo_transf_pesos;
						  moduloDestino= IEnlace.MAbono_transf_pesos;
					  }else if("DOLARES".equals(tipoDivisa)){
						  moduloOrigen= IEnlace.MCargo_transf_dolar;
						  moduloDestino= IEnlace.MAbono_transf_dolar;

					  }
				    }
				  }
			    else if (strTrans.equals("2"))
			    {
				    tipo_operacion=CAD_TIP_OP_PAGT;
				    moduloOrigen= IEnlace.MCargo_transf_pesos;
					moduloDestino= IEnlace.MAbono_tarjeta_cred;
			    }

					    EIGlobal.mensajePorTrace("transferencia.ejecucion_operaciones_transferencias_con_tabla->tipo operacion =>>"+tipo_operacion, EIGlobal.NivelLog.DEBUG);

					    if (concepto.trim().length()==0)
					    {
						if   ((strTrans.equals("0"))|| (strTrans.equals("1"))) {

							  String tipoDEST = ObtenTipoCuenta2(cta_destino, strTrans, llamaServicio, session, req);
							  llamaServicio = false;
		  					  if(tipoDEST.equals("TARJCRED"))
		  					  {
							   concepto="PAGO TARJETA DE CREDITO";
		  					  }
						      else{
							   concepto="TRANSFERENCIA EN EFECTIVO";}
						   }
						else if (strTrans.equals("2"))
							 concepto="PAGO TARJETA DE CREDITO";
					    }

					    conceptocompleto="";
					    if (concepto.length()>40){
						    conceptocompleto=concepto.substring(0,40);}
					    else
					    {
						    conceptocompleto=concepto;
						    longi=concepto.length();
						    for(j=0;j<(40-longi);j++)
							    conceptocompleto+=" ";
					    }

					    try{
						    if	((RFCs[indice1].length()!=0)&&(Ivas[indice1].length()!=0))
							     conceptocompleto+="RFC "+RFCs[indice1]+" IVA "+Ivas[indice1];
					    }catch(Exception e) {}
					    if (tipo_fecha==0)
					    {
							if( tdc2chq.equals("1") ) /*TRANSFERENCIA TDC a CHEQUES*/
							{
                                                          tipo_operacion = "TRTJ";
							  trama_entrada="1EWEB|"+usuario+"|" + tipo_operacion + "|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_destino+"|"+tipo_cta_destino+"|"+
											cta_origen+"|"+tipo_cta_origen+"|"+importe_string+"|"+conceptocompleto+"| |";
							  moduloOrigen="43@";
				  			  moduloDestino="44@";
							}
							else /*TRAMA NORMAL PARA TRANSFERENCIA*/
							{
							  trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
											cta_destino+"|"+tipo_cta_destino+"|"+importe_string+"|"+conceptocompleto+"| |";
							}
						}
						else
					    {
							if ( tdc2chq.equals("1") ) /*TRANSFERENCIA TDC a CHEQUES*/
							{
                                                          //tipo_operacion = "PROG";
							  trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|TRTJ|"+sfecha_programada+"|"+cta_destino+"|"+tipo_cta_destino+"|"+
											cta_origen+"|"+tipo_cta_origen+"|"+importe_string+"|0|"+conceptocompleto+"| |";
							  	moduloOrigen="43@";
				  				moduloDestino="44@";
							}
							else
							{

							  trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
											cta_destino+"|"+tipo_cta_destino+"|"+importe_string+"|0|"+conceptocompleto+"| |";
							}
						}


						String IP = " ",fechaHr = "";										//variables locales al mtodo
						IP = req.getRemoteAddr();											//ObtenerIP
						java.util.Date fechaHrAct = new java.util.Date();
						SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
						fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";				//asignaci? de fecha y hora

						ServicioTux tuxGlobal = new ServicioTux();
						//VALIDACION DE CUENTAS DE CARGO Y DE ABONO
						boolean cuentasValidas = true;
						String mensajeCargo = "" , mensajeAbono = "";
						String tipoCuenta = (String)sess.getAttribute("TIPO_CUENTA_TRAN");
						String facNoReg = (String)sess.getAttribute("FAC_CUENTA_NR");


							try{


								EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->" +
										"Validando Cuenta de cargo " + cta_origen , EIGlobal.NivelLog.INFO);


								String[] datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_origen, moduloOrigen);


								if(datosCuenta == null){
									cuentasValidas = false;
									mensajeCargo = "CUENTA DE CARGO NO PERTENECE AL CONTRATO";
									EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> Cuenta de cargo" +
											"No pertenece al contrato : "+cta_origen, EIGlobal.NivelLog.INFO);
								}

							EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->" +
									"Validando Cuenta de abono " + cta_destino , EIGlobal.NivelLog.INFO);

							if("NR".equals(tipoCuenta) && "SI".equals(facNoReg)){
								EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> " +
														 "OPCION CUENTA NO REGISTRADA : " +
														 "CON FACULTAD DE ABONO CTAS. NO REGISTRADAS "+cta_destino, EIGlobal.NivelLog.INFO);

							}else
							{
								if(cuentasValidas){


									datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_destino, moduloDestino);
									if(datosCuenta == null){
										cuentasValidas = false;
										mensajeCargo = "CUENTA DE ABONO NO PERTENECE AL CONTRATO";
										EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> Cuenta de abono" +
												"No pertenece al contrato : "+cta_origen, EIGlobal.NivelLog.INFO);

									}
								}
							}
							}catch(Exception e)
							{
								StackTraceElement[] lineaError;
								lineaError = e.getStackTrace();
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(fechaHr+ "**"+ this.getClass().getName() +"*** en validacion de cuentas contrato transferencias " + e.getMessage(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(fechaHr+ "**"+ this.getClass().getName() +"*** en validacion de cuentas contrato transferencias Linea " + lineaError[0], EIGlobal.NivelLog.INFO);
							}finally{
							}


						EIGlobal.mensajePorTrace(fechaHr+"***transferencia.ejecucion_operaciones_transferencias_con_tabla-> WEB_RED-Transferencias>>"+indice1+":"+trama_entrada, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("-----------------------------------------------------------------", EIGlobal.NivelLog.DEBUG);

						try{
							if(cuentasValidas){
								boolean autorizoLynx = true; // Variable para indicar si se envia o no a tuxedo
								boolean autorizar = true; // Variable para indicar si se envia o no a tuxedo
                                boolean procesaLynx = false; // Indicador de si autorizo con Lynx
								if(CAD_TIP_OP_TRAN.equals(tipo_operacion))
								{
									String fechaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa");
									String horaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "th:tm:ts");
									WSLinCapService wsLCService = new WSLinCapService();
									EIGlobal.mensajePorTrace("***INICIA VALIDACION CON WEB SERVICE Linea CAptura->", EIGlobal.NivelLog.DEBUG);
									String respuesta = wsLCService.validaLineaCaptura(req, conceptocompleto, importe_string, "001", "0001", fechaSist, horaSist, "", cta_destino, "", "", cta_origen, tipo_operacion, null);
									EIGlobal.mensajePorTrace("***RESPUESTA VALIDACION CON WEB SERVICE Linea CAptura->" + respuesta, EIGlobal.NivelLog.DEBUG);
                                    autorizar = "0000".equals(respuesta.substring(4,8));
									if(autorizar){
										EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla -> Cuentas validas ejecutar servicio", EIGlobal.NivelLog.DEBUG);
							            procesaLynx = Global.LYNX_INICIALIZADO &&
												"Moneda Nacional".equals(req.getSession().getAttribute(MONEDA))
												&& !"P".equalsIgnoreCase(tipo_cta_destino);
							            EIGlobal.mensajePorTrace("LYNX=> Tipos de cuentas origen y destino: " + tipo_cta_origen + " - " + tipo_cta_destino + 
												", tipo_operacion: " + tipo_operacion + ", Procesar: " + procesaLynx + 
												", Moneda: " + req.getSession().getAttribute(MONEDA), EIGlobal.NivelLog.DEBUG);
										if (procesaLynx) {
                                            RespuestaLynxDTO respuestaLynx = 
                                                    EnlaceLynxUtils.llamadoConectorLynxTranferencia(
                                                    usuario, contrato,
                                                    cta_origen,
                                                    cta_destino,
                                                    importe_string,
                                                    sfecha_programada,
                                                    session, Ivas, RFCs,
                                                    map, indice1);
											EIGlobal.mensajePorTrace("Termina envio de mensaje a Lynx, respuesta: " 
                                                    + respuestaLynx.getCodError() + "/" + respuestaLynx.getMsgError()
                                                    + ", respuesta de riesgo de fraude: " + respuestaLynx.getCodErrorLynx(), 
                                                    EIGlobal.NivelLog.INFO);
											if (LynxConstants.ERROR_CODE_OK.equals(respuestaLynx.getCodError())) {
                                                if (respuestaLynx.getCodErrorLynx() != 0) {
                                                    autorizoLynx = false;
                                                    contTransRechazadas++;
                                                    coderror = EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA;
                                                    codErrorOper += EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA + "-";
                                                }
											} else {
                                                procesaLynx = false;
                                            }
										}
									} else {
										coderror = respuesta;
										codErrorOper = respuesta.substring(0,8);
									}
								}
								EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->"+coderror, EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla codErrorOper->"+codErrorOper, EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace("Lynx Autorizo: " + autorizoLynx, EIGlobal.NivelLog.DEBUG);
                                if (autorizar) {
                                    if (autorizoLynx) {
                                        // Enviar mensaje a tuxedo para realizar operacion
                                        Hashtable hs = tuxGlobal.web_red(trama_entrada);
                                        EIGlobal.mensajePorTrace(fechaHr+"***transferencia  WEB_RED-Transferencias>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
                                        coderror = (String) hs.get("BUFFER");
                                        codErrorOper+=hs.get("COD_ERROR")+"-";
                                        flgOperValidaCSD = coderror != null && "OK".equals(coderror.substring(0, 2));
                                    } else {
                                        // Grabar error en bitacora
                                        BitaTCTBean beanTCT = new BitaTCTBean();
                                        beanTCT = bh.llenarBeanTCT(beanTCT);
                                        beanTCT.setReferencia(obten_referencia_operacion(
                                                Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
                                        sreferencia = String.valueOf(beanTCT.getReferencia());
                                        beanTCT.setCodError(EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA);
                                        if (session.getContractNumber() != null) {
                                            beanTCT.setNumCuenta(session.getContractNumber().trim());
                                        }
                                        if (session.getUserID8() != null) {
                                            beanTCT.setUsuario(session.getUserID8().trim());
                                            beanTCT.setOperador(session.getUserID8().trim());
                                        }
                                        beanTCT.setTipoOperacion("TRAN");
                                        beanTCT.setCuentaOrigen(cta_origen);
                                        beanTCT.setCuentaDestinoFondo(cta_destino);
                                        beanTCT.setImporte(importe);
                                        BitaHandler.getInstance().insertBitaTCT(beanTCT);
                                    }
                                }
								EIGlobal.mensajePorTrace("***transferencia<<"+coderror, EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace("***codErrorOper["+indice1+"]--->>"+codErrorOper, EIGlobal.NivelLog.DEBUG);
                                // Confirmar operaciones si se proceso con lynx y no autorizo o fallo
								if (procesaLynx && (!autorizoLynx || (coderror!= null
                                        && !"OK".equals(coderror.substring(0, 2))
                                        && !"MANC".equals(coderror.substring(0,4))))) {
									RespuestaLynxDTO respuestaDenegadas = null;
									EIGlobal.mensajePorTrace("Lynx: informar operacion denegadas o canceladas", EIGlobal.NivelLog.DEBUG);									
									Map<Integer, String> mapDenCan = new HashMap<Integer, String>();
									// Se cargan valores por default para el mensaje
									mapDenCan = EnlaceLynxConstants.cargaValoresDefaultCF(mapDenCan, cta_destino, (map.containsKey(3)) ? map.get(3) : "");
									mapDenCan.put(6, autorizoLynx ? EnlaceLynxConstants.CODRESP_ERR : EnlaceLynxConstants.CODRESP_DEN_LYNX);
									// Se agregan valores faltantes a mapa de valores para armar el mensaje.
									respuestaDenegadas = ConectorLynx.enviaMsg(mapDenCan, EnlaceLynxConstants.TIPO_MSG_ACTUALIZACION_RESP);
									EIGlobal.mensajePorTrace("Lynx: Resultado actualizacion cancelacion y/o denegacion: "
                                            + respuestaDenegadas.getCodError() + " / " + respuestaDenegadas.getMsgError(),
                                            EIGlobal.NivelLog.DEBUG);
								}
								if("OK".equals(coderror.substring(0, 2))||"MANC".equals(coderror.substring(0,4))||"0000".equals(codErrorOper.substring(4, 8))||"0001".equals(codErrorOper.substring(4, 8))){
									// Metodo para enviar los datos al monitor plus
									String numRefe = coderror.substring(9, 16);
									TransferenciasMonitorPlusUtils.enviarTMBIndividual(
													(cta_origen + "|" + cta_destino + "|" + IEnlace.SUCURSAL_OPERANTE+ "|" + importe_string+"|"+des_cta_destino+"|"+des_cta_origen),
													session, req, tipo_cta_destino, numRefe, codErrorOper);
								}
							}else{
								coderror ="CTASINVA";
							}
							EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->"+coderror, EIGlobal.NivelLog.DEBUG);
						}catch( java.rmi.RemoteException re ) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
                        }

						 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
						try {
							log("VSWF: Ejecuta Transferencias");


							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean) bh.llenarBean(bt);
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA)){
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA_AGREGAR_REGISTRO_BIT);
								bt.setTipoMoneda("MN");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES)){
								bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES_AGREGAR_REGISTRO_BIT);
								bt.setTipoMoneda("USD");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_TARJETA_CREDITO_PAGO)){
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_PAGO_AGREGAR_REGISTRO_BIT);
								bt.setTipoMoneda("MN");
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO).
									equals(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION)){
								bt.setNumBit(BitaConstants.ET_TARJETA_CREDITO_DISPOSICION_AGREGAR_REGISTRO_BIT);
								bt.setTipoMoneda("MN");
							}
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}
							if (cta_origen != null) {
								if (cta_origen.length() > 11) {
									bt.setCctaOrig(cta_origen.substring(0, 11));
								} else {
									bt.setCctaOrig(cta_origen.trim());
								}
							}
							if (cta_destino != null) {
								if (cta_destino.length() > 20) {
									bt.setCctaDest(cta_destino.substring(0, 20));
								} else {
									bt.setCctaDest(cta_destino.trim());
								}
							}
							bt.setImporte(Double.parseDouble(importe_string));
							if(coderror != null){
								if(coderror.substring(0,2).equals("OK")){
									bt.setIdErr(tipo_operacion + "0000");
								}else if(coderror.length() > 8){
									bt.setIdErr(coderror.substring(0,8));
								}else{
									bt.setIdErr(coderror.trim());
								}
							}
							if (tipo_operacion!= null) {
								bt.setServTransTux(tipo_operacion.trim());
							}
							bt.setBancoDest("SANTANDER");
							if(coderror != null){
                                if (coderror.startsWith(EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA)
                                        && sreferencia.length() > 0) {
									bt.setReferencia(Long.parseLong(sreferencia.trim()));
                                } else
								if ((coderror.length()>=16) && !coderror.substring(0,4).equals("MANC")){
									sreferencia = coderror.substring(8,16);
									bt.setReferencia(Long.parseLong(sreferencia.trim()));
								}else{
									sreferencia=sreferencia.trim();
								}
							}
							if(req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null){
								if(((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).equals(BitaConstants.VALIDA)){
									bt.setIdToken(session.getToken().getSerialNumber());
								}
							}

							EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla ::des_cta_destino->"+des_cta_destino, EIGlobal.NivelLog.DEBUG);
							if(des_cta_destino!=null && des_cta_destino!=""){
								bt.setBeneficiario(des_cta_destino.trim());
							}

							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

						}
						 }


						sreferencia="";


					if((coderror!=null && coderror.length() >= 8)
						&& (coderror.substring(0,8).equals("TUBO4060") || coderror.substring(0,8).equals("TUBO9999"))&&indice1==1)	//Error TUBO4060
					  {
					   despliegaPaginaError("SERVICIO NO DISPONIBLE POR EL MOMENTO PARA EFECTUAR ESTE TIPO DE OPERACIONES", req,res);			//Error TUBO4060
                                  return 1;

}

						else if(coderror!=null)
						{

							if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC")))) {
								sreferencia=coderror.substring(8,16);


							}
							sreferencia=sreferencia.trim();

							mensaje_salida.append("<TD class="+clase+" align=left	nowrap >"+cta_origen+"	"+des_cta_origen+"</TD>")
								.append("<TD class=").append(clase).append(" align=left	nowrap>").append( cta_destino ).append(' ').append(des_cta_destino).append("</TD>")
								.append("<TD class="+clase+" align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>")
								.append("<TD class="+clase+CENTR_NOWRP+fecha+"</TD>")
								.append("<TD class="+clase+" align=left nowrap>"+concepto.trim()+"</TD>");

							try{
							EIGlobal.mensajePorTrace("CSA::CODERROR->: "+coderror, EIGlobal.NivelLog.DEBUG);
							if (coderror.substring(0,2).equals("OK"))
							{

								if((coderror.length()>=16)&&(coderror.indexOf("MANC") != -1))
								{
									  EIGlobal.mensajePorTrace("Operacion mancomunada referencia: "+sreferencia, EIGlobal.NivelLog.DEBUG);
									  mensaje_salida.append("<TD class="+clase+" align=center nowrap>Operaci&oacute;n Mancomunada</TD>")
									  	.append("<TD class="+clase+CENTR_NOWRP+sreferencia+"</TD>");
									  if(!tdc2chq.equals("1")){
									  mensaje_salida.append("<TD class="+clase+RIGHT_NOWRAP)
									  	.append("<TD class="+clase+ESPACIO_BLANCO);
									  }

									  estatusTrans = "MANCOMUNADA";
								}
								else
								{
									if (tipo_fecha==0)
									{
										contadorok++;
										cta_origen+="  "+des_cta_origen;
										cta_destino+=" "+des_cta_destino;
										tramaok+=contadorok+"|"+(tdc2chq.equals("1")?"10":strTrans)+"|"+sreferencia.trim()+"|"+importe_string+"|"+cta_origen+"|"+cta_destino+"|"+concepto;
										EIGlobal.mensajePorTrace("***CSA DICE: tdc2chq = "+tdc2chq, EIGlobal.NivelLog.INFO);

										if(!tdc2chq.equals("1"))
										{
											try{
													EIGlobal.mensajePorTrace("***ENTRANDO PARA AGREGAR RFC E IVA: "+tdc2chq, EIGlobal.NivelLog.INFO);
													EIGlobal.mensajePorTrace("***Indice: "+indice1, EIGlobal.NivelLog.INFO);
													tramaok+="|"+RFCs[indice1]+"|"+Ivas[indice1];
											} catch(Exception e) {
												EIGlobal.mensajePorTrace("Truene en el indice: "+indice1, EIGlobal.NivelLog.INFO);
												EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

											}
										}
										tramaok+="@";

										mensaje_salida.append("<TD class="+clase+" align=center nowrap>Operaci&oacute;n realizada</TD>")
											.append("<TD class="+clase+" align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value,"+contadorok+");\">"+sreferencia+"</A></TD>");

										if(!tdc2chq.equals("1"))
										{
											if (coderror.length()>=30){
												mensaje_salida.append("<TD class="+clase+" align=right nowrap>"+ FormatoMoneda(coderror.substring(16,30))+"</TD>");}
											else{
													mensaje_salida.append("<TD class="+clase+RIGHT_NOWRAP);
												}

												mensaje_salida.append("<TD class="+clase+ESPACIO_BLANCO);
										}
										  estatusTrans = "REALIZADA";
								  }
								  else
								  {

										mensaje_salida.append("<TD class="+clase+" align=center nowrap>Operaci&oacute;n programada</TD>")
											.append("<TD class="+clase+CENTR_NOWRP+sreferencia+"</TD>");
										if(!tdc2chq.equals("1")){
											mensaje_salida.append("<TD class="+clase+RIGHT_NOWRAP)
												.append("<TD class="+clase+ESPACIO_BLANCO);
										}
										estatusTrans = "PROGRAMADA";
								  }
							}
                         try{
							if (estatusTrans.equals("MANCOMUNADA")) {
								estatusExp = "Operacion Mancomunada";
								if(!tdc2chq.equals("1")){
									saldoExp = "-------";
								}
							}
							if (estatusTrans.equals("REALIZADA")) {
								estatusExp = "Operacion realizada";
								if (coderror.length()>=30) {
									saldoExp = FormatoMoneda(coderror.substring(16,30));
								} else {
									saldoExp = "-------";
								}
							}
							if (estatusTrans.equals("PROGRAMADA")) {
								estatusExp = "Operacion programada";
								if(!tdc2chq.equals("1")){
									saldoExp = "-------";
								}
							}

							if(grabaArchivo && cta_origen!=null && ArcSal!=null){
								cadena = cta_origen.concat(" \t")
										.concat(cta_destino).concat(" \t")
										.concat(FormatoMoneda(importe_string)).concat(" \t")
										.concat(fecha).concat(" \t")
										.concat(concepto.trim()).concat(" \t")
										.concat(estatusExp).concat(" \t")
										.concat(sreferencia).concat(" \t")
										.concat(saldoExp);

								ArcSal.escribeLinea(cadena + "\r\n");
								EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  " + cadena, EIGlobal.NivelLog.DEBUG);
							}
                         }catch(Exception e ){
                        	 EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  Termina "+e.getMessage() + cadena, EIGlobal.NivelLog.INFO);
                         }
						}
						else
						{

							if (coderror.substring(0,4).equals("MANC")){
								if((tipo_operacion.equals(CAD_TIP_OP_PAGT) || tipo_operacion.equals(CAD_TIP_OP_TRAN))
										&& coderror.substring(0,8).equals("MANC0018")){
										error=coderror.substring(16,coderror.length());
									}
								else{
										error=coderror.substring(8,coderror.length());
									}
							}
							else if ("WTRA".equals(coderror.substring(0,4)))
							{
								error="Error de operaci&oacute;n";
							}
							else if ((coderror.length()>16)&&(!(coderror.substring(0,4).equals("MANC")))){
								error=coderror.substring(16,coderror.length());}
							else
								if (EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA.equals(coderror)) {
									error = "Operaci&oacute;n en Verificaci&oacute;n, Favor de Contactar a su Ejecutivo";
								} else{
								error="Error de operaci&oacute;n";}

							if("CTASINVA".equals(coderror)){
								error = mensajeCargo + mensajeAbono;

							}
							mensaje_salida.append("<TD class="+clase+CENTR_NOWRP+error+"</TD>");
							if (coderror.substring(0,4).equals("MANC")){
								mensaje_salida.append("<TD class="+clase+" align=center nowrap>-------</TD>");}
							else

							{
							  if(coderror.substring(0,8).equals("TUBO4060") || coderror.substring(0,8).equals("TUBO9999")){
							     mensaje_salida.append("<TD class="+clase+CENTR_NOWRP+"SERVICIO NO DISPONIBLE POR EL MOMENTO PARA EFECTUAR ESTE TIPO DE OPERACIONES"+"</TD>");}
							  else{

								mensaje_salida.append("<TD class="+clase+CENTR_NOWRP+sreferencia+"</TD>");}


							}

							cta_origen+="  "+des_cta_origen;
							cta_destino+=" "+des_cta_destino;
							if(!tdc2chq.equals("1")){
							mensaje_salida.append("<TD class="+clase+RIGHT_NOWRAP);

									if ("WTRA".equals(coderror.substring(0,4)))
									{
										mensaje_salida.append("<TD class="+clase+" nowrap>" + coderror.substring(16) + "</TD>");
									}
									else
									{
										mensaje_salida.append("<TD class="+clase+ESPACIO_BLANCO);
									}

							}


							if (coderror.contains("TUBO0001") && coderror.contains("Operacion Realizada")) {
								estatusTrans = "REALIZADA";
							} else {
							  estatusTrans = "NO REALIZADA";
							}

							if(grabaArchivo){
								cadena = cta_origen.concat(" \t")
										.concat(cta_destino).concat(" \t")
										.concat(FormatoMoneda(importe_string)).concat(" \t")
										.concat(fecha).concat(" \t")
										.concat(concepto.trim()).concat(" \t")
										.concat(error).concat(" \t")
										.concat("-------").concat(" \t")
										.concat("-------");

								ArcSal.escribeLinea(cadena + "\r\n");
								EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  " + cadena, EIGlobal.NivelLog.DEBUG);
							}

						}
					} catch(Exception e) {
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("transferencia::ejecucion_operaciones_transferencias_con_tabla:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
												   + "<DATOS GENERALES>"
												   + "Usuario->" + session.getUserID8()
												   + "Contrato->" + session.getContractNumber()
												   + "Linea de truene->" + lineaError[0]
									               , EIGlobal.NivelLog.ERROR);
					}
					}
					else
					{

						mensaje_salida.append("<TD class="+clase+" align=left	nowrap >"+cta_origen+"	"+des_cta_origen+"</TD>")
							.append("<TD class="+clase+" align=left	nowrap>"+cta_destino+"	"+des_cta_destino+"</TD>")
							.append("<TD class="+clase+" align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>")
							.append("<TD class="+clase+CENTR_NOWRP+fecha+"</TD>")
							.append("<TD class="+clase+" align=left nowrap>"+concepto.trim()+"</TD>");



						if(coderror == null || coderror.substring(0,8).equals("TUBO4060") || coderror.substring(0,8).equals("TUBO9999")) {
						   error="SERVICIO NO DISPONIBLE POR EL MOMENTO PARA EFECTUAR ESTE TIPO DE OPERACIONES";}
						else{
						   error="Error de operaci&oacute;n";}

						mensaje_salida.append("<TD class="+clase+CENTR_NOWRP+error+"</TD>")
							.append("<TD class="+clase+" align=center nowrap>-------</TD>");
						if(!tdc2chq.equals("1")){
						mensaje_salida.append("<TD class="+clase+RIGHT_NOWRAP)
							.append("<TD class="+clase+ESPACIO_BLANCO);
						}

						if(grabaArchivo && cta_origen!=null && ArcSal!=null){
							cadena = cta_origen.concat(" \t")
									.concat(cta_destino).concat(" \t")
									.concat(FormatoMoneda(importe_string)).concat(" \t")
									.concat(fecha).concat(" \t")
									.concat(concepto.trim()).concat(" \t")
									.concat(error).concat(" \t")
									.concat("-------").concat(" \t")
									.concat("-------");

							ArcSal.escribeLinea(cadena + "\r\n");
							EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  " + cadena, EIGlobal.NivelLog.DEBUG);
						}

					}
					mensaje_salida.append("</TR>");

					folios = folios + sreferencia+", ";
					estatus = estatus + estatusTrans +", ";

				}//if
				EIGlobal.mensajePorTrace("CSD operacionMasiva flgOperValidaCSD|" + flgOperValidaCSD + "|", EIGlobal.NivelLog.DEBUG);
                if (flgOperValidaCSD) {
                    if (operMasivas == null) {
                        operMasivas = new ArrayList<BeanDatosOperMasiva>();
                    }
                    if(cta_destino == null){
                    	cta_destino = "";
                    }
                    
                    String idConv = CuentasDAO.consultarCuentaConv(cta_destino.length() > 11 ? cta_destino.substring(0, 11).trim() : cta_destino);
                    if (!idConv.isEmpty()) {
                        EIGlobal.mensajePorTrace("La cuenta destino|" + cta_destino + "| tiene convenio|" + idConv + "|", EIGlobal.NivelLog.DEBUG);
                        operMasivas.add(new BeanDatosOperMasiva(SelloDigitalUtils.armarFchPag(fecha), Integer.valueOf(ConstantesSD.ID_CANAL_), importe, concepto, cta_destino.length() > 11 ? cta_destino.substring(0, 11).trim() : cta_destino , sreferencia));
                        EIGlobal.mensajePorTrace("CSD operacionMasiva agregando operacion, datos|" + SelloDigitalUtils.armarFchPag(fecha) + "|" + Integer.valueOf(ConstantesSD.ID_CANAL_) +"|" + importe + "|" + concepto + "|" + cta_destino + "|"+ sreferencia + "|", EIGlobal.NivelLog.DEBUG);
                    }
                    
                    
                                    }
			}//for
		  SelloDigitalUtils.guardaMovimientos(req, operMasivas);
		  req.getSession().setAttribute(ConstantesSD.HIDDEN_OPER_CSD, cadenaOperCSD.toString());
		  try{
			if( grabaArchivo ) {
				ArcSal.cierraArchivo();
			}

			ArchivoRemoto archR = new ArchivoRemoto();
			if(!archR.copiaLocalARemoto(fileOut,"WEB")) {
				EIGlobal.mensajePorTrace("***transferencia.class No se pudo crear archivo para exportar transferencia", EIGlobal.NivelLog.ERROR);
			}
		  }
			catch(Exception e){EIGlobal.mensajePorTrace("***transferencia.class Exportar-->"+e.getMessage(), EIGlobal.NivelLog.ERROR);}


			req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

			try{
				validaCuentas.closeTransaction();
			}catch(Exception e){
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("*** en validacion de cuentas contrato" + e.getMessage(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("*** en validacion de cuentas contrato linea " + lineaError[0], EIGlobal.NivelLog.INFO);
			}


		mensaje_salida.append("<tr><td class=textabref colspan=9>Si desea obtener su comprobante, haga click en el n&uacute;mero de referencia subrayado.</td></tr>");
		//INICIO PYME INDRA 2015 se modifica exportacion para MN
		//Modificacion INDRA PYME PAGO TARJETA DE CREDITO
		boolean dispocion = req.getParameter("nombreFlujo")!= null && req.getParameter("nombreFlujo").toString().equals("tarjetaDisposicion") ? true : false;
		if ( ("0".equals(strTrans) || "2".equals(strTrans)) && !dispocion) {
			mensaje_salida
				.append( "<tr><td colspan=\"9\" align=\"center\" class=\"textabref\"><a style=\"cursor: pointer;\" >Exporta en TXT <input id=\"tipoExportacion\" type=\"radio\" value =\"txt\" name=\"tipoArchivo\" /></a>\n</td></tr>" )
				.append( "<tr><td colspan=\"9\" align=\"center\" class=\"textabref\"><a style=\"cursor: pointer;\" >Exporta en XLS <input id=\"tipoExportacion\" type=\"radio\" value =\"csv\" checked name=\"tipoArchivo\" /></a>\n</td></tr>" );

			   ExportModel em = new ExportModel("resultadoOperacionRealizada", '|', "\t", true)
			   .addColumn(new Column(0, "Cuenta Cargo"))
			   .addColumn(new Column(1, "Cuenta abono / M�vil"))
			   .addColumn(new Column(2, "Importe"))
			   .addColumn(new Column(3, "Fecha Aplicaci�n"))
			   .addColumn(new Column(4, "Concepto"))
			   .addColumn(new Column(5, "Estatus"));
			   EIGlobal.mensajePorTrace("RRRR transferencia.ejecucion_operaciones_transferencias_con_tabla->  strTrans:"+"\""+strTrans+"\"", EIGlobal.NivelLog.DEBUG);
			   if( "2".equals(strTrans)  ){
				   em.addColumn(new Column(6, "Saldo Actualizado"))
					   .addColumn(new Column(7, "Observaciones"));
			   } else {
				   em.addColumn(new Column(6, "Referencia"))
					   .addColumn(new Column(7, "Saldo Actualizado"))
					   .addColumn(new Column(8, "Observaciones"));
			   }
			   String archExportar = IEnlace.DOWNLOAD_PATH.concat(fileOut);
			   req.setAttribute("em", "em");
			   req.setAttribute("af", "af");
			   sess.setAttribute("af", archExportar);
			   sess.setAttribute("em", em);
			   req.setAttribute("metodoExportacion", "ARCHIVO");
		}
		mensaje_salida.append("</TABLE>");
		if ( ("0".equals(strTrans) || "1".equals(strTrans) || "2".equals(strTrans)) && !dispocion ) {
			StringBuffer botonEx = new StringBuffer(
				"<td align=\"right\" style=\"width: 430\">")
				.append( "0".equals(strTrans) || "1".equals(strTrans) ? "<a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0>" : "")
				.append( "0".equals(strTrans) || "1".equals(strTrans) ? "<img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a>" : "" )
				.append( "<a href = \"")
				.append( "0".equals(strTrans) || "2".equals(strTrans) ? "javascript:fncExportar()" : "/Download/" + fileOut)
				.append("\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>" );
			req.setAttribute("botonexp", botonEx.toString());
		}
		req.setAttribute("mensaje_salida",mensaje_salida.toString());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("tramaok","\""+tramaok+"\"");
		req.setAttribute("contadorok",""+contadorok);
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->  tramaok:"+"\""+tramaok+"\"", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->  contadorok:"+contadorok, EIGlobal.NivelLog.DEBUG);
		req.setAttribute("banco",session.getClaveBanco());
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->banco:"+session.getClaveBanco()+":", EIGlobal.NivelLog.DEBUG);
		String Banco= session.getClaveBanco();

		req.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		req.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

		if ( "0".equals(strTrans) || "1".equals(strTrans)) {
			StringBuffer botonEx = new StringBuffer("<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0>")
				.append( "<img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a>" )
				.append( "<a href = \"")
				.append( "0".equals(strTrans) ? "javascript:fncExportar()" : "/Download/" + fileOut)
				.append("\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>" );
			sess.setAttribute("botonexp", botonEx.toString());
		}
		sess.setAttribute("mensaje_salida",mensaje_salida.toString());
		sess.setAttribute("MenuPrincipal", session.getStrMenu());
		sess.setAttribute("newMenu", session.getFuncionesDeMenu());
		sess.setAttribute("tramaok","\""+tramaok+"\"");
		sess.setAttribute("contadorok",""+contadorok);
		sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		sess.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
		//FIN PYME INDRA 2015 se modifica exportacion para MN
		if (strTrans.equals("0"))
		{
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt	Moneda Nacional &gt En l&iacute;nea","s25380h",req ));
			sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras	en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s25380h",req ));
		}
		else if (strTrans.equals("1"))
		{
			req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s25420h",req));
			sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt	d&oacute;lares","s25420h",req));
		}
		else if (strTrans.equals("2"))
		{
			if(tdc2chq.equals("1"))
			{


				req.setAttribute("Encabezado",CreaEncabezado("Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n","s40020h",req));
				sess.setAttribute("Encabezado",CreaEncabezado("Disposici&oacute;n de tarjeta de cr&eacute;dito","Transferencias &gt Tarjeta de Cr&eacute;dito &gt Disposici&oacute;n","s40020h",req));
			}
			else
			{
				req.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito","s25530h",req));
				sess.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de cr&eacute;dito","Transferencias &gt Pago de Tarjeta de Cr&eacute;dito","s25530h",req));

			}
		}
		req.setAttribute("web_application",Global.WEB_APPLICATION);
		sess.setAttribute("web_application",Global.WEB_APPLICATION);
		sess.setAttribute("banco",Banco);

		EIGlobal.mensajePorTrace("11111***transferencia.ejecucion_operaciones_transferencias_con_tabla-> llamando:jsp ["+"/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL+"]", EIGlobal.NivelLog.DEBUG);

		EmailSender emailSender=new EmailSender();

		EmailDetails emailDetails = new EmailDetails();


		Double total = 0.0;

		String ctaCargo = "";
		String ctaDest = "";


		EIGlobal.mensajePorTrace("------------------------> codErrorOper ->" + codErrorOper, EIGlobal.NivelLog.INFO);

		if(!codErrorOper.startsWith("MANC0018") && emailSender.enviaNotificacion(codErrorOper)){
			try {
				if (folios!=null && folios.contains(",")) {
					folios = folios.substring(0, folios.lastIndexOf(","));
				}
				emailDetails.setEstatusActual(codErrorOper);
				emailDetails.setNumRegImportados(contador);
				emailDetails.setImpTotal(ValidaOTP.formatoNumero(sumaImporte) );
				emailDetails.setNumeroContrato(session.getContractNumber());
				emailDetails.setRazonSocial(session.getNombreContrato());
				emailDetails.setNumRef(folios);
				emailDetails.setPAGT(false);
				emailDetails.setMayorA100Reg(false);


				EIGlobal.mensajePorTrace("���������������������������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("���������������������������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("���������������������������������������� Importe ->" + total, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("���������������������������������������� Referencia ->" + folios, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("���������������������������������������� Registros Importados ->" + contador, EIGlobal.NivelLog.INFO);
					if(tipo_operacion != null){
						if(tipo_operacion.equals(CAD_TIP_OP_PAGT)){
							EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><> Pago de tarjeta de credito <><><><><><><><><><><><><>" , EIGlobal.NivelLog.INFO);
							emailDetails.setTipoTransferenciaUni(" Pago de Tarjeta de Cr"+(char)233+"dito ");
							emailDetails.setPAGT(true);
						}

						if(tipo_operacion.equals("TRTJ")){
							EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><> Disposicion de tarjeta de credito <><><><><><><><><><><><><>" , EIGlobal.NivelLog.INFO);
							emailDetails.setTipoTransferenciaUni("Disposici&oacute;n de tarjeta de cr&eacute;dito");
							emailDetails.setCuentaDestino(cta_destino);
							emailDetails.setNumCuentaCargo(cta_origen);

						}
					}
				EIGlobal.mensajePorTrace("<><><><><><> contador ><><><>" +contador, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("<><><><><><> tipo_operacion <><><>" +tipo_operacion, EIGlobal.NivelLog.INFO);

				if(contador > 1) {
						if(tipo_operacion.equals("TRTJ")){
							if(emailSender.enviaNotificacion(CodErrorOper)){
								emailSender.sendNotificacion(req,IEnlace.TRANSF_TDC_DISPOSICION,emailDetails);
							}
						}else{
							emailSender.sendNotificacion(req,IEnlace.MAS_TRANS_MISMO_BANCO, emailDetails);
						}

				} else {//contador, cuando son transferencias en linea

					if(cta_origen.contains(" ")) {
						ctaCargo = cta_origen.substring(0,cta_origen.indexOf(" "));
					} else {
						ctaCargo = cta_origen;
					}
					if(cta_destino.contains(" ")) {
						ctaDest = cta_destino.substring(0,cta_destino.indexOf(" "));
					} else {
						ctaDest = cta_destino;
					}



					EIGlobal.mensajePorTrace("���������������������������������������� CuentaCargo ->" + ctaCargo, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("���������������������������������������� CuentaDestino ->" + ctaDest, EIGlobal.NivelLog.INFO);

					emailDetails.setNumCuentaCargo(ctaCargo);
					emailDetails.setCuentaDestino(ctaDest);

					String moneda = (String) sess.getAttribute(MONEDA);

					if(moneda != null){
					if(moneda.equals("Moneda Nacional")) {
						emailDetails.setTipoTransferenciaUni(" entre chequeras en M.N. ");
					}
					if(moneda.equals("Dolares")) {
						emailDetails.setTipoTransferenciaUni(" entre chequeras en D"+(char)243+"lares ");
						}
					}

					emailSender.sendNotificacion(req,IEnlace.UNI_TRANS_MISMO_BANCO, emailDetails);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(" al enviar el correo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}

		}
		EIGlobal.mensajePorTrace("Lynx: Finalizo flujo........ denegadas, contador: " +contTransRechazadas, EIGlobal.NivelLog.DEBUG);
		if (contTransRechazadas > 0) {
			// Se manda Pop Up para informar que algunas transacciones no fueron realizadas
			String msgTranDen ="cuadroDialogo(\"<br>Estimado usuario, de acuerdo a las reglas de seguridad determinadas en su contrato ENLACE,"
                    + " algunas de las  transacciones no se pudieron realizar hasta su validaci\u00f3n." 
                    + " Le pedimos revisar el detalle de cada Operaci\u00f3n Rechazada y contactar a su Ejecutivo,"
                    + " para continuar el proceso.\",1)";
            req.setAttribute("LynxTranDenegadas", msgTranDen);
		}
		EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla->"+coderror, EIGlobal.NivelLog.DEBUG);
		evalTemplate(IEnlace.MI_OPER_TMPL, req, res);

		} // banderaTransaccion
		else
		{   //Generar variables para pantalla aviso de Transaccion en Proceso.
			EIGlobal.mensajePorTrace("***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);
			String InfoUser= "<br>Su transacci&oacute;n ya est&aacute; en proceso, Verifique posteriormente resultados";
			InfoUser="cuadroDialogo (\""+ InfoUser +"\",1)";
			req.setAttribute("InfoUser", InfoUser);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("trans",strTrans);
			if (strTrans.equals("0"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55280",req ));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s55280",req ));
			}
			else if (strTrans.equals("1"))
			{
				req.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55280a",req));
				sess.setAttribute("Encabezado", CreaEncabezado("Transferencia entre chequeras en D&oacute;lares","Transferencias &gt  Cuentas mismo banco  &gt  d&oacute;lares","s55280a",req));
			}

			req.setAttribute("operacion","tranopera");  //antes "valida"
			evalTemplate("/jsp/TransInternasTranenProceso.jsp", req, res);
		}
		return 1;
	}


	/**
	* Pantalla_resultado_operaciones_por_archivo.
	*
	* @param tipo the tipo
	* @param req the req
	* @param res the res
	* @return the int
	* @throws IOException Signals that an I/O exception has occurred.
	* @throws ServletException the servlet exception
	*/
	int pantalla_resultado_operaciones_por_archivo(String tipo, HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException
	{

	 EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> Entrando", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> tipo"+getFormParameter(req,"trans"), EIGlobal.NivelLog.INFO);

	String mensaje_salida="";
	String trama="";
	String tramaux="";
	String[] datos;
	String clase="";
	boolean par;
	int contador=0;

	HttpSession sess = req.getSession();
    BaseResource session = (BaseResource) sess.getAttribute("session");


	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo->  contador"+contador, EIGlobal.NivelLog.DEBUG);

	if (tipo.equals("O"))
	{
	trama=getFormParameter(req,"operrealizadas");
	  contador=Integer.parseInt(getFormParameter(req,"contadorok"));
	}
	else if (tipo.equals("P"))
	{
		   trama=getFormParameter(req,"operprogramadas");
		   contador=Integer.parseInt(getFormParameter(req,"contadorprog"));
	}
	else if (tipo.equals("M"))
	{
		   trama=getFormParameter(req,"opermancomunadas");
		   contador=Integer.parseInt(getFormParameter(req,"contadormanc"));
	}
	else if (tipo.equals("R"))
	{
		   trama=getFormParameter(req,"operrechazadas");
		   contador=Integer.parseInt(getFormParameter(req,"contadorerror"));
	}

	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> trama operrealizadas:"+getFormParameter(req,"operrealizadas"), EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> trama operprogramadas:"+getFormParameter(req,"operprogramadas"), EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> trama opermancomunadas:"+getFormParameter(req,"opermancomunadas"), EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> trama operrechazadas:"+getFormParameter(req,"operrechazadas"), EIGlobal.NivelLog.DEBUG);


	EIGlobal.mensajePorTrace("***transferencia.pantalla_resultado_operaciones_por_archivo-> contador:"+contador, EIGlobal.NivelLog.DEBUG);

	mensaje_salida+="<table width=760 border=0 cellspacing=2 cellpadding=3>";
	mensaje_salida+="<tr><td class=textabref colspan=8>Este es el resultado de sus operaciones</td></tr>";
	mensaje_salida+="<TR>";
	mensaje_salida+="<TD align=center class=tittabdat width=200>Cuenta cargo</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=200>" + ("PESOS".equals(sess.getAttribute("TIPO_DIVISA")) ? "Cuenta abono&nbsp;/&nbsp;M&oacute;vil" : "Cuenta abono") + "</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=85 >Importe</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=75 >Fecha de Aplicaci&oacute;n</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=230>Concepto</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=60>Estatus</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=60>Referencia</TD>";
	mensaje_salida+="<TD class=tittabdat align=center width=80>Saldo actualizado</TD></TR>";
	par=false;
	BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
    BitaTransacBean bt = new BitaTransacBean();
	for (int i=0;i<contador;i++)
	{

		  if (trama!=null)
		  {

		    tramaux=trama.substring(0,trama.indexOf("@"));
		    trama=trama.substring(trama.indexOf("@")+1,trama.length());

	    datos = desentramaC(8+"|"+tramaux,'|');
		    if (par==false)
	    {
	      par=true;
			  clase="textabdatobs";
	    }
	    else
		    {
	      par=false;
			  clase="textabdatcla";
	    }
	    mensaje_salida+="<TR><TD class="+clase+" align=center nowrap >"+datos[1]+"</TD>";
	    mensaje_salida+="<TD class="+clase+CENTR_NOWRP+datos[2]+"</TD>";
	    mensaje_salida+="<TD class="+clase+" align=right nowrap>"+datos[3]+"</TD>";
	    mensaje_salida+="<TD class="+clase+CENTR_NOWRP+datos[4]+"</TD>";
		    mensaje_salida+="<TD class="+clase+" align=left>"+datos[5]+"</TD>";
	    if (tipo.equals("O")){
			   mensaje_salida=mensaje_salida+"<TD class="+clase+" align=center nowrap>Operaci&oacute;n realizada</TD>";}
	    else if (tipo.equals("P")){
	       mensaje_salida=mensaje_salida+"<TD class="+clase+" align=center nowrap>Operaci&oacute;n programada</TD>";}
			else if (tipo.equals("M")){
	       mensaje_salida=mensaje_salida+"<TD class="+clase+" align=center nowrap>Operaci&oacute;n Mancomunada</TD>";}
	    else{
	       mensaje_salida=mensaje_salida+"<TD class="+clase+CENTR_NOWRP+datos[6]+"</TD>";}
	    if (tipo.equals("O")){
				  mensaje_salida=mensaje_salida+"<TD class="+clase+" align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value,"+contador+");\">"+datos[7]+"</A></TD>";}
	    else{
			  mensaje_salida=mensaje_salida+"<TD class="+clase+CENTR_NOWRP+datos[7]+"</TD>";}
		if (tipo.equals("O")){
			  mensaje_salida=mensaje_salida+"<TD class="+clase+CENTR_NOWRP+datos[8]+"</TD></TR>";}
		else{
			  mensaje_salida=mensaje_salida+"<TD class="+clase+" align=right nowrap>-------</TD></TR>";}

		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		if (((String)sess.getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_LINEA_PREPAGO)){
		    bt = (BitaTransacBean) bh.llenarBean(bt);

	   		bt.setNumBit(BitaConstants.ER_CREDITO_LINEA_PREPAGO_EFECTUA_OPERACIONES);
			if (session.getContractNumber() != null) {
				bt.setContrato(((BaseResource) req.getSession().getAttribute("session")).getContractNumber().trim());
			}
	   		if (datos[1] != null) {
				bt.setCctaOrig(datos[1].trim());
			}else{
				bt.setCctaOrig(" ");}
	   		if (datos[2] != null) {
				bt.setCctaDest(datos[2].trim());
			}
	   		else{
	   			bt.setCctaDest(" ");}
	   		if (datos[3] != null) {
				bt.setImporte(Double.parseDouble(datos[3].trim()));
			}
	   		else{
	   			bt.setImporte(0);}
			if (session.getUserID8() != null) {
				bt.setNombreArchivo(session.getUserID8() + "CodErrTransfer.doc");
			}
			else{
				bt.setNombreArchivo(" ");}
			if (tipo != null) {
				bt.setEstatus(tipo.substring(0, 1));
			}
			if (datos[7] != null) {
				bt.setReferencia(Long.parseLong(datos[7].trim()));
			}
			else{
				bt.setReferencia(0);}

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {

			} catch (Exception e) {

			}
		    }
		 }

	  }
	}
	mensaje_salida+="</TABLE>";
	req.setAttribute("mensaje_salida",mensaje_salida);
	req.setAttribute("MenuPrincipal", session.getStrMenu());
	req.setAttribute("newMenu", session.getFuncionesDeMenu());
	req.setAttribute("tramaok",getFormParameter(req,"tramaok"));
	req.setAttribute("contadorok",getFormParameter(req,"contadorok"));
	req.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras  en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt	Moneda Nacional &gt En l&iacute;nea","s25380h",req ));


	sess.setAttribute("mensaje_salida",mensaje_salida);
	sess.setAttribute("MenuPrincipal", session.getStrMenu());
	sess.setAttribute("newMenu", session.getFuncionesDeMenu());
	sess.setAttribute("tramaok",getFormParameter(req,"tramaok"));
	sess.setAttribute("contadorok",getFormParameter(req,"contadorok"));
	sess.setAttribute("Encabezado", CreaEncabezado("Transferencias entre chequeras	en M.N. ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt En l&iacute;nea","s25380h",req));

	res.sendRedirect(res.encodeRedirectURL(IEnlace.MI_OPER_TMPL));


	return 0;
	}

	/**
	* Obten hora completa.
	*
	* @return the string
	*/
	String ObtenHoraCompleta()
	{
		String hora = "";
		String[] AM_PM = {"AM","PM"};
		int minutos =0;
		String sminutos ="";
		int segundos=0;
		String ssegundos="";
		int milisegundos=0;
		String smilisegundos="";

		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);

			if (AM_PM[Cal.get(Calendar.AM_PM)].equals("AM"))
			{
				minutos=Cal.get(Calendar.MINUTE);
				sminutos=((minutos < 10) ? "0" : "") + minutos;
				segundos=Cal.get(Calendar.SECOND);
				ssegundos=((segundos < 10) ? "0" : "") + segundos;

				milisegundos=Cal.get(Calendar.MILLISECOND);
				smilisegundos=((milisegundos < 10) ? "0" : "") + milisegundos;

				hora =	((Cal.get(Calendar.HOUR)%12))+":"+ sminutos +": "+ssegundos+": "+smilisegundos+"  hrs";
			}
			else
			{
				minutos=Cal.get(Calendar.MINUTE);
				sminutos=((minutos < 10) ? "0" : "") + minutos;
				segundos=Cal.get(Calendar.SECOND);
				ssegundos=((segundos < 10) ? "0" : "") + segundos;

				milisegundos=Cal.get(Calendar.MILLISECOND);
				smilisegundos=((milisegundos < 10) ? "0" : "") + milisegundos;

				hora =	((Cal.get(Calendar.HOUR)%12)+12)+":"+ sminutos +": "+ssegundos+": "+smilisegundos+ " hrs";
			}
		return hora;
	}

	/**
	* Cuentas no registradas.
	*
	* @param facultad the facultad
	* @return the string
	*/
	String cuentasNoRegistradas(boolean facultad)
	{
		String strNoRegistradas="";

		if(facultad)
		{
			strNoRegistradas+="<tr valign=middle><td class=tabmovtexbol nowrap><INPUT TYPE=radio value=OnNoReg NAME=ChkOnline>No Registrada</TD></TR>";
			strNoRegistradas+="<TR><TD class=tabmovtexbol nowrap>";
			strNoRegistradas+="<INPUT TYPE=text NAME=destinoNoReg SIZE=25 class=tabmovtexbol>";
			strNoRegistradas+="</TD></TR>";
			strNoRegistradas+="<INPUT TYPE=HIDDEN NAME=banderaNoReg VALUE=1>";
		} //fin del if de las facultades
		else
		{
			strNoRegistradas += "";
			strNoRegistradas+="<INPUT TYPE=HIDDEN NAME=banderaNoReg VALUE=0>";
		}
		return strNoRegistradas;
	}

	/**
	* Checar_fecha.
	*
	* @param fecha the fecha
	* @return the short
	*/
	public short checar_fecha(String fecha)
	{

		EIGlobal.mensajePorTrace("***transferencia.checar_fecha()-> Entrando : fecha:"+fecha, EIGlobal.NivelLog.DEBUG);

		short tipo=0; //-1 menor, 0 hoy, 1 programada, -2 en mal formato
		int dia, mes, anio;
		try
	{
		dia=new Integer(fecha.substring(0,2)).intValue();
		mes=new Integer(fecha.substring(3,5)).intValue();
		anio=new Integer(fecha.substring(6,fecha.length())).intValue();
		java.util.Date dia_hoy=new java.util.Date();
		GregorianCalendar cal1=new GregorianCalendar();
		GregorianCalendar cal2=new GregorianCalendar();
		cal1.setTime(dia_hoy);
		mes--;
		cal2.set(anio,mes,dia);
		if (cal2.get(Calendar.YEAR)>cal1.get(Calendar.YEAR)){
		tipo=1;}
		else if (cal2.get(Calendar.YEAR)==cal1.get(Calendar.YEAR))
		{
		if (cal2.get(Calendar.MONTH)>cal1.get(Calendar.MONTH)){
		  tipo=1;}
		else if (cal2.get(Calendar.MONTH)==cal1.get(Calendar.MONTH))
		{
		 if (cal2.get(Calendar.DATE)>cal1.get(Calendar.DATE)){
		     tipo=1;}
		 else if(cal2.get(Calendar.DATE)==cal1.get(Calendar.DATE)){
		     tipo=0;}
			 else{
				 tipo=-1;}
		}
		else{
			  tipo=-1;}
		}
		else{
		tipo=-1;}
		}
		catch(Exception e)

	{
	  EIGlobal.mensajePorTrace("***transferencia.checar_fecha()->error: "+e, EIGlobal.NivelLog.ERROR);

	  tipo=-2;
	}


		EIGlobal.mensajePorTrace("***transferencia.checar_fecha()-> Saliendo : tipo:"+tipo, EIGlobal.NivelLog.DEBUG);
		return tipo;
	}

	/**
	* Poner_campos_fecha.
	*
	* @param fac_programadas the fac_programadas
	* @return the string
	*/
	String poner_campos_fecha(boolean fac_programadas)
	{
	String dia_forma=ObtenDia();
	String mes_forma=ObtenMes();
	String anio_forma=ObtenAnio();
	String campos_fecha="";

	campos_fecha=campos_fecha+"<INPUT ID=dia  TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
	campos_fecha=campos_fecha+"<INPUT ID=mes  TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
	campos_fecha=campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";

	return campos_fecha;
	}

	/**
	* Poner_calendario_programadas.
	*
	* @param fac_programadas the fac_programadas
	* @param nombre_campo the nombre_campo
	* @param funcion the funcion
	* @return the string
	*/
	String poner_calendario_programadas(boolean fac_programadas,String nombre_campo,String funcion)
	{
	String cal="";
	cal=cal+"<INPUT ID="+nombre_campo+" TYPE=TEXT SIZE=15 class=tabmovtexbol NAME="+nombre_campo+" VALUE="+ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio()+" OnFocus=\"blur();\">";
		if (fac_programadas)
	cal=cal+"<A HREF=\"javascript:"+funcion+";\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" width=12 height=14 border=0 align=absmiddle></A>";
	return cal;
	}

	/**
	* Arma dias inhabiles js.
	*
	* @return the string
	*/
	String armaDiasInhabilesJS()
	{
	int indice	 = 0;
	String resultado = "";
	Vector diasInhabiles;

	diasInhabiles = CargarDias(1);

	if(diasInhabiles!=null)
	{
try{
	 resultado = diasInhabiles.elementAt(indice).toString();
}catch(Exception e) {};

	 for(indice=1; indice<diasInhabiles.size(); indice++)
		   resultado += ", " + diasInhabiles.elementAt(indice).toString();

	}
	return resultado;
	}

	/**
	* Cargar dias.
	*
	* @param formato the formato
	* @return the vector
	*/
	public Vector CargarDias(int formato)
	{
			EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Entrando", EIGlobal.NivelLog.DEBUG);

	Connection  Conexion=null;
	Statement qrDias=null;
	ResultSet rsDias=null;

	String	   sqlDias;
	Vector diasInhabiles = new Vector();


	 sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
	 EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	 try{

		Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			 if(Conexion!=null)
		 {
		    qrDias = Conexion.createStatement();

		    if(qrDias!=null)
		    {
			  rsDias = qrDias.executeQuery(sqlDias);
		       if(rsDias!=null)
		       {
			      String dia  = "";
			      String mes  = "";
			      String anio = "";

			  while( rsDias.next()){
					  try{
				dia  = rsDias.getString(1);
				mes  = rsDias.getString(2);
				anio = rsDias.getString(3);
					  } catch(Exception e) {
						  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					  }
			      if(formato == 0)
			       {
						  try{
				  dia  =  Integer.toString( Integer.parseInt(dia) );
				  mes  =  Integer.toString( Integer.parseInt(mes) );
				  anio =  Integer.toString( Integer.parseInt(anio) );
						  } catch(Exception e) {

						  }
			       }

			      String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
					  if(fecha.trim().equals(""))
						  fecha="1/1/2002";
			      diasInhabiles.addElement(fecha);
			  }

		       }
				   else{
			 EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);}
		    }
				else{
		      EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create Query", EIGlobal.NivelLog.ERROR);}
		 }
			 else{
				EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);}
	   }catch(SQLException sqle ){
		   EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	   } catch(Exception e) {
		   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	   }
	   finally
	   {
	    try	{
	       rsDias.close();
	    }catch(Exception e){}
	    try	{
		   qrDias.close();
	    }catch(Exception e){}
	    try	{
	       Conexion.close();
	    }catch(Exception e){}
       }

	   return(diasInhabiles);
	}

	/**
	* Valida cuenta no reg.
	*
	* @param usuario the usuario
	* @param clave_perfil the clave_perfil
	* @param contrato the contrato
	* @param cta_checar the cta_checar
	* @param req the req
	* @return the string
	*/
	String ValidaCuentaNoReg(String usuario,String clave_perfil,String contrato, String cta_checar, HttpServletRequest req)
	{
	EIGlobal.mensajePorTrace("***transferencia.ValidaCuentaNoReg()->Entrando", EIGlobal.NivelLog.DEBUG);
        tipo_operacion = "NAME";
	String trama_entrada="1EWEB|"+usuario+"|" + tipo_operacion + "|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_checar+"|NR";
	String coderror="";
    String descrip="";

	ServicioTux tuxGlobal = new ServicioTux();

	String IP = " ",fechaHr = "";										//variables locales al mtodo
	IP = req.getRemoteAddr();											//ObtenerIP
	java.util.Date fechaHrAct = new java.util.Date();
	SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
	fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";				//asignaci? de fecha y hora

	EIGlobal.mensajePorTrace(fechaHr+"***transferencia.ValidaCuentaNoReg()->WEB_RED-NAME>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
	try{
		Hashtable hs = tuxGlobal.tct_red(trama_entrada);
		coderror = (String) hs.get("BUFFER");
		EIGlobal.mensajePorTrace("***transferencia.ValidaCuentaNoReg()->coderror: "+coderror, EIGlobal.NivelLog.DEBUG);
	}catch( java.rmi.RemoteException re ){

	} catch (Exception e) {}

	if (coderror.substring(0,2).equals("OK"))
	descrip=coderror.substring(16,coderror.length());

	req.setAttribute("desctanoreg:",descrip);

	return	descrip;
	}


	public String BuscarCuentaAltairTemp (String cuenta) {
		  EIGlobal.mensajePorTrace("Entrando BuscarCuentaAltairTemp ()", EIGlobal.NivelLog.ERROR);
	  String cta=null;
	  String sqlcta = "Select num_cuenta  From nucl_relac_ctas where num_cuenta='" +cuenta+ "' and rownum = 1";
		  EIGlobal.mensajePorTrace("Entrando BuscarCuentaAltairTemp Query:"+ sqlcta, EIGlobal.NivelLog.ERROR);
	 Connection conn = null;
     PreparedStatement pscta = null;
     ResultSet rscta = null;
	 try {
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
			 if(conn == null)
				EIGlobal.mensajePorTrace("BuscarCuentaAltairTemp(): No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
	     	 pscta = conn.prepareStatement ( sqlcta );
			 if(pscta == null)
				EIGlobal.mensajePorTrace("BuscarCuentaAltairTemp(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
	     rscta  = pscta.executeQuery ();
			 if(!rscta.next())
		 EIGlobal.mensajePorTrace("BuscarCuentaAltairTemp(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
	     rscta.next ();
	     cta = rscta.getString ( 1 );
	     if(cta!=null)
		 cta=cta.trim ();

	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	   log ( "BaseServlet.java:BuscarCuentaAltairTemp():" + "ERROR: Creando conexi en:" );
	     //sqle.printStackTrace ();
	     cta=null;
	 } catch (Exception e) {
	     log (" al obtener cta santander = >" +e.toString ()+ "<");
	     cta=null;
	 } finally {
		try {
         rscta.close ();
        }catch(Exception x) {}
		try {
	     pscta.close ();
        }catch(Exception x) {}
		try {
	     conn.close ();
        }catch(Exception x) {}
	 }

	 EIGlobal.mensajePorTrace ( "*** BuscarCuentaAltairTemp : >" +cta+ "<", EIGlobal.NivelLog.DEBUG);
	 return cta;
     }

/**
 * Obten equivalencia altair cta.
 *
 * @param cuentaHogan the cuenta hogan
 * @return the string
 */
 public String	ObtenEquivalenciaAltairCta (String cuentaHogan) {



		 EIGlobal.mensajePorTrace("Entrando ObtenEquivalenciaAltairCta()", EIGlobal.NivelLog.ERROR);

	 String cta=null;
	 String sqlcta = "Select num_cuenta From tct_cuentas_serfin where num_cuenta='" +cuentaHogan+ "'";

		 EIGlobal.mensajePorTrace("ObtenEquivalenciaAltairCta(): Query = "+sqlcta, EIGlobal.NivelLog.ERROR);
	     Connection conn= null;
		 ResultSet rscta = null;
		 PreparedStatement pscta = null;
	 try {
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
			 if(conn == null)
				EIGlobal.mensajePorTrace("ObtenEquivalenciaAltairCta(): No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
	         pscta = conn.prepareStatement ( sqlcta );
			 if(pscta == null)
				EIGlobal.mensajePorTrace("ObtenEquivalenciaAltairCta(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
			 rscta	= pscta.executeQuery ();
			 if(!rscta.next())
				EIGlobal.mensajePorTrace("ObtenEquivalenciaAltairCta(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
	     rscta.next ();
	     cta = rscta.getString ( 1 );
	     rscta.close ();
	     pscta.close ();
	     conn.close ();

	     if(cta!=null)
		 cta=cta.trim ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	     log ( "BaseServlet.java:ObtenEquivalenciaAltairCta:" + ": Creando conexi? en:" );
	     //sqle.printStackTrace ();
	 }
     finally {
		try {
         rscta.close ();
        }catch(Exception x) {}
		try {
	     pscta.close ();
        }catch(Exception x) {}
		try {
	     conn.close ();
        }catch(Exception x) {}
	 }
	 EIGlobal.mensajePorTrace ( "***ObtenEquivalenciaAltairCta : >" +cta+ "<", EIGlobal.NivelLog.DEBUG);
	 return cta;
     }

/**
 * Despliega mensaje.
 *
 * @param mensajeHTML the mensaje html
 * @return the string
 */
  String despliegaMensaje(String mensajeHTML)
	{
	  String msg="";

	  msg+="\n  <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=400 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n   <td align=center>";
	  msg+="\n     <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td colspan=3> </td>";
	  msg+="\n	   </tr>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td width=21 background='/gifs/EnlaceMig/gfo25030.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n		 <td width=428 height=100 valign=top align=center>";
	  msg+="\n		   <table width=380 border=0 cellspacing=2 cellpadding=3 background='/gifs/EnlaceMig/gau25010.gif'>";
	  msg+="\n			 <tr>";
	  msg+="\n			   <td class='tittabcom' align=center>";
	  msg+=                mensajeHTML;
	  msg+="\n			   </td>";
	  msg+="\n			 </tr>";
	  msg+="\n		   </table>";
	  msg+="\n		 </td>";
	  msg+="\n         <td width=21 background='/gifs/EnlaceMig/gfo25040.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n	   </tr>";
	  msg+="\n	 </table>";
	  msg+="\n	</td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  return msg;
	}

	/**
	* Despliega pagina error url.
	*
	* @param Error the error
	* @param param1 the param1
	* @param param2 the param2
	* @param param3 the param3
	* @param req the req
	* @param res the res
	* @throws IOException Signals that an I/O exception has occurred.
	* @throws ServletException the servlet exception
	*/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException
	{

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		req.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		req.setAttribute( "Error", Error );

		req.setAttribute( "MenuPrincipal", session.getStrMenu() );
		req.setAttribute( "newMenu", session.getFuncionesDeMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,req ) );

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		//req.setAttribute( "URL", "MDI_Interbancario?Modulo=0");
		req.setAttribute( "URL", "TransferenciasMultiples?ventana=3");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( req, res );
	}

/**
 * Verifica archivos.
 *
 * @param arc the arc
 * @param eliminar the eliminar
 * @return true, if successful
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
public boolean verificaArchivos(String arc,boolean eliminar)
	throws ServletException, java.io.IOException
{
	EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
	File archivocomp = new File( arc );
	if (archivocomp.exists())
	 {
	   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.DEBUG);
	   if(eliminar)
		{
	      archivocomp.delete();
		  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
		}
	   return true;
	 }
	EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.DEBUG);
	return false;
}

	/**
	* Obten tipo cuenta.
	*
	* @param cuenta the cuenta
	* @return the string
	*/
	private String ObtenTipoCuenta(String cuenta)
		{

		Connection conn = null;
		Statement TipoQuery = null;
		ResultSet TipoResult = null;
		String resultado = "";
		String query = "";
		boolean datoEncontrado = false;

		try
			{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null) {
				EIGlobal.mensajePorTrace("Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR); 
				return "";
			}

			TipoQuery = conn.createStatement();
			if(TipoQuery == null) {
				EIGlobal.mensajePorTrace("No se pudo crear Query.", EIGlobal.NivelLog.ERROR); 
				return "";
			}

			query = "select n_subprod from nucl_relac_ctas where num_cuenta = '" + cuenta + "' and rownum = 1 and cve_producto = 'TCT'";
			EIGlobal.mensajePorTrace("Query: " + query, EIGlobal.NivelLog.INFO);
			TipoResult = TipoQuery.executeQuery(query);
			if(TipoResult == null) {
				EIGlobal.mensajePorTrace("No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR); 
				return "";
			}

			while(TipoResult.next()) {
				resultado = TipoResult.getString(1);
				datoEncontrado = true;
			}
			}
		catch( SQLException sqle )
			{
			EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);

			return ObtenTipoCuenta3(cuenta);
			}
		finally
			{
				try	{
					TipoResult.close();
				} catch(Exception e) {}
				try	{
					conn.close();
				} catch(Exception e) {}
			}

		if(!datoEncontrado) {
			return ObtenTipoCuenta3(cuenta);
		}

		EIGlobal.mensajePorTrace("TIPO DE CUENTA PARA " + cuenta + ": " + resultado, EIGlobal.NivelLog.DEBUG);
		return resultado.trim();
		}

	/**
	* Obten tipo cuenta2.
	*
	* @param cuenta the cuenta
	* @param strTrans the str trans
	* @param llamarServicio the llamar servicio
	* @param session the session
	* @param req the req
	* @return the string
	*/
	private String ObtenTipoCuenta2(String cuenta, String strTrans, boolean llamarServicio, BaseResource session, HttpServletRequest req)	{


		String datoscta[] = null;

		@SuppressWarnings("unused")
		String strArchivo;

		if(cuenta.indexOf(' ') != -1) {
			cuenta = cuenta.substring(0, cuenta.indexOf(" "));
		}
		if(cuenta.indexOf('|') != -1) {
			cuenta = cuenta.substring(0, cuenta.indexOf(SEPARADOR_PIPE));
		}

		try	{
			strArchivo = new String(getFileInBytes(req));
		}
		catch(Exception e) {
			strArchivo = "";
		}

		ValidaCuentaContrato valCtas = new ValidaCuentaContrato();
		if (!strTrans.equals("1")) {

			datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
					  						  session.getUserID8(),
					  						  session.getUserProfile(),
					  						  cuenta.trim(),
					  						  IEnlace.MCargo_transf_pesos);

			if(datoscta == null) {

				datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
						  						  session.getUserID8(),
						                          session.getUserProfile(),
						                          cuenta.trim(),
						                          IEnlace.MCargo_transf_pesos);
			}
		}
		else {

			datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
											  session.getUserID8(),
											  session.getUserProfile(),
											  cuenta.trim(),
											  IEnlace.MCargo_transf_dolar);
			if(datoscta == null) {

				datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
												  session.getUserID8(),
												  session.getUserProfile(),
												  cuenta.trim(),
												  IEnlace.MCargo_transf_dolar);
			}

		}
		valCtas.closeTransaction();
		if(datoscta == null) {
			return ObtenTipoCuenta(cuenta);
		}

		EIGlobal.mensajePorTrace("TIPO DE CUENTA PARA " + cuenta + ": " + datoscta[7], EIGlobal.NivelLog.INFO);

		return datoscta[7].trim();
	}

	/**
	* Obten tipo cuenta3.
	*
	* @param cuenta the cuenta
	* @return the string
	*/
	private String ObtenTipoCuenta3(String cuenta) {
		String resultado;
		String cta4 = cuenta.substring(0, 4);
		if(cuenta.length() != 16) {
			resultado = "OTRO";
		}
		else if(cta4.equals("4513") || cta4.equals("5579")) {
			resultado = "TARJDEB";
		}
		else {
			resultado = "TARJCRED";
		}
		EIGlobal.mensajePorTrace("TIPO DE CUENTA PARA " + cuenta + ": " + resultado, EIGlobal.NivelLog.INFO);
		return resultado;

	}

	/**
	* Trim cuenta.
	*
	* @param cuenta_input the cuenta_input
	* @return the string
	*/
	private String trimCuenta(String cuenta_input) {
		String cuenta = cuenta_input;
		if(cuenta.indexOf(" ") != -1) { //limpia cuentas de tipo CUENTA CUENTAHABIENTE
			cuenta = cuenta.substring(0, cuenta.indexOf(" "));
		}
		if(cuenta.indexOf(SEPARADOR_PIPE) != -1) { //limpia cuentas de tipo CUENTA|INFO|INFO
			cuenta = cuenta.substring(0, cuenta.indexOf(SEPARADOR_PIPE));
		}
		return cuenta;
	}
	/**
	* Es cuenta valida.
	*
	* @param cardNumber the card number
	* @return true, if successful
	*/
  private boolean esCuentaValida(String cardNumber) {
    int sum = 0;
    int digit = 0;
    int addend = 0;
    boolean timesTwo = false;

    for (int i = cardNumber.length () - 1; i >= 0; i--) {
      digit = Integer.parseInt (cardNumber.substring (i, i + 1));
      if (timesTwo) {
        addend = digit * 2;
        if (addend > 9) {
          addend -= 9;
        }
      }
      else {
        addend = digit;
      }
      sum += addend;
      timesTwo = !timesTwo;
    }

    int modulus = sum % 10;
    return modulus == 0;
  }

	/**
	* Call passmark.
	*
	* @param usuario the usuario
	* @param deviceprint the deviceprint
	* @param ipe the ipe
	* @param FSOanterior the FS oanterior
	* @param monto the monto
	* @return the string
	*/
	private String CallPassmark(String usuario, String deviceprint, String ipe, String FSOanterior, String monto)
	{
		String FSONuevo = null;
		BufferedWriter out = null;
		BufferedReader in  = null;

		try{
			if(monto.indexOf(".")>-1)
			{

				monto = monto.substring(0,monto.indexOf("."));
			}
		}
		catch(Exception e){

			monto = "";
		}

        try {

            URL url = new URL(Global.URL_PASSMARK_ENLACE);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);

            out = new BufferedWriter( new OutputStreamWriter( conn.getOutputStream() ) );
			out.write("Oper=a&");
            out.write("usuario=" +usuario     +"&");
            out.write("device="  +deviceprint +"&");
            out.write("ipuser="  +ipe         +"&");
			out.write("pmmonto=" +monto       +"&");
            out.write("fsoantes="+FSOanterior +"&");
            out.write("canal=2");
            out.flush();
            out.close();

            in = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );

            String response;
            while ( (response = in.readLine()) != null ) {

				if(response.indexOf("PMDATA=[")>-1 && response.indexOf("]!-")>-1)
				{
					FSONuevo = response.substring(response.indexOf("PMDATA=[")+8,response.indexOf("]!-"));
				}
				else
				{

				}
            }
            in.close();
        }
        catch ( MalformedURLException ex ) {

        }
        catch ( IOException ex ) {

        }
		catch ( Exception ex ) {

        }
		finally{
			try{
				out.close();
			}
			catch(Exception e){

			}
			try{
				in.close();
			}
			catch(Exception e){

			}
		}

		try{

		}catch(Exception e){

		}
		return FSONuevo;
    }

// Aumenta con espacios a la derecha o corta una cadena, a la longitud deseada
	/**
	* Formato salida.
	*
	* @param cadena the cadena
	* @param longitud the longitud
	* @return the string
	*/
	private String formatoSalida(String cadena, int longitud){
		String cadenaNueva = cadena;
		if ( cadena.length() < longitud){
			do{
				cadenaNueva += " ";
				}while(cadenaNueva.length() < longitud);
			}
		else if ( cadena.length() > longitud){
			cadenaNueva = cadena.substring(0, longitud);
			}
		return cadenaNueva;
	}

	/**
	 * Se encarga de armar el objeto de favorito para ser dado de alta.
	 * @param datos Informacion del favorito.
	 * @param idModulo tipo de transaccion efectuada
	 */
	private void darDeAltaFavorito( String datos, String idModulo) {
		EIGlobal.mensajePorTrace("Se va a dar de alta el favorito", EIGlobal.NivelLog.INFO);
		FavoritosEnlaceBean favorito = 	new FavoritosEnlaceBean();
		String[] dat = datos.split("@");
		favorito.setContrato( dat[0] );
		favorito.setCodCliente( dat[1] );
		favorito.setCuentaCargo( dat[2] );
		favorito.setDescCuentaCargo( dat[3] );
		favorito.setCuentaAbono( dat[4] );
		favorito.setDescCuentaAbono( dat[5] );
		favorito.setConceptoOperacion( dat[6] );
		favorito.setIdModulo(idModulo);
		favorito.setImporte(new Double( dat[7]) );
		favorito.setDescripcion( dat[8] );
		EIGlobal.mensajePorTrace("Se creara el favorito: "+favorito.getDescripcion(), EIGlobal.NivelLog.INFO);
		if(!this.agregarFavorito(favorito)){
			EIGlobal.mensajePorTrace("ERROR al crear el favorito: "+favorito.getDescripcion(), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * CAMBIO PYME FSW - INDRA MARZO 2015
	 * Se encarga de cargar los datos del favorito en el form.
	 * @param ses Objeto sesion
	 * @param req Objeto request
	 * @return String Id Modulo
	 */
	private String iniciaCargaDeFavorito(HttpSession ses,
			HttpServletRequest req) {
		FavoritosEnlaceBean favorito = (FavoritosEnlaceBean) ses.getAttribute(FavoritosConstantes.ID_FAVORITO);
		EIGlobal.mensajePorTrace("Inicia la carga de un favorito: "+favorito, EIGlobal.NivelLog.INFO);
		if(favorito!=null){
			req.setAttribute("esFavorito", "1");
			req.setAttribute("concepto",favorito.getConceptoOperacion());
			EIGlobal.mensajePorTrace("favorito.getConceptoOperacion()["+favorito.getConceptoOperacion()+"]", EIGlobal.NivelLog.INFO);
			double importe=favorito.getImporte();
			NumberFormat formatter = new DecimalFormat("#0.0#");
			req.setAttribute("monto",formatter.format(importe));
			EIGlobal.mensajePorTrace("favorito.getImporte()["+favorito.getImporte()+"]", EIGlobal.NivelLog.INFO);
			req.setAttribute("ctaCargoFav",favorito.getCuentaCargo());
			EIGlobal.mensajePorTrace("favorito.getCuentaCargo()["+favorito.getCuentaCargo()+"]", EIGlobal.NivelLog.INFO);
			req.setAttribute("ctaAbonoFav",favorito.getCuentaAbono());
			if( "TRAN".equals( favorito.getIdModulo() )  ){//Solo para Transf Mismo Banco MN
				EIGlobal.mensajePorTrace("favorito.getCuentaAbono()["+favorito.getCuentaAbono()+"]", EIGlobal.NivelLog.INFO);
				req.setAttribute("tipoCtaAbono", favorito.getCuentaAbono().length() == 16 ? "TDC" : "CTA" );
			}
		}
		EIGlobal.mensajePorTrace("Termina la carga de un favorito: "+favorito, EIGlobal.NivelLog.INFO);
		if( "PAGT".equals( favorito.getIdModulo() )  ){
			return "3";// Para Pago TDC
		} else {
			return "0";// Para Moneda Nacional
		}
	}
} //Fin de la Clase