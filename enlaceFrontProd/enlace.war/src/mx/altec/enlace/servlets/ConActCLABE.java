/*Banco Santander Mexicano
  Clase ConActCLABE consulta de cuentas CLABE 11 digitos
  @Autor:Rafael Rosiles Soto
  @version: 1.0
  fecha de creacion: 14 de Enero de 2004
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ConActCLABE extends BaseServlet {
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction (request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction (request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		EIGlobal.mensajePorTrace ("*** ConActCLABE.DefaultAction entrando ***", EIGlobal.NivelLog.INFO);

		boolean sesionvalida = SesionValida( request, response );
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta y Actualizaci&oacute;n de cuentas CLABE",
				"Administraci&oacute;n y control &gt; Cuentas &gt; Actualizaci&oacute;n de cuentas CLABE","s37010h", request));
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("Hora", ObtenHora());
		request.setAttribute("ContUser",ObtenContUser(request));

		String pathFileOutput = "/Download/"+ session.getContractNumber() + ".oclabee";
		String pathFileInput = Global.DOWNLOAD_PATH + session.getContractNumber() + ".oclabe";

		String cuadroDialogo = "";

		String showButton = "";

		// Parametro que se debe agregar a IENLACE para mostrar el numero de cuentas
		// que el usuario desee
		String NumeroPag = IEnlace.REGISTROS_CLABE;
        System.out.println("N�mero de registros a mostrar en CLABE  ....." +NumeroPag);

		/*Valida la sesion del usuario*/
		System.out.println("Voy a verificar las facultades .....");
		if(sesionvalida &&
		   (session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC) || session.getFacultad(session.FAC_TRANSF_NOREG) ||
			session.getFacultad(session.FAC_CCENVARCHPR_CONF)    || session.getFacultad(session.FAC_CARGO_CHEQUES))){
				System.out.println("Voy a verificar las facultades .....");
		 	    int total=0;
  		        int NumeroRegistros = 0;
  		        int Servicio;

	            // llamada a Servicio para crear archivo de salida
	            Servicio = consultaCLABE( request );

	            if (Servicio==1) {
		            NumeroRegistros = getnumberlinesCtasCLABE(pathFileInput)-1;
	    	  	}
	    	  	else  {
					NumeroRegistros = 0;
				}

				EIGlobal.mensajePorTrace( "***cuentas CLABE > " + String.valueOf(NumeroRegistros), EIGlobal.NivelLog.INFO);

				String[][] arrCuentas = ObteniendoCtasCLABE(pathFileInput,NumeroRegistros, "oclabe");

				String avisoCuentas;
				System.out.println("Regrese de ObteniendoCtasCLABE ");

				if (NumeroRegistros <= 0){
					cuadroDialogo   = " showMessage('" + NumeroRegistros + "');";
				}
				if (NumeroRegistros > Integer.parseInt(NumeroPag) ) {
                    System.out.println("NumeroRegistros  ....." +NumeroRegistros);
                    System.out.println("NumeroPag  ....." +NumeroPag);
					NumeroRegistros = Integer.parseInt(NumeroPag);
					cuadroDialogo   = " showMessage('" + Integer.parseInt(NumeroPag) + "');";
					showButton      = "<a href='javascript:;' onClick=" + "\"MM_openBrWindow('" + pathFileOutput + "','ayuda','menubar=yes,scrollbars=yes,resizable=yes,width=750,height=500')\""+ ">" + "<img border='0' name='imageField32' src='/gifs/EnlaceMig/gbo25230.gif' width='83' height='22' alt='Exportar'></a>";
					System.out.println("El show es:  ====> " + showButton);
					//showButton      = "<a href='" +  pathFileOutput + "'><img border='0' name='imageField32' src='/gifs/EnlaceMig/gbo25230.gif' width='83' height='22' alt='Exportar'></a>";
					//<a href="javascript:;" onClick="MM_openBrWindow('/EnlaceMig/ayuda004_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=320')"
					avisoCuentas    = "Si desea realizar la actualizaci&oacute;n de m&aacute;s de "+NumeroPag+" cuentas, deber&aacute; exportar su archivo e importarlo en la opci&oacute;n de Alta de cuentas CLABE";

				}
				else {
					avisoCuentas = "El n&uacute;mero de cuentas que se pueden visualizar son : " + NumeroPag;
				}


				String tablaCuentas="<table border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF>"+
						    "<tr><td align='center' class=tittabdat nowrap>Todas&nbsp;<input type='checkbox' name='allbox' value='checkbox' onClick='CheckAll();'></td>"+
						    "<td align='center' class=tittabdat nowrap>Tipo Operaci&oacute;n</td>"+
						    "<td align='center' class=tittabdat nowrap>Clave Prov</td>"+
						    "<td align='center' class=tittabdat nowrap>Cuenta anterior</td>"+
					   	    "<td align='center' class=tittabdat nowrap>Cuenta CLABE</td>"+
						    "<td align='center' class=tittabdat nowrap>Descripci&oacute;n</td>"+
						    "<td align='center' class=tittabdat nowrap>Banco</td>"+
						    "<td align='center' class=tittabdat nowrap>Plaza</td>"+
						    "<td align='center' class=tittabdat nowrap>Sucursal</td></tr>";

				String color = "";
				String fin_linea="";
				//System.out.println("Antes de entrar al for");
				for ( int i = 1; i <= NumeroRegistros; i++ ) {
					if ( ( i % 2 ) == 0 )
						color = "textabdatobs";
					else
						color = "textabdatcla";
					//Validar cuenta para inhabilitar el checkbox
					fin_linea="";
					//System.out.println("============================================================");
					//System.out.println(arrCuentas[i][1].trim());
					//System.out.println(arrCuentas[i][2].trim());
					//System.out.println(arrCuentas[i][3].trim());
					//System.out.println(arrCuentas[i][4].trim());
					//System.out.println(arrCuentas[i][5].trim());
					//System.out.println(arrCuentas[i][6].trim());
					//System.out.println(arrCuentas[i][7].trim());
					//System.out.println(arrCuentas[i][8].trim());
					//System.out.println("============================================================");
					if(validaCuenta(arrCuentas[i][4].trim()))
						fin_linea = "<input type='checkbox' name='ctasCLABE" + String.valueOf(i)+ "' value='"+arrCuentas[i][1]+"|"+arrCuentas[i][2]+"|"+arrCuentas[i][4] + "|'>";
					else
						fin_linea = "&nbsp;";

						//System.out.println("FIN_LINEA .....  >" + fin_linea + "<");

						tablaCuentas += "<tr><td align='center' class="+ color +">"+ fin_linea+//<input type='checkbox' name='ctasCLABE"+String.valueOf(i)+"' value='"+arrCuentas[i][1]+"|"+arrCuentas[i][2]+"|"+arrCuentas[i][4] + fin_linea +
	            	                "&nbsp;</td><td align='center' class="+ color +">"+arrCuentas[i][1]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][2]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][3]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][4]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][5]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][6]+
									"&nbsp;</td><td class="+ color +" nowrap>"+arrCuentas[i][7]+
									"&nbsp;</td><td align='center' class="+ color +">"+arrCuentas[i][8]+"&nbsp;</td></tr>";
				}

				tablaCuentas += "</table>";

				//System.out.println("La tabla de consulta es: ....................");
				//System.out.println("====>" + tablaCuentas + "<====");
				request.setAttribute("cuadroDialogo", cuadroDialogo);
				request.setAttribute("showButton", showButton);
				request.setAttribute("PathFile", pathFileOutput);
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Cuentas", tablaCuentas );
				request.setAttribute("avisoCuentas", avisoCuentas );
				evalTemplate( "/jsp/ConActCLABE.jsp", request, response );
			}
		else {
			//System.out.println("Este contrato NO tiene facultades para esta operaci�n");
			//request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			//evalTemplate( IEnlace.ERROR_TMPL, request, response );
			response.sendRedirect("SinFacultades");
		}
    }


    public int consultaCLABE( HttpServletRequest request ) throws ServletException
    {
    	HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String archivoRemoto = session.getContractNumber() + ".oclabe";
		String archivoRemoto2 = session.getContractNumber() + ".oclabee";
		String trama_salida	  = null;
		String trama_entrada  = "";
		String cabecera		  = "1EWEB"; // medio de entrega
		String tipo_operacion = "CCCO";  // prefijo del servicio ( consulta de cuentas Clabe)

		String usuario	= session.getUserID();         // Usuario

		String contrato = session.getContractNumber(); // Contrato

		String perfil   = session.getUserProfile();    // Perfil

		int result = 0;

	    EIGlobal.mensajePorTrace( "*** ConActCLABE.consultaCLABE entrado ***", EIGlobal.NivelLog.INFO);
		trama_entrada	=       cabecera + "|"
						+        usuario + "|"
						+ tipo_operacion + "|"
						+       contrato + "|"
						+        usuario + "|"
						+         perfil + "|"
						+		          "1|";

		EIGlobal.mensajePorTrace( "*** ConActCLABE.consultaCLABE  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		try{
			Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
			trama_salida = (String) hs.get("BUFFER");
		}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
				return 0;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}

		EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  trama salida >> "+trama_salida, EIGlobal.NivelLog.DEBUG);

	    // Verifica si la cadena es nula o existe error
		if(trama_salida.substring (0,trama_salida.indexOf('|')).equals("CCCO0000")) {

			EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  inicio de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);

			ArchivoRemoto archivoCLABE = new ArchivoRemoto();
			ArchivoRemoto archivoCLABE2 = new ArchivoRemoto();

			// Copia de archivo remoto a ruta local
			if(!archivoCLABE.copiaArchivo(archivoRemoto, Global.DOWNLOAD_PATH)) {
				EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  no se pudo copiar archivo remoto:" + archivoRemoto, EIGlobal.NivelLog.INFO);
			}
			else {
			    EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  archivo remoto copiado exitosamente:" + archivoRemoto, EIGlobal.NivelLog.INFO);
			    result = 1;
			}

			// Copia de archivo remoto a ruta local
			if(!archivoCLABE.copiaArchivo(archivoRemoto2, Global.DOWNLOAD_PATH)) {
				EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  no se pudo copiar archivo remoto:" + archivoRemoto, EIGlobal.NivelLog.INFO);
			}
			else {
			    EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  archivo remoto copiado exitosamente:" + archivoRemoto, EIGlobal.NivelLog.INFO);
			    result = 1;
			}


			if(!archivoCLABE2.copiaLocalARemoto(archivoRemoto2,"WEB")){
			//if(!archivoCLABE2.copiaArchivo(archivoRemoto2, "/tmp/")) {
				EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  no se pudo copiar archivo remoto:" + archivoRemoto2, EIGlobal.NivelLog.INFO);
			}
			else {
			    EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  archivo remoto copiado exitosamente:" + archivoRemoto2, EIGlobal.NivelLog.INFO);
			    result = 1;
			}

			EIGlobal.mensajePorTrace("*** ConActCLABE.consultaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);

		}
		else {
			if (trama_salida==null) {
				EIGlobal.mensajePorTrace( "*** ConActCLABE.consultaCLABE  error >> Trama de salida vacia...<<", EIGlobal.NivelLog.DEBUG);
			}
			else {
				String mensajeErr = trama_salida.substring(trama_salida.indexOf('|')+1,trama_salida.length()) ;
				EIGlobal.mensajePorTrace( "*** ConActCLABE.consultaCLABE  error >>" + mensajeErr + "<<", EIGlobal.NivelLog.DEBUG);
			}
			result=0;
		}

	    EIGlobal.mensajePorTrace ("*** ConActCLABE.consultaCLABE  saliendo ... ***", EIGlobal.NivelLog.INFO);

	    return result;
    }

	private boolean validaCuenta(String cuenta){
		//System.out.println("Estoy entrando a Validacuenta ........ " + cuenta);
		String validos="0123456789";
		boolean resp=false;
		int cont=0;
		int i=0;
		char car;
		for(i=0;i<cuenta.length();i++){
			car = cuenta.charAt(i);
			if(validos.indexOf(car)!=-1){
			    cont++;
			}
		}
		//System.out.println("Los valores en Validacuenta con ....i=>" + i + "<    cont=>" + cont + "<" );
		if(i!=cont)
			resp=false;
		else{
			if(cuenta.trim().equals("000000000000000000"))
				resp=false;
			else
				resp=true;
		}
		return resp;
	}
}
