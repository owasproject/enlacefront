package mx.altec.enlace.servlets;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.beans.OperacionesProgramadasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.MDI_Importar;
import mx.altec.enlace.bo.OperacionesProgramadasBO;
import mx.altec.enlace.dao.FavoritosEnlaceDAO;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.FavoritosConstantes;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

//VSWF
/**Praxis
*Q05-0001087
*ivn Ma. Isabel Valencia Navarrete
*25/01/2005
*CUANDO SE REALIZA UNA TRANSFERENCIA PINTAR EN EL LOG FECHA, HR,
*DIRECCI?N IP(DE DONDE FUE REALIZADA LA TRANSFERENCIA)
*EJEMPLO:
*[20/Jan/2005 12:02:03] DIRECCI?N IP  TRAMA
*/

public class MDI_Interbancario extends BaseServlet
{
	
	/**
	 * Constante con la literal "DIBT"
	 */
	public static final String DIBT = "DIBT";
	/**
	 * Constante con la literal "\n		</td>"
	 */
	public static final String CIERRA_TD = "\n		</td>";
	/**
	 * Constante con la literal "NOSUCCESS"
	 */
	public static final String NOSUCCESS = "NOSUCCESS";
	/**
	 * Constante con la literal "MDI_Interbancario - generaTablaDepositos(): Algo salio mal escribiendo: "
	 */
	public static final String MENS_ERROR = "MDI_Interbancario - generaTablaDepositos(): Algo salio mal escribiendo: ";
	/**
	 * Constante con la literal "NuevoFormatoInterbancario"
	 */
	public static final String NUEVO_FORMATO_INTERBANCARIO = "NuevoFormatoInterbancario";
	/**
	 * Constante con la literal "Forma Aplicaci&oacute;n"
	 */
	public static final String FORMA_APLICACION = "Forma Aplicaci&oacute;n";
	
	/**
	 * Constante con la literal cuenta sesion
	 */
	private static final String CUENTA_SES = "cuenta_ses";

	/** La constante serialVersionUID **/
	private static final long serialVersionUID = -7486117522399718098L;

	/**
	 * Variable para la cuentas no registradas
	 */
	transient String facCtasNoReg="";

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		String contrato="";
		String tipoCuenta="";
		String usuario="";
		String clavePerfil="";
		String facImpArc="";
		String modulo="";
		/*variables para separar operaciones en linea y programadas*/
			String inTransNoReg="";
			String outTransNoRegNProg="";
			String outTransNoRegSProg="";
		/*variables para calendario*/
			boolean fac_programadas;
			String strInhabiles = "";
			String fecha_hoy	= "";

		EI_Tipo Reg=new EI_Tipo(this);
		EI_Tipo NoReg=new EI_Tipo(this);

		//contiene las tramas de favoritos
		EI_Tipo descFav = new EI_Tipo(this);

		short  sucursalOpera=(short)787;
		int radio=0;
		/************* Modificacion para la sesion ***************/
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		/*Se inicializan variables para calendario*/
			fac_programadas = session.getFacultad(session.FAC_PROGRAMA_OP);
			strInhabiles    = armaDiasInhabilesJS();
			fecha_hoy       = ObtenFecha();

		/*Se establecen parametros para calendario*/
		req.setAttribute("diasInhabiles", strInhabiles);
		req.setAttribute("fecha_hoy",fecha_hoy);
		if ( !fac_programadas ) {
			req.setAttribute("fac_programadas1","0");
		} else {
				req.setAttribute("fac_programadas1","1");
		}

		EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Entrando a Deposito Interbancario. Ver ARV 3.0", EIGlobal.NivelLog.DEBUG);
		//Analisis de sesiones
		// VSWF - JGAL - Se omite de logs:
		EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): la Basesession es " + session, EIGlobal.NivelLog.DEBUG);
		if (ses.isNew()){
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): es nueva la SESSION ", EIGlobal.NivelLog.DEBUG);
		}
		if (ses.getAttribute(CUENTA_SES) == null)
		{
			EIGlobal.mensajePorTrace("MDI_Interbancario, Creando nuevo contador " , EIGlobal.NivelLog.DEBUG);
			ses.setAttribute(CUENTA_SES,Integer.valueOf(0) );
		}

		int cuenta_ses = ( (Integer) ses.getAttribute(CUENTA_SES) ).intValue();
		EIGlobal.mensajePorTrace("MDI_Interbancario, contador sesion: " + cuenta_ses, EIGlobal.NivelLog.DEBUG);
		ses.setAttribute(CUENTA_SES, Integer.valueOf(cuenta_ses +1) );

		String NombreArchInter=(String) getFormParameter(req,"ArchivoNomInter");
		EIGlobal.mensajePorTrace("Nombre del Archivo " + NombreArchInter , EIGlobal.NivelLog.INFO);

		if (ses.getAttribute("InterArchivo") != null){
			req.getSession().removeAttribute("InterArchivo");
		}
		if (NombreArchInter != null)
		{
			EIGlobal.mensajePorTrace("MDI_Interbancario, Archivo con Nombre:" + NombreArchInter, EIGlobal.NivelLog.INFO);

			//Separaci?n de Nombre del Archivo
			char separaCampo='\\';
			int j_aux=0;
			for (int i_aux = 0; i_aux < NombreArchInter.length(); i_aux++ )
			  {
				if ( NombreArchInter.charAt( i_aux ) == separaCampo ){
					j_aux=i_aux;}
			  }

			String SoloNombreArch = NombreArchInter.substring(j_aux+1);
			EIGlobal.mensajePorTrace("MDI_Interbancario, Nombre final Archivo:" + SoloNombreArch, EIGlobal.NivelLog.INFO);

			//Se separo nombre Archivo y se carga a variable Sesion. Se borra si ya exist?a
			if (ses.getAttribute("InterArchivo") != null){
				req.getSession().removeAttribute("InterArchivo");}

			if (ses.getAttribute("InterArchivo") == null){
				ses.setAttribute("InterArchivo", SoloNombreArch );}
		}
		else{
			EIGlobal.mensajePorTrace("MDI_Interbancario, de Nada", EIGlobal.NivelLog.DEBUG);}
		//Termina analisis de sesion

		boolean sesionvalida=SesionValida( req, res );
		if(sesionvalida)
		{
			if (req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
				if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
				} else if (getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
					req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
							getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER));
				}
			}
			session.setModuloConsultar(IEnlace.MDep_inter_cargo);
			//***********************************************
			//Variables de sesion ...
			contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): contrato: "+contrato, EIGlobal.NivelLog.INFO);
			sucursalOpera=Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			usuario=(session.getUserID8()==null)?"":session.getUserID8();
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): usuario: "+usuario, EIGlobal.NivelLog.INFO);
		 	clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): clavePerfil: "+clavePerfil, EIGlobal.NivelLog.INFO);
			facCtasNoReg=(session.getFacultad(session.FAC_TRANSF_NOREG))?"true":"false";
			facImpArc=(session.getFacultad(session.FAC_IMPORTA_INTERBAN))?"true":"false";

			//Se llama si es desde favoritos
			if(identificarEsFavorito(req)){
				EIGlobal.mensajePorTrace("Si viene de favoritos", EIGlobal.NivelLog.INFO);
				iniciaDepositosFavoritos(ses,req);
				modulo = "0";
			}else {
			modulo=(String) getFormParameter(req,"Modulo");
				//INDRA PYME
				// UNIFICAR ARCHIVO FAVORITOS TOKEN
				if (modulo == null) {
					modulo = (String)  req.getAttribute("Modulo");
				}
			}
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): el modulo es: "+modulo , EIGlobal.NivelLog.INFO);
			if (getFormParameter(req,"Registradas")!=null)
			 {
				radio = Integer.parseInt((String) getFormParameter(req,"Registradas"));
				EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Radio = " + radio, EIGlobal.NivelLog.INFO);
			 }
			else
			 {
				radio=0; //por default
				EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Radio = " + radio, EIGlobal.NivelLog.INFO);
			 }
			req.getSession().setAttribute ("Radio",radio);

			/* VECTOR 06-2016: SPID */
			String operDivisa = (String) getFormParameter(req,"tipoDivisaForm");
			req.setAttribute("valorDivisaForm",operDivisa);
			String rfcTransferUsd = (String) getFormParameter(req,"RFCReg");
			req.setAttribute("rfcTransferUsd",rfcTransferUsd == null ? " ": rfcTransferUsd);
			EIGlobal.mensajePorTrace("MDI_Interbancario - RFC Trx <" + rfcTransferUsd + ">", EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace(" ", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): ", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("                               radio  = " + radio, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("                               modulo = " + modulo, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(" ", EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("modulo.equals(\"1\")["+"1".equals(modulo)+"]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("modulo.equals(\"2\")["+"2".equals(modulo)+"]", EIGlobal.NivelLog.INFO);
			String auxi = radio!=2?"diferente":"igual";
			EIGlobal.mensajePorTrace("radio!=2["+auxi+"]", EIGlobal.NivelLog.INFO);
			if("1".equals(modulo) || "2".equals(modulo) && (radio!=2)) //jppm
			 {
				//INICIO PYME 2015 INDRA operaciones programadas Registradas / No Registradas
				String inTransReg="";
				String outTransRegNProg="";
				String outTransRegSProg="";
				if ("2".equals(modulo)){
					inTransReg = getFormParameter(req,"TransReg");
					inTransNoReg = getFormParameter(req,"TransNoReg");
					outTransNoRegNProg = "";
					outTransNoRegSProg = "";

					if (inTransReg.length()>0){
//						outTransRegNProg = sepTrnsPrg("N", inTransReg, "@", 10, 15);
//						outTransRegSProg = sepTrnsPrg("S", inTransReg, "@", 10, 15);
						Reg.iniciaObjeto(inTransReg);
					}
					if (inTransNoReg.length()>0)
					{
//						outTransNoRegNProg=sepTrnsPrg("N", inTransNoReg, "@", 10, 15);
//						outTransNoRegSProg=sepTrnsPrg("S", inTransNoReg, "@", 10, 15);
						NoReg.iniciaObjeto(inTransNoReg);
					}
//					EIGlobal.mensajePorTrace("outTransRegNProg: "+outTransRegNProg, EIGlobal.NivelLog.INFO);
//					EIGlobal.mensajePorTrace("outTransRegSProg: "+outTransRegSProg, EIGlobal.NivelLog.INFO);
//					EIGlobal.mensajePorTrace("outTransNoRegNProg: "+outTransNoRegNProg, EIGlobal.NivelLog.INFO);
//					EIGlobal.mensajePorTrace("outTransNoRegSProg: "+outTransNoRegSProg, EIGlobal.NivelLog.INFO);

//					if(!"".equals(outTransRegNProg)){
//						Reg.iniciaObjeto(outTransRegNProg);
//					}else{
//						Reg.iniciaObjeto(outTransRegSProg);
//					}
//					if(!"".equals(outTransNoRegNProg)){
//						NoReg.iniciaObjeto(outTransNoRegNProg);
//					}else{
//						NoReg.iniciaObjeto(outTransNoRegSProg);
//					}

				} else {
					Reg.iniciaObjeto(getFormParameter(req,"TransReg"));
					NoReg.iniciaObjeto(getFormParameter(req,"TransNoReg"));
				}
				//FIN PYME 2015 INDRA operaciones programadas
				req.getSession().setAttribute ("Registradas",getFormParameter(req,"TransReg"));
				req.getSession().setAttribute ("NoRegistradas",getFormParameter(req,"TransNoReg"));
			}

			//FAVORITOS ----------------------
			String txtFavoritos = req.getParameter("strFav");
			EIGlobal.mensajePorTrace("------------------>Los datos de las descripciones de los favoritos es: "+txtFavoritos, EIGlobal.NivelLog.INFO);
			if((txtFavoritos!=null)&&(!"".equals(txtFavoritos))){
				descFav.iniciaObjeto(txtFavoritos);
				EIGlobal.mensajePorTrace("---SE inserta el EIGlobal con favoritos",EIGlobal.NivelLog.INFO);
				ses.setAttribute(FavoritosConstantes.ID_LISTA_DESCRIPCIONES, descFav);
			}

			if("0".equals(modulo)){
				iniciaDepositos( req, res , tipoCuenta, facImpArc, contrato);
			}
			else if("1".equals(modulo))
				/* VECTOR 06-2016: SPID */
				generaTablaDepositos( req, res , contrato, tipoCuenta ,  usuario, clavePerfil, facImpArc, Reg, NoReg, NombreArchInter, operDivisa);
			else if("2".equals(modulo)) //validaci?n de datos duplicados
			 {


				EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Verificando si son en linea o por archivo para validar la duplicidad", EIGlobal.NivelLog.DEBUG);
				if (radio!=2)
				 {
					creaArchivoTib(req, res, Reg, NoReg);
					EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Se crea archivo para duplicidad en linea", EIGlobal.NivelLog.DEBUG);
				 }
				else
				 {
					EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Se validan duplicados por archivo", EIGlobal.NivelLog.DEBUG);
					validaDuplicados(req, res);
				 }
			 }
		}

		EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Saliendo de Deposito interbancario.", EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Metodo para desentramas la cadena de las operaciones programadas.
	 * @since 16/03/2015
	 * @author FSW-Indra
	 * @param cadenaProgramadas de tipo String
	 * @param contrato de tipo String
	 * @param usuario de tipo String
	 * @param clavePerfil de tipo String
	 * @return ArrayList<OperacionesProgramadasBean> de tipo OperacionesProgramadasBean
	 */
	private List<OperacionesProgramadasBean> obtieneValorasOpeProgramadas(
			String cadenaProgramadas, String contrato, String usuario,
			String clavePerfil) {
		/*REGISTRADAS
			CuentaCargo|DescripcnCuentaCargo|CuentaAbno|Beneficiario|Impr|Concepto|RefIBnc|Banco|Plaza|Scrl|FechDelDia|RFC|ImpIVA|A|AplicacionDes|FchDeProgr@
			SPEI
				11000360907|PRUEBA PYMES 7X|4423430802|BENEFICIARIO02 SI PROG|02.00|CONCEPTO02 SI PROG|0234567|BANAM|01001|0234|23/03/2015|||H|Mismo D�a|24/03/2015@
			TEF
				11000402037|PRUEBA PYMES 5|4423430858|BENEFICIARIO|11.00|CONCEPTO|1234567|BAJIO|01001|1234|23/03/2015|||2|D�a Siguiente|24/03/2015@*/
		/*NO REGISTRADAS
			CuentaCargo|DescripcnCuentaCargo|CuentaAbno|Beneficiario|Impr|Concepto|RefIBnc|Banco|Plaza|Scrl|FechDelDia|RFC|ImpIVA|A|AplicacionDes|FchDeProgr@
			SPEI
				11000360907|PRUEBA PYMES 7X|4423430802|BENEFICIARIO02 SI PROG|02.00|CONCEPTO02 SI PROG|0234567|BANAM|01001|0234|23/03/2015|||H|Mismo D�a|24/03/2015@
			TEF
				11000402037|PRUEBA PYMES 5|4423430858|BENEFICIARIO|11.00|CONCEPTO|1234567|BAJIO|01001|1234|23/03/2015|||2|D�a Siguiente|24/03/2015@*/
		EIGlobal.mensajePorTrace(" ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               ENTRO obtieneValorasOpeProgramadas()", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               cadenaProgramadas = " + cadenaProgramadas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               contrato          = " + contrato, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               usuario           = " + usuario, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("                               clavePerfil       = " + clavePerfil, EIGlobal.NivelLog.INFO);

		ArrayList<OperacionesProgramadasBean> listaOperacionesProgramadas = new ArrayList<OperacionesProgramadasBean>();
		StringTokenizer numeroOperacionesProgramadas = new StringTokenizer(cadenaProgramadas,"@");
		EIGlobal.mensajePorTrace(" MDI_Interbancario - Operaciones Programadas ", EIGlobal.NivelLog.INFO);
		while (numeroOperacionesProgramadas.hasMoreElements()) {
			String cadenaOperacionesProg = (String) numeroOperacionesProgramadas.nextElement();
			OperacionesProgramadasBean operacionesProgramadas = new OperacionesProgramadasBean();
			String[] valoresProgramdas = cadenaOperacionesProg.split("\\|");
			operacionesProgramadas.setNumCuenta(contrato);
			operacionesProgramadas.setUsuario(usuario);
			operacionesProgramadas.setTipoCtaOrigen("P");
			operacionesProgramadas.setTipoCtaDestino("T");
			operacionesProgramadas.setTitulos("0");
			operacionesProgramadas.setCvePerfil(clavePerfil);
			operacionesProgramadas.setTipoOperacion(DIBT);
			operacionesProgramadas.setReferenciaBit("0");
			operacionesProgramadas.setCuenta1(valoresProgramdas[0]); //CuentaCargo
			operacionesProgramadas.setDescripcionCtaOrigen(valoresProgramdas[1]); //DescripcnCuentaCargo
			operacionesProgramadas.setCuenta2(valoresProgramdas[2]); //CuentaAbno
			operacionesProgramadas.setDescripcionCtaDestino(valoresProgramdas[3]); //Beneficiario?
			operacionesProgramadas.setImporte(valoresProgramdas[4]); //Impr
			operacionesProgramadas.setConcepto(armaConcepto(valoresProgramdas[5], valoresProgramdas[11], valoresProgramdas[12])); //Concepto
			operacionesProgramadas.setRefInterb(valoresProgramdas[6]); //RefIBnc
			operacionesProgramadas.setCveBancoCtaDestino(valoresProgramdas[7]); //Banco
			operacionesProgramadas.setPlazaCtaDestino(valoresProgramdas[8]); //Plaza
			operacionesProgramadas.setSucursalCtaDestino(valoresProgramdas[9]); //Scrl
			operacionesProgramadas.setFchRegistro(valoresProgramdas[10]); //FechDelDia
			if ( "2".equals(valoresProgramdas[13]) ) { //A (Forma de Aplicacion)
				operacionesProgramadas.setFormaAplicacion("TEF");
			} else {
				operacionesProgramadas.setFormaAplicacion("SPEI");
			}
			operacionesProgramadas.setFechaAplicacion(valoresProgramdas[15]); //FchDeProgr
			operacionesProgramadas.setDivisa(valoresProgramdas[16]);
			listaOperacionesProgramadas.add(operacionesProgramadas);
		}
		return listaOperacionesProgramadas;
	}

	/**
	 * @param concepto String
	 * @param rfc String
	 * @param iva String
	 * @return String
	 */
  private String armaConcepto(String concepto, String rfc, String iva)
  {
    concepto=formateaTamanioCampo(concepto,40);
    if(!"".equals(rfc.trim())){
      return concepto + "RFC " + rfc + " IVA " + iva;
    } else {
      return concepto;
    }
  }

  /**
	 * @param campo String
	 * @param tamanio String
	 * @return String
	 */
  private String formateaTamanioCampo(String campo,int tamanio)
  {
    String spc="                                                                                                                                                ";
    String campoFormateado="";

    campo=campo.trim();
    if(campo.length()<tamanio){
      campoFormateado=campo + spc.substring(0, (tamanio-campo.length()) );}
    if(campo.length()>tamanio){
      campoFormateado=campo.substring(0,tamanio);}
    if(campo.length()==tamanio){
      campoFormateado=campo;}
    return campoFormateado;
  }

  private String sepTrnsPrg(String prog, String inTrm, String sSep, int nCmp1, int nCmp2)
  {
    String outTrm="";
    String curMem="";
    String nxtTrm="";
    EIGlobal.mensajePorTrace("MDI_Interbancario - sepTrnsPrg():", EIGlobal.NivelLog.INFO);
    EIGlobal.mensajePorTrace("                    prog<" + prog + ">", EIGlobal.NivelLog.INFO);
    EIGlobal.mensajePorTrace("                    inTrm<" + inTrm + ">", EIGlobal.NivelLog.INFO);
    EIGlobal.mensajePorTrace("                    sSep<" + sSep + ">", EIGlobal.NivelLog.INFO);
    EIGlobal.mensajePorTrace("                    nCmp1<" + nCmp1 + ">", EIGlobal.NivelLog.INFO);
    EIGlobal.mensajePorTrace("                    nCmp1<" + nCmp2 + ">", EIGlobal.NivelLog.INFO);
    curMem=inTrm.substring(0,inTrm.indexOf(sSep));
    nxtTrm=inTrm.substring(inTrm.indexOf(sSep)+1);
    if(obtCmpNReg(curMem, "|", nCmp1).equals(obtCmpNReg(curMem, "|", nCmp2)))
    {
      if("S".equals(prog))
      {
        outTrm = "";
      }
      else
      {
        outTrm = curMem + sSep;
      }
    }
    else
    {
      if("S".equals(prog))
      {
        outTrm = curMem + sSep;
      }
      else
      {
        outTrm = "";
      }
    }
    if(nxtTrm.length()>0)
    {
      return outTrm + sepTrnsPrg(prog, nxtTrm, sSep, nCmp1, nCmp2);
    }
    else
    {
      return outTrm;
    }
  }

  /**
	 * @param inReg String
	 * @param sSep String
	 * @param nCmp int
	 * @return String
	 */
  private String obtCmpNReg(String inReg, String sSep, int nCmp)
  {
    String sCmp = "";
    if (nCmp == 0)
    {
      if (inReg.indexOf(sSep)>(-1))
      {
        sCmp = inReg.substring(0,inReg.indexOf(sSep));
      }
      else
      {
        sCmp = inReg;
      }
      EIGlobal.mensajePorTrace("MDI_Interbancario - obtCmpNReg(): => sCmp<" + sCmp + ">", EIGlobal.NivelLog.ERROR);
    }
    else
    {
      if (inReg.indexOf(sSep)>(-1))
      {
        sCmp = obtCmpNReg(inReg.substring(inReg.indexOf(sSep)+1, inReg.length()), sSep, nCmp-1);
      }
      else
      {
        sCmp = inReg;
      }
    }
    return sCmp.trim();
  }

/**
	 * Se encarga de cargar los datos del favorito en el form.
	 * @param ses Objeto sesion
	 * @param req Objeto request
	 */
private void iniciaDepositosFavoritos(HttpSession ses,
			HttpServletRequest req) {

	FavoritosEnlaceBean favorito = (FavoritosEnlaceBean) ses.getAttribute(FavoritosConstantes.ID_FAVORITO);
	FavoritosEnlaceDAO favDAO = null;
	String datosAdicionales = "";
	EIGlobal.mensajePorTrace("va a iniciar la carga de un favorito: "+favorito, EIGlobal.NivelLog.INFO);

	BaseResource session = (BaseResource) ses.getAttribute("session");

	if(favorito!=null){

		favDAO = new FavoritosEnlaceDAO();
		try{
		    datosAdicionales = favDAO.consultarDatosCuenta(session.getContractNumber(), favorito.getCuentaAbono());
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		req.setAttribute("esFavorito", "1");
		req.setAttribute("CargoReg",favorito.getCuentaCargo()+"|"+favorito.getDescCuentaCargo()+"|");
		req.setAttribute("AbonoReg",favorito.getCuentaAbono()+"|"+favorito.getDescCuentaAbono()+"@"+datosAdicionales);
		req.setAttribute("Concepto",favorito.getConceptoOperacion());
		double importe=favorito.getImporte();
		NumberFormat formatter = new DecimalFormat("#0.0#");
		req.setAttribute("Importe",formatter.format(importe));
		req.setAttribute("ctaCargoFav",favorito.getCuentaCargo());
		req.setAttribute("ctaAbonoFav",favorito.getCuentaAbono());
		String funcion_llamar	= getFormParameter(req,"ventana");
		req.setAttribute("Registradas", "1");

	}

	}
	/**
	 * Metodo para generar el campo Fecha Aplicacion para las Ctas
	 * Registradas
	 * @since 20/04/2015
	 * @author FSW-Indra
	 * @param contrato Numero de contrato
	 * @return String html para generar el campo Fecha aplicacion
	 */
	String generaCampoFchApRegistradas(String contrato){
		OperacionesProgramadasBO operacionesProgramadasBO = new OperacionesProgramadasBO();
        StringBuffer fchApReg = new StringBuffer("");
        try {
			if(!operacionesProgramadasBO.validaContratoMancomunado(contrato)){
				fchApReg.append( "\n	   <tr>" )
				.append( "\n	    <td class='tabmovtexbol' nowrap valign='middle'>Fecha de aplicaci&oacute;n:</td>" )
				.append( "\n	   </tr>" )
				.append( "\n	   <tr>" )
				.append( "\n	   <td class='tabmovtexbol' nowrap valign='middle'>" )
				.append( operacionesProgramadasBO.obtieneCamposFechaProgramada("fecha_completaReg", ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio())  )
				.append( operacionesProgramadasBO.obtieneOcultosFechaProgramada( ObtenDia(), ObtenMes(), ObtenAnio(), "1") )
				.append( "</td>" )
				.append( "\n	   </tr>" );
			}
		} catch (BusinessException e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return fchApReg.toString();
	}
/*************************************************************************************/
/******************************************** Desplegar Cuentas NoReg (si tiene fac) */
/*************************************************************************************/
	/**
	 * Metodo para generar el codigo html para la cuentas no registradas
	 * @param tipoCuenta tipo de cuenta a manejar
	 * @param req informacion de la pantalla
	 * @param contrato contrato de tipo string
	 * @return String cadena con el html para las ctas no registradas
	 */
	String cuentasNoRegistradas(String tipoCuenta, HttpServletRequest req, String contrato)
	{
		StringBuffer strNoRegistradas = new StringBuffer("");
		String totales="";

		ResultSet contResult = null;
		ResultSet bancoResult = null;
		Connection conn = null;
		//FSW-Indra ARS Inicio
		OperacionesProgramadasBO operacionesProgramadasBO = new OperacionesProgramadasBO();
		boolean esMancomunado = false;
		//FSW-Indra ARS Fin

		int totalBancos;

		/************* Modificacion para la sesion ***************/
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): FacCtasNoReg="+facCtasNoReg, EIGlobal.NivelLog.INFO);
		if("true".equals(facCtasNoReg.trim()))
		  {

			//***************************
			strNoRegistradas.append( "\n	<tr>" )
			.append( "\n	 <td colspan=4 class='tittabdat'><INPUT TYPE=radio id=ctaNoregistradas value=1 NAME=Registradas onClick='VerificaFac(0);'>&nbsp;<b>Cuentas No Registradas</b></td>" )
			.append( "\n	</tr>" )

			.append( "\n   <tr>" )
			.append( "\n	 <td class='textabdatcla' colspan=4>" )
			.append( "\n	  <table class='textabdatcla' cellspacing=3 cellpadding=2 border=0 width='100%'>" )
			.append( "\n	   <tr>" )
			.append( "\n	    <td class='tabmovtex' colspan=2 width='48%'>Cuenta de Cargo</td>" )
			.append( "\n		<td class='tabmovtex' colspan=2 >Cuenta de Abono</th>" )
			.append( "\n	   </tr>" )
			.append( "\n	   <tr>" )
			.append( "\n	    <td colspan=2 class='tabmovtex'>" )
			.append( "\n  		 <!--<input type=text name=textCargoNoReg  class=tabmovtexbol maxlength=22 size=22 onfocus=\"blur();\">" )
			.append( "\n        <A HREF=\"javascript:PresentarCuentasCargoNoReg();\"><IMG SRC=\"/gifs/EnlaceMig/gbo25420.gif\" border=0 align=absmiddle></A> -->" )
			// INICIO FSW-INDRA PYME MARZO 2015
			.append( "\n        <div style=\"width: 100%\">" )
			.append( "\n        <table>" )
			.append( "\n        <tr>" )
			.append( "\n        <td colspan=\"2\" width=\"290\"> " )
			.append( "\n        <select id=\"\" style=\"width: 100%\" class=\"tabmovtexbol comboCuenta\" disabled>" )
			.append( "\n        <option value=\"\">Seleccione una cuenta...</option>" )
			.append( "\n        </select>" )
			.append( "\n        </td>" )
			.append( "\n        </tr>" )
			.append( "\n        </table>" )
			.append( "\n       </div>" )

			// Fin FSW-INDRA PYME MARZO 2015
			.append( "\n        <input type=\"hidden\" name=\"CargoNoReg\" id=\"CargoNoReg\" >" )
			.append( "\n	    </td>" )

			// ClaBE ************
			.append( "\n		<td class='tabmovtex'>" )
			.append( "\n		  Tipo de Cuenta: " )
			.append( CIERRA_TD )
			.append( "\n		<td class='tabmovtex'>" )
			.append( "\n		 <input type=radio name=TipoCuenta checked>Cuenta ClaBE " )
			.append( "\n		 <input type=radio name=TipoCuenta >Tarjeta de D&eacute;bito " )
			.append( "\n		<br><input type=radio name=TipoCuenta >N&uacute;mero M&oacute;vil" )
			.append( CIERRA_TD )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n	    <td colspan=2 class='tabmovtex'><br></td>" )
			.append( "\n	    <td class='tabmovtex'>" )
			.append( "\n	      Cuenta/No. Tarjeta:/M&oacute;vil " )
			.append( "\n	    </td>" )
			.append( "\n	    <td class='tabmovtex'>" )
			.append( "\n		 <INPUT TYPE=text NAME=AbonoNoReg SIZE=25 MAXLENGTH=18>" )
			.append( CIERRA_TD )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n	    <td colspan=4 class='tabmovtex'><br></td>" )
			.append( "\n	   </tr>" )

			.append( "\n      <tr>" )
			.append( "\n        <td class='tabmovtex'>Banco:</td>\n" )
			.append( "\n        <td class='tabmovtex'><SELECT NAME=Banco class='tabmovtex'><option value=0 class='tabmovtex'> Seleccione un Banco </OPTION>\n" );

			String cveBanco=(session.getClaveBanco()==null)?"014":session.getClaveBanco();

			EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): Clave Banco : " +cveBanco, EIGlobal.NivelLog.INFO);

			try
			{
				conn = createiASConn(Global.DATASOURCE_ORACLE);
				if(conn == null)
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}

				/************************************************** Contar registros de bancos .... */
				totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";
				PreparedStatement contQuery = conn.prepareStatement(totales);
				if(contQuery == null)
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}
				contResult = contQuery.executeQuery();
				if(contResult==null)
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}
				if(contResult.next()){
					totalBancos = Integer.parseInt(contResult.getString(1));}
				else
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}
				EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

				/************************************************** Traer Bancos ... */
				String sqlbanco="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
				PreparedStatement bancoQuery = conn.prepareStatement(sqlbanco);
				if(bancoQuery==null)
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}
				bancoResult = bancoQuery.executeQuery();
				if(bancoResult==null)
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					return NOSUCCESS;
				}

				while(bancoResult.next())
				{
					if(!"BANME".equals((bancoResult.getString(1).trim())) && !("SERFI".equals(bancoResult.getString(1).trim())) ){
						strNoRegistradas.append( "<option value='" )
							.append( bancoResult.getString(1) ).append( "-" )
							.append( bancoResult.getString(3) ).append( "'>" )
							.append( bancoResult.getString(2) ).append( "\n" ); }
				}
				/**************************************************************************************************************/

				EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): Armando la pagina de cuentas no registradas.", EIGlobal.NivelLog.DEBUG);
			}
			catch( SQLException sqle )
			{EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);}
			finally
			{
				try
				{
					if( bancoResult != null ){
						bancoResult.close();
					}
					if( contResult != null ){
						contResult.close();
					}
					if( conn != null ){
						conn.close();
					}
				}catch(SQLException e) { EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);}
			}

			strNoRegistradas.append( "\n		 </SELECT>" )
			.append( CIERRA_TD )
			.append( "\n	    <td class='tabmovtex' >Plaza:</td>" )
			.append( "\n		<td class='tabmovtex' >" )
			.append( "\n			<table cellspacing=0 cellpadding=0 border=0>" )
			.append( "\n			<tr>" )
			.append( "\n				<td class='tabmovtex' >" )
			.append( "\n					<input type=hidden name=Plaza value='01001'>" )
			.append( "\n					<input type=text size=25 name=Descripcion value='MEXICO, DF' onFocus='blur();'>" )
			.append( "\n				</td>" )
			.append( "\n				<td class='tabmovtex' >" )
			.append( "\n					<a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif' ></a>" )
			.append( "\n				</td>" )
			.append( "\n			</tr>" )
			.append( "\n			</table>" )
			.append( CIERRA_TD )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n		<td class='tabmovtex'>Beneficiario:</td>" )
			.append( "\n		<td class='tabmovtex'><INPUT TYPE=text SIZE=25 MAXLENGTH=25 NAME=Beneficiario></td>" )
			.append( "\n		<td class='tabmovtex'>Sucursal:</td>" )
			.append( "\n		<td class='tabmovtex'><INPUT TYPE=text SIZE=10 MAXLENGTH=4 NAME=Sucursal></td>" )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n		<td class='tabmovtex'>Concepto:</td>" )
			.append( "\n		<td class='tabmovtex'><INPUT TYPE=text SIZE=25 MAXLENGTH=40 NAME=ConceptoNoReg></td>" )
			.append( "\n		<td class='tabmovtex'>Importe:</td>" )
			.append( "\n		<td class='tabmovtex'><INPUT TYPE=text NAME=ImporteNoReg SIZE=10 MAXLENGTH=17></td>" )
			.append( "\n	   </tr>" )

			/*MSD Ley de Transparencia, 11/2005*/
			.append( "\n	   <tr>" )
			.append( "\n		<td class='tabmovtex'>Referencia Interbancaria:</td>" )
			.append( "\n		<td class='tabmovtex'><INPUT TYPE=text SIZE=7 MAXLENGTH=7 NAME=ReferenciaNoReg></td>" )
			.append( "\n	    <td class='tabmovtex'>Requiere estado de cuenta fiscal?</td>" )
			.append( "\n	    <td class='tabmovtex'>&nbsp; <input type=radio name=EdoFiscal value=0 checked> No &nbsp; <input type=radio name=EdoFiscal value=1> Si</td>" )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n	    <td class='tabmovtex'>Forma de aplicaci&oacute;n</td>" )
			.append( "\n	    <td class='tabmovtex'><input type=radio name=FormaAplicaNR value=H checked> Mismo D&iacute;a</td>" )
			.append( "\n	    <td class='tabmovtex'>RFC Beneficiario:</td>" )
			.append( "\n	    <td class='tabmovtex'><input type=text name=RFC size=13 maxlength=13 onFocus='verificaRFCIVA(this,document.transferencia.EdoFiscal);'></td>" )
			.append( "\n	   </tr>" )

			.append( "\n	   <tr>" )
			.append( "\n	    <td class='tabmovtex'><br></td>" )
			.append( "\n	    <td class='tabmovtex'><input type=radio name=FormaAplicaNR value=2> D&iacute;a Siguiente</td>" )
			.append( "\n	    <td class='tabmovtex'>Importe IVA:</td>" )
			.append( "\n	    <td class='tabmovtex'><input type=text name=ImporteIVA size=13 maxlength=15 onFocus='verificaRFCIVA(this,document.transferencia.EdoFiscal);'></td>" )
			.append( "\n	   </tr>" );
			try {
				esMancomunado = operacionesProgramadasBO.validaContratoMancomunado(contrato);
				if( !esMancomunado ){
					//FSW-Indra ARS Inicio
					strNoRegistradas.append( "\n	   <tr>" )
					.append( "\n	    <td class='tabmovtexbol' nowrap valign='middle'>Fecha de aplicaci&oacute;n:</td>" )
					.append( "\n	   </tr>" )
					.append( "\n	   <tr>" )
					.append( "\n	   <td class='tabmovtexbol' nowrap valign='middle'>" )
					.append( operacionesProgramadasBO.obtieneCamposFechaProgramada("fecha_completaNoReg", ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio())  )
					.append( operacionesProgramadasBO.obtieneOcultosFechaProgramada( ObtenDia(), ObtenMes(), ObtenAnio(), "") )
					.append( "</td>" )
					.append( "\n	   </tr>" );
					//FSW-Indra ARS Fin
				}
			} catch (BusinessException e) {
				EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR); // ARS Indra Validar el tipo de excepcion el eplicar Sonnar
			}


			strNoRegistradas.append( "\n	  </table>" )
			.append( "\n	 </td>" )
			.append( "\n	</tr>" )

			.append( "\n	<tr>" )
			.append( "\n	 <td bgcolor=white colspan=4><br></td>" )
			.append( "\n	</tr>" )

			.append( "\n   <input type=hidden name=FechaNoReg size=10 value='" )
			.append( EnlaceGlobal.fechaHoy("dd/mm/aaaa") )
			.append( "' onFocus='blur();'>" );
		 }
		else
		 {
			// Para no perder el orden y numero de datos ...
			strNoRegistradas.append( "   <input type=hidden name=Radio>\n" )
			.append( "    <input type=hidden name=NCargo>\n" )
			.append( "    <input type=hidden name=NHIDDEN>\n" )

			.append( "    <input type=hidden name=RadioCheques>\n" )
			.append( "    <input type=hidden name=RadioDebito>\n" )

			.append( "    <input type=hidden name=NAbono>\n" )
			.append( "    <input type=hidden name=NBanco>\n" )
			.append( "    <input type=hidden name=NCPlaza>\n" )
			.append( "    <input type=hidden name=NPlaza>\n" )
			.append( "    <input type=hidden name=NBeneficiario>\n" )
			.append( "    <input type=hidden name=NSucursal>\n" )
			.append( "    <input type=hidden name=NConcepto>\n" )
			.append( "    <input type=hidden name=NImporte>\n" )

			.append( "    <input type=hidden name=NReferencia>\n" )
			.append( "    <input type=hidden name=NRadioFiscal>\n" )
			.append( "    <input type=hidden name=NRadioFiscal>\n" )
			.append( "    <input type=hidden name=NRadioFormaAplica>\n" )
			.append( "    <input type=hidden name=NBeneficiario>\n" )
			.append( "    <input type=hidden name=NRadioFormaAplica>\n" )
			.append( "    <input type=hidden name=NImporteIVA>\n" );
		 }
		//INCIO PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
		strNoRegistradas.append( "    <input type=\"hidden\" name=\"pintarFechaAp\" value=\"");
		if( !esMancomunado ){
			strNoRegistradas.append( "TRUE");
		} else {
			strNoRegistradas.append( "FALSE");
		}
		strNoRegistradas.append( "\"/>");
		//FIN PYME 2015 INDRA para manejar el campo fecha Ap en la operaciones programadas
		return strNoRegistradas.toString();
	}


/*************************************************************************************/
/************************************************** Generacion de Tabla - Modulo=1   */
/*************************************************************************************/
	public void generaTablaDepositos(HttpServletRequest req, HttpServletResponse res, String contrato, String tipoCuenta, String usuario, String clavePerfil, String facImpArc, EI_Tipo Reg, EI_Tipo NoReg, String NombreArchInter, String tipoDivisa)
			throws ServletException, IOException
	{

		String strTabla="";
		String strTablaArchivo="";
		String strBotones="";
		String botones="";
		String strArchivo="";
		String strArcRfc="";
		String strConceptoRfc="";

		String strErr="";
//		String strArc="";
		StringBuilder strArc =  new StringBuilder();
		String strArcProgramadas="";
		String tramaEnviar="";
		String nuevoFormatoPag="";
		String msgArchivo="";

		Vector listaPlazas = new Vector();
		List<String> listaBancos = new ArrayList<String>();

		int radio=0;
		int existeError=0;
		int cont=0;
		int contOk=0;

		boolean contEsc=true;
		boolean importacion=true;
		boolean arcErrores=true;
		boolean facCargo50=false;

		double importeTotal=0;

		EI_Tipo ArchivoI=new EI_Tipo(this);

		/************* Modificacion para la sesion ***************/
		HttpSession ses = req.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): GeneraTablaDepositos 1", EIGlobal.NivelLog.DEBUG);

		radio = Integer.parseInt((String) getFormParameter(req,"Registradas"));
		EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Radio: "+radio, EIGlobal.NivelLog.INFO);

		/****************** 50 */
		//Facultad para la la cuenta "$50"
		facCargo50=(session.getFacultad("DEPSINCAR"))?true:false;
		EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Facultad DEPSINCAR: "+facCargo50, EIGlobal.NivelLog.INFO);
		/*****************/

		try
		{
		importeTotal=0;
/************************************************** Calcular Importe para Registradas*/
		if(radio==0 || Reg.totalRegistros>=1){
			for(int i=0;i<Reg.totalRegistros;i++){
				importeTotal+=new Double(Reg.camposTabla[i][4]).doubleValue();}
		}
/************************************************** Calcular Importe para No Registradas */
		if(radio==1 || NoReg.totalRegistros>=1){
			for(int i=0;i<NoReg.totalRegistros;i++){
				importeTotal+=new Double(NoReg.camposTabla[i][4]).doubleValue();}
		}
/************************************************** Agregar desde Archivo */
		if(radio==2)
		{
			EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Procesando Archivo ...", EIGlobal.NivelLog.DEBUG);
			try
			{
				strArchivo += new String(getFileInBytes(req));
			}catch(Exception e)
			{EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
				strArchivo+="";}

			String miArchivo=(String) getFormParameter(req,"Archivo");

			EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Archivo: "+miArchivo, EIGlobal.NivelLog.INFO);

			MDI_Importar ArchImp = new MDI_Importar();
			nuevoFormatoPag=getFormParameter(req,"Formato");
			if(nuevoFormatoPag==null){
				nuevoFormatoPag="NO";
			}
			if(strArchivo.length()>0)
			{
				String linea="";
				StringBuilder errores = new StringBuilder();

				cont=0;
				contOk=0;

				//***** everis Declaraci?n RandomAccessFile 06/05/2008  ..inicio
				RandomAccessFile archivoEnvioPago = null;
				//***** everis Declaraci?n RandomAccessFile 06/05/2008  ..fin
				ValidaCuentaContrato valCtas = null;
				String tipoMoneda=(String) getFormParameter(req,"tipoMoneda");
	            if("USD".equals(tipoMoneda)){
	                ArchImp.indicadorOperDolares = true;
	            }
	            //tipoDivisa = tipoMoneda; 
	            EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): *****************************"
	                    + "Archivo: TIPO MONEDA "+tipoMoneda, EIGlobal.NivelLog.INFO);
				try
				{
					//creacion del archivo .tib para realilizar la validacion de duplicados jppm, incidencia IM199738
					File archivoEnvio	= new File(IEnlace.LOCAL_TMP_DIR+"/"+session.getUserID8()+".tib");

					//***** everis Declaraci?n RandomAccessFile l?nea comentada 06/05/2008  ..inicio
					//RandomAccessFile archivoEnvioPago = null;
					//***** everis Declaraci?n RandomAccessFile l?nea comentada 06/05/2008  ..fin

					if(archivoEnvio.exists())
						archivoEnvio.delete();
					archivoEnvioPago = new RandomAccessFile(IEnlace.LOCAL_TMP_DIR+"/"+session.getUserID8()+".tib", "rw");
					archivoEnvioPago.setLength(0); //para truncar el archivo en caso que exista
					archivoEnvioPago.writeBytes(strArchivo);
					EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos():Archivo importado - Se genero el archivo .tib", EIGlobal.NivelLog.DEBUG);
					//fin de creaci?n en archivo .tib

					BufferedReader strOut=new BufferedReader(new StringReader(strArchivo));
					EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Asignando buffer.", EIGlobal.NivelLog.DEBUG);

					llenaListaPlazas(listaPlazas);
					llenaListaBancos(listaBancos);

					String nombreArchivo=usuario+".lti";
					EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
					EI_Exportar ArcErr=new EI_Exportar(IEnlace.DOWNLOAD_PATH+"Errores.html");

					if(ArcErr.creaArchivo())
					{
						arcErrores=true;
						StringBuilder header=new StringBuilder();
						header.append("<html>");
						header.append("<head>\n<title>Estatus</title>\n");
						header.append("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
						header.append("</head>");
						header.append("<body bgcolor='white'>");
						header.append("<form>");
						header.append("<table border=0 width=420 class='textabdatcla' align=center>");
						header.append("<tr><th class='tittabdat'>informaci&oacute;on del Archivo Importado</th></tr>");
						header.append("<tr><td class='tabmovtex1' align=center>");

						header.append("<table border=0 align=center>");
						header.append("<tr><td class='tabmovtex1' align=center>");
						header.append("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
						header.append("<td class='tabmovtex1' align='center'>");
						header.append("<br>No se llev&oacute; a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>");
						header.append("</td></tr></table>");

						header.append("</td></tr>");
						header.append("<tr><td class='tabmovtex1'>");

						if(!ArcErr.escribeLinea(header.toString())){
							EIGlobal.mensajePorTrace(MENS_ERROR+strErr, EIGlobal.NivelLog.INFO);}
					}
					else{
						arcErrores=false; }
					EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Formato de Archivo: "+nuevoFormatoPag, EIGlobal.NivelLog.INFO);

					if(ses.getAttribute(NUEVO_FORMATO_INTERBANCARIO)!=null){
						ses.removeAttribute(NUEVO_FORMATO_INTERBANCARIO);
					}
					if("OK".equals(nuevoFormatoPag.trim()))
					  {
						ArchImp.nuevoFormato=true;
						ses.setAttribute(NUEVO_FORMATO_INTERBANCARIO,"true");
					  }
					 else{
						ses.setAttribute(NUEVO_FORMATO_INTERBANCARIO,"false");
					 }
				  if(ArcSal.creaArchivo())
				   {
					  //modificacion para integracion
					  // INICIA - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
					  // GENERACION DEL ARCHIVO AMBCI Y AMBCA
					  /**
					  llamado_servicioCtasInteg(IEnlace.MDep_inter_cargo," "," "," ",session.getUserID8()+".ambci",req);
					  llamado_servicioCtasInteg(IEnlace.MDep_Inter_Abono," "," "," ",session.getUserID8()+".ambca",req);
					   */
					  // FIN - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
					  // GENERACION DEL ARCHIVO AMBCI Y AMBCA

					  String tipo_relac_cta="";
					  String datoscta[]=null;
					  boolean verificaIndicador=false;
					  valCtas = new ValidaCuentaContrato();

					  while((linea=strOut.readLine()) != null)
					   {
						if("".equals(linea.trim())){
							linea = strOut.readLine();}
						if( linea == null ){
							break;
					   }
						cont++;
						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): formateando linea: "+linea.length(), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): linea: "+linea, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): linea.length: "+linea.length(), EIGlobal.NivelLog.INFO);
						ArchImp.formateaLinea(linea);

						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): err: "+ArchImp.err, EIGlobal.NivelLog.INFO);
						if(ArchImp.err==0)
						 {
						  existeError=0;
						  errores.delete(0, errores.length());

						  /****************** 50 */
						  if("SIN CUENTA".equals(ArchImp.cuentaCargo.trim()) && facCargo50)
							{
							  tipo_relac_cta="P";
							  ArchImp.titular="SIN TITULAR";
							}
						  else
							{
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Cuentas clabe " + ArchImp.conReferenciaInter, EIGlobal.NivelLog.INFO);
							  if(!(ArchImp.cuentaCargo.trim().length()==11))
								{
								  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Error en la longitud de la cuenta Cargo", EIGlobal.NivelLog.ERROR);
								  errores.append("<br><DD><LI>Error en la logitud de la cuenta Cargo.");
								  existeError++;
								}
							  else
								{
								  // INICIA - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
								  // GENERACION DEL ARCHIVO AMBCI Y AMBCA
								  /**
								  datoscta=BuscandoCtaInteg(ArchImp.cuentaCargo.trim(),IEnlace.LOCAL_TMP_DIR+"/"+session.getUserID8()+".ambci",IEnlace.MDep_inter_cargo);
								  */
							      if(ArchImp.indicadorOperDolares){
							          datoscta = valCtas.obtenDatosValidaCtaDolares(ArchImp.cuentaCargo.trim(), 
                                      IEnlace.MCargo_transf_dolar, req);
							      }else{

		                                 datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
                                                 session.getUserID8(),
                                                 session.getUserProfile(),
                                                 ArchImp.cuentaCargo.trim(),
                                                 IEnlace.MDep_inter_cargo);
							      }

								  // FIN - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
								  // GENERACION DEL ARCHIVO AMBCI Y AMBCA

								  if(datoscta==null)
									{
									  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La cuenta de Cargo No Existe.", EIGlobal.NivelLog.ERROR);
									  errores.append("<br><DD><LI>La cuenta de Cargo No Existe.");
									  existeError++;
									}
								  else
									{
									  ArchImp.cuentaCargo=datoscta[1].trim();
									  ArchImp.titular=datoscta[4];
									  if(ArchImp.titular==null || "".equals( ArchImp.titular.trim())){
										  ArchImp.titular="SIN TITULAR";}
									  tipo_relac_cta=datoscta[2];
									  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): tipo_relac_cta" + tipo_relac_cta, EIGlobal.NivelLog.INFO);
									}
								}
							}

						  // INICIA - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
						  // GENERACION DEL ARCHIVO AMBCI Y AMBCA
						  /**
						  datoscta=BuscandoCtaInteg(ArchImp.cuentaAbono.trim(),IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambca",IEnlace.MDep_Inter_Abono);
						  */
						  if(ArchImp.indicadorOperDolares){
						      datoscta = valCtas.obtenDatosValidaCtaDolares(ArchImp.cuentaAbono.trim(),
						              IEnlace.MDep_Inter_Abono, req);
      
						  }else{
						      datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
								                                    session.getUserID8(),
								                                    session.getUserProfile(),
								                                    ArchImp.cuentaAbono.trim(),
								                                    IEnlace.MDep_Inter_Abono);
						  }
						  // FIN - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
						  // GENERACION DEL ARCHIVO AMBCI Y AMBCA
						  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): CCBDatoscta = " + datoscta, EIGlobal.NivelLog.INFO);
						  if(datoscta==null && (!"true".equals(facCtasNoReg.trim()) || ArchImp.indicadorOperDolares)){
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La cuenta Abono No Existe.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>La cuenta de Abono No Existe.");
							  existeError++;
						  }
  						  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): CCBLength = " + ArchImp.cuentaAbono.trim().length(), EIGlobal.NivelLog.INFO);
						  //Se validan cuentas de 11 posiciones Q7295
						   if(!(ArchImp.cuentaAbono.trim().length()==16) && !(ArchImp.cuentaAbono.trim().length()==11) && !(ArchImp.cuentaAbono.trim().length()==10))
							{
							  if(ArchImp.cuentaAbono.trim().length()==18)
								{
								  String msg=verificaCuentaClaBE(ArchImp.banco.trim(),ArchImp.cuentaAbono.trim(),Integer.parseInt(ArchImp.sucursal));
								  if(!"OK".equals(msg))
									{
			  						  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Error en el d&iacute;gito verificador de la cuenta/CLABE no es v&aacute;lido.", EIGlobal.NivelLog.ERROR);
			  						 errores.append("<br><DD><LI>");
			  						 errores.append(msg);
			  						 existeError++;
									}
							    }
							   else
							    {
	  							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Cuenta inv&aacute;lida, la cuenta debe ser de 11, 16 (T. DEBITO) &oacute 18 (Cta. CLABE)  posiciones.", EIGlobal.NivelLog.ERROR);
	  							 errores.append("<br><DD><LI>Cuenta inv&aacute;lida, la cuenta debe ser de 11, 16 (T. DEBITO) &oacute 18 (Cta. CLABE)  posiciones.");
	  							 existeError++;
							    }
						    }

						  /**************************************************************************************************************/
						  /* Proyecto Unico  */
						  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): ArchImp.banco.trim()=" + ArchImp.banco.trim(), EIGlobal.NivelLog.INFO);
						  if( "SERFI".equals(ArchImp.banco.trim()) || "BANME".equals(ArchImp.banco.trim()))
							{
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La cuenta abono es santander o serfin no se puede abonoar interbancarias a cuentas internas.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>No se puede realizar una operaci&oacute;n interbancaria a cuentas internas.");
							  existeError++;
							}
						  /**************************************************************************************************************/

						  if(!ArchImp.verificaImporte())
							{
  							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El Importe No es Valido.", EIGlobal.NivelLog.ERROR);
  							 errores.append("<br><DD><LI>El Importe No es V&aacute;lido.");
  							 existeError++;
  							}
						  if(!ArchImp.verificaConcepto())
							{
  							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El Concepto No es Valido.", EIGlobal.NivelLog.ERROR);
  							 errores.append("<br><DD><LI>El Concepto debe ser alfanum&eacute;rico.");
  							 existeError++;
  							 }
						  if(!ArchImp.verificaReferencia())
							{
  							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La Referencia Interbancaria No es Valida.", EIGlobal.NivelLog.ERROR);
  							 errores.append("<br><DD><LI>La Referencia Interbancaria debe ser num&eacute;rica.");
  							 existeError++;
  							 }

						  if(!ArchImp.verificaSucursal())
							{
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La Sucursal No es Valida.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>La Sucursal No es V&aacute;lida.");
							  existeError++;
							  }
						  if(listaBancos.indexOf(ArchImp.banco.trim())<0)
							{
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El Banco no existe en Catalogo.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>El Banco no existe en Catalogo.");
							  existeError++;
							  }
						  if(listaPlazas.indexOf(ArchImp.plaza)<0)
						   {
 							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La Plaza No existe en Catalogo.", EIGlobal.NivelLog.ERROR);
 							 errores.append("<br><DD><LI>La Plaza No existe en Catalogo.");
 							 existeError++;
 							 }

						  if(ArchImp.indicadorOperDolares && "OK".equals(nuevoFormatoPag) && 
						         (ArchImp.formaAplica == null || "".equals(ArchImp.formaAplica.trim()))){
							  ArchImp.formaAplica = "1";
					      }

						  if(!ArchImp.verificaFormaAplica())
						   {
 							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): La forma de aplicacion no es valida.", EIGlobal.NivelLog.ERROR);
 							 errores.append("<br><DD><LI>El campo forma de aplicaci&oacute;n no es v&aacute;lido.");
 							 existeError++;}
						  //Inicio GAE
						  if(!ArchImp.existePuntoImporteIva() && "OK".equals(nuevoFormatoPag)){
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El importe IVA debe tener punto decimal.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>El importe IVA debe tener punto decimal.");
							  existeError++;
						  }else if(!ArchImp.validaFormatoImporteIva() && "OK".equals(nuevoFormatoPag)){
							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Solo se permiten 12 Enteros en el importe IVA.", EIGlobal.NivelLog.ERROR);
							  errores.append("<br><DD><LI>Solo se permiten 12 Enteros en el importe IVA.");
							  existeError++;
						  }
						  //Fin GAE

						  if(ArchImp.indicadorOperDolares && "OK".equals(nuevoFormatoPag) && !ArchImp.validaRFC()){
 							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El RFC no es valido.", EIGlobal.NivelLog.ERROR);
 							 errores.append("<br><DD><LI>El RFC es inv&aacute;lido.");
 							 existeError++;
 							 }



						  // Verificar que la forma de aplicaci?n se incluya en todos los registros o en ninguno
						  if(cont==1)
							 {
							    verificaIndicador=ArchImp.indicadorFormaAplica;
								EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): El indicador " + verificaIndicador +" en linea " + cont, EIGlobal.NivelLog.INFO);
							 }
						  if((ArchImp.indicadorFormaAplica!=verificaIndicador))
						    {
 							  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Debe especifiar la forma de aplicacion para todos.", EIGlobal.NivelLog.ERROR);
 							 errores.append("<br><DD><LI>Debe especificar la forma de aplicaci&oacute;n en todos los registros u omitirla.");
 							 existeError++;}

						  if(!ArchImp.verificaTipoPago() && ArchImp.indicadorOperDolares){
						      EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Debe especifiar el tipo de Pago", EIGlobal.NivelLog.ERROR);
	                             errores.append("<br><DD><LI>Debe especificar el tipo de pago para transferencias en Dolares.");
	                             existeError++; 
						  }

							//TODO BIT CU2061, genera tabla dep?sitos
							/*
							 * VSWF-HGG-I
							 */
						  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
							try{
								BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));

								BitaTransacBean bt = new BitaTransacBean();

								bt = (BitaTransacBean)bh.llenarBean(bt);
								bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_GENERA_TABLA_DEPOSITOS);
								if (contrato != null) {
									bt.setContrato(contrato.trim());
								}
								if (NombreArchInter != null) {
									if(NombreArchInter.lastIndexOf("\\") > -1) {
										bt.setNombreArchivo(NombreArchInter.substring(NombreArchInter.lastIndexOf("\\")));
									}
									else {
										bt.setNombreArchivo(NombreArchInter);
									}
								}
								if (ArchImp.cuentaCargo != null) {
									if(ArchImp.cuentaCargo.length() > 20){
										bt.setCctaOrig(ArchImp.cuentaCargo.substring(0,20));
									}else{
										bt.setCctaOrig(ArchImp.cuentaCargo.trim());
									}
								}
								if (ArchImp.cuentaAbono != null) {
									if(ArchImp.cuentaAbono.length() > 20){
										bt.setCctaDest(ArchImp.cuentaAbono.substring(0,20));
									}else{
										bt.setCctaDest(ArchImp.cuentaAbono.trim());
									}
								}
								if (ArchImp.banco != null) {
									bt.setBancoDest(ArchImp.banco.trim());
								}
								bt.setImporte(ArchImp.importe);
								if (ArchImp.referencia != null) {
									bt.setReferencia(
										Long.parseLong(("".equals(ArchImp.referencia.trim()))
												? "0"
												: ArchImp.referencia.trim()));
								}
								 /* VECTOR 06-2016: SPID */
								bt.setTipoMoneda(tipoDivisa);

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
							}
						  }
							/*
							 * VSWF-HGG-F
							 */
						  if(existeError==0)
						   {
							 if(contEsc)
							  {
								tramaEnviar="";
								strArc.delete(0, strArc.length());
								strArcRfc = " | ";
								strConceptoRfc="";

								//CSA
								DecimalFormat formateador = (DecimalFormat) DecimalFormat.getInstance();
								formateador.applyPattern("##0.##");

								if(ArchImp.nuevoFormato)
								 {
								   EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): NuevoFormato.", EIGlobal.NivelLog.DEBUG);
								   if("S".equals(ArchImp.comproFiscal) || ArchImp.indicadorOperDolares)
									{
									  strArcRfc=ArchImp.RFC+"|";
									  strArcRfc+=formateador.format(ArchImp.importeIVA);//new Double(ArchImp.importeIVA).toString();
									  strConceptoRfc="RFC "+ArchImp.RFC+" IVA "+formateador.format(ArchImp.importeIVA);
									}
								 }

								EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): RFC: "+strArcRfc, EIGlobal.NivelLog.INFO);

								String plaza = (ArchImp.plaza == null || ArchImp.plaza.equals("")) ? " " : ArchImp.plaza;
								String beneficiario = (ArchImp.beneficiario == null || ArchImp.beneficiario.equals("")) ? " " : ArchImp.beneficiario;
								String sucursal = ArchImp.sucursal;
								sucursal = (sucursal == null || "".equals(sucursal)) ? " " : sucursal;

								tramaEnviar = tramaEnviar.concat(IEnlace.medioEntrega1+"|");
								tramaEnviar = tramaEnviar.concat(usuario+"|");
								tramaEnviar = tramaEnviar.concat(DIBT+"|");
								tramaEnviar = tramaEnviar.concat(contrato+"|");
								tramaEnviar = tramaEnviar.concat(usuario+"|");
								tramaEnviar = tramaEnviar.concat(clavePerfil+"|");

								tramaEnviar = tramaEnviar.concat(ArchImp.cuentaCargo+"|");
								tramaEnviar = tramaEnviar.concat(tipo_relac_cta+"|");
								tramaEnviar = tramaEnviar.concat(ArchImp.titular+"|");
								tramaEnviar = tramaEnviar.concat(ArchImp.cuentaAbono+"|");
								tramaEnviar = tramaEnviar.concat(ArchImp.banco+"|");
								tramaEnviar = tramaEnviar.concat(beneficiario+"|");
								tramaEnviar = tramaEnviar.concat(sucursal+"|");
								tramaEnviar = tramaEnviar.concat(formateador.format(ArchImp.importe)+"|");//CSA
								tramaEnviar = tramaEnviar.concat(plaza+"|");
								tramaEnviar = tramaEnviar.concat(EnlaceGlobal.fechaHoy("ddthtmts")+"|");
								tramaEnviar = tramaEnviar.concat(ArchImp.concepto+strConceptoRfc+"|");
								tramaEnviar = tramaEnviar.concat((("".equals(ArchImp.referencia.trim()))?" ":ArchImp.referencia.trim())+"|");
								tramaEnviar = tramaEnviar.concat(" |");
								tramaEnviar = tramaEnviar.concat(ArchImp.getFormaAplica()+"|");
								tramaEnviar = tramaEnviar.concat(tipoDivisa + "|");
                                tramaEnviar = tramaEnviar.concat(ArchImp.RFC + "|");
                                tramaEnviar = tramaEnviar.concat(obtenerIPCliente(req) + "|");
                                tramaEnviar = tramaEnviar.concat(ArchImp.tipoPago + "|");

								strArc.append(ArchImp.cuentaCargo);
								strArc.append("|");
								strArc.append(ArchImp.titular);
								strArc.append("|");
								strArc.append(ArchImp.cuentaAbono);
								strArc.append("|");
								strArc.append(ArchImp.beneficiario);
								strArc.append("|");
								strArc.append(formateador.format(ArchImp.importe));//CSA
								strArc.append("|");
								strArc.append(ArchImp.concepto);//CSA
								strArc.append("|");
								strArc.append((("".equals(ArchImp.referencia.trim()))?" ":ArchImp.referencia.trim()));
								strArc.append("|");
								strArc.append(ArchImp.banco);
								strArc.append("|");
								strArc.append(ArchImp.plaza);
								strArc.append("|");
								strArc.append(ArchImp.sucursal);
								strArc.append("|");
								strArc.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
								strArc.append("|");
								strArc.append(strArcRfc);
								strArc.append("|");
								strArc.append(ArchImp.getFormaAplica());
								strArc.append("|");
								strArc.append(ArchImp.getDescAplica());
								strArc.append("|");

								/** Indra PYME Mayo Importar archivo programadas **/
								if(getFormParameter(req,"fecha_completaReg") != null) {
									strArc.append(getFormParameter(req,"fecha_completaReg"))
									;
								}
								/** Indra PYME Mayo Importar archivo programadas **/
								/** VECTOR 06-2016: SPID **/
								strArc.append("|").append(tipoDivisa).append("|");
								strArc.append(ArchImp.RFC);
								strArc.append("|");
								strArcProgramadas += strArc.toString();
								strArcProgramadas = strArcProgramadas.concat(ArchImp.tipoPago + "|" + "@");
								strArc.append(obtenerIPCliente(req)).append("|");
								strArc.append(ArchImp.tipoPago).append("|");//FORMA DE PAGO SPID Se debe actulizar valor cuando SPID acepte operaciones en Dolares por archivo
								strArc.append("@");
								crearListaTemporalFavPorARchivo(ArchImp,ses,contOk);
								EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Trama datos exportar: "+strArcProgramadas, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Trama para tabla de datos: "+strArc, EIGlobal.NivelLog.INFO);

								importeTotal+=ArchImp.importe;

								//Tabla de Archivo Importado.
								//FSW Indra ARS se comenta por solicitud de Isban, solicit� mostrar todos los registros importadosif(cont<32){
								  ArchivoI.strOriginal+=strArc;
								//}
								  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): "
								          + "\n1501 : tramaEnviar : <"+ tramaEnviar + ">", EIGlobal.NivelLog.INFO);
								if(!ArcSal.escribeLinea("\n"+tramaEnviar)){
								  EIGlobal.mensajePorTrace(MENS_ERROR+strArc, EIGlobal.NivelLog.INFO);
								}
							  }
							 contOk++;
						   }
						  else
						   {
							 if(arcErrores)
							  {
								strErr="\n<br><b>Linea: "+Integer.toString(cont)+".</b>"+errores.toString();
								EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Los errores son: >>>>" + strErr + "<<<<", EIGlobal.NivelLog.INFO);
								if(!ArcErr.escribeLinea(strErr)){
								  EIGlobal.mensajePorTrace(MENS_ERROR+strErr, EIGlobal.NivelLog.INFO); }
							  }
							 //strErr+="<br><b>Linea: "+Integer.toString(cont)+".</b>"+errores;
							 contEsc=false;
						   }
						 }
						else
						 {
							 //strErr="<br><b>Linea: "+cont+".</b> <font color=red>No cumple con el Formato.</font>";
							 if(arcErrores)
							  {
								strErr="<br><b>Linea: "+cont+".</b> <font color=red>No cumple con el Formato.</font>";
								if(!ArcErr.escribeLinea(strErr)){
								  EIGlobal.mensajePorTrace(MENS_ERROR+strErr, EIGlobal.NivelLog.INFO); }
							  }
						 }
					  }
					  
					  if(existeError > 0 || arcErrores && "USD".equals(tipoMoneda)){
						  req.setAttribute("valTransfArch", "ERR");
					  }

					  //***** everis Cerrando BufferedReader  06/05/2008  ..inicio
					  valCtas.closeTransaction();
					  strOut.close();
					  //****  everis Cerrando BufferedReader  06/05/2008  ..fin

				   }
				  else
				   {
					 strErr="<p><b> Problemas para crear archivo de transferencia.";
					 existeError=0;
					 contOk=-1;
				   }
				  ArcSal.cierraArchivo();

				  //*************************************************************
				  if(arcErrores)
				   {
					  String footer="";

					  footer="<br></td></tr>";
					  footer+="</table>";
					  footer+="<table border=0 align=center >";
					  footer+="<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a><A href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir'></a></td></tr>";
					  footer+="</table>";
					  footer+="</form>";
					  footer+="</body>\n</html>";

					  if(!ArcErr.escribeLinea(footer)){
						EIGlobal.mensajePorTrace(MENS_ERROR+strErr, EIGlobal.NivelLog.INFO); }
					  ArcErr.cierraArchivo();

					  ArchivoRemoto archR = new ArchivoRemoto();
					  if(!archR.copiaLocalARemoto("Errores.html","WEB")){
						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): No se pudo generar copia remota de errores", EIGlobal.NivelLog.ERROR);
					  }else{
						EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Si se pudo generar copia remota de errores", EIGlobal.NivelLog.DEBUG);}
				   }
				  //**************************************************************

				  //***** everis Cerrando RandomAccessFile 06/05/2008  ..inicio
				  archivoEnvioPago.close();
				  //****  everis Cerrando RandomAccessFile 06/05/2008  ..fin

				}catch(Exception e)
				 {EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
				   EIGlobal.mensajePorTrace("MDI_Interbancar1io - generaTablaDepositos(): B Excepcion, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				 }


				//***** everis Cerrando RandomAccessFile 06/05/2008  ..inicio
				finally{
					try{
			    		valCtas.closeTransaction();
			    	}catch (Exception e) {
			    		EIGlobal.mensajePorTrace("Error al cerrar la conexion a MQ"+e.getMessage(), EIGlobal.NivelLog.ERROR);
					}

                    if(null != archivoEnvioPago){
                        try{
                        	archivoEnvioPago.close();
                        }catch(Exception e){ EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);}
                        archivoEnvioPago = null;
                    }
                }

				//****  everis Cerrando RandomAccessFile 06/05/2008  ..fin


			}
		   else
			{
			  msgArchivo="cuadroDialogo('No existe el archivo o esta vacio. Por favor verifiquelo.', 4);";
			  importacion=false;
			}

		   if(contOk==cont)
		     strErr="<br><b>El archivo fue importado<font color=blue> exitosamente.</font></b><br>";
		   else
			 {
			   importacion=false;
			   req.setAttribute("ArchivoErr","archivoErrores();");
			 }
		   strErr="<p><b>Total de lineas:</b> "+Integer.toString(cont)+"<br><b>Lineas correctas:</b> "+Integer.toString(contOk)+"<br>"+strErr;
	     }

/*************************************** Generar Archivo de Exportacion *********/
       if(NoReg.totalRegistros>=1 || Reg.totalRegistros>=1)
        {
          String nombreArchivo=usuario+".doc";
          StringBuilder arcLinea = new StringBuilder();
          arcLinea.delete(0, arcLinea.length());

          EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
          EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);
          if(ArcSal.creaArchivo())
           {
        	 arcLinea.delete(0, arcLinea.length());
             int c[]={0,2,7,3,9,4,8,5,6};       // Indice del elemento deseado
             int b[]={16,20,5,40,4,15,5,50,7};  // Longitud maxima del campo para el elemento VECTOR 06-2016: SPID

             EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDeposito(): Exportando Cuentas no registradas.", EIGlobal.NivelLog.DEBUG);
             for(int i=0;i<NoReg.totalRegistros;i++)
              {
            	 arcLinea.delete(0, arcLinea.length());
                for(int j=0;j<9;j++){
                	arcLinea.append(ArcSal.formateaCampo(NoReg.camposTabla[i][c[j]],b[j]));
                }
                	/* VECTOR 06-2016: SPID Se agrega al archivo el tipo de divisa MXN o USD que se ha elegido en el Front */
                arcLinea.append(tipoDivisa);
                arcLinea.append("\n");
                EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDeposito(): "
                        + "Exportando Cuentas registradas." + arcLinea.toString(), EIGlobal.NivelLog.DEBUG);
            
                if(!ArcSal.escribeLinea(arcLinea.toString())) {
                  EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Algo salio mal escribiendo "+strArc, EIGlobal.NivelLog.ERROR);
                }
              }

             EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDeposito(): Exportando Cuentas registradas.", EIGlobal.NivelLog.DEBUG);
             for(int i=0;i<Reg.totalRegistros;i++)
              {
            	 arcLinea.delete(0, arcLinea.length()); //arcLinea="";
                for(int j=0;j<8;j++){
                	arcLinea.append(ArcSal.formateaCampo(Reg.camposTabla[i][c[j]],b[j]));
                }
                	/* VECTOR 06-2016: SPID Se agrega al archivo el tipo de divisa MXN o USD que se ha elegido en el Front */
                arcLinea.append(tipoDivisa);
                arcLinea.append("\n");
                
                EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDeposito(): "
                        + "Exportando Cuentas registradas." + arcLinea.toString(), EIGlobal.NivelLog.DEBUG);
                if(!ArcSal.escribeLinea(arcLinea.toString()))
                  EIGlobal.mensajePorTrace(MENS_ERROR+strArc, EIGlobal.NivelLog.ERROR);
              }
             ArcSal.cierraArchivo();

             // Boton de Exportacion de Archivo.
             //*************************************************************
             ArchivoRemoto archR = new ArchivoRemoto();
             if(archR.copiaLocalARemoto(nombreArchivo,"WEB")) {
            	//PYME 2015 INDRA
              ExportModel  em = new ExportModel("sepfileexp", '|', true)
             .addColumn(new Column(0, 15, "Cuenta de Cargo"))
             .addColumn(new Column(16, 35, "Cuenta de Abono/M�vil"))
             .addColumn(new Column(36, 40, "Banco"))
             .addColumn(new Column(41, 80, "Beneficiario"))
             .addColumn(new Column(81, 84, "Sucursal"))
             .addColumn(new Column(85, 99, "Importe", "$####################0.00"))
             .addColumn(new Column(100, 104, "Plaza"))
             .addColumn(new Column(105, 150, "Concepto"))
             /* VECTOR 06-2016: SPID */
             .addColumn(new Column(155, 159, "Divisa"));

             ses.setAttribute("ExportModel", em);
             ses.setAttribute("af", IEnlace.DOWNLOAD_PATH+nombreArchivo);

              strBotones+="\n<a href='javascript:exportarArchivoPlano();'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
             //PYME 2015 INDRA
             }
             else
               EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): No se pudo generar la copia remota", EIGlobal.NivelLog.ERROR);
             //**************************************************************
           }
          else
           {
             EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.ERROR);
           }
        }
/*************************************** Fin de Exportacion de Archivo *********/
		//TODO BIT CU2061, genera tabla dep?sitos
	  	/*
	   	 * VSWF-HGG-I
	   	 */
       if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try{
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));

			BitaTransacBean bt = new BitaTransacBean();

			bt = (BitaTransacBean) bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_GENERA_ARCHIVO_EXPORTACION);
			if (contrato != null) {
				bt.setContrato(contrato.trim());
			}
			if (usuario != null) {
				bt.setNombreArchivo(usuario + ".doc"); //archivo de exportaci?n
			}
			bt.setIdErr("ExportaA");

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
       }
		/*
		 * VSWF-HGG-F
		 */

/*********************************************************************************/
       // Boton Estatus de archivo y cotizar/Enviar (solo si hay registros que enviar) ...
	   //Boton de estatus...
	   String status="";
	   if(radio==2 && cont>0)
	     status+="<td align=center><A href='javascript:archivoErrores();' border = 0><img src='/gifs/EnlaceMig/gbo25248.gif' border=0 alt='Informaci&oacute;n del Archivo Importado'></a></td>";
	   //Boton de estatus...
	   if(NoReg.totalRegistros>=1 || Reg.totalRegistros>=1 || importacion)
		  strBotones+="<A href='javascript:ValidaDuplicados();' border = 0><img src='/gifs/EnlaceMig/gbo25222.gif' border=0 alt='Validar JPPM'></a>";
/************************************************** Salida de datos en pantalla */
	   ArchivoI.iniciaObjeto(ArchivoI.strOriginal);
		if(radio==0 || Reg.totalRegistros>=1)
		 {
		   EnlaceGlobal.formateaImporte(Reg, 4);
		   EnlaceGlobal.formateaImporte(Reg,12);
		   strTabla+=generaTabla("Cuentas Registradas",0,Reg);
		   if(NoReg.totalRegistros>=1){
			 strTabla+="<br>";}
		 }

		if(radio==1 || NoReg.totalRegistros>=1)
		 {
		   EnlaceGlobal.formateaImporte(NoReg, 4);
		   EnlaceGlobal.formateaImporte(NoReg,12);
		   strTabla+=generaTabla("Cuentas No Registradas",1,NoReg);
		 }

		if(ArchivoI.totalRegistros<=Global.MAX_REGISTROS)
		 {
		   EnlaceGlobal.formateaImporte(ArchivoI, 4);
		   if(ArchivoI.totalCampos>=15) // RFC + IVA
			{
		      EnlaceGlobal.formateaImporte(ArchivoI,12);
		      strTablaArchivo+=generaTabla("Importadas de Archivo",3,ArchivoI);
			}
		   else
			strTablaArchivo+=generaTabla("Importadas de Archivo",2,ArchivoI);
		 }

		req.setAttribute("ArchivoFallo",msgArchivo);
        req.setAttribute("Fecha",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
        req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("NumeroRegistros",Integer.toString(Global.MAX_REGISTROS));

		/** Indra PYME Mayo Importar archivo programadas **/

		req.setAttribute("strArc", strArcProgramadas);

		req.setAttribute("fecha_completaReg",
					getFormParameter(req,"fecha_completaReg") != null
					? getFormParameter(req,"fecha_completaReg") : EnlaceGlobal.fechaHoy("dd/mm/aaaa"));


		/** Indra PYME Mayo Importar archivo programadas **/

		req.setAttribute("Botones",strBotones);
        req.setAttribute("Modulo","1");
		//Envio del nombre de Archivo IM199738
		req.setAttribute("ArchivoNomInter",NombreArchInter);

        req.setAttribute("ModuloTabla","2");
        //PYME 2015 INDRA Se agrega calendario para operaciones programadas en Registradas
        req.setAttribute("fechaApliacionRegistradas", generaCampoFchApRegistradas(contrato));
        req.setAttribute("cuentasNoRegistradas",cuentasNoRegistradas(tipoCuenta, req, contrato.trim()));
        req.setAttribute("TransReg",Reg.strOriginal);
        req.setAttribute("TransNoReg",NoReg.strOriginal);

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

		if(NoReg.totalRegistros>=1 || Reg.totalRegistros>=1 || !importacion){
	      req.setAttribute("TransArch","");
		}else{
          req.setAttribute("TransArch","Archivo de Exportacion");}

	    req.setAttribute("Lineas",Integer.toString(cont));
		req.setAttribute("Correctas",Integer.toString(contOk));
        req.setAttribute("archivoEstatus",strErr);

        req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias &gt; Interbancarias","s25440h",req));

		req.setAttribute("strImportar",importarArchivo(facImpArc, (String)req.getAttribute("valTransfArch")));
		req.setAttribute("Registradas",getFormParameter(req,"Registradas"));
		/* VECTOR 06-2016: SPID */
		req.setAttribute("divisaImportArchivo", tipoDivisa);

		if(NoReg.totalRegistros>=1 || Reg.totalRegistros>=1 || !importacion)
		 {//Transferencias en Linea
		   if(NoReg.totalRegistros>=1 || Reg.totalRegistros>=1)
			 {
			   req.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Reg.totalRegistros+NoReg.totalRegistros));
			   req.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)) );
			   req.setAttribute("Mensaje","Elimine la marca de las operaciones que no desea realizar<br>");
			 }
		   req.setAttribute("Tabla",strTabla);
		   req.setAttribute("Status",status); //############Boton de estatus...
		   String uso_cta_cheque = Global.USO_CTA_CHEQUE;
		   req.setAttribute("USO_CTA_CHEQUE1", uso_cta_cheque);

           evalTemplate("/jsp/MDI_Interbancario.jsp", req, res);
		 }
	    else
		 {//Transferencias Importadas de Archivo
		   if(cont>Global.MAX_REGISTROS)
		   {
			  String mensajeHTML="\n<br><br><i>El archivo fue importado <font color=green>satisfactoriamente.</font></i><br>";
					 mensajeHTML+="\nAhora ya puede <font color=blue>continuar</font> con el proceso de validaci&oacute;n de datos.<br>";
			  req.setAttribute("Tabla",despliegaMensaje(mensajeHTML));
           }
		   else
			 req.setAttribute("Tabla",strTablaArchivo);
		   
		   EIGlobal.mensajePorTrace(" Archivo Tabla : [<" + strTablaArchivo + ">]", EIGlobal.NivelLog.INFO);
		   req.setAttribute("importeTotal",FormatoMoneda(Double.toString(importeTotal)));

		   evalTemplate("/jsp/MDI_InterbancarioArchivo.jsp", req, res);
		 }
        } catch(Exception e)
		 {EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);}
   }

/**
 * Se crea una alta temporal de favoritos correspondiente a cada uno de los registros del archivo.
 * @param archImp Datos del registro
 * @param sesion Objeto de la sesion
 * @param numeroReg Numero de registro a dar de alta
 */
private void crearListaTemporalFavPorARchivo(MDI_Importar archImp,HttpSession sesion,int numeroReg) {
	EIGlobal.mensajePorTrace("?????????????da de alta favorito temp:"+numeroReg, EIGlobal.NivelLog.INFO);

	List<FavoritosEnlaceBean> favoritosTmp = (List<FavoritosEnlaceBean>) sesion.getAttribute(FavoritosConstantes.ID_LISTA_FAV_TMP);
	BaseResource bR=(BaseResource) sesion.getAttribute("session");

	if ( (favoritosTmp==null) || ( (numeroReg==0) && (!favoritosTmp.isEmpty()) ) ) {
		favoritosTmp=new ArrayList<FavoritosEnlaceBean>();
		EIGlobal.mensajePorTrace("Se crea una nueva lista", EIGlobal.NivelLog.INFO);
	}

	 FavoritosEnlaceBean favorito = 	new FavoritosEnlaceBean();

		favorito.setContrato(bR.getContractNumber());
		favorito.setCodCliente(bR.getUserID8());
		favorito.setCuentaCargo(archImp.cuentaCargo);
		favorito.setDescCuentaCargo(archImp.titular);
		favorito.setCuentaAbono(archImp.cuentaAbono);
		favorito.setDescCuentaAbono(archImp.beneficiario);
		favorito.setConceptoOperacion(archImp.concepto);
		favorito.setIdModulo(DIBT);
		favorito.setImporte(new Double(archImp.importe));
		favorito.setTipoTransaccion("H".equals("H")?FavoritosConstantes.ID_SPEI_TI:FavoritosConstantes.ID_TEF_TI);


		favoritosTmp.add(favorito);

		sesion.setAttribute(FavoritosConstantes.ID_LISTA_FAV_TMP, favoritosTmp);

}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
  public int iniciaDepositos(HttpServletRequest req, HttpServletResponse res, String tipoCuenta, String facImpArc, String contrato)
			throws ServletException, IOException {

	    /************* Modificacion para la sesion ***************/
		HttpSession ses = req.getSession();

		BaseResource session = (BaseResource) ses.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		req.setAttribute("ArchivoFallo","");
		req.setAttribute("strCheck","");
		req.setAttribute("Lineas","");
		req.setAttribute("importeTotal","");
		req.setAttribute("ModuloTabla","0");
		req.setAttribute("TransReg","");
		req.setAttribute("TransNoReg","");
		req.setAttribute("TransArch","");
		req.setAttribute("archivoEstatus","");
		req.setAttribute("comprobante","");
		req.setAttribute("ArchivoErr","");

        req.setAttribute("Fecha",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
        req.setAttribute("Modulo","1");
        //PYME 2015 INDRA se agrega campo fecha de aplicacion para Ctas Registradas
        req.setAttribute("fechaApliacionRegistradas", generaCampoFchApRegistradas(contrato));
        req.setAttribute("cuentasNoRegistradas",cuentasNoRegistradas(tipoCuenta,req, contrato));
        req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("NumeroRegistros",Integer.toString(Global.MAX_REGISTROS));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

        req.setAttribute("TransReg","");
        req.setAttribute("TransNoReg","");
        req.setAttribute("TransArch","");

		req.setAttribute("strImportar",importarArchivo(facImpArc, (String)req.getAttribute("valTransfArch")));

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias &gt; Interbancarias","s25440h",req));
		//TODO BIT CU2061, Inicio de flujo de Transferencias Interbancarias
		/*
		 * VSWF-HGG-I
		 */
		if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try {
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
			if(req.getParameter(BitaConstants.FLUJO) != null){
				bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			}else{
				bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt); /* EWEB_TRAN_BITACORA */
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		}
		/*
		 * VSWF-HGG-F
		 */

		String uso_cta_cheque = Global.USO_CTA_CHEQUE;
   	    req.setAttribute("USO_CTA_CHEQUE1", uso_cta_cheque);

        evalTemplate("/jsp/MDI_Interbancario.jsp", req, res);

     return 1;
   }

/*************************************************************************************/
/*********************************************************** LLena Lista Plazas      */
/*************************************************************************************/
   void llenaListaPlazas(Vector listaPlazas){
      String totales="";
	  String dato="";

      int Existe=0;
	  int totalPlazas=0;

	  Connection conn = null;
	  ResultSet contResult = null;
	  ResultSet plazaResult = null;

	  try{
	      conn = createiASConn(Global.DATASOURCE_ORACLE);
	      if(conn == null)
	        EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
	      totales="Select count(*) from comu_pla_banxico ";
		  PreparedStatement contQuery = conn.prepareStatement(totales);
	      if(contQuery == null)
		     EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  contResult = contQuery.executeQuery();
	      if(contResult.next())
	         EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  totalPlazas=Integer.parseInt(contResult.getString(1));
		  EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): Total de plazas: "+Integer.toString(totalPlazas), EIGlobal.NivelLog.INFO);
	      String sqlplaza="Select plaza_banxico from comu_pla_banxico order by plaza_banxico";
	      PreparedStatement plazaQuery = conn.prepareStatement( sqlplaza );
	      if(plazaQuery==null)
		     EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  plazaResult = plazaQuery.executeQuery();
	      if( !plazaResult.next())
	         EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaPlazas(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  for(int i=0;i<totalPlazas;i++)
	       {
	         dato=plazaResult.getString(1);
			 listaPlazas.addElement(dato);
	         plazaResult.next();
	       }
		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		finally
		  {
			try
			 {
			   if( plazaResult != null ){
				   plazaResult.close();
			   }
			   if( contResult != null ){
				   contResult.close();
			   }
			   if( conn != null ){
				   conn.close();
			   }
			 }catch(SQLException e) {EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);}
		  }
    }

/*************************************************************************************/
/*********************************************************** LLena Lista Bancos      */
/*************************************************************************************/
   void llenaListaBancos(List<String> listaBancos)
    {
      String dato="";
	  String totales="";

      int Existe=0;
	  int totalBancos=0;

	  Connection conn = null;
	  ResultSet contResult = null;
	  ResultSet bancoResult = null;

	  try{
	      conn = createiASConn(Global.DATASOURCE_ORACLE);

	      if(conn == null)
	        EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): No se puedo crear conexion.", EIGlobal.NivelLog.ERROR);

	      totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";
	      PreparedStatement contQuery = conn.prepareStatement(totales);
	      if(contQuery == null)
		     EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  contResult = contQuery.executeQuery();
	      if(!contResult.next())
	         EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);

		  totalBancos=Integer.parseInt(contResult.getString(1));
		  String sqlbanco="Select cve_interme from comu_interme_fin where num_cecoban<>0 order by cve_interme ";
		  PreparedStatement bancoQuery = conn.prepareStatement(sqlbanco);
	      if(bancoQuery==null)
		     EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  bancoResult = bancoQuery.executeQuery();
	      if(!bancoResult.next())
	         EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  for(int i=0;i<totalBancos;i++)
	       {
			 dato=bancoResult.getString(1);
			 EIGlobal.mensajePorTrace("MDI_Interbancario - llenaListaBancos(): BANCO: [" + dato + "]", EIGlobal.NivelLog.ERROR);
	         listaBancos.add(dato.trim());
	         bancoResult.next();
	       }
		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		finally
		  {
			try
			 {
			   if( bancoResult != null ){
				   bancoResult.close();
			   }
			   if( contResult != null ){
				   contResult.close();
			   }
			   if( conn != null ){
				   conn.close();
			   }
			 }catch(SQLException e) {EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);}
		  }
    }

/**CCB 12Feb2004
 *Trae la clave del banco por nombre corto
 * @param nombreCorto String
 * @return String
 */
    String traeClaveBanco(String nombreCorto)
    {
      String dato="";
      String datotmp = "";
	  String totales="";

      int Existe=0;
	  int totalBancos=0;

	  Connection conn = null;
	  ResultSet cveBancoResult = null;

	  try{
	      conn = createiASConn(Global.DATASOURCE_ORACLE);

	      if(conn == null)
	        EIGlobal.mensajePorTrace("MDI_Interbancario - traeClaveBanco: No se puedo crear conexion.", EIGlobal.NivelLog.ERROR);

		  String sqlcveBanco="Select NUM_CECOBAN from COMU_INTERME_FIN where TRIM(cve_interme) = '" + nombreCorto.trim() +"'";
		  EIGlobal.mensajePorTrace("MDI_Interbancario - traeClaveBanco: CCBQuery = " + sqlcveBanco, EIGlobal.NivelLog.INFO);
		  PreparedStatement cveBancoQuery = conn.prepareStatement(sqlcveBanco);
	      if(cveBancoQuery==null)
		     EIGlobal.mensajePorTrace("MDI_Interbancario - traeClaveBanco: No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
		  cveBancoResult = cveBancoQuery.executeQuery();
	      if(!cveBancoResult.next())
	         EIGlobal.mensajePorTrace("MDI_Interbancario - traeClaveBanco: No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
		  else{
		 	  dato=cveBancoResult.getString(1);
		 	  if(dato==null)
		 	  	dato="";
		 	  else if(dato.trim().length()<3){
		 	  	for(int i=0; i<3-dato.trim().length();i++){
		 	  		datotmp = datotmp.concat("0");
		 	  	}
		 	  	datotmp = datotmp.concat(dato);
		 	  	dato = datotmp;
		 	  }
		 	}
		}catch( SQLException sqle ){
			EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		finally
		  {
			try

			 {
			   if( cveBancoResult != null ){
				   cveBancoResult.close();
			   }
			   if( conn != null ){
				   conn.close();
			   }
			 }catch(SQLException e) {
				 EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			 }






		  }
	return dato;
    }



    /**
     * regresa cadena para la opcion de importacion desde archivo si tiene facultad
     * @param facImpArc     el parametro facImpArc
     * @param valTransfArch el parametro valTransfArch
     * @return cadena para opcion de importacion desde archivo
     */
   String importarArchivo(String facImpArc, String valTransfArch)
	{
	   StringBuilder strImp = new StringBuilder();
	   
	   strImp.append("<!-- No Tiene Facultad. //-->");

	   if("true".equals(facImpArc.trim()))
		{
		  strImp.append("\n  <tr>");
		  strImp.append("\n    <td class='tittabdat' colspan=4><INPUT TYPE='radio' value=2 NAME='Registradas' onClick='VerificaFac(2);'>&nbsp;<b>Importar</b></td>");
		  strImp.append("\n  </tr>");

		  strImp.append("\n  <tr>");
		  strImp.append("\n   <td class='textabdatcla' colspan=4>");
		  strImp.append("\n    <table border=0 class='textabdatcla' cellspacing=3 cellpadding=2 width='100%'>");

		  strImp.append("\n     <tr>");
		  strImp.append("\n      <td width='100%' class='tabmovtex' align=center>Importar transferencias desde archivo &nbsp; <INPUT TYPE='file' NAME='Archivo' value=' Archivo ' onClick='javascript:Agregar1();'></td>");
		  strImp.append("\n     </tr>");

		  strImp.append("\n     <tr>");
		   
		  if("ERR".equalsIgnoreCase(valTransfArch) ){
			  strImp.append("\n      <td width='100%' class='tabmovtex' align=center>Archivo con Formato RFC? &nbsp; <input type=radio name=Formato id='formatoNO' value='NO' disabled='true'> No &nbsp; <input type=radio name=Formato id='formatoSI' value='OK' checked> Si</td>");  
		  }
		  else{
			  strImp.append("\n      <td width='100%' class='tabmovtex' align=center>Archivo con Formato RFC? &nbsp; <input type=radio name=Formato id='formatoNO' value='NO' checked> No &nbsp; <input type=radio name=Formato id='formatoSI' value='OK'> Si</td>");
			  
		  }
		  
		  
		  strImp.append("\n     </tr>");

		  strImp.append("\n    </table>");
		  strImp.append("\n   </td>");
		  strImp.append("\n  </tr>");
		}
	   return strImp.toString();
	}

/*************************************************************************************/
/****************** Despliega Mensaje para mas de 30 transferencias desde archivo    */
/*************************************************************************************/
  String despliegaMensaje(String mensajeHTML)
	{
	  String msg="";

	  msg+="\n  <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=400 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n   <td align=center>";
	  msg+="\n     <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td colspan=3> </td>";
	  msg+="\n	   </tr>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td width=21 background='/gifs/EnlaceMig/gfo25030.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n		 <td width=428 height=100 valign=top align=center>";
	  msg+="\n		   <table width=380 border=0 cellspacing=2 cellpadding=3 background='/gifs/EnlaceMig/gau25010.gif'>";
	  msg+="\n			 <tr>";
	  msg+="\n			   <td class='tittabcom' align=center>";
	  msg+=                mensajeHTML;
	  msg+="\n			   </td>";
	  msg+="\n			 </tr>";
	  msg+="\n		   </table>";
	  msg+="\n		 </td>";
	  msg+="\n         <td width=21 background='/gifs/EnlaceMig/gfo25040.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n	   </tr>";
	  msg+="\n	 </table>";
	  msg+="\n	</td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  return msg;
	}


/*************************************************************************************/
/********************** genera Tabla con icono de informacion sobre transferencia    */
/*************************************************************************************/
   String generaTabla(String titulo, int ind, EI_Tipo Trans)
	{
     /* Datos de la transferencia...
     0 CuentaCargo+"|";
 	 1 titular+"|";
	 2 cuentaAbono+"|";
	 3 cbeneficiario+"|";
	 4 importe+"|";
	 5 concepto+"|";
	 6 referenciaInterbancaria+"|";
 	 7 banco+"|";
	 8 plaza+"|";
	 9 sucursal+"|";
	10 fechaHoy("dd/mm/aaaa")+"|";
	11 RFC+"|";  // Opcional
	12 IVA+"|";  // Opcional
	13 FormaAplicacion+"|";
	14 Descripcion FormaAplicacion+"@";
	*/
		/*
		 * VECTOR 06-2016 SPID: Se modifican los arreglos titulos, datos y
		 * aling. Para titulos se agrega "Divisa", en datos en la posicion 10 se
		 * asigna el valor 16, en align para la posicion 10 se asigna el valor 1
		 */
		String[] titulos = { "",
	                      "Cuenta de Cargo",
		                  "Titular",
						  "Cuenta de Abono/M&oacute;vil",
						  "Beneficiario",
		                  "Banco",
						  "Sucursal",
		                  "Plaza",
						  "Importe",
						  "Divisa",
		                  "Concepto",
		                  "Referencia Interbancaria",
		                  "RFC Beneficiario",
		                  "Importe Iva",
		                  FORMA_APLICACION,FavoritosConstantes.TITULO_FAVORITOS};

	   int datos[]={14,0,0,1,2,3,7,9,8,4,16,5,6,11,12,14,14};
	   int values[]={2,0,1,2};
	   int align[]={0,0,0,0,1,1,1,2,1,1,1,0,2,1,0};

	   EIGlobal.mensajePorTrace("MDI_Interbancario - generatabla(): Genrando Tabla de Transferencias.", EIGlobal.NivelLog.DEBUG);

	   titulos[0]=titulo;

	   if(ind==0 || ind==1)
 	     datos[1]=2;

	   // Archivo, pero formato viejo...
	   if(ind==2)
		{
		  datos[0]=11;
		  datos[12]=12;
		  titulos[11]=FORMA_APLICACION;
		}

	   // Para duplicidad de archivos
	   if(ind==4)
		  datos[14]=14;

	   return Trans.generaTabla(titulos,datos,values,align);
	}

   /**
    * Imprime tabla duplicados.
    * @since 23/04/2015
    * @author FSW-Indra
    * @param titulo Titulo tabla
    * @param ind indice
    * @param Trans tipo transferencia
    * @return HTML a imprimir
    */
   private String generaTablaDuplicados(String titulo, int ind, EI_Tipo Trans)
	{

    /* Datos de la transferencia...
    0 CuentaCargo+"|";
	 1 titular+"|";
	 2 cuentaAbono+"|";
	 3 cbeneficiario+"|";
	 4 importe+"|";
	 5 concepto+"|";
	 6 referenciaInterbancaria+"|";
	 7 banco+"|";
	 8 plaza+"|";
	 9 sucursal+"|";
	10 fechaHoy("dd/mm/aaaa")+"|";
	11 RFC+"|";  // Opcional
	12 IVA+"|";  // Opcional
	13 FormaAplicacion+"|";
	14 Descripcion FormaAplicacion+"@";
	*/

	   String[] titulos={ "",
	                      "Cuenta de Cargo",
		                  "Titular",
						  "Cuenta de Abono/M&oacute;vil",
						  "Beneficiario",
		                  "Banco",
						  "Sucursal",
		                  "Plaza",
						  "Importe",
		                  "Concepto",
		                  "Referencia Interbancaria",
		                  "RFC Beneficiario",
		                  "Importe Iva",
		                  FORMA_APLICACION};

	   int datos[]={13,0,0,1,2,3,7,9,8,4,5,6,11,12,14,13};
	   int values[]={2,0,1,2};
	   int align[]={0,0,0,0,1,1,1,2,1,1,0,2,1,0};

	   EIGlobal.mensajePorTrace("MDI_Interbancario - generatabla(): Genrando Tabla de Transferencias.", EIGlobal.NivelLog.DEBUG);

	   titulos[0]=titulo;
	   if(ind==0 || ind==1)
	     datos[1]=2;

	   // Archivo, pero formato viejo...
	   if(ind==2)
		{
		  datos[0]=11;
		  datos[12]=12;
		  titulos[11]=FORMA_APLICACION;
		}

	   // Para duplicidad de archivos
	   if(ind==4)
		  datos[14]=14;

	   return Trans.generaTabla(titulos,datos,values,align);
	}


/*************************************************************************************/
/********************************************************** Verifica cuenta Clabe    */
/*************************************************************************************/
  /**
   * Revisa el valor de la cuenta clabe
   * @param banco numero de banco
   * @param cuenta numero de cuenta
   * @param sucursal numero de sucursal
   * @return String valor verificado
   */
  public String verificaCuentaClaBE(String banco, String  cuenta, int sucursal)
	{
      String cveBanco=traeClaveBanco(banco);
	  String cveB=cuenta.substring(0, 3);
	  String cveS=cuenta.substring(3,6);
	  String cveSucursal=new Integer(sucursal).toString();

	  cveSucursal=(cveSucursal.length()<3)?(cveSucursal="000".substring(0,3-cveSucursal.length())+cveSucursal):cveSucursal;

	  EIGlobal.mensajePorTrace("MDI_Interbancario - verificaCuentaClaBE(): cveBanco " + cveBanco, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("MDI_Interbancario - verificaCuentaClaBE(): cveB " + cveB, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("MDI_Interbancario - verificaCuentaClaBE(): cveSucursal " + cveSucursal, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("MDI_Interbancario - verificaCuentaClaBE(): cveS " + cveS, EIGlobal.NivelLog.INFO);

	  if( !"".equals(cveBanco) && !cveBanco.equals(cveB) )
		 return "La Cuenta/CLABE no coincide con el banco especificado";

	  /* No se requiere la validacion
	  if(!cveSucursal.equals(cveS))
		  return "La Cuenta/CLABE no coincide con la sucursal indicada";
	  */

	  if(! validaDigitoVerificador(cuenta) )
		return "El d&iacute;gito verificador no corresponde con la Cuenta/CLABE indicada";

	  return "OK";
	}

	/**
	 * @param cuenta String
	 * @return boolean
	 */
 private boolean validaDigitoVerificador(String cuenta)
	{
	    String pesos="371371371371371371371371371";
		String cuenta2=cuenta.substring(0,17);
		int DC=0;

		for(int i=0;i<cuenta2.length();i++)
	        DC += (Integer.parseInt(cuenta2.substring(i,i+1))) * (Integer.parseInt(pesos.substring(i,i+1))) % 10;
		DC = 10 - (DC % 10);
		DC = (DC >= 10) ? 0 : DC;
		cuenta2+=DC;
		if(cuenta2.trim().equals(cuenta.trim()))
			return true;
	   return false;
	}


  public void creaArchivoTib(HttpServletRequest req, HttpServletResponse res, EI_Tipo Reg, EI_Tipo NoReg)
			throws ServletException, IOException
	{
		  HttpSession ses = req.getSession();
		  BaseResource session = (BaseResource) ses.getAttribute("session");

		  String nombreArchivo= session.getUserID8()+".tib";
		  String arcLinea="";
		  String strCheck="";
		  String cadaux="";

		  int indice;

		  EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
		  EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);


		// OPC:PASSMARK 16/12/2006 ******************* BEGIN
		String pmuser      = session.getUserID8();
		String devicePrint = (String)req.getParameter ("pmdevice")==null?"":(String)req.getParameter ("pmdevice");
		String FSOanterior = (String)req.getParameter ("fsoantes")==null?"":(String)req.getParameter ("fsoantes");
		String ipuser      = (String)req.getRemoteAddr();

		if("1".equals(Global.ACTIVAR_PASSMARK))
		{
			EIGlobal.mensajePorTrace("creaArchivoTib: "+ Global.ACTIVAR_PASSMARK , EIGlobal.NivelLog.DEBUG);
		}
		else
		{
			FSOanterior = null;
		}

		// OPC:PASSMARK 16/12/2006 ******************* END


		  if(ArcSal.creaArchivo())
		  {
			 strCheck+=(String) req.getParameter("strCheck");
			 EIGlobal.mensajePorTrace("strCheck contiene: "+ strCheck , EIGlobal.NivelLog.INFO);

			 int i = 0;
			 //Obtiene los datos de los favoritos seleccionados
			 String favSel = "";
			 String aux = "";

			 for(i=1;i<=strCheck.length();i++){
				 if((i%2)==0){
					 favSel+=strCheck.charAt(i-1);
				 }else{
					 aux+=strCheck.charAt(i-1);
				 }
			 }
			 EIGlobal.mensajePorTrace("La relacion de fav seleccionados: "+favSel+" --- opereacionSel: "+aux, EIGlobal.NivelLog.INFO);
			 strCheck=aux;
			 //----------------------------
			 req.getSession().setAttribute("strCheck", strCheck);


			 arcLinea="";
			 int c[]={0,2,7,3,9,4,8,5,6};       // Indice del elemento deseado
			 int b[]={16,20,5,40,4,15,5,40,7};  // Longitud maxima del campo para el elemento

			 EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): Exportando Cuentas no registradas.", EIGlobal.NivelLog.DEBUG);
			 //Se agrega contador para favoritos, dar de alta intercalados PYME 2015
			 int contFav = 0;
			 for(i=0;i<NoReg.totalRegistros;i++)
			 {

				if(strCheck.charAt(i)!='0')
				{
					arcLinea="";
					cadaux="";
					indice=-1;
                    aux="";
					for(int j=0;j<9;j++)
					{
						// IM199738 Generacion de variable importe sin punto decimal para archivo tib.
						if(j == 5)
						 {
                           EIGlobal.mensajePorTrace("MDI_Interbancario importe orig (Noreg): " + NoReg.camposTabla[i][c[j]], EIGlobal.NivelLog.INFO);

						// OPC:PASSMARK 16/12/2006 ******************* BEGIN
						if("1".equals(Global.ACTIVAR_PASSMARK))
						{
								String FSOnuevo = CallPassmark(pmuser,devicePrint,ipuser,FSOanterior,NoReg.camposTabla[i][c[j]]);
								FSOanterior=FSOnuevo;
						}
						// OPC:PASSMARK 16/12/2006 ******************* END

						   indice =  NoReg.camposTabla[i][c[j]].indexOf('.');
						   if (indice != -1)
							{
							  cadaux = NoReg.camposTabla[i][c[j]].substring(0,indice)+ NoReg.camposTabla[i][c[j]].substring(indice+1);
							  EIGlobal.mensajePorTrace("MDI_Interbancario importe p/arch (Noreg): " + cadaux, EIGlobal.NivelLog.INFO);
							  arcLinea+=ArcSal.formateaCampo(cadaux,b[j]);
							}
						 }
						else if(j == 8)
						 {
							cadaux = "                                                                                          "; //90 espacios
							arcLinea+=cadaux;
							arcLinea+=ArcSal.formateaCampo(NoReg.camposTabla[i][c[j]],b[j]);
						 }
						else
							arcLinea+=ArcSal.formateaCampo(NoReg.camposTabla[i][c[j]],b[j]);
					}
					//Favoritos-------------------
					//Se agrega contador para favoritos, dar de alta intercalados PYME 2015
					darDeAltaFavoritoTransInterb(NoReg.camposTabla[i],contFav,favSel.charAt(i),ses);
					if( favSel.charAt(i) == '1' ){
						contFav++;
					}
					arcLinea+="\n";
					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): Algo salio mal escribiendo ", EIGlobal.NivelLog.ERROR);
				}
			}

			EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): Exportando Cuentas registradas.", EIGlobal.NivelLog.DEBUG);
			//Se agrega contador para favoritos, dar de alta intercalados PYME 2015
			contFav = 0;
			for(i=0;i<Reg.totalRegistros;i++)
			{
				if(strCheck.charAt(i)!='0')
				{
					arcLinea="";
					cadaux="";
					indice=-1;

					for(int j=0;j<9;j++)
					{
						// IM199738 Generacion de variable importe sin punto decimal para archivo tib.
						if(j == 5)
						 {
                          EIGlobal.mensajePorTrace("MDI_Interbancario importe orig(Reg): " + Reg.camposTabla[i][c[j]], EIGlobal.NivelLog.INFO);

							// OPC:PASSMARK 16/12/2006 ******************* BEGIN
							if("1".equals(Global.ACTIVAR_PASSMARK))
							{
									String FSOnuevo = CallPassmark(pmuser,devicePrint,ipuser,FSOanterior,Reg.camposTabla[i][c[j]]);
									FSOanterior=FSOnuevo;
							}
							// OPC:PASSMARK 16/12/2006 ******************* END

						  indice =  Reg.camposTabla[i][c[j]].indexOf('.');
						  if (indice != -1)
							{
							  cadaux = Reg.camposTabla[i][c[j]].substring(0,indice) + Reg.camposTabla[i][c[j]].substring(indice+1);
							  EIGlobal.mensajePorTrace("MDI_Interbancario importe p/arch(Reg): " + cadaux, EIGlobal.NivelLog.INFO);
							  try {
								  arcLinea+=ArcSal.formateaCampo(cadaux,b[j]);
							  } catch(Exception e) {
								  EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
								  EIGlobal.mensajePorTrace("Error en formateaCampo, C parametros: <" + cadaux + "> <" + b[j] + ">", EIGlobal.NivelLog.ERROR);
							  }
							}
						 }
						else if(j == 8)
						 {
							cadaux = "                                                                                          "; //90 espacios
							arcLinea+=cadaux;
							arcLinea+=ArcSal.formateaCampo(Reg.camposTabla[i][c[j]],b[j]);
						 }
						else {
						  arcLinea+=ArcSal.formateaCampo(Reg.camposTabla[i][c[j]],b[j]);
					    }
				    }
					//Favoritos
					//Se agrega contador para favoritos, dar de alta intercalados PYME 2015
					darDeAltaFavoritoTransInterb(Reg.camposTabla[i],contFav,favSel.charAt(i),ses);
					if( favSel.charAt(i) == '1' ){
						contFav++;
					}
					arcLinea+="\n";
					EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): L?nea para archivo: <" + arcLinea + "> longitud " + arcLinea.length(), EIGlobal.NivelLog.INFO);
					if(!ArcSal.escribeLinea(arcLinea))
						EIGlobal.mensajePorTrace("MDI_Interbancario - creaArchivoTib(): Algo salio mal escribiendo: ", EIGlobal.NivelLog.ERROR);
				}
				EIGlobal.mensajePorTrace("MDI_Interbancario - ars3(): Exportando Cuentas registradas." + Reg.totalRegistros, EIGlobal.NivelLog.DEBUG);
			}
			ArcSal.cierraArchivo();
			if(FSOanterior!=null) // OPC:PASSMARK
				req.setAttribute("_fsonuevoMID",FSOanterior); // OPC:PASSMARK
			validaDuplicados(req, res);
		  }
	}


  /**
   * Se encarga de armar el objeto de favorito para ser dado de alta.
   * @param datos Informacion del favorito.
   * @param indice Numero de operacion.
   * @param esValido Id que identifica que si la operacion actual se selecciono el check de favoritos.
   * @param sesion Objeto httpeticion.
   */
	private void darDeAltaFavoritoTransInterb(String[] datos,int indice, char esValido, HttpSession sesion) {

		EIGlobal.mensajePorTrace("Se va a dar de alta el favorito", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Indice "+ indice, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("esValido "+ esValido, EIGlobal.NivelLog.INFO);

		BaseResource bR = (BaseResource) sesion.getAttribute("session");
		EI_Tipo descripciones = (EI_Tipo) sesion.getAttribute(FavoritosConstantes.ID_LISTA_DESCRIPCIONES);

		FavoritosEnlaceBean favorito = null;


		if(esValido == '1'){
			favorito = 	new FavoritosEnlaceBean();

			favorito.setContrato(bR.getContractNumber());
			favorito.setCodCliente(bR.getUserID8());
			favorito.setCuentaCargo(datos[0]);
			favorito.setDescCuentaCargo(datos[1]);
			favorito.setCuentaAbono(datos[2]);
			favorito.setDescCuentaAbono( datos[3] == null ? "" : datos[3] );
			favorito.setConceptoOperacion(datos[5]);
			favorito.setIdModulo(DIBT);
			favorito.setImporte(new Double(datos[4]));
			favorito.setTipoTransaccion("H".equals(datos[13])?FavoritosConstantes.ID_SPEI_TI:FavoritosConstantes.ID_TEF_TI);
			favorito.setDescripcion(descripciones.camposTabla[indice][0]);

			EIGlobal.mensajePorTrace("se creara el favorito: "+favorito.getDescripcion(), EIGlobal.NivelLog.INFO);

			if(!"SIN CUENTA".equals(favorito.getCuentaCargo()) && !this.agregarFavorito(favorito)){
				EIGlobal.mensajePorTrace("ERROR al crear el favorito: "+favorito.getDescripcion(), EIGlobal.NivelLog.INFO);
			}
		}else{
			EIGlobal.mensajePorTrace("Este favorito no fue seleccionado: "+datos[0] +"-- "+datos[2], EIGlobal.NivelLog.INFO);

		}

}

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException e
	 * @throws IOException e
	 */
	public void validaDuplicados(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

		HttpSession ses = req.getSession();
        BaseResource session = (BaseResource) ses.getAttribute("session");
		ArchivoRemoto archR = new ArchivoRemoto();
		ArchivoRemoto archR2 = new ArchivoRemoto();	// Para la copia de Exportar Archivo.
		boolean facCargo50=false;
		String strTabla="";
		String strExportar = "";
		String favoritosSeleccionados = (String) req.getParameter("strCheck"); //PARA FAVORITOS.
		EI_Tipo descFavoritos = (EI_Tipo) ses.getAttribute((FavoritosConstantes.ID_LISTA_DESCRIPCIONES));

		/* VECTOR 06-2016: SPID */
		String operDivisa = (String) req.getParameter("divTrx") == null ? "" : req.getParameter("divTrx");

		/***********************limpieza de variables de control de sesion IM199738  ********************/
		verificaArchivos(IEnlace.LOCAL_TMP_DIR+"/"+req.getSession().getId().toString()+".ses",true);
		if(req.getSession().getAttribute("Recarga_MDI_Enviar")!=null)
		 {
			req.getSession().removeAttribute("Recarga_MDI_Enviar");
			EIGlobal.mensajePorTrace( "MDI_Interbancario Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
         }
		else{
			EIGlobal.mensajePorTrace( "MDI_Interbancario La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);
		}
		// Termina modificacion de limpieza variables de control de sesion IM199738 *****

		/****************** 50 */
		//Facultad para la la cuenta "$50"
		facCargo50=(session.getFacultad("DEPSINCAR"))?true:false;
		EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Facultad DEPSINCAR: "+facCargo50, EIGlobal.NivelLog.INFO);
		/*****************/
		if(archR.copiaLocalARemoto(session.getUserID8()+".tib")){
			EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): el archivo .tib se copio correctamente", EIGlobal.NivelLog.DEBUG);
		} else {
			EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): error al copiar el archivo .tib", EIGlobal.NivelLog.ERROR);
		}
		String IP = " ";
		String fechaHr = "";												//variables locales al m?todo
		IP = req.getRemoteAddr();											//ObtenerIP
		java.util.Date fechaHrAct = new java.util.Date();
		SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci?n de fecha y hora

		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult=null;

		String var="false";
		String nuevoF="5";
		if(ses.getAttribute(NUEVO_FORMATO_INTERBANCARIO)!=null){
		   var=(String)ses.getAttribute(NUEVO_FORMATO_INTERBANCARIO);}

		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Archivo con RFC: <" + var + ">", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): TIPO DIVISA <" + operDivisa + ">", EIGlobal.NivelLog.INFO);
		String tramaEnviada ="";

		tramaEnviada="1EWEB|"+session.getUserID8()+"|VALF|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|4|";
		EIGlobal.mensajePorTrace(fechaHr+"MDI_Interbancario - validaDuplicados(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);

		try
		  {
			htResult=tuxGlobal.web_red(tramaEnviada);
	      }catch(java.rmi.RemoteException re)
		   {EIGlobal.mensajePorTrace(re.getMessage(), EIGlobal.NivelLog.ERROR);}
		   catch(Exception e)
		   {EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			   EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Excepcion A->: "+e.getMessage(), EIGlobal.NivelLog.INFO);}

		//Recepcion de la respuesta del servicio Tuxedo
		   String CodError = null;
		   if (htResult != null) {
			   CodError = ( String ) htResult.get( "BUFFER" );
		   }
		//Se emplea para simular la respuesta Ok del servicio Tuxedo
		//String CodError = "VALF0000@Transaccion Exitosa@/tmp/1001851.tibr1";
		//VALF0005@Error al acceder a la tabla TCT_BITACORA@
		//TCT0321     012Mensaje

		//TODO BIT CU2061, Valida Duplicados
		/*
		 * VSWF-HGG-I
		 */
	    if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		String nomArchRegreso = "";
		try{
			if(CodError != null && !"0000".equals(CodError.trim().substring(4, 8))
					&& "TCT".equals(CodError.trim().substring(0, 3))){
				String msgT="";
				if(CodError.length()>16) msgT=CodError.substring(16);
				StringTokenizer separa = new StringTokenizer(CodError, "@");
				String[] resultado = new String[3];
				int ii =0;
				while (separa.hasMoreTokens())
				 {
					resultado[ii]= separa.nextToken();
					ii++;
				 }

				int NumError = Integer.parseInt(resultado[0].substring(4,8));

				if ((NumError==0) && !("-".equals(resultado[2].trim()))){
					nomArchRegreso = resultado[2].substring(resultado[2].lastIndexOf('/')+1);
				}
			}

			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession(false));
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ET_INTERBANCARIAS_VALIDA_DUPLICADOS);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			if(CodError != null){
				if("OK".equals(CodError.substring(0,2))){
					bt.setIdErr("VALF0000");
				}else if(CodError.length()>8){
					bt.setIdErr(CodError.substring(0,8));
				}else{
					bt.setIdErr(CodError.trim());
				}
			}else{
				bt.setIdErr("");
			}
			if (nomArchRegreso != null) {
				bt.setNombreArchivo(nomArchRegreso.trim());
			}
			bt.setServTransTux("VALF");

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
	    }
		/*
		 * VSWF-HGG-F
		 */

		String radio=req.getSession().getAttribute("Radio").toString();
		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Radio: "+radio, EIGlobal.NivelLog.DEBUG);
		if("2".equals(radio)){
			darDeAltaFavoritosArchivo(favoritosSeleccionados,descFavoritos,ses);
		}
		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): El servicio regreso CodError: "+CodError, EIGlobal.NivelLog.DEBUG);

		if(CodError==null)
		  {
			despliegaPaginaErrorURL("Su <font color=blue>transacci&oacute;n</font> no puede ser atendida en este momento, por favor intente m&aacute;s tarde.","Transferencias Interbancarias","Transferencias > Interbancarias" ,"", req, res);
			return;
		  }

		if(!"0000".equals(CodError.trim().substring(4,8)) && "TCT".equals(CodError.trim().substring(0,3)))
		  {
			String msgT="";
			if(CodError.length()>16)
				msgT=CodError.substring(16);
			else
			  msgT="Su <font color=blue>transacci&oacute;n</font> no puede ser atendida en este momento, por favor intente m&aacute;s tarde.";
			despliegaPaginaErrorURL(msgT,"Transferencias Interbancarias","Transferencias > Interbancarias" ,"", req, res);
			return;
		  }

		StringTokenizer separador = new StringTokenizer(CodError, "@");
		String[] result = new String[3];
		int i =0;
		while (separador.hasMoreTokens())
		 {
			result[i]= separador.nextToken();
			EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Elemento " + i + " es : "+ result[i], EIGlobal.NivelLog.DEBUG);
			i++;
		 }

		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): CodError para parseInt "+ result[0].substring(4,8), EIGlobal.NivelLog.DEBUG);

		int NumeroError = Integer.parseInt(result[0].substring(4,8));

		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): CodError para parseInt "+ NumeroError, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): ELMENTO"+ result[2], EIGlobal.NivelLog.DEBUG);
		if ((NumeroError==0) && !("-".equals(result[2].trim())))
		  {

			EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Existen duplicados", EIGlobal.NivelLog.DEBUG);

			long posicionReg = 0;
			long residuo	 = 0;

			int cont=0;
			int indice;

			String linea="";
			String strArc ="";
			String cad_decimal="";
			String datoscta[]=null;
			String nombreArchRegreso = result[2].substring(result[2].lastIndexOf('/')+1);
			ValidaCuentaContrato valCtas = null;

		    try
		    {
				if(archR.copia(nombreArchRegreso))
				  EIGlobal.mensajePorTrace("***MDI_Interbancario. El archivo tibr se copio correctamente", EIGlobal.NivelLog.DEBUG);
				else
				  EIGlobal.mensajePorTrace("***MDI_Interbancario el archivo tibr no se copio correctamente", EIGlobal.NivelLog.ERROR);

				MDI_Importar ArchImp = new MDI_Importar();
				EI_Tipo ArchivoI=new EI_Tipo(this);
				EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

				String aux_nombreArchRegreso = nombreArchRegreso; //Dom31Oct
				nombreArchRegreso=Global.DIRECTORIO_LOCAL+"/"+nombreArchRegreso;
				BufferedReader arch	= new BufferedReader( new FileReader(nombreArchRegreso) );

				//Se trata de generar la copia remota del Archivo para Exportacion. Dom31Oct
				if(archR2.copiaLocalARemoto( aux_nombreArchRegreso,"WEB") )
				 {
					EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Copia Exportacion: "+ aux_nombreArchRegreso, EIGlobal.NivelLog.INFO);
					session.setRutaDescarga( "/Download/" + aux_nombreArchRegreso );
					req.setAttribute("DescargaArchivo",session.getRutaDescarga());
				 }
				else
					EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): No se pudo copiar el archivo de Exportacion: "+ aux_nombreArchRegreso, EIGlobal.NivelLog.INFO);
				//Termina copia remota para Exportar Archivo. Dom31Oct

				ArchImp.nuevoFormato=false;
				if("true".equals(var.trim()))
					  ArchImp.nuevoFormato=true;
				EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Cargando variable con RFC: "+ ArchImp.nuevoFormato, EIGlobal.NivelLog.INFO);
				long numLinea = 0;
				valCtas = new ValidaCuentaContrato();
				// Exportar Duplicados
				List<MDI_Importar> exportarDuplicados = new ArrayList<MDI_Importar>();

				while((linea=arch.readLine()) != null)
				 {
					numLinea++;
					indice = -1;
					cad_decimal = "";
					if("".equals(linea.trim()))
						linea = arch.readLine();
					if( linea == null )
						break;
					EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados():  linea->" + linea +"<-", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): formateando linea: " + numLinea + ", longitud: " + linea.length(), EIGlobal.NivelLog.INFO);
					ArchImp.formateaLinea(linea);
					EIGlobal.mensajePorTrace("MDI_Interbancario ArchImp.cuentaCargo: "+ ArchImp.cuentaCargo, EIGlobal.NivelLog.DEBUG);
					/****************** 50 */
					if("SIN CUENTA".equals(ArchImp.cuentaCargo.trim()) && facCargo50)
					  ArchImp.titular="SIN TITULAR";
					else
					  {
						// INICIA - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
						// GENERACION DEL ARCHIVO AMBCI Y AMBCA
						/**
						datoscta=BuscandoCtaInteg(ArchImp.cuentaCargo.trim(),IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci",IEnlace.MDep_inter_cargo);
						*/
					    if("2".equals(radio) && "USD".equals(operDivisa)){
					        
					        EIGlobal.mensajePorTrace("La operacion es en Dolares : "
					                        + "-- Busca info para dol", EIGlobal.NivelLog.DEBUG);
					        
					        datoscta = valCtas.obtenDatosValidaCtaDolares(ArchImp.cuentaCargo.trim(), 
                                    IEnlace.MCargo_transf_dolar, req);
					    }else{
						datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
											                      session.getUserID8(),
											                      session.getUserProfile(),
											                      ArchImp.cuentaCargo.trim(),
											                      IEnlace.MDep_inter_cargo);
					    }
           			    // FIN - MODIFICACION PARA QUITAR EL LLAMADO PARA LA
					    // GENERACION DEL ARCHIVO AMBCI Y AMBCA

						EIGlobal.mensajePorTrace("MDI_Interbancario datoscta: "+ datoscta, EIGlobal.NivelLog.DEBUG);
						if(datoscta!=null && datoscta.length>3)
							ArchImp.titular=datoscta[4];
					  }

					exportarDuplicados.add(ArchImp);




					EIGlobal.mensajePorTrace("MDI_Interbancario cont: "+ cont, EIGlobal.NivelLog.DEBUG);
					 if(cont < 30) //Control mostrar 30 regs. max pantalla. Se genera tabla con tope a 30 regs. Dom31Oct
					  {
						strArc+= ArchImp.cuentaCargo+"|";
						strArc+= ArchImp.titular+"|";
						strArc+= ArchImp.cuentaAbono+"|";
						strArc+= ArchImp.beneficiario+"|";

						//Modificacion para validar numero decimales a mostrar
						EIGlobal.mensajePorTrace("MDI_Interbancario importe orig: "+ ArchImp.importe, EIGlobal.NivelLog.DEBUG);
						String dato_aux = new Double(ArchImp.importe).toString();
						indice = dato_aux.indexOf('.');
						if (indice !=-1)
						 {
							cad_decimal = dato_aux.substring(indice+1);
							if (cad_decimal.length() == 1)
							 {
								strArc+= dato_aux +"0|";
								EIGlobal.mensajePorTrace("MDI_Interbancario importe final: "+ strArc, EIGlobal.NivelLog.DEBUG);
							 }
							else
							  strArc+= dato_aux +"|";
						 }
						else
						  strArc+= dato_aux +"|";

						strArc+= ArchImp.concepto+"|";
						strArc+= ArchImp.referencia+"|";
						strArc+= ArchImp.banco+"|";
						strArc+= ArchImp.plaza+"|";
						strArc+= ArchImp.sucursal+"|";
						strArc+= EnlaceGlobal.fechaHoy("dd/mm/aaaa") + "|";
						strArc+= ArchImp.RFC +"|";
						strArc+= ArchImp.importeIVA +"|";
						strArc+= ArchImp.formaAplica +"|";
						strArc+= ArchImp.getDescAplica() +"|";
						strArc+= "@";

						/* Datos de la transferencia...
						 0 CuentaCargo+"|";
						 1 titular+"|";
						 2 cuentaAbono+"|";
						 3 cbeneficiario+"|";
						 4 importe+"|";
						 5 concepto+"|";
						 6 referenciaInterbancaria+"|";
						 7 banco+"|";
						 8 plaza+"|";
						 9 sucursal+"|";
						10 fechaHoy("dd/mm/aaaa")+"|";
						11 RFC+"|";  // Opcional
						12 IVA+"|";  // Opcional
						13 FormaAplicacion+"|";
						14 Descripcion FormaAplicacion+"@";
						*/

						EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Trama para tabla de datos: "+strArc, EIGlobal.NivelLog.INFO);
						cont++;
					 }
				 }

				ExportModel em = new ExportModel("sepfileexp", '|', true)
				.addColumn(new Column(0, 15 , "Cuenta de Cargo"))
				.addColumn(new Column(16, 35, "Cuenta de Abono/M�vil"))
				.addColumn(new Column(36,40, "Banco"))
				.addColumn(new Column(41, 80, "Beneficiario"))
				.addColumn(new Column(81, 84, "Sucursal"))
				.addColumn(new Column(85, 99, "Importe"))
				.addColumn(new Column(100, 104, "Plaza"))
				.addColumn(new Column(105, 144, "Concepto"))
				;

				 ses.setAttribute("ExportModel", em);
				 ses.setAttribute("af", nombreArchRegreso);

				 strExportar+="<tr><td colspan=\"2\" align=\"center\" class=\"textabref\"><a style=\"cursor: pointer;\" >Exporta en TXT <input id=\"tipoExportacion\" type=\"radio\" value =\"txt\" name=\"tipoExportacion\" /></a>\n</td></tr>";
	             strExportar+="<tr><td colspan=\"2\" align=\"center\" class=\"textabref\"><a style=\"cursor: pointer;\" >Exporta en XLS <input id=\"tipoExportacion\" type=\"radio\" value =\"csv\" checked name=\"tipoExportacion\" /></a>\n</td></tr>";
	             strExportar+="<tr><td colspan=\"2\" align=\"center\" class=\"textabref\"><a style=\"cursor: pointer;\" >\n<a href='javascript:exportarArchivoPlano();'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";

	             req.setAttribute("BotonesExportar", strExportar);

				 //***** everis Cerrando BufferedReader  06/05/2008  ..inicio
				  valCtas.closeTransaction();
				  arch.close();
				  //**** everis Cerrando BufferedReader  06/05/2008  ..fin


				String InfoUser="";
				if (cont < 30)
				 {
					InfoUser= "<br>Esta  transmisi&oacute;n contiene registros duplicados, " +
							"si desea realizar el envi&oacute; de cualquier manera,"+
								"capture su contrase�a din&aacute;mica y presione Aceptar";
				 }
				else
				 {
					InfoUser= "<br>Esta  transmisi&oacute;n contiene registros duplicados, " +
							"se muestran solo los primeros 30 registros, pero el n&uacute;mero puede ser mayor."+
							"Exporte la informaci&oacute;n para ver el total de registros duplicados."+
							"S&iacute; desea enviarlo(s) de cualquier manera, capture su contrase�a din&aacute;mica y presione Aceptar";
				 }

				InfoUser="cuadroDialogo(\""+ InfoUser +"\",11)";
				req.setAttribute("InfoUser", InfoUser);

				ArchivoI.strOriginal+=strArc;
				ArchivoI.iniciaObjeto(ArchivoI.strOriginal);
				strTabla+=generaTablaDuplicados("Registros Duplicados",4,ArchivoI);

				EIGlobal.mensajePorTrace("Se obtuvieron los parametros de archivo y tabla", EIGlobal.NivelLog.DEBUG);

				req.setAttribute("Tabla",strTabla);
				req.setAttribute("strCheck",req.getParameter("strCheck"));
				req.setAttribute("Lineas",req.getParameter("Lineas"));
				req.setAttribute("Importe",req.getParameter("Importe"));
				req.setAttribute("ModuloTabla",req.getParameter("Modulotabla"));
				req.setAttribute("TransReg",req.getParameter("TransReg"));
				req.setAttribute("TransNoReg",req.getParameter("TransNoReg"));
				req.setAttribute("TransArch",req.getParameter("TransArch"));
				req.setAttribute("archivoEstatus",req.getParameter("archivoEstatus"));

				/** PYME FSW - INDRA MAYO 2015 Archivo Transferencias Programadas
				 **/
				req.setAttribute("fecha_completaReg",req.getParameter("fecha_completaReg"));
				req.setAttribute("Fecha",req.getParameter("Fecha"));
				/** PYME FSW - INDRA MAYO 2015 Archivo Transferencias Programadas
				 **/
				EIGlobal.mensajePorTrace("Se establecieron los atributos", EIGlobal.NivelLog.DEBUG);

				req.setAttribute("Registradas",req.getParameter("Registradas"));
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias &gt; Interbancarias","s55230",req));

				EIGlobal.mensajePorTrace("Ya cargue todos los parametros a MDI_ValidaDuplicados.jsp", EIGlobal.NivelLog.DEBUG);
				// INDRA PYME
				// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA

				if(req.getParameter("TransArch")!=null && "Archivo de Exportacion".equals(req.getParameter("TransArch").trim()))
			     {
					req.setAttribute("TotalTrans",req.getParameter("TransArch"));
					/** PYME FSW - INDRA MARZO 2015 Unificacion Token
					 * Se comenta envio a jsp para que esta parte del flujo
					 * se mande llamar dentro de la validacion del token */
					EIGlobal.mensajePorTrace("Redireccionando a MDI_Enviar - execute(): .", EIGlobal.NivelLog.INFO);
					 RequestDispatcher rdSalida = getServletContext()
					 .getRequestDispatcher("/enlaceMig/MDI_Enviar");
					 rdSalida.forward( req, res );
				 } else {
				EIGlobal.mensajePorTrace("Redireccionando a MDI_Cotizar - execute(): .", EIGlobal.NivelLog.INFO);
				req.setAttribute("Modulo", "1");
				RequestDispatcher rdSalida = getServletContext()
				.getRequestDispatcher("/enlaceMig/MDI_Cotizar");
				rdSalida.forward( req, res );
				 }
				// INDRA PYME
				// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA

			}catch(Exception e) {
				EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados() D->: Excepcion: "+e.getMessage(), EIGlobal.NivelLog.INFO);}
			finally{
				try{
					valCtas.closeTransaction();
		    	}catch (Exception e) {
		    		EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
		    		EIGlobal.mensajePorTrace("Error al cerrar la conexion a MQ", EIGlobal.NivelLog.ERROR);
				}
			}
		  } else if ((NumeroError==0) && ("-".equals(result[2].trim()))) {
			EIGlobal.mensajePorTrace("MDI_Interbancario - validaDuplicados(): Sin duplicados", EIGlobal.NivelLog.DEBUG);

			String mensajeHTML="";


			mensajeHTML= "La validaci&oacute;n se realiz&oacute; exitosamente, no existen registros <font color=blue>duplicados</font>., " +
			"Ahora puede <font color=green>continuar</font> con el proceso de env&iacute;o.<br>";

			mensajeHTML="cuadroDialogo(\""+ mensajeHTML +"\",11)";
			req.setAttribute("InfoUser", mensajeHTML);


			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias Interbancarias","Transferencias &gt; Interbancarias","s55230",req));
			req.setAttribute("Mensaje",despliegaMensaje(mensajeHTML));
			req.setAttribute("strCheck",req.getParameter("strCheck")); // Q05-0016065 - NVS - 050615
			req.setAttribute("Lineas",req.getParameter("Lineas"));
			req.setAttribute("Importe",req.getParameter("Importe"));
			req.setAttribute("ModuloTabla",req.getParameter("Modulotabla"));
			req.setAttribute("TransReg",req.getParameter("TransReg"));
			req.setAttribute("TransNoReg",req.getParameter("TransNoReg"));
			req.setAttribute("TransArch",req.getParameter("TransArch"));
			req.setAttribute("archivoEstatus",req.getParameter("archivoEstatus"));
			req.setAttribute("Registradas",req.getParameter("Registradas"));
			req.setAttribute("BotonesExportar", strExportar);

			/** PYME FSW - INDRA MAYO 2015 Archivo Transferencias Programadas
			 **/
			req.setAttribute("fecha_completaReg",req.getParameter("fecha_completaReg"));
			req.setAttribute("Fecha",req.getParameter("Fecha"));
			/** PYME FSW - INDRA MAYO 2015 Archivo Transferencias Programadas
			 **/

			//Verificar si las operaciones son en linea o por archivo
			if(req.getParameter("TransArch")!=null && "Archivo de Exportacion".equals(req.getParameter("TransArch").trim()))
		     {
				req.setAttribute("TotalTrans",req.getParameter("TransArch"));
				/** PYME FSW - INDRA MARZO 2015 Unificacion Token
				 * Se comenta envio a jsp para que esta parte del flujo
				 * se mande llamar dentro de la validacion del token */
				EIGlobal.mensajePorTrace("Redireccionando a MDI_Enviar - execute(): .", EIGlobal.NivelLog.INFO);
				 RequestDispatcher rdSalida = getServletContext()
				 .getRequestDispatcher("/enlaceMig/MDI_Enviar");
				 rdSalida.forward( req, res );
			 } else {
			// INDRA PYME
			// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA
				 EIGlobal.mensajePorTrace("Redireccionando a MDI_Enviar - execute(): .", EIGlobal.NivelLog.INFO);
				 req.setAttribute("Modulo", "1");
				 RequestDispatcher rdSalida = getServletContext()
				 .getRequestDispatcher("/enlaceMig/MDI_Cotizar");
				 rdSalida.forward( req, res );
			 }
			// INDRA PYME
			// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA
	     }
	    else if (NumeroError>0)
	     {
			despliegaPaginaErrorURL(result[1],"Transferencias Interbancarias","Transferencias > Interbancarias" ,"", req, res);
	     }

	}//fin de la funci?n validaDuplicados

/**
 * Da de alta los favoritos seleccionados del archivo.
 * @param favoritosSeleccionados Lista de favoritos seleccionados
 * @param descFavoritos Descripciones
 * @param sesion Objeto sesion.
 */
private void darDeAltaFavoritosArchivo(String favoritosSeleccionados,
			EI_Tipo descFavoritos, HttpSession sesion) {

	EIGlobal.mensajePorTrace("SE dara de alta los favortios del archvio", EIGlobal.NivelLog.INFO);
	List<FavoritosEnlaceBean> favoritosTmp = (List<FavoritosEnlaceBean>) sesion.getAttribute(FavoritosConstantes.ID_LISTA_FAV_TMP);
	char valor = '1';
	String descripcion = "";
	FavoritosEnlaceBean favorito = null;

	EIGlobal.mensajePorTrace("favoritosTmp tama�o ::: "+favoritosTmp.size(), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("favoritosSeleccionados ::: "+favoritosSeleccionados.length(), EIGlobal.NivelLog.INFO);

	for(FavoritosEnlaceBean favorito_for: favoritosTmp){
		EIGlobal.mensajePorTrace("favorito.getContrato()::: "+favorito_for.getContrato(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("favorito.getCodCliente()::: "+favorito_for.getCodCliente(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("favorito.getCuentaCargo()::: "+favorito_for.getCuentaCargo(), EIGlobal.NivelLog.INFO);
	}

	if(favoritosTmp!=null&&!favoritosTmp.isEmpty()){
		//Se agrega contador para cuando los favoritos son intercalados
		int contFav = 0;
		for(int i=0;i<favoritosSeleccionados.length();i++){
			valor = favoritosSeleccionados.charAt(i);
			if(valor == '1'){
				descripcion = descFavoritos.camposTabla[contFav][0];
				contFav++;
				EIGlobal.mensajePorTrace("------> Se va a dar de alta un favoritos con la descripcion:"+descripcion, EIGlobal.NivelLog.INFO);
				favorito = favoritosTmp.get(i);

				favorito.setDescripcion(descripcion);
				if(!this.agregarFavorito(favorito)){
					EIGlobal.mensajePorTrace("ERROR al crear el favorito: "+favorito.getDescripcion(), EIGlobal.NivelLog.INFO);

				}
			}
		}
		favoritosTmp.clear();
	}else{
		EIGlobal.mensajePorTrace("ERROR no estan los temporales dados de alta", EIGlobal.NivelLog.INFO);
	}
	}

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		req.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		req.setAttribute( "Error", Error );

		req.setAttribute( "MenuPrincipal", session.getStrMenu() );
		req.setAttribute( "newMenu", session.getFuncionesDeMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,req ) );

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute( "URL", "MDI_Interbancario?Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( req, res );
	}

// Metodo para verificar existencia del Archivo de Sesion para evitar transmision duplicada
/*************************************************************************************/
/****************************************************************   verificaArchivos */
/*************************************************************************************/
public boolean verificaArchivos(String arc,boolean eliminar)
	throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
		File archivocomp = new File( arc );
		if (archivocomp.exists())
		 {
		   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.DEBUG);
		   if(eliminar)
			{
			  archivocomp.delete();
			  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
			}
		   return true;
		 }
		EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.DEBUG);
		return false;
	}

    // OPC:PASSMARK 16/12/2006
	private String CallPassmark(String usuario, String deviceprint, String ipe, String FSOanterior, String monto)
	{
		String FSONuevo = null;
		BufferedWriter out = null;
		BufferedReader in  = null;
		GregorianCalendar calendar = new GregorianCalendar();
        long nowInMilliseconds = calendar.getTime().getTime();
		try{
			if(monto.indexOf(".")>-1)
			{
				monto = monto.substring(0,monto.indexOf("."));
			}
		}
		catch(Exception e){
			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			monto = "";
		}

        try {
            URL url = new URL(Global.URL_PASSMARK_ENLACE);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            //conn.setConnectTimeout(1000);
            out = new BufferedWriter( new OutputStreamWriter( conn.getOutputStream() ) );
			out.write("Oper=a&");
            out.write("usuario=" +usuario     +"&");
            out.write("device="  +deviceprint +"&");
            out.write("ipuser="  +ipe         +"&");
			out.write("pmmonto=" +monto       +"&");
            out.write("fsoantes="+FSOanterior +"&");
            out.write("canal=2");
            out.flush();
            out.close();

            in = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );

            String response;
            while ( (response = in.readLine()) != null ) {
				if(response.indexOf("PMDATA=[")>-1 && response.indexOf("]!-")>-1)
				{
					FSONuevo = response.substring(response.indexOf("PMDATA=[")+8,response.indexOf("]!-"));
				}
				else
				{
					EIGlobal.mensajePorTrace("MDI_Interbancario --> CallPassmark :: FORMATO DE LA RESPUESTA INCORRECTO", EIGlobal.NivelLog.DEBUG);
				}
            }
            in.close();
        }
        catch ( MalformedURLException ex ) {
        	EIGlobal.mensajePorTrace(ex.getMessage(), EIGlobal.NivelLog.ERROR);
			//ex.printStackTrace();
        }
        catch ( IOException ex ) {
        	EIGlobal.mensajePorTrace(ex.getMessage(), EIGlobal.NivelLog.ERROR);
			//ex.printStackTrace();
        }
		catch ( Exception ex ) {
			EIGlobal.mensajePorTrace(ex.getMessage(), EIGlobal.NivelLog.ERROR);
			//ex.printStackTrace();
        }
		finally{
			try{
				out.close();
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			try{
				in.close();
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
//		GregorianCalendar calendar2 = new GregorianCalendar();
//        long nowInMilliseconds2 = calendar2.getTime().getTime();
//		try{
//			//System.out.println( "["+(nowInMilliseconds2-nowInMilliseconds)+" ms] MDI_Interbancario :: CallPassmark :: FSONUEVO = " + FSONuevo );
//		}catch(Exception e){
//			EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
//			//e.printStackTrace();
//		}
		return FSONuevo;
    }

	/**
	 * Arma dias inhabiles js.
	 *
	 * @return the string
	 */
	String armaDiasInhabilesJS()
	{
		int indice	 = 0;
		StringBuffer resultado = new StringBuffer();
		List<String> diasInhabiles;

		diasInhabiles = cargarDias(1);

		if(diasInhabiles!=null)
		{

			resultado.append( diasInhabiles.get(indice).toString() );
			for(indice=1; indice<diasInhabiles.size(); indice++){
				resultado.append(", ").append(diasInhabiles.get(indice).toString());}
		}
		return resultado.toString();
	}

	/**
	 * Cargar dias.
	 *
	 * @param formato the formato
	 * @return the vector
	 */
	public List<String> cargarDias(int formato)
	{
		EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Entrando", EIGlobal.NivelLog.DEBUG);
		Connection  Conexion=null;
		Statement qrDias=null;
		ResultSet rsDias=null;
		String	   sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		List<String> diasInhabiles = new ArrayList<String>();
		 EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Query"+sqlDias, EIGlobal.NivelLog.ERROR);
		 try{

			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
				 if(Conexion!=null)
			 {
			    qrDias = Conexion.createStatement();

			    if(qrDias!=null)
			    {
				  rsDias = qrDias.executeQuery(sqlDias);
			       if(rsDias!=null){
			    	   diasInhabiles = obtenerDias(rsDias, diasInhabiles, formato);
			       } else {
			    	   EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);}
			    } else {
			      EIGlobal.mensajePorTrace("***transferencia.CargarDias()->  Cannot Create Query", EIGlobal.NivelLog.ERROR);}
			 } else {
					EIGlobal.mensajePorTrace("***transferencia.CargarDias()-> Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);}
		   }catch(SQLException sqle ){
			   EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
		   }
		   finally
		   {
		    try	{

		    	if( rsDias != null ){
		    		rsDias.close();
		    	}
		        if( qrDias != null ){
		        	qrDias.close();
		        }
		        if( Conexion != null ){
		        	Conexion.close();
		        }
		    }catch(SQLException e){
				EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
	       }

		   return(diasInhabiles);
	}
	/**
	 * Metodo para obtener los dias
	 * @since 27/04/2015
	 * @author FSW-Indra
	 * @param rsDias resulset de los dias
	 * @param diasInhabiles dias habiles
	 * @param formato formato del dia
	 * @return List<String> dias
	 */
	public List<String> obtenerDias(ResultSet rsDias, List<String> diasInhabiles, int formato ){
	      String dia  = "";
	      String mes  = "";
	      String anio = "";
	      try{
			  while( rsDias.next()){
//				  try{
					dia  = rsDias.getString(1);
					mes  = rsDias.getString(2);
					anio = rsDias.getString(3);
//				  } catch(Exception e) {
//					  EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
//				  }
			      if(formato == 0){
//			    	  try{
						  dia  =  Integer.toString( Integer.parseInt(dia) );
						  mes  =  Integer.toString( Integer.parseInt(mes) );
						  anio =  Integer.toString( Integer.parseInt(anio) );
//					  } catch(Exception e) {
//						  EIGlobal.mensajePorTrace(e.getMessage(), EIGlobal.NivelLog.ERROR);
//					  }
			      }
			      String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
			      if("".equals(fecha.trim())){
						  fecha="1/1/2002";}
			      diasInhabiles.add(fecha);
			  }
	      } catch (SQLException sqle){
	    	  EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	      }
	      return diasInhabiles;
	}

}
/*2007.01.1*/