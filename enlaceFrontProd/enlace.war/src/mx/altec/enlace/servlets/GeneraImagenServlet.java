package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.ImagenDTO;

import org.apache.commons.codec.binary.Base64;

/**
 * Servlet implementation class GeneraImagenServlet
 */
public class GeneraImagenServlet extends HttpServlet {
	/**serialVersionUID**/
	private static final long serialVersionUID = 1L;

	 /** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<ImagenDTO> listaImagenes  = (List<ImagenDTO>) request.getSession().getAttribute("listaImages");
		int pos = Integer.parseInt(request.getParameter("val").toString());
		
    	byte[] imageBytes = Base64.decodeBase64(listaImagenes.get(pos).getImagen().getBytes());
    	
    	response.setHeader("Cache-Control", "no-store");
    	response.setHeader("Pragma", "no-cache");
    	response.setDateHeader("Expires", 0);
    	response.setContentType("image/jpeg");
    	response.setContentLength(imageBytes.length);

    	response.getOutputStream().write(imageBytes);
    	response.getOutputStream().close();
    	
    	
		
	}

	 /** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
