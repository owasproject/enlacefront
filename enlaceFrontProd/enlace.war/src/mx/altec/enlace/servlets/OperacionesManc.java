/*****************************************************************************
 Banco Santander Mexicano
 Clase: OperacionesManc -- Clase para la autorizacion de operaciones mancomunadas
 Fecha: 28 de enero de 2004
*****************************************************************************/

package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosManc;
import mx.altec.enlace.bo.DatosBeneficiarioBO;
import mx.altec.enlace.bo.DatosMancInter;
import mx.altec.enlace.bo.OperacionesInternacionalesBO;
import mx.altec.enlace.bo.RespManc;
import mx.altec.enlace.bo.TrxGP93BO;
import mx.altec.enlace.bo.TrxGP93VO;
import mx.altec.enlace.bo.TrxGPF2BO;
import mx.altec.enlace.bo.TrxGPF2VO;
import mx.altec.enlace.bo.TrxPE80BO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.BancosEU;
import mx.altec.enlace.utilerias.CatDivisaPais;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;
import java.lang.*;

public class OperacionesManc extends BaseServlet
{
	/**
     * Inits the.
     *
     * @param config the config
     * @throws ServletException the servlet exception
     */
    public void init (ServletConfig config) throws ServletException
	{
		super.init (config);
    }

	/**
     * Destroy.
     */
    public void destroy () { }

    /**
     *  Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
	{
		if (SesionValida (request, response))
		{
			BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
		    Integer Autoriza = (Integer) request.getSession ().getAttribute ("Autoriza");
		    String next = request.getParameter ( "Next" );
		    HttpSession sessionHttp = request.getSession();

			/******************************************Inicia validacion OTP**************************************/
			String valida = request.getParameter ( "valida" );

			if ( next==null && validaPeticion( request, response,session,request.getSession (),valida))
			{
				  EIGlobal.mensajePorTrace("\n\n ENTR� A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
			}
			/******************************************Termina validacion OTP**************************************/
			else
			{


				if(next==null && valida==null) {

					String validaChallenge = request.getAttribute("challngeExito") != null
						? request.getAttribute("challngeExito").toString() : "";

					EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {

						String []datosDentroRSA = null;
						if(request.getParameterValues("Folio") != null) {
							datosDentroRSA=request.getParameterValues("Folio");
						}

						EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);


						sessionHttp.setAttribute("yaValidoRSA","1");
						sessionHttp.setAttribute("FolioParam",datosDentroRSA);
						ValidaOTP.guardaParametrosEnSession(request);

						validacionesRSA(request, response);
						return;
					}
				}


		    int aut = 2;
		    boolean valBitacora = true;
		    if( next==null && valida==null)
			{

		    	String []datos = null;
				EIGlobal.mensajePorTrace(">>> Valor de 'yaValidoRSA__->' : "+sessionHttp.getAttribute("yaValidoRSA"), EIGlobal.NivelLog.DEBUG);
				if("1".equals(sessionHttp.getAttribute("yaValidoRSA"))) {
					datos= (String[])sessionHttp.getAttribute("FolioParam");
					sessionHttp.removeAttribute("yaValidoRSA");
				} else {
					datos=request.getParameterValues("Folio");
				}


				request.getSession().setAttribute("FolioParam",datos);

				java.util.ArrayList notifica = new java.util.ArrayList ();
				notifica.add("");
		    	request.getSession().setAttribute ("datosNotificacion",notifica);
				if(valida == null) {

					boolean solVal=ValidaOTP.solicitaValidacion(
								session.getContractNumber(),IEnlace.MANCOMUNIDAD);



					if( session.getValidarToken() &&
						session.getFacultad(session.FAC_VAL_OTP) &&
						session.getToken().getStatus() == 1 &&
						solVal )
					{

		//				VSWF-HGG -- Bandera para guardar el token en la bit�cora
						request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
							ValidaOTP.guardaParametrosEnSession(request);

						EIGlobal.mensajePorTrace("TOKEN MANCOMUNIDAD OPERACIONES INTERNACIONALES", EIGlobal.NivelLog.INFO);

						request.getSession().setAttribute("registrosMancOI", String.valueOf(datos.length));
						request.getSession().removeAttribute("mensajeSession");
						if (request.getParameter("TipoOp").toString().trim().equals("1")) {
							ValidaOTP.mensajeOTP(request, "Manc_Op_Inter_A");
						} else {
							ValidaOTP.mensajeOTP(request, "Manc_Op_Inter_C");
						}

						ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
					}
					else{

						ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
						valida="1";
						valBitacora = false;
					}
				}
			}

		//retoma el flujo
		if( (next!=null ||(valida!=null && valida.equals("1"))))
		{

			if (valBitacora) {

	    		try{
	    			EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

	    			String token = "0";
	    			if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
					{token = request.getParameter("token");}
	    			int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
	    			String claveOperacion = BitaConstants.CTK_OPERACIONES_MANC;
	    			String concepto = BitaConstants.CTK_CONCEPTO_OPERACIONES_MANC;

	    			EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
	    			EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
	    			EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);

	    			String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0",token,concepto,0);
	    		} catch(Exception e) {
	    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	    		}
			}


			request.getSession ().setAttribute ("Next", next);

			try
			{
				aut = Integer.parseInt (request.getParameter ("Autoriza"));

		    } catch (NumberFormatException ex) {

		    }

		    if (Autoriza == null)
			Autoriza = new Integer (aut);


			request.getSession ().setAttribute ("Autoriza", Autoriza);
			String operacion = "";
			if (Autoriza.intValue () == 1){
				operacion = "Autorizaci&oacute;n";
				request.getSession ().setAttribute("Encabezado", CreaEncabezado(""+ operacion +" de Operaciones Mancomunadas Internacionales",
			"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Operaciones Internacionales Mancomunadas", "s39030h", request));
			}else{
				operacion = "Cancelaci&oacute;n";
				request.getSession ().setAttribute("Encabezado", CreaEncabezado(""+ operacion +" de Operaciones Mancomunadas Internacionales",
			"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Operaciones Internacionales Mancomunadas", "s39040h", request));
			}

			request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
			request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());

			request.getSession ().setAttribute ("operacion", operacion);
			autorizaOperaciones (request, response);

		}
		}
    }}

    /**
     *  Metodo para autorizar las operaciones seleccionadas en la consulta de mancomunidad.
     *
     * @param request Objeto request del servlet
     * @param response Objeto response del servlet
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void autorizaOperaciones (HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
	{
    	//Declaraci�n de variables Migraci�n MA a 390
    	Integer referencia = 0;
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable hs = null;
		String buffer = "";
		Double conSaldo = 0.0;
		Double importeDiv = 0.0;
		String desResCodErr = "";
		String codError = "";
		String respuesta = "";
		String respuesta02 = "";
		String respuesta07 = "";
		String divisaOperacion = "";
		String resCodError = null;
		String folioOperacion = "";
		String idPaisBcoCorresp = "";
		String idBcoCorresp = "";
    	String bancoDesBita = "";
    	String ciudadDesBita = "";
    	String[] titularCtaCargo = null;
    	String[] datosCtaInter = null;
    	String[] transfer = null;
    	Vector paises = null;
    	TipoCambioEnlaceDAO tipoCambio = null;
    	TrxGPF2BO busGPF2 = new TrxGPF2BO();
    	TrxPE80BO busPE80 = new TrxPE80BO();
    	TrxGP93BO busGP93 = new TrxGP93BO();
    	String tramaMancActEst = "";
    	String importeFinal = "";
    	String importeActEst = "";
    	String tipoCambioOperacion = "";


		String[] indices = (String[])request.getSession().getAttribute("FolioParam");

		request.getSession().removeAttribute("FolioParam");
		int index = 0;
		try
		{
			index = Integer.parseInt ((String)request.getSession ().getAttribute ("Next"));
		} catch (NumberFormatException ex) {

		}




		java.util.ArrayList porProcesar = (java.util.ArrayList) request.getSession ().getAttribute ("OperacionesMancomunadasInter");

		if (indices == null && (porProcesar == null || porProcesar.isEmpty ()))
		{	    despliegaPaginaError ("Error al realizar la autorizaci&oacute;n", request, response);
		    return;
		}
		Enumeration names = request.getSession ().getAttributeNames ();


		try{
		if (indices != null)
		{
			request.getSession ().setAttribute ("Pendientes", new Object ());

		    java.util.ArrayList operaciones = (java.util.ArrayList) request.getSession ().getAttribute ("ResultadosMancomunidadInter");

		    porProcesar = new java.util.ArrayList (indices.length);

			for (int x = 0; x < indices.length; x++)
			{
				try
				{
					int temp = Integer.parseInt (indices[x]);
					porProcesar.add (operaciones.get (temp));

				} catch (Exception ex) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
				}
			}
			request.getSession ().removeAttribute ("ResultadosMancomunidadInter");
		    request.getSession ().setAttribute ("OperacionesMancomunadasInter", porProcesar);
		}
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

		Object o = porProcesar.get (index);
		if (!(o instanceof mx.altec.enlace.bo.DatosMancInter))
		{
			java.util.ListIterator liPorProcesar = porProcesar.listIterator (index);

		    while (liPorProcesar.hasNext ())
			{
				o = liPorProcesar.next ();
				if (o instanceof mx.altec.enlace.bo.DatosMancInter)
					break;
			}
		}

		if (o instanceof mx.altec.enlace.bo.DatosMancInter)
		{

			ServicioTux servicio = new ServicioTux ();

			String trama;
			String estatus = "";
			String fecha_auto = ObtenFecha(true);
			String folio_auto = "";
			String orden      = "";
			String saldo_disponible = "";
			String rastreo = "";
			String beneficiario="";
		    String cod_error = "";

			BaseResource ses = (BaseResource) request.getSession ().getAttribute ("session");
			int aut = 2;
			try
			{
				aut = ((Integer) request.getSession ().getAttribute ("Autoriza")).intValue ();
			} catch (NumberFormatException ex) {

			}

			if (aut == 1){

				trama = ((mx.altec.enlace.bo.DatosMancInter) o).getTramaAutorizacion (ses.getUserID (), ses.getContractNumber (), ses.getUserProfile ());

				try {
					hs = tuxGlobal.sreferencia("901");;
					codError = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
					referencia = (Integer) hs.get("REFERENCIA");
				} catch (Exception e) {
					codError = "";
					referencia = 0;
				}
				// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
				if (codError.endsWith("0000")) {
					//FORMANDO TRAMA PARA SERVICIO MANC_VALIDA
					String trama_manc = "";
					SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
					trama_manc += "|" + ((DatosMancInter)o).getFolio_registro() + "|";
					trama_manc += ses.getContractNumber() + "|";
					trama_manc += ((DatosMancInter)o).getTipo_operacion() + "|";
					trama_manc += sdf.format(new Date()) + "|";
	  				trama_manc += referencia.toString() +  "|";
	  				trama_manc += convierteUsr8a7(ses.getUserID()) +"|";
	  				trama_manc += ((DatosMancInter)o).getCta_origen() +"|";
	  				trama_manc += ((DatosMancInter)o).getCta_destino() +"|";
	  				trama_manc += ((DatosMancInter)o).getDivisaAbono() +"|";
	  				trama_manc += ((DatosMancInter)o).getTipoCompraVenta() +"|";
	  				trama_manc += ((DatosMancInter)o).getDivisaCargo();
	  				if(((DatosMancInter)o).getTipo_operacion().equals("DIPD")){
	  					trama_manc += "||||d||CHDO||||";
	  	  				trama_manc += ((DatosMancInter)o).getBeneficiario() +"||||";
	  	  				trama_manc += ((DatosMancInter)o).getConcepto() +"|||";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteMN()) +"|";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteUSD()) +"|";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteUSD()) +"|";
	  	  				trama_manc += ((DatosMancInter)o).getTipoCambioMN() +"|";
	  	  				trama_manc += "1.000000" + "| |";
	  				}else if(((DatosMancInter)o).getTipo_operacion().equals("DITA")){
	  					trama_manc += "|";
	  	  				trama_manc += ((DatosMancInter)o).getBanco() +"|||||TRAN|||";
	  	  				trama_manc += ((DatosMancInter)o).getCiudad() +"|";
	  	  				trama_manc += ((DatosMancInter)o).getBeneficiario() +"||";
	  	  				trama_manc += ((DatosMancInter)o).getPais() +"||";
	  	  				trama_manc += ((DatosMancInter)o).getConcepto() +"|||";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteMN()) +"|";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteUSD()) +"|";
	  	  				trama_manc += formatoImporte(((DatosMancInter)o).getImporteUSD()) +"|";
	  	  				trama_manc += ((DatosMancInter)o).getTipoCambioMN() +"|";
	  	  				trama_manc += "1.000000" + "|";
	  	  				trama_manc += ((DatosMancInter)o).getTitular_cargo() +"|";
	  				}


	  				//LLAMADO AL MANC_VALIDA
					try {
						hs = tuxGlobal.manc_valida(trama_manc);
						codError = (String) hs.get("COD_ERROR");
						buffer = (String) hs.get("BUFFER");
					} catch (Exception e) {
						codError = null;
					}

					//VERIFICANDO RESPUESTA DEL MANC_VALIDA
					if (codError.endsWith("0000")) {
						try {
							if(buffer.substring(0,1).equals("1")){
								//SE EXISTE FOLIO EN MANCOMUNIDAD
								resCodError = "OK";
								// FLUJO DE AUTORIZACION CAMBIO DE DIVISA
								if(((DatosMancInter)o).getTipo_operacion().equals("DIPD")){
									EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: Se ejecuta GP02", EIGlobal.NivelLog.INFO);
									//EJECUCI�N GP02 LO QUE ERA SERVICIO DITA
									tipoCambio = new TipoCambioEnlaceDAO();

									if(((DatosMancInter)o).getTipoCompraVenta().trim().equals("VTA"))
										divisaOperacion = ((DatosMancInter)o).getDivisaAbono();
									else if(((DatosMancInter)o).getTipoCompraVenta().trim().equals("CPA"))
										divisaOperacion = ((DatosMancInter)o).getDivisaCargo();
									EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: divisaOperacion->"+divisaOperacion, EIGlobal.NivelLog.INFO);

									respuesta = tipoCambio.getTranspasoCuentas(
											((DatosMancInter)o).getCta_origen(), //cuentaCargo
											divisaOperacion, //divisa
											formatoImporte(((DatosMancInter)o).getImporteDivisa()), //montoDivisa
											((DatosMancInter)o).getTipoCambioMN(),   //tipo cambio
											formatoImporte(((DatosMancInter)o).getImporteMN()),  //equivalenteMN
											formatoImporte(((DatosMancInter)o).getImporteUSD()), //equivalenteDLLS
											((DatosMancInter)o).getCta_destino(), //cuentaAbono
											((DatosMancInter)o).getCveEspecial(), //referenciaClave
											"", //folioOpCancelacion
											"0981", //centroOperante
											"0981", //centroOrigen
											"0981", //centroDestino
											((DatosMancInter)o).getConcepto(), //ObsCargo
											((DatosMancInter)o).getConcepto()); //ObsAbono

									EIGlobal.mensajePorTrace("OperacionesManc . RESPUESTA GP02<<" + respuesta + ">>", EIGlobal.NivelLog.INFO);

									//VERIFICANDO SI SE EJECUTO CORRECTAMENTE LA TRX
									//GPA0024 LIQUIDADA
									//GPA0063 PENDIENTE
									//GPA0010 ANULACION EFECTUADA
									//GPA00
									//ERBGE0038 CUENTA NO EXISTE EN TABLA DE MAESTRA DE CUENTAS
									if(respuesta.substring(0, 2).trim().equals("@1")){

										//SE LLENA VARIABLE DE FOLIO DE LIQUIDACI�N
										respuesta02 = respuesta.substring(respuesta.indexOf("DCGPM0021 P"), respuesta.length());
										folioOperacion = respuesta02.substring(264, 274);
										EIGlobal.mensajePorTrace("OperacionesManc::autorizaOperaciones:: folioOperacion<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
										//SE LLENA VARIABLE DE ORDEN
										orden = folioOperacion;
										EIGlobal.mensajePorTrace("OperacionesManc::autorizaOperaciones:: orden<<" + orden + ">>", EIGlobal.NivelLog.INFO);

										//SE BITACORIZA EN LA EWEB_BITA_INTER
										tipoCambio.insertEWEBBITA(ses.getContractNumber(), "D", orden, referencia, "RT",
															      "RT", ((DatosMancInter)o).getTipoCompraVenta(), ((DatosMancInter)o).getPais(),
															      ((DatosMancInter)o).getDivisaAbono(), ((DatosMancInter)o).getDivisaCargo(),
												                  "CHDO", ses.getUserID8(),"", ((DatosMancInter)o).getCveEspecial(),
												                  ((DatosMancInter)o).getCta_origen(), ((DatosMancInter)o).getCta_destino(),
												                  folioOperacion, folioOperacion, 0, 0, ((DatosMancInter)o).getImporteMN(),
												                  ((DatosMancInter)o).getImporteUSD(),((DatosMancInter)o).getImporteUSD(),
												                  ((DatosMancInter)o).getTipoCambioMN(), "1.000000",
												                  sdf.format(new Date()), sdf.format(new Date()), "", "", "",
												                  ((DatosMancInter)o).getConcepto(),
												                  ((DatosMancInter)o).getBeneficiario(), "");

									}else{
										//ERROR AL EJECUTAR GP02
										resCodError = "ERROR";
										desResCodErr = respuesta.substring(18, respuesta.indexOf('�'));
										codError = respuesta.substring(10, 17);
										EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: RECHAZADA->"+desResCodErr, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: COD ERROR->"+codError, EIGlobal.NivelLog.INFO);
									}

								// FLUJO DE AUTORIZACION TRANSFERENCIA INTERNACIONAL
								}else if(((DatosMancInter)o).getTipo_operacion().equals("DITA")){

										if(((DatosMancInter)o).getDivisaCargo().trim().equals("MN")){
											importeDiv = Double.parseDouble(((DatosMancInter)o).getImporteMN());}
										else{
											importeDiv = Double.parseDouble(((DatosMancInter)o).getImporteUSD());}
										EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: importeDiv->"+importeDiv, EIGlobal.NivelLog.INFO);


											tipoCambio = new TipoCambioEnlaceDAO();
											//INDRA-SWIFT-P022574-ini
											DatosBeneficiarioBO beneficiarioBO = new DatosBeneficiarioBO();
											DatosBeneficiarioBean beneficiarioBean = new DatosBeneficiarioBean();
											OperacionesInternacionalesBO internacionalesBO = new OperacionesInternacionalesBO();
											//INDRA-SWIFT-P022574-fin
											//OBTENER TITULAR CUENTA DE CARGO, EJECUCI�N PE80
											try{
												titularCtaCargo = busPE80.obtenTitularCtaCargo(((DatosMancInter)o).getCta_origen());
											}catch(Exception e){
												titularCtaCargo = new String[3];
												titularCtaCargo[0] = "";
												titularCtaCargo[1] = "";
												titularCtaCargo[2] = "";
												EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: Problemas al obtener titular de cuenta de cargo por PE80", EIGlobal.NivelLog.INFO);
											}

											//OBTENER LOS DATOS DE LA CUENTA INTERNACIONAL
											try{
												datosCtaInter = tipoCambio.obtenDatosCtaInter(ses.getContractNumber(), ((DatosMancInter)o).getCta_destino());
												if(datosCtaInter[0].trim().equals("USA")){
													idBcoCorresp = datosCtaInter[1];}
												else{
													idBcoCorresp =  datosCtaInter[2];}
											}catch(Exception e){
												idBcoCorresp = "";
												EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: Problemas al obtener los datos de la cuenta internacional", EIGlobal.NivelLog.INFO);
											}
											EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: idBcoCorresp->"+idBcoCorresp, EIGlobal.NivelLog.INFO);

									    	//OBTENER LOS DATOS DE LA CUENTA INTERNACIONAL - INDRA-SWIFT-P022574-ini
									try {
										beneficiarioBean = beneficiarioBO.consultarDatosBenef(ses.getContractNumber(), ((DatosMancInter)o).getCta_destino());
										
										if ("USA".equals(beneficiarioBean.getCvePais())) {
											idBcoCorresp = beneficiarioBean.getCveAba();
										}else {
											idBcoCorresp = beneficiarioBean.getCveSwift();
										}
									}catch(Exception e){
										idBcoCorresp = "";
										EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: Problemas al obtener idBcoCorresp", EIGlobal.NivelLog.ERROR);
									}
									EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: idBcoCorresp->"+idBcoCorresp, EIGlobal.NivelLog.INFO);
									//INDRA-SWIFT-P022574-fin
											
											paises = CatDivisaPais.getInstance().getListaPaises();
									    	for(int i=0; i<paises.size(); i++){
									    		if(((TrxGP93VO)paises.get(i)).getOVarCod().substring(4).trim().equals(((DatosMancInter)o).getPais().trim())){
									    			idPaisBcoCorresp = ((TrxGP93VO)paises.get(i)).getOVarCod().substring(0,3).trim();
									    			break;
									    		}
									    	}
											EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: idPaisBcoCorresp->"+idPaisBcoCorresp, EIGlobal.NivelLog.INFO);


									if(("DA".equals(((DatosMancInter)o).getDivisaCargo().trim()) || "USD".equals(((DatosMancInter)o).getDivisaCargo().trim())) && 
											"USD".equals(((DatosMancInter)o).getDivisaAbono().trim())){
										//EJECUCI�N DE SERVICIO TRANSANT
										EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: OPERACION POR TRANSFER", EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: PAIS BENEF::"+beneficiarioBean.getCvePaisBenef(), EIGlobal.NivelLog.INFO);
										if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) { //INDRA-SWIFT-P022574
											EIGlobal.mensajePorTrace("MTI_Enviar:: Va por Tuxedo", EIGlobal.NivelLog.INFO);
											transfer = tipoCambio.ejecutaServicioTransfer(referencia, ((DatosMancInter)o).getCta_origen(),
								               ((DatosMancInter)o).getTitular_cargo(), ((DatosMancInter)o).getCta_destino(),
								               ((DatosMancInter)o).getBeneficiario(), ((DatosMancInter)o).getImporteUSD(),
								               ((DatosMancInter)o).getBanco(), ((DatosMancInter)o).getSucursal(),
								               ((DatosMancInter)o).getPais(), ((DatosMancInter)o).getCiudad(),
								               idBcoCorresp.trim(), ((DatosMancInter)o).getConcepto(),"",
								               ((DatosMancInter)o).getUsuario1(), ses.getUserID8());
										}else {
											//INDRA-SWIFT-P022574-ini
											EIGlobal.mensajePorTrace("MTI_Enviar:: Va por MQ", EIGlobal.NivelLog.INFO);
											transfer = internacionalesBO.enviaOperacionInternacional(beneficiarioBean, 
													OperacionesInternacionalesBO.OP_MISMA_DIVISA, referencia.toString(), 
													ses.getUserID8(), ((DatosMancInter)o).getCta_origen(), titularCtaCargo[0], ((DatosMancInter)o).getImporteUSD(), "USD", 
													titularCtaCargo[1], titularCtaCargo[2], titularCtaCargo[3], ((DatosMancInter)o).getCta_destino(), "", "");
											//INDRA-SWIFT-P022574-fin
										}
												
												if(transfer[0].endsWith("0000")){
													resCodError = "OK";
													desResCodErr = "ENVIADA";
													codError = "DIIN0000";

													folioOperacion = transfer[1];
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: folioOperacion Transfer<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
													//SE LLENA VARIABLE DE ORDEN
													orden = folioOperacion;
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: orden Transfer<<" + orden + ">>", EIGlobal.NivelLog.INFO);

													//SI NO HAY ERROR, SE BITACORIZA EN LA EWEB_BITA_INTER
													bancoDesBita = ((DatosMancInter)o).getBanco();
													ciudadDesBita = ((DatosMancInter)o).getCiudad();
									    			if(((DatosMancInter)o).getBanco().length()>40)
									    				bancoDesBita = ((DatosMancInter)o).getBanco().substring(0, 39);
													if(((DatosMancInter)o).getCiudad().length()>40)
														ciudadDesBita = ((DatosMancInter)o).getCiudad().substring(0, 39);

													tipoCambio.insertEWEBBITA(ses.getContractNumber(), "X", orden, referencia, "PV",
														      "PV", "VTA", ((DatosMancInter)o).getPais(),
														      ((DatosMancInter)o).getDivisaAbono(), ((DatosMancInter)o).getDivisaCargo(),
											                  "TRAN", ses.getUserID8(),
											                  idBcoCorresp.trim(), ((DatosMancInter)o).getCveEspecial(),
											                  ((DatosMancInter)o).getCta_origen(), ((DatosMancInter)o).getCta_destino(),
											                  folioOperacion, folioOperacion, Integer.parseInt(orden),
											                  Integer.parseInt(orden), "0", ((DatosMancInter)o).getImporteUSD(),
											                  ((DatosMancInter)o).getImporteUSD(), ((DatosMancInter)o).getTipoCambioMN(),
											                  "1.000000", sdf.format(new Date()), sdf.format(new Date()),
											                  bancoDesBita, "", ciudadDesBita,
											                  ((DatosMancInter)o).getConcepto(),
											                  ((DatosMancInter)o).getBeneficiario(), "");

													//NO SE CONSULTA EL SALDO DISPONIBLE CON QUE QUEDA LA CUENTA
												}else{
													//ERROR AL EJECUTAR OPERACION POR TRANSFER
													resCodError = "ERROR";
													desResCodErr = (transfer[2]!=null)?transfer[2]:"OPERACION RECHAZADA POR TRANSFER";
													codError = transfer[0];
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: RECHAZADA por Transfer->"+desResCodErr, EIGlobal.NivelLog.INFO);
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: COD ERROR por Transfer->"+codError, EIGlobal.NivelLog.INFO);
												}

											}else{
												//**********************************************************************
												//*************EJECUCI�N GP07 LO QUE ERA SERVICIO DITA******************
												//**********************************************************************
												if(getDivisaTrx(((DatosMancInter)o).getCta_origen()).trim().equals("USD")){
													importeFinal = ((DatosMancInter)o).getImporteUSD();
													tipoCambioOperacion = ((DatosMancInter)o).getTipoCambioDA();
												}else{
													importeFinal = ((DatosMancInter)o).getImporteMN();
													tipoCambioOperacion = ((DatosMancInter)o).getTipoCambioMN();
												}

												EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: Se ejecuta enviaOperacionInternacional", EIGlobal.NivelLog.INFO);
												EIGlobal.mensajePorTrace("MTI_Enviar:: iniciaEnviar:: PAIS BENEF::"+beneficiarioBean.getCvePaisBenef(), EIGlobal.NivelLog.INFO);
												if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
													EIGlobal.mensajePorTrace("MTI_Enviar:: Va por Tuxedo", EIGlobal.NivelLog.INFO);
													tipoCambio = new TipoCambioEnlaceDAO();
													respuesta = tipoCambio.getTransfInternacionales(
														((DatosMancInter)o).getCta_origen(),	//cuentaCargo,
														((DatosMancInter)o).getDivisaAbono(),	//divisa,
														formatoImporte(((DatosMancInter)o).getImporteDivisa()),	//montoDivisa,
														formatoImporte6(tipoCambioOperacion),	//tipoCambio,
														formatoImporte(importeFinal),		//si es DLL importeDA, si es MXN importeMN,
														((DatosMancInter)o).getCveEspecial(), 					//refClave,
														titularCtaCargo[0],//NombreBenef,
														titularCtaCargo[1],//apPatBenef,
														titularCtaCargo[2],//apMatBenef,
														idPaisBcoCorresp.trim(), 									//PABenef,
														((DatosMancInter)o).getBeneficiario(), 	//NombreBenef,
														"", 									//apPatBenef,
														"", 									//apMatBenef,
														"", 									//RFCBenef,
														((DatosMancInter)o).getCta_destino(),	//cuentaBenef,
														idBcoCorresp.trim(),//bancoCorresponsal,
														((DatosMancInter)o).getConcepto(), 									//detalleOp,
														"0981", 								//centroOperante,
														"0981", 								//CentroOrigen,
														"0981", 								//centroDestino,
														((DatosMancInter)o).getConcepto(), 		//ObsCargo,
														"" 										//ObsAbono
													);
												}else {
													//INDRA-SWIFT-P022574-ini
													EIGlobal.mensajePorTrace("MTI_Enviar:: Va por MQ", EIGlobal.NivelLog.INFO);
													transfer = internacionalesBO.enviaOperacionInternacional(beneficiarioBean, 
															OperacionesInternacionalesBO.OP_CAMBIARIA, ((DatosMancInter)o).getCveEspecial(), 
															ses.getUserID8(), ((DatosMancInter)o).getCta_origen(), titularCtaCargo[0], ((DatosMancInter)o).getImporteDivisa(), 
															getDivisaTrx(((DatosMancInter)o).getCta_origen()).trim(),titularCtaCargo[1], titularCtaCargo[2], titularCtaCargo[3], 
															((DatosMancInter)o).getCta_destino(), tipoCambioOperacion, importeFinal);
													//INDRA-SWIFT-P022574-fin
												}
										EIGlobal.mensajePorTrace("OperacionesManc . RESPUESTA GP07<<" + respuesta + ">>", EIGlobal.NivelLog.INFO);
										//INDRA-SWIFT-P022574-ini
										boolean resp= false;
										if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
											if(respuesta.substring(0, 2).trim().equals("@1")){
												resp = true;
											}
										}else {
											if(transfer[0].endsWith("0000")){ //INDRA-SWIFT-P022574
												resp = true;
											}
										}//INDRA-SWIFT-P022574-fin
										//if("@1".equals(respuesta.substring(0, 2).trim())){
										//if(transfer[0].endsWith("0000")) { //INDRA-SWIFT-P022574
										if (resp) {
											resCodError = "OK";
											desResCodErr = "ENVIADA";
											codError = "DIIN0000";
											//SE LLENA VARIABLE DE FOLIO DE LIQUIDACI�N
											//INDRA-SWIFT-P022574-ini
											if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
												respuesta07 = respuesta.substring(respuesta.indexOf("DCGPM0071 P"), respuesta.length());
												folioOperacion = respuesta07.substring(406, 416);
											}else {
												folioOperacion = transfer[1];	
											}
											//INDRA-SWIFT-P022574-fin
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: folioOperacion<<" + folioOperacion + ">>", EIGlobal.NivelLog.INFO);
													//SE LLENA VARIABLE DE ORDEN Y RASTREO
													orden = folioOperacion;
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: orden<<" + orden + ">>", EIGlobal.NivelLog.INFO);
													rastreo = "098121" + folioOperacion;
													EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: rastreo<<" + rastreo + ">>", EIGlobal.NivelLog.INFO);

													//SI NO HAY ERROR, SE BITACORIZA EN LA EWEB_BITA_INTER
													bancoDesBita = ((DatosMancInter)o).getBanco();
													ciudadDesBita = ((DatosMancInter)o).getCiudad();
									    			if(((DatosMancInter)o).getBanco().length()>40)
									    				bancoDesBita = ((DatosMancInter)o).getBanco().substring(0, 39);
													if(((DatosMancInter)o).getCiudad().length()>40)
														ciudadDesBita = ((DatosMancInter)o).getCiudad().substring(0, 39);

													tipoCambio.insertEWEBBITA(ses.getContractNumber(), "D", orden, referencia, "RT",
														      "RT", ((DatosMancInter)o).getTipoCompraVenta(), ((DatosMancInter)o).getPais(),
														      ((DatosMancInter)o).getDivisaAbono(), ((DatosMancInter)o).getDivisaCargo(),
											                  "TRAN", ses.getUserID8(),
											                  idBcoCorresp.trim(), ((DatosMancInter)o).getCveEspecial(),
											                  ((DatosMancInter)o).getCta_origen(), ((DatosMancInter)o).getCta_destino(),
											                  folioOperacion, folioOperacion, 0,
											                  0, ((DatosMancInter)o).getImporteMN(), ((DatosMancInter)o).getImporteUSD(),
											                  ((DatosMancInter)o).getImporteDivisa(), ((DatosMancInter)o).getTipoCambioMN(),
											                  ((DatosMancInter)o).getTipoCambioDA(), sdf.format(new Date()), sdf.format(new Date()),
											                  bancoDesBita, "", ciudadDesBita,
											                  ((DatosMancInter)o).getConcepto(),
											                  ((DatosMancInter)o).getBeneficiario(), "");

										}else {
											//ERROR AL EJECUTAR GP07
											resCodError = "ERROR";
											//INDRA-SWIFT-P022574-ini
											if (beneficiarioBean.getCvePaisBenef() == null || "".equals(beneficiarioBean.getCvePaisBenef())) {
												desResCodErr = respuesta.substring(18, respuesta.indexOf('�'));
												codError = respuesta.substring(10, 17);
											}else {
												desResCodErr = transfer[2];
												codError = transfer[0];	
											}
											//INDRA-SWIFT-P022574-fin
											EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: RECHAZADA->"+desResCodErr, EIGlobal.NivelLog.INFO);
											EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: COD ERROR->"+codError, EIGlobal.NivelLog.INFO);
										}
									}


										}

								}

						} catch (Exception e) {
							EIGlobal.mensajePorTrace("*** OperacionesManc - ERROR EN LA TRANSFERENCIA  ***"+e.getMessage(), EIGlobal.NivelLog.INFO);
							resCodError = "ERROR";
							codError = "MANC9999";
							desResCodErr = "PROBLEMAS AL REALIZAR LA OPERACI�N";
						}
					}else{
						//LA RESPUESTA DEL MANC_VALIDA NO FUE EXITOSA
						resCodError = "ERROR";
						desResCodErr = getDesErrManc(codError);
						EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: respuesta no existosa:: "+desResCodErr, EIGlobal.NivelLog.INFO);
					}
				}else{
					//CASO CONTRARIO REFERENCIA INCORRECTA
					resCodError = "ERROR";
					desResCodErr = "PROBLEMAS AL OBTENER LA REFERENCIA";
					EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: referencia incorrecta:: "+desResCodErr, EIGlobal.NivelLog.INFO);
				}

  				//SI EXISTE ERROR, SE LLAMA AL MANC_ACT_EST PARA ACTUALIZAR LA OPERACI�N COMO ERRONEA
				if(!codError.endsWith("0000")){
					try {
						if(((DatosMancInter)o).getDivisaCargo().trim().equals("DA") ||
						   ((DatosMancInter)o).getDivisaCargo().trim().equals("USD")){
							importeActEst = ((DatosMancInter)o).getImporteUSD();}
						else{
							importeActEst = ((DatosMancInter)o).getImporteMN();}
						tramaMancActEst = ((DatosMancInter)o).getFolio_registro() + "|"
									    + ses.getContractNumber() + "|"
									    + ((DatosMancInter)o).getTipo_operacion() + "|"
									    + importeActEst + "|"
									    + ((DatosMancInter)o).getCta_origen() + "|"
									    + "OPERACI�N RECHAZADA" + "|";
						hs = tuxGlobal.manc_act_est(tramaMancActEst);

						//SE ACTUALIZA EL ESTATUS DE LA OPERACI�N ANTES DE SALIR
						if (((String)hs.get("COD_ERROR")).endsWith("0000")){
							EIGlobal.mensajePorTrace("OperacionesManc::autorizaOperaciones:: SE ACTUALIZO LA OPERACION CORRECTAMENTE", EIGlobal.NivelLog.INFO);}
						else{
							EIGlobal.mensajePorTrace("OperacionesManc::autorizaOperaciones:: FALLO LA ACTUALIZACION DE LA OPERACION", EIGlobal.NivelLog.INFO);}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("OperacionesManc::autorizaOperaciones:: FALLO LA ACTUALIZACION DE LA OPERACION POR ERROR AL EJECUTAR SERVICIO", EIGlobal.NivelLog.INFO);
					}
				}

				//FLUJO FIN SUSTITUCION MA A 390
			}else{
				trama = ((mx.altec.enlace.bo.DatosMancInter) o).getTramaCancelacion (convierteUsr8a7(ses.getUserID ()), ses.getContractNumber (), ses.getUserProfile ());
				//Llamado al SREFERENCIA
				try {
					hs = tuxGlobal.sreferencia("901");;
					codError = hs.get("COD_ERROR").toString();
					EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
					referencia = (Integer) hs.get("REFERENCIA");
				} catch (Exception e) {
					codError = "";
					referencia = 0;
				}
				// VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
				if (codError.endsWith("0000")) {
						//FORMANDO TRAMA PARA SERVICIO MANC_CANCELA
						String trama_manc = "";
						trama_manc += ((DatosMancInter)o).getFchRegistro().substring(0,2) +
									  ((DatosMancInter)o).getFchRegistro().substring(3,5)+
						              ((DatosMancInter)o).getFchRegistro().substring(6,10) + "@";
						trama_manc += ((DatosMancInter)o).getFolio_registro() + "@";
						trama_manc += ses.getContractNumber() + "@";
						trama_manc += convierteUsr8a7(ses.getUserID()) + "@";

	  				//LLAMADO AL MANC_CANCELA
					try {
						hs = tuxGlobal.manc_cancela(trama_manc);
						codError = (String) hs.get("COD_ERROR");
						buffer = (String) hs.get("BUFFER");
					} catch (Exception e) {
						codError = null;
					}

					//VERIFICANDO RESPUESTA DEL MANC_CANCELA
					if (codError.endsWith("0000")) {
						resCodError = "OK";
						desResCodErr = "CANCELADA";
						codError = "MANC0000";
					}
				}else{
					//CASO CONTRARIO REFERENCIA INCORRECTA
					resCodError = "ERROR";
					desResCodErr = "PROBLEMAS AL OBTENER LA REFERENCIA";
					EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: referencia incorrecta::: "+desResCodErr, EIGlobal.NivelLog.INFO);
				}
			}

			if (resCodError == null || resCodError.equals (""))
			{

				estatus = "Error en el servicio";
				folio_auto = "&nbsp;";
				fecha_auto = "&nbsp;";
			}
			else
			{
				estatus = desResCodErr;
				folio_auto = referencia.toString().trim();
				EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: estatus->"+estatus, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("OperacionesManc:: autorizaOperaciones:: folio_auto->"+folio_auto, EIGlobal.NivelLog.INFO);

			}

			if (Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, ses, request.getSession(false));
				BitaTransacBean bt = new BitaTransacBean();
				BitaTCTBean beanTCT = new BitaTCTBean ();
				bt = (BitaTransacBean)bh.llenarBean(bt);

				if(request.getSession()
						.getAttribute(BitaConstants.SESS_ID_FLUJO)
						.equals(BitaConstants.EA_MANCOM_CONS_AUTO))
					bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_REALIZA_OPERACIONES);

				if(request.getSession()
						.getAttribute(BitaConstants.SESS_ID_FLUJO)
						.equals(BitaConstants.EA_MANCOM_OPER_INTER))
					bt.setNumBit(BitaConstants.EA_MANCOM_OPER_INTER_REALIZA_OPERACIONES);

				if(resCodError!=null){
       				 if(resCodError.equals("OK")){
       					   bt.setIdErr(((mx.altec.enlace.bo.DatosMancInter)o).getTipo_operacion ()+"0000");
       				 }else{
       					bt.setIdErr(((mx.altec.enlace.bo.DatosMancInter)o).getTipo_operacion ()+"9999");
       				 }
       				}
				bt.setContrato(ses.getContractNumber());

				if(((mx.altec.enlace.bo.DatosMancInter)o).getCta_origen()!=null&&!((mx.altec.enlace.bo.DatosMancInter)o).getCta_origen().equals(""))
					bt.setCctaOrig(((mx.altec.enlace.bo.DatosMancInter)o).getCta_origen());
				if(((mx.altec.enlace.bo.DatosMancInter)o).getCta_destino()!=null&&!((mx.altec.enlace.bo.DatosMancInter)o).getCta_destino().equals(""))
					bt.setCctaDest(((mx.altec.enlace.bo.DatosMancInter)o).getCta_destino());
				if(((mx.altec.enlace.bo.DatosMancInter)o).getImporteDivisa()!=null&&!((mx.altec.enlace.bo.DatosMancInter)o).getImporteDivisa().equals(""))
					bt.setImporte(Double.parseDouble(((mx.altec.enlace.bo.DatosMancInter)o).getImporteDivisa()));
				//VSWF RRG I 12-Mayo-2008 Se agrego este codigo para agregar el beneficiario a la bitacora
				if(beneficiario!=null && !beneficiario.equals(""))
					bt.setBeneficiario(beneficiario);

				bt.setServTransTux(((mx.altec.enlace.bo.DatosMancInter)o).getTipo_operacion ());

				if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
					&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
							.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(ses.getToken().getSerialNumber());
				}

				try {
					BitaHandler.getInstance().insertBitaTransac(bt);
					//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
					beanTCT = bh.llenarBeanTCT(beanTCT);
					beanTCT.setCuentaOrigen(((mx.altec.enlace.bo.DatosMancInter)o).getCta_origen());
					beanTCT.setCuentaDestinoFondo(((mx.altec.enlace.bo.DatosMancInter)o).getCta_destino());
					beanTCT.setReferencia(referencia);
					beanTCT.setTipoOperacion(((mx.altec.enlace.bo.DatosMancInter)o).getTipo_operacion ());
					beanTCT.setCodError(codError);
					beanTCT.setUsuario(ses.getUserID8());
					if(((DatosMancInter)o).getTipo_operacion().equals("DIPD")){
						if(getDivisaTrx(((mx.altec.enlace.bo.DatosMancInter)o).getCta_origen()).equals("USD")){
							beanTCT.setImporte(Double.parseDouble(((mx.altec.enlace.bo.DatosMancInter)o).getImporteUSD().trim()));}
						else{
							beanTCT.setImporte(Double.parseDouble(((mx.altec.enlace.bo.DatosMancInter)o).getImporteMN().trim()));}
					}else if(((DatosMancInter)o).getTipo_operacion().equals("DITA")){
						beanTCT.setImporte(Double.parseDouble(((mx.altec.enlace.bo.DatosMancInter)o).getImporteDivisa()));
					}
				    BitaHandler.getInstance().insertBitaTCT(beanTCT);
					//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
			}

			request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);

			int i = porProcesar.indexOf (o);


			porProcesar.add (i, new RespManc ((mx.altec.enlace.bo.DatosMancInter) o, fecha_auto, folio_auto, estatus ,orden, rastreo, ses.getUserID()+"  "+ses.getNombreUsuario(), true));
		    java.util.ArrayList listaNotificacion = new java.util.ArrayList();
		    try{
		    	Object elemento = "";
		    	String tipoOper = "";
		    	EIGlobal.mensajePorTrace( "***********Antes de lista de elementos a notificar -->", EIGlobal.NivelLog.INFO);
		    	String numRefOper,estatusOper,oper,ctaCargoOper,importeOper;

		    	numRefOper = (folio_auto == null ? "" : folio_auto);
		    	estatusOper = (estatus == null ? "" : estatus);
		    	oper = (((DatosMancInter)o).getTipo_operacion() == null ? "" : ((DatosMancInter)o).getTipo_operacion());
		    	ctaCargoOper = (((DatosMancInter)o).getCta_origen() == null ? "" : ((DatosMancInter)o).getCta_origen());
		    	EIGlobal.mensajePorTrace( "*********YHG Validando divisa*(((DatosMancInter)o).getImporteDivisa() ->" + ((DatosMancInter)o).getImporteDivisa(), EIGlobal.NivelLog.DEBUG);
		    	EIGlobal.mensajePorTrace( "*********YHG Validando divisa*(((DatosMancInter)o).getDivisaAbono() ->" + ((DatosMancInter)o).getDivisaAbono(), EIGlobal.NivelLog.DEBUG);
		    	EIGlobal.mensajePorTrace( "*********YHG Validando divisa*(((DatosMancInter)o).getDivisaCargo() ->" + ((DatosMancInter)o).getDivisaCargo(), EIGlobal.NivelLog.DEBUG);
		    	EIGlobal.mensajePorTrace( "*********YHG Validando divisa*(((DatosMancInter)o).todo() ->" + ((DatosMancInter)o).toString(), EIGlobal.NivelLog.DEBUG);

		    	if(((DatosMancInter)o).getDivisaAbono()!=null && (((DatosMancInter)o).getDivisaAbono().equals("MXN")))
		    		{importeOper = (((DatosMancInter)o).getImporteMN() == null ? "" : ((DatosMancInter)o).getImporteMN());

		    		}

		    	else{
		    		importeOper = (((DatosMancInter)o).getImporteUSD() == null ? "" : ((DatosMancInter)o).getImporteUSD());

		    		}




		    	elemento = "/" + cod_error + "/" + numRefOper + "/" + estatusOper + "/" + oper + "/" + ctaCargoOper + "/" + importeOper +  "/"+((DatosMancInter)o).getDivisaAbono();

		    	EIGlobal.mensajePorTrace( "***********Nuevo elemento a notificar ->" + elemento.toString(), EIGlobal.NivelLog.INFO);
	    		listaNotificacion = (java.util.ArrayList) request.getSession().getAttribute ("datosNotificacion");
	    		EIGlobal.mensajePorTrace( "***********Se obtuvo lista de elementos de session ->", EIGlobal.NivelLog.INFO);
	    		listaNotificacion.add(elemento);
	    		request.getSession().setAttribute ("datosNotificacion", listaNotificacion);
				if (request.getParameter("TipoOp") != null && !request.getParameter("TipoOp").toString().equals("")){
					if (request.getParameter("TipoOp").toString().trim().equals("1")) {
						EIGlobal.mensajePorTrace("*******************se actualizo con Autorizaci---->  ", EIGlobal.NivelLog.INFO);
						tipoOper = "Autorizaci"+ (char)243 +"n";
					} else {
						EIGlobal.mensajePorTrace("*******************se actualizo con Cancelaci---->  ", EIGlobal.NivelLog.INFO);
						tipoOper = "Cancelaci"+ (char)243 +"n";
					}
					request.getSession().setAttribute ("tipoOper", tipoOper);
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			porProcesar.remove (o);

			ses.setBandManc (String.valueOf (porProcesar.size ())) ;


			String banderaManc = "";
			banderaManc = ses.getBandManc ();


			if (i + 1 == porProcesar.size ())
			{
				request.getSession ().removeAttribute ("Pendientes");
				request.getSession ().removeAttribute ("Autoriza");
				request.getSession ().removeAttribute ("ses.getBandManc ()");

				try{
					request.getSession ().removeAttribute ("datosNotificacion");
					String tipoOperacion ="";
					if (request.getSession().getAttribute ("tipoOper") != null){
						tipoOperacion = request.getSession().getAttribute ("tipoOper").toString();
					}
					request.getSession ().removeAttribute ("tipoOper");
				  EIGlobal.mensajePorTrace("***********Se borro lista de notificacion de session -->", EIGlobal.NivelLog.INFO);

					BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
					EmailSender sender = new EmailSender();
					String codErrorOper = "";
					double importeUsd = 0.0;
					double importeMn = 0.0;
					boolean enviaNotificacion=false;
					String codErrUnico="";
					int numRegImportados=0;
					String importeTotalUSD = "";
					String importeTotalMN = "";
					String divisa="";
					//obtiene los codigos de error
					for (int j = 1; j < listaNotificacion.size(); j++) {
							codErrUnico= ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 2);
							EIGlobal.mensajePorTrace("----codErrUnico2-->" + codErrUnico, EIGlobal.NivelLog.DEBUG);
							codErrorOper=codErrorOper+codErrUnico+ "-" ;

						if(codErrUnico!=null && codErrUnico.contains("0000")){
							numRegImportados++;
							enviaNotificacion=true;
							divisa=ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 8);
							if(divisa!=null){divisa=divisa.trim();}
							else {divisa="";}

							if(divisa.equals("MXN")){
									importeMn = importeMn + Double.parseDouble(ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 7));}
							else{
								importeUsd = importeMn + Double.parseDouble(ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 7));}
						}


					}
					if(importeUsd>0)
					importeTotalUSD = ValidaOTP.formatoNumero(importeUsd);
					if(importeMn>0)
					importeTotalMN = ValidaOTP.formatoNumero(importeMn);
					EIGlobal.mensajePorTrace("----codErrorOper-->" + codErrorOper, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("----importeTotalUSD-->" + importeTotalUSD, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("----importeTotalMN-->" + importeTotalMN, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("----divisa-->" + divisa, EIGlobal.NivelLog.DEBUG);
					if(enviaNotificacion){
						EmailDetails details = new EmailDetails();

						details.setNumeroContrato(session.getContractNumber());
						details.setRazonSocial(session.getNombreContrato());
						details.setImpTotalUSD(importeTotalUSD);
						details.setImpTotalMXN(importeTotalMN);
						details.setNumRegImportados(numRegImportados);
						details.setTipo_operacion(tipoOperacion);

						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> num contrato   : " + details.getNumeroContrato(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> nom contrato   : " + details.getRazonSocial(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> importeTotalUSD    : " + details.getImpTotalUSD(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> importeTotalMN    : " + details.getImpTotalMXN(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> num regImp     : " + details.getNumRegImportados(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> tipo operacion     : " + details.getTipo_operacion(), EIGlobal.NivelLog.DEBUG);

						sender.sendNotificacion(request,IEnlace.NOT_AUT_CANC_OPER_MANC_INTER, details,listaNotificacion);
					}
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

			}

			++i;

			request.setAttribute("banderaManc",ses.getBandManc ());
		 {

		}
			request.getRequestDispatcher ("/jsp/ResMancomunidadInter.jsp").forward (request, response);
		}
    }

  //VSWF RRG I 12-Mayo-2008 ALENTREBBIA Metodo posCar que devuelve la posicion de un caracter en un String

	/**
   * Pos car.
   *
   * @param cad the cad
   * @param car the car
   * @param cant the cant
   * @return the int
   */
	public int posCar(String cad, char car, int cant) {
        int result = 0, pos = 0;
        for (int i = 0; i < cant; i++) {
            result = cad.indexOf(car, pos);
            pos = result + 1;
        }
        return result;
    }
  //VSWF RRG F 12-Mayo-2008
   /**
   *  Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException the servlet exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /**
     *  Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /**
     *  Returns a short description of the servlet.
     *
     * @return the servlet info
     */
    public String getServletInfo () {
	return "Short description";
    }

	/**
	 * Metodo para formatear el importe que necesita el servicio MANC_VALIDA
	 *
	 * @param imp		Cadena de importe a formatear
	 * @return			Cadena a dos digitos
	 */
	private String formatoImporte(String imp){
		EIGlobal.mensajePorTrace("OperacionesManc::formatoImporte:: cantidad recibida->" + imp, EIGlobal.NivelLog.INFO);
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale (language,  country);
		NumberFormat nf = NumberFormat.getInstance(local);
		EIGlobal.mensajePorTrace("OperacionesManc::formatoImporte:: cantidad formateada->" + nf.format(new Double (imp).doubleValue ()).replace(",",""), EIGlobal.NivelLog.INFO);
		imp = nf.format(new Double (imp).doubleValue ()).replace(",","");
		int dec = 0;
		if(imp.indexOf(".")>0){
			EIGlobal.mensajePorTrace("OperacionesManc::formatoImporte:: Longitud cantidad->" + imp.length(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("OperacionesManc::formatoImporte:: Caracter punto->" + imp.indexOf("."), EIGlobal.NivelLog.DEBUG);
			dec = imp.length() - (imp.indexOf(".")+1);
			EIGlobal.mensajePorTrace("OperacionesManc::formatoImporte:: Numero de decimales->" + dec, EIGlobal.NivelLog.DEBUG);
			if(dec>=2){
				return imp.substring(0, imp.indexOf(".")) + imp.substring(imp.indexOf("."), imp.indexOf(".")+3);}
			else{
				for(int i=0; i<dec; i++){
					imp += "0";}
			}
		}else{
			imp += ".00";
		}
		return imp;
	}

	/**
	 * Metodo para obtener el error de salida del servicio MANC_VALIDA
	 * cuando este no es exitoso
	 *
	 * @param codError		Codigo de Error de salida
	 * @return				Descripci�n del Error
	 */
	private String getDesErrManc(String codError){
		if(codError!=null){
			if(codError.endsWith("0001")){
				return "No existe cuenta en mancomunidad";
			}else if(codError.endsWith("0002")){
				return "No existe tipo operacion en mancomunidad";
			}else if(codError.endsWith("0003")){
				return "No existe usuario en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0004")){
				return "No pudo abrir archivo en mancomunidad";
			}else if(codError.endsWith("0005")){
				return "Usuario tipo A en mancomunidad";
			}else if(codError.endsWith("0006")){
				return "Tipo de operacion no mancomunada";
			}else if(codError.endsWith("0007")){
				return "Tipo firma no autorizada en mancomunidad";
			}else if(codError.endsWith("0008")){
				return "Diferente importe en mancomunidad";
			}else if(codError.endsWith("0009")){
				return "Diferente tipo de cuenta de cargo en mancomunidad - Genere nuevo folio";
			}else if(codError.endsWith("0010")){
				return "No existe folio en mancomunidad-Verifique que sus datos sean correctos";
			}else if(codError.endsWith("0011")){
				return "Contrato-tipo de operacion no mancomunado";
			}else if(codError.endsWith("0012")){
				return "Rechazada";
			}else if(codError.endsWith("0013")){
				return "No hay registros";
			}else if(codError.endsWith("0014")){
				return "Usuarios iguales en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0015")){
				return "Diferente tipo de operacion en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0016")){
				return "Contrato no mancomunado";
			}else if(codError.endsWith("0017")){
				return "Folio no permitido en mancomunidad - Genere un nuevo folio";
			}else if(codError.endsWith("0018")){
				return "Suma de montos autorizados menor a importe - Genere un nuevo folio";
			}else if(codError.endsWith("0019")){
				return "No se permite la operacion con cuenta en dolares";
			}else if(codError.endsWith("0024")){
				return "Usuarios iguales en mancomunidad por producto o general";
			}else if(codError.endsWith("9999")){
				return "Servicio de mancomunidad no disponible (No se tiene conexion con la BD)";
			}
		}
	return "";
	}

	/**
	 * Metodo para obtener la divisa de la cuenta.
	 *
	 * @param cuenta the cuenta
	 * @return Divisa
	 */
	private String getDivisaTrx(String cuenta){
		String prefijo = "";
		try{
			prefijo = cuenta.substring(0,2);
			if(prefijo.trim().equals("09") || prefijo.trim().equals("42") ||
			   prefijo.trim().equals("49") || prefijo.trim().equals("82") ||
		       prefijo.trim().equals("83") || prefijo.trim().equals("97") ||
		       prefijo.trim().equals("98")){
				return "USD";
		    }else if(prefijo.trim().equals("08") || prefijo.trim().equals("10") ||
					   prefijo.trim().equals("11") || prefijo.trim().equals("13") ||
					   prefijo.trim().equals("15") || prefijo.trim().equals("16") ||
					   prefijo.trim().equals("17") || prefijo.trim().equals("18") ||
					   prefijo.trim().equals("19") || prefijo.trim().equals("20") ||
					   prefijo.trim().equals("21") || prefijo.trim().equals("22") ||
					   prefijo.trim().equals("23") || prefijo.trim().equals("24") ||
					   prefijo.trim().equals("25") || prefijo.trim().equals("26") ||
					   prefijo.trim().equals("27") || prefijo.trim().equals("28") ||
					   prefijo.trim().equals("40") || prefijo.trim().equals("43") ||
					   prefijo.trim().equals("44") || prefijo.trim().equals("45") ||
					   prefijo.trim().equals("46") || prefijo.trim().equals("47") ||
					   prefijo.trim().equals("48") || prefijo.trim().equals("50") ||
					   prefijo.trim().equals("51") || prefijo.trim().equals("52") ||
					   prefijo.trim().equals("53") || prefijo.trim().equals("54") ||
					   prefijo.trim().equals("55") || prefijo.trim().equals("56") ||
					   prefijo.trim().equals("57") || prefijo.trim().equals("58") ||
					   prefijo.trim().equals("59") || prefijo.trim().equals("60") ||
					   prefijo.trim().equals("61") || prefijo.trim().equals("62") ||
					   prefijo.trim().equals("63") || prefijo.trim().equals("64") ||
					   prefijo.trim().equals("65") || prefijo.trim().equals("66") ||
					   prefijo.trim().equals("67") || prefijo.trim().equals("68") ||
					   prefijo.trim().equals("69") || prefijo.trim().equals("70") ||
					   prefijo.trim().equals("71") || prefijo.trim().equals("72") ||
					   prefijo.trim().equals("73") || prefijo.trim().equals("74") ||
					   prefijo.trim().equals("75") || prefijo.trim().equals("76") ||
					   prefijo.trim().equals("77") || prefijo.trim().equals("78") ||
					   prefijo.trim().equals("79") || prefijo.trim().equals("80") ||
					   prefijo.trim().equals("81") || prefijo.trim().equals("85") ||
					   prefijo.trim().equals("86") || prefijo.trim().equals("87") ||
					   prefijo.trim().equals("88") || prefijo.trim().equals("89") ||
					   prefijo.trim().equals("90") || prefijo.trim().equals("91") ||
					   prefijo.trim().equals("92") || prefijo.trim().equals("93") ||
					   prefijo.trim().equals("94") || prefijo.trim().equals("95") ||
					   prefijo.trim().equals("96") || prefijo.trim().equals("97") ||
					   prefijo.trim().equals("99")){
		    	return "MXP";
		    }
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("MIPD_cambios::getDivisaTrx:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		return "USD";
	}

	/**
	 * Metodo para formatear cantidades a 6 decimales
	 *
	 * @param imp		Cantidad a formatear
	 * @return			Cantidad formateada
	 */
	private String formatoImporte6(String imp){
		EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte6:: cantidad recibida->" + imp, EIGlobal.NivelLog.INFO);
		int dec = 0;
		if(imp.indexOf(".")>0){
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte6:: Longitud cantidad->" + imp.length(), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte6:: Caracter punto->" + imp.indexOf("."), EIGlobal.NivelLog.DEBUG);
			dec = imp.length() - (imp.indexOf(".")+1);
			EIGlobal.mensajePorTrace("MIPD_Cambios::formatoImporte6:: Numero de decimales->" + dec, EIGlobal.NivelLog.DEBUG);
			if(dec>=6)
				return imp.substring(0, imp.indexOf(".")) + imp.substring(imp.indexOf("."), imp.indexOf(".")+7);
			else{
				for(int i=0; i<(6-dec); i++)
					imp += "0";
			}
		}else{
			imp += ".000000";
		}
		return imp;
	}

}