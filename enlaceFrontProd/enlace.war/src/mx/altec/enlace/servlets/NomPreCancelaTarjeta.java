package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1E;
import mx.altec.enlace.beans.NomPreLM1D.Relacion;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.dao.NomPreTarjetaDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import static mx.altec.enlace.utilerias.NomPreUtil.logInfo;

/**
 * @author bmesquitillo
 * Fecha: 17/junio/2009
 * proyecto: 200813700 Nomina Prepago
 * Descripcion: Servlet para operaciones Cancelacion y Bloque de tarjeta de prepago.
 */
 public class NomPreCancelaTarjeta extends BaseServlet implements javax.servlet.Servlet {

	private final static String CANC_TAR_INICIA="inicia";
	private final static String CANC_TAR_CONS="consulta";
	private final static String CANC_TAR_EJECUTA="ejecuta";
	private final static String OPER_BLOQUEO="bloqueo";
	private final static String OPER_CANCELA="cancela";


	public NomPreCancelaTarjeta() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}
	protected void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("NomPreCancelaTarjeta.class Entra a defaultAction", EIGlobal.NivelLog.INFO);
		String accion = request.getParameter("opcion");
		accion = accion == null ||  accion.length() == 0? CANC_TAR_INICIA : accion;
		String operacion=(String)request.getParameter("operacionNP");
		if(operacion!=null &&  operacion.length() > 0){
			request.getSession().setAttribute("operNP", operacion);
		}
		operacion=(String)request.getSession().getAttribute("operNP");

		operacion=operacion==null||operacion.length()==0?OPER_BLOQUEO:operacion;

		if(SesionValida(request, response))
		{
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			String strMenu =  (session.getstrMenu());
			strMenu =  (strMenu != null ? strMenu : "");
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			//CGH Valida servicio de nomina
			if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				if(OPER_BLOQUEO.equals(operacion.trim())) {
					request.setAttribute("Encabezado", CreaEncabezado("Bloqueo de tarjeta", "Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Bloqueo","s25320h",request));//YHG NPRE
				} else {
					request.setAttribute("Encabezado", CreaEncabezado("Baja de tarjeta", "Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Baja","s25320h",request));//YHG NPRE
				}
				return;
			}

			if(operacion.trim().equals(OPER_BLOQUEO)){
				if(accion.trim().equals(CANC_TAR_INICIA)){
					request.setAttribute("Encabezado", CreaEncabezado( "Bloqueo de tarjeta","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Bloqueo","s25320h",request));//YHG NPRE
					request.setAttribute("titTabla", "Bloqueo de Tarjeta:");
					inicia(request, response,OPER_BLOQUEO);
				}else if(accion.trim().equals(CANC_TAR_CONS)){
					request.setAttribute("Encabezado", CreaEncabezado( "Bloqueo de tarjeta - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Bloqueo","s25320h",request));//YHG NPRE
					consulta(request, response,session,OPER_BLOQUEO);
				}else if(accion.trim().equals(CANC_TAR_EJECUTA)){
					request.setAttribute("Encabezado", CreaEncabezado( "Bloqueo de tarjeta - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Bloqueo","s25320h",request));//YHG NPRE
					ejecuta(request, response,session,OPER_BLOQUEO);


				}
			}else{
				if(accion.trim().equals(CANC_TAR_INICIA))
				{
					request.setAttribute("Encabezado", CreaEncabezado( "Baja de tarjeta","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Baja","s25320h",request));//YHG NPRE
					request.setAttribute("titTabla", "Baja de Tarjeta:");
					inicia(request, response,OPER_CANCELA);
				}else if(accion.trim().equals(CANC_TAR_CONS)){
					request.setAttribute("Encabezado", CreaEncabezado( "Baja de tarjeta - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Baja","s25320h",request));//YHG NPRE
					consulta(request, response,session,OPER_CANCELA);
				}else if(accion.trim().equals(CANC_TAR_EJECUTA)){
					request.setAttribute("Encabezado", CreaEncabezado( "Baja de tarjeta - confirmaci&oacute;n","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Baja","s25320h",request));//YHG NPRE
					ejecuta(request, response,session,OPER_CANCELA);
				}
			}

		}else
			{
				request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response);
			}
	}

	/**@author Berenice Mesquitillo
	 * Metodo inicial, carja el jsp inicial ya sea para la cancelacion o bloqueo de tarjeta
	 * @param request
	 * @param response
	 * Fecha: 22-06-2009
	 */
	private void inicia(final HttpServletRequest request, HttpServletResponse response,String operacion){
		try {

			BitaTransacBean bt = new BitaTransacBean();
			logInfo("id flujo="+(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)));


			if(operacion.equals(OPER_CANCELA)){
    			bt.setNumBit(BitaConstants.ES_NP_TARJETA_BAJA_ENTRA);
    		}else{
    				bt.setNumBit(BitaConstants.ES_NP_TARJETA_BLOQUEO_ENTRA);
    		}
			NomPreUtil.bitacoriza(request,null,null,true, bt);
			evalTemplate(IEnlace.NOMPRE_CANCELA_TARJETA, request, response);

		} catch (ServletException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author Berenice Mesquitillo
	 * Metodo donde se consultan los datos de la tarjeta que se pretende cancelar o bloquear.
	 * la informacion obtenida es mostrada al usuario.
	 * @param request
	 * @param response
	 * Fecha: 23-06-2009
	 */
	private void consulta(final HttpServletRequest request, HttpServletResponse response,BaseResource session,String operacion){

		String numTarjeta=request.getParameter("tarjeta");
		String auxEstadoBit = "";
		String codOperacion = "";
		if(numTarjeta!=null){
			numTarjeta=numTarjeta.trim();
		}
		try {

			NomPreLM1D lm1d = consultarLM1D(session, numTarjeta);

			if (lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0) {

				Relacion relacion = lm1d.getDetalle().get(0);

				request.setAttribute("numempleado", relacion.getEmpleado().getNoEmpleado());
				request.setAttribute("nomempleado", relacion.getEmpleado().getNombre()+" "+
						relacion.getEmpleado().getPaterno()+" "+ relacion.getEmpleado().getMaterno());
				request.setAttribute("numtarjeta", relacion.getTarjeta().getNoTarjeta());
				request.setAttribute("edotarjeta", relacion.getTarjeta().getDescEstadoTarjeta());
				request.setAttribute("operconfirma","consulta");
				auxEstadoBit = relacion.getTarjeta().getDescEstadoTarjeta();
				codOperacion = lm1d.getCodigoOperacion();

			}else{

				String cuadroDialogo="cuadroDialogo(\"No se encontr&oacute; informaci&oacute;n para la tarjeta: "+numTarjeta+"\", 1)";
				request.setAttribute("mensajecancela", cuadroDialogo);

			}
			final String estadoBit= auxEstadoBit.trim();

			BitaTransacBean bt = new BitaTransacBean();
			if(operacion.equals(OPER_CANCELA)){
    			bt.setNumBit(BitaConstants.ES_NP_TARJETA_BAJA_CONSULTA);
    		}else{
    				bt.setNumBit(BitaConstants.ES_NP_TARJETA_BLOQUEO_CONSULTA);
    		}
			if(estadoBit!=null && estadoBit.length()>0)
				bt.setEstatus(estadoBit.substring(0, 1));
			NomPreUtil.bitacoriza(request,"LM1D", codOperacion, bt);


			evalTemplate(IEnlace.NOMPRE_CANCELA_TARJETA_CONFIRMA, request, response);

		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "consultaBloqueo" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author Berenice Mesquitillo
	 * Metodo que realiza las operaciones bloqueo o cancelacion de tarjeta
	 * @param request
	 * @param response
	 * @param operacion (cancelacion o bloqueo)
	 * Fecha:23-06-2009
	 */
	private void ejecuta(final HttpServletRequest request, HttpServletResponse response,BaseResource session,String operacion){
		try {
			String numTarjeta=request.getParameter("tarjeta");
			String numEmpleado=request.getParameter("numEmp");
			String nomEmpleado=request.getParameter("nomEmp");
			String estatus=request.getParameter("estatus");
			String cuadroDialogo=null;
			String operConfirma=null;
			String opertct="";
			NomPreTarjetaDAO prepagoDAO=new NomPreTarjetaDAO();
			NomPreLM1E beanLM1E;
			if(operacion.trim().equals("cancela")){
				beanLM1E=prepagoDAO.cancelaTarjeta(session.getContractNumber(), numTarjeta, numEmpleado);

				if(beanLM1E!=null && beanLM1E.isCodExito()){
					cuadroDialogo="cuadroDialogo(\"La tarjeta: "+numTarjeta+" ha sido cancelada\", 1)";
				}else{
					cuadroDialogo="cuadroDialogo(\"No se ha podido realizar la cancelaci&oacute;n de la tarjeta: "+numTarjeta+"\", 1)";
				}
			}else{
				beanLM1E=prepagoDAO.bloqueaTarjeta(session.getContractNumber(), numTarjeta, numEmpleado);
				if(beanLM1E!=null && beanLM1E.isCodExito()){
					cuadroDialogo="cuadroDialogo(\"La tarjeta: "+numTarjeta+" ha sido bloqueada. �Desea realizar la reposici&oacute;n de esta tarjeta?\", 2)";
				}else{
					cuadroDialogo="cuadroDialogo(\"" + (beanLM1E == null ? "Servicio no disponible" : beanLM1E.getMensError())+"\", 1)";
				}
			}

			if(beanLM1E!=null && beanLM1E.isCodExito()){
				NomPreLM1D lm1d = consultarLM1D(session, numTarjeta);
				if (lm1d != null && lm1d.getDetalle() != null && lm1d.getDetalle().size() > 0){
					Relacion relacion = lm1d.getDetalle().get(0);
					estatus=relacion.getTarjeta().getDescEstadoTarjeta();
				}
				operConfirma="ejecutaOK";
			}

			final String edoBit=estatus;

			BitaTransacBean bt = new BitaTransacBean();
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			String codErrBit=" ";
			if(operacion.equals(OPER_CANCELA)){
    			bt.setNumBit(BitaConstants.ES_NP_TARJETA_BAJA_CONFIRMA);
    			opertct=NomPreUtil.ES_NP_TARJETA_BAJA_OPER;
    		}else{
    				bt.setNumBit(BitaConstants.ES_NP_TARJETA_BLOQUEO_CONFIRMA);
    				opertct=NomPreUtil.ES_NP_TARJETA_BLOQUEO_OPER;
    		}
			if(edoBit!=null)
				bt.setEstatus(edoBit.substring(0,1));

			if(beanLM1E!=null && beanLM1E.getCodigoOperacion()!=null && !beanLM1E.getCodigoOperacion().trim().equals("")){
				codErrBit=beanLM1E.getCodigoOperacion();
			}
			NomPreUtil.bitacoriza(request,"LM1E",codErrBit, bt);
			if(beanLM1E!=null && beanLM1E.isCodExito()){
				try
				{
					String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID8()," ",0.0,opertct," ",0," ");
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
				}
			}
			request.setAttribute("numempleado",numEmpleado);
			request.setAttribute("nomempleado",nomEmpleado);
			request.setAttribute("numtarjeta",numTarjeta);
			request.setAttribute("edotarjeta",estatus);
			request.setAttribute("operconfirma",operConfirma);
			request.setAttribute("mensajecancela",cuadroDialogo);

			evalTemplate(IEnlace.NOMPRE_CANCELA_TARJETA_CONFIRMA, request, response);
		} catch (ServletException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}
	private NomPreLM1D consultarLM1D(BaseResource session, String numTarjeta) {
		NomPreLM1D lm1d = new NomPreBusquedaAction().obtenRelacionTarjetaEmpleado(session.getContractNumber(), numTarjeta, null, null);
		return lm1d;
	}

}