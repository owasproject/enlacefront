/*Banco Santander Mexicano
  Clase MPagoImpuestos clase que contiene funciones comunes para CuentasImpuestos y PagosImpuestos
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 01/03/2001 - Quitar codigo muerto
 */

package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class MPagoImpuestos extends BaseServlet{
	final String  pagoimpuestosok="PIMP0000";

	protected String llamada_servicio_consulta_cuentas_pi(int opcion, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String coderror = "";
		String trama_entrada = "";
		String mensaje = null;
		String archivoe = IEnlace.REMOTO_TMP_DIR + "/" + session.getUserID();
		String archivol = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID();


		//TuxedoGlobal tuxGlobal = new TuxedoGlobal(this);

		trama_entrada = "2TCT|" + session.getUserID() + "|ACIM|" + session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@";
		try{
			//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

			Hashtable hs = tuxGlobal.web_red( trama_entrada );
			EIGlobal.mensajePorTrace("***CuentasImpuestos WEB_RED-ACIM>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
			coderror = (String) hs.get("BUFFER");
			EIGlobal.mensajePorTrace("***CuentasImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re){
			re.printStackTrace();
		} catch (Exception e) {}


		if(  (coderror.equals(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID())) ||
			(coderror.equals(IEnlace.REMOTO_TMP_DIR + "/"+ session.getUserID()))){
			///////////////////////////
			ArchivoRemoto archR = new ArchivoRemoto();
			if(!archR.copia(session.getUserID()))
				EIGlobal.mensajePorTrace("***CuentasImpuestos.class No se pudo archivo remoto:consultacuentaspi:"+IEnlace.REMOTO_TMP_DIR+session.getUserID(), EIGlobal.NivelLog.INFO);
			else
				EIGlobal.mensajePorTrace("***CuentasImpuestos.class archivo remoto copiado:consultacuentaspi:"+IEnlace.REMOTO_TMP_DIR+session.getUserID(), EIGlobal.NivelLog.INFO);

			mensaje = lecturactaspi(archivol,opcion, req, res);
		}
		return mensaje;
    }


    //Lectura del archivo donde vienen las cuentas dadas de alta
	//para el pago de impuestos
	protected String lecturactaspi(String archivo,int opcion, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{
		EIGlobal.mensajePorTrace("***MPagoImpuestos.class Entrando a lecturactaspi", EIGlobal.NivelLog.INFO);
		boolean primerlinea = false;
		String salida = "";
		String linea = "";
		String cuenta = "";
		int cont = 0;
		String[] arrcuentas = null;
		int residuo = 0;
		int indice = 0;
		String clase = "";
		try{
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			if (opcion ==0){
				salida = "<table width=500 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
				salida = salida+"<tr><td align=center class=tittabdat width=74>Seleccione</td><td width=100 class=tittabdat align=center>Cuenta</td><td class=tittabdat align=center width=320>Descripci&oacute;n</td></tr>";
			}else
				if (opcion==1)
					salida = "<OPTION VALUE=-1>-----------</OPTON>";
				else
					if (opcion==2)
						salida = "";

			while ((linea = lector.readLine())!=null){
				if( linea.trim().equals(""))
					linea = lector.readLine();
				if( linea == null )
					break;
				if (primerlinea==false)
					primerlinea = true;
				else{
					indice++;
					cuenta = linea.substring(0,linea.indexOf(";"));
					if(opcion == 0){

						String nombrecuenta = linea.substring(linea.indexOf(";")+1,linea.length());
						nombrecuenta = nombrecuenta.substring(0,nombrecuenta.indexOf(";"));
						residuo = indice%2;
						if (residuo == 0)
							clase="textabdatobs";
						else
							clase = "textabdatcla";

						if (indice == 1)
							salida = salida+"<TR><TD class="+clase+" align=center width=74><INPUT TYPE=radio NAME=cuenta VALUE="+cuenta+"  checked></TD><TD class="+clase+" nowrap align=center width=80>"+cuenta+"</TD><TD class="+clase+" width=320>"+nombrecuenta+"</TD></TR>";
						else
							salida = salida+"<TR><TD class="+clase+" align=center width=74><INPUT TYPE=radio NAME=cuenta VALUE="+cuenta+"></TD><TD class="+clase+" nowrap align=center width=80>"+cuenta+"</TD><TD class="+clase+" width=320>"+nombrecuenta+"</TD></TR>";
					}else
						if (opcion == 1)
							salida = salida+"<OPTION VALUE=\""+linea+"\">"+cuenta+"</OPTON>";
						else
							if (opcion==2)
								salida += linea+"@";
					cont++;
				}
			}
			if (opcion == 0)
				salida = salida+"</TABLE>";

			lector.close();
			if(cont == 0){
				despliegaPaginaError("El contrato no tiene cuentas asociadas para Pago de Impuestos","Impuestos  Federales","",req,res);
				salida = null;
			}
		}catch(IOException e){
			EIGlobal.mensajePorTrace("***MPagoImpuestos.class "+e, EIGlobal.NivelLog.ERROR);
			despliegaPaginaError("No se posible leer el archivo de cuentas en este momento","Impuestos  Federales","",req, res);
			salida = null;
		}
		return salida;
    }

    //Manda los mensajes correspondientes al template
	protected int operacionrealizada(String mensaje1,String mensaje2,String mensaje3,String mensaje4, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{

	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

       EIGlobal.mensajePorTrace("***MPagoImpuestos.class Entrando  operacionrealizada", EIGlobal.NivelLog.INFO);
       int salida;

	   HttpSession ses = req.getSession();

		if(ses.getAttribute("mensaje_salida")!=null)
			ses.removeAttribute("mensaje_salida");
		if(ses.getAttribute("fecha_hoy")!=null)
			ses.removeAttribute("fecha_hoy");
		if(ses.getAttribute("MenuPrincipal")!=null)
			ses.removeAttribute("MenuPrincipal");
		if(ses.getAttribute("newMenu")!=null)
			ses.removeAttribute("newMenu");
		if(ses.getAttribute("Encabezado")!=null)
			ses.removeAttribute("Encabezado");

	   ses.setAttribute("mensaje_salida",mensaje2);
       ses.setAttribute("fecha_hoy",ObtenFecha());
       ses.setAttribute("MenuPrincipal", session.getStrMenu());
       ses.setAttribute("newMenu", session.getFuncionesDeMenu());
       ses.setAttribute("Encabezado", CreaEncabezado(mensaje1,mensaje3,mensaje4, req));
       ses.setAttribute("web_application",Global.WEB_APPLICATION);
	   ses.setAttribute("banco",session.getClaveBanco());


       //evalTemplate(IEnlace.PIOPER_REAL_TMPL, req, res);
	   //<vswf:meg cambio de NASApp por Enlace 08122008>
	   res.sendRedirect(res.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+IEnlace.PIOPER_REAL_TMPL));

       return 0;
    }

    //Crea componentes hidden que contienen el dia,mes y anio
    String poner_dia_mes_anio(){
      String campos_fecha = "";
      String dia_forma = ObtenDia();
      String mes_forma = ObtenMes();
      String anio_forma = ObtenAnio();
      campos_fecha = "<INPUT ID=dia TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
      campos_fecha = campos_fecha+"<INPUT ID=mes TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
      campos_fecha = campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";
      return campos_fecha;
    }

    //pone los campos correspondientes de acuerdo a si se tiene facultad de operaciones programadas o no
    //Esta funcion es diferente a la definida en el BaseAppLogic ya que se le pasan mas variables ....
	String poner_campos_fecha(boolean fac_programadas,String nombre_campo,String funcion){
		String dia_forma = ObtenDia();
		String mes_forma = ObtenMes();
		String anio_forma = ObtenAnio();
		String campos_fecha = "";
		if (fac_programadas == true){
			campos_fecha = campos_fecha+"<INPUT ID="+nombre_campo+" TYPE=TEXT SIZE=12 class=tabmovtex NAME="+nombre_campo+" VALUE="+dia_forma+"/"+mes_forma+"/"+anio_forma+" OnFocus=\"blur();\">";
			campos_fecha = campos_fecha+"<A HREF=\"javascript:"+funcion+"();\"><IMG  src=\"/gifs/EnlaceMig/gbo25410.gif\" width=\"12\" height=\"14\" border=\"0\" align=\"absmiddle\" ></A>";
		}else{
			campos_fecha = dia_forma+"/"+mes_forma+"/"+anio_forma;
			campos_fecha = campos_fecha+"<INPUT ID=dia  TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
			campos_fecha = campos_fecha+"<INPUT ID=mes  TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
			campos_fecha = campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";
		}
		return campos_fecha;
    }
    static protected String _defaultTemplate = "EnlaceMig/MPagoImpuestos";
}


