package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.FormatosMoneda;
import mx.altec.enlace.bo.MDI_Importar;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceLynxConstants;
import mx.altec.enlace.utilerias.EnlaceLynxUtils;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.TransferenciasMonitorPlusUtils;
import mx.isban.conector.lynx.ConectorLynx;
import mx.isban.conector.lynx.dto.RespuestaLynxDTO;
import mx.isban.conector.lynx.util.LynxConstants;
//VSWF


/**
* Modific&oacute; Hugo Ru&iacute;z Zepeda.
*<P>
* 03/09/2002: Se cambi&oacute; el tama&ndilde;o del campo de <I>importe</I> de abono de 10 a 15 posiciones.<BR>
* 04/09/2002: Se cambi&oacute; el tama&ntilde; de los campos de <I>cargo</I>.   Se modific&oacute; el dise&ntilde;o de la p&aacute;gina.<BR>
* 05/09/2002: Se verific&oacute; que no pierda la suma de <I>cargoTotal</I> al traer una nueva cuenta.<BR>
* </P>
*/

/**Praxis
*Q05-0001087
*ivn Ma. Isabel Valencia Navarrete
*25/01/2005
*CUANDO SE REALIZA UNA TRANSFERENCIA PINTAR EN EL LOG FECHA, HR,
*DIRECCI�N IP(DE DONDE FUE REALIZADA LA TRANSFERENCIA)
*EJEMPLO:
*[20/Jan/2005 12:02:03] DIRECCI�N IP  TRAMA
*/

public class TransferenciasMultiples extends BaseServlet{
		/**
		 * The suc_opera
		 * */
        private static short  suc_opera                        =(short)787;
        /**
		 * The strdest
		 * */
        private static String strdest                           = "";
        /**
		 * The strorig
		 * */
        private static String strorig                           = "";
        /**
		 * The strInhabiles
		 * */
        private static String strInhabiles         = "";
        /**
		 * Constante TRAN
		 * */
    	private static final String TRAN = "TRAN";
    	/**
		 * Constante CADENA_VACIA
		 * */
    	private static final String CADENA_VACIA = "";
        /**
		 * The tipo_operacion
		 * */
        private String tipo_operacion= "";
        
    	/**
    	 * M�todo que responde a peticiones GET
    	 * @param req instancia de HTTPServletRequest
    	 * @param res instancia de HTTPServletResponse
    	 * @throws ServletException error en el servlet
    	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
    	 * */
        public void doGet( HttpServletRequest req, HttpServletResponse res )
                        throws ServletException, IOException{
                defaultAction( req, res );
        }

    	/**
    	 * M�todo que responde a peticiones POST
    	 * @param req instancia de HTTPServletRequest
    	 * @param res instancia de HTTPServletResponse
    	 * @throws ServletException error en el servlet
    	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
    	 * */
        public void doPost( HttpServletRequest req, HttpServletResponse res )
                        throws ServletException, IOException{
                defaultAction( req, res );
        }

    	/**
    	 * M�todo que responde a peticiones por default
    	 * @param req instancia de HTTPServletRequest
    	 * @param res instancia de HTTPServletResponse
    	 * @throws ServletException error en el servlet
    	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
    	 * */
        public void defaultAction( HttpServletRequest req, HttpServletResponse res)
                        throws ServletException, IOException
		{

			String          contrato                                = null;
			String          usuario                                 = null;
			String          strDebug                                = null;
			String          menuSesion                              = null;
			String          clave_perfil                    = null;
			boolean         cadena_getFacCOpProg;
			String[][]      arrayCuentas                    = null;
			int    nmaxoper;
			HttpSession sess = req.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			boolean sesionvalida = SesionValida( req, res );
			if (sesionvalida)
			{
				/******************************************Inicia validacion OTP**************************************/
				String valida = req.getParameter ( "valida" );
				if(validaPeticion( req,  res,session,sess,valida))
				{
					EIGlobal.mensajePorTrace("SI---------------***ENTRA A VALIDAR LA PETICION *********", EIGlobal.NivelLog.INFO);
				}
                else{
				/******************************************Termina validacion OTP**************************************/
				if (req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
					if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
						req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
								req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
					} else if (getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
						req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
								getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER));
					}
				}

				//modificacion para integracion
				session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

				contrato             = session.getContractNumber();
				usuario                       = session.getUserID8();
				clave_perfil          = session.getUserProfile();
				suc_opera                     = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
				menuSesion            = session.getstrMenu();
				cadena_getFacCOpProg = session.getFacultad(session.FAC_PROGRAMA_OP);

				nmaxoper = Global.MAX_REGISTROS;

				strInhabiles =diasInhabilesDesp();
				/* if (strInhabiles.trim().length()==0)
				{
					despliegaPaginaError("Servicio no disponible por el momento","Transferencias","", req, res);
					return;
				}
				*/

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples ","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s26430ah",req));
				///////////////////////////////////////////////////////////////////////////////////////

				String funcion_llamar = ( String ) req.getParameter("ventana");
				EIGlobal.mensajePorTrace("***(13Ene05 - TransferenciasMultiples.class funcion_llamar: "+funcion_llamar, EIGlobal.NivelLog.INFO);

				// Entrando al menu del flujo
				if (session.getFacultad(session.FAC_ABONO_CHEQUES))
				{
					if ("0".equals(funcion_llamar)){
						pantalla_inicial_transferencias_multiples(nmaxoper,arrayCuentas,cadena_getFacCOpProg, strDebug, req, res );}    // Cuentas Propias Moneda Nacional
					else if ("1".equals(funcion_llamar)){
						pantalla_tabla_transferencias_multiples_sel(nmaxoper,arrayCuentas, req, res );}            // Refresh bot�n Adicionar
					else if ("2".equals(funcion_llamar)){
						pantalla_confirm_transferencias_multiples(nmaxoper,arrayCuentas, req, res );}            // Refresh bot�n Adicionar
					else if ("3".equals(funcion_llamar)){ //Req Q1466. Validacion de posibles registros duplicados por envio previo
						pantalla_valdup_transferencias_multiples(clave_perfil,strDebug, usuario, contrato, req, res );}
					else if ("4".equals(funcion_llamar))
					{
						String [] arregloctas_origen2_array = req.getParameter("arregloctas_origen2").toString().split("@");
						String [] arregloctas_destino2_array = req.getParameter("arregloctas_destino2").toString().split("@");
						String [] arregloimportes2_array = req.getParameter("arregloimportes2").toString().split("@");
						String [] arregloestatus2_array = req.getParameter("arregloestatus2").toString().split("@");
						String arregloctas_origen2_string = "";
						String arregloctas_destino2_string = "";
						String arregloimportes2_string = "";
						String arregloestatus2_string = "";
						for (int aux=0; aux<arregloestatus2_array.length; aux++) {
							if ("1".equals(arregloestatus2_array[aux])) {
								arregloctas_origen2_string = arregloctas_origen2_string.concat(arregloctas_origen2_array[aux]).concat("@");
								arregloctas_destino2_string = arregloctas_destino2_string.concat(arregloctas_destino2_array[aux]).concat("@");
								arregloimportes2_string = arregloimportes2_string.concat(arregloimportes2_array[aux]).concat("@");
								arregloestatus2_string = arregloestatus2_string.concat(arregloestatus2_array[aux]).concat("@");
							}
						}
						req.setAttribute("arregloctas_origen2_string", arregloctas_origen2_string);
						req.setAttribute("arregloctas_destino2_string", arregloctas_destino2_string);
						req.setAttribute("arregloimportes2_string", arregloimportes2_string);
						req.setAttribute("arregloestatus2_string", arregloestatus2_string);
						
						///////////////////--------------challenge--------------------------
						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : "";
						
						EIGlobal.mensajePorTrace("--------------->TransferenciasMultiples > validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);
						
						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->TransferenciasMultiples > Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
							//ValidaOTP.guardaParametrosEnSession(req);
							guardaParametrosEnSession(req);
							validacionesRSA(req, res);
							return;
						} 
						
						//Fin validacion rsa para challenge
						///////////////////-------------------------------------------------
						
						
						//interrumpe la transaccion para invocar la validaci�n
						boolean valBitacora = true;
						if(  valida==null){
							EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.INFO);

							boolean solVal=ValidaOTP.solicitaValidacion(
										session.getContractNumber(),IEnlace.TRANS_INTERNAS);

						if( session.getValidarToken() &&
							    session.getFacultad(session.FAC_VAL_OTP) &&
							    session.getToken().getStatus() == 1 &&
							    solVal)
							{
								//VSWF-HGG -- Bandera para guardar el token en la bit�cora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
								guardaParametrosEnSession(req);
								//ValidaOTP.guardaParametrosEnSession(req);
								req.getSession().removeAttribute("mensajeSession");
								ValidaOTP.mensajeOTP(req, "TransferenciaMultiple");
								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
							} else {
								valida="1";
								valBitacora = false;
                                                                ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
                                                        }
						}
		                //retoma el flujo
		                if( null != valida && "1".equals(valida))
						{
		                	if (valBitacora)
							{
			        			try{
			        				EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);
			        				int numeroReferencia  = obten_referencia_operacion(suc_opera);
			        				String claveOperacion = BitaConstants.CTK_TRANSFERENCIA_MN;
			        				String concepto = BitaConstants.CTK_CONCEPTO_TRANSFERENCIA_MN;
			        				String cuenta = "0";
			        				double importeDouble = 0;
			        				String importe_str = "0.0";
			        				String token = "0";
									//if (req.getParameter("token") != null && !req.getParameter("token").equals(""))
									{token = req.getParameter("token");}
			        				String importes = req.getAttribute("arregloimportes2_string").toString();
			        				String[] imp = importes.split("@");
			        				for(int n = 0; n < imp.length; n++){
			        					importe_str  = imp[n];
			        					importeDouble = importeDouble + Double.valueOf(importe_str).doubleValue();}
			        				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
			        			} catch(Exception e) {
			        				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			        				EIGlobal.mensajePorTrace("ENTRA  BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.INFO);
			        			}
							}
                                                        EIGlobal.mensajePorTrace("---1-----retoma el flujo  *********", EIGlobal.NivelLog.INFO);

							ejecucion_operaciones_transferencias_multiples(clave_perfil,strDebug, usuario, contrato, req, res );     // Ejecuci�n de transferencia
                                                        ValidaOTP.guardaBitacora((List)sess.getAttribute("bitacora"),tipo_operacion);
						}
					}
				} else
				{
					despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Transferencias M&uacute;ltiples","Transferencias M&uacute;ltiples ","s26430h", req, res);
				}
			}
        }
        }

    	/**
    	 * M�todo que realiza la pantalla inicial de transferencias m�ltiples
    	 * @param nmaxoper dato int
    	 * @param arrayCuentas dato String[][]
    	 * @param cadena_getFacCOpProg dato boolean
    	 * @param strDebug dato String
    	 * @param req instancia de HTTPServletRequest
    	 * @param res instancia de HTTPServletResponse
    	 * @throws ServletException error en el servlet
    	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
    	 * @return bandera de exito
    	 * */
    public int pantalla_inicial_transferencias_multiples(int    nmaxoper,String[][] arrayCuentas,boolean cadena_getFacCOpProg, String strDebug, HttpServletRequest req, HttpServletResponse res)
						throws ServletException, IOException {


                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class Entrando a pantalla_inicial_transferencias_multiples", EIGlobal.NivelLog.INFO);
                int salida                                      = 0;
                int numero_cuentas_trans_car= 0;
                int numero_cuentas_trans_abo= 0;
                boolean fac_programadas;

                String campos_fecha     = "";
                String strSalida2       = "";
                String coderror         = "";
                String strSalida        = "";
                String fecha_hoy        = "";
                String tipoCuenta       = "";
                String claveProducto= "";
                String NoRegistradas= "";
                String divisa           = "";

           		HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

				/***********************.  Req. Q1466. limpieza de variables de Sesion. *****************************/
				//Se controla la Recarga para evitar duplicidad de registros transferencia

				verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",true);

				if(req.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
				{
					req.getSession().removeAttribute("Recarga_Ses_Multiples");
					EIGlobal.mensajePorTrace( "Transferencias Multiples - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
		        }
				else{
					EIGlobal.mensajePorTrace( "Transferencias Multiples - La variable ya fue borrada", EIGlobal.NivelLog.INFO);}

				/**********************Termina limpieza de variables de Sesion**********************/


                fac_programadas                 = cadena_getFacCOpProg;
                fecha_hoy                       = ObtenFecha();

                numero_cuentas_trans_car=1;
                numero_cuentas_trans_abo=1;

                if ( (numero_cuentas_trans_car>0) && (numero_cuentas_trans_abo>0) )
                {
                        if(strDebug==null||strDebug.trim().equals(""))
                                strDebug = "  ";
                        req.setAttribute("strDebug", strDebug);
                        req.setAttribute("diasInhabiles", strInhabiles);
                        req.setAttribute("tipoDivisa", divisa );
                        req.setAttribute("arregloctas_origen1","");
                        req.setAttribute("arregloctas_destino1","");
                        req.setAttribute("arregloimportes1","");
                        req.setAttribute("arreglofechas1","");
                        req.setAttribute("arregloconceptos1","");
                        req.setAttribute("arregloestatus1","");
                        req.setAttribute("arregloctas_origen2","");
                        req.setAttribute("arregloctas_destino2","");
                        req.setAttribute("arregloimportes2","");
                        req.setAttribute("arreglofechas2","");
                        req.setAttribute("arregloconceptos2","");
                        req.setAttribute("arregloestatus2","");
                        req.setAttribute("contador1","0");
                        req.setAttribute("contador2","0");
                        req.setAttribute("tabla","");
                        req.setAttribute("fecha_hoy",fecha_hoy);
                        req.setAttribute("ContUser",ObtenContUser(req));
                        req.setAttribute("fecha1",ObtenFecha(true));
                        req.setAttribute("TIPOTRANS","LINEA");
                        req.setAttribute("INICIAL","SI");
                        req.setAttribute("IMPORTACIONES","0");
                        req.setAttribute("NMAXOPER"," "+nmaxoper);
                        req.setAttribute("divisa","Pesos");
                        req.setAttribute("trans","0");
                        req.setAttribute("ctascargo1",CADENA_VACIA+numero_cuentas_trans_car);
                        req.setAttribute("ctasabono1",CADENA_VACIA+numero_cuentas_trans_abo);
                    req.setAttribute("ctascargo2",CADENA_VACIA+numero_cuentas_trans_car);
                        req.setAttribute("ctasabono2",CADENA_VACIA+numero_cuentas_trans_abo);
                        req.setAttribute("campos_totales","");
                        req.setAttribute("campos_concepto_fecha","");
                        req.setAttribute("campos_botones","");



                        if (!fac_programadas){
                                req.setAttribute("fac_programadas1","0");}
                        else{
                                req.setAttribute("fac_programadas1","1");}
						//TODO BIT CU2021, inicia el flujo de Transferencias m�ltiples

						/*
						 * VSWF-HGG-I
						 */
                        if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						try {
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));

							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_MULTI_ENTRA);
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber());
							}

							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
                        }
						/*
						 * VSWF-HGG-F
						 */
                        req.setAttribute("MenuPrincipal", session.getStrMenu());
            			req.setAttribute("newMenu", session.getFuncionesDeMenu());
            			req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s26430ah",req));

                        evalTemplate(IEnlace.TRANSFERENCIAS_MULTIPLES_TMPL, req, res);
                }
                else   // No existen cuentas de cargo y abono
                {
                    despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Transferencias M&uacute;ltiples","Transferencias M&uacute;ltiples","s26430h", req, res);
                }
                return 1;
        }  // fin m�todo

    /**
	 * M�todo que realiza la pantalla tabla transferencias multiples sel
	 * @param nmaxoper operaciones
	 * @param arrayCuentas cuentas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera de exito
	 * */
   public int pantalla_tabla_transferencias_multiples_sel(int    nmaxoper,String[][] arrayCuentas, HttpServletRequest req, HttpServletResponse res)
                        throws ServletException, IOException {
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class Entrando a pantalla_tabla_transferencias sel ", EIGlobal.NivelLog.INFO);
                String  tabla                           = "";
                String  arrctas_origen          = "";
                String  arrctas_destino         = "";
                String  arrimporte                      = "";
                String  arrconceptos            = "";
                String  arrestatus                      = "";
                String  arrfechas                       = "";
                String  cadena_cta_origen       = "";
                String  cadena_cta_destino      = "";
                String  cta_origen                      = "";
                String  cta_destino                     = "";
                String  scontador                       = "";
                String  simporte                        = "";
                String  concepto                        = "";
                String  fecha                           = "";
                String  nombre_campo            = "";
                String  strSalida                       = "";
                String  fecha_hoy                       = "";
                String  valor                           = "";
                String  sfac_programadas        = "";
                String  campos_fecha            = "";
                String  strSalida2                      = "";
                String  strArchivo                      = "";
                String  chkvalor                        = "";
                String  coderror                        = "";
                String  tipoCuenta                      = "";
                String  claveProducto           = "";
                String montoCargo;
                String montoAbono;
            	String  sesion_cta_destino      = "";
                double  importetabla=0.0;
                double sumaMontoCargo=0.0;
                double sumaMontoAbono=0.0;
                double  selsumcargo=0.0;
                double  selsumabono=0.0;

                int     contador                        = 0;
                int     salida                          = 0;
                int     indice                          = 0;
                boolean fac_programadas;


				HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

				String arrayCuentasCargo[][]=null;
                String arrayCuentasAbono[][]=null;


                String selorigen         = ( String ) req.getParameter ("selorigen");
                String selorigentrama    = ( String ) req.getParameter ("selorigentrama");
                String selorigencontador = ( String ) req.getParameter ("selorigencontador");
                String selorigenimporte  = ( String ) req.getParameter ("selorigenimporte");
                String selorigenestatus  = ( String ) req.getParameter ("selorigenestatus");


                montoCargo=req.getParameter("montoCargo");
                montoAbono=req.getParameter("montoAbono");

               if(montoCargo!=null && montoCargo.length()>0)
                {
                  try
                  {
                     sumaMontoCargo=Double.parseDouble(montoCargo);
                  }
                  catch(NumberFormatException nfe)
                  {
                     sumaMontoCargo=0.0;
                  } // Fin try-catch
                }
                else
                {
                  sumaMontoCargo=0.0;
                } // Fin if-else


                if(montoAbono!=null && montoAbono.length()>0)
                {
                  try
                  {
                     sumaMontoAbono=Double.parseDouble(montoAbono);
                  }
                  catch(NumberFormatException nfe)
                  {
                     sumaMontoAbono=0.0;
                  } // Fin try-catch
                }
                else
                {
                  sumaMontoAbono=0.0;
                } // Fin if-else




                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigen "+selorigen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama "+selorigentrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigencontador "+selorigencontador, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigenimporte "+selorigenimporte, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigenestatus "+selorigenestatus, EIGlobal.NivelLog.INFO);


                if  (selorigen==null){
                   selorigen="";}
                if ((selorigencontador==null)||("".equals(selorigencontador))){
                  selorigencontador="0";}
                if (selorigentrama==null){
            selorigentrama="";}

                int iselorigencontador=0;
                iselorigencontador=Integer.parseInt(selorigencontador);

                if  (!selorigen.equals(""))
            {
           selorigentrama+=selorigen;
                   iselorigencontador++;
                }

            if  (!"".equals(selorigentrama)){
                   arrayCuentasCargo=ObtenArregloBoxes(CADENA_VACIA+iselorigencontador+"@"+selorigentrama);}

                if(selorigenimporte==null){
                        selorigenimporte="";}
                if(selorigenestatus==null){
                        selorigenestatus="";}

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigen2 "+selorigen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama2 "+selorigentrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigencontador2 "+selorigencontador, EIGlobal.NivelLog.INFO);


                String seldestino         = ( String ) req.getParameter ("seldestino");
                String seldestinotrama    = ( String ) req.getParameter ("seldestinotrama");
                String seldestinocontador = ( String ) req.getParameter ("seldestinocontador");
                String seldestinoimporte  = ( String ) req.getParameter ("seldestinoimporte");
                String seldestinoestatus  = ( String ) req.getParameter ("seldestinoestatus");


                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestino "+seldestino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama "+seldestinotrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinocontador "+seldestinocontador, EIGlobal.NivelLog.INFO);


        if (seldestino==null){
         seldestino="";}
       if ((seldestinocontador==null)||("".equals(seldestinocontador))){
         seldestinocontador="0";}
                if (seldestinotrama==null){
            seldestinotrama="";}

                int iseldestinocontador=0;
                iseldestinocontador=Integer.parseInt(seldestinocontador);
                if(!"".equals(seldestino))
            {
           seldestinotrama+=seldestino;
                   iseldestinocontador++;
                }
        if  (!"".equals(seldestinotrama)){
              arrayCuentasAbono=ObtenArregloBoxes(CADENA_VACIA+iseldestinocontador+"@"+seldestinotrama);}

        if(selorigenimporte==null){
                        seldestinoimporte="";}
                if(selorigenestatus==null){
                        seldestinoestatus="";}

        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestino 2"+seldestino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama 2"+seldestinotrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinocontador 2"+seldestinocontador, EIGlobal.NivelLog.INFO);


                chkvalor                  =  ( String ) req.getParameter ("ChkOnline");

                cadena_cta_origen  = ( String ) req.getParameter ("ctasorigen");
                cadena_cta_destino = ( String ) req.getParameter ("ctasdestino");
                simporte           = ( String ) req.getParameter ("ctasimporte");
                concepto           = ( String ) req.getParameter ("concepto");
                fecha              = ( String ) req.getParameter ("fecha_completa");


                arrctas_origen     = ( String ) req.getParameter ("arregloctas_origen1");
                arrctas_destino    = ( String ) req.getParameter ("arregloctas_destino1");
                arrimporte         = ( String ) req.getParameter ("arregloimportes1");
                arrconceptos       = ( String ) req.getParameter ("arregloconceptos1");

                arrfechas          = ( String ) req.getParameter ("arreglofechas1");
                arrestatus         = ( String ) req.getParameter ("arregloestatus1");
                scontador          = ( String ) req.getParameter ("contador1");
                sfac_programadas   = ( String ) req.getParameter ("fac_programadas1");

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  cadena_cta_origen: "+cadena_cta_origen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  cadena_cta_destino: "+cadena_cta_destino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  simporte: "+simporte, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  concepto: "+concepto, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  fecha_completa: "+fecha, EIGlobal.NivelLog.INFO);

                if(concepto==null){
         concepto="";}
                if(fecha==null){
         fecha="";}

                if (concepto.length()==0)
                        concepto="TRANSFERENCIA ELECTRONICA"; // MODIFICAR


				EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  contador: "+contador, EIGlobal.NivelLog.INFO);

                contador=Integer.parseInt(scontador);
        int numero_cuentas_trans_car=0;
                int numero_cuentas_trans_abo=0;

        String tipocolumna="";
                boolean chkseg=true;

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  ArregloCuentasCargo ", EIGlobal.NivelLog.INFO);
        if(arrayCuentasCargo!=null)
            {
         strSalida="<TABLE><TR bgcolor=#A4BEE4><TD align=center class=tittabdat>Seleccione</td><TD align=center  colspan=2 class=tittabdat>Cuenta Cargo</td><TD align=center  class=tittabdat>Importe</td></TR>";

                 String SelOrigenEstatus[]=null;
                 if(!selorigenestatus.equals(""))
                 {
                    selorigenestatus+="@";
                        SelOrigenEstatus= desentramaC(CADENA_VACIA+iselorigencontador+"@"+selorigenestatus,'@');
                 }

         String SelOrigenImporte[]=null;
                 if(!selorigenimporte.equals(""))
                 {
                    selorigenimporte+="@";
                        SelOrigenImporte= desentramaC(CADENA_VACIA+iselorigencontador+"@"+selorigenimporte,'@');
                 }


                 for (int ind=1;ind<=Integer.parseInt(arrayCuentasCargo[0][0]);ind++)
                 {
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  i: "+ind, EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][1]: "+arrayCuentasCargo[ind][1], EIGlobal.NivelLog.INFO);

                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][2]: "+arrayCuentasCargo[ind][2], EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][3]: "+arrayCuentasCargo[ind][3], EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][4]: "+arrayCuentasCargo[ind][4], EIGlobal.NivelLog.INFO);

                    tipoCuenta          = arrayCuentasCargo[ind][1].substring(0, 2);
                        claveProducto   = arrayCuentasCargo[ind][3];

                        if (numero_cuentas_trans_car%2==0)
            {
                           strSalida+="<TR  bgcolor=#CCCCCC>";
                           tipocolumna="<TD class=textabdatobs>";
                        }
                        else
            {
                           strSalida+="<TR  bgcolor=#EBEBEB>";
                           tipocolumna="<TD class=textabdatcla>";
                        }
                        if (SelOrigenEstatus!=null)
                        {
              if ("1".equals(SelOrigenEstatus[ind])){
                            strSalida+=tipocolumna+"<input type=checkbox NAME=origen CHECKED VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";}
                          else{
                 strSalida+=tipocolumna+"<input type=checkbox NAME=origen VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";}
            }
                        else

              strSalida+=tipocolumna+"<input type=checkbox NAME=origen VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";
                        strSalida+=tipocolumna+arrayCuentasCargo[ind][1]+"</TD>";
                        if (arrayCuentasCargo[ind][4].trim().length()>0)
                          strSalida+=tipocolumna+arrayCuentasCargo[ind][4].trim()+"</TD>";
                        else
               strSalida+=tipocolumna+"&nbsp;&nbsp;</TD>";
                        if(SelOrigenImporte!=null)
                        {
                          if(SelOrigenImporte[ind]!=null)
                          {
                                  strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\""+SelOrigenImporte[ind]+"\"   SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                              if(SelOrigenImporte[ind].trim().length()==0)
                                          SelOrigenImporte[ind]="0.0";
                  selsumcargo=new Double(SelOrigenImporte[ind]).doubleValue();

                          }
                          else
                          {
                                  strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                  selsumcargo+=0.0;
                          }
                        }
                        else
                        {
                                strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                selsumcargo+=0.0;
                        }
                        numero_cuentas_trans_car++;
                 }
                 strSalida+="</TABLE>";

        }
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  ArregloCuentasAbono ", EIGlobal.NivelLog.INFO);

                if (arrayCuentasAbono!=null)
                {
                 strSalida2="<TABLE><TR bgcolor=#A4BEE4><TD align=center class=tittabdat>Seleccione</td><TD align=center  colspan=2 class=tittabdat>Cuenta Abono/M&oacute;vil</td><TD align=center  class=tittabdat>Importe</td></TR>";


                 String SelDestinoEstatus[]=null;
                 if(!"".equals(seldestinoestatus))
                 {
                    seldestinoestatus+="@";
                        SelDestinoEstatus= desentramaC(CADENA_VACIA+iseldestinocontador+"@"+seldestinoestatus,'@');
                 }

         String SelDestinoImporte[]=null;
                 if(!"".equals(seldestinoimporte))
                 {
                    seldestinoimporte+="@";
                        SelDestinoImporte= desentramaC(CADENA_VACIA+iseldestinocontador+"@"+seldestinoimporte,'@');
                 }



                 for (int ind=1;ind<=Integer.parseInt(arrayCuentasAbono[0][0]);ind++)
                 {
                         if (numero_cuentas_trans_abo%2==0)
             {
                                 strSalida2+="<TR  bgcolor=#CCCCCC>";
                                 tipocolumna="<TD class=textabdatobs>";
                         }
                         else
             {
                             strSalida2+="<TR  bgcolor=#EBEBEB>";
                             tipocolumna="<TD class=textabdatcla>";
                         }
             if(SelDestinoEstatus!=null)
             {
                if("1".equals(SelDestinoEstatus[ind])){
                                   strSalida2+=tipocolumna+"<input type=checkbox NAME=destino CHECKED VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";}
                                else{
                                   strSalida2+=tipocolumna+"<input type=checkbox NAME=destino VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";}
             }
                         else
                                 strSalida2+=tipocolumna+"<input type=checkbox NAME=destino VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";

                         strSalida2+=tipocolumna+arrayCuentasAbono[ind][1]+"</TD>";
                         if (arrayCuentasAbono[ind][4].trim().length()>0){
                           strSalida2+=tipocolumna+arrayCuentasAbono[ind][4].trim()+"</TD>";}
                         else{
               strSalida2+=tipocolumna+"&nbsp;&nbsp;</TD>";}
                         if(SelDestinoImporte!=null)
             {
                            if(SelDestinoImporte[ind]!=null)
                                {
                                        strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\""+SelDestinoImporte[ind] +"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                        if(SelDestinoImporte[ind].trim().length()==0)
                                          SelDestinoImporte[ind]="0.0";
                       selsumabono=new Double(SelDestinoImporte[ind]).doubleValue();

                                }
                           else
                           {
                                   strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                               selsumabono+=0.0;
                           }
                         }
                         else
                         {
                                 strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                 selsumabono+=0.0;
                         }
                         numero_cuentas_trans_abo++;


                 } // for (ind)
                 strSalida2+="</TABLE>";
        }

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  Termine Arreglos ", EIGlobal.NivelLog.INFO);
                //EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  strSalida "+strSalida, EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  strSalida2 "+strSalida2, EIGlobal.NivelLog.INFO);


                int cont = 0;
                /*
                //System.out.println("--->cadena_cta_origen ="+cadena_cta_origen);
                //System.out.println("--->cadena_cta_origen ="+cadena_cta_destino);
                //System.out.println("Debe tener arroba sino da index out of bounds exception");
                */
				int nctascargo=0;
                int nctasabono=0;
                if ((cadena_cta_origen!=null)&&
                    (cadena_cta_destino!=null)&&
                        (!"".equals(cadena_cta_origen))&&
                        (!"".equals(cadena_cta_destino)) )
                {
                  try
                  {
                         nctascargo=Integer.parseInt(cadena_cta_origen.substring(0,cadena_cta_origen.indexOf("@")));
             nctasabono=Integer.parseInt(cadena_cta_destino.substring(0,cadena_cta_destino.indexOf("@")));
             cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf("@")+1,cadena_cta_origen.length());
             cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf("@")+1,cadena_cta_destino.length());
                     simporte=simporte.substring(simporte.indexOf("@")+1,simporte.length());
                  }
                  catch (Exception e)
                  {
                	  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                  }
                }


        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class nctascargo:"+nctascargo, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class nctasabono:"+nctasabono, EIGlobal.NivelLog.INFO);



        if (nctascargo>1)
        {
           arrctas_origen+=cadena_cta_origen;
                   arrimporte+=simporte;
           for(int i=0;i<nctascargo;i++)
                   {
             arrctas_destino+=cadena_cta_destino;
                         arrconceptos+=concepto+"@";
                         arrfechas+=fecha+"@";
             arrestatus+="1@";
             contador++;
           }
        }
                else if (nctasabono>1)
                {
           arrctas_destino+=cadena_cta_destino;
                   arrimporte+=simporte;
           for(int i=0;i<nctasabono;i++)
                   {
             arrctas_origen+=cadena_cta_origen;
                         arrconceptos+=concepto+"@";
                         arrfechas+=fecha+"@";
             arrestatus+="1@";
             contador++;
                   }
                }
                else if ((nctascargo==1)&&(nctasabono==1))
                {
            contador=1;
            arrctas_origen+=cadena_cta_origen;
                        arrimporte+=simporte;
                        arrctas_destino+=cadena_cta_destino;
                    arrconceptos+=concepto+"@";
                        arrfechas+=fecha+"@";
            arrestatus+="1@";

                }

                fecha_hoy=ObtenFecha();

                if ("0".equals(sfac_programadas)){
                        fac_programadas=false;}
                else{
                        fac_programadas=true;}

                //campos_fecha=poner_campos_fecha(fac_programadas);

                String arrconceptosprueba="\""+arrconceptos+"\"";
                String arrctas_origenprueba="\""+arrctas_origen+"\"";
                String arrctas_destinoprueba="\""+arrctas_destino+"\"";
                req.setAttribute("diasInhabiles", strInhabiles);
                req.setAttribute("cuentas_destino", strSalida2);
                req.setAttribute("cuentas_origen", strSalida);

                req.setAttribute("arregloctas_origen1",arrctas_origenprueba);
                req.setAttribute("arregloctas_destino1",arrctas_destinoprueba);
                req.setAttribute("arregloctas_origen2",arrctas_origenprueba);
                req.setAttribute("arregloctas_destino2",arrctas_destinoprueba);
                req.setAttribute("arregloimportes1",arrimporte);
                req.setAttribute("arregloimportes2",arrimporte);
                req.setAttribute("arregloconceptos1",arrconceptosprueba);
                req.setAttribute("arregloconceptos2",arrconceptosprueba);
                req.setAttribute("arreglofechas1",arrfechas);
                req.setAttribute("arreglofechas2",arrfechas);
                req.setAttribute("arregloestatus1",arrestatus);
                req.setAttribute("arregloestatus2",arrestatus);
                req.setAttribute("contador1",Integer.toString(contador));
                req.setAttribute("contador2",Integer.toString(contador));
                req.setAttribute("fecha_hoy",fecha_hoy);
				////System.out.println("fecha1:"+ObtenFecha(true));
				req.setAttribute("fecha1",ObtenFecha(true));
                req.setAttribute("fac_programadas1",sfac_programadas);
                req.setAttribute("INICIAL","NO");
                req.setAttribute("TIPOTRANS", ( String ) req.getParameter ("TIPOTRANS"));
                req.setAttribute("IMPORTACIONES","1");
                req.setAttribute("NMAXOPER"," "+nmaxoper);
                req.setAttribute("trans",( String ) req.getParameter ("trans"));
                req.setAttribute("ctascargo1",CADENA_VACIA+numero_cuentas_trans_car);
                req.setAttribute("ctasabono1",CADENA_VACIA+numero_cuentas_trans_abo);
                req.setAttribute("ctascargo2",CADENA_VACIA+numero_cuentas_trans_car);
                req.setAttribute("ctasabono2",CADENA_VACIA+numero_cuentas_trans_abo);

                req.setAttribute("selorigentrama","\""+selorigentrama+"\"");
                req.setAttribute("selorigencontador",CADENA_VACIA+iselorigencontador);
                req.setAttribute("seldestinotrama","\""+seldestinotrama+"\"");
                req.setAttribute("seldestinocontador",CADENA_VACIA+iseldestinocontador);

        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama:"+selorigentrama, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama:"+seldestinotrama, EIGlobal.NivelLog.INFO);




                tabla="";

                if ((nctascargo==1)||(nctasabono==1))

      {

                   String[] Ctas_origen  = desentramaC(Integer.toString(contador)+"@"+arrctas_origen,'@');
                   String[] Ctas_destino = desentramaC(Integer.toString(contador)+"@"+arrctas_destino,'@');
                   String[] Importes     = desentramaC(Integer.toString(contador)+"@"+arrimporte,'@');
                   String[] Fechas       = desentramaC(Integer.toString(contador)+"@"+arrfechas,'@');
                   String[] Conceptos    = desentramaC(Integer.toString(contador)+"@"+arrconceptos,'@');
                   String[] estatus      = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');


                   tabla=tabla+"<TR >";
                   tabla=tabla+"<TD align=center width=26 class=tittabdat></TD>";
                   tabla=tabla+"<TD align=center width=200 class=tittabdat>Cuenta cargo</TD>";
                   tabla=tabla+"<TD align=center colspan=2 class=tittabdat>Cuenta abono/M&oacute;vil</TD>";
                   tabla=tabla+"<TD align=center width=90 class=tittabdat>Importe</TD>";
                   tabla=tabla+"<TD align=center width=60 class=tittabdat>Fecha aplicaci&oacute;n</TD>";
                   tabla=tabla+"<TD align=center class=tittabdat>Concepto</TD>";
                   tabla=tabla+"</TR>";



                   int residuo=0;
                   String clase="";
           for (indice=1;indice<=contador;indice++)
                   {
                          cadena_cta_origen  = Ctas_origen[indice];
                          cadena_cta_destino = Ctas_destino[indice];

                          cta_origen=cadena_cta_origen.substring(0,cadena_cta_origen.indexOf('|'));
                          cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length());
              cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length()-1);
                          if (cadena_cta_destino.indexOf((int)'|') == -1 )
                          {
                                 cta_destino = cadena_cta_destino;
                                 cadena_cta_destino="";
                          }
                          else
                          {
                                  cta_destino=cadena_cta_destino.substring(0,cadena_cta_destino.indexOf('|'));
                                  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length());
                  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length()-1);
                          }

                          residuo=indice%2;
                          tabla=tabla+"<TR>";
                          if (residuo==0){
                                clase="textabdatobs";}
                          else{
                                clase="textabdatcla";}

                          valor=estatus[indice];
                          nombre_campo="CH"+indice;
                  if ("0".equals(valor)){
                            tabla=tabla+ "<TD align=right class="+clase+"> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+"></TD>";}
                          else{
                            tabla=tabla+ "<TD align=right class="+clase+"> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+" checked ></TD>";}


              importetabla+=new Double(Importes[indice]).doubleValue();
                          tabla=tabla+"<TD class="+clase+" align=left  nowrap >"+cta_origen+" "+cadena_cta_origen+"</TD>";
                          tabla=tabla+"<TD class="+clase+" nowrap align=left colspan=2 >"+cta_destino+" "+cadena_cta_destino+"</TD>";
                          tabla=tabla+"<TD class="+clase+" align=right nowrap>"+FormatoMoneda(Importes[indice])+"</TD>";
                          tabla=tabla+"<TD class="+clase+" align=center nowrap>"+Fechas[indice]+"</TD>";
                          tabla=tabla+"<TD class="+clase+"  align=left nowrap>"+Conceptos[indice]+"</TD>";
                  tabla=tabla+"</TR>";
                   } // for ( indice )

                   tabla=tabla+"</TABLE>";

        }

                 ///////////////////////////////////////////////
                 req.setAttribute("contadoroperaciones","Total de registros:"+contador+" por "+ FormatoMoneda(CADENA_VACIA+importetabla));
                 req.setAttribute("tabla",tabla);
                 if(contador>0){
                   req.setAttribute("botontransferir","<td align=right width=89><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>");}
                 else{
                   req.setAttribute("botontransferir","");}

                 req.setAttribute("MenuPrincipal", session.getStrMenu());
                 req.setAttribute("newMenu", session.getFuncionesDeMenu());
                 req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s26430ah",req));
                 if ((iselorigencontador>0)&&(iseldestinocontador>0))
             {
                   req.setAttribute("campos_totales",Poner_totales(sumaMontoCargo, sumaMontoAbono));
                   req.setAttribute("campos_concepto_fecha",Poner_concepto_fecha(poner_calendario_programadas(fecha,fac_programadas,"fecha_completa","WindowCalendar()"),
                                                                                   poner_campos_fecha(fecha,fac_programadas),concepto));
                   req.setAttribute("campos_botones",Poner_botones());
                 }
                 else
             {
                   req.setAttribute("campos_totales","");
                   req.setAttribute("campos_concepto_fecha","");
                   req.setAttribute("campos_botones","");

                 }
             evalTemplate(IEnlace.TRANSFERENCIAS_MULTIPLES_TMPL, req, res);
                 return 1;

        }  // m�todo



   /**
	 * M�todo que realiza la pantalla de confirmacion de transferencias multiples
	 * @param nmaxoper operaciones
	 * @param arrayCuentas cuentas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera de exito
	 * */
   public int pantalla_confirm_transferencias_multiples(int    nmaxoper,String[][] arrayCuentas, HttpServletRequest req, HttpServletResponse res)
                        throws ServletException, IOException {
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class Entrando a pantalla_tabla_transferencias sel ", EIGlobal.NivelLog.INFO);
                String  tabla                           = "";
                String  arrctas_origen          = "";
                String  arrctas_destino         = "";
                String  arrimporte                      = "";
                String  arrconceptos            = "";
                String  arrestatus                      = "";
                String  arrfechas                       = "";
                String  cadena_cta_origen       = "";
                String  cadena_cta_destino      = "";
                String  cta_origen                      = "";
                String  cta_destino                     = "";
                String  scontador                       = "";
                String  simporte                        = "";
                String  concepto                        = "";
                String  fecha                           = "";
                String  nombre_campo            = "";
                String  strSalida                       = "";
                String  fecha_hoy                       = "";
                String  valor                           = "";
                String  sfac_programadas        = "";
                String  campos_fecha            = "";
                String  strSalida2                      = "";
                String  strArchivo                      = "";
                String  chkvalor                        = "";
                String  coderror                        = "";
                String  tipoCuenta                      = "";
                String  claveProducto           = "";
                double  importetabla=0.0;

               double  selsumcargo=0.0;
               double  selsumabono=0.0;
               double sumaMontoCargo=0.0;
               double sumaMontoAbono=0.0;


                int     contador                        = 0;
                int     salida                          = 0;
                int     indice                          = 0;
                boolean fac_programadas;


				HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");
        		String fileOut="";
        		fileOut = session.getUserID8() + "tm.doc";
        		boolean grabaArchivo = true;
        		String cadena = "";
        		EI_Exportar ArcSal;
        		ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
        		EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
        		ArcSal.creaArchivo();


        String arrayCuentasCargo[][]=null;
                String arrayCuentasAbono[][]=null;


        String selorigen         = ( String ) req.getParameter ("selorigen");
        String selorigentrama    = ( String ) req.getParameter ("selorigentrama");
                String selorigencontador = ( String ) req.getParameter ("selorigencontador");
                String selorigenimporte  = ( String ) req.getParameter ("selorigenimporte");
                String selorigenestatus  = ( String ) req.getParameter ("selorigenestatus");
                String montoCargo=req.getParameter("montoCargo");
                String montoAbono=req.getParameter("montoAbono");

               if(montoCargo!=null && montoCargo.length()>0)
                {
                  try
                  {
                     sumaMontoCargo=Double.parseDouble(montoCargo);
                  }
                  catch(NumberFormatException nfe)
                  {
                     sumaMontoCargo=0.0;
                  } // Fin try-catch
                }
                else
                {
                  sumaMontoCargo=0.0;
                } // Fin if-else


               if(montoAbono!=null && montoAbono.length()>0)
                {
                  try
                  {
                     sumaMontoAbono=Double.parseDouble(montoAbono);
                  }
                  catch(NumberFormatException nfe)
                  {
                     sumaMontoAbono=0.0;
                  } // Fin try-catch
                }
                else
                {
                  sumaMontoAbono=0.0;
                } // Fin if-else

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigen "+selorigen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama "+selorigentrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigencontador "+selorigencontador, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigenimporte "+selorigenimporte, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigenestatus "+selorigenestatus, EIGlobal.NivelLog.INFO);


                if  (selorigen==null){
                   selorigen="";}
                if ((selorigencontador==null)||("".equals(selorigencontador))){
                  selorigencontador="0";}
                if (selorigentrama==null){
            selorigentrama="";}

                int iselorigencontador=0;
                iselorigencontador=Integer.parseInt(selorigencontador);

                if  (!"".equals(selorigen))
            {
           selorigentrama+=selorigen;
                   iselorigencontador++;
                }

            if  (!selorigentrama.equals(""))
                   arrayCuentasCargo=ObtenArregloBoxes(CADENA_VACIA+iselorigencontador+"@"+selorigentrama);

                if(selorigenimporte==null){
                        selorigenimporte="";}
                if(selorigenestatus==null){
                        selorigenestatus="";}

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigen2 "+selorigen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama2 "+selorigentrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigencontador2 "+selorigencontador, EIGlobal.NivelLog.INFO);


        String seldestino         = ( String ) req.getParameter ("seldestino");
        String seldestinotrama    = ( String ) req.getParameter ("seldestinotrama");
                String seldestinocontador = ( String ) req.getParameter ("seldestinocontador");
                String seldestinoimporte  = ( String ) req.getParameter ("seldestinoimporte");
                String seldestinoestatus  = ( String ) req.getParameter ("seldestinoestatus");


                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestino "+seldestino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama "+seldestinotrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinocontador "+seldestinocontador, EIGlobal.NivelLog.INFO);


        if (seldestino==null){
         seldestino="";}
       if ((seldestinocontador==null)||("".equals(seldestinocontador))){
         seldestinocontador="0";}
                if (seldestinotrama==null)
            seldestinotrama="";

                int iseldestinocontador=0;
                iseldestinocontador=Integer.parseInt(seldestinocontador);
                if      (!"".equals(seldestino))
            {
           seldestinotrama+=seldestino;
                   iseldestinocontador++;
                }
        if  (!seldestinotrama.equals("")){
              arrayCuentasAbono=ObtenArregloBoxes(CADENA_VACIA+iseldestinocontador+"@"+seldestinotrama);}

        if(selorigenimporte==null){
                        seldestinoimporte="";}
                if(selorigenestatus==null){
                        seldestinoestatus="";}

        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestino 2"+seldestino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama 2"+seldestinotrama, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinocontador 2"+seldestinocontador, EIGlobal.NivelLog.INFO);


                chkvalor                  =  ( String ) req.getParameter ("ChkOnline");

                cadena_cta_origen  = ( String ) req.getParameter ("ctasorigen");
                cadena_cta_destino = ( String ) req.getParameter ("ctasdestino");
                simporte           = ( String ) req.getParameter ("ctasimporte");
                concepto           = ( String ) req.getParameter ("concepto");
                fecha              = ( String ) req.getParameter ("fecha_completa");


                arrctas_origen     = ( String ) req.getParameter ("arregloctas_origen1");
                arrctas_destino    = ( String ) req.getParameter ("arregloctas_destino1");
                arrimporte         = ( String ) req.getParameter ("arregloimportes1");
                arrconceptos       = ( String ) req.getParameter ("arregloconceptos1");

                arrfechas          = ( String ) req.getParameter ("arreglofechas1");
                arrestatus         = ( String ) req.getParameter ("arregloestatus1");
                scontador          = ( String ) req.getParameter ("contador1");
                sfac_programadas   = ( String ) req.getParameter ("fac_programadas1");

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  cadena_cta_origen: "+cadena_cta_origen, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  cadena_cta_destino: "+cadena_cta_destino, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  simporte: "+simporte, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  concepto: "+concepto, EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  fecha_completa: "+fecha, EIGlobal.NivelLog.INFO);

                if(concepto==null){
         concepto="";}
                if(fecha==null){
         fecha="";}

                if (concepto.trim().length()==0)
                        concepto="TRANSFERENCIA ELECTRONICA"; // MODIFICAR

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  contador: "+contador, EIGlobal.NivelLog.INFO);

                contador=Integer.parseInt(scontador);
        int numero_cuentas_trans_car=0;
                int numero_cuentas_trans_abo=0;

        String tipocolumna="";
                boolean chkseg=true;

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  ArregloCuentasCargo ", EIGlobal.NivelLog.INFO);
        if(arrayCuentasCargo!=null)
            {
         strSalida="<TABLE><TR bgcolor=#A4BEE4><TD align=center class=tittabdat>Seleccione</td><TD align=center  colspan=2 class=tittabdat>Cuenta Cargo</td><TD align=center  class=tittabdat>Importe</td></TR>";

                 String SelOrigenEstatus[]=null;
                 if(!selorigenestatus.equals(""))
                 {
                    selorigenestatus+="@";
                        SelOrigenEstatus= desentramaC(CADENA_VACIA+iselorigencontador+"@"+selorigenestatus,'@');
                 }

         String SelOrigenImporte[]=null;
                 if(!selorigenimporte.equals(""))
                 {
                    selorigenimporte+="@";
                        SelOrigenImporte= desentramaC(CADENA_VACIA+iselorigencontador+"@"+selorigenimporte,'@');
                 }


                 for (int ind=1;ind<=Integer.parseInt(arrayCuentasCargo[0][0]);ind++)
                 {
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  i: "+ind, EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][1]: "+arrayCuentasCargo[ind][1], EIGlobal.NivelLog.INFO);

                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][2]: "+arrayCuentasCargo[ind][2], EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][3]: "+arrayCuentasCargo[ind][3], EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  arrayCuentas[ind][4]: "+arrayCuentasCargo[ind][4], EIGlobal.NivelLog.INFO);

                    tipoCuenta          = arrayCuentasCargo[ind][1].substring(0, 2);
                        claveProducto   = arrayCuentasCargo[ind][3];

                        if (numero_cuentas_trans_car%2==0)
            {
                           strSalida+="<TR  bgcolor=#CCCCCC>";
                           tipocolumna="<TD class=textabdatobs>";
                        }
                        else
            {
                           strSalida+="<TR  bgcolor=#EBEBEB>";
                           tipocolumna="<TD class=textabdatcla>";
                        }
                        if (SelOrigenEstatus!=null)
                        {
              if ("1".equals(SelOrigenEstatus[ind])){
                            strSalida+=tipocolumna+"<input type=checkbox NAME=origen CHECKED VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";}
                          else{
                 strSalida+=tipocolumna+"<input type=checkbox NAME=origen VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";}
            }
                        else

              strSalida+=tipocolumna+"<input type=checkbox NAME=origen VALUE=\""+arrayCuentasCargo[ind][1]+"|"+arrayCuentasCargo[ind][2]+"|"+arrayCuentasCargo[ind][4]+"|"+"\" onclick=\"return validarchcargo(this);\"></TD>";
                        strSalida+=tipocolumna+arrayCuentasCargo[ind][1]+"</TD>";
                        if (arrayCuentasCargo[ind][4].trim().length()>0){
                          strSalida+=tipocolumna+arrayCuentasCargo[ind][4].trim()+"</TD>";}
                        else{
               strSalida+=tipocolumna+"&nbsp;&nbsp;</TD>";}
                        if(SelOrigenImporte!=null)
                        {
                          if(SelOrigenImporte[ind]!=null)
                          {
                                  strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\""+SelOrigenImporte[ind]+"\"   SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                              if(SelOrigenImporte[ind].trim().length()==0)
                                          SelOrigenImporte[ind]="0.0";
                  selsumcargo=new Double(SelOrigenImporte[ind]).doubleValue();

                          }
                          else
                          {
                                  strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                  selsumcargo+=0.0;
                          }
                        }
                        else
                        {
                                strSalida+=tipocolumna+"<INPUT TYPE=text NAME=impc"+numero_cuentas_trans_car+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpcargo(this);\" ></TD>"+"</TR>";
                selsumcargo+=0.0;
                        }
                        numero_cuentas_trans_car++;
                 }
                 strSalida+="</TABLE>";

        }
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  ArregloCuentasAbono ", EIGlobal.NivelLog.INFO);

                if (arrayCuentasAbono!=null)
                {
                 strSalida2="<TABLE><TR bgcolor=#A4BEE4><TD align=center class=tittabdat>Seleccione</td><TD align=center  colspan=2 class=tittabdat>Cuenta Abono/M&oacute;vil</td><TD align=center  class=tittabdat>Importe</td></TR>";


                 String SelDestinoEstatus[]=null;
                 if(!seldestinoestatus.equals(""))
                 {
                    seldestinoestatus+="@";
                        SelDestinoEstatus= desentramaC(CADENA_VACIA+iseldestinocontador+"@"+seldestinoestatus,'@');
                 }

         String SelDestinoImporte[]=null;
                 if(!seldestinoimporte.equals(""))
                 {
                    seldestinoimporte+="@";
                        SelDestinoImporte= desentramaC(CADENA_VACIA+iseldestinocontador+"@"+seldestinoimporte,'@');
                 }



                 for (int ind=1;ind<=Integer.parseInt(arrayCuentasAbono[0][0]);ind++)
                 {
                         if (numero_cuentas_trans_abo%2==0)
             {
                                 strSalida2+="<TR  bgcolor=#CCCCCC>";
                                 tipocolumna="<TD class=textabdatobs>";
                         }
                         else
             {
                             strSalida2+="<TR  bgcolor=#EBEBEB>";
                             tipocolumna="<TD class=textabdatcla>";
                         }
             if(SelDestinoEstatus!=null)
             {
                if("1".equals(SelDestinoEstatus[ind])){
                                   strSalida2+=tipocolumna+"<input type=checkbox NAME=destino CHECKED VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";}
                                else{
                                   strSalida2+=tipocolumna+"<input type=checkbox NAME=destino VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";}
             }
                         else
                                 strSalida2+=tipocolumna+"<input type=checkbox NAME=destino VALUE=\""+arrayCuentasAbono[ind][1]+"|"+arrayCuentasAbono[ind][2]+"|"+arrayCuentasAbono[ind][4]+"|"+"\" onclick=\"return validarchabono(this);\"></TD>";

                         strSalida2+=tipocolumna+arrayCuentasAbono[ind][1]+"</TD>";
                         if (arrayCuentasAbono[ind][4].trim().length()>0){
                           strSalida2+=tipocolumna+arrayCuentasAbono[ind][4].trim()+"</TD>";}
                         else{
               strSalida2+=tipocolumna+"&nbsp;&nbsp;</TD>";}
                         if(SelDestinoImporte!=null)
             {
                            if(SelDestinoImporte[ind]!=null)
                                {
                                        strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\""+SelDestinoImporte[ind] +"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                        if(SelDestinoImporte[ind].trim().length()==0)
                                          SelDestinoImporte[ind]="0.0";
                       selsumabono=new Double(SelDestinoImporte[ind]).doubleValue();

                                }
                           else
                           {
                                   strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                               selsumabono+=0.0;
                           }
                         }
                         else
                         {
                                 strSalida2+=tipocolumna+"<INPUT TYPE=text NAME=impa"+numero_cuentas_trans_abo+" value=\"\"  SIZE=\"15\" MAXLENGTH=\"15\"  onChange=\"return validarimpabono(this);\"></TD>"+"</TR>";
                 selsumabono+=0.0;
                         }
                         numero_cuentas_trans_abo++;


                 } // for (ind)
                 strSalida2+="</TABLE>";
        }

                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  Termine Arreglos ", EIGlobal.NivelLog.INFO);
                //EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  strSalida "+strSalida, EIGlobal.NivelLog.INFO);
        //EIGlobal.mensajePorTrace("***TransferenciasMultiples.class  strSalida2 "+strSalida2, EIGlobal.NivelLog.INFO);


                int cont = 0;
                /*
                //System.out.println("--->cadena_cta_origen ="+cadena_cta_origen);
                //System.out.println("--->cadena_cta_origen ="+cadena_cta_destino);
                //System.out.println("Debe tener arroba sino da index out of bounds exception");
                */
				int nctascargo=0;
                int nctasabono=0;
                if ((cadena_cta_origen!=null)&&
                    (cadena_cta_destino!=null)&&
                        (!cadena_cta_origen.equals(""))&&
                        (!cadena_cta_destino.equals("")) )
                {
                  try
                  {
                         nctascargo=Integer.parseInt(cadena_cta_origen.substring(0,cadena_cta_origen.indexOf("@")));
             nctasabono=Integer.parseInt(cadena_cta_destino.substring(0,cadena_cta_destino.indexOf("@")));
             cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf("@")+1,cadena_cta_origen.length());
             cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf("@")+1,cadena_cta_destino.length());
                     simporte=simporte.substring(simporte.indexOf("@")+1,simporte.length());
                  }
                  catch (Exception e)
                  {
                	  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                  }
                }


        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class nctascargo:"+nctascargo, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class nctasabono:"+nctasabono, EIGlobal.NivelLog.INFO);



        if (nctascargo>1)
        {
           arrctas_origen+=cadena_cta_origen;
                   arrimporte+=simporte;
           for(int i=0;i<nctascargo;i++)
                   {
             arrctas_destino+=cadena_cta_destino;
                         arrconceptos+=concepto+"@";
                         arrfechas+=fecha+"@";
             arrestatus+="1@";
             contador++;
           }
        }
                else if (nctasabono>1)
                {
           arrctas_destino+=cadena_cta_destino;
                   arrimporte+=simporte;
           for(int i=0;i<nctasabono;i++)
                   {
             arrctas_origen+=cadena_cta_origen;
                         arrconceptos+=concepto+"@";
                         arrfechas+=fecha+"@";
             arrestatus+="1@";
             contador++;
                   }
                }
                else if ((nctascargo==1)&&(nctasabono==1))
                {
            contador=1;
            arrctas_origen+=cadena_cta_origen;
                        arrimporte+=simporte;
                        arrctas_destino+=cadena_cta_destino;
                    arrconceptos+=concepto+"@";
                        arrfechas+=fecha+"@";
            arrestatus+="1@";

                }

                fecha_hoy=ObtenFecha();

                if ("0".equals(sfac_programadas)){
                        fac_programadas=false;}
                else{
                        fac_programadas=true;}

                //campos_fecha=poner_campos_fecha(fac_programadas);

                String arrconceptosprueba="\""+arrconceptos+"\"";
                String arrctas_origenprueba="\""+arrctas_origen+"\"";
                String arrctas_destinoprueba="\""+arrctas_destino+"\"";
                req.setAttribute("diasInhabiles", strInhabiles);
                req.setAttribute("cuentas_destino", strSalida2);
                req.setAttribute("cuentas_origen", strSalida);

                req.setAttribute("arregloctas_origen1",arrctas_origenprueba);
                req.setAttribute("arregloctas_destino1",arrctas_destinoprueba);
                req.setAttribute("arregloctas_origen2",arrctas_origenprueba);
                req.setAttribute("arregloctas_destino2",arrctas_destinoprueba);
                req.setAttribute("arregloimportes1",arrimporte);
                req.setAttribute("arregloimportes2",arrimporte);
                req.setAttribute("arregloconceptos1",arrconceptosprueba);
                req.setAttribute("arregloconceptos2",arrconceptosprueba);
                req.setAttribute("arreglofechas1",arrfechas);
                req.setAttribute("arreglofechas2",arrfechas);
                req.setAttribute("arregloestatus1",arrestatus);
                req.setAttribute("arregloestatus2",arrestatus);
                req.setAttribute("contador1",Integer.toString(contador));
                req.setAttribute("contador2",Integer.toString(contador));
                req.setAttribute("fecha_hoy",fecha_hoy);
                req.setAttribute("fac_programadas1",sfac_programadas);
                req.setAttribute("INICIAL","NO");
                req.setAttribute("TIPOTRANS", ( String ) req.getParameter ("TIPOTRANS"));
                req.setAttribute("IMPORTACIONES","1");
                req.setAttribute("NMAXOPER"," "+nmaxoper);
                req.setAttribute("trans",( String ) req.getParameter ("trans"));
                req.setAttribute("ctascargo1",CADENA_VACIA+numero_cuentas_trans_car);
                req.setAttribute("ctasabono1",CADENA_VACIA+numero_cuentas_trans_abo);
                req.setAttribute("ctascargo2",CADENA_VACIA+numero_cuentas_trans_car);
                req.setAttribute("ctasabono2",CADENA_VACIA+numero_cuentas_trans_abo);

                req.setAttribute("selorigentrama","\""+selorigentrama+"\"");
                req.setAttribute("selorigencontador",CADENA_VACIA+iselorigencontador);
                req.setAttribute("seldestinotrama","\""+seldestinotrama+"\"");
                req.setAttribute("seldestinocontador",CADENA_VACIA+iseldestinocontador);

        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class selorigentrama:"+selorigentrama, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class seldestinotrama:"+seldestinotrama, EIGlobal.NivelLog.INFO);
        //naa
		strorig=arrctas_origenprueba; //cuentas origen
        EIGlobal.mensajePorTrace("1.1) CUENTAS ORIGEN  arregloctas_origen2"+strorig, EIGlobal.NivelLog.INFO);
		strdest=arrctas_destinoprueba; //cuentas destino
        EIGlobal.mensajePorTrace("2.1) CUENTAS DESTINO arregloctas_destino2"+strdest, EIGlobal.NivelLog.INFO);


                tabla="";

                if ((nctascargo==1)||(nctasabono==1))

      {

                   String[] Ctas_origen  = desentramaC(Integer.toString(contador)+"@"+arrctas_origen,'@');
                   String[] Ctas_destino = desentramaC(Integer.toString(contador)+"@"+arrctas_destino,'@');
                   String[] Importes     = desentramaC(Integer.toString(contador)+"@"+arrimporte,'@');
                   String[] Fechas       = desentramaC(Integer.toString(contador)+"@"+arrfechas,'@');
                   String[] Conceptos    = desentramaC(Integer.toString(contador)+"@"+arrconceptos,'@');
                   String[] estatus      = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');


                   tabla=tabla+"<TR >";
                   tabla=tabla+"<TD align=center width=26 class=tittabdat></TD>";
                   tabla=tabla+"<TD align=center width=200 class=tittabdat>Cuenta cargo</TD>";
                   tabla=tabla+"<TD align=center colspan=2 class=tittabdat>Cuenta abono/M&oacute;vil</TD>";
                   tabla=tabla+"<TD align=center width=90 class=tittabdat>Importe</TD>";
                   tabla=tabla+"<TD align=center width=60 class=tittabdat>Fecha aplicaci&oacute;n</TD>";
                   tabla=tabla+"<TD align=center class=tittabdat>Concepto</TD>";
                   tabla=tabla+"</TR>";



                   int residuo=0;
                   String clase="";
           for (indice=1;indice<=contador;indice++)
                   {
                          cadena_cta_origen  = Ctas_origen[indice];
                          cadena_cta_destino = Ctas_destino[indice];

                          cta_origen=cadena_cta_origen.substring(0,cadena_cta_origen.indexOf('|'));
                          cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length());
              cadena_cta_origen=cadena_cta_origen.substring(cadena_cta_origen.indexOf('|')+1,cadena_cta_origen.length()-1);
                          if (cadena_cta_destino.indexOf((int)'|') == -1 )
                          {
                                 cta_destino = cadena_cta_destino;
                                 cadena_cta_destino="";
                          }
                          else
                          {
                                  cta_destino=cadena_cta_destino.substring(0,cadena_cta_destino.indexOf('|'));
                                  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length());
                  cadena_cta_destino=cadena_cta_destino.substring(cadena_cta_destino.indexOf('|')+1,cadena_cta_destino.length()-1);
                          }

                          residuo=indice%2;
                          tabla=tabla+"<TR>";
                          if (residuo==0){
                                clase="textabdatobs";}
                          else
                                clase="textabdatcla";

                          valor=estatus[indice];
                          nombre_campo="CH"+indice;
                  if ("0".equals(valor)){
                            tabla=tabla+ "<TD align=right class="+clase+"> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+"></TD>";}
                          else
                            tabla=tabla+ "<TD align=right class="+clase+"> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+" checked ></TD>";


              importetabla+=new Double(Importes[indice]).doubleValue();
                          tabla=tabla+"<TD class="+clase+" align=left  nowrap >"+cta_origen+" "+cadena_cta_origen+"</TD>";
                          tabla=tabla+"<TD class="+clase+" nowrap align=left colspan=2 >"+cta_destino+" "+cadena_cta_destino+"</TD>";
                          tabla=tabla+"<TD class="+clase+" align=right nowrap>"+FormatoMoneda(Importes[indice])+"</TD>";
                          tabla=tabla+"<TD class="+clase+" align=center nowrap>"+Fechas[indice]+"</TD>";
                          tabla=tabla+"<TD class="+clase+"  align=left nowrap>"+Conceptos[indice]+"</TD>";
                  tabla=tabla+"</TR>";
  				if(grabaArchivo){
  					cadena = cta_origen.concat(" ").concat(cadena_cta_origen).concat(" \t")
  							.concat(cta_destino).concat(" ").concat(cadena_cta_destino).concat(" \t")
  									.concat(FormatoMoneda(Importes[indice])).concat(" \t")
  									.concat(Fechas[indice]).concat(" \t").concat(Conceptos[indice]).concat(" \t");
  					ArcSal.escribeLinea(cadena + "\r\n");
  					EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  "+cadena, EIGlobal.NivelLog.DEBUG);
  				}
                   } // for ( indice )

                   tabla=tabla+"</TABLE>";
       			if( grabaArchivo ) {
       				ArcSal.cierraArchivo();
       			}

       			ArchivoRemoto archR = new ArchivoRemoto();
       			if(!archR.copiaLocalARemoto(fileOut,"WEB")) {
       				EIGlobal.mensajePorTrace("***TransferenciasMultiples.class No se pudo crear archivo para exportar transferencias multiples", EIGlobal.NivelLog.ERROR);
       			}
        }

                 ///////////////////////////////////////////////
                 req.setAttribute("contadoroperaciones","Total de registros:"+contador+" por "+ FormatoMoneda(CADENA_VACIA+importetabla));
                 req.setAttribute("tabla",tabla);
                 if(contador>0){ //RMV linea original
                   //req.setAttribute("botontransferir","<td align=right width=89><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a></td>");
				   //Req. Q1466. Cambio boton con nombre (Validar)

                   req.setAttribute("botontransferir","<td align=center width=300><a href=\"javascript:confirmacion();\" border=0><img src=/gifs/EnlaceMig/gbo25222.gif border=0 ></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                   										"<a href=\"javascript:scrImpresion();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25240.gif></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                   										"<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>");}
                 else
                   req.setAttribute("botontransferir","");

                 req.setAttribute("MenuPrincipal", session.getStrMenu());
         req.setAttribute("newMenu", session.getFuncionesDeMenu());
         req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s26430bh",req));
                 if ((iselorigencontador>0)&&(iseldestinocontador>0))
             {
                   req.setAttribute("campos_totales",Poner_totales(sumaMontoCargo, sumaMontoAbono));
                   req.setAttribute("campos_concepto_fecha",Poner_concepto_fecha(poner_calendario_programadas(fecha,fac_programadas,"fecha_completa","WindowCalendar()"),
                                                                                   poner_campos_fecha(fecha,fac_programadas),concepto));
                   req.setAttribute("campos_botones",Poner_botones());
                 }
                 else
             {
                   req.setAttribute("campos_totales","");
                   req.setAttribute("campos_concepto_fecha","");
                   req.setAttribute("campos_botones","");

                 }
             evalTemplate("/jsp/TransferenciasMultiplesConfir.jsp", req, res);

                 return 1;

        }  // m�todo




   /**
	 * M�todo que realiza la ejecuci�n d elas operaciones de referencias m�ltiples
	 * @param clave_perfil clave de peril
	 * @param strDebug 
	 * @param usuario usuario
	 * @param contrato contrato
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera de exito
	 * */
   public int ejecucion_operaciones_transferencias_multiples(String clave_perfil, String strDebug, String usuario, String contrato,HttpServletRequest req, HttpServletResponse res)
              throws ServletException, IOException {

        EIGlobal.mensajePorTrace("***transferenciasMultiples.class Entrando a ejecucion_operaciones_transferencias ", EIGlobal.NivelLog.INFO);

        StringBuilder mensaje_salida = new StringBuilder();
		String coderror = "";
		tipo_operacion = TRAN;
		String cta_origen = "";
		String cta_destino = "";
		String des_cta_origen = "";
		String des_cta_destino = "";
		String importe_string = "";
		double importe = 0.0;
		String concepto = "";
        String sfecha_programada="";
        String bandera_fecha_programada="1";
        String fecha_programada_b="";
        int salida=0;
        Double importedbl;
        boolean programada=false;
        String tipo_cta_origen="";
        String tipo_cta_destino="";
        String cadena_cta_origen="";
        String cadena_cta_destino="";
        String arreglo_ctas_origen="";
        String arreglo_ctas_destino="";
        String arreglo_importes="";
        String arreglo_fechas="";
        String arreglo_conceptos="";
        String arreglo_estatus="";
        int contador =0;
        String scontador="";
		int indice = 0;
		String fecha = "";
		String bandera_estatus = "";
		String fecha_hoy = "";
		String miSalida = "";
		String tramaok = "";
		String sesion_cta_destino = "";
		String strdest2 = "";
		String strorig2 = "";
		String mensajeError = "";
		boolean comuError = false;
        String[] arrCuentas=new String[4]; //Numero de cuentas  X 2 (cuenta|tipo|)
        scontador=( String ) req.getParameter("contador2");
        contador=Integer.parseInt(scontador);

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		/***********************************Req. Q1466 Construccion variables sesion p/control recarga ****************/
		boolean banderaTransaccion = true;

		if(req.getSession().getAttribute("Recarga_Ses_Multiples")!=null && verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
		{
			EIGlobal.mensajePorTrace( "Transferencias Multiples El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.INFO);
			banderaTransaccion =false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
		{
			EIGlobal.mensajePorTrace("Transferencias Multiples Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.INFO);
			req.getSession().removeAttribute("Recarga_Ses_Multiples");
			banderaTransaccion=false;
		}
		else
		if(verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false) )
		{
			EIGlobal.mensajePorTrace( "Transferencias Multiples El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.INFO);
			banderaTransaccion = false;
		}
		else
		if(req.getSession().getAttribute("Recarga_Ses_Multiples")==null && !verificaArchivos(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses",false))
        {
		    EIGlobal.mensajePorTrace( "Transferencias Multiples Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.INFO);
			banderaTransaccion = true;
		}

		/***********************************Req. Q1466 Termina construccion variables control recarga sesion ********************************/
		if (banderaTransaccion) //Q1466 Procesar si no es recarga de la pantalla ejecucion
		{
			//Se crea el archivo para control de recarga sesion, y puesta de var. Recarga
			EI_Exportar arcTmp=new EI_Exportar(Global.DOWNLOAD_PATH+req.getSession().getId().toString()+".ses");
			if(arcTmp.creaArchivo())
				EIGlobal.mensajePorTrace( "Transferencias Multiples - El archivo.ses Se creo exitosamente ", EIGlobal.NivelLog.INFO);
			req.getSession().setAttribute("Recarga_Ses_Multiples","TRUE");

			//***** everis Cerrando RandomAccessFile (EI_Exportar) 08/05/2008  ..inicio
			arcTmp.cierraArchivo();
			//****  everis Cerrando RandomAccessFile (EI_Exportar) 08/05/2008  ..fin

			// Carga de Arreglos parametros a arreglos locales.
			arreglo_ctas_origen=( String ) req.getParameter("arregloctas_origen2");
			arreglo_ctas_destino=( String ) req.getParameter("arregloctas_destino2");
			arreglo_importes=( String ) req.getParameter("arregloimportes2");
			arreglo_fechas=( String ) req.getParameter("arreglofechas2");
			arreglo_conceptos=( String ) req.getParameter("arregloconceptos2");
			arreglo_estatus=( String ) req.getParameter("arregloestatus2");


		// GETRONICS CP MEXICO, NAA.  EN ATENCION A LA INCIDENCIA Q1322
		// Adicional Correccion, se agrega variable auxiliar para no alterar cad original si se recarga la sesion
			String strorig_aux="";
			EIGlobal.mensajePorTrace("1.1) CADENA ORIGEN variable-arregloctas_origen2 " +strorig, EIGlobal.NivelLog.INFO);
			strorig_aux=strorig.substring(1,strorig.length()-1);
			EIGlobal.mensajePorTrace("1.1) CUENTA ORIGEN variable-arregloctas_origen2 " +strorig_aux, EIGlobal.NivelLog.INFO);
			strorig2=arreglo_ctas_origen;
			EIGlobal.mensajePorTrace("1.2) CADENA ORIGEN arreglo_ctas_origen          " +strorig2, EIGlobal.NivelLog.INFO);

			if (!strorig_aux.equals(strorig2))
		     {
  		      EIGlobal.mensajePorTrace("ERROR: Se Modifico la(s) Cuenta(s) Cargo", EIGlobal.NivelLog.INFO);
			  comuError=true;
              mensajeError="Transaccion No V�lida";
			 }
		//Misma correccion con cadena auxiliar para cuenta destino.
			String strdest_aux="";
			EIGlobal.mensajePorTrace("2.1) CADENA DESTINO variable-arregloctas_destino2 "+strdest, EIGlobal.NivelLog.INFO);
 			strdest_aux=strdest.substring(1,strdest.indexOf('|'));
			EIGlobal.mensajePorTrace("2.1) CUENTA DESTINO variable-arregloctas_destino2 "+strdest_aux, EIGlobal.NivelLog.INFO);
			strdest2=arreglo_ctas_destino;
			EIGlobal.mensajePorTrace("2.2) CADENA DESTINO arreglo_ctas_destino          "+strdest2, EIGlobal.NivelLog.INFO);
  			strdest2=strdest2.substring(0,strdest2.indexOf('|'));
			EIGlobal.mensajePorTrace("2.2) CUENTA DESTINO arreglo_ctas_destino          "+strdest2, EIGlobal.NivelLog.INFO);

			if (!strdest_aux.equals(strdest2))
		    {
  		      EIGlobal.mensajePorTrace("ERROR: -Se Modifico la(s) Cuenta(s) Abono", EIGlobal.NivelLog.INFO);
              comuError=true;
              mensajeError="Transaccion No V�lida";
		    }

  			if(comuError)
			{
				despliegaPaginaError(mensajeError,"Transferencias Multiples","Transferencias &gt; Multiples &gt; Transferencias &gt; Env�o","s25470h", req, res);
				return 1;
       		}


//NAA.
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_ctas_origen  "+arreglo_ctas_origen, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_ctas_destino "+arreglo_ctas_destino, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_importes "+arreglo_importes, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_fechas "+arreglo_fechas, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_conceptos "+arreglo_conceptos, EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_estatus "+arreglo_estatus, EIGlobal.NivelLog.INFO);

			fecha_hoy=ObtenFecha();


			miSalida = "arreglo_ctas_origen "+arreglo_ctas_origen + "\n arreglo_ctas_destino "+arreglo_ctas_destino + "\n arreglo_importes "+arreglo_importes + "\n arreglo_fechas "+arreglo_fechas + "\n arreglo_conceptos "+arreglo_conceptos + "\n arreglo_estatus "+arreglo_estatus + "\n fecha_hoy "+fecha_hoy ;


			String[] Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');
			String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
			String[] Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
			String[] Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
			String[] Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');
			String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');


			mensaje_salida.append("<table width=760 border=0 cellspacing=2 cellpadding=3>");
            mensaje_salida.append("<tr><td class=textabref colspan=8>&Eacute;ste es el resultado de sus operaciones</td></tr>");
			mensaje_salida.append("<TR>");
			mensaje_salida.append("<TD align=left class=tittabdat width=200>Cuenta cargo</TD>");
			mensaje_salida.append("<TD class=tittabdat align=left width=200>Cuenta abono/M&oacute;vil</TD>");
			mensaje_salida.append("<TD class=tittabdat align=center width=85 >Importe</TD>");
			mensaje_salida.append("<TD class=tittabdat align=center width=75 >Fecha de Aplicaci&oacute;n</TD>");
			mensaje_salida.append("<TD class=tittabdat align=center width=230>Concepto</TD>");
            mensaje_salida.append("<TD class=tittabdat align=center width=60>Estatus</TD>");
			mensaje_salida.append("<TD class=tittabdat align=center width=60>Referencia</TD>");
			mensaje_salida.append("<TD class=tittabdat align=center width=80>Saldo actualizado</TD>");

			mensaje_salida.append("</TR>");

			boolean  par=false;

			int contadorok=0;
            int contadorprog=0;
            int contadormanc=0;
            int contadorerror=0;
			String trama_entrada="";
			boolean fecha_menor=false;
            String sreferencia="";
            //ring mensajeerror="";

			String clase="";
            String conceptocompleto=""; //*****Cambio para comprobante fiscal
            int longi=0;                //*****Cambio para comprobante fiscal
            int j=0;                    //*****Cambio para comprobante fiscal
            String estatus = "";
            String folios = "";
            StringBuilder codErrorOper =  new StringBuilder();

				int contTransRechazadas = 0;
				// Se crea mapa para envio a conector lynx
				Map<Integer, String> map = new HashMap<Integer, String>();
				if (Global.LYNX_INICIALIZADO) {
					// Se cargan valores por default para el mensaje
					map = EnlaceLynxConstants.cargaValoresDefaultPB(map, req);
					map.put(7, CuentasDAO.consultarIdUsuarioContrato(contrato)); // Identificador del cliente dueno de la cuenta
					EIGlobal.mensajePorTrace("La configuracion de Lynx esta incializada y se cargaron valores default de trama PB", EIGlobal.NivelLog.DEBUG);
				} 
            for (int indice1=1;indice1<=contador;indice1++)
			{
				bandera_estatus=Estatus[indice1];
				if ("1".equals(bandera_estatus))
				{

					cadena_cta_origen  = Ctas_origen[indice1];
					cadena_cta_destino = Ctas_destino[indice1];
					importe_string     = Importes[indice1];
					concepto           = Conceptos[indice1];
					fecha              = Fechas[indice1];

					arrCuentas= desentramaC("3|" + cadena_cta_origen,'|');
					cta_origen = arrCuentas[1];
					tipo_cta_origen=arrCuentas[2];
                    des_cta_origen=arrCuentas[3];
                    if (cadena_cta_destino.indexOf((int)'|') == -1 )
                         cadena_cta_destino = cadena_cta_destino + "|NR||" ;

					arrCuentas= desentramaC("3|" + cadena_cta_destino,'|');
                    cta_destino = arrCuentas[1];
					tipo_cta_destino=arrCuentas[2];
					des_cta_destino=arrCuentas[3];
                    importedbl = new Double (importe_string);
					importe = importedbl.doubleValue();

					sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());

                    fecha_menor=checar_fecha_menor(fecha);
					mensaje_salida.append("<TR>");

                    if (!par)
					{
						par=true;
                        clase="textabdatobs";
					}
					else
					{
						par=false;
                        clase="textabdatcla";
					}

					if (!fecha_menor)
					{
						programada=checar_si_es_programada(fecha);
						trama_entrada="";

						if (("49".equals(cta_destino.substring(0,2)))&&(cta_destino.length()==16)){
						tipo_operacion="PAGT";}
						else{
						tipo_operacion=TRAN;}

                                  //********************************
						concepto=concepto.trim();
                        if (concepto.trim().length()==0)
							concepto="TRANSFERENCIA ELECTRONICA";
                                 //*********************************
                        conceptocompleto="";        //*****Cambio para comprobante fiscal
                        if (concepto.length()>30){  //*****Cambio para comprobante fiscal
							conceptocompleto=concepto.substring(0,30);} //*****Cambio para comprobante fiscal   antes concepto completo
                        else                       //*****Cambio para comprobante fiscal
                        {                          //*****Cambio para comprobante fiscal
                            conceptocompleto=concepto;//*****Cambio para comprobante fiscal
							longi=concepto.length();  //*****Cambio para comprobante fiscal
							for(j=0;j<(30-longi);j++) //*****Cambio para comprobante fiscal
                                conceptocompleto+=" ";  //*****Cambio para comprobante fiscal
						}


						if (programada==false){
							trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
                                cta_destino+"|"+tipo_cta_destino+"|"+importe_string+"|"+conceptocompleto+"| |";}
						else{
							trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cta_origen+"|"+tipo_cta_origen+"|"+
                                cta_destino+"|"+tipo_cta_destino+"|"+importe_string+"|0|"+conceptocompleto+"| |";}

                 //TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
                        ServicioTux tuxGlobal = new ServicioTux();
                        //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

						//ivn Q05-0001087 inicio lineas agregadas 1
						String IP = " ";
						String fechaHr = "";												//variables locales al m�todo
						IP = req.getRemoteAddr();											//ObtenerIP
						java.util.Date fechaHrAct = new java.util.Date();
						SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
						fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
						//ivn Q05-0001087 fin lineas agregada 1

						/*ivn Q05-0001087 linea modificada: se agreg� la variable fechaHr
						 * EIGlobal.mensajePorTrace("***transferencia  WEB_RED-Transferencias>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);*/
						//INICIO - ESC 1/JUN/2010 - VALIDACION DE CUENTAS DE CARGO Y DE ABONO
						boolean cuentasValidas = true;
						String moduloOrigen =  IEnlace.MCargo_transf_pesos;
						String moduloDestino = IEnlace.MAbono_transf_pesos;
						String mensajeCargo = "" , mensajeAbono = "";

						ValidaCuentaContrato validaCuentas = null;

						try{
							 validaCuentas = new ValidaCuentaContrato();

							//String[] datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_origen, IEnlace.MCargo_transf_pesos);
							String[] datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_origen, moduloOrigen);
							if(datosCuenta == null){
								cuentasValidas = false;
								mensajeCargo = "CUENTA DE CARGO NO PERTENECE AL CONTRATO";
								EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> Cuenta de cargo" +
										"No pertenece al contrato : "+cta_origen, EIGlobal.NivelLog.INFO);
							}
							if(cuentasValidas){

								//datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_destino, IEnlace.MAbono_transf_pesos);
								datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil, cta_destino, moduloDestino);
								if(datosCuenta == null){
									cuentasValidas = false;
									mensajeCargo = "CUENTA DE ABONO NO PERTENECE AL CONTRATO";
									EIGlobal.mensajePorTrace("***transferencia.ejecucion_operaciones_transferencias_con_tabla-> Cuenta de abono" +
											"No pertenece al contrato : "+cta_origen, EIGlobal.NivelLog.INFO);

								}
							}
						}catch(Exception e)
						{
							StackTraceElement[] lineaError;
							lineaError = e.getStackTrace();
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(fechaHr+ "**"+ this.getClass().getName() +"*** en validacion de cuentas contrato transferencias " + e.getMessage(), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(fechaHr+ "**"+ this.getClass().getName() +"*** en validacion de cuentas contrato transferencias Linea " + lineaError[0].getLineNumber(), EIGlobal.NivelLog.INFO);
						}finally{
							try{
								validaCuentas.closeTransaction();
							}catch(Exception e){
								StackTraceElement[] lineaError;
								lineaError = e.getStackTrace();
								EIGlobal.mensajePorTrace(fechaHr+"*** en validacion de cuentas contrato" + e.getMessage(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(fechaHr+"*** en validacion de cuentas contrato linea " + lineaError[0].getLineNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}


						//FIN ESC 1/JUN/2010


						try
						{
							//INICIO - ESC 1/JUN/2010 - VALIDACION DE CUENTAS DE CARGO Y DE ABONO
							if(cuentasValidas){
								EIGlobal.mensajePorTrace("Cuentas validas, incia bloque transferencias multiples Lynx", EIGlobal.NivelLog.DEBUG);
								boolean autorizoLynx = true; // Variable para indicar si se envia o no a tuxedo
								boolean procesaLynx = Global.LYNX_INICIALIZADO // Indica si aplica para llamado a Lynx
										&& TRAN.equals(tipo_operacion)
										&& !"P".equalsIgnoreCase(tipo_cta_destino); // Se verifica que la transaccion sea de tipo TRAN
								EIGlobal.mensajePorTrace("Tipos de cuentas origen y destino: " + tipo_cta_origen + " - " + tipo_cta_destino + 
										", tipo_operacion: " + tipo_operacion + ", procesa a Lynx: " + procesaLynx, EIGlobal.NivelLog.DEBUG);
								if (procesaLynx) {
									EIGlobal.mensajePorTrace("El tipo de operacion es: " + tipo_operacion, EIGlobal.NivelLog.DEBUG);									
									SimpleDateFormat format = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, EnlaceLynxConstants.LOCALE_MX);
									SimpleDateFormat formatoHora = new SimpleDateFormat(EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, EnlaceLynxConstants.LOCALE_MX);
                                    StringBuilder sb = new StringBuilder();
									// Se agregan valores faltantes a mapa de valores para armar el mensaje.
									map.put(3, formatoHora.format(new Date()) + cta_origen); // Id unico de latransaccion
									map.put(6, usuario); // Identificador del cliente
									map.put(8, contrato); // Identificador de contrato
									map.put(9, session.getNombreContrato()); // Nombre de contrato
                                    map.put(22, EnlaceLynxConstants.FORMLIQ_MISMO_DIA); // Mismo dia
                                    map.put(25, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);  // Tipo de cuenta origen
									map.put(26, cta_origen); // Numero de cuenta donde el cliente desea realizar la transaccion
									map.put(30, cta_destino); // Numero de Cuenta a la que se hara el Abono
									map.put(31, "0014"); // Entidad financiera destino, a la que pertenece el beneficiario de la operacion
									map.put(32, EnlaceLynxConstants.PAIS); //Pais de la cuenta destino 
									map.put(35, format.format(new Date())); // Fecha y hora del pago
									map.put(36, map.get(35)); // Fecha y hora en que se envia la operacion al aplicativo para su tratamiento
									map.put(37, EnlaceLynxConstants.TRANSFERENCIA); // Comentario asociado al pago (transferencia, traspaso de saldo, etc)
									map.put(38, EnlaceLynxUtils.formateaImporte(importe_string, sb)); // Importe de la transaccion
									map.put(40, map.get(38)); // Importe en moneda original
									map.put(48, EnlaceLynxConstants.PAIS); // Pais del movil recargado (no encontrado)
                                    map.put(52, EnlaceLynxConstants.TIPO_CARGO); // Tipo cargo
                                    map.put(55, EnlaceLynxConstants.COMPROBANTE_NINGUNO); // Sin comprobante fiscal
									map.put(65, EnlaceLynxUtils.formateaFecha(sfecha_programada, sb)); // Fecha planificada para hacer la transferencia
									// Se crea objeto de respuesta para almacenar el codigo y mensaje de respuesta de la operacion hacia lynx
									EIGlobal.mensajePorTrace("Inicia envio de mensaje a Lynx", EIGlobal.NivelLog.DEBUG);
									RespuestaLynxDTO respuestaLynx = ConectorLynx.enviaMsg(
                                            map, EnlaceLynxConstants.TIPO_MSG_TMB);
									EIGlobal.mensajePorTrace("LYNX: Termina envio, respuesta: " + respuestaLynx.getCodError() + ", respuesta de riesgo de fraude: " + respuestaLynx.getCodErrorLynx(), EIGlobal.NivelLog.INFO);
									if (LynxConstants.ERROR_CODE_OK.equals(respuestaLynx.getCodError())) { // Si se efectuo con exito el llamado a linx
										if (respuestaLynx.getCodErrorLynx() != 0) {
											EIGlobal.mensajePorTrace("Conector obteniendo respuesta de riesgo de fraude: " + respuestaLynx.getCodErrorLynx(), EIGlobal.NivelLog.DEBUG);
											autorizoLynx = false;
											contTransRechazadas++;
											coderror = EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA;
											codErrorOper.append(EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA).append("-");
										}
									} else {
                                        procesaLynx = false;
                                    }
								}
								EIGlobal.mensajePorTrace("Lynx Autorizo operacion: " + autorizoLynx, EIGlobal.NivelLog.DEBUG);
								if (autorizoLynx) {
									// Enviar mensaje a tuxedo para realizar operacion
									Hashtable hs = tuxGlobal.web_red(trama_entrada);
									EIGlobal.mensajePorTrace(fechaHr+"***transferencia  WEB_RED-Transferencias>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
									coderror = (String) hs.get("BUFFER");
									codErrorOper.append(hs.get("COD_ERROR")).append("-");
								} else {
                                    // Grabar error en bitacora
                                    BitaTCTBean beanTCT = new BitaTCTBean();
                                    BitaHelper bh = new BitaHelperImpl(req, session, sess);
                                    beanTCT = bh.llenarBeanTCT(beanTCT);
                                    beanTCT.setReferencia(obten_referencia_operacion(
                                            Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
                                    sreferencia = String.valueOf(beanTCT.getReferencia());
                                    beanTCT.setCodError(EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA);
                                    if (session.getContractNumber() != null) {
                                        beanTCT.setNumCuenta(session.getContractNumber().trim());
                                    }
                                    if (session.getUserID8() != null) {
                                        beanTCT.setUsuario(session.getUserID8().trim());
                                        beanTCT.setOperador(session.getUserID8().trim());
                                    }
                                    beanTCT.setTipoOperacion("TRAN");
                                    beanTCT.setCuentaOrigen(cta_origen);
                                    beanTCT.setCuentaDestinoFondo(cta_destino);
                                    beanTCT.setImporte(importe);
									BitaHandler.getInstance().insertBitaTCT(beanTCT);
                                }
								// Envia mensaje a Monitor Plus
								EIGlobal.mensajePorTrace(" MP *******************  |"
										+ " <---- TRAN ----> " + tipo_operacion
										+ " <---- CODERROR ----> "+coderror.substring(0, 2) + " | "
										+ " <---- MANC ----> " + coderror.substring(0, 4)
										+ " | \n ******************************* "
										, EIGlobal.NivelLog.DEBUG);
                    			String numRefe = coderror.substring(9, 16);
                                if (TRAN.equals(tipo_operacion) && EnlaceMonitorPlusConstants.OK.equals(coderror.substring(0, 2))
                                    || EnlaceMonitorPlusConstants.MANC.equals(coderror.substring(0, 4))
                                     ) {
                                	// Metodo de mensaje a Monitor Plus
                                	EIGlobal.mensajePorTrace("-----> SE ENVIAN PARAMETROS PARA MAPEO A ** MP **", EIGlobal.NivelLog.DEBUG);
                                	TransferenciasMonitorPlusUtils.enviarTMultiple(req, TRAN, importe_string, (cta_origen + "|" + cta_destino), 
                                    		(cadena_cta_origen +"|"+ cadena_cta_destino +"|"+ (tipo_operacion + "0000")),
                                    		numRefe, tipo_operacion, session, IEnlace.SUCURSAL_OPERANTE, tipo_cta_destino, codErrorOper.toString());
                                }
									
								EIGlobal.mensajePorTrace("***transferencia<<"+coderror, EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace("***codErrorOper["+indice1+"]--->>"+codErrorOper.toString(), EIGlobal.NivelLog.DEBUG);
								EIGlobal.mensajePorTrace("Validando para hacer envio informativo a lynx de operaciones canceladas por Enlace o no autorizadas por Lynx", EIGlobal.NivelLog.DEBUG);
								if (procesaLynx && ((coderror!= null 
                                        && !"OK".equals(coderror.substring(0,2)) 
                                        && !"MANC".equals(coderror.substring(0,4))) 
                                        || !autorizoLynx)) {
									RespuestaLynxDTO respuestaDenegadas = null;
									Map<Integer, String> mapDenCan = new HashMap<Integer, String>();
									// Se cargan valores por default para el mensaje
									mapDenCan = EnlaceLynxConstants.cargaValoresDefaultCF(mapDenCan, cta_origen, (map.containsKey(3)) ? map.get(3) : "");
									mapDenCan.put(6, autorizoLynx ? EnlaceLynxConstants.CODRESP_ERR : EnlaceLynxConstants.CODRESP_DEN_LYNX);
									// Se agregan valores faltantes a mapa de valores para armar el mensaje.
									respuestaDenegadas = ConectorLynx.enviaMsg(mapDenCan, EnlaceLynxConstants.TIPO_MSG_ACTUALIZACION_RESP);
									EIGlobal.mensajePorTrace("Lynx: actualizacion de cancelacion y/o denegacion, respuesta: "
                                            + respuestaDenegadas.getCodError() + " /"
                                            + respuestaDenegadas.getMsgError(),
                                            EIGlobal.NivelLog.DEBUG);
								}
							}else{
								coderror ="CTASINVA";
							}
							//FIN ESC 1/JUN/2010
						}
						catch( java.rmi.RemoteException re)
						{
							EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}

						//TODO BIT CU2021, ejecuta las transferencias
						/*
						 * VSWF-HGG-I
						 */
						 if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						try {
							BitaTransacBean bt = new BitaTransacBean();
							BitaHelper bh = new BitaHelperImpl(req, session, sess);

							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_MULTI_AGREGAR_REGISTRO_BIT);
							if (contrato != null) {
								bt.setContrato(contrato.trim());
							}
							if (cta_origen != null) {
								if(cta_origen.length() > 20){
									bt.setCctaOrig(cta_origen.substring(0,20));
								}else{
									bt.setCctaOrig(cta_origen.trim());
								}
							}
							if (cta_destino != null) {
								if(cta_destino.length() > 20){
									bt.setCctaDest(cta_destino.substring(0,20));
								}else{
									bt.setCctaDest(cta_destino.trim());
								}
							}
							if (importe_string != null) {
								bt.setImporte(Double.parseDouble(importe_string.trim()));
							}
							if(coderror != null){
								if("OK".equals(coderror.substring(0,2))){
									bt.setIdErr(tipo_operacion + "0000");
								}else if(coderror.length() > 8){
									bt.setIdErr(coderror.substring(0,8));
								}else{
									bt.setIdErr(coderror.trim());
								}
							}
							if (tipo_operacion != null) {
								bt.setServTransTux((programada == false)
										? tipo_operacion
										: "PROG");
							}
							bt.setBancoDest("SANTANDER");
							bt.setTipoMoneda("MN");
							if (fecha != null) {
								if (programada) {
									bt.setFechaProgramada(BitaHelperImpl.MdyToDate(fecha));
								}
							}
							if(coderror!=null){
								if ((coderror.length()>=16)&&(!("MANC".equals(coderror.substring(0,4)))))
									sreferencia=coderror.substring(8,16);
								sreferencia=sreferencia.trim();
								bt.setReferencia(Long.parseLong(sreferencia));
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
								&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
										.equals(BitaConstants.VALIDA)) {
								bt.setIdToken(session.getToken().getSerialNumber());
							}
							/*VSWF I Autor=BMB fecha=06-05-08 OT=ALENTREBBIA Desc=Se bitacoriza el beneficiario*/
							bt.setBeneficiario((des_cta_destino==null)?" ":des_cta_destino);
							/*VSWF F*/
							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
						 }
						/*
						 * VSWF-HGG-F
						 */

						req.setAttribute("strDebug", "");
						sreferencia="";

						EIGlobal.mensajePorTrace("strDebug "+strDebug, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("***transferencia  coderror>>"+coderror, EIGlobal.NivelLog.DEBUG);


						mensaje_salida.append("<TD class="+clase+" align=center nowrap >"+cta_origen+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center nowrap >"+cta_destino+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=right nowrap  >"+FormatoMoneda(importe_string)+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center nowrap>"+fecha+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center nowrap>"+concepto.trim()+"</TD>");


						if(coderror!=null)
						{

							if ((coderror.length()>=16)&&(!("MANC".equals(coderror.substring(0,4)))))
								sreferencia=coderror.substring(8,16);
							sreferencia=sreferencia.trim();


							if ("OK".equals(coderror.substring(0,2)))
							{
								if((coderror.length()>16)&&("MANC").equals(coderror.substring(16,coderror.length())))
								{
									estatus = estatus + " MANCOMUNADA" + ",";
									contadormanc++;
									mensaje_salida.append("<TD class="+clase+" align=left >Operaci&oacute;n Mancomunada</TD>");
									mensaje_salida.append("<TD class="+clase+" align=center nowrap>"+sreferencia+"</TD>");
									mensaje_salida.append("<TD class="+clase+" align=right nowrap>-------</TD>");
								}
								else
								{
									if (programada==false)
									{
										estatus = estatus + " REALIZADA" + ",";
										contadorok++;
										mensaje_salida.append("<TD class="+clase+" align=left >Operaci&oacute;n realizada</TD>");
										cta_origen+="  "+des_cta_origen;
										cta_destino+=" "+des_cta_destino;
										tramaok+=contadorok+"|0|"+sreferencia.trim()+"|"+importe_string+"|"+cta_origen+"|"+cta_destino+"|"+concepto+"|@";
										mensaje_salida.append("<TD class="+clase+" align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value,"+contadorok+");\" >"+sreferencia+"</A></TD>");
										if (coderror.length()>=30){
											mensaje_salida.append("<TD class="+clase+" align=right nowrap>"+ FormatoMoneda(coderror.substring(16,30))+"</TD>");}
										else{
											mensaje_salida.append("<TD class="+clase+" align=right nowrap>-------</TD>");}
									}
									else
									{
										estatus = estatus + " PROGRAMADA" + ",";
										contadorprog++;
										mensaje_salida.append("<TD class="+clase+" align=left >Operaci&oacute;n programada</TD>");
										mensaje_salida.append("<TD class="+clase+" align=center nowrap>"+sreferencia+"</TD>");
										mensaje_salida.append("<TD class="+clase+" align=right nowrap>-------</TD>");
									}
								}
							}
							else
							{

								contadorerror++;
								if ("MANC".equals(coderror.substring(0,4))){
									mensaje_salida.append("<TD class="+clase+" align=left >"+coderror.substring(8,coderror.length()).trim()+"</TD>");}
								if ((coderror.length()>16)&&(!("MANC".equals(coderror.substring(0,4))))){
									mensaje_salida.append("<TD class="+clase+" align=left >"+coderror.substring(16,coderror.length()).trim()+"</TD>");}
								else
									if (EnlaceLynxConstants.COD_ERROR_LYNX_DENIEGA.equals(coderror)) {
										mensaje_salida.append("<TD class="+clase+" align=left >Operaci&oacute;n en Verificaci&oacute;n, Favor de Contactar a su Ejecutivo</TD>");
									} else {
										mensaje_salida.append("<TD class="+clase+" align=left >Error de operaci&oacute;n</TD>");	
									}

								if ("MANC".equals(coderror.substring(0,4))){
									EIGlobal.mensajePorTrace("*** No se realiza ninguna accion hasta despues del else OIVI.", EIGlobal.NivelLog.INFO);}
								//INICIO - ESC 1/JUN/2010 - VALIDACION DE CUENTAS DE CARGO Y DE ABONO
								else if("CTASINVA".equals(coderror)){
									mensaje_salida.append("<TD class="+clase+" align=left >" + mensajeCargo + mensajeAbono + "</TD>");
								}//FIN ESC 1/JUN/2010
								else
									mensaje_salida.append("<TD class="+clase+" align=center nowrap>"+sreferencia+"</TD>");
								mensaje_salida.append("<TD class="+clase+" align=right nowrap>-------</TD>");
							}
						}
						else
						{

							contadorerror++;
							mensaje_salida.append("<TD class="+clase+" align=left >Error de operaci&oacute;n</TD>");
							mensaje_salida.append("<TD class="+clase+" align=center nowrap>-------</TD>");
						}
					}
					else
					{
						mensaje_salida.append("<TD class="+clase+" align=center nowrap >"+cta_origen+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center nowrap >"+cta_destino+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=right  nowrap >"+FormatoMoneda(importe_string)+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center nowrap >"+fecha+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=left nowrap>"+concepto+"</TD>");
						mensaje_salida.append("<TD class="+clase+" align=center >Fecha invalida</TD>");
                        mensaje_salida.append("<TD class="+clase+" align=left  nowrap>-------</TD>");
						mensaje_salida.append("<TD class="+clase+" align=right nowrap>-------</TD>");
						contadorerror++;
					}
					mensaje_salida.append("</TR>");

					folios = folios+", "+ sreferencia;

				}//if
			}//for
			//VSWF-HGG  -  borra bandera
			req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
			mensaje_salida.append("</TABLE>");


			req.setAttribute("mensaje_salida",mensaje_salida.toString());
			req.setAttribute("fecha_hoy",fecha_hoy);
			//req.setAttribute("ContUser",ObtenContUser());
			//req.setAttribute("titulo","Transferencia Procesada");
			req.setAttribute("miSalida",miSalida);
			EIGlobal.mensajePorTrace("***TransferenciasMultiples.class tramaok:"+tramaok, EIGlobal.NivelLog.INFO);


			req.setAttribute("tramaok","\""+tramaok+"\"");
			req.setAttribute("contadorok",CADENA_VACIA+contadorok);
			req.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
			req.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

            req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s25380tmh",req));
	        req.setAttribute("web_application",Global.WEB_APPLICATION);
            req.setAttribute("banco",session.getClaveBanco());
			String banco=session.getClaveBanco();
			//naa
			if(strDebug!=null && !strDebug.trim().equals(""))
			{
				req.setAttribute("strDebug",strDebug);
				sess.setAttribute("strDebug",strDebug);
			}
			sess.setAttribute("mensaje_salida",mensaje_salida.toString());
			sess.setAttribute("fecha_hoy",fecha_hoy);
			//req.setAttribute("ContUser",ObtenContUser());
			//req.setAttribute("titulo","Transferencia Procesada");
			sess.setAttribute("miSalida",miSalida);

			sess.setAttribute("tramaok","\""+tramaok+"\"");
			sess.setAttribute("contadorok",CADENA_VACIA+contadorok);
			sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
			sess.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

			sess.setAttribute("MenuPrincipal", session.getStrMenu());
			sess.setAttribute("newMenu", session.getFuncionesDeMenu());
			sess.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s25380tmh",req));
			sess.setAttribute("web_application",Global.WEB_APPLICATION);
			sess.setAttribute("banco",banco);

			EIGlobal.mensajePorTrace("*** invoca redirect 1", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("*** invoca codErrorOper-->"+codErrorOper.toString(), EIGlobal.NivelLog.DEBUG);
			EmailSender emailSender=new EmailSender();
			//if (contadorok>0 || contadormanc>0) {
			if(emailSender.enviaNotificacion(codErrorOper.toString())){

			try {
           		EmailDetails emailDetails = new EmailDetails();

           		String emailUsuario = "";
           		String ctaCargo = "";
				String ctaDest = "";

           		Double total = 0.0;

           		for(int n = 1; n <= contador; n++){
           			total = total + Double.parseDouble(Importes[n]);
           		}

           		if(contador > 1) {
           			estatus = contadorok + " ENVIADAS, " + contadormanc + " MANCOMUNADAS, " + contadorprog + " PROGRAMADAS";
           		}

				if(cta_origen.contains(" ")){
           			ctaCargo = cta_origen.substring(0,cta_origen.indexOf(" "));}
				else{
					ctaCargo = cta_origen;}

				if(cta_origen.contains(" ")){
           			ctaDest = cta_destino.substring(0,cta_destino.indexOf(" "));}
				else{
           			ctaDest = cta_destino;}

           		emailDetails.setNumRegImportados(contador);
           		emailDetails.setImpTotal(ValidaOTP.formatoNumero(total));
           		emailDetails.setNumeroContrato(session.getContractNumber());
           		emailDetails.setRazonSocial(session.getNombreContrato());
           		emailDetails.setNumRef(folios.substring(2,folios.length()));
           		emailDetails.setNumCuentaCargo(ctaCargo);
           		emailDetails.setTipoTransferenciaUni(" transferencia Multiple ");

           		EIGlobal.mensajePorTrace("�������������������� Mail en session en TransferenciasMultiples ->" + emailUsuario, EIGlobal.NivelLog.INFO);
        		EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
        		EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
        		EIGlobal.mensajePorTrace("�������������������� Importe ->" + total, EIGlobal.NivelLog.INFO);
        		EIGlobal.mensajePorTrace("�������������������� Referencia ->" + folios.substring(2,folios.length()), EIGlobal.NivelLog.INFO);
        		EIGlobal.mensajePorTrace("�������������������� CuentaCargo ->" + ctaCargo, EIGlobal.NivelLog.INFO);


           			if(contador > 1){
           				EIGlobal.mensajePorTrace("�������������������� Estatus ->" + estatus, EIGlobal.NivelLog.INFO);


           				emailDetails.setEstatusActual(codErrorOper.toString());

           				emailSender.sendNotificacion(req,IEnlace.MAS_TRANS_MISMO_BANCO, emailDetails);
           			}
           			else{
           				estatus = estatus.replaceAll(",", "");

           				EIGlobal.mensajePorTrace("�������������������� Estatus ->" + estatus, EIGlobal.NivelLog.INFO);
           				EIGlobal.mensajePorTrace("�������������������� CuentaDestino ->" + ctaDest, EIGlobal.NivelLog.INFO);

           				emailDetails.setEstatusActual(codErrorOper.toString());
           				emailDetails.setTipoTransferenciaUni(" M"+(char)250+"ltiple ");
           				emailDetails.setCuentaDestino(ctaDest);

           				emailSender.sendNotificacion(req,IEnlace.UNI_TRANS_MISMO_BANCO, emailDetails);
           			}
           		} catch (Exception e) {
           			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
           			EIGlobal.mensajePorTrace(" al enviar el correo ->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
           		}
			}
			
			EIGlobal.mensajePorTrace("Lynx: Finalizo flujo........ denegadas, contador: " +contTransRechazadas, EIGlobal.NivelLog.DEBUG);
			if (contTransRechazadas > 0) {
				// Se manda Pop Up para informar que algunas transacciones no fueron realizadas
				String msgTranDen ="cuadroDialogo(\"<br>Estimado usuario, de acuerdo a las reglas de seguridad determinadas en su contrato ENLACE,"
            			+ " algunas de las  transacciones no se pudieron realizar hasta su validaci\u00f3n." 
            			+ " Le pedimos revisar el detalle de cada Operaci\u00f3n Rechazada y contactar a su Ejecutivo,"
            			+ " para continuar el proceso.\",1)";
	   			req.setAttribute("LynxTranDenegadas", msgTranDen);
			}
			//res.sendRedirect(res.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL));
                 evalTemplate(IEnlace.MI_OPER_TMPL, req, res);

			// salida=req.setAttribute("MenuPrincipal", menuSesion);
			//evalTemplate(IEnlace.MI_OPER_TMPL, req, res);
			//desconectando();
			//return 1;
		} // banderaTransaccion
		else
		{   //Req Q1466. Generar variables para pantalla aviso de Transaccion en Proceso.
			EIGlobal.mensajePorTrace("***Entra NominaOcupada", EIGlobal.NivelLog.INFO);
			String InfoUser= "<br>Su transacci&oacute;n ya est&aacute; en proceso, Verifique posteriormente resultados";
			InfoUser="cuadroDialogo (\""+ InfoUser +"\",1)";
			req.setAttribute("InfoUser", InfoUser);
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transacci&oacute;n en Proceso","Servicios &gt; Transferencias &gt; M&uacute;ltiples", "s55220",req));

			req.setAttribute("operacion","tranopera");
			evalTemplate("/jsp/TransMultiplesTranenProceso.jsp", req, res);
		}

		return 1;

    } // Fin del Metodo


   /**
	 * Metodo para tratamiento de la pantalla de validacion de Registros Duplicados.
	 * @param clave_perfil clave de perfil
	 * @param strDebug
	 * @param usuario usuario
	 * @param contrato contrato
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera de exito
	 * */
    public int pantalla_valdup_transferencias_multiples(String clave_perfil, String strDebug, String usuario, String contrato,HttpServletRequest req, HttpServletResponse res)
               throws ServletException, IOException {

		EIGlobal.mensajePorTrace("***transferenciasMultiples.class Entrando a pantalla_valdup_transferencias_multiples ", EIGlobal.NivelLog.INFO);

        String mensaje_salida="";
        String coderror="";
        tipo_operacion=TRAN;
                String cta_origen="";
        String cta_destino="";
                String des_cta_origen="";
            String des_cta_destino="";
        String importe_string="";
        double importe=0.0;
        String concepto="";
        String sfecha_programada="";
        String bandera_fecha_programada="1";
        String fecha_programada_b="";
        int salida=0;
        Double importedbl;
        boolean programada=false;
        String tipo_cta_origen="";
        String tipo_cta_destino="";
        String cadena_cta_origen="";
        String cadena_cta_destino="";
        String arreglo_ctas_origen="";
        String arreglo_ctas_destino="";
        String arreglo_importes="";
        String arreglo_fechas="";
        String arreglo_conceptos="";
        String arreglo_estatus="";
        int contador =0;
        String scontador="";
        int indice=0;
        String fecha="";
        String bandera_estatus="";
        String fecha_hoy="";
                String miSalida = "";
                String tramaok="";
		String  sesion_cta_destino      = "";
        String strdest2                            = "";
        String strorig2                             = "";
        String  mensajeError            = "";
        boolean comuError=false;
        String[] arrCuentas=new String[4]; //Numero de cuentas  X 2 (cuenta|tipo|)
        scontador=( String ) req.getParameter("contador2");
        contador=Integer.parseInt(scontador);


		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

	    //Declaracion Objeto para la copia remota a desabsm
		ArchivoRemoto archR = new ArchivoRemoto();
	    String nombreArchivo= session.getUserID8()+".trn";
		//String strTabla="";

        arreglo_ctas_origen=( String ) req.getParameter("arregloctas_origen2");
        arreglo_ctas_destino=( String ) req.getParameter("arregloctas_destino2");
        arreglo_importes=( String ) req.getParameter("arregloimportes2");
        arreglo_fechas=( String ) req.getParameter("arreglofechas2");
        arreglo_conceptos=( String ) req.getParameter("arregloconceptos2");
        arreglo_estatus=( String ) req.getParameter("arregloestatus2");


        EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_ctas_origen  "+arreglo_ctas_origen, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_ctas_destino "+arreglo_ctas_destino, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_importes "+arreglo_importes, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_fechas "+arreglo_fechas, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_conceptos "+arreglo_conceptos, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***transferenciasMultiples.class  arreglo_estatus "+arreglo_estatus, EIGlobal.NivelLog.INFO);

        EIGlobal.mensajePorTrace("***Valor del Contador: "+contador, EIGlobal.NivelLog.INFO);

		// OPC:PASSMARK 16/12/2006 ******************* BEGIN
		String pmuser      = session.getUserID8();
		String devicePrint = (String)req.getParameter ("pmdevice")==null?"":(String)req.getParameter ("pmdevice");
		String FSOanterior = (String)req.getParameter ("fsoantes")==null?"":(String)req.getParameter ("fsoantes");
		String ipuser      = (String)req.getRemoteAddr();

		if("1".equals(Global.ACTIVAR_PASSMARK))
		{
			//System.out.println("TRANSFERENCIAS :: devicePrint DP: "+devicePrint);
			//System.out.println("TRANSFERENCIAS :: FSOanterior DP: "+FSOanterior);
			//System.out.println("TRANSFERENCIAS :: ipuser DP: "+ipuser);
			//System.out.println("TRANSFERENCIAS :: pmuser DP: "+pmuser);
		}
		else
		{
			FSOanterior = null;
		}
		// OPC:PASSMARK 16/12/2006 ******************* END

        fecha_hoy=ObtenFecha();

        miSalida = "arreglo_ctas_origen "+arreglo_ctas_origen + "\n arreglo_ctas_destino "+arreglo_ctas_destino + "\n arreglo_importes "+arreglo_importes + "\n arreglo_fechas "+arreglo_fechas + "\n arreglo_conceptos "+arreglo_conceptos + "\n arreglo_estatus "+arreglo_estatus + "\n fecha_hoy "+fecha_hoy ;

        String[] Ctas_origen  = desentramaC(scontador+"@"+arreglo_ctas_origen,'@');
        String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
        String[] Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
        String[] Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
        String[] Conceptos    = desentramaC(scontador+"@"+arreglo_conceptos,'@');
        String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');

//Codificacion para Armar el Archivo de Envio de Validaci�n de registros Duplicados.

		EI_Exportar ArcSalida=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
		EIGlobal.mensajePorTrace("TransferenciasMultiples pantallavaldup...(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

		String LineaArchivo ="";
		if(ArcSalida.creaArchivo())
		{

			for (int indice1=1;indice1<=contador;indice1++)
			{

				LineaArchivo = "";  //Limpiar para cargar cada l�nea del Archivo.
				bandera_estatus=Estatus[indice1];
				if ("1".equals(bandera_estatus))
				{

					cadena_cta_origen  = Ctas_origen[indice1];
					cadena_cta_destino = Ctas_destino[indice1];
					importe_string     = Importes[indice1];
					concepto           = Conceptos[indice1];
					fecha              = Fechas[indice1];

					arrCuentas= desentramaC("3|" + cadena_cta_origen,'|');
					cta_origen = arrCuentas[1];
					tipo_cta_origen=arrCuentas[2];
                    des_cta_origen=arrCuentas[3];
                    if (cadena_cta_destino.indexOf((int)'|') == -1 )
						cadena_cta_destino = cadena_cta_destino + "|NR||" ;

					arrCuentas= desentramaC("3|" + cadena_cta_destino,'|');
                    cta_destino = arrCuentas[1];
					tipo_cta_destino=arrCuentas[2];
					des_cta_destino=arrCuentas[3];
                    importedbl = new Double (importe_string);
					importe = importedbl.doubleValue();
					sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(6,fecha.length());

					//Linea para archivo.
					LineaArchivo+= ArcSalida.formateaCampo(cta_origen,11) + "     ";
					LineaArchivo+= ArcSalida.formateaCampo(cta_destino,11) + "     ";
					LineaArchivo+= ArcSalida.formateaCampo(importe_string,13);
					LineaArchivo+= ArcSalida.formateaCampo(concepto,30) + "          ";
					LineaArchivo+= ArcSalida.formateaCampo(sfecha_programada,8);

					//Visualizar los campos desarmados previos a armar el archivo
					EIGlobal.mensajePorTrace("***Cta de Origen:  "+cta_origen, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Cta de Destino: "+cta_destino, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Importe:        "+importe, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***Fecha:          "+sfecha_programada, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***LineaArchivo:   "+LineaArchivo, EIGlobal.NivelLog.INFO);

					// OPC:PASSMARK 16/12/2006 ******************* BEGIN
					if(Global.ACTIVAR_PASSMARK.equals("1"))
					{
						String FSOnuevo = CallPassmark(pmuser,devicePrint,ipuser,FSOanterior,importe_string);
						FSOanterior=FSOnuevo;
					}
					// OPC:PASSMARK 16/12/2006 ******************* END

					LineaArchivo+="\n";
					if(!ArcSalida.escribeLinea(LineaArchivo)){ //Se graba L�nea al Archivo de Salida de validaci�n.
						EIGlobal.mensajePorTrace("TransferenciasMultiples pantallavaldup...(): Algo salio mal escribiendo ", EIGlobal.NivelLog.INFO);}

				} //Termina if(bandera_estatus)

			} //Termina for(indice1 ....)

		} //Termina if(creaArchivo....)

		//Cerrar el Archivo de validaci�n.
		ArcSalida.cierraArchivo();

		if(FSOanterior!=null){ // OPC:PASSMARK
			req.setAttribute("_fsonuevoMULT",FSOanterior);} // OPC:PASSMARK

//Codigo para generar la copia remota al equipo BIA1 (desabsm en desarrollo) desde desabsm2

		if(archR.copiaLocalARemoto(session.getUserID8()+".trn"))
		{
			EIGlobal.mensajePorTrace("Transferencias Multiples - pantalla_valdup_transferencias_multiples(): el archivo .tib se copio correctamente", EIGlobal.NivelLog.INFO);
		}
		else
		{
			EIGlobal.mensajePorTrace("Transferencias Multiples - pantalla_valdup_transferencias_multiples(): error al copiar el archivo .trn", EIGlobal.NivelLog.INFO);
		}


//Codigo para el envio de transaccion a Tuxedo
        //Trama que se enviar� v�a Tuxedo para el servicio de validaci�n.
		String tramaEnviada ="";
		tramaEnviada="1EWEB|"+session.getUserID8()+"|VALF|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|1|";

		//ivn Q05-0001087 inicio lineas agregadas 2
		String IP = " ";
		String fechaHr = "";												//variables locales al m�todo
		IP = req.getRemoteAddr();											//ObtenerIP
		java.util.Date fechaHrAct = new java.util.Date();
		SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		//fin lineas agregada 2


		/*ivn Q05-0001087 se agrego al envio de la trama la variable fechaHr
		 *EIGlobal.mensajePorTrace("TransferenciasMultiples - pantalla_valdup....(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);*/

		EIGlobal.mensajePorTrace(fechaHr+"TransferenciasMultiples - pantalla_valdup....(): La trama que se enviara es: "+tramaEnviada, EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable htResult=null;

		try{
			htResult=tuxGlobal.web_red(tramaEnviada);
		}
		catch(java.rmi.RemoteException re){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch(Exception e) {}


//Codigo para la recepcion de la respuesta del Servicio Tuxedo de validaci�n registros Duplicados

	    String CodError = ( String ) htResult.get( "BUFFER" );
		//Lo siguiente se puede usar para simular respuesta del Servicio de Validaci�n.
		//String CodError = "VALF0000@Transaccion Exitosa@/tmp/1001851.trnr";

		EIGlobal.mensajePorTrace("Transferencias Multiples - pantalla_valdup....(): El servicio regreso CodError: "+CodError, EIGlobal.NivelLog.DEBUG);

		StringTokenizer separador = new StringTokenizer(CodError, "@");
		String[] result = new String[3];
		int i =0;
		while (separador.hasMoreTokens())
		{
			EIGlobal.mensajePorTrace("Dentro del ciclo For ", EIGlobal.NivelLog.DEBUG);
			result[i]= separador.nextToken();
			EIGlobal.mensajePorTrace("TransferenciasMultiples - pantalla_valdup....(): Elemento " + i + " es : "+ result[i], EIGlobal.NivelLog.DEBUG);
			i++;
		}

		int NumeroError = Integer.parseInt(result[0].substring(4,8));

// Despliegue de los arreglos Antes de enviar.

        EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_ctas_origen  "+arreglo_ctas_origen, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_ctas_destino "+arreglo_ctas_destino, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_importes "+arreglo_importes, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_fechas "+arreglo_fechas, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_conceptos "+arreglo_conceptos, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***transferenciasale.class  arreglo_estatus "+arreglo_estatus, EIGlobal.NivelLog.INFO);

        String arreglodeconceptos="\""+arreglo_conceptos+"\"";
        String arreglodecuentas_origen="\""+arreglo_ctas_origen+"\"";
        String arreglodecuentas_destino="\""+arreglo_ctas_destino+"\"";

		//TODO BI CU2021, Valida Duplicados
		/*
		 * VSWF-HGG-I
		 */
        if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try{
			log("VSWF: Valida Duplicados");
			log("VSWF: FLUJO " + (String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();

			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_MULTI_VALIDA_DUPLICADOS);
			if(result[0] != null){
				if("OK".equals(result[0].substring(0,2))){
					bt.setIdErr("VALF0000");
				}else if(result[0].length() > 8){
					bt.setIdErr(result[0].substring(0,8));
				}else{
					bt.setIdErr(result[0].trim());
				}
			}
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			bt.setImporte(importe);
			if (nombreArchivo != null) {
				bt.setNombreArchivo(nombreArchivo.trim());

			}
			bt.setServTransTux("VALF");
			bt.setTipoMoneda("MN");
			bt.setBancoDest("SANTANDER");
			if (fecha != null) {
				if (programada) {
					bt.setFechaProgramada(BitaHelperImpl.MdyToDate(fecha));
				}
			}
			log("VSWF: Archivo " + bt.getNombreArchivo());

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
        }
		/*
		 * VSWF-HGG-I
		 */
//Codigo para El tratamiento de desarme del Archivo de Respuesta, o de no regs Duplicados y Salida a JSP

		if ((NumeroError==0) && !(result[2].trim().equals("-")))
		{

			long posicionReg = 0;
			long residuo	 = 0;

			String nombreArchRegreso = result[2].substring(result[2].lastIndexOf('/')+1);
			// Para pruebas duplicados arch. simulado //String nombreArchRegreso = "1001851.xdrr";
			MDI_Importar ArchImpRegreso = new MDI_Importar();
			ArchImpRegreso.nuevoFormato=false;


			//***** everis Declaraci�n BufferedReader 08/05/2008  ..inicio
			BufferedReader arch = null;
			//***** everis Declaraci�n BufferedReader 08/05/2008  ..fin



			try
			{
				if(archR.copia(nombreArchRegreso)){
					EIGlobal.mensajePorTrace("***Transferencias Multiples el archivo trnr se copio correctamente", EIGlobal.NivelLog.INFO);}
				else
					EIGlobal.mensajePorTrace("***Transferencias Multiples el archivo trnr no se copio correctamente", EIGlobal.NivelLog.INFO);

// Se arma la cabecera de la tabla a Desplegar en el Jsp de Regs Duplicados
				String contenidoArchivoStr = "";

				contenidoArchivoStr="<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
							        "<tr> "+
									"<td class=\"textabref\">Transferencias M&uacute;ltiples</td>"+
									"</tr>"+

									"<tr>"+
									"<td align=center class=\"titpag\">Listado de Registros Duplicados</td>"+
									"</tr>"+

									"</table>"+
									"<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">"+
									"<tr> "+
									//"<td align=center width=26 class=tittabdat></td>"+
									"<td align=center width=200 class=tittabdat>Cuenta cargo</td>"+
									//"<td align=center colspan=2 class=tittabdat>Cuenta abono</td>"+
									"<td align=center width=200 class=tittabdat>Cuenta abono/M&oacute;vil</td>"+
									"<td align=center width=90 class=tittabdat>Importe</td>"+
									"<td align=center class=tittabdat>Concepto</td>"+
									"<td align=center width=60 class=tittabdat>Fecha aplicaci&oacute;n</td>"+
									"</tr>";

// Extraer los registros del archivo y depositarlos en la tabla.

				nombreArchRegreso=Global.DIRECTORIO_LOCAL+"/"+nombreArchRegreso;


				//***** everis Declaraci�n BufferedReader 08/05/2008  ..inicio

				//System.out.println("*** everis Asigna valores BufferedReader  arch");
				//BufferedReader arch	= new BufferedReader( new FileReader(nombreArchRegreso) );
				arch	= new BufferedReader( new FileReader(nombreArchRegreso) );

				//***** everis Declaraci�n BufferedReader 08/05/2008  ..fin


				String linea;
				String cad_decimal="";

				while ((linea = arch.readLine()) !=null)
				{
					//System.out.println("Comienza la lectura del archivo --------------->" + nombreArchRegreso);
					indice = -1;
					cad_decimal = "";
					if( "".equals(linea.trim())){
						linea = arch.readLine();}
					if( linea == null )
						break;
					EIGlobal.mensajePorTrace("Transferencias Multiples liniecita: "+linea, EIGlobal.NivelLog.INFO);
					contenidoArchivoStr=contenidoArchivoStr+"<tr>";

					posicionReg++;
					residuo=posicionReg % 2 ;
					String bgcolor="textabdatobs";
					if (residuo == 0){
						bgcolor="textabdatcla";
					}

					contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(0,11).trim()+"&nbsp;</td>"; // Cuenta de Cargo
					contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(16,27).trim()+"&nbsp;</td>"; // Cuenta de Abono
					contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(32,45).trim()+"&nbsp;</td>"; // Importe
					contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(45,75).trim()+"&nbsp;</td>"; // Concepto
					//contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+EIGlobal.formatoFecha(linea.substring(85,93).trim(),"dd/mm/aaaa")+"&nbsp;</td>"; // Fecha de Aplicacion
					contenidoArchivoStr+= "<td class=\""+bgcolor+"\" nowrap align=\"center\">"+linea.substring(87,89).trim()+'/'+linea.substring(85,87).trim()+'/'+linea.substring(89,93).trim()+"&nbsp;</td>"; // Fecha de Aplicacion
					contenidoArchivoStr=contenidoArchivoStr+"</tr>";
				}

				contenidoArchivoStr=contenidoArchivoStr+"</table>"; // Cerrar tabla.



				//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
				arch.close();
				//****  everis Cerrando BufferedReader 08/05/2008  ..fin




// Codigo de envio de parametros al Jsp de registros duplicados

				req.setAttribute("ContenidoArchivoStr",contenidoArchivoStr);

				String mensajeHTML="\n  <br>RESULTADO<br>";
				mensajeHTML+="\n <br>";
				mensajeHTML+="\n  VALIDACION : EXISTEN REGISTROS DUPLICADOS.<br>";

				String InfoUser= "<br>Esta  transmisi�n contiene registros  que se han enviado  anteriormente, " +
								 "si desea realizar  �l envi� de cualquier manera, presione el bot�n transferir " +
								 "ubicado en la parte inferior de esta pantalla. ";

				InfoUser="cuadroDialogoMedidas(\""+ InfoUser +"\",1,380,220)";
   				req.setAttribute("InfoUser", InfoUser);

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				//req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples","Transferencias &gt  Cuentas mismo banco  &gt  Moneda Nacional &gt M&uacute;ltiples","s25380tmh",req));
				req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples - Existen registros Duplicados","Transferencias  &gt  M&uacute;ltiples &gt Cuentas mismo banco &gt Moneda Nacional", "s55210",req));
				req.setAttribute("Mensaje",despliegaMensaje(mensajeHTML));
				req.setAttribute("arregloctas_origen2",arreglodecuentas_origen);
				req.setAttribute("arregloctas_destino2",arreglodecuentas_destino);
				req.setAttribute("arregloimportes2",arreglo_importes);
				req.setAttribute("arregloconceptos2",arreglodeconceptos);
				req.setAttribute("arreglofechas2",arreglo_fechas);
				req.setAttribute("arregloestatus2",arreglo_estatus);
				req.setAttribute("contador2",scontador);
				req.setAttribute("web_application",Global.WEB_APPLICATION);
				req.setAttribute("banco",session.getClaveBanco());
				String banco=session.getClaveBanco();
				req.setAttribute("botontransferir","<td align=right width=89><a href=\"javascript:valregdup();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a></td><td align=center><a href=\"javascript:regresar();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25320.gif></a></td>");

				evalTemplate("/jsp/TransferenciasMultiplesValidar.jsp", req, res);

			}catch(Exception e) {}


			//***** everis Cerrando BufferedReader 08/05/2008  ..inicio
			finally{
	            if(null != arch){
	                try{
	                	arch.close();
	                }catch(Exception e){}
	                arch = null;
	            }
	        }

			//****  everis Cerrando BufferedReader 08/05/2008  ..fin


		}
	    else if ((NumeroError==0) && (result[2].trim().equals("-")))
			//Corresponde al Caso donde no hay registros Duplicados.
		{
			String mensajeHTML="\n  <br>RESULTADO<br>";
			mensajeHTML+="\n <br>";
			mensajeHTML+="\n  VALIDACION EXITOSA, NO EXISTEN REGISTROS DUPLICADOS.<br>";

			String InfoUser= "<br>Validacion Exitosa:  No existen registros duplicados. " +
							 "Ahora puede realizar el env�o, presione el bot�n transferir " +
							 "ubicado en la parte inferior de la pantalla. ";

			InfoUser="cuadroDialogoMedidas(\""+ InfoUser +"\",1,380,220)";
   			req.setAttribute("InfoUser", InfoUser);

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Transferencias M&uacute;ltiples - No existen registros Duplicados","Transferencias  &gt  M&uacute;ltiples &gt Cuentas mismo banco &gt Moneda Nacional", "s55210",req));
			req.setAttribute("Mensaje",despliegaMensaje(mensajeHTML));

			req.setAttribute("arregloctas_origen2",arreglodecuentas_origen);
			req.setAttribute("arregloctas_destino2",arreglodecuentas_destino);
			req.setAttribute("arregloimportes2",arreglo_importes);
			req.setAttribute("arregloconceptos2",arreglodeconceptos);
			req.setAttribute("arreglofechas2",arreglo_fechas);
			req.setAttribute("arregloestatus2",arreglo_estatus);
			req.setAttribute("contador2",scontador);
			req.setAttribute("web_application",Global.WEB_APPLICATION);
            req.setAttribute("banco",session.getClaveBanco());
			String banco=session.getClaveBanco();

			req.setAttribute("botontransferir","<td align=center width=300><a href=\"javascript:valregdup();\" border=0><img src=/gifs/EnlaceMig/gbo25360.gif border=0 ></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript:regresar();\" border=0><img border=0  src=/gifs/EnlaceMig/gbo25320.gif></a></td>");

			evalTemplate("/jsp/TransferenciasMultiplesValidar.jsp", req, res);
		}
		else if (NumeroError>0)
		{
			despliegaPaginaErrorURL(result[1],"Transferencias Multiples","Transferencias > Multiples" ,"", req, res);
		}

        EIGlobal.mensajePorTrace("***transferenciasMultiples.class Saliendo de pantalla_valdup_transferencias_multiples ", EIGlobal.NivelLog.INFO);
		return 1;
    }



    /**
	 * Metodo para obtener la hora completa
	 * @return hora completa en formato string
	 * */
     String ObtenHoraCompleta()
        {
      String hora = "";
      String[] AM_PM = {"AM","PM"};
      int minutos =0;
      String sminutos ="";
      int segundos=0;
      String ssegundos="";
      int milisegundos=0;
      String smilisegundos="";

      Date Hoy = new Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Cal.setTime(Hoy);

      if (AM_PM[Cal.get(Calendar.AM_PM)].equals("AM"))
      {
        minutos=Cal.get(Calendar.MINUTE);
        sminutos=((minutos < 10) ? "0" : "") + minutos;
        segundos=Cal.get(Calendar.SECOND);
        ssegundos=((segundos < 10) ? "0" : "") + segundos;

        milisegundos=Cal.get(Calendar.MILLISECOND);
        smilisegundos=((milisegundos < 10) ? "0" : "") + milisegundos;

        hora =  ((Cal.get(Calendar.HOUR)%12))+":"+ sminutos +": "+ssegundos+": "+smilisegundos+"  hrs";
      }
      else
      {
        minutos=Cal.get(Calendar.MINUTE);
        sminutos=((minutos < 10) ? "0" : "") + minutos;
        segundos=Cal.get(Calendar.SECOND);
        ssegundos=((segundos < 10) ? "0" : "") + segundos;

        milisegundos=Cal.get(Calendar.MILLISECOND);
        smilisegundos=((milisegundos < 10) ? "0" : "") + milisegundos;


        hora =  ((Cal.get(Calendar.HOUR)%12)+12)+":"+ sminutos +": "+ssegundos+": "+smilisegundos+ " hrs";
      }
      return hora;
    }


     /**
 	 * Metodo para validar la fecha menor
 	 * @param fecha fecha
 	 * @return bandera de exito
 	 * */
boolean checar_fecha_menor(String fecha)
{
   boolean menor=false;
   int idia, imes, ianio;
   idia=new Integer(fecha.substring(0,2)).intValue();
   imes=new Integer(fecha.substring(3,5)).intValue();
   ianio=new Integer(fecha.substring(6,fecha.length())).intValue();
   Date hoy= new Date();
   GregorianCalendar cal=new GregorianCalendar();
   cal.setTime(hoy);


   //if ((idia==hoy.getDay())&&((imes-1)==cal.get(Calendar.MONTH))&&(ianio==cal.get(Calendar.YEAR)))
   if ((idia== cal.get(Calendar.DAY_OF_WEEK))&&((imes-1)==cal.get(Calendar.MONTH))&&(ianio==cal.get(Calendar.YEAR))){
      menor=false;}
   else
   {

      if (ianio<cal.get(Calendar.YEAR)){
            menor=true;}
          else
          {
         if (ianio==cal.get(Calendar.YEAR))
         {
            if ((imes-1)<cal.get(Calendar.MONTH))
                       menor=true;
            else if ((imes-1)==cal.get(Calendar.MONTH))
                        {
              if (idia<cal.get(Calendar.DATE)){
                            menor=true;}

                        }
                 }

      }
   }
   return menor;
}

/**
 * Metodo que coloca las fechas en los campos
 * @param fecha 
 * @param fac_programadas
 * @return cadena con las fechas
 * */
String poner_campos_fecha(String fecha,boolean fac_programadas)
{
   String dia_forma=ObtenDia();
   String mes_forma=ObtenMes();
   String anio_forma=ObtenAnio();
   String campos_fecha="";
   if(!fecha.equals(""))
   {
           dia_forma=fecha.substring(0, 2);
       mes_forma=fecha.substring(3, 3);
           anio_forma=fecha.substring(6,fecha.length());
   }
   campos_fecha=campos_fecha+"<INPUT ID=dia  TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
   campos_fecha=campos_fecha+"<INPUT ID=mes  TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
   campos_fecha=campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";

   return campos_fecha;
}

/**
 * Metodo que coloca las fechas programadas
 * @param fecha 
 * @param fac_programadas
 * @param nombre_campo
 * @param funcion
 * @return cadena con las fechas
 * */
String poner_calendario_programadas(String fecha,boolean fac_programadas,String nombre_campo,String funcion)
{
        String cal="";
        if (fac_programadas)
    {
       if(!fecha.equals("")){
         cal=cal+"<INPUT ID="+nombre_campo+"TYPE=TEXT SIZE=15 class=tabmovtexbol NAME="+nombre_campo+" VALUE="+fecha.substring(0,2)+"/"+fecha.substring(3,5)+"/"+fecha.substring(6,fecha.length())+" OnFocus=\"blur();\">";}
       else
         cal=cal+"<INPUT ID="+nombre_campo+"TYPE=TEXT SIZE=15 class=tabmovtexbol NAME="+nombre_campo+" VALUE="+ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio()+" OnFocus=\"blur();\">";

           cal=cal+"<A HREF=\"javascript:"+funcion+";\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0></A>";
        }
        return cal;
}

/**
 * Metodo que obtiene las cajas de texto
 * @param trama
 * @return arreglo de cajas de texto
 * */
public String[][] ObtenArregloBoxes(String trama)
{

        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class entrando a  ObtenArregloBoxes "+trama, EIGlobal.NivelLog.INFO);

        String[][] arrCuentas = null;
        String[] Cuentas = desentramaC(trama, '@' );
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class desentrame Cuentas", EIGlobal.NivelLog.INFO);

        ////System.out.println(Cuentas[0]);
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class desentrame Contadors"+Cuentas[0], EIGlobal.NivelLog.INFO);

        arrCuentas = new String[Integer.parseInt( Cuentas[0] ) + 1][5];
        arrCuentas[0][0] = Cuentas[0];
        arrCuentas[0][1] = "NUM_CUENTA";
        arrCuentas[0][2] = "TIPO_RELAC_CTAS";
        arrCuentas[0][3] = "CVE_PRODUCTO";
        arrCuentas[0][4] = "NOMBRE_TITULAR";
        for ( int indice = 1; indice <= Integer.parseInt( Cuentas[0] ); indice++ )
        {
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class Cuentas[indice]"+Cuentas[indice], EIGlobal.NivelLog.INFO);

                arrCuentas[indice] = desentramaC( "4|" + Cuentas[indice] + "|", '|' );
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class arrCuentas[indice][0]:"+arrCuentas[indice][0], EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class arrCuentas[indice][1]:"+arrCuentas[indice][1], EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class arrCuentas[indice][2]:"+arrCuentas[indice][2], EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class arrCuentas[indice][3]:"+arrCuentas[indice][3], EIGlobal.NivelLog.INFO);
                EIGlobal.mensajePorTrace("***TransferenciasMultiples.class arrCuentas[indice][4]:"+arrCuentas[indice][4], EIGlobal.NivelLog.INFO);


        }
        EIGlobal.mensajePorTrace("***TransferenciasMultiples.class saliendo de ObtenArregloBoxes "+trama, EIGlobal.NivelLog.INFO);

        return arrCuentas;

}

/**
 * Metodo que coloca los totales de las sumas en los campos
 * @param selsumcargo 
 * @param selsumabono
 * @return cadena con los totales
 * */
public String Poner_totales(double selsumcargo,double selsumabono)
{
  String totales="";
  FormatosMoneda fm=new FormatosMoneda();
  fm.setNumeroEnteros(20);

  totales="<TR bgcolor=#EBEBEB>";
  // 03/09/2002: Se cambi� el tama�o de estos campos de 10 a 15 posiciones.
  // 04/09/2002: Se retir� la propiedad maxlength

  /*
  totales+="<TD class=\"textabdatcla\" align=\"RIGHT\">Total Cargo:<INPUT TYPE=\"TEXT\" NAME=\"montoCargo\" VALUE=\""+FormatoMoneda(selsumcargo).substring(1, FormatoMoneda(selsumcargo).length())+"\"   SIZE=\"20\" onFocus=\"blur();\"></TD>";
  totales+="<TD class=\"textabdatcla\" align=\"RIGHT\">Total Abono:<INPUT TYPE=\"TEXT\" NAME=\"montoAbono\" VALUE=\""+FormatoMoneda(selsumabono).substring(1, FormatoMoneda(selsumabono).length())+"\"   SIZE=\"20\" onFocus=\"blur();\"></TD>";
  */


  totales+="<TD class=\"textabdatcla\" align=\"RIGHT\">Total Cargo:<INPUT TYPE=\"TEXT\" NAME=\"montoCargo\" VALUE=\""+fm.daMonedaSinComas(selsumcargo)+"\" SIZE=\"20\" onFocus=\"blur();\"></TD>";
  totales+="<TD class=\"textabdatcla\" align=\"RIGHT\">Total Abono:<INPUT TYPE=\"TEXT\" NAME=\"montoAbono\" VALUE=\""+fm.daMonedaSinComas(selsumabono)+"\" SIZE=\"20\" onFocus=\"blur();\"></TD>";


  totales+="</TR>";
  return totales;
}


/**
 * Metodo que coloca el concepto de fecha
 * @param calendarionormal calendario normal
 * @param campo_fecha fecha
 * @param concepto concepto
 * @return cadena con el concepto por fecha
 * */
public String Poner_concepto_fecha(String calendarionormal,String campo_fecha,String concepto)
{
  String campos="";

  campos="<TABLE BORDER=0 ALIGN=LEFT bgcolor=#EBEBEB><TR bgcolor=#EBEBEB>";
  campos+="<TD class=textabdatcla>Concepto:</TD >";
  campos+="<TD class=textabdatcla><INPUT TYPE=TEXT NAME=concepto VALUE=\""+concepto+"\" SIZE=40 MAXLENGTH=40></TD>";
  campos+="</TR>";
  campos+="<TR bgcolor=#EBEBEB>";
  campos+="<td class=textabdatcla>Fecha de aplicaci&oacute;n:</TD>";
  campos+="<TD>"+calendarionormal+campo_fecha+"</TD>";
  campos+="</TR></TABLE>";

  return campos;
 }

/**
 * Metodo que coloca los botones en pantalla
 * @return cadena con el codigo html de los botones
 * */
public String Poner_botones()
{
  String botones;

  botones="<TABLE ALIGN=CENTER ><TR>";
  botones+="<td ALIGN=CENTER><input type=image border=0  name=Agregar src=\"/gifs/EnlaceMig/gbo25290.gif\"   width=89 height=22 alt=Agregar></td>";
  botones+="<td valign=top  width=76><a href=\"javascript:limpiar_datos();\"><img  border=0 src=\"/gifs/EnlaceMig/gbo25250.gif\" ></a></td>";
  botones+="</TR></TABLE>";
  return botones;

}


/**************************************************************************
 * Se Agrega Metodo para desplegar Mensaje    Q1466 
 * @param mensajeHTML
 * @return mensaje de retorno
 **************************************************************************/
  String despliegaMensaje(String mensajeHTML)
	{
	  String msg="";

	  msg+="\n  <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=400 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n   <td align=center>";
	  msg+="\n     <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td colspan=3> </td>";
	  msg+="\n	   </tr>";
	  msg+="\n	   <tr>";
	  msg+="\n	     <td width=21 background='/gifs/EnlaceMig/gfo25030.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n		 <td width=428 height=100 valign=top align=center>";
	  msg+="\n		   <table width=380 border=0 cellspacing=2 cellpadding=3 background='/gifs/EnlaceMig/gau25010.gif'>";
	  msg+="\n			 <tr>";
	  msg+="\n			   <td class='tittabcom' align=center>";
	  msg+=                mensajeHTML;
	  msg+="\n			   </td>";
	  msg+="\n			 </tr>";
	  msg+="\n		   </table>";
	  msg+="\n		 </td>";
	  msg+="\n         <td width=21 background='/gifs/EnlaceMig/gfo25040.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>";
	  msg+="\n	   </tr>";
	  msg+="\n	 </table>";
	  msg+="\n	</td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  msg+="\n <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n  <tr>";
	  msg+="\n    <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>";
	  msg+="\n  </tr>";
	  msg+="\n </table>";

	  return msg;
	}

/*************************************************************************************
* Se agrega Metodo despliegaPaginaErrorURL    Q1466
* @param Error                                
* @param param1                                
* @param param2                                
* @param param3                                
* @param req                                
* @param res
* @throws IOException
* @throws ServletException
*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		req.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		req.setAttribute( "Error", Error );

		req.setAttribute( "MenuPrincipal", session.getStrMenu() );
		req.setAttribute( "newMenu", session.getFuncionesDeMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,req ) );

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		//req.setAttribute( "URL", "MDI_Interbancario?Modulo=0");
		req.setAttribute( "URL", "TransferenciasMultiples?ventana=3");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( req, res );
	}

	/**
	 * Metodo para verificar existencia del Archivo de Sesion
	 * @param arc
	 * @param eliminar
	 * @return bandera de exito
	 * @throws IOException
	 * @throws ServletException
	 * */
	
public boolean verificaArchivos(String arc,boolean eliminar)
	throws ServletException, java.io.IOException
{
	EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.INFO);
	File archivocomp = new File( arc );
	if (archivocomp.exists())
	 {
	   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.INFO);
	   if(eliminar)
		{
	      archivocomp.delete();
		  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.INFO);
		}
	   return true;
	 }
	EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.INFO);
	return false;
}
/**
 * Metodo para que guarda par�metros de Sesion
 * @param request
 * */
  public  void guardaParametrosEnSession (HttpServletRequest request){
          EIGlobal.mensajePorTrace("1---------Interrumple el flujo--"+request.getParameter("ventana" ), EIGlobal.NivelLog.INFO);
          String template= request.getServletPath();

          request.getSession().setAttribute("plantilla",template);

        HttpSession session = request.getSession(false);

        session.removeAttribute("bitacora");

        if(session != null){

            Map tmp = new HashMap();
        //    Enumeration enumer = request.getParameterNames();

                EIGlobal.mensajePorTrace("5--------------->enumer**********--->"+request.getParameter("ventana" ), EIGlobal.NivelLog.INFO);
                  tmp.put("ventana",request.getParameter("ventana" ));

                    tmp.put("arregloctas_origen2",request.getParameter("arregloctas_origen2" ));
                    tmp.put("arregloctas_destino2",request.getParameter("arregloctas_destino2" ));
                     tmp.put("arregloimportes2",request.getParameter("arregloimportes2" ));
                    tmp.put("arreglofechas2",request.getParameter("arreglofechas2" ));
                    tmp.put("arregloconceptos2",request.getParameter("arregloconceptos2" ));
                    tmp.put("arregloestatus2",request.getParameter("arregloestatus2" ));
                    tmp.put("contador2",request.getParameter("contador2" ));

          /*  while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
               EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name), EIGlobal.NivelLog.INFO);

                tmp.put(name,request.getParameter(name));
            }*/
            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }

    }

    // OPC:PASSMARK 16/12/2006
  /**
   * Metodo que realiza el Call Pass Mark
   * @param usuario
   * @param deviceprint
   * @param ipe
   * @param FSOanterior
   * @param monto
   * @return cadena con el resultado
   * */
	private String CallPassmark(String usuario, String deviceprint, String ipe, String FSOanterior, String monto)
	{
		String FSONuevo = null;
		BufferedWriter out = null;
		BufferedReader in  = null;
		GregorianCalendar calendar = new GregorianCalendar();
        long nowInMilliseconds = calendar.getTime().getTime();
		try{
			if(monto.indexOf(".")>-1)
			{
				//System.out.println("TransferenciasMultiples : CallPassmark : Convirtiendo Monto ="+monto);
				monto = monto.substring(0,monto.indexOf("."));
			}
		}
		catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			//System.out.println("TransferenciasMultiples: CallPassmark : "+e.toString());
			monto = "";
		}

        try {
       		//System.out.println("TransferenciasMultiples :: PASSMARK : URL = "+Global.URL_PASSMARK_ENLACE+"]");
			//System.out.println("TransferenciasMultiples :: CallPassmark  :: FSOANTES = " + FSOanterior );
            URL url = new URL(Global.URL_PASSMARK_ENLACE);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            //conn.setConnectTimeout(1000);
            out = new BufferedWriter( new OutputStreamWriter( conn.getOutputStream() ) );
			out.write("Oper=a&");
            out.write("usuario=" +usuario     +"&");
            out.write("device="  +deviceprint +"&");
            out.write("ipuser="  +ipe         +"&");
			out.write("pmmonto=" +monto       +"&");
            out.write("fsoantes="+FSOanterior +"&");
            out.write("canal=2");
            out.flush();
            out.close();

            in = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );

            String response;
            while ( (response = in.readLine()) != null ) {
                //System.out.println( "TransferenciasMultiples: CallPassmark  :: RESPUESTA = " + response );
				if(response.indexOf("PMDATA=[")>-1 && response.indexOf("]!-")>-1)
				{
					FSONuevo = response.substring(response.indexOf("PMDATA=[")+8,response.indexOf("]!-"));
				}
				else
				{
					//System.out.println("TransferenciasMultiples: CallPassmark :: FORMATO DE LA RESPUESTA INCORRECTO");
				}
            }
            in.close();
        }
        catch ( MalformedURLException ex ) {
        	//System.out.println("CallPassmark.MalformedURLException:-["+ex.toString());
        	EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
        catch ( IOException ex ) {
        	//System.out.println("CallPassmark.IOException:-["+ex.toString());
        	EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
		catch ( Exception ex ) {
        	//System.out.println("CallPassmark.Exception:-["+ex.toString());
			EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
		finally{
			try{
				out.close();
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try{
				in.close();
			}
			catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		GregorianCalendar calendar2 = new GregorianCalendar();
        long nowInMilliseconds2 = calendar2.getTime().getTime();
		try{
			//System.out.println( "["+(nowInMilliseconds2-nowInMilliseconds)+" ms] TransferenciasMultiples :: CallPassmark :: FSONUEVO = " + FSONuevo );
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return FSONuevo;
    }


} //Fin de la Clase
/*2007.01.1*/