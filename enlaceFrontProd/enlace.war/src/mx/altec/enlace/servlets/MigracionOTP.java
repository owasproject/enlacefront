package mx.altec.enlace.servlets;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;


import java.util.Calendar;
import java.util.StringTokenizer;
import java.sql.*;


import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Contrato;
import mx.altec.enlace.bo.Solicitud;
import mx.altec.enlace.bo.SolicitudBloqueoToken;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.NIPManagerServiceException;
import mx.altec.enlace.servicios.NIPManagerServices;

import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


import java.util.Vector;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class MigracionOTP extends BaseServlet {
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	defaultAction(request, response);
}

public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	defaultAction(request, response);
}

public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	HttpSession sess = request.getSession();
	BaseResource session=(BaseResource) sess.getAttribute("session");
	String ventana=request.getParameter("ventana");
	String accion = request.getParameter("accion");
	boolean session_ok=true;
	session_ok = SesionValidaOTP(request, response);
	if (session_ok) {
		String IP = " ",fechaHr = "";										//variables locales al m�todo
		IP = request.getRemoteAddr();											//ObtenerIP
		java.util.Date fechaHrAct = new java.util.Date();
	    SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");//formato fecha
		String sid = sess.getId();
		fechaHr ="["+fdate.format(fechaHrAct)+"] IP: "+ IP+", SID: " + sid + " USR: " + session.getUserID8() + " CONTRATO: " + session.getContractNumber() + " ";				//asignacin de fecha y hora

		//----------------------------------------------------------------------
		Calendar cal = new GregorianCalendar();
		int dia = cal.get(Calendar.DATE);
		int mes = cal.get(cal.MONTH)+1;
		System.out.println(dia + " " + mes + " " + cal.get(Calendar.YEAR));

		if (dia < 13 && mes <=11 && cal.get(Calendar.YEAR) <= 2006 ){
			if(accion.equals("solToken"))
			{
				request.setAttribute("mensajeError", "cuadroDialogo('Funcionalidad temporalmente fuera de servicio', 1);");
				evalTemplate( IEnlace.MENU_TOKEN, request, response );
				return;
			}
			if(accion.equals("solTokens"))
			{
				request.setAttribute("mensajeError", "cuadroDialogo('Funcionalidad temporalmente fuera de servicio', 1);");
				evalTemplate( IEnlace.MENU_TOKEN, request, response );
				return;
			}
		}
		//----------------------------------------------------------------------
		if(accion.equals("menu"))
		{
			sess.setAttribute("conActual",ObtenContUser (request));
			sess.setAttribute("usuario",session.getUserID8());
			String[][] arrayUsuarios = TraeUsuarios(request);
			for(int i=1;i<=Integer.parseInt(arrayUsuarios[0][0].trim());i++){
				if(session.getUserID8().equals(arrayUsuarios[i][1].trim()))
					sess.setAttribute("nameUsu",arrayUsuarios[i][2].trim());
			}
			evalTemplate( IEnlace.MENU_TOKEN, request, response );
		}
		if(accion.equals("solToken"))
		{
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}catch(Exception e) { e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // sali� bien
				stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status = stok.nextToken();
				}
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			if(status.startsWith("NO EXISTE") || (codError == 0 && Integer.parseInt(status) != 7)) {
				evalTemplate( IEnlace.SOLICITUD_INDIVIDUAL, request, response );
			}else{
				if(codError == 0 && Integer.parseInt(status) == 7){
					request.setAttribute("mensajeError", "cuadroDialogo('No se pueden solicitar Tokens que est�n bloqueados', 1);");
					evalTemplate( IEnlace.MENU_TOKEN, request, response );
				}else{
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: "+ status +"', 1);");
					evalTemplate( IEnlace.MENU_TOKEN, request, response );
				}
			}
		}
		if(accion.equals("actToken")) {
			String respuesta="";
			try {
				respuesta=NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			} catch(NIPManagerServiceException e) {
				e.printStackTrace();
				respuesta = "|1|PROBLEMA CON NIP MANAGER|";
			}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok=new StringTokenizer(respuesta,"|");
			int codError=-1;
			String status="";
			if (stok.hasMoreTokens()) {
				codError=Integer.parseInt(stok.nextToken());
			}
			if(codError==0 && stok.hasMoreTokens()) {
				stok.nextToken();//ignora el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status=stok.nextToken();
				}
			} else if (stok.hasMoreTokens()) {
				status=stok.nextToken();
			}
			if (codError==0 && Integer.parseInt(status)==4) {
				evalTemplate(IEnlace.ACTUALIZA_TOKEN, request, response);
			} else {
				if(codError == 1 && !status.startsWith("NO EXISTE")){
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: "+ status +"', 1);");
					evalTemplate(IEnlace.MENU_TOKEN, request, response);
				}else{
					request.setAttribute("mensajeError", "cuadroDialogo('S&oacute;lo se pueden Activar Tokens Entregados', 1);");
					evalTemplate(IEnlace.MENU_TOKEN, request, response);
				}
			}
		}
		if(accion.equals("msgActToken")) {
			request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Necesita autenticarse de nuevo, por favor introduzca sus datos\", 1);" );
			evalTemplate ( IEnlace.LOGIN, request, response );
			int resDup = cierraDuplicidad(session.getUserID8());
			session.setSeguir(false);
			sess.invalidate();
		}
		if(accion.equals("consToken"))
		{
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}catch(Exception e) { e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // sali� bien
			   stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status = stok.nextToken();
				}
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			if(codError == 0) {
				switch(Integer.parseInt(status)) {
					case 1:		status = "Solicitado";
								break;
					case 2:		status = "En Tr�mite";
								break;
					case 3:		status = "Solicitud Rechazada";
								break;
					case 4:		status = "Entregado";
								break;
					case 5:		status = "Entregado a trav�s de Ejecutivo";
								break;
					case 6:		status = "Activo";
								break;
					case 7:		status = "Bloqueado";
								break;
					case 8:		status = "Solicitud Vencida";
								break;
					default:	status = "Estado desconocido";
								break;
				}
			}
			request.setAttribute("codError", "" + codError);
			request.setAttribute("status", "" + status);
			request.setAttribute("serial", "" + obtenNoSerie(session.getUserID8()));
			if(codError == 1 && !status.startsWith("NO EXISTE")){
				request.setAttribute("mensajeError", "cuadroDialogo('ERROR: "+ status +"', 1);");
				evalTemplate(IEnlace.MENU_TOKEN, request, response);
			}else{
				evalTemplate( IEnlace.CONSULTA_TOKEN, request, response );
			}
			mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace(fechaHr + " Consulta Token: " + codError + "/" + status, mx.altec.enlace.utilerias.EIGlobal.NivelLog.INFO);
		}
		if(accion.equals("bloqToken"))
		{
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}catch(Exception e) { e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // sali� bien
				stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status = stok.nextToken();
				}
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			if(codError == 0 && Integer.parseInt(status) >= 4 && Integer.parseInt(status) <= 6) {
				evalTemplate( IEnlace.BLOQUEA_TOKEN, request, response );
			}
			else {
				if(codError == 1 && !status.startsWith("NO EXISTE")){
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: "+ status +"', 1);");
					evalTemplate(IEnlace.MENU_TOKEN, request, response);
				}else{
					request.setAttribute("mensajeError", "cuadroDialogo('S�lo se pueden bloquear Tokens en estado de Entregado o Activo', 1);");
					evalTemplate( IEnlace.MENU_TOKEN, request, response );
				}
			}
		}
		if(accion.equals("repoToken"))
		{
			String respuesta = "";
			try {
				respuesta = NIPManagerServices.consultaEstado(session.getUserID8(), "E");
			}catch(Exception e) { e.printStackTrace(); respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
			System.out.println("Estado: " + respuesta);
			StringTokenizer stok = new StringTokenizer(respuesta, "|");
			int codError = -1;
			String status = "";
			if(stok.hasMoreTokens()) {
				codError = Integer.parseInt(stok.nextToken());
			}
			if(codError == 0 && stok.hasMoreTokens()) { // sali� bien
				stok.nextToken();	//ignorar el status de NIP Manager
				if(stok.hasMoreTokens()) {
					status = stok.nextToken();
				}
			}
			else if(stok.hasMoreTokens()) {
				status = stok.nextToken();
			}
			if(codError == 0 && Integer.parseInt(status) == 7) {
				evalTemplate( IEnlace.REPOSICION_TOKEN, request, response );
			}
			else {
				if(codError == 1 && !status.startsWith("NO EXISTE")){
					request.setAttribute("mensajeError", "cuadroDialogo('ERROR: "+ status +"', 1);");
					evalTemplate(IEnlace.MENU_TOKEN, request, response);
				}else{
					request.setAttribute("mensajeError", "cuadroDialogo('S�lo se puede reponer Tokens bloqueados', 1);");
					evalTemplate( IEnlace.MENU_TOKEN, request, response );
				}
			}
		}
		if(accion.equals("solTokens"))
		{
			String[][] arrayUsuarios = TraeUsuarios(request);
			String [][] datos = new String [Integer.parseInt(arrayUsuarios[0][0].trim())][5];
			for(int i=1;i<=Integer.parseInt(arrayUsuarios[0][0].trim());i++) {
					datos[i - 1][0] = arrayUsuarios[i][1];			//cve usuario
					datos[i - 1][1] = arrayUsuarios[i][2];			//nombre
					String respuesta = "";
					try {
						respuesta = NIPManagerServices.consultaEstado(convierteUsr7a8(datos[i - 1][0]), "E");
					}catch(Exception e) { e.printStackTrace();  respuesta = "|1|PROBLEMA CON NIP MANAGER|";}
					System.out.println("Estado: " + respuesta);
					StringTokenizer stok = new StringTokenizer(respuesta, "|");
					int codError = -1;
					String status = "";
					if(stok.hasMoreTokens()) {
						codError = Integer.parseInt(stok.nextToken());
					}
					if(codError == 0 && stok.hasMoreTokens()) { // sali� bien
						stok.nextToken();	//ignorar el status de NIP Manager
						if(stok.hasMoreTokens()) {
							status = stok.nextToken();
						}
					}
					else if(stok.hasMoreTokens()) {
						status = stok.nextToken();
					}
					if(codError == 0) {
						switch(Integer.parseInt(status)) {
							case 1:		datos[i - 1][4] = status;						//numero de status
										status = "Solicitado";
										break;
							case 2:		datos[i - 1][4] = status;						//numero de status
										status = "En Tr�mite";
										break;
							case 3:		datos[i - 1][4] = status;						//numero de status
										status = "Solicitud Rechazada";
										break;
							case 4:		datos[i - 1][4] = status;						//numero de status
										status = "Entregado";
 									break;
							case 5:		datos[i - 1][4] = status;						//numero de status
										status = "Entregado a trav�s de Ejecutivo";
										break;
							case 6:		datos[i - 1][4] = status;						//numero de status
										status = "Activo";
										break;
							case 7:		datos[i - 1][4] = status;						//numero de status
										status = "Bloqueado";
										break;
							case 8:		datos[i - 1][4] = status;						//numero de status
										status = "Solicitud Vencida";
										break;
							default:	datos[i - 1][4] = status;						//numero de status
										status = "Estado desconocido";
										break;
						}
					}else{
						datos[i - 1][4] = status;						//numero de status
					}
					datos[i - 1][2] = status;						//status
					datos[i - 1][3] = obtenNoSerie(convierteUsr7a8(datos[i - 1][0]));	//serie
			}
			request.setAttribute("datos", datos);
			evalTemplate( IEnlace.SOLICITUD_CONTRATO, request, response );
		}

		if(accion.equals("PDFBloqueo"))
		{
			System.out.println("obteniendo datos para generacion de solicitud de baja");
			SolicitudBloqueoToken solBloq = new SolicitudBloqueoToken();

			Vector vec = (Vector)sess.getAttribute("clientes");
			String usuario[] = null;
			if(!vec.isEmpty()) usuario=(String[])vec.get(0);

			solBloq.setHoraBloqueo((String) sess.getAttribute("horaBloqueo"));
			solBloq.setFolio((String) sess.getAttribute("folio"));
			solBloq.setContrato(session.getContractNumber());
			solBloq.setCodCteEmpresa(convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
			solBloq.setRazonSocial(session.getNombreContrato());
			solBloq.setNombre(usuario[1]);
			solBloq.setCodCteUsuario(usuario[0]);
			solBloq.setNumSerieToken(obtenNoSerie(session.getUserID8()));
			solBloq.setRepresentante((String)sess.getAttribute("representante"));

			System.out.println("horaBloqueo = " + sess.getAttribute("horaBloqueo"));
			System.out.println("folio = " + sess.getAttribute("folio"));
			System.out.println("setContrato = " + session.getContractNumber());
			System.out.println("setCodCteEmpresa = " + convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
			System.out.println("setRazonSocial = " + session.getNombreContrato());
			System.out.println("setNombre = " + usuario[1]);
			System.out.println("setcodCtrUsuario = " + usuario[0]);
			System.out.println("setNumSerieToken = " + solBloq.getNumSerieToken());
			System.out.println("setRepresentante = " + (String)sess.getAttribute("representante"));

			ByteArrayOutputStream baos = solBloq.generaSolicitudBloqueo();
			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			// setting the content type
			response.setContentType("application/pdf");
			// the contentlength is needed for MSIE!!!
			response.setContentLength(baos.size());
			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
			out.close();
		}

		if(accion.equals("PDFContrato"))
		{
			Contrato con = new Contrato();
			con.setContrato(session.getContractNumber());
			con.setRazonSocial(session.getNombreContrato());
			con.setNombre((String)sess.getAttribute("representante"));
			con.setDireccion((String)sess.getAttribute("direccion"));
			con.setColonia((String)sess.getAttribute("colonia"));
			con.setCpEstado((String)sess.getAttribute("cpestado"));
			ByteArrayOutputStream baos = con.generaContrato();
			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			// setting the content type
			response.setContentType("application/pdf");
			// the contentlength is needed for MSIE!!!
			response.setContentLength(baos.size());
			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
			out.close();
		}
		if(accion.equals("PDFSolicitud"))
		{
			Solicitud sol = new Solicitud();
			System.out.println("Solicitud: "+request.getParameter("solicitud"));
			if(request.getParameter("solicitud")!=null && request.getParameter("solicitud").equals("inicial")) {
				String solIni = (String) request.getSession().getAttribute("solInicial");
				if(solIni == null || solIni.equals("true")) {
					System.out.println("Caso true");
					sol.setInicial(true);
				}
				else {
					System.out.println("Caso false");
					sol.setInicial(false);
				}
			}
			else {
				sol.setInicial(false);
			}
			sol.setCodCliente(convierteUsr7a8(obtenUsuarioEmpresa(session.getContractNumber())));
			sol.setContrato(session.getContractNumber());
			sol.setNomRepLegal((String)sess.getAttribute("representante"));
			sol.setRazonSocial(session.getNombreContrato());
			Vector vec = (Vector)sess.getAttribute("clientes");
			if(vec!=null)
				sol.setUsuarios(vec);

			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			ByteArrayOutputStream baos = sol.generaSolicitud();

			if(baos == null) {
				System.out.println("***** baos nulo *****");
				request.setAttribute(
					"mensajeError",
					"cuadroCerrar('Error al generar solicitud', 1);" +
					"window.history.back();");
				evalTemplate( IEnlace.IMPRESION_DOCUMENTOS, request, response );
				return;
			}
			else{
				System.out.println("***** baos correcto *****");
				// setting the content type
				response.setContentType("application/pdf");
				// the contentlength is needed for MSIE!!!
				response.setContentLength(baos.size());
				// write ByteArrayOutputStream to the ServletOutputStream
				ServletOutputStream out = response.getOutputStream();
				baos.writeTo(out);
				out.flush();
				out.close();
			}
		}
	} else {
		evalTemplate( IEnlace.SESION_FUERA, request, response );
	}
}
/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param usr
 * @return
 */
public String obtenNoSerie(String usr) {
	ResultSet rs = null;
	Connection conn = null;
	Statement sDup = null;
	 String serial = "";
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT substr(serialnumber, 1, 10) as SERIALNUMBER FROM TOKEN_ENLACE WHERE USERNAME = '" + usr + "'";
		mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace ("MigracionOTP - obtenNoSerie-> Query: <" + query + ">", mx.altec.enlace.utilerias.EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		while(rs.next()) {
			serial = rs.getString("SERIALNUMBER");
		}

	 }
	 catch(SQLException e) {
		e.printStackTrace();
		mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace ("MigracionOTP - obtenNoSerie -> Problemas para cargar serial Token para " + usr, mx.altec.enlace.utilerias.EIGlobal.NivelLog.INFO);
	 } finally {
		 EI_Query.cierraConexion(rs, sDup, conn);
	 }
	 return serial;
}
/**
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 * @param contrato
 * @return
 */
public String obtenUsuarioEmpresa(String contrato) {
	ResultSet rs  = null;
	Connection conn = null;
	Statement sDup = null;
	 String usuarioEmpresa = "";
	 try {
		conn = createiASConn ( Global.DATASOURCE_ORACLE );
		sDup = conn.createStatement();
		String query = "SELECT num_persona FROM nucl_cuentas WHERE num_cuenta = '"+ contrato +"'";
		mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace ("MigracionOTP - obtenUsuarioEmpresa-> Query: <" + query + ">", mx.altec.enlace.utilerias.EIGlobal.NivelLog.INFO);
		rs  = sDup.executeQuery(query);
		while(rs.next()) {
			usuarioEmpresa = rs.getString("num_persona");
		}

	 }
	 catch(SQLException e) {
		e.printStackTrace();
		mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace ("MigracionOTP - obtenUsuarioEmpresa -> Problemas para cargar el usuarioEmpresa " + contrato, mx.altec.enlace.utilerias.EIGlobal.NivelLog.INFO);
	 } finally {
		 EI_Query.cierraConexion(rs, sDup, conn);
	 }
	 return usuarioEmpresa;
}
}