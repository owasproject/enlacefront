/**
 * 
 */
package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaTcIndBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConfEdosCtaTcIndBO;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * @author FSW-Indra
 * @sice 04/03/2015
 *
 */
public class ConfEdosCtaTcIndAux extends BaseServlet {
	/**
	 * Version de la clase
	 */
	private static final long serialVersionUID = 1980980484874000010L;
	/** Constante para estado de cuenta disponible */
	private static final String EDO_CTA_DISP = "edoCtaDisponible";
	/** Constante para Secuencia de Domicilio **/
	private static final String SEQ_DOM = "hdSeqDomicilio";
	/** Constante para Estado de Cuenta Disponible **/
	private static final String EDOCTA_DISP = "hdEdoCtaDisponible";
	/** Constante para Suscripcion a Paperless **/
	private static final String SUSCR_PAPER = "hdSuscripPaperless";
	/** Constante para eval template **/
	private static final String EVAL_TEMPLATE="/jsp/confEdosCtasTcIndividual.jsp";
	/** Constante para Cadena hdCodCliente **/
    private static final String CAD_HDCOD_CLIENTE="hdCodCliente";
	/** Constante para Cadena hdCuenta **/
    private static final String CAD_HDCUENTA="hdCuenta";
	/** Constante para Cadena MensajeErr **/
    private static final String CAD_MENSAJE_ERR="MensajeErr";
    /** Constante para Cadena de Cuadro Dialogo **/
    private static final String CAD_CUADRO_DIALOGO	= "cuadroDialogo('%s', 1);";    
	/** Constante para suscripPaperless */
	private static final String SUS_PAPERLESS = "suscripPaperless";
	/** Constante para cuenta */
	private static final String CUENTA = "txtCuenta";
	/** Constante para cuenta nueva */
	private static final String CUENTA_NVA = "cuentaNva";
	/** TAG para logs **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfEdosCtaIndServlet ::: ";
	/** Constante para exportar cuentas **/
	private static final String CONTENT_TYPE="text/plain";
	/** Constante para exportar cuentas **/
	private static final String HNAME_CONTENT_DISPOSITION="Content-Disposition";
	/** Constante para exportar cuentas **/
	private static final String HVALUE_CONTENT_DISPOSITION="attachment;filename=Cuentas.txt";
	/** Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion */
	private static final int BYTES_DOWNLOAD = 1024;

	

	/**
	 * Metodo para validar que haya aceptado el contrato y se registra en bitacora
	 * @since 04/03/2015
	 * @author FSW-Indra PYME
	 * @param request peticion
	 * @param aceptaContrato codigo de aceptacion contrato
	 * @param aceCon acepto contrato
	 * @param valida valida
	 * @param tarjetaCuenta numero de tarjeta o cuenta
	 * @param bajaPaperless suscribe o cancela paperless
	 * @return boolean acepto o no contrato
	 */
	public boolean validaAceptaContrato( HttpServletRequest request,
			String aceptaContrato, boolean aceCon, String valida,
			String tarjetaCuenta, boolean bajaPaperless ) {
		
		EIGlobal.mensajePorTrace(this + "-----> Ingresando a validaAceptaContrato-->" + aceptaContrato+"<--", EIGlobal.NivelLog.INFO);
		ConfEdosCtaTcIndBean confEdosCtaTcIndBean = new ConfEdosCtaTcIndBean();
		//[IECP-01] Modifica estado de env�o de Estado de Cuenta Impreso Individual
		String bitaC_ECP=BitaConstants.EA_OP_PDF_EDO_CTA_IND;
		bitacorizaAdmin(request, confEdosCtaTcIndBean, bitaC_ECP, true);
		
		EIGlobal.mensajePorTrace(this + "-----> validaAceptaContrato-->" + aceptaContrato+"<--", EIGlobal.NivelLog.INFO);
		
		
		if ( aceptaContrato != null && aceptaContrato.equalsIgnoreCase(BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS) ) {	
			EIGlobal.mensajePorTrace("si Acepto contrato, bitacoriza aceptacion", EIGlobal.NivelLog.DEBUG);
			aceCon = true;
			//if (valida == null && !bajaPaperless) { Se comenta para bitacorizar tambi�n en baja de servicio
			//[ACAP-00] Acepta Convenio de Aceptacion Paperless Individual
			bitacorizaAdmin(request, null, BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS,true);
			// [ACAP-00] Realizado guardado en bitacora de operaciones.
			bitacoriza(request, tarjetaCuenta, BitaConstants.EA_CONV_ACEPT_PAPERLESS_CONFIRM, BitaConstants.CONCEPTO_ACEPTA_CONV_PAPERLESS);
			//}
		}	
		return aceCon;
	}
	/**
	 * Metodo para validar la condiguracion previa paperless
	 * @since 04/03/2015
	 * @author FSW-Indra PYME
	 * @param request peticion cliente
	 * @param response reponse
	 * @param tarjetaCuenta numero de tarjeta o cuenta
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void validaConfPrevia(HttpServletRequest request,
			HttpServletResponse response, String tarjetaCuenta)
			throws ServletException, IOException {
		String seqDomicilio = request.getParameter("hdSeqDomicilio");
		String codCliente = request.getParameter("hdCodCliente");
		ConfEdosCtaTcIndBO confEdosCtaTcIndBO = new ConfEdosCtaTcIndBO();
		String mensaje = confEdosCtaTcIndBO.consultaConfigPreviaPaperless(
				seqDomicilio, codCliente, tarjetaCuenta);
		EIGlobal.mensajePorTrace(mensaje, EIGlobal.NivelLog.DEBUG);
		if (!"".equals(mensaje)) {
			request.setAttribute(CAD_MENSAJE_ERR,
					String.format(CAD_CUADRO_DIALOGO, mensaje));
			subirAtributosPaper(request);
			evalTemplate(EVAL_TEMPLATE, request, response);
		}
	}
	/**
	 * Metodo para enviar notificaciones despues de que la operacion de
	 * suscripcion o cancelacion se realizo con exito
	 * @since 04/03/2015
	 * @author FSW-Indra PYME
	 * @param request peticion del cliente
	 * @param response response
	 * @param confEdosCtaTcIndBean informacion de la operacion
	 * @param bitaC_ECP codigo para registrar en bitacora
	 * @throws ServletException excepcion a manejar
	 * @throws IOException excepcion a manejar
	 */
	public void notificaOkOperacion(HttpServletRequest request,
			HttpServletResponse response,
			ConfEdosCtaTcIndBean confEdosCtaTcIndBean, String bitaC_ECP)
			throws ServletException, IOException {
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute("session");
		notifica(request, session, confEdosCtaTcIndBean,
				"PDF".concat(request.getParameter("tipoOp")));
		// [IECP-02]-[BICP-00]Finaliza la Inscripcion o Baja Estado de Cuenta a
		// Paperless
		bitacorizaAdmin(request, confEdosCtaTcIndBean, bitaC_ECP, true);
		String mensaje = "La operaci&oacute;n se ha realizado exitosamente. Puede consultar el estatus en el m&oacute;dulo de Consulta de "
				.concat("Solicitudes de Configuraci&oacute;n con el Folio: ")
				.concat(confEdosCtaTcIndBean.getFolioOp());
		request.setAttribute(CAD_MENSAJE_ERR,
				String.format(CAD_CUADRO_DIALOGO, mensaje));		
	}
	
	/**
     * Metodo para realizar la notificacion
     * @param request Objeto request
     * @param session BaseResource
     * @param confEdosCtaTcIndBean Bean de entrada
     * @param flujo Flujo de operacion
     */
    private void notifica(HttpServletRequest request, BaseResource session, ConfEdosCtaTcIndBean confEdosCtaTcIndBean, String flujo) {
    	EmailSender emailSender = null;
		EmailDetails beanEmailDetails = null;
		
		emailSender = new EmailSender();
		beanEmailDetails = new EmailDetails();
		beanEmailDetails.setNumeroContrato(session.getContractNumber());
		beanEmailDetails.setRazonSocial(session.getNombreContrato());
		beanEmailDetails.setCuentaDestino(confEdosCtaTcIndBean.getTarjeta());
		beanEmailDetails.setNumRef(confEdosCtaTcIndBean.getFolioOp());
		beanEmailDetails.setSecuencia(confEdosCtaTcIndBean.getSecDomicilio());
		beanEmailDetails.setEstatusFinal(String.valueOf(confEdosCtaTcIndBean.isSuscripPaperless()));
		beanEmailDetails.setEstatusActual(String.valueOf(confEdosCtaTcIndBean.isEdoCtaDisponible()));
		beanEmailDetails.setDescContrato(confEdosCtaTcIndBean.getDescCuenta());
		beanEmailDetails.setTipo_operacion(flujo);
				
		emailSender.sendNotificacion(request,IEnlace.EDO_CUENTA_IND,beanEmailDetails);		
    }
	
    /**
     * Metodo para subir al request atributos para configuracion paperless
     * @param request Request
     */
    private void subirAtributosPaper(HttpServletRequest request) {
    	request.setAttribute("segmentoDom", request.getParameter(SEQ_DOM));
		request.setAttribute("domicilio", request.getParameter("hdDomicilio"));
		request.setAttribute(EDO_CTA_DISP, "A".equals(request.getParameter(EDOCTA_DISP)) ? true : false);
		request.setAttribute(SUS_PAPERLESS, "S".equals(request.getParameter(SUSCR_PAPER)) ? true : false);
		request.setAttribute("codCliente", request.getParameter(CAD_HDCOD_CLIENTE));
		request.setAttribute(CUENTA, request.getParameter(CAD_HDCUENTA));
		request.setAttribute(CUENTA_NVA, request.getParameter(CAD_HDCUENTA));
		request.setAttribute("fechaConfigValida", request.getParameter("hdFechaConfigValida"));
    }
    
	/**
	 * Metodo para bitacorizar en Admin
	 * @param request request
	 * @param confEdosCtaTcIndBean informacion a bitacorizar
	 * @param flujo flujo momento en el que si bitacoriza
	 * @param use cual es codigo que se registra en bitacora
	 */
	public void bitacorizaAdmin(HttpServletRequest request, ConfEdosCtaTcIndBean confEdosCtaTcIndBean, String flujo, boolean use) {
    	BitaAdminBean bean = new BitaAdminBean();
    	String numBit  = flujo, tipoOp  = " ", tranTux = " ", idTabla = " ", campo   = " ", tabla   = " ", datoNvo = " ", dato = " ";
    	boolean token  = false;
    	
    	if (use) {
        	//Evalua flujo y asigna valores
        	if (flujo.equalsIgnoreCase(BitaConstants.EA_CONFIG_PDF_EDO_CTA_IND)) {
        		token = true;
        	}
        	if (flujo.equalsIgnoreCase(BitaConstants.EA_OP_PDF_EDO_CTA_IND)) {
        		tranTux="ODD4";
        	}
        	else if ( (flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS)) || 
        			(flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS)) ) {
        		tranTux="ODB2";
        	}
        	if ( (flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS)) || 
        			(flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS)) ) {
        		tipoOp = "U";
        		idTabla = confEdosCtaTcIndBean.getFolioOp();
            	//campo = "001".equals(request.getParameter("tipoOp")) ? request.getParameter("hdCuenta") : request.getParameter("hdTarjeta");
        		campo = "ID_EDO_CTA_CTRL";
        		tabla = "EWEB_EDO_CTA_DET";
            	datoNvo = "S".equals(request.getParameter(SUS_PAPERLESS)) ? "S" : "N";
            	dato = "S".equals(request.getParameter(SUS_PAPERLESS)) ? "N" : "S";
            	
        	}
        	
        	//Carga Datos en el bean
        	bean.setNumBit(numBit);
        	bean.setTipoOp(tipoOp);
        	bean.setServTransTux(tranTux);
        	bean.setIdTabla(idTabla);
        	bean.setCampo(campo);
        	bean.setTabla(tabla);
        	bean.setDato(dato);
        	bean.setDatoNvo(datoNvo);
          	bean.setReferencia(100000);
        	BitacoraBO.bitacorizaAdmin(request, request.getSession(), numBit, bean, token);
    	}
    }
	
	/**
     * Metodo para bitacorizar en TCT
     * @param request Servlet request
     * @param numCuenta Cuenta
     * @param tipoOp Tipo de operacion
     * @param concepto Concepto
     */
	public void bitacoriza(HttpServletRequest request, String numCuenta, String tipoOp, String concepto) {
    	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));    	
    	BitacoraBO.bitacoraTCT(request, request.getSession(), tipoOp, referencia, numCuenta, "", BitaConstants.COD_ERR_PDF_XML, concepto);
    }
	
	/**
	 * Metodo para consultar las cuentas y tarjetas a presentar en el combo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void ctasTarjetas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConfigModEdoCtaTDCBO consultaCtasTarjetas = new ConfigModEdoCtaTDCBO(); 
		List<ConfEdosCtaArchivoBean> cuentasTarjetas = new ArrayList<ConfEdosCtaArchivoBean>();
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		ConCtasPdfXmlServlet pdfXml = new ConCtasPdfXmlServlet();
		//boolean cambioContrato = pdfXml.cambioContrato(request);
		boolean cambioContrato =ConfigMasPorTCAux.cambiaContrato(request.getSession(), session.getContractNumber());
		String opcion = request.getAttribute("opcion") != null && !"".equals(request.getAttribute("opcion")) 
			? request.getAttribute("opcion").toString() : ""; 
		
		EIGlobal.mensajePorTrace(this + "-----> opcion: " + opcion, EIGlobal.NivelLog.INFO);
		
		if (("".equals(opcion) && request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null)
				|| cambioContrato) {
			cuentasTarjetas = consultaCtasTarjetas.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()), session.getContractNumber(), session.getUserProfile());
			EIGlobal.mensajePorTrace("Longitud de la lista: "+ cuentasTarjetas.size(),
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
			request.getSession().setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
		} else {
			ConfigMasPorTCAux.filtrarDatos(request, response);			
		}
		
		cuentasTarjetas = (List<ConfEdosCtaArchivoBean>) request.getAttribute(EdoCtaConstantes.LISTA_CUENTAS);	
		
		if( cuentasTarjetas.size() > 25 ){
			List<ConfEdosCtaArchivoBean> cuentasTarjetasAux = new ArrayList<ConfEdosCtaArchivoBean>();
			for( int i = 0 ; i < 25 ; i++ ){
				cuentasTarjetasAux.add( cuentasTarjetas.get(i) );
			}
			cuentasTarjetas = cuentasTarjetasAux;
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
			request.setAttribute("msgExcede", "-1");
			request.setAttribute("msgOption", "La consulta gener&oacute; m&aacute;s de 25 resultados, debe ser m&aacute;s espec&iacute;fico en el filtro");
		}				
	}
	
	/**
	 * Metodo para exportar las cuentas a un archivo
	 * @param request servlet request
	 * @param response servlet response
	 */
	public void exportar(HttpServletRequest request, HttpServletResponse response) {	
		EIGlobal.mensajePorTrace(String.format("%s Inicia Metodo: EXPORTAR",LOG_TAG),NivelLog.INFO);
		String flujo = request.getParameter("flujoOp");
		List<?> listCuentasTarjetas = null;
		InputStream input = null;
		OutputStream os = null;	
		if( "EXPCTA".equals( flujo )  ){
			listCuentasTarjetas = (ArrayList<?>)request.getSession().getAttribute("cuentas");
		} else {
			listCuentasTarjetas = (ArrayList<?>)request.getSession().getAttribute("tarjetas");
		}
		
		EIGlobal.mensajePorTrace(String.format("%s Se obtienen [%s] cuentas de la sesion.",LOG_TAG,(listCuentasTarjetas!=null)?listCuentasTarjetas.size():"0"), NivelLog.INFO);						
		
		try {
			StringBuilder tramas = new StringBuilder();		
			for(Object trama : listCuentasTarjetas ){
				tramas.append(trama.toString());
				tramas.append("\n");
			}		
			response.setContentType(CONTENT_TYPE);
			response.setHeader(HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION);
			EIGlobal.mensajePorTrace(String.format("%s Se configuran los Headers :::::\n" +
					"ContentType [%s]\n" +
					"Header[%s,%s]",LOG_TAG,CONTENT_TYPE,HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION),NivelLog.INFO);
			
			EIGlobal.mensajePorTrace(String.format("%s Crea objetos STREAM",LOG_TAG),NivelLog.INFO);			
			input = new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
			os = response.getOutputStream();	
			
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			EIGlobal.mensajePorTrace(String.format("%s Comienza Descarga",LOG_TAG),NivelLog.INFO);
			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
				os.flush();
				EIGlobal.mensajePorTrace(String.format("%sFlushing [%s] bytes Readed to outputStream.",
						LOG_TAG,read), NivelLog.DEBUG);
			}
			EIGlobal.mensajePorTrace(String.format("%s Cierra objetos STREAM",LOG_TAG),NivelLog.INFO);
			
			EIGlobal.mensajePorTrace(String.format("%s Termina Descarga",LOG_TAG),NivelLog.INFO);
		} catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} finally {
			try {
				if (os != null){
					os.close();
				}
				if (input != null){
					input.close();
				}
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
			}
			
		}
	}
}