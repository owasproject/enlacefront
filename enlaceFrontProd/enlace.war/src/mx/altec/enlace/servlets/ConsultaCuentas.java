/**
*   Isban Mexico
*   Clase: ConsultaCuentas.java
*   Descripcion: Servlet que gestiona la consulta de cuentas.
*
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid).
*/
package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.ws.webservices.engine.encoding.Base64;

import mx.altec.enlace.beans.ArchivoNominaDetBean;
import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConsultaCuentasBean;
import mx.altec.enlace.beans.CuentaBean;
import mx.altec.enlace.beans.ParametroConsultaCuentaBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.bo.ConsultaCuentasBO;
import mx.altec.enlace.bo.NominaPagosBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import java.text.Normalizer;

/**
 * Servlet implementation class ConsultaCuentas
 */
public class ConsultaCuentas extends BaseServlet {

	/**
	 * Constante para indicar que la ventana es de nomina.
	 */
	public static final String VENTANA_NOMINA = "10";

	/**
	 * Constante para indicar que la opcion es de nomina.
	 */
	public static final String OPCION_NOMINA = "11";
	/** Constante para referenciar lista de TDC*/
	private static final String LISTA_TDC = "listaTDC_CMov";
	/** Serial Version UID. */
	private static final long serialVersionUID = 1L;
	/** Constante para Cadena MensajeErr **/
    private static final String CAD_MENSAJE_ERR="MensajeErr";
    /** Constante para Cadena de Cuadro Dialogo **/
    private static final String CAD_CUADRO_DIALOGO	= "cuadroDialogo('%s', 1);";


	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("*** Peticion por metodo GET no permitida.", EIGlobal.NivelLog.ERROR);
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		Map<String, String[]> paramMap = req.getParameterMap();
		if (Utilerias.validarRequest(paramMap)){
			res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parametro invalido.");
        }
		else{
		BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
        String json = "";
        if(br != null){
            json = br.readLine();
        }
        br.close();

        EIGlobal.mensajePorTrace("*** ConsultaCuentas mensaje JSON:" + json, EIGlobal.NivelLog.ERROR);

        ObjectMapper mapper = new ObjectMapper();
        ParametroConsultaCuentaBean[] listaParametros = mapper.readValue(json, ParametroConsultaCuentaBean[].class);

		boolean sesionValida = SesionValida(req, res);
		if(sesionValida) {

			HttpSession sess = req.getSession();
            BaseResource session = (BaseResource) sess.getAttribute("session");

			//Se obtiene este parametro para el modulo de mancomunidad, ya que en base al Tipo de Operacion se muestran las cuentas

			if (getFormParameter(req, "tipoOp") != null) {
				String tipoOperacion = getFormParameter(req, "tipoOp");
				if ("CFRP".equals(tipoOperacion) || "FXIN".equals(tipoOperacion)) {
					session.setModuloConsultar("35@36@");
				} else if ("FXMB".equals(tipoOperacion)) {
					session.setModuloConsultar("38@");
				} else {
					session.setModuloConsultar("22@");
				}
			}

			StringBuilder jsonRespuesta = new StringBuilder();
			StringBuilder jsonResAux = new StringBuilder();
			String jsonResB64 ="";
			jsonRespuesta.append("{");
			jsonRespuesta.append("\"infoCombos\":\"[");

			for(int i = 0; i < listaParametros.length; i++) {

				 EIGlobal.mensajePorTrace("*** Entrando a consulta de empleeados JSON:" + listaParametros[i].getVentana() + listaParametros[i].getOpcion() , EIGlobal.NivelLog.ERROR);
				if (VENTANA_NOMINA.equals(listaParametros[i].getVentana().trim()) &&  OPCION_NOMINA.equals(listaParametros[i].getOpcion().trim())) {
					  EIGlobal.mensajePorTrace("*** Entrando a consulta de empleeados JSON:" + listaParametros[i].getVentana() + listaParametros[i].getOpcion() , EIGlobal.NivelLog.ERROR);
					 NominaPagosBO npbo = new NominaPagosBO();
					 jsonResAux.append(consultaCuentaEmpleados(listaParametros[i], npbo, req));
				} else {
					ConsultaCuentasBO ccbo = new ConsultaCuentasBO();
					jsonResAux.append(consultaCuenta(listaParametros[i], ccbo, req));
				}
				if(i >= 0 && (i + 1) < listaParametros.length) {
					jsonResAux.append(",");
				}
	        }

			jsonResB64 = Base64.encode(jsonResAux.toString().getBytes());

			jsonRespuesta.append(jsonResB64);
			jsonRespuesta.append("]\",\"serial\":\"");
			jsonRespuesta.append(this.crc(jsonResB64));		
			jsonRespuesta.append("\"}");
			EIGlobal.mensajePorTrace("***Trama respuesta:  [" + jsonRespuesta + "]" +"CheckSum"+ this.crc(jsonResB64), EIGlobal.NivelLog.DEBUG);
			

			this.writeResponse(res,jsonRespuesta.toString());
		}
		}
	}
	
	   public static int crc(String string) {
		   System.out.println(string);
	        int result = 0;
	        for(final char c : string.toCharArray()) {
	            result = (result << 1) ^ c;
	        }
            System.out.println(result);
	        return result;
	    }

	/**
	 * Consulta cuenta.
	 * @param param the param
	 * @param ccbo the ccbo
	 * @return the string
	 */
	private String consultaCuenta(ParametroConsultaCuentaBean param, ConsultaCuentasBO ccbo, HttpServletRequest req) {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String moduloconsultar = "";
		String chogan = " ";
		String jsonCuenta = "";
		String ventana = param.getVentana();
		String opcion = param.getOpcion();
		String numeroCuenta = StringUtils.defaultString(param.getNumeroCuenta()); // -> "ctasantander"
		String descripcionCuenta = StringUtils.defaultString(param.getDescripcion()); // -> "descrip"
		String nombreCombo = param.getNombreCombo();

		List<CuentaBean> cuentas = null;
		List<CuentaBean> listaTDC = new ArrayList<CuentaBean>();
    	EIGlobal.mensajePorTrace("***cuentasSerfinSantander Ventana [" + ventana + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***cuentasSerfinSantander opcion  [" + opcion + "]", EIGlobal.NivelLog.DEBUG);

		moduloconsultar = session.getModuloConsultar(); // Se obtiene la clave del modulo en sesion
		moduloconsultar = ccbo.consultaModulo(opcion, moduloconsultar);

		EIGlobal.mensajePorTrace("***cuentasSerfinSantander Modulo  [" + moduloconsultar + "]", EIGlobal.NivelLog.DEBUG);

		if ("28@".equals(moduloconsultar)) {
			EIGlobal.mensajePorTrace("ConsultaCuentas - consultaCuenta() [Entra al modulo 28]-----------------------------", EIGlobal.NivelLog.INFO);

			//
			boolean cambioContrato=ConfigMasPorTCAux.cambiaContrato(req.getSession(), session.getContractNumber());
			EIGlobal.mensajePorTrace("ConsultaCuentas - LISTA_TDC-----------------------------"+sess.getAttribute(LISTA_TDC), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("ConsultaCuentas - cambioContrato----------------------------"+cambioContrato, EIGlobal.NivelLog.INFO);
			if (sess.getAttribute(LISTA_TDC) == null || cambioContrato) {
				ConfigModEdoCtaTDCBO configMasTCCuentaBO = new ConfigModEdoCtaTDCBO();
				cuentas = configMasTCCuentaBO.consultaTarjetasCredito(convierteUsr8a7(session.getUserID()),session.getContractNumber(), session.getUserProfile());
				sess.setAttribute(LISTA_TDC, cuentas);
				EIGlobal.mensajePorTrace(String.format("ConsultaCuentas - consultaCuenta() - Entro a poner TC Cuentas en sesion --->%s",cuentas.size()),
						NivelLog.DEBUG);
			} else {
				cuentas = (List<CuentaBean>)sess.getAttribute(LISTA_TDC);
				EIGlobal.mensajePorTrace(String.format("ConsultaCuentas - consultaCuenta() - NO Entro a recuperar TC Cuentas --->%s",cuentas.size()),
						NivelLog.DEBUG);
			}
			if (numeroCuenta.length() > 0 || descripcionCuenta.length() > 0) {
				listaTDC = filtrarDatosTDC(cuentas, numeroCuenta, descripcionCuenta);
			} else {
				//Asigna las primeras 25 de la lista cuando no hay filtro
				listaTDC = new ArrayList<CuentaBean>();
				int contador=0;
				for(CuentaBean beanCuentas : cuentas){
					listaTDC.add(beanCuentas);
					contador ++;
					if (contador > 24){
						beanCuentas = new CuentaBean();
						beanCuentas.setNumCuenta("");
						beanCuentas.setDescripcion("La consulta genero mas de 25 resultados, debe ser mas especifico en el filtro");
						listaTDC.add(beanCuentas);
						break;
					}
				}
				EIGlobal.mensajePorTrace("ConsultaCuentas - Asigna las primeras 25 de la lista cuando no hay filtro " + listaTDC.size(), EIGlobal.NivelLog.INFO);
			}
			jsonCuenta = generarJSONCuenta(listaTDC, nombreCombo);
			EIGlobal.mensajePorTrace("ConsultaCuentas - consultaCuenta() [Sale del modulo 28]-----------------------------", EIGlobal.NivelLog.INFO);
		}else{
			EIGlobal.mensajePorTrace("ConsultaCuentas - consultaCuenta() [Sale del modulo 29]-----------------------------"+moduloconsultar, EIGlobal.NivelLog.INFO);
			String nombreArchivo = session.getUserID8() + ".ctas";
			if (IEnlace.MAbono_cambio_dolar.equals(moduloconsultar.trim()) && "5".equals(opcion.trim())) {//ARS Vector se agrega para identificar la Consulta de Cuentas en Dolares
				chogan = "D";
				moduloconsultar = IEnlace.MDep_Inter_Abono;
			}
			if (IEnlace.MDep_inter_cargo.equals(moduloconsultar.trim()) && "6".equals(opcion.trim())) {//ARS Vector se agrega para identificar la Consulta de Cuentas en Dolares
				chogan = "D";
				moduloconsultar = IEnlace.MCargo_transf_dolar;
			}
			//ARS Vector se agrega para identificar la Consulta de Cuentas en Dolares
			boolean resultadoConsulta = llamado_servicio50CtasInteg(moduloconsultar, numeroCuenta, chogan, descripcionCuenta, nombreArchivo, req);
			if(resultadoConsulta) {
				String archivo = session.getUserID8() + ".ctas";
				boolean facuntadSinCuenta = false;
				facuntadSinCuenta = (session.getFacultad("DEPSINCAR"))?true:false;
				String[] argParametrosNoContempladosDao = new String[1];
				if(!numeroCuenta.equals("")) {
					argParametrosNoContempladosDao[0] = "";
				}else{
					argParametrosNoContempladosDao[0] = String.valueOf(facuntadSinCuenta);
				}

				cuentas = ccbo.consultaCuentas(archivo, moduloconsultar, argParametrosNoContempladosDao);
				for(CuentaBean obj : cuentas){
					EIGlobal.mensajePorTrace("ConsultaCuentas - consultaCuenta() REGISTRO_CTA::: "+obj.getDescripcion(), EIGlobal.NivelLog.INFO);
				}
				jsonCuenta = generarJSONCuenta(cuentas, nombreCombo);
				EIGlobal.mensajePorTrace("ConsultaCuentas - consultaCuenta() [Sale del modulo 30]-----------------------------", EIGlobal.NivelLog.INFO);
			}
		}
		return jsonCuenta;
	}

	/**
	 * @param listaFiltro lista  filtro
	 * @param cuenta  cuenta
	 * @param descripcion  descripcion
	 * @return  lista del tipo CuentaBean
	 */
	public List<CuentaBean> filtrarDatosTDC(List<CuentaBean> listaFiltro, String cuenta, String descripcion) {
		List<CuentaBean> cuentasNueva = new ArrayList<CuentaBean>();
		if (cuenta.length() > 0 || descripcion.length() > 0) {
			EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Con filtro..." , EIGlobal.NivelLog.INFO);
			for(int i = 0; i < listaFiltro.size(); i++) {
				// Busqueda unicamente por cuenta.
				if((("".equals(descripcion.trim())) && (cuenta.length() > 0)) && (listaFiltro.get(i).getNumCuenta().contains(cuenta))) {
						EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Con filtro cuenta" , EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Cuenta: " + cuenta, EIGlobal.NivelLog.INFO);
						cuentasNueva.add(listaFiltro.get(i));
						/** continue;*/
				}
				// Busqueda unicamente por descripcion.
				if((("".equals(cuenta.trim())) && (descripcion.length() > 0)) && (listaFiltro.get(i).getDescripcion().contains(descripcion))) {
						EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Con filtro descripcion" , EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Descripcion: " + descripcion, EIGlobal.NivelLog.INFO);
						cuentasNueva.add(listaFiltro.get(i));
						/** continue;*/
				}
				// Busqueda por cuenta y por descripcion.
				if((cuenta.length() > 0 && descripcion.length() > 0) && (listaFiltro.get(i).getNumCuenta().contains(cuenta) &&
						listaFiltro.get(i).getDescripcion().contains(descripcion))) {
							EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Con filtro cuenta y descripcion" , EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Descripcion: " + descripcion, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("ConsultaCuentas - filtrarDatosTDC() -----> Cuenta: " + cuenta, EIGlobal.NivelLog.INFO);
							cuentasNueva.add(listaFiltro.get(i));
							/** continue;*/
				}
			}
		}
		return cuentasNueva;
	}

	/**
	 * Consulta cuenta empleados.
	 * @param param the param
	 * @param ccbo the ccbo
	 * @return the string
	 */
	private String consultaCuentaEmpleados(ParametroConsultaCuentaBean param, NominaPagosBO ccbo, HttpServletRequest req) {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String moduloconsultar = "";
		String jsonCuenta = "";
		String ventana = param.getVentana();
		String opcion = param.getOpcion();
		String numeroCuenta = StringUtils.defaultString(param.getNumeroCuenta()); // -> "ctasantander"
		String descripcionCuenta = StringUtils.defaultString(param.getDescripcion()); // -> "descrip"
		String nombreCombo = param.getNombreCombo();

    	EIGlobal.mensajePorTrace("***cuentasSerfinSantander Ventana [" + ventana + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("***cuentasSerfinSantander opcion  [" + opcion + "]", EIGlobal.NivelLog.DEBUG);

		moduloconsultar = session.getModuloConsultar(); // Se obtiene la clave del modulo en sesion


		EIGlobal.mensajePorTrace("***cuentasSerfinSantander Modulo  [" + moduloconsultar + "]", EIGlobal.NivelLog.DEBUG);


			try {
				List<ArchivoNominaDetBean> cuentasEmpleado = ccbo.consultaCtasCatalogo(session.getContractNumber(), numeroCuenta, descripcionCuenta);
				List<CuentaBean> cuentas = new ArrayList<CuentaBean>();
				Boolean agregar = true;

				for (ArchivoNominaDetBean emp : cuentasEmpleado) {
					if (agregar) {
					CuentaBean cuenta = new CuentaBean();
					cuenta.setNumCuenta(emp.getCuentaAbono());
					cuenta.setTipoCuenta("");
					cuenta.setDescripcion(emp.getAppPaterno() + " " + emp.getAppMaterno() + " "  + emp.getNombre());
					cuenta.setTipoProd(emp.getIdEmpleado());
					cuenta.setBanco(emp.getAppPaterno());
					cuenta.setPais(emp.getAppMaterno());
					cuenta.setCiudad(emp.getNombre());
					cuenta.setPlaza("");
					cuenta.setSucursal("");
					cuenta.setDivisa("");
					cuentas.add(cuenta);
					if(cuentas.size() >= 25) { //Valida si excede las 25 cuentas...
						cuenta = new CuentaBean();
						cuenta.setNumCuenta("");
						cuenta.setDescripcion("La consulta gener� m�s de 25 resultados, debe ser m�s espec�fico en el filtro");
						cuenta.setTipoCuenta("");
						cuentas.add(cuenta);
						agregar  = false;
					}
					}
				}

				jsonCuenta = generarJSONCuenta(cuentas, nombreCombo);
			}
			catch (BusinessException e) {
				// TODO: handle exception
			}

		return jsonCuenta;
	}


	/**
	 * Escribe la respuesta.
	 * @param res the res
	 * @param jsonRespuesta the json respuesta
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void writeResponse(HttpServletResponse res, String jsonRespuesta) throws IOException {
//		res.setHeader("Content-Type", "text/html; charset=iso-8859-1");
		res.setHeader("Content-Type", "application/json; charset=iso-8859-1");
		ServletOutputStream sos = null;

		try {
			sos = res.getOutputStream();
			sos.print(jsonRespuesta);
			sos.flush();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("***Error al cerrar outputstream:  [" + e.getMessage() + "]", EIGlobal.NivelLog.DEBUG);
		} finally {
			if (sos != null) {
				sos.close();
			}
		}
	}

	/**
	 * Genera json respuesta.
	 * @param cuentas the cuentas
	 * @return the string
	 */
	private String generarJSONCuenta(List<CuentaBean> cuentas, String nombreCombo) {
		StringBuilder jsonResp = new StringBuilder();
		jsonResp.append("{");
		jsonResp.append(parseCuentas(cuentas));
		jsonResp.append(",\"nombreCombo\":\"");
		jsonResp.append(nombreCombo);
		jsonResp.append("\"");
		jsonResp.append("}");
		return jsonResp.toString();
	}

	/**
	 * Arma el JSON de cuentas.
	 * @param cuentas the cuentas
	 * @return the string builder
	 */
	private String parseCuentas(List<CuentaBean> cuentas) {
		EIGlobal.mensajePorTrace("ConsultaCuentas - parseCuentas() ***Armando trama de respuesta, " + cuentas.size() + " registros.", EIGlobal.NivelLog.DEBUG);
		StringBuilder str = new StringBuilder();
		str.append("\"cuentas\": [ ");

		for(int i=0; i < cuentas.size(); i++) {
			CuentaBean cuenta = cuentas.get(i);
			str.append(this.parseCuenta(cuenta));
			if(i < (cuentas.size() -1)) {
				str.append(",");
			}
		}
		str.append("]");
		return str.toString();
	}

	/**
	 * Ensambla JSON de Cuenta.
	 * @param cuenta the cuenta
	 * @return the string
	 */
	private String parseCuenta(CuentaBean cuenta) {
		EIGlobal.mensajePorTrace("ConsultaCuentas - parseCuenta() ***Numero de cuenta [" + cuenta.getNumCuenta() + "]", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ConsultaCuentas - parseCuenta() ***Descripcion cuenta  [" + cuenta.getDescripcion() + "]", EIGlobal.NivelLog.DEBUG);
		StringBuilder str = new StringBuilder();
		String auxDescripcion = "";
		auxDescripcion = Normalizer.normalize(cuenta.getDescripcion(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

		
		str.append("{");
		str.append("\"numero\":\"");
		str.append(cuenta.getNumCuenta());
		str.append("\",");
		str.append("\"descripcion\":\"");
		str.append(auxDescripcion);
		str.append("\",");

		str.append("\"banco\":\"");
		str.append(cuenta.getBanco());
		str.append("\",");
		str.append("\"plaza\":\"");
		str.append(cuenta.getPlaza());
		str.append("\",");
		str.append("\"sucursal\":\"");
		str.append(cuenta.getSucursal());
		str.append("\",");
		str.append("\"pais\":\"");
		str.append(cuenta.getPais());
		str.append("\",");
		str.append("\"ciudad\":\"");
		str.append(cuenta.getCiudad());
		str.append("\",");
		str.append("\"divisa\":\"");
		str.append(cuenta.getDivisa());
		str.append("\",");

		str.append("\"tipre\":\"");
		str.append(cuenta.getTipoCuenta());
		str.append("\",");
		str.append("\"tipro\":\"");
		str.append(cuenta.getTipoProd());
		str.append("\"");
		str.append("}");
		return str.toString();
	}
}