package mx.altec.enlace.servlets;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.*;
import java.lang.reflect.*;
import java.sql.*;
import java.io.*;


public class El_Ciudades extends BaseServlet{

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	EIGlobal.mensajePorTrace("El_Ciudades - execute(): Entrando a seleccion de ciudades de Estados Unidos. el bueno", EIGlobal.NivelLog.INFO);
	 String strPlazas	="";
	 String totales		="";
	 String Error		="SUCCESS";

	 int totalPlazas = 0;
	 int salida=0;
	try{
		  Connection conn = createiASConn("jdbc/enlace");

		  if(conn == null){
			EIGlobal.mensajePorTrace("El_Ciudades - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
		    Error="NOSUCCESS";
		  }

//		  totales="Select count(*) from camb_banco_eu"; // se necesita saber con que campo se sacan las ciudades
		  totales ="SELECT count(RTRIM(CIUDAD)) AS FCIUDAD, ESTADO From camb_banco_eu ORDER BY FCIUDAD";
		PreparedStatement contQuery = conn.prepareStatement(totales);
		if(contQuery == null)
		   {
		 EIGlobal.mensajePorTrace("El_Plazas - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
		 Error="NOSUCCESS";
		}
		ResultSet contResult = contQuery.executeQuery();
		//**********
		if(contResult==null)
       {
         EIGlobal.mensajePorTrace("El_Plazas - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
	     Error="NOSUCCESS";
       }

		 if(contResult.next())
		  totalPlazas=Integer.parseInt(contResult.getString(1));
		  else
		  totalPlazas =0;
		  EIGlobal.mensajePorTrace("El_Ciudades - execute(): Total de Ciudades: "+Integer.toString(totalPlazas), EIGlobal.NivelLog.INFO);

		/************************************************** Contar registros de plazas .... */
		  String sqlplaza ="SELECT DISTINCT(RTRIM(CIUDAD)) AS FCIUDAD, ESTADO From camb_banco_eu ORDER BY FCIUDAD";
		  PreparedStatement plazaQuery = conn.prepareStatement(sqlplaza);
		  if(plazaQuery == null)
		   {
		 EIGlobal.mensajePorTrace("El_Ciudades - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
		 Error="NOSUCCESS";
		}
		ResultSet plazaResult = plazaQuery.executeQuery();

      if(plazaResult==null)
       {
         EIGlobal.mensajePorTrace("El_Ciudades - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
	     Error="NOSUCCESS";
       }

        if(Error.equals("SUCCESS"))
       {
         String arcLinea="";
         String nombreArchivo=IEnlace.DOWNLOAD_PATH+"El_Ciudades.html";

		 EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
         if(ArcSal.recuperaArchivo())
           {
             arcLinea="\n<html>";
			 arcLinea+="\n<head><title>Ciudades de Estados Unidos</title>";
			 arcLinea+="<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>";
			 arcLinea+="\n</head>";
			 arcLinea+="\n<body bgcolor='#FFFFFF'>";
             arcLinea+="\n <form name=Forma>";
             arcLinea+="\n   <table border=0>";
             arcLinea+="\n      <tr>";
             arcLinea+="\n        <th class='tittabdat'> Seleccione una ciudad </th>";
             arcLinea+="\n      </tr>";
             arcLinea+="\n      <tr>";
             arcLinea+="\n        <td class='tabmovtex'>";
             arcLinea+="\n         <select NAME=SelCiudad size=20 class='tabmovtex'>\n";
             if(!ArcSal.escribeLinea(arcLinea))
               EIGlobal.mensajePorTrace("El_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
			   plazaResult.next();
			   totalPlazas =	5748;
           for(int i=0; i<totalPlazas; i++)
		          {
				  arcLinea="\n          <option value="+plazaResult.getString(2).trim()+">"+plazaResult.getString(1).trim()+", "+plazaResult.getString(2).trim()+"</option>";
				if (arcLinea != null){
	                if(!ArcSal.escribeLinea(arcLinea))
		            EIGlobal.mensajePorTrace("El_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
					plazaResult.next();
					}
				}

				/*
				do
				{
					arcLinea="\n          <option value="+plazaResult.getString(2).trim()+">"+plazaResult.getString(1).trim()+", "+plazaResult.getString(2).trim()+"</option>";
					if(!ArcSal.escribeLinea(arcLinea))
					EIGlobal.mensajePorTrace("El_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
				}while(plazaResult.next()==0);
				*/
           EIGlobal.mensajePorTrace("El_Ciudades - execute(): Se escribieron las ciudades...", EIGlobal.NivelLog.INFO);

            while(!ArcSal.eof())
               {
                if(!ArcSal.escribeLinea("                                                                                  "))
                 EIGlobal.mensajePorTrace("El_Ciudades - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
               }
             arcLinea+="\n         </select>";
			 arcLinea+="\n        </td>";
			 arcLinea+="\n      </tr>";
             arcLinea+="\n      <tr>";
			 arcLinea+="\n          <td><br></td>";
			 arcLinea+="\n      </tr>";
             arcLinea+="\n      <tr>";
             arcLinea+="\n        <td align=center>";
			 arcLinea+="\n         <table border=0 align=center cellspacing=0 cellpadding=0>";
			 arcLinea+="\n          <tr>";
			 arcLinea+="\n           <td>";
			 arcLinea+="\n            <a href='javascript:opener.Selecciona(3); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>";
			 arcLinea+="\n           </td>";
			 arcLinea+="\n          </tr>";
			 arcLinea+="\n         </table>";
             arcLinea+="\n        </td>";
             arcLinea+="\n      </tr>";
             arcLinea+="\n    </table>\n </form>\n</body>\n</html>";

            if(!ArcSal.escribeLinea(arcLinea))
               EIGlobal.mensajePorTrace("El_Ciudades - execute(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
             ArcSal.cierraArchivo();
            //*************************************************************
			 ArchivoRemoto archR = new ArchivoRemoto();
			 if(!archR.copiaLocalARemoto("El_Ciudades.html","WEB"))
			  EIGlobal.mensajePorTrace("El_Ciudades - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
			 //**************************************************************
           }
          else
           {
             strPlazas+="\n<p><br><table border=0 width=90% align=center>";
             strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
             strPlazas+="\n<tr><th bgcolor=red><font color=white>En este momento no se pudo generar catalogo. Por favor intente mas tarde.<font></td></tr>";
             strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
             strPlazas+="\n</table>";
           }
         plazaResult.close();
       }
      else
       {
         strPlazas+="\n<p><br><table border=0 width=90% align=center>";
         strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
         strPlazas+="\n<tr><th bgcolor=red><font color=white>Su transaccitransaccionoacute;n no puede ser atendida. Por favor intente mas tarde.<font></td></tr>";
         strPlazas+="\n<tr><td bgcolor='#F0F0F0'><br></td></tr>";
         strPlazas+="\n</table>";
       }

	}catch( SQLException sqle ){
		sqle.printStackTrace();
	} catch(Exception e) {
		e.printStackTrace();
	}
	 req.setAttribute("Sciudades",strPlazas);
	 evalTemplate("/jsp/El_Ciudades.jsp", req, res);

	EIGlobal.mensajePorTrace("El_Ciudades - execute(): Saliendo de seleccion de ciudades de Estados Unidos.", EIGlobal.NivelLog.INFO);

	}
}
