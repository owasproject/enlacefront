package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.*;


/**<B><I><code>MCL_PosMov</code></I></B>
* <P>Modificaciones:
* 01/10/2002 Se cambiaron vocales acentuadas por su c&oacute;digo de escape.<BR>
* </P>
*/
public class MCL_PosMov extends BaseServlet{

		String pipe         = "|";

    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		String tipo = "";
		String strTmp      = "";
		String cadenaTCT   = "";
		String cadenaFac   = "";
		String contrato    = "";
		String usuario     = "";
		String clavePerfil = "";

		String tipoRelacion = "P";
		String tipoModulo   = "";
		String tipoCuenta   = "";
		String strTipo      = "";
		String strMov       = "";
		String titulo       = "";

		String arcAyuda	   = "";

		boolean existenCuentas = false;

		EI_Tipo Saldo      = new EI_Tipo(this);
		EI_Tipo TCTArchivo = new EI_Tipo(this);
		EI_Tipo FacArchivo = new EI_Tipo(this);


		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("MCL_PosMov - execute(): Entrando a Posicion - Movimientos (menu).", EIGlobal.NivelLog.INFO);

		if(SesionValida( req, res))
		 {
			//Variables de sesion ...
			contrato = session.getContractNumber();

			usuario = session.getUserID8();
			clavePerfil = session.getUserProfile();

			tipoModulo = ( String ) req.getParameter("Tipo");
			tipoCuenta = ( String ) req.getParameter("Cuenta");

			if(tipoModulo.equals("Movimientos"))
			  strTipo = "Movimientos";
			else
			  strTipo = "Posici&oacute;n";

			if(tipoCuenta.equals("5"))
			  {
				strMov = "Tarjeta";
				titulo = "Consultas &gt; "+strTipo+" &gt; Tarjeta de Cr&eacute;dito";
				if(tipoModulo.equals("Movimientos"))
				 arcAyuda="s25230h"; //Mov Tarjeta
			    else
				 arcAyuda="s25130h"; //Pos Tarjeta
			  }
			 else
			  {
				strMov = "L&iacute;nea";
				titulo = "Consultas &gt; "+strTipo+" &gt; L&iacute;nea de Cr&eacute;dito";
				if(tipoModulo.equals("Movimientos"))
				 arcAyuda="s26310h"; //Mov Linea
			    else
				 arcAyuda="s26270h"; //Pos Linea
		      }

			/*************************************** Recuperar Cuentas desde archivo.amb *********/
			String nombreArchivo = IEnlace.LOCAL_TMP_DIR+"/"+usuario+".amb";
			String arcLinea = "";

			EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

			if(ArcEnt.abreArchivoLectura()){
				boolean noError = true;
				do{
					arcLinea = ArcEnt.leeLinea();
					if(!arcLinea.equals("ERROR"))
					 {
						if(arcLinea.substring(0,5).trim().equals("ERROR"))
						  break;
						// Obtiene cuentas asociadas ...
						if(arcLinea.substring(0,1).equals("2"))
							cadenaTCT  += arcLinea.substring(2,arcLinea.lastIndexOf(';'))+" @";
						// Obtiene facultades para cuentas ...
						if(arcLinea.substring(0,1).equals("7"))
							cadenaFac  += arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
					 }
					else
						noError = false;
				}while(noError);

				ArcEnt.cierraArchivo();

				// Registros de lineas y tarjetas (5,0)
				TCTArchivo.iniciaObjeto(';','@',cadenaTCT);
				// Registros de facultades (7)
				FacArchivo.iniciaObjeto(';','@',cadenaFac);

				// Si existen cuentas asociadas ...
				if(TCTArchivo.totalRegistros >= 1)
				  {
					for(int i = 0;i<TCTArchivo.totalRegistros;i++)
					  {
						if(TCTArchivo.camposTabla[i][3].equals(tipoCuenta.trim()))
						  {
							if(TCTArchivo.camposTabla[i][3].equals("5"))
							  {
								// Tarjetas de Cr�dito ...
								// Revisar facultades para cuenta[i] - Tarjeta.
								tipoRelacion = TCTArchivo.camposTabla[i][2];
								if( (TCTArchivo.camposTabla[i][2].trim().equals("P") && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOS",req)) ||
								(TCTArchivo.camposTabla[i][2].trim().equals("T") && tieneFacultad(FacArchivo,tipoRelacion,TCTArchivo.camposTabla[i][1],"CONSSALDOTER",req)) )
								  {
									existenCuentas = true;
									TCTArchivo.camposTabla[i][4] = "TARJETA CREDITO";
									for(int a = 0;a<6;a++)
									 {
										strTmp += TCTArchivo.camposTabla[i][a];
										strTmp += pipe;
									 }
									strTmp += " @";
								}
							}
						   else
							{
								// Lineas de Cr�dito ...
								// Revisar facultades para cuenta[i] - Linea.
								tipoRelacion = TCTArchivo.camposTabla[i][2];
								if(tieneFacultadNueva(FacArchivo,TCTArchivo.camposTabla[i][0].trim(),"TECRESALDOSC",req) && tipoRelacion.trim().equals("P") )
								 {
									existenCuentas = true;
									TCTArchivo.camposTabla[i][4] = "LINEA CREDITO";
									for(int a = 0;a<6;a++)
									 {
										strTmp += TCTArchivo.camposTabla[i][a];
										strTmp +=pipe;
									 }
									strTmp +=" @";
								}
							}
						}
					}
					Saldo.iniciaObjeto(strTmp);
				}
				generaTablaConsultar(existenCuentas,strTipo,TCTArchivo,TCTArchivo,Saldo,strMov,titulo,arcAyuda,tipoCuenta, tipoModulo, req, res );
			}else
			despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res);
		}
		EIGlobal.mensajePorTrace("MCL_PosMov - execute(): Saliendo de Posicion - Movimientos (menu).", EIGlobal.NivelLog.INFO);

    }

/*************************************************************************************/
/*********************************************************** tabla Consultar         */
/*************************************************************************************/
	public int generaTablaConsultar(boolean existenCuentas,String strTipo,EI_Tipo FacArchivo,EI_Tipo TCTArchivo,EI_Tipo Saldo,String strMov,String titulo, String arcAyuda,String tipoCuenta,String tipoModulo, HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException{
		String strTabla = "";
		String[] titulos = {"",
		     				"Cr&eacute;dito",
		     				"Descripci&oacute;n",
							"Tipo"};
		/*********************
		Indice Dato

		//Datos del archivo de ambiente.
			0 2|
			1 4915121000545517|             //credito
			2 VICTOR GONZALEZ ESTRADA|      //descripcion
			3 P|                            //tipo relacion
			4 5|                            //tipo de cuenta,
			5  |                            //" cuenta anterior "
			6 ANGEL OVIEDO ESQUIVEL |       //Descripcion titular
		*********************/

		int[] datos = {2,1,0,1,4};
		int[] value = {5,0,1,2,3,4,5};

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

		if(Saldo.totalRegistros>= 1)
		  {
			//Si es posicion para tarjeta se pueden seleccionar varias
			//opciones para consultar saldos.
			if(tipoModulo.equals("Posicion") && tipoCuenta.equals("5"))
			 {
				datos[1] = 4;
				req.setAttribute("radTabla","<input type=hidden name=radTabla>");
			 }

			strTabla += Saldo.generaTabla(titulos,datos,value);

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			req.setAttribute("strCuentas",TCTArchivo.strOriginal);
			req.setAttribute("FacArchivo",FacArchivo.strOriginal);
			req.setAttribute("Tabla",strTabla);
			req.setAttribute("FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
			req.setAttribute("Tipo",tipoModulo);

			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado",CreaEncabezado("Consultas de"+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,req));

			evalTemplate("/jsp/MCL_PosMov.jsp", req, res);
		 }
	    else
		 {
			if(existenCuentas)
			  despliegaPaginaError("No tiene facultad.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo,arcAyuda, req, res);
			else
			  despliegaPaginaError("No se encontraron "+strMov+"s de Cr&eacute;dito Asociadas.","Consultas de "+strTipo+" de "+strMov+" de Cr&eacute;dito",titulo, arcAyuda,req, res);
		 }
		return 1;
     }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultad(EI_Tipo FacArchivo,String tipoRelacion,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
          {
            if(tipoRelacion.trim().equals("P"))
              FacultadAsociadaACuenta=true;
            else
              FacultadAsociadaACuenta=false;
          }
       }
	  EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }

    boolean facultadAsociada(EI_Tipo FacArchivo,String credito,String facultad,String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
        for(int i=0;i<FacArchivo.totalRegistros;i++)
         {
           if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
            if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
             if(FacArchivo.camposTabla[i][3].trim().equals(signo))
			  {
			    EIGlobal.mensajePorTrace("MCL_PosMov - facultadAsociada(): Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
                return true;
			  }
         }
	   EIGlobal.mensajePorTrace("MCL_PosMov - facultadAsociada(): Facultad asociada regresa false: Fac=" +facultad+" Credito="+credito+" signo="+signo, EIGlobal.NivelLog.INFO);
       return false;
     }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultadNueva(EI_Tipo FacArchivo,String credito,String facultad, HttpServletRequest request)
     {
      //############ Cambiar ....
      //return true;

	  boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

      facultad=facultad.trim();

      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
        FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
        FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.INFO);

	  EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo -
         if(facultadAsociada(FacArchivo,credito,facultad,"-"))
           FacultadAsociadaACuenta=false;
         else
           FacultadAsociadaACuenta=true;
	   }
      else
       {
         //Buscala en la matriz de facultades asociadas a cuentas con signo +
         if(facultadAsociada(FacArchivo,credito,facultad,"+"))
           FacultadAsociadaACuenta=true;
         else
		   FacultadAsociadaACuenta=false;
       }
	  EIGlobal.mensajePorTrace("MCL_PosMov - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
      return FacultadAsociadaACuenta;
     }
}