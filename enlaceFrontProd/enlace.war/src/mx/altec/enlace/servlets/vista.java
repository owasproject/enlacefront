/*Banco Santander Mexicano
   Clase vista Contiene las funciones para la presentacion y ejecucion de operaciones vista
   @Autor:Paulina Ventura Agustin
   @version: 1.0
   fecha de creacion:
   responsable: Roberto Resendiz
   modificacion:Paulina Ventura Agustin 03/03/2001 - Quitar codigo muerto
                                        11/04/2001 - Quitar tabla de agregar operaciones.
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.servicios.ValidaCuentaContrato;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;


public class vista extends BaseServlet {

     public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
 		defaultAction( request, response );
 	}

 	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
 		defaultAction( request, response );
 	}

 	public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {

 	  boolean sesionvalida = SesionValida( request, response );
 	  if(!sesionvalida) {
		  return;
	  }

       EIGlobal.mensajePorTrace("***vista.class Entrando a execute ", EIGlobal.NivelLog.INFO);

      HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  if(session!=null) {

 		 if(session.getStrMenu()!=null)
 			  request.setAttribute("MenuPrincipal", session.getStrMenu());
 		 else
  			  request.setAttribute("MenuPrincipal", session.getStrMenu());
 		 if(session.getFuncionesDeMenu()!=null)
 		      request.setAttribute("newMenu",       session.getFuncionesDeMenu());
 		 else
 		      request.setAttribute("newMenu",       "");
 		 request.setAttribute("Encabezado",    CreaEncabezado("Inversi&oacute;n Vista", "Tesorer&iacute;a &gt; Inversiones &gt; Vista",request));
 	  }


       if (session.getFacultad(session.FAC_DISPONIBLE_INVERSION))
	   {

         String funcion_llamar=request.getParameter("ventana");

         EIGlobal.mensajePorTrace("***vista.class ventana:"+funcion_llamar, EIGlobal.NivelLog.INFO);

         if (funcion_llamar.equals("0"))
           pantalla_inicial_vista( request, response );
         else if (funcion_llamar.equals("1"))
           pantalla_cuenta_vista(request,response);
         else if (funcion_llamar.equals("2"))
           ejecucion_operaciones_vista( request, response );
       }
	   else if(sesionvalida)
	     despliegaPaginaError("Lo sentimos, la p&aacute;gina que usted solicit&oacute; no est&aacute; disponible ya que usted no tiene privilegios de acceso a este servicio","Vista","Vista","s26130h", request, response );


     }




     public void pantalla_inicial_vista( HttpServletRequest request, HttpServletResponse response )
		 throws IOException, ServletException
	 {
		EIGlobal.mensajePorTrace("***Vista.class Entrando a pantalla_inicial_vista ", EIGlobal.NivelLog.INFO);
        boolean fac_programadas;
        String campos_fecha   = "";

  		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

        boolean cadena_getFacCOpProg=session.getFacultad(session.FAC_PROGRAMA_OP);

        fac_programadas = cadena_getFacCOpProg;
        String fecha_hoy   = "";

        fecha_hoy=ObtenFecha();
        campos_fecha=poner_campos_fecha(fac_programadas);

        request.setAttribute("arregloctas_destino1","");
        request.setAttribute("arregloimportes1","");
        request.setAttribute("arreglofechas1","");
        request.setAttribute("arregloopciones1","");
        request.setAttribute("arregloestatus1","");
        request.setAttribute("arregloctas_destino2","");
        request.setAttribute("arregloimportes2","");
        request.setAttribute("arreglofechas2","");
        request.setAttribute("arregloopciones2","");
        request.setAttribute("arregloestatus2","");
        request.setAttribute("contador1","0");
        request.setAttribute("contador2","0");
        request.setAttribute("tabla","");
        request.setAttribute("fecha_hoy",fecha_hoy);
        //request.setAttribute("ContUser",ObtenContUser());
        request.setAttribute("campos_fecha",campos_fecha);


        if (fac_programadas==false)
          request.setAttribute("fac_programadas1","0");
        else
          request.setAttribute("fac_programadas1","1");


 		request.setAttribute("diasInhabiles",diasInhabilesDesp());
         //request.setAttribute("MenuPrincipal", session.getstrMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26130h",request) );
        request.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);

 		EIGlobal.mensajePorTrace("***Vista.class web_application "+Global.WEB_APPLICATION, EIGlobal.NivelLog.INFO);

		session.setModuloConsultar(IEnlace.MVista);


		//TODO CU3021 Inicio de flujo
		/**
		 * VSWF- FVC - I
		 * 17/eNERO/2007 MODIFICACION
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		try{
		BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
		BitaHelperImpl bh = new BitaHelperImpl(request, session, sess);

		BitaTransacBean bt = new BitaTransacBean();

		if(request.getParameter(BitaConstants.FLUJO) != null)
	 			bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
	 		else
	 			bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));

		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ER_INVERSIONES_VISTA_ENTRA);

			bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(Exception e){
			e.printStackTrace();
		}
		}
		/**
		 * VSWF - FVC - F
		 */
	    evalTemplate(IEnlace.MIS_OPERVISTA_TMPL, request, response );


     }


      public void pantalla_cuenta_vista( HttpServletRequest request, HttpServletResponse response )
		 throws IOException, ServletException
	 {
		EIGlobal.mensajePorTrace("***Vista.class Entrando a pantalla_cuenta_vista ", EIGlobal.NivelLog.INFO);
		String cuenta= request.getParameter ("cuenta");
		String tipo=   request.getParameter ("tipo");
		String descrip= request.getParameter ("descrip");

		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

        boolean fac_programadas;
        String campos_fecha   = "";
        boolean cadena_getFacCOpProg=session.getFacultad(session.FAC_PROGRAMA_OP);

        fac_programadas = cadena_getFacCOpProg;
        String fecha_hoy   = "";

        fecha_hoy=ObtenFecha();

        campos_fecha=poner_campos_fecha(fac_programadas);


         request.setAttribute("arregloctas_destino1","");
         request.setAttribute("arregloimportes1","");
         request.setAttribute("arreglofechas1","");
         request.setAttribute("arregloopciones1","");
         request.setAttribute("arregloestatus1","");
         request.setAttribute("arregloctas_destino2","");
         request.setAttribute("arregloimportes2","");
         request.setAttribute("arreglofechas2","");
         request.setAttribute("arregloopciones2","");
         request.setAttribute("arregloestatus2","");
         request.setAttribute("contador1","0");
         request.setAttribute("contador2","0");
         request.setAttribute("tabla","");
         request.setAttribute("fecha_hoy",fecha_hoy);
         //request.setAttribute("ContUser",ObtenContUser());
         request.setAttribute("campos_fecha",campos_fecha);


         if (fac_programadas==false)
          request.setAttribute("fac_programadas1","0");
         else
          request.setAttribute("fac_programadas1","1");


 		request.setAttribute("diasInhabiles",diasInhabilesDesp());
         //request.setAttribute("MenuPrincipal", session.getstrMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26130h",request) );
        request.setAttribute("textdestino","\""+cuenta+" "+descrip+"\"");
		request.setAttribute("destino","\""+cuenta+"|"+tipo+"|"+descrip+"|"+"\"");
        request.setAttribute("WEB_APPLICATION",Global.WEB_APPLICATION);

		request.setAttribute("producto",obten_producto(cuenta,request));
        request.setAttribute("subproducto",obten_subproducto(cuenta));


		session.setModuloConsultar(IEnlace.MVista);
	    evalTemplate(IEnlace.MIS_OPERVISTA_TMPL, request, response );


     }





     public void ejecucion_operaciones_vista( HttpServletRequest request, HttpServletResponse response )	throws IOException, ServletException
	 {

 		EIGlobal.mensajePorTrace("***vista.class Entrando a ejecucion_operaciones_vista ", EIGlobal.NivelLog.INFO);

         String mensaje_salida           = "";
         String coderror                 = "";
         String clave_perfil             = "";
         String contrato                 = "";
         String tipo_operacion           = "";
         String usuario                  = "";
         String cta_destino              = "";
         String importe_string           = "";
         double importe                  = 0.0;
         String opcion                   = "";
         String sfecha_programada        = "";
         String bandera_fecha_programada = "1";
         String fecha_programada_b       = "";
         int salida                      = 0;
         boolean programada              = false;
         String tipo_cta_destino         = "";
         int indice                      = 0;
         String fecha                    = "";
         String bandera_estatus          = "";
         String fecha_hoy                = "";
         String cuenta1                  = "";
         String cuenta2                  = "";
         String tipo_cuenta              = "";
         boolean dentro_horario          = true;
         String  cadena_cta_destino      = "";
 		String trama_entrada            = "";
 		String tramaok                  = "";
 		//TuxedoGlobal tuxGlobal          = ( TuxedoGlobal ) session.getEjbTuxedo();
		String producto                  ="";
		String subproducto               ="";

		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		ServicioTux tuxGlobal = new ServicioTux();
 		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

 		Hashtable htResult              = null;
		ValidaCuentaContrato valCtas = new ValidaCuentaContrato();
		String [] datosCta = new String[8];

         String[] arrCuentas=new String[3]; //Numero de cuentas  X 2 (cuenta|tipo|)

         cadena_cta_destino = request.getParameter ("destino");
         importe_string     = request.getParameter ("importe");
         opcion             = request.getParameter ("opcion");
      	 fecha              = request.getParameter ("fecha");
		 producto           = request.getParameter ("producto");
		 subproducto        = request.getParameter ("subproducto");
         fecha_hoy=ObtenFecha();

		EIGlobal.mensajePorTrace("***vista.class  Entrando a cadena_cta_destino:"+cadena_cta_destino, EIGlobal.NivelLog.INFO);
     	EIGlobal.mensajePorTrace("***vista.class  Entrando a importe:"+importe, EIGlobal.NivelLog.INFO);
     	EIGlobal.mensajePorTrace("***vista.class  Entrando a opcion:"+opcion, EIGlobal.NivelLog.INFO);
     	EIGlobal.mensajePorTrace("***vista.class  Entrando a fecha:"+fecha, EIGlobal.NivelLog.INFO);

 		contrato=session.getContractNumber();
         usuario=session.getUserID8();
         clave_perfil=session.getUserProfile();

		 mensaje_salida="<table width=760 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
         mensaje_salida=mensaje_salida+"<TR>";
  		 mensaje_salida=mensaje_salida+"<td width=100 class=tittabdat align=center>Tipo de operaci&oacute;n</td>";
         mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=162>Cuenta</td>";
         mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=100>Importe</td>";
		  mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=100>Tasa</td>";
         mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=100>Fecha de aplicaci&oacute;n</td>";
         mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=70>Estatus</td>";
         mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=70>Referencia</td>";
         //mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=100>Saldo Actualizado</td>";
         mensaje_salida=mensaje_salida+"</TR>";

         boolean  par=false;

         arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');
         cta_destino = arrCuentas[1];
         tipo_cta_destino=arrCuentas[2];
         importe =  new Double (importe_string).doubleValue();
         sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());
 		programada=checar_si_es_programada(fecha);
        	tipo_cuenta=cta_destino.substring(0, 2);

         if (opcion.equals("0"))
         {
            tipo_operacion="CPDI";
            if (tipo_cuenta.equals("60"))
            {
                cuenta1=cta_destino;
                cuenta2="61"+cta_destino.substring(2,cta_destino.length());
            }

            if (tipo_cuenta.equals("65"))
            {
                cuenta1=cta_destino;
                cuenta2="66"+cta_destino.substring(2,cta_destino.length());
            }
            dentro_horario=checar_hora(20);
          }
          else
          {
             tipo_operacion="CPID";
             if (tipo_cuenta.equals("60"))
             {
                cuenta2=cta_destino;
                cuenta1="61"+cta_destino.substring(2,cta_destino.length());
             }

             if (tipo_cuenta.equals("65"))
             {
                cuenta2=cta_destino;
                cuenta1="66"+cta_destino.substring(2,cta_destino.length());
             }
             dentro_horario=checar_hora(17);
           }

           if (programada==false)
              trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_destino+"|"+tipo_cta_destino+"|"
                                +importe_string+"| |";
           else
              trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+
                               cta_destino+"|"+tipo_cta_destino+"|"+importe_string+"| |";

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


           EIGlobal.mensajePorTrace(fechaHr+"***vista WEB_RED-VISTA>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);

 			try
			{
				datosCta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																						 session.getUserID8(),
																						 session.getUserProfile(),
																						 cta_destino,
																						 IEnlace.MVista);
				if (datosCta == null)
				{
					valCtas.closeTransaction();
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26140h",request));
					request.setAttribute("Mensaje", "Error H701: Cuenta inv&aacute;lida.");
                    evalTemplate( "/jsp/EI_Mensaje2.jsp", request, response);
				}else{
 					htResult = tuxGlobal.web_red( trama_entrada );
				}
 			} catch ( java.rmi.RemoteException re ) {
 				re.printStackTrace();
 			} catch (Exception e) {}
			finally{
				try{
					valCtas.closeTransaction();
				}catch (Exception e){
					EIGlobal.mensajePorTrace("Error al cerrar la conexion a MQ", EIGlobal.NivelLog.ERROR);
				}
			}
 		   coderror = ( String ) htResult.get( "BUFFER" );
 		   EIGlobal.mensajePorTrace("***vista<<"+coderror, EIGlobal.NivelLog.DEBUG);

 		   //para prueba...
           //coderror="OK12345678791123456789123456777887877887";
		   if (coderror!=null)
		   {
		       String  sreferencia="";
				if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC"))))
					sreferencia=coderror.substring(8,16);
				sreferencia=sreferencia.trim();
				mensaje_salida=mensaje_salida+"<TR>";
  				if (opcion.equals("0"))
                  mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left nowrap>Cheque-Vista</TD>";
				else
                  mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left nowrap>Vista-Cheque</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+cta_destino+"</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>";
				//mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+obten_tasa(importe,producto,subproducto,request)+"%</TD>";
				//Cambio pendiente para eliminar tasasrvr
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+obten_tasa_BD07(importe,producto,subproducto,request)+"%</TD>";

				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";

				if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
  		  			mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n Mancomunada</TD>";
 				else if ((coderror.substring(0,2).equals("OK"))&&(programada==true))
					mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n programada</TD>";
				else if ((coderror.substring(0,2).equals("OK"))&&(programada==false))
					mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Tranferencia aceptada</TD>";
				else
				{
					if (programada==true)
						mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n NO programada </TD>";
					else
					{
						if ((coderror.length()>16)&&(!(coderror.substring(0,4).equals("MANC"))))
							mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(16,coderror.length()) +"</TD>";
						else if (coderror.substring(0,4).equals("MANC"))
							mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(8,coderror.length())+"</TD>";
						else
							mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
					}//else
 				}//else

 				String tipo="";

 				if (coderror.substring(0,2).equals("OK"))
				{
					if (programada==false)
					{
						if(!((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC"))))
						{
 							if (tipo_operacion.equals("CPDI"))
 								tipo="3";
 							else
								tipo="4";

					  //if (tipo_operacion.equals("CPDI"))

                      //  mensaje_salida=mensaje_salida+"<TD><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceMig.comprobante_trans?tipo=2&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cuenta1+"&cta_destino="+cuenta2+"&enlinea=s\">"+sreferencia+"</A></TD>";

                      //else

                      //  mensaje_salida=mensaje_salida+"<TD><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceMig.comprobante_trans?tipo=3&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cuenta1+"&cta_destino="+cuenta2+"&enlinea=s\">"+sreferencia+"</A></TD>";


							tramaok+="1|"+tipo+"|"+sreferencia.trim()+"|"+importe_string+"|"+cuenta1+"|"+cuenta2+"| @";
 							mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value, 1);\" >"+sreferencia+"</A></TD>";

 							/*if (coderror.length()>=30)
 								mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+ FormatoMoneda(coderror.substring(16,30))+"</TD>";
 							else
 								mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";   */
                           //mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+Obten_saldo_actual(cta_destino,tipo_cta_destino)+"</td>";


 						}
						else
 						{
							mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
 							//mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
						}
 					}
					else
 					{
 						mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
						//mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
 					}
				}
				else if (coderror.substring(0,4).equals("MANC"))
 				{
					mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
 					//mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
				}
 				else
 				{
					mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
 					//mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
				}
		   }
		   else
		   {
  				if (opcion.equals("0"))
                  mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left nowrap>Cheque-Vista</TD>";
				else
                  mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left nowrap>Vista-Cheque</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+cta_destino+"</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>-------</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
			}


				mensaje_salida=mensaje_salida+"</TABLE>";
				mensaje_salida+="<table width=760 border=0 cellspacing=2 cellpadding=0><tr><td valign=top align=left class=textabref width=372>Si desea obtener su comprobante, d&eacute; click en el n&uacute;mero de referencia subrayado.</td></tr></table>";
				request.setAttribute("mensaje_salida",mensaje_salida);
				request.setAttribute("fecha_hoy",fecha_hoy);
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				if (opcion.equals("0"))
				   request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26140h",request));
				else
   				  request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26140bh",request));

				request.setAttribute("tramaok","\""+tramaok+"\"");
				request.setAttribute("contadorok","1");
 				request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
				request.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

  				request.setAttribute("banco",session.getClaveBanco());
				//System.out.println("banco "+session.getClaveBanco());
                String banco=session.getClaveBanco();
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//HttpSession ses = request.getSession();
		        sess.setAttribute("banco",session.getClaveBanco());
				sess.setAttribute("mensaje_salida",mensaje_salida);
				sess.setAttribute("fecha_hoy",fecha_hoy);
				sess.setAttribute("MenuPrincipal", session.getStrMenu());
				sess.setAttribute("newMenu", session.getFuncionesDeMenu());
				if (opcion.equals("0"))
				  sess.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26140h",request));
				else
				  sess.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n Vista","Tesorer&iacute;a &gt; Inversiones &gt; Vista","s26140bh",request));


				sess.setAttribute("tramaok","\""+tramaok+"\"");
				sess.setAttribute("contadorok","1");
 				sess.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
				sess.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
				sess.setAttribute("web_application",Global.WEB_APPLICATION);

				//TODO CU3021 realiza operaciones
				/**
				 * VSWF - FVC - I
				 * 17/eNERO/2007	MODIFICACION
				 */
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
				try{
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelperImpl bh = new BitaHelperImpl(request, session, sess);
				BitaTransacBean bt = new BitaTransacBean();

				bt = (BitaTransacBean)bh.llenarBean(bt);
				if(opcion.equals("0")){
					bt.setNumBit(BitaConstants.ER_INVERSIONES_VISTA_OPER_REDSRV_CPDI);//cheques-vista
					bt.setServTransTux("CPDI");
					/*BMB-I*/
					if(coderror!=null){
		    			if(coderror.substring(0,2).equals("OK")){
		    				bt.setIdErr("CPDI0000");
		    			}else if(coderror.length()>8){
			    			bt.setIdErr(coderror.substring(0,8));
			    		}else{
			    			bt.setIdErr(coderror.trim());
			    		}
		    		}
					/*BMB-F*/
				}
				else{
					bt.setNumBit(BitaConstants.ER_INVERSIONES_VISTA_OPER_REDSRV_CPID);//vista-cheques
					bt.setServTransTux("CPID");
					/*BMB-I*/
					if(coderror!=null){
		    			if(coderror.substring(0,2).equals("OK")){
		    				bt.setIdErr("CPID0000");
		    			}else if(coderror.length()>8){
			    			bt.setIdErr(coderror.substring(0,8));
			    		}else{
			    			bt.setIdErr(coderror.trim());
			    		}
		    		}
					/*BMB-F*/
				}
				bt.setCctaOrig((cta_destino == null)?" ":cta_destino);
				if(programada)
					bt.setFechaProgramada((sfecha_programada == null)?new Date():BitaHelperImpl.MdyToDate(sfecha_programada));

				if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC")))){
					bt.setReferencia(Long.parseLong(coderror.substring(8,16).trim()));
				}
				bt.setImporte(importe);
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());


					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(Exception e){
					e.printStackTrace();
				}
				}
				/**
				 * VSWF - FVC - F
				 */

				//<vswf:meg cambio de NASApp por Enlace 08122008>
				response.sendRedirect(response.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL));

    } // metodo


	//Funcion para checar el horario para las diferentes operaciones vista

    //CPDI limite 20 hrs

    //CPID limite 17 hrs

     boolean checar_hora(int hora_fin)
     {

        String hora=ObtenHora();
        String solo_hora=hora.substring(0,hora.indexOf(":"));
        int    ihora=Integer.parseInt(solo_hora);

		if (ihora<hora_fin)
          return true;
        else return false;
     }

	 public String obten_producto(String cta,HttpServletRequest request)
	 {
        String producto="01";
        try
		{
		  producto= consultaClaves(cta,request);
		  if(producto.length()==0)
            producto="01";
        }
		catch(Exception e)
		{
   		    EIGlobal.mensajePorTrace("***vista.class "+e.toString(), EIGlobal.NivelLog.INFO);

        }
		return producto;
	 }
	 public String obten_subproducto(String cta)
	 {
        String subproducto;
		if(cta.substring(0,2).equals("60"))
            subproducto="61";
		else
			subproducto="66";

		return subproducto;
     }



	public String consultaClaves(String cuenta,HttpServletRequest request)throws ServletException, IOException
	{
		    EIGlobal.mensajePorTrace("***vista.class consultaClaves", EIGlobal.NivelLog.INFO);

		    HttpSession sess = request.getSession();
            BaseResource session = (BaseResource) sess.getAttribute("session");

			ServicioTux tuxGlobal = new ServicioTux();
			//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			String tipo_cuenta			= "";
			String fileOut				= "";
			String tramaEntrada			= "";
			String tramaSalida			= "";
		    String path_archivo			= "";
			String nombre_archivo		= "";
			String registro				= "";
			String claveCuen			= "";
			String producto="";
			BufferedReader fileSua      =  null;

			fileOut = session.getUserID8() + ".tmp";
			EI_Exportar ArcSal;
			try
			{
			  ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			  ArcSal.creaArchivo();
			  ArcSal.escribeLinea(cuenta+ "\n" );			//+ "\r\n"
			  ArcSal.cierraArchivo();
			  ArchivoRemoto archR = new ArchivoRemoto();
			  if(!archR.copiaLocalARemoto(fileOut))
		      {
			    EIGlobal.mensajePorTrace("***vista.class No se pudo crear archivo para exportar claves", EIGlobal.NivelLog.INFO);
			    return "";
			  }
			}
			catch(Exception ioeSua )
			{
		       EIGlobal.mensajePorTrace( "***vista.class Excepcion %consultaClaves() >> "+ ioeSua.getMessage() +  " <<", EIGlobal.NivelLog.INFO);
			   return "";
            }

			tramaEntrada = "2EWEB|" + session.getUserID8() + "|ECBE|" + session.getContractNumber() + "|" + session.getUserID8() +
						   "|" + session.getUserProfile() + "|";

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


		    EIGlobal.mensajePorTrace(fechaHr+"***vista.class >> tecb :" + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);
			try
			{
				Hashtable hs = tuxGlobal.web_red(tramaEntrada);
      			tramaSalida = (String) hs.get("BUFFER");
	  			EIGlobal.mensajePorTrace( "***vista.class trama_salida >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);
			}
			catch(java.rmi.RemoteException re )
			{
	  			re.printStackTrace();
				return "";
			}
			catch (Exception e) {return "";}

	        boolean errorFile  = false;
            try
            {
		      path_archivo = tramaSalida;
              nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());

			  EIGlobal.mensajePorTrace( "***vista.class & nombre_archivo_cb:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);

			  // Copia Remota
			  ArchivoRemoto archR = new ArchivoRemoto();
			  if(!archR.copia(nombre_archivo))
			  {
				EIGlobal.mensajePorTrace( "***vista.class & consultaClaves: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);
			  }
			  else
			  {
				EIGlobal.mensajePorTrace( "***vista.class & consultaClaves: Copia remota OK. &", EIGlobal.NivelLog.INFO);
			  }

		      path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo; // +"0";

			  EIGlobal.mensajePorTrace( "***vista.class & path_archivo_cb:" + path_archivo + " &", EIGlobal.NivelLog.INFO);

         /*     File drvSua = new File(path_archivo);
              RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");*/

  		      fileSua =new BufferedReader( new FileReader(path_archivo) );


			  int indice = 1;
              while((registro = fileSua.readLine() ) != null)
			  {
			    String cadAux = registro.substring(0, 6).trim();
			    String elError= registro.substring(16, registro.trim().length());
		      	EIGlobal.mensajePorTrace( "***vista.class & consultaClaves: cadAux. & "+ cadAux, EIGlobal.NivelLog.INFO);
				if (! ( cadAux.equals("CORR00") || cadAux.equals("ERROR3") )  )
  			    {
			      claveCuen   = EIGlobal.BuscarToken(registro, '|', 3);
				}
				else
                  claveCuen   = "";
			  } // del while
	  		  //fileSua.close();
         }
		 catch(Exception ioeSua )
         {
			  EIGlobal.mensajePorTrace( "***vista.class Excepcion %consultaClaves() >> "+ ioeSua.getMessage() +  " <<", EIGlobal.NivelLog.INFO);
			  return "";
         }
		 finally
		 {
              try
			  {
				  fileSua.close();
			  }
			  catch(Exception e)
			  {
				   e.printStackTrace();
              }
         }
       	 EIGlobal.mensajePorTrace( "***vista.class claveCuen >> "+claveCuen +  " <<", EIGlobal.NivelLog.INFO);

		 if(claveCuen.substring(0,4).indexOf(Global.CLAVE_SANTANDER)>=0)
            producto="01";
		 else if(claveCuen.substring(0,4).indexOf(Global.CLAVE_SERFIN)>=0)
            producto="09";

         return producto;


   }

   public double obten_tasa(double importe,String producto, String subproducto,HttpServletRequest req)
   {
      EIGlobal.mensajePorTrace("***vista obten_tasa ", EIGlobal.NivelLog.DEBUG);
      EIGlobal.mensajePorTrace("***vista importe "+importe, EIGlobal.NivelLog.DEBUG);
      EIGlobal.mensajePorTrace("***vista producto "+producto, EIGlobal.NivelLog.DEBUG);
      EIGlobal.mensajePorTrace("***vista subproducto "+subproducto, EIGlobal.NivelLog.DEBUG);

      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");


	  double tasaplicar=0.0;
	  double rangomenor=0.0;
	  double rangomayor=0.0;

  	  if(subproducto.length()==4)
		subproducto=subproducto.substring(2, 4);

	  String tipo_persona="";
	  if (subproducto.equals("61")) //este es ek subproducto para cuentas tipo 60
			tipo_persona="F";
	  else
			tipo_persona="J";
     EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
      BufferedReader in=null;

	  try
      {
            EnlaceGlobal.llamada_consulta_tasas_plazos_por_servicio(subproducto+"|"+producto+"|");

		   String linea="";
           String datos[]=null;
           in=new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR+ "/tasas.dat" ));

           while ((linea=in.readLine())!=null)
           {
              //sprintf( linea,"%s|%s|%s|%f|%f|%f|",fecha_aux,element->instrumento,element->persona_juridica,element->plazo,element->rango_menor,element->rango_mayor,element->tasa);
              datos = desentramaC("7|"+linea,'|');
              if(datos!=null)
              {
                 if ((datos[3].equals(tipo_persona))&&
                     (datos[2].substring(0,1).equals("-")) )
				 {
                     rangomenor=new Double(datos[5]).doubleValue();
					 rangomayor=new Double(datos[6]).doubleValue();
                     if ((importe>=rangomenor)&&(importe<=rangomayor))
				       tasaplicar=new Double(datos[7]).doubleValue();
                  }
              }
           }
      }
      catch(IOException e)
      {
              EIGlobal.mensajePorTrace("***vista.class: IOException"+e, EIGlobal.NivelLog.ERROR);
      }
      catch(Exception e)
      {
              EIGlobal.mensajePorTrace("***vista de otro tipo "+e, EIGlobal.NivelLog.ERROR);
      }
	  finally
		 {
              try
			  {
				  in.close();
			  }
			  catch(Exception e)
			  {
				   e.printStackTrace();
              }
         }

       EIGlobal.mensajePorTrace("***vista "+tasaplicar, EIGlobal.NivelLog.ERROR);
       return tasaplicar;
   }


   double Obten_saldo_actual(String cuenta, String tipo,HttpServletRequest request)
   {

	  HttpSession sess = request.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  String tramaentrada = "1EWEB|" + session.getUserID8() + "|SDCT|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
      String tramaentrada2 = "";
      String coderror     = "";
	  double totalSaldos  =0.0;
	  Hashtable htResult;

      try
	  {
			ServicioTux tuxGlobal = new ServicioTux();
			//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			tramaentrada2 = tramaentrada + cuenta + "|" + tipo + "|";
		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


		 EIGlobal.mensajePorTrace(fechaHr+"vista - Trama a enviar"+tramaentrada2, EIGlobal.NivelLog.DEBUG);

			htResult = tuxGlobal.web_red( tramaentrada2 );
			coderror = ( String ) htResult.get( "BUFFER" );
			EIGlobal.mensajePorTrace("***vista.class: "+coderror, EIGlobal.NivelLog.ERROR);
            if (coderror.substring(0,2).equals("OK"))
			{
				try
				{
					totalSaldos += new Double(coderror.substring(44,58)).doubleValue();
                }
				catch(NumberFormatException e)
				{
					EIGlobal.mensajePorTrace("***vista.class: "+e, EIGlobal.NivelLog.ERROR);
				}
				catch(Exception e)
                {
					EIGlobal.mensajePorTrace("***vista.class: "+e, EIGlobal.NivelLog.ERROR);
				}
			}
      }
      catch( Exception e )
      {
	    EIGlobal.mensajePorTrace("***vista.class: "+e, EIGlobal.NivelLog.ERROR);
      }
	  return totalSaldos;
   }





public double obten_tasa_BD07(double importe, String producto, String
subproducto,HttpServletRequest req)
   {
       EIGlobal.mensajePorTrace("**************        VISTA OBTEN_TASA_BD07     **************", EIGlobal.NivelLog.ERROR);
       EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 importe "+importe, EIGlobal.NivelLog.ERROR);
       EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 producto "+producto, EIGlobal.NivelLog.ERROR);
       EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 subproducto "+subproducto, EIGlobal.NivelLog.ERROR);

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource)
        sess.getAttribute("session");


	    EIGlobal EnlaceGlobal = new EIGlobal(session,getServletContext(),this);
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) BAL.getServletContext()).getContext());

        Hashtable htResult = null;
        String CODE_ERROR_OK="TUBO0000";
        String coderror = "";
        String FMLString = "";
        String campo_41204 = "";
        String campo_41205 = "";
        String campo_41206 = "";
        String campo_41265 = "";
        String FMLString_aux = "";
        int continua = 0;
        int cont = 0;

        double tasaplicar = 0.0;
        double rangomenor = 0.0;        double rangomayor = 0.0;

        if(subproducto.length()==4)
            subproducto=subproducto.substring(0, 3);

        String tipo_persona="";

        if (subproducto.trim().equals("61")||subproducto.trim().equals("0061")) //este es el subproducto para cuentas tipo 60
                tipo_persona="F";
        else
                tipo_persona="J";

        if(subproducto.trim().length()==2)
            if(!subproducto.substring(0,1).trim().equals("00"))
                subproducto = "00" + subproducto.trim();

        /********************************************************************************/
        /*                              PRIMER LLAMADO                                  */
        /********************************************************************************/

         try
        {

            htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A", "", "");
            coderror = ( String ) htResult.get("COD_ERROR");
            EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

            if(coderror == null)
                coderror = "";

             if(coderror.trim().equals(CODE_ERROR_OK))
            {
                EIGlobal.mensajePorTrace("*******************************************************", EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace("*****     RESULTADO DEL 1ER LLAMADO AL TUBOCT     *****", EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace("*******************************************************", EIGlobal.NivelLog.ERROR);

                FMLString = ( String ) htResult.get("FMLSTRING");
                //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 FMLString: " + FMLString, EIGlobal.NivelLog.ERROR);
                campo_41204 = ( String ) htResult.get("TIPO_SOC");
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 TIPO_SOC: " + campo_41204, EIGlobal.NivelLog.ERROR);
                campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
                EIGlobal.mensajePorTrace("***VISTA obten_tasa:BD07 PAIS_ORIGEN: " + campo_41205, EIGlobal.NivelLog.ERROR);
                campo_41206 = ( String ) htResult.get("SECTOR");
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07  SECTOR: " + campo_41206, EIGlobal.NivelLog.ERROR);
                campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 CAMPO_AUX2: " + campo_41265, EIGlobal.NivelLog.ERROR);
             }
             else
             {
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
                return -1;
             }
        }
         catch (Exception e)
        {
            EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07  Error en la primera llamada a TUBOCT: "+e.toString(), EIGlobal.NivelLog.DEBUG);
        }

        /********************************************************************************/
        /*                               SIGUIENTES LLAMADOS                            */
        /********************************************************************************/
        if( (campo_41204.trim().equals("") && campo_41206.trim().equals("MXP"))
        || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP")) )
            continua = 1;
        else
            continua = 0;

         while(continua==1&&cont<4)
        {
            FMLString_aux = FMLString_aux + FMLString.substring(0,FMLString.length() - 21);

            coderror = "";

             try
            {
                htResult = tuxGlobal.TUBOCT( producto,subproducto, "BD07A",campo_41206, campo_41265);

                FMLString = "";
                campo_41204 = "";
                campo_41205 = "";
                campo_41206 = "";
                campo_41265 = "";

                coderror = ( String ) htResult.get("COD_ERROR");
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 COD_ERROR: " + coderror, EIGlobal.NivelLog.ERROR);

                if(coderror == null)
                    coderror = "";

                 if(coderror.trim().equals(CODE_ERROR_OK))
                {
                    EIGlobal.mensajePorTrace("*******************************************************", EIGlobal.NivelLog.ERROR);
                    EIGlobal.mensajePorTrace("*****     RESULTADO DEL 2DO LLAMADO AL TUBOCT     *****", EIGlobal.NivelLog.ERROR);
                    EIGlobal.mensajePorTrace("*******************************************************", EIGlobal.NivelLog.ERROR);
                    FMLString = ( String ) htResult.get("FMLSTRING");
                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 FMLString: " + FMLString, EIGlobal.NivelLog.ERROR);
                    campo_41204 = ( String ) htResult.get("TIPO_SOC");
                    EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 TIPO_SOC: " + campo_41204, EIGlobal.NivelLog.ERROR);
                    campo_41205 = ( String ) htResult.get("PAIS_ORIGEN");
                    EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 PAIS_ORIGEN: " + campo_41205, EIGlobal.NivelLog.ERROR);
                    campo_41206 = ( String ) htResult.get("SECTOR");
                    EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 SECTOR: " + campo_41206, EIGlobal.NivelLog.ERROR);                    campo_41265 = ( String ) htResult.get("CAMPO_AUX2");
                    EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 CAMPO_AUX2: " + campo_41265, EIGlobal.NivelLog.ERROR);
                }
                 else
                {
                    EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 Error (COD_ERROR): " + coderror, EIGlobal.NivelLog.DEBUG);
                    return -1;
                }
            }
             catch (Exception e)
            {
                EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 Error en la segunda llamada al TUBOCT: " + e.toString(), EIGlobal.NivelLog.DEBUG);
            }

            if( (campo_41204.trim().equals("") && campo_41206.trim().equals("MXP"))
            || (campo_41204.trim().equals(producto) && campo_41206.trim().equals("MXP")) )
                continua = 1;
            else
                continua = 0;
            cont = cont + 1;
        }

        FMLString_aux = FMLString_aux + FMLString;
        FMLString = FMLString_aux;

        if(FMLString == null)
            FMLString = "";

         if(!FMLString.trim().equals(""))
        {
            //Variables para contar los registros
            int noproductos = 0;
            int nocampos = 0;
            int records_serfin = 11;
            int noregistros = 0;

            for(int i=0; i < FMLString.length();)
            {
                if (FMLString.substring(i, i+1).trim().equals("|"))
                    nocampos++;                            //CONTABILIZA LOS PIPES EN LA RESPUESTA
                if (FMLString.substring(i, i+1).trim().equals("@"))
                    noproductos++;                         //CONTABILIZA LAS ARROBAS (PRODUCTOS) EN LA RESPUESTA
                i++;
            }

            noregistros = nocampos / records_serfin;     //OBTIENE EL NUMERO DE REGISTROS EFECTIVOS

            //Variables para separar los datos
            int arrobas = 0;
            int campos = 0, j = 0;
            String dato = "";
            int posicionador = 0;

            //Variables para guardar los datos
            String sub_prod = "";
            String instrumento = "";
            String personalidad = "";
            String plazo = "";
            String plazo_nuevo = "";
            String plazo_ant = "";
            double imp_min = 0;
            double imp_max = 0;
            double tasa_min = 0;
            double tasa_min_aux = 0;
            double tasa_max = 0;
            double tasa = 0;

             for(int i=0; i < noregistros;i++)
            {
                sub_prod = "";
                instrumento = "";
                personalidad = "";
                plazo = "";
                plazo_nuevo = "";
                plazo_ant = "";
                imp_min  = 0;
                imp_max = 0;
                tasa_min = 0;
                tasa_min_aux = 0;
                tasa_max = 0;
                tasa = 0;
                campos = 0;

                for (j = posicionador;j < FMLString.length();)         //BARRE CARACTER X CARACTER DE LA RESPUESTA
                {
                     if(FMLString.substring(j,j+1).trim().equals("|")||FMLString.substring(j,j+1).trim().equals("@"))
                    {                       // PREGUNTA SI EL CARACTER ES UN PIPE PARA CONTABILIZAR UN CAMPO

                        campos++;
                         switch(campos)
                        {
                            case 1:
                                    //EIGlobal.mensajePorTrace("***************************************************", EIGlobal.NivelLog.ERROR);
                                    sub_prod = dato;
                                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 TIPO_PERSONA: " + tipo_persona, EIGlobal.NivelLog.ERROR);
                                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 SUBPRODUCTO: " + sub_prod, EIGlobal.NivelLog.ERROR);
                                    break;
                            case 4:
									imp_min = new Double(dato).doubleValue();
                                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 IMP_MIN: " + imp_min, EIGlobal.NivelLog.ERROR);
									break;
                            case 5:
									imp_max = new Double(dato).doubleValue();
									//EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 IMP_MAX: " + imp_max, EIGlobal.NivelLog.ERROR);
                                    break;
                            case 6:
									tasa_min = new Double(dato).doubleValue();
									//EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 TASA_MIN: " + tasa_min, EIGlobal.NivelLog.ERROR);
                                    tasa_min_aux = new Double(dato).doubleValue();
									break;
                            case 7:
									tasa_max = new Double(dato).doubleValue();
									//EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 TASA_MAX: " + tasa_max, EIGlobal.NivelLog.ERROR);
                                    break;
                            case 8:
                                    personalidad = dato;
                                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 PERSONALIDAD: " + personalidad, EIGlobal.NivelLog.ERROR);
                                    break;
                            case 9:
                                    plazo = dato;
                                    //EIGlobal.mensajePorTrace("***VISTA obten_tasa_BD07 PLAZO: " + plazo, EIGlobal.NivelLog.ERROR);
                                    break;
                        }
                        dato = "";  // CORTA LA CADENA PARA Q UNICAMENTE CONTENGA EL DATO

                        if (FMLString.substring(j,j+1).trim().equals("@"))
                        {
                            arrobas++;
                            posicionador = j+2;
                            //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
                            break;
                        }
                    }
                     else
                    {
                        dato = dato +
                        FMLString.substring(j,j+1);                       //ASIGNA UN CARACTER A LA VARIABLE, MIENTRAS NO SEA UN PIPE
                    }

                     if(campos == records_serfin)
                    {
                        posicionador = j+1;                                 //MUEVE EL PUNTERO AL SIGUIENTE CARACTER, QUE SE TRATA DE UN NUEVO REGISTRO
                        break;
                    }

                    j++;
                } //Fin FOR (para obtener campos * registro)


                if (tasa_min_aux < 0.0001)
                    tasa_min_aux = 0.0;


                 switch(Integer.parseInt(sub_prod))
                {
                    case 21:
                        instrumento = "6";
                        break;
                    case 43:
                        instrumento = "5";
                        break;
                    case 47:
                        instrumento = "5";
                        break;
                    case 44:
                        instrumento = "14";
                        break;
                    case 42:
                        instrumento = "16";
                        break;
                    case 15:
                        instrumento = "15";
                        break;
                    case 52:
                        instrumento = "7";
                        break;
                    case 53:
                        instrumento = "8";
                        break;
                    case 54:
                        instrumento = "8";
                        break;
                    case 55:
                        instrumento = "9";
                        break;
                    case 57:
                        instrumento = "6";
                        break;
                    case 40:
                        instrumento = "17";
                        break;
                    case 60:
                        instrumento = "1";
                        break;
                    case 61:
                        instrumento = "2";
                        plazo = "V";
                        break;
                    case 65:
                        instrumento = "1";
                        break;
                    case 66:
                        instrumento = "2";
                        plazo = "V";
                        break;
                    case 69:
                        instrumento = "13";
                        break;
                    case 70:
                        instrumento = "3";
                        break;
                    case 79:
                        instrumento = "11";
                        break;
                    case 62:
                        instrumento = "4";
                        break;
                    case 67:
                        instrumento = "4";
                        break;
                    case 82:
                        instrumento = "18";
                        break;
                    case 83:
                        instrumento = "19";
                        break;
                    case 64:
                        instrumento = "12";
                        break;
                }

                // Valida si el subproducto es diferente de 44
                // Tasa maxima es CERO
                 if (!sub_prod.trim().equals("44"))
                {
                    tasa_min = tasa_min_aux;
                    tasa_max = 0;
                }

                // Valida si el plazo es mayor a 366 dias,
                // Dividir la tasa /2
                if(!plazo.trim().equals("V"))
                    if (Integer.parseInt(plazo) >= 366)
                        tasa_min = tasa_min/2;

                if (!sub_prod.trim().equals("44"))
                    tasa_max = tasa_min_aux;  // todos los productos excepto '34'


                 if ( imp_max > 0.0 && imp_min >= 0.0 )
                {
                     if( Integer.parseInt(sub_prod) == 44 || Integer.parseInt(sub_prod) == 69 || Integer.parseInt(sub_prod) == 70 || Integer.parseInt(sub_prod) == 62 ||Integer.parseInt(sub_prod) == 67 )
                    {

                        plazo_nuevo = plazo;
                        plazo_ant = plazo;

                        if(!plazo.trim().equals("V"))
                         switch(Integer.parseInt(plazo_nuevo))
                        {
                            case 1:   plazo_nuevo = "4";
                                      break;
                            case 5:   plazo_nuevo = "13";
                                      break;
                            case 14:  plazo_nuevo = "23";
                                      break;
                            case 24:  plazo_nuevo = "39";
                                      break;
                            case 40:  plazo_nuevo = "75";
                                      break;
                            case 76:  plazo_nuevo = "134";
                                      break;
                            case 135: plazo_nuevo = "249";
                                      break;
                            case 250: plazo_nuevo = "365";
                                      break;
                            case 366: plazo_nuevo = "371";
                                      break;
                        };

                        //Si es tasa neta
                        plazo = plazo_nuevo;
                        tasa = tasa_min;

                        //Si es tasa bruta
                        instrumento = "-" + instrumento;
                        plazo = plazo_ant;
                        tasa = tasa_max;
                    }
                    else
                    {
                        //Si es tasa neta
                        tasa = tasa_min;

                        //Si es tasa bruta
                        instrumento = "-" + instrumento;
                        tasa = tasa_max;
                    }

                    if (personalidad.trim().equals(tipo_persona) && instrumento.trim().substring(0,1).trim().equals("-"))
                    {
                        rangomenor= imp_min;
                        rangomayor= imp_max;
                        if ((importe>=rangomenor)&&(importe<=rangomayor))
                            tasaplicar= new Double(tasa).doubleValue();
                    }
                }
            }//Fin FOR (para conteo registros)
       }

       return tasaplicar;
    }



}