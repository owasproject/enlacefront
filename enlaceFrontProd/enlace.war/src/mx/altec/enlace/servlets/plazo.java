/*Banco Santander Mexicano
  Clase plazo  Contiene las funciones para la presentacion de las pantallas de plazo y la ejecucion de ellos
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 28/02/2001 - Quitar codigo muerto
 */

package mx.altec.enlace.servlets;

import java.util.*;

import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.io.IOException;
import java.sql.*;


public class plazo extends BaseServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}


	public void doPost( HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException {
		defaultAction( request, response );
	}


	public void defaultAction( HttpServletRequest request, HttpServletResponse response )throws IOException, ServletException {


		boolean sesionvalida = SesionValida( request, response );
		EIGlobal.mensajePorTrace("***plazo.class Entrando a execute ", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");



		//request.setAttribute("MenuPrincipal", session.getstrMenu());
		//request.setAttribute("Fecha", ObtenFecha());
		if (session!=null){
		    if (session.getStrMenu()!=null)
           	    request.setAttribute("MenuPrincipal", session.getStrMenu());
            else
			    request.setAttribute("MenuPrincipal", "");
            if(session.getFuncionesDeMenu()!=null)
		       request.setAttribute("newMenu", session.getFuncionesDeMenu());
			else
			    request.setAttribute("newMenu",       "");
		    request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Operaciones Plazo",request));
          }

//        request.setAttribute("MenuPrincipal", session.getStrMenu());
//        request.setAttribute("newMenu", session.getFuncionesDeMenu());
//        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Operaciones Plazo"));


		//if(sesionvalida&&session.getFacPlazo(request).trim().length()!=0)
		if(sesionvalida&&session.getFacultad("FacPlazo") )
			{
        String funcion_llamar=request.getParameter("ventana");
        EIGlobal.mensajePorTrace("***plazo.class funcion_llamar "+funcion_llamar, EIGlobal.NivelLog.INFO);
        if (funcion_llamar.equals("0"))
          pantalla_inicial_plazo( request, response );
        //else if (funcion_llamar.equals("1"))

        //  pantalla_tabla_plazo();
        else if (funcion_llamar.equals("2"))
          ejecucion_operaciones_plazo( request, response );
      } else {
          // request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
          // evalTemplate(IEnlace.ERROR_TMPL,(ITemplateData) null, map);
           despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Plazo","Plazo","s26170h",request, response );
       }
    }


	//Genera la pantalla inicial  para las operaciones plazo
    /* modificacion para integracion
		public void pantalla_inicial_plazo( HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException {
      EIGlobal.mensajePorTrace("***plazo.class Entrando a pantalla_inicial_plazo ", EIGlobal.NivelLog.INFO);

	  int salida      = 0;
      String contrato = "";
      boolean fac_programadas;
      String campos_fecha = "";
      int numero_oper_plazo=0;
      short suc_opera     = (short)787;
      String usuario      = "";
      String clave_perfil = "";
      String coderror     = "";


      //Variables de sesion
      contrato     = session.getContractNumber();
      suc_opera    = (short)787;//Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
      usuario      = session.getUserID();
      clave_perfil = session.getUserProfile();
      String cadena_getFacCOpProg = session.getFacCOpProg();
      fac_programadas = (cadena_getFacCOpProg!="");


      String[][] arrayCuentas = ContCtasRelacpara_mis_transferencias(contrato);

      String strSalida   = "";

      String fecha_hoy   = "";

      String tipo_cuenta = "";

      boolean chkseg     = true;



      for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)

      {

        tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);

        if ((arrayCuentas[indice][2].equals("P"))&&( tipo_cuenta.equals("60") || tipo_cuenta.equals("65") ) )

        {

           //coderror=llamada_seguridad(arrayCuentas[indice][1],clave_perfil,"DISPVALPROP",1,suc_opera);

           //if (coderror.substring(0,8).equals("SEGU0000"))

           //{

		   chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "DISPVALPROP", "-");

           if(chkseg==true)

	       {

             strSalida = strSalida + "<OPTION VALUE=\""+arrayCuentas[indice][1]+"|"+arrayCuentas[indice][2]+"|"+arrayCuentas[indice][4]+"|"+"\"> "  + arrayCuentas[indice][1] +"   "+arrayCuentas[indice][4]+" </OPTION>\n" ;

             numero_oper_plazo++;



           }

        }

      }



      fecha_hoy=ObtenFecha();

      request.setAttribute("diasInhabiles",diasInhabilesDesp());

      if (numero_oper_plazo>0)

      {

        campos_fecha=poner_campos_fecha(fac_programadas);


        request.setAttribute("cuentas_destino", strSalida);
        request.setAttribute("arregloctas_destino1","");
        request.setAttribute("arregloimportes1","");
        request.setAttribute("arreglofechas1","");
        request.setAttribute("arregloplazo1","");
        request.setAttribute("arregloinstr1","");
        request.setAttribute("arregloctas_destino2","");
        request.setAttribute("arregloestatus1","");
        request.setAttribute("arregloestatus2","");
        request.setAttribute("arregloimportes2","");
        request.setAttribute("arreglofechas2","");
        request.setAttribute("arregloplazo2","");
        request.setAttribute("arregloinstr2","");
        request.setAttribute("contador1","0");
        request.setAttribute("contador2","0");
        request.setAttribute("tabla","");
        request.setAttribute("fecha_hoy",fecha_hoy);
        //request.setAttribute("ContUser",ObtenContUser());
        request.setAttribute("campos_fecha",campos_fecha);

        String mistasas = llamada_consulta_tasas_plazos_por_query();

        request.setAttribute("tasas1",mistasas);
        request.setAttribute("tasas2",mistasas);

        if (fac_programadas==false)
         request.setAttribute("fac_programadas1","0");
        else
         request.setAttribute("fac_programadas1","1");

        //request.setAttribute("MenuPrincipal",session.getstrMenu());

        request.setAttribute("Fecha", ObtenFecha());
   	    request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; Tradicional","s26170h"));


        evalTemplate( IEnlace.MIS_OPERPLAZO_TMPL, request, response );

      } else {

        despliegaPaginaError("No existen contratos de plazos","Plazo","", request, response );

      }

    }*/

      public void pantalla_inicial_plazo( HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException {
      EIGlobal.mensajePorTrace("***plazo.class Entrando a pantalla_inicial_plazo ", EIGlobal.NivelLog.INFO);

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
	  int salida      = 0;
      String contrato = "";
      boolean fac_programadas;
      String campos_fecha = "";
      short suc_opera     = (short)787;
      String usuario      = "";
      String clave_perfil = "";
      String coderror     = "";


      //Variables de sesion
      contrato     = session.getContractNumber();
      suc_opera    = (short)787;//Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
      usuario      = session.getUserID();
      clave_perfil = session.getUserProfile();
//      String cadena_getFacCOpProg = session.getFacCOpProg();
      //String cadena_getFacCOpProg = session.getFacultad("FacCOpProg");
      //fac_programadas = (cadena_getFacCOpProg!="");
	  fac_programadas =session.getFacultad("FacCOpProg");


      String fecha_hoy   = "";

      String tipo_cuenta = "";







      fecha_hoy=ObtenFecha();

      request.setAttribute("diasInhabiles",diasInhabilesDesp());



        campos_fecha=poner_campos_fecha(fac_programadas);

        request.setAttribute("arregloctas_destino1","");
        request.setAttribute("arregloimportes1","");
        request.setAttribute("arreglofechas1","");
        request.setAttribute("arregloplazo1","");
        request.setAttribute("arregloinstr1","");
        request.setAttribute("arregloctas_destino2","");
        request.setAttribute("arregloestatus1","");
        request.setAttribute("arregloestatus2","");
        request.setAttribute("arregloimportes2","");
        request.setAttribute("arreglofechas2","");
        request.setAttribute("arregloplazo2","");
        request.setAttribute("arregloinstr2","");
        request.setAttribute("contador1","0");
        request.setAttribute("contador2","0");
        request.setAttribute("tabla","");
        request.setAttribute("fecha_hoy",fecha_hoy);
        //request.setAttribute("ContUser",ObtenContUser());
        request.setAttribute("campos_fecha",campos_fecha);

        String mistasas = llamada_consulta_tasas_plazos_por_query();

        request.setAttribute("tasas1",mistasas);
        request.setAttribute("tasas2",mistasas);

        if (fac_programadas==false)
         request.setAttribute("fac_programadas1","0");
        else
         request.setAttribute("fac_programadas1","1");

        //request.setAttribute("MenuPrincipal",session.getstrMenu());

        request.setAttribute("Fecha", ObtenFecha());
   	    request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; Tradicional","s26170h",request));

        session.setModuloConsultar(IEnlace.MPlazo_tradicional);
        evalTemplate( IEnlace.MIS_OPERPLAZO_TMPL, request, response );



    }

    //Genera la pantalla con los plazos que se han adicionado antes de enviarlos

   /*

	public int pantalla_tabla_plazo()

    {

      System.out.print("entrando a pantalla tabla plazo");
      String  contrato="";
      int     salida=0;
      String  tabla="";
      String  arrctas_destino="";
      String  arrimporte="";
      String  arrplazos="";
      String  arrinstr="";
      String  arrestatus="";
      String  arrfechas="";
      String  cadena_cta_destino="";
      String  cta_origen="";
      String  cta_destino="";
      int     contador=0;
      String  scontador="";
      String  simporte="";
      String  opcion="";
      int     indice=0;
      String  fecha="";
      String  nombre_campo="";
      String  strSalida="";
      String  fecha_hoy="";
      String  valor="";
      boolean fac_programadas;
      String  sfac_programadas="";
      String  campos_fecha="";
      String  plazo="";
      String  instr="";
      short suc_opera=(short)787;
      String usuario="";
      String clave_perfil="";
      String coderror="";

      TemplateMapBasic map2 = new TemplateMapBasic();

      cadena_cta_destino = request.getParameter ("destino");
      simporte           = request.getParameter ("importe");
      opcion             = request.getParameter ("opcion");
      plazo              = request.getParameter ("plazo");
      instr              = request.getParameter ("inst1");


      fecha              = request.getParameter ("fecha");
      arrctas_destino    = request.getParameter ("arregloctas_destino1");
      arrimporte         = request.getParameter ("arregloimportes1");
      arrplazos          = request.getParameter ("arregloplazo1");
      arrinstr           = request.getParameter ("arregloinstr1");

      arrfechas          = request.getParameter ("arreglofechas1");
      arrestatus         = request.getParameter ("arregloestatus1");
      scontador          = request.getParameter ("contador1");
      sfac_programadas   = request.getParameter ("fac_programadas1");

      contador=Integer.parseInt(scontador);
      contador++;

      //variables de sesion
      contrato=session.getContractNumber();
      suc_opera=(short)787;//Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
      usuario=session.getUserID();
      clave_perfil=session.getUserProfile();
      System.out.println("suc_opera "+suc_opera);
      System.out.println("usuario "+usuario);
      System.out.println("clave_perfil "+clave_perfil);

      if (sfac_programadas.equals("0"))
        fac_programadas=false;
      else
        fac_programadas=true;

      String[][] arrayCuentas=ContCtasRelacpara_mis_transferencias(contrato);
      String tipo_cuenta;

	  for (indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
      {
         tipo_cuenta="";
         tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);

		 if ((tipo_cuenta.equals("60")) && (arrayCuentas[indice][2].equals("P"))) {
	             coderror=llamada_seguridad(arrayCuentas[indice][1],clave_perfil,"DISPVALPROP",1,suc_opera);
		        if (coderror.substring(0,8).equals("SEGU0000")) {
				       strSalida = strSalida + "<OPTION VALUE=\""+arrayCuentas[indice][1]+"|"+arrayCuentas[indice][2]+"|"+arrayCuentas[indice][4]+"|"+ "\"> "  + arrayCuentas[indice][1] +"   "+arrayCuentas[indice][4]+" </OPTION>\n" ;
		        }
         }
         else if ((tipo_cuenta.equals("65")) && (arrayCuentas[indice][2].equals("P")))
         {
             coderror=llamada_seguridad(arrayCuentas[indice][1],clave_perfil,"DISPVALPROP",1,suc_opera);
              if (coderror.substring(0,8).equals("SEGU0000"))
              {
                strSalida = strSalida + "<OPTION VALUE=\""+arrayCuentas[indice][1]+"|"+arrayCuentas[indice][2]+"|"+arrayCuentas[indice][4]+"|"+ "\"> "  + arrayCuentas[indice][1] +"   "+arrayCuentas[indice][4]+" </OPTION>\n" ;
              }
          }
      }

      arrctas_destino=arrctas_destino+cadena_cta_destino+"@";
      arrplazos=arrplazos+plazo+"@";
      arrinstr=arrinstr+instr+"@";
      arrimporte=arrimporte+simporte+"@";
      arrfechas=arrfechas+fecha+"@";
      arrestatus=arrestatus+"1@";
      fecha_hoy=ObtenFecha();

      System.out.println(strSalida);
      System.out.println(arrctas_destino);
      System.out.println("Plazos ");
      System.out.println(arrplazos);
      System.out.println("instruccion ");
      System.out.println(arrinstr);
      System.out.println(arrfechas);
      System.out.println(arrestatus);
      System.out.println(fecha_hoy);

      campos_fecha=poner_campos_fecha(fac_programadas);

      String arrctas_destinoprueba="\""+arrctas_destino+"\"";

      request.setAttribute("cuentas_destino", strSalida);
      request.setAttribute("arregloctas_destino1",arrctas_destinoprueba);
      request.setAttribute("arregloctas_destino2",arrctas_destinoprueba);
      request.setAttribute("arregloimportes1",arrimporte);
      request.setAttribute("arregloimportes2",arrimporte);
      request.setAttribute("arregloplazo1",arrplazos);
      request.setAttribute("arregloplazo2",arrplazos);
      request.setAttribute("arregloimportes2",arrimporte);
      request.setAttribute("arregloinstr1",arrinstr);
      request.setAttribute("arregloinstr2",arrinstr);
      request.setAttribute("arreglofechas1",arrfechas);
      request.setAttribute("arreglofechas2",arrfechas);
      request.setAttribute("arregloestatus1",arrestatus);
      request.setAttribute("arregloestatus2",arrestatus);
      request.setAttribute("contador1",Integer.toString(contador));
      request.setAttribute("contador2",Integer.toString(contador));
      request.setAttribute("fecha_hoy",fecha_hoy);
      request.setAttribute("ContUser",ObtenContUser());
      request.setAttribute("campos_fecha",campos_fecha);
      request.setAttribute("fac_programadas1","sfac_programadas");
      String mistasas = llamada_consulta_tasas_plazos_por_query();
      request.setAttribute("tasas1",mistasas);
      request.setAttribute("tasas2",mistasas);

      String[] Ctas_destino = desentramaC(Integer.toString(contador)+"@"+arrctas_destino,'@');
      String[] Importes     = desentramaC(Integer.toString(contador)+"@"+arrimporte,'@');
      String[] Fechas       = desentramaC(Integer.toString(contador)+"@"+arrfechas,'@');
      String[] Plazos       = desentramaC(Integer.toString(contador)+"@"+arrplazos,'@');
      String[] Instr        = desentramaC(Integer.toString(contador)+"@"+arrinstr,'@');
      String[] estatus      = desentramaC(Integer.toString(contador)+"@"+arrestatus,'@');

      tabla="<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=0 CELLSPACING=0 >";
      tabla=tabla+"<TR>";
      tabla=tabla+"<TD></TD>";
      tabla=tabla+"<TD WIDTH=153 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Cuenta Cargo</FONT></B></TD>";
      tabla=tabla+"<TD WIDTH=153 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Plazo</FONT></B></TD>";
      tabla=tabla+"<TD WIDTH=153 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Instrucci&oacute;n</FONT></B></TD>";
      tabla=tabla+"<TD WIDTH=153 BGCOLOR=#FF0000><P ALIGN=RIGHT><B><FONT COLOR=#FFFFFF FACE=Abadi>Importe</FONT></B></TD>";
      tabla=tabla+"<TD WIDTH=100 BGCOLOR=#FF0000><P ALIGN=RIGHT><B><FONT COLOR=#FFFFFF FACE=Abadi>Fecha</FONT></B></TD>";
      tabla=tabla+"</TR>";


      int residuo=0;

      for (indice=1;indice<=contador;indice++)
      {
         cadena_cta_destino = Ctas_destino[indice];
         cta_destino=cadena_cta_destino.substring(0,cadena_cta_destino.indexOf('|'));
         residuo=indice%2;

         if (residuo==0)
           tabla=tabla+"<TR bgcolor=#FFFFFF >";
         else
           tabla=tabla+"<TR bgcolor=#F0F0F0 >";

         valor=estatus[indice];
         nombre_campo="CH"+indice;

		 if (valor.equals("0"))
            tabla=tabla+ "<TD> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+"></TD>";
         else
            tabla=tabla+ "<TD> <INPUT TYPE =CHECKBOX NAME="+nombre_campo +" VALUE ="+valor+" checked ></TD>";

         tabla=tabla+"<TD>"+cta_destino+"</TD>";
         tabla=tabla+"<TD>"+Plazos[indice]+" dias </TD>";

		 if (Instr[indice].equals("1"))
            tabla=tabla+"<TD>Reinvertir Capital e interes</TD>";
         else if (Instr[indice].equals("2"))
            tabla=tabla+"<TD>Reinvertir Capital</TD>";
         else if (Instr[indice].equals("3"))
            tabla=tabla+"<TD>Transferir Capital e intereses</TD>";

         String importe_formateado=FormatoMoneda(Importes[indice]);
         tabla=tabla+"<TD><P ALIGN=RIGHT>"+importe_formateado+"</TD>";
         tabla=tabla+"<TD><P ALIGN=RIGHT>"+Fechas[indice]+"</TD>";
         tabla=tabla+"</TR>";
      }

      tabla=tabla+"</TABLE>";
      request.setAttribute("tabla",tabla);
      request.setAttribute("MenuPrincipal", session.getstrMenu());
      request.setAttribute("Fecha", ObtenFecha());
      salida=evalTemplate(IEnlace.MIS_OPERPLAZO_TMPL,(ITemplateData) null, map2);
      System.out.println("salida de pantalla tabla plazo");

      return 1;

    }


   */

    //Ejecuta la lista de las operaciones plazo seleccionadas llamando al servicio WEB_RED

    /*
	public int ejecucion_operaciones_plazo()
    {
        System.out.println("entrando a ejecucion de operaciones_plazo");

        String mensaje_salida="";
        String coderror="";
        String coderror_operacion="";
        String clave_perfil="";
        String contrato="";
        String tipo_operacion="";
        String usuario="";
        String cta_destino="";
        String importe_string="";
        double importe=0.0;
        String opcion="";
        String sfecha_programada="";
        String bandera_fecha_programada="1";
        String fecha_programada_b="";
        int salida=0;
        Double importedbl;
        boolean programada=false;
        String tipo_cta_destino="";
        String cadena_cta_destino="";
        String arreglo_ctas_destino="";
        String arreglo_importes="";
        String arreglo_fechas="";
        String arreglo_plazos="";
        String arreglo_instrs="";
        String arreglo_estatus="";
        int contador =0;
        String scontador="";
        int indice=0;
        String fecha="";
        String bandera_estatus="";
        String fecha_hoy="";
        String cuenta1="";
        String cuenta2="";
        String tipo_cuenta="";
        String tipo_prod="";

        String indicador1="";
        String indicador2="";
        String plazo="";
        String instr="";
        String salida_buffer="";


        String[] arrCuentas=new String[3]; //Numero de cuentas  X 2 (cuenta|tipo|)

        TemplateMapBasic map = new TemplateMapBasic();

		String trama_entrada="";
		TuxedoGlobal tuxGlobal = new TuxedoGlobal(this);
		String  sreferencia="";
        String mensajeerror="";
		String importe_formateado="";

        scontador=request.getParameter("contador2");
        contador=Integer.parseInt(scontador);

        arreglo_ctas_destino=request.getParameter("arregloctas_destino2");
        arreglo_importes=request.getParameter("arregloimportes2");
        arreglo_fechas=request.getParameter("arreglofechas2");
        arreglo_plazos=request.getParameter("arregloplazo2");
        arreglo_instrs=request.getParameter("arregloinstr2");
        arreglo_estatus= request.getParameter ("arregloestatus2");

        fecha_hoy=ObtenFecha();

        System.out.println("arreglo_ctas_destino "+arreglo_ctas_destino);
        System.out.println("arreglo_importes "+arreglo_importes);
        System.out.println("arreglo_fechas "+arreglo_fechas);
        System.out.println("arreglo_plazos "+arreglo_plazos);
        System.out.println("arreglo_instr "+arreglo_instrs);
        System.out.println("fecha_hoy "+fecha_hoy);

        String[] Ctas_destino = desentramaC(scontador+"@"+arreglo_ctas_destino,'@');
        String[] Importes     = desentramaC(scontador+"@"+arreglo_importes,'@');
        String[] Fechas       = desentramaC(scontador+"@"+arreglo_fechas,'@');
        String[] Plazos       = desentramaC(scontador+"@"+arreglo_plazos,'@');
        String[] Instruc      = desentramaC(scontador+"@"+arreglo_instrs,'@');
        String[] Estatus      = desentramaC(scontador+"@"+arreglo_estatus,'@');
        contrato=session.getContractNumber();
        usuario=session.getUserID();
        clave_perfil=session.getUserProfile();
        tipo_operacion="CPDV";

        System.out.println("//////////////");

        mensaje_salida="<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=0 CELLSPACING=0>";
        mensaje_salida=mensaje_salida+"<TR>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Referencia</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Cuenta cargo</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Plazo</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=LEFT><B><FONT COLOR=#FFFFFF FACE=Abadi>Instrucci&oacute;n</FONT></B></TD>";

        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=RIGHT><B><FONT COLOR=#FFFFFF FACE=Abadi>Importe</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=RIGHT><B><FONT COLOR=#FFFFFF FACE=Abadi>Fecha</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"<TD WIDTH=163 BGCOLOR=#FF0000><P ALIGN=RIGHT><B><FONT COLOR=#FFFFFF FACE=Abadi>Estatus</FONT></B></TD>";
        mensaje_salida=mensaje_salida+"</TR>";

        boolean  par=false;
        for (int indice1=1;indice1<=contador;indice1++)
        {
            bandera_estatus=Estatus[indice1];
            if (bandera_estatus.equals("1"))
            {

              cadena_cta_destino = Ctas_destino[indice1];
              importe_string     = Importes[indice1];
              plazo              = Plazos[indice1];
              fecha              = Fechas[indice1];
              instr              = Instruc[indice1];

              arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');
              cta_destino = arrCuentas[1];
              tipo_cta_destino=arrCuentas[2];
              importedbl = new Double (importe_string);
              importe = importedbl.doubleValue();
              sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());


              programada=checar_si_es_programada(fecha);

              System.out.println(cta_destino);
              System.out.println(importe);
              System.out.println(plazo);
              System.out.println(instr);
              System.out.println("fecha:");
              System.out.println(sfecha_programada);
              System.out.println("programada:");
              System.out.println(programada);

              tipo_cuenta=cta_destino.substring(0, EIGlobal.NivelLog.ERROR);
              if (tipo_cuenta.equals("60"))
              {
                 cuenta1=cta_destino;
                 cuenta2="62"+cta_destino.substring(2,cta_destino.length());
                 tipo_prod="77";

              }
              else
              {
                 cuenta1=cta_destino;
                 cuenta2="67"+cta_destino.substring(2,cta_destino.length());
                 tipo_prod="78";

              }

              if (instr.equals("1"))
              {
                indicador1="S";
                indicador2="S";
              }
              else if (instr.equals("2"))
              {
                indicador1="S";
                indicador2="N";
              }
              else if (instr.equals("3"))
              {
                indicador1="N";
                indicador2="N";
              }
              System.out.println("tipo_prod "+plazo);
              System.out.println("tipo_operacion "+tipo_operacion);
              System.out.println("cuenta1 "+cuenta1);
              System.out.println("cuenta2 "+cuenta2);
              System.out.println("sfecha_programada "+sfecha_programada);


              if (programada==false)
                trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cuenta1+"|"+tipo_cta_destino+"|"
                               +importe_string+"|"+cuenta2+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";
              else
                trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cuenta1+"|"+tipo_cta_destino+"|"
                               +importe_string+"|"+cuenta2+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";
              coderror=tuxGlobal.ejecutaWEB_RED(trama_entrada);

              if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC"))))
                 sreferencia=coderror.substring(8,16);
			  sreferencia=sreferencia.trim();
              if (par==false)
              {
                 mensaje_salida=mensaje_salida+"<TR bgcolor=#FFFFFF >";
                 par=true;
              }
              else
              {
                mensaje_salida=mensaje_salida+"<TR bgcolor=#F0F0F0>";
                par=false;
              }
			  coderror_operacion="";
			  salida_buffer="";
 			  if (coderror.substring(0,2).equals("OK"))
              {
                if (programada==false)
                {
                   if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
                      mensaje_salida=mensaje_salida+"<TD>"+sreferencia+"</TD>";
                   else
                   {
                     coderror_operacion=coderror.substring(coderror.indexOf("|")+1,coderror.length());
                     salida_buffer=coderror_operacion;
					 coderror_operacion=coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
                     salida_buffer=salida_buffer.substring(salida_buffer.indexOf("|")+1,salida_buffer.length());
					 salida_buffer=salida_buffer.substring(0,salida_buffer.indexOf("|"));
					 System.out.println("coderror de operacion "+coderror);
					 if (coderror_operacion.equals("CABO0000"))
                       mensaje_salida=mensaje_salida+"<TD><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceInternet.comprobante_trans?tipo=4&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cuenta1+"&cta_destino="+cuenta2+"&plazo="+plazo+"&enlinea=s\">"+sreferencia+"</A></TD>";
					 else
                       mensaje_salida=mensaje_salida+"<TD>"+sreferencia+"</TD>";
				   }
                }
				else
					mensaje_salida=mensaje_salida+"<TD>"+sreferencia+"</TD>";
			  }
			  else if (coderror.substring(0,4).equals("MANC"))
                   mensaje_salida=mensaje_salida+"<TD>-------</TD>";
              else
                   mensaje_salida=mensaje_salida+"<TD>"+sreferencia+"</TD>";

			  mensaje_salida=mensaje_salida+"<TD>"+cta_destino+"</TD>";
              mensaje_salida=mensaje_salida+"<TD>"+ plazo +" dias </TD>";
              if (instr.equals("1"))
                mensaje_salida=mensaje_salida+"<TD>Reinvertir Capital e interes</TD>";
              else if (instr.equals("2"))
                mensaje_salida=mensaje_salida+"<TD>Reinvertir Capital</TD>";
              else if (instr.equals("3"))
                mensaje_salida=mensaje_salida+"<TD>Transferir Capital e intereses</TD>";

              importe_formateado=FormatoMoneda(importe_string);
              mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>"+importe_formateado+"</TD>";
              mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>"+fecha+"</TD>";

              if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
	           	mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>Operaci&oacute;n Mancomunada</TD>";
			  else if ((coderror.substring(0,2).equals("OK"))&&(programada==true))
                mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>Operaci&oacute;n programada</TD>";
              else if ((coderror.substring(0,2).equals("OK"))&&(programada==false)&&(coderror_operacion.equals("CABO0000")))
                mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>Tranferencia aceptada</TD>";
              else
              {
                if (programada==true)
                  mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT> Operaci&oacute;n NO programada </TD>";
                else
                {
                  if (coderror.substring(0,4).equals("MANC"))
                    mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>"+coderror.substring(8,coderror.length())+"</TD>";
   				  else if ((coderror.length()>16)&&(!(coderror.substring(0,4).equals("MANC"))))
                     mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>"+salida_buffer+"</TD>";
                  else
                     mensaje_salida=mensaje_salida+"<TD><P ALIGN=RIGHT>Error de operaci&oacute;n</TD>";
                }
              }
			}//if
          }//for
          mensaje_salida=mensaje_salida+"</TABLE>";

        System.out.println("mensaje_salida "+mensaje_salida);
        request.setAttribute("titulo","Transferencias cheques Plazo Procesadas");
        request.setAttribute("mensaje_salida",mensaje_salida);
        request.setAttribute("fecha_hoy",fecha_hoy);
        request.setAttribute("ContUser",ObtenContUser());
        request.setAttribute("MenuPrincipal", session.getstrMenu());
        request.setAttribute("Fecha", ObtenFecha());

        salida=evalTemplate(IEnlace.MI_OPER_TMPL,(ITemplateData) null, map);

       System.out.println("Saliendo de  ejecucion de  plazo ");

      return 1;
    }

    */


    public void ejecucion_operaciones_plazo( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

    	EIGlobal.mensajePorTrace("***plazo.class Entrando a ejecucion_operaciones_plazo ", EIGlobal.NivelLog.INFO);

        String mensaje_salida			= "";
        String coderror					= "";
        String coderror_operacion		= "";
        String clave_perfil				= "";
        String contrato					= "";
        String tipo_operacion			= "";
        String usuario					= "";
        String cta_destino              = "";
        String importe_string           = "";
        double importe                  = 0.0;
        String opcion                   = "";
        String sfecha_programada        = "";
        String bandera_fecha_programada = "1";
        String fecha_programada_b       = "";
        int salida                      = 0;
        boolean programada              = false;
        String tipo_cta_destino         = "";
        String cadena_cta_destino       = "";
        String arreglo_ctas_destino     = "";
        int indice                      = 0;
        String fecha                    = "";
        String fecha_hoy                = "";
        String cuenta1                  = "";
        String cuenta2                  = "";
        String tipo_cuenta              = "";
        String tipo_prod                = "";

        String indicador1               = "";
        String indicador2               = "";
        String plazo                    = "";
        String instr                    = "";
        String salida_buffer            = "";
		String tramaok                  = "";
        String[] arrCuentas             = new String[3]; //Numero de cuentas  X 2 (cuenta|tipo|)

		String trama_entrada            = "";
		//TuxedoGlobal tuxGlobal          = ( TuxedoGlobal ) session.getEjbTuxedo();
		ServicioTux tuxGlobal = new ServicioTux();
		BaseResource session			= (BaseResource) request.getSession().getAttribute("session");
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	    Hashtable htResult              = null;
		String  sreferencia             = "";
        String mensajeerror             = "";
		//String importe_formateado="";

        cadena_cta_destino = request.getParameter ("destino");
        importe_string     = request.getParameter ("importe");
        opcion             = request.getParameter ("opcion");
        plazo              = request.getParameter ("plazo");
        instr              = request.getParameter ("inst1");
		fecha              = request.getParameter ("fecha");
	    fecha_hoy=ObtenFecha();


        EIGlobal.mensajePorTrace("***plazo.class cadena_cta_destino:"+cadena_cta_destino, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***plazo.class importe_string:"+importe_string, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("***plazo.class opcion:"+opcion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***plazo.class plazo:"+plazo, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***plazo.class instr:"+instr, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***plazo.class fecha:"+fecha, EIGlobal.NivelLog.INFO);

		contrato=session.getContractNumber();
        usuario=session.getUserID();
        clave_perfil=session.getUserProfile();
        tipo_operacion="CPDV";

        mensaje_salida="<table width=760 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
        mensaje_salida=mensaje_salida+"<TR>";
        mensaje_salida=mensaje_salida+"<td width=80 class=tittabdat align=center>Cuenta</td>";
        mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=60>Plazo</td>";
        mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=150>Istrucci&oacute;n al vencimiento</td>";
        mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=90>Importe</td>";

		mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=75>Fecha de aplicaci&oacute;n</td>";
		mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=66>Estatus</td>";
        mensaje_salida=mensaje_salida+" <td class=tittabdat align=center width=65>Referencia</td>";
		mensaje_salida=mensaje_salida+"</TR>";


		 System.out.println("ctas destino ="+cadena_cta_destino);
        arrCuentas= desentramaC("2|" + cadena_cta_destino,'|');

        cta_destino = arrCuentas[1];
        tipo_cta_destino=arrCuentas[2];

        importe = new Double(importe_string).doubleValue();
        sfecha_programada=fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());
        programada=checar_si_es_programada(fecha);
        tipo_cuenta=cta_destino.substring(0, 2);
        if (tipo_cuenta.equals("60"))
        {
           cuenta1=cta_destino;
           cuenta2="62"+cta_destino.substring(2,cta_destino.length());
           tipo_prod="77";

     }
        else
        {
           cuenta1=cta_destino;
           cuenta2="67"+cta_destino.substring(2,cta_destino.length());
           tipo_prod="78";
        }
        if (instr.equals("1"))
        {
           indicador1="S";
           indicador2="S";
        }
        else if (instr.equals("2"))
        {
           indicador1="S";
           indicador2="N";
        }
        else if (instr.equals("3"))
        {
            indicador1="N";
            indicador2="N";
        }

        if (programada==false)
           trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cuenta1+"|"+tipo_cta_destino+"|"
                         +importe_string+"|"+cuenta2+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";
        else
           trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cuenta1+"|"+tipo_cta_destino+"|"
                         +importe_string+"|"+cuenta2+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";

		EIGlobal.mensajePorTrace("***plazo WEB_RED-plazo>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
		try {
	    	htResult = tuxGlobal.web_red( trama_entrada );
	    } catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace();
	    }catch ( Exception e ) {}
		coderror = ( String ) htResult.get( "BUFFER" );
		EIGlobal.mensajePorTrace("***plazo<<"+coderror, EIGlobal.NivelLog.DEBUG);

 		if (coderror.indexOf("NO EXISTEN DATOS PARA LA CONSULTA SOLICITADA") > -1 )
        {
		 despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Inversi&oacute;n a Plazo","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; Tradicional","s26180h",request, response  );

        }else
		{
        if ((coderror.length()>=16)&&  (!(coderror.substring(0,4).equals("MANC"))))

             sreferencia=coderror.substring(8,16);
		sreferencia=sreferencia.trim();
        mensaje_salida=mensaje_salida+"<TR>";
        coderror_operacion="";
		salida_buffer="";

		mensaje_salida=mensaje_salida+"<TD class=textabdatobs nowrap align=center>"+cta_destino+"</TD>";
        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center>"+ plazo +" dias </TD>";

		if (instr.equals("1"))
             mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital e interes</TD>";
        else if (instr.equals("2"))
             mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital</TD>";
        else if (instr.equals("3"))
             mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=left>Transferir Capital e intereses</TD>";

        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>";
        mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";


        if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
	      	mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n Mancomunada</TD>";
		else if ((coderror.substring(0,2).equals("OK"))&&(programada==true))
            mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n programada</TD>";
        else if ((coderror.substring(0,2).equals("OK"))&&(programada==false))
        {
          coderror_operacion=coderror.substring(coderror.indexOf("|")+1,coderror.length());
	      coderror_operacion=coderror_operacion.substring(0,coderror_operacion.indexOf("|"));

		  if (coderror_operacion.equals("CABO0000"))
		     mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Tranferencia aceptada</TD>";
          else
 		     mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";

		}
		else
        {
           if (programada==true)
              mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n NO programada </TD>";
           else
           {
              if (coderror.substring(0,4).equals("MANC"))
                 mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(8,coderror.length())+"</TD>";
   			  else if ((coderror.length()>32)&&(!(coderror.substring(0,4).equals("MANC"))))
                 mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(32,coderror.length())+"</TD>";
              else
                 mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
            }
        }

//else {despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Plazo","", request, response ); }

 		if (coderror.substring(0,2).equals("OK"))
        {
           if (programada==false)
           {
              if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
			     mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";

			   else
               {
                 coderror_operacion=coderror.substring(coderror.indexOf("|")+1,coderror.length());
                 salida_buffer=coderror_operacion;
				 coderror_operacion=coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
                 salida_buffer=salida_buffer.substring(salida_buffer.indexOf("|")+1,salida_buffer.length());
				 salida_buffer=salida_buffer.substring(0,salida_buffer.indexOf("|"));

				 if (coderror_operacion.equals("CABO0000"))
				 {
			        //mensaje_salida=mensaje_salida+"<TD ALIGN=RIGHT><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceInternet.comprobante_trans?tipo=4&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cuenta1+"&cta_destino="+cuenta2+"&plazo="+plazo+"&enlinea=s\">"+sreferencia+"</A></TD>";
                     tramaok+="1|5|"+sreferencia.trim()+"|"+importe_string+"|"+cuenta1+"|"+cuenta2+"| @";
			         mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value, 1);\" >"+sreferencia+"</A></TD>";

		         }
		         else
                     mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
			   }
            }
			else
				mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
		}
		else if (coderror.substring(0,4).equals("MANC"))
           mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
        else
           mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";

        mensaje_salida=mensaje_salida+"</TR>";
        mensaje_salida=mensaje_salida+"</TABLE>";
		mensaje_salida+="<table width=760 border=0 cellspacing=2 cellpadding=0><tr><td valign=top align=left class=textabref width=372>Si desea obtener su comprobante, d&eacute; click en el n&uacute;mero de referencia subrayado.</td></tr></table>";

        request.setAttribute("titulo","Transferencias cheques Plazo Procesadas");
        request.setAttribute("mensaje_salida",mensaje_salida);
        request.setAttribute("fecha_hoy",fecha_hoy);
        //request.setAttribute("ContUser",ObtenContUser());
        //request.setAttribute("MenuPrincipal", session.getstrMenu());
        request.setAttribute("Fecha", ObtenFecha());
   	    request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; Tradicional","s26180h",request));
        request.setAttribute("tramaok","\""+tramaok+"\"");
        request.setAttribute("contadorok","1");
        request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
        request.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");
     	request.setAttribute("banco",session.getClaveBanco());

 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
         HttpSession ses = request.getSession();
		 ses.setAttribute("titulo","Transferencias cheques Plazo Procesadas");
         ses.setAttribute("mensaje_salida",mensaje_salida);
         ses.setAttribute("fecha_hoy",fecha_hoy);
        //request.setAttribute("ContUser",ObtenContUser());
        //request.setAttribute("MenuPrincipal", session.getstrMenu());
        ses.setAttribute("Fecha", ObtenFecha());
   	    ses.setAttribute("MenuPrincipal", session.getStrMenu());
        ses.setAttribute("newMenu", session.getFuncionesDeMenu());
        ses.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; Tradicional","s26180h",request));
        ses.setAttribute("tramaok","\""+tramaok+"\"");
        ses.setAttribute("contadorok","1");
        ses.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
        ses.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");
   	    ses.setAttribute("web_application",Global.WEB_APPLICATION);
   	    //<vswf:meg cambio de NASApp por Enlace 08122008>
		response.sendRedirect(response.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL));
//		evalTemplate(IEnlace.MI_OPER_TMPL, request, response );

    } //else

} //  metodo

	//Hace consulta en tabla comu_tablas para obtener las tasas de plazo
    /**
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
     */
    public String llamada_consulta_tasas_plazos_por_query() {

    	EIGlobal.mensajePorTrace("***plazo.class Entrando a ejecucion_operaciones_plazo ", EIGlobal.NivelLog.INFO);

		String coderror          = "";
        String cadena_consulta   = "";
        String buffer			 = "";
        String query_realizar    = "";
        String cadena_resultante = "";
        int contador             = 0;
        Connection conn          = null;
	    PreparedStatement ps     = null;
	    ResultSet rs             = null;


	    try {

	    	conn = createiASConn( Global.DATASOURCE_ORACLE );
	        query_realizar="Select ltrim(to_char(fecha,'dd/mm/yyyy')),to_number(plazo),ltrim(to_char(rango_menor,'$999,999,999.00')),ltrim(to_char(rango_mayor,'$999,999,999,999.00')), ltrim(tasa) ";
	        query_realizar=query_realizar + " from comu_tasa ";
	        query_realizar=query_realizar + " where instrumento='4' and ";
	        query_realizar=query_realizar + " person_juridica='M' ";
	        query_realizar=query_realizar + " order by fecha, to_number(plazo), rango_menor ";
	        EIGlobal.mensajePorTrace("***plazo.class Query:"+query_realizar, EIGlobal.NivelLog.ERROR);
	        ps = conn.prepareStatement( query_realizar );
		    rs = ps.executeQuery();
	        cadena_resultante = "";
	        String strCadenaED ="";

	        while ( rs.next() ) {
	          strCadenaED = rs.getString(2).trim();
	          cadena_resultante = cadena_resultante + rs.getString(1) +"|";
	          cadena_resultante = cadena_resultante + rs.getString(2) +"|";
	          cadena_resultante = cadena_resultante + rs.getString(3) +"|";
	          cadena_resultante = cadena_resultante + rs.getString(4) +"|";
	          cadena_resultante = cadena_resultante + rs.getString(5) +"|";
	          contador++;
	        }

	        cadena_resultante = "\""+contador +"|"+cadena_resultante+"\"";



    	} catch ( SQLException sqle ) {
    		sqle.printStackTrace();
    	} finally {
    		 try {
    			 EI_Query.cierraConexion(rs, ps, conn);
			} catch (Exception e) {
				e.printStackTrace();
			}

    	}

        return cadena_resultante;
    }
}