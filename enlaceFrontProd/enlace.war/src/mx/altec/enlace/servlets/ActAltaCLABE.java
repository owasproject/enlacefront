/*Banco Santander Mexicano
  Clase ActAltaCLABE actualizaci�n masiva de cuentas CLABE
  @Autor:Rafael Rosiles Soto
  @version: 1.0
  fecha de creacion: 14 de Enero de 2004
*/

package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ActAltaCLABE extends BaseServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
	HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		EIGlobal.mensajePorTrace ("*** ActAltaCLABE.defaultAction entrando ***", EIGlobal.NivelLog.INFO);

     	request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta y Actualizaci&oacute;n de cuentas CLABE",
				"Administraci&oacute;n y control &gt; Cuentas &gt; Actualizaci&oacute;n de cuentas CLABE","s37040h", request));
		request.setAttribute("Fecha", ObtenFecha());
		request.setAttribute("Hora", ObtenHora());
		request.setAttribute("ContUser",ObtenContUser(request));

		boolean sesionvalida = SesionValida( request, response );

		// Parametro que se debe agregar a IENLACE para mostrar el numero de cuentas
		// que el usuario desee
		String NumeroPag = IEnlace.REGISTROS_CLABE;
		String Archivo = session.getContractNumber()+".aclabe";
		String pathFileOutput = "/Download/"+ session.getContractNumber() + ".aclabee";

		boolean result = false;

		//if(sesionvalida && session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC) && session.getFacultad(session.FAC_TRANSF_NOREG)) {
		if(sesionvalida && session.getFacultad(session.FAC_ALTA_CTAS_OTROSBANC)) {

			// llamar actualizacion de cuentas CLABE
			String[] vectorCuentas = actualizaCLABE( "B" , Archivo, request, response );

			if (!vectorCuentas[1].equals("ACCL0000")) {
				request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
				evalTemplate( IEnlace.ERROR_TMPL, request, response );
			}
			else {

				StringBuffer strFecha = new StringBuffer("");

	      		Date Hoy = new Date();

	      		GregorianCalendar Cal = new GregorianCalendar();

	      		Cal.setTime(Hoy);

	           	strFecha.append(Cal.get(Calendar.DATE));

	        	strFecha.append("/");

	      	    strFecha.append((Cal.get(Calendar.MONTH)+1));

	      		strFecha.append("/");

	        	strFecha.append(Cal.get(Calendar.YEAR));

				String tablaResultados="<table border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF>"+
						    "<tr><td align='center' class=tittabdat nowrap>Operaci&oacute;n</td>"+
						    "<td align='center' class=tittabdat nowrap>Fecha</td>"+
						    "<td align='center' class=tittabdat nowrap>Total de cuentas</td>"+
					   	    "<td align='center' class=tittabdat nowrap>Cuentas actualizadas</td></tr>"+
				            "<tr><td align='center' class='textabdatcla' nowrap>&nbsp;Actualizaci&oacute;n de cuentas CLABE</td>"+
				            "<td align='center' class='textabdatcla' nowrap>"+strFecha.toString()+"</td>"+
				            "<td align='center' class='textabdatcla' nowrap>"+vectorCuentas[2]+
							"&nbsp;</td><td align='center' class='textabdatcla' nowrap>"+vectorCuentas[3]+
							"&nbsp;</td></tr></table><br><br>"+
							"<table align='center'><tr>"+
					        "<td><a href='javascript:;' onClick=" + "\"MM_openBrWindow('" + pathFileOutput + "','ayuda','menubar=yes,scrollbars=yes,resizable=yes,width=750,height=500')\""+ ">" + "<img border='0' name='imageField32' src='/gifs/EnlaceMig/gbo25230.gif' width='83' height='22' alt='Exportar'></a>" +
							//"<td><a href='" +  pathFileOutput + "'><img border='0' name='imageField32' src='/gifs/EnlaceMig/gbo25230.gif' width='83' height='22' alt='Exportar'></a>"+
							"<a href='javascript:scrImpresion();'><img border='0' name='imageField3' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'></a></td></tr>"+
							"</table>";
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Resultados", tablaResultados );
				evalTemplate( "/jsp/ActCtaCLABE.jsp", request, response );
			}

		}
		else {
			//System.out.println("Este contrato NO tiene facultades para esta operaci�n");
			//request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP );
			//evalTemplate( IEnlace.ERROR_TMPL, request, response );
			response.sendRedirect("SinFacultades");
		}
	}
}