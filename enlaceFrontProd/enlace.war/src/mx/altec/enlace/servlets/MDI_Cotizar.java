package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.text.SimpleDateFormat;
import java.sql.*;

public class MDI_Cotizar extends BaseServlet
 {
    /**
     * Variable para definir separador en el armado de tramas
     */
	String pipe="|";

    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

  public void defaultAction( HttpServletRequest req, HttpServletResponse res)		throws ServletException, IOException
    {
		boolean arcExp=false;

		EI_Tipo Reg=new EI_Tipo(this);
		EI_Tipo NoReg=new EI_Tipo(this);

		EIGlobal.mensajePorTrace("MDI_Cotizar - execute(): Entrando a Cotizacion Interbancaria.", EIGlobal.NivelLog.INFO);

		boolean sesionvalida=SesionValida( req, res );

		EIGlobal.mensajePorTrace("MDI_Cotizar - defaultAction(): La sesion es: " +sesionvalida, EIGlobal.NivelLog.INFO);
		if(sesionvalida)
		 {
			EIGlobal.mensajePorTrace("MDI_Cotizar - defaultAction(): Se valido sesion", EIGlobal.NivelLog.INFO);
			Reg.iniciaObjeto(req.getParameter("TransReg"));
			NoReg.iniciaObjeto(req.getParameter("TransNoReg"));
			EIGlobal.mensajePorTrace("MDI_Cotizar - defaultAction(): TransArch = [" + req.getParameter("TransArch") + "]", EIGlobal.NivelLog.INFO);

			if("Archivo de Exportacion".equals(((String) req.getParameter("TransArch")).trim())){
			 arcExp=true; }
			iniciaCotizar(NoReg,Reg,arcExp,req, res );
		 }
		EIGlobal.mensajePorTrace("MDI_Cotizar - execute(): Saliendo de Cotizacion Interbancaria.", EIGlobal.NivelLog.INFO);
    }

/*************************************************************************************/
/***************************	  ************************************* Inicio Cotizar	   */
/*************************************************************************************/
  public void iniciaCotizar(EI_Tipo NoReg,EI_Tipo Reg,boolean arcExp,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	 String strTabla="";
     String strCheck="";

     String nuevaTramaReg="";
     String nuevaTramaNoReg="";
	 String fechaMenor="";
	 String fechaMayor="";
	 /* VECTOR 06-2016: SPID */	
	 String divisaOper = "";

     int n1=Reg.totalRegistros;
//     int n2=n1+NoReg.totalRegistros;

	 double importeTotal=0;

	 EI_Tipo Todas=new EI_Tipo(this);

	 /************* Modificacion para la sesion ***************/
	 HttpSession sess = req.getSession();
	 BaseResource session = (BaseResource) sess.getAttribute("session");
	 EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	 /*
		 0 Cuenta|
		 1 Titular|
		 2 Cuenta Abono|
		 3 Beneficiario|
		 4 Importe|
		 5 Concepto|
		 6 Refe Inter|
		 7 Banco|
		 8 Plaza|
		 9 Sucursal|
		10 Fecha|
		11 RFC|
		12 Iva|
		13 forma Aplica|
		14 Desc Forma Aplica@
	 */

     String[] titulos={"",
		       "Cuenta de Cargo",
		       "Titular",
		       "Cuenta de Abono/M&oacute;vil",
			   "Beneficiario",
		       "Importe",
		       "Concepto",
		       "Referencia Interbancaria",
		       "Forma Aplicaci&oacute;n",
		       "Fecha Aplicaci&oacute;n"};

     int[] datos={9,0,0,1,2,3,4,5,6,14,15};
	 int[] values={0,0,0};
	 int[] align={0,0,0,0,2,0,0,0,1};

	String IP = " ";
	String fechaHr = "";												//variables locales al método
	IP = req.getRemoteAddr();											//ObtenerIP
	java.util.Date fechaHrAct = new java.util.Date();
	SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
	fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			        //asignación de fecha y hora

	EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): <"+fechaHr+">", EIGlobal.NivelLog.DEBUG);

	Vector diasNoHabiles = CargarDias(1);

	if(arcExp)
	 {
	   fechaMenor=creaFechaCotiza(false,diasNoHabiles,req);
	   fechaMayor=creaFechaCotiza(true,diasNoHabiles,req);
	 }
	else
	 {
	   strCheck+=(String) req.getParameter("strCheck");
	   EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): Datos en objeto Reg <"+Reg.totalCampos+">", EIGlobal.NivelLog.DEBUG);
	   EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): Datos en objeto NoReg <"+NoReg.totalCampos+">", EIGlobal.NivelLog.DEBUG);
	   EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): Datos en objeto NoReg <"+Reg.totalRegistros+">", EIGlobal.NivelLog.DEBUG);
	   //INICIA PYME 2015 INDRA se quitan los valores correspondientes a favoritos
	   int longitud = strCheck.length();
	   StringBuffer strCheckAux = new StringBuffer();
	   for( int i = 0 ; i < longitud ; i+=2 ){
		   strCheckAux.append(strCheck.charAt(i));
	   }
	   strCheck = strCheckAux.toString();
	   //FIN PYME 2015 INDRA se quitan los valores correspondientes a favoritos
			for (int i = 0; i < Reg.totalRegistros; i++) {
				if (strCheck.charAt(i) != '0') {
					for (int c = 0; c < 15; c++) {
						EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): TRAMA RED <" + Reg.camposTabla[i][c] + ">", EIGlobal.NivelLog.INFO);
						nuevaTramaReg += Reg.camposTabla[i][c] + pipe;
					}
					if (Reg.camposTabla[i][13].trim().equals("H")) {
						nuevaTramaReg += creaFechaCotiza(true, diasNoHabiles, req) + pipe;
					} else {
						nuevaTramaReg += creaFechaCotiza(false, diasNoHabiles, req) + pipe;
					}
					if(!"".equals(Reg.camposTabla[i][16])){
						divisaOper = Reg.camposTabla[i][16];
						EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): DIVISA <" + divisaOper + ">", EIGlobal.NivelLog.INFO);
					}
					nuevaTramaReg += "@";
				}
			}
		for(int i=0;i<NoReg.totalRegistros;i++){
		  if(strCheck.charAt(i+n1)!='0')
		   {
			 for(int c=0;c<15;c++){
				 nuevaTramaNoReg+=NoReg.camposTabla[i][c]+pipe; }
			 if(NoReg.camposTabla[i][13].trim().equals("H")){
				 nuevaTramaNoReg+=creaFechaCotiza(true,diasNoHabiles,req)+pipe;
			 } else {
				 nuevaTramaNoReg+=creaFechaCotiza(false,diasNoHabiles,req)+pipe; }
			 nuevaTramaNoReg+="@";
		   }
		}
		// TODO CAMBIO SPID
		//PYME 2015 INDRA se agrega encoding
		Todas.iniciaObjeto(new String( (nuevaTramaReg+nuevaTramaNoReg).getBytes("ISO-8859-1"),"UTF-8" ));
		importeTotal=0;
		for(int i=0;i<Todas.totalRegistros;i++){
			importeTotal+=new Double(Todas.camposTabla[i][4]).doubleValue(); }
		EnlaceGlobal.formateaImporte(Todas, 4);

		strTabla+=Todas.generaTabla(titulos,datos,values,align);
		EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): Se genero la tabla", EIGlobal.NivelLog.INFO);
	}

   if(arcExp){
	   req.setAttribute("Monto","Piso CERO");
   }
   req.setAttribute("FechaMenor",fechaMenor);
   req.setAttribute("FechaMayor",fechaMayor);

   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
   if (req.getAttribute("Tabla") != null) {
	   req.setAttribute("Tabla", req.getAttribute("Tabla"));
   } else {
	   req.setAttribute("Tabla", "");
   }

   req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
   req.setAttribute("MenuPrincipal", session.getStrMenu());
   req.setAttribute("newMenu", session.getFuncionesDeMenu());
   req.setAttribute("Encabezado", CreaEncabezado("Cotización Interbancaria","Transferencias &gt; Interbancarias &gt; Cotización","s25460h",req));

   req.setAttribute("Lineas",(String) req.getParameter("Lineas"));
   req.setAttribute("Importe",(String) req.getParameter("Importe"));

   EIGlobal.mensajePorTrace("MDI_Cotizar - iniciaCotizar(): Se enviaron los parametros", EIGlobal.NivelLog.INFO);

 //  if(!arcExp)
//	{
	  req.setAttribute("TotalTrans",Todas.strOriginal);
	  req.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Todas.totalRegistros));
	  req.setAttribute("importeTotal","Importe Total: "+FormatoMoneda(Double.toString(importeTotal)));
	  sess.setAttribute("registrosTIB", Todas.totalRegistros);
	  sess.setAttribute("importeTIB", importeTotal);
	  sess.setAttribute("cuentaTIB", datos[0]);
	  /* VECTOR 06-2016: SPID */	
	  sess.setAttribute("DivisaMsjOper", divisaOper);

		// Transferencias programadas
	  	// INDRA PYME ABRIL 2015
	  req.setAttribute("TransReg",req.getParameter("TransReg"));
	  req.setAttribute("TransNoReg",req.getParameter("TransNoReg"));

				// Transferencias programadas
			  	// INDRA PYME ABRIL 2015
	  /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
	   * Se agrega validacion para que continue con la validacion del token
	   * y no con el jsp*/
      	//evalTemplate("/jsp/MDI_Cotizar.jsp", req, res);
		// INDRA PYME
		// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA
		req.setAttribute("Modulo", "1");
		RequestDispatcher rdSalida = getServletContext()
	    .getRequestDispatcher("/enlaceMig/MDI_Enviar");
		rdSalida.forward( req, res );

		// INDRA PYME
		// VALIDACION DUPLICADOS EN UNA SOLA PANTALLA
	//}
   //else
	//{
	 // req.setAttribute("TotalTrans","Archivo de Exportacion");

//	  evalTemplate("/jsp/MDI_CotizarArchivo.jsp", req, res);
	//}
	/*
	despliegaPaginaError(cmpCtz[2],"Cotización Interbancaria","Transferencias &gt; Interbancarias &gt; Cotización", "s25460h",req, res);
	despliegaPaginaError("Pagina no disponible.","Cotización Interbancaria","Transferencias &gt; Interbancarias &gt; Cotización","s25460h",req, res);
	*/
  }

/*************************************************************************************/
/************************************************************** fecha de Cotizacion  */
/*************************************************************************************/
   String creaFechaCotiza(boolean tipo, Vector diasNoHabiles, HttpServletRequest req)
    {
      String strFecha = "";
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Vector DiasInhabiles = null;
      Calendar DiaHabil=null;

	  Cal.setTime(Hoy);

      if(!tipo)
       {  //Siguiente dia ...
		  GregorianCalendar Cal24 = new GregorianCalendar();
		  Cal24.setTime(Hoy);
		  Cal24=(GregorianCalendar)evaluaDiaSiguienteHabil(Cal24, diasNoHabiles);

		  if(Cal24.get(Calendar.DATE) <= 9)
			strFecha+="0" + Cal24.get(Calendar.DATE) + "/";
		  else
			strFecha+=Cal24.get(Calendar.DATE) + "/";
		  if(Cal24.get(Calendar.MONTH)+1 <= 9)
			strFecha+="0" + (Cal24.get(Calendar.MONTH)+1) + "/";
		  else
			strFecha+=(Cal24.get(Calendar.MONTH)+1) + "/";
		  strFecha+=Cal24.get(Calendar.YEAR);
	   }
      else
		{ //Mismo dia ...
		  if(Cal.get(Calendar.DATE) <= 9)
			strFecha+="0" + Cal.get(Calendar.DATE) + "/";
		  else
			strFecha+=Cal.get(Calendar.DATE) + "/";
		  if(Cal.get(Calendar.MONTH)+1 <= 9)
			strFecha+="0" + (Cal.get(Calendar.MONTH)+1) + "/";
		  else
			strFecha+=(Cal.get(Calendar.MONTH)+1) + "/";
		  strFecha+=Cal.get(Calendar.YEAR);
		}

      return strFecha;
    }

/*************************************************************************************/
/******************************************************** evaluaDiaSiguienteHabil    */
/*************************************************************************************/
 public static Calendar evaluaDiaSiguienteHabil(Calendar fecha, Vector diasNoHabiles)
  {
	 int      indice;
	 int      diaDeLaSemana;
	 boolean  encontrado;

	 EIGlobal.mensajePorTrace("EvaluaDiaHabil", EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Dia  " + fecha.get( fecha.DAY_OF_MONTH), EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Mes  " + fecha.get( fecha.MONTH), EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Anio " + fecha.get( fecha.YEAR), EIGlobal.NivelLog.INFO);

	 do
		{
		   fecha.add( fecha.DAY_OF_MONTH,  1);
		   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
		   encontrado = false;
		   if(diasNoHabiles!=null)
			{
			  for(indice=0; indice<diasNoHabiles.size(); indice++)
			   {
				  String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
				  String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
				  String anio = Integer.toString( fecha.get( fecha.YEAR) );
				  dia=(dia.trim().length()==1)?"0"+dia.trim():dia.trim();
				  mes=(mes.trim().length()==1)?"0"+mes.trim():mes.trim();
				  String strFecha = dia.trim() + "/" + mes.trim() + "/" + anio.trim();

				  if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
				   {
					 EIGlobal.mensajePorTrace("Dia es inhabil", EIGlobal.NivelLog.INFO);
					 encontrado = true;
					 break;
				   }
			   }
			}
	   }while( (diaDeLaSemana == Calendar.SUNDAY) || (diaDeLaSemana == Calendar.SATURDAY) || encontrado );

	 EIGlobal.mensajePorTrace("Termina EvaluaDiaHabil", EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Dia limite encontrado", EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Dia  " + fecha.get( fecha.DAY_OF_MONTH ), EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Mes  " + fecha.get( fecha.MONTH ), EIGlobal.NivelLog.INFO);
	 EIGlobal.mensajePorTrace("Anio " + fecha.get( fecha.YEAR ), EIGlobal.NivelLog.INFO);

	 return fecha;
  }

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas   */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace("***transferencia.class Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();

	  boolean    estado;

	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("***tranferencia.class Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  //******************
		          }
		         rsDias.close();
		       }
			  else
		         EIGlobal.mensajePorTrace("***tranferencia.class  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
		    }
		   else
		      EIGlobal.mensajePorTrace("***transferencia.class  Cannot Create Query", EIGlobal.NivelLog.ERROR);
		 }
		else
		 EIGlobal.mensajePorTrace("***transferencia.class Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
	   }catch(SQLException sqle ){
		   EIGlobal.mensajePorTrace(sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	   }
	   finally {
		   try {
			   if( rsDias != null ){
				   rsDias.close();
			   }
			   Conexion.close();
		   }catch(Exception e) {}
	   }
	 return(diasInhabiles);
	}

}