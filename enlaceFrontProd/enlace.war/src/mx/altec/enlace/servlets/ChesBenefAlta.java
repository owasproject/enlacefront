package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.lang.Number.*;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class ChesBenefAlta extends BaseServlet{
    //int numero_cuentas= 0;
    int salida= 0;
    //int i = 0;
    String fecha_hoy = "";
    String strFecha = "";
    String cuenta = "";
    String VarFechaHoy="";
    short suc_opera = (short)787;

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		String usuario = "";
		String contrato = "";
		String clave_perfil = "";
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
		if(SesionValida( req, res )){
			usuario = session.getUserID8();
	    	contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
	    	suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

	    	String ventana = (String) req.getParameter("ventana");
			if(ventana.equals("0"))
	        {
			   if (session.getFacultad(session.FAC_BENEF_NOREG))
			   {
				inicia(clave_perfil, contrato, usuario,  req, res );
               }
			   else
               {
                  req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
	              despliegaPaginaErrorURL("No tiene facultad para dar de alta beneficiarios", "Alta de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta", "s25950h", "ChesBenefAlta?ventana=0", req, res);
			   }
	    	}
			if(ventana.equals("1")){
		    	alta(clave_perfil,contrato, usuario, req, res );
			}
		}
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &Termina clase&", EIGlobal.NivelLog.INFO);
    }

	public int inicia(String clave_perfil,String contrato, String usuario, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		int EXISTE_ERROR = 0;
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta = (String) req.getParameter("txtFechaAlta");
		EXISTE_ERROR = 0;
		int err [] = new int [1];
		err[0] = 0;
		//String cadenaBeneficiarios = listarBeneficiarios(err,usuario, contrato, clave_perfil, suc_opera);
		EXISTE_ERROR = err[0];
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);
		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		//req.setAttribute("MenuPrincipal", session.getstrMenu());
		//req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta","s25950h",req) );
		//if (EXISTE_ERROR ==1)
		//	despliegaPaginaError(cadenaBeneficiarios,"Alta de Beneficiario","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta",req, res);
		//else
			evalTemplate("/jsp/ChesBenefAlta.jsp", req, res );

		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);
		return salida;
	}


	public int alta(String clave_perfil, String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  int err [] = new int [1];
      EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &inicia alta()&", EIGlobal.NivelLog.INFO);
      //String cadenaBeneficiarios = listarBeneficiarios(err,usuario,contrato, clave_perfil, suc_opera);
	  String CveBeneficiario=(String) req.getParameter("txtCveBeneficiario");
      String Beneficiario=(String) req.getParameter("txtBeneficiario");
      String strFechaAlta=(String) req.getParameter("txtFechaAlta");
      String loc_trama = "";
      String glb_trama = "";
      String trama_entrada="";
      String trama_salida="";
      String cabecera = "1EWEB";
      String operacion = "ABEN";
      String nombre_benef="";
      String resultado="";
	  String mensaje = "";
      int primero=0;
      int ultimo=0;
	  int EXISTE_ERROR = 0;

	  EXISTE_ERROR = 0;
      cuenta = CveBeneficiario;
      //TuxedoGlobal tuxedoGlobal;
		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


	  fecha_hoy=ObtenFecha();
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &validando datos del beneficiario ...&", EIGlobal.NivelLog.INFO);
        if ( CveBeneficiario.trim().equals("") )
        {
            mensaje = "Debe introducir la cuenta al beneficiario.";
        }
		if ( Beneficiario.trim().equals("") )
        {
            mensaje = "Debe poner un beneficiario.";
        }
        else
        {
              loc_trama = formatea_contrato(contrato) + "@" + CveBeneficiario.trim() + "@" + Beneficiario.trim() + "@" ;
              trama_entrada = cabecera + "|" + clave_perfil + "|" + operacion + "|" + contrato + "|" + clave_perfil + "|" ;
              trama_entrada = trama_entrada + usuario + "|" + loc_trama ;
			  EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
			  EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			  try{
			  	//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo();
			  	//Hashtable hs = tuxedoGlobal.web_red("2TCT|1001851|CFRR|80000648376|1001851|1001851A|/tmp/1001851.tmp@1@asra.TXT@|||");
                Hashtable hs = tuxedoGlobal.web_red(trama_entrada);

				trama_salida = (String) hs.get("BUFFER");
              	EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
              }catch( java.rmi.RemoteException re ){
				re.printStackTrace();
			  } catch(Exception e) {}
              if(trama_salida.startsWith("OK"))
              {
                  primero = trama_salida.indexOf( (int) '@');
                  ultimo  = trama_salida.lastIndexOf( (int) '@');
                  resultado = trama_salida.substring(primero+1, ultimo);
                  mensaje = resultado;
				  //mensaje = trama_salida;
              }
              else
              {
                  mensaje = trama_salida ;
              }

        }

		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &desplegando datos en pantalla ...&", EIGlobal.NivelLog.INFO);
        req.setAttribute("FechaHoy", ObtenFecha(false));
        //req.setAttribute("MenuPrincipal", session.getstrMenu());
        req.setAttribute("ContUser",ObtenContUser(req));
        req.setAttribute("Fecha", ObtenFecha(true));
        req.setAttribute("mensaje_salida", mensaje);

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta","s25950h",req) );

	    despliegaPaginaErrorURL(mensaje, "Alta de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Alta", "s25950h", "ChesBenefAlta?ventana=0", req, res);
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &termina m�todo alta()&", EIGlobal.NivelLog.INFO);
		return salida;
	}
	/*-----------------------------------------------------------------------*/
	String formatea_contrato (String ctr_tmp){
		int i=0;
		String temporal="";
		String caracter="";

		for (i = 0 ; i < ctr_tmp.length() ; i++)
		    {
		    caracter = ctr_tmp.substring(i, i+1);
		    if (caracter.equals("-"))
		    {
		        // se elimina el gui�n de la cadena
		    }
		    else
		    {
		    temporal += caracter ;
		    }


		   }
		return temporal;
	}
	/*--------------------------------------------------------*/
/*	public String listarBeneficiarios(int err[] ,String usuario, String contrato, String clave_perfil, short sucOpera){
	   EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &inicia m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
       int    indice;
       String encabezado          = "";
       String tramaEntrada        = "";
       String tramaSalida         = "";
       String path_archivo        = "";
       String nombre_archivo      = "";
       String trama_salidaRedSrvr = "";
       String retCodeRedSrvr      = "";
       String registro            = "";
       String listaBeneficiarios  = "";
       String listaNombreBeneficiario = "";
	   String strDebug="";
       //TuxedoGlobal tuxGlobal;
		ServicioTux tuxGlobal = new ServicioTux();
		tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

       String medio_entrega  = "2EWEB";
       String tipo_operacion = "CBEN";
	   err[0]=0;
       encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
       encabezado += usuario + "|" + clave_perfil + "|";
       tramaEntrada = contrato + "@";
       tramaEntrada = encabezado + tramaEntrada;
       strDebug += "\ntecb :" + tramaEntrada;

		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
	   try{
	   		//tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
			EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
			}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
		} catch (Exception e) {}
       strDebug += "\ntscb:" + tramaSalida;

       path_archivo = tramaSalida;
       nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', EIGlobal.NivelLog.DEBUG);
       path_archivo = "/tmp/" + nombre_archivo;
       EIGlobal.mensajePorTrace( "Nombre_archivo"+nombre_archivo, EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace( "path_archivo"+path_archivo, EIGlobal.NivelLog.INFO);
       strDebug += "\npath_cb:" + path_archivo;

       boolean errorFile=false;
       try
         {
			// Copia Remota
			EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia(nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &error de remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

            File drvSua = new File(path_archivo);
            RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
            trama_salidaRedSrvr = fileSua.readLine();
            retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
			EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);

            if (retCodeRedSrvr.equals("OK"))
             {
               while((registro = fileSua.readLine()) != null){
               		/////
					if( registro.trim().equals(""))
						registro = fileSua.readLine();
					if( registro == null )
						break;
                  listaBeneficiarios += "<option value=\"" + EIGlobal.BuscarToken(registro, ';', 1) + "\"> " + EIGlobal.BuscarToken(registro, ';', 1) +"   "+ EIGlobal.BuscarToken(registro, ';', 2) +"</option>\n";
                }
             }
			 else
			 {
				// listaBeneficiarios +="Operaci&oacute;n no disponible por el momento. Intente m&aacute;s tarde!. ";
				listaBeneficiarios += trama_salidaRedSrvr;
				//EXISTE_ERROR =1 ;
				err[0] = 1;
			 }
            fileSua.close();
         }catch(Exception ioeSua )
            {
			   EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  Excepci�n en listarBeneficiarios() >>"+ioeSua.getMessage()+"<<",1 );
               //listaBeneficiarios += ioeSua.getMessage();
			   //EXISTE_ERROR = 1;
            }
      EIGlobal.mensajePorTrace( "***ChesBenefAlta.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
      return(listaBeneficiarios);
   }*/
}