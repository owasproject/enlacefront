/******************************************************************************/
/** Clase: BaseServlet
 */

package mx.altec.enlace.servlets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.io.*;
import java.text.*;

import javax.servlet.http.*;
//import javax.servlet.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;

import java.sql.*;

import javax.sql.*;
import javax.naming.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.passmarksecurity.PassMarkDeviceSupportLite;

import mx.altec.enlace.beans.ArchivoNominaBean;
import mx.altec.enlace.beans.ChallengeBean;
import mx.altec.enlace.beans.FavoritosEnlaceBean;
import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.Base64;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaDatosPersonaBO;
import mx.altec.enlace.bo.EmailCelularBean;
import mx.altec.enlace.bo.EpymeBO;
import mx.altec.enlace.bo.InterpretaCEAV;
import mx.altec.enlace.bo.TramasCE;
import mx.altec.enlace.bo.WSBloqueoToken;
import mx.altec.enlace.dao.EmailCelularDAO;
import mx.altec.enlace.dao.EmailDetails;

import mx.altec.enlace.dao.FavoritosEnlaceDAO;
import mx.altec.enlace.dao.FavoritosEnlaceNominaDAO;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.FavoritosConstantes;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.MultipartRequest;
import mx.altec.enlace.utilerias.NominaConstantes;
import mx.altec.enlace.servicios.ServiceLocator;
import mx.altec.enlace.utilerias.UtilidadesChallengeRSA;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.isban.rsa.Exception.BusinessException;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;


//import com.vasco.utils.*;
import java.security.*;
import java.net.*;
import mx.altec.enlace.utilerias.FielConstants;

//VSWF
/**
 * HD1000000134656 - Se agregan validaciones para validar que solo existe una sesion
 * activa por medio del Id de sesion....
 */


public class BaseServlet extends HttpServlet
{
	/**
	 * Atributo estatico y constante para establecer la base a la cual sera
	 * convertida el codigo de cliente.
	 */
    public static final int BASE = 32;
    /**
     * Constante para la literal "operacion"
     */
    private static final String OPERACION = "operacion";
    /**
     * Constante para la literal "strCheck"
     */
    private static final String STRCHECK = "strCheck";
    /**
     * Constante para la literal "folio"
     */
    private static final String FOLIO = "folio";
    /**
     * Constante para la literal "txtFav"
     */
    private static final String TXTFAV = "txtFav";
    /**
     * Constante para la literal "horario_seleccionado"
     */
    private static final String HORARIO_SELECCIONADO = "horario_seleccionado";
    /**
     * Constante para la literal "urlCancelar"
     */
    private static final String URLCANCELAR = "urlCancelar";

    /** Simbolo del signo igual para no repetir constantes. */
    private static final String SIGNO_IGUAL = "=";

    /** Ruta base por defecto. */
    private static final String CONTEXT_PATH_ENLACE = "/Enlace/";

    /** Constante CADENA_VACIA */
    private static final String CADENA_VACIA = "";

    /** Constante  FILE_NAME  parametro nombre de archivo*/
    private static final String FILE_NAME = "fileName";

    /** Constante PARAM_SESSION parametro de sesion*/
    private static final String PARAM_SESSION = "session";

    /** Constante CONTEXT_PATH_ENLACEMIG Ruta base por defecto. */
    private static final String CONTEXT_PATH_ENLACEMIG = "/EnlaceMig";

    /** Constante PARAM_REFERER parametro referer*/
    private static final String PARAM_REFERER = "referer";

    /** Constante PARAM_FLUJO_IGUAL  de flujo*/
    private static final String PARAM_FLUJO_IGUAL = "flujo=";

    /** Constante PARAM_ENROLAR de enrolamiento */
    private static final String PARAM_ENROLAR = "enrolar";

    /** Constante PARAM_FECHA parametro de Fecha. */
    private static final String PARAM_FECHA = "Fecha";

	/**
	 * FACULTADNLN FACULTAD NOMINA EN LINEA
	 */
	static final String FACULTADNLN="INOMPAGLINE";
	/** Atributo estatico para definir login. */
	static boolean boolLogin = false;
	/** Atributo estatico para  */
	static long ultimaFecha;
	/** Atributo para almacenar las URLS del menu. */
	String [][] arregloLocations;//Arreglo de URLS de menu
	/** Atributo para almacenar codigo de menu. */
	String codeMenu;
	/** Atributo para almacenar tipo de divisa. */
	String tipo_divisa = "";
	/** Atributo para almacenar el modulo. */
	String modulo = "";
	/** Atributo para almacenar el la descripcion del contrato. */
	String nomContrato = "";
	/** Atributo para almacenar mensaje de error de Micrositio. */
	String msgErrorMicrositio = "";
	/** Atributo para almacenar codigo de error de Micrositio. */
	String codErrorMicrositio = "";
    /*private static DataSource ds;
    private static DataSource ds2;
    private static DataSource dsSnetComercios;*/
    // <vswf meg>
    /** Atributo para almacenar datos correspondientes a sesion del cliente. */
    mx.altec.enlace.bo.BaseResource session = null; // 09102002
    // <vswf meg>
    // Our session proxy
    //public EnlaceMig.BaseResource session; 09102002
    //Initializes the servlet.


/********************************************/
/** Metodo: init
 * @param config : ServletConfig -> config
 * @throws ServletException : Excepcion de servlet
 */
/********************************************/
     public void init (ServletConfig config)
     throws ServletException {
		 super.init (config);
		 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
		 //java.util.Enumeration enum1 = config.getInitParameterNames ();
		 /*while (enum1.hasMoreElements ()) {
		     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + enum1.nextElement (),EIGlobal.NivelLog.DEBUG);
		 }*/
		 //enum1 = config.getServletContext ().getAttributeNames ();
		 /*while (enum1.hasMoreElements ()) {
		     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + enum1.nextElement (),EIGlobal.NivelLog.DEBUG);
		 }*/
		 //enum1 = config.getServletContext ().getInitParameterNames ();
		 /*while (enum1.hasMoreElements ()) {
		     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + enum1.nextElement (),EIGlobal.NivelLog.DEBUG);
		 }*/
     }
	/*public BaseResource getSessionProxy()
	{
		return session;
	}*/

/********************************************/
/** Metodo: createiASConn
 * @param dbName: String -> dbName
 * @return Connection
 * @throws SQLException : Excepcion al ejecutar conexion.
 */
/********************************************/
     public Connection createiASConn ( String dbName )
     throws SQLException {
	 	return createiASConn_static (dbName);
     }

     /**
      * Metodo estatitco para realizar conexion a base de datos.
      *
      * @param dbName : Cadena con el dbName.
      * @return Connection : Objeto con la conexion establecida.
      * @throws SQLException : Excepcion al ejecutar conexion.
      */
     public static Connection createiASConn_static ( String dbName )
     throws SQLException {

		 Connection conn = null;
		 ServiceLocator servLocator = ServiceLocator.getInstance();
		 DataSource ds = null;
		 try {
			 ds = servLocator.getDataSource(dbName);
		 } catch(NamingException e) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">, mensaje: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		 }
		 if(ds == null) {
			 EIGlobal.mensajePorTrace("Error al obtener conexion para <" + dbName + ">",EIGlobal.NivelLog.ERROR);
			 return null;
		 }
		 conn = ds.getConnection ();
		 return conn;
     }

	// Everis - Nuevo metodo para DataSource [enlacewebgfss]
     /**
      * Metodo estatico para invocar y realizar la conexion a base de datos.
      *
      * @param dbName : Cadena con el dbName.
      * @return Connection  : Objeto con la conexion establecida.
      * @throws SQLException : Excepcion al ejecutar conexion.
      */
     public static Connection createiASConn_static2 ( String dbName )
     throws SQLException {
	 	return createiASConn_static (dbName);
     }

     /**
      * Metodo que abre conexi�n en base de datos de supernet comercios.
      *
      * @param dbName the db name
      * @return Connection
      * @throws SQLException the SQL exception
      * @throws NamingException the naming exception
      */
     public static Connection createConnSnetComercios ( String dbName )
     throws SQLException, NamingException {
	 	return createiASConn_static (dbName);
     }

/********************************************/
/** Metodo: getFileInBytes
 * @param HttpServletRequest -> request
 * @param String -> sNombreArchivo
 * @return byte[]
      * @throws IOException Signals that an I/O exception has occurred.
      * @throws SecurityException the security exception
 */
/********************************************/
     public byte[] getFileInBytes ( HttpServletRequest request, String sNombreArchivo )
     throws IOException, SecurityException {

    if(getFormParameter2(request,"fileName") == null)
    {
    	MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR, 2097152 );
    }

	 FileInputStream entrada;
	 String sFile = sNombreArchivo.substring (sNombreArchivo.lastIndexOf ( "\\" ) + 1 , sNombreArchivo.length () );
	 File fArchivo = new File ( ( IEnlace.LOCAL_TMP_DIR + "/" + sFile ).intern () );
	 byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
	 entrada = new FileInputStream ( fArchivo );
	 byte bByte = 0;
		 try{
	 for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
	     aBArchivo[ i ] = bByte;
	 }
		 }catch(Exception e) {
			 throw new IOException();
		 } finally {
		 entrada.close ();
		     fArchivo.delete ();
		 }
	 return aBArchivo;
     }



   /**
    *   Metodo temporal para unificar manejo de logs
    * @param msg the msg
    *   @deprecated Ya no se debe utilizar
    */
    @Deprecated
    public void log(String msg) {
        EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + msg,EIGlobal.NivelLog.DEBUG);
    }

/********************************************/
/** Metodo: getFileInBytes
 * @param HttpServletRequest -> request
 * @return byte[]
      * @throws IOException Signals that an I/O exception has occurred.
      * @throws SecurityException the security exception
 */
/********************************************/
     public byte[] getFileInBytes ( HttpServletRequest request )
     throws IOException, SecurityException {
	 String sFile = "";
	 FileInputStream entrada;
	 if(getFormParameter2(request,"fileName")== null)
	 {
	 EIGlobal.mensajePorTrace("BaseServlet - getFileInBytes - Subiendo archivo por MultipartRequest",EIGlobal.NivelLog.DEBUG);
	 MultipartRequest mprFile = new MultipartRequest ( request, IEnlace.LOCAL_TMP_DIR, 2097152 );

	 Enumeration files = mprFile.getFileNames ();
	 if( files.hasMoreElements () ) {
	     String name = ( String ) files.nextElement ();
	     sFile = mprFile.getFilesystemName ( name );
	     /*VSWF-HGG-I*/
	     if ("ON".equals(Global.USAR_BITACORAS)){
	     request.getSession().setAttribute("ArchImporta",sFile);
	     }
	     /*VSWF-HGG-F*/
	 }
	 }else
	 {
		 EIGlobal.mensajePorTrace("BaseServlet - getFileInBytes - Ya fue subido por ServletFileUpload",EIGlobal.NivelLog.DEBUG);
		 sFile = getFormParameter2(request,"fileName");
		 request.getSession().setAttribute("ArchImporta",sFile);
	 }

	 File fArchivo = new File ( ( IEnlace.LOCAL_TMP_DIR + "/" + sFile ).intern () );
	 byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
	 entrada = new FileInputStream ( fArchivo );
	 byte bByte = 0;
	 try{
	 for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
	     aBArchivo[ i ] = bByte;
	 }
		 } catch(Exception e) {
			 throw new IOException();
	 } finally {
		 entrada.close ();
		     fArchivo.delete ();
		 }
	 return aBArchivo;
     }

/********************************************/
/** Metodo: getBinaryFile
 * @return boolean
 * @param HttpServletRequest -> request
 * @param String -> sPathArchivo
      * @throws IOException Signals that an I/O exception has occurred.
      * @throws SecurityException the security exception
 */
/********************************************/
     public boolean getBinaryFile ( HttpServletRequest request, String sPathArchivo)
     throws IOException, SecurityException {
	 boolean result = false;
	 MultipartRequest mprFile = new MultipartRequest ( request, sPathArchivo, 2097152 );
	 if (mprFile != null)
	     result = true;
	 return result;
     }

/********************************************/
/** Metodo: Description
 * @return String
 * @param String -> key
 */
/********************************************/
     public String Description ( String key ) {
	 Hashtable GetMovDescription = new Hashtable ();
	 if(key == null)
	     return "";
	 try {
	     GetMovDescription.put ("SDCT", "Cons. Saldo Efectivo");
	     GetMovDescription.put ("SDSI", "Cons. Saldo Fondos");
	     GetMovDescription.put ("SDCR", "Cons. Saldo Tarjeta");
	     GetMovDescription.put ("MOVS", "Cons. Movimientos");
	     GetMovDescription.put ("MOVV", "Cons. Movtos. Vista");
	     GetMovDescription.put ("MOVD", "Cons. Motos. Devoluciones");
	     GetMovDescription.put ("MOVT", "Cons. Movtos. T.C.");
	     GetMovDescription.put ("POSI", "Cons. Posici&oacute;n");
	     GetMovDescription.put ("AMBI", "Act. del Ambiente");
	     GetMovDescription.put ("PASS", "Cambio de Contrase&ntilde;a");
	     GetMovDescription.put ("INDI", "Cons. &Iacute;ndice BMV");
	     GetMovDescription.put ("PHOY", "Cons. Emisora(d&iacute;a)");
	     GetMovDescription.put ("PMES", "Cons. Emisora(mes)");
	     GetMovDescription.put ("INNO", "Cons. Noticias");
	     GetMovDescription.put ("NOTI", "Cons. una Noticia");
	     GetMovDescription.put ("MDIN", "Cons. Mdo. de Dinero");
	     GetMovDescription.put ("TACP", "Cons. Tasas Cta. Personal");
	     GetMovDescription.put ("ENME", "Env&iacute;o Mensaje");
	     GetMovDescription.put ("AVIS", "Cons. Avisos");
	     GetMovDescription.put ("TCAM", "Cons. Tipos de Cambio");
	     GetMovDescription.put ("MENS", "Cons. de Mensajes");
	     GetMovDescription.put ("INFL", "Cons. Inflaci&oacute;n");
	     GetMovDescription.put ("CPSI", "Compra Soc. de Inversi&oacute;n");
	     GetMovDescription.put ("VTSI", "Venta Soc. de Inversi&oacute;n");
	     GetMovDescription.put ("TRAN", "Transferencia Efectivo");
	     GetMovDescription.put ("CAIB", "Dep&oacute;sito Interbancario");
	     GetMovDescription.put ("PAGT", "Dep&oacute;sito de Tarjeta");
	     GetMovDescription.put ("DICR", "Disposici&oacute;n Cr&eacute;dito");
	     GetMovDescription.put ("CATA", "Act. de Cat&aacute;logos");
	     GetMovDescription.put ("PROG", "PROG de Operaciones");
	     GetMovDescription.put ("CPRO", "Cancelaci&oacute;n Operaci&oacute;n");
	     GetMovDescription.put ("ABIT", "Act. de Bit&aacute;cora");
	     GetMovDescription.put ("CPDI", "Cheques a Inversi&oacute;n Vista");
	     GetMovDescription.put ("CPDV", "Cheques a Inversi&oacute;n a Plazo");
	     GetMovDescription.put ("CPID", "Inversi&oacute;n Vista Cheques");
	     GetMovDescription.put ("CPVD", "Inversi&oacute;n a Plazo Cheques");
	     GetMovDescription.put ("CPCC", "Cons. Contr. Plazo");
	     GetMovDescription.put ("PSER", "Pago de Servcio");
	     GetMovDescription.put ("RETJ", "Retiro de Tarjeta");
	     GetMovDescription.put ("TRTJ", "Retiro de Tarjeta");
	     GetMovDescription.put ("CPCI", "Cambio de Instrucci&oacute;n");
	     GetMovDescription.put ("DIBC", "Cotizaci&oacute;n Interbancaria");
	     GetMovDescription.put ("DIBT", "Pago Intebancario");
	     GetMovDescription.put ("DIBA", "Consulta Pago Interbancario");
	     GetMovDescription.put ("RE03", "Pago Cuota IMSS");
	     GetMovDescription.put ("RG01", "Consulta Saldo Cr&eacute;dito");
	     GetMovDescription.put ("RG02", "Movimientos de Cr&eacute;dito");
	     GetMovDescription.put ("RG03", "Prepago Cr&eacute;dito");
	     GetMovDescription.put ("RPIB", "Recuperaci&oacute;n Interbancarios");
	     GetMovDescription.put ("ALCT", "Alta de Cuentas");
	     GetMovDescription.put ("BACT", "Baja de Cuenta");
		 /*
		  * Mejoras de Linea de Captura
		  * Se modifica la leyenda LC por Pago Referenciado SAT para la Bitacora
		  * Inicia RRR - Indra Marzo 2014
		  */
	     GetMovDescription.put ("PILC", "Pago de Impuestos por Pago Referenciado SAT");
	     /*
	      * Fin RRR - Indra Marzo 2014
	      */
	     GetMovDescription.put ("PIMP", "Pago de Impuestos");
	     GetMovDescription.put ("CIMP", "Cancelaci&oacute;n de Impuestos");
	     GetMovDescription.put ("CONI", "Consulta de Impuestos");
	     GetMovDescription.put ("INGC", "Ing. cheque seg.");
	     GetMovDescription.put ("INGA", "Ing. Arch cheques seg.");
	     GetMovDescription.put ("ERRO", "Operaci&oacute;n no Ejecutada");
	     GetMovDescription.put ("TI01", "Alta Estruct. Teso");
	     GetMovDescription.put ("TI02", "Consulta Estruct. Teso");
	     GetMovDescription.put ("TI03", "Baja Estruct. Teso");
	     GetMovDescription.put ("TI04", "Mod. Estruct. Teso");
	     GetMovDescription.put ("TI05", "Cons. Hist. Teso");
	     GetMovDescription.put ("TRAF", "Envio Archivo R&aacute;faga");
	     GetMovDescription.put ("CRAF", "Consulta Archivo R&aacute;faga");
	     GetMovDescription.put ("ACMA", "Act. de Firmas Mancomunadas");
	     GetMovDescription.put ("CAMA", "Cancelaci&oacute;n de Oper. Mancomunadas");
	     GetMovDescription.put ("ESMA", "Oper. Especiales Mancomunadas");
	     GetMovDescription.put ("IN07", "Act. Empleados N&oacute;mina Santander");
	     GetMovDescription.put ("IN09", "Rec. Arch. Empleados N&oacute;mina Santander");
	     GetMovDescription.put ("IN04", "Pago Nomina Santander");
	     GetMovDescription.put ("CFAC", "Act. Cat&aacute;logos Confirming");
	     GetMovDescription.put ("CFAR", "Actualizaci&oacute;n de Archivos de proveedores");
	     GetMovDescription.put ("CFAP", "Act. Cat&aacute;logos Generales");
	     GetMovDescription.put ("CFAE", "Act. Archivos de proveedores");
	     GetMovDescription.put ("CFRP", "Recepci&oacute;n de Archivo Pagos");
	     GetMovDescription.put ("CFRR", "Recepci&oacute;n de Archivo de proveedores");
	     GetMovDescription.put ("CFMR", "Modificaci&oacute;n de un proveedor");
	     GetMovDescription.put ("CFCP", "Cancelaci&oacute;n de Pago");
	     GetMovDescription.put ("CFEP", "Recuperaci&oacute;n de Archivos de Pago");
	     GetMovDescription.put ("CFER", "Recuperaci&oacute;n de Archivos de proveedores");
	     GetMovDescription.put ("CFDP", "Cons. Datos proveedor");
	     GetMovDescription.put ("CFTF", "Ac. Tasas de Factoraje");
	     GetMovDescription.put ("CFPB", "Baja Cuenta Impuestos");
	     GetMovDescription.put ("CFCA", "Cotizaci&oacute;n Doctos.");
	     GetMovDescription.put ("CFPC", "Atc. Doctos. Confirming");
	     GetMovDescription.put ("AAOP", "Alta de Archivo Pago Directo");
	     GetMovDescription.put ("CBOP", "Cons. de Benef. Pago Directo");
	     GetMovDescription.put ("CROP", "Cons Pagos Pago Directo");
	     GetMovDescription.put ("COOP", "Cons. Pagos Pago Directo");
	     GetMovDescription.put ("OAOP", "Cons. Sucuarsales Pago Directo");
	     GetMovDescription.put ("ALOP", "Reg. L&iacute;nea Pago Directo");
	     GetMovDescription.put ("ABOP", "Alta Benef. Pago Directo");
	     GetMovDescription.put ("BBOP", "Baja Benef. Pago Directo");
	     GetMovDescription.put ("MBOP", "Modif. BEnef. Pago Directo");
	     GetMovDescription.put ("AROP", "Alta/Baja Rel. Ctas. Pago Directo");
	     GetMovDescription.put ("CAOP", "Cancelaci&oacute;n Pago Directo");
	     GetMovDescription.put ("ABRE", "Alata/Baja Relaci&oacute;n Cheque Seg.");
	     GetMovDescription.put ("ACRE", "Actualizaci&oacute;n Relaci&oacute;n Cheque Seg.");
	     GetMovDescription.put ("CBEN", "Actualizaci&oacute;n Beneficiarios Cheques Seg.");
	     GetMovDescription.put ("CCOH", "Consulta de Cheques Seg.");
	     GetMovDescription.put ("RE01", "Obtiene Referencia del SUA");
	     GetMovDescription.put ("RE04", "Consulta de SUA");
	     GetMovDescription.put ("REFC", "Obtiene Referencia de Cheque Seg.");
	     GetMovDescription.put ("SEOP", "Secuencia de Archivo Orden de Pago");
	     GetMovDescription.put ("ARIM", "Envio de Declaraci&oacute;n de Pago de Impuestos");
	     GetMovDescription.put ("ACIM", "Actualizaci&oacute;n de Pago de Impuestos");
	     GetMovDescription.put ("IN05", "Ingreso de Pago N&oacute;mina");
	     GetMovDescription.put ("IN06", "Invern&oacute;mina");
	     GetMovDescription.put ("DIBP", "Pago Interbancario");
	     GetMovDescription.put ("IN02", "Alta de Empleados de Invern?ina");
	     GetMovDescription.put ("ABEN", "Alta y Baja de Beneficiario");
	     GetMovDescription.put ("AIMP", "Alta de cuenta de pago de impuestos");
	     GetMovDescription.put ("BIMP", "Baja de cuenta de pago de imp.");
	     GetMovDescription.put ("CACH", "Cancelaci&oacute;n de cheques Seg.");
	     GetMovDescription.put ("COCH", "Consulta de cheq. Seg.");
	     GetMovDescription.put ("RE02", "Elimina un archivo incompleto de SUA");
	     GetMovDescription.put ("IN01", "Aviso del n?ero de registros a enviar");
	     GetMovDescription.put ("IN03", "Verifica num reg insertados en Invern&oacute;mina");
	     GetMovDescription.put ("IN08", "Invern&oacute;mina");
	     GetMovDescription.put ("MBEN", "Modificaci&oacute;n de Beneficiarios de cheques Seg.");
	     GetMovDescription.put ("TF01", "Tesorer&iacute;a de la Fed");
	     GetMovDescription.put ("BBEN", "Baja beneficiario cheq. Seg.");
	     GetMovDescription.put ("ERAF", "Ejecuta Trans. Rafaga");
	     GetMovDescription.put ("MAXB", "Saldos");
	     GetMovDescription.put ("MAXT", "Saldos");
	     GetMovDescription.put ("MAXC", "Saldos");
	     GetMovDescription.put ("CCTR", "Cambio de Contrato");
	     GetMovDescription.put ("TI01", "Alta Estruct. Teso");
	     GetMovDescription.put ("TI02", "Consulta Estruct. Teso");
	     GetMovDescription.put ("TI03", "Baja Estruct. Teso");
	     GetMovDescription.put ("TI04", "Mod. Estruct. Teso");
	     GetMovDescription.put ("TI05", "Cons. Hist. Teso");
	     // se le adiciono el 12072002 jahh
	     GetMovDescription.put ("COMI", "Consulta de comisiones cobradas");
	     GetMovDescription.put ("OCUR", "Ordenes de pago ocurre");
	     GetMovDescription.put ("MULT", "Multicheque");
	     GetMovDescription.put ("DICO", "Cotizaci&oacute;n Internacional");
	     GetMovDescription.put ("DITA", "Transferencia Internacional");
	     GetMovDescription.put ("DIPD", "Cambios(Transferencia MN-Dlls / Dlls-MN");
	     GetMovDescription.put ("DIIF", "Consulta Transferencias Internacionales");
	     GetMovDescription.put ("DIAE", "Alta Ctas. Internacionales");
	     GetMovDescription.put ("DIBE", "Baja Ctas. Internacionales");
	     GetMovDescription.put ("DICE", "Consulta Ctas. Internacionales");
	     GetMovDescription.put ("DIDV", "Cheques a Inversi&oacute;n a Plazo D&oacute;lar");
	     GetMovDescription.put ("SATP", "Nuevo pago de impuestos");
		 // Claves para Norma43
		 GetMovDescription.put ("APRM", "Alta de Registro de Consultas Programadas");
	     GetMovDescription.put ("CPRV", "Consultas de Programaciones Vigentes");
	     GetMovDescription.put ("MPRM", "Modificaci&oacute;n de Programaciones Vigentes");
	     GetMovDescription.put ("CPRM", "Cancelaci&oacute;n de Consultas Programadas");
	     GetMovDescription.put ("CREP", "Resultado de Consultas Programadas");
	     GetMovDescription.put ("CMPR", "Consulta para Modificaci&oacute;n de Programaciones");
		 //Claves en comisiones GSV 13/10/2003
	     GetMovDescription.put ("RAOP", "Registro de Archivo de Pago Directo");
	     GetMovDescription.put ("RACH", "Registro de Archivo de Chequera Seguridad");
	     GetMovDescription.put ("TICO", "Concentraci&oacute;n de Tesoreria Inteligente");
	     GetMovDescription.put ("TIDI", "Dispersi&oacute;n de Tesoreria Inteligente");
	     GetMovDescription.put ("FOBC", "Fondeo Estructura de Base Cero");
	     GetMovDescription.put ("CCCP", "Registro Consultas Programadas");
		 GetMovDescription.put ("ACCL", "Alta de cuenta CLABE");

		 // Clave-Descripcion de Pago SAR, Getronics 22/12/2004
		 GetMovDescription.put ("SARA", "Pago de SAR");
		 GetMovDescription.put ("SARB", "Consulta de Pago SAR");
		 GetMovDescription.put ("SARC", "Cancelaci&oacute;n de Pago SAR");
		 GetMovDescription.put ("SARD", "Consulta de Dependencia");
		 GetMovDescription.put ("SARE", "Consulta/Reimpresi&oacute;n de Comprobante SAR");

		 //Validacin de duplicidades impuestos Getronics 27/12/2004
		 GetMovDescription.put ("VAPA", "Validaci&oacute;n Duplicidad Impuestos Federales");
		 GetMovDescription.put ("VAPS", "Validaci&oacute;n Duplicidad SAT");

		 /*******************************************************/
		 /* TESOFE						*/
		 /*******************************************************/
		 GetMovDescription.put ("TF06","TESOFE Consulta de Recaudaci&oacute;n General");
		 GetMovDescription.put ("TF07","TESOFE Consulta de Recaudaci&oacute;n Detalle");
		 GetMovDescription.put ("TF08","TESOFE Consulta de Detalle para Sucursales");
		 GetMovDescription.put ("TD07","TESOFE Env&iacute;o de Archivos");
		 GetMovDescription.put ("TD08","TESOFE Consulta de Archivos");
		 GetMovDescription.put ("TD09","TESOFE Env&iacute;o de Detalle de Archivos");
		 GetMovDescription.put ("TD10","TESOFE Cancelaci&oacute;n de Archivos");
		 GetMovDescription.put ("TD11","TESOFE Consulta de Registros");
		 /*******************************************************/

		 /*******************************************************/
		 /* NUEVO MODULO ALTA DE CUENTAS                        */
		 /*******************************************************/
		 GetMovDescription.put ("CT01","Alta de Cuentas");
		 GetMovDescription.put ("CT02","Baja de Cuentas");
		 GetMovDescription.put ("CT03","Consulta de Cuentas");
		 GetMovDescription.put ("CT04","Autorizaci&oacute;n de Alta y Baja de Cuentas");
		 GetMovDescription.put ("CT05","Rechazo de Alta y Baja de Cuentas");
		 /*******************************************************/
		 /*********************SALTO ENLACE - SUPERNET COMERCIOS JPH VSWF 22 JUN 2009***********************/
		 GetMovDescription.put ("CCSC","Cambio de Canal Super Comercios");
		 GetMovDescription.put ("CSEN","Cierre de Sesion de Enlace");
		 GetMovDescription.put ("ESEN","Expiraci&oacute;n de Sesion de Enlace");
		 /***********/
		 /*SIPARE - SUA Linea de Captura*/
		 /*******************************************************/
		 GetMovDescription.put ("SULC", "Pago S.U.A. L&iacute;nea de Captura");
		 /***********/
		 /*NUEVO MODULO DE TARJETA PREPAGO*/
		 /*******************************************************/
		 GetMovDescription.put ("CORE", "Consulta de Remesa");
		 GetMovDescription.put ("RCRE", "Recepci�n de Remesa");
		 GetMovDescription.put ("CARE", "Cancelaci�n de remesa");
		 GetMovDescription.put ("ATEI", "Asignaci�n Tarjeta Empleado Individual");
		 GetMovDescription.put ("ATIM", "Asignaci�n tarjeta Empleado Importaci�n");
		 GetMovDescription.put ("CRTE", "Consulta de Relaci�n Tarjeta Empleado");
		 GetMovDescription.put ("BATA", "Baja de Tarjeta");
		 GetMovDescription.put ("BLTA", "Bloqueo de Tarjeta");
		 GetMovDescription.put ("RETA", "Reposici�n de Tarjeta");
		 GetMovDescription.put ("CAAM", "Cancelaci�n de Archivo de Asignaci�n masiva");
		 GetMovDescription.put ("MOTE", "Modificaci�n Tarjeta Empleado");
//		 GetMovDescription.put ("PNOS", "Tarjeta de Pagos Santander");
//		 GetMovDescription.put ("PNIS", "Pago Nomina Individual Santander");
		 GetMovDescription.put ("PNIS", "Pago Tarjeta de Pagos Santander Individual");//YHG NPRE
		 GetMovDescription.put ("PNOS", "Pago Tarjeta de Pagos Santander ");//YHG NPRE
		 GetMovDescription.put ("CPNI", "Consulta Pago de Nomina Individual");
		 GetMovDescription.put ("CNIE", "Consulta Pago de Nomina Importaci�n env�o");
		 GetMovDescription.put ("CAPI", "Cancelaci�n Pago Individual");
		 GetMovDescription.put ("CAMP", "Cancelaci�n masiva de� Pago");

		 /*******************************************************/
		 /* CONCEPTOS � DESCRIPCIONES DE BITACORIZACION DE CONFIRMACION CON TOKEN                       */
		 /*******************************************************/
		 GetMovDescription.put ("GFOL","Generaci&oacute;n de Folio de Mancomunidad");
		 GetMovDescription.put ("PASS","Cambio de Contrase�a");
		 GetMovDescription.put ("MODP","Modificaci&oacute;n de Datos Personales");
		 GetMovDescription.put ("CCNM","Consulta de Cat&aacute;logo de N&oacute;mina Mismo Banco");
		 GetMovDescription.put ("MCNM","Modificaci&oacute;n de Cat&aacute;logo de N&oacute;mina Mismo Banco");
		 GetMovDescription.put ("BCNM","Baja de Cat&aacute;logo de N&oacute;mina Mismo Banco");
		 GetMovDescription.put ("COST","Consulta de Seguimiento de Transferencias");
		 GetMovDescription.put ("CCNI","Consulta de Cat&aacute;logo de N&oacute;mina Interbancaria");
		 GetMovDescription.put ("BCNI","Baja de Cat&aacute;logo de Nomina Interbancaria");
		 GetMovDescription.put ("ACNI","Autorizaci&oacute;n y Cancelaci&oacute;n de Cuentas en N&oacute;mina Interbancaria");
		 GetMovDescription.put ("CMAN","Confirmaci&oacute;n de Operaciones Mancomunadas");
		 GetMovDescription.put ("CGFO","Confirmaci&oacute;n de Generaci&oacute;n de Folio");
		 GetMovDescription.put ("CCTR","Cambio de Contrato");
		 GetMovDescription.put ("CTRA","Confirmaci&oacute;n de transferencia Moneda Nacional");
		 GetMovDescription.put ("CTUS","Confirmaci&oacute;n de transferencia en D&oacute;lares");
		 GetMovDescription.put ("COCM","Confirmaci&oacute;n de Pago Ocurre Personas Morales");
		 GetMovDescription.put ("COCF","Confirmaci&oacute;n de Pago Ocurre F&iacute;sicas");
		 GetMovDescription.put ("COTI","Confirmaci&oacute;n de Transferencias Interbancarias");
		 GetMovDescription.put ("CPTC","Confirmaci&oacute;n de Pago de Tarjeta de Cr&eacute;dito");
		 GetMovDescription.put ("CDTC","Confirmaci&oacute;n Disposici&oacute;n de Tarjeta de Cr&eacute;dito");
		 GetMovDescription.put ("CTIN","Confirmaci&oacute;n de Transferencias Internacionales");
		 GetMovDescription.put ("CSAT","Confirmaci&oacute;n de Pago de Impuestos Provisionales");
		 GetMovDescription.put ("CSAE","Confirmaci&oacute;n de Pago de Impuestos del Ejercicio");
		 GetMovDescription.put ("CSEF","Confirmaci&oacute;n de Pago de Impuestos Entidades Federativas");
		 GetMovDescription.put ("CDPA","Confirmaci&oacute;n de Pago de Impuestos Derechos, Productos y Aprovechamiento");
		 /*
		  * Mejoras de Linea de Captura
		  * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT para la Bitacora
		  * Inicia RRR - Indra Marzo 2014
		  */
		 GetMovDescription.put ("CPLC","Confirmaci"+ (char)243 +"n de Pago de Impuestos por Pago Referenciado SAT");
		 /*
		  * Fin RRR - Indra Marzo 2014
		  */
		 GetMovDescription.put ("CSUA","Confirmaci&oacute;n de Pago de Impuestos SUA");
		 GetMovDescription.put ("CSIP","Confirmaci&oacute;n de pago SUA (SIPARE)");
		 GetMovDescription.put ("CPNS","Confirmaci&oacute;n de Pago de N&oacute;mina Santander");

		 GetMovDescription.put ("CPNI","Confirmaci&oacute;n de Pago de N&oacute;mina Interbancaria");
		 GetMovDescription.put ("CPNP","Confirmaci&oacute;n de Pago de Tarjeta de Pagos Santander ");
		 GetMovDescription.put ("CCHS","Confirmaci&oacute;n de Liberaci&oacute;n de Cheque Seguridad");
		 GetMovDescription.put ("CPAD","Confirmaci&oacute;n de Liberaci&oacute;n de Pago Directo");
		 GetMovDescription.put ("COPP","Confirmaci&oacute;n de Pago a Proveedores");
		 GetMovDescription.put ("CCDI","Confirmaci&oacute;n de Cambio de Divisas");
		 GetMovDescription.put ("CSAR","Confirmaci&oacute;n de Pago SAR");
		 GetMovDescription.put ("CCFI","Confirmaci&oacute;n de Compra de Fondos de Inversi&oacute;n");
		 GetMovDescription.put ("CVFI","Confirmaci&oacute;n de Venta de Fondos de Inversi&oacute;n");
		 GetMovDescription.put ("CCHV","Confirmaci&oacute;n de Cheque Vista");
		 GetMovDescription.put("INTE", "N&oacute;mina Interbancaria");


		 //SuperUsuario
		 GetMovDescription.put ("AUSS","Alta / Vinculacion de Usuarios por Superusuario");
         GetMovDescription.put ("BUSS","Baja / Desvinculacion de Usuarios por Superusuario");
         GetMovDescription.put ("MFUS","Modificacion de Facultades de Usuarios por Superusuario");

         //Transferencias Reuters
		 GetMovDescription.put ("ACRR","Acceso Pactado de Operaciones FX Online");
         GetMovDescription.put ("CTCR","Complemento de Operaciones FX Online");
         GetMovDescription.put ("LOPC","Transferencia de Operaciones FX Online");

         //Cuenta Nivel 3
		 GetMovDescription.put ("INAR","Recepci&oacute;n de Remesas Cuenta Inmediata");
         GetMovDescription.put ("INXR","Cancelaci&oacute;n de Remesas Cuenta Inmediata");
         GetMovDescription.put ("INCR","Consulta de Remesas Cuenta Inmediata");
		 GetMovDescription.put ("INCT","Cancelaci&oacute;n de Tarjetas Cuenta Inmediata");
         GetMovDescription.put ("INAI","Asignaci&oacute;n Individual Cuenta Inmediata");
         GetMovDescription.put ("INAA","Asignaci&oacute;n Por Archivo Cuenta Inmediata");
         GetMovDescription.put ("INAC","Consulta de Asignaci&oacute;n Cuenta Inmediata");
         GetMovDescription.put ("INCF","Cancelaci&oacute;n de Asignaci&oacute;n Cuenta Inmediata");

         //Cuenta Nivel 1
		 GetMovDescription.put ("ALTC","Alta de Vinculaci&oacute;n Tarjeta de Pagos");
         GetMovDescription.put ("COTC","Consulta de Vinculaci&oacute;n Tarjeta de Pagos");
         GetMovDescription.put ("BATC","Baja de Vinculaci&oacute;n Tarjeta de Pagos");

         //Mancomunidad
		 GetMovDescription.put ("MPGT","Dep&oacute;sito de Tarjeta");
         GetMovDescription.put ("VPGT","Dep&oacute;sito de Tarjeta");
         GetMovDescription.put ("PPGT","Dep&oacute;sito de Tarjeta");
		 GetMovDescription.put ("MDIB","Transferencia Interbancaria Mancomunada");
         GetMovDescription.put ("VDIB","Transferencia Interbancaria Verificada");
         GetMovDescription.put ("PDIB","Transferencia Interbancaria Pre-Autorizada");
		 GetMovDescription.put ("MTRA","Transferencia Mismo Banco Mancomunada");
         GetMovDescription.put ("VTRA","Transferencia Mismo Banco Verificada");
         GetMovDescription.put ("PTRA","Transferencia Mismo Banco Pre-Autorizada");
		 //IOV claves Mancomunidad Fase II
		 GetMovDescription.put ("MPNI", "Pago Tarjeta de Pagos Santander Individual");
		 GetMovDescription.put ("VPNI", "Pago Tarjeta de Pagos Santander Individual");
		 GetMovDescription.put ("PPNI", "Pago Tarjeta de Pagos Santander Individual");
		 GetMovDescription.put ("MPNO", "Pago Tarjeta de Pagos Santander ");
		 GetMovDescription.put ("VPNO", "Pago Tarjeta de Pagos Santander ");
		 GetMovDescription.put ("PPNO", "Pago Tarjeta de Pagos Santander ");
		 GetMovDescription.put ("MINT", "N&oacute;mina Interbancaria");
		 GetMovDescription.put ("VINT", "N&oacute;mina Interbancaria");
		 GetMovDescription.put ("PINT", "N&oacute;mina Interbancaria");
		 GetMovDescription.put ("MIN4", "Pago Nomina Santander");
		 GetMovDescription.put ("VIN4", "Pago Nomina Santander");
		 GetMovDescription.put ("PIN4", "Pago Nomina Santander");
		 /**
		  * Claves para Micrositio
		  */
		 GetMovDescription.put ("MMIC","Pago Micrositio");
         GetMovDescription.put ("VMIC","Pago Micrositio");
         GetMovDescription.put ("AMIC","Pago Micrositio");
         GetMovDescription.put ("PMIC","Pago Micrositio");

		 GetMovDescription.put ("CPMS","Confirmaci&oacute;n de Pago Micrositio");
		 GetMovDescription.put ("CIMS","Confirmaci&oacute;n de Ingreso Micrositio");

         //Nomina en linea
	 GetMovDescription.put ("LNPI","Importaci&oacute;n Pago N&oacute;mina en L&iacute;nea");
         GetMovDescription.put ("CONL","Confirmaci&oacute;n de N&oacute;mina en L&iacute;nea");
         GetMovDescription.put ("LNPC","Consulta de N&oacute;mina en L&iacute;nea");
         GetMovDescription.put ("PNLI","Pago de N&oacute;mina en L&iacute;nea");
       //Nomina en linea - Mancomunidad
         GetMovDescription.put ("MNLI","Pago N&oacute;mina en L&iacute;nea Mancomunada");
         GetMovDescription.put ("VNLI","Pago N&oacute;mina en L&iacute;nea Verificada");
         GetMovDescription.put ("PANL","Pago N&oacute;mina en L&iacute;nea Pre Autorizada");
       //PORTAL NEGOCIO INTERNACIONAL
         GetMovDescription.put ("PCIM","Entrada Portal de Negocio Internacional");

         // Claves de Operacion Estados de Cuenta PDF y XML
         GetMovDescription.put("CECI", BitaConstants.CONCEPTO_CONF_CONFIG_EDO_CUENTA_IND);
         GetMovDescription.put("ACAP", BitaConstants.CONCEPTO_ACEPTA_CONV_PAPERLESS);
         GetMovDescription.put("BICP", BitaConstants.CONCEPTO_BAJA_PAPERLESS);
         GetMovDescription.put("IECP", BitaConstants.INSCRIPCION_EDO_CUENTA_PAPERLESS);
         GetMovDescription.put("CECM", BitaConstants.CECM);
         GetMovDescription.put("CMED", BitaConstants.CMED);
         GetMovDescription.put("CCON", BitaConstants.CONCEPTO_CONSULTA_EDO_CUENTA);
         GetMovDescription.put("CDEC", BitaConstants.TOKEN_VALIDO_DESCARGA_EDO_CTA);
         GetMovDescription.put("DEDC", BitaConstants.CONCEPTO_DESCARGA_EDO_CTA);
         GetMovDescription.put("CSPH", BitaConstants.CONCEPTO_TOKEN_VALIDO_SOL_EDO_HIST);
         GetMovDescription.put("DEDA", BitaConstants.CONCEPTO_SOL_EDO_HIST);
         GetMovDescription.put("CONH", BitaConstants.CONCEPTO_CONSULTA_HIST);

		 //Importacion de Archivos Cifrados
         GetMovDescription.put ("IMCD","AC Importaci&oacute;n de Certificado Digital");
         GetMovDescription.put ("RCCE","AC Revocaci&oacute;n de Certificado Cliente");
         GetMovDescription.put ("DCBC","AC Descarga de Certificado Banco");
         GetMovDescription.put ("CCCE","AC Consulta Certificado Cliente");
       //Importaci�n de Archivos Cifrados - Flujos Enlace
         GetMovDescription.put ("ACTN","AC Transferencias Internas M.N");
         GetMovDescription.put ("ACTD","AC Transferencias Internas DLLS.");
         GetMovDescription.put ("ACTB","AC Transferencias Interbancarias");
         GetMovDescription.put ("ACPT","AC Pagos Tarjeta Cr&eacute;dito");
         GetMovDescription.put ("ACAE","AC Alta de Empleados");
         GetMovDescription.put ("ACPN","AC Pago de N&oacute;mina");
         GetMovDescription.put ("ACNI","AC Pago de N&oacute;mina Interbancaria");
         GetMovDescription.put ("ACAP","AC Alta Proveedores");
         GetMovDescription.put ("ACPP","AC Pago Proveedores");
         GetMovDescription.put ("ACPD","AC Pago Directo");
         GetMovDescription.put ("ACOF","AC Pago Ocurre F&iacute;sicas");
         GetMovDescription.put ("ACOM","AC Pago Ocurre Morales");
         GetMovDescription.put ("ACCH","AC Chequera de Seguridad");
         GetMovDescription.put ("ACTE","AC Relaci&oacute;n Tarjeta Empleado");
         GetMovDescription.put ("ACPI","AC Pago Tarjeta Inmediata");
         GetMovDescription.put ("ACAN","AC Alta Empleado Catalogo de N&oacute;mina");
         GetMovDescription.put ("ACBN","AC Baja Empleado Catalogo de N&oacute;mina");
         GetMovDescription.put ("ACMN","AC Modificaci&oacute;n Empleado Catalogo de N&oacute;mina");
         GetMovDescription.put ("ACAT","AC Asignaci&oacute;n Tarjeta Empleado");
         GetMovDescription.put ("ACAI","AC Alta de Empleados N&oacute;mina Interbancaria");
         GetMovDescription.put ("ACAC","AC Alta de Cuentas");
         GetMovDescription.put ("ACNL","AC Nomina en Linea");
			// Pago referenciado SAT por Micrositio
			GetMovDescription.put("MMRF", "Pago Referencioado SAT por Micrositio");
			GetMovDescription.put("VMRF", "Pago Referencioado SAT por Micrositio");
			GetMovDescription.put("PARF", "Pago Referencioado SAT por Micrositio");
			GetMovDescription.put("PMRF", "Pago Referencioado SAT por Micrositio");

			/************************************************************/
			/* Claves de Operaci�n para Validaci�n de L�nea de Captura  */
			/************************************************************/
			GetMovDescription.put ("WTRA","Validaci�n LC Transferencia Moneda Nacional");
			GetMovDescription.put ("WPMI","Validaci�n LC Transferencia Moneda Nacional por Micrositio");
			GetMovDescription.put ("WPRO","Validaci�n LC Transferencia Moneda Nacional Programada");
			//Consulta de Transferencias Interbancarias Recibidas
			GetMovDescription.put ("CTIR","Consulta de Transferencias Interbancarias Recibidas");
			//*******************INICIA TCS FSW 12/2016***********************************	
			//Consulta de Transferencias Interbancarias Recibidas en Dolares
			GetMovDescription.put ("CTIRD","Consulta de Transferencias Interbancarias Recibidas D&oactute;lares");
			//*******************FIN TCS FSW 12/2016***********************************
			GetMovDescription.put ("CTIT","Consulta de Transferencias Internacionales Recibidas");
			/**Inicio modificacion jorge olalde**/
			GetMovDescription.put ("ECBE","Consulta de Cuentas Santander");
			GetMovDescription.put ("EPBA","Entrada al portal Banxico");
			/**Fin modificacion jorge olalde**/

			//MODULOS DE FIEL
			GetMovDescription.put("FIEA", "Registro de FIEL");
			GetMovDescription.put("FIEB", "Baja de FIEL");
			GetMovDescription.put("FIEM", "Modificacion de FIEL");
	 } catch( Exception e ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     return null;
	 }
	 String regreso = "";
	 regreso = ( String ) GetMovDescription.get ( key );
	 if(regreso ==null)
	     regreso = "";
	 return ( regreso );
     }

/********************************************/
/** Metodo: FormatoMoneda
 * @return String
 * @param double -> cantidad
 */
/********************************************/
     public String FormatoMoneda ( double cantidad ) {
		 String formato = "";
		 String language = "la"; // la
		 String country = "MX";  // MX
		 Locale local = new Locale (language,  country);
		 NumberFormat nf = NumberFormat.getCurrencyInstance (local);
		 if (cantidad < 0)  {
		     formato = nf.format ( cantidad );
		     try {
		    	 if (!(formato.substring (0,1).equals ("$"))) {
		    		 if (formato.substring (0,3).equals ("(MX")) {
		    			 formato ="$ -"+ formato.substring (4,formato.length () - 1);
		    		 } else {
		    			 if (formato.substring (0,3).equals ("-MX")) {
		    				 formato ="$ -"+ formato.substring (4,formato.length ());
		    			 } else {
		    				 formato ="$ -"+ formato.substring (2,formato.length ());
		    			 }
		    		 }
		    	 }
		     } catch(NumberFormatException e) {}
		 } else {
		     formato = nf.format ( cantidad );
		     try {
			 if (!(formato.substring (0,1).equals ("$"))) {
	    		 if (formato.substring (0,3).equals ("MXN")) {
	    			 formato ="$ "+ formato.substring (3,formato.length ());
	    		 } else {
	    			 formato ="$ "+ formato.substring (1,formato.length ());
	    		 }
			 }
		     } catch(NumberFormatException e) {}
		 }
		 return formato;
     }

/********************************************/
/** Metodo: FormatoMoneda
 * @return String
 * @param String -> cantidad
 */
/********************************************/
     public String FormatoMoneda ( String cantidad ) {
		 String language = "la"; // ar
		 String country = "MX";  // AF
		 Locale local = new Locale (language,  country);
		 NumberFormat nf = NumberFormat.getCurrencyInstance (local);
		 String formato = "";
		 if(cantidad ==null ||cantidad.equals (""))
		     cantidad="0.0";
		 double importeTemp = 0.0;
		 importeTemp = new Double (cantidad).doubleValue ();
		 if (importeTemp < 0) {
		     try {
			 formato = nf.format (new Double (cantidad).doubleValue ());
	    	 if (!(formato.substring (0,1).equals ("$"))) {
	    		 if (formato.substring (0,3).equals ("MXN")) {
	    			 formato ="$ -"+ formato.substring (4,formato.length ());
	    		 } else {
	    			 formato ="$ -"+ formato.substring (2,formato.length ());
	    		 }
	    	 }
		     } catch(NumberFormatException e) {formato="$0.00";}
		 } else {
		     try {
			 formato = nf.format (new Double (cantidad).doubleValue ());
			 if (!(formato.substring (0,1).equals ("$"))) {
	    		 if (formato.substring (0,3).equals ("MXN")) {
	    			 formato ="$ "+ formato.substring (3,formato.length ());
	    		 } else {
	    			 formato ="$ "+ formato.substring (1,formato.length ());
	    		 }
			 }
		     } catch(NumberFormatException e) {
			 formato="$0.00";}
		 } // del else
		 if(formato==null)
		     formato = "";
		 return formato;
     }

/********************************************/
/** Metodo: SesionValida
 * @return boolean
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 * HD1000000134656
 * 		-Se llama a la funcion ValidaSesionDB para validar que el id de sesion no cambie
 *
 */
/********************************************/
     public boolean SesionValida ( HttpServletRequest request, HttpServletResponse response )
     throws ServletException, java.io.IOException {
	 HttpSession sess;
	 boolean resultado = false;
	 String idSession = null;
	 sess = request.getSession ( false );
	 mx.altec.enlace.bo.BaseResource session = null; // 09102002
	/*imprimir header usuario, idsesion,fecha*/
	try	 {
		EIGlobal.mensajePorTrace("Entrando SesionValida  " + ObtenFecha() + ": IV-USER <" + request.getHeader("IV-USER") + ">" + "idsesion<" + (request.getSession() !=null ? request.getSession().getId() : "null") + ">", EIGlobal.NivelLog.INFO);
	}catch(Exception e)
	{
		EIGlobal.mensajePorTrace ("E->SesionValida SAM->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}

	 if ( sess == null ) {
	     request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado introduzca sus datos nuevamente\",3);" );
	     evalTemplate ( IEnlace.LOGIN, request, response );
	     return resultado;
	 }
	 else {
		 idSession = sess.getId();
	     session = (BaseResource) request.getSession ().getAttribute ("session");//new BaseResource ( /*  11/10/2002 sess*/ );
	     if( session == null || // 11/10/2002
		 session.getUserID8 () == null || session.getUserID8 ().equals ("") ) {
		 String nombreArchivoEmp = null;
                 if(session != null) session.getArchivoEmpNom ();
		 if ( nombreArchivoEmp != null) {
		     try {
			 File archivo = new File ( nombreArchivoEmp );
			 if ( archivo.exists () ) archivo.delete ();
		     } catch ( Exception e ) {
		    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E en el objeto File(SesionValida(): "
		    			 + e,EIGlobal.NivelLog.ERROR);
                           EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		     }
		 }
		 if(session != null) session.setArchivoEmpNom ( null );
		 /*sess.removeAttribute ("session");
		 sess.setAttribute ("session",session);*/
		 //request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"C&oacute;digo de Cliente o Contrase&ntilde;a incorrectos\", 3);" );
		 request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado introduzca sus datos nuevamente\", 3);" );

	     } else {

				 session.setUltAcceso(Calendar.getInstance().getTime().getTime());
				 if(session.getTiempoActivo() > (Global.EXP_SESION * 60000)) {
					 //evalTemplate ( IEnlace.LOGIN, request, response );
					 int resDup = cierraDuplicidad(session.getUserID8());
					 session.setSeguir(false);
					 sess.setAttribute("session", session);
					 sess.removeAttribute("session");
					 sess.invalidate();
					 resultado = false;
					 //forzado por SW
					 request.getSession().setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado; introduzca sus datos nuevamente\", 3);" );
					 //response.sendRedirect("/NASApp/enlaceMig/IndexServlet");
					 //return resultado;

					 /*<VC proyecto="200710001" autor="JGR" fecha="13/07/2007" descripcin="USUARIO SAM">*/
					if (request.getHeader("IV-USER")!=null){
						EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
						evalTemplate(IEnlace.LOGIN, request, response);
						return false;
					}else{
						response.sendRedirect("/EnlaceMig");
						return resultado;
					}
					/*<VC FIN>*/
				 }
				 /*HD1000000134656 Se valida que solo exista un ID de sesion activo
				  * Si el id no corresponde finaliza sesion*/
				 if(session.getValidarSesion()) {
					 if(!validaSesionDB(session.getUserID8() , idSession)){

						 cierraDuplicidad(session.getUserID8());
						 session.setSeguir(false);
						 sess.removeAttribute("session");
						 sess.invalidate();
						 request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su sesi&oacute;n actual ser&aacute; cerrada, le pedimos contactarnos[SG02].\", 3);" );

						if (request.getHeader("IV-USER")!=null){
							EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
							evalTemplate(IEnlace.LOGIN, request, response);

						}else{
							response.sendRedirect("/EnlaceMig");

						}
						return false;
					 }
				 }

			 	String origen = request.getServletPath();

				Enumeration enumer = request.getParameterNames();

				int num = 0;
				String params = "";
				while(enumer.hasMoreElements()) {
					String name = (String)enumer.nextElement();
					if(num++ == 0) {
						params += "?";
					}
					else {
						params += "&";
					}
					params += name + "=" + ((String) getFormParameter(request , name));
				}
				origen += params;

			 	int statusToken = session.getToken().getStatus();

			 	/*CODIGO TEMPORAL*/
			 	if(statusToken <= 2 && statusToken >=0 && !session.getFacultad(session.FAC_VAL_OTP)) {
			 		session.setFacultad(session.FAC_VAL_OTP);
				}
				/*CODIGO TEMPORAL*/
				//--------------------------------------------------------------

			 	if(sess.getAttribute("valTK") == null &&
				   origen.indexOf("cmdpasswd") == -1 &&
			 	   origen.indexOf("CreaAmbiente") == -1 &&
			 	   (origen.indexOf("CambioContrato?Modulo=1") >= 0 || origen.indexOf("CambioContrato") == -1) &&
			 	   origen.indexOf("logout") == -1 &&
			 	   origen.indexOf("LoginServlet") == -1 &&
			 	   origen.indexOf("Micrositio") == -1) {

					boolean fchOTP = false;
					//boolean facOTP = false; //Validacion Facultad
					boolean valOTP = false;
					boolean noPaso = false;

					Token tok = session.getToken();
					EIGlobal.mensajePorTrace("validando esquemaToken con contrato = " + session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
					tok.setContrato(session.getContractNumber());

					try {
						Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
						//tok.esquemaToken(conn, session.getFacultad(session.FAC_VAL_OTP));
						conn.close();
					}catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

					EIGlobal.mensajePorTrace("validando esquemaToken", EIGlobal.NivelLog.DEBUG);
					//Validacion Facultad
					if(tok.getEsquemaToken() && (!session.getValidarToken() /*|| !session.getFacultad(session.FAC_VAL_OTP)*/)){
						noPaso = true;
						EIGlobal.mensajePorTrace("entra if esquemaToken", EIGlobal.NivelLog.DEBUG);
					}

					Calendar cal = new GregorianCalendar();
					int dia = cal.get(Calendar.DATE);
					int mes = cal.get(cal.MONTH)+1;
					String tsString = "" + cal.get(Calendar.YEAR);
					tsString += mes < 10 ? "0" + mes : "" + mes;
					tsString += dia < 10 ? "0" + dia : "" + dia;
					int tsInt = Integer.parseInt(tsString);

					EIGlobal.mensajePorTrace("\n\n\n Fecha vencimiento normativa OTP = " + Global.FCH_VAL_OTP, EIGlobal.NivelLog.DEBUG);
					if (tsInt > Integer.parseInt(Global.FCH_VAL_OTP)){ //fecha en que se deben tener todos los usuarios con token
						fchOTP = true;
						EIGlobal.mensajePorTrace("entra if fecha = " + tsInt, EIGlobal.NivelLog.DEBUG);
					}
					if (session.getValidarToken()){
						valOTP = true;
						EIGlobal.mensajePorTrace("entra if valOTP = " + valOTP + "\n\n\n", EIGlobal.NivelLog.DEBUG);
					}

					EIGlobal.mensajePorTrace("\n\n\nfecha = " + tsInt + "\nfchOTP = " + fchOTP + "\nfacOTP = " /*+ facOTP*/ + "\nvalOTP = " + valOTP + "\n\n\n", EIGlobal.NivelLog.DEBUG);

					if (fchOTP) {
						if (/*!facOTP || */!valOTP) {
							noPaso = true;
						}
					}
					/*if (facOTP && !valOTP){
						noPaso = true;
					}*/

					// si se cumplen condiciones anteriores, se regresa a login
					if (noPaso){
						int resDup = cierraDuplicidad(session.getUserID8());
						request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogoEspecial(\"Para poder utilizar el servicio es necesario contar con token.\", 3);");
						//response.sendRedirect("/EnlaceMig");
						//return true;


						 /*<VC proyecto="200710001" autor="JGR" fecha="13/07/2007" descripcin="USUARIO SAM">*/
						if (request.getHeader("IV-USER")!=null){
							EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
							evalTemplate(IEnlace.LOGIN, request, response);
							return false;
						}else{
							response.sendRedirect("/EnlaceMig");
							return true;
						}
						/*<VC FIN>*/

					}
					sess.setAttribute("valTK", "valTK");
				}

				//--------------------------------------------------------------
			 	if(session.getValidarToken() &&
			 	   !session.getTokenValidado() &&
			 	   session.getFacultad(session.FAC_VAL_OTP) &&
			 	   origen.indexOf("cmdpasswd") == -1 &&
			 	   origen.indexOf("CreaAmbiente") == -1 &&
			 	   (origen.indexOf("CambioContrato?Modulo=1") >= 0 || origen.indexOf("CambioContrato") == -1) &&
			 	   origen.indexOf("logout") == -1 &&
			 	   origen.indexOf("LoginServlet") == -1 &&
			 	   origen.indexOf("Micrositio") == -1) {
				   switch(statusToken) {
						case 0:		//activacin
									sess.setAttribute("conActual",ObtenContUser (request));
									sess.setAttribute("usuario",session.getUserID8());
									String[][] arrayUsuarios = TraeUsuarios(request);
									for(int i=1;i<=Integer.parseInt(arrayUsuarios[0][0].trim());i++){
										if(session.getUserID8().equals(arrayUsuarios[i][1].trim())) {
											sess.setAttribute("nameUsu",arrayUsuarios[i][2].trim());
										}
									}
									//<vswf:meg cambio de NASApp por Enlace 08122008>
									response.sendRedirect("/Enlace/enlaceMig/MigracionOTP?accion=actToken&origen=login");
									return true;
						case 1:		//validacin
									request.getSession().setAttribute("plantilla", origen);
									//<vswf:meg cambio de NASApp por Enlace 08122008>
									response.sendRedirect("/Enlace" + IEnlace.TOKEN);
									return true;
						case 2:		//bloqueado
									int resDup = cierraDuplicidad(session.getUserID8());
									request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogoEspecial(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 3);");
									//request.setAttribute ( "Fecha", ObtenFecha () );
									//response.sendRedirect("/EnlaceMig");
									//return true;

									 /*<VC proyecto="200710001" autor="JGR" fecha="13/07/2007" descripcin="USUARIO SAM">*/
									if (request.getHeader("IV-USER")!=null){
										EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
										evalTemplate(IEnlace.LOGIN, request, response);
										return false;
									}else{
										response.sendRedirect("/EnlaceMig");
										return true;
									}
									/*<VC FIN>*/
						default:	//error, caso por verse
									break;
				   }
				}

                 if(session.getValidarSesion()) {
                 	actualizaSesionDB(session.getUserID8());
			 	 }
                 //session.guardaHash();
                 resultado = true;
	     }
	 }
	 if(session != null) { // 09102002
	     sess.setAttribute ("session", session); // 09102002
	 }

     campaniaActualizaDatos(request, response, "");

	 return resultado;
     }


     /**************************************************************
      * Sesion valida otp.
      *
      * @param request the request
      * @param response the response
      * @return true, if successful
      * @throws ServletException the servlet exception
      * @throws IOException Signals that an I/O exception has occurred.
      */
     public boolean SesionValidaOTP ( HttpServletRequest request, HttpServletResponse response )
     throws ServletException, java.io.IOException {
	 HttpSession sess;
	 boolean resultado = false;
	 String idSession = null;
	 sess = request.getSession ( false );

	 mx.altec.enlace.bo.BaseResource session = null; // 09102002


	 if ( sess == null ) {
	     request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado introduzca sus datos nuevamente\", 4);" );
	     evalTemplate ( IEnlace.LOGIN, request, response );
	     return resultado;
	 }
	 else {
		 idSession = sess.getId();
	     session = (BaseResource) request.getSession ().getAttribute ("session");//new BaseResource ( /*  11/10/2002 sess*/ );
	     if( session == null || // 11/10/2002
		 session.getUserID8 () == null || session.getUserID8 ().equals ("") ) {
		 String nombreArchivoEmp = null;
                 if(session != null) session.getArchivoEmpNom ();
		 if ( nombreArchivoEmp != null) {
		     try {
			 File archivo = new File ( nombreArchivoEmp );
			 if ( archivo.exists () ) archivo.delete ();
		     } catch ( Exception e ) {
		    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E en el objeto File(SesionValida(): "
		    			 + e,EIGlobal.NivelLog.DEBUG);
                         EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		     }
		 }
		 if(session != null) session.setArchivoEmpNom ( null );
		 /*sess.removeAttribute ("session");
		 sess.setAttribute ("session",session);*/
		 //request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"C&oacute;digo de Cliente o Contrase&ntilde;a incorrectos\", 3);" );
		 request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado introduzca sus datos nuevamente\", 4);" );

	     } else {

				 session.setUltAcceso(Calendar.getInstance().getTime().getTime());
				 if(session.getTiempoActivo() > (Global.EXP_SESION * 60000)) {
					 //evalTemplate ( IEnlace.LOGIN, request, response );
					 int resDup = cierraDuplicidad(session.getUserID8());
					 session.setSeguir(false);
					 sess.setAttribute("session", session);
					 sess.removeAttribute("session");
					 sess.invalidate();
					 resultado = false;
					 //response.sendRedirect("/NASApp/enlaceMig/IndexServlet");
					 //response.sendRedirect("/NASApp/enlaceMig" + IEnlace.LOGIN);
					 //return resultado;
					 request.getSession().setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su Sesi&oacute;n ha expirado (forzado por SW); introduzca sus datos nuevamente\", 4);" );
					 /*<VC proyecto="200710001" autor="JGR" fecha="13/07/2007" descripcin="USUARIO SAM">*/
					if (request.getHeader("IV-USER")!=null){
						EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN SesionValidaOTP", EIGlobal.NivelLog.DEBUG);
						evalTemplate(IEnlace.LOGIN, request, response);
						return false;
					}else{
						response.sendRedirect("/EnlaceMig");
						return resultado;
					}
					/*<VC FIN>*/
				 }

				 /*HD1000000134656 Se valida que solo exista un ID de sesion activo
				  * Si el id no corresponde finaliza sesion*/
				 if(session.getValidarSesion()) {
					 if(!validaSesionDB(session.getUserID8() , idSession)){

						 cierraDuplicidad(session.getUserID8());
						 session.setSeguir(false);
						 sess.removeAttribute("session");
						 sess.invalidate();
						 request.setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su sesi&oacute;n actual ser&aacute; cerrada, le pedimos contactarnos[SG03].\", 3);" );

						if (request.getHeader("IV-USER")!=null){
							EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
							evalTemplate(IEnlace.LOGIN, request, response);

						}else{
							response.sendRedirect("/EnlaceMig");

						}
						return false;
					 }
				 }


                 if(session.getValidarSesion()) {
                 	actualizaSesionDB(session.getUserID8());
			 	 }
                 //session.guardaHash();
                 resultado = true;
	     }
	 }
	 if(session != null) { // 09102002
	     sess.setAttribute ("session", session); // 09102002
	 }

     campaniaActualizaDatos(request, response, "");

	 return resultado;
     }



 /********************************************/
 /** Metodo: validaCambioIp
  * Verifica que no haya cambio de ip durante la aplicacion, o cierra la session
  * @param HttpServletRequest -> req
  * @param HttpServletResponse -> res
  */
 /********************************************/
     public void validaCambioIp(HttpServletRequest req, HttpServletResponse res) {

    	BaseResource session = (BaseResource) req.getSession().getAttribute ("session");
    	HttpSession sess = req.getSession();
    	String ipActual = "";
    	String url = "";
    	String claveOp = "";
    	StringBuilder query = new StringBuilder();

		try {

			ipActual = obtenerIPCliente(req);


			 if (req.getRequestURL() != null) {
				 url = req.getRequestURL().toString();
			 } else {
				 if (req.getHeader("referer") != null) {
					 url = req.getHeader("referer");
				 }
			 }

			 if (req.getRequestURL() != null && req.getRequestURL().toString().contains("flujo=")) {
				 claveOp = req.getRequestURL().toString().substring(req.getRequestURL().toString().indexOf("flujo=")+6);
			 } else {
				 if (req.getHeader("referer") != null && req.getHeader("referer").contains("flujo=")) {
					 claveOp = req.getHeader("referer").substring(req.getHeader("referer").indexOf("flujo=")+6);
				 }
			 }

			 if (claveOp.length() > 4) {
				 claveOp = claveOp.substring(0, 4);
			 }

			 if (session!=null && session.getIpLogin()==null) {
				 session.setIpLogin("0.0.0.0");
			 }

			 if (session != null) {
				 EIGlobal.mensajePorTrace("Ip session BaseResource ->" + session.getIpLogin(), EIGlobal.NivelLog.DEBUG);
				 EIGlobal.mensajePorTrace("Ip actual ->" + ipActual, EIGlobal.NivelLog.DEBUG);
			 }

			 if (session!=null && !session.getIpLogin().equals("0.0.0.0") && !session.getIpLogin().equals(ipActual) && !url.contains("LoginServlet")) {
				 EIGlobal.mensajePorTrace("Hubo un cambio de ip", EIGlobal.NivelLog.INFO);

				 EIGlobal.mensajePorTrace("RequestURL ->" + req.getRequestURL(), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("Header referer ->" + req.getHeader("referer"), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("Bandera de cerrar session activa ->" + session.isCierraSesion(), EIGlobal.NivelLog.INFO);
				 EIGlobal.mensajePorTrace("Contrato y usuario especiales para cambio de ip ->" + session.isExcepcionCierraSess(), EIGlobal.NivelLog.INFO);

				 Connection conn= createiASConn (Global.DATASOURCE_ORACLE );
		 		 Statement statement = conn.createStatement();

		 		 if (url.contains("?")) {
		 			 claveOp = claveOp.concat(" ").concat(url.substring(url.indexOf("enlaceMig/")+10, url.indexOf("?")).trim());
		 		 } else {
		 			claveOp = claveOp.concat(" ").concat(url.substring(url.indexOf("enlaceMig/")+10).trim());
		 		 }

		 		 if (claveOp.length()>35) {
		 			 claveOp = claveOp.substring(0, 35);
		 		 }

		 		 if (session.getBucContrato() == null) {
		 			session.setBucContrato("");
		 		 }

		 		 //query.append("insert into EWEB_IP_CAMBIO (CANAL, BUC_CONTRATO, NUM_CONTRATO, BUC_USUARIO, FECHA_OPERACION, HORA_OPERACION, ")
		 		 query.append("insert into EWEB_IP_CAMBIO@LWEBGFSS (CANAL, BUC_CONTRATO, NUM_CONTRATO, BUC_USUARIO, FECHA_OPERACION, HORA_OPERACION, ")
		 		 		.append("CLAVE_TRANSACCION, IP_INICIAL, IP_VARIABLE) values ('ENLACE', '").append(session.getBucContrato())
		 		 		.append("', '").append(session.getContractNumber()).append("', '").append(session.getUserID8()).append("', ")
		 		 		.append("SYSDATE, ").append("SYSDATE, '")
		 		 		.append(claveOp).append("', '").append(session.getIpLogin()).append("', '").append(ipActual).append("')");

		 		 EIGlobal.mensajePorTrace("Query insert EWEB_IP_CAMBIO ->" + query.toString(), EIGlobal.NivelLog.INFO);
		 		 statement.executeUpdate(query.toString());

		 		 statement.close();
		 		 conn.close();
		 		if (session.isCierraSesion() && !session.isExcepcionCierraSess() && !url.contains("logout")) {
		 			EIGlobal.mensajePorTrace("Procede a cerrar sesion", EIGlobal.NivelLog.INFO);
		 			 int resDup = cierraDuplicidad(session.getUserID8());

					 session.setSeguir(false);
					 sess.setAttribute("session", session);
					 sess.removeAttribute("session");
					 sess.invalidate();
					 req.getSession().setAttribute ( "MensajeLogin01","cuadroDialogo(\"Su sesi&oacute;n actual ser&aacute; cerrada, le pedimos contactarnos. \", 3);" );

					if (req.getHeader("IV-USER")!=null) {
						EIGlobal.mensajePorTrace("UTILIZO EVALTEMPLATE PARA IR A LOGIN", EIGlobal.NivelLog.DEBUG);
						evalTemplate(IEnlace.LOGIN, req, res);
					} else {
						res.sendRedirect("/EnlaceMig");
					}
		 		}
			 }
		 } catch (Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 }

     }

/********************************************/
/** Metodo: obtenerContratos
 * @return void
 * @param String -> usr
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
     public void obtenerContratos ( String usr, HttpServletRequest request, HttpServletResponse response )
     throws ServletException, IOException, Exception {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::Inicia obtenerContratos()",EIGlobal.NivelLog.DEBUG);
	 String strContratos = null;
	 ServicioTux ctRemoto = new ServicioTux ();
	 //ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	 try {
	     strContratos = ctRemoto.obtenerCuentas ( usr );
	 } catch (Exception e) {
              EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace ("ERROR->TUXEDO->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		throw e;
	}
	 session.setContratos ( strContratos );
	 /*sess.removeAttribute ("session");
	 sess.setAttribute ("session",session);*/
	 sess.setAttribute ("contratos",strContratos);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::obtenerContratos():301:strContratos = >" +strContratos+ "<",
			 EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::Termina obtenerContratos()",
			 EIGlobal.NivelLog.DEBUG);
     }

/********************************************/
/** Metodo: cambiaContrato
 * @return int
 * @param boolean -> CambiaContrato
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public int cambiaContrato ( boolean CambiaContrato, HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::Inicia cambiaContrato()",
				EIGlobal.NivelLog.DEBUG);
		//String usr = null;
		String usr8 = null;
		String clv4 = null;
		String contra = "";
		String templateElegido = "";
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		try{
			//usr  = session.getUserID();
			usr8  = session.getUserID8();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + usr8,EIGlobal.NivelLog.DEBUG);
			clv4 = session.getPassword ();
			//log (clv4);

			//jppm el metodo getContractNumber ya se asigno en FuncionLogin()
			if (getFormParameter(request,"bs_ca")!=null)
			{
				contra=session.getContractNumber();
				templateElegido=(String)getFormParameter(request,"templateElegido");
			}
			else {
				request.setAttribute("cambiaContratos","1");
			}
		}catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		try {
			//TODO BIT CU5041, Inicio de flujo de Cambio de Contrato
			/*VSWF-HGG-I*/
			if (Global.USAR_BITACORAS.trim().equals("ON")){
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRATO_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
                                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace ("ERROR->CambiaContrato->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
                                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace ("ERROR->CambiaContrato->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			}


			FuncionLogin ( CambiaContrato, usr8, clv4, contra, templateElegido, request, response);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		sess.setAttribute ("session", session);
		return 1;
	}
/********************************************/
/** Metodo: login
 * @return int
 * @param boolean -> CambiaContrato
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public int login ( boolean CambiaContrato, HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::Inicia login()",EIGlobal.NivelLog.DEBUG);

		request.setAttribute("enrolar", request.getSession().getAttribute("enrolar"));
		//<VC:CGH> Se obtiene el valor temporal de la sesion del atributo origensnet para devolverlo una ves inicializada la sesion
		request.setAttribute("origensnet", request.getSession().getAttribute("origensnet"));
		//</VC>
		String coderror="";
		String contra = "";
		String templateElegido = "";
		String usr = "";
		String usrTmp = "";
		String usr8 = "";
		String clv5 = "";
		String clv4 = "";
		//String pwdold = "";
		BaseResource session = null;

	    HttpSession httpsNew = null; //request.getSession ( /*true */);
		//httpsNew.invalidate ();
	    //httpsNew = request.getSession ();

	    session = new BaseResource();
	    //httpsNew.setAttribute("session",session );
	    //usr  = request.getParameter ( "usuario" );
	    usrTmp  = (String) request.getSession().getAttribute( "usuario" );
		usr = convierteUsr7a8(usrTmp);
	    usr8  = (String) request.getSession().getAttribute( "usuario8" );

		//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
	    //contra = request.getParameter ( "contrato" );

	    //clv4   = request.getParameter ( "clave" );
	    clv4   = (String) request.getSession().getAttribute( "clave" );

	    coderror = (String) request.getSession().getAttribute( "coderror" );
	    EIGlobal.mensajePorTrace("Codigo de error: " + coderror, EIGlobal.NivelLog.INFO);

	    contra = (String) request.getSession().getAttribute( "contrato" );

		//pwdold = usr.substring (3,7).trim ();
		//templateElegido = request.getParameter ( "plantilla" );
		templateElegido = (String) request.getSession().getAttribute( "plantilla" );
		//httpsNew.setAttribute ("session", session);
		//inicializando ();
		//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet



				String[] arrCodError=null;
				arrCodError=desentramaS(coderror,'@');

			   boolean acceso = request.getSession().getAttribute("acceso") != null;

			   if(!acceso) {
					boolLogin = false;
					request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"No tiene sesi&oacute;n activa, favor de firmarse\", 4);" );
					request.setAttribute ( "Fecha", ObtenFecha () );
					evalTemplate ( IEnlace.LOGIN, request, response );
					return 1;
			   }

				else if(!validaDuplicidad(usr8, request, session)) {
					boolLogin = false;
					request.setAttribute ( "MensajeLogin01", "cuadroDialogoEspecial2(\"Enlace ha detectado que ya existe una sesi&oacute;n activa con este c&oacute;digo de cliente,  si ha tenido que reiniciar su PC, o si por alguna raz&oacute;n ha perdido la conexi&oacute;n con la p&aacute;gina del banco cuando su sesi&oacute;n estaba activa, es necesario que espere " + Global.EXP_SESION + " minutos y vuelva a intentarlo. Esta es una medida de seguridad para proteger su informaci&oacute;n. Para cualquier duda comun&iacute;quese a S&uacute;per L&iacute;nea Empresarial en la Ciudad de M&eacute;xico al 51694343 o del interior de la Rep&uacute;blica al 018005095000.\", 3);");
					request.setAttribute ( "Fecha", ObtenFecha () );
					evalTemplate ( IEnlace.LOGIN, request, response );
					return 1;
				}
				//<VC:CGH> Vuelve a colocar el valor en la sesion
			   request.getSession().setAttribute("origensnet", request.getAttribute("origensnet"));
				//</VC>
			   request.getSession().setAttribute("enrolar", request.getAttribute("enrolar"));
				Connection conn=null;
					BaseResource sess = (BaseResource) request.getSession().getAttribute("session");
				try {
					Token tok = new Token(usr8);
					//Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
					conn= createiASConn ( Global.DATASOURCE_ORACLE );
					sess.setValidarToken(tok.inicializa(conn));
					//conn.close();
					sess.setToken(tok);
					//CSA TOKEN
					if(("".equals(templateElegido)) && (tok.getStatus() == Global.STATUS_BLOQ_INTFALLIDOS) ) {
						boolLogin = false;
						request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S&uacute;per l&iacute;nea Empresarial para desbloquearlo.\", 4);" );
						request.setAttribute ( "Fecha", ObtenFecha () );
						/*int resDup = cierraDuplicidad(session.getUserID8());
						EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup, EIGlobal.NivelLog.DEBUG);*/
						evalTemplate ( IEnlace.LOGIN, request, response );
						return 1;
				   }else if(("".equals(templateElegido)) && (tok.getStatus() == Global.STATUS_BLOQ_INACTIVIDAD) ) {
						boolLogin = false;
						request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado temporalmente por inactividad, favor de llamar a nuestra S&uacute;per l&iacute;nea Empresarial para desbloquearlo.\", 4);" );
						request.setAttribute ( "Fecha", ObtenFecha () );
						evalTemplate ( IEnlace.LOGIN, request, response );
						return 1;
					}

					EIGlobal.mensajePorTrace ("BaseServlet - status Token: " + tok.getStatus(), EIGlobal.NivelLog.INFO);
				} catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				  finally{
				  			try{
				  					if(conn!= null){
				  						conn.close();
				  						conn=null;
				  					}
				  			}catch(Exception e1){
                                                                EIGlobal.mensajePorTrace ("Token - login: ->" , EIGlobal.NivelLog.DEBUG);
				  				EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
						}
				}

				  try {
					ConsultaDatosPersonaBO bo = new ConsultaDatosPersonaBO();
					 sess.setNombreUsuarioReal(bo.consultarNombrePersona(usr8));
				  } catch(Exception e) {
                                           EIGlobal.mensajePorTrace("Nombre no obtenido de PE68: " + sess.getNombreUsuario(), EIGlobal.NivelLog.ERROR);
					  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					  sess.setNombreUsuarioReal("No disponible");
				  }

				  if (sess.getNombreUsuarioReal() == null || sess.getNombreUsuarioReal().equals("") || sess.getNombreUsuarioReal().indexOf("dispon") > 0) {
					  sess.setStatusCelular("X");
					  sess.setStatusCorreo("X");
					  EIGlobal.mensajePorTrace("No existe persona, no se podra modificar Celular ni correo, tampoco se Consultaran ODB5 ni ODB6.", EIGlobal.NivelLog.INFO);
				  }


				//TODO BIT  inicio de sesi?
				/*VSWF-HGG-I*/
				if (Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo(BitaConstants.E_INICIO_SESION);

				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.E_INICIO_SESION_ENTRA);
				if (usr != null && usr8 != null) {
					bt.setUsr(usr.trim());
					bt.setCodCliente(usr);
				}

				try {
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				}
				/*VSWF-HGG-F*/

				try {
					obtenerContratos (convierteUsr8a7(usr), request, response);	//a migrar en EBE con migraci�n Perfilaci�n y Contrataci�n

				} catch (Exception e) {
					 EIGlobal.mensajePorTrace("ex AL CONSULTAR CONTRATOS->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					int resDup = cierraDuplicidad(usr8);
					request.getSession().invalidate();
					request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);" );
					EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Cierra Duplicidad en obtenerContratos: " + resDup,
							EIGlobal.NivelLog.DEBUG);
					evalTemplate (IEnlace.LOGIN,request, response);
					return -1;

				}


				request.setAttribute ( "coderror", coderror);


				String enrolar=(String)request.getSession().getAttribute("enrolar");
				if("true".equalsIgnoreCase(enrolar)){
					sess.setValidaRSA(true);
					request.getSession().setAttribute("enrolar", enrolar);
				}else{
					sess.setValidaRSA(false);
				}

				FuncionLogin ( CambiaContrato, usr8, clv4, contra, templateElegido, request, response );

			httpsNew = request.getSession();
            httpsNew.setAttribute ("session", session);

		return 1;
	}




/**
 * Usuario bloqueado.
 *
 * @param usuario the usuario
 * @return true, if successful
 */
public boolean usuarioBloqueado(String usuario) { //el contenido del m?odo fue borrado por ya no requerirse.

		return false;

	}

/**
 * Intento fallido.
 *
 * @param tok the tok
 */
public void intentoFallido(Token tok) { //el contenido del m?odo fue borrado por ya no requerirse.
	 try {


			Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
			tok.incrementaIntentos(conn);
			conn.close();
		} catch(SQLException e) {
                             EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
}


/**
 * Limpia intento fallido.
 *
 * @param tok the tok
 */
public void limpiaIntentoFallido(Token tok) { //el contenido del m?odo fue borrado por ya no requerirse.
	try {


		Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
		tok.reinicializaIntentos(conn);
		conn.close();
	} catch(SQLException e) {
                         EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	}
}


/********************************************/
/** Metodo: validaDuplicidad
 * @return int
 * @param String -> usuario
 * @param HttpSession -> session
 *    -Se modifican las consulta para usar objetos prepare statemen en vez de statemens
 *    -Se setea la variable del BaseResourse Validar sesion.
 *
 */
/********************************************/
	public boolean validaDuplicidad(String usr, HttpServletRequest request, BaseResource sess) {
		String usuario = null;
		if(usr.length() == 7) {
			usuario = convierteUsr7a8(usr);
		}
		else {
			usuario = usr;
		}

		long ultAcceso = -1;
		String ultAccesoStr = "";
		boolean valSesion = true;
		String activo = "";
        String validarSesion = "";
		boolean acceso = false;
        HttpSession session = request.getSession();
        String sessionId = null;

		EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Usuario a validar: <" + usuario + ">", EIGlobal.NivelLog.DEBUG);

    	Connection conn1= null;
    	PreparedStatement sDup1 =null;
    	ResultSet rs1=null;
    	Connection conn2=null;
    	PreparedStatement sDup2 =null;

    	try {
	     	conn1= createiASConn ( Global.DATASOURCE_ORACLE );

	     	String query = "SELECT TO_CHAR(ULT_ACCESO, 'DD-MON-YYYY HH24:MI', 'NLS_DATE_LANGUAGE = SPANISH') AS ULT_ACCESO, (SYSDATE - ULT_ACCESO) * 86400 AS TIEMPO, ACTIVO, VALIDAR_SESION, ID_SESION FROM EWEB_VALIDA_SESION WHERE CVE_USUARIO = ? ";
	     	sDup1 = conn1.prepareStatement(query);//HD1000000134656
	     	sDup1.setString(1, usuario);
	     	//String query = "SELECT TO_CHAR(ULT_ACCESO, 'DD-MON-YYYY HH24:MI', 'NLS_DATE_LANGUAGE = SPANISH') AS ULT_ACCESO, (SYSDATE - ULT_ACCESO) * 86400 AS TIEMPO, ACTIVO, VALIDAR_SESION FROM EWEB_VALIDA_SESION WHERE CVE_USUARIO = '" + usuario + "'";
	     	EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Query: <" + query + ">" + usuario + "", EIGlobal.NivelLog.DEBUG);
	     	//ResultSet rs  = sDup.executeQuery(query);
	     	rs1  = sDup1.executeQuery();
	     	while(rs1.next()) {
				ultAcceso = rs1.getLong("TIEMPO");
				ultAccesoStr = rs1.getString("ULT_ACCESO");
				activo = rs1.getString("ACTIVO");
                validarSesion = rs1.getString("VALIDAR_SESION");
                sessionId = rs1.getString("ID_SESION");

				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> ULT_ACCESO: " + ultAcceso, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> ULT_ACCESO: " + ultAccesoStr, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> TIEMPO_SES: " + (Global.EXP_SESION * 60), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> ACTIVO:   : " + activo, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> ID_SESION ANT:   : " + activo, EIGlobal.NivelLog.DEBUG);
			}
		}catch(SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad ->[" + usuario + "] Problemas para validar acceso, se deniega", EIGlobal.NivelLog.ERROR);
			acceso = true;
			return acceso;
		}finally{
				try{
					if(rs1!=null){
						rs1.close();
						rs1=null;
					}
					if(sDup1!=null){
						sDup1.close();
						sDup1=null;
					}
					if(conn1!=null){
						conn1.close();
						conn1=null;
					}
				}catch(Exception e1){
                                        EIGlobal.mensajePorTrace ("BaseServlet-validaDuplicidad-> "+e1, EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
				}
		}


        if("N".equals(validarSesion)) {
           EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad ->[" + usuario + "]Usuario configurado para NO validar sesiones duplicadas -> se acepta acceso", EIGlobal.NivelLog.INFO);
           acceso = true;
           valSesion = false;
        }else
        	if(ultAcceso == -1) {
				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad ->[" + usuario + "] Usuario sin info en tabla -> se acepta acceso", EIGlobal.NivelLog.INFO);
				acceso = true;
			}else
			  	if("S".equals(activo)) {
			  		EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad ->[" + usuario + "] Usuario activo en sistema, se validan horas", EIGlobal.NivelLog.INFO);
			  		if(ultAcceso > (Global.EXP_SESION * 60)) {
			  			acceso = true;
			  			EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> [" + usuario + "]Sesi&oacute;n vencida-> se acepta acceso", EIGlobal.NivelLog.INFO);
			  			request.setAttribute ( "plantillaElegida", "cuadroDialogoEspecial2('Enlace ha detectado que no cerr&oacute; su sesi&oacute;n adecuadamente la &uacute;ltima vez. \\nPor favor d&eacute; click en Logout una vez que termine de usar su sesi&oacute;n.\\nPara cualquier duda comun&iacute;quese a Superl&iacute;nea Empresarial en M&eacute;xico al\\n51694343 o del interior al 018005095000', 4); " );
			  		}else {
			  				acceso = false;
			  				EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad ->[" + usuario + "] Sesi&oacute;n activa-> se deniega acceso", EIGlobal.NivelLog.INFO);
			  		}
			  		//validar accesos;
			  	}else {
			  			EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> [" + usuario + "]Usuario inactivo en sistema, se acepta login", EIGlobal.NivelLog.INFO);
			  			acceso = true;
			  	}
		if(acceso && valSesion) {


			try {	conn2= createiASConn ( Global.DATASOURCE_ORACLE );

					String query = "";
					if(ultAcceso != -1) {
						query = "UPDATE EWEB_VALIDA_SESION SET ULT_ACCESO = SYSDATE, ACTIVO = 'S' WHERE CVE_USUARIO = ?";//HD1000000134656
					}
					else {
						query = "INSERT INTO EWEB_VALIDA_SESION VALUES (?, SYSDATE, 'S', 'S')";
					}
					sDup2 = conn2.prepareStatement(query);//HD1000000134656
					sDup2.setString(1,usuario);
					EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Query2: <" + query + ">", EIGlobal.NivelLog.DEBUG);
					int result = sDup2.executeUpdate();
					EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Registros actualizados: " + result, EIGlobal.NivelLog.DEBUG);
					if(result == 0)
						acceso = false;
        			//sDup.close();
                	//conn.close();
                    //session.setAttribute("session", sess);
            }catch(SQLException e){
                EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Problemas para actualizar acceso, se deniega", EIGlobal.NivelLog.INFO);
            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                       acceso = true;
                       return acceso;
            }finally{
            		try{
						if(sDup2!=null){
							sDup2.close();
							sDup2=null;
						}
						if(conn2!=null){
							conn2.close();
							conn2=null;
						}
					}catch(Exception e1){
	                                        EIGlobal.mensajePorTrace ("BaseServlet-validaDuplicidad-> "+e1, EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
			}
		}

		if(acceso) {
			EIGlobal.mensajePorTrace("VALOR DE PLANTILLA SAM: " +
					request.getSession().getAttribute("plantillaSAM"), EIGlobal.NivelLog.DEBUG);
			String plantillaSAM = (String)request.getSession().
								getAttribute("plantillaSAM");
			String pmfso_set_devToken = session.getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO)!=null ?
					(String) session.getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO) : "";
			String devicePrint = request.getSession().getAttribute("valorDeviceP") != null ?
	    			request.getSession().getAttribute("valorDeviceP").toString() : "";
			session.removeAttribute("token"); //MSD
			session.invalidate ();
			session = request.getSession ();
			session.setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, pmfso_set_devToken);
			session.setAttribute("valorDeviceP", devicePrint);
			EIGlobal.mensajePorTrace("ATTR_SET_FLASH_SO en validaDuplicidad: " + session.getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO),
					EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("valorDeviceP en validaDuplicidad: " + session.getAttribute("valorDeviceP"),
					EIGlobal.NivelLog.DEBUG);
			if(valSesion) {
				sess.setUserID(usuario);
				sess.setUserID8(usuario);//cambio
				sess.setSid(session.getId());
				sess.setUltAcceso(Calendar.getInstance().getTime().getTime());
				sess.setTiempoSesion((Global.EXP_SESION * 60000));

				sess.setValidarSesion(valSesion);
				PreparedStatement sDup3 = null;

				sessionId = session.getId();
				try {
					conn2= createiASConn ( Global.DATASOURCE_ORACLE );

					String query;
					query = "UPDATE EWEB_VALIDA_SESION SET ID_SESION = ? WHERE CVE_USUARIO = ?";
					sDup3 = conn2.prepareStatement(query);
					sDup3.setString(1, sessionId);
					sDup3.setString(2, usuario);
					sDup3.executeUpdate();

				}catch(SQLException e){
	                EIGlobal.mensajePorTrace ("BaseServlet - validaDuplicidad -> Problemas para actualizar acceso, se deniega", EIGlobal.NivelLog.INFO);
	            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	                       acceso = true;
	                       return acceso;
	            }finally{
					try{
						if(sDup2!=null){
							sDup2.close();
							sDup2=null;
						}
						if(conn2!=null){
							conn2.close();
							conn2=null;
						}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace ("BaseServlet-validaDuplicidad-> "+e1, EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
				}
			}
			//Guarda la fecha de Ultimo Acceso
			sess.setUltAccesoStr(ultAccesoStr);
			session.setAttribute ("session", sess);
			/*<VC proyecto="200710001" autor="JGR" fecha="22/05/2007" descripci?="PRUEBAS">*/
			session.setAttribute("plantillaSAM", plantillaSAM);
			EIGlobal.mensajePorTrace("VALOR DE PLANTILLA SAM 1: " +
					request.getSession().getAttribute("plantillaSAM"), EIGlobal.NivelLog.DEBUG);
			/*</VC F>*/
		}

		return acceso;

	}


/********************************************/
/** Metodo: validaDuplicidad
 * @return int
 * @param String -> usuario
 * @param HttpSession -> session
 */
/********************************************/
	public boolean sesionLibre(String usuario, HttpServletRequest request) {
		long ultAcceso = -1;
		String activo = "N";
		String validarSesion = "N";
		boolean acceso;
		HttpSession session = request.getSession();

		Connection conn=null;
		Statement sDup =null;
		ResultSet rs =null;

		try {
	     	//Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     	//Statement sDup = conn.createStatement();
	     	conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     	sDup = conn.createStatement();

	     	String query = "SELECT (SYSDATE - ULT_ACCESO) * 86400 AS TIEMPO, ACTIVO, VALIDAR_SESION FROM EWEB_VALIDA_SESION WHERE CVE_USUARIO = '" + usuario + "'";
	     	EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Query: <" + query + ">", EIGlobal.NivelLog.DEBUG);
			//ResultSet rs  = sDup.executeQuery(query);
	     	rs  = sDup.executeQuery(query);
	     	while(rs.next()) {
				ultAcceso = rs.getLong("TIEMPO");
				activo = rs.getString("ACTIVO");
                validarSesion = rs.getString("VALIDAR_SESION");
				EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> ULT_ACCESO: " + ultAcceso, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> ACTIVO:   : " + activo, EIGlobal.NivelLog.DEBUG);
			}
			//sDup.close();
			//conn.close();
		}
		catch(SQLException e) {
                        EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Problemas para validar acceso, se acepta", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			acceso = true;
			return acceso;
		}finally{
					try{
						if(rs!=null){
							rs.close();
							rs=null;
						}
						if(sDup!=null){
							sDup.close();
							sDup=null;
						}
						if(conn!=null){
							conn.close();
							conn=null;
						}
					}catch(Exception e1){
	                                        EIGlobal.mensajePorTrace ("BaseServlet-sesionLibre-> "+e1, EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
		}

        if("N".equals(validarSesion)) {
        	EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Usuario configurado para NO validar sesiones duplicadas -> se acepta acceso", EIGlobal.NivelLog.INFO);
        	acceso = true;
        }
        else if(ultAcceso == -1) {
			EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Usuario sin info en tabla -> se acepta acceso", EIGlobal.NivelLog.INFO);
			acceso = true;
		}
		else if("S".equals(activo)) {
			EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Usuario activo en sistema, se validan horas", EIGlobal.NivelLog.INFO);
			if(ultAcceso > (Global.EXP_SESION * 60000)) {
				acceso = true;
				EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Sesi&oacute;n vencida-> se acepta acceso", EIGlobal.NivelLog.INFO);
				request.setAttribute ( "plantillaElegida", "cuadroDialogoEspecial2('Enlace ha detectado que no cerr&oacute; su sesi&oacute;n adecuadamente la &uacute;ltima vez. \\nPor favor d&eacute; click en Logout una vez que termine de usar su sesi&oacute;n.\\nPara cualquier duda comun&iacute;quese a Superl&iacute;nea Empresarial en M&eacute;xico al\\n51694343 o del interior al 018005095000', 4); " );
			}
			else {
				acceso = false;
				EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Sesin activa-> se deniega acceso", EIGlobal.NivelLog.INFO);
			}
			//validar accesos;
		}
		else {
			EIGlobal.mensajePorTrace ("BaseServlet - sesionLibre -> Usuario inactivo en sistema, se acepta login", EIGlobal.NivelLog.INFO);
			acceso = true;
		}
		return acceso;
	}



/**
 * Actualiza sesion db.
 *
 * @param usr the usr
 */
public void actualizaSesionDB(String usr) {
	String usuario = null;
	if(usr.length() == 7) {
		usuario = convierteUsr7a8(usr);
	}
	else {
		usuario = usr;
	}

	Connection conn=null;
	PreparedStatement sDup=null;

	            try {
		        //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
		        conn= createiASConn ( Global.DATASOURCE_ORACLE );
	            String query = "UPDATE EWEB_VALIDA_SESION SET ULT_ACCESO = SYSDATE, ACTIVO = 'S' WHERE CVE_USUARIO = ?";
	            //Statement sDup = conn.createStatement();
	            sDup = conn.prepareStatement(query);
	            sDup.setString(1,usr);
	            sDup.executeUpdate();
	            EIGlobal.mensajePorTrace ("BaseServlet - actualizarSesion -> Query: <" + query + ">", EIGlobal.NivelLog.DEBUG);

				int resultado = sDup.executeUpdate();
	            	EIGlobal.mensajePorTrace ("BaseServlet - Registros actualizados: <" + resultado + ">", EIGlobal.NivelLog.DEBUG);
	            	//ultAccesoAnt = ultAcceso;

	            //sDup.close();
	            //conn.close();
	        }
	        catch(SQLException e) {
                        EIGlobal.mensajePorTrace ("BaseServlet - actualizaSesionDB -> Problemas para actualizar sesin en DB", EIGlobal.NivelLog.ERROR);
	        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	        }finally{
					try{
						if(sDup!=null){
							sDup.close();
							sDup=null;
						}
						if(conn!=null){
							conn.close();
							conn=null;
						}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
			}
}

//HD1000000134656 Se valida que solo exista un ID de sesion activo

/**
 * Funcion para validar que el id de sesion actual se igual con la sesion del login.
 * @param usr
 * @param idSesion
 * @return
 */
public boolean validaSesionDB(String usr,String idSesion ) {

	EIGlobal.mensajePorTrace ("BaseServlet - Entrando validaSesionDB: <" + usr + ">"+"<" + idSesion + ">", EIGlobal.NivelLog.INFO);
	String usuario = null;

	String idSesionLogin = null;
	boolean valido = true;
	if(usr.length() == 7) {
		usuario = convierteUsr7a8(usr);
	}
	else {
		usuario = usr;
	}

	Connection conn=null;
	PreparedStatement ptSession=null;
	ResultSet rs = null;

	            try {
		        //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
		        conn= createiASConn ( Global.DATASOURCE_ORACLE );
	            String query = "SELECT ID_SESION FROM EWEB_VALIDA_SESION where CVE_USUARIO = ?";
	            //Statement sDup = conn.createStatement();
	            ptSession = conn.prepareStatement(query);
	            ptSession.setString(1, usr);
	            EIGlobal.mensajePorTrace ("BaseServlet - validaSesionDB -> Query: <" + query + "> " + usr, EIGlobal.NivelLog.DEBUG);

				rs = ptSession.executeQuery();

				if(rs.next()){
					idSesionLogin = rs.getString("ID_SESION");
					idSesionLogin = (idSesionLogin != null ? idSesionLogin.trim() : idSesionLogin);
					EIGlobal.mensajePorTrace ("BaseServlet - validaSesionDB -> Comparando: <" + idSesionLogin + "><" + idSesion +">", EIGlobal.NivelLog.INFO);


					if(idSesionLogin == null){
						valido = false;
						EIGlobal.mensajePorTrace ("BaseServlet - validaSesionDB -> Sesion nula finalizar sesion", EIGlobal.NivelLog.DEBUG);
					}else if(!idSesionLogin.equals(idSesion)){
						EIGlobal.mensajePorTrace ("BaseServlet - validaSesionDB -> Sesion diferente finalizar sesion", EIGlobal.NivelLog.DEBUG);
						valido = false;
					}
				}
	        }
	        catch(SQLException e) {
                        EIGlobal.mensajePorTrace ("BaseServlet - validaSesionDB -> Problemas para actualizar sesin en DB", EIGlobal.NivelLog.ERROR);
	        	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	        }finally{
					try{
						if(rs!=null){
							rs.close();
							rs = null;
						}
						if(ptSession !=null){
							ptSession.close();
							ptSession = null;
						}
						if(conn!=null){
							conn.close();
							conn=null;
						}
					}catch(Exception e1){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
			}
	 EIGlobal.mensajePorTrace ("BaseServlet - Saliendo validaSesionDB: <" + usr + ">"+"<" + valido + ">", EIGlobal.NivelLog.INFO);
	 return valido;
}
/********************************************/
/** Metodo: convToHTML
 * @return String
 * @param String -> cadena
 */
/********************************************/
	public String convToHTML( String cadena)
		throws IOException, ServletException
	{
		String s1="";
		String s2="";
		// [a] equivale a ?
		// [e] equivale a ?
		String[] dominio={"[a]","[e]","[i]","[o]","[u]"};
		String[] dominioX=  {"a","e","i","o","u"};
		int pos=0;

		while(cadena.indexOf("[n]")>0)
		{
			pos=cadena.indexOf("[n]");
			s1=cadena.substring(0,pos)+"&ntilde;";
			s2=cadena.substring(pos+3);
			cadena=s1+s2;
		}

		for (int i=0;i<dominio.length;i++)
		{
			while(cadena.indexOf(dominio[i])>0)
			{
				pos=cadena.indexOf(dominio[i]);
				s1=cadena.substring(0,pos)+"&"+dominioX[i]+"acute;";
				s2=cadena.substring(pos+3);
				cadena=s1+s2;
			}
		}
		return cadena;
	}

/********************************************/
/** Metodo: desentramaS
 * @return Strong[]
 * @param String -> cadena
 * @param char -> caracter
 */
/********************************************/
	public String[] desentramaS(String cadena, char caracter)
	{
		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++ )
		{
		  if ( regtmp.charAt( i ) == caracter )
			totalCampos++;
		}
		camposTabla = new String[totalCampos];
		for (int i = 0; i < totalCampos; i++ )
		{
			if ( regtmp.charAt( 0 ) == caracter )
			{
				vartmp = " ";
				regtmp = regtmp.substring( 1, regtmp.length() );
			}
			else
			{
				vartmp = regtmp.substring( 0, regtmp.indexOf( caracter )).trim();
				if ( regtmp.indexOf( caracter ) > 0 )
					regtmp=regtmp.substring( regtmp.indexOf( caracter ) + 1,regtmp.length() );
			}
			camposTabla[i]=vartmp;
		}
		return camposTabla;
	}

/********************************************/
/** Metodo: mlog
 * @return void
 * @param String -> valor
 */
/********************************************/
	public void mlog (String valor) {
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + valor,EIGlobal.NivelLog.DEBUG);
	}

/********************************************/
/** Metodo: FuncionLogin
 * @return int
 * @param boolean -> CambiaContrato
 * @param String -> usr
 * @param String -> clv4
 * @param String -> contra
 * @param String -> templateElegido
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public int FuncionLogin ( boolean CambiaContrato, String usr, String clv4, String contra, String templateElegido, HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java:: inicia FuncionLogin()",
				EIGlobal.NivelLog.DEBUG);
		boolean UnContrato = false;
		String strSalida = "";
		String strTemplate = "";
		String contratosAsoc = "";

		boolLogin = true;
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		//<VC:CGH> Se Obtiene el origen del canal
		String redirSupernet = (String)request.getAttribute("origensnet");
		//</VC>
		session.setUserID ( usr );
		session.setUserID8 ( usr );
		session.setPassword ( clv4 );

		//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
		String coderror = (String) request.getAttribute("coderror");

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "************************* --> CODERROR: " + coderror,
				EIGlobal.NivelLog.INFO);

		String entro_cambio_nip="";
		if (sess.getAttribute("entro_cambio_nip")!=null)
		{
			entro_cambio_nip =(String) sess.getAttribute("entro_cambio_nip");
		}

		contratosAsoc = session.getContratos ();
		String[] contratos = null;
		contratos = desentramaC ( contratosAsoc, '@' );
		for (int i=1; i<=Integer.parseInt ( contratos[0] ); i++)
		{
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::FuncionLogin():301:contratos = >" +contratos[i]+ "<",
					EIGlobal.NivelLog.DEBUG);
		}
		if ( (Integer.parseInt ( contratos[0] ) == 1) )
		{
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::FuncionLogin()()::if:UN SOLO CONTRATO",
					EIGlobal.NivelLog.DEBUG);
			//strTemplate = IEnlace.CONTRATOS_TMPL;
			strTemplate = IEnlace.PRINCIPAL;
			String[] contratos3 = desentramaC ( "2|" + contratos[1] + "|", '|' );
			String AmbCod = TraeAmbiente ( session.getUserID8(), contratos3[1],request );
			String[] noPerfil = TraePerfil (request);


			/**
			 *aqui es donde se va a cachar el contrato enviado por el salto de supernet a enlace
			 *como el campo contrato ya no existe se tiene que eliminar esta validacion para que el usuario
			 *pueda operar en enlace con el unico contrato existente en caso de que tenga contratos diferentes de supernet y enlace
		     */
			//*<VC proyecto="200710001" autor="ESC" fecha="03/08/2007" descripci?="SALTO SUPERNET-ENLACE">*/
			/*if(contra != null && !contra.equals("")) {
				if(!contra.equals(contratos3[1])) {
					int resDup = cierraDuplicidad(session.getUserID());
					request.setAttribute ("MensajeLogin01", "cuadroDialogo(\"El contrato que ingres&oacute; no es v&aacute;lido o no <br>est&aacute; asociado a su usuario.\", 3);");
					sess.setAttribute("session", session);
					log( "LoginServlet.java::token() -> resDup: " + resDup );
					sess.invalidate();
					evalTemplate (IEnlace.LOGIN,request, response);
					return -1;
				}
			}*/
			//*</VC>*/


			//Guardo el nombre en la session
			try {
				request.setAttribute ("nuser", contratos3[2]);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			strSalida = "";
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Antes FijaAmbiente",EIGlobal.NivelLog.DEBUG);
			sess.setAttribute ("ContractNumber",contratos3[1]);
			session.setContractNumber (contratos3[1]);

			/*FSW - Stefanini - Menu Epyme Unicontrato */

			EpymeBO epymeBO = new EpymeBO();
			boolean esMenuReducido = epymeBO.esMenuReducido(session.getContractNumber(),session.getUserID8());
			session.setEpyme(esMenuReducido);
			EIGlobal.mensajePorTrace("***BaseServlet.FuncionLogin()-> contrato Epyme = [" + contratos3[1] + "][ " + esMenuReducido + "]" , EIGlobal.NivelLog.INFO);
			/*FSW - Stefanini */

			if ( !FijaAmbiente (request) )
			{
				boolLogin = false;
				request.setAttribute ( "MsgError", "No se pudo obtener la configuraci&oacute;n del usuario.<P>" );
				request.setAttribute ( "Fecha",ObtenFecha () );
				evalTemplate ( IEnlace.ERROR_TMPL, request, response );
				return -1;
			}
			// rfv - begin
			if(!verificaFacultad ("ACCESINTERNET",request))  //cambio
			{
				//<VC:CGH> Estado del origen del canal
				EIGlobal.mensajePorTrace ("REDIR-SUPERNET B1:" + redirSupernet, EIGlobal.NivelLog.DEBUG);
				//</VC>
				EIGlobal.mensajePorTrace ("***login.class Facultad ACCESINTERNET False", EIGlobal.NivelLog.INFO);
				boolLogin = false;
				request.setAttribute ("MensajeLogin01", "cuadroDialogo(\"No tiene facultad de operar en Enlace Internet <br> con el contrato " + contratos3[1] + ".\", 3);");
				int resDup = cierraDuplicidad(session.getUserID8());
				sess.setAttribute("session", session);
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
						EIGlobal.NivelLog.DEBUG);
				sess.invalidate();
				//<VC:CGH> Redireccion a Supernet si no se cuenta con facultades
				if(redirSupernet != null && "true".equals(redirSupernet)) {
					EIGlobal.mensajePorTrace ("FACULTAD-REDIR: SUPERNET " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
					request.setAttribute("snetuser", session.getUserID8());
					request.setAttribute("snetcont", session.getContractNumber());
					evalTemplate (Global.URL_SUPERNET,request, response);
				} else {
					EIGlobal.mensajePorTrace ("FACULTAD-REDIR: ENLACE " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
					evalTemplate (IEnlace.LOGIN,request, response);
				}
				//</VC>
				return -1;
			} else {
				EIGlobal.mensajePorTrace ("***login.class Facultad ACCESINTERNET True", EIGlobal.NivelLog.DEBUG);
			}
			// rfv - end
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Antes CambiaContrato",EIGlobal.NivelLog.DEBUG);
			if ( !CambiaContrato )
			{
				arregloLocations = asignaURLS (request, session);
				String nuevoMenu = creaFuncionMenu ( arregloLocations, request );
                /*Stefanini - Modificacion Menu Epyme*/
				String elMenu = creaCodigoMenu (arregloLocations,request);
				session.setFuncionesDeMenu ( nuevoMenu );
				sess.setAttribute ("funcionesMenu",nuevoMenu);
				sess.setAttribute ("StrMenu",elMenu);
				session.setStrMenu ( elMenu );
			}
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Despues CambiaContrato", EIGlobal.NivelLog.DEBUG);
			sess.setAttribute ("UserProfile", noPerfil[1]);
			session.setUserProfile ( noPerfil[1] );
			sess.setAttribute ("ContractNumber",contratos3[1]);
			session.setContractNumber ( contratos3[1] );
			request.setAttribute ("Mensaje1", strSalida);
			request.setAttribute ("Fecha", ObtenFecha ());
			request.setAttribute ("ContUser", ObtenContUser (request));
			request.setAttribute ("MenuPrincipal", session.getStrMenu ());
			request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
			request.setAttribute ("fechaHoy", ObtenFecha ());
			request.setAttribute ("horaHoy", ObtenHora ());
			//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
			sess.setAttribute("entro_cambio_nip","1");
			UnContrato = true;
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "FIN DEL IF CONTRATO = 1", EIGlobal.NivelLog.DEBUG);
		}
		else
		{
			//CSA- valida cuando no contratos asociados
			if ( (Integer.parseInt ( contratos[0] ) == 0) ){
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::login()::if:else:CERO CONTRATOS ACTIVOS O SIN ASOCIAR",
						EIGlobal.NivelLog.DEBUG);
				request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"El usuario no tiene contrato activo o asociado.\",3);" );
				evalTemplate ( IEnlace.LOGIN, request, response );
				sess.invalidate();


			}else{





			//M? DE UN CONTRATO
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::login()::if:else:M? DE UN CONTRATO",
					EIGlobal.NivelLog.DEBUG);
			strTemplate = IEnlace.CONTRATOS_TMPL;
			strSalida = "";
			String[] Contratos2 = null;

			boolean contratoValido = false;
			for ( int indice = 1; indice <= Integer.parseInt ( contratos[0] ); indice++ )
			{
				Contratos2 = desentramaC ( "2|" + contratos[indice] + "|", '|' );
				strSalida = strSalida + "<OPTION Value = \"" + Contratos2[1] + "\">" + Contratos2[1] + " " + Contratos2[2] + "</OPTION>";
				if ( Contratos2[1].equals ( contra ) )
				{
					contratoValido = true;
					String AmbCod = TraeAmbiente ( convierteUsr7a8(usr), contra,request );	// se env�a a 7 porque esto se va con perfilaci�n y contrataci�n EBE
					String[] noPerfil = TraePerfil (request);
					String strSalida2  = "";
					sess.setAttribute ("ContractNumber",contra);
					session.setContractNumber (contra);
					if ( !FijaAmbiente (request) )
					{
						boolLogin = false;
						request.setAttribute ("MsgError", "No se pudo obtener la configuraci&oacute;n del usuario.<P>");
						request.setAttribute ("Fecha",ObtenFecha ());
						evalTemplate ( IEnlace.ERROR_TMPL, request, response );
						return -1;
					}
					session = (BaseResource) request.getSession().getAttribute("session");
					// rfv - begin
					if(!verificaFacultad ("ACCESINTERNET",request)) //cambio
					{
						EIGlobal.mensajePorTrace ("***login.class Facultad ACCESINTERNET False", EIGlobal.NivelLog.INFO);
						boolLogin = false;
						//<VC:CGH> Estado del origen del canal
						EIGlobal.mensajePorTrace ("REDIR-SUPERNET B2:" + redirSupernet, EIGlobal.NivelLog.DEBUG);
						//</VC>
						request.setAttribute ("MensajeLogin01", "cuadroDialogo(\"No tiene facultad de operar en Enlace Internet <br> con el contrato " + contra + ".\", 3);");
						int resDup = cierraDuplicidad(session.getUserID8());
						sess.setAttribute("session", session);
						 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
								 EIGlobal.NivelLog.DEBUG);
						sess.invalidate();
						//<VC:CGH> Redireccion a Supernet si no se cuenta con facultades
						if(redirSupernet != null && "true".equals(redirSupernet.toString())) {
							request.setAttribute("snetuser", session.getUserID8());
							request.setAttribute("snetcont", session.getContractNumber());
							EIGlobal.mensajePorTrace ("FACULTAD-REDIR: SUPERNET " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
							evalTemplate (Global.URL_SUPERNET,request, response);
						} else {
							EIGlobal.mensajePorTrace ("FACULTAD-REDIR: ENLACE " + session.getUserID8(), EIGlobal.NivelLog.DEBUG);
							evalTemplate (IEnlace.LOGIN,request, response);
						}
						//</VC>
						return -1;
					} else {
						EIGlobal.mensajePorTrace ("***FuncionLogin.class Facultad ACCESINTERNET True", EIGlobal.NivelLog.DEBUG);
					}
					// rfv - end
					sess.setAttribute ("UserProfile",noPerfil[1]);
					session.setUserProfile ( noPerfil[1] );
					sess.setAttribute ("ContractNumber",contra);
					session.setContractNumber ( contra );
					if ( !CambiaContrato )
					{
						String [][] arregloLocations2 = asignaURLS (request, session);
						String nuevoMenu2 = creaFuncionMenu ( arregloLocations2, request );
						/*Stefanini - Modificacion Menu Epyme*/
						String elMenu2 = creaCodigoMenu (arregloLocations2,request);
						sess.setAttribute ("funcionesMenu",nuevoMenu2);
						session.setFuncionesDeMenu ( nuevoMenu2 );
						sess.setAttribute ("StrMenu",elMenu2);
						session.setStrMenu ( elMenu2 );
						request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Contrato", "Login &gt; Selecci&oacute;n de Contrato" ,"s26031h",request));
					} else {
						request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Contrato", "Administraci&oacute;n y control &gt; Cambio de Contrato" ,"s26030h",request));
					}
					request.setAttribute ("MenuPrincipal", session.getStrMenu () );
					request.setAttribute ( "newMenu", session.getFuncionesDeMenu () );
					request.setAttribute ("Mensaje1", strSalida2);
					request.setAttribute ("fechaHoy", ObtenFecha ());
					request.setAttribute ("horaHoy", ObtenHora ());
					//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
					sess.setAttribute("entro_cambio_nip","1");
					strTemplate = IEnlace.PRINCIPAL;
					break;
				}// fin if contra
			} // fin del for
			/**
			 *aqui es donde se va a cachar el contrato enviado por el salto de supernet a enlace
			 *como el campo contrato ya no existe se tiene que eliminar esta validacion para el el usuario
			 *pueda operar en enlace con el unico contrato existente
		     */
			//*<VC proyecto="200710001" autor="ESC" fecha="03/08/2007" descripci?="SALTO SUPERNET-ENLACE">
			/*
			if(contra != null && !contra.equals("") && !contratoValido) {
				int resDup = cierraDuplicidad(session.getUserID());
				sess.setAttribute("session", session);
				log( "LoginServlet.java::token() -> resDup: " + resDup );
				sess.invalidate();
				request.setAttribute ("MensajeLogin01", "cuadroDialogo(\"El contrato que ingres&oacute; no es v&aacute;lido o no <br>est&aacute; asociado a su usuario.\", 3);");
				evalTemplate (IEnlace.LOGIN,request, response);
				return -1;
			}*/
			//</VC>
			}//fin del else de 0 contratos
		} //fin else MAS DE UN CONTRATO
		request.setAttribute ( "Output1", strSalida );
		request.setAttribute ( "Fecha", ObtenFecha () );
		request.setAttribute ( "ContUser", ObtenContUser (request) );
		request.setAttribute ( "fechaHoy", ObtenFecha () );
		request.setAttribute ( "horaHoy", ObtenHora () );
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "############### ----->"+ObtenContUser (request),
				 EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "UnContrato = " + UnContrato,
				 EIGlobal.NivelLog.DEBUG);
		if ( UnContrato )
		{
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::login()::2da verificacion:if:UN SOLO CONTRATO",
					 EIGlobal.NivelLog.DEBUG);
			request.setAttribute ("Fecha",ObtenFecha ());
			request.setAttribute ("ContUser", ObtenContUser (request));
			request.setAttribute ("fechaHoy", ObtenFecha ());
			request.setAttribute ("horaHoy", ObtenHora ());
			String avisos = obtenerAvisos (request);


			/*Declaraci? para envio de mensajes para un solo contrato   everis   27/06/2008     inicio*/
			String mensaje = muestraMensaje(request, response);
			/*Declaraci? para envio de mensajes para un solo contrato   everis   27/06/2008     fin   */


			String vencimientosCE=avisosVencimientosCE(request);


			request.setAttribute ("muestraAvisos", avisos);


			/*Llamado para envio de mensajes para un solo contrato   everis   27/06/2008     inicio*/
			EIGlobal.mensajePorTrace("--- BaseServlet Mensaje Login: " + mensaje, EIGlobal.NivelLog.INFO);
			if(!"".equals(mensaje))
			{
				request.setAttribute("MensajeLogin01", " cuadroDialogoMensaje(\" <tr><td class=textabdatobs align=center ><br> Importante leer los siguientes mensajes: </td></tr>" + mensaje + ".\", 4);");
			}



			/*Llamado para envio de mensajes para un solo contrato   everis   27/06/2008     fin*/

			//Stefanini enrolamiento
			int statusToken = session.getToken().getStatus();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "ESTATUS TOKEN = " + statusToken, EIGlobal.NivelLog.DEBUG);
			String enrolar=(String)request.getSession().getAttribute("enrolar");
			if("true".equalsIgnoreCase(enrolar)&&statusToken<=0){
				templateElegido = "12";
			}

			request.setAttribute("datosVencimientosCE", vencimientosCE);
			if ( "".equals(templateElegido) )
				templateElegido = "3";
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "templateElegido = " + templateElegido, EIGlobal.NivelLog.DEBUG);
			String laDireccion = determinaTemplate ( Integer.parseInt ( templateElegido ) );
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "laDireccion = " + laDireccion, EIGlobal.NivelLog.DEBUG);
			if(request.getAttribute("plantillaElegida") != null) {
				String dirFinal = (String) request.getAttribute("plantillaElegida");
				request.setAttribute ( "plantillaElegida", dirFinal + laDireccion);
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Cargando Template = " + dirFinal + laDireccion,
						EIGlobal.NivelLog.DEBUG);
			}
			else {
				request.setAttribute ( "plantillaElegida", laDireccion);
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Cargando Template = " + laDireccion, EIGlobal.NivelLog.DEBUG);
			}
			//modificacion para integracion


			/*Se elimina funcionalidad de b?queda de cuentas interbancarias no actualizadas

			// **************************************************************
			// Rafael Rosiles
			// Modificacion para verificar si el contrato tiene cuentas CLABE 11 digitos
			// **************************************************************
			int cuentaCLABE;
			EIGlobal.mensajePorTrace( "*** CreaAmbiente modificacion cambio CLABE para un solo contrato", EIGlobal.NivelLog.INFO);
			cuentaCLABE = verificaCLABE( request, response);
			EIGlobal.mensajePorTrace( "*** CreaAmbiente Numero de cuentas CLABE>>"+ String.valueOf(cuentaCLABE), EIGlobal.NivelLog.DEBUG);
			if (cuentaCLABE !=0)
			{
				String direccion;
				direccion = laDireccion.substring(laDireccion.indexOf("'")+1,laDireccion.length()-1);
				EIGlobal.mensajePorTrace( "*** CreaAmbiente direccion elegida >>" + direccion, EIGlobal.NivelLog.INFO);
				request.setAttribute ( "plantillaElegida", "window.open('AvisoCLABE?plantillaOriginal="+direccion+"','AvisoCLABE','width=380,height=210,resizable=no,toolbar=no,scrollbars=no,left=210,top=225')" );
			}
			//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet

			Termina eliminacin de funcionalidad de CUENTA CLABE*/

            if (!"1".equals(entro_cambio_nip))
            {

				String[] arrCodError=null;
				arrCodError=desentramaS(coderror,'@');
				if(arrCodError[0].equals("SEG_0001"))
				{
					request.setAttribute("bs_ca","1");
					request.setAttribute("templateElegido", templateElegido);
					request.setAttribute("num_dias",arrCodError[1]);
					request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Contrase&ntilde;a por expirar", "Login &gt; Contrase&ntilde;a por expirar" ,"s26032h",request));
					evalTemplate ( "/jsp/Conf_cambio_nip.jsp", request, response );
					return 1;
				}

				if (arrCodError[0].equals("SEG_0002"))
				{

					request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a" ,"s26040h",request));
					request.setAttribute ( "MensajeCambioPass", "cuadroDialogo(\"Su password ha expirado, le suplicamos ingresar una nueva contrase&ntilde;a de 8 a 20 caracteres. \", 3);" );
					evalTemplate ( IEnlace.CAMBIO_PASSWD, request, response );
					return 1;
				}
			}
			evalTemplate ( IEnlace.CARGATEMPLATE_TMPL, request, response );
		}
		else
		{
			//CSA- valida cuando no contratos asociados
			if ( (Integer.parseInt ( contratos[0] ) == 0) ){
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::login()::if:else:CERO CONTRATOS ACTIVOS O SIN ASOCIAR",
						EIGlobal.NivelLog.DEBUG);
				request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"El usuario no tiene contrato activo o asociado.\",3);" );
				evalTemplate ( IEnlace.LOGIN, request, response );
				sess.invalidate();


			}else{
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::login()::2da verificacin: if:else:M? DE UN CONTRATO",
					 EIGlobal.NivelLog.DEBUG);
			request.setAttribute ( "Fecha", ObtenFecha ( false ) );
			if ( !CambiaContrato )
			{
				String [][] arregloLocations2 = asignaURLS (request, session);
				String nuevoMenu3 = creaFuncionMenu ( arregloLocations2, request );
				/*Stefanini - Modificacion Menu Epyme*/
                String elMenu3 = creaCodigoMenu (arregloLocations2,request);
				sess.setAttribute ("funcionesMenu",nuevoMenu3);
				session.setFuncionesDeMenu ( nuevoMenu3 );
				sess.setAttribute ("StrMenu",nuevoMenu3);
				session.setStrMenu ( elMenu3 );
				request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Contrato", "Login &gt; Selecci&oacute;n de Contrato" ,"s26031h",request));
			} else {
				request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Seleccione Contrato", "Administraci&oacute;n y control &gt; Cambio de Contrato" ,"s26030h",request));
			}

			//Stefanini enrolamiento
			int statusToken = session.getToken().getStatus();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "ESTATUS TOKEN = " + statusToken, EIGlobal.NivelLog.DEBUG);
			String enrolar=(String)request.getSession().getAttribute("enrolar");
			if("true".equalsIgnoreCase(enrolar)&&statusToken<=0){
				templateElegido = "12";
			}


			if ( "".equals(templateElegido) )
				templateElegido = "3";
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "templateElegido = " + templateElegido, EIGlobal.NivelLog.DEBUG);
			String laDireccion = determinaTemplate ( Integer.parseInt ( templateElegido ) );
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "laDireccion = " + laDireccion, EIGlobal.NivelLog.DEBUG);
			request.setAttribute ( "fechaHoy", ObtenFecha ());
			request.setAttribute ( "horaHoy", ObtenHora ());
			request.setAttribute ( "opcionElegida", laDireccion );
			request.setAttribute ( "plantillaElegida", laDireccion);
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "strTemplate = " + strTemplate, EIGlobal.NivelLog.DEBUG);
			//modificacion para integracion
			if(strTemplate.equals (IEnlace.CONTRATOS_TMPL)) {
				evalTemplate (strTemplate, request, response );
			}
			else
            {
				//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
				if (!"1".equals(entro_cambio_nip))
				{
					String[] arrCodError=null;
					arrCodError=desentramaS(coderror,'@');
					if(arrCodError[0].equals("SEG_0001"))
					{
						request.setAttribute("bs_ca","1");
						request.setAttribute("templateElegido", templateElegido);
						request.setAttribute("num_dias",arrCodError[1]);
						request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Contrase&ntilde;a por expirar", "Login &gt; Contrase&ntilde;a por expirar" ,"s26032h",request));
						evalTemplate ( "/jsp/Conf_cambio_nip.jsp", request, response );
						return 1;
					}
					if (arrCodError[0].equals("SEG_0002"))
					{
						request.setAttribute ("Encabezado", CreaEncabezadoContrato ("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a" ,"s26040h",request));
						request.setAttribute ( "MensajeCambioPass", "cuadroDialogo(\"Su password ha expirado, le suplicamos ingresar una nueva contrase&ntilde;a de 8 a 20 caracteres. \", 4);" );
						evalTemplate ( IEnlace.CAMBIO_PASSWD, request, response );
						return 1;
					}
				}
				evalTemplate ( IEnlace.CARGATEMPLATE_TMPL, request, response );
            }
		  }//fin del else 0contratos

		}//fin del else (UnContrato)


		if (!CambiaContrato) {

			session.setIpLogin(obtenerIPCliente(request));
			session.setCierraSesion(banderaCierraSession());
			session.setCarrierCompleto(EmailCelularDAO.consultaCarrier());

			EIGlobal.mensajePorTrace("IpLogin ->" + session.getIpLogin(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CierraSesion ->" + session.isCierraSesion(), EIGlobal.NivelLog.INFO);
		}


		sess.setAttribute ("session", session);
		return 1;
	}
	//metodo para revisar la existencia de una facultad asociada a una cuenta.

/********************************************/
/** Metodo: desentramaC
 * @return String[]
 * @param String -> buffer
 * @param char -> Separador
 */
/********************************************/
	public String[] desentramaC ( String buffer, char Separador ) {
		EIGlobal.mensajePorTrace("buffer["+ buffer +"] ", EIGlobal.NivelLog.INFO);
	 String bufRest;
	 int intNoCont = Integer.parseInt ( buffer.substring ( 0, buffer.indexOf ( Separador ) ) );
	 EIGlobal.mensajePorTrace("intNoCont["+ intNoCont +"] ", EIGlobal.NivelLog.INFO);
	 String[] arregloSal = new String[ intNoCont + 1 ];
	 arregloSal[0] = buffer.substring ( 0, buffer.indexOf ( Separador ) );
	 bufRest = buffer.substring ( buffer.indexOf ( Separador ) + 1, buffer.length () );
	 for( int cont = 1; cont <= intNoCont; cont++ ) {
	     arregloSal[cont] = bufRest.substring ( 0, bufRest.indexOf ( Separador ) );
	     EIGlobal.mensajePorTrace("arregloSal["+cont+"]["+ arregloSal[cont] +"] ", EIGlobal.NivelLog.INFO);
	     bufRest = bufRest.substring ( bufRest.indexOf ( Separador) + 1, bufRest.length () );
	 }
	 return arregloSal;
     }
     /**
      * TraePerfil
      */

/********************************************/
/** Metodo: TraePerfil
 * @return String[]
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String[] TraePerfil (HttpServletRequest request) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::TraePerfil()", EIGlobal.NivelLog.DEBUG);
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 BufferedReader entrada;
	 String linea = "";
	 String result[] = new String[ 2 ];
	 FileReader lee = null;
		 try{
			 lee = new FileReader ( session.getArchivoAmb () );
		 }catch(Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 }
	 try {
	     entrada = new BufferedReader ( lee );
	     linea = entrada.readLine ();
	     while ( ( linea = entrada.readLine () ) != null ) {
		 if ( "".equals(linea) )
		     linea = entrada.readLine ();
		 if ( linea == null )
		     break;
		 if ( linea.startsWith ( "5" ) ) {
		     result[ 0 ] = "SEGU0000";
		     result[ 1 ] = ( linea.substring ( linea.indexOf ( ";" ) + 1,
		     linea.lastIndexOf ( ";" ) ) ).trim ();
		     break;
		 }
	     }
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                  EIGlobal.mensajePorTrace ("E->TraePerfil->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	     result[ 0 ] = "null";
	     result[ 1 ] = "null";
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
		 }
	 return result;
     }

/********************************************/
/** Metodo: ObtenContUser
 * @return String
 * @param HttpServletRequest -> request
 */
/********************************************/
	public String ObtenContUser (HttpServletRequest request)
	{
		String contrato = "";
		String Nombre = "";
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");
		try {
			contrato = session.getContractNumber ();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "contrato = >"+contrato+ "<", EIGlobal.NivelLog.DEBUG);
			Nombre = session.getNombreContrato ();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "nombre = >" +Nombre+ "<", EIGlobal.NivelLog.DEBUG);
		} catch(Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		if(contrato==null)
			contrato = " ";
		if(Nombre==null)
			Nombre = "";
		return "&nbsp;" + contrato + "&nbsp;" + Nombre;
	}

/********************************************/
/** Metodo: ObtenFecha
 * @return String
 */
/********************************************/
	public String ObtenFecha ()
	{
		String strFecha = "";
		String[] dias = { "","Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};
		String[] meses = { "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		java.util.Date Hoy = new java.util.Date ();
		GregorianCalendar Cal = new GregorianCalendar ();
		Cal.setTime (Hoy);
		strFecha = Cal.get (Calendar.DATE) + " de " + meses[Cal.get (Calendar.MONTH)] + " de " + Cal.get (Calendar.YEAR);
		return strFecha;
	}

/********************************************/
/** Metodo: ObtenFecha
 * @return String
 * @param boolean -> formato_corto
 */
/********************************************/
	public String ObtenFecha ( boolean formato_corto )
	{
		String strFecha = "";
		java.util.Date Hoy = new java.util.Date ();
		GregorianCalendar Cal = new GregorianCalendar ();
		Cal.setTime (Hoy);
		int eldia;
		if(Cal.get (Calendar.DATE) <= 9)
		{
			strFecha += "0" + Cal.get (Calendar.DATE) + "/";
		} else {
			strFecha += Cal.get (Calendar.DATE) + "/";
		}
		if (Cal.get (Calendar.MONTH)+1 <= 9)
		{
			strFecha += "0" + (Cal.get (Calendar.MONTH)+1) + "/";
		} else {
			strFecha += (Cal.get (Calendar.MONTH)+1) + "/";
		}
		strFecha += Cal.get (Calendar.YEAR);
		if(!formato_corto)
		{
			strFecha = "";
			String[] dias = {"","Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};
			String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			strFecha = /*dias[Cal.get(Calendar.DAY_OF_WEEK)] + ", " +*/ Cal.get (Calendar.DATE) + " de " + meses[Cal.get (Calendar.MONTH)] + " de " + Cal.get (Calendar.YEAR);
		}
		return strFecha;
	}

/********************************************/
/** Metodo: TraeAmbiente
 * @return String
 * @param String -> strUsr
 * @param String -> strCont
 * @param HttpServletRequest -> request
 */
/********************************************/
	 public String TraeAmbiente ( String strUsrTmp, String strCont,HttpServletRequest request ) {
	 	try {
			 String strUsr = "";
			 strUsr = convierteUsr7a8(strUsrTmp);
			 HttpSession sess = request.getSession ();
			 BaseResource session = (BaseResource) sess.getAttribute ("session");
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "->BaseServlet.java::TraeAmbiente( " + strUsr + ", " + strCont + " )",
					 EIGlobal.NivelLog.INFO);
			 int numeroReferencia = 0;
			 String coderror = "";
			 String AMBI_OK_CODE = "AMBI0000";
			 String NombreArchivo = IEnlace.REMOTO_TMP_DIR + "/" + strUsr + IEnlace.ARCHIVO_AMBIENTE;
			 try {
				 ServicioTux ctRemoto = new ServicioTux ();
				 //ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
				 coderror = ctRemoto.traeAmbiente ( strUsr, strCont );
			 } catch ( java.rmi.RemoteException re ) {
                                 EIGlobal.mensajePorTrace ("E->TraeAmbiente->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
				 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			 } catch (Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
			 ArchivoRemoto archR = new ArchivoRemoto ();
			 if ( !archR.copia ( strUsr + IEnlace.ARCHIVO_AMBIENTE ) ) {
					 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "No se realizo la copia remota de archivo ambiente",
							 EIGlobal.NivelLog.ERROR);
			 }
				 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "copiando Archivo Remoto:" + IEnlace.LOCAL_TMP_DIR + "/" + strUsr,
						 EIGlobal.NivelLog.DEBUG);
			 sess.setAttribute ("ArchivoAmb",IEnlace.LOCAL_TMP_DIR + "/" + strUsr + IEnlace.ARCHIVO_AMBIENTE);
			 session.setArchivoAmb ( IEnlace.LOCAL_TMP_DIR + "/" + strUsr + IEnlace.ARCHIVO_AMBIENTE );
			 //sess.removeAttribute ("session");
			 sess.setAttribute ("session",session);
			 return coderror;
		 } catch(Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 return "AMBI8888";
		 }
     }

/********************************************/
/** Metodo: getFileData
 * @return String
 * @param String -> filename
 */
/********************************************/
     public String getFileData ( String filename ) {
	 String salida = null;
		 FileInputStream myfile = null;
	 try {
	     myfile = new FileInputStream ( filename );
	     int inBytes=myfile.available ();
	     byte inBuffer[] = new byte[inBytes];
	     int bytesReads = myfile.read (inBuffer,0,inBytes);
	     salida =new String (inBuffer);
	 } catch ( Exception e ) {
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "error \n" + e.toString (),
				 EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try{
				      myfile.close ();
			 }catch(Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
		 }
	 return salida;
     }

/********************************************/
/** Metodo: TraeUsuarios
 * @return String[][]
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String[][] TraeUsuarios (HttpServletRequest request)
     {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 String linea = "";
	 StringBuffer UsuariosSB = new StringBuffer();
	 String Usuarios = "";
	 BufferedReader entrada;
	 int cont = 0;
	 FileReader lee = null;
	 try {
			 lee = new FileReader ( session.getArchivoAmb () );
	     entrada = new BufferedReader ( lee );
	     linea = entrada.readLine ();
	     while ( ( linea = entrada.readLine () ) != null ) {
		 if ( "".equals(linea) )
		     linea = entrada.readLine ();
		 if ( linea == null )
		     break;
		 if ( linea.substring ( 0, 3 ).trim ().equals ( "10;" ) ) {
		     UsuariosSB.append(linea);
		     UsuariosSB.append("@");
		     cont++;
		 }
	     }
	     Usuarios = cont + "@" + UsuariosSB.toString();
	 } catch ( Exception e ) {
                 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E " + e.toString (), EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
		 }
	 String[][] arrayUsuarios = new String[cont + 2][3];
	 arrayUsuarios[0][0] = "" + (cont + 1) + "";
	 arrayUsuarios[cont + 1][1] = session.getUserID8 ();
	 arrayUsuarios[cont + 1][2] = session.getNombreUsuario ();
	 try {
		 String[] arrayLineas = desentramaC (Usuarios,'@');
		 for ( int indice = 1; indice <= cont; indice++ ) {
		     String[] arrayLineaUsuario = desentramaC ( "5;" + arrayLineas[indice], ';' );
		     arrayUsuarios[indice][1] = arrayLineaUsuario[2];
		     arrayUsuarios[indice][2] = arrayLineaUsuario[3] + " " + arrayLineaUsuario[4] + " " + arrayLineaUsuario[5];
		 }
	 } catch(Exception e) {
		EIGlobal.mensajePorTrace("E en traeUsuarios, variable Usuarios: <" + Usuarios + ">", EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 return arrayUsuarios;
     }

/********************************************/
/** Metodo: FijaAmbiente
 * @return boolean
 * @param HttpServletRequest -> request
 */
/********************************************/
	public boolean FijaAmbiente (HttpServletRequest request)
	{
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");
		//log("FijaAmbiente inicial, session=" + session);
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::FijaAmbiente()", EIGlobal.NivelLog.DEBUG);
		String cuentas = "";
		String[] arrcuentas = null;
		String ctasOtrosBancos = "";
		String[] arrctasOtrosBancos = null;
		String linea = null;
		int contcuentas = 0;
		int contctasOtrosBancos = 0;
		int i = 0;
		int j = 0;
		int k = 0;
		boolean AmbienteFijo = false;
		boolean CargoCheques = false;
		boolean AbonoCheques = false;
		boolean VistaInver   = false;
		boolean InverVista   = false;
		BufferedReader entrada;
		//***********************************************
		//modificacin para integracin pva 06/03/2002
		//***********************************************
		int ntotalctas = 0;
		int ncheqctas = 0;
		int nespctas = 0;
		//***********************************************
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Termina de inicializar... Antes de FileReader lee", EIGlobal.NivelLog.DEBUG);
		FileReader lee = null;
		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Termina de inicializar... Pasa FileReader lee", EIGlobal.NivelLog.DEBUG);
		try {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Asigna valor a lee",EIGlobal.NivelLog.DEBUG);
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "session.getArchivoAmb () = [" + session.getArchivoAmb () + "]",
					 EIGlobal.NivelLog.DEBUG);
			lee = new FileReader ( session.getArchivoAmb () );
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "lee sale con valor <<" + lee +">>", EIGlobal.NivelLog.DEBUG);
			entrada = new BufferedReader ( lee );
			linea = entrada.readLine ();
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "linea = >>" + linea + "<<", EIGlobal.NivelLog.DEBUG);
			if (null == linea)
				 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "FijaAmbiente linea null", EIGlobal.NivelLog.DEBUG);
			while ( ( linea = entrada.readLine () ) != null )
			{
				/* Modificacin  para verificacion de facultades 19/08/2002
				inicio*/
				if( linea.trim ().equals ("") ){
				continue;
			}
			try {
				java.util.StringTokenizer tokenizer = new java.util.StringTokenizer (linea,";");
				String tipoRegistro = tokenizer.nextToken ();
				if("4".equals(tipoRegistro) || "7".equals(tipoRegistro))
				{
					String facultad = tokenizer.nextToken ();
					if (null != facultad && !facultad.trim ().equals (""))
					{
						/* 11/10/2002 */
						//log("facultad:" + facultad);
						session.setFacultad( facultad );
						//session.setAttribute (facultad,new Boolean (true) );
					}
				}
			} catch (java.util.NoSuchElementException ex) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
		}
		/* fin modificaion 19/08/2002 */
		if ( linea.substring ( 0, 2 ).equals ( "1;" ) )
		{
			String[] NombreContrato = desentramaC ( linea, ';' );
			session.setNombreContrato ( NombreContrato[1] );
		} else if ( linea.substring ( 0, 2 ).equals ( "9;" ) ) {
		     String[] NombreUsuario = desentramaC ("4;" + linea,';');
		     session.setNombreUsuario (NombreUsuario[2] + " " + NombreUsuario[3] + " " + NombreUsuario[4]);
		 }
	     }
/*	     if(session.getNombreContrato == null) {
			 session.setNombreContrato = "";
		 }*/
	     ObtenclaveBanco (request);
	     entrada.close ();
	     AmbienteFijo = true;
	     //sess.removeAttribute ("session");
	     //sess.setAttribute ("session",session);
	 }   catch ( Exception e ) {
                 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer el ambiente " + e.toString (), EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     //sess.removeAttribute ("session");
	     //sess.setAttribute ("session",session);
				try{
					lee.close();
				}catch(Exception ee) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(ee), EIGlobal.NivelLog.INFO);
				}
	     //e.printStackTrace ();
	     EIGlobal.mensajePorTrace ("ERROR->FijaAmbiente->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	     return false;
	 }finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
		 }
	 //sess.removeAttribute ("session");
	 sess.setAttribute ("session",session);
//		 session = (BaseResource) sess.getAttribute ("session");
//		 log("FijaAmbiente fin, session=" + session);
	 return AmbienteFijo;
     }

/********************************************/
/** Metodo: inicializando
 * @return int
 */
/********************************************/
     public int inicializando () {
	 return 1;
     }

/********************************************/
/** Metodo: evalTemplate
 * @return void
 * @param String -> template
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 */
/********************************************/
     public void evalTemplate (String template, HttpServletRequest req, HttpServletResponse res)
     throws ServletException, IOException {
		 /*<VC proyecto="200710001" autor="JGR" fecha="17/05/2007" descripci?="IR PAGINA SAM">*/
		 if (template.equals(IEnlace.LOGIN)){
			 EIGlobal.mensajePorTrace("DENTRO DE EVALTEMPLATE->" + req.getHeader("IV-USER"), EIGlobal.NivelLog.DEBUG);
			 if (req.getHeader("IV-USER")!=null){	//EL USUARIO ACCESO POR SAM
				 redirSAM(req, res);
				 EIGlobal.mensajePorTrace("SE VA A PAGINA SAM", EIGlobal.NivelLog.DEBUG);
				 return;
			 }
		 }
		 //<VC:CGH> Muestra el mensaje de facultad no valida y redirige a supernet
		 if (Global.URL_SUPERNET.equals(template)) {
			 redirSupernet(req, res);
			 return;
		 }

		 //JPH vswf 05/06/2009 requerimiento Liga Enlace supernet comercios
		 if (Global.URL_SUPERNET_COMERCIOS.equals(template)) {
			 EIGlobal.mensajePorTrace("LOGOUT ENLACE---> REDIRECCIONAMIENTO A SUPERNET COMERCIOS", EIGlobal.NivelLog.DEBUG);
			 redirSupernetComer(req, res);
			 return;
		 }


		 EIGlobal.mensajePorTrace("EJECUTA EVALTEMPLATE NORMAL", EIGlobal.NivelLog.DEBUG);


		 validaCambioIp(req, res);

		 if ("/jsp/MMC_Alta.jsp".equals(template) || "/jsp/Cont_Cons.jsp".equals(template)) {
			 campaniaActualizaDatos(req, res, template);
		 }

      //HD000000000000 - CSA-Sesion invadalia Cliente - se quita el llamado ya que este llamado se realiza en la parte
      /* boolean despliegaPagMedioNot = false;

       if (template.equals(IEnlace.C_SALDO_TMPL)) {
    	   EIGlobal.mensajePorTrace ("ENTRO A C_SALDO_TMPL", EIGlobal.NivelLog.INFO);
    	   BaseResource session = null;
		   session = (BaseResource) req.getSession().getAttribute ("session");

		   verificaSuperUsuario(session.getContractNumber(), req);

		   EIGlobal.mensajePorTrace("Es SuperUsuario " + session.getUserID8() + " ->" + session.getMensajeUSR().isSuperUsuario(), EIGlobal.NivelLog.INFO);

		   subeDatosDeContacto(session.getUserID8(), req);

		   if (//verificaFacultad("CAMMEDNOTSUP", req) &&
				   session.getMensajeUSR().isSuperUsuario()) {
			   if (!session.getStatusCelular().equals("X") && !session.getStatusCorreo().equals("X") &&
					session.getMensajeUSR().getMedioNotSuperUsuario().equals("") &&
					session.getMensajeUSR().getLadaSuperUsuario().equals("") &&
					session.getMensajeUSR().getNoCelularSuperUsuario().equals("") &&
					session.getMensajeUSR().getEmailSuperUsuario().equals("")) {

				   despliegaPagMedioNot = true;
			   }
		   }

       }

        if (despliegaPagMedioNot) {
        	EIGlobal.mensajePorTrace("Debido a falta de datos para medios de notificacion, se redirige a la pantalla para actualizar datos", EIGlobal.NivelLog.INFO);
        	res.sendRedirect("ConfigMedioNot?opcion=2");
 		} else {*/
			 RequestDispatcher disp = getServletContext().getRequestDispatcher(template);
			 res.setContentType("text/html");

			 try {
				 disp.include (req,res);
			 } catch (Exception e) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
 		/*}*/
     }

     /**
      * Metodo encargado de redirigir a la pantalla de mantenimiento de datos para que actualice sus datos
      * @param req -> HttpServletRequest
      * @param res -> HttpServletResponse
      * @param template -> Template elegido
      */
     public void campaniaActualizaDatos(HttpServletRequest req, HttpServletResponse res, String template) {
    	 String url = "";

    	 BaseResource session = null;

    	 try {
			 session = (BaseResource) req.getSession().getAttribute ("session");

	    	 if (req.getRequestURL() != null) {
	    		 url = req.getRequestURL().toString();
	    	 }

	    	 if (session!=null && (session.isCampaniaActiva() || session.isCampaniaCtoActiva())) {
		    	 if (url.contains("/ANomMttoEmp") || url.contains("/InicioNomina") || url.contains("/CatalogoNominaEmpl") ||
					 url.contains("/InicioNominaInter") || url.contains("/CatalogoNominaEmplInterb") || url.contains("/CatNomInterbRegistro") ||
					 url.contains("/CatNomInterbBaja") || url.contains("/CatalogoNominaAutInterb") || url.contains("/NomPreRemesas") ||
					 url.contains("/NomPreAltaTarjeta") || url.contains("/NomPreBusqueda") || url.contains("/NomPreCancelaTarjeta") ||
					 url.contains("/NomPreReposicionTarjeta") || url.contains("/NomPreConsultaEstado") || url.contains("/NomPredispersion") ||
					 url.contains("/RET_LiberaOper") ||
					 "/jsp/MMC_Alta.jsp".equals(template) || "/jsp/Cont_Cons.jsp".equals(template)) {

					 	EIGlobal.mensajePorTrace("����������������������������������������������������������", EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getContractNumber ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getUserID8 ->" + session.getUserID8(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getClaveSuperUsuario ->" + session.getMensajeUSR().getClaveSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("isSuperUsuario() ->" + session.getMensajeUSR().isSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getLadaSuperUsuario() ->" + session.getMensajeUSR().getLadaSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getNoCelularSuperUsuario() ->" + session.getMensajeUSR().getNoCelularSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getCompCelularSuperUsuario() ->" + session.getMensajeUSR().getCompCelularSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getEmailSuperUsuario() ->" + session.getMensajeUSR().getEmailSuperUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getLadaUsuario() ->" + session.getMensajeUSR().getLadaUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getNoCelularUsuario() ->" + session.getMensajeUSR().getNoCelularUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getCompCelularUsuario() ->" + session.getMensajeUSR().getCompCelularUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getEmailUsuario() ->" + session.getMensajeUSR().getEmailUsuario(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("isCampaniaActiva() ->" + session.isCampaniaActiva(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getStatusCelular() ->" + session.getStatusCelular(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getStatusCorreo() ->" + session.getStatusCorreo(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("isCampaniaCtoActiva() ->" + session.isCampaniaCtoActiva(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("getEmailContrato() ->" + session.getMensajeUSR().getEmailContrato(), EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("Url ->" + url, EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("Template ->" + template, EIGlobal.NivelLog.INFO);
					 	EIGlobal.mensajePorTrace("����������������������������������������������������������", EIGlobal.NivelLog.INFO);

					 	if (!verificaDatosCampania(session.getMensajeUSR().isSuperUsuario(), req)) {
							req.getSession().setAttribute("campa�a", "true");
							try {
								res.sendRedirect("CapturaDatosUsr?opcion=0");
							} catch (IOException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
					 	}
	    		 } else {
	    			 if (req.getSession().getAttribute("campa�a") != null) {
	    				 if (req.getSession().getAttribute("campa�a").toString().equals("true") && url.contains("/CapturaDatosUsr")) {
	    					 PrintWriter out;
							try {
								out = res.getWriter();
								out.write("<script>window.open('/Enlace/jsp/alertActualizaDatos.jsp','trainerWindow','width=430,height=200,toolbar=no,scrollbars=no,left=210,top=225').focus();</script>");
		    					req.getSession().removeAttribute("campa�a");
							} catch (IOException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}

	    				 }
	    			 }
	    		 }
	    	 }
     	} catch (Exception e) {
     		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
     }

     /**
      * Metodo para verificar si los datos de contacto son incorrectos y mandar a actualizarlos
      * @param superUsuario -> Indica si el perfil es de superusuario
      * @param req -> HttpServletRequest
      * @return datosCorrectos -> Indica si los datos de contacto est�n correctos
      */
     public boolean verificaDatosCampania(boolean superUsuario, HttpServletRequest req) {
    	 boolean datosCorrectos = true;

    	 BaseResource session = null;
		 session = (BaseResource) req.getSession().getAttribute ("session");

		 MensajeUSR mensajeUSR = session.getMensajeUSR();
		 List listaCorreosInvalidos = new ArrayList();
		 int opcion = 0;

		 listaCorreosInvalidos.add("banca_elactronica@bsantander.com.mx");
		 listaCorreosInvalidos.add("banca_electronica@bsantander.com.mx");
		 listaCorreosInvalidos.add("banca_electronica@santander.com.mx");
		 listaCorreosInvalidos.add("hugrodriguez@santander.com.mx");
		 listaCorreosInvalidos.add("xanca_elactronica@bsantander.com.mx");

		 if (!session.getStatusCelular().equals("X") && !session.getStatusCorreo().equals("X")) {
			 opcion = 1;
		 }
		 if (!session.getStatusCelular().equals("X") && session.getStatusCorreo().equals("X")) {
			 opcion = 2;
		 }
		 if (session.getStatusCelular().equals("X") && !session.getStatusCorreo().equals("X")) {
			 opcion = 3;
		 }

		 if (session.isCampaniaActiva()) {
	    	 if (superUsuario) {

	    		 switch (opcion) {
	    		 	case 1:
	    		 		if (mensajeUSR.getEmailSuperUsuario()==null || mensajeUSR.getEmailSuperUsuario().equals("") ||
			    				 mensajeUSR.getLadaSuperUsuario()==null || mensajeUSR.getLadaSuperUsuario().equals("") ||
			    				 mensajeUSR.getNoCelularSuperUsuario()==null || mensajeUSR.getNoCelularSuperUsuario().equals("") ||
			    				 mensajeUSR.getCompCelularSuperUsuario()==null || mensajeUSR.getCompCelularSuperUsuario().equals("")) {
			    			 datosCorrectos = false;
			    		 } else {
			    			 for (int aux=0; aux<listaCorreosInvalidos.size(); aux++) {
			    				 if (mensajeUSR.getEmailSuperUsuario().equalsIgnoreCase((String) listaCorreosInvalidos.get(aux))) {
			    					 datosCorrectos = false;
			    					 break;
			    				 }
			    			 }
			    		 }
	    		 		break;

	    		 	case 2:
	    		 		if (mensajeUSR.getLadaSuperUsuario()==null || mensajeUSR.getLadaSuperUsuario().equals("") ||
			    				 mensajeUSR.getNoCelularSuperUsuario()==null || mensajeUSR.getNoCelularSuperUsuario().equals("") ||
			    				 mensajeUSR.getCompCelularSuperUsuario()==null || mensajeUSR.getCompCelularSuperUsuario().equals("")) {
			    			 datosCorrectos = false;
			    		 }
	    		 		break;

	    		 	case 3:
	    		 		if (mensajeUSR.getEmailSuperUsuario()==null || mensajeUSR.getEmailSuperUsuario().equals("")) {
			    			 datosCorrectos = false;
			    		 } else {
			    			 for (int aux=0; aux<listaCorreosInvalidos.size(); aux++) {
			    				 if (mensajeUSR.getEmailSuperUsuario().equalsIgnoreCase((String) listaCorreosInvalidos.get(aux))) {
			    					 datosCorrectos = false;
			    					 break;
			    				 }
			    			 }
			    		 }
	    		 		break;

	    		 	default:
	    		 		break;
				}


	    	 } else {

	    		 if (!session.getStatusCorreo().equals("X")) {
		    		 if (mensajeUSR.getEmailUsuario()==null || mensajeUSR.getEmailUsuario().equals("")) {
		    			 datosCorrectos = false;
		    		 } else {
		    			 for (int aux=0; aux<listaCorreosInvalidos.size(); aux++) {
		    				 if (mensajeUSR.getEmailUsuario().equalsIgnoreCase((String) listaCorreosInvalidos.get(aux))) {
		    					 datosCorrectos = false;
		    					 break;
		    				 }
		    			 }
		    		 }
	    		 }

	    	 }
		 }

		 if (session.isCampaniaCtoActiva()) {
	    	 if (mensajeUSR.getEmailContrato()==null || mensajeUSR.getEmailContrato().equals("")) {
	    		 datosCorrectos = false;
	    	 } else {
	    		 for (int aux=0; aux<listaCorreosInvalidos.size(); aux++) {
					 if (mensajeUSR.getEmailContrato().equalsIgnoreCase((String) listaCorreosInvalidos.get(aux))) {
						 datosCorrectos = false;
						 break;
					 }
				 }
	    	 }
		 }

    	 EIGlobal.mensajePorTrace("Estado de la verificacion de datos para campa�a ->" + datosCorrectos, EIGlobal.NivelLog.INFO);
    	 return datosCorrectos;
     }


      /********************************************/
      /**
       ** Metodo encargado de verificar que el registro del contrato exista
   	      en tct_cuentas_tele
       * @return clave SuperUsuario y MedioNot
       * @param  String -> contrato
       * @param  req -> HttpServletRequest
       */
      /********************************************/
      	public void verificaSuperUsuario(String contrato, HttpServletRequest req) {
      		String query = "";
      		ResultSet rs = null;
      		String[] datos = new String[2];

      		BaseResource session = null;
 		    session = (BaseResource) req.getSession().getAttribute ("session");

      		datos[0] = "";
			datos[1] = "";

      		try {
      			Connection conn= createiASConn (Global.DATASOURCE_ORACLE );
      			Statement sDup = conn.createStatement();
      			query = "select * from tct_cuentas_tele where num_cuenta='".concat(contrato).concat("'");
      			EIGlobal.mensajePorTrace("BaseServlet::verificaSuperUsuario para " + session.getUserID8() + ", Query:" + query, EIGlobal.NivelLog.INFO);
      			rs = sDup.executeQuery(query);

      			if(rs.next()) {
  					datos[0] = rs.getString("cve_superusuario");
  					datos[1] = rs.getString("medio_notificacion");

  					if (datos[0]!=null && datos[0].equals(session.getUserID8())) {
	  					session.getMensajeUSR().setSuperUsuario(true);
	  					session.getMensajeUSR().setClaveSuperUsuario(datos[0]!=null ? datos[0] : "");
	  					session.getMensajeUSR().setMedioNotSuperUsuario(datos[1]!=null ? datos[1] : "");
  					} else {
  						EIGlobal.mensajePorTrace("BaseServlet::verificaSuperUsuario. No es superusuario, el superusuario es ->" + datos[0], EIGlobal.NivelLog.INFO);
  						session.getMensajeUSR().setSuperUsuario(false);
  						session.getMensajeUSR().setClaveSuperUsuario(datos[0]!=null ? datos[0] : "");
  						session.getMensajeUSR().setMedioNotSuperUsuario(datos[1]!=null ? datos[1] : "");
  					}
      			} else {
      				session.getMensajeUSR().setSuperUsuario(false);
      				session.getMensajeUSR().setClaveSuperUsuario("");
      				session.getMensajeUSR().setMedioNotSuperUsuario("");
      			}

      			rs.close();
      			sDup.close();
      			conn.close();
      		} catch (SQLException e) {
      			session.getMensajeUSR().setSuperUsuario(false);
      			session.getMensajeUSR().setClaveSuperUsuario("");
      			session.getMensajeUSR().setMedioNotSuperUsuario("");
      			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
      		}

      	}

    	/**
    	 * Metodo para consultar la informacion de contacto de un usuario
    	 * @param usuario -> Usuario a consultar su informacion
    	 * @param req -> HttpServletRequest
    	 * @return emailCelularBean -> Bean con la informacion recuperada
    	 */
    	private EmailCelularBean consultaMailCelular(String usuario, HttpServletRequest req) {
    		EmailCelularDAO emailCelularDAO = new EmailCelularDAO();
    		EmailCelularBean celularBeanConsulta = new EmailCelularBean();
    		EmailCelularBean emailBeanConsulta = new EmailCelularBean();
    		EmailCelularBean emailCelularBean = new EmailCelularBean();

    		BaseResource session = null;
 		    session = (BaseResource) req.getSession().getAttribute ("session");

 		    if (usuario == null) {
 		    	usuario = "";
 		    }

 		    EIGlobal.mensajePorTrace("BaseServlet::consultaMailCelular Se consultan datos para ->" + usuario + "<-", EIGlobal.NivelLog.INFO);

 		    if (!"".equals(usuario)) {
	 		    if (session.getStatusCelular() == null || !session.getStatusCelular().equals("X")) {
					celularBeanConsulta = emailCelularDAO.consultaCelular(usuario);

					if (celularBeanConsulta.getEstatus().equals("ODA0002")) {
						EIGlobal.mensajePorTrace("La consulta del celular fue correcta", EIGlobal.NivelLog.INFO);
		    			emailCelularBean.setLada(celularBeanConsulta.getLada() != null ? celularBeanConsulta.getLada() : "");
		    			emailCelularBean.setNoCelular(celularBeanConsulta.getNoCelular() != null ? celularBeanConsulta.getNoCelular() : "");
		    			emailCelularBean.setCarrier(celularBeanConsulta.getCarrier() != null ? celularBeanConsulta.getCarrier() : "");

		    			session.setStatusCelular("M");
					} else {
						EIGlobal.mensajePorTrace("No pudo realizarse la consulta del celular. Codigo ->" + celularBeanConsulta.getEstatus(), EIGlobal.NivelLog.INFO);
						if (celularBeanConsulta.getEstatus().equals("ODE0071") || celularBeanConsulta.getEstatus().equals("ODE0074")) {

							session.setStatusCelular("A");
						} else {
							session.setStatusCelular("X");
						}
						emailCelularBean.setLada("");
		    			emailCelularBean.setNoCelular("");
		    			emailCelularBean.setCarrier("");
					}
	 		    }
	 		    if (session.getStatusCorreo() == null || !session.getStatusCorreo().equals("X")) {
		    		emailBeanConsulta = emailCelularDAO.consultaCorreo(usuario);

		    		if (emailBeanConsulta.getEstatus().equals("ODA0002")) {
		    			EIGlobal.mensajePorTrace("La consulta del correo fue correcta", EIGlobal.NivelLog.INFO);
		    			emailCelularBean.setCorreo(emailBeanConsulta.getCorreo()!= null ? emailBeanConsulta.getCorreo() : "");

		    			session.setStatusCorreo("M");
		    		} else {
		    			EIGlobal.mensajePorTrace("No pudo realizarse la consulta del correo. Codigo ->" + emailBeanConsulta.getEstatus(), EIGlobal.NivelLog.INFO);
		    			if (emailBeanConsulta.getEstatus().equals("ODE0071") || emailBeanConsulta.getEstatus().equals("ODE0075")) {

							session.setStatusCorreo("A");
						} else {
							session.setStatusCorreo("X");
						}
		    			emailCelularBean.setCorreo("");
		    		}
	 		    }
 		    }

			return emailCelularBean;
    	}

    	/**
    	 * Metodo para subir a session los datos de contacto de un usuario
    	 * @param usuario -> Usuario de quien se obtendra su informacion de contacto
    	 * @param req -> HttpServletRequest
    	 */
    	public void subeDatosDeContacto(String usuario, HttpServletRequest req) {
    		try {
	    		BaseResource session = null;
	 		    session = (BaseResource) req.getSession().getAttribute ("session");

	 		    EmailCelularBean emailCelularBean = new EmailCelularBean();
	 		    String cveSuperusuario = "";

	    		if (session.getMensajeUSR().isSuperUsuario()) {
	    			emailCelularBean = consultaMailCelular(session.getUserID8(), req);

	    			session.getMensajeUSR().setEmailSuperUsuario(emailCelularBean.getCorreo());
	    			session.getMensajeUSR().setEmailUsuario(emailCelularBean.getCorreo());
	    			session.getMensajeUSR().setLadaSuperUsuario(emailCelularBean.getLada());
	    			session.getMensajeUSR().setNoCelularSuperUsuario(emailCelularBean.getNoCelular());
	    			session.getMensajeUSR().setLadaUsuario(emailCelularBean.getLada());
	    			session.getMensajeUSR().setNoCelularUsuario(emailCelularBean.getNoCelular());
	    			session.getMensajeUSR().setCompCelularSuperUsuario(emailCelularBean.getCarrier());
	    			session.getMensajeUSR().setCompCelularUsuario(emailCelularBean.getCarrier());
	    		} else {
	    			emailCelularBean = consultaMailCelular(session.getMensajeUSR().getClaveSuperUsuario(), req);

	    			session.getMensajeUSR().setEmailSuperUsuario(emailCelularBean.getCorreo());
	    			session.getMensajeUSR().setLadaSuperUsuario(emailCelularBean.getLada());
	    			session.getMensajeUSR().setNoCelularSuperUsuario(emailCelularBean.getNoCelular());
	    			session.getMensajeUSR().setCompCelularSuperUsuario(emailCelularBean.getCarrier());

	    			//Primero se consulta el superusuario, para que al final queden las variables de session statusCorreo y statusCelular
	    			//con los valores del usuario operador

	    			emailCelularBean = consultaMailCelular(session.getUserID8(), req);

	    			session.getMensajeUSR().setEmailUsuario(emailCelularBean.getCorreo());
	    			session.getMensajeUSR().setLadaUsuario(emailCelularBean.getLada());
	    			session.getMensajeUSR().setNoCelularUsuario(emailCelularBean.getNoCelular());
	    			session.getMensajeUSR().setCompCelularUsuario(emailCelularBean.getCarrier());
	    		}

	    		if (session.getStatusCelular().equals("X") && session.getStatusCorreo().equals("X")) {
	    			session.setCampaniaActiva(false);
	    			EIGlobal.mensajePorTrace ("Campa�a de actualizacion de datos desactivada debido a que es imposible consultar sus datos ", EIGlobal.NivelLog.INFO);
	    		} else {
	    			session.setCampaniaActiva(true);
	    			EIGlobal.mensajePorTrace ("Campa�a de actualizacion de datos activada: StatusCelular " + session.getStatusCelular() + " , StatusCorreo " + session.getStatusCorreo(), EIGlobal.NivelLog.INFO);
	    		}

	    		session.getMensajeUSR().setEmailContrato(mailContrato(session.getContractNumber()));
	    		session.setCampaniaCtoActiva(true);
    		} catch (Exception e) {
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}
    	}

    	/**
    	 * Metodo para obtener el correo del contrato
    	 * @param numContrato -> Numero del contrato a obtener su correo
    	 * @return mailContrato -> Correo obtenido
    	 */
    	private String mailContrato(String numContrato){
    		String query = "";
    		String mailContrato = "";
    		ResultSet rs = null;

    		try {
    			Connection conn= createiASConn (Global.DATASOURCE_ORACLE );
    			Statement sDup = conn.createStatement();
    			query = "select email ".concat("from tct_cuentas_tele ").concat("where num_cuenta='").concat(numContrato).concat("'");
    			EIGlobal.mensajePorTrace ("BaseServlet::mailContrato:: -> Query:" + query, EIGlobal.NivelLog.INFO);
    			rs = sDup.executeQuery(query);

    			if(rs.next()) {
    				mailContrato = rs.getString("email");
    			}

    			rs.close();
    			sDup.close();
    			conn.close();
    		} catch (Exception e) {
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}

    		return mailContrato;
    	}

    	/**
    	 * Metodo encargado de consultar si la bandera de cierre de session en cambio de ip est� habilitada
    	 * @return valorBandera, valor de la bandera de cierre se session
    	 */
    	public boolean banderaCierraSession() {

    		String query = "";
    		String valorBanderaString = "1";
    		boolean valorBandera = false;
    		ResultSet rs = null;

    		try {
    			Connection conn = createiASConn (Global.DATASOURCE_ORACLE);
    			Statement sDup = conn.createStatement();

    			//query = "select VALOR as valor from EWEB_PARAMETROS_WEBGFSS where ID='FIN_SESION'";
    			query = "select VALOR as valor from EWEB_PARAMETROS@LWEBGFSS where ID='FIN_SESION'";
    			EIGlobal.mensajePorTrace ("BaseServlet::consultaBanderaCierraSession:: -> Query:" + query, EIGlobal.NivelLog.INFO);
    			rs = sDup.executeQuery(query);

    			if(rs.next()) {
    				valorBanderaString = rs.getString("valor");
    			}

    			//valor=0 (bandera activa)
    			//valor=1 (bandera desactivada)
    			if("0".equals(valorBanderaString)) {
    				valorBandera = true;
    			}

    			rs.close();
    			sDup.close();
    			conn.close();
    		} catch (SQLException e) {
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}

    		return valorBandera;
    	}

    	/**
    	 * Metodo encargado de consultar si el contrato y usuario se encuentran en la tabla de excepciones
    	 * @param noContrato -> Contrato del que se busca la excepcion de cambio de ip
    	 * @param codUsuario -> Usuario del que se busca la excepcion de cambio de ip
    	 * @return -> Valor para saber si el usuario mas contrato estan en la tabla de excepciones de cambio de ip
    	 */
    	public boolean excepCierreSession(String noContrato, String codUsuario) {

    		String query = "";
    		boolean excepcion = false;
    		ResultSet rs = null;

    		try {
    			Connection conn = createiASConn (Global.DATASOURCE_ORACLE);
    			Statement sDup = conn.createStatement();

    			//query = "select NUM_CONTRATO as contrato from EWEB_IP_EXCEPCIONES where NUM_CONTRATO='"
    			query = "select NUM_CONTRATO as contrato from EWEB_IP_EXCEPCIONES@LWEBGFSS where NUM_CONTRATO='"
    					/*Plan de calidad Enlace -  Validacion de solo contrato en cambio de IP*/
    					//.concat(noContrato).concat("' and COD_CLIENTE='").concat(codUsuario) // Se elimina
    					.concat(noContrato)
    					.concat("' and CANAL='ENLACE'");

    			EIGlobal.mensajePorTrace ("BaseServlet::consultaExcepcionCierraSession:: -> Query:" + query, EIGlobal.NivelLog.INFO);
    			rs = sDup.executeQuery(query);

    			if(rs.next()) {
    				excepcion = true;
    			}

    			rs.close();
    			sDup.close();
    			conn.close();
    		} catch (SQLException e) {
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}
    		EIGlobal.mensajePorTrace("Excepcion de cambio de ip Contrato [" + noContrato + "] Usuario [" +
					codUsuario + "] " + excepcion , EIGlobal.NivelLog.INFO);


    		return excepcion;
    	}

    	/**
    	 * Metodo encargado de consultar el buc del contrato
    	 * @param noContrato -> Contrato
    	 * @return -> bucContrato, valor de NUM_PERSONA
    	 */
    	public String recuperaBucContrato(String noContrato) {

    		String query = "";
    		String bucContrato = "";
    		ResultSet rs = null;

    		try {
    			Connection conn = createiASConn (Global.DATASOURCE_ORACLE);
    			Statement sDup = conn.createStatement();

    			query = "select NUM_PERSONA as bucContrato from NUCL_CUENTAS where NUM_CUENTA='"
    					.concat(noContrato).concat("'");

    			EIGlobal.mensajePorTrace ("BaseServlet::consultaNumCuenta:: -> Query:" + query, EIGlobal.NivelLog.INFO);
    			rs = sDup.executeQuery(query);

    			if(rs.next()) {
    				bucContrato = rs.getString("bucContrato");
    			}

    			rs.close();
    			sDup.close();
    			conn.close();
    		} catch (SQLException e) {
    			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		}

    		return bucContrato;
    	}




/********************************************/
/** Metodo: evalTemplate
 * @return void
 * @param String -> template
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 */
/********************************************/
     public void forwardTemplate (String template, HttpServletRequest req, HttpServletResponse res)
     throws ServletException, IOException {
	 RequestDispatcher disp = getServletContext ().getRequestDispatcher (template);
	res.setContentType("text/html");
	 disp.forward (req,res);
     }


/********************************************/
/** Metodo: desconectando
 * @return void
 */
/********************************************/
     public void desconectando (){}

/********************************************/
/** Metodo: obten_referencia_operacion
 * @return int
 * @param short -> suc_opera
 */
/********************************************/
     public int obten_referencia_operacion ( short suc_opera ) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::obten_reeferencia_operacion( " + suc_opera + " )",
    			 EIGlobal.NivelLog.DEBUG);
	 String buffer = "";
	 String coderror = "";
	 int numeroReferencia = 0;
	 String REFE_OK_CODE = "REFE0000";
	 Hashtable htResult = null;
	 try {
	     ServicioTux ctRemoto = new ServicioTux ();
	     //ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	     htResult = ctRemoto.sreferencia ( IEnlace.SUCURSAL_OPERANTE );
         coderror = ( String ) htResult.get ( "COD_ERROR" );

	 if ( coderror.equals ( REFE_OK_CODE ) ) {
	     numeroReferencia = ( ( Integer ) htResult.get ( "REFERENCIA" ) ).intValue ();
	 } else {
	     buffer = "NO SE EFECTUO LA referencia";
	 }

	 } catch ( java.rmi.RemoteException re ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
                 EIGlobal.mensajePorTrace ("E->obten_referencia_operacion->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
	 } catch (Exception e) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 return numeroReferencia;

     }

/********************************************/
/** Metodo: llamada_bitacora
 * @return String
 * @param int -> numeroReferencia
 * @param String -> coderror1
 * @param String -> contrato
 * @param String -> usuario
 * @param String -> cuenta
 * @param double -> importe
 * @param String -> tipo_operacion
 * @param String -> cuenta2
 * @param int -> cant_titulos
 * @param String -> dispositivo2
 */
/********************************************/
     public String llamada_bitacora ( int  numeroReferencia, String coderror1,String contrato, String usuario,String cuenta, double importe,String tipo_operacion, String cuenta2, int cant_titulos,String dispositivo2 ) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Entrando a llamada_bitacora ", EIGlobal.NivelLog.DEBUG);
	 String coderror    = "";
	 Hashtable htResult = null;
	 try {
	     //TuxedoGlobal ctRemoto = ( TuxedoGlobal ) session.getEjbTuxedo();
	     ServicioTux ctRemoto = new ServicioTux ();
	     //ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	     htResult = ctRemoto.sbitacora ( numeroReferencia, coderror1,
	     contrato, usuario, cuenta, importe, tipo_operacion,
	     cuenta2, cant_titulos, dispositivo2 );
	 } catch ( java.rmi.RemoteException re ) {
                 EIGlobal.mensajePorTrace ("E->llamada_bitacora->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 String BITACORA_OK_CODE = "BITA0000";
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesa resultado: " + htResult, EIGlobal.NivelLog.DEBUG);
	 coderror = ( String ) htResult.get ( "COD_ERROR" );
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesa coderror: " + coderror, EIGlobal.NivelLog.DEBUG);
	 if ( coderror == null )
	     coderror = "BITACORA ERROR";
	 if ( coderror.equals (BITACORA_OK_CODE ) ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BITACORA OK, termina",EIGlobal.NivelLog.DEBUG);
	 } else {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BITACORA ERROR, termina",EIGlobal.NivelLog.ERROR);
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Saliendo de llamada_bitacora ",EIGlobal.NivelLog.DEBUG);
	 return coderror;
     }

/********************************************/
/** Metodo: checar_si_es_programada
 * @return boolean
 * @param String -> fecha
 */
/********************************************/
     public boolean checar_si_es_programada ( String fecha ) {
	 boolean programada;
	 Integer idia;
	 Integer imes;
	 Integer  ianio;
	 int dia=0,mes=0,anio=0;
	 String sdia="",smes="",sanio="";
	 sdia=fecha.substring (0, 2);
	 smes=fecha.substring (3,5);
	 sanio=fecha.substring (6,fecha.length ());
	 idia=new Integer (sdia);
	 imes=new Integer (smes);
	 ianio=new Integer (sanio);
	 dia=idia.intValue ();
	 mes=imes.intValue ();
	 anio=ianio.intValue ();
	 java.util.Date dia_hoy = new java.util.Date ();
	 GregorianCalendar cal1 = new GregorianCalendar ();
	 GregorianCalendar cal2 = new GregorianCalendar ();
	 cal1.setTime (dia_hoy);
	 mes--;
	 cal2.set (anio,mes,dia);
	 if ( cal2.get ( Calendar.YEAR ) > cal1.get ( Calendar.YEAR ) ) {
	     programada = true;
	 } else if ( cal2.get ( Calendar.YEAR ) == cal1.get ( Calendar.YEAR ) ) {
	     if ( cal2.get ( Calendar.MONTH ) > cal1.get ( Calendar.MONTH ) ) {
		 programada = true;
	     } else if ( cal2.get ( Calendar.MONTH ) == cal1.get ( Calendar.MONTH ) ) {
	    	 if ( cal2.get ( Calendar.DATE ) > cal1.get ( Calendar.DATE ) ) {
		     programada = true;
	    	 } else {
		     programada = false;
	    	 }
	     } else {
	    	 programada = false;
	     }
	 } else {
	     programada = false;
	 }
	 return programada;
     }

/********************************************/
/** Metodo: ObtenHora
 * @return String
 */
/********************************************/
     public String ObtenHora () {
	 String hora = "";
	 String[] AM_PM = {"AM","PM"};
	 int minutos =0;
	 String sminutos ="";
	 java.util.Date Hoy = new java.util.Date ();
	 GregorianCalendar Cal = new GregorianCalendar ();
	 Cal.setTime (Hoy);
	 if (AM_PM[Cal.get (Calendar.AM_PM)].equals ("AM")) {
	     minutos=Cal.get (Calendar.MINUTE);
	     sminutos=((minutos < 10) ? "0" : "") + minutos;
	     hora =  ((Cal.get (Calendar.HOUR)%12))+":"+ sminutos + "  a.m.";
	 }
	 else {
	     minutos=Cal.get (Calendar.MINUTE);
	     sminutos=((minutos < 10) ? "0" : "") + minutos;
	     hora =  ((Cal.get (Calendar.HOUR)%12)+12)+":"+ sminutos + " p.m.";
	 }
	 return hora;
     }

	/**
	 * Obten anio.
	 *
	 * @return the string
	 */
	String ObtenAnio ()
	{
		int ianio;
		String sanio;
		java.util.Date Hoy = new java.util.Date ();
		GregorianCalendar Cal = new GregorianCalendar ();
		Cal.setTime (Hoy);
		ianio=Cal.get (Calendar.YEAR);
		sanio = Integer.toString (ianio);
		return sanio;
	}

	/**
	 * Obten mes.
	 *
	 * @return the string
	 */
	String ObtenMes ()
	{
		int imes=0;
		String smes="";
		java.util.Date Hoy = new java.util.Date ();
		GregorianCalendar Cal = new GregorianCalendar ();
		Cal.setTime (Hoy);
		imes=Cal.get (Calendar.MONTH);
		imes++;
		smes=((imes < 10) ? "0" : "") + imes;
		return smes;
	}

	/**
	 * Obten dia.
	 *
	 * @return the string
	 */
	String ObtenDia ()
	{
		int idia=0;
		String sdia="";
		java.util.Date Hoy = new java.util.Date ();
		GregorianCalendar Cal = new GregorianCalendar ();
		Cal.setTime (Hoy);
		idia=Cal.get (Calendar.DATE);
		sdia=((idia < 10) ? "0" : "") + idia;
		return sdia;
	}

     /**
      * Poner_campos_fecha.
      *
      * @param fac_programadas the fac_programadas
      * @return the string
      */
     String poner_campos_fecha (boolean fac_programadas) {
	 String dia_forma=ObtenDia ();
	 String mes_forma=ObtenMes ();
	 String anio_forma=ObtenAnio ();
	 String campos_fecha="";
	 if (fac_programadas==true) {
	     campos_fecha="<INPUT ID=dia TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
	     campos_fecha=campos_fecha+"<INPUT ID=mes TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
	     campos_fecha=campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";
	     campos_fecha=campos_fecha+"<INPUT ID=fecha_completa TYPE=TEXT SIZE=10 NAME=fecha_completa VALUE="+dia_forma+"/"+mes_forma+"/"+anio_forma+" OnFocus=\"blur();\">";
	     campos_fecha=campos_fecha+"<A HREF=\"javascript:WindowCalendar1();\"><IMG src=\"/gifs/EnlaceMig/gbo25410.gif\" width=12 height=14 border=0 align=absmiddle></A>";
	     //log("Campos fecha: "+campos_fecha);
	 }
	 else {
	     campos_fecha=dia_forma+"/"+mes_forma+"/"+anio_forma;
	     campos_fecha=campos_fecha+"<INPUT ID=dia  TYPE=HIDDEN NAME=dia VALUE="+dia_forma +" SIZE=2 MAXLENGTH=2> ";
	     campos_fecha=campos_fecha+"<INPUT ID=mes  TYPE=HIDDEN NAME=mes VALUE="+mes_forma +" SIZE=2 MAXLENGTH=2> ";
	     campos_fecha=campos_fecha+"<INPUT ID=anio TYPE=HIDDEN NAME=anio VALUE="+anio_forma+" SIZE=4 MAXLENGTH=4> ";
	     //log("Campos fecha: "+campos_fecha);
	 }
	 return campos_fecha;
     }

     /**
      * Formatea_importe.
      *
      * @param importe the importe
      * @return the string
      */
     String formatea_importe (String importe) {
	 //log("Entrando formatea_importe"+importe);
	 String importe_sin_punto="";
	 String parte_decimal="";
	 int	posi_importe1=0;
	 String cadena_final="";
	 posi_importe1=importe.indexOf (".");
	 if (posi_importe1==-1) {
	     importe_sin_punto=importe;
	     parte_decimal=".00";
	 }
	 else {
	     importe_sin_punto=importe.substring (0,importe.indexOf ("."));
	     if (importe_sin_punto.length ()+1 == importe.length ()) {
		 //log ("teclee X.");
		 parte_decimal=".00";
	     }
	     else {
		 parte_decimal=importe.substring (importe.indexOf (".")+1,importe.length ());
	    	 if (parte_decimal.length ()==1) {
		     parte_decimal=parte_decimal+"0";
	    	 } else if (parte_decimal.length ()>2) {
		     parte_decimal=parte_decimal.substring (0, 2);
	    	 }
		 parte_decimal="."+parte_decimal;
	     }
	 }
	 String cadena_importe="";
	 String cadena_importeres="";
	 int contador=1;
	 int valor=0;
	 for(int i=importe_sin_punto.length ()-1;i>=0;i--) {
	     valor=contador%3;
	     contador++;
	     cadena_importeres=cadena_importe;
	     cadena_importe=importe_sin_punto.charAt (i)+cadena_importeres;
	     if ((valor==0)&&(contador<=importe_sin_punto.length ()))
		 cadena_importe=","+cadena_importe;
	 }
	 cadena_importe=cadena_importe+parte_decimal;
	 cadena_final="$"+cadena_importe;
	 return cadena_final;
     }

     /**
      * Formatea_importe.
      *
      * @param importe the importe
      * @param simbolo the simbolo
      * @return the string
      */
     String formatea_importe (String importe, boolean simbolo) {
	 EIGlobal.mensajePorTrace ("Entrando formatea_importe 2: "+importe, EIGlobal.NivelLog.DEBUG);
	 String importe_sin_punto="";
	 String parte_decimal="";
	 int	posi_importe1=0;
	 String cadena_final="";
	 posi_importe1=importe.indexOf (".");
	 if (posi_importe1==-1) {
	     importe_sin_punto=importe;
	     parte_decimal=".00";
	 }
	 else {
	     importe_sin_punto=importe.substring (0,importe.indexOf ("."));
	     if (importe_sin_punto.length ()+1 == importe.length ()) {
		 parte_decimal=".00";
	     }
	     else {
		 parte_decimal=importe.substring (importe.indexOf (".")+1,importe.length ());

		 if (parte_decimal.length ()==1) {
		     parte_decimal=parte_decimal+"0";
		 } else if (parte_decimal.length ()>2) {
		     parte_decimal=parte_decimal.substring (0, 2);
		 }
		 parte_decimal="."+parte_decimal;
	     }
	 }
	 String cadena_importe="";
	 String cadena_importeres="";
	 int contador=1;
	 int valor=0;
	 for(int i=importe_sin_punto.length ()-1;i>=0;i--) {
	     valor=contador%3;
	     contador++;
	     cadena_importeres=cadena_importe;
	     cadena_importe=importe_sin_punto.charAt (i)+cadena_importeres;
	     if ((valor==0)&&(contador<=importe_sin_punto.length ()))
		 cadena_importe=","+cadena_importe;
	 }
	 cadena_importe=cadena_importe+parte_decimal;
	 if (simbolo) {
		 cadena_final="$"+cadena_importe;
	 } else {
		 cadena_final=cadena_importe;
	 }
	 return cadena_final;
     }
     //Devuelve los dias inhabiles del dia anterior hasta 4 meses atras

/********************************************/
/** Metodo: diasInhabilesAnt
 * @return String
 */
/********************************************/
     public String diasInhabilesAnt () {
	 String  diasInhabiles = "";
	 String sqlDias = "Select to_char(fecha, 'dd/mm/yyyy') From comu_dia_inhabil" +
	 " Where cve_pais = 'MEXI' and fecha < sysdate and fecha > ADD_MONTHS(sysdate,-4)";
	Connection conn=null;
	PreparedStatement psDias=null;
	ResultSet rsDias =null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement psDias = conn.prepareStatement ( sqlDias );
	     //ResultSet rsDias = psDias.executeQuery ();
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     psDias = conn.prepareStatement ( sqlDias );
	     rsDias = psDias.executeQuery ();

	     while ( rsDias.next () ) {
	    	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Leyendo fecha", EIGlobal.NivelLog.DEBUG);
		 String fecha  = rsDias.getString ( 1 );
		 	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "fecha no habil " + fecha , EIGlobal.NivelLog.DEBUG);
		 diasInhabiles += ", " + fecha;
	     }
	     //rsDias.close ();
	     //psDias.close ();
	     //conn.close ();
	     if (diasInhabiles.trim ().length ()==0){
		 diasInhabiles = ", ";
	     }
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::diasInhabilesAnt()::" +
			     "ERROR: Creando conexin en:", EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	 }finally{
			try{
				if(rsDias!=null){
					rsDias.close();
					rsDias=null;
				}
				if(psDias!=null){
					psDias.close();
					psDias=null;
				}
				if(conn!=null){
					conn.close();
					conn=null;
				}
			}catch(Exception e1){
				EIGlobal.mensajePorTrace ("BaseServlet-diasInhabilesAnt-> "+e1, EIGlobal.NivelLog.DEBUG);
                                EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
			}
	 }
	 return ( diasInhabiles );
     }
     //Devuelve los dias inhabiles del dia actual hasta 3 meses despues

/********************************************/
/** Metodo: diasInhabilesDesp
 * @return String
 */
/********************************************/
     public String diasInhabilesDesp ()
     {
	 String  diasInhabiles = "";
	 String sqlDias = "Select to_char(fecha, 'dd/mm/yyyy') From comu_dia_inhabil" +
	 " Where cve_pais = 'MEXI' and fecha >= sysdate and fecha < ADD_MONTHS(sysdate,3)";
	Connection conn= null;
	Statement psDias = null;
	ResultSet rsDias = null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //Statement psDias = conn.createStatement (	);
	     //ResultSet rsDias = psDias.executeQuery (sqlDias);
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     psDias = conn.createStatement (	);
	     rsDias = psDias.executeQuery (sqlDias);
	     while ( rsDias.next () ) {
	    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Leyendo fecha", EIGlobal.NivelLog.DEBUG);
		 String fecha  = rsDias.getString ( 1 );
		 	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "fecha no habil " + fecha, EIGlobal.NivelLog.DEBUG);
		 diasInhabiles += ", " + fecha;
	     }
	     //rsDias.close ();
	     //psDias.close ();
	     //conn.close ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::diasInhabilesAnt()::" +
			     "E: Creando conexion en:",EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	 }finally{
				try{
					if(rsDias!=null){
						rsDias.close();
						rsDias=null;
					}
					if(psDias!=null){
						psDias.close();
						psDias=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace ("BaseServlet-diasInhabilesDesp-> "+e1, EIGlobal.NivelLog.DEBUG);
                                        EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
				}
	 }
	 return ( diasInhabiles );
     }
     //Devuelve la descripcion del Banco

/********************************************/
/** Metodo: descBanco
 * @return String
 * @param String -> cveBanco
 */
/********************************************/
     public String descBanco (String cveBanco) {
	 String  descBanco = "";
	 String sqlDias = "Select nombre_largo From comu_interme_fin Where cve_interme = '" +
	 cveBanco + "'";
	Connection conn= null;
	PreparedStatement psDias = null;
	ResultSet rsDias =  null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement psDias = conn.prepareStatement ( sqlDias );
	     //ResultSet rsDias = psDias.executeQuery ();
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     psDias = conn.prepareStatement ( sqlDias );
	     rsDias = psDias.executeQuery ();
	     rsDias.next ();
	     descBanco = rsDias.getString ( 1 );
	     //rsDias.close ();
	     //psDias.close ();
	     //conn.close ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::diasInhabilesAnt()::" +
			     "E: Creando conexion en:", EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	 }finally{
				try{
					if(rsDias!=null){
						rsDias.close();
						rsDias=null;
					}
					if(psDias!=null){
						psDias.close();
						psDias=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
                                         EIGlobal.mensajePorTrace ("BaseServlet-descBanco-> "+e1, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
				}
	 }
	 return ( descBanco );
     }

/********************************************/
/** Metodo: descPlaza
 * @return String
 * @param String -> cvePlaza
 */
/********************************************/
     public String descPlaza ( String cvePlaza ) { //Devuelve la descripcion de la Plaza
	 String  descPlaza = "";
	 String sqlDias = "Select descripcion From comu_pla_banxico Where plaza_banxico = '" +
	 cvePlaza + "'";

	Connection conn= null;
	PreparedStatement psDias =null;
	ResultSet rsDias = null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement psDias = conn.prepareStatement ( sqlDias );
	     //ResultSet rsDias = psDias.executeQuery ();
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     psDias = conn.prepareStatement ( sqlDias );
	     rsDias = psDias.executeQuery ();
	     if(rsDias.next()) {
	         descPlaza = rsDias.getString ( 1 );
            }
            else {
                descPlaza = " ";
            }
	     //rsDias.close ();
	     //psDias.close ();
	     //conn.close ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::diasInhabilesAnt()::" +
			     "E: Creando conexin en:", EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	 }finally{
				try{
					if(rsDias!=null){
						rsDias.close();
						rsDias=null;
					}
					if(psDias!=null){
						psDias.close();
						psDias=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
	                                EIGlobal.mensajePorTrace ("BaseServlet-descPlaza-> "+e1, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
				}
	 }
	 return ( descPlaza );
     }

/********************************************/
/** Metodo: despliegaPaginaErrorURL
 * @return void
 * @param String -> Error
 * @param String -> param1
 * @param String -> param2
 * @param String -> param3
 * @param String -> pagina
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
     public void despliegaPaginaErrorURL ( String Error, String param1, String param2, String param3, String pagina, HttpServletRequest request, HttpServletResponse response )
     throws IOException, ServletException {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 EIGlobal Global = new EIGlobal (session , getServletContext () , this	);
	 request.setAttribute ( "FechaHoy", Global.fechaHoy ("dt, dd de mt de aaaa"));
	 request.setAttribute ( "Error", Error );
	 request.setAttribute ( "MenuPrincipal", session.getStrMenu () );
	 request.setAttribute ( "newMenu", session.getFuncionesDeMenu ());
	 request.setAttribute ( "Encabezado", CreaEncabezado ( param1, param2, param3 ,request) );
	 String contrato_ = session.getContractNumber ();
	 if(contrato_!=null) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato no es nulo ="+contrato_, EIGlobal.NivelLog.DEBUG);
	 } else {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato es nulo le ponemos cadena vacia",EIGlobal.NivelLog.DEBUG);
	     contrato_ ="";
	 }
	 request.setAttribute ( "NumContrato", contrato_ );
	 request.setAttribute ( "NomContrato", session.getNombreContrato () );
	 request.setAttribute ( "NumUsuario", session.getUserID8 () );
	 request.setAttribute ( "NomUsuario", session.getNombreUsuario () );
	 request.setAttribute ( "URL", pagina);
	 RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/EI_Mensaje.jsp" );
	response.setContentType("text/html");
	 rdSalida.include ( request, response );
     }

/********************************************/
/** Metodo: despliegaPaginaError
 * @return void
 * @param String -> Error
 * @param String -> param1
 * @param String -> param2
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public void despliegaPaginaError ( String Error, String param1, String param2, HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession ();
		BaseResource session = null;
		if(sess != null) {
			 session = (BaseResource) sess.getAttribute ("session");
			 if(session == null) {
				 session = new BaseResource();
			 }
		 }
		 else {
			 session = new BaseResource();
		 }
		EIGlobal Global = new EIGlobal (session , getServletContext () , this);
		request.setAttribute ( "FechaHoy",Global.fechaHoy ( "dt, dd de mt de aaaa" ) );
		request.setAttribute ( "Error", Error );
		request.setAttribute ( "MenuPrincipal", session.getStrMenu () );
		request.setAttribute ( "newMenu", session.getFuncionesDeMenu ());
		request.setAttribute ( "Encabezado", CreaEncabezado ( param1, param2, "" ,request) );
		String contrato_ = session.getContractNumber ();
		if(contrato_!=null)
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato no es nulo = >" +contrato_+ "<",EIGlobal.NivelLog.DEBUG);
		else {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato es nulo le ponemos cadena vacia",EIGlobal.NivelLog.DEBUG);
			contrato_ ="";
		}
		request.setAttribute ( "NumContrato", contrato_ );
		request.setAttribute ( "NomContrato", session.getNombreContrato () );
		request.setAttribute ( "NumUsuario", session.getUserID8 () );
		request.setAttribute ( "NomUsuario", session.getNombreUsuario () );
		RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/EI_Error.jsp" );
		response.setContentType("text/html");
		rdSalida.include ( request, response );
	}

/********************************************/
/** Metodo: despliegaPaginaError
 * @return void
 * @param String -> Error
 * @param String -> param1
 * @param String -> param2
 * @param String -> param3
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public void despliegaPaginaError ( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
	 HttpSession sess = request.getSession ();
	 BaseResource session = null;
	 if(sess != null) {
		 session = (BaseResource) sess.getAttribute ("session");
		 if(session == null) {
			 session = new BaseResource();
		 }
	 }
	 else {
		 session = new BaseResource();
	 }
	 EIGlobal Global = new EIGlobal (session , getServletContext () , this	);
	 //EIGlobal Global = new EIGlobal( this );
	 request.setAttribute ( "FechaHoy",Global.fechaHoy ( "dt, dd de mt de aaaa" ) );
	 request.setAttribute ( "Error", Error );
	 request.setAttribute ( "MenuPrincipal", session.getStrMenu () );
	 request.setAttribute ( "newMenu", session.getFuncionesDeMenu ());
	 request.setAttribute ( "Encabezado", CreaEncabezado ( param1, param2, param3 ,request) );
	 String contrato_ = session.getContractNumber ();
	 if(contrato_!=null) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato no es nulo ="+contrato_,EIGlobal.NivelLog.DEBUG);
	 } else {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato es nulo le ponemos cadena vacia",EIGlobal.NivelLog.DEBUG);
	     contrato_ ="";
	 }
	 request.setAttribute ( "NumContrato", contrato_ );
	 request.setAttribute ( "NomContrato", session.getNombreContrato () );
	 request.setAttribute ( "NumUsuario", session.getUserID8 () );
	 request.setAttribute ( "NomUsuario", session.getNombreUsuario () );
	 RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/EI_Error.jsp" );
	    response.setContentType("text/html");
	 rdSalida.include ( request, response );
     }

	/**
	 * Despliega mensaje de error general
	 * @param Error variable de error
	 * @param param1 parametro uno
	 * @param param2 parametro dos
	 * @param param3 parametro tres
	 * @param request variable HttpServletRequest
	 * @param response variable HttpServletResponse
	 * @throws IOException excepcion del metodo
	 * @throws ServletException excepcion del servlet
	 */
	public void despliegaPaginaMsjError ( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response )
	throws IOException, ServletException
	{
	 HttpSession sess = request.getSession ();
	 BaseResource session = null;
	 if(sess != null) {
		 session = (BaseResource) sess.getAttribute ("session");
		 if(session == null) {
			 session = new BaseResource();
		 }
	 }
	 else {
		 session = new BaseResource();
	 }
	 EIGlobal Global = new EIGlobal (session , getServletContext () , this	);
	 request.setAttribute ( "FechaHoy",Global.fechaHoy ( "dt, dd de mt de aaaa" ) );
	 request.setAttribute ( "Error", Error );
	 request.setAttribute ( "MenuPrincipal", session.getStrMenu () );
	 request.setAttribute ( "newMenu", session.getFuncionesDeMenu ());
	 request.setAttribute ( "Encabezado", CreaEncabezado ( param1, param2, param3 ,request) );
	 String contrato_ = session.getContractNumber ();
	 if(null != contrato_) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato no es nulo ="+contrato_,EIGlobal.NivelLog.DEBUG);
	 } else {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El contrato es nulo le ponemos cadena vacia",EIGlobal.NivelLog.DEBUG);
	     contrato_ ="";
	 }
	 request.setAttribute ( "NumContrato", contrato_ );
	 request.setAttribute ( "NomContrato", session.getNombreContrato () );
	 request.setAttribute ( "NumUsuario", session.getUserID8 () );
	 request.setAttribute ( "NomUsuario", session.getNombreUsuario () );
	 RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/EI_Msjerror.jsp" );
	    response.setContentType("text/html");
	 rdSalida.include ( request, response );
	 }

/********************************************/
/** Metodo: despliegaPaginaError
 * @return void
 * @param String -> Error
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
     public void despliegaPaginaError ( String Error, HttpServletRequest request,HttpServletResponse response ) throws IOException, ServletException {
	 //EIGlobal Global=new EIGlobal( this );
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 EIGlobal Global = new EIGlobal (session , getServletContext () , this	);
	 request.setAttribute ( "FechaHoy", Global.fechaHoy ( "dt, dd de mt de aaaa" ) );
	 request.setAttribute ( "Error", Error );
	 request.setAttribute ( "MenuPrincipal", session.getStrMenu () );
	 request.setAttribute ( "newMenu", session.getFuncionesDeMenu () );
	 request.setAttribute ( "Encabezado", CreaEncabezado ( "Error en la Aplicaci&oacute;n",
	 "La operaci&oacute;n no se pudo Completar", "",request ) );
	 request.setAttribute ( "NumContrato", session.getContractNumber () );
	 request.setAttribute ( "NomContrato", session.getNombreContrato () );
	 request.setAttribute ( "NumUsuario", session.getUserID8 () );
	 request.setAttribute ( "NomUsuario", session.getNombreUsuario () );
	 RequestDispatcher rdSalida = getServletContext ().getRequestDispatcher ( "/jsp/EI_Error.jsp" );
	    response.setContentType("text/html");
	 rdSalida.include ( request, response );
     }

/********************************************/
/** Metodo: llamada_valida_manc
 * @return String[]
 * @param String -> trama
 * @param int -> servicioManc
 */
/********************************************/
     public String[] llamada_valida_manc ( String trama, int servicioManc ) {
	 //fecha,referencia,contrato,tipo_operacion,usuario,cuenta,cuenta2,
	 //procedencia,importe,propter,propter2,aux,op_prog,fch_prog
	 // Trama Completa
	 // Los campos pueden ir vacios, dependera de la accion a realizar
	 //
	 //fch_registro,  folio_registro, contrato,  tipo_operacion,fch_autorizac,
	 //folio_autorizac, usuario,  cta_origen, cta_destino,banco,  sucursal,  plaza,
	 //concepto2,	importe,  estatus,tipo_origen,	tipo_destino,  plazo,
	 //instruccion,  titulos,  titular,beneficiario,  referencia_medio, op_prog, fch_prog
	 String result_manc[] = new String [2];
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::llamada_valida_manc( " +trama+ ", " +servicioManc+ " )",
			 EIGlobal.NivelLog.DEBUG);
	 String buffer = "";
	 String coderror = "";
	 String MANC_VALIDA_OK_CODE = "MANC0000";
	 String MANC_VALIDA_GENERIC_ERROR = "MANC9999";
	 Hashtable htResult = null;
	 ServicioTux ctRemoto = new ServicioTux ();
	 //ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	 switch ( servicioManc ) {
	     case 1:
		 try {
		     htResult = ctRemoto.manc_cancela ( trama );
		 } catch ( java.rmi.RemoteException re ) {
                         EIGlobal.mensajePorTrace ("E->llamada_valida_manc->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		 } catch (Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 }
		 break;
	     case 2:
		 try {
		     htResult = ctRemoto.manc_act_est ( trama );
		 } catch ( java.rmi.RemoteException re ) {
                       EIGlobal.mensajePorTrace ("E->llamada_valida_manc->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		 }	catch (Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 }
		 break;
	     default:
		 try {
		     htResult = ctRemoto.manc_valida ( trama );
		 } catch ( java.rmi.RemoteException re ) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
                         EIGlobal.mensajePorTrace ("E->llamada_valida_manc->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
		 } catch(Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 }
		 break;
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Procesando resultado",EIGlobal.NivelLog.DEBUG);
	 coderror = ( String ) htResult.get ( "COD_ERROR" );
	 buffer = ( String ) htResult.get ( "BUFFER" );
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "COD_ERROR : >" +coderror+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.DEBUG);
	 if ( coderror == null ) {
	     buffer = "Problemas con MANC_VALIDA";
	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "CODERROR MANC_VALIDA NULO, ",EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.INFO);
	 } else if ( coderror.equals ( MANC_VALIDA_OK_CODE ) ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "MANC_VALIDA OK, const = >" +MANC_VALIDA_OK_CODE+ "<",EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.INFO);
	 } else {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "MANC_VALIDA ERROR",EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BUFFER : >" +buffer+ "<",EIGlobal.NivelLog.ERROR);
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Saliendo de llamada_valida_manc",EIGlobal.NivelLog.DEBUG);
	 result_manc[0] = coderror;
	 result_manc[1] = buffer;
	 return result_manc;
     }
	/*Metodo que revisa las facultades existentes en un archivo de ambiente, para formar con ellas
	 * un arreglo bidimensional de strings que almacenara en la primera columna el nombre de la facultad
	 * y en la segunda el location correspondiente a dicha facultad, los location seran pasados a una etiqueta
	 * GX
	 * autor: Rodrigo Godo, 26 de Abril de 2001
	 */

/********************************************/
/** Metodo: asignaURLS
 * @return String[][]
 * @param HttpServletRequest -> request
 * @param BaseResource -> session
 */
/********************************************/
	public String[][] asignaURLS (HttpServletRequest request, BaseResource session)
	{
		String unContrato = "";
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "CCB-AsignaURLIntegra",EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::asignaURLS()",EIGlobal.NivelLog.DEBUG);
		//se dimensiona el arreglo de 100 filas ya que ese es el numero de opciones existentes en el menu
		/*
		* Modificado por Miguel Cortes Arellano
		* Fecha 04/12/03
		* Empieza codigo Sinapsis
		* Proposito: Redimensionar el array para colocacion de la nueva opcion Consultas
		*				   Array index 110 es para consultas
		* 115 para Menu Tarjeta Credito
		*Se redimenciono el array matrizLocations a 116 para agregar la opcin Pago SAR en el menu servicios */
		//117 para Menu de Derechos, Productos y Aprochamientos  (vcl-16/ago/04)

		/*******************************************************/
		/* NUEVO MODULO ALTA DE CUENTAS                        */
		/*******************************************************/
		/*******************************************************/
		/* TESOFE					                           */
		/*******************************************************/
		//118 para Tesofe
		//119 para Consulta y autorizacion de cuentas
		//120 para pago de impuestos
		//String[][] matrizLocations = new String[121][2];


			/************************************************************************
		 *everis.
		 *Feb/2008
		 *Se modifica el men� para agregar las opciones
		 *		Catalogo de N�mina
		 *			Busqueda
		 *			Modificacion por Archivo
		 *			Baja por Archivo
		 *		Mantenimiento Intebancario
		 *			Alta en Linea
		 *			Alta por Archivo
		 *			Modificacion por Archivo
		************************************************************************/

		/*******************************************************/
		/* NUEVO MODULO CATALOGO DE NOMINA                     */
		/*******************************************************/

		//121 Catalogo de N�mina / Busqueda
		//122 Catalogo de N�mina / Modificacion por Archivo
		//123 Catalogo de N�mina / Baja de Archivo
		//124 Mantenimiento Interbancario / Alta en L�nea
		//125 Mantenimiento Interbancario / Alta por Archivo
		//126 Mantenimiento Interbancario / Modificacion por Archivo
		 //127 Nomina Interbancaria / Busqueda
		 //128 Autorizacion y Cancelacion

		 /*<vc>*/
		 	//Cambia el tama�o de matrizLocations de 152 a 153
		 	//String[][] matrizLocations = new String[152][2];
		 	//Utilizamos 152 para Catalogo de nomina / Alta por archivo
		 /*<vc>*/

		 /*<vc>*/
		 	//Cambia el tama�o de matrizLocations de 153 a 154
		 	//String[][] matrizLocations = new String[153][2];
		 	//Utilizamos 153 para Catalogo de nomina / Liberacion de empleados no registrados
		 /*<vc>*/

		 /*<P020101  Cuenta Nivel 3> - Se cambia el tama�o del menu
		157 Administracion de Remesas
		158 Consulta de Remesas de Remesas
		159 Asignacion Individual
		160 Asignacion por archivo
		161 Consulta Estado Asignacion
		</P020101  Cuenta Nivel 3>*/

		 /*<Nomina en Linea> - Se cambia el tama�o del menu
			165 Importacion de archivo
			166 Consulta Nomina en linea - Archivos
			</Nomina en Linea>*/

		String[][] matrizLocations;
		matrizLocations = new String[185][2]; //FSW - Vector - URL Nomina Pagos Epyme



		//Finaliza codigo Sinapsis
		//String[][] matrizLocations = new String[110][2];
		//*******************************************************************************************************
		//*********************************************** CONSULTAS *********************************************
		//*******************************************************************************************************
		try {
			//SALDOS> Por cuenta> Cheques
			if ( verificaFacultad ("CONSSALDOS", request) ) {
				matrizLocations[0][0]="CONSSALDOS";
				//<vswf:meg cambio de NASApp por Enlace 08122008>
				matrizLocations[0][1]="/Enlace/"+Global.WEB_APPLICATION+"/csaldo1?prog=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_SALDO_CUENTA_CHEQUE;
			} else {
				//<vswf:meg cambio de NASApp por Enlace 08122008>
				matrizLocations[0][0]="SinFacultades";
				matrizLocations[0][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//SALDOS>Por cuenta>Banca Especializada
			if ( verificaFacultad ("CONSSALDOS", request) )
			{
				//<vswf:meg cambio de NASApp por Enlace 08122008>
				matrizLocations[1][0]="CONSSALDOS";
				matrizLocations[1][1]="/Enlace/"+Global.WEB_APPLICATION+"/BE_ConsultaSaldos?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_SALDO_CUENTA_BANCA;
			} else {
				//<vswf:meg cambio de NASApp por Enlace 08122008>
				matrizLocations[1][0]="SinFacultades";
				matrizLocations[1][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//SALDOS>Por cuenta>Tarjeta de credito
			/*if(verificaFacultad("CONSSALDOTER",request))
			{
				matrizLocations[2][0]="CONSSALDOTER";
				matrizLocations[2][1]="MCL_Credito?Cuenta=5";
			} else {
				matrizLocations[2][0]="SinFacultades";
				matrizLocations[2][1]="SinFacultades";
			}*/
			//<vswf:meg cambio de NASApp por Enlace 08122008>
			matrizLocations[2][0]="NoNecesaria";
			matrizLocations[2][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_Credito?Cuenta=5&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_SALDO_CUENTA_TARJETA;
			//SALDOS>Consolidados>Cheques

			matrizLocations[3][0]="NoNecesaria";
			matrizLocations[3][1]="/Enlace/"+Global.WEB_APPLICATION+"/MSC_Inicio?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_SALDO_CONS_CHEQUE;
			//SALDOS>Consolidados>Banca Especializada
			/*matrizLocations[4][0]="EnConstruccion";
			matrizLocations[4][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
			//POSICION>Chequeras
			if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
				matrizLocations[5][0]="CONSSALDOS";
				matrizLocations[5][1]="/Enlace/"+Global.WEB_APPLICATION+"/c_posicion?opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_POSICION_CHEQUERAS;
			} else {
				matrizLocations[5][0]="SinFacultades";
				matrizLocations[5][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//POSICION>Banca Especializada
			if ( verificaFacultad ( "CONSPOSIPROP" ,request) ) {
				matrizLocations[6][0]="CONSPOSIPROP";
				matrizLocations[6][1]="/Enlace/"+Global.WEB_APPLICATION+"/BE_ConsultaPosicion?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_POSICION_BANCA;
			} else {
				matrizLocations[6][0]="SinFacultades";
				matrizLocations[6][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//POSICION>Tarjeta de Credito
			if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
				matrizLocations[7][0]="CONSSALDOS";
				matrizLocations[7][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_MenuPos?Tipo=Posicion&Cuenta=5&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_POSICION_TARJETA;
			} else {
				matrizLocations[7][0]="SinFacultades";
				matrizLocations[7][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Chequeras>Enlinea
			if ( verificaFacultad ( "CONSMOVTOS" ,request) ) {
				matrizLocations[8][0]="CONSMOVTOS";
				matrizLocations[8][1]="/Enlace/"+Global.WEB_APPLICATION+"/CMovimiento?prog=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_CHEQ_LINEA;
			} else {
				matrizLocations[8][0]="SinFacultades";
				matrizLocations[8][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Chequeras>Programados>Registro
			if ( verificaFacultad ( "CONSMOVTOS" ,request) ) {
				matrizLocations[106][0]="CONSMOVTOS";
				matrizLocations[106][1]="/Enlace/"+Global.WEB_APPLICATION+"/csaldo1?prog=1&prog1=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_CHEQ_PROG_REGISTRO;
			} else {
				matrizLocations[106][0]="SinFacultades";
				matrizLocations[106][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Chequeras>Programados>Consulta
			if ( verificaFacultad ( "CONSMOVTOS" ,request) ) {
				matrizLocations[107][0]="CONSMOVTOS";
				matrizLocations[107][1]="/Enlace/"+Global.WEB_APPLICATION+"/csaldo1?prog=5&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_CHEQ_PROG_CONSULTA; //CMovimiento?prog=1
			} else {
				matrizLocations[107][0]="SinFacultades";
				matrizLocations[107][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Chequeras>Interbancarias Recibidas
			if ( verificaFacultad ( "CONSMOVTOS" ,request) ) {
				matrizLocations[177][0]="CONSMOVTOS";
				matrizLocations[177][1]="/Enlace/"+Global.WEB_APPLICATION+"/InterbancariasRecibidas?Modulo=inicio&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_CHEQ_INT_RECIBIDAS;
			} else {
				matrizLocations[177][0]="SinFacultades";
				matrizLocations[177][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			
			//*******************INICIA TCS FSW 12/2016***********************************
			// 		MOVIMIENTOS>Chequeras>Interbancarias Recibidas Dolares
			if (verificaFacultad("CONSMOVTOS", request)) {
				matrizLocations[181][0] = "CONSMOVTOS";
				matrizLocations[181][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/InterbancariasRecibidasUSD?Modulo=inicio&" + BitaConstants.FLUJO + SIGNO_IGUAL
						+ BitaConstants.EC_MOV_CHEQ_INT_RECIBIDAS_DOLAR;
			} else { 
				matrizLocations[181][0] = "SinFacultades";
				matrizLocations[181][1] = "/Enlace/" + Global.WEB_APPLICATION + "/SinFacultades";
			}
			//*******************FIN TCS FSW 12/2016***********************************
			
			//MOVIMIENTOS>Banca Especializada
			if ( verificaFacultad ( "CONSMOVTOS" ,request) ) {
				matrizLocations[9][0]="CONSMOVTOS";
				matrizLocations[9][1]="/Enlace/"+Global.WEB_APPLICATION+"/CMovimiento?Banca=E&prog=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_BANCA;
			} else {
				matrizLocations[9][0]="SinFacultades";
				matrizLocations[9][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Tarjeta de Credito
			if(verificaFacultad ("CONSMOVTOS",request)) {
				matrizLocations[10][0]="CONSMOVTOS";
				matrizLocations[10][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_Movimientos?Tipo=1&Cuenta=5&Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_TARJETA;
			} else {
				matrizLocations[10][0]="SinFacultades";
				matrizLocations[10][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//MOVIMIENTOS>Terminales Punto de Venta
				/*matrizLocations[11][0]="EnConstruccion";
				matrizLocations[11][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
			//MOVIMIENTOS>Multicheque
			matrizLocations[72][0]="EnConstruccion";
			matrizLocations[72][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesBenefBaja?ventana=10&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_MOV_MULTICHEQUE;
			/*matrizLocations[72][0]="NoNecesaria";
			matrizLocations[72][1]="/NASApp/"+Global.WEB_APPLICATION+"/consultaMulticheque";*/
			//OPERACIONES PROGRAMADAS
			/*if(verificaFacultad("TENIPCAM",request))
			{
				matrizLocations[12][0]="TENIPCAM";
				matrizLocations[12][1]="programada?ventana=0";
			} else {
				matrizLocations[12][0]="SinFacultades";
				matrizLocations[12][1]="SinFacultades";
			}*/
			matrizLocations[12][0]="NoNecesaria";
			matrizLocations[12][1]="/Enlace/"+Global.WEB_APPLICATION+"/programada?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_OPER;
			//BITACORA
			if ( verificaFacultad ("CONSBITACORA" ,request) ) {
				matrizLocations[13][0]="CONSBITACORA";
				matrizLocations[13][1]="/Enlace/"+Global.WEB_APPLICATION+"/CBitacora?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_BITACORA;
			} else {
				matrizLocations[13][0]="SinFacultades";
				matrizLocations[13][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//COMISIONES COBRADAS
			/*if(verificaFacultad("CONSBITACORA",request))
			{
					matrizLocations[71][0]="CONSBITACORA";
					matrizLocations[71][1]="CComisiones";
			} else {
					matrizLocations[71][0]="SinFacultades";
					matrizLocations[71][1]="SinFacultades";
			}*/
			matrizLocations[71][0]="NoNecesaria";
			matrizLocations[71][1]="/Enlace/"+Global.WEB_APPLICATION+"/consultaComisiones?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EC_INFORME;
			//*******************************************************************************************************
			//********************************************* TRANSFERENCIAS*******************************************
			//*******************************************************************************************************
			//CUENTAS SANTANDER>Moneda Nacional>En L?ea
			if ( verificaFacultad ( "CARCHPROP",request ) ) {
				matrizLocations[14][0]="CARCHPROP";
				matrizLocations[14][1]="/Enlace/"+Global.WEB_APPLICATION+"/transferencia?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA;
			} else {
				matrizLocations[14][0]="SinFacultades";
				matrizLocations[14][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//CUENTAS SANTANDER>Moneda Nacional>M?tiples
			if ( verificaFacultad ( "CARCHPROP",request ) ) {
				matrizLocations[15][0]="CARCHPROP";
				matrizLocations[15][1]="/Enlace/"+Global.WEB_APPLICATION+"/TransferenciasMultiples?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_MULTI;
			} else {
				matrizLocations[15][0]="SinFacultades";
				matrizLocations[15][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//CUENTAS SANTANDER>Dolares
			if ( verificaFacultad ( "CARCHPROP" ,request) ) {
				matrizLocations[16][0]="CARCHPROP";
				matrizLocations[16][1]="/Enlace/"+Global.WEB_APPLICATION+"/transferencia?ventana=4&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_CUENTAS_MISMO_BANCO_DOLARES;
			} else {
				matrizLocations[16][0]="SinFacultades";
				matrizLocations[16][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//ORDENES DE PAGO OCURRE>Personas F?sicas
			if(verificaFacultad ("ALTAPAGOOCURRE",request)) {
				matrizLocations[62][0]="ALTAPAGOOCURRE";
				matrizLocations[62][1]="/Enlace/"+Global.WEB_APPLICATION+"/MDI_Orden_Pago_Ocurre?personalidad=1&opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_FISICAS;
			} else {
				matrizLocations[62][0]="SinFacultades";
				matrizLocations[62][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//ORDENES DE PAGO OCURRE>Personas Morales
			if(verificaFacultad ("ALTAPAGOOCURRE",request)) {
				matrizLocations[63][0]="ALTAPAGOOCURRE";
				matrizLocations[63][1]="/Enlace/"+Global.WEB_APPLICATION+"/MDI_Orden_Pago_Ocurre?personalidad=2&opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_ORDENES_PAGO_OCURRE_PERS_MORALES;
			} else {
				matrizLocations[63][0]="SinFacultades";
				matrizLocations[63][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//INTERBANCARIAS
			if ( verificaFacultad ( "DEPINTERBANCA",request ) ) {
				matrizLocations[17][0]="DEPINTERBANCA";
				matrizLocations[17][1]="/Enlace/"+Global.WEB_APPLICATION+"/MDI_Interbancario?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_INTERBANCARIAS;
			} else {
				matrizLocations[17][0]="SinFacultades";
				matrizLocations[17][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//PAGO DE TARJETA DE CREDITO
			if ( verificaFacultad ( "CARCHPROP",request ) ) {
				matrizLocations[18][0]="CARCHPROP";
				matrizLocations[18][1]="/Enlace/"+Global.WEB_APPLICATION+"/transferencia?ventana=3&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_TARJETA_CREDITO_PAGO;
			} else {
				matrizLocations[18][0]="SinFacultades";
				matrizLocations[18][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
	    //DISPOSICION DE TARJETA DE CREDITO A CHEQUES
			if ( verificaFacultad ( "DISPTARJCRED",request ) ) {
					matrizLocations[114][0] = "DISPTARJCRED";
					matrizLocations[114][1] = "/Enlace/" + Global.WEB_APPLICATION + "/transferencia?ventana=6&" +
						BitaConstants.FLUJO +"=" + BitaConstants.ET_TARJETA_CREDITO_DISPOSICION;
			} else {
					matrizLocations[114][0] = "SinFacultades";
					matrizLocations[114][1] = "/Enlace/" + Global.WEB_APPLICATION + "/SinFacultades";
			}


			//CAMBIOS(Transferencias M.N.-DLLS./DLLS.-M.N.) OPERACAMBIOSREG
			// HMM - Sep/2004
			if(verificaFacultad ("OPERACAMBIOSREG",request)) {

				matrizLocations[54][0]="OPERACAMBIOSREG";
				matrizLocations[54][1]="/Enlace/"+Global.WEB_APPLICATION+"/MIPD_cambios?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_CAMBIOS;
			} else {
				matrizLocations[54][0]="SinFacultades";
				matrizLocations[54][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}

			/*trizLocations[54][0]="EnConstruccion";
			matrizLocations[54][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
			//TRANSFERENCIAS INTERNACIONALES
			if(verificaFacultad ("CARCHPROP",request)) {
				matrizLocations[55][0]="CARCHPROP";
				matrizLocations[55][1]="/Enlace/"+Global.WEB_APPLICATION+"/MTI_Inicio?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ET_TRANSFERENCIAS_INTER;
			} else {
				matrizLocations[55][0]="SinFacultades";
				matrizLocations[55][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//Transferencias Reuters>Pactado de Operaciones
				matrizLocations[154][0]="NoNecesaria";
				matrizLocations[154][1]="/Enlace/"+Global.WEB_APPLICATION+"/RET_PactaOper?opcion=iniciar";
			//Transferencias Reuters>Liberaci�n de Operaciones
				matrizLocations[155][0]="NoNecesaria";
				matrizLocations[155][1]="/Enlace/"+Global.WEB_APPLICATION+"/RET_LiberaOper?opcion=iniciar";
			//Transferencias Reuters>Consulta de Operaciones
				matrizLocations[156][0]="NoNecesaria";
				matrizLocations[156][1]="/Enlace/"+Global.WEB_APPLICATION+"/RET_ConsultaOper?opcion=iniciar";

			/*trizLocations[55][0]="EnConstruccion";
			matrizLocations[55][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
			//*******************************************************************************************************
			//*********************************************** TESORERIA *********************************************
			//*******************************************************************************************************
			//INVERSIONES>Fondos de Inversi?
			if ( verificaFacultad ( "CONSSALDOS",request ) ) {
				matrizLocations[19][0]="CONSSALDOS";
				matrizLocations[19][1]="/Enlace/"+Global.WEB_APPLICATION+"/InverSoc?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_INVERSIONES_FONDOS_INVERSION;
			} else {
				matrizLocations[19][0]="SinFacultades";
				matrizLocations[19][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//INVERSIONES>Vista
			if ( verificaFacultad ( "CONSSALDOS",request ) ) {
				matrizLocations[20][0]="CONSSALDOS";
				matrizLocations[20][1]="/Enlace/"+Global.WEB_APPLICATION+"/vista?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_INVERSIONES_VISTA;
			} else {
				matrizLocations[20][0]="SinFacultades";
				matrizLocations[20][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			//INVERSIONES>Plazo>Tradicional
			/*if ( verificaFacultad( "CONSSALDOS" ,request) )
			{
				matrizLocations[21][0]="CONSSALDOS";
				matrizLocations[21][1]="/NASApp/"+Global.WEB_APPLICATION+"/plazo?ventana=0";
			} else {
				matrizLocations[21][0]="SinFacultades";
				matrizLocations[21][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
			}*/
			//INVERSIONES>Plazo>Registro de Operaciones
				matrizLocations[22][0]="EnConstruccion";
				matrizLocations[22][1]="/Enlace/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";
			/*if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
		 matrizLocations[22][0]="CONSSALDOS";
		 matrizLocations[22][1]="/NASApp/"+Global.WEB_APPLICATION+"/inversion7_28?ventana=0";
	     } else {
		 matrizLocations[22][0]="SinFacultades";
		 matrizLocations[22][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/
	     //INVERSIONES>Plazo>Cambio de Instruccion
				matrizLocations[23][0]="EnConstruccion";
				matrizLocations[23][1]="/Enlace/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";
			/*if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
		 matrizLocations[23][0]="CONSSALDOS";
		 matrizLocations[23][1]="/NASApp/"+Global.WEB_APPLICATION+"/c_posicion?opcion=1";
	     } else {
		 matrizLocations[23][0]="SinFacultades";
		 matrizLocations[23][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/
	     //INVERSIONES>Plazo>Dolares
			/*if(verificaFacultad("TECRESALDOSC",request))
			{
				matrizLocations[56][0]="TECRESALDOSC";
				matrizLocations[56][1]="/NASApp/"+Global.WEB_APPLICATION+"/MIPD_plazo?ventana=0";
			} else {
				matrizLocations[56][0]="SinFacultades";
				matrizLocations[56][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
			}*/
	     //CREDITO>Credito En Linea>Consultas>Saldo
	     if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
		 matrizLocations[24][0]="CONSSALDOS";
		 matrizLocations[24][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_Credito?Cuenta=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_LINEA_CONSULTA_SALDO;
	     } else {
		 matrizLocations[24][0]="SinFacultades";
		 matrizLocations[24][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //CREDITO>Credito En Linea>Consultas>Posicion
	     if ( verificaFacultad ( "CONSSALDOS" ,request) ) {
		 matrizLocations[25][0]="CONSSALDOS";
		 matrizLocations[25][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_MenuPos?Tipo=Posicion&Cuenta=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_LINEA_CONSULTA_POSICION;
	     } else {
		 matrizLocations[25][0]="";
		 matrizLocations[25][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //CREDITO>Credito En Linea>Consultas>Movimientos
			/*if ( verificaFacultad( "CONSMOVTOTER" ,request) )
			{
				matrizLocations[26][0]="CONSMOVTOTER";
				matrizLocations[26][1]="MCL_Movimientos?Tipo=1&Cuenta=0&Modulo=0";
			} else {
				matrizLocations[26][0]="SinFacultades";
				matrizLocations[26][1]="SinFacultades";
			}*/
	     matrizLocations[26][0]="NoNecesaria";
	     matrizLocations[26][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_Movimientos?Tipo=2&Cuenta=0&Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_LINEA_CONSULTA_MOVS;
	     //CREDITO>Credito En Linea>Prepago
	     if ( verificaFacultad ( "TECRESALDOSC" ,request) ) {
		 matrizLocations[27][0]="TECRESSALDOSC";
		 matrizLocations[27][1]="/Enlace/"+Global.WEB_APPLICATION+"/MCL_Pagos?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_LINEA_PREPAGO;
	     } else {
		 matrizLocations[27][0]="SinFacultades";
		 matrizLocations[27][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //CREDITO>Credito Electronico>Consultas
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[96][0]="TETESESTRUCC";
				matrizLocations[96][1]="ceConSaldos?Modulo=0";
			} else {
				matrizLocations[96][0]="SinFacultades";
				matrizLocations[96][1]="SinFacultades";
			}*/
	     matrizLocations[96][0]="No Necesaria";
	     matrizLocations[96][1]="/Enlace/"+Global.WEB_APPLICATION+"/ceConSaldos?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS;
	     //CREDITO>Credito Electronico>Prepagos
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[97][0]="TETESESTRUCC";
				matrizLocations[97][1]="cePrepagos?Modulo=0";
			} else {
				matrizLocations[97][0]="SinFacultades";
				matrizLocations[97][1]="SinFacultades";
			}*/
	     matrizLocations[97][0]="No Necesaria";
	     matrizLocations[97][1]="/Enlace/"+Global.WEB_APPLICATION+"/cePrepagos?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS;
	     //CREDITO>Credito Electronico>Disposiciones Vista
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[98][0]="TETESESTRUCC";
				matrizLocations[98][1]="ceVista";
			} else {
				matrizLocations[98][0]="SinFacultades";
				matrizLocations[98][1]="SinFacultades";
			}*/
	     matrizLocations[98][0]="No Necesaria";
	     matrizLocations[98][1]="/Enlace/"+Global.WEB_APPLICATION+"/ceVista?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_CREDITO_ELECTRONICO_DISPOS;

	 //TESORERIA INTERNACIONAL>Cambios(Transferencias M.N.-DLLS./DLLS.-M.N.)
	 //HMM - Sep/2004

	     if(verificaFacultad ("OPERACAMBIOSREG",request)) {
			 matrizLocations[57][0]="OPERACAMBIOSREG";
			 matrizLocations[57][1]="/Enlace/"+Global.WEB_APPLICATION+"/MIPD_cambios?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTER_CAMBIOS;
	     } else {
			 matrizLocations[57][0]="SinFacultades";
			 matrizLocations[57][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     /*trizLocations[57][0]="EnConstruccion";
	     matrizLocations[57][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
	     //TESORERIA INTERNACIONAL>Transferencias Internacionales
	     if(verificaFacultad ("CARCHPROP",request)) {
		 matrizLocations[58][0]="CARCHPROP";
		 matrizLocations[58][1]="/Enlace/"+Global.WEB_APPLICATION+"/MTI_Inicio?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTER_TRANSFER_INTER;
	     } else {
		 matrizLocations[58][0]="SinFacultades";
		 matrizLocations[58][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     /*trizLocations[58][0]="EnConstruccion";
	     matrizLocations[58][1]="/NASApp/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";*/
	     //TESORERIA INTELIGENTE>Mantenimiento de Estructuras>Concentracion
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[64][0]="TETESESTRUCC";
				matrizLocations[64][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[64][0]="SinFacultades";
				matrizLocations[64][1]="SinFacultades";
			}*/
	     matrizLocations[64][0]="No Necesaria";
	     matrizLocations[64][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIConcentracion?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC;
	     //TESORERIA INTELIGENTE>Mantenimiento de Estructuras>Dispersi?
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[65][0]="TETESESTRUCC";
				matrizLocations[65][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[65][0]="SinFacultades";
				matrizLocations[65][1]="SinFacultades";
			}*/
	     matrizLocations[65][0]="No Necesaria";
	     matrizLocations[65][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIDispersion?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP;
	     //TESORERIA INTELIGENTE>Mantenimiento de Estructuras>Fondeo Automatico
			/*if(verificaFacultad("TETESESTRUCC",request))
			{
				matrizLocations[66][0]="TETESESTRUCC";
				matrizLocations[66][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[66][0]="SinFacultades";
				matrizLocations[66][1]="SinFacultades";
			}*/
	     matrizLocations[66][0]="No Necesaria";
	     matrizLocations[66][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIFondeo?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_FOND;
	     //TESORERIA INTELIGENTE>Consulta de Saldos>Concentracion
			/*if(verificaFacultad("TETESSALDGRC",request))
			{
				matrizLocations[67][0]="TETESSALDGRC";
				matrizLocations[67][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[67][0]="SinFacultades";
				matrizLocations[67][1]="SinFacultades";
			}*/
	     matrizLocations[67][0]="No Necesaria";
	     matrizLocations[67][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIConSaldos?opcion=1&Accion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_CONS_SALDO_CONC;
	     //TESORERIA INTELIGENTE>Consulta de Saldos>Dispersi?
			/*if(verificaFacultad("TETESSALDGRC",request))
			{
				matrizLocations[68][0]="TETESSALDGRC";
				matrizLocations[68][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[68][0]="SinFacultades";
				matrizLocations[68][1]="SinFacultades";
			}*/
	     matrizLocations[68][0]="No Necesaria";
	     matrizLocations[68][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIConSaldos?opcion=0&Accion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_CONS_SALDO_DISP;
	     //TESORERIA INTELIGENTE>Consulta de Movimientos>Concentracion
			/*if(verificaFacultad("TETESMOVSGRC",request))
			{
				matrizLocations[69][0]="TETESMOVSGRC";
				matrizLocations[69][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[69][0]="SinFacultades";
				matrizLocations[69][1]="SinFacultades";
			}*/
	     matrizLocations[69][0]="No Necesaria";
	     matrizLocations[69][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIConMovtos?Tipo=C&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC;
	     //TESORERIA INTELIGENTE>Consulta de Movimientos>Dispersi?
			/*if(verificaFacultad("TETESMOVSGRC",request))
			{
				matrizLocations[70][0]="TETESMOVSGRC";
				matrizLocations[70][1]="MTI_Inicio?Modulo=0";
			} else {
				matrizLocations[70][0]="SinFacultades";
				matrizLocations[70][1]="SinFacultades";
			}*/
	     matrizLocations[70][0]="No Necesaria";
	     matrizLocations[70][1]="/Enlace/"+Global.WEB_APPLICATION+"/TIConMovtos?Tipo=D&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP;
	     //ESQUEMA BASE CERO>Mantenimiento de Estructura

	     if ( verificaFacultad ( "BASACCESO" ,request) )
	     {

			 matrizLocations[83][0]="BASACCESO";
		 matrizLocations[83][1]="/Enlace/"+Global.WEB_APPLICATION+"/bcMtoEstructura?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_ESQ_BASE0_MANT_ESTRUC;
		 matrizLocations[84][0]="BASACCESO";
		 matrizLocations[84][1]="/Enlace/"+Global.WEB_APPLICATION+"/bcConSaldos?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_ESQ_BASE0_CONS_SALDO;
		 matrizLocations[85][0]="BASACCESO";
		 matrizLocations[85][1]="/Enlace/"+Global.WEB_APPLICATION+"/bcConFechasProg?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_ESQ_BASE0_PROG_OP_CONS;
			 matrizLocations[86][0]="BASACCESO";
		 matrizLocations[86][1]="/Enlace/"+Global.WEB_APPLICATION+"/bcProgFechas?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ER_ESQ_BASE0_PROG_OP_REG;


		 }

		 else
		 {
			 matrizLocations[83][0]="SinFacultades";
			 matrizLocations[83][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			 matrizLocations[84][0]="SinFacultades";
			 matrizLocations[84][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			 matrizLocations[85][0]="SinFacultades";
			 matrizLocations[85][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			 matrizLocations[86][0]="SinFacultades";
			 matrizLocations[86][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";

		 }















	     //*******************************************************************************************************
	     //*********************************************** SERVICIOS *********************************************
	     //*******************************************************************************************************
	     //SERVICIOS>Impuestos Federales>Mantenimiento Cuentas>Alta
	     if ( verificaFacultad ( "TEIMFALTCTAM" ,request) ) {
		 matrizLocations[28][0]="TEIMFALTCTAM";
		 matrizLocations[28][1]="/Enlace/"+Global.WEB_APPLICATION+"/CuentasImpuestos?OPCION=0";
	     } else {
		 matrizLocations[28][0]="SinFacultades";
		 matrizLocations[28][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Impuestos Federales>Mantenimiento Cuentas>Baja
	     if ( verificaFacultad ( "TEIMFALTCTAM" ,request) ) {
		 matrizLocations[29][0]="TEIMFALTCTAM";
		 matrizLocations[29][1]="/Enlace/"+Global.WEB_APPLICATION+"/CuentasImpuestos?OPCION=10";
	     } else {
		 matrizLocations[29][0]="SinFacultades";
		 matrizLocations[29][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Impuestos Federales>Pagos
	     if ( verificaFacultad ( "TEIMFCAPPAGM" ,request) ) {
		 matrizLocations[30][0]="TEIMFCAPPAGM";
		 matrizLocations[30][1]="/Enlace/"+Global.WEB_APPLICATION+"/PagosImpuestos?OPCION=0";
	     } else {
		 matrizLocations[30][0]="SinFacultades";
		 matrizLocations[30][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Impuestos Federales>Envio Declaracion
	     if ( verificaFacultad ( "TEIMFIMPENVM" ,request) ) {
		 matrizLocations[31][0]="TEIMFIMPENVM";
		 matrizLocations[31][1]="/Enlace/"+Global.WEB_APPLICATION+"/ImpDeclaracion?ventana=0";
	     } else {
		 matrizLocations[31][0]="SinFacultades";
		 matrizLocations[31][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Impuestos Federales>Consulta
	     matrizLocations[32][0]="NoNecesaria";
	     matrizLocations[32][1]="/Enlace/"+Global.WEB_APPLICATION+"/PagosImpuestos?OPCION=10&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONSULTA;









		/**
		*		Modificado por Miguel Cortes Arellano
		*		Fecha: 09/01/2004
		*		Proposito: Adherir 2 submenus para los pagos de impuestos de Credito Fiscal y Entidades Federativas.
		*/
		 //SERVICIOS>Nuevo Esquema de Pago de Impuestos>Creditos Fiscales
	     matrizLocations[104][0]="NoNecesaria";
	     matrizLocations[104][1]="/Enlace/"+Global.WEB_APPLICATION+"/NuevoPagoImpuestos?ventana=0&tipopago=004";



		 //SERVICIOS>Nuevo Esquema de Pago de Impuestos>Derechos, Productos y Aprovechamientos		(vcl-06/ago/04)
	     matrizLocations[116][0]="NoNecesaria";
	     matrizLocations[116][1]="/Enlace/"+Global.WEB_APPLICATION+"/NuevoPagoImpuestos?ventana=0&tipopago=010&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_IMP_DER_PROD_APR;
		 //Finaliza codigo Sinapsis
	     //SERVICIOS>Aplicaciones Obrero Patronales>Pagos
	     if ( verificaFacultad ( "CARCHPROP" ,request) ) {
		 matrizLocations[33][0]="CARCHPROP";
		 matrizLocations[33][1]="/Enlace/"+Global.WEB_APPLICATION+"/SuaDisco?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_AP_OP_PAGOS;
	     } else {
		 matrizLocations[33][0]="SinFacultades";
		 matrizLocations[33][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Aplicaciones Obrero Patronales>Consultas
	     if ( verificaFacultad ( "TEIMSCONPAGC" ,request) ) {
		 matrizLocations[34][0]="TEIMSCONPAGC";
		 matrizLocations[34][1]="/Enlace/"+Global.WEB_APPLICATION+"/SuaConsulta?origen=Consulta&ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_AP_OP_CONSULTAS;
	     } else {
		 matrizLocations[34][0]="SinFacultades";
		 matrizLocations[34][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     /*
	      * SUAConsultaPagoServlet
			SUALineaCapturaServlet
	      */
	     //SERVICIOS>Aplicaciones Obrero Patronales>SIPARE - Pago
	     if ( verificaFacultad ( "TEIMSACCESOM" ,request) && verificaFacultad ( "CARCHPROP" ,request)) {
		 matrizLocations[142][0]="TEIMSACCESOM|CARCHPROP";
		 matrizLocations[142][1]="/Enlace/"+Global.WEB_APPLICATION+"/SUALineaCapturaServlet?opcionLC=1";
	     } else {
		 matrizLocations[142][0]="SinFacultades";
		 matrizLocations[142][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Aplicaciones Obrero Patronales>SIPARE - Consulta
	     if ( verificaFacultad ( "TEIMSCONPAGC" ,request) ) {
		 matrizLocations[143][0]="TEIMSCONPAGC";
		 matrizLocations[143][1]="/Enlace/"+Global.WEB_APPLICATION+"/SUAConsultaPagoServlet?opcion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_AP_OP_CONSULTAS;
	     } else {
		 matrizLocations[143][0]="SinFacultades";
		 matrizLocations[143][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina>Mantenimineto de Empleados
	    /* if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[35][0]="INOMMANTEMP";
		 matrizLocations[35][1]="/NASApp/"+Global.WEB_APPLICATION+"/ANomMttoEmp";
	     } else {
		 matrizLocations[35][0]="SinFacultades";
		 matrizLocations[35][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/
		 /* Modificado por Miguel Cortes Arellano
		* Fecha 15/12/03
		* Empieza codigo Sinapsis
		* Proposito: Redimensionar el array para colocacion de la nueva opcion Consultas
		*				   Array index 110 es para consultas
		*/
		//SERVICIOS>Nomina>Empleados > Mantenimiento
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[35][0]="INOMMANTEMP";
		 matrizLocations[35][1]="/Enlace/"+Global.WEB_APPLICATION+"/ANomMttoEmp?page=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_MANTEN;
	     } else {
		 matrizLocations[35][0]="SinFacultades";
		 matrizLocations[35][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina>Empleados > Consultas
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) || verificaFacultad ( "INOMACTUEMP" ,request)) {
		 matrizLocations[110][0]="INOMMANTEMP";
		 matrizLocations[110][1]="/Enlace/"+Global.WEB_APPLICATION+"/ANomMttoEmp?page=2&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[110][0]="SinFacultades";
		 matrizLocations[110][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }


	     /************************************************************************
		 *everis.
		 *Feb/2008
		 *Se modifica el men� para agregar las opciones
		 *		Catalogo de N�mina
		 *			Busqueda
		 *			Modificacion por Archivo
		 *			Baja por Archivo
		 *		Mantenimiento Intebancario
		 *			Alta en Linea
		 *			Alta por Archivo
		 *			Modificacion por Archivo
		************************************************************************/

	     //SERVICIOS>Nomina>Empleados > Cat�logo de N�mina > B�squeda
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[121][0]="INOMMANTEMP";
		 matrizLocations[121][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaEmpl?opcion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[121][0]="SinFacultades";
		 matrizLocations[121][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     /*<vc>*/
	   //SERVICIOS>Nomina>Empleados > Catalogo de nomina > Alta por Archivo
	  // ESC SE QUITA LA VALIDACION DE FACULTAD Y LA FACULTAD SE VALIDA EN BOTON DE ALTA
	     //if ( verificaFacultad ( "INOMENVIEMP" ,request) ) {
			 matrizLocations[152][0]="INOMENVIEMP";
			 matrizLocations[152][1]="/Enlace/"+Global.WEB_APPLICATION+"/PrevalidadorServlet?opcion=1&" +
						BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
		/*} else {
			 matrizLocations[152][0]="SinFacultades";
			 matrizLocations[152][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
		}*/

	     /*<vc>*/


	     //SERVICIOS>Nomina>Empleados > Cat�logo de N�mina > Modificaci�n por Archivo
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[122][0]="INOMMANTEMP";
		 matrizLocations[122][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaEmpl?opcion=9&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[122][0]="SinFacultades";
		 matrizLocations[122][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina>Empleados > Cat�logo de N�mina > Baja de Archivo
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[123][0]="INOMMANTEMP";
		 matrizLocations[123][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaEmpl?opcion=6&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[123][0]="SinFacultades";
		 matrizLocations[123][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }


	     /*<vc>*/
		   //SERVICIOS>Nomina>Empleados > Catalogo de nomina > Liberacion Empleados no registrados
		    // ESC SE QUITA LA VALIDACION DE FACULTAD Y LA FACULTAD SE VALIDA EN BOTON DE ALTA
		    // if ( verificaFacultad ( "INOMENVIEMP" ,request) ) {
				 matrizLocations[153][0]="INOMENVIEMP";
				 matrizLocations[153][1]="/Enlace/"+Global.WEB_APPLICATION+"/LiberaEmpleadosServlet?opcion=1&" +
							BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
			 /*} else {
				 matrizLocations[153][0]="SinFacultades";
				 matrizLocations[153][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}*/

		     /*<vc>*/


	     //SERVICIOS>Nomina>Empleados > Mantenimiento Interbancario > Alta en L�nea
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[124][0]="INOMMANTEMP";
		 matrizLocations[124][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaInterb?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[124][0]="SinFacultades";
		 matrizLocations[124][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina>Empleados > Mantenimiento Interbancario > Alta por Archivo
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[125][0]="INOMMANTEMP";
		 matrizLocations[125][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaInterb?opcion=12&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[125][0]="SinFacultades";
		 matrizLocations[125][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina>Empleados > Mantenimiento Interbancario > Modificacion por Archivo
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[126][0]="INOMMANTEMP";
		 matrizLocations[126][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaInterb?opcion=6&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
		 matrizLocations[126][0]="SinFacultades";
		 matrizLocations[126][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     //SERVICIOS>Nomina Interbancaria> Registro de Cuentas > Busqueda

	     if ( verificaFacultad ( "CONSCUENTAS" ,request) ) {
			 matrizLocations[127][0]="ALTACTAOTRB|AUTOCTATER";
			 matrizLocations[127][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaEmplInterb?opcion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
			 matrizLocations[127][0]="SinFacultades";
			 matrizLocations[127][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     if ( verificaFacultad ( "ALTACTAOTRB" ,request) ||
		    	  verificaFacultad ( "AUTOCTATER" ,request) ) {
				 matrizLocations[128][0]="ALTACTAOTRB|AUTOCTATER";
				 matrizLocations[128][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatalogoNominaAutInterb?Modulo=2&" +
						BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
		     } else {
				 matrizLocations[128][0]="SinFacultades";
				 matrizLocations[128][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
		     }

		 //Finaliza Codigo SINAPSIS
	     //SERVICIOS>Nomina>Pagos Importar - Enviar
	     if ( verificaFacultad ( "INOMPAGONOM" ,request) ) {
		 matrizLocations[36][0]="INOMPAGONOM";
		 matrizLocations[36][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNomina?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO;
	     } else {
		 matrizLocations[36][0]="SinFacultades";
		 matrizLocations[36][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			 //SERVICIOS>Nomina>Pagos Consulta - Cancelaciones
	     if ( verificaFacultad ( "INOMPAGONOM" ,request) ) {
		 matrizLocations[100][0]="INOMPAGONOM";
		 matrizLocations[100][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNomina?Modulo=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_PAGOS_CONSULTA;
	     } else {
		 matrizLocations[100][0]="SinFacultades";
		 matrizLocations[100][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }


	     /*Inicio - Nomina en Linea - Menu*/
	     //SERVICIOS>Nomina>Nomina en Linea>Pagos
	     if ( verificaFacultad ( FACULTADNLN ,request)   ) {
		 matrizLocations[164][0]=FACULTADNLN;
		 matrizLocations[164][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNominaLn?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINALN_PAGOS_IMP_ENVIO;//FIXME Implementar variable para bitacora
	     } else {
		 matrizLocations[164][0]="SinFacultades";
		 matrizLocations[164][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

	     /*Inicio - Nomina - Pagos - Menu*/
	     //SERVICIOS>Nomina>Nomina en Linea>Pagos
	     if ( verificaFacultad ( "INOMPAGONOM" ,request)  ) {
		 matrizLocations[180][0]="INOMPAGONOM";
		 matrizLocations[180][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNominaLn?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINALN_PAGOS_IMP_ENVIO + "&submodulo=2";//FIXME Implementar variable para bitacora
	     } else {
		 matrizLocations[180][0]="SinFacultades";
		 matrizLocations[180][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

		 //SERVICIOS>Nomina>Pagos Consulta - Cancelaciones
	     if ( verificaFacultad ( FACULTADNLN ,request) ) {
		 matrizLocations[165][0]=FACULTADNLN;
		 matrizLocations[165][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNominaLn?Modulo=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINALN_PAGOS_CONSULTA;
	     } else {
		 matrizLocations[165][0]="SinFacultades";
		 matrizLocations[165][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     /*Fin - Nomina en Linea - Menu*/


	     //SERVICIOS>Chequera Seguridad>Registros Cheques>En linea
	     if ( verificaFacultad ( "TECHSACCESOM" ,request) ) {
		 matrizLocations[37][0]="TECHSACCESOM";
		 matrizLocations[37][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesLinea?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CH_SEG_REG_CHEQUES_LINEA;
	     } else {
		 matrizLocations[37][0]="SinFacultades";
		 matrizLocations[37][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Registros Cheques>Por Archivo
	     if ( verificaFacultad ( "TECHSACCESOM" ,request) ) {
		 matrizLocations[38][0]="TECHSACCESOM";
		 matrizLocations[38][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesRegistro?operacion=inicio&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CH_SEG_REG_CHEQUES_ARCHIVO;
	     } else {
		 matrizLocations[38][0]="SinFacultades";
		 matrizLocations[38][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Consultas>Cheques/Cancelacin
	     if ( verificaFacultad ( "TECHSCONC" ,request) ) {
		 matrizLocations[39][0]="TECHSCONC";
		 matrizLocations[39][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesConsulta?modulo=cheques&opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CH_SEG_CONS_CHEQUES_CANC;
	     } else {
		 matrizLocations[39][0]="SinFacultades";
		 matrizLocations[39][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Consultas>Archivos
	     if ( verificaFacultad ( "TECHSCONC" ,request) ) {
		 matrizLocations[101][0]="TECHSCONC";
		 matrizLocations[101][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesConsulta?modulo=archivos&opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CH_SEG_CONS_ARCHIVOS;
	     } else {
		 matrizLocations[101][0]="SinFacultades";
		 matrizLocations[101][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Mantenimiento a Beneficiarios>Alta
	     /*if ( verificaFacultad ( "TECHSBENM" ,request) ) {
		 matrizLocations[40][0]="TECHSBENM";
		 matrizLocations[40][1]="/NASApp/"+Global.WEB_APPLICATION+"/ChesBenefAlta?ventana=0";
	     } else {
		 matrizLocations[40][0]="SinFacultades";
		 matrizLocations[40][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Mantenimiento a Beneficiarios>Baja
	     if ( verificaFacultad ( "TECHSBENM" ,request) ) {
		 matrizLocations[41][0]="TECHSBENM";
		 matrizLocations[41][1]="/NASApp/"+Global.WEB_APPLICATION+"/ChesBenefBaja?ventana=0";
	     } else {
		 matrizLocations[41][0]="SinFacultades";
		 matrizLocations[41][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Mantenimiento a Beneficiarios>Modificacion
	     if ( verificaFacultad ( "TECHSBENM" ,request) ) {
		 matrizLocations[42][0]="TECHSBENM";
		 matrizLocations[42][1]="/NASApp/"+Global.WEB_APPLICATION+"/ChesBenefModif?ventana=0";
	     } else {
		 matrizLocations[42][0]="SinFacultades";
		 matrizLocations[42][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/
	     //SERVICIOS>Chequera Seguridad>Mantenimiento de Cuentas>Altas
	     if ( verificaFacultad ( "TECHSALTM" ,request) ) {
		 matrizLocations[43][0]="TECHSALTM";
		 matrizLocations[43][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesAltaChequera?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CHE_SEG_MAN_CUENTAS_ALTA;
	     } else {
		 matrizLocations[43][0]="SinFacultades";
		 matrizLocations[43][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Chequera Seguridad>Mantenimiento de Cuentas>Bajas
	     if ( verificaFacultad ( "TECHSBAJM" ,request) ) {
		 matrizLocations[44][0]="TECHSBAJM";
		 matrizLocations[44][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesBajaChequera?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CHE_SEG_MAN_CUENTAS_BAJA;
	     } else {
		 matrizLocations[44][0]="SinFacultades";
		 matrizLocations[44][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

		 //sjr

		//SERVICIOS>Pago Directo>Registro de Pagos>En Linea
		 /*if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[73][0]="CONSUSUARIOS";
		 matrizLocations[73][1]="/NASApp/"+Global.WEB_APPLICATION+"/pdLinea";
		}
		else{
			matrizLocations[73][0]="SinFacultades";
			matrizLocations[73][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
		}*/
	    matrizLocations[73][0]="No Necesaria";
		matrizLocations[73][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdLinea?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_REG_LINEA;

		//SERVICIOS>Pago Directo>Registro de Pagos>Por Archivo
		matrizLocations[74][0]="No Necesaria";
		matrizLocations[74][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdRegistro?txtOpcion=13&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_REG_PAGOS_ARCH;

		//SERVICIOS>Pago Directo>Consulta y Modificacion de Pagos en Linea
		matrizLocations[75][0]="No Necesaria";
		//matrizLocations[75][1]="pdConsultas";
		matrizLocations[75][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdConsultas?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_CONS_MODIF;

		//SERVICIOS>Pago Directo>Modificacion de Pagos por Archivo
		matrizLocations[76][0]="No Necesaria";
		matrizLocations[76][1]="/Enlace/"+Global.WEB_APPLICATION+"/CtasInternacionales";

		//SERVICIOS>Pago Directo>Mantenimiento de Cuentas>Alta
		matrizLocations[77][0]="No Necesaria";
		matrizLocations[77][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdAltaCuentas?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_MAN_CUENTAS_ALTA;

		//SERVICIOS>Pago Directo>Mantenimiento de Cuentas>Baja
		matrizLocations[78][0]="No Necesaria";
		matrizLocations[78][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdBajaCuentas?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_MAN_CUENTAS_BAJA;

		//SERVICIOS>Pago Directo>Mantenimiento de Beneficiarios>Alta
		matrizLocations[79][0]="No Necesaria";
		matrizLocations[79][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdAltaBenef?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_MAN_BENEF_ALTA;

		//SERVICIOS>Pago Directo>Mantenimiento de Beneficiarios>Baja
		matrizLocations[80][0]="No Necesaria";
		matrizLocations[80][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdBajaBenef?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_MAN_BENEF_BAJA;

		//SERVICIOS>Pago Directo>Mantenimiento de Beneficiarios>Modificacion
	    matrizLocations[81][0]="No Necesaria";
		matrizLocations[81][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdModifBenef?ventana=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_MAN_BENEF_MODIF;

		//SERVICIOS>Pago Directo>Consulta de Catalogo de Sucursales
		matrizLocations[82][0]="No Necesaria";
		matrizLocations[82][1]="/Enlace/"+Global.WEB_APPLICATION+"/pdSucursales?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_DIR_CAT_SUCURSALES;

        //sjr


	     //SERVICIOS>Confirming>Mantenimiento a Proveedores>Alta
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[87][0]="CONSUSUARIOS";
		 matrizLocations[87][1]="/Enlace/"+Global.WEB_APPLICATION+"/coMtoProveedor?opcion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_MANT_ALTA;
	     }
	     else{
		 matrizLocations[87][0]="SinFacultades";
		 matrizLocations[87][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[87][0]="No Necesaria";
			matrizLocations[87][1]="coMtoProveedor";*/
	     //SERVICIOS>Confirming>Mantenimiento a Proveedores>Consulta y Modificacion en Linea
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[88][0]="CONSUSUARIOS";
		 matrizLocations[88][1]="/Enlace/"+Global.WEB_APPLICATION+"/coMtoProveedor?opcion=2&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_MANT_CONS;
	     }
	     else{
		 matrizLocations[88][0]="SinFacultades";
		 matrizLocations[88][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[88][0]="No Necesaria";
			matrizLocations[88][1]="coMtoProveedor";*/
	     //SERVICIOS>Confirming>Mantenimiento a Proveedores>Catalogo
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[89][0]="CONSUSUARIOS";
		 matrizLocations[89][1]="/Enlace/"+Global.WEB_APPLICATION+"/CtasInternacionales";
	     }
	     else{
		 matrizLocations[89][0]="SinFacultades";
		 matrizLocations[89][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[89][0]="No Necesaria";
			matrizLocations[89][1]="CtasInternacionales";*/
	     //SERVICIOS>Confirming>Pagos
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[90][0]="CONSUSUARIOS";
		 matrizLocations[90][1]="/Enlace/"+Global.WEB_APPLICATION+"/coPagos?opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_PAGO;
	     }
	     else{
		 matrizLocations[90][0]="SinFacultades";
		 matrizLocations[90][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[90][0]="No Necesaria";
			matrizLocations[90][1]="CtasInternacionales";*/
	     //SERVICIOS>Confirming>Pago y Mantenimiento a Proveedores
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[91][0]="CONSUSUARIOS";
		 matrizLocations[91][1]="/Enlace/"+Global.WEB_APPLICATION+"/CtasInternacionales";
	     }
	     else{
		 matrizLocations[91][0]="SinFacultades";
		 matrizLocations[91][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[91][0]="No Necesaria";
			matrizLocations[91][1]="CtasInternacionales";*/
	     //SERVICIOS>Confirming>Consulta y Cancelacion de Pagos
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[92][0]="CONSUSUARIOS";
		 matrizLocations[92][1]="/Enlace/"+Global.WEB_APPLICATION+"/coCancelacion?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_CONS_CANC;
	     }
	     else{
		 matrizLocations[92][0]="SinFacultades";
		 matrizLocations[92][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[92][0]="No Necesaria";
			matrizLocations[92][1]="CtasInternacionales";*/
	     //SERVICIOS>Confirming>Estadisticas>Reportes
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[93][0]="CONSUSUARIOS";
		 matrizLocations[93][1]="/Enlace/"+Global.WEB_APPLICATION+"/coEstadisticas?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_EST_REP;
	     }
	     else{
		 matrizLocations[93][0]="SinFacultades";
		 matrizLocations[93][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[93][0]="No Necesaria";
			matrizLocations[93][1]="coEstadisticas";*/
	     //SERVICIOS>Confirming>Estadisticas>Graficas
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[94][0]="CONSUSUARIOS";
		 matrizLocations[94][1]="/Enlace/"+Global.WEB_APPLICATION+"/confGrafica?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_CONF_PAGO_PROV_EST_GRA;
	     }
	     else{
		 matrizLocations[94][0]="SinFacultades";
		 matrizLocations[94][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[94][0]="No Necesaria";
			matrizLocations[94][1]="confGrafica";*/
	     //SERVICIOS>Confirming>Cancelacion de Archivos
	     if(verificaFacultad ("CONSUSUARIOS",request)){
		 matrizLocations[95][0]="CONSUSUARIOS";
		 matrizLocations[95][1]="/Enlace/"+Global.WEB_APPLICATION+"/CtasInternacionales";
	     }
	     else{
		 matrizLocations[95][0]="SinFacultades";
		 matrizLocations[95][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			/*matrizLocations[95][0]="No Necesaria";
			matrizLocations[95][1]="CtasInternacionales";*/

		 //SERVICIOS>SAR>Pagos JPPM
		 if(verificaFacultad ("SARPAGOENL",request))
		 {
		 matrizLocations[115][0]="SARPAGOENL";
		 matrizLocations[115][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConsultaUsuarios?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_SAR_PAGOS;
		 }
		 else
		 {
		 matrizLocations[115][0]="SinFacultades";
		 matrizLocations[115][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
		 }

		 //SERVICIOS>SAR>Consulta y cancelacin de Pagos JPPM
		 if(verificaFacultad ("SARCONSENL",request))
		 {
		 matrizLocations[117][0]="SARCONSENL";
		 matrizLocations[117][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConsultaUsuarios?Modulo=6&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_SAR_CONS_CANCEL;
		 }
		 else
		 {
		 matrizLocations[117][0]="SinFacultades";
		 matrizLocations[117][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
		 }

		 /*******************************************************/
		 /* TESOFE						                        */
		 /*******************************************************/
		 if(
			  verificaFacultad("TESORECDET",request) || verificaFacultad("TESORECCON",request) ||
			  verificaFacultad("TESOIMPDEV",request) || verificaFacultad("TESOIMPCAN",request) ||
			  verificaFacultad("TESOENVDEV",request) || verificaFacultad("TESOENVCAN",request) ||
			  verificaFacultad("TESOCONREG",request) || verificaFacultad("TESOCONARC",request)
		   )
			{
			  matrizLocations[118][0]="true";
			  matrizLocations[118][1]="true";
			}
		   else
			{
			  matrizLocations[118][0]="false";
			  matrizLocations[118][1]="false";
			}
		 /*******************************************************/

	     //*******************************************************************************************************
	     //************************************* ADMINISTRACION Y CONTROL ****************************************
	     //*******************************************************************************************************
	     //ADMINISTRACION Y CONTROL>Mancomunidad>Consulta y Autorizacion
	     //no verifica facultades
	     matrizLocations[45][0]="NoNecesaria";
	     matrizLocations[45][1]="/Enlace/"+Global.WEB_APPLICATION+"/AMancomunidad?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_MANCOM_CONS_AUTO;
	     //ADMINISTRACION Y CONTROL>Mancomunidad>Generacion de Folios
	     //no verifica facultades
	     matrizLocations[46][0]="NoNecesaria";
	     matrizLocations[46][1]="/Enlace/"+Global.WEB_APPLICATION+"/AMancEspecial?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_MANCOM_GEN_FOLIOS;
	     //ADMINISTRACION Y CONTROL>Mancomunidad>Operaciones internacionales
	     //no verifica facultades
	     matrizLocations[113][0]="NoNecesaria";
	     matrizLocations[113][1]="/Enlace/"+Global.WEB_APPLICATION+"/AMancomunidadInter?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_MANCOM_OPER_INTER;
	     //ADMINISTRACION Y CONTROL>Mantenimiento Local
	     //no existe, "Under construction"
	     matrizLocations[47][0]="EnConstruccion";
	     matrizLocations[47][1]="/Enlace/"+Global.WEB_APPLICATION+"/EI_EnConstruccion";
	     //ADMINISTRACION Y CONTROL>Cambio de contrato
	     //no verifica facultades
	     unContrato = session.getContratos ();
	     String[] contratos = desentramaC ( unContrato, '@' );
	     if ( (Integer.parseInt ( contratos[0] ) == 1) ) {
	    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "AQUI DEBE MANDAR A LLAMAR A CUADRO DE DIALOGO",EIGlobal.NivelLog.DEBUG);
		 matrizLocations[48][0]="NoNecesaria";
		 matrizLocations[48][1]="javascript:unSoloContrato();";
	     } else {
		 matrizLocations[48][0]="NoNecesaria";
		 matrizLocations[48][1]="/Enlace/"+Global.WEB_APPLICATION+"/CambioContrato?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CAMBIO_CONTRATO;
	     }
	    //ADMINISTRACION Y CONTROL>Cambio de contrase?
	    /*if ( verificaFacultad ( "CAMBCONTRA" ,request) ){
				matrizLocations[49][0]="CAMBCONTRA";
				matrizLocations[49][1]="/NASApp/"+Global.WEB_APPLICATION+"/CambioPasswd";
	    } else {
		matrizLocations[49][0]="SinFacultades";
		matrizLocations[49][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	    }*/
	     //ADMINISTRACION Y CONTROL>Cambio de contrase?
	     //no verifica facultades
	     matrizLocations[49][0]="NoNecesaria";
	     matrizLocations[49][1]="/Enlace/"+Global.WEB_APPLICATION+"/CambioPasswd?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CAMBIO_CONTRASENA;
		/*<VC proyecto="200710001" autor="TIB" fecha="07/06/2007" descripci?="CAMBIO DE CONTRASENA">*/
		String headerUser = (String) request.getHeader("IV-USER");
		if (headerUser != null) {
		  matrizLocations[49][1]+="&pwdSAM=1";
		}
		/*</VC>*/
	     //ADMINISTRACION Y CONTROL>Cuentas>Consultas
	     if ( verificaFacultad ( "CONSCUENTAS" ,request) ) {
		 matrizLocations[50][0]="CONSCUENTAS";
		 matrizLocations[50][1]="/Enlace/"+Global.WEB_APPLICATION+"/ContCtas_AL?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CUENTAS_CONSULTA;
	     } else {
		 matrizLocations[50][0]="SinFacultades";
		 matrizLocations[50][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     /*******************************************************/
		 /* NUEVO MODULO ALTA DE CUENTAS                        */
		 /*******************************************************/
	     //ADMINISTRACION Y CONTROL>Cuentas>Alta
	     //en visual se verifica: TEADMALTCAM
		 /* Alta */
		 if (
			  verificaFacultad ( "ALTACTASANPROP",request ) ||
			  verificaFacultad ( "ALTACTASANTER" ,request) ||
			  verificaFacultad ( "ALTACTAOTRB",request ) ||
			  verificaFacultad ( "ALTACTAINTER",request ) ||
			  verificaFacultad ( "ALTACTAMOVIL",request )
			)
			{
			  matrizLocations[51][0]="ALTACTASANPROP|ALTACTASANTER|ALTACTAOTRB|ALTACTAINTER|ALTACTAMOVIL";
			  matrizLocations[51][1]="/Enlace/"+Global.WEB_APPLICATION+"/MMC_Alta?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CUENTAS_ALTA;
			}
		 else
			{
			  matrizLocations[51][0]="SinFacultades";
			  matrizLocations[51][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
		 /* Baja */
		 if (
			  verificaFacultad ( "BAJACTASAN",request ) ||
			  verificaFacultad ( "BAJACTAOTR",request ) ||
			  verificaFacultad ( "BAJACTAINTER",request )
			)
			{
			  matrizLocations[52][0]="CARCHPROP";
			  matrizLocations[52][1]="/Enlace/"+Global.WEB_APPLICATION+"/MMC_Baja?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CUENTAS_BAJA;
			}
		 else
			{
			  matrizLocations[52][0]="SinFacultades";
			  matrizLocations[52][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
		 /* Consulta y Autorizacion */
		 matrizLocations[119][0]="No Necesaria";
		 matrizLocations[119][1]="/Enlace/"+Global.WEB_APPLICATION+"/ContCtas_AL?Modulo=2&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_CUENTAS_AUTO_CANC;

	     /*
		 if ( verificaFacultad ( "TEADMALSANM",request ) || verificaFacultad ( "TEADMALOTRM" ,request) ) {
		 matrizLocations[51][0]="TEADMALSANM|TEADMALOTRM";
		 matrizLocations[51][1]="/NASApp/"+Global.WEB_APPLICATION+"/MMC_Alta?Modulo=0";
	     } else {
		 matrizLocations[51][0]="SinFacultades";
		 matrizLocations[51][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //ADMINISTRACION Y CONTROL>Cuentas>Baja
	     if ( verificaFacultad ( "CARCHPROP" ,request) ) {
		 matrizLocations[52][0]="CARCHPROP";
		 matrizLocations[52][1]="/NASApp/"+Global.WEB_APPLICATION+"/MMC_Baja?Modulo=0";
	     } else {
		 matrizLocations[52][0]="SinFacultades";
		 matrizLocations[52][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/

	    /***************************************************
	     * Modificado por: Rafael M. Rosiles Soto
		 * Fecha:	   15 de Enero de 2004
	     * Descripcin:    Menu para consulta, actualizacin y alta de cuentas CLABE
	     */
		 // ADMINISTRACION Y CONTROL>Cuentas>Consulta y Administracion cuentas CLABE>Consulta cuentas CLABE
	     if ( verificaFacultad ( "TEADMALOTRM" ,request) || verificaFacultad ( "ABOEXTTERNREG" ,request) ||
			  verificaFacultad ( "CCENVARCHPR" ,request) || verificaFacultad ( "CARCHPROP" ,request)) {
			matrizLocations[111][0]="TEADMALOTRM|ABOEXTTERNREG|CCENVARCHPR|CARCHPROP";
			matrizLocations[111][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConActCLABE";
	     } else {
			matrizLocations[111][0]="SinFacultades";
			 matrizLocations[111][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //ADMINISTRACION Y CONTROL>Cuentas>Consulta y Administracion cuentas CLABE>Alta cuentas CLABE
	     //if ( verificaFacultad ( "TEADMALOTRM" ,request) && verificaFacultad ( "ABOEXTTERNREG" ,request) ) {
			 //matrizLocations[112][0]="TEADMALOTRM|ABOEXTTERNREG";
			 matrizLocations[112][0]="NoNecesaria";
			 matrizLocations[112][1]="/Enlace/"+Global.WEB_APPLICATION+"/AltaCtaCLABE?Operacion=1";
	     //} else {
		 //	 matrizLocations[112][0]="SinFacultades";
		 //	 matrizLocations[112][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     //}
	     // fin de modificacin
	     //***************************************************
	     //ADMINISTRACION Y CONTROL>Cuentas>Usuarios
	     if ( verificaFacultad ( "CONSUSUARIOS" ,request) ) {
		 matrizLocations[53][0]="CONSUSUARIOS";
		 matrizLocations[53][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConsultaUsuarios?" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_USUARIOS;
	     } else {
		 matrizLocations[53][0]="SinFacultades";
		 matrizLocations[53][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //ADMINISTRACION Y CONTROL>Cuentas>Internacionales
			/*if(verificaFacultad("CONSUSUARIOS",request)){
				matrizLocations[59][0]="CONSUSUARIOS";
				matrizLocations[59][1]="/NASApp/"+Global.WEB_APPLICATION+"/CtasInternacionales";
			}
			else{
				matrizLocations[59][0]="SinFacultades";
				matrizLocations[59][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
			}
			/*matrizLocations[59][0]="No Necesaria";
			matrizLocations[59][1]="CtasInternacionales";*/
			 matrizLocations[60][0]="No Necesaria";
			 matrizLocations[60][1]="/Enlace/"+Global.WEB_APPLICATION+"/InicioNominaInter?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_INTERBANCARIA;
	     //ADMINISTRACION Y CONTROL>Cuentas>Usuarios
	     /*if ( verificaFacultad ( "CONSUSUARIOS" ,request) ) {
		 matrizLocations[102][0]="CONSUSUARIOS";
		 matrizLocations[102][1]="/NASApp/"+Global.WEB_APPLICATION+"/ChesConsulta?modulo=macros";
	     } else {
		 matrizLocations[102][0]="SinFacultades";
		 matrizLocations[102][1]="/NASApp/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }*/
		 matrizLocations[102][0]="No Necesaria";
		 matrizLocations[102][1]="/Enlace/"+Global.WEB_APPLICATION+"/ChesConsulta?modulo=macros&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_GEN_MANUAL_ARCHIVOS;

		 matrizLocations[108][0]="NoNecesaria";
		 matrizLocations[108][1]="/Enlace/"+Global.WEB_APPLICATION+"/CapturaDatosUsr?opcion=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.EA_GEN_MANUAL_ARCHIVOS;

            // Facultad para nuevo pago de Impuestos por linea de captura 23/12/2005 JVL
	    if ( verificaFacultad ( "TEIMFCAPPAGM" ,request) )
    	    {
			matrizLocations[120][0]="TEIMFCAPPAGM"; // Facultad para pago de impuestos
			matrizLocations[120][1]="/Enlace/"+Global.WEB_APPLICATION+"/CambioContrato?Modulo=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_PAGO_LINEA_CAPTURA;
	     }
		else
		 {
		   matrizLocations[120][0]="SinFacultades";
		   matrizLocations[120][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	    // Fin 23/12/2005 JVL
	     //SERVICIOS>Nomina Prepago>Remesas > Recepci�n
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[129][0]="INOMMANTEMP";
		 matrizLocations[129][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreRemesas?operacion=inirecep&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_REMESAS_RECEPCION;
	     } else {
		 matrizLocations[129][0]="SinFacultades";
		 matrizLocations[129][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Remesas > Consulta
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[130][0]="INOMMANTEMP";
		 matrizLocations[130][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreRemesas?operacion=iniconsul&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_REMESAS_CONSULTA;
	     } else {
		 matrizLocations[130][0]="SinFacultades";
		 matrizLocations[130][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Asignacion
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[131][0]="INOMMANTEMP";
		 matrizLocations[131][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreAltaTarjeta?opcion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_ASIGNACION ;
	     } else {
		 matrizLocations[131][0]="SinFacultades";
		 matrizLocations[131][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Consulta
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[132][0]="INOMMANTEMP";
		 matrizLocations[132][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreBusqueda?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_CONSULTA;
	     } else {
		 matrizLocations[132][0]="SinFacultades";
		 matrizLocations[132][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
//	   SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Baja
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[133][0]="INOMMANTEMP";
		 matrizLocations[133][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreCancelaTarjeta?operacionNP=cancela&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_BAJA;
	     } else {
		 matrizLocations[133][0]="SinFacultades";
		 matrizLocations[133][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
//	   SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Bloqueo
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[134][0]="INOMMANTEMP";
		 matrizLocations[134][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreCancelaTarjeta?operacionNP=bloqueo&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_BLOQUEO;
	     } else {
		 matrizLocations[134][0]="SinFacultades";
		 matrizLocations[134][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Reasignaci�n
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[135][0]="INOMMANTEMP";
		 matrizLocations[135][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreReposicionTarjeta?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_REASIGNACION;
	     } else {
		 matrizLocations[135][0]="SinFacultades";
		 matrizLocations[135][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Asignaci�n Tarjeta - Empleado >Consulta Estado Asignaci�n Masiva
	     if ( verificaFacultad ( "INOMMANTEMP" ,request) ) {
		 matrizLocations[136][0]="INOMMANTEMP";
		 matrizLocations[136][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPreConsultaEstado?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_TARJETA_CONSULTA_ALTA;
	     } else {
		 matrizLocations[136][0]="SinFacultades";
		 matrizLocations[136][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Nomina Prepago>Pagos >Importaci�n Envio
	     if ( verificaFacultad ( "INOMPAGONOM" ,request) ) {
		 matrizLocations[137][0]="INOMPAGONOM";
		 matrizLocations[137][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPredispersion?Modulo=7&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_PAGOS_IMPORTACION;
	     } else {
		 matrizLocations[137][0]="SinFacultades";
		 matrizLocations[137][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
//	   SERVICIOS>Nomina Prepago>Pagos >Pago Individual
	     if ( verificaFacultad ( "INOMPAGONOM" ,request) ) {
		 matrizLocations[138][0]="INOMPAGONOM";
		 matrizLocations[138][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPredispersion?Modulo=9&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_PAGOS_INDIVIDUAL;
	     } else {
		 matrizLocations[138][0]="SinFacultades";
		 matrizLocations[138][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			 //SERVICIOS>Nomina Prepago>Pagos >Consulta
	     if ( verificaFacultad ( "INOMPAGONOM" ,request) ) {
		 matrizLocations[139][0]="INOMPAGONOM";
		 matrizLocations[139][1]="/Enlace/"+Global.WEB_APPLICATION+"/NomPredispersion?Modulo=8&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NP_PAGOS_CONSULTA;
	     } else {
		 matrizLocations[139][0]="SinFacultades";
		 matrizLocations[139][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			 //SERVICIOS>Nomina Interbancaria >Registro de Cuentas >Alta
	     if ( verificaFacultad ( "ALTACTAOTRB" ,request) ||
	    	  verificaFacultad ( "AUTOCTATER" ,request) ) {
			 matrizLocations[140][0]="ALTACTAOTRB|AUTOCTATER";
			 matrizLocations[140][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatNomInterbRegistro?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
			 matrizLocations[140][0]="SinFacultades";
			 matrizLocations[140][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
			 //SERVICIOS>Nomina Interbancaria >Registro de Cuentas >Baja
	     if ( verificaFacultad ( "ALTACTAOTRB" ,request) ||
	    	  verificaFacultad ( "AUTOCTATER" ,request) ) {
			 matrizLocations[141][0]="ALTACTAOTRB|AUTOCTATER";
			 matrizLocations[141][1]="/Enlace/"+Global.WEB_APPLICATION+"/CatNomInterbBaja?Modulo=0&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
			 matrizLocations[141][0]="SinFacultades";
			 matrizLocations[141][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }

		 // Capitulo X - Administracion Super Usuario
		 // Se validan Facultades en el Servlet inicial
			 String blanco="";
			 String destino = "/jsp/adminusr/adminAltaUsr.jsp";
			 String error = "/jsp/adminusr/error.jsp";
			 matrizLocations[144][1]="/Enlace/" + Global.WEB_APPLICATION + "/AdmonUsuarioServlet?Operacion=alta&" +
			 "JSON=" + blanco + "&Origen=" + blanco + "&Destino=" + destino + "&Error=" + error;
			 destino = "/jsp/adminusr/adminBajaUsr.jsp";
			 matrizLocations[145][1]="/Enlace/" + Global.WEB_APPLICATION + "/AdmonUsuarioServlet?Operacion=baja&" +
			 "JSON=" + blanco + "&Origen=" + blanco + "&Destino=" + destino + "&Error=" + error;
			 destino = "/jsp/adminusr/adminModiUsr.jsp";
			 error = "/jsp/adminlym/error.jsp";
			 matrizLocations[146][1]="/Enlace/" + Global.WEB_APPLICATION + "/AdmonUsuarioServlet?Operacion=cambio&" +
			 "JSON=" + blanco + "&Origen=" + blanco + "&Destino=" + destino + "&Error=" + error;
			 destino = "/jsp/adminlym/adminlym.jsp";
			 matrizLocations[147][1]="/Enlace/" + Global.WEB_APPLICATION + "/AdmonLimitesMontosServlet?Operacion=admin&" +
			 "JSON=" + blanco + "&Origen=" + blanco + "&Destino=" + destino + "&Error=" + error;

	     matrizLocations[148][0]="NoNecesaria";
	     matrizLocations[148][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConfigMedioNot?opcion=0";

		 //Inicio FSW Stefanini Mancomunidad Fase 1 Bloque 3
		 matrizLocations[149][0]="NoNecesaria";
		 matrizLocations[149][1]="/Enlace/"+Global.WEB_APPLICATION+"/AMancomunidadSA?" +
					             BitaConstants.FLUJO +"=" + BitaConstants.EA_MANCOM_CONS_AUTO_ARCH;
		 //Fin FSW Stefanini Mancomunidad Fase 1 Bloque 3

		 /*Inicio - P020101  Cuenta Nivel 3 - Menu*/
			if (verificaFacultad("INOMREMADM", request)) {
				matrizLocations[157][0] = "INOMREMADM";
				matrizLocations[157][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/AdmonRemesasServlet?operacion=1&" + BitaConstants.FLUJO
						+ "=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
			} else {
				matrizLocations[157][0] = "SinFacultades";
				matrizLocations[157][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/SinFacultades";
			}

			if (verificaFacultad("INOMREMCON", request)) {
				matrizLocations[158][0] = "INOMREMCON";
				matrizLocations[158][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/AdmonRemesasServlet?operacion=2&" + BitaConstants.FLUJO
						+ "=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
			} else {
				matrizLocations[158][0] = "SinFacultades";
				matrizLocations[158][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/SinFacultades";
			}

			if (verificaFacultad("INOMASIIND", request)) {
				matrizLocations[159][0] = "INOMASIIND";
				matrizLocations[159][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/AsignacionEmplServlet?opcion=iniciarIndividual&" + BitaConstants.FLUJO
						+ "=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
			} else {
				matrizLocations[159][0] = "SinFacultades";
				matrizLocations[159][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/SinFacultades";
			}

			if (verificaFacultad("INOMASIARC", request)) {
				matrizLocations[160][0] = "INOMASIARC";
				matrizLocations[160][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/AsignacionEmplServlet?opcion=iniciarArchivo&" + BitaConstants.FLUJO
						+ "=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
			} else {
				matrizLocations[160][0] = "SinFacultades";
				matrizLocations[160][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/SinFacultades";
			}

			if (verificaFacultad("INOMASICON", request)) {
				matrizLocations[161][0] = "INOMASICON";
				matrizLocations[161][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/ConsultaAsignacionServlet?" + BitaConstants.FLUJO
						+ "=" + BitaConstants.ES_NOMINA_EMP_CONS_ALTA;
	     } else {
				matrizLocations[161][0] = "SinFacultades";
				matrizLocations[161][1] = "/Enlace/" + Global.WEB_APPLICATION
						+ "/SinFacultades";
	     }
		/*Fin- P020101  Cuenta Nivel 3 - Menu*/
		/*Inicio - Stefanini  Cuenta Nivel 1 - Menu*/
	     //SERVICIOS>Tarjeta de Pagos Santander >Vinculaci�n tarjeta � contrato>Alta
	     if ( verificaFacultad ( "ALTVINCTARJ" ,request)) {
			 matrizLocations[162][0]="ALTVINCTARJ";
			 matrizLocations[162][1]="/Enlace/"+Global.WEB_APPLICATION+"/EnlcAltaTarjeta?operacion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_EN_ALTA_TARJETA_VINCULACION;
	     } else {
			 matrizLocations[162][0]="SinFacultades";
			 matrizLocations[162][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     //SERVICIOS>Tarjeta de Pagos Santander >Vinculaci�n tarjeta � contrato>Consulta y Baja
	     if ( verificaFacultad ( "CONVINCTARJ" ,request)) {
			 matrizLocations[163][0]="CONVINCTARJ";
			 matrizLocations[163][1]="/Enlace/"+Global.WEB_APPLICATION+"/EnlcConsultaTarjeta?operacion=1&" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_EN_CONS_TARJETA_VINCULACION;
	     } else {
			 matrizLocations[163][0]="SinFacultades";
			 matrizLocations[163][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
	     }
	     /*Fin - Stefanini  Cuenta Nivel 1 - Menu*/
	     /*Fin - Stefanini  Cuenta Nivel 1 - Menu*/
	     /*Fin- P020101  Cuenta Nivel 3 - Menu*/

		//Inicio Stefanini - RSA
		matrizLocations[166][0]="SinFacultades";
		matrizLocations[166][1]="/Enlace/"+Global.WEB_APPLICATION+"/DesvinculacionDispositivosServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_DESV_DISP;

		matrizLocations[167][0]="SinFacultades";
		matrizLocations[167][1]="/Enlace/"+Global.WEB_APPLICATION+"/CambioImagenServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_CAMBIO_IMAGEN;

		matrizLocations[168][0]="SinFacultades";
		matrizLocations[168][1]="/Enlace/"+Global.WEB_APPLICATION+"/CambioPreguntaServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_CAMBIO_PREGUNTA;
		//Inicio Stefanini - RSA

	 	/* M/021044 -- Inicio Menu PDF XML */

		matrizLocations[169][0]=session.FAC_EDO_CTA_IND;//"SOLEDOCTAIND";
		/**
		 * 02/03/2015 FSW - Indra PYME
		 * Se cambia servlet
		 */
		matrizLocations[169][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConfEdosCtaTcIndServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_CONFIG_PDF_EDO_CTA_IND;

		matrizLocations[170][0]=session.FAC_CONS_EDO_HIST;//"CONSEDOHIST";
		matrizLocations[170][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConsultaHistEdoCtaServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.CONSULTA_HIST;

		matrizLocations[171][0]=session.FAC_EDO_CTA_MAS;//"SOLEDOCTAARCH";
		matrizLocations[171][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConfigMasPorCuentaServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_ENTRA_MOD_PAPERLESS_MASIVO;

		matrizLocations[178][0]=session.FAC_EDO_CTA_MAS;//"SOLEDOCTAARCH";
		matrizLocations[178][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConfigMasPorTCServlet?" +
		BitaConstants.FLUJO +"=" + EdoCtaConstantes.EA_ENTRA_MOD_PAPERLESS_MASIVO_TDC;


		matrizLocations[172][0]=session.FAC_CONS_EDO_CTA;//"CONSFOLIOSOLIC";
		matrizLocations[172][1]="/Enlace/"+Global.WEB_APPLICATION+"/ConsultaEdoCtaServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.CONSULTA_EDO_CUENTA;


		matrizLocations[173][0]=session.FAC_CON_PERHIST;//"SOLEDOCTAHIS";
		matrizLocations[173][1]="/Enlace/"+Global.WEB_APPLICATION+"/EstadoCuentaHistoricoServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.ES_SOL_ESTADO_CUENTA_HISTORICO_PDF;


		matrizLocations[174][0]=session.FAC_EDO_CTA_DESCARGA;//"DESCARGAEDOCTA";
		matrizLocations[174][1]="/Enlace/"+Global.WEB_APPLICATION+"/DescargaEdoCtaPDFServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.DESC_EDOCTA_DEDC00;

		matrizLocations[175][0]=session.FAC_EDO_CTA_DESCARGA;//"DESCARGAEDOCTA";
		matrizLocations[175][1]="/Enlace/"+Global.WEB_APPLICATION+"/DescargaEdoCtaXMLServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.DESC_EDOCTA_DEDC03;

		/* M/021044 -- Fin Menu PDF XML */

		//Stefanini cifrado 2013
		matrizLocations[176][0]="SinFacultades";
		matrizLocations[176][1]="/Enlace/"+Global.WEB_APPLICATION+"/CertificadoDigitalServlet?" +
		BitaConstants.FLUJO +"=" + BitaConstants.EA_CIFFRADO;
	    //FIN Stefanini cifrado 2013

		matrizLocations[179][0]="NoNecesaria";
		matrizLocations[179][1]="/Enlace/"+Global.WEB_APPLICATION+"/InternacRecibidas?" +
		BitaConstants.FLUJO +"=" + BitaConstants.CONS_TRAN_INTER_REC;


		/*if ( verificaFacultad ("FAC_CONSMOVTOS", request)) {
			matrizLocations[179][0]="FAC_CONSMOVTOS";
			matrizLocations[179][0]="NoNecesaria";
			matrizLocations[179][1]="/Enlace/"+Global.WEB_APPLICATION+"/InternacRecibidas?" +
			BitaConstants.FLUJO +"=" + BitaConstants.EA_RTI;
		} else if(verificaFacultad ("FAC_CONSMOVTOSTER", request)) {

			matrizLocations[179][0]="FAC_CONSMOVTOSTER";
			matrizLocations[179][1]="/Enlace/"+Global.WEB_APPLICATION+"/InternacRecibidas?" +
			BitaConstants.FLUJO +"=" + BitaConstants.EA_RTI;
		} else {
			matrizLocations[179][0]="SinFacultades";
			matrizLocations[179][1]="/Enlace/"+Global.WEB_APPLICATION+"/SinFacultades";
		}*/

		// MODULO FIEL
		if ( verificaFacultad ( BaseResource.FAC_CARGO_CHEQUES,request ) ) {

			//Registro Fiel
			matrizLocations[182][0]=BaseResource.FAC_CARGO_CHEQUES;
			matrizLocations[182][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/FielRegistro?" +
				BitaConstants.FLUJO +SIGNO_IGUAL + FielConstants.BITA_FIEL_ALTA;

			//Modificacion Fiel
			matrizLocations[183][0]=BaseResource.FAC_CARGO_CHEQUES;
			matrizLocations[183][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/FielModificacion?" +
				BitaConstants.FLUJO +SIGNO_IGUAL + FielConstants.BITA_FIEL_MODIFICA;

			//Baja Fiel
			matrizLocations[184][0]=BaseResource.FAC_CARGO_CHEQUES;
			matrizLocations[184][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/FielBaja?" +
				BitaConstants.FLUJO +SIGNO_IGUAL + FielConstants.BITA_FIEL_BAJA;

		} else {

			matrizLocations[182][0]="SinFacultades";
			matrizLocations[182][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/SinFacultades";

			matrizLocations[183][0]="SinFacultades";
			matrizLocations[183][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/SinFacultades";

			matrizLocations[184][0]="SinFacultades";
			matrizLocations[184][1]=CONTEXT_PATH_ENLACE+Global.WEB_APPLICATION+"/SinFacultades";

		}

	 } catch( Exception e ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 return matrizLocations;
     }
     //metodo para verificar si una facultad existe o no dentro del archivo de ambiente

/********************************************/
/** Metodo: verificaFacultad
 * @return boolean
 * @param String -> facultad
 * @param HttpServletRequest -> request
 */
/********************************************/
	public boolean verificaFacultad ( String facultad ,HttpServletRequest request)
	{
		//log("verificaFacultad : " + facultad);
		HttpSession sess = request.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");
		//log("verificaFacultad Res: " + session.getFacultad(facultad));

		if(session == null)
			return false;
		if(facultad == null)
			return false;

		return session.getFacultad(facultad);
	}

/********************************************/
/** Metodo: CreaEncabezado
 * @return String
 * @param String -> tituloPantalla
 * @param String -> posicionMenu
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String CreaEncabezado ( String tituloPantalla, String posicionMenu , HttpServletRequest request) {
	 String strEncabezado = "";
	 String v_contrato = "";
	 String tit_contrato = "";
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 if ( session.getContractNumber () != null ) {
	     tit_contrato = "Contrato:";
	     v_contrato = ObtenContUser (request);
	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "#############-----------------@@@@@@@@@---->"+v_contrato,
	    		 EIGlobal.NivelLog.DEBUG);
	 }//ivn Q24649 cambio gbo50001.gif por demo2_off.gif y gbo50002.gif por demo2_on.gif
	 //ruta de este archivo /proarchivapp/ias/APPS/enlaceMig/enlaceMig/WEB-INF/classes/EnlaceMig
	 //EIGlobal.mensajePorTrace ( "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\nIVN es tu fuente  aki no se si vayaIVN\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", EIGlobal.NivelLog.INFO);//ivn borrar esta linea
	 strEncabezado = new String ( "<table width=\"720\" border=\"0\" cellspacing=\"0\"" +
	 " cellpadding=\"0\">\n" +
	 "<tr>\n" +
	 "<td width=\"676\" valign=\"top\" align=\"left\">\n" +
	 "<table width=\"610\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n" +
	 "<tr>\n" +
	 "<td width=\"528\" valign=\"top\" class=\"titpag\">\n" + tituloPantalla + "</td>\n" +
	 "<td width=\"115\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n" +
	 ObtenFecha () + "</td>\n" +
	 "</tr>\n" +
	 "<tr>\n" +
	 "<td valign=\"top\" class=\"texencrut\">\n" +
	 "<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n" + posicionMenu +
	 "</td>\n" +
	 "<td class=\"texenchor\" align=\"right\" valign=\"top\">\n" + ObtenHora () +
	 "</td>\n" +
	 "</tr>\n" +
	 "</table>\n" +
	 "</td>\n" +

	 "<script>"+
"function MostrarDemo() {"+
"	msg = window.open('/index_demo.html',null,'left=0,top=0,height=500,width=800,menubar=0,resizable=0,scrollbars=1,status=0,titlebar=0,toolbar=0,left=0,top=0');"+
"	msg.focus();"+
"}"+
	 "</script>"+

	 "<td width=\"40\" valign=\"top\"><a href=\"javascript:MostrarDemo();\""+
	 " onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('Cuentas',''," +
	 "'/gifs/EnlaceMig/demo2_on.gif',1)\">\n " +
	 "<img src=\"/gifs/EnlaceMig/demo2_off.gif\"   name=\"Cuentas\"" +

	 " width=\"40\" height=\"49\"  border=\"0\"></a></td>\n" +
	 "<td width=\"40\" valign=\"top\"><a href=\"javascript:VentanaAyuda('MENU');\"" +
	 " onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda',''," +
	 "'/gifs/EnlaceMig/gbo25171.gif',1)\">\n" +
	 "<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\"" +
	 " border=\"0\"></a></td>\n" +
	 "<td width=\"44\" valign=\"top\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/" +
	 "logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion'," +
	 "'','/gifs/EnlaceMig/gbo25181.gif',1)\">\n" +
	 "<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\"" +
	 " name=\"finsesion\" border=\"0\"></a></td>\n\n" +
	 "</tr>\n" +
	 "</table>\n" +
	 //Contrato
	 "<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n" +
	 "<tr>\n" +
	 "<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">" +
	 tit_contrato+"</span>\n" + v_contrato + "</td>\n" +
	 "</tr>\n" +
	 "</table>\n" +
	 "<Script language = \"JavaScript\">\n" +
	 "<!--\n" + "function FrameAyuda(ventana){\n" +
	 "hlp=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/Ayuda?ClaveAyuda=\" + ventana ,\"hlp\",\"" +
	 "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes," +
	 "resizable=yes,width=400,height=450\");\n" + "hlp.focus();\n" +
	 "}\n" +
	 "var opciongralctas; "+
	 "function VentanaCuentas() \n" +
	 "{ opciongralctas=4; msg=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/cuentasSerfinSantander?Ventana=2&opcion=4\",\"Cuentas\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290\");"+
	 " msg.focus(); } \n"+
	 "<!-- Q24649: Se agrego la siguiente funcin para llamar el demo"+
	 "//-->\n"+
	 "</Script>\n");
	 return strEncabezado;
     }
     //Metodo con el parametro para la pantalla de ayuda

/**
      * *****************************************.
      *
      * @param tituloPantalla the titulo pantalla
      * @param posicionMenu the posicion menu
      * @param ClaveAyuda the clave ayuda
      * @param request the request
      * @return the string
      */
/** Metodo: CreaEncabezado
 * @return String
 * @param String -> tituloPantalla
 * @param String -> posicionMenu
 * @param String -> ClaveAyuda
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String CreaEncabezado ( String tituloPantalla, String posicionMenu, String ClaveAyuda ,HttpServletRequest request) {
     String lenSnetComFX = "";
	 String strEncabezado="";
	 String v_contrato="";
	 String tit_contrato="";
	 HttpSession sess = request.getSession ();
	 BaseResource session = null;
	 if(sess != null) {
		 session = (BaseResource) sess.getAttribute ("session");
		 if(session == null) {
		 	 session = new BaseResource();
		 }
	 }
	 else {
		 session = new BaseResource();
	 }
	 if ( session.getContractNumber () != null ) {
	     tit_contrato = "Contrato:";
	     v_contrato = ObtenContUser (request);
	     EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "#############-----------------@@@@@@@@@---->"+v_contrato,
	    		 EIGlobal.NivelLog.DEBUG);
	 }
	 StringBuffer strEnc = new StringBuffer ();
strEnc.append ("<script>");
strEnc.append ("function MostrarDemo() {");
strEnc.append ("	msg = window.open('/index_demo.html',null,'left=0,top=0,height=500,width=800,menubar=0,resizable=0,scrollbars=1,status=0,titlebar=0,toolbar=0,left=0,top=0');");
strEnc.append ("	msg.focus();");
strEnc.append ("}");
strEnc.append ("</script>");

	//Mostrar Ultimo Acceso
	String nombreUsuario = session.getNombreUsuarioReal();
	String fechaUltimoAcceso = session.getUltAccesoStr();

	strEnc.append ("<table width=\"930px\"> ");
	strEnc.append ("<tr class=\"texencfec\">");
	strEnc.append ("     <td width=\"150px\">Usuario:</td>");
	strEnc.append ("     <td width=\"300px\">").append(nombreUsuario).append("</td>");
	strEnc.append ("   <td width=\"200px\"></td>");
	strEnc.append ("     <td width=\"300px\">" + ObtenFecha () + "</td>");
	strEnc.append ("</tr>");
	strEnc.append ("<tr class=\"texencfec\">");
	strEnc.append ("     <td width=\"150px\">Ultimo acceso:</td>");
	strEnc.append ("     <td width=\"300px\">").append(fechaUltimoAcceso).append("</td>");
	strEnc.append ("   <td width=\"200px\"></td>");
	strEnc.append ("     <td width=\"300px\">" + ObtenHora () + "</td>");
	strEnc.append ("</tr>");
	strEnc.append ("</table>");
	 strEnc.append ("<table width=");
	 strEnc.append ("\"720\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	 strEnc.append ("\n<tr>\n");
	 strEnc.append ("<td width=\"676\" valign=\"top\" align=\"left\">\n");

	/*<VC proyecto="200710001" autor="ESC" fecha="29/05/2007" descripci?="LIGA A SUPERNET Validar si se pinta la liga para no desalinear el encabezado">*/

	strEnc.append ("<table width=\"420\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
	//if (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true")){
		 //strEnc.append ("<table width=\"560\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
		 //strEnc.append ("<table width=\"420\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
	//}else{
	 	 //strEnc.append ("<table width=\"610\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
		 //strEnc.append ("<table width=\"500\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
	//}


	 strEnc.append ("<tr>\n");

	 if (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true")) {
		strEnc.append ("<td width=\"445\" valign=\"top\" class=\"titpag\">\n");
	 } else {
		 strEnc.append ("<td width=\"528\" valign=\"top\" class=\"titpag\">\n");
	 }
	/*</VC>*/

	 strEnc.append (tituloPantalla);
	 strEnc.append ("</td>\n");
	 /*Se realiza la validacion para mostrar la liga a Supernet Comercios
	  * Aparece en esta posicion cuando se tienen los datos necesarios para la liga a supernet empresas
	  * Requerimiento Liga Enlace Supernet Comercios
	  * JPH VSWF 03/06/2009
	  * Se valida que el arreglo de cuentas sea masyoa cero indicando que existen cuentas afiliadas
	  * a supernet comercios y que el valor para la url de supernet empresas este en true.
	  */

	 HashMap<String, String> cuetasAfiliadas = (HashMap)session.getCtasSnetComercios();
	 EIGlobal.mensajePorTrace("BaseServlet::CreaEncabezado:: Iniciando validaci�n "
			                 +"Administrador Supernet Comercios", EIGlobal.NivelLog.DEBUG);
	 boolean facSnetComAdmin = verificaFacultad("ADMSNETCOMER", request);
	 /**
	 if(facSnetComAdmin){
		 EIGlobal.mensajePorTrace("Enlace a Supernet Comercios habilitado", EIGlobal.NivelLog.DEBUG);
		 strEnc.append ("<td width=\"44\" valign=\"middle\" rowspan=\"2\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout?destino=supernetComer&usrSnetComAdmin=admin\">\n");
		 strEnc.append ("<img src=\"/gifs/EnlaceMig/supernetComer.gif\"  border = \"0\" alt = \"Acceso Supernet Comercios\" onclick=\"return(confirm('Est&aacute a punto de salir de Enlace Internet para ingresar a Supernet Comercios. �Desea Continuar?'));\"/></a></td>\n");
     }else if((cuetasAfiliadas!=null && cuetasAfiliadas.size()>0)&& (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true") )){
		 EIGlobal.mensajePorTrace("Enlace a Supernet Comercios habilitado", EIGlobal.NivelLog.DEBUG);
		 strEnc.append ("<td width=\"44\" valign=\"middle\" rowspan=\"2\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout?destino=supernetComer\">\n");
		 strEnc.append ("<img src=\"/gifs/EnlaceMig/supernetComer.gif\"  border = \"0\" alt = \"Acceso Supernet Comercios\" onclick=\"return(confirm('Est&aacute a punto de salir de Enlace Internet para ingresar a Supernet Comercios. �Desea Continuar?'));\"/></a></td>\n");
	 }*/


	 //strEnc.append ("<td width=\"150\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n");
	 //strEnc.append (ObtenFecha ());
	 strEnc.append ("</td>\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("<tr>\n");
	 strEnc.append ("<td valign=\"top\" class=\"texencrut\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n");
	 strEnc.append (posicionMenu);
	 strEnc.append ("</td>\n");
	 /**
     if(facSnetComAdmin){
		 strEnc.append ("<td class=\"texenchor\" align=\"right\" colspan=\"2\" valign=\"top\">\n");
		// strEnc.append (ObtenHora ());
		 strEnc.append ("</td>\n");
     }else if((cuetasAfiliadas!=null && cuetasAfiliadas.size()>0)){
		 strEnc.append ("<td class=\"texenchor\" align=\"right\" colspan=\"2\" valign=\"top\">\n");
		 //strEnc.append (ObtenHora ());
		 strEnc.append ("</td>\n");
	 }else{
	 strEnc.append ("<td class=\"texenchor\" align=\"right\" valign=\"top\">\n");
	 //strEnc.append (ObtenHora ());
	 strEnc.append ("</td>\n");
	 }**/
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 strEnc.append ("</td>\n");

	 if (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true")
			 && (facSnetComAdmin || (cuetasAfiliadas!=null && cuetasAfiliadas.size()>0))){
		 strEnc.append("<td width=\"44\" valign=\"middle\">");
		 strEnc.append("<img border=\"0\" width=\"0\" src=\"/gifs/EnlaceMig/gau25010.gif\"></td>");
	 }else if (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true")
			 && !(facSnetComAdmin || (cuetasAfiliadas!=null && cuetasAfiliadas.size()>0))){
		 strEnc.append("<td width=\"44\" valign=\"middle\">");
		 strEnc.append("<img border=\"0\" width=\"90\" src=\"/gifs/EnlaceMig/gau25010.gif\"></td>");
	 }else if (!(session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true"))
			 && (facSnetComAdmin || (cuetasAfiliadas!=null && cuetasAfiliadas.size()>0))){
		 strEnc.append("<td width=\"44\" valign=\"middle\">");
		 strEnc.append("<img border=\"0\" width=\"80\" src=\"/gifs/EnlaceMig/gau25010.gif\"></td>");
	 }else if (!(session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true"))
			 && !(facSnetComAdmin || (cuetasAfiliadas!=null && cuetasAfiliadas.size()>0))){
		 strEnc.append("<td width=\"44\" valign=\"middle\">");
		 strEnc.append("<img border=\"0\" width=\"170\" height=\"20\" src=\"/gifs/EnlaceMig/gau25010.gif\"></td>");
	 }


	 //ivn cambio gbo50001.gif por demo2_off.gif y gbo50002.gif por demo2_on.gif
	 //ruta de este archivo /proarchivapp/ias/APPS/enlaceMig/enlaceMig/WEB-INF/classes/EnlaceMig
	 //EIGlobal.mensajePorTrace ( "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\nIVN es tu fuente IVN\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", EIGlobal.NivelLog.INFO);//ivn borrar esta linea
	  /*<VC proyecto="200710001" autor="JGR" fecha="29/05/2007" descripci?="LIGA A SUPERNET">*/
	 if (session.getUrlSuperNet()!=null && session.getUrlSuperNet().equals("true")){
		 EIGlobal.mensajePorTrace("Se pinta la url a SuperNet", EIGlobal.NivelLog.DEBUG);
		 strEnc.append ("<td width=\"44\" valign=\"middle\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout?destino=supernet\">\n");
		 strEnc.append ("<img src=\"/gifs/EnlaceMig/img_supernet.png\"  border = \"0\" alt = \"Acceso Supernet Empresas\" /></a></td>\n");
	 }

	 /*Se realiza la validacion para mostrar la liga a Supernet Comercios
	  * Requerimiento Liga Enlace Supernet Comercios
	  * JPH VSWF 03/06/2009
	  * Se valida que el arreglo de cuentas sea masyoa cero indicando que existen cuentas afiliadas
	  * a supernet comercios y que el valor para la url de supernet empresas este en false.
	  */
	 if(facSnetComAdmin){
		 EIGlobal.mensajePorTrace("Enlace a Supernet Comercios habilitado", EIGlobal.NivelLog.DEBUG);
		 strEnc.append ("<td width=\"44\" valign=\"middle\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout?destino=supernetComer&usrSnetComAdmin=admin\">\n");
		 strEnc.append ("<img src=\"/gifs/EnlaceMig/supernetComer.gif\"  border = \"0\" alt = \"Acceso Supernet Comercios\" onclick=\"return(confirm('Est&aacute a punto de salir de Enlace Internet para ingresar a Supernet Comercios. �Desea Continuar?'));\"/></a></td>\n");
	 }else if((cuetasAfiliadas!=null && cuetasAfiliadas.size()>0)){
		 EIGlobal.mensajePorTrace("Enlace a Supernet Comercios habilitado", EIGlobal.NivelLog.DEBUG);
		 strEnc.append ("<td width=\"44\" valign=\"middle\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout?destino=supernetComer\">\n");
		 strEnc.append ("<img src=\"/gifs/EnlaceMig/supernetComer.gif\"  border = \"0\" alt = \"Acceso Supernet Comercios\" onclick=\"return(confirm('Est&aacute a punto de salir de Enlace Internet para ingresar a Supernet Comercios. �Desea Continuar?'));\"/></a></td>\n");
	 }

     /*<VC>*/
	 strEnc.append ("<td><img width=\"126\" height=\"33\" border=\"0\" src=\"/gifs/EnlaceMig/gba25010.gif\"></td>\n");
	 strEnc.append ("<td width=\"40\" valign=\"top\"><a href=\"javascript:MostrarDemo();\"");
	 strEnc.append (" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('Cuentas','',");
	 strEnc.append ("'/gifs/EnlaceMig/demo2_on.gif',1)\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/demo2_off.gif\"   name=\"Cuentas\"");
	 strEnc.append (" width=\"40\" height=\"49\"  border=\"0\"></a></td>\n");
	 strEnc.append ( "<td width=\"40\" valign=\"top\"><a href=\"javascript:FrameAyuda('" );
	 strEnc.append ( ClaveAyuda );
	 strEnc.append ("');\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\" border=\"0\"></a></td>\n");
	 //strEnc.append("<td width=\"44\" valign=\"top\"><a href=\"logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
	 strEnc.append ("<td width=\"44\" valign=\"top\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\" name=\"finsesion\" border=\"0\"></a></td>\n\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 //Contrato
	 strEnc.append ("<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n");
	 strEnc.append ("<tr>\n");
	 strEnc.append ("<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">");
	 strEnc.append (tit_contrato);
	 strEnc.append ("</span>\n");
	 strEnc.append (v_contrato);
	 strEnc.append ("</td>\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 strEnc.append ("<Script language = \"JavaScript\">\n");
	 strEnc.append ("<!--\n");
	 strEnc.append ("function FrameAyuda(ventana){\n");
	 strEnc.append ("hlp=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/Ayuda?ClaveAyuda=\" + ventana ,\"hlp\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450\");\n");
	 strEnc.append ("hlp.focus();\n");
	 strEnc.append ("}\n");
	 strEnc.append ("function VentanaCuentas() \n" );
	 strEnc.append ("{ opciongralctas=4;	msg=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/cuentasSerfinSantander?Ventana=2&opcion=4\",\"Cuentas\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290\");\n");
	 strEnc.append (" msg.focus(); } \n\n");
	 strEnc.append ("//-->\n");
	 strEnc.append ("</Script>\n");
	 strEncabezado= strEnc.toString ();
	 return strEncabezado;
     }

/********************************************/
/** Metodo: CreaEncabezadoContrato
 * @return String
 * @param String -> tituloPantalla
 * @param String -> posicionMenu
 * @param String -> ClaveAyuda
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String CreaEncabezadoContrato ( String tituloPantalla, String posicionMenu, String ClaveAyuda ,HttpServletRequest request) {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 String strEncabezado = "";
	 String v_contrato = "";
	 String tit_contrato = "";
	 if ( session.getContractNumber () != null ) {


	     tit_contrato = "Contrato:";
	     v_contrato = ObtenContUser (request);
	     //log("#############-----------------@@@@@@@@@---->"+v_contrato);
	 }
	 StringBuffer strEnc = new StringBuffer ();
	 strEnc.append ("<table width=");
	 strEnc.append ("\"720\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	 strEnc.append ("\n<tr>\n");
	 strEnc.append ("<td width=\"676\" valign=\"top\" align=\"left\">\n");
	 strEnc.append ("<table width=\"610\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\">\n");
	 strEnc.append ("<tr>\n");
	 strEnc.append ("<td width=\"528\" valign=\"top\" class=\"titpag\">\n");
	 strEnc.append (tituloPantalla);
	 strEnc.append ("</td>\n");
	 strEnc.append ("<td width=\"115\" align=\"right\" valign=\"bottom\" class=\"texencfec\">\n");
	 strEnc.append (ObtenFecha ());
	 strEnc.append ("</td>\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("<tr>\n");
	 strEnc.append ("<td valign=\"top\" class=\"texencrut\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gic25030.gif\" width=\"7\" height=\"13\"> \n");
	 strEnc.append (posicionMenu);
	 strEnc.append ("</td>\n");
	 strEnc.append ("<td class=\"texenchor\" align=\"right\" valign=\"top\">\n");
	 strEnc.append (ObtenHora ());
	 strEnc.append ("</td>\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 strEnc.append ("</td>\n");
	 strEnc.append ( "<td width=\"40\" valign=\"top\"><a href=\"javascript:FrameAyuda('" );
	 strEnc.append ( ClaveAyuda );
	 strEnc.append ("');\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('ayuda','','/gifs/EnlaceMig/gbo25171.gif',1)\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gbo25170.gif\" width=\"33\" height=\"49\" name=\"ayuda\" border=\"0\"></a></td>\n");
	 strEnc.append ("<td width=\"44\" valign=\"top\"><a href=\"/Enlace/"+Global.WEB_APPLICATION+"/logout\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)\">\n");
	 strEnc.append ("<img src=\"/gifs/EnlaceMig/gbo25180.gif\" width=\"44\" height=\"49\" name=\"finsesion\" border=\"0\"></a></td>\n\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 //Contrato
	 strEnc.append ("<table width=\"760\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">\n");
	 strEnc.append ("<tr>\n");
	 strEnc.append ("<td align=\"center\" valign=\"top\" class=\"texenccon\"><span class=\"texencconbol\">");
	 strEnc.append (tit_contrato);
	 strEnc.append ("</span>\n");
	 strEnc.append (v_contrato);
	 strEnc.append ("</td>\n");
	 strEnc.append ("</tr>\n");
	 strEnc.append ("</table>\n");
	 strEnc.append ("<Script language = \"JavaScript\">\n");
	 strEnc.append ("<!--\n");
	 strEnc.append ("function FrameAyuda(ventana){\n");
	 strEnc.append ("hlp=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/Ayuda?ClaveAyuda=\" + ventana ,\"hlp\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450\");\n");
	 strEnc.append ("hlp.focus();\n");
	 strEnc.append ("}\n");
	 strEnc.append ("function VentanaCuentas() \n" );
	 strEnc.append ("{ opciongralctas=4;	msg=window.open(\"/Enlace/"+Global.WEB_APPLICATION+"/cuentasSerfinSantander?Ventana=2&opcion=4\",\"Cuentas\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290\");");
	 strEnc.append (" msg.focus(); } ");
	 strEnc.append ("//-->\n");
	 strEnc.append ("</Script>\n");
	 strEncabezado = strEnc.toString ();
	 return strEncabezado;
     }
     //Funciones Agregadas para crear el menu de las aplicaciones
     //Rodrigo Godo, Junio 2001.

/********************************************/
/** Metodo: creaCodigoMenu
 * @return elmenu de tipo String
 * @param arregloDirecciones[][] de tipo String
 * @param request de tipo
 */
   public String creaCodigoMenu (String arregloDirecciones[][], HttpServletRequest request) {

  /*Stefanini - Se cambia Menu Epyme*/
    boolean esEpyme = false;
    String menuFXOnline = "";
    String menuTesoreria = "";
    HttpSession session = request.getSession();
    BaseResource sessBr = null;
    StringBuffer elMenuBuffer = new StringBuffer();
    String jspPmfso = request.getContextPath().concat("/jsp/rsa/pmfso.jsp");
    String jspPmfsoset = request.getContextPath().concat("/jsp/rsa/pmfso_set.jsp");
    String jspDevicePrint = request.getContextPath().concat("/jsp/rsa/setDevicePrint.jsp");

    if(session != null){
      sessBr = (BaseResource) session.getAttribute("session");
    	 if(sessBr != null){
    		 esEpyme = sessBr.isEpyme();

     }
    }

    /*SE oculta el Link de FXOnline*/
    if (!esEpyme){
    	menuFXOnline = "	   <td><a href=\"RET_PactaOper?opcion=iniciar\"><img width=\"112\" height=\"39\" border=\"0\" src=\"/gifs/EnlaceMig/fxOnline.gif\"></a>"
    			             + "<a href=\"EncriptionAES\"><img width=\"112\" height=\"39\" border=\"0\" src=\"/gifs/EnlaceMig/comex.gif\" onclick=\"return(confirm('El Portal Santander Trade es un conjunto de contenidos, bases de datos e informaci&oacute;n relacionada con el comercio exterior. Al momento de ingresar al Portal Santander Trade, por su seguridad, se cerrar&aacute; autom&aacute;ticamente la sesi&oacute;n abierta con Banco Santander &#40;M&eacute;xico&#41; S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico y se ingresar&aacute; a otro sitio web donde Export Enterprises, S.A. es la &uacute;nica responsable por la informaci&oacute;n y servicios ofrecidos por el Portal.'));\"></a><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"126\" height=\"33\"></td>";
    	menuTesoreria = "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[20][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"  onMouseOver=\"window.FW_showMenu(window.fw_menu_2,228,88);MM_nbGroup('over','gbo25050','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif', 1);MM_nbGroup('down','group1','gbo25050','/gifs/EnlaceMig/gbo25052.gif',1)\" ><img name=\"gbo25050\" src=\"/gifs/EnlaceMig/gbo25050.gif\" width=\"50\" height=\"37\" border=\"0\"></a></td>" ;
    }else{
    	menuFXOnline ="<td><a href=\"EncriptionAES\"><img width=\"112\" height=\"39\" border=\"0\" src=\"/gifs/EnlaceMig/comex.gif\" onclick=\"return(confirm('El Portal Santander Trade es un conjunto de contenidos, bases de datos e informaci&oacute;n relacionada con el comercio exterior. Al momento de ingresar al Portal Santander Trade, por su seguridad, se cerrar&aacute; autom&aacute;ticamente la sesi&oacute;n abierta con Banco Santander &#40;M&eacute;xico&#41; S.A., Instituci&oacute;n de Banca M&uacute;ltiple, Grupo Financiero Santander M&eacute;xico y se ingresar&aacute; a otro sitio web donde Export Enterprises, S.A. es la &uacute;nica responsable por la informaci&oacute;n y servicios ofrecidos por el Portal.'));\"></a><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"126\" height=\"33\"></td>";
    	menuTesoreria = "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[20][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"  onMouseOver=\"window.FW_showMenu(window.fw_menu_2,228,88);MM_nbGroup('over','gbo25050','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif', 1);MM_nbGroup('down','group1','gbo25050','/gifs/EnlaceMig/gbo25052.gif',1)\" ><img name=\"gbo25050\" src=\"/gifs/EnlaceMig/gbo25050.gif\" width=\"50\" height=\"37\" border=\"0\"></a></td>" ;
    }


	EIGlobal.mensajePorTrace("BaseServlet - esEpyme " + esEpyme,EIGlobal.NivelLog.DEBUG);

    String menuPyme="B";
    String mensajeTitle="En la opci&oacute;n de Men&uacute; reducido, usted podr&aacute; encontrar las funciones que son de mayor uso por nuestros clientes Pyme, con el fin de agilizar sus operaciones en Enlace, si desea m&aacute;s informaci&oacute;n llamar a Superl&iacute;nea Pyme al 01 800 911 5511 ";
    String mensajeMenuPyme="IMenuReducido.gif";

    if(esEpyme) {
    	menuPyme="A";
    	mensajeMenuPyme="SMenuReducido.gif";
    }


    EIGlobal.mensajePorTrace("BaseServlet - menuPyme " + menuPyme,EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("BaseServlet - mensajeMenuPyme " + mensajeMenuPyme,EIGlobal.NivelLog.DEBUG);

	 StringBuffer elmenu=new StringBuffer();
	 elmenu.append("<script language=\"JavaScript1.2\">fwLoadMenus();</script>"+
	 "<!-- Este es el codigo HTML del Navegador. Aqui es donde estan todas las imagenes y los botones que mandan llamar a los javascripts del menu -->"+
	 "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
	 "  <tr>"+
	 "    <td width=\"596\"> "+
	 "	<table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"596\">"+
	 "	  <!-- fwtable fwsrc=\"Botones del navegador.png\" fwbase=\"botones.gif\" fwstyle=\"Dreamweaver\" fwdocid = \"742308039\" fwnested=\"0\" -->"+
	 "	  <tr valign=\"top\"> "+
	 "	    <td colspan=\"10\" height=\"49\"> "+
	 "	      <table width=\"596\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"+
	 "		<tr valign=\"top\"> "+
	 "		  <td rowspan=\"2\"><img src=\"/gifs/EnlaceMig/glo25010.gif\" width=\"237\" height=\"41\"></td>"+
	 "		  <td rowspan=\"2\"><a href=\"Contactenos?ventana=0\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)\"><img src=\"/gifs/EnlaceMig/gbo25110.gif\" width=\"30\" height=\"33\" name=\"contactanos\" border=\"0\"></a></td>"+
	 "		  <td rowspan=\"2\"><img src=\"/gifs/EnlaceMig/gbo25120.gif\" width=\"59\" height=\"20\"></td>"+
	 "		  <td rowspan=\"2\"><a href=\"AtencionTelefonica\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)\"><img src=\"/gifs/EnlaceMig/gbo25130.gif\" width=\"36\" height=\"33\" name=\"atencion\" border=\"0\"></a></td>"+
	 "		  <td rowspan=\"2\"><img src=\"/gifs/EnlaceMig/gbo25140.gif\" width=\"90\" height=\"20\"></td>"+
	 "		  <td rowspan=\"2\"><a href=\"CentroMensajes?ventana=0\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)\"><img src=\"/gifs/EnlaceMig/gbo25150.gif\" width=\"33\" height=\"33\" name=\"centro\" border=\"0\"></a></td>"+
	 "		  <td rowspan=\"2\"><img src=\"/gifs/EnlaceMig/gbo25160.gif\" width=\"93\" height=\"20\"></td>"+
	 "		  <td width=\"18\" bgcolor=\"#DE0000\"><img src=\"/gifs/EnlaceMig/relleno.gif\" width=\"18\" height=\"20\"></td>"+
	 "		</tr>"+
	 "		<tr valign=\"top\">"+
	 "		  <td><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"18\" height=\"24\"></td>"+
	 "		</tr>"+
	 "	      </table>"+
	 "	    </td>"+
	 "	  </tr>"+
	 "	  <tr>"+
	 "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[0][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"	onMouseOver=\"window.FW_showMenu(window.fw_menu_0,0,88);MM_nbGroup('over','gbo25010','/gifs/EnlaceMig/gbo25011.gif','/gifs/EnlaceMig/gbo25012.gif', 1);MM_nbGroup('down','group1','gbo25010','/gifs/EnlaceMig/gbo25012.gif',1)\" ><img name=\"gbo25010\" src=\"/gifs/EnlaceMig/gbo25010.gif\" width=\"55\" height=\"37\" border=\"0\"></a></td>"+
	 "	    <td rowspan=\"2\" bgcolor=\"#ffffff\"></td>"+
	 "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[14][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"  onMouseOver=\"window.FW_showMenu(window.fw_menu_1,55,88);MM_nbGroup('over','gbo25030','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif', 1);MM_nbGroup('down','group1','gbo25030','/gifs/EnlaceMig/gbo25032.gif',1)\" ><img name=\"gbo25030\" src=\"/gifs/EnlaceMig/gbo25030.gif\" width=\"50\" height=\"37\" border=\"0\"></a></td>"+
	 "	    <td rowspan=\"2\" bgcolor=\"#ffffff\"></td>"+
	 /*Stefanini - Quitar Acceso default tesoreria para clientes Epyme*/
	 menuTesoreria
	 +

	 "	    <td rowspan=\"2\" bgcolor=\"#ffffff\"></td>"+
	 "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[180][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"  onMouseOver=\"window.FW_showMenu(window.fw_menu_3,258,86);MM_nbGroup('over','gbo25070','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif', 1);MM_nbGroup('down','group1','gbo25070','/gifs/EnlaceMig/gbo25072.gif',1)\" ><img name=\"gbo25070\" src=\"/gifs/EnlaceMig/gbo25070.gif\" width=\"50\" height=\"37\" border=\"0\"></a></td>"+
	 "	    <td rowspan=\"2\" bgcolor=\"#ffffff\"></td>"+
	 "	    <td rowspan=\"3\"><a href=\"" + arregloDirecciones[45][1] + "\" onMouseOut=\"MM_nbGroup('out');FW_startTimeout();\"  onMouseOver=\"window.FW_showMenu(window.fw_menu_4,412,88);MM_nbGroup('over','gbo25090','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif', 1);MM_nbGroup('down','group1','gbo25090','/gifs/EnlaceMig/gbo25092.gif',1)\" ><img name=\"gbo25090\" src=\"/gifs/EnlaceMig/gbo25090.gif\" width=\"50\" height=\"37\" border=\"0\"></a></td>"+
	 "	    <td bgcolor=\"#ffffff\"></td>"+
	 "	  </tr>"+
	 "	  <tr> "+
	 "	    <td rowspan=\"2\" valign=\"bottom\"><img name=\"txt_admon\" src=\"/gifs/EnlaceMig/gbo25100.gif\" width=\"84\" height=\"28\" border=\"0\"></td>"+
	 "	  </tr>"+
	 "	  <tr> "+
	 "	    <td valign=\"bottom\"><img name=\"txt_consultas\" src=\"/gifs/EnlaceMig/gbo25020.gif\" width=\"59\" height=\"21\" border=\"0\"></td>"+
	 "	    <td valign=\"bottom\"><img name=\"txt_transferencias\" src=\"/gifs/EnlaceMig/gbo25040.gif\" width=\"85\" height=\"21\" border=\"0\"></td>"+
	 "	    <td valign=\"bottom\"><img name=\"txt_tesoreria\" src=\"/gifs/EnlaceMig/gbo25060.gif\" width=\"57\" height=\"21\" border=\"0\"></td>"+
	 "	    <td valign=\"bottom\"><img name=\"txt_servicios\" src=\"/gifs/EnlaceMig/gbo25080.gif\" width=\"56\" height=\"21\" border=\"0\"></td>"+
	 "	  </tr>"+
	 "	  <tr> "+
	 "	    <td colspan=\"10\" bgcolor=\"#ffffff\"></td>"+
	 "	  </tr>"+
	 "	</table>"+
	 "    </td>"+
	 "    <td align=\"center\" width=\"100%\" valign=\"top\"> ");
		 elmenu.append(
				 "	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#FFFFFF\">"+
				 "		  <td rowspan=\"2\" valign=\"top\"><a href=\"csaldo1?prog=0&menuPyme="+menuPyme+"\" onMouseOut=\"MM_swapImgRestore()\" onMouseOver=\"MM_swapImage('pyme','','/gifs/EnlaceMig/SmenuPyme.gif',1)\"><img src=\"/gifs/EnlaceMig/CmenuPyme.gif\" width=\"32\" height=\"35\" name=\"pyme\" title=\""+mensajeTitle+"\" border=\"0\" onclick=\"return(confirm('�Est&aacute; seguro de continuar? En caso de aceptar y tener en proceso su operaci&oacute;n tendr&aacute; que realizarla nuevamente ya que ser&aacute; enviado a la pantalla de Consulta de Saldos. Para Continuar seleccione la opci&oacute;n Aceptar, de lo contrario seleccione la opci&oacute;n Cancelar'));\"></a></td>"+//FIXME
				 "		  <td rowspan=\"2\" valign=\"top\"><img src=\"/gifs/EnlaceMig/"+mensajeMenuPyme+"\" width=\"105\" height=\"20\"></td>");
		 elmenu.append("        <td valign=\"top\"><img src=\"/gifs/EnlaceMig/relleno.gif\"> </td>"+
				 "        <td valign=\"top\"><img src=\"/gifs/EnlaceMig/relleno.gif\"> </td>"+
				 "        <td valign=\"top\"><img src=\"/gifs/EnlaceMig/relleno.gif\"> </td>"+
				 "        <td valign=\"top\"><img src=\"/gifs/EnlaceMig/relleno.gif\"> </td>"+
				 "     </table>"+
		 "     <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#FFFFFF\">");
	 elmenu.append("	 <tr> ");

      /*Stefanini - Se modifca menu para FXOnline*/
	 elmenu.append(menuFXOnline);



	 elmenu.append( "	 </tr>"+
	 "	 <tr> "+
	 "	   <td background=\"/gifs/EnlaceMig/gfo25020.gif\"><img src=\"/gifs/EnlaceMig/gfo25020.gif\" width=\"5\" height=\"12\"></td>"+
	 "	 </tr>"+
	 "     </table>"+
	 "   </td>"+
	 " </tr>"+
	 "</table><!-- Fin del codigo HTML del Navegador -->");
	 return elmenu.toString();
     }

/********************************************/
/** Metodo: creaFuncionMenu
 * @return String
 * @param String[][] -> arregloDirecciones
 */
/********************************************/
	public String creaFuncionMenu(String[][] arregloDirecciones, HttpServletRequest request)
	 {

	 /*************************************************/
	 /* Tesofe					  */
	 /*************************************************/
	 String strTESOFEMenu="";
	 String strTESOFEOpcion="";
	 // StringBuffer para ir reemplazando a la variable String nuevoMenu.
	 StringBuffer sbNuevoMenu = new StringBuffer();

	 /*Stefanini - Modificacion para ocultar menus para clientes Epyme1*/
	 HttpSession session = null;
	 BaseResource sessBr = null;

	 boolean esEpyme = false;

	 session = request.getSession();
	 StringBuffer brEpyme = null;

	 if(session != null){
		 sessBr = (BaseResource) session.getAttribute("session");
		 if(sessBr != null){
			 esEpyme = sessBr.isEpyme();

		 }
	 }



	 	/**
	 	 * Menu de Transferencias Reuters - 201130200 Herramienta Web FX Spot BEI-SGC
	 	 *
	 	 */
	 	String strRET = "";
	 	strRET = " window.fw_menu_2_6 = new Menu(\"FX Online\",250,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" +
			     "   fw_menu_2_6.addMenuItem(\"Compra y Venta de Divisas\",\"location='"+ arregloDirecciones[154][1] + "'\");\n" +
			     "   fw_menu_2_6.addMenuItem(\"Liberaci�n de Operaciones\",\"location='"+ arregloDirecciones[155][1] + "'\");\n" +
			     "   fw_menu_2_6.addMenuItem(\"Consulta de Operaciones\",\"location='"+ arregloDirecciones[156][1] + "'\");\n" +
			     "   fw_menu_2_6.hideOnMouseOut=true;\n" +
	             "   fw_menu_2_6.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n";

	 if(arregloDirecciones[118][0].equals("true") && arregloDirecciones[118][1].equals("true"))
		{

			 strTESOFEMenu=
					" window.fw_menu_3_9 = new Menu(\"TESOFE\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
					"   fw_menu_3_9.addMenuItem(\"Consulta de Archivos y Registros\",\"location='MTE_ConsultaCancelacion?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_TESOFE_CONS_ARCH_REG + "'\");\n"+
					"   fw_menu_3_9.addMenuItem(\"Recaudaciones\",\"location='MTE_Recaudaciones?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_TESOFE_RECAUDACIONES + "'\");\n" +
					"   fw_menu_3_9.addMenuItem(\"Importaci&oacute;n - Env&iacute;o\",\"location='MTE_Importar?" +
					BitaConstants.FLUJO +"=" + BitaConstants.ES_TESOFE_IMPORTACION_ENVIO + "'\");\n";
			 if(Global.ACT_TESOFE.trim().equals("true")){
				 strTESOFEMenu += "   fw_menu_3_9.addMenuItem(\"Movimientos Estados de Cuenta\",\"location='MTE_MovEdoCuenta?opcion=iniciar" + "'\");\n";
			 }

		     strTESOFEMenu +=
					"   fw_menu_3_9.hideOnMouseOut=true;\n" +
					"   fw_menu_3_9.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n";

			 strTESOFEOpcion ="  fw_menu_3.addMenuItem(fw_menu_3_9);\n";
		}

	 /*************************************************/


		StringBuffer nuevomenu = new StringBuffer(/*new String(*/ "function unSoloContrato(){ \n" ).append( "cuadroDialogo ('Solo existe un contrato asociado.', 4);\n }" ).append( "function MM_nbGroup(event, grpName) { \n" ).append( "var i,img,nbArr,args=MM_nbGroup.arguments;\n" ).append( "  if (event == \"init\" && args.length > 2) {\n").append( "    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {\n").append( "	img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;\n" ).append( "	if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();\n" ).append( "	nbArr[nbArr.length] = img;\n" ).append( "	for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {\n" ).append( "	  if (!img.MM_up) img.MM_up = img.src;\n" ).append( "	  img.src = img.MM_dn = args[i+1];\n" ).append( "	  nbArr[nbArr.length] = img;\n" ).append( "    } }\n" ).append( "  } else if (event == \"over\") {\n" ).append( "    document.MM_nbOver = nbArr = new Array();\n" ).append( "	 for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {\n" ).append( "	if (!img.MM_up) img.MM_up = img.src;\n")
			.append( "	img.src = (img.MM_dn && args[i+2]) ? args[i+2] : args[i+1];\n" ).append( "	nbArr[nbArr.length] = img;\n" ).append( "    }\n" ).append( "  } else if (event == \"out\" ) {\n" ).append( "	 for (i=0; i < document.MM_nbOver.length; i++) {\n" ).append( " img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }\n" ).append( "  } else if (event == \"down\") {\n" ).append( "	if ((nbArr = document[grpName]) != null)\n" ).append( " for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }\n" ).append( "	 document[grpName] = nbArr = new Array();\n" ).append( "    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {\n" ).append( "	if (!img.MM_up) img.MM_up = img.src;\n" ).append( "	img.src = img.MM_dn = args[i+1];\n" ).append( " nbArr[nbArr.length] = img;\n" ).append( "  } }\n" ).append( "}\n" )
			//MENU CONSULTAS
			.append(" function fwLoadMenus() {\n" ).append( "  if (window.fw_menu_0) return;\n" ).append( " window.fw_menu_0_1_1 = new Menu(\"Por Cuenta\",135,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "  fw_menu_0_1_1.addMenuItem(\"Cheques\",\"location='" ).append( arregloDirecciones[0][1] ).append( "'\");\n" ).append( "	fw_menu_0_1_1.addMenuItem(\"Banca Especializada\",\"location='" ).append( arregloDirecciones[1][1] ).append( "'\");\n" ).append( "	fw_menu_0_1_1.addMenuItem(\"Tarjeta de Cr&eacute;dito\",\"location='" ).append( arregloDirecciones[2][1] ).append( "'\");\n" ).append( "	 fw_menu_0_1_1.hideOnMouseOut=true;\n" )
			//Stefanini - Se ocultara de menu
			.append( "	window.fw_menu_0_1_2 = new Menu(\"Consolidados\",100,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append( "	fw_menu_0_1_2.addMenuItem(\"Cheques\",\"location='" ).append( arregloDirecciones[3][1] ).append( "'\");\n" )

			//Se elimino por que esta en Construccion
			/*"	  fw_menu_0_1_2.addMenuItem(\"Banca Especializada\",\"location='"+arregloDirecciones[4][1]+"'\");\n"+ */
			.append("	 fw_menu_0_1_2.hideOnMouseOut=true;\n" ).append( "    window.fw_menu_0_1 = new Menu(\"Saldos\",99,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_0_1.addMenuItem(fw_menu_0_1_1);\n" + "	");

			/*Stefanini - Se oculta del menu Epyme  Consultas > Saldos > Consolidados > Cheques*/
			brEpyme=new StringBuffer(nuevomenu);

			if(!esEpyme){
				brEpyme.append(" fw_menu_0_1.addMenuItem(fw_menu_0_1_2);\n");
			}
			nuevomenu = new StringBuffer( brEpyme );
			brEpyme = null;

			nuevomenu.append( "	   fw_menu_0_1.hideOnMouseOut=true;\n" ).append( "     fw_menu_0_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "    window.fw_menu_0_2 = new Menu(\"Posici&oacute;n\",135,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"	)
			.append( "    fw_menu_0_2.addMenuItem(\"Chequeras\",\"location='" ).append( arregloDirecciones[5][1] ).append( "'\");\n")
			.append( "	 fw_menu_0_2.addMenuItem(\"Banca Especializada\",\"location='" ).append( arregloDirecciones[6][1] ).append( "'\");\n" ).append( "    fw_menu_0_2.addMenuItem(\"Tarjeta de Cr&eacute;dito\",\"location='" ).append( arregloDirecciones[7][1] ).append( "'\");\n" ).append( "	fw_menu_0_2.hideOnMouseOut=true;\n" )

			//Modificacin para el nuevo menu de Norma43
			//Stefanini - Modificacion clientes Epyme1
			.append("	window.fw_menu_0_3_1 = new Menu(\"Programados\",135,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append( "  fw_menu_0_3_1.addMenuItem(\"Registro\",\"location='" ).append( arregloDirecciones[106][1] ).append( "'\");\n" ).append( " fw_menu_0_3_1.addMenuItem(\"Consulta\",\"location='" ).append( arregloDirecciones[107][1] ).append( "'\");\n" )
			.append( "	 fw_menu_0_3_1.hideOnMouseOut=true;\n")

			.append( "	window.fw_menu_0_3_2 = new Menu(\"Chequeras\",200,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_0_3_2.addMenuItem(\"En L&iacute;nea\",\"location='" ).append( arregloDirecciones[8][1] ).append( "'\");\n");

			/*Stefanini - Se oculta del menu Epyme   Consultas > Movimientos > Chequeras > Programados*/

			brEpyme=new StringBuffer(nuevomenu);

			if(!esEpyme){
				brEpyme.append("	fw_menu_0_3_2.addMenuItem(fw_menu_0_3_1);\n");
			}
			nuevomenu = new StringBuffer( brEpyme );
			brEpyme = null;


			nuevomenu.append("  fw_menu_0_3_2.addMenuItem(\"Interbancarias Recibidas\",\"location='" ).append( arregloDirecciones[177][1] ).append( "'\");\n")
			/*Vector 2015 RMI*/
			//*******************INICIA TCS FSW 12/2016***********************************
			.append(" fw_menu_0_3_2.addMenuItem(\"Interbancarias Recibidas D&oacute;lares\",\"location='" ).append(arregloDirecciones[181][1] ).append( "'\");\n")
			//*******************FIN TCS FSW 12/2016***********************************
			.append("  fw_menu_0_3_2.addMenuItem(\"Internacionales Recibidas\",\"location='" ).append( arregloDirecciones[179][1] ).append( "'\");\n")
			/*FIN Vector 2015 RMI*/
			.append("	fw_menu_0_3_2.hideOnMouseOut=true;\n" ).append( "  fw_menu_0_3_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "    window.fw_menu_0_3 = new Menu(\"Movimientos\",176,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "    fw_menu_0_3.addMenuItem(fw_menu_0_3_2);\n" ).append( "	 fw_menu_0_3.addMenuItem(\"Banca Especializada\",\"location='" ).append( arregloDirecciones[9][1] ).append( "'\");\n" ).append( "    fw_menu_0_3.addMenuItem(\"Tarjeta de Cr&eacute;dito\",\"location='" ).append( arregloDirecciones[10][1] ).append( "'\");\n" ).append( "	fw_menu_0_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" );

			//Se elimino por que esta en Construccion
			/*"	fw_menu_0_3.addMenuItem(\"Terminales Punto de Venta\",\"location='"+arregloDirecciones[11][1]+"'\");\n"+ */

			/*Stefanini - Se oculta del menu Epyme  Consultas > Movimientos > Multicheque*/
			brEpyme=new StringBuffer(nuevomenu);

			if(!esEpyme){
				brEpyme.append("    fw_menu_0_3.addMenuItem(\"Multicheque\",\"location='" + arregloDirecciones[72][1] + "'\");\n");
				brEpyme.append("	   fw_menu_0_3.hideOnMouseOut=true;\n");
			}
			nuevomenu = new StringBuffer( brEpyme );
			brEpyme = null;


            nuevomenu.append( "  window.fw_menu_0 = new Menu(\"root\",171,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "  fw_menu_0.addMenuItem(fw_menu_0_1);\n" ).append( "  fw_menu_0.addMenuItem(fw_menu_0_2);\n" ).append( "  fw_menu_0.addMenuItem(fw_menu_0_3);\n" ).append( "  fw_menu_0.addMenuItem(\"Operaciones Programadas\",\"location='" ).append( arregloDirecciones[12][1] ).append( "'\");\n" ).append( "	fw_menu_0.addMenuItem(\"Bit&aacute;cora\",\"location='" ).append( arregloDirecciones[13][1] ).append( "'\");\n" ).append( "  fw_menu_0.addMenuItem(\"Informe de Comisiones\",\"location='" ).append( arregloDirecciones[71][1] ).append( "'\");\n" ).append( "	 fw_menu_0.hideOnMouseOut=true;\n" ).append( "	 fw_menu_0.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )
			//MENU TRANSFERENCIAS


            .append("	window.fw_menu_1_1_1 = new Menu(\"Moneda Nacional\",73,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( " fw_menu_1_1_1.addMenuItem(\"En l&iacute;nea\",\"location='" ).append( arregloDirecciones[14][1] ).append( "'\");\n");
			/*Stefanini -Se oculta del menu Epyme  Transferencia > Cuenta mismo Banco > Moneda Nacional > M�ltiple*/
			brEpyme=new StringBuffer(nuevomenu);

			if(!esEpyme){
				brEpyme.append(" fw_menu_1_1_1.addMenuItem(\"M&uacute;ltiple\",\"location='" ).append( arregloDirecciones[15][1] ).append( "'\");\n");
				brEpyme.append(" fw_menu_1_1_1.hideOnMouseOut=true;\n");
			}
			nuevomenu = new StringBuffer(brEpyme);
			brEpyme = null;

            nuevomenu.append( "   window.fw_menu_1_2 = new Menu(\"Ordenes de Pago Ocurre\",118,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "   fw_menu_1_2.addMenuItem(\"Personas F&iacute;sicas\",\"location='" ).append( arregloDirecciones[62][1] ).append( "'\");\n" ).append( "   fw_menu_1_2.addMenuItem(\"Personas Morales\",\"location='" ).append( arregloDirecciones[63][1] ).append( "'\");\n" ).append( "   fw_menu_1_2.hideOnMouseOut=true;\n")
            .append( "   window.fw_menu_1_1 = new Menu(\"Cuentas mismo Banco\",118,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "   fw_menu_1_1.addMenuItem(fw_menu_1_1_1);\n");


            /*Stefanini - Se oculta del menu Epyme  Transferencia > Cuenta mismo Banco > Moneda Nacional > M�ltiple*/
			brEpyme=new StringBuffer(nuevomenu);

			if(!esEpyme){
				brEpyme.append("   fw_menu_1_1.addMenuItem(\"D&oacute;lares\",\"location='" ).append( arregloDirecciones[16][1] ).append( "'\");\n");
				brEpyme.append("    fw_menu_1_1.hideOnMouseOut=true;\n");
			}
			nuevomenu = new StringBuffer(brEpyme);
			brEpyme = null;

			 nuevomenu.append( "    fw_menu_1_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "   window.fw_menu_1_3 = new Menu(\"Tarjeta de Cr&eacute;dito\",118,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "   fw_menu_1_3.addMenuItem(\"Pago\",\"location='" ).append( arregloDirecciones[18][1] ).append( "'\");\n" ).append( "   fw_menu_1_3.addMenuItem(\"Disposici&oacute;n\",\"location='" ).append( arregloDirecciones[114][1] ).append( "'\");\n" ).append( " window.fw_menu_1 = new Menu(\"root\",275,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			 .append( " fw_menu_1.addMenuItem(fw_menu_1_1);\n");

			 /*Stefanini - Se oculta del menu Epyme  Transferencia > Ordenes de pago Ocurre*/
			 brEpyme=new StringBuffer(nuevomenu);
			if(!esEpyme){

				brEpyme.append(" fw_menu_1.addMenuItem(fw_menu_1_2);\n");
			}
			nuevomenu = new StringBuffer( brEpyme );
			brEpyme = null;


			nuevomenu.append( " fw_menu_1.addMenuItem(\"Interbancarias\",\"location='" ).append( arregloDirecciones[17][1] ).append( "'\");\n" ).append( " fw_menu_1.addMenuItem(fw_menu_1_3);\n" + " fw_menu_1.addMenuItem(\"Cambios (Transferencias M.N.-Dlls./Dlls.-M.N.)\",\"location='" ).append( arregloDirecciones[54][1] ).append( "'\");\n" ).append( " fw_menu_1.addMenuItem(\"Transferencias Internacionales\",\"location='" ).append( arregloDirecciones[55][1] ).append( "'\");\n" ).append( "  fw_menu_1.hideOnMouseOut=true;\n" ).append( "  fw_menu_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" );

			//MENU TESORERIA

			/*Stefanini - Se elimina todo el menu de tesoreria*/
			 brEpyme=new StringBuffer(nuevomenu);


				brEpyme.append("     window.fw_menu_2_1_1 = new Menu(\"Plazo\",149,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	 fw_menu_2_1_1.addMenuItem(\"Registro de Operaciones\",\"location='" + arregloDirecciones[22][1] + "'\");\n" + "     fw_menu_2_1_1.addMenuItem(\"Cambio de Instrucci&oacute;n\",\"location='" + arregloDirecciones[23][1] + "'\");\n");
				brEpyme.append("	fw_menu_2_1_1.hideOnMouseOut=true;\n");
				brEpyme.append("   window.fw_menu_2_1 = new Menu(\"Inversiones\",161,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");


				brEpyme.append("	 fw_menu_2_1.addMenuItem(\"Fondos de Inversi&oacute;n\",\"location='" + arregloDirecciones[19][1] + "'\");\n");


				brEpyme.append("    fw_menu_2_1.addMenuItem(\"Vista\",\"location='" + arregloDirecciones[20][1] + "'\");\n");

				if(!esEpyme){
				brEpyme.append("    fw_menu_2_1.addMenuItem(fw_menu_2_1_1);\n");
				}
				brEpyme.append("	fw_menu_2_1.hideOnMouseOut=true;\n" + " fw_menu_2_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");
				brEpyme.append("	  window.fw_menu_2_2_1_1 = new Menu(\"Consultas\",94,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	  fw_menu_2_2_1_1.addMenuItem(\"Saldo\",\"location='" + arregloDirecciones[24][1] + "'\");\n" + "	  fw_menu_2_2_1_1.addMenuItem(\"Posici&oacute;n\",\"location='" + arregloDirecciones[25][1] + "'\");\n" + "	  fw_menu_2_2_1_1.addMenuItem(\"Movimientos\",\"location='" + arregloDirecciones[26][1] + "'\");\n" + "    fw_menu_2_2_1_1.hideOnMouseOut=true;\n" + "	window.fw_menu_2_2_1 = new Menu(\"Cr&eacute;dito en L&iacute;nea\",79,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_2_2_1.addMenuItem(fw_menu_2_2_1_1);\n" + "	fw_menu_2_2_1.addMenuItem(\"Prepago\",\"location='" + arregloDirecciones[27][1]);
				brEpyme.append("'\");\n" + "  fw_menu_2_2_1.hideOnMouseOut=true;\n" + "	 fw_menu_2_2_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" + "	window.fw_menu_2_2_2 = new Menu(\"Cr&eacute;dito Electr&oacute;nico\",100,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_2_2_2.addMenuItem(\"Consultas\",\"location='" + arregloDirecciones[96][1] + "'\");\n" + "	fw_menu_2_2_2.addMenuItem(\"Pagos\",\"location='" + arregloDirecciones[97][1] + "'\");\n" + "	fw_menu_2_2_2.addMenuItem(\"Disposici&oacute;n\",\"location='" + arregloDirecciones[98][1] + "'\");\n" + "	 fw_menu_2_2_2.hideOnMouseOut=true;\n" + "			window.fw_menu_2_2 = new Menu(\"Cr&eacute;dito\",125,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	 fw_menu_2_2.addMenuItem(fw_menu_2_2_1);\n" + "    fw_menu_2_2.addMenuItem(fw_menu_2_2_2);\n" + "    fw_menu_2_2.hideOnMouseOut=true;\n");
				brEpyme.append("	fw_menu_2_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" + "     window.fw_menu_2_3 = new Menu(\"Tesorer&iacute;a Internacional\",255,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_2_3.addMenuItem(\"Cambios (Transferencias M.N.-Dlls./Dlls.-M.N.)\",\"location='" + arregloDirecciones[57][1] + "'\");\n" + "	fw_menu_2_3.addMenuItem(\"Transferencias Internacionales\",\"location='" + arregloDirecciones[58][1] + "'\");\n" + "	 fw_menu_2_3.hideOnMouseOut=true;\n" + "     fw_menu_2_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" + " window.fw_menu_2_4_1 = new Menu(\"Mantenimiento de Estructuras\",120,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_2_4_1.addMenuItem(\"Concentraci&oacute;n\",\"location='" + arregloDirecciones[64][1] + "'\");\n" + "	fw_menu_2_4_1.addMenuItem(\"Dispersi&oacute;n\",\"location='");
				brEpyme.append(arregloDirecciones[65][1] + "'\");\n" + "	fw_menu_2_4_1.addMenuItem(\"Fondeo Autom&aacute;tico\",\"location='" + arregloDirecciones[66][1] + "'\");\n" + "	 fw_menu_2_4_1.hideOnMouseOut=true;\n" + "	window.fw_menu_2_4_2 = new Menu(\"Consulta de Saldos\",91,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_2_4_2.addMenuItem(\"Concentraci&oacute;n\",\"location='" + arregloDirecciones[67][1] + "'\");\n" + "	fw_menu_2_4_2.addMenuItem(\"Dispersi&oacute;n\",\"location='" + arregloDirecciones[68][1] + "'\");\n" + "	 fw_menu_2_4_2.hideOnMouseOut=true;\n" + "	window.fw_menu_2_4_3 = new Menu(\"Consulta de Movimientos\",91,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + " fw_menu_2_4_3.addMenuItem(\"Concentraci&oacute;n\",\"location='" + arregloDirecciones[69][1] + "'\");\n" + "	fw_menu_2_4_3.addMenuItem(\"Dispersi&oacute;n\",\"location='" + arregloDirecciones[70][1]);
				brEpyme.append("'\");\n" + "  fw_menu_2_4_3.hideOnMouseOut=true;\n" + "    window.fw_menu_2_4 = new Menu(\"Tesorer&iacute;a Inteligente\",185,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "    fw_menu_2_4.addMenuItem(fw_menu_2_4_1);\n" + "    fw_menu_2_4.addMenuItem(fw_menu_2_4_2);\n" + "	 fw_menu_2_4.addMenuItem(fw_menu_2_4_3);\n" + "     fw_menu_2_4.hideOnMouseOut=true;\n" + "	fw_menu_2_4.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" + "	window.fw_menu_2_5_1 = new Menu(\"Programaci&oacute;n de Operaci&oacute;n\",84,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + " fw_menu_2_5_1.addMenuItem(\"Consulta\",\"location='" + arregloDirecciones[85][1] + "'\");\n" + "	fw_menu_2_5_1.addMenuItem(\"Registro\",\"location='" + arregloDirecciones[86][1] + "'\");\n" + "	 fw_menu_2_5_1.hideOnMouseOut=true;\n");
				brEpyme.append("    window.fw_menu_2_5 = new Menu(\"Esquema Base Cero\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "    fw_menu_2_5.addMenuItem(\"Mantenimiento de Estructura\",\"location='" + arregloDirecciones[83][1] + "'\");\n" + "	fw_menu_2_5.addMenuItem(\"Consulta de Saldos\",\"location='" + arregloDirecciones[84][1] + "'\");\n" + "    fw_menu_2_5.addMenuItem(fw_menu_2_5_1);\n" + "     fw_menu_2_5.hideOnMouseOut=true;\n" + "	   fw_menu_2_5.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" + strRET);

				brEpyme.append("  window.fw_menu_2 = new Menu(\"root\",150,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
				brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_1);\n");
				brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_2);\n");

				if(!esEpyme){
					brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_3);\n");
					brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_4);\n");
					brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_5);\n");
					brEpyme.append("  fw_menu_2.addMenuItem(fw_menu_2_6);\n");
				}

				brEpyme.append("   fw_menu_2.hideOnMouseOut=true;\n");
				brEpyme.append("   fw_menu_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" );

			nuevomenu = new StringBuffer(brEpyme);
			brEpyme = null;

			 nuevomenu.append(

			//MENU SERVICIOS
			"	window.fw_menu_3_1_1 = new Menu(\"Mantenimiento de Cuentas\",48,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_1_1.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[28][1] ).append( "'\");\n" ).append( "	fw_menu_3_1_1.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[29][1] ).append( "'\");\n" ).append( "	 fw_menu_3_1_1.hideOnMouseOut=true;\n" ).append( "    window.fw_menu_3_1 = new Menu(\"Impuestos Federales\",175,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_3_1.addMenuItem(fw_menu_3_1_1);\n" ).append( "    fw_menu_3_1.addMenuItem(\"Pagos\",\"location='" ).append( arregloDirecciones[30][1] ).append( "'\");\n" )
			/*	 SE QUITA PARA INCIDENCIA IM50600   "	 fw_menu_3_1.addMenuItem(\"Env&iacute;o de Declaraci&oacute;n\",\"location='"+arregloDirecciones[31][1]+"'\");\n"+ */
			.append("    fw_menu_3_1.addMenuItem(\"Consultas\",\"location='" ).append( arregloDirecciones[32][1] ).append( "'\");\n" ).append( "	 fw_menu_3_1.hideOnMouseOut=true;\n" ).append( "     fw_menu_3_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "    window.fw_menu_3_7 = new Menu(\"Nuevo Esquema de Pago de Impuestos\",250,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");










			/**
			*		Modificado por Miguel Cortes Arellano
			*		Fecha: 09/01/2004
			*		Proposito: Adherir 2 submenus para los pagos de impuestos de Credito Fiscal y Entidades Federativas.
			*/
			//Opcin de menu "Creditos Fiscales" deshabilitada el 19 Oct 2004
			 nuevomenu.append("    //fw_menu_3_7.addMenuItem(\"Creditos Fiscales\",\"location='" ).append( arregloDirecciones[104][1] ).append( "'\");\n");
			/*Stefanini - Se oculta del menu Epyme  */














			//Stefanini - Menu de Derechos, Productos y Aprovechamientos  (vcl)
			nuevomenu.append( "  fw_menu_3_7.addMenuItem(\"Derechos, Productos y Aprovechamientos\",\"location='" ).append( arregloDirecciones[116][1] ).append( "'\");\n" )
			//Finaliza Codigo Sinapsis
			// Menu de Pago de Impuestos por Linea de Captura (Dic 2005)
			/*
			 * Mejoras de Linea de Captura
			 * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
			 * Inicio RRR - Indra Marzo 2014
			 */
			.append("    fw_menu_3_7.addMenuItem(\"Pago Referenciado SAT\",\"location='" ).append( arregloDirecciones[120][1] ).append( "'\");\n" )
			.append("     fw_menu_3_7.hideOnMouseOut=true;\n")
			/*
			 * Fin RRR - Indra Marzo 2014
			 */
			// VSWF - Se agrega menu SIPARE (inicio)

			.append( "	 window.fw_menu_3_2 = new Menu(\"Aportaciones Obrero Patronales (S.U.A.)\",79,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	 fw_menu_3_2.addMenuItem(\"Pagos\",\"location='" ).append( arregloDirecciones[33][1] ).append( "'\");\n")
			.append( "	 fw_menu_3_2.addMenuItem(\"Consultas\",\"location='" ).append( arregloDirecciones[34][1] ).append( "'\");\n")
			.append( " fw_menu_3_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )

			// VSWF - Se agrega menu SIPARE (fin)

			/* "	window.fw_menu_3_3_1 = new Menu(\"Pagos\",160,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"	fw_menu_3_3_1.addMenuItem(\"Importaci&oacute;n - Envio\",\"location='"+arregloDirecciones[36][1]+"'\");\n"+
			"	fw_menu_3_3_1.addMenuItem(\"Consultas - Cancelaciones\",\"location='"+arregloDirecciones[100][1]+"'\");\n"+
			"	 fw_menu_3_3_1.hideOnMouseOut=true;\n"+
			"    window.fw_menu_3_3 = new Menu(\"N&oacute;mina\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"    fw_menu_3_3.addMenuItem(\"Mantenimiento de Empleados\",\"location='"+arregloDirecciones[35][1]+"'\");\n"+
			"    fw_menu_3_3.addMenuItem(fw_menu_3_3_1);\n"+
			//"	fw_menu_3_3.addMenuItem(\"Pagos\",\"location='"+arregloDirecciones[36][1]+"'\");\n"+
			"    fw_menu_3_3.addMenuItem(\"Interbancaria\",\"location='"+arregloDirecciones[60][1]+"'\");\n"+
			"     fw_menu_3_3.hideOnMouseOut=true;\n"+
			"     fw_menu_3_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n"+*/
			/*
			* Modificado por : Miguel Cortes Arellano
			* Fecha: 4/12/2003
			* Empieza codigo Sinapsis
			* Proposito: Adherir al menu de Empleados (Antes Mantenimiento de Empleados) los 2 siguientes submenus
			*				  - Mantenimiento
			*				  - Consultas
			*/
			//"	window.fw_menu_3_3_1 = new Menu(\"Empleados\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "	fw_menu_3_3_1.addMenuItem(\"Mantenimiento\",\"location='" + arregloDirecciones[35][1] + "'\");\n" + "	fw_menu_3_3_1.addMenuItem(\"Consulta de Proceso de Alta\",\"location='" + arregloDirecciones[110][1] + "'\");\n" + "	 fw_menu_3_3_1.hideOnMouseOut=true;\n" +


			//everisSe modifica el
			/************************************************************************
			 *everis.
			 *Feb/2008
			 *Se modifica el men� para agregar las opciones
			 *		Catalogo de N�mina
			 *			Busqueda
			 *			Modificacion por Archivo
			 *			Baja por Archivo
			 *		Mantenimiento Intebancario
			 *			Alta en Linea
			 *			Alta por Archivo
			 *			Modificacion por Archivo
			************************************************************************/


			.append("	window.fw_menu_3_3_1_1 = new Menu(\"Cat&aacute;logo de N&oacute;mina\",240,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_3_1_1.addMenuItem(\"B&uacute;squeda\",\"location='" ).append( arregloDirecciones[121][1] ).append( "'\");\n");

				/*Stefanini - Se oculta del menu Epyme
				 * Servicios > Nomina > catalogo nomina > Alta por archivo
				   Servicios > Nomina > catalogo nomina > modificaci�n por archivo
				   Servicios > Nomina > catalogo nomina > baja por archivo*/
				brEpyme=new StringBuffer(nuevomenu);

				if(!esEpyme){
					brEpyme.append("	fw_menu_3_3_1_1.addMenuItem(\"Alta por archivo\",\"location='" ).append( arregloDirecciones[152][1] + "'\");\n");
					brEpyme.append("	fw_menu_3_3_1_1.addMenuItem(\"Modificaci&oacute;n por Archivo\",\"location='" ).append( arregloDirecciones[122][1] + "'\");\n");
					brEpyme.append("	fw_menu_3_3_1_1.addMenuItem(\"Baja por Archivo\",\"location='" ).append( arregloDirecciones[123][1] + "'\");\n");
				}
				nuevomenu = new StringBuffer(brEpyme);
				brEpyme = null;


				nuevomenu.append( "	fw_menu_3_3_1_1.addMenuItem(\"Liberaci&oacute;n de empleados no registrados\",\"location='" ).append( arregloDirecciones[153][1] ).append( "'\");\n")

				.append(
			"	window.fw_menu_3_3_1_2 = new Menu(\"Mantenimiento Interbancario\",220,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
				.append( "	fw_menu_3_3_1_2.addMenuItem(\"Alta en L&iacute;nea\",\"location='" ).append( arregloDirecciones[124][1] ).append( "'\");\n")
				.append( "	fw_menu_3_3_1_2.addMenuItem(\"Alta por Archivo\",\"location='" ).append( arregloDirecciones[125][1] ).append( "'\");\n")
				.append( "	fw_menu_3_3_1_2.addMenuItem(\"Modificaci&oacute;n por Archivo\",\"location='" ).append( arregloDirecciones[126][1] ).append( "'\");\n")

				.append(
			"	window.fw_menu_3_3_1 = new Menu(\"Empleados\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
				.append( "	fw_menu_3_3_1.addMenuItem(\"Mantenimiento\",\"location='" ).append( arregloDirecciones[35][1] ).append( "'\");\n")
				.append( "	fw_menu_3_3_1.addMenuItem(\"Consulta de Proceso de Alta\",\"location='" ).append( arregloDirecciones[110][1] ).append( "'\");\n")
				/*+ "	fw_menu_3_3_1.addMenuItem(fw_menu_3_3_1_1);\n"
				+ "	fw_menu_3_3_1.addMenuItem(fw_menu_3_3_1_2);\n"*/
				.append( "	 fw_menu_3_3_1.hideOnMouseOut=true;\n")
				.append( "	fw_menu_3_3_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )

			//Finaliza Codigo Sinapsis
			.append("	window.fw_menu_3_3_2 = new Menu(\"Pagos\",160,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");

			/*Stefanini - Se oculta del menu Epyme Servicios -> Nomina ->Pagos -> Importacion -Envio*/
			brEpyme=new StringBuffer(nuevomenu);
			if(!esEpyme){
				brEpyme.append("	fw_menu_3_3_2.addMenuItem(\"Importaci&oacute;n - Envio\",\"location='" ).append( arregloDirecciones[36][1] ).append( "'\");\n");
			}
			nuevomenu = new StringBuffer(brEpyme);
			brEpyme = null;

			nuevomenu
			.append( "	fw_menu_3_3_2.addMenuItem(\"Consultas - Cancelaciones\",\"location='" ).append( arregloDirecciones[100][1] ).append( "'\");\n")
			.append( "	 fw_menu_3_3_2.hideOnMouseOut=true;\n" )
			// "	fw_menu_3_3.addMenuItem(\"Mantenimiento de Empleados\",\"location='"+arregloDirecciones[35][1]+"'\");\n"+
			/*
			* Modificado por : Miguel Cortes Arellano
			* Fecha: 4/12/2003
			* Empieza codigo Sinapsis
			* Proposito: Adherir al menu de Empleados (Antes Mantenimiento de Empleados) los 2 siguientes submenus
			*				  - Mantenimiento
			*				  - Consultas
			*/

			/**************************************
			 *  INICIA MENU NOMINA EN LINEA
			 **************************************/
			.append(" window.fw_menu_3_3_1_6 = new Menu(\"N&oacute;mina en L&iacute;nea\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( " fw_menu_3_3_1_6.addMenuItem(\"Pagos\",\"location='")
			.append( arregloDirecciones[164][1])
			.append( "'\");\n")
			.append( " fw_menu_3_3_1_6.addMenuItem(\"Consulta\",\"location='")
			.append( arregloDirecciones[165][1])
			.append( "'\");\n")
			/**************************************
			 *  FINALIZA MENU NOMINA EN LINEA
			 **************************************/

			.append("    window.fw_menu_3_3 = new Menu(\"N&oacute;mina\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_3_3.addMenuItem(fw_menu_3_3_1);\n" ).append( "    fw_menu_3_3.addMenuItem(fw_menu_3_3_2);\n")
			.append( "	fw_menu_3_3.addMenuItem(fw_menu_3_3_1_1);\n")
				/*+ "	fw_menu_3_3.addMenuItem(fw_menu_3_3_1_2);\n"*/
			//Finaliza Codigo Sinapsis
			.append( "    fw_menu_3_3.addMenuItem(\"Interbancaria\",\"location='" ).append( arregloDirecciones[60][1] ).append( "'\");\n" ).append( " fw_menu_3_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "	window.fw_menu_3_4_1 = new Menu(\"Registro de Cheques\",85,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_4_1.addMenuItem(\"En L&iacute;nea\",\"location='" ).append( arregloDirecciones[37][1] ).append( "'\");\n" ).append( " fw_menu_3_4_1.addMenuItem(\"Por Archivo\",\"location='" ).append( arregloDirecciones[38][1] ).append( "'\");\n" ).append( "	 fw_menu_3_4_1.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_4_2 = new Menu(\"Mantenimiento de Beneficiarios\",91,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_4_2.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[40][1] ).append( "'\");\n" ).append( "	fw_menu_3_4_2.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[41][1] ).append( "'\");\n");

		/* Nomina en Linea - Validacion para mostrar menu NOMINA EN LINEA*/
            final StringBuffer br=new StringBuffer(nuevomenu);
			if (verificaFacultad ( FACULTADNLN ,request) && "SI".equals(Global.nlnVerMenu)) {
				br.append(" fw_menu_3_3.addMenuItem(fw_menu_3_3_1_6);\n");
			}
			nuevomenu = new StringBuffer(br);
		/* Nomina en Linea - FIN Validacion para mostrar menu NOMINA EN LINEA*/

			/*Vector - Nuevo Menu Epyme Nomina*/
			nuevomenu
			/*Submenu consultas*/
			.append( " window.fw_menu_3_99_2 = new Menu(\"Consultas\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_99_2.addMenuItem(\"Alta Empleados\",\"location='" ).append( arregloDirecciones[110][1] ).append( "'\");\n")
			.append( " fw_menu_3_99_2.addMenuItem(\"Pagos En L&iacute;nea\",\"location='"   	).append( arregloDirecciones[165][1] ).append( "'\");\n")
			.append( "	fw_menu_3_99_2.addMenuItem(\"Pagos Programados\",\"location='" ).append( arregloDirecciones[100][1] ).append( "'\");\n" )
			.append( "	fw_menu_3_99_2.hideOnMouseOut=true;\n" )
			/*Submenu Catalogo de nomina*/
			.append( "	window.fw_menu_3_99_4 = new Menu(\"Cat&aacute;logo de N&oacute;mina\",240,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_99_4.addMenuItem(\"B&uacute;squeda\",\"location='" ).append( arregloDirecciones[121][1] ).append( "'\");\n");

			if(!esEpyme){
				nuevomenu
				.append( "	fw_menu_3_99_4.addMenuItem(\"Alta por archivo\",\"location='" ).append( arregloDirecciones[152][1] ).append( "'\");\n")
				.append( "	fw_menu_3_99_4.addMenuItem(\"Modificaci&oacute;n por Archivo\",\"location='" ).append( arregloDirecciones[122][1] ).append( "'\");\n")
				.append( "	fw_menu_3_99_4.addMenuItem(\"Baja por Archivo\",\"location='" ).append( arregloDirecciones[123][1] ).append( "'\");\n");

			}

			nuevomenu
			.append( "	fw_menu_3_99_4.addMenuItem(\"Liberaci&oacute;n de empleados no registrados\",\"location='" ).append( arregloDirecciones[153][1] ).append( "'\");\n")
			.append( "	fw_menu_3_99_4.hideOnMouseOut=true;\n")
			/*Menu Principal*/
			.append( " window.fw_menu_3_99 = new Menu(\"N&oacute;mina\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( " fw_menu_3_99.addMenuItem(\"Pagos\",\"location='" ).append( arregloDirecciones[180][1] + "'\");\n")
			.append( " fw_menu_3_99.addMenuItem(fw_menu_3_99_2);\n")
			.append( "	fw_menu_3_99.addMenuItem(\"Alta de Empleados\",\"location='" ).append( arregloDirecciones[35][1] + "'\");\n")
			.append( " fw_menu_3_99.addMenuItem(fw_menu_3_99_4);\n")
			.append( " fw_menu_3_99.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" );

			/*Fin nuevo menu Nomina*/


			//Pruebas JGAL
			/* Menu Nomina Interbancaria
				- Nomina Interbancaria
					- Pagos*/
			nuevomenu.append(	"	window.fw_menu_3_3_1_4 = new Menu(\"Pagos\",160,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_3_1_4.addMenuItem(\"Importaci&oacute;n - Envio\",\"location='" ).append( arregloDirecciones[60][1] ).append( "'\");\n")
			/*+ "	fw_menu_3_3_1_4.addMenuItem(\"Consultas\",\"location='" + arregloDirecciones[100][1] +  "'\");\n"*/
			.append( "	 fw_menu_3_3_1_4.hideOnMouseOut=true;\n")

			.append( "	window.fw_menu_3_3_1_5 = new Menu(\"Registro de Cuentas\",220,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_3_1_5.addMenuItem(\"B&uacute;squeda\",\"location='" ).append( arregloDirecciones[127][1] +  "'\");\n")
			.append( "	fw_menu_3_3_1_5.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[140][1] ).append( "'\");\n")
			.append( "	fw_menu_3_3_1_5.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[141][1] ).append(  "'\");\n")
			.append( "	fw_menu_3_3_1_5.addMenuItem(\"Autorizaci&oacute;n y Cancelaci&oacute;n\",\"location='" ).append( arregloDirecciones[128][1] ).append(  "'\");\n")
			.append( "	 fw_menu_3_3_1_5.hideOnMouseOut=true;\n")




			.append( "    window.fw_menu_3_3_1_3 = new Menu(\"N&oacute;mina Interbancaria\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "    fw_menu_3_3_1_3.addMenuItem(fw_menu_3_3_1_4);\n" + "    fw_menu_3_3_1_3.addMenuItem(fw_menu_3_3_1_5);\n")
			.append( " fw_menu_3_3_1_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
			//Pruebas JGAL
			/*INICIA MENU NOMINA*/
			.append( 	" window.fw_menu_3_3_3_1 = new Menu(\"Remesas\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append(" fw_menu_3_3_3_1.addMenuItem(\"Recepci&oacute;n\",\"location='" ).append( arregloDirecciones[129][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_1.addMenuItem(\"Consulta\",\"location='" ).append( arregloDirecciones[130][1] ).append( "'\");\n" )

			.append(" window.fw_menu_3_3_3_2 = new Menu(\"Asignaci&oacute;n tarjeta-empleado\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Asignaci&oacute;n\",\"location='" ).append( arregloDirecciones[131][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Consulta\",\"location='" ).append( arregloDirecciones[132][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[133][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Bloqueo\",\"location='" ).append( arregloDirecciones[134][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Reposici&oacute;n\",\"location='" ).append( arregloDirecciones[135][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_2.addMenuItem(\"Consulta estado asignaci&oacute;n masiva\",\"location='" ).append( arregloDirecciones[136][1] ).append( "'\");\n" )

			.append(" window.fw_menu_3_3_3_3 = new Menu(\"Pagos\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append(" fw_menu_3_3_3_3.addMenuItem(\"Importaci&oacute;n env&iacute;o\",\"location='" ).append( arregloDirecciones[137][1] ).append( "'\");\n" )
				//" fw_menu 3_3_3_3.addMenuItem(\"Pago Individual\",\"location='" + arregloDirecciones[136][1] + "'\");\n" +
			.append(" fw_menu_3_3_3_3.addMenuItem(\"Pago Individual\",\"location='" ).append( arregloDirecciones[138][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_3.addMenuItem(\"Consulta\",\"location='" ).append( arregloDirecciones[139][1] ).append( "'\");\n" )

			.append(" window.fw_menu_3_3_3_4 = new Menu(\"Vinculaci&oacute;n tarjeta \",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append(" fw_menu_3_3_3_4.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[162][1] ).append( "'\");\n" )
			.append(" fw_menu_3_3_3_4.addMenuItem(\"Consulta y Baja\",\"location='" ).append( arregloDirecciones[163][1] ).append( "'\");\n" )

			.append(" window.fw_menu_3_3_3 = new Menu(\"Tarjeta de Pagos Santander \",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append(" fw_menu_3_3_3.addMenuItem(fw_menu_3_3_3_1);\n" )
			.append(" fw_menu_3_3_3.addMenuItem(fw_menu_3_3_3_2);\n" )
			.append(" fw_menu_3_3_3.addMenuItem(fw_menu_3_3_3_3);\n" )
     	   		//" fw_menu_3_3_3.addMenuItem(fw_menu_3_3_3_4);\n" +
			.append(" fw_menu_3_3_3.hideOnMouseOut=true;\n" )
     	   	.append(" fw_menu_3_3_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")

     	   	/*FIN MENU NOMINA*/

			.append( "	fw_menu_3_4_2.addMenuItem(\"Modificaci&oacute;n\",\"location='" ).append( arregloDirecciones[42][1] ).append( "'\");\n" ).append( "	 fw_menu_3_4_2.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_4_5 = new Menu(\"Consultas\",150,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_4_5.addMenuItem(\"Cheques/Cancelaci&oacute;n\",\"location='" ).append( arregloDirecciones[39][1] + "'\");\n" ).append( "	fw_menu_3_4_5.addMenuItem(\"Archivos\",\"location='" ).append( arregloDirecciones[101][1] ).append( "'\");\n" ).append( "	 fw_menu_3_4_5.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_4_3 = new Menu(\"Mantenimiento de Cuentas\",48,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_4_3.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[43][1] ).append( "'\");\n" ).append( "	fw_menu_3_4_3.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[44][1] ).append( "'\");\n" ).append( "	 fw_menu_3_4_3.hideOnMouseOut=true;\n")
			.append( "    window.fw_menu_3_4 = new Menu(\"Chequera Seguridad\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_3_4.addMenuItem(fw_menu_3_4_1);\n" )
			//"	fw_menu_3_4.addMenuItem(\"Consulta de Cheques\",\"location='"+arregloDirecciones[39][1]+"'\");\n"+
			//"	fw_menu_3_4.addMenuItem(fw_menu_3_4_2);\n"+
			.append("    fw_menu_3_4.addMenuItem(fw_menu_3_4_5);\n" ).append( "    fw_menu_3_4.addMenuItem(fw_menu_3_4_3);\n" ).append( "	  fw_menu_3_4.hideOnMouseOut=true;\n" + "     fw_menu_3_4.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )
			/*"	  window.fw_menu_3_4_1 = new Menu(\"Registro de Cheques\",85,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"	fw_menu_3_4_1.addMenuItem(\"En L&iacute;nea\",\"location='"+arregloDirecciones[37][1]+"'\");\n"+
			"	fw_menu_3_4_1.addMenuItem(\"Por Archivo\",\"location='"+arregloDirecciones[38][1]+"'\");\n"+
			"	 fw_menu_3_4_1.hideOnMouseOut=true;\n"+
			"	window.fw_menu_3_4_2 = new Menu(\"Mantenimiento de Beneficiarios\",91,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"	fw_menu_3_4_2.addMenuItem(\"Alta\",\"location='"+arregloDirecciones[40][1]+"'\");\n"+
			"	fw_menu_3_4_2.addMenuItem(\"Baja\",\"location='"+arregloDirecciones[41][1]+"'\");\n"+
			"	fw_menu_3_4_2.addMenuItem(\"Modificaci&oacute;n\",\"location='"+arregloDirecciones[42][1]+"'\");\n"+
			"	 fw_menu_3_4_2.hideOnMouseOut=true;\n"+
			"	window.fw_menu_3_4_3 = new Menu(\"Mantenimiento de Cuentas\",48,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"	fw_menu_3_4_3.addMenuItem(\"Alta\",\"location='"+arregloDirecciones[43][1]+"'\");\n"+
			"	fw_menu_3_4_3.addMenuItem(\"Baja\",\"location='"+arregloDirecciones[44][1]+"'\");\n"+
			"	 fw_menu_3_4_3.hideOnMouseOut=true;\n"+
			"    window.fw_menu_3_4 = new Menu(\"Chequera Seguridad\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+
			"    fw_menu_3_4.addMenuItem(fw_menu_3_4_1);\n"+
			"    fw_menu_3_4.addMenuItem(\"Consulta de Cheques\",\"location='"+arregloDirecciones[39][1]+"'\");\n"+
			"    fw_menu_3_4.addMenuItem(fw_menu_3_4_2);\n"+
			"    fw_menu_3_4.addMenuItem(fw_menu_3_4_3);\n"+
			"     fw_menu_3_4.hideOnMouseOut=true;\n"+
			"     fw_menu_3_4.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n"+*/
			.append("	window.fw_menu_3_5_1 = new Menu(\"Registro de Pagos\",85,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_5_1.addMenuItem(\"En L&iacute;nea\",\"location='" ).append( arregloDirecciones[73][1] ).append( "'\");\n" + " fw_menu_3_5_1.addMenuItem(\"Por Archivo\",\"location='" ).append( arregloDirecciones[74][1] ).append( "'\");\n" ).append( "	 fw_menu_3_5_1.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_5_2 = new Menu(\"Mantenimiento de Cuentas\",48,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_5_2.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[77][1] ).append( "'\");\n" ).append( "	fw_menu_3_5_2.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[78][1] ).append( "'\");\n" ).append( "	 fw_menu_3_5_2.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_5_3 = new Menu(\"Mantenimiento de Beneficiarios\",91,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_3_5_3.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[79][1] ).append( "'\");\n" ).append( "	fw_menu_3_5_3.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[80][1] ).append( "'\");\n" ).append( "	fw_menu_3_5_3.addMenuItem(\"Modificaci&oacute;n\",\"location='" ).append( arregloDirecciones[81][1] ).append( "'\");\n" ).append( "	 fw_menu_3_5_3.hideOnMouseOut=true;\n" ).append("    window.fw_menu_3_5 = new Menu(\"Pago Directo\",215,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_3_5.addMenuItem(fw_menu_3_5_1);\n" ).append( "    fw_menu_3_5.addMenuItem(\"Consulta y Modificaci&oacute;n de Pagos\",\"location='" ).append( arregloDirecciones[75][1] ).append( "'\");\n" )
			//Esta opcion de Confirming todavia no aplica para EnlaceInternet
			/*"	fw_menu_3_5.addMenuItem(\"Modificaci&oacute;n de Pagos por Archivo\",\"location='"+arregloDirecciones[76][1]+"'\");\n"+ */
			.append("    fw_menu_3_5.addMenuItem(fw_menu_3_5_2);\n" ).append( "    fw_menu_3_5.addMenuItem(fw_menu_3_5_3);\n" ).append( "	 fw_menu_3_5.addMenuItem(\"Cat&aacute;logo de Sucursales\",\"location='" ).append( arregloDirecciones[82][1] ).append( "'\");\n" ).append( "	 fw_menu_3_5.hideOnMouseOut=true;\n" ).append( "     fw_menu_3_5.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( " window.fw_menu_3_6_1 = new Menu(\"Mantenimiento a Proveedores\",150,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_6_1.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[87][1] ).append( "'\");\n" ).append( "	fw_menu_3_6_1.addMenuItem(\"Consulta y Modificaci&oacute;n\",\"location='" ).append( arregloDirecciones[88][1] ).append( "'\");\n" )
			//Esta opcion de Confirming todavia no aplica para EnlaceInternet
			/*"	  fw_menu_3_6_1.addMenuItem(\"Cat&aacute;logo\",\"location='"+arregloDirecciones[89][1]+"'\");\n"+*/
			.append("	 fw_menu_3_6_1.hideOnMouseOut=true;\n" ).append( "	window.fw_menu_3_6_2 = new Menu(\"Estad&iacute;sticas\",65,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "	fw_menu_3_6_2.addMenuItem(\"Reportes\",\"location='" ).append( arregloDirecciones[93][1] ).append( "'\");\n" ).append( "	fw_menu_3_6_2.addMenuItem(\"Gr&aacute;ficas\",\"location='" ).append( arregloDirecciones[94][1] ).append( "'\");\n" ).append( "  fw_menu_3_6_2.hideOnMouseOut=true;\n" ).append( "    window.fw_menu_3_6 = new Menu(\"Confirming Pago a proveedores\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_3_6.addMenuItem(fw_menu_3_6_1);\n" ).append( "	fw_menu_3_6.addMenuItem(\"Pagos\",\"location='" ).append( arregloDirecciones[90][1] ).append( "'\");\n" )
			//Esta opcion de Confirming todavia no aplica para EnlaceInternet
			/*"	fw_menu_3_6.addMenuItem(\"Pagos y Mantenimiento a Proveedores\",\"location='"+arregloDirecciones[91][1]+"'\");\n"+*/
			.append("    fw_menu_3_6.addMenuItem(\"Consulta y Cancelaci&oacute;n de Pagos\",\"location='" ).append( arregloDirecciones[92][1] ).append( "'\");\n" ).append( "    fw_menu_3_6.addMenuItem(fw_menu_3_6_2);\n" )
			//Esta opcion de Confirming todavia no aplica para EnlaceInternet
			/*"	fw_menu_3_6.addMenuItem(\"Cancelaci&oacute;n de Archivos\",\"location='"+arregloDirecciones[95][1]+"'\");\n"+*/
			.append("     fw_menu_3_6.hideOnMouseOut=true;\n" ).append( "	  fw_menu_3_6.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")

			//Pago de SAR. Se a?dio la opcin pago de SAR, getronics JPPM 15/11/04
			.append("	window.fw_menu_3_8 = new Menu(\"SAR\",199,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n"+ "	fw_menu_3_8.addMenuItem(\"Pagos\",\"location='").append(arregloDirecciones[115][1]).append("'\");\n")
			.append("	fw_menu_3_8.addMenuItem(\"Consulta y Cancelaci&oacute;n de Pagos\",\"location='").append(arregloDirecciones[117][1]).append("'\");\n" ).append( " fw_menu_3_8.hideOnMouseOut=true;\n" ).append( " fw_menu_3_8.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
			.append( " window.fw_menu_3_14 = new Menu(\"S.U.A. L&iacute;nea de Captura\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append(" fw_menu_3_14.addMenuItem(\"Pagos\",\"location='").append( arregloDirecciones[142][1] ).append( "'\");\n")//--cambio menu SIPARE
				.append( " fw_menu_3_14.addMenuItem(\"Consulta\",\"location='").append( arregloDirecciones[143][1] ).append( "'\");\n")//--cambio menu SIPARE
				.append( " fw_menu_3_14.hideOnMouseOut=true;\n")
				.append( " fw_menu_3_14.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")


				/*P020101  Cuenta Nivel 3 - INICIA MENU CUENTA INMEDIATA*/
				.append(" window.fw_menu_3_13_1 = new Menu(\"Remesas\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")

				.append( " fw_menu_3_13_1.addMenuItem(\"Administraci&oacute;n\",\"location='")
				.append( arregloDirecciones[157][1])
				.append( "'\");\n")
				.append( " fw_menu_3_13_1.addMenuItem(\"Consulta\",\"location='")
				.append( arregloDirecciones[158][1])
				.append( "'\");\n")


				.append(" window.fw_menu_3_13_2 = new Menu(\"Asignaci&oacute;n tarjeta-empleado\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")

				.append( " fw_menu_3_13_2.addMenuItem(\"Asignaci&oacute;n Individual\",\"location='")
				.append( arregloDirecciones[159][1])
				.append( "'\");\n")
				.append( " fw_menu_3_13_2.addMenuItem(\"Importaci&oacute;n env&iacute;o\",\"location='")
				.append( arregloDirecciones[160][1])
				.append( "'\");\n")
				.append( " fw_menu_3_13_2.addMenuItem(\"Consulta\",\"location='")
				.append( arregloDirecciones[161][1])
				.append( "'\");\n")


				.append(" window.fw_menu_3_13 = new Menu(\"Cuenta Inmediata\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
				.append( " fw_menu_3_13.addMenuItem(fw_menu_3_13_1);\n")
				.append( " fw_menu_3_13.addMenuItem(fw_menu_3_13_2);\n")
				.append( " fw_menu_3_13.hideOnMouseOut=true;\n")
				.append( " fw_menu_3_13.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
				.append( " window.fw_menu_3_14 = new Menu(\"S.U.A. L&iacute;nea de Captura\",190,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
				.append( " fw_menu_3_14.addMenuItem(\"Pagos\",\"location='").append( arregloDirecciones[142][1] ).append( "'\");\n")//--cambio menu SIPARE
				.append( " fw_menu_3_14.addMenuItem(\"Consulta\",\"location='").append( arregloDirecciones[143][1] ).append( "'\");\n")//--cambio menu SIPARE
				.append( " fw_menu_3_14.hideOnMouseOut=true;\n")
				.append( " fw_menu_3_14.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")

				/*P020101  Cuenta Nivel 3 - FIN MENU CUENTA INMEDIATA*/

			/****************************************************/
			/* TESOFE */
			/****************************************************/
			.append(strTESOFEMenu
			/****************************************************/
			// JGAL
			).append("  window.fw_menu_3 = new Menu(\"root\",248,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");

			/*Stefanini - Se Oculta el menu epyme Servicios > Impuestos Federales*/
			brEpyme=new StringBuffer(nuevomenu);
			if(!esEpyme){
				brEpyme.append("  fw_menu_3.addMenuItem(fw_menu_3_1);\n");

			}
			nuevomenu = new StringBuffer(brEpyme);
			brEpyme = null;


			nuevomenu.append( "  fw_menu_3.addMenuItem(fw_menu_3_7);\n" ).append( "  fw_menu_3.addMenuItem(fw_menu_3_2);\n");
			try {
				Class.forName("mx.altec.enlace.servlets.SUALineaCapturaServlet");
				if ("SI".equals(Global.SUALC_VER_SIPARE)) { // Condiciona Opcion SIPARE a indicador en EI.cfg
					nuevomenu.append(" fw_menu_3.addMenuItem(fw_menu_3_14);\n");
				}
			} catch(ClassNotFoundException e) {}

			nuevomenu

			//.append( "  fw_menu_3.addMenuItem(fw_menu_3_3);\n")


			/*Vector - Nuevo Menu Nomina Epyme*/
			.append("  fw_menu_3.addMenuItem(fw_menu_3_99);\n");

			/*Se oculta del menu Epyme  Servicios > Nomina > Interbancaria */

			if(!esEpyme){
				nuevomenu.append( "  fw_menu_3.addMenuItem(fw_menu_3_3_1_3);\n");//CatNomInterb
			}


			try {
				Class.forName("mx.altec.enlace.servlets.NomPreBusqueda");
				if ("SI".equals(Global.NOMPRE_VERMENU) && !esEpyme ) { // Condiciona Opcion N�mina Prepago a indicador en EI.cfg
					/*Stefanini - Se oculta del menu Epyme  Servicios > Tarjeta de Pagos Santander */
						nuevomenu.append( "  fw_menu_3.addMenuItem(fw_menu_3_3_3);\n");

				}
			} catch(ClassNotFoundException e) {}


			/*P020101  Cuenta Nivel 3 - INICIA MENU CUENTA INMEDIATA*/
			if ("SI".equals(Global.N3_VER_MENU)) {
					nuevomenu.append( " fw_menu_3.addMenuItem(fw_menu_3_13);\n");

			}		//--  comentado por no estar en Global.java

			/*P020101  Cuenta Nivel 3 - FIN MENU CUENTA INMEDIATA*/
			/*Stefanini - Se oculta del menu Epyme  Servicios > Confirming */
			brEpyme=new StringBuffer(nuevomenu);
			if(!esEpyme){

				brEpyme.append("  fw_menu_3.addMenuItem(fw_menu_3_4);\n");
				brEpyme.append("  fw_menu_3.addMenuItem(fw_menu_3_5);\n");
				brEpyme.append("  fw_menu_3.addMenuItem(fw_menu_3_6);\n");  // A?dir menu confirming 20030428
				brEpyme.append("  fw_menu_3.addMenuItem(fw_menu_3_8);\n"); // Opcin SAR, getronics 15/11/04

			}
			nuevomenu = new StringBuffer( brEpyme );
			brEpyme = null;

			/****************************************************/
			/* TESOFE */
			/****************************************************/
			nuevomenu.append( " fw_menu_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n"  ).append(  strTESOFEOpcion )
			/****************************************************/

			//MENU ADMINISTRACION Y CONTROL
			.append("    window.fw_menu_4_1 = new Menu(\"Mancomunidad\",265,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" ).append( "    fw_menu_4_1.addMenuItem(\"Consulta y Autorizaci&oacute;n\",\"location='" ).append( arregloDirecciones[45][1] ).append( "'\");\n" ).append( "    fw_menu_4_1.addMenuItem(\"Generaci&oacute;n de Folios\",\"location='" ).append( arregloDirecciones[46][1] ).append( "'\");\n" ).append( "	 fw_menu_4_1.addMenuItem(\"Operaciones Internacionales\",\"location='" ).append( arregloDirecciones[113][1] ).append( "'\");\n");
			// Condiciona Opcion Consulta y Autorizacion Archivos y Servicios a indicador en EI.cfg

			try {
				Class.forName("mx.altec.enlace.servlets.AMancomunidadSA");
				if ("SI".equals(Global.MFI_VER_MENU)) {
					nuevomenu.append(" fw_menu_4_1.addMenuItem(\"Consulta y Autorizaci&oacute;n Archivos y Servicios\",\"location='" ).append( arregloDirecciones[149][1] ).append( "'\");\n");
				}
			} catch(ClassNotFoundException e) {
				EIGlobal.mensajePorTrace("No se encontro la clase para Mancomunidad FI", EIGlobal.NivelLog.INFO);
			}
			/**
			 * Menu Cuenta Nivel 1 Fase II
			 */
			try {
				Class.forName("mx.altec.enlace.servlets.EnlcAltaTarjeta");
	            Class.forName("mx.altec.enlace.servlets.EnlcConsultaTarjeta");
	            Class.forName("mx.altec.enlace.servlets.EnlcBajaTarjeta");
	            if ("SI".equals(Global.CN1_VER_MENU)) {
	               nuevomenu.append(" fw_menu_3_3_3.addMenuItem(fw_menu_3_3_3_4);\n");
	            }
	         } catch(ClassNotFoundException e) {
	            EIGlobal.mensajePorTrace("No se encontraron las clases para Vinculacion de Tarjetas", EIGlobal.NivelLog.INFO);
	         }
			nuevomenu.append("	fw_menu_4_1.hideOnMouseOut=true;\n")

			/***************************************************
			* Modificado por: Rafael M. Rosiles Soto
			* Fecha:	  15 de Enero de 2004
			* Descripcin:	  Menu para consulta, actualizacin y alta de cuentas CLABE
			*/
			//MSD se elimina men?
			//"   window.fw_menu_4_3 = new Menu(\"Actualizaci&oacute;n de cuentas CLABE\",215,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "   fw_menu_4_3.addMenuItem(\"Consulta y Actualizaci&oacute;n CLABE\",\"location='" + arregloDirecciones[111][1] + "'\");\n" + "   fw_menu_4_3.addMenuItem(\"Alta de cuentas CLABE\",\"location='" + arregloDirecciones[112][1] + "'\");\n" + "	 fw_menu_4_3.hideOnMouseOut=true;\n" +
			// fin de modificacion
			// ***************************************************
			.append("    window.fw_menu_4_2 = new Menu(\"Cuentas\",200,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append("    fw_menu_4_2.addMenuItem(\"Consulta\",\"location='" ).append( arregloDirecciones[50][1] ).append( "'\");\n" )
			.append("	 fw_menu_4_2.addMenuItem(\"Alta\",\"location='" ).append( arregloDirecciones[51][1] ).append( "'\");\n" )
			.append("    fw_menu_4_2.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[52][1] ).append( "'\");\n")
			/*******************************************************/
			/* NUEVO MODULO ALTA DE CUENTAS                        */
			/*******************************************************/
			.append("    fw_menu_4_2.addMenuItem(\"Autorizaci&oacute;n y Cancelaci&oacute;n\",\"location='" ).append( arregloDirecciones[119][1] ).append( "'\");\n" )


            // Nuevo modulo FIEL*/

			.append("    window.fw_menu_4_6 = new Menu(\"Fiel\",200,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
			.append("    fw_menu_4_6.addMenuItem(\"Registro\",\"location='" ).append( arregloDirecciones[182][1] ).append( "'\");\n" )
			.append("	 fw_menu_4_6.addMenuItem(\"Modificacion\",\"location='" ).append( arregloDirecciones[183][1] ).append( "'\");\n" )
			.append("    fw_menu_4_6.addMenuItem(\"Baja\",\"location='" ).append( arregloDirecciones[184][1] ).append( "'\");\n")
            .append("    fw_menu_4_6.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
            .append("    fw_menu_4_6.hideOnMouseOut=true;\n")

			/***************************************************
			* Modificado por: Rafael M. Rosiles Soto
			* Fecha:	  15 de Enero de 2004
			* Descripcin:	  Menu para consulta, actualizacin y alta de cuentas CLABE
			*/
			//MSD se elimina men?
			//"   fw_menu_4_2.addMenuItem(fw_menu_4_3);\n" +
			// fin de modificacion
			// ***************************************************
			.append("   fw_menu_4_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
			//Se elimino porque se reestructuro el menu
			/*"  fw_menu_4_2.addMenuItem(\"Internacionales\",\"location='"+arregloDirecciones[59][1]+"'\");\n"+*/
			.append("     fw_menu_4_2.hideOnMouseOut=true;\n")

			.append(" window.fw_menu_4_3_1 = new Menu(\"Administraci&oacute;n de Usuarios\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n")
			.append( "	fw_menu_4_3_1.addMenuItem(\"Alta de Usuarios\",\"location='" ).append( arregloDirecciones[144][1] ).append( "'\");\n")
			.append("	fw_menu_4_3_1.addMenuItem(\"Baja de Usuarios\",\"location='" ).append( arregloDirecciones[145][1] ).append( "'\");\n")
			.append( "	fw_menu_4_3_1.addMenuItem(\"Modificaci&oacute;n de Usuarios\",\"location='" ).append( arregloDirecciones[146][1] ).append( "'\");\n")
			.append( " fw_menu_4_3_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n")
			.append( "	fw_menu_4_3_1.hideOnMouseOut=true;\n")

			.append( " window.fw_menu_4_3 = new Menu(\"Administraci&oacute;n Super Usuario\",250,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");

				//Condiciona a una constante si se muestra del menu de Limites y Montos (EI.cfg)
				try {
					Class.forName("mx.altec.enlace.servlets.AdmonUsuarioServlet");

					//Condiciona a una constante si se muestra del menu de Administraci�n de Usuarios (EI.cfg)
					if ("SI".equals(Global.ADMONUSR_VER_MENU)) {
						nuevomenu.append( "    fw_menu_4_3.addMenuItem(fw_menu_4_3_1);\n");
					}

					//Condiciona a una constante si se muestra del menu de Limites y Montos (EI.cfg)
					if ("SI".equals(Global.ADMONLYM_VER_MENU)) {
						nuevomenu.append( "    fw_menu_4_3.addMenuItem(\"Establecimiento de L&iacute;mites y Montos\",\"location='" ).append( arregloDirecciones[147][1] ).append( "'\");\n");
					}
					//Condiciona a una constante si se muestra del menu de Configuracion de Medios de Notificacion (EI.cfg)
					if ("SI".equals(Global.MEDNOT_VER_MENU)) {
						nuevomenu.append( "    fw_menu_4_3.addMenuItem(\"Configuraci&oacute;n de Medios de Notificaci&oacute;n\",\"location='" ).append( arregloDirecciones[148][1] ).append( "'\");\n");
					}

				} catch(ClassNotFoundException e) {}
					 nuevomenu.append( "	fw_menu_4_3.hideOnMouseOut=true;\n")
						.append( " fw_menu_4_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )

						//Nuevo menu mantenimiento RSA Stefanini

						.append("    window.fw_menu_4_5 = new Menu(\"Control de Acceso\",200,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" )
						.append("    fw_menu_4_5.addMenuItem(\"Mantenimiento Imagen\",\"location='" ).append( arregloDirecciones[167][1] ).append( "'\");\n" )
						.append("	 fw_menu_4_5.addMenuItem(\"Mantenimiento Pregunta Secreta\",\"location='" ).append( arregloDirecciones[168][1] ).append( "'\");\n" )
						.append("    fw_menu_4_5.addMenuItem(\"Desvinculaci�n de Dispositivos\",\"location='" ).append( arregloDirecciones[166][1] ).append( "'\");\n" )
						.append(" 	 fw_menu_4_5.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" )
						.append("	 fw_menu_4_5.hideOnMouseOut=true;\n");

						//FIN Nuevo menu mantenimiento RSA Stefanini


					/*
					 * Menu EdoCta PDF XML
					 */
					try {
						EIGlobal.mensajePorTrace("BaseServlet --> Menu Estados de Cuenta ", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("BaseServlet --> Menu Estados de Cuenta  ", EIGlobal.NivelLog.ERROR);
						sbNuevoMenu.append(nuevomenu);

						sbNuevoMenu.append(" window.fw_menu_4_4_1_1 = new Menu(\"Masivo\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_1_1.addMenuItem(\"Cuenta\",\"location='").append(arregloDirecciones[171][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_1_1.addMenuItem(\"Tarjeta de cr&eacute;dito\",\"location='").append(arregloDirecciones[178][1]).append("'\");\n");

						sbNuevoMenu.append("	fw_menu_4_3.hideOnMouseOut=true;\n");
						sbNuevoMenu.append(" fw_menu_4_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");

						sbNuevoMenu.append(" window.fw_menu_4_4_1 = new Menu(\"Modificar env&iacute;o de Estado de Cuenta\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_1.addMenuItem(\"Individual\",\"location='").append(arregloDirecciones[169][1]).append("'\");\n");

						try {
							Class.forName("mx.altec.enlace.servlets.ConfigMasPorTCServlet");
							sbNuevoMenu.append(" fw_menu_4_4_1.addMenuItem(fw_menu_4_4_1_1);\n");
						} catch (ClassNotFoundException e) {
							sbNuevoMenu.append(" fw_menu_4_4_1.addMenuItem(\"Masivo\",\"location='").append(arregloDirecciones[171][1]).append("'\");\n");
						}


						sbNuevoMenu.append(" fw_menu_4_4_1.addMenuItem(\"Consulta\",\"location='").append(arregloDirecciones[172][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_1.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");

						sbNuevoMenu.append(" window.fw_menu_4_4_2 = new Menu(\"Descarga de Estado de Cuenta\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_2.addMenuItem(\"PDF\",\"location='").append(arregloDirecciones[174][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_2.addMenuItem(\"XML\",\"location='").append(arregloDirecciones[175][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_2.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");

						sbNuevoMenu.append(" window.fw_menu_4_4_3 = new Menu(\"Periodos Hist&oacute;ricos Formato PDF\",170,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_3.addMenuItem(\"Solicitud\",\"location='").append(arregloDirecciones[173][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_3.addMenuItem(\"Consulta\",\"location='").append(arregloDirecciones[170][1]).append("'\");\n");
						sbNuevoMenu.append(" fw_menu_4_4_3.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");

						sbNuevoMenu.append(" window.fw_menu_4_4 = new Menu(\"Estados de Cuenta\",250,17,\"Verdana, Arial, Helvetica, sans- serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n");
						sbNuevoMenu.append(" fw_menu_4_4.addMenuItem(fw_menu_4_4_1);\n");
						sbNuevoMenu.append(" fw_menu_4_4.addMenuItem(fw_menu_4_4_2);\n");
						sbNuevoMenu.append(" fw_menu_4_4.addMenuItem(fw_menu_4_4_3);\n");
						sbNuevoMenu.append(" fw_menu_4_4.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n");
						sbNuevoMenu.append(" fw_menu_4_4.hideOnMouseOut=true;\n");

						sbNuevoMenu.append("  window.fw_menu_4 = new Menu(\"root\",200,17,\"Verdana, Arial, Helvetica, sans-serif\",10,\"#000000\",\"#ffffff\",\"#e6e5e1\",\"#3c4780\");\n" + "  fw_menu_4.addMenuItem(fw_menu_4_1);\n");
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(\"Cambio de Contrato\",\"location='").append(arregloDirecciones[48][1]).append("'\");\n");
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(\"Cambio de Contrase&ntilde;a\",\"location='").append(arregloDirecciones[49][1]).append("'\");\n");
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(fw_menu_4_2);\n");
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(fw_menu_4_6);\n");
						sbNuevoMenu.append("	fw_menu_4.addMenuItem(\"Usuarios\",\"location='").append(arregloDirecciones[53][1]).append("'\");\n");
						/*Stefanini - Se oculta Menu Administracion y control -> Generacion Manual de Archivos*/
						if(!esEpyme){
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(\"Generaci&oacute;n manual de archivos\",\"location='").append(arregloDirecciones[102][1]).append("'\");\n");
						}


						sbNuevoMenu.append("  fw_menu_4.addMenuItem(\"Mant. de Datos Personales\",\"location='").append(arregloDirecciones[108][1]).append("'\");\n");
						sbNuevoMenu.append("  fw_menu_4.addMenuItem(fw_menu_4_5);\n");

						nuevomenu = new StringBuffer(sbNuevoMenu);
						sbNuevoMenu.delete(0, sbNuevoMenu.length());
				} catch (ArrayIndexOutOfBoundsException e) {
					EIGlobal.mensajePorTrace("Error al generar menu de Estados de Cuenta PDF/XML: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
					/*
					 * Fin Menu EdoCta PDF XML
					 */


			//Condiciona a que si tiene cualquiera de las 6 facultades de superusuario muestra el menu completo

				if ( verificaFacultad ( "ALTAVINUSRSUP" ,request) ||
				     verificaFacultad ( "BAJAVINUSRSUP" ,request) ||
				     verificaFacultad ( "MODFACUSRSUP" ,request) ||
				     verificaFacultad ( "MODLMUSSUP" ,request) ||
				     verificaFacultad ( "MODLMCTOSUP" ,request) ||
				     verificaFacultad ( "CAMMEDNOTSUP" ,request)) {
						nuevomenu.append( "  fw_menu_4.addMenuItem(fw_menu_4_3)\n");
				}
			sbNuevoMenu.append(nuevomenu);
			sbNuevoMenu.append("  fw_menu_4.addMenuItem(fw_menu_4_4)\n");
			nuevomenu = new StringBuffer(sbNuevoMenu);

			//stefanini cifrado 05/11/2013
	  		final StringBuffer strbuf=new StringBuffer(nuevomenu);//validar facultad
	  		if (verificaFacultad ( "IMPARCHCIFRADO" ,request) && "SI".equals(Global.archCifVerMenu)) {
	  			if(!esEpyme){
					strbuf.append("  fw_menu_4.addMenuItem(\"Certificado digital\",\"location='");
					strbuf.append(arregloDirecciones[176][1]);
					strbuf.append("'\");\n");
	  		    }
	  		}
	  		nuevomenu = new StringBuffer(strbuf);
	  		//fin stefanini cifrado 05/11/2013
			// Termina armado del menu
			nuevomenu.append( "   fw_menu_4.hideOnMouseOut=true;\n" ).append( "   fw_menu_4.childMenuIcon=\"/gifs/EnlaceMig/gic25010.gif\";\n" ).append( "  fw_menu_4.writeMenus();\n" ).append( "} // fwLoadMenus()\n" + "//-->\n" ).append( "<!-- Final del Javascript de los menus de cascada -->") /*)*/;
		return nuevomenu.toString();
	}//acaba creaFuncionMenu(String[][])

/********************************************/
/** Metodo: determinaTemplate
 * @return String
 * @param int -> opcionElegida
 */
/********************************************/
     public String determinaTemplate ( int opcionElegida ) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "entrando al metodo determinaTemplate()",EIGlobal.NivelLog.DEBUG);
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "numero de opcion elegida : >" +opcionElegida+ "<",EIGlobal.NivelLog.DEBUG);
	 String[] templatesDefault = { IEnlace.PRINCIPAL,
	 "document.location='MSC_Inicio?"+BitaConstants.FLUJO+"="+BitaConstants.EC_SALDO_CONS_CHEQUE+"'",
	 "document.location=\'MCL_Credito?Cuenta=5&"+BitaConstants.FLUJO+"="+BitaConstants.EC_SALDO_CUENTA_TARJETA+"\'",
	 "document.location=\'csaldo1?prog=0&"+BitaConstants.FLUJO+"="+BitaConstants.EC_SALDO_CUENTA_CHEQUE +"\'",
	 "document.location=\'CMovimiento?prog=0&"+BitaConstants.FLUJO+"="+BitaConstants.EC_MOV_CHEQ_LINEA+"\'",
	 "document.location=\'AMancomunidad?"+BitaConstants.FLUJO+"="+BitaConstants.EA_MANCOM_CONS_AUTO+"\'",
	 "document.location=\'http://www.bsch-infocash.com\'",
	 "document.location=\'transferencia?ventana=0&"+BitaConstants.FLUJO+"="+BitaConstants.ET_CUENTAS_MISMO_BANCO_MN_LINEA+"\'",
	 "document.location=\'MDI_Interbancario?Modulo=0&"+BitaConstants.FLUJO+"="+BitaConstants.ET_INTERBANCARIAS + "\'",
	 //"document.location=\'PagosImpuestos?OPCION=0\'",
	 "document.location=\'CambioContrato?Modulo=1&"+BitaConstants.FLUJO+"="+BitaConstants.ES_PAGO_LINEA_CAPTURA+"\'",
	 "document.location=\'InicioNomina?Modulo=0&"+BitaConstants.FLUJO+"="+BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO+"\'",
	 "document.location=\'MigracionOTP?accion=menu\'",
	 "document.location=\'EnrolamientoRSAServlet\'"};

	 return templatesDefault[opcionElegida];

     }

/********************************************/
/** Metodo: obtenerAvisos
 * @return String
 * @param HttpServletRequest -> request
 */
/********************************************/
     public String obtenerAvisos (HttpServletRequest request) //cambio
     {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 EIGlobal.mensajePorTrace ( "***CemePrincipal.class & Entrando a obtenerAvisos() &", EIGlobal.NivelLog.DEBUG);
	 String contrato    = session.getContractNumber ();
	 String usuario     = session.getUserID8 ();
	 String clavePerfil = session.getUserProfile ();
	 short	sucOpera    = Short.parseShort (IEnlace.SUCURSAL_OPERANTE);
	 ServicioTux tuxGlobal;
	 int salida=0;
	 String encabezado	    = "";
	 String tramaEntrada	    = "";
	 String tramaSalida	    = "";
	 String path_archivo	    = "";
	 String nombre_archivo	    = "";
	 String trama_salidaRedSrvr = "";
	 String retCodeRedSrvr	    = "";
	 String registro	    = "";
	 String strMensaje	    = "";
	 String strTabla	    = "";
	 String strAvisos	    = "";
	 String strClase	    = "";
	 String medio_entrega  = "2EWEB";
	 String tipo_operacion = "AVIS";
	 String ultimoMensaje = "0";
	 encabezado    = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
	 encabezado   += usuario + "|" + clavePerfil + "|";
	 tramaEntrada += ultimoMensaje;
	 tramaEntrada  = encabezado + tramaEntrada;
	 EIGlobal.mensajePorTrace ( "***BaseServlet.class >> tramaEntrada AVIS : >" +tramaEntrada+ "<", EIGlobal.NivelLog.DEBUG);
	 try {
	     tuxGlobal = new ServicioTux ();
	     //tuxGlobal.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	     Hashtable hs = tuxGlobal.web_red ( tramaEntrada );
	     tramaSalida = (String) hs.get ("BUFFER");
	 } catch(java.rmi.RemoteException re) {
                  EIGlobal.mensajePorTrace ("E->obtenerAvisos->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
		 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
	 } catch (Exception e) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 EIGlobal.mensajePorTrace ( "***BaseServlet.class >> tramaSalida AVIS : >" +tramaSalida+ "<", EIGlobal.NivelLog.DEBUG);
	 path_archivo = tramaSalida;
	 nombre_archivo = path_archivo.substring (path_archivo.lastIndexOf ('/')+1 , path_archivo.length ());
	 EIGlobal.mensajePorTrace ( "***BaseServlet.class & nombre_archivo AVIS : >" +nombre_archivo+ "<", EIGlobal.NivelLog.DEBUG);
	 // Copia Remota
	 ArchivoRemoto archR = new ArchivoRemoto ();
	 if(!archR.copia (nombre_archivo)) {
	     EIGlobal.mensajePorTrace ( "***BaseServlet.class & obtenerAvisos: No se realizo la copia remota. &", EIGlobal.NivelLog.ERROR);
	     strAvisos = "muestraAvisos('No se pudo obtener el archivo de avisos.');";
	 } else {
	     boolean errorFile=false;
			 FileReader lee = null;
	     try {
		 EIGlobal.mensajePorTrace ( "***BaseServlet.class & obtenerAvisos: Copia remota OK. &", EIGlobal.NivelLog.DEBUG);
		 path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;
		 EIGlobal.mensajePorTrace ( "***BaseServlet.class & path_archivo AVIS : >" +path_archivo+ "<", EIGlobal.NivelLog.DEBUG);
		 //File drvFile = new File (path_archivo);
		 //RandomAccessFile tmpFile = new RandomAccessFile (drvFile, "r");
		 BufferedReader tmpFile = null;
				 lee = new FileReader ( path_archivo );
		 tmpFile = new BufferedReader ( lee );
		 trama_salidaRedSrvr = tmpFile.readLine ();
		 EIGlobal.mensajePorTrace ( "***BaseServlet.class & trama_salidaRedSrvr AVIS : >" +trama_salidaRedSrvr+ "<", EIGlobal.NivelLog.INFO);
		 retCodeRedSrvr = trama_salidaRedSrvr.substring (0, 8).trim ();
		 if ("OK".equals(retCodeRedSrvr)) {
		     strTabla  = "<table width=420 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		     while((registro = tmpFile.readLine ()) != null) {
			 EIGlobal.mensajePorTrace ( "***BaseServlet.class & registro AVIS : >" +registro+ "<", EIGlobal.NivelLog.DEBUG);
			 int numAvisos = EIGlobal.CuantosTokens (registro, '|');
			 for(int i=0;i<numAvisos;i++) {
			     if((i%2)==0) {
				 strClase = "textabdatobs";
			     } else {
				 strClase = "textabdatcla";
			     }
			     String aviso = EIGlobal.BuscarToken (registro, '|', i+1);
			     EIGlobal.mensajePorTrace ( "***BaseServlet.class & aviso AVIS : >" +aviso+ "<", EIGlobal.NivelLog.DEBUG);
			     String mensaje = aviso.substring (aviso.indexOf ("@")+1);
			     EIGlobal.mensajePorTrace ( "***BaseServlet.class & mensaje AVIS : >" +mensaje+ "<", EIGlobal.NivelLog.DEBUG);
			     strTabla += "  <tr> ";
			     strTabla += "   <td class=" + strClase + ">" + mensaje + "</td>";
			     strTabla += "  </tr> ";
			 }
		     }
		     strTabla += "</table><br>";
		     strAvisos = "muestraAvisos('" +strTabla+ "');";
		 }
		 tmpFile.close ();
	     } catch(Exception ioeSua) {
		 EIGlobal.mensajePorTrace ( "***BaseServlet.class EX %obtenerAvisos() >> " + ioeSua.getMessage () + " <<", EIGlobal.NivelLog.ERROR);
                 EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
		 strAvisos = "muestraAvisos(\"" + "Error al leer el archivo de avisos." + "\");";
	     } finally {
				 try{
					 lee.close();
				 }catch(Exception e) {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				 }
			 }
	 }
	 EIGlobal.mensajePorTrace ( "***BaseServlet.class & Saliendo de obtenerAvisos() &", EIGlobal.NivelLog.DEBUG);
	 return strAvisos;
     }
      /**<B><I><code></code></I></B> Da los avisos de disposiciones vencidas (HRZ).
       <P>
	 11/12/2002 <BR>
	 13/12/2002 Copia remota.<BR>
       </P>
       @param peticion <B>HttpServletRequest</B>
      */

/********************************************/
/** Metodo: avisosVencimientosCE
 * @return String
 * @param HttpServletRequest -> peticion
 */
/********************************************/
      public String avisosVencimientosCE(HttpServletRequest peticion)
      {
	 HttpSession sesion;
	 BaseResource sesionBS;
	 String vencimientosCE=new String("");
	 String usuario, perfil, contrato, claveBanco;
	 ServicioTux llamoTuxedo;
	 TramasCE armaTrama;
	 InterpretaCEAV avisosCE;
	 EIGlobal enlaceGlobal;
	 Hashtable mapa;
	 String consultaCEAV, respuestaCEAV;
	 sesion=peticion.getSession();
	 sesionBS=(BaseResource)sesion.getAttribute("session");

	 if(sesionBS == null) {
		 return "Sesion expirada.";
	 }

	 enlaceGlobal=new EIGlobal(sesionBS, getServletContext(), this);
	 enlaceGlobal.mensajePorTrace("Se recuper el objeto \"sesion\" para \"avisosVencimientosCE\"", EIGlobal.NivelLog.DEBUG);
	 contrato=sesionBS.getContractNumber();
	 usuario=sesionBS.getUserID8();
	 perfil=sesionBS.getUserProfile();
	 claveBanco=sesionBS.getClaveBanco();
	 if(contrato!=null && usuario!=null && perfil!=null)
	 {
	    armaTrama=new TramasCE();
	    consultaCEAV=armaTrama.consultaAvisosCE(contrato, usuario, perfil);
	    enlaceGlobal.mensajePorTrace("La trama de consulta para tuxedo del servicio CEAV es: "+consultaCEAV, EIGlobal.NivelLog.INFO);
	    try
	    {
	       llamoTuxedo=new ServicioTux();
	       //llamoTuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	       mapa=llamoTuxedo.web_red(consultaCEAV);
	       respuestaCEAV=(String)mapa.get("BUFFER");
	       enlaceGlobal.mensajePorTrace("Respuesta CEAV: "+respuestaCEAV, EIGlobal.NivelLog.INFO);
	       avisosCE=new InterpretaCEAV();
	       avisosCE.setTramaRespuesta(respuestaCEAV);

	       if(avisosCE.interpretaTramaRespuesta())
	       {
		  ArchivoRemoto ar=new ArchivoRemoto();

		  if(ar.copia(usuario))
		  {
		     if(avisosCE.leeArchivo())
		     {
			enlaceGlobal.mensajePorTrace("Si se ley el archivo: ", EIGlobal.NivelLog.DEBUG);
			if(avisosCE.getRenglonesLeidos()>0)
			{
			   vencimientosCE=avisosCE.getDatosEnTabla();
			   enlaceGlobal.mensajePorTrace("Se obtuvieron datos", EIGlobal.NivelLog.DEBUG);
			   enlaceGlobal.mensajePorTrace(vencimientosCE, EIGlobal.NivelLog.DEBUG);
			   // Si todo sale bien, el m?odo regresa lo que contiene vencimientosCE
			}
			else
			{
			   enlaceGlobal.mensajePorTrace("No se ley contenido del archivo", EIGlobal.NivelLog.ERROR);
			   return "";
			} // Fin if-else renglones
		     }
		     else
		     {
			enlaceGlobal.mensajePorTrace("No se pudo leer el archivo generado por el servicio CEAV", EIGlobal.NivelLog.ERROR);
			return "";
		     } // Fin if-else archivo
		  }
		  else
		  {
		     enlaceGlobal.mensajePorTrace("No se pudo hacer copia remota del archivo.", EIGlobal.NivelLog.ERROR);
		     return "";
		  } // Fin if-else copia remota
	       }
	       else
	       {
		  enlaceGlobal.mensajePorTrace("La trama de respuesta de tuxedo del servicio CEAV no es correcta.", EIGlobal.NivelLog.ERROR);
		  return "";
	       } // Fin if-else interpreta trama
	    }
	    catch(java.rmi.RemoteException re)
	    {  EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
	       enlaceGlobal.mensajePorTrace("E al llamar al servicio de tuxedo: "+re.getMessage(), EIGlobal.NivelLog.ERROR);
	    }
	    catch(Exception e)
	    {  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	       enlaceGlobal.mensajePorTrace("E al llamar al servicio de tuxedo: "+e.getMessage(), EIGlobal.NivelLog.ERROR);
	    } // Fin try-catch
	 }
	 else
	 {
	    enlaceGlobal.mensajePorTrace("Los atributos de sesion son nulos", EIGlobal.NivelLog.DEBUG);
	    return "";
	 } // Fin if-else
	 return vencimientosCE;
      } // Fin m?odo avisosVencimientosCE

/********************************************/
/** Metodo: validaDigitoCuenta
 * @return boolean
 * @param String -> cuenta
 */
/********************************************/
     public boolean validaDigitoCuenta ( String cuenta ) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Entrando al metodo validaDigitoCuenta()",EIGlobal.NivelLog.DEBUG);
	 String cuentaCompuesta = "";
	 int longitudCuenta = 11;
	 int ponderado = 0;
	 String ponderacion = "234567234567";
	 int nCta = 0;
	 int digitoPonderacion = 0;
	 boolean cuentaValida = false;
	 cuentaCompuesta = "21" + cuenta.trim ();
	 String count = cuenta.trim ();
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "La longitud de count es : >" +count.length ()+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El ultimo caracter de la cuenta es : >" +count.charAt ( 10 )+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "La cuenta compuesta es : >" +cuentaCompuesta+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Longitud de cuenta compuesta : (menos uno) >" +( cuentaCompuesta.length () - 1 )+ "<",EIGlobal.NivelLog.DEBUG);
	 for ( int i = 0; i < cuentaCompuesta.length () - 1; i++ ) {
	     try {
		 nCta = Integer.parseInt ( String.valueOf ( cuentaCompuesta.charAt ( i ) ) );
		 digitoPonderacion = Integer.parseInt ( String.valueOf ( ponderacion.charAt ( i ) ) );
		 ponderado=ponderado + ( nCta * digitoPonderacion );
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Ponderado : >" +ponderado+ "<",EIGlobal.NivelLog.DEBUG);
	     } catch ( NumberFormatException e ) {
                 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Ha ocurrido un E en la conversion de numeros " + e.toString (),EIGlobal.NivelLog.ERROR);
	    	 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     }
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Valor final de ponderado : >" +ponderado+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El residuo de ponderado es : >" +( ponderado % longitudCuenta )+ "<",EIGlobal.NivelLog.DEBUG);
	 switch( ponderado%longitudCuenta ) {
	     case 0:
		 if ( cuentaCompuesta.endsWith ( "0" ) ) {
		     cuentaValida = true;
		     break;
		 } else {
		     cuentaValida = false;
		     break;
		 }
	     case 1:
		 cuentaValida = false;
		 break;
	     default:
	    	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El residuo no es 0 ni 1",EIGlobal.NivelLog.DEBUG);
	     	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El valor que se comparara es: " + ( longitudCuenta - ( ponderado % longitudCuenta ) ),EIGlobal.NivelLog.DEBUG);
		 if ( ( longitudCuenta - ( ponderado % longitudCuenta ) ) == Integer.parseInt ( String.valueOf ( count.charAt ( 10 ) ) ) ) {
		     cuentaValida = true;
		     break;
		 } else {
		     cuentaValida = false;
		     break;
		 }
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El metodo validaDigitoCuenta() es : >" +cuentaValida+ "<",EIGlobal.NivelLog.INFO);
	 return cuentaValida;
     }//fin del metodo para verificar que el digito verificador de las cuentas sea el correcto
     /////////////////////////////////////////////
     //modificacion para integracion
     /////////////////////////////////////////////

/********************************************/
/** Metodo: Obtener_mensaje_error
* @return String
* @param String -> coderror
*/
/********************************************/
	 private String  Obtener_mensaje_error(String coderror)
	 {
	String mensaje  = "Error de Seguridad";
	String sqlmensaje = "Select msg_error from comu_error where cod_error='" +coderror+ "'";

	Connection conn= null;
	PreparedStatement psmensaje= null;
	ResultSet rsmensaje  = null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement psmensaje = conn.prepareStatement ( sqlmensaje );
	     //ResultSet rsmensaje  = psmensaje.executeQuery ();
	      conn= createiASConn ( Global.DATASOURCE_ORACLE );
	      psmensaje = conn.prepareStatement ( sqlmensaje );
	      rsmensaje  = psmensaje.executeQuery ();

	     rsmensaje.next ();
	     mensaje = rsmensaje.getString ( 1 );
	     //rsmensaje.close ();
	     //psmensaje.close ();
	     //conn.close ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::ObtenMensajeError" + "ERROR: Creando conexion en:",
				 EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("E->obtener_mensaje_eE->" + sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	     mensaje="Error de Seguridad";
	 } catch (Exception e) {
                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E de Seguridad "+e.toString (), EIGlobal.NivelLog.DEBUG);
	     mensaje="Error de Seguridad";
	 }finally{
				try{
					if(rsmensaje!=null){
						rsmensaje.close();
						rsmensaje=null;
					}
					if(psmensaje!=null){
						psmensaje.close();
						psmensaje=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
                                          EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("BaseServlet-Obtener_mensaje_E-> "+e1, EIGlobal.NivelLog.ERROR);
				}
	 }

	 if(mensaje!=null) {
	     mensaje=mensaje.trim ();
	 } else {
	     mensaje="Error de Seguridad";
	 }
	 EIGlobal.mensajePorTrace ( "***Obtener_mensaje_error: >" +mensaje+ "<", EIGlobal.NivelLog.INFO);
	 return mensaje;
	 }

/********************************************/
/** Metodo: validaDigitoCuentaHogan
 * @return boolean
 * @param String -> cuenta
 */
/********************************************/
     public boolean validaDigitoCuentaHogan ( String cuenta ) {
    	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Entrando al metodo validaDigitoCuentaHogan()",EIGlobal.NivelLog.DEBUG);
	 String cuentaCompuesta = "";
	 int longitudCuenta = 11;
	 int ponderado = 0;
	 String ponderacion = "4322765432";
	 int nCta = 0;
	 int digitoPonderacion = 0;
	 boolean cuentaValida = false;
	 String count = cuenta.trim ();
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "La longitud de count es : >" +count.length ()+ "<",
			 EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El ultimo caracter de la cuenta es : >" +count.charAt ( 10 )+ "<",
			 EIGlobal.NivelLog.DEBUG);
	 for ( int i = 0; i < longitudCuenta-1; i++ ) {
	     try {
		 nCta = Integer.parseInt ( String.valueOf ( cuenta.charAt ( i ) ) );
		 digitoPonderacion = Integer.parseInt ( String.valueOf ( ponderacion.charAt ( i ) ) );
		 ponderado+=nCta*digitoPonderacion;
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "ponderado : >" +ponderado+ "<",EIGlobal.NivelLog.DEBUG);
	     } catch( NumberFormatException e ) {
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Ha ocurrido un E en la conversion de numeros " + e.toString (),
	    			 EIGlobal.NivelLog.ERROR);
	    	 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     }
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Valor final de ponderado : >" +ponderado+ "<",EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El residuo de ponderado es : >" +( ponderado % longitudCuenta )+ "<",EIGlobal.NivelLog.DEBUG);
	 switch( ponderado%longitudCuenta ) {
	     case 0:
		 if ( count.endsWith ( "0" ) ) {
		     cuentaValida = true;
		     break;
		 } else {
		     cuentaValida = false;
		     break;
		 }
	     case 1:
		 cuentaValida = false;
		 break;
	     default:
	    	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El residuo no es 0 ni 1",EIGlobal.NivelLog.DEBUG);
	     	EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El valor que se comparara es : >" +( longitudCuenta - ( ponderado % longitudCuenta ) ),
	     			EIGlobal.NivelLog.DEBUG);
		 if ( ( longitudCuenta - ( ponderado % longitudCuenta ) ) == Integer.parseInt ( String.valueOf ( count.charAt ( 10 ) ) ) ) {
		     cuentaValida = true;
		     break;
		 } else {
		     cuentaValida = false;
		     break;
		 }
	 }
	 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "El metodo validaDigitoCuentaHogan() es : >" +cuentaValida+ "<",EIGlobal.NivelLog.INFO);
	 return cuentaValida;
     }//fin del metodo para verificar que el digito verificador de las cuentas sea el correcto

/********************************************/
/** Metodo: ObtenclaveBanco
 * @return void
 * @param HttpServletRequest -> request
 */
/********************************************/
     public void ObtenclaveBanco (HttpServletRequest request) {
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 String  banco = "000";
	 String sqlbanco = "Select banco From tct_cuentas_tele where num_cuenta='" +session.getContractNumber ()+ "'";

	Connection conn= null;
	PreparedStatement psbanco =null;
	ResultSet rsbanco	= null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement psbanco = conn.prepareStatement ( sqlbanco );
	     //ResultSet rsbanco	= psbanco.executeQuery ();
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     psbanco = conn.prepareStatement ( sqlbanco );
	     rsbanco	= psbanco.executeQuery ();

	     if(rsbanco.next()) {
	     	banco = rsbanco.getString(1);
		 }
		 else {
			 banco = null;
		 }

	     //rsbanco.close ();
	     //psbanco.close ();
	     //conn.close ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::ObtenNombreBanco()::" + "E: Creando conexiOn en:",
				 EIGlobal.NivelLog.ERROR);
	    EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("ERROR->ObtenclaveBanco->" + sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	     banco="000";
	 } catch (Exception e) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al obtener banco "+e.toString (),EIGlobal.NivelLog.ERROR);
                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     banco="000";
	 }finally{
				try{
					if(rsbanco!=null){
						rsbanco.close();
						rsbanco=null;
					}
					if(psbanco!=null){
						psbanco.close();
						psbanco=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
					EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
                                        EIGlobal.mensajePorTrace ("BaseServlet-ObtenclaveBanco-> "+e1, EIGlobal.NivelLog.DEBUG);
				}
	 }

	 if(banco!=null) {
	     banco=banco.trim();
	 }
	 else {
	     banco="000";
	 }
	 EIGlobal.mensajePorTrace( "***ObtenNombreBanco : >" +banco+ "<", EIGlobal.NivelLog.DEBUG);
	 session.setClaveBanco(banco);
	 /*sess.removeAttribute ("session");
	 sess.setAttribute ("session",session);*/
     }

/********************************************/
/** Metodo: BuscarCuentaAltair
 * @return String
 * @param String -> cuenta
 */
/********************************************/
     public String BuscarCuentaAltair (String cuenta) {
	 String cta=null;
	 String sqlcta = "Select num_cuenta  From nucl_cuentas where num_cuenta='" +cuenta+ "'";
	Connection conn= null;
	PreparedStatement pscta = null;
	ResultSet rscta  = null;
	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement pscta = conn.prepareStatement ( sqlcta );
	     //ResultSet rscta  = pscta.executeQuery ();
	     conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     pscta = conn.prepareStatement ( sqlcta );
	     rscta  = pscta.executeQuery ();
	     rscta.next();
	     cta = rscta.getString ( 1 );
	     //rscta.close();
	     //pscta.close();
	     //conn.close();
	     if(cta!=null)
		 cta=cta.trim ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::ObtenNombreBanco()::" + "ERROR: Creando conexiOn en:",
				 EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("E->BuscarCuentaAltair->" + sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	     cta=null;
	 } catch (Exception e) {
           EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al obtener cta santander = >" +e.toString ()+ "<",EIGlobal.NivelLog.ERROR);
	     cta=null;
	 }finally{
				try{
					if(rscta!=null){
						rscta.close();
						rscta=null;
					}
					if(pscta!=null){
						pscta.close();
						pscta=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
                                        EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("BaseServlet-ObtenclaveBanco-> "+e1, EIGlobal.NivelLog.DEBUG);
				}
	 }


	 EIGlobal.mensajePorTrace ( "***ObtenEquivalenciaAltair : >" +cta+ "<", EIGlobal.NivelLog.DEBUG);
	 return cta;
     }

/********************************************/
/** Metodo: ObtenEquivalenciaAltair
 * @return String
 * @param String -> cuentaHogan
 */
/********************************************/
     public String  ObtenEquivalenciaAltair (String cuentaHogan) {
	 String cta=null;
	 String sqlcta = "Select num_cuenta From tct_cuentas_serfin where num_cuenta_serfin='" +cuentaHogan+ "'";
	Connection conn= null;
	PreparedStatement pscta = null;
	ResultSet rscta  = null;

	 try {
	     //Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     //PreparedStatement pscta = conn.prepareStatement ( sqlcta );
	     //ResultSet rscta  = pscta.executeQuery ();

	      conn= createiASConn ( Global.DATASOURCE_ORACLE );
	      pscta = conn.prepareStatement ( sqlcta );
	      rscta  = pscta.executeQuery ();


	     rscta.next ();
	     cta = rscta.getString ( 1 );
	     //rscta.close ();
	     //pscta.close ();
	     //conn.close ();
	     if(cta!=null)
		 cta=cta.trim ();
	 } catch ( SQLException sqle ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::ObtenNombreBanco()::" + "E: Creando conexiOn en:",
				 EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("E->ObtenEquivalenciaAltair->" + sqle.getMessage(), EIGlobal.NivelLog.ERROR);
	 }finally{
				try{
					if(rscta!=null){
						rscta.close();
						rscta=null;
					}
					if(pscta!=null){
						pscta.close();
						pscta=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
                                        EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace ("BaseServlet-ObtenEquivalenciaAltair-> "+e1, EIGlobal.NivelLog.DEBUG);
				}
	 }

	 EIGlobal.mensajePorTrace ( "***ObtenEquivalenciaAltair : >" +cta+ "<", EIGlobal.NivelLog.DEBUG);
	 return cta;
     }

/********************************************/
/** Metodo: getnumberlinesCtasInteg
 * @return int
 * @param String -> filename
 */
/********************************************/
     public int getnumberlinesCtasInteg (String filename) {
	 EIGlobal.mensajePorTrace ("***getnumberlinesCtasInteg ", EIGlobal.NivelLog.DEBUG);
	 int j = 0;
	 String linea = null;
		 FileReader lee = null;
	 try{
			 lee= new FileReader (filename);
	     BufferedReader entrada= new BufferedReader (lee);
	     while((linea = entrada.readLine ())!=null) {
		 if(linea.trim ().equals (""))
		     linea = entrada.readLine ();
		 if(linea == null)
		     break;
		 j++;
	     }
	     entrada.close ();
	 } catch(Exception e) {
	     EIGlobal.mensajePorTrace (""+e, EIGlobal.NivelLog.ERROR);
	     EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("E->getnumberlinesCtasInteg->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	     return 0;
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
			 }
		 }
	 EIGlobal.mensajePorTrace ("***leer numero de lineas "+j, EIGlobal.NivelLog.DEBUG);
	 return j;
     }

/********************************************/
/** Metodo: ObteniendoCtasInteg
 * @return String[][]
 * @param int -> inicio
 * @param int -> fin
 * @param String -> archivo
 */
/********************************************/
     public String[][] ObteniendoCtasInteg (int inicio, int fin, String archivo) {
	 EIGlobal.mensajePorTrace ("***Entrando a  ObteniendoCtasInteg ", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***inicio = >" +inicio+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***fin = >" +fin+ "<", EIGlobal.NivelLog.DEBUG);
	 String[][] arrcuentas =null;
	 String datos[] =null;
	 String linea = "";
	 int separadores=0;
	 BufferedReader entrada;
	 int indice=1;
	 int contador=0;
	 arrcuentas = new String[fin+1][9];
	 arrcuentas[0][1] = "NUM_CUENTA";
	 arrcuentas[0][2] = "TIPO_RELAC_CTAS";
	 arrcuentas[0][3] = "TIPO_CUENTA";
	 arrcuentas[0][4] = "NOMBRE_TITULAR";
	 arrcuentas[0][5] = "CUENTA_HOGAN";
	 arrcuentas[0][6] = "FM";
	 arrcuentas[0][7] = "CVE_PRODUCTO_S";
	 arrcuentas[0][8] = "CVE_SUBPRODUCTO_S";
	 FileReader lee = null;
	 try {
			 lee = new FileReader (archivo);
	     entrada = new BufferedReader ( lee );
	     while ( ( linea = entrada.readLine () ) != null ) {
		 separadores=cuentaseparadores (linea);
		 datos = desentramaC (""+separadores+"@"+linea,'@');
		 if ((contador>=inicio)&&(contador<fin)) {
		     if(separadores!=5) {
			 arrcuentas[indice][0]="8";
			 arrcuentas[indice][1]=datos[1]; //numero cta
			 //EIGlobal.mensajePorTrace("**1 "+datos[1] , EIGlobal.NivelLog.INFO);
			 arrcuentas[indice][2]=datos[8]; //propia o de terceros
			 //EIGlobal.mensajePorTrace("**8 "+datos[8] , EIGlobal.NivelLog.INFO);
			 arrcuentas[indice][3]=datos[7]; //tipo cta
			 //EIGlobal.mensajePorTrace("**7 "+datos[7] , EIGlobal.NivelLog.INFO);
			 if (datos[2].equals ("NINGUNA")) {
			     arrcuentas[indice][4]=datos[3]; //descripcion
			 } else {
			     arrcuentas[indice][4]=datos[2]+"  "+datos[3];
			 }
			 //EIGlobal.mensajePorTrace("**3 "+datos[3] , EIGlobal.NivelLog.INFO);
			 if (datos[2].equals ("NINGUNA")) {
			     arrcuentas[indice][5]="";
			 } else {
			     arrcuentas[indice][5]=datos[2]; //cuenta hogan
			 }
			 //EIGlobal.mensajePorTrace("**2 "+datos[2] , EIGlobal.NivelLog.INFO);
			 arrcuentas[indice][6]=datos[6]; //FM
			 //EIGlobal.mensajePorTrace("**6 "+datos[6] , EIGlobal.NivelLog.INFO);
			 arrcuentas[indice][7]=datos[4]; //FM
			 //EIGlobal.mensajePorTrace("**4 "+datos[4] , EIGlobal.NivelLog.INFO);
			 arrcuentas[indice][8]=datos[5];
			 //EIGlobal.mensajePorTrace("**5 "+datos[5] , EIGlobal.NivelLog.INFO);
		     }
		     else //interbancarios
		     {
			 arrcuentas[indice][1]=datos[1];
			 arrcuentas[indice][2]=datos[2];
			 arrcuentas[indice][3]=datos[3];
			 arrcuentas[indice][4]=datos[4];
			 arrcuentas[indice][5]=datos[5];
		     }
		     indice++;
		 }
		 contador++;
	     }//while
	     entrada.close ();
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer cuentas" + e.toString (),
				 EIGlobal.NivelLog.ERROR);
	              EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
			 }
		 }
	 indice--;
	 arrcuentas[0][0] = ""+indice;
	 EIGlobal.mensajePorTrace ("**Sali del arrcuentas = >" +arrcuentas[0][0]+ "<", EIGlobal.NivelLog.DEBUG);
	 return arrcuentas;
     }
     //Metodo para Leer de un Archivo y desentramar el Archivo que contiene los Movimientos del Cliente

/********************************************/
/** Metodo: ObteniendoCtasInteg1
 * @return String[][]
 * @param int -> inicio
 * @param int -> fin
 * @param String -> archivo
 */
/********************************************/
     public String[][] ObteniendoCtasInteg1 (int inicio, int fin, String archivo) {
	 EIGlobal.mensajePorTrace ("***Entrando a  ObteniendoCtasInteg 1 ", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***inicio = >" +inicio+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***fin = >" +fin+ "<", EIGlobal.NivelLog.DEBUG);
	 String[][] arrcuentas =null;
	 String datos[] =null;
	 String linea = "";
	 int separadores=0;
	 BufferedReader entrada;
	 int indice=1;
	 int contador=0;
	 arrcuentas = new String[fin+1][10];
	 arrcuentas[0][1] = "NUM_CUENTA";
	 arrcuentas[0][2] = "TIPO_RELAC_CTAS";
	 arrcuentas[0][3] = "TIPO_CUENTA";
	 arrcuentas[0][4] = "NOMBRE_TITULAR";
	 arrcuentas[0][5] = "CUENTA_HOGAN";
	 arrcuentas[0][6] = "FM";
	 arrcuentas[0][7] = "CVE_PRODUCTO_S";
	 arrcuentas[0][8] = "CVE_SUBPRODUCTO_S";
	 FileReader lee = null;
	 try {
			 lee = new FileReader (archivo);
	     entrada = new BufferedReader ( lee );
	     while ( ( linea = entrada.readLine () ) != null ) {
		 separadores=cuentaseparadores (linea);
		 datos = desentramaC (""+separadores+"@"+linea,'@');
		 if ((contador>=inicio)&&(contador<fin)) {
		     if(separadores!=5) {
			 arrcuentas[indice][0]="8";
			 arrcuentas[indice][1]=datos[1]; //numero cta
			 arrcuentas[indice][2]=datos[8]; //propia o de terceros
			 arrcuentas[indice][3]=datos[7]; //tipo cta
			 if (datos[2].equals ("NINGUNA")) {
			     arrcuentas[indice][4]=datos[3]; //descripcion
			 } else {
			     arrcuentas[indice][4]=datos[2]+"  "+datos[3];
			 }
			 if (datos[2].equals ("NINGUNA")) {
			     arrcuentas[indice][5]="";
			 } else {
			     arrcuentas[indice][5]=datos[2]; //cuenta hogan
			 }
			 arrcuentas[indice][6]=datos[6]; //FM
			 arrcuentas[indice][7]=datos[4]; //FM
			 arrcuentas[indice][8]=datos[5];
			 arrcuentas[indice][9]=datos[9];
		     }
		     else //interbancarios
		     {
			 arrcuentas[indice][1]=datos[1];
			 arrcuentas[indice][2]=datos[2];
			 arrcuentas[indice][3]=datos[3];
			 arrcuentas[indice][4]=datos[4];
			 arrcuentas[indice][5]=datos[5];
		     }
		     indice++;
		 }
		 contador++;
	     }//while
	     entrada.close ();
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer cuentas" + e.toString (),
				 EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
			 }
		 }
	 indice--;
	 arrcuentas[0][0] = ""+indice;
	 EIGlobal.mensajePorTrace ("**Sali del arrcuentas = >" +arrcuentas[0][0]+ "<", EIGlobal.NivelLog.DEBUG);
	 return arrcuentas;
     }// fin 1
     //Metodo para Leer de un Archivo y desentramar el Archivo que contiene los Resultados

/********************************************/
/** Metodo: ObteniendoResultados
 * @return String[][]
 * @param int -> inicio
 * @param int -> fin
 * @param String -> archivo
 */
/********************************************/
     public String[][] ObteniendoResultados (int inicio, int fin, String archivo) {
	 EIGlobal.mensajePorTrace ("***Entrando a  ObteniendoResultados ", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***inicio = >" +inicio+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***fin = >" +fin+ "<", EIGlobal.NivelLog.DEBUG);
	 String[][] arrcuentas =null;
	 String datos[] =null;
	 String linea = "";
	 int separadores=0;
	 BufferedReader entrada;
	 int indice=1;
	 int contador=0;
	 arrcuentas = new String[fin+1][10];
	 arrcuentas[0][1] = "TIPO CONSULTA";
	 arrcuentas[0][2] = "NUMERO FOLIO";
	 arrcuentas[0][3] = "HORA ENTREGA";
	 arrcuentas[0][4] = "FECHA ENTREGA";
	 arrcuentas[0][5] = "MEDIO ENTREGA";
	 arrcuentas[0][6] = "FORMATO ARCHIVO";
	 arrcuentas[0][7] = "OBSERVACIONES";
	 arrcuentas[0][8] = "PATH ARCHIVO";
	 arrcuentas[0][9] = "NOMBRE ARCHIVO";
	 FileReader lee = null;
	 try {
		 lee = new FileReader (archivo);
	     entrada = new BufferedReader ( lee );
	     while ( ( linea = entrada.readLine () ) != null ) {
		 separadores=cuentaseparadores1 (linea);
		 datos = desentramaC (""+separadores+"$"+linea,'$');
		 if ((contador>=inicio)&&(contador<fin)) {
		     if(separadores!=5) {
			 arrcuentas[indice][0]="7";
			 arrcuentas[indice][1]=datos[1];
			 arrcuentas[indice][2]=datos[2];
			 arrcuentas[indice][3]=datos[3];
		     arrcuentas[indice][4]=datos[4];
		     arrcuentas[indice][5]=datos[5];
			 arrcuentas[indice][6]=datos[6];
			 arrcuentas[indice][7]=datos[7];
		     arrcuentas[indice][8]=datos[8];
			 arrcuentas[indice][9]=datos[9];
			 }
		     else
		     {
			 arrcuentas[indice][1]=datos[1];
			 arrcuentas[indice][2]=datos[2];
			 arrcuentas[indice][3]=datos[3];
			 arrcuentas[indice][4]=datos[4];
			 arrcuentas[indice][5]=datos[5];
		     }
		     indice++;
		 }
		 contador++;
	     }//while
	     entrada.close ();
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer Resultado" + e.toString (),
				 EIGlobal.NivelLog.ERROR);
                    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try{
				 lee.close();
			 }catch(Exception e) {
			 }
		 }
	 indice--;
	 arrcuentas[0][0] = ""+indice;
	 EIGlobal.mensajePorTrace ("**Sali del arrcuentas = >" +arrcuentas[0][0]+ "<", EIGlobal.NivelLog.DEBUG);
	 return arrcuentas;
     }//fin obteniendo

/********************************************/
/** Metodo: BuscandoCtaInteg
 * @return String[]
 * @param String -> ctabuscar
 * @param String -> archivo
 * @param String -> modulo
 */
/********************************************/
     public String[] BuscandoCtaInteg (String ctabuscar,String archivo,String modulo) {
	 EIGlobal.mensajePorTrace ("***Entrando a  BuscandoCtaInteg ", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***ctabuscar = >" +ctabuscar+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***ctabuscar.length() == >" +ctabuscar.length ()+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("***archivo = >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
	 String linea = "";
	 BufferedReader entrada;

	 String datos[] =null;
	 String result[] = null;
	 int registro=8;
	 if (modulo.equals (IEnlace.MDep_Inter_Abono))
	     registro=5;
		 FileReader lee = null;
	 try {
	     if ( ctabuscar.length () >= 11) {
				 lee = new FileReader (archivo);
		 entrada = new BufferedReader ( lee );
		 while ( ( linea = entrada.readLine () ) != null ) {
		     if (linea.indexOf (ctabuscar)>=0) {
			 datos = desentramaC (""+registro+"@"+linea,'@');
			 result=new String[registro+1];
			 result[0]=""+registro;
			 if (modulo.equals (IEnlace.MDep_Inter_Abono)) {
			     result=datos;
			     EIGlobal.mensajePorTrace ("***Encontre cuenta externa", EIGlobal.NivelLog.DEBUG);
			 } else {
			     result[1]=datos[1]; //numero cta
			     result[2]=datos[8]; //propia o de terceros
			     result[3]=datos[7]; //tipo cta
			     result[4]=datos[3]; //descripcion
			     result[5]=datos[2]; //cuenta hogan
			     result[6]=datos[6]; //FM
			     result[7]=datos[4]; //producto
			     result[8]=datos[5]; //subproducoto
			     EIGlobal.mensajePorTrace ("***Encontre cuenta interna result[2] = >" +result[2]+ "<", EIGlobal.NivelLog.DEBUG);
			 }
			 return result;
		     }
		 }//while
		 entrada.close ();
	     } //del if
	 } catch ( Exception e ) {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer cuentas" + e.toString (),
				 EIGlobal.NivelLog.ERROR);
                   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 } finally {
			 try
			 {
				 lee.close();
			 }
			 catch (Exception e)
			 {
			 }
		 }
	 return result;
     }


/********************************************/
	 /** Metodo: llamado_servicioCtasInteg
	  * @return boolean
	  * @param String -> modulo
	  * @param String -> caltair
	  * @param String -> chogan
	  * @param String -> cdescrip
	  * @param String -> archivo
	  * @param HttpServletRequest -> request
	  */
	 /********************************************/
	  public boolean llamado_servicio50CtasInteg (String modulo, String caltair, String chogan, String cdescrip, String archivo,HttpServletRequest request) {
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a llamado_servicio 50 cuentas", EIGlobal.NivelLog.DEBUG);
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a modulo   : >" +modulo+ "<", EIGlobal.NivelLog.DEBUG);
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a caltair  : >" +caltair+ "<", EIGlobal.NivelLog.DEBUG);
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a chogan   : >" +chogan+ "<", EIGlobal.NivelLog.DEBUG);
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a cdescrip : >" +cdescrip+ "<", EIGlobal.NivelLog.DEBUG);
		  EIGlobal.mensajePorTrace ("BaseServlet entrando a archivo  : >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
		  HttpSession sess = request.getSession ();
		  BaseResource session = (BaseResource) sess.getAttribute ("session");
		  boolean result=false;
		  ServicioTux tuxGlobal = new ServicioTux ();
		  //tuxGlobal.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		  String trama_entrada = session.getContractNumber () + "|"
		                       + session.getUserID8() + "|"
		                       + session.getUserProfile() + "|"
		                       + IEnlace.LOCAL_TMP_DIR + "/" + archivo + "|"
		                       + modulo + "|"
		                       + caltair + "|"
		                       + chogan + "|"
		                       + cdescrip + "|"
		                       + "S|";//Se a�ade un parametro para obtener solo 50 cuentas.
		  EIGlobal.mensajePorTrace ("BaseServlet llamado_servicio 50 cuentas>" +trama_entrada+ "<", EIGlobal.NivelLog.DEBUG);
		  try {
			  Hashtable hs =  tuxGlobal.cuentas_modulo (trama_entrada);
			  String trama_salida = (String) hs.get ("COD_ERROR");
			  EIGlobal.mensajePorTrace ( "BaseServlet trama_salidav = >" +trama_salida+ "<", EIGlobal.NivelLog.DEBUG);
			  if ("CCTA0000".equals(trama_salida)) {
				  ArchivoRemoto archR = new ArchivoRemoto ();
				  if(!archR.copia (archivo)) {
					  EIGlobal.mensajePorTrace ("BaseServlet No se pudo copiar el archivo remoto:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.ERROR);
				  } else {
					  EIGlobal.mensajePorTrace ("BaseServlet archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.DEBUG);
					  result=true;
				  }
			  }
		  } catch( java.rmi.RemoteException re ) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			  EIGlobal.mensajePorTrace ("E->llamado_servicioCtasInteg 50 cuentas->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
		  } catch (Exception e) {}
		  	  EIGlobal.mensajePorTrace ("*** result : >" +result+ "<", EIGlobal.NivelLog.ERROR);
		  	  return result;
	  	}


/********************************************/
/** Metodo: llamado_servicioCtasInteg
 * @return boolean
 * @param String -> modulo
 * @param String -> caltair
 * @param String -> chogan
 * @param String -> cdescrip
 * @param String -> archivo
 * @param HttpServletRequest -> request
 */
/********************************************/
     public boolean llamado_servicioCtasInteg (String modulo, String caltair, String chogan, String cdescrip, String archivo,HttpServletRequest request) {
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a llamado_servicio", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a modulo   : >" +modulo+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a caltair  : >" +caltair+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a chogan   : >" +chogan+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a cdescrip : >" +cdescrip+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a archivo  : >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 boolean result=false;
	 ServicioTux tuxGlobal = new ServicioTux ();
	 //tuxGlobal.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	  String trama_entrada = session.getContractNumber () + "|"
					       + session.getUserID8() + "|"
					       + session.getUserProfile() + "|"
					       + IEnlace.LOCAL_TMP_DIR + "/" + archivo + "|"
					       + modulo + "|"
					       + caltair + "|"
					       + chogan + "|"
					       + cdescrip + "|"
					       + "N|";//Se a�ade un parametro mas para indicar que el flujo es normal.
	 EIGlobal.mensajePorTrace ("BaseServlet llamado_servicio >" +trama_entrada+ "<", EIGlobal.NivelLog.DEBUG);
	 try {
	     Hashtable hs =  tuxGlobal.cuentas_modulo (trama_entrada);
	     String trama_salida = (String) hs.get ("COD_ERROR");
	     EIGlobal.mensajePorTrace ( "BaseServlet trama_salidav = >" +trama_salida+ "<", EIGlobal.NivelLog.DEBUG);
	     if ("CCTA0000".equals(trama_salida)) {
			ArchivoRemoto archR = new ArchivoRemoto ();
				 if(!archR.copia (archivo)) {
					 EIGlobal.mensajePorTrace ("BaseServlet No se pudo copiar el archivo remoto:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.ERROR);
				 } else {
					 EIGlobal.mensajePorTrace ("BaseServlet archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.DEBUG);
					 result=true;
				 }
	     }
	 } catch( java.rmi.RemoteException re ) {
	    EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
	     EIGlobal.mensajePorTrace ("E->llamado_servicioCtasInteg->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
	 } catch (Exception e) {}
	 EIGlobal.mensajePorTrace ("*** result : >" +result+ "<", EIGlobal.NivelLog.ERROR);
	 return result;
     }

/********************************************/
/** Metodo: llamado_servicioCtasInteg1
 * @return boolean
 * @param String -> modulo
 * @param String -> caltair
 * @param String -> chogan
 * @param String -> cdescrip
 * @param String -> archivo
 * @param HttpServletRequest -> request
 */
/********************************************/
     public boolean llamado_servicioCtasInteg1 (String modulo, String caltair, String chogan, String cdescrip, String archivo,HttpServletRequest request) {
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a llamado_servicio N43", EIGlobal.NivelLog.DEBUG);

	 EIGlobal.mensajePorTrace ("BaseServlet entrando a modulo   : >" +modulo+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a caltair  : >" +caltair+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a chogan   : >" +chogan+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a cdescrip : >" +cdescrip+ "<", EIGlobal.NivelLog.DEBUG);
	 EIGlobal.mensajePorTrace ("BaseServlet entrando a archivo  : >" +archivo+ "<", EIGlobal.NivelLog.DEBUG);
	 HttpSession sess = request.getSession ();
	 BaseResource session = (BaseResource) sess.getAttribute ("session");
	 boolean result=false;
	 ServicioTux tuxGlobal = new ServicioTux ();
	 //tuxGlobal.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
	 String trama_entrada = "N43" + session.getContractNumber ()+"|"+session.getUserID8 ()+"|"+session.getUserProfile ()+"|"+IEnlace.LOCAL_TMP_DIR + "/" + archivo+"|"+modulo+"|"+caltair+"|"+chogan+"|"+cdescrip+"|";
	 EIGlobal.mensajePorTrace ("BaseServlet llamado_servicio N43 >" +trama_entrada+ "<", EIGlobal.NivelLog.DEBUG);
	 try {
	     Hashtable hs =  tuxGlobal.cuentas_modulo1 (trama_entrada);
	     String trama_salida = (String) hs.get ("COD_ERROR");
	     EIGlobal.mensajePorTrace ( "BaseServlet trama_salidav N43 = >" +trama_salida+ "<", EIGlobal.NivelLog.DEBUG);
	     if ("CCTA0000".equals(trama_salida)) {
		 ArchivoRemoto archR = new ArchivoRemoto ();
		 if(!archR.copia (archivo)) {
		     EIGlobal.mensajePorTrace ("BaseServlet No se pudo copiar el archivo remoto:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.ERROR);
		 } else {
		     EIGlobal.mensajePorTrace ("BaseServlet archivo remoto copiado:"+IEnlace.REMOTO_TMP_DIR+archivo, EIGlobal.NivelLog.DEBUG);
		     result=true;
		 }
	     }
	 } catch( java.rmi.RemoteException re ) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
                  EIGlobal.mensajePorTrace ("E->llamado_servicioCtasInteg1->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
	 } catch (Exception e) {
		 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	 }
	 EIGlobal.mensajePorTrace ("*** result : >" +result+ "<", EIGlobal.NivelLog.DEBUG);
	 return result;
     }// fin llamado 1

/********************************************/
/** Metodo: cuentaseparadores
 * @return int
 * @param String -> linea
 */
/********************************************/
     public int cuentaseparadores (String linea) {
	 int sep=0;
	 int pos=0;
	 int longi=linea.length ();
	 String linea_aux="";
	 while ((pos>-1)&&(pos<longi)) {
	     pos=linea.indexOf ("@");
	     if (pos>=0) {
		 sep++;
		 linea=linea.substring (linea.indexOf ("@")+1,linea.length ());
	     }
	 }
	 return sep;
     }

/********************************************/
/** Metodo: cuentaseparadores1
 * @return int
 * @param String -> linea
 */
/********************************************/
     public int cuentaseparadores1 (String linea) {
	 int sep=0;
	 int pos=0;
	 int longi=linea.length ();
	 String linea_aux="";
	 while ((pos>-1)&&(pos<longi)) {
	     pos=linea.indexOf ("$");
	     if (pos>=0) {
		 sep++;
		 linea=linea.substring (linea.indexOf ("$")+1,linea.length ());
	     }
	 }
	 return sep;
     }//fin cuenta
/* ----------------------------------------------------------------------------------------------------------------------- */

/**
 * *****************************************.
 *
 * @param CambiaContrato the cambia contrato
 * @param request the request
 * @param response the response
 * @return the int
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
/** Metodo: loginMicrositio
 * @return int
 * @param boolean -> CambiaContrato
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
	public int loginMicrositio ( boolean CambiaContrato, HttpServletRequest request, HttpServletResponse response ) /*MSE*/throws ServletException, IOException
	{
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::Inicia loginMicrositio()",EIGlobal.NivelLog.DEBUG);

		String coderror;
		String contra = "";
		String templateElegido = "";
		String usr = "";
		String usrTmp = "";
		String clv4 = "";
		BaseResource session = null;
		HttpSession httpsNew = request.getSession ( /*true */);
		httpsNew.invalidate ();
		httpsNew = request.getSession ();
		session = new BaseResource();
		httpsNew.setAttribute("session",session );
		usrTmp  = getFormParameter(request, "usuario" );
		usr = convierteUsr7a8(usrTmp);
	    //jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
		// enlace internet
		contra = getFormParameter(request, "contrato" );
	    clv4   = getFormParameter(request, "clave" );
		templateElegido = getFormParameter(request, "plantilla" );

		// Micrositio recepcion de campos
		String convenio = "";
		String importe = "";
		// Micrositio transferencias
		String referencia = "";
		String url = "";
		// Micrositio SAT
		String lineaCaptura = "";
		String servicioId = "";

		servicioId = getFormParameter(request, "servicio_id");
		convenio = getFormParameter(request, "convenio");
		importe = getFormParameter(request, "importe");
		if (null == servicioId)
			servicioId = (String) request.getAttribute("servicio_id");
		if (null == convenio)
			convenio = (String) request.getAttribute("convenio");
		if (null == importe)
			importe = (String) request.getAttribute("importe");

		request.setAttribute("convenio", convenio);
		request.setAttribute("importe", importe);

		if ("PMRF".equals(servicioId)) {
			lineaCaptura = getFormParameter(request, "linea_captura");
			if (null == lineaCaptura)
				lineaCaptura = (String) request.getAttribute("linea_captura");

			request.setAttribute("linea_captura", lineaCaptura);
			request.setAttribute("servicio_id", servicioId);
			EIGlobal.mensajePorTrace(this.getClass().getName() + " - "
					+ " LOGIN Variables Micrositio Pago Referenciado" + ": "
					+ convenio + "   " + lineaCaptura + "   " + importe + "   "
					+ servicioId, EIGlobal.NivelLog.DEBUG);
		} else {
			referencia = getFormParameter(request, "referencia");
			url = getFormParameter(request, "url");
			request.setAttribute("referencia", referencia);
		request.setAttribute ( "url", url );
		//log (usr+" "+clv5+" "+contra+" "+templateElegido+ " "+clv4+ " "+pwdold);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + " LOGIN Variables Micrositio: "+convenio+ "   "+referencia+"   "+importe+"   "+url,
				 EIGlobal.NivelLog.DEBUG);
		}
		httpsNew.setAttribute ("session", session);
		inicializando ();
		//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
		//String tramaEnviada= usr + "@" + clv4 + "@";
				/*VC ESC 12/nov/2007 validando si viene del SAM*/
		String headerUser ;
		headerUser = (String)request.getHeader("IV-USER");

		if(headerUser == null)
			{
		/*/VC*/
		//200529800 Recomendaciones CNBV - Praxis - NVS - Inicio -----------
		coderror = obtenLongPwd(usr);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "defaultAction() Respuesta de revision de pwd = >" +coderror+ "<",
				 EIGlobal.NivelLog.DEBUG);

		String [] tramaRecibida = desentramaS(coderror, '@');

		String pwdEncript1 = encriptaPwd(usr, clv4, 10);
		String pwdEncript2 = encriptaPwd(usr, clv4, 20);
		String tramaEnviada = usr;

		if (!tramaRecibida[2].equals("28")){
			tramaEnviada = tramaEnviada + "@" + pwdEncript1 + "@" + pwdEncript2 + "@";
		}
		else {
			tramaEnviada = tramaEnviada + "@ @" + pwdEncript2 + "@";
		}

		EIGlobal.mensajePorTrace("tramaEnviada = " + tramaEnviada, EIGlobal.NivelLog.INFO);
		//200529800 Recomendaciones CNBV - Praxis - NVS - Fin --------------

		ServicioTux ctRemoto = new ServicioTux ();
		//ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		Hashtable htResult = null;
		try {
			htResult = ctRemoto.validaPassword(tramaEnviada);

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace ("E->loginMicrositio->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			// 27/09/2002 Modificacion para correcion de sc_internal_Server_error *
			boolLogin = false;
			String HoraError = ObtenHora ();
			request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\",\"0992\",\"" + HoraError + "\");" );
			//request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Por favor, Intente mas tarde.\",0992,"+HoraError+");" );
			evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );  //MSE
			return 0;
			// Fin Modificacion para correcion de sc_internal_Server_error*
		}

		coderror = ( String ) htResult.get ( "BUFFER" );
		//coderror= (String)request.getParameter("coderror");

		/*VC ESC 12/nov/2007 valida si viene del SAM: el origen es SAM ya se valida la contrase?*/
		}
		else
		{
				usr = convierteUsr7a8(headerUser);
				coderror = Global.CORREDOR_EXITO_SAM;
				//coderror = "SEG_0000@Operaci[s]n Exitosa@";
				//--------------- Se omite llamado a ExpDatePwd
				/*try{ //Se valida la fecha de expiraci? de la contresa? del usuario de SAM
					//String strUrl = "http://appsqa02.mx.bsch:9080/axis/ExpDatePwd";
					String strUrl = Global.URL_EXPIRACION_CONTRASENA_SAM;
					String usr8 = convierteUsr7a8(usr);
					String parametrosUrl = "?usrId=" + usr8 +"&lang=ES&tipoOper=SNE";
					strUrl = strUrl + parametrosUrl;
					EIGlobal.mensajePorTrace("URL->" + strUrl, EIGlobal.NivelLog.DEBUG);

					URL urlService = new URL(strUrl);
					URLConnection urlConn = urlService.openConnection();
					DataInputStream input = new DataInputStream(urlConn.getInputStream());
					BufferedReader d = new BufferedReader(new InputStreamReader(input));
					String expiracionResult = d.readLine();
					expiracionResult = expiracionResult + "|";
					EIGlobal.mensajePorTrace("URL Resultado->" + expiracionResult, EIGlobal.NivelLog.INFO);

					String[] expiracionArray = desentramaS(expiracionResult, '|');


					if (expiracionArray[0].trim().equals("WSEG00FF")){
						coderror = Global.CORREDOR_EXITO_SAM;
						//request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"El usuario no existe\", 3);" );
						//evalTemplate ( IEnlace.LOGIN, request, response );
						//return;
					}else{
						if (expiracionArray[0].trim().equals("WSEG1000")){
							//Va a expirar en tantos dias
							String diasExpStr = expiracionArray[1].substring(
									expiracionArray[1].indexOf(':') + 1,
									expiracionArray[1].length()).trim();
							int diasExp = Integer.parseInt(diasExpStr);

							if (diasExp<=5){	//Se le indica al usuario que su contrase? va a expirar en X dias
								//coderror = "SEG_0001@" + diasExp +"@";
								coderror = Global.CORREDOR_DIAS_EXPIRACION_SAM + diasExp +"@";
							}else{	//No se le indica al usuario sobre la expiracion de su contrase?
								//coderror = "SEG_0000@Operaci[s]n Exitosa@";
								coderror = Global.CORREDOR_EXITO_SAM;
							}
						}else{
							coderror = Global.CORREDOR_EXITO_SAM;

							//if (expiracionArray[0].trim().equals("WSEG1003")){
								//Se le indica al usuario que su contrase? ha expirado
								//coderror = "SEG_0002@La Contrase[n]a Actual ha expirado@";
								//coderror = Global.CORREDOR_EXPIRACION__SAM;
							//}else{
								//Ocurrio un error no hacer nada no se debe interrumpir el flujo
								/*request.setAttribute ("MensajeLogin01",
								//	"cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
								//evalTemplate ( IEnlace.LOGIN, request, response );
								//return;
							//}
						}
					}
				}catch(Exception e){
					EIGlobal.mensajePorTrace("EX expiracion SAM->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					coderror = Global.CORREDOR_EXITO_SAM;
					//<VC proyecto="200710001" autor="JGR" fecha="15/05/2007" descripci?="EL SERVICIO NO DEBE INTERUMPIR EL FLUJO">
					//request.setAttribute ("MensajeLogin01",
					//"cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
					//evalTemplate ( IEnlace.LOGIN, request, response );

					//return;
					//VC
				}*/
				//---------------
			}

		/*/VC*/
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java:: loginMicrositio() Codigo de Error = >" +coderror+ "<",
				 EIGlobal.NivelLog.DEBUG);

		/* 27/09/2002 Modificacion para correccion de sc_internal_server_error*/

		if ( (coderror == null || coderror.equals ("")) )
		{
			boolLogin = false;
			String HoraError = ObtenHora ();
			request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\",\"0992\",\"" + HoraError + "\");" );
			//request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\",0992,"+HoraError+");" );
			evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );  //MSE
		} else {
			String[] arrCodError=null;
			arrCodError=desentramaS(coderror,'@');
			if (arrCodError[0].equals("SEG_0000") || arrCodError[0].equals("SEG_0001"))
			{
				try {
					obtenerContratos (convierteUsr8a7(usr), request, response);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				try {
					Token tok = new Token(usr);
					Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
					BaseResource sess = (BaseResource) request.getSession().getAttribute("session");
					tok.inicializa(conn);
					sess.setValidarToken(true);
					conn.close();
					sess.setToken(tok);
					//CSA TOKEN
					if(tok.getStatus() == Global.STATUS_BLOQ_INTFALLIDOS) {
						boolLogin = false;
						request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S&uacute;per l&iacute;nea Empresarial para desbloquearlo.\", 4);" );
						request.setAttribute ( "Fecha", ObtenFecha () );
						/*int resDup = cierraDuplicidad(session.getUserID8());
						EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
						EIGlobal.NivelLog.DEBUG);*/
						evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );
						return 1;
				   }
				   if(tok.getStatus() == Global.STATUS_BLOQ_INACTIVIDAD) {
						boolLogin = false;
						request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado temporalmente por inactividad, favor de llamar a nuestra S&uacute;per l&iacute;nea Empresarial para desbloquearlo.\", 4);" );
						request.setAttribute ( "Fecha", ObtenFecha () );
						evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );
						return 1;
					}
					EIGlobal.mensajePorTrace ("BaseServlet - status Token: " + tok.getStatus(), EIGlobal.NivelLog.INFO);
					/*if(tok.getStatus() == 2) {
							int resDup = cierraDuplicidad(sess.getUserID());
							request.setAttribute ( "MensajeLogin01", "cuadroDialogoEspecial(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 1);");
							request.setAttribute ( "Fecha", ObtenFecha () );
							evalTemplate ( IEnlace.LOGIN, request, response );
							return 1;
					}*/
				} catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				// *************************Modificacion de la funcion para que
				// reciba los datos del SAT para pago referenciado
				FuncionLoginMicrositio(CambiaContrato, usr, clv4, contra,templateElegido, convenio, referencia, importe, url,lineaCaptura, servicioId, request, response);
			} else {
				boolLogin = false;
				String HoraError = ObtenHora ();
				request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\" "+ convToHTML(arrCodError[1]) +"\",\"" + arrCodError[0] + "\",\"" + HoraError + "\");" );
				evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );  //MSE
			}
		}

		httpsNew.setAttribute ("session", session);
		return 1;
	}
	/* ----------------------------------------------------------------------------------------------------------------------- */

/**
	 * *****************************************.
	 *
	 * @param CambiaContrato the cambia contrato
	 * @param usr the usr
	 * @param clv4 the clv4
	 * @param contra the contra
	 * @param templateElegido the template elegido
	 * @param convenio the convenio
	 * @param referencia the referencia
	 * @param importe the importe
	 * @param url the url
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
/** Metodo: FuncionLoginMicrositio
 * @return int
 * @param boolean -> CambiaContrato
 * @param String -> usr
 * @param String -> clv4
 * @param String -> contra
 * @param String -> templateElegido
 * @param String -> convenio
 * @param String -> referencia
 * @param String -> importe
 * @param String -> url
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 */
/********************************************/
	public int FuncionLoginMicrositio(boolean CambiaContrato, String usr,String clv4, String contra, String templateElegido,String convenio, String referencia, String importe, String url,String lineaCaptura, String servicioId, HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java:: inicia FuncionLoginMicrositio()",
				 EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "********* FUNCION LOGIN Variables Micrositio: "+convenio+" "+referencia+" "+importe+" "+url + " " + contra,
				 EIGlobal.NivelLog.DEBUG);
		boolean UnContrato = false;
		String PtoVta;
		String coderror;
		String strSalida = "";
		String strTemplate = "";
		String buffer = "";
		String clv5 = "";
		String trama_entrada = "";
		String pwdold = "";
		String strTipo = "";
		String strProd = "";
		String contratosAsoc = "";
		int result = 1;
		int resultado = 1;
		boolLogin = true;
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		session.setUserID(convierteUsr7a8(usr));
		session.setUserID8(convierteUsr7a8(usr));// cambio
		session.setPassword(clv4);
		contratosAsoc = session.getContratos();

		req.setAttribute("convenio", convenio);
		req.setAttribute("importe", importe);

		if ("PMRF".equals(servicioId)) {
			req.setAttribute("linea_captura", lineaCaptura);
			req.setAttribute("servicio_id", servicioId);
		} else {
			// Micrositio - Recupera los parametros de la URL para Transferencia
			req.setAttribute("referencia", referencia);
			req.setAttribute("url", url);
		}

		String[] contratos = null;
		contratos = desentramaC ( contratosAsoc, '@' );
		for (int i=1; i<=Integer.parseInt ( contratos[0] ); i++) {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::FuncionLoginMicrositio():301:contratos = >" +contratos[i]+ "<",
					 EIGlobal.NivelLog.DEBUG);
		}
		if ( (Integer.parseInt ( contratos[0] ) == 1) ) {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::FuncionLoginMicrositio()()::if:UN SOLO CONTRATO",EIGlobal.NivelLog.DEBUG);

			String[] contratos3 = desentramaC ( "2|" + contratos[1] + "|", '|' );
			// IMPLEMENTACI�N DE TOKEN PARA CLIENTES UNICONTRATO
			int sesionvalida = ValidaSesionMicrositio(req, res, contratos3[1]);
			if(sesionvalida==1){
				EIGlobal.mensajePorTrace("----- Antes de llamar al metodo CREAAMBIENTE -----", EIGlobal.NivelLog.DEBUG);
				nomContrato = session.getNombreContrato ();
				EIGlobal.mensajePorTrace("\n\n\nnomContrato = "+nomContrato, EIGlobal.NivelLog.INFO);
				int codigo = creaAmbiente(req,res,contratos3[1]); // m?odo CreaAmbiente
				if(codigo != 0){
					req.setAttribute ("CodError", codErrorMicrositio);
					req.setAttribute ("MsgError", msgErrorMicrositio);
					req.setAttribute ("HoraError",ObtenHora ());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return 1;
				}
				nomContrato = session.getNombreContrato();
				result = inicio( req, res ); // m?odo principal
				if(result == 0){
					if ("MXP".equals(tipo_divisa)) {
						modulo = IEnlace.MCargo_transf_pesos;
					} else {
						modulo = IEnlace.MAbono_transf_dolar;
					}
					// Obtener las cuentas asociadas al cliente
					if(llamado_servicioCtasInteg(modulo," "," "," ",session.getUserID8()+".ambci",req)){
						resultado = lectura_cuentas_para_pantalla(req,res,modulo,session.getUserID8()+".ambci");
						if (resultado == 0)
							evalTemplate(IEnlace.CUENTACARGO_LOGIN_MICROSITIO, req, res);
					}else{
						String msgErrorMicrositio = "No existen cuentas de cheques para hacer el pago.";
						String codErrorMicrositio = "0997";
						req.setAttribute ("CodError", codErrorMicrositio);
						req.setAttribute ("MsgError", msgErrorMicrositio);
						req.setAttribute ("HoraError",ObtenHora ());
						evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
						return 1;
					}
				}else{
					String msgErrorMicrositio = "Error al obtener los datos de la empresa a pagar, intente mas tarde.";
					String codErrorMicrositio = "0997";
					req.setAttribute ("CodError", codErrorMicrositio);
					req.setAttribute ("MsgError", msgErrorMicrositio);
					req.setAttribute ("HoraError",ObtenHora ());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return 1;
				}
				UnContrato = true;
				 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "FIN DEL IF CONTRATO = 1",
						 EIGlobal.NivelLog.DEBUG);
			}else if(sesionvalida==0){
				String msgErrorMicrositio = "Su sesi�n ha expirado.";
				String codErrorMicrositio = "0999";
				req.setAttribute ("CodError", codErrorMicrositio);
				req.setAttribute ("MsgError", msgErrorMicrositio);
				req.setAttribute ("HoraError",ObtenHora ());
				evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
				return 1;
			}else{
				return 1;
			}

		}else{
			//M? DE UN CONTRATO
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::loginMicrositio()::if:else:M? DE UN CONTRATO",
					 EIGlobal.NivelLog.DEBUG);
			strTemplate = IEnlace.CONTRATOS_TMPL_MICROSITIO;  /*MSE*/
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Valor de strTemplate = " + strTemplate,EIGlobal.NivelLog.DEBUG);
			strSalida = "";
			String[] Contratos2 = null;
			for ( int indice = 1; indice <= Integer.parseInt ( contratos[0] ); indice++ ) {
				Contratos2 = desentramaC ( "2|" + contratos[indice] + "|", '|' );
				strSalida = strSalida + "<OPTION Value = \"" + Contratos2[1] + "\">" + Contratos2[1] + " " + Contratos2[2] + "</OPTION>";
				if( Contratos2[1].equals ( contra ) ) {
					sess.setAttribute ("ContractNumber",contra);
					session.setContractNumber (contra);
					sess.setAttribute ("ContractNumber",contra);
					session.setContractNumber(contra);
					nomContrato = session.getNombreContrato ();
					String Cont = session.getContractNumber ();
					int codigo = creaAmbiente(req,res,contra); // m?odo CreaAmbiente
					if(codigo != 0){
						req.setAttribute ("CodError", codErrorMicrositio);
						req.setAttribute ("MsgError", msgErrorMicrositio);
						req.setAttribute ("HoraError",ObtenHora ());
						evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
						return 1;
					}
					nomContrato = session.getNombreContrato();
					result = inicio( req, res ); // m?odo principal
					if(result == 0){
						if ("MXP".equals(tipo_divisa)) {
							modulo = IEnlace.MCargo_transf_pesos;
						} else {
							modulo = IEnlace.MAbono_transf_dolar;
						}
						// Obtener las cuentas asociadas al cliente
						if(llamado_servicioCtasInteg(modulo," "," "," ",session.getUserID8()+".ambci",req)){
							resultado = lectura_cuentas_para_pantalla(req,res,modulo,session.getUserID8()+".ambci");
							if (resultado == 0){
								evalTemplate(IEnlace.CUENTACARGO_LOGIN_MICROSITIO, req, res);
								return 1;
							}
						}else{
							String msgErrorMicrositio = "No existen cuentas de cheques para hacer el pago.";
							String codErrorMicrositio = "0997";
							req.setAttribute ("CodError", codErrorMicrositio);
							req.setAttribute ("MsgError", msgErrorMicrositio);
							req.setAttribute ("HoraError",ObtenHora ());
							evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
							return 1;
						}
					}else{
						String msgErrorMicrositio = "Error al obtener los datos de la empresa a pagar, intente mas tarde.";
						String codErrorMicrositio = "0997";
						req.setAttribute ("CodError", codErrorMicrositio);
						req.setAttribute ("MsgError", msgErrorMicrositio);
						req.setAttribute ("HoraError",ObtenHora ());
						evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
						return 1;
					}
				}
			} // fin del for
		} //fin else MAS DE UN CONTRATO
		req.setAttribute ( "Output1", strSalida );
		req.setAttribute ( "Fecha", ObtenFecha () );
		req.setAttribute ( "ContUser", ObtenContUser (req) );
		req.setAttribute ( "fechaHoy", ObtenFecha () );
		req.setAttribute ( "horaHoy", ObtenHora () );
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "############### ----->"+ObtenContUser (req),
				 EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + " UnContrato = " + UnContrato,
				 EIGlobal.NivelLog.DEBUG);
		if ( UnContrato ) {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::loginMicrositio()::2da verificacin:if:UN SOLO CONTRATO",
					 EIGlobal.NivelLog.DEBUG);
			req.setAttribute ("Fecha",ObtenFecha ());
			req.setAttribute ("ContUser", ObtenContUser (req));
			req.setAttribute ("fechaHoy", ObtenFecha ());
			req.setAttribute ("horaHoy", ObtenHora ());
		} else {
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "BaseServlet.java::loginMicrositio()::2da verificacin: if:else:M? DE UN CONTRATO",
					 EIGlobal.NivelLog.DEBUG);
			req.setAttribute ( "Fecha", ObtenFecha ( false ) );
			//modificacion para integracion
			 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "****  Valor Template ****" + strTemplate,
					 EIGlobal.NivelLog.DEBUG);
			if(strTemplate.equals (IEnlace.CONTRATOS_TMPL_MICROSITIO)){
				 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "template Elegido = " +strTemplate,
						 EIGlobal.NivelLog.DEBUG);
					evalTemplate (strTemplate, req, res);
			}else{
				 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "template Elegido CARGATEMPLATE_TMPL_MICROSITIO",
						 EIGlobal.NivelLog.DEBUG);
				evalTemplate ( IEnlace.CARGATEMPLATE_TMPL_MICROSITIO, req, res );  /*MSE*/
			}
		}//fin del else (UnContrato)
		sess.setAttribute ("session", session);
		return 1;
	}

/**
 * *****************************************.
 *
 * @param req the req
 * @param res the res
 * @param Contrato the contrato
 * @return the int
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
/** Metodo: creaAmbiente
 * @return int
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 * @param String -> Contrato
 */
/********************************************/
	public int creaAmbiente( HttpServletRequest req, HttpServletResponse res, String Contrato)throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "#- CreaAmbiente Micrositio									      -#" , EIGlobal.NivelLog.DEBUG);
		boolean sesionvalida = SesionValida(req, res);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		if ( sesionvalida ) {
			//String Contrato = req.getParameter( "lstContratos" );
			String usr = session.getUserID8();
			String clv = session.getPassword();
			String direccion = getFormParameter(req, "opcionElegida" );
			int i = 0;
			String AmbCod = TraeAmbiente(usr,Contrato,req);
			String[] noPerfil = TraePerfil(req);
			EIGlobal.mensajePorTrace( "#- Micrositio Variables de ambiente fijadas #" , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- Direccion de acceso = " + direccion  , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- Usuario		  = " + usr  , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- Contrato		  = " + Contrato  , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- AmbCod		  = " + AmbCod	, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- noPerfil		  = " + noPerfil  , EIGlobal.NivelLog.INFO);
			for( i = 0; i < noPerfil.length; i++) {
				EIGlobal.mensajePorTrace( "#- noPerfil[" + i + "] = " + noPerfil[i]  , EIGlobal.NivelLog.DEBUG);
			}
			session.setContractNumber(Contrato);
			session.setUserProfile(noPerfil[1]);
			String strSalida = "";
			String avisos = "";
			String vencimientosCE;
			if(!FijaAmbiente(req)){
				EIGlobal.mensajePorTrace("***CreaAmbiente Micrositio - FijaAmbiente False", EIGlobal.NivelLog.DEBUG);
				msgErrorMicrositio = "No tiene facultad de operar en Enlace Micrositio Internet <br> con el contrato " + Contrato;
				codErrorMicrositio = "0993";
				return 1;
			}
			if(!verificaFacultad("ACCESINTERNET",req)){
				EIGlobal.mensajePorTrace("***CreaAmbiente Micrositio - Facultad ACCESINTERNET False", EIGlobal.NivelLog.INFO);
				msgErrorMicrositio = "No tiene facultad de operar en Enlace Micrositio Internet <br> con el contrato " + Contrato;
				codErrorMicrositio = "0993";
				return 1;
			} else {
				EIGlobal.mensajePorTrace("***CreaAmbiente Micrositio - Facultad ACCESINTERNET True", EIGlobal.NivelLog.DEBUG);
			}
			String [][] arregloLocations2 = asignaURLS (req, session);
			String nuevoMenu3 = creaFuncionMenu ( arregloLocations2, req );
			EIGlobal.mensajePorTrace("***CreaAmbiente Micrositio AmbCod = &" +AmbCod+ "&", EIGlobal.NivelLog.DEBUG);
			if("AMBI0000".equals(AmbCod)){
				/******* Getronics CP M?ico  Inicia modificacin por Q14886 *******
				if(session.getFacultad(session.FAC_CARGO_CHEQUES) && session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG))
				*/
				EIGlobal.mensajePorTrace("FAC_CARGO_CHEQUES [" + session.getFacultad(session.FAC_CARGO_CHEQUES) + "]", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("FAC_ABOMICRO_PAGCH [" + session.getFacultad(session.FAC_ABOMICRO_PAGCH) + "]", EIGlobal.NivelLog.DEBUG);
				if(session.getFacultad(session.FAC_CARGO_CHEQUES) && session.getFacultad(session.FAC_ABOMICRO_PAGCH))
				/******* Getronics CP M?ico  Termina modificacin por Q14886 *******/
					return 0;
				else{
					msgErrorMicrositio = "No tiene facultades para realizar la operacion";
					codErrorMicrositio = "0995";
					return 1;
				}
			}else{
				EIGlobal.mensajePorTrace("***CreaAmbiente.class AmbCod != 'AMBI0000'", EIGlobal.NivelLog.ERROR);
				msgErrorMicrositio = "Problemas al crear el ambiente.";
				codErrorMicrositio = AmbCod;
				return 1;
			}
		}else{
			msgErrorMicrositio = "El servicio no esta disponible, intente mas tarde";
			codErrorMicrositio = "0992";
			return 1;
		}
	}
	//=============================================================================================

/**
	 * *****************************************.
	 *
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
/** Metodo: inicio
 * @return int
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 */
/********************************************/
	public int inicio( HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***Prueba.class  &inicia m?odo inicio B750()&", EIGlobal.NivelLog.DEBUG);
		req.setAttribute( "fecha", ObtenFecha(true) );
		String salida			= "";
		String contrato 		= "";
		String usuario			= "";
		String usuario1 		= "";
		String convenio 		= "";
		String contratosAsoc		= "";
		String[] camposEmpresa = new String[4];
		int llamada_servicio = 0;
		boolean [] regreso = new boolean [7];
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");
		contratosAsoc = session.getContratos ();
		contrato = session.getContractNumber();
		usuario = session.getUserID8();
		usuario1	= session.getUserID8();
		//perfil = session.getUserProfile();
		convenio	= ( String) req.getParameter( "convenio" );
		try{
			//String el_ = (String) req.getAttribute("contrato");
			if ( usuario.length() < 2 ) usuario = session.getUserID8();
			if ( convenio==null ) convenio = ( String) req.getParameter( "convenio" );
			llamada_servicio = consultaB750(usuario,contrato, req, res);  // llamada al servicio e inicializacin de archivo
			if ( llamada_servicio != 0 ){
				EIGlobal.mensajePorTrace( "***Prueba.class  &error en el servicio&", EIGlobal.NivelLog.DEBUG);
				return 1;
			}else{
				EIGlobal.mensajePorTrace( "***Prueba.class  &servicio OK ...&", EIGlobal.NivelLog.DEBUG);
				return 0;
			}
		}catch (Exception e) {
	                  EIGlobal.mensajePorTrace("***ConsultaDatosEmpresa.class  EX en inicio() >>"+"<<", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return 1; // error
		}
	} // m?odo
	//=============================================================================================

/**
	 * *****************************************.
	 *
	 * @param usuario the usuario
	 * @param contrato the contrato
	 * @param req the req
	 * @param res the res
	 * @return the int
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
/** Metodo: consultaB750
 * @return int
 * @param String -> usuario
 * @param String -> contrato
 * @param HttpServletRequest -> req
 * @param HttpServletResponse -> res
 */
/********************************************/
	public int consultaB750(String usuario, String contrato,HttpServletRequest req, HttpServletResponse res )throws ServletException, IOException{
		int salida			= 0; // por default OK
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String camposEmpresa[]	= null;
		String trama_salida = "";
		String trama_entrada = "";
		String tipo_operacion = "CDEM";  // prefijo del servicio ( consulta datos empresa Pago en l?nea)
		String cabecera = "1EWEB"; // medio de entrega ( regresa trama)
		String convenio = req.getParameter("convenio");
		String referencia = req.getParameter( "referencia" );
		String importe = req.getParameter( "importe" );
		String url = req.getParameter( "url" );
		String perfil = "";
		String pipe = "|";
		int tokens = 13;
		perfil = session.getUserProfile();
		boolean errorFile = false;
		if (!convenio.trim().equals("")){
			try{

				//CSA
				EIGlobal.mensajePorTrace( "***CSA -> ConsultaDatosEmpresa.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.DEBUG);
				trama_entrada	=	cabecera + "|"
								+	 usuario + "|"
								+ tipo_operacion + "|"
								+	contrato + "|"
								+	 usuario + "|"
								+	  perfil + "|"
								+		convenio;
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				try{
					Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
					trama_salida = (String) hs.get("BUFFER");
				}catch( java.rmi.RemoteException re ){
                                        EIGlobal.mensajePorTrace ("E->consultaB750->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				}catch(Exception e) {
                                         EIGlobal.mensajePorTrace ("E->consultaB750->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				if(trama_salida==null || trama_salida.equals("null"))
					trama_salida="";
				EIGlobal.mensajePorTrace("ConsultaDatosEmpresa - iniciaConsultaDatosEmpresa Trama salida: "+trama_salida, EIGlobal.NivelLog.DEBUG);
				camposEmpresa=desentramaDatosEmpresa(trama_salida, tokens, pipe);
				if(camposEmpresa[0].equals("TUBO0000")){
					if (camposEmpresa[4].equals("") || camposEmpresa[12].equals("")){
						salida = 1; // error
						EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Los datos de la empresa vienen vacios >>"+camposEmpresa[0]+"<<", EIGlobal.NivelLog.ERROR);
					}else{
						req.setAttribute("numContrato",contrato);
						req.setAttribute("nomContrato", nomContrato);
						req.setAttribute("empresa",camposEmpresa[12]);
						if (camposEmpresa[4].trim().length() > 11)
							req.setAttribute("ctaabono", camposEmpresa[4].substring(9,20));
						req.setAttribute("convenio", convenio);
						req.setAttribute("referencia", referencia);
						req.setAttribute("importe", importe);
						req.setAttribute("web_application",Global.WEB_APPLICATION);
						req.setAttribute("url", url);
						tipo_divisa =  camposEmpresa[5].trim();
						salida=0; // OK
					}
				}else{
					salida=1; // error
				}
			}catch(Exception e){
				salida = 1; // error
                                 EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Exc en consultaB750() >>"+"<<", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}else{
			salida = 1;
			EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  El contrato viene vacio ...&", EIGlobal.NivelLog.DEBUG);
		}
		return salida;
	}

/**
 * *****************************************.
 *
 * @param trama the trama
 * @param tokens the tokens
 * @param separador the separador
 * @return the string[]
 */
/** Metodo: desentramaDatosEmpresa
* @return String []
* @param String -> trama
* @param int -> tokens
* @param String -> separador
*/
/********************************************/
	private String [] desentramaDatosEmpresa(String trama, int tokens, String separador) //Desentrama los datos que regresa por trama el TUBO al ejecutar la CDEM
	{
		String [] aEmpresa = new String [tokens];
		int indice = trama.indexOf(separador); // String el pipe, o ;
		EIGlobal.mensajePorTrace("B750_Micrositio - desentramaDatosEmpresa(): Formateando arreglo de datos.", EIGlobal.NivelLog.DEBUG);
		for(int i=0; i<tokens;i++){
			if(indice>0)
			aEmpresa [i] = trama.substring(0,indice);
			if(aEmpresa[i] == null)
				aEmpresa [i] = "	  ";
			trama = trama.substring(indice+1);
			indice = trama.indexOf(separador);
		}
		return aEmpresa;
	}

/**
 * *****************************************.
 *
 * @param req the req
 * @param res the res
 * @param modulo the modulo
 * @param archivo the archivo
 * @return the int
 * @throws ServletException the servlet exception
 * @throws IOException Signals that an I/O exception has occurred.
 */
/** Metodo: lectura_cuentas_para_pantalla
* @return int
* @param HttpServletRequest -> req
* @param HttpServletResponse -> res
* @param String -> modulo
* @param String -> archivo
*/
/********************************************/
	private int lectura_cuentas_para_pantalla(HttpServletRequest req, HttpServletResponse res, String modulo, String archivo) throws ServletException, IOException
    {
	EIGlobal.mensajePorTrace("---------------------------------------------", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("***Entrando a  lectura_cuentas_para_pantalla ", EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("---------------------------------------------", EIGlobal.NivelLog.DEBUG);
	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute("session");
	String resultado="";
	BufferedReader entrada=null;
	String linea = "";
	int separadores =0;
	boolean continua=true;
	int contador=1;
	String clase="";
	String trama="";
	String datos[] =null;
		Hashtable tabla_cuentas = new Hashtable();
		Hashtable htResult;
		Vector cuentas_regreso = new Vector();
		String cabecera_trama = "1EWEB|" + session.getUserID8() + "|SDCT|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
		String trama_entrada = "";
		String cuenta = "";
		String saldo = "";
		String titular = "";
		String coderror = "";
		String[] aCuenta = new String[3];
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	try{
	    entrada = new BufferedReader( new FileReader( IEnlace.LOCAL_TMP_DIR + "/" + archivo));
	    while((linea = entrada.readLine() ) != null ){
		separadores=cuentaseparadores(linea);
		continua=true;
		if (continua==true){
		    datos = desentramaC(""+separadores+"@"+linea,'@');
					trama_entrada = cabecera_trama + datos[1] + "|" + "P" + "|";
					EIGlobal.mensajePorTrace("TRAMA ENTRADA SDCT >>" + trama_entrada + "<<", EIGlobal.NivelLog.INFO);
					htResult = tuxGlobal.web_red(trama_entrada);
					coderror = ( String ) htResult.get( "BUFFER" );
					EIGlobal.mensajePorTrace("TRAMA SALIDA SDCT = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);
					if (coderror.substring(0,2).equals("OK")){
						saldo = coderror.substring(16,30).trim();
						cuenta	= datos[1].trim();
						titular = datos[3].trim();
						aCuenta[0] = cuenta;
						aCuenta[1] = titular;
						aCuenta[2] = saldo;
						String[] a = (String []) aCuenta;
					    tabla_cuentas.put(aCuenta[0], a.clone());
						cuentas_regreso.add(a.clone());
						contador++;
					}else{
						cuenta	= datos[1].trim();
						titular = datos[3].trim();
						saldo = "No se pudo obtener informacion de esa cuenta";
						aCuenta[0] = cuenta;
						aCuenta[1] = titular;
						aCuenta[2] = saldo;
						String[] a = (String []) aCuenta;
					    tabla_cuentas.put(aCuenta[0], a.clone());
						cuentas_regreso.add(a.clone());
						contador++;
					}
		}
	    }//while
	}catch(Exception e){
                EIGlobal.mensajePorTrace ("E->lectura_cuentas_para_pantalla->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return 1;
	}finally{
			try{
				entrada.close();
			}catch(Exception e){
	                        EIGlobal.mensajePorTrace ("E->lectura_cuentas_para_pantalla->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				return 1;
			}
	}
	if ((contador>0)/*&&(contador<=Global.MAX_CTAS_MODULO)*/){
	    req.setAttribute("vector_cuentas",cuentas_regreso);
	}else{
			msgErrorMicrositio = "No existen cuentas de cheques asociadeas a ese usuario.";
			codErrorMicrositio = "0999";
			req.setAttribute ("CodError", codErrorMicrositio);
			req.setAttribute ("MsgError", msgErrorMicrositio);
			req.setAttribute ("HoraError",ObtenHora ());
			evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
	}
	return 0;
    }
    /*Banco Santander Mexicano
	  M?odo desentramaString desentrama una cadena por un separador dado
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
     * *****************************************.
     *
     * @param buffer the buffer
     * @param Separador the separador
     * @return the string[]
     */
/** Metodo: desentramaString
 * @return String[]
 * @param String -> buffer
 * @param char -> Separador
 */
/********************************************/
    public String[] desentramaString ( String buffer, char Separador ) {
	     String bufRest;
		 int numeroSeparadores=0;
	     numeroSeparadores = cuentaseparadorCLABE(buffer,String.valueOf(Separador));
		 String[] arregloSal = new String[ numeroSeparadores + 1 ];
	     arregloSal[0] = String.valueOf( numeroSeparadores );
		 bufRest = buffer;
		 for( int cont = 1; cont <= numeroSeparadores; cont++ ) {
			 arregloSal[cont] = bufRest.substring ( 0, bufRest.indexOf ( Separador ) );
		     bufRest = bufRest.substring ( bufRest.indexOf ( Separador) + 1, bufRest.length () );
		 }
	     return arregloSal;
    }
    /*Banco Santander Mexicano
	  M?odo cuentaseparadorCLABE cuenta los separadores de un string
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
     * *****************************************.
     *
     * @param linea the linea
     * @param Separador the separador
     * @return the int
     */
/** Metodo: cuentaseparadorCLABE
 * @return int
 * @param String -> linea
 * @param String -> Separador
 */
/********************************************/
    public int cuentaseparadorCLABE (String linea, String Separador) {
		 int sep=0;
		 int pos=0;
		 int longi=linea.length ();
		 String linea_aux="";
		 while ((pos>-1)&&(pos<longi)) {
		     pos=linea.indexOf (Separador);
		     if (pos>=0) {
				 sep++;
				 linea=linea.substring (linea.indexOf (Separador)+1,linea.length ());
		     }
		 }
		 return sep;
     }
    /*Banco Santander Mexicano
	  M?odo actualizaCLABE ejecuta servicio para actualizacion de cuentas CLABE
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
     * *****************************************.
     *
     * @param Operacion the operacion
     * @param Archivo the archivo
     * @param request the request
     * @param response the response
     * @return the string[]
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
/** Metodo: actualizaCLABE
 * @return String[]
 * @param String -> Operacion
 * @param String -> Archivo
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
    public String[] actualizaCLABE(String Operacion, String Archivo, HttpServletRequest request, HttpServletResponse response )throws IOException, ServletException {
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String vectortrama[]  = null;
		String trama_salida	  = "";
		String trama_entrada  = "";
		String cabecera 	  = "1EWEB"; // medio de entrega
		String tipo_operacion = "ACCL";  // prefijo del servicio ( actualizacion de cuentas Clabe)
		String usuario	= session.getUserID8();	       // Usuario
		String contrato = session.getContractNumber(); // Contrato
		String perfil	= session.getUserProfile();    // Perfil
		String valida  = null;
		// Actualizacion manual
		if ("A".equals(Operacion)) {
			valida	= "0";
		}
		// Actualizacion masiva
		else if ("B".equals(Operacion)) {
			valida	= "1";
		}
		// Validacion del archivo
		else if ("C".equals(Operacion)) {
			valida	= "2";
		}
		// Generacin archivo Export .ambcie
		else if ("D".equals(Operacion)) {
			valida	= "3";
		}
	    EIGlobal.mensajePorTrace( "*** BaseServlet.actualizaCLABE entrado ***", EIGlobal.NivelLog.DEBUG);
		trama_entrada	=	 cabecera + "|"
						+	  usuario + "|"
						+  tipo_operacion + "|"
						+	 contrato + "|"
						+	  usuario + "|"
						+	   perfil + "|"
						+	   valida + "|" // '0' indica stream
											   // '1' indica archivo
						+	  Archivo + "|";
		EIGlobal.mensajePorTrace( "*** BaseServlet.actualizaCLABE  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "*** BaseServlet.actualizaCLABE  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		try{
			Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
			trama_salida = (String) hs.get("BUFFER");
		}catch( java.rmi.RemoteException re ){
                        EIGlobal.mensajePorTrace ("E->actualizaCLABE->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		}catch(Exception e) {
                        EIGlobal.mensajePorTrace ("E->actualizaCLABE->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE trama salida >> "+trama_salida, EIGlobal.NivelLog.DEBUG);
		// desentrama los valores de la trama de salida
		// la posicion [0] contiene el numero de elementos
		if (trama_salida==null || trama_salida.equals("")) {
				EIGlobal.mensajePorTrace( "*** BaseServlet.actualizaCLABE  error >> Trama de salida vacia...<<", EIGlobal.NivelLog.DEBUG);
				vectortrama = new String[2];
				vectortrama[1] = "ACCL9999";
		}
		else {
			if(trama_salida.indexOf('|')<=0){
				trama_salida = trama_salida+"|";
				EIGlobal.mensajePorTrace("CON EL PIPE  >"+trama_salida+"<", EIGlobal.NivelLog.DEBUG);
			}
			vectortrama=desentramaString(trama_salida, '|');
			if (!trama_salida.substring (0,trama_salida.indexOf('|')).equals("ACCL0000")){
				if(!trama_salida.substring(0,4).equals("ACCL")){
					response.sendRedirect("SinFacultades");
				}
				String mensajeErr = trama_salida.substring(trama_salida.indexOf('|')+1,trama_salida.length()) ;
				EIGlobal.mensajePorTrace( "*** BaseServlet.actualizaCLABE  error >>" + mensajeErr + "<<", EIGlobal.NivelLog.DEBUG);
			}
			else if ("C".equals(Operacion)){
				String archDet = contrato +".aclabe";
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  inicio de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
				ArchivoRemoto archivoCLABE = new ArchivoRemoto();
				// Copia de archivo remoto a ruta local
				if(!archivoCLABE.copiaArchivo(archDet, Global.DOWNLOAD_PATH)) {
					EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  no se pudo copiar archivo remoto:" + archDet, EIGlobal.NivelLog.ERROR);
					vectortrama[1] = "ACCL9999";
				}
				else {
				    EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  archivo remoto copiado exitosamente:" + archDet, EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
			}else if ("D".equals(Operacion)) {
				String archDet = usuario +".ambcie";
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  inicio de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
				ArchivoRemoto archivoExportCLABE = new ArchivoRemoto();
				ArchivoRemoto archivoExportCLABE2 = new ArchivoRemoto();
				// Copia de archivo remoto a ruta local del aplication
				if(!archivoExportCLABE.copiaArchivo(archDet, Global.DOWNLOAD_PATH)) {
					EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  no se pudo copiar archivo remoto:" + archDet, EIGlobal.NivelLog.ERROR);
					vectortrama[1] = "ACCL9999";
				}
				else {
				    EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  archivo remoto copiado exitosamente:" + archDet, EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
				// Copia de archivo remoto a ruta local del WEB
				if(!archivoExportCLABE2.copiaLocalARemoto(archDet,"WEB")){
				//if(!archivoExportCLABE2.copiaArchivo(archDet, "/tmp/")) {
					EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  no se pudo copiar archivo remoto_web:" + archDet, EIGlobal.NivelLog.ERROR);
					vectortrama[1] = "ACCL9999";
				}
				else {
				    EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  archivo remoto copiado exitosamente:" + archDet, EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
			}else if ("B".equals(Operacion)) {
				String archDet = contrato +".aclabee";
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  inicio de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
				ArchivoRemoto archivoExportCLABE = new ArchivoRemoto();
				ArchivoRemoto archivoExportCLABE2 = new ArchivoRemoto();
				// Copia de archivo remoto a ruta local APP
				if(!archivoExportCLABE.copiaArchivo(archDet, Global.DOWNLOAD_PATH)) {
					EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  no se pudo copiar archivo remoto:" + archDet, EIGlobal.NivelLog.ERROR);
					vectortrama[1] = "ACCL9999";
				}
				else {
				    EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  archivo remoto copiado exitosamente:" + archDet, EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
				// Copia de archivo remoto a ruta local WEB
				if(!archivoExportCLABE2.copiaLocalARemoto(archDet,"WEB")){
				//if(!archivoExportCLABE2.copiaArchivo(archDet, "/tmp/")) {
					EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  no se pudo copiar archivo remoto:" + archDet, EIGlobal.NivelLog.ERROR);
					vectortrama[1] = "ACCL9999";
				}
				else {
				    EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  archivo remoto copiado exitosamente:" + archDet, EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("*** BaseServlet.actualizaCLABE  fin de copia de archivo remoto  ...", EIGlobal.NivelLog.DEBUG);
			}
		}
	    EIGlobal.mensajePorTrace ("*** BaseServlet.actualizaCLABE  saliendo .... ***" +vectortrama[0]+ "<", EIGlobal.NivelLog.DEBUG);
		return vectortrama;
    }
    /*Banco Santander Mexicano
	  M?odo verificaCLABE servicio para verificar si un contrato tiene	cuentas CLABE 11 digitos
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
     * *****************************************.
     *
     * @param request the request
     * @param response the response
     * @return the int
     * @throws ServletException the servlet exception
     */
/** Metodo: verificaCLABE
 * @return int
 * @param HttpServletRequest -> request
 * @param HttpServletResponse -> response
 */
/********************************************/
    public int verificaCLABE( HttpServletRequest request, HttpServletResponse response ) throws ServletException{
	HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String trama_salida	  = null;
		String trama_entrada  = "";
		String cabecera 	  = "1EWEB"; // medio de entrega
		String tipo_operacion = "CCCO";  // prefijo del servicio ( consulta de cuentas Clabe)
		String usuario	= session.getUserID8();	       // Usuario
		String contrato = session.getContractNumber(); // Contrato
		String perfil	= session.getUserProfile();    // Perfil
		int result = 0;
			    EIGlobal.mensajePorTrace( "*** BaseServlet.verificaCLABE entrado ***", EIGlobal.NivelLog.DEBUG);
				trama_entrada	=	cabecera + "|"
								+	 usuario + "|"
								+ tipo_operacion + "|"
								+	contrato + "|"
								+	 usuario + "|"
								+	  perfil + "|"
								+			  "0|";
		EIGlobal.mensajePorTrace( "*** BaseServlet.verificaCLABE trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "*** BaseServlet.verificaCLABE ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		try{
			Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
			trama_salida = (String) hs.get("BUFFER");
		}catch( java.rmi.RemoteException re ){
                        EIGlobal.mensajePorTrace ("E->verificaCLABE->" + re.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				return 0;
		}catch(Exception e) {
                        EIGlobal.mensajePorTrace ("E->verificaCLABE->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return 0;
		}
		EIGlobal.mensajePorTrace("*** BaseServlet.verificaCLABE trama salida >> "+trama_salida, EIGlobal.NivelLog.DEBUG);
		if(trama_salida.substring (0,trama_salida.indexOf('|')).equals("CCCO0000")) {
			EIGlobal.mensajePorTrace("*** BaseServlet.verificaCLABE valor >> "+trama_salida.substring(trama_salida.indexOf('|')+1,trama_salida.length()-1), EIGlobal.NivelLog.DEBUG);
			result = Integer.parseInt(trama_salida.substring(trama_salida.indexOf('|')+1,trama_salida.length()-1));
		}
		else {
			if (trama_salida==null) {
				EIGlobal.mensajePorTrace( "*** BaseServlet.verificaCLABE error >> Trama de salida vacia...<<", EIGlobal.NivelLog.DEBUG);
			}
			else {
				String mensajeErr = trama_salida.substring(trama_salida.indexOf('|')+1,trama_salida.length()) ;
				EIGlobal.mensajePorTrace( "*** BaseServlet.verificaCLABE error >>" + mensajeErr + "<<", EIGlobal.NivelLog.DEBUG);
			}
		}
	    EIGlobal.mensajePorTrace ("*** BaseServlet.verificaCLABE saliendo .... ***", EIGlobal.NivelLog.DEBUG);
	    return result;
    }
    /*Banco Santander Mexicano
	  M?odo ObteniendoCtasCLABE lectura del archivo de cuentas CLABE
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
     * *****************************************.
     *
     * @param archivo the archivo
     * @param elementNum the element num
     * @param Modulo the modulo
     * @return the string[][]
     */
/** Metodo: ObteniendoCtasCLABE
 * @return String[][]
 * @param String -> archivo
 * @param int -> elementNum
 * @param String -> Modulo
 */
/********************************************/
	 public String[][] ObteniendoCtasCLABE (String archivo, int elementNum, String Modulo) {
		EIGlobal.mensajePorTrace ("*** BaseServlet.ObteniendoCtasCLABE entrando ***", EIGlobal.NivelLog.DEBUG);
	 String[][] arrcuentas =null;
		 String datos[] =null;
		 String linea = "";
		 String tipoCta = "";
		 int separadores=0;
		 BufferedReader entrada;
		 int indice=1;
		 arrcuentas = new String[elementNum+1][9];
		 FileReader lee = null;
		 try {
				 lee = new FileReader (archivo);
		     entrada = new BufferedReader ( lee );
		     while ( ( linea = entrada.readLine () ) != null) {
				if (indice!=1) {
						if ("oclabe".equals(Modulo)) {
					if (linea.substring(0,2).equals("01")) {
						tipoCta = "Transferencias Interbancarias";
					} else {
						tipoCta = "Confirming";
					}
					// Tipo de cuenta
						arrcuentas[indice-1][1]=tipoCta;
						// Clave del proveedor
							arrcuentas[indice-1][2]=linea.substring(3,23);
							// Cuenta anterior
							arrcuentas[indice-1][3]=linea.substring(24,40);
							// Cuenta CLABE
							arrcuentas[indice-1][4]=linea.substring(41,61);
							// Descripcin
							arrcuentas[indice-1][5]=linea.substring(62,102);
							// Clave Banco
							//arrcuentas[indice-1][6]=linea.substring(103,108);
							// Clave Plaza
							//arrcuentas[indice-1][7]=linea.substring(109,114);
							// Sucursal
							arrcuentas[indice-1][8]=linea.substring(115,119);
							// Nombre Banco
							arrcuentas[indice-1][6]=linea.substring(120,160);
							// Nombre Plaza
							arrcuentas[indice-1][7]=linea.substring(161,201);
						}
						else {
							// Tipo registro A=Actulizado R=Rechazado
							//arrcuentas[indice-1][1]=linea.substring(linea.length()-43,linea.length()-42).trim();
							arrcuentas[indice-1][1]=linea.substring(203,204).trim();
							// Descripcion de validacion del registro
							//arrcuentas[indice-1][2]=linea.substring(linea.length()-41,linea.length()-1).trim();
							arrcuentas[indice-1][2]=linea.substring(205,linea.length()-1).trim();
						}
					}
				indice++;
		     }//while
		     entrada.close ();
		 } catch ( Exception e ) {
                        EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "E al traer cuentas " + e.toString (),EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		 } finally {
				 try{
					 lee.close();
				 }catch(Exception e) {
				 }
			 }
		 indice--;
		 arrcuentas[0][0] = ""+indice;
		 EIGlobal.mensajePorTrace ("**Salir del arrcuentas = >" +arrcuentas[0][0]+ "<", EIGlobal.NivelLog.DEBUG);
		 return arrcuentas;
     } // fin de ObteniendoCtasCLABE
	/*Banco Santander Mexicano
	  M?odo ObteniendoCtasCLABE lee el numero de cuentas del archivo de CLABES
	  @Autor:Rafael Rosiles Soto
	  Fecha : 14 de Enero de 2004
	*/

/**
	 * *****************************************.
	 *
	 * @param filename the filename
	 * @return the numberlines ctas clabe
	 */
/** Metodo: getnumberlinesCtasCLABE
 * @return int
 * @param String -> filename
 */
/********************************************/
    public int getnumberlinesCtasCLABE (String filename) {
		 EIGlobal.mensajePorTrace ("*** BaseServlet.getnumberlinesCtasCLABE entrando ***", EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace ("*** BaseServlet.getnumberlinesCtasCLABE nombre archivo >>" + filename, EIGlobal.NivelLog.DEBUG);
		 int j = 0;
		 String linea = null;
			 FileReader lee = null;
		 try{
			lee= new FileReader (filename);
		     BufferedReader entrada= new BufferedReader (lee);
		     while((linea = entrada.readLine ())!=null) {
			 if(linea.trim ().equals (""))
			     linea = entrada.readLine ();
			 if(linea == null)
			     break;
			 j++;
		     }
		     entrada.close ();
		 } catch(Exception e) {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		     return 0;
		 } finally {
				 try{
					 lee.close();
				 }catch(Exception e) {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				 }
			 }
		 EIGlobal.mensajePorTrace ("***leer numero de lineas "+j, EIGlobal.NivelLog.DEBUG);
		 return j;
     }

/********************************************/
/** Metodo: validaDuplicidad
 * @return int
 * @param String -> usuario
 * @param HttpSession -> session
 */
/********************************************/
	public int cierraDuplicidad(String usr) {
		String usuario = null;
		if(usr.length() == 7) {
			usuario = convierteUsr7a8(usr);
		}
		else {
			usuario = usr;
		}
		int result = 0;
		EIGlobal.mensajePorTrace ("logout - cierraDuplicidad -> Usuario a cerrar: <" + usuario + ">", EIGlobal.NivelLog.INFO);
		Connection conn=null;
		Statement sDup =null;

		try {
	     	//Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     	//Statement sDup = conn.createStatement();
	     	conn= createiASConn ( Global.DATASOURCE_ORACLE );
	     	sDup = conn.createStatement();
	     	String query = "UPDATE EWEB_VALIDA_SESION SET ACTIVO = 'N' ,ID_SESION = null WHERE CVE_USUARIO = '" + usuario + "'";
	     	//String query = "UPDATE EWEB_VALIDA_SESION SET ACTIVO = 'N' WHERE CVE_USUARIO = '" + usuario + "'";
	     	EIGlobal.mensajePorTrace ("logout - validaDuplicidad -> Query: <" + query + ">", EIGlobal.NivelLog.INFO);
	     	result = sDup.executeUpdate(query);
			//sDup.close();
			//conn.close();
		}
		catch(SQLException e) {
                        EIGlobal.mensajePorTrace ("logout - validaDuplicidad -> Problemas para validar acceso, se deniega", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			return -1;
		}finally{
				try{
					if(sDup!=null){
						sDup.close();
						sDup=null;
					}
					if(conn!=null){
						conn.close();
						conn=null;
					}
				}catch(Exception e1){
                                        EIGlobal.mensajePorTrace ("BaseServlet-cierraDuplicidad-> "+e1, EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
				}
		}
		return result;
	}

	//200529800 Recomendaciones CNBV - Praxis - NVS - Inicio -------------------
	/**
	 * Obten long pwd.
	 *
	 * @param usr the usr
	 * @return the string
	 */
	public String obtenLongPwd(String usr){
	    EIGlobal.mensajePorTrace("Funcion obtenLongPwd()", EIGlobal.NivelLog.DEBUG);

		String tramaEnviada= usr;
		String coderror = "";

		ServicioTux ctRemoto = new ServicioTux();
		//ctRemoto.setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		Hashtable htResult = null;

		try {
			htResult = ctRemoto.longitudPassword(tramaEnviada);
			coderror = (String) htResult.get ("BUFFER");
		} catch (Exception e) {
                        EIGlobal.mensajePorTrace ("E->cierraDuplicidad->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return coderror;
	}

	/**
	 * Encripta pwd.
	 *
	 * @param usr the usr
	 * @param pwd the pwd
	 * @param len the len
	 * @return the string
	 */
	public String encriptaPwd(String usr, String pwd, int len){
	    EIGlobal.mensajePorTrace("Funcion encriptaPwd()", EIGlobal.NivelLog.DEBUG);
		String pwdEncript = "";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			String amessage = usr + pwd;
			byte [] hash = md.digest(amessage.getBytes());
			String a = new String(hash);

			pwdEncript = Base64.encodeBytes(hash, 0, len);
		}
		catch(Exception e) {
                        EIGlobal.mensajePorTrace ("E->cierraDuplicidad->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return pwdEncript;
	}

	/**
	 * Verifica validez pwd.
	 *
	 * @param pwd the pwd
	 * @return true, if successful
	 */
	public boolean verificaValidezPwd(String pwd) {

        boolean intHayNumeros	= false;
        boolean intHayAlfa		= false;

	    EIGlobal.mensajePorTrace("Funcion verificaValidezPwd()", EIGlobal.NivelLog.DEBUG);

		//verifica longitud rango [8,20] caracteres
		if ( pwd.length()< 8 || pwd.length()>20 ) { return false; }

        //verifica nuevo pwd contenga digitos y letras exclusivamente
        for (int intCiclo=0; intCiclo < pwd.length(); intCiclo++) {
			if ((pwd.charAt(intCiclo) >= 48) && (pwd.charAt(intCiclo) <= 57)) {
				intHayNumeros = true;//de 0 a 9
			} else if ((pwd.charAt(intCiclo) >= 65) && (pwd.charAt(intCiclo) <= 90)) {
				intHayAlfa = true;//de A a Z
			} else if ((pwd.charAt(intCiclo) >= 97) && (pwd.charAt(intCiclo) <= 122)) {
				intHayAlfa = true;//de a a z
			} else {
				return false;
			}
		}

        EIGlobal.mensajePorTrace("intHayNumeros = " + intHayNumeros, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("intHayAlfa = " + intHayAlfa, EIGlobal.NivelLog.DEBUG);

        if (intHayNumeros == false)	{
            EIGlobal.mensajePorTrace("No existe numero en contrase?", EIGlobal.NivelLog.DEBUG);
            return false;
        }
		if (intHayAlfa == false)	{
		    EIGlobal.mensajePorTrace("No existe letra en contrase?", EIGlobal.NivelLog.DEBUG);
		    return false;
	    }

        //valida que no existan caracteres consecutivos en forma ascendente y
        //descendente
		for(int i=0; i<pwd.length()-2 ;i++) {
			if(pwd.charAt(i)+1 == pwd.charAt(i+1) && pwd.charAt(i)+2 == pwd.charAt(i+2))
			    return false; //3 caracteres consecutivos de forma ascendente. Ej. [123] o [abc]
			if(pwd.charAt(i) == pwd.charAt(i+1) && pwd.charAt(i)==pwd.charAt(i+2))
		    	return false; //3 caracteres consecutivos iguales. Ej.[111] o [aaa]
			if(pwd.charAt(i)-1 == pwd.charAt(i+1) && pwd.charAt(i)-2 == pwd.charAt(i+2))
			    return false; //3 caracteres consecutivos de forma descendente. Ej. [321] o [cba]
		}

		return true;
	}

	/**
	 * Verifica pwd prohibido.
	 *
	 * @param usr the usr
	 * @param pwd the pwd
	 * @return true, if successful
	 */
	public boolean verificaPWDProhibido(String usr, String pwd) {
	    EIGlobal.mensajePorTrace("Funcion verificaPWDProhibido()", EIGlobal.NivelLog.DEBUG);

		//verifica que el usuario no este contenido en la contrase?
		if (pwd.equals(usr)) { return false; }

		//Obtiene la lista de palabras prohibidas y valida
		String prohibidas [] = {
			"santander", "serfin", "altec", "enlace", "banca", "electronica",
			"bsch", usr
		};

		for (int i = 0; i < prohibidas.length; i++) {
			if (pwd.toLowerCase().lastIndexOf(prohibidas[i]) != -1) {
				return false;
			}
		}
		return true;
	}
	//200529800 Recomendaciones CNBV - Praxis - NVS - Fin ----------------------

	/*	MSD Octubre 2006
     *          BD   RE    BD   RE    BD   RE    BD   RE
     *          --   --    --   --    --   --    --   --
     *          A    10    H    13    O    20    V    26
     *          B    11    I    14    P    21    W    27
     *          C     *    J    15    Q    22    X    28
     *          D     *    K    16    R    23    Y    29
     *          E     *    L    17    S     *    [    30
     *          F     *    M    18    T    24    ]    31
     *          G    12    N    19    U    25    _    32
	 *											 ~	  33
	 *											 #    34
     *Modificacion: ESC (Z096114) se agregan nuevos caracterse a la tabla de conversion para llegar a los 34 millones.
     */
	/**
	 * Metodo estatico para la conversion del codigo de cliente de 7 a 8 posiciones.
	 * Soporta conversion hasta el codigo de cliente 99,999,999
	 *
	 * @param usr (String)con el codigo de cliente de 7 posiciones.
	 * @return String con el codigo de cliente convertido a 8 posiciones.
	 */
	public static String convierteUsr7a8(String usr) {
		if(usr == null) {
			return "";
		}
		String result = null;
		int codCliente;
		if(usr.length() == 8) result = usr;
		if(usr.length() == 7) {
			if(usr.charAt(0) >= '0' && usr.charAt(0) <= '9') {
				result =  "0" + usr;
			} else {
				switch(usr.charAt(0)) {
					case 'A':	result =  "10" + usr.substring(1);
								break;
					case 'B':	result =  "11" + usr.substring(1);
								break;
					case 'C':
					case 'D':
					case 'E':
					case 'F':	result = null;
								break;
					case 'G':	result = "12" + usr.substring(1);
								break;
					case 'H':	result = "13" + usr.substring(1);
								break;
					case 'I':	result = "14" + usr.substring(1);
								break;
					case 'J':	result = "15" + usr.substring(1);
								break;
					case 'K':	result = "16" + usr.substring(1);
								break;
					case 'L':	result = "17" + usr.substring(1);
								break;
					case 'M':	result = "18" + usr.substring(1);
								break;
					case 'N':	result = "19" + usr.substring(1);
								break;
					case 'O':	result = "20" + usr.substring(1);
								break;
					case 'P':	result = "21" + usr.substring(1);
								break;
					case 'Q':	result = "22" + usr.substring(1);
								break;
					case 'R':	result = "23" + usr.substring(1);
								break;
					case 'S':	result = null;
								break;
					case 'T':	result = "24" + usr.substring(1);
								break;
					case 'U':	result = "25" + usr.substring(1);
								break;
					case 'V':	result = "26" + usr.substring(1);
								break;
					case 'W':	result = "27" + usr.substring(1);
								break;
					case 'X':	result = "28" + usr.substring(1);
								break;
					case 'Y':	result = "29" + usr.substring(1);
								break;
					case '_':	result = "30" + usr.substring(1);
								break;
					case '~':	result = "31" + usr.substring(1);
								break;
					case '#':	result = "32" + usr.substring(1);
								break;
					case '[':	result = "33" + usr.substring(1);
								break;
					case ']':
						codCliente = Integer.parseInt(usr.substring(1), BASE);
						result = Integer.toString(codCliente);
					break;
					default:	result = null;
								break;
				}
			}
		}
		return result;
	}	/*	MSD Octubre 2006*/



	/**
	 * Valida peticion.
	 *
	 * @param req the req
	 * @param res the res
	 * @param session the session
	 * @param sess the sess
	 * @param valida the valida
	 * @return true, if successful
	 */
	public  boolean validaPeticion(HttpServletRequest req, HttpServletResponse res, BaseResource session,HttpSession sess, String valida) {
		try {
			EIGlobal.mensajePorTrace( "TOMO CAMBIO", EIGlobal.NivelLog.DEBUG);

			String opcion = req.getParameter ( "opctrans" );
			String enrolamiento= req.getParameter("enrolamiento");
			EIGlobal.mensajePorTrace("Enrolamiento: " + enrolamiento,
					EIGlobal.NivelLog.INFO);

				String token=req.getParameter("token");
				String usr = session.getUserID8();
				Token tok = session.getToken();
				boolean tokenValido = false;

			Connection conn = null;
			//CSA-  ------ Siempre validar en el valida peticion
			int estatusToken = obtenerEstatusToken(tok);
			if (estatusToken == 1) {
				EIGlobal.mensajePorTrace("Estatus Activo:-> " , EIGlobal.NivelLog.INFO);
				if (opcion != null && opcion.equals("1") && valida != null
						&& valida.equals("0")) {
					EIGlobal.mensajePorTrace("Entra a VALIDAR EL TOKEN.",
							EIGlobal.NivelLog.INFO);
				try {
						conn = createiASConn(Global.DATASOURCE_ORACLE);
					tokenValido = tok.validaToken(conn, token);
					conn.close();
				} catch(SQLException e) {
						EIGlobal.mensajePorTrace(
								"SQLE en BaseServlet al validar token",
								EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e),
								EIGlobal.NivelLog.INFO);
					} finally {
						if (conn != null){
							conn.close();
						}
				}

			//BITACORA//////////////////////////////////////////////////////////
			/*try{
				short suc_opera             = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
				String contrato              = session.getContractNumber();
				String usuario               = session.getUserID();
				String fecha_hoy             = ObtenFecha();
				int numeroReferencia      = obten_referencia_operacion(suc_opera);
				String ruta_archivo_servicio = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID();
				String archivo_servicio      = session.getUserID();
				int numero_campos         = 7 ;

				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
				int numeroReferenciab     = obten_referencia_operacion(suc_opera);
				EIGlobal.mensajePorTrace( "-----------numeroReferencia--b->"+ numeroReferenciab, EIGlobal.NivelLog.INFO);
				//coderror_bit=llamada_bitacora(numeroReferencia,"PROG0000",contrato,usuario,"0",0,"COPR","",0,"");
			} catch(Exception e) {
				e.printStackTrace();
			}*/
            ////////////////////////////////////////////////////////////////////
				if(tokenValido) {
					ValidaOTP.guardaRegistroBitacora(req,"Token valido.");
					limpiaIntentoFallido(tok);
					String templateElegido = (String) req.getSession().getAttribute("plantilla");
					/**
					 * PYME Nomina
					 */
					if( req.getParameter("opcLn") != null && "SI".equals(req.getParameter("opcLn")) ){
						templateElegido = "/enlaceMig/OpcionesNominaLn";
					}

					String params ="";
					if (templateElegido.indexOf("/transferencia") >= 0 ||
						templateElegido.indexOf("/MMC_Alta") >= 0 ||
						templateElegido.indexOf("/MDI_Enviar") >= 0 ||
						templateElegido.indexOf("/MTI_Enviar") >= 0 ||
						templateElegido.indexOf("/SuaEnviar") >= 0 ||
						templateElegido.indexOf("/ConsultaUsuarios") >= 0 ||
						templateElegido.indexOf("/NuevoPagoImpuestos") >= 0) {
						params= ValidaOTP.reestableceParametrosEnSessionEncoded( req);
						params+= "encoded=true&";
						/**
						 * INICIO PYME 2015 Unificacion Token Nomina Epyme
						 */
						if( templateElegido.indexOf("/MMC_Alta") >= 0 ){
							params+= "Modulo=2&";
						}
						EIGlobal.mensajePorTrace( "---> Por aquipaso flujo", EIGlobal.NivelLog.DEBUG);
					}
					/**
					 * INICIO PYME 2015 Unificacion Token Nomina Epyme
					 */
					 else if ( templateElegido.indexOf("/OpcionesNominaLn") == -1 && templateElegido.indexOf("/OpcionesNomina") >= 0 && req.getParameter(OPERACION) != null) {
						StringBuffer aux = new StringBuffer("");
						aux.append("statusDuplicado=").append( req.getParameter("statusDuplicado") )
							.append("&registro=").append( req.getParameter("registro") )
							.append("&strCheck=").append( req.getParameter(STRCHECK) )
							.append("&statushrc=").append( req.getParameter("statushrc") )
							.append("&tipoArchivo=").append( req.getParameter("tipoArchivo") )
							.append("&operacion=").append( req.getParameter(OPERACION) )
							.append("&folio=").append( req.getParameter(FOLIO) )
							.append("&encFechApli=").append( req.getParameter("encFechApli") )
							.append("&txtFav=").append( req.getParameter(TXTFAV) )
							.append("&horario_seleccionado=").append( req.getParameter(HORARIO_SELECCIONADO) )
							.append("&");
						params = aux.toString();
						EIGlobal.mensajePorTrace( "---> Por aquipaso flujo es de nomina", EIGlobal.NivelLog.DEBUG);
					} else if ( templateElegido.indexOf("/OpcionesNominaLn") >= 0 && req.getParameter(OPERACION) != null) {
						StringBuffer aux = new StringBuffer("");
						aux.append("statusDuplicado=").append( req.getParameter("statusDuplicado") )
							.append("&registro=").append( req.getParameter("registro") )
							.append("&strCheck=").append( req.getParameter(STRCHECK) )
							.append("&statushrc=").append( req.getParameter("statushrc") )
							.append("&tipoArchivo=").append( req.getParameter("tipoArchivo") )
							.append("&operacion=").append( req.getParameter(OPERACION) )
							.append("&folio=").append( req.getParameter(FOLIO) )
							.append("&encFechApli=").append( req.getParameter("encFechApli") )
							.append("&txtFav=").append( req.getParameter(TXTFAV) )
							.append("&horario_seleccionado=").append( req.getParameter(HORARIO_SELECCIONADO) )
							.append("&");
							EIGlobal.mensajePorTrace( "---> Por aquipaso flujo es de nominaLn", EIGlobal.NivelLog.DEBUG);
						params = aux.toString();
					}
					/**
					 * FIN
					 */
					else {
						EIGlobal.mensajePorTrace( "---> Por aquipaso flujo es de Otro", EIGlobal.NivelLog.DEBUG);
						params= ValidaOTP.reestableceParametrosEnSession( req);
					}
					req.setAttribute("forzar", "no");
					req.setAttribute("valida2", "1");

					templateElegido+="?"+params+"valida=1";
					EIGlobal.mensajePorTrace( "---> Por aquipaso al final", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace( "------------TOKEN VALIDO---->"+ templateElegido, EIGlobal.NivelLog.DEBUG);
					evalTemplate (templateElegido,req, res);
				}
				else {
					EIGlobal.mensajePorTrace( "------------TOKEN INVALIDO---->", EIGlobal.NivelLog.INFO);
					boolLogin = false;
					if(usuarioBloqueado(usr)) {
						ValidaOTP.guardaRegistroBitacora(req,"Usuario bloqueado.");
						req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Usuario Bloqueado.\", 3);" );
						req.setAttribute ( "Fecha", ObtenFecha () );
						int resDup = cierraDuplicidad(session.getUserID8());
						sess.setAttribute("session", session);
						 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
								 EIGlobal.NivelLog.DEBUG);
						sess.invalidate();
						evalTemplate(IEnlace.LOGIN, req, res);
					}
					else {
						if(tok.getIntentos() >= 4){ //CSA TOKEN
							//llamar al WS de bloqueo
							WSBloqueoToken bloqueo = new WSBloqueoToken();
							String[] resultado = bloqueo.bloqueoToken(usr);
							//Realizar envio de notificaci�n
							EmailSender emailSender=new EmailSender();
							EmailDetails emailDetails = new EmailDetails();

							String numContrato = session.getUserID();
							String nombreContrato = session.getNombreUsuario();

							emailDetails.setNumeroContrato(numContrato);
							emailDetails.setRazonSocial(nombreContrato);

							emailSender.sendNotificacion(req,IEnlace.BLOQUEO_TOKEN_INTENTOS,emailDetails);
							//Guardar pista de auditoria
							ValidaOTP.guardaRegistroBitacora(req,"BTIF Bloqueo de token por intentos fallidos");
							//Realizar cierre de sesi�n y enviar alerta con mensaje de bloqueo por intentos

							BitaTCTBean beanTCT = new BitaTCTBean ();
							BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));

							beanTCT = bh.llenarBeanTCT(beanTCT);
							beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
							beanTCT.setTipoOperacion("BTIF");
							beanTCT.setCodError("BTIF0000");
							beanTCT.setOperador(session.getUserID8());
							beanTCT.setConcepto("Bloqueo de token por intentos fallidos");

							try {
								BitaHandler.getInstance().insertBitaTCT(beanTCT);
							}catch(Exception e) {
								EIGlobal.mensajePorTrace("BaseServlet - Error al insertar en bitacora intentos fallidos",EIGlobal.NivelLog.ERROR);

							}

							bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setIdFlujo("BTIF");
							bt.setNumBit("000000");
							bt.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
							bt.setIdToken(tok.getSerialNumber());

							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}

							try {
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace ("LoginServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
							}
							//Realizar cierre de sesi�n y enviar alerta con mensaje de bloqueo por intentos
							intentoFallido(tok);
							req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�per l�nea Empresarial para desbloquearlo.\", 3);" );
							req.setAttribute ( "Fecha", ObtenFecha () );
							int resDup = cierraDuplicidad(session.getUserID8());
							sess.setAttribute("session", session);
							 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServlet.java::token() -> resDup: " + resDup,
									 EIGlobal.NivelLog.DEBUG);
							sess.invalidate();
							evalTemplate(IEnlace.LOGIN, req, res);
							return true;
						}
						intentoFallido(tok);
						ValidaOTP.guardaRegistroBitacora(req,"Token no valido.");
						EIGlobal.mensajePorTrace( "------------TOKEN INVALIDO--REPITE-->", EIGlobal.NivelLog.DEBUG);
						req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Contrase&ntilde;a Incorrecta\", 3);" );
						req.setAttribute ( "Fecha", ObtenFecha () );
						sess.setAttribute("session", session);
						if(("1").equalsIgnoreCase(enrolamiento)){
							evalTemplate ( IEnlace.VALIDA_OTP_ENROLAMIENTO, req, res );
						}else{
							/**
							 * Inicio PYME 2015 Unificacion Token
							 */
							StringBuffer templateElegido = new StringBuffer( (String) req.getSession().getAttribute("plantilla") );
							EIGlobal.mensajePorTrace( "------------getParameter---->"+ req.getParameter(URLCANCELAR), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getAttribute---->"+ req.getAttribute(URLCANCELAR), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------templateElegido---->"+ templateElegido, EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter folio---->"+ req.getParameter(FOLIO), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter txtFav---->"+ req.getParameter(TXTFAV), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter strCheck---->"+ req.getParameter(STRCHECK), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter chkBoxFav---->"+ req.getParameter("chkBoxFav"), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter horario_seleccionado---->"+ req.getParameter(HORARIO_SELECCIONADO), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter HorarioPremier---->"+ req.getParameter("HorarioPremier"), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace( "------------getParameter Concepto---->"+ req.getParameter("Concepto"), EIGlobal.NivelLog.DEBUG);

							StringBuffer params = new StringBuffer("");
							if( req.getParameter(URLCANCELAR) != null && !"".equals(req.getParameter(URLCANCELAR)) ){
								if ( templateElegido.indexOf("/transferencia") >= 0 ||
										templateElegido.indexOf("/MDI_Enviar") >= 0
										|| templateElegido.indexOf("/Fiel") >= 0) {
									params.append( ValidaOTP.reestableceParametrosEnSessionEncoded( req) )
										.append( "encoded=true&" );
								} else if ( templateElegido.indexOf("/OpcionesNomina") >= 0 ||
										templateElegido.indexOf("/MMC_Alta") >= 0 ){
										try {
											Class.forName("mx.altec.enlace.dao.ConsultaCtasDAO");
											params.append( ValidaOTP.reestableceParametrosEnSession( req) );
											EIGlobal.mensajePorTrace("BaseServletBansera ", EIGlobal.NivelLog.DEBUG);
										} catch (ClassNotFoundException e) {
											EIGlobal.mensajePorTrace("BaseServlet BaseServlet", EIGlobal.NivelLog.DEBUG);
										}

								}
								if( templateElegido.indexOf("/MMC_Alta") >= 0 ){
									try {
											Class.forName("mx.altec.enlace.dao.ConsultaCtasDAO");
											templateElegido.append( "?Modulo=2&valida=" );
											EIGlobal.mensajePorTrace( "------------Modulo 2---->"+ templateElegido, EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getParameter(cuentasSel)---->"+ req.getParameter("cuentasSel"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getParameter(strTramaMB)---->"+ req.getParameter("strTramaMB"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getParameter(strTramaBN)---->"+ req.getParameter("strTramaBN"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getParameter(strTramaBI)---->"+ req.getParameter("strTramaBI"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getAttribute(cuentasSel)---->"+ req.getAttribute("cuentasSel"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getAttribute(strTramaMB)---->"+ req.getAttribute("strTramaMB"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getAttribute(strTramaBN)---->"+ req.getAttribute("strTramaBN"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace( "------------BaseServlet req.getAttribute(strTramaBI)---->"+ req.getAttribute("strTramaBI"), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace("BaseServletBansera ", EIGlobal.NivelLog.DEBUG);
										} catch (ClassNotFoundException e) {
											EIGlobal.mensajePorTrace("BaseServlet BaseServlet", EIGlobal.NivelLog.DEBUG);
										}
								} else {
									templateElegido.append( '?' ).append( params ).append( "valida=" );
								}
								EIGlobal.mensajePorTrace( "------------templateElegido---->"+ templateElegido, EIGlobal.NivelLog.DEBUG);
								req.setAttribute("challngeExito","");
								req.setAttribute(FOLIO, req.getParameter(FOLIO));
								req.setAttribute(TXTFAV, req.getParameter(TXTFAV));
								req.setAttribute(STRCHECK, req.getParameter(STRCHECK));
								req.setAttribute("chkBoxFav", req.getParameter("chkBoxFav"));
								req.setAttribute(HORARIO_SELECCIONADO, req.getParameter(HORARIO_SELECCIONADO));
								req.setAttribute("HorarioPremier", req.getParameter("HorarioPremier"));
								req.setAttribute("Concepto", req.getParameter("Concepto"));
								req.setAttribute("forzar", "si");
								evalTemplate (templateElegido.toString(),req, res);
							} else {
								evalTemplate ( IEnlace.VALIDA_OTP, req, res );
							}
							/**
							 * Fin PYME 2015 Unificacion Token
							 */
						}
					}
					return true;
				}
				return true;
			}
			} else {
				EIGlobal.mensajePorTrace("Token diferente de activo:: " + estatusToken + ", entra al else --> " , EIGlobal.NivelLog.INFO);

				if(estatusToken == -1){
					try{
				    	ValidaOTP.guardaRegistroBitacora(req, "Token Deshabilitado");
						req.setAttribute("MensajeLogin01",
								"cuadroDialogo(\"Error al validar el estatus del token, Por Favor, Intente mas Tarde.\", 3);");
						req.setAttribute("Fecha", ObtenFecha());
						int resDup = cierraDuplicidad(session.getUserID8());
						req.getSession().setAttribute("session", session);
						EIGlobal.mensajePorTrace(this.getClass().getName() + " - "
								+ "LoginServlet.java::token() -> resDup: " + resDup,
								EIGlobal.NivelLog.DEBUG);
						req.getSession().invalidate();
						evalTemplate(IEnlace.LOGIN, req, res);
			    	}catch(Exception e) {
		                EIGlobal.mensajePorTrace ("E->cierraSesionTokenInvalido->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
		                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			    	}
				}else{
					cierraSesionTokenBloqueado(session, req, res);
				}

				return true;
			}
		}catch(Exception e) {
                        EIGlobal.mensajePorTrace ("E->validaPeticion->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return false;
	}

	/**
	 * Metodo que direcciona hacia la pagina de SAM
	 * @param req
	 * @param res
	 * @throws IOException
	 */
    /*<VC proyecto="200710001" autor="JGR" fecha="17/05/2007" descripcin="IR PAGINA SAM">*/
	public void redirSAM(HttpServletRequest req, HttpServletResponse res)
		throws IOException{
		 String mensajeMostrar = recuperaMensajeCuadro(req);

		 try{
			 res.setContentType("text/html");
			 res.getWriter().print("<html>");
			 res.getWriter().print("<head>");

			 if (mensajeMostrar!=null && !mensajeMostrar.equals("")){
				 EIGlobal.mensajePorTrace("mensajeMostrar->" + mensajeMostrar , EIGlobal.NivelLog.DEBUG);
				 res.getWriter().print("<script language = \"JavaScript\" " +
				 		"SRC= \"/EnlaceMig/cuadroDialogo.js\"></script>");
				 res.getWriter().print("<script language = \"JavaScript\">\n" +
						 mensajeMostrar + "\n</script>");
			 }

			 res.getWriter().print("<script language = \"JavaScript\">" +
				"function logoutSAM(){\nwindow.location.href = '" + Global.REDIR_LOGOUT_SAM + "';\n}</script>");

			 res.getWriter().print("</head>");
			 res.getWriter().print("<body onLoad=\"window.setTimeout('logoutSAM()', 500);\">");
			 res.getWriter().print("</body>");
			 res.getWriter().print("</html>");
			 res.getWriter().flush();
			 res.getWriter().close();

		 }catch(IOException ioe){
                         EIGlobal.mensajePorTrace("EX EN REDIRSAM->" + ioe.getMessage() , EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.INFO);
		 }
		 return;
	}
	/**
	 * Metodo que direcciona hacia la pagina de Supernet
	 * CGH 16-11-2007
	 * @param req
	 * @param res
	 * @throws IOException
	 */
	public void redirSupernet(HttpServletRequest req, HttpServletResponse res)
		throws IOException{
		 String mensajeMostrar = recuperaMensajeCuadro(req);

		 try{
			 res.getWriter().print("<html>");
			 res.getWriter().print("<head>");

			 if (mensajeMostrar!=null && !mensajeMostrar.equals("")){
				 EIGlobal.mensajePorTrace("mensajeMostrar->" + mensajeMostrar , EIGlobal.NivelLog.DEBUG);
				 res.getWriter().print("<script language = \"JavaScript\" " +
				 		"SRC= \"/EnlaceMig/cuadroDialogo.js\"></script>");
				 res.getWriter().print("<script language = \"JavaScript\">\n" +
						 mensajeMostrar + "\n</script>");
			 }

			 res.getWriter().print("<script language = \"JavaScript\">" +
				"function logoutSuperNet(){\nwindow.parent.LOGUEADO = false;\nwindow.parent.location.href = '" + Global.URL_SUPERNET + "?cliente="+ convierteUsr7a8((String)req.getAttribute("snetuser")) + "&contrato=" + Global.PREF_CONTR_SNET_EMPR + req.getAttribute("snetcont") +"';\n}</script>");

			 res.getWriter().print("</head>");
			 res.getWriter().print("<body onLoad=\"window.setTimeout('logoutSuperNet()', 500);\">");
			 res.getWriter().print("</body>");
			 res.getWriter().print("</html>");
			 res.getWriter().flush();
			 res.getWriter().close();

		 }catch(IOException ioe){
                         EIGlobal.mensajePorTrace("EX EN REDIRSuperNet->" + ioe.getMessage() , EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.INFO);
		 }
	}
	/**
	 * Metodo que direcciona hacia la pagina de Supernet comercios
	 * JPH 05/06/2009
	 * @param req
	 * @param res
	 * @throws IOException
	 */
	public void redirSupernetComer(HttpServletRequest req, HttpServletResponse res)
		throws IOException{
		 String mensajeMostrar = recuperaMensajeCuadro(req);

		 try{
			 res.getWriter().print("<html>");
			 res.getWriter().print("<head>");

			 if (mensajeMostrar!=null && !mensajeMostrar.equals("")){
				 EIGlobal.mensajePorTrace("mensajeMostrar->" + mensajeMostrar , EIGlobal.NivelLog.DEBUG);
				 res.getWriter().print("<script language = \"JavaScript\" " +
				 		"SRC= \"/EnlaceMig/cuadroDialogo.js\"></script>");
				 res.getWriter().print("<script language = \"JavaScript\">\n" +
						 mensajeMostrar + "\n</script>");
			 }
			 res.getWriter().print("<script language = \"JavaScript\">" +
				"function logoutSuperNet(){\nwindow.parent.LOGUEADO = false;\nwindow.parent.location.href = '" + req.getAttribute("urlSnetComer") +"';\n}</script>");

			 res.getWriter().print("</head>");
			 res.getWriter().print("<body onLoad=\"window.setTimeout('logoutSuperNet()', 500);\">");
			 res.getWriter().print("</body>");
			 res.getWriter().print("</html>");
			 res.getWriter().flush();
			 res.getWriter().close();

		 }catch(IOException ioe){
                         EIGlobal.mensajePorTrace("EX EN REDIRSuperNet->" + ioe.getMessage() , EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace(new Formateador().formatea(ioe), EIGlobal.NivelLog.INFO);
		 }
	}

	/**
	 * Recupera mensaje cuadro.
	 *
	 * @param req the req
	 * @return the string
	 */
	public String recuperaMensajeCuadro(HttpServletRequest req){
		String mensaje = "";

		try{
			if(req.getSession().getAttribute("MensajeLogin01") != null &&
				req.getAttribute("MensajeLogin01") == null) {
				req.setAttribute("MensajeLogin01", req.getSession().
						getAttribute("MensajeLogin01"));
				req.getSession().removeAttribute("MensajeLogin01");
			}

			mensaje = (String)req.getAttribute("MensajeLogin01");

			if (mensaje!=null && !mensaje.equals("")){
				int cortar = mensaje.indexOf('(');

				if (cortar != -1){
					String mensajeAuxInicio = mensaje.substring(0, cortar);
					String mensajeAuxFin = mensaje.substring(cortar, mensaje.length());
					if (mensajeAuxInicio.trim().equals("cuadroDialogo")){
						mensaje = "cuadroDialogoSAM" + mensajeAuxFin;
					}
					if (mensajeAuxInicio.trim().equals("cuadroDialogoEspecial")){
						mensaje = "cuadroDialogoEspecialSAM" + mensajeAuxFin;
					}
					if (mensajeAuxInicio.trim().equals("cuadroDialogoEspecial2")){
						mensaje = "cuadroDialogoEspecial2SAM" + mensajeAuxFin;
					}
				}
			}
		}catch(Exception e){
			 EIGlobal.mensajePorTrace("EX EN RECUPERAR MENSAJE->"
					 + e.getMessage() , EIGlobal.NivelLog.ERROR);
		          EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return mensaje;
	}


	/**
	 * Recupera mensaje.
	 *
	 * @param req the req
	 * @return the string
	 */
	public String recuperaMensaje(HttpServletRequest req){
		String mensajeMostrar = "";

		String[] temp = {"a", "e", "i", "o", "u"};
		String[] temp2 = {"�", "�", "�", "�", "�"};

		try{
			if(req.getSession().getAttribute("MensajeLogin01") != null &&
				req.getAttribute("MensajeLogin01") == null) {
				req.setAttribute("MensajeLogin01", req.getSession().
						getAttribute("MensajeLogin01"));
				req.getSession().removeAttribute("MensajeLogin01");
			}

			String mensaje = (String)req.getAttribute("MensajeLogin01");

			if (mensaje!=null && !mensaje.trim().equals("")){
				String[] array = desentramaS(mensaje, '\"');
				mensajeMostrar = limpiaMensaje(array[1]);
				String cute = "";
				for (byte i = 0; i < temp.length; i++) {
					cute = "&".concat(temp[i]).concat("acute;");
					mensajeMostrar = replaceAll(mensajeMostrar, cute, temp2[i]);
				}
			}
		}catch(Exception e){
			 EIGlobal.mensajePorTrace("EX EN RECUPERAR MENSAJE->"
					 + e.getMessage() , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		return mensajeMostrar;
	}


	/**
	 * Limpia mensaje.
	 *
	 * @param mensaje the mensaje
	 * @return the string
	 */
	public static String limpiaMensaje(String mensaje){
		String cadenaReg = "";
		int i = 0;
		int aux = i;
		for (i = 0; i < mensaje.length(); i++) {
			if (mensaje.charAt(i) == '<'){
				cadenaReg = cadenaReg + mensaje.substring(aux, i-1);
				while(mensaje.charAt(i) != '>'){
					i = i+1;
				}
				aux = i+1;
			}
		}
		cadenaReg = cadenaReg + mensaje.substring(aux, i);
		return cadenaReg;
	}


	/**
	 * Replace all.
	 *
	 * @param arg0 the arg0
	 * @param arg1 the arg1
	 * @param arg2 the arg2
	 * @return the string
	 */
	private static String replaceAll(String arg0, String arg1, String arg2) {
		byte buffer = (byte)arg1.length();
		String temp = arg0;
		short actual = 0;
		short i;
		for (i = 0; i < arg0.length(); i++) {
			if (i - buffer >= 0) {
				if (arg0.substring(i - buffer, i).equals(arg1)) {
					if (temp.equals(arg0)) temp = "";
					temp += arg0.substring(actual, i - buffer) + arg2;
					actual = i;
					i += buffer;
				}
			}
		}
		if (!temp.equals(arg0)) {
			if (actual != arg0.length()) {
				temp += arg0.substring(actual);
			}
		}
		return temp;
	}
	/**
	 * Metodo estatico para la conversion del codigo de cliente de 8 a 7 posiciones.
	 * Soporta conversion desde el codigo de cliente 99999999 hasta el codigo 00000001.
	 *
	 * @param usr (String)con el codigo de cliente de 8 posiciones.
	 * @return String con el codigo de cliente convertido a 7 posiciones.
	 */
	public static String convierteUsr8a7(String usr) {
		String result = null;
		int codCliente;
		if(usr.length() == 7) {
			result = usr;
		} else if(usr.length() == 8) {

			if(usr.charAt(0) == '0') {
				result =  usr.substring(1);
			} else {
				String usuConv = usr.substring(0, 2);
				int usuConvInt = Integer.parseInt(usuConv);

				switch(usuConvInt) {
					case 10:	result = 'A' + usr.substring(2);
								break;
					case 11:	result = 'B' + usr.substring(2);
								break;
					case 12:	result = 'G' + usr.substring(2);
								break;
					case 13:	result = 'H' + usr.substring(2);
								break;
					case 14:	result = 'I' + usr.substring(2);
								break;
					case 15:	result = 'J' + usr.substring(2);
								break;
					case 16:	result = 'K' + usr.substring(2);
								break;
					case 17:	result = 'L' + usr.substring(2);
								break;
					case 18:	result = 'M' + usr.substring(2);
								break;
					case 19:	result = 'N' + usr.substring(2);
								break;
					case 20:	result = 'O' + usr.substring(2);
								break;
					case 21:	result = 'P' + usr.substring(2);
								break;
					case 22:	result = 'Q' + usr.substring(2);
								break;
					case 23:	result = 'R' + usr.substring(2);
								break;
					case 24:	result = 'T' + usr.substring(2);
								break;
					case 25:	result = 'U' + usr.substring(2);
								break;
					case 26:	result = 'V' + usr.substring(2);
								break;
					case 27:	result = 'W' + usr.substring(2);
								break;
					case 28:	result = 'X' + usr.substring(2);
								break;
					case 29:	result = 'Y' + usr.substring(2);
								break;
					case 30:	result = '_' + usr.substring(2);
								break;
					case 31:	result = '~' + usr.substring(2);
								break;
					case 32:	result = '#' + usr.substring(2);
								break;
					case 33:	result = '[' + usr.substring(2);
								break;
					default:
						if (isBetween(usuConvInt, 34, 99)) {
							codCliente = Integer.parseInt(usr);
							result = "]" + Integer.toString(codCliente, BASE).toUpperCase();
						} else {
							result = null;
						}
								break;
				}
			}
		}
		EIGlobal.mensajePorTrace("USUARIO CONVERTIDO 8->7:'" + result + "'", EIGlobal.NivelLog.DEBUG);
		return result;
	}
    /*<VC FIN>*/



	/**
     * Checa si un valor entero proporcionado se encuentra dentro de un rango definido.
     *
     * @param valor (int) con el valor a verificar entre el rango inicial y el rango final definido.
     * @param rangoIni (int) con el valor inicial del rango.
     * @param rangoFin (int) con el valor final del rango.
     * @return true, si el valor se encuentra dentro del rango proporcionado.
     */
    public static boolean isBetween(final int valor, final int rangoIni, final int rangoFin) {
		return rangoIni <= valor && valor <= rangoFin;
	}


    /*M?odo para obtener los mensajes por usuario y contrato     everis  27/06/2008     inicio */

    /**
     * Muestra mensaje.
     *
     * @param request the request
     * @param response the response
     * @return the string
     */
    public String muestraMensaje(HttpServletRequest request, HttpServletResponse response)

	{

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if(session == null) {
			return "Sesion expirada.";
		}

	    EIGlobal.mensajePorTrace( "***BaseServlet.java & Entrando a creacion de mensajes &", EIGlobal.NivelLog.DEBUG);

	 	String contrato    = session.getContractNumber ();
	 	String usuario     = session.getUserID8 ();
	 	String clavePerfil = session.getUserProfile ();

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

       	int salida=0;

        String tramaEntrada        = "";
        String tramaSalida         = "";
        String path_archivo        = "";
        String nombre_archivo      = "";
        String nom_archivo2 	   = "";
        String trama_salidaRedSrvr = "";
        String retCodeRedSrvr      = "";
        String registro            = "";
	    String strMensaje          = "";
	    String strMuestraMens	   = "";

  	    String texto = "";

	    StringBuffer encabezado          = new StringBuffer("");
	    StringBuffer strTabla            = new StringBuffer("");
	    StringBuffer tramaMensajes       = new StringBuffer("");

       	String medio_entrega  = "2EWEB";
       	String tipo_operacion = "MENS";

	   	String ultimoMensaje = "0";

       	encabezado.append( medio_entrega );
	   	encabezado.append("|" );
	   	encabezado.append(usuario );
	   	encabezado.append("|" );
	   	encabezado.append(tipo_operacion );
	   	encabezado.append("|" );
	   	encabezado.append(contrato );
	   	encabezado.append("|");
       	encabezado.append(usuario );
	   	encabezado.append("|" );
	   	encabezado.append(clavePerfil );
	   	encabezado.append("|");

       	tramaEntrada  = encabezado.toString() + ultimoMensaje;

	   	EIGlobal.mensajePorTrace( "***BaseServlet.java >> tramaEntrada MENS: " + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);


	   	try{
			 Hashtable hs = tuxGlobal.web_red(tramaEntrada);
		   	 tramaSalida = (String) hs.get("BUFFER");
		  	 EIGlobal.mensajePorTrace( "***BaseServlet.java trama_salidaRedSrvr >>" + tramaSalida + "<<", EIGlobal.NivelLog.DEBUG);
	    }catch( java.rmi.RemoteException re ){
	    	EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
	    } catch(Exception e) {
	    	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	    }


	    EIGlobal.mensajePorTrace( "***BaseServlet.java >> tramaSalida MENS:" + tramaSalida + " <<", EIGlobal.NivelLog.DEBUG);

	    path_archivo = tramaSalida;
        nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());


	    EIGlobal.mensajePorTrace( "***BaseServlet.java & nombre_archivo MENS:" + nombre_archivo + " &", EIGlobal.NivelLog.DEBUG);

        nom_archivo2 = nombre_archivo + "mens";


	    EIGlobal.mensajePorTrace( "***BaseServlet.java & nombre_archivo MENS:" + nom_archivo2 + " &", EIGlobal.NivelLog.DEBUG);

		ArchivoRemoto archR = new ArchivoRemoto();
		if(!archR.copia(nom_archivo2))
		{
			EIGlobal.mensajePorTrace( "***BaseServlet.java & paginaPrincipal: No se realizo la copia remota. &", EIGlobal.NivelLog.ERROR);
			strMensaje = "No se pudo obtener el archivo de mensajes. Intente m&aacute;s tarde.";
		}
		else
		 {
	        boolean errorFile=false;
            BufferedReader tmpFile = null;
	        try
		     {
				EIGlobal.mensajePorTrace( "***BaseServlet.java & paginaPrincipal: Copia remota OK. &", EIGlobal.NivelLog.DEBUG);

				path_archivo = Global.DIRECTORIO_LOCAL + "/" + nom_archivo2;
				EIGlobal.mensajePorTrace( "***BaseServlet.java & path_archivo MENS:" + nom_archivo2 + " &", EIGlobal.NivelLog.DEBUG);

		        File drvFile = new File(path_archivo);
			    tmpFile = new java.io.BufferedReader(new java.io.FileReader(drvFile));

				trama_salidaRedSrvr = tmpFile.readLine();
				EIGlobal.mensajePorTrace( "******BaseServlet.java trama_salidaRedSrvr MENS: &" + trama_salidaRedSrvr + "&", EIGlobal.NivelLog.INFO);


				if(trama_salidaRedSrvr!=null){

					int numMensajes = EIGlobal.CuantosTokens (trama_salidaRedSrvr, '|');

					EIGlobal.mensajePorTrace ( "***BaseServlet.java & registro MENS : >" + numMensajes + "<", EIGlobal.NivelLog.DEBUG);

					for(int i=0; i <= numMensajes; i++ )
					{
						texto = EIGlobal.BuscarToken (trama_salidaRedSrvr, '|', i+1);
						EIGlobal.mensajePorTrace ( "***BaseServlet.java & registro MENS : >" + texto + "<", EIGlobal.NivelLog.DEBUG);
						strMensaje += "   <tr><td class=textabdatobs align= center ><br>" + texto + "</td></tr>";

					}

					strMuestraMens = strMensaje;

					EIGlobal.mensajePorTrace ("***BaseServlet.java - muestra mensaje -> El mensaje a mostrar es: <" + strMuestraMens + ">", EIGlobal.NivelLog.DEBUG);
				}

				tmpFile.close();

			 }
			 catch(Exception e)
               {           EIGlobal.mensajePorTrace ("E->muestraMensaje->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
               }finally{
                   if(null != tmpFile){
                       try{
                           tmpFile.close();
                       }catch(java.io.IOException ex){
                    	   EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
                       }
                       tmpFile = null;
                   }
               }
			return strMuestraMens;
		 }

		 EIGlobal.mensajePorTrace ( "***BaseServlet.class & Saliendo de obtener Mensajes() &", EIGlobal.NivelLog.DEBUG);

        return strMuestraMens;

    }


    /*M?odo para obtener los mensajes por usuario y contrato     everis  27/06/2008     fin    */

    /**
     * Descripcion: metodo para leer parametros de formularios multipart
     *
     * @param request the request
     * @param parametro the parametro
     * @return the form parameter
     */
    public String getFormParameter(HttpServletRequest request, String parametro)
    {
    	HashMap<String,String> formParameters  = null;
    	String value = "";

    	if(!ServletFileUpload.isMultipartContent(request) || request.getParameter(parametro) != null)
    	{
    		return request.getParameter(parametro);
    	}

    	if( request.getAttribute("formParameters")== null)
    	{
    		List parametros = null;
    		Iterator it;
    		FileItem fileItem;
    		String sFile, sFileBk;
    		boolean errorEnZip = false;
			String nombreArchivo;


    		ServletFileUpload sfp = new ServletFileUpload(new DiskFileItemFactory());

    		try {
    			parametros = sfp.parseRequest(request);

				it = parametros.iterator();
				formParameters = new HashMap<String,String>();
				while(it.hasNext())
				{
					fileItem = (FileItem)it.next();

					if(fileItem.isFormField())
					{
						EIGlobal.mensajePorTrace("campo<" + fileItem.getFieldName() + "> valor <" + fileItem.getString() + ">",EIGlobal.NivelLog.DEBUG);
						Object obj = null;
						obj = formParameters.get(fileItem.getFieldName());
						if (obj == null)
							formParameters.put(fileItem.getFieldName(),fileItem.getString());
					}else
					{
						String fileNameBk = obtenNombreArchivo(fileItem.getName());
						String fileName = fileNameBk;
						if (!"".equals(fileName)) {

							// construimos un objeto file para recuperar el
							// trayecto completo
							//File fichero = new File(fileName);

							//EIGlobal.mensajePorTrace("El nombre del fichero es " + fichero.getName(), EIGlobal.NivelLog.DEBUG);

							// nos quedamos solo con el nombre y descartamos el
							// path
							File fichero = new File(IEnlace.LOCAL_TMP_DIR + "/" + fileName);

							for(int x = 1;fichero.exists() && x <= 1000; x++) {	//para generar nombre de archivo distinto si existe
								fileName = "" + x + fileNameBk;
								fichero = new File(IEnlace.LOCAL_TMP_DIR + "/" + fileName);
							}

							EIGlobal.mensajePorTrace("El nombre del archivo es " + fichero.getName(), EIGlobal.NivelLog.INFO);


							// escribimos el fichero colgando del nuevo path
							fileItem.write(fichero);

							sFile = fileName;
							if (sFile.toLowerCase().endsWith(".zip")) {
								// EIGlobal.mensajePorTrace("EI_Importar -
								// importaArchivo():El archivo viene en zip",
								// EIGlobal.NivelLog.INFO);

								final int BUFFER = 2048;
								BufferedOutputStream dest = null;
								FileInputStream fis = new FileInputStream(
										IEnlace.LOCAL_TMP_DIR + "/" + sFile);
								ZipInputStream zis = new ZipInputStream(
										new BufferedInputStream(fis));
								ZipEntry entry;
								entry = zis.getNextEntry();

								// EIGlobal.mensajePorTrace("EI_Importar -
								// importaArchivo():entry: "+entry,
								// EIGlobal.NivelLog.INFO);
								if (entry == null) {
									// EIGlobal.mensajePorTrace("EI_Importar -
									// importaArchivo():Entry fue null, archivo
									// no es zip o esta da�ando ",
									// EIGlobal.NivelLog.INFO);
									errorEnZip = true;
								} else {
									// EIGlobal.mensajePorTrace( "EI_Importar -
									// importaArchivo():Extrayendo " + entry,
									// EIGlobal.NivelLog.INFO);

									int count;
									byte data[] = new byte[BUFFER];

									// Se escriben los archivos a disco
									// Nombre del archivo zip
									File archivocomp = new File(
											IEnlace.LOCAL_TMP_DIR + "/" + sFile);

									// Nombre del archivo que incluye el zip
									sFile = entry.getName();
									sFileBk = sFile;

									File archivocomp2 = new File(
											IEnlace.LOCAL_TMP_DIR + "/" + sFile);


									for(int x = 1;archivocomp2.exists() && x <= 1000; x++) {	//para generar nombre de archivo distinto si existe
										sFile = "" + x + sFileBk;
										archivocomp2 = new File(IEnlace.LOCAL_TMP_DIR + "/" + sFile);
									}



									FileOutputStream fos = new FileOutputStream(
											IEnlace.LOCAL_TMP_DIR + "/" + sFile);
									dest = new BufferedOutputStream(fos, BUFFER);
									while ((count = zis.read(data, 0, BUFFER)) != -1)
										dest.write(data, 0, count);
									dest.flush();
									dest.close();

									// Borra el archivo comprimido
									if (archivocomp.exists())
										archivocomp.delete();
									// EIGlobal.mensajePorTrace( "EI_Importar -
									// importaArchivo():Fin extraccion de: " +
									// entry, EIGlobal.NivelLog.INFO);
								}
								zis.close();
							}

							if (!errorEnZip) {
								nombreArchivo = IEnlace.LOCAL_TMP_DIR + "/"
										+ sFile;
							} else {
								nombreArchivo = "ZIPERROR";
							}

							formParameters.put("fileNameUpload", nombreArchivo);
							formParameters.put("fileName", sFile);
						}
					}
				}
				request.setAttribute("formParameters",formParameters);
			} catch (FileUploadException e) {
	                       EIGlobal.mensajePorTrace ("E->getFormParameter->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
	                        EIGlobal.mensajePorTrace ("E->getFormParameter->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
    	}else
    	{
    		formParameters = (HashMap<String,String>) request.getAttribute("formParameters");
    	}
    	value = formParameters != null ? formParameters.get(parametro) : null;
    	EIGlobal.mensajePorTrace("getFormParameters <" + parametro+ "><" + value +  ">",EIGlobal.NivelLog.DEBUG);
    	return ( value);
    }

    /**
     * Gets the form parameter2.
     *
     * @param request the request
     * @param parametro the parametro
     * @return the form parameter2
     */
    public String getFormParameter2(HttpServletRequest request, String parametro)
    {
    	HashMap<String,String> formParameters  = null;
    	String value = null;
    	formParameters = (HashMap<String,String>) request.getAttribute("formParameters");
    	value = formParameters != null ? formParameters.get(parametro) : null;
    	EIGlobal.mensajePorTrace("getFormParameters2 <" + parametro+ "><" + value +  ">",EIGlobal.NivelLog.DEBUG);
    	return ( value);

    }

    /**
     * Obten nombre archivo.
     *
     * @param nombreArchivo the nombre archivo
     * @return the string
     */
    private String obtenNombreArchivo(String nombreArchivo)
	{
		String nombreAux = "";
		if(nombreArchivo == null)
			return "";
		//Remplazamos diagonales por diagonales inversas
		nombreAux = nombreArchivo.replace('/','\\');
		if(nombreAux.lastIndexOf('\\') >= 0)
		{
			nombreAux = nombreAux.substring(nombreArchivo.lastIndexOf('\\')+1);
		}
		EIGlobal.mensajePorTrace("Nombre Archivo " + nombreAux, EIGlobal.NivelLog.DEBUG);
		return nombreAux.toLowerCase();
	}


    /*M?odo para obtener los mensajes por usuario y contrato     everis  27/06/2008     fin    */

    /**
     * Descripcion: metodo para leer parametros de formularios multipart
     *
     * @param request the request
     * @return the form parameters
     */
    public HashMap<String,String> getFormParameters(HttpServletRequest request)
    {
    	HashMap<String,String> formParameters  = null;
    	String value = "";


    	if( request.getAttribute("formParameters")== null)
    	{
    		List parametros = null;
    		Iterator it;
    		FileItem fileItem;
    		String sFile;
    		boolean errorEnZip = false;
			String nombreArchivo;


    		ServletFileUpload sfp = new ServletFileUpload(new DiskFileItemFactory());

    		try {
    			parametros = sfp.parseRequest(request);

				it = parametros.iterator();
				formParameters = new HashMap<String,String>();
				while(it.hasNext())
				{
					fileItem = (FileItem)it.next();

					if(fileItem.isFormField())
					{
						EIGlobal.mensajePorTrace("campo<" + fileItem.getFieldName() + "> valor <" + fileItem.getString() + ">",EIGlobal.NivelLog.DEBUG);
						formParameters.put(fileItem.getFieldName(),fileItem.getString());
					}else
					{

						String fileName = obtenNombreArchivo(fileItem.getName());
						if (!"".equals(fileName)) {

							// construimos un objeto file para recuperar el
							// trayecto completo
							File fichero = new File(fileName);
							EIGlobal.mensajePorTrace("El nombre del fichero es " + fichero.getName(), EIGlobal.NivelLog.DEBUG);

							// nos quedamos solo con el nombre y descartamos el
							// path
							fichero = new File(IEnlace.LOCAL_TMP_DIR + "/"
									+ fichero.getName());

							// escribimos el fichero colgando del nuevo path
							fileItem.write(fichero);

							sFile = fileName;
							if (sFile.toLowerCase().endsWith(".zip")) {
								// EIGlobal.mensajePorTrace("EI_Importar -
								// importaArchivo():El archivo viene en zip",
								// EIGlobal.NivelLog.INFO);

								final int BUFFER = 2048;
								BufferedOutputStream dest = null;
								FileInputStream fis = new FileInputStream(
										IEnlace.LOCAL_TMP_DIR + "/" + sFile);
								ZipInputStream zis = new ZipInputStream(
										new BufferedInputStream(fis));
								ZipEntry entry;
								entry = zis.getNextEntry();

								// EIGlobal.mensajePorTrace("EI_Importar -
								// importaArchivo():entry: "+entry,
								// EIGlobal.NivelLog.INFO);
								if (entry == null) {
									// EIGlobal.mensajePorTrace("EI_Importar -
									// importaArchivo():Entry fue null, archivo
									// no es zip o esta da�ando ",
									// EIGlobal.NivelLog.INFO);
									errorEnZip = true;
								} else {
									// EIGlobal.mensajePorTrace( "EI_Importar -
									// importaArchivo():Extrayendo " + entry,
									// EIGlobal.NivelLog.INFO);

									int count;
									byte data[] = new byte[BUFFER];

									// Se escriben los archivos a disco
									// Nombre del archivo zip
									File archivocomp = new File(
											IEnlace.LOCAL_TMP_DIR + "/" + sFile);
									// Nombre del archivo que incluye el zip
									sFile = entry.getName();

									FileOutputStream fos = new FileOutputStream(
											IEnlace.LOCAL_TMP_DIR + "/" + sFile);
									dest = new BufferedOutputStream(fos, BUFFER);
									while ((count = zis.read(data, 0, BUFFER)) != -1)
										dest.write(data, 0, count);
									dest.flush();
									dest.close();

									// Borra el archivo comprimido
									if (archivocomp.exists())
										archivocomp.delete();
									// EIGlobal.mensajePorTrace( "EI_Importar -
									// importaArchivo():Fin extraccion de: " +
									// entry, EIGlobal.NivelLog.INFO);
								}
								zis.close();
							}

							if (!errorEnZip) {
								nombreArchivo = IEnlace.LOCAL_TMP_DIR + "/"
										+ sFile;
							} else {
								nombreArchivo = "ZIPERROR";
							}

							formParameters.put("fileNameUpload", nombreArchivo);
							formParameters.put("fileName", sFile);
						}
					}
				}
				request.setAttribute("formParameters",formParameters);
			} catch (FileUploadException e) {
                                EIGlobal.mensajePorTrace ("E->getFormParameters->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
                                  EIGlobal.mensajePorTrace ("E->getFormParameters->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
    	}else
    	{
    		formParameters = (HashMap<String,String>) request.getAttribute("formParameters");
    	}
    	return ( formParameters);
    }

    /**
     * M�todo que realiza guarda en la tabla SNTC_SESION la informaci�n al momento de hacer el salto de Enlace
     * a Supernet Comercios
     * @param sessionId the session id
     * @param usuario the usuario
     * @param nombreUsuario the nombre usuario
     * @param contrato the contrato
     * @param canal the canal
     * @param cuenta the cuenta
     * @param cuentas the cuentas
     * @param esAdmin the es admin
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean insertaDatosSnetComercios(String sessionId, String usuario, String nombreUsuario, String contrato, String canal, String cuenta, String cuentas, int esAdmin) throws Exception {
    	EIGlobal.mensajePorTrace("iniciando en insertaDatosSnetComercios()",EIGlobal.NivelLog.DEBUG);
    	boolean exito = false;
    	Connection conn = null;
    	try {

    		//int esAdmin=0; variable utilizada para insertar en el campo ES_ADMIN posiblemente este valor vaya a ser dinamico para hacer la modificacion.
    		conn = createConnSnetComercios(Global.DATA_SOURCE_SNET_COMER);
			EIGlobal.mensajePorTrace("CONEXION ABIERTA",EIGlobal.NivelLog.DEBUG);



			StringBuffer query = new StringBuffer("");
			query.append("INSERT INTO SNTC_SESION VALUES (");
			query.append("'");//abre comilla
			query.append(sessionId); //CAMPO ID_SESION
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(usuario);//CAMPO COD_CLIENTE
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(cuenta);//CAMPO NUM_CUENTA
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(IEnlace.CANAL_ENLACE);//CAMPO CANAL
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(contrato);//CAMPO CONTRATO
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(cuentas);//CAMPO CUENTAS
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(esAdmin);//CAMPO ES_ADMIN
			query.append("',");//cierra comilla
			query.append("'");//abre comilla
			query.append(nombreUsuario);//CAMPO NOMBRE_CLIENTE
			query.append("')");//cierra comilla y sentencia
			EIGlobal.mensajePorTrace("SE EJECUTA EL INSERT ----> " + query.toString(),EIGlobal.NivelLog.DEBUG);


			CallableStatement cstm = conn.prepareCall(query.toString());//preparamos la sentencia
			cstm.setQueryTimeout(Integer.parseInt(Global.TIMEOUT_DATASOURCE));//asignamos timeout a la peticion.
			int result = cstm.executeUpdate(); //ejecutamos la sentencia
			//int resultado = stm.executeUpdate(query.toString());
			EIGlobal.mensajePorTrace("SE EJECUTO EL QUERY CON RESULTADO ----> " + result,EIGlobal.NivelLog.DEBUG);
			if(result==1){
				exito=true;
			}
			else {
				EIGlobal.mensajePorTrace("NO SE INSERTARON REGISTROS, result: " + result,EIGlobal.NivelLog.ERROR);
			}

    	}catch (SQLException sqle){
    		EIGlobal.mensajePorTrace("E AL EJECUTAR EL QUERY " + sqle,EIGlobal.NivelLog.ERROR);
    		EIGlobal.mensajePorTrace("E AL EJECUTAR EL QUERY " + sqle.getMessage(),EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
    		throw new Exception(sqle.getMessage());
    	}catch (NamingException nme){
    		EIGlobal.mensajePorTrace("E AL CONECTARSE CON EL DATA SOURCE " + nme.getMessage(),EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace(new Formateador().formatea(nme), EIGlobal.NivelLog.INFO);
    		throw new Exception(nme.getMessage());
    	} catch (Exception e){
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace("SE GENERO UN E AL CREAR EL REGISTRO " + e.getMessage(),EIGlobal.NivelLog.ERROR);
    		throw new Exception(e.getMessage());
    	} finally{
			if(conn!=null){
				EIGlobal.mensajePorTrace("CERRANDO CONEXION",EIGlobal.NivelLog.DEBUG);
				try {
					conn.close();
				} catch (SQLException e) {
                                         EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("SE GENERO UN E AL CERRRAR LA CONEXION " + e.getMessage(),EIGlobal.NivelLog.DEBUG);
					throw new Exception(e.getMessage());
				}
				EIGlobal.mensajePorTrace("CONEXION CERRADA",EIGlobal.NivelLog.DEBUG);
			}
		}


    	return exito;

    }

     /**
     * M�todo que obtiene todas las cuentas integrales. No por paginacion.
      * @param total the total
      * @param archivo the archivo
      * @return the string[][]
     */
    /*JPH VSWF 31/07/2009 se agrega para corregir incidente GCS*/
    public String[][] ObteniendoTodasCtasInteg (int total,String archivo) {
    	EIGlobal.mensajePorTrace ("***Entrando a  ObteniendoTodasCtasInteg ", EIGlobal.NivelLog.INFO);

    	String[][] arrcuentas =null;
    	String datos[] =null;
    	String linea = "";
    	int separadores=0;
    	BufferedReader entrada;
    	int indice=1;
    	int contador=0;
    	arrcuentas = new String[total+1][9];
    	arrcuentas[0][1] = "NUM_CUENTA";
    	arrcuentas[0][2] = "TIPO_RELAC_CTAS";
    	arrcuentas[0][3] = "TIPO_CUENTA";
    	arrcuentas[0][4] = "NOMBRE_TITULAR";
    	arrcuentas[0][5] = "CUENTA_HOGAN";
    	arrcuentas[0][6] = "FM";
    	arrcuentas[0][7] = "CVE_PRODUCTO_S";
    	arrcuentas[0][8] = "CVE_SUBPRODUCTO_S";
    	FileReader lee = null;
    	try {
    		lee = new FileReader (archivo);
    		entrada = new BufferedReader ( lee );
    		while ( ( linea = entrada.readLine () ) != null ) {
    			separadores=cuentaseparadores (linea);
    			datos = desentramaC (""+separadores+"@"+linea,'@');
    			if(separadores!=5) {
    				arrcuentas[indice][0]="8";
    				arrcuentas[indice][1]=datos[1]; //numero cta
    				//EIGlobal.mensajePorTrace("**1 "+datos[1] , EIGlobal.NivelLog.INFO);
    				arrcuentas[indice][2]=datos[8]; //propia o de terceros
    				//EIGlobal.mensajePorTrace("**8 "+datos[8] , EIGlobal.NivelLog.INFO);
    				arrcuentas[indice][3]=datos[7]; //tipo cta
    				//EIGlobal.mensajePorTrace("**7 "+datos[7] , EIGlobal.NivelLog.INFO);
    				if (datos[2].equals ("NINGUNA")) {
    					arrcuentas[indice][4]=datos[3]; //descripcion
    				} else {
    					arrcuentas[indice][4]=datos[2]+"  "+datos[3];
    				}
    				//EIGlobal.mensajePorTrace("**3 "+datos[3] , EIGlobal.NivelLog.INFO);
    				if (datos[2].equals ("NINGUNA")) {
    					arrcuentas[indice][5]="";
    				} else {
    					arrcuentas[indice][5]=datos[2]; //cuenta hogan
    				}
    				//EIGlobal.mensajePorTrace("**2 "+datos[2] , EIGlobal.NivelLog.INFO);
    				arrcuentas[indice][6]=datos[6]; //FM
    				//EIGlobal.mensajePorTrace("**6 "+datos[6] , EIGlobal.NivelLog.INFO);
    				arrcuentas[indice][7]=datos[4]; //FM
    				//EIGlobal.mensajePorTrace("**4 "+datos[4] , EIGlobal.NivelLog.INFO);
    				arrcuentas[indice][8]=datos[5];
    				//EIGlobal.mensajePorTrace("**5 "+datos[5] , EIGlobal.NivelLog.INFO);
    			}
    			else //interbancarios
    			{
    				arrcuentas[indice][1]=datos[1];
    				arrcuentas[indice][2]=datos[2];
    				arrcuentas[indice][3]=datos[3];
    				arrcuentas[indice][4]=datos[4];
    				arrcuentas[indice][5]=datos[5];
    			}
    			indice++;

    		}//while
    		entrada.close ();
    	} catch ( Exception e ) {
    		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "Error al traer cuentas" + e.toString (),
    				EIGlobal.NivelLog.ERROR);
    		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    	} finally {
    		try{
    			lee.close();
    		}catch(Exception e) {
    		}
    	}
    	indice--;
    	arrcuentas[0][0] = ""+indice;
    	EIGlobal.mensajePorTrace ("**Sali del arrcuentas = >" +arrcuentas[0][0]+ "<", EIGlobal.NivelLog.DEBUG);
    	return arrcuentas;
    }

	/**
	 * Metodo encargado de consultar los comercios para el Administrador
	 *
	 * @param  cuentasEnlace 	Cuentas Enlace
	 * @param  total			Total de Cuentas Enlace
	 * @return comerciosAdmin 	Cuentas de Supernet Comercios que aplican para el Administrador
	 */
	public Vector getComerciosAdmin(String cuentasEnlace) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		Vector comerciosAdmin = new Vector();

		try {
			conn = createConnSnetComercios(Global.DATA_SOURCE_SNET_COMER);
			sDup = conn.createStatement();
			query = "Select a.num_cuenta as numCuenta "
				  + "From sntc_com a "
				  + "Where a.num_cuenta in(" + cuentasEnlace + ")";

			EIGlobal.mensajePorTrace ("BaseServlet::getComerciosAdmin:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			while(rs.next()){
				comerciosAdmin.add(rs.getString("numCuenta")!=null?rs.getString("numCuenta"):"");
			}

			EIGlobal.mensajePorTrace ("BaseServlet::getComerciosAdmin:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
                        EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace ("BaseServlet::getComerciosAdmin:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}catch (NamingException nme){
                        EIGlobal.mensajePorTrace(new Formateador().formatea(nme), EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace("BaseServlet::getComerciosAdmin:: Error al conectarse con el datasource" + nme.getMessage(),EIGlobal.NivelLog.DEBUG);
    	} catch (Exception e){
                   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace("BaseServlet::getComerciosAdmin:: Se genero un error al consultar comercios para Administrador" + e.getMessage(),EIGlobal.NivelLog.DEBUG);
    	}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
			}
		}
		return comerciosAdmin;
    }

    /**
     * Validar xss.
     *
     * @param cad the cad
     * @return true, if successful
     */
    public boolean validarXSS(String cad){
		String cadTemp=cad.toLowerCase();
		System.out.println(cadTemp);
		Pattern patron=Pattern.compile("<\\s*script");
		Matcher m=patron.matcher(cadTemp);
		return m.find();


    }

    /**
     * Validar parametos contienen xss.
     *
     * @param req the req
     * @return true, if successful
     */
    public boolean validarParametosContienenXSS(HttpServletRequest req){
		Enumeration nombresParametros=req.getParameterNames();
		String  valorParametro="";
		try{
			while(nombresParametros.hasMoreElements()){
				String nombreParametro = (String)nombresParametros.nextElement();
				String[] valoresParametro =req.getParameterValues(nombreParametro);
				for(int i=0; i<valoresParametro.length; i++) {
						valorParametro = valoresParametro[i];
						if (!(valorParametro.length() == 0)){
							if(validarXSS(valorParametro))
							{
								return true;
							}
						}
					}

			}
		}catch(Exception e){
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    		EIGlobal.mensajePorTrace("BaseServlet::validarParametosContienenXSS:: atencion a parametrosXSS se genera el mensaje" + e.getMessage(),EIGlobal.NivelLog.DEBUG);
		}
		return false;
    }

    	/**
		 * Metodo para validar la sessi�n del micrositio y validar el token para clientes unicontrato
		 * @param request			Resquest de la operaci�n
		 * @param response			Response de la operaci�n
		 * @param numContrato		Numero de contrato
		 * @return 					true  - Si la operaci�n ya es segura y validada
		 * 							false - Si la operaci�n debe ser truncada
	     * @throws ServletException the servlet exception
	     * @throws IOException Signals that an I/O exception has occurred.
		 */
		private int ValidaSesionMicrositio( HttpServletRequest request, HttpServletResponse response, String numContrato )
	    throws ServletException, java.io.IOException{
			HttpSession sess;
			int resultado = 0;
			sess = request.getSession (false);
			mx.altec.enlace.bo.BaseResource session = null;
			EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 1: " + resultado , EIGlobal.NivelLog.DEBUG);
			if(sess == null){
				resultado = 0;
				EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 2: " + resultado , EIGlobal.NivelLog.DEBUG);
			}else{
				session = (BaseResource)request.getSession().getAttribute("session");
				EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 3: " + resultado , EIGlobal.NivelLog.DEBUG);
				if(session == null || session.getUserID8 () == null || session.getUserID8 ().equals ("")){
					resultado = 0;
					EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 4: " + resultado , EIGlobal.NivelLog.DEBUG);
				}else{
					EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 5: " + resultado , EIGlobal.NivelLog.DEBUG);
				 	String origen = request.getServletPath();
				 	int statusToken = session.getToken().getStatus();
				 	EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: Status Token: " + statusToken , EIGlobal.NivelLog.DEBUG);
				 	EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: Validar Token: " + session.getValidarToken() , EIGlobal.NivelLog.DEBUG);
				 	EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: Token Validado2: " + session.getTokenValidado() , EIGlobal.NivelLog.DEBUG);
				 	if(session.getValidarToken() &&
				 	  !session.getTokenValidado() /*&&
				 	   session.getFacultad(session.FAC_VAL_OTP) MSD TEMPORALMENTE*/) {
				 		switch(statusToken) {
				 			case 0:		//activaci�n
				 					request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su token ya le fue entregado pero no est� activo. Por favor f�rmese en Enlace por Internet para activarlo e intente nuevamente.\", 1);");
				 					evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);
				 				   	return 2;
				 			case 1:		//validaci�n
				 					guardaParamMicrositioUnicontrato(request, numContrato);
				 					evalTemplate(IEnlace.TOKEN_MICROSITIO, request, response);
				 					return 2;
				 			case 2:		//bloqueado
				 					request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 1);");
				 					evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);
				 					return 2;
							case 4:		//bloqueado por inactividad
									request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado temporalmente por inactividad, favor de llamar a nuestra S&uacute;per l&iacute;nea Empresarial para desbloquearlo .\", 1);");
									evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);
									return 2;
				 			default://RFC100001001099 Validacion de estatus diferentes a ENTREGADO,ACTIVO BLOQUEADO
				 					request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token es en un estatus que no le permite operar en Enlace Internet.\", 1);");
		 							evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);
		 							return 2;

				 		}
					}
					resultado = 1;
					EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 6: " + resultado , EIGlobal.NivelLog.DEBUG);
				}
				EIGlobal.mensajePorTrace("BaseServlet::ValidaSesionMicrositio:: resultado 7: " + resultado , EIGlobal.NivelLog.DEBUG);
			}
			if(session != null){
				sess.setAttribute ("session", session);
				resultado = 1;
			}
			return resultado;
		}

		/**
		 * Metodo para guardar parametros en session
		 * @param request			Request de la operaci�n
		 * @param numContrato		Numero de contrato
		 */
	    private void guardaParamMicrositioUnicontrato(HttpServletRequest request, String numContrato){
			EIGlobal.mensajePorTrace("-333333aaaaa---------Interrumple el flujo--",EIGlobal.NivelLog.INFO);
			//String template= request.getServletPath();
			String template=	"/enlaceMig/CuentaCargoMicrositio";
			EIGlobal.mensajePorTrace("5-****--------------->template**********--->"+template,EIGlobal.NivelLog.INFO);
			request.getSession().setAttribute("plantilla",template);
			HttpSession session = request.getSession(false);
			if(session != null){
				Map tmp = new HashMap();
				Enumeration enumer = request.getParameterNames();
				EIGlobal.mensajePorTrace("6665555-****--------------->enumer**********--->"+request.getParameter( "valida" ),EIGlobal.NivelLog.INFO);
				while(enumer.hasMoreElements()){
					String name = (String)enumer.nextElement();
					EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name),EIGlobal.NivelLog.INFO);
					tmp.put(name,request.getParameter(name));
				}
				EIGlobal.mensajePorTrace("setteando CONTRATO->"+numContrato,EIGlobal.NivelLog.INFO);
				session.setAttribute("contratoBTIF", numContrato);

			EIGlobal.mensajePorTrace("lstContratos----valor-------"	+ numContrato, EIGlobal.NivelLog.INFO);
			tmp.put("lstContratos", numContrato);

			// guarda parametros SAT para unicontrato

			if ("PMRF".equals(request.getAttribute("servicio_id"))) {
				tmp.put("importe", request.getAttribute("importe"));
				tmp.put("linea_captura", request.getAttribute("linea_captura"));
				tmp.put("convenio", request.getAttribute("convenio"));
				tmp.put("servicio_id", request.getAttribute("servicio_id"));
			}
				session.setAttribute("parametros",tmp);
				tmp = new HashMap();
				enumer = request.getAttributeNames();
				while(enumer.hasMoreElements()){
					String name = (String)enumer.nextElement();
					tmp.put(name,request.getAttribute(name));
				}
				session.setAttribute("atributos",tmp);
			}

	}
	    /**
	     * Metodo para cerrar la sesi�n cuando se valida que el Token esta en estatus bloqueado
	     * @param session objeto del tipo BaseResource.
	     * @param request objeto del tipo HttpServletRequest.
	     * @param response objeto del tipo HttpServletResponse
	     */
	    public void cierraSesionTokenBloqueado(BaseResource session, HttpServletRequest request,
				HttpServletResponse response){
	    	try{
	    		EIGlobal.mensajePorTrace("Entra a cierraSesionTokenBloqueado ->",EIGlobal.NivelLog.INFO);
		    	ValidaOTP.guardaRegistroBitacora(request, "Token Deshabilitado");
				request.setAttribute("MensajeLogin01",
						"cuadroDialogo(\"El estado de su token es bloqueado, por favor para m�s informaci�n comun�quese a Superlinea Santander.\", 3);");
				request.setAttribute("Fecha", ObtenFecha());
				int resDup = cierraDuplicidad(session.getUserID8());
				request.getSession().setAttribute("session", session);
				EIGlobal.mensajePorTrace(this.getClass().getName() + " - "
						+ "LoginServlet.java::token() -> resDup: " + resDup,
						EIGlobal.NivelLog.DEBUG);
				request.getSession().invalidate();
				evalTemplate(IEnlace.LOGIN, request, response);
				EIGlobal.mensajePorTrace("Termina cierraSesionTokenBloqueado ->",EIGlobal.NivelLog.INFO);
	    	}catch(Exception e) {
                EIGlobal.mensajePorTrace ("E->cierraSesionTokenBloqueado->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	    	}
	    }

	    /**
	     * Metodo para obtener el estatus del Token
	     * @param token objeto del tipo Token
	     * @return int Estatus del Token
	     */
	    public int obtenerEstatusToken(Token token){
	    	EIGlobal.mensajePorTrace("Entra a obtenerEstatusToken ->",EIGlobal.NivelLog.INFO);
	    	Connection conn = null;
			int estatusToken = -1;
			boolean tokenValido = false;
			try{
				conn =  createiASConn(Global.DATASOURCE_ORACLE);
				tokenValido = token.inicializa(conn);
				conn.close();
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("SQLE en BaseServlet al consultar estatus token",EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e),EIGlobal.NivelLog.INFO);
			} finally{
				if(conn != null){
					try {
						conn.close();
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace("SQLE en BaseServlet al consultar estatus token",EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(new Formateador().formatea(e),EIGlobal.NivelLog.INFO);
					}
				}
			}
			if(tokenValido){
	        	estatusToken = token.getStatus();
	    	}
			EIGlobal.mensajePorTrace("Termina el obtenerEstatusToken ->",EIGlobal.NivelLog.INFO);
		return estatusToken;
	    }

	    /**
	     * Se encarga de llevar a cabo las validaciones necesarias en RSA para
	     * determinar el riesgo que corre una operaci&oacute; y con esto decidir
	     * si permite o restringe el acceso al aplicativo o la ejecuci&oacute;n
	     * de operaciones.
	     * @param req Objeto tipo HttpServletRequest con los datos de la
	     * sesi&oacute;n.
	     * @param res Objeto tipo HttpServletResponse con los datos que
	     * ser&aacute; enviados.
	     */
		public void validacionesRSA(HttpServletRequest req, HttpServletResponse res) {

			boolean validacionexito = false;
			boolean validarRSA = true;
			HttpSession ses = req.getSession();
			BaseResource session = (BaseResource) ses.getAttribute("session");
			String template= req.getServletPath();

			/**
			 * PYME Se agrega validacion para Unificacion de token
			 * en Nomina Epyme Psgos
			 */
			if( req.getAttribute("vieneDe") != null ){
				//template = "/enlaceMig/OpcionesNomina";
				template = req.getAttribute("vieneDe").toString();
				req.setAttribute("validaNull", "si");
			}

			EIGlobal.mensajePorTrace("--------->>>>>>>>>>>>>>>> Plantilla regreso challenge: " + template, EIGlobal.NivelLog.DEBUG);

			//validacion rsa para challenge
			ServiciosAAResponse resp = new ServiciosAAResponse();

			if(resp != null ) {
				UtilidadesRSA rsa = new UtilidadesRSA();
				RSABean  rsaBean=  rsa.generaBean(req, "", "", req.getHeader("IV-USER"));

				try {
					resp = rsa.ejecutaAnalyzePostLogin(rsaBean, req, res);
				} catch (NullPointerException e) {
					validarRSA = false;
				}
				try {
					if(validarRSA) {
						UtilidadesChallengeRSA utils = new UtilidadesChallengeRSA();
						ChallengeBean challengeBean = new ChallengeBean();
						challengeBean = utils.validarRespuestaRSA(req, res, resp , template);
						int referenciaBit = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

						if(challengeBean.getRespuesta() != null && challengeBean.getRespuesta().equals("CHALLENGE")) {
							//UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_CHALLENGE, referenciaBit, "RSA_0000", UtilidadesRSA.DES_CHALLENGE);
							//UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_CHALLENGE, "");
							req.setAttribute("sessionID", challengeBean.getSeesionID());
							req.setAttribute("transactionID", challengeBean.getTransactionID());
							req.setAttribute("preguntaUser", challengeBean.getPreguntaUser());
							req.setAttribute("preguntaUserID", challengeBean.getPreguntaUserID());
							req.setAttribute("servletRegreso", challengeBean.getServletRegreso());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("Encabezado", CreaEncabezado("Pregunta Reto","", "", req));
							EIGlobal.mensajePorTrace("-------------->Intenta direccionar pagina challenge" ,EIGlobal.NivelLog.INFO);
							evalTemplate("/jsp/paginaChallenge.jsp", req, res);
							return;

						} else if(challengeBean.getRespuesta() != null && challengeBean.getRespuesta().equals("DENY")) {
							UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_DENY, referenciaBit, "RSA_0000", UtilidadesRSA.DES_DENY);
							UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_DENY, "A",referenciaBit);
							req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"La situaci&oacute;n de su usuario est&aacute; siendo analizada por el banco " +
								"le pedimos se comunique con su ejecutivo de cuenta para darle seguimiento.\", 3);" );
							cierraDuplicidad(session.getUserID8());
							evalTemplate ( IEnlace.LOGIN, req, res );
							ses.invalidate();

						} else if(challengeBean.getRespuesta() != null && challengeBean.getRespuesta().equals("REVIEW")) {
							UtilidadesRSA.bitacoraTCT(req, UtilidadesRSA.CVE_REVIEW, referenciaBit, "RSA_0000", UtilidadesRSA.DES_REVIEW);
							UtilidadesRSA.bitacorizaTransac(req, UtilidadesRSA.CVE_REVIEW, "A",referenciaBit);
							req.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"La situaci&oacute;n de su usuario est&aacute; siendo analizada" +
								" por el banco, cualquier duda favor de comunicarse a S&uacute;per L&iacute;nea Empresarial a los tel&eacute;fonos 5169 4343 o Lada sin costo 01 800 509 5000.\", 13);" );
							cierraDuplicidad(session.getUserID8());
							evalTemplate ( IEnlace.LOGIN, req, res );
							ses.invalidate();

						} else if(challengeBean.getRespuesta().equals("SUCCESS")) {
							String params = ValidaOTP.reestableceParametrosEnSessionEncoded(req);
							EIGlobal.mensajePorTrace("-->BaseServlet params hacia challenge " + params ,EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("-->BaseServlet template hacia challenge " + template + "?" + params ,EIGlobal.NivelLog.DEBUG);
							req.setAttribute("challngeExito", "SUCCESS");
							req.getSession().removeAttribute("valTokenRSA");
							RequestDispatcher rd = req.getRequestDispatcher(template + "?" + params );
							rd.forward(req, res);
							return;
						}
					} else {
						String params = ValidaOTP.reestableceParametrosEnSessionEncoded(req);
						EIGlobal.mensajePorTrace("-->BaseServlet params hacia challenge " + params ,EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("-->BaseServlet template hacia challenge " + template + "?" + params ,EIGlobal.NivelLog.DEBUG);
						req.setAttribute("challngeExito", "SUCCESS");
						RequestDispatcher rd = req.getRequestDispatcher(template + "?" + params );
						rd.forward(req, res);
						return;
					}
				} catch (BusinessException e) {
					e.printStackTrace();
				} catch (ServletException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			EIGlobal.mensajePorTrace("--------->>>>>>>>>>>>>>>> Fin rsa", EIGlobal.NivelLog.DEBUG);
		}

		/**
		 * Identifica que las peticiones vienen de una carga de favoritos.
		 *
		 * @param req Peticion que contiene los paramtros de origen.
		 * @return True si la peticion viene del servlet de favoritos.
		 */
		public boolean identificarEsFavorito(HttpServletRequest req){

			boolean esDeFavoritos = false;

			HttpSession sesion = req.getSession();
			//Object paramPeticion = sesion.getAttribute(FavoritosConstantes.ID_PARAM_FAV);

			String paramPeticion = req.getParameter(FavoritosConstantes.ID_PARAM_FAV);




			EIGlobal.mensajePorTrace("Identificando si se va a cargar un favorito, el parametro es: "+paramPeticion, EIGlobal.NivelLog.INFO);

			if((paramPeticion!=null)&&(!"".equals(paramPeticion))&&(FavoritosConstantes.ES_PETICION_DE_FAVORITO.equals(paramPeticion.toString()))){
				esDeFavoritos = true;
				//sesion.removeAttribute(FavoritosConstantes.ID_PARAM_FAV);
			}


			return esDeFavoritos;
		}

		/**
		 * Da de alta un favorito en la BD.
		 * @param favorito Favorito a dar de alta.
		 * @return True si el alta fue exitosa.
		 */
		public boolean agregarFavorito(FavoritosEnlaceBean favorito){
			boolean altaExitosa = true;

			FavoritosEnlaceDAO favoritoDAO = new FavoritosEnlaceDAO();

			altaExitosa = favoritoDAO.agregarFavorito(favorito);

			return altaExitosa;
		}

		/**
		 * Se encarga de dar de alta un favorito para nomina
		 * @param request Objeto peticion.
		 * @param datFav informacion de favorito
		 * @return boolean alta exitosa
		 */
		public boolean agregarFavoritoNomina(HttpServletRequest request,ArchivoNominaBean datFav){
			boolean altaExitosa = true;

			String selFav = request.getParameter(STRCHECK);
			String txtFav = "";
			String usuario = "";
			FavoritosEnlaceNominaDAO favoritoDAO = null;
			HttpSession sesion = request.getSession();
			BaseResource bR =(BaseResource) sesion.getAttribute("session");

			EIGlobal.mensajePorTrace("se selecciono favorito:"+selFav, EIGlobal.NivelLog.INFO);


			if((selFav!=null)&&(!"".equals(selFav))&&("1".equals(selFav))){
				txtFav = request.getParameter(TXTFAV);
				favoritoDAO = new FavoritosEnlaceNominaDAO();

				datFav.setContrato(bR.getContractNumber());

				altaExitosa = favoritoDAO.agregarFavorito(txtFav, bR.getUserID8(), datFav);

			}


			return altaExitosa;
		}


		/**
		 * Se encarga de bitacorizar las operaciones de Favoritos.
		 * @param tipoOperacion Tipo de operacion a bitacorizar.
		 * @param favorito Favorito con la informacion a bitacorizar.
		 * @param request Peticion.
		 */
		public void bitacorizarOperacionesFavoritos(String tipoOperacion,
				FavoritosEnlaceBean favorito, HttpServletRequest request) {
			EIGlobal.mensajePorTrace("Entrando a bitacorizar la operacion: "
					+ tipoOperacion, EIGlobal.NivelLog.INFO);

			int numRef = -1;

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			BitaTCTBean beanBitacora = new BitaTCTBean();

			BitaHelper bitaHelper = new BitaHelperImpl(request, session, sess);
			BitaHandler manejadorBitacora = BitaHandler.getInstance();

			numRef = obten_referencia_operacion(Short
					.parseShort(IEnlace.SUCURSAL_OPERANTE));

			beanBitacora = bitaHelper.llenarBeanTCT(beanBitacora);
			beanBitacora.setReferencia(numRef);
			beanBitacora.setTipoOperacion(tipoOperacion);

			if (!BitaConstants.CONSULTA_FAVORITOS.equals(tipoOperacion)) {
				beanBitacora.setCuentaOrigen(favorito.getCuentaCargo());
				beanBitacora.setCuentaDestinoFondo(favorito.getCuentaAbono());
				beanBitacora.setImporte(favorito.getImporte());
			}

			beanBitacora.setCodError(tipoOperacion + "0000");

			EIGlobal.mensajePorTrace("Antes insert #####", EIGlobal.NivelLog.DEBUG);

			try {
				manejadorBitacora.insertBitaTCT(beanBitacora);
			} catch (SQLException e) {

				EIGlobal.mensajePorTrace(
						"ERROR SQL en la bitacorizacion: " + e.getMessage(),
						EIGlobal.NivelLog.ERROR);

			}catch (Exception e) {
				EIGlobal.mensajePorTrace(
						"ERROR en la bitacorizacion: " + e.getMessage(),
						EIGlobal.NivelLog.ERROR);

			}

		}


		/**
		 * Obtiene la ip del cliente.
		 * @param req Peticion
		 * @return Ip del cliente
		 */
		public static String obtenerIPCliente(HttpServletRequest req) {
				String ipActual = "";
				String headerEnlace= "iv-remote-address";
				String headerAkamai = "True-Client-IP";

				EIGlobal.mensajePorTrace("Obteniendo la IP del cliente", EIGlobal.NivelLog.INFO);
				if (req.getHeader(headerAkamai)!= null && !"".equals(req.getHeader(headerAkamai).trim())) {
					EIGlobal.mensajePorTrace("Obteniendo la IP del cliente desde AKAMAI", EIGlobal.NivelLog.INFO);
					ipActual = req.getHeader(headerAkamai);
				}else if (req.getHeader(headerEnlace)!= null && !"".equals(req.getHeader(headerEnlace).trim())) {
					EIGlobal.mensajePorTrace("Obteniendo la IP del cliente desde los equipos de ENLACE", EIGlobal.NivelLog.INFO);
					ipActual = req.getHeader(headerEnlace);
				} else {
					EIGlobal.mensajePorTrace("Obteniendo la IP desde la peticion", EIGlobal.NivelLog.INFO);
					ipActual = req.getRemoteAddr();
				}
				EIGlobal.mensajePorTrace("la IP del cliente obtenida: "+ipActual, EIGlobal.NivelLog.INFO);

				return ipActual;
			}
}//fin de la clase BaseServlet