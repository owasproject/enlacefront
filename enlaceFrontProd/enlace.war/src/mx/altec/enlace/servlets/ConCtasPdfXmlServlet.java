package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.bo.ConsultaCtasAsociadasBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.Formateador;

/**
 * @author Stefanini
 *
 */
@SuppressWarnings({ "unchecked", "unused" })
public class ConCtasPdfXmlServlet extends BaseServlet {


	/** Constante serial version */
	private static final long serialVersionUID = 3634258462003593871L;
	
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {    	
    	lupaCtas(request, response);
    }
    	
	/**
	 * Metodo para consultar las cuentas a presentar en la lupa
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	
	private void lupaCtas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ConsultaCtasAsociadasBO consultaCtasAsociadasBO = new ConsultaCtasAsociadasBO(); 
		List<ConfEdosCtaArchivoBean> cuentas = null;//new ArrayList<ConfEdosCtaArchivoBean>();
		ConfigModEdoCtaTDCBO configModEdoCtaTDCBO=new ConfigModEdoCtaTDCBO();
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		boolean cambioContrato =ConfigMasPorTCAux.cambiaContrato(request.getSession(), session.getContractNumber());
			//cambioContrato(request);
		
		String opcion = request.getParameter("opcion") != null && !"".equals(request.getParameter("opcion")) 
			? request.getParameter("opcion") : ""; 
		
		EIGlobal.mensajePorTrace(this + "----->  opcion: " + opcion, EIGlobal.NivelLog.INFO);
		
		if (("".equals(opcion) && request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null)
				|| cambioContrato) {
			cuentas = configModEdoCtaTDCBO.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()), session.getContractNumber(), session.getUserProfile());
			EIGlobal.mensajePorTrace("Longitud de la lista  : "+ cuentas.size(),
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			request.getSession().setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
		} else {
			
			ConfigMasPorTCAux.filtrarDatos(request, response);
			
		}
		evalTemplate("/jsp/consultaCuentasPdfXml.jsp", request, response);
	}
	
	


    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException {
		processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException {
		processRequest (request, response);
    }
}