package mx.altec.enlace.servlets;

import java.util.*;
//import java.lang.reflect.*;
import java.io.*;
//import java.rmi.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
//import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


/**
 * Class CreaArchMov.
 */
public class CreaArchMov extends BaseServlet
{
	
	/** La constante SIGNO_MENOS. */
	private static final String SIGNO_MENOS="-";
	
	/** La constante PREFIJO_REFERENCIA. */
	private static final String PREFIJO_REFERENCIA="20";
	
	/** La constante SIGNO_MAS. */
	private static final String SIGNO_MAS="+";
	
	/** La constante VACIO. */
	private static final String VACIO = "";
	
	/** El objeto my path. */
	String myPath = IEnlace.DOWNLOAD_PATH;
	
	/** El objeto file id. */
	int fileID    = Math.abs(new Random().nextInt());

	/** El objeto str extencion mov. */
	String strExtencionMov = SIGNO_MENOS + fileID + "MV.txt";
	
	/** El objeto cuenta. */
	private String cuenta  = VACIO;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction( req, res );}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction( req, res );}

	/**
	 * Default action.
	 *
	 * @param req El objeto: req
	 * @param res El objeto: res
	 * @throws ServletException La servlet exception
	 * @throws IOException Una excepcion de I/O ha ocurrido.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean sesionvalida = SesionValida( req, res );
		if (sesionvalida&&session.getFacultad(session.FAC_CONSULTA_MOVTOS))
		{
			req.setAttribute("MenuPrincipal", session.getstrMenu());
			req.setAttribute("Fecha", ObtenFecha());
			req.setAttribute("ContUser",ObtenContUser(req));
			int x;

			String arch=IEnlace.LOCAL_TMP_DIR+"/";
			String myrealfile = session.getArchivoMov().substring(arch.length(),session.getArchivoMov().length() - 6);
			//String filename = myrealfile + strExtencionMov;
			String filename = myrealfile +".doc";
			String prev =  (String) req.getParameter("prev");
			String next =  (String) req.getParameter("next");
			x = GeneraArchivoMovs((String )req.getParameter("EnlaceCuenta"),session.getArchivoMov(),IEnlace.DOWNLOAD_PATH + filename, Integer.parseInt(prev), Integer.parseInt(next),null);

			if(x == 0 )
			{
				req.setAttribute("Output1","<h1>Archivo creado</h1><a href =  \"/Download/"+filename + "\">Consigue el archivo</A>");
				evalTemplate(IEnlace.MOVIMIENTOS_TMPL,req, res);
			} else {
				log("error al crear el archivo : " + x);
				req.setAttribute("Output1","<h1>Error al intentar generar el archivo</h1>");
				evalTemplate(IEnlace.MOVIMIENTOS_TMPL,req, res);
			}
		} else if(sesionvalida) {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL,req,res);
		}
	}

	/**
	 * Definir el objeto: cuenta.
	 *
	 * @param cuenta El nuevo objeto: cuenta
	 */
	public void setCuenta(String cuenta)
	{
			this.cuenta = cuenta;
	}

	/**
	 * Genera archivo movs.
	 *
	 * @param cuenta El objeto: cuenta
	 * @param fileIn El objeto: file in
	 * @param fileOut El objeto: file out
	 * @param inicio El objeto: inicio
	 * @param fin El objeto: fin
	 * @param tipoBanca El objeto: tipo banca
	 * @return Objeto int
	 */
	public int GeneraArchivoMovs(String cuenta,String fileIn,String fileOut,int inicio,int fin, String tipoBanca)
	{
		if (cuenta.indexOf("@")!=-1)
		cuenta=cuenta.substring(0,cuenta.indexOf("@"));

		int j = 0;

		if(cuenta.indexOf("|")!=-1)
			{cuenta = cuenta.substring(cuenta.indexOf("|")+1,cuenta.lastIndexOf("|"));}

		String tabla = VACIO;

		EI_Exportar ArcSal;
		ArcSal = new EI_Exportar(IEnlace.DOWNLOAD_PATH +fileOut+"mv"+cuenta+".doc");
		ArchivoRemoto archR = new ArchivoRemoto();//Modificacion KPU 301003
		ArcSal.creaArchivo();
		EIGlobal.mensajePorTrace("Archivo Salida: " + IEnlace.DOWNLOAD_PATH +fileOut+"mv"+cuenta+".doc", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Archivo Remoto: " + archR.getNombreArchivo(), EIGlobal.NivelLog.DEBUG);

		String mvs[][] = readlinefromFile(fileIn,11,inicio,fin);

		for(j=0;j< fin; j++)
		{
			EIGlobal.mensajePorTrace("Contenido REGISTRO: ", EIGlobal.NivelLog.INFO);
			for (String strings : mvs[j]) {				
				EIGlobal.mensajePorTrace("Info sector: ", EIGlobal.NivelLog.INFO);
			}
			if(mvs == null || j >= mvs.length) {
				EIGlobal.mensajePorTrace("Archivo accedido: " + fileIn, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Valor de inicio: " + inicio, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Valor de fin: " + fin, EIGlobal.NivelLog.ERROR);
				if(mvs == null) {
					EIGlobal.mensajePorTrace("mvs nulo.", EIGlobal.NivelLog.ERROR);
				}
				else {
					EIGlobal.mensajePorTrace("Indice invalido accesando a mvs[j] con valor de j: " + j, EIGlobal.NivelLog.ERROR);
				}
				continue;
			}
			if(mvs[j] == null) {
				EIGlobal.mensajePorTrace("mvs[j] nulo para valor de j: " + j, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Archivo: " + fileIn, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Valor de inicio: " + inicio, EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("Valor de fin: " + fin, EIGlobal.NivelLog.ERROR);
				continue;
			}
			//cta
			tabla =rellenaCaracter(cuenta,' ',16,false);
			int tamReg = mvs[j].length;
			EIGlobal.mensajePorTrace("Longitud arreglo: " + tamReg, EIGlobal.NivelLog.DEBUG);
			
			if ("E".equals(tipoBanca)) {
				tabla += obtenerRegistroBancaEspecializada(mvs,tamReg,j);
			} else {
				tabla += obtenerRegistroChequera(mvs,tamReg,j);
			}

			tabla += "\r\n";
			ArcSal.escribeLinea(tabla);	   //linea 1495 de consulta bitacora
		}
		ArcSal.cierraArchivo();

		if(!archR.copiaLocalARemoto(fileOut+"mv"+cuenta+".doc","WEB"))
		{
			EIGlobal.mensajePorTrace("***CreaArchMov.class No se pudo crear archivo para exportar movimientos", EIGlobal.NivelLog.ERROR);
		}
		return 0;
	}
	
	/**
	 * Obtener registro chequera.
	 *
	 * @param mvs El objeto: mvs
	 * @param tamReg El objeto: tam reg
	 * @param j El objeto: j
	 * @return Objeto string
	 */
	private String obtenerRegistroChequera(String[][] mvs, int tamReg, int j) {
		StringBuilder tabla = new StringBuilder();
		String campoCompleto = VACIO; //Ley de Transparencia II (HGCV)
		String concepto     = VACIO; //Ley de Transparencia II (HGCV)
		// Fecha
		if(esInformacionValida(tamReg,mvs[j][1],1)) {
			tabla.append(mvs[j][1].substring(2,4)+mvs[j][1].substring(0,2))
			.append("20")
			.append(mvs[j][1].substring(4,mvs[j][1].length()));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',8,false));//8 espacios de fecha vacia
		}
		//Hora
		if(esInformacionValida(tamReg,mvs[j][7],7)) {
			tabla.append(mvs[j][7].substring(0,2)).append(mvs[j][7].substring(3,mvs[j][7].length()));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',4,false));//4 espacios de hora vacia
		}
		//Sucursal
		if(esInformacionValida(tamReg,mvs[j][6],6)) {
			if(mvs[j][6].trim().length()<4) {
				tabla.append(rellenaCaracter(mvs[j][6].trim(),' ',4,false));//4 espacios de
			}else {
				tabla.append(mvs[j][6]);
			}
		}else{
			tabla.append(rellenaCaracter(VACIO,' ',4,false));//4 espacios de sucursal
		}
		//Descripción
		if(esInformacionValida(tamReg,mvs[j][2],2)) {
			tabla.append(rellenaCaracter(mvs[j][2],' ',40,false));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',40,false));
		}
		//Cargo/Abono
		if(esInformacionValida(tamReg,mvs[j][0],0)) {
			tabla.append(mvs[j][0]);
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',1,false));
		}
		//Importe
		if(esInformacionValida(tamReg,mvs[j][5],5)) {
			if (mvs[j][5].trim().indexOf(".")>-1) {
				tabla.append(rellenaCaracter(mvs[j][5].trim().substring(0,mvs[j][5].trim().indexOf(".")),'0',14,true));
			}else {
				tabla.append(rellenaCaracter(mvs[j][5].trim().substring(0,mvs[j][5].trim().length()),'0',14,true));
			}
		}else{			
			tabla.append(rellenaCaracter(VACIO,'0',14,true));
		}
		//Saldo
		if(esInformacionValida(tamReg,mvs[j][8],8)) {
			if (mvs[j][8].trim().indexOf(".")>-1) {
				tabla.append(rellenaCaracter(mvs[j][8].trim().substring(0,mvs[j][8].trim().indexOf(".")),'0',14,true));
			}else{
				tabla.append(rellenaCaracter(mvs[j][8].trim().substring(0,mvs[j][8].trim().length()),'0',14,true));
			}
		}else {
			tabla.append(rellenaCaracter(VACIO,'0',14,true));
		}
		// Referencia
		if(esInformacionValida(tamReg,mvs[j][4],4)) {
			tabla.append(rellenaCaracter(mvs[j][4],'0',8,true));
		}else {
			tabla.append(rellenaCaracter(VACIO,'0',8,true));
		}
		if(tamReg > 9) {
			campoCompleto = mvs[j][9];
		}else{
			campoCompleto = VACIO;
		}
		EIGlobal.mensajePorTrace("CAMPO COMPLETO EXPORTACION= >" +campoCompleto+ "<", EIGlobal.NivelLog.INFO);

		//Concepto
		if (tamReg > 2 && mvs[j][2] != null && mvs[j][2].trim().equals("PAG SPEUA/INTE"))
		{
			if (campoCompleto.indexOf("REF") >= 0)
			{
				concepto = campoCompleto.substring(0,campoCompleto.indexOf("REF"));
				EIGlobal.mensajePorTrace("CONCEPTO EXPORTAR NUEVO = >" +concepto+ "<", EIGlobal.NivelLog.INFO);
				tabla.append(rellenaCaracter(concepto,' ',40,false));
			} else {
				if (campoCompleto.length() >= 40){
					concepto = campoCompleto.substring(0,40);
					EIGlobal.mensajePorTrace("CONCEPTO EXPORTAR NUEVO = >" +concepto+ "<", EIGlobal.NivelLog.INFO);
					tabla.append(rellenaCaracter(concepto,' ',40,false));
				} else {
					tabla.append(rellenaCaracter(campoCompleto,' ',40,false));
				}
			}
		} else	if (tamReg > 2 && mvs[j][2] != null && mvs[j][2].trim().equals("APORT LC INNET")){
				tabla.append(rellenaCaracter(campoCompleto,' ',40,false));
		}else {

			if(mvs == null || mvs[j] == null || mvs[j][9] == null || j >= mvs.length || 9 >= mvs[0].length ) {
				tabla.append(rellenaCaracter(VACIO,' ',40,false));
				try {
					EIGlobal.mensajePorTrace("Datos invalidos.", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS es nulo: " + (mvs == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS[j] para j=" + j + " es nulo: " + (mvs[j] == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS[j][9] para j=" + j + " es nulo: " + (mvs[j][9] == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("la variable j desborda mvs[j]: " + (j >= mvs.length), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("la variable 9 desborda mvs[j][9]: " + (9 >= mvs[j].length), EIGlobal.NivelLog.ERROR);
				} catch(Exception e) {}
			}
			else {
				tabla.append(rellenaCaracter(mvs[j][9].trim(),' ',40,false));
			}
		}
		return tabla.toString();
	}

	/**
	 * Es informacion valida.
	 *
	 * @param tamReg El objeto: tam reg
	 * @param valor El objeto: valor
	 * @param indice El objeto: indice
	 * @return true, si exitoso
	 */
	private boolean esInformacionValida(int tamReg, String valor, int indice) {
		return (tamReg > indice && valor != null && valor.trim().length()> 0);
	}

	/**
	 * Obtener registro banca especializada.
	 *
	 * @param mvs El objeto: mvs
	 * @param tamReg El objeto: tam reg
	 * @param j El objeto: j
	 * @return Objeto string
	 */
	private String obtenerRegistroBancaEspecializada(String[][] mvs, int tamReg, int j) {
		StringBuilder tabla = new StringBuilder();
		String campoCompleto = ""; //Ley de Transparencia II (HGCV)
		String concepto     = ""; //Ley de Transparencia II (HGCV)
		String referencia   = ""; //Ley de Transparencia II (HGCV)
		// Referencia
		if(esInformacionValida(tamReg,mvs[j][1],1)) {
			tabla.append(mvs[j][1].substring(2,4)+mvs[j][1].substring(0,2))
			.append(PREFIJO_REFERENCIA)
			.append(mvs[j][1].substring(4,mvs[j][1].length()));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',12,false));//8 espacios de fecha vacia
		}
		//Hora
		if(esInformacionValida(tamReg,mvs[j][7],7)) {
			tabla.append(mvs[j][7].substring(0,2)).append(mvs[j][7].substring(3,mvs[j][7].length()));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',4,false));//4 espacios de hora vacia
		}
		//Sucursal
		if(esInformacionValida(tamReg,mvs[j][6],6)) {
			if(mvs[j][6].trim().length()<4) {
				tabla.append(rellenaCaracter(mvs[j][6].trim(),' ',4,false));//4 espacios de
			}else {
				tabla.append(mvs[j][6]);
			}
		}else{
			tabla.append(rellenaCaracter(VACIO,' ',4,false));//4 espacios de sucursal
		}
		//Descripcion
		if(esInformacionValida(tamReg,mvs[j][2],2)) {
			tabla.append(rellenaCaracter(mvs[j][2],' ',40,false));
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',40,false));
		}
		//Fecha
		if(esInformacionValida(tamReg,mvs[j][0],0)) {
			tabla.append(mvs[j][0]);
		}else {
			tabla.append(rellenaCaracter(VACIO,' ',6,false));
		}
		// Importe
		if(esInformacionValida(tamReg,mvs[j][10],10)) {
			tabla.append(obtenerCargoAbono(mvs[j][10].trim()));
		}else {
			tabla.append(rellenaCaracter(VACIO,'0',36,true));
		}
		//Concepto
		if (tamReg > 2 && mvs[j][2] != null && mvs[j][2].trim().equals("PAG SPEUA/INTE"))
		{
			if (campoCompleto.indexOf("REF") >= 0)
			{
				concepto = campoCompleto.substring(0,campoCompleto.indexOf("REF"));
				EIGlobal.mensajePorTrace("CONCEPTO EXPORTAR NUEVO = >" +concepto+ "<", EIGlobal.NivelLog.INFO);
				tabla.append(rellenaCaracter(concepto,' ',40,false));
			} else {
				if (campoCompleto.length() >= 40)
				{
					concepto = campoCompleto.substring(0,40);
					EIGlobal.mensajePorTrace("CONCEPTO EXPORTAR NUEVO = >" +concepto+ "<", EIGlobal.NivelLog.INFO);
					tabla.append(rellenaCaracter(concepto,' ',40,false));
				} else {
					tabla.append(rellenaCaracter(campoCompleto,' ',40,false));
				}
			}
		} else	if (tamReg > 2 && mvs[j][2] != null && mvs[j][2].trim().equals("APORT LC INNET")){
			tabla.append(rellenaCaracter(campoCompleto,' ',40,false));

		}else {
			if(mvs == null || mvs[j] == null || mvs[j][9] == null || j >= mvs.length || 9 >= mvs[0].length ) {
				tabla.append(rellenaCaracter("",' ',40,false));
				try {
					EIGlobal.mensajePorTrace("Datos invalidos.", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS es nulo: " + (mvs == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS[j] para j=" + j + " es nulo: " + (mvs[j] == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("MVS[j][9] para j=" + j + " es nulo: " + (mvs[j][9] == null), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("la variable j desborda mvs[j]: " + (j >= mvs.length), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("la variable 9 desborda mvs[j][9]: " + (9 >= mvs[j].length), EIGlobal.NivelLog.ERROR);
				} catch(Exception e) {}
			}
			else {
				tabla.append(rellenaCaracter(mvs[j][9].trim(),' ',40,false));
			}
		}
		return tabla.toString();
	}

	/**
	 * Obtener cargo abono.
	 *
	 * @param numero El objeto: numero
	 * @return Objeto string
	 */
	private String obtenerCargoAbono(String numero) {
		StringBuilder sb = new StringBuilder();
		String signo = SIGNO_MENOS;
		if (numero.indexOf(SIGNO_MENOS)==-1) {
			signo = SIGNO_MAS;
		}
		sb.append(signo);
		numero = numero.replace(SIGNO_MENOS,VACIO);
		sb.append(rellenaCaracter(numero,'0',35,true));
		return sb.toString();
	}

	//Ley de Transparencia Fase II
	//se cambio el metodo para crear la tabla, se creo una instancia de la clase EI_Tipo.class
	//y se le mando a llamar al metodo iniciaObjeto() HGCV 20/03/2006
	/**
	 * Readlinefrom file.
	 *
	 * @param filename El objeto: filename
	 * @param Ndelim El objeto: ndelim
	 * @param b_ini El objeto: b_ini
	 * @param b_end El objeto: b_end
	 * @return Objeto string[][]
	 */
	public String[][] readlinefromFile(String filename,int Ndelim,int b_ini,int b_end)
	{
		BufferedReader entrada = null;
		StringBuffer cadenaArc = new StringBuffer();
		String linea = null;
		int i = 0;
		int j = 0;
		int k = 0;
		int i_ini = b_ini;
		int i_end = b_end;

		EI_Tipo armaTabla = new EI_Tipo();

		String[][] salida = new String[i_end - i_ini][Ndelim];

		try {
			entrada = new BufferedReader(new FileReader(filename));
			linea = entrada.readLine();
			if(i_ini>0)
			{
				for(j=0;j<i_ini;j++)
				{
					linea = entrada.readLine();

					if( linea.trim().equals(VACIO))
						linea = entrada.readLine();
					if( linea == null )
						break;
				}
			}
			for(j=0;j<(i_end- i_ini);j++)
			{
				linea = entrada.readLine();
				EIGlobal.mensajePorTrace("Linea Original Exp = >" +linea+ "<", EIGlobal.NivelLog.DEBUG);
				if(linea == null) {
					EIGlobal.mensajePorTrace("Linea Original es nula, no se agregan datos...", EIGlobal.NivelLog.ERROR);
				}
				else {
					cadenaArc.append(linea.trim() + "@");
				}
			}
			armaTabla.iniciaObjeto(';','@',cadenaArc.toString());
			//entrada.close();
		} catch(Exception e) {
			EIGlobal.mensajePorTrace("Error en lectura de archivo <" + filename + ">, mensaje: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}
		finally {
			try {
				entrada.close();
			} catch(Exception e) {}
		}
		return armaTabla.camposTabla;
	}

	/**
	 * Rellena caracter.
	 *
	 * @param origen El objeto: origen
	 * @param caracter El objeto: caracter
	 * @param cantidad El objeto: cantidad
	 * @param izquierda El objeto: izquierda
	 * @return Objeto string
	 */
	public String rellenaCaracter(String origen, char caracter, int cantidad, boolean izquierda)
	{
		while(cantidad > origen.length())
			{if(izquierda) origen = caracter + origen; else origen = origen + caracter;}
		return origen;
	}
}