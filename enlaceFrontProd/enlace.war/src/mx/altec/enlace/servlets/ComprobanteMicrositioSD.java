package mx.altec.enlace.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.cliente.ws.csd.ConstantesSD;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.HTMLUtil;
import mx.altec.enlace.utilerias.SelloDigitalUtils;

/**
 * Servlet implementation class ComprobanteMicrositioSD
 */
public class ComprobanteMicrositioSD extends BaseServlet {
    /**
     * Serial version
     */
    private static final long serialVersionUID = -8521002701596192315L;

    /**
     * Do get.
     * 
     * @param req the req
     * @param res the res
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("<--------- INICIA **ComprobanteMicrositioSD.doGet -------->", EIGlobal.NivelLog.INFO);
		defaultAction(req, res);
		EIGlobal.mensajePorTrace("<--------- SALIENDO DE  **ComprobanteMicrositioSD.doGet -------->", EIGlobal.NivelLog.INFO);
	}

    /**
     * Do post.
     * 
     * @param req the req
     * @param res the res
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("<--------- INICIA **ComprobanteMicrositioSD.doPost -------->", EIGlobal.NivelLog.INFO);
		defaultAction(req, res);
		EIGlobal.mensajePorTrace("<--------- SALIENDO DE  **ComprobanteMicrositioSD.doGet -------->", EIGlobal.NivelLog.INFO);
	}
	
    /**
     * Default action.
     * 
     * @param request inforacion de la peticion web
     * @param res datos de respuesta
     * @throws ServletException the servlet exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void defaultAction(HttpServletRequest request, HttpServletResponse res) throws ServletException, IOException {
        EIGlobal.mensajePorTrace("ComprobanteMicrositioSD <------ **INICIA defaultAction", EIGlobal.NivelLog.INFO);

        String mod = SelloDigitalUtils.obtenerModulo(request);
        EIGlobal.mensajePorTrace("modulo|" + mod + "|", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Cuenta>|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO) + "|", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("LinCap|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_LIN_CAP) + "|", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Import|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_IMPORTE) + "|", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Refer|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_REFE) + "|", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Fecha|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA) + "|", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("Hora|" + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_HORA) + "|", EIGlobal.NivelLog.INFO);
        try {
            if (ConstantesSD.MODULO_VSD.equals(mod)
                    || ConstantesSD.MODULO_CSD.equals(mod)) {
                char cEspacio = ' ';
                String cuentaDestino = ConstantesSD.CADENA_VACIA;
                if (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO) != null) {
                    if (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(cEspacio) < 0) {
                        cuentaDestino = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO);
                    } else {
                        cuentaDestino = request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO)
                                .substring(0, request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(cEspacio));
                    }
                }
                String idConv;
                if (!(idConv = CuentasDAO.consultarCuentaConv(cuentaDestino)).isEmpty()) {
                    EIGlobal.mensajePorTrace("<-- ComprobanteMicrositioSD **defaultAction no es cuenta Convenio", EIGlobal.NivelLog.INFO);
                }
                request.setAttribute(ConstantesSD.QUERY_PARAM_MOD,
                        (request.getParameter(ConstantesSD.QUERY_PARAM_MOD) != null)
                        ? request.getParameter(ConstantesSD.QUERY_PARAM_MOD)
                        : ConstantesSD.MODULO_VSD);
                generarComprobanteSD(request, res, idConv);
            } else {
                EIGlobal.mensajePorTrace("<-- ComprobanteMicrositioSD **defaultAction no se encontro modulo", EIGlobal.NivelLog.INFO);
            }
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            EIGlobal.mensajePorTrace("<-- ComprobanteMicrositioSD **defaultAction error de arreglos|" + aiobe.getMessage() + "|", EIGlobal.NivelLog.INFO);
        }
        evalTemplate(ConstantesSD.PATH_JSP_SD, request, res);
        EIGlobal.mensajePorTrace("<---- ComprobanteMicrositioSD **FINALIZA defaultAction", EIGlobal.NivelLog.INFO);
    }

    /**
     * Generacion de comprobante con sello digital
     *
     * @param request Objeto con informacion inherente de la peticion
     * @param response Objeto con informacion inherente de la respuesta
     * @param idConv identificador del convenio
     * @throws ServletException en caso de ocurrir una ServletException
     * @throws IOException en caso de ocurrir una IOException
     */
    public void generarComprobanteSD(HttpServletRequest request, HttpServletResponse response, String idConv) throws ServletException, IOException {
        EIGlobal.mensajePorTrace("INICIA generarComprobanteSD", EIGlobal.NivelLog.INFO);
        StringBuilder msgSalida = new StringBuilder(HTMLUtil.crearElemento(
                5, HTMLUtil.generarAttr(new String[][]{{
            "style", "margin: auto; text-align: center;"}}),
                "<h3>Ocurri&oacute; un error al solicitar comprobante con sello digital, intente m&aacute;s tarde</h3>").getEtiquetaAttr());

        String module = SelloDigitalUtils.obtenerModulo(request);
        EIGlobal.mensajePorTrace("generarComprobanteSD modulo|" + module + "|", EIGlobal.NivelLog.INFO);
        if (ConstantesSD.MODULO_VSD.equals(module)) {
            EIGlobal.mensajePorTrace("generarComprobanteSD fecha|" + ((request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA) != null)
                    ? SelloDigitalUtils.armarFchPag(request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA).trim() + " " + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_HORA).trim())
                    : new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX).format(new Date())) + "|", EIGlobal.NivelLog.INFO);
            msgSalida.replace(0, msgSalida.length(), SelloDigitalUtils.validarComprobante(new String[]{
                (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_LIN_CAP) != null ? request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_LIN_CAP)
                : request.getParameter(ConstantesSD.QUERY_PARAM_TMB_LIN_CAP)),
                request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_IMPORTE),
                request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_REFE),
                (request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA) != null)
                ? SelloDigitalUtils.armarFchPag(request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_FECHA).trim() + " " + request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_HORA).trim())
                : new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX).format(new Date()),
                ConstantesSD.ID_CANAL_,
                ((request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(" ") < 0)
                ? request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO)
                : request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).substring(0, request.getParameter(ConstantesSD.QUERY_PARAM_BITACORA_CTA_DESTINO).indexOf(" "))),
                idConv,
                new StringBuilder(ConstantesSD.PATH_SERVLET_CSD_BITACORA).append("?").append(ConstantesSD.QUERY_PARAM_MOD).append(ConstantesSD.SIGNO_IGUAL)
                .append(ConstantesSD.MODULO_CSD).toString()}, request, response));
        } else if (ConstantesSD.MODULO_CSD.equals(module)) {
            StringBuilder camposFijos = new StringBuilder(request.getParameter(ConstantesSD.HIDDEN_CAMPOS_ADIC_OPER)).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_NOMBRE)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_DOMICILIO)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_COLONIA)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_CP)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_DELEGACION_MUNICIPIO)).append(ConstantesSD.SIMPLE_PIPE).append(
                    request.getParameter(ConstantesSD.PARAM_CSD_FORM_ESTADO)).append(ConstantesSD.SIMPLE_PIPE);

            String[] camposAdicionalesWS = request.getParameter(ConstantesSD.HIDDEN_CAMPOS_ADIC_WS).split(ConstantesSD.SPLIT_DOBLE_PIPE);
            for (String string : camposAdicionalesWS) {
                EIGlobal.mensajePorTrace("generarComprobanteSD camposAdicionalesWS|" + string + "|", EIGlobal.NivelLog.ERROR);
            }
            StringBuilder camposAdicionales = new StringBuilder(ConstantesSD.CADENA_VACIA);
            for (int i = 0; Integer.parseInt(request.getParameter(ConstantesSD.HIDDEN_NUM_CAMPOS_ADIC_WS)) > 0 && i < camposAdicionalesWS.length; i++) {
                if (camposAdicionalesWS != null && camposAdicionalesWS[i] != null
                        && camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE).length == 3) {
                    String name = camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE)[0];
                    EIGlobal.mensajePorTrace("generarComprobanteSD camposDinamicos campo|" + name + "| valor|" + request.getParameter(name) + "|", EIGlobal.NivelLog.ERROR);
                    camposAdicionales.append(name).append(ConstantesSD.SIMPLE_PIPE).append((ConstantesSD.TIPO_DATO_FECHA.equals(camposAdicionalesWS[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE)[1]))
                            ? SelloDigitalUtils.armarFchPag(request.getParameter(name))
                            : request.getParameter(name)).append(ConstantesSD.SIMPLE_PIPE);
                }
            }
            EIGlobal.mensajePorTrace("generarComprobanteSD camposDinamicos|" + camposAdicionales.toString() + "| camposFijos|" + camposFijos.toString() + "|", EIGlobal.NivelLog.ERROR);
            msgSalida.replace(0, msgSalida.length(), SelloDigitalUtils.generarComprobante(new String[]{
                camposFijos.toString(), camposAdicionales.toString()}, request, response));
        } else {
            EIGlobal.mensajePorTrace("generarComprobanteSD no se encontro modulo", EIGlobal.NivelLog.ERROR);
        }
        request.setAttribute(ConstantesSD.HTML_SD, msgSalida.toString());
        EIGlobal.mensajePorTrace("FINALIZA generarComprobanteSD msgSalida|" + msgSalida.toString() + "|", EIGlobal.NivelLog.INFO);
    }

}