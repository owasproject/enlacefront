package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.*;
import java.lang.reflect.*;
import java.sql.*;
import java.io.*;

public class MDI_Plazas extends BaseServlet{

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

	 EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Entrando a Catalogo de Plazas.", EIGlobal.NivelLog.INFO);
	StringBuffer strPlazas = new StringBuffer ("");  /*JEV 04/03/04*/
	 String totales="";
	 String Error="SUCCESS";

	 int totalPlazas = 0;
	try{
		  Connection conn = createiASConn(Global.DATASOURCE_ORACLE);

		  if(conn == null){
			EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
		    Error="NOSUCCESS";
		  }

		/************************************************** Contar registros de plazas .... */

		  totales="Select count(*) from comu_pla_banxico";
		  PreparedStatement contQuery = conn.prepareStatement(totales);
		  if(contQuery == null)
		   {
		 EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
		 Error="NOSUCCESS";
		}
		ResultSet contResult = contQuery.executeQuery();
//**********
		if(contResult==null)
       {
         EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
	     Error="NOSUCCESS";
       }

	   if(contResult.next())
		totalPlazas=Integer.parseInt(contResult.getString(1));
	   else
		  totalPlazas =0;
		EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Total de Plazas: "+Integer.toString(totalPlazas), EIGlobal.NivelLog.INFO);

		/************************************************** Contar registros de bancos .... */
		 String sqlplaza="Select * from comu_pla_banxico order by descripcion";
		 PreparedStatement plazaQuery = conn.prepareStatement(sqlplaza);
		 if(plazaQuery == null)
		   {
		 EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Error al intentar crear Query.", EIGlobal.NivelLog.ERROR);
		 Error="NOSUCCESS";
		}

		ResultSet plazaResult = plazaQuery.executeQuery();
		  if(plazaResult==null)
		   {
		     EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Error al intentar crear ResultSet.", EIGlobal.NivelLog.ERROR);
		 	 Error="NOSUCCESS";
		   }
	     EIGlobal.mensajePorTrace("MDI_Plazas prueba - Termino de generar el Result.", EIGlobal.NivelLog.ERROR);

		 if(Error.equals("SUCCESS"))
		   {
			 StringBuffer arcLinea = new StringBuffer ("");
		     String nombreArchivo=IEnlace.DOWNLOAD_PATH+"MDI_Plazas.html";

			 EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
		     if(ArcSal.recuperaArchivo())
		       {
	     	     EIGlobal.mensajePorTrace("MDI_Plazas - Entro a ArcSal.recuperaArchivo() .", EIGlobal.NivelLog.ERROR);
		         arcLinea = new StringBuffer ("\n<html>");/*JEV 04/03/04*/
				 arcLinea.append ("\n<head><title>Plazas</title>");/*JEV 04/03/04*/
				 arcLinea.append ("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");/*JEV 04/03/04*/
				 arcLinea.append ("\n</head>");/*JEV 04/03/04*/
				 /*-->LCP inicia modificacion por incidencia IM159775*/
				 arcLinea.append ("\n<script language='javaScript'>");/*JEV 04/03/04*/
				 arcLinea.append ("\n	var guardado = '';");/*JEV 04/03/04*/
				 arcLinea.append ("\n	function fnGuardaAnterior()");/*JEV 04/03/04*/
				 arcLinea.append ("\n	{");/*JEV 04/03/04*/
				 arcLinea.append ("\n		if(guardado.length == 0)");/*JEV 04/03/04*/
				 arcLinea.append ("\n			guardado = window.opener.document.transferencia.Descripcion.value;");/*JEV 04/03/04*/
				 arcLinea.append ("\n	}");/*JEV 04/03/04*/
				 arcLinea.append ("\n</script>");/*JEV 04/03/04*/
				 /*LCP - termina modificacion por incidencia IM159775 <--*/
				 arcLinea.append ("\n<body bgcolor='#FFFFFF'>");/*JEV 04/03/04*/
		         arcLinea.append ("\n <form name=Forma>");/*JEV 04/03/04*/
		         arcLinea.append ("\n   <table border=0>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      <tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n        <th class='tittabdat'> Seleccione una plaza </th>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      </tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      <tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n        <td class='tabmovtex'>");/*JEV 04/03/04*/
				 /*--> LCP se agrega el OnChange por incidencia IM159775 <--*/
		         arcLinea.append ("\n         <select NAME=SelPlaza size=20 OnChange='javascript:fnGuardaAnterior();opener.Selecciona(1);' class='tabmovtex'>\n");/*JEV 04/03/04*/
				 /*LCP - termina <--*/
		         if(!ArcSal.escribeLinea(arcLinea.toString()))/*JEV 04/03/04*/
		           EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea.toString(), EIGlobal.NivelLog.INFO);

				 plazaResult.next();
		         for(int i=0; i<totalPlazas; i++)
		          {
		            arcLinea = new StringBuffer  ("\n          <option value=");/*JEV 04/03/04*/
					arcLinea.append	(plazaResult.getString(1).trim());/*JEV 04/03/04*/
					arcLinea.append (">"+plazaResult.getString(3).trim());/*JEV 04/03/04*/
					arcLinea.append ("</option>");/*JEV 04/03/04*/
		            if(!ArcSal.escribeLinea(arcLinea.toString()))/*JEV 04/03/04*/
		              EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea.toString(), EIGlobal.NivelLog.INFO);
		            plazaResult.next();
		          }

				  EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Se escribieron las plazas...", EIGlobal.NivelLog.INFO);

		          /*--> LCP - modificacion por incidencia IM159775
				  while(!ArcSal.eof())
		           {
		            if(!ArcSal.escribeLinea("                                                                                  "))
		             EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Algo salio mal eof()", EIGlobal.NivelLog.INFO);
		           }<--*/

		         arcLinea.append ("\n         </select>");/*JEV 04/03/04*/
				 arcLinea.append ("\n        </td>");/*JEV 04/03/04*/
				 arcLinea.append ("\n      </tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      <tr>");/*JEV 04/03/04*/
				 arcLinea.append ("\n          <td><br></td>");/*JEV 04/03/04*/
				 arcLinea.append ("\n      </tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      <tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n        <td align=center>");/*JEV 04/03/04*/
				 arcLinea.append ("\n         <table border=0 align=center cellspacing=0 cellpadding=0>");/*JEV 04/03/04*/
				 arcLinea.append ("\n          <tr>");/*JEV 04/03/04*/
				 arcLinea.append ("\n           <td>");/*JEV 04/03/04*/
				 /*--> LCP inicia. IM159775 Se agrega funcionalidad a bot�n de Cancelar*/
				 arcLinea.append ("\n            <a href='javascript:opener.Selecciona(1); window.close();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0></a><a href='javascript:window.opener.document.transferencia.Descripcion.value=guardado;window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a>");/*JEV 04/03/04*/
				 /*--> LCP termina. IM159775 Se agrega funcionalidad a bot�n de Cancelar*/
				 arcLinea.append ("\n           </td>");/*JEV 04/03/04*/
				 arcLinea.append ("\n          </tr>");/*JEV 04/03/04*/
				 arcLinea.append ("\n         </table>");/*JEV 04/03/04*/
		         arcLinea.append ("\n        </td>");/*JEV 04/03/04*/
		         arcLinea.append ("\n      </tr>");/*JEV 04/03/04*/
		         arcLinea.append ("\n    </table>\n </form>\n</body>\n</html>");/*JEV 04/03/04*/

		         if(!ArcSal.escribeLinea(arcLinea.toString()))/*JEV 04/03/04*/
		           EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Algo salio mal escribiendo: "+arcLinea.toString(), EIGlobal.NivelLog.INFO);
		         ArcSal.cierraArchivo();

				 //###### Cambiar....
				 //*************************************************************
				 ArchivoRemoto archR = new ArchivoRemoto();
				 if(!archR.copiaLocalARemoto("MDI_Plazas.html","WEB"))
				 EIGlobal.mensajePorTrace("MDI_Plazas - execute(): No se pudo copiar el archivo.", EIGlobal.NivelLog.INFO);
				 //**************************************************************
		       }
		      else
		       {
		         strPlazas.append ("\n<p><br><table border=0 width=90% align=center>");/*JEV 04/03/04*/
		         strPlazas.append ("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");/*JEV 04/03/04*/
		         strPlazas.append ("\n<tr><th bgcolor=red><font color=white>En este momento no se pudo generar catalogo. Por favor intente mas tarde.<font></td></tr>");/*JEV 04/03/04*/
		         strPlazas.append ("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");/*JEV 04/03/04*/
		         strPlazas.append ("\n</table>");/*JEV 04/03/04*/
		       }
		     plazaResult.close();
		   }
		  else
		   {
		     strPlazas.append ("\n<p><br><table border=0 width=90% align=center>");/*JEV 04/03/04*/
		     strPlazas.append ("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");/*JEV 04/03/04*/
		     strPlazas.append ("\n<tr><th bgcolor=red><font color=white>Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.<font></td></tr>");/*JEV 04/03/04*/
		     strPlazas.append ("\n<tr><td bgcolor='#F0F0F0'><br></td></tr>");/*JEV 04/03/04*/
		     strPlazas.append ("\n</table>");/*JEV 04/03/04*/
		   }
     conn.close();   /*JEV 09/03/04*/
	}catch( SQLException sqle ){
		sqle.printStackTrace();
	} catch(Exception e) {
		e.printStackTrace();
	}
     EIGlobal.mensajePorTrace("MDI_Plazas - el valor de strPlazas>>>>> : "+strPlazas.toString(), EIGlobal.NivelLog.INFO);
	 req.setAttribute("Splazas",strPlazas.toString());
	 evalTemplate("/jsp/MDI_Plazas.jsp", req, res);

	EIGlobal.mensajePorTrace("MDI_Plazas - execute(): Saliendo de Catalogo de Plazas.", EIGlobal.NivelLog.INFO);

	}
}
