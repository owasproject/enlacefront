package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import mx.altec.enlace.beans.ConsultaCtasMovilesBean;
import mx.altec.enlace.beans.CuentaMovilBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaCtasMovilesBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;

/**
 * The Class CtasMoviles.
 */
public class CtasMoviles extends BaseServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The Constant SESSION_BR. */
	private static final String SESSION_BR = "session";


	/** The Constant NUM_PAGINA. */
	private static final String NUM_PAGINA = "numPag";


	/** The Constant RANGO. */
	private static final int RANGO = 100;


	/** The Constant SALTO. */
	private static final String SALTO = "\n";


	/** The Constant TIPO_OPER. */
//	private static final String TIPO_OPER = "S";

        /**  Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion. */
	private static final int BYTES_DOWNLOAD = 1024;

        /**  Constante para titulo de pantalla. */
 //	private static final String TITULO_PANTALLA = "Consulta de cuentas de tipo n&uacute;mero m&oacute;vil";

 	/**  Constante para posicion de menu. */
 	private static final String POS_MENU = "Cuentas &gt; Consulta &gt; Cuentas M&oacute;viles";

	/**  Constante mensaje de error por facultades. */
//	private static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";

	/**  Constante para archivo de ayuda *. */
	private static final String ARCHIVO_AYUDA = "aEDC_ConConfig";//TODO



	/**
	 * Get.
	 *
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "CtasMoviles-> Metodo DoGet", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}

	/**
	 * Post.
	 *
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "CtasMoviles-> Metodo DoPost", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}


	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		String opcion= (String)req.getParameter("opcion");
		EIGlobal.mensajePorTrace( "CtasMoviles opcion " + opcion, EIGlobal.NivelLog.INFO);

		if(opcion==null|| "0".equals(opcion)){
			opcion="1";
		}


		final int op = Integer.parseInt(opcion);
	    final boolean sesionvalida = SesionValida( req, res);
		if ( sesionvalida ){
			if(op==1){
				realizaConsulta(req, res);
			}else if(op==2){
				generaArchivoExport(req, res);
			}
		 }
	}


	/**
	 * Realiza consulta.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void realizaConsulta(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "CtasMoviles-> Metodo realizaConsulta", EIGlobal.NivelLog.DEBUG);

		final HttpSession sess = req.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

		final String descErr = req.getParameter("descErr");
		final ConsultaCtasMovilesBO consCtaBO = new ConsultaCtasMovilesBO();
		final ConsultaCtasMovilesBean consCtaBean = new ConsultaCtasMovilesBean();

		final int numPag = req.getParameter(NUM_PAGINA)!=null?Integer.parseInt(req.getParameter(NUM_PAGINA)):1;
		int totalPaginas = 1;
		int regTotal = 0;

		consCtaBean.setDescErr(descErr);

		consCtaBean.setContrato(session.getContractNumber());
		EIGlobal.mensajePorTrace("Descripcion del Error ->" + consCtaBean.getDescErr(),
					EIGlobal.NivelLog.INFO);

        consCtaBean.setRegIni((numPag-1)*RANGO);
		consCtaBean.setRegFin(numPag*RANGO);

		final List<CuentaMovilBean> ctasList = consCtaBO.consultaCtasMoviles(consCtaBean);

		if(ctasList != null && !ctasList.isEmpty()){
			req.setAttribute("ctasList", ctasList);
			regTotal = ctasList.get(0).getTotalRegistros();

			EIGlobal.mensajePorTrace("Registros totales ->" + regTotal, EIGlobal.NivelLog.INFO);
		}else{
			despliegaPaginaErrorURL("No existen datos disponibles para su consulta.","Consulta de cuentas m&oacute;viles","Cuentas &gt; Consulta &gt; Cuentas M&oacute;viles",ARCHIVO_AYUDA,req,res);
			return;
		}


		if(regTotal>RANGO){
			totalPaginas = regTotal/RANGO;
			if(regTotal%RANGO > 0){
				totalPaginas++;
			}
		}

		req.setAttribute("totalPaginas",totalPaginas);
		req.setAttribute("numPag",numPag);
		req.setAttribute("descErrC", descErr);

		generaMenu(req, session, POS_MENU);


		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			try {
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_NUMERO_MOVIL);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			bt.setNombreArchivo(session.getUserID8()+".ambcie");
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CtaMoviles - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CtaMoviles - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}

		/**  Instancia para la bitacora admin *. */
	/*	final BitaAdminBean admin = new BitaAdminBean();
		admin.setTipoOp(TIPO_OPER);
		BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.EA_CUENTAS_CONSULTA_CONSULTA_NUMERO_MOVIL, admin, boolLogin);*/
		evalTemplate("/jsp/ctasMoviles.jsp", req, res);
	}



	/**
	 * Despliega pagina error url.
	 *
	 * @param Error the error
	 * @param param1 the param1
	 * @param param2 the param2
	 * @param param3 the param3
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/

		EIGlobal.mensajePorTrace( "despliegaPaginaErrorURL-> entra ", EIGlobal.NivelLog.DEBUG);

		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		final EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "ContCtas_AL");

		EIGlobal.mensajePorTrace( "despliegaPaginaErrorURL-> sale ", EIGlobal.NivelLog.DEBUG);

		final RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}


	/**
	 * Genera menu.
	 *
	 * @param req the req
	 * @param session the session
	 * @param headerVal the header val
	 */
	private void generaMenu(HttpServletRequest req, BaseResource session, String headerVal){

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Cuentas M&oacute;viles",headerVal,ARCHIVO_AYUDA,req));

	}


	/**
	 * Genera archivo export.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void generaArchivoExport(HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException
	{
		try {
			final HttpSession sess = req.getSession();
			final BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

			List<CuentaMovilBean> ctasMovilesList = new ArrayList<CuentaMovilBean>();
			final ConsultaCtasMovilesBO movilesBO =  new ConsultaCtasMovilesBO();
			final StringBuilder moviles = new StringBuilder();

			final ConsultaCtasMovilesBean conCtasMovBean = new ConsultaCtasMovilesBean();
			conCtasMovBean.setContrato(session.getContractNumber());
			conCtasMovBean.setRegIni(0);
			conCtasMovBean.setRegFin(Integer.MAX_VALUE);

			ctasMovilesList = movilesBO.consultaCtasMoviles(conCtasMovBean);

			res.setContentType("text/plain");
			res.setHeader("Content-Disposition",
			    "attachment;filename=CuentasMoviles.doc");

			for (CuentaMovilBean cuenta : ctasMovilesList) {
				moviles.append(StringUtils.rightPad(cuenta.getNumeroMovil(), 20));
				moviles.append(StringUtils.rightPad(cuenta.getTitular(), 40));
				moviles.append(StringUtils.rightPad(cuenta.getCveBanco(), 5));
				moviles.append(StringUtils.rightPad(cuenta.getDescBanco(), 40)).append(SALTO);
			}

			final InputStream input =  new ByteArrayInputStream(moviles.toString().getBytes("UTF8"));

			int read = 0;
			final byte[] bytes = new byte[BYTES_DOWNLOAD];
			final OutputStream os = res.getOutputStream();

			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
			os.flush();
			os.close();
        } catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
     }

}