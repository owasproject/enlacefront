package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.sql.*;

//VSWF
public class MTI_Consultar extends BaseServlet
{

	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void service( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		String contrato     = "";
		String usuario      = "";
		String clavePerfil  = "";
		String Result       = "";

		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("MTI_Consultar - execute(): Entrando a Consultas de Trans. Inter.", EIGlobal.NivelLog.INFO);

		String modulo = (String) req.getParameter("Modulo");

        boolean sesionvalida = SesionValida(req, res);
        if(sesionvalida)
        {
			//sucursalOpera     = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			//Variables de sesion ...
			contrato          = session.getContractNumber();
			usuario           = session.getUserID8();
			clavePerfil       = session.getUserProfile();

			if(modulo.equals("0"))
					iniciaConsultar(req, res);
			else if(modulo.equals("1"))
				generaTablaConsultar(Result,  clavePerfil, usuario, contrato, req, res);
         }
		 else if(sesionvalida)
		 {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
		 }
		 EIGlobal.mensajePorTrace("MTI_Consultar - execute(): Saliendo de Consultas de Trans. Inter.", EIGlobal.NivelLog.INFO);
    }

/*************************************************************************************/
/************************************************** Inicio de Consultas Modulo=0     */
/*************************************************************************************/
    public int iniciaConsultar(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
       StringBuffer strTabla     = new StringBuffer("");
       StringBuffer varFechaHoy  = new StringBuffer("");
	   String strInhabiles = armaDiasInhabilesJS();

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   //TODO BIT , inicio de consulta de Transferencias Internacionales
	   //TODO BIT CU2091 Inicia  consulta desde cambios,BIT CU2101 Inicio de consulta de transferencias internacionales
		/*
		 * VSWF-***-I
		 */
	   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			/*HGG-I*/
			if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO))
					.equals(BitaConstants.ET_CAMBIOS)) {
				bt.setNumBit(BitaConstants.ET_CAMBIOS_INICIO_CONS_TRANSF_INTERN);
			}
			if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO))
					.equals(BitaConstants.ET_TRANSFERENCIAS_INTER)) {
				bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_INICIA_CONS_TRANSF_INTER);
			}
			if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)) {
				bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_CONSULTA_DB);
			}

			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			/*HGG-F*/

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	   }
		/*
		 * VSWF-***-F
		 */

/************************************************** Genera tabla de busqueda ... */
       strTabla.append("\n<p><br>");
       strTabla.append("\n  <table width=250 border=0 cellspacing=2 cellpadding=3 class='tabfonbla' align=center>");
	   strTabla.append("\n    <tr>");
       strTabla.append("\n     <td class='tittabdat' colspan=2> Periodo a consultar</td>");
       strTabla.append("\n    </tr>");
	   strTabla.append("\n    <tr align=center>");
       strTabla.append("\n     <td class='textabdatcla' valign=top colspan=2>");
	   strTabla.append("\n      <table width=100% border=0 cellspacing=5 cellpadding=0>");
	   strTabla.append("\n        <tr>");
	   strTabla.append("\n          <td class='textabdatcla'>");
	   strTabla.append("\n          <input type=radio name='Busqueda' value='HOY' onClick='cambiaFechas(this);' checked>");
	   strTabla.append("\n          Del d&iacute;a</td>");
	   strTabla.append("\n        <td>&nbsp;</td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n        <td class='textabdatcla'>");
	   strTabla.append("\n          <input type=radio name='Busqueda' value='HIS' onClick='cambiaFechas(this);'>");
	   strTabla.append("\n          Hist&oacute;rico </td>");
	   strTabla.append("\n        <td>&nbsp;</td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n        <td class='textabdatcla' align=right> De la fecha:</td>");
	   strTabla.append("\n        <td>");
	   //strTabla.append("\n          <input type=text name=FechaIni value='"+generaFechaAnt("FECHA")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
	   strTabla.append("\n          <input type=text name=FechaIni value='");
	   strTabla.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   strTabla.append("' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
	   strTabla.append("\n        </td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n       <td class='textabdatcla' align=right>A la fecha:</td>");
	   strTabla.append("\n        <td>");
	   //strTabla.append("\n          <input type=text name=FechaFin value='"+generaFechaAnt("FECHA")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(1);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Final' align='absmiddle'></a>");
	   strTabla.append("\n          <input type=text name=FechaFin value='");
	   strTabla.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   strTabla.append("' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(1);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Final' align='absmiddle'></a>");
	   strTabla.append("\n        </td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n     </table>");
	   strTabla.append("\n    </td>");
	   strTabla.append("\n   </tr>");
       strTabla.append("\n  </table>");
       strTabla.append("\n  <input type=hidden name=Numero value=");
	   strTabla.append(Integer.toString(Global.MAX_REGISTROS));
	   strTabla.append(">");
       strTabla.append("\n  <table border=0 cellspacing=0 cellpading=0 align=center>");
       strTabla.append("\n    <tr align=\"center\"  id=\"enviar2\" style=\"visibility:hidden\" class=\"tabmovtex\">");
       strTabla.append("\n    	<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>");
       strTabla.append("\n    </tr>");
	   strTabla.append("\n    <tr id=\"enviar\">");
       strTabla.append("\n      <td colspan=3 align=center><br><a href='javascript:Enviar();'><img src='/gifs/EnlaceMig/gbo25220.gif' border=0></a></td>");
       strTabla.append("\n    </tr>");
       strTabla.append("\n  </table>");

/************************************************** Arreglos de fechas para 'Hoy' ... */
       varFechaHoy.append("  //Fecha De Hoy");
       varFechaHoy.append("\n  dia=new Array(");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  mes=new Array(");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  anio=new Array(");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(");");

       varFechaHoy.append("\n\n  //Fechas Seleccionadas");
       varFechaHoy.append("\n  dia1=new Array(");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  mes1=new Array(");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  anio1=new Array(");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  Fecha=new Array('");
	   varFechaHoy.append(generaFechaAnt("FECHA"));
	   varFechaHoy.append("','");
	   varFechaHoy.append(generaFechaAnt("FECHA"));
	   varFechaHoy.append("');");

       req.setAttribute("DiasInhabiles",strInhabiles);
	   req.setAttribute("VarFechaHoy",varFechaHoy.toString());
       req.setAttribute("Modulo","1");
       req.setAttribute("Tabla",strTabla.toString());
       req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
	   req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   req.setAttribute("FechaAyer",formateaFecha("FECHA"));
	   req.setAttribute("FechaPrimero","01/"+formateaFecha("MES2")+"/"+formateaFecha("ANIO"));

	   req.setAttribute("MenuPrincipal", session.getStrMenu());
	   req.setAttribute("newMenu", session.getFuncionesDeMenu());
	   req.setAttribute("Encabezado", CreaEncabezado("Consultas de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Consultas","s27580h",req));

	   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

       evalTemplate("/jsp/MTI_Consultar.jsp", req, res);
       return 1;
     }

/*************************************************************************************/
/*********************************************** Resultados de la busqueda Modulo=1  */
/*************************************************************************************/
    public int generaTablaConsultar(String Result, String clavePerfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
     {
	   String Trama="";
       String tabla="";
       String totales="";
       String rconsulta="";
       String pie="";

	   StringBuffer botones=new StringBuffer("");
	   StringBuffer tablaRegistros=new StringBuffer("");

	   String mensajeError="";
	   String tipoOperacion="DIIF"; //servicio de consulta internacional.

       String fechaIni=(String) req.getParameter("FechaIni");
       String fechaFin=(String) req.getParameter("FechaFin");
       String tipoBusqueda=(String) req.getParameter("Busqueda");

       Hashtable htResult=null;

	   boolean comuError=false;

       int pagina=Integer.parseInt((String) req.getParameter("Pagina"));
       int regPagina=Integer.parseInt((String) req.getParameter("Numero"));
       int fin=0;

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): Generando Fechas de consulta.", EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): fechaIni="+fechaIni+", fechaFin="+fechaFin, EIGlobal.NivelLog.INFO);

/************************************** Ejecuta servicio recupera TCTRED ***/
       //Cabecera
       Trama = IEnlace.medioEntrega2 + "|" +
	  	 	   usuario 				 + "|" +
			   tipoOperacion		 + "|" +
       		   contrato				 + "|" +
	       	   usuario 				 + "|" +
       		   clavePerfil 			 + "|" +
			   fechaIni				 + "@" +
		       fechaFin				 + "@";

	   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

	   ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   try {
			htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	   } catch(Exception e){}

	   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
/*************************************** Recuperar Transferencias TCTArchivo *********/

     String cadenaArchivo="";
     String arcLinea="";

	 StringBuffer cadenaTCT=new StringBuffer("");

	 //###### Cambiar ....
	 String nombreArchivo=IEnlace.LOCAL_TMP_DIR + "/" + usuario;
	 //String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/consulTI.prueba";

     boolean status=false;
     int totalLineas=0;

	 EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);
	 EI_Tipo TCTArchivo = new EI_Tipo(this);

	  if(Result==null || Result.equals("null"))
	   {
		 comuError = true;
		 mensajeError = "Problemas en el servicio de consulta. Por favor intente mas tarde.";
	   }
	  else
	   {
		 // **********************************************************  Copia Remota
		 ArchivoRemoto archR = new ArchivoRemoto();
		 if(!archR.copia(usuario))
		   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
		 // **********************************************************

		 if(ArcEnt.abreArchivoLectura())
		  {
			boolean noError = true;
			do
			 {
			   arcLinea=ArcEnt.leeLinea();
			   if(totalLineas==0)
				if((arcLinea.substring(0,2).equals("OK") || arcLinea.substring(0,8).equals("TCT_9010")))
				 status=true;
			   if(!arcLinea.equals("ERROR"))
				{
				  if(arcLinea.substring(0,5).trim().equals("ERROR"))
					break;
				  totalLineas++;
				  if(totalLineas>=2)
				   {
					 cadenaTCT.append(arcLinea.substring(0,arcLinea.length()-1));
					 cadenaTCT.append(" @");
				   }
				}
			   else
				 noError=false;
			 }while(noError);
			ArcEnt.cierraArchivo();

			if(arcLinea.substring(0,5).trim().equals("ERROR") && totalLineas<=1 && !status)
			  {
				comuError = true;
				mensajeError = "Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
			  }
		  }
		 else
		  {
			comuError = true;
			mensajeError = "Problemas de comunicacion. Por favor intente mas tarde.";
		  }
	   }


	    //TODO BIT CU3131 Genera la consulta para el flujo de Transferencias Iternacionales del modulo de Tesorer�a
	    //TODO BIT CU3121 Consulta Cambios(Transferencias Internacionales M.N. Dlls./ Dlls. M.N.)
		//TODO BIT CU2091 Consulta (Cambios),BIT CU2101 Consulta Transferencias Internacionales
		/*
		 * VSWF-HGG-I
		 */
	  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try {
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_CAMBIOS)){
				bt.setNumBit(BitaConstants.ET_CAMBIOS_CONS_TRANSF_INTERN);
			}
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
				bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_CONS_TRANSF_INTER);
			}

			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}
			if(Result!=null){
				if(Result.length()>8){
					bt.setIdErr(Result.substring(0,8));
				}else{
					bt.setIdErr(Result.trim());
				}
			}else{
				bt.setIdErr(" ");
			}
			if (usuario != null) {
				bt.setUsr(usuario.trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	  }
		/*
		 * VSWF-HGG-F
		 */
/********************************************************************************/
    if(!comuError)
     {
       TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());
       if(TCTArchivo.totalRegistros>=1)
        {
		  botones.append("\n <br>");
		  botones.append("\n <table border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
          if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina(0);' class='texfootpaggri'>Anterior</a>&nbsp;");

		  if(calculaPaginas(TCTArchivo.totalRegistros,regPagina)>=2)
           {
			 for(int i=1;i<=calculaPaginas(TCTArchivo.totalRegistros,regPagina);i++)
			  if(pagina+1==i)
			   {
                 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
			     botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i+3));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a>&nbsp;");
			   }
           }
			EIGlobal.mensajePorTrace("MTI_Consultar - calculando paginas", EIGlobal.NivelLog.INFO);
          if(TCTArchivo.totalRegistros>((pagina+1)*regPagina))
           {
             botones.append("<a href='javascript:BotonPagina(1);' class='texfootpaggri'>Siguiente</a> &gt;");
             fin=regPagina;
           }
          else
             fin=TCTArchivo.totalRegistros-(pagina*regPagina);

		  EIGlobal.mensajePorTrace("MTI_Consultar - colocando botones paginas", EIGlobal.NivelLog.INFO);

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");

           /*
            0 contrato
            1 tipo_orden
            2 numero_orden
            3 referencia
            4 estatus cargo
			5 estatus abono+++++
            6 tipo_operacion
            7 cve_pais
            8 cve_divisa_abono
            9 cve_divisa_cargo
           01 instrumento
           11 usuario
           12 cve_aba
		   13 cve_esp
		   14 cta_cargo
		   15 cta_abono
		   16 folio_liq_cargo
		   17 folio_liq_abono
		   18 importe_pesos
		   19 importe_dolares
		   20 importe_divisa
		   21 tipo_cambio_pesos
		   22 tipo_cambio_dolar
		   23 fecha_envio
		   24 fecha_aplicacion
		   25 banco
		   26 sucursal
		   27 ciudad
		   28 concepto
		   29 beneficiario
		   30 referencia mecanismo (FED)
		   31 referencia de confirmacion (rastreo)
           */

/************************************************* Encabezado de la pagina ... */
		  tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");

          tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
          tablaRegistros.append("\n  <tr>");
          tablaRegistros.append("\n    <td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>");
		  tablaRegistros.append(Integer.toString(TCTArchivo.totalRegistros));
		  tablaRegistros.append("</font> para el contrato: <font color=red>");
		  tablaRegistros.append(contrato);
		  tablaRegistros.append("</font>&nbsp;</td>");
          tablaRegistros.append("\n    <td align=right class='textabref'><b> del <font color=red>");
		  tablaRegistros.append(fechaIni);
		  tablaRegistros.append("</font> al <font color=red>");
		  tablaRegistros.append(fechaFin);
		  tablaRegistros.append("</font></td>");
          tablaRegistros.append("\n  </tr>");
          tablaRegistros.append("\n  <tr>");
          tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Pagina <font color=blue>");
		  tablaRegistros.append(Integer.toString(pagina+1));
		  tablaRegistros.append("</font> de <font color=blue>");
		  tablaRegistros.append(Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina)));
		  tablaRegistros.append("</font>&nbsp;</b></td>");
          tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+1));
		  tablaRegistros.append(" - ");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+fin));
		  tablaRegistros.append("</font></b>&nbsp;</td>");
          tablaRegistros.append("\n  </tr>");
          tablaRegistros.append("\n </table>");

		  EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);

		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");

		  tablaRegistros.append(seleccionaRegistros(pagina*regPagina,fin,TCTArchivo,req));

		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n </table>");

		  req.setAttribute("ClaveBanco",session.getClaveBanco());
		  EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): La clave de banco ... (ok) arriba", EIGlobal.NivelLog.INFO);

          HttpSession ses=req.getSession();

		  if( ses.getAttribute("TotalTrans")!=null )
		      ses.removeAttribute("TotalTrans");
  			  ses.setAttribute("TotalTrans", TCTArchivo.strOriginal);
		  if( ses.getAttribute("Busqueda")!=null )
			  ses.removeAttribute("Busqueda");
			  ses.setAttribute("Busqueda", tipoBusqueda);
		  if( ses.getAttribute("FechaIni")!=null )
			  ses.removeAttribute("FechaIni");
			  ses.setAttribute("FechaIni", fechaIni);
		  if( ses.getAttribute("FechaFin")!=null )
		      ses.removeAttribute("FechaFin");
  			  ses.setAttribute("FechaFin", fechaFin);
		  if( ses.getAttribute("FechaHoy")!=null )
			  ses.removeAttribute("FechaHoy");
			  ses.setAttribute("FechaHoy", EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		  if( ses.getAttribute("Botones")!=null )
			  ses.removeAttribute("Botones");
			  ses.setAttribute("Botones", botones.toString());
		  if( ses.getAttribute("Registros")!=null )
		      ses.removeAttribute("Registros");
  			  ses.setAttribute("Registros", tablaRegistros.toString());
		  if( ses.getAttribute("Numero")!=null )
			  ses.removeAttribute("Numero");
			  ses.setAttribute("Numero", Integer.toString(regPagina));
		  if( ses.getAttribute("Pagina")!=null )
			  ses.removeAttribute("Pagina");
			  ses.setAttribute("Pagina", Integer.toString(pagina));
		  if( ses.getAttribute("Modulo")!=null )
			  ses.removeAttribute("Modulo");
			  ses.setAttribute("Modulo", "1");
          if( ses.getAttribute("Exportar")!=null )
			  ses.removeAttribute("Imprimir");
			  ses.setAttribute("Imprimir","<a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a>");
		  if( ses.getAttribute("Exportar")!=null )
			  ses.removeAttribute("Exportar");
			  ses.setAttribute("Exportar",exportarArchivo(TCTArchivo,usuario));
		  if( ses.getAttribute("web_application")!=null )
			  ses.removeAttribute("web_application");
			  ses.setAttribute("web_application",Global.WEB_APPLICATION);

          req.setAttribute("TotalTrans",TCTArchivo.strOriginal);
          req.setAttribute("Busqueda",tipoBusqueda);
          req.setAttribute("FechaIni",fechaIni);
          req.setAttribute("FechaFin",fechaFin);
          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
          req.setAttribute("Botones",botones.toString());
          req.setAttribute("Registros",tablaRegistros.toString());
          req.setAttribute("Numero",Integer.toString(regPagina));
          req.setAttribute("Pagina",Integer.toString(pagina));
          req.setAttribute("Modulo","1");
		  req.setAttribute("Imprimir","<a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a>");
		  req.setAttribute("Exportar",exportarArchivo(TCTArchivo,usuario));
		  req.setAttribute("web_application",Global.WEB_APPLICATION);

		  if( ses.getAttribute("MenuPrincipal")!=null )
		      ses.removeAttribute("MenuPrincipal");
  			  ses.setAttribute("MenuPrincipal", session.getStrMenu());
		  if( ses.getAttribute("newMenu")!=null )
			  ses.removeAttribute("newMenu");
			  ses.setAttribute("newMenu", session.getFuncionesDeMenu());
		  if( ses.getAttribute("Encabezado")!=null )
			  ses.removeAttribute("Encabezado");
			  ses.setAttribute("Encabezado", CreaEncabezado("Consultas de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Consultas","s27590h",req));

		  req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
		  req.setAttribute("Encabezado", CreaEncabezado("Consultas de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Consultas","s27590h",req));

		  if( ses.getAttribute("NumContrato")!=null )
		      ses.removeAttribute("NumContrato");
  			  ses.setAttribute("NumContrato", session.getContractNumber());
		  if( ses.getAttribute("NomContrato")!=null )
			  ses.removeAttribute("NomContrato");
			  ses.setAttribute("NomContrato", session.getNombreContrato());
		  if( ses.getAttribute("NumUsuario")!=null )
			  ses.removeAttribute("NumUsuario");
			  ses.setAttribute("NumUsuario", session.getUserID8());
		  if( ses.getAttribute("NomUsuario")!=null )
		      ses.removeAttribute("NomUsuario");
  			  ses.setAttribute("NomUsuario", session.getNombreUsuario());
		  EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): la clave de banco ... (ses)", EIGlobal.NivelLog.INFO);
		  if( ses.getAttribute("ClaveBanco")!=null )
		      ses.removeAttribute("ClaveBanco");
  			  ses.setAttribute("ClaveBanco", session.getClaveBanco());

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          evalTemplate("/jsp/MTI_Consultar.jsp", req, res);
        }
       else
		{
		  EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): No existen Registros.", EIGlobal.NivelLog.INFO);
          despliegaPaginaErrorURL("No se encontraron movimientos de Transferencias Internacionales realizados en este intervalo de tiempo.","Consultas de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Consultas", "s27590h",req, res);
		}
    }
    else
	{
	   EIGlobal.mensajePorTrace("MTI_Consultar - generaTablaConsultar(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
       despliegaPaginaErrorURL(mensajeError,"Consultas de Transferencias Internacionales","Transferencias &gt; Internacionales &gt; Consultas", "s27590h",req, res);
	}
    return 1;
  }

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		String contrato_ = session.getContractNumber();
		if(contrato_!=null)
			EIGlobal.mensajePorTrace("MTI_Consultar - despliegaPaginaErrorURL(): El contrato no es nulo ="+contrato_, EIGlobal.NivelLog.INFO);
		else
		  {
			EIGlobal.mensajePorTrace("MTI_Consultar - despliegaPaginaErrorURL(): El contrato es nulo le ponemos cadena vacia", EIGlobal.NivelLog.INFO);
			contrato_ ="";
		  }

		request.setAttribute("NumContrato", contrato_);
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "MTI_Consultar?Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

/*************************************************************************************/
/********************************************************** Esportacion de Archivo   */
/*************************************************************************************/
 public String exportarArchivo(EI_Tipo Registros,String usuario)
   {
     String nombreArchivo=usuario+".cti";
	 String strBoton="";
	 StringBuffer arcLinea;
	 String rfcUsuario="";
	 String importeIVA="";

     EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
	 EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

     if(ArcSal.creaArchivo())
      {
        int c[]={14,15,18,19,20,9,21,24,28,28,28,5,2,16,31,30,0,1};
		// Indice del elemento deseado
        int b[]={16,20,16,16,16,4,14,19,30,13,13,20,10,10,14,20,10,10};
		// Longitud maxima del campo para el elemento
		//cta,cta, imp mn usd, imp div, imp, div

		EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): Exportando Registros.", EIGlobal.NivelLog.INFO);
        for(int i=0;i<Registros.totalRegistros;i++)
         {
           arcLinea=new StringBuffer("");

		   if(Registros.camposTabla[i][28].length()>60)
			 {
			   rfcUsuario=Obten_RFC(Registros.camposTabla[i][28]);
			   importeIVA=Obten_IVA(Registros.camposTabla[i][28]);
			 }
		    else
		     {
			   rfcUsuario="";
			   importeIVA="";
			 }
            String tmp1="";

           for(int j=0;j<16;j++)
			 {
			   if(j==2)
				 arcLinea.append(ArcSal.formateaCampo(formatea_importe(Registros.camposTabla[i][c[j]]),b[j]));
			   else
			   if(j==3 || j==4)
				 {
				   tmp1=formatea_importe(Registros.camposTabla[i][c[j]]);
				   arcLinea.append(ArcSal.formateaCampo(tmp1.replace('$',' ').trim(),b[j]));
				 }
			   else
			   if(j==9)
				 arcLinea.append(ArcSal.formateaCampo(rfcUsuario,b[j]));
			   else
				if(j==10)
				 arcLinea.append(ArcSal.formateaCampo(importeIVA,b[j]));
			   else
				   /*
				 if(j==6)
				  arcLinea.append(Obten_tipo_cambio(Registros.camposTabla[j][9],Registros.camposTabla[j][21],Registros.camposTabla[j][21]));
			   else*/
				 arcLinea.append(ArcSal.formateaCampo(Registros.camposTabla[i][c[j]],b[j]));
			 }
           arcLinea.append("\n");
           if(!ArcSal.escribeLinea(arcLinea.toString()))
			 EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): Algo salio mal escribiendo "+arcLinea.toString(), EIGlobal.NivelLog.INFO);
         }

		EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): Se llevo a cabo la exportacion", EIGlobal.NivelLog.INFO);

        ArcSal.cierraArchivo();

        // Boton de Exportacion de Archivo.
		//*************************************************************
		ArchivoRemoto archR = new ArchivoRemoto();
        if(archR.copiaLocalARemoto(nombreArchivo,"WEB"))
 	     strBoton="\n<a href='/Download/"+nombreArchivo+"'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
		else
		 EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
		//**************************************************************
      }
     else
      {
        //strBoton="<b>Exportacion no disponible<b>";
		EIGlobal.mensajePorTrace("MDI_Consultar - exportarArhivo(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
      }
	return strBoton;
  }

/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

/*************************************************************************************/
/***************************************************************** fecha Consulta    */
/*************************************************************************************/
   String formateaFecha(String tipo)
    {
      StringBuffer strFecha = new StringBuffer("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);
      do
       {
         Cal.add(Calendar.DATE,-1);
       }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);

      if(tipo.equals("FECHA"))
       {
         if(Cal.get(Calendar.DATE) <= 9)
		  {
            strFecha.append("0");
			strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append(Cal.get(Calendar.DATE));
			strFecha.append("/");
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append("0" );
			strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
	   }

	  if(tipo.equals("MES2"))
	   {
		 if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append("0");
			strFecha.append((Cal.get(Calendar.MONTH)+1));
		  }
         else
           strFecha.append((Cal.get(Calendar.MONTH)+1));
	   }

      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

/*************************************************************************************/
/******************************************************************* fecha Consulta  */
/*************************************************************************************/
   String generaFechaAnt(String tipo)
    {
      StringBuffer strFecha = new StringBuffer("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);

      do
	   {
         Cal.add(Calendar.DATE,-1);
       }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);

      if(tipo.equals("FECHA"))
       {
         if(Cal.get(Calendar.DATE) <= 9)
		  {
            strFecha.append("0" );
			strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append("/");
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append("0");
			strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
        }
      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

/*************************************************************************************/
/********************************* tabla de movimientos seleccionados por intervalo  */
/*************************************************************************************/
   String seleccionaRegistros(int ini, int fin, EI_Tipo Trans, HttpServletRequest req)
    {
      StringBuffer strTabla       = new StringBuffer("");
      String colorbg        = "";
	  String str            = "";
	  StringBuffer strComprobante;
	  ValidaCuentaContrato valCtas = null;

	  //boolean muestraCompro=false;

	  EI_Tipo Consultas = new EI_Tipo(this);

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

	  try{
		  valCtas = new ValidaCuentaContrato();
	      if(Trans.totalRegistros>=1)
	       {
			 Consultas.iniciaObjeto(Trans.strOriginal);

	         strTabla.append("\n<table border=0 align=center>");
	         strTabla.append("\n<tr>");

	         strTabla.append("\n<th class='tittabdat' align='left'>Ref. de Cargo</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Cuenta Cargo</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Cuenta Abono</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>RFC Beneficiario</th>");
			 strTabla.append("\n<th class='tittabdat' align='center'>Importe M.N.</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Importe USD</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Importe Divisa</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Importe IVA</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Tipo Cambio</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Fecha Env&iacute;o</th>");
	         strTabla.append("\n<th class='tittabdat' align='center' width='200'>Concepto</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>Estatus</th>");
	         strTabla.append("\n<th class='tittabdat' align='center'>No Orden</th>");
			 //strTabla.append("\n<th class='tittabdat' align='center'>Referencia</th>");
	         strTabla.append("\n<th class='tittabdat' align='left'>Ref. de Envio</th>");
			 strTabla.append("\n<th class='tittabdat' align='left'>Ref. de Confirmaci&oacute;n (FED)</th>");
	         strTabla.append("\n</tr>");

	         EIGlobal.mensajePorTrace("MTI_Consultar - seleccionaRegistros(): Generando la Tabla.", EIGlobal.NivelLog.INFO);

	         if(fin>Trans.totalRegistros)
	           fin=Trans.totalRegistros;

			 EIGlobal.mensajePorTrace("MTI_Consultar - seleccionaRegistros(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);
	        //modificacion para integracion
	        //llamado_servicioCtasInteg(IEnlace.MCargo_TI_pes_dolar," "," "," ",session.getUserID8()+".ambci",req);
	        String datoscta[]=null;
			String titularcargo="";

			String ctaO="", ctaD="";
			 for(int i=ini;i<(ini+fin);i++)
	          {
				if((i%2)==0)
	             colorbg="class='textabdatobs'";
	           else
	             colorbg="class='textabdatcla'";
	            strTabla.append("\n<tr>");

				String rfcUsuario = "", concepto = "", importeIVA = "", status_oper = "", tipo_cambio = "", referenciameca="", importeTmp="";
				concepto      = Obten_Concepto(Trans.camposTabla[i][28]);
				rfcUsuario    = Obten_RFC(Trans.camposTabla[i][28]);
				importeIVA    = Obten_IVA(Trans.camposTabla[i][28]);
				status_oper   = Obten_status(Trans.camposTabla[i][5]);
				tipo_cambio   = Obten_tipo_cambio(Trans.camposTabla[i][9],Trans.camposTabla[i][21],Trans.camposTabla[i][21]);
				referenciameca=Trans.camposTabla[i][30];

				//Modificacion para mostrar todos los comprobantes para cualquier estatus

				//if(status_oper.equals("ENVIADA") ||	status_oper.equals("CONFIRMADA"))
				//muestraCompro=true;

				//Modif PVA 08/08/2002 y luega ARV 02/10/2002
	            String divisa_real="";
				divisa_real=Trans.camposTabla[i][8];
	            //operaciones de cambios cargo d�lar
				if(Trans.camposTabla[i][10].equals("CHDO") )/*&& esCuentaDolares(Trans.camposTabla[i][14])) divisa_real=Trans.camposTabla[i][9];*/
				  divisa_real="USD";

				//if(muestraCompro)
				//{
					int j=0;
					EIGlobal.mensajePorTrace("MTI_Consultar - seleccionaRegistros(): Se busca titular para cuenta: ("+Trans.camposTabla[i][14]+")", EIGlobal.NivelLog.INFO);
					//datoscta=BuscandoCtaInteg(Trans.camposTabla[i][14],IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci",IEnlace.MCargo_TI_pes_dolar);
					datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
							  								  session.getUserID8(),
													          session.getUserProfile(),
													          Trans.camposTabla[i][14].trim(),
													          IEnlace.MCargo_TI_pes_dolar);
	                if(datoscta!=null)
				     {
					   if((datoscta[4]!=null)&&(!datoscta[4].trim().equals("")))
					    titularcargo =datoscta[4];
					   else
	                    titularcargo="SIN TITULAR";
	                 }
				    else
	                  titularcargo="SIN TITULAR";

					if(Trans.camposTabla[i][10].equals("CHDO") )
					 {
						String cta_origen="";
						String cta_abono="";
						String contadorok="1";

						//if( datoscta!=null && datoscta[2].trim().equals("6") )  //si cuenta cargo es pesos y abono d�lar
						 //{CSA
						   cta_origen=Trans.camposTabla[i][14]+ " "+titularcargo;ctaO=cta_origen;
						   cta_abono=Trans.camposTabla[i][15]+ " "+Trans.camposTabla[i][29].trim();ctaD=cta_abono;
						 //}
						//else
						 //{
						 //  cta_origen=Trans.camposTabla[i][14]+ " "+Trans.camposTabla[i][29].trim();ctaO=cta_origen;
						 //  cta_abono=Trans.camposTabla[i][15]+ " "+titularcargo;ctaD=cta_abono;
						 //}

						/*
						"
						cta_origen|
						cta_destino|
						importemn|
						importeusd|
						TipoCambio|
						fecha|
						concepto|
						numorden|
						referencia|
						RFC|
						Iva@
						*/

						importeIVA=Obten_IVA(Trans.camposTabla[i][28],false);

					    strComprobante=new StringBuffer("");
						strComprobante.append(cta_origen.trim());
						strComprobante.append("|");
						strComprobante.append(cta_abono.trim());
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][18].trim());
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][19].trim());
						strComprobante.append("|");
						strComprobante.append(tipo_cambio.trim());
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][24].trim() );
						strComprobante.append("|");
						strComprobante.append(concepto.trim() );
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][2].trim() );
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][31].trim() );
						strComprobante.append("|");
						strComprobante.append(rfcUsuario.trim() );
						strComprobante.append(" |");
						strComprobante.append(importeIVA.trim());
						//Q6973 Getronics
						strComprobante.append(" |");
						strComprobante.append(status_oper.trim());

						str="GenerarComprobante('"+strComprobante.toString()+"', 1);\"";
						//str="alert('No disponible por el momento.');";
					 }
					else
					 {
					    strComprobante=new StringBuffer("");
					    strComprobante.append(Trans.camposTabla[i][16]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][2]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][31]);
						strComprobante.append("|");
						strComprobante.append(concepto);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][14]);
						strComprobante.append(" ");
						strComprobante.append(titularcargo);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][15]);
						strComprobante.append(" ");
						strComprobante.append(Trans.camposTabla[i][29].trim() );
						strComprobante.append("|");
						strComprobante.append(rfcUsuario);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][25]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][7]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][27]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][8]);
						strComprobante.append("|");
						strComprobante.append(tipo_cambio);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][18]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][19]);
						strComprobante.append("|");
						strComprobante.append(Trans.camposTabla[i][20]);
						strComprobante.append("|");
						strComprobante.append(importeIVA);
						strComprobante.append("|");
						strComprobante.append(status_oper);
						strComprobante.append("|");
						strComprobante.append(referenciameca);
						strComprobante.append("|");
						strComprobante.append("@");

						str="despliegaDatos('"+strComprobante.toString()+"');";
					 }
				    strTabla.append("<td ");
					strTabla.append(colorbg);
					strTabla.append(" align='right'><a href=\"javascript:");
					strTabla.append(str);
					strTabla.append("\">");
					strTabla.append(Trans.camposTabla[i][16]);
					strTabla.append("</a>&nbsp;</td>");

				// }
				//else
				//strTabla.append("<td ");
				//strTabla.append(colorbg);
				//strTabla.append(" align='right'>");
				//strTabla.append(Trans.camposTabla[i][16]);
				//strTabla.append("&nbsp;</td>");

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle'>");
				strTabla.append(Trans.camposTabla[i][14]);
				strTabla.append("&nbsp;</td>"); //cargo
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle'>");
				strTabla.append(Trans.camposTabla[i][15]);
				strTabla.append("&nbsp;</td>"); //abono
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle'>");
				strTabla.append(rfcUsuario);
				strTabla.append("&nbsp;</td>"); //rfc benef

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(formatea_importe(Trans.camposTabla[i][18]));
				strTabla.append("&nbsp;</td>");  //imp mn
				importeTmp=formatea_importe(Trans.camposTabla[i][19]);
				importeTmp=importeTmp.replace('$',' ').trim();
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(importeTmp);
				strTabla.append("&nbsp;</td>");  // imp usd

				importeTmp=formatea_importe(Trans.camposTabla[i][20]);
				importeTmp=importeTmp.replace('$',' ').trim();

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(divisa_real);
				strTabla.append(" ");
				strTabla.append(importeTmp);
				strTabla.append("&nbsp;</td>"); // imp div

				importeTmp=formatea_importe(importeIVA);
				importeTmp=importeTmp.replace('$',' ').trim();
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(importeTmp);
				strTabla.append("&nbsp;</td>"); //imp iva

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(tipo_cambio);
				strTabla.append("&nbsp;</td>"); //tipo cambio

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='center'>");
				strTabla.append(Trans.camposTabla[i][24]);
				strTabla.append("&nbsp;</td>"); //fecha

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle'>");
				strTabla.append(concepto);
				strTabla.append("&nbsp;</td>"); //concepto

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='center'>");
				strTabla.append(status_oper);
				strTabla.append("&nbsp;</td>"); //estatus

				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(Trans.camposTabla[i][2]);
				strTabla.append("&nbsp;</td>");  //num orden

				//strTabla.append("<td "+colorbg+" valign='middle' align='right'>"+Trans.camposTabla[i][16]+"&nbsp;</td>"); //3 para referencia de enlace
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(Trans.camposTabla[i][31]);
				strTabla.append("&nbsp;</td>");  //ref envio
				strTabla.append("<td ");
				strTabla.append(colorbg);
				strTabla.append(" valign='middle' align='right'>");
				strTabla.append(Trans.camposTabla[i][30]);
				strTabla.append("&nbsp;</td>");  //ref conf (fed)
	            strTabla.append("\n</tr>");


	            //TODO BIT CU3131 Genera la consulta para el flujo de Transferencias Iternacionales del modulo de Tesorer�a
	    	    //TODO BIT CU3121 Consulta Cambios(Transferencias Internacionales M.N. Dlls./ Dlls. M.N.)
	    		/*
	    		 * VSWF-***-I
	    		 */
	            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
	    		try {
	    			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_CAMBIOS)){
		    			BitaHelper bh = new BitaHelperImpl(req, session, sess);

		    			BitaTransacBean bt = new BitaTransacBean();
		    			bt = (BitaTransacBean)bh.llenarBean(bt);

		    			bt.setNumBit(BitaConstants.ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIIF);
		    			if (Trans.camposTabla[i][18] != null) {
							bt.setImporte(Double.parseDouble(Trans.camposTabla[i][18].trim()));
						}
		    			if (divisa_real != null) {
							bt.setTipoMoneda(divisa_real.trim());
						}
		    			if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}
		    			if (Trans.camposTabla[i][14] != null) {
							bt.setCctaOrig(Trans.camposTabla[i][14].trim());
						}
		    			if (Trans.camposTabla[i][15] != null) {
							bt.setCctaDest(Trans.camposTabla[i][15].trim());
						}
		    			if (status_oper != null) {
							bt.setEstatus(status_oper.substring(0, 1).trim());
						}
		    			if (tipo_cambio != null) {
							bt.setTipoCambio(Double.parseDouble(tipo_cambio.trim()));
						}
		    			bt.setServTransTux("DIIF");

		    			BitaHandler.getInstance().insertBitaTransac(bt);
	    			}
	    		}catch(SQLException e){
	    			e.printStackTrace();
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}
	            }
	    		/*
	    		 * VSWF-***-F
	    		 */


	          }
			 strTabla.append("\n <tr>");
			 strTabla.append("\n   <td colspan=8 class='textabref'>Si desea obtener su comprobante, haga click en el n&uacute;mero de refencia subrayado</td>");
	         strTabla.append("\n </tr>");
	         strTabla.append("\n</table>");
	       }
	  }catch(Exception e){
		  EIGlobal.mensajePorTrace("Error en metodo seleccionaRegistros de clase MTI_Consultar->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
	  }finally{
	      try{
	    	  valCtas.closeTransaction();
	      }catch (Exception e) {
	    	  EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
	      }
	  }
	  return strTabla.toString();
    }

/*************************************************************************************/
/************************************************************* Arma dias inhabiles   */
/*************************************************************************************/
  String armaDiasInhabilesJS()
   {
      StringBuffer resultado=new StringBuffer("");
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado.append(diasInhabiles.elementAt(indice).toString());
		 for(indice=1; indice<diasInhabiles.size(); indice++)
		  {
  		    resultado.append( ", " );
			resultado.append(diasInhabiles.elementAt(indice).toString());
		  }
       }
      return resultado.toString();
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas   */
/*************************************************************************************/
  public Vector CargarDias(int formato)
    {
       EIGlobal.mensajePorTrace( "***ChesConsulta.class  &inicia m�todo CargarDias()&", EIGlobal.NivelLog.INFO);

	   Connection Conexion=null;
       PreparedStatement qrDias;
       ResultSet rsDias=null;
       String sqlDias;

	   boolean estado;

       Vector diasInhabiles = new Vector();

       sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha <= sysdate";

       try{
		   Conexion = createiASConn(Global.DATASOURCE_ORACLE);

		   if(Conexion!=null)
			 {
				qrDias = Conexion.prepareStatement(sqlDias);
				if(qrDias!=null)
				  {
					rsDias = qrDias.executeQuery();
					if(rsDias!=null)
					 {
						while ( rsDias.next())
						 {
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0){
								dia  = Integer.toString( Integer.parseInt(dia) );
								mes  = Integer.toString( Integer.parseInt(mes) );
								anio = Integer.toString( Integer.parseInt(anio) );
						  }
						 String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
						 diasInhabiles.addElement(fecha);
					  }											}
					else
						EIGlobal.mensajePorTrace("MDI_Interbancario - cargarDias(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				  }
				 else
					EIGlobal.mensajePorTrace("MDI_Interbancario - cargarDias(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			   }
			  else
				EIGlobal.mensajePorTrace("MDI_Interbancario - cargarDias(): No se pudo crear conexion.", EIGlobal.NivelLog.ERROR);
		}catch( SQLException sqle ){
          sqle.printStackTrace();
		}finally
		  {
			try
			 {
			   Conexion.close();
			   rsDias.close();
			 }catch(Exception e) {}
		  }

		EIGlobal.mensajePorTrace( "***ChesConsulta.class  &termina m�todo CargarDias()&", EIGlobal.NivelLog.INFO);
        return(diasInhabiles);
    }

/*************************************************************************************/
/******************************************************* obtener Datos de Concepto   */
/*************************************************************************************/
  public String Obten_Concepto(String campo)
    {
	  int pos=0;

	  pos=campo.indexOf('+');
	  if(pos>=60)
	    return campo.substring(0,pos);
	  else
	    return campo;
	}

  public String Obten_RFC(String campo)
    {
	  int pos=0, pos2=0;

	  pos=campo.indexOf("+");
	  pos2=campo.indexOf("IVA");

	  if(pos>=60 && pos2>60)
		return campo.substring(pos+1,pos2).trim();
	  return "";
	}

  public String Obten_IVA(String campo)
	{
	  int pos=0;
	  pos=campo.indexOf("IVA");
	  if(pos>60)
	   if(! (campo.substring(pos+3,campo.length()).trim().equals("")) )
		return formatea_importe(campo.substring(pos+3,campo.length()).trim());
	  return "";
	}

  public String Obten_IVA(String campo,boolean formato)
	{
	  int pos=0;
	  pos=campo.indexOf("IVA");
	  if(pos>60)
	   if(! (campo.substring(pos+3,campo.length()).trim().equals("")) )
		{
		  if(formato)
		    return formatea_importe(campo.substring(pos+3,campo.length()).trim());
	      else
			return campo.substring(pos+3,campo.length()).trim();
		}
	  return "";
	}

  public String Obten_tipo_cambio(String d_cargo, String tc_p, String tc_d)
    {
	  String tipoCambio="";

	  /*
	  tc_p=formatea_importe(tc_p);
	  tc_p=tc_p.replace('$',' ').trim();
	  tc_d=formatea_importe(tc_d);
	  tc_d=tc_d.replace('$',' ').trim();
	  */

	  if(d_cargo.equals("MN"))
		 tipoCambio=/*d_cargo+"&nbsp;"+*/tc_p;
	  else
	   {
		 if(d_cargo.trim().equals("DA"))
		  d_cargo="USD";
		 tipoCambio=/*d_cargo+"&nbsp;"+*/tc_d;
	   }
	  return tipoCambio;
	}

  public String Obten_status(String campo){
	/*
	PV- Por verificar
	LI- Listo
	EN- Enviado
	CO- Confirmado
	CA- Cancelado
	ER- Error
	CP- Capturada/ no enviada
	RT- Orden con restricci�n
	MA- Confirmaci�n manual
	AU- Autorizada
	*/
		if (campo.equals("CP") || campo.equals("RT"))
			return "RECIBIDA";
		if (campo.equals("EN") || campo.equals("PV") || campo.equals("LI"))
			return "ENVIADA";
		if (campo.equals("CO") || campo.equals("MA") || campo.equals("AU"))
			return "CONFIRMADA";
		if (campo.equals("CA"))
			return "CANCELADA";
		if (campo.equals("ER"))
			return "ERROR";
	return "";
	}

   static protected String _defaultTemplate ="EnlaceMig/MTI_Consultar";
}