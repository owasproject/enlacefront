/** Banco Santander Mexicano
 *    Clase NomEmpOpciones
 *   @author Gerardo Salazar Vega
 *   @version 1.0
 *   fecha de creacion: 08 de Noviembre del 2000
 *   responsable: Roberto Guadalupe Resendiz Altamirano
 *  @return SUCCESS resultado de la ejecucion del applogic
 *   descripcion: Applogic que ejecuta las operaciones: alta,baja,modificacion,envio,recuperacion,borrar
 */

package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.archivoEmpleados;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.EmailSender;

public class NomEmpOpciones extends BaseServlet {

	String textoLog ="";
	private String textoLog(String funcion){
		return textoLog = NomEmpOpciones.class.getName() + "." + funcion + "::";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		boolean sesionvalida;
		textoLog("defaultAction");
		/* evita que la sesion expire si se envia un archivo que tard? mucho tiempo */
		HttpSession sess = request.getSession(false);
		BaseResource session = null;

	if ( sess == null && getFormParameter(request, "operacion").equals("envio")
		&& request.getParameter("basefilename")!=null)
	{

			sess = request.getSession();
			EIGlobal.mensajePorTrace("deserializando objeto de archivo: "+ getFormParameter(request, "basefilename"), EIGlobal.NivelLog.INFO);
			session = retriveBaseResource(getFormParameter(request, "basefilename"));

			if (session != null) {
				sess.setAttribute("session", session);
				sess.setAttribute("facultadesE",getFormParameter(request,  "facultades" ));
				sesionvalida = true;
			} else {
				sesionvalida = false;
			}

		} else {
			deleteBasefile(getFormParameter(request, "basefilename")); //Borra el objeto serializado al importar
			sesionvalida = SesionValida(request, response);
			session = (BaseResource) sess.getAttribute("session");
		}

		if (sesionvalida) {
			String valida = getFormParameter(request,  "valida" );
			if (validaPeticion(request, response, session, sess, valida)) {
				EIGlobal.mensajePorTrace(textoLog + "ENTRA A VALIDAR LA PETICION", EIGlobal.NivelLog.INFO);
			}
			else {
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser", ObtenContUser(request));
				request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25740h", request));
				request.setAttribute("facultades", sess.getAttribute("facultadesE"));

				EIGlobal.mensajePorTrace(textoLog + "Entrando ", EIGlobal.NivelLog.INFO);

				// La variable operacion tiene el tipo de operacion a realizar:
				// alta,baja, modificacion, etc.
				String strOperacion = getFormParameter(request, "operacion");
				//strOperacion.trim();
				if(strOperacion == null) {
					strOperacion = "";
				}
				String strRegistro = "";
				String listaTotalEmpleados = "";

				request.setAttribute("operacion", strOperacion);
				request.setAttribute("max_registros", "" + Global.MAX_REGISTROS);
				request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25740h", request));
				request.setAttribute("botLimpiar", "");

				EIGlobal.mensajePorTrace(textoLog + "operacion  = " + strOperacion.trim(), EIGlobal.NivelLog.INFO);

				if (strOperacion.equals("alta")) {
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25740h", request));
					request.setAttribute("botonOperacion", "<img border=\"0\" name=\"boton\" src=\"/gifs/EnlaceMig/gbo25480.gif\" width=\"66\" height=\"22\" alt=\"Alta\">");
					request.setAttribute("botLimpiar", "<td align=left valign=top width=76><A HREF=javascript:js_limpiar();><img border=0 name=boton src=/gifs/EnlaceMig/gbo25250.gif width=76 height=22 alt=Limpiar prueba></a></td>");
				}
				if (strOperacion.equals("baja")) {
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					request.setAttribute("Encabezado",CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25760h_b", request));
					request.setAttribute("botonOperacion", "<img border=\"0\" name=\"boton\" src=\"/gifs/EnlaceMig/gbo25490.gif\" width=\"68\" height=\"22\" alt=\"Baja\">");
				}
				if (strOperacion.equals("modificacion")) {
					request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25760h", request));
					request.setAttribute("botonOperacion", "<img border=\"0\" name=\"boton\" src=\"/gifs/EnlaceMig/gbo25510.gif\" width=\"93\" height=\"22\" alt=\"Modificaci&oacute;n\">");
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
				}
				if (strOperacion.equals("baja") || strOperacion.equals("modificacion")) {
					cargaCampos(strOperacion, strRegistro, request, response);
				}
				else if (strOperacion.equals("envio")) {
					EIGlobal.mensajePorTrace(textoLog + "Entro en operacion = envio", EIGlobal.NivelLog.INFO);
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					EIGlobal.mensajePorTrace(textoLog + "ANTES DE EJECUTAR ENVIO", EIGlobal.NivelLog.INFO);
					ejecutaEnvio(request, response);
					EIGlobal.mensajePorTrace(textoLog + "DESPUES DE EJECUTAR ENVIO", EIGlobal.NivelLog.INFO);
				}
				else if (strOperacion.equals("recuperar")) {
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					ejecutaRecuperar(request, response);
				}
				else if (strOperacion.equals("borrar")) {
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					ejecutaBorrar(request, response);
				} else if (strOperacion.equals("reporte")) {
					request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					request.setAttribute("total_registros",getFormParameter(request, "total_registros"));
					request.setAttribute("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
					listaTotalEmpleados = getFormParameter(request, "lista_numeros_empleado");
					request.setAttribute("tipo_archivo",getFormParameter(request, "tipo_archivo"));
					strRegistro = getFormParameter(request, "registro");
					ejecutaReporte(request, response);
				} else if (strOperacion.equals("exportar")) {
					request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s26450h", request));
                	request.setAttribute("Sucursal",getFormParameter(request, "Sucursal"));
					ejecutaExportar(request, response);
				} else if (strOperacion.equals("alta")) {
					String nombreArchivoEmpAltas = session.getArchivoEmpNom();
					archivoEmpleados archivo3 = new archivoEmpleados(nombreArchivoEmpAltas, session.getClaveBanco());
					request.setAttribute("lista_numeros_empleado", archivo3.lista_empleados.toString());
					evalTemplate(IEnlace.NOM_EMP_DATOS_TMPL, request, response);
				} else if (strOperacion.equals("obtensucursal")) {
					try {
						request.setAttribute("operacion", strOperacion);
						request.setAttribute("valor_radio", getFormParameter(request, "valor_radio"));
						request.setAttribute("texto", getFormParameter(request, "texto"));
						EIGlobal.mensajePorTrace("texto " + getFormParameter(request, "texto"), EIGlobal.NivelLog.INFO);
						evalTemplate(IEnlace.NOM_EMP_CNX_TMPL, request, response);
					} catch (Exception e) {
					}
				}// enviaEmpDetalle
				else if (strOperacion.equals("enviaEmpDetalle")) {
					try {
						request.setAttribute("operacion", strOperacion);
						request.setAttribute("valor_radio", getFormParameter(request, "valor_radio"));
						request.setAttribute("texto", getFormParameter(request, "texto"));
						EIGlobal.mensajePorTrace("texto " + getFormParameter(request, "texto"), EIGlobal.NivelLog.INFO);
						evalTemplate(IEnlace.NOM_EMP_CNX_TMPL, request, response);
					} catch (Exception e) {
					}
				}

			} // Termina else de validaOTP

		} else if (!sesionvalida) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	/* Metodo que borra el archivo con el objeto serializado al importar */
	private void deleteBasefile(String filename) {
		textoLog("deleteBaseFile");
		try {
			File file = new File(IEnlace.DOWNLOAD_PATH + filename + "_base.bak");
			if (file.exists()) {
				file.delete();
			}
			/*try {		SE ELIMINA BLOQUE DE DEPURACION.
				File[] listFiles = new File(IEnlace.DOWNLOAD_PATH).listFiles();
				for (int i = 0; i < listFiles.length; i++) {
					if (listFiles[i].isFile() && listFiles[i].getName().toLowerCase().endsWith("_base.bak") && listFiles[i].lastModified() < (System.currentTimeMillis() - 15000000)) {
						listFiles[i].delete();
					}
				}
			} catch (SecurityException secex) {
			} catch (Exception e) {
			}*/
		} catch (NullPointerException nullerror) {
			EIGlobal.mensajePorTrace(textoLog + "Problema borrando archivo base (null)", EIGlobal.NivelLog.INFO);
		} catch (Exception exerro) {
			EIGlobal.mensajePorTrace(textoLog + "Problema borrando archivo base", EIGlobal.NivelLog.INFO);
		}
	}

	/*
	 * Metodo que deserializa el objeto serializado al importar para impedir que
	 * la sesion expire
	 */
	private BaseResource retriveBaseResource(String filename) {
		textoLog("retrieveBaseResource");
		BaseResource base = null;
		try {
			File file = new File(IEnlace.DOWNLOAD_PATH + filename + "_base.bak");
			ObjectInputStream ob = new ObjectInputStream(new FileInputStream(file));
			base = (BaseResource) ob.readObject();
			if (file.exists()) {
				file.delete();
			}
			EIGlobal.mensajePorTrace(textoLog + "Objeto deserialziado", EIGlobal.NivelLog.INFO);
		} catch (IOException ioe) {
			EIGlobal.mensajePorTrace(textoLog + "Problema en el archivo base", EIGlobal.NivelLog.INFO);
			return null;
		} catch (ClassNotFoundException cnfe) {
			EIGlobal.mensajePorTrace(textoLog + "Problema creando BaseResource ", EIGlobal.NivelLog.INFO);
			return null;
		}
		return base;
	}

	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;

		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}

	public void cargaCampos(String p_operacion, String s_registro, HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("cargaCampos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		long longRegistro = 0;
		String nombreArchivoEmp = "";

		try {
			longRegistro = Long.parseLong(s_registro);
		} catch (NumberFormatException e) {
			EIGlobal.mensajePorTrace(textoLog + "Problema en el formato", EIGlobal.NivelLog.INFO);
			request.setAttribute("Encabezado", CreaEncabezado("Mantenimiento a Empleados", "Servicios &gt; Nomina &gt; Mantenimiento a Empleados", "s25740h", request));
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo leer el registro\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}

		// Lee los datos del archivo correspondiente
		EIGlobal.mensajePorTrace(textoLog + "session.getArchivoEmpNom():" + session.getArchivoEmpNom(), EIGlobal.NivelLog.INFO);
		archivoEmpleados archivo2 = new archivoEmpleados(session.getArchivoEmpNom(), session.getClaveBanco());

		int result = archivo2.leeRegistro(longRegistro);

		if (p_operacion.equals("baja")) {
			request.setAttribute("soloLectura", "blur()");
		}

		// Crea las variables con el nombre del archivo y la liga para el
		// download
		nombreArchivoEmp = session.getArchivoEmpNom();
		String liga = nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/"));
		request.setAttribute("NumeroEmpleado", archivo2.NumeroEmpleado);
		request.setAttribute("NumeroDepto", archivo2.NumeroDepto);
		request.setAttribute("ApellidoPaterno", archivo2.ApellidoPaterno);
		request.setAttribute("ApellidoMaterno", archivo2.ApellidoMaterno);
		request.setAttribute("Nombre", archivo2.Nombre);
		request.setAttribute("RFC", archivo2.RFC);
		request.setAttribute("Homoclave", archivo2.Homoclave);
		request.setAttribute("Sexo", archivo2.Sexo);
		request.setAttribute("Nacionalidad", archivo2.Nacionalidad);
		request.setAttribute("EstadoCivil", archivo2.EstadoCivil);
		request.setAttribute("NombreCorto", archivo2.NombreCorto);
		request.setAttribute("Domicilio", archivo2.Domicilio);
		request.setAttribute("Colonia", archivo2.Colonia);
		request.setAttribute("DelegacionoMpio", archivo2.DelegacionoMpio);
		request.setAttribute("CiudadoPoblacion", archivo2.CiudadoPoblacion);
		request.setAttribute("CodigoPostal", archivo2.CodigoPostal);
		request.setAttribute("Pais", archivo2.Pais);
		request.setAttribute("EstatusCasa", archivo2.EstatusCasa);
		request.setAttribute("FechaResidencia", archivo2.FechaResidencia);
		request.setAttribute("ClaveEnvio", archivo2.ClaveEnvio);
		request.setAttribute("DomicilioOfna", archivo2.DomicilioOfna);
		request.setAttribute("ColoniaOficina", archivo2.ColoniaOficina);
		request.setAttribute("DeloMpioOficina", archivo2.DeloMpioOficina);
		request.setAttribute("CdoPobOficina", archivo2.CdoPobOficina);
		request.setAttribute("CodigoPostalOfna", archivo2.CodigoPostalOfna);
		request.setAttribute("PaisOficina", archivo2.PaisOficina);
		request.setAttribute("PrefijoTelefono", archivo2.PrefijoTelefono);
		request.setAttribute("Telefono", archivo2.Telefono);
		request.setAttribute("FechaIngreso", archivo2.FechaIngreso);
		request.setAttribute("PrefijoParTelefono", archivo2.PrefijoParTelefono);
		request.setAttribute("ParTelefono", archivo2.ParTelefono);
		request.setAttribute("Extension", archivo2.Extension);

		int indice = 0;
		for (int i = 0; i < archivo2.IngresoMensual.length() && archivo2.IngresoMensual.charAt(i) == '0'; i++, indice++);

		request.setAttribute("IngresoMensual", archivo2.IngresoMensual.substring(indice));
		request.setAttribute("NumeroCuenta", archivo2.NumeroCuenta);
		request.setAttribute("NumeroTarjeta", archivo2.NumeroTarjeta);
		request.setAttribute("posicionRegistro", s_registro);
		/*
		 * Proposito: Inicializar el valor de la sucursal entrada por el usuario
		 * en la modificacion y baja de un registro
		 */
		if (!archivo2.Sucursal.equals("----"))
			request.setAttribute("AMESucursal", archivo2.Sucursal);
		else
			request.setAttribute("AMESucursal", "");
		/*
		 * Coloca las variables a mostrar en el template de detalle estas
		 * variables aparecen en listas de tipo <select> por lo que se tiene que
		 * seleccionar la variable que corresponde al valor que tiene el campo
		 * en el archivo
		 */
		if (archivo2.Sexo.trim().equals("M")) {
			request.setAttribute("Sexo_m", "selected");
		} else {
			request.setAttribute("Sexo_f", "selected");
		}

		if (archivo2.EstadoCivil.trim().equals("C")) {
			request.setAttribute("casado", "selected");
		}

		if (archivo2.EstadoCivil.trim().equals("D")) {
			request.setAttribute("divorciado", "selected");
		}

		if (archivo2.EstadoCivil.trim().equals("S")){
			request.setAttribute("soltero", "selected");
		}

		if (archivo2.EstadoCivil.trim().equals("U")){
			request.setAttribute("union", "selected");
		}

		if (archivo2.EstadoCivil.trim().equals("V")){
			request.setAttribute("viudo", "selected");
		}

		if (archivo2.Nacionalidad.trim().equals("CANA")){
			request.setAttribute("n_canada", "selected");
		}

		if (archivo2.Nacionalidad.trim().equals("ESPA")){
			request.setAttribute("n_espana", "selected");
		}

		if (archivo2.Nacionalidad.trim().equals("USA")){
			request.setAttribute("n_eu", "selected");
		}

		if (archivo2.Nacionalidad.trim().equals("FRA")){
			request.setAttribute("n_francia", "selected");
		}

		if (archivo2.Nacionalidad.trim().equals("MEXI")){
			request.setAttribute("n_mexico", "selected");
		}

		if (archivo2.Pais.trim().equals("CANA")){
			request.setAttribute("p_canada", "selected");
		}

		if (archivo2.Pais.trim().equals("ESPA")){
			request.setAttribute("p_espana", "selected");
		}

		if (archivo2.Pais.trim().equals("USA")){
			request.setAttribute("p_eu", "selected");
		}

		if (archivo2.Pais.trim().equals("FRA")){
			request.setAttribute("p_francia", "selected");
		}

		if (archivo2.Pais.trim().equals("MEXI")){
			request.setAttribute("p_mexico", "selected");
		}

		if (archivo2.EstatusCasa.trim().equals("P")){
			request.setAttribute("casapropia", "selected");
		}

		if (archivo2.EstatusCasa.trim().equals("R")){
			request.setAttribute("casarentada", "selected");
		}

		if (archivo2.ClaveEnvio.trim().equals("1")){
			request.setAttribute("enviocasa", "selected");
		}

		if (archivo2.ClaveEnvio.trim().equals("2")){
			request.setAttribute("enviooficina", "selected");
		}

		if (archivo2.PaisOficina.trim().equals("CANA")){
			request.setAttribute("pf_canada", "selected");
		}

		if (archivo2.PaisOficina.trim().equals("ESPA")){
			request.setAttribute("pf_espana", "selected");
		}

		if (archivo2.PaisOficina.trim().equals("USA")){
			request.setAttribute("pf_eu", "selected");
		}

		if (archivo2.PaisOficina.trim().equals("FRA")){
			request.setAttribute("pf_francia", "selected");
		}

		if (archivo2.PaisOficina.trim().equals("MEXI")){
			request.setAttribute("pf_mexico", "selected");
		}

		EIGlobal.mensajePorTrace(textoLog + "s_registro=" + s_registro, EIGlobal.NivelLog.INFO);

		if (result == 1){
			request.setAttribute("ContenidoArchivo", "");
		}
		else{
			request.setAttribute("mensaje_js", "cuadroDialogo (\"Error leyendo Archivo\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}

		evalTemplate(IEnlace.NOM_EMP_DATOS_TMPL, request, response);

	}

	public void ejecutaEnvio(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		textoLog("ejecutaEnvio");
		EmailSender es = new EmailSender(); // Objeto para envio de notificacion NVS
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario = session.getUserID8();
		String contrato = session.getContractNumber();
		String nombre = session.getNombreContrato(); // Experimental NVS
		String clavePerfil = session.getUserProfile();
		archivoEmpleados arch = new archivoEmpleados(session.getArchivoEmpNom(), session.getClaveBanco());
		String errorMsg = "";
		String trama_entrada = "";
		String CodError = "";
		String trama_inicio = "";
		String secuencia = "";
		StringBuffer cadReg = new StringBuffer("");
		boolean bandera = true;
		Hashtable htResult = null; // MODIFICAR
        String           sucursal	   = getFormParameter(request, "Sucursal");
        String			 statusDupC    = getFormParameter(request, "statusDup"); // Agregado por Eric Gomez 6 / FEB / 2004
		String nombreArchivoEmp = session.getArchivoEmpNom();
		int tamanio = arch.tamanioArchivo();
        ServicioTux      tuxGlobal     = new ServicioTux();
		// Arma la trama para llamar al servicio IN01
		sucursal = sucursal.trim();
		if (sucursal.length() > 4) {
			// MARCAR ERROR
			String infoUser = " La sucursal es inv&aacute;lida, ingrese un dato correcto. ";
			String infoEncabezadoUser = "<B>Error en datos capturados</B>";
			infoUser = "cuadroDialogoMttoEmp(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
			request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
			request.setAttribute("mensaje_js", infoUser);
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
			return;
		}

		if (sucursal.equals(""))
			trama_entrada = "1EWEB|" + usuario + "|IN01|" + contrato + "|"
					+ usuario + "|" + clavePerfil + "|" + contrato + "@"
					+ arch.tamanioArchivo() + "@";
		else
			trama_entrada = "1EWEB|" + usuario + "|IN01|" + contrato + "|"
					+ usuario + "|" + clavePerfil + "|" + contrato + "@"
					+ arch.tamanioArchivo() + "@" + sucursal + "@";

		/* Se verifica que el archivo contenga datos */
		EIGlobal.mensajePorTrace(textoLog + " arch.tamanioArchivo() = " + arch.tamanioArchivo(), EIGlobal.NivelLog.DEBUG);
		if (arch.tamanioArchivo() == 0) {
			request.setAttribute("mensaje_js", "cuadroDialogo (\"El archivo no contiene datos\", 1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
			bandera = false;
		}

		/*
		 * Si se requiere validar duplicados, se a?ade a la trama de entrada la
		 * cadena <#REGS>@<NUM_EMPL>@<RFC>@...@<NUM_EMPL>@<RFC>@ donde
		 * <#REGS> es 5 si el archivo tiene al menos 5 registros ? bien es la
		 * cantidad de registros, <NUM_EMPL> y <RFC> son los datos b?sicos de
		 * cada uno de los <#REGS> empleados para poder validar la duplicidad
		 * del archivo. Agregado por Javier D?az - 01/2004.
		 */

		EIGlobal.mensajePorTrace(textoLog + "archDuplicado :" + statusDupC + ":", EIGlobal.NivelLog.INFO);

		/* validar la dupliciadad de archivos */
		if (statusDupC.compareTo("1") == 0) {
			trama_entrada = trama_entrada + arch.tramaValidaDuplicado();
		}

		EIGlobal.mensajePorTrace(textoLog + "TRAMA 1 [" + trama_entrada + "] TRAMA 2 [" + arch.tramaValidaDuplicado() + "]", EIGlobal.NivelLog.INFO);

		if (request.getSession().getAttribute("solTokenCodError") == null) {
			// Se llama al servicio de tuxedo
			try {
				htResult = tuxGlobal.web_red(trama_entrada);
			} catch (java.rmi.RemoteException re) {
				EIGlobal.mensajePorTrace(textoLog + "Despues de llamar a WEB_RED, cod error:" + CodError, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace(textoLog + "Despues de llamar a WEB_RED, cod error:" + CodError, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			try {
				CodError = (String) htResult.get("BUFFER");
				if(CodError == null)
					CodError = "";
					
			}catch (Exception e){
				CodError = "";
				EIGlobal.mensajePorTrace("Despues de obtener el BUFFERTUX ", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}

		} else
			CodError = (String) request.getSession().getAttribute("solTokenCodError");

		EIGlobal.mensajePorTrace(textoLog + "Despues de llamar a WEB_RED, cod error:" + CodError, EIGlobal.NivelLog.INFO);

		// Checa el codigo de error regresado por el servicio
		// CodError = "@errores fatales@eror@";
		if (!(CodError.indexOf("INOM0000") > 0)) {
			/*
			 * Si el error es INOM0034 (Archivo Duplicado), informar al usuario
			 * para que decida si env?a ? no el archivo. Agregado por Javier
			 * D?az - 01/2004.
			 */
			EIGlobal.mensajePorTrace(textoLog + "ENTRO EN ARCHIVO DUPLICADO 935", EIGlobal.NivelLog.INFO);
			if (CodError.indexOf("INOM0034") > 0) {
				String infoUser = " El archivo ya existe. �Desea transmitirlo nuevamente? ";
				String infoEncabezadoUser = "<B>Archivo Duplicado</B>";

				infoUser = "cuadroDialogoMttoEmp(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
				/*Cambio la variable del mensaje para que imprima el cuadro de dialogo*/
				request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
				request.setAttribute("mensaje_js", infoUser);
				evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
				bandera = false;
			}
			else { // otro error distinto a INOM0034
				try{
					errorMsg = CodError.substring(posCar(CodError, '@', 1) + 1, posCar(CodError, '@', 2)) + ", " + CodError.indexOf("INOM0000");
				} catch (StringIndexOutOfBoundsException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
				request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo realizar el envio. Intente de nuevo.\",1)");
				evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
				bandera = false;
			}
		} else
			statusDupC = "1"; // validar la duplicidad de archivos

		EIGlobal.mensajePorTrace(textoLog + "bandera = " + bandera, EIGlobal.NivelLog.INFO);
		if (bandera) {
			String valida = getFormParameter(request,  "valida" );
			EIGlobal.mensajePorTrace(textoLog + "Entro en bandera = true", EIGlobal.NivelLog.INFO);

			// interrumpe la transaccion para invocar la validaci?n
			if (valida == null) {
				EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci?n est? parametrizada para solicitar la validaci?n con el OTP", EIGlobal.NivelLog.INFO);

				boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);

				if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal) {
					// VSWF-HGG -- Bandera para guardar el token en la bit?cora
					request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
					EIGlobal.mensajePorTrace(textoLog + "El usuario tiene Token. \nSolicit? la validaci?n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.INFO);
					guardaParametrosEnSession(request);
					request.getSession().setAttribute("solTokenCodError", CodError);
					ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP);
				} else {
					ValidaOTP.guardaRegistroBitacora(request, "Token deshabilitado.");
					valida = "1";
				}
			}
			// retoma el flujo
			if (valida != null && valida.equals("1")) {
				request.getSession().removeAttribute("solTokenCodError");
				// Obtiene la secuencia devuelta por el servicio IN01
				try {
					EIGlobal.mensajePorTrace(textoLog + "CodError :  ->" + CodError + "<-", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(textoLog + "posCar(CodError,'@',2) :  " + posCar(CodError, '@', 2), EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace(textoLog + "posCar(CodError,'@',3) :  " + posCar(CodError, '@', 3), EIGlobal.NivelLog.INFO);

					secuencia = CodError.substring(posCar(CodError, '@', 2) + 1, posCar(CodError, '@', 3));
				} catch (StringIndexOutOfBoundsException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				// Arma la trama de inicio para llamar al servicio IN02
				/*
				 * Se invocar? al nuevo servicio NOIE para ingresar los datos de
				 * los empleados. Este servicio considera los nuevos campos
				 * Fecha de Ingreso y Tel?fono Particular. Modificado por Javier
				 * D?az - 01/2004.
				 */
				// trama_inicio=
				// "1EWEB|"+usuario+"|IN02|"+contrato+"|"+usuario+"|"+clavePerfil+"|"+
				// contrato+"@"+secuencia+"@1@";
				// trama_inicio=
				// "1EWEB|"+usuario+"|NOIE|"+contrato+"|"+usuario+"|"+clavePerfil+"|"+contrato+"@"+secuencia+"@1@";
				String archivoTuxedo = IEnlace.DOWNLOAD_PATH + sess.getId()
						+ "_" + secuencia + ".tux";
				//Version JSCH, Proyecto CNBV
				String pathArchivo = sess.getId()+ "_" + secuencia + ".tux";
				trama_inicio = "1EWEB|" + usuario + "|NOIE|" + contrato + "|"
						+ usuario + "|" + clavePerfil + "|" + archivoTuxedo
						+ "@";

				// El arreglo registros contiene las tramas que se van a enviar
				// al servicio: trama inicio + datos del registro
				String registros[] = arch.envioArchivo("02@");
				String Cod = "";
				StringBuffer SucursalVacia = new StringBuffer("");
				// Realiza el envio de todos los registros del archivo
				boolean bufferError = false;
				File archivofinal = new File(archivoTuxedo);
				FileOutputStream fos = null;
				DataOutputStream dos = null;
				try {
					fos = new FileOutputStream(archivofinal);
					dos = new DataOutputStream(fos);

					dos.writeBytes("01@" + contrato + "@" + secuencia + "@"
							+ (tamanio + 1) + "@\n");
					for (int i = 0; i < registros.length; i++) {
						if (registros[i].length() > 0) {
							SucursalVacia = new StringBuffer("");
							/*
							 * Proposito: Verificar si en el contenido de la
							 * linea tiene un registro vacio. Si es asi enviarlo
							 * con la sucursal entrada por el usuario en la main
							 * interface. Ademas verificar si el registro
							 * proviene de un archivo importado la diferencia
							 * son los dashes.
							 */
							// if(registros[i].substring(516,520).equals("----"))
							// El registro viene con sucursal o dashes
							if (registros[i].indexOf("----") != -1) // El registro viene con dashes o con sucursal
							{
								if (registros[i].indexOf("----") != -1) {
									SucursalVacia.append(registros[i].substring(0, registros[i].indexOf("----")));
									if (!sucursal.equals(""))
										SucursalVacia.append(sucursal + "@" + registros[i].substring(registros[i].indexOf("----") + 5));
									else
										SucursalVacia.append("" + "@" + registros[i].substring(registros[i].indexOf("----") + 5));
								} else {// El registro proviene de un Archivo o
										// Cifras de control con SUCURSAL por lo
										// tanto solo se envia a Tuxedo
									SucursalVacia.append(registros[i]);
								}
							} else {// El registro proviene del Archivo, se
									// checa si se tiene una Sucursal en las
									// cifras de control para adherirsela
								SucursalVacia.append(registros[i]);
								if (!sucursal.equals(""))
									SucursalVacia.append(sucursal + "@" + registros[i].substring(registros[i].indexOf("----") + 5));
							}

							// el valor de codigo IN02OK12993INOM0000@Transaccion Exitosa@1@0@
							dos.writeBytes(registros[i]);
						}
						else {
							EIGlobal.mensajePorTrace(textoLog + "no hay mas elementos en el arreglo:", EIGlobal.NivelLog.INFO);
							break;
						}
					}
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					bufferError = true;
				} finally {
					try {
						fos.flush();
						fos.close();
					} catch (IOException closex) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(closex), EIGlobal.NivelLog.INFO);
					}
				}
				if (bufferError) {
					request.setAttribute("mensaje_js", "cuadroDialogo (\"Error leyendo archivo.\",1)");
					evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
				} else {
					try {
						//Version JSCH, Proyecto CNBV
						if (copyRemoteFile(pathArchivo)) {
						
						//if (copyRemoteFile(archivoTuxedo)) {
							EIGlobal.mensajePorTrace(textoLog + "Comenzando Envio", EIGlobal.NivelLog.INFO);
							String archivoresp = sess.getId() + "_" + secuencia + ".tux.resp";
							Cod = tuxGlobal.web_red_mod(trama_inicio, tamanio, archivoresp, IEnlace.DOWNLOAD_PATH);
						} else {
							EIGlobal.mensajePorTrace(textoLog + "Problema copiando archivo RCP " + archivoTuxedo, EIGlobal.NivelLog.INFO);
							Cod = "error";
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}

					if (Cod.equals("error"))
						errorMsg = Cod;

					// Checa el codigo de error en el envio
					if (errorMsg.equals("")) {
						String cE2 = "";
						String tot_reg = "";
						String reg_acep = "";

						// Arma la trama para el servicio IN03
						trama_entrada = "1EWEB|" + usuario + "|IN03|"
								+ contrato + "|" + usuario + "|" + clavePerfil
								+ "|" + contrato + "@" + secuencia + "@";

						try {
							htResult = tuxGlobal.web_red(trama_entrada);
							ValidaOTP.guardaBitacora((List) sess.getAttribute("bitacora"), "IN03");
						} catch (java.rmi.RemoteException re) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
						} catch (Exception e) {
						}

						cE2 = (String) htResult.get("BUFFER");

						EIGlobal.mensajePorTrace(textoLog + "Despues de llamar a EWEB_RED(IN03), cod error:" + cE2, EIGlobal.NivelLog.INFO);

						sess = request.getSession(false);
						if (sess == null) {
							sess = request.getSession();
							sess.setAttribute("session", session);
							sess.setAttribute("facultadesE",getFormParameter(request,  "facultades" ));
						}

						// Checa el codigo de error
						if (cE2.indexOf("INOM0000") != -1) {

							// Obtiene los datos de resumen del servicio
							tot_reg = cE2.substring(posCar(cE2, '@', 2) + 1, posCar(cE2, '@', 3));
							reg_acep = cE2.substring(posCar(cE2, '@', 3) + 1, posCar(cE2, '@', 4));

							int total = 0, aceptados = 0, rechazados = 0;

							total = Integer.parseInt(tot_reg.trim());
							aceptados = Integer.parseInt(reg_acep.trim());
							rechazados = total - aceptados;

							StringBuffer nombreArchivoEmailEmp = new StringBuffer();

							nombreArchivoEmailEmp.append(IEnlace.DOWNLOAD_PATH);
							nombreArchivoEmailEmp.append(usuario);
							nombreArchivoEmailEmp.append(".nae");
							EIGlobal.mensajePorTrace(textoLog + "nombreArchivoEmailEmp = " + nombreArchivoEmailEmp, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "NomEmpOpciones = " + nombreArchivoEmailEmp.toString() + " " + contrato + " " + nombre + " " + usuario + " " + secuencia + " " + total, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("<><><><><><>ENVIANDO NOTIFICACION VIA CORREO<><><><><><>", EIGlobal.NivelLog.INFO);
							es.confirmaEmailEmpl(request, nombreArchivoEmailEmp.toString(), contrato, nombre, usuario, secuencia, total);
							EIGlobal.mensajePorTrace("<><><><><><>TERMINA ENVIO NOTIFICACION VIA CORREO<><><><><><>", EIGlobal.NivelLog.INFO);
							try {
								new File(nombreArchivoEmailEmp.toString()).delete();
								EIGlobal.mensajePorTrace(textoLog + "Borrando archivo " + nombreArchivoEmailEmp.toString(), EIGlobal.NivelLog.DEBUG);
							}
							catch(Exception e) {
								EIGlobal.mensajePorTrace(textoLog + "Problema borrando archivo " + nombreArchivoEmailEmp.toString(), EIGlobal.NivelLog.WARN);
							}
							// Envia los datos al template
							request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
							request.setAttribute("tipo_archivo", "enviado");
							request.setAttribute("r_numtran", secuencia);
							request.setAttribute("r_totregs", tot_reg);
							request.setAttribute("r_portran", "0");
							request.setAttribute("r_acept", reg_acep);
							request.setAttribute("r_rech", "" + rechazados);
							request.setAttribute("r_fchtran", ObtenFecha(true));
							request.setAttribute("mensaje_js", "cuadroDialogo (\"Se enviaron: " + tot_reg + " registros, se aceptaron: " + reg_acep + " registros\", 1)");
							EIGlobal.mensajePorTrace(textoLog + "Cargando Template:forward", EIGlobal.NivelLog.INFO);

							/* VSWF */
							if (Global.USAR_BITACORAS.equals("ON")) {
								try {
									BitaHelper bh = new BitaHelperImpl(request, session, sess);
									BitaTransacBean bt = new BitaTransacBean();
									bt = (BitaTransacBean) bh.llenarBean(bt);
									bt.setNumBit(BitaConstants.ES_NOMINA_EMP_MANTEN_ENVIAR);
									bt.setContrato(contrato);
									bt.setUsr(usuario);
									bt.setIdErr(CodError.substring(0, 8));
									bt.setServTransTux("IN03");
									if (cE2 != null) {
										if (cE2.substring(0, 2).equals("OK")) {
											bt.setIdErr("INOM0000");
										} else if (cE2.length() > 8) {
											bt.setIdErr(cE2.substring(0, 8));
										} else {
											bt.setIdErr(cE2.trim());
										}
									}
									log("VSWF: bandera token " + request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN));
									if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null && ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim().equals(BitaConstants.VALIDA)) {
										bt.setIdToken(session.getToken().getSerialNumber());
									} else {
										log("VSWF: no hubo bandera de token");
									}
									BitaHandler.getInstance().insertBitaTransac(bt);
								} catch (SQLException e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}
							/* VSWF */
							evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
							bandera = false;
						} else {
							// Obtiene el mensaje de error y lo muestra en el template
							errorMsg = cE2.substring(posCar(cE2, '@', 1) + 1, posCar(cE2, '@', 2));

							request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo realizar el envio, intente mas tarde\",1)");
							evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
							bandera = false;
						}
					} else {
						// Muestra el error en el template
						request.setAttribute("mensaje_js","cuadroDialogo (\"No se pudo realizar el envio , intente mas tarde\",1)");
						evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
					}
				}
				if (archivofinal.exists()) {
					EIGlobal.mensajePorTrace(textoLog + "Borrando archivo tuxedo..", EIGlobal.NivelLog.INFO);
					archivofinal.delete();
				}

				request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
			} // fin de validacion con el OTP

			EIGlobal.mensajePorTrace(textoLog + "Saliendo de bandera = true", EIGlobal.NivelLog.INFO);
		} // fin de if bandera

	}

	public boolean copyRemoteFile(String path) {
		textoLog("copyRemoteFile");
		boolean ready = true;
		/* Se comenta por sustitucion a JSCH 240712
		EIGlobal.mensajePorTrace(textoLog + "Ejecutando Comando RCP:", EIGlobal.NivelLog.INFO);
		String command = "";********************/
		try {
			EIGlobal.mensajePorTrace("NomEmpOpciones - envia - copyRemoteFile - Inicia", EIGlobal.NivelLog.INFO);
			//IF PROYECTO ATBIA1 (NAS) FASE II		
            
            ArchivoRemoto envArch = new ArchivoRemoto();

            if ( !envArch.copiaLocalARemoto( path, Global.DIRECTORIO_REMOTO) )
			{
				EIGlobal.mensajePorTrace( "***NomEmpOpciones.class & copyRemoteFile: No se realizo el envio del archivo. ", EIGlobal.NivelLog.DEBUG);
				ready = false;
				
			} else {
				EIGlobal.mensajePorTrace( "***NomEmpOpciones.class & copyRemoteFile: Se realizo el envio del archivo.", EIGlobal.NivelLog.DEBUG);
			}
        	
		} catch (Exception exception) {
			//EIGlobal.mensajePorTrace(textoLog + "-!COMANDO[" + command + "] RESULT[" + exception.getMessage() + "]", EIGlobal.NivelLog.DEBUG);
			ready = false;
			EIGlobal.mensajePorTrace (exception.getMessage(), EIGlobal.NivelLog.INFO);
			exception.printStackTrace(System.err);
		}
		return ready;
	}

	public void ejecutaRecuperar(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaRecuperar");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario = session.getUserID8(), contrato = session.getContractNumber();
		String clavePerfil = session.getUserProfile();
		String claveperfil = clavePerfil;
		String trama_entrada = "";
		String nombre_arch_salida = "";
		String CodError3 = "";
		String nomSalida = "";
		String contenidoArch = "";
		int posNombre = -1;
		Hashtable htResult = null;

		// Obtiene el numero de secuencia para recuperar el archivo

        String secuencia_rec = getFormParameter(request, "secuencia");
		ServicioTux tuxGlobal = new ServicioTux();

		// Arma la trama para llamar al servicio IN09
		trama_entrada = "2EWEB|" + usuario + "|IN09|" + contrato + "|"
				+ usuario + "|" + claveperfil + "|" + contrato + "@"
				+ secuencia_rec + "@";

		try {
			htResult = tuxGlobal.web_red(trama_entrada); // MODIFICAR
		} catch (java.rmi.RemoteException re) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
		}

		CodError3 = (String) htResult.get("BUFFER");

		// Obtiene el nombre del archivo de salida del servicio
		posNombre = CodError3.lastIndexOf('/');

		if (posNombre == -1) {
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se encontraron datos para la secuencia: " + secuencia_rec + "\")");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}
		else {
			EIGlobal.mensajePorTrace(textoLog + "codigo devuelto: " + CodError3, EIGlobal.NivelLog.INFO);
			nombre_arch_salida = CodError3.substring(posNombre);
		}

		// Copia Remota
		ArchivoRemoto archR = new ArchivoRemoto();

		if (!archR.copia(nombre_arch_salida.substring(1))) {
			EIGlobal.mensajePorTrace(textoLog + "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo recuperar el archivo\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}

		EIGlobal.mensajePorTrace(textoLog + "copiando Archivo Remoto:" + Global.DOWNLOAD_PATH + nombre_arch_salida, EIGlobal.NivelLog.INFO);
		nombre_arch_salida = Global.DIRECTORIO_REMOTO + nombre_arch_salida;
		session.setArchivoEmpNom(nombre_arch_salida);
		archivoEmpleados archSalida = new archivoEmpleados(nombre_arch_salida, session.getClaveBanco());

		// Crea el archivo con el formato de salida
		nomSalida = archSalida.escribeArchivoSalida();
		EIGlobal.mensajePorTrace(textoLog + "Despues de llamar a EWEB_RED(recuperar), cod error:" + CodError3, EIGlobal.NivelLog.INFO);

		contenidoArch = archSalida.lecturaArchivoSalida();

		// Valida si el archivo de salida contiene datos
		if (contenidoArch.equals("") || contenidoArch.indexOf("Error") >= 0){
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se encontraron datos para la secuencia: " + secuencia_rec + "\")");
		}
		else{
			request.setAttribute("recuperado", "S");
			request.setAttribute("r_numtran", secuencia_rec);
			request.setAttribute("r_totregs", archSalida.asTotalRegistros);
			request.setAttribute("r_portran", "0");
			request.setAttribute("r_acept", archSalida.asAceptados);
			request.setAttribute("r_rech", archSalida.asRechazados);
			request.setAttribute("r_fchtran", archSalida.asFechaTransmision);
			request.setAttribute("r_fchact", archSalida.asFechaActualizacion);
			int int_total_reg = Integer.parseInt(archSalida.asAceptados.trim());
			request.setAttribute("total_registros", archSalida.asTotalRegistros);
			request.setAttribute("tipo_archivo", "recuperado");
			request.setAttribute("registros_aceptados", archSalida.asAceptados);
			EIGlobal.mensajePorTrace(textoLog + "valor de int_total_reg : " + archSalida.asTotalRegistros.trim(), EIGlobal.NivelLog.INFO);

			if (int_total_reg <= Global.MAX_REGISTROS){
				request.setAttribute("ContenidoArchivo", contenidoArch);

				/* VSWF ARR -I */
				if (Global.USAR_BITACORAS.trim().equals("ON")) {
					try {
						BitaHelper bh = new BitaHelperImpl(request, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean) bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_NOMINA_EMP_MANTEN_RECUPERA_ARCHIVO_NOMINA);
						bt.setContrato(contrato);
						bt.setNombreArchivo(nombre_arch_salida);
						bt.setServTransTux("IN09");
						if (CodError3 != null) {
							if (CodError3.substring(0, 2).equals("OK")) {
								bt.setIdErr("IN090000");
							} else if (CodError3.length() > 8) {
								bt.setIdErr(CodError3.substring(0, 8));
							} else {
								bt.setIdErr(CodError3.trim());
							}
						}
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}
				/* VSWF ARR -F */

			}
			else
				request.setAttribute("mensaje_js", "cuadroDialogo (\"El archivo contiene mas de " + Global.MAX_REGISTROS + " registros, utilice la opcion Exportar\")");
		}
		evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
	}

	public void ejecutaBorrar(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaBorrar");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String nombreArchivoEmp = session.getArchivoEmpNom();

		try{
			// Si existe el archivo lo borra
			File archivo = new File(nombreArchivoEmp);

			if (archivo.exists()){
				archivo.delete();
				request.setAttribute("mensaje_js", "cuadroDialogo (\"El archivo ha sido eliminado \")");
			}
		}
		catch (Exception e){
			EIGlobal.mensajePorTrace(textoLog + "problema en el objeto File(borrar): " + e, EIGlobal.NivelLog.INFO);
		}

		// Elimina el nombre del archivo del objeto session
		session.setArchivoEmpNom(null);
		evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
	}

	public void ejecutaReporte(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException{
		textoLog("ejecutaReporte");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
        String tipoArchivo= getFormParameter(request, "tipo_archivo");
		String tablaSalida = "";
		EIGlobal.mensajePorTrace(textoLog + "entrando", EIGlobal.NivelLog.INFO);

		if (tipoArchivo == null)
			tipoArchivo = "";

		archivoEmpleados archrep = new archivoEmpleados(session.getArchivoEmpNom(), session.getClaveBanco());
		int int_total_reg=Integer.parseInt(getFormParameter(request, "registros_aceptados"));  //El servicio solo devuelve los que fueron aceptados

		if (int_total_reg <= Global.MAX_REGISTROS){
			if (tipoArchivo.equals("recuperado")){
				tablaSalida = "<table><tr><td class=tabmovtex>Nombre del Archivo: Recuperado" + "</td></tr><tr><td class=tabmovtex>Numero de Transmisi&oacute;n: " + getFormParameter(request, "r_numtran") + "</td></tr></table>";
				request.setAttribute("ContenidoTitulos", tablaSalida);
				request.setAttribute("ContenidoArchivo", archrep.reporteArchivoSalida());
			}
			else if (tipoArchivo.equals("enviado")){
				tablaSalida = "<table><tr><td class=tabmovtex>Nombre del Archivo: " + getFormParameter(request, "archivo_actual") + "</td></tr><tr><td class=tabmovtex>Numero de Transmisi&oacute;n: " + getFormParameter(request, "r_numtran") + "</td></tr></table>";
				request.setAttribute("ContenidoTitulos", tablaSalida);
				request.setAttribute("ContenidoArchivo", archrep.reporteArchivoSalida());
			}
			else{
				tablaSalida = "<table><tr><td class=tabmovtex>Nombre del Archivo: " + getFormParameter(request, "archivo_actual") + "</td></tr><tr><td class=tabmovtex>Numero de Transmisi&oacute;n: No se ha transmitido</td></tr></table>";
				request.setAttribute("ContenidoTitulos", tablaSalida);
				request.setAttribute("ContenidoArchivo", archrep.reporteArchivo());
			}
			evalTemplate(IEnlace.NOM_EMP_REP_TMPL, request, response);
		}
		else{
			request.setAttribute("mensaje_js", "cuadroDialogo (\"El reporte contiene mas de " + Global.MAX_REGISTROS + " registros, utilice la opcion exportar\")");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}
	}

	public void ejecutaExportar(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		textoLog("ejecutaExportar");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String nombreArchivo = "";
        String tipoArchivo= getFormParameter(request, "tipo_archivo");
        String archivo_actual= getFormParameter(request, "archivo_actual");

		if (tipoArchivo == null) tipoArchivo = "";
		if (archivo_actual == null) archivo_actual = "";

		ArchivoRemoto archR = new ArchivoRemoto();

		EIGlobal.mensajePorTrace(textoLog + "archR = " + archR, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(textoLog + "ArchivoEmpNom = " + session.getArchivoEmpNom(), EIGlobal.NivelLog.INFO);

		nombreArchivo = session.getArchivoEmpNom().substring(session.getArchivoEmpNom().lastIndexOf("/") + 1);

		if (tipoArchivo.equals("recuperado"))
			nombreArchivo += "_emp_sal.doc";
		else
			request.setAttribute("archivo_actual", archivo_actual);

		if (!archR.copiaLocalARemoto(nombreArchivo, "WEB")) {
			EIGlobal.mensajePorTrace(textoLog + "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
			request.setAttribute("mensaje_js", "cuadroDialogo (\"No se pudo exportar el archivo\",1)");
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		} else {
			request.setAttribute("mensaje_js", "window.location=\"/Download/" + nombreArchivo + "\"");
			String nombreArchivoEmp = session.getArchivoEmpNom();

			if (nombreArchivoEmp != null) { // MODIFICAR
				EIGlobal.mensajePorTrace(textoLog + "session.getArchivoEmpNom=" + nombreArchivoEmp, EIGlobal.NivelLog.INFO);
				archivoEmpleados archivo3 = new archivoEmpleados(nombreArchivoEmp, session.getClaveBanco());
				// Realiza la lectura del archivo para mostrarlo en pantalla
				request.setAttribute("total_registros", "" + archivo3.tamanioArchivo());
				request.setAttribute("archivo_actual", nombreArchivoEmp.substring(nombreArchivoEmp.lastIndexOf("/") + 1));
				request.setAttribute("facultades", sess.getAttribute("facultadesE"));
				request.setAttribute("lista_numeros_empleado", archivo3.lista_empleados.toString());
				request.setAttribute("registros_aceptados", archivo3.asAceptados.trim());
			}
			evalTemplate(IEnlace.NOM_MTTO_EMP_TMPL, request, response);
		}
	}

	public void guardaParametrosEnSession(HttpServletRequest request) {
		String template = request.getServletPath();
		request.getSession().setAttribute("plantilla", template);
		HttpSession session = request.getSession(false);
		session.removeAttribute("bitacora");

		if (session != null) {
			Map tmp = new HashMap();
            tmp.put("operacion",getFormParameter(request, "operacion"));
            tmp.put("envio",getFormParameter(request, "envio"));
			tmp.put("basefilename",getFormParameter(request, "basefilename"));
			tmp.put("facultades",getFormParameter(request, "facultades"));
			String value;
			value = (String) getFormParameter(request, "valida");
			if (value != null && !value.equals(null)) {
				tmp.put("valida",getFormParameter(request, "valida"));
			}

			tmp.put("statusDup",getFormParameter(request, "statusDup"));
			tmp.put("Sucursal",getFormParameter(request, "Sucursal"));
			tmp.put("total_registros",getFormParameter(request, "total_registros"));
			tmp.put("lista_numeros_empleado",getFormParameter(request, "lista_numeros_empleado"));
			tmp.put("tipo_archivo",getFormParameter(request, "tipo_archivo"));
			tmp.put("registro",getFormParameter(request, "registro"));

			session.setAttribute("parametros", tmp);

			tmp = new HashMap();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
	}
}