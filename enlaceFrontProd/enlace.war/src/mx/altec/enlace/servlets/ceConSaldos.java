package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.IOException;
import java.sql.SQLException;
import java.text.*;


/**
* ceConSaldos is a blank servlet to which you add your own code.
* <P>Modificaciones:<BR>
* 30/09/2002 Se corrigieron acentos. Se quit&oacute; el texto de ayuda mientras se define el mismo.<BR>
* 02/10/2002 Se agreg&oacute; <I>noLineas</I> como atributo.<BR>
* 04/10/2002 Se agreg&oacute; una columna en blanco en la segunda pantalla.<BR>
* </P>
*/
public class ceConSaldos extends BaseServlet
{

    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
	** prueba
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>displayMessage</code> allows for a short message to be streamed
    ** back to the user.  It is used by various NAB-specific wizards.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @param messageText the message-text to stream to the user.
    * <P>
    * 22/08/2002: Se vefific&oacute; la facultad de saldos. Hugo Ru&iacute;z Zepeda.
    * </P>
    */
   public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(messageText);
    }


    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {

	  String modulo;
  	  String fileOut = "";

	  Boolean facultadSaldo;

	  modulo       = "";
	  modulo = (String) req.getParameter( "Modulo" );
	  log( "defaultAction()::2da::Modulo =" + modulo );

	  //Modificaci�n para usuarios concurrentes
	  HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");



      boolean sesionvalida = SesionValida(req, res);

        if (sesionvalida)
		{

         /*Se obtiene y se verifica la facultad de saldo de cr�dito el�ctrico.
         * 22/08/2002
         */
         if(session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC))
         {
            facultadSaldo=new Boolean(session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC));
         }
         else
         {
            facultadSaldo=new Boolean(false);
            EIGlobal.mensajePorTrace("No se obtuvo el valor de la facultad de saldos de cr�dito electr�nico.", EIGlobal.NivelLog.INFO);
         } // Fin if-else facultad saldos cr�dito electr�nico



         // 30/09/2002 Se coment� con el fin de revisar el m�dulo
         if(!facultadSaldo.booleanValue())
         {
            getServletContext().getRequestDispatcher("/SinFacultades").forward(req, res);
            EIGlobal.mensajePorTrace("El usuario carece de la facultad de saldos de cr�dito electr�nico: "+session.getFacultad(session.FAC_TECREPOSICIOC_CRED_ELEC), EIGlobal.NivelLog.INFO);
            return;
         } // Fin if vefificaci�n facultad saldos cr�dito electr�nico




			fileOut = session.getUserID8()+".doc";  

			boolean grabaArchivo = true;

			EI_Exportar archSalida;
			archSalida = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			archSalida.creaArchivo();

			if (modulo.equals( "0" ) )
			{

        		if (session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC))
				{
	        		EIGlobal.mensajePorTrace("***ceConSaldos.class Entrando a ceConSaldos", EIGlobal.NivelLog.INFO);

					int      contador  = 0;
					boolean encontrado = false;


					String[] cta_posi2 = null;
					String cta = null;


					req.setAttribute("MenuPrincipal", session.getStrMenu());
				    req.setAttribute("newMenu", session.getFuncionesDeMenu());

 				    req.setAttribute("Encabezado", CreaEncabezado("Consulta de Saldos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Saldo","s32000h", req)); //Falta ayuda

			        req.setAttribute("Fecha",ObtenFecha());
				    String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
					datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
	                datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
		            req.setAttribute("Movfechas",datesrvr);


		            //TODO BIT CU3091 Entrada al flujo

		            /**
					 * VSWF
					 * 17/Enero/2007
					 */
		            if (Global.USAR_BITACORAS.trim().equals("ON")){
		            	try {
				            if(((String)req.getParameter(BitaConstants.FLUJO)).
				            		equals(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS)){
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								BitaTransacBean bt = new BitaTransacBean();
								bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								bt = (BitaTransacBean)bh.llenarBean(bt);

								bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_ENTRA);
								bt.setContrato((session.getContractNumber() == null)? " ":session.getContractNumber());

								BitaHandler.getInstance().insertBitaTransac(bt);
				            }
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}

		            }
					/**
					 * VSWF
					 */

	                session.setModuloConsultar(IEnlace.MCredito_empresarial);
                   //***********************************************
		            evalTemplate("/jsp/ceConSaldos.jsp", req, res);
				}else
					despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de Saldos de Cr&eacute;dito Electr&oacute;nico", "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Saldo","s32000h", req, res);
		    }
			else // Modulo = 1
			{
		    	if ( session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC))
				{
					String ctaSaldo     = "";
		            String tipoCuenta   = "";
		            String nombreCuenta = "";
					String Trama        = "";
					String []arrcta;
				    String pipe         = "|";
					String arroba		= "@";
					String tramaCtaSaldo;
					String Result       = "";
					String contrato     = "";
                    String usuario      = "";
        		    String clavePerfil  = "";
					String tabla = "";
					String estatusError = "";
					String errores = "";
					String titulo1 = "Consulta de Saldos de Cr&eacute;dito Electr&oacute;nico";
					String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Saldo";
					String ResultError = "";
					String noLineas = "";
					tramaCtaSaldo = (String )req.getParameter("EnlaceCuenta");
					String errorBit="";
					EIGlobal.mensajePorTrace("***ceConSaldos.class Entrando a ceConSaldosDetalles", EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace("***tramaCtaSaldo"+tramaCtaSaldo, EIGlobal.NivelLog.INFO);

					if (tramaCtaSaldo.indexOf("@")==-1)
			       {
					    ctaSaldo    = tramaCtaSaldo;
					    tipoCuenta   = "P";
					    nombreCuenta = "";
			       }
				    else
		           {
					    arrcta       = desentramaC("3@"+tramaCtaSaldo,'@');
						ctaSaldo    = arrcta[1];
			            tipoCuenta   = arrcta[2];
			            nombreCuenta = arrcta[3];
			       }

				   EIGlobal.mensajePorTrace("***movimientos.class cuentamov "+ctaSaldo, EIGlobal.NivelLog.INFO);
				   EIGlobal.mensajePorTrace("***movimientos.class tipocuenta "+tipoCuenta, EIGlobal.NivelLog.INFO);
				   EIGlobal.mensajePorTrace("***movimientos.class nombrecuenta "+nombreCuenta, EIGlobal.NivelLog.INFO);

					ServicioTux tuxGlobal = new ServicioTux();
					//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				    Hashtable htResult = null;

 					contrato      = session.getContractNumber();
            		usuario       = session.getUserID8(); 
        		    clavePerfil   = session.getUserProfile();


	                Trama+=IEnlace.medioEntrega2  +pipe;    // 1EWEB
                    Trama+=usuario          +pipe;			// empleado
                    Trama+="CEDC"           +pipe;			// tipo_operacion
                    Trama+=contrato         +pipe;			// contrato
                    Trama+=usuario          +pipe;			// empleado
                    Trama+=clavePerfil      +pipe;			// clave_perfil
                    Trama+=contrato         +arroba;		// contrato
                    Trama+=ctaSaldo         +arroba;        // Cta cheques
					Trama+=" "              +arroba;        // Linea de Credito
                    Trama+=usuario          +pipe;			// empleado

                    EIGlobal.mensajePorTrace("ceConSaldosDetalle - execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

					try {
						    	htResult = tuxGlobal.web_red( Trama );
						    } catch ( java.rmi.RemoteException re ) {
						    	re.printStackTrace();
						    }catch ( Exception e ){}

					Result = ( String ) htResult.get( "BUFFER" );
					EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
					ResultError = Result;
					errorBit= ResultError;
					if(Result.substring(19,23).equals("0000"))
					{
						Result=formateaResultLinea(usuario).trim();
						EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Trama Salida Formatea: "+Result, EIGlobal.NivelLog.INFO);
					}
					else
						Result="OPENFAIL";


                    if(Result.equals("OPENFAIL"))
                    {
						EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Estoy en el if: "+Result, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Estoy en el if: "+ResultError, EIGlobal.NivelLog.INFO);
						estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
						//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
						//req.setAttribute("Errores",errores);
						despliegaPaginaError(estatusError,titulo1,titulo2,"s32010h", req, res );
					}
					else
					{
						EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Estoy en el else:", EIGlobal.NivelLog.INFO);
						EI_Tipo ResultTmp=new EI_Tipo(this);

					    Result+=" @";
					    ResultTmp.strOriginal=Result;
				        ResultTmp.strCuentas=Result;
				        ResultTmp.numeroRegistros();
						ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
			            ResultTmp.llenaArreglo();
				        ResultTmp.separaCampos();


						noLineas += ctaSaldo+pipe+nombreCuenta+pipe;

						for (int i=0;i<ResultTmp.totalRegistros;i++)
						{

							 noLineas += ResultTmp.camposTabla[i][1]+pipe;

							String lineaCredito = new String(ResultTmp.camposTabla[i][1]);
							double autorizado = new Double(ResultTmp.camposTabla[i][4]).doubleValue();
							double dispuesto = new Double(ResultTmp.camposTabla[i][5]).doubleValue();
							double disponible = new Double(ResultTmp.camposTabla[i][6]).doubleValue();

							EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Autorizado: "+autorizado, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Dispuesto: "+dispuesto, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Disponible: "+disponible, EIGlobal.NivelLog.INFO);

                     tabla+="<TR><TD width=\"40\"><BR></TD>";
							tabla += "<td class=textabdatcla align=center><input type=radio name=ctasLineas value=\"3|"+ctaSaldo +"|"+lineaCredito+"|"+nombreCuenta+"|\"></td>";
						 	tabla += "<td class='textabdatcla' nowrap align=center>"+ ctaSaldo + "</td>";
							tabla += "<td class='textabdatcla' nowrap align=center>" + lineaCredito + "</td>";
							tabla += "<td class='textabdatcla' colspan=2>" + nombreCuenta + "</td>";
							tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(autorizado) + "</td>" ;
							tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(dispuesto) + "</td>" ;
							tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(disponible) + "</td>" ;
                     tabla += "<TD width=\"20\"><BR></TD>";
							tabla += "</tr>";
//							tabla += "<br>";
//							tabla += "<br>";


							// Exportar las lineas
							String lineaArchivo = "";

							lineaArchivo = ctaSaldo+" "+lineaCredito+" "+nombreCuenta+" "+FormatoMonedaExportar(autorizado)+" "+FormatoMonedaExportar(dispuesto)+" "+FormatoMonedaExportar(disponible);

							if (grabaArchivo)
							{
								archSalida.escribeLinea(lineaArchivo+ "\r\n");
							}

							 /**
							 * VSWF
							 * 17/Enero/2007
							 */
//							TODO BIT CU3091 o 3101 realiza operaci�n CEDEC
							if (Global.USAR_BITACORAS.trim().equals("ON")){
								try {
									BitaHelper bh = new BitaHelperImpl(req, session, sess);
									BitaTransacBean bt = new BitaTransacBean();
									bt = (BitaTransacBean)bh.llenarBean(bt);

									if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS)){
										bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_CEDC);
									}else
										if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS)){
											bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CEDC);
										}
									bt.setContrato((contrato == null)? " ":contrato);
									bt.setNombreArchivo((fileOut == null)? " ":fileOut);
									bt.setCctaOrig((ctaSaldo == null)?" ":ctaSaldo);
									bt.setImporte(disponible);
									bt.setServTransTux("CEDC");
									/*BMB-I*/
									if(errorBit!=null){
										if(errorBit.substring(0,2).equals("OK")){
											bt.setIdErr("CEDC0000");
										}else if(errorBit.length()>8){
											bt.setIdErr(errorBit.substring(0,8));
										}else{
											bt.setIdErr(errorBit.trim());
										}

									}
									BitaHandler.getInstance().insertBitaTransac(bt);
								} catch (SQLException e) {
									e.printStackTrace();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							/**
							 * VSWF
							 */
						}

						if (grabaArchivo)
							archSalida.cierraArchivo();

						ArchivoRemoto archRemoto = new ArchivoRemoto();
						if(!archRemoto.copiaLocalARemoto(fileOut,"WEB"))
							EIGlobal.mensajePorTrace("***ceConsaldos.cladd No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.INFO);


						//Exportar
						String exportar = "";
   						if ( grabaArchivo )
						exportar = "<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A>";
						req.setAttribute("Exportar",exportar);

						req.setAttribute("valor",tabla);
						req.setAttribute("cta",ctaSaldo);
                  req.setAttribute("noLineas", noLineas);
						req.setAttribute("MenuPrincipal", session.getStrMenu());
					    req.setAttribute("newMenu", session.getFuncionesDeMenu());


						req.setAttribute("Encabezado", CreaEncabezado("Consulta de Saldos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Saldo","s32010h", req));

				        session.setModuloConsultar(IEnlace.MCredito_empresarial);

				        evalTemplate("/jsp/ceConSaldosDetalle.jsp", req, res);
					}
					}
			}
	    }
		else if( sesionvalida )
		{
            despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de Saldos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Saldo", "s32010h", req, res);
		}

	}

/*************************************************************************************/
/*************************************************** formatea Reultado para Linea    */
/*************************************************************************************/
    String formateaResultLinea(String usuario)
     {
       String cadenaTCT="";
       String arcLinea="";
       String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;
	   int totalLineas=0;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 EIGlobal.mensajePorTrace("ceConSaldosDetalle- formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
        {
          boolean noError=true;
		  do
		  {
			arcLinea=ArcEnt.leeLinea();
          if(!arcLinea.equals("ERROR"))
            {
              if(arcLinea.substring(0,5).trim().equals("ERROR"))
                break;
              totalLineas++;
              if(totalLineas>=1)
                cadenaTCT+=arcLinea+"@";
            }
           else
             noError=false;
         }while(noError);
		 ArcEnt.cierraArchivo();
       }
       else
        return "OPENFAIL";
       return cadenaTCT;



    }

	public String FormatoMonedaExportar ( double cantidad ) {
         String formato = "";
         String language = "la"; // la
         String country = "MX";  // MX
         Locale local = new Locale (language,  country);
         NumberFormat nf = NumberFormat.getCurrencyInstance (local);
         if (cantidad < 0)  {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" -"+ formato.substring (4,formato.length ());
    	    		 else
    	    			 formato =" -"+ formato.substring (2,formato.length ());
             } catch(NumberFormatException e) {}
         } else {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" "+ formato.substring (3,formato.length ());
    	    		 else
    	    			 formato =" "+ formato.substring (1,formato.length ());
             } catch(NumberFormatException e) {}
         }
         return formato;
     }

}