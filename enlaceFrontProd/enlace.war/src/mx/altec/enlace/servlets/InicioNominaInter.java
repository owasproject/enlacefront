/**Banco Santander Mexicano
* Clase	MtoNomina, Carga la	plantilla para el mantenimineto	del	archivo	de pagos
* @author Rodrigo Godo
* @version 1.1
* fecha	de creacion: Diciembre 2000	- Enero	20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: Crea	un objeto del tipo TemplateMapBasic, carga un calendario para exhibirse	dentro del TemplateMap,
				asi como un combo con la lista de cuentas con las cuales puede operar el usuario,
				valida facultades para saber  que tipo de operaciones pueden efectuarse sobre los archivos de pago de nomina
				(creacion, importacion, altas, bajas, cambios, borrado y recuperacion de archivos,
				asi como la transmision de los pagos que habran de efectuarse).
*/

package	mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;



public class InicioNominaInter extends BaseServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
			defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException
	 {
		EIGlobal.mensajePorTrace("***InicioNominaInter.class &Entrando al metodo execute: &", EIGlobal.NivelLog.DEBUG);
		Vector diasNoHabiles;
		//Vector diasNoHabilesJS;
		CalendarNomina myCalendarNomina=null;
		String nuevoMenu;

		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    boolean sesionvalida = false;
		//diasNoHabilesJS = new Vector();
	    try{
		    if(session!=null){
				myCalendarNomina= new CalendarNomina(  );	
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser",ObtenContUser(request));
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				estableceFacultades(request,response);	
				sesionvalida = SesionValida( request, response );
		    }
	    }
	    catch(Exception e)
	    {
	    	EIGlobal.mensajePorTrace("*** LA SESION EXPITO "+e.getMessage(), EIGlobal.NivelLog.ERROR);
	    }
		if(sesionvalida)
		 {
			String VarFechaHoy = "";
			String strInhabiles = "";

			try
			{
			  diasNoHabiles = myCalendarNomina.CargarDias();
			  strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
			}catch(ArrayIndexOutOfBoundsException e)
			 {
				EIGlobal.mensajePorTrace("*** InicioNominaInter.class Error al crear los dias inhabiles", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("*** InicioNominaInter.class MSG "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				strInhabiles ="";
			 }
			//TODO: BIT CU 4161, EL cliente entra al flujo
			/*
    		 * VSWF ARR -I
    		 */
			if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{

			BitaHelper bh = new BitaHelperImpl(request, session,sess);
			bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_NOMINA_INTERBANCARIA_ENTRA);
			bt.setContrato(session.getContractNumber());

			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
			}
    		/*
    		 * VSWF ARR -F
    		 */
			Calendar fechaFin = myCalendarNomina.calculaFechaFin();
			VarFechaHoy = myCalendarNomina.armaArregloFechas();
			String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();

			Hashtable Facultades=(Hashtable)sess.getAttribute("FacultadesNominaInterbancaria");
			EIGlobal.mensajePorTrace("***InicioNominaInter.class Facultades se obtienen: " + Facultades.size(), EIGlobal.NivelLog.INFO);
			boolean Acceso=((Boolean)Facultades.get("Accesa")).booleanValue();
			EIGlobal.mensajePorTrace("***InicioNominaInter.class Acceso al modulo:  " + Acceso, EIGlobal.NivelLog.INFO);

			if(Acceso)
			 {
				verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",true);

				if(request.getSession().getAttribute("Recarga_Opc_Nomina")!=null)
				  {
					request.getSession().removeAttribute("Recarga_Opc_Nomina");
					EIGlobal.mensajePorTrace( "OpcionesNominaInter Borrando variable de sesion.", EIGlobal.NivelLog.DEBUG);
				  }
				else
					EIGlobal.mensajePorTrace( "OpcionesNominaInter La variable ya fue borrada", EIGlobal.NivelLog.DEBUG);

				if(request.getSession().getAttribute("Recarga_Opc_Nomina2")!=null)
				  {
					request.getSession().removeAttribute("Recarga_Opc_Nomina2");
					EIGlobal.mensajePorTrace( "OpcionesNominaInter Borrando segunda variable de sesion.", EIGlobal.NivelLog.DEBUG);
				  }
				else
					EIGlobal.mensajePorTrace( "OpcionesNominaInter La segunda variable ya fue borrada", EIGlobal.NivelLog.DEBUG);

				String datesrvr="";
				datesrvr+= "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
				datesrvr+= "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
				datesrvr+= "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";

				request.setAttribute( "VarFechaHoy", VarFechaHoy);
				request.setAttribute( "fechaFin", strFechaFin);
				request.setAttribute( "diasInhabiles", strInhabiles);
				request.setAttribute( "Fechas", ObtenFecha( true ) );
				request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
				request.setAttribute( "ContenidoArchivo", "" );
				request.setAttribute( "Encabezado",CreaEncabezado( "Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request ) );

				String NumberOfRecords = "";
				if(session.getNominaNumberOfRecords() != null)
				NumberOfRecords = session.getNominaNumberOfRecords();

				request.setAttribute( "totRegs", NumberOfRecords);
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nom_IN);

				evalTemplate( "/jsp/MantenimientoNominaInter.jsp", request, response );
			 }
			else
			 {
				String laDireccion = "document.location='SinFacultades'";
				request.setAttribute( "plantillaElegida", laDireccion);
				request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
			 }
		}
	}


  // M�todo que establecer� las facultades para el m�dulo de pago de n�mina interbancaria
  public void estableceFacultades( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException
	 {
		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		/**
		* Eliminar el attributo de la session para actualizarlo con las facultades correspondientes
		*/
		if(sess.getAttribute("FacultadesNominaInterbancaria")!=null)
		   sess.removeAttribute("FacultadesNominaInterbancaria");

		Hashtable Facultades=new Hashtable();

		Facultades.put("Importar",new Boolean(verificaFacultad(session.FAC_NOMINT_IMPO,request)));
		Facultades.put("Alta",new Boolean(verificaFacultad(session.FAC_NOMINT_ALTA,request)));
		Facultades.put("Edicion",new Boolean(verificaFacultad(session.FAC_NOMINT_EDIT,request)));
		Facultades.put("Baja",new Boolean(verificaFacultad(session.FAC_NOMINT_BAJA,request)));
		Facultades.put("Envio",new Boolean(verificaFacultad(session.FAC_NOMINT_ENVI,request)));
		Facultades.put("Recupera",new Boolean(verificaFacultad(session.FAC_NOMINT_RECU,request)));
		Facultades.put("Exporta",new Boolean(verificaFacultad(session.FAC_NOMINT_EXPO,request)));
		Facultades.put("Borra",new Boolean(verificaFacultad(session.FAC_NOMINT_BORR,request)));
		Facultades.put("Accesa",new Boolean(verificaFacultad(session.FAC_NOMINT_ACCE,request)));
		Facultades.put("CuentasNoReg",new Boolean(verificaFacultad(session.FAC_NOMINT_CTNR,request)));

		sess.setAttribute("FacultadesNominaInterbancaria",Facultades);

		EIGlobal.mensajePorTrace("Se establecieron las facultades de Nomina Interbancaria en session.", EIGlobal.NivelLog.DEBUG);
	}

// Funcionalidad para verificar Archivos.
public boolean verificaArchivos(String arc,boolean eliminar)
	throws ServletException, java.io.IOException
{
	EIGlobal.mensajePorTrace( "Consult.java Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
	File archivocomp = new File( arc );
	if (archivocomp.exists())
	 {
	   EIGlobal.mensajePorTrace( "Consult.java El archivo si existe.", EIGlobal.NivelLog.DEBUG);
	   if(eliminar)
		{
	      archivocomp.delete();
		  EIGlobal.mensajePorTrace( "Consult.java El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
		}
	   return true;
	 }
	EIGlobal.mensajePorTrace( "Consult.java El archivo no existe.", EIGlobal.NivelLog.DEBUG);
	return false;
}


} //Fin de la clase