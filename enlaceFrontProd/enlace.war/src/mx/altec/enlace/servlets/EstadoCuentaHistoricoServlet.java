package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceException;

import org.apache.commons.lang.StringUtils;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.beans.ConfEdosCtaTcIndBean;
import mx.altec.enlace.beans.EstadoCuentaHistoricoBean;
import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.beans.TipoFormatoEstadoCuenta;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConfEdosCtaIndBO;
import mx.altec.enlace.bo.ConfEdosCtaTcIndBO;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.bo.EstadoCuentaHistoricoBO;
import mx.altec.enlace.bo.ValidationException;
import mx.altec.enlace.dao.DY02DAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class EstadoCuentaHistoricoServlet extends BaseServlet {
	/** Id de version generado automaticamente para la serializacion de objeto */
	private static final long serialVersionUID = 5476264382078623903L;
	/** Id para identificar la pantalla inicial */
	private static final String VENTANA_INICIAL = "0";
	/** Id para identificar la pantalla que permite seleccionar los periodos */
	private static final String VENTANA_SELECCION_PERIODOS = "1";
	/** Id para identificar la pantalla que realiza la solicitud de los estados de cuenta */
	private static final String VENTANA_REALIZAR_PETICION = "2";
	/** Id para identificar la operacion que realiza el cambio de cuenta */
	private static final String VALIDAR_CAMBIO_CUENTA = "4";
	/** Id para identificar la operacion que realiza el llamado al WebService **/
	private static final String VALIDAR_PERIODOS_WEBSERVICE = "5";
	/** Identificador de las sesion guardada en el request */
	private static final String SESSION_NAME = "session";
	/** Pantalla inicial */
	private static final String URL_INICIO = "EstadoCuentaHistoricoServlet?ventana=0";
	/** Variable para identificar el menu principal */
	private static final String STR_MENU_PRINCIPAL = "MenuPrincipal";
	/** Variable para identificar el nuevo menu */
	private static final String STR_NEW_MENU = "newMenu";
	/** Variable para identificar el encabezado */
	private static final String STR_ENCABEZADO = "Encabezado";
	/** Variable para identificar la posicion del menu */
	private static final String STR_POSICION_MENU = "Estados de Cuenta &gt; Solicitud de Periodos Hist&oacute;ricos formato PDF &gt; Solicitud";
	/** Variable para identificar el texto: Estado de Cuenta */
	private static final String STR_ESTADO_CUENTA = "Estados de Cuenta";
	///** Variable para identificar el codigo de error utilizado */
	//private static final String COD_ERROR = "s55140h";
	/** Variable para identificar el texto: FechaHoy */
	private static final String STR_FECHA_HOY = "FechaHoy";
	/** Formato de la fecha */
	private static final String FORMATO_FECHAS = "dd/mm/aaaa";
	/** Codigo de Transacciones */
	private static final String CODIGO_SERVICIO_TUX = "ODD4-OW42-WS2";
	/** Codigo de Transacciones Finalizacion */
	private static final String CODIGO_SERVICIO_TUX_FINALIZA= "DY02";
	/** Codigo de retorno del WS El Estado de Cuenta del periodo solicitado YA esta disponible en Ondemand **/
	private static final int WS_PERIODO_EDC_EXISTE = 100;
	/** Codigo de retorno del WS El Estado de Cuenta del periodo solicitado NO esta disponible en Ondemand **/
	private static final int WS_PERIODO_EDC_NOEXISTE = 301;
	/** Codigo de retorno para solicitudes ya existentes en la tabla eweb_edo_cta_his **/
	private static final String COD_SOLICITUD_PREVIA = "501";
	/** Codigo de retorno cuando el web service no responde a la solicitud **/
	private static final String COD_SERVICIO_NO_DISPONIBLE = "601";
	/** Codigo de retorno al llamado por ajax, que devuelve error **/
	private static final int AJAX_ERROR_GENERICO = 1;

	/** Atributo del request Clave Banco **/
	private static final String CVE_BANCO_REQUEST = "ClaveBanco";
	/** Atributo del request web_application **/
	private static final String WEB_APP_REQUEST = "web_application";
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_SoliPerHis";
	/** Atributo del request Cuenta **/
	private static final String CUENTA_TXT_REQUEST = "Cuentas";
	/** Atributo del request CambioCuentaTXT **/
	private static final String CAMBIO_CTATXT_REQUEST = "cambioCuentaTXT";
	/** Atributo del request Error **/
	private static final String ERROR_REQUEST = "ERROR";

	/** Constante Clave Banco **/
	private static final String CVE_BANCO = "014" ;
	/** Constante para mensaje usuario sin facultades **/
	private static final String STR_SIN_FACULTADES =  "<br>Usuario sin facultades para operar con este servicio<br>";
	/** Constante para definir separador**/
	private static final String STR_SEPARADOR = ",";
	/** Constante HTML **/
	private static final String STR_CADENA_HTML = "</td><td>";
	/** Constante para el campo en la finalizacion de solicitud **/
	private static final String CAMPO_FIN = "ID_EDO_CTA_HIST";
	/** Constante para el campo en la finalizacion de solicitud **/
	private static final String TABLA_FIN = "EWEB_EDO_CTA_HIST";
	/** Atributo del request ODD4 **/
	private static final String ODD4_BEAN_REQUEST = "odd4Bean";
	/** Atributo del request OW42 **/
	//private static final String OW42_BEAN_REQUEST = "ow42Bean";

	/** Constante para exportar cuentas **/
	private static final String CONTENT_TYPE="text/plain";
	/** Constante para exportar cuentas **/
	private static final String HNAME_CONTENT_DISPOSITION="Content-Disposition";
	/** Constante para exportar cuentas **/
	private static final String HVALUE_CONTENT_DISPOSITION="attachment;filename=Cuentas.txt";
	/** Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion */
	private static final int BYTES_DOWNLOAD = 1024;

	/** Constante Para el Log **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: EstadoCuentaHistoricoServlet ::: ";
	
	/** Constante que indica la literal flujoOp */
	private static final String FLUJOOP = "flujoOp";
	/** Constante para tarjeta */
	private static final String TARJETA = "txtTarjeta";
	/** Constante que indica tipo cuenta */
	private static final String TCUENTA = "001";
	/** Constante que indica tipo tarjeta */
	private static final String TTARJETA = "003";
	/** Constante codigo de exito de ODD4 */
	private static final String COD_EXITO = "ODA0002";
	/** Constante para estado de cuenta disponible */
	private static final String EDO_CTA_DISP = "edoCtaDisponible";
	/** Constante para eval template **/
	private static final String EVAL_TEMPLATE="/jsp/SeleccionPeriodosPDFECH.jsp";
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Solicitud de Estados de Cuenta Hist&oacute;ricos";
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Estados de Cuenta &gt; Solicitud de Periodos Hist&oacute;ricos formato PDF &gt; Solicitud";
	/** Constante para session de BaseResource */
	private static final String BASE_RESOURCE = "session";
	/**
	 * Constante para Texto opcion.
	 */
	private static final String OPCION = "opcion";
	
	/**
	 * Constante para Texto tipoOp.
	 */
	private static final String TIPO_OP = "tipoOp";
	
	/**
	 * Constate para texto fechaConfigValida
	 */
	private static final String FECHA_CONFIG_VALIDA = "fechaConfigValida";
	
	/**
	 * Constate para texto tarjetas
	 */
	private static final String TARJETAS = "tarjetas";
	

	/**
	 * Constate para texto "msgExcede"
	 */
	private static final String MSG_EXCEDE = "msgExcede";
	
	/** Instancia de clase de negocios **/
	private final transient EstadoCuentaHistoricoBO estadoCuentaBO = new EstadoCuentaHistoricoBO();

	/**
	 * Recibe las peticiones Get de las peticiones en Solicitud de Estados de Cuenta Historicos
	 * Solo recibe la peticion y la redirecciona al metodo iniciaServlet
	 * @param request : request Objeto con los datos de la peticion
	 * @param response : responseObjeto con los datos de la respuesta
	 * @throws ServletException : ServletException En caso de que exista un error en la operacion
	 * @throws IOException : IOException En caso de que exista un error en la operacion
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
	throws ServletException, IOException {
		EIGlobal.mensajePorTrace( "EstadoCuentaHistorico-> Metodo DoGet", NivelLog.INFO);
		iniciaServlet( request, response );
	}

	/**
	 * Recibe las peticiones Get de las peticiones en Solicitud de Estados de Cuenta Historicos
	 * Solo recibe la peticion y la redirecciona al metodo iniciaServlet
	 * @param request :r esponse Objeto con los datos de la peticion
	 * @param response : response Objeto con los datos de la respuesta
	 * @throws ServletException : ServletExceptionEn caso de que exista un error en la operacion
	 * @throws IOException : IOException En caso de que exista un error en la operacion
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
	throws ServletException, IOException {
		EIGlobal.mensajePorTrace( "EstadoCuentaHistorico-> Metodo DoPost", NivelLog.INFO);
		iniciaServlet( request, response );
	}

	/**
	 * Carga valores para encabezados y menu de la aplicacion
	 * @param request request de la peticion
	 */
	public void cargaMenuEncabezados (HttpServletRequest request) {
		/************* Modificacion para la sesion ***************/
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		final EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);

		request.setAttribute(STR_MENU_PRINCIPAL, session.getStrMenu());
		request.setAttribute(STR_NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute(STR_ENCABEZADO, CreaEncabezado(STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA,request));
		request.setAttribute(STR_FECHA_HOY, EnlaceGlobal.fechaHoy(FORMATO_FECHAS));
		EIGlobal.mensajePorTrace(String.format("%s Enviando fechas y encabezados",LOG_TAG), NivelLog.INFO);
		estadoCuentaBO.agregarDatosComunes(request, session);
		request.setAttribute(CVE_BANCO_REQUEST,	(session.getClaveBanco() == null) ? CVE_BANCO : session.getClaveBanco());
		request.setAttribute(WEB_APP_REQUEST,Global.WEB_APPLICATION);
		EIGlobal.mensajePorTrace(String.format("%s Enviando las nuevas variables y Verificando Facultades", LOG_TAG),NivelLog.INFO);
		//session.setModuloConsultar(IEnlace.MOperaciones_mancom);
	}

	/**
	 * Valida que tipo de peticion se ha realizado y ejecuta el metodo que sea
	 * necesario
	 * @param request :request Objeto con los datos de la peticion
	 * @param response : response Objeto con los datos de la respuesta
	 * @throws ServletException : En caso de que exista un error en la operacion
	 * @throws IOException : En caso de que exista un error en la operacion
	 */
	public void iniciaServlet( HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		/************* Modificacion para la sesion ***************/
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);

		cargaMenuEncabezados(request);
		String ventana = (String)request.getParameter("ventana");
		setAtributos(request);
		if(ventana == null){
			ventana = VENTANA_INICIAL;
		}
		EIGlobal.mensajePorTrace(String.format("%sEntrando ventana = %s", LOG_TAG,ventana),NivelLog.INFO);
		boolean sesionValida = SesionValida(request, response);
		if (sesionValida) {
			if(session.getFacultad(BaseResource.FAC_CON_PERHIST)){
				if(VENTANA_INICIAL.equals(ventana)){
					iniciaConsulta(request, response);
				} else if( "CTATARJETACOMBO".equals(ventana) ) {
					request.setAttribute(OPCION,"1");
					request.setAttribute(FLUJOOP,"CONS");
					ctasTarjetas(request, response);
					evalTemplate("/jsp/InicioECH.jsp", request, response );
				} else if(VENTANA_SELECCION_PERIODOS.equals(ventana)){
					mostrarPantallaPeriodos(request, response);
				} else if(VENTANA_REALIZAR_PETICION.equals(ventana)){
					realizarSolicitudEstadoCuenta(request, response);
				} else if(VALIDAR_CAMBIO_CUENTA.equals(ventana)) {
					validarCambioCuenta(request, response);
				} else if (VALIDAR_PERIODOS_WEBSERVICE.equals(ventana) ) {
					validarPeriodosWebservice(request, response);
				} else if( "EXPCTA".equals(ventana) || "EXPTARJ".equals(ventana) ) {
					exportar(request, response);
				}
			} else {
				despliegaPaginaErrorURL(STR_SIN_FACULTADES,
						STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA, null, request, response);
			}
		}
	}




	/**
	 * Inicia el proceso de la solicitud de estado de cuenta
	 * @param request : request Objeto con la informacion del Request
	 * @param response : response  Objeto con la informacion de Response
	 * @throws ServletException : En caso de que exista un error en la operacion
	 * @throws IOException : En caso de que exista un error en la operacion
	 */
	public void iniciaConsulta(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s Entrando iniciaConsulta", LOG_TAG) , NivelLog.INFO);

		/************* Modificacion para la sesion ***************/
		if (request.getSession().getAttribute(ODD4_BEAN_REQUEST)!=null) {
			EIGlobal.mensajePorTrace(String.format("%s Asegura un nuevo odd4Bean.",LOG_TAG), NivelLog.DEBUG);
			request.getSession().removeAttribute(ODD4_BEAN_REQUEST);
		}
		request.setAttribute(FLUJOOP,"CONS");
		request.setAttribute(OPCION,"");
		ctasTarjetas(request, response);

		evalTemplate("/jsp/InicioECH.jsp", request, response );

	}

	/**
	 * Muestra la pantalla para la seleccion de los periodos
	 * @param request : request Contiene la informacion de la peticion al servlet
	 * @param response : response Contiene la informacion de la respuesta del servlet
	 * @throws ServletException : En caso de un error en la operacion
	 * @throws IOException : En caso de un error en la operacion
	 */
	public void mostrarPantallaPeriodos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s Entrando mostrarPantallaPeriodos", LOG_TAG), NivelLog.INFO);

		final String txtCuenta = request.getParameter("hdnCuentasTarjetas");
		request.setAttribute("cuentaFormatoOriginal", txtCuenta);
		mostrarPantallaPeriodosPDF(request, response, txtCuenta);

	}

	/**
	 * Muestra la pantalla para la seleccion de los periodos para estado de
	 * cuenta en PDF
	 * @param request : request Contiene la informacion de la peticion al servlet
	 * @param response : response Contiene la informacion de la respuesta del servlet
	 * @param txtCuenta :txtCuenta  Cuenta y titular separados por pipes
	 * @throws ServletException : En caso de un error en las operaciones
	 * @throws IOException :  En caso de un error en las operaciones
	 */
	public void mostrarPantallaPeriodosPDF(HttpServletRequest request, HttpServletResponse response, String txtCuenta)
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s CuentaSeleccionada [%s]",LOG_TAG,txtCuenta),NivelLog.INFO);
		String numeroCuentaNuevo = request.getParameter(CAMBIO_CTATXT_REQUEST);
		List<String> aux = new ArrayList<String>();
		String numeroCuenta = txtCuenta;
		if(numeroCuentaNuevo == null || "".equals(numeroCuentaNuevo)) {
			numeroCuentaNuevo = numeroCuenta;
		}
		String errorCod = request.getAttribute(ERROR_REQUEST) != null ? request.getAttribute(ERROR_REQUEST).toString() : "";
		if("ERRORIN01".equals(errorCod) || "ERRORIN00".equals(errorCod)) {
			numeroCuenta = txtCuenta;//datosCuentaSeleccionada[POSICION_NUMERO_CUENTA]; BVB
		}
		EIGlobal.mensajePorTrace(String.format("%s Cuenta Separada [%s]",LOG_TAG,numeroCuenta),NivelLog.INFO);
		if(numeroCuenta == null || numeroCuenta.length() == 0){
			despliegaPaginaConMensaje("Ocurrio un error al obtener los periodos", STR_ESTADO_CUENTA,
					STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
		} else {

			boolean ocurrioUnError = false;
			try {
				
				String cuenta = "";
				if(null == numeroCuentaNuevo || "".equals(numeroCuentaNuevo)){
					cuenta = request.getParameter("hdnCuentasTarjetas") != null ? request
							.getParameter("hdnCuentasTarjetas") : "";
				}else{
					cuenta = numeroCuentaNuevo;
				}
				
				final HttpSession httpSession = request.getSession();
				//[DEDA-01] Entra a pantalla de solicitud de Periodos Historicos PDF
				BitaAdminBean bean = new BitaAdminBean();
				bean.setTipoOp(" ");
				bean.setIdTabla(" ");
				bean.setServTransTux(CODIGO_SERVICIO_TUX);
				bean.setDato(cuenta);
				bean.setReferencia(100000);
				BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.ENTRA_SOLICITUD_ESTADOS_HISTORICOS, bean, false);
				
				ConfEdosCtaIndBO confEdosCtaIndBO = new ConfEdosCtaIndBO();
				ConfEdosCtaTcIndBO confEdosCtaTcIndBO = new ConfEdosCtaTcIndBO();
		    	ConfEdosCtaIndBean confEdosCtaIndBean = null;
		    	ConfEdosCtaTcIndBean confEdosCtaTcIndBean = null;
						    	
				String tarjeta = "";
				
				SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy", Locale.US);
				String fechaHoy = sFormat.format(new Date());
				String tipoOp = "";
				String codigoCliente = "";
				
				if (cuenta.trim().length() == 16) {
					tarjeta = cuenta.trim(); 
					cuenta = "";
					tipoOp = TTARJETA;
				} else {
					cuenta = cuenta.trim();
					tarjeta = "";
					tipoOp = TCUENTA;
				}
				
				if(TCUENTA.equals(tipoOp)){
					//Es por cuenta
					//bitacorizaAdmin(request, confEdosCtaIndBean, BitaConstants.EA_OP_PDF_EDO_CTA_IND,true);
					confEdosCtaIndBean = confEdosCtaIndBO.mostrarCuentasPDF(cuenta);
					request.setAttribute("segmentoDom", confEdosCtaIndBean.getSecDomicilio());
					request.setAttribute("domicilio", confEdosCtaIndBean.getDomCompleto());
					request.setAttribute(EDO_CTA_DISP, confEdosCtaIndBean.isEdoCtaDisponible());
					request.setAttribute("codCliente", confEdosCtaIndBean.getCodCliente());
					
					if(confEdosCtaIndBean.isTieneProblemaDatos()){
						request.setAttribute("MENSAJE_PROBLEMA_DATOS", confEdosCtaIndBean.getMensajeProblemaDatos());
					}
					
					request.setAttribute("cuenta", cuenta);
					
					request.setAttribute(FLUJOOP,"EDIT");
					request.setAttribute(TIPO_OP,TCUENTA);
					
					if(confEdosCtaIndBean.getFechaConfig() != null) {
						request.setAttribute(FECHA_CONFIG_VALIDA,
								!StringUtils.equals(fechaHoy, sFormat.format(confEdosCtaIndBean.getFechaConfig())) ? "SI" : "NO");
					} else {
						request.setAttribute(FECHA_CONFIG_VALIDA, "SI");
					}
					
					
					if(COD_EXITO.equals(confEdosCtaIndBean.getCodRetorno()) || "DYE0021".equals(confEdosCtaIndBean.getCodRetorno())
							|| ("ODE0065".equals(confEdosCtaIndBean.getCodRetorno()) && confEdosCtaIndBean.getCuentas().size() > 0 )) {
						request.getSession().setAttribute("cuentas", confEdosCtaIndBean.getCuentas());
					}else {
						EIGlobal.mensajePorTrace("CodRetorno ["+confEdosCtaIndBean.getCodRetorno()+"]",
								EIGlobal.NivelLog.DEBUG);
						despliegaPaginaErrorURL(confEdosCtaIndBean.getDescRetorno()  == null || "".equals(confEdosCtaIndBean.getDescRetorno()) ? "Ocurrio un error al obtener los periodos para PDF"
								: confEdosCtaIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, "EstadoCuentaHistoricoServlet", request, response);
						return;
					}
					
					EIGlobal.mensajePorTrace(String.format("%s Numero Cliente[%s]",LOG_TAG,confEdosCtaIndBean.getCodCliente()), NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("%s Secuencia Domicilio[%s]",LOG_TAG,confEdosCtaIndBean.getSecDomicilio()), NivelLog.INFO);
					if(null != confEdosCtaIndBean.getCuentas()){
						EIGlobal.mensajePorTrace(String.format("%s Numeros De Cuenta[%s]",LOG_TAG,confEdosCtaIndBean.getCuentas().toArray()), NivelLog.INFO);
						httpSession.setAttribute("cuentasRelacionadas", confEdosCtaIndBean.getCuentas());
					}else{
						httpSession.setAttribute("cuentasRelacionadas", aux);
					}
					EIGlobal.mensajePorTrace(String.format("%s Nombre Completo [%s]",LOG_TAG,confEdosCtaIndBean.getNombre()), NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("%s Domicilio Completo[%s]",LOG_TAG,confEdosCtaIndBean.getDomCompleto()), NivelLog.INFO);
					codigoCliente = confEdosCtaIndBean.getCodCliente();
					
				}else if(TTARJETA.equals(tipoOp)){
					//Es por tarjeta
					confEdosCtaTcIndBean = confEdosCtaTcIndBO.mostrarTarjetasPDF(tarjeta);
					
					if(confEdosCtaTcIndBean.isTieneProblemaDatos()){
//						request.setAttribute("MENSAJE_PROBLEMA_DATOS", confEdosCtaTcIndBean.getMensajeProblemaDatos());
						EIGlobal.mensajePorTrace("CodRetorno ["+confEdosCtaTcIndBean.getCodRetorno()+"]",
								EIGlobal.NivelLog.DEBUG);
						despliegaPaginaErrorURL(confEdosCtaTcIndBean.getDescRetorno() == null || "".equals(confEdosCtaTcIndBean.getDescRetorno()) ? "Ocurrio un error al obtener los periodos para PDF" 
								: confEdosCtaTcIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA, "EstadoCuentaHistoricoServlet", request, response);
						return;
					}
					
					request.setAttribute("contrato", confEdosCtaTcIndBean.getCodent() + "-" + confEdosCtaTcIndBean.getCentalt() + "-" + confEdosCtaTcIndBean.getContrato());
					request.setAttribute("credito", tarjeta);
					request.setAttribute("descripcion", confEdosCtaTcIndBean.getNombreComp());
					EIGlobal.mensajePorTrace("confEdosCtaTcIndBean.getDomCompleto["+confEdosCtaTcIndBean.getDomCompleto()+"]", EIGlobal.NivelLog.DEBUG);
					request.setAttribute("domicilio", confEdosCtaTcIndBean.getDomCompleto());
					request.setAttribute("codCliente", confEdosCtaTcIndBean.getCodCliente());
					request.setAttribute("segmentoDom", "0");
					request.setAttribute(TARJETA, tarjeta);
					request.setAttribute(FLUJOOP,"EDIT");
					request.setAttribute(TIPO_OP,TTARJETA);
					
					if(confEdosCtaTcIndBean.getFechaConfig() != null) {
						request.setAttribute(FECHA_CONFIG_VALIDA,
								!StringUtils.equals(fechaHoy, sFormat.format(confEdosCtaTcIndBean.getFechaConfig())) ? "SI" : "NO");
					} else {
						request.setAttribute(FECHA_CONFIG_VALIDA, "SI");
					}
					request.getSession().setAttribute(TARJETAS, confEdosCtaTcIndBean.getTarjetas());
					EIGlobal.mensajePorTrace(String.format("%s Numero Cliente[%s]",LOG_TAG,confEdosCtaTcIndBean.getCodCliente()), NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("%s Secuencia Domicilio[%s]",LOG_TAG,confEdosCtaTcIndBean.getSecDomicilio()), NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("%s Nombre Completo [%s]",LOG_TAG,confEdosCtaTcIndBean.getNombreComp()), NivelLog.INFO);
					EIGlobal.mensajePorTrace(String.format("%s Domicilio Completo[%s]",LOG_TAG,confEdosCtaTcIndBean.getDomCompleto()), NivelLog.INFO);
					if(null != confEdosCtaTcIndBean.getTarjetas()){
						EIGlobal.mensajePorTrace(String.format("%s Numeros De Cuenta[%s]",LOG_TAG,confEdosCtaTcIndBean.getTarjetas().toArray()), NivelLog.INFO);
						httpSession.setAttribute(TARJETAS, confEdosCtaTcIndBean.getTarjetas());
					}else{
						httpSession.setAttribute(TARJETAS, aux);
					}
					codigoCliente = confEdosCtaTcIndBean.getCodCliente();
					httpSession.setAttribute("cuentasRelacionadas", aux);
				}
				final OW42Bean ow42Bean = estadoCuentaBO.obtenerPeriodos(numeroCuentaNuevo,codigoCliente);		//50000147300
				
				EIGlobal.mensajePorTrace(String.format("%s Parejas Periodo Folio[%s]",LOG_TAG,ow42Bean.obtenerParejasPeriodoFolio().toString()), NivelLog.INFO);

				String valorCambioCuenta = request.getAttribute(CAMBIO_CTATXT_REQUEST) != null ? request.getAttribute(CAMBIO_CTATXT_REQUEST).toString() :  numeroCuentaNuevo;
				request.setAttribute("cuentaSeleccionda", numeroCuenta);
				request.setAttribute("periodosRecientes", ow42Bean.obtenerParejasPeriodoFolio() != null ? ow42Bean.obtenerParejasPeriodoFolio() : aux);
				request.setAttribute(CAMBIO_CTATXT_REQUEST, valorCambioCuenta);
				
				evalTemplate(EVAL_TEMPLATE, request, response);

			} catch (BusinessException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				despliegaPaginaConMensaje(e.getMessage(), STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}

			if(ocurrioUnError){
				despliegaPaginaConMensaje("Ocurrio un error al obtener los periodos para PDF", STR_ESTADO_CUENTA,
						STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}
		}
	}

	/**
	 * Despliega una pagina de error
	 * @param error : error El error que se desea mostrar
	 * @param tituloPantalla : tituloPantalla Titulo de la pantalla de error
	 * @param posicionPantalla : posicionPantalla Posicion del contenido de la pantalla de error
	 * @param claveAyuda : claveAyuda Clave de ayuda para identificar el error
	 * @param request : request Contiene la informacion de la peticion al servlet
	 * @param response : response Contiene la informacion de la respuesta del servlet
	 * @param URL : URL Ruta a donde se va a redirigir la peticion
	 * @throws IOException : En caso de error con las operaciones
	 * @throws ServletException : En caso de error con las operaciones
	 */
	private void despliegaPaginaConMensaje(String error, String tituloPantalla, String posicionPantalla, String claveAyuda,
			HttpServletRequest request, HttpServletResponse response, String URL) throws IOException, ServletException{

		EIGlobal.mensajePorTrace("Entrando a despliegaPaginaConMensaje MENSAJE ->["+error+"]", NivelLog.DEBUG);

		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		final EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		request.setAttribute(STR_MENU_PRINCIPAL, session.getStrMenu() );
		request.setAttribute(STR_NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute(STR_ENCABEZADO,CreaEncabezado(tituloPantalla, posicionPantalla, claveAyuda, request));
		request.setAttribute(STR_FECHA_HOY, EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		EIGlobal.mensajePorTrace(String.format("%s Enviando fechas y encabezados",LOG_TAG), NivelLog.INFO);
		estadoCuentaBO.agregarDatosComunes(request, session);
		request.setAttribute(CVE_BANCO_REQUEST,	(session.getClaveBanco() == null) ? CVE_BANCO : session.getClaveBanco());
		request.setAttribute(WEB_APP_REQUEST,Global.WEB_APPLICATION);
		EIGlobal.mensajePorTrace(String.format("%s Enviando las nuevas variables y Verificando Facultade", LOG_TAG),NivelLog.INFO);
		request.setAttribute("URL", URL);
		request.setAttribute("Error", error );

		final RequestDispatcher rdSalida = getServletContext().getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		rdSalida.include( request, response );
	}

	/**
	 * Realiza la solicitud del estado de cuenta
	 * @param request : request Contiene la informacion de la peticion al servlet
	 * @param response : responseContiene la informacion de la respuesta del servlet
	 * @throws ServletException : En caso de error en la operacion
	 * @throws IOException : En caso de error en la operacion
	 */
	public void realizarSolicitudEstadoCuenta(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		//final EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
		String mensaje="";
		String mensajeError="";
		StringBuffer mensajeFinal=new StringBuffer();

		EIGlobal.mensajePorTrace(String.format("%s Entrando realizarSolicitudEstadoCuenta",LOG_TAG), NivelLog.INFO);
		//request.setAttribute(STR_MENU_PRINCIPAL, session.getStrMenu());
		//request.setAttribute(STR_NEW_MENU, session.getFuncionesDeMenu());
		//request.setAttribute(STR_ENCABEZADO,CreaEncabezado(STR_ESTADO_CUENTA, STR_POSICION_MENU, COD_ERROR,request));
		//request.setAttribute(STR_FECHA_HOY, EnlaceGlobal.fechaHoy(FORMATO_FECHAS));
		//request.setAttribute(CVE_BANCO_REQUEST,(session.getClaveBanco() == null) ? CVE_BANCO : session.getClaveBanco());
		//request.setAttribute(WEB_APP_REQUEST,Global.WEB_APPLICATION);
		//estadoCuentaBO.agregarDatosComunes(request, session);

			String valida = request.getParameter("valida");
			if (valida == null) {
				valida = validaToken(request, response, "EstadoCuentaHistoricoServlet");
			}
			if (valida != null && "1".equals(valida)) {
				final String cuentaSeleccionada = request.getParameter("cuentaTXT");
				final String cuentaOriginal = request.getParameter(CUENTA_TXT_REQUEST);
				String codigoCliente = StringUtils.EMPTY;
				String numeroSecuenciaDomicilio = StringUtils.EMPTY;
				TipoFormatoEstadoCuenta tipoFormato = TipoFormatoEstadoCuenta.XML;
				BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.TOKEN_VALIDO_SOL_EDO_HIST,obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),cuentaSeleccionada, "", BitaConstants.COD_ERR_PDF_XML,BitaConstants.CONCEPTO_TOKEN_VALIDO_SOL_EDO_HIST);

				final EstadoCuentaHistoricoBean beanBase = new EstadoCuentaHistoricoBean();
				final String periodos = request.getParameter("periodosSoliciar").substring(0,request.getParameter("periodosSoliciar").length()-1);
				final String[] periodosSeleccionados = periodos.split("\\|");
				codigoCliente = request.getParameter("hdCodCliente");
				numeroSecuenciaDomicilio = getFormParameter(request, "hdSeqDomicilio");
				tipoFormato = TipoFormatoEstadoCuenta.PDF;
				//[CSPH-00] Confirma solicitud de periodos historicos PDF
				BitaAdminBean bean = new BitaAdminBean();
				bean.setTipoOp(" ");
				bean.setIdTabla(" ");
				bean.setDato(cuentaSeleccionada);
				bean.setReferencia(100000);
				BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.CONFIRMACION_PERIODOS_HISTORICOS_PDF, bean, true);

				try {
					EIGlobal.mensajePorTrace(String.format("%s CuentaEnlaceSession [%s] --- CodigoCliente [%s]",LOG_TAG,session.getContractNumber(),codigoCliente),NivelLog.INFO);
					List <EstadoCuentaHistoricoBean> solicitudesECHB = new ArrayList<EstadoCuentaHistoricoBean>();
					beanBase.setCuentaEstadoCuenta(cuentaSeleccionada);
					beanBase.setFormato(tipoFormato);
					beanBase.setCliente(codigoCliente);
					beanBase.setNumeroSecuenciaDomicilio(numeroSecuenciaDomicilio);
					beanBase.setUsuarioEnlace(session.getUserID8());
					beanBase.setCuentaEnlace(session.getContractNumber());
					solicitudesECHB = estadoCuentaBO.registrarSolicitudes(periodosSeleccionados, beanBase);
					boolean folios=false;
					boolean ferror=false;
					String cuentaDestino=beanBase.getCuentaEstadoCuenta();
					List<String> periodoFolio=new ArrayList<String>();
					List<String> periodoError=new ArrayList<String>();
					StringBuffer cadenaFolio=new StringBuffer();
					StringBuffer cadenaError=new StringBuffer();

					for (EstadoCuentaHistoricoBean Bean : solicitudesECHB){
						if ( "DYA0001".equalsIgnoreCase(Bean.getCodigoRespuestaDY02()) && "100".equalsIgnoreCase(Bean.getCodigoRespuestaBD()) ) {
							folios=true;
							cadenaFolio.append(Bean.getFolioPeticion()).append(STR_SEPARADOR);
							periodoFolio.add(Bean.getPeriodo()+"|"+Bean.getFolioPeticion());
							EIGlobal.mensajePorTrace(String.format("%s se  Respuesta DY02 [%s] ferror [%s] folios [%s]"
									,LOG_TAG,Bean.getCodigoRespuestaDY02(),ferror,folios),
									NivelLog.DEBUG);
						} else if ("DYA0001".equalsIgnoreCase(Bean.getCodigoRespuestaDY02())) {
							ferror=true;
							cadenaError.append("<tr class=\"textabdatcla\"><td>").append(Bean.getPeriodo()).append(STR_CADENA_HTML).append(Bean.getCodigoRespuestaBD()).append(STR_CADENA_HTML).append(Bean.getDescripcionRespuestaBD()).append("</td></tr>");
							periodoError.add(Bean.getPeriodo()+"|"+Bean.getCodigoRespuestaBD()+"|"+Bean.getDescripcionRespuestaBD());
							EIGlobal.mensajePorTrace(String.format("%s se  Respuesta DY02 [%s] ferror [%s] folios [%s]"
									,LOG_TAG,Bean.getCodigoRespuestaDY02(),ferror,folios),
									NivelLog.DEBUG);
						} else {
							ferror=true;
							cadenaError.append("<tr class=\"textabdatcla\"><td>").append(Bean.getPeriodo()).append(STR_CADENA_HTML).append(Bean.getCodigoRespuestaDY02()).append(STR_CADENA_HTML).append(Bean.getDescripcionRespuestaDY02()).append("</td></tr>");
							periodoError.add(Bean.getPeriodo()+"|"+Bean.getCodigoRespuestaDY02()+"|"+Bean.getDescripcionRespuestaDY02());
							EIGlobal.mensajePorTrace(String.format("%s se  Respuesta DY02 [%s] ferror [%s] folios [%s]"
									,LOG_TAG,Bean.getCodigoRespuestaDY02(),ferror,folios),
									NivelLog.DEBUG);
						}
						// [DEDA] Se realiza guardado en bitacora de operaciones.
						BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.SOLICITUD_ESTADOS_HISTORICOS,obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),Bean.getCuentaEstadoCuenta(), "", BitaConstants.COD_ERR_PDF_XML,BitaConstants.CONCEPTO_SOL_EDO_HIST);
						int folioPeticion=Bean.getFolioPeticion();

						/** [DEDA-03] Finaliza solicitud de periodos historicos de Estados de Cuenta - PDF **/
						BitaAdminBean bean03 = new BitaAdminBean();
						bean03.setTipoOp("I");
						bean03.setIdTabla(Integer.toString(folioPeticion));
						bean03.setCampo(CAMPO_FIN);
						bean03.setTabla(TABLA_FIN);
						bean03.setServTransTux(CODIGO_SERVICIO_TUX_FINALIZA);
						bean03.setDatoNvo(Integer.toString(folioPeticion));
						bean03.setReferencia(100000);
						BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.FINALIZA_SOLICITUD_PERIODOS_HISTORICOS_PDF, bean03, false);

					}
					if (folios && cadenaFolio.length()>1) {
						cadenaFolio.replace(cadenaFolio.length()-1,cadenaFolio.length()," ");
						mensaje = (periodoFolio.size()==1)
								? String.format("Se ha registrado la solicitud de Estado de Cuenta de Periodos Hist&oacute;ricos. Folio: <b>%s.</b> Utilice el n&uacute;mero de folio para consultar el estatus de la disposici&oacute;n en el m&oacute;dulo \"Estados de Cuenta\" &gt; Solicitud de periodos Hist&oacute;ricos formato PDF &gt; Consulta\" <br>",cadenaFolio.toString())
								: String.format("Se han registrado las solicitudes de Estado de Cuenta de Periodos Hist&oacute;ricos. Folios:<b>%s.</b> Utilice los n&uacute;meros de folio para consultar el estatus de la disposici&oacute;n en el m&oacute;dulo \"Estados de Cuenta\" &gt; Solicitud de periodos Hist&oacute;ricos formato PDF &gt; Consulta\" <br>",cadenaFolio.toString());
						estadoCuentaBO.notifica(request,session,cuentaDestino,periodoFolio,periodoError,request.getParameter(TIPO_OP));
					}else {
						mensaje=" ";
					}
					if (ferror) {
						mensajeError=String.format("<br><table><tr class=\"tabdatfonazu\"><td class=\"tittabdat\">Periodo</td><td class=\"tittabdat\">Error</td><td class=\"tittabdat\">Detalle</td></tr> %s </table><br>",cadenaError.toString());
					}else {
						mensajeError=" ";
					}


					mensajeFinal.append(mensaje).append(mensajeError);
					despliegaPaginaConMensaje(mensajeFinal.toString(), STR_ESTADO_CUENTA,STR_POSICION_MENU,ARCHIVO_AYUDA, request, response, String.format("%s&Cuentas=%s", URL_INICIO, cuentaOriginal));
					return;
				} catch (BusinessException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
					mensaje = DY02DAO.CODIGOS_OPERACION.get(e.getMessage());
					if(mensaje == null || mensaje.length() == 0){
						mensaje = "Ocurrio un error inesperado al realizar la operaci&oacute;n";
					}
					despliegaPaginaConMensaje(String.format("<br>%s<br>", mensaje), STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
					return;
				}
			}
		}

	/**
	 * Metodo encargado de realizar las operaciones de validacion de token
	 * @param request : request Contiene la informacion de la peticion al servlet
	 * @param response : response Contiene la informacion de la respuesta del servlet
	 * @param servletName : servletName Nombre del servlet con el cual se esta realizando la peticion
	 * @return Validacion del token, cadena vacia en caso de validacion pendiente
	 * @throws IOException : En caso de un error con la operacion
	 */
	private  String validaToken(HttpServletRequest request, HttpServletResponse response, String servletName)
			throws IOException{
		final HttpSession ses = request.getSession();
		final BaseResource session = (BaseResource) ses.getAttribute(SESSION_NAME);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);
		//CSA-Vulnerabilidad  Token Bloqueado-Inicio
        int estatusToken = obtenerEstatusToken(session.getToken());

		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 1 && solVal) {
			EIGlobal.mensajePorTrace( "El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion",
					NivelLog.DEBUG);

			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request,servletName);
			ValidaOTP.validaOTP(request, response,IEnlace.VALIDA_OTP_CONFIRM);
		}else if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 2 && solVal){                        
        	cierraSesionTokenBloqueado(session,request,response);
		}
		//CSA-Vulnerabilidad  Token Bloqueado-Fin
		else {
			return "1";
		}
		return "";
	}



	/**
	 * Metodo que valida si la cuenta es correcta
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : exception lanzada
	 * @throws IOException : exception lanzada
	 */
	private void validarCambioCuenta(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EIGlobal.mensajePorTrace(String.format("%s Entrando validarCambioCuenta", LOG_TAG), NivelLog.INFO);
		final String cuentaSeleccionadaTMP = request.getParameter(CAMBIO_CTATXT_REQUEST);
		final String cuentas =  request.getParameter(CUENTA_TXT_REQUEST);
		boolean ocurrioUnError = false;

		EIGlobal.mensajePorTrace(String.format(">>>>>>>>>>>>>>>>>>>>>>Cuenta a validar:" +  cuentaSeleccionadaTMP),
				NivelLog.INFO);
		request.setAttribute("cuentaFormatoOriginal", request.getParameter(CUENTA_TXT_REQUEST));
		request.setAttribute(CAMBIO_CTATXT_REQUEST, request.getParameter(CAMBIO_CTATXT_REQUEST));
		String error = "";
		try {
			if(!estadoCuentaBO.validaCuenta(cuentaSeleccionadaTMP, request)) {
				EIGlobal.mensajePorTrace(String.format(">>>>>>>>>>>>>>>> La cuenta es incorrecta"),
						NivelLog.INFO);
				request.setAttribute(ERROR_REQUEST, "ERRORIN01");
			}
		} catch (ValidationException e) {
			error = e.getMessage();
			ocurrioUnError = true;
		}
		if(ocurrioUnError) {
			EIGlobal.mensajePorTrace(String.format(">>>>>>>>>>>>>>>> La cuenta es incorrecta error:" + error),
					NivelLog.INFO);
			request.setAttribute(ERROR_REQUEST, "ERRORIN00");
			request.setAttribute("MENSAJEERROR", error);
		}
		mostrarPantallaPeriodosPDF(request, response, cuentas);
	}

	/**
	 * Ejecuta el webservice para consultar si ya se genero el estado de cuenta PDF
	 * @param request : request
	 * @param response : response
	 * @throws IOException :  IOException
	 * @throws ServletException : ServletException
	 */
	private void validarPeriodosWebservice(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		EIGlobal.mensajePorTrace(String.format("%s %s",LOG_TAG,"::::: Inicia Validacion del WS :::::"),NivelLog.DEBUG);
		HttpSession httpSession = request.getSession();

		boolean existeSolicitudPrevia = false;
		String tipoedc= "001",pais = "MX", numcliente = "", numcuenta  = "", foliood = "",
		       periodoNumero="", branch = " ", idProcesar = " ", rfc = "", tipo = "00000001";

		/** [DEDA-02] Selecciona periodos historicos PDF **/
		BitaAdminBean bean = new BitaAdminBean();
		bean.setTipoOp(" ");
		bean.setIdTabla(" ");
		bean.setReferencia(100000);
		BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.SELECCIONA_PERIODOS_HISTORICOS_PDF, bean, false);
		ServletOutputStream out = response.getOutputStream();

		try {
			EIGlobal.mensajePorTrace(String.format("%s cadena [%s]",LOG_TAG,request.getParameter("cadena")), NivelLog.INFO);
			String[] cadena = (request.getParameter("cadena")!=null) ? request.getParameter("cadena").toString().split("@") : new String[0];
			EIGlobal.mensajePorTrace(String.format("%s Cadena=%s Long=",LOG_TAG,cadena,cadena.length),NivelLog.INFO);
			if (cadena.length > 0 && cadena.length==6) {
				foliood = cadena[1];
				tipoedc = cadena[2];
				periodoNumero = cadena[3];
				numcuenta = cadena[4];
				numcliente = cadena[5];
				// Se valida si ya existe el periodo consultado previamente en la tabla historica.

				existeSolicitudPrevia = estadoCuentaBO.existeSolicitudPrevia(numcuenta, periodoNumero);
				// Si ya existe la solicitud previa, termina flujo y se
				// envia el correspondiente codigo que sera interpretado en
				// el jsp SeleccionPeriodosPDFECH.
				if (existeSolicitudPrevia) {
					response.setContentType(CONTENT_TYPE);
					EIGlobal.mensajePorTrace(String.format("%s Se retorna codigo 501 a jsp ya que existe una solicitud previamente realizada",LOG_TAG),NivelLog.DEBUG);
					out.write(COD_SOLICITUD_PREVIA.getBytes());
					out.flush();
					return;
				}

				EIGlobal.mensajePorTrace(String.format("%s Inicia llamado a WebService",LOG_TAG), NivelLog.DEBUG);
				String[] respuestaWS=estadoCuentaBO.invocaWebServicePDF(tipoedc, branch, foliood, idProcesar, numcliente, numcuenta, pais, periodoNumero, rfc, tipo);
				int respWSint=Integer.valueOf(respuestaWS[0]);

				if (respWSint==WS_PERIODO_EDC_EXISTE || respWSint==WS_PERIODO_EDC_NOEXISTE) {
					response.setContentType(CONTENT_TYPE);
					EIGlobal.mensajePorTrace(String.format("%s RespuestaWS [%s]",LOG_TAG,respuestaWS[0]),NivelLog.INFO);
				    out.write(respuestaWS[0].getBytes());
		            out.flush();
				}
			}
			else {
				EIGlobal.mensajePorTrace(String.format("%sERROR en parametros enviados al Servlet.",LOG_TAG), NivelLog.DEBUG);
				despliegaPaginaConMensaje("<br>ERROR en parametros enviados al Servlet.<br>",STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}
		} catch (BusinessException e) {
			EIGlobal.mensajePorTrace(String.format("%s Ocurrio un error [%s]",LOG_TAG,e.getMessage()), NivelLog.DEBUG);
			response.setContentType(CONTENT_TYPE);
		    out.write((AJAX_ERROR_GENERICO+"ERROR: "+e.toString()).getBytes());
            out.flush();
		} catch (WebServiceException e) {
			response.setContentType(CONTENT_TYPE);
			EIGlobal.mensajePorTrace(String.format("%s Se retorna codigo 601 a jsp debido a que el web service definido no responde", LOG_TAG), NivelLog.DEBUG);
			out.write(COD_SERVICIO_NO_DISPONIBLE.getBytes());
			out.flush();
		}

	}

	/**
	 * Metodo para consultar las cuentas y tarjetas a presentar en el combo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void ctasTarjetas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ConfigModEdoCtaTDCBO consultaCtasTarjetas = new ConfigModEdoCtaTDCBO(); 
		List<ConfEdosCtaArchivoBean> cuentasTarjetas = new ArrayList<ConfEdosCtaArchivoBean>();
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		//ConCtasPdfXmlServlet pdfXml = new ConCtasPdfXmlServlet();
		boolean cambioContrato =ConfigMasPorTCAux.cambiaContrato(request.getSession(),  session.getContractNumber());
		//boolean cambioContrato = pdfXml.cambioContrato(request);
		
		String opcion = request.getAttribute(OPCION) != null && !"".equals(request.getAttribute(OPCION)) 
			? request.getAttribute(OPCION).toString() : ""; 
		
		EIGlobal.mensajePorTrace(this + "-----> opcion: " + opcion, EIGlobal.NivelLog.INFO);
		
		if (("".equals(opcion) && request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null)
				|| cambioContrato) {
			cuentasTarjetas = consultaCtasTarjetas.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()), session.getContractNumber(), session.getUserProfile());
			EIGlobal.mensajePorTrace(EdoCtaConstantes.LISTA_CUENTAS+"<<---Asigna Lista Cuentas Longitud de la lista: "+ cuentasTarjetas.size(),
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
			request.getSession().setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
		} else {
			ConfigMasPorTCAux.filtrarDatos(request, response);			
		}
		
		cuentasTarjetas = (List<ConfEdosCtaArchivoBean>) request.getAttribute(EdoCtaConstantes.LISTA_CUENTAS);
		
		EIGlobal.mensajePorTrace("Recuperando Lista Cuentas--> "+ cuentasTarjetas.size(),EIGlobal.NivelLog.DEBUG);
		
		if( cuentasTarjetas.size() > 25 ){
			List<ConfEdosCtaArchivoBean> cuentasTarjetasAux = new ArrayList<ConfEdosCtaArchivoBean>();
			for( int i = 0 ; i < 25 ; i++ ){
				cuentasTarjetasAux.add( cuentasTarjetas.get(i) );
			}
			cuentasTarjetas = cuentasTarjetasAux;
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentasTarjetas);
			request.setAttribute("msgOption", "La consulta gener&oacute; m&aacute;s de 25 resultados, debe ser m&aacute;s espec&iacute;fico en el filtro");
			request.setAttribute(MSG_EXCEDE, "-1");
		}				
	}

	/**
     * Metodo para setear valores en vacio al request
     * @since 05/03/2015
     * @author FSW-Indra
     * @param request peticion
     */
    public void setAtributos(HttpServletRequest request){
    	
    	request.setAttribute(MSG_EXCEDE, request.getParameter(MSG_EXCEDE) != null ? request.getParameter(MSG_EXCEDE) : "");
    	
    }
    

	/**
	 * Metodo para exportar las cuentas a un archivo
	 * @param request servlet request
	 * @param response servlet response
	 */
	public void exportar(HttpServletRequest request, HttpServletResponse response) {	
		EIGlobal.mensajePorTrace(String.format("%s Inicia Metodo: EXPORTAR",LOG_TAG),NivelLog.INFO);
		String flujo = request.getParameter(TIPO_OP);
		List<?> listCuentasTarjetas = null;
		if( "EXPCTA".equals( flujo )  ){
			listCuentasTarjetas = (ArrayList<?>)request.getSession().getAttribute("cuentas");
		} else {
			listCuentasTarjetas = (ArrayList<?>)request.getSession().getAttribute(TARJETAS);
		}
		
		EIGlobal.mensajePorTrace(String.format("%s Se obtienen [%s] cuentas de la sesion.",LOG_TAG,(listCuentasTarjetas!=null)?listCuentasTarjetas.size():"0"), NivelLog.INFO);						
		
		try {
			StringBuilder tramas = new StringBuilder();		
			for(Object trama : listCuentasTarjetas ){
				tramas.append(trama.toString());
				tramas.append("\n");
			}		
			response.setContentType(CONTENT_TYPE);
			response.setHeader(HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION);
			EIGlobal.mensajePorTrace(String.format("%s Se configuran los Headers :::::\n" +
					"ContentType [%s]\n" +
					"Header[%s,%s]",LOG_TAG,CONTENT_TYPE,HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION),NivelLog.INFO);
			
			EIGlobal.mensajePorTrace(String.format("%s Crea objetos STREAM",LOG_TAG),NivelLog.INFO);			
			final InputStream input = new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
			final OutputStream os = response.getOutputStream();	
			
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			EIGlobal.mensajePorTrace(String.format("%s Comienza Descarga",LOG_TAG),NivelLog.INFO);
			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
				os.flush();
				EIGlobal.mensajePorTrace(String.format("%sFlushing [%s] bytes Readed to outputStream.",
						LOG_TAG,read), NivelLog.DEBUG);
			}
			EIGlobal.mensajePorTrace(String.format("%s Cierra objetos STREAM",LOG_TAG),NivelLog.INFO);
			os.close();
			input.close();
			EIGlobal.mensajePorTrace(String.format("%s Termina Descarga",LOG_TAG),NivelLog.INFO);
		} catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} 
	}
	
}