/*Banco Santander Mexicano
  Clase CBitacora Contiene las funciones para la presentacion de pantalla de consulta de bitacora
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 08/03/2001 - Permitir la consulta de bitacora de todas las ctas de captacion
 */

package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Convertidor;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.io.*;

//02/ENE/07
import java.sql.SQLException;
public class CBitacora extends BaseServlet
{
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
     boolean sesionvalida = SesionValida(req, res);
	 if(!sesionvalida) {
		 return;
	 }
	 HttpSession sess = req.getSession();
     BaseResource session = (BaseResource) sess.getAttribute("session");
        //if (sesionvalida&&session.getFacCBitacora().trim().length()!=0){
		//if (session.getFacultad(session.FAC_CONSULTA_BITACORA).trim().length() != 0) {
		if (session.getFacultad(session.FAC_CONSULTA_BITACORA)) {
			EIGlobal.mensajePorTrace( "***CBitacora.class  &Inicia clase&", EIGlobal.NivelLog.DEBUG);

			String menu_ = session.getstrMenu();

			if(menu_ ==null) {
				//System.out.println("\n*\n*\n*Error menu nulo");
				EIGlobal.mensajePorTrace( "Error menu nulo", EIGlobal.NivelLog.ERROR);
				menu_ = "";
			}

			String m_principal ="";
			m_principal = session.getstrMenu();
			if(m_principal==null)
				m_principal = "";

			req.setAttribute("MenuPrincipal", m_principal );
		    req.setAttribute("Fecha", ObtenFecha());
	       	req.setAttribute("ContUser",ObtenContUser(req));
			req.setAttribute("diasInhabiles",diasInhabilesAnt());

  		    //***********************************************
			//modificaci�n para integraci�n pva 07/03/2002
            //***********************************************
            /*String[][] arrayCuentas = ContCtasRelacpara_mis_transferencias(session.getContractNumber());
            String Cuentas        = "";
            String tipo_cuenta    = "";
			String tipo_relacion  = "";
			String indice_cuenta  = "";
			String cuentaAmbiente = "";
			String titular        = "";
			boolean tieneFacultad = false;

            for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++){
				tipo_cuenta    = arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR); // Primeros dos caract�res ...
				cuentaAmbiente = FormateaContrato( arrayCuentas[indice][1].trim() ); // n�mero de cuenta
				tipo_relacion  = arrayCuentas[indice][2]; // Propias o de terceros ...
				indice_cuenta  = arrayCuentas[indice][3]; // personal, empresarial, chequera etc...
				titular        = arrayCuentas[indice][4].trim(); // propietario de la cuenta
				tieneFacultad = false;

				if ( !indice_cuenta.equals("5") ) // tarjetas de cr�dito NO
				{
					if ( tipo_relacion.equals("P") ) // Propias
					{
						tieneFacultad = !existeFacultadCuenta(cuentaAmbiente,"CONSMOVTOS","-");
					}
					else if ( tipo_relacion.equals("T") ) // de Terceros
					{
						tieneFacultad = existeFacultadCuenta(cuentaAmbiente,"CONSMOVTOTER","+");
					}

					if ( tieneFacultad )
					{
						Cuentas += "<option value = " + cuentaAmbiente + ">" + cuentaAmbiente + " " + titular + "</option>\n";
						EIGlobal.mensajePorTrace( "***CBitacora.class  &se agrega cuenta:" + cuentaAmbiente + "&", EIGlobal.NivelLog.INFO);
					} // if ( tienefacultad )
				} // if ( indice_cuenta )
			} // for ( indice ) */
            //***********************************************
            String[][] arrayUsuarios = TraeUsuarios(req);
            String Usuarios = "",NoUsuario="";
            long UserAux	= 0;
            long numero1	= 0;
            long numero2	= 0;
            //Modificacion para el soporte de 8 digitos. 11/03/2004 cgc.
            Convertidor con=new Convertidor();  //Clase para la conversion de 7 a 8 11/03/2004 cgc
            for(int z=1;z<=Integer.parseInt(arrayUsuarios[0][0].trim());z++)
                arrayUsuarios[z][1] = con.cliente_7To8(arrayUsuarios[z][1]);
            //fin de la modificacion del cliente de 7to8 cgc 11/03/2004.
            for(int i=1;i<=Integer.parseInt(arrayUsuarios[0][0].trim());i++){
              for(int j=i;j<=Integer.parseInt(arrayUsuarios[0][0].trim());j++){
                 numero1 = new Long(arrayUsuarios[i][1]).longValue();
                 numero2 = new Long(arrayUsuarios[j][1]).longValue();
                 if(numero1 > numero2){
                    Usuarios = arrayUsuarios[i][2];
                    NoUsuario = arrayUsuarios[i][1];
                    arrayUsuarios[i][2]=arrayUsuarios[j][2];
                    arrayUsuarios[i][1]=arrayUsuarios[j][1];
                    arrayUsuarios[j][2]=Usuarios;
                    arrayUsuarios[j][1]=NoUsuario;
                 }

              }
            }
            Usuarios="";
            StringBuffer sbUsuarios = new StringBuffer();
			EIGlobal.mensajePorTrace( "***CBitacora.class  &n�mero de usuarios : "+ arrayUsuarios[0][0]+"&", EIGlobal.NivelLog.INFO);
            for (int indice=1;indice<=Integer.parseInt(arrayUsuarios[0][0].trim());indice++){
              //Usuarios += "<OPTION value = " + arrayUsuarios[indice][1] + ">" + arrayUsuarios[indice][1] + " " + arrayUsuarios[indice][2] + "\n"; //modificacion para la migraci�n del cliente 7to8
              sbUsuarios.append("<OPTION value = ");
              sbUsuarios.append((arrayUsuarios[indice][1]));
              sbUsuarios.append(">");
              sbUsuarios.append(arrayUsuarios[indice][1]);
              sbUsuarios.append(" ");
              sbUsuarios.append(arrayUsuarios[indice][2]);
              sbUsuarios.append("\n");
            }
            Usuarios = sbUsuarios.toString();

            //POST ---------------
            sess.setAttribute("listaUsuarios", arrayUsuarios);
            //POST -------------

            String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
                   datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
                   datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";

            req.setAttribute("Bitfechas",datesrvr);
            //modificaci�n para integraci�n req.setAttribute("Cuentas",Cuentas);
            req.setAttribute("Usuarios",Usuarios);
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Consulta de Bit&aacute;cora","Consultas &gt; Bit&aacute;cora", "s25290h",req));
			EIGlobal.mensajePorTrace( "***CBitacora.class  &Desplegando datos en pantalla&", EIGlobal.NivelLog.DEBUG);
		    //***********************************************
			//modificaci�n para integraci�n pva 07/03/2002
            //***********************************************
			session.setModuloConsultar(IEnlace.MConsulta_Bitacora);
            //***********************************************
               req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
               //TODO  BIT CU1501
               /* 09/01/07
   			 * VSWF-BMB-I
   			 */
               if (Global.USAR_BITACORAS.trim().equals("ON")){
	               try{
			           BitaHelper bh = new BitaHelperImpl(req, session, sess);
			           if(req.getParameter(BitaConstants.FLUJO)!=null){
			        	   bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			           }else{
			        	   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			           }
			   		   BitaTransacBean bt = new BitaTransacBean();
			   		   bt = (BitaTransacBean)bh.llenarBean(bt);
			   		   bt.setNumBit(BitaConstants.EC_BITACORA_ENTRA);
			   		   if(session.getContractNumber()!=null){
			   			   bt.setContrato(session.getContractNumber());
			   		   }
			   		   BitaHandler.getInstance().insertBitaTransac(bt);
	               }catch(SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
               }
   		    /*
			 * VSWF-BMB-F
			 */

            evalTemplate(IEnlace.C_BITACORA_TMPL, req, res);
        }else if(sesionvalida){
			EIGlobal.mensajePorTrace( "***CBitacora.class  &Sesi�n o facultades no v�lidas. Finaliza clase&", EIGlobal.NivelLog.ERROR);
            req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute( "Encabezado", CreaEncabezado( "Consulta de Bit&aacute;cora","Consultas &gt; Bit&aacute;cora","s25290h",req) );
            evalTemplate(IEnlace.ERROR_TMPL, req, res );
        }
    }

	/*----------------------------------------------------------------------------------------------------*/

	String FormateaContrato (String ctr_tmp){
		int i			= 0;
		String temporal	= "";
		String caracter	= "";

		for (i = 0 ; i < ctr_tmp.length() ; i++){
			caracter = ctr_tmp.substring(i, i+1);
			if (caracter.equals("-")){
				// no se toma en cuenta el guion
			}else{
				temporal += caracter ;
			}
		}
		return temporal;
	}

}