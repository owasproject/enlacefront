package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.lang.String;
import java.io.*;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.rmi.*;
import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TI_Cuenta;
import mx.altec.enlace.bo.bc_CuentaProgFechas;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;
import java.text.SimpleDateFormat;

public class bcProgFechas extends BaseServlet
{

	public BaseResource session;
	public ServletContext contexto;
	public String[] HorasPermit = { "08","09","10","11","12","13","14","15","16","17","18","19" };
	public String[] MinutPermit = { "00","30" };
	public String   Xerr = "0";

	EIGlobal EnlaceGlobal=new EIGlobal(session,contexto,this); //amt


    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}


    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}


	/************** --- (Este m�todo se ejecuta en cuanto se invoca al servlet) ********************
	*
	* Nombre: defaultAction
	*
	*
	************************************************************************************************/
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = null;
		String mensError [] = new String[1];
		mensError[0]="";





		try
		{

			session= (BaseResource) sess.getAttribute("session");
		}
		catch(Exception e)
		{
			despliegaPaginaError("Error servicio no disponible por el momento","Base Cero ","Progr de Operaciones","s25010h",req, res );   // s26170h
		}



		EIGlobal.mensajePorTrace(">bcProgFechas--> ENTRANDO A defaultAction() = "+(String)req.getParameter("Accion"), EIGlobal.NivelLog.INFO);

		String parAccion   = (String)req.getParameter("Accion");   if(parAccion == null)   parAccion    = "";
		String parTrama    = (String)req.getParameter("Trama");    if(parTrama == null)    parTrama     = "";
		String parCuentas  = (String)req.getParameter("Cuentas");  if(parCuentas == null)  parCuentas   = "";
		String parTramaOld = (String)req.getParameter("tramaOld"); if(parTramaOld == null) parTramaOld  = "";

		int pant = 0;




		EIGlobal.mensajePorTrace("<bcProgFechas-->defaultAction: parTrama: " + parTrama + "\n parCuentas:" + parCuentas + "\n parTramaOld:"+ parTramaOld, EIGlobal.NivelLog.INFO);




		if(!SesionValida(req,res))
		{
					despliegaPaginaError("Error servicio no disponible por el momento","Base Cero ","Progr de Operaciones","s33010h",req, res );
					return;
		}


		String varFechaHoy = "  //Fecha De Hoy"
				+ "\n  dia=new Array("+formateaFecha("DIA")+","+formateaFecha("DIA")+");"
				+ "\n  mes=new Array("+formateaFecha("MES")+","+formateaFecha("MES")+");"
				+ "\n  anio=new Array("+formateaFecha("ANIO")+","+formateaFecha("ANIO")+");"
				+ "\n\n  //Fechas Seleccionadas"
				+ "\n  dia1=new Array("+formateaFecha("DIA")+","+formateaFecha("DIA")+");"
				+ "\n  mes1=new Array("+formateaFecha("MES")+","+formateaFecha("MES")+");"
				+ "\n  anio1=new Array("+formateaFecha("ANIO")+","+formateaFecha("ANIO")+");"
				+ "\n  Fecha=new Array('"+formateaFecha("FECHA")+"','"+formateaFecha("FECHA")+"');";

		String strInhabiles = armaDiasInhabilesJS();
		String diasInhabiles = diasInhabilesAnt();
        String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

        if (diasInhabiles.trim().length()==0)
		{
			mensError[0] = "X-PServicio no disponible por el momento";
			pant = 4;

		}


		// --- Modificacion para integraci�n. de las cuentas
		session.setModuloConsultar(IEnlace.MMant_estruc_B0);


		if(parAccion.equals("")) { if (!session.getFacultad(session.FAC_BASECONEST_BC)) {
//			TODO: Bitacora inicia flujo
			/*
			  * VSWF ARR -I
			  */
			 				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			 				try{
			 				BitaHelper bh = new BitaHelperImpl(req, session, sess);
			 				if (req.getParameter(BitaConstants.FLUJO)!=null){
			 				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			 				  }else{
			 				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			 				  }
			 				BitaTransacBean bt = new BitaTransacBean();
			 				bt = (BitaTransacBean)bh.llenarBean(bt);
			 				bt.setNumBit(BitaConstants.ER_ESQ_BASE0_PROG_OP_REG_ENTRA);
			 				bt.setContrato(session.getContractNumber());

			 				BitaHandler.getInstance().insertBitaTransac(bt);
			 				}catch (SQLException ex){
			 					ex.printStackTrace();
			 				}catch(Exception e){

			 					e.printStackTrace();
			 				}
			 				}
			 	    		/*
			 	    		 * VSWF ARR -F
		    		 */

			parCuentas = consultaArbol(req, mensError); req.setAttribute("Trama", parTrama);}  else {mensError[0] = "Usted no tiene facultad para consultar programaciones."; pant= 3;}}



		if(parAccion.equals("REGRESA")) { if (!session.getFacultad(session.FAC_BASECONEST_BC)) {parCuentas = consultaArbol(req, mensError); req.setAttribute("Trama", parTrama);}  else {mensError[0] = "Usted no tiene facultad para consultar programaciones."; pant= 3;}}

		/*Mas lineas pero mas claro*/
		if(parAccion.equals("AGREGA"))
		{
			if (session.getFacultad(session.FAC_BASPROGRA_BC))
			{

				parCuentas = agregaCuenta(parTrama, parCuentas, req, mensError);
				req.setAttribute("Trama", parTrama);

			}
			else
			{

				mensError[0] = "Usted no tiene facultad para agregar cuentas."; pant= 3;

			}

		}
		if(parAccion.equals("ELIMINA")) { if (session.getFacultad(session.FAC_BASBORPRO_BC)){parCuentas = eliminaCuenta(parTrama, parCuentas, req, mensError); req.setAttribute("Trama", parTrama);} else {mensError[0] = "Usted no tiene facultad para eliminar programaciones."; pant= 3;}}
		if(parAccion.equals("ALTA_ARBOL")) { if (session.getFacultad(session.FAC_BASENVEST_BC)) {altaArbol(parCuentas, req, mensError); req.setAttribute("Trama", parTrama);} else mensError[0] = "Usted no tiene facultad para enviar estructuras.";}

		if(parAccion.equals("MODIFICA")) {if(session.getFacultad(session.FacCapDisper)) {parTrama = tramaCuenta(parTrama, parCuentas); pant=1; req.setAttribute("Trama", parTrama);} else mensError[0] = "Usted no tiene facultad para capturar datos"; pant= 1;}
		if(parAccion.equals("MODIFICA_MODIFICA"))
		{
			Xerr = "0";
			parCuentas = tramaModificaCuenta(parTrama, parCuentas, parTramaOld, req, mensError);

			if( Xerr.equals("0"))
				pant = 1;
			else
			 	pant = 4;

		}
		if(parAccion.equals("IMPORTA")) {if(session.getFacultad(session.FAC_BASIMPORT_BC)) {parCuentas = importaDatos(req, parCuentas, mensError);} else {mensError[0] = "Usted no tiene facultad para Importar programaciones."; pant= 3;}}



		req.setAttribute("diasInhabiles", strInhabiles);
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());

		req.setAttribute("Encabezado", CreaEncabezado("Registro de programaci&oacute;n de operaciones","Tesorer�a &gt; Esquema Base Cero &gt; Programaci&oacute;n de operaciones &gt; Registro","s33055h",req));


		req.setAttribute("Cuentas",parCuentas);
		req.setAttribute("Mensaje",mensError[0]);
		req.setAttribute("tramaOld",parTramaOld);
		req.setAttribute("DiasInhabiles",strInhabiles);
		req.setAttribute("Fecha",ObtenFecha());
		req.setAttribute("Movfechas",datesrvr);
		req.setAttribute("FechaAyer",formateaFecha("FECHA"));
		req.setAttribute("FechaPrimero","01/"+formateaFecha("MES2")+"/"+formateaFecha("ANIO"));
		req.setAttribute("VarFechaHoy",varFechaHoy);

		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));







		/*
		   Para estos casos no volver a realizar la consulta de cuentas,
		   solo mostrar lo que su trama regreso

		 */
		if ( !(parAccion.equals("MODIFICA_MODIFICA"))  || !(parAccion.equals("IMPORTA")) ||
		     !(parAccion.equals("REGRESA")) )
		{
			req.setAttribute("ComboMtoEst",comboCuentas(req, mensError));
		}








		switch(pant)
		{
			case 0: evalTemplate("/jsp/bcProgFechas.jsp",req,res); break;
			case 1: evalTemplate("/jsp/bcEditaFechas.jsp",req,res); break;
			case 3: despliegaMensaje(mensError[0],"Registro de programaci&oacute;n de operaciones","Tesorer�a &gt; Esquema Base Cero &gt; Programaci&oacute;n de operaciones &gt; Registro", req, res); break;
			case 4: despliegaPaginaError(mensError[0].substring(3),"Registro de programaci&oacute;n de operaciones","Tesorer�a &gt; Esquema Base Cero &gt; Programaci&oacute;n de operaciones &gt; Registro", req, res); break;
		}

		EIGlobal.mensajePorTrace("bcProgFechas--> SALIENDO DE defaultAction(), pant:"+pant+ " Xerr:"+ Xerr, EIGlobal.NivelLog.INFO);

	}




	/************************** Agrega una cuenta a la trama ****************************************
	*
	* Nombre: agregaCuenta
	*
	*
	************************************************************************************************/
	private String agregaCuenta(String trama, String tramaCuentas, HttpServletRequest req, String [] mensError)
	{

		EIGlobal.mensajePorTrace("<bcProgFechas-->agregaCuenta> .Entrando trama= " + trama + "\ntramaCuentas= " + tramaCuentas, EIGlobal.NivelLog.INFO);


		/* LASR --> TRUE si la operacion fue programada exitosamente */
		if (!altaArbol(trama, req, mensError))
		{

			return tramaCuentas;
		}

		else
		{

			return consultaArbol(req, mensError);
			//return trama;

		}



	}


	/************************** Elimina una cuenta de la trama de cuentas **************************
	*
	* Nombre: eliminaCuenta
	*
	*
	************************************************************************************************/
	private String eliminaCuenta(String cuentaFecha, String tramaCuentas, HttpServletRequest req, String [] mensError){
		EIGlobal.mensajePorTrace("<DEBUG Mayen> ..........  eliminaCuenta, \ncuentaFecha= " + cuentaFecha + "\ntramaCuentas=\n" + tramaCuentas, EIGlobal.NivelLog.INFO); //Salida
		Vector cuentas;
		bc_CuentaProgFechas cta;
		boolean consistente;
		String fechaIni, fechaFin, fechaIni1, fechaFin1;
		String cuenta, retorno, creaTrama="";
		String tramaArchivo;
		int a, index;

		cuenta = cuentaFecha.substring(0, cuentaFecha.indexOf("|"));
		cuentas = creaCuentas(tramaCuentas, req, mensError);
		fechaIni = cuentaFecha.substring(cuentaFecha.indexOf("|")+6,cuentaFecha.indexOf("|")+16);
		fechaFin = cuentaFecha.substring(cuentaFecha.indexOf("|")+21);
		fechaIni1 = quitaFormatoFecha(fechaIni);
		fechaFin1 = quitaFormatoFecha(fechaFin);

		for (a=0;a<cuentas.size();a++){
			cta = (bc_CuentaProgFechas)cuentas.get(a);

			if(cta.getNumCta().equals(cuenta) && cta.getVigencia1().equals(fechaIni) && cta.getVigencia2().equals(fechaFin)){
				retorno = enviaTuxedo("BORRAR", cuenta, req, fechaIni1, fechaFin1, mensError);

				if (retorno.substring(1,4) != "tmp"){
					if (retorno.substring(16,24).equals("BASE0000")){
						//cuentas.remove(cta);
						cuentas.remove(a);
						creaTrama = creaTrama(cuentas);
						mensError[0] = "La cuenta ha sido eliminada.";
					}else{
						creaTrama = creaTrama(cuentas);
						mensError[0] = "La cuenta no pudo ser eliminada.";
					}
				}else{
					creaTrama = creaTrama(cuentas);
					mensError[0] = "Favor de repetir la operaci&oacute;n";
				}
			}else {
					creaTrama = creaTrama(cuentas);
			}
		}
		return creaTrama;
	}

	// --- Indica si una cuenta se encuentra dentro de una trama -----------------------
	//							 partrama       parCuenta
	private boolean existeCuenta(String cuenta, String tramaCuentas)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Entro a existeCuenta(),\n cuenta:\n" + cuenta + "\ntramaCuentas:\n" + tramaCuentas, EIGlobal.NivelLog.INFO);
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		boolean existe = false;
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) {existe = true; break;}
				}
			}
		catch(Exception e)
			{EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error en existeCuenta(): " + e.toString(), EIGlobal.NivelLog.INFO);}
		finally
			{return existe;}
		}

	// --- Ordena las cuentas seg�n el arbol -------------------------------------------
	private String ordenaArbol(String tramaEntrada, HttpServletRequest req, String [] mensError)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> ..........  Llego a ordenaArbol en el Servlet, tramaEntrada=\n" + tramaEntrada, EIGlobal.NivelLog.INFO); //Salida
		String tramaSalida;
		Vector cuentas, ctasOrden;
		bc_CuentaProgFechas ctaAux;
		int a, b;

		b = 1;
		cuentas = creaCuentas(tramaEntrada, req, mensError);
		ctasOrden = new Vector();
		while(cuentas.size() > 0)
			{
			for(a=0;a<cuentas.size();a++)
				{
				ctaAux = ((bc_CuentaProgFechas)cuentas.get(a));
				if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
				}
			b++;
			}
		cuentas = ctasOrden;
		ctasOrden = new Vector();
		for(a=0;a<cuentas.size();a++)
			{
			ctaAux = ((bc_CuentaProgFechas)cuentas.get(a));
			if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
			}
		while(cuentas.size() > 0)
			{
			ctaAux = ((bc_CuentaProgFechas)cuentas.get(0));
			ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
			cuentas.remove(ctaAux);
			}
		cuentas = ctasOrden;
		tramaSalida = "";
		for(a=0;a<cuentas.size();a++) tramaSalida += ((bc_CuentaProgFechas)cuentas.get(a)).trama();
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... ordenaArbol, tramaSalida: \n" + tramaSalida, EIGlobal.NivelLog.INFO);
		return tramaSalida;
		}

	// --- Regresa un vector de cuentas a partir de una trama --------------------------
	public Vector creaCuentas(String tramaCuentas, HttpServletRequest req, String [] mensError)
		{
		//tramaCuentas = tramaCuentas.trim();
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a creaCuentas, tramaCuentas=*" + tramaCuentas + "*", EIGlobal.NivelLog.INFO);
		Vector cuentas = null;
		StringTokenizer token;
		bc_CuentaProgFechas ctaAux1, ctaAux2;
		int a, b;
		String valorToken;

		//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... creaCuentas1", EIGlobal.NivelLog.INFO);
		try
			{
			cuentas = new Vector();
			token = new StringTokenizer(tramaCuentas,"@");
			//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... creaCuentas2", EIGlobal.NivelLog.INFO);
			while(token.hasMoreTokens())
				{
				valorToken = token.nextToken().trim();
				cuentas.add(bc_CuentaProgFechas.creaCuentaFechas("@" + valorToken));
				//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... creaCuentas3, valorToken= \n*" + valorToken + "*", EIGlobal.NivelLog.INFO);
				}
			}
		catch(Exception e)
			{
			//EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error en creaCuentas(): " + e.toString(), EIGlobal.NivelLog.INFO);
			cuentas = new Vector();
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
		finally{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Saliendo de creaCuentas", EIGlobal.NivelLog.INFO);
			return cuentas;}
		}

	// --- Regresa una trama con el arbol ----------------------------------------------
	private String consultaArbol(HttpServletRequest req, String [] mensError)
	{
		String  retorno,
		        trama      = "",
		        tramaFin   = "",
		        aviso,
		        msgError,
		        cuentaHija = "",
		        desc,
		        token;

		String trama1 = "",
		       token1;

		String comboCuentas1 = comboCuentas1(req, mensError);

		int car;

		EIGlobal.mensajePorTrace("bcProgFechas> ...Entrando a consultaArbol, comboCuentas1=\n" + comboCuentas1, EIGlobal.NivelLog.INFO);

		/**/try
			{
				StringTokenizer tokens = new StringTokenizer(comboCuentas1,"@");

				token = tokens.nextToken();

				while(tokens.hasMoreTokens())
				{
					EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... consultaArbol, token:\n" + token, EIGlobal.NivelLog.INFO);

					car = token.indexOf("|");

					/*getronics mar-2005*/
					cuentaHija = R_eliminaSalto(token.substring(0,car) );
					desc = token.substring(car+1);


					/* se realiza una copia del archivo procesado por Tuxedo*/
					retorno = enviaTuxedo("CONSULTA", cuentaHija,req, "", "", mensError);

					EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... consultaArbol, retorno:\n" + retorno, EIGlobal.NivelLog.INFO);

					trama = leeArchivo(retorno);

					EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... consultaArbol, trama 1:\n**" + trama + "**", EIGlobal.NivelLog.INFO);
					aviso = trama.substring(0,trama.indexOf("@"));
					trama = trama.substring(trama.indexOf("@") + 1);
					EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... consultaArbol, trama 2:\n**" + trama + "**", EIGlobal.NivelLog.INFO);

					if(!aviso.equals("TRAMA_OK"))
					{

						/*LASR:Si regresara ya no mostraria los siguientes contratos
						return "";
						*/

					}
					else
					{
						// Agrega la descripcion a las cuentas
						StringTokenizer tokens1 = new StringTokenizer(trama,"@");

						while (tokens1.hasMoreTokens())
						{
							token1  = tokens1.nextToken(); // cuenta|descripcion
							trama1 += token1 + desc + "@";

						}

						tramaFin += trama1;
					}

					token = tokens.nextToken();
		    	}

		}
		catch (Exception e)
		{
			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... Error en consultaArbol(): " + e + "\n", EIGlobal.NivelLog.INFO);
		}
		finally
		{
			EIGlobal.mensajePorTrace("<DEBUG bcProgFechas> .......... consultaArbol, tramaFin=\n**" + tramaFin + "**", EIGlobal.NivelLog.INFO);
			//return tramaFin;
			return trama1;
		}

	}


	/****************************** elimina caracteres raros *************************************
	 *
	 * Nombre:--> eliminaSalto
	 * Descri:--> Elimina caracter '\n' al inicio de una cadena
	 * Autor :--> LASR
	 *
	 *********************************************************************************************/

	 private String R_eliminaSalto(String src)
	 {

		 EIGlobal.mensajePorTrace("<Entrando a bcConFechasProg-->R_eliminaSalto src["+ src +"]\n" , EIGlobal.NivelLog.INFO);

		 if(src.charAt(0) == '\n')
		 {

			return ( src.substring(1,src.length()) );

	 	 }
	 	 else
	 	 {
	 	 	return src;

	 	 }

 	}



	// --- Indica si una cuenta es hoja o padre ----------------------------------------
	public boolean esHoja(bc_CuentaProgFechas cuenta, Vector cuentas)
		{
		TI_Cuenta ctaActual;
		boolean hoja;

		hoja = true;
		for(int a=0;a<cuentas.size() && hoja;a++)
			{
			ctaActual = ((bc_CuentaProgFechas)cuentas.get(a)).getPadre();
			if(ctaActual != null) if(ctaActual.equals(cuenta)) hoja = false;
			}
		return hoja;
		}

	String formateaFecha(String tipo){
		String strFecha = "";
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);
		do{
			Cal.add(Calendar.DATE,-1);
		}while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);
		if(tipo.equals("FECHA")){
			if(Cal.get(Calendar.DATE) <= 9)
				strFecha += "0" + Cal.get(Calendar.DATE) + "/";
			else
				strFecha += Cal.get(Calendar.DATE) + "/";
			if(Cal.get(Calendar.MONTH)+1 <= 9)
				strFecha += "0" + (Cal.get(Calendar.MONTH)+1) + "/";
			else
				strFecha += (Cal.get(Calendar.MONTH)+1) + "/";
			strFecha += Cal.get(Calendar.YEAR);
		}
		if(tipo.equals("MES2")){
			if(Cal.get(Calendar.MONTH)+1 <= 9)
           		strFecha+="0" + (Cal.get(Calendar.MONTH)+1);
         	else
           		strFecha+=(Cal.get(Calendar.MONTH)+1);
	    }
		if(tipo.equals("DIA"))
			strFecha += Cal.get(Calendar.DATE);
		if(tipo.equals("MES"))
			strFecha += (Cal.get(Calendar.MONTH)+1);
		if(tipo.equals("ANIO"))
			strFecha += Cal.get(Calendar.YEAR);
		return strFecha;
    }

	// --- determina dias inhabiles -----------------------------------------------------
	String armaDiasInhabilesJS()
		{
		String resultado = "";
		Vector diasInhabiles;

		int indice = 0;

		diasInhabiles = CargarDias(1);
		if((diasInhabiles!=null) && (diasInhabiles.size() > 0))
			{
			resultado = diasInhabiles.elementAt(indice).toString();
			for(indice = 1; indice<diasInhabiles.size(); indice++)
  				resultado  +=  ", " + diasInhabiles.elementAt(indice).toString();
			}
		return resultado;
		}

	// --- Carga Dias -------------------------------------------------------------------
	public Vector CargarDias(int formato)
		{
		String sqlDias;
		boolean    estado;
		Vector diasInhabiles = new Vector();
		Connection Conexion;
		PreparedStatement qrDias;
		ResultSet rsDias;

		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		try
			{
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null)
				{
				qrDias = Conexion.prepareStatement( sqlDias );
				//qrDias.setSQL(sqlDias);
				if(qrDias!=null)
					{
        			//rsDias = Conexion.executeQuery(0, qrDias, null, null);
        			rsDias = qrDias.executeQuery();

					if(rsDias!=null)
						{
						rsDias.next();
						do
							{
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0)
								{
								dia=Integer.toString( Integer.parseInt(dia) );
								mes=Integer.toString( Integer.parseInt(mes) );
								anio=Integer.toString( Integer.parseInt(anio) );
								}
							String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
							diasInhabiles.addElement(fecha);
							}
						while(rsDias.next());
						rsDias.close();
						}
					else
			  			EIGlobal.mensajePorTrace("puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					}
				else
       	  			EIGlobal.mensajePorTrace("puedo crear Query.", EIGlobal.NivelLog.ERROR);
				}
			else
       			EIGlobal.mensajePorTrace("No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
			Conexion.close();
			}
		catch(SQLException sqle)
			{
      		EIGlobal.mensajePorTrace("efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
      		EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
			}
		return(diasInhabiles);
		}

	// --- Regresa la trama de la cuenta indicada --------------------------------------
	private String tramaCuenta(String trama, String tramaCuentas){
		StringTokenizer tokens;
		String cuenta, fecha, fecha1;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		EIGlobal.mensajePorTrace("<bcProgFechas-->tramaCuenta()ENTRANDO \ntrama=" + trama + "\ntramaCuentas=" + tramaCuentas, EIGlobal.NivelLog.INFO);
		try{
			tokens = new StringTokenizer(tramaCuentas,"@");
			cuenta = trama.substring(0,trama.indexOf("|"));
			fecha = trama.substring(trama.indexOf("|")+6, trama.indexOf("|")+16);


			while(tokens.hasMoreTokens())
			{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				fecha1 = tramaCta.substring(tramaCta.indexOf("|")+1, tramaCta.indexOf("|")+11);
				EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... tramaCuenta()\nfecha1=" + fecha1, EIGlobal.NivelLog.INFO);

				if((numCta.equals(cuenta)) && (fecha1.equals(fecha)))
				{

					tramaFinal = "@" + tramaCta;
				}

			}
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error en tramaCuenta(): " + e.toString(), EIGlobal.NivelLog.INFO);
		}
		finally
		{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... tramaCuenta()\ntramaFinal: " + tramaFinal, EIGlobal.NivelLog.INFO);
			return tramaFinal;
		}
	}


	/************************* Regresa una trama a partir de un vector de cuentas ****************
	 *
	 * Nombre:--> creaTrama
	 * Descri:-->
	 * Autor :-->
	 *
	 *********************************************************************************************/

	public String creaTrama(Vector vectorCuentas)
	{
		StringBuffer regreso = new StringBuffer("");

		for(int a=0;a<vectorCuentas.size();a++)
		{
			regreso.append(((bc_CuentaProgFechas)vectorCuentas.get(a)).trama());
		}

		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... creaTrama, regreso.toString()=\n" + regreso.toString(), EIGlobal.NivelLog.INFO);

		return regreso.toString();
	}


	/************************* Edita la trama con la nueva informaci�n ****************************
	 *
	 * Nombre:--> tramaModificaCuenta
	 * Descri:-->
	 * Autor :--> mtto.- getronics.
	 *
	 *********************************************************************************************/
	private String tramaModificaCuenta(String tramaCuentaNva,  String tramaCuentas, String  tramaCuentaOld ,
	                                   HttpServletRequest req, String [] mensError)
	{
		StringTokenizer tokens;

		String
		       tramaFinal       = "",
		       cuenta           = "";


		/*Mtto. getronics. marzo-2005*/
		String fechaOldIni     =  "",
			   fechaOldFin     =  "",
			   fechaNewIni     =  "",
			   fechaNewFin     =  "";


		String retornoBorra,
		       retornoAlta;

		String mensError1;
		String parTramaMod="";


		tramaCuentaNva = tramaCuentaNva.substring(1);
		tramaCuentaNva = tramaCuentaNva.substring(0, tramaCuentaNva.indexOf("@"));

		tramaCuentaOld = tramaCuentaOld.substring(1);


		EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta()\ntramaCuentaNva:"  + tramaCuentaNva + "*", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta()\ntramaCuentaOld :" + tramaCuentaOld + "*", EIGlobal.NivelLog.INFO);


		try{

			/*Extraer fecha y cuenta e eliminar*/
			cuenta        = tramaCuentaOld.substring(0,tramaCuentaOld.indexOf("|")).trim();
			fechaOldIni   = tramaCuentaOld.substring(tramaCuentaOld.indexOf("|")+1 , tramaCuentaOld.indexOf("|")+11);
			fechaOldFin   = tramaCuentaOld.substring(tramaCuentaOld.indexOf("|")+12, tramaCuentaOld.indexOf("|")+22);


			/*quitando las diagonales*/
			fechaOldIni   = quitaFormatoFecha(fechaOldIni);
			fechaOldFin   = quitaFormatoFecha(fechaOldFin);

			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta() CuentaOld   :" + cuenta, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta() fechaOldIni :" + fechaOldIni, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta() fechaOldFin :" + fechaOldFin, EIGlobal.NivelLog.INFO);


			retornoBorra = enviaTuxedo("BORRAR", cuenta, req, fechaOldIni, fechaOldFin, mensError);


			if ( retornoBorra.substring(16,24).equals("BASE0000") )
			{
				if (altaArbol(tramaCuentaNva, req, mensError))
				{

					req.setAttribute("Trama1", "@" + tramaCuentaNva);

					mensError[0] = "La operaci&oacute;n fue modificada Exitosamente";

					tramaFinal = tramaCuentaNva;

					Xerr = "0";





				}
				else
				{
					/*
						Intentando restaurar el estado original de la programacion
					*/
					mensError1 = mensError[0];
					altaArbol(tramaCuentaOld, req, mensError);
					mensError[0] = mensError1;

					tramaFinal = tramaCuentaOld;

					Xerr = "1";
				}

			}

			else
			{

				mensError[0] = "No fue posible realizar la modificacion";
				tramaFinal = tramaCuentas;

				Xerr = "1";

			}

		}//TRY
		catch(Exception e)
		{

			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta()..ERROR. " + e.toString(), EIGlobal.NivelLog.INFO);

		}
		finally
		{

			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta(). ,Xerr      =" + Xerr, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("<bcProgFechas-->tramaModificaCuenta(). ,tramaFinal=" + tramaFinal.trim(), EIGlobal.NivelLog.INFO);
			return tramaFinal = tramaFinal.trim();

		}

	}





// --- Indica si las cuentas de una estructura tienen todos sus datos capturados ---
	private boolean tramaLista(String trama, HttpServletRequest req, String [] mensError)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a  tramaLista", EIGlobal.NivelLog.INFO);
		Vector cuentas = creaCuentas(trama, req, mensError);
		bc_CuentaProgFechas cuenta;
		boolean retorno = true;
		int num;

		for(num=0;num<cuentas.size() && retorno;num++)
			{
			cuenta = (bc_CuentaProgFechas)cuentas.get(num);
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... tramaLista, retorno"+num+": " + retorno, EIGlobal.NivelLog.INFO);
/*			if(cuenta.getHorarios().size() == 0)
			//	if(!(cuenta.nivel() == 1))
				if(!(cuenta.nivel() == 1 && cuenta.getTipo() != 2 && cuenta.getTipo() != 3))
			    //if (cuenta.nivel()!=1 || cuenta.getTipo()==2 || cuenta.getTipo()==3)
					retorno = false;
*/			}
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Salio de  tramaLista; " + "retorno: " + retorno, EIGlobal.NivelLog.INFO);
		return retorno;
		}

	// --- Indica si no hay hojas en el nivel 1 del arbol ------------------------------
	private boolean hayHojasEnPrimerNivel(Vector cuentas)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a hayHojasEnPrimerNivel", EIGlobal.NivelLog.INFO);
		bc_CuentaProgFechas ctaActual;
		boolean hoja;

		hoja = false;
		for(int a=0;a<cuentas.size() && !hoja;a++)
			{
			ctaActual = (bc_CuentaProgFechas)cuentas.get(a);
			if(ctaActual.nivel() == 1 && esHoja(ctaActual,cuentas)) hoja = true;
			}
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Salio de hayHojasEnPrimerNivel", EIGlobal.NivelLog.INFO);
		return hoja;
		}

	// --- Construye el archivo para enviar a Tuxedo -----------------------------------
	private boolean construyeArchivo(String tramaCuentas,HttpServletRequest req, String [] mensError){
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a construyeArchivo\ntramaCuentas=" + tramaCuentas, EIGlobal.NivelLog.INFO);
		boolean noError = true;
		FileWriter archivo;
		StringBuffer buffer, tramaHijoPadre, tramaProgramaciones;
		String vigencia1, vigencia2, horOper1, horOper2;
		Vector cuentas, horarios;
		int numHijoPadre, numCuentasHoras, a, b;
		bc_CuentaProgFechas cuenta;

		try{
			cuentas = creaCuentas(tramaCuentas, req, mensError);
			buffer = new StringBuffer("");
			numHijoPadre = 0;
			numCuentasHoras = 0;
			tramaHijoPadre = new StringBuffer("");
			tramaProgramaciones = new StringBuffer("");
			for(b=0;b<cuentas.size();b++){
				cuenta = (bc_CuentaProgFechas)cuentas.get(b);
				vigencia1 = quitaFormatoFecha(cuenta.getVigencia1());
				vigencia2 = quitaFormatoFecha(cuenta.getVigencia2());

				// PROCEDIMIENTO para surimir los : de el horario y agregar cero al final si lo requiere
				horOper1 = cuenta.getHorarioOper1();
				horOper2 = cuenta.getHorarioOper2();
				/*if (horOper1.length() <= 4){
					horOper1 = horOper1.substring(0, horOper1.indexOf(":")) + horOper1.substring(horOper1.indexOf(":")+1);
					horOper1 = "0" + horOper1 ;
				}

				if (horOper2.length() <= 4){
					horOper2 = horOper2.substring(0, horOper2.indexOf(":")) + horOper2.substring(horOper2.indexOf(":")+1);
					horOper2 = "0" + horOper2 ;
				} // Fin del PROCEDIMIENTO*/

				EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... construyeArchivo\nhorOper1= " + horOper1 + ",  horOper2= " + horOper2, EIGlobal.NivelLog.INFO);
				tramaProgramaciones.append(cuenta.getNumCta() + "@"
						+ vigencia1 + "@"
						+ vigencia2 + "@"
						+ horOper1  + "@"
						+ horOper2  + "@"
						//+ cuenta.getHorarioOper1() + "@" // 8:00
						//+ cuenta.getHorarioOper2() + "@" // 9:00   10:00   17:00
						+ cuenta.getTechopre() + "\n");
			}

			buffer.append("" + tramaProgramaciones + "\n");
			archivo = new FileWriter(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tmp"); 
			archivo.write("" + buffer);
			archivo.close();
			}
		catch(Exception e){
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error intentando construir archivo: " + e.toString() + " <---> " + e.getMessage(), EIGlobal.NivelLog.INFO);
			noError = false;
		}finally{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Salio de construyeArchivo", EIGlobal.NivelLog.INFO);
			return noError;
		}
	}



	/*******************Ejecuta una consulta a Tuxedo************************************************
	*
	* Nombre: enviaTuxedo
	* Desc: Ejecuta una consulta a Tuxedo
	*
	************************************************************************************************/

	private String enviaTuxedo(String tipo, String cuentaHija, HttpServletRequest req, String fechaIni, String fechaFin, String [] mensError)
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("<bcProgFechas-->Llego a enviaTuxedo. Tipo "+tipo, EIGlobal.NivelLog.INFO);

		String trama = "";
		Hashtable ht;
		String tipoOper="";
		String Cuenta="";
		String fecha="";
		String Arch="";
		String nuBit="";
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		boolean criterio = false;

		// --- Secci�n de archivo remoto -----
		if(tipo.equals("ALTA") || tipo.equals("MODIFICACION"))
		{
			// Aqui realiza la copia para tuxedo
			if(archRemoto.copiaLocalARemoto(session.getUserID8()+".tmp")) 
			{
				EIGlobal.mensajePorTrace("bcProgFechas-->enviaTuxedo... Copia remota realizada (local a Tuxedo)", EIGlobal.NivelLog.INFO);
			}
			else
			{
				EIGlobal.mensajePorTrace("bcProgFechas-->enviaTuxedo... Copia remota NO realizada (local a Tuxedo)", EIGlobal.NivelLog.INFO);
				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}

		}

		if(tipo.equals("ALTA"))
		{
			// No debe llevar el nombre del archivo en la trama
			trama = "2EWEB|"+session.getUserID8()+"|B0PA|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+"1"+"| |";
			tipoOper="B0PA";
			nuBit="ERBR02";
		}
		else if(tipo.equals("CONSULTA"))
			{trama = "2EWEB|"+session.getUserID8()+"|B0PC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|N@"+cuentaHija+"| |";
			tipoOper="B0PC";
			Cuenta=cuentaHija;

			}
		else if(tipo.equals("BORRAR"))
			{trama = "2EWEB|"+session.getUserID8()+"|B0PB|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+cuentaHija+"@N@"+fechaIni+"@"+fechaFin+"@| |";
			tipoOper="B0PB";
			Cuenta=cuentaHija;
			fecha=fechaFin;
			nuBit="ERBR03";
			}
		else
			{trama = session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tmp@" + "|6@|" + tipo + "| | |"; criterio = true;
			Arch=session.getUserID8()+".tmp";
			}
		try
			{


		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


				EIGlobal.mensajePorTrace(fechaHr+"-bcProgFechas-->enviaTuxedo>, trama :"+trama, EIGlobal.NivelLog.INFO);

				// --- Crea el ambiente para Tuxedo
				//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				if(!criterio)
				{
					ht = tuxedo.web_red(trama);
				}
				else
				{
					ht = tuxedo.cuentas_modulo(trama);
				}


				// --- Guarda el resultado de Tuxedo en trama
				trama = (String)ht.get("BUFFER");

				/*lasr*/
				EIGlobal.mensajePorTrace("<bcProgFechas-->enviaTuxedo> , trama 1    : " + trama, EIGlobal.NivelLog.INFO);



				if(criterio && !((String)ht.get("COD_ERROR")).equals("BASE0000"))
				{
					criterio = false;
					mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
					EIGlobal.mensajePorTrace("<bcProgFechas-->enviaTuxedo,  Error en consulta a Tuxedo", EIGlobal.NivelLog.INFO);
				}


				if(criterio) trama = session.getUserID8() + ".ctasA"; 

				// --- Secci�n de archivo remoto -----
				if(tipo.equals("CONSULTA") || tipo.equals("ALTA") || criterio)
				{
					trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());

					if(archRemoto.copia(trama))
						{

							trama = Global.DIRECTORIO_LOCAL + "/" +trama;
							EIGlobal.mensajePorTrace("<bcProgFechas-->enviaTuxedo ..... Copia remota realizada (Tuxedo a local), trama= " + trama, EIGlobal.NivelLog.INFO);
						}
					else
						{
							EIGlobal.mensajePorTrace("<bcProgFechas-->enviaTuxedo ..... Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
							mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
						}
				}
				/*
				 * VSWF ARR -I
				 */
				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					 if(!tipo.equals("CONSULTA")){
					try{
						String coderror =((String)ht.get("COD_ERROR"));
						BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(nuBit);
						bt.setContrato(session.getContractNumber());
						if(coderror!=null){
							if(coderror.substring(0,2).equals("OK")){
								bt.setIdErr(tipoOper + "0000");
							}else if(coderror.length()>8){
								bt.setIdErr(coderror.substring(0,8));
							}else{
								bt.setIdErr(coderror);
							}
						}
						if(tipoOper!=null && !tipoOper.equals(""))
							bt.setServTransTux(tipoOper);
						if(Arch!=null && !Arch.equals(""))
							bt.setNombreArchivo(Arch);
						BitaHandler.getInstance().insertBitaTransac(bt);
					   }catch(SQLException e){
						e.printStackTrace();
					   }catch(Exception e){
						e.printStackTrace();
					}
					}
				}
			/*
			 * VSWF ARR -F
   		     */
			}
			catch(Exception e)
			{
				EIGlobal.mensajePorTrace("<bcProgFechas-->enviaTuxedo.... Error intentando conectar a Tuxedo: " + e.toString() + " <---> " + e.getMessage() +  " <---> " + e.getLocalizedMessage(), EIGlobal.NivelLog.INFO);
				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
			finally
			{
				EIGlobal.mensajePorTrace("<SALIENDO-->enviaTuxedo. Trama:"+ trama, EIGlobal.NivelLog.INFO);
				return trama;
			}
		}





	// --- Lee el archivo especificado y obtiene datos de cuentas ----------------------
	private Vector datosCuentas(String archivo, HttpServletRequest req, String [] mensError)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a datosCuentas", EIGlobal.NivelLog.INFO);
		int car;
		FileReader fuente;
		StringBuffer buffer;
		StringTokenizer separaTrama;
		Vector datos = null;
		bc_CuentaProgFechas cuenta;

		try
			{
			fuente = new FileReader(archivo);
			buffer = new StringBuffer("");
			while((car = fuente.read()) != -1) {if(car != 10 && car != 13) buffer.append("" + (char)car);}
			fuente.close();
			while((car = buffer.toString().indexOf("@@")) != -1) buffer.insert(car+1," ");
			separaTrama = new StringTokenizer(buffer.toString(),"@");
			datos = new Vector();
			while(separaTrama.hasMoreTokens())
				{
				cuenta = new bc_CuentaProgFechas(separaTrama.nextToken());
				separaTrama.nextToken();
				cuenta.setDescripcion(separaTrama.nextToken());
				separaTrama.nextToken(); separaTrama.nextToken(); separaTrama.nextToken();
				cuenta.setTipo(Integer.parseInt(separaTrama.nextToken()));
				separaTrama.nextToken();
				datos.add(cuenta);
				}
			}
		catch(Exception e)
			{
			datos = null;
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error en datosCuentas(): " + e.toString(), EIGlobal.NivelLog.INFO);
			}
		finally
			{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a datosCuentas", EIGlobal.NivelLog.INFO);
			return datos;
			}
		}

	// --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
	// REVISAR EL PROCESO EN ESTE METODO
	private String leeArchivo(String nombreArchivo)
		{
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Llego a leeArchivo", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... nombreArchivo de la trama: " + nombreArchivo, EIGlobal.NivelLog.INFO);
		StringBuffer tramaT = new StringBuffer(""), tramaR = new StringBuffer("");
		StringBuffer nomArchivo1 = new StringBuffer("");
		StringBuffer nomArchivo2 = new StringBuffer("");
		StringTokenizer tokens;
		FileReader archivo;
		String strAux, linea;//, tipoArbol;
		Vector cuentas;
		bc_CuentaProgFechas cuenta;
		int car, a, b;

		BufferedReader nomArchivo;

		try{
			nomArchivo = new BufferedReader(new FileReader (nombreArchivo));
			linea = nomArchivo.readLine();
			//nomArchivo1.append(linea + "\n" );
			nomArchivo1.append(linea);
			while ((linea = nomArchivo.readLine()) != null){
				//nomArchivo1.append("@" + linea + "\n" );
				nomArchivo1.append("@" + linea);
			}
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... leeArchivo, nomArchivo1=\n**" + nomArchivo1+"**", EIGlobal.NivelLog.INFO);
			// Se lee del archivo al StringBuffer
/*			archivo = new FileReader(nombreArchivo);
			car = 0; while(car != -1) {car = archivo.read(); tramaT.append("" + (char)car);}
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... leeArchivo, car= "+car+" y tramaT= "+tramaT, EIGlobal.NivelLog.INFO);
			archivo.close();
*/
			if(!nomArchivo1.toString().substring(16,24).equals("BASE0000"))
				{nomArchivo1.delete(0,24); nomArchivo1.insert(0,"<<ERROR>>@trama de tuxedo: ");}
			else {
				nomArchivo1.delete(0,nomArchivo1.toString().indexOf("@") + 1);
				nomArchivo1.insert(0,"TRAMA_OK@");
				}
			nomArchivo.close();
		}catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Error en leeArchivo(): " + e.toString(), EIGlobal.NivelLog.INFO);
			//tramaT = new StringBuffer("<<ERROR>>@trama en el archivo: " + tramaT.toString());
			nomArchivo1 = new StringBuffer("<<ERROR>>@trama en el archivo: " + nomArchivo1.toString());
			}
		finally
			{
			EIGlobal.mensajePorTrace("<DEBUG Mayen> .......... Saliendo de leeArchivo", EIGlobal.NivelLog.INFO);
			//return tramaT.toString();
			return nomArchivo1.toString();
			}
		}

	// --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
	private String leeArchivo1(String nombreArchivo)
	{
		EIGlobal.mensajePorTrace("<bcProgFechas-->Llego a leeArchivo1, nombreArchivo de la trama:\n" + nombreArchivo, EIGlobal.NivelLog.INFO);

		StringBuffer Trama = new StringBuffer("");
		FileReader archivo;
		int car;

		try
			{
				// Se lee del archivo al StringBuffer
				archivo = new FileReader(nombreArchivo);

				car = 0;

				while(car != -1)
				{
					car = archivo.read();
					Trama.append("" + (char)car);
				}


				archivo.close();
				EIGlobal.mensajePorTrace("<bcProgFechas-->leeArchivo1     , Trama.toString(): " + Trama.toString(), EIGlobal.NivelLog.INFO);
			}
		catch(Exception e)
			{
				EIGlobal.mensajePorTrace("<bcProgFechas-->leeArchivo1> .......... Error en bcProgFechas.leeArchivo1() ", EIGlobal.NivelLog.INFO);
			}
		finally
			{
				EIGlobal.mensajePorTrace("<bcProgFechas-->leeArchivo1> .......... Saliendo de leeArchivo1", EIGlobal.NivelLog.INFO);
				return Trama.toString();
			}
	}




	/***********************************************************************************************
	*
	* Nombre: altaArbol
	* Desc: Da de alta en la lista de la base de datos
	*
	************************************************************************************************/

	private boolean altaArbol(String tramaCuentas, HttpServletRequest req, String [] mensError)
	{
		EIGlobal.mensajePorTrace("<bcProgFechas-->altaArbol  tramaCuentas de entrada:\n" + tramaCuentas, EIGlobal.NivelLog.INFO);
		String resultadoTuxedo, tramaArchivo;
		boolean archivoCreado;
		boolean regreso = false;


		archivoCreado = construyeArchivo(tramaCuentas, req, mensError);

		if(!archivoCreado)
		{
			mensError[0] = "Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			regreso = false;

		}
		else
		{

			resultadoTuxedo = enviaTuxedo("ALTA", "", req, "", "", mensError);


			tramaArchivo = leeArchivo1(resultadoTuxedo);



			if(!tramaArchivo.substring(16,24).equals("BASE0000"))
			{

				mensError[0] = tramaArchivo.substring(tramaArchivo.indexOf("|")+10, tramaArchivo.lastIndexOf("|"));
				regreso = false;

			}
			else
			{

				mensError[0] = "La operaci&oacute;n fue programada Exitosamente";
				regreso =  true;
			}


		}

		return regreso;
	}

	/***********************************************************************************************
	*
	* Nombre: despliegaMensaje
	* Desc:
	*
	************************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request,
	                             HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal Global=new EIGlobal(session,contexto,this);

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		//	<vswf:meg cambio de NASApp por Enlace 08122008>

		request.setAttribute( "URL", "/Enlace/enlaceMig/bcProgFechas" );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );



		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
	}

	/********Genera las cuentas hijas del Mantenimiento a Estructuras para el Combo en el JSP********
	*
	* Nombre: comboCuentas
	* Desc:
	*
	************************************************************************************************/

	public String comboCuentas(HttpServletRequest req, String [] mensError)
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		Hashtable ht;
		String trama = "";
		Vector cuentasMto = new Vector();
		String valorFinal = "";
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		Hashtable infoCuentas = new Hashtable();
		String infoArchivo = "infoCtas.mto";
		String infoLinea = "";
		String infoClave, infoContenido;
		BufferedReader infoEntrada = null;
		int n=0;

		EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas ...Entrando", EIGlobal.NivelLog.INFO);

		try
		{
			// Hacer consulta a tuxedo
			trama = "2EWEB|"+session.getUserID8()+"|B0AC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile();
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 EIGlobal.mensajePorTrace(fechaHr+"bcProgFechas - Trama enviada:: "+trama, EIGlobal.NivelLog.DEBUG);
	     //MSD Q05-0029909 fin lineas agregada

			ht = tuxedo.web_red(trama);
			trama = (String)ht.get("BUFFER");



			// realizar copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
			if(archRemoto.copia(trama))
			{

				trama = Global.DIRECTORIO_LOCAL + "/" + trama;
			}
			else
			{

				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				return "";

			}

			// Leer datos
			BufferedReader entrada = new BufferedReader(new FileReader(trama));
			String linea = entrada.readLine();

			if(linea != null) linea = entrada.readLine(); else return "";
			if(linea == null) return "";

			n = 2;

			while((linea = entrada.readLine()) != null)
			{
				n++;
				cuentasMto.add(linea.substring(0,linea.indexOf("|")));
			}

			//  Obtener descripciones de las cuentas. Realizar consulta para obtener archivo de cuentas
			if(llamado_servicioCtasInteg("7@", " ", " ", " ",infoArchivo, req))
			{
				// Hacer un parser del archivo y guardar la info en un Hastable: clave: cuenta, valor: descripci�n
				infoEntrada = new BufferedReader(new FileReader(Global.DOWNLOAD_PATH + infoArchivo));

				while((infoLinea = infoEntrada.readLine())!= null)
				{
					infoClave = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoContenido = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoCuentas.put(infoClave,infoContenido);
				}
			}

			// Construir c�digo HTML con la info
			while(cuentasMto.size()>0)
			{

				infoContenido = (String)infoCuentas.get((String)cuentasMto.get(0));

				if(infoContenido == null) infoContenido = "";
				valorFinal += "<OPTION value=\"" + (String)cuentasMto.get(0) + "\">" + (String)cuentasMto.remove(0) + " -- " + infoContenido + "</OPTION>";

			}

			infoEntrada.close();
			entrada.close();
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas.java> Error: " + e, EIGlobal.NivelLog.INFO);
		}

		EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas> ., valorFinal: " + valorFinal, EIGlobal.NivelLog.INFO);
		return valorFinal;
}




	/************************************************************************************************
	*
	* Nombre: comboCuentas
	* Desc: Genera las cuentas hijas del Mantenimiento a Estructuras para el
	*       DESPLIEGUE en la lista de Programaciones
	*
	************************************************************************************************/


	public String comboCuentas1(HttpServletRequest req, String [] mensError)
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		Hashtable ht;
		String trama = "";
		Vector cuentasMto = new Vector();
		String valorFinal = "";
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		Hashtable infoCuentas = new Hashtable();
		String infoArchivo = "infoCtas.mto";
		String infoLinea = "";
		String infoClave, infoContenido;
		BufferedReader infoEntrada = null;
		int n=0, i;

		EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas1 ........Entrando", EIGlobal.NivelLog.INFO);

		try
		{
			// Hacer consulta a tuxedo
			trama = "2EWEB|"+session.getUserID8()+"|B0AC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile();

			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 EIGlobal.mensajePorTrace(fechaHr+"bcProgFechas - Trama enviada: "+trama, EIGlobal.NivelLog.DEBUG);
		 //MSD Q05-0029909 fin lineas agregada


			ht = tuxedo.web_red(trama);
			trama = (String)ht.get("BUFFER");


			// realizar copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
			if(archRemoto.copia(trama))
			{

				trama = Global.DIRECTORIO_LOCAL + "/" + trama;
			}
			else
			{

				mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
				return "";

			}

			// Leer datos
			BufferedReader entrada = new BufferedReader(new FileReader(trama));
			String linea = entrada.readLine();



			if(linea != null) linea = entrada.readLine(); else return "";
			if(linea == null) return "";


			n = 2;

			while((linea = entrada.readLine()) != null)
			{

				n++;
				cuentasMto.add(linea.substring(0,linea.indexOf("|")));

			}

			//  Realizar consulta para obtener archivo de cuentas
			//  Obtener descripciones de las cuentas
			if(llamado_servicioCtasInteg("7@", " ", " ", " ",infoArchivo, req))
			{
				//- Hacer un parser del archivo y guardar la info en un Hastable: clave: cuenta, valor: descripci�n
				infoEntrada = new BufferedReader(new FileReader(Global.DOWNLOAD_PATH + infoArchivo));

				while((infoLinea = infoEntrada.readLine())!= null)
				{

					infoClave = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoLinea = infoLinea.substring(infoLinea.indexOf("@")+1);
					infoContenido = infoLinea.substring(0,infoLinea.indexOf("@"));
					infoCuentas.put(infoClave,infoContenido);
				}
			}

			for (i=0; i<cuentasMto.size(); i++)
			{

				infoContenido = (String)infoCuentas.get((String)cuentasMto.get(i));

				if(infoContenido == null)
					infoContenido = "";

				valorFinal += (String)cuentasMto.get(i) + "|" + infoContenido + "@\n";

			}

			infoEntrada.close();
			entrada.close();
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas1.java> Error: " + e, EIGlobal.NivelLog.INFO);
		}

		EIGlobal.mensajePorTrace("<bcProgFechas-->comboCuentas1,.. valorFinal:\n" + valorFinal, EIGlobal.NivelLog.INFO);
		return valorFinal;
	}

	/**************Importa los datos del archivo a importar seleccionado por el usuario**********
	 *
	 *
	 *
	 *
	 *
	 *
	 *******************************************************************************************/
	private String importaDatos(HttpServletRequest req, String parCuentas, String [] mensError)
	{
		EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos() ---- ENTRANDO:", EIGlobal.NivelLog.INFO);


		String          archivo;		/* archivo que viene del jsp para la importacion*/
		BufferedReader  contArchivo;	/* contenido del archivo*/
		String          linea = "";
		String          cta, fechaIni, fechaFin, horaIni, horaFin, techo;
		String          trama = "", tramaTemp = "";
		String          mensaje = "", retorno = "";
		StringTokenizer tokens, tokens1;
		String          token, token1;

		/*mtto - getronics*/
		String          HorInicio = "",
						MinInicio = "",
						HorFinal  = "",
						MinFinal  = "";
		boolean         HorIniValido = false,
						MinIniValido = false,
						HorFinValido = false,
						MinFinValido = false;

		try{
				// Leer el archivo
				archivo = new String(getFileInBytes(req));

				contArchivo = new BufferedReader(new StringReader(archivo));

				// Crear la trama de Cuentas
				while ((linea = contArchivo.readLine()) != null)
				{
					if (linea.length() == 50)
					{
						cta	= quitaCeros(linea.substring(0,16).trim());
						fechaIni = agregaFormatoFecha(linea.substring(16,24).trim());
						fechaFin = agregaFormatoFecha(linea.substring(24,32).trim());

						horaIni = linea.substring(32,36).trim();
						horaFin = linea.substring(36,40).trim();

						HorInicio = horaIni.substring(0,2).trim(); /*Extrae horas*/
						MinInicio = horaIni.substring(2,4).trim(); /*Extrae minut*/
						HorFinal  = horaFin.substring(0,2).trim(); /*Extrae horas*/
						MinFinal  = horaFin.substring(2,4).trim(); /*Extrae minut*/

						/*Validando que el horario importado sea valido */

						for (int i = 0; i < HorasPermit.length;i++ )
						{
							if(	HorasPermit[i].equals(HorInicio) )
							{
								HorIniValido = true;

							}

							if(	HorasPermit[i].equals(HorFinal) )
							{
								HorFinValido = true;

							}


						}

						for (int i = 0; i < MinutPermit.length;i++ )
						{
							if(	MinutPermit[i].equals(MinInicio) )
							{
								MinIniValido = true;

							}

							if(	MinutPermit[i].equals(MinFinal) )
							{
								MinFinValido = true;

							}


						}

						if ( !HorIniValido || !HorFinValido || !MinIniValido || !MinFinValido )
						{
							EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos().ERROR-->horaIni:" + horaIni, EIGlobal.NivelLog.INFO);
							mensaje = "El Horario es invalido horaIni:"+ horaIni + " horaFin:" + horaFin;
							return "";
						}

						HorIniValido = false;
						HorFinValido = false;
						MinIniValido = false;
						MinFinValido = false;

						EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos()..horaIni:" + horaIni, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos()..horaFin:" + horaFin, EIGlobal.NivelLog.INFO);

						techo = quitaCeros(linea.substring(40).trim());
						trama += cta + "|" + fechaIni + "|" + fechaFin + "|" + horaIni + "|" + horaFin + "|" + techo + "@";
					}
					else
					{
						mensaje = "La longitud de la trama no es correcta.";
						return "";
					}
				}


				trama = trama.substring(0, trama.lastIndexOf("@"));
				EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos().... importaDatos() trama =" + trama, EIGlobal.NivelLog.INFO);

				// Borrar los registros originales

				tokens = new StringTokenizer(parCuentas,"@");

				EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos().... parCuentas(=" + parCuentas, EIGlobal.NivelLog.INFO);

				while(tokens.hasMoreTokens())
				{
					token	 = tokens.nextToken().trim();
					cta		 = token.substring(0,token.indexOf("|"));
					token	 = token.substring(token.indexOf("|")+1);
					fechaIni = quitaFormatoFecha(token.substring(0,token.indexOf("|")));
					token	 = token.substring(token.indexOf("|")+1);
					fechaFin = quitaFormatoFecha(token.substring(0,token.indexOf("|")));
					retorno  = enviaTuxedo("BORRAR", cta, req, fechaIni, fechaFin, mensError);
				}




				// Dar de alta los nuevos registros
				tokens1 = new StringTokenizer(trama,"@");

				while(tokens1.hasMoreTokens())
				{
					token1	 = "@" + tokens1.nextToken().trim() + "| |@";

					EIGlobal.mensajePorTrace("bcProgFechas-->importaDatos> .......... importaDatos() token1<" + token1 + "<", EIGlobal.NivelLog.INFO);

					if (!altaArbol(token1, req, mensError))
					{
						mensaje = mensError[0];
						return "";
					}


				}

			contArchivo.close();
			mensaje = "Operaci&oacute;n realizada correctamente.";

		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("<bcProgFechas-->importaDatos> ......ImportaDatos() ERROR=*" + e + "*", EIGlobal.NivelLog.INFO);
		}
		finally
		{

			mensError[0] = mensaje;
			return consultaArbol(req, mensError);
		}
	}

	//Elimina el formato de fecha dd/mm/aaaa a ddmmaaaa
	 private String quitaFormatoFecha(String fecha){
		String dia, mes, ano, fechaFinal;
		int car;

		/*for (int i=0;i<=fecha.length;){
			car = fecha.indexOf("|");
		}*/

		car = fecha.indexOf("/");
		dia = fecha.substring(0, car);
		fecha = fecha.substring(car+1);
		car = fecha.indexOf("/");
		mes = fecha.substring(0, car);
		fecha = fecha.substring(car+1);
		ano = fecha;

/*		dia = fecha.substring(0, EIGlobal.NivelLog.ERROR);
		mes = fecha.substring(3, EIGlobal.NivelLog.INFO);
		ano = fecha.substring(5);*/
		fechaFinal = dia + mes + ano;

		return fechaFinal;
	 }

	// Le agrega el formato de fecha de ddmmaaaa a dd/mm/aaaa
	 private String agregaFormatoFecha(String fecha)
	 {
		String fechaFinal;
		fechaFinal = fecha.substring(0,2) + "/" + fecha.substring(2,4) + "/" + fecha.substring(4,8) ;
		return fechaFinal;
	 }

	private String quitaCeros(String dato)
	{
		String datoFinal;
		String cero = "";

		do{
			cero = dato.substring(0, 4);

			if(cero.equals("0"))
			{
				dato = dato.substring(1);
			}
		}while(cero.equals("0"));


		return datoFinal = dato;
	}


}
