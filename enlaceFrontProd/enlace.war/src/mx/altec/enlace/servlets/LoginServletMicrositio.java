package mx.altec.enlace.servlets;

import java.io.*;
import java.net.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;

import javax.sql.*;
import java.text.DateFormat;

import com.passmarksecurity.PassMarkDeviceSupportLite;

import mx.altec.enlace.beans.PagoMicrositioRefBean;
import mx.altec.enlace.beans.RSABeanAUX;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.PagoMicrositioRefBo;
import mx.altec.enlace.bo.WSBloqueoToken;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BloqueoUsuarioBO;
import java.util.*;
import java.util.concurrent.ExecutionException;

/************************************************
 *
 * @author MDR
 * incia import para RSA
 *
 */

import mx.altec.enlace.utilerias.RSAFI;
import mx.altec.enlace.utilerias.RSAFII;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;

/************************************************
 *
 * MDR termina Import para RSA
 *
 */

public class LoginServletMicrositio extends BaseServlet {

	/**
	 * Linea de captura
	 */
	static final String LINEA_CAPTURA = "linea_captura";

	/**
	 * convenio
	 */
	static final String CONVENIO = "convenio";

	/**
	 * importe
	 */
	static final String IMPORTE = "importe";

	/**
	 * servicio_id
	 */
	static final String SERVICIO_ID = "servicio_id";

	/**
	 * Codigo de error
	 */
	static final String COD_ERROR = "CodError";

	/**
	 * Mensaje de error
	 */
	static final String MSG_ERROR = "MsgError";

	/**
	 * Hora error
	 */
	static final String HORA_ERROR = "HoraError";

	/**
	 * Atributo url
	 */
	private static final String PARAMETER_KEY_URL = "url";
	/**
	 * Atributo ATTRIBUTE_KEY_MENSAJE_LOGIN01
	 */
	private static final String ATTRIBUTE_KEY_MENSAJE_LOGIN01 = "MensajeLogin01";

	/**
	 * Atributto ATTRIBUTE_KEY_SESSION
	 */
	private static final String ATTRIBUTE_KEY_SESSION = "session";
	/**
	 * Atributo ATTRIBUTE_KEY_USR_CCS
	 */
	private static final String ATTRIBUTE_KEY_USR_CCS =	"usrCSS";
	/**
	 *Atributo HEADER_KEY_IV_USER
	 */
	private static final String HEADER_KEY_IV_USER = "IV-USER";

	/**
	 *Version de componente
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Clave de operacion para pago referenciado
	 */
	private static final String CLAVE_OPERACION_PAGO_REF = "PMRF";

	/************************************************************
	 * get
	 *
	 * @param request
	 *            variable Request
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ***********************************************************/
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		defaultAction(request, response);
	}

	/************************************************************
	 * post
	 *
	 * @param request
	 *            variable Request
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ***********************************************************/
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		defaultAction(request, response);
	}

    /**
	 * Valida intentos fallidos
	 * @param tok : tok
	 */
	public void intentoFallido(Token tok) {
		Connection conn = null;
	 	try {
			conn= createiASConn ( Global.DATASOURCE_ORACLE );
			tok.incrementaIntentos(conn);
			conn.close();
		} catch(SQLException e) {
                             EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			try {
				if (conn!=null) {
					conn.close();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace( "SQLE en LoginServletMicrositio al cerrar conexion en el metodo intentoFallido" , EIGlobal.NivelLog.ERROR);
			}
		}
	}

	/**
	 * Limpia intentos fallidos
	 * @param tok : tok
	 */
	public void limpiaIntentoFallido(Token tok) {
		Connection conn= null;
		try {
			conn= createiASConn ( Global.DATASOURCE_ORACLE );
			tok.reinicializaIntentos(conn);
			conn.close();
		} catch(SQLException e) {
	                         EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}finally{
			try {
				if (conn!=null) {
					conn.close();
				}
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace( "SQLE en LoginServletMicrositio al cerrar conexion en el metodo limpiaIntentoFallido" , EIGlobal.NivelLog.ERROR);
			}
		}
	}
	/************************************************
	 * defaultAction Realiza acciones generales para get y post
	 *
	 * @param request
	 *            variable Requesta
	 * @param response
	 *            variable Response
	 * @throws IOException
	 *             Excepcion Generica
	 * @throws ServletException
	 *             Excepcion Generica
	 ************************************************/
	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		/***************************************************
		 * Codigo insertado para el control de RSA Fase I ** MDR 21/12/2012
		 ***************************************************/

		String usrCSS = null;
		HttpSession sessCSS = request.getSession();

		//Se agregan paramentros de pago referenciado

		String idSat = "";
		String tipoPersona = "";
		String importeSat = "";
		String rfc = "";
		String datosHash = "";
		String lineaCaptura = "";
		String convenio = "";

		idSat = (String) request.getParameter("ID");
		convenio = (String) request.getParameter("CST1");

		if((null != idSat && "SAT".equals(idSat)) || (null != convenio && "0093".equals(convenio)))
		{
			PagoMicrositioRefBean beanPagRef =  null;
			PagoMicrositioRefBo boPagRef = new PagoMicrositioRefBo();
			boolean valErrPMRF = false;

			EIGlobal.mensajePorTrace("LoginServletMicrositio::La opreacion a realizar es pago Referenciado datos....", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Linea de captura :"+ request.getParameter("LC"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Importe :"+ request.getParameter("IM"), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Convenio :"+ request.getParameter("CST1"), EIGlobal.NivelLog.INFO);

			tipoPersona = (String) request.getParameter("TP");
			lineaCaptura = (String) request.getParameter("LC");
			importeSat = (String) request.getParameter("IM");
			rfc = (String) request.getParameter("RFC");
			datosHash = (String) request.getParameter("SH");

			request.setAttribute(CONVENIO, convenio);
			request.setAttribute(IMPORTE, importeSat);
			request.setAttribute(SERVICIO_ID, CLAVE_OPERACION_PAGO_REF);
			request.setAttribute(LINEA_CAPTURA, lineaCaptura);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Servicio :"+ request.getAttribute(SERVICIO_ID), EIGlobal.NivelLog.INFO);

			beanPagRef = new PagoMicrositioRefBean();
			beanPagRef.setIdSat(idSat);
			beanPagRef.setLineaCaptura(lineaCaptura);
			beanPagRef.setImporteSat(importeSat);
			beanPagRef.setRfc(rfc);
			beanPagRef.setTipoPersona(tipoPersona);
			beanPagRef.setConvenio(convenio);
			beanPagRef.setDatosHash(datosHash);

			//Funcionalidad para desplegar mensaje en caso de que algun campo no contenga informacion

			if(!boPagRef.validaDatos(beanPagRef) && !valErrPMRF)
			{
				msgErrorMicrositio = beanPagRef.getMsgError().toString();
				codErrorMicrositio = "2002";
				EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Datos PMRF Incorrectos" , EIGlobal.NivelLog.INFO);
				valErrPMRF = true;
			}

			//Fin valida datos

			//Se agrega funcionalidad para validar el hash

			if(!boPagRef.validaHash(beanPagRef) && !valErrPMRF)
			{
				msgErrorMicrositio = beanPagRef.getMsgError().toString();
				codErrorMicrositio = "2003";
				EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Hash Incorrecto" , EIGlobal.NivelLog.INFO);
				valErrPMRF = true;
			}
			//Fin Validacion Hash

			//Despliega Mensaje de error
			if(valErrPMRF)
			{
				String usrCSS1 = "";
				EIGlobal.mensajePorTrace("LoginServletMicrositio:: valErrPMRF " + valErrPMRF, EIGlobal.NivelLog.DEBUG);
				if(null!=request.getHeader(HEADER_KEY_IV_USER)){
					usrCSS1 = request.getHeader(HEADER_KEY_IV_USER);
				}
				else if(null!=session.getUserID()){
					usrCSS1 = session.getUserID();
				}
				EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS1, EIGlobal.NivelLog.DEBUG);
				sessCSS.setAttribute(ATTRIBUTE_KEY_USR_CCS, usrCSS1);

				request.setAttribute (COD_ERROR, codErrorMicrositio);
				request.setAttribute (LINEA_CAPTURA, lineaCaptura);
				request.setAttribute (MSG_ERROR, msgErrorMicrositio);
				request.setAttribute (HORA_ERROR,ObtenHora ());
				evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, request, response);
				return;
			}
		}

		if(request.getSession().getAttribute("inyeccionURL") != null){

			EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: <<<<---Cierre por vulnerabilidad en URL--->>>> ", EIGlobal.NivelLog.DEBUG);
			try{
				EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: Sacando usuario de sesion por error de inyeccion en URL", EIGlobal.NivelLog.DEBUG);
				if(sessCSS.getAttribute(ATTRIBUTE_KEY_USR_CCS)!=null){
					usrCSS = (String)sessCSS.getAttribute(ATTRIBUTE_KEY_USR_CCS);
					EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: variable rescatada usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);
					cierraDuplicidad(usrCSS);
					sessCSS.setAttribute(ATTRIBUTE_KEY_SESSION, session);
					sessCSS.removeAttribute(ATTRIBUTE_KEY_SESSION);
					sessCSS.setMaxInactiveInterval( 0 );
					sessCSS.invalidate();
					redirSAM(request, response);
					EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: SE VA A PAGINA SAM", EIGlobal.NivelLog.DEBUG);
					return;
				}else{
					EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: NO SE ENCUENTRA USUARIO", EIGlobal.NivelLog.DEBUG);
				}
			}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: Captando usuario que se loguea para una futura validacion de CSS", EIGlobal.NivelLog.DEBUG);
		if(null!=request.getHeader(HEADER_KEY_IV_USER)){
			usrCSS = request.getHeader(HEADER_KEY_IV_USER);
		}
		else if(null!=session.getUserID()){
			usrCSS = session.getUserID();
		}
		EIGlobal.mensajePorTrace("LoginServletMicrositio::defaultAction:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);
		sessCSS.setAttribute(ATTRIBUTE_KEY_USR_CCS, usrCSS);

		response.setContentType("text/html");

		boolean banderaFaseII=Global.BANDERA_RSAFII;

		if(!banderaFaseII){
			/***************************************************
			 * Codigo insertado para el control de RSA Fase I ** MDR 21/12/2012
			 ***************************************************/
			final String origenLogin = request.getParameter("origenLogin");
			final boolean validarRSA=Global.VALIDA_RSA;

			if("1".equals(origenLogin)&&validarRSA){

				final String deviceToken[] = ejecutaAnalyze(request);

				EIGlobal.mensajePorTrace("deviceTokenCookie"+deviceToken[0],EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("deviceTokenFSO"+deviceToken[1],EIGlobal.NivelLog.INFO);

				final Cookie deviceTokenCookie = new Cookie("deviceTokenCookie",deviceToken[0]);
				final Cookie deviceTokenFSO = new Cookie("deviceTokenFSO",deviceToken[1]);

				final int SECONDS_PER_YEAR = 60 * 60 * 24 * 90;

				deviceTokenCookie.setMaxAge(SECONDS_PER_YEAR);
				deviceTokenFSO.setMaxAge(SECONDS_PER_YEAR);

				response.addCookie(deviceTokenCookie);
				response.addCookie(deviceTokenFSO);

			}
			/***************************************************
			 * Termina Codigo insertado para el control de RSA Fase I ** MDR
			 * 21/12/2012
			 *
			 *
			 * **************************************************/
		}else{
			/***************************************************
			 * Codigo insertado para el control de RSA Fase II ** MDR 21/12/2012
			 ***************************************************/
			final String origenLogin = request.getParameter("origenLogin");
			String devicePrint = request.getParameter("devicePrinter");
			request.getSession().setAttribute("valorDeviceP", devicePrint );

			boolean validaRSA = true;
			String estatusUsuario = "";
			String deviceTokenFSO = "";
			String deviceTokenCookie = "";

			if(Global.VALIDA_RSA) {
				if("1".equals(origenLogin)){

					UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
				    utilidadesRSA.iniciarConexionRSA();
				    
				    ServiciosAAResponse resp = new ServiciosAAResponse();
				    RSABeanAUX  rsaBeanAUX = new RSABeanAUX();
				    RSABean  rsaBean = new RSABean();
				    rsaBean = utilidadesRSA.generaBean(request, "", "", request.getHeader(HEADER_KEY_IV_USER));
				    rsaBeanAUX.setRsaBean(rsaBean);
				    rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.QUERY);

				    RSAFII rsa = new RSAFII();

				    try {
						resp = (ServiciosAAResponse) rsa.ejecutaMetodosRSA7(rsaBeanAUX);

						estatusUsuario = resp.getIdentificationData().getUserStatus().toString();
					    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
					    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();

					} catch (ExecutionException e) {
						EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
						validaRSA = false;
					} catch (InterruptedException e) {
						EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
						validaRSA = false;
					} catch (ClassCastException e) {
						EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
					}

				}

				 if(validaRSA) {
					UtilidadesRSA.crearCookie(response, deviceTokenCookie);
					request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

					EIGlobal.mensajePorTrace("Estatus del usuario: " + estatusUsuario,EIGlobal.NivelLog.INFO);
				}
			}
			/***************************************************
			 * Termina Codigo insertado para el control de RSA Fase II ** MDR
			 * 21/12/2012
			 *
			 *
			 * **************************************************/
		}
		String opcion = request.getParameter("opcion");

		if(opcion != null && "1".equals(opcion)) { //token

			EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Entra a opcion 1" , EIGlobal.NivelLog.INFO);
			String templateElegido = (String) request.getSession().getAttribute("plantilla");

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute(ATTRIBUTE_KEY_SESSION);
			boolean sesionvalida = true;
			if (sess == null) {
				sesionvalida = false;
			} else {
				session = (BaseResource) sess.getAttribute(ATTRIBUTE_KEY_SESSION);
				if (session == null) {
					sesionvalida = false;
				}
			}
			if (!sesionvalida) {
				msgErrorMicrositio = "Su sesi�n ha expirado.";
				codErrorMicrositio = "0999";
				request.setAttribute (COD_ERROR, codErrorMicrositio);
				request.setAttribute (MSG_ERROR, msgErrorMicrositio);
				request.setAttribute (HORA_ERROR,ObtenHora ());
				EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Sesion Expirada" , EIGlobal.NivelLog.INFO);
				evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, request, response);
				return;
			}

			BloqueoUsuarioBO bloq = new BloqueoUsuarioBO();

			Map tmp2 = new HashMap();
			tmp2 = (HashMap)sess.getAttribute("parametros");
			String contrato = (String)tmp2.get("lstContratos");

			if(bloq.isBlockedUser(contrato,session.getUserID8())){
				EIGlobal.mensajePorTrace( "<<<<---USUARIO BLOQUEADO POR INACTIVIDAD--->>>> ", EIGlobal.NivelLog.DEBUG);
				cierraDuplicidad(session.getUserID8());
				request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Su Usuario ha sido bloqueado por inactividad.\",3,\""+ObtenHora()+"\");" );
				request.setAttribute ( HORA_ERROR, ObtenFecha () );
				sess.invalidate();
				request.getSession().invalidate();
				evalTemplate ( IEnlace.LOGIN_MICROSITIO, request, response );
				return;
			}


			String usr = session.getUserID8();
			Token tok = session.getToken();
			boolean tokenValido = false;
			try {
				Connection conn= createiASConn ( Global.DATASOURCE_ORACLE );
				tokenValido = tok.validaToken(conn, request.getParameter("token"));
				conn.close();
			} catch(SQLException e) {

			}

			session.setTokenValidado(tokenValido);
			if(tokenValido) {
				limpiaIntentoFallido(tok);
				EIGlobal.mensajePorTrace("Template: " + templateElegido, EIGlobal.NivelLog.DEBUG);
				String params = reestableceParametrosEnSession(request);
				EIGlobal.mensajePorTrace("Params: " + request, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Params: " + params, EIGlobal.NivelLog.DEBUG);
				evalTemplate (templateElegido + "?" + params + "&token=true",request, response);
			}
			else {
				boolLogin = false;
				if(usuarioBloqueado(usr)) {
					request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Usuario Bloqueado.\", 3);" );
					request.setAttribute ( "Fecha", ObtenFecha () );
					sess.setAttribute(ATTRIBUTE_KEY_SESSION, session);
					sess.invalidate();
					evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);

				}
				else {
					if(tok.getIntentos() >= 4){
							//llamar al WS de bloqueo
							WSBloqueoToken bloqueo = new WSBloqueoToken();
							String[] resultado = bloqueo.bloqueoToken(usr);

							//Realizar envio de notificaci�n
							EmailSender emailSender=new EmailSender();
							EmailDetails emailDetails = new EmailDetails();

							String contra = (String)sess.getAttribute("contratoBTIF");
                            EIGlobal.mensajePorTrace("contratoBTIF-> " +contra, EIGlobal.NivelLog.DEBUG);

							String nombreContrato = session.getNombreContrato();

							emailDetails.setNumeroContrato(contra);
							emailDetails.setRazonSocial(nombreContrato);
							emailSender.sendNotificacion(request,IEnlace.BLOQUEO_TOKEN_INTENTOS,emailDetails);

							BitaTCTBean beanTCT = new BitaTCTBean ();
							BitaHelper bh =	new BitaHelperImpl(request,session,request.getSession(false));

							beanTCT = bh.llenarBeanTCT(beanTCT);
							beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
							beanTCT.setNumCuenta(contra);
							beanTCT.setTipoOperacion("BTIF");
							beanTCT.setCodError("BTIF0000");
							beanTCT.setOperador(session.getUserID8());
							beanTCT.setConcepto("Bloqueo de token por intentos fallidos");

							try {
								BitaHandler.getInstance().insertBitaTCT(beanTCT);
							}catch(Exception e) {
								EIGlobal.mensajePorTrace("LoginServletMicrositio - Error al insertar en bitacora intentos fallidos",EIGlobal.NivelLog.ERROR);

							}

							bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setIdFlujo("BTIF");
							bt.setNumBit("000000");
							bt.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
							bt.setIdToken(tok.getSerialNumber());

							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}

							try {
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								EIGlobal.mensajePorTrace ("LoginServletMicrositio - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
							} catch (Exception e) {
								EIGlobal.mensajePorTrace ("LoginServletMicrositio - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
							}
							//Realizar cierre de sesi�n y enviar alerta con mensaje de bloqueo por intentos
							intentoFallido(tok);
							request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�per l�nea Empresarial para desbloquearlo.\", 3);" );
							request.setAttribute ( "Fecha", ObtenFecha () );
							int resDup = cierraDuplicidad(session.getUserID8());
							sess.setAttribute(ATTRIBUTE_KEY_SESSION, session);
							 EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "LoginServletMicrositio.java::token() -> resDup: " + resDup, EIGlobal.NivelLog.DEBUG);
							sess.invalidate();
							evalTemplate(IEnlace.LOGIN_MICROSITIO, request, response);
							return;
						}
						intentoFallido(tok);
						boolLogin = false;
						EIGlobal.mensajePorTrace( "------------TOKEN INVALIDO--REPITE-->", EIGlobal.NivelLog.DEBUG);
						request.setAttribute ( ATTRIBUTE_KEY_MENSAJE_LOGIN01, "cuadroDialogo(\"Contrase�a incorrecta\", 3);" );
						request.setAttribute ( "Fecha", ObtenFecha () );
						sess.setAttribute(ATTRIBUTE_KEY_SESSION, session);
						evalTemplate ( IEnlace.TOKEN_MICROSITIO, request, response );
				}
				return;
			}
			return;
		}
		else {
			try{
				if(validaCrossSiteScript(request, response)){
					request.setAttribute("inyeccion", true);
					request.setAttribute (HORA_ERROR, ObtenHora());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, request, response);
					return;
				} else {
			                loginMicrositio( false, request, response );
				}
			} catch(Exception e) {
				response.setContentType("text/html");
				evalTemplate( IEnlace.LOGIN_MICROSITIO, request, response );
			}
		}

	}



	/**
	 * Correccion vulnerabilidades Deloitte
	 * Stefanini Nov 2013
	 * @param request @see {@link HttpServletRequest}
	 * @param response @see {@link HttpServletResponse}
	 * @return true en caso de identificar que un parametro es incorrecto
	 * @throws ServletException @see {@link ServletException}
	 * @throws IOException @see {@link IOException}
	 */
	private boolean validaCrossSiteScript(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: Entrando...", EIGlobal.NivelLog.INFO);

		// Definicion de objetos para transferencias
		String referencia = "";
		String concepto = "";
		boolean valError = false;

		// Definicion compartida
		String convenio = "";
		String importe = "";
		String servicio_id = "";

		HttpSession sess = request.getSession();

		// Definicion SAT
		String lineaCaptura = "";

		if (CLAVE_OPERACION_PAGO_REF.equals(request.getAttribute(SERVICIO_ID)) || CLAVE_OPERACION_PAGO_REF.equals(request.getParameter(SERVICIO_ID)))
		{
			EIGlobal.mensajePorTrace("LoginServletMicrositio::El servicio a realizar es pago referenciado :"+ request.getAttribute(SERVICIO_ID), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Linea de captura :" +  request.getAttribute(LINEA_CAPTURA), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::Importe :" + request.getAttribute(IMPORTE), EIGlobal.NivelLog.INFO);
			convenio = (String) request.getAttribute(CONVENIO);
			if(null ==  convenio) {
				convenio = (String)request.getParameter(CONVENIO);
			}
			importe = (String) request.getAttribute(IMPORTE);
			if(null ==  importe){
				importe = (String)request.getParameter(IMPORTE);
			}
			servicio_id = (String)request.getAttribute(SERVICIO_ID);
			if(null ==  servicio_id){
				servicio_id = (String)request.getParameter(SERVICIO_ID);
			}
			lineaCaptura = (String)request.getAttribute(LINEA_CAPTURA);
			if(null ==  lineaCaptura){
				lineaCaptura = (String)request.getParameter(LINEA_CAPTURA);
			}
		} else {
			EIGlobal.mensajePorTrace("LoginServletMicrositio::el contenido del campo URL es:" + request.getParameter(PARAMETER_KEY_URL), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)request.getParameter(PARAMETER_KEY_URL)), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::el contenido del campo URL despues es:" + request.getParameter(PARAMETER_KEY_URL), EIGlobal.NivelLog.INFO);

			convenio = (String) request.getParameter(CONVENIO);
			referencia = Util.HtmlEncode((String)request.getParameter("referencia"));
			importe = (String) request.getParameter(IMPORTE);
			concepto = Util.HtmlEncode((String)request.getParameter("concepto"));
			servicio_id = Util.HtmlEncode((String)request.getParameter(SERVICIO_ID));

			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: el contenido del campo referencia->" + referencia, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: el contenido del campo concepto->" + concepto, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: el contenido del campo servicio_id->" + servicio_id, EIGlobal.NivelLog.INFO);

			if(null!=importe && Util.validaCCX(importe, 4)){
				EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: el contenido del campo importe es correcto->" + importe, EIGlobal.NivelLog.INFO);
			} else {
				request.setAttribute(COD_ERROR, "1004");
				EIGlobal.mensajePorTrace("El campo importe no acepta caracteres especiales por favor introduzca n�meros y dos decimales->" + importe, EIGlobal.NivelLog.INFO);
				valError = true;
			}

		}

		if(null!=convenio && Util.validaCCX(convenio, 1)){
			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
		} else {
			request.setAttribute(COD_ERROR, "1003");
			EIGlobal.mensajePorTrace("El campo convenio no acepta caracteres especiales por favor introduzca solo n�meros ->" + convenio, EIGlobal.NivelLog.INFO);
			valError = true;
		}
		if(valError) {
			String usrCSS = "";
			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
			if(null!=request.getHeader(HEADER_KEY_IV_USER)){
				usrCSS = request.getHeader(HEADER_KEY_IV_USER);
			}
			else if(null!=session.getUserID()){
				usrCSS = session.getUserID();
			}
			EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);
			sess.setAttribute(ATTRIBUTE_KEY_USR_CCS, usrCSS);
			request.getSession().setAttribute("inyeccionURL","true");
			request.setAttribute(MSG_ERROR, "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
		}
		return valError;
	}

	/**
	 *
	 * @param request : request
	 * @return param : param
	 */
    private String reestableceParametrosEnSession(HttpServletRequest request){

        StringBuffer param = new StringBuffer("");
        HttpSession session = request.getSession(false);
//VSWF RRG 08-Dic-2008 I Se le agrego el segundo parametro al metodo decode, para migracion Enlace
		String enc="UTF-8";
//VSWF RRG 08-Dic-2008 F

        if(session != null){
            Map tmp = (Map)session.getAttribute("atributos");
            Iterator it = tmp.keySet().iterator();
            while(it.hasNext()){
                String key = (String)it.next();
                request.setAttribute(key,tmp.get(key));
            }
            tmp = (Map)session.getAttribute("parametros");
            it = tmp.keySet().iterator();

            while(it.hasNext()){
                String key = (String)it.next();
                try {
                	param.append(key + "=" + java.net.URLEncoder.encode((String)tmp.get(key),enc) +"&") ;
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace("Error en parsear datos..."+e, EIGlobal.NivelLog.DEBUG);
				}
			}
        }

		return param.toString();
	}

	/**************************************************************
	 * ejecutaAnalyze Metodo que ejecuta el analyze
	 *
	 * @param request
	 *            Request del servlet
	 * @return deviceToken Arreglo que contiene el valor de las Cookies
	 **************************************************************/
	private String[] ejecutaAnalyze(HttpServletRequest request) {

		String deviceTokenFSOBrowser = "";
		String deviceTokenCookieBrowser = "";

		final String cookieNameDeviceToken = "deviceTokenCookie";
		final String cookieNameDeviceTokenFSO = "deviceTokenFSO";

		final Cookie[] cookies = request.getCookies();
		if ((cookies == null) || (cookies.length == 0)) {
			EIGlobal.mensajePorTrace("No Cookies found",
					EIGlobal.NivelLog.DEBUG);
		} else {
			for (int i = 0; i < cookies.length; i++) {
				if (cookieNameDeviceToken.equals(cookies[i].getName())) {
					deviceTokenCookieBrowser = cookies[i].getValue();
				}
				if (cookieNameDeviceTokenFSO.equals(cookies[i].getName())) {
					deviceTokenFSOBrowser = cookies[i].getValue();
				}
			}
		}
		final String devicePrinter = request.getParameter("devicePrinter");
		final String httpAcept = request.getHeader("Accept");
		final String httpAceptChars = request.getHeader("Accept-Charset");
		final String httpAceptEncoding = request.getHeader("Accept-Encoding");
		final String httpAceptLanguage = request.getHeader("Accept-Language");
		final String httpReferer = request.getHeader("Referer");
		String ipAddr = request.getHeader("iv-remote-address");
		if (ipAddr == null) {
			ipAddr = request.getRemoteAddr();
		}
		final String httpUserAgent = request.getHeader("User-Agent");
		// String userName = request.getParameter("userName");
		final String eDescription = "Acceso de Micrositio Enlace";
		EIGlobal.mensajePorTrace("devicePrinter: " + devicePrinter,
				EIGlobal.NivelLog.DEBUG);

		final RSAFI rsa = new RSAFI();
		final String usr8 = request.getHeader(HEADER_KEY_IV_USER);
		String deviceToken[] = new String[2];

		try {
			deviceToken = rsa.ejecutaAnalyzeRequest(devicePrinter, httpAcept,
					httpAceptChars, httpAceptEncoding, httpAceptLanguage,
					httpReferer, ipAddr, httpUserAgent, usr8, eDescription,
					usr8, "Micrositio Enlace", deviceTokenCookieBrowser,
					deviceTokenFSOBrowser);
		} catch (InterruptedException e) {
			deviceToken[0] = "";
			deviceToken[1] = "";
			EIGlobal.mensajePorTrace("No hay conexion con RSA ejecutaAnalyzeMicro1",EIGlobal.NivelLog.INFO);
		} catch (ExecutionException e) {
			deviceToken[0] = "";
			deviceToken[1] = "";
			EIGlobal.mensajePorTrace("No hay conexion con RSA ejecutaAnalyzeMicro2",EIGlobal.NivelLog.INFO);
		}
		return deviceToken;
	}

}