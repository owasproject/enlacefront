package mx.altec.enlace.servlets;

import javax.servlet.http.*;

public class LongLivedCookie extends Cookie {
	
	/**segundos por a/no**/
	public static final int SECONDS_PER_YEAR = 60*60*24*365;
	/**pmdata**/
	public static final String PMDATA = "PMData";
	/**serial version**/
	private static final long serialVersionUID = 1L;
  
 /**
  * Genera la pmdata cookie
  * @param value : String 
  */
public LongLivedCookie(String value) {
   
	  super(PMDATA, value);
	  
	  setMaxAge(SECONDS_PER_YEAR);
	  //setDomain("Santander");
	  setPath("/");
	  //setSecure(true);
  }
}
