package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.InterbancariasRecibidasBean;
import mx.altec.enlace.beans.InternacRecibidasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.InternacRecibidasBO;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet implementation class InternacRecibidas
 */
public class InternacRecibidas extends BaseServlet {

	/** El objeto serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** El objeto FLUJO TITULO. */
	private static final String FLUJO_TITULO = "Consulta de Movimientos Internacionales Recibidos";

	/** El objeto RUTA FLUJO. */
	private static final String RUTA_FLUJO = "Consultas &gt; Movimientos &gt; Chequeras &gt; Internacionales Recibidos";

	/** El objeto ID FLUJO. */
	private static final String ID_FLUJO = "Chequeras";


	/**
	 * doGet.
	 *
	 * @param request El HttpServletRequest obtenido de la vista
	 * @param response El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}


    /**
	 * doPost.
	 *
	 * @param request El HttpServletRequest enviado a la vista
	 * @param response El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}


	/**
	 * Default action.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {

		CalendarNomina myCalendarNomina;
		EIGlobal.mensajePorTrace("***InternacRecibidas :: INICIO :: defaultAction()", EIGlobal.NivelLog.INFO);
		String modulo = "";

 		final boolean sesionvalida = SesionValida( request, response );

 		final HttpSession sess = request.getSession();
 	    final BaseResource session = (BaseResource) sess.getAttribute("session");

 	   if(!sesionvalida){
	    	EIGlobal.mensajePorTrace("***InternacRecibidas :: defaultAction() :: Fallo Sesion", EIGlobal.NivelLog.ERROR);
	    	return;
	    }

		        myCalendarNomina= new CalendarNomina();
		 		request.setAttribute("Fecha", ObtenFecha());
		 		request.setAttribute("ContUser",ObtenContUser(request));
		 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		 		modulo = request.getParameter("Modulo");

				String VarFechaHoy = "";
				VarFechaHoy = myCalendarNomina.armaArregloFechas();
				String fechaHoy = "";
				final GregorianCalendar cal = new GregorianCalendar();
				fechaHoy = formatoFecha(cal.getTime());
				request.setAttribute("VarFechaHoy", VarFechaHoy);
				request.setAttribute("DiaHoy",fechaHoy);

		        Date fechaActual = new Date();
			    String hoy = formatoFecha(fechaActual);
			    request.setAttribute("fechaDia",hoy);

				session.setModuloConsultar (IEnlace.MConsulta_Saldos);

				if ( (modulo == null) || ( ("").equals(modulo))) {
					modulo = "1";
				}

				 EIGlobal.mensajePorTrace("***InternacRecibidas :: defaultAction() :: MODULO <"+modulo+">", EIGlobal.NivelLog.INFO);

				if("1".equals(modulo)){
		 	    	presentaRecibidas(request, response);
		 	    }else if("2".equals(modulo)){
		 	    	consultaRecibidas(request, response);
		 	    }
	}

	/**
	 * Presenta recibidas.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void presentaRecibidas(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		EIGlobal.mensajePorTrace("***InternacRecibidas :: presentaRecibidas() ", EIGlobal.NivelLog.INFO);

		request.setAttribute("codRespuesta", "NOK");

	    request.setAttribute("Encabezado",CreaEncabezado(FLUJO_TITULO,RUTA_FLUJO,"s25800h",request));
		evalTemplate("/jsp/internacRecibidas.jsp", request, response);
	}

	/**
	 * Consulta recibidas.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void consultaRecibidas(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() ", EIGlobal.NivelLog.INFO);

		InternacRecibidasBO consultaRecibidas = new InternacRecibidasBO();

		final HttpSession sess = request.getSession();
 		BaseResource sesion = (BaseResource) sess.getAttribute ("session");

 		String fecha = request.getParameter("txtFecha").trim();
 		String lineaCuenta = request.getParameter("EnlaceCuenta").trim() != null ? request.getParameter("EnlaceCuenta") : "";;
 		String referencia =  request.getParameter("txtReferencia").trim() != null ? request.getParameter("txtReferencia") : "";

 		String obtenerCuenta[] = lineaCuenta.split("@");
 		String cuenta = obtenerCuenta[0];

 		EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() NUM CUENTA <" + cuenta + ">", EIGlobal.NivelLog.INFO);

 		InterbancariasRecibidasBean datosIniciales = new InterbancariasRecibidasBean();

 		EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() datosIniciales", EIGlobal.NivelLog.INFO);
 		
 		InternacRecibidasBean resultadoConsulta = new InternacRecibidasBean();
 		EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() resultadoConsulta", EIGlobal.NivelLog.INFO);
 		try{
 			resultadoConsulta = consultaRecibidas.exportarRecibidas(fecha, cuenta, referencia, sesion.getUserID());
	 		EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() RESULTADO CONSULTA <"+resultadoConsulta.getLstTransferRecibidas().get(0).getCodRespuesta()+">", EIGlobal.NivelLog.INFO);
	
	 		if("NOK".equals(resultadoConsulta.getLstTransferRecibidas().get(0).getCodRespuesta())){
	 			EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() Sin Registros", EIGlobal.NivelLog.DEBUG);
				despliegaPaginaError("No se encontraron registros en la consulta.",
						FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response);
				return;
	 		} 
	 		if("SIF".equals(resultadoConsulta.getLstTransferRecibidas().get(0).getCodRespuesta()) || resultadoConsulta.getLstTransferRecibidas().get(0).getCodRespuesta() == null ){
	 			EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() Error de Conexion", EIGlobal.NivelLog.INFO);
	 			despliegaPaginaError("Por el momento no se encuentra disponible el servicio.",
						FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response); 			
	 			return;
	 		}
 		}catch(IndexOutOfBoundsException e){
 			EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() IndexOutOfBoundsException: " + e.getMessage(), EIGlobal.NivelLog.INFO);
 			despliegaPaginaError("Por el momento no se encuentra disponible el servicio.",
					FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response); 			
 			return;
 		}catch(NullPointerException e){
 			EIGlobal.mensajePorTrace("***InternacRecibidas :: consultaRecibidas() NullPointerException: " + e.getMessage(), EIGlobal.NivelLog.INFO);
 			despliegaPaginaError("Por el momento no se encuentra disponible el servicio.",
					FLUJO_TITULO, RUTA_FLUJO, ID_FLUJO, request, response); 			
 			return;
 		}

 		bitacoriza(resultadoConsulta.getLstTransferRecibidas().get(0), request, response);
		request.setAttribute("resultadoConsulta", resultadoConsulta.getLstTransferRecibidas().get(0));
 		sess.setAttribute("archivoExportarTI","/Download/" + sesion.getUserID() + ".doc");
 		request.setAttribute("NombreUsuario",sesion.getNombreUsuario());
	  request.setAttribute("MenuPrincipal", sesion.getStrMenu());
	  request.setAttribute("newMenu", sesion.getFuncionesDeMenu());
 		request.setAttribute("codRespuesta", resultadoConsulta.getLstTransferRecibidas().get(0).getCodRespuesta());
	  request.setAttribute("Encabezado",CreaEncabezado(FLUJO_TITULO,RUTA_FLUJO,"s25800h",request));

		evalTemplate("/jsp/internacRecibidas.jsp", request, response);
	}

	/**
	 * Bitacoriza.
	 *
	 * @param datos the datos
	 * @param request the request
	 * @param response the response
	 */
	public void bitacoriza(InternacRecibidasBean datos, HttpServletRequest request,
			HttpServletResponse response){

		ServicioTux tuxGlobal = new ServicioTux();
 		Hashtable hs = null;
 		Integer referencia = 0 ;
 		String codError = null;
 		InternacRecibidasBean beanDatos = null;
		beanDatos = datos;

		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute("session");

			try {

             hs = tuxGlobal.sreferencia("901");
             EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",       EIGlobal.NivelLog.INFO);
             EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hs.toString(), EIGlobal.NivelLog.INFO);
             codError = hs.get("COD_ERROR").toString();

             if (codError.endsWith("0000")) {
                    referencia = (Integer) hs.get("REFERENCIA");
             }
      } catch (Exception e1) {
             EIGlobal.mensajePorTrace("ERROR : "
                          + e1.getMessage(), EIGlobal.NivelLog.ERROR);
      }

		if (("ON").equals(Global.USAR_BITACORAS.trim())){
	        try {
	             BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
	             BitaHelper bh = new BitaHelperImpl(request, session, sess);
	             bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
	             BitaTransacBean bt = new BitaTransacBean();
	             BitaTCTBean beanTCT = new BitaTCTBean ();
	             bt = (BitaTransacBean)bh.llenarBean(bt);
	             bt.setNumBit(BitaConstants.EA_CONS_TRAN_INTER_REC);
	             BitaHandler.getInstance().insertBitaTransac(bt);
	
	             beanTCT = bh.llenarBeanTCT(beanTCT);
	             beanTCT.setReferencia(referencia);
	             beanTCT.setTipoOperacion(BitaConstants.CONS_TRAN_INTER_REC);
	             beanTCT.setCodError(BitaConstants.CONS_TRAN_INTER_REC + "0000");
	             beanTCT.setUsuario(session.getUserID8());
	             beanTCT.setConcepto("Consulta de Oper. Internacional Recibida: REFERENCIA - " + beanDatos.getReferencia());
	             beanTCT.setCuentaDestinoFondo(beanDatos.getCuentaDestino());
	             beanTCT.setImporte(Double.parseDouble(beanDatos.getImporte()));
	
	          BitaHandler.getInstance().insertBitaTCT(beanTCT);
	        }catch(SQLException e){
		    	  EIGlobal.mensajePorTrace("***InternacRecibidas :: SQL Exception : " + e.getMessage(),       EIGlobal.NivelLog.INFO);
		    }catch(Exception e){
		    	  EIGlobal.mensajePorTrace("***InternacRecibidas :: Exception : " + e.getMessage(),       EIGlobal.NivelLog.INFO);
		    }
  		}
	}


	/**
	 * Formato fecha.
	 *
	 * @param date the date
	 * @return the string
	 */
	private String formatoFecha(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
		return sdf.format(date);
	}
}