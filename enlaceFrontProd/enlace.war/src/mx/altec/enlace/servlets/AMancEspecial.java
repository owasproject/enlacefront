package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class AMancEspecial extends BaseServlet
{
	public void doGet( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {
		defaultAction( req, res );
	}
	public void doPost( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {
		defaultAction( req, res );
	}
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException
	{
		boolean testVar= false;
		boolean sesionvalida = SesionValida( req, res );

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if (sesionvalida)
		{
			// Obtiene las cuentas asociadas al contrato
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Generaci&oacute;n de Folios","Administraci&oacute;n y Control &gt; Mancomunidad &gt; Generaci&oacute;n de Folios", "s26010h", req));

			session.setModuloConsultar(IEnlace.MOperaciones_mancom);
			req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
			//TODO BIT CU5021,inicio de flujo de Generación de folios
			/*VSWF-HGG-I*/
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_MANCOM_GEN_FOLIOS_ENTRA);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}

				try {
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			/*VSWF-HGG-F*/
			evalTemplate(IEnlace.MANC_ESPECIAL_TMPL, req, res);
		} else {
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.MSG_PAG_NO_DISP, req, res);
		}
	}
}