package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceException;

import mx.altec.enlace.beans.CuentaODD4Bean;
import mx.altec.enlace.beans.CuentasDescargaBean;
import mx.altec.enlace.beans.ODD4Bean;
import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.DescargaEstadoCuentaBO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class DescargaEdoCtaPDFServlet extends DescargaPdfTdcServlet{
	/** Id de version generado automaticamente para la serializacion de objeto */
	private static final long serialVersionUID = 5476264382078623903L;
	/** Id formato PDF **/
	private static final int STR_FORMATO_PDF = 1;
	/** Id para identificar la pantalla inicial */
	private static final String VENTANA_INICIAL = "0";
	/** Tipo que define TDC **/
	private static final String TIPO_EDCCTA_TDC = "003";
	/** Id para identificar la pantalla que permite seleccionar los periodos */
	private static final String VENTANA_SELECCION_PERIODOS = "1";
	/** Id para identificar la pantalla que permite descargar el estado de cuenta */
	private static final String VENTANA_DESCARGA_EDOCTA = "2";	
	/** Id para identificar la operacion que realiza la exportacion de las cuentas relacionadas con un domicilio */
	private static final String VENTANA_EXPORTAR_CUENTAS = "3";
	/** Id para identificar la operacion que realiza el cambio de cuenta */
	private static final String VALIDAR_CAMBIO_CUENTA = "4";
	/** Id para identificar la operacion que realiza el llamado al WebService **/
	private static final String VALIDAR_PERIODOS_WEBSERVICE = "5";

	/** Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion */
	private static final int BYTES_DOWNLOAD = 1024;
	/** Identificador de las sesion guardada en el request */
	private static final String SESSION_NAME = "session";


	/** Codigo de retorno del WS El Estado de Cuenta del periodo solicitado YA esta disponible en Ondemand **/
	private static final int WS_PERIODO_EDC_EXISTE = 100;
	/** Codigo de retorno del WS El Estado de Cuenta del periodo solicitado NO esta disponible en Ondemand **/
	private static final int WS_PERIODO_EDC_NOEXISTE = 301;
	/** Codigo de retorno cuando el web service no responde a la solicitud **/
	private static final String COD_SERVICIO_NO_DISPONIBLE = "601";
	/** Codigo de retorno al llamado por ajax, que devuelve error **/
	private static final String AJAX_ERROR_GENERICO = "001";	
	/** Atributo del request Cuenta **/ 
	//private static final String CUENTA_TXT_REQUEST_ = "Cuentas";
	private static final String CUENTA_TXT_REQUEST = "cuentasTarjetas";
	
	/** Constante para mensaje usuario sin facultades **/
	private static final String STR_SIN_FACULTADES =  "<br>Usuario sin facultades para operar con este servicio<br>";
	/** Constante para tipo de contenido de la salida del servlet **/
	private static final String STR_CONTENT_TYPE = "text/plain";	
	/** Id de una operacion exitosa, ya no existen mas registros */
	private static final String OPERACION_EXITOSA = "AVODA0002";
	
	/** Id de operacion exitosa, existen mas registros */
	private static final String EXISTEN_MAS_REGISTROS = "AVPEA8001";
	
	
	/** Objeto de negocio **/
	private transient final DescargaEstadoCuentaBO descargaEstadoCuentaBO=new DescargaEstadoCuentaBO();
	
	/**
	 * Recibe las peticiones Get de las peticiones de Descarga de EdoCta PDF
	 * Solo recibe la peticion y la redirecciona al metodo doPost
	 * @param request Objeto con los datos de la peticion
	 * @param response Objeto con los datos de la respuesta
	 * @throws ServletException En caso de que exista un error en la operacion
	 * @throws IOException En caso de que exista un error en la operacion
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s -> Metodo doGet",LOGTAG), NivelLog.INFO);
		doPost( request, response );
	}

	/**
	 * Recibe las peticiones Get de las peticiones de Descarga de EdoCta PDF
	 * Solo recibe la peticion y la redirecciona al metodo doProcess
	 * @param request Objeto con los datos de la peticion
	 * @param response Objeto con los datos de la respuesta
	 * @throws ServletException En caso de que exista un error en la operacion
	 * @throws IOException En caso de que exista un error en la operacion
	 */
	public void doPost( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s -> Metodo doPost",LOGTAG), NivelLog.INFO);
		doProcess( request, response );		
	}
	
	/**
	 * Recibe las peticiones Get de las peticiones de Descarga de EdoCta PDF
	 * Valida que tipo de peticion se ha realizado y ejecuta el metodo que sea
	 * necesario para la Descarga de EdoCta PDF
	 * 
	 * @param request Objeto con los datos de la peticion
	 * @param response Objeto con los datos de la respuesta
	 * @throws ServletException En caso de que exista un error en la operacion
	 * @throws IOException En caso de que exista un error en la operacion
	 */
	public void doProcess( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		boolean sesionValida = SesionValida(request, response);
		
	

		request.setAttribute("msgOption", "");
		request.setAttribute("msgExcede", "");
		String ventana = (request.getParameter("ventana")==null) ? ventana = VENTANA_INICIAL : (String)request.getParameter("ventana");
		//String ventana = request.getParameter("ventana");
		EIGlobal.mensajePorTrace(String.format("%s -> Entrando ventana = %s", LOGTAG,ventana),NivelLog.INFO);		
		if (sesionValida) {
			if (session.getFacultad(BaseResource.FAC_EDO_CTA_DESCARGA)) {					
				descargaEstadoCuentaBO.agregarDatosComunes(request, session);
				setRequest(request,response);
				if(VENTANA_INICIAL.equals(ventana)){
					iniciaConsulta(request, response, ventana);
				} else if(VENTANA_SELECCION_PERIODOS.equals(ventana)){
					mostrarPantallaPeriodos(request, response);
				} else if (VENTANA_DESCARGA_EDOCTA.equals(ventana)) {
					descargaEstadoCuentaPDF(request, response);
				} else if(VENTANA_EXPORTAR_CUENTAS.equals(ventana)){
					exportarCuentas(request, response);
				} else if(VALIDAR_CAMBIO_CUENTA.equals(ventana)) {
					validarCambioCuenta(request, response);
				} else if (VALIDAR_PERIODOS_WEBSERVICE.equals(ventana) ) {
					validarPeriodosWebservice(request, response);
					return;
				}
			} else {
				despliegaPaginaErrorURL(STR_SIN_FACULTADES, STR_ESTADO_CUENTA, STR_POSICION_MENU, "", null, request, response);
			}
		}
	}
	

	
	/**
	 * Muestra la pantalla para la seleccion de los periodos
	 * @param request Contiene la informacion de la peticion al servlet
	 * @param response Contiene la informacion de la respuesta del servlet
	 * @throws ServletException En caso de un error en la operacion
	 * @throws IOException En caso de un error en la operacion
	 */
	public void mostrarPantallaPeriodos(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {

		setRequest(request,response);
		
		final String txtCuenta = request.getParameter(CUENTA_TXT_REQUEST);
		EIGlobal.mensajePorTrace(String.format("%s -> public void mostrarPantallaPeriodos %s [%s]", 
				LOGTAG,CUENTA_TXT_REQUEST,txtCuenta), NivelLog.INFO);	
		request.setAttribute("cuentaFormatoOriginal", txtCuenta);
		
		if (txtCuenta!=null) {
			if (txtCuenta.length()==11 || txtCuenta.length()==14) {
				EIGlobal.mensajePorTrace(String.format("%s Es cuenta de Cheques Longitud [%s]",
						LOGTAG,txtCuenta), NivelLog.DEBUG);
						mostrarPantallaPeriodosPDF(request, response, txtCuenta);
			}else if (txtCuenta.length()==16) {
				EIGlobal.mensajePorTrace(String.format("%s Es cuenta de Credito Longitud [%s]",
						LOGTAG,txtCuenta), NivelLog.DEBUG);
						mostrarPeriodosPDFTDC(request, response, txtCuenta);
			}else {
				//Validado para cuentas de cheques (11 y 14 posiciones) y Tarjeta de Credito (16 posiciones)
				EIGlobal.mensajePorTrace(String.format("%s Cuenta Invalida [%s]",
						LOGTAG,txtCuenta), NivelLog.DEBUG);
				despliegaPaginaConMensaje(String.format("Cuenta invalida [%s]",txtCuenta), 
						STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}			
		}else {
			//La cuenta viene nula, por lo tanto no puede continuar
			EIGlobal.mensajePorTrace(String.format("%s No viene informada la cuenta [%s].",
					LOGTAG,txtCuenta), NivelLog.DEBUG);
			despliegaPaginaConMensaje(String.format("No viene informada la cuenta [%s]",txtCuenta), 
					STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);			
		}
					
	}
	
	/**
	 * Muestra la pantalla para la seleccion de los periodos para estado de 
	 * cuenta en PDF
	 * @param request Contiene la informacion de la peticion al servlet
	 * @param response Contiene la informacion de la respuesta del servlet
	 * @param txtCuenta Cuenta y titular separados por pipes
	 * @throws ServletException En caso de un error en las operaciones
	 * @throws IOException En caso de un error en las operaciones
	 */
	public void mostrarPantallaPeriodosPDF(HttpServletRequest request, HttpServletResponse response, String txtCuenta) 
			throws ServletException, IOException {
		
		
		setRequest(request,response);
		EIGlobal.mensajePorTrace(String.format("%s mostrarPantallaPeriodosPDF -> CuentaSeleccionada > %s",LOGTAG,txtCuenta), NivelLog.INFO);
		final String[] datosCuentaSeleccionada = txtCuenta.split("\\|");
		String numeroCuentaNuevo = request.getParameter(CAMBIO_CTATXT_REQUEST), numeroCuenta = datosCuentaSeleccionada[POSICION_NUMERO_CUENTA],
			errorCod = request.getAttribute(ERROR_REQUEST) != null ? request.getAttribute(ERROR_REQUEST).toString() : "";
		
		if(numeroCuentaNuevo == null || "".equals(numeroCuentaNuevo)) {
			EIGlobal.mensajePorTrace(String.format("%s Valor nulo o vacio para cambioCuentaTXT: [%s]", LOGTAG, numeroCuentaNuevo), NivelLog.DEBUG);
			numeroCuentaNuevo = numeroCuenta;
		}  		
		if("ERRORIN01".equals(errorCod) || "ERRORIN00".equals(errorCod)) {
			numeroCuenta = datosCuentaSeleccionada[POSICION_NUMERO_CUENTA];
		}
		if(numeroCuenta == null || numeroCuenta.length() == 0){
			despliegaPaginaConMensaje("Ocurrio un error al obtener los periodos", STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
		} else {
			boolean ocurrioUnError = false;
			try {
				final HttpSession httpSession = request.getSession();					 								
				final ODD4Bean odd4Bean = (httpSession.getAttribute(ODD4_BEAN_REQUEST)==null)
											? descargaEstadoCuentaBO.obtenerInformacionCliente(numeroCuenta)
											: (ODD4Bean)httpSession.getAttribute(ODD4_BEAN_REQUEST);
				
				// Se valida si la transaccion devolvio un codigo de operacion diferente de exitoso.
				if (!OPERACION_EXITOSA.equals(odd4Bean.getCodigoOperacion())  && 
						!EXISTEN_MAS_REGISTROS.equals(odd4Bean.getCodigoOperacion())) {
					EIGlobal.mensajePorTrace(String.format("%s Se muestra pagina error debido al codigo de operacion recibido por la transaccion ODD4: [%s]"
							,LOGTAG,odd4Bean.getCodigoOperacion()), NivelLog.DEBUG);
					despliegaPaginaErrorURL(odd4Bean.getMensajeOperacion(), STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA, null, request, response);
					return;
				}
				EIGlobal.mensajePorTrace(String.format("%s Numero de Cuenta Nuevo [%s]", LOGTAG, numeroCuentaNuevo), NivelLog.DEBUG);
				final OW42Bean ow42Bean = descargaEstadoCuentaBO.obtenerPeriodos(numeroCuentaNuevo,"00000000");
								
				String valorCambioCuenta = request.getAttribute(CAMBIO_CTATXT_REQUEST) != null 
					? request.getAttribute(CAMBIO_CTATXT_REQUEST).toString() :  numeroCuentaNuevo; 
					
				request.setAttribute("cuentaSeleccionda", numeroCuenta);
				request.setAttribute("codigoCliente", odd4Bean.getNumeroCliente());
				request.setAttribute("numeroSecuencia", odd4Bean.getSecuenciaDomicilio());
				request.setAttribute("titularCuenta", odd4Bean.obtenerNombreCompleto());
				request.setAttribute("domicilioRegistrado", odd4Bean.obtenerDomicilioCompleto());				
				request.setAttribute("periodosRecientes", ow42Bean.obtenerParejasPeriodoFolio());
				request.setAttribute(CAMBIO_CTATXT_REQUEST, valorCambioCuenta);
				httpSession.setAttribute("cuentasRelacionadas", odd4Bean.getCuentas());				
				httpSession.setAttribute(ODD4_BEAN_REQUEST, odd4Bean);
				httpSession.setAttribute(OW42_BEAN_REQUEST, ow42Bean);				
				if(odd4Bean.isTieneProblemaDatos()){
					request.setAttribute("MENSAJE_PROBLEMA_DATOS", odd4Bean.getMensajeProblemaDatos());
				}
				EIGlobal.mensajePorTrace(LOGTAG+"Redireccionando evalTemplate("+JSP_PERIODOS+")", NivelLog.DEBUG);
				evalTemplate(JSP_PERIODOS, request, response );
				
			} catch (BusinessException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				despliegaPaginaConMensaje(e.getMessage(), STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			} 
			
			if(ocurrioUnError){
				despliegaPaginaConMensaje("Ocurrio un error al obtener los periodos para PDF", STR_ESTADO_CUENTA,
						STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}
		}
	}
	
	/**
	 * Descarga Estado de cuenta PDF
	 * @param request : servlet request
	 * @param response : servlet response
	 * @throws IOException : Excepcion
	 * @throws ServletException : Excepcion 
	 */
	private void descargaEstadoCuentaPDF(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException{
		
		/** Envia Respuesta y URL descarga **/
		EIGlobal.mensajePorTrace(LOGTAG+" :::::: Inica descarga del Estado de Cuenta", NivelLog.INFO);
		HttpSession httpSession = request.getSession();
		String valida = request.getParameter("valida");
		CuentasDescargaBean cuentasDescargaBean=(httpSession.getAttribute("cuentasDescargaBean")!=null) ? 
				(CuentasDescargaBean)httpSession.getAttribute("cuentasDescargaBean") : new CuentasDescargaBean() ;	
		String tipoEDC="";
		
		boolean paramValidos=false;
		if (cuentasDescargaBean!=null) {
			EIGlobal.mensajePorTrace(LOGTAG+"El Bean de Descarga no es Nulo - SI PUEDE CONTINUAR", NivelLog.DEBUG);
			request.setAttribute("cuentaOTP", cuentasDescargaBean.getCuenta());
			request.setAttribute("periodoOTP", cuentasDescargaBean.getPeriodoSeleccionado());
			tipoEDC=cuentasDescargaBean.getTipoEdoCta();
			paramValidos=true;
		}else {
			EIGlobal.mensajePorTrace(LOGTAG+"El Bean de Descarga Nulo - NO PUEDE CONTINUAR", NivelLog.DEBUG);
			paramValidos=false;
		}
		if (valida == null && paramValidos) {
			valida = validaToken(request, response, "DescargaEdoCtaPDFServlet");
		}
		if (valida != null && "1".equals(valida) && paramValidos) {							
			/** PISTA_AUDITORA [CDEC-00] Confirma Descarga de Estados de Cuenta **/
			//descargaEstadoCuentaBO.bitaAdmin(request, httpSession, " ", "EWEB_EDOCTA_SEGU"," ", BitaConstants.DESC_EDOCTA_CDEC00, true);
			descargaEstadoCuentaBO.bitaAdmin(request, httpSession, " ", cuentasDescargaBean.getCuenta()," ", BitaConstants.DESC_EDOCTA_CDEC00, true);
			/** BITACORA_OPERACION [CDEC-00] Confirma Descarga de Estados de Cuenta **/
			BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.CONF_DESC_EDOCTA_CDEC, obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuenta(), "PDF", "ECTA0000", BitaConstants.TOKEN_VALIDO_DESCARGA_EDO_CTA);				
					
			//TDC CADENA [SEP-2014@????????@003@5471460035081640@10048237@201409]
			/** 10000052@001@201301  folioOD, tipoEDC, periodo**/

			if (paramValidos) {
				EIGlobal.mensajePorTrace(String.format("%s Cargando Llave de Descarga al Bean",LOGTAG), NivelLog.DEBUG);
				cuentasDescargaBean.setLlaveDescarga(descargaEstadoCuentaBO.genKey());
				EIGlobal.mensajePorTrace(String.format("%s Bean listo para descarga [%s]",LOGTAG,cuentasDescargaBean), NivelLog.DEBUG);
				
				try {
					String urlDescargaEdoCta = "../../../EdoEnlace/edoCtaEnlace/descargaEdoCta.do";
					if (descargaEstadoCuentaBO.insertaRegistroDescarga(cuentasDescargaBean)) {					
						request.setAttribute(URL_EDOCTA_REQUEST,String.format("%s?cadena=%s", urlDescargaEdoCta, cuentasDescargaBean.getLlaveDescarga()) );
						EIGlobal.mensajePorTrace(String.format("%s urlDescargaEdoCta [PDF]:  [%s]", LOGTAG,request.getAttribute(URL_EDOCTA_REQUEST)), NivelLog.INFO);
						System.out.println("urlDescargaEdoCta [PDF]: " + request.getAttribute(URL_EDOCTA_REQUEST));
						
						/** CARGANDO VALORES AL REQUEST PARA MOSTRAR PANTALLA DE PERIODOS **/
						request.setAttribute("cuentaFormatoOriginal", request.getParameter(CUENTA_TXT_REQUEST));
						if (TIPO_EDCCTA_TDC.equals(tipoEDC)) {
							mostrarPeriodosPDFTDC(request, response, request.getParameter(CUENTA_TXT_REQUEST));
						}else {
							mostrarPantallaPeriodosPDF(request, response, request.getParameter(CUENTA_TXT_REQUEST));
						}
					}else {
						despliegaPaginaConMensaje("<br>Error al Crear Registro en Tabla para Descarga <br>",STR_ESTADO_CUENTA,	STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
					}
				} catch (BusinessException e) {
					EIGlobal.mensajePorTrace("Ocurrio un error en : " + e, NivelLog.DEBUG);
					despliegaPaginaConMensaje("<br>Error "+e+" <br>",STR_ESTADO_CUENTA,	STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
				} finally {
					/** PISTA_AUDITORA [DEDC-02] Inicia proceso de Descarga de Estado de Cuenta - PDF**/
					descargaEstadoCuentaBO.bitaAdmin(request, httpSession, " ", cuentasDescargaBean.getCuenta()," ", BitaConstants.DESC_EDOCTA_DEDC02, false);
					/** BITACORA_OPERACION [DEDC-05] Inicia proceso de Descarga de Estado de Cuenta **/	
					BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.DESC_EDOCTA_DEDC, obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), cuentasDescargaBean.getCuenta(), "PDF", "ECTA0000", BitaConstants.CONCEPTO_DESCARGA_EDO_CTA);										
				}
			}else {
				EIGlobal.mensajePorTrace(LOGTAG+" Error en los parametros del periodo Seleccionado "+cuentasDescargaBean, NivelLog.DEBUG);
				despliegaPaginaConMensaje("<br>Error en los parametros del periodo Seleccionado <br>",STR_ESTADO_CUENTA,	STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}						
		}else {
			EIGlobal.mensajePorTrace(LOGTAG+" Error al validar con Token ParamValidos ["+paramValidos+"]", NivelLog.DEBUG);
			despliegaPaginaConMensaje("<br>Error al validar con Token.<br>",STR_ESTADO_CUENTA,	STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
		}
	}
	
	/**
	 * Exporta las cuentas obtenidas de la transaccion ODD4
	 * @param request Contiene la informacion de la peticion al servlet
	 * @param response Contiene la informacion de la respuesta del servlet
	 * @throws ServletException En caso de un error en la operacion
	 * @throws IOException En caso de un error en la operacion
	 */
	public void exportarCuentas(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(String.format("%s -> Entrando exportarCuentas",LOGTAG),NivelLog.INFO);
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);	
		
		if(session.getFacultad(BaseResource.FAC_EDO_CTA_DESCARGA)){
			boolean ocurrioUnError = false;			
			final String cuentaSeleccionada = getFormParameter(request, "cuentaSeleccionada");
			try{
				if(cuentaSeleccionada == null){
					despliegaPaginaConMensaje(
							"No fue posible obtener la informaci&oacute;n de la cuenta, hace falta el N&uacute;mero de Cuenta.", "Estados de Cuenta ",
							STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
					return;
				}				
				final StringBuilder tramas = new StringBuilder();
				
				@SuppressWarnings("unchecked")
				List<CuentaODD4Bean> listaOdd4Bean = (List<CuentaODD4Bean>) httpSession.getAttribute("cuentasRelacionadas");
				
				response.setContentType(STR_CONTENT_TYPE);
				response.setHeader("Content-Disposition",String.format("attachment;filename=Cuentas%s.txt", cuentaSeleccionada));
				
				for(CuentaODD4Bean bean : listaOdd4Bean){
					EIGlobal.mensajePorTrace(String.format("%s -> httpSession.getAttribute(\"cuentasRelacionadas\") [%s]",LOGTAG, bean.getNumeroCuenta() ),NivelLog.INFO);
					if(bean.getNumeroCuenta().length() == 12) {
						if(bean.getNumeroCuenta().startsWith("00")) {
							bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("00", "0"));
						} else if(bean.getNumeroCuenta().startsWith("0")) {
							bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("0", ""));
						} else if (bean.getNumeroCuenta().startsWith("B") && !bean.getNumeroCuenta().startsWith("BME")) {
							bean.setNumeroCuenta(bean.getNumeroCuenta().replaceFirst("B", "BME"));
						}
					}
					tramas.append(bean.getNumeroCuenta());
					tramas.append("\n");
				}			
				InputStream input =	new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
				
				int read = 0;
				byte[] bytes = new byte[BYTES_DOWNLOAD];
				final OutputStream os = response.getOutputStream();

				while ((read = input.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				os.flush();
				os.close();
				
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				ocurrioUnError = true;
			}
			
			if(ocurrioUnError){
				despliegaPaginaConMensaje(
					"No fue posible generar el archivo", "Estados de Cuenta ",
					STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
			}
		}
		else{
			despliegaPaginaConMensaje(STR_SIN_FACULTADES,STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
		}
	}
	
	/**
	 * Metodo encargado de realizar las operaciones de validacion de token
	 * @param request Contiene la informacion de la peticion al servlet
	 * @param response Contiene la informacion de la respuesta del servlet
	 * @param servletName Nombre del servlet con el cual se esta realizando la peticion
	 * @return Validacion del token, cadena vacia en caso de validacion pendiente
	 * @throws IOException En caso de un error con la operacion
	 */
	private String validaToken(HttpServletRequest request, HttpServletResponse response, String servletName) 
			throws IOException{
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);
		//CSA-Vulnerabilidad  Token Bloqueado-Inicio
        int estatusToken = obtenerEstatusToken(session.getToken());
	
		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 1 && solVal) {
			EIGlobal.mensajePorTrace(String.format("%s %s %s %s",LOGTAG,"El usuario tiene Token.",
												"\nSolicito la validaci&oacute;n.",
												"\nSe guardan los parametros en sesion"), NivelLog.DEBUG);
			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request,servletName);
			ValidaOTP.validaOTP(request, response,IEnlace.VALIDA_OTP_CONFIRM);
		}else if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP)
				&& estatusToken == 2 && solVal){                        
        	cierraSesionTokenBloqueado(session,request,response);
		}
		//CSA-Vulnerabilidad  Token Bloqueado-Fin
		else {
			return "1";
		}
		return "";
	}
	
	/**
	 * Metodo que valida si la cuenta es correcta
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : exception lanzada
	 * @throws IOException : exception lanzada
	 */
	private void validarCambioCuenta(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s >>>> validarCambioCuenta <<<<",LOGTAG),NivelLog.INFO);
		
		final String cuentaSeleccionadaTMP = request.getParameter(CAMBIO_CTATXT_REQUEST);
		final String cuentas =  request.getParameter(CUENTA_TXT_REQUEST);
		boolean ocurrioUnError = false;
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION_NAME);
		
		setRequest(request,response);
		
		EIGlobal.mensajePorTrace(String.format("%s -> Entrando inicio Consulta",LOGTAG), NivelLog.INFO);
		descargaEstadoCuentaBO.agregarDatosComunes(request, session);		
		EIGlobal.mensajePorTrace(String.format("%s >>>>> Cuenta a validar: [%s] <<<<<",LOGTAG,cuentaSeleccionadaTMP), 
				NivelLog.INFO);
		
		request.setAttribute("cuentaFormatoOriginal", request.getParameter(CUENTA_TXT_REQUEST));
		request.setAttribute(CAMBIO_CTATXT_REQUEST, request.getParameter(CAMBIO_CTATXT_REQUEST));
		String error = "";
		try {
			if(!descargaEstadoCuentaBO.validaCuenta(cuentaSeleccionadaTMP, request)) {
				EIGlobal.mensajePorTrace(String.format("%s >>>>> La cuenta es incorrecta. <<<<<",LOGTAG), 
						NivelLog.INFO);
				request.setAttribute(ERROR_REQUEST, "ERRORIN01");
			} 
		} catch (BusinessException e) {
			error = e.getMessage();
			ocurrioUnError = true;
		} 
		
		if(ocurrioUnError) {
			EIGlobal.mensajePorTrace(String.format("%s >>>>> La cuenta es incorrecta error: [%s] <<<<<",LOGTAG,error), 
					NivelLog.INFO);
			request.setAttribute(ERROR_REQUEST, "ERRORIN00");
			request.setAttribute("MENSAJEERROR", error); 
		}
		mostrarPantallaPeriodosPDF(request, response, cuentas);	
	}
	
	/**
	 * Invoca el WS de PDF y response via Ajax el error o invoca el metodo de descarga.
	 * @param request : Servlet request
	 * @param response : Servlet response
	 * @throws IOException : Excepcion
	 * @throws ServletException : Excepcion
	 */
	private void validarPeriodosWebservice(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, ServletException  {

		EIGlobal.mensajePorTrace(String.format("%s %s",LOGTAG,"::::: Inicia Validacion Descarga por WS :::::"),NivelLog.DEBUG);
//		String tipoedc="001",pais="MX",numcliente="",numcuenta="",foliood="",periodoNumero="",branch=" ";
		String tipoedc="001",pais="MX",numcliente="",numcuenta="",foliood="",branch=" ";
		CuentasDescargaBean cuentasDescargaBean=new CuentasDescargaBean();
		ServletOutputStream out = response.getOutputStream();
		HttpSession httpSession = request.getSession();

		/** Se quita la bitacora de aqui para tener el Numero de cuenta **/
		try {	
			//ventana=5  cadena=JUN-2012@10000042@001@56722701244@00000372@201206
//			EIGlobal.mensajePorTrace(String.format("%s requestCadena [%s]",LOGTAG,request.getParameter("cadena").toString()), NivelLog.DEBUG);
			String[] cadena = (request.getParameter("cadena")!=null) ? request.getParameter("cadena").toString().split("@") : new String[0];
			EIGlobal.mensajePorTrace(String.format("%s longitud [%s] cadena [%s]",LOGTAG,cadena.length,cadena), NivelLog.DEBUG);
			
			EIGlobal.mensajePorTrace(String.format("%s longitud [%s] cadena [%s]",LOGTAG,cadena.length,cadena), NivelLog.DEBUG);
			for(int p=0;p<cadena.length;p++)
			{
				EIGlobal.mensajePorTrace(String.format("%s cadena [%s]-->[%s]<---",LOGTAG,p,cadena[p]), NivelLog.DEBUG);				
			}
			
			if (cadena.length > 0 && cadena.length>=6) {
				//Valida si es TDC o Cheques
				if (cadena[3].length()==16) {
					EIGlobal.mensajePorTrace("Es Tarjeta de Credito ", NivelLog.DEBUG);
					foliood = descargaEstadoCuentaBO.lPad("",8,"0");
					numcliente = descargaEstadoCuentaBO.lPad("", 1, "0");
				} else {
					EIGlobal.mensajePorTrace("Es Cuenta de Cheques ", NivelLog.DEBUG);
					foliood = cadena[1];
					numcliente = cadena[4];
				}
				tipoedc = cadena[2];
				numcuenta = cadena[3];
//				periodoNumero = cadena[5];
				/** Se mueve la bitacora aqui para tener el Numero de cuenta **/
				/** PISTA_AUDITORA [DEDC-01] Selecciona Periodo para Descarga - PDF **/
//				EIGlobal.mensajePorTrace(String.format("%s PistaAuditora [DEDC-01] Selecciona Periodo para Descarga - PDF",LOGTAG), NivelLog.DEBUG);
				descargaEstadoCuentaBO.bitaAdmin(request, httpSession, " ", numcuenta, " ", BitaConstants.DESC_EDOCTA_DEDC01, true);

//				EIGlobal.mensajePorTrace(String.format("%s Se llena el bean cuentasDescargaBean",LOGTAG), NivelLog.DEBUG);
				if(numcuenta.indexOf("BME") == 0){
					cuentasDescargaBean.setCuenta(numcuenta);
				}else{
					cuentasDescargaBean.setCuenta( (TIPO_EDCCTA_TDC.equals(tipoedc)) ? numcuenta : descargaEstadoCuentaBO.lPad(numcuenta,11,"0"));
				}
				
				cuentasDescargaBean.setCodigoCliente((TIPO_EDCCTA_TDC.equals(tipoedc)) ? numcliente : descargaEstadoCuentaBO.lPad(numcliente,8,"0"));
				cuentasDescargaBean.setPais(pais);
				cuentasDescargaBean.setTipoEdoCta(descargaEstadoCuentaBO.lPad(tipoedc,3,"0"));
//				cuentasDescargaBean.setPeriodoSeleccionado(periodoNumero);
				cuentasDescargaBean.setPeriodoSeleccionado(cadena[5]);
				cuentasDescargaBean.setFolioSeleccionado(descargaEstadoCuentaBO.lPad(foliood,8,"0"));
				cuentasDescargaBean.setTipo(descargaEstadoCuentaBO.lPad(tipoedc,8,"0"));
				cuentasDescargaBean.setFormato(STR_FORMATO_PDF);
				cuentasDescargaBean.setBranch(branch);
				httpSession.setAttribute("cuentasDescargaBean", cuentasDescargaBean);
//				EIGlobal.mensajePorTrace(String.format("%s Se sube a sesion el bean \n%s",LOGTAG,cuentasDescargaBean), NivelLog.DEBUG);
				
//				EIGlobal.mensajePorTrace(String.format("%s Inicia llamado a WebService",LOGTAG), NivelLog.DEBUG);
				String[] respuestaWS=descargaEstadoCuentaBO.invocaWebServicePDF(cuentasDescargaBean);
				int respWSint=Integer.valueOf(respuestaWS[0]);
				
				if (respWSint==WS_PERIODO_EDC_EXISTE || respWSint==WS_PERIODO_EDC_NOEXISTE) {
					response.setContentType(STR_CONTENT_TYPE);
					EIGlobal.mensajePorTrace(String.format("%s RespuestaWS [%s]",LOGTAG,respuestaWS[0]),NivelLog.INFO);
				    out.write(respuestaWS[0].getBytes());
		            out.flush();
				}
			} else {
				EIGlobal.mensajePorTrace(String.format("%sERROR parametros insuficientes",LOGTAG), NivelLog.DEBUG);
			}
		}catch (BusinessException e) {
			EIGlobal.mensajePorTrace(String.format("%s Ocurrio un error [%s]",LOGTAG,e.getMessage()), NivelLog.DEBUG);
			response.setContentType(STR_CONTENT_TYPE);
		    out.write((AJAX_ERROR_GENERICO+"ERROR: "+e.toString()).getBytes());
            out.flush();
		} catch (WebServiceException e) {
			response.setContentType(STR_CONTENT_TYPE);
			EIGlobal.mensajePorTrace(String.format("%s Se retorna codigo 601 a jsp debido a que el web service definido no responde", LOGTAG), NivelLog.DEBUG);
			out.write(COD_SERVICIO_NO_DISPONIBLE.getBytes());
			out.flush();
		}
	}
	
}/** Fin del Servlet **/