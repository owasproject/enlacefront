/*
 * AutorizaAMancomunidad.java
 *
 * Created on 12 de marzo de 2003, 12:20 PM
 *
 * Modificado: 17/11/2003 por Maria Elena de la Pena P. Incidencia: IM227985
 */

package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.beans.SUALCLZS8;
import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosManc;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.bo.RespManc;
import mx.altec.enlace.bo.TransferenciaInterbancariaBUS;
import mx.altec.enlace.bo.SUAConsultaPagoAction;
import mx.altec.enlace.bo.SUAGenCompPDF;
import mx.altec.enlace.bo.SUALineaCapturaAction;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.WSLinCapService;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.utilerias.EmailSender;
import java.sql.SQLException;
import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;  //08-2012 sipare
import mx.altec.enlace.dao.ConfigMancDAO;
//VSWF


/**
 *
 * @author  horacio
 * @version
 */
public class AutorizaAMancomunidad extends BaseServlet {

	/** Pago Sua LC */
    private static final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";
    /** Siguiente */
    private static final String NEXT = "Next";
    /** SESSION */
    private static final String SESSION = "session";
    /** Atributo autoriza */
    private static final String AUTORIZA = "Autoriza";
    /** Atributo ya valido */
    private static final String YA_VALIDO_RSA = "yaValidoRSA";
    /** Atributo FOLIOPARAM */
    private static final String FOLIO_PARAM = "FolioParam";
    /** Atributo datosNotificacion */
    private static final String DATOS_NOTIF = "datosNotificacion";
    /** Atributo tipo operacion */
    private static final String TIPO_OPER = "TipoOp";
    /** Menu principal */
    private static final String MENU_PRINCIPAL = "MenuPrincipal";
    /** Encabezado de pantalla */
    private static final String ENCABEZADO = "Encabezado";
    /** BUFFER de respuesta de operaciones */
    private static final String BUFFER = "BUFFER";

    /** Codigo de error MANC0022 para operaciones verificados*/
    private static final String MANC0022 = "MANC0022";
    /** Codigo de error MANC0023  para operaciones preautorizadas*/
    private static final String MANC0023 = "MANC0023";
    /** Codigo de error MANC0000 para operaciones Autorizadas*/
    private static final String MANC0000 = "MANC0000";
    /** Codigo de error MANC para operaciones Mancomunadas */
    private static final String MANC = "MANC";

   /**
    * Codigo de exito
    */
    private static final String COD_EXITO = "0000";

    /**
     * init
     * @param config config
     * @throws ServletException ServletException
     */
    public void init (ServletConfig config) throws ServletException {

    	super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException IOException
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	String next = request.getParameter ( NEXT );
    	String todas = request.getParameter ("Todas");
    	HttpSession sessionHttp = request.getSession();
	if (SesionValida (request, response)) {
	    BaseResource session =
		(BaseResource) request.getSession ().getAttribute (SESSION);
	    Integer Autoriza = (Integer) request.getSession().getAttribute (AUTORIZA); //CSA

			if (request.getParameter("opcionLC") == null) {
			////System.out.println("  TC A U T O R I Z A:" + Autoriza);
			/******************************************Inicia validacion OTP**************************************/
					String valida = request.getParameter ( "valida" );

					if ( next==null && validaPeticion( request, response,session,request.getSession (),valida))
					{
						  EIGlobal.mensajePorTrace("\n\n ENTRO A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
					}
					/******************************************Termina validacion OTP**************************************/
					else
					{
						//String []datosAntesRSA=request.getParameterValues("Folio");

						///////////////////--------------challenge--------------------------

						if(next==null && valida==null) {

							String validaChallenge = request.getAttribute("challngeExito") != null
								? request.getAttribute("challngeExito").toString() : "";

							EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

							if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {

								String []datosDentroRSA = null;
								if(request.getParameterValues("Folio") != null) {
									datosDentroRSA=request.getParameterValues("Folio");
								}

								EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);

								//String []datos=request.getParameterValues("Folio");

								sessionHttp.setAttribute(YA_VALIDO_RSA,"1");
								sessionHttp.setAttribute(FOLIO_PARAM,datosDentroRSA);
								ValidaOTP.guardaParametrosEnSession(request);

								validacionesRSA(request, response);
								return;
							}
						}

						//Fin validacion rsa para challenge
						///////////////////-------------------------------------------------

								int aut = 2;
								boolean valBitacora = true;

						if( next==null && valida==null)
						{
							String []datos = null;
							EIGlobal.mensajePorTrace(">>> Valor de 'yaValidoRSA__->' : "+sessionHttp.getAttribute(YA_VALIDO_RSA), EIGlobal.NivelLog.DEBUG);
							if("1".equals(sessionHttp.getAttribute(YA_VALIDO_RSA))) {
								datos= (String[])sessionHttp.getAttribute(FOLIO_PARAM);
								sessionHttp.removeAttribute(YA_VALIDO_RSA);
							} else {
								datos=request.getParameterValues("Folio");
							}


							sessionHttp.setAttribute(FOLIO_PARAM,datos);
							java.util.ArrayList notifica = new java.util.ArrayList ();
							notifica.add("");
							request.getSession().setAttribute (DATOS_NOTIF,notifica);

						boolean solVal=ValidaOTP.solicitaValidacion(
									session.getContractNumber(),IEnlace.MANCOMUNIDAD);
						if( session.getValidarToken() &&
							session.getFacultad(session.FAC_VAL_OTP) &&
							session.getToken().getStatus() == 1 &&
							solVal )
						{
							//VSWF-HGG -- Bandera para guardar el token en la bitacora
							request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
							ValidaOTP.guardaParametrosEnSession(request);

							EIGlobal.mensajePorTrace("TOKEN MANCOMUNIDAD", EIGlobal.NivelLog.INFO);
							request.getSession().removeAttribute("mensajeSession");

							if("1".equals(todas)) {
								//Modificación OIVI
								//Se valida que el numero de registros sea igual a 0 para no ejecutar el proceso de autorización asincrono.
						    	if (request.getSession().getAttribute("regManc").equals("0"))
						    	{
						    		request.setAttribute("mensajeError", "cuadroDialogo('No existen más registros por procesar.', 4);");
									evalTemplate( "/jsp/GetAMancomunidad.jsp", request, response );
									return;
						    	}else {
						    		ValidaOTP.mensajeOTP(request, "AutorizaManc_A_T");
						    	}
							}
							else if (request.getParameter(TIPO_OPER)!=null && request.getParameter(TIPO_OPER).toString().trim().equals("1")) {
								ValidaOTP.mensajeOTP(request, "AutorizaManc_A");
							} else {
								ValidaOTP.mensajeOTP(request, "AutorizaManc_C");
							}

							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);


						}
						else{
							ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
							valida="1";
							valBitacora = false;

						}
						}

					//retoma el flujo
						if( (next!=null ||(valida!=null && valida.equals("1"))))
						{


							if (valBitacora && !"1".equals(todas)) {

								try{
									EIGlobal.mensajePorTrace( "*************************Entro cOdigo bitacorizaciOn--->", EIGlobal.NivelLog.INFO);

									HttpSession sessionBit = request.getSession(false);
									Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
									Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");

									String registros[] = (String []) sessionHttp.getAttribute(FOLIO_PARAM);
									//double importeDouble = 0;

									if (tmpParametros!=null && tmpAtributos!=null) {
										Enumeration enumer = request.getParameterNames();

										while(enumer.hasMoreElements()) {
											String name = (String)enumer.nextElement();
										}

										enumer = request.getAttributeNames();

										while(enumer.hasMoreElements()){
											String name = (String)enumer.nextElement();
										}
									}

									short suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
									int numeroReferencia  = obten_referencia_operacion(suc_opera);
									String claveOperacion = BitaConstants.CTK_OPERACIONES_MANC;;
									String concepto = BitaConstants.CTK_CONCEPTO_OPERACIONES_MANC;
									String token = "0";
									if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
									{token = request.getParameter("token");}

									sessionBit.removeAttribute("parametrosBitacora");
									sessionBit.removeAttribute("atributosBitacora");

									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

								} catch(Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							}

							//----- CSA -------
							Integer autP = null;
							try {
								autP = Integer.parseInt(request
										.getParameter(AUTORIZA));
							} catch (NumberFormatException ex) {
								EIGlobal.mensajePorTrace("No se obtuvo el parametro Autoriza", EIGlobal.NivelLog.INFO);
							}

							if (Autoriza == null){
								aut = autP;
								Autoriza = new Integer(aut);
							}
							else if(autP != null){
								if(Autoriza.intValue() != autP.intValue()){
									aut = autP;
									Autoriza = new Integer(aut);
								} else {
									aut = Autoriza;
								}
							}else {
								aut = Autoriza;
							}
							// --- CSA ---------------

							request.getSession ().setAttribute (AUTORIZA, Autoriza);
							String operacion = "";
							if (Autoriza.intValue () == 1)
							{
								operacion = "Autorizaci&oacute;n";
							}
							else
							{
								operacion = "Cancelaci&oacute;n";
							}

							request.getSession ().setAttribute (NEXT, next);

							request.getSession ().setAttribute (MENU_PRINCIPAL, session.getStrMenu ());
							request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
							request.getSession ().setAttribute(ENCABEZADO, CreaEncabezado(""+ operacion +" de Operaciones Mancomunadas",
							"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y "+ operacion,
							"s26000h", request));
							request.getSession ().setAttribute ("operacion", operacion);
							if("1".equals(todas)) {
								//Modificación OIVI
								//Se valida que el numero de registros sea igual a 0 para no ejecutar el proceso de autorización asincrono.
						    	if (request.getSession().getAttribute("regManc").equals("0")) {
						    		request.setAttribute("mensajeError", "cuadroDialogo('No hay registros por autorizar.', 4);");
									evalTemplate( "/jsp/GetAMancomunidad.jsp", request, response );
									return;
						    	} else {
						    		autorizaArchivo(request, response);
						    	}
							} else {
								autorizaOperaciones (request, response);
							}
							EIGlobal.mensajePorTrace("\n\n\n FIN de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.DEBUG);
						}
					} //
			} else {
				generaComprobanteSUALC(request, response);
    }}


}

    /** Metodo para autorizar por archivo
     * @param request Objeto request del servlet
     * @param response Objeto response del servlet
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    private void autorizaArchivo (HttpServletRequest request, HttpServletResponse response)
    	throws ServletException, java.io.IOException {

    	BaseResource session = (BaseResource) request.getSession ().getAttribute (SESSION);
    	ServicioTux servicio = new ServicioTux();
    	String trama = "";
    	String result = "";
    	String codError = "";
    	String mensajeErrorFolio = "";
    	String mensajeErrorEnviar = "";
    	String mensajeErrorDispersor = "";
    	String folioArchivo = "";

		EIGlobal.mensajePorTrace("°°°°°°°°°°°°°°°°°°°° estraeOperaciones indiceArchivoSession ->" + request.getSession().getAttribute("indiceArchivoSession"), EIGlobal.NivelLog.DEBUG);


    	String tramaParaFolio = session.getUserID8() + "|" + session.getUserProfile() + "|" + session.getContractNumber() + "|MNCA|" + request.getSession().getAttribute("regManc") +"|";
		try {
			Hashtable hs = servicio.alta_folio(tramaParaFolio);
		 	result= (String) hs.get(BUFFER) +"";
		 } catch( java.rmi.RemoteException re ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		 } catch(Exception e) {}

		if (result == null || "null".equals(result) || "".equals(result)) {
			result = "ERROR            Error en el servcio.";
		}

		EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: GENERADOR DE FOLIO : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);
		codError = result.substring(0, result.indexOf('|'));

		if (!codError.trim().equals("TRSV0000")) {
			mensajeErrorFolio = result.substring(result.indexOf('|') + 1, result.length());
		}
		else {
			folioArchivo = result.substring(result.indexOf('|') + 1, result.length() );
			folioArchivo = folioArchivo.trim();
			if (folioArchivo.length() < 12) {
				do {
					folioArchivo = "0" + folioArchivo;
				} while(folioArchivo.length() < 12);
			}
			if ((folioArchivo.length() != 12 ) || folioArchivo.equals("000000000000")) {
				mensajeErrorFolio = result ;
			}
			session.setFolioArchivo(folioArchivo);
			request.setAttribute("folioArchivo", folioArchivo);

			String tramaParaEnviar = session.getUserID8() + "|"
			+ session.getUserProfile() + "|"
			+ session.getContractNumber() + "|"
			+ session.getFolioArchivo() + "|MNCA|"
			+ 0 + "|"
			+ session.getUserProfile() + " |" + request.getSession().getAttribute("tramaManc") + "|";

			try{
				Hashtable hs = servicio.envia_datos(tramaParaEnviar);
				result= (String) hs.get(BUFFER) +"";
			} catch(java.rmi.RemoteException re) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.ERROR);
	 		} catch(Exception e) {
	 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	 		}

	 		if(result == null || "null".equals(result) || "".equals(result)) {
	 			result = "ERROR            Error en el servicio.";
	 		}

			EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);
			codError = result.substring(0, result.indexOf('|'));

			if (!codError.trim().equals("TRSV0000")) {
				mensajeErrorEnviar = result.substring(result.indexOf('|') + 1, result.length());
				EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se enviaron los registros al controlador de registros Mensaje: " +mensajeErrorEnviar, EIGlobal.NivelLog.ERROR);
			}
			else {
				mensajeErrorEnviar = result.substring(result.indexOf('|') + 1, result.length() );
				if ("OK".equals(mensajeErrorEnviar)) {
					String tramaParaDispersor = session.getUserID8() + "|"
						+ session.getUserProfile() + "|"
						+ session.getContractNumber() + "|"
						+ session.getFolioArchivo() + "|MNCA|";
					try {
				 		Hashtable hs = servicio.inicia_proceso(tramaParaDispersor);
				 		result= (String) hs.get(BUFFER) +"";
					} catch( java.rmi.RemoteException re ) {
				 		EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				 	} catch(Exception e) {
				 		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
				 	}

					if (result == null || "null".equals(result) || "".equals(result)) {
						result = "ERROR            Error en el servicio.";
					}

					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  DISPERSOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);

					codError = result.substring(0, result.indexOf('|'));

					if (!codError.trim().equals("TRSV0000")) {
						mensajeErrorDispersor = result.substring(result.indexOf('|') + 1, result.length());
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se realizo la dispersion de registros", EIGlobal.NivelLog.DEBUG);
					}
					else {
						mensajeErrorDispersor = result.substring(result.indexOf('|') + 1, result.length());
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
					}
				}
		}

		request.getSession().setAttribute("codError", codError);

		String mensaje_salida = "";
		mensaje_salida="<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=3 CELLSPACING=2  width=300>";
		// EVERIS 28 Mayo 2007 Mostrar el numero de folio
		mensaje_salida += "<TR><td class=tittabcom align=center><br><i>Las operaciones est&aacute;n siendo autorizadas.</i><br>";
			mensaje_salida += "El n&uacute;mero de Folio correspondiente a esta operaci&oacute;n es: <br>";
			mensaje_salida += "<b><font color=red>" + request.getAttribute("folioArchivo") +"</font></b><br> ";
			mensaje_salida += "Utilice este n&uacute;mero de Folio para consultar las <b><font color=blue>referencias</font></b> y<br>";
			mensaje_salida += "<font color=green><b>estado</b></font> de las operaciones, en <br>";
			mensaje_salida += "<b>Consulta Folios</b>.</td> </TR>";
			mensaje_salida+="</TABLE>";

		/*request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");*/
		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");
		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");

		//req.setAttribute("strDebug",strDebug);
		request.setAttribute("mensaje_salida",mensaje_salida);
		request.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());

		request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		request.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
		request.setAttribute("banco",session.getClaveBanco());
		//String Banco= session.getClaveBanco();


		//sess.setAttribute("strDebug",strDebug);
		request.getSession().setAttribute("mensaje_salida",mensaje_salida);
		request.getSession().setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		request.getSession().setAttribute("newMenu", session.getFuncionesDeMenu());
		request.getSession().setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		request.getSession().setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

		request.setAttribute(ENCABEZADO, CreaEncabezado("Consulta de Bit&aacute;cora de Mancomunidad ","Administraci&oacute;n y Control &gt  Mancomunidad  &gt	Consulta y Autorizaci&oacute;n","s25380h",request ));
		request.getSession().setAttribute(ENCABEZADO, CreaEncabezado("Consulta de Bit&aacute;cora de Mancomunidad ","Administraci&oacute;n y Control &gt  Mancomunidad  &gt	Consulta y Autorizaci&oacute;n","s25380h",request ));
//		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");

//		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");

		request.setAttribute("web_application",Global.WEB_APPLICATION);
		request.getSession().setAttribute("web_application",Global.WEB_APPLICATION);
		request.getSession().setAttribute("banco",session.getClaveBanco());

		evalTemplate(IEnlace.MI_OPER_TMPL, request, response);


    }


    /** Metodo para autorizar las operaciones seleccionadas en la consulta de mancomunidad
     * @param request Objeto request del servlet
     * @param response Objeto response del servlet
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    private void autorizaOperaciones (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        boolean indSipare = false;
	String refSipare = "";
	String respuesta = "";
	String divisaAud = "";
    	String[] indices=(String[])request.getSession().getAttribute(FOLIO_PARAM);
    	request.getSession().removeAttribute(FOLIO_PARAM);

	int index = 0;
	if(request.getSession ().getAttribute(NEXT)!=null){
	try {
	    index = Integer.parseInt ((String)request.getSession ().getAttribute(NEXT));
	} catch (NumberFormatException ex) {}
    }

	java.util.ArrayList porProcesar =   (java.util.ArrayList) request.getSession ().getAttribute ("OperacionesMancomunadas");

	if (indices == null && (porProcesar == null || porProcesar.isEmpty ())) {
	    despliegaPaginaError ("Error al realizar la autorizaci&oacute;n", request, response);
	    return;
	}
	/*Enumeration names = request.getSession ().getAttributeNames ();
	while (names.hasMoreElements ()) {
	    //System.out.println (names.nextElement ());
	}*/

	if (indices != null) {

	    request.getSession ().setAttribute ("Pendientes", new Object ());
	    java.util.ArrayList operaciones =new ArrayList();


	    	 if( request.getSession ().getAttribute ( "ResultadosMancomunidad")!=null)
	    	 {
	    		 operaciones =(java.util.ArrayList) request.getSession ().getAttribute ( "ResultadosMancomunidad");
	    	 }



	    porProcesar = new java.util.ArrayList (indices.length);

	    for (int x = 0; x < indices.length; x++) {
		try {
		    int temp = Integer.parseInt (indices[x]);
		    porProcesar.add (operaciones.get (temp));
		    //System.out.println ("Indice agregado: " + temp);
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
		}
	    }

	    request.getSession ().removeAttribute ("ResultadosMancomunidad");
	    request.getSession ().setAttribute ("OperacionesMancomunadas", porProcesar);
	}

	Object o = porProcesar.get (index);
	if (!(o instanceof mx.altec.enlace.bo.DatosManc)) {
	    java.util.ListIterator liPorProcesar = porProcesar.listIterator (index);
	    //System.out.println ("Buscando operaciOn a procesar");
	    while (liPorProcesar.hasNext ()) {
		o = liPorProcesar.next ();
			if (o instanceof mx.altec.enlace.bo.DatosManc)
			{
			    break;
			}
	    }
	}
	if (o instanceof mx.altec.enlace.bo.DatosManc) {
	    //System.out.println ("Operacion encontrada");
	    ServicioTux servicio = new ServicioTux ();

	    /*servicio.setContext (
		((com.netscape.server.servlet.platformhttp.PlatformServletContext)
		    getServletContext ()).getContext ());*/
	    String trama;
		String tramaManc;
	    String codError = null;
	    String cod_error = "";

	    String estatus;
	    String fecha_auto = ObtenFecha(true); //Obtine la fecha de hoy PHH

//		  (new java.text.SimpleDateFormat ("dd/mm/yyyy")).format (new Date ());
	    String folio_auto = "";
	    BaseResource ses = (BaseResource) request.getSession ().getAttribute (SESSION);
	    int aut = 2;
	    try {
		aut = ((Integer) request.getSession ().getAttribute (AUTORIZA)).intValue ();
	    } catch (NumberFormatException ex) {}

	    //HD1000000119983 - Correcion del error de perfil inexistente
	    String error = "";
	    if(ses.getUserProfile() == null || "".equals(ses.getUserProfile()))
	    {
	    	error = TraeAmbiente(ses.getUserID (), ses.getContractNumber (), request);
	    	if("AMBI0000".equals(error)){
	    		String[] noPerfil = TraePerfil (request);
	    		ses.setUserProfile(noPerfil[1]);
	    	}else{
	    		request.setAttribute( MENU_PRINCIPAL, "");
				String encabezado = "Autorización de Operaciones Mancomunadas";
				request.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,"Mancomunidad","s25320h",request) );
				request.setAttribute("MsgError", "Problemas al autorizar la operaci&ocaute;n, finalice sei&oacute;n y vuelva a intentarlo");
				evalTemplate( IEnlace.ERROR_TMPL, request, response );
	    	}
	    }

	    if (aut == 1) {
		trama = ((mx.altec.enlace.bo.DatosManc) o).getTramaAutorizacion (ses.getUserID (), ses.getContractNumber (), ses.getUserProfile ());
	    } else {
		trama = (
		    (mx.altec.enlace.bo.DatosManc) o).getTramaCancelacion (
			ses.getUserID (), ses.getContractNumber (), ses.getUserProfile ());
	    }
	    try {
		    Hashtable hs = new Hashtable();
		    EIGlobal.mensajePorTrace("AutorizaAMancomunidad - tipo Operacion: [" +
						((mx.altec.enlace.bo.DatosManc) o).getTipo_operacion() + "]",EIGlobal.NivelLog.DEBUG);

		    if (((mx.altec.enlace.bo.DatosManc) o).getTipo_operacion().equals(ES_PAGO_SUA_LINEA_CAPTURA)) {

		    	if (aut == 1) {

		    		SUALineaCapturaServlet SUALCServlet = new SUALineaCapturaServlet();
		    		if (SUALCServlet.validaHorario()) {
			    		tramaManc = ((mx.altec.enlace.bo.DatosManc) o).getTramaAutorizacion (convierteUsr8a7(ses.getUserID ()), ses.getContractNumber (), ses.getUserProfile ());
			    		EIGlobal.mensajePorTrace("AutorizaAMancomunidad - trama Aut para manc_valida: [" + tramaManc+ "]",EIGlobal.NivelLog.DEBUG);

			    		SUALineaCapturaAction action = new SUALineaCapturaAction();
						String[] resultManc = action.llamada_valida_manc(tramaManc);
						codError = resultManc[0];
						action = null;
		    		} else {
		    			/* Es horario / fecha inhabil SIPARE */
				        codError = null; /* La operacion no se ejecuto */
				        cod_error = "SIPARE01";
		    		}
		    		SUALCServlet = null;
		    	}else{
		    		((mx.altec.enlace.bo.DatosManc) o).setTipo_operacion("CAMA");
		    		tramaManc = ((mx.altec.enlace.bo.DatosManc) o).getTramaCancelacion (convierteUsr8a7(ses.getUserID ()), ses.getContractNumber (), ses.getUserProfile ());
		    		EIGlobal.mensajePorTrace("AutorizaAMancomunidad - trama Can para manc_valida: [" + tramaManc+ "]",EIGlobal.NivelLog.DEBUG);
		    		hs = servicio.web_red(tramaManc);
			        codError = (String) hs.get (BUFFER);
			        cod_error = (String) hs.get ("COD_ERROR");
		    	}
//					SUALineaCapturaAction action = new SUALineaCapturaAction();
//					String[] resultManc = action.llamada_valida_manc(tramaManc);
//					codError = resultManc[0];
//					action = null;
		} else {

			if ("TRAN".equals(((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ()) ||
				  "PMIC".equals(((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ()))
			{
				String fechaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa");
				String horaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "th:tm:ts");
				String convenio = null;
				String concepto = ((DatosManc)o).getConcepto();
				if("PMIC".equals(((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ()))
				{
						convenio = concepto.substring(concepto.indexOf('@')+1, concepto.lastIndexOf('@')).trim();
			    		concepto = concepto.substring(concepto.lastIndexOf('@')+1);
				}

				WSLinCapService wsLCService = new WSLinCapService();
				respuesta = wsLCService.validaLineaCaptura(request,concepto,((DatosManc)o).getImporte() , "001", "0001", fechaSist, horaSist, "", ((DatosManc)o).getCta_destino(), "", "", ((DatosManc)o).getCta_origen(),((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion (),convenio);
				codError = respuesta;
				cod_error = respuesta.substring(0,8);
			}

			if ("".equals(respuesta) || COD_EXITO.equals(respuesta.substring(4,8)))
			{

				hs = servicio.web_red(trama);
				codError = (String) hs.get (BUFFER);
				cod_error = (String) hs.get ("COD_ERROR");

			} else {
				if (!COD_EXITO.equals(respuesta.substring(4,8)))
				{
					ConfigMancDAO cnfMancDao = new ConfigMancDAO();
					Boolean actualiza = cnfMancDao.actEstatusOperacion(((mx.altec.enlace.bo.DatosManc)o).getFolio_registro(),
//																	folioReg,
																	((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion(),
																	convierteUsr8a7(ses.getUserID()),
																	codError.substring(16),
																	ses.getContractNumber(),
//																	String fchRegistro);
																	((mx.altec.enlace.bo.DatosManc)o).toString().substring(0,10),
																	EIGlobal.formatoFecha(new GregorianCalendar(), "dd/mm/aaaa"));
					if (actualiza)
					{
						EIGlobal.mensajePorTrace("Actualización Correcta",EIGlobal.NivelLog.DEBUG);
					} else {
						EIGlobal.mensajePorTrace("Error al Realizar la Actualización",EIGlobal.NivelLog.DEBUG);
					}
				}
			}

		}

	    } catch (Exception ex) {
	    	EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
	    }
	    // VSWF - JGAL - SIPARE - 09-09-2009
	    BaseResource session = (BaseResource) request.getSession()
					.getAttribute(SESSION);
	    // VSWF - JGAL - SIPARE - 09-09-2009
	    EIGlobal.mensajePorTrace("AutorizaAMancomunidad - codError: [" +
					codError + "]",EIGlobal.NivelLog.DEBUG);

	    if (codError == null || codError.equals ("")) {
			folio_auto = "&nbsp;";
			fecha_auto = "&nbsp;";
	    	if ("SIPARE01".equals(cod_error)) {
				estatus = "Operaci&oacute;n fuera de horario o es d&iacute;a inh&aacute;bil.";
	    	} else {
				estatus = "Error en el servicio";
	    	}
	    } else {
		// VSWF - JGAL - SIPARE - 10-09-2009
		if (((mx.altec.enlace.bo.DatosManc) o).getTipo_operacion().equals(ES_PAGO_SUA_LINEA_CAPTURA)) {
			indSipare = true;
			request.setAttribute("indSipare", "SI");
		}
		else {
			request.setAttribute("indSipare", "NO");
	    }

		if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("OCUR")) {
			if (COD_EXITO.equals (codError.substring (13, 17))) {
				estatus = codError.substring (30);
				folio_auto = codError.substring (0, 9);
			}
			else {
				estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
				folio_auto = "&nbsp;";
			}

		}
		else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("TRAN")
				|| ((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PMIC")) {

			if (codError.substring (0, 2).equals ("OK")) {

				estatus = "Operaci&oacute;n Ejecutada";
				if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PMIC")
						&& !cod_error.equals(MANC0000)) { // No es cancelacion
					String folio = codError.substring (3);
					folio_auto = folio.substring(0, folio.indexOf('|'));
				} else {
					folio_auto = codError.substring (8, 16);
				}
			    EIGlobal.mensajePorTrace("AutorizaAMancomunidad - folio_auto_PMIC/TRAN: [" +
			    		folio_auto + "]",EIGlobal.NivelLog.DEBUG);

			}
			else if(cod_error.equals(MANC0022)) {

				estatus = "Operaci&oacute;n Verificada";
				//folio_auto = "&nbsp;";//codError.substring (8);
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.equals(MANC0023)) {

				estatus = "Operaci&oacute;n Pre-Autorizada";
				//folio_auto = "&nbsp;";//codError.substring (8);
				folio_auto = codError.substring (8, 16);
			}
			else if ("WTRA".equals(cod_error.substring(0,4)) || "WPMI".equals(cod_error.substring(0,4)))
			{
				estatus = codError.substring(16);
			}
			else {
				estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
				folio_auto = "&nbsp;";
			}
		}

		else if ("PMRF".equals (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ())) {

			if (codError.substring (0, 2).equals ("OK")) {

				estatus = "Operaci&oacute;n Ejecutada";
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.equals(MANC0022)) {

				estatus = "Operaci&oacute;n Verificada";
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.equals(MANC0023)) {

				estatus = "Operaci&oacute;n Pre-Autorizada";
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.startsWith(MANC)) {
				estatus = (codError.length () > 16) ? codError.substring (8) : "Operaci&oacute;n no ejecutada";
				folio_auto = "&nbsp;";
			}
			else {
				estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
				folio_auto = codError.substring (8, 16);
			}
		}

		else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PILC")) {
			if (codError.substring (0, 2).equals ("OK"))
			 {
			   estatus = "Operaci&oacute;n Ejecutada";
			   folio_auto = codError.substring (8, 16);
			 }
		    else
			 {
			   estatus = (codError.length () > 16) ? codError.substring (16):"Operaci&oacute;n no ejecutada";
			   folio_auto = (codError.length ()>=16)? codError.substring (8,16):"&nbsp;";
			 }
			}	// VSWF - JGAL - SIPARE  08-2012
			 else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion().equals(ES_PAGO_SUA_LINEA_CAPTURA)){
				 SUALCLZS8 beanValidaciones = null;
				 String impNvo ="";
					 if (codError.trim().equals(MANC0000)) {
						// Invoca a la transacción LZS8 para grabar pago SUALC
						SUALineaCapturaAction actionSUA = new SUALineaCapturaAction();
						SimpleDateFormat sdf = new SimpleDateFormat();
						sdf.applyPattern("dd/MM/yyyy");
						String impStr = "" + new Double(((mx.altec.enlace.bo.DatosManc) o)
								.getImporte()).doubleValue();
						impNvo = ((mx.altec.enlace.bo.DatosManc) o).getImporte();
						impNvo = impNvo.substring(0, impNvo.indexOf(".")+3);

						EIGlobal.mensajePorTrace("AutorizaAMancomunidad - impNvo Final: [" +
								impNvo + "]",EIGlobal.NivelLog.DEBUG);

						impStr = impStr.substring(0, impStr.indexOf(".")) + impStr.substring(impStr.indexOf(".") + 1);
						EIGlobal.mensajePorTrace("AutorizaAMancomunidad - SUALCLZS8: [" +
								((mx.altec.enlace.bo.DatosManc) o)
								.getConcepto() + "]",EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("AutorizaAMancomunidad - impStr Final: [" +
								impStr + "]",EIGlobal.NivelLog.DEBUG);
						//SUALCLZS8 beanValidaciones = actionSUA
						beanValidaciones = actionSUA
								.validacionPagoSUALC(((mx.altec.enlace.bo.DatosManc) o).getCta_origen(),
								Global.SUALC_CTA_ABONO, sdf.format(new Date()),
								impNvo,((mx.altec.enlace.bo.DatosManc) o)
								.getConcepto(), false, true, request);
						beanValidaciones.setLineaCap(((mx.altec.enlace.bo.DatosManc) o).getConcepto());

						  beanValidaciones
								.setCuentaCargo(((mx.altec.enlace.bo.DatosManc) o).getCta_origen());

						beanValidaciones
								.setNombreCuentaCargo(((mx.altec.enlace.bo.DatosManc) o)
										.getTitular());
						EIGlobal.mensajePorTrace("AutorizaAMancomunidad - beanValidaciones.getMsjErr(): [" +
								beanValidaciones.getMsjErr() + "]",EIGlobal.NivelLog.DEBUG);

						if ((beanValidaciones.getMsjErr() != null) && (beanValidaciones.getMsjErr().equals(
								"Operacion Exitosa"))) {
							estatus = "Operaci&oacute;n Ejecutada";
							refSipare = "" + new Long(beanValidaciones.getFolioSua()).longValue();
							folio_auto = ((mx.altec.enlace.bo.DatosManc) o).getFolio_autorizacion();//codError.substring(8, 16);
							EIGlobal.mensajePorTrace("AutorizaAMancomunidad - folio_auto: [" +
									folio_auto + "]",EIGlobal.NivelLog.DEBUG);
							// Procede a llamar a bitacorización
							beanValidaciones.getImporteTotal();
							grabaBita(beanValidaciones, session,((mx.altec.enlace.bo.DatosManc) o).getImporte(), request);

						} else {
							//VAO 08-2012 Correccion para que mande el Error enviado por la transaccion.
							TipoCambioEnlaceDAO actualizaMancDao = new TipoCambioEnlaceDAO();
							// EN CASO DE QUE HAYA UNA ERROR AL APLICAR LA OPERACION, SE GUARDA COMO RECHAZADA
							actualizaMancDao.actEstatusOperManc(beanValidaciones.getNombreContrato(), ((mx.altec.enlace.bo.DatosManc) o).getFolio_registro(), "R");
							estatus = beanValidaciones.getMsjErr();
							refSipare = "";
							folio_auto = ""; //codError.substring(8, 16);
						}
					} else {
						// Si regreso un error de Mancomunidad y es pago Linea de Captura
						beanValidaciones = new SUALCLZS8();
						beanValidaciones.setCodErr(codError.trim()); // carga el codigo de Error generado por Mancomunidad
						beanValidaciones.setEstatus("Operaci&oacute;n Ejecutada" + codError.trim());

						estatus = (codError.length() > 16) ? codError
								.substring(16)
								: "Operaci&oacute;n no ejecutada";
						folio_auto = (codError.length() >= 16) ? codError
								.substring(8, 16) : "&nbsp;";
					}

					 //************************************************
					 //******** Notificacion SIPARE *******************
					 EmailSender emailSender = new EmailSender();
						EmailDetails beanEmailDetails = new EmailDetails();
						if(emailSender.enviaNotificacion(beanValidaciones.getCodErr())){
								EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("CuentaCargo ->" + ((mx.altec.enlace.bo.DatosManc) o).getCta_origen(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Importe ->" + impNvo, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Linea de Captura ->" + ((mx.altec.enlace.bo.DatosManc) o).getConcepto(), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("codErrorOper ->" + beanValidaciones.getCodErr(), EIGlobal.NivelLog.INFO);

								beanEmailDetails.setNumeroContrato(session.getContractNumber());
								beanEmailDetails.setRazonSocial(session.getNombreContrato());
								beanEmailDetails.setNumCuentaCargo(((mx.altec.enlace.bo.DatosManc) o).getConcepto());
								beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(impNvo));
								beanEmailDetails.setTipoPagoImp("SUA Línea de Captura");
								beanEmailDetails.setEstatusActual(beanValidaciones.getEstatus());
								EIGlobal.mensajePorTrace("Estatus->" + beanEmailDetails.getEstatusActual(), EIGlobal.NivelLog.INFO);

								EIGlobal.mensajePorTrace("SIPARE mail->" + IEnlace.PAGO_SUA_LC, EIGlobal.NivelLog.DEBUG);

										beanEmailDetails.setEstatusActual(beanValidaciones.getCodErr());
										emailSender.sendNotificacion(request,IEnlace.PAGO_SUA_LC,beanEmailDetails);

						 }
					 //************************************************
					 //************************************************
				}
				// VSWF - JGAL - SIPARE

		    //MPP181203 IM227985
				else if (((mx.altec.enlace.bo.DatosManc)
		o).getTipo_operacion ().equals ("PROG"))
				{
			if (codError.substring (0, 2).equals ("OK"))
					{
			estatus = "Operaci&oacute;n Ejecutada";
			folio_auto = codError.substring (8, 16);
		    }
			else if(cod_error.equals(MANC0022)) {

				estatus = "Operaci&oacute;n Verificada";
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.equals(MANC0023)) {

				estatus = "Operaci&oacute;n Pre-Autorizada";
				folio_auto = codError.substring (8, 16);
			}
			else if(cod_error.startsWith(MANC)) {
				estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
				folio_auto = "&nbsp;";
			}
					else
					{
			estatus = (codError.length () > 16) ? codError.substring (16) :
			    "Operaci&oacute;n no ejecutada";
			folio_auto = "&nbsp;";
		    }
		//MPP181203 IM227985
		}
				else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PAGT")) {

					if (codError.substring (0, 2).equals ("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.equals(MANC0022)) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.equals(MANC0023)) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.startsWith(MANC)) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
		//Clave Nomina Individual Manc Fase II IOV
				else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PNIS")) {

					if (codError.substring (0, 2).equals ("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
							if (codError.length () > 16)
								folio_auto = codError.substring (8, 16);
							else
								folio_auto = codError.substring (8);
					}
					else if(cod_error.equals(MANC0022)) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.equals(MANC0023)) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.startsWith(MANC)) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
				else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("PNLI")) {

					if (codError.substring (0, 2).equals ("OK")) {

						estatus = "Operaci&oacute;n Ejecutada";
							if (codError.length () > 16)
								folio_auto = codError.substring (8, 16);
							else
								folio_auto = codError.substring (8);
					}
					else if(cod_error.equals(MANC0022)) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.equals(MANC0023)) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.startsWith(MANC)) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
				}
				else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("DIBT")) {
					/*SPID 2016*/
					EIGlobal.mensajePorTrace("AutorizaAMancomunidad - AUTORIZACION MANCOMUNIDAD --> DIBT --> "+codError, EIGlobal.NivelLog.DEBUG);
				    if (codError.substring(0, 2).equals("OK")) {
						estatus = "Operaci&oacute;n Ejecutada";
						folio_auto = codError.substring(8, 16);
					    EIGlobal.mensajePorTrace("\n AUTORIZACION MANCOMUNIDAD SPEI COMPLEMENTO-->"+folio_auto+" MSJ ERROR <"+codError+">", EIGlobal.NivelLog.DEBUG);
						TransferenciaInterbancariaBUS TransInterbancariaBUS=TransferenciaInterbancariaBUS.getInstance();
						TransInterbancariaBUS.registraTransferencia(trama, codError);
				    }
					else if(cod_error.equals(MANC0022)) {

						estatus = "Operaci&oacute;n Verificada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.equals(MANC0023)) {

						estatus = "Operaci&oacute;n Pre-Autorizada";
						folio_auto = codError.substring (8, 16);
					}
					else if(cod_error.startsWith(MANC)) {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}
					else {
						estatus = (codError.length () > 16) ? codError.substring (16) : "Operaci&oacute;n no ejecutada";
						folio_auto = "&nbsp;";
					}

				}
				else if (((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ().equals ("RG03")) //IM314336 - NVS - 08-Abril-04
				{
		    if (codError.substring (0, 2).equals ("OK"))
					{
			estatus = "Operaci&oacute;n Ejecutada";
			folio_auto = codError.substring (8, 16);
		    }
					else
					{
			estatus = (codError.length () > 16) ? codError.substring (16) :
			    "Operaci&oacute;n no ejecutada";
			folio_auto = "&nbsp;";
		    }

		}
				else if (((mx.altec.enlace.bo.DatosManc)
		o).getTipo_operacion ().equals ("CAMA"))
				{
		    if (codError.substring (0, 2).equals ("OK"))
					{
			estatus = "Operaci&oacute;n Ejecutada";
			folio_auto = codError.substring (8, 16);
		    }
					else
					{
			estatus = (codError.length () > 16) ? codError.substring (16) :
			    "Operaci&oacute;n no ejecutada";
			folio_auto = "&nbsp;";
		    }

		}
				else if (((mx.altec.enlace.bo.DatosManc)
		o).getTipo_operacion ().equals ("CPDI"))
				{
		    if (codError.substring (0, 2).equals ("OK"))
					{
			estatus = "Operaci&oacute;n Ejecutada";
			folio_auto = codError.substring (8, 16);
		    }
					else
					{
			estatus = (codError.length () > 16) ? codError.substring (16) :
			    "Operaci&oacute;n no ejecutada";
			folio_auto = "&nbsp;";
		    }

		}
				else
				{
		    if (COD_EXITO.equals (codError.substring (4, 8)) ||
		    codError.substring (0, 2).equals ("OK"))
					{
			if (((mx.altec.enlace.bo.DatosManc)
			o).getTipo_operacion ().equals ("CPVD"))
						{
			    String temp = codError.substring (codError.indexOf ('|') + 1);
			    temp = temp.substring (0, temp.indexOf ('|'));
			    if (temp.equals ("CAB00000")) {
				estatus = "Operaci&oacute;n Ejecutada";
				folio_auto = codError.substring (8, 16);
			    }
							else
							{
				estatus = (codError.length () > 16) ? codError.substring (16) :
				    "Operaci&oacute;n no ejecutada";
				folio_auto = codError.substring (8, 16);
			    }
			}
						else
						{
			    folio_auto = codError.substring (8, 16);
			    estatus = (codError.length () > 16) ? codError.substring (16) :
				"Operaci&oacute;n no ejecutada";
			}
		    }
					else
					{
			folio_auto = codError.substring (8, 16);
			estatus = (codError.length () > 16) ? codError.substring (16) :
			    "Operaci&oacute;n no ejecutada";
		    }
		}
	    }

			//System.out.println ("INDEXOF --->" + (int)porProcesar.indexOf(o));

	    int i = porProcesar.indexOf (o);
			//System.out.println ("*****//////i:"+i);
	    //System.out.println ("Cambiando tipo de objeto");
			//System.out.println ("------------------------ >> fecha autorizacion"+fecha_auto);
	    porProcesar.add(i, new RespManc((mx.altec.enlace.bo.DatosManc) o,
					fecha_auto, folio_auto, estatus, indSipare, refSipare));

	    java.util.ArrayList listaNotificacion = new java.util.ArrayList();
	    try{
	    	Object elemento = "";
	    	String tipoOper = "";
	    	EIGlobal.mensajePorTrace( "***********Antes de lista de elementos a notificar -->", EIGlobal.NivelLog.INFO);
	    	String numRefOper,estatusOper,oper,ctaCargoOper,importeOper;

	    	numRefOper = (folio_auto == null ? "" : folio_auto);
	    	estatusOper = (estatus == null ? "" : estatus);
	    	oper = (((DatosManc)o).getTipo_operacion() == null ? "" : ((DatosManc)o).getTipo_operacion());
	    	ctaCargoOper = (((DatosManc)o).getCta_origen() == null ? "" : ((DatosManc)o).getCta_origen());
	    	importeOper = (((DatosManc)o).getImporte() == null ? "" : ((DatosManc)o).getImporte());
	    	elemento = "/" + cod_error + "/" + numRefOper + "/" + estatusOper + "/" + oper + "/" + ctaCargoOper + "/" + importeOper +  "/";

	    	EIGlobal.mensajePorTrace( "***********Nuevo elemento a notificar ->" + elemento.toString(), EIGlobal.NivelLog.INFO);
    		listaNotificacion = (java.util.ArrayList) request.getSession().getAttribute (DATOS_NOTIF);
    		EIGlobal.mensajePorTrace( "***********Se obtuvo lista de elementos de session ->", EIGlobal.NivelLog.INFO);
    		listaNotificacion.add(elemento);
    		request.getSession().setAttribute (DATOS_NOTIF, listaNotificacion);

			for (int j = 0; j < listaNotificacion.size(); j++) {
				EIGlobal.mensajePorTrace( "***********Lista Actualizada de elementos de Notificacion-->" + listaNotificacion.get(j).toString(), EIGlobal.NivelLog.INFO);
			}

			if (request.getParameter(TIPO_OPER) != null && !request.getParameter(TIPO_OPER).toString().equals("")){
				if (request.getParameter(TIPO_OPER).toString().trim().equals("1")) {
					EIGlobal.mensajePorTrace("*******************se actualizo con Autorizaci---->  ", EIGlobal.NivelLog.INFO);
					tipoOper = "Autorizaci"+ (char)243 +"n";
				} else {
					EIGlobal.mensajePorTrace("*******************se actualizo con Cancelaci---->  ", EIGlobal.NivelLog.INFO);
					tipoOper = "Cancelaci"+ (char)243 +"n";
				}
				request.getSession().setAttribute ("tipoOper", tipoOper);
			}

		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}

	    porProcesar.remove (o);
	    //System.out.println ("Objeto cambiado");
			//System.out.println ("****//////porProcesar.size ()"+porProcesar.size ());
			//System.out.println ("****//////getBandManc() "+ ses.getBandManc ());
//			if (ses.getBandManc() == null)
//			{
				//System.out.println ("Estoy en el if (ses.getBandManc() == null)");
				ses.setBandManc (String.valueOf (porProcesar.size ())) ;
//			}

			String banderaManc = "";
			banderaManc = ses.getBandManc ();


			//System.out.println ("****//////getBandManc ()"+ ses.getBandManc ());
			//System.out.println ("****////// i "+ i);
		//TODO BIT CU5011(Comsulta y AutorizaciOn), BIT CU5031 (Consulta y AutorizaciOn de MInternacional)
		/*VSWF-HGG-I*/
		if (Global.USAR_BITACORAS.equals("ON")){
			BitaHelper bh = new BitaHelperImpl(request, ses, request.getSession(false));
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);

			if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null &&
					request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)
									   .equals(BitaConstants.EA_MANCOM_CONS_AUTO))
				bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_REALIZA_OPERACIONES);

			if(request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO) != null &&
					request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)
									  .equals(BitaConstants.EA_MANCOM_OPER_INTER))
				bt.setNumBit(BitaConstants.EA_MANCOM_OPER_INTER_REALIZA_OPERACIONES);

			if(codError!=null && ((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion() != null){
				if(codError.substring(0,2).equals("OK")){
					bt.setIdErr(((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion() + COD_EXITO);
				}else if(codError.length()>8){
			bt.setIdErr(codError.substring (0, 8));
				}else{
					bt.setIdErr(codError.trim());
				}
			bt.setServTransTux(((mx.altec.enlace.bo.DatosManc)o).getTipo_operacion ());
			}else{
				bt.setIdErr(" ");
			}
			if (request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
				&& ((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
						.equals(BitaConstants.VALIDA)) {
				bt.setIdToken(ses.getToken().getSerialNumber());
			}
			if (ses.getContractNumber() != null) {
				bt.setContrato(ses.getContractNumber().trim());
			}
			/*SPID Vector*/
			bt.setTipoMoneda(divisaAud);
			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		/*VSWF-HGG-F*/
		//VSWF-HGG  -  borra bandera
		request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);


	    if (i + 1 == porProcesar.size ()) {
		request.getSession ().removeAttribute ("Pendientes");
		request.getSession ().removeAttribute (AUTORIZA);
				request.getSession ().removeAttribute ("ses.getBandManc ()");


				try{
					request.getSession ().removeAttribute (DATOS_NOTIF);
					String tipoOperacion ="";
					if (request.getSession().getAttribute ("tipoOper") != null){
						tipoOperacion = request.getSession().getAttribute ("tipoOper").toString();
					}
					request.getSession ().removeAttribute ("tipoOper");
				  EIGlobal.mensajePorTrace("***********Se borro lista de notificacion de session -->", EIGlobal.NivelLog.INFO);

					//BaseResource session = (BaseResource) request.getSession ().getAttribute (SESSION);
                                        session = (BaseResource) request.getSession ().getAttribute (SESSION);
					EmailSender sender = new EmailSender();
					String codErrorOper = "";
					String codErrUnico="";
					double importe = 0.0;
					boolean enviaNotificacion=false;
					int numRegImportados=0;
					String importeTotal = "";
					//obtiene los codigos de error
					for (int j = 1; j < listaNotificacion.size(); j++) {
							codErrUnico= ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 2);
							EIGlobal.mensajePorTrace("----codErrUnico--->" + codErrUnico, EIGlobal.NivelLog.DEBUG);
							codErrorOper=codErrorOper+codErrUnico+ "-" ;

						if(codErrUnico!=null && codErrUnico.contains(COD_EXITO)){
							numRegImportados++;
							enviaNotificacion=true;
						importe = importe + Double.parseDouble(ValidaOTP.cortaCadena(listaNotificacion.get(j).toString(),'/', 7));
						}
					}
					importeTotal = ValidaOTP.formatoNumero(importe);
					EIGlobal.mensajePorTrace("----codErrorOper-->" + codErrorOper, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("----importeTotal-->" + importeTotal, EIGlobal.NivelLog.DEBUG);
					if(enviaNotificacion){
						EmailDetails details = new EmailDetails();

						details.setNumeroContrato(session.getContractNumber());
						details.setRazonSocial(session.getNombreContrato());
						details.setImpTotal(importeTotal);
						details.setImpTotalMXN(importeTotal);
						details.setImpTotalUSD("");
						details.setNumRegImportados(numRegImportados);
						details.setTipo_operacion(tipoOperacion);

						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> num contrato   : " + details.getNumeroContrato(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> nom contrato   : " + details.getRazonSocial(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> setImpTotal    : " + details.getImpTotal(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> num regImp     : " + details.getNumRegImportados(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("*******************AutorizacionCancelacionManc----> tipo operacion     : " + details.getTipo_operacion(), EIGlobal.NivelLog.DEBUG);

						sender.sendNotificacion(request,IEnlace.NOT_AUT_CANC_OPER_MANC, details,listaNotificacion);
					}
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

	    }
	    //System.out.println ("Forward a pagina");
			++i;
			//  response.sendRedirect("/NASApp/" + Global.WEB_APPLICATION + "/jsp/ResMancomunidad.jsp");
			request.setAttribute("banderaManc",ses.getBandManc ());

	    request.getRequestDispatcher ("/jsp/ResMancomunidad.jsp").forward (request, response);
	}

    }

/**
	 * VSWF - JGAL - SIPARE - 08-09-2009
	 *
	 * @param beanValidaciones
	 * @param request
	 * @param response
	 */

	/**
	 * ** Por folio
	 *
	 */

	private void generaComprobanteSUALC(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal.mensajePorTrace("Enlace a SUAConsultaPagoServlet",
				EIGlobal.NivelLog.DEBUG);
		SUAConsultaPagoAction action = new SUAConsultaPagoAction();
		String folioSUA = req.getParameter("folioSUA");
		String lc = req.getParameter("lc");


		SUALCORM4 beanDao = action.anterioresSUALC("", folioSUA, "", "", "", lc, 2);

		if (!beanDao.getCodErr().equals("Operacion Exitosa")){
			EIGlobal.mensajePorTrace("VAO Consulta sin LC -> ", EIGlobal.NivelLog.DEBUG);
			beanDao = action.anterioresSUALC("", folioSUA, "", "", "", "", 2); // VAO por si no funciona la consulta por lc
		}

		List lista = beanDao.getConsulta();
		if (beanDao.getCodErr().equals("Operacion Exitosa")) {
			if (lista.size() > 0) {
				SUALCORM4 detalle = (SUALCORM4) lista.get(0);
				double importeTotal;
				try {

					importeTotal = Double.valueOf(detalle.getImporteTotal());
					importeTotal = (importeTotal / 100);// por los decimales
				} catch (NumberFormatException exception) {
					importeTotal = 0;
				}
				String periodopagMes = null;
				String periodopagAnio = null;
				if (detalle.getPeriodoPago() != null
						&& detalle.getPeriodoPago().length() > 0) {
					try {
						periodopagAnio = detalle.getPeriodoPago().substring(0, 4);
						periodopagMes = detalle.getPeriodoPago().substring(4,
								detalle.getPeriodoPago().length());
					} catch (IndexOutOfBoundsException ibf) {
						periodopagAnio = "";
						periodopagMes = "";
					}
				} else {
					periodopagAnio = "";
					periodopagMes = "";
				}
				SUAGenCompPDF comp;
				comp = new SUAGenCompPDF(session.getContractNumber(), session
						.getNombreContrato(), session.getUserID(), session
						.getNombreUsuario(), detalle.getFolioSua(), detalle
						.getCuentaCargo(), "", detalle.getRegPatronal(),
						periodopagMes, periodopagAnio, detalle.getLinSua(),
						importeTotal, detalle.getNumeroMovimiento(),
						detalle.getTimestamp().substring(0, 10),
						detalle.getTimestamp().substring(11, 19).replace('.', ':'),
						"INTERNET", "EXITO", detalle.getMsjErr());

				pantCompSUA(req, res, comp.generaComprobanteBAOS());

			}else {
				req.setAttribute( MENU_PRINCIPAL, "");
				String encabezado = "Consulta de Bit&aacute;cora hist&oacute;rica";
				req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,"Consultas &gt; Bit&aacute;cora","s25320h",req) );
				req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate( IEnlace.ERROR_TMPL, req, res );
			}
		}else {
			req.setAttribute( MENU_PRINCIPAL, "");
			String encabezado = "Consulta de Bit&aacute;cora hist&oacute;rica";
			req.setAttribute( ENCABEZADO, CreaEncabezado(encabezado,"Consultas &gt; Bit&aacute;cora","s25320h",req) );
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}

	}

	/**
	 * Generacion del comprobante PDF
	 * @param req
	 * @param res
	 * @param archivo
	 * @throws ServletException
	 * @throws IOException
	 */
	private void  pantCompSUA(HttpServletRequest req, HttpServletResponse res, ByteArrayOutputStream archivo) throws ServletException, IOException
	{

		res.setHeader("Expires", "0");
		res.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		res.setHeader("Pragma", "public");
		// setting the content type
		res.setContentType("application/pdf");
		// the contentlength is needed for MSIE!!!
		res.setContentLength(archivo.size());
		// write ByteArrayOutputStream to the ServletOutputStream
		ServletOutputStream out = res.getOutputStream();
		archivo.writeTo(out);
		out.flush();
		out.close();

	}

	void grabaBita(SUALCLZS8 bo,BaseResource bs,String importe, HttpServletRequest req){
		EIGlobal.mensajePorTrace("-->AutorizaAMancomunidad - grabaBita - " +
				"Llama bitacora: " ,EIGlobal.NivelLog.DEBUG);
		try{
		if(bo!=null){
				if(bo.getMsjErr().equals("Operacion Exitosa")){
				double imp=0;
				try{
					imp=Double.parseDouble(importe);
				}catch(NumberFormatException e){
					imp=0.0;
				}catch (Exception e){
					imp=0.0;
				}
					if (bo.getCodErr().length() > 13){
						if (bo.getCodErr().toUpperCase().equals("OPERACION EXITOSA")) {
							bo.setCodErr("SULC0000");
						} else {
							bo.setCodErr(bo.getCodErr().substring(0, 13));
						}
					}
				EIGlobal.mensajePorTrace(obten_referencia_operacion
						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)) +" - "+
							(bo.getCodErr()!=null?bo.getCodErr():"")+" - "+
						bs.getContractNumber()!=null?bs.getContractNumber():""+" - "+
							bs.getUserID8()!=null?bs.getUserID8():""+" - "+
						(bo.getCuentaCargo()!=null?bo.getCuentaCargo():"")+" - "+
						imp+" -" + ES_PAGO_SUA_LINEA_CAPTURA ,EIGlobal.NivelLog.DEBUG);
				if(bo.getFolioSua()==null || bo.getFolioSua().length()==0){
					bo.setFolioSua("0");
				}

		    	    BitaTCTBean beanTCT = new BitaTCTBean ();
		  			BitaHelper bh =	new BitaHelperImpl(req,bs,req.getSession(false));

		  			beanTCT = bh.llenarBeanTCT(beanTCT);
		  			beanTCT.setReferencia(obten_referencia_operacion
							(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
		  			beanTCT.setCodError(bo.getCodErr()!=null?bo.getCodErr():"");
		  			beanTCT.setUsuario(bs.getUserID8()!=null?bs.getUserID8():"");
		  			beanTCT.setCuentaOrigen(bo.getCuentaCargo()!=null?bo.getCuentaCargo():"");
		  			beanTCT.setImporte(imp);
					beanTCT.setTipoOperacion(ES_PAGO_SUA_LINEA_CAPTURA);
					beanTCT.setProteccion1(Long.parseLong(bo.getFolioSua()));
					beanTCT.setConcepto(bo.getLineaCap()!=null?bo.getLineaCap():""); //VAO 12-09-2012
				    BitaHandler.getInstance().insertBitaTCT(beanTCT);

					//EIGlobal.mensajePorTrace("-Bitacora-:" + llam,EIGlobal.NivelLog.DEBUG);
				}else{
					EIGlobal.mensajePorTrace("No se guarda bitacora operacion erronea: " + bo.getCodErr() ,EIGlobal.NivelLog.DEBUG);
				}
		}
			}catch(SQLException sqle){
				EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	}

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException java.io.IOException
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException ServletException
     * @throws java.io.IOException
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     * @return String servlet info
     */
    public String getServletInfo () {
	return "Short description";
    }

}