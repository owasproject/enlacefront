package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CuentasBO;
import mx.altec.enlace.bo.DatosBeneficiarioBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


import java.sql.*;

public class cuentasSerfinSantander extends  BaseServlet
{
	/**
	 * Holds the value of property MDisposicion_Cargo 
	 * */
	private static final String MDisposicion_Cargo = "43@";
	/**
	 * Holds the value of property MDisposicion_Abono 
	 * */
	private static final String MDisposicion_Abono = "44@";
	/** constante para cadTMP */
	private static final String CAD_TMP = "cadTMP";
	/** constante para LineasCredito */
	private static final String LINEAS_CREDITO = "LineasCredito";
	/** constante para tramaok */
	private static final String TRAMA_OK = "tramaok";
	/** constante para contadorok */
	private static final String CONTADOR_OK = "contadorok";
	/** constante para resultado */
	private static final String RESULTADO = "resultado";
	/** constante para tramadicional */
	private static final String TRAMADICIONAL = "tramadicional";
	
	/**
	 * M�todo que responde a peticiones GET
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	/**
	 * M�todo que responde a peticiones POST
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	/**
	 * M�todo que responde a peticiones la peticion que valida la sesion del usuario
	 * @param request instancia de HTTPServletRequest
	 * @param response instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return expresa un valor booleano con la autenticaci&oacute;n del usuario
	 * */
	public boolean SesionValida( HttpServletRequest request, HttpServletResponse response )	throws ServletException, java.io.IOException
	{
		HttpSession sess  = request.getSession( false );
		boolean resultado = false;
		String remotehost = request.getRemoteHost();
		String servername = request.getServerName();
		String port       = String.valueOf(request.getServerPort());
		String serversofw = getServletContext().getServerInfo();
        String ip         = request.getRemoteAddr();
		String protocolo  = Global.PROTOCOLO;
		String usuario;

        EIGlobal.mensajePorTrace("***cuentasSerfinSantander remotehost ["+ remotehost +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***cuentasSerfinSantander port       ["+ port       +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***cuentasSerfinSantander servername ["+ servername +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***cuentasSerfinSantander serversofw ["+ serversofw +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***cuentasSerfinSantander ip		   ["+ ip         +"]", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***cuentasSerfinSantander protocolo  ["+ protocolo  +"]", EIGlobal.NivelLog.DEBUG);
        usuario = (String)request.getHeader("IV-USER");
        

		if( sess == null )
		{
	        request.setAttribute("web_application",Global.WEB_APPLICATION);
			request.setAttribute("host",protocolo+"://"+remotehost);
			
			if(null != usuario && !"".equals(usuario)){				
				cierraDuplicidad(usuario);					
			}
			
			evalTemplate("/jsp/sessionfuera.jsp", request, response );
			
			
		}
		else
		{
            BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
			if( null == session || null == session.getUserID8() || "".equals(session.getUserID8()) )
			{
	            request.setAttribute("web_application",Global.WEB_APPLICATION);
				request.setAttribute("host",protocolo+"://"+remotehost);
				
				if(null != usuario && !"".equals(usuario)){
					sess.invalidate();
					cierraDuplicidad(usuario);					
				}
			    evalTemplate("/jsp/sessionfuera.jsp", request, response );
			}
			else
			{
				resultado = true;
			}
		}
		return resultado;
	}

	/**
	 * M�todo que realiza por default que define la ventana de carga de cuentas que se mostrar�
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		boolean sesionvalida     = SesionValida(req, res);
		String  moduloconsultar  = "";

		EIGlobal.mensajePorTrace("$$$$$$$$$$$$$$$ Version Stf.",EIGlobal.NivelLog.DEBUG);

		if(sesionvalida) {
			HttpSession sess = req.getSession();
            BaseResource session = (BaseResource) sess.getAttribute("session");
			String ventana   = req.getParameter("Ventana");
			String crialtair = req.getParameter("ctasantander");
			String crihogan  = req.getParameter("ctaserfin");
			String cridesc   = req.getParameter("descrip");
			String opcion    = req.getParameter("opcion");

			if( crialtair == null     )	{crialtair = " ";}
			if( crihogan  == null     )	{crihogan  = " ";}
			if( cridesc   == null     )	{cridesc   = " ";}
			if( crialtair.length()==0 )	{crialtair = " ";}
			if( crihogan.length() ==0 )	{crihogan  = " ";}
			if( cridesc.length()  ==0 )	{cridesc   = " ";}

			// Stefanini*200820500*25/03/2009*JAL**I
			//Se obtiene este parametro para el modulo de mancomunidad, ya que en base
			//al Tipo de Operacion se muestran las cuentas
			if (getFormParameter(req, "tipoOp") != null) {
			String tipoOperacion = getFormParameter(req, "tipoOp");
                if("CFRP".equals(tipoOperacion) || "FXIN".equals(tipoOperacion)){
                    session.setModuloConsultar("35@36@");
               }else if("FXMB".equals(tipoOperacion) ){
                    session.setModuloConsultar("38@");
               }else {
            	   session.setModuloConsultar("22@");
               }

			}
			// Stefanini*200820500*25/03/2009*JAL**F

			EIGlobal.mensajePorTrace("***cuentasSerfinSantander Ventana ["+ventana+"]", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("***cuentasSerfinSantander opcion  ["+opcion +"]", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("***cuentasSerfinSantander Modulo  ["+ session.getModuloConsultar()+"]", EIGlobal.NivelLog.DEBUG);

			moduloconsultar = session.getModuloConsultar(); // Se obtiene la clave del modulo en sesion

			if ( null == moduloconsultar || "4".equals(opcion) ){
				moduloconsultar=IEnlace.MMant_gral_ctas;
				opcion="4";
			}

			if ( IEnlace.MCargo_transf_pesos.equals(moduloconsultar)     && "2".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_transf_pesos;
			}
			else if( IEnlace.MCargo_transf_dolar.equals(moduloconsultar) && "2".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_transf_dolar;
			}
			else if( IEnlace.MCargo_transf_pesos.equals(moduloconsultar) && "3".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_tarjeta_cred;
			}
			else if( IEnlace.MDep_inter_cargo.equals(moduloconsultar)    && "2".equals(opcion)){
				moduloconsultar=IEnlace.MDep_Inter_Abono;
			}
			else if( IEnlace.MMant_gral_ctas.equals(moduloconsultar)     && "2".equals(opcion)){
				moduloconsultar=IEnlace.MCons_Baja_Cta;
			}
			else if( IEnlace.MMant_gral_ctas.equals(moduloconsultar)     && "3".equals(opcion)){
			    moduloconsultar=IEnlace.MAbono_TI;
			}
			else if( IEnlace.MCargo_cambio_pesos.equals(moduloconsultar) && "1".equals(opcion)){
				moduloconsultar=IEnlace.MCargo_cambio_pesos+IEnlace.MCargo_cambio_dolar;
			}
			else if( IEnlace.MCargo_cambio_pesos.equals(moduloconsultar) && "2".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_cambios_pesos+IEnlace.MAbono_cambio_dolar;
			}
			else if( IEnlace.MCargo_TI_pes_dolar.equals(moduloconsultar) && "2".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_TI;
			}
			/**
			 * Validaci�n para ventana de muestra de cuentas para m�vil
			 * */
			else if( IEnlace.MMant_gral_ctas.equals(moduloconsultar) && "5".equals(opcion)){
				moduloconsultar=IEnlace.MCons_Ctas_movil;
			}
			else if( MDisposicion_Cargo.equals(moduloconsultar) && "3".equals(opcion)){
				moduloconsultar=MDisposicion_Abono;
			}
			else if( IEnlace.MAltaProveedor_abono.equals(moduloconsultar) && "1".equals(opcion)){
				moduloconsultar=IEnlace.MAbono_transf_pesos;
			}
			else if ( IEnlace.MAltaProveedor_abono.equals(moduloconsultar) && "2".equals(opcion)){
				moduloconsultar=IEnlace.MDep_Inter_Abono;
			}
			EIGlobal.mensajePorTrace("***cuentasSerfinSantander moduloconsultar  "+ moduloconsultar, EIGlobal.NivelLog.DEBUG);

			if( "0".equals(ventana) ) {
				pant_inicial_filtro(req,res,moduloconsultar);
			}
			else if( "1".equals(ventana) ) {
				pant_resultado_consulta(req,res,moduloconsultar,crialtair,crihogan,cridesc,opcion);
			}
			else if( "2".equals(ventana) ) {
				pant_cuentas(req,res,opcion);
			}
			else if( "3".equals(ventana) ) {
					EIGlobal.mensajePorTrace("ENCONTRANDO OPCION 3 CON EL MODULO"+moduloconsultar, EIGlobal.NivelLog.DEBUG);

					if( moduloconsultar.equals(IEnlace.MMant_estruc_B0) ) {
						if ( consLineaCredito(req, res) ) {
							EIGlobal.mensajePorTrace("ENCONTRE LINEAS, SALIENDO defaultAction", EIGlobal.NivelLog.DEBUG);
							evalTemplate("/jsp/framecuentas.jsp",req,res);
						}
					}
					else {
						EIGlobal.mensajePorTrace("NO HAY LINEAS U OPCION NO VALIDA, SALIENDO defaultAction", EIGlobal.NivelLog.DEBUG);
						req.setAttribute ("LINEAS_CRE","");
				        req.setAttribute ("PR_LLAMADO","1");
						req.setAttribute (CAD_TMP,req.getParameter(CAD_TMP));
						//INDRA-SWIFT-P022574-ini
						EIGlobal.mensajePorTrace("OPCION"+opcion, EIGlobal.NivelLog.DEBUG);
						if ("35@36@".equals(moduloconsultar) && "66".equals(opcion)){
							DatosBeneficiarioBO beneficiarioBO = new DatosBeneficiarioBO();
							String trama = req.getParameter(CAD_TMP);

							EIGlobal.mensajePorTrace("cuenta"+trama.substring(0,trama.indexOf('|')), EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("contrato"+session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
							
							DatosBeneficiarioBean beneficiarioBean = beneficiarioBO
								.consultarDatosBenef(session.getContractNumber(),
										trama.substring(0,trama.indexOf('|'))); 
							
							if ("".equals(beneficiarioBean.getDireccion()) || "".equals(beneficiarioBean.getCiudad()) ||
									"".equals(beneficiarioBean.getIdCliente()) || "".equals(beneficiarioBean.getCvePaisBenef())){
								req.setAttribute("dirBenef", "true");
							}else{
								req.setAttribute("dirBenef", "false");
							}
							EIGlobal.mensajePorTrace("dirBenef: "+req.getAttribute("dirBenef"), EIGlobal.NivelLog.DEBUG);
						}
						//INDRA-SWIFT-P022574-fin
						evalTemplate("/jsp/framecuentas.jsp",req,res);
					}
			}
		}
	}

	/************************************************************************************************
	*
	* Nombre: consLineaCredito: Consulta de linea de credito por cuenta
	* Autor : mtto getronics (LASR)
	* Fecha : 15/04/2005
	* @param req instancia de HTTPServletRequest
	* @param res instancia de HTTPServletResponse
	* @throws ServletException error en el servlet
	* @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	* @return regresa un booleano expresando si la consulta fue exitosa
	*
	*************************************************************************************************/
	public boolean consLineaCredito( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		HttpSession sess        = req.getSession();
        BaseResource session    = (BaseResource) sess.getAttribute("session");

		String lineasCredito    = null;
		Hashtable cuentas       = null;

		String ctaSRC = req.getParameter("ctaSRC");


		EIGlobal.mensajePorTrace("***Entrando a CONSLINEACREDITO***", EIGlobal.NivelLog.DEBUG);



			try
			{


				EIGlobal.mensajePorTrace("consLineaCredito-->cuentaSS= "+ctaSRC, EIGlobal.NivelLog.DEBUG);


				EIGlobal.mensajePorTrace("***EXTRAE LINEAS DE CREDITO***", EIGlobal.NivelLog.DEBUG);

				if(creayCopiaArchivo(session.getUserID8(),ctaSRC))
				{

					cuentas       = consultaLineasCredito(req,ctaSRC.trim() );
					lineasCredito = (String)cuentas.get(ctaSRC);

				}


				if(lineasCredito == null)
				{
					lineasCredito = "";
				}
				//CSA Se modifica para proporcionar el contrato en lugar del usuario
				lineasCredito+= traeCuentasPropias(session.getContractNumber());

				EIGlobal.mensajePorTrace("LAS LINEAS Y LAS LINEAS PROPIAS SON:"+lineasCredito, EIGlobal.NivelLog.DEBUG);


				 req.setAttribute ("LINEAS_CRE",(String)lineasCredito);
				 req.setAttribute ("PR_LLAMADO","1");
				 req.setAttribute (CAD_TMP,req.getParameter(CAD_TMP));

				cuentas.clear();
				cuentas = null;
				return true;


			}
			catch ( Exception e )
			{
				EIGlobal.mensajePorTrace( "consLineaCredito-->Error al traer lineas de credito" + e.toString(), EIGlobal.NivelLog.ERROR );
				//e.printStackTrace();
				cuentas.clear();
				cuentas = null;
				return false;
			}


	}

	/**
	 * M�todo que realiza el seteo de valores en el frame para la llamada al mismo que muestra las cuentas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @param opcion instancia que indica la opcion que sera mostrada en la ventana
	 * @return bandera que indica el exito de la operaci�n
	 * */
	private int  pant_cuentas(HttpServletRequest req, HttpServletResponse res,String opcion) throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & pant_cuentas", EIGlobal.NivelLog.DEBUG);
		StringBuffer codigo=new StringBuffer();
		String path_web=Global.WEB_APPLICATION;

		// Modificaci�n para l�neas de cr�dito
		boolean lineasCredito = (req.getParameter(LINEAS_CREDITO) != null);
		if (lineasCredito){
			req.setAttribute(LINEAS_CREDITO,req.getParameter(LINEAS_CREDITO));
		}

		//<vswf:meg cambio de NASApp por Enlace 08122008>
		codigo.append("<FRAME SRC=\"/Enlace/").append(path_web).append("/cuentasSerfinSantander?Ventana=0&opcion=").append(opcion).append(((lineasCredito)?"&LineasCredito=1":"")).append("\" NAME=\"framefiltro\"  SCROLLING=\"NO\"   marginheight=\"0\" marginwidth=\"0\">");
		codigo.append("<FRAME SRC=\"/Enlace/").append(path_web).append("/cuentasSerfinSantander?Ventana=1&opcion=").append(opcion).append(((lineasCredito)?"&LineasCredito=1":"")).append("\" NAME=\"framecuentas\" SCROLLING=\"AUTO\" marginheight=\"0\" marginwidth=\"0\">");
	    codigo.append("<FRAME SRC=\"/Enlace/").append(path_web).append("/jsp/framebotones.jsp\" NAME=\"framebotones\"   SCROLLING=\"NO\" marginheight=\"0\" marginwidth=\"0\">");

		req.setAttribute("codigo",codigo.toString());
		evalTemplate("/jsp/pantcuentas.jsp",req,res);
		return 1;
	}

	/**
	 * M�todo que realiza la colocacion de los t�tulos del frame que muestra las cuentas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @param modulo instancia que indica el modulo que sera consultado
	 * @return bandera que indica el exito de la operaci�n
	 * */
	private int  pant_inicial_filtro(HttpServletRequest req, HttpServletResponse res,String modulo) throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & pant_inicial_filtro", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & modulo:"+modulo, EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		// Modificaci�n para l�neas de cr�dito
		boolean lineasCredito = (req.getParameter(LINEAS_CREDITO) != null);
		if (lineasCredito){
			req.setAttribute(LINEAS_CREDITO,req.getParameter(LINEAS_CREDITO));
		}
		if( modulo.equals(IEnlace.MDep_Inter_Abono)|| modulo.equals(IEnlace.MCons_Baja_Cta))
		{
			req.setAttribute("titulo","<td colspan=2 class=titpag>Cuentas de Otros Bancos </td>");
			req.setAttribute("textboxserfin","");
			req.setAttribute("textboxsantander","<td  class=tabtexcal>Cuenta</td><td  class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
		}
        else if( modulo.equals(IEnlace.MAbono_TI) )
		{
			  req.setAttribute("titulo","<td colspan=2 class=titpag>Cuentas Internacionales</td>");
			  req.setAttribute("textboxsantander","<td  class=tabtexcal>Cuenta</td><td  class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
			  req.setAttribute("textboxserfin","");
		}
	    /**************************************************************************************************************/
	    /* Proyecto Unico  */
		req.setAttribute("titulo", "<td colspan=2 class=titpag>Cuentas </td>");
		req.setAttribute("textboxsantander", "<td  class=tabtexcal>Cuenta</td><td  class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
		req.setAttribute("textboxserfin", "");
		/**************************************************************************************************************/

		evalTemplate("/jsp/framefiltro.jsp",req,res);
		return 1;
	}

	/**
	 * M�todo que crea el archivo con la trama que sera enviada al TUX para recibir la trama con los datos de las cuentas consultadas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @param modulo instancia que indica el modulo que sera consultado
	 * @param caltair ceunta altair
	 * @param chogan cuenta hogan
	 * @param cdescrip descripcion de la cuenta
	 * @param opcion instancia que indica la opcion que sera mostrada en la ventana
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera que indica el exito de la operaci�n
	 * */
	private int  pant_resultado_consulta(HttpServletRequest req, HttpServletResponse res, String modulo, String caltair, String chogan,String cdescrip, String opcion) throws ServletException, IOException
	{
		HttpSession sess     = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		String archivo = session.getUserID8() + ".ctas";

		// Modificaci�n para l�neas de cr�dito
		boolean lineasCredito = (req.getParameter(LINEAS_CREDITO) != null);
		if(lineasCredito){
			req.setAttribute(LINEAS_CREDITO,req.getParameter(LINEAS_CREDITO));
		}
		try{
			//if (llamado_servicioCtasInteg(modulo,caltair,chogan,cdescrip,archivo,req)==true) {
			if (llamado_servicio50CtasInteg(modulo,caltair,chogan,cdescrip,archivo,req)) {
				lectura_cuentas_para_pantalla(req,res,modulo,archivo, opcion);
			}
			else {
				req.setAttribute("MensajeError", "cuadroDialogo(\"Error al obtener cuentas\", 3); ");
				req.setAttribute(TRAMA_OK,"");
				req.setAttribute(CONTADOR_OK,"");
				req.setAttribute(RESULTADO,"");
				req.setAttribute(TRAMADICIONAL,"");
				evalTemplate("/jsp/framecuentas.jsp",req,res);
			}
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("cuentasSerfinSantander::pant_resultado_consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
		               				+ "Contrato->" + session.getContractNumber ()
		               				+ "Usuario->" + session.getUserID8()
		               				+ "PerfilUsuario->" + session.getUserProfile()
		               				+ "Archivo->" + IEnlace.LOCAL_TMP_DIR + "/" + archivo
		               				+ "Modulo->" + modulo
		               				+ "caltair->" + caltair
		               				+ "chogan->" + chogan
		               				+ "cdescrip->" + cdescrip
		               				+ "<RASTRO ERROR>"
						 			+ "Linea de truene->" + lineaError[0]
						 			+ "Siguiente nivel 1->" + lineaError[1]
						 	        + "Siguiente nivel 2->" + lineaError[2]
						 			, EIGlobal.NivelLog.ERROR);

		}
		return 1;
	}

	/**
	 * M�todo que crea el archivo con la trama que sera enviada al TUX para recibir la trama con los datos de las cuentas consultadas
	 * @param req instancia de HTTPServletRequest
	 * @param cta cuenta que se le env�a al metodo
	 * @return regresa un hashtable con las cuentas consultadas
	 * */
	private Hashtable consultaLineasCredito(HttpServletRequest req,String cta)
	{
		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		Hashtable cuentas = new Hashtable();
		Hashtable ht;
		String trama, error;
		File archivo;
		BufferedReader entrada=null;

		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		try
		{

			// - Obtener contrato, usuario, etc
			EIGlobal.mensajePorTrace("Trama para traer lineas de credito", EIGlobal.NivelLog.DEBUG);
			trama = "2EWEB|" + session.getUserID8() + "|B0AM|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
			EIGlobal.mensajePorTrace("Trama: "+trama, EIGlobal.NivelLog.INFO);

			// - Realizar consulta
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama); //cuentas_modulo
			trama = (String)ht.get("BUFFER");
			error = (String)ht.get("COD_ERROR");
			ht.clear();
			ht = null;
			tuxedo = null;

			EIGlobal.mensajePorTrace("regreso el servicio: "+trama, EIGlobal.NivelLog.INFO);

			// - Hacer copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());

			if(archRemoto.copia(trama))
			{
				EIGlobal.mensajePorTrace("<cuentasLineasCredito--> Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.DEBUG);
				trama = Global.DIRECTORIO_LOCAL + "/" +trama;
			}
			else
			{
				EIGlobal.mensajePorTrace("<cuentasLineasCredito--> Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.ERROR);
			}

			// - Consultar archivo
			EIGlobal.mensajePorTrace("Se abre el archivo: "+trama, EIGlobal.NivelLog.DEBUG);
			archivo = new File(trama);
			entrada = new BufferedReader(new FileReader(archivo));

			// - Armar y retornar el hastable
			trama = entrada.readLine();
			EIGlobal.mensajePorTrace("<cuentasLineasCredito--> cabecera de archivo = " + trama, EIGlobal.NivelLog.DEBUG);

			if( !"OK".equals(trama.substring(0,2)) ){
				throw new Exception("error en el formato de las cuentas");
			}
			while( (trama=entrada.readLine()) != null)
			{

				 EIGlobal.mensajePorTrace("Se lee del archivo: "+trama, EIGlobal.NivelLog.DEBUG);
				 cuentas.put(cta,trama);
			}
		}
		catch(Exception e)
		{
			EIGlobal.mensajePorTrace("Error en consultaLineasCredito: " + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
        finally
		{
           try
		   {
               entrada.close();
               entrada = null;
 		   }
           catch(Exception e)
		   {
			    //e.printStackTrace();
           }
		}
        return cuentas;
	}

	/**
	 * M�todo que realiza la lectura de las cuentas preparandolas para su presentaci&oacute;n en pantalla
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @param modulo instancia que indica el modulo que sera consultado
	 * @param archivo archivo generado previamente con la trama de las cuentas que se van a imprimir en pantalla
	 * @param opcion instancia que indica la opcion que sera mostrada en la ventana
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * @return bandera que indica el exito de la operaci�n
	 * */
    private int lectura_cuentas_para_pantalla(HttpServletRequest req, HttpServletResponse res, String modulo, String archivo, String opcion) throws ServletException, IOException
    {
   		EIGlobal.mensajePorTrace("***Entrando a  lectura_cuentas ", EIGlobal.NivelLog.DEBUG);

		BufferedReader entrada = null;
        StringBuffer resultado = new StringBuffer();
		String linea     = "";
		int separadores  = 0;
		boolean continua = true;
		int contador     = 1;
		String clase     = "";
		StringBuffer trama = new StringBuffer();
        String datos[]   = null;
        boolean sigue    = true;
		String contratos_inversion       = "";
		HttpSession  sess    = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		try
		{
		    entrada = new BufferedReader( new FileReader( IEnlace.LOCAL_TMP_DIR + "/" + archivo));

		    if( modulo.equals(IEnlace.MDep_Inter_Abono) ||modulo.equals(IEnlace.MCons_Baja_Cta)){
   				resultado.append("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
		    }
			else if( modulo.equals(IEnlace.MAbono_TI) ){
				resultado.append("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Divisa</TD><TD class=tittabdat align=center >Beneficiario</TD></tr>");
			}
		    /**
		     * Construcci�n del encabezado de la tabla para mostrar cuentas m�viles
		     * */
			else if( modulo.equals(IEnlace.MCons_Ctas_movil)){
				resultado.append("<TR><TD class=tittabdat align=center >Cuenta/M&oacute;vil</TD><TD class=tittabdat align=center>Descripci&oacute;n</TD></TR>");
			}
			/**************************************************************************************************************/
			/* Proyecto Unico  */
			/*
			else if( !session.getClaveBanco().equals(Global.CLAVE_SANTANDER) )
		    {
			   if( modulo.equals(IEnlace.MAbono_tarjeta_cred) )
   			       resultado.append("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
               else
				   resultado.append("<TR><TD class=tittabdat align=center >Cuenta Nueva</TD><TD class=tittabdat align=center >Cuenta Anterior</TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
            }
            else
   		        resultado.append("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
			*/
			else{
			  resultado.append("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
			}
			/**************************************************************************************************************/


			/****************** 50 */
			EIGlobal.mensajePorTrace("*** El parametro es:"+modulo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("*** Se compara contra" + IEnlace.MDep_Inter_Abono , EIGlobal.NivelLog.DEBUG);
			boolean facCargo50=false;
			facCargo50=(session.getFacultad("DEPSINCAR"))?true:false;
			EIGlobal.mensajePorTrace("MDI_Interbancario - execute(): Facultad DEPSINCAR: "+facCargo50, EIGlobal.NivelLog.DEBUG);
			if(modulo.equals(IEnlace.MDep_inter_cargo) && facCargo50)
			 {
			   EIGlobal.mensajePorTrace("*** Entrando a la parte $50 ", EIGlobal.NivelLog.DEBUG);

			    /**************************************************************************************************************/
				/* Proyecto Unico  */
				/*
			    if(!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
				 trama.append(contador).append("|SIN CUENTA|datos[2]|SIN TITULAR|datos[4]|datos[5]|datos[6]|1|P@");
			    else
				 trama.append(contador).append("|SIN CUENTA| |SIN TITULAR|datos[4]|datos[5]|datos[6]|1|P@");
			    */
				trama.append(contador).append("|SIN CUENTA| |SIN TITULAR|datos[4]|datos[5]|datos[6]|1|P@");
				/**************************************************************************************************************/

			   resultado.append("<TR><td class=textabdatobs align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,").append(contador).append(");\">SIN CUENTA </A></TD>");
			   resultado.append("<td class=textabdatobs align=left > SIN TITULAR </td></tr>");
			   contador++;
			 }
			/***********************/

			while ( (linea = entrada.readLine()) != null )
			{
				separadores = cuentaseparadores(linea);
				continua=true;

				if( modulo.equals(IEnlace.MEnvio_pago_imp) )
                {
					if (separadores!=17) {continua=false;}
				}
				else if( modulo.equals(IEnlace.MDep_Inter_Abono)|| modulo.equals(IEnlace.MCons_Baja_Cta) )
				{
					if(separadores!=5) {continua=false;}
				}
                else if( separadores!=8 ){
					continua=false;
                }
                if(continua)
                {
					datos = desentramaC(Integer.toString(separadores)+"@"+linea,'@');

                    if( (contador % 2)==0 ){
                        clase="textabdatcla";
                    }
		            else{
                        clase="textabdatobs";
		            }
					if (!(modulo.equals(IEnlace.MDep_Inter_Abono))&&
						!(modulo.equals(IEnlace.MEnvio_pago_imp))&&
					    !(modulo.equals(IEnlace.MAbono_TI))&&
					    !(modulo.equals(IEnlace.MCons_Ctas_movil))&&
						!(modulo.equals(IEnlace.MCons_Baja_Cta)))
					{
						sigue=true;

                        if(sigue)
                        {
						  /**************************************************************************************************************/
						  /* Proyecto Unico  */
						  /*
						  if( !session.getClaveBanco().equals(Global.CLAVE_SANTANDER) )
								trama.append(contador).append("|").append(datos[1]).append("|").append(datos[2]).append("|").append(datos[3]).append("|").append(datos[4]).append("|").append(datos[5]).append("|").append(datos[6]).append("|").append(datos[7]).append("|").append(datos[8]).append("@");
				          else
								trama.append(contador).append("|").append(datos[1]).append("| |").append(datos[3]).append("|").append(datos[4]).append("|").append(datos[5]).append("|").append(datos[6]).append("|").append(datos[7]).append("|").append(datos[8]).append("@");
						  */
						  trama.append(contador).append('|').append(datos[1]).append('|').append('|').append(datos[3]).append('|').append(datos[4]).append('|').append(datos[5]).append('|').append(datos[6]).append('|').append(datos[7]).append('|').append(datos[8]).append('@');
						  /**************************************************************************************************************/

					      if( IEnlace.MMant_gral_ctas.equals(modulo) && "4".equals(opcion) ){
							 resultado.append("<TR><td class=").append(clase).append(" align=left >").append(datos[1]).append("</TD>");
					      }
                          else{
							 resultado.append("<TR><td class=").append(clase).append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,").append(contador).append(");\">").append(datos[1]).append("</A></TD>");
                          }

						  /**************************************************************************************************************/
						  /* Proyecto Unico  */
						  /*
						  if( !session.getClaveBanco().equals(Global.CLAVE_SANTANDER) )
						  {
							  if( !modulo.equals(IEnlace.MAbono_tarjeta_cred) )
                              {
							    if( datos[2].equals("") )
							        resultado.append("<td class=").append(clase).append(" align=left >&nbsp;&nbsp;</td>");
							    else
							        resultado.append("<td class=").append(clase).append(" align=left >").append(datos[2]).append("</td>");
                              }
                          }
						  */
						  /**************************************************************************************************************/

					  	  if( "".equals(datos[3]) ){
			                  resultado.append("<td class=").append(clase).append(" align=left >&nbsp;</td></tr>");
					  	  }
			              else{
			                  resultado.append("<td class=").append(clase).append(" align=left >").append(datos[3]).append("</td></tr>");
			              }
                           EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.DEBUG);
					       contador++;
						}

                    }
					else if( modulo.equals(IEnlace.MEnvio_pago_imp) )
                    {
						/**************************************************************************************************************/
						/* Proyecto Unico  */
						/*
						if( !session.getClaveBanco().equals(Global.CLAVE_SANTANDER) )
						  trama.append(contador).append("|").append(datos[1]).append("|").append(datos[2]).append("|").append(datos[3]).append("|").append(datos[4]).append("|").append(datos[5]).append("|").append(datos[6]).append("|").append(datos[7]).append("|").append(datos[8]).append(";").append(datos[9]).append(";").append(datos[10]).append(";").append(datos[11]).append(";").append(datos[12]).append(";").append(datos[13]).append(";").append(datos[14]).append(";").append(datos[15]).append(";").append(datos[16]).append(";").append(datos[17]).append(";@");
				        else
						  trama.append(contador).append("|").append(datos[1]).append("| |").append(datos[3]).append("|").append(datos[4]).append("|").append(datos[5]).append("|").append(datos[6]).append("|").append(datos[7]).append("|").append(datos[8]).append(";").append(datos[9]).append(";").append(datos[10]).append(";").append(datos[11]).append(";").append(datos[12]).append(";").append(datos[13]).append(";").append(datos[14]).append(";").append(datos[15]).append(";").append(datos[16]).append(";").append(datos[17]).append(";@");
						*/
						trama.append(contador).append('|').append(datos[1]).append('|').append('|').append(datos[3]).append('|').append(datos[4]).append('|').append(datos[5]).append('|').append(datos[6]).append('|').append(datos[7]).append('|').append(datos[8]).append(';').append(datos[9]).append(';').append(datos[10]).append(';').append(datos[11]).append(';').append(datos[12]).append(';').append(datos[13]).append(';').append(datos[14]).append(';').append(datos[15]).append(';').append(datos[16]).append(';').append(datos[17]).append(';').append('@');
						/**************************************************************************************************************/

						resultado.append("<TR><td class=").append(clase).append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,"+contador+");\">").append(datos[1]).append("</A></TD>");

						/**************************************************************************************************************/
						/* Proyecto Unico  */
						/*
						if( !session.getClaveBanco().equals(Global.CLAVE_SANTANDER) )
			              resultado.append("<td class=").append(clase).append(" align=left >").append(datos[2]).append("</td>");
						*/
						/**************************************************************************************************************/

						if( "".equals(datos[3]) ){
			                resultado.append("<td class=").append(clase).append(" align=left >&nbsp;</td></tr>");
						}
			            else{
			                resultado.append("<td class=").append(clase).append(" align=left >").append(datos[3]).append("</td></tr>");
			            }

                        EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.DEBUG);
					    contador++;
					}
					else if( modulo.equals(IEnlace.MDep_Inter_Abono) || modulo.equals(IEnlace.MCons_Baja_Cta))
                    {
						trama.append(contador).append('|').append(datos[1]).append('|').append(datos[2]).append('|').append(datos[3]).append('|').append(datos[4]).append('|').append(datos[5]).append('|').append(traeBanco(datos[2])).append('|').append(traePlaza(datos[4])).append('|').append('@');
					    resultado.append("<TR><td class=").append(clase).append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,").append(contador).append(");\">").append(datos[1]).append("</A></TD>");

			 		    if ("".equals(datos[3])){
			              resultado.append("<td class=").append(clase).append(" align=left >&nbsp;</td></tr>");
			 		    }
			            else{
			              resultado.append("<td class=").append(clase).append(" align=left >").append(datos[3]).append("</td></tr>");
			            }

                        EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.DEBUG);
					    contador++;
                    }
					else if( modulo.equals(IEnlace.MCons_Ctas_movil)) //Cuentas M�vil
					{
						trama.append(contador).append('|').append(datos[1]).append('|').append(datos[2]).append('|').append(datos[3]).append("| | |").append(traeBanco(datos[2])).append("| |").append('@');
						resultado.append("<tr><td class=").append(clase).append(" align=left ><a href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,").append(contador).append(");\">").append(datos[1]).append("</a></td>");

						if ("".equals(datos[3])){
				            resultado.append("<td class=").append(clase).append(" align=left >&nbsp;</td></tr>");
						}
			            else{
			            	resultado.append("<td class=").append(clase).append(" align=left >").append(datos[3]).append("</td></tr>");
			            }

						EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.DEBUG);
						contador++;
					}
					else //abono Internacional
					{
						String divisaextranjera = session.getDivisaCuentaInter();

                        if( divisaextranjera==null ){
                          divisaextranjera="todas";
                        }
						if( "todas".equals(divisaextranjera) || datos[4].trim().equals(divisaextranjera) )
                        {
						  if("todas".equals(divisaextranjera)){
							  trama.append(contador).append('|').append(datos[1]).append('|').append(datos[2]).append('|').append(traePais(datos[3])).append('|').append(traeMoneda(datos[4])).append('|').append(datos[5]).append('|').append(datos[6]).append('|').append(datos[7]).append('|').append(datos[8]).append('@');
						  }
						  else{
							  trama.append(contador).append('|').append(datos[1]).append('|').append(datos[2]).append('|').append(datos[3]).append('|').append(datos[4]).append('|').append(datos[5]).append('|').append(datos[6]).append('|').append(datos[7]).append('|').append(datos[8]).append('@');
						  }

						  resultado.append("<TR><td class=").append(clase).append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,").append(contador).append(");\">").append(datos[1]).append("</A></TD>");
						  resultado.append("<td class=").append(clase).append(" align=left >").append(datos[4]).append("</td>");
						  resultado.append("<td class=").append(clase).append(" align=left >").append(datos[8]).append("</td>");

                          EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.DEBUG);
				          contador++;
                        }
                    }

					if (contador>Global.MAX_CTAS_MODULO){
                     break;
					}
				}
	        }  //while
			//Modif PVA 03/12/2002 mejor en el finally entrada.close();
        }
		catch ( Exception e ){
			//e.printStackTrace();
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("cuentasSerfinSantander::lectura_cuentas_para_pantalla:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->" + e.getMessage()
								   + "<DATOS GENERALES>"
								   + "Usuario->" + session.getUserID8()
								   + "Contrato->" + session.getContractNumber()
								   + "Linea de truene->" + lineaError[0]
					               , EIGlobal.NivelLog.ERROR);

		}
        finally
		{
           try
		   {
               entrada.close();
	 	   }
	       catch(Exception e)
		   {
			    e.printStackTrace();
           }
		}

   		EIGlobal.mensajePorTrace("***Global.MAX_CTAS_MODULO: "+Global.MAX_CTAS_MODULO, EIGlobal.NivelLog.DEBUG);

        if( contador > 0 && contador <= Global.MAX_CTAS_MODULO )
        {
          req.setAttribute(TRAMA_OK,"\""+trama.toString()+"\"");
	      req.setAttribute(CONTADOR_OK,Integer.toString(contador));
          req.setAttribute(RESULTADO,resultado.toString());
          req.setAttribute(TRAMADICIONAL,contratos_inversion);
		}
		else if( contador > Global.MAX_CTAS_MODULO )
		{
          req.setAttribute(TRAMA_OK,"\""+trama.toString()+"\"");
	      req.setAttribute(CONTADOR_OK,Integer.toString(contador));
          req.setAttribute(RESULTADO,resultado.toString());
          req.setAttribute(TRAMADICIONAL,contratos_inversion);
          req.setAttribute("MensajeError", "cuadroDialogo(\"Excedi&oacute; el n&uacute;mero de cuentas posibles a visualizar. Especifique su b&uacute;squeda por cuenta o descripci&oacute;n   \", 4); ");
		}
		else
		{
   	      req.setAttribute("MensajeError", "cuadroDialogo(\"No se encontraron cuentas\", 3); ");
          req.setAttribute(TRAMA_OK,"");
	      req.setAttribute(CONTADOR_OK,"");
          req.setAttribute(RESULTADO,"");
          req.setAttribute(TRAMADICIONAL,"");
        }

		evalTemplate("/jsp/framecuentas.jsp",req,res);
		return 1;
   }

    /**
     * M&eacute;todo que a partir de la clave del banco invoca el nombre del mismo
     * @param cveBanco Clave del banco que recibe para invocar el nombre del mismo
     * @return regresa una cadena con el nombre del banco
     * */
	private String traeBanco( String cveBanco )
	{
		String totales = "";
		String resBanco = cveBanco;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
	    Connection conn = null;

		try
		{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
		  	totales = "Select nombre_corto from comu_interme_fin where num_cecoban<>0 " +
		  			  "and cve_interme='" + cveBanco + "'";

		  	psCont = conn.prepareStatement( totales );
		  	rsCont = psCont.executeQuery();
		  	rsCont.next();
		  	resBanco = rsCont.getString( 1 ).trim();
		  	//rsCont.close();
		}
		catch ( Exception e )
		{
		    EIGlobal.mensajePorTrace( "cuentasSerfinSantander - traeBanco(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR );
		    resBanco = cveBanco;
		}
        finally
        {
           try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
			   e.printStackTrace();
			}
	    }
		return resBanco;
	}

	/**
     * M&eacute;todo que a partir de la clave invoca la plaza de la cuenta
     * @param cvePlaza Clave de la plaza que recibe para invocar la plaza de la cuenta
     * @return regresa una cadena con la plaza de la cuenta del banco
     * */
	private String traePlaza( String cvePlaza )
	{
		String totales = "";
		String resPlaza = cvePlaza;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;

		try
		{
		   conn = createiASConn(Global.DATASOURCE_ORACLE );
		   totales = "Select descripcion from comu_pla_banxico where plaza_banxico='" +
		   cvePlaza + "'";
		   psCont = conn.prepareStatement( totales );
		   rsCont = psCont.executeQuery();
		   rsCont.next();
		   resPlaza = rsCont.getString( 1 ).trim();
		   //rsCont.close();
		}
		catch( Exception e )
		{
		   EIGlobal.mensajePorTrace("cuentasSerfinSantander- traePlaza(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		   return resPlaza = cvePlaza;
		}
		finally
        {
            try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
				e.printStackTrace();
			}
	    }
    	EIGlobal.mensajePorTrace( "cuentasSerfinSanander - plaza " +resPlaza, EIGlobal.NivelLog.DEBUG);
		return resPlaza;
	}

	/**
     * M&eacute;todo que a partir de la clave del pa�s invoca el nombre del mismo
     * @param cvepais Clave del pa�s que recibe para invocar el nombre del mismo
     * @return regresa una cadena con el nombre del pa�s origen del banco
     * */
	private String traePais( String cvepais )
	{
		String totales = "";
		String respais = cvepais;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;

		try
		{
		   conn = createiASConn(Global.DATASOURCE_ORACLE );
		   totales = "Select descripcion from comu_paises  where cve_pais='" +
		   cvepais + "'";
		   psCont = conn.prepareStatement( totales );
		   rsCont = psCont.executeQuery();
		   rsCont.next();
		   respais = rsCont.getString( 1 ).trim();
		   //rsCont.close();
		}
		catch( Exception e )
		{
		   EIGlobal.mensajePorTrace("cuentasSerfinSantander- traePais(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		   return respais = cvepais;
		}
		finally
        {
            try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
				e.printStackTrace();
			}
	    }

    	EIGlobal.mensajePorTrace( "cuentasSerfinSanander - Pais " +respais, EIGlobal.NivelLog.DEBUG);
		return respais;
	}

	/**
     * M&eacute;todo que a partir de la clave de moneda trae la divisa correspondiente
     * @param cvemoneda Clave de moneda que recibe para invocar la divisa correspondiente
     * @return regresa una cadena con la divisa correspndiente
     * */
    private String traeMoneda( String cvemoneda )
	{
		String totales = "";
		String resmoneda = cvemoneda;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;

		try
		{
		   conn = createiASConn(Global.DATASOURCE_ORACLE );
		   totales = "Select descripcion from comu_mon_divisa  where cve_divisa='" +
		   cvemoneda + "'";
		   psCont = conn.prepareStatement( totales );
		   rsCont = psCont.executeQuery();
		   rsCont.next();
		   resmoneda = rsCont.getString( 1 ).trim();
		   //rsCont.close();
		}
		catch( Exception e )
		{
		   EIGlobal.mensajePorTrace("cuentasSerfinSantander- traePais(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		   return resmoneda = cvemoneda;
		}
		finally
        {
            try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
				e.printStackTrace();
			}
	    }

    	EIGlobal.mensajePorTrace( "cuentasSerfinSanander - Moneda " +resmoneda, EIGlobal.NivelLog.DEBUG);
		return resmoneda;
	}

    /**
     * M&eacute;todo usado para crear y copiar el archivo dela trama de consulta
     * @param usuario Clave del usuario actual del sistema
     * @param cuenta Cuenta o contrato del usuario actual
     * @return regresa un valor booleano indicando el �xito de la operaci�n
     * */
	public boolean creayCopiaArchivo(String usuario, String cuenta)
	{
	     String nombreArchivo="";
		 boolean seGenero=true;

		 nombreArchivo=usuario+".tmp";

		 EI_Exportar ArcTux=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

		 if(ArcTux.creaArchivo())
		 {
			if(!ArcTux.escribeLinea(cuenta+"|")){
			 seGenero=false;
			}
		 }
		 else{
			seGenero=false;
		 }

		 ArchivoRemoto archR = new ArchivoRemoto();

		 if(!archR.copiaLocalARemoto(nombreArchivo))
	     {
			  EIGlobal.mensajePorTrace(" No se pudo generar copia remota de errores", EIGlobal.NivelLog.ERROR);
			  seGenero=false;
		 }
		 else{
			 EIGlobal.mensajePorTrace(" Si se pudo generar copia remota de errores", EIGlobal.NivelLog.DEBUG);
		 }

		 ArcTux.cierraArchivo();
		 return seGenero;
	 }

	/**
	 *
	 * CSA ARS Se actualiza debido a que el archivo .amb no contendr� las cuentas asociadas al contrato.
	 * @param contrato para consultar las cuentas relacionadas.
	 * @return cadena con las cuentas concatenadas.
	 * @since 10/06/2013.
	 *
	 */
	public String traeCuentasPropias(String contrato)
	{
		String arcLinea="";
		String cadena="";
		StringBuffer cadenaTCT   = new StringBuffer("");

		EI_Tipo CtasLineas=new EI_Tipo();
		CuentasBO cuentasBo = new CuentasBO();
		EIGlobal.mensajePorTrace("CSA: Consulta las cuentas del contrato :"+ contrato, EIGlobal.NivelLog.INFO);
		List<String> cuentas=cuentasBo.consultaCuentasRel(contrato);
		EIGlobal.mensajePorTrace("CSA: Numero de cuentas obtenidas :"+cuentas.size(), EIGlobal.NivelLog.INFO);
		Iterator<String> cuentasIter = cuentas.iterator();
		while(cuentasIter.hasNext()){
			arcLinea = cuentasIter.next();
			if(!"ERROR".equals(arcLinea))
			 {
					if("ERROR".equals(arcLinea.substring(0,5).trim()))
					{
						break;
					}
					if("2".equals(arcLinea.substring(0,1)))
					 {
						cadenaTCT.append (arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @");
					}
				 }
				else{
					break;
				}
		}
		EIGlobal.mensajePorTrace("CSA: Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);

		//CSA ----------------------

		EIGlobal.mensajePorTrace("Se leyeron los registros: "+cadenaTCT.toString(), EIGlobal.NivelLog.DEBUG);
		CtasLineas.iniciaObjeto(';','@',cadenaTCT.toString());

		for(int i=0;i<CtasLineas.totalRegistros;i++)
		{
		   EIGlobal.mensajePorTrace("Registro: "+i+" tipoRelacion: "+ CtasLineas.camposTabla[i][3] + " cveProducto: "+ CtasLineas.camposTabla[i][4], EIGlobal.NivelLog.DEBUG);
		   if("P".equals(CtasLineas.camposTabla[i][3].trim()) && "0".equals(CtasLineas.camposTabla[i][4].trim()))
			{
			   EIGlobal.mensajePorTrace("Linea de credito: "+i+" tipoRelacion: "+ CtasLineas.camposTabla[i][3] + " cveProducto: "+ CtasLineas.camposTabla[i][4], EIGlobal.NivelLog.DEBUG);
		       cadena+=CtasLineas.camposTabla[i][1]+" ";
			}
		}
	    return "$"+cadena;
	 }

}