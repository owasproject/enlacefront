/*Banco Santander Mexicano
  Clase cuentasaldo  llamada al servicio para la consulta de saldos
  @Autor:
  @version: 1.1.2
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 28/02/2001 - Quitar codigo muerto
  modificaci�n: Jorge Barbosa 29-03-2001 - Columna Fecha ya no aplica
	�ltima modificaci�n: Daniel C�zares Beltr�n 05/09/2002. Se agreg� c�digo
				 							 para validar el Tipo de cada una de las cuentas que
											 se consultan. Esto con el objetivo de
				 							 obtener por separado la suma de los montos para Cuentas
											 de Cheques en Pesos y Cheques Dls.
 */
package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.rmi.*;
import java.sql.SQLException;

import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.export.Column;
import mx.altec.enlace.export.ExportModel;
import mx.altec.enlace.export.HtmlTableExporter;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.io.*;

// 02/ENE/07

/**
 * The Class cuentasaldo.
 */
public class cuentasaldo extends BaseServlet {
	// String myPath = IEnlace.DOWNLOAD_PATH;
	// int fileID = Math.abs(new Random().nextInt());
	// String strExtensionSal = "-" + fileID + "CS.txt";
	// String fileOut = "";

	/** Total */
	private static final String TOTAL = "0.00";
	
	/** Consulta Saldos */
	private static final String CONSULTA_SALDOS = "Consulta de Saldos de cuentas de cheques";
	
	/** Rellenar Caracter */
	private static final String RELLENAR_CARACTER = "000";
	
	/** Consulta por cuenta */
	private static final String CONSULTA_POR_CUENTA = "Consultas &gt; Saldos &gt; Por cuenta &gt; Cheques";

	
	/**
	 * doGet.
	 * 
	 * @param req El HttpServletRequest obtenido de la vista
	 * @param res El HttpServletResponse obtenido de la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		defaultAction(req, res);
	}


	/**
	 * doPost.
	 * 
	 * @param req El HttpServletRequest enviado a la vista
	 * @param res El HttpServletResponse enviado a la vista
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		defaultAction(req, res);
	}


	/**
	 * Default action.
	 *
	 * @param req the req
	 * @param res the res
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

	  HttpSession sess = req.getSession();
	  
      BaseResource session = (BaseResource) sess.getAttribute("session");
      
		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***INICIA cuentasaldo.java", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "++++++++++++++++++++++++++++++++++", EIGlobal.NivelLog.DEBUG);

		int indice = 0;
		int residuo = 0;
		String pre = "";
		String nex = "";
		String tota = "";
		String tramaentrada = "";
		String valorbrowser = "";
		String valorarchivo = "";
		String cta = "";
		String nombre = "";
		String tipo = "";
		String coderror = "";
		String c = "";
		String cuentacompleta = "";
		String disponible = "";
		String sbc = "";
		String total = "";
		String fechaexpt = "";
		String StrTipoCta = "";
		String s25020h = "s25020h";
		String contador_cta = "";
		
		double dispo = 0;
		double sbcP = 0;
		double totalP = 0;
						 
		String totalBitacorizar =TOTAL;

		boolean sesionvalida = SesionValida( req, res );
		//if (sesionvalida && session.getFacCSaldo().trim().length() != 0)

		if ( sesionvalida && session.getFacultad(session.FAC_CONSULTA_SALDO)) 		//
			{
			String cta_saldo;
			String fileOut="";
			fileOut = session.getUserID8() + "sd.doc";

			//Obtiene las cuentas en el formato NoCta@Propiedad@Descripcion.
			//Todas las Cuentas son separadas por caracter "," (coma).
			cta_saldo = ( String ) req.getParameter( "cta_saldo" );
			EIGlobal.mensajePorTrace("Resultado inicio cta_saldo: "+cta_saldo, EIGlobal.NivelLog.DEBUG);
			String m = ( String ) req.getParameter( "j" );
			
			
			String tipoArchivo = req.getParameter("tArchivo");
			EIGlobal.mensajePorTrace("FSW VECTOR ---> CuentaSaldo --> TIPO ARCHIVO <"+tipoArchivo+">", EIGlobal.NivelLog.INFO);				
			if(tipoArchivo != null &&!("".equals(tipoArchivo))){ 
//				 String rutaArchivo = IEnlace.DOWNLOAD_PATH + fileOut;
				String rutaArchivo = IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8() + "sd.doc";
				//IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8();
				EIGlobal.mensajePorTrace("FSW VECTOR ---> CuentaSaldo --> ARCHIVO EXPORTAR <"+rutaArchivo+">", EIGlobal.NivelLog.INFO);

				EIGlobal.mensajePorTrace("FSW VECTOR ---> CuentaSaldo --> INICIADO EXPORTACION  RUTA ARCHIVO ", EIGlobal.NivelLog.INFO);
				exportarArchivo(tipoArchivo, rutaArchivo, res);
			}		
					

			
			if(validarParametosContienenXSS(req)){
				despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP, CONSULTA_SALDOS, CONSULTA_POR_CUENTA, req, res );
				return;

			}

			//int h = Integer.parseInt(m);
			boolean grabaArchivo = true;

			// jbg 29032001
			double totalDisponible = 0;
			double totalSBC		   = 0;
			double totalSaldos	   = 0;
			double totalSaldosPesos = 0;
			double totalSaldosDls = 0;

			boolean bandera		   = false;

			//Se agrega arreglo para Consutar las Cuentas
			////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			try{
				
				pre = req.getParameter ("prev");
				nex =  req.getParameter ("next");
				tota = req.getParameter ("total");
				
				
				EIGlobal.mensajePorTrace ( "***cuentasaldo.java prev "+pre, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ( "***cuentasaldo.java next "+nex, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace ( "***cuentasaldo.java total "+tota, EIGlobal.NivelLog.DEBUG);
			 }catch(Exception e) {}

	         int total1=0,prev=0,next=0;
	         if((pre!=null) &&  (nex!=null) && (tota!=null)) {
				try{
	                prev =  Integer.parseInt (pre);
	                next =  Integer.parseInt (nex);
	                total1 = Integer.parseInt (tota);
				} catch(Exception e) {
					EIGlobal.mensajePorTrace(">>> cuentasaldo ->  defaultAction -> Mensaje Excepcion "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				                     }
	         } else {

	            EIGlobal.mensajePorTrace ( "***csaldo1 todos nullos", EIGlobal.NivelLog.DEBUG);
				try{
				     EIGlobal.mensajePorTrace("el userID: "+session.getUserID8(), EIGlobal.NivelLog.INFO);
				   } catch(Exception e) {
					   EIGlobal.mensajePorTrace(">>> cuentasaldo ->  defaultAction -> Mesaje EIGlobal "+e.getMessage(), EIGlobal.NivelLog.ERROR);
				                        }
				EIGlobal.mensajePorTrace("Tomamos las cuentas", EIGlobal.NivelLog.DEBUG);
	            llamado_servicioCtasInteg (IEnlace.MConsulta_Saldos," "," "," ",session.getUserID8 ()+".ambci",req);
	            total1=getnumberlinesCtasInteg (IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+".ambci");
				//EIGlobal.mensajePorTrace(total, EIGlobal.NivelLog.INFO);
	            prev=0;

	            if (total1>Global.NUM_REGISTROS_PAGINA) {
	                next=Global.NUM_REGISTROS_PAGINA;
	            } else {
	                      next=total1;
	                    }
	         }
	      
	         String[][] arrayCuentas = ObteniendoCtasInteg (prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8 ()+".ambci");
	         int contador1=1;
	         String strSalida = "";

	         
			///////////////////////////////////////////////////////////////////////////////////////
			
			
			//Se insertan las cuentas en un Arreglo de Strings. Cada cuenta en un
			//espacio del arreglo.
			EIGlobal.mensajePorTrace("m "+m+"   cta_saldo "+cta_saldo, EIGlobal.NivelLog.DEBUG);
            //MHG (IM325073) se cambio el separador coma (,) por un pipe(|)
			String[] Carreglo = desentramaC( m + "|" + cta_saldo,'|');
			//arregloSal[0] = buffer.substring ( 0, buffer.indexOf ( Separador ) );


			EI_Exportar ArcSal;
			ArcSal = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			EIGlobal.mensajePorTrace( "***Se verificar� que el archivo no exista en caso contrario se borrara y crear� uno nuevo", EIGlobal.NivelLog.DEBUG);
			ArcSal.creaArchivo();

			Hashtable htResult;

			try
				{
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				tramaentrada = "1EWEB|" + session.getUserID8() + "|SDCT|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
				
				//***********************************************
				//modificaci�n para integraci�n pva 07/03/2002
				//***********************************************
				//String[][] arrayCuentas = ContCtasRelac( session.getContractNumber() );
				//***********************************************
				
				//Ciclo que recorre arreglo de cuentas.

				for (indice = 1; indice <= Integer.parseInt( Carreglo[0].trim() ); indice++ )
					{
										
					//VSWF-BMB
					totalBitacorizar=TOTAL;
					total="";
					//***********************************************
					//modificaci�n para integraci�n pva 07/03/2002
					//***********************************************
					//tipo_cuenta = arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
					//***********************************************
					EIGlobal.mensajePorTrace("Carreglo "+Carreglo[indice], EIGlobal.NivelLog.DEBUG);
					
					
					if(Carreglo[indice].length()>0 )
						{
						residuo = indice % 2 ;
						if (residuo == 0){
							c = "textabdatobs";
						}else{
							c = "textabdatcla";
						}

						cuentacompleta = Carreglo[indice];
						//Obtiene No. De Cuenta
						cta = cuentacompleta.substring(0,cuentacompleta.indexOf("@"));
						cuentacompleta = cuentacompleta.substring(cuentacompleta.indexOf("@")+1,cuentacompleta.length());
						//Obtiene el tipo de Cuenta. Valores P=Propia T=Terceros
						tipo = cuentacompleta.substring(0,cuentacompleta.indexOf("@"));
						cuentacompleta = cuentacompleta.substring(cuentacompleta.indexOf("@")+1,cuentacompleta.length());
						//Obtiene la Descripci�n de la Cuenta
						nombre = cuentacompleta.substring(0,cuentacompleta.indexOf("@"));
						cuentacompleta = cuentacompleta.substring(cuentacompleta.indexOf("@")+1,cuentacompleta.length());
						//Obtiene el Tipo de Cuenta
						StrTipoCta = cuentacompleta.substring(0,cuentacompleta.indexOf("@"));
						cuentacompleta = cuentacompleta.substring(cuentacompleta.indexOf("@")+1,cuentacompleta.length());
						//Obtiene el Contador de la cuenta
						contador_cta = cuentacompleta.substring(0,cuentacompleta.indexOf("@"));
						EIGlobal.mensajePorTrace("StrCta "+StrTipoCta, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Contador: "+ contador_cta, EIGlobal.NivelLog.DEBUG);
						

						for (int indice1 = 1;indice1<=Integer.parseInt (arrayCuentas[0][0]);indice1++){
							
				                strSalida = strSalida + arrayCuentas[indice1][1] +"@"+arrayCuentas[indice1][2]+"@"+arrayCuentas[indice1][4]+"@" + arrayCuentas[indice1][3] + "@" + "|";
				                contador1++;

				                
				                //EIGlobal.mensajePorTrace("La cuenta es la correta: "+ contador_cta + " " + contador1, EIGlobal.NivelLog.DEBUG);
				                
				                int cont1 =  Integer.parseInt(contador_cta);
				               
				                if (cont1 == contador1)
				                {
				                	EIGlobal.mensajePorTrace("La cuenta es la correta: "+ contador_cta + " " + contador1, EIGlobal.NivelLog.DEBUG);
				                	cta = arrayCuentas[indice1][1];
				                	tipo = arrayCuentas[indice1][2];
				                	nombre = arrayCuentas[indice1][4];
				                	StrTipoCta = arrayCuentas[indice1][3];
				      				                	
				                }

				       
						  }//for carga archivo
						
						   contador1 = 1;
						
						if (Global.NIVEL_TRACE>1)
							EIGlobal.mensajePorTrace( "cuentasaldo = <<" + tramaentrada + cta + "|" + tipo + "|" + ">>", EIGlobal.NivelLog.INFO);

						
						//Llama a la consulta del saldo de la cuenta.
						htResult = tuxGlobal.web_red( tramaentrada + cta + "|" + tipo + "|" );
						coderror = ( String ) htResult.get( "BUFFER" );
						
						EIGlobal.mensajePorTrace( "----> FSW VALOR TRAMA  <<<<<<<" +coderror+ ">>>>>>", EIGlobal.NivelLog.INFO);
						

						if (Global.NIVEL_TRACE>1)
							EIGlobal.mensajePorTrace( "CODIGO ERROR = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);

						fechaexpt = ObtenFecha(true);
						fechaexpt = fechaexpt.substring(0,2)+fechaexpt.substring(3,5)+fechaexpt.substring(6,fechaexpt.length());

						if ("OK".equals(coderror.substring(0,2)))
							{
							// FSW 
							bandera = true;
							EIGlobal.mensajePorTrace( "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< REALIZA CONSULTA DE CHEQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", EIGlobal.NivelLog.INFO);
							valorbrowser += "<tr>"
									+ "<td class=" + c + " align=center>"
											+ "<input type=radio name=cta_posi value=\"3|"+cta+"|" + nombre+"|"+tipo+"|\">"+ "&nbsp;</td>" + //RADIO BOTON
											"<td class="+c+" nowrap align=center>"+ cta +  "&nbsp;</td>" + //CUENTA
											"<td class="+c+" colspan=2>" + nombre + "&nbsp;</td>"+ //NOMBRE 
											"<td class="+c+" align=right nowrap>  " + FormatoMoneda(coderror.substring(16,30)) +  "&nbsp;</td>" + //DISPONIBLE 
											"<td class="+c+" align=right nowrap>  " + FormatoMoneda(coderror.substring(30,44)) +  "&nbsp;</td>"; //SBC
											

							//"<td bgcolor="+c+" align=center>&nbsp;" + ObtenFecha()+ "</td>";
//							totalDisponible += new Double(coderror.substring(16,30)).doubleValue();
//							totalSBC		+= new Double(coderror.substring(30,44)).doubleValue();

							try	{
								totalDisponible += new Double(coderror.substring(16,30)).doubleValue();
								EIGlobal.mensajePorTrace("***cuentasaldo.class despues de total disponible"+totalDisponible, EIGlobal.NivelLog.DEBUG);
								//valorarchivo = cta + ";" + nombre + ";" + FormatoMoneda(coderror.substring(16,30)) + ";" + FormatoMoneda(coderror.substring(30,44)) + ";" + ObtenFecha(false) + ";" ;
								disponible = coderror.substring(16,30);
								}	catch(NumberFormatException e)
								{
								totalDisponible += 0.0;
								disponible=TOTAL;
								}
							try
								{
								totalSBC += new Double(coderror.substring(30,44)).doubleValue();
								sbc = coderror.substring(30,44);
								}
							catch(NumberFormatException e)
								{
								totalSBC  += 0.0;
								sbc=TOTAL;
								}

							disponible = disponible.trim();

							if (disponible.indexOf(".")>-1)
								disponible = disponible.substring(0,disponible.indexOf('.')) +
									disponible.substring(disponible.indexOf('.') + 1, disponible.length());

							sbc = sbc.trim();
							dispo = Double.parseDouble(disponible) / 100;
							sbcP = Double.parseDouble(sbc) / 100;

							if (sbc.indexOf(".")>-1)
								sbc = sbc.substring(0,sbc.indexOf('.'))+sbc.substring(sbc.indexOf('.')+1,sbc.length());

//							valorarchivo = rellenaCaracter(cta,' ',16,false)+";"+rellenaCaracter(nombre,' ',40,false)+";"+fechaexpt+";"+
//										   obtenHora()+";"+dispo+";"+sbcP;
							
							valorarchivo = rellenaCaracter(cta,' ',16,false)+rellenaCaracter(nombre,' ',40,false)+ fechaexpt +
									   obtenHorac()+rellenaCaracter(disponible,'0',15,true)+rellenaCaracter(sbc,'0',15,true);							

							if (coderror.length()>=58)
								{//Obtiene Total de Saldos en Pesos y en Dls.
								valorbrowser+="<td class="+c+" align=right nowrap>" + FormatoMoneda(coderror.substring(44,58)) + "</td></tr>\n" ; // TOTAL CH
								total = coderror.substring(44,58);
								total = total.trim();
								/*
								 * VSWF
								 */
								totalBitacorizar = total;
								/*
								 * VSWF
								 */

								if (total.indexOf(".")>-1)
									total = total.substring(0,total.indexOf('.'))+total.substring(total.indexOf('.')+1,total.length());

								totalSaldos += new Double(coderror.substring(44,58)).doubleValue();

								//Validaci�n para saber el tipo de cuenta
								if(null == StrTipoCta){
								  EIGlobal.mensajePorTrace( "***Error al recibir Tipo de Cuenta", EIGlobal.NivelLog.ERROR);
								}else{

								   //Si es cuenta de Cheques en Pesos
								   if(!"0".equals(StrTipoCta) && !"4".equals(StrTipoCta) &&
									 	  !"5".equals(StrTipoCta) && !"6".equals(StrTipoCta) &&
											!"SI".equals(cta.substring(0,2)) &&
											!"BM".equals(cta.substring(0,2))){

										  totalSaldosPesos +=
											   new Double(coderror.substring(44,58)).doubleValue();
									 }else{
									    //Si es cuenta de cheques en D�lares StrTipoCta.equals("6")
									    if("6".equals(StrTipoCta)){
											   totalSaldosDls +=
												    new Double(coderror.substring(44,58)).doubleValue();
											}
									 }
								}
							}else
								{
								valorbrowser+="<td class="+c+" align=right nowrap>&nbsp;</td></tr>\n" ;
								total=RELLENAR_CARACTER;
								}
							
							totalP = Double.parseDouble(total)/100;							
							valorarchivo+= rellenaCaracter(total,'0',15,true);
							}
						else
							{
							// FSW CONDICION PARA CUANDO NO SE ENCUENTRA INFORMACION DE LA CUENTA
							
							bandera = true;

							valorbrowser += "<tr>"
									+ "<td class=" + c + " align=center><input type=radio name=cta_posi value=\"3|"+cta +"|"+nombre+"|"+tipo+"|\"></td>"
									+ "<td class=" + c + " nowrap align=center >" + cta + "</td>" 
									+ "<td class="+c+" colspan=2>No se pudo obtener la informaci&oacute;n de esta cuenta</td>"
									+ "<td class="+c+" align=right  nowrap>&nbsp;</td>" 
									+ "<td class="+c+" align=right  nowrap>&nbsp;</td>" 
									+ //"<td class="+c+" align=center>" + ObtenFecha(false) + "</td>" +
										"<td class="+c+" align=right  nowrap>&nbsp;</td>\n";

//							valorarchivo = rellenaCaracter(cta,' ',16,false)+";"+rellenaCaracter(nombre,' ',40,false)+";"+fechaexpt +";"+
//								obtenHora() +";"+ rellenaCaracter(RELLENAR_CARACTER,'0',15,true) +";"+ rellenaCaracter(RELLENAR_CARACTER,'0',15,true) +";"+
//								rellenaCaracter(RELLENAR_CARACTER,'0',15,true);
							
							valorarchivo = rellenaCaracter(cta,' ',16,false)+rellenaCaracter(nombre,' ',40,false)+ fechaexpt +
									obtenHorac() + rellenaCaracter(RELLENAR_CARACTER,'0',15,true) + rellenaCaracter(RELLENAR_CARACTER,'0',15,true) +
									rellenaCaracter(RELLENAR_CARACTER,'0',15,true);							
							
							}

						if(grabaArchivo){
							ArcSal.escribeLinea(valorarchivo + "\r\n");
							EIGlobal.mensajePorTrace( "***Se enscribio en el archivo  "+valorarchivo, EIGlobal.NivelLog.DEBUG);
						}

						/*
						 * 02/ENE/07
						 * VSWF-BMB-I
						 */
						  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						try{
							if(totalBitacorizar==null|| "".equals(totalBitacorizar.trim())){
								totalBitacorizar=TOTAL;
							}
				            BitaHelper bh = new BitaHelperImpl(req, session, sess);
				    		BitaTransacBean bt = new BitaTransacBean();
				    		bt = (BitaTransacBean)bh.llenarBean(bt);
				    		bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_CHEQUE_CONS_SALDO_EFEC);
				    		if(session.getContractNumber()!=null){
				    			bt.setContrato(session.getContractNumber());
				    		}
				    		if(cta!=null){
				    			if(cta.length()>20){
				    				bt.setCctaOrig(cta.substring(0,20));
				    			}else{
				    				bt.setCctaOrig(cta.trim());
				    			}
				    		}
				    		bt.setImporte(Double.parseDouble(totalBitacorizar));
				    		if(coderror!=null){
				    			if("OK".equals(coderror.substring(0,2))){
				    				bt.setIdErr("SDCT0000");
				    			}else if(coderror.length()>8){
					    			bt.setIdErr(coderror.substring(0,8));
					    		}else{
					    			bt.setIdErr(coderror.trim());
					    		}
				    		}
				    		if(fileOut!=null){
				    			bt.setNombreArchivo(fileOut);
				    		}
				    		bt.setServTransTux("SDCT");
				    		BitaHandler.getInstance().insertBitaTransac(bt);
			    		}catch(SQLException e){
			    			EIGlobal.mensajePorTrace(">>> cuentasaldo ->  defaultAction -> SQLException"+e.getMessage(), EIGlobal.NivelLog.ERROR);
			    		}catch(Exception e){
			    			EIGlobal.mensajePorTrace(">>> cuentasaldo ->  defaultAction -> "+e.getMessage(), EIGlobal.NivelLog.ERROR);
			    		}
						  }
						//Se inicializa variable cta_valida
						  

			    		/*
						 * 02/ENE/07
						 * VSWF-BMB-F
						 */
						}//if
					
					
					}//for
			   					
					
				/*valorbrowser += "<tr>";
				valorbrowser += "<td align=right>&nbsp;</td>";
				valorbrowser += "<td>&nbsp;</td>";
				valorbrowser += "<td width=100% align=right>&nbsp;</td>";
				valorbrowser += "<td class=tittabdat align=center>Total</td>";
				valorbrowser += "<td class=tittabdat align=right>" + FormatoMoneda(totalDisponible)  + "</td>";
				valorbrowser += "<td class=tittabdat align=right>" + FormatoMoneda(totalSBC)  + "</td>";
				valorbrowser += "<td class=tittabdat align=right>" + FormatoMoneda(totalSaldos)  + "</td>";
				valorbrowser += "</tr>";*/
				
															
				if( grabaArchivo )
					ArcSal.cierraArchivo();

				ArchivoRemoto archR = new ArchivoRemoto();
				if(!archR.copiaLocalARemoto(fileOut,"WEB"))
					EIGlobal.mensajePorTrace("***cuentasaldo.class No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.ERROR);
								
			} catch( Exception e ){
				/*despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP,
						CONSULTA_SALDOS, "", req, res ); */
			}
			//se modific hgcv se puso el if - else
			//if ( (!coderror.substring(0,2).equals("OK") ) || ( bandera = false  ))
			if ( bandera = false  )
			{
				despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP, CONSULTA_SALDOS, CONSULTA_POR_CUENTA, s25020h, req, res );
			} else {				
				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado(CONSULTA_SALDOS,CONSULTA_POR_CUENTA,s25020h,req));
				req.setAttribute("cta",valorbrowser);
				req.setAttribute("TotalPesos",FormatoMoneda(totalSaldosPesos));
				req.setAttribute("TotalDls",FormatoMoneda(totalSaldosDls));

				String downloadFile = "";
				if ( grabaArchivo )
					downloadFile = "<a id=\"exportar\" href=\"javascript:Exportar();\" border=\"0\" ><img src = \"/gifs/EnlaceMig/gbo25230.gif\" alt=\"Exportar\"  border=\"0\" /></a>";
//				downloadFile = "<a id=\"exportar\"  onclick=\"Exportar();\" href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a>";
					req.setAttribute("DownLoadFile",downloadFile);
								
										
				evalTemplate( IEnlace.SALDO_TMPL, req, res );
			}

		} else if (sesionvalida) {
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,CONSULTA_SALDOS,CONSULTA_POR_CUENTA,s25020h,req,res);
		}
		
	}

	/**
	 * Rellena caracter.
	 *
	 * @param origen the origen
	 * @param caracter the caracter
	 * @param cantidad the cantidad
	 * @param izquierda the izquierda
	 * @return the string
	 */
	public String rellenaCaracter(String origen, char caracter, int cantidad,
			boolean izquierda) {
		int cfin = cantidad - origen.length();
		if (origen.length() < cantidad) {
			for (int contador = 0; contador < cfin; contador++) {
				if (izquierda)
					origen = caracter + origen;
				else
					origen = origen + caracter;
			}
		}
		// ---
		if (origen.length() > cantidad)
			origen = origen.substring(0, cantidad);
		// ---
		return origen;
	}


	/**
	 * Obten hora.
	 *
	 * @return the string
	 */
	public String obtenHorac() {
		String hora = "";
		int horas, minutos;
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();

		Cal.setTime(Hoy);
		horas = Cal.get(Calendar.HOUR_OF_DAY);
		minutos = Cal.get(Calendar.MINUTE);
		hora = (horas < 10 ? "0" : "") + horas + ":"
				+ (minutos < 10 ? "0" : "") + minutos;
		return hora;
	}
	
	/**
	 * Exportar archivo.
	 *
	 * @param tipoArchivo El tipo de archivo a exportar
	 * @param nombreArchivo La ruta del archivo de donde se obtienen los datos
	 * @param res El request de la pagina
	 */
	private void exportarArchivo(String tipoArchivo, String nombreArchivo, HttpServletResponse res){
		EIGlobal.mensajePorTrace(">>Export CuentaSaldo -> ARCHIVO DE ENTRADA  -->"+nombreArchivo+"<--",EIGlobal.NivelLog.DEBUG);
		   ExportModel em = new ExportModel("cSaldoCheque",'|',true)
		   .addColumn(new Column(0,15, "Cuenta"))//16	
		   .addColumn(new Column(16,55, "Descripcion"))//40
		   .addColumn(new Column(56,63, "Fecha"))//8
		   .addColumn(new Column(64,68, "Hora"))//5
		   .addColumn(new Column(69,83,"Saldo Disponible","$#########################0.00",2))//15
		   .addColumn(new Column(84,99, "SBC (Salvo Buen Cobro)","$#########################0.00",2))//15
		   .addColumn(new Column(100,115, "Total de la Cuenta","$#########################0.00",2));//15

		   try{
			   EIGlobal.mensajePorTrace(">>Export CuentaSaldo -> Generando Exportacion --> ",EIGlobal.NivelLog.DEBUG);
	
			   HtmlTableExporter.export(tipoArchivo, em, nombreArchivo, res);
			 //HtmlTableExporter.export(tipoArchivo, em, archivoOrigen, response);
			   
		   }catch(IOException ex){
			   EIGlobal.mensajePorTrace(">>Export CuentaSaldo -> ERROR --> Exportar Archivo -> " + ex.getMessage(),EIGlobal.NivelLog.ERROR);
		   }
	}
}