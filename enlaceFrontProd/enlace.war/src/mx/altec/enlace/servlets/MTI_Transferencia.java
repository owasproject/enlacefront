/***********************************************************

 Banco: ALDEN STATE BANK

 Ciudad: ALDEN

 ABA: 72405442

 Estado: MI

************************************************************
Servicio    Servidor

CONPAISES   CATASRVR

Catalogo de paises...

*************************************************************/



package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.beans.DatosBeneficiarioBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosBeneficiarioBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.TrxGP93BO;
import mx.altec.enlace.bo.TrxGP93VO;
import mx.altec.enlace.bo.TrxGPF2BO;
import mx.altec.enlace.bo.TrxGPF2VO;
import mx.altec.enlace.dao.TipoCambioEnlaceDAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.CatDivisaPais;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.lang.reflect.*;
import java.lang.String.*;
import java.sql.*;

public class MTI_Transferencia extends BaseServlet{

	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = -3496302255635656038L;
	/** constante para session */
	private static final String SESSION = "session";
	/** constante para Modulo */
	private static final String MODULO = "Modulo";
	/** constante para TransNoReg */
	private static final String TRANS_NO_REG = "TransNoReg";
	/** constante para TransReg */
	private static final String TRANS_REG = "TransReg";
	/** constante para Registradas */
	private static final String REGISTRADAS = "Registradas";
	/** constante para TCV_Venta */
	private static final String TCV_VENTA = "TCV_Venta";
	/** constante para TCE_Venta */
	private static final String TCE_VENTA = "TCE_Venta";
	/** constante para TCE_Dolar */
	private static final String TCE_DOLAR = "TCE_Dolar";
	/** constante para inicialImporte */
	private static final String INICIAL_IMPORTE = "inicialImporte";
	/** constante para inicialCveDivisa */
	private static final String INICIAL_CVE_DIVISA = "inicialCveDivisa";
	/** constante para inicialDesDivisa */
	private static final String INICIAL_DESC_DIVISA = "inicialDesDivisa";
	/** constante para inicialCuentaCargo */
	private static final String INICIAL_CTA_CARGO = "inicialCuentaCargo";
	/** constante para ImporteRegUSD */
	private static final String IMPORTE_REG_USD = "ImporteRegUSD";
	/** constante para ImporteReg */
	private static final String IMPORTE_REG = "ImporteReg";
	/** constante para ImporteRegDivisa */
	private static final String IMPORTE_REG_DIVISA = "ImporteRegDivisa";
	/** constante para ERROR */
	private static final String ERROR = "ERROR";
	/** constante para VACIO */
	private static final String VACIO = "VACIO";
	/** constante para busDesBanco */
	private static final String BUS_DESC_BANCO = "busDesBanco";
	/** constante para busDesCiudad */
	private static final String BUS_DESC_CIUDAD = "busDesCiudad";
	/** constante para MenuPrincipal */
	private static final String MENU_PRINCIPAL = "MenuPrincipal";
	/** constante para newMenu */
	private static final String NEW_MENU = "newMenu";
	/** constante para Encabezado */
	private static final String ENCABEZADO = "Encabezado";
	/** constante para Transferencias Internacionales */
	private static final String TRANS_INTERNAC = "Transferencias Internacionales";
	/** constante para s27550h */
	private static final String S27550H = "s27550h";
	/** constante para DA */
	private static final String DA = "DA";
	/** constante para USD */
	private static final String USD = "USD";
	

	/**
	 * metodo doGet
	 * @param req request
	 * @param res response
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )

	    throws ServletException, IOException{

	defaultAction( req, res );

    }

	/**
	 * metodo doPost
	 * @param req request
	 * @param res response
	 */
    public void doPost( HttpServletRequest req, HttpServletResponse res )

	    throws ServletException, IOException{

	defaultAction( req, res );

    }


    /**
	 * metodo defaultAction
	 * @param req request
	 * @param res response
	 */
    public void defaultAction( HttpServletRequest req, HttpServletResponse res)

	    throws ServletException, IOException

    {

	String contrato = "";

	String tipoCuenta = "";

	String usuario = "";

	String clavePerfil = "";

	String facImpArc = "";

	String facCtasNoReg="";



	EI_Tipo Reg=new EI_Tipo(this);

	EI_Tipo NoReg=new EI_Tipo(this);

	EI_Tipo NoRegCorrectos=new EI_Tipo(this);

	EI_Tipo ArchivoI=new EI_Tipo(this);


	short  sucursalOpera=(short)787;



		/************* Modificacion para la sesion ***************/

		HttpSession sess = req.getSession();

		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );



		EIGlobal.mensajePorTrace("MTI_Transferencia - execute(): Entrando a Transferencias Internacionales.", EIGlobal.NivelLog.INFO);



	boolean sesionvalida=SesionValida( req, res );

	if(sesionvalida)

	 {

	   //Variables de sesion ...

	   session.setModuloConsultar(IEnlace.MCargo_TI_pes_dolar);

	   contrato = session.getContractNumber();

	   sucursalOpera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

	   usuario = session.getUserID8();

	   clavePerfil = session.getUserProfile();

	   facCtasNoReg = (session.getFacultad(session.FAC_TRANSF_NOREG_INTERNAC))?"true":"false";
	   EIGlobal.mensajePorTrace("Facultad para Cuentas No Registradas:"+facCtasNoReg, EIGlobal.NivelLog.INFO);


	   //facImpArc = session.getFacImportaInterban();

	   facImpArc="false";



	   String modulo = (String) req.getParameter(MODULO);



		   EIGlobal.mensajePorTrace("MTI_Transferencia - execute():"+req.getParameter(TRANS_NO_REG), EIGlobal.NivelLog.INFO);

		   req.setAttribute("comboPaises", traePaises());//INDRA-SWIFT-P022574

	   if("0".equals(modulo)){
/*STFQRO 31012011*/
		 EIGlobal.mensajePorTrace("MTI_Transferencia - MODULO 0: "+Reg.strOriginal, EIGlobal.NivelLog.INFO);
/*STFQRO 31012011*/
	     iniciaDepositos(req,res,tipoCuenta,facImpArc,contrato,facCtasNoReg);
		}else if("1".equals(modulo)){
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: MODULO 1: "+Reg.strOriginal, EIGlobal.NivelLog.DEBUG);
	    	 
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: calleBenef : " + req.getParameter("calleBenef"), EIGlobal.NivelLog.DEBUG);
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: paisBIBenef : " + req.getParameter("paisBIBenef"), EIGlobal.NivelLog.DEBUG);
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: ciudadBenef : " + req.getParameter("ciudadBenef"), EIGlobal.NivelLog.DEBUG);
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: idBenef : " + req.getParameter("idBenef"), EIGlobal.NivelLog.DEBUG);
	    	 //INDRA-SWIFT-P022574-ini
	    	 DatosBeneficiarioBean beneficiarioBean = new DatosBeneficiarioBean();
	    	 DatosBeneficiarioBO beneficiarioBO = new DatosBeneficiarioBO();
	    	 beneficiarioBean.setDireccion(req.getParameter("calleBenef"));
	    	 beneficiarioBean.setCiudad(req.getParameter("ciudadBenef"));
	    	 beneficiarioBean.setIdCliente(req.getParameter("idBenef"));
	    	 beneficiarioBean.setCvePaisBenef(req.getParameter("paisBIBenef"));

	    	 //Actualizar cuenta beneficiaria internacional
	    	 EI_Tipo obj = new EI_Tipo(this);
	    	 obj.iniciaObjeto((String)req.getParameter(TRANS_REG));
	    	 if (!"".equals(req.getParameter("calleBenef")) && !"0".equals(req.getParameter("paisBIBenef")) &&
	    			 !"".equals(req.getParameter("ciudadBenef")) && !"".equals(req.getParameter("idBenef"))) {
	    		 beneficiarioBO.almacenaDatosBenef(beneficiarioBean, contrato, obj.camposTabla[0][2]);
	    	 }
	    	 //INDRA-SWIFT-P022574-fin
	    	 
	    	 if(!((String)req.getParameter("CveEspecial")).trim().equals("") &&
	    		!((String)req.getParameter("tipoEspecial")).trim().equals("")){
	    		 req.getSession().setAttribute ("CveEspecial",req.getParameter("CveEspecial"));
				 req.getSession().setAttribute ("tipoEspecial",req.getParameter("tipoEspecial"));
			 }
	    	 validarDatosBanco(req, res);
	    	 EIGlobal.mensajePorTrace("MTI_Transferencia::defaultAction:: MODULO 1: "+Reg.strOriginal, EIGlobal.NivelLog.DEBUG);

	     }

	   else

	    if("2".equals(modulo))

	     {
	       EIGlobal.mensajePorTrace("MTI_Transferencia - MODULO 2: "+Reg.strOriginal, EIGlobal.NivelLog.INFO);
	       if(validarDatosBancoEnvio(req, res)){
		       if(actualizaBancoInter(req)){
		    	   Reg.iniciaObjeto(req.getParameter(TRANS_REG));
		    	   EIGlobal.mensajePorTrace("MTI_Transferencia - execute(): Objeto Registradas: "+Reg.strOriginal, EIGlobal.NivelLog.INFO);
		    	   NoReg.iniciaObjeto(req.getParameter(TRANS_NO_REG));
		    	   req.getSession().setAttribute (REGISTRADAS,req.getParameter(TRANS_REG));
		    	   req.getSession().setAttribute ("NoRegistradas",req.getParameter(TRANS_NO_REG));
		    	   EIGlobal.mensajePorTrace("MTI_Transferencia - execute(): Objeto No Registradas: "+NoReg.strOriginal, EIGlobal.NivelLog.INFO);
		    	   generaTablaDepositos(req,res,contrato,tipoCuenta,usuario,clavePerfil,facImpArc,Reg,NoReg,facCtasNoReg,NoRegCorrectos);
		       }else{
				     req.setAttribute("MsgError", "Error al actualizar el banco del beneficiario.");
					 evalTemplate(IEnlace.ERROR_TMPL, req, res);
		       }
	       }
	     }

	 }

	else
		{
			 EIGlobal.mensajePorTrace("Facultad para Cuentas No Registradas:"+facCtasNoReg, EIGlobal.NivelLog.INFO);

			 if(sesionvalida)
 		     {
			     req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);

				 evalTemplate(IEnlace.ERROR_TMPL, req, res);

			  }

		     EIGlobal.mensajePorTrace("MTI_Transferencia - execute(): Saliendo de Deposito Internacionales.", EIGlobal.NivelLog.INFO);

		}
}


/************************************************************************************

						 Desplegar Cuentas NoReg (si tiene fac)

/*************************************************************************************/

  /**
   * Cuentas no registradas
   * @param tipoCuenta Tipo de cuenta
   * @param facCtasNoReg Facultad ctas no registradas
   * @param inicialCuentaCargo Cuenta cargo
   * @param inicialCveDivisa Clave divisa
   * @param inicialDesDivisa Descripcion divisa
   * @param inicialImporte Importe
   * @param req Request
   * @return String Respuesta
   */
String cuentasNoRegistradas(String tipoCuenta, String facCtasNoReg,
		String inicialCuentaCargo, String inicialCveDivisa,
		String inicialDesDivisa,String inicialImporte, HttpServletRequest req)
{
	StringBuffer strNoRegistradas = new StringBuffer();
	String totales="";
	int totalBancos;
	/************* Modificacion para la sesion ***************/
	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute(SESSION);
	EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );
	EIGlobal.mensajePorTrace("MTI_Transferencia - cuentasNoRegistradas(): FacCtasNoReg="+facCtasNoReg, EIGlobal.NivelLog.INFO);

	if("true".equals(facCtasNoReg.trim())) {
		EIGlobal.mensajePorTrace("Si tiene la facultad - facCtasNoReg:"+facCtasNoReg, EIGlobal.NivelLog.INFO);

		strNoRegistradas.append("\n   <tr>");
		strNoRegistradas.append("\n	 <td colspan=4 class='tittabdat'><INPUT TYPE=radio value=1 NAME=Registradas onClick='VerificaFac(0);'>&nbsp;<b>Cuentas No Registradas</b></td>");
		strNoRegistradas.append("\n	</tr>");
		strNoRegistradas.append("\n	<tr>");
		strNoRegistradas.append("\n	 <td class='textabdatcla' colspan=4>");
		strNoRegistradas.append("\n	  <table class='textabdatcla' cellspacing=3 cellpadding=2 border=0 width='100%'>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' colspan=2 width='50%'>Ordenante</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' colspan=2 width='50%'>Cuenta del Beneficiario</th>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td colspan=2 class='tabmovtex'>");
		strNoRegistradas.append("\n  <table border=1 width='100%' cellpadding=2 cellspacing=0>");
		strNoRegistradas.append("\n	<tr>");
		strNoRegistradas.append("\n	 <td class='tabmovtex' width='100%'>");
		strNoRegistradas.append(generaCuentaCargo(inicialCuentaCargo,"NOREG"));
		strNoRegistradas.append("\n	 </td>");
		strNoRegistradas.append("\n	</tr>");
		strNoRegistradas.append("\n  </table>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	    <td colspan=2 class='tabmovtex'>");
		strNoRegistradas.append("\n	     <table border=0 cellpadding=0 cellspacing=0 width='100%'>");
		strNoRegistradas.append("\n	      <tr>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'><INPUT TYPE=text NAME=AbonoNoReg SIZE=22 MAXLENGTH=20></td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'> &nbsp; <input type=text name=CveDivisa maxlength=5 size=5 onFocus='blur();' value='"+inicialCveDivisa+"' onChange='SeleccionaDivisa();'>");
		strNoRegistradas.append("<input type=text name=Moneda onFocus='blur();' value='"+inicialDesDivisa+"' onChange='SeleccionaDivisa();'>");
		strNoRegistradas.append("\n	       </td>");
		strNoRegistradas.append("\n	      </tr>");
		strNoRegistradas.append("\n	     </table>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td colspan=4 class='tabmovtex'><br></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Pa&iacute;s:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>");
		strNoRegistradas.append("\n	     <SELECT NAME=Pais class='tabmovtex' onChange='verificaPais();'>");
		strNoRegistradas.append("\n	      <option value=0 class='tabmovtex'> Seleccione un Pa&iacute;s</OPTION>");
		strNoRegistradas.append(traePaises());
		strNoRegistradas.append("\n	     </SELECT>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' width='110'>Clave ABA:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><INPUT TYPE=text SIZE=10 MAXLENGTH=9 NAME=ClaveABA onFocus='verificaClaveABA();' onChange='TraerDatosDeClave(this.value);'>");
		strNoRegistradas.append("\n	     <a href='javascript:TraerClavesABA();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' >Ciudad:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' >");
		strNoRegistradas.append("\n	     <input type=hidden name=Ciudad value='01'>");
		strNoRegistradas.append("\n	     <input type=text size=35 name=Descripcion value='' onFocus='verificaCiudad();' maxlength=40>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Banco:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>");
		strNoRegistradas.append("\n	     <input type=hidden name=Banco value='01'>");
		strNoRegistradas.append("\n	     <input type=text size=25 name=DescripcionBanco value='' onFocus='verificaBanco();' maxlength=40>");
		strNoRegistradas.append("\n	     <a href='javascript:TraerBancos();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Beneficiario:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><INPUT TYPE=text SIZE=25 MAXLENGTH=40 NAME=Beneficiario></td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Concepto:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><INPUT TYPE=text SIZE=25 MAXLENGTH=60 NAME=ConceptoNoReg></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td colspan=2 class='tabmovtex'>");
		strNoRegistradas.append("\n	     <table border=0 cellpadding=0 cellspacing=0 width='100%'>");
		strNoRegistradas.append("\n	      <tr>");
		strNoRegistradas.append("\n	       <td class='tabmovtex' width='20%'>Importe MN:</td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex' width='25%'><INPUT TYPE=text SIZE=10 MAXLENGTH=10 NAME=ImporteNoReg value='' onBlur='cambiaImpMN(this,document.transferencia.ImporteNoRegDivisa,document.transferencia.ImporteNoRegUSD);'></td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'>Importe Divisa:</td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'><INPUT TYPE=text SIZE=10 MAXLENGTH=10 NAME=ImporteNoRegDivisa onBlur='cambiaImpDIV(this,document.transferencia.ImporteNoReg,document.transferencia.ImporteNoRegUSD);'></td>");
		strNoRegistradas.append("\n	      </tr>");
		strNoRegistradas.append("\n	     </table>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	   <td class='tabmovtex' colspan=2><br></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td colspan=2 class='tabmovtex'>");
		strNoRegistradas.append("\n	     <table border=0 cellpadding=0 cellspacing=0 width='100%'>");
		strNoRegistradas.append("\n	      <tr>");
		strNoRegistradas.append("\n	       <td class='tabmovtex' width='20%'>Importe Dlls:</td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex' width='25%'><INPUT TYPE=text SIZE=10 MAXLENGTH=10 NAME=ImporteNoRegUSD onBlur='cambiaImpUSD(this,document.transferencia.ImporteNoReg,document.transferencia.ImporteNoRegDivisa);'></td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'><br></td>");
		strNoRegistradas.append("\n	       <td class='tabmovtex'><br></td>");
		strNoRegistradas.append("\n	      </tr>");
		strNoRegistradas.append("\n	     </table>");
		strNoRegistradas.append("\n	    </td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' colspan=2>Requiere estado de cuenta fiscal? &nbsp; <input type=radio name=EdoFiscal value=0 checked> No &nbsp; <input type=radio name=EdoFiscal value=1> Si</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex' colspan=2><br></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Clave</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><input type=text size=10 maxlength=10 name=CveEspecialNoReg></td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>RFC Beneficiario:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><input type=text name=RFC size=13 maxlength=13 onFocus='verificaRFC(this,document.transferencia.EdoFiscal);'></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	   <tr>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Tipo de Cambio</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><input type=text size=10 maxlength=10 name=tipoEspecialNoReg onFocus='verificaClave(this,document.transferencia.CveEspecialNoReg);'></td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'>Importe IVA:</td>");
		strNoRegistradas.append("\n	    <td class='tabmovtex'><input type=text name=ImporteIVA size=13 maxlength=12 onFocus='verificaRFC(this,document.transferencia.EdoFiscal);'></td>");
		strNoRegistradas.append("\n	   </tr>");
		strNoRegistradas.append("\n	  </table>");
		strNoRegistradas.append("\n	 </td>");
		strNoRegistradas.append("\n	</tr>");
		strNoRegistradas.append("\n	<tr>");
		strNoRegistradas.append("\n	 <td bgcolor=white colspan=4><br></td>");
		strNoRegistradas.append("\n	</tr>");
		strNoRegistradas.append("\n	<input type=hidden name=FechaNoReg size=10 value='"+EnlaceGlobal.fechaHoy("dd/mm/aaaa")+"' onFocus='blur();'>");
	 }
	else {
	    // Para no perder el orden y numero de datos ...
	    EIGlobal.mensajePorTrace("No tiene la facultad - facCtasNoReg:"+facCtasNoReg, EIGlobal.NivelLog.INFO);

	    strNoRegistradas.append("    <input type=hidden name=Radio>\n");
	    strNoRegistradas.append("    <input type=hidden name=NCargo>\n");
	    strNoRegistradas.append("    <input type=hidden name=NAbono>\n");
	    strNoRegistradas.append("    <input type=hidden name=NClave>\n");
	    strNoRegistradas.append("    <input type=hidden name=NDivisa>\n");
	    strNoRegistradas.append("    <input type=hidden name=NPais>\n");
	    strNoRegistradas.append("    <input type=hidden name=NABA>\n");
	    strNoRegistradas.append("    <input type=hidden name=NCveCiudad>\n");
	    strNoRegistradas.append("    <input type=hidden name=NCiudad>\n");
	    strNoRegistradas.append("    <input type=hidden name=NCveBanco>\n");
	    strNoRegistradas.append("    <input type=hidden name=NBanco>\n");
	    strNoRegistradas.append("    <input type=hidden name=NBeneficiario>\n");
	    strNoRegistradas.append("    <input type=hidden name=NConcepto>\n");
	    strNoRegistradas.append("    <input type=hidden name=NImporteMN>\n");
	    strNoRegistradas.append("    <input type=hidden name=NImporteDIV>\n");
	    strNoRegistradas.append("    <input type=hidden name=NImporteUSD>\n");
	    strNoRegistradas.append("    <input type=hidden name=Nradio1\n");
	    strNoRegistradas.append("    <input type=hidden name=Nradio2>\n");
	    strNoRegistradas.append("    <input type=hidden name=NEspecial>\n");
	    strNoRegistradas.append("    <input type=hidden name=NRFC>\n");
	    strNoRegistradas.append("    <input type=hidden name=NTipoEspecial>\n");
	    strNoRegistradas.append("    <input type=hidden name=NImporte>\n");
	}
      return strNoRegistradas.toString();
 }



/*************************************************************************************

                               Generacion de Tabla - Modulo=1

*************************************************************************************/

/**
 * generar tabla de depositos
 * @param req Request 
 * @param res Response
 * @param contrato Contrato
 * @param tipoCuenta Tipo de cuenta
 * @param usuario Usuario
 * @param clavePerfil Clave de perfil
 * @param facImpArc FacImpArc
 * @param Reg instancia EI_Tipo
 * @param NoReg instancia EI_Tipo
 * @param facCtasNoReg Fac CtasNoReg
 * @param NoRegCorrectos instancia EI_Tipo
 * @throws ServletException Excepcion servlet
 * @throws IOException Excepcion io
 */
    public void generaTablaDepositos(HttpServletRequest req, HttpServletResponse res, String contrato, String tipoCuenta, String usuario, String clavePerfil, String facImpArc,EI_Tipo Reg, EI_Tipo NoReg,String facCtasNoReg, EI_Tipo NoRegCorrectos )

	    throws ServletException, IOException

    {

    //Variables MA a 390 - INI
    String montoUSDreal = "";
    String montoMNreal = "";
    String montoDivisareal = "";
	String[] datosBusDesBanco = new String[3];
	StringTokenizer token = null;
    //Variables MA a 390 - FIN


	String cadenaCuentas1="";

	String strTabla="";

	String strTablaArchivo="";

	String strBotones="";

	String strArchivo="";

	String strArchivoI="";

	String strArc="";


	String TCV_Venta="";

	String TCE_Venta="";

	String TCE_Dolar="";

	String inicialImporte="";

	String inicialDesDivisa="";

	String inicialCveDivisa="";

	String inicialCuentaCargo="";

	String[][] datosBcoExt=new String[2][2];


	String strBancoTxt="";

	String strCiudad="";

	String strEstado="";

	String strABA="";

	String mensajeError="";

	String catCvePais = "";



	EI_Tipo ArchivoI=new EI_Tipo(this);





		/************* Modificacion para la sesion ***************/

		HttpSession ses = req.getSession();

		BaseResource session = (BaseResource) ses.getAttribute(SESSION);

		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );



	int radio=0;

	int existeError=0;

	int cont=0;

	int contOk=0;



	double importeTotal=0;



	boolean contEsc=true;

	boolean importacion=false;

	boolean arcErrores=true;

	boolean errorEncontrado=false;



	EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Entrando a tabla depositos", EIGlobal.NivelLog.INFO);



	/* Datos de la pantalla inicial */

	TCV_Venta = (String) req.getParameter(TCV_VENTA);

	TCE_Venta = (String) req.getParameter(TCE_VENTA);

	TCE_Dolar = (String) req.getParameter(TCE_DOLAR);

	inicialImporte = (String) req.getParameter(INICIAL_IMPORTE);

	inicialCveDivisa = (String) req.getParameter(INICIAL_CVE_DIVISA);

	inicialDesDivisa = (String) req.getParameter(INICIAL_DESC_DIVISA);

	inicialCuentaCargo = (String) req.getParameter(INICIAL_CTA_CARGO);

	radio = Integer.parseInt( (String) req.getParameter(REGISTRADAS));

	montoUSDreal = (String) req.getParameter(IMPORTE_REG_USD);
    montoMNreal = (String) req.getParameter(IMPORTE_REG);
    montoDivisareal = (String) req.getParameter(IMPORTE_REG_DIVISA);

    EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos():: montoUSDreal =  " + montoUSDreal, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos():: montoMNreal =  " + montoMNreal, EIGlobal.NivelLog.DEBUG);
    EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos():: montoDivisareal =  " + montoDivisareal, EIGlobal.NivelLog.DEBUG);


	cadenaCuentas1+=generaCuentaCargo(inicialCuentaCargo,"REG");



		importeTotal=0;

/************************************************** Agregar Cuentas Registradas */

	if(radio==0 || Reg.totalRegistros>=1)

	  for(int i=0;i<Reg.totalRegistros;i++)

	    importeTotal+=new Double(Reg.camposTabla[i][4]).doubleValue();



		EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Registradas =  "+Reg.totalRegistros, EIGlobal.NivelLog.INFO);

/************************************************** Agregar Cuentas No Registradas */

	if(radio==1 || NoReg.totalRegistros>=1)

	 for(int i=0;i<NoReg.totalRegistros;i++) /* AQUI PAU DEBE BUSCAR UN ERROR*/

	    importeTotal+=new Double(NoReg.camposTabla[i][4]).doubleValue();



		EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): No Registradas =	"+NoReg.totalRegistros, EIGlobal.NivelLog.INFO);



	/*********************************************************************

        	Checar la claveABA y validar los datos del banco *

	*********************************************************************/

		if(NoReg.totalRegistros>=1)

		 {

		   for(int i=0;i<NoReg.totalRegistros;i++)

		    {

		      errorEncontrado=false;

		      mensajeError="";



		      strABA=NoReg.camposTabla[i][9];

		      strCiudad=NoReg.camposTabla[i][10];

		      strBancoTxt=NoReg.camposTabla[i][11];



		      if("USA".equals(NoReg.camposTabla[i][8].trim()))

		       {

			  EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): El banco no es USA", EIGlobal.NivelLog.INFO);

			  int ind=strCiudad.lastIndexOf(",");

			  if(!strCiudad.trim().equals("") && ind>=0)

			   {

			     strEstado=strCiudad.substring(ind+1,strCiudad.length()).trim();

			     strCiudad=strCiudad.substring(0,ind);

			   }

			  else

			   {

			     strEstado="";

			     strCiudad="";

			   }



			  datosBcoExt=traeDatosBancoExt(strBancoTxt,strCiudad,strEstado,strABA);

			  if(datosBcoExt[0][0].equals(ERROR))

			   {

			     EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): No se encontraron los datos de la clave", EIGlobal.NivelLog.INFO);

			     errorEncontrado=true;

			     mensajeError="cuadroDialogo('Error al Buscar los datos en los catalogos\\n Intente mas tarde.', 4);";

			   }

			  else

			   {

			     if(datosBcoExt[0][0].equals(VACIO))

			      {

				if("BC".equals(datosBcoExt[0][1])) 
				{
				  mensajeError="cuadroDialogo('La clave ABA proporcionada no existe.', 4);";
				}
				if("ABA".equals(datosBcoExt[0][1]))
				{
				  mensajeError="cuadroDialogo('No existe ese banco en la ciudad seleccionada:  <br> Banco: "+strBancoTxt+"<br> Ciudad: "+strCiudad+"', 4);";
				}
				errorEncontrado=true;

			      }

			     else

			      {

				if("BC".equals(datosBcoExt[0][0])) //trae banco y ciudad

				 {

				   strBancoTxt=datosBcoExt[0][1];

				   strCiudad=datosBcoExt[1][0];

				  }

				 else

				   strABA=datosBcoExt[1][0];

			      }

			    }

			 }



		       if("null".equals(strABA.trim()))

			{

			  EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Clave ABA NULL.", EIGlobal.NivelLog.INFO);

			  strABA="";

			}

		    //TODO BIT CU3131 Geenra tabla depositos
			//TODO BIT CU2101, verifica clave ABA
			/**
			 * VSWF-***-I
			 * 15/Enero/2007	Modificaci�n para el modulo de Tesorer�a
			 */
		       if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try {
				BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaTransacBean bt = new BitaTransacBean();
				BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
					bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_VALIDA_CLAVE_ABA);
				}

				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
					bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_CONSULTA_DB);
				}
				if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
						equals(BitaConstants.EC_SALDO_CONS_CHEQUE)){
					bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_CONS_COTIZA_INTER_DEPOSITO);
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if(datosBcoExt[0][0]!=null){
					if(datosBcoExt[0][0].length()>8){
						bt.setIdErr(datosBcoExt[0][0].substring(0,8));
					}else{
						bt.setIdErr(datosBcoExt[0][0].trim());
					}
				}else{
					bt.setIdErr(" ");
				}
				if (strBancoTxt != null) {
					bt.setBancoDest(strBancoTxt.trim());
				}
				if (inicialImporte != null) {
					bt.setImporte(Double.parseDouble(inicialImporte.trim()));
				}
				if (inicialCuentaCargo != null) {
					if(inicialCuentaCargo.length() > 20){
						bt.setCctaOrig(inicialCuentaCargo.substring(0,20));
					}else{
						bt.setCctaOrig(inicialCuentaCargo.trim());
					}
				}
				if (TCV_Venta != null) {
					bt.setTipoCambio(Double.parseDouble(TCV_Venta.trim()));
				}

				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
		       }
			/**
			 * VSWF-***-F
			 */


		       /****************  Asignar nuevos datos a la trama *******************/

		       if(!errorEncontrado)

			{

			   EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Clave ABA encontrada.", EIGlobal.NivelLog.INFO);

			   NoReg.camposTabla[i][9]=strABA;

			   NoReg.camposTabla[i][11]=strBancoTxt;

			   NoReg.camposTabla[i][10]=strCiudad;



			   int a=0;

			   for(a=0;a<NoReg.totalCampos;a++)

			     NoRegCorrectos.strOriginal+=NoReg.camposTabla[i][a]+"|";

			   NoRegCorrectos.strOriginal+=NoReg.camposTabla[i][a];

			   NoRegCorrectos.strOriginal+="@";

			}

		       /*********************************************************************/

		    }

		 }



	/* Despliega informacion en pantalla

	strTabla+="<br> Banco: "+strBancoTxt;

	strTabla+="<br> Ciudad: "+strCiudad;

	strTabla+="<br> ABA: "+strABA;

	strTabla+="<br> Estado: "+strEstado;

	strTabla+="<br> Mensaje: "+mensajeError;

	strTabla+="<br> NuevaTrama: "+NoRegCorrectos.strOriginal;

	*/



	NoRegCorrectos.iniciaObjeto(NoRegCorrectos.strOriginal);

/*************************************** Generar Archivo de Exportacion *********/

	if(NoRegCorrectos.totalRegistros>=1 || Reg.totalRegistros>=1)

	 {

	   String nombreArchivo=usuario+".doc";

	   String arcLinea="";



	   EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

	   EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

	   if(ArcSal.creaArchivo())

	    {

	      arcLinea="";

	      int c[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13};       // Indice del elemento deseado

	      int b[]={16,20,20,20,20,15,15,15,15,15,15,15,15,15,15};  // Longitud maxima del campo para el elemento



	      EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDeposito(): Exportando Cuentas no registradas.", EIGlobal.NivelLog.INFO);

	      for(int i=0;i<NoRegCorrectos.totalRegistros;i++)

	       {

		 arcLinea="";

		 for(int j=0;j<12;j++)

		   arcLinea+=ArcSal.formateaCampo(NoRegCorrectos.camposTabla[i][c[j]],b[j]);

		 arcLinea+="\n";

		 if(!ArcSal.escribeLinea(arcLinea))

		   EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Algo salio mal escribiendo "+strArc, EIGlobal.NivelLog.INFO);

	       }



	      EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDeposito(): Exportando Cuentas registradas.", EIGlobal.NivelLog.INFO);

	      for(int i=0;i<Reg.totalRegistros;i++)

	       {

		 arcLinea="";

		 for(int j=0;j<8;j++)

		   arcLinea+=ArcSal.formateaCampo(Reg.camposTabla[i][c[j]],b[j]);

		 arcLinea+="\n";

		 if(!ArcSal.escribeLinea(arcLinea))

		   EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): Algo salio mal escribiendo: "+strArc, EIGlobal.NivelLog.INFO);

	       }

	      ArcSal.cierraArchivo();

	    }

	   else

	    {

	      //strTabla+="<b>Exportacion no disponible<b>";

	      EIGlobal.mensajePorTrace("MTI_Transferencia - generaTablaDepositos(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);

	    }

	 }

/*************************************** Fin de Exportacion de Archivo *********/



	if(NoRegCorrectos.totalRegistros>=1 || Reg.totalRegistros>=1 || importacion)

		  strBotones+="<A href='javascript:Enviar();' border = 0><img src='/gifs/EnlaceMig/gbo25390.gif' border=0></a>";



/************************************************** Salida de datos en pantalla */



	Global.configura(Global.ARCHIVO_CONFIGURACION);

	ArchivoI.iniciaObjeto(strArchivoI);

	if(radio==0 || Reg.totalRegistros>=1)

	 {

		//SE ACTUALIZA LOS DATOS DEL BANCO CON LA INFORMACI�N CORRECT
		//INICIA MIGUEL ANGEL CAMBIOS
		EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos:: Inicia cambio sobre extracci�n de banco", EIGlobal.NivelLog.DEBUG);

		if(req.getParameter("busCvePais")!=null)
			catCvePais = (String) req.getParameter("busCvePais");
		if(req.getParameter("busCveAbaSwift")!=null)
			strABA = (String) req.getParameter("busCveAbaSwift");
		if(req.getParameter(BUS_DESC_BANCO)!=null){
			token = new StringTokenizer((String) req.getParameter(BUS_DESC_BANCO),"|");
			for(int j=0; j<3; j++)
				datosBusDesBanco[j] = token.nextToken();
			strBancoTxt = datosBusDesBanco[2];
		}
		if(req.getParameter(BUS_DESC_CIUDAD)!=null)
			strCiudad = (String) req.getParameter(BUS_DESC_CIUDAD);

		EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos:: catCvePais->" + catCvePais, EIGlobal.NivelLog.DEBUG);
		Reg.camposTabla[0][9] = strABA;
	    Reg.camposTabla[0][11] = strBancoTxt;
	    Reg.camposTabla[0][10] = strCiudad;
		EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos:: strABA->" + Reg.camposTabla[0][9], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos:: strBancoTxt->" + Reg.camposTabla[0][11], EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MTI_Transferencia::generaTablaDepositos:: strCiudad->" + Reg.camposTabla[0][10], EIGlobal.NivelLog.DEBUG);
		//FIN MIGUEL ANGEL CAMBIOS

	   //EnlaceGlobal.formateaImporte(Reg, EIGlobal.NivelLog.INFO);  //Importe

	   strTabla+=generaTabla("Cuentas Registradas",0,Reg,true,req);

	   Reg.strOriginal=strEspecial(Reg);

	   if(NoRegCorrectos.totalRegistros>=1)

	     strTabla+="<br>";

	 }



	if(radio==1 || NoRegCorrectos.totalRegistros>=1)

	 {

	    //EnlaceGlobal.formateaImporte(NoReg, EIGlobal.NivelLog.INFO); //Importe

	    strTabla+=generaTabla("Cuentas No Registradas",1,NoRegCorrectos,true,req);

	    NoRegCorrectos.strOriginal=strEspecial(NoRegCorrectos);

	 }



	req.setAttribute("cuentasAsociadasCargo", cadenaCuentas1);



	req.setAttribute("Fecha",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

	req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));

	req.setAttribute("NumeroRegistros",Integer.toString(Global.MAX_REGISTROS));



	req.setAttribute("mensajeError",mensajeError);

	req.setAttribute(TCV_VENTA,TCV_Venta);

	req.setAttribute(TCE_VENTA,TCE_Venta);

	req.setAttribute(TCE_DOLAR,TCE_Dolar);

	/*MSD Q05-24447*/

	req.getSession().setAttribute(TCE_DOLAR,TCE_Dolar);
	/*MSD Q05-24447*/

	req.setAttribute(INICIAL_IMPORTE,inicialImporte);

	req.setAttribute(INICIAL_DESC_DIVISA,inicialDesDivisa);

	req.setAttribute(INICIAL_CVE_DIVISA,inicialCveDivisa);

	req.setAttribute(INICIAL_CTA_CARGO,inicialCuentaCargo);



	req.setAttribute("Botones",strBotones);

	req.setAttribute(MODULO,"1");

	req.setAttribute("ModuloTabla","2");

	req.setAttribute("cuentasNoRegistradas",cuentasNoRegistradas(tipoCuenta,facCtasNoReg,inicialCuentaCargo,inicialCveDivisa,inicialDesDivisa,inicialImporte,req));

	req.setAttribute(TRANS_REG,Reg.strOriginal);

	req.setAttribute(TRANS_NO_REG,NoRegCorrectos.strOriginal);



	req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());

	req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());

	req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());

	req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());



	req.setAttribute("Lineas",Integer.toString(cont));

	req.setAttribute("Correctas",Integer.toString(contOk));



	req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());

	req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());

	req.setAttribute(ENCABEZADO, CreaEncabezado(TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req));



	req.setAttribute("strImportar",importarArchivo(facImpArc));

	req.setAttribute("Tabla",strTabla);
	req.setAttribute("muestraBusquedaBancos", "N");

	req.getSession().setAttribute("catCvePais", catCvePais);
	req.getSession().setAttribute("montoMNreal", montoMNreal);
	req.getSession().setAttribute("montoUSDreal", montoUSDreal);
	req.getSession().setAttribute("montoDivisareal", montoDivisareal);

	//req.setAttribute("Result",Result + session.getDivisaCuentaInter());



	if(NoRegCorrectos.totalRegistros>=1 || Reg.totalRegistros>=1)

	 {

	   req.setAttribute("totalRegistros","Total de Registros: "+Integer.toString(Reg.totalRegistros+NoRegCorrectos.totalRegistros));

	   req.setAttribute("importeTotal","Importe Total: "+FormatoMoneda((double)importeTotal));

	   req.setAttribute("Mensaje","Elimine la marca de las operaciones que no desea realizar<br>");

		 }



	evalTemplate("/jsp/MTI_Transferencia.jsp", req, res);

   }



/*************************************************************************************/

/************************************************** Pantalla Principal - Modulo=0    */

/*************************************************************************************/

    /**
     * Inicia depositos
     * @param req Request
     * @param res Response
     * @param tipoCuenta Tipo cuenta
     * @param facImpArc Fac Imp Arc
     * @param contrato Contrato
     * @param facCtasNoReg Fac Ctas No Reg
     * @return int Respuesta
     * @throws ServletException Excepcion servlet
     * @throws IOException Excepcion io
     */
  public int iniciaDepositos(HttpServletRequest req, HttpServletResponse res, String tipoCuenta, String facImpArc,String contrato, String facCtasNoReg)

	    throws ServletException, IOException

  {
	  	EIGlobal.mensajePorTrace(" **** MTI_Transferencia - iniciaDepositos ****", EIGlobal.NivelLog.INFO);

	  /*STFQRO 31012011 INI*/
		ServicioTux tuxGlobal = new ServicioTux();
		Integer referencia = 0 ;
		String codError = "";
		TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();
		DecimalFormat df1 = new DecimalFormat("###,###.000000");
		/*STFQRO 31012011 FIN*/



      String TCV_Venta="";

      String TCE_Venta="";

      String TCE_Dolar="";

      String inicialImporte="";

      String inicialDesDivisa="";

      String inicialCveDivisa="";

      String inicialCuentaCargo="";

      String tipoCambioDolar="";

      String cadenaCuentas1="";



      String tipoOperacion = "";

      String monedaDestino = "";

      String monedaOrigen  = "";

      String importeTrans  = "";

      //Modif PVA 07/08/2002

      String Directo_Inverso="";




      String Trama		 ="";

	  String Result 	 ="";

	  Hashtable hsResult = null;



      boolean servicioErroneo=false;



      EI_Tipo CuentaIni = new EI_Tipo(this);

      EI_Tipo Serv = new EI_Tipo(this);



	  /************* Modificacion para la sesion ***************/

	  HttpSession sess = req.getSession();

	  BaseResource session = (BaseResource) sess.getAttribute(SESSION);

	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );



//STFQRO 31012011   ServicioTux tuxGlobal = new ServicioTux();

      //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



      /* Datos de la pantalla inicial */

      if(req.getParameter("Importe") == null || req.getParameter("CveDivisa") == null ||
			   req.getParameter("DesDivisa") == null || req.getParameter("Cuentas") == null || req.getParameter(TCV_VENTA) == null)
      {
    		  inicialImporte =  req.getSession().getAttribute("Importe").toString().trim();

    	      inicialCveDivisa =  req.getSession().getAttribute("CveDivisa").toString().trim();

    	      inicialDesDivisa =  req.getSession().getAttribute("DesDivisa").toString().trim();

    	      inicialCuentaCargo =  req.getSession().getAttribute("Cuentas").toString().trim();


    	      tipoCambioDolar =  req.getSession().getAttribute(TCV_VENTA).toString().trim();

    	      Directo_Inverso    = req.getSession().getAttribute("DirectoInverso").toString().trim();
    	  } else {
    		  inicialImporte = (String) req.getParameter("Importe").trim();

    	      inicialCveDivisa = (String) req.getParameter("CveDivisa").trim();

    	      inicialDesDivisa = (String) req.getParameter("DesDivisa").trim();

    	      inicialCuentaCargo = (String) req.getParameter("Cuentas").trim();

    	      tipoCambioDolar = (String) req.getParameter(TCV_VENTA).trim();

    	      Directo_Inverso = (String) req.getParameter("DirectoInverso").trim();
    	  }



	  session.setDivisaCuentaInter(inicialCveDivisa);



//      TCV_Venta=tipoCambioVentanillaDivisa(inicialCveDivisa);



      //Por los nuevos cambios y lo de integracion siempre habra cuenta seleccionada.

      cadenaCuentas1+=generaCuentaCargo(inicialCuentaCargo,"REG");



      Global.configura(Global.ARCHIVO_CONFIGURACION);



      /*********************************************************************

      Llamada al servicio de cotizacion

      *********************************************************************/


      CuentaIni.iniciaObjeto(inicialCuentaCargo);

      if(CuentaIni.camposTabla[0][2].trim().equals("6"))

    	  monedaOrigen=DA;

      else

    	  monedaOrigen="MN";

      monedaDestino=inicialCveDivisa;

      importeTrans=inicialImporte;

      tipoOperacion="VTA";

      /*STFQRO 31012011 INI*/
      try {
	      if(!(monedaOrigen.equals(DA) && !monedaOrigen.equals(USD))) {
             Directo_Inverso = "I";}
    	  Hashtable hash = tuxGlobal.sreferencia("901");
    	  EIGlobal.mensajePorTrace("***********R E F E R E N C I A***********",	EIGlobal.NivelLog.INFO);
    	  EIGlobal.mensajePorTrace("Hashtable hs = tuxGlobal.sreferencia('901') - "+ hash.toString(), EIGlobal.NivelLog.INFO);


    	  codError = hash.get("COD_ERROR").toString();


    	  // VERIFICANDO QUE LA REFERENCIA SEA CORRECTA
    	  if (codError.endsWith("0000")) {
    		  referencia = (Integer) hash.get("REFERENCIA");
	    	  if((monedaOrigen.equals(DA) || monedaOrigen.equals(USD)) &&
	    	     (monedaDestino.equals(DA) || monedaDestino.equals(USD)) ){
	    		  EIGlobal.mensajePorTrace("MTI_Transferencia - iniciaCotizar(): Transferencia dolar - dolar.", EIGlobal.NivelLog.INFO);
	    		  //Result="OK	      96818013:20:07|"+tipoCambioDolar+"|1.0000|D|1.0000|1.000000|";
	    		  // Tipo Camb. Ventanilla
	    		  TCV_Venta = tipoCambioDolar;
	    		  //Tipo Camb. Enlace USD contra Peso
	    		  EIGlobal.mensajePorTrace("CUENTA CARGO->"+CuentaIni.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
	    		  //TCE_Venta = tipoCambio.getCambioSugerido(CuentaIni.camposTabla[0][0],
	    		  //	                                   "VTA",
	    		  //		                                   (inicialCveDivisa.trim().equals("MN")?"MXP":inicialCveDivisa),
	    		  //		                                   getDivisaTrx(CuentaIni.camposTabla[0][0]),"TRAN");
	    		  TCE_Venta = tipoCambioDolar;
	    		  // Tipo Camb. Enlace USD contra Dol�r	    		  
	    		  TCE_Dolar = "1.0000";
	    		 // Directo_Inverso="I";
	    	  }else{
    		  	  // Tipo Camb. Ventanilla
	    		  EIGlobal.mensajePorTrace("CUENTA CARGO->" + CuentaIni.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
    		  	  TCV_Venta = tipoCambio.getCambioVentanilla(CuentaIni.camposTabla[0][0],
    		  			                                     tipoOperacion,
    		  			                                    (inicialCveDivisa.trim().equals("MN")?"MXP":inicialCveDivisa),
    		  			                                     getDivisaTrx(CuentaIni.camposTabla[0][0]),"TRAN");
    		  	  // Tipo Camb. Enlace Divisa contra Peso
    			  TCE_Venta = tipoCambio.getCambioSugerido(CuentaIni.camposTabla[0][0],
    					  								   "VTA",
								                           (inicialCveDivisa.trim().equals("MN")?"MXP":inicialCveDivisa),
								                           "MXP","TRAN");
    			  if(monedaDestino.equals(DA) || monedaDestino.equals(USD)){
    				  TCE_Dolar = "1.00000000";
    			  }else{
    				  // Tipo Camb. Enlace Divisa contra Dol�r
	    			  TCE_Dolar = tipoCambio.getCambioSugerido(CuentaIni.camposTabla[0][0],
								   	                           "VTA",
								   	                           (inicialCveDivisa.trim().equals("MN")?"MXP":inicialCveDivisa),
								   	                           USD,"TRAN");
    			  }
    			 // Directo_Inverso="I";//Serv.camposTabla[0][3]; //Siempre viene un D o I

    			  EIGlobal.mensajePorTrace("MTI_Transferencia - iniciaTablaDepositos(): Directo_Inverso: "+Directo_Inverso, EIGlobal.NivelLog.DEBUG);
	    	  }
	    	  codError = "DIIN0000";
    	  }
	  } catch (Exception e) {
		  e.printStackTrace();
		  codError = "DIIN9999";
		  servicioErroneo=true;
	  }
/*STFQRO 31012011 INI

        Trama=IEnlace.medioEntrega1+"|"+session.getUserID8()+"|DICO|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+CuentaIni.camposTabla[0][0].trim()+"@TRAN@"+tipoOperacion+"@"+monedaDestino+"@"+monedaOrigen+"@"+importeTrans+"@@";

        EIGlobal.mensajePorTrace("MTI_Transferencia - iniciaTablaDepositos(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);



       if(

	   (monedaOrigen.equals(DA) || monedaOrigen.equals(USD)) &&

	   (monedaDestino.equals(DA) || monedaDestino.equals(USD))

	  )

	 {

	   EIGlobal.mensajePorTrace("MTI_Cotizar - iniciaCotizar(): Transferencia dolar - dolar.", EIGlobal.NivelLog.INFO);

	   Result="OK	      96818013:20:07|"+tipoCambioDolar+"|1.0000|D|1.0000|1.000000|";

	 }

     else

	 {

	  try

	   {

	      hsResult = tuxGlobal.web_red(Trama);

	   } catch ( java.rmi.RemoteException re )

	    {

	       re.printStackTrace();

	    } catch(Exception e) {}

	   Result=(String)hsResult.get( "BUFFER" );
	 }
STFQRO 31012011 FIN*/

	   //#########

	   //prueba Desarrollo

	   //Result="OK 	96818013:20:07|1.000|1.0000|D|1.0000|1.000000|1.00000|";


       //TODO BIT CU3131 inicia depositos, operaci�n DICO
       //TODO BIT CU2101, Llamada al servicio de Contizaci�n
		/**
		 * VSWF-***-I
		 * 15/Enero/2007	Modificaci�n para el modulo de Tesrorer�a
		 */
	  if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
		try {
			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaTransacBean bt = new BitaTransacBean();
			BitaTCTBean beanTCT = new BitaTCTBean ();
			BitaHelper bh =	new BitaHelperImpl(req,session,req.getSession(false));
			bt = (BitaTransacBean)bh.llenarBean(bt);
			/*HGG-I*/
			if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ET_TRANSFERENCIAS_INTER)){
				 bt.setNumBit(BitaConstants.ET_TRANSFERENCIAS_INTER_COTIZA_INTERNACIONAL);
			}
			/*HGG-F*/
			 if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTER_TRANSFER_INTER)){
				 bt.setNumBit(BitaConstants.ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DICO);
			 }
			 if ((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).
					 equals(BitaConstants.EC_SALDO_CONS_CHEQUE)){
				 bt.setNumBit(BitaConstants.EC_SALDO_CONS_CHEQUE_CONS_COTIZA_INTERNACIONAL);
			 }
			 if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			 }
			 if(!servicioErroneo){
				bt.setIdErr("DICO0000");
			 }else{
				bt.setIdErr("DICO9999");
			 }
			 bt.setServTransTux("DICO");
			 if (importeTrans != null) {
				bt.setImporte(Double.parseDouble(importeTrans));
			}
			 if (tipoCambioDolar != null) {
				bt.setTipoCambio(Double.parseDouble(tipoCambioDolar.trim()));
			}
			 if (monedaDestino != null) {
				bt.setTipoMoneda(monedaDestino.trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
			//INI BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
			beanTCT = bh.llenarBeanTCT(beanTCT);
			beanTCT.setReferencia(referencia);
			beanTCT.setTipoOperacion("DICO");
			beanTCT.setCodError(codError);
			beanTCT.setUsuario(session.getUserID8());
		    BitaHandler.getInstance().insertBitaTCT(beanTCT);
			//FIN BITACORIZANDO EN LA TCT_BITACORA - MIGRACI�N MA A 390
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
       }
    }
		/**
		 * VSWF-***-F
		 */
/**
      EIGlobal.mensajePorTrace("MTI_Transferencia - iniciaTablaDepositos(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);



      if(Result!=null || Result.equals("null"))

       if(!Result.substring(0,2).equals("OK"))

	 servicioErroneo=true;

       else

	 {

	   Serv.iniciaObjeto(Result+"@");

	   TCE_Venta=Serv.camposTabla[0][1];

	   TCE_Dolar=Serv.camposTabla[0][2];

	   //Modif PVA 07/08/2002

	   Directo_Inverso=Serv.camposTabla[0][3];

	   EIGlobal.mensajePorTrace("MTI_Transferencia - iniciaTablaDepositos(): Directo_Inverso: "+Directo_Inverso, EIGlobal.NivelLog.DEBUG);

	 }*/



      req.setAttribute(INICIAL_IMPORTE,inicialImporte);

      req.setAttribute(INICIAL_DESC_DIVISA,inicialDesDivisa);

      req.setAttribute(INICIAL_CVE_DIVISA,inicialCveDivisa);

      req.setAttribute(INICIAL_CTA_CARGO,inicialCuentaCargo);



      req.setAttribute(TCV_VENTA,TCV_Venta);

      req.setAttribute(TCE_VENTA,TCE_Venta);

      req.setAttribute(TCE_DOLAR,TCE_Dolar);

      //Modif PVA 07/08/2002

      req.getSession().setAttribute("Directo_Inverso",Directo_Inverso);

      req.setAttribute("Result",Result + session.getDivisaCuentaInter());

	  req.setAttribute("mensajeError","");

	  req.setAttribute("Lineas","");

	  req.setAttribute("importeTotal","");

	  req.setAttribute("ModuloTabla","");


	  if(!verificaCuenta(CuentaIni.camposTabla[0][0], inicialCveDivisa, getDivisaTrx(CuentaIni.camposTabla[0][0]),req, res)){
	  /**
      if(datosCot==null || datosCot.length<0)

    	  despliegaErrorDeDivisa(datosCot,inicialCveDivisa,req,res);

      else*/
		  EIGlobal.mensajePorTrace("MTI_Transferencia::iniciaDepositos:: La cuenta NO es operable",EIGlobal.NivelLog.DEBUG);
	  }else{
      if(servicioErroneo)
      {
    	  despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.",TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req,res);
      }
      else

       {

	  req.setAttribute("cuentasAsociadasCargo",cadenaCuentas1);

	  req.setAttribute("Fecha",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

	  req.setAttribute(MODULO,"1");

	  req.setAttribute("cuentasNoRegistradas",cuentasNoRegistradas(tipoCuenta,facCtasNoReg,inicialCuentaCargo,inicialCveDivisa,inicialDesDivisa,inicialImporte,req));

	  req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));

	  req.setAttribute("NumeroRegistros",Integer.toString(Global.MAX_REGISTROS));

	  req.setAttribute("strImportar",importarArchivo(facImpArc));



	  req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());

		  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());

		  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());

		  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());



	  req.setAttribute(TRANS_REG,"");

	  req.setAttribute(TRANS_NO_REG,"");

	  req.setAttribute("TransArch","");



	  req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());

	  req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());

	  req.setAttribute(ENCABEZADO, CreaEncabezado(TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req));

/*STFQRO 31012011 INI*/
	  EIGlobal.mensajePorTrace("*** REDIRECCIONANDO : /jsp/MTI_Transferencia.jsp ***", EIGlobal.NivelLog.DEBUG);
/*STFQRO 31012011 FIN*/
	  evalTemplate("/jsp/MTI_Transferencia.jsp", req, res);

       }
	  }

     return 1;

   }



/******************************************************************************/
/*********************************************************** trae Divisas     */
/******************************************************************************/
  /**
   * TIpo cambio ventanilla
   * @param divisa Divisa
   * @return String Respuesta
   */
public String tipoCambioVentanillaDivisa(String divisa) {
	String tcvVenta="";
	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();
/*STFQRO INI	String  query_realizar="";

	PreparedStatement TestQuery = null;
	ResultSet TestResult = null;
	Connection connbd = null;
STFQRO FIN*/
	EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Entrando a tipoCambioVentanilla", EIGlobal.NivelLog.INFO);

/*STFQRO 31012011	try {
		connbd = createiASConn(Global.DATASOURCE_ORACLE);
		if(connbd==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
			return "";
		}STFQRO 31012011 */

/*STFQRO 31012011 INI
		query_realizar="SELECT TO_CHAR(fecha,'YYYYMMDD hh24:mi:ss'),to_char(tc_cpa_vent,'999,999,999.000000'),to_char(tc_vta_vent,'999,999,999.000000')";
		query_realizar=query_realizar + " from comu_tc_base ";
		query_realizar=query_realizar + " WHERE cve_divisa = '"+divisa+"' ";
		query_realizar=query_realizar + " AND fecha = (select MAX(fecha) from comu_tc_base WHERE cve_divisa = '"+divisa+"')";

		EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Query"+query_realizar, EIGlobal.NivelLog.ERROR);

		TestQuery  = connbd.prepareStatement(query_realizar);

		if(TestQuery == null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Cannot Create Query", EIGlobal.NivelLog.ERROR);
STFQRO 31012011 FIN*/
			try {
				//tcvVenta = tipoCambio.getCambioVentanilla("VTA", divisa);
				Long.parseLong(tcvVenta);
//				connbd.close();
			}catch(Exception e){
		EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla"+e.getMessage(),
				EIGlobal.NivelLog.ERROR);

		tcvVenta = "";
	}
	/*STFQRO 31012011 FIN*/
/*STFQRO 31012011 EIGlobal.mensajePorTrace("TestQuery nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}

		TestResult = TestQuery.executeQuery();
		if(TestResult == null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
			connbd.close();
			try {
				TestQuery.close();
				connbd.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("TestResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}

		if(TestResult.next()) {
			tcvVenta= (TestResult.getString(3)==null)?"":TestResult.getString(3);
		}
	}catch( SQLException sqle ){
		EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla() Excepcion Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
	}
	finally {
		try {
			TestQuery.close();
			TestResult.close();
			connbd.close();
		}catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla() en finally", EIGlobal.NivelLog.INFO);
		}
	}STFQRO 31012011 */
	return tcvVenta.trim();
}



/****************************************************************************************/

/***************************   cuentas Asociadas Extranjeras   **************************/

/****************************************************************************************/

/**
 * PosCar
 * @param cad Cadena
 * @param car Caracter
 * @param cant Cantidad
 * @return int Respuesta
 */
public int posCar(String cad,char car,int cant)

 {

   int result=0;

   int pos=0;



   for(int i=0;i<cant;i++)

    {

      result=cad.indexOf(car,pos);

      pos=result+1;

    }

   return result;

 }



/*************************************************************************************/

/*** regresa cadena para la opcion de importacion desde archivo si tiene facultad    */

/*************************************************************************************/

/**
 * Importar Archivo
 * @param facImpArc Fac Imp Arch
 * @return String Respuesta 
 */
   String importarArchivo(String facImpArc)

    {

       String strImp="<!-- No Tiene Facultad. //-->";



       if("true".equals(facImpArc.trim()))

	{

	  strImp="\n  <tr>";

	  strImp+="\n	 <td class='tittabdat' colspan=4><INPUT TYPE='radio' value=2 NAME='Registradas' onClick='VerificaFac(2);'>&nbsp;<b>Archivo Externo</b></td>";

	  strImp+="\n  </tr>";



	  strImp+="\n  <tr>";

	  strImp+="\n	<td colspan=4 class='tabmovtex' align=center>Importar trasnferencias desde archivo &nbsp; <INPUT TYPE='file' NAME='Archivo' value=' Archivo ' onClick='javascript:Agregar();'></td>";

	  strImp+="\n  </tr>";

	}

       return strImp;

    }



/******************************************************************************/
/*********** Despliega Mensaje para mas de 30 transferencias desde archivo    */
/******************************************************************************/
  /**
   * Despliega mensaje
   * @return String Respuesta
   */
String despliegaMensaje() {

	StringBuffer msg= new StringBuffer();

	msg.append("\n	<table width=430 border=0 cellspacing=0 cellpadding=0 align=center>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n </table>");
	msg.append("\n <table width=400 border=0 cellspacing=0 cellpadding=0 align=center>");
	msg.append("\n	<tr>");
	msg.append("\n	 <td align=center>");
	msg.append("\n	   <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>");
	msg.append("\n	   <tr>");
	msg.append("\n	     <td colspan=3> </td>");
	msg.append("\n	   </tr>");
	msg.append("\n	   <tr>");
	msg.append("\n	     <td width=21 background='/gifs/EnlaceMig/gfo25030.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>");
	msg.append("\n	     <td width=428 height=100 valign=top align=center>");
	msg.append("\n	       <table width=380 border=0 cellspacing=2 cellpadding=3 background='/gifs/EnlaceMig/gau25010.gif'>");
	msg.append("\n		 <tr>");
	msg.append("\n		   <td class='tittabcom' align=center>");
	msg.append("\n		   <br><br><i>El archivo fue importado <font color=green>satisfactoriamente.</font></i><br>");
	msg.append("\n		   Las transferencias ya han sido registradas. <br>");
	msg.append("\n		   Ahora ya puede <font color=blue>continuar</font> con la cotizaci&oacute;n.<br>");
	msg.append("\n		   </td>");
	msg.append("\n		 </tr>");
	msg.append("\n	       </table>");
	msg.append("\n	     </td>");
	msg.append("\n	       <td width=21 background='/gifs/EnlaceMig/gfo25040.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width=21 height=2 name='..'></td>");
	msg.append("\n	   </tr>");
	msg.append("\n	 </table>");
	msg.append("\n	</td>");
	msg.append("\n	</tr>");
	msg.append("\n </table>");
	msg.append("\n <table width=430 border=0 cellspacing=0 cellpadding=0 align=center>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=8 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n	<tr>");
	msg.append("\n	  <td class='tittabdat' colspan=3><img src='/gifs/EnlaceMig/gau25010.gif' width=5 height=2 name='.'></td>");
	msg.append("\n	</tr>");
	msg.append("\n </table>");

	return msg.toString();
}





/*************************************************************************************/

/********************** genera Tabla con icono de informacion sobre transferencia    */

/*************************************************************************************/

/**
 * GEnera Tabla
 * @param titulo Titulo
 * @param ind Indicador
 * @param Trans Transferencia
 * @param RFC RFC
 * @param req Request
 * @return String Respuesta
 */
   String generaTabla(String titulo, int ind, EI_Tipo Trans,boolean RFC, HttpServletRequest req)

    {

/* Orden de datos en Objeto Trans

   0 Cargo

   1 Titular

   2 Abono

   3 Beneficiario

   4 Importe

   5 Concepto

   6 Divisa

   7 TCE_Venta

   8 Pais

   9 ClaveABA

  10 Ciudad

  11 Banco

  12 Fecha

  13 RFC

  14 IVA



  15 ClaveEspecial

  16 TipoCambioEspecial

*/

       /************* Modificacion para la sesion ***************/

	   HttpSession sess = req.getSession();

	   BaseResource session = (BaseResource) sess.getAttribute(SESSION);

	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );



       String[] titulos={ "",

			  "Cuenta Ordenante",

			  "Nombre Ordenante",

			  "Cuenta Beneficiario",

			  "Nombre Beneficiario",

			  //"Tipo de Cambio",

			  "Importe de Operacion",

			  "Concepto",

			  "Divisa",

			  "Banco",

			  "Ciudad",

			  "Pais",

			  "Clave ABA",

			  "RFC Beneficiario",

			  "Importe IVA",

			  "Clave"};



       // Con tipo de cambio

       // int datos[]={12,0,0,1,2,3,7,4,5,6,11,10,8,9,13,14,15};

       //int align[]={0,0,0,0,2,2,1,0,0,0,0,0,0,2,2,0,0};

       int datos[]={11,0,0,1,2,3,4,5,6,11,10,8,9,13,14,15};

       int values[]={2,0,1,2};

       int align[]={0,0,0,0,2,1,0,0,0,0,0,0,2,2,0,0};



       if(RFC)
       {
	 datos[0]=13;
       }


       //Checar si trae cve especial, asiganar el tipo de cambio y desplegarla

       for(int i=0;i<Trans.totalRegistros;i++)

	{

	  if(!Trans.camposTabla[i][15].trim().equals(""))

	   {

	     Trans.camposTabla[i][7]=Trans.camposTabla[i][16];

	     datos[0]=14;

	   }

	}



       EIGlobal.mensajePorTrace("MTI_Transferencia - generaTabla(): Genrando Tabla de Transferencias.", EIGlobal.NivelLog.INFO);



       titulos[0]=titulo;

       if(ind==0 || ind==1)
       {
	 datos[1]=2;
       }


       EnlaceGlobal.formateaImporte(Trans, 4);  //Importe

       Trans.camposTabla[0][4]=Trans.camposTabla[0][4].replace('$',' ').trim();

       return Trans.generaTabla(titulos,datos,values,align);

    }



/*************************************************************************************/

/*************************************************************************************/

/*************************************************************************************/

   /**
    * strEspecial
    * @param Reg Registro
    * @return String Respuesta
    */
   String strEspecial(EI_Tipo Reg)

     {

       int a=0;

       int b=0;



       for(int i=0;i<Reg.totalRegistros;i++)

	{

	  if(!Reg.camposTabla[i][15].trim().equals(""))
	  {
	     Reg.camposTabla[i][7]=Reg.camposTabla[i][16];
	  }
	}



       Reg.strOriginal="";

       for(b=0;b<Reg.totalRegistros;b++)

	{

	  for(a=0;a<Reg.totalCampos;a++)

	    Reg.strOriginal+=Reg.camposTabla[b][a]+"|";

	  Reg.strOriginal+=Reg.camposTabla[b][a];

	  Reg.strOriginal+="@";

	}

       return Reg.strOriginal;

     }



/******************************************************************************/
/** traePaises ****************************************************************/
/******************************************************************************/
   /**
    * Trae Paises
    * @return String Respuesta
    */
public String traePaises() {

	String strOpcion="";
	String sqlPais="";
	ResultSet PaisResult = null;
	Connection conn = null;
	PreparedStatement PaisQuery = null;

	try {
		conn = createiASConn(Global.DATASOURCE_ORACLE);

		if(conn==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traePaises(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
			return "";
		}

		sqlPais = "SELECT CVE_PAIS, DESCRIPCION from COMU_PAISES order by DESCRIPCION";
		PaisQuery  = conn.prepareStatement(sqlPais);

		if(PaisQuery==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traePaises(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			try {
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("PaisQuery nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}

			return "";
		}

		PaisResult = PaisQuery.executeQuery();

		if(PaisResult==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traePaises(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
			conn.close();
			try {
				PaisQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("PaisResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}

		while(PaisResult.next())
			strOpcion += "<option CLASS='tabmovtex' value='" + PaisResult.getString(1) + "'>" + PaisResult.getString(2) + "\n";
	}
	catch( SQLException sqle ) {
		EIGlobal.mensajePorTrace("MTI_Transferencia - traePaises(): Excepcion, Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
		return "";
	}
	finally {
		try {
			PaisQuery.close();
			PaisResult.close();
			conn.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traePaises() en finally", EIGlobal.NivelLog.INFO);
		}
	}
	return strOpcion;
}

/******************************************************************************/
/**	traeDivisas ***************************************************************/
/******************************************************************************/
/**
 * Trae Divisas
 * @param cveDivisa Clave Divisas
 * @return String Respuesta
 */
public String traeDivisas(String cveDivisa) {
	String strOpcion="";
	Vector divisas = null;
	EIGlobal.mensajePorTrace("MTI_Transferencia::traeDivisas:: Iniciando extracci�n de divisas...", EIGlobal.NivelLog.DEBUG);

	try{
		divisas = CatDivisaPais.getInstance().getListaDivisas();
    	for(int i=0; i<divisas.size(); i++){
    		strOpcion += "<option CLASS='tabmovtex' value='" + ((TrxGP93VO)divisas.get(i)).getOVarCod().trim() + "'>"
    		          +  ((TrxGP93VO)divisas.get(i)).getOVarDes().trim() + "\n";
    	}
    	EIGlobal.mensajePorTrace("MTI_Transferencia::traeDivisas:: saliendo de la extracci�n de divisas...", EIGlobal.NivelLog.DEBUG);
	}catch(Exception e){
		StackTraceElement[] lineaError;
		lineaError = e.getStackTrace();
		EIGlobal.mensajePorTrace("Error al realizar la extracci�n de divisas por la GP93.", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("MTI_Transferencia::traeDivisas:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
								+ e.getMessage()
	               				+ "<DATOS GENERALES>"
					 			+ "Linea encontrada->" + lineaError[0]
					 			, EIGlobal.NivelLog.DEBUG);
	}
	/**
	String sqlDivisa="";
	PreparedStatement DivisaQuery = null;
	ResultSet DivisaResult = null;
	Connection conn = null;

	try {
		conn = createiASConn(Global.DATASOURCE_ORACLE);

		if(conn==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDivisas(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
			return "";
		}

		sqlDivisa="SELECT CVE_DIVISA, UPPER(DESCRIPCION) FROM COMU_MON_DIVISA ORDER BY UPPER(DESCRIPCION)";
		DivisaQuery	= conn.prepareStatement(sqlDivisa);

		if(DivisaQuery==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDivisas(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			try {
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("DivisaQuery nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}

		DivisaResult = DivisaQuery.executeQuery();

		if(DivisaResult==null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDivisas(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
			conn.close();
			try {
				DivisaQuery.close();
				conn.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("DivisaResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return "";
		}

		while(DivisaResult.next()) {
			if(DivisaResult.getString(1).trim().equals(cveDivisa.trim()) )
				strOpcion+="<option CLASS='tabmovtex' value='"+DivisaResult.getString(1)+"' SELECTED>"+DivisaResult.getString(2)+"\n";
			else
				strOpcion+="<option CLASS='tabmovtex' value='"+DivisaResult.getString(1)+"' >"+DivisaResult.getString(2)+"\n";
		}
	}
	catch( SQLException sqle ) {
		EIGlobal.mensajePorTrace("MTI_Transferencia - traeDivisas(): Excepcion, Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
		return "";
	}
	finally {
		try {
			DivisaQuery.close();
			DivisaResult.close();
			conn.close();
		}catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDivisas() en finally", EIGlobal.NivelLog.INFO);
		}
	}**/
	return strOpcion;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/**
 * Trae DAtos Banco Exterior
 * @param strBancoTxt Banco
 * @param strCiudad Ciudad
 * @param strEdo Estado
 * @param strABA Aba
 * @return String[][] Respuesta
 */
String[][] traeDatosBancoExt(String strBancoTxt, String strCiudad, String strEdo, String strABA) {

	String queryDatosBanco="";
	String[][] datosBanco=new String[2][2];
	PreparedStatement contQuery  = null;
	ResultSet contResult = null;
	Connection conn = null;
	ServicioTux tuxGlobal = new ServicioTux();
	//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	if(strABA.length()>2)
		queryDatosBanco="SELECT NOMBRE, CIUDAD, ESTADO FROM CAMB_BANCO_EU WHERE CVE_ABA="+strABA;
	else
		queryDatosBanco="SELECT CVE_ABA FROM CAMB_BANCO_EU WHERE NOMBRE LIKE '"+strBancoTxt+"%' AND CIUDAD LIKE '"+strCiudad+"%' AND ESTADO='"+strEdo+"'";

	EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt(): Ejecutando query : "+queryDatosBanco, EIGlobal.NivelLog.INFO);

	try {
		conn = createiASConn(Global.DATASOURCE_ORACLE);
		datosBanco[0][0]=ERROR;
		datosBanco[0][1]="NULL";
		datosBanco[1][0]="";
		datosBanco[1][1]="";

		if(conn == null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
		}
		else {
			contQuery  = conn.prepareStatement(queryDatosBanco);
			if(contQuery == null) {
				EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
				conn.close();
			}
			else {
				contResult = contQuery.executeQuery();
				if(contResult==null) {
					EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					conn.close();
					try {
						contQuery.close();
						conn.close();
					}catch(Exception e){
						EIGlobal.mensajePorTrace("contResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
					}
				}
				else {
					if (strABA.length()>2) {
						if (contResult.next()) {
							datosBanco[0][0]="BC"; //trae Banco, Ciudad y Estado
							datosBanco[0][1]=contResult.getString(1);
							datosBanco[1][0]=contResult.getString(2);
							datosBanco[1][1]=contResult.getString(3);
						}
						else {
							datosBanco[0][0]=VACIO;
							datosBanco[0][1]="BC";
							datosBanco[1][0]="";
							datosBanco[1][1]="";
						}
					}
					else {
						if (contResult.next()) {
							datosBanco[0][0]="ABA"; //trae Clave ABA
							datosBanco[0][1]="";
							datosBanco[1][0]=contResult.getString(1);
							datosBanco[1][1]="";
						}
						else {
							datosBanco[0][0]=VACIO;
							datosBanco[0][1]="ABA";
							datosBanco[1][0]="";
							datosBanco[1][1]="";
						}
					}
				}
			}
		}
	}
	catch(Exception e) {
		EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt(): Excepcion, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);
	}
	finally {
		try {
			contQuery.close();
			contResult.close();
			conn.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - traeDatosBancoExt() en finally", EIGlobal.NivelLog.INFO);
		}
	}
	return datosBanco;
}

/*************************************************************************************
							verifica info Cuenta
*************************************************************************************/

/**
 * Verifica info cuenta
 * @param claveABA Clave ABA
 * @return String[] Respuesta
 */
 String[] verificaInfoCuenta(String claveABA)

    {

      String[][] datosBcoExt=new String[2][2];



      String mensajeError="";

      String strBancoTxt="";

      String strCiudad="";

      String strABA="";

      String[] Result=new String[5];



      boolean errorEncontrado=false;



      datosBcoExt=traeDatosBancoExt("","","",claveABA);

      if(datosBcoExt[0][0].equals(ERROR))

	{

	  errorEncontrado=true;

	  mensajeError="Error al Buscar los datos en los catalogos\\n Intente mas tarde";

	}

       else

	{

	  if(datosBcoExt[0][0].equals(VACIO))

	   {

	     if("BC".equals(datosBcoExt[0][1]))
	     {
	       mensajeError="La clave ABA proporcionada no existe:  <br> Clave ABA: "+strABA;
	     }
	     if("ABA".equals(datosBcoExt[0][1]))
	     {
	       mensajeError="No existe ese banco en la ciudad seleccionada:  <br> Banco: "+strBancoTxt+"<br> Ciudad: "+strCiudad;
	     }
	     errorEncontrado=true;

	   }

	  else

	   {

	     if("BC".equals(datosBcoExt[0][0])) //trae banco y ciudad

	      {

		Result[0]="OK";

		strBancoTxt=datosBcoExt[1][0];

		strCiudad=datosBcoExt[1][1];

	      }

	     else

	      strABA=datosBcoExt[1][0];

	   }

	}



     if(errorEncontrado)

      {

	Result[0]=ERROR;

	Result[1]=mensajeError;

      }

     else

      {

	Result[0]="OK";

	Result[1]="";

	Result[2]=strBancoTxt;

	Result[3]=strCiudad;

      }

     return Result;

    }



/************************************************************************************

								generaCuentaCargo

*************************************************************************************/

 /**
  * Genera Cuenta cargo
  * @param inicialCuentaCargo Cuenta cargo
  * @param reg Registro
  * @return String Respuesta
  */
 public String generaCuentaCargo(String inicialCuentaCargo,String reg)

    {

      String cuenta="";

	  String campo="";



      EI_Tipo Tmp=new EI_Tipo();



      EIGlobal.mensajePorTrace("MTI_Transferencia - generaCuentaCargo(): Entrando a buscar la cuenta.", EIGlobal.NivelLog.INFO);



	  if(reg.trim().equals("NOREG"))

	    campo="CargoNoReg";

	  else

	    campo="CargoReg";



      Tmp.iniciaObjeto(inicialCuentaCargo);

	  cuenta=Tmp.camposTabla[0][0]+" "+Tmp.camposTabla[0][1];

      cuenta+="\n	<input type=Hidden name="+campo+" VALUE=\""+Tmp.camposTabla[0][2] +"|"+Tmp.camposTabla[0][0]+"|"+Tmp.camposTabla[0][1]+"|"+"\"> ";



	  EIGlobal.mensajePorTrace("MTI_Transferencia - generaCuentaCargo(): Se regresa la cuenta: "+cuenta, EIGlobal.NivelLog.INFO);



      return cuenta;

    }



/****************************************************************************************

							despliegaTiempoTerminado

****************************************************************************************/

 /**
  * Despliega Error Divisa
  * @param tcv Tcv
  * @param inicialCveDivisa Divisa
  * @param req Request
  * @param res Response
  * @throws ServletException Excepcion
  * @throws IOException Excepcion
  */
  void despliegaErrorDeDivisa(String [] tcv,String inicialCveDivisa, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException

    {

      String strCuentas="";



      /************* Modificacion para la sesion ***************/

	  HttpSession sess = req.getSession();

	  BaseResource session = (BaseResource) sess.getAttribute(SESSION);

	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );



      //Pantalla inicio


      //Para detectar si el tipo de cambio es del d�a.

      if(tcv!=null)

	{

	  if(tcv[0].trim().indexOf(EnlaceGlobal.fechaHoy("aaaammdd"))<0)

	    req.setAttribute("TipoCambioError","El tipo de cambio no est&aacute; actualizado.");

	  else

	    req.setAttribute("TipoCambioError","");

	}

      else

	req.setAttribute("TipoCambioError","No se pudo obtener la informaci&oacute;n del tipo de cambio.");



      req.setAttribute(TCV_VENTA,tcv[2].trim());

      req.setAttribute("TCV_Compra",tcv[1].trim());

      req.setAttribute("Divisas",traeDivisas(inicialCveDivisa));

      req.setAttribute("TiempoTerminado","cuadroDialogo('No existe cotizaci&oacute;n para la divisa especificada, seleccione otra divisa.');");

      req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());

	  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());

	  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());

	  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

      req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));

      req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());

      req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());

      req.setAttribute(ENCABEZADO,CreaEncabezado(TRANS_INTERNAC,"Transferencias &gt; Internacionales","s27570h",req));

      evalTemplate("/jsp/MTI_Inicio.jsp", req, res);

    }



/******************************************************************************/
/** trae Divisas **************************************************************/
/******************************************************************************/
  /**
   * Verifica Cuenta
   * @param cuentaCargo Cuenta cargo
   * @param divisa Divisa
   * @param divisaCruce Divisa cruce
   * @param req Request
   * @param res Response
   * @return boolean Respuesta
   */
public boolean verificaCuenta(String cuentaCargo, String divisa, String divisaCruce, HttpServletRequest req, HttpServletResponse res) {
//STFQRO INI	String query_realizar="";
	String trama = "";
	String resultado = "";
	boolean cuentaOperable = false;
	TipoCambioEnlaceDAO tipoCambio = new TipoCambioEnlaceDAO();

	EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla():",
			EIGlobal.NivelLog.INFO);
//	PreparedStatement TestQuery = null;
//	ResultSet TestResult = null;
//	Connection connbd = null;

//	EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Entrando a tipoCambioVentanilla", EIGlobal.NivelLog.INFO);

	/* STFQRO INICIO TRANSACCION GPA0  - 31012011 2011*/
	try {
		EIGlobal.mensajePorTrace("CUENTA CARGO->"+cuentaCargo,EIGlobal.NivelLog.DEBUG);
		trama  = tipoCambio.verificaCuenta(cuentaCargo, "VTA", (divisa.trim().equals("MN")?"MXP":divisa), divisaCruce,"TRAN");

		if(trama.substring(0, 2).trim().equals("@2")){
			resultado = formatoMensaje(trama.substring(18));//SACANDO EL ERROR
			EIGlobal.mensajePorTrace("MTI_Transferencia::verificaCuenta:: "+resultado,EIGlobal.NivelLog.DEBUG);
			despliegaPaginaError(resultado.replace('?', ' '),TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req,res);
		}else{
			cuentaOperable = true;
			EIGlobal.mensajePorTrace("MTI_Transferencia::verificaCuenta:: La cuenta es operable",EIGlobal.NivelLog.DEBUG);
		}

	} catch (Exception e){
		EIGlobal.mensajePorTrace ( "ERROR:: MTI_Transferencia.verificaCuenta: "
				+ e.getMessage() + ":" , EIGlobal.NivelLog.ERROR);}
	/* STFQRO FIN DE LA TRANSACCION GPA0 31012011*/
/*STFQRO 31012011 INI
		query_realizar="SELECT TO_CHAR(fecha,'YYYYMMDD hh24:mi:ss'),to_char(tc_cpa_vent,'999,999,999.000000'),to_char(tc_vta_vent,'999,999,999.000000')";
		query_realizar=query_realizar + " from comu_tc_base ";
		query_realizar=query_realizar + " WHERE cve_divisa = 'USD' ";
		query_realizar=query_realizar + " AND fecha = (select MAX(fecha) from comu_tc_base WHERE cve_divisa = 'USD')";

		EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Query"+query_realizar, EIGlobal.NivelLog.ERROR);
		TestQuery	= connbd.prepareStatement(query_realizar);

		if(TestQuery == null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Cannot Create Query", EIGlobal.NivelLog.ERROR);
			connbd.close();
			return tcv;
		}

		TestResult = TestQuery.executeQuery();
		if(TestResult == null) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla(): Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
			connbd.close();
			try {
				TestQuery.close();
				connbd.close();
			}catch(Exception e){
				EIGlobal.mensajePorTrace("TestResult nulo, mensaje de aviso en cierre de conexion", EIGlobal.NivelLog.ERROR);
			}
			return tcv;
		}

		if(TestResult.next()) {
			tcv[0]=TestResult.getString(1);
			tcv[1]=TestResult.getString(2);
			tcv[2]=TestResult.getString(3);
		}
	}
	catch( SQLException sqle ) {
		EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla() Excepcion Error... "+sqle.getMessage(), EIGlobal.NivelLog.INFO);
	}
	finally {
		try {
			TestQuery.close();
			TestResult.close();
			connbd.close();
		}
		catch(Exception e) {
			EIGlobal.mensajePorTrace("MTI_Transferencia - tipoCambioVentanilla() en finally", EIGlobal.NivelLog.INFO);
		}
	}STFQRO 31012011 */
	//prueba en desarrollo
	/*
	tcv[0]="20030505";
	tcv[1]="9.0";
	tcv[2]="9.1";*/
	return cuentaOperable;
}

	/**
	 * Metodo para obtener la divisa de la cuenta
	 * @param cuenta
	 * @return String Respuesta
	 */
	private String getDivisaTrx(String cuenta){
		String prefijo = "";
		try{
			prefijo = cuenta.substring(0,2);
			if(prefijo.trim().equals("09") || prefijo.trim().equals("42") ||
			   prefijo.trim().equals("49") || prefijo.trim().equals("82") ||
		       prefijo.trim().equals("83") || prefijo.trim().equals("97") ||
		       prefijo.trim().equals("98")){
				return USD;
		    }else if(prefijo.trim().equals("08") || prefijo.trim().equals("10") ||
					   prefijo.trim().equals("11") || prefijo.trim().equals("13") ||
					   prefijo.trim().equals("15") || prefijo.trim().equals("16") ||
					   prefijo.trim().equals("17") || prefijo.trim().equals("18") ||
					   prefijo.trim().equals("19") || prefijo.trim().equals("20") ||
					   prefijo.trim().equals("21") || prefijo.trim().equals("22") ||
					   prefijo.trim().equals("23") || prefijo.trim().equals("24") ||
					   prefijo.trim().equals("25") || prefijo.trim().equals("26") ||
					   prefijo.trim().equals("27") || prefijo.trim().equals("28") ||
					   prefijo.trim().equals("40") || prefijo.trim().equals("43") ||
					   prefijo.trim().equals("44") || prefijo.trim().equals("45") ||
					   prefijo.trim().equals("46") || prefijo.trim().equals("47") ||
					   prefijo.trim().equals("48") || prefijo.trim().equals("50") ||
					   prefijo.trim().equals("51") || prefijo.trim().equals("52") ||
					   prefijo.trim().equals("53") || prefijo.trim().equals("54") ||
					   prefijo.trim().equals("55") || prefijo.trim().equals("56") ||
					   prefijo.trim().equals("57") || prefijo.trim().equals("58") ||
					   prefijo.trim().equals("59") || prefijo.trim().equals("60") ||
					   prefijo.trim().equals("61") || prefijo.trim().equals("62") ||
					   prefijo.trim().equals("63") || prefijo.trim().equals("64") ||
					   prefijo.trim().equals("65") || prefijo.trim().equals("66") ||
					   prefijo.trim().equals("67") || prefijo.trim().equals("68") ||
					   prefijo.trim().equals("69") || prefijo.trim().equals("70") ||
					   prefijo.trim().equals("71") || prefijo.trim().equals("72") ||
					   prefijo.trim().equals("73") || prefijo.trim().equals("74") ||
					   prefijo.trim().equals("75") || prefijo.trim().equals("76") ||
					   prefijo.trim().equals("77") || prefijo.trim().equals("78") ||
					   prefijo.trim().equals("79") || prefijo.trim().equals("80") ||
					   prefijo.trim().equals("81") || prefijo.trim().equals("85") ||
					   prefijo.trim().equals("86") || prefijo.trim().equals("87") ||
					   prefijo.trim().equals("88") || prefijo.trim().equals("89") ||
					   prefijo.trim().equals("90") || prefijo.trim().equals("91") ||
					   prefijo.trim().equals("92") || prefijo.trim().equals("93") ||
					   prefijo.trim().equals("94") || prefijo.trim().equals("95") ||
					   prefijo.trim().equals("96") || prefijo.trim().equals("97") ||
					   prefijo.trim().equals("99")){
		    	return "MXP";
		    }
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("MIPD_cambios::getDivisaTrx:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return USD;
	}

	/**
	 * Metodo para darle formato al mensaje de respuesta
	 *
	 * @param cadena		Cadena a formatear
	 * @return	String		Cadena formateada
	 */
	private String formatoMensaje(String cadena){
		String patron = "ABCDEFGHIJKLMN�OPQRSTUVWXYZabcdefghijklmn�opqrstuvwxyz0123456789 ";
		String formato = "";
		for(int i=0; i<=cadena.length()-1; i++){
			if(patron.indexOf(cadena.charAt(i))>=0){
				formato += cadena.charAt(i);
			}
		}
		return formato;
	}

	/**
	 * Metodo encargado de mostrar los datos del banco capturado para comprobar la clave ABA o SWIFT
	 * con que se realizara el pago.
	 *
	 * @param req		Request  de la petici�n
	 * @param res		Response de la petici�n
	 * @throws ServletException Excepcion
	 * @throws IOException Excepcion
	 */
	private void validarDatosBanco(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Iniciando el Metodo.....", EIGlobal.NivelLog.DEBUG);
	    String TransReg = null;
	    String TransNoReg = null;
	    String TCV_Venta = null;
	    String TCE_Venta = null;
	    String TCE_Dolar = null;
	    String inicialImporte = null;
	    String inicialCveDivisa = null;
	    String inicialDesDivisa = null;
	    String inicialCuentaCargo = null;
	    String Registradas = null;
	    String ImporteRegUSD = null;
	    String ImporteReg = null;
	    String ImporteRegDivisa = null;
	    String CveEspecial = null;
	    String tipoEspecial = null;
	    String cadenaCuentas1 = "";

	    String numCtaAbono = null;
	    String numContrato = null;
	    String busquedaNueva = null;
	    String busCvePais = "";
	    String busDesPais = "";
	    String busCveAba = "";
	    String busCveSwift = "";
	    String busDesBanco = "";
	    String busDesCiudad = "";
	    String[] datosCta = null;
	    String tablaValDatos = null;
	    String[] datosBanco = null;
	    Vector paises = null;
	    Vector listaBusquedaBancos = null;
	    TrxGP93BO gp93 = new TrxGP93BO();
	    TrxGPF2BO gpf2 = new TrxGPF2BO();
	    EI_Tipo obj = new EI_Tipo(this);
	    TipoCambioEnlaceDAO dao = new TipoCambioEnlaceDAO();
		HttpSession sess = req.getSession();
		BaseResource bSess = (BaseResource) sess.getAttribute(SESSION);

	    try{
	    	TransReg           = (String)req.getParameter(TRANS_REG);
	    	TransNoReg         = (String)req.getParameter(TRANS_NO_REG);
	    	TCV_Venta          = (String)req.getParameter(TCV_VENTA);
	    	TCE_Venta          = (String)req.getParameter(TCE_VENTA);
	    	TCE_Dolar          = (String)req.getParameter(TCE_DOLAR);
	    	inicialImporte     = (String)req.getParameter(INICIAL_IMPORTE);
	    	inicialCveDivisa   = (String) req.getParameter(INICIAL_CVE_DIVISA);
	    	inicialDesDivisa   = (String) req.getParameter(INICIAL_DESC_DIVISA);
	    	inicialCuentaCargo = (String) req.getParameter(INICIAL_CTA_CARGO);
	    	Registradas        = (String) req.getParameter(REGISTRADAS);
	    	ImporteRegUSD      = (String) req.getParameter(IMPORTE_REG_USD);
	    	ImporteReg         = (String) req.getParameter(IMPORTE_REG);
	    	ImporteRegDivisa   = (String) req.getParameter(IMPORTE_REG_DIVISA);
	    	CveEspecial        = (String) req.getParameter("CveEspecial");
	    	tipoEspecial       = (String) req.getParameter("tipoEspecial");

	    	if(req.getParameter("busquedaNueva")!=null)
	    		busquedaNueva = (String) req.getParameter("busquedaNueva");
	    	else
	    		busquedaNueva = "0";

	    	obj.iniciaObjeto(TransReg);
	    	numCtaAbono = obj.camposTabla[0][2];
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Entra Cuenta de Abono->"+numCtaAbono, EIGlobal.NivelLog.DEBUG);
	    	numContrato = bSess.getContractNumber();
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Entra Contrato->"+numContrato, EIGlobal.NivelLog.DEBUG);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Busqueda Nueva->"+busquedaNueva, EIGlobal.NivelLog.DEBUG);
	    	if(busquedaNueva.equals("0")){
		    	datosCta = dao.obtenDatosCtaInter(numContrato, numCtaAbono);
			    busCvePais = datosCta[0];
			    busCveAba = (datosCta[1]!=null && !datosCta[1].equals("null") && !datosCta[1].equals(""))?datosCta[1]:"CLAVEMAL";
			    busCveSwift = (datosCta[2]!=null && !datosCta[2].equals("null") && !datosCta[2].equals(""))?datosCta[2]:"CLAVEMAL";
			    busDesBanco = datosCta[3];
			    busDesCiudad = datosCta[4];
			    paises = CatDivisaPais.getInstance().getListaPaises();
			    EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco::Pais a buscar->"+busCvePais.trim()+"<", EIGlobal.NivelLog.DEBUG);
		    	for(int i=0; i<paises.size(); i++){
		    		if(((TrxGP93VO)paises.get(i)).getOVarCod().substring(4).trim().equals(busCvePais.trim())){
		    			busCvePais = ((TrxGP93VO)paises.get(i)).getOVarCod().substring(0,3).trim();
		    		    busDesPais = ((TrxGP93VO)paises.get(i)).getOVarDes().trim();
		    		    break;
		    		}
		    	}
	    	}else{
	    		if(req.getParameter("busCvePais")!=null)
	    			busCvePais = (String) req.getParameter("busCvePais");
	    		if(req.getParameter("busDesPais")!=null)
	    			busDesPais = (String) req.getParameter("busDesPais");
	    		if(busCvePais.equals("001") && req.getParameter("busCveAbaSwift")!=null)
	    			busCveAba = (String) req.getParameter("busCveAbaSwift");
	    		if(!busCvePais.equals("001") && req.getParameter("busCveAbaSwift")!=null)
	    			busCveSwift = (String) req.getParameter("busCveAbaSwift");
	    		if(req.getParameter(BUS_DESC_BANCO)!=null)
	    			busDesBanco = (String) req.getParameter(BUS_DESC_BANCO);
	    		if(req.getParameter(BUS_DESC_CIUDAD)!=null)
	    			busDesCiudad = (String) req.getParameter(BUS_DESC_CIUDAD);
	    	}
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Pais->" + busCvePais, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Descripci�n Pais->" + busDesPais, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Clave ABA->" + busCveAba, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Clave SWIFT->" + busCveSwift, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Descripci�n Banco->" + busDesBanco, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Descripci�n Ciudad->" + busDesCiudad, EIGlobal.NivelLog.INFO);

			listaBusquedaBancos = gpf2.obtenCatalogoBancos((busCvePais.equals("001")?busCveAba:busCveSwift), busCvePais, "", (busCvePais.equals("001")?"A":(!busCveSwift.trim().equals("")?" ":"S")));
	    	tablaValDatos = dibujaTabla(busCvePais, busDesPais, listaBusquedaBancos, busquedaNueva);
	    	if(busquedaNueva.equals("0") && listaBusquedaBancos.size()==1)
	    		req.setAttribute("mensaje", "\"La clave ABA/SWIFT es correcta.\",1");
	    	else if(busquedaNueva.equals("0") && listaBusquedaBancos.size()!=1)
	    		req.setAttribute("mensaje", "\"La clave ABA / SWIFT es incorrecta, favor de buscarla.\",3");
	    	else if(!busquedaNueva.equals("0") && listaBusquedaBancos.size()>0)
	    		req.setAttribute("mensaje", "\"La consulta de bancos fue exitosa.\",1");
	    	else if(!busquedaNueva.equals("0") && listaBusquedaBancos.size()==0)
	    		req.setAttribute("mensaje", "\"No hay registros asociados con la consulta.\",3");

	    }catch(Exception e){
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.ERROR);
	    }
	    cadenaCuentas1+=generaCuentaCargo(inicialCuentaCargo,"REG");

	    req.setAttribute(TRANS_REG, TransReg);
	    req.setAttribute(TRANS_NO_REG, TransNoReg);
	    req.setAttribute(TCV_VENTA, TCV_Venta);
	    req.setAttribute(TCE_VENTA, TCE_Venta);
	    req.setAttribute(TCE_DOLAR, TCE_Dolar);
	    req.setAttribute(INICIAL_IMPORTE, inicialImporte);
	    req.setAttribute(INICIAL_CVE_DIVISA, inicialCveDivisa);
	    req.setAttribute(INICIAL_DESC_DIVISA, inicialDesDivisa);
	    req.setAttribute(INICIAL_CTA_CARGO, inicialCuentaCargo);
	    req.setAttribute(REGISTRADAS, Registradas);
	    req.setAttribute(IMPORTE_REG_USD, ImporteRegUSD);
	    req.setAttribute(IMPORTE_REG, ImporteReg);
	    req.setAttribute(IMPORTE_REG_DIVISA, ImporteRegDivisa);
	    req.setAttribute("CveEspecial", CveEspecial);
	    req.setAttribute("tipoEspecial", tipoEspecial);
	    req.setAttribute("cuentasAsociadasCargo",cadenaCuentas1);
	    req.setAttribute("muestraBusquedaBancos", "S");
	    req.setAttribute(MODULO, "2");
	    req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute(MENU_PRINCIPAL, bSess.getStrMenu());
		req.setAttribute(NEW_MENU, bSess.getFuncionesDeMenu());
		req.setAttribute(ENCABEZADO, CreaEncabezado(TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req));
	    EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Saliendo del Metodo.....", EIGlobal.NivelLog.INFO);
	    evalTemplate("/jsp/MTI_Transferencia.jsp", req, res);
	}

	/**
	 * Metodo para dibujar la tabla de datos de la cuenta de abono
	 * @param busCvePais Clave pais
	 * @param busDesPais Descripcion Pais
	 * @param listaBusquedaBancos lista bancos
	 * @param busquedaNueva busqueda nueva
	 * @return String Respuesta
	 */
	private String dibujaTabla(String busCvePais, String busDesPais, Vector listaBusquedaBancos, String busquedaNueva){
		StringBuffer sb = new StringBuffer("");
	    String pintaCveAbaSwift = "";
	    String pintaDesCiudad = "";
	    String readOnly = "";
	    boolean bancoExistente = true;
		EIGlobal.mensajePorTrace("MTI_Transferencia::dibujaTabla:: Entrando a dibujaTabla.", EIGlobal.NivelLog.DEBUG);
		if(listaBusquedaBancos.size()==1){
		    pintaCveAbaSwift = ((TrxGPF2VO)listaBusquedaBancos.get(0)).getOIdBaCo();
		    pintaDesCiudad = ((TrxGPF2VO)listaBusquedaBancos.get(0)).getOTwn();
		    if(busquedaNueva.equals("0")){
		    	readOnly = "readonly=\"readonly\"";
		    }
		}else if(listaBusquedaBancos.size()==0)
			bancoExistente = false;

		sb.append("<table cellspacing=\"3\" cellpadding=\"2\" border=\"0\" align=\"center\" width=\"500\" class=\"textabdatcla\">");
		sb.append("<tbody>");
		sb.append("<tr>");
		sb.append("<td class=\"tittabdat\" colspan=\"4\"><b>Datos Cuenta del Beneficiario</b></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td width=\"100%\" colspan=\"4\" class=\"textabdatcla\">");
		sb.append("<table cellspacing=\"3\" cellpadding=\"2\" border=\"0\" width=\"100%\" class=\"textabdatcla\">");
		sb.append("<tbody>");
		sb.append("<tr>");
		sb.append("<td colspan=\"4\">(*) filtros de consulta</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\">Pais:</td>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\">Banco Corresponsal:</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\"><input type=\"text\" name=\"busDesPais\" value=\""+ busDesPais +"\" size=\"25\" readonly=\"readonly\"></td>");
		sb.append("<input type=\"hidden\" name=\"busCvePais\" value=\""+ busCvePais +"\"> ");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\"><select name=\"busDesBanco\" id=\"busDesBanco\" onchange=\"actualizaClave()\""+ readOnly +">"+ armaComboBancos(pintaCveAbaSwift, listaBusquedaBancos, busquedaNueva) +"</select></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\">* Ciudad:</td>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\">* Clave ABA / SWIFT:</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		if(bancoExistente)
			sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\"><input type=\"text\" name=\"busDesCiudad\" id=\"busDesCiudad\" value=\"" + pintaDesCiudad + "\" size=\"25\" readonly=\"readonly\"></td>");
		else
			sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\"><input type=\"text\" name=\"busDesCiudad\" id=\"busDesCiudad\" value=\"\" size=\"25\" readonly=\"readonly\"></td>");
		sb.append("<td width=\"50%\" colspan=\"2\" align=\"center\" class=\"tabmovtex\"><input type=\"text\" name=\"busCveAbaSwift\" id=\"busCveAbaSwift\" value=\"" + pintaCveAbaSwift + "\" size=\"15\" "+ readOnly +"></td>");
		sb.append("</tr>");
		sb.append("</tbody>");
		sb.append("</table>");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</tbody>");
		sb.append("</table>");
		sb.append("<table cellspacing=\"3\" cellpadding=\"2\" border=\"0\" align=\"center\" width=\"500\">");
		sb.append("<tr>");
		sb.append("<td class=\"textabref\">");
		sb.append("D&eacute; click sobre el bot&oacute;n ACEPTAR para seguir con la cotizaci�n de su transferencia.");
		if(!bancoExistente || listaBusquedaBancos.size()!=1 || !busquedaNueva.equals("0"))
			sb.append("Si desea realizar otra  busqueda de bancos d&eacute; click sobre el bot&oacute;n CONSULTAR.");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("<br>");
		sb.append("<table border=0 cellpadding=0 cellspacing=0 align=center>");
		sb.append("<tr>");
		if(!bancoExistente || listaBusquedaBancos.size()!=1 || !busquedaNueva.equals("0")){
			sb.append("<td><A href=\"javascript:Buscar();\" border=0><img src=\"/gifs/EnlaceMig/gbo25220.gif\" border=0></a></td>");
			sb.append("<td><A href=\"javascript:Limpiar();\" border=0><img src=\"/gifs/EnlaceMig/gbo25250.gif\" border=0></a></td>");
		}
		if(bancoExistente)
			sb.append("<td><a href=\"javascript:AceptarBanco();\"><img src=\"/gifs/EnlaceMig/gbo25280.gif\" border=0></a></td>");
		sb.append("</tr>");
		sb.append("</table>");
		EIGlobal.mensajePorTrace("MTI_Transferencia::dibujaTabla:: Saliendo de dibujaTabla.", EIGlobal.NivelLog.DEBUG);

		return sb.toString();
	}

	/**
	 * Metodo para crear el combo de bancos elegible
	 * @param cveAbaSwift Aba Swift
	 * @param listaBusquedaBancos Lista bancos
	 * @param busquedaNueva Busqueda nueva
	 * @return String Respuesta
	 */
	private String armaComboBancos(String cveAbaSwift, Vector listaBusquedaBancos, String busquedaNueva){
		StringBuffer sb = new StringBuffer();
		EIGlobal.mensajePorTrace("MTI_Transferencia::armaComboBancos:: Entrando a armaComboBancos", EIGlobal.NivelLog.DEBUG);
		if(listaBusquedaBancos.size()!=1 || !busquedaNueva.equals("0"))
			sb.append("<option value=\"\">SELECCIONE UNA OPCION</option>");
		try{
			for(int i=0; i<listaBusquedaBancos.size(); i++){
				if(((TrxGPF2VO)listaBusquedaBancos.get(i)).getOIdBaCo().equals(cveAbaSwift.trim())){
					sb.append("<option selected value=\""+ ((TrxGPF2VO)listaBusquedaBancos.get(i)).getOIdBaCo() + "|"
														 + ((TrxGPF2VO)listaBusquedaBancos.get(i)).getOTwn() + "|"
														 + ((TrxGPF2VO)listaBusquedaBancos.get(i)).getODesCor() +  "\">"
					+ ((TrxGPF2VO)listaBusquedaBancos.get(i)).getODesCor() + "</option>");
				}
				else{
					sb.append("<option value=\""+ ((TrxGPF2VO)listaBusquedaBancos.get(i)).getOIdBaCo() + "|"
											    + ((TrxGPF2VO)listaBusquedaBancos.get(i)).getOTwn() + "|"
											    + ((TrxGPF2VO)listaBusquedaBancos.get(i)).getODesCor() + "\">"
			  		+ ((TrxGPF2VO)listaBusquedaBancos.get(i)).getODesCor() + "</option>");
				}
			}
			EIGlobal.mensajePorTrace("MTI_Transferencia::armaComboBancos:: Saliendo de armaComboBancos", EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("MTI_Transferencia::armaComboBancos:: Problemas al armar el combo de bancos->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return sb.toString();
	}
	
	/**
	 * Metodo encargado de validar la correcta emisi�n del banco a la cotizaci�n
	 *
	 * @param req		Request  de la petici�n
	 * @param res		Response de la petici�n
	 */
	private boolean validarDatosBancoEnvio(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException{
		EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Iniciando el Metodo.....", EIGlobal.NivelLog.DEBUG);
	    String TransReg = null;
	    String TransNoReg = null;
	    String TCV_Venta = null;
	    String TCE_Venta = null;
	    String TCE_Dolar = null;
	    String inicialImporte = null;
	    String inicialCveDivisa = null;
	    String inicialDesDivisa = null;
	    String inicialCuentaCargo = null;
	    String Registradas = null;
	    String ImporteRegUSD = null;
	    String ImporteReg = null;
	    String ImporteRegDivisa = null;
	    String CveEspecial = null;
	    String tipoEspecial = null;
	    String cadenaCuentas1 = "";
	    boolean flag = false;

	    String busquedaNueva = "";
	    String busCvePais = "";
	    String busDesPais = "";
	    String busCveAba = "";
	    String busCveSwift = "";
	    String busDesBanco = "";
	    String busDesCiudad = "";	    
	    String tablaValDatos = null;	    
	    Vector listaBusquedaBancos = null;
	    TrxGPF2BO gpf2 = new TrxGPF2BO();	    	    
		HttpSession sess = req.getSession();
		BaseResource bSess = (BaseResource) sess.getAttribute(SESSION);

	    try{
	    	TransReg           = (String)req.getParameter(TRANS_REG);
	    	TransNoReg         = (String)req.getParameter(TRANS_NO_REG);
	    	TCV_Venta          = (String)req.getParameter(TCV_VENTA);
	    	TCE_Venta          = (String)req.getParameter(TCE_VENTA);
	    	TCE_Dolar          = (String)req.getParameter(TCE_DOLAR);
	    	inicialImporte     = (String)req.getParameter(INICIAL_IMPORTE);
	    	inicialCveDivisa   = (String) req.getParameter(INICIAL_CVE_DIVISA);
	    	inicialDesDivisa   = (String) req.getParameter(INICIAL_DESC_DIVISA);
	    	inicialCuentaCargo = (String) req.getParameter(INICIAL_CTA_CARGO);
	    	Registradas        = (String) req.getParameter(REGISTRADAS);
	    	ImporteRegUSD      = (String) req.getParameter(IMPORTE_REG_USD);
	    	ImporteReg         = (String) req.getParameter(IMPORTE_REG);
	    	ImporteRegDivisa   = (String) req.getParameter(IMPORTE_REG_DIVISA);
	    	CveEspecial        = (String) req.getParameter("CveEspecial");
	    	tipoEspecial       = (String) req.getParameter("tipoEspecial");
	    	
	    	busquedaNueva = "1";
    		if(req.getParameter("busCvePais")!=null)
    			busCvePais = (String) req.getParameter("busCvePais");
    		if(req.getParameter("busDesPais")!=null)
    			busDesPais = (String) req.getParameter("busDesPais");
    		if(busCvePais.equals("001") && req.getParameter("busCveAbaSwift")!=null)
    			busCveAba = (String) req.getParameter("busCveAbaSwift");
    		if(!busCvePais.equals("001") && req.getParameter("busCveAbaSwift")!=null)
    			busCveSwift = (String) req.getParameter("busCveAbaSwift");
    		if(req.getParameter(BUS_DESC_BANCO)!=null)
    			busDesBanco = (String) req.getParameter(BUS_DESC_BANCO);
    		if(req.getParameter(BUS_DESC_CIUDAD)!=null)
    			busDesCiudad = (String) req.getParameter(BUS_DESC_CIUDAD);
	    	
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Pais->" + busCvePais, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Descripci�n Pais->" + busDesPais, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Clave ABA->" + busCveAba, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Clave SWIFT->" + busCveSwift, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Descripci�n Banco->" + busDesBanco, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Descripci�n Ciudad->" + busDesCiudad, EIGlobal.NivelLog.INFO);

			listaBusquedaBancos = gpf2.obtenCatalogoBancos((busCvePais.equals("001")?busCveAba:busCveSwift), busCvePais, busDesCiudad, (busCvePais.equals("001")?"A":(!busCveSwift.trim().equals("")?" ":"S")));
			if(listaBusquedaBancos.size()!=1){
				tablaValDatos = dibujaTabla(busCvePais, busDesPais, listaBusquedaBancos, busquedaNueva);
				req.setAttribute("muestraBusquedaBancos", "S");
				req.setAttribute("mensaje", "\"La clave ABA / SWIFT es incorrecta, favor de buscarla.\",3");
			}else{
				req.setAttribute("muestraBusquedaBancos", "N");
				flag = true;
			}

	    }catch(Exception e){
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBancoEnvio:: Problemas..."+e.getMessage(), EIGlobal.NivelLog.ERROR);
	    }
	    cadenaCuentas1+=generaCuentaCargo(inicialCuentaCargo,"REG");

	    req.setAttribute(TRANS_REG, TransReg);
	    req.setAttribute(TRANS_NO_REG, TransNoReg);
	    req.setAttribute(TCV_VENTA, TCV_Venta);
	    req.setAttribute(TCE_VENTA, TCE_Venta);
	    req.setAttribute(TCE_DOLAR, TCE_Dolar);
	    req.setAttribute(INICIAL_IMPORTE, inicialImporte);
	    req.setAttribute(INICIAL_CVE_DIVISA, inicialCveDivisa);
	    req.setAttribute(INICIAL_DESC_DIVISA, inicialDesDivisa);
	    req.setAttribute(INICIAL_CTA_CARGO, inicialCuentaCargo);
	    req.setAttribute(REGISTRADAS, Registradas);
	    req.setAttribute(IMPORTE_REG_USD, ImporteRegUSD);
	    req.setAttribute(IMPORTE_REG, ImporteReg);
	    req.setAttribute(IMPORTE_REG_DIVISA, ImporteRegDivisa);
	    req.setAttribute("CveEspecial", CveEspecial);
	    req.setAttribute("tipoEspecial", tipoEspecial);
	    req.setAttribute("cuentasAsociadasCargo",cadenaCuentas1);	    
	    req.setAttribute(MODULO, "2");
	    req.setAttribute("tablaValDatos", tablaValDatos);
		req.setAttribute(MENU_PRINCIPAL, bSess.getStrMenu());
		req.setAttribute(NEW_MENU, bSess.getFuncionesDeMenu());
		req.setAttribute(ENCABEZADO, CreaEncabezado(TRANS_INTERNAC,"Transferencias &gt; Internacionales",S27550H,req));
	    EIGlobal.mensajePorTrace("MTI_Transferencia::validarDatosBanco:: Saliendo del Metodo.....", EIGlobal.NivelLog.DEBUG);
	    if(!flag)
	    	evalTemplate("/jsp/MTI_Transferencia.jsp", req, res);
	    return flag;
	}

	/**
	 * Metodo encargado de actualizar los datos del banco del beneficiario elegido
	 * @param req	Request de la aplicaci�n
	 * @return		true  - en caso de haber aplicado correctamente
	 * 				false - en caso de haber encontrado un error
	 */
	private boolean actualizaBancoInter(HttpServletRequest req){
		EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: Entra a actualizar los datos del banco", EIGlobal.NivelLog.DEBUG);
		boolean flag = false;
		String numContrato = null;
		String numCuenta = null;
		String cvePais = null;
		String cveAbaSwift = null;
		String desBanco = null;
		String desCiudad = null;
		String TransReg = null;
		String[] datosBusDesBanco = new String[3];
		StringTokenizer token = null;
		EI_Tipo obj = new EI_Tipo(this);
		TipoCambioEnlaceDAO actBanco = new TipoCambioEnlaceDAO();
		HttpSession sess = req.getSession();
		BaseResource bSess = (BaseResource) sess.getAttribute(SESSION);

		try{
    		if(req.getParameter("busCvePais")!=null)
    			cvePais = (String) req.getParameter("busCvePais");
    		if(req.getParameter("busCveAbaSwift")!=null)
    			cveAbaSwift = (String) req.getParameter("busCveAbaSwift");
    		if(req.getParameter(BUS_DESC_BANCO)!=null){
    			token = new StringTokenizer((String) req.getParameter(BUS_DESC_BANCO),"|");
    			for(int i=0; i<3; i++)
    				datosBusDesBanco[i] = token.nextToken();

    			desBanco = datosBusDesBanco[2];
    			if(datosBusDesBanco[2].length()>40)
    				desBanco = desBanco.substring(0, 39);
    		}
    		desBanco = desBanco.replace('\'', ' ').replace(',', ' ').replace('(', ' ').replace(')', ' ').replace('/', ' ').replace('"', ' ').replace('\\', ' ');

    		if(req.getParameter(BUS_DESC_CIUDAD)!=null){
    			desCiudad = (String) req.getParameter(BUS_DESC_CIUDAD);
    			if(desCiudad.length()>40)
    				desCiudad = desCiudad.substring(0, 39);
    		}
    		desCiudad = desCiudad.replace('\'', ' ').replace(',', ' ').replace('(', ' ').replace(')', ' ').replace('/', ' ').replace('"', ' ').replace('\\', ' ');

    		TransReg = (String)req.getParameter(TRANS_REG);
    		obj.iniciaObjeto(TransReg);
    		numCuenta = obj.camposTabla[0][2];
	    	numContrato = bSess.getContractNumber();
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: Datos antes de ir a actualizar=", EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: cvePais->" + cvePais, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: cveAbaSwift->" + cveAbaSwift, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: desBanco->" + desBanco, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: desCiudad->" + desCiudad, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: numCuenta->" + numCuenta, EIGlobal.NivelLog.INFO);
	    	EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: numContrato->" + numContrato, EIGlobal.NivelLog.INFO);
			flag = actBanco.actBancoCtaInter(numContrato, numCuenta, cvePais, cveAbaSwift, desBanco, desCiudad);
			if(flag)
				EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: Se actualizo correctamente", EIGlobal.NivelLog.INFO);
			else
				EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: Hubo un error al actualizar", EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("MTI_Transferencia::actualizaBancoInter:: Problemas al actualizar datos del banco->"+e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		return flag;
	}

}