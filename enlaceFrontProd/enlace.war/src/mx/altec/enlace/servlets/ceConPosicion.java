package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import java.rmi.RemoteException;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ConsultaPosicionCE;
import mx.altec.enlace.bo.ConsultaSaldosCE;
import mx.altec.enlace.bo.TramasCE;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



/**
 *
 *
 *
 *
 * ceConPosicion es un servlet que se encarga de las consultas de posici&oacute;n y de su despliegue.
 * <P>
 * 18/04/2002 Se cambiaron los par�metros que recibe para adecuarlos con <I>ceConSaldos</I><BR>
 * 19/04/2002 Se continu&oacute; con los cambios y con la funcionalidad de "Anterior" y "Siguiente".<BR>
 * 22/04/2002 Se arreglaron los par&aacute;metros para "Anterior" y "Siguiente"<BR>
 * 23/04/2002 Modificaci�n para manejar <I>Consulta de disposici&oacute;n.</I><BR>
 * 25/04/2002 Verificaci&oacute;n c&oacute;digo.<BR>
 * 16/07/2002 Optimizaci&oacute;n del c&oacute;digo.<BR>
 * 17/07/2002 Inicio de optimizaci&oacute;n del c&oacute;digo. <BR>
 * 24/07/2002 Fin de la optimizaci&oacute;n del c&oacute;digo. Se sustituyeron par&aacute;metros por los atributos.<BR>
 * 25/07/2002 Comparaci&oacute;n tiempos.<BR>
 * 29/07/2002 Facultad saldos.<BR>
 * 01/08/2002 Cambios t&iacute;tulos y menues aplicaci&oacute;n.<BR>
 * 15/08/2002 Se agreg&oacute la facultad de consulta de posici&oacute;n.<BR>
 * 21/08/2002 Verificaci&oacute;n de las facultades de saldo y posici&oacute;n.<BR>
 * 22/08/2002 Revisi&oacute;n facultades.<BR>
 * 23/08/2002 Redirecci&oacute;n p&aacute;gina error si no recibe par&aacute;metros.<Br>
 * 30/08/2002 Sustituci&oacute;n caracteres por secuencias escape.<BR>
 * 02/10/2002 Se modific&oacute; c&oacute;mo se recibe el par&aacute;metro <I>ctasLineas</I>.<BR>
 * 03/10/2002 Se coment&oacute; donde verifica las facultades de posici&oacute; y movimientos.<BR>
 * 09/10/2002 Se despliegan las tramas de respuesta. Correcci&oacute;n concatenaci&oacute;n.<BR>
 * 10/10/2002 Se modificaron los m&eacute;todos <I>creaEncabezado</I>.<BR>
 * 11/101/2002 Se cambi&oacute; una condici&oacute;n.   Se agregaron mensajes.<BR>
 * 11/10/2002 Se cambi&oacute; d&oacute;nde se verifica que se recibi&oacute; el par&aacute;metro <I>noLineas</I>.<BR>
 * 11/10/2002 Se cambi&oacute; el manejo de las facultades y de los objetos de sesi&oacute;n.<>BR
 * 12/10/2002 Correcci&oacute;n ortogr&aacute;fica.   Se usa <B><I>ConsultaSaldosCE</I></B> como JavaBean.<BR>
 * 16/10/2002 Verificaci&oacute;n condiciones para desplegar la p&aacute;gina correcta.<BR>
 * 17/10/2002 Verifiaci&oacute;n respuesta tramas.<BR>
 * 22/10/2002 Revisa si la disposici&oacute;n tiene descripci&oacute;n.<BR>
 * 25/10/2002 Se agregaron m&aacute;s mensajes a la bit&aacute;cora.<BR>
 * 31/10/2002 Se agreg&oacute; el c&oacute;digo para que cuando tenga varias l&iacute;as y ya consult&oacute; previamente, entre a la p&aacute;gina.
 * 04/11/2002 Se colocan encabezados a la p&aacute;gina de error.<BR>
 * 05/11/2002 Encabezados de p&aacute;gina de error.<BR>
 * 06/11/2002 Correcci&oacute;n men&uacute; navegaci&oacute;n.<BR>
 * 14/11/2002 Cambio t&iacute;tulo y men&uacute; ayuda.<BR>
 * 15/11/2002 Ayudas.<BR>
 * 25/11/2002 Se despliegan mensajes de error de las tramas.<BR>
 * </P>
 * @author Hugo Ru&iacute;z Zepeda
 * @version 1.4.2
 */
public class ceConPosicion extends BaseServlet
{

   /**Versi&oacute;n.
   */
   public static final String version="1.4.2";

   /**Este objeto tiene diversos m&eacute;todos auxilares.
   */
  //private EIGlobal enlaceGlobal;

  /**Nombre del atributo de sesi&oacute;n del contrato.
  */
  private String atributoContrato="ContractNumber";

  //private String atributoUsuario="NombreUsuario";

  /**Nombre del atributo de sesi&oacute;n usuario
  */
  private String atributoUsuario="UserID";

  /**Nombre del atributo de sesi&ocute;n del perfil del usuario.
  */
  private String atributoPerfil="UserProfile";

  /**Se usa como encabezado de la pantalla desplegada por el <I>jsp</I> para consulta de posici&oacute;n.
  */
  private String tituloPosicion="Consulta de posici&oacute;n de Cr&eacute;dito Electr&oacute;nico";

  /**Men&uacute; para indicar la navegaci&oacute;n dentro de la aplicaci&oacute;n (posici&oacute;n).
  */
  private String menuNavegacionPosicion="Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consulta de posici&oacute;n";

  /**Men&uacute; para indicar la navegaci&oacute;n dentro de la aplicaci&oacute;n (disposici&oacute;n).
  */
  private String menuNavegacionDisposicion="Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consulta de disposici&oacute;n";

  /**Se usa como encabezado de la pantalla <I>detalle de disposici&oacute;n</I>
  */
  private String tituloDisposicion="Consulta de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico";

  /**Ruta relativa del <I>jsp</I> que corresponde a la pantalla principal.
  */
  private String redirecciona="/jsp/ceConPosicion.jsp";

  /**Caracter que se usa como separador de campos.
  */
  private String separador="|";


   /**<code><B><I>init</I></B></code> es ejecutado cuando se inicia el servidor y obtiene<BR>
   * una instancia de "EIGlobal".
   */
  public void init()
  {

    EIGlobal.mensajePorTrace("Iniciando servlet \"ceConPosicion\".", EIGlobal.NivelLog.INFO);
  } // Fin m�todo init


   /**<code><B><I>destroy</I></B></code> se ejecuta cuando se "tira" el servidor.
   */
  public void destroy()
  {
    EIGlobal.mensajePorTrace("Destruyendo el servlet \"ceConPosicion\".", EIGlobal.NivelLog.INFO);
  } // Fin destroy


    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param peticion the HttpServletRequest
    ** @param respuesta the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)
                    throws ServletException, IOException
    {
         defaultAction(peticion, respuesta);
    } // Fin doGet

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param peticion the HttpServletRequest
    ** @param respuesta the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest peticion, HttpServletResponse respuesta)
                    throws ServletException, IOException
    {
         defaultAction(peticion, respuesta);
    } // Fin doPost



    /**
    ** <code>displayMessage</code> allows for a short message to be streamed
    ** back to the user.  It is used by various NAB-specific wizards.
    ** @param peticion the HttpServletRequest
    ** @param respuesta the HttpServletResponse
    ** @param messageText the message-text to stream to the user.
    */
    public void displayMessage(HttpServletRequest peticion,
                      HttpServletResponse respuesta,
                      String messageText)
                    throws ServletException, IOException
    {
        //respuesta.setContentType("text/html");

        /*
        PrintWriter salida = respuesta.getWriter();
        salida.println(messageText);
        */
    } // Fin metodo displayMessage


    /**
    ** <code>defaultAction</code> es el m&eacute;todo por defecto para recibir todas las peticiones "get" y "post".<BR>
    ** @param peticion es un objeto de tipo HttpServletRequest que maneja todas las llamadas al servlet.
    ** @param respuesta es un objeto de tipo HttpServletResponse que indica c&oacute;mo responde el servlet y redirege la salida<BR>
    ** a los <I>jsp</I>.
    ** @exception javax.servlet.ServletException Es arrojada cuando hay una excepci&oacute;n con el servlet.
    ** @exception java.io.IOException Es arrojada cuando hay una excepci&oacute;n en la salida del servlet.
    */
    public void defaultAction(HttpServletRequest peticion, HttpServletResponse respuesta)
                   throws ServletException, IOException
    {
      //EI_Tipo eit=new EI_Tipo(this);
      String cuenta=null;
      String descripcion=null;
      String linea=null;
      String contrato=null;
      String usuario=null;
      String perfil=null;
      //PrintWriter salida;
      boolean sesionValida;
      boolean verificaDatos=false;
      boolean hayDatos=false;
      boolean primeraRevision=true;
      boolean entra=false;
      boolean esConsultaPosicion=false;
      //Enumeration atributos;
      ServicioTux tuxGlobal;
      String tramaCS=new String();
      String respuestaTramaCS=null;
      String tramaCP=new String();
      String respuestaTramaCP=new String("");
      String cadenaErrores=new String("");
      Hashtable ht;
      StringTokenizer st;
      Object revisa[]=null;
      String ctasLineas=null;
      String noLineas=null;
      String lineas[]=null;
      Vector errores=new Vector();
      String cadDireccion=null;
      int direccion=0, datosCuenta=0;
      //String nuevasCuentas="";
      //String nuevaLinea="";
      String noDisp="";
      String tipoOperacion=null;
      boolean consultoTuxedo=false;
      ConsultaSaldosCE csCE=null;
      ConsultaPosicionCE obtieneNueva=null;
      //ConsultaSaldosCE csCE02=null;
      //ConsultaPosicionCE cpCE02=null;
      Boolean facultadSaldo;
      Boolean facultadDisposicion;
      Boolean facultadPosicion;
      HttpSession sesion;
      EIGlobal enlaceGlobal;
      boolean tramasCorrectas=false;
      String mensajesError="";


      //respuesta.setContentType("text/html");
      //salida = respuesta.getWriter();
      sesion=peticion.getSession();
      BaseResource sesionBS =(BaseResource)sesion.getAttribute("session");
      enlaceGlobal=new EIGlobal(sesionBS, getServletContext(), this);


      enlaceGlobal.mensajePorTrace("Iniciando el servlet \"ceConPosicion\".", EIGlobal.NivelLog.INFO);

      if(SesionValida(peticion, respuesta))
      {

         /*
         if(sesionBS.getFacultad( FAC_TECRESALDOSC_CRED_ELEC()!=null)
         {
            facultadSaldo=new Boolean(session.getFAC_TECRESALDOSC_CRED_ELEC());
         }
         else
         {
            facultadSaldo=new Boolean(false);
            enlaceGlobal.mensajePorTrace("No se obtuvo el valor de la facultad de saldos de cr�dito electr�nico.", EIGlobal.NivelLog.INFO);
         } // Fin if-else facultad saldos cr�dito electr�nico
         */

         //facultadSaldo=sesionBS.getFacultad(BaseResource.FAC_TECRESALDOSC_CRED_ELEC);


         // Se coment� con el fin de hacer pruebas
         //if(!facultadSaldo.booleanValue())
         if(!sesionBS.getFacultad(BaseResource.FAC_TECRESALDOSC_CRED_ELEC))
         {
            getServletContext().getRequestDispatcher("/SinFacultades").forward(peticion, respuesta);
            enlaceGlobal.mensajePorTrace("El usuario carece de la facultad de saldos de cr�dito electr�nico.", EIGlobal.NivelLog.INFO);
            return;
         } // Fin if vefificaci�n facultad saldos cr�dito electr�nico





        peticion.setAttribute("MenuPrincipal", sesionBS.getStrMenu());
			  peticion.setAttribute("newMenu", sesionBS.getFuncionesDeMenu());
			  //peticion.setAttribute("Encabezado",CreaEncabezado("Consultas de l&iacute;neas de Cr&eacute;dito electr&oacute;nicas",titulo));
         /*
        contrato=(String)sesion.getAttribute(atributoContrato);
        usuario=(String)sesion.getAttribute(atributoUsuario);
        perfil=(String)sesion.getAttribute(atributoPerfil);
        */

        contrato=sesionBS.getContractNumber();
        usuario=sesionBS.getUserID8();  
        perfil=sesionBS.getUserProfile();

        ctasLineas=peticion.getParameter("ctasLineas");
        noLineas=peticion.getParameter("noLineas");
        noDisp=peticion.getParameter("noDisp");
        cadDireccion=peticion.getParameter("direccion");
        //enlaceGlobal.mensajePorTrace("Se obtuvieron parametros y atributos.", EIGlobal.NivelLog.INFO);
        //atributos=session.getAttributeNames();
        //despliegaAtributos(atributos, salida);

			//salida.println(session.getFAC_TECREPOSICIOC_CRED_ELEC());

         if(!verificaParametros(ctasLineas, noDisp, noLineas, enlaceGlobal))
         {
            //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
            despliegaPaginaError("No se recibieron par&aacute;metros.", tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
            enlaceGlobal.mensajePorTrace("No se recibieron par�metros para efectar la consulta.", EIGlobal.NivelLog.INFO);
            return;
         } // Fin if

      try
      {
        direccion=Integer.parseInt(cadDireccion);
      }
      catch(NumberFormatException nfe)
      {
        direccion=0;
      } // Fin try-catch direccion

        if(noDisp!=null && noDisp.length()>0)
        {

            /*
            if(session.getFAC_TECREMOVTOSC_CRED_ELEC()!=null)
            {
               facultadDisposicion=new Boolean(session.getFAC_TECREMOVTOSC_CRED_ELEC());

            }
            else
            {
               facultadDisposicion=new Boolean(false);
               enlaceGlobal.mensajePorTrace("No se obtuvo la facultad de disposici�n de cr�dito electr�nico: "+session.getFAC_TECREMOVTOSC_CRED_ELEC(), EIGlobal.NivelLog.INFO);
            } // Fin if-else facultad disposici�n
            */


            //if(!facultadDisposicion.booleanValue())
            if(!sesionBS.getFacultad(BaseResource.FAC_TECREMOVTOSC_CRED_ELEC))
            {
               getServletContext().getRequestDispatcher("/SinFacultades").forward(peticion, respuesta);
               enlaceGlobal.mensajePorTrace("El usuario no tiene facultad de movimientos (disposiciones).", EIGlobal.NivelLog.INFO);
               return;
            } // Fin if verificaci�n facultad disposici�n de cr�dito electr�nico


          ctasLineas=noDisp;
          entra=true;
          tipoOperacion="PD78";
          esConsultaPosicion=false;
          peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionDisposicion, "s32070h", peticion));
          //salida.println("Consulta de disposicion.\n<BR>");
          enlaceGlobal.mensajePorTrace("Es consulta de disposici�n.", EIGlobal.NivelLog.INFO);
        }
        else
        {

          if(ctasLineas!=null && ctasLineas.length()>0 && noLineas!=null && noLineas.length()>0)
          {

            /*
            if(session.getFAC_TECREPOSICIOC_CRED_ELEC()!=null)
            {
               facultadPosicion=new Boolean(session.getFAC_TECREPOSICIOC_CRED_ELEC());

            }
            else
            {
               facultadPosicion=new Boolean(false);
               enlaceGlobal.mensajePorTrace("No se obtuvo la facultad de posici�n de cr�dito electr�nico. ", EIGlobal.NivelLog.INFO);
            } // Fin if-else facultad posici�n
            */



            // Se coment� con el fin de hacer pruebas.
            //if(!facultadPosicion.booleanValue())
            if(!sesionBS.getFacultad(BaseResource.FAC_TECREPOSICIOC_CRED_ELEC))
            {
               getServletContext().getRequestDispatcher("/SinFacultades").forward(peticion, respuesta);
               enlaceGlobal.mensajePorTrace("El usuario no tiene facultad para esta operaci�n. ", EIGlobal.NivelLog.INFO);
               return;
            } // Fin if verificaci�n facultad posici�n de cr�dito electr�nico


            entra=true;
            tipoOperacion="PC49";
            esConsultaPosicion=true;
            //salida.println("Consulta de posicion");
            peticion.setAttribute("Encabezado",CreaEncabezado(tituloPosicion, menuNavegacionPosicion, "s32030h", peticion));
            enlaceGlobal.mensajePorTrace("Es consulta de posici�n", EIGlobal.NivelLog.INFO);
          }
          else
          {
            enlaceGlobal.mensajePorTrace("No es consulta de posici�n ni de detalle de disposici�n.", EIGlobal.NivelLog.INFO);
            peticion.setAttribute("Encabezado",CreaEncabezado("Consultas de Cr&eacute;dito Electr&oacute;nico", "No es consulta de posici�n ni de detalle de disposici�n.", "s32030h", peticion));
            entra=false;
          } // Fin if consulta posicion
        } // Fin if noDisp

        if(entra)
        {
          try
          {
            if(esConsultaPosicion)
            {
              st=new StringTokenizer(ctasLineas, separador);
              //salida.println("Separa posicion.\n<BR>");
            }
            else
            {
              st=new StringTokenizer(noDisp, separador);
              //salida.println("Separa disposicion.\n<BR>");
            }

            datosCuenta=st.countTokens();

            if(datosCuenta==2 || datosCuenta==3 || datosCuenta==4)
            {

               if(esConsultaPosicion)
                  st.nextToken();

              cuenta=st.nextToken();
              linea=st.nextToken();

              enlaceGlobal.mensajePorTrace("Cuenta: "+cuenta, EIGlobal.NivelLog.INFO);

              if(esConsultaPosicion)
                  enlaceGlobal.mensajePorTrace("L�nea: "+linea, EIGlobal.NivelLog.INFO);
              else
                  enlaceGlobal.mensajePorTrace("Disposici�n: "+linea, EIGlobal.NivelLog.INFO);

               /*Se a�adi� si la cuenta no tiene descripci�n.
               */
               if(st.hasMoreTokens())
               {
                  descripcion=st.nextToken();
                  enlaceGlobal.mensajePorTrace("Descripci�n: "+descripcion, EIGlobal.NivelLog.INFO);
               }
               else
               {
                  descripcion=" ";
                  enlaceGlobal.mensajePorTrace("Cuenta sin descripci�n. Se asigna un espacio por defecto.", EIGlobal.NivelLog.INFO);
               } // Fin if descripci�n.

              /*
                Se usa en n�mero de disposici�n como n�mero de l�nea.
              */
              if(!esConsultaPosicion)
              {
                noLineas=cuenta+"| |"+linea;
                //salida.println("Datos disposicion :<BR>");
                //salida.println(noLineas+"\n<BR>");
              }

              st=new StringTokenizer(noLineas, separador);

              if(st.countTokens()>0)
              {
                int aux1=st.countTokens()-2;

                lineas=new String[aux1];
                st.nextToken();
                st.nextToken();

                for(int i=0;i<aux1;i++)
                {
                  lineas[i]=st.nextToken();
                } // Fin for
              }
              else
              {
                //errores.addElement("No hay lineas.\n");
                enlaceGlobal.mensajePorTrace("No hay lineas para consultar\n", EIGlobal.NivelLog.INFO);
                primeraRevision=false;
              } // Fin if tokens lineas
            }
            else
            {
              //errores.addElement("No hay datos de la cuenta.\n");
              enlaceGlobal.mensajePorTrace("No hay datos de la cuenta.\n", EIGlobal.NivelLog.INFO);
              primeraRevision=false;
            } // Fin if tokens ctas


          }
          catch(ArrayIndexOutOfBoundsException aioob)
          {
            //errores.addElement(aioob.getMessage());
            enlaceGlobal.mensajePorTrace(aioob.getMessage()+"\n", EIGlobal.NivelLog.INFO);
            primeraRevision=false;
          } // Fin try-catch
        }
        else
        {
          //errores.addElement("No se recibieron los parametros ctasLineas ni noLineas");
          enlaceGlobal.mensajePorTrace("No se recibieron los parametros ctasLineas ni noLineas", EIGlobal.NivelLog.INFO);
          primeraRevision=false;
        } // Fin try-catch

        revisa=revisaDatos(usuario, contrato, perfil, cuenta, descripcion);
        verificaDatos=((Boolean)revisa[0]).booleanValue();
        enlaceGlobal.mensajePorTrace("Resultado revisaDatos: "+verificaDatos, EIGlobal.NivelLog.INFO);

         if(!verificaDatos)
         {
            enlaceGlobal.mensajePorTrace("Error verificaDatos: "+(String)revisa[1], EIGlobal.NivelLog.INFO);
         } // Fin if verificaDatos






        if(verificaDatos && primeraRevision)
        {
          TramasCE construyeTramas=new TramasCE();
          //atributos=session.getAttributeNames();


          csCE=(ConsultaSaldosCE)getServletContext().getAttribute("csCE");
          obtieneNueva=(ConsultaPosicionCE)getServletContext().getAttribute("posicionesCE");

          if(csCE==null || obtieneNueva==null)
          {
             consultoTuxedo=false;
             getServletContext().removeAttribute("csCE");
             getServletContext().removeAttribute("posicionesCE");
             //salida.println("No ha consultado.<BR>");
          }
          else
          {
             if(csCE.getCuenta().equalsIgnoreCase(cuenta) && obtieneNueva.getCuenta().equalsIgnoreCase(cuenta) && obtieneNueva.getContrato().equalsIgnoreCase(contrato) && direccion!=0 && !csCE.esCaduco() && !obtieneNueva.esCaduco())
             {
               consultoTuxedo=true;
               int num;

               //salida.println("Posici�n anterior: "+obtieneNueva.getPosicion()+"<BR>");
               enlaceGlobal.mensajePorTrace("Posici�n anterior: "+obtieneNueva.getPosicion(), EIGlobal.NivelLog.INFO);
               obtieneNueva.setPosicion(obtieneNueva.getPosicion()+direccion);
               enlaceGlobal.mensajePorTrace("Posici�n nueva: "+obtieneNueva.getPosicion(), EIGlobal.NivelLog.INFO);

               /*
               salida.println("Direcci�n: "+direccion+"<BR>");
               salida.println("Nueva posici�n: "+obtieneNueva.getPosicion()+"<BR>");
               salida.println("L�nea: "+obtieneNueva.getLinea()+"<BR>");
               */

               getServletContext().removeAttribute("csCE");
               getServletContext().removeAttribute("posicionesCE");
               getServletContext().setAttribute("csCE", csCE);
               getServletContext().setAttribute("posicionesCE", obtieneNueva);




               hayDatos=true;
               //salida.println("Ya consult�.<BR>");
               enlaceGlobal.mensajePorTrace("Ya consult� y ya tiene datos.", EIGlobal.NivelLog.INFO);
             }
             else
             {
               csCE=null;
               obtieneNueva=null;
               getServletContext().removeAttribute("csCE");
               getServletContext().removeAttribute("posicionesCE");
               //salida.println("No coincidieron.<BR>");
               consultoTuxedo=false;
             } // Fin if
          } // Fin if csCE


         if(!consultoTuxedo)
         {
            construyeTramas.setMedioEntrega(TramasCE.MEDIOS_WEB[TramasCE.TRAMA]);
            tramaCS=construyeTramas.tramaConsultaSaldos(usuario, TramasCE.SDCT, contrato, perfil, cuenta, TramasCE.PERSONAL);
            //tramaCP=construyeTramas.tramaConsultaLineasCredito(usuario, contrato, perfil, cuenta);


            if(descripcion==null)
               descripcion="";




            try
            {
               tuxGlobal=new ServicioTux();
               //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
               ht=tuxGlobal.web_red(tramaCS);
               respuestaTramaCS=(String)ht.get("BUFFER");
               enlaceGlobal.mensajePorTrace("Respuesta consulta saldos: "+respuestaTramaCS, EIGlobal.NivelLog.DEBUG);



               for(int j=0;j<lineas.length;j++)
               {
                  //tramaCP=construyeTramas.consultaPosicionLinea(usuario, TramasCE.PC49,
                  //contrato, perfil, lineas[j]);

                  tramaCP=construyeTramas.consultaPosicionLinea(usuario, tipoOperacion,
                  contrato, perfil, lineas[j]);
                  //salida.println(tramaCP);
                  //salida.println("<BR>");

                  ht=tuxGlobal.web_red(tramaCP);

                  if(j>0)
                     respuestaTramaCP+="@";

                  respuestaTramaCP+=(String)ht.get("BUFFER");
                  //salida.println((String)ht.get("BUFFER"));
                  //salida.println("<BR>");
               } // Fin for j

               enlaceGlobal.mensajePorTrace("Respuestas consulta posici�n: "+respuestaTramaCP, EIGlobal.NivelLog.DEBUG);

               if(respuestaTramaCS==null || respuestaTramaCS.length()<1)
               {
                  hayDatos=false;
                  cadenaErrores+="No hay datos de saldos.";
                  enlaceGlobal.mensajePorTrace("No hay datos de saldos.", EIGlobal.NivelLog.INFO);
               }
               else
               {
                  hayDatos=true;
               } // Fin if respuestaTramaCS


               if(respuestaTramaCP==null || respuestaTramaCP.length()<1)
               {
                  hayDatos=false;
                  cadenaErrores+="No hay datos.";
                  enlaceGlobal.mensajePorTrace("No hay datos de consulta de posici�n.", EIGlobal.NivelLog.INFO);
               }
               else
               {
                  hayDatos=hayDatos && true;
               } // Fin if respueta tramaCP


            }
            catch(RemoteException re)
            {
               respuestaTramaCS="Error al solicitar el servicio Tuxedo.";
               enlaceGlobal.mensajePorTrace("Error al solicitar el servicio Tuxedo. ceConPosicion", EIGlobal.NivelLog.INFO);
               //System.err.println("ceConPosicion: "+re.getMessage());
               enlaceGlobal.mensajePorTrace("Error en el servicio de tuxedo: "+re.getMessage(), EIGlobal.NivelLog.INFO);
               //re.printStackTrace();
            }
            catch(Exception e)
            {
               respuestaTramaCS="Error al solicitar el servicio Tuxedo.";
               //System.err.println("ceConPosicion: "+e.getMessage());
               enlaceGlobal.mensajePorTrace("Error al solicitar el servicio Tuxedo. ceConPosicion "+e.getMessage(), EIGlobal.NivelLog.INFO);
               //e.printStackTrace();
            } // Fin try-catch tuxGlobal

         } // Fin if consultoTuxedo

        }
        else
        {
            enlaceGlobal.mensajePorTrace("No hay datos de la consulta\n", EIGlobal.NivelLog.INFO);
            enlaceGlobal.mensajePorTrace("Mensaje error: "+ (String)revisa[1], EIGlobal.NivelLog.INFO);
            //despliegaPaginaError((String)revisa[1], peticion, respuesta);
        } // Fin if-else revisaDatos

      }
      else
      {
        enlaceGlobal.mensajePorTrace("Se intento tener acceso a \"ceConPosicion\" fuera de sesion.", EIGlobal.NivelLog.INFO);
        //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
        despliegaPaginaError("Sesi&oacute;n no v&aacute;lida.", tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
      } // Fin if

      //Simulaci�n de que s� respondi� tuxedo.
      //hayDatos=true;

      enlaceGlobal.mensajePorTrace("Hay datos: "+hayDatos, EIGlobal.NivelLog.INFO);
      enlaceGlobal.mensajePorTrace("Verific� datos: "+verificaDatos, EIGlobal.NivelLog.INFO);



      if(hayDatos && verificaDatos)
      {
        // Simulaci�n




         /*Aqu� se refiere a que no haya consultados saldos y posiciones previamente.
         * Es decir, acaba de disponer de los servicios de tuxedo.
         */
        if(!consultoTuxedo)
        {
            //csCE=new ConsultaSaldosCE(cuenta, descripcion);
            csCE=new ConsultaSaldosCE();
            csCE.setCuenta(cuenta);
            csCE.setDescripcion(descripcion);
            tramasCorrectas=csCE.descomponeTrama(respuestaTramaCS);



            if(tramasCorrectas)
            {
               getServletContext().setAttribute("csCE", csCE);
               enlaceGlobal.mensajePorTrace("La trama de respuesta de consulta de saldos es correcta.", EIGlobal.NivelLog.INFO);
            }
            else
            {
               mensajesError="<BR>No pudo efectuar la consulta de saldo.";
               enlaceGlobal.mensajePorTrace("La trama de respuesta de consulta de saldos es incorrecta.", EIGlobal.NivelLog.INFO);
            } // Fin primer if-else tramas


            obtieneNueva=new ConsultaPosicionCE();
            obtieneNueva.setContrato(contrato);
            obtieneNueva.setCuenta(cuenta);
            obtieneNueva.setNoLineas(noLineas);
            obtieneNueva.setDatos(respuestaTramaCP);
            //obtieneNueva.setPosicion(obtieneNueva.getPosicion(linea));
            obtieneNueva.setLinea(linea);
            tramasCorrectas=tramasCorrectas && obtieneNueva.esCorrecta();

            /*
            salida.println("Contrato: "+obtieneNueva.getContrato()+"<BR>");
            salida.println("Cuenta: "+obtieneNueva.getCuenta()+"<BR>");
            salida.println("Cuenta: "+csCE.getCuenta()+"<BR>");
            salida.println("L�nea: "+obtieneNueva.getLinea()+"<BR>");
            salida.println("Posici�n: "+obtieneNueva.getPosicion()+"<BR>");
            */


            if(obtieneNueva.esCorrecta())
            {
               getServletContext().setAttribute("posicionesCE", obtieneNueva);
               enlaceGlobal.mensajePorTrace("La trama de respuesta de posici�n (o detalle de movimiento) para la l�nea "+obtieneNueva.getLinea()+" es correcta.", EIGlobal.NivelLog.INFO);
            }
            else
            {
               mensajesError+="<BR>"+obtieneNueva.getMensajeError();
               enlaceGlobal.mensajePorTrace("La trama de respuesta de posici�n (o detalle de movimiento) para la l�nea "+obtieneNueva.getLinea()+" es incorrecta.", EIGlobal.NivelLog.INFO);
            } // Fin if consulta posicion correcta

            //salida.println("Se subieron atributos.");



         }
         else
         {
            /* Aqu� va a verificar que los datos recuperados previamente consultados
            * sean correctos.
            */
            tramasCorrectas=csCE.getEsCorrecta() && obtieneNueva.esCorrecta();
            enlaceGlobal.mensajePorTrace("Consult� previamente y la respuesta de las tramas es :"+tramasCorrectas, EIGlobal.NivelLog.INFO);
         } // Fin if-else no consultoTuxedo


         if(tramasCorrectas)
         {
            if(esConsultaPosicion)
            {
               //salida.println("ctasLineas: \n<BR>"+nuevasCuentas+"<BR>");
               //salida.println("noLineas: "+noLineas+"<BR>");
               //salida.println("Datos: "+respuestaTramaCP+"<BR>");
               //evalTemplate(redirecciona+"?ctasLineas="+nuevasCuentas+"&noLineas="+noLineas+"&cuenta="+cuenta+"&descripcion="+descripcion+"&saldo="+csCE.getSaldo()+"&datos="+respuestaTramaCP+"&linea="+nuevaLinea+"&direccion="+direccion, peticion, respuesta);
               //peticion.setAttribute("Encabezado", CreaEncabezado(titulo, menuNavegacionPosicion));

            	/**
				 * VSWF
				 * 17/Enero/2007
				 */
            	//TODO VIT CU3091 Realiza operacion PC49
            	if (Global.USAR_BITACORAS.equals("ON")){
            		try {
						BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC49);
				        bt.setCctaOrig((cuenta == null)? " ":cuenta);
						bt.setContrato((contrato == null)? " ":contrato);
						bt.setServTransTux("PC49");
						/*BMB-I*/
						if(respuestaTramaCP!=null){
							if(respuestaTramaCP.substring(0,2).equals("OK")){
								bt.setIdErr("PC490000");
							}else if(respuestaTramaCP.length()>8){
								bt.setIdErr(respuestaTramaCP.substring(0,8));
							}else{
								bt.setIdErr(respuestaTramaCP.trim());
							}
						}
							/*BMB-F*/
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
            	}
				/**
				 * VSWF
				 */
               evalTemplate(redirecciona+"?ctasLineas="+ctasLineas+"&noLineas="+noLineas+"&direccion="+direccion, peticion, respuesta);
            }
            else
            {
               //salida.println("noDisp: \n<BR>"+noDisp+"<BR>");
               //salida.println("noLineas: "+noLineas+"<BR>");
               //salida.println("Datos: "+respuestaTramaCP+"<BR>");
               //evalTemplate(redirecciona+"?noDisp="+noDisp+"&noLineas="+noLineas+"&cuenta="+cuenta+"&descripcion="+descripcion+"&saldo="+csCE.getSaldo()+"&datos="+respuestaTramaCP+"&linea="+linea+"&direccion="+direccion, peticion, respuesta);

               //TODO VIT CU3091 Realiza operacion PD78
            	/**
				 * VSWF
				 * 17/Enero/2007
				 */
            	if (Global.USAR_BITACORAS.equals("ON")){
            		try {
						BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD78);
						bt.setCctaOrig((cuenta == null)? " ":cuenta);
						bt.setContrato((contrato == null)? " ":contrato);
						bt.setServTransTux("PD78");
						/*BMB-I*/
						if(respuestaTramaCP!=null){
							if(respuestaTramaCP.substring(0,2).equals("OK")){
								bt.setIdErr("PD780000");
							}else if(respuestaTramaCP.length()>8){
								bt.setIdErr(respuestaTramaCP.substring(0,8));
							}else{
								bt.setIdErr(respuestaTramaCP.trim());
							}
						}
						/*BMB-F*/
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
            	}
				/**
				 * VSWF
				 */
               evalTemplate(redirecciona+"?noDisp="+noDisp+"&noLineas="+noLineas+"&direccion="+direccion, peticion, respuesta);
            } // Fin if-else
         }
         else
         {
            if(esConsultaPosicion)
            {
               //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
               despliegaPaginaError("No se pudo obtener informaci&oacute;n de la l&iacute;nea."+mensajesError, tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
            }
            else
            {
               //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
               despliegaPaginaError("No se pudo obtener informaci&oacute;n de la disposici&oacute;n."+mensajesError, tituloDisposicion, menuNavegacionDisposicion, "s32070h", peticion, respuesta);
            } // Fin if-else esConsultaPosicion
         } // Fin if tramas respuesta correctas


      }
      else
      {
        enlaceGlobal.mensajePorTrace("No hay datos para desplegar", EIGlobal.NivelLog.INFO);

        if(primeraRevision)
        {
          if(verificaDatos)
          {
            //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
            despliegaPaginaError("No hay datos.", tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
          }
          else
          {
            //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
            despliegaPaginaError((String)revisa[1], tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
          } // Fin if-else


        }
        else
        {
         //peticion.setAttribute("Encabezado",CreaEncabezado(tituloDisposicion, menuNavegacionPosicion, peticion));
         despliegaPaginaError("No se recibieron los par&aacute;metros", tituloPosicion, menuNavegacionPosicion, "s32030h", peticion, respuesta);
        } // Fin if primeraRevision


      } // Fin if hayDatos

    } // Fin metodo defaultAction



   /**<code><B><I>despliegaAtributos</I></B></code> despliega en pantalla todos los atributos
   * @param atributos es un objeto de tipo <I>Enumeration</I> que contiene atributos (de petici&oacute;n o sesi&oacute;n).
   * @param imprime es un objeto de tipo <I>PrintWriter</I> que imprime al flujo por defecto.
   */
    public void despliegaAtributos(Enumeration atributos, PrintWriter imprime, HttpServletRequest peticion)
    {
      String nombre;
      HttpSession estaSesion;

      estaSesion=peticion.getSession();

      while(atributos.hasMoreElements())
          {
            nombre=(String)atributos.nextElement();
            imprime.println(nombre+" "+(String)estaSesion.getAttribute(nombre)+"<BR>");
          } // Fin while
    } // Fin despliegaAtributos


   /**<code><B><I>revisaDatos</I></B></code> revisa que todos los atributos y par&aacute;metros no sean nulos.
   * <P>Regresa una arreglo de objetos con dos elementos.   El primero contien una valor booleano<BR>
   * iagual a verdadero si todos los datos son no nulos; falso si uno de ellos es nulo.
   * </P>
   * @param usuario Es un atributo de sesi&oacute;n
   * @param contrato Es un atributo de sesi&oacute;n
   * @param perfil Es el perfil del usuario; tambi&eacute;n es un atributo de sesi&oacute;n.
   * @param cuenta Es el n&uacute;mero de cuenta.
   * @param descripci&oacute;n de la cuenta
   */
    public Object[] revisaDatos(String usuario, String contrato, String perfil, String cuenta, String descripcion)
    {
      Object revision[]=new Object[2];
      boolean resultado=true;
      String mensaje=new String("");

      if(usuario==null || usuario.length()<1)
      {
        mensaje="No se tiene usuario. <BR>";
        resultado=false;
      } // Fin if usuario


      if(contrato==null || contrato.length()<1)
      {
        mensaje+="No se tiene contrato. <BR>";
        resultado=false;
      } // Fin if contrato


      if(perfil==null || perfil.length()<1)
      {
        mensaje+="No se tiene perfil. <BR>";
        resultado=false;
      } // Fin if perfil


      if(cuenta==null || cuenta.length()<1)
      {
        mensaje+="No se tiene cuenta. <BR>";
        resultado=false;
      } // Fin if cuenta

      if(descripcion==null || descripcion.length()<1)
      {
        mensaje+="No se tiene descripci&oacute;n. <BR>";
      } // Fin if descripcion


      revision[0]=new Boolean(resultado);
      revision[1]=mensaje;

      return revision;
    } // Fin revisaDatos


   /**<B><I><code>verificaParametros</code></I></B>
   * <P>Verifica que los par&aacute;metros que se reciban contengan datos.
   * </P>
   * @param ctasLineas Par&aacute;metro que contiene la l&iacute;nea a consultar.
   * @param noDisp Par&aacute;metro que contiene el n&uacute;mero de disposici&oacute;n.
   * @param noLineas Par&aacute;metro que contiene la l&iacute;neas de cr&eacute;dito; trabaja junto con <I>ctasLineas</I>.
   * @param enlaceGlobal envi&iacute;a mensajes a la bit&aacute;cora.
   */
   private boolean verificaParametros(String ctasLineas, String noDisp, String noLineas, EIGlobal enlaceGlobal)
   {


      if(ctasLineas==null || ctasLineas.length()<1)
      {
         if(noDisp==null || noDisp.length()<1)
         {
            enlaceGlobal.mensajePorTrace("No se recibieron par�metros.", EIGlobal.NivelLog.INFO);
            return false;
         } // Fin if noDisp
      }
      else
      {
         if(noLineas==null || noLineas.length()<1)
         {
            enlaceGlobal.mensajePorTrace("No se recibi� el par�metro \"noLineas\".", EIGlobal.NivelLog.INFO);
            return false;
         } // Fin if noLineas

      } // Fin if ctasLineas

      return true;
   } // Fin m�todo verificaParametros

} // Fin clase ceConPosicion