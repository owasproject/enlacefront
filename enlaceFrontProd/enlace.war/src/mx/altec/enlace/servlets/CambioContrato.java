package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


public class CambioContrato extends BaseServlet
{

	/**
	 * Constante para el valor session
	 */
	static final String SESSION = "session";
	/**
	 * Constante para el valor valida
	 */
	static final String VALIDA = "valida";
	/**
	 * Constante para el valor importe
	 */
	static final String IMPORTE = "importe";
	/**
	 * Constante para el valor tipoRelacion
	 */
	static final String TIPO_RELACION = "tipoRelacion";
	/**
	 * Constante para el valor newMenu
	 */
	static final String NEW_MENU = "newMenu";
	/**
	 * Constante para el valor MenuPrincipal
	 */
	static final String MENU_PRINCIPAL = "MenuPrincipal";
	/**
	 * Constante para el valor Encabezado
	 */
	static final String ENCABEZADO = "Encabezado";
	/**
	 * Constante para el valor Pago Referenciado SAT
	 */
	static final String PAGO_REFERENCIADO_SAT = "Pago Referenciado SAT";
	/**
	 * Constante para el valor Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; Pago Referenciado SAT
	 */
	static final String SERV_NVO_ESQ_PAG_SAT = "Servicios &gt; Nuevo Esquema de Pago de Impuestos &gt; Pago Referenciado SAT";
	/**
	 * Constante para el valor s55205h
	 */
	static final String S55205H = "s55205h";
	/**
	 * pipe
	 */
	static final String pipe = "|";

	/**
	 * Constante para el valor fecha
	 */
	private static final String FECHA = "fecha";
	/**
	 * Constante para el valor concepto
	 */
	private static final String CONCEPTO = "concepto";
	/**
	 * Constante para el valor titular
	 */
	private static final String TITULAR = "titular";
	/**
	 * Constante para el valor modulo
	 */
	private static final String MODULO = "Modulo";

	/**
	 * doGet
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServlet( req, res );
	 }
	/**
	 * doPost
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServlet( req, res );
	 }

    /**
     * @param req : req
     * @param res : res
     * @throws ServletException : ServletException
     * @throws IOException : IOException
     */
    public void inicioServlet( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
    {

		//Obtenemos los parametros del request
		EIGlobal.mensajePorTrace("\n\n\n OBTENER LOS PARAMETROS DEL REQUEST \n\n\n", EIGlobal.NivelLog.INFO);
		obtenerNombresParametros(req);

		String modulo ="";

	    HttpSession sess = req.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		boolean sesionvalida = SesionValida( req, res );

	    if (sesionvalida)
		{
			/******************************************Inicia validacion OTP**************************************/
			String valida = getFormParameter(req, VALIDA );
			if(validaPeticion( req,  res,session,sess,valida)){
				EIGlobal.mensajePorTrace("\n\n\n EN validacion OTP \n\n\n", EIGlobal.NivelLog.INFO);
			}
			/******************************************Termina validacion OTP**************************************/
			else
			{
				modulo=getFormParameter(req, MODULO);

				if(modulo == null || "".equals(modulo) )
				{
				   cambiaContrato( true, req , res );

					try{
						EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);
						short suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
						int numeroReferencia  = obten_referencia_operacion(suc_opera);
						String claveOperacion = BitaConstants.CTK_CAMBIO_CONTRATO;
						String concepto =  BitaConstants.CONCEPTO_CAMBIO_CONTRATO;
						EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);

						String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);
					} catch(Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("ENTRA  BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

					}


				}
				else
				{
					EIGlobal.mensajePorTrace( "CambioContrato defaultAction(): Iniciando linea de Captura ", EIGlobal.NivelLog.INFO);

					 /* Verificar la facultad ..*/
					if(session.getFacultad(session.FAC_CAP_PAG_PI))
					{
						if ("1".equals(modulo)){ 			/* Pantalla de inicio */
							//TODO: BIT CU 4091. EL cliente entra al flujo
							/*
	                 		 * VSWF ARR -I
	                 		 */
							if ( "ON".equals(Global.USAR_BITACORAS.trim()) ){
							try{
	                			BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
								if (req.getParameter(BitaConstants.FLUJO)!=null){
								       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
								  }else{
								   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
								  }
	                			BitaTransacBean bt = new BitaTransacBean();
	                			bt = (BitaTransacBean)bh.llenarBean(bt);
	                			bt.setNumBit(BitaConstants.ES_PAGO_LINEA_CAPTURA_ENTRA);
	                			bt.setContrato(session.getContractNumber());

	                			BitaHandler.getInstance().insertBitaTransac(bt);
	                			}catch(SQLException e){
	                				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	                			}catch (Exception e) {
	                				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}

							}
	                 		/*
	                 		 * VSWF ARR -F
	                 		 */
						   iniciaTransferencia(req,res);
						}
						if ("2".equals(modulo))
						{/* Lectura de parametros */
						   presentaTransferencia(req,res);
						}
						if ("3".equals(modulo))	/* Envio de datos */
						{
							///////////////////--------------challenge--------------------------
							String validaChallenge = req.getAttribute("challngeExito") != null
								? req.getAttribute("challngeExito").toString() : "";

							EIGlobal.mensajePorTrace("--------------->CambioContrato.java > validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

							if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
								EIGlobal.mensajePorTrace("--------------->CambioContrato.java > Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
								//ValidaOTP.guardaParametrosEnSession(req);
								guardaParametrosEnSession(req);
								validacionesRSA(req, res);
								return;
							}

							//Fin validacion rsa para challenge
							///////////////////-------------------------------------------------

							boolean valBitacora = true;
							if(  valida==null)
							{
								EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.INFO);

								boolean solVal=ValidaOTP.solicitaValidacion(
											session.getContractNumber(),IEnlace.PAGO_IMPUESTOS);

								if( session.getValidarToken() &&
									session.getFacultad(session.FAC_VAL_OTP) &&
									session.getToken().getStatus() == 1 &&
									solVal )
								{
									//VSWF-HGG -- Bandera para guardar el token en la bit�cora
									req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
									EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
									guardaParametrosEnSession(req);
									req.getSession().removeAttribute("mensajeSession");
									ValidaOTP.mensajeOTP(req, "ImpLC");
									ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);
								}
							    else{
	                                    ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
	                                    valida="1";
	                                    valBitacora = false;
                                                            }
							}
							//retoma el flujo
						    if( valida!=null && "1".equals(valida))
							{

								if (valBitacora)
								{
									try{
										EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);

										short suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
										int numeroReferencia  = obten_referencia_operacion(suc_opera);
										String claveOperacion = BitaConstants.CTK_PAGO_IMP_DER_PROD_LCAP;
										String concepto_ctk = BitaConstants.CTK_CONCEPTO_PAGO_IMP_DER_PROD_LCAP;
										double importeDouble = 0;
										String token = "0";
										String cuenta="0";


										if (getFormParameter(req,IMPORTE) != null && !getFormParameter(req,IMPORTE).equals(""))
										{String importeStr = getFormParameter(req,IMPORTE);
										importeDouble=Double.valueOf(importeStr).doubleValue();}

										if ((String)req.getSession().getAttribute("cuenta") != null ){
											cuenta=(String)req.getSession().getAttribute("cuenta");
										}

										if (req.getParameter("token") != null && !"".equals(req.getParameter("token")));
										{token = req.getParameter("token");}

										EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto_ctk, EIGlobal.NivelLog.INFO);

										String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto_ctk,0);
									} catch(Exception e) {
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace("ENTRA  BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

									}
								}
								EIGlobal.mensajePorTrace("\n\n\n******Enviar la transferencia 78******\n\n\n", EIGlobal.NivelLog.INFO);
								enviaTransferencia(req,res);

							}

						}
					} else
					{
						String laDireccion = "document.location='SinFacultades'";
						req.setAttribute( "plantillaElegida", laDireccion);
						req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
					}
		        }
			}//Fin else de validacion del OTP
	    }// Fin if sesionvalida
    }//Fin metodo inicioServlet

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
    /**
     * iniciaTransferencia
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws ServletException excepcion a manejar
     * @throws IOException excepcion a manejar
     */
	public void iniciaTransferencia( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CambioContrato - iniciaTransferencia(): Iniciando por primera vez.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		EIGlobal.mensajePorTrace("CambioContrato - iniciaTransferencia(): Limpiando variables de sesion.", EIGlobal.NivelLog.INFO);

		if(req.getSession().getAttribute("cuenta")!=null){
			req.getSession().removeAttribute("cuenta");
		}
		if(req.getSession().getAttribute(TIPO_RELACION)!=null){
			req.getSession().removeAttribute(TIPO_RELACION);
		}
		if(req.getSession().getAttribute(TITULAR)!=null){
			req.getSession().removeAttribute(TITULAR);
		}
		if(req.getSession().getAttribute(IMPORTE)!=null){
			req.getSession().removeAttribute(IMPORTE);
		}
		if(req.getSession().getAttribute(CONCEPTO)!=null){
			req.getSession().removeAttribute(CONCEPTO);
		}
		if(req.getSession().getAttribute(FECHA)!=null){
			req.getSession().removeAttribute(FECHA);
		}
		if(req.getSession().getAttribute("hora")!=null){
			req.getSession().removeAttribute("hora");
		}
		if(req.getSession().getAttribute("usuario")!=null){
			req.getSession().removeAttribute("usuario");
		}
		if(req.getSession().getAttribute("contrato")!=null){
			req.getSession().removeAttribute("contrato");
		}

		EIGlobal.mensajePorTrace( "CambioContrato - iniciaTransferencia(): Enviando variable de cuentas a session", EIGlobal.NivelLog.INFO);
		session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		/*
		 * Mejoras de Linea de Captura
		 * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
		 * Inicio RRR - Indra Marzo 2014
		 */
		req.setAttribute(ENCABEZADO, CreaEncabezado(PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,"s60010h", req));
		/*
		 * Fin RRR - Indra Marzo 2014
		 */
        EIGlobal.mensajePorTrace("CambioContrato - iniciaTransferencia(): Finalizando transferencia.", EIGlobal.NivelLog.INFO);
		evalTemplate("/jsp/MLC_Inicio.jsp", req, res);
	}


/*************************************************************************************/
/********************************************* presentar Transferencia - Modulo=1    */
/*************************************************************************************/
	/**
     * presentaTransferencia
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws ServletException excepcion a manejar
     * @throws IOException excepcion a manejar
     */
	public void presentaTransferencia( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
	    EIGlobal.mensajePorTrace("CambioContrato - presentaTransferencia(): presenta Transferencia.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		String cuentaCargo=getFormParameter(req,"cuentaCargo");
		String importe=getFormParameter(req,IMPORTE);
		String concepto=getFormParameter(req,CONCEPTO);

		String cuenta="";
		String tipoRelacion="";
		String titular="";

		EIGlobal.mensajePorTrace("CambioContrato - presentaTransferencia(): cuentaCargo " + cuentaCargo, EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("CambioContrato - presentaTransferencia(): importe " + importe, EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("CambioContrato - presentaTransferencia(): concepto " + concepto, EIGlobal.NivelLog.ERROR);

		EI_Tipo Cta=new EI_Tipo();

		Cta.iniciaObjeto(cuentaCargo+"@");
		cuenta=Cta.camposTabla[0][0];
		tipoRelacion=Cta.camposTabla[0][1];
		titular=Cta.camposTabla[0][2];

		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar calFecha = new GregorianCalendar();
		calFecha.setTime(Hoy);

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		/*
		 * Mejoras de Linea de Captura
		 * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
		 * Inicio RRR - Indra Marzo 2014
		 */
		req.setAttribute(ENCABEZADO, CreaEncabezado(PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,"s60020h", req));
		/*
		 * Fin RRR - Indra Marzo 2014
		 */
		req.getSession().setAttribute("cuenta",cuenta);
		req.getSession().setAttribute(TIPO_RELACION,tipoRelacion);
		req.getSession().setAttribute(TITULAR,titular);

		req.getSession().setAttribute(IMPORTE,importe);
		req.getSession().setAttribute(CONCEPTO,concepto.toUpperCase().trim());
		req.getSession().setAttribute(FECHA,EIGlobal.formatoFecha(calFecha,"dd/mm/aaaa"));

		EIGlobal.mensajePorTrace("CambioContrato - presentaTransferencia(): Finalizando transferencia.", EIGlobal.NivelLog.INFO);
		evalTemplate("/jsp/MLC_Envia.jsp", req, res);
	}

/*************************************************************************************/
/************************************************ enviar Transferencia - Modulo=2    */
/*************************************************************************************/
	/**
     * enviaTransferencia
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @throws ServletException excepcion a manejar
     * @throws IOException excepcion a manejar
     */
	public void enviaTransferencia( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CambioContrato - enviaTransferencia(): Envio de la transferencia.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		String importe="";
		String concepto="";
		String cuenta="";
		String tipoRelacion="";
		String titular="";

		String Trama="";
		String Result="";
		String cabecera="";
		String regServicio="";
		String codErrorOper="";

		String sqlCuenta="";

		String usuario="";
		String contrato="";
		String clavePerfil="";
		String tipoOperacion="";
		String medioEntrega="";
		String destino="";
		String destinoRelacion="";

		String referencia390="";
		String sreferencia="";
		String estatus="";
		String mensaje="";

		Vector sqlResultado=new Vector();

		EI_Query DB=new EI_Query();
		EI_Tipo Cta=new EI_Tipo();
		ServicioTux TuxGlobal = new ServicioTux();
		cuenta=(String)req.getSession().getAttribute("cuenta");
		tipoRelacion=(String)req.getSession().getAttribute(TIPO_RELACION);
		titular=(String)req.getSession().getAttribute(TITULAR);
		concepto=(String)req.getSession().getAttribute(CONCEPTO);
		importe=(String)req.getSession().getAttribute(IMPORTE);

		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		tipoOperacion="PILC";
		medioEntrega="1EWEB";

		sqlCuenta="SELECT cta_cheques FROM  cata_cheq_enlace where cve_chequera = 'PIMR' AND sucursal_region = '14'";

		EIGlobal.mensajePorTrace( "CambioContrato -> Se establecieron los datos para la trama, se procede con la cuenta de abono.", EIGlobal.NivelLog.INFO);

		sqlResultado=DB.ejecutaQueryCampo(sqlCuenta);
		if(sqlResultado.size()<1)
		 {
			EIGlobal.mensajePorTrace( "CambioContrato -> Se detiene el proceso, no se obtuvo la cuenta para el cargo.", EIGlobal.NivelLog.INFO);
			/*
			 * Mejoras de Linea de Captura
			 * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
			 * Inicio RRR - Indra Marzo 2014
			 */
			despliegaPaginaErrorURL("<i><font color=red>Error</font></i> de comunicaciones.<br> No se pudo obtener la <b>cuenta</b> para la transferencia. Por favor realice la operaci&oacute;n <font color=green> nuevamente</font>.<br><br><br>",PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,S55205H, req, res);

			return;
		 }


		  ///INICIO YHG
        EIGlobal.mensajePorTrace("NuevoPagoImpuestos-Pago Referenciado SAT - Valida Cuenta(): Trama entrada: "+cuenta, EIGlobal.NivelLog.INFO);
        /*
		 * Fin RRR - Indra Marzo 2014
		 */
        ValidaCuentaContrato valCtas = null;
        String datoscta[]=null;
        try
         {

			valCtas = new ValidaCuentaContrato();
     	   EIGlobal.mensajePorTrace("NuevoPagoImpuestos-Pago Referenciado SAT - cuentaValidar: "+cuenta, EIGlobal.NivelLog.INFO);

			  datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
															  session.getUserID8(),
													          session.getUserProfile(),
													          cuenta.trim(),
													          IEnlace.MCargo_transf_pesos);
				 EIGlobal.mensajePorTrace("NuevoPagoImpuestos-Pago Referenciado SAT - datoscta: "+datoscta, EIGlobal.NivelLog.INFO);

				 if(datoscta==null){
					 /*
					  * Mejoras de Linea de Captura
					  * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
					  * Inicio RRR - Indra Marzo 2014
					  */
					 despliegaPaginaErrorURL("<i><font color=red>Error</font></i>   Error H701:Cuenta Inv&aacute;lida .<br><br><br>",PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,S55205H, req, res);
					 /*
					  * Fin RRR - Indra Marzo 2014
					  */
					 return;
				}
		}

        catch (Exception e) {e.printStackTrace();}
	    finally
		{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}
		}

		destino=(String)sqlResultado.get(0);
		destinoRelacion="X";

			cabecera=	medioEntrega		+ pipe +
						usuario				+ pipe +
						tipoOperacion		+ pipe +
						contrato			+ pipe +
						usuario				+ pipe +
						clavePerfil			+ pipe;

			regServicio=
						cuenta				+ pipe +
						tipoRelacion		+ pipe +
						destino				+ pipe +
						destinoRelacion		+ pipe +
						importe				+ pipe +
						concepto			+ pipe + " |";

			Trama=cabecera + regServicio;

		try
		 {
			Hashtable hs = TuxGlobal.web_red(Trama);
			Result= (String) hs.get("BUFFER") +"";
			codErrorOper=(String) hs.get("COD_ERROR") +"";
		 }catch( java.rmi.RemoteException re )
		  {
			 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		  }catch (Exception e) {
			  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

		  }

		EIGlobal.mensajePorTrace( "CambioContrato -> Se recibe del servidor: "+ Result, EIGlobal.NivelLog.ERROR);
		//"TCT_0016      14"

		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar calFecha = new GregorianCalendar();
		calFecha.setTime(Hoy);

		if(Result==null || "".equals(Result))
		 {
		   EIGlobal.mensajePorTrace("CambioContrato-> El servidor regreso null se procesa el mensaje de error", EIGlobal.NivelLog.DEBUG);
		   /*
			* Mejoras de Linea de Captura
			* Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
			* Inicio RRR - Indra Marzo 2014
			*/
		   despliegaPaginaErrorURL("Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.",PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,S55205H, req, res);
		   /*
			* Fin RRR - Indra Marzo 2014
			*/
		   return;
		 }
		else
		 {
			if(Result.length()>=16){
				sreferencia=Result.substring(8,16);
			}
			if("OK".equals(Result.substring(0,2)))
			 {
				EIGlobal.mensajePorTrace("CambioContrato-> El servidor regreso codigo de error", EIGlobal.NivelLog.DEBUG);
			    if(Result.substring(16).indexOf("MANC")>=0)
				 {
				   EIGlobal.mensajePorTrace("CambioContrato-> OK MANC La operacion esta Mancomunada", EIGlobal.NivelLog.DEBUG);
				   estatus="MANCOMUNADA";
				   mensaje="<font color=green>Operaci&oacute;n Mancomunada</font>";
				 }
				else
				 {
					estatus="OK";
					mensaje="<font color=blue>Operaci&oacute;n Exitosa</font>";
					if(Result.length()>16){
						referencia390=Result.substring(16).trim();
					}else{
					  referencia390="0000";
					}
					EIGlobal.mensajePorTrace("CambioContrato-> Referencia390 "+referencia390, EIGlobal.NivelLog.DEBUG);
				 }
			 }
			else
			 {
				estatus="ERROR";
				if(Result.length()>16){
					mensaje=Result.substring(16);
				}
				else
				 {
					sreferencia="0";
				    mensaje="<font color=red>ERROR</font> en la operaci&oacute;n";
				 }

				if( "MANC".equals(Result.substring(0,4)) )
				 {
				   EIGlobal.mensajePorTrace("CambioContrato-> La operacion est&aacute; Mancomunada", EIGlobal.NivelLog.DEBUG);
				   estatus="MANCOMUNADA";
				   mensaje="<font color=green>Operaci&oacute;n Mancomunada</font>";
				 }

			 }
		 }
		 try{
			 EmailSender emailSender = new EmailSender();

			 if(emailSender.enviaNotificacion(codErrorOper)){

		     		   EmailDetails EmailDetails = new EmailDetails();

			           EIGlobal.mensajePorTrace("*********************CONTRATO: " + contrato, EIGlobal.NivelLog.INFO);
			           EIGlobal.mensajePorTrace("*********************NOMBRE CONTRATO: " + session.getNombreContrato().toString(), EIGlobal.NivelLog.INFO);
			           EIGlobal.mensajePorTrace("*********************ESTATUS: " + estatus, EIGlobal.NivelLog.INFO);
			           EIGlobal.mensajePorTrace("*********************REFERENCIA: " + sreferencia.trim(), EIGlobal.NivelLog.INFO);
			           String importeStr = getFormParameter(req,IMPORTE);
			    	   cuenta =(String)req.getSession().getAttribute("cuenta");
			    	   EIGlobal.mensajePorTrace("*********************IMPORTE: " + importeStr, EIGlobal.NivelLog.INFO);
			    	   EIGlobal.mensajePorTrace("*********************CUENTA: " + cuenta, EIGlobal.NivelLog.INFO);
			    	   EIGlobal.mensajePorTrace("*********************Result: " + Result, EIGlobal.NivelLog.INFO);



			    	   EmailDetails.setNumeroContrato(contrato);
			           EmailDetails.setRazonSocial(session.getNombreContrato().toString());
			           EmailDetails.setNumRef(sreferencia.trim());
			           EmailDetails.setNumCuentaCargo(cuenta);
			           EmailDetails.setImpTotal(ValidaOTP.formatoNumero(importeStr));
			           /*
						* Mejoras de Linea de Captura
						* Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
						* Inicio RRR - Indra Marzo 2014
						*/
			           EmailDetails.setTipoPagoImp("Impuesto Pago Referenciado SAT");
			           /*
						* Fin RRR - Indra Marzo 2014
						*/
   					   EmailDetails.setEstatusActual(codErrorOper);

						try {
							emailSender.sendNotificacion(req,IEnlace.NOT_PAGO_IMPUESTOS, EmailDetails);
						} catch (Exception e) {
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}

			}

 		} catch(Exception e) {
 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
 			EIGlobal.mensajePorTrace("ENTRA  NOTIFICACION MENSAJE -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);


 		}
		req.setAttribute("estatus",estatus);
		req.setAttribute("mensaje",mensaje);
		req.setAttribute("sreferencia",sreferencia.trim());
		req.setAttribute("referencia390",referencia390);
		req.getSession().setAttribute(FECHA,EIGlobal.formatoFecha(calFecha,"dd/mm/aaaa"));
		req.getSession().setAttribute("hora",EIGlobal.formatoFecha(calFecha,"th:tm"));
		req.getSession().setAttribute("usuario",session.getUserID8() + " " + session.getNombreUsuario());
		req.getSession().setAttribute("contrato",session.getContractNumber() + " " + session.getNombreContrato());

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		/*
		 * Mejoras de Linea de Captura
		 * Se modifica la leyenda Linea de Captura por Pago Referenciado SAT
		 * Inicio RRR - Indra Marzo 2014
		 */
		req.setAttribute(ENCABEZADO, CreaEncabezado(PAGO_REFERENCIADO_SAT,SERV_NVO_ESQ_PAG_SAT,"s60030h", req));
		/*
		 * Fin RRR - Indra Marzo 2014
		 */

        EIGlobal.mensajePorTrace("CambioContrato - enviaTransferencia(): Finalizando transferencia.", EIGlobal.NivelLog.INFO);
//      TODO: BIT CU 4091, A6
    	/*
		 * VSWF ARR -I
		 */
        if ( "ON".equals(Global.USAR_BITACORAS.trim()) ){
        try{

    	BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_PAGO_LINEA_CAPTURA_REALIZA_VALIDACION);
		bt.setContrato(contrato);
		if(Result!=null){
			 if( "OK".equals(Result.substring(0,2)) ){
				   bt.setIdErr(tipoOperacion+"0000");
			 }else if(Result.length()>8){
				  bt.setIdErr(Result.substring(0,8));
			 }else{
				  bt.setIdErr(Result.trim());
			 }
			}
		if(importe!=null && !"".equals(importe)){
			bt.setImporte(Double.parseDouble(importe));
		}
		if(tipoOperacion!=null && !"".equals(tipoOperacion)){
			bt.setServTransTux(tipoOperacion);
		}
		if(cuenta!=null && !"".equals(cuenta)){
			bt.setCctaOrig(cuenta);
		}

		if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
			&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
					.equals(BitaConstants.VALIDA))
		{
			bt.setIdToken(session.getToken().getSerialNumber());
		}

		req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
        }
		/*
		 * VSWF ARR -F
		 */
		evalTemplate("/jsp/MLC_Resultado.jsp", req, res);
	}

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	/**
     * despliegaPaginaErrorURL
     * @param Error Error
     * @param param1 param1
     * @param param2 param2
     * @param param3 param3
     * @param request HttpServletRequest
     * @param response response
     * @throws IOException excepcion a manejar
     * @throws ServletException excepcion a manejar
     */
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("CambioContrato - despliegaPaginaErrorURL(): Generando error.", EIGlobal.NivelLog.INFO);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3=S55205H;

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute( MENU_PRINCIPAL, session.getStrMenu() );
		request.setAttribute( ENCABEZADO, CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute( "URL", "CambioContrato?Modulo=1");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}
	/**
	 * guardaParametrosEnSession
	 * @param request HttpServletRequest
	 */
	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());

      javax.servlet.http.HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();
			/*
			req.getParameter ( VALIDA );
			req.getParameter(MODULO);

			req.getParameter("cuentaCargo");
			req.getParameter(IMPORTE);
			req.getParameter("concepto");

			request.getParameter("bs_ca")
			request.getParameter("templateElegido");
			*/

			String value;

			value = getFormParameter(request,VALIDA);
			if(null != value) {
				tmp.put(VALIDA,getFormParameter(request,VALIDA));
			}

            tmp.put(MODULO,getFormParameter(request,MODULO));

			tmp.put("cuentaCargo",getFormParameter(request,"cuentaCargo"));
            tmp.put(IMPORTE,getFormParameter(request,IMPORTE));
			tmp.put(CONCEPTO,getFormParameter(request,CONCEPTO));

			tmp.put("bs_ca",getFormParameter(request,"bs_ca"));
			tmp.put("templateElegido",getFormParameter(request,"templateElegido"));

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
				EIGlobal.mensajePorTrace("\n\n\n Dentro del while de la enumeracion, sesion dif. de null \n\n\n", EIGlobal.NivelLog.INFO);
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
    }

	/**
	 * @param request : request
	 */
	public void obtenerNombresParametros(HttpServletRequest request) {
		EIGlobal.mensajePorTrace("\n\n\n dentro de obtenerNombresParametros \n\n\n", EIGlobal.NivelLog.INFO);		// Obtiene todos los nombres de los par�metros
		Enumeration parametros = request.getParameterNames();

		String parametro = null; // variable para guardar el par�metro

		// Obteniendo los parametros pasados con la petici�n.
		while (parametros.hasMoreElements())
		{ //mientras que existan par�metros
			parametro = (String)parametros.nextElement(); //cargo el siguiente par�metro
			EIGlobal.mensajePorTrace("\n" + parametro + ":" + request.getParameter(parametro) + " ", EIGlobal.NivelLog.INFO);
		} //fin while
	}
}