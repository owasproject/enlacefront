package mx.altec.enlace.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaArchivoDatosBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConfigMasCuentasCuentaBO;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet ConfigMasPorCuentaServlet
 */
public class ConfigMasPorTCServlet extends BaseServlet {
	/**
	 * serial del servlet
	 */
	private static final long serialVersionUID = 3473908399656854207L;	
	/** objeto de servicio BO TC*/ 
	private static ConfigModEdoCtaTDCBO configMasTCCuentaBO = new ConfigModEdoCtaTDCBO();
	/** bean de bitacora */
	private static BitaAdminBean bean = new BitaAdminBean();

	/** referencia o folio de la operacion */
	private static int referencia = 0;
	/** bean de cuentas */
	private static List<ConfEdosCtaArchivoBean> cuentas = new ArrayList<ConfEdosCtaArchivoBean>();
	/** cliente */
	private static String cliente = "";
	/** nuemro de contrato */
	private static String contrato = "";
	
		
	/**
     * Default constructor. 
     */
    public ConfigMasPorTCServlet() {
    	bean.setTipoOp(" ");
		bean.setIdTabla(" ");
    }
	/**
	 * @see HttpServlet#doGet(HttpServletrequestuest requestuest, HttpServletResponse response)
	 */
    /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletrequestuest requestuest, HttpServletResponse response)
	 */
	/** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean sesionvalida = SesionValida(request, response);

		if (sesionvalida) {
			Date ahora = new Date();
	        SimpleDateFormat formateador = new SimpleDateFormat(EdoCtaConstantes.FECHA_CONFIG_MAS_TDC,Locale.getDefault());
	        String diaActual = formateador.format(ahora);
	        //String diasInhabiles = diasInhabilesDesp();
	        String diasInhabiles = diasInhabilesAnt();
	        Calendar c = Calendar.getInstance();
	        c.setTime(ahora);
	        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				
			if(!diasInhabiles.contains(diaActual) && dayOfWeek != 7 && dayOfWeek != 1 ){	
				EIGlobal.mensajePorTrace(
						String.format("%s >>>>>>>> YYY Entra a dias habiles <<<<<<<<",
								EdoCtaConstantes.LOG_TAG), 
						NivelLog.INFO);
				metodoInicio(request, response);
			} else {
				despliegaPaginaErrorURL(
						EdoCtaConstantes.MSJ_ERROR_DIAS,
					EdoCtaConstantes.TITULO_PANTALLA, EdoCtaConstantes.POS_MENU, EdoCtaConstantes.ARCHIVO_AYUDA, null, request, response);
            }
		}
	}
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	@SuppressWarnings("unchecked")
	private void metodoInicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s Inicio metodoInicio.",EdoCtaConstantes.LOG_TAG),
				NivelLog.DEBUG);
		String valida = request.getParameter("valida");
		EIGlobal.mensajePorTrace(String.format("%s valida [%s]",EdoCtaConstantes.LOG_TAG,valida)
				, NivelLog.DEBUG);
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(EdoCtaConstantes.BASE_RESOURCE);
		referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		cliente = (String) session.getUserID8();
		contrato = session.getContractNumber();
		
		EIGlobal.mensajePorTrace(String.format("%s Valida facultades FAC_EDO_CTA_MAS [%s]",EdoCtaConstantes.LOG_TAG,BaseResource.FAC_EDO_CTA_MAS),
				NivelLog.DEBUG);

		if(!session.getFacultad(BaseResource.FAC_EDO_CTA_MAS)) {
			despliegaPaginaErrorURL(
					EdoCtaConstantes.MSJ_ERROR_FAC,
					EdoCtaConstantes.TITULO_PANTALLA, EdoCtaConstantes.POS_MENU, EdoCtaConstantes.ARCHIVO_AYUDA, null, request, response);
			return;
		}
		
		String accion = request.getParameter("accion") != null ? request.getParameter("accion").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s accion [%s]",EdoCtaConstantes.LOG_TAG,accion),NivelLog.DEBUG);
		if("".equals(accion) || "null".equalsIgnoreCase(accion)){
			//[CMED-00] Entra a Modificacion Paperless Masivo
			bean.setReferencia(100000);
			BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_ENTRA_MOD_PAPERLESS_MASIVO, bean,false);
						
			boolean cambioContrato=ConfigMasPorTCAux.cambiaContrato(request.getSession(), contrato);
		
			if (httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null || cambioContrato) {
				cuentas = configMasTCCuentaBO.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()),session.getContractNumber(), session.getUserProfile());				
				httpSession.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
				EIGlobal.mensajePorTrace(String.format(" Entro a poner TC Cuentas en sesion YY--->%s",cuentas.size()),
						NivelLog.DEBUG);
				
			} else {
				cuentas = (List<ConfEdosCtaArchivoBean>)
							httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS); 
				EIGlobal.mensajePorTrace(String.format(" NO Entro a recuperar TC Cuentas YY--->%s",cuentas.size()),
						NivelLog.DEBUG);					 				
			}
								
			EIGlobal.mensajePorTrace(String.format("%s Longitud de la lista: [%s]",EdoCtaConstantes.LOG_TAG,cuentas.size()),
					NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, ConfigMasPorTCAux.cuentasTarjetasCredito(cuentas));
			EIGlobal.mensajePorTrace(String.format("%s Va a Agregar Encabezados %s",EdoCtaConstantes.LOG_TAG,cuentas.size()),
					NivelLog.DEBUG);
			agregarEncabezado("/jsp/configMasTCSeleccion.jsp", request, response);
		}else{
			if("3".equals(accion) || "4".equals(accion)){
				//Flujo de PDF
				if("3".equals(accion) && valida == null){
					EIGlobal.mensajePorTrace(String.format("%s accion = [%s] bitacorizaAdmin",EdoCtaConstantes.LOG_TAG,accion), 
							NivelLog.DEBUG);
					//[CMED-02] Modifica estado de suscripcion a Paperless de forma masiva
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_MODIF_EDO_SUSCRIP_PAPERLESS_MASIVO, bean, false);
					// [ACAP-00] Realizado guardado en bitacora de operaciones.
					BitacoraBO.bitacoraTCT(request, httpSession,
							BitaConstants.EA_CONV_ACEPT_PAPERLESS_CONFIRM,
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
							"", "", BitaConstants.COD_ERR_PDF_XML,
							BitaConstants.CONCEPTO_ACEPTA_CONV_PAPERLESS);
				}
				procesarCuentas(request, response, accion);
			}else{
				EIGlobal.mensajePorTrace(String.format("%s Accion [%s]",EdoCtaConstantes.LOG_TAG,accion), NivelLog.DEBUG);
				//BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_SELEC_CTAS_PAPERLESS_MASIVO, bean, false);
				String formato = "";
				String mostrar = "";
				if("1".equals(accion)){
					formato = "PDF";
					mostrar = "1";
					//[CMED-01] Selecciona cuentas para modificacion de suscripcion a Paperless Masivo
					EIGlobal.mensajePorTrace(String.format("%s Entra a bitacorizaAdmin",EdoCtaConstantes.LOG_TAG),NivelLog.DEBUG);
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_SELEC_CTAS_PAPERLESS_MASIVO, bean, false);
				}
				request.setAttribute("formato", formato);
				request.setAttribute("mostrar", mostrar);
				request.setAttribute("contrato", cliente);
				configuracion(request, response);
			}
		}
		
	}
	
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void configuracion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cadena = request.getParameter("cadenaCuenta") != null ? request.getParameter("cadenaCuenta").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s Cadena: [%s]",EdoCtaConstantes.LOG_TAG,cadena), NivelLog.DEBUG);
		List<ConfEdosCtaArchivoBean> cuentas = formatoCuentas(cadena, 2);
		EIGlobal.mensajePorTrace(String.format("%s Longitud de la lista: [%s]",EdoCtaConstantes.LOG_TAG,cuentas.size()),
				NivelLog.DEBUG);
		request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
		request.setAttribute("elementos", cuentas.size());
		agregarEncabezado("/jsp/configMasTCParam.jsp", request, response);
	}
	
	/**
	 * @param cadena : String que contiene las cuentas
	 * @param opcion : tipo de cadena a formatear
	 * @return List<ConfEdosCtaArchivoBean> cuentas
	 */
	private List<ConfEdosCtaArchivoBean> formatoCuentas(String cadena, int opcion){
		
		EIGlobal.mensajePorTrace(String.format("%s Inicia formatoCuentas --> Cadena: [%s]",EdoCtaConstantes.LOG_TAG,cadena),
				NivelLog.DEBUG);		
		List<ConfEdosCtaArchivoBean> cuentas = new ArrayList<ConfEdosCtaArchivoBean>();

		if(opcion == 1){
			EIGlobal.mensajePorTrace(String.format("%s Opcion [%s]",EdoCtaConstantes.LOG_TAG,opcion),NivelLog.DEBUG);
			String[] cadenas = cadena.split("##");
			for(int i = 0; i < cadenas.length; i++){
				String valores = cadenas[i];
				String[] datosCuenta = valores.split("#");
				EIGlobal.mensajePorTrace(String.format("%s cadenas[%s]=[%s]",EdoCtaConstantes.LOG_TAG,i,valores), NivelLog.DEBUG);
				ConfEdosCtaArchivoBean cuenta = new ConfEdosCtaArchivoBean();
				cuenta.setNomCuenta(datosCuenta[0]);
				cuenta.setNombreTitular(datosCuenta[1]);
				cuenta.setPapaerless(datosCuenta[2].charAt(0));
				cuentas.add(cuenta);
			}
		}		
		if(opcion == 2){
			EIGlobal.mensajePorTrace(String.format("%s Opcion [%s]",EdoCtaConstantes.LOG_TAG,opcion),NivelLog.DEBUG);
			String[] cadenas = cadena.split("#");
			for(int i = 0; i < cadenas.length; i++){
				EIGlobal.mensajePorTrace(String.format("%s cadenas[%s]=[%s]",EdoCtaConstantes.LOG_TAG,i,cadenas), NivelLog.DEBUG);
				ConfEdosCtaArchivoBean cuenta = new ConfEdosCtaArchivoBean();
				int longitud = cadenas[i].indexOf(' ');
				String noCuenta = cadenas[i].substring(0,longitud);
				String titular =  cadenas[i].substring(longitud+1, cadenas[i].length());
				cuenta.setNomCuenta(noCuenta);
				cuenta.setNombreTitular(titular);
				cuentas.add(cuenta);
			}
		}
		
		return cuentas;
	}
	
	/**
	 * @param cuentas : cuentas que se van a parametrizar
	 * @param usuario : usuario que realiza la operacion
	 * @param contrato : numero de contrato del cliente
	 * @return Map<String, String > con valores de la respuesta
	 */
	private Map<String, String > configurarCuentas(List<ConfEdosCtaArchivoBean> cuentas, String usuario, String contrato){
		
		ConfigMasCuentasCuentaBO configMasCuentasBO = new ConfigMasCuentasCuentaBO();
		
		for (ConfEdosCtaArchivoBean cuenta : cuentas) {
			cuenta.setTipoMasivo("003");
		}
		
		ConfEdosCtaArchivoDatosBean confEdosDatosBean = new ConfEdosCtaArchivoDatosBean();
		confEdosDatosBean.setListaDatos(cuentas);
		confEdosDatosBean.setIdUsuario(usuario);
		confEdosDatosBean.setNumeroCuenta(contrato);
		
		
		
		Map<String, String > respuesta = new HashMap<String, String >();
		
		respuesta = configMasCuentasBO.mandaAlmacenarDatos(confEdosDatosBean);
		
		return respuesta;
	} 
	
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @param accion : accion que indica el flujo a seguir
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	@SuppressWarnings("unchecked")
	private void procesarCuentas(HttpServletRequest request, HttpServletResponse response,
			String accion) throws ServletException, IOException{
		
		EIGlobal.mensajePorTrace(String.format("%s Inicia Metodo procesarCuentas.",EdoCtaConstantes.LOG_TAG),NivelLog.DEBUG);
		String valida = request.getParameter("valida");
		String cuentasConfiguradas = request.getParameter("cadena") != null ? request.getParameter("cadena").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s CuentasConfiguradas: [%s]",EdoCtaConstantes.LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);

		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(EdoCtaConstantes.BASE_RESOURCE);
		
		if( validaPeticion(request, response, session, request.getSession(), valida) ) {
			  EIGlobal.mensajePorTrace(String.format("%s >>>>> ENTRO A VALIDAR LA PETICION <<<<< ",EdoCtaConstantes.LOG_TAG), NivelLog.DEBUG);
		}else {
			EIGlobal.mensajePorTrace(String.format("%s cuentas Configuradas: [%s]",EdoCtaConstantes.LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
			if (valida == null) {				
				EIGlobal.mensajePorTrace(String.format("Valida [%s]",EdoCtaConstantes.LOG_TAG,valida), NivelLog.DEBUG);
				//[ACAP-01] Acepta Convenio de Aceptacion Paperless Masivo
				bean.setReferencia(100000);
				BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_ACEPT_CONV_PAPERLESS_MASIVO, bean, false);
				valida = ConfigMasCuentasCuentaBO.validaToken(request, response, "ConfigMasPorTCServlet");
			}
			EIGlobal.mensajePorTrace(String.format("%s cuenta(s) Configurada(s): [%s]",EdoCtaConstantes.LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
			if (valida != null && "1".equals(valida)) {
				if("3".equals(accion)){					
					//[CECM-00] Confirma Modificacion Paperless Masiva
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_CONFR_MODIF_PAPERLESS_MASIVO, bean, true);
				}			
				List<ConfEdosCtaArchivoBean> cuentasParametrizadas = new ArrayList<ConfEdosCtaArchivoBean>();
				EIGlobal.mensajePorTrace(String.format("%s Cuenta(s) Configurada(s): [%s]",EdoCtaConstantes.LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
				cuentasParametrizadas = formatoCuentas(cuentasConfiguradas,1);
				
				//TODO eliminar cjpv
				EIGlobal.mensajePorTrace("ConfigMasPorTCServlet :: Cuentas Parametrizadas: ["
						+ cuentasParametrizadas + "]", EIGlobal.NivelLog.INFO);
				
				
				if(cuentasParametrizadas != null){
					
					//Bitacorizacion
					BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.EA_ESTADOS_CUENTA_ARCHIVO_CONFIRM, 
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), "", "", BitaConstants.COD_ERR_PDF_XML, BitaConstants.CECM);
					
					//Registro en BD
					EIGlobal.mensajePorTrace(String.format("%s El usuario es: [%s]",EdoCtaConstantes.LOG_TAG,cliente),NivelLog.DEBUG);
					Map<String, String > respuesta = configurarCuentas(cuentasParametrizadas, cliente , contrato);
					String folio = "";
					if(respuesta != null && "none".equals(respuesta.get("error"))){
						folio = respuesta.get("secuencia");
						//[CMED-03] Finaliza la modificacion de Paperless masiva
						
						BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_FINAL_MODIF_PAPERLESS_MASIVO,setBitacoraAux(folio,""), false);
						BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.EA_ESTADOS_CUENTA_ARCHIVO_FIN, obten_referencia_operacion
								(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), "", "", BitaConstants.COD_ERR_PDF_XML, BitaConstants.CMED);		
						//envio de notficaciones
						String [] datos = new String[3];
						datos[0] = cuentasParametrizadas.get(0).getFormato();
						datos[1] = String.valueOf(cuentasParametrizadas.size());
						datos[2] = "0";
						EIGlobal.mensajePorTrace(String.format("%s datos[] = {%s}",EdoCtaConstantes.LOG_TAG,datos),NivelLog.DEBUG);
						ConfigMasPorTCAux.enviarNotificaciones(session, referencia, folio, request, datos);
						
						request.setAttribute("folio", folio);
						
						//TODO eliminar cjpv
						EIGlobal.mensajePorTrace("ConfigMasPorTCServlet :: Folio Masivo Configuracion: ["
								+ folio + "]", EIGlobal.NivelLog.INFO);
						
						
						//cuentas = configMasCuentasCuenta.consultaCuentas(request, response);
					
						cuentas = (List<ConfEdosCtaArchivoBean>)
										httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS); 								 				
					
						//TODO eliminar cjpv
						EIGlobal.mensajePorTrace("ConfigMasPorTCServlet :: Cuentas Tama�o: ["
								+ cuentas.size() + "]", EIGlobal.NivelLog.INFO);
						
						request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, ConfigMasPorTCAux.cuentasTarjetasCredito(cuentas));
						
						agregarEncabezado("/jsp/configMasTCSeleccion.jsp", request, response);
					}else {
						despliegaPaginaErrorURL("Error al almacenar los datos", EdoCtaConstantes.TITULO_PANTALLA,
								EdoCtaConstantes.POS_MENU,
								EdoCtaConstantes.ARCHIVO_AYUDA, "ConfigMasPorTCServlet", request, response);
					}
				}
			}						
		}
	}
	
	/**
	 * @param jsp : jsp al que se direccionara
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void agregarEncabezado(String jsp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		EIGlobal.mensajePorTrace(String.format("%s Entra a agregarEncabezados",EdoCtaConstantes.LOG_TAG), NivelLog.DEBUG);
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(EdoCtaConstantes.BASE_RESOURCE);
		request.setAttribute(EdoCtaConstantes.OPCION, request.getParameter(EdoCtaConstantes.OPCION));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado(EdoCtaConstantes.TITULO_PANTALLA, EdoCtaConstantes.POS_MENU, EdoCtaConstantes.ARCHIVO_AYUDA, request));
		EIGlobal.mensajePorTrace(String.format("%s Termina agregarEncabezados",EdoCtaConstantes.LOG_TAG), NivelLog.DEBUG);
		evalTemplate(jsp, request, response);		
	}
	
	/**
	 * Auxiliar para generar el bean
	 * @param secuencia : secuencia
	 * @param archivoName : archivoName
	 * @return BitaAdminBean : bean
	 */
	private BitaAdminBean setBitacoraAux(String secuencia, String archivoName) {
		BitaAdminBean bean = new BitaAdminBean();
		bean.setIdTabla(secuencia);
		bean.setCampo("ID_EDO_CTA_CTRL");
		bean.setTabla("EWEB_EDO_CTA_CTRL");
		bean.setDato(archivoName);
		bean.setDatoNvo(secuencia);
		bean.setTipoOp("I");
		bean.setReferencia(100000);
		
		return bean;
	}	
}