package mx.altec.enlace.servlets;

import java.util.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.CuentasBO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

/*****************

 ---- Historial de Modificaciones

15/10/2003: Incidencia IM77993. Elizabeth Infante Colina - EIC
Descripci�n: Se modificar� el fuente MCL_Pagos.java para
que el importe que forma parte de la trama que se env�a
no lleve el signo de pesos, ya que actualmente al importe
se le da formato de moneda. Por otro lado se seleccionar�
la cuenta de cr�dito para que forme parte de la trama y no
la cuenta de cheques como actualmente se env�a al servidor
de tuxedo, motivo por el cual ibm390 nos marca cuenta inexistente.

******************/
public class MCL_Pagos extends BaseServlet {

	String pipe="|";

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException {

		String contrato    = "";
		String usuario	   = "";
		String usuario7	   = "";
		String clavePerfil = "";

		String estatusError= "";
		String errores	   = "";

		String Trama	   = "";
		String Result	   = "";
		Hashtable htResult = null;

		EI_Tipo TCTArchivo   = new EI_Tipo(this);
		EI_Tipo FacArchivo   = new EI_Tipo(this);
		EI_Tipo Pagos	     = new EI_Tipo(this);

		boolean existenCuentas = false;

		EIGlobal.mensajePorTrace("MCL_Pagos - execute(): Entrando a Prepagos de Credito.", EIGlobal.NivelLog.INFO);

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		String Modulo=request.getParameter("Modulo");
		if(Modulo.trim().equals("1"))
		  efectuaPago( request, response);
		else
		 {
			boolean sesionvalida = SesionValida( request, response );
			if(sesionvalida)
			 {
				//Variables de sesion
				contrato=session.getContractNumber();
				usuario=session.getUserID8();
				usuario7=session.getUserID();
				clavePerfil=session.getUserProfile();

				String nombreArchivo = IEnlace.LOCAL_TMP_DIR+"/"+usuario+".amb";
				//########## Cambiar solo para pruebas
				//if(tipoCuenta.equals("0"))
				//String nombreArchivo="/tmp/rada.amb.tmp";


				String tipo	     = "";
				String arcLinea      = "";
				String strTmp	     = "";
				StringBuffer cadenaTCT   = new StringBuffer("");
				String cadenaFac     = "";
				String radTabla      = "";
				String tipoRelacion  = "";




				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);

				if(ArcEnt.abreArchivoLectura())
				{
				  boolean noError=true;
				  do
				   {
					 arcLinea=ArcEnt.leeLinea();
					 if(!arcLinea.equals("ERROR"))
					  {
						if(arcLinea.substring(0,5).trim().equals("ERROR"))
						  break;




						//if(arcLinea.substring(0,1).equals("2"))
						//cadenaTCT+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";


						// Obtiene facultades para cuentas ...
						if(arcLinea.substring(0,1).equals("7"))
						  cadenaFac+=arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @";
					  }
					 else
					   noError=false;
				   }while(noError);
				  ArcEnt.cierraArchivo();

					// Obtiene cuentas asociadas ...

				  CuentasBO cuentasBo = new CuentasBO();
                  EIGlobal.mensajePorTrace("MCL_Pagos.java -> Consulta las cuentas del contrato :"+ contrato, EIGlobal.NivelLog.INFO);
                  List<String> cuentas=cuentasBo.consultaCuentasRel(contrato);
                  EIGlobal.mensajePorTrace("MCL_Pagos.java -> Numero de cuentas obtenidas :"+cuentas.size(), EIGlobal.NivelLog.INFO);
                  Iterator<String> cuentasIter = cuentas.iterator();
                  while(cuentasIter.hasNext()){
                         arcLinea = cuentasIter.next();
                         EIGlobal.mensajePorTrace("MCL_MenuPos.java ->" + arcLinea , EIGlobal.NivelLog.DEBUG);
                         if(!arcLinea.equals("ERROR"))
                          {
                                      if(arcLinea.substring(0,5).trim().equals("ERROR"))
                                      {
                                             break;
                                      }
                                      if(arcLinea.substring(0,1).equals("2"))
                                       {
                                             cadenaTCT.append(arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @");

                                      }
                                 }
                                else{
                                      break;
                                }
                  }
                  EIGlobal.mensajePorTrace("MCL_Pagos.java -> Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);

				  // Registros de lineas y tarjetas (5,0)
				  TCTArchivo.iniciaObjeto(';','@',cadenaTCT.toString());
				  EIGlobal.mensajePorTrace("MCL_Pagos.java TCTArchivo->" + TCTArchivo, EIGlobal.NivelLog.INFO);
				  // Registros de facultades (7)
				  FacArchivo.iniciaObjeto(';','@',cadenaFac);
				  EIGlobal.mensajePorTrace("MCL_MenuPos.java FacArchivo->" + FacArchivo, EIGlobal.NivelLog.INFO);

				  if(TCTArchivo.totalRegistros>=1)
				   {
					 for(int i=0;i<TCTArchivo.totalRegistros;i++)
					  {
					   if(TCTArchivo.camposTabla[i][4].equals("0"))
						{
						   //########### Modificacion incidencia
						   tipoRelacion = TCTArchivo.camposTabla[i][3];
							if(tipoRelacion.trim().equals("P"))
							   existenCuentas=true;
						   if(tieneFacultad(FacArchivo,TCTArchivo.camposTabla[i][1].trim(),"TECRESALDOSC",request) && tieneFacultad(FacArchivo,TCTArchivo.camposTabla[i][1].trim(),"TECREPPAGOSM",request) && tipoRelacion.trim().equals("P"))
 						   //MHG
                           //if(tieneFacultad(FacArchivo,TCTArchivo.camposTabla[i][1].trim(),"ALTACTAINTERN",request))
						   {
							 tipo="Linea";
							 Trama="";
							 Trama+=IEnlace.medioEntrega2  +pipe;
							 Trama+=usuario 	       +pipe;
							 Trama+="RG01"		       +pipe;
							 Trama+=contrato	       +pipe;
							 Trama+=usuario 	       +pipe;
							 Trama+=clavePerfil	       +pipe;
							 Trama+=TCTArchivo.camposTabla[i][1].substring(0,2)   + "@";
							 Trama+=TCTArchivo.camposTabla[i][1].substring(2,10)  + "@";
							 Trama+=TCTArchivo.camposTabla[i][1].substring(10,11) + "@";
							 //MSD Q05-0029909 inicio lineas agregadas
							 String IP = " ";
							 String fechaHr = "";												//variables locales al m�todo
							 IP = request.getRemoteAddr();											//ObtenerIP
							 java.util.Date fechaHrAct = new java.util.Date();
							 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
							 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
							 //MSD Q05-0029909 fin lineas agregada


							 EIGlobal.mensajePorTrace(fechaHr+"MCL_Pagos - execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);
							 //########## Cambiar ....
							 try {
								htResult = tuxGlobal.web_red( Trama );
							 }catch( java.rmi.RemoteException re ){
								 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
							 } catch(Exception e) {
								 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							 }

							try{
								 Result = ( String ) htResult.get( "BUFFER" );
							 if(Result==null || Result.equals("null"))
								Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";
							 else
							    Result=formateaResultLinea(usuario).trim();
							 EIGlobal.mensajePorTrace("MCL_Pagos - execute(): Trama result: "+Result, EIGlobal.NivelLog.DEBUG);
							 EIGlobal.mensajePorTrace("������������ MCL_Pagos::default linea 183 desarrollo BUFFER ->" + Result, EIGlobal.NivelLog.INFO);

							 EI_Tipo ResultTmp=new EI_Tipo(this);

							 if(Result!=null)
							 {
								 if(Result.length()>=8 && Result.substring(0,8).equals("RGOS0000"))
							  {
								String adeudo	  = calculaAdeudoTotal(Result);
								String disponible = calculaCapitalDisponible(Result,adeudo);

								Result+=" @";
								ResultTmp.iniciaObjeto(Result);

								//Chequera
								strTmp+=ResultTmp.camposTabla[0][35]+"-";
								strTmp+=ResultTmp.camposTabla[0][36]+"-";
								strTmp+=ResultTmp.camposTabla[0][37]+ pipe;
								strTmp+=TCTArchivo.camposTabla[i][1]+pipe;
								strTmp+=TCTArchivo.camposTabla[i][2]+pipe;
								strTmp+=EnlaceGlobal.fechaHoy("dd/mm/aa|th:tm:ts")+pipe;
								strTmp+=ResultTmp.camposTabla[0][29]+pipe;
								strTmp+=adeudo+pipe;

								strTmp+=TCTArchivo.camposTabla[i][1].trim()+pipe;
								strTmp+=TCTArchivo.camposTabla[i][2].trim()+pipe;
								strTmp+=TCTArchivo.camposTabla[i][3].trim()+pipe;
								strTmp+=TCTArchivo.camposTabla[i][4].trim()+"@";
							  }
							 else
							  {
							   if( Result.equals("OPENFAIL"))
								{
								  estatusError+="Error "+TCTArchivo.camposTabla[i][1]+"<br>";
								  //errores+="\nalert('Error " + TCTArchivo.camposTabla[i][1]+"');";
								  errores+="\ncuadroDialogo('Error " + TCTArchivo.camposTabla[i][1]+"', 3);";
								}
							   else
								{
								  estatusError+="Cuenta "+TCTArchivo.camposTabla[i][1]+" "+Result.substring(16,Result.length())+"<br>";
								  //errores+="\nalert('Error en la cuenta: " + TCTArchivo.camposTabla[i][1]+".\\n"+Result.substring(16,Result.length())+"');";
								  errores+="\ncuadroDialogo('Error en la cuenta: " + TCTArchivo.camposTabla[i][1]+".\\n"+Result.substring(16,Result.length())+"', 3);";
								}
							  }
							 }
							}catch(Exception e){
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						   }
						}
					  }
				   }
				 Pagos.iniciaObjeto(strTmp);
				 //TODO CU3081 Entra al flujo
				 	/**
			 		 * VSWF - FVC - I
			 		 * 17/Enero/2007
			 		 */
				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
			 		try {
			 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
						BitaHelper bh = new BitaHelperImpl(request, session, sess);

						BitaTransacBean bt = new BitaTransacBean();
						bh.incrementaFolioFlujo((String)request.getParameter(BitaConstants.FLUJO));
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(mx.altec.enlace.bita.BitaConstants.ER_CREDITO_LINEA_PREPAGO_ENTRA);
						bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				 }
			 		/**
			 		 * VSWF - FVC - F
			 		 */
				 generaTablaPagos(Pagos,FacArchivo,existenCuentas,estatusError,errores, request, response );
			    }
			   else
				 despliegaPaginaError("Su transacci&oacute;n no puede ser atendida en este momento.",
						"Prepagos de Cr&eacute;dito",
						"Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ","s26310h", request, response );
			 }
			else
			 if(sesionvalida)
			   {
			     request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				 evalTemplate( IEnlace.ERROR_TMPL, request, response );
			   }
		 }
	  EIGlobal.mensajePorTrace("MCL_Pagos - execute(): Saliendo de Prepagos de Credito.", EIGlobal.NivelLog.INFO);
    }

/*************************************************************************************/
/***************************************************************** tabla Pagos	     */
/*************************************************************************************/
    public void generaTablaPagos(EI_Tipo Pagos,EI_Tipo FacArchivo,boolean existenCuentas,String estatusError,String errores, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

       String strTabla	= "";
       String[] titulos = {"",
			   //"Chequera",
			   "Cr&eacute;dito",
			   "Descripci&oacute;n",
			   //"Fecha",
			   //"Hora",
			   //"Sdo. Chequera",
			   //"Sdo. Cr�dito"
			   };

       //int[] datos={5,1,0,1,2,5,6,7};
       //int[] value={4,0,1,2,3};
	   //int[] align={0,0,0,2,2,1};

	   int[] datos={2,1,1,2};
	   // credito|descripcion|tipo relacion|tipo cuenta|chequera@
	   int[] value={5,1,2,9,10,0};
	   int[] align={0,0,1};

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = request.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

       if(Pagos.totalRegistros>=1)
		{
		  EnlaceGlobal.formateaImporte(Pagos,5);
		  EnlaceGlobal.formateaImporte(Pagos,6);
		  strTabla+=Pagos.generaTabla(titulos,datos,value,align);

		  request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		  request.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

		  request.setAttribute("FacArchivo",FacArchivo.strOriginal);
		  request.setAttribute("Tabla",strTabla);
		  request.setAttribute("Errores",errores);
		  request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));

		  request.setAttribute("MenuPrincipal", session.getStrMenu());
		  request.setAttribute("newMenu", session.getFuncionesDeMenu());
		  request.setAttribute("Encabezado", CreaEncabezado("Prepagos de Cr&eacute;dito","Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ","s26320h",request));

		  evalTemplate("/jsp/MCL_Pagos.jsp", request, response );
	    }
	   else
		{
		  if(existenCuentas && estatusError.trim().equals(""))
			  despliegaPaginaError("No tiene Facultad.","Prepagos de Cr&eacute;dito",
				"Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ",
					"s26310h",request, response );
		   else
		  if(existenCuentas)
			despliegaPaginaError(estatusError,"Prepagos de Cr&eacute;dito",
				"Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ",
					"s26310h",request, response );
		  else
			despliegaPaginaError("No se encontraron Lineas de Cr&eacute;dito Asociadas.",
				"Prepagos de Cr&eacute;dito",
				"Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ",
					"s26310h",request, response );
		}
     }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
	public void efectuaPago( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("������������ Entra a MCL_Pagos::efectuaPago", EIGlobal.NivelLog.INFO);

		String strTabla    = "";
		String resultPago  = "";
		String titulo	   = "";
		String tipo	   = "";
		String estatus	   = "";
		String descripcion = "";
		String referencia  = "";
		String spc	   = "		       ";

		String Trama	   = "";
		String Result	   = "";
		String codError	   = "";
		Hashtable htResult     = null;

		String contrato    = "";
		String usuario	   = "";
		String clavePerfil = "";

		String importe	   = request.getParameter("Importe");

		EI_Tipo Seleccion    = new EI_Tipo(this);

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		// Carlos Maya Ramirez (CMR) IM314336 29/03/2004
		// Definir variables para obtener tipo de firma
		char tipoFirma = ' ';
		java.sql.ResultSet rs = null;
		java.sql.Connection conn = null;
		java.sql.Statement stmt = null;

		if(SesionValidaCerrar(request,response))
		{
		  contrato=session.getContractNumber();
		  usuario=session.getUserID8();
		  clavePerfil=session.getUserProfile();

		  Seleccion.iniciaObjeto(request.getParameter("radTabla"));
		  if(Seleccion.totalRegistros>=1)
		   {
			 Trama="";
			 Trama+=IEnlace.medioEntrega1	+pipe;
			 Trama+=usuario 		+pipe;
			 Trama+="RG03"			+pipe;
			 Trama+=contrato		+pipe;
			 Trama+=usuario 		+pipe;
			 Trama+=clavePerfil		+pipe;

//IM314336 - NVS - 14-Abril-2005
//System.out.println("*** NVS --> cuenta origen v3= " +  Seleccion.camposTabla[0][4]);
			 Trama+=Seleccion.camposTabla[0][4].substring(0, 2);
//System.out.println("*** NVS --> cuenta origen v3= " +  Seleccion.camposTabla[0][4].substring(0,2));
			 Trama+=Seleccion.camposTabla[0][4].substring(3,11);
//System.out.println("*** NVS --> cuenta origen v3= " +  Seleccion.camposTabla[0][4].substring(3,11));
			 Trama+=Seleccion.camposTabla[0][4].substring(12,13) + "|";
//System.out.println("*** NVS --> cuenta origen v3= " +  Seleccion.camposTabla[0][4].substring(12,13));
			/* 15/10/2003. EIC INICIO*/
			 Trama+=Seleccion.camposTabla[0][0].substring(0,2)   + "|";
			 Trama+=Seleccion.camposTabla[0][0].substring(2,10)  + "|";
			 Trama+=Seleccion.camposTabla[0][0].substring(10,11) + "|";

			 Trama+=EnlaceGlobal.fechaHoy("ddmmaa") +pipe;
			 Trama+=importe 	   +pipe+" "+pipe;
		     
		     importe = FormatoMoneda(importe);
			/* 15/10/2003. EIC FIN*/

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = request.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


			 EIGlobal.mensajePorTrace(fechaHr+"MCL_Pagos - efectuaPago(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

			 //########## Cambiar ....
			 try {
				htResult = tuxGlobal.web_red( Trama );
			 } catch ( java.rmi.RemoteException re ) {
				 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
			 }catch ( Exception e ){
				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			 }
			 Result= ( String ) htResult.get( "BUFFER" );
			 codError = (String) htResult.get("COD_ERROR");
			 EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago BUFFER linea 450 desarrollo ->" + Result, EIGlobal.NivelLog.INFO);
			 //Result="ERROR   98761   Su operaci�n no se llevar� a cabo.";
			 //Result="OK	   98713   Aceptado";
			 //Result="OK	   98713   MANC";
			 if(Result==null || Result.equals("null"))
				Result="ERROR  12345678 Su transacci&oacute;n no puede ser atendida.";

			 EIGlobal.mensajePorTrace("MCL_Pagos - efectuaPago(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
			 String trama= Result;
			 if(Result.length()<16)
			   Result+=spc.substring(0,16-Result.length());
			 if(Result.substring(0,2).trim().equals("OK"))
			 {
				//resultPago="alert('Se efectuo un pago de "+importe+" a la cuenta "+Seleccion.camposTabla[0][0]+"');";
				//referencia=Result.substring(8,16);
				 referencia=trama.substring(8,16);
			         EIGlobal.mensajePorTrace("MCL_Pagos - efectuaPago(): Referencia: "+ referencia+" trama: " + trama, EIGlobal.NivelLog.DEBUG);
				if(Seleccion.camposTabla[0][1].trim().equals(""))
				 descripcion="SIN DESCRIPCI&Oacute;N";
				else
				 descripcion=Seleccion.camposTabla[0][1];

				if(Seleccion.camposTabla[0][2].trim().equals("T"))
				 tipo="TERCEROS";
				else
				 if(Seleccion.camposTabla[0][2].trim().equals("P"))
				  tipo="PROPIA";
				 else
				  tipo="NO REGISTRADA";

				// Carlos Maya Ramirez (CMR) IM314336 29/03/2004
				//if(Result.substring(16,Result.length()).trim().equals("MANC"))
				//if(Result.indexOf("MANC") != -1)
				if(trama.indexOf("MANC") != -1) {
				    //System.out.println("MCL_Pagos - efectuaPago(): Mancomunada");
				 	titulo="MANCOMUNADA";
				} else {
				    //System.out.println("MCL_Pagos - efectuaPago(): REALIZADA");
				    titulo="REALIZADA";
				}

				/*
				strTabla+="<table border=0 width=100%>";
				strTabla+="\n<tr><th colspan=2 bgcolor='#DDDDDD'> Informaci&oacute;n del Pago </th></tr>";
				strTabla+="\n<tr><td bgcolor='#FFFFFF' align=left> <b>Referencia</b> &nbsp; </td><td align=rigth bgcolor='#FFFFFF'> &nbsp; "+referencia+" </td></tr>";
				strTabla+="\n<tr><td bgcolor='#F0F0F0' align=left> <b>Cuenta</b> &nbsp; </td><td align=rigth bgcolor='#F0F0F0'> &nbsp; "+Seleccion.camposTabla[0][0]+" </td></tr>";
				strTabla+="\n<tr><td bgcolor='#FFFFFF' align=left> <b>Descripci&oacute;n</b> &nbsp; </td><td align=rigth bgcolor='#FFFFFF'> &nbsp; "+descripcion+" </td></tr>";
				strTabla+="\n<tr><td bgcolor='#F0F0F0' align=left> <b>Tipo Cuenta</b> &nbsp; </td><td align=rigth bgcolor='#F0F0F0'> &nbsp; "+tipo+" </td></tr>";
				strTabla+="\n<tr><td bgcolor='#FFFFFF' align=left> <b>Importe</b> &nbsp; </td><td align=rigth bgcolor='#FFFFFF'> &nbsp; "+importe+" </td></tr>";
				strTabla+="\n<tr><td bgcolor='#F0F0F0' align=left> <b>Estatus</b> &nbsp; </td><td align=rigth bgcolor='#F0F0F0'> &nbsp; "+estatus+" </td></tr>";
				strTabla+="\n</table>";
				*/

				for(int a=0;a<Seleccion.totalCampos;a++)
				  strTabla+=Seleccion.camposTabla[0][a]+"<br>";

				request.setAttribute("Chequera",Seleccion.camposTabla[0][4]);
				request.setAttribute("Cuenta",Seleccion.camposTabla[0][0]);
				request.setAttribute("Referencia",referencia);
				request.setAttribute("Concepto",descripcion);
				request.setAttribute("Importe",importe);
				request.setAttribute("FechaHora",EnlaceGlobal.fechaHoy("dd/mm/aaaa &nbsp; th:tm"));

				EIGlobal.mensajePorTrace("MCL_Pagos - efectuaPago(): Obteniendo datos de forma req ...", EIGlobal.NivelLog.DEBUG);

				request.setAttribute("NumContrato",request.getParameter("NumContrato"));
				request.setAttribute("NomContrato",request.getParameter("NomContrato"));
				request.setAttribute("NumUsuario",request.getParameter("NumUsuario"));
				request.setAttribute("NomUsuario",request.getParameter("NomUsuario"));
				request.setAttribute("ClaveBanco",request.getParameter("ClaveBanco"));

				request.setAttribute("ResultPago",resultPago);
				request.setAttribute("titulo",titulo);
				request.setAttribute("Tabla",strTabla);
				request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));
				request.setAttribute("Encabezado", CreaEncabezado("Prepagos de Cr&eacute;dito","Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ","s26320h",request));//IM314336 SLF
				request.setAttribute("newMenu", session.getFuncionesDeMenu());//IM314336 SLF
				request.setAttribute("MenuPrincipal", session.getStrMenu());//IM314336 SLF
				request.setAttribute("Comprobante", Comprobante(request));//IM314336 SLF

				/***********************************************************************/
				HttpSession ses = request.getSession();

				ses.setAttribute("Chequera",Seleccion.camposTabla[0][4]);
				ses.setAttribute("Cuenta",Seleccion.camposTabla[0][0]);
				ses.setAttribute("Referencia",referencia);
				ses.setAttribute("Concepto",descripcion);
				ses.setAttribute("Importe",importe);
				ses.setAttribute("FechaHora",EnlaceGlobal.fechaHoy("dd/mm/aaaa &nbsp; th:tm"));

				EIGlobal.mensajePorTrace("MCL_Pagos - efectuaPago(): Obteniendo datos de forma ses ...", EIGlobal.NivelLog.DEBUG);

				ses.setAttribute("NumContrato",request.getParameter("NumContrato"));
				ses.setAttribute("NomContrato",request.getParameter("NomContrato"));
				ses.setAttribute("NumUsuario",request.getParameter("NumUsuario"));
				ses.setAttribute("NomUsuario",request.getParameter("NomUsuario"));
				ses.setAttribute("ClaveBanco",request.getParameter("ClaveBanco"));

				ses.setAttribute("ResultPago",resultPago);
				ses.setAttribute("titulo",titulo);
				ses.setAttribute("Tabla",strTabla);
				ses.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa  th:tm hrs."));

				//System.out.println("MCL_Pagos - efectuaPago(): NVS *** Agregando tipo de firma a la sesion");
				ses.setAttribute("TipoFirma", titulo);
//				response.sendRedirect(response.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+"/jsp/MCL_EfectuaPagos.jsp"));
			try {
				EmailSender emailSender = new EmailSender();
				EmailDetails beanEmailDetails = new EmailDetails();

				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Chequera ->" + Seleccion.camposTabla[0][4], EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Cuenta ->" + Seleccion.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Referencia ->" + referencia, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Concepto ->" + descripcion, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Importe ->" + importe, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago NumContrato ->" + request.getParameter("NumContrato"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago NomContrato ->" + request.getParameter("NomContrato"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago NomUsuario ->" + request.getParameter("NomUsuario"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago ClaveBanco ->" + request.getParameter("ClaveBanco"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago ResultPago ->" + Result, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Cod Error ->" + codError, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago Chequera ->" + resultPago, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("������������ MCL_Pagos::efectuaPago titulo ->" + titulo, EIGlobal.NivelLog.DEBUG);

				if(emailSender.enviaNotificacion(codError)) {

					EIGlobal.mensajePorTrace("�������������������� Se debe enviar notificacion? " + emailSender.enviaNotificacion(codError), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� Notificacion en MCL_Pagos", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� Importe ->" + importe, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� Cuenta cargo ->" + Seleccion.camposTabla[0][0], EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� referencia ->" + referencia, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("�������������������� status ->" + codError, EIGlobal.NivelLog.DEBUG);

					beanEmailDetails.setNumeroContrato(session.getContractNumber());
					beanEmailDetails.setRazonSocial(session.getNombreContrato());
					beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(importe));
					beanEmailDetails.setNumCuentaCargo(Seleccion.camposTabla[0][0]);
					beanEmailDetails.setNumRef(referencia);
					beanEmailDetails.setEstatusActual(codError);


						emailSender.sendNotificacion(request, IEnlace.NOT_CREDITO_PREPAGO, beanEmailDetails);

				}
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				//TODO CU3081 Efectua Pago

				/**
		 		 * VSWF - FVC - I
		 		 * 17/Enero/2007
		 		 */
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		 		try {
		 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					BitaHelper bh = new BitaHelperImpl(request, session, sess);
					BitaTransacBean bt = new BitaTransacBean();

					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(mx.altec.enlace.bita.BitaConstants.ER_CREDITO_LINEA_PREPAGO_EFECTUA_OPERACIONES);
					bt.setContrato((contrato == null)?" ":contrato);
					String importeAux = importe;
					if(importeAux == null) importeAux = "0";
					importeAux = importeAux.replaceAll(". ", "");
					importeAux = importeAux.replaceAll(",", "");
					bt.setImporte(Double.parseDouble((importeAux == null)?"0":importeAux));
					bt.setBancoDest((request.getParameter("ClaveBanco") == null)?" ":request.getParameter("ClaveBanco"));
					bt.setCctaOrig((Seleccion.camposTabla[0][4] == null)?" ":Seleccion.camposTabla[0][4]);
					bt.setCctaDest((Seleccion.camposTabla[0][0] == null)?" ":Seleccion.camposTabla[0][0]);
					bt.setServTransTux("RG03");
					/*BMB-I*/
					if(Result!=null){
		    			if(Result.substring(0,2).equals("OK")){
		    				bt.setIdErr("RG030000");
		    			}else if(Result.length()>8){
			    			bt.setIdErr(Result.substring(0,8));
			    		}else{
			    			bt.setIdErr(Result.trim());
			    		}
		    		}
					/*BMB-F*/
					BitaHandler.getInstance().insertBitaTransac(bt);
					log("Termina bitacora");
				} catch (NumberFormatException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (SQLException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				}
		 		/**
		 		 * VSWF - FVC - F
		 		 */

				evalTemplate("/jsp/MCL_EfectuaPagos.jsp", request, response );//IM314336 SLF
			  }
			 else
			  {
				if(Result.length()>16)
				  despliegaError(Result.substring(16,Result.length()), request, response );
				else
				  despliegaError("No se pudo completar el servicio.", request, response );
			  }
		   } else
		   despliegaPaginaError( "Su transacci&oacute;n no puede ser atendida en  este	momento.",
				"Prepagos de Cr&eacute;dito",
				"Cr&eacute;dito &gt; Cr&eacute;dito en L&iacute;nea &gt; Prepago ","s26310h", request, response );
		}
    }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
	public String calculaAdeudoTotal(String Result)
	 {
       float adeudoTotal=0;

       EI_Tipo ResultTmp=new EI_Tipo(this);

       Result += " @";
	   ResultTmp.iniciaObjeto(Result);

       float capitalExigible	  = new Float(ResultTmp.camposTabla[0][13]).floatValue();
       float interesExigible	  = new Float(ResultTmp.camposTabla[0][14]).floatValue();
       float ivaInteresExigible   = new Float(ResultTmp.camposTabla[0][15]).floatValue();

       float capitalNoExigible	  = new Float(ResultTmp.camposTabla[0][16]).floatValue();
       float interesNoExigible	  = new Float(ResultTmp.camposTabla[0][17]).floatValue();
       float ivaInteresNoExigible = new Float(ResultTmp.camposTabla[0][18]).floatValue();

       float interesMoratorio	  = new Float(ResultTmp.camposTabla[0][19]).floatValue();
       float ivaInteresMoratorio  = new Float(ResultTmp.camposTabla[0][20]).floatValue();
       float seguro		  = new Float(ResultTmp.camposTabla[0][21]).floatValue();

       float gastosJuridicos	  = new Float(ResultTmp.camposTabla[0][24]).floatValue();
       float penaConvencional	  = new Float(ResultTmp.camposTabla[0][25]).floatValue();
       float ivaPenaConvencional  = new Float(ResultTmp.camposTabla[0][26]).floatValue();

       float otros		  = new Float(ResultTmp.camposTabla[0][27]).floatValue();
       float ivaOtros		  = new Float(ResultTmp.camposTabla[0][28]).floatValue();

       adeudoTotal=capitalExigible + interesExigible + ivaInteresExigible;
       adeudoTotal+=capitalNoExigible + interesNoExigible + ivaInteresNoExigible;
       adeudoTotal+=interesMoratorio + ivaInteresMoratorio + seguro + gastosJuridicos;
       adeudoTotal+=penaConvencional + ivaPenaConvencional + otros + ivaOtros;

       return Float.toString(adeudoTotal);
     }

/*************************************************************************************/
/************************************************************ capital Disponible     */
/*************************************************************************************/
    String calculaCapitalDisponible(String Result, String adeudo)
     {
       float cdisponible=0;
       float adeudoTotal=new Float(adeudo).floatValue();

       EI_Tipo ResultTmp=new EI_Tipo(this);

       Result+=" @";
       ResultTmp.iniciaObjeto(Result);

       float capitalExigible   = new Float(ResultTmp.camposTabla[0][13]).floatValue();
       float capitalNoExigible = new Float(ResultTmp.camposTabla[0][16]).floatValue();
       float capitalAutorizado = new Float(ResultTmp.camposTabla[0][40]).floatValue();

       if(adeudoTotal>capitalAutorizado || capitalExigible>0)
	 cdisponible=0;
       else
	 cdisponible=capitalAutorizado-capitalNoExigible;

       return Float.toString(cdisponible);
     }

/*************************************************************************************/
/**************************************************************** Despliega Error    */
/*************************************************************************************/
	private void despliegaError(String Error, HttpServletRequest request,
			HttpServletResponse response ) throws ServletException, IOException
	  {

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = request.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

       request.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
       request.setAttribute("Error",Error);

       request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	   request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	   request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	   request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
	   request.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());

       evalTemplate( "/jsp/EI_TmpError.jsp", request, response );
	}

/*************************************************************************************/
/*************************************************** formatea Reultado para Linea    */
/*************************************************************************************/
    private String formateaResultLinea(String usuario)
	 {
	   String arcLinea="";
       String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 EIGlobal.mensajePorTrace("MCL_Pagos - formateaResultLinea(): No se pudo realizar la copia remota.", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
       if(ArcEnt.abreArchivoLectura()) {
	  boolean noError=true;
	  arcLinea=ArcEnt.leeLinea();
	  if(arcLinea.substring(0,2).equals("OK"))
	   {
	     arcLinea=ArcEnt.leeLinea();
	     if(arcLinea.equals("ERROR"))
	       return "OPENFAIL";
	   }
		  ArcEnt.cierraArchivo();
	} else
		return "OPENFAIL";
       return arcLinea;


    }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
   boolean tieneFacultad(EI_Tipo FacArchivo,String credito,String facultad,HttpServletRequest request)
	{


      boolean FacAmb=false;

      boolean FacultadAsociadaACuenta=false;
      try{
	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");


   if (facultad!=null && session!=null)
   {
	   facultad=facultad.trim();
      if(facultad.equals(session.FAC_TECRESALDOSC_CRED))
		  FacAmb=session.getFacultad(session.FAC_TECRESALDOSC_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_TER_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_POSICI_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_TER_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_SALDO_TER_CRED);
      else
      if(facultad.equals(session.FAC_CONS_POSICI_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_POSICI_CRED);
      else
      if(facultad.equals(session.FAC_CONS_SALDO_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_SALDO_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVS_TER_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_MOVS_TER_CRED);
      else
      if(facultad.equals(session.FAC_TECREMOVTOSC_CRED))
		  FacAmb=session.getFacultad(session.FAC_TECREMOVTOSC_CRED);
      else
      if(facultad.equals(session.FAC_TECREDISPOSM_CRED))
		  FacAmb=session.getFacultad(session.FAC_TECREDISPOSM_CRED);
      else
      if(facultad.equals(session.FAC_TECREPOSICIOC_CRED))
		  FacAmb=session.getFacultad(session.FAC_TECREPOSICIOC_CRED);
      else
      if(facultad.equals(session.FAC_TECREPPAGOSM_CRED))
		  FacAmb=session.getFacultad(session.FAC_TECREPPAGOSM_CRED);
      else
      if(facultad.equals(session.FAC_CONS_MOVTOS_CRED))
		  FacAmb=session.getFacultad(session.FAC_CONS_MOVTOS_CRED);
      else
		EIGlobal.mensajePorTrace("MCL_Pagos - tieneFacultad(): #################################\nNo reviso ninguna facultad\n##############################", EIGlobal.NivelLog.DEBUG);

	  EIGlobal.mensajePorTrace("MCL_Pagos - tieneFacultad(): Facultad "+ facultad +" en ambiente = "+FacAmb, EIGlobal.NivelLog.INFO);

      if(FacAmb)
	   {
		 //Buscala en la matriz de facultades asociadas a cuentas con signo -
		 if(facultadAsociada(FacArchivo,credito,facultad,"-"))
		   FacultadAsociadaACuenta=false;
		 else
		   FacultadAsociadaACuenta=true;
	   }
	  else
	   {
		 //Buscala en la matriz de facultades asociadas a cuentas con signo +
		 if(facultadAsociada(FacArchivo,credito,facultad,"+"))
		   FacultadAsociadaACuenta=true;
		 else
		   FacultadAsociadaACuenta=false;
       }
      EIGlobal.mensajePorTrace("MCL_Pagos - tieneFacultad(): Facultad de regreso="+FacultadAsociadaACuenta, EIGlobal.NivelLog.INFO);
	 }
   else
	      EIGlobal.mensajePorTrace("MCL_Pagos - PARAMETROS VIENEN NULO facultad-->"+facultad+"  session-->"+session, EIGlobal.NivelLog.INFO);
      }

	 catch(Exception e){
		 EIGlobal.mensajePorTrace("MCL_Pagos - tieneFacultad(): Facultad de regreso-->"+e.getMessage(), EIGlobal.NivelLog.ERROR);

	 }
      return FacultadAsociadaACuenta;
     }

    boolean facultadAsociada(EI_Tipo FacArchivo,String credito,String facultad,String signo)
     {
      /*
       0 7
       1 Clave Fac
       2 Cuenta
       3 signo
      */

       if(FacArchivo.totalRegistros>=1)
	for(int i=0;i<FacArchivo.totalRegistros;i++)
	 {
	   if(credito.trim().equals(FacArchivo.camposTabla[i][2].trim()))
	    if(FacArchivo.camposTabla[i][1].trim().equals(facultad))
	     if(FacArchivo.camposTabla[i][3].trim().equals(signo))
	       return true;
	 }
       EIGlobal.mensajePorTrace("MCL_Pagos - facultadAsociada(): Facultad asociada regresa false", EIGlobal.NivelLog.DEBUG);
       return false;
     }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
   public boolean SesionValidaCerrar( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, java.io.IOException
	{
		String remotehost=request.getRemoteHost();
		String servername = request.getServerName();
		String port=String.valueOf(request.getServerPort());
		String serversofw=getServletContext().getServerInfo();
		String ip=request.getRemoteAddr();
		String protocolo=request.getProtocol();

		boolean resultado = false;

		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		protocolo=protocolo.substring(0,protocolo.indexOf("/"));

		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): remotehost "+remotehost, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): port "+port, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): servername "+servername, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): serversofw "+serversofw, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): ip "+ip, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MCL_Pagos - SesionValidaCerrar(): protocolo "+protocolo, EIGlobal.NivelLog.INFO);

		if(sess==null)
		 {
		request.setAttribute("web_application",Global.WEB_APPLICATION);
			request.setAttribute("host",protocolo+"://"+remotehost);
			evalTemplate("/jsp/sessionfuera.jsp", request, response );

		 }
		else
		 {
			if( session.getUserID8() == null || session.getUserID8().equals("") )
			 {
		    request.setAttribute("web_application",Global.WEB_APPLICATION);
				request.setAttribute("host",protocolo+"://"+remotehost);
			    evalTemplate("/jsp/sessionfuera.jsp", request, response );
			 }
			else
			 resultado = true;
		 }
		return resultado;
	}



//IM314336 SLF JULIO 2004 CODIGO GENERADO PARA DESPLEGAR COMPROBANTE

public String Comprobante(HttpServletRequest request) throws ServletException
{
 String salida="";
salida= salida + "<html>"+"\n";
salida= salida + "<head>"+"\n";
salida= salida + "<title>Prepagos de Cr&eacute;dito</TITLE>"+"\n";
salida= salida + "<script language=\"javascript\" type=\"text/javascript\" src=\"/EnlaceMig/scrImpresion.js\"></script> "+"\n";
salida= salida + "<link rel=\"stylesheet\" href=\"/EnlaceMig/consultas.css\" type=\"text/css\"> "+"\n";
salida= salida + "</head> "+"\n";

salida= salida + "<body background=\"/gifs/EnlaceMig/gfo25010.gif\" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth=\"0\" marginheight=\"0\"> "+"\n";

salida= salida + "<form name=\"form1\" method=\"post\" action=\"\"> "+"\n";
salida= salida + "  <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr>  "+"\n";
//NVS Modificacion IM314336 glo25030.gif
//System.out.println("*** NVS --> IMAGEN GLO25040.gif");
//salida= salida + "      <td valign=\"top\" class=\"titpag\" height=\"40\"><img src=\"/gifs/EnlaceMig/glo25040.gif\" width=\"152\" height=\"30\" alt=\"Enlace\"></td> "+"\n";
salida= salida + "      <td valign=\"top\" class=\"titpag\" ><img src=\"/gifs/EnlaceMig/glo25040.gif\" alt=\"Enlace\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  <table width=\"430\" border=\"0\" cellspacing=\"6\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr>  "+"\n";

if(request.getAttribute("titulo").equals("REALIZADA"))
 salida= salida + "      <td width=\"247\" valign=\"top\" class=\"titpag\">Comprobante de operaci&oacute;n</td> "+"\n";
else
 salida= salida + "      <td width=\"247\" valign=\"top\" class=\"titpag\">Operaci&oacute;n Mancomunada</td> "+"\n";

if(request.getAttribute("ClaveBanco")!=null) {
if(request.getAttribute("ClaveBanco").equals("014")) {
salida= salida + "    <td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"right\" width=\"35\">"+"\n";
//NVS Modificacion IM314336 glo25040.gif
//System.out.println("*** NVS --> IMAGEN GLO25030.gif");
//salida= salida + "     <IMG SRC=\"/gifs/EnlaceMig/glo25030.gif\" WIDTH=\"24\" HEIGHT=\"24\" BORDER=0 ALT=\"\">"+"\n";
salida= salida + "     <IMG SRC=\"/gifs/EnlaceMig/glo25030.gif\" BORDER=0 ALT=\"\">"+"\n";

} else
   if(request.getAttribute("ClaveBanco").equals("003")) {
salida= salida + "    <td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"right\" width=\"35\">"+"\n";
salida= salida + "     <IMG SRC=\"/gifs/EnlaceMig/glo25040s.gif\" WIDTH=\"24\" HEIGHT=\"24\" BORDER=0 ALT=\"\">"+"\n";
   }
   }
salida= salida + "    </tr> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td valign=\"top\" class=\"titenccom\">Prepago de cr&eacute;dito</td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  <table width=\"400\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr> "+"\n";
salida= salida + "      <td align=\"center\">  "+"\n";
salida= salida + "        <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "          <tr>  "+"\n";
salida= salida + "            <td colspan=\"3\"> </td> "+"\n";
salida= salida + "          </tr> "+"\n";
salida= salida + "          <tr>  "+"\n";
salida= salida + "            <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25030.gif\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"21\" height=\"2\" alt=\"..\" name=\"..\"></td> "+"\n";
salida= salida + "            <td width=\"428\" height=\"150\" valign=\"top\" align=\"center\">  "+"\n";
salida= salida + "              <table width=\"380\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" background=\"/gifs/EnlaceMig/gau25010.gif\"> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\" width=\"107\">Contrato:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" width=\"255\" nowrap> "+"\n";


String numContrato = "";

					   if(request.getAttribute("NumContrato")!= null)
					     numContrato = (String)request.getAttribute("NumContrato");
					   salida = salida + numContrato;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Usuario:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";

					String numUsuario = "";

					  if (request.getAttribute("NumUsuario")!= null)
					    numUsuario = (String)request.getAttribute("NumUsuario");
				       salida = salida + numUsuario;

//salida= salida + "					<script>"+"\n";
//salida= salida + "					  document.write(cliente_7To8(\"" + numUsuario.trim() + "\"));"+"\n";
//salida= salida + "	                </script>"+"\n";
salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Fecha y Hora:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";
				      String fechaHora = "";
					  if (request.getAttribute("FechaHora")!= null)
					    fechaHora = (String)request.getAttribute("FechaHora");
				    salida = salida + fechaHora;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Importe:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";

				   String importe = "";

				     if (request.getAttribute("Importe")!= null)
					   importe = (String)request.getAttribute("Importe");
				   salida = salida + importe;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Concepto:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";
				     String concepto = "";
				     if (request.getAttribute("Concepto")!= null)
					   concepto = (String)request.getAttribute("Concepto");
				   salida = salida + concepto;

salida= salida + "                  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Referencia:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";
				     String referencia = "";

				     if (request.getAttribute("Referencia")!= null)
					   referencia = (String)request.getAttribute("Referencia");
				   salida = salida + referencia;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">Cuenta cargo:</td> "+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";

				   String chequera = "";
				     if (request.getAttribute("Chequera")!= null)
					   chequera = (String)request.getAttribute("Chequera");
				   salida = salida + chequera;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "                <tr>  "+"\n";
salida= salida + "                  <td class=\"tittabcom\" align=\"right\">N&uacute;mero de cr&eacute;dito:</td>"+"\n";
salida= salida + "                  <td class=\"textabcom\" align=\"left\" nowrap> "+"\n";

				   String cuenta = "";

				     if (request.getAttribute("Cuenta")!= null)
					    cuenta = (String)request.getAttribute("Cuenta");
				   salida = salida + cuenta;

salida= salida + "				  </td> "+"\n";
salida= salida + "                </tr> "+"\n";
salida= salida + "              </table> "+"\n";
salida= salida + "            </td> "+"\n";
salida= salida + "            <td width=\"21\" background=\"/gifs/EnlaceMig/gfo25040.gif\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"21\" height=\"2\" alt=\"..\" name=\"..\"></td> "+"\n";
salida= salida + "          </tr> "+"\n";
salida= salida + "        </table> "+"\n";
salida= salida + "      </td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"8\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td class=\"tittabdat\" colspan=\"3\"><img src=\"/gifs/EnlaceMig/gau25010.gif\" width=\"5\" height=\"2\" alt=\"..\" name=\".\"></td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  <table width=\"430\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"> "+"\n";
salida= salida + "    <tr>  "+"\n";
salida= salida + "      <td align=\"center\"><br> "+"\n";
salida= salida + "        <table width=\"154\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"22\"> "+"\n";
salida= salida + "          <tr>  "+"\n";
salida= salida + "            <td align=\"right\" width=\"83\" valign=\"top\">  "+"\n";
salida= salida + "			  <a href=\"javascript:scrImpresion();\" border = 0><img src = \"/gifs/EnlaceMig/gbo25240.gif\" border=\"0\" alt=\"Imprimir\"></a>"+"\n";
salida= salida + "            </td>"+"\n";
salida= salida + "            <td align=\"left\" width=\"71\" valign=\"top\">  "+"\n";
salida= salida + "              <a href=\"javascript:window.close();\" border = 0><img src = \"/gifs/EnlaceMig/gbo25200.gif\" border =\"0\" alt=\"Cerrar\"></a>"+"\n";
salida= salida + "            </td> "+"\n";
salida= salida + "          </tr> "+"\n";
salida= salida + "        </table>"+"\n";
salida= salida + "	  </td> "+"\n";
salida= salida + "    </tr> "+"\n";
salida= salida + "  </table> "+"\n";
salida= salida + "  </form> "+"\n";

salida= salida + "</body> "+"\n";
salida= salida + "</html>"+"\n";

//EIGlobal.mensajePorTrace("MCL_Pagos - Comprobante(): salida->  "+salida, EIGlobal.NivelLog.INFO);

return salida;
}
}