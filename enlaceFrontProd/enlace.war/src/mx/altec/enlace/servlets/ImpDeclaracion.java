package mx.altec.enlace.servlets;



import java.io.*;

import java.util.*;

import java.lang.reflect.*;

import javax.servlet.http.*;

import javax.servlet.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;




public class ImpDeclaracion extends BaseServlet{



    //String contrato          = "";

    //String usuario           = "";

    //String clavePerfil       = "";

    //short  sucOpera          = (short)787;

	//String mensaje           = "";

    GregorianCalendar calHoy = new GregorianCalendar();

    //EIGlobal insEIGlobal = new EIGlobal(this);



    public void doGet( HttpServletRequest req, HttpServletResponse res )

			throws ServletException, IOException{

		defaultAction( req, res );

	}



	public void doPost( HttpServletRequest req, HttpServletResponse res )

			throws ServletException, IOException{

		defaultAction( req, res );

	}



	public void defaultAction( HttpServletRequest req, HttpServletResponse res)

			throws ServletException, IOException {

			BaseResource session = (BaseResource) req.getSession().getAttribute("session");

       	   String contrato          = "";

		   String usuario           = "";

		   String clavePerfil       = "";

		   short  sucOpera          = (short)787;

		   String mensaje           = "";

       	if(SesionValida( req, res )){

			String ventana = ( String ) req.getParameter("ventana");

			System.out.println("ventana : " + ventana);

			contrato          = session.getContractNumber();

			sucOpera          = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

			usuario           = session.getUserID();

			clavePerfil       = session.getUserProfile();





			if (ventana.equals("0"))

				pedirArchivo( req, res );

			else

				if (ventana.equals("1"))

					importarArchivo(clavePerfil,req,res, contrato, usuario);

				else

					if (ventana.equals("2")){

						if(EIGlobal.esHabil(calHoy))

							enviarArchivo( req, res, contrato, usuario, clavePerfil );

						else{

							mensaje = "La declaraci&oacute;n no puede ser enviada en un dia inhabil.";

							despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25630h",req, res);

						}

					}

		}

	}







    public void pedirArchivo( HttpServletRequest req, HttpServletResponse res)

    		throws ServletException, IOException{

					BaseResource session = (BaseResource) req.getSession().getAttribute("session");

		String fecha_hoy = "";

		fecha_hoy = ObtenFecha();

		req.setAttribute("newMenu",            session.getFuncionesDeMenu());

		req.setAttribute("MenuPrincipal",      session.getStrMenu());

		req.setAttribute("Encabezado",         CreaEncabezado("Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25630h",req));

		evalTemplate("/jsp/ImpDeclaracion.jsp", req, res);

	}





  public int importarArchivo(String clavePerfil, HttpServletRequest req, HttpServletResponse res, String contrato, String usuario)

  		throws ServletException, IOException{



	  String nombreOriginal    = "";

	  String nombreArchivo     = "";

	  String nombreCertificado = "";

	  String nombreBasura      = "";

  	  String nombreSalida      = "";

      String mensaje           = "";

      int    salida            = 0;



      boolean errorGuardar = false;

	  try{



           nombreOriginal = ( String ) req.getParameter("nombreArchivoDe");

		   byte byteArchivo[];



		   //byteArchivo = ( byte[] ) req.getParameter("archivoDe");

		   //byteArchivo = getFileInBytes( req );

		   boolean getFileOK = getBinaryFile(req, Global.DIRECTORIO_LOCAL);

           //byteArchivo     = valIn.getValBLOB("archivoDe");

           //int sizeArchivo = byteArchivo.length;



		   if(getFileOK)

			{

	           nombreOriginal.trim();

		       nombreArchivo     = nombreOriginal.substring(nombreOriginal.lastIndexOf('\\')+1, nombreOriginal.length());

			   nombreArchivo     = Global.DIRECTORIO_LOCAL + "/" + nombreArchivo;

			   //nombreCertificado = IEnlace.APP_PATH + "/sat.cer";

			   nombreCertificado = Global.APP_PATH + "/sat.cer";

   			   nombreBasura      = Global.DIRECTORIO_LOCAL + "/" + usuario + ".dectrash";

			   nombreSalida      = Global.DIRECTORIO_LOCAL + "/" + usuario + ".decout";



				/*File drvDe = new File(nombreArchivo);

				if(drvDe.exists()){

					if(drvDe.delete()){

						EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvDe: Archivo Eliminado &", EIGlobal.NivelLog.INFO);

					}else{

						EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvDe: No se puede Borrar el Archivo &", EIGlobal.NivelLog.INFO);

					}

				}



				RandomAccessFile fileDe = new RandomAccessFile(drvDe, "rw");

				fileDe.write(byteArchivo, 0,  sizeArchivo);

				fileDe.close();


				*/

				EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & nombreArchivo :" + nombreArchivo + " &", EIGlobal.NivelLog.INFO);

				mensaje = "Se guardo OK el archivo " + nombreArchivo;

			}

		   else

			{

				EIGlobal.mensajePorTrace( "***ImpDeclaracion.class importarArchivo() getFileOK >> "+ getFileOK + " <<", EIGlobal.NivelLog.INFO);

				errorGuardar = true;

				mensaje = "Error al guardar el archivo " + nombreOriginal;

				despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25630h",req,res);

			}



        }catch(Exception e){

			 EIGlobal.mensajePorTrace( "***ImpDeclaracion.class Excepcion %importarArchivo() >> "+ e.getMessage() + " <<", EIGlobal.NivelLog.INFO);

			 errorGuardar = true;

             mensaje = "Error al guardar el archivo " + nombreOriginal;

			 despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h",req,res);

          }



	  if(!errorGuardar){

	        boolean errorValidar = false;

			int tmp       =  0;

			int exitValue = -1;



			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & Thread.activeCount() :" + Thread.activeCount() + " &", EIGlobal.NivelLog.INFO);



			Runtime r = Runtime.getRuntime();



			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & ALERTA Memoria Disponible " + r.totalMemory() +" bytes &", EIGlobal.NivelLog.INFO);



			Process p =null;



			try{

    			//String comando = IEnlace.APP_PATH + "/sat.exe " + nombreArchivo + " " + nombreCertificado + " " + nombreBasura + " " + nombreSalida;

			    String comando = Global.APP_PATH + "/sat.exe " + nombreArchivo + " " + nombreCertificado + " " + nombreBasura + " " + nombreSalida;

				EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & comando:" + comando + " &", EIGlobal.NivelLog.INFO);

				p=r.exec(comando);

				tmp       = p.waitFor();

				exitValue = p.exitValue();

				EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & salida de sat.exe :" + exitValue + " &", EIGlobal.NivelLog.INFO);

				/**
				 * <VC autor="GGB" fecha="18/09/2007"
				 * 		descripcion="Cerrar explícitamente los Stream que tiene el objeto Process">
				 */
				try {
					p.getErrorStream().close();
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(
							"ImpDeclaracion.importarArchivo -> Mensaje: "
									+ e.getMessage(), EIGlobal.NivelLog.INFO);
				}
				try {
					p.getInputStream().close();
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(
							"ImpDeclaracion.importarArchivo -> Mensaje: "
									+ e.getMessage(), EIGlobal.NivelLog.INFO);
				}
				try {
					p.getOutputStream().close();
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(
							"ImpDeclaracion.importarArchivo -> Mensaje: "
									+ e.getMessage(), EIGlobal.NivelLog.INFO);
				}
				/** </VC> */

				p.destroy();

				p=null;

				EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & sat.exe destruido &", EIGlobal.NivelLog.INFO);

			}catch(Exception e){

				EIGlobal.mensajePorTrace("***ImpDeclaracion.class Excepcion %importarArchivo():Error al ejecutar sat.exe:>> "+ e.getMessage() + "|" + " <<", EIGlobal.NivelLog.INFO);

				e.printStackTrace();

                errorValidar=true;

                mensaje = "Error al validar el archivo " + nombreArchivo;

			    despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h", req,res);

			}



            boolean errorLeerOut = false;

			String strResult = "";

			if(!errorValidar)

			 {

				if(exitValue == 0)

				 {

					try

					 {

						//String path_archivo = IEnlace.APP_PATH + "/" + usuario+ ".dec";

						File drvSua = new File(nombreSalida);

						RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");

						strResult = fileSua.readLine();



						EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & strResult:" + strResult + " &", EIGlobal.NivelLog.INFO);

						fileSua.close();

					 }catch(Exception ioeSua )

						{

			 			    EIGlobal.mensajePorTrace( "***ImpDeclaracion.class Excepcion %importarArchivo():Error al leer el archivo sat.out:>> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);

							mensaje = "Error al validar el archivo " + nombreArchivo;

							errorLeerOut = true;

						    despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h" ,req,res);

						}

					if(!errorLeerOut)

					 {

						validarDatosDeclaracion(clavePerfil,usuario, strResult, nombreArchivo, req, res, contrato);

					 }

				 }

				else

				 {

					mensaje = "El archivo " + nombreOriginal + " no es un archivo de declaraciones v&aacute;lido.";

				    despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n",req,res);

				 }



			 } // errorValidar



			 File drvBas = new File(nombreBasura);

			 if(drvBas.exists())

			  {

			    if(drvBas.delete())

                  {

					EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvBas: Archivo Eliminado " + nombreBasura + " &", EIGlobal.NivelLog.INFO);

				  }

		        else

			      {

					EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvBas: No se puede Borrar el Archivo " + nombreBasura + " &", EIGlobal.NivelLog.INFO);

		          }

			  }

			 File drvSal = new File(nombreSalida);

			 if(drvSal.exists())

			  {

			    if(drvSal.delete())

                  {

					EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvSal: Archivo Eliminado " + nombreSalida + " &", EIGlobal.NivelLog.INFO);

				  }

		        else

			      {

					EIGlobal.mensajePorTrace("***ImpDeclaracion.class & importarArchivo() drvSal: No se puede Borrar el Archivo " + nombreSalida + " &", EIGlobal.NivelLog.INFO);

		          }

			  }



		 } // errorGuardar

     return 1;

  }



  public void validarDatosDeclaracion(String clavePerfil,String usuario,String decDatos, String nombreArchivo, HttpServletRequest req, HttpServletResponse res, String contrato)

      	throws ServletException, IOException{

	  String decRFC        = EIGlobal.BuscarToken(decDatos, '|', 1).trim();

  	  String decPeriodoIni = EIGlobal.BuscarToken(decDatos, '|', 2).trim();

  	  String decPeriodoFin = EIGlobal.BuscarToken(decDatos, '|', 3).trim();

	  String decFolio      = EIGlobal.BuscarToken(decDatos, '|', 4).trim();

	  String decImporte    = EIGlobal.BuscarToken(decDatos, '|', 5).trim();

			BaseResource session = (BaseResource) req.getSession().getAttribute("session");

	  String strPeriodoIni = decPeriodoIni.substring(0, 2) + "/" + decPeriodoIni.substring(2, 6);

	  String strPeriodoFin = decPeriodoFin.substring(0, 2) + "/" + decPeriodoFin.substring(2, 6);



	  String srvCuenta       = "";

	  String regCuenta       = "";

	  String srvRFC          = "";

	  String srvTipoPago     = "";

	  String srvFormaEntrega = "";

	  String srvRazonSocial  = "";

	  Double decDblImporte   = new Double(decImporte);

	  Double srvDblImporte   = new Double(0);



	  String mensaje = "";



      boolean errorDatos = true;

	  if(decDblImporte.doubleValue() > 0){

	      String regPago = obtenerRegistroPago(decFolio , contrato, usuario,clavePerfil);



		  if(regPago.substring(0, 5).equals("ERROR")){

			  mensaje = EIGlobal.BuscarToken(regPago, '|', 2);

		   }else if(regPago.equals("NO_EXISTE_PAGO")){

			  mensaje = "El número de folio " + decFolio + " de la declaración electrónica es diferente al registrado en su pago.";

		   }else{

		      srvCuenta       = EIGlobal.BuscarToken(regPago, ';', 2);

		      regCuenta       = obtenerRegistroCuenta(srvCuenta, contrato, usuario,clavePerfil);

		      if(regPago.substring(0, 5).equals("ERROR")){

			     mensaje = EIGlobal.BuscarToken(regCuenta, '|', 2);

		       }else if(regPago.equals("NO_EXISTE_CUENTA")){

			     mensaje = "El número de Cuenta " + srvCuenta + " registrado en su pago no es una cuenta registrada.";

		       }else{

				 srvRFC          = EIGlobal.BuscarToken(regCuenta, ';', 10).trim();

				 srvDblImporte   = new Double(EIGlobal.BuscarToken(regPago, ';', 4).trim());

				 srvTipoPago     = EIGlobal.BuscarToken(regPago, ';', 5).trim();

				 srvFormaEntrega = EIGlobal.BuscarToken(regPago, ';', 6).trim();

				 srvRazonSocial  = EIGlobal.BuscarToken(regCuenta, ';', 2).trim();



				 if( decRFC.equals(srvRFC) ){

					 if( decDblImporte.doubleValue() == srvDblImporte.doubleValue() ){

						 if( srvFormaEntrega.equals("B") )

							errorDatos = false;

						 else

							mensaje = "No se especifico banca electrónica como forma de entrega de declaración.";

					  }else

						mensaje = "El importe que contiene su pago por transferencia electrónica es diferente al indicado en el concepto 900 000 de la declaración.";

				  }else

					 mensaje = "La clave del RFC que contiene su pago es diferente al indicado en la declaración.";



			  } // else Cuenta

		   } // else Folio

	   }else

		errorDatos = false;



      //String strBoton        = "";

	  String msgTitulo       = "";

	  String tipoComprobante = "";

	  String decDatosNew     = "";



	  decDatosNew  = decDatos + "|" + nombreArchivo + "|" + srvCuenta + "|" + srvRazonSocial + "|";

      decDatosNew += srvRFC   + "|" + srvDblImporte.toString() + "|" + mensaje + "|" + srvTipoPago + "|";





	  if(errorDatos){

 		  msgTitulo       = "Declaraciones Electr&oacute;nicas";

		  //strBoton        = "Generar Comprobante de Rechazo";

		  tipoComprobante = "RECHAZO";

		  publicarDeclaraMensajes(msgTitulo, mensaje, tipoComprobante, decDatosNew,req, res);

	   }else{

		  req.setAttribute("titulo",        "Validaci&oacute;n de Archivo");

          req.setAttribute("newMenu",       session.getFuncionesDeMenu());

          req.setAttribute("MenuPrincipal", session.getStrMenu());

          req.setAttribute("Encabezado",    CreaEncabezado("Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25660h",req));

		  req.setAttribute("decDatos",      decDatosNew);

		  req.setAttribute("numFolio",      decFolio);

		  req.setAttribute("RFC",           decRFC);

		  req.setAttribute("razonSocial",   srvRazonSocial);

		  req.setAttribute("periodoInicio", strPeriodoIni);

		  req.setAttribute("periodoFin",    strPeriodoFin);

		  req.setAttribute("importe",       FormatoMoneda(decDblImporte.doubleValue()));



		  evalTemplate("/jsp/ImpDeclaraImporta.jsp",req, res);

	   }

  }



  public String obtenerRegistroPago(String strFolio, String contrato, String usuario, String clavePerfil){

      String   tramaEntrada  = "";

	  String   tramaSalida   = "";

      Calendar fechaHoy      = new GregorianCalendar();

      Calendar fechaInicio   = new GregorianCalendar();

	  String   regPago       = "NO_EXISTE_PAGO";



      fechaInicio.add( Calendar.MONTH, -1 );



	  String medioEntrega  = "2TCT";

	  String tipoOperacion = "CONI";

	  String strHoy        = EIGlobal.formatoFecha(fechaHoy   , "aaaammdd");

	  String strInicio     = EIGlobal.formatoFecha(fechaInicio, "aaaammdd");

	  String tipoConsulta  = "0";



	  tramaEntrada  = medioEntrega +"|"+ usuario +"|"+ tipoOperacion +"|";

	  tramaEntrada += contrato     +"|"+ usuario +"|"+ clavePerfil   +"|";

	  tramaEntrada += strInicio    +"|"+ strHoy  +"|"+ tipoConsulta  +"|";



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tec CONI RegPago : " + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);



	  try {

      	//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();

		ServicioTux tuxGlobal = new ServicioTux();

		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



        Hashtable hs = tuxGlobal.web_red(tramaEntrada);

      	tramaSalida = (String) hs.get("BUFFER");

      }catch( java.rmi.RemoteException re ){

      	re.printStackTrace();

      } catch (Exception e) {}



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tsc CONI RegPago :" + tramaSalida + " <<", EIGlobal.NivelLog.DEBUG);



      String path_archivo   = tramaSalida;

	  String nombre_archivo = nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & nombre_archivo_cc:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);



	  // Copia Remota

	  ArchivoRemoto archR = new ArchivoRemoto();

	  if(!archR.copia(nombre_archivo)){

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & obtenerRegistroPago: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);

		 }else{

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & obtenerRegistroPago: Copia remota OK. &", EIGlobal.NivelLog.INFO);

		 }



  	  path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;



  	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & path_archivo_cb CONI:" + path_archivo + " &", EIGlobal.NivelLog.INFO);



      //String strPago = "";

	  boolean errorFile=false;

      try{

            File drvDec     = new File(path_archivo);

            RandomAccessFile fileDec = new RandomAccessFile(drvDec, "r");

            tramaSalida     = fileDec.readLine();

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & tramaSalida Pago : " + tramaSalida + " &", EIGlobal.NivelLog.INFO);

            String retCode  = tramaSalida.substring(0, 8).trim();

			String registro = "";

            if (retCode.equals("OK")){

               while((registro = fileDec.readLine()) != null){

               	/////

               	if( registro.trim().equals(""))

					registro = fileDec.readLine();

				if( registro == null )

					break;



			      EIGlobal.mensajePorTrace( "***ImpDeclaracion.class # registro Pago : " + registro + " #", EIGlobal.NivelLog.INFO);

                  if(strFolio.equals(EIGlobal.BuscarToken(registro, ';', 1)))

					  regPago = registro;

                }

             }else

			   regPago = "ERROR|" + tramaSalida.substring(16, tramaSalida.length());



            fileDec.close();

         }catch(Exception ioeSua ){

			   EIGlobal.mensajePorTrace( "***ImpDeclaracion.class Excepcion %obtenerRegistroPago() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);

               errorFile = true;

            }





   	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & regPago: " + regPago + " &", EIGlobal.NivelLog.INFO);



      return(regPago);



   }





  public String obtenerRegistroCuenta(String strCuenta, String contrato, String usuario, String clavePerfil ){

      String   tramaEntrada  = "";

	  String   tramaSalida   = "";

      Calendar fechaHoy      = new GregorianCalendar();

      Calendar fechaInicio   = new GregorianCalendar();

	  String   regCuenta     = "NO_EXISTE_CUENTA";



      fechaInicio.add( Calendar.MONTH, -1 );



	  String medioEntrega  = "2TCT";

	  String tipoOperacion = "ACIM";

	  String tipoConsulta  = "0";



	  tramaEntrada  = medioEntrega + "|" + usuario + "|" + tipoOperacion + "|";

	  tramaEntrada += contrato     + "|" + usuario + "|" + clavePerfil   + "|" + contrato + "@";



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tec ACIM RegCuenta : " + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);



	  try{

      	//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();

		ServicioTux tuxGlobal = new ServicioTux();

		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



      	Hashtable hs = tuxGlobal.web_red(tramaEntrada);

      	tramaSalida = (String) hs.get("BUFFER");

      }catch( java.rmi.RemoteException re ){

      	re.printStackTrace();

      } catch (Exception e) {}



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tsc ACIM RegCuenta : " + tramaSalida + " <<", EIGlobal.NivelLog.DEBUG);



      String path_archivo   = tramaSalida;

      String nombre_archivo = nombre_archivo = path_archivo.substring(path_archivo.lastIndexOf('/')+1 , path_archivo.length());



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & nombre_archivo_cc:" + nombre_archivo + " &", EIGlobal.NivelLog.INFO);



	  // Copia Remota

	  ArchivoRemoto archR = new ArchivoRemoto();

	  if(!archR.copia(nombre_archivo)){

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & obtenerRegistroCuenta: No se realizo la copia remota. &", EIGlobal.NivelLog.INFO);

		 }else{

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & obtenerRegistroCuenta: Copia remota OK. &", EIGlobal.NivelLog.INFO);

		 }



  	  path_archivo = Global.DIRECTORIO_LOCAL + "/" + nombre_archivo;



  	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & path_archivo_cb ACIM:" + path_archivo +  " &", EIGlobal.NivelLog.INFO);



      //String strPago = "";

	  boolean errorFile=false;

      try{

            File drvDec = new File(path_archivo);

            RandomAccessFile fileDec = new RandomAccessFile(drvDec, "r");

            tramaSalida     = fileDec.readLine();

			EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & tramaSalida Cuentas :" + tramaSalida +  " &", EIGlobal.NivelLog.INFO);

            String retCode  = tramaSalida.substring(0, 8).trim();

			String registro = "";

            if (retCode.equals("OK")){

            	/////

               while((registro = fileDec.readLine()) != null){

               	if( registro.trim().equals(""))

					registro = fileDec.readLine();

				if( registro == null )

					break;

			      EIGlobal.mensajePorTrace( "***ImpDeclaracion.class # registro Cuenta : " + registro + " #", EIGlobal.NivelLog.INFO);

				  if(strCuenta.equals(EIGlobal.BuscarToken(registro, ';', 1)))

					  regCuenta = registro;

                }

             }else

			   regCuenta = "ERROR|" + tramaSalida.substring(16, tramaSalida.length());



            fileDec.close();

         }catch(Exception ioeSua ){

  			   EIGlobal.mensajePorTrace( "***ImpDeclaracion.class Excepcion %obtenerRegistroPago() >> "+ ioeSua.getMessage() + " <<", EIGlobal.NivelLog.INFO);

               errorFile = true;

            }





	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & regCuenta: " + regCuenta + " &", EIGlobal.NivelLog.INFO);



      return(regCuenta);



   }



  public void enviarArchivo( HttpServletRequest req, HttpServletResponse res, String contrato , String usuario, String clavePerfil )

      	throws ServletException, IOException{

	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & ENVIAR ARCHIVO &", EIGlobal.NivelLog.INFO);



	  String   tramaEntrada        = "";

	  String   tramaSalida         = "";

      Calendar fechaHoy            = new GregorianCalendar();

      Calendar fechaInicio         = new GregorianCalendar();

	  String   regPago             = "";

	  String   nombreArchivoDec    = "";

	  String   nombreArchivoRemoto = "";



      fechaInicio.add( Calendar.MONTH, -1 );



	  String decDatos = ( String ) req.getParameter("decDatos");



  	  String decRFC        = EIGlobal.BuscarToken(decDatos, '|', 1).trim();

  	  String decPeriodoIni = EIGlobal.BuscarToken(decDatos, '|', 2).trim();

  	  String decPeriodoFin = EIGlobal.BuscarToken(decDatos, '|', 3).trim();

	  String decFolio      = EIGlobal.BuscarToken(decDatos, '|', 4).trim();

	  String decImporte    = EIGlobal.BuscarToken(decDatos, '|', 5).trim();

	  String pathArchivo   = EIGlobal.BuscarToken(decDatos, '|', 6).trim();



	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & pathArchivo : " + pathArchivo + " &", EIGlobal.NivelLog.INFO);



      nombreArchivoDec     = pathArchivo.substring(pathArchivo.lastIndexOf('/')+1 , pathArchivo.length());

	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & nombreArchivoDec : " + nombreArchivoDec + " &", EIGlobal.NivelLog.INFO);



	  nombreArchivoRemoto  = Global.DIRECTORIO_REMOTO + "/" + nombreArchivoDec;

 	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & nombreArchivoRemoto : " + nombreArchivoRemoto + " &", EIGlobal.NivelLog.INFO);



	  Double decDblImporte = new Double(decImporte);



	  //2TCT | empleado | ARIM | contrato_enlace | empleado | cve_perfil | contrato @

	  //path_arch_env @ folio @ RFC @ importe @

	  //(no ceros)TCT|6980|ARIM|09000357620|6980|perfil|09000357620@OK@folio@SET870101AAA@1@

	  //(en ceros)TCT|6980|ARIM|09000357620|6980|perfil|09000357620@OK@@SET870101AAA@0@022001@022001@





	  String medioEntrega  = "2TCT";

	  String tipoOperacion = "ARIM";

	  String tipoConsulta  = "0";



	  if(decDblImporte.doubleValue() > 0){

		 tramaEntrada  = medioEntrega +"|"+ usuario             +"|"+ tipoOperacion +"|";

		 tramaEntrada += contrato     +"|"+ usuario             +"|"+ clavePerfil   +"|";

		 tramaEntrada += contrato     +"@"+ nombreArchivoRemoto +"@"+ decFolio      +"@";

		 tramaEntrada += decRFC       +"@"+ decImporte          +"@";

	   }else{

  		 tramaEntrada  = medioEntrega +"|"+ usuario             +"|"+ tipoOperacion +"|";

		 tramaEntrada += contrato     +"|"+ usuario             +"|"+ clavePerfil   +"|";

		 tramaEntrada += contrato     +"@"+ nombreArchivoRemoto +"@"+ "@" + decRFC  +"@";

		 tramaEntrada += decImporte   +"@"+ decPeriodoIni       +"@"+ decPeriodoFin +"@";

	   }



 	  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tramaEntrada ARIM:" + tramaEntrada + " <<", EIGlobal.NivelLog.DEBUG);



      String mensaje = "";

	  // Copia Remota

	  ArchivoRemoto archE = new ArchivoRemoto();

	  if(!archE.copiaLocalARemoto(nombreArchivoDec)){

		  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & enviarArhivo: No se realizo el envio del archivo &", EIGlobal.NivelLog.INFO);

		  mensaje = "No se pudo realizar el env&iacute;o del archivo. Intente m&aacute;s tarde.";

		  despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h",req,res);

	   }else{

  		  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & enviarArhivo: Envio OK. &", EIGlobal.NivelLog.INFO);

		  try{



		  	//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();

		ServicioTux tuxGlobal = new ServicioTux();

		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



		  	Hashtable hs = tuxGlobal.web_red(tramaEntrada);

		  	tramaSalida = (String) hs.get("BUFFER");

		  }catch( java.rmi.RemoteException re){

		  	re.printStackTrace();

		  } catch (Exception e) {}



		  EIGlobal.mensajePorTrace( "***ImpDeclaracion.class >> tramaSalida ARIM:" + tramaSalida + " <<", EIGlobal.NivelLog.DEBUG);



          //String strBoton        = "";

	      String msgTitulo       = "";

	      String tipoComprobante = "";



          String redRetCode  = "";

	      String referencia  = "";

          String pimpRetCode = "";



          redRetCode  = tramaSalida.substring(0, 8).trim();

	      //redRetCode  = "OK";

          if (redRetCode.equals("OK")){

		      referencia  = tramaSalida.substring(8,  16);

		      pimpRetCode = tramaSalida.substring(16, 24);

 		      //pimpRetCode = "PIMP0000";

		      if (pimpRetCode.equals("PIMP0000")){

			      mensaje   = "El archivo que contiene su declaración se recibió satisfactoriamente.";

			      mensaje  += "<BR>La referencia de env&iacute;o es : " + referencia;

			      msgTitulo = "Declaraciones Electr&oacute;nicas";

			      //strBoton  = "Generar Comprobante de Recepci&oacute;n";

			      tipoComprobante = "RECEPCION";

		          publicarDeclaraMensajes(msgTitulo, mensaje, tipoComprobante, decDatos, req, res);

		        }else if(pimpRetCode.substring(0, 4).equals("PIMP")){

                  mensaje = tramaSalida.substring(16, tramaSalida.length()); //ojo: checar tramas.

			      despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25630h",req, res);

		        }else{

                  mensaje = "Hubo error al transmitir el archivo.";

			      despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h", req, res);

			    }

	        }

	      else if(redRetCode.equals("PIMP0022")){

              mensaje = tramaSalida.substring(24, tramaSalida.length());

		      despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n", "s25630h",req, res);

	        }else{

              mensaje = tramaSalida;

		      despliegaPaginaError(mensaje , "Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n","s25630h", req, res);

	        }



		   File drvDec = new File(pathArchivo);

		   if(drvDec.exists()){

			   if(drvDec.delete()){

					EIGlobal.mensajePorTrace("***ImpDeclaracion.class & enviarArhivo() drvDec: Archivo Eliminado " + pathArchivo + " &", EIGlobal.NivelLog.INFO);

				 }else{

				    EIGlobal.mensajePorTrace("***ImpDeclaracion.class & enviarArhivo() drvDec: No se puede Borrar el Archivo " + pathArchivo + " &", EIGlobal.NivelLog.INFO);

		         }

			 }



	   }







   }



	public void publicarDeclaraMensajes(String msgTitulo, String mensaje, String tipoComprobante, String decDatos, HttpServletRequest req, HttpServletResponse res)

			throws ServletException, IOException{
			BaseResource session = (BaseResource) req.getSession().getAttribute("session");
		EIGlobal.mensajePorTrace( "***ImpDeclaracion.class & publicarDeclaraMensajes &", EIGlobal.NivelLog.INFO);

		String fecha_hoy = EIGlobal.formatoFecha(calHoy, "dt, dd de mt de aaaa");

		req.setAttribute("mensaje_salida",  "<B>" + mensaje + "</B>");

		req.setAttribute("newMenu",         session.getFuncionesDeMenu());

		req.setAttribute("MenuPrincipal",   session.getStrMenu());

		req.setAttribute("Encabezado",      CreaEncabezado("Env&iacute;o de Declaraci&oacute;n de Impuestos","Servicios &gt; Impuestos Federales&gt; Env&iacute;o de Declaraci&oacute;n",req));

		req.setAttribute("tipoComprobante", tipoComprobante);

		req.setAttribute("decDatos",        decDatos);

		evalTemplate("/jsp/ImpDeclaraMensajes.jsp", req, res);

	}

}