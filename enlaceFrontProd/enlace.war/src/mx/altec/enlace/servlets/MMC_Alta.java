/**
*   Isban Mexico
*   Clase: MMC_Alta.java
*   Descripcion: Servlet que maneja todo el flujo de Alta de Cuentas.
*
*   Control de Cambios:
*   1.1 22/06/2016  FSW. Everis-Implementacion permite la gestion de transferencia en dolares(Spid).
*/
package mx.altec.enlace.servlets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.MMC_SpidBO;
import mx.altec.enlace.bo.TrxGP93VO;
import mx.altec.enlace.bo.TrxGPF2BO;
import mx.altec.enlace.bo.TrxGPF2VO;
import mx.altec.enlace.bo.TrxOD13BO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.MAC_Registro;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.CatDivisaPais;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailContainer;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.EnlaceLynxConstants;
import mx.altec.enlace.utilerias.EnlaceLynxUtils;
import mx.altec.enlace.utilerias.EnlaceMonitorPlusConstants;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.VinculacionMonitorPlusUtils;
//VSWF
/**
 * Servlet que maneja todo el flujo de Alta de Cuentas 
 * */
@SuppressWarnings({ "unchecked", "rawtypes", "serial", "unused", "deprecation", "static-access"})
public class MMC_Alta extends BaseServlet
 {
	/**
	 * Representaci�n de la cadena <strong>Modulo</strong> proveniente de los servlets que
	 * indica la operaci�n que se realizar� en el servlet
	 * */
	private static final String MODULO_PARAM 			= "Modulo";

	/**
	 * Representaci�n de la cadena <strong>session</strong> que es un valor de sesi�n.
	 * */
	private static final String SESSION_ATTRIBUTE 		= "session";

	/**
	 * VALIDA_PARAM
	 */
	private static final String VALIDA_PARAM            = "valida";

	/**
	 * FACALTACTAS
	 */
	private static final String FACALTACTAS             ="facultadesAltaCtas";

	/**
	 * TITULO_PARAM
	 */
	private static final String TITULO_PARAM            ="Administraci&oacute;n y control &gt; Cuentas &gt; Alta";

	/**
	 * ALTACTAS_PARAM
	 */
	private static final String ALTACTAS_PARAM          ="Alta de cuentas";

	/**
	 * CLAVEAYUDA_PARAM
	 */
	private static final String CLAVEAYUDA_PARAM        ="s55205h";

	/**
	 * STRTRAMAMB
	 */
	private static final String STRTRAMAMB              ="strTramaMB";

	/**
	 * STRTRAMAMBBACKUP
	 */
	private static final String STRTRAMAMBBACKUP        ="strTramaMBBackup";

	/**
	 * STRTRAMAMBBACKUP
	 */
	private static final String STRTRAMABN              ="strTramaBN";

	/**
	 * STRTRAMABNBACKUP
	 */
	private static final String STRTRAMABNBACKUP        ="strTramaBNBackup";

	/**
	 * STRTRAMABI
	 */
	private static final String STRTRAMABI              ="strTramaBI";

	/**
	 * STRTRAMABIBACKUP
	 */
	private static final String STRTRAMABIBACKUP        ="strTramaBIBackup";

	/**
	 * CUENTASSEL
	 */
	private static final String CUENTASSEL              ="cuentasSel";

	/**
	 * CUENTASSELBACKUP
	 */
	private static final String CUENTASSELBACKUP        ="cuentasSelBackup";

	/**
	 * TIPOALTA
	 */
	private static final String TIPOALTA                ="TipoAlta";

	/**
	 * FILENAMEUPLOAD
	 */
	private static final String FILENAMEUPLOAD          ="fileNameUpload";

	/**
	 * S�mbolo separador de tramas
	 * */
	private static final String PIPE = "|";
	private static final String CLAVE_TIPO_DATO = "tipoDato";

	/**
	 * Representacion de cadena vacia
	 */
	private static final String CADENA_VACIA = "";

    /** Clave de banco. */
    private static final String BANCO_BANME = "BANME";

    /** Indicador de que la bitacora esta activa. */
    private static final String BITA_ACTIVA = "ON";

    /** Caracter de salto de linea. */
    private static final char CHAR_ENTER = '\n';

    /** Caracter de signo menos reutilizable. */
    private static final char CHAR_MENOS = '-';

    /** Caracter de signo mas reutilizable. */
	private static final char CHAR_MAS = '+';
	/**CONSTANTE NOMBRE_ARCHIVO_IMP*/
	private final static String NOMBRE_ARCHIVO_IMP = "nombreArchivoImp";
	/**CONSTANTE TOTAL_REGISTROS*/
	private final static String TOTAL_REGISTROS = "totalRegistros";
	/**CONSTANTE NEW_MENU*/
	private final static String NEW_MENU = "newMenu";
	/**CONSTANTE MENU_PRINCIPAL*/
	private final static String MENU_PRINCIPAL = "MenuPrincipal";
	/**CONSTANTE ENCABEZADO*/
	private final static String ENCABEZADO = "Encabezado";
	/**CONSTANTE NUM_CONTRATO*/
	private final static String NUM_CONTRATO = "NumContrato";
	/**CONSTANTE NOM_CONTRATO*/
	private final static String NOM_CONTRATO = "NomContrato";
	/**CONSTANTE NUM_USUARIO*/
	private final static String NUM_USUARIO = "NumUsuario";
	/**CONSTANTE NOM_USUARIO*/
	private final static String NOM_USUARIO = "NomUsuario";
	/**CONSTANTE AGREGA_CUENTAS_FIN*/
	private final static String AGREGA_CUENTAS_FIN = "MMC_Alta - agregaCuentas(): Finalizando alta.";

	/**
	 * M�todo que responde a peticiones GET
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServletAltaCuentas( req, res );
	 }

	/**
	 * M�todo que responde a peticiones POST
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {
		inicioServletAltaCuentas( req, res );
	 }

/*************************************************************************************/
/************************************************** inicioServletAltaCuentas		 */
/*************************************************************************************/
	/**
	 * Inicializador del servlet para Alta de Cuentas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos
	 * */
	public void inicioServletAltaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("MMC_Alta - execute(): Entrando a Alta de Cuentas ver Feb2017.", EIGlobal.NivelLog.INFO);
		String modulo = ( String ) getFormParameter(req, MODULO_PARAM );
		if (modulo == null) {
			modulo = "0";
			EIGlobal.mensajePorTrace("\n\n MMC_Alta - inicioServletAltaCuentas - El modulo venía en null, se le asigna \"0\"" , EIGlobal.NivelLog.INFO);
			req.getSession().removeAttribute(CLAVE_TIPO_DATO);
		}
		EIGlobal.mensajePorTrace("\n\nEl modulo es: " + modulo + "\n\n", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);

		EIGlobal.mensajePorTrace("MMC_Alta - execute(): Estableciendo session.", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("MMC_Alta - execute(): El directorio al que van los archivos es: " + Global.DIRECTORIO_REMOTO_CUENTAS , EIGlobal.NivelLog.INFO);

		HashMap facultadesAltaCtas = new HashMap ();

		if(SesionValida( req, res ))
		{

			//*****************************************Inicia validacion OTP****************
			/**
			 * Inicio PYME 2015 Unificacion Token Contrasena incorrecta
			 */
	        String valida = "";
			if( (req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull"))) &&
				(req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar"))) ){
				valida = null;
				EIGlobal.mensajePorTrace( "------------valida---->"+ valida, EIGlobal.NivelLog.DEBUG);
			} else {
				valida = getFormParameter(req, VALIDA_PARAM );
				if( ( valida == null || "".equals( valida ) ) &&
					req.getAttribute("valida2") != null ){
					valida = req.getAttribute("valida2").toString();
				}
			}
			/**
			 * Fin PYME 2015 Unificacion Token
			 */
			if ( validaPeticion( req, res,session,sess,valida))
			{
				  EIGlobal.mensajePorTrace("\n\n ENTR� A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.INFO);
			}
			//*****************************************Termina validacion OTP**************************************
			else
			{
				if (req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER) == null) {
					EIGlobal.mensajePorTrace("MMC_Alta -- DATOS BROWSER >"+req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER)
							+"< >" + getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER)
							+'<', EIGlobal.NivelLog.DEBUG);
					if (req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
						req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
								req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER));
					} else if (getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
						req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER,
								getFormParameter(req,EnlaceMonitorPlusConstants.DATOS_BROWSER));
					}
				}
				// Facultades ...
				facultadesAltaCtas.put ("AltaCtasIA", true); //No se requiere facultad para importar
				facultadesAltaCtas.put ("AltaCtasMB", ((session.getFacultad(session.FAC_ALTA_MB_P) || session.getFacultad(session.FAC_ALTA_MB_T) )?true:false) );
				facultadesAltaCtas.put ("AltaCtasBN", session.getFacultad(session.FAC_ALTA_BN) );
				facultadesAltaCtas.put ("AltaCtasBI", session.getFacultad(session.FAC_ALTA_BI) );
				//Se agrega la facultad de alta de n�meros m�viles
				facultadesAltaCtas.put ("AltaCtasNM", session.getFacultad(session.FAC_ALTA_NM));

				facultadesAltaCtas.put ("AutoCtasMB", session.getFacultad(session.FAC_AUTO_AB_MB));
				facultadesAltaCtas.put ("AutoCtasOB", session.getFacultad(session.FAC_AUTO_AB_OB));

				EIGlobal.mensajePorTrace("MMC_Alta - execute(): Leyendo facultades.", EIGlobal.NivelLog.INFO);

				if(req.getSession ().getAttribute (FACALTACTAS)!=null){
	 			  req.getSession().removeAttribute(FACALTACTAS);
				}
				req.getSession ().setAttribute (FACALTACTAS, facultadesAltaCtas);

				if (
					 !((Boolean)facultadesAltaCtas.get("AltaCtasBN")).booleanValue() &&
					 !((Boolean)facultadesAltaCtas.get("AltaCtasMB")).booleanValue() &&
					 !((Boolean)facultadesAltaCtas.get("AltaCtasBI")).booleanValue() &&
					 !((Boolean)facultadesAltaCtas.get("AltaCtasNM")).booleanValue()
				   ){
				  despliegaPaginaErrorURL("<font color=red>No</font> tiene <i>facultades</i> para dar cuentas de <font color=green>Alta</font>.",ALTACTAS_PARAM,TITULO_PARAM,CLAVEAYUDA_PARAM, req, res);
				}else
				 {
				   if( "0".equals(modulo) ){ //Default page if no "modulo" is given
				   	try {

						iniciaAlta( req, res);
					} catch (SQLException e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

					}
				   }
				   if( "1".equals(modulo) ){ //Agregar cuentas
					  agregaCuentas( req, res);
				   }
				   if( "2".equals(modulo) ) //Enviar cuentas
				    {
						///////////////////--------------challenge--------------------------
						String validaChallenge = req.getAttribute("challngeExito") != null
							? req.getAttribute("challngeExito").toString() : "";

						EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

						if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
							EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
							//ValidaOTP.guardaParametrosEnSession(req);
							guardaParametrosEnSession(req);
							/**
							 * PYME 2015 Unificacion Token para contrase�a incorrecta
							 */
							req.setAttribute("vieneDe", "/enlaceMig/MMC_Alta");
							sess.setAttribute( STRTRAMAMBBACKUP, getFormParameter(req, STRTRAMAMB ));
							sess.setAttribute( STRTRAMABNBACKUP, getFormParameter(req, STRTRAMABN ));
							sess.setAttribute( STRTRAMABIBACKUP, getFormParameter(req, STRTRAMABI ));
							sess.setAttribute( CUENTASSELBACKUP, getFormParameter(req, CUENTASSEL ));
							sess.setAttribute( TIPOALTA, getFormParameter(req, TIPOALTA ));
							sess.setAttribute( FILENAMEUPLOAD, getFormParameter(req, FILENAMEUPLOAD ));
							sess.setAttribute( "fileName", getFormParameter(req, "fileName" ));
							validacionesRSA(req, res);
							return;
						}

						//Fin validacion rsa para challenge
						///////////////////-------------------------------------------------
						EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.INFO);
						//interrumpe la transaccion para invocar la validaci�n
						if(  valida==null )
						{
							EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci�n est� parametrizada para solicitar la validaci�n con el OTP \n\n\n", EIGlobal.NivelLog.INFO);

							boolean solVal=ValidaOTP.solicitaValidacion(
										session.getContractNumber(),IEnlace.ALTA_CUENTAS);

							if( session.getValidarToken() &&
								session.getFacultad(session.FAC_VAL_OTP) &&
								session.getToken().getStatus() == 1 &&
								solVal )
							{
								//VSWF-HGG -- Bandera para guardar el token en la bit�cora
								req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
								EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit� la validaci�n. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.INFO);
								/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
								 * Solo va a presentar cuents cuando no es una carga por archivo*/

								String tipAlta = "";//getFormParameter(req,TIPOALTA) == null || "".equals(getFormParameter(req,TIPOALTA)) ? req.getSession().getAttribute(TIPOALTA).toString() : getFormParameter(req, TIPOALTA).toString();
								String tipoAltaReq=getFormParameter(req,TIPOALTA);
								EIGlobal.mensajePorTrace("----------->EtipoAltaReq-->"+tipoAltaReq ,EIGlobal.NivelLog.INFO);

								Object altaTipoSesObj=req.getSession().getAttribute(TIPOALTA);
								EIGlobal.mensajePorTrace("----------->altaTipoSesObj-->"+altaTipoSesObj ,EIGlobal.NivelLog.INFO);
								String tipAltaSes=null;

								if(altaTipoSesObj!=null){
								 tipAltaSes=altaTipoSesObj.toString();
								}


								EIGlobal.mensajePorTrace("----------->tipAltaSes-->"+tipAltaSes ,EIGlobal.NivelLog.INFO);
								if(tipAltaSes==null || tipAltaSes.trim().length()==0){
									tipAlta=tipoAltaReq;
									EIGlobal.mensajePorTrace("----------->Recuperando de Request-->"+tipAlta ,EIGlobal.NivelLog.INFO);
								}else{
									tipAlta=tipAltaSes;
									EIGlobal.mensajePorTrace("----------->Recuperando de Sesion-->"+tipAlta ,EIGlobal.NivelLog.INFO);

								}
								int tipoAlta = Integer.parseInt( tipAlta );
								EIGlobal.mensajePorTrace("------------>tipoAltaFinal-->"+tipoAlta ,EIGlobal.NivelLog.INFO);
								guardaParametrosEnSession(req);
								ValidaOTP.guardaParametrosEnSession(req);
								req.getSession().setAttribute(TIPOALTA, tipoAlta);
								if( tipoAlta == 3){
									agregaCuentas( req, res);
									if( "0".equals( req.getAttribute(MODULO_PARAM) ) ){
										EIGlobal.mensajePorTrace("MMC_Alta - execute(): Saliendo de Alta de Cuentas 1.", EIGlobal.NivelLog.INFO);
										return;
									}
								} else {
									presentaCuentas(req,res);
								}
								guardaParametrosEnSession(req);
								ValidaOTP.guardaParametrosEnSession(req);
								req.getSession().setAttribute(TIPOALTA, tipoAlta);
								req.getSession().removeAttribute("mensajeSession");

								ValidaOTP.mensajeOTP(req, "AltaCuenta");
								/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
								 * Se modica jsp al que se envia
								ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP_CONFIRM);*/
								ValidaOTP.validaOTP(req,res, "/jsp/MMC_AltaArchivo.jsp");
								/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
							}
							else{
                                    ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
                                    valida="1";
                                }
						}
						//retoma el flujo
						if( valida!=null && "1".equals(valida))
						{
							EIGlobal.mensajePorTrace("\n\n\n SE RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);
							enviaCuentas( req, res);
							req.getSession().removeAttribute(TIPOALTA);
							EIGlobal.mensajePorTrace("\n\n\n FIN de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.INFO);

						}
					}
				   /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
//				   if( modulo.equals("3") ) //Presenta cuentas
//					  presentaCuentas(req,res);
				   /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
				 }
			}//Termina else de valida OTP
		} // Termina sesion valida

		EIGlobal.mensajePorTrace("MMC_Alta - execute(): Saliendo de Alta de Cuentas.", EIGlobal.NivelLog.INFO);
	}
	/*************************************************************************************/
	/************************************************** Pantalla Principal - Modulo=0    */
	/*************************************************************************************/
	/**
	 * Operaci�n que prepara la presentaci�n en HTML de las cuentas agregadas
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos
	 * @return int Indica si la operaci�n fue correcta o no
	 * */
	public int presentaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);
		HttpSession session2 = req.getSession(false);
		Map tmpAtributos = (HashMap) session2.getAttribute("atributos");
		String cuentasSel=(String)getFormParameter(req,CUENTASSEL);
		if( ( cuentasSel == null || "".equals( cuentasSel ) ) && tmpAtributos.get(CUENTASSEL) != null ){
			cuentasSel = tmpAtributos.get(CUENTASSEL).toString();
		}
		if( ( cuentasSel == null || "".equals( cuentasSel ) ) && sess.getAttribute( CUENTASSELBACKUP ) != null ){
			cuentasSel = (String) sess.getAttribute( CUENTASSELBACKUP );
			sess.removeAttribute( CUENTASSELBACKUP );
			EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Tomo de Sesion cuentasSel"+cuentasSel, EIGlobal.NivelLog.INFO);
		}


		EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Seleccionadas "+ cuentasSel, EIGlobal.NivelLog.INFO);

		String strTramaMB=(String)getFormParameter(req,STRTRAMAMB);
		if( ( strTramaMB == null || "".equals( strTramaMB ) ) && tmpAtributos.get(STRTRAMAMB) != null ){
			strTramaMB = tmpAtributos.get(STRTRAMAMB).toString();
		}
		if( ( strTramaMB == null || "".equals( strTramaMB ) ) && sess.getAttribute( STRTRAMAMBBACKUP ) != null ){
			strTramaMB = (String) sess.getAttribute( STRTRAMAMBBACKUP );
			sess.removeAttribute( STRTRAMAMBBACKUP );
			EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Tomo de Sesion strTramaMB"+strTramaMB, EIGlobal.NivelLog.INFO);
		}
		String strTramaBN=(String)getFormParameter(req,STRTRAMABN);
		if( ( strTramaBN == null || "".equals( strTramaBN ) ) && tmpAtributos.get(STRTRAMABN) != null ){
			strTramaBN = tmpAtributos.get(STRTRAMABN).toString();
		}
		if( ( strTramaBN == null || "".equals( strTramaBN ) ) && sess.getAttribute( STRTRAMABNBACKUP ) != null ){
			strTramaBN = (String) sess.getAttribute( STRTRAMABNBACKUP );
			sess.removeAttribute( STRTRAMABNBACKUP );
			EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Tomo de Sesion strTramaBN"+strTramaBN, EIGlobal.NivelLog.INFO);
		}
		String strTramaBI=(String)getFormParameter(req,STRTRAMABI);
		if( ( strTramaBI == null || "".equals( strTramaBI ) ) && tmpAtributos.get(STRTRAMABI) != null ){
			strTramaBI = tmpAtributos.get(STRTRAMABI).toString();
		}
		if( ( strTramaBI == null || "".equals( strTramaBI ) ) && sess.getAttribute( STRTRAMABIBACKUP ) != null ){
			strTramaBI = (String) sess.getAttribute( STRTRAMABIBACKUP );
			sess.removeAttribute( STRTRAMABIBACKUP );
			EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Tomo de Sesion strTramaBI"+strTramaBI, EIGlobal.NivelLog.INFO);
		}
        String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/" + session.getUserID8()+".txt";
		//StringBuilder arcMB = new StringBuilder();
		String arcLinea="";

		EI_Tipo MB= new EI_Tipo();
		EI_Tipo BN= new EI_Tipo();
		EI_Tipo BI= new EI_Tipo();
		final int LONGITUD_CUENTA_MOVIL 	= 10;

		int reg=0;
		int MBr=0;
		int BNr=0;
		int BIr=0;
		if(strTramaMB == null){
			strTramaMB = "";
		}
		if(strTramaBN == null){
			strTramaBN = "";
		}
		if(strTramaBI == null){
			strTramaBI = "";
		}
		MB.iniciaObjeto(strTramaMB);
		BN.iniciaObjeto(strTramaBN);
		BI.iniciaObjeto(strTramaBI);
		EIGlobal.mensajePorTrace("ARS - 1. "+strTramaMB, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ARS - 2. "+strTramaBN, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ARS - 3. "+strTramaBI, EIGlobal.NivelLog.INFO);

		if(MB.totalRegistros>=1 || BN.totalRegistros>=1 || BI.totalRegistros>=1)
        {

          EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
          EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);
          if(ArcSal.creaArchivo())
           {
             arcLinea="";
             /**
              * Los valores del arreglo "c" especifican la obtenci�n de los siguientes campos a manera de posiciones dentro de
              *  MB.camposTabla[posicion] :
              * [
              * numeroCuentaBN,
              * nombreTitularBN,
              * cveBanco,
              * cvePlaza,
              * strSuc,
              * tipoCuentaBN,
              *
              * ]*/
             int c[]={0,1,4,2,8,7};			// BN Indice del elemento deseado

             /**
              * Los valores del arreglo "b" indican el tama�o de longitud m�xima aceptada en el layout de importaci�n de registros
              * por archivo.
              * */
             int b[]={20,40,5,5,5,2};		// BN Longitud maxima del campo para el elemento

			 int ci[]={0,1,2,5,3,4,8,9,10,11,12};		// BI Indice del elemento deseado
			 int bi[]={20,40,4,4,40,40,12,4,140,140,20};	// BI Longitud maxima del campo para el elemento

             EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Exportando Cuentas mismo banco.", EIGlobal.NivelLog.INFO);
             for(int i=0;i<MB.totalRegistros;i++)
              {
				if(cuentasSel.charAt(reg+i)=='1')
				  {
					StringBuilder arcMB = new StringBuilder();
					String cuenta 				= MB.camposTabla[i][0].trim();
					boolean isMovil				= LONGITUD_CUENTA_MOVIL == cuenta.length();
					String tipoCuenta 			= isMovil?"MOVIL ":"SANTAN";

					arcMB.append(tipoCuenta);
					arcMB.append(ArcSal.formateaCampo(cuenta,20));
					arcMB.append(ArcSal.formateaCampo(MB.camposTabla[i][1],40));

//					arcLinea=tipoCuenta;
//					arcLinea+=ArcSal.formateaCampo(cuenta,20);
//					arcLinea+=ArcSal.formateaCampo(MB.camposTabla[i][1],40);


					/**
					 * En caso que el n�mero de cuenta sea un n�mero movil, agrega el cuarto campo requerido en el layout de texto
					 * */
					if(isMovil){
						arcMB.append(ArcSal.formateaCampo(MB.camposTabla[i][4],5));}
					arcMB.append(CHAR_ENTER);
					arcLinea = arcMB.toString();
					EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Insertando cuenta numero " + arcLinea + (i + 1), EIGlobal.NivelLog.INFO);
					if(!ArcSal.escribeLinea(arcLinea)){
					  EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Algo salio mal escribiendo "+arcLinea, EIGlobal.NivelLog.INFO);
					}
					MBr++;
				  }
              }
			 reg+=MB.totalRegistros;

             EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Exportando Cuentas Bancos nacionales.", EIGlobal.NivelLog.INFO);
             for(int i=0;i<BN.totalRegistros;i++)
              {
				if(cuentasSel.charAt(reg+i)=='1')
				  {
//					si la longitud == 10 entonces arcLinea="MOVIL ";
//					si la longitud != 10 entonces arcLinea="EXTRNA";
					arcLinea = LONGITUD_CUENTA_MOVIL == BN.camposTabla[i][0].trim().length()?"MOVIL ":"EXTRNA";

					BN.camposTabla[i][4]=BN.camposTabla[i][4].substring(0,BN.camposTabla[i][4].indexOf(CHAR_MAS));

					StringBuffer sb = new StringBuffer(arcLinea);

					for(int j=0;j<c.length;j++){
					  sb.append(ArcSal.formateaCampo(BN.camposTabla[i][c[j]],b[j]));
					}
					sb.append(CHAR_ENTER);
					arcLinea = sb.toString();
					if(!ArcSal.escribeLinea(arcLinea)){
					  EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas()s(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
					}
					BNr++;
				  }
              }
			 reg+=BN.totalRegistros;

			 EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Exportando Cuentas Bancos internacionales.", EIGlobal.NivelLog.INFO);
             for(int i=0;i<BI.totalRegistros;i++)
              {
				if(cuentasSel.charAt(reg+i)=='1')
				  {
					arcLinea="INTNAL";
					StringBuffer sb = new StringBuffer(arcLinea);
					for(int j=0;j<ci.length;j++){
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Elmento actual: " + j, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Longitud del elemento actual: " + BI.camposTabla[i].length, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Elemento actual: " + BI.camposTabla[i][ci[j]], EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Longitud para el elemento actual: " + bi[j], EIGlobal.NivelLog.INFO);
					try {
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Campo actual de tabla: " + BI.camposTabla[i], EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Linea a escribir: " + ArcSal.formateaCampoCtaInt(BI.camposTabla[i][ci[j]],bi[j]), EIGlobal.NivelLog.INFO);
						sb.append(ArcSal.formateaCampoCtaInt(BI.camposTabla[i][ci[j]],bi[j]));
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Error: " + e.getMessage(), EIGlobal.NivelLog.INFO);
					}
					  EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): Agregando registro al archivo: " + arcLinea, EIGlobal.NivelLog.INFO);
					}
					sb.append("\n");
					arcLinea = sb.toString();
					if(!ArcSal.escribeLinea(arcLinea)){
					  EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas()s(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
					}else{
						EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas()s(): Escribio en archivo: "+arcLinea, EIGlobal.NivelLog.INFO);
					}
					BIr++;
				  }
              }
             EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas()s(): Termina recorrido de for: ", EIGlobal.NivelLog.INFO);
             ArcSal.cierraArchivo();
           }
          else
           {
             //strTabla+="<b>Exportacion no disponible<b>";
             EIGlobal.mensajePorTrace("MMC_Alta - presentaCuentas(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
           }
        }

	    req.setAttribute(NOMBRE_ARCHIVO_IMP,nombreArchivo);
		req.setAttribute(TOTAL_REGISTROS,Integer.toString(MBr+BNr+BIr));
		req.setAttribute("numeroCtasInternas",Integer.toString(MBr));
		req.setAttribute("numeroCtasNacionales",Integer.toString(BNr));
		req.setAttribute("numeroCtasInternacionales",Integer.toString(BIr));
		/** CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token se elimina mensaje */
		//req.setAttribute("msg", "Fueron seleccionados " +Integer.toString(MBr+BNr+BIr)+" registros para solicitud de Alta.");
		//req.setAttribute(MODULO_PARAM, "2");

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		req.setAttribute(ENCABEZADO, CreaEncabezado(ALTACTAS_PARAM,"Administraci&oacute;n y control &gt; Cuentas  &gt; Alta","s26082h", req));

		req.setAttribute(NUM_CONTRATO, (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute(NOM_CONTRATO, (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute(NUM_USUARIO, (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute(NOM_USUARIO, (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

		/////////////////////////////PYME 2015 Unificacion Token Contrasena incorrecta
		req.setAttribute(CUENTASSEL, cuentasSel);
		req.setAttribute(STRTRAMAMB, strTramaMB);
		req.setAttribute(STRTRAMABN, strTramaBN);
		req.setAttribute(STRTRAMABI, strTramaBI);
		///////////////////////////

        EIGlobal.mensajePorTrace(AGREGA_CUENTAS_FIN, EIGlobal.NivelLog.INFO);
        /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
         * Se comenta linea para que ya no envia a ese jsp y continue el flujo*/
		//evalTemplate("/jsp/MMC_AltaArchivo.jsp", req, res);

		return 1;
	}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	/**
	 * Operaci�n que realiza en env�o de las cuentas para su guardado en BD
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos
	 * */
	public int enviaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("MMC_Alta - enviaCuentas(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);

		EIGlobal.mensajePorTrace("Parameter nombreArchivoImp["+req.getParameter(NOMBRE_ARCHIVO_IMP)+"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Attribute nombreArchivoImp["+req.getAttribute(NOMBRE_ARCHIVO_IMP).toString()+"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Sesion nombreArchivoImp["+ req.getSession() != null && req.getSession().getAttribute(NOMBRE_ARCHIVO_IMP)!=null && !"".equals(req.getSession().getAttribute(NOMBRE_ARCHIVO_IMP)) ? req.getSession().getAttribute(NOMBRE_ARCHIVO_IMP).toString() : "null ]", EIGlobal.NivelLog.INFO);

		String nombreArchivoImp = (String) req.getAttribute(NOMBRE_ARCHIVO_IMP);
		String totalRegistros = (String) req.getAttribute(TOTAL_REGISTROS);
		String codErrorOper = "";
		String strMensaje="";
		String nombreArchivo="";

		EIGlobal.mensajePorTrace("MMC_Alta - enviaCuentas(): nombreArchivoImp: "+nombreArchivoImp, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MMC_Alta - enviaCuentas(): totalRegistros: "+totalRegistros, EIGlobal.NivelLog.INFO);

		/**GAE**/
		String datoSpecial = null;
		String regAceptados = null;
		String idLote = null;
		/**FIN GAE**/


		String cabecera="";
		String regServicio="";
		String Trama="";
		String Result="";

		String medioEntrega	="";
		String usuario ="";
		String contrato="";
		String clavePerfil="";
		String tipoOperacion="";

		String line="";

		String registroDeSolicitud="";
		String registroDeAlta="";
		String folioRegistro="";

		StringBuffer lineDuplicados=new StringBuffer("");

		boolean consulta=false;

		/*
			0 Envio Correcto
		    1 Error en Envio
			2 Envio con errores
			3 Envio correcto (Pero no se pudo identificar si hubo duplicidad)
		*/

		HashMap facultadesAltaCtas = new HashMap ();
		facultadesAltaCtas=(HashMap)req.getSession ().getAttribute (FACALTACTAS);

		ServicioTux TuxGlobal = new ServicioTux();
		//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		nombreArchivo=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf('/')+1);
		EIGlobal.mensajePorTrace( "MMC_Alta-> Nombre para la copia "+nombreArchivo, EIGlobal.NivelLog.INFO);

		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		tipoOperacion="CT01";
		medioEntrega="2EWEB";

		ArchivoRemoto archR = new ArchivoRemoto();

		EIGlobal.mensajePorTrace( "MMC_Alta-> Se continua con el envio del archivo", EIGlobal.NivelLog.INFO);

		if( !archR.copiaLocalARemotoCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) )
		 {
			EIGlobal.mensajePorTrace( "MMC_Alta-> No se realizo la copia remota a tuxedo.", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "MMC_Alta-> Se detiene el proceso pues no se envia el archivo.", EIGlobal.NivelLog.INFO);
			despliegaPaginaErrorURL("<i><font color=red>Error</font></i> de comunicaciones.<br><br> Por favor realice la importaci&oacute;n <font color=green> nuevamente </font>.<br> El archivo <font color=blue><b> "+nombreArchivo+ " </b></font> no ha sido enviado correctamente.<br><br>",ALTACTAS_PARAM,TITULO_PARAM,CLAVEAYUDA_PARAM, req, res);
			return 1;
		 }
		else
		 {
			EIGlobal.mensajePorTrace( "MMC_Alta-> Copia remota OK.", EIGlobal.NivelLog.INFO);

			cabecera=	medioEntrega		+ PIPE +
						usuario				+ PIPE +
						tipoOperacion		+ PIPE +
						contrato			+ PIPE +
						usuario				+ PIPE +
						clavePerfil			+ PIPE;

			regServicio=
						contrato			+ "@" +
						nombreArchivo		+ "@" +
				        totalRegistros		+ "@" ;

			Trama=cabecera + regServicio;

			/*####################################*/
			/**/
			EIGlobal.mensajePorTrace("MMC_Alta->  Trama entrada envio: "+Trama, EIGlobal.NivelLog.DEBUG);
			Date fechaHoraOper = new Date();
				try
				 {
					 Hashtable hs = TuxGlobal.web_red(Trama);
					 Result= (String) hs.get("BUFFER") +CADENA_VACIA;
					 ValidaOTP.guardaBitacora((List)req.getSession().getAttribute("bitacora"),tipoOperacion);
					 codErrorOper = hs.get("COD_ERROR") + CADENA_VACIA;
				 }catch( java.rmi.RemoteException re )
				  {
					 EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				  } catch (Exception e) {
					  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				  }

			EIGlobal.mensajePorTrace("MMC_Alta-> Trama salida envio: "+Result, EIGlobal.NivelLog.DEBUG);

			//TODO BIT CU5071, Realiza el Alta de la cuenta nueva
			/*VSWF HGG I*/
            // Cargar campos por defecto
            HashMap<Integer, String> campos = new HashMap<Integer, String>();
			if (Global.LYNX_INICIALIZADO) {
				EnlaceLynxConstants.cargaValoresDefaultNM(campos, req);
			}
			Map<String, String> cuentas = (Map<String, String>)
                    req.getSession().getAttribute("CuentasListaAlta");
			if (BITA_ACTIVA.equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())
                    && cuentas != null) {
				Iterator i = cuentas.keySet().iterator();
				while(i.hasNext()){
					String cuentaDest = "";
					String divisaBit = "";
					try{

						cuentaDest = i.next().toString();
						divisaBit=cuentas.get(cuentaDest);
						EIGlobal.mensajePorTrace("MMC_Alta-> cuentaDest: "+cuentaDest+" divisaBit: "+divisaBit+" tipoOper: "+divisaBit.substring(0, 1), EIGlobal.NivelLog.DEBUG);
						if("1".equals(divisaBit.substring(0, 1)) && divisaBit.indexOf("|USD")>0){
						    tipoOperacion="CT06";
						    EIGlobal.mensajePorTrace("MMC_Alta-> tipoOperacion: "+tipoOperacion, EIGlobal.NivelLog.DEBUG);
						}else{
						    tipoOperacion="CT01";
						}
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.EA_CUENTAS_ALTA_REALIZA_ALTA);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}
						if(Result != null){
							if(Result.length() >= 2 && "OK".equals(Result.substring(0,2))){
								bt.setIdErr(tipoOperacion + "0000");
							}else if(Result.length() > 8){
								bt.setIdErr(Result.substring(0,8));
							}else{
								bt.setIdErr(Result.trim());
							}
						}
						if (usuario != null) {
							bt.setUsr(usuario.trim());
						}
						if (nombreArchivo != null) {
							bt.setNombreArchivo(nombreArchivo.trim());
						}
						if (tipoOperacion != null) {
							bt.setServTransTux(tipoOperacion);
						}
						bt.setCctaDest(cuentaDest);
						if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
							&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
									.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
						}else{
							log("VSWF: no hubo bandera de token");
						}

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
				}
			}
			req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
			req.getSession().removeAttribute("CuentasListaAlta");
			/*VSWF HGG F*/

			if(Result==null || "".equals(Result))
			 {
			   despliegaPaginaErrorURL("Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.",ALTACTAS_PARAM,TITULO_PARAM,CLAVEAYUDA_PARAM, req, res);
			   return 1;
			 }
			else
			 {
				EIGlobal.mensajePorTrace("MMC_Alta-> La informacion se recibio, se evalua el contenido", EIGlobal.NivelLog.DEBUG);
				EI_Tipo trRes=new EI_Tipo();
				if( !(Result.trim().substring(0,2).equals("OK")) )
				 {
					consulta=false;
					nombreArchivo=Result.substring(Result.indexOf("/"));
					EIGlobal.mensajePorTrace("MMC_Alta-> La consulta no fue exitosa.", EIGlobal.NivelLog.INFO);
				 }
				else
				 {
					consulta=true;
					trRes.iniciaObjeto('@','|',Result+"|");
					nombreArchivo=trRes.camposTabla[0][2];
					EIGlobal.mensajePorTrace("MMC_Alta-> La consulta fue exitosa.", EIGlobal.NivelLog.INFO);
				 }

				EIGlobal.mensajePorTrace( "MMC_Alta-> Nombre de archivo obtenido:  "+nombreArchivo, EIGlobal.NivelLog.INFO);

				if( !archR.copiaCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) )
				 {
				   EIGlobal.mensajePorTrace( "MMC_Alta-> No se realizo la copia remota desde tuxedo.", EIGlobal.NivelLog.INFO);
				   nombreArchivo="-1";
				   if(consulta){
					  strMensaje="<br>El archivo fue enviado pero <font color=red>no</font> se pudo obtener la <font color=green>informaci&oacute;n</font> de registro.<br><br>Verifique en la <font color=blue>Autorizaci&oacute;n y Cancelaci&oacute;n </font> de cuentas para obtener el <b>registro</b> de la informaci&oacute;n <i>procesada</i>.";
				   } else {
					  strMensaje="<br><b><font color=red>Transacci&oacute;n</font></b> no exitosa.<br><br>El archivo <font color=blue>no</font> fue enviado correctamente.<br>Porfavor <font color=green>intente</font> en otro momento.";}
				 }
				else
				 {
				   EIGlobal.mensajePorTrace( "MMC_Alta-> Copia remota desde tuxedo se realizo correctamente.", EIGlobal.NivelLog.INFO);
				   BufferedReader arc;
				   try
					 {
					   /* Se le quita el path al archivo para operar con el, en el directorio temporal del app*/
					   EIGlobal.mensajePorTrace( "MMC_Alta-> El archivo que se trajo (E): "+ nombreArchivo, EIGlobal.NivelLog.INFO);
					   nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
					   EIGlobal.mensajePorTrace( "MMC_Alta-> El archivo que se abre (E): "+nombreArchivo, EIGlobal.NivelLog.INFO);

					   arc=new BufferedReader(new FileReader(nombreArchivo));
					   if((line=arc.readLine())!=null)
						{
						  EIGlobal.mensajePorTrace( "MMC_Alta-> Se leyo del archivo: "+line, EIGlobal.NivelLog.INFO);
						  if(!consulta)
						   {
							 if(line.trim().length()>16){
							   strMensaje=line.trim().substring(16);
							 } else {
							   strMensaje="Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.<br><br>"; }
						   }
						  else
						   {
							 EI_Tipo Sec=new EI_Tipo();
							 Sec.iniciaObjeto(';','|',line+"|");
							 registroDeSolicitud=Sec.camposTabla[0][0].trim();
							 registroDeAlta=Sec.camposTabla[0][1].trim();
							 folioRegistro=Sec.camposTabla[0][2].trim();
							 /**GAE**/
							 datoSpecial = Sec.camposTabla[0][4].trim();
							 regAceptados = Sec.camposTabla[0][1].trim();
							 idLote = Sec.camposTabla[0][3].trim();
							 try {
								 EmailSender emailSender=new EmailSender();
								 if(emailSender.enviaNotificacion(codErrorOper)){
										/**FIN GAE**/
										 EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><>ENTRANDO A CONFIGURACION DE NOTIFICACIONES<><><><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
										 EmailDetails details = new EmailDetails();
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> totalRegistros para solicitud de alta      : " + totalRegistros, EIGlobal.NivelLog.INFO);
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> Cuentas registradas para solicitud de alta : " + registroDeSolicitud, EIGlobal.NivelLog.INFO);
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> Numero de contrato para solicitud de alta  : " + session.getContractNumber(), EIGlobal.NivelLog.INFO);
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> Nombre de contrato para solicitud de alta  : " + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> folioRegistro para solicitud de alta       : " + folioRegistro, EIGlobal.NivelLog.INFO);
										 EIGlobal.mensajePorTrace( "<><><><><><><><><><><><><>MMC_Alta -> codErrorOper para solicitud de alta        : " + codErrorOper, EIGlobal.NivelLog.INFO);
										 details.setNumRegImportados(Integer.parseInt(totalRegistros));
										 int regDif=Integer.parseInt(registroDeAlta)+Integer.parseInt(registroDeSolicitud);
										 int regRec=Integer.parseInt(totalRegistros)-regDif;
										 details.setNumeroContrato(session.getContractNumber());
										 details.setRazonSocial(session.getNombreContrato());
										 details.setNumRef(folioRegistro);
										 details.setDetalle_Alta(registroDeAlta+","+registroDeSolicitud+","+regRec);
										 details.setTipoOpCuenta("ALTA");
										 details.setEstatusActual("ENVIADO");



											details.setEstatusActual(codErrorOper);
											emailSender.sendNotificacion(req, IEnlace.NOT_ALTA_CUENTAS, details);
										}

								} catch (Exception e) {
									EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								}
							 strMensaje="<br>La informaci&oacute;n fue <font color=blue>enviada</font> y <b>procesada</b> por el sistema. <br>Para verificar el <font color=green>estatus</font> de las cuentas enviadas presione el bot&oacute;n Ver Detalle.<br><br>";

							 EIGlobal.mensajePorTrace( "MMC_Alta-> Cuentas registradas para solicitud de alta: "+registroDeSolicitud, EIGlobal.NivelLog.INFO);
							 EIGlobal.mensajePorTrace( "MMC_Alta-> Cuentas registradas para solicitud de alta: "+registroDeAlta, EIGlobal.NivelLog.INFO);
							 EIGlobal.mensajePorTrace( "MMC_Alta-> Folio de registro para el envio: "+folioRegistro, EIGlobal.NivelLog.INFO);
						   }
						}
                        // Envio a Lynx
                        EnlaceLynxUtils.llamadoConectorLynxAlta(contrato,
                                facultadesAltaCtas, fechaHoraOper,
                                (cuentas != null && !cuentas.isEmpty()) ? cuentas
                                : (Map<String, String>)
                                req.getSession().getAttribute("CuentasListaAltaArch"),
                                campos, arc);
                    	// Envio a Monitor Plus
                        arc = new BufferedReader(new FileReader(nombreArchivo));
                        EIGlobal.mensajePorTrace( "<------ SE ENVIA PARAMETROS PARA MAPEO A ** MONITORPLUS **  ----> "
                    	, EIGlobal.NivelLog.INFO);

                        VinculacionMonitorPlusUtils.enviarAltaCuentas(req, facultadesAltaCtas, (cuentas != null && !cuentas.isEmpty()) ? cuentas
                            : (Map<String, String>) req.getSession().getAttribute("CuentasListaAltaArch"), arc, IEnlace.SUCURSAL_OPERANTE, session,
                            (req.getSession().getAttribute(CLAVE_TIPO_DATO)!= null && "true".equals(String.valueOf(req.getSession().getAttribute(CLAVE_TIPO_DATO)))) ? true : false);
					   try
						  {
							arc.close();
						  }catch(IOException a){
							  EIGlobal.mensajePorTrace(new Formateador().formatea(a), EIGlobal.NivelLog.INFO);
						  }
					 }
				   catch(IOException e)
					 {
					   EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					 }
				 }
			 }
		 }

		if(!consulta){
			despliegaPaginaErrorURL(strMensaje,ALTACTAS_PARAM,TITULO_PARAM,CLAVEAYUDA_PARAM, req, res);
		}else
		 {
			req.setAttribute("strMensaje",strMensaje);
			req.setAttribute(NOMBRE_ARCHIVO_IMP,nombreArchivoImp);
			req.setAttribute("nombreArchivoDetalle",nombreArchivo);
			req.setAttribute("registroDeSolicitud",registroDeSolicitud);
			req.setAttribute("registroDeAlta",registroDeAlta);
			req.setAttribute(TOTAL_REGISTROS,totalRegistros);

			req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
			req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
			req.setAttribute(ENCABEZADO, CreaEncabezado(ALTACTAS_PARAM,"Administraci&oacute;n y control &gt; Cuentas  &gt; Alta &gt; Env&iacute;o","s26084h", req));

			req.setAttribute(NUM_CONTRATO, (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute(NOM_CONTRATO, (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute(NUM_USUARIO, (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute(NOM_USUARIO, (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
			req.getSession().removeAttribute(CLAVE_TIPO_DATO);
			EIGlobal.mensajePorTrace(AGREGA_CUENTAS_FIN, EIGlobal.NivelLog.INFO);
			evalTemplate("/jsp/MMC_AltaEnvio.jsp", req, res);
		 }
	  return 1;
	}



/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=1    */
/*************************************************************************************/
	/**
	 * Operaci�n que agrega las cuentas a la lista provisional que despu�s se enviar� al servidor para guardado
	 * @param req instancia de HTTPServletRequest
	 * @param res instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException fallo en operaciones de lectura de archivos/dispositivos
	 * */
	public int agregaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);
		//Se agrega por unificacion de token
		int tipoAlta=Integer.parseInt( (String)getFormParameter(req,TIPOALTA) == null ? sess.getAttribute(TIPOALTA ).toString() : (String)getFormParameter(req,TIPOALTA) );
		EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): tipoAlta." +tipoAlta, EIGlobal.NivelLog.INFO);

		String numeroCuentaMB=(String)getFormParameter(req,"numeroCuentaMB");
		String nombreTitularMB=(String)getFormParameter(req,"nombreTitularMB");

		String tipoCuentaBN=(String)getFormParameter(req,"hiTipoCuentaBN");
		String numeroCuentaBN=(String)getFormParameter(req,"numeroCuentaBN");
		String nombreTitularBN=(String)getFormParameter(req,"nombreTitularBN");
		String bancoBN= (String)getFormParameter(req,"bancoBN");
		String divisaBN=(String)getFormParameter(req,"cmbDivisaOBN");

		String paisBI=(String)getFormParameter(req,"paisBI");
		String strCiudadBI=(String)getFormParameter(req,"strCiudadBI");
		String strBancoBI=(String)getFormParameter(req,"strBancoBI");
		String cveDivisaBI=(String)getFormParameter(req,"cveDivisaBI");
		String strDivisaBI=(String)getFormParameter(req,"strDivisaBI");
		String numeroCuentaBI=(String)getFormParameter(req,"numeroCuentaBI");
		String strClaveABABI=(String)getFormParameter(req,"strClaveABABI");
		String nombreTitularBI=(String)getFormParameter(req,"nombreTitularBI");
		String paisBIBenef = (String)getFormParameter(req,"paisBIBenef");
		String calleBenef = (String)getFormParameter(req,"calleBenef");
		String ciudadBenef = (String)getFormParameter(req,"ciudadBenef");
		String idBenef = (String)getFormParameter(req,"idBenef");

		String numeroCuentaNM=(String)getFormParameter(req,"numeroCuentaNM");
		String tipoCuentaNM=(String)getFormParameter(req,"tipoCuentaNM");
		String nombreTitularNM=(String)getFormParameter(req,"nombreTitularNM");
		String bancoNM=(String)getFormParameter(req,"bancoNM");
		//Inicio se modifica por unificacion de token
		StringBuffer strTramaMB = new StringBuffer("");
		StringBuffer strTramaBN = new StringBuffer("");
		StringBuffer strTramaBI = new StringBuffer("");

		if (getFormParameter(req,STRTRAMAMB) != null){
			strTramaMB = new StringBuffer( (String)getFormParameter(req,STRTRAMAMB) );
		}
		if (getFormParameter(req,STRTRAMABN) != null){
			strTramaBN = new StringBuffer( (String)getFormParameter(req,STRTRAMABN) );
		}
		if (getFormParameter(req,STRTRAMABI) != null){
			strTramaBI = new StringBuffer( (String)getFormParameter(req,STRTRAMABI) );
		}
		//Fin se modifica por unificacion de token
		String cvePlaza="";
		String strPlaza="";
		String cveBanco="";
		String strBanco="";
		String strSuc="";
		String strCtaTmp="";
		String modulo="1";

		String JSP_TEMPLATE="/jsp/MMC_Alta.jsp";

		 TrxGPF2VO trxGPF2VO	= null;
		 boolean tipoDato = false;
		switch (tipoAlta)
		 {
		    case 0: // Mismo Banco
			       EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): Mismo Banco.", EIGlobal.NivelLog.INFO);
			       cvePlaza = "01001";
				   strPlaza = "MEXICO, D.F.";
				   cveBanco = BANCO_BANME;
				   strBanco = "BANCO MEXICANO";
				   strSuc   = "035";

				   int leng = strTramaMB.length();
				   strTramaMB.append( numeroCuentaMB ).append( PIPE )
				   	.append( nombreTitularMB ).append( PIPE )
				   	.append( cvePlaza ).append( PIPE )
				   	.append( strPlaza ).append( PIPE )
				   	.append( cveBanco ).append( PIPE )
				   	.append( strBanco ).append( PIPE )
				   	.append( strSuc ).append( '@' );

					strCtaTmp = strTramaMB.substring(leng, strTramaMB.length() - 1);
				   EmailContainer ecMB = new EmailContainer();
				   ecMB.setNumCuenta(numeroCuentaMB);
				   ecMB.setTitular(nombreTitularMB);
				   if(req.getSession().getAttribute("detalleCuentasAlta") == null){
					   Vector detalleCuentasAlta = new Vector();
					   detalleCuentasAlta.add(ecMB);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					}else{
					   Vector detalleCuentasAlta = (Vector)req.getSession().getAttribute("detalleCuentasAlta");
					   detalleCuentasAlta.add(ecMB);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					}
			       break;
			case 1: // Banco Nacional
			       EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): Banco Nacional.", EIGlobal.NivelLog.INFO);
			       cvePlaza = "01001";
				   strPlaza = "MEXICO, D.F.";
				   cveBanco = bancoBN.substring(0,bancoBN.indexOf(CHAR_MENOS)).trim();
				   strBanco = bancoBN.substring(bancoBN.indexOf(CHAR_MENOS)+1).trim();
				   strSuc   = "0001";
				   leng = strTramaBN.length();
				   strTramaBN.append( numeroCuentaBN ).append( PIPE )
				   	.append( nombreTitularBN ).append( PIPE )
				   	.append( cvePlaza ).append( PIPE )
				   	.append( strPlaza ).append( PIPE )
				   	.append( cveBanco ).append( PIPE )
				   	.append( strBanco ).append( PIPE )
				   	.append( strSuc ).append( PIPE )
				    .append( tipoCuentaBN ).append( PIPE )
				    .append( divisaBN ).append( '@' );

					 strCtaTmp = strTramaBN.substring(leng, strTramaBN.length() - 1);

				   EmailContainer ecBN = new EmailContainer();
				   ecBN.setNumCuenta(numeroCuentaBN);
				   ecBN.setTitular(nombreTitularBN);
				   if(req.getSession().getAttribute("detalleCuentasAlta") == null){
					   Vector detalleCuentasAlta = new Vector();
					   detalleCuentasAlta.add(ecBN);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					}else{
					   Vector detalleCuentasAlta = (Vector)req.getSession().getAttribute("detalleCuentasAlta");
					   detalleCuentasAlta.add(ecBN);
					   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
					}
			       break;
			case 2: // Banco Internacional
			       /************************* Cambio - Clave SWIFT ******************/
				trxGPF2VO	= existeClaveSwift(paisBI, strClaveABABI);

			       if(trxGPF2VO != null){
						leng = strTramaBI.length();
			    	   strTramaBI.append( numeroCuentaBI ).append( PIPE )
					   	.append( nombreTitularBI ).append( PIPE )
					   	.append( paisBI ).append( PIPE )
					   	.append( trxGPF2VO.getOTwn() ).append( PIPE )
					   	.append( trxGPF2VO.getODesCor() ).append( PIPE )
					   	.append( cveDivisaBI ).append( PIPE )
					   	.append( strDivisaBI ).append( PIPE )
					    .append( numeroCuentaBI ).append( PIPE )
			    	    .append( trxGPF2VO.getOIdBaCo() ).append( PIPE )
			    	    .append( paisBIBenef ).append( PIPE )
			    	    .append( calleBenef ).append( PIPE )
			    	    .append( ciudadBenef ).append( PIPE )
			    	    .append( idBenef ).append( PIPE ).append( '@' );

						 strCtaTmp = strTramaBI.substring(leng, strTramaBI.length() - 1);

			    	   EmailContainer ecBI = new EmailContainer();
				       ecBI.setNumCuenta(numeroCuentaBI);
				       ecBI.setTitular(nombreTitularBI);
				       if(req.getSession().getAttribute("detalleCuentasAlta") == null){
						   Vector detalleCuentasAlta = new Vector();
						   detalleCuentasAlta.add(ecBI);
						   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
						}else{
						   Vector detalleCuentasAlta = (Vector)req.getSession().getAttribute("detalleCuentasAlta");
						   detalleCuentasAlta.add(ecBI);
						   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
						}
			       }else{
				   req.setAttribute("nombreTitularBI", nombreTitularBI);
			    	   req.setAttribute("numeroCuentaBI", numeroCuentaBI);
			    	   req.setAttribute("cveDivisaBI", cveDivisaBI);
			    	   req.setAttribute("strBancoBI", strBancoBI);
			    	   req.setAttribute("strCiudadBI", strCiudadBI);
			    	   req.setAttribute("paisBI", paisBI);
			    	   req.setAttribute("paisBIBenef", paisBIBenef);
			    	   req.setAttribute("calleBenef", calleBenef);
			    	   req.setAttribute("ciudadBenef", ciudadBenef);
			    	   req.setAttribute("idBenef", idBenef);
			    	   req.setAttribute("mensaje", "\"La Clave ABA / SWIFT ingresada es incorrecta.\",11");
			    	   modulo="1";
			       }
			       break;

			case 3: // Archivo
			       EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): Por Archivo.", EIGlobal.NivelLog.INFO);
			       tipoDato = true;
			       req.getSession().setAttribute(CLAVE_TIPO_DATO,tipoDato);
			       boolean var = importarArchivo(req,res);
			       EIGlobal.mensajePorTrace("__________________ var " + var, EIGlobal.NivelLog.INFO);
				   if(!var){
					 modulo="0";
				   }
				   /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
				    * Se comenta el jsp a redireccionar, para que ahora continue con
				    * la validacion del token (esto lo hace cuando el modulo es igual a 2)*/
				   else{
//					   JSP_TEMPLATE="/jsp/MMC_AltaArchivo.jsp";
					   modulo = "2";
				   }
				   sess.removeAttribute( TIPOALTA );
				   sess.removeAttribute( FILENAMEUPLOAD );
				   sess.removeAttribute( "fileName" );
				   /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
			       break;
			case 4: //movil

				EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas(): Movil. bancoNM="+bancoNM , EIGlobal.NivelLog.INFO);

				final String TIPO_CUENTA_NM_SANTANDER 	= "1";
				final String TIPO_CUENTA_NM_OTROS 		= "2";


				/**Valida si el tipo de cuenta seleccionado es Santander*/
				if(TIPO_CUENTA_NM_SANTANDER.equals(tipoCuentaNM)){

					if(this.validateNumeroMovil(numeroCuentaNM)){


						cveBanco = BANCO_BANME;
						strBanco = "BANCO MEXICANO";

						leng = strTramaMB.length();
						strTramaMB
							.append(numeroCuentaNM).append(PIPE)
							.append(nombreTitularNM).append(PIPE)
							.append(cvePlaza).append(PIPE)
							.append(strPlaza).append(PIPE)
							.append(cveBanco).append(PIPE)
							.append(strBanco).append(PIPE)
							.append(strSuc).append('@');
						strCtaTmp = strTramaMB.substring(leng, strTramaMB.length() - 1);
					}
					else{
						req.setAttribute("numeroCuentaNM", numeroCuentaNM);
						req.setAttribute("tipoCuentaNM", tipoCuentaNM);
						req.setAttribute("nombreTitularNM", nombreTitularNM);
						req.setAttribute("bancoNM", bancoNM);

						req.setAttribute("mensaje", "\"La cuenta no existe\", 11");
				    	modulo="1";
					}
				}
				/**Valida si el tipo de cuenta seleccionado es Otros Bancos*/
				else if(TIPO_CUENTA_NM_OTROS.equals(tipoCuentaNM)){
					cveBanco = bancoNM.substring(0,bancoNM.indexOf('-')).trim();
					strBanco = bancoNM.substring(bancoNM.indexOf('-')+1).trim();

					leng = strTramaBN.length();
					strTramaBN.append(numeroCuentaNM)
						.append(PIPE).append(nombreTitularNM)
						.append(PIPE).append(cvePlaza)
						.append(PIPE).append(strPlaza)
						.append(PIPE).append(cveBanco)
						.append(PIPE).append(strBanco)
						.append(PIPE).append(strSuc)
						.append(PIPE).append(' ')
						.append(PIPE).append(' ').append('@');
					strCtaTmp = strTramaBN.substring(leng, strTramaBN.length() - 1);
				}

			   EmailContainer ecNM = new EmailContainer();
			   ecNM.setNumCuenta(numeroCuentaNM);
			   ecNM.setTitular(nombreTitularNM);
			   if(req.getSession().getAttribute("detalleCuentasAlta") == null){
				   Vector detalleCuentasAlta = new Vector();
				   detalleCuentasAlta.add(ecNM);
				   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
				}else{
				   Vector detalleCuentasAlta = (Vector)req.getSession().getAttribute("detalleCuentasAlta");
				   detalleCuentasAlta.add(ecNM);
				   req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
				}
		       break;
		 }

		EIGlobal.mensajePorTrace(AGREGA_CUENTAS_FIN, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas()::tipoAlta::"+tipoAlta+"::", EIGlobal.NivelLog.INFO);

		if(tipoAlta!=3)
		 {
            String numeroCuenta = null;
            switch (tipoAlta)
            {
                case 0: // Mismo Banco
                       numeroCuenta = numeroCuentaMB;
                       break;
                case 1: // Banco Nacional
                       numeroCuenta = numeroCuentaBN;
                       break;
                case 2: // Banco Internacional
                       numeroCuenta = numeroCuentaBI;
                       break;
                 case 4: // Moviles
                     numeroCuenta = numeroCuentaNM;
                     break;
            }

            EIGlobal.mensajePorTrace("MMC_Alta - agregaCuentas()::numeroCuenta::"+numeroCuenta+"::", EIGlobal.NivelLog.INFO);
            Map<String, String> cuentas = req.getSession().getAttribute("CuentasListaAlta") == null ?
                    new HashMap<String, String>() : (Map<String, String>)req.getSession().getAttribute("CuentasListaAlta");
            cuentas.put(numeroCuenta, String.valueOf(tipoAlta).concat(
                    String.valueOf(PIPE).concat(strCtaTmp)));
            req.getSession().setAttribute("CuentasListaAlta",cuentas);
            req.setAttribute(MODULO_PARAM, modulo);

		   req.setAttribute("tablaBN", generaTablaCuentas(strTramaBN.toString(),false));
		   req.setAttribute(STRTRAMABN,strTramaBN.toString());
		   EIGlobal.mensajePorTrace(">>>>>>>>>>>>>>>> (--)" + strTramaBI.length(), EIGlobal.NivelLog.INFO);
		   EIGlobal.mensajePorTrace(">>>>>>>>>>>>>>>> (--)" + strTramaBI, EIGlobal.NivelLog.INFO);
		   req.setAttribute("tablaMB", generaTablaCuentas(strTramaMB.toString(),true));
		   req.setAttribute(STRTRAMAMB,strTramaMB.toString());

		   req.setAttribute("tablaBI", generaTablaCuentasBI(strTramaBI.toString(),"Cuentas Internacionales"));
		   req.setAttribute(STRTRAMABI,strTramaBI.toString());

		   req.setAttribute("comboPaises", traeDatos("PAIS").toString());
		   req.setAttribute("comboDivisas", traeDatos("DIVISA").toString());
		   req.setAttribute("ListSpid", traeBancosSPID().toString());
		   req.setAttribute("ListAll", traeBancos().toString());
		 }

		EIGlobal.mensajePorTrace("------>guarda Tipo Cuenta en sesion -->" +tipoAlta+"<---", EIGlobal.NivelLog.INFO);
		req.getSession().setAttribute(TIPOALTA, tipoAlta);

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		req.setAttribute(ENCABEZADO, CreaEncabezado(ALTACTAS_PARAM,"Administraci&oacute;n y control &gt; Cuentas &gt; Alta &gt; Agregar","s26080h", req));

		req.setAttribute(NUM_CONTRATO, (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute(NOM_CONTRATO, (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute(NUM_USUARIO, (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute(NOM_USUARIO, (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

        EIGlobal.mensajePorTrace(AGREGA_CUENTAS_FIN, EIGlobal.NivelLog.INFO);

        EIGlobal.mensajePorTrace("_______________ JSP_TEMPLATE :" + JSP_TEMPLATE,EIGlobal.NivelLog.INFO);
        /** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token
         * Solo se enviara al jsp cuando no sea una carga por archivo*/
        if( tipoAlta != 3 || !"2".equals( modulo ) ){
        	evalTemplate(JSP_TEMPLATE, req, res);
        }
        /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 Unificacion Token */
		return 1;
	}

	/**
	 * Cambio - Clave SWIFT
	 * @param busCvePais clave de pa�s
	 * @param claveSwift clave internacional de cuenta bancaria
	 * @return instancia de {@link mx.altec.enlace.bo.TrxGPF2VO}
	 * */
	private TrxGPF2VO existeClaveSwift(String busCvePais, String claveSwift){
		List trxGPF2VOList;
	    List paises 		= CatDivisaPais.getInstance().getListaPaises();
	    TrxGPF2BO trxGPF2BO	= new mx.altec.enlace.bo.TrxGPF2BO();
	    TrxGPF2VO trxGPF2VO	= null;

		for(int i=0; i< paises.size(); i++){
			if(((TrxGP93VO)paises.get(i)).getOVarCod().substring(4).trim().equals(busCvePais.trim())){
				busCvePais = ((TrxGP93VO)paises.get(i)).getOVarCod().substring(0,3).trim();
			    break;
			}
    	}

		trxGPF2VOList	= trxGPF2BO.obtenCatalogoBancos(claveSwift, busCvePais, "", "001".equals(busCvePais)?"A":"S");

		if(trxGPF2VOList.size() == 1){
			trxGPF2VO	= (TrxGPF2VO) trxGPF2VOList.get(0);
		}
		return trxGPF2VO;
	}


	/**
	 * Verifica que la cuenta movil Mismo Banco tenga relaci&oacute;n
	 * en la tabla personas.
	 * @param numeroMovil 	El n&uacute;mero m&oacute;vil que se
	 * 						pretende verificar.
	 * @return 				<i>true</i> en caso de que se haya encontrado la
	 * 						cuenta, <i>false</i> en caso contrario.
	 * */
	private boolean validateNumeroMovil(String numeroMovil){
		TrxOD13BO trxOD13BO = new TrxOD13BO();
		return trxOD13BO.validateNumeroMovil(numeroMovil);
	}

	/**
	 * Operaci�n que valida y carga la informaci�n de <strong>Alta de Cuentas por Archivo</strong>
	 * @param req 				instancia de HTTPServletRequest
	 * @param res 				instancia de HTTPServletResponse
	 * @throws ServletException error en el servlet
	 * @throws IOException 		fallo en operaciones de lectura de archivos
	 * @return 					<strong>true</strong> en caso de �xito, de no ser as� devolver� <strong>false</strong>
	 * */
boolean importarArchivo( HttpServletRequest req, HttpServletResponse res) throws ServletException, java.io.IOException
	 {
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);

		EI_Importar arcImp= new EI_Importar();
		MAC_Registro Registro;

		String nombreArchivoImp="";
		String nombreArchivoErr="";
		String usuario="";
		String line="";

		/**GAE**/
		Vector detalleCuentasAlta = new Vector();
		/**FIN GAE**/

		StringBuffer erroresRegistro=new StringBuffer("");

		Vector cuentas=new Vector();

		boolean erroresEnArchivo=false;

		int totalRegistros				= 0;
		int numeroCtasInternas			= 0;
		int numeroCtasNacionales		= 0;
		int numeroCtasInternacionales	= 0;
		/** Mapa de cuentas **/
		Map<String, String> cuentasArc = new HashMap<String, String>();

		int err=-1;

		//nombreArchivoImp=arcImp.importaArchivo(req,res);
		nombreArchivoImp = getFormParameter(req,FILENAMEUPLOAD) == null ? sess.getAttribute(FILENAMEUPLOAD).toString() : getFormParameter(req,FILENAMEUPLOAD).toString();
		EIGlobal.mensajePorTrace("AQUI ESTA EL MEOLLO :" + nombreArchivoImp, EIGlobal.NivelLog.INFO);
		File f = new File(nombreArchivoImp);
		EIGlobal.mensajePorTrace("EXISTE :" + f.exists(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("TAMANO :" + f.length(), EIGlobal.NivelLog.DEBUG);


		usuario=session.getUserID8();
		nombreArchivoErr="MAC_Errores."+usuario+".html";

		HashMap facultadesAltaCtas = new HashMap ();
		facultadesAltaCtas=(HashMap)req.getSession ().getAttribute (FACALTACTAS);

		EIGlobal.mensajePorTrace("****************** nombreArchivoImp : " +nombreArchivoImp +"  *************" , EIGlobal.NivelLog.INFO);
			BufferedReader archImp= new BufferedReader( new FileReader(nombreArchivoImp) );
			BufferedWriter archErr = new BufferedWriter( new FileWriter(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr) );

			File arcE=new File(nombreArchivoImp);
			File arcS=new File(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr);

			EIGlobal.mensajePorTrace("MMC_Alta - importarArchivo(): Buscando archivos ... " + IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MMC_Alta - importarArchivo(): Buscando archivos ... " + nombreArchivoImp, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("MMC_Alta - Nombre Archivo ...> " + nombreArchivoImp +"<--", EIGlobal.NivelLog.DEBUG);

			int tamNombre=nombreArchivoImp.length();
			EIGlobal.mensajePorTrace("MMC_Alta - importarArchivo():TIPO ARCHIVO ...>" + tamNombre +"<--", EIGlobal.NivelLog.DEBUG);

		    String sbstExt=nombreArchivoImp.substring(tamNombre-4);


		    if(!sbstExt.equalsIgnoreCase(".txt") && !sbstExt.equalsIgnoreCase(".zip")&& !sbstExt.equalsIgnoreCase(".rar")){
		    	req.setAttribute("ErroresEnArchivo","cuadroDialogo('El archivo no tiene la extensi&oacute;n correcta, por favor verifiquelo e intente nuevamente.', 3);");
				return false;
		    }


			if(arcS.exists()){
				archErr.write(erroresHead());
			}
			if( (!arcE.exists()) || arcE.length()<50 )
		     {
			   EIGlobal.mensajePorTrace("MMC_Alta - importarArchivo(): Alguno de los archivos no se encontro", EIGlobal.NivelLog.DEBUG);
			   if(arcS.exists()){
				   archErr.write("<br>\n<b><font color=red>Error general</font></b>\n<br><DD><LI>El archivo que especific&oacute; no contiene datos o fall&oacute; el env&iacute;o.");
			   }
			   erroresEnArchivo=true;
			 }
			else
			 {
				EIGlobal.mensajePorTrace("MMC_Alta - importarArchivo(): Comenzando la validaci�n del Archivo.", EIGlobal.NivelLog.DEBUG);

				/*Obtener catalogos de paises,bancos,plazas,divisas	*/
				/***********************************************************************************************/
				String sqlPais   = "SELECT CVE_PAIS from COMU_PAISES order by CVE_PAIS";
				String sqlDivisa = "SELECT CVE_DIVISA FROM COMU_MON_DIVISA order BY UPPER(CVE_DIVISA)";
				String sqlBanco  = "SELECT cve_interme from comu_interme_fin where num_cecoban<>0 and FCH_BAJA is null order by cve_interme";
				String sqlPlaza  = "select PLAZA_BANXICO from comu_pla_banxico order by PLAZA_BANXICO";

				EI_Query BD= new EI_Query();
				Registro = new MAC_Registro();
                StringBuilder regAlta = new StringBuilder();
                char regSep = '|';

				Registro.listaPaises =BD.ejecutaQueryCampo(sqlPais);
				Registro.listaDivisas=BD.ejecutaQueryCampo(sqlDivisa);
				Registro.listaBancos =BD.ejecutaQueryCampo(sqlBanco);
				Registro.listaPlazas =BD.ejecutaQueryCampo(sqlPlaza);

				EIGlobal.mensajePorTrace( "MMC_Alta->  tama�o : "+Registro.listaPaises.size() , EIGlobal.NivelLog.DEBUG);

				if( Registro.listaPaises==null  ||
					Registro.listaDivisas==null ||
					Registro.listaBancos==null  ||
					Registro.listaPlazas==null )
				 {
				   EIGlobal.mensajePorTrace( "MMC_Alta-> Alguno no funciono ...", EIGlobal.NivelLog.DEBUG);
				   erroresEnArchivo=true;
				   erroresRegistro=new StringBuffer("");
				   erroresRegistro.append("<br>\n<b>General:</b>\n<br>");
				   erroresRegistro.append("<DD><LI>Alguno de los cat&aacute;logos no se pudo cargar y el archivo no se puede validar correctamente. Intente m�s tarde porfavor.");
				 }

				EIGlobal.mensajePorTrace( "MMC_Alta-> Se realizaron los Querys ", EIGlobal.NivelLog.DEBUG);
				/***********************************************************************************************/

				while( (line=archImp.readLine())!= null )
				  {
					String cuenta= line.substring(6,26);
					cuenta=cuenta.trim();

					String tipoCuenta= line.substring(0,6);
					tipoCuenta=tipoCuenta.trim();

					EIGlobal.mensajePorTrace( "MMC_Alta-> Tipo Cuenta"+ tipoCuenta, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "MMC_Alta-> Cuenta"+ cuenta, EIGlobal.NivelLog.INFO);
					if(!cuenta.trim().matches("[0-9]*")&&(tipoCuenta.equalsIgnoreCase("MOVIL")||tipoCuenta.equalsIgnoreCase("SANTAN"))){
						EIGlobal.mensajePorTrace( "MMC_Alta-> Cuentas con caracteres raros", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("La cuenta contiene caracteres no permitidos");

						if(arcS.exists()){
						  archErr.write(erroresRegistro.toString()); }
					}



					if(line.length()==Registro.LEN_MB && !((Boolean)facultadesAltaCtas.get("AltaCtasMB")).booleanValue() )
					 {
						EIGlobal.mensajePorTrace( "MMC_Alta-> Sin facultad para Alta MB", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("No tiene <font color=blue>FACULTAD</font> para dar de alta cuentas del Mismo Banco");

						if(arcS.exists()){
						  archErr.write(erroresRegistro.toString()); }
					 }
					else
					if(line.length()==Registro.LEN_BN && !((Boolean)facultadesAltaCtas.get("AltaCtasBN")).booleanValue() )
					  {
						EIGlobal.mensajePorTrace( "MMC_Alta-> Sin facultad para Alta BN", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("No tiene <font color=blue>FACULTAD</font> para dar de alta cuentas de Bancos Externos");

						if(arcS.exists()){
						  archErr.write(erroresRegistro.toString()); }
					  }
					else
					if(line.length()==Registro.LEN_BI && !((Boolean)facultadesAltaCtas.get("AltaCtasBI")).booleanValue() )
					  {
						EIGlobal.mensajePorTrace( "MMC_Alta-> Sin facultad para Alta BI", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("No tiene <font color=blue>FACULTAD</font> para dar de alta cuentas Internacionales");

						if(arcS.exists()){
						  archErr.write(erroresRegistro.toString()); }
					  }
					if(line.length()==Registro.LENNM && !((Boolean)facultadesAltaCtas.get("AltaCtasNM")).booleanValue() )
					{

						EIGlobal.mensajePorTrace( "MMC_Alta-> Sin facultad para Alta NM", EIGlobal.NivelLog.INFO);
						erroresEnArchivo=true;
						erroresRegistro=new StringBuffer("");
						erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
						erroresRegistro.append("<DD><LI>");
						erroresRegistro.append("No tiene <font color=blue>FACULTAD</font> para dar de alta cuentas de N&uacute;meros M&oacute;viles");


						if(arcS.exists()){
							archErr.write(erroresRegistro.toString());}
					}
					else
					  {
						err = Registro.parse(line);
						EIGlobal.mensajePorTrace( "MMC_Alta-> Vector " + Registro.listaErrores.size() + " err "+ err, EIGlobal.NivelLog.INFO);

						if(err>0)
						  {
							 erroresEnArchivo=true;
							 erroresRegistro=new StringBuffer("");
							 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
							 for(int i=0;i<Registro.listaErrores.size();i++)
							  {
								erroresRegistro.append("<DD><LI>");
								erroresRegistro.append(Registro.listaErrores.get(i).toString());
							  }
							 if(arcS.exists()){
								archErr.write(erroresRegistro.toString()); }
						  }
						 else
						  {
							 if(cuentas.indexOf(Registro.cuenta)>=0)
							  {
								 EIGlobal.mensajePorTrace( "MMC_Alta-> La cuenta ya existe en el archivo", EIGlobal.NivelLog.INFO);
								 erroresEnArchivo=true;
								 erroresRegistro=new StringBuffer("");
								 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
								 erroresRegistro.append("<DD><LI>");
								 erroresRegistro.append("La cuenta ya <font color=green>existe</font> en el archivo: " + Registro.cuenta);

								 if(arcS.exists()){
									archErr.write(erroresRegistro.toString()); }
							  }
							 else{
								 EIGlobal.mensajePorTrace( "MMC_Alta-> Cargando bean para email", EIGlobal.NivelLog.INFO);
								 EmailContainer ec = new EmailContainer();
								 ec.setNumCuenta(Registro.cuenta);
								 ec.setTitular(Registro.nombreTitular);
								 EIGlobal.mensajePorTrace( "MMC_Alta-> cuenta email->" + ec.getNumCuenta(), EIGlobal.NivelLog.INFO);
								 detalleCuentasAlta.add(ec);

								 cuentas.add(Registro.cuenta);
                                 regAlta.setLength(0);
                                 regAlta.append(EnlaceLynxUtils.obtenerTipoAltaPorArchivo(Registro.tipoRegistro))
                                         .append(regSep)
                                         .append(Registro.cuenta) /* 1 */
                                         .append(regSep)
                                         .append(Registro.nombreTitular) /* 2 */
                                         .append(regSep)
                                         .append(Registro.cvePais.trim()) /* 3 */
                                         .append(regSep)
                                         .append(Registro.ciudad.trim())
                                         .append(regSep)
                                         .append(Registro.cveBanco.trim()) /* 5 */
                                         .append(regSep)
                                         .append(Registro.cveDivisa) /* 6 */
                                         .append(regSep)
                                         .append(Registro.banco.trim()) /* 7 */
                                         .append(regSep)
                                         .append(Registro.cveABA) /* 8 */
                                         .append(regSep)
                                         .append(Registro.cveABA); /* 9 */
								 EIGlobal.mensajePorTrace( "MMC_Alta-> cuenta alta lynx ->"
                                         + regAlta.toString() + "<", EIGlobal.NivelLog.DEBUG);
                                 cuentasArc.put(cuenta, regAlta.toString());
							 }
						  }
					  }

					try {
	  					if(Registro.tipoRegistro.equals(Registro.SAN)){
						  numeroCtasInternas++;
	  					}
					    if(Registro.tipoRegistro.equals(Registro.EXT)){
						  numeroCtasNacionales++;
					    }
						if(Registro.tipoRegistro.equals(Registro.INT)){
						  numeroCtasInternacionales++;
						}
						if(Registro.MOV.equals(Registro.tipoRegistro)){
							if(BANCO_BANME.equals(Registro.cveBanco.trim())){
								numeroCtasInternas++;
							}
							else{
								numeroCtasNacionales++;
							}
						}

					  } catch(Exception e) {
							 erroresEnArchivo=true;
							 erroresRegistro=new StringBuffer("");
							 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+1)+"</b>\n<br>");
							 erroresRegistro.append("<DD><LI>");
							 erroresRegistro.append("Existe un error con el tipo de cuenta.");
							 if(arcS.exists()){
								archErr.write(erroresRegistro.toString()); }
							 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					  }

					totalRegistros++;
					Registro.listaErrores.clear();
				  }
                req.getSession().setAttribute("CuentasListaAltaArch", cuentasArc);
				req.getSession().setAttribute("detalleCuentasAlta", detalleCuentasAlta);
			 }
			//TODO BIT CU5071, importar archivo de Alta de cuentas
			/*VSWF HGG I*/
			if (BITA_ACTIVA.equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
			try {
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.EA_CUENTAS_ALTA_IMPORTAR_ARCHIVO);
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
				}
				if (nombreArchivoImp != null) {
					bt.setNombreArchivo(nombreArchivoImp);
				}

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			}
			/*VSWF HGG F*/
			EIGlobal.mensajePorTrace( "MMC_Alta-> Cerrando los archivos .. ", EIGlobal.NivelLog.INFO);

			if(arcS.exists()){
				archErr.write(erroresFoot());
			}
			archErr.flush();
			archErr.close();
			archImp.close();
			cuentas.clear();

	   if(erroresEnArchivo)
		 {
		   /* Nombre de archivo de errores para descarga en web. Sin path */
		   ArchivoRemoto archR = new ArchivoRemoto();
		   if(archR.copiaLocalARemoto(nombreArchivoErr,"WEB")){
			 req.setAttribute("ErroresEnArchivo","js_despliegaErrores('"+nombreArchivoErr+"');");
		   }else
			 {
			   req.setAttribute("ErroresEnArchivo","cuadroDialogo('Durante la importaci&oacute;n se encontraron algunos errores en el archivo, por favor verifiquelo e intente nuevamente.', 3);");
			   EIGlobal.mensajePorTrace("MMC_Alta-> exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
			 }
		   return false;
		 }
		else
		 {
		   req.setAttribute(NOMBRE_ARCHIVO_IMP,nombreArchivoImp);
		   req.setAttribute(TOTAL_REGISTROS,Long.toString(totalRegistros));
		   req.setAttribute("numeroCtasInternas",Long.toString(numeroCtasInternas));
		   req.setAttribute("numeroCtasNacionales",Long.toString(numeroCtasNacionales));
		   req.setAttribute("numeroCtasInternacionales",Long.toString(numeroCtasInternacionales));

		   req.setAttribute("msg", "Se importaron exitosamente " +Long.toString(totalRegistros)+" registros");
		 }

	   EIGlobal.mensajePorTrace( "MMC_Alta-> Se validaron los registros: " + totalRegistros, EIGlobal.NivelLog.INFO);
	   return true;
	 }

	/**
	 * Operaci�n recibe las tramas de las cuentas Mismo Banco y Cuentas Bancos Nacionales
	 * para generar las respectivas representaciones en HTML
	 * @param strCtaTmp	contiene la trama de cuentas separadas por el s�mbolo <strong>arroba (@)</strong>
	 * @param titulo	si es <i>true</i> entonces el t�tulo de la tabla ser� "Cuentas Mismo Banco",
	 * 					si es <i>false</i> entonces el t�tulo de la tabla ser� "Cuentas Bancos Nacionales"
	 * @return representaci�n en HTML de la trama de entrada
	 * */
 String generaTablaCuentas(String strCtaTmp,boolean titulo)
	{
		if( "".equals(strCtaTmp.trim()) ){
			return "";
		}
		String [] titulos={"",
					   "Cuenta/M&oacute;vil",
					   "Titular",
					   "Plaza",
					   "Banco",
					   "Sucursal",
					   "Divisa"};

		int[] datos={6,2,0,1,3,5,6,8};
		int[] values={0,0,1,2,3,4,5,6,8};
		int[] align={1,0,0,0,1,0,0};

		if(titulo)
		 {
		   datos[0]=2;
		   titulos[0]="Cuentas Mismo Banco";
		 }
		else
		 {
		   datos[0]=6;
		   titulos[0]="Cuentas Bancos Nacionales";
		 }

		EI_Tipo ctaMB=new EI_Tipo();

		ctaMB.iniciaObjeto(strCtaTmp);
		return ctaMB.generaTabla(titulos,datos,values,align);
	}

	/**
	 * Operaci�n recibe las tramas de las cuentas de Bancos Extranjeros
	 * para generar las respectivas representaciones en HTML
	 * @param strCtaTmp	trama de cuentas de Bancos Extranjeros separadas por el s�mbolo <strong>arroba (@)</strong>
	 * @param titulo	valor que tomar&aacute; como t&iacute;tulo de la tabla.
	 * @return representaci�n en HTML de la trama de entrada
	 * */
 String generaTablaCuentasBI(String strCtaTmp,String titulo)
	{
		if( "".equals(strCtaTmp.trim()) ){
			return "";
		}
		String [] titulos={"",
					   "Cuenta",
					   "Titular",
			           "Clave Divisa",
			           "Divisa",
					   "Pais",
					   "Banco",
					   "Ciudad",
					   "Clave ABA/SWIFT "};

		titulos[0]=titulo;

        /*
		0 numeroCuentaBI
		1 nombreTitularBI
		2 paisBI
		3 strCiudadBI
		4 strBancoBI
		5 cveDivisaBI
		6 strDivisaBI
		7 numeroCuentaBI
		8 strClaveABABI
		*/

		int[] datos={8,2,0,1,5,6,2,4,3,8};
		int[] values={0,0,1,2,3,4,5,6,7};
		int[] align={1,0,0,0,1,0,0,0,0,0};

		EI_Tipo ctaMB=new EI_Tipo();

		ctaMB.iniciaObjeto(strCtaTmp);
		return ctaMB.generaTabla(titulos,datos,values,align);
	}

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	/**
	 * Operaci�n ejecutada por defecto una vez que se ingresa al m�dulo de Altas de Cuentas
	 * @param req 				instancia de HTTPServletRequest
	 * @param res 				instancia de HTTPServletResponse
	 * @throws Exception 		forma gen�rica que lanza errores inesperados
	 * @return 					el valor <strong>1</strong> en todos los casos
	 * */
	public int iniciaAlta( HttpServletRequest req, HttpServletResponse res)
		throws Exception
	{
		EIGlobal.mensajePorTrace("MMC_Alta - iniciaAlta(): IniciaAlta.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);

		req.setAttribute(MODULO_PARAM, "0");

		req.setAttribute("comboPaises", traeDatos("PAIS").toString());
		req.setAttribute("comboDivisas", traeDatos("DIVISA").toString());
		req.setAttribute("ListSpid", traeBancosSPID().toString());
		req.setAttribute("ListAll", traeBancos().toString());

		req.setAttribute(NEW_MENU, session.getFuncionesDeMenu());
		req.setAttribute(MENU_PRINCIPAL, session.getStrMenu());
		req.setAttribute(ENCABEZADO, CreaEncabezado(ALTACTAS_PARAM,TITULO_PARAM,"s26080h", req));

		req.setAttribute(NUM_CONTRATO, (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute(NOM_CONTRATO, (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute(NUM_USUARIO, (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute(NOM_USUARIO, (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		//TODO BIT CU5071, inicia el flujo de Alta Cuentas
		/*VSWF HGG I*/
		if ( BITA_ACTIVA.equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim()) ){
		try{
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			if(getFormParameter(req,BitaConstants.FLUJO) != null){
				bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
			}else{
				bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_ALTA_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);

		} catch (SQLException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		}
		/*VSWF HGG F*/
        EIGlobal.mensajePorTrace("MMC_Alta - iniciaAlta(): Finalizando alta.", EIGlobal.NivelLog.INFO);
		evalTemplate("/jsp/MMC_Alta.jsp", req, res);
		return 1;
	}


	/**

	 * Operaci�n que trae Paises
	 * @param datos				unknown
	 * @return 					representaci&oacute;n en HTML de la informaci&oacute;n solicitada de los paises
	 * */
public StringBuffer traeDatos(String datos)
  {
	StringBuffer strOpcion=new StringBuffer("");
	String sqlQuery ="";

	EIGlobal.mensajePorTrace("MMC_Alta - traeDatos(): Busqueda de datos para " + datos, EIGlobal.NivelLog.INFO);

	if( "PAIS".equals(datos)){
	   sqlQuery = "SELECT CVE_PAIS, DESCRIPCION from COMU_PAISES order by DESCRIPCION";
	} else {
	   sqlQuery = "SELECT CVE_DIVISA, UPPER(DESCRIPCION) FROM COMU_MON_DIVISA ORDER BY UPPER(DESCRIPCION)";
	}
	Hashtable result=null;

	EI_Query BD= new EI_Query();

	result=BD.ejecutaQuery(sqlQuery);
	if(result==null){ return strOpcion; }
	String[] columns=new String[BD.getNumeroColumnas()];

	EIGlobal.mensajePorTrace("MMC_Alta - traeDatos(): Numero de datos. " + result.size() +" Columnas. " + BD.getNumeroColumnas(), EIGlobal.NivelLog.INFO);

	EIGlobal.mensajePorTrace("MMC_Alta - traeDatos(): Tama�o del arreglo datos " + columns.length, EIGlobal.NivelLog.DEBUG);

	if(BD.getNumeroColumnas()==2)
	  {
		for(int i=0;i<result.size();i++)
		  {
			columns=(String[])result.get(""+i);
			strOpcion.append("\n            <option class='componentesHtml' value='");
			strOpcion.append(columns[0]);
			strOpcion.append("'>");
			strOpcion.append(columns[1]);
			strOpcion.append("</option>");
		  }
	  }
	return strOpcion;
  }

/**
 * Operaci�n que trae Bancos
 * FWD IDS :::JMFR:::
 * 14-12-2016 Creacion Normativo SPID.
 * @return strOpcion de tipo StringBuffer.
 * */
private StringBuffer traeBancos()
 {
	StringBuffer strOpcion = new StringBuffer("@");
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancos(): INI", EIGlobal.NivelLog.INFO);
	MMC_SpidBO spidBO = new MMC_SpidBO();
	spidBO.consultaBancos(strOpcion);
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancos(): strBancos: " + strOpcion.toString(), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancos(): END", EIGlobal.NivelLog.INFO);
	return strOpcion;
  }

/**
 * Operaci�n que trae Bancos SPID para alta de cuentas
 * FWD IDS :::JMFR:::
 * 14-12-2016 Creacion Normativo SPID.
 * @return strOpcion de tipo StringBuffer.
 * */
private StringBuffer traeBancosSPID()
 {
	StringBuffer strOpcion = new StringBuffer("@");
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancosSPID(): INI", EIGlobal.NivelLog.INFO);
	MMC_SpidBO spidBO = new MMC_SpidBO();
	spidBO.consultaBancosSPID(strOpcion);
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancosSPID(): strBancos: " + strOpcion.toString(), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("MMC_Alta - traeBancosSPID(): END", EIGlobal.NivelLog.INFO);
	return strOpcion;
  }

/**
 * Operaci�n genera un cuadro est&aacute;tico en HTML que muestra el encabezado de errores sobre la importaci&oacute;n
 * @return 					representaci&oacute;n en HTML de la informaci&oacute;n de encabezado
 * */
 public String erroresHead()
	{

	  StringBuffer strH=new StringBuffer("");

	  strH.append("<html>");
	  strH.append("<head>\n<title>Estatus de Importaci&oacute;n</title>\n");
	  strH.append("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	  strH.append("</head>");
	  strH.append("<body bgcolor='white'>");
	  strH.append("<form>");
	  strH.append("<table border=0 width=420 class='textabdatcla' align=center>");
	  strH.append("<tr><th class='tittabdat'>Errores en archivo de importaci&oacute;n</th></tr>");
	  strH.append("<tr><td class='texencconbol' align=center>");
	  strH.append("<table border=0 align=center width='100%'><tr><td class='texencconbol' align=center width=60>");
	  strH.append("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
	  strH.append("<td class='texencconbol' align='center'><br>No se llevo a cabo la importaci&oacute;n del archivo seleccionado<br>ya que se encontraron los siguientes errores.<br><br>");
	  strH.append("</td></tr></table>");
	  strH.append("</td></tr>");
	  strH.append("<tr><td class='texencconbol'>");

	  return strH.toString();
	}

 /**
  * Operaci�n genera un cuadro est&aacute;tico en HTML que muestra el pie de p�gina de errores sobre la importaci&oacute;n
  * @return 					representaci&oacute;n en HTML de la informaci&oacute;n de pie de p&aacute;gina
  * */
 public String erroresFoot()
	{

	  StringBuffer strF=new StringBuffer("");

	  EIGlobal.mensajePorTrace("MMC_Alta - erroresFoot(): Escribiendo final de errores.", EIGlobal.NivelLog.INFO);

	  strF.append("</td></tr>");
	  strF.append("<tr><td class='texencconbol' ><br>Revise el <font color=blue>archivo</font> e intente nuevamente.<br><br>");
	  strF.append("</td></tr>");
	  strF.append("</table>");
	  strF.append("<table border=0 align=center >");
	  strF.append("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	  strF.append("</table>");
	  strF.append("</form>");
	  strF.append("</body>\n</html>");

	  return strF.toString();
	}

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
 /**
  * Operaci�n env&iacute;a la informaci&oacute;n de errores hacia el cliente web
  * @param Error 				informaci&oacute;n del error
  * @param param1				informaci&oacute;n para creaci&oacute;n del encabezado
  * @param param2				informaci&oacute;n para creaci&oacute;n del encabezado
  * @param param3				informaci&oacute;n para creaci&oacute;n del encabezado
  * @param request 				instancia de HTTPServletRequest
  * @param response 			instancia de HTTPServletResponse
  * @throws ServletException 	error en el servlet
  * @throws IOException 		fallo en operaciones de lectura de archivos/dispositivos
  * */
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_ATTRIBUTE);
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3=CLAVEAYUDA_PARAM;

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute( MENU_PRINCIPAL, session.getStrMenu() );
		request.setAttribute( ENCABEZADO, CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute(NUM_CONTRATO, (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute(NOM_CONTRATO, (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute(NUM_USUARIO, (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute(NOM_USUARIO, (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "MMC_Alta?Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

	/**
	 * Guarda par&aacute;metros en sesi&oacute;n
	 * @param request 				instancia de HTTPServletRequest
	 *
	 * */
	public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

        HttpSession session = request.getSession(false);
        if(session != null)
		{
			Map tmp = new HashMap();

			EIGlobal.mensajePorTrace("valida-> " + getFormParameter(request,VALIDA_PARAM), EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("Modulo-> " + getFormParameter(request,MODULO_PARAM), EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("nombreArchivoImp-> " + getFormParameter(request,NOMBRE_ARCHIVO_IMP), EIGlobal.NivelLog.DEBUG);
            EIGlobal.mensajePorTrace("totalRegistros-> " + getFormParameter(request,TOTAL_REGISTROS), EIGlobal.NivelLog.DEBUG);

			String value;

			value = (String) getFormParameter(request,VALIDA_PARAM);
			if(value != null && !value.equals(null)) {
				tmp.put(VALIDA_PARAM,getFormParameter(request,VALIDA_PARAM));
			}

			value = (String) getFormParameter(request,MODULO_PARAM);
			if(value != null && !value.equals(null)) {
            	tmp.put(MODULO_PARAM,getFormParameter(request,MODULO_PARAM));
			}

			value = (String) getFormParameter(request,NOMBRE_ARCHIVO_IMP);
			if(value != null && !value.equals(null)) {
            	tmp.put(NOMBRE_ARCHIVO_IMP,getFormParameter(request,NOMBRE_ARCHIVO_IMP));
			}

			value = (String) getFormParameter(request,TOTAL_REGISTROS);
			if(value != null && !value.equals(null)) {
            	tmp.put(TOTAL_REGISTROS,getFormParameter(request,TOTAL_REGISTROS));
			}

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()) {
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.INFO);
    }

 }