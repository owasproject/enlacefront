package mx.altec.enlace.servlets;
//Modificacon RMM 20021217 cerrado de archivos , cambio RandomAccessFile por BufferedReader
import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.lang.Number.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
//09/01/07
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class ChesBenefBaja extends BaseServlet{
    int numero_cuentas= 0;
    int salida= 0;
    //int i = 0;
    String fecha_hoy = "";
    String strFecha = "";
    String strDebug="";
    String VarFechaHoy="";
    short suc_opera = (short)787;
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		String usuario = "";
		String contrato = "";
		String clave_perfil = "";
		if(SesionValida( req, res )){
	    	  //TODO CU1131
		    /*
			 * 09/ENE/07
			 * VSWF-BMB-I
			 */
			if (Global.USAR_BITACORAS.equals("ON")){
				try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					if((req.getParameter(BitaConstants.FLUJO))!=null){
						bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
					}else{
						bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					}
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_MOV_MULTICHEQUE_ENTRA);
					bt.setContrato(contrato);
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch(SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
			}
		    /*
			 * VSWF-BMB-F
			 */

		//parche para que pueda jalar multicheque ----------------------
		if(req.getParameter("ventana").equals("10"))
			{
			consultaMulticheque unServlet = new consultaMulticheque();
			unServlet.init(getServletConfig());
			unServlet.defaultAction(req,res);
			return;
			}
		// -------------------------------------------------------------




			usuario = session.getUserID8();
		    contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
		    suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		    String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &ventana:"+ventana+"&", EIGlobal.NivelLog.INFO);
			if(ventana.equals("0"))
	        {
			   if (session.getFacultad(session.FAC_BENEF_NOREG))
			   {
				inicia(clave_perfil, contrato, usuario,  req, res );
               }
			   else
               {
                  req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
	              despliegaPaginaErrorURL("No tiene facultad para dar de alta beneficiarios","Baja de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Baja","s25960h","ChesBenefBaja?ventana=0",req,res);
			   }
		    }
			if(ventana.equals("1")){
			    baja(clave_perfil,contrato, usuario, req, res );
		    }

			if (ventana.equals("10"))
			{
               if(!SesionValida(req, res))
			   {
			     despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos","", req, res);
			     return;
			   }

               String diasInhabiles = diasInhabilesAnt();

               if (diasInhabiles.trim().length()==0)
			   {
			      despliegaPaginaError("Servicio no disponible por el momento","Movimientos","", req, res);
			      return;
			   }

		//----- Se capturan par�metros de la entidad invocadora

        	   String parAccion = (String)req.getParameter("Accion"); if(parAccion == null) parAccion = "";
		       String parTrama = (String)req.getParameter("Trama"); if(parTrama == null) parTrama = "";
		       String parOffset = (String)req.getParameter("Offset"); if(parOffset == null) parOffset = "0";
		       String parAncho = (String)req.getParameter("Ancho"); if(parAncho == null) parAncho = "";
		       String parTotal = (String)req.getParameter("Total"); if(parTotal == null) parTotal = "";
		       String parCuenta = (String)req.getParameter("Cuenta"); if(parCuenta == null) parCuenta = "";
	           String parFecha1 = (String)req.getParameter("Fecha1"); if(parFecha1 == null) parFecha1 = "";
		       String parFecha2 = (String)req.getParameter("Fecha2"); if(parFecha2 == null) parFecha2 = "";
		       String parArchivo = (String)req.getParameter("Archivo"); if(parArchivo == null) parArchivo = "";
		       String parArchTux = (String)req.getParameter("ArchTux"); if(parArchTux == null) parArchTux = "";
		       String parDivisa = (String)req.getParameter("Divisa"); if(parDivisa == null) parDivisa = "";
		       String parCodigo = (String)req.getParameter("Codigo"); if(parCodigo == null) parCodigo = "";

		// ----- -------------------------------------------------------------------------

		       int pantalla = 0;
		       String strAux = "";
		       String fechaIni = fechaInicial();
		       String fechaFin = fechaFinal();
		       String fechaHoy = formateaFecha(new GregorianCalendar());
		       String mensError = null;
		//----- -------------------------------------------------------------------------
		       session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

		       try
			   {
			      if(parAccion.equals("CONSULTAR"))
				  {
				     pantalla = 1;
				     strAux = parTrama;


				     if(parOffset.equals("0"))
				     {
				       //- Se construye la trama para enviar a Tuxedo
				       strAux = construyeTrama(strAux,req);
				       //- Se env�a la consulta
				       strAux = enviaConsuta(strAux,req);
				       //- se realiza copia remota
				       strAux = parArchTux = traeArchivo(strAux);
				       //- Se traduce el archivo y se cambia su formato para que se pueda exportar
				       parArchivo = traduceArchivo(strAux, parTrama,req);
				       //- Se leen los datos del archivo y se construye la trama para enviar
				       strAux = leeArchivo(strAux);
				       //- Se asignan los par�metros
				 	   parDivisa = strAux.substring(0,strAux.indexOf("@")); strAux = strAux.substring(strAux.indexOf("@")+1);
					   parCodigo = strAux.substring(0,strAux.indexOf("@")); strAux = strAux.substring(strAux.indexOf("@")+1);
					   parAncho = "" + Global.NUM_REGISTROS_PAGINA;
					   parTotal = "" + cuentaRegistros(strAux);
					   StringTokenizer tokens = new StringTokenizer(parTrama.substring(1),"|");
					   parCuenta = tokens.nextToken() + " " + tokens.nextToken();
					   parFecha1 = tokens.nextToken();
					   parFecha2 = tokens.nextToken();
					 }
           			 else
                     {
				       //- Se leen los datos del archivo y se construye la trama para enviar
					   strAux = leeArchivo(parArchTux);
					   strAux = strAux.substring(strAux.indexOf("@")+1);
					   strAux = strAux.substring(strAux.indexOf("@")+1);
				     }

				     //- Se 'recorta' la consulta para mostrar solo las cuentas de la p�gina seleccionada
				     strAux = recortaInfo(strAux,Integer.parseInt(parOffset),Integer.parseInt(parAncho));
				  }
			   }

		       catch(Exception e)
			   {
			      pantalla = 2;
			      mensError = e.getMessage();
			   }
		       finally
			   {;}

		       //----- Se asignan par�metros para el JSP
		       req.setAttribute("diasInhabiles",diasInhabiles);
		       req.setAttribute("FechaIni",fechaIni);
		       req.setAttribute("FechaFin",fechaFin);
		       req.setAttribute("FechaHoy",fechaHoy);
		       req.setAttribute("newMenu", session.getFuncionesDeMenu());
		       req.setAttribute("MenuPrincipal", session.getStrMenu());
		       req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos multicheque","Consultas &gt; Movimientos &gt; Multicheque","s27000h",req));
		       req.setAttribute("Trama",parTrama);
		       req.setAttribute("Datos",strAux);
		       req.setAttribute("Offset",parOffset);
		       req.setAttribute("Ancho",parAncho);
		       req.setAttribute("Total",parTotal);
		       req.setAttribute("Cuenta",parCuenta);
		       req.setAttribute("Fecha1",parFecha1);
		       req.setAttribute("Fecha2",parFecha2);
		       req.setAttribute("Archivo",parArchivo);
		       req.setAttribute("ArchTux",parArchTux);
		       req.setAttribute("Divisa",parDivisa);
		       req.setAttribute("Codigo",parCodigo);

		       //----- Se selecciona la pantalla de salida seg�n los resultados de la consulta
		       switch(pantalla)
			   {
			      case 0: evalTemplate("/jsp/ConsultaMultiFiltro.jsp", req, res); break;
			      case 1: evalTemplate("/jsp/ConsultaMultiResult.jsp", req, res); break;
			      case 2: despliegaMensaje(mensError,"Consulta de movimientos multicheque","Consultas &gt; Movimientos &gt; Multicheque", req, res); break;
			   }
		       debug("Saliendo de defaultAction");
			}
		}
	}

	/*----------------------------------------------------------------------*/
	public int inicia(String clave_perfil, String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		int EXISTE_ERROR = 0;
		EXISTE_ERROR =0;
		String cadenaBeneficiarios = listarBeneficiarios(usuario,contrato, clave_perfil, suc_opera);
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		//req.setAttribute("MenuPrincipal", session.getstrMenu());
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Baja de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Baja","s25960h",req) );

		if ( EXISTE_ERROR == 1 )
			despliegaPaginaError(cadenaBeneficiarios,"Baja de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Baja", req, res);
		else
			evalTemplate("/jsp/ChesBenefBaja.jsp", req, res );

		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);

		return 1;
	}
	/*-----------------------------------------------------------------------*/
	public int baja(String clave_perfil,String contrato, String usuario, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &inicia m�todo baja()&", EIGlobal.NivelLog.INFO);
		String cboCveBeneficiario=(String) req.getParameter("cboCveBeneficiario");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		String loc_trama = "";
		String glb_trama = "";
		String trama_entrada="";
		String trama_salida="";
		String cabecera = "1EWEB";
		String operacion = "BBEN";
		String nombre_benef="";
		String resultado="";
		String cuenta = "";
		String mensaje = "";
		int EXISTE_ERROR = 0;
		int primero=0;
		int ultimo=0;
		EXISTE_ERROR = 0;
		String benef =  trama_salida.trim();
		cuenta = cboCveBeneficiario.trim();
		//TuxedoGlobal tuxedoGlobal;
				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		fecha_hoy=ObtenFecha();

		if ( cboCveBeneficiario.trim().equals("") ){
			mensaje = "Debe seleccionar una cuenta.";
		}else{
			loc_trama = formatea_contrato(contrato) + "@" + cuenta + "@";
			trama_entrada = cabecera + "|" + clave_perfil + "|" + operacion + "|" + contrato + "|" + clave_perfil + "|" ;
			trama_entrada = trama_entrada + usuario + "|" + loc_trama ;

			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			try{
				//tuxedoGlobal = (TuxedoGlobal) session.getEjbTuxedo();
				Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
				trama_salida = (String) hs.get("BUFFER");
			}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
			} catch (Exception e) {}

			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
			if(trama_salida.startsWith("OK")){
				primero = trama_salida.indexOf( (int) '@');
				ultimo  = trama_salida.lastIndexOf( (int) '@');
				resultado = trama_salida.substring(primero+1, ultimo);
				mensaje = resultado;
			}else{
				mensaje = trama_salida;
			}

		}
		String cadenaBeneficiarios = listarBeneficiarios(usuario,contrato, clave_perfil, suc_opera);
        EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);
		req.setAttribute("FechaHoy", ObtenFecha(false));
        //req.setAttribute("MenuPrincipal", session.getstrMenu());
        req.setAttribute("Fecha", ObtenFecha(true));
        req.setAttribute("ContUser",ObtenContUser(req));
        req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
        req.setAttribute("mensaje_salida", mensaje);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Baja de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Baja","s25960h",req) );

	    despliegaPaginaErrorURL(mensaje,"Baja de Beneficiarios","Servicios &gt; Chequera Seguridad &gt; Mantenimiento de Beneficiarios &gt; Baja","s25960h","ChesBenefBaja?ventana=0",req,res);
        EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &termina m�todo baja()&", EIGlobal.NivelLog.INFO);
		return salida;
	}

	String formatea_contrato (String ctr_tmp){
		int i=0;
		String temporal="";
		String caracter="";

		for (i = 0 ; i < ctr_tmp.length() ; i++){
		    caracter = ctr_tmp.substring(i, i+1);
		    if (caracter.equals("-")){
		        //se elimina el gui�n
		    }else{
		    	temporal += caracter ;
		    }
	   }
	   return temporal;
	}
	/*--------------------------------------------------------*/
	public String listarBeneficiarios(String usuario, String contrato, String clave_perfil, short sucOpera){
		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &inicia m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		int    indice;
		String encabezado          = "";
		String tramaEntrada        = "";
		String tramaSalida         = "";
		String path_archivo        = "";
		String nombre_archivo      = "";
		String trama_salidaRedSrvr = "";
		String retCodeRedSrvr      = "";
		String registro            = "";
		String listaBeneficiarios  = "";
		String listaNombreBeneficiario = "";
		int EXISTE_ERROR = 0;
		//TuxedoGlobal tuxGlobal;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



		String medio_entrega  = "2EWEB";
		String tipo_operacion = "CBEN";
		encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
		encabezado += usuario + "|" + clave_perfil + "|";
		tramaEntrada = contrato + "@";
		tramaEntrada = encabezado + tramaEntrada;
		strDebug += "\ntecb :" + tramaEntrada;

		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

		try{
			//tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re ){
			re.printStackTrace();
		} catch(Exception e) {}
		strDebug += "\ntscb:" + tramaSalida;
		log("\ntscb:" + tramaSalida);
		path_archivo = tramaSalida;
		nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', 5);
		path_archivo = Global.DOWNLOAD_PATH + nombre_archivo;
		strDebug += "\npath_cb:" + path_archivo;
		log("\npath_cb:" + path_archivo);

        //Modificacion RMM 20021217
        java.io.BufferedReader fileSua = null;
		boolean errorFile=false;
		try
		 {

			// Copia Remota
			EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &error en remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

		    File drvSua = new File(path_archivo);
            //Modificacion RMM 20021217
		    fileSua = new java.io.BufferedReader(new java.io.FileReader(drvSua) );
		    trama_salidaRedSrvr = fileSua.readLine();
		    retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
		    if (retCodeRedSrvr.equals("OK"))
		     {
				EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
		       while((registro = fileSua.readLine()) != null){
		      	 	/////
					if( registro.trim().equals(""))
						registro = fileSua.readLine();
					if( registro == null )
						break;
		          listaBeneficiarios += "<option value=\"" + EIGlobal.BuscarToken(registro, ';', 1) + "\"> " + EIGlobal.BuscarToken(registro, ';', 1) +"   "+ EIGlobal.BuscarToken(registro, ';', 2) +"</option>\n";
		        }
		     }
		    fileSua.close();
		 }catch(Exception ioeSua )
		    {
				   EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  Excepci�n en listarBeneficiarios >>"+ioeSua.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
			   EXISTE_ERROR =1;
		       return ( ioeSua.getMessage() );
		    } finally{
                //Modificacion RMM 20021217
                if(null != fileSua){
                    try{
                        fileSua.close();
                    }catch(Exception e){}
                    fileSua = null;
                }
            }

		EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);

		return(listaBeneficiarios);
	}

//--- Construye la trama para enviar a Tuxedo --------------------------------------
    private String construyeTrama(String trama,HttpServletRequest req)
    {
	   HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");
       debug("Entrando a construyeTrama");
       String cuenta, fecha1, fecha2, folio1, folio2, cheque1, cheque2;

       //- Se obtienen los datos de 'trama'
       cuenta = trama.substring(1,trama.indexOf("|"));
       trama = trama.substring(trama.indexOf("|") + 1);
       trama = trama.substring(trama.indexOf("|") + 1);
       fecha1 = trama.substring(0,trama.indexOf("|"));
       trama = trama.substring(trama.indexOf("|") + 1);
       fecha2 = trama.substring(0,trama.indexOf("|"));
       trama = trama.substring(trama.indexOf("|") + 1);
       folio1 = trama.substring(0,trama.indexOf("|"));
       trama = trama.substring(trama.indexOf("|") + 1);
       folio2 = trama.substring(0,trama.indexOf("|"));
       trama = trama.substring(trama.indexOf("|") + 1);
       cheque1 = trama.substring(0,trama.indexOf("|"));
       cheque2 = trama.substring(trama.indexOf("|") + 1,trama.length()-1);
       //- Se formatean las fechas
       fecha1 = fecha1.substring(6) + "-" + fecha1.substring(3,5) + "-" + fecha1.substring(0, 2);
       fecha2 = fecha2.substring(6) + "-" + fecha2.substring(3,5) + "-" + fecha2.substring(0, 2);
       //- se construye la trama para Tuxedo
       trama = "2EWEB|" + session.getUserID8() + "|MULT|" + session.getContractNumber() + "|" + session.getUserID8()
	   + "|" + session.getUserProfile() + "|" + cuenta	+ "@" + fecha1 + "@" + fecha2
	   + "@" + folio1 + "@" + folio2 + "@" + cheque1 + "@" + cheque2 + "@|";
       debug("Saliendo de construyeTrama");
       return trama;
	}


    //--- Env�a 'trama' a Tuxedo y retorna el aviso de Tuxedo --------------------------
	private String enviaConsuta(String trama,HttpServletRequest req) throws Exception
	{
	   HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");
	   debug("Entrando a enviaConsulta");
	   Hashtable ht;
	   ServicioTux tuxedo = new ServicioTux();
	   //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	   ht = tuxedo.web_red(trama);
	   trama = (String)ht.get("BUFFER");
	   debug("Tuxedo regresa: " + trama);
	   if(!trama.substring(0,8).equals("MULT0000")) throw new Exception(trama.substring(9));
	   trama = Global.DOWNLOAD_PATH + session.getUserID8();
	   debug("Saliendo de enviaConsulta");
       EIGlobal.mensajePorTrace( "***ChesBenefBaja.class  trama de salida >>"+trama+"<<", EIGlobal.NivelLog.DEBUG);
	   return trama;
	}

	//--- Realiza copia remota del archivo de resgreso de Tuxedo -----------------------
	private String traeArchivo(String path) throws Exception
    {
       debug("Entrando a traeArchivo");
       ArchivoRemoto archRemoto = new ArchivoRemoto();
       path = path.substring(path.lastIndexOf('/')+1);
       if(!archRemoto.copia(path))
	   {
	      debug("Error en traeArchivo(): no pudo realizarse copia remota: " + path);
	      throw new Exception("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
	   }
          debug("Saliendo de traeArchivo");
	      return Global.DIRECTORIO_LOCAL + "/" + path;
	}

	//--- Lee los datos del archivo y construye la trama para enviar -------------------
	private String leeArchivo(String path) throws Exception
	{
	   debug("Entrando a leeArchivo");
	   String trama = "",
	   linea,
       fecha,
	   folio,
	   sucursal,
	   entidad,
	   cuenta,
	   cheque,
	   importe,
	   status,
	   causaDevol,
	   ref;
       StringBuffer bufferLinea;
       StringTokenizer bufferTokens;
       BufferedReader archivo;
       int car;
       archivo = new BufferedReader(new FileReader(path));
       linea = archivo.readLine();
       car = linea.indexOf("|");
       trama = linea.substring(0,car) + "@" + linea.substring(car+1,linea.length()-1);

       while((linea = archivo.readLine()) != null)
	   {
	      bufferLinea = new StringBuffer(linea);
	      while((car = bufferLinea.toString().indexOf("||")) != -1) bufferLinea.insert(car+1," ");
    	      bufferTokens = new StringTokenizer(bufferLinea.toString(),"|");
	          fecha = bufferTokens.nextToken();
	          folio = bufferTokens.nextToken();
	          sucursal = bufferTokens.nextToken();
			  entidad = bufferTokens.nextToken();
			  cuenta = bufferTokens.nextToken();
			  cheque = bufferTokens.nextToken();
			  importe = bufferTokens.nextToken();
			  status = bufferTokens.nextToken();
			  causaDevol = bufferTokens.nextToken();
			  ref = bufferTokens.nextToken();
			  fecha = fecha.substring(8,10) + "/" + fecha.substring(5,7) + "/" + fecha.substring(0, 4);
			  importe = formateaMoneda(importe);
			  if(causaDevol.equals(" ")) causaDevol = "&nbsp;";

			  trama += "@" + fecha + "|" + sucursal + "|" + entidad + "|" + cuenta + "|" + cheque + "|"
			  + importe + "|" + status + "|" + causaDevol + "|" + folio + "|" + ref;
	   }

			   archivo.close();
               debug("Saliendo de leeArchivo");
		       return trama;
	}

    //--- Traduce el archivo de Tuxedo para que se pueda exportar ----------------------
    private String traduceArchivo(String nombreArchivo, String trama, HttpServletRequest req) throws Exception
    {
   	   HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");
       debug("Entrando a traduceArchivo");
       BufferedReader archTux = new BufferedReader(new FileReader(nombreArchivo));
       String linea, propietario, cuenta;
       StringBuffer buffer;
       boolean primeraLinea = true;
       int car;
       EI_Exportar archivo;
       ArchivoRemoto archR;

       StringTokenizer tokens = new StringTokenizer(trama.substring(1),"|");
       cuenta = tokens.nextToken(); propietario = tokens.nextToken();

       nombreArchivo = session.getUserID8() + ".doc";
       archivo = new EI_Exportar(IEnlace.DOWNLOAD_PATH + nombreArchivo);
       archivo.creaArchivo();

       while((linea = archTux.readLine()) != null)
	   {
	      buffer = new StringBuffer(linea);
	      while((car = buffer.toString().indexOf("|")) != -1) buffer.replace(car,car+1,",");
	      if(primeraLinea)
		  {
		     primeraLinea = false;
		     buffer.append(propietario + "," + cuenta);
		  }
	      else
		     buffer.deleteCharAt(buffer.length()-1);
	         archivo.escribeLinea(buffer.toString()+"\n");
	   }

       archivo.cierraArchivo();
	   archR = new ArchivoRemoto();
	   if(!archR.copiaLocalARemoto(nombreArchivo,"WEB"))
	   {
	      debug("Error en traduceArchivo(): No se pudo crear archivo para exportar saldos");
	      throw new Exception("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
	   }
          debug("Saliendo de traduceArchivo");
	      return "/Download/" + nombreArchivo;
	}

    //--- Cuenta los registros dentro de la trama --------------------------------------
    private int cuentaRegistros(String trama) throws Exception
    {
       debug("Entrando a cuentaRegistros");
       int numReg = 0;
       StringTokenizer tokens = new StringTokenizer(trama,"@");
       while(tokens.hasMoreTokens()) {numReg++; tokens.nextToken();}
       debug("Saliendo de cuentaRegistros");
       return numReg;
    }

    //--- 'Recorta' la consulta para mostrar solo las cuentas de la p�gina seleccionada-
    private String recortaInfo(String trama, int offset, int numCtas)  throws Exception
    {
       debug("Entrando a recortaInfo");
       int actual = 0;
       StringTokenizer tokens = new StringTokenizer(trama,"@");
       trama = "";
       while(tokens.hasMoreTokens() && actual<offset) {actual++; tokens.nextToken();}
       while(tokens.hasMoreTokens() && actual<offset+numCtas) {actual++; trama += "@"+tokens.nextToken();}
       debug("Saliendo de recortaInfo");
       return trama;
    }

   	//--- Formatea una cadena que representa un n�mero que es una cant. de moneda ------
    private String formateaMoneda(String cadena)
    {
       debug("Entrando a formateaMoneda");
       cadena = "" + Double.parseDouble(cadena);
       if(cadena.indexOf(".") == cadena.length() - 2) cadena += "0";
       for(int a=cadena.length()-6;a>0;a-=3) cadena = cadena.substring(0,a) + "," + cadena.substring(a);
       debug("Saliendo de formateaMoneda");
       return "$" + cadena;
    }

    //---
    private String fechaInicial()
    {
       debug("Entrando a fechaInicial");
       GregorianCalendar fecIni = new GregorianCalendar();
       if(fecIni.get(Calendar.DATE) == 1) fecIni.add(Calendar.MONTH,-1); else fecIni.set(Calendar.DATE, 4);
       debug("Saliendo de fechaInicial");
       return formateaFecha(fecIni);
    }

    //---
    private String fechaFinal()
    {
       debug("Entrando a fechaFinal");
       GregorianCalendar fecFin = new GregorianCalendar();
       fecFin.add(Calendar.DATE,-1);
       debug("Saliendo de fechaFinal");
       return formateaFecha(fecFin);
    }

    //---
    private String formateaFecha(GregorianCalendar fecha)
    {
       debug("Entrando a formateaFecha");
       String dia, mes, anno;
       int m = fecha.get(Calendar.MONTH) + 1;
       dia = ((fecha.get(Calendar.DATE)<10)?"0":"") + fecha.get(Calendar.DATE);
       mes = ((m<10)?"0":"") + m;
       anno = "" + fecha.get(Calendar.YEAR);
       debug("Saliendo de formateaFecha");
       return dia + "/" + mes + "/" + anno;
    }

   	//---
    private void debug(String mensaje)
      {
       EIGlobal.mensajePorTrace("<DEBUG ChesBenefBaja.java> " + mensaje, EIGlobal.NivelLog.INFO);
	   }

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
    public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException    {
       HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute("session");
       EIGlobal Global = new EIGlobal(session , getServletContext() , this );
       String contrato_ = session.getContractNumber();

       if(contrato_==null) contrato_ ="";
         req.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
         req.setAttribute( "Error", mensaje );
//       request.setAttribute( "URL", "consultaMulticheque?Accion=NADA" );
         req.setAttribute( "URL", "ChesBenefBaja?ventana=10" );

         req.setAttribute( "MenuPrincipal", session.getStrMenu() );
         req.setAttribute( "newMenu", session.getFuncionesDeMenu());
         req.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s27000h",req));

         req.setAttribute( "NumContrato", contrato_ );
         req.setAttribute( "NomContrato", session.getNombreContrato() );
         req.setAttribute( "NumUsuario", session.getUserID8() );
         req.setAttribute( "NomUsuario", session.getNombreUsuario() );

         evalTemplate( "/jsp/EI_Mensaje.jsp" , req, res );
    }
}