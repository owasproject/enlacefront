package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;
import static mx.altec.enlace.utilerias.NomPreUtil.getValor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.EmpleadoBean;
import mx.altec.enlace.beans.LMXE;
import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.beans.NomPreCodPost;
import mx.altec.enlace.beans.TarjetasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.AsignacionEmplBO;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.AdmonRemesasDAO;
import mx.altec.enlace.dao.AsignacionEmplDAO;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;

import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Servlet implementation class AsignacionEmplServlet
 */
public class AsignacionEmplServlet extends BaseServlet {

	private static final long serialVersionUID = -6225627412976885211L;
	private static final String textoLog = AsignacionEmplServlet.class
			.getName();

	private static final String JSP_ASIGNACION_INDIVIDUAL = "/jsp/AsignacionEmplInd.jsp";

	private static final String JSP_CARGA_ARCHIVO = "/jsp/AsignacionEmplArchivo.jsp";

	private static final String JSP_ASIGNACIONEMPL = "/jsp/ResumenArchAsig.jsp";

	private static final String JSP_EXITO_ASIGNACION_INDIVIDUAL = "/jsp/ResultadoAsignacion.jsp";

	private static final String ENCABEZADO_MENU_ALTA = "Asignaci&oacute;n Individual";
	private static final String ENCABEZADO_MENU_ALTA_MASIVA = "Importaci&oacute;n Env&iacute;o";
	private static final String SUB_ENCABEZADO_MENU_ALTA = "Servicios > Cuenta Inmediata  > Asignaci&oacute;n Tarjeta-Empleado >"
			+ "Asignaci&oacute;n Individual";
	private static final String SUB_ENCABEZADO_MENU_ALTA_MASIVA = "Servicios > Cuenta Inmediata  > Asignaci&oacute;n Tarjeta-Empleado >"
			+ "Importaci&oacute;n Env&iacute;o";

	private static final String rutadeSubida = Global.DOWNLOAD_PATH; //Ruta HardCode
	private List<EmpleadoBean> lstEmpleados = new ArrayList<EmpleadoBean>();
	private String totalReg;
	private String correoContrato = "";



	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AsignacionEmplServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void defaultAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		EmpleadoBean empleadoBean = new EmpleadoBean();

		EIGlobal.mensajePorTrace("primer LOG ", NivelLog.INFO);

		HttpSession sess = request.getSession();
		boolean sesionvalida = SesionValida(request, response);

		EIGlobal.mensajePorTrace("La sesion es " + sesionvalida, NivelLog.INFO);

		if (sesionvalida) {
			BaseResource session = (BaseResource) sess.getAttribute("session");

			EIGlobal.mensajePorTrace("Obtiene SessionValida " + sesionvalida,
					NivelLog.INFO);

			MensajeUSR mensajeUSR = session.getMensajeUSR();
			String mailContaro = mensajeUSR.getEmailContrato();

			correoContrato = mailContaro;

			String opcion = "";
			String numContrato = "";
			String descContrato = "";
			String usuarioSesion = "";
			String clavePerfil = "";

			usuarioSesion = (session.getUserID() == null) ? "" : session
					.getUserID8();
			descContrato = (session.getNombreContrato() == null) ? "" : session
					.getNombreContrato();

			try {

				// Comienza validar diferentes de opciones
				// Si es la primera vez se carga la pagina por default
				opcion = request.getParameter("opcion");
				if (opcion == null) {
					EIGlobal.mensajePorTrace("Opcion es nula *****  JC "
							+ opcion, NivelLog.INFO);
					opcion = "1";
				}

				EIGlobal.mensajePorTrace("OBTIENE strMenu ******  JC " + opcion,
						NivelLog.INFO);
				String strMenu = (session.getstrMenu());
				strMenu = (strMenu != null ? strMenu : "");

				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("Encabezado", CreaEncabezado( ENCABEZADO_MENU_ALTA ,SUB_ENCABEZADO_MENU_ALTA,"RetComp01", request));

				if (opcion.equals("iniciarIndividual")) {
					EIGlobal.mensajePorTrace("Entra iniciarIndividual  JC "
							+ opcion, NivelLog.INFO);


					AsignacionEmplBO asignacionEmplBO = new AsignacionEmplBO();
					this.llenaBeanEmpleado(empleadoBean, request, session);
					empleadoBean.setEdoCivil("S");
					empleadoBean.setCveNacionalidad("MEXI");
					empleadoBean.setPaisNacimiento("MEXI");
					empleadoBean.setTipoDocto("IF");
					empleadoBean.setCveEstado("AS");
					empleadoBean.setCveEstadoNombre(" ");
					request.setAttribute("cveEstado", "AS");
					request.setAttribute("cveEstadoNombre", "");
					request.setAttribute("empleadoBean", empleadoBean);
					evalTemplate(JSP_ASIGNACION_INDIVIDUAL, request, response);
				}

				EIGlobal.mensajePorTrace("Opcion antes altaEmpIndividual**** "
						+ opcion, NivelLog.INFO);
				if (opcion.equals("altaEmpIndividual")) {

					EIGlobal
							.mensajePorTrace(
									"*****Entra altaEmpIndividual ***********"
											+ opcion, NivelLog.INFO);

					this.llenaBeanEmpleado(empleadoBean, request, session);




					EIGlobal
							.mensajePorTrace(
									textoLog
											+ "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
									EIGlobal.NivelLog.DEBUG);
					boolean solVal = ValidaOTP.solicitaValidacion(session
							.getContractNumber(), IEnlace.NOMINA);
					
					//CSA-Vulnerabilidad  Token Bloqueado-Inicio
					int estatusToken = obtenerEstatusToken(session.getToken());

					if (session.getValidarToken()
							&& session.getFacultad(session.FAC_VAL_OTP)
							&& estatusToken == 1 && solVal) {
						EIGlobal
								.mensajePorTrace(
										"El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion",
										EIGlobal.NivelLog.DEBUG);
						guardaParametrosEnSession(request, empleadoBean);
						ValidaOTP.validaOTP(request, response,
								IEnlace.VALIDA_OTP_CONFIRM);
					}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
								&& estatusToken == 2 && solVal){
						cierraSesionTokenBloqueado(session,request,response);
						
					}else{
						guardaParametrosEnSession(request, empleadoBean);
						this.exitoAltaIndividual(request, response);
					}
					//CSA-Vulnerabilidad  Token Bloqueado-Fin
				}

				if (opcion.equals("ExitoAltaIndividual")) {
					EIGlobal.mensajePorTrace(
							"Viene del TOKEN Valida Opcion ExitoAltaIndividual**** "
									+ opcion, NivelLog.INFO);
					this.exitoAltaIndividual(request, response);
				}

				if (opcion.equals("ExitoAltaMasiva")) {
					EIGlobal.mensajePorTrace(
							"Viene del TOKEN Valida Opcion ExitoAltaIndividual**** "
									+ opcion, NivelLog.INFO);
					this.exitoAltaMasiva(request, response);
				}

				if (opcion.equals("codigoPostalIndividual")) {

					AsignacionEmplBO asignacionEmplBO = new AsignacionEmplBO();
					this.llenaBeanEmpleado(empleadoBean, request, session);
					this.consultaCodigoPostal(request, response, sess,
							empleadoBean);
				}

				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("Encabezado", CreaEncabezado( ENCABEZADO_MENU_ALTA_MASIVA ,SUB_ENCABEZADO_MENU_ALTA_MASIVA,"RetComp01", request));


				if (opcion.equals("iniciarArchivo")) {
					evalTemplate(JSP_CARGA_ARCHIVO, request, response);

				}


				if (opcion.equals("ArchivoValido")) {

					EIGlobal.mensajePorTrace("Entro a Archivo valido",
							NivelLog.INFO);

					String sFile = "";
					String mensajes = null;
					AsignacionEmplBO asignacionBo = new AsignacionEmplBO();
					request.setAttribute("MensajeValidaciones", null);
					sFile = getFormParameter(request, "fileName");
					request.setAttribute("rutaArchivo", sFile);
					request.getSession().setAttribute("nombreArchivoBita", sFile);

					File fArchivo = new File((rutadeSubida + "/" + sFile).intern());

					request.setAttribute("MensajeValidaciones", null);
					request.removeAttribute("MensajeValidaciones");
					if(!asignacionBo.fileIsEmpty(fArchivo)){
						if(Global.VALIDA_NOMINA_INMEDIATA.trim().equals("ON"))
							asignacionBo.generaInfoTarjetas(request, response);
					}

					EIGlobal.mensajePorTrace("AsignacionEmplServlet :: ArchivoValido :: se ejecuta validaArchivo(fArchivo, session.getContractNumber())", NivelLog.INFO);
					HttpSession session1 = request.getSession();
					BaseResource session2 = (BaseResource) sess.getAttribute("session");
					mensajes = asignacionBo.validaArchivo(fArchivo, session2.getContractNumber());
					EIGlobal.mensajePorTrace("AsignacionEmplServlet :: ArchivoValido ::Size mensajes "+mensajes.length(), NivelLog.INFO);

					if (mensajes.length() == 0) {
						EIGlobal.mensajePorTrace("Dentro IF", NivelLog.INFO);
						lstEmpleados = new ArrayList<EmpleadoBean>();

						//llamada
						EIGlobal.mensajePorTrace("AsignacionEmplServlet :: ArchivoValido :: Llamada a obtenerListEmpl(fArchivo)", NivelLog.INFO);
						lstEmpleados=asignacionBo.obtenerListEmpl(fArchivo);

						totalReg = String.valueOf(lstEmpleados.size());

						request.setAttribute("totalRegistros", totalReg);
						request.setAttribute("bandera", "confirmacion");
						evalTemplate(JSP_ASIGNACIONEMPL, request, response);

					} else {
						EIGlobal.mensajePorTrace("<<<<<<<< 0 >>>>>>>" + mensajes,
								NivelLog.INFO);
						request.setAttribute("MensajeValidaciones", mensajes);
						evalTemplate(JSP_CARGA_ARCHIVO, request, response);
					}

				}

				if (opcion.equals("ConfirmarToken")) {
					request.setAttribute("bandera", "noConfirmacion");
					evalTemplate(JSP_ASIGNACIONEMPL, request, response);
				}

				if (opcion.equals("EnviarToken")) {
					EIGlobal.mensajePorTrace(
							"*****Entra altaEmpMasiva***********" + opcion,
							NivelLog.INFO);

					EIGlobal
							.mensajePorTrace(
									textoLog
											+ "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP",
									EIGlobal.NivelLog.DEBUG);
					boolean solVal = ValidaOTP.solicitaValidacion(session
							.getContractNumber(), IEnlace.NOMINA);
					//CSA-Vulnerabilidad  Token Bloqueado-Inicio
					int estatusToken = obtenerEstatusToken(session.getToken());
					
					if (session.getValidarToken()
							&& session.getFacultad(session.FAC_VAL_OTP)
							&& estatusToken == 1 && solVal) {
						EIGlobal
								.mensajePorTrace(
										"El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion",
										EIGlobal.NivelLog.DEBUG);
						guardaParametrosArchivo(request, lstEmpleados, totalReg);
						ValidaOTP.validaOTP(request, response,
								IEnlace.VALIDA_OTP_CONFIRM);
					}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP)
								&& estatusToken == 2 && solVal){
						cierraSesionTokenBloqueado(session,request,response);
					}else{						
						guardaParametrosArchivo(request, lstEmpleados, totalReg);
						this.exitoAltaMasiva(request, response);
					}
					//CSA-Vulnerabilidad  Token Bloqueado-Fin

				}

				if (opcion.equals("CancelaImportacion")) {
					EIGlobal.mensajePorTrace("Se ha cancelado la importacion",
							NivelLog.INFO);
					evalTemplate(JSP_CARGA_ARCHIVO, request, response);
				}

				EIGlobal.mensajePorTrace("Sube a sesion  empleadoBean"
						+ empleadoBean, NivelLog.INFO);
				request.setAttribute("empleadoBean", empleadoBean);

			} catch (Exception e) {
				e.printStackTrace();
				EIGlobal.mensajePorTrace("***EXCEPCION**** " + e.getMessage(),
						NivelLog.INFO);

			}

		} else {
			EIGlobal.mensajePorTrace("La sesion no es valida", NivelLog.INFO);
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}

	}

	/**
	 * Metodo para recuperar los valores al retornar del token
	 *
	 * @param request
	 * @param response
	 */
	private void exitoAltaMasiva(HttpServletRequest request,
			HttpServletResponse response) {

		try {

			EIGlobal.mensajePorTrace(
					"SIG  request.getSession().getAttribute(parametros)",
					NivelLog.INFO);
			Map<Object, Object> tmp = (Map<Object, Object>) request
					.getSession().getAttribute("parametros");
			EIGlobal.mensajePorTrace("CAST ListaToken", NivelLog.INFO);
			List<EmpleadoBean> lstEmpleados = (List<EmpleadoBean>) request
					.getAttribute("lstEmpleadosToken");
			String totalReg = (String) request
					.getAttribute("totalRegEmpleados");
			AsignacionEmplBO asignacionEmplBO = new AsignacionEmplBO();

			HttpSession sess = request.getSession();

			BaseResource session = (BaseResource) sess.getAttribute("session");

			String descSesion = (session.getNombreContrato() == null) ? ""
					: session.getNombreContrato();

			String numContrato = (session.getContractNumber() == null) ? ""
					: session.getContractNumber();
			String cveUsuario = (session.getUserID() == null) ? "" : session
					.getUserID8();

			String idFolio = asignacionEmplBO.insertarArchivo(lstEmpleados,
					descSesion, totalReg, numContrato, cveUsuario, request,
					response);

			request.setAttribute("idFolio", idFolio);
			request.setAttribute("tiporegreso", "AA");

			evalTemplate(JSP_EXITO_ASIGNACION_INDIVIDUAL, request, response);

		} catch (ServletException e) {
			EIGlobal.mensajePorTrace("***EXCEPCION**** " + e.getMessage(),
					NivelLog.INFO);
			e.printStackTrace();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("***EXCEPCION**** " + e.getMessage(),
					NivelLog.INFO);
			e.printStackTrace();
		}

	}

	/***
	 * Metodo para despues del regreso de token para registro individual
	 *
	 * @param request
	 * @param response
	 */
	private void exitoAltaIndividual(HttpServletRequest request,
			HttpServletResponse response) {
		boolean huboError = false;
		String mensajeError = null;
		MQQueueSession mqsess = null;
		try {

			Map<Object, Object> tmp = (Map<Object, Object>) request
					.getSession().getAttribute("parametros");

			EIGlobal.mensajePorTrace("CAST beanTopken", NivelLog.INFO);
			EmpleadoBean beanTopken = (EmpleadoBean) request
					.getAttribute("empleadoBeanToken");

			AsignacionEmplBO asignacionEmplBO = new AsignacionEmplBO();

			EIGlobal.mensajePorTrace("**********SIG HttpSession sess",
					NivelLog.INFO);
			HttpSession sess = request.getSession();

			EIGlobal.mensajePorTrace("**********SIG BaseResource session",
					NivelLog.INFO);
			BaseResource session = (BaseResource) sess.getAttribute("session");

			EIGlobal.mensajePorTrace("**********SIG descSesion session",
					NivelLog.INFO);
			String descSesion = (session.getNombreContrato() == null) ? ""
					: session.getNombreContrato();

			EIGlobal
			.mensajePorTrace(
					"****CVE ESTADO ***********"
							+ beanTopken.getCveEstado(), NivelLog.INFO);

			//SE REALIZA VALIDACI�N POR MEDIO DE LA TRANSACCI�N LMXE
			//PARA VERIFICAR SITUACI�N DE LA TARJETA A ASIGNAR.
			//INICIO
			if(Global.VALIDA_NOMINA_INMEDIATA.trim().equals("ON")){
				//Declaraci�n de variables.
				LMXE entradaLmxe = new LMXE();
				LMXE salidaLmxe  = new LMXE();
				TarjetasBean beanLmxe = new TarjetasBean();
				AdmonRemesasDAO daoLmxe = new AdmonRemesasDAO();
				AsignacionEmplDAO daoConsulta = AsignacionEmplDAO.getSingleton();

				//Inicializaci�n de datos de entrada.
				entradaLmxe.setNumeroRemesa("999999999999");
				entradaLmxe.setCtroDistribucion("999999");
				entradaLmxe.setTarjeta(beanTopken.getNumTarjeta());
				entradaLmxe.setContratoEnlace(session.getContractNumber());

				EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Entidad->" + entradaLmxe.getEntidad(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Remesa ->" + entradaLmxe.getNumeroRemesa(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Centro Distribucion->" + entradaLmxe.getCtroDistribucion(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Tarjeta->" + entradaLmxe.getTarjeta(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Contrato->" + entradaLmxe.getContratoEnlace(), EIGlobal.NivelLog.INFO);

				//Inicializando conexi�n a MQ
				try{
					mqsess = new MQQueueSession(NP_JNDI_CONECTION_FACTORY,
							NP_JNDI_QUEUE_RESPONSE, NP_JNDI_QUEUE_REQUEST);
					daoLmxe.setMqsess(mqsess);
			    }catch (Exception e){
			    	StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al crear la conexion", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea de truene->" + lineaError[0]
								 			, EIGlobal.NivelLog.DEBUG);
			    }finally{
			    	try{
			    		mqsess.close();
			    	}catch(Exception e1) {
				    	StackTraceElement[] lineaError;
						lineaError = e1.getStackTrace();
						EIGlobal.mensajePorTrace("Error al cerrar la conexion MQ...", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
												+ e1.getMessage()
					               				+ "<DATOS GENERALES>"
									 			+ "Linea de truene->" + lineaError[0]
									 			, EIGlobal.NivelLog.DEBUG);
			    	}
			    }

				//Se realiza consulta para conocer la situaci�n de la tarjeta.
				salidaLmxe = daoLmxe.consultaDetalleRemesa(entradaLmxe);

				//Se realiza la validaci�n correspondiente.
				if(salidaLmxe.getListaTarjetas()!=null){
					if(salidaLmxe.getListaTarjetas().size()==1){
						beanLmxe = (TarjetasBean)salidaLmxe.getListaTarjetas().get(0);
						if(beanLmxe.getEstadoTarjeta().trim().equals("RECIBIDA")){
							EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: La tarjeta se puede asignar."
									, EIGlobal.NivelLog.INFO);
						}else{
							mensajeError = "No es posible asignar la tarjeta, no esta disponible.";
							huboError = true;
						}
					}else{
						mensajeError = "La tarjeta no es valida.";
						huboError = true;
					}
				}else{
					mensajeError = "La tarjeta no es valida.";
					huboError = true;
				}

				if(!huboError){
					EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: No hubo errores en PAMPA, " +
							"validar si la tarjeta ya esta recibida."
							, EIGlobal.NivelLog.INFO);
					try{
						daoConsulta.initTransac();
						if(daoConsulta.verificaTarjetaAsignacion(session.getContractNumber(), beanTopken.getNumTarjeta())){
							EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: La tarjeta ya se encuentra Enviada"
									, EIGlobal.NivelLog.INFO);
							mensajeError = "La tarjeta ya fue enviada el d�a de hoy.";
							huboError = true;
						}
					}catch(Exception e){
						EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: Error al consultar la tarjeta-> " +
								e.getMessage()
								, EIGlobal.NivelLog.INFO);
						mensajeError = "Error al validar la tarjeta.";
						huboError = true;
					}finally{
						daoConsulta.closeTransac();
					}
				}
				if(!huboError){
					beanTopken.setRemesa("" + Integer.parseInt(beanLmxe.getFechaUltimaMod()));
					EIGlobal.mensajePorTrace("AsignacionEmplServlet::exitoAltaIndividual:: No hubo errores en PAMPA, la remesa es->" + beanTopken.getRemesa(), EIGlobal.NivelLog.INFO);
				}
			}
			//FIN

			if(!huboError){
				String idFolio = asignacionEmplBO.invocaAsignacionTarjeta(
						beanTopken, descSesion, request, response);
				request.setAttribute("empleadoBean", beanTopken);
				request.setAttribute("idFolio", idFolio);
			}else{
				request.setAttribute("mensajeError",mensajeError);
			}
			request.setAttribute("tiporegreso", "AI");
			evalTemplate(JSP_EXITO_ASIGNACION_INDIVIDUAL, request, response);

		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo para obtener la colonia por el codigo postal
	 *
	 * @param request
	 * @param response
	 * @param sess
	 * @param empleadoBean
	 */
	private void consultaCodigoPostal(HttpServletRequest request,
			HttpServletResponse response, HttpSession sess,
			EmpleadoBean empleadoBean) {

		try {

			String codPostal = getFormParameter(request, "codPostal");
			String colonias = "";
			String delegacion = "";
			String ciudad = "";
			String estado = "";
			String cveEstadoCP = "";

			EIGlobal.mensajePorTrace("Entramos consultaCodigoPostal JC"
					+ request.getParameter("codPostal"), NivelLog.INFO);

			NomPreEmpleadoDAO empleadoDAO = new NomPreEmpleadoDAO();
			NomPreCodPost npcodpostal = empleadoDAO
					.consultarCodigoPostal(codPostal);

			if (npcodpostal != null && npcodpostal.getDetalle() != null
					&& npcodpostal.isCodExito()) {

				EIGlobal.mensajePorTrace("Existe codigo postal " + codPostal,
						NivelLog.INFO);
				ListIterator<?> li = npcodpostal.getDetalle().listIterator();

				while (li.hasNext()) {
					NomPreCodPost npCodPost = (NomPreCodPost) li.next();
					colonias += "<option CLASS='tabmovtex'>"
							+ npCodPost.getColonia() + "</option> \n";
					delegacion = npCodPost.getDelegacion();
					ciudad = npCodPost.getCiudad();
					estado = npCodPost.getEstado();
					cveEstadoCP = npCodPost.getCodAplicacion();

				}
			} else {
				EIGlobal.mensajePorTrace(
						"No Existe Codigo Posta codigo postal " + codPostal,
						NivelLog.INFO);
				sess.setAttribute("MensajeLogin01",
						"cuadroDialogo(\"No existe c&oacute;digo postal: "
								+ codPostal + "\", 1)");

				request.setAttribute("mensaje", "No existe c&oacute;digo postal: "+ codPostal);

				if (getFormParameter(request, "dirColoniaEmp") != null) {
					colonias += "<option CLASS='tabmovtex'>"
							+ getFormParameter(request, "dirColoniaEmp")
							+ "</option> \n";
				} else {
					colonias = "";
				}
				if (getFormParameter(request, "dirDelegacionEmp") != null) {
					delegacion = (String) request
							.getParameter("dirDelegacionEmp");
				} else {
					delegacion = "";
				}
				if (getFormParameter(request, "dirCiudadEmp") != null) {
					ciudad = (String) request.getParameter("dirCiudadEmp");
				} else {
					ciudad = "";
				}
				if (getFormParameter(request, "dirEstadoEmp") != null) {
					estado = (String) request.getParameter("dirEstadoEmp");
				} else {
					estado = "";
				}
			}



			request.setAttribute("tabCodPostal", "1");
			request.setAttribute("codPostal", codPostal);
			request.setAttribute("colonia", colonias);
			request.setAttribute("delegacion", ciudad);
			request.setAttribute("ciudad", delegacion);
			request.setAttribute("cveEstadoNombre", estado);
			request.setAttribute("cveEstado", empleadoBean.getCveEstado());


			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_ENTRA);

			NomPreUtil.bitacoriza(request, null, null, true, bt);

			request.setAttribute("empleadoBean", empleadoBean);
			evalTemplate(JSP_ASIGNACION_INDIVIDUAL, request, response);
		} catch (ServletException e) {
			EIGlobal.mensajePorTrace(
					"ServletException --> consultaCodigoPostal JC"
							+ e.getMessage(), NivelLog.INFO);

		} catch (IOException e) {

			EIGlobal.mensajePorTrace("IOException --> consultaCodigoPostal JC"
					+ e.getMessage(), NivelLog.DEBUG);
		}

	}

	/**
	 * MEtodo encargado de mapear los datos que capturaron en el JSP hacia el
	 * bean
	 *
	 * @param empleadoBean
	 *            java Bean
	 * @param request
	 */
	private void llenaBeanEmpleado(EmpleadoBean empleadoBean,
			HttpServletRequest request, BaseResource session) {
		EIGlobal.mensajePorTrace("Entra llenaBeanEmpleado  JC ", NivelLog.INFO);

		empleadoBean.setContrato((session.getContractNumber() == null) ? ""
				: session.getContractNumber());
		empleadoBean.setSecuencia(request.getParameter("secuencia"));
		empleadoBean.setNoSecuencia(request.getParameter("noSecuencia"));
		empleadoBean
				.setNoEmpleado(request.getParameter("noEmpleado") == null ? ""
						: request.getParameter("noEmpleado"));
		empleadoBean
				.setAppPaterno(request.getParameter("appPaterno") == null ? ""
						: request.getParameter("appPaterno"));
		empleadoBean
				.setAppMaterno(request.getParameter("appMaterno") == null ? ""
						: request.getParameter("appMaterno"));
		empleadoBean
				.setNombreEmpleado(request.getParameter("nombreEmpleado") == null ? ""
						: request.getParameter("nombreEmpleado"));
		empleadoBean.setRfc(request.getParameter("rfc") == null ? "" : request
				.getParameter("rfc"));
		empleadoBean
				.setHomoclave(request.getParameter("homoclave") == null ? ""
						: request.getParameter("homoclave"));
		empleadoBean
				.setTipoDocto(request.getParameter("tipoDocto") == null ? ""
						: request.getParameter("tipoDocto"));
		empleadoBean.setNoIdentificacion(request
				.getParameter("noIdentificacion") == null ? "" : request
				.getParameter("noIdentificacion"));
		empleadoBean.setSexo(request.getParameter("sexo") == null ? ""
				: request.getParameter("sexo"));
		empleadoBean.setFechaNac(request.getParameter("fechaNac") == null ? ""
				: request.getParameter("fechaNac"));
		empleadoBean
				.setCveNacionalidad(request.getParameter("cveNacionalidad") == null ? ""
						: request.getParameter("cveNacionalidad"));
		empleadoBean
				.setPaisNacimiento(request.getParameter("paisNacimiento") == null ? ""
						: request.getParameter("paisNacimiento"));
		empleadoBean
				.setCveEstado(request.getParameter("cveEstado") == null ? ""
						: request.getParameter("cveEstado"));

		empleadoBean
				.setCveEstadoNombre(request.getParameter("cveEstadoNombre") == null ? ""
						: request.getParameter("cveEstadoNombre"));

		empleadoBean.setEntidadExtranjera(request
				.getParameter("entidadExtranjera") == null ? "" : request
				.getParameter("entidadExtranjera"));
		empleadoBean.setEdoCivil(request.getParameter("edoCivil") == null ? ""
				: request.getParameter("edoCivil"));
		empleadoBean.setCalle(request.getParameter("calle") == null ? ""
				: request.getParameter("calle"));
		empleadoBean
				.setNoExterior(request.getParameter("noExterior") == null ? ""
						: request.getParameter("noExterior"));
		empleadoBean
				.setNoInterior(request.getParameter("noInterior") == null ? ""
						: request.getParameter("noInterior"));
		empleadoBean.setColonia(request.getParameter("colonia") == null ? ""
				: request.getParameter("colonia"));

		empleadoBean.setColoniaExtranjero(request
				.getParameter("coloniaExtranjero") == null ? "" : request
				.getParameter("coloniaExtranjero"));

		empleadoBean
				.setCodPostal(request.getParameter("codPostal") == null ? ""
						: request.getParameter("codPostal"));
		empleadoBean
				.setCodPostalTrab(request.getParameter("codPostalTrab") == null ? ""
						: request.getParameter("codPostalTrab"));
		empleadoBean.setLada(request.getParameter("lada") == null ? ""
				: request.getParameter("lada"));
		empleadoBean.setTelefono(request.getParameter("telefono") == null ? ""
				: request.getParameter("telefono"));
		empleadoBean.setSucursal(request.getParameter("sucursal") == null ? ""
				: request.getParameter("sucursal"));
		empleadoBean
				.setCodigoCliente(request.getParameter("codigoCliente") == null ? ""
						: request.getParameter("codigoCliente"));
		empleadoBean
				.setNumTarjeta(request.getParameter("numTarjeta") == null ? ""
						: request.getParameter("numTarjeta"));
		empleadoBean
				.setNumCuenta(request.getParameter("numCuenta") == null ? ""
						: request.getParameter("numCuenta"));
		empleadoBean.setEstatus(request.getParameter("estatus") == null ? ""
				: request.getParameter("estatus"));
		empleadoBean.setRemesa(request.getParameter("remesa"));
		empleadoBean.setReactivacion(request.getParameter("reactivacion"));
		empleadoBean.setEnviarCatalogo(request.getParameter("enviarCatalogo"));
		empleadoBean.setUsuarioSession((session.getUserID() == null) ? ""
				: session.getUserID8());
		empleadoBean.setCorreoContrato(correoContrato);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getContrato() "
						+ empleadoBean.getContrato(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getSecuencia() "
						+ empleadoBean.getSecuencia(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNoSecuencia() "
						+ empleadoBean.getNoSecuencia(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNoEmpleado()"
						+ empleadoBean.getNoEmpleado(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getAppPaterno()"
						+ empleadoBean.getAppPaterno(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getAppMaterno()"
						+ empleadoBean.getAppMaterno(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNombreEmpleado()"
						+ empleadoBean.getNombreEmpleado(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("VALOR BEAN EMPLEADOS empleadoBean.getRfc()"
				+ empleadoBean.getRfc(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getHomoclave()"
						+ empleadoBean.getHomoclave(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getTipoDocto()"
						+ empleadoBean.getTipoDocto(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNoIdentificacion() "
						+ empleadoBean.getNoIdentificacion(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("VALOR BEAN EMPLEADOS empleadoBean.getSexo()"
				+ empleadoBean.getSexo(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getFechaNac()"
						+ empleadoBean.getFechaNac(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getCveNacionalidad()"
						+ empleadoBean.getCveNacionalidad(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getPaisNacimiento()"
						+ empleadoBean.getPaisNacimiento(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getCveEstado()"
						+ empleadoBean.getCveEstado(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getEntidadExtranjera() "
						+ empleadoBean.getEntidadExtranjera(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getEdoCivil() "
						+ empleadoBean.getEdoCivil(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("VALOR BEAN EMPLEADOS empleadoBean.getCalle()"
				+ empleadoBean.getCalle(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNoExterior() "
						+ empleadoBean.getNoExterior(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNoInterior() "
						+ empleadoBean.getNoInterior(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getColonia()"
						+ empleadoBean.getColonia(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getCodPostal()"
						+ empleadoBean.getCodPostal(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getCodPostalTrab()"
						+ empleadoBean.getCodPostalTrab(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("VALOR BEAN EMPLEADOS empleadoBean.getLada() "
				+ empleadoBean.getLada(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getTelefono()"
						+ empleadoBean.getTelefono(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getSucursal()"
						+ empleadoBean.getSucursal(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getCodigoCliente() "
						+ empleadoBean.getCodigoCliente(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS  empleadoBean.getNumTarjeta()"
						+ empleadoBean.getNumTarjeta(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getNumCuenta() "
						+ empleadoBean.getNumCuenta(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS  empleadoBean.getEstatus()"
						+ empleadoBean.getEstatus(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS	empleadoBean.getRemesa()"
						+ empleadoBean.getRemesa(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS empleadoBean.getReactivacion() "
						+ empleadoBean.getReactivacion(), NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(
				"VALOR BEAN EMPLEADOS  empleadoBean.getEnviarCatalogo()"
						+ empleadoBean.getEnviarCatalogo(), NivelLog.DEBUG);

	}

	/**
	 * Metodo para guardar los atributos en sesion para cuando se va al token
	 *
	 * @param request
	 * @param empleadoBean
	 */
	public void guardaParametrosEnSession(HttpServletRequest request,
			EmpleadoBean empleadoBean) {

		empleadoBean.setCorreoContrato(correoContrato);
		EIGlobal.mensajePorTrace(textoLog
				+ "Dentro de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
		request.getSession()
				.setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);

		if (session != null) {
			Map<Object, Object> tmp = new HashMap<Object, Object>();

			tmp.put("opcion", "ExitoAltaIndividual");

			request.setAttribute("empleadoBeanToken", empleadoBean);

			// tmp.put("Appmaterno", empleadoBean.getAppMaterno());

			EIGlobal.mensajePorTrace(textoLog
					+ " GUARDA EN MAPA empleadoBeanToken",
					EIGlobal.NivelLog.DEBUG);

			session.setAttribute("parametros", tmp);

			tmp = new HashMap<Object, Object>();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog
				+ "Saliendo de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Metodo para guardar los atributos en sesion para cuando se va al token
	 *
	 * @param request
	 * @param lstEmpleados
	 * @param totalReg
	 */

	private void guardaParametrosArchivo(HttpServletRequest request,
			List<EmpleadoBean> lstEmpleados, String totalReg) {
		EIGlobal.mensajePorTrace(textoLog
				+ "Dentro de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
		request.getSession()
				.setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);

		if (session != null) {
			Map<Object, Object> tmp = new HashMap<Object, Object>();

			tmp.put("opcion", "ExitoAltaMasiva");
			lstEmpleados.get(0).setCorreoContrato(correoContrato);
			request.setAttribute("lstEmpleadosToken", lstEmpleados);
			request.setAttribute("totalRegEmpleados", totalReg);

			EIGlobal.mensajePorTrace(textoLog
					+ " GUARDA EN MAPA LST DE EMPLEADOS",
					EIGlobal.NivelLog.DEBUG);

			session.setAttribute("parametros", tmp);

			tmp = new HashMap<Object, Object>();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog
				+ "Saliendo de guardaParametrosEnSession",
				EIGlobal.NivelLog.DEBUG);
	}


}