/*Banco Santander Mexicano
  Clase MIPD_plazo  Contiene las funciones para la presentacion de las pantallas de plazo dolares y la ejecucion de ellos
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  creaci�n:Paulina Ventura Agustin 20/07/2001 -
  */

package mx.altec.enlace.servlets;

import java.lang.reflect.*;
import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class MIPD_plazo extends BaseServlet
{
	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		EIGlobal.mensajePorTrace("***MIPD_plazo.class Entrando a execute", EIGlobal.NivelLog.INFO);

		/************* Modificacion para la sesion ***************/
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		boolean sesionvalida = SesionValida(request, response);

		try
		{
			if(sesionvalida&&session.getFacultad(session.FAC_INVERSION_DISPONIBLE) )
			{
				if( ses.getAttribute("MenuPrincipal")!=null )
					ses.removeAttribute("MenuPrincipal");
  					ses.setAttribute("MenuPrincipal", session.getStrMenu());
				if( ses.getAttribute("newMenu")!=null )
					ses.removeAttribute("newMenu");
					ses.setAttribute("newMenu", session.getFuncionesDeMenu());
				if( ses.getAttribute("Encabezado")!=null )
					ses.removeAttribute("Encabezado");
					ses.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo D&oacute;lares","Operaciones Plazo D&oacute;lares",request));
////////////////////////////////////////////////////////////Para las paginas de error
  				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n a Plazo D&oacute;lares","Operaciones Plazo D&oacute;lares",request));

				String funcion_llamar = request.getParameter("ventana");
				EIGlobal.mensajePorTrace("***MIPD_plazo.class funcion_llamar "+funcion_llamar, EIGlobal.NivelLog.INFO);
				if (funcion_llamar.equals("0"))
					pantalla_inicial_plazo(request, response);
				else if (funcion_llamar.equals("2"))
					ejecucion_operaciones_plazo(request, response);
			}
			else if (sesionvalida)
			{
				despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Inversi&oacute;n a Plazo D&oacute;lares","Operaciones Plazo D&oacute;lares", request, response );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}

    static protected String _defaultTemplate ="enlaceMig/plazo";

    /*Modificacion para integracion
    public int pantalla_inicial_plazo( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("***MIPD_plazo.class Entrando a pantalla_inicial_plazo ", EIGlobal.NivelLog.INFO);

		String contrato = "";
		String usuario = "";
		String clave_perfil = "";
		String cadena_getFacCOpProg = "";

		String campos_fecha = "";
		String cadena_retorno_cuentas = "";
		String Cuentas_operar[] = null;
		String coderror = "";
		String contratos_inversion = "";
		String cadena_resultante = "";
		String strSalida = "";
		String fecha_hoy = "";
		String tipo_cuenta = "";
		String aux2_contratos_inversion = "";
		String mistasas = "";
		String option_contratos = "";

		short suc_opera;
		boolean fac_programadas;
	    boolean chkseg = true;

		int contador = 0;
		int numero_cuentas_operar = 0;
		int numero_contratos_operar = 0;
		int siguiente = 0;
		int contador_prueba = 0;
		int numero_cuentas_inversion = 0;
		int numero_cuentas_inversion_por_cuenta = 0;

		String strInhabiles = diasInhabilesDesp();

		if (strInhabiles.trim().length()==0)
		{
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, request, response );
			return 0;
		}
		else
		{
			contrato = session.getContractNumber();
			suc_opera = (short)787;
			usuario = session.getUserID();
			clave_perfil = session.getUserProfile();
			cadena_getFacCOpProg = session.getFacCOpProg();
			fac_programadas = (cadena_getFacCOpProg != "");

			String[][] arrayCuentas = ContCtasRelacpara_mis_transferencias(contrato);

			request.setAttribute("diasInhabiles",strInhabiles);

			for (int indice=1;indice<=Integer.parseInt(arrayCuentas[0][0]);indice++)
			{
				tipo_cuenta = arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);

				if ( (arrayCuentas[indice][2].equals("P"))&&
					 (arrayCuentas[indice][3].equals("6")))
				{
					chkseg  = !existeFacultadCuenta(arrayCuentas[indice][1], "DISPVALPROP", "-");
					if(chkseg==true)
					{
						cadena_retorno_cuentas = llamada_cuentas_para_inversion7_28(arrayCuentas[indice][1]);

					//para prueba...
					//if (indice==1)
					//	cadena_retorno_cuentas="POSI0000|2|62070600535|60070600535|60070600535|0|64070600535|60070600535|60070600535|0|";
					//else if (indice==2)
					//	cadena_retorno_cuentas="POSI0000|2|62500469720|60500469720|60500469720|1|64500469720|60500469720|60500469720|1|";
					//else if (indice==3)
					//	cadena_retorno_cuentas="POSI0000|1|47001203862|57000235496|57000235496|0|";
					//else
						//cadena_retorno_cuentas="POSI0000|1|47004696262|57000136554|57000136554|0|";
					//	cadena_retorno_cuentas="POSI0000|2|42000751359|57008483327|57008483327|0|42004392087|57008483327|57008483327|0|";

						System.out.println("cadena_retorno_cuentas "+cadena_retorno_cuentas);
						if (cadena_retorno_cuentas.substring(0,8).equals("POSI0000"))
						{
							try {
								cadena_resultante = cadena_retorno_cuentas.substring(9,cadena_retorno_cuentas.length());
								contador_prueba = Integer.parseInt(cadena_resultante.substring(0,cadena_resultante.indexOf("|")));
								if (contador_prueba>0)
								{
									cadena_resultante = cadena_resultante.substring(cadena_resultante.indexOf("|")+1,cadena_resultante.length());
									contador_prueba = contador_prueba*4;
									cadena_resultante = String.valueOf(contador_prueba)+"|"+cadena_resultante;
									Cuentas_operar = desentramaC(cadena_resultante,'|');
									numero_cuentas_operar = Integer.parseInt(Cuentas_operar[0]);
									numero_cuentas_operar = numero_cuentas_operar/4;
									siguiente = 1;
									numero_cuentas_inversion_por_cuenta = 0;
									aux2_contratos_inversion = "";

									for(int i=1;i<=numero_cuentas_operar;i++)
									{
										if (Cuentas_operar[siguiente].substring(0,2).equals("42"))
										{
											aux2_contratos_inversion+=Cuentas_operar[siguiente]+"|";
											numero_cuentas_inversion++;
											numero_cuentas_inversion_por_cuenta++;
										}
											siguiente=siguiente+4;
									}
									if (numero_cuentas_inversion_por_cuenta>0)
									{
										contratos_inversion+=contratos_inversion+arrayCuentas[indice][1]+"|"+numero_cuentas_inversion_por_cuenta+"|";
										contratos_inversion+=aux2_contratos_inversion;
										strSalida = strSalida + "<OPTION VALUE=\""+arrayCuentas[indice][1]+"|"+arrayCuentas[indice][2]+"|"+arrayCuentas[indice][4]+"|"+"\"> "  + arrayCuentas[indice][1] +"   "+arrayCuentas[indice][4]+" </OPTION>\n" ;
										contratos_inversion+="@";
									}
								}
							}
							catch(Exception e)
							{
								EIGlobal.mensajePorTrace("***MIPD_plazo.class "+e, EIGlobal.NivelLog.ERROR);
							}
						}
					}
				}
			}
			fecha_hoy = ObtenFecha();
			//System.out.println("numero_cuentas_inversion = >>>" + numero_cuentas_inversion + "<<<");
			if (numero_cuentas_inversion>0)
			{
				campos_fecha = poner_campos_fecha(fac_programadas);

				request.setAttribute("cuentas_destino", strSalida);
				request.setAttribute("arregloctas_destino1","");
				request.setAttribute("arreglocontratos1","");
				request.setAttribute("arregloimportes1","");
				request.setAttribute("arreglofechas1","");
				request.setAttribute("arregloplazo1","");
				request.setAttribute("arregloinstr1","");
				request.setAttribute("arregloctas_destino2","");
				request.setAttribute("arreglocontratos2","");
				request.setAttribute("arregloestatus1","");
				request.setAttribute("arregloestatus2","");
				request.setAttribute("arregloimportes2","");
				request.setAttribute("arreglofechas2","");
				request.setAttribute("arregloplazo2","");
				request.setAttribute("arregloinstr2","");
				request.setAttribute("contador1","0");
				request.setAttribute("contador2","0");
				request.setAttribute("tabla","");
				request.setAttribute("fecha_hoy",fecha_hoy);
				request.setAttribute("ContUser",ObtenContUser(request));
				request.setAttribute("campos_fecha",campos_fecha);
				request.setAttribute("contratos_inversion",contratos_inversion);

				mistasas = llamada_consulta_tasas_plazos_por_servicio();

				request.setAttribute("tasas1",mistasas);
				request.setAttribute("tasas2",mistasas);

				try {
					String contratos_inicial = contratos_inversion.substring(0,contratos_inversion.indexOf("@"));
					contratos_inicial = contratos_inicial.substring(contratos_inicial.indexOf("|")+1,contratos_inicial.length());
					String[] Contr_cuentas=desentramaC(contratos_inicial,'|');

					for (int j=1;j<=Integer.parseInt(Contr_cuentas[0]);j++)
					{
						option_contratos = option_contratos+"<OPTION VALUE="+Contr_cuentas[j]+"> "+Contr_cuentas[j];
					}

				}
				catch(Exception e)
				{
				EIGlobal.mensajePorTrace("***MIPD_plazo.class "+e, EIGlobal.NivelLog.INFO);
				}
				request.setAttribute("cuentas_contratos",option_contratos);
				if (fac_programadas==false)
					request.setAttribute("fac_programadas1","0");
				else
					request.setAttribute("fac_programadas1","1");
					request.setAttribute("MenuPrincipal", session.getStrMenu());
					request.setAttribute("newMenu", session.getFuncionesDeMenu());
					request.setAttribute("Encabezado", CreaEncabezado("Inversiones a Plazo D&oacute;lares","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; D&oacute;lares ","s26170h",request));

					evalTemplate(IEnlace.PLAZOS_DOLAR_TMPL,request, response);
			}
			else
			{
			despliegaPaginaError("No existen contratos de inversi&oacute;n ","Inversiones Plazo D&oacute;lares","", request, response );
			}
		}
		return 1;
    }*/


   public void pantalla_inicial_plazo( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {

      EIGlobal.mensajePorTrace("***MIPD_plazo.class Entrando a pantalla_inicial_plao ", EIGlobal.NivelLog.INFO);

      int salida=0;
      String contrato="";
      boolean fac_programadas;
      String campos_fecha="";
      String cadena_retorno_cuentas="";
      int contador=0;
      String Cuentas_operar[]=null;
      int siguiente=0;
      int contador_prueba=0;
      String usuario="";
      String clave_perfil="";
      String coderror="";
      String contratos_inversion="";
      int numero_cuentas_inversion=0;
      int numero_cuentas_inversion_por_cuenta=0;
  	  short suc_opera;
		//************************
      String strInhabiles =diasInhabilesDesp();//armaDiasInhabilesJS();

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = request.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

	  if (strInhabiles.trim().length()==0)

	  {

          despliegaPaginaError("Servicio no disponible por el momento",request,response); //"Transferencias","");

          return ;

	  }
      contrato=session.getContractNumber();
      suc_opera=(short)787;
      usuario=session.getUserID();
      clave_perfil=session.getUserProfile();
      String cadena_getFacCOpProg=(session.getFacultad(session.FAC_PROGRAMA_OP))?"true":"false";
      fac_programadas=(cadena_getFacCOpProg!="");
      String cadena_resultante="";

      String fecha_hoy="";
      String tipo_cuenta="";
	  //String aux1_contratos_inversion="";
	  String aux2_contratos_inversion="";
      request.setAttribute("diasInhabiles",diasInhabilesDesp());
	  boolean chkseg=true;
	  String claveProducto= "";
      fecha_hoy=ObtenFecha();


        campos_fecha=poner_campos_fecha(fac_programadas);
        request.setAttribute("arregloctas_destino1","");
        request.setAttribute("arreglocontratos1","");
        request.setAttribute("arregloimportes1","");
        request.setAttribute("arreglofechas1","");
        request.setAttribute("arregloplazo1","");
        request.setAttribute("arregloinstr1","");
        request.setAttribute("arregloctas_destino2","");
        request.setAttribute("arreglocontratos2","");
        request.setAttribute("arregloestatus1","");
        request.setAttribute("arregloestatus2","");
        request.setAttribute("arregloimportes2","");
        request.setAttribute("arreglofechas2","");
        request.setAttribute("arregloplazo2","");
        request.setAttribute("arregloinstr2","");
        request.setAttribute("contador1","0");
        request.setAttribute("contador2","0");
        request.setAttribute("tabla","");
        request.setAttribute("fecha_hoy",fecha_hoy);
        request.setAttribute("ContUser",ObtenContUser(request));
        request.setAttribute("campos_fecha",campos_fecha);
        request.setAttribute("contratos_inversion",contratos_inversion);

        String mistasas = llamada_consulta_tasas_plazos_por_servicio();

        request.setAttribute("tasas1",mistasas);
        request.setAttribute("tasas2",mistasas);
        String option_contratos="";

        if (fac_programadas==false)
          request.setAttribute("fac_programadas1","0");
        else
          request.setAttribute("fac_programadas1","1");
 	    //request.setAttribute("MenuPrincipal", session.getstrMenu());
        //request.setAttribute("Fecha", ObtenFecha());
        request.setAttribute("MenuPrincipal", session.getStrMenu());
        request.setAttribute("newMenu", session.getFuncionesDeMenu());
        request.setAttribute("Encabezado", CreaEncabezado("Inversi&oacute;n 7/28","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; 7/28","s26170h",request));
        session.setModuloConsultar(IEnlace.MPlazo_dolares);
		evalTemplate( IEnlace.PLAZOS_DOLAR_TMPL, request, response );

    }


	public int ejecucion_operaciones_plazo(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
    {
       EIGlobal.mensajePorTrace("***MIPD_plazo.class Entrando a ejecucion_operaciones_plazo ", EIGlobal.NivelLog.INFO);

       Hashtable htResult = null;

       String cadena_cta_destino = "";
       String contrato_inversion = "";
       String importe_string = "";
       String plazo = "";
       String instr = "";
       String fecha = "";
       String fecha_hoy = "";

       String contrato = "";
	   String usuario = "";
	   String clave_perfil = "";
       String tipo_operacion = "";
       String mensaje_salida = "";
       String cta_destino = "";

       String coderror = "";
       String coderror_operacion = "";
       double importe = 0.0;
       String opcion = "";
       String sfecha_programada = "";
       String bandera_fecha_programada = "1";
       String fecha_programada_b = "";
       Double importedbl;
       boolean programada = false;
       String tipo_cta_destino = "";
       int indice = 0;
       String tipo_cuenta = "";
       String tipo_prod = "";

       String indicador1 = "";
       String indicador2 = "";
       String num_promotor = "";
       String salida_buffer = "";
       String cont_cuentas = "";
       String arrcont_cuentas = "";

       String[] arrCuentas = new String[4];

       String trama_entrada = "";
	   String sreferencia = "";
       String mensajeerror = "";
	   String tramaok = "";

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = request.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

	   ServicioTux tuxGlobal = new ServicioTux();
	   //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

       cadena_cta_destino = request.getParameter ("destino");
       contrato_inversion = request.getParameter ("contratos");
       importe_string     = request.getParameter ("importe");
       plazo              = request.getParameter ("plazo");
       instr              = request.getParameter ("inst1");
       fecha              = request.getParameter ("fecha");

       fecha_hoy = ObtenFecha();

       EIGlobal.mensajePorTrace("***MIPD_plazo.class cadena_cta_destino: "+cadena_cta_destino, EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("***MIPD_plazo.class cont_cuentas:       "+cont_cuentas, EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("***MIPD_plazo.class importe_string:     "+importe_string, EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("***MIPD_plazo.class plazo:              "+plazo, EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("***MIPD_plazo.class instr:              "+instr, EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("***MIPD_plazo.class fecha:              "+fecha, EIGlobal.NivelLog.INFO);

	   String trama = "";
	   String[] resultManc = {""};

       contrato = session.getContractNumber();
       usuario = session.getUserID();
       clave_perfil = session.getUserProfile();
       //tipo_operacion = "CPDV";
       tipo_operacion = "DIDV";

       mensaje_salida="<table width=760 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
       mensaje_salida=mensaje_salida+"<TR>";
       mensaje_salida=mensaje_salida+"<td width=80 class=tittabdat align=center>Cuenta</td>";
       mensaje_salida=mensaje_salida+"<td width=80 class=tittabdat align=center>Contrato</td>";

       mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=60>Plazo</td>";
       mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=150>Istrucci&oacute;n al vencimiento</td>";
       mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=90>Importe</td>";

	   mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=75>Fecha de aplicaci&oacute;n</td>";
	   mensaje_salida=mensaje_salida+"<td class=tittabdat align=center width=66>Estatus</td>";
       mensaje_salida=mensaje_salida+" <td class=tittabdat align=center width=65>Referencia</td>";
	   mensaje_salida=mensaje_salida+"</TR>";

       arrCuentas = desentramaC("3|" + cadena_cta_destino,'|');
       cta_destino = arrCuentas[1];
       tipo_cta_destino = arrCuentas[2];
       importe = new Double (importe_string).doubleValue();
       sfecha_programada = fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());
       String manc_fecha_prog = fecha.substring(3,5)+fecha.substring(0,2)+fecha.substring(8,fecha.length());
       programada = checar_si_es_programada(fecha);
       tipo_prod = "32"; //tipo de producto para dolar

       if (instr.equals("1"))
       {
           indicador1="S";
           indicador2="S";
       }
       else if (instr.equals("2"))
       {
           indicador1="S";
           indicador2="N";
       }
       else if (instr.equals("3"))
       {
           indicador1="N";
           indicador2="N";
       }
       if (programada==false)
            trama_entrada="1EWEB|"+usuario+"|"+tipo_operacion+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+cta_destino+"|"+tipo_cta_destino+"|"
                          +importe_string+"|"+contrato_inversion+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";
       else
            trama_entrada="1EWEB|"+usuario+"|"+"PROG"+"|"+contrato+"|"+usuario+"|"+clave_perfil+"|"+tipo_operacion +"|"+sfecha_programada+"|"+cta_destino+"|"+tipo_cta_destino+"|"
                          +importe_string+"|"+contrato_inversion+"|"+plazo+"|"+tipo_prod+"|"+indicador1+"|"+indicador2+"|"+" |";

       EIGlobal.mensajePorTrace("***inversion7_28 WEB_RED-inversion7_28> "+trama_entrada, EIGlobal.NivelLog.DEBUG);

	   try {
			htResult = tuxGlobal.web_red( trama_entrada );
		} catch ( java.rmi.RemoteException re ) {
			re.printStackTrace();
    	} catch(Exception e) {}
  		    coderror = ( String ) htResult.get( "BUFFER" );

    EIGlobal.mensajePorTrace("***MIPD_Plazo.class<<"+coderror, EIGlobal.NivelLog.DEBUG);
	//***************************************************************************
	//Se le debe adicionar los mensajes de error.
	//***************************************************************************
	if (  !(coderror.substring(0,2).equals("OK")) || coderror.indexOf("LA CUENTA ANTIGUA NO EXISTE O NO TIENE CTA. NUEVA.") > -1 )
		{
		despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Inversiones a Plazo D&oacute;lares","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; D&oacute;lares ",request, response  );
		}else
		{
       coderror_operacion = "";
	   salida_buffer = "";
       if ((coderror.length()>=16)&&(!(coderror.substring(0,4).equals("MANC"))))
              sreferencia = coderror.substring(8,16);
	   sreferencia = sreferencia.trim();
       mensaje_salida = mensaje_salida+"<TR>";

	   mensaje_salida = mensaje_salida+"<TD class=textabdatobs nowrap align=center>"+cta_destino+"</TD>";
       mensaje_salida = mensaje_salida+"<TD class=textabdatobs nowrap align=center>"+contrato_inversion+"</TD>";
	   mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center>"+ plazo +" dias </TD>";
       if (instr.equals("1"))
          mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital e interes</TD>";
       else if (instr.equals("2"))
          mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=left>Reinvertir Capital</TD>";
       else if (instr.equals("3"))
          mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=left>Transferir Capital e intereses</TD>";
       mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=right nowrap>"+FormatoMoneda(importe_string)+"</TD>";
       mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";

       if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
	      mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n Mancomunada</TD>";
	   else if ((coderror.substring(0,2).equals("OK"))&&(programada==true))
          mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n programada</TD>";
       else if ((coderror.substring(0,2).equals("OK"))&&(programada==false))
	   {
          coderror_operacion = coderror.substring(coderror.indexOf("|")+1,coderror.length());
	      coderror_operacion = coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
		  if (coderror_operacion.equals("CABO0000"))
             mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Tranferencia aceptada</TD>";
	      else
 		     mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
	   }
	   else
       {
          if (programada==true)
            mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Operaci&oacute;n NO programada </TD>";
          else
          {
            if (coderror.substring(0,4).equals("MANC"))
                mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(8,coderror.length())+"</TD>";
            else if ((coderror.length()>32)&&(!(coderror.substring(0,4).equals("MANC"))))
                mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+coderror.substring(32,coderror.length())+"</TD>";
            else
                mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>Error de operaci&oacute;n</TD>";
          }
       }
       if (coderror.substring(0,2).equals("OK"))
       {
          if (programada==false)
          {
             if((coderror.length()>16)&&(coderror.substring(16,coderror.length()).equals("MANC")))
                mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
             else
             {
                coderror_operacion = coderror.substring(coderror.indexOf("|")+1,coderror.length());
                salida_buffer = coderror_operacion;
				coderror_operacion = coderror_operacion.substring(0,coderror_operacion.indexOf("|"));
                salida_buffer = salida_buffer.substring(salida_buffer.indexOf("|")+1,salida_buffer.length());
				salida_buffer = salida_buffer.substring(0,salida_buffer.indexOf("|"));
				if (coderror_operacion.equals("CABO0000"))
				{
		           // mensaje_salida=mensaje_salida+"<TD ALIGN=RIGHT><A HREF=\"/cgi-bin/gx.cgi/AppLogic+EnlaceInternet.comprobante_trans?tipo=4&refe="+sreferencia+"&importe="+importe_string+"&cta_origen="+cta_destino+"&cta_destino="+contrato_inversion+"&plazo="+plazo+"&enlinea=s\">"+sreferencia+"</A></TD>";
				   tramaok+="1|7|"+sreferencia.trim()+"|"+importe_string+"|"+cta_destino+"|"+contrato_inversion+"| @";
			       mensaje_salida = mensaje_salida+"<TD class=textabdatobs align=center nowrap><A  href=\"javascript:GenerarComprobante(document.operacionrealizada.tramaok.value, 1);\" >"+sreferencia+"</A></TD>";
				}
				else
                   mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
 		     }

          }
		  else
			mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
	   }
	   else if (coderror.substring(0,4).equals("MANC"))
          mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>-------</TD>";
       else
       mensaje_salida=mensaje_salida+"<TD class=textabdatobs align=center nowrap>"+sreferencia+"</TD>";
       mensaje_salida=mensaje_salida+"</TR>";
       mensaje_salida=mensaje_salida+"</TABLE>";
	   mensaje_salida+="<table width=760 border=0 cellspacing=2 cellpadding=0><tr><td valign=top align=left class=textabref width=372>Si desea obtener su comprobante, d&eacute; click en el n&uacute;mero de referencia subrayado.</td></tr></table>";

       request.setAttribute("titulo","Inversiones plazo-dolar Procesadas");
       request.setAttribute("mensaje_salida",mensaje_salida);
       request.setAttribute("fecha_hoy",fecha_hoy);
       //map.putString("ContUser",ObtenContUser(request));
       //map.putString("MenuPrincipal", session.getstrMenu());
       request.setAttribute("Fecha", ObtenFecha());

       request.setAttribute("MenuPrincipal", session.getStrMenu());
       request.setAttribute("newMenu", session.getFuncionesDeMenu());
       request.setAttribute("Encabezado", CreaEncabezado("Inversiones a Plazo D&oacute;lares","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; D&oacute;lares ","s26180h",request));
       request.setAttribute("tramaok","\""+tramaok+"\"");
       request.setAttribute("contadorok","1");
       request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
       request.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");
	   request.setAttribute("banco",session.getClaveBanco());

	   ///////////////////////////////////////////////////////////////////////
		HttpSession ses = request.getSession();

	   ses.setAttribute("titulo","Inversiones plazo-dolar Procesadas");
       ses.setAttribute("mensaje_salida",mensaje_salida);
       ses.setAttribute("fecha_hoy",fecha_hoy);
       //map.putString("ContUser",ObtenContUser(request));
       //map.putString("MenuPrincipal", session.getstrMenu());
       ses.setAttribute("Fecha", ObtenFecha());

       ses.setAttribute("MenuPrincipal", session.getStrMenu());
       ses.setAttribute("newMenu", session.getFuncionesDeMenu());
       ses.setAttribute("Encabezado", CreaEncabezado("Inversiones a Plazo D&oacute;lares","Tesorer&iacute;a &gt; Inversiones &gt; Plazo fijo &gt; D&oacute;lares ","s26180h",request));
       ses.setAttribute("tramaok","\""+tramaok+"\"");
       ses.setAttribute("contadorok","1");
       ses.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
       ses.setAttribute("datosusuario","\""+session.getUserID()+"  "+session.getNombreUsuario()+"\"");

	   //response.sendRedirect(response.encodeRedirectURL(IEnlace.MI_OPER_TMPL, request, response));
       ses.setAttribute("web_application",Global.WEB_APPLICATION);
       //<vswf:meg cambio de NASApp por Enlace 08122008>
       

       evalTemplate("/Enlace/"+Global.WEB_APPLICATION+IEnlace.MI_OPER_TMPL, request, response);

    } // del else
	return 1;
} // metodo
	//llama al servicio BASICP para obtener los contratos de inversion  asociadas a las cuentas
    public String llamada_cuentas_para_inversion7_28(String cuenta)
    {
		EIGlobal.mensajePorTrace( "***MIPD_Plazo Entrando a llamada_cuentas_para_inversion7_28 ", EIGlobal.NivelLog.DEBUG);

		String coderror;
		String tramaRegresoServicio = "";

	    ServicioTux tuxGlobal = new ServicioTux();
	    //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	    int reqStatus = 0;
        int nSize = 0;

	    Hashtable htResult = null;
		String POSI_OK_CODE = "POSI0000";
		String trama_enviar = "";
		String buffer = "";
		trama_enviar = cuenta + "|C|";

		try {
		//*************************************
			try {
				Hashtable hs = tuxGlobal.web_red(trama_enviar);
				tramaRegresoServicio = (String) hs.get("BUFFER");

				//reqStatus = Integer.parseInt((String) hs.get("BUFFER"));
			} catch( java.rmi.RemoteException re ) {
				re.printStackTrace();
			} catch (Exception e) {}

		//*********************************
	    	htResult = tuxGlobal.basicp( trama_enviar );
			} catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace();
	    } catch (Exception e) {}

		coderror = ( String ) htResult.get( "COD_ERROR" );

		System.out.println("CODIGO DE ERROR BASICP" + coderror );
		EIGlobal.mensajePorTrace("***MIPD_Plazo BASICP " + coderror, EIGlobal.NivelLog.DEBUG);

		if ( coderror == null )
		{
			buffer = "Problemas con BASICP";
			EIGlobal.mensajePorTrace("***MIPD_Plazo buffer: "+buffer, EIGlobal.NivelLog.INFO);
		}
		else if ( coderror.equals( POSI_OK_CODE ) )
		{
			buffer = ( String ) htResult.get( "BUFFER" );
			buffer = coderror + "|" + buffer;
		}
		else
		{
			buffer = coderror;
		}
		EIGlobal.mensajePorTrace("***MIPD_Plazo buffer: "+buffer, EIGlobal.NivelLog.INFO);
		return buffer;
    }

	//Hace consulta en tabla comu_tablas para obtener las tasas de plazo

	public String llamada_consulta_tasas_plazos_por_servicio()
		throws ServletException, IOException
    {
		EIGlobal.mensajePorTrace("***llamada_consulta_tasas_plazos_por_servicio", EIGlobal.NivelLog.DEBUG);
		String coderror = "";
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		int reqStatus = 0;
		int nSize = 0;

   	    Hashtable htResult = null;
		String POSI_OK_CODE="POSI0000";
		String trama_enviar = "";
	    String cadena_resultante="";
		String trama_buffer = "|42|";
		String trama_medio_entrega = "|TCT|";

		try
		{
			htResult = tuxGlobal.consultaTasas( trama_buffer, trama_medio_entrega );
			coderror = ( String ) htResult.get( "COD_ERROR" );
			EIGlobal.mensajePorTrace("***MIPD_Plazo consultaTasas " + coderror, EIGlobal.NivelLog.DEBUG);
	    } catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace(); }
		catch (Exception e) {}

		if ( coderror == null )
		{
			cadena_resultante = "Problemas con consultaTasas";
			EIGlobal.mensajePorTrace("***MIPD_Plazo cadena_resultante: " + cadena_resultante, EIGlobal.NivelLog.INFO);
		} else if ( coderror.equals( POSI_OK_CODE ) )
		{
			cadena_resultante = ( String ) htResult.get( "BUFFER" );
			cadena_resultante = coderror + "|" + cadena_resultante;
		} else {
			cadena_resultante = coderror;
		}

		System.out.println("COD_ERROR" + coderror );
		EIGlobal.mensajePorTrace("***MIPD_Plazo cadena_resultante: " + cadena_resultante, EIGlobal.NivelLog.INFO);

        if (coderror == null)
		{
			System.out.println("servicio error");
			EIGlobal.mensajePorTrace("***Error en servicio: CONSTASAS ", EIGlobal.NivelLog.INFO);
            // look into the buffer if this is a service error
            // return HTML error
        }
		else
        {
		   //traer archivo tasas.dat
		   ArchivoRemoto archivo_remoto = new ArchivoRemoto();

 		      if(!archivo_remoto.copiaOtroPathRemoto("tasas.dat",Global.DIRECTORIO_LOCAL) )
		      //if(!archivo_remoto.copia("tasas.dat") )
		      {
	             EIGlobal.mensajePorTrace("***MIPD_plazo.class No se pudo copiar archivo remoto:"+IEnlace.REMOTO_TMP_DIR+"tasas.dat", EIGlobal.NivelLog.INFO);

		      }
		      else
		      {
			     EIGlobal.mensajePorTrace("***MIPD_plazo.class archivo remoto copiado"+IEnlace.REMOTO_TMP_DIR+"tasas.dat", EIGlobal.NivelLog.INFO);
                 try
				 {
			       String linea="";
				   String parte1="";
				   String parte2="";

				   BufferedReader in;
				   in = new BufferedReader(new FileReader(IEnlace.LOCAL_TMP_DIR + "/tasas.dat" ));


				   while ((linea=in.readLine())!=null)
				   {
						//sprintf( linea,"%s|%s|%s|%f|%f|%f|",fecha_aux,element->instrumento,element->plazo,element->rango_menor,element->rango_mayor,element->tasa);

						parte1=linea.substring(0,linea.indexOf("|")+1); //fecha;
						parte2=linea.substring(linea.indexOf("|")+1,linea.length());//esto para quitarle instrumento
						parte2=parte2.substring(parte2.indexOf("|")+1,parte2.length());//esto para quitarle instrumento
						cadena_resultante+=parte1+parte2;
        			}
                 }
			     catch(IOException e)
			     {
				  EIGlobal.mensajePorTrace("***MIPD_plazo.class: "+e, EIGlobal.NivelLog.ERROR);
				 }
             } // del sele
		   } // del else

        EIGlobal.mensajePorTrace("***salida llamada_consulta_tasas_plazos_por_servicio: ", EIGlobal.NivelLog.INFO);
       return cadena_resultante;
   }
}