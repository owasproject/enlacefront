/** 
*   Isban Mexico
*   Clase: FielModificacion.java
*   Descripcion: Servlet que gestiona el flujo de la modificacion de la Fiel.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.FielConstants.AYUDA_FIEL_MODIF;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.FielConstants;
import mx.altec.enlace.utilerias.FielUtils;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.UtilAdminFIEL;
/**
 * Servlet implementation class FielModifica
 * @author FSW
 */
public class FielModificacion extends BaseServlet {
   
    /**
     * Serial version ID.
     */
    private static final long serialVersionUID = -8458224639719139084L; 
    
    /** Titulo del encabezado de la pagina. */
    private static final String PAG_ENCAB_TITULO = "Modificacion de FIEL";
    /** Ruta base del encabezado de la pagina. */
    private static final String PAG_ENCAB_RUTA = "Administraci&oacute;n y control &gt; FIEL &gt; Modificacion";
    /** Cadena base de registro en Log. */
    private static final String LOG_MODULO = "FielModificacion - ";

    /**
     * Metodo que responde a peticiones GET
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que responde a peticiones POST
     *
     * @param request objeto inherente a la peticion
     * @param response objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        defaultAction(request, response);
    }

    /**
     * Metodo que atiende peticiones de Modificacion FIEL
     *
     * @param req objeto inherente a la peticion
     * @param res objeto inherente a la respuesta
     * @throws ServletException Excepcion generica
     * @throws IOException Excepcion generica
     */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
    	
    	EIGlobal.mensajePorTrace("defaultAction - Inicia FielModificacion", EIGlobal.NivelLog.INFO);
        HttpSession ses = req.getSession();
        BaseResource attrSession = (BaseResource) ses.getAttribute("session");
      
        if (SesionValida(req, res)) {
			attrSession.setModuloConsultar(IEnlace.MDep_inter_cargo); // Limpiar modulo de busueda de cuentas
            String valida = FielUtils.validaRecToken(req, this);
            
            if (validaPeticion(req, res, attrSession, req.getSession(), valida)) {
                EIGlobal.mensajePorTrace("\n\nENTRO A VALIDAR LA PETICION\n\n", EIGlobal.NivelLog.INFO);
            } else {
                String mod = getFormParameter(req, FielConstants.PARAM_MODULO);
                EIGlobal.mensajePorTrace(LOG_MODULO + "Modulo - " + mod, EIGlobal.NivelLog.INFO);
                if (mod == null || mod.isEmpty()) {
                    mod = FielConstants.INICIA_REGISTRO;
                }
                
                // Accion por modulo
                String pagResultado = FielConstants.JSP_FIEL_MOD_INIT;
                switch (mod.charAt(0)) {
                    case '1': // Consulta fiel
                        EIGlobal.mensajePorTrace(LOG_MODULO + "ConsultaFiel", EIGlobal.NivelLog.INFO);
                        FielUtils.cargaEncabezadoPagina(req, this, attrSession, 
                                PAG_ENCAB_TITULO, PAG_ENCAB_RUTA,
                                AYUDA_FIEL_MODIF);                        
                        consultaFiel(req, ses);
                        pagResultado = FielConstants.JSP_FIEL_MOD_INIT;                   
                        break;
                    case '2': // validaModificacion
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Validacion", EIGlobal.NivelLog.INFO);
                        pagResultado = validaModificacion(req, res, ses, attrSession, valida);                      
                        break;
                    case '3': // confirmacion
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Confirmacion", EIGlobal.NivelLog.INFO);
                        FielUtils.cargaEncabezadoPagina(req, this, attrSession, 
                                PAG_ENCAB_TITULO, PAG_ENCAB_RUTA + " &gt; Confirmacion",
                                AYUDA_FIEL_MODIF);
                        pagResultado = FielConstants.JSP_FIEL_RESULTADO;
                        break;
                    case '4': // comprobante 
                        EIGlobal.mensajePorTrace(LOG_MODULO + "Imprimir comprobante", EIGlobal.NivelLog.INFO);
                        req.setAttribute(FielConstants.PARAM_FIEL_RESULT,
                                req.getSession().getAttribute(FielConstants.PARAM_FIEL_RESULT));
                        pagResultado = FielConstants.JSP_FIEL_COMPROBANTE;
                        break;
                    default: // Inicio
                        EIGlobal.mensajePorTrace(LOG_MODULO + " Inicio", EIGlobal.NivelLog.INFO);
                        FielUtils.cargaEncabezadoPagina(req, this, attrSession, 
                                PAG_ENCAB_TITULO, PAG_ENCAB_RUTA,
                                AYUDA_FIEL_MODIF);
                        req.getSession().removeAttribute(FielConstants.PARAM_FIEL_RESULT);
                        ses.removeAttribute(FielConstants.PARAM_DETALLE_FIEL);
                        ses.removeAttribute(FielConstants.PARAM_DETALLE_FIEL_TRX54);
                        FielUtils.grabaPistas(req, attrSession, FielConstants.BITA_FIEL_MODIFICA_ENTRA,
                                null, null, null, false, null);
                        break;
                }
                EIGlobal.mensajePorTrace(LOG_MODULO + "pagResultado **** " + pagResultado + " MODAT- " + mod.charAt(0) , EIGlobal.NivelLog.INFO);
                if (pagResultado != null) {
                    evalTemplate(pagResultado, req, res);
                }
            }
        }
        
        EIGlobal.mensajePorTrace("defaultAction - Saliendo de FielModificacion", EIGlobal.NivelLog.INFO);
    }
    
    /**
     * Metodo para consultar la Fiel en Transaccion 390
     * @param req objeto inherente a la peticion
     * @param ses objeto de sesion
     */
    private void consultaFiel(HttpServletRequest req, HttpSession ses){
        EIGlobal.mensajePorTrace("entra a consultaFiel", EIGlobal.NivelLog.INFO);
        String detalleCuenta = getFormParameter(req, "comboCuenta");
        BeanAdminFiel beanAdminFiel = new BeanAdminFiel();
        if (detalleCuenta != null) {
            String cuenta;
            if (detalleCuenta.indexOf('|') >= 0) {
                cuenta = detalleCuenta.substring(0, detalleCuenta.indexOf('|'));
            } else {
                cuenta = detalleCuenta;
            }
            beanAdminFiel.setCuenta(cuenta);
            beanAdminFiel= UtilAdminFIEL.consultaFielTrx(beanAdminFiel);
            //validar cuenta y fiel no vacias desde el bean
            if(FielConstants.COD_ERRROR_OK.equals(beanAdminFiel.getCodError())){
				EIGlobal.mensajePorTrace("FielModificacion - Fiel TRXOD54 "
										+ beanAdminFiel.getDetalleConsulta().getNumSerie().trim(), 
											EIGlobal.NivelLog.DEBUG);
                req.setAttribute("comboCuenta", beanAdminFiel.getCuenta());
                req.setAttribute("fiel", beanAdminFiel.getDetalleConsulta().getNumSerie().trim());
                ses.setAttribute(FielConstants.PARAM_DETALLE_FIEL_TRX54, beanAdminFiel);
            }else{
                req.setAttribute("msgError", beanAdminFiel.getMsgError());
            }
        }
    }
    
  
    /**
     * Metodo que valida los datos de la FIEL
     * @param req objeto inherente a la peticion
     * @param res objeto inherente a la respuesta
     * @param ses objeto de sesion
     * @param attrSession sesion interna de enlace
     * @param valida bandera del resultado de la validacion
     * @return pagina donde se redireccionara
     * @throws IOException en caso de error
     */
    private String validaModificacion(HttpServletRequest req, HttpServletResponse res,
            HttpSession ses, BaseResource attrSession, String valida) throws IOException {
        String paginaRetorno = FielConstants.JSP_FIEL_MOD_INIT;
        String fielAntigua = getFormParameter(req, "fielAntigua");
        String cuenta = getFormParameter(req, "cuenta");
        if (cuenta != null && cuenta.indexOf(FielConstants.PIPE_CHAR) >= FielConstants.CERO_INT) {
            cuenta = cuenta.substring(FielConstants.CERO_INT, cuenta.indexOf(FielConstants.PIPE_CHAR));
        }
                
        String fielNueva = getFormParameter(req, "fiel");
        EIGlobal.mensajePorTrace(LOG_MODULO + "IN> Cuenta = " + cuenta 
                        + "| -- Fiel -- " + fielNueva, EIGlobal.NivelLog.INFO);
        BeanAdminFiel detalleFiel = (BeanAdminFiel) ses.getAttribute(FielConstants.PARAM_DETALLE_FIEL);
        if (detalleFiel == null) {
            detalleFiel = new BeanAdminFiel();
            detalleFiel.setFiel(fielNueva);
            detalleFiel.setCuenta(cuenta);
            detalleFiel = UtilAdminFIEL.buscaFiel(detalleFiel);
        }

        if (FielConstants.COD_ERRROR_OK.equals(detalleFiel.getCodError())) {
            ses.setAttribute(FielConstants.PARAM_DETALLE_FIEL, detalleFiel);
            if (FielUtils.validaRSA(req, res, ses, this, "/enlaceMig/FielModificacion")) {
                paginaRetorno = null;
            } else {
                EIGlobal.mensajePorTrace("\n\n\n ANTES DE VERIFICACION DE TOKEN \n\n\n", EIGlobal.NivelLog.INFO);
                if (valida == null) {
                    EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: valida es null", EIGlobal.NivelLog.DEBUG);
                    boolean solVal = ValidaOTP.solicitaValidacion(attrSession.getContractNumber(), IEnlace.FIEL_OTP_MODIFICA);
                    if (solVal && attrSession.getValidarToken() && attrSession.getFacultad(BaseResource.FAC_VAL_OTP)
                            && attrSession.getToken().getStatus() == 1) {
                        req.setAttribute(FielConstants.DATOS_FIEL, FielConstants.DATOS_FIEL);
                        paginaRetorno = FielUtils.validaToken(req, res, attrSession, this,
                                paginaRetorno, PAG_ENCAB_TITULO, 
                                PAG_ENCAB_RUTA + " &gt; Confirmaci&oacute;n", AYUDA_FIEL_MODIF,
                                FielConstants.CONTEXTO_ENLACE + Global.WEB_APPLICATION + "/FielModificacion?modulo=0&"
                                + BitaConstants.FLUJO + "=" + FielConstants.BITA_FIEL_MODIFICA,
                                "&#191;Desea modificar la FIEL "+fielAntigua
                                + " asociada a la cuenta " + cuenta + "&#63;");
                    } else {
                        EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: valida NO es null", EIGlobal.NivelLog.DEBUG);
                        ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado.");
                        valida = FielConstants.UNO_STR;
                    }
                }
            }
            EIGlobal.mensajePorTrace(LOG_MODULO + "TRACE: continua Valida >" + valida, EIGlobal.NivelLog.INFO);
            if (valida != null && FielConstants.UNO_STR.equals(valida)) {
                int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
                //------------------------- BAJA --------------------------------
                bajaFiel(ses);
                //------------------------- REGISTRO ---------------------------
                detalleFiel.setReferencia(String.valueOf(referencia));
                detalleFiel.setIndicadorOperacion("A");
                req.setAttribute(FielConstants.ETIQUETA_NOMBRE_MODULO_FIEL, "MODIFICACION FIEL");
                paginaRetorno = FielConstants.JSP_FIEL_RESULTADO;

                detalleFiel = UtilAdminFIEL.administraFiel(detalleFiel);
                req.getSession().setAttribute(FielConstants.PARAM_FIEL_RESULT, detalleFiel);
                FielUtils.realizarOperaciones(req, attrSession, 
                            referencia, cuenta,
                            FielConstants.BITA_FIEL_MODIFICA_EJEC, 
                            FielConstants.BITA_FIEL_MODIFICA,
                            detalleFiel.getCodError(),
                            "Modificacion", Description(FielConstants.BITA_FIEL_MODIFICA));
            } 
            if (paginaRetorno != null) {
                EIGlobal.mensajePorTrace(LOG_MODULO + "paginaRetorno - se crea encabezado " + paginaRetorno , EIGlobal.NivelLog.INFO);
                FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO,
                        PAG_ENCAB_RUTA + " &gt; Resultado", AYUDA_FIEL_MODIF);
            }
        } else {
            req.setAttribute(FielConstants.PARAM_MODULO, FielConstants.INICIA_REGISTRO);
            req.setAttribute(FielConstants.ETIQUETA_MSG_ERROR, detalleFiel.getMsgError());
            FielUtils.cargaEncabezadoPagina(req, this, attrSession, PAG_ENCAB_TITULO,
                    PAG_ENCAB_RUTA + " &gt; Modificacion", AYUDA_FIEL_MODIF);
        }
        
        return paginaRetorno;
    }
    
    /**
     * Metodo para dar de baja FIEL
     * @param ses objeto de sesion
     */
    private void bajaFiel(HttpSession ses){
        EIGlobal.mensajePorTrace(LOG_MODULO + "-- entra a bajaFiel --", EIGlobal.NivelLog.INFO);
        BeanAdminFiel detalleFielBaja = (BeanAdminFiel) ses.getAttribute(FielConstants.PARAM_DETALLE_FIEL_TRX54);
        detalleFielBaja.setIndicadorOperacion("B");
        detalleFielBaja = UtilAdminFIEL.administraFiel(detalleFielBaja);
        EIGlobal.mensajePorTrace(LOG_MODULO + "-- bajaFiel codError--" + detalleFielBaja.getCodError(), EIGlobal.NivelLog.INFO);
    }

}
