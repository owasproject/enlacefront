package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.RespInterbBean;
import mx.altec.enlace.dao.CatNominaInterbDAO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.sql.*;

//VSWF


public class CatNomInterbBaja extends BaseServlet
{
	String pipe = "|";

	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		inicioServletBajaCuentas( request, response );
	}
	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException {
		inicioServletBajaCuentas( request, response );
	}

/*************************************************************************************/
/************************************************** inicioServletBajaCuentas		 */
/*************************************************************************************/
	public void inicioServletBajaCuentas( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
		HashMap facultadesBajaCtas = new HashMap ();
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("CatNomInterbBaja - defaultAction(): Entrando a Baja de Cuentas.", EIGlobal.NivelLog.INFO);

		String modulo = request.getParameter( "Modulo" );
		EIGlobal.mensajePorTrace("CatNomInterbBaja - defaultAction(): modulo ="+modulo, EIGlobal.NivelLog.INFO);
		Boolean envioCompleto = new Boolean((String)request.getParameter("flag"));
		if(modulo==null)
			modulo="0";

		if( SesionValida( request, response ) )
		{
			// Facultades ...
			facultadesBajaCtas.put ("BajaCtasBN", new Boolean (session.getFacultad(session.FAC_BAJA_BN)));
			facultadesBajaCtas.put ("AutoCtasOB", new Boolean (session.getFacultad(session.FAC_AUTO_AB_OB)));

			EIGlobal.mensajePorTrace("CatNomInterbBaja - execute(): Leyendo facultades.", EIGlobal.NivelLog.INFO);

			if(request.getSession ().getAttribute ("facultadesBajaCtas")!=null)
 			  request.getSession().removeAttribute("facultadesBajaCtas");
			request.getSession ().setAttribute ("facultadesBajaCtas", facultadesBajaCtas);

			if (!((Boolean)facultadesBajaCtas.get("BajaCtasBN")).booleanValue())
			  despliegaPaginaErrorURL("<font color=red>No</font> tiene <i>facultades</i> para dar cuentas de <font color=green>Baja</font>.","Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja","s55205h", request, response);
			else
			 {
			   if( modulo.equals("0") )
					iniciaBaja( request, response);
			   if( modulo.equals("1") )
				  agregaCuentas( request, response);
			   if( modulo.equals("2") && envioCompleto.booleanValue())
				  enviaCuentas( request, response);
			   if( modulo.equals("2") && !envioCompleto.booleanValue())
				  enviaCuentasCancel(request, response);
			   if( modulo.equals("3") )
				  presentaCuentas( request, response);
			 }
		}
	   EIGlobal.mensajePorTrace("CatNomInterbBaja - execute(): Saliendo de Baja de Cuentas.", EIGlobal.NivelLog.INFO);
	}

/*************************************************************************************/
/*************************************************************** inicioBajas		 */
/*************************************************************************************/
	public void iniciaBaja( HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("CatNomInterbBaja - iniciaBaja(): Pantalla Principal de baja de cuentas.", EIGlobal.NivelLog.INFO);

		request.setAttribute( "Modulo", "0" );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute("comboCuentas", traeCuentas(session.getContractNumber()==null?"":session.getContractNumber().toString()));
		sess.removeAttribute("foliosBaja");
		//TODO BIT CU5081, inicia el flujo de Baja de Cuentas
		/*VSWF HGG I*/
		/*if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			if (request.getParameter(BitaConstants.FLUJO)!=null){
				bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
			}else{
				bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			}
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CUENTAS_BAJA_ENTRA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}

			BitaHandler.getInstance().insertBitaTransac(bt);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		} */
		/*VSWF HGG F*/

		request.setAttribute( "newMenu", session.getFuncionesDeMenu() );
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute("Encabezado", CreaEncabezado("Baja de cuentas", "Servicios &gt; N&oacute;mina Interbancaria &gt; Registro de Cuentas &gt; Baja","s26090h", request));

		session.setModuloConsultar(IEnlace.MMant_gral_ctas);

		EIGlobal.mensajePorTrace("CatNomInterbBaja - iniciaBaja(): Invoca la Pantalla Principal.", EIGlobal.NivelLog.INFO);

		evalTemplate( "/jsp/CatNomInterbBaja.jsp", request, response );
	}

	/*
	 * Trae las cuentas interbancarias para conbo de pantalla
	 */

	public String traeCuentas(String contrato)
	 {
		StringBuffer strOpcion=new StringBuffer("");
		CatNominaInterbDAO dao = new CatNominaInterbDAO();

		ArrayList<String[]> datos = dao.obtenCuentas(contrato);

		EIGlobal.mensajePorTrace("CatNomInterbRegistro - traeCuentas(): Numero de datos. " + datos.size() , EIGlobal.NivelLog.ERROR);

		for(String[] dato: datos)
		  {
			strOpcion.append ("<option CLASS='tabmovtex' value='");
			strOpcion.append (dato[0]);
			strOpcion.append ("+");
			strOpcion.append (dato[1]);
			strOpcion.append ("-");
			strOpcion.append (dato[2]);
			strOpcion.append ("=");
			strOpcion.append (dato[3]);
			strOpcion.append ("'>");
			strOpcion.append (dato[3]);
			strOpcion.append ("\n");
		  }
		if(datos != null) {
			datos.clear();
			datos = null;
		}
		return strOpcion.toString();
	  }

/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int agregaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): IniciaBaja.", EIGlobal.NivelLog.ERROR);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): tipoBaja." + (String)req.getParameter("radioTipoCta"), EIGlobal.NivelLog.ERROR);

		int radioTipoCta=Integer.parseInt((String)req.getParameter("radioTipoCta"));

		String cmbCtasTerceros    = (String) req.getParameter( "cmbCtasTerceros" );
		/**GAE**/
		String folioBajaLote = req.getParameter( "folioBajaLote" );
		Vector foliosBaja = null;

		if(req.getSession().getAttribute("foliosBaja") == null){
		 	foliosBaja = new Vector();
		}else{
			foliosBaja = (Vector)req.getSession().getAttribute("foliosBaja");
		}
		/**FIN GAE**/

		String strTramaBN=(String)req.getParameter("strTramaBN");

		String numeroCuenta="";
		String nombreTitular="";

		String cvePlaza="";
		String strPlaza="";
		String cveBanco="";
		String strBanco="";
		String strSuc="";
		String strInterme="";
		String tipoCuenta="";

		String strCtaTmp="";

		String modulo="1";

		/*
		 * nombre "+" plaza_banxico "-" cve_interme "=" num_cuenta_ext ">"
		 */

		switch (radioTipoCta)
		 {
			case 0: // Banco Nacional
			       EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): Banco Nacional.", EIGlobal.NivelLog.ERROR);

				   numeroCuenta  = cmbCtasTerceros.substring(cmbCtasTerceros.indexOf("=") + 1, cmbCtasTerceros.length());
				   nombreTitular = cmbCtasTerceros.substring( 0, cmbCtasTerceros.indexOf( '+' ) );
				   cvePlaza = cmbCtasTerceros.substring(cmbCtasTerceros.indexOf("+") + 1, cmbCtasTerceros.indexOf('-'));
				   strInterme = cmbCtasTerceros.substring(cmbCtasTerceros.indexOf("-") + 1, cmbCtasTerceros.indexOf('='));
				   cveBanco = numeroCuenta.substring(0, 3);
				   CatNominaInterbDAO dao = new CatNominaInterbDAO();
				   String [] datosBco = dao.obtenDatosBanco(Integer.parseInt(cveBanco));
				   dao = null;
				   strBanco = datosBco[0];
				   datosBco = null;
				   tipoCuenta="40";

				   strTramaBN +=
					         numeroCuenta		+ pipe +
					         nombreTitular		+ pipe +
					         cvePlaza			+ pipe +
					         strPlaza			+ pipe +
					         cveBanco			+ pipe +
					         strBanco			+ pipe +
					         strSuc				+ pipe +
					         strInterme			+ pipe +
					         tipoCuenta			+ "@";


				   // Traer datos


			       break;
			case 1: //Baja Masiva de Cuentas

				EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): baja masiva de cuentas, folio->" + folioBajaLote, EIGlobal.NivelLog.ERROR);
				if(folioBajaLote!=null && !folioBajaLote.equals("")){
					if(existeLote(folioBajaLote)){
						if(folioRepetido(foliosBaja, folioBajaLote)){
							req.setAttribute("errorLote", "El lote ya se encuentra seleccionado");
						}else{
							foliosBaja.add(folioBajaLote);
						}
						req.getSession().setAttribute("foliosBaja", foliosBaja);
					}else{
						req.setAttribute("errorLote", "El lote seleccionado no existe");
					}
				}else{
					req.setAttribute("errorLote", "El lote seleccionado no puede ser vacio");
				}
				break;
			/**FIN GAE**/
		 }

		if(!numeroCuenta.equals("")){
			if(req.getSession().getAttribute("CuentasListaBaja") == null){
				List cuentas = new ArrayList();
				cuentas.add(numeroCuenta);
				req.getSession().setAttribute("CuentasListaBaja",cuentas);
			}else{
				List cuentas = (List)req.getSession().getAttribute("CuentasListaBaja");
				cuentas.add(numeroCuenta);
				req.getSession().setAttribute("CuentasListaBaja",cuentas);
			}
		}


		EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): Finalizando baja.", EIGlobal.NivelLog.ERROR);

		req.setAttribute("Modulo", "1");

		req.setAttribute("tablaBN", generaTablaCuentas(strTramaBN,false));
		req.setAttribute("strTramaBN",strTramaBN);
		req.setAttribute("tablaLotes", generaTablaLotes(foliosBaja));
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja &gt; Agregar","s26090h", req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("comboCuentas", traeCuentas(session.getContractNumber()==null?"":session.getContractNumber().toString()));

        EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): Finalizando Baja.", EIGlobal.NivelLog.ERROR);
		evalTemplate("/jsp/CatNomInterbBaja.jsp", req, res);
		return 1;

	}


/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int presentaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas(): IniciaBaja.", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String cuentasSel=(String)req.getParameter("cuentasSel");
		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas(): Seleccionadas "+ cuentasSel, EIGlobal.NivelLog.INFO);

		String strTramaBN=(String)req.getParameter("strTramaBN");

		int reg=0;
		int BNr=0;

		/**GAE**/
		int r = 0;
		String msg = null;
		String cuentasSelByLote = "";
		String cuentasSelTmp = "";
		Integer conCuentasSelByLote = new Integer("0");
		boolean flag = false;
		Integer cuentasCanceladas = null;
		Vector foliosBaja = (Vector)req.getSession().getAttribute("foliosBaja");
		String cuentasNoAut = null;
		if(foliosBaja!=null){
			String[] tramas = getTramas(foliosBaja, cuentasSel);
			strTramaBN += tramas[0];
			cuentasNoAut = tramas[1];
			conCuentasSelByLote = new Integer(tramas[2]);
		}

		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas() + Cuentas seleccionadas antes masivo->"+cuentasSel, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas() + Iteraciones antes masivo->"+conCuentasSelByLote.intValue(), EIGlobal.NivelLog.INFO);
		if(foliosBaja!=null){
			for(int l=0; l<conCuentasSelByLote.intValue(); l++){
				cuentasSelByLote += "1";
				r++;
			}
			for(int g=0; g<(cuentasSel.length()-foliosBaja.size()); g++){
				cuentasSelTmp += cuentasSel.charAt(g);
			}
			cuentasSel = cuentasSelTmp + cuentasSelByLote;
		}
		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas() - Cuentas seleccionadas despues masivo->"+cuentasSel, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas() Iteraciones despues masivo->"+r, EIGlobal.NivelLog.INFO);

		/**FIN GAE**/

		if(strTramaBN.length()>=1){
			String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/" + session.getUserID8()+".txt";
			String arcLinea="";
			EI_Tipo BN= new EI_Tipo();
			BN.iniciaObjeto(strTramaBN);
			if(BN.totalRegistros>=1)
	        {

	          EI_Exportar ArcSal=new EI_Exportar(nombreArchivo);
	          EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);
	          if(ArcSal.creaArchivo())
	           {
	             arcLinea="";
	            // int c[]={0, 1, 3, 4, 5, 7};			// BN Indice del elemento deseado
	            // int b[]={20,40,5,5,20,5};		// BN Longitud maxima del campo para el elemento
	             int c[]={0,1,4,2,6,7};			// BN Indice del elemento deseado
	             int b[]={20,40,5,5,5,2};		// BN Longitud maxima del campo para el elemento

	             EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas(): Exportando Cuentas Bancos nacionales.", EIGlobal.NivelLog.INFO);
	             for(int i=0;i<BN.totalRegistros;i++)
	              {
					if(cuentasSel.charAt(reg+i)=='1')
					  {
						arcLinea="EXTRNA";
						for(int j=0;j<c.length;j++)
						  arcLinea+=ArcSal.formateaCampo(BN.camposTabla[i][c[j]],b[j]);
						arcLinea+="\n";
						if(!ArcSal.escribeLinea(arcLinea))
						  EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas()s(): Algo salio mal escribiendo: "+arcLinea, EIGlobal.NivelLog.INFO);
						BNr++;
					  }
	              }
				 reg+=BN.totalRegistros;

	             ArcSal.cierraArchivo();
	             flag = true;
	             msg = "Se agregaron " +Integer.toString(BNr)+" registros para solicitud de Baja";
	           }
	          else
	           {
	             //strTabla+="<b>Exportacion no disponible<b>";
	             EIGlobal.mensajePorTrace("CatNomInterbBaja - presentaCuentas(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
	           }
	        }
			req.setAttribute("nombreArchivoImp",nombreArchivo);
		}

		/**GAE**/
		if(foliosBaja!=null){
			try{
			cuentasCanceladas = new Integer(cuentasNoAut);
				if(cuentasCanceladas.intValue()>0){
					   req.setAttribute("cuentasNoAut", cuentasNoAut);
					   req.setAttribute("msgCuentasCanceladas", "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
							   + "<tr>"
							   + "<td colspan=5 class=\"textabdatcla\"> "
							   + "<font color=red size=3>Nota:</font><font size=2>El total de cuentas a cancelar es de " + cuentasCanceladas.toString()
							   + "</font></td></tr></table></td></tr>");
				}
			}catch(Exception e){
			EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Error al cancelar las cuentas.", EIGlobal.NivelLog.INFO);
			}
			req.setAttribute("flag", new Boolean(flag));
			if(cuentasCanceladas.intValue()>0){
				if(msg!=null){
					req.setAttribute("msg", msg + " y " + cuentasCanceladas.toString() + " para Cancelaci&oacute;n.");
				}else{
					req.setAttribute("msg", "Se agregaron " + cuentasCanceladas.toString() + " registros para solicitud de cancelaci&oacute;n.");
				}
			}else{
				req.setAttribute("msg", msg);
			}
	    }else{
			req.setAttribute("flag", new Boolean(flag));
			req.setAttribute("msg", "Se agregaron " +Integer.toString(BNr)+" registros para solicitud de Baja.");
		}
		/**FIN GAE**/

		req.setAttribute("totalRegistros",Integer.toString(BNr));
		req.setAttribute("numeroCtasNacionales",Integer.toString(BNr));
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Baja de Cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja","s26092h", req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

        EIGlobal.mensajePorTrace("CatNomInterbBaja - agregaCuentas(): Finalizando Baja.", EIGlobal.NivelLog.ERROR);
		evalTemplate("/jsp/CatNomInterb_BajaArchivo.jsp", req, res);
		return 1;
	}


/*************************************************************************************/
/************************************************** Pantalla Principal - Modulo=0    */
/*************************************************************************************/
	public int enviaCuentas( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("CatNomInterbBaja - enviaCuentas(): IniciaBaja.", EIGlobal.NivelLog.ERROR);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String nombreArchivoImp=(String)req.getParameter("nombreArchivoImp");
		String totalRegistros=(String)req.getParameter("totalRegistros");
		/**GAE**/
		Integer cuentasCanceladas = null;
		String cuentasNoAut = (String)req.getParameter("cuentasNoAut");
		Vector foliosBaja = (Vector)req.getSession().getAttribute("foliosBaja");
		/**FIN GAE**/
		String strMensaje="";
		String nombreArchivo="";

		EIGlobal.mensajePorTrace("CatNomInterbBaja - enviaCuentas(): nombreArchivoImp: "+nombreArchivoImp, EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("CatNomInterbBaja - enviaCuentas(): totalRegistros: "+totalRegistros, EIGlobal.NivelLog.ERROR);

		String cabecera="";
		String regServicio="";
		String Trama="";
		String Result="";

		String medioEntrega	="";
		String usuario ="";
		String contrato="";
		String clavePerfil="";
		String tipoOperacion="";

		String line="";

		String registroDeSolicitud="";
		String registroDeBaja="";
		String folioRegistro="";

		StringBuffer lineDuplicados=new StringBuffer("");

		boolean consulta=false;

		/*
			0 Envio Correcto
		    1 Error en Envio
			2 Envio con errores
			3 Envio correcto (Pero no se pudo identificar si hubo duplicidad)
		*/

		HashMap facultadesBajaCtas = new HashMap ();
		facultadesBajaCtas=(HashMap)req.getSession ().getAttribute ("facultadesBajaCtas");

		ServicioTux TuxGlobal = new ServicioTux();
		//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		nombreArchivo=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf('/')+1);
		EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Nombre para la copia "+nombreArchivo, EIGlobal.NivelLog.INFO);

		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Se continua con el envio del archivo", EIGlobal.NivelLog.INFO);
//desde aqui
/*		if( !archR.copiaLocalARemotoCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) )
		 {
			EIGlobal.mensajePorTrace( "CatNomInterbBaja-> No se realizo la copia remota a tuxedo.", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Se detiene el proceso pues no se envia el archivo.", EIGlobal.NivelLog.INFO);
			despliegaPaginaErrorURL("<i><font color=red>Error</font></i> de comunicaciones.<br><br> Por favor realice la importaci&oacute;n <font color=green> nuevamente </font>.<br> El archivo <font color=blue><b> "+nombreArchivo+ " </b></font> no ha sido enviado correctamente.<br><br>","Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja","s55205h", req, res);
			return 1;
		 }
		else
		 {
			EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Copia remota OK.", EIGlobal.NivelLog.INFO);

			cabecera=	medioEntrega		+ pipe +
						usuario				+ pipe +
						tipoOperacion		+ pipe +
						contrato			+ pipe +
						usuario				+ pipe +
						clavePerfil			+ pipe;

			regServicio=
						contrato			+ "@" +
						nombreArchivo		+ "@" +
			            totalRegistros		+ "@" ;

			Trama=cabecera + regServicio;
			EIGlobal.mensajePorTrace("CatNomInterbBaja->  Trama entrada envio: "+Trama, EIGlobal.NivelLog.DEBUG);

				try
				 {
					 Hashtable hs = TuxGlobal.web_red(Trama);
					 Result= (String) hs.get("BUFFER") +"";
				 }catch( java.rmi.RemoteException re )
				  {
					re.printStackTrace();
				  } catch (Exception e) {}

			EIGlobal.mensajePorTrace("CatNomInterbBaja-> Trama salida envio: "+Result, EIGlobal.NivelLog.DEBUG);
			if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			if(req.getSession().getAttribute("CuentasListaBaja") != null){
				List cuentas = (List)req.getSession().getAttribute("CuentasListaBaja");
				Iterator i = cuentas.iterator();
				while(i.hasNext()){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);

					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EA_CUENTAS_BAJA_REALIZA_BAJA);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if(Result != null){
						if(Result.substring(0,2).equals("OK")){
							bt.setIdErr(tipoOperacion + "0000");
							log("VSWF: IdErr " + bt.getIdErr());
						}else if(Result.length() > 8){
							bt.setIdErr(Result.substring(0,8));
						}else{
							bt.setIdErr(Result.trim());
						}
					}
					if (tipoOperacion != null) {
						bt.setServTransTux(tipoOperacion);
					}
					if (nombreArchivo != null) {
						bt.setNombreArchivo(nombreArchivo);
					}
					bt.setCctaDest(i.next().toString());

					BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			}
			req.getSession().removeAttribute("CuentasListaBaja");


			if(Result==null || Result.equals(""))
			 {
			   EIGlobal.mensajePorTrace("CatNomInterbBaja-> El servidor regreso null se procesa el mensaje de error", EIGlobal.NivelLog.DEBUG);
			   despliegaPaginaErrorURL("Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.","Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja","s55205h", req, res);
			   return 1;
			 }
			else
			 {
				EIGlobal.mensajePorTrace("CatNomInterbBaja-> La informacion se recibio, se evalua el contenido", EIGlobal.NivelLog.DEBUG);
				EI_Tipo trRes=new EI_Tipo();
				if( !(Result.trim().substring(0,2).equals("OK")) )
				 {
					consulta=false;
					nombreArchivo=Result.substring(Result.indexOf("/"));
					EIGlobal.mensajePorTrace("CatNomInterbBaja-> La consulta no fue exitosa.", EIGlobal.NivelLog.INFO);
				 }
				else
				 {
					consulta=true;
					trRes.iniciaObjeto('@','|',Result+"|");
					nombreArchivo=trRes.camposTabla[0][2];
					EIGlobal.mensajePorTrace("CatNomInterbBaja-> La consulta fue exitosa.", EIGlobal.NivelLog.INFO);
				 }

				EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Nombre de archivo obtenido:  "+nombreArchivo, EIGlobal.NivelLog.INFO);

				if( !archR.copiaCUENTAS(nombreArchivo,Global.DIRECTORIO_LOCAL) )
				 {
				   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> No se realizo la copia remota desde tuxedo.", EIGlobal.NivelLog.INFO);
				   nombreArchivo="-1";
				   if(consulta)
					  strMensaje="<br>El archivo fue enviado pero <font color=red>no</font> se pudo obtener la <font color=green>informaci&oacute;n</font> de registro.<br><br>Verifique en la <font color=blue>Autorizaci&oacute;n y Cancelaci&oacute;n </font> de cuentas para obtener el <b>registro</b> de la informaci&oacute;n <i>procesada</i>.";
				   else
					  strMensaje="<br><b><font color=red>Transacci&oacute;n</font></b> no exitosa.<br><br>El archivo <font color=blue>no</font> fue enviado correctamente.<br>Porfavor <font color=green>intente</font> en otro momento.";
				 }
				else
				 {
				   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Copia remota desde tuxedo se realizo correctamente.", EIGlobal.NivelLog.INFO);
				   BufferedReader arc;
				   try
					 {
					   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> El archivo que se trajo (E): "+ nombreArchivo, EIGlobal.NivelLog.INFO);
					   nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
					   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> El archivo que se abre (E): "+nombreArchivo, EIGlobal.NivelLog.INFO);

					   arc=new BufferedReader(new FileReader(nombreArchivo));
					   if((line=arc.readLine())!=null)
						{
						  EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Se leyo del archivo: "+line, EIGlobal.NivelLog.INFO);
						  if(!consulta)
						   {
							 if(line.trim().length()>16)
							   strMensaje=line.trim().substring(16);
							 else
							   strMensaje="Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.";
						   }
						  else
						   {
							 EI_Tipo Sec=new EI_Tipo();
							 Sec.iniciaObjeto(';','|',line+"|");
							 registroDeSolicitud=Sec.camposTabla[0][0].trim();
							 registroDeBaja=Sec.camposTabla[0][1].trim();
							 folioRegistro=Sec.camposTabla[0][2].trim();

							 strMensaje="<br>La informaci&oacute;n fue <font color=blue>enviada</font> y <b>procesada</b> por el sistema. <br>Para verificar el <font color=green>estatus</font> de las cuentas enviadas presione el bot&oacute;n Ver Detalle.<br><br>";

							 EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Cuentas registradas para solicitud de baja: "+registroDeSolicitud, EIGlobal.NivelLog.INFO);
							 EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Cuentas registradas para solicitud de baja: "+registroDeBaja, EIGlobal.NivelLog.INFO);
							 EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Folio de registro para el envio: "+folioRegistro, EIGlobal.NivelLog.INFO);
						   }
						}
					   try
						  {
							arc.close();
						  }catch(IOException a){}
					 }
				   catch(IOException e)
					 {EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Error al leer el archivo de tuxedo.", EIGlobal.NivelLog.INFO);}
				 }
			 }
		 }

	   try{
		   System.out.println("checa1->"+cuentasNoAut);
		   if(cuentasNoAut!=null && !cuentasNoAut.equals("")){
			   System.out.println("checa2->"+cuentasNoAut);
			   cuentasCanceladas = cancelaCuentas(cuentasNoAut, foliosBaja);
			   if(cuentasCanceladas.intValue()>0){
				   req.setAttribute("msgCuentasCanceladasFinal", "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
						   									   + "<tr><td colspan=5 class=\"textabdatcla\"> "
						   									   + "<br>Se cancelaron un total de <font color=red>" + cuentasCanceladas.toString() + "</font> cuentas.<br><br>nuevo estado <font color=blue>Cancelada</font>"
						   									   + "</td></tr></table></td></tr>");
			   }else {
				   req.setAttribute("msgCuentasCanceladasFinal", "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
														   	   + "<tr><td colspan=5 class=\"textabdatcla\"> "
															   + "<font color=red><br>Error al tratar de Cancelar las cuentas.<br><br>Por favor vuelva a intentar</font>"
															   + "</td></tr></table></td></tr>");
			   }
		   }

		   req.getSession().removeAttribute("foliosBaja");
	   }catch(Exception e){
		   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Error al cancelar las cuentas.", EIGlobal.NivelLog.INFO);
	   }
	*/
		// hasta aqui

		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> enviaCuentas() - grabaCuentas.", EIGlobal.NivelLog.INFO);
 		CatNominaInterbDAO dao = new CatNominaInterbDAO();
 		String facultad = "";
 		// Se validan las mismas facultades que para las altas
 		if (session.getFacultad(session.FAC_AUTO_AB_OB))
 			facultad = "AUTO";
 		else
 		if(session.getFacultad(session.FAC_ALTA_BN))
 			facultad = "ALTA";

 		RespInterbBean resp = dao.bajaCuentasInterb(nombreArchivoImp, contrato, usuario, facultad);
 		long lote = resp.getLote();
 		int rechazados = resp.getRechazados();
 		dao = null;
		EIGlobal.mensajePorTrace( "CatNomInterbRegistro-> agregaCuentas() - El LOTE: [" + lote + "]", EIGlobal.NivelLog.INFO);


	    if(lote == 0) {
			strMensaje="Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Por favor intente mas tarde.<br><br>";
			despliegaPaginaErrorURL(strMensaje,"Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas &gt; Baja","s55205h", req, res);
	    }
	    else
		 {
	    	/**GAE**/
	    	if(foliosBaja!=null){
	    		// insertBajaLote(foliosBaja);
	    	}
			req.setAttribute("flagFin", new Boolean(true));
			/**FIN GAE**/

	    	strMensaje="<br>La informaci&oacute;n fue <font color=blue>enviada</font> y <b>procesada</b> por el sistema.<br>";
			strMensaje+="<br>En caso de haberse generado rechazos favor de consultar el detalle en la opci&oacute;n de <b>Autorizaci&oacute;n y Cancelaci&oacute;n.</b><br>";
			req.setAttribute("strMensaje",strMensaje);
			req.setAttribute("nombreArchivoImp",nombreArchivoImp);
/*			req.setAttribute("nombreArchivoDetalle",nombreArchivo);
			req.setAttribute("registroDeSolicitud",registroDeSolicitud);
			req.setAttribute("registroDeBaja",registroDeBaja); */
			req.setAttribute("totalRegistros",totalRegistros);
			req.setAttribute("rechazados","" + rechazados);
			req.setAttribute("respBean", resp);

			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Baja de cuentas","Administraci&oacute;n y control &gt; Cuentas  &gt; Baja &gt; Env&iacute;o","s26094h", req));

			req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
			req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
			req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
			req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

			try{
				EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

				int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				String claveOperacion = BitaConstants.BAJA_CAT_NOMINA_INTERB;
				String concepto = BitaConstants.CONCEPTO_BAJA_CAT_NOMINA_INTERB;

				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

			} catch(Exception e) {
				EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

			}
			EIGlobal.mensajePorTrace("CatNomInterbBaja - enviaCuentas(): Finalizando Baja.", EIGlobal.NivelLog.ERROR);
			evalTemplate("/jsp/CatNomInterb_BajaEnvio.jsp", req, res);
		}
	  return 1;
	}


	/**
	 * Metodo de envio alternativo para enviar solo cuentas canceladas, dado que
	 * no se crea archivo de tuxedo
	 *
	 * @param req					Request de la petici�n
	 * @param res					Response de la petici�n
	 * @return						1 en caso de que todo termine bien
	 * @throws ServletException		Error de tipo ServletException
	 * @throws IOException			Error de tipo IOException
	 */
	public int enviaCuentasCancel( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException{
		Integer cuentasCanceladas = null;
		String cuentasNoAut = (String)req.getParameter("cuentasNoAut");
		Vector foliosBaja = (Vector)req.getSession().getAttribute("foliosBaja");
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario=(session.getUserID8()==null)?"":session.getUserID8();
		String contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		String facultad = "";
		String nombreArchivoImp = (String)req.getAttribute("nombreArchivoImp");
 		// Se validan las mismas facultades que para las altas
 		if (session.getFacultad(session.FAC_AUTO_AB_OB))
 			facultad = "AUTO";
 		else
 		if(session.getFacultad(session.FAC_ALTA_BN))
 			facultad = "ALTA";
		try{
			   if(cuentasNoAut!=null){
				   System.out.println("checa->"+cuentasNoAut);
				   cuentasCanceladas = cancelaCuentas(cuentasNoAut, foliosBaja,
						   nombreArchivoImp, contrato, usuario, facultad);
				   if(cuentasCanceladas.intValue()>0){
					   if(foliosBaja!=null){
						   //insertBajaLote(foliosBaja);
					   }
					   req.setAttribute("msgCuentasCanceladasFinal", "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
							   									   + "<tr><td colspan=5 class=\"textabdatcla\"> "
							   									   + "<br>Se cancelaron un total de <font color=red>" + cuentasCanceladas.toString() + "</font> cuentas.<br><br>nuevo estado <font color=blue>Cancelada</font>"
							   									   + "</td></tr></table></td></tr>");
				   }else {
					   req.setAttribute("msgCuentasCanceladasFinal", "<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\"> "
															   	   + "<tr><td colspan=5 class=\"textabdatcla\"> "
																   + "<font color=red><br>Error al tratar de Cancelar las cuentas.<br><br>Por favor vuelva a intentar</font>"
																   + "</td></tr></table></td></tr>");
				   }
			   }

		   }catch(Exception e){
			   EIGlobal.mensajePorTrace( "CatNomInterbBaja-> Error al cancelar las cuentas.", EIGlobal.NivelLog.INFO);
		   }

		   req.setAttribute("strMensaje","");
		   req.setAttribute("nombreArchivoImp","");
		   req.setAttribute("nombreArchivoDetalle","");
		   req.setAttribute("registroDeSolicitud","0");
		   req.setAttribute("registroDeBaja","0");
		   req.setAttribute("totalRegistros","0");
		   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

		   req.getSession().removeAttribute("foliosBaja");
		   req.setAttribute("flagFin", new Boolean(false));
		   req.setAttribute("newMenu", session.getFuncionesDeMenu());
		   req.setAttribute("MenuPrincipal", session.getStrMenu());
		   req.setAttribute("Encabezado", CreaEncabezado("Baja de cuentas","Servicios &gt; N&oacute;mina Interbancaria &gt; Registro de Cuentas &gt; Baja","s26094h", req));

		   EIGlobal.mensajePorTrace("CatNomInterbBaja - enviaCuentasCancel(): Finalizando Baja.", EIGlobal.NivelLog.ERROR);
		   evalTemplate("/jsp/CatNomInterb_BajaEnvio.jsp", req, res);
		   return 1;
	}

/*************************************************************************************/
/************************************************************  generaTablaCuentasMB  */
/*************************************************************************************/
 String generaTablaCuentas(String strCtaTmp,boolean titulo)
	{
		if(strCtaTmp.trim().equals(""))
			return "";

		String [] titulos={"",
					   "Cuenta",
					   "Titular",
					   "Plaza",
					   "Banco" /*,
					   "Sucursal"*/};

/*		int[] datos={5,2,0,1,3,5,6};
		int[] values={0,0,1,2,3,4,5,6};
		int[] align={1,0,0,0,1,0}; */

		int[] datos={5,2,0,1,2,5,6};
		int[] values={0,0,1,2,3,4,5,6};
		int[] align={1,0,0,0,1,0};


		if(titulo)
		 {
		   datos[0]=2;
		   titulos[0]="Cuentas Mismo Banco";
		 }
		else
		 {
		   datos[0]=4;
		   titulos[0]="Cuentas Bancos Nacionales";
		 }

		EI_Tipo ctaMB=new EI_Tipo();

		ctaMB.iniciaObjeto(strCtaTmp);
		return ctaMB.generaTabla(titulos,datos,values,align);
	}

/*************************************************************************************/
/************************************************************  generaTablaCuentasBI  */
/*************************************************************************************/
 String generaTablaCuentasBI(String strCtaTmp,String titulo)
	{
		if(strCtaTmp.trim().equals(""))
			return "";

		String [] titulos={"",
					   "Cuenta",
					   "Titular",
			           "Clave Divisa",
			           "Divisa",
					   "Pais",
					   "Banco",
					   "Ciudad",
					   "Clave ABA"};

		titulos[0]=titulo;

        /*
		0 numeroCuentaBI
		1 nombreTitularBI
		2 paisBI
		3 strCiudadBI
		4 strBancoBI
		5 cveDivisaBI
		6 strDivisaBI
		7 numeroCuentaBI
		8 strClaveABABI
		*/

		int[] datos={8,2,0,1,5,6,2,4,3,8};
		int[] values={0,0,1,2,3,4,5,6,7};
		int[] align={1,0,0,0,1,0,0,0,0,0};

		EI_Tipo ctaMB=new EI_Tipo();

		ctaMB.iniciaObjeto(strCtaTmp);
		return ctaMB.generaTabla(titulos,datos,values,align);
	}

/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace("CatNomInterbBaja - despliegaPaginaErrorURL(): Generando error.", EIGlobal.NivelLog.INFO);

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3="s55205h";

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "CatNomInterbBaja?Modulo=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

/*************************************************************************************/
/****************************************************************** trae Divisas     */
/*************************************************************************************/
public String traeClave(String tipo,String dato)
 {
	String strOpcion="";
	String sqlQuery ="";
	Vector result=null;

	if(tipo.equals("PAIS"))
	  sqlQuery = "SELECT CVE_PAIS from COMU_PAISES where upper(DESCRIPCION) like '%"+dato.toUpperCase() + "%' order by CVE_PAIS";
	if(tipo.equals("DIVISA"))
	  sqlQuery = "SELECT CVE_DIVISA FROM COMU_MON_DIVISA where upper(DESCRIPCION) like '%"+dato.toUpperCase() + "%' order BY UPPER(CVE_DIVISA)";
	/**GAE**/
	if(tipo.equals("STRDIVISA"))
	  sqlQuery = "SELECT upper(DESCRIPCION) FROM COMU_MON_DIVISA where CVE_DIVISA like '%"+dato.toUpperCase() + "%' order BY UPPER(CVE_DIVISA)";
	/**FIN GAE**/
	EI_Query BD= new EI_Query();
	result=BD.ejecutaQueryCampo(sqlQuery);
	if(result==null && result.size()<=0)
		return dato;
	strOpcion=(String)result.get(0);
	return strOpcion;

  }

	/**
	 * GAE
	 * Metodo encargado de construir la tabla de folio de lotes para baja masiva
	 *
	 * @param foliosBaja			Vector con los Folios de lote por carga
	 * @return table				Tabla con la descripci�n detallada de los
	 * 								lotes a eliminar.
	 */
	private String generaTablaLotes(Vector foliosBaja) {
		StringBuffer table = new StringBuffer();
		String colLin = null;
		if(foliosBaja==null){
			return "";
		}
		if(foliosBaja.size()==0){
			return "";
		}
		table.append("<table border=0 cellspacing=2 cellpadding=3 align=center bgcolor='#FFFFFF'>");
		table.append("<tr><td class='textittab'colspan=3 align=center>Lotes de Carga de Cuentas</td></tr>");
		table.append("<tr>");
		table.append("<th class='tittabdat'><br></th>");
		table.append("<th class='tittabdat'> &nbsp; Lote &nbsp; </th>");
		table.append("<th class='tittabdat'> &nbsp; Fecha Carga &nbsp; </th>");
		table.append("<th class='tittabdat'> &nbsp; Cuentas Otros Bancos &nbsp; </th>");
		table.append("</tr>");

		for(int i=0; i<foliosBaja.size(); i++){
			table.append("<tr>");
			if((i%2)==0)
				colLin="class='textabdatobs'";
		    else
		        colLin="class='textabdatcla'";
			table.append("<td "+ colLin + "align=center><input type=checkbox name='idLote1' CHECKED></td>");
			table.append("<td "+ colLin + "align=center>&nbsp;"+ (String)foliosBaja.get(i) +"&nbsp;<br></td>"); //lote
			table.append("<td "+ colLin + "align=center>&nbsp;"+ getFechLote((String)foliosBaja.get(i)) +"&nbsp;<br></td>");
			table.append("<td "+ colLin + "align=center>&nbsp;"+ getCuentasLote((String)foliosBaja.get(i),"='E'") +"&nbsp;<br></td>");
			table.append("</tr>");
		}
		table.append("</table>");
		return table.toString();
	}

	/**
	 * GAE
	 * Metodo encargado de obtener la fecha de carga del lote
	 *
	 * @param folioLote		Folio del lote de carga
	 * @return fechLote		Fecha del lote de carga
	 * @throws Exception	Error de tipo Exception
	 */
	private String getFechLote(String folioLote){
		String query = null;
		String fechLote = null;
		ResultSet rs = null;
		try {
			Connection conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			// conn= createiASConn (Global.DATASOURCE_ORACLE );
			Statement sDup = conn.createStatement();
			query = "select to_char(trunc(fch_env),'DD-MM-YYYY') as fechLote "
				  + "from eweb_cat_nom_aut_ext "
				  + "where id_lote=" + folioLote;
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::getFechLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				fechLote = rs.getString("fechLote");
			}
			rs.close();
			sDup.close();
			conn.close();
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::getFechLote:: -> Exception: [" + e + "]", EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace ("CatNomInterbBaja::getFechLote:: -> Fecha: [" + fechLote + "]", EIGlobal.NivelLog.INFO);
		return fechLote;
	}

	/**
	 * GAE
	 * Metodo funcional para obtener la fecha del lote de carga
	 *
	 * @param folioLote		Folio de carga del lote
	 * @param tipoCuenta	Valor entero para identificar el tipo de cuenta
	 * @return counter		Contador seg�n el tipo de cuenta
	 * @throws Exception	Error de tipo Exception
	 */
	private int getCuentasLote(String folioLote, String tipoCuenta){
		String query = null;
		int counter = 0;
		ResultSet rs = null;

		try {
			Connection conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			Statement sDup = conn.createStatement();
			query = "Select count(num_cuenta_ext) as counter "
				  + "from eweb_cat_nom_aut_ext "
				  + "where id_lote=" + folioLote;
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::getCuentasLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				counter = rs.getInt("counter");
			}
			rs.close();
			sDup.close();
			conn.close();
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::getCuentasLote:: -> Exception: [" + e + "]", EIGlobal.NivelLog.INFO);
		}
		return counter;
	}

	/**
	 * GAE
	 * Metodo encargado de regresar las tramas correspondientes a cuentas
	 * Santader, otros Bancos e Internacionales que tengan estatus_oper en 'A'
	 * (Autorizadas)
	 *
	 * @param foliosBaja		Vector contenedor de los folios de los lotes
	 * @param cuentasSel		Cuentas Seleccionadas desde el front
	 * @return tramasAut		Arreglo contenedor de cuentas autorizadas
	 * 							Posici�n 0 - Cuentas Santander
	 *						  	Posici�n 1 - Cuentas Otros Bancos
	 *						  	Posici�n 2 - Cuentas Internacionales
	 */
	private String[] getTramas(Vector foliosBaja, String cuentasSel) {
		String[] tramasAut = new String[3];
		String idLote = null;
		String strTramaBN = "";
		Integer cuentasNoAut = new Integer("0");
		Integer numCuentasByLote = new Integer("0");
		String[] foliosActivos = null;

		foliosActivos = dameFoliosActivos(cuentasSel, foliosBaja.size());
		for(int i=0; i<foliosBaja.size(); i++){
			if(foliosActivos[i].equals("1")){
				idLote = (String)foliosBaja.get(i);
				strTramaBN += getTramasDetails(idLote, 0);
				cuentasNoAut = new Integer(cuentasNoAut.intValue()
							 + new Integer(getTramasDetails(idLote, 1)).intValue());
				numCuentasByLote = new Integer(numCuentasByLote.intValue()
								 + dameNumCuentasByLote(idLote).intValue());
			}
		}
		tramasAut[0] = strTramaBN;
		tramasAut[1] = "" + cuentasNoAut;
		tramasAut[2] = "" + numCuentasByLote.intValue();

		return tramasAut;
	}

	/**
	 * GAE
	 * Metodo encargado de analizar que folios fueron seleccionados desde el
	 * front
	 *
	 * @param cuentasSel		Seleccion desde el front
	 * @param lonVector			Numero de lotes del front
	 * @return foliosActivos	Banderas de selecci�n por lote
	 * @throws Exception		Error de tipo Exception
	 */
	private String[] dameFoliosActivos(String cuentasSel, int lonVector){
		String[] foliosActivos = null;
		int totalCuentasSel = 0;
		int totalFoliosSel = 0;

		try{
			totalCuentasSel = cuentasSel.length();
			totalFoliosSel = lonVector;
			foliosActivos = new String[totalFoliosSel];
			for(int i=0; i<foliosActivos.length; i++){
				foliosActivos[i] = "" + cuentasSel.charAt(totalCuentasSel-totalFoliosSel);
				totalFoliosSel--;
			}

			//Solo para Debug
			for(int j=0; j<foliosActivos.length; j++){
				System.out.println("Folio " + (j+1) + ":" + foliosActivos[j]);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return foliosActivos;
	}

	/**
	 * GAE
	 * Metodo encargado de ver cuantas cuentas se encuentran dentro del lote
	 *
	 * @param idLote			Folio de Carga Masiva
	 * @return counter			conteo final
	 * @throws Exception		Error de tipo Exception
	 */
	private Integer dameNumCuentasByLote(String idLote){
		String query = null;
		Integer counter = null;
		int c = 0;
		ResultSet rs = null;
		try {
			Connection conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			Statement sDup = conn.createStatement();
			query = "Select count(1) as counter "
				  + "from eweb_cat_nom_aut_ext "
				  + "where id_lote=" + idLote;
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::dameNumCuentasByLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);
			if(rs.next()){
				c = rs.getInt("counter");
			}
			counter = new Integer(c);
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::dameNumCuentasByLote: <<VALOR DE COUNTER>>" + counter.intValue(), EIGlobal.NivelLog.INFO);
			rs.close();
			sDup.close();
			conn.close();
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::dameNumCuentasByLote: Exception : [" + e + "]", EIGlobal.NivelLog.INFO);
		}
		return counter;
	}

	/**
	 * GAE
	 * Metodo encargado de formar las distintas tramas que provienen de la baja
	 * masiva
	 *
	 * @param idLote			Folio de Carga Masiva
	 * @param condicion			Tipo de cuenta: Santander, Otros Bancos o Inter
	 * @param where				Condici�n de consulta
	 * @return result			Trama Final
	 * @throws Exception		Error de tipo Exception
	 */
	private String getTramasDetails(String idLote, int condicion){
		String query = null;
		int counter = 0;
		ResultSet rs = null;
		StringBuffer strTramaBN = new StringBuffer("");
		String result = "";
		String numeroCuenta  = "";
        String nombreTitular ="";
        String cvePlaza = "";
        String strPlaza = "";
        String cveBanco = "";
        String strBanco = "";
        String strSuc   = "";
        String tipoCuenta = "";
        Connection conn = null;
		try {
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			Statement sDup = conn.createStatement();
			switch (condicion)
			 {
				case 0: // Banco Nacional
				       EIGlobal.mensajePorTrace("CatNomInterbBaja - getTramasDetails(): Banco Nacional.", EIGlobal.NivelLog.ERROR);
				       query = "Select num_cuenta_ext as numeroCuenta, "
				    	      + "titular as nombreTitular, "
				    	      + "plaza_banxico as cvePlaza, "
				    	      + "cve_interme as cveBanco, "
				    	      + "sucursal as strSuc "
							  + "from eweb_cat_nom_aut_ext "
							  + "where id_lote=" + idLote
							  + " and estatus_oper='A'";
						EIGlobal.mensajePorTrace ("CatNomInterbBaja::getTramasDetails:: -> Query:" + query, EIGlobal.NivelLog.INFO);
						rs = sDup.executeQuery(query);
						while(rs.next()){
							numeroCuenta = (rs.getString("numeroCuenta")!=null)?rs.getString("numeroCuenta"):"";
							nombreTitular = (rs.getString("nombreTitular")!=null)?rs.getString("nombreTitular"):"";
							cvePlaza = (rs.getString("cvePlaza")!=null)?rs.getString("cvePlaza"):"";
							strPlaza = ""; //traePlaza(cvePlaza.trim());
							cveBanco = (rs.getString("cveBanco")!=null)?rs.getString("cveBanco"):"";
							strBanco = traeBanco(cveBanco.trim());
							strSuc = (rs.getString("strSuc")!=null)?rs.getString("strSuc"):"";
							tipoCuenta = "40"; //(rs.getString("tipoCuenta")!=null)?rs.getString("tipoCuenta"):"";

							strTramaBN.append(numeroCuenta.trim()		+ pipe);
					        strTramaBN.append(nombreTitular.trim()		+ pipe);
					        strTramaBN.append(cvePlaza.trim()			+ pipe);
					        strTramaBN.append(strPlaza.trim()			+ pipe);
					        strTramaBN.append(cveBanco.trim()			+ pipe);
					        strTramaBN.append(strBanco.trim()			+ pipe);
					        strTramaBN.append(strSuc.trim()				+ pipe);
					        strTramaBN.append(tipoCuenta.trim()			+ "@");
						}
					   result = strTramaBN.toString();
				       break;

				case 1: //Cuentas no autorizadas
				       EIGlobal.mensajePorTrace("CatNomInterbBaja - getTramasDetails(): Cuentas no Autorizadas.", EIGlobal.NivelLog.ERROR);
				       query = "Select count(1) as cuentas "
							  + "from eweb_cat_nom_aut_ext "
							  + "where id_lote=" + idLote + " "
							  + "and estatus_oper in ('P', 'I')";
						EIGlobal.mensajePorTrace ("CatNomInterbBaja::getTramasDetails:: -> Query:" + query, EIGlobal.NivelLog.INFO);
						rs = sDup.executeQuery(query);
						numeroCuenta = "0";
						if(rs.next()){
							numeroCuenta = "" + rs.getInt("cuentas");
						}
						result = numeroCuenta;
				    	break;
			 }
			rs.close();
			sDup.close();
			conn.close();
		}catch (Exception e) {
			e.printStackTrace();
			try {
				conn.close();
			}catch(Exception e1){};
		}
		return result;
	}

	/**
	 * GAE
	 * Metodo encargado de cancelar las cuentas que no han sido autorizadas.
	 *
	 * @param cuentasNoAut			Cuentas a cancelar
	 * @return foliosBaja			Lotes
	 * @throws Exception			Error de tipo Exception
	 */
	private Integer cancelaCuentas(String cuentasNoAut, Vector foliosBaja,
			String archivo, String contrato, String usuario, String facultad)
			throws Exception{
		String query = null;
		int counter = 0;
		Integer count = new Integer("0");
		try {
				if(!cuentasNoAut.equals("") && !cuentasNoAut.equals("0")){
					Connection conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
					Statement sDup = conn.createStatement();
					for(int i=0; i<foliosBaja.size(); i++){
						counter = 0;
						query = "Update eweb_cat_nom_aut_ext set "
							  + "estatus_oper='C' "
							  + "where id_lote = " + (String)foliosBaja.get(i) + " "
							  + "and estatus_oper in ('P','I')";
						EIGlobal.mensajePorTrace ("CatNomInterbBaja::getCuentasLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
						counter = sDup.executeUpdate(query);
						EIGlobal.mensajePorTrace ("CatNomInterbBaja::getCuentasLote:: -> Actualizados:" + counter, EIGlobal.NivelLog.INFO);
						count = new Integer(count.intValue() + counter);
					}

					sDup.close();
					conn.close();
				}
		}catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}

		CatNominaInterbDAO dao = new CatNominaInterbDAO();
 		RespInterbBean resp = dao.bajaCuentasInterb(archivo, contrato, usuario, facultad);
 		dao = null;

		return count;
	}

	/**
	 * GAE
	 * Metodo prestado para traer la descripcion del banco
	 *
	 * @param cveBanco		Clave del banco
	 * @return resBanco		Descripcion del banco
	 */
	private String traeBanco( String cveBanco )
	{
		String totales = "";
		String resBanco = cveBanco;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
	    Connection conn = null;

		try
		{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
		  	totales = "Select nombre_corto from comu_interme_fin where num_cecoban<>0 " +
		  			  "and cve_interme='" + cveBanco + "'";

		  	psCont = conn.prepareStatement( totales );
		  	rsCont = psCont.executeQuery();
		  	rsCont.next();
		  	resBanco = rsCont.getString( 1 ).trim();
		  	//rsCont.close();
		}
		catch ( Exception e )
		{
		    EIGlobal.mensajePorTrace( "cuentasSerfinSantander - traeBanco(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.ERROR );
		    resBanco = cveBanco;
		}
        finally
        {
           try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
			   e.printStackTrace();
			}
	    }
		return resBanco;
	}

	/**
	 * GAE
	 * Metodo prestado para traer la descripcion de la plaza
	 *
	 * @param cvePlaza		Clave de la plaza
	 * @return resPlaza		Descripcion de la plaza
	 */
	private String traePlaza( String cvePlaza )
	{
		String totales = "";
		String resPlaza = cvePlaza;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;

		try
		{
		   conn = createiASConn(Global.DATASOURCE_ORACLE );
		   totales = "Select descripcion from comu_pla_banxico where plaza_banxico='" +
		   cvePlaza + "'";
		   psCont = conn.prepareStatement( totales );
		   rsCont = psCont.executeQuery();
		   if (rsCont.next()){
			   //rsCont.next();
			   resPlaza = rsCont.getString( 1 ).trim();
		   }
		   //rsCont.close();
		}
		catch( Exception e )
		{
		   EIGlobal.mensajePorTrace("cuentasSerfinSantander- traePlaza(): Ha ocurrido una excepcion: " + e.getMessage(), EIGlobal.NivelLog.INFO);
		   return resPlaza = cvePlaza;
		}
		finally
        {
            try
			{
			   rsCont.close ();
               psCont.close ();
               conn.close ();
            }
			catch(Exception e)
            {
				e.printStackTrace();
			}
	    }
    	EIGlobal.mensajePorTrace( "cuentasSerfinSanander - plaza " +resPlaza, EIGlobal.NivelLog.INFO);
		return resPlaza;
	}

	/**
	 * GAE
	 * Metodo encargado de validar que el lote agregado contenga cuentas a dar
	 * de Baja o a Cancelar
	 *
	 * @param folioBajaLote			Identificador del lote de carga
	 * @return 						true  - si existen cuentas a dar de baja o
	 * 									    cancelar en el lote
	 * 								false - si no existen cuentas a dar de baja
	 * 									    o cancelar en el lote
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	private boolean validaLote(String folioBajaLote) {
		String query = null;
		int counter = 0;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(*) as counter "
				  + "from eweb_baja_lote "
				  + "where id_lote='" + folioBajaLote + "'";
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::validaLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				counter = rs.getInt("counter");
			}
			System.out.println("<<VALOR DE COUNTER>>" + counter);
			if(counter>0){
				return true;
			}


		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return false;
	}

	/**
	 * GAE
	 * Metodo encargado de validar que el lote agregado contenga cuentas a dar
	 * de Baja o a Cancelar
	 *
	 * @param folioBajaLote			Identificador del lote de carga
	 * @return 						true  - si existen cuentas a dar de baja o
	 * 									    cancelar en el lote
	 * 								false - si no existen cuentas a dar de baja
	 * 									    o cancelar en el lote
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	private boolean validaLote2(String folioBajaLote) {
		String query = null;
		int counter = 0;
		ResultSet rs = null;
		Connection conn= null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select count(*) as counter "
				  + "from eweb_aut_operctas "
				  + "where id_lote='" + folioBajaLote + "' "
				  + "and estatus_oper in ('A','P','I')";
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::validaLote2:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				counter = rs.getInt("counter");
			}
			System.out.println("<<VALOR DE COUNTER>>" + counter);
			if(counter>0){
				return true;
			}


		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return false;
	}

	/**
	 * GAE
	 * Metodo encargado de validar que el lote agregado exista
	 *
	 * @param folioBajaLote			Identificador del lote de carga
	 * @return 						true  - si existe lote
	 * 								false - si no existen lote
	 */
	private boolean existeLote(String folioBajaLote) {
		String query = null;
		int counter = 0;
		ResultSet rs = null;
		Connection conn = null;
		Statement sDup = null;

		try {
			//Connection conn= createiASConn (Global.DATASOURCE_ORACLE2 );
			conn = BaseServlet.createiASConn_static2(Global.DATASOURCE_ORACLE2);
			sDup = conn.createStatement();
			query = "Select count(1) as counter "
				  + "from eweb_cat_nom_aut_ext "
				  + "where id_lote = " + folioBajaLote ;
			EIGlobal.mensajePorTrace ("CatNomInterbBaja::existeLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				counter = rs.getInt("counter");
			}
			EIGlobal.mensajePorTrace ("CatNomInterbBaja: :existeLote:: -> COUNTER:" + counter, EIGlobal.NivelLog.INFO);
			if(counter>0){
				return true;
			}
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("CatNomInterbBaja: :existeLote: Exception: -> " + e, EIGlobal.NivelLog.INFO);
			e.printStackTrace();
		} finally {
			EI_Query.cierraConexion(rs, sDup, conn);
		}
		return false;
	}

	/**
	 * GAE
	 * Metodo encargado de bitacorizar el lote que se dara de baja
	 * foliosBaja			Vector con folios de cargas
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 */
	private void insertBajaLote(Vector foliosBaja) {
		String query = null;
		String in = null;
		int[] i = null;
		Connection conn= null;
		Statement sDup = null;

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			for(int j=0; j<foliosBaja.size(); j++){
				in = (String)foliosBaja.get(j);
				query = "Insert into eweb_baja_lote(id_lote) "
					  + "values('" + in.trim() + "')";
				EIGlobal.mensajePorTrace ("CatNomInterbBaja::insertBajaLote:: -> Query:" + query, EIGlobal.NivelLog.INFO);
				sDup.addBatch(query);
			}
			i = sDup.executeBatch();

			EIGlobal.mensajePorTrace ("CatNomInterbBaja::insertBajaLote:: -> "
									 + "Se bitacorizan lote(s) exitosamente", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			EI_Query.cierraStatement(sDup);
			EI_Query.cierraConnection(conn);
		}
	}

	/**
	 * GAE
	 *
	 * @param foliosBaja			Vector con los folios activos
	 * @param folioBajaLote			Folio a registrar
	 * @return						true si existe folio
	 * 								false si no existe folio
	 */
	private boolean folioRepetido(Vector foliosBaja, String folioBajaLote) {
		for(int h=0; h<foliosBaja.size(); h++){
			if(folioBajaLote.trim().equals((String)foliosBaja.get(h))){
				return true;
			}
		}
		return false;
	}


}