package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.DetalleCuentasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.servicios.DetalleCuentasServicio;
import mx.altec.enlace.utilerias.DetalleCuentasException;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.dao.ValidaOTP;

/**
 * Servlet implementation class RegistroNominaServlet
 */
public class DetalleCuentasServlet extends BaseServlet{
	private static final long serialVersionUID = 1L;
	private static final String LIST_CTAS_BEANS = "listaDetalles";
	private static final String PARAM_MENSAJE = "msg";
	private static final String CODIGO_ERROR_ALTA_ARCHIVO = "NOAP9999";
	private static final String CODIGO_EXITO_ALTA_ARCHIVO = "NOAP0000";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetalleCuentasServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		proccesRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		proccesRequest(request, response);
	}

	private void proccesRequest(HttpServletRequest request,
			HttpServletResponse response)throws ServletException, IOException {
		
		HttpSession sessionHttp = request.getSession();
		
		BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
		boolean sesionvalida=SesionValida( request, response );
		request.removeAttribute(PARAM_MENSAJE);
		if(sesionvalida)
		{
	

				String valida = request.getParameter("valida");

				EIGlobal.mensajePorTrace("VALIDA>>>" + valida,  EIGlobal.NivelLog.INFO);

				try {
					request.setAttribute ("MenuPrincipal", session.getStrMenu ());
					request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
				}
				catch(Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}				

				request.setAttribute ("Encabezado", CreaEncabezado ( "Alta de Empleados Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > Empleados > " +
						"Cat&aacute;logo de N&oacute;mina > Alta por Archivo", "s37120h", request ) );

				List listaDetalles=null;
				String str = "---> ";
				try {
					String paramNameCtas = "ctas";			
					String ctas = request.getParameter(paramNameCtas);
					EIGlobal.mensajePorTrace(str+"Cuentas antes de validacion: "+ctas, EIGlobal.NivelLog.INFO);
					/*Peticion para procesamiento de cuentas a registrar*/
					if ( ctas != null){
						if(verificaFacultad ( "INOMENVIEMP" ,request)){

							EIGlobal.mensajePorTrace("Recepcion de peticion para registrar cuentas", EIGlobal.NivelLog.INFO);

							/*
							 * valida es null cuando apenas se va a pedir el token
							 * */	
							if (valida == null){
								EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
								boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA_PREPAGO);
								//CSA-Vulnerabilidad  Token Bloqueado-Inicio
		                        int estatusToken = obtenerEstatusToken(session.getToken());
		                        
								if ((session.getValidarToken() && 
										session.getFacultad(session.FAC_VAL_OTP) && 
										estatusToken == 1 && 
										solVal)){ //Si queremos que valide el token
									EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
									ValidaOTP.guardaParametrosEnSession(request);
									request.getSession().removeAttribute("mensajeSession");
									ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
								}else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && 
										estatusToken == 2 && solVal){                        
		                        		cierraSesionTokenBloqueado(session,request,response);		                
								}else{//No queremos que valide el token
	                        		valida = "1";
	                        	}
							//CSA-Vulnerabilidad  Token Bloqueado-Fin
							}

							/*
							 * Valida = "1" cuando ya se valid� el token
							 * */	
							if (valida != null && valida.equals("1")){

								if( !ctas.equalsIgnoreCase("") ){
									EIGlobal.mensajePorTrace(str+"Cuentas recibidas: " + ctas,  EIGlobal.NivelLog.INFO);
									if( ctas.endsWith("@") ){
										ctas = ctas.substring(0, ctas.length()-1);
									}
									String [] cuentas = ctas.split("@");
									listaDetalles = (List)request.getSession().getAttribute(LIST_CTAS_BEANS);

									if( listaDetalles == null ){///Sesion expirada enviar error.
										enviarMensaje(request,response,"Ha expirado la sesi&oacute;n");
										return;
									}

									listaDetalles = actualizarCuentasAGuardar(cuentas,listaDetalles);


									String contrato = session.getContractNumber();
									DetalleCuentasServicio servicio = new DetalleCuentasServicio();
									List<DetalleCuentasBean> listaDetallesCuentas = getCuentasSeleccionadas(listaDetalles);
									List<String> cuentasErrores = new ArrayList<String>();
									List<String> contratosErrores = new ArrayList<String>();
									long folioContrato = 1;


									try{
										folioContrato = servicio.insertarContrato(contrato);
										bitacorizaRegistroContrato(request,session,contrato,CODIGO_EXITO_ALTA_ARCHIVO);
									}catch (DetalleCuentasException e) {
										bitacorizaRegistroContrato(request,session,contrato,CODIGO_ERROR_ALTA_ARCHIVO);
										contratosErrores.add(contrato);
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

									}catch (Exception e) {
										bitacorizaRegistroContrato(request,session,contrato,CODIGO_ERROR_ALTA_ARCHIVO);
										contratosErrores.add(contrato);
										EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

									}




									for(DetalleCuentasBean detalleCuentasBean : listaDetallesCuentas){
										try{
											servicio.insertarCuenta(detalleCuentasBean, contrato, folioContrato);
											bitacorizaRegistroCuenta(request,session,detalleCuentasBean,CODIGO_EXITO_ALTA_ARCHIVO);
										}catch (DetalleCuentasException e) {
											bitacorizaRegistroCuenta(request,session,detalleCuentasBean,CODIGO_ERROR_ALTA_ARCHIVO);
											cuentasErrores.add(detalleCuentasBean.getCuenta());
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											continue;
										}catch (Exception e) {
											bitacorizaRegistroCuenta(request,session,detalleCuentasBean,CODIGO_ERROR_ALTA_ARCHIVO);
											cuentasErrores.add(detalleCuentasBean.getCuenta());
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											continue;
										}
									}

									if((cuentasErrores != null && cuentasErrores.size() > 0) || (contratosErrores!=null && contratosErrores.size()>0)
									){
										try{
											String mensajeError = getMensajeErrorRegistroCuentas(cuentasErrores, contratosErrores);
											request.getSession().setAttribute("prevalidadorMensaje",mensajeError);
											request.getRequestDispatcher("/enlaceMig/PrevalidadorServlet?opcion=4").forward(request, response);
										} catch (Exception e) {
											try {
												EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
												response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
											} catch (Exception ex) {
												EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);			
											}
										}
										return;							
									}

									request.getSession().removeAttribute(LIST_CTAS_BEANS);

									/*Nos redirigimos a la pantalla del prevalidador con mensaje de �xito*/
									request.getRequestDispatcher("/enlaceMig/PrevalidadorServlet?opcion=3").forward(request, response);

								}else{//No se recibieron cuentas enviar error
									String m = "No se recibieron cuentas para registrar";
									EIGlobal.mensajePorTrace(m, EIGlobal.NivelLog.INFO);

									enviarMensaje(request,response,m);
									return;
								}
							}
						}else{
							request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(request, response);
						}
						/*Peticion para elegir cuentas - Redirigiendo a pantalla*/
					}else{
						EIGlobal.mensajePorTrace("Redirigiendo a la pantalla de detalleCuentas", EIGlobal.NivelLog.INFO);
						listaDetalles = (List)request.getAttribute(LIST_CTAS_BEANS);								
						request.getSession().setAttribute(LIST_CTAS_BEANS, listaDetalles);
						enviarAPantalla(request,response);
					}
				} catch (Exception e) {				
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					enviarMensaje(request,response,"Se produjo un error al procesar la petici&oacute;n");
				}
			
		}
	}
	
	/**
	 * Obtiene la lista de DetalleCuentasBean que fueron
	 * seleccionados.
	 * @param listaDetalles
	 * @return
	 */
	private List<DetalleCuentasBean> getCuentasSeleccionadas(List<?> listaDetalles){
		List<DetalleCuentasBean> listaDetallesCuentas = new ArrayList<DetalleCuentasBean>();
		for(int i=0; i<listaDetalles.size(); i++){
			DetalleCuentasBean detalleCuenta = (DetalleCuentasBean) listaDetalles.get(i);
			if(detalleCuenta.isGuardar())
				listaDetallesCuentas.add(detalleCuenta);
		}
		return listaDetallesCuentas;
	}
	
	/**
	 * BITACORIZACI�N REGISTRO EN BITACORA DE OPERACIONES
	 * @param request
	 * @param session
	 * @param beanDetalleCuentas
	 * @param codError
	 */
	private void bitacorizaRegistroCuenta(HttpServletRequest request, BaseResource session,
			DetalleCuentasBean beanDetalleCuentas, String codError){
		EIGlobal.mensajePorTrace("***ENTRA BITACORIZACION TCT BIT�CORA DE OPERACIONES",EIGlobal.NivelLog.INFO);
		try{
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
		  	BitaTCTBean bean = new BitaTCTBean ();
		  	bean.setReferencia(referencia);
		  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS);
		  	bean.setConcepto(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS_MSG);
		   	bean.setNumCuenta(beanDetalleCuentas.getCuenta());
		   	bean = bh.llenarBeanTCT(bean);
		   	bean.setCodError(codError);
		    BitaHandler.getInstance().insertBitaTCT(bean);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace("***SALE BITACORIZACION TCT BIT�CORA DE OPERACIONES",EIGlobal.NivelLog.INFO);
	}
	
	
	/**
	 * BITACORIZACI�N REGISTRO EN BITACORA DE OPERACIONES DEL CONTRATO
	 * @param request
	 * @param session
	 * @param beanDetalleCuentas
	 * @param codError
	 */
	private void bitacorizaRegistroContrato(HttpServletRequest request, BaseResource session,
			String contrato, String codError){
		EIGlobal.mensajePorTrace("***ENTRA BITACORIZACION TCT BIT�CORA DE OPERACIONES",EIGlobal.NivelLog.INFO);
		try{
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
		  	BitaTCTBean bean = new BitaTCTBean ();
		  	bean.setReferencia(referencia);
		  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS);
		  	bean.setConcepto(BitaConstants.ES_NOMINA_REGISTRO_EMPLEADOS_MSG);
		   	bean.setNumCuenta(contrato);
		   	bean = bh.llenarBeanTCT(bean);
		   	bean.setCodError(codError);
		    BitaHandler.getInstance().insertBitaTCT(bean);
		}catch(SQLException e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
		EIGlobal.mensajePorTrace("***SALE BITACORIZACION TCT BIT�CORA DE OPERACIONES",EIGlobal.NivelLog.INFO);
	}
	
	
	/**
	 * Devuelve el mensaje a mostrar en pantalla para las cuentas
	 * que no se pudieron registrar en la base de datos.
	 * @param cuentasError
	 * @return
	 */
	private String getMensajeErrorRegistroCuentas(List<String> cuentasError, List<String> contratosError){
		StringBuffer mensaje = new StringBuffer();
		int totalErrores = cuentasError.size();
		int limiteListado = (totalErrores > 10)?10:totalErrores;
		int cantidadErroresNoMostrados = totalErrores - limiteListado;
		
		if (contratosError!=null && contratosError.size()>0){
			mensaje.append("Se produjeron errores al registrar el siguiente contrato : " + contratosError.get(0) + "<br/>");	
		}
		
		if (cuentasError!=null && cuentasError.size()>0){
			mensaje.append("Se produjeron errores al registrar las siguientes cuentas en la base de datos:<br/>");		
			for(int i=0; i<limiteListado; i++){
				String cuenta = cuentasError.get(i);
				mensaje.append(cuenta);
				mensaje.append("<br/>");
			}
			if(cantidadErroresNoMostrados > 0)
				mensaje.append("Existen otros "+cantidadErroresNoMostrados+" errores.");
		}
		return mensaje.toString();
	}
	
	
	private void enviarAPantalla(HttpServletRequest request,
			HttpServletResponse response){
		try{			
			evalTemplate("/jsp/detalleCuentas.jsp", request, response );
		} catch (Exception e) {
			try {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (Exception ex) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
			}
		}
	}	
	
	private void enviarMensaje(HttpServletRequest request,
			HttpServletResponse response,String mensaje){
		try{
			request.setAttribute(PARAM_MENSAJE, mensaje);
			evalTemplate("/jsp/detalleCuentas.jsp", request, response );
		} catch (Exception e) {
			try {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (Exception ex) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);			
			}
		}
	}	

	private List actualizarCuentasAGuardar(String [] cuentas,List beans){
		
		Iterator it = beans.iterator();
		ArrayList<Integer> indices = new ArrayList<Integer>();
		int indice = 0;
		for( Object bean : beans ){			
			if ( buscarEn(cuentas,((DetalleCuentasBean)bean).getCuenta()) ){
				indices.add(indice);
			}
			indice++;
		}
		
		for(Integer indiceGuardar:indices){
			((DetalleCuentasBean)beans.get(indiceGuardar.intValue())).setGuardar(true);
		}
		
		return beans;
	}

	private boolean buscarEn(String[] cuentas,String cuenta){
		for(String cuentaEnArray:cuentas){
			if( cuenta.trim().equalsIgnoreCase(cuentaEnArray.trim()) ){
				return true;
			}
		}
		return false;
	}
	
}
