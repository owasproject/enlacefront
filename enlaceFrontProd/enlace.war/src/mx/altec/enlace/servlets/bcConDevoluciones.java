/**Banco Santander Mexicano
   Create Date: 04-2002
	 Modify Date: 23-04-2002
   @author Daniel Cazares Beltran. (ASTECI)
   @version 1.01
*/
package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.util.Map;

/**

** bcConDevoluciones is a blank servlet to which you add your own code.

*/

public class bcConDevoluciones extends BaseServlet{
    //Class fields.

    private String strMsgError = "";

    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
        defaultAction(req, res);

    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException{
        defaultAction(req, res);
    }

    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException{
        String strJspPath = null;
		//<vswf:meg cambio de NASApp por Enlace 08122008>
        final String StrServletPath = "/Enlace/enlaceMig/bcConSaldos";


        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "BEGIN bcConDevoluciones v1.0.1", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);



        /*Parameter stores events to let
         *know which page must be displayed
        */
        String strEvent = req.getParameter("event");

	boolean bInsanserSession = SesionValida(req,res);



        if(bInsanserSession){
            if(strEvent.equals("initDetail")){
                //Detail Page
                try{
                    strJspPath = getDetail(req,res);
                    evalTemplate(strJspPath,req,res);
                }catch(Exception e){
                    //Error Page
                    despliegaPaginaError(e.getMessage(),
                                        "Consulta de Devoluciones de Base" +
                                           " Cero",
                                        "Tesorer�a &gt; Esquema Base Cero &gt; " +
                                            "Consulta de Saldos &gt; "+
                                            "Consulta de Devoluciones",
                                        "s33040h",req,res);


                }//end catch
            }//end if(strEvent.equals("initDetail"))
        }//end if(bInsanserSession)
    }//end defaultAction


    /**
     *Obtains Base Cero Devolutions Detail.
     *@return String representing the path to a JSP.
     */
    private String getDetail(HttpServletRequest req, HttpServletResponse res)
        throws Exception{

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


        final char chrSlash = '/';

        //Detail's List
        List lstDetail = null;

        //Error messages.
        final String msgCopyError = "Error en la copia de datos.";
        final String msgNull = "No existen datos.";
        final String msgRecovError = "Error al recuperar los datos.";

        //This String stores buffer sent Tuxedo.
        String strData = "";

        //Stores response sent from Tuxedo
        String strRespTux = "";

        //Next JSP
        final String strJspPath = "/jsp/bcConDevoluciones.jsp";

        //Objects to process files
        String strFile = "";
        ArchivoRemoto RemoteFile;

        //Detail balances Summatory.
        float DevolSum[] = new float[1];
        Float FlDevolSum = null;
        String StrDevolSum = null;

        //Space Index
        int intSpaceIdx = 0;

        final String StrSpace = " ";
        /***End of declarations***/

        strData = buildData(req);

        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "TRAMA ENTRADA: " + "\"" + strData + "\"", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);



        strRespTux = sendData(strData);

        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "TRAMA SALIDA: " + "\"" + strRespTux + "\"", EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace( "**************************", EIGlobal.NivelLog.INFO);



        if(strRespTux == null){
            throw new Exception(this.strMsgError);
        }else{
            //Copies file from Tux Server to App Server
            strFile = strRespTux.substring(strRespTux.lastIndexOf(chrSlash)+1,
                                           strRespTux.length());

            RemoteFile = new ArchivoRemoto();

            if(!RemoteFile.copia(strFile)){
                throw new Exception(msgCopyError);
            }

            strFile = Global.DIRECTORIO_LOCAL + chrSlash + strFile;




            //loads details records on a List.
            lstDetail = readDetails(strFile.trim(),DevolSum);
            FlDevolSum = new Float(DevolSum[0]);
            StrDevolSum = FormatoMoneda(FlDevolSum.toString());

            //Quita el espacio entre la cifra y el signo de moneda($)
            StrDevolSum.trim();
            intSpaceIdx =  StrDevolSum.indexOf(StrSpace);

            //Si hay espacio
            if(intSpaceIdx != -1){
               StrDevolSum = StrDevolSum.substring(0,intSpaceIdx) +
                             StrDevolSum.substring((intSpaceIdx + 1),
                                StrDevolSum.length());
            }

            if(lstDetail == null){
                throw new Exception(this.strMsgError);
            }else{
                req.setAttribute("newMenu",session.getFuncionesDeMenu());

                //Creates header for response page
                req.setAttribute("Encabezado",
                            CreaEncabezado("Consulta de Devoluciones de Base" +
                                           " Cero",
                                      "Tesorer�a &gt; Esquema Base Cero &gt; " +
                             "Consulta de Saldos &gt; Consulta de Devoluciones",
                             "s33040h",req));
                req.setAttribute("MenuPrincipal",session.getStrMenu());

					   //TODO: Bitacora de bcConSaldos
                /*
 	            * VSWF ARR -I
 	            */
 	           if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
 	           try{

 	               String strLastDate = req.getParameter("LastDate");
 	              String strAccount = req.getParameter("Account");
 				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
 				BitaTransacBean bt = new BitaTransacBean();
 				bt = (BitaTransacBean)bh.llenarBean(bt);
 				bt.setNumBit("ERBC03");
 				bt.setContrato(session.getContractNumber());

 				if(strRespTux!=null){
 				 if(strRespTux.substring(0,2).equals("OK")){
 					   bt.setIdErr("B0CD" + "0000");
 				 }else if(strRespTux.length()>8){
 					  bt.setIdErr(strRespTux);
 				 }else{
 					  bt.setIdErr(strRespTux);
 				 }
 				}
 				if(strAccount!=null && !strAccount.equals(""))
 					bt.setCctaOrig(strAccount);
 				bt.setServTransTux("B0CS");
 				if(strLastDate!=null && !strLastDate.equals(""))
 					bt.setFechaProgramada(Utilerias.MdyToDate(strLastDate));
 				BitaHandler.getInstance().insertBitaTransac(bt);
 				}catch(SQLException e){
 					e.printStackTrace();
 				}catch(Exception e){
 					e.printStackTrace();
 				}
 	           }
 	   		/*
 	   		 * VSWF ARR -F
 	   		 */
                req.setAttribute("DetailList",lstDetail);
                req.setAttribute("devolution",StrDevolSum);
            }//end else if(lstDetail == null)
        }//end else if(strRespTux == null)

        return strJspPath;
    }//END getDetail()


    /**Builds all data sent Tux Server.
     *he order of data is acording to Devolutions transaction.
     *@return String with data.
     */
    public String buildData(HttpServletRequest req){
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");


        String strData = "";

        //Fields separators
        final char chrPipe = '|';
        final char chrAt = '@';
        final char chrSpace = ' ';
        final char chrSlash = '/';

        //Some default values to transact.

        //how data is delivered
        final String strDeliverWay = IEnlace.medioEntrega2;
        final String strService = "B0CD"; //transaction's name.

        //Gets User, Profile and Contract from session object.
        String strUser = session.getUserID8(); 
        String strProfile = session.getUserProfile();
        String strContract = session.getContractNumber();




        //Gets account and range for the Balance query.
        String strAccount = req.getParameter("Account");
        String strFirstDate = req.getParameter("FirstDate");
        String strLastDate = req.getParameter("LastDate");
        strFirstDate = delSlashToDate(strFirstDate);
        strLastDate = delSlashToDate(strLastDate);


        strData = strDeliverWay + chrPipe +  strUser + chrPipe + strService +
		  chrPipe + strContract + chrPipe +  strUser + chrPipe +
		  strProfile + chrPipe + strAccount + chrAt + strFirstDate +
		  chrAt + strLastDate + chrPipe + chrSpace + chrPipe +
                  chrSpace + chrPipe;

        return strData;
    }//end buildData()


    /**Removes slash to strDate.
     *@param strDate Date in dd/mm/yyyy style.
     *@return String representing a date in ddmmyyyy style.
     */
    private String delSlashToDate(String strDate){
        int firstSlash = strDate.indexOf("/");
        int lastSlash = strDate.lastIndexOf("/");
        String strReturn = strDate.substring(0,firstSlash) +
                           strDate.substring(firstSlash+1,lastSlash) +
                           strDate.substring(lastSlash+1,strDate.length());

        return strReturn;
    }


    /**Gives data to Tux Server
     *@param strData is the String sent to server.
     *@return String with response.
     */
    private String sendData(String strData){
        //Error messages.
        final String msgSendError = "Error en el env�o de datos.";
        final String msgOpenError = "Error al iniciar comunicaci�n.";
        final String msgMapError = "Error en la recuperaci�n de datos.";

        ServicioTux servTx = new ServicioTux();


        String strRespTux = "";
        Map mp = null;

        /*servTx.setContext(
              ((com.netscape.server.servlet.platformhttp.PlatformServletContext)
              getServletContext()).getContext());*/


        try{
            //Sends buffer and receives response.
            mp = servTx.web_red(strData);
            strRespTux = (String) (mp.get("BUFFER"));



        }catch(java.rmi.RemoteException re){
            strRespTux = null;
            this.strMsgError = msgSendError;
        }catch(Exception e){
            strRespTux = null;
            this.strMsgError = msgSendError;
        }finally{
            return strRespTux;
        }
    }//end sendData()


    /**Reads a file and copies records on a List.
     *@param strFile is the path and name of the file.
     *@param parmDevSum stores devolutions balance summary
     *@return List is the list with all the devolutions detail.

     *The response file has in its first line:

     +++++++++++++++++++++++++++++++++++++++++++++
     Position       Field
     +++++++++++++++++++++++++++++++++++++++++++++
     [1-8]       Mensaje de Respuesta
     [9-16]      Numero de Referencia
     [17]        Separador
     [18-25]     Nombre del servicio

     For instance: "OK      86880   @BASE0000"

     For the next lines, the structure is as follows:

     "Numero de Contrato|Cuenta de Cheques|Banco|Cuenta de Abono|
     Fecha Devolucion|Hora Devolucion|Referencia|Importe de Devoluci�n|"

     */
    private List readDetails(String strFile, float parmDevSum[]){
        //Error messages.
        final String msgReadError = "Error al leer datos.";
        final String msgDataError = "Error en el servicio solicitado.";
        final String msgNotKnown = "Error inesperado.";
        final String msgNoRecords = "No existen datos por consultar.";
        final String msgOpenError = "Error en acceso a datos.";
        final String msgNoDataSep = "Error al procesar los datos.";
        final String msgDataStruct = "La estructura de los datos es inv�lida";

        //Objects to process file.
        EI_Exportar LocalFile;
        String strLine = "";

        //Details List.
        List lstReturn = null;

        //list Index.
        int idx = 0;
        int intFirstIdx = 0;
        int intLastIdx = 0;

        String StrDevolSum = "";

        //Data separator.
        final String StrDataSep = "|";
        final String strArroba = "@";

        //Breaks a loop.
        boolean boolLoopError = false;
        LocalFile = new EI_Exportar(strFile);

        //Devolutions balance summatory
        float fDevolSum = 0.0F;

        //Process file on App Server
        try{
            if(LocalFile.abreArchivoLectura()){
                strLine = LocalFile.leeLinea();

                if(strLine.substring(0,8).trim().equals("OK") &&
                   strLine.substring(16,24).trim().equals("BASE0000")){

                    lstReturn = new ArrayList();
                    idx = 0;
                    boolLoopError = false;

                    while(!LocalFile.eof() && boolLoopError == false){
                        strLine = LocalFile.leeLinea();
                        EIGlobal.mensajePorTrace("Linea no formateada: " + strLine, EIGlobal.NivelLog.INFO);


                        strLine = formatData(strLine,parmDevSum);
                        fDevolSum+= parmDevSum[0];



                        EIGlobal.mensajePorTrace("Linea formateada: " + strLine, EIGlobal.NivelLog.INFO);

                        if(strLine != null){

                            //Contract and Account are removed from String
                            for(int i = 0; i < 2; i++){
                                intLastIdx = strLine.indexOf(StrDataSep);

                                if(intLastIdx != -1){
                                    strLine = strLine.substring(intLastIdx + 1,
                                                             strLine.length());
                                }else{
                                    boolLoopError = true;
                                }
                            }

                            //Removes the last data separator on String
                            strLine = strLine.substring(0,strLine.length() - 1);

                            //Index is set in Bank first character.
                            intFirstIdx = 0;
                            intLastIdx = strLine.indexOf(StrDataSep);

                            if(intLastIdx == -1){
                                //data separator was not found.
                                lstReturn = null;
                                this.strMsgError = msgNoDataSep;
                                boolLoopError = true;
                            }else{
                                while(intLastIdx != -1){
                                    //Copies each field on List.
                                    lstReturn.add(idx,
                                     strLine.substring(intFirstIdx,intLastIdx));

                                    intFirstIdx = intLastIdx + 1;

                                    strLine =
                                            strLine.substring(intFirstIdx,
                                                              strLine.length());

                                    intFirstIdx = 0;
                                    intLastIdx = strLine.indexOf(StrDataSep);
                                    idx++;
                                }

                                lstReturn.add(idx,
                                              strLine.substring(intFirstIdx,
                                              strLine.length()));
                                idx++;
                            }//end if
                        }//end if
                    }//end while

                    //Copies summatory reference
                    parmDevSum[0] = fDevolSum;

                    EIGlobal.mensajePorTrace("Total devoluciones=" + parmDevSum[0], EIGlobal.NivelLog.INFO);

                    if(idx == 0){
                        //If no record was copied to Detail's List.
                        lstReturn = null;
                        this.strMsgError = msgNoRecords;
                    }
                }else{
                    if(strLine.substring(0,8).trim().equals("ERROR") &&
                       strLine.substring(16,24).trim().equals("BASE0001")){

                        this.strMsgError = strLine.substring(24,
                                                             strLine.length());
                    }else{
                        if(strLine.substring(0,8).trim().equals("OK") &&
                           strLine.substring(16,24).trim().equals("BASE0001")){

                            this.strMsgError = strLine.substring(24,
                                                              strLine.length());
                        }else{
                            if(strLine.substring(0,5).equals("ERROR")){
                                this.strMsgError = strLine.substring(16,strLine.length());
                            }else{
                                this.strMsgError = strLine;
                            }
                        }
                    }
                    //lstReturn = null;
                }
                LocalFile.cierraArchivo();
            } else{
                this.strMsgError = msgOpenError;
                lstReturn = null;
            }
        }catch(Exception e){
            lstReturn = null;
            this.strMsgError = msgReadError;
        }finally{
            return lstReturn;
        }
    }//end readDetails()


    /**
     *Formats all data.
     *@param LstNotFormated List not formated.
     *@param parmSum is a float object that stores the details balance summatory.
     * The order of data is as follows:
     *
     *"Numero de Contrato|Cuenta de Cheques|Banco|Cuenta de Abono|
     *Fecha Devolucion|Hora Devolucion|Referencia|Importe de Devoluci�n|"
     *
     * i.e.
     *
     *@return String Returns formatted String. The method uses currency format
     *
     */
    private String formatData(String StrNotFormatted, float parmSum[]){
	String StrReturn = "";
        int intNumFields = 8;
        int intInitPos = 0;
        int intEndPos = 0;
        String StrDataSep = "|";
        String StrAuxField = null;
        int intSpaceIdx = 0;
        final String StrSpace = " ";
        //Devolutions balance SUMMATORY
        Float FlDevSum = null;

        for(int i = 0; i < intNumFields; i++){

            intEndPos = StrNotFormatted.indexOf(StrDataSep,intInitPos);

            if(intEndPos != -1){

                StrAuxField = StrNotFormatted.substring(intInitPos,intEndPos);

                switch(i){
                    case 7:

                        try{
                            FlDevSum = Float.valueOf(StrAuxField);
                        }catch(NumberFormatException e){
                            FlDevSum = new Float(0.0F);
                        }

                        EIGlobal.mensajePorTrace("DEVOLUCION: "+
                                                  FlDevSum.toString(), EIGlobal.NivelLog.INFO);

                        StrAuxField = FormatoMoneda(StrAuxField);

                        EIGlobal.mensajePorTrace("cifra antes de quitar espacio "+
                                                  StrAuxField, EIGlobal.NivelLog.INFO);



                        //Quita el espacio entre la cifra y el signo de moneda($)
                        StrAuxField.trim();
                        intSpaceIdx =  StrAuxField.indexOf(StrSpace);

                        //Si hay espacio
                        if(intSpaceIdx != -1){
                            StrAuxField = StrAuxField.substring(0,intSpaceIdx) +
                                        StrAuxField.substring((intSpaceIdx + 1),
                                                        StrAuxField.length());
                        }

                        EIGlobal.mensajePorTrace("cifra DESPUES de quitar espacio "+
                                                  StrAuxField, EIGlobal.NivelLog.INFO);



                        break;
                    default:
                        break;
                }//end switch(i)

                StrReturn = StrReturn + StrAuxField + StrDataSep;

                intInitPos = intEndPos + 1;

            }else{
                 StrReturn = StrNotFormatted;
                 break;
            }
        }

        //Stores the detail balances summatory.
        parmSum[0] = FlDevSum.floatValue();

        return StrReturn;

    }//end formatData

}//class