package mx.altec.enlace.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import static mx.altec.enlace.utilerias.NomPreUtil.logInfo;

/**
 * @author Berenice Mesquitillo
 * Fecha: 29/junio/2009
 * proyecto: 200813700 Nomina Prepago
 * Descripcion: Servlet para Consulta de relacion tarjeta empleado.
 */
 public class NomPreBusqueda extends mx.altec.enlace.servlets.BaseServlet implements javax.servlet.Servlet {

	private final static String EMP_BUSQUEDA_INICIA="inicia";
	private final static String EMP_BUSQUEDA_CONS="consulta";
	private final static String EMP_BUSQUEDA_EJECUTA="ejecuta";
	private final static String EMP_BUSQUEDA_EXPORTA="exporta";
	private static final String SER_ARCH_CONSULTA_TAREMP = "archcontaremp";
	public NomPreBusqueda() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defaultAction(request, response);
	}
	protected void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String accion = (String)request.getParameter("opcion");
		accion = accion == null ||  accion.length() == 0? EMP_BUSQUEDA_INICIA : accion;
		String salida=(String)request.getParameter("salida");
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if(sesionvalida)
		{

			String strMenu =  (session.getstrMenu());
			strMenu =  (strMenu != null ? strMenu : "");
			request.setAttribute("MenuPrincipal", strMenu);
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			//CGH Valida servicio de nomina
			if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				request.setAttribute("Encabezado", CreaEncabezado( "Consulta de relaci&oacute;n tarjeta empleado","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Consulta","s25320h",request));//YHG NPRE
				return;
			}

			if(accion.trim().equals(EMP_BUSQUEDA_INICIA)){
				request.setAttribute("Encabezado", CreaEncabezado( "Consulta de relaci&oacute;n tarjeta empleado","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Consulta","s25320h",request));//YHG NPRE
				inicia(request, response,session);
			}else if(accion.trim().equals(EMP_BUSQUEDA_CONS)){
				request.setAttribute("Encabezado", CreaEncabezado( "Consulta de relaci&oacute;n tarjeta empleado","Servicios &gt; Tarjeta de Pagos Santander  &gt; Asignaci&oacute;n tarjeta-empleado &gt; Consulta","s25320h",request));//YHG NPRE
				consulta(request, response);
			}

		}else{
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	/**
	 * @author Berenice Mesquitillo
	 *  Metodo inicial, carja el jsp inicial ya sea para la busqueda de empleados
	 * @param request
	 * @param response
	 * Fecha: 29-06-2009
	 */
	private void inicia(final HttpServletRequest request, HttpServletResponse response,BaseResource session){
		try {
			StringBuffer sbTabla=new StringBuffer();
			StringBuffer varFechaHoy = new StringBuffer();
		    String strInhabiles = "";
		    NomPreBusquedaAction busquedaAction=new NomPreBusquedaAction();
		    EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this);
			String fechHoy=EnlaceGlobal.fechaHoy("dd/mm/aaaa");
			sbTabla=busquedaAction.armaTablaFiltros(fechHoy);
			/********************************** Arreglos de fechas para 'Hoy' .... */
		    varFechaHoy.append("  //Fecha De Hoy");
		    varFechaHoy.append("\n  dia=new Array("+busquedaAction.generaFechaAnt("DIA")+","+busquedaAction.generaFechaAnt("DIA")+");");
		    varFechaHoy.append("\n  mes=new Array("+busquedaAction.generaFechaAnt("MES")+","+busquedaAction.generaFechaAnt("MES")+");");
		    varFechaHoy.append("\n  anio=new Array("+busquedaAction.generaFechaAnt("ANIO")+","+busquedaAction.generaFechaAnt("ANIO")+");");
		    varFechaHoy.append("\n\n  //Fechas Seleccionadas");
		    varFechaHoy.append("\n  dia1=new Array("+busquedaAction.generaFechaAnt("DIA")+","+busquedaAction.generaFechaAnt("DIA")+");");
		    varFechaHoy.append("\n  mes1=new Array("+busquedaAction.generaFechaAnt("MES")+","+busquedaAction.generaFechaAnt("MES")+");");
		    varFechaHoy.append("\n  anio1=new Array("+busquedaAction.generaFechaAnt("ANIO")+","+busquedaAction.generaFechaAnt("ANIO")+");");
		    varFechaHoy.append("\n  Fecha=new Array('"+busquedaAction.generaFechaAnt("FECHA")+"','"+busquedaAction.generaFechaAnt("FECHA")+"');");

		    request.setAttribute("anio", ObtenAnio());
		    request.setAttribute("mes", ObtenMes());
		    request.setAttribute("dia", ObtenDia());

		    request.setAttribute("diasInhabiles",diasInhabilesAnt());
		    request.setAttribute("fechaHoy", ObtenFecha(true));
			request.setAttribute("Tabla",sbTabla.toString());

			BitaTransacBean bt = new BitaTransacBean();
			bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_ENTRA);
			NomPreUtil.bitacoriza(request,null,null,true, bt);

			evalTemplate(IEnlace.NOMPRE_BUSCA_EMP, request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		EIGlobal.mensajePorTrace(this.getClass().getName() + " - " + "inicia" + " -Termina",EIGlobal.NivelLog.INFO);

	}

	/**
	 * @author Berenice Mesquitillo
	 * Metodo donde se consultan los datos (relacion tarjeta empleado).
	 * @param request
	 * @param response
	 * Fecha: 30-06-2009
	 * @throws IOException
	 * @throws ServletException
	 */
	private void consulta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//tipoFiltro
		logInfo("NomPreBusqueda.class>>Entra a consulta");
		String tipoFiltro=(String)request.getParameter("tipoFiltro");
		String salida = (String)request.getParameter("salida");
		NomPreBusquedaAction busquedaAction=new NomPreBusquedaAction();

		String noTarjeta=null;
		Date fecha=null;

			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			NomPreLM1D lm1dTarjeta=null;
			String include = null;
			String archivo = null;
			NomPreEmpleado empleado=new NomPreEmpleado();
			SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy");

			logInfo("NomPreBusqueda.class>>Global.NOMPRE_ACTIVA_DEMOGRAFICO>>"+Global.NP_ACTIVA_DEMOGRAFICO);

			if(Global.NP_ACTIVA_DEMOGRAFICO.trim().toUpperCase().equals("SI")){
				request.setAttribute("modificar", "activo");
			}

			if (request.getParameter("inicial") != null  ) {

				if(!tipoFiltro.trim().equals("TO")){
					if(tipoFiltro.trim().equals("NT")){
						noTarjeta=request.getParameter("valNoTarjeta");
					}else if(tipoFiltro.trim().equals("NE")){
						empleado.setNoEmpleado(request.getParameter("valNoEmpleado"));
					}else if(tipoFiltro.trim().equals("NO")){
						empleado.setNombre(request.getParameter("valNombre"));
						empleado.setPaterno(request.getParameter("valPaterno"));
						empleado.setMaterno(request.getParameter("valMaterno"));
					}else if(tipoFiltro.trim().equals("FE")){
						try{
							fecha=df.parse(request.getParameter("fecha1"));
						}catch(ParseException e){}
					}

				}

				lm1dTarjeta= busquedaAction.obtenRelacionTarjetaEmpleado(session.getContractNumber(), noTarjeta, fecha,empleado);
				EIGlobal.mensajePorTrace("NomPreBusqueda: codExito " + (lm1dTarjeta == null ? "null" : lm1dTarjeta.isCodExito()), NivelLog.INFO);

				archivo = NomPreUtil.saveObjectToFile(lm1dTarjeta, sess);

				sess.setAttribute(SER_ARCH_CONSULTA_TAREMP, archivo);

				final String nomEmp=empleado.getNombre()+" "+empleado.getPaterno()+" "+empleado.getMaterno();

				BitaTransacBean bt = new BitaTransacBean();
				bt.setNumBit(BitaConstants.ES_NP_TARJETA_CONSULTA_CONSULTA);
				if(noTarjeta != null){
					bt.setCctaOrig(noTarjeta);
				}
				if(nomEmp!= null){
					bt.setBeneficiario(nomEmp);
				}
				if(archivo != null){
					bt.setNombreArchivo(archivo);
				}
				String codErr=null;
				if(lm1dTarjeta!=null && lm1dTarjeta.getCodigoOperacion()!=null){
					codErr=lm1dTarjeta.getCodigoOperacion();
				}
				NomPreUtil.bitacoriza(request,"LM1D",codErr, bt);
				if(lm1dTarjeta!=null && lm1dTarjeta.isCodExito()){
					try
					{
						int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
						String codErrBit=" ";
						if(lm1dTarjeta!=null && lm1dTarjeta.getCodigoOperacion()!=null && !lm1dTarjeta.getCodigoOperacion().trim().equals("")){
							codErrBit=lm1dTarjeta.getCodigoOperacion();
						}
						String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID8()," ",0.0,NomPreUtil.ES_NP_TARJETA_CONSULTA_OPER," ",0," ");
					}catch(Exception e){
						EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
					}
				}
			} else {

				archivo = sess.getAttribute(SER_ARCH_CONSULTA_TAREMP).toString();

				lm1dTarjeta = NomPreUtil.objectFromFile(archivo);
			}

			request.setAttribute("lm1d", lm1dTarjeta);

			if ("excel".equals(salida)) {

				configurarSalidaReporte(response, "tarjeta_empleado");
				include = IEnlace.NOMPRE_BUSCA_EMP_EXP_DET;

			} else {

				include = IEnlace.NOMPRE_BUSCA_EMP_DET;
			}


			request.getRequestDispatcher(include).include(request,response);
	}


	private void configurarSalidaReporte(HttpServletResponse response, String nombreArchivo) throws IOException {

		response.setContentType("application/vnd.ms-excel");

		response.setHeader("Content-disposition", "attachment; filename="
				+ nombreArchivo + ".xls");
	}

}