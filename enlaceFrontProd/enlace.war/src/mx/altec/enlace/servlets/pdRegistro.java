/*
 * pdRegistro.java
 *
 * Created on 25 de marzo de 2002, 09:28 AM
 */

/* EVERIS
 * 28 Mayo 2007
 * Art. 52 Solucion de raiz
 * Jose Antonio Rodriguez Alba
 * Modificacion en la forma en que se realizan las transacciones por archivo.
 * Se realiza el llamado de servicios tuxedo: generador de folio,
 * controlador de registros y dispersor de transacciones.
 * Estos requieren archivos de tramas y trama para transaccion de cada archivo.
 *
 **/

package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.pdArchivoTemporal;
import mx.altec.enlace.bo.pdBeneficiario;
import mx.altec.enlace.bo.pdCalendario;
import mx.altec.enlace.bo.pdPago;
import mx.altec.enlace.bo.pdServicio;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.dao.pdImportarArchivo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.util.*;
import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;


/**
 *
 * @author  Horacio Oswaldo Ferro Daz
 * @version 1.0
 */
public class pdRegistro extends BaseServlet {

	//Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - Inicio
	/**
	 * Establece el numero de registros que seran almacenados en cada archivo
	 * que sea generado por la funcion "makeFiles".
	 */
	private final int numRegs= 250;
	//Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - Fin

    short sucOpera = (short) 787;

    // EVERIS 28 Mayo 2007 Variables
	//int numRegistrosOk = 0;
	//String folioArchivo = null;
	//String mensajeErrorFolio = null;
	//String mensajeErrorEnviar = null;
	//String mensajeErrorDispersor = null;
	// EVERIS 28 Mayo 2007 Fin Variables nuevas

    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

    }

    /** Destroys the servlet. */
    public void destroy() {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
    * @param request servlet request
    * @param response servlet response
    */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	//System.out.println("pdRegistro.class>>Entra processRequest");

        BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
	HttpSession sess = request.getSession(false);

        String valida = getFormParameter(request, "valida" );

	if(validaPeticion( request,  response ,session,sess,valida)){
            EIGlobal.mensajePorTrace("SI---------------***ENTRA A VALIDAR LA PETICION *********", EIGlobal.NivelLog.ERROR);
	}else{

            // Permisos de usuarios
            HashMap Permisos = new HashMap ();
            // Fecha de hoy
            GregorianCalendar hoy = new GregorianCalendar ();
            // Validamos la sesion
            boolean Sesion = SesionValida(request, response);

            String opc = getFormParameter(request,"txtOpcion");
            if (opc == null || opc.equals("")) opc = "0";
            int Opcion = Integer.parseInt(opc);
            String archivoAyuda = "";
            if( Opcion == 6)
                archivoAyuda = "s31013h";
            else
                archivoAyuda = (Opcion == 2 || Opcion == 3 ) ? "s31011h" : "s31010h";

            if (!Sesion) {
				return;
			}
            pdArchivoTemporal Archivo;

            if (session != null) {
                // Validamos que el usuario pueda entrar al modulo de pago directo
                if (!(session.getFacultad (session.FAC_PADIRACATSU_PD) &&
                    session.getFacultad (session.FAC_PADIRENVARC_PD))) {
                    // Cambiar por algo apropiado
                    String laDireccion = "document.location='SinFacultades'";
                    request.setAttribute( "plantillaElegida", laDireccion);
                    request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request, response);
                } else {
                    // Si tiene facultad cargamos sus permisos
                    Permisos.put ("Envio", new Boolean (session.getFacultad (session.FAC_PADIRENVARC_PD))); // Facultad de envio y creacin
                    Permisos.put ("Alta", new Boolean (session.getFacultad (session.FAC_PADIRALTMAN_PD))); // Facultad de alta manual
                    Permisos.put ("Importar", new Boolean (session.getFacultad (session.FAC_PADIRIMPARC_PD))); // Facultad de importar archivo
                    Permisos.put ("Manipulacion", new Boolean (session.getFacultad (session.FAC_PADIRMTTOAR_PD))); // Facultad de manipulacin de registros
                    Permisos.put ("Imprimir", new Boolean (session.getFacultad (session.FAC_PADIRIMPARC_PD))); // Facultad para imprimir el archivo
                    Permisos.put ("Modificar", new Boolean (session.getFacultad (session.FAC_PADIRMODSER_PD))); // Facultad para modificar un archivo ya enviado
                    Permisos.put ("Recuperar", new Boolean (session.getFacultad (session.FAC_PADIRACTARC_PD))); // Facultad para recuperar el archivo enviado
                                    Permisos.put ("Benefnoreg", new Boolean (session.getFacultad ("PADIRBENORE"))); //Facultad para beneficiarios no registrados
                }
                request.setAttribute("newMenu", session.getFuncionesDeMenu());
                request.setAttribute("MenuPrincipal", session.getStrMenu());
                request.setAttribute("Encabezado", CreaEncabezado("Registro de Pago Directo",
                                "Servicios &gt; Pago Directo &gt; Registro de Pago Directo &gt; Por Archivo",
                                archivoAyuda, request));
                request.getSession ().setAttribute ("Permisos", Permisos);
            }
            try {
            	Archivo = (pdArchivoTemporal) request.getSession().getAttribute("Archivo");
			} catch(ClassCastException e) {
				Archivo = null;
			}

            EIGlobal.mensajePorTrace("pdRegistro Archivo=" + Archivo , EIGlobal.NivelLog.ERROR);
            if ((null != Archivo) && (Archivo.getRegistros () > 0) &&
                session.getFacultad (session.FAC_PADIREXPARC_PD)) {

                String URLArchivo = Archivo.exportar(session.getUserID8 ());
                request.getSession().setAttribute("URLArchivo", URLArchivo);
            }
            session.setModuloConsultar(IEnlace.MAlta_ordenes_pago);
            switch (Opcion) {
                case -1:
                    // En caso de que la sesion no sea valida
                    break;
                case 0:{
                    // Opcion para ingresar a la pagina principal
                    pdServicio Servicio = new pdServicio();
                    //Servicio.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                    Servicio.setContrato (session.getContractNumber ());
                    Servicio.setUsuario (session.getUserID8 ());
                    request.getSession ().setAttribute ("mapBen", Servicio.getBeneficiariosMapa ());
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 1:{
                    // Opcion para crear el archivo de envio
                    Archivo = new pdArchivoTemporal();
                    boolean Respuesta = Archivo.crea(getFormParameter(request,"txtArchivo"));
                    String Message = "";
                    if (Respuesta) {
                        Message = "El archivo fue creado";
                        request.getSession ().removeAttribute ("Archivo");
                        request.getSession ().setAttribute ("Archivo", Archivo);
                        Archivo.setClavePerfil(session.getUserProfile());
                    } else {
                        Message = "El archivo ya existe, seleccione otro nombre";
                    }
                    request.getSession().setAttribute("Message", Message);
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 2:{
                    // Opcion para ir a la pagina de agregar un registro
                    request.getSession ().removeAttribute ("Pago");
                    pdServicio serv = new pdServicio ();
                    //serv.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
                    serv.setContrato (session.getContractNumber ());
                    serv.setUsuario (session.getUserID8 ());
                    GregorianCalendar fechaLib = pdCalendario.getSigDiaHabil (hoy, getDiasInhabiles ());
                    ArrayList Calendario = pdCalendario.getCalendario (fechaLib, getDiasInhabiles ());
                    GregorianCalendar fechaLimite = (GregorianCalendar) fechaLib.clone ();
                    fechaLimite.set (GregorianCalendar.MONTH, fechaLimite.get (GregorianCalendar.MONTH) + 3);
                    fechaLimite = pdCalendario.getAntDiaHabil (fechaLimite, getDiasInhabiles ());
                    ArrayList beneficiarios = serv.getBeneficiarios ();
                    HashMap mapaBen = serv.getBeneficiarios (beneficiarios);
                    request.getSession ().setAttribute ("Beneficiarios", beneficiarios);
                    request.getSession ().setAttribute ("Cuentas", serv.getCuentas ());
                    request.getSession ().setAttribute ("mapaBen", mapaBen);
                    request.getSession ().setAttribute ("fechaLib", fechaLib);
                    request.getSession ().setAttribute ("Calendario", Calendario);
                    request.getSession ().setAttribute ("fechaLimite", fechaLimite);
                    evalTemplate("/jsp/pdRegistroDetalle.jsp", request, response);
                    break;
                }
                case 3:{
                    // Opcion para agregar un registro al Archivo
                    Archivo = (pdArchivoTemporal) request.getSession ().getAttribute ("Archivo");
                                    HashMap Beneficiarios;
                    HashMap mapaBen = (HashMap) request.getSession ().getAttribute ("mapaBen");
                                    Beneficiarios = mapaBen;
                                    String claveno = getFormParameter(request,"clv_no_reg");
                                    EIGlobal.mensajePorTrace("La clave a comparar \""+claveno+"\"", EIGlobal.NivelLog.DEBUG);
                                    if(!claveno.equals("")){
                                            if(Beneficiarios.containsKey(claveno)){
                                                    EIGlobal.mensajePorTrace("La clave existe", EIGlobal.NivelLog.DEBUG);
                                                    request.setAttribute("Mensaje",(String)"La clave del Beneficiario ya existe");
                                                    evalTemplate("/jsp/pdRegistroDetalle.jsp",request,response);
                                                    break;
                                            }
                                    }
                    pdPago pag = new pdPago ();
                    pdBeneficiario ben ;
                    String claveBen = getFormParameter(request,"NomBen");
                                    /* Modificacion para beneficiarios no Registrados
                                      fecha de modificacion 20122002
                                    */
                                    EIGlobal.mensajePorTrace("El valor de beneficiario en registro \""+claveBen+"\"", EIGlobal.NivelLog.DEBUG);
                                    if(!claveBen.equals("SIN_SELECCION") ) {
                                            EIGlobal.mensajePorTrace("entrando a obtener nombre del bneficiario", EIGlobal.NivelLog.DEBUG);
                                            pag.setClaveBen (claveBen);
                                            EIGlobal.mensajePorTrace("Se coloco l clave del beneficiario", EIGlobal.NivelLog.DEBUG);
                                            ben = (pdBeneficiario) mapaBen.get (claveBen);
                                            EIGlobal.mensajePorTrace("Se obtuvo el nombre del beneficiario", EIGlobal.NivelLog.DEBUG);
                                            //EIGlobal.mensajePorTrace("Nombre \""+ben.getNombre()+"\"", EIGlobal.NivelLog.DEBUG);
                                            pag.setNomBen (ben.getNombre ());
                                    }else{
                                            EIGlobal.mensajePorTrace("El beneficiario no esta registrado", EIGlobal.NivelLog.DEBUG);
                                            pag.setClavenoReg(claveno);
                                            pag.setNomBen(getFormParameter(request,"Ben_no_reg"));
                                    }
                                    /*	pag.setClaveBen (claveBen);
                                            ben = (pdBeneficiario) mapaBen.get (claveBen);
                                            pag.setNomBen (ben.getNombre ());
                                            Fin de la modificacion para beneficairios no regitrados

                                            Fin de la modificacion  20122002
                                    */
                    pag.setImporte (getFormParameter(request,"Importe"));
                    pag.setNoPago (getFormParameter(request,"NoPago"));
                    pag.setCuentaCargo (getFormParameter(request,"CuentaCargo"));
                    pag.setFechaLib (getFormParameter(request,"FechaLibramiento"));
                    pag.setFechaPago (getFormParameter(request,"FechaLimite"));
                    claveBen = getFormParameter(request,"ClaveSucursal");
                    if (claveBen.equalsIgnoreCase ("Todas"))
                        pag.setClaveSucursal ("");
                    else
                        pag.setClaveSucursal (claveBen);
                    if (getFormParameter(request,"FormaPago").equals ("E"))
                        pag.setFormaPago ('E');
		    else if (getFormParameter(request,"FormaPago").equals ("T"))
			pag.setFormaPago ('T');
                    else
                        pag.setFormaPago ('C');
                    pag.setConcepto (getFormParameter(request,"Concepto"));
                    if (Archivo.agrega (pag))
                        evalTemplate ("/jsp/pdRegistroDetalle.jsp", request, response);
                    else {
                        request.getSession ().setAttribute ("Pago", pag);
                                            evalTemplate ("/jsp/pdRegistroDetalle.jsp?Numero=Incorrecto", request, response);

                    }
                    break;
                }
                case 4:{
                    //Opcion para borrar el archivo
                    Archivo = (pdArchivoTemporal) request.getSession ().getAttribute ("Archivo");
                    Archivo.borrarArchivo ();
                    request.getSession ().removeAttribute ("Archivo");
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 5:{
                    //Opcion para borrar un registro del archivo
                    Archivo = (pdArchivoTemporal) request.getSession().getAttribute("Archivo");
                    Archivo.borrar(getFormParameter(request,"txtNumPag"));
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 6:{
                    // Opcion para ir a la pagina de modificacion de pago
                    pdPago Temp;
                    Archivo = (pdArchivoTemporal) request.getSession().getAttribute("Archivo");
                    Temp = Archivo.getPago(getFormParameter(request,"txtNumPag"));
                    pdServicio serv = new pdServicio ();
                    //serv.getServicioTux ().setContext (((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
                    serv.setContrato (session.getContractNumber ());
                    serv.setUsuario (session.getUserID8 ());
                    GregorianCalendar fechaLib = Temp.getFechaLib ();
                    ArrayList Calendario = pdCalendario.getCalendario (fechaLib, getDiasInhabiles ());
                    GregorianCalendar fechaLimite = (GregorianCalendar) fechaLib.clone ();
                    fechaLimite.set (GregorianCalendar.MONTH, fechaLimite.get (GregorianCalendar.MONTH) + 3);
                    fechaLimite = pdCalendario.getAntDiaHabil (fechaLimite, getDiasInhabiles ());
                    ArrayList beneficiarios = serv.getBeneficiarios ();
                    HashMap mapaBen = serv.getBeneficiarios (beneficiarios);
                    request.getSession ().setAttribute ("Beneficiarios", beneficiarios);
                    request.getSession ().setAttribute ("Cuentas", serv.getCuentas ());
                    request.getSession ().setAttribute ("mapaBen", mapaBen);
                    request.getSession ().setAttribute ("fechaLib", fechaLib);
                    request.getSession ().setAttribute ("Calendario", Calendario);
                    request.getSession ().setAttribute ("fechaLimite", fechaLimite);
                    request.getSession().setAttribute("Pago", Temp);
                    evalTemplate("/jsp/pdRegistroDetalle.jsp", request, response);
                    break;
                }
                case 7:{
                    // Opcion para modificar el registro del pago
                    Archivo = (pdArchivoTemporal) request.getSession().getAttribute("Archivo");
                                    HashMap Beneficiarios;
                    HashMap mapaBen = (HashMap) request.getSession ().getAttribute ("mapaBen");
                                    Beneficiarios = mapaBen;
                                    String claveno = getFormParameter(request,"clv_no_reg");
                                    if(!claveno.equals("")){
                                            if(Beneficiarios.containsKey(claveno)){
                                                    EIGlobal.mensajePorTrace("La clave existe en el catalogo", EIGlobal.NivelLog.DEBUG);
                                                    request.setAttribute("Mensaje",(String)"La clave del Beneficiario ya existe");
                                                    evalTemplate("/jsp/pdRegistroDetalle.jsp",request,response);
                                                    break;
                                            }
                                    }
                    pdPago pag = new pdPago ();
                    pdBeneficiario ben;
                    String claveBen = getFormParameter(request,"NomBen");
                                    /*
                                            Modificacion para Beneficiarios no registrados
                                    20122002
                                    */
                                    EIGlobal.mensajePorTrace("El valor de beneficiario \""+claveBen+"\"", EIGlobal.NivelLog.DEBUG);
                                    if(!claveBen.equals("SIN_SELECCION") ) {
                                            EIGlobal.mensajePorTrace("obteniendo el nombre del bneficiario", EIGlobal.NivelLog.DEBUG);
                                            pag.setClaveBen (claveBen);
                                            ben = (pdBeneficiario) mapaBen.get (claveBen);
                                            pag.setNomBen (ben.getNombre ());
                                    }else{
                                            EIGlobal.mensajePorTrace("El beneficiario no ha sido regsitrado", EIGlobal.NivelLog.DEBUG);
                                            pag.setClavenoReg(claveno);
                                            pag.setNomBen(getFormParameter(request,"Ben_no_reg"));
                                    }

                    /*pag.setClaveBen (claveBen);
                    ben = (pdBeneficiario) mapaBen.get (claveBen);
                    pag.setNomBen (ben.getNombre ());
                                    Fin de la modificacion para beneficairios no regitrados
                                    */

                    pag.setImporte (getFormParameter(request,"Importe"));
                    pag.setNoPago (getFormParameter(request,"NoPago"));
                    pag.setCuentaCargo (getFormParameter(request,"CuentaCargo"));
                    pag.setFechaLib (getFormParameter(request,"FechaLibramiento"));
                    pag.setFechaPago (getFormParameter(request,"FechaLimite"));
                    claveBen = getFormParameter(request,"ClaveSucursal");
                    if (claveBen.equalsIgnoreCase ("Todas"))
                        pag.setClaveSucursal ("");
                    else
                        pag.setClaveSucursal (claveBen);
                    if (getFormParameter(request,"FormaPago").equals ("E"))
                        pag.setFormaPago ('E');
		    else if (getFormParameter(request,"FormaPago").equals ("T"))
			pag.setFormaPago ('T');
                    else
                        pag.setFormaPago ('C');
                    pag.setConcepto (getFormParameter(request,"Concepto"));
                    Archivo.modifica(pag);
                    request.getSession().removeAttribute("Pago");
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 8:{
                    // Opcin para modificar un archivo
                    pdServicio Servicio = new pdServicio();
                    Archivo = (pdArchivoTemporal) request.getSession().getAttribute("Archivo");
                    //Servicio.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                    Servicio.setContrato (session.getContractNumber ());
                    Servicio.setUsuario (session.getUserID8 ());
                    String Message;
                    if (Archivo.escribe ()) {
                        try {
                            Message = Archivo.getRespuesta (Servicio.envia_tux(Archivo.getArchivo(), Archivo.getRegistros (), 'M'));
                        } catch (Exception ex) {
                            Message = ex.getMessage ();
                        }
                    } else {
                        Message = "Error al escribir el archivo";
                    }
                    Archivo.setbEnviado (true);
                    request.getSession().setAttribute("Message", Message);
                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 9:{
                	// Opcion para importar el archivo
                	String sArchivo = session.getUserID8 ();
                	pdServicio serv = new pdServicio ();
                	serv.setContrato (session.getContractNumber ());
                	serv.setUsuario (session.getUserID8 ());

                	/*
                	 * Nueva importacion de archivos.
                	 * Modifico: Alejandro Rada Vazquez.
                	 * Getronics.
                	 * Incidencia IM394887
                	 * SC 20314
                	 * Se cambio la importacion de archivos y los metodos para
                	 * validar el archivo pues no se permitia la importacion de
                	 * mas de 2000 registros.
                	 * Se agrego funcionalidad para importar archivos en formato
                	 * ZIP.
                	 * */

                	String nombreArchivoImp="";
                	String disparaJSP="/jsp/pdRegistro.jsp";

                	pdImportarArchivo ArchivoImp = new pdImportarArchivo (sArchivo, getDiasInhabiles (), serv.getBeneficiariosMapa ());
                	nombreArchivoImp=getFormParameter(request,"fileNameUpload");

                	//System.out.println(" ***********  pdRegistro.java: Comenzando la nueva importacion");
                	if(nombreArchivoImp.indexOf("ZIPERROR")>=0) {
                		//System.out.println(" ***********  pdRegistro.java: Se grabara el error en la lista.");
                		ArchivoImp.zipError();
                		//System.out.println(" ***********  pdRegistro.java: Actualizando datos en sesion");
                		request.getSession ().setAttribute ("numLineas", new java.lang.Long (ArchivoImp.getNumLineas ()));
                		request.getSession ().setAttribute ("numCorrectas", new java.lang.Long (ArchivoImp.getLineasCorrectas ()));
                		request.getSession ().setAttribute ("listaErrores", ArchivoImp.getListaErrores ());
                		request.getSession ().setAttribute ("tituloPagina", "Errores en el Archivo");
                		disparaJSP="/jsp/pdRegistro.jsp";
                		//System.out.println(" ***********  pdRegistro.java: Continuando con la operacion");
                	}
                	else {
                		//System.out.println(" ***********  pdRegistro.java: Abriendo archivo de cuentas");
                		llamado_servicioCtasInteg (IEnlace.MAlta_ordenes_pago, " "," ", " ", session.getUserID8 () + ".ctas", request);
                		ArchivoImp.generaCuentas (session.getUserID8 () + ".ctas");
                		ArchivoImp.setContrato(session.getContractNumber());
                		ArchivoImp.setUsuario(session.getUserID8());

                		if (ArchivoImp.nuevaImportacion(nombreArchivoImp)) {
                			String URLArchivo = ArchivoImp.getNombreExportar();
                			if (! ArchivoImp.envia(new File(URLArchivo)))
                				URLArchivo = null;
                			request.getSession().setAttribute("URLArchivo", URLArchivo);
                			request.getSession().setAttribute ("numLineas", new java.lang.Long (ArchivoImp.getNumLineas ()));
                			request.getSession().setAttribute ("importeTotal", new java.lang.Double (ArchivoImp.getImporteTotal()) );
                			request.getSession().setAttribute ("Archivo", Archivo);
                			request.getSession().setAttribute ("NombreDeArchivo", nombreArchivoImp);

                			GregorianCalendar fechaTransmision = ArchivoImp.getFechaTransmision();
                			GregorianCalendar fechaActualizacion = ArchivoImp.getFechaActualizacion();
                			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                			request.getSession().setAttribute("archivoEnviado", "NO");
                			request.getSession().setAttribute("numTransmision", new Long(0));
                			request.getSession().setAttribute("registrosAceptados", new Long(ArchivoImp.getLineasCorrectas()));
                			request.getSession().setAttribute("registrosRechazados", new Long(ArchivoImp.getNumLineas() - ArchivoImp.getLineasCorrectas()));
                			request.getSession().setAttribute("fechaTransmisionTime", fechaTransmision == null ? "" : formato.format(fechaTransmision.getTime()));
                			request.getSession().setAttribute("fechaActualizacionTime", fechaActualizacion == null ? "" : formato.format(fechaActualizacion.getTime()));
                			request.getSession().setAttribute("Message", "Se importo el archivo satisfactoriamente.");

                			//System.out.println(" ***********  pdRegistro.java: Actualizando informacion de Archivo.");
                			Archivo.setbEnviado (true);
                			disparaJSP="/jsp/pdRegistroImporta.jsp";
                		}
                		else {
                			//System.out.println(" ***********  pdRegistro.java: El archivo contenia errores no se importo.");
                			request.getSession ().setAttribute ("numLineas", new java.lang.Long (ArchivoImp.getNumLineas()));
                			request.getSession ().setAttribute ("numCorrectas", new java.lang.Long (ArchivoImp.getLineasCorrectas ()));
                			request.getSession ().setAttribute ("listaErrores", ArchivoImp.getListaErrores ());
                			request.getSession ().setAttribute ("tituloPagina", "Errores en el Archivo");
                			disparaJSP="/jsp/pdRegistro.jsp";
                		}
                	}
                	//System.out.println(" ************* pdRegistro.java: Se ejecuta:"+ disparaJSP);
                	request.getSession ().setAttribute ("Importado", "..");
                	evalTemplate (disparaJSP, request, response);
                	break;
                }
                case 10:{
                	// Opcion para enviar el archivo
                	/*
                	 * Nueva importacion de archivos.
                	 * Modifico: Alejandro Rada Vazquez.
                	 * Getronics.
                	 * Incidencia IM394887
                	 * SC 20314
                	 * Se cambio la importacion de archivos y los metodos para validar el archivo
                	 * pues no se permitia la importacion de mas de 2000 registros.
                	 * Se agrego funcionalidad para importar archivos en formato ZIP.
                	 * Modificacion para el evnio de archivos, pues la importacion se cambio por completo.
                	 * */

					///////////////////--------------challenge--------------------------
					String validaChallenge = request.getAttribute("challngeExito") != null
						? request.getAttribute("challngeExito").toString() : "";

					EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
						EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
						//ValidaOTP.guardaParametrosEnSession(request);
						guardaParametrosEnSession(request);
						validacionesRSA(request, response);
						return;
					}

					//Fin validacion rsa para challenge
					///////////////////-------------------------------------------------

                	boolean valBitacora = true;

                	if(valida == null) {
                		boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber (),IEnlace.PAGO_DIR);

                		if(
                			session.getValidarToken() &&
                			session.getFacultad(session.FAC_VAL_OTP) &&
                			session.getToken().getStatus() == 1 &&
                			solVal
                		){
                			request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
                			request.getSession().removeAttribute("bitacora");
                			guardaParametrosEnSession(request);
                			request.getSession().removeAttribute("mensajeSession");
                			ValidaOTP.mensajeOTP(request, "PagoDirArchivo");
                			ValidaOTP.validaOTP(request,response, IEnlace.VALIDA_OTP_CONFIRM);
                		}
                		else {
                			ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
                			valida="1";
                			valBitacora = false;
                		}
                	}

                	if(valida != null && valida.equals("1")){
                		if (valBitacora) {
                			try{
                				int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_PAGO_DIRECTO;
									String concepto = BitaConstants.CTK_CONCEPTO_PAGO_DIRECTO;
									String cuenta = "0";
									String token = "0";

									if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
									{token = request.getParameter("token");}

									double importeDouble = 0;
									String importe_Str = "0";
									if (sess.getAttribute("importeTotal") != null && !sess.getAttribute("importeTotal").equals(""))
									{importe_Str = sess.getAttribute("importeTotal").toString();
									importeDouble = Double.valueOf(importe_Str).doubleValue();}

									EIGlobal.mensajePorTrace( "numeroReferencia "+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "importe "+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "claveOperacion "+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "concepto "+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "token "+ token, EIGlobal.NivelLog.INFO);

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
                			}
                			catch(Exception e) {
                				EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);
                			}
                		}

                		String nombreDeArchivoImportado="";
                		String num=null;
                		long nLineas = 0;

                		nombreDeArchivoImportado=(String)request.getSession().getAttribute("NombreDeArchivo");

                		if(request.getSession().getAttribute("numLineas")!=null){
                			num = (request.getSession().getAttribute("numLineas")).toString();
                		}

                		nLineas = (num!=null)?new Long(num).longValue():0;
                		pdServicio Servicio = new pdServicio();
                		Servicio.setContrato (session.getContractNumber ());
                		Servicio.setUsuario (session.getUserID8 ());

                		String Message="";
                		String importOK="";
                		String Result = null;
                		String codError = null;
                		String tramaParaFolio = null;
                		boolean errorEnvio = true;
                		GregorianCalendar fechaTransmision = null;
                		String folioArchivo = null;

                		pdImportarArchivo ArchivoEnv = new pdImportarArchivo(nLineas,nombreDeArchivoImportado,session.getUserID8 ());
                		long errores = 0;

                		if(ArchivoEnv.generaArchivoTuxedo()) {
                			try {
                				Servicio.setPerfil(session.getUserProfile());
                				tramaParaFolio = session.getUserID8 () + "|" + session.getUserProfile() + "|" +session.getContractNumber () + "|PAGD|" + nLineas + "|";

                				try {
                					Hashtable hs = Servicio.obtiene_folio_tux(tramaParaFolio);
                					Result= (String) hs.get("BUFFER") +"";
                				}
                				catch( java.rmi.RemoteException re ) {
                					re.printStackTrace();
                					errorEnvio=true;
                				}
                				catch(Exception e) {
                					EIGlobal.mensajePorTrace(getClass().getName() + "::processRequest()::Hubo un problema al obtener folio", EIGlobal.NivelLog.INFO);
                				}

                				if(Result==null || Result.equals("null") )
                					Result="ERROR      Error en el servicio.";

                				tramaParaFolio=null;
                				codError=Result.substring(0,Result.indexOf("|") );

                				if(!codError.trim().equals("TRSV0000")) {
                					errorEnvio=true;
                				}
                				else {
                					folioArchivo = Result.substring( Result.indexOf("|")+1, Result.length() );
                					folioArchivo = folioArchivo.trim();

                					if ( folioArchivo.length() < 12) {
                						do {
                							folioArchivo = "0" + folioArchivo;
                						}while(folioArchivo.length() < 12);
                					}

                					if ( ( folioArchivo.length() != 12 ) || folioArchivo.equals("000000000000") ) {
                						errorEnvio=true;
                					}
                					else {
                						errorEnvio = false;
                						request.removeAttribute("folioArchivo");
                						session.setFolioArchivo (folioArchivo);
                						Servicio.setFolioArchivo(session.getFolioArchivo());
                						request.setAttribute("folioArchivo", folioArchivo);
                					}
                				}

                				if ( !errorEnvio){
                					Message = makeFiles(ArchivoEnv, Servicio, response, folioArchivo);
                					Message += " Folio: <b>" + folioArchivo + "</b>";
                					fechaTransmision = new GregorianCalendar();
                				}
                				else {
                					Message = "Error al generar folio.";
                				}

                				if(Message.indexOf("<b>Linea")==-1) {
                					Message += "<br>N&uacute;mero Secuencia: <b>" + Servicio.getNumSecuencia () + "</b>";

                					if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                						try{
                							String importe = String.valueOf(request.getSession().getAttribute("importeTotal"));
                							BitaHelper bh = new BitaHelperImpl(request, session,sess);
                							BitaTransacBean bt = new BitaTransacBean();
                							bt = (BitaTransacBean)bh.llenarBean(bt);
                							bt.setNumBit(BitaConstants.ES_PAGO_DIR_REG_PAGOS_ARCH_IMPORTAR_ARCHIVO);
                							bt.setContrato(session.getContractNumber());
                							bt.setNombreArchivo(nombreDeArchivoImportado);
                							bt.setImporte(Double.parseDouble(importe));
                							bt.setIdErr(codError.length() > 8 ? codError.substring(0,8):codError);
                							log("VSWF: bandera token " + request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN));

                							if (
                								request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null &&
                								((String) request.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim().equals(BitaConstants.VALIDA)) {
                									bt.setIdToken(session.getToken().getSerialNumber());
                									}
                									else{
                										log("VSWF: no hubo bandera de token");
                									}
                							request.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
                							BitaHandler.getInstance().insertBitaTransac(bt);
                						}
                						catch (SQLException e){
                							e.printStackTrace();
                						}catch(Exception e){
                							e.printStackTrace();
                						}
                					}
                				}
                				else {
                					for(int i = Message.indexOf("Linea"); i != -1; i = Message.indexOf("Linea", i + 3), errores++);
                						Message = "<b>Existen errores al enviar el archivo:<b><br />"+Message+"<br>Nmero Secuencia: <b>" + Servicio.getNumSecuencia () + "</b>";
                				}

                				importOK="SI";
                			} catch (Exception ex){
                				Message = ex.getMessage ();
                			}
                		}
                		else {
                			Message = "Error al escribir el archivo.";
                			importOK="NO";
                			errores = ArchivoEnv.getLineasCorrectas();
                		}

                		GregorianCalendar fechaActualizacion = ArchivoEnv.getFechaActualizacion();
                		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                		request.setAttribute("IMPORT_OK",importOK);
                		request.setAttribute("RespuestaTux",Message);
                		request.getSession ().removeAttribute ("URLArchivo");
                		request.getSession().setAttribute("archivoEnviado", "SI");
                		request.getSession().setAttribute("numTransmision", new Long(Servicio.getNumSecuencia()));
                		request.getSession().setAttribute("registrosAceptados", new Long(ArchivoEnv.getLineasCorrectas() - errores));
                		request.getSession().setAttribute("registrosRechazados", new Long(errores));
                		request.getSession().setAttribute("fechaTransmisionTime", fechaTransmision == null ? "" : formato.format(fechaTransmision.getTime()));
                		request.getSession().setAttribute("fechaActualizacionTime", fechaActualizacion == null ? "" : formato.format(fechaActualizacion.getTime()));
                		request.getSession().setAttribute("Message", Message);

                		evalTemplate("/jsp/pdRegistroImporta.jsp", request, response);
                	}
                	break;
                }
                case 11: {
                    // Opcion para actualizar los pagos del servidor
                    String NumReferencia = getFormParameter(request,"NumReferencia");
                    pdServicio Servicio = new pdServicio ();
                    //Servicio.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                    Servicio.setContrato (session.getContractNumber ());
                    Servicio.setUsuario (session.getUserID8 ());
                    Archivo = Servicio.actualizaPago (NumReferencia);
                                    //System.out.println("\nArchivo registros: "+ Archivo.getRegistros()+"\n");

                     request.getSession ().setAttribute ("mapaBen", Servicio.getBeneficiariosMapa ());
                     if (null != Archivo) {request.getSession ().setAttribute ("Archivo", Archivo);
                        String URLArchivo = Archivo.exportar(session.getUserID8 ());
                        request.getSession().setAttribute("URLArchivo", URLArchivo);
                     }
                    if(Archivo.getRegistros()==0)//slf IM323100
                                    {
                                            String Message = "<b>Su consulta no contiene datos<b><br />";
                                            request.getSession().setAttribute("Message", Message);
                                    }
                    evalTemplate ("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
                case 12: {
                    // Opcin para desplegar los errores del archivo
                    evalTemplate ("/jsp/pdErrores.jsp", request, response);
                    break;
                }
                case 13:{
                    // Opcion para ingresar a la pagina principal
            	// BIT CU 4241, El cliente entra al flujo
            	/*
	    		 * VSWF
	    		 */
                	 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                try{

				BitaHelper bh = new BitaHelperImpl(request, session,sess);
				if (getFormParameter(request, BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)getFormParameter(request, BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_PAGO_DIR_REG_PAGOS_ARCH_ENTRA);
				bt.setContrato(session.getContractNumber());

				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
                	 }
	    		/*
	    		 * VSWF
	    		 */
                    request.getSession ().removeAttribute ("Archivo");
                    pdServicio Servicio = new pdServicio();
                    //Servicio.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                    Servicio.setContrato (session.getContractNumber ());
                    Servicio.setUsuario (session.getUserID8 ());
                    request.getSession ().setAttribute ("mapBen", Servicio.getBeneficiariosMapa ());

                    evalTemplate("/jsp/pdRegistro.jsp", request, response);
                    break;
                }
            }
        }
    }

    /** Handles the HTTP <code>GET</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest(request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
    * @param request servlet request
    * @param response servlet response
    */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest(request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * Metodo para obtener los dias inhabiles a partir de la fecha actual
     * @return Lista con los dias inhabiles
     * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
     */
    protected ArrayList getDiasInhabiles () {
        java.sql.ResultSet rs = null;
        java.sql.Connection conn = null;
        java.sql.Statement stmt = null;
        ArrayList diasInhabiles = new ArrayList ();
        GregorianCalendar fecha;
        int Dia, Mes, Anio;
        String sql = "select to_char(FECHA, 'dd'), to_char(FECHA, 'mm'), to_char(FECHA, 'yyyy') from COMU_DIA_INHABIL where CVE_PAIS = 'MEXI' and FECHA >= sysdate";
        try {
            conn = createiASConn(Global.DATASOURCE_ORACLE);
            if (conn == null) return null;
            stmt = conn.createStatement ();
            rs = stmt.executeQuery (sql);
            while (rs.next ()) {
                Dia = Integer.parseInt (rs.getString (1));
                Mes = Integer.parseInt (rs.getString (2));
                Anio = Integer.parseInt (rs.getString (3));
		Mes++;
                fecha = new GregorianCalendar (Anio, Mes - 2, Dia);
                diasInhabiles.add (fecha);
            }

            
        } catch (java.sql.SQLException ex) {
            System.err.println (ex.getMessage ());
            ex.printStackTrace (System.err);
			diasInhabiles = null;

        }finally {
        	EI_Query.cierraConexion(rs, stmt, conn);
        }
      return diasInhabiles;
    }


	// EVERIS 28 Mayo 2007 Metodo Modificado:
	/* Genera archivo de trama y llama a servicios tuxedo: controlador de registros y
	*  dispersor de registros
	**/

	// Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - inicio
	/**
	 * Metodo que toma un archivo importado de orden de pago en lnea y lo
	 * divide en varios archivos mas pequeos, estos archivos tendran un tamao
	 * en funcion de la variable "numRegs", la cual indica la cantidad de
	 * registros que seran almacenados por cada archivo.
	 *
	 * @param ArchivoEnv Objeto tipo pdImportarArchivo con la informacion del
	 * archivo que tiene los registros que se procesaran.
	 * @param Servicio Objeto tipo pdServicio para obtener datos de la trama que
	 * sera enviada, asi como la funcion para enviar la misma.
	 * @param response Respuesta del servlet.
	 *
	 * @return message Cadena de respuesta obtenida del envio de la trama.
	 */


	protected String makeFiles(pdImportarArchivo ArchivoEnv, pdServicio Servicio, HttpServletResponse response, String folioArchivo) {

		String message=""; //Cadena que contendra la respuesta de Tuxedo.
		try {
			// EVERIS 28 Mayo 2007 Agregado: Nombre que tendra el archivo que sera enviado a Tuxedo. cliente_0000_folio
			String fileNameSalida = ArchivoEnv.getNombreArchivoTuxedo() + "_";
			String gzipTrama = null; // Trama para el archivo gzip

			//Nombre que tendra el archivo que sera enviado a Tuxedo.
			String fileName = ArchivoEnv.getNombreArchivoTuxedo() + ".ParaTuxedo";
			//Buffer de entrada que contiene los registros que seran tomados
			//para ser separados en archivos mas pequeos.
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			//Buffer de salida que ser encargado de generar los archivos que
			//seran enviados posteriormente a Tuxedo.
			BufferedWriter out = null;

			String numeroSecuencia = ""; //Numero de secuencia de registro de pago directo.
			String line = ""; //Linea leida para ser procesada.
			String outFileName = ""; //Nombre del archivo de salida.

			int lineNumber=0; //Contador numero de lineas totales.
			int numFiles =0; // Contador numero de archivos que seran procesados.
			int regPerFile=0; //Contador de registros que tendra cada archivo.
			int pct=0; //Contador de porcentaje de avance de envio.

			//Vector que almmacena nombre de archivo, cantidad de lineas del
			//mismo y nombre del archivo de respuesta dentro de un arreglo.
			Vector vectorAux = new Vector();
			Object arrayAux[] = null;

			//obtiene numero total de archivos
			int lineTot = (int) ArchivoEnv.getNumLineas();
			int archTot = lineTot / numRegs;
			if ( (lineTot % numRegs) != 0 ) { //agrega uno mas si hay residuo
				archTot++;
			}

			//Stream que sera el encargado demostrar al usuario una ventana con
			//la finalidad de indicar el progreso que ha logrado el envio de
			//archivos de pago.

			//ServletOutputStream servletOut = response.getOutputStream();
			PrintWriter servletOut = response.getWriter();
			//response.setContentType("text/html");
 			servletOut.println("<html><body>");
			servletOut.println("<script>\nvar ventana = window.open('','PROGRESO', 'height=125,width=400,menubar=no,titlebar=yes,resizable=no,scrollbars=no,toolbar=no');");
			servletOut.println("ventana.document.write('<html><head><link rel=stylesheet href=/EnlaceMig/consultas.css type=text/css></head><body><table border=0 width=365 class=textabdatcla align=center cellspacing=3 cellpadding=0><tr><th height=25 class=tittabdat colspan=2>Env&iacute;o de archivo de Pago Directo</th></tr><tr><td class=tabmovtex1 align=right width=50><img src=/gifs/EnlaceMig/gic25020.gif border=0></td><td class=tabmovtex1 align=center width=300><br><div id=estado>Enviando archivos...</div><br></td></tr></table></body></html>');");
			servletOut.println("ventana.document.close();");
			servletOut.flush();
			servletOut.println("var state = ventana.document.all.estado;");
			// -- servletOut.println("var state = ventana.document.getElementById('estado');");
			servletOut.println("</script></body></html>");
            //ciclo para procesar cada linea del archivo de entrada y colocarla
            //en el archivo que le corresponda.
			line = in.readLine();
			//Servicio.numSecuencia = -1;
			//System.out.println("MSD ---> Secuencia antes del ciclo: " + Servicio.getNumSecuencia());
			boolean primeraVez = true;
			while(line != null){
				//porcentaje de progreso de envio.
				pct = (numFiles*100)/archTot;
				//creaArchivo define si se crea un archivo nuevo o no.
				int creaArchivo = lineNumber % numRegs;
				//indica en que archivo debe colocar registro.
				numFiles = (lineNumber / numRegs) + 1;
				//si el contador es 0 crea otro archivo.
				if (creaArchivo == 0) {
					//si el contador es 0 envia el archivo.
					if (regPerFile != 0){
						// EVERIS 28 Mayo 2007 Objeto redimensionado a 4
						arrayAux = new Object[3];
						arrayAux[0]=outFileName; //archivo origen
						arrayAux[1]=new Integer(regPerFile); //lineas x archivo

						regPerFile=0;
				    	servletOut.println("<script>state.innerHTML='Enviando archivo...<br>Porcentaje enviado = " + pct + " %';</script>");
						servletOut.flush();
						//valida si existe numero de secuencia
						//System.out.println("pdRegistro MSD ---> Pasando por primer ciclo");
						//System.out.println("pdRegistro MSD ---> Secuencia en momento crtico: " + Servicio.getNumSecuencia());
						if (primeraVez) {
							numeroSecuencia = "";
							primeraVez = false;
						}
						else {
							if (numeroSecuencia.equals("")){
								numeroSecuencia = Long.toString(Servicio.getNumSecuencia());
							}
						}
						EIGlobal.mensajePorTrace("pdRegistro::procesa vector en indice - numero de secuencia = " + numeroSecuencia, EIGlobal.NivelLog.INFO);

						// EVERIS 28 Mayo 2007 Genera Archivo gzip con tramas para tuxedo
						if( ! mx.altec.enlace.utilerias.GzipArchivo.zipFile( (String)arrayAux[0], (String)arrayAux[0]+"_" + folioArchivo + ".tram" ) ) {
							EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Error: No se reallizo el gzip del archivo "+(String)arrayAux[0], EIGlobal.NivelLog.ERROR);
						}
						else {
							EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Se realizo el gzip: " + (String)arrayAux[0]+"_" + folioArchivo + ".tram.gz", EIGlobal.NivelLog.DEBUG);
						}

						ArchivoRemoto archRemoto = new ArchivoRemoto();
						gzipTrama = ((String)arrayAux[0]).substring( ((String)arrayAux[0]).indexOf("/",30)+1, ((String)arrayAux[0]).length()  ) +"_" + folioArchivo + ".tram.gz";
						boolean rcpRealizado = false;
	 					if ( ( new File( IEnlace.DOWNLOAD_PATH + gzipTrama)).exists() )
	 						{
	 						if(archRemoto.copiaLocalARemoto(gzipTrama))
	 							{
								EIGlobal.mensajePorTrace(" ***********  pdRegistro.java El archivo "+gzipTrama+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
								rcpRealizado = true;
								}
	 						else
								EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Error al copiar el archivo " +gzipTrama, EIGlobal.NivelLog.DEBUG);
							}
                        // Se comenta para FASE II NAS
						//if ( (new File( (String)arrayAux[0]+"_" + folioArchivo + ".tram.gz")).exists() ) new File( (String)arrayAux[0]+"_" + folioArchivo + ".tram.gz").delete();

						if (!rcpRealizado)
	 					   {
	 					   System.out.println(" ***********  pdRegistro.java No se realizo la copia por NAS.");
                           message =  "No se realizo la copia del archivo. ";
	 					   }
	 					else{
	 						if ( !Servicio.envia_datos_tux_aux(new File((String)arrayAux[0]), ((Integer)arrayAux[1]).intValue(), 'N', numeroSecuencia))
	 						   {
	 						   message = "";
	 						   }
	 						else {
								message =  "Error en el envio de datos. ";
								}
	 					    }

						//arrayAux[2]=arrayAux[0] + ".RespTuxedo";
						// EVERIS 28 Mayo 2007 Agregar el nombre del archivo al arreglo
						arrayAux[2] = arrayAux[0] + "_";
						vectorAux.add(arrayAux); //agrega datos al vector

					}
                    // EVERIS 28 Mayo 2007 Aumento del rango del numero de archivos posibles a crear (4 digitos)
                    //define el consecutivo del nombre del archivo
					DecimalFormat df = new DecimalFormat("0000");
					// EVERIS 28 Mayo 2007 Para generar un archivo de salida con nombre diferente fileNameSalida
					outFileName = fileNameSalida + df.format(numFiles);
					File toDelete = new File(outFileName);
					if (toDelete.exists()) {
						EIGlobal.mensajePorTrace("pdRegistro::makeFiles - Eliminando archivo " + outFileName + "creado previamente...", EIGlobal.NivelLog.INFO);
						toDelete.delete();
					}
					else {
						EIGlobal.mensajePorTrace("pdRegistro::makeFiles - Archivo creado previamente no existe, procede a crear nuevo", EIGlobal.NivelLog.INFO);
					}
					EIGlobal.mensajePorTrace("pdRegistro::makeFiles - Archivo = " + outFileName, EIGlobal.NivelLog.INFO);
				}
				//escribe el archivo de salida y lo cierra
				out = new BufferedWriter(new FileWriter(outFileName, true));
				out.write(line);
				out.newLine();
				out.flush();
				out.close();
				lineNumber++;
				regPerFile++;
				line = in.readLine();
			}

			//lineas para procesar el ultimo archivo generado (inicio)----------
			// EVERIS 28 Mayo 2007 Objeto redimensionado a 4
			Object arrayAuxUR[] = new Object[3];
			arrayAuxUR[0]=outFileName;
			arrayAuxUR[1]=new Integer(regPerFile);

			pct = (numFiles*100)/archTot;
	    	servletOut.println("<script>state.innerHTML='Enviando archivo...<br>Porcentaje enviado = " + pct + " %';</script>");
			servletOut.flush();
			//valida si existe numero de secuencia

			//System.out.println("pdRegistro MSD ---> Pasando por segundo ciclo");
			if (primeraVez) {
				numeroSecuencia = "";
				primeraVez = false;
			}
			else if (numeroSecuencia.equals("")){
				numeroSecuencia = Long.toString(Servicio.getNumSecuencia());
				EIGlobal.mensajePorTrace("pdRegistro::makeFiles - numero de secuencia = " + numeroSecuencia, EIGlobal.NivelLog.INFO);
			}

            // EVERIS 28 Mayo 2007 Genera Archivo gzip con tramas para tuxedo
			if( ! mx.altec.enlace.utilerias.GzipArchivo.zipFile( (String)arrayAuxUR[0], (String)arrayAuxUR[0]+"_" + folioArchivo + ".tram" ) )
				EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Error: No se reallizo el gzip del archivo "+(String)arrayAuxUR[0], EIGlobal.NivelLog.ERROR);
			else EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Se realizo el gzip: " + (String)arrayAuxUR[0]+"_" + folioArchivo + ".tram.gz", EIGlobal.NivelLog.DEBUG);

			ArchivoRemoto archRemoto = new ArchivoRemoto();
			gzipTrama = ((String)arrayAuxUR[0]).substring( ((String)arrayAuxUR[0]).indexOf("/",30)+1, ((String)arrayAuxUR[0]).length()  ) +"_" + folioArchivo + ".tram.gz";
			boolean rcpRealizado = false;

	 		if ( ( new File( IEnlace.DOWNLOAD_PATH + gzipTrama)).exists() )
	 			{
	 			if(archRemoto.copiaLocalARemoto(gzipTrama))
	 				{
					EIGlobal.mensajePorTrace(" ***********  pdRegistro.java El archivo "+gzipTrama+" se copio correctamente", EIGlobal.NivelLog.DEBUG);
					rcpRealizado = true;
					}
	 			else
					EIGlobal.mensajePorTrace(" ***********  pdRegistro.java Error al copiar el archivo " +gzipTrama, EIGlobal.NivelLog.ERROR);
				}
            // Se comenta para FASE II NAS
			//if ( (new File( (String)arrayAuxUR[0]+"_" + folioArchivo + ".tram.gz")).exists() ) new File( (String)arrayAuxUR[0]+"_" + folioArchivo + ".tram.gz").delete();

			if (!rcpRealizado)
	 			{
	 			EIGlobal.mensajePorTrace(" ***********  pdRegistro.java No se realizo la copia por NAS.", EIGlobal.NivelLog.ERROR);
	 			message =  "No se realizo la copia del archivo. ";
	 			}
	 		else{
				if ( !Servicio.envia_datos_tux_aux(new File((String)arrayAuxUR[0]), ((Integer)arrayAuxUR[1]).intValue() , 'N', numeroSecuencia) )
					{
						if ( !Servicio.dispersor_tux_aux() )
						{
						message =  "Archivo Procesado. ";
						}
						else{
							message =  "Error en el envio de datos. ";
						}
					}
				else {
					message =  "Error en el envio de datos. ";
					}

	 			}

			//arrayAuxUR[2]=arrayAuxUR[0] + ".RespTuxedo";
			// EVERIS 28 Mayo 2007 Agregar el nombre del archivo al arreglo
			arrayAuxUR[2]=arrayAuxUR[0] + "_";
			vectorAux.add(arrayAuxUR);

			//lineas para procesar el ultimo archivo generado (fin)-------------

            //Arreglo que tendra todas las lineas de respuesta de los archivos
			//enviados.
			String respuesta[] = new String[lineNumber];
			// Variable que tomara el error en caso de que exista
			String error = null;
			int numLineaGral = 0;

	    	servletOut.println("<script>state.innerHTML='Procesando Archivos Enviados,<br>favor de esperar un momento.';</script>");
			servletOut.flush();

			EIGlobal.mensajePorTrace("pdRegistro::makeFiles - vectorAux.size() =" + vectorAux.size(), EIGlobal.NivelLog.INFO);
			for(int a=0;a<vectorAux.size();a++){
				Object tmpArray[] = (Object[])vectorAux.elementAt(a);
       			File archBorrar = new File((String)tmpArray[0]);
				archBorrar.delete();
				EIGlobal.mensajePorTrace("pdRegistro::makeFiles - Archivo " + tmpArray[0] + " eliminado.", EIGlobal.NivelLog.INFO);
			}
			EIGlobal.mensajePorTrace("pdRegistro::makeFiles - numero de registros = " + lineNumber, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("pdRegistro::makeFiles - numero de archivos = " + numFiles, EIGlobal.NivelLog.INFO);
			servletOut.println("<script>ventana.close();</script>");
		}
		catch (IOException ioe) {
			//System.out.println("Hubo errror en la aplicacin");
			ioe.printStackTrace();
		}
		catch (Exception e){
			message = e.getMessage();
			e.printStackTrace();
		}
		return message;
	}
	// Q24146 - Praxis - Norberto Velazquez Sepulveda - 17/Ene/2005 - fin

           public void guardaParametrosEnSession(HttpServletRequest request){

          request.getSession().setAttribute("plantilla",request.getServletPath());

          HttpSession session = request.getSession(false);
          if(session != null){

            Map tmp = new HashMap();
            tmp.put("txtOpcion", getFormParameter(request, "txtOpcion"));
            session.setAttribute("parametros",tmp);
            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }
    }

}