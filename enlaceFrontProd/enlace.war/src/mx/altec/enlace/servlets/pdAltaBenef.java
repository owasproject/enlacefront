package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.sql.Connection;
import java.sql.SQLException;

public class pdAltaBenef extends BaseServlet
{
	int numero_cuentas= 0;
	int salida= 0;
	int i = 0;
	String fecha_hoy = "";
	String strFecha = "";
	String cuenta = "";
	String VarFechaHoy="";
	short suc_opera = (short)787;

	public void doGet(HttpServletRequest req, HttpServletResponse res)

	{try{
		defaultAction(req, res);
         }catch(Exception e)
         {e.printStackTrace();}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)

	{try{
		defaultAction(req, res);
         }catch(Exception e)
         {e.printStackTrace();}
	}

	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{        boolean sesionvalida = SesionValida( req, res );
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		if ( !sesionvalida ) { return; }


		String usuario = "";
		String contrato = "";
		String clave_perfil = "";
		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &Inicia clase&", EIGlobal.NivelLog.INFO);

			usuario = session.getUserID8();
			contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
			suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);

      String valida = req.getParameter ( "valida" );
      if(validaPeticion( req,  res,session,sess,valida)){
      	EIGlobal.mensajePorTrace("SI---------------***ENTRA A VALIDAR LA PETICION *********", EIGlobal.NivelLog.INFO);
      }else{

			String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &Inicia default  &"+ventana, EIGlobal.NivelLog.INFO);

			if(ventana.equals("0"))
			{
				if (session.getFacultad(session.FAC_PADIRMTOBEN_PD))
				{
//					TODO: BIT CU 4281, El cliente entra al flujo
					/*
		    		 * VSWF ARR -I
		    		 */
					 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					if (req.getParameter(BitaConstants.FLUJO)!=null){
					       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
					  }else{
					   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					  }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_ALTA_ENTRA);
					bt.setContrato(session.getContractNumber());

					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						e.printStackTrace();
					}catch(Exception e){

						e.printStackTrace();
					}
					 }
		    		/*
		    		 * VSWF ARR -F
		    		 */
					inicia(clave_perfil, contrato, usuario,  req, res );
				}
				else
				{
					req.setAttribute("Error","No tiene facultad para dar de alta beneficiarios");
					despliegaPaginaErrorURL("No tiene facultad para dar de alta beneficiarios", "Alta de Beneficiarios", "Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta", "s31070h", "pdAltaBenef?ventana=0", req, res);
				}
			}
			if(ventana.equals("1"))
			{EIGlobal.mensajePorTrace("----ENTRO VENTANA 1  *********", EIGlobal.NivelLog.INFO);
				if(valida==null){
					EIGlobal.mensajePorTrace("-------55444-------Interrumpe el flujo  *********", EIGlobal.NivelLog.INFO);

					boolean solVal=ValidaOTP.solicitaValidacion(session.getContractNumber (),IEnlace.PAGO_DIR);

					if(session.getValidarToken() &&
                                            session.getFacultad(session.FAC_VAL_OTP) &&
					    session.getToken().getStatus() == 1 &&
					    solVal)
					{
                                         EIGlobal.mensajePorTrace("-------------Interrumpe el flujo 4444 *********"+req.getParameter("ventana"), EIGlobal.NivelLog.INFO);
                                         	guardaParametrosEnSession(req);

						//VSWF-HGG -- Bandera para guardar el token en la bit�cora
						req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
						req.getSession().setAttribute("plantilla","/enlaceMig/pdAltaBenef");
						ValidaOTP.validaOTP(req,res, IEnlace.VALIDA_OTP);
					}
					else {
                                            ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
                                            valida="1";
                                        }
				}

				if(valida!=null && valida.equals("1")){
					alta(clave_perfil,contrato, usuario, req, res );
				}
			}
		}

		EIGlobal.mensajePorTrace("pdAltaBenef.class  &Termina clase&", EIGlobal.NivelLog.INFO);
	}

	public int inicia(String clave_perfil,String contrato, String usuario, HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		int EXISTE_ERROR = 0;
		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta = (String) req.getParameter("txtFechaAlta");
		EXISTE_ERROR = 0;
		int err [] = new int [1];
		err[0] = 0;
		//String cadenaBeneficiarios = listarBeneficiarios(err,usuario, contrato, clave_perfil, suc_opera);
		EXISTE_ERROR = err[0];
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);
		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		//req.setAttribute("MenuPrincipal", session.getstrMenu());
		//req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta","s31070h",req));
		/*if (EXISTE_ERROR ==1)
		despliegaPaginaError(cadenaBeneficiarios,"Alta de Beneficiario","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta",req, res);
		else*/
		forwardTemplate("/jsp/pdAltaBenef.jsp", req, res );

		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);
		return salida;
	}


	public int alta(String clave_perfil, String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		int err [] = new int [1];
		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &inicia alta()&", EIGlobal.NivelLog.INFO);
		//String cadenaBeneficiarios = listarBeneficiarios(err,usuario,contrato, clave_perfil, suc_opera);
		String CveBeneficiario=(String) req.getParameter("txtCveBeneficiario");
		String Beneficiario=(String) req.getParameter("txtBeneficiario");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		String loc_trama = "";
		String glb_trama = "";
		String trama_entrada="";
		String trama_salida="";
		String cabecera = "1EWEB";
		String operacion = "ABOP";
		String nombre_benef="";
		String resultado="";
		String mensaje = "";
		int primero=0;
		int ultimo=0;
		int EXISTE_ERROR = 0;

		EXISTE_ERROR = 0;
		cuenta = CveBeneficiario;
		//TuxedoGlobal tuxedoGlobal;
		ServicioTux tuxedoGlobal = new ServicioTux();
		//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		fecha_hoy=ObtenFecha();
		EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &validando datos del beneficiario ...&", EIGlobal.NivelLog.INFO);
		if ( CveBeneficiario.trim().equals("") )
		{
			mensaje = "Debe introducir la cuenta al beneficiario.";
		}
		if ( Beneficiario.trim().equals("") )
		{
			mensaje = "Debe poner un beneficiario.";
		}
		else
		{
			loc_trama = formatea_contrato(contrato) + "@" + CveBeneficiario.trim() + "@" + Beneficiario.trim() + "@" ;
			trama_entrada = cabecera + "|" + usuario + "|" + operacion + "|" + formatea_contrato(contrato) + "|" + usuario + "|" ;
			trama_entrada = trama_entrada + usuario + "|" + loc_trama ;
			EIGlobal.mensajePorTrace( "***pdAltaBenef.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***pdAltaBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			try
			{
				Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
				trama_salida = (String) hs.get("BUFFER");
                                ValidaOTP.guardaBitacora((List)req.getSession().getAttribute("bitacora"),operacion);
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
			}
			catch( java.rmi.RemoteException re )
			{
				re.printStackTrace();
			} catch(Exception e) {}
				if(trama_salida.startsWith("OK"))
				{
					primero = trama_salida.indexOf( (int) '@');
					ultimo  = trama_salida.lastIndexOf( (int) '@');
					resultado = trama_salida.substring(primero+1, ultimo);
					mensaje = resultado;
//					TODO: BIT CU 4281, A4
					/*
					 * VSWF ARR -I
					 */
					 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_ALTA_ALTA_CUENTA_BENEF);
					bt.setContrato(contrato);
					bt.setServTransTux(operacion);
					if(trama_salida!=null){
						 if(trama_salida.substring(0,2).equals("OK")){
							   bt.setIdErr("CONI0000");
						 }else if(trama_salida.length()>8){
							  bt.setIdErr(trama_salida.substring(0,8));
						 }else{
							  bt.setIdErr(trama_salida.trim());
						 }
						}
					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
						&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
								.equals(BitaConstants.VALIDA))
						bt.setIdToken(session.getToken().getSerialNumber());
					req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
					 }
					/*
					 * VSWF ARR -F
					 */
				}
				else
				{
					mensaje = trama_salida ;
				}


			}

			EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &desplegando datos en pantalla ...&", EIGlobal.NivelLog.INFO);
			req.setAttribute("FechaHoy", ObtenFecha(false));
			req.setAttribute("ContUser",ObtenContUser(req));
			req.setAttribute("Fecha", ObtenFecha(true));
			req.setAttribute("mensaje_salida", mensaje);

			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute( "Encabezado", CreaEncabezado( "Alta de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta","s31070h",req) );

			despliegaPaginaErrorURL(mensaje,"Alta de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Alta","s31070h","pdAltaBenef?ventana=0",req,res);
			EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &termina m�todo alta()&", EIGlobal.NivelLog.INFO);
			return salida;
		}

		/*-----------------------------------------------------------------------*/
		String formatea_contrato (String ctr_tmp)
		{
			int i=0;
			String temporal="";
			String caracter="";

			for (i = 0 ; i < ctr_tmp.length() ; i++)
			{
				caracter = ctr_tmp.substring(i, i+1);
				if (caracter.equals("-"))
				{
					// se elimina el gui�n de la cadena
				}
				else
				{
					temporal += caracter ;
				}


			}
			return temporal;
		}




				/*--------------------------------------------------------*/
				/*	public String listarBeneficiarios(int err[] ,String usuario, String contrato, String clave_perfil, short sucOpera)
				{
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &inicia m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
				int    indice;
				String encabezado          = "";
				String tramaEntrada        = "";
				String tramaSalida         = "";
				String path_archivo        = "";
				String nombre_archivo      = "";
				String trama_salidaRedSrvr = "";
				String retCodeRedSrvr      = "";
				String registro            = "";
				String listaBeneficiarios  = "";
				String listaNombreBeneficiario = "";
				String strDebug="";
				//TuxedoGlobal tuxGlobal;
				ServicioTux tuxGlobal = new ServicioTux();
				tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

				String medio_entrega  = "2EWEB";
				String tipo_operacion = "CBOP";
				err[0]=0;
				encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
				encabezado += usuario + "|" + clave_perfil + "|";
				tramaEntrada = contrato + "@";
				tramaEntrada = encabezado + tramaEntrada;
				strDebug += "\ntecb :" + tramaEntrada;

				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
				try
				{
				Hashtable hs = tuxGlobal.web_red(tramaEntrada);
				tramaSalida = (String) hs.get("BUFFER");
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
				}
				catch( java.rmi.RemoteException re )
				{
				re.printStackTrace();
				} catch (Exception e) {}
				strDebug += "\ntscb:" + tramaSalida;

				path_archivo = tramaSalida;
				nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', EIGlobal.NivelLog.DEBUG);
				path_archivo = "/tmp/" + nombre_archivo;
				EIGlobal.mensajePorTrace("nombre_archivo" + nombre_archivo, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("path_archivo" + path_archivo, EIGlobal.NivelLog.INFO);
				strDebug += "\npath_cb:" + path_archivo;

				boolean errorFile=false;
				try
				{
				// Copia Remota
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
				ArchivoRemoto archivo_remoto = new ArchivoRemoto();

				if(!archivo_remoto.copia(nombre_archivo ) )
				{
				// error al copiar
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &error de remote shell ...&", EIGlobal.NivelLog.INFO);
				}
				else
				{
				// OK
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
				}

				File drvSua = new File(path_archivo);
				RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
				trama_salidaRedSrvr = fileSua.readLine();
				retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);

				if (retCodeRedSrvr.equals("OK"))
				{
				while((registro = fileSua.readLine()) != null)
				{
				/////
				if( registro.trim().equals(""))
				registro = fileSua.readLine();
				if( registro == null )
				break;
				listaBeneficiarios += "<option value=\"" + EIGlobal.BuscarToken(registro, ';', 1) + "\"> " + EIGlobal.BuscarToken(registro, ';', 1) +"   "+ EIGlobal.BuscarToken(registro, ';', 2) +"</option>\n";
				}
				}
				else
				{
				listaBeneficiarios += trama_salidaRedSrvr;
				err[0] = 1;
				}
				fileSua.close();
				}
				catch(Exception ioeSua )
				{
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  Excepci�n en listarBeneficiarios() >>"+ioeSua.getMessage()+"<<",1 );
				}
				EIGlobal.mensajePorTrace( "***pdAltaBenef.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
				return(listaBeneficiarios);
				}*/

        public  void guardaParametrosEnSession (HttpServletRequest request){

          EIGlobal.mensajePorTrace("1---------Interrumple el flujo--"+request.getParameter("ventana" ), EIGlobal.NivelLog.INFO);
          String template= request.getServletPath();
          request.getSession().removeAttribute("bitacora");

          request.getSession().setAttribute("plantilla",template);

          HttpSession session = request.getSession(false);


          if(session != null){

            Map tmp = new HashMap();
        //    Enumeration enumer = request.getParameterNames();

            EIGlobal.mensajePorTrace("5--------------->enumer**********--->"+request.getParameter("ventana" ), EIGlobal.NivelLog.INFO);
            tmp.put("ventana",request.getParameter("ventana" ));
            tmp.put("txtCveBeneficiario",request.getParameter("txtCveBeneficiario" ));
            tmp.put("txtBeneficiario",request.getParameter("txtBeneficiario" ));
            tmp.put("txtFechaAlta",request.getParameter("txtFechaAlta" ));

          /*  while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
               EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name), EIGlobal.NivelLog.INFO);

                tmp.put(name,request.getParameter(name));
            }*/
            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }

    }
			}