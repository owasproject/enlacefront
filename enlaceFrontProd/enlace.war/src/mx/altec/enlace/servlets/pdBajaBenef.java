package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.lang.Number.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;




public class pdBajaBenef extends BaseServlet{
    int numero_cuentas= 0;
    int salida= 0;
    int i = 0;
    String fecha_hoy = "";
    String strFecha = "";
    String strDebug="";
    String VarFechaHoy="";
    short suc_opera = (short)787;
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		String usuario = "";
		String contrato = "";
		String clave_perfil = "";
		if(SesionValida( req, res )){
			usuario = session.getUserID8();
		    contrato = session.getContractNumber();
			clave_perfil = session.getUserProfile();
		    suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		    String ventana = (String) req.getParameter("ventana");
			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &Inicia clase&", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &ventana:"+ventana+"&", EIGlobal.NivelLog.INFO);
			if(ventana.equals("0"))
			{
			   if (session.getFacultad(session.FAC_PADIRMTOBEN_PD))
			   {
//				 TODO: BIT CU 4291, El cliente entra al flujo.
					/*
		    		 * VSWF ARR -I
		    		 */
				   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					if (req.getParameter(BitaConstants.FLUJO)!=null){
					       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
					  }else{
					   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					  }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_BAJA_ENTRA);
					bt.setContrato(session.getContractNumber());

					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						e.printStackTrace();
					}catch(Exception e){

						e.printStackTrace();
					}
				   }
		    		/*
		    		 * VSWF ARR -F
		    		 */
			     inicia(clave_perfil,contrato,usuario,req, res);
			   }
			   else
			   {
                  req.setAttribute("Error","No tiene facultad para dar de baja beneficiarios");
		  	      despliegaPaginaErrorURL("No tiene facultad para dar de baja beneficiarios", "Baja de Beneficiarios", "Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja", "s31071h", "pdBajaBenef?ventana=0", req, res);
			   }
		    }
			if(ventana.equals("1"))
			{
               baja(clave_perfil,contrato, usuario, req, res );
		    }
		}
    }
	/*----------------------------------------------------------------------*/
	public int inicia(String clave_perfil, String contrato, String usuario,HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &inicia m�todo inicia()&", EIGlobal.NivelLog.INFO);
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		int EXISTE_ERROR = 0;
		EXISTE_ERROR =0;
		String cadenaBeneficiarios = listarBeneficiarios(usuario,contrato, clave_perfil, suc_opera);
		fecha_hoy = ObtenFecha();
		strFecha = ObtenFecha(true);
		strFechaAlta = strFecha.substring(6, 10) + ", " + strFecha.substring(3, 5) + "-1, " + strFecha.substring(0, 2);

		req.setAttribute("FechaHoy", ObtenFecha(false) );
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
//		req.setAttribute( "Encabezado", CreaEncabezado( "Baja de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja",cadenaBeneficiarios) );
		req.setAttribute("Fecha", ObtenFecha(true));

		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Baja de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja","s31071h",req) );

		if ( EXISTE_ERROR == 1 ){
			despliegaPaginaError(cadenaBeneficiarios,"Baja de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja", req, res);
			despliegaPaginaError("No se pudieron encontrar beneficiarios","Baja de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja", req, res);
		}
		else
			evalTemplate("/jsp/pdBajaBenef.jsp", req, res );

		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &termina m�todo inicia()&", EIGlobal.NivelLog.INFO);

		return 1;
	}
	/*-----------------------------------------------------------------------*/
	public int baja(String clave_perfil,String contrato, String usuario, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
	    HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &inicia m�todo baja()&", EIGlobal.NivelLog.INFO);
		String cboCveBeneficiario=(String) req.getParameter("cboCveBeneficiario");
		String strFechaAlta=(String) req.getParameter("txtFechaAlta");
		String loc_trama = "";
		String glb_trama = "";
		String trama_entrada="";
		String trama_salida="";
		String cabecera = "1EWEB";
		String operacion = "BBOP";
		String nombre_benef="";
		String resultado="";
		String cuenta = "";
		String mensaje = "";
		int EXISTE_ERROR = 0;
		int primero=0;
		int ultimo=0;
		EXISTE_ERROR = 0;
		String benef =  trama_salida.trim();
		cuenta = cboCveBeneficiario.trim();
		//TuxedoGlobal tuxedoGlobal;
				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());


		fecha_hoy=ObtenFecha();

		if ( cboCveBeneficiario.trim().equals("") ){
			mensaje = "Debe seleccionar una cuenta.";
		}else{
			loc_trama = formatea_contrato(contrato) + "@" + cuenta + "@";
			trama_entrada = cabecera + "|" + clave_perfil + "|" + operacion + "|" + formatea_contrato(contrato) + "|" + clave_perfil + "|" ;
			trama_entrada = trama_entrada + usuario + "|" + loc_trama ;

			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);
			try{
				Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
				trama_salida = (String) hs.get("BUFFER");
			}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
			} catch (Exception e) {}

			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  trama de salida >>"+trama_salida+"<<", EIGlobal.NivelLog.DEBUG);
			if(trama_salida.startsWith("OK")){
				primero = trama_salida.indexOf( (int) '@');
				ultimo  = trama_salida.lastIndexOf( (int) '@');
				resultado = trama_salida.substring(primero+1, ultimo);
				mensaje = resultado;
//				TODO: BIT CU 4291, A4
				/*
				 * VSWF ARR -I
				 */
				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
				BitaHelper bh = new BitaHelperImpl(req, session,sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_PAGO_DIR_MAN_BENEF_BAJA_BAJA_CUENTA_BENEF);
				bt.setContrato(session.getContractNumber());
				bt.setServTransTux(operacion);
				if(trama_salida!=null){
					 if(trama_salida.substring(0,2).equals("OK")){
						   bt.setIdErr("BBOP0000");
					 }else if(trama_salida.length()>8){
						  bt.setIdErr(trama_salida.substring(0,8));
					 }else{
						  bt.setIdErr(trama_salida.trim());
					 }
					}
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
				 }
				/*
				 * VSWF ARR -F
				 */
			}else{
				mensaje = trama_salida;
			}

		}
		String cadenaBeneficiarios = listarBeneficiarios(usuario,contrato, clave_perfil, suc_opera);
        EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &se despliegan datos en pantalla&", EIGlobal.NivelLog.INFO);
		req.setAttribute("FechaHoy", ObtenFecha(false));
        req.setAttribute("Fecha", ObtenFecha(true));
        req.setAttribute("ContUser",ObtenContUser(req));
        req.setAttribute("cboCveBeneficiarios", cadenaBeneficiarios);
        req.setAttribute("mensaje_salida", mensaje);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute( "Encabezado", CreaEncabezado( "Baja de Beneficiarios","Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja","s31071h",req) );

	    despliegaPaginaErrorURL(mensaje, "Baja de Beneficiarios", "Servicios &gt; Pago Directo &gt; Mantenimiento de Beneficiarios &gt; Baja", "s31071h", "pdBajaBenef?ventana=0", req, res);
        EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &termina m�todo baja()&", EIGlobal.NivelLog.INFO);
		return salida;
	}

	String formatea_contrato (String ctr_tmp){
		int i=0;
		String temporal="";
		String caracter="";

		for (i = 0 ; i < ctr_tmp.length() ; i++){
		    caracter = ctr_tmp.substring(i, i+1);
		    if (caracter.equals("-")){
		        //se elimina el gui�n
		    }else{
		    	temporal += caracter ;
		    }
	   }
	   return temporal;
	}
	/*--------------------------------------------------------*/
	public String listarBeneficiarios(String usuario, String contrato, String clave_perfil, short sucOpera){
		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &inicia m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);
		int    indice;
		String encabezado          = "";
		String tramaEntrada        = "";
		String tramaSalida         = "";
		String path_archivo        = "";
		String nombre_archivo      = "";
		String trama_salidaRedSrvr = "";
		String retCodeRedSrvr      = "";
		String registro            = "";
		String listaBeneficiarios  = "";
		String listaNombreBeneficiario = "";
		int EXISTE_ERROR = 0;
		//TuxedoGlobal tuxGlobal;
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());



		String medio_entrega  = "2EWEB";
		String tipo_operacion = "CBOP";
		encabezado  = medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|";
		encabezado += usuario + "|" + clave_perfil + "|";
		tramaEntrada = contrato + "@";
		tramaEntrada = encabezado + tramaEntrada;
		strDebug += "\ntecb :" + tramaEntrada;

		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  trama de entrada >>"+tramaEntrada+"<<", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

		try{
			Hashtable hs = tuxGlobal.web_red(tramaEntrada);
			tramaSalida = (String) hs.get("BUFFER");
			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  trama de salida >>"+tramaSalida+"<<", EIGlobal.NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re ){
			re.printStackTrace();
		} catch(Exception e) {}
		strDebug += "\ntscb:" + tramaSalida;
		log("\ntscb:" + tramaSalida);
		path_archivo = tramaSalida;
		nombre_archivo = EIGlobal.BuscarToken(path_archivo, '/', 5);
		path_archivo = Global.DOWNLOAD_PATH + nombre_archivo;
		strDebug += "\npath_cb:" + path_archivo;
		log("\npath_cb:" + path_archivo);

		boolean errorFile=false;
		try
		 {

			// Copia Remota
			EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &remote shell ...&", EIGlobal.NivelLog.INFO);
			ArchivoRemoto archivo_remoto = new ArchivoRemoto();

			if(!archivo_remoto.copia( nombre_archivo ) )
			{
				// error al copiar
				EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &error en remote shell ...&", EIGlobal.NivelLog.INFO);
			}
			else
			{
				// OK
				EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &remote shell OK ...&", EIGlobal.NivelLog.INFO);
			}

		    File drvSua = new File(path_archivo);
		    RandomAccessFile fileSua = new RandomAccessFile(drvSua, "r");
		    trama_salidaRedSrvr = fileSua.readLine();
		    retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();
		    if (retCodeRedSrvr.equals("OK"))
		     {
				EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &leyendo archivo ...&", EIGlobal.NivelLog.INFO);
		       while((registro = fileSua.readLine()) != null){
		      	 	/////
					if( registro.trim().equals(""))
						registro = fileSua.readLine();
					if( registro == null )
						break;
					listaBeneficiarios += "<option value=\"" + EIGlobal.BuscarToken(registro, ';', 1) + "\"> " + EIGlobal.BuscarToken(registro, '@', 1) +" "+ EIGlobal.BuscarToken(registro, '@', 2) +"</option>\n";
		        }
		     }
		    fileSua.close();
		 }catch(Exception ioeSua )
		    {
				   EIGlobal.mensajePorTrace( "***pdBajaBenef.class  Excepci�n en listarBeneficiarios >>"+ioeSua.getMessage()+"<<",EIGlobal.NivelLog.ERROR );
			   EXISTE_ERROR =1;
		       return ( ioeSua.getMessage() );
		    }

		EIGlobal.mensajePorTrace( "***pdBajaBenef.class  &termina m�todo listarBeneficiarios()&", EIGlobal.NivelLog.INFO);

		return(listaBeneficiarios);
	}
}