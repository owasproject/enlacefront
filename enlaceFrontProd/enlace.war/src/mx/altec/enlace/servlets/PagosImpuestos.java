/*Banco Santander Mexicano
  Clase PagosImpuestos  Clase donde se generan los pagos de impuestos
  @Autor:Paulina Ventura Agustin
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 01/03/2001 - Quitar codigo Muerto
  modificacion: Rafael Martinez Montiel 10/09/2002 - Correccion de formato en resultado de alta de pago
 */
package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import java.sql.*;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;




public class PagosImpuestos extends mx.altec.enlace.servlets.MPagoImpuestos{

	String datosctas;
	int contador;
    public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("*** PagosImpuestos.class Entrando a execute ", EIGlobal.NivelLog.INFO);
		String opcion =  ( String ) req.getParameter("OPCION");
		if(opcion == null) opcion = "20";
		if (opcion.equals("20"))
			{
			/*
			String parMes = (String)req.getParameter("mes");
			String parAnn = (String)req.getParameter("anio");
			String parDiaI = (String)req.getParameter("diaI");
			String parMesI = (String)req.getParameter("mesI");
			String parAnnI = (String)req.getParameter("anioI");
			String parDiaF = (String)req.getParameter("diaF");
			String parMesF = (String)req.getParameter("mesF");
			String parAnnF = (String)req.getParameter("anioF");
			String parInhabiles = (String)req.getParameter("diasInhabiles");
			if(parMes == null) parMes = "";
			if(parAnn == null) parAnn = "";
			if(parDiaI == null) parDiaI = "";
			if(parMesI == null) parMesI = "";
			if(parAnnI == null) parAnnI = "";
			if(parDiaF == null) parDiaF = "";
			if(parMesF == null) parMesF = "";
			if(parAnnF == null) parAnnF = "";
			if(parInhabiles == null) parInhabiles = "";
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> " + parMes + " -- " + parAnn + " -- " + parDiaI + " -- " + parMesI + " -- " + parAnnI + " -- " + parDiaF + " -- " + parMesF + " -- " + parAnnF + " -- " + parInhabiles + " -- ");
			req.setAttribute("mes",parMes);
			req.setAttribute("anio",parAnn);
			req.setAttribute("diaI",parDiaI);
			req.setAttribute("mesI",parMesI);
			req.setAttribute("anioI",parAnnI);
			req.setAttribute("diaF",parDiaF);
			req.setAttribute("mesF",parMesF);
			req.setAttribute("anioF",parAnnF);
			req.setAttribute("diasInhabiles",parInhabiles);
			*/
			evalTemplate("/jsp/CalendarioHTML.jsp",req,res);
			return;
			}

		boolean sesionvalida = SesionValida( req, res);

        /* Modificacion validacion de facultades 19/09/2002 */
		if(!sesionvalida){return;}

        System.out.println( "req.getSession ().getAttribute (\"TEIMFCAPPAGM\")=" + req.getSession ().getAttribute ("TEIMFCAPPAGM") );
		if( session.getFacultad(session.FAC_CAP_PAG_PI) ) {
//AMT   if( null!=(req.getSession ().getAttribute ("TEIMFCAPPAGM") ) ){
            // Modificación de session 10/10/2002 HOFD
            //BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
            //modifcacion para integracion
		    session.setModuloConsultar(IEnlace.MEnvio_pago_imp);
            if (opcion.equals("0"))
              despliegaPaginaError("Le informamos que por disposición oficial a partir de esta  fecha únicamente podrá realizar su pago de Impuestos Federales (Esquema Anterior) a través de nuestras ventanillas de la red de sucursales Santander.<BR><BR><BR>", "Pago de Impuestos Federales","Servicios &gt; Impuestos federales &gt; Pagos", "s25600h",req, res);
//				pantpagopi( req, res ); Modificación (ARV) req. Q05-34192 Cierre de Pago de Impuestos Federales
			else if (opcion.equals("1"))
				pagopi( req, res );
			else if (opcion.equals("2"))
				comprobantepagopi("s", req, res);
			else if (opcion.equals("3"))
				comprobantepagopi("n", req, res);
			else if (opcion.equals("10")){
//				TODO: BIT CU 4041. El cliente entra al flujo
				/*
	    		 * VSWF ARR -I
	    		 */
				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				if (req.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CONSULTA_ENTRA);
				bt.setContrato(session.getContractNumber());

				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException ex){
					ex.printStackTrace();
				}catch(Exception e){

					e.printStackTrace();
				}
				}
	    		/*
	    		 * VSWF ARR -F
	    		 */
				pantconsultapagospi( req, res );
			}
			else if (opcion.equals("11"))
				consultpagopi( req, res );
			else if (opcion.equals("12"))
				cancelpagospi( req, res );
		} else {
            //despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de Saldos de cuentas de cheques", "Consultas &gt; Saldos &gt; Por cuenta &gt; Cheques", "s25010h",req, res);
            // Modificado por incidencia SANT-324
            //req.getRequestDispatcher ("/EnlaceMig/SinFacultades").forward (req, res);
            //req.getRequestDispatcher ("/SinFacultades").forward (req, res);
            //req.getRequestDispatcher ("/jsp/SinFacRedirect.jsp").forward (req, res);
            //req.getRequestDispatcher ( Global.APP_PATH + "/WEB-INF/classes/EnlaceMig/SinFacultades").forward (req, res);
            String laDireccion = "document.location='SinFacultades'";
            req.setAttribute( "plantillaElegida", laDireccion);
            req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
        }
	}

	//Genera la pantalla inicial para los pagos de impuestos
    private void pantpagopi( HttpServletRequest req, HttpServletResponse res )
    		throws ServletException, IOException{
		EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a pantpagopi", EIGlobal.NivelLog.INFO);
		//String ctas = "";
		int salida = 0;
                BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		//ctas = llamada_servicio_consulta_cuentas_pi(1, req, res);
		//if(ctas!=null){
			//req.setAttribute("cuentasgx",ctas);
			req.setAttribute("defecha",poner_campos_fecha(true,"fechaini","WindowCalendarfi"));
			req.setAttribute("afecha",poner_campos_fecha(true,"fechafin","WindowCalendarff"));
			req.setAttribute("fechapago",poner_campos_fecha(true,"fecha_completa","WindowCalendarfa"));
			req.setAttribute("camposfecha",poner_dia_mes_anio());
			req.setAttribute("fecha_hoy",ObtenFecha());
			req.setAttribute("diasInhabiles", armaDiasInhabilesJS());
			req.setAttribute("MenuPrincipal", session.getStrMenu());
			req.setAttribute("newMenu", session.getFuncionesDeMenu());
			req.setAttribute("Encabezado", CreaEncabezado("Pago de impuestos y derechos federales","Servicios &gt; Impuestos federales &gt; Pagos","s25570h", req));

			evalTemplate(IEnlace.PIPAGO_TMPL, req, res);
		//}
    }

    //llama al servicio WEB_RED para generar el pago
	private int pagopi( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a pagopi", EIGlobal.NivelLog.INFO);
                BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		boolean estatus = false;
		String coderror = "";
		String mensaje ="";
		String contrato = session.getContractNumber();
		String claveperfil = session.getUserProfile();
		String usuario = session.getUserID8();
                EIGlobal.mensajePorTrace("***Entra a las modificaciones*********************************************************************************", EIGlobal.NivelLog.INFO);
                String cuenta = ( String ) req.getParameter("cuenta");
                req.setAttribute("cuenta", cuenta); // Agrego DAG
                String mensajeservicio = "";
		int numeroreferencia = 0;

		if (cuenta.length()>11)
			cuenta = cuenta.substring(0,11);
		else
			EIGlobal.mensajePorTrace("***PagosImpuestos.class cuenta Erronea:"+cuenta, EIGlobal.NivelLog.INFO);

                String rfc = ( String ) req.getParameter("rfc");
                req.setAttribute("rfc", rfc); // Agrego DAG
                String homoclave = ( String ) req.getParameter("homoclave");
                req.setAttribute("homoclave", homoclave); // Agrego DAG
                String razonsocial = ( String ) req.getParameter("nombrerazonsocial");
                req.setAttribute("nombrerazonsocial", razonsocial); // Agrego DAG
                String calle = ( String ) req.getParameter ("calle");
                req.setAttribute("calle", calle); // Agrego DAG
                String colonia = ( String ) req.getParameter ("colonia");
                req.setAttribute("colonia", colonia); // Agrego DAG
                String delegacion = ( String ) req.getParameter ("delegacion");
                req.setAttribute("delegacion", delegacion); // Agrego DAG
                String estado = ( String ) req.getParameter ("estado");
                req.setAttribute("estado", estado); // Agrego DAG
                String ciudad = ( String ) req.getParameter ("ciudad");
                req.setAttribute("ciudad", ciudad); // Agrego DAG
                String cp = ( String ) req.getParameter ("cp");
                req.setAttribute("cp", cp); // Agrego DAG
                String telofc = ( String ) req.getParameter ("telefono");
                req.setAttribute("telefono", telofc); // Agrego DAG
                String fechaini = ( String ) req.getParameter ("fechaini");
                req.setAttribute("fechaini", fechaini); // Agrego DAG
                String fechafin = ( String ) req.getParameter ("fechafin");
                req.setAttribute("fechafin", fechafin); // Agrego DAG
                String fecha = ( String ) req.getParameter ("fecha_completa");
                req.setAttribute("fecha_completa", fecha); // Agrego DAG
                String cvefolio = "";
                String folioaut = ( String ) req.getParameter("folioautorizacion");
                req.setAttribute("folioautorizacion", folioaut); // Agrego DAG

		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi cuenta: "+cuenta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi rfc: "+rfc, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi homoclave: "+homoclave, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi razonsocial: "+razonsocial, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi calle: "+calle, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi colonia: "+colonia, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi delegacion: "+delegacion, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi estado: "+estado, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi ciudad: "+ciudad, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi cp: "+cp, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi telofc: "+telofc, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi fechaini: "+fechaini, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi fechafin: "+fechafin, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi fecha: "+fecha, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("***PagosImpuestos.class pagopi folioautorizacion: "+folioaut, EIGlobal.NivelLog.INFO);

		String fechaini2 = cambiaformatofecha(fechaini);
		String fechafin2 = cambiaformatofecha(fechafin);
		String fecha2  = cambiaformatofecha(fecha);

		String tipmovi = ( String ) req.getParameter ("tpago");
                req.setAttribute("tpago", tipmovi); // Agrego DAG
		String parImporte = ( String ) req.getParameter ("importe");
                req.setAttribute("importe", parImporte); // Agrego DAG
		if(parImporte == null) parImporte = "0.0";
		if(parImporte.trim().equals("")) parImporte = "0.0";
		double importe = (new Double(parImporte)).doubleValue();
		int importesinpunto = (int) importe;

		String tipconsult = ( String ) req.getParameter ("tconsult");
                req.setAttribute("tconsult", tipconsult); // Agrego DAG

        // Aqui Inicia el Codigo DAG
                if(req.getParameter("verificado")==null || (req.getParameter("verificado")!=null && !req.getParameter("verificado").equals("1"))){
                    EIGlobal.mensajePorTrace("***Entra Verificar si es repetido el proceso", EIGlobal.NivelLog.INFO);
                    // Validación contra Tuxedo
                    String trama_validacion = "";
                    if((folioaut==null)||(folioaut.length()==0))
                            trama_validacion="1EWEB|"+usuario+"|VAPA|"+contrato+"|"+usuario+"|"+claveperfil+"|"+cuenta+"|"+razonsocial+"|"+calle+"|"+colonia+"|"+delegacion+"|"+estado+"|"+ciudad+"|"+cp+"|"+telofc+"|"+fechaini2+"|"+fechafin2+"|"+rfc+homoclave+"|"+fecha2+"|"+tipmovi+"|"+importe+"|"+tipconsult+"| |";
                    else
                            trama_validacion="1EWEB|"+usuario+"|VAPA|"+contrato+"|"+usuario+"|"+claveperfil+"|"+cuenta+"|"+razonsocial+"|"+calle+"|"+colonia+"|"+delegacion+"|"+estado+"|"+ciudad+"|"+cp+"|"+telofc+"|"+fechaini2+"|"+fechafin2+"|"+rfc+homoclave+"|"+fecha2+"|"+tipmovi+"|"+importe+"|"+tipconsult+"|"+folioaut+"|";
                    try{
                        ServicioTux tuxGlobal = new ServicioTux();
                        //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                        EIGlobal.mensajePorTrace("***PagosImpuestos WEB_RED-VAPA>>"+trama_validacion, EIGlobal.NivelLog.DEBUG);
                        Hashtable hs = tuxGlobal.web_red(trama_validacion);
                        coderror = (String) hs.get("BUFFER");
                        EIGlobal.mensajePorTrace("***PagosImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
                        if(coderror.startsWith("IMPV0000")){
                            req.setAttribute("estatus","1");
//                          TODO: BIT CU 4051,4061,4071,4081 C4
                            /*
            	    		 * VSWF
            	    		 */
                            if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                            try{
                            BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
                            BitaTransacBean bt = new BitaTransacBean();
                			bt = (BitaTransacBean)bh.llenarBean(bt);
                			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
                				bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
                			else
                				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
                					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
                				else
                					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
                						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
                					else
                						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
                							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);
                			bt.setContrato(session.getContractNumber());
                			if(coderror!=null){
               				 if(coderror.substring(0,2).equals("OK")){
               					   bt.setIdErr("IMPV0000");
               				 }else if(coderror.length()>8){
               					  bt.setIdErr(coderror.substring(0,8));
               				 }else{
               					  bt.setIdErr(coderror.trim());
               				 }
               				}
                			bt.setUsr(usuario);
                			bt.setServTransTux("VAPA");
                			BitaHandler.getInstance().insertBitaTransac(bt);
                			}catch(SQLException e){
                				e.printStackTrace();
                			}catch (Exception e){
                				e.printStackTrace();
                			}
                            }
            	    		/*
            	    		 * VSWF
            	    		 */

                        }
                        else
                            if(coderror.startsWith("IMPV9999")){
                                req.setAttribute("estatus","2");
                                req.setAttribute("MenuPrincipal", session.getStrMenu());
                                req.setAttribute("newMenu", session.getFuncionesDeMenu());
                                req.setAttribute("Encabezado", CreaEncabezado("Pago de impuestos y derechos federales","Servicios &gt; Impuestos Federales &gt; Pagos", "s25570h",req));
                                evalTemplate("/jsp/PMVerificadorPI.jsp", req, res);
                                return 0;
                            }else{
                                req.setAttribute("estatus","0");
                                req.setAttribute("MenuPrincipal", session.getStrMenu());
                                req.setAttribute("newMenu", session.getFuncionesDeMenu());
                                req.setAttribute("Encabezado", CreaEncabezado("Pago de impuestos y derechos federales","Servicios &gt; Impuestos Federales &gt; Pagos", "s25570h",req));
//                              TODO: BIT CU 4051,4061,4071,4081 C4
                                /*
                	    		 * VSWF
                	    		 */
                                if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                                try{
                                	System.out.println("Ya entre a bitacorizar C$");
                                BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
                                BitaTransacBean bt = new BitaTransacBean();
                    			bt = (BitaTransacBean)bh.llenarBean(bt);
                    			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
                    				bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
                    			else
                    				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
                    					bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
                    				else
                    					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
                    						bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
                    					else
                    						if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
                    							bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);
                    			bt.setContrato(session.getContractNumber());
                    			bt.setIdErr(coderror);
                    			bt.setUsr(usuario);
                    			bt.setServTransTux("VAPA");
                    			BitaHandler.getInstance().insertBitaTransac(bt);
                    			}catch(SQLException e){
                    				e.printStackTrace();
                    			}catch (Exception e){
                    				e.printStackTrace();
                    			}
                                }
                				//	bh.cierraFlujo();
                	    		/*
                	    		 * VSWF
                	    		 */
                                evalTemplate("/jsp/PMVerificadorPI.jsp", req, res);
                                return 0;
                            }
                    }catch( java.rmi.RemoteException re){
                            re.printStackTrace();
                    } catch(Exception e) {}
                }
        // Aqui Termina el Codigo DAG
                EIGlobal.mensajePorTrace("***Salio de las modificaciones*********************************************************************************", EIGlobal.NivelLog.INFO);

                String trama_entrada = "";
		if((folioaut==null)||(folioaut.length()==0))
			trama_entrada="1EWEB|"+usuario+"|PIMP|"+contrato+"|"+usuario+"|"+claveperfil+"|"+cuenta+"|"+razonsocial+"|"+calle+"|"+colonia+"|"+delegacion+"|"+estado+"|"+ciudad+"|"+cp+"|"+telofc+"|"+fechaini2+"|"+fechafin2+"|"+rfc+homoclave+"|"+fecha2+"|"+tipmovi+"|"+importe+"|"+tipconsult+"| |";
		else
			trama_entrada="1EWEB|"+usuario+"|PIMP|"+contrato+"|"+usuario+"|"+claveperfil+"|"+cuenta+"|"+razonsocial+"|"+calle+"|"+colonia+"|"+delegacion+"|"+estado+"|"+ciudad+"|"+cp+"|"+telofc+"|"+fechaini2+"|"+fechafin2+"|"+rfc+homoclave+"|"+fecha2+"|"+tipmovi+"|"+importe+"|"+tipconsult+"|"+folioaut+"|";

		try{
			//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

			EIGlobal.mensajePorTrace("***PagosImpuestos WEB_RED-PIMP>>"+trama_entrada, EIGlobal.NivelLog.DEBUG);
			Hashtable hs = tuxGlobal.web_red(trama_entrada);
			coderror = (String) hs.get("BUFFER");
			EIGlobal.mensajePorTrace("***PagosImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re){
			re.printStackTrace();
		} catch(Exception e) {}

		BaseServlet baseServlet = new BaseServlet();
		mensaje = "<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";
		mensaje += "<TR>";
		mensaje += "<td width=80 class=tittabdat align=center>Cuenta cargo</td>";
		mensaje += "<td class=tittabdat align=center width=150>Descripci&oacute;n</td>";
		mensaje += "<td class=tittabdat align=center width=60>RFC</td>";
		mensaje += "<td class=tittabdat align=center width=45>Homo</td>";
		mensaje += "<td class=tittabdat align=center width=90>Importe</td>";
		mensaje += "<td class=tittabdat align=center width=70>Fecha de aplicaci&oacute;n</td>";
		mensaje += "<td class=tittabdat align=center width=70>Estatus</td>";
		mensaje += "<td class=tittabdat align=center width=110>Folio</td>";
		mensaje += "</TR>";
		mensaje += "<TR>";
		mensaje += "<TD class=textabdatobs align=center nowrap>"+cuenta+"</TD>";
		mensaje += "<TD class=textabdatobs nowrap>"+razonsocial+"</TD>";
		mensaje += "<TD class=textabdatobs align=center nowrap>"+rfc+"</TD>";
		mensaje += "<TD class=textabdatobs align=center nowrap>"+homoclave+"</TD>";
		mensaje += "<TD class=textabdatobs align=right nowrap >"+ baseServlet.FormatoMoneda(importesinpunto)+"</TD>";
		mensaje += "<TD class=textabdatobs align=center nowrap>"+fecha+"</TD>";
		mensaje += "<TD class=textabdatobs align=center nowrap>";
		//para probar
		//coderror="OK1231231231231231231231231231231231231231231231123123123";

		if(coderror.substring(0,2).equals("OK")) {
			if (checar_si_es_programada(fecha)==false)
				mensaje += "Pago realizado. " + coderror.substring(8,16);
			else
				mensaje += "Pago programado. " + coderror.substring(8,16);
		}else{
			if (coderror.substring(0,4).equals("MANC")) {
			// Modificacion Rafael Martinez Montiel
			//	mensaje += "<TD class=textabdatobs align=center nowrap><P ALIGN=RIGHT>"+coderror.substring(8,coderror.length())+"</TD>";
				mensaje += "<P ALIGN=RIGHT>"+coderror.substring(8,coderror.length())+"</p>";
			} else {
			// Modificacion Rafael Martinez Montiel
			//	mensaje += "<TD class=textabdatobs align=center nowrap><P ALIGN=RIGHT>"+coderror.substring(16,coderror.length())+"</TD>";
				mensaje += "<P ALIGN=RIGHT>"+coderror.substring(16,coderror.length())+"</p>";
			}
		}
		mensaje += "</TD>";
		mensaje += "<TD class=textabdatobs align=center nowrap>";

		//Temporal de prueba
		//coderror="OK12345678901234567890123456789012345";

		if (coderror.substring(0,2).equals("OK")){
			cvefolio = coderror.substring(16,coderror.length());
			req.getSession ().setAttribute ("Referencia", coderror.substring (9, 16));
			if (tipconsult.equals("A"))
				tipconsult = "A-Internet";
			else if (tipconsult.equals("B"))
				tipconsult = "B-BancaElectronica";
			else if (tipconsult.equals("C"))
				tipconsult = "C-Diskette";
			else if (tipconsult.equals("D"))
				tipconsult = "D-Papel";
			else if (tipconsult.equals("E"))
				tipconsult = "E-Otro";

			if (tipmovi.equals("1"))
				tipmovi = "Provisional";
			else if (tipmovi.equals("2"))
				tipmovi = "Anual";
			else
				tipmovi = "Otro";

			session.setPIfolio(cvefolio);
			session.setPIrfc(rfc);
			session.setPIhomoclave(homoclave);
			session.setPIcuenta(cuenta);
			session.setPIrz(razonsocial);
			session.setPIcalle(calle);
			session.setPIcolonia(colonia);
			session.setPIdel(delegacion);
			session.setPIestado(estado);
			session.setPIciudad(ciudad);
			session.setPItel(telofc);

			if (checar_si_es_programada(fecha)==false)
				mensaje = mensaje + "<A  href=\"javascript:GenerarComprobante('"+cp+"','"+tipmovi+"','"+fechaini+"','"+fechafin+"','"+importe+"','"+fecha+"','"+tipconsult +"');\" >"+cvefolio+"</A>";
			else
				mensaje = mensaje + cvefolio ;

		}else
			mensaje = mensaje+"-------";

		mensaje += "</TD>";
		mensaje += "</TR>";
		mensaje += "<tr><td class=textabref nowrap colspan=8>Si desea obtener su comprobante,d&eacute; click sobre el n&uacute;mero de folio subrayado.</td></tr>";
		mensaje += "</TABLE>";

		if (!(coderror.substring(0,2).equals("OK"))){
			mensaje = mensaje + "<TABLE BORDER=0 ALIGN=CENTER>";
			mensaje = mensaje + "<TR>";
			mensaje = mensaje + "<TD ><CENTER><A HREF=\"javascript:history.go(-2)\" border=0><IMG SRC =\"/gifs/EnlaceMig/gbo25320.gif\" ALING=LEFT ALT=Regresar border=0></A><BR> </CENTER></TD>";
			mensaje = mensaje + "</TR>";
			mensaje = mensaje + "</TABLE>";
		}

		operacionrealizada("Pago de impuestos y derechos federales",mensaje,"Servicios &gt; Impuestos federales &gt; Pagos","s25580h", req, res);
//		TODO: BIT CU 4051,4061,4071,4081 C4
		/*
		 * VSWF
		 */
		if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		try{
			System.out.println("Ya entre a bitacorizar c4");
		BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
        BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_PROVISIONAL))
			bt.setNumBit(BitaConstants.ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO);
		else
			if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_EJERCICIO))
				bt.setNumBit(BitaConstants.ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO);
			else
				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_ENT_FED))
					bt.setNumBit(BitaConstants.ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO);
				else
					if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_PAGO_IMP_DER_PROD_APR))
						bt.setNumBit(BitaConstants.ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO);
		bt.setContrato(session.getContractNumber());
		bt.setIdErr(coderror);
		bt.setUsr(usuario);
		bt.setServTransTux("PIMP");
		BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch (Exception e){
			e.printStackTrace();
		}
		}
		/*
		 * VSWF
		 */
		return  0;
	}


    //Genera el comprobante del pago de impuestos
    private void comprobantepagopi(String linea, HttpServletRequest req, HttpServletResponse res)
    		throws ServletException, IOException{
       BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
       EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a   comprobantepagopi", EIGlobal.NivelLog.INFO);
       EIGlobal.mensajePorTrace("***PagosImpuestos.class linea: "+linea, EIGlobal.NivelLog.INFO);
	   int salida;

	   req.setAttribute("contrato",session.getContractNumber());
       req.setAttribute("ClaveBanco",session.getClaveBanco());

	   EIGlobal.mensajePorTrace("***PagosImpuestos.class linea: Session 1", EIGlobal.NivelLog.INFO);

       if (linea.equals("s")){
	     EIGlobal.mensajePorTrace("***PagosImpuestos.class linea: Session 2", EIGlobal.NivelLog.INFO);
         req.setAttribute("folio",session.getPIfolio());
         req.setAttribute("rfc",session.getPIrfc());
         req.setAttribute("homoclave",session.getPIhomoclave());
         req.setAttribute("cuenta",session.getPIcuenta());
         req.setAttribute("nombrerazonsocial",session.getPIrz());
         req.setAttribute("calle",session.getPIcalle());
         req.setAttribute("colonia",session.getPIcolonia());
         req.setAttribute("delegacion",session.getPIdel());
         req.setAttribute("estado",session.getPIestado());
         req.setAttribute("ciudad",session.getPIciudad());
         req.setAttribute("telefono",session.getPItel());
		 req.setAttribute("importe",( (String) req.getParameter("imp")).substring(0,( (String) req.getParameter("imp")).indexOf(".")));
       }else{
		 req.setAttribute("folio",req.getParameter ("folio"));
         req.setAttribute("rfc",req.getParameter ("rfc"));
         req.setAttribute("homoclave",req.getParameter ("homoclave"));
         req.setAttribute("cuenta",req.getParameter ("cuenta"));
         req.setAttribute("nombrerazonsocial",req.getParameter ("nombrerazonsocial"));
         req.setAttribute("calle",req.getParameter ("calle"));
         req.setAttribute("colonia",req.getParameter ("colonia"));
         req.setAttribute("delegacion",req.getParameter ("delegacion"));
         req.setAttribute("estado",req.getParameter ("estado"));
         req.setAttribute("ciudad",req.getParameter ("ciudad"));
		 EIGlobal.mensajePorTrace("***PagosImpuestos.class ciudad: "+( String ) req.getParameter ("ciudad"), EIGlobal.NivelLog.INFO);
         req.setAttribute("telefono",req.getParameter ("telefono"));
         EIGlobal.mensajePorTrace("***PagosImpuestos.class telefono: "+( String ) req.getParameter ("telefono"), EIGlobal.NivelLog.INFO);
		 req.setAttribute("importe",req.getParameter("imp"));
       }

	   EIGlobal.mensajePorTrace("***PagosImpuestos.class linea: Session 3", EIGlobal.NivelLog.INFO);

       req.setAttribute("cp",req.getParameter ("cp"));
       EIGlobal.mensajePorTrace("***PagosImpuestos.class cp "+( String ) req.getParameter ("cp"), EIGlobal.NivelLog.INFO);

       req.setAttribute("tpago",req.getParameter ("tp"));
       EIGlobal.mensajePorTrace("***PagosImpuestos.class tpago: "+( String ) req.getParameter ("tp"), EIGlobal.NivelLog.INFO);

       req.setAttribute("periodopago",( String ) req.getParameter ("fi")+" a "+( String ) req.getParameter ("ff"));
       EIGlobal.mensajePorTrace("***PagosImpuestos.class fi: "+( String ) req.getParameter ("fi"), EIGlobal.NivelLog.INFO);

       req.setAttribute("fecha",req.getParameter ("fa"));
       req.setAttribute("formaentrega",req.getParameter ("td"));
       EIGlobal.mensajePorTrace("***PagosImpuestos.class td "+( String ) req.getParameter ("td"), EIGlobal.NivelLog.INFO);
		String MenuPrincipal ="";
		MenuPrincipal = session.getstrMenu();
		if(MenuPrincipal==null)
			MenuPrincipal = "";
       req.setAttribute("MenuPrincipal", MenuPrincipal);
       evalTemplate(IEnlace.PICOMP_TMPL, req, res);
    }

    private void pantconsultapagospi( HttpServletRequest req, HttpServletResponse res )
    		throws ServletException, IOException{
      BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
      EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a  pantconsultapagospi", EIGlobal.NivelLog.INFO);
	  int salida;
	  String Faccondec;
      req.setAttribute("defecha",poner_campos_fecha(true,"fechaini","WindowCalendarfini"));
      req.setAttribute("afecha",poner_campos_fecha(true,"fechafin","WindowCalendarffin"));
      req.setAttribute("camposfecha",poner_dia_mes_anio());
      req.setAttribute("fecha_hoy",ObtenFecha());
 	  req.setAttribute("diasInhabiles", armaDiasInhabilesJS());
      req.setAttribute("MenuPrincipal", session.getStrMenu());
      req.setAttribute("newMenu", session.getFuncionesDeMenu());
      req.setAttribute("Encabezado", CreaEncabezado("Consulta de pagos y declaraciones de impuestos","Servicios &gt; Impuestos federales &gt; Consultas","s25600h", req));
//AMT req.setAttribute("faccondec",session.getFAC_CON_DEC_PI()); // las siguientes 4 lineas sustituyen esta
	  if (session.getFacultad(session.FAC_CON_DEC_PI)){
		  Faccondec = "true";
	  }else Faccondec = "false";
      req.setAttribute("faccondec", Faccondec);

	  evalTemplate(IEnlace.PICONS_PAGO_TMPL, req, res );
    }

	//llama al servicio WEB_RED para generar la consulta de los pago de impuestos
    private void consultpagopi( HttpServletRequest req, HttpServletResponse res )
    		throws ServletException, IOException{
      BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
      EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a consultpagopi", EIGlobal.NivelLog.INFO);
      int salida = 0;
      String contrato = session.getContractNumber();
      String claveperfil = session.getUserProfile();
      String usuario = session.getUserID8();
      int numeroreferencia = 0;
      String coderror = "";
      String referencia = "";
      String archivoe = IEnlace.REMOTO_TMP_DIR+"/"+usuario;
      String archivol = IEnlace.LOCAL_TMP_DIR+"/"+usuario;

      String buffer = "0";
      String cvefolio = "";
      String rfc = "";
      String importe = "";

      String opcion = ( String ) req.getParameter("opcionconsulta");
      String fechaini  = ( String ) req.getParameter("fechaini");
      String fechafin  = ( String ) req.getParameter("fechafin");

      String folioini  = ( String ) req.getParameter("folio");
      String referenciaini  =  ( String ) req.getParameter("refererencia");
      String importeini  =  ( String ) req.getParameter("importe");
      String mensaje = "";
	  String cuentasimpuestos = "";

	  EIGlobal.mensajePorTrace("***PagosImpuestos  opcion>>"+opcion, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***PagosImpuestos  fechaini>>"+fechaini, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***PagosImpuestos  fechafin>>"+fechafin, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***PagosImpuestos  folioini>>"+folioini, EIGlobal.NivelLog.INFO);
	  EIGlobal.mensajePorTrace("***PagosImpuestos  importeini>>"+referenciaini, EIGlobal.NivelLog.INFO);


	  fechaini = cambiaformatofecha(fechaini);
      fechafin = cambiaformatofecha(fechafin);

      if( (opcion.equals("1")) ||(opcion.equals("2"))||(opcion.equals("3")))
        buffer = "0";
      else if (opcion.equals("4"))
        buffer = "1";
      else if (opcion.equals("5"))
        buffer = "2";
      cuentasimpuestos = llamada_servicio_consulta_cuentas_pi(2, req, res);
	  EIGlobal.mensajePorTrace("***PagosImpuestos cuentasimpuestos<<"+cuentasimpuestos, EIGlobal.NivelLog.INFO);

      if (cuentasimpuestos!=null){

      String tramaentrada = "";

      tramaentrada="2EWEB|"+usuario+"|CONI|"+contrato+"|"+usuario+"|"+claveperfil+"|"+fechaini+"|"+fechafin+"|"+buffer+"|";
      try{
      	//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

      	Hashtable hs = tuxGlobal.web_red( tramaentrada );
	  	EIGlobal.mensajePorTrace("***PagosImpuestos WEB_RED-CONI>>"+tramaentrada, EIGlobal.NivelLog.DEBUG);
      	coderror = (String) hs.get("BUFFER");
	  	EIGlobal.mensajePorTrace("***PagosImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
	  }catch( java.rmi.RemoteException re){
	  	re.printStackTrace();
	  } catch (Exception e) {}
 	  if(  (coderror.equals(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8())) ||
		   (coderror.equals(IEnlace.REMOTO_TMP_DIR + "/"+ session.getUserID8()))){

 		 ArchivoRemoto archR = new ArchivoRemoto();

 		 if(!archR.copia(session.getUserID8()))
		    EIGlobal.mensajePorTrace("***PagosImpuestos.class No se pudo archivo remoto:consultapagospi:"+IEnlace.REMOTO_TMP_DIR+session.getUserID8(), EIGlobal.NivelLog.INFO);
		 else
   		    EIGlobal.mensajePorTrace("***PagosImpuestos.class archivo remoto copiado:consultapagospi:"+IEnlace.REMOTO_TMP_DIR+session.getUserID8(), EIGlobal.NivelLog.INFO);



         mensaje = lecturapagospi(archivol,buffer,referenciaini,folioini,importeini,false,cuentasimpuestos, req, res);
         if (mensaje!=null){

		   req.setAttribute("mensaje_salida",mensaje);
           String titulo="";
	       if (buffer.equals("0"))
             titulo="Consulta de Pagos";
           else if (buffer.equals("1"))
             titulo="Consulta de Declaraciones con Pago";
           else
             titulo="Consulta de Declaraciones sin Pago";

           req.setAttribute("titulo",titulo);
           req.setAttribute("tipoconsulta","<INPUT TYPE=hidden NAME=tipoconsulta  VALUE="+buffer+">");
           req.setAttribute("referenciaini","<INPUT TYPE=hidden NAME=referenciaini  VALUE="+referenciaini+">");
           req.setAttribute("folioini","<INPUT TYPE=hidden NAME=folioini  VALUE="+folioini+">");
           req.setAttribute("importeini","<INPUT TYPE=hidden NAME=importeini VALUE="+importeini+">");
           req.setAttribute("fecha_hoy",ObtenFecha());
		   req.setAttribute("tramaok","\""+datosctas+"\"");
		   req.setAttribute("contadorok",""+contador);

           String downloadFile = "<a href = \"/Download/" + session.getUserID8()+".doc" + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A>";
           req.setAttribute("DownLoadFile",downloadFile);

	       req.setAttribute("MenuPrincipal", session.getStrMenu());
           req.setAttribute("newMenu", session.getFuncionesDeMenu());
           req.setAttribute("Encabezado",CreaEncabezado("Consulta de pagos y declaraciones de impuestos","Servicios &gt; Impuestos federales &gt; Consultas","s25620h", req));
           //TODO: BIT CU 4041 A5. El se realiza el envio de trama
           /*
            * VSWF ARR -I
            */
           if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
           try{
        	   if(importeini == null || importeini=="")
        		   importeini ="0";
			BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_CONSULTA_TUX_GENERA_PAGO);
			bt.setContrato(contrato);
			if(coderror!=null){
			 if(coderror.substring(0,2).equals("OK")){
				   bt.setIdErr("CONI0000");
			 }else if(coderror.length()>8){
				  bt.setIdErr(coderror.substring(0,8));
			 }else{
				  bt.setIdErr(coderror.trim());
			 }
			}
			if(referenciaini!=null && !referencia.equals(""))
				bt.setReferencia(Long.parseLong(referenciaini));
			if(importeini!=null && !importeini.equals(""))
				bt.setImporte(Double.parseDouble(importeini));
			bt.setServTransTux("CONI");
			bt.setNombreArchivo(session.getUserID8()+".doc");
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){
				e.printStackTrace();
			}
           }
   		/*
   		 * VSWF ARR -F
   		 */

           evalTemplate(IEnlace.PIPAGO_CONS_TMPL , req, res);
		 }
      }else{
          despliegaPaginaError("Error en Consulta de Pagos  de Impuestos","Consulta de pagos y declaraciones de impuestos","Servicios &gt; Impuestos federales &gt; Consultas","s25600h",req,res);

	  }
      }
    }

	//llama al servicio WERB_RED para cancelar un pago
    private void cancelpagospi( HttpServletRequest req, HttpServletResponse res )
    		throws ServletException, IOException{
       BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
       EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a cancelpagospi", EIGlobal.NivelLog.INFO);
       String contrato = session.getContractNumber();
       String claveperfil = session.getUserProfile();
       String usuario = session.getUserID8();
       String coderror = "";
       String folio;
       String strpagos = ( String ) req.getParameter("pagoseliminar");
       String strcontpagos = ( String ) req.getParameter("contpagoseliminar");
       String mensajeservicio = "";
       String mensaje = "";
       String tipoconsulta = ( String ) req.getParameter("tipoconsulta");
       int numeroreferencia = 0;
	   String tramaentrada = "";


       int cont = Integer.parseInt(strcontpagos);

       mensaje = "<TABLE BORDER=1 ALIGN=CENTER>";
       mensaje = mensaje+"<TR><TD align=center class=tittabdat width=200>Folio</TD><TD class=tittabdat align=center width=60>Estatus</TD></TR>";


       for(int i=0;i<cont;i++){
          folio = strpagos.substring(0,strpagos.indexOf("@"));
          strpagos = strpagos.substring(strpagos.indexOf("@")+1,strpagos.length());
	      tramaentrada = "1EWEB|"+usuario+"|CIMP|"+contrato+"|"+usuario+"|"+claveperfil+"|"+folio+"|"+tipoconsulta+"|";
	      try{
	   		//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   		Hashtable hs = tuxGlobal.web_red(tramaentrada);
			EIGlobal.mensajePorTrace("***PagosImpuestos WEB_RED-CIMP>>"+tramaentrada, EIGlobal.NivelLog.DEBUG);
		  	coderror = (String) hs.get("BUFFER");
          	EIGlobal.mensajePorTrace("***PagosImpuestos<<"+coderror, EIGlobal.NivelLog.DEBUG);
		   }catch(java.rmi.RemoteException re){
		   		re.printStackTrace();
	   	   } catch (Exception e) {}

          mensaje = mensaje+"<TR>";
          mensaje = mensaje+"<TD class=textabdatobs >"+folio +"</TD>";
          mensaje = mensaje+"<TD class=textabdatobs>";
          if (coderror.substring(0,2).equals("OK"))
          {
//        	TODO: BIT CU 4041. El se realiza la cancelacion de pagos
              /*
               * VSWF ARR -I
               */
        	  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
              try{

   			BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
   			BitaTransacBean bt = new BitaTransacBean();
   			bt = (BitaTransacBean)bh.llenarBean(bt);
   			bt.setNumBit(BitaConstants.ES_CONSULTA_CANCELA_PAGP);
   			bt.setContrato(contrato);
			if(coderror!=null){
			 if(coderror.substring(0,2).equals("OK")){
				   bt.setIdErr("CIMP0000");
			 }else if(coderror.length()>8){
				  bt.setIdErr(coderror.substring(0,8));
			 }else{
				  bt.setIdErr(coderror.trim());
			 }
			}
   			bt.setServTransTux("CIMP");
   			BitaHandler.getInstance().insertBitaTransac(bt);
   			}catch(SQLException e){
   				e.printStackTrace();
   			}catch(Exception e){
   				e.printStackTrace();
   			}
        	  }
      		/*
      		 * VSWF ARR -F
      		 */
            mensaje = mensaje+"Cancelaci&oacute;n realizada ";
          }
          else
            mensaje=mensaje+coderror.substring(16,coderror.length());
          mensaje=mensaje+"</TD>";
          mensaje=mensaje+"</TR>";
       }
       mensaje=mensaje+"</TABLE>";
       operacionrealizada("Cancelaci&oacute;n de  pagos de impuestos",mensaje,"Servicios &gt; Impuestos federales &gt; Consultas","", req, res);

    }

    //Si la fecha es la de hoy o mayor retorna TRUE
	//Si la fecha es menor a la de hoy retorna FALSE
    boolean verifica_canc(String fecha,String estatus)
    {

       EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a verifica_canc", EIGlobal.NivelLog.INFO);

	   boolean bandera=false;
	   String fecha_hoy=ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio();
       if (((estatus.charAt(0)=='R')||(estatus.charAt(0)=='E'))||(estatus.charAt(0)=='P'))
         bandera=true;
       else bandera=false;

	   if (bandera==true)
	   {
	     if (fecha.equals(fecha_hoy))
           bandera=true;
         else
           bandera=checar_si_es_programada(fecha);
       }
       return bandera;
    }

	boolean verifica_canc_decl(String fecha,String estatus)
    {
       boolean bandera=false;
	   String fecha_hoy=ObtenDia()+"/"+ObtenMes()+"/"+ObtenAnio();
       if (estatus.charAt(0)=='R')
          bandera=true;
	   else
          bandera=false;

	   if (bandera==true)
	   {
         if (fecha.equals(fecha_hoy))
           bandera=true;
         else
           bandera=checar_si_es_programada(fecha);
	   }
	   return bandera;

    }


    //Lee el archivo donde se encuentran los pagos consultados
    private String lecturapagospi(String archivo,String buffer,String referenciain,String folioin,String  importein,boolean exportacion,String cuentasimpuestos, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException{
       BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
       EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a lecturapagospi", EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("***PagosImpuestos.class referenciain:"+referenciain, EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("***PagosImpuestos.class folioin:"+folioin, EIGlobal.NivelLog.INFO);
	   EIGlobal.mensajePorTrace("***PagosImpuestos.class importein:"+importein, EIGlobal.NivelLog.INFO);

	   boolean primerlinea=false;
       String salida="";
       String linea="";
       String folio="";
       int cont=0;
       String[] apagos=null;
       int residuo=0;
       int indice=0;
       EI_Exportar ArcSal=null;
       String clase="";
	   String formaentrega="";
	   String tipopago="";
	   String rfcobtenido="";

	   if ((referenciain!=null)&&(referenciain.trim().length()==0))
	     referenciain=null;
	   if ((folioin!=null)&&(folioin.trim().length()==0))
	     folioin=null;
	   if ((importein!=null)&&(importein.trim().length()==0))
	     importein=null;


       boolean checar_cancelacion=false;
       ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".doc");
       ArcSal.creaArchivo();
       try{
         BufferedReader lector=new BufferedReader(new FileReader(archivo));
         salida="<table border=0 cellspacing=2 cellpadding=3 class=tabfonbla>";


         if (buffer.equals("0"))
            salida+="<TR><td align=center width=25 class=tittabdat>Seleccionar</td><td align=center width=75 class=tittabdat>Cuenta cargo</td><td align=center class=tittabdat width=55>Fecha de aplicaci&oacute;n</td><td align=center class=tittabdat width=70>Importe</td><td align=center class=tittabdat width=60>Tipo de pago</td><td align=center class=tittabdat width=65>Tipo de declaraci&oacute;n</td><td align=center class=tittabdat width=50>Estatus</td><td align=center class=tittabdat width=110>Folio</td></tr>";
		 else if (buffer.equals("1"))
           salida=salida+"<TR><td align=center class=tittabdat width=110>Folio</td><td align=center width=75 class=tittabdat>RFC</td><td align=center class=tittabdat width=55>Fecha de Recepci&oacute;n</td><td align=center class=tittabdat width=70>Importe</TD><td align=center class=tittabdat width=50>Estatus</TD></TR>";
         else
           salida=salida+"<TR><TD align=center class=tittabdat width=110>Referencia</TD><td align=center class=tittabdat width=55>Fecha de Recepci&oacute;n</TD><td align=center width=75 class=tittabdat>RFC</TD><td align=center class=tittabdat width=55>del periodo</TD><td align=center class=tittabdat width=55>al periodo</td><td align=center class=tittabdat width=50>Estatus</td></tr>";
         datosctas="";
		 while ((linea=lector.readLine())!=null){

		 	if( linea.trim().equals(""))
				linea = lector.readLine();
			if( linea == null )
				break;

		   EIGlobal.mensajePorTrace("***PagosImpuestos.class linea "+linea, EIGlobal.NivelLog.INFO);

           if (primerlinea==false)
             primerlinea=true;
           else{
             indice++;
             if(buffer.equals("0")){
               apagos = desentramaC("11;"+linea,';');



               if (((folioin==null)&&(importein==null))||
                  ((importein!=null)&&(apagos[4].substring(0,apagos[4].indexOf(".")).equals(importein)))||
                  ((folioin!=null)&&(apagos[1].equals(folioin)))){


                    ArcSal.escribeLinea(apagos[1]+" "+apagos[2]+"      "+cambiaformatofecha2(apagos[3])+" "+formatoexport(apagos[4])+"  "+apagos[5]+" "+apagos[6]+" "+apagos[7]+"\r\n");
                    salida+="<TR>";
                    residuo=indice%2;
                    if (residuo==0)
                       clase="textabdatobs";
                    else
					   clase="textabdatcla";

					checar_cancelacion=verifica_canc(apagos[3],apagos[7]);
                    if (checar_cancelacion==false)
                      salida=salida+"<TD align=center class="+clase+" nowrap>&nbsp;</TD>";
                    else
                      salida=salida+"<TD align=center class="+clase+" nowrap><INPUT TYPE=checkbox NAME=cuenta VALUE="+apagos[1]+"></TD>";


                    salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[2]+"</TD>";
                    salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[3]+"</TD>";
                    salida=salida+"<TD class="+clase+" nowrap align=right>"+apagos[4].substring(0,apagos[4].indexOf("."))+"</TD>";
                    if (apagos[5].equals("1")){
                      salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[5]+"-Provisional</TD>";
                      tipopago="Provisional";
					}else  if (apagos[5].equals("2")){
					  salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[5]+"-Anual</TD>";
                      tipopago="Anual";
					}else{
					  salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[5]+"-Otro</TD>";
                      tipopago="otro";
					}

                    switch (apagos[6].charAt(0)){

                     case 'A':salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Internet</TD>";formaentrega="A-Internet";break;
                     case 'B':salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Banca El&eacute;ctronica</TD>";formaentrega="'B-Banca Electronica'";break;
                     case 'C':salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Diskette</TD>";formaentrega="C-Diskette";break;
                     case 'D':salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Papel</TD>"; formaentrega="D-Papel";break;
                     case 'E':salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Otro</TD>"; formaentrega="E-Otro";break;
                    }

					switch (apagos[7].charAt(0)){
					  case 'P' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"-Programado</TD>";break;
					  case 'C' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"-Cancelado</TD>";break;
					  case 'R' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"-Pagado sin documentos</TD>";break;
					  case 'X' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"-No aplicado</TD>";break;
					  case 'E' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"-Sellado en sucursal</TD>";break;
                      default  :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[7]+"</TD>";

					}
					if ((apagos[7].charAt(0)=='R')||(apagos[7].charAt(0)=='E')){
                      datosctas+=indice+";"+obten_trama_cuenta(apagos[2],cuentasimpuestos)+";"+apagos[1]+";"+tipopago+";"+apagos[8]+";"+apagos[9]+";"+apagos[4].substring(0,apagos[4].indexOf("."))+";"+apagos[3]+";"+formaentrega+"@";
					  salida=salida+"<TD class="+clase+" nowrap align=center><A  href=\"javascript:GenerarComprobante("+indice+");\">"+apagos[1]+"</A></TD>";
                    }else
                      salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[1]+"</TD>";
					salida=salida+"</TR>";

               }//del if
             }else if (buffer.equals("1")){
               apagos = desentramaC("11;"+linea,';');
               if (((folioin==null)&&(importein==null))||
                  ((importein!=null)&&(apagos[4].substring(0,apagos[4].indexOf(".")).equals(importein)))||
                  ((folioin!=null)&&(apagos[1].equals(folioin)))){
       		     EIGlobal.mensajePorTrace("***PagosImpuestos.class fecha         "+apagos[11], EIGlobal.NivelLog.INFO);
       		     EIGlobal.mensajePorTrace("***PagosImpuestos.class estatus "+apagos[10], EIGlobal.NivelLog.INFO);

                 rfcobtenido=obten_rfc_cuenta(apagos[2],cuentasimpuestos);
				 ArcSal.escribeLinea(apagos[1]+" "+rfcobtenido+"      "+cambiaformatofecha2(apagos[11])+" "+formatoexport(apagos[4])+"  "+apagos[10]);
                 if (rfcobtenido==null)
					 rfcobtenido="&nbsp;";

                   residuo=indice%2;
                   if (residuo==0)
                       clase="textabdatobs";
                    else
					   clase="textabdatcla";
				   if (verifica_canc_decl(apagos[11],apagos[10])==true)
                     salida=salida+"<TR><TD class="+clase+" nowrap align=center><INPUT TYPE=checkbox NAME=cuenta VALUE="+apagos[1]+">"+apagos[1]+"</TD>";
				   else
                     salida=salida+"<TR><TD class="+clase+" nowrap align=center>"+apagos[1]+"</TD>";

				   salida=salida+"<TD class="+clase+" nowrap align=center>"+rfcobtenido+"</TD>";
                   salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[11]+"</TD>";
                   salida=salida+"<TD class="+clase+" nowrap align=right>"+apagos[4].substring(0,apagos[4].indexOf("."))+"</TD>";
				   switch (apagos[10].charAt(0))
					{
					  case 'C' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[10]+"-Cancelado</TD>";break;
					  case 'R' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[10]+"-Recibido</TD>";break;
					  case 'E' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[10]+"-Enviado</TD>";break;
                      default  :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[10]+"</TD>";
					}
               }
             }else{
               apagos = desentramaC("6;"+linea,';');
               if ( (referenciain==null)||
                    ((referenciain!=null)&&(apagos[1].equals(referenciain)))){

					ArcSal.escribeLinea(apagos[1]+" "+cambiaformatofecha2(apagos[2])+" "+apagos[3]+" "+apagos[4]+" "+apagos[5]+" "+apagos[6]);

                   residuo=indice%2;
                   if (residuo==0)
                      clase="textabdatobs";
                   else
				      clase="textabdatcla";
                   if (verifica_canc_decl(apagos[2],apagos[6])==true)
                     salida=salida+"<TR ><TD class="+clase+" nowrap align=center><INPUT TYPE=checkbox NAME=cuenta VALUE="+apagos[1]+">"+apagos[1]+"</TD>";
                   else
                     salida=salida+"<TR ><TD class="+clase+" nowrap align=center>"+apagos[1]+"</TD>";
				   salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[2]+"</TD>";
                   salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[3]+"</TD>";
                   salida=salida+"<TD class="+clase+" nowrap align=right>"+apagos[4]+"</TD>";
                   salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[5]+"</TD>";
				   switch (apagos[6].charAt(0)){
					  case 'C' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Cancelado</TD>";break;
					  case 'R' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Recibido</TD>";break;
					  case 'E' :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"-Enviado</TD>";break;
   				      default  :salida=salida+"<TD class="+clase+" nowrap align=center>"+apagos[6]+"</TD>";break;
				  }
               }
             }
             cont++;

           }//del else
         } //del while
         salida=salida+"</TABLE>";
         lector.close();

        //if (exportacion)
         ArcSal.cierraArchivo();

         ArchivoRemoto archR = new ArchivoRemoto();
         if(!archR.copiaLocalARemoto(session.getUserID8()+".doc","WEB"))
       	    EIGlobal.mensajePorTrace("***PagosImpuestos.class No se pudo crear archivo para exportar pagos de impuestos", EIGlobal.NivelLog.INFO);

		 if (cont==0){
            despliegaPaginaError("No hay registros asociados al contrato","Consulta de pagos y declaraciones de impuestos","Servicios &gt; Impuestos federales &gt; Consultas","s25600h",req,res);
            salida=null;
		 }
       }catch(IOException e){
		 EIGlobal.mensajePorTrace("***PagosImpuestos.class "+e, EIGlobal.NivelLog.ERROR);
        // salida="<p> No es  posible leer el archivo de consulta de pagos y declaraciones  en este momento <br>";
         despliegaPaginaError("No es  posible leer el archivo de consulta de pagos y declaraciones  en este momento","Consulta de pagos y declaraciones de impuestos","Servicios &gt; Impuestos federales &gt; Consultas","s25600h",req,res);
         salida=null;
	   }
	   contador=indice;
       return salida;
    }

    //Formatea el importe para el archivo de exportacion
    private String formatoexport(String  importestring)
    {
       String imp=importestring.substring(0,importestring.indexOf("."));
       String impceros="";

       for (int i=0;i<(16-imp.length());i++)
          impceros=impceros+"0";

       return (impceros+imp);
    }

    String obten_trama_cuenta(String ctabuscar,String cuentas)
	{
      EIGlobal.mensajePorTrace("***PagosImpuestos obten_trama_cuenta<<", EIGlobal.NivelLog.INFO);
      String tramacta="";
      String aux="";

 	  EIGlobal.mensajePorTrace("***PagosImpuestos ctabuscar:<<"+ctabuscar, EIGlobal.NivelLog.INFO);
 	  EIGlobal.mensajePorTrace("***PagosImpuestos cuentas:<<"+cuentas, EIGlobal.NivelLog.INFO);


      while (cuentas.length()>0)
      {
         aux=cuentas.substring(0,cuentas.indexOf("@"));
		 aux=aux.substring(0,cuentas.indexOf(";"));
		 if (aux.equals(ctabuscar))
		 {
		   tramacta=cuentas.substring(0,cuentas.indexOf("@"));
           break;
         }
		 else
	       cuentas=cuentas.substring(cuentas.indexOf("@")+1,cuentas.length());
      }

      return tramacta;
    }


	String obten_rfc_cuenta(String ctabuscar,String cuentas)
	{
      EIGlobal.mensajePorTrace("***PagosImpuestos obten_rfc_cuenta<<", EIGlobal.NivelLog.INFO);
      String tramacta="";
      String aux="";
      String rfc=null;
 	  EIGlobal.mensajePorTrace("***PagosImpuestos ctabuscar:<<"+ctabuscar, EIGlobal.NivelLog.INFO);
 	  EIGlobal.mensajePorTrace("***PagosImpuestos cuentas:<<"+cuentas, EIGlobal.NivelLog.INFO);


      while (cuentas.length()>0)
      {
         aux=cuentas.substring(0,cuentas.indexOf("@"));
		 aux=aux.substring(0,cuentas.indexOf(";"));
		 if (aux.equals(ctabuscar))
		 {
		   tramacta=cuentas.substring(0,cuentas.indexOf("@"));
           break;
         }
		 else
	       cuentas=cuentas.substring(cuentas.indexOf("@")+1,cuentas.length());
      }

      if (tramacta!="")
      {
        EIGlobal.mensajePorTrace("***PagosImpuestos tramacta<<"+tramacta, EIGlobal.NivelLog.INFO);
        for (int i=1;i<10;i++)
        {
           tramacta=tramacta.substring(tramacta.indexOf(";")+1,tramacta.length());
        }
		rfc=tramacta.substring(0,tramacta.indexOf(";"));
      }
      EIGlobal.mensajePorTrace("***PagosImpuestos rfc:<<"+rfc, EIGlobal.NivelLog.INFO);
      return rfc;
    }


    //Cambia el formato de la fecha para el pago de: dd/mm/yyyy a yyyymmdd
    private String cambiaformatofecha(String fecha_origen)
    {
      String dia="";
      String mes="";
      String anio="";
      dia=fecha_origen.substring(0, 2);
      mes=fecha_origen.substring(3, 3);
      anio=fecha_origen.substring(6,10);
      return anio+mes+dia;
    }

	//Cambia el formato de la fecha de: dd/mm/yyyy a ddmmyyyy
    private String cambiaformatofecha2(String fecha_origen)
    {
      return (fecha_origen.substring(0,2)+fecha_origen.substring(3,5)+fecha_origen.substring(6,10));
    }


	String armaDiasInhabilesJS()
    {
      int indice       = 0;
      String resultado = "";
      Vector diasInhabiles;

      diasInhabiles = CargarDias(1);

      if(diasInhabiles!=null)
      {
        resultado = diasInhabiles.elementAt(indice).toString();
        for(indice=1; indice<diasInhabiles.size(); indice++)
          resultado += ", " + diasInhabiles.elementAt(indice).toString();

      }
      return resultado;
    }

	/**
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 * @param formato
	 * @return
	 */
	public Vector CargarDias(int formato)
		{
		EIGlobal.mensajePorTrace("***PagosImpuestos.class Entrando a CargarDias", EIGlobal.NivelLog.INFO);

System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 1 ");

		Connection Conexion = null;
		PreparedStatement qrDias  = null;
		ResultSet rsDias  = null;
		boolean estado;
		String sqlDias;
		Vector diasInhabiles = new Vector();

		//sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI'";
		EIGlobal.mensajePorTrace("***PagosImpuestos.class Query:"+sqlDias, EIGlobal.NivelLog.ERROR);
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 2");
		try
			{
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 3");
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 4");
			if(Conexion!=null)
				{
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 5");
				EIGlobal.mensajePorTrace("***PagosImpuestos.class conexion creada", EIGlobal.NivelLog.ERROR);
				//qrDias = createQuery();
				qrDias = Conexion.prepareStatement(sqlDias);
				//qrDias.setSQL(sqlDias);
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 6");
				if(qrDias!=null)
					{
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 7");
					EIGlobal.mensajePorTrace("***PagosImpuestos.class Query Creado", EIGlobal.NivelLog.ERROR);
					rsDias = qrDias.executeQuery();
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 8");
					if(rsDias!=null)
						{
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 9");
						EIGlobal.mensajePorTrace("***PagosImpuestos.class ResulSet Creado", EIGlobal.NivelLog.ERROR);
						while( rsDias.next())
							{
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>XX 10");
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);
							if(formato == 0)
								{
								dia  =  Integer.toString( Integer.parseInt(dia) );
								mes  =  Integer.toString( Integer.parseInt(mes) );
								anio =  Integer.toString( Integer.parseInt(anio) );
								}

							String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
							diasInhabiles.addElement(fecha);
System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>XX 11");
							}

						}
					else
						EIGlobal.mensajePorTrace("***PagosImpuestos.class Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
					}
				else
					EIGlobal.mensajePorTrace("***PagosImpuestos.class Cannot Create Query", EIGlobal.NivelLog.ERROR);
				}
			else
				EIGlobal.mensajePorTrace("***PagosImpuestos.class Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
			}
		catch( SQLException sqle )
			{
			sqle.printStackTrace();
			} finally {
				EI_Query.cierraConexion(rsDias, qrDias, Conexion);
			}

		return(diasInhabiles);
		}


    static protected String _defaultTemplate ="EnlaceMig/PagosImpuestos";
}