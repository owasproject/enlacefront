package mx.altec.enlace.servlets;

import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.text.*;
import javax.naming.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.MTE_Header;
import mx.altec.enlace.bo.MTE_Registro;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.lang.reflect.*;
import java.lang.*;
import java.sql.*;


public class MTE_Importar extends BaseServlet
 {
	String pipe="|";

	/* Lista de Archivos (jsp) */
	/*
	    /jsp/MTE_ImportaInicio.jsp
		/jsp/MTE_ImportaEnvio.jsp
		/jsp/MTE_ImportaResultado.jsp
	*/

	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	 {
		EIGlobal.mensajePorTrace( "MTE_Importar-> Metodo DoGet", EIGlobal.NivelLog.INFO);
		iniciaServlet( req, res );
	 }

	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	 {
		EIGlobal.mensajePorTrace( "MTE_Importar-> Metodo DoPost", EIGlobal.NivelLog.INFO);
		iniciaServlet( req, res );
	 }

	public void iniciaServlet( HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	 {

		//String app= request.getParameter("app");
		String app= getFormParameter(request,"app");
		//String pagina= (String)request.getParameter("pagina");
		String pagina= getFormParameter(request,"pagina");

		EIGlobal.mensajePorTrace( "MTE_Importar-> Iniciando Importacion TESOFE pagina="+ pagina, EIGlobal.NivelLog.INFO);
		if(pagina==null)
			pagina="1";

		if(app!=null)
		  descargaArchivos(request, response);

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");

		boolean sesionvalida = SesionValida( request, response );
		if ( sesionvalida )
		{
		  if(
			  session.getFacultad(session.FAC_RECDET) || session.getFacultad(session.FAC_RECCON) ||
			  session.getFacultad(session.FAC_IMPDEV) || session.getFacultad(session.FAC_IMPCAN) ||
			  session.getFacultad(session.FAC_ENVDEV) || session.getFacultad(session.FAC_ENVCAN) ||
			  session.getFacultad(session.FAC_CONREG) || session.getFacultad(session.FAC_CONARC)
		   )
			 {

				if(pagina.trim().equals("1")) // Importacion Inicio
				 iniciaImportacion(request,response);
				if(pagina.trim().equals("2")) // Importacion Detalle
				 verificaArchivoImportado(request,response);
				if(pagina.trim().equals("3")) // Importacion Envio
				 enviaArchivoImportado(request,response);
			 }
			else
			 {
                String laDireccion = "document.location='SinFacultades'";

                request.setAttribute( "plantillaElegida", laDireccion);
                request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
             }
		}
	}

	protected void descargaArchivos(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, java.io.IOException
	{
		String nombreDelArchivo= request.getParameter("nombreArchivo");
		String app= request.getParameter("app");
		String val1= request.getParameter("val1");

		BaseResource session = (BaseResource) request.getSession().getAttribute("session");

		if(app.trim().equals("") )
			app="application/x-msdownload";

		if(val1!=null)
			session.setFacultad(val1);
		else
		  {
			EIGlobal.mensajePorTrace( "MTE_Importar-> Tipo de descarga: "+ app, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "MTE_Importar-> Entrando al metodo para descarga de: "+ nombreDelArchivo, EIGlobal.NivelLog.INFO);

			response.setHeader("Content-disposition","attachment; filename=" + nombreDelArchivo );
			response.setContentType(app);

			BufferedInputStream  entrada = null;
			BufferedOutputStream salida = null;
			InputStream arc= new FileInputStream (IEnlace.LOCAL_TMP_DIR+"/"+nombreDelArchivo);

			ServletOutputStream out=response.getOutputStream();
			entrada = new BufferedInputStream(arc);
			salida = new BufferedOutputStream(out);
			byte[] buff = new byte[1024];
			int cantidad=0;

			while( (cantidad = entrada.read(buff, 0, buff.length )) !=-1)
				salida.write(buff,0,cantidad);

			salida.close();
			entrada.close();
		  }
	}

/*************************************************************************************/
/*************************************************************** iniciaImportacion   */
/*************************************************************************************/
	public void iniciaImportacion(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "MTE_Importar-> Inicia importacion de Archivos", EIGlobal.NivelLog.INFO);

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Importaci&oacute;n  (Devoluciones / Cancelaciones)","Servicios &gt; TESOFE &gt; Importaci&oacute;n - Env&iacute;o","s55170h",req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
		req.setAttribute("web_application",Global.WEB_APPLICATION);

		EIGlobal.mensajePorTrace( "MTE_Importar-> Se presenta pantalla de importacion", EIGlobal.NivelLog.INFO);

		//TODO BIT CU 4421 El cliente entra al flujo
		/*
		 * VSWF ARR -I
		 */
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
		try{
		BitaHelper bh = new BitaHelperImpl(req, session, sess);
		bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
		BitaTransacBean bt = new BitaTransacBean();
		bt = (BitaTransacBean)bh.llenarBean(bt);
		bt.setNumBit(BitaConstants.ES_TESOFE_IMPORTACION_ENVIO_ENTRA);
		bt.setContrato(session.getContractNumber());

			BitaHandler.getInstance().insertBitaTransac(bt);
		}catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		 }
		/*
		 * VSWF ARR -F
		 */
		evalTemplate ("/jsp/MTE_ImportaInicio.jsp", req, res );
	}

/*************************************************************************************/
/*************************************************************** iniciaImportacion   */
/*************************************************************************************/
	public void verificaArchivoImportado(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "MTE_Importar-> Verificando archivo importado", EIGlobal.NivelLog.INFO);

		EI_Importar arcImp= new EI_Importar();
		MTE_Header Encabezado;
		MTE_Registro Registro;

        String nombreArchivoImp="";
		String nombreArchivoErr="";
		String usuario="";
		String sessionId="";
		String tipo="";
		String line="";
		String tipoArchivo="";
		GregorianCalendar fechaAplicacion=new GregorianCalendar();
		GregorianCalendar fechaPresentacion=new GregorianCalendar();

		StringBuffer erroresHeader=new StringBuffer("");
		StringBuffer erroresRegistro=new StringBuffer("");

		GregorianCalendar CalHoy = new GregorianCalendar();     //Dia acutal
		GregorianCalendar CalMa�ana = new GregorianCalendar();  //Siguien dia (Natural)
		GregorianCalendar Cal24 = new GregorianCalendar();      //24 horas (Siguiente dia Habil)
		GregorianCalendar Cal48 = new GregorianCalendar();      // 48 horas (2 dias habiles)

		Vector diasNoHabiles = CargarDias(1);

		CalMa�ana.add(Calendar.DATE, 4);

		Cal24=(GregorianCalendar)evaluaDiaSiguienteHabil(Cal24, diasNoHabiles);
		Cal48=(GregorianCalendar)evaluaDiaSiguienteHabil(Cal48, diasNoHabiles);
		Cal48=(GregorianCalendar)evaluaDiaSiguienteHabil(Cal48, diasNoHabiles);

		EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha Cal 24: " + Cal24.get(Calendar.YEAR)+ " Fecha Cal 48: "+  Cal48.get(Calendar.YEAR) , EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha Cal 24: " + Cal24.get(Calendar.MONTH)+ " Fecha Cal 48: "+  Cal48.get(Calendar.MONTH) , EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha Cal 24: " + Cal24.get(Calendar.DATE)+ " Fecha Cal 48: "+  Cal48.get(Calendar.DATE) , EIGlobal.NivelLog.DEBUG);

		boolean erroresEnArchivo=false;
		boolean lecturaArchivo=true;
		double importeTotal=0.0;
		int totalRegistros=0;
		int err=0;

		//tipo=(String)req.getParameter("Tipo");
		tipo = getFormParameter(req,"Tipo");
		//tipoArchivo=(String)req.getParameter("Tipo");
		tipoArchivo=getFormParameter(req,"Tipo");

		//nombreArchivoImp=arcImp.importaArchivo(req,res);
		nombreArchivoImp = getFormParameter(req,"fileNameUpload");
		sessionId = (String) req.getSession().getId();
		usuario=session.getUserID8();
		nombreArchivoErr="MTE_Errores."+usuario+".html";

		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		EIGlobal.mensajePorTrace( "MTE_Importar-> nombre de Archivo: "+ nombreArchivoImp, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_Importar-> nombre de Archivo Err: "+ nombreArchivoErr, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_Importar-> tipo de archivo: " + tipo, EIGlobal.NivelLog.INFO);

		/* Cambiar nombre de arhivo de trabajo por usuario (no funcional).
		if(arcImp.cambiaNombreArchivo(IEnlace.LOCAL_TMP_DIR + "/"+ usuario ))
		  nombreArchivoImp=IEnlace.LOCAL_TMP_DIR + "/"+ usuario ;
		*/

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
		req.setAttribute("web_application",Global.WEB_APPLICATION);

		if(nombreArchivoImp.indexOf("ZIPERROR")>=0)
		 {
			req.setAttribute("Encabezado", CreaEncabezado("Importaci&oacute;n  (Devoluciones / Cancelaciones)","Servicios &gt; TESOFE &gt; Importaci&oacute;n - Env&iacute;o","s55170h",req));
			req.setAttribute("ErroresEnArchivo","cuadroDialogo('El archivo que intent&oacute; importar, no es un archivo comprimido o esta da�ado, revise e intente nuevamente.', 3);");
			evalTemplate ("/jsp/MTE_ImportaInicio.jsp", req, res );
		 }
		else
		 {
			BufferedReader archImp= new BufferedReader( new FileReader(nombreArchivoImp) );
			BufferedWriter archErr = new BufferedWriter( new FileWriter(IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivoErr) );

			boolean IMPDEV=session.getFacultad(session.FAC_IMPDEV);
			boolean IMPCAN=session.getFacultad(session.FAC_IMPCAN);

			EIGlobal.mensajePorTrace( "MTE_Importar-> Facultad IMPDEV >" + IMPDEV, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "MTE_Importar-> Facultad IMPCAN >" + IMPCAN, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "MTE_Importar-> tipoArchivo " + tipoArchivo, EIGlobal.NivelLog.INFO);

			archErr.write(erroresHead());
			line=archImp.readLine();
			if(line==null)
			 {
				EIGlobal.mensajePorTrace( "MTE_Importar-> La linea es nulla, esta vacio o no existe", EIGlobal.NivelLog.INFO);
				lecturaArchivo=false;
			 }

			Encabezado=new MTE_Header();
			if(lecturaArchivo)
			 {
				err = Encabezado.parse(line);
				EIGlobal.mensajePorTrace( "MTE_Importar-> Vector " + Encabezado.listaErrores.size() + " err "+ err, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "MTE_Importar-> Encabezado.codigoOperacion " + Encabezado.codigoOperacion, EIGlobal.NivelLog.INFO);

				/* Verificar la facultad para la importacion de archivos:
				   Archivos de cancelacion
				   Archivos de devolucion
				*/
				if (
					(tipoArchivo.trim().equals("Devoluciones") && IMPDEV)
					 ||
					(tipoArchivo.trim().equals("Cancelaciones") && IMPCAN)
				   )
				 {
					EIGlobal.mensajePorTrace( "MTE_Importar-> Tiene facultad para importar cancelaciones o devoluciones ", EIGlobal.NivelLog.INFO);

					 if(err>0)
					  {
						erroresEnArchivo=true;
						EIGlobal.mensajePorTrace( "MTE_Importar-> Errres en el encabezado " + tipo, EIGlobal.NivelLog.INFO);
						for(int i=0;i<Encabezado.listaErrores.size();i++)
						  {
							erroresHeader.append("<DD><LI>");
							erroresHeader.append(Encabezado.listaErrores.get(i).toString());
						  }
						escribeError(archErr,"Encabezado",erroresHeader.toString());
					  }
					 EIGlobal.mensajePorTrace( "MTE_Importar-> Validando la fecha del encabezado.", EIGlobal.NivelLog.INFO);

					 if(Encabezado.fechaPresentacion==null)
						Encabezado.fechaPresentacion=new GregorianCalendar();
					 fechaPresentacion=Encabezado.fechaPresentacion;
					 /*#####################################################################*/
					 /* FECHA */
					 EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha presentacion year: " + Encabezado.fechaPresentacion.get(Calendar.YEAR)+ " fecha ma�ana: "+  CalMa�ana.get(Calendar.YEAR) , EIGlobal.NivelLog.DEBUG);
					 EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha presentacion month: " + Encabezado.fechaPresentacion.get(Calendar.MONTH)+ " fecha ma�ana: "+  CalMa�ana.get(Calendar.MONTH) , EIGlobal.NivelLog.DEBUG);
					 EIGlobal.mensajePorTrace( "MTE_Importar-> Fecha presentacion date: " + Encabezado.fechaPresentacion.get(Calendar.DATE)+ " fecha ma�ana: "+  CalMa�ana.get(Calendar.DATE) , EIGlobal.NivelLog.DEBUG);
					 /*Encabezado.fechaPresentacion.getTime().getTime() < CalHoy.getTime().getTime()*/
					 if( !fechaIgual(Encabezado.fechaPresentacion,CalHoy)
					   )
					  {
						 erroresEnArchivo=true;
						 escribeError(archErr,"Encabezado","La fecha de presentaci&oacute;n no puede ser diferente al d&iacute;a de hoy.");
					  }
					 /*#####################################################################*/
					 EIGlobal.mensajePorTrace( "MTE_Importar-> Validando los registros", EIGlobal.NivelLog.INFO);
					 if(
						 (tipoArchivo.trim().equals("Devoluciones") && Encabezado.codigoOperacion==60) ||
						 (tipoArchivo.trim().equals("Cancelaciones") && Encabezado.codigoOperacion==63)
					   )
					  {
						 totalRegistros=0;
						 importeTotal=0.0;
						 while( (line=archImp.readLine())!= null )
						  {
							 Registro=new MTE_Registro();
							 EIGlobal.mensajePorTrace( "MTE_Importar-> La longitud de la linea: "+line.length(), EIGlobal.NivelLog.INFO );
							 err=Registro.parse(line);
							 if(Registro.fechaAplicacion==null)
								 Registro.fechaAplicacion=new GregorianCalendar();
							 fechaAplicacion=Registro.fechaAplicacion;

							 if(err>0)
							  {
								 erroresEnArchivo=true;
								 erroresRegistro=new StringBuffer("");
								 erroresRegistro.append("<br>\n<b>L&iacute;nea "+(totalRegistros+2)+"</b>\n<br>");
								 for(int i=0;i<Registro.listaErrores.size();i++)
								  {
									erroresRegistro.append("<DD><LI>");
									erroresRegistro.append(Registro.listaErrores.get(i).toString());
								  }
								 archErr.write(erroresRegistro.toString());
							  }
							 EIGlobal.mensajePorTrace( "MTE_Importar-> tipoArchivo: " + tipoArchivo + " Registro.codigoOperacion " + Registro.codigoOperacion, EIGlobal.NivelLog.INFO);
							 if(
								 (tipoArchivo.trim().equals("Devoluciones") && Registro.codigoOperacion!=60) ||
								 (tipoArchivo.trim().equals("Cancelaciones") && Registro.codigoOperacion!=63)
								)
								  {
									 erroresEnArchivo=true;
									 escribeError(archErr,Integer.toString(totalRegistros+2),"El c&oacute;digo de operaci&oacute;n no corresponde con la categor&iacute;a seleccionada. " + Registro.codigoOperacion);
								  }

							 /*  Claves de arvhivos
								 63 cancelacion >=  60 devolucion >
							 */
							 if(Registro.codigoOperacion==60)
							  if( !(
								      Registro.fechaAplicacion.after(CalHoy) &&
								      Registro.fechaAplicacion.before(Cal48)
								   )
								 )
								{
								  /*##################################################################### */
								  /* FECHA */
								  erroresEnArchivo=true;
								  escribeError(archErr,Integer.toString(totalRegistros+1),"La fecha de aplicaci&oacute;n debe ser mayor a la fecha actual y dentro de las proximas 48 hrs h&aacute;biles. ");
								  /*##################################################################### */
								}

							 if(Registro.codigoOperacion==63)
							  if( !fechaIgual(Registro.fechaAplicacion,CalHoy) )
								{
								  /*##################################################################### */
								  /* FECHA */
								  erroresEnArchivo=true;
								  escribeError(archErr,Integer.toString(totalRegistros+1),"La fecha de aplicaci&oacute;n debe ser igual a la fecha actual. ");
								  /*##################################################################### */
								}

							 totalRegistros++;
							 if(Registro.numeroSecuencia!=totalRegistros)
							  {
								 erroresEnArchivo=true;
								 escribeError(archErr,Integer.toString(totalRegistros+1),"El n&uacute;mero de secuencia no corresponde con el registro. " + Registro.numeroSecuencia);
							  }
							 importeTotal+=Registro.importeAplicar;
						  }
						 EIGlobal.mensajePorTrace( "MTE_Importar-> Importes: " + importeTotal + " contra " + Encabezado.importeTotal, EIGlobal.NivelLog.INFO);
						 if(totalRegistros!=Encabezado.numeroRegistros)
						  {
							erroresEnArchivo=true;
							escribeError(archErr,"Encabezado","El n&uacute;mero de registros en el archivo no corresponde con el especificado. " + Encabezado.numeroRegistros);
						  }
						 if(importeTotal!=Encabezado.importeTotal)
						  {
							erroresEnArchivo=true;
							escribeError(archErr,"Encabezado","El importe especificado no corresponde con el importe global del archivo. Importe en Encabezado: " + FormatoMoneda ((Encabezado.importeTotal/100)) + " Importe en Detalle:  " + FormatoMoneda((importeTotal/100)));
						  }
					  }
					 else
					  {
						 erroresEnArchivo=true;
						 escribeError(archErr,"Encabezado","La categor&iacute;a de la importaci&oacute;n no corresponde con la clave de operaci&oacute;n especificada en el archivo.");
					  }
				}
			   else
				{
				   erroresEnArchivo=true;
				   escribeError(archErr,"General","No tiene facultad para importar archivos de " + tipoArchivo);
				}
			 }
			else
			 {
				erroresEnArchivo=true;
				escribeError(archErr,"General","El archivo especificado no existe o esta vacio. ");
			 }

			archErr.write(erroresFoot());
			archErr.flush();
			archErr.close();
			archImp.close();

			EIGlobal.mensajePorTrace( "MTE_Importar-> Se validaron los registros: " + totalRegistros, EIGlobal.NivelLog.INFO);

			if(erroresEnArchivo)
			 {
			   EIGlobal.mensajePorTrace( "MTE_Importar-> Errores en el archivo de importacion ", EIGlobal.NivelLog.INFO);
			   /* Nombre de archivo de errores para descarga en web. Sin path */
			   ArchivoRemoto archR = new ArchivoRemoto();
			   if(archR.copiaLocalARemoto(nombreArchivoErr,"WEB"))
				 req.setAttribute("ErroresEnArchivo","js_despliegaErrores('"+nombreArchivoErr+"');");
			   else
				 {
				   req.setAttribute("ErroresEnArchivo","cuadroDialogo('Durante la importaci&oacute;n se encontraron algunos errores en el archivo, por favor verifiquelo e intente nuevamente.', 3);");
				   EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
				 }
			   req.setAttribute("Encabezado", CreaEncabezado("Importaci&oacute;n  (Devoluciones / Cancelaciones)","Servicios &gt; TESOFE &gt; Importaci&oacute;n - Env&iacute;o","s55170h",req));

			   evalTemplate ("/jsp/MTE_ImportaInicio.jsp", req, res );
			 }
			else
			{
			  EIGlobal.mensajePorTrace( "MTE_Importar-> No hubo errores en el archivo.", EIGlobal.NivelLog.INFO);

			  req.setAttribute("Encabezado", CreaEncabezado("Importaci&oacute;n  (Devoluciones / Cancelaciones)","Servicios &gt; TESOFE &gt; Importaci&oacute;n - Env&iacute;o","s55180h",req));
			  req.setAttribute("nombreArchivoImp",nombreArchivoImp);
			  req.setAttribute("numeroRegistros",Long.toString(Encabezado.numeroRegistros));
			  req.setAttribute("importeTotal",Double.toString(Encabezado.importeTotal/100));
			  req.setAttribute("claveEmisora",Integer.toString(Encabezado.claveEmisora));
			  req.setAttribute("codigoOperacion",Integer.toString(Encabezado.codigoOperacion));

			  int mesI=fechaAplicacion.get(Calendar.MONTH)+1;
			  int mesJ=fechaPresentacion.get(Calendar.MONTH)+1;
			  String mesC=(mesI<=9)?"0"+mesI:""+mesI;
			  String mesD=(mesJ<=9)?"0"+mesJ:""+mesJ;

			  req.setAttribute("fechaPresentacion",
				  fechaPresentacion.get(Calendar.DATE) + "/" +
				  mesD + "/" +
				  fechaPresentacion.get(Calendar.YEAR)  );

			  req.setAttribute("fechaAplicacion",
				  fechaAplicacion.get(Calendar.DATE) + "/" +
				  mesC + "/" +
				  fechaAplicacion.get(Calendar.YEAR)  );

			  req.setAttribute("tipoArchivo",tipoArchivo);
             //TODO: BIT CU 4141 A5, BIT CU 4421 B3
			  /*
	    		 * VSWF
	    		 */
			  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
			  try{
				BitaHelper bh = new BitaHelperImpl(req, session,sess);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO_IMPORTA_ARCH_NOMINA);

				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO))
	  				bt.setNumBit(BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO_IMPORTA_ARCH_NOMINA);
	  			else
	  				if((req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ES_TESOFE_IMPORTACION_ENVIO))
	  					bt.setNumBit(BitaConstants.ES_TESOFE_IMPORTACION_ENVIO_ENVIO_ARCH);

				bt.setContrato(session.getContractNumber());
				bt.setNombreArchivo(nombreArchivoImp);
				bt.setImporte(Encabezado.importeTotal/100);
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			  }
	    		/*
	    		 * VSWF
	    		 */
			  evalTemplate ("/jsp/MTE_ImportaEnvio.jsp", req, res );
			}
		 }
		EIGlobal.mensajePorTrace( "MTE_Importar-> Se termino el proceso de validacion de archivo", EIGlobal.NivelLog.INFO);
	}


/*************************************************************************************/
/******************************************************************** escribeError   */
/*************************************************************************************/
   public void escribeError(BufferedWriter arch,String linea, String msg) throws ServletException, java.io.IOException
	{
		StringBuffer strErrores=new StringBuffer("");

		strErrores=new StringBuffer("");
		if(linea.trim().equals("General"))
		  strErrores.append("<br>\n<b> Error <font color=red>"+ linea + "</font></b>\n<br>");
		else
		  strErrores.append("<br>\n<b>L&iacute;nea <font color=red>"+ linea + "</font></b>\n<br>");
		strErrores.append("<DD><LI>");
		strErrores.append(msg);
		arch.write(strErrores.toString());
	}

/*************************************************************************************/
/********************************************************************** fechaIgual   */
/*************************************************************************************/
    public boolean fechaIgual(GregorianCalendar f1, GregorianCalendar f2)
	{
		int anio1=f1.get(Calendar.YEAR);
		int anio2=f2.get(Calendar.YEAR);
		int mes1=f1.get(Calendar.MONTH);
		int mes2=f2.get(Calendar.MONTH);
		int dia1=f1.get(Calendar.DATE);
		int dia2=f2.get(Calendar.DATE);

		if(anio1==anio2 && mes1==mes2 && dia1==dia2)
			return true;
		return false;
	}

/*************************************************************************************/
/*********************************************************** enviaArchivoImportado   */
/*************************************************************************************/
	public void enviaArchivoImportado(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String Trama="";
		String Result="";
		String nombreArchivoImp="";
		String tipoArchivo="";
		String strMensaje="";

		String usuario="";
		String contrato="";
		String clavePerfil="";

		String numeroRegistros="";
		String numeroSecuencia="";
		String importeTotal="";

		boolean consulta=false;

		String registrosDevueltos="";
		String importeDevuelto="";
		String errorEnArchivo="";
		String line="";

		StringBuffer lineDuplicados=new StringBuffer("");
		StringBuffer lineCancelados=new StringBuffer("");

		/* Variables de ambiente */
		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();

		EIGlobal.mensajePorTrace( "MTE_Importar-> Inicia el envio de la importacion de Archivos", EIGlobal.NivelLog.INFO);

		ServicioTux TuxGlobal = new ServicioTux();
		//TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		nombreArchivoImp=(String)req.getParameter("nombreArchivoImp");
		tipoArchivo=(String)req.getParameter("tipoArchivo");
		numeroRegistros=(String)req.getParameter("numeroRegistros");
		importeTotal=(String)req.getParameter("importeTotal");

		String nombreArchivo=nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf('/')+1);
		EIGlobal.mensajePorTrace( "MTE_Importar-> Nombre para la copia "+nombreArchivo, EIGlobal.NivelLog.INFO);

		boolean ENVDEV=session.getFacultad(session.FAC_ENVDEV);
		boolean ENVCAN=session.getFacultad(session.FAC_ENVCAN);

		EIGlobal.mensajePorTrace( "MTE_Importar-> Facultad ENVDEV >" + ENVDEV, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_Importar-> Facultad ENVCAN >" + ENVCAN, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_Importar-> tipoArchivo " + tipoArchivo, EIGlobal.NivelLog.INFO);

		ArchivoRemoto archR = new ArchivoRemoto();

		 if(
			(tipoArchivo.trim().equals("Cancelaciones") && ENVCAN)
			  ||
		    (tipoArchivo.trim().equals("Devoluciones") && ENVDEV)
		   )
		 {
			EIGlobal.mensajePorTrace( "MTE_Importar-> Se tiene facultad para el envio de cancelaciones o devoluciones", EIGlobal.NivelLog.INFO);

			if(!archR.copiaLocalARemotoTESOFE(nombreArchivo,Global.DIRECTORIO_LOCAL))
			 {
				EIGlobal.mensajePorTrace( "MTE_Importar-> No se realizo la copia remota a tuxedo.", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "MTE_Importar-> Se detiene el proceso pues no se envia el archivo.", EIGlobal.NivelLog.INFO);

				strMensaje="<br><i><font color=red>Error</font></i> de comunicaciones.<br><br> Por favor realice la importaci&oacute;n <font color=green> nuevamente </font>.<br> El archivo <font color=blue><b> "+nombreArchivo+ " </b></font> no ha sido enviado.<br><br>";
			 }
			else
			 {
				EIGlobal.mensajePorTrace( "MTE_Importar-> Copia remota OK.", EIGlobal.NivelLog.INFO);

				Trama=generaTrama(clavePerfil,usuario,contrato,"TD07","2EWEB",Global.DIRECTORIO_REMOTO_TESOFE_INTERNET+"/"+nombreArchivo,tipoArchivo.substring(0,1));
				EIGlobal.mensajePorTrace("MTE_Importar->  Trama entrada por generaTrama: "+Trama, EIGlobal.NivelLog.DEBUG);

					try{
						 Hashtable hs = TuxGlobal.web_red(Trama);
						 Result= (String) hs.get("BUFFER") +"";
					}catch( java.rmi.RemoteException re ){
						re.printStackTrace();
					} catch (Exception e) {}



				if(Result==null || Result.equals("null"))
				  strMensaje="<br><br><br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento. Porfavor intente mas tarde.";
				else
				 {
					EIGlobal.mensajePorTrace("MTE_Importar-> Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
					EI_Tipo trRes=new EI_Tipo();
					if(Result.trim().substring(0,2).equals("OK"))
					 {
						consulta=true;
						trRes.iniciaObjeto('@','|',Result+"|");
						//Se requiere el archivo con path
						//nombreArchivo=trRes.camposTabla[0][2].substring(trRes.camposTabla[0][2].lastIndexOf('/')+1);
						nombreArchivo=trRes.camposTabla[0][2];
						EIGlobal.mensajePorTrace("MTE_Importar-> La consulta fue exitosa.", EIGlobal.NivelLog.INFO);
					 }
					else
					  //Se requiere el archivo con path
					  //nombreArchivo=Result.substring(Result.lastIndexOf('/')+1);
					  //Se tomara el nombre del archivo a partir de la primera '/'
					  nombreArchivo=Result.substring(Result.indexOf("/"));
					/*
					Devoluciones:
					Archivo ok
					more /tmp/80000648376_191
					191@1@1.00@
					---------------------------------
					Archivo err
					/tmp/1001851
					TESD0010     152ARCHIVO YA EXISTE
					*/
					EIGlobal.mensajePorTrace( "MTE_Importar-> Nombre de archivo obtenido:  "+nombreArchivo, EIGlobal.NivelLog.INFO);

					if(archR.copiaTESOFE(nombreArchivo,Global.DIRECTORIO_LOCAL) )
					 {
					   EIGlobal.mensajePorTrace( "MTE_Importar-> Copia remota desde tuxedo se realizo correctamente.", EIGlobal.NivelLog.INFO);
					   BufferedReader arc;
					   try
						 {
						   /* Se le quita el path al archivo para operar con el, en el directorio temporal del app*/
						   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se trajo (E): "+ nombreArchivo, EIGlobal.NivelLog.INFO);
						   nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
						   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se abre (E): "+nombreArchivo, EIGlobal.NivelLog.INFO);

						   arc=new BufferedReader(new FileReader(nombreArchivo));
						   if((line=arc.readLine())!=null)
							{
							  EIGlobal.mensajePorTrace( "MTE_Importar-> Se leyo del archivo: "+line, EIGlobal.NivelLog.INFO);
							  if(!consulta)
								errorEnArchivo=line.trim().substring(16);
							  else
							   {
								 /****************************  Devoluciones *********/
								 if(tipoArchivo.trim().equals("Devoluciones"))
								   {
									 EIGlobal.mensajePorTrace( "MTE_Importar-> Importe Total enviado: "+importeTotal, EIGlobal.NivelLog.INFO);
									 EIGlobal.mensajePorTrace( "MTE_Importar-> Numero registros enviados: "+numeroRegistros, EIGlobal.NivelLog.INFO);
									 EI_Tipo Sec=new EI_Tipo();
									 Sec.iniciaObjeto('@','|',line+"|");
									 numeroSecuencia=Sec.camposTabla[0][0];
									 registrosDevueltos=Sec.camposTabla[0][1];
									 importeDevuelto=Sec.camposTabla[0][2];
									 EIGlobal.mensajePorTrace( "MTE_Importar-> Importe Total devuelto: "+importeDevuelto, EIGlobal.NivelLog.INFO);
									 EIGlobal.mensajePorTrace( "MTE_Importar-> Numero registros devuelto: "+registrosDevueltos, EIGlobal.NivelLog.INFO);

									 int regDev=Integer.parseInt(numeroRegistros)-Integer.parseInt(registrosDevueltos);
									 EIGlobal.mensajePorTrace( "MTE_Importar-> Numero registros duplicados --> " + regDev, EIGlobal.NivelLog.INFO);

									 if(!numeroRegistros.trim().equals(registrosDevueltos.trim()))
									   {
										 //leer archivo y sacar los registros duplicados
										 int c=1;
										 while((line=arc.readLine())!=null && c<=regDev)
										  {
											lineDuplicados.append("" + c++);
											lineDuplicados.append(";");
											lineDuplicados.append(line);
											lineDuplicados.append("@");
										  }
										 consulta=false;
										 errorEnArchivo="DUPLICADOS";
										 EIGlobal.mensajePorTrace("MTE_Importar-> El numero de registros no es el mismo", EIGlobal.NivelLog.INFO);
									   }
								   }
								 /****************************  Cancelaciones *********/
								 else
								   {
									 int d=0;
									 EIGlobal.mensajePorTrace("MTE_Importar-> Recuperando registros que no se cancelaron", EIGlobal.NivelLog.INFO);
									 while((line=arc.readLine())!=null)
									  {
										consulta=false;
										lineCancelados.append("" + (d+1));
										lineCancelados.append(";");
										lineCancelados.append(line);
										lineCancelados.append("@");
										errorEnArchivo="CANCELADOS";
										EIGlobal.mensajePorTrace("MTE_Importar-> Recuperando registros que no se cancelaron", EIGlobal.NivelLog.INFO);
										d++;
									  }

									 if(d==0)
										EIGlobal.mensajePorTrace("MTE_Importar-> Se cancelaron todos los registros.", EIGlobal.NivelLog.INFO);
									 else
										EIGlobal.mensajePorTrace("MTE_Importar-> Hubo algunos registros que no se cancelaron.", EIGlobal.NivelLog.INFO);
								   }
							   }
							}
//							 TODO BIT CU 4421
							/*
							 *VSWF ARR -I
							 * */
							 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
							try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ES_TESOFE_IMPORTACION_ENVIO_ENVIO_ARCH);
							bt.setContrato(contrato);
							bt.setServTransTux("TD07");
							if(Result!=null){
								 if(Result.substring(0,2).equals("OK")){
									   bt.setIdErr("TD070000");
								 }else if(Result.length()>8){
									  bt.setIdErr(Result.substring(0,8));
								 }else{
									  bt.setIdErr(Result.trim());
								 }
								}
							bt.setNombreArchivo(nombreArchivoImp);
							if(importeTotal!=null &&  !importeTotal.equals(""))
								bt.setImporte(Double.parseDouble(importeTotal));
							BitaHandler.getInstance().insertBitaTransac(bt);
							}catch(SQLException e){
							e.printStackTrace();
							}catch(Exception e){
								e.printStackTrace();
							}
							 }
							/*
							 * VSWF ARR -F
							 * */
						   try
							  {
								arc.close();
							  }catch(IOException a){}
						 }

					   catch(IOException e)
						 {EIGlobal.mensajePorTrace( "MTE_Importar-> Error al leer el archivo de tuxedo.", EIGlobal.NivelLog.INFO);}
						if(tipoArchivo.trim().equals("Devoluciones"))
						 {
							if(consulta)
							  strMensaje="<br><i>El archivo ha sido enviado satisfactoriamente ...</i><br><br>Se enviaron <font color=green>"+numeroRegistros+"</font> registros con un importe total de <font color=red> "+ FormatoMoneda(importeTotal)+" </font><br>Su n�mero de secuencia es: <font color=green><b> "+numeroSecuencia+ " </b></font> con &eacute;l podr&aacute; consultar los <b><font color=blue>estatus</font></b><br> de su operaci&oacute;n y/o verificar el detalle del archivo enviado<br><br>";
							else
							  strMensaje="<br><i>El archivo no ha sido enviado correctamente ...</i><br><font color=red>Error:</font> " + errorEnArchivo + "<br><br>Por favor revise su informaci&oacute;n y realice el env&iacute;o <font color=blue>nuevamente</font>.";
						 }
						else
						 {
							if(consulta)
							  strMensaje="<br><i>El archivo ha sido enviado satisfactoriamente.</i><br><br>Se procesar&aacute;n <font color=green>"+numeroRegistros+"</font> registros para la cancelaci&oacute;n.<br>Con un importe total de <font color=red> "+ FormatoMoneda(importeTotal) + "</font><br>Verifique sus operaciones en la <font color=green>consulta de archivos</font>";
							else
							  strMensaje="<br><i>El archivo no ha sido enviado correctamente.</i><br><font color=red>Error:</font> " + errorEnArchivo + "<br><br>Por favor revise su informaci&oacute;n y realice el env&iacute;o <font color=blue>nuevamente</font><br>Todos los registros se han podido cancelar sin nin&uacute;n problema.";
						 }
					 }
					else
					 {
					   EIGlobal.mensajePorTrace( "MTE_Importar-> No se realizo la copia remota desde tuxedo.", EIGlobal.NivelLog.INFO);
					   errorEnArchivo="";
					   if(consulta)
						 strMensaje="<br>El archivo fue enviado pero <font color=red>no</font> se pudo obtener la <font color=green>informaci&oacute;n</font> de registro.<br><br>Verifique en la <font color=blue>consulta de archivos</font> para obtener el detalle del <b>n&uacute;mero</b> de registros que fueron enviados para ser <i>procesados</i>.";
					   else
						 strMensaje="<br><b><font color=red>Transacci&oacute;n</font></b> no exitosa.<br><br>El archivo <font color=blue>no</font> fue enviado correctamente.<br>Porfavor <font color=green>intente</font> en otro momento.";
					   numeroSecuencia="<Consultar>";
					 }
				 }
			 }
		 }
		else
		 {
			EIGlobal.mensajePorTrace( "MTE_Importar-> No se tiene facultad para el envio de cancelaciones o devoluciones", EIGlobal.NivelLog.INFO);
			strMensaje="<br><br>No tiene <font color =red >facultad </font> para el env&iacute;o de archivos de <font color=blue>" + tipoArchivo +"</font>.";
		 }

		if(errorEnArchivo.trim().equals("CANCELADOS"))
		 {
			EIGlobal.mensajePorTrace("MTE_Importar-> Existen registros que no se cancelaron", EIGlobal.NivelLog.INFO);
			EI_Tipo Cancelados=new EI_Tipo();
			String[] titulos={"Detalle de Registros no cancelados",
		       "Registro",
			   "Referencia",
		       "RFC",
			   "Estatus"};

			int[] datos={4,0,0,1,2,3,4};
			int[] values={0};
			int[] align={1,1,0,1,1};

			Cancelados.iniciaObjeto(';','@',lineCancelados.toString());

			strMensaje="<br>Durante el proceso de <font color=green>actualizaci&oacute;n</font> de datos se encontraron algunos registros en <font color=red>proceso</font>. Dichos registros no pudieron ser cancelados.<br>Por favor <i>verifique</i> la informacion. ";
			strMensaje+="<br><br>Se cancelaron <font color=blue>"+(Integer.parseInt(numeroRegistros) - Cancelados.totalRegistros)+"</font> satisfactoriamente y se omitieron <font color=blue>"+Cancelados.totalRegistros+"</font> registros.";

			req.setAttribute("strDuplicadosCancelados",Cancelados.generaTabla(titulos,datos,values,align));
		 }

		if(errorEnArchivo.trim().equals("DUPLICADOS"))
		 {

			EIGlobal.mensajePorTrace("MTE_Importar-> Existen registros que no se registraron por duplicidad", EIGlobal.NivelLog.INFO);
			int diferencia=0;
			EI_Tipo Duplicados=new EI_Tipo();
			String[] titulos={"Detalle de Registros duplicados",
		       "Registro",
			   "Referencia",
		       "RFC",
		       "Secuencia"};

			int[] datos={4,0,0,1,2,3,4,5};
			int[] values={0};
			int[] align={1,1,0,1,0};

			Duplicados.iniciaObjeto(';','@',lineDuplicados.toString());

			diferencia=Integer.parseInt(numeroRegistros)-Integer.parseInt(registrosDevueltos);

			/*
			strMensaje="";
			if(Integer.parseInt(registrosDevueltos)>0)
				strMensaje+="Su n�mero de secuencia es: <font color=green><b> "+numeroSecuencia + "</font>";
			*/
			strMensaje+="<br>Durante el proceso de <font color=green>actualizaci&oacute;n</font> de datos se encontraron algunos registros <font color=red>duplicados</font>. Dichos registros no fueron insertados.<br>Por favor <i>verifique</i> la informacion. ";
			strMensaje+="<br><br>Se encontraron <font color=blue>"+registrosDevueltos+"</font> registros correctos y <font color=blue>"+diferencia+"</font> registros con <font color=red>duplicidad</font> en el sistema. ";
			strMensaje+="El archivo <font color=red><b>NO</b></font> ser&aacute; procesado, revise y env&iacute;e nuevamente.";

			req.setAttribute("strDuplicadosCancelados",Duplicados.generaTabla(titulos,datos,values,align));
		 }

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Importaci&oacute;n  (Devoluciones / Cancelaciones)","Servicios &gt; TESOFE &gt; Importaci&oacute;n - Env&iacute;o","s55190h",req));

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
		req.setAttribute("web_application",Global.WEB_APPLICATION);

		req.setAttribute("strMensaje", strMensaje);

		EIGlobal.mensajePorTrace( "MTE_Importar-> Se presenta pantalla resultados", EIGlobal.NivelLog.INFO);

		evalTemplate ("/jsp/MTE_ImportaResultado.jsp", req, res );
	}


/*************************************************************************************/
/********************************************************************* erroresHead   */
/*************************************************************************************/
 public String generaTrama(String clavePerfil, String usuario, String contrato, String tipoOperacion, String medioEntrega, String nombreArchivoImp, String tipoArchivo)
	{
	  String cabecera="";
	  String regServicio="";

  	  cabecera=	  medioEntrega			+ pipe +
				  usuario				+ pipe +
				  tipoOperacion			+ pipe +
		          contrato				+ pipe +
		          usuario				+ pipe +
		          clavePerfil			+ pipe;

	  regServicio=
	              contrato				+ "@" +
		          nombreArchivoImp		+ "@" +
				  tipoArchivo			+ "@";

      EIGlobal.mensajePorTrace("MTE_Importar-> TipoArchivo en genera Trama: " + tipoArchivo, EIGlobal.NivelLog.INFO);

	  regServicio+=(tipoArchivo.equals("D"))?"0@|":"|";

      return cabecera + regServicio;
	}

/*************************************************************************************/
/********************************************************************* erroresHead   */
/*************************************************************************************/
 public String erroresHead()
	{

	  StringBuffer strH=new StringBuffer("");

	  strH.append("<html>");
	  strH.append("<head>\n<title>Estatus de Importaci&oacute;n</title>\n");
	  strH.append("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	  strH.append("</head>");
	  strH.append("<body bgcolor='white'>");
	  strH.append("<form>");
	  strH.append("<table border=0 width=420 class='textabdatcla' align=center>");
	  strH.append("<tr><th class='tittabdat'>Errores en archivo de importaci&oacute;n</th></tr>");
	  strH.append("<tr><td class='texencconbol' align=center>");
	  strH.append("<table border=0 align=center width='100%'><tr><td class='texencconbol' align=center width=60>");
	  strH.append("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
	  strH.append("<td class='texencconbol' align='center'><br>No se llevo a cabo la importaci&oacute;n del archivo seleccionado<br>ya que se encontraron los siguientes errores.<br><br>");
	  strH.append("</td></tr></table>");
	  strH.append("</td></tr>");
	  strH.append("<tr><td class='texencconbol'>");

	  return strH.toString();
	}

/*************************************************************************************/
/********************************************************************* erroresFoot   */
/*************************************************************************************/
 public String erroresFoot()
	{

	  StringBuffer strF=new StringBuffer("");

	  strF.append("</td></tr>");
	  strF.append("<tr><td class='texencconbol' ><br>Revise el <font color=blue>archivo</font> e intente nuevamente.<br><br>");
	  strF.append("</td></tr>");
	  strF.append("</table>");
	  strF.append("<table border=0 align=center >");
	  strF.append("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
	  strF.append("</table>");
	  strF.append("</form>");
	  strF.append("</body>\n</html>");

	  return strF.toString();
	}

/*************************************************************************************/
/******************************************************** evaluaDiaSiguienteHabil    */
/*************************************************************************************/
 public static Calendar evaluaDiaSiguienteHabil(Calendar fecha, Vector diasNoHabiles)
  {
	 int      indice;
	 int      diaDeLaSemana;
	 boolean  encontrado;

	 System.out.println("EvaluaDiaHabil");
	 System.out.println("Dia  " + fecha.get( fecha.DAY_OF_MONTH) );
	 System.out.println("Mes  " + fecha.get( fecha.MONTH) );
	 System.out.println("Anio " + fecha.get( fecha.YEAR) );

	 do
		{
		   fecha.add( fecha.DAY_OF_MONTH,  1);
		   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
		   encontrado = false;
		   if(diasNoHabiles!=null)
			{
			  for(indice=0; indice<diasNoHabiles.size(); indice++)
			   {
				  String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
				  String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
				  String anio = Integer.toString( fecha.get( fecha.YEAR) );
				  dia=(dia.trim().length()==1)?"0"+dia.trim():dia.trim();
				  mes=(mes.trim().length()==1)?"0"+mes.trim():mes.trim();
				  String strFecha = dia.trim() + "/" + mes.trim() + "/" + anio.trim();

				  if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
				   {
					 System.out.println("Dia es inhabil");
					 encontrado = true;
					 break;
				   }
			   }
			}
	   }while( (diaDeLaSemana == Calendar.SUNDAY) || (diaDeLaSemana == Calendar.SATURDAY) || encontrado );

	 System.out.println("Termina EvaluaDiaHabil");
	 System.out.println("Dia limite encontrado");
	 System.out.println("Dia  " + fecha.get( fecha.DAY_OF_MONTH ) );
	 System.out.println("Mes  " + fecha.get( fecha.MONTH ) );
	 System.out.println("Anio " + fecha.get( fecha.YEAR ) );

	 return fecha;
  }

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace("***transferencia.class Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias = null;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();

	  boolean    estado;

	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("***tranferencia.class Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  //******************
		          }
		       }
			  else
		         EIGlobal.mensajePorTrace("***tranferencia.class  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
		    }
		   else
		      EIGlobal.mensajePorTrace("***transferencia.class  Cannot Create Query", EIGlobal.NivelLog.ERROR);
		 }
		else
		 EIGlobal.mensajePorTrace("***transferencia.class Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
	   }catch(SQLException sqle ){
	   	sqle.printStackTrace();
	   } finally {
		   EI_Query.cierraConexion(rsDias, qrDias, Conexion);
	   }
	 return(diasInhabiles);
	}
}