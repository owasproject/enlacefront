package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.cliente.ws.csd.ConstantesSD;
import mx.altec.enlace.dao.CuentasDAO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.servicios.WSLinCapService;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;
import java.io.*;
import mx.altec.enlace.cliente.ws.csd.BeanDatosOperMasiva;
import mx.altec.enlace.utilerias.SelloDigitalUtils;


/**
 * @author C109834
 * PagoMicrositio
 * Realiza las validaciones y ejecuta el pago desde el Micrositio Enlace
 * Graba Pistas de Auditoria, Bitacora de Operaciones y Envia Notificaciones
 *
 */
public class PagoMicrositio extends BaseServlet
{
    /**
     *Version de clase
     */
    private static final long serialVersionUID = 1L;
    /**
     * URL url de respuesta
     */
    private static final String URL = "url";
    /**
     * Convenio a realizar abono
     */
    private static final String CONVENIO = "convenio";
    /**
     * Referencia de pago
     */
    private static final String REFERENCIA = "referencia";
    /**
     * Importe de pago
     */
    private static final String IMPORTE = "importe";
    /**
     * Concepto de pago
     */
    private static final String CONCEPTO = "concepto";
    /**
     * CodError Codigo de ERROR
     */
    private static final String COD_ERROR = "CodError";
    /**
     * Servicio ID
     */
    private static final String SERVICIO_ID = "servicio_id";
    /**
     * Clave de operacion Micrositio
     */
    private static final String CLAVE_MICROSITIO = "PMIC";
    /**
     * Codigo de Exito de la operacion Micrositio
     */
    private static final String COD_EXITO_MICROSITIO = "PMIC0000";
    /**
     * msgRstOperacion Mensaje resultadop de operacion
     */
    private static final String MSG_RST_OPERACION = "msgRstOperacion";
    /**
     * MsgError Mensaje de error
     */
    private static final String MSG_ERROR= "MsgError";
    /**
     * HoraError hora error
     */
    private static final String HORA_ERROR = "HoraError";

    /**
     * HoraError session
     */
    private static final String SESSION = "session";


    /************************************************************
     * get Implementa doGet del Servlet
     *
     * @param req variable Request
     * @param res variable Response
     * @throws IOException Excepcion Generica
     * @throws ServletException Excepcion Generica
     ***********************************************************/

    public void doGet( HttpServletRequest req, HttpServletResponse res )
            throws ServletException, IOException{
        defaultAction( req, res );
    }
    /************************************************************
     * post implementa doPost del Servlet
     *
     * @param req variable Request
     * @param res variable Response
     * @throws IOException Excepcion Generica
     * @throws ServletException Excepcion Generica
     ***********************************************************/
    public void doPost( HttpServletRequest req, HttpServletResponse res )
            throws ServletException, IOException{
        defaultAction( req, res );
    }
    /************************************************
     * defaultAction Realiza acciones generales para get y post
     * @param req  request
     * @param res  response
     * @throws IOException Excepcion Generica
     * @throws ServletException Excepcion Generica
     ************************************************/
    public void defaultAction( HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        String msgErrorMicrositio2="";
        String codErrorMicrositio2="";
        String cuentaCargo = null;
        String url = "";
        String importe = "";
        String concepto = "";
        String referencia = "";
        String convenio = null;
        String numUsuario  = "";
        String nomContrato = "";
        String numContrato = "";
        String nomUsuario  = "";
        String servicio_id="";
        ArrayList <String> nomUserCont = new ArrayList<String>();
        boolean conToken = false;
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESSION);
        EIGlobal.mensajePorTrace("PagoMicrositio::defaultAction::el contenido del campo URL es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("PagoMicrositio::defaultAction::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)req.getParameter(URL)), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("PagoMicrositio::defaultAction::el contenido del campo URL despues es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);

        if(req.getParameter("token") != null) {
            try {
                importe = java.net.URLDecoder.decode((String) req.getParameter(IMPORTE));
                referencia = java.net.URLDecoder.decode((String) req.getParameter(REFERENCIA));
                url = java.net.URLDecoder.decode((String) req.getParameter(URL));
                concepto = java.net.URLDecoder.decode((String)req.getParameter(CONCEPTO));
                servicio_id = java.net.URLDecoder.decode((String)req.getParameter(SERVICIO_ID));
                nomUserCont.add(importe);
                nomUserCont.add(referencia);
                nomUserCont.add(url);
                nomUserCont.add(concepto);
                nomUserCont.add(servicio_id);
            }
            catch(Exception e) {
                System.out.println("Error en parsear datos...");
            }
        }
        else {
            importe = (String) req.getParameter(IMPORTE);
            referencia = Util.HtmlEncode((String)req.getParameter(REFERENCIA));
            url = req.getParameter(URL);
            concepto = Util.HtmlEncode((String)req.getParameter(CONCEPTO));
            servicio_id = Util.HtmlEncode((String)req.getParameter(SERVICIO_ID));
            nomUserCont.add(importe);
            nomUserCont.add(referencia);
            nomUserCont.add(url);
            nomUserCont.add(concepto);
            nomUserCont.add(servicio_id);
        }
        String sesionValida = ValidaSesionMicrositio(req, res);
        //Se imprime para ampliar analisi de log
        EIGlobal.mensajePorTrace("ValidaSesionMicrositio + sesionValida ->" + sesionValida, EIGlobal.NivelLog.INFO);
        if("OK".equals(sesionValida)){
            try{
                numUsuario  = session.getUserID8();
                nomUsuario  = session.getNombreUsuario();
                numContrato = session.getContractNumber();
                nomContrato = Util.HtmlEncode((String)(session.getNombreContrato()));
                nomUserCont.add(numUsuario);
                nomUserCont.add(nomUsuario);
                nomUserCont.add(numContrato);
                nomUserCont.add(nomContrato);
                System.out.println("PagoMicrositio ...................................................");
                String folioOper = null;
                if(req.getParameter("token") != null) {
                    convenio = java.net.URLDecoder.decode((String) req.getParameter(CONVENIO));
                    cuentaCargo = java.net.URLDecoder.decode((String) req.getParameter("cuentaCargo"));
                    folioOper = java.net.URLDecoder.decode( ((String) req.getParameter("folioOper")) == null ? "" : req.getParameter("folioOper"));
                }
                else {
                    convenio = ( String) req.getParameter(CONVENIO);
                    cuentaCargo = ( String) req.getParameter("cuentaCargo");
                    folioOper = ( String) req.getParameter("folioOper");
                }
                 /**
                 * Correccion vulnerabilidades Deloitte
                 * Stefanini Nov 2013
                 */
                boolean valError = false;

                if(null!=convenio && Util.validaCCX(convenio, 1)){
                    EIGlobal.mensajePorTrace("el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
                } else {
                    req.setAttribute(COD_ERROR, "1003");
                    EIGlobal.mensajePorTrace("El campo convenio no acepta caracteres especiales por favor introduzca solo letras y n�meros->" + convenio, EIGlobal.NivelLog.INFO);
                    valError = true;
                }

                if(null!=importe && Util.validaCCX(importe, 4)){
                    EIGlobal.mensajePorTrace("el contenido del campo importe es correcto->" + importe, EIGlobal.NivelLog.INFO);
                } else {
                    req.setAttribute(COD_ERROR, "1004");
                    EIGlobal.mensajePorTrace("El campo importe no acepta caracteres especiales por favor introduzca n�meros y dos decimales->" + importe, EIGlobal.NivelLog.INFO);
                    valError = true;
                }

                if(null!= cuentaCargo && Util.validaCCX(cuentaCargo, 1)){
                    EIGlobal.mensajePorTrace("el contenido del campo cuentaCargo es correcto->" + cuentaCargo, EIGlobal.NivelLog.INFO);
                } else {
                    req.setAttribute(COD_ERROR, "1009");
                    EIGlobal.mensajePorTrace("El campo cuenta cargo no acepta caracteres especiales por favor introduzca solo n�meros->" + cuentaCargo, EIGlobal.NivelLog.INFO);
                    valError = true;
                }

                if(null!=numContrato && Util.validaCCX(numContrato, 1)){
                    EIGlobal.mensajePorTrace("el contenido del campo numContrato es correcto->" + numContrato, EIGlobal.NivelLog.INFO);
                } else {
                    req.setAttribute(COD_ERROR, "1006");
                    EIGlobal.mensajePorTrace("El campo contrato no acepta caracteres especiales por favor introduzca solo n�meros->" + numContrato, EIGlobal.NivelLog.INFO);
                    valError = true;
                }

                if(valError) {
                    String usrCSS = "";
                    EIGlobal.mensajePorTrace("PagoMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
                    if(null!=req.getHeader("IV-USER")){
                        usrCSS = req.getHeader("IV-USER");
                    }
                    else if(null!=session.getUserID()){
                        usrCSS = session.getUserID();
                    }
                    EIGlobal.mensajePorTrace("PagoMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);

                    sess.setAttribute("usrCSS", usrCSS);
                    req.getSession().setAttribute("inyeccionURL","true");
                    req.setAttribute("inyeccion", true);
                    req.setAttribute (HORA_ERROR, ObtenHora());
                    req.setAttribute(MSG_ERROR, "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
                    evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
                    return;
                }
                /**
                 * Fin correccion vulnerabilidades Deloitte
                 */

                conToken = false;

                /**
                 * Pistas de Auditoria MICE02 - Confirmar Datos
                 */

                if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
                    EIGlobal.mensajePorTrace( "[-----------------------------------------------------------------------]", EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace("Pago Micrositio - defaultAction " +
                            "- Inicia grabado de Pistas de Auditoria Operacion Confirmar Datos",EIGlobal.NivelLog.DEBUG);
                    try{
                        BitaHelper bh = new BitaHelperImpl(req, session, sess);
                        // Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
                        BitaTransacBean bt = new BitaTransacBean();
                        bt = (BitaTransacBean)bh.llenarBean(bt);
                        //Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
                        bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
                        bt.setEstatus("A");
                        bt.setContrato(session.getContractNumber());
                        bt.setUsr(session.getUserID8());
                        if(importe!=null ){
                            try{
                            bt.setImporte(Double.valueOf(importe));
                            }catch (NumberFormatException e) {
                                bt.setImporte(0.0);
                            }
                        }
                        if(cuentaCargo!=null){
                            bt.setCctaOrig(cuentaCargo);
                        }
                        if(convenio!=null){
                            bt.setCctaDest(convenio);
                        }
                        bt.setFechaAplicacion(new Date());
                        bt.setServTransTux(this.CLAVE_MICROSITIO);
                        bt.setNumBit(BitaConstants.ME_BTN_CONF_DATOS);
                        bt.setIdErr(this.COD_EXITO_MICROSITIO);
                        if (session.getContractNumber() != null) {
                            bt.setContrato(session.getContractNumber().trim());
                        }
                        if (session.getUserID8() != null) {
                            bt.setCodCliente(session.getUserID8().trim());
                        }
                        if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
                                && ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
                                        .equals(BitaConstants.VALIDA)) {
                                bt.setIdToken(session.getToken().getSerialNumber());
                        }
                        //Inserto en Bitacoras
                        EIGlobal.mensajePorTrace("Pago Micrositio - defaultAction - " +
                                "bitacorizando: [" + BitaConstants.ME_BTN_CONF_DATOS  + "]", EIGlobal.NivelLog.INFO);
                        BitaHandler.getInstance().insertBitaTransac(bt);
                    }catch (SQLException e) {
                        EIGlobal.mensajePorTrace("Pago Micrositio - SQLException - Exception [" + e + "]", EIGlobal.NivelLog.INFO);
                    }
                    catch (Exception e) {
                        e.getMessage();
                        EIGlobal.mensajePorTrace("Pago Micrositio - Exception [" + e + "]", EIGlobal.NivelLog.INFO);
                    }
                }
                /**
                 * Pistas de Auditoria MICE02 - Confirmar Datos
                 */

                AplicaBN05(req,res, nomUserCont);
                //AplicaBN05(req,res,nomUserCont);
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }else{
            if ("KO".equals(sesionValida)) {
                if (session != null) {
                    int resDup = cierraDuplicidad(session.getUserID8());
                }
                msgErrorMicrositio2 = "Su sesi�n ha expirado.";
                codErrorMicrositio2 = "0999";
                req.setAttribute (COD_ERROR, codErrorMicrositio2);
                req.setAttribute (MSG_ERROR, msgErrorMicrositio2);
                req.setAttribute (HORA_ERROR,ObtenHora());
                req.setAttribute (REFERENCIA, referencia);
                req.setAttribute (IMPORTE, importe);
                req.setAttribute (URL,url);
                req.setAttribute (CONCEPTO,concepto);
                req.setAttribute (SERVICIO_ID,servicio_id);
                evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);  /*MSE*/
            }
        }
    }

    /*----------------------------------------------------------------------------------------------------*/


    /**
     *  //public void AplicaBN05( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
     *  Aplica BN05
     * @param req request
     * @param res response
     * @param nomUserCont
     */
    public void AplicaBN05( HttpServletRequest req, HttpServletResponse res, ArrayList nomUserCont) throws ServletException, IOException {
        final String LOG_METODO = "PagoMicrositio - AplicaBN05 - ";
        EIGlobal.mensajePorTrace(LOG_METODO + "inicio",EIGlobal.NivelLog.DEBUG);
        String msgErrorMicrositio="";
        String codErrorMicrositio="";
        String importe = (String)nomUserCont.get(0);
        String contrato = "";
        String referencia = (String)nomUserCont.get(1);
        String usuario       = "";
        String clave_perfil = "";
        String url = (String)nomUserCont.get(2);
        String concepto = (String)nomUserCont.get(3);
        String servicio_id= (String)nomUserCont.get(4);
        ArrayList <String> res390 = new ArrayList<String>();
        ValidaCuentaContrato validaCuentas = null;
        String sesionValida = ValidaSesionMicrositio( req, res );
        //Valida la sesion
        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESSION);
        if ( "KO".equals(sesionValida)) {
            if (session != null) {
                EIGlobal.mensajePorTrace(LOG_METODO + "Sesion Expirada, se cierra duplicidad",EIGlobal.NivelLog.DEBUG);
                int resDup = cierraDuplicidad(session.getUserID8());
            }
            msgErrorMicrositio = "Su sesi�n ha expirado.";
            codErrorMicrositio = "0999";
            req.setAttribute (COD_ERROR, codErrorMicrositio);
            req.setAttribute (MSG_ERROR, msgErrorMicrositio);
            req.setAttribute (HORA_ERROR,ObtenHora ());
            evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
            return;
        }
        /*
         * Carga los Datos del Correo del usuario
         */
        verificaSuperUsuario(session.getContractNumber(), req);
        EIGlobal.mensajePorTrace(LOG_METODO + "Es SuperUsuario " + session.getUserID8() + " ->" + session.getMensajeUSR().isSuperUsuario(), EIGlobal.NivelLog.INFO);
        subeDatosDeContacto(session.getUserID8(), req);

        /********************* Getronics CP M�xico. Inicia modificaci�n por Q14886 *****************************
            if(session.getFacultad(session.FAC_CARGO_CHEQUES) && session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG)){
        *************************************************************************************************************/
        EIGlobal.mensajePorTrace(LOG_METODO + "Q14884: FAC_CARGO_CHEQUES [" + session.getFacultad(BaseResource.FAC_CARGO_CHEQUES) + "]",EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace(LOG_METODO + "Q14884: FAC_ABOMICRO_PAGCH [" + session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH) + "]",EIGlobal.NivelLog.DEBUG);
        if(session.getFacultad(BaseResource.FAC_CARGO_CHEQUES) && session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH)){
            /********* Getronics CP M�xico. Termina modificaci�n por Q14886 *****************/
            contrato = session.getContractNumber();
            usuario = session.getUserID8();
            clave_perfil = session.getUserProfile();


            /* modificacion para validar que la cuenta pertenezca al contrato seleccionado. */
            validaCuentas = new ValidaCuentaContrato();

            String moduloOrigen = IEnlace.MCargo_transf_pesos;
            String ctaCargo = java.net.URLDecoder.decode((String) req.getParameter("cuentaCargo"));
            String cuentaAbono = java.net.URLDecoder.decode((String) req.getParameter("cuentaAbono"));

             String[] datosCuenta = validaCuentas.obtenDatosValidaCuenta(contrato, usuario, clave_perfil,
                     ctaCargo, moduloOrigen);


             if (null!=cuentaAbono && !Util.validaCCX(cuentaAbono, 1)) {
                req.setAttribute(COD_ERROR, "1008");
                req.setAttribute(MSG_ERROR, "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
                req.setAttribute(HORA_ERROR, ObtenHora());
                req.setAttribute("inyeccion", true);
                evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);
                return;
            } else {
                EIGlobal.mensajePorTrace("el contenido del campo cuentaAbono es correcto ->" + cuentaAbono, EIGlobal.NivelLog.INFO);
            }

             int ctaAbonoValida = consultaB750(usuario,contrato,req,res,cuentaAbono);



            if(datosCuenta==null){
                EIGlobal.mensajePorTrace(LOG_METODO + "***0928 LA CUENTA CARGO PROPORCIONADA NO ES VALIDA",EIGlobal.NivelLog.INFO);
                codErrorMicrositio = "0928";
                msgErrorMicrositio = "La cuenta cargo proporcionada no es v&aacute;lida";
                req.setAttribute (COD_ERROR, codErrorMicrositio);
                req.setAttribute (MSG_ERROR, msgErrorMicrositio);
                req.setAttribute (HORA_ERROR,ObtenHora ());
                evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
            }else if (ctaAbonoValida==1){
                EIGlobal.mensajePorTrace(LOG_METODO + "***0928 LA CUENTA ABONO PROPORCIONADA NO ES VALIDA",EIGlobal.NivelLog.INFO);
                codErrorMicrositio = "0929";
                msgErrorMicrositio = "La cuenta ABONO proporcionada no es v&aacute;lida";
                req.setAttribute (COD_ERROR, codErrorMicrositio);
                req.setAttribute (MSG_ERROR, msgErrorMicrositio);
                req.setAttribute (HORA_ERROR,ObtenHora ());
                evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);

            }else{
                res390=ejecutaTransferenciaMicrositio(clave_perfil,usuario,contrato, nomUserCont, req, res); // Ejecuci�n de transferencia
                //result=ejecutaTransferenciaMicrositio(clave_perfil,usuario,contrato, req, res, nomUserCont ); // Ejecuci�n de transferencia
                int resp390 = new Integer(res390.get(0)).intValue();

                //if(result == 0){
                if(resp390 == 0){
                    EIGlobal.mensajePorTrace(LOG_METODO + "Contrato: ["+ session.getContractNumber() + "]",EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace(LOG_METODO + "Nombre Contrato: ["+ session.getNombreContrato() + "]",EIGlobal.NivelLog.DEBUG);
                    req.setAttribute("NumContrato",session.getContractNumber());
                    req.setAttribute("NomContrato",session.getNombreContrato());
                    String idconvenio = CuentasDAO.consultarCuentaConv(cuentaAbono);
                    if (idconvenio != null && !idconvenio.isEmpty()) {
                        EIGlobal.mensajePorTrace("<----- PagoMicrositio Flujo SD ---- Entro  ** consultarCuentaConv ** |Convenio = "
                                + idconvenio + "| |CuentaAbono = " + cuentaAbono, EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("Importe - " + importe, EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("LineaCaptura - " + referencia, EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("Folio - " + req.getAttribute("folioOper"), EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("Concepto - " + concepto, EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("ServicioID - " + servicio_id , EIGlobal.NivelLog.DEBUG);

                        req.getSession().setAttribute(ConstantesSD.HIDDEN_CONVENIO_TRUE, true);
                        req.getSession().setAttribute(ConstantesSD.MS_PARAM_ID_CONVENIO, idconvenio);
                        List<BeanDatosOperMasiva> operMasivas = new ArrayList<BeanDatosOperMasiva>();
                        operMasivas.add(new BeanDatosOperMasiva(SelloDigitalUtils.armarFchPag(null),
                                Integer.valueOf(ConstantesSD.ID_CANAL_), Double.parseDouble(importe),
                                referencia,
                                cuentaAbono.length() > 11 ? cuentaAbono.substring(0, 11).trim()
                                : cuentaAbono, (String)req.getAttribute("folioOper")));
                        SelloDigitalUtils.guardaMovimientos(req, operMasivas);
                    }

                    evalTemplate("/jsp/operacionPagoMicrositioOK.jsp", req, res);
                    //Duplicidad Pago micrositio CSA             		 
                    EIGlobal.mensajePorTrace("Operacion exitosa Micrositio--> Redirige a template: "+
                    		IEnlace.OPERACION_TMPL_MICROSITIO_OK, EIGlobal.NivelLog.DEBUG);                    
            		res.sendRedirect("/Enlace" + IEnlace.OPERACION_TMPL_MICROSITIO_OK);
            		 
                    
                }else{
                    EIGlobal.mensajePorTrace(LOG_METODO + "Hubo un error al ejecutar la BN05 ",EIGlobal.NivelLog.INFO);
                    if (res390.size()>1){
                        req.setAttribute(COD_ERROR, res390.get(1));
                        EIGlobal.mensajePorTrace(LOG_METODO + "codErrorMicrositio <<" + res390.get(1) + ">>",EIGlobal.NivelLog.INFO);
                        req.setAttribute(MSG_ERROR, res390.get(2));
                        EIGlobal.mensajePorTrace(LOG_METODO + "msgError <<" + res390.get(2) + ">>",EIGlobal.NivelLog.INFO);
                                                /**
                         * Correccion vulnerabilidades Deloitte
                         * Stefanini Nov 2013
                         */
                        if(res390.size() > 3) {
                            req.setAttribute("inyeccion", true);
                        }
                        /** Correccion vulnerabilidades Deloitte */
                    } else {
                        codErrorMicrositio = (String) res390.get(1);
                        req.setAttribute(COD_ERROR, codErrorMicrositio);
                        EIGlobal.mensajePorTrace(LOG_METODO + "codErrorMicrositio <<" + codErrorMicrositio + ">>",EIGlobal.NivelLog.INFO);
                        msgErrorMicrositio = (String) res390.get(2);
                        req.setAttribute(MSG_ERROR, msgErrorMicrositio);
                        EIGlobal.mensajePorTrace(LOG_METODO + "msgError <<" + msgErrorMicrositio + ">>",EIGlobal.NivelLog.INFO);
                    }

                    //req.setAttribute(HORA_ERROR,ObtenHora());

                    // Invoca al metodo de cierre de duplicidad de sesion
                    int resDup = cierraDuplicidad(session.getUserID8());
                    // Borrado de la sesion
                    sess.setAttribute(SESSION, session);
                    sess.removeAttribute(SESSION);
                    sess.setMaxInactiveInterval( 0 );
                    session.setSeguir(false);
                    sess.invalidate();

                    req.setAttribute(REFERENCIA, referencia);
                    req.setAttribute(IMPORTE, importe);
                    req.setAttribute (HORA_ERROR,ObtenHora());
                    req.setAttribute (URL,url);
                    req.setAttribute (CONCEPTO,concepto);
                    req.setAttribute (SERVICIO_ID,servicio_id);
                    evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);  /*MSE*/
                }
            }
        }else{
            msgErrorMicrositio = "No tiene facultades para realizar la operacion";
            codErrorMicrositio = "0099";
            req.setAttribute (COD_ERROR, codErrorMicrositio);
            req.setAttribute (MSG_ERROR, msgErrorMicrositio);
            req.setAttribute (HORA_ERROR,ObtenHora());
            req.setAttribute (REFERENCIA, referencia);
            req.setAttribute (IMPORTE, importe);
            req.setAttribute (URL,url);
            req.setAttribute (CONCEPTO,concepto);
            req.setAttribute (SERVICIO_ID,servicio_id);
            evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);  /*MSE*/
        }
        EIGlobal.mensajePorTrace(LOG_METODO + "Termina clase&",EIGlobal.NivelLog.INFO );
    }

    /**
     * Metodo Encargado para ejecutar la transferencia de pago micrositio
     * @param clave_perfil Clave perfil
     * @param usuario usuario ejecuta
     * @param contrato contrato para transferencia
     * @param nomUserCont nombre usuario contrato
     * @param req   request
     * @param res   response
     * @return resultado resutado operacion
     * @throws ServletException Excepcion Generica
     * @throws IOException Excepcion Generica
     */
    public ArrayList ejecutaTransferenciaMicrositio(String clave_perfil, String usuario, String contrato, ArrayList nomUserCont,
            HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException{
        final String LOG_METODO = "PagoMicrositio - ejecutaTransferenciaMicrositio - ";
        //public int ejecutaTransferenciaMicrositio(String clave_perfil, String usuario, String contrato, HttpServletRequest req,
        //HttpServletResponse res, String numUsuario)
        //throws ServletException, IOException{
        EIGlobal.mensajePorTrace(LOG_METODO + "Entrando",EIGlobal.NivelLog.INFO);
        ArrayList <String> resultado = new ArrayList <String>();
        String msgErrorMicrositio3="";
        String codErrorMicrositio3="";
        String titularCuentaCargo = "";
        String fecha = "";
        String importe = "";
        String concepto = "";
        String servicio_id="";
        String cuentaCargo = "";
        //String numUsuario  = "";
        String convenio = "";
//        String numContrato = "";
        String referencia = "";
        String empresa = "";
       // String nomUsuario  = "";
       // String nomContrato = "";
        boolean conToken = false;
        String cuentaAbono = "";
        String url = "";
        String camposTransferencia[]  = null;
        String trama_salida="";
        /**
         * Variable para folio de Referencia
         */
        String folioOperacion = "0";
        /**
         * Codigo de error Tuxedo
         */
        String cod_error="";

        String trama_entrada="";
        String sreferencia="";
        String saldoCtaOrigen="";
       // String clase="";
        String pipe="|";
        int  tokens = 13;
        int salida = 0;

        HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute(SESSION);
        EIGlobal.mensajePorTrace("PagoMicrositio::ejecutaTransferenciaMicrositio::el contenido del campo URL es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("PagoMicrositio::ejecutaTransferenciaMicrositio::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)req.getParameter(URL)), EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace("PagoMicrositio::ejecutaTransferenciaMicrositio::el contenido del campo URL despues es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);
        String hora = ObtenHora();
        if(!conToken) {
            if(req.getParameter("token") != null) {
                try {
                    cuentaCargo = java.net.URLDecoder.decode((String) req.getParameter("cuentaCargo"));
                    titularCuentaCargo = java.net.URLDecoder.decode(new String(req.getParameter("titularCuentaCargo").getBytes("ISO-8859-1"),"UTF-8"));
                    cuentaAbono = java.net.URLDecoder.decode((String) req.getParameter("cuentaAbono"));
                    fecha = java.net.URLDecoder.decode((String) req.getParameter("fecha"));
                    empresa = java.net.URLDecoder.decode((String) req.getParameter("empresa"));
                    convenio = java.net.URLDecoder.decode((String) req.getParameter(CONVENIO));
                    importe = java.net.URLDecoder.decode((String) req.getParameter(IMPORTE));
                    referencia = java.net.URLDecoder.decode((String) req.getParameter(REFERENCIA));
                    url = java.net.URLDecoder.decode((String) req.getParameter(URL));
                    concepto = java.net.URLDecoder.decode((String)req.getParameter(CONCEPTO));
                    servicio_id = java.net.URLDecoder.decode((String)req.getParameter(SERVICIO_ID));
                }
                catch(Exception e) {
                    System.out.println("Error en parsear datos...");
                }
            }
            else {
                cuentaCargo = (String) req.getParameter("cuentaCargo");
                titularCuentaCargo = Util.HtmlEncode((String)req.getParameter("titularCuentaCargo"));
                cuentaAbono = (String) req.getParameter("cuentaAbono");
                fecha = (String) req.getParameter("fecha");
                empresa = Util.HtmlEncode((String)req.getParameter("empresa"));
                convenio = (String) req.getParameter(CONVENIO);
                concepto = Util.HtmlEncode((String)req.getParameter(CONCEPTO));
                servicio_id = Util.HtmlEncode((String)req.getParameter(SERVICIO_ID));
                importe = (String) req.getParameter(IMPORTE);
                referencia = Util.HtmlEncode((String)req.getParameter(REFERENCIA));
                url = req.getParameter(URL);
            }
            boolean valError = false;
            /**
             * Correccion vulnerabilidades Deloitte
             * Stefanini Nov 2013
             */
            if(null!=cuentaAbono && Util.validaCCX(cuentaAbono, 1)){
                EIGlobal.mensajePorTrace("el contenido del campo cuentaAbono es correcto->" + cuentaAbono, EIGlobal.NivelLog.INFO);
            } else {
                codErrorMicrositio3 = "1008";
                EIGlobal.mensajePorTrace("El campo cuenta abono no acepta caracteres especiales por favor introduzca solo n�meros->" + cuentaAbono, EIGlobal.NivelLog.INFO);
                valError = true;
            }
            if(null!= fecha && Util.validaCCX(fecha, 3)){
                EIGlobal.mensajePorTrace("el contenido del campo fecha es correcto->" + fecha, EIGlobal.NivelLog.INFO);
            } else {
                codErrorMicrositio3 = "1007";
                EIGlobal.mensajePorTrace("El campo fecha no acepta caracteres especiales por favor introduzca solo n�meros y 2 diagonales->" + fecha, EIGlobal.NivelLog.INFO);
                valError = true;
            }

            if(valError) {
                String usrCSS = "";
                EIGlobal.mensajePorTrace("CuentaCargoMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
                if(null!=req.getHeader("IV-USER")){
                    usrCSS = req.getHeader("IV-USER");
                }
                else if(null!=session.getUserID()){
                    usrCSS = session.getUserID();
                }
                EIGlobal.mensajePorTrace("CuentaCargoMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);

                sess.setAttribute("usrCSS", usrCSS);
                req.getSession().setAttribute("inyeccionURL","true");
                req.setAttribute("inyeccion", true);
                msgErrorMicrositio3="<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.";
                resultado.add("-1");
                resultado.add(codErrorMicrositio3);
                resultado.add(msgErrorMicrositio3);
                resultado.add("true");
                return resultado;
            }
            /**
             * fin Correccion vulnerabilidades Deloitte
             */

        } //conToken=false

        //String tipo_operacion="PGSM";
        /**
         * Se modifica para la nueva clave de operaci�n
         */

        String tipo_cta_origen = "P";
        String tipo_cta_destino = "NR";
        //String tipo_cta_destino = "P";
        String estatus ="";

        System.out.println("cuentaCargo ....." + cuentaCargo);
        System.out.println("cuentaAbono ....." + cuentaAbono);
        System.out.println("importe ....." + importe);
        System.out.println("referencia  ....." + referencia);
        System.out.println("fecha   ....." + fecha);
        System.out.println("url     ....." + url);
        System.out.println("concepto    ....." + concepto);
        System.out.println("servicio_id ....." + servicio_id);

        EIGlobal.mensajePorTrace(LOG_METODO + "referencia "+sreferencia,EIGlobal.NivelLog.INFO);
        trama_entrada=  "1EWEB|"+usuario+"|"
                        + this.CLAVE_MICROSITIO +"|"
                        +contrato+"|"
                        +usuario+"|"
                        +clave_perfil+"|"
                        +cuentaCargo+"|"
                        +tipo_cta_origen+"|"
                        +cuentaAbono+"|"
                        +tipo_cta_destino+"|"
                        +importe+"|"
                        +concepto + "@" + convenio + "@" + referencia + "| |";
                        //+referencia+"|";   //*****Cambio para comprobante fiscal

        ServicioTux tuxGlobal = new ServicioTux();
        Hashtable hs = null;
        EIGlobal.mensajePorTrace(LOG_METODO + "WEB_RED-PMIC>>"+":"+trama_entrada,EIGlobal.NivelLog.INFO);
        try{

            String fechaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa");
            String horaSist = EIGlobal.formatoFecha(new GregorianCalendar(), "th:tm:ts");
            WSLinCapService wsLCService = new WSLinCapService();
            String respuesta = wsLCService.validaLineaCaptura(req,referencia, importe, "001", "0001", fechaSist, horaSist, "", cuentaAbono, "", "", cuentaCargo, this.CLAVE_MICROSITIO, convenio);
            if("0000".equals(respuesta.substring(4,8))){

                //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                hs = tuxGlobal.web_red(trama_entrada);  // MODIFICAR
                trama_salida = (String) hs.get("BUFFER");
                cod_error = (String) hs.get("COD_ERROR");
            } else {
                cod_error = respuesta.substring(0,8);
                trama_salida = respuesta;
            }
            EIGlobal.mensajePorTrace(LOG_METODO + "BUFFER: ["+ trama_salida + "]",EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(LOG_METODO + "COD_ERROR: [" + cod_error + "]",EIGlobal.NivelLog.INFO);
        }catch( java.rmi.RemoteException re ) {
            re.printStackTrace();
            salida=1;
            resultado.add("1");
            //System.out.println("Error al ejecutar la aplicacion Tuxedo");
        } catch (Exception e) {
            e.printStackTrace();
            salida=1;
            resultado.add("1");
        }
        if(trama_salida==null || trama_salida.equals("null"))
            trama_salida="";

        // coderror="OK1232343243454353454353123345345456456|4534543534534|545645612|";
        req.setAttribute(MSG_RST_OPERACION, "");
        if(!trama_salida.equals("")){
            EIGlobal.mensajePorTrace(LOG_METODO + "Trama salida: "+trama_salida,EIGlobal.NivelLog.INFO);
            req.setAttribute("folioOper", folioOperacion);
            req.setAttribute("referenciaMicrositio","");
            req.setAttribute("saldoCtaOrigen","");
            req.setAttribute("saldoDespues","");
            if (cod_error != null && !cod_error.startsWith("MANC")) {
                EIGlobal.mensajePorTrace(LOG_METODO + "Operacion sin Mancomunidad o Firma A",EIGlobal.NivelLog.INFO);
                camposTransferencia=desentramaDatosTransferencia(trama_salida, tokens, pipe);
                EIGlobal.mensajePorTrace(LOG_METODO + "camposTransferencia[0]: [" +
                        camposTransferencia[0] +"]",EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace(LOG_METODO + "camposTransferencia[1]: [" +
                        camposTransferencia[1] +"]",EIGlobal.NivelLog.DEBUG);

                if(camposTransferencia[0].equals("OK")){
                    EIGlobal.mensajePorTrace(LOG_METODO + "Operacion Aceptada",EIGlobal.NivelLog.INFO);
                    estatus = "Aceptada";
                    sreferencia = camposTransferencia[1];
                    saldoCtaOrigen = camposTransferencia[2];
                    folioOperacion = camposTransferencia[1];
                    req.setAttribute("folioOper",folioOperacion);
                    req.setAttribute("saldoDespues",camposTransferencia[2]);
                    if (resultado. isEmpty()){
                        resultado.add("0");
                    } else {
                        resultado.clear();
                        resultado.add("0");
                    }
                } else {
                    /**
                     * Pasa validacion de mancomunidad pero se rechada por otras validaciones
                     */
                    EIGlobal.mensajePorTrace(LOG_METODO + "Pasa validacion de mancomunidad pero se rechada por otras validaciones",EIGlobal.NivelLog.INFO);
                    estatus = "No Ejecutada";
                    salida=0; // error
                    //CSA - RFC100001007747 - Correcion defecto micrositio
                    if (resultado.isEmpty()){
                        resultado.add("1");
                    }else {
                        resultado.clear();
                        resultado.add("1");
                    }
                    req.setAttribute(REFERENCIA,referencia);
                    req.setAttribute(IMPORTE,importe);
                    req.setAttribute(URL, url);
                    req.setAttribute(CONCEPTO,concepto);
                    req.setAttribute(SERVICIO_ID,servicio_id);

                    if ("WPMI".equals(cod_error.substring(0,4)))
                    {
                        req.setAttribute(MSG_RST_OPERACION, trama_salida.substring(16));
                        resultado.clear();
                        resultado.add("0");
                    } else
                    if(!camposTransferencia[0].trim().equals("") && camposTransferencia[0] != null &&
                       !camposTransferencia[1].trim().equals("") && camposTransferencia[1] != null){
                        codErrorMicrositio3 = camposTransferencia[0].trim();
                        msgErrorMicrositio3 = camposTransferencia[1].trim();
                        resultado.add(codErrorMicrositio3);
                        resultado.add(msgErrorMicrositio3);
                    }else{
                        codErrorMicrositio3 = "0099";
                        msgErrorMicrositio3 = "Ocurrio un error no esperado al ejecutar la transferencia.";
                        //CSA - RFC100001007747 - Correcion defecto micrositio
                        if(trama_salida.length() >= 16){
                            resultado.add(cod_error);
                            resultado.add(trama_salida.substring(16));
                        }else {
                        resultado.add(codErrorMicrositio3);
                        resultado.add(msgErrorMicrositio3);
                    }
                    }
                    //Pintar error
                    if (trama_salida != null && trama_salida.length() > 16) {
                        req.setAttribute(MSG_RST_OPERACION, trama_salida.substring(16));
                    }
                    EIGlobal.mensajePorTrace(LOG_METODO + "Pasa validacion de mancomunidad pero se rechaza por otras validaciones ["
                            + msgErrorMicrositio3 +"]",EIGlobal.NivelLog.INFO);

                }
            } else if (cod_error != null && "MANC0000".equals(cod_error) &&
                    trama_salida.startsWith("OK")) {
                EIGlobal.mensajePorTrace(LOG_METODO + "Operacion Mancomunada",EIGlobal.NivelLog.INFO);
                estatus = "Mancomunada";
            } else {
                /**
                 * No pasa la validacion de Mancomunidad por firmas, montos, etc.
                 */
                EIGlobal.mensajePorTrace(LOG_METODO + "No pasa la validacion de Mancomunidad por firmas, montos, etc.",EIGlobal.NivelLog.INFO);
                estatus = "Rechazada";
                //Pintar error
                if (trama_salida != null && trama_salida.length() > 16) {
                    req.setAttribute(MSG_RST_OPERACION, trama_salida.substring(16));
                }

            }
            EIGlobal.mensajePorTrace(LOG_METODO + "estatus: [" + estatus + "]",EIGlobal.NivelLog.DEBUG);
                req.setAttribute("numUsuario",nomUserCont.get(5));
                req.setAttribute("nomUsuario",nomUserCont.get(6));
                req.setAttribute("numContrato",nomUserCont.get(7));
                req.setAttribute("nomContrato",nomUserCont.get(8));
                req.setAttribute("cuentaCargo",cuentaCargo);
                req.setAttribute("cuentaAbono",cuentaAbono);
                req.setAttribute("titularCuentaCargo",titularCuentaCargo);
                req.setAttribute(IMPORTE,importe);
                req.setAttribute(REFERENCIA,referencia);
                req.setAttribute("fecha",fecha);
                req.setAttribute("hora",hora);
                //req.setAttribute("folioOper",camposTransferencia[1]);
                //req.setAttribute("saldoDespues",camposTransferencia[2]);
                req.setAttribute("estatus",estatus);
                req.setAttribute("empresa",empresa);
                req.setAttribute(CONVENIO,convenio);
                req.setAttribute(URL,url);
                req.setAttribute(CONCEPTO,concepto);
                req.setAttribute(SERVICIO_ID,servicio_id);

                if ("Mancomunada".equals(estatus)){
                    folioOperacion = trama_salida.substring(8,16);
                    req.setAttribute("folioOper", folioOperacion);
                    salida=0; // error
                    if (resultado.isEmpty()){
                        resultado.add("0");
                    } else {
                        resultado.clear();
                        resultado.add("0");
                    }
                    //CSA - RFC100001007747 - Correcion defecto micrositio
                } else if ("Rechazada".equals(estatus) /*|| "".equals(camposTransferencia[1]) || "".equals(camposTransferencia[2])*/){
                    salida = 0; // error
                    if (resultado.isEmpty()){
                        resultado.add("0");
                    } else {
                        resultado.clear();
                        resultado.add("0");
                    }
                    EIGlobal.mensajePorTrace(LOG_METODO + "Los datos de la transferencia vienen vacios",EIGlobal.NivelLog.ERROR );
                    //CSA - RFC100001007747 - Correcion defecto micrositio
                }else if ("Aceptada".equals(estatus))   {
                    req.setAttribute("referenciaMicrositio",camposTransferencia[1]);
                    req.setAttribute("saldoCtaOrigen",camposTransferencia[2]);
                    salida=0; // OK
                    if (resultado.isEmpty()){
                        resultado.add("0");
                    } else {
                        resultado.clear();
                        resultado.add("0");
                    }
                    //resultado.add("0");
                }
        } //!trama_salida.equals("")

        /**
         * Pistas de Auditoria MICE04 - Pago Micrositio
         * Bitacora de Operaciones CPMS
         */

        if ("0".equals((String)resultado.get(0)) && "ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
            EIGlobal.mensajePorTrace(LOG_METODO + "Inicia grabado de Bitacoras",EIGlobal.NivelLog.DEBUG);
            try{
                BitaHelper bh = new BitaHelperImpl(req, session, sess);

                // Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
                BitaTransacBean bt = new BitaTransacBean();
                bt = (BitaTransacBean)bh.llenarBean(bt);

                //Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
                bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
                EIGlobal.mensajePorTrace(LOG_METODO + "folioOperacion: [" + folioOperacion + "]",EIGlobal.NivelLog.DEBUG);
                if (!"".equals(folioOperacion.trim())) {
                    bt.setReferencia(Long.valueOf(folioOperacion));
                }
                bt.setEstatus("A");
                bt.setContrato(session.getContractNumber());
                bt.setUsr(session.getUserID8());
                if(importe!=null ){
                    try{
                    bt.setImporte(Double.valueOf(importe));
                    }catch (NumberFormatException e) {
                        bt.setImporte(0.0);
                    }
                }
                if(cuentaCargo!=null){
                    bt.setCctaOrig(cuentaCargo);
                }
                if(cuentaAbono!=null){
                    bt.setCctaDest(cuentaAbono);
                }
                bt.setFechaAplicacion(new Date());
                bt.setServTransTux(this.CLAVE_MICROSITIO);

                // Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
                BitaTCTBean beanTCT = new BitaTCTBean ();
                beanTCT = bh.llenarBeanTCT(beanTCT);

                //Datos Comunes
                bt.setNumBit(BitaConstants.ME_PAGO);
                bt.setIdErr(cod_error);
                beanTCT.setCodError(this.COD_EXITO_MICROSITIO);

                if (session.getContractNumber() != null) {
                    bt.setContrato(session.getContractNumber().trim());
                    beanTCT.setNumCuenta(session.getContractNumber().trim());
                }

                if (session.getUserID8() != null) {
                    bt.setCodCliente(session.getUserID8().trim());
                    beanTCT.setUsuario(session.getUserID8().trim());
                    beanTCT.setOperador(session.getUserID8().trim());
                }

                if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
                        && ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
                                .equals(BitaConstants.VALIDA)) {
                        bt.setIdToken(session.getToken().getSerialNumber());
                }

                Integer ref_tct  = 0 ;
                String codError = null;
                hs = tuxGlobal.sreferencia("901");
                codError = hs.get("COD_ERROR").toString();
                ref_tct = (Integer) hs.get("REFERENCIA");
                if(codError.endsWith("0000") && ref_tct!=null){
                    beanTCT.setReferencia(ref_tct);
                }

                //Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
                beanTCT.setTipoOperacion(BitaConstants.CTK_PAGO_MICROSITIO);

                //Inserto en Bitacoras
                EIGlobal.mensajePorTrace(LOG_METODO + "bitacorizando: [" + BitaConstants.ME_PAGO + ", " +
                        BitaConstants.ME_OPERACION_MICROSITIO + "]", EIGlobal.NivelLog.INFO);
                BitaHandler.getInstance().insertBitaTransac(bt);
                BitaHandler.getInstance().insertBitaTCT(beanTCT);

            }catch (SQLException e) {
                EIGlobal.mensajePorTrace(LOG_METODO + "SQLException - [" + e + "]", EIGlobal.NivelLog.INFO);
            }
            catch (Exception e) {
                EIGlobal.mensajePorTrace(LOG_METODO + "Exception [" + e + "]", EIGlobal.NivelLog.INFO);
            }
            /**
             * Pistas de Auditoria MICE04 - Pago Micrositio
             * Bitacora de Operaciones CPMS
             */

            /**
             * Env�a notificaciones de la operaci�n
             */
            EmailSender emailSender=new EmailSender();
            EmailDetails emailDetails = new EmailDetails();

            if(("TRAN0000".equals(cod_error) || "MANC0000".equals(cod_error)) && emailSender.enviaNotificacion(cod_error)){
                    emailDetails.setEstatusActual(cod_error);
                    emailDetails.setImpTotal(ValidaOTP.formatoNumero(importe) );
                    emailDetails.setNumeroContrato(session.getContractNumber());
                    emailDetails.setRazonSocial(session.getNombreContrato());
                    emailDetails.setNumRef(folioOperacion);
                    emailDetails.setTipoTransferenciaUni(" Pago Micrositio ");
                    emailDetails.setCuentaDestino(cuentaAbono);
                    emailDetails.setNumCuentaCargo(cuentaCargo);
                    emailDetails.setConvenio(convenio);
                    if (empresa != null){
                        emailDetails.setNombreConvenio(empresa.trim());
                    }
                    emailSender.sendNotificacion(req,IEnlace.NOT_PAGO_MICROSITIO, emailDetails);
            }
        }
        EIGlobal.mensajePorTrace(LOG_METODO + "resultado: [" +  resultado.size() + "]",EIGlobal.NivelLog.DEBUG);
        return resultado;
    }


    /**
     * Desentrama los datos que regresa por trama el TUBO al ejecutar la PGSM
     * @param trama trama
     * @param tokens tokens
     * @param separador separador
     * @return aTransferencia desentramado
     */
    private String [] desentramaDatosTransferencia(String trama, int tokens, String separador){
        String [] aTransferencia = new String [tokens];
        int indice = trama.indexOf(separador); // String el pipe, o ;
        EIGlobal.mensajePorTrace("PGSM_Micrositio - desentramaDatosTransferencia(): Formateando arreglo de datos.",EIGlobal.NivelLog.INFO);
        for(int i=0; i<tokens;i++){
            if(indice>0)
            aTransferencia [i] = trama.substring(0,indice);
            if(aTransferencia[i] == null)
                aTransferencia [i] = "    ";
            trama = trama.substring(indice+1);
            indice = trama.indexOf(separador);
        }
        return aTransferencia;
    }

    /**
     * Valida la Sesion micrositio
     * @param request request
     * @param response response
     * @return resultado sesion valida
     * @throws ServletException Excepcion generica
     * @throws java.io.IOException Excepcion generica
     */
    public String ValidaSesionMicrositio( HttpServletRequest request, HttpServletResponse response )
    throws ServletException, java.io.IOException{
        final String LOG_METODO = "PagoMicrositio - ValidaSesionMicrositio - ";
        String resultado = "";
        int resDup = 0;
         HttpSession sess;
         String idSession = null;
         sess = request.getSession (false);
         mx.altec.enlace.bo.BaseResource session = null;
         EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
         if(sess == null){
             resultado = "KO";
             EIGlobal.mensajePorTrace(LOG_METODO + "sesion nula", EIGlobal.NivelLog.DEBUG);
         }else{
             idSession = sess.getId();
             session = (BaseResource)request.getSession().getAttribute(SESSION);
             if(session == null || session.getUserID8 () == null || session.getUserID8 ().equals ("")){
                resultado = "KO";
                EIGlobal.mensajePorTrace(LOG_METODO + "Datos vacios" , EIGlobal.NivelLog.DEBUG);
             }else{
                 session.setUltAcceso(Calendar.getInstance().getTime().getTime());
                 if(session.getTiempoActivo() > (Global.EXP_SESION * 60000)) {
                     resDup = cierraDuplicidad(session.getUserID8());
                     session.setSeguir(false);
                     sess.setAttribute(SESSION, session);
                     sess.removeAttribute(SESSION);
                     sess.invalidate();
                     return "KO";
                 }
                 if(session.getValidarSesion() && (!validaSesionDB(session.getUserID8() , idSession))){
                     resDup = cierraDuplicidad(session.getUserID8());
                     session.setSeguir(false);
                     sess.removeAttribute(SESSION);
                     sess.invalidate();
                    return "KO";
                 }
                int statusToken = session.getToken().getStatus();
                EIGlobal.mensajePorTrace(LOG_METODO + "Status Token: " + statusToken , EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace(LOG_METODO + "Validar Token: " + session.getValidarToken() , EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace(LOG_METODO + "Token Validado2: " + session.getTokenValidado() , EIGlobal.NivelLog.DEBUG);
                if(session.getValidarToken() &&
                   !session.getTokenValidado()) {
                   switch(statusToken) {
                        case 0:     //activaci�n
                                    response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
                                    request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su token ya le fue entregado pero no est� activo. Por favor f�rmese en Enlace por Internet para activarlo e intente nuevamente.\", 1);");
                                    return "act";
                        case 1:     //validaci�n
                                    request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
                                    guardaParametrosEnSession(request, session.getContractNumber());
                                    response.sendRedirect("/Enlace" + IEnlace.TOKEN_MICROSITIO);
                                    return "token";
                        case 2:     //bloqueado
                                    resDup = cierraDuplicidad(session.getUserID8());
                                    request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 1);");
                                    response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
                                    return "bloq";
                        default:
                                    request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token es en un estatus que no le permite operar en Enlace Internet.\", 1);");
                                    response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
                                    return "errorToken";
                   }
                }
                resultado = "OK";
             }
         }
         if(session != null){
             sess.setAttribute (SESSION, session);
                if(session.getValidarSesion()) {
                    actualizaSesionDB(session.getUserID8());
                 }
         }
         EIGlobal.mensajePorTrace(LOG_METODO + "Fin datos OK - resultado: ["+ resultado  + "], [" + resDup + "]", EIGlobal.NivelLog.DEBUG);
         return resultado;
    }

    /**
     * Guarda los parametros de sesion
     * @param request request
     * @param numContrato numero de contrato
     */
   private void guardaParametrosEnSession (HttpServletRequest request, String numContrato){

          EIGlobal.mensajePorTrace("-333333aaaaa---------Interrumple el flujo--",EIGlobal.NivelLog.INFO);
          String template= request.getServletPath();
          EIGlobal.mensajePorTrace("5-****--------------->template**********--->"+template,EIGlobal.NivelLog.INFO);
          request.getSession().setAttribute("plantilla",template);

        HttpSession session = request.getSession(false);
        if(session != null){

            Map tmp = new HashMap();
            Enumeration enumer = request.getParameterNames();
  EIGlobal.mensajePorTrace("6665555-****--------------->enumer**********--->"+request.getParameter( "valida" ),EIGlobal.NivelLog.INFO);

            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
               EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name),EIGlobal.NivelLog.INFO);

                tmp.put(name,request.getParameter(name));
            }

            EIGlobal.mensajePorTrace("lstContratos----valor IOV -------"+numContrato,EIGlobal.NivelLog.INFO);
            tmp.put("lstContratos", numContrato);

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
            }
            session.setAttribute("atributos",tmp);
        }

    }

   /**
    * Consulta la transacci�n B750
    * @param usuario usuario
    * @param contrato contrato
    * @param req request
    * @param res response
    * @param ctaAbonoRec cuenta abono
    * @return salida respuesta
    * @throws ServletException Excepcion Generica
    * @throws IOException Excepcion Generica
    */
   public int consultaB750(String usuario, String contrato,HttpServletRequest req, HttpServletResponse res, String ctaAbonoRec)
   throws ServletException, IOException{

       int salida           = 0; // por default OK

       HttpSession sess = req.getSession();
       BaseResource session = (BaseResource) sess.getAttribute(SESSION);
       String camposEmpresa[]   = null;
       String trama_salida = "";
       String trama_entrada = "";
       String tipo_operacion = "CDEM";  // prefijo del servicio ( consulta datos empresa Pago en l�nea)
       String cabecera = "1EWEB"; // medio de entrega ( regresa trama)

       String convenio = req.getParameter(CONVENIO);
     //  String referencia = req.getParameter( REFERENCIA );
       //String importe = req.getParameter( IMPORTE );
       //String url = req.getParameter( URL );
       //String concepto = (String)req.getParameter(CONCEPTO);
      // String servicio_id = (String)req.getParameter(SERVICIO_ID);

       String perfil = "";
       String pipe = "|";
       int tokens = 13;
       //System.out.println("facultad_consulta        ------->  "+facultad_consulta);

       perfil = session.getUserProfile();
       //boolean errorFile = false;

       if (!convenio.trim().equals("")){
           try{
               EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.INFO);
               trama_entrada    =   cabecera + "|"
               +     usuario + "|"
               + tipo_operacion + "|"
               +    contrato + "|"
               +     usuario + "|"
               +      perfil + "|"
               +        convenio;
               EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
               EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

               ServicioTux tuxedoGlobal = new ServicioTux();
               //tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
               try{
                   Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
                   trama_salida = (String) hs.get("BUFFER");
               }catch( java.rmi.RemoteException re ){
                   re.printStackTrace();
               }catch(Exception e) {
                   e.printStackTrace();
               }
               if(trama_salida==null || trama_salida.equals("null"))
                   trama_salida="";
               EIGlobal.mensajePorTrace("ConsultaDatosEmpresa - iniciaConsultaDatosEmpresa Trama salida: "+trama_salida, EIGlobal.NivelLog.DEBUG);
               camposEmpresa=desentramaDatosEmpresa(trama_salida, tokens, pipe);
               if(camposEmpresa[0].equals("TUBO0000")){
                   if (camposEmpresa[4].equals("") || camposEmpresa[12].equals("")){
                       salida = 1; // error
                       EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Los datos de la empresa vienen vacios >>"+camposEmpresa[0]+"<<", EIGlobal.NivelLog.ERROR);
                   }else{
                       if (camposEmpresa[4].trim().length() > 11){
                           String ctaAbonofinal = camposEmpresa[4].substring(9,20);
                           if(ctaAbonofinal.trim().equalsIgnoreCase(ctaAbonoRec.trim())){
                               salida=0; // OK
                           }else{
                               salida=1; // error
                           }
                       }
                   }
               }else{
                   salida=1; // error
               }
           }catch(Exception e){
               salida = 1; // error
               EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Excepci�n en consultaB750() >>"+"<<", EIGlobal.NivelLog.ERROR);
               e.printStackTrace();
           }
       }else{
           salida = 1;
           EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  El contrato viene vacio ...&", EIGlobal.NivelLog.INFO);
       }
       return salida;
   }

   /**
    * Desentrama datos de la empresa
    * @param trama trama entrada
    * @param tokens tokens
    * @param separador separadores
    * @return aEmpresa datos empresa
    */
   private String [] desentramaDatosEmpresa(String trama, int tokens, String separador) //Desentrama los datos que regresa por trama el TUBO al ejecutar la CDEM
   {
       String [] aEmpresa = new String [tokens];
       int indice = trama.indexOf(separador); // String el pipe, o ;
       EIGlobal.mensajePorTrace("B750_Micrositio - desentramaDatosEmpresa(): Formateando arreglo de datos.", EIGlobal.NivelLog.INFO);
       for(int i=0; i<tokens;i++){
           if(indice>0)
               aEmpresa [i] = trama.substring(0,indice);
           if(aEmpresa[i] == null)
               aEmpresa [i] = "   ";
           trama = trama.substring(indice+1);
           indice = trama.indexOf(separador);
       }
       return aEmpresa;
   }
}