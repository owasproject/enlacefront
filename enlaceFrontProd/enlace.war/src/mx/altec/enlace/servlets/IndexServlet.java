
package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class IndexServlet extends BaseServlet
{
	public void init (ServletConfig config)
		throws ServletException
	{
		super.init (config);
		 // <vswf RRG cambio nombre de la variable enum a enum1 05122008>
		java.util.Enumeration enum1 = config.getInitParameterNames ();

		System.out.println ( enum1.hasMoreElements ());

		while (enum1.hasMoreElements ()) {
			System.out.println ((String) enum1.nextElement ());
		}
		enum1 = config.getServletContext ().getAttributeNames ();
		while (enum1.hasMoreElements ()) {
			System.out.println ((String) enum1.nextElement ());
		}
		enum1 = config.getServletContext ().getInitParameterNames ();
		while (enum1.hasMoreElements ()) {
			System.out.println ((String) enum1.nextElement ());
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("SAM DOGET", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("SAM USUARIO->" + (String)req.getHeader("IV-USER"), EIGlobal.NivelLog.ERROR);
		defaultAction(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace("SAM DOPOST", EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("SAM USUARIO->" + (String)req.getHeader("IV-USER"), EIGlobal.NivelLog.ERROR);
		defaultAction(req, res);
	}

	public void displayMessage(HttpServletRequest req, HttpServletResponse res, String messageText)
		throws ServletException, IOException
	{
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println(messageText);
	}

	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		Enumeration enume = req.getHeaderNames();
		while(enume.hasMoreElements()) {
			   String key = (String)enume.nextElement();
			   String value = req.getHeader(key);
			   EIGlobal.mensajePorTrace(key + "=" + value, EIGlobal.NivelLog.ERROR);
		}

		EIGlobal.mensajePorTrace("referer " + req.getParameter("ref"), EIGlobal.NivelLog.DEBUG);
		/*<VC autor ="ESC" fecha = "18/03/2008" descripcion = "validar que venga del redir"> */

		String headerUser=(String)req.getHeader("IV-USER");
		String headerUser8=(String)req.getHeader("IV-USER");
		if (headerUser!=null)
		{

			if(req.getParameter("ref") != null &&  req.getParameter("ref") != "")
			{
				String referer = req.getParameter("ref");
			//Validar si viene de supernet

				String origensnet = (String)req.getSession().getAttribute("origensnet");
				String dirRedir = (String)req.getSession().getAttribute("dirRedir");

				EIGlobal.mensajePorTrace(referer + " ref " + Global.URL_ACCESO_UNICO + ":" + referer.indexOf(Global.URL_ACCESO_UNICO) , EIGlobal.NivelLog.INFO);

				/*if(referer.indexOf(Global.URL_ACCESO_UNICO) <= -1 && !"true".equals(origensnet) && req.getRequestURI().toString().indexOf("/webpro") <= -1)
				{
					if(dirRedir == null)
					{
				  	 redirAcceso(req,res);
				  	return;
				}
				else
				{
					  req.getSession().removeAttribute("dirRedir");
				}
			}*/

			}
		/*</VC>*/


			String plantilla = req.getParameter("plantilla");
			//<VC proyecto="200710001" autor="JGR" fecha="01/08/2007" descripción="SALTO SUPERNET ENLACE">
			//String contrato = req.getParameter("contrato");
			String contrato = (String)req.getSession().getAttribute("contrato");
			//</VC>
			EIGlobal.mensajePorTrace("Plantilla->" + plantilla, EIGlobal.NivelLog.ERROR);
			if (plantilla == null){
				plantilla = "";
			}
			//<VC proyecto="200710001" autor="JGR" fecha="01/08/2007" descripción="SALTO SUPERNET ENLACE">
		    contrato = (contrato==null?"":contrato);
		    System.out.print("Contrato:" + contrato);
		    //</VC>
			headerUser = convierteUsr7a8(headerUser);
			EIGlobal.mensajePorTrace("DE7A8->" + headerUser, EIGlobal.NivelLog.ERROR);
            /*res.sendRedirect("devicePrintEnlace?opcion=0&usuario=" + headerUser + "&usuario8=" + headerUser8 +
					"&origenSAM=1&plantilla="+plantilla+("".equals(contrato)?"":"&contrato="+contrato));*/
			req.setAttribute("opcion","0");
			req.setAttribute("usuario",headerUser);
			req.setAttribute("usuario8",headerUser8);
			req.setAttribute("origenSAM","1");
			req.setAttribute("plantilla",plantilla);
			req.setAttribute("origenLogin","1");
			if("".equals(contrato)){
				req.setAttribute("contrato",contrato);
			}			
			evalTemplate("/jsp/devicePrintEnlace.jsp",req,res);
			EIGlobal.mensajePorTrace("Pantalla SAM", EIGlobal.NivelLog.ERROR);
		}else{
			 RequestDispatcher disp =
				 getServletContext ().getRequestDispatcher (IEnlace.LOGIN);
			 res.setContentType("text/html");
			 disp.include (req,res);
			EIGlobal.mensajePorTrace("Pantalla Enlace", EIGlobal.NivelLog.ERROR);
		}
	}

	public void redirAcceso(HttpServletRequest req, HttpServletResponse res)
		throws IOException{
		 String mensajeMostrar = recuperaMensajeCuadro(req);
		//Validar si ya redirigio una vez al redir para no ciclar el aplicativo.
		req.getSession().setAttribute("dirRedir","true");
		 try{
			 res.getWriter().print("<html>");
			 res.getWriter().print("<head>");



			System.out.println("Redirigiendo A REDIR: ");

			 res.getWriter().print("<script language = \"JavaScript\">" +
				"function logoutRedir(){\nwindow.parent.LOGUEADO = false;\nwindow.parent.location.href = '" + Global.URL_ACCESO_UNICO + "';\n}</script>");

			 res.getWriter().print("</head>");
			 res.getWriter().print("<body >");
			 res.getWriter().print("<script>logoutRedir()</script>");

			 res.getWriter().print("</body>");
			 res.getWriter().print("</html>");
			 res.getWriter().flush();
			 res.getWriter().close();

		 }catch(IOException ioe){
			 EIGlobal.mensajePorTrace("EXCEPTION EN REDIRSuperNet->" + ioe.getMessage() , EIGlobal.NivelLog.INFO);
			 ioe.printStackTrace();
			 throw ioe;
		 }
	}


}