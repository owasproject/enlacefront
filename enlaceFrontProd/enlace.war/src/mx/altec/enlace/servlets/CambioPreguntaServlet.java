package mx.altec.enlace.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.PreguntaDTO;
import mx.altec.enlace.beans.RSABeanAUX;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.RSAFII;
import mx.altec.enlace.utilerias.UtilidadesRSA;
import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;

import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * Servlet implementation class CambioImagenServlet
 */
public class CambioPreguntaServlet extends BaseServlet {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L; 
    /**respuesta**/
	private static final String RESPUESTA = "respuesta";
	
	/**session**/
	private static final String SESSIONS = "session";
	/**fail**/
	private static final String FAIL = "fail";

	 /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cargaDatosPagina(request, response);
	}

	 /** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace("--------->  CambioPreguntaServlet -- doPost", EIGlobal.NivelLog.DEBUG );
		HttpSession sess = request.getSession();

		String valida = request.getParameter("valida");
		//String stringNull = null;
		
		boolean sesionvalida = SesionValida( request, response );
		if(Global.VALIDA_RSA) { 
			if (! sesionvalida ){
				return;
			}
			
			if (valida == null) {
				valida = validaToken(request, response);
			}
	
			if (valida != null && "1".equals(valida)) {
			
				String devicePrint = request.getParameter("devicePrinter");
				request.getSession().setAttribute("valorDeviceP", devicePrint ); 
		
				Map<String, String> mapa = new HashMap<String, String>();
				mapa = ejecutaRSA(request, response);
				boolean validaRSA = Boolean.valueOf(mapa.get("ValidaRSA").toString());
				String respuesta = mapa.get(RESPUESTA);
				
				if(validaRSA) {
				    EIGlobal.mensajePorTrace("--------->  CambioImagenServlet -- respuesta rsa" + respuesta, EIGlobal.NivelLog.DEBUG );
				    if("SUCCESS".equals(respuesta)) {
				    	mandaCorreo(request);
				    	bitacorizaProceso(request, sess);
				    	
				    	request.setAttribute(RESPUESTA, "exito");
				    	
				    } else {
				    	request.setAttribute(RESPUESTA, FAIL);
				    }
				
				} else {
					request.setAttribute(RESPUESTA, FAIL);
				}
			    
				UtilidadesRSA.crearCookie(response, mapa.get("deviceTokenCookie"));
				request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, mapa.get("deviceTokenFSO"));
			
			}	
		} else {
			request.setAttribute(RESPUESTA, FAIL);
		}
		cargaDatosPagina(request, response);
	}
	
	
	/**
	 * Pagina para cargar datos.
	 * @param request : request
	 * @param response : response
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 * @throws BusinessException 
	 */
	private void cargaDatosPagina (HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		request.setAttribute("opcion", request.getParameter("opcion"));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Cambio de Pregunta","Administraci&oacute;n y Control &gt; Mant. de Datos Personales", "RSAPreg01", request));
		
		if(Global.VALIDA_RSA) {
			if(request.getParameter("preguntaID")==null || "".equals(request.getParameter("preguntaID"))) {
				UtilidadesRSA utils = new UtilidadesRSA();
				RSABean rsaBean = new RSABean();
				rsaBean = utils.generaBean(request, "", "", request.getHeader("IV-USER"));
				
				List<PreguntaDTO> listaPreguntas;
				try {				
						listaPreguntas = utils.obtenerPreguntas(rsaBean, request, response);
						request.setAttribute("listaPreguntas", listaPreguntas);										
				} catch (BusinessException e) {
					request.setAttribute(RESPUESTA, FAIL);
				} catch (NullPointerException e) {
					request.setAttribute(RESPUESTA, FAIL);
				}
			}else {
				request.setAttribute(RESPUESTA, "exito");
			}
				
		} else {
			request.setAttribute(RESPUESTA, FAIL);
		}
		evalTemplate("/jsp/paginaCambioPregunta.jsp", request, response);
		
		
	}
	
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @return String
	 * @throws IOException : exception
	 */
	private String validaToken(HttpServletRequest request, HttpServletResponse response) 
		throws IOException{
		EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.CAMBIO_PREGUNTA);
		//CSA-Vulnerabilidad  Token Bloqueado-Inicio
        int estatusToken = obtenerEstatusToken(session.getToken());
		
		if (session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP) && 
				estatusToken == 1 && solVal) {
			EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
			
			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request, "CambioPreguntaServlet");
			ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
		} else if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP) && 
				estatusToken == 2 && solVal){                        
        	cierraSesionTokenBloqueado(session,request,response);
		}
		//CSA-Vulnerabilidad  Token Bloqueado-Fin
		else {
			return  "1";
		}
		return "";
	}
	
	/**
	 * @param request : request
	 */
	private void mandaCorreo(HttpServletRequest request) {
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
		EmailSender sender = new EmailSender();
		EmailDetails details = new EmailDetails();
		details.setNumeroContrato(session.getContractNumber());
		details.setRazonSocial(session.getNombreContrato());
		details.setUsuario(session.getUserID8());
		
		sender.sendNotificacion(request,IEnlace.CAMBIO_PREGUNTA, details);
	}
	
	/**
	 * @param request : request
	 * @param sess : sess
	 */
	private void bitacorizaProceso(HttpServletRequest request, HttpSession sess) {
		
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			HttpSession ses = request.getSession();
			BaseResource session = (BaseResource) ses.getAttribute(SESSIONS);
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			if (request.getParameter(BitaConstants.FLUJO) != null) {
				bh.incrementaFolioFlujo(
					request.getParameter(BitaConstants.FLUJO).toString());
			}else{
				request.getSession().getAttribute((BitaConstants.SESS_ID_FLUJO).toString());
			}
			
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(BitaConstants.EA_CAMBIO_PREGUNTA.substring(0,4));
			bt.setNumBit(BitaConstants.EA_CAMBIO_PREGUNTA);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CambioPreguntaServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CambioPreguntaServlet - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
		}
	}
	
	/**
	 * @param request : request
	 * @param response : request
	 * @return Map<String, String> mapa de resultados
	 */
	private Map<String, String> ejecutaRSA(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, String> mapa = new HashMap<String, String>();
		RSABean rsaBean = new RSABean();
		UtilidadesRSA utils = new UtilidadesRSA();
		ServiciosAAResponse res = new ServiciosAAResponse();		
		boolean validaRSA = true;
		String deviceTokenFSO = "";
	    String deviceTokenCookie = ""; 
		
		rsaBean = utils.generaBean(request, "", "", request.getHeader("IV-USER"));
		
		String pregunta = request.getParameter("preguntaID") != null ? request.getParameter("preguntaID") : "";
		String respuesta = request.getParameter("idRespuesta") != null ? request.getParameter("idRespuesta") : "";
		
		ChallengeQuestion challengeQuestionList = new ChallengeQuestion();
		challengeQuestionList.setActualAnswer(respuesta);
		challengeQuestionList.setQuestionId(pregunta);
		
		ChallengeQuestion[] arrayChallengeQuesitons = {challengeQuestionList};		
		RSAFII rsa = new RSAFII();
		RSABeanAUX aux = new RSABeanAUX();
		aux.setRsaBean(rsaBean);
		aux.setValorMetodo(RSAFII.valorMetodo.SET_PREGUNTA_USER);
		aux.setArrayChallengeQuesitons(arrayChallengeQuesitons);
		
		try {
				res = (ServiciosAAResponse) rsa.ejecutaMetodosRSA7(aux);								
			if(res.getDeviceResult().getDeviceData() != null && res.getDeviceResult().getCallStatus() != null) {
				deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO();
			    deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie();
			    respuesta = res.getDeviceResult().getCallStatus().getStatusCode();
			} else {
				validaRSA = false;
			}
		} catch (ExecutionException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
			validaRSA = false;
		} catch (InterruptedException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
			validaRSA = false;
		}
		
		mapa.put("ValidaRSA", String.valueOf(validaRSA));
		mapa.put(RESPUESTA, respuesta);
		mapa.put("deviceTokenFSO", deviceTokenFSO);
		mapa.put("deviceTokenCookie", deviceTokenCookie);
		
		return mapa;
		
	}

}
