/* Modulo de Enlace Micrositio - Transaccion B750 */
package mx.altec.enlace.servlets;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.*;
import javax.servlet.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;

import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Hashtable;
import java.math.BigDecimal;

/**
 * @author C109834
 *
 */
public class CuentaCargoMicrositio extends BaseServlet{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Codigo de Exito de la operacion Micrositio
	 */
	private static final String COD_EXITO_MICROSITIO = "PMIC0000";
	/** ID servicio pago referenciado */
	private static final String ID_SERVICIO_SAT = "PMRF";
	/** atributo servico_id */
	private static final String SERVICIO_ID = "servicio_id";
	/** atributo url */
	private static final String URL = "url";
	/** atributo CONVENIO */
	private static final String CONVENIO = "convenio";
	/** atributo SESSION */
	private static final String SESSION = "session";

	/** atributo CODERROR */
	private static final String COD_ERROR = "CodError";
	/** atributo MsgError */
	private static final String MSG_ERROR = "MsgError";
	/** atributo HoraError */
	private static final String HORA_ERROR = "HoraError";



	/**
	 * Mensaje de error de sesion existente
	 */
	private static final String MSJ_SESION_EXISTENTE = "Enlace ha detectado que ya existe " +
	"una sesi&oacute;n activa con este c&oacute;digo de cliente, si ha tenido que reiniciar" +
	" su PC, o si por alguna raz&oacute;n ha perdido la conexi&oacute;n con la p&aacute;gina" +
	" del banco cuando su sesi&oacute;n estaba activa, es necesario que espere " +
	Global.EXP_SESION + " minutos y vuelva a intentarlo. Esta es una medida de seguridad " +
	"para proteger su informaci&oacute;n. Para cualquier duda comun&iacute;quese a " +
	"S&uacute;per L&iacute;nea Empresarial en la Ciudad de M&eacute;xico al 51694343 o " +
	"del interior de la Rep&uacute;blica al 018005095000.";

	/** Divisa */
	String tipoDivisa = "";
	/** Modulo */
	String modulo = "";
	/** nombre contrato */
	String nomContrato = "";
	/** Mensaje error */
	String msgErrorMicrositio = "";
	/** Codigo error */
	String codErrorMicrositio = "";

	/**
	 * Implementa doGet del servlet
	 * @param req request
	 * @param res response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * Implementa doPost del servlet
	 * @param req request
	 * @param res response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

/**
 * Metodo default de la clase
 * @param req request
 * @param res response
 * @throws ServletException Excepcion generica
 * @throws IOException Excepcion generica
 */
public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction", EIGlobal.NivelLog.INFO);

		boolean sesionvalida = true;
		BaseResource session = null;

		String convenio = "";
		String importe = "";
		String servicioId = "";

		String referencia = "";
		String url = "";
		String concepto = "";
		String contrato = "";

		String lineaCaptura = "";

		if( ID_SERVICIO_SAT.equals((String)req.getParameter(SERVICIO_ID))){
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio::El tipo de servicio es pago referenciado :" + (String)req.getParameter(SERVICIO_ID), EIGlobal.NivelLog.INFO);
			servicioId = (String)req.getParameter(SERVICIO_ID);
			lineaCaptura = (String)req.getParameter("linea_captura");
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Linea de Captura   = " + lineaCaptura, EIGlobal.NivelLog.INFO);
		} else {
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio::el contenido del campo URL es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)req.getParameter(URL)), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio::el contenido del campo URL despues es:" + req.getParameter(URL), EIGlobal.NivelLog.INFO);
			referencia = Util.HtmlEncode((String)req.getParameter("referencia"));

			url = req.getParameter(URL);
			servicioId = Util.HtmlEncode((String)req.getParameter(SERVICIO_ID));
			concepto = Util.HtmlEncode((String)req.getParameter("concepto"));
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Referencia = "+referencia, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - url    = "+ url, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - concepto    = "+ concepto, EIGlobal.NivelLog.INFO);
		}

		convenio = req.getParameter( CONVENIO );
		importe = req.getParameter( "importe" );
        contrato = req.getParameter( "lstContratos" );

		EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Convenio   = "+convenio, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Importe    = "+importe, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - servicio_id    = "+ servicioId, EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
		if (sess == null) {
			sesionvalida = false;
		} else {
			session = (BaseResource) sess.getAttribute(SESSION);
			if (session == null) {
				sesionvalida = false;
			}
		}
		if (!sesionvalida) {
			msgErrorMicrositio = "Su sesi�n ha expirado.";
			codErrorMicrositio = "0999";
			req.setAttribute ("CodError", codErrorMicrositio);
			req.setAttribute ("MsgError", msgErrorMicrositio);
			req.setAttribute ("HoraError",ObtenHora ());
			evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
			return;
		}

		int result = 1;
		int resultado = 1;
		EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Antes Validar Duplicidad" , EIGlobal.NivelLog.DEBUG);

		if((!session.getTokenValidado() || sess.getAttribute("unicontratoOK") == null )
				&& !validaDupMicrositio(session.getUserID8(), req, session)) {
			req.setAttribute ("CodError", "0999");
			req.setAttribute ("MsgError", MSJ_SESION_EXISTENTE);
			req.setAttribute ("HoraError",ObtenHora ());
			evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
			return;
			//Fin validar duplicidad MIcrositio....
		} else {
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - " +
					"Regresa de validaDuplicidad Usr:[" +session.getUserID8()+ "]", EIGlobal.NivelLog.DEBUG);


			String resSesion = ValidaSesionMicrositio(req, res);
			if("OK".equals(resSesion)){

				EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Antes de llamar al metodo CREAAMBIENTE -----", EIGlobal.NivelLog.INFO);
				nomContrato = session.getNombreContrato ();
				int codigo = creaAmbiente(req,res); // m�todo CreaAmbiente
				if(codigo != 0){
					req.setAttribute ("CodError", codErrorMicrositio);
					req.setAttribute ("MsgError", msgErrorMicrositio);
					req.setAttribute ("HoraError",ObtenHora ());
					int cierraDuplicidad = cierraDuplicidad(session.getUserID8());
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Error al crear ambiente - cierraDuplicidad: [" +
							cierraDuplicidad + "]", EIGlobal.NivelLog.INFO);
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}

				EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - " +
						"Creo ambioente contrato: [" + session.getContractNumber() +
						"]", EIGlobal.NivelLog.DEBUG);


				nomContrato = session.getNombreContrato();
                boolean valError = false;

				/**
				 * Correccion vulnerabilidades Deloitte
				 * Stefanini Nov 2013
				 */
            	if(null!=contrato && Util.validaCCX(contrato, 1)){
					EIGlobal.mensajePorTrace("el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute("CodError", "1010");
					EIGlobal.mensajePorTrace("El campo contrato no acepta caracteres especiales por favor introduzca solo n�meros->" + convenio, EIGlobal.NivelLog.INFO);
					valError = true;
				}

				if(null!=convenio && Util.validaCCX(convenio, 1)){
					EIGlobal.mensajePorTrace("el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute("CodError", "1003");
					EIGlobal.mensajePorTrace("El campo convenio no acepta caracteres especiales por favor introduzca solo n�meros->" + convenio, EIGlobal.NivelLog.INFO);
					valError = true;
				}
				
				if( ID_SERVICIO_SAT.equals((String)req.getParameter(SERVICIO_ID))){
					if(null!=importe && Util.validaCCX(importe, 1)){
						EIGlobal.mensajePorTrace("el contenido del campo importe es correcto->" + importe, EIGlobal.NivelLog.INFO);
					} else {
						req.setAttribute("CodError", "1004");
						EIGlobal.mensajePorTrace("El campo importe no acepta caracteres especiales por favor introduzca n?meros y dos decimales->" + importe, EIGlobal.NivelLog.INFO);
						valError = true;
					}
				}
				else {
				
					if(null!=importe && Util.validaCCX(importe, 4)){
						EIGlobal.mensajePorTrace("el contenido del campo importe es correcto->" + importe, EIGlobal.NivelLog.INFO);
					} else {
						req.setAttribute("CodError", "1004");
						EIGlobal.mensajePorTrace("El campo importe no acepta caracteres especiales por favor introduzca n�meros y dos decimales->" + importe, EIGlobal.NivelLog.INFO);
						valError = true;
					}
				}

				if(valError) {
					String usrCSS = "";
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
					if(null!=req.getHeader("IV-USER")){
						usrCSS = req.getHeader("IV-USER");
					}
					else if(null!=session.getUserID()){
						usrCSS = session.getUserID();
					}
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);

					sess.setAttribute("usrCSS", usrCSS);
					req.getSession().setAttribute("inyeccionURL","true");
					req.setAttribute("inyeccion", true);
					req.setAttribute ("HoraError", ObtenHora());
					req.setAttribute("MsgError", "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}
				/**
				 * Fin correccion vulnerabilidades Deloitte
				 */

				// LLamar a la B750 para obtener lso datos de la empresa
				result = inicio( req, res ); // m�todo principal
				if(result == 0){
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Divisa[" + tipoDivisa + "]\n", EIGlobal.NivelLog.INFO);
					if (tipoDivisa.equals("MXP"))
						modulo = IEnlace.MCargo_transf_pesos;
					else
						modulo = IEnlace.MCargo_transf_dolar;
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Modulo[" + modulo + "]\n", EIGlobal.NivelLog.INFO);


					EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Pistas de Auditoria MICE - Micrositio Ingreso a Enlace", EIGlobal.NivelLog.INFO);

					/**
					 * Pistas de Auditoria MICE - Micrositio Ingreso a Enlace
					 */
					try {
						//TODO BIT CU5041, Inicio de flujo de Cambio de Contrato
						/*VSWF-HGG-I*/
						if ("ON".equals(Global.USAR_BITACORAS.trim())){
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.ME_OPERACION_MICROSITIO));
							BitaTransacBean bt = new BitaTransacBean();
							//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							bt = (BitaTransacBean)bh.llenarBean(bt);
							bt.setNumBit(BitaConstants.ME_INGRESO_TOKEN_ID);
							bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
							bt.setContrato(session.getContractNumber());
							bt.setUsr(session.getUserID8());
							if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
							}
							if(convenio!=null){
								bt.setCctaDest(convenio);
							}
							bt.setFechaAplicacion(new Date());
							bt.setEstatus("A");
							bt.setIdErr(CuentaCargoMicrositio.COD_EXITO_MICROSITIO);
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}
							try {
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
				                                 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace ("ERROR->Ingreso Micrositio->" + e, EIGlobal.NivelLog.ERROR);
							} catch (Exception e) {
				                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace ("ERROR->Ingreso Micrositio->" + e, EIGlobal.NivelLog.ERROR);
							}
						}
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
					/**
					 * Pistas de Auditoria MICE01 - Seleccion de Contrato Micrositio
					 */
					EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Pistas de Auditoria MICE01 - Seleccion de Contrato Micrositio", EIGlobal.NivelLog.INFO);

					if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						EIGlobal.mensajePorTrace( "[--------<---------------------------------------------------------------]", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Inicia grabado de Pistas de Auditoria",EIGlobal.NivelLog.DEBUG);
						try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);

							// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);

							//Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
							bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
							bt.setEstatus("A");
							bt.setContrato(session.getContractNumber());
							bt.setUsr(session.getUserID8());
							if(importe!=null ){
								try{
								bt.setImporte(Double.valueOf(importe));
								}catch (NumberFormatException e) {
									bt.setImporte(0.0);
								}
							}
							if(convenio!=null){
								bt.setCctaDest(convenio);
							}
							bt.setFechaAplicacion(new Date());
							bt.setNumBit(BitaConstants.ME_SEL_CONTRATO);
							bt.setIdErr(CuentaCargoMicrositio.COD_EXITO_MICROSITIO);
							if (session.getContractNumber() != null) {
								bt.setContrato(session.getContractNumber().trim());
							}
							if (session.getUserID8() != null) {
								bt.setCodCliente(session.getUserID8().trim());
							}
							if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
									&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN))
											.equals(BitaConstants.VALIDA)) {
									bt.setIdToken(session.getToken().getSerialNumber());
							}
							//Inserto en Bitacora
							BitaHandler.getInstance().insertBitaTransac(bt);
							EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - bitacorizando: [" + BitaConstants.ME_SEL_CONTRATO + "]", EIGlobal.NivelLog.INFO);

						}catch (SQLException e) {
							EIGlobal.mensajePorTrace("Pago Micrositio - SQLException - Exception [" + e + "]", EIGlobal.NivelLog.INFO);
						}
						catch (Exception e) {
							EIGlobal.mensajePorTrace("Pago Micrositio - Exception - Exception [" + e + "]", EIGlobal.NivelLog.INFO);
						}
						EIGlobal.mensajePorTrace( "[---------<-------------------------------------------------------------]", EIGlobal.NivelLog.DEBUG);
					}
					/**
					 * Fin Registro de Pistas de Auditoria MICE01 - Seleccion de Contrato Micrositio
					 */

					/**
					 * Inicia Bitacora de Operaciones - CIMS Confirmacion de Ingreso Micrositio
					 */
					if ("ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim())){
						EIGlobal.mensajePorTrace( "[-----------------------------------------------------------------------<]", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Inicia grabado de Bitacora de Operaciones",EIGlobal.NivelLog.DEBUG);
						try{
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
							BitaTCTBean beanTCT = new BitaTCTBean ();
							beanTCT = bh.llenarBeanTCT(beanTCT);
							beanTCT.setTipoOperacion(BitaConstants.CTK_INGRESO_MICROSITIO);
							beanTCT.setCodError(CuentaCargoMicrositio.COD_EXITO_MICROSITIO);
							if (session.getContractNumber() != null) {
								beanTCT.setNumCuenta(session.getContractNumber().trim());
							}
							if (session.getUserID8() != null) {
								beanTCT.setUsuario(session.getUserID8().trim());
								beanTCT.setOperador(session.getUserID8().trim());
							}
							Integer refTct  = 0 ;
							String codError = null;
							ServicioTux tuxGlobal = new ServicioTux();
							Map hs = tuxGlobal.sreferencia("901");
							codError = hs.get("COD_ERROR").toString();
							refTct = (Integer) hs.get("REFERENCIA");
							if(codError.endsWith("0000") && refTct!=null){
								beanTCT.setReferencia(refTct);
							}
							EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - bitacorizando: [" + BitaConstants.CTK_INGRESO_MICROSITIO + "]", EIGlobal.NivelLog.INFO);
							BitaHandler.getInstance().insertBitaTCT(beanTCT);
						}catch (SQLException e) {
							EIGlobal.mensajePorTrace("Pago Micrositio - SQLException - " +
									"Exception [" + e + "]", EIGlobal.NivelLog.INFO);
						}
						catch (Exception e) {
							EIGlobal.mensajePorTrace("Pago Micrositio - Exception [" + e + "]", EIGlobal.NivelLog.INFO);
						}

					}
					/**
					 * Fin Bitacora de Operaciones - CIMS Confirmacion de Ingreso Micrositio
					 */








					// Obtener las cuentas asociadas al cliente
					if(llamado_servicioCtasInteg(modulo," "," "," ",session.getUserID8()+".ambci",req)){
						resultado = lectura_cuentas_para_pantalla(req,res,modulo,session.getUserID8()+".ambci");
						if (resultado == 0) {
							evalTemplate(IEnlace.CUENTACARGO_LOGIN_MICROSITIO, req, res);
						}
						else {
							String msgErrorMicrositio = "No existen cuentas de cheques para hacer el pago.";
							String codErrorMicrositio = "0997";
							req.setAttribute ("CodError", codErrorMicrositio);
							req.setAttribute ("MsgError", msgErrorMicrositio);
							req.setAttribute ("HoraError",ObtenHora ());
							evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
							return;
						}
				    }else{
						String msgErrorMicrositio = "No existen cuentas de cheques para hacer el pago.";
						String codErrorMicrositio = "0997";
						req.setAttribute ("CodError", codErrorMicrositio);
						req.setAttribute ("MsgError", msgErrorMicrositio);
						req.setAttribute ("HoraError",ObtenHora ());
						evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
						return;
					}
				}else{
					String msgErrorMicrositio = "Error al obtener los datos de la empresa a pagar, intente mas tarde.";
					String codErrorMicrositio = "0997";
					req.setAttribute ("CodError", codErrorMicrositio);
					req.setAttribute ("MsgError", msgErrorMicrositio);
					req.setAttribute ("HoraError",ObtenHora ());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}
			}else{
				if ("KO".equals(resSesion)) {
					if (session != null) {
						EIGlobal.mensajePorTrace("CuentaCargoMicrositio - defaultAction - Entro KO", EIGlobal.NivelLog.INFO);
						int resDup = cierraDuplicidad(session.getUserID8());
					}
					String msgErrorMicrositio = "Su sesi�n ha expirado.";
					String codErrorMicrositio = "0999";
					req.setAttribute ("CodError", codErrorMicrositio);
					req.setAttribute ("MsgError", msgErrorMicrositio);
					req.setAttribute ("HoraError",ObtenHora ());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}
			}
		}
	}

	//=============================================================================================
	/**
	 * @param req request
	 * @param res response
	 * @return resultado
	 * @throws ServletException Execpcion Generica
	 * @throws IOException Execpcion Generica
	 */
	public int creaAmbiente( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException
	{

		EIGlobal.mensajePorTrace( "#-------------------------------------------------------------------------------#" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - CreaAmbiente Micrositio              -#" , EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "#-------------------------------------------------------------------------------#" , EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute(SESSION);

			String Contrato = req.getParameter( "lstContratos" );
			String usr = session.getUserID8();
			String direccion = req.getParameter( "opcionElegida" );
			String servicio_id = req.getParameter (SERVICIO_ID);
		int i = 0;

			String AmbCod = TraeAmbiente(usr,Contrato,req);
			String[] noPerfil = TraePerfil(req);

			EIGlobal.mensajePorTrace( "#-----------------------------------------------------------------------------------#" , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - Micrositio Variables de ambiente fijadas -#" , EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - Direccion de acceso = " + direccion, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - Usuario = " + usr, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - Contrato = " + Contrato, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - AmbCod = " + AmbCod, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "#- CuentaCargoMicrositio - creaAmbiente - noPerfil = " + noPerfil, EIGlobal.NivelLog.INFO);

	    for( i = 0; i < noPerfil.length; i++) {
				EIGlobal.mensajePorTrace( "#- noPerfil[" + i + "] = " + noPerfil[i]  , EIGlobal.NivelLog.INFO);
	    }
			EIGlobal.mensajePorTrace( "#-------------------------------------------------------------------------------#" , EIGlobal.NivelLog.INFO);

			session.setContractNumber(Contrato);
			session.setUserProfile(noPerfil[1]);

			if(!FijaAmbiente(req)){
				EIGlobal.mensajePorTrace("CuentaCargoMicrositio - creaAmbiente - FijaAmbiente False", EIGlobal.NivelLog.INFO);
				boolLogin = false;
				sess.removeAttribute(SESSION);
				sess.setAttribute(SESSION,session);
				msgErrorMicrositio = "No tiene facultad de operar en Enlace Micrositio Internet <br> con el contrato " + Contrato;
				codErrorMicrositio = "0993";
				return 1;
			}

			if(!verificaFacultad("ACCESINTERNET",req)){
				EIGlobal.mensajePorTrace("CuentaCargoMicrositio - creaAmbiente - Facultad ACCESINTERNET False", EIGlobal.NivelLog.INFO);
				boolLogin = false;
				msgErrorMicrositio = "No tiene facultad de operar en Enlace Micrositio Internet <br> con el contrato " + Contrato;
				codErrorMicrositio = "0993";
				return 1;
			} else {
				EIGlobal.mensajePorTrace("CuentaCargoMicrositio - creaAmbiente - Facultad ACCESINTERNET True", EIGlobal.NivelLog.INFO);
			}


			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - creaAmbiente -  AmbCod = &" +AmbCod+ "&", EIGlobal.NivelLog.INFO);

			if(AmbCod.equals("AMBI0000")){
				/******* Getronics CP M�xico  Inicia modificaci�n por Q14886 *******
				if(session.getFacultad(session.FAC_CARGO_CHEQUES) && session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG))
				*/
				if (!ID_SERVICIO_SAT.equals(servicio_id))
				{
				EIGlobal.mensajePorTrace("Q14884: FAC_CARGO_CHEQUES [" + session.getFacultad(BaseResource.FAC_CARGO_CHEQUES) + "]", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Q14884: FAC_ABOMICRO_PAGCH [" + session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH) + "]", EIGlobal.NivelLog.INFO);
				if(session.getFacultad(BaseResource.FAC_CARGO_CHEQUES) && session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH))
				/******* Getronics CP M�xico  Termina modificaci�n por Q14886 *******/
					return 0;
				else{
					msgErrorMicrositio = "No tiene facultades para realizar la operacion";
					codErrorMicrositio = "0995";
					return 1;
					}
				} else {
					EIGlobal.mensajePorTrace("Q14884: TEIMFCAPPAGM [" + session.getFacultad(BaseResource.FAC_CAP_PAG_PI) + "]", EIGlobal.NivelLog.INFO);
					if(session.getFacultad(BaseResource.FAC_CAP_PAG_PI))
						return 0;
					else{
						msgErrorMicrositio = "Usuario sin facultades para operar Pago de Impuestos Referenciado por Micrositio";
						codErrorMicrositio = "0995";
						return 1;
					}
				}
			}else{
				EIGlobal.mensajePorTrace("***CreaAmbiente.class AmbCod != 'AMBI0000'", EIGlobal.NivelLog.INFO);
				msgErrorMicrositio = "Problemas al crear el ambiente.";
				codErrorMicrositio = AmbCod;
				return 1;
			}
	}

	//=============================================================================================
	/**
	 * Inicio
	 * @param req request
	 * @param res response
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @return int entero
	 */
	public int inicio( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "CuentaCargoMicrositio - inicio - B750()&", EIGlobal.NivelLog.INFO);
		req.setAttribute( "fecha", ObtenFecha(true) );

		String contrato 		= "";
		String usuario			= "";
		String usuario1 		= "";
		String convenio 		= "";
		String contratosAsoc		= "";

		String[] camposEmpresa = new String[4];

		int llamada_servicio = 0;
		boolean [] regreso = new boolean [7];

		/************* Modificacion para la sesion ***************/

		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute (SESSION);

		contratosAsoc = session.getContratos ();

		contrato = session.getContractNumber();
		usuario = session.getUserID8();
		usuario1	= session.getUserID8();
		//perfil = session.getUserProfile();
		convenio	= ( String) req.getParameter( CONVENIO );
		try{
			//String el_ = (String) req.getAttribute("contrato");
			//System.out.println("Contrato ="+el_);
			if ( usuario.length() < 2 ){ usuario = session.getUserID8();}
			if ( convenio==null ){ convenio = ( String) req.getParameter( CONVENIO );}

			llamada_servicio = consultaB750(usuario,contrato, req, res);  // llamada al servicio e inicializaci�n de archivo

			if ( llamada_servicio != 0 ){
				EIGlobal.mensajePorTrace( "CuentaCargoMicrositio - inicio -  &error en el servicio&", EIGlobal.NivelLog.INFO);
				return 1;
			}else{
				EIGlobal.mensajePorTrace( "CuentaCargoMicrositio - inicio -  &servicio OK ...&", EIGlobal.NivelLog.INFO);
				return 0;
			}
		}catch (Exception e) {
			EIGlobal.mensajePorTrace("CuentaCargoMicrositio - inicio - Excepci�n en inicio() >>"+"<<", EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
			return 1; // error
		}
	} // m�todo

	//=============================================================================================
	/**
	 * Consulta B750
	 * @param usuario usuario
	 * @param contrato contrato
	 * @param req request
	 * @param res response
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 * @return int resultado
	 */

	public int consultaB750(String usuario, String contrato,HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		int salida			= 0; // por default OK

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		String camposEmpresa[]	= null;
		String trama_salida = "";
		String trama_entrada = "";
		String referencia ="";
		String url = "";
		String concepto = "";
		String lineaCaptura = "";
		String tipo_operacion = "CDEM";  // prefijo del servicio ( consulta datos empresa Pago en l�nea)
		String cabecera = "1EWEB"; // medio de entrega ( regresa trama)

		String convenio = req.getParameter(CONVENIO);
		String importe = req.getParameter( "importe" );
		String servicioId = (String)req.getParameter(SERVICIO_ID);

		if(ID_SERVICIO_SAT.equals(servicioId))
		{
			lineaCaptura = req.getParameter( "linea_captura" );
		} else {
			referencia = req.getParameter( "referencia" );
			url = req.getParameter( URL );
	        concepto = (String)req.getParameter("concepto");
		}

		String perfil = "";
		String pipe = "|";
		int tokens = 13;
		//System.out.println("facultad_consulta 		  ------->  "+facultad_consulta);

		perfil = session.getUserProfile();
		boolean errorFile = false;

		if (!convenio.trim().equals("")){
			try{
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  &facultad de consulta OK ...&", EIGlobal.NivelLog.INFO);
				trama_entrada	=	cabecera + "|"
								+	 usuario + "|"
								+ tipo_operacion + "|"
								+	contrato + "|"
								+	 usuario + "|"
								+	  perfil + "|"
								+		convenio;
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  trama de entrada >>"+trama_entrada+"<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  ejecutando servicio ...", EIGlobal.NivelLog.DEBUG);

				ServicioTux tuxedoGlobal = new ServicioTux();
				//tuxedoGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				try{
					Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
					trama_salida = (String) hs.get("BUFFER");
				}catch( java.rmi.RemoteException re ){
					re.printStackTrace();
				}catch(Exception e) {
					e.printStackTrace();
				}
				if(trama_salida==null || trama_salida.equals("null"))
					trama_salida="";
				EIGlobal.mensajePorTrace("ConsultaDatosEmpresa - iniciaConsultaDatosEmpresa Trama salida: "+trama_salida, EIGlobal.NivelLog.DEBUG);
				camposEmpresa=desentramaDatosEmpresa(trama_salida, tokens, pipe);
				if(camposEmpresa[0].equals("TUBO0000")){
					if (camposEmpresa[4].equals("") || camposEmpresa[12].equals("")){
						salida = 1; // error
						EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Los datos de la empresa vienen vacios >>"+camposEmpresa[0]+"<<", EIGlobal.NivelLog.ERROR);
					}else{
						req.setAttribute("numContrato",contrato);
						req.setAttribute("nomContrato", nomContrato);
						req.setAttribute("empresa",camposEmpresa[12]);
						if (camposEmpresa[4].trim().length() > 11)
							req.setAttribute("ctaabono", camposEmpresa[4].substring(9,20));

						req.setAttribute(CONVENIO, convenio);
						req.setAttribute("importe", importe);
						req.setAttribute("web_application",Global.WEB_APPLICATION);
						req.setAttribute(SERVICIO_ID, servicioId);

						if(ID_SERVICIO_SAT.equals(servicioId)){
							req.setAttribute("linea_captura", lineaCaptura);
						} else {
							req.setAttribute("referencia", referencia);
							req.setAttribute(URL, url);
	                        req.setAttribute("concepto", concepto);
						}

                        tipoDivisa =  camposEmpresa[5].trim();
						salida=0; // OK
					}
				}else{
					salida=1; // error
				}
			}catch(Exception e){
				salida = 1; // error
				EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  Excepci�n en consultaB750() >>"+"<<", EIGlobal.NivelLog.ERROR);
				e.printStackTrace();
			}
		}else{
			salida = 1;
			EIGlobal.mensajePorTrace( "***ConsultaDatosEmpresa.class  El contrato viene vacio ...&", EIGlobal.NivelLog.INFO);
		}
		return salida;
	}

	private String [] desentramaDatosEmpresa(String trama, int tokens, String separador) //Desentrama los datos que regresa por trama el TUBO al ejecutar la CDEM
	{
		String [] aEmpresa = new String [tokens];
		int indice = trama.indexOf(separador); // String el pipe, o ;
		EIGlobal.mensajePorTrace("B750_Micrositio - desentramaDatosEmpresa(): Formateando arreglo de datos.", EIGlobal.NivelLog.INFO);
		for(int i=0; i<tokens;i++){
			if(indice>0)
			aEmpresa [i] = trama.substring(0,indice);
			if(aEmpresa[i] == null)
				aEmpresa [i] = "	  ";
			trama = trama.substring(indice+1);
			indice = trama.indexOf(separador);
		}
		return aEmpresa;
	}

    private int lectura_cuentas_para_pantalla(HttpServletRequest req, HttpServletResponse res, String modulo, String archivo) throws ServletException, IOException
    {
	EIGlobal.mensajePorTrace("---------------------------------------------", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("***Entrando a  lectura_cuentas_para_pantalla ", EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("---------------------------------------------", EIGlobal.NivelLog.INFO);
	HttpSession sess = req.getSession();
	BaseResource session = (BaseResource) sess.getAttribute(SESSION);
	String resultado="";
	BufferedReader entrada=null;
	String linea = "";
	int separadores =0;
	boolean continua=true;
	int contador=1;
	String clase="";
	String trama="";
	String datos[] =null;
		Hashtable tabla_cuentas = new Hashtable();
		Hashtable htResult;
		Vector cuentas_regreso = new Vector();
		String cabecera_trama = "1EWEB|" + session.getUserID8() + "|SDCT|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";
		String trama_entrada = "";
		String cuenta = "";
		String saldo = "";
		String titular = "";
		String coderror = "";
		String[] aCuenta = new String[3];
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
	try{
	    entrada = new BufferedReader( new FileReader( IEnlace.LOCAL_TMP_DIR + "/" + archivo));
	    while((linea = entrada.readLine() ) != null ){
		separadores=cuentaseparadores(linea);
		continua=true;
		if (continua==true){
		    datos = desentramaC(""+separadores+"@"+linea,'@');
					trama_entrada = cabecera_trama + datos[1] + "|" + "P" + "|";
					EIGlobal.mensajePorTrace("TRAMA ENTRADA SDCT >>" + trama_entrada + "<<", EIGlobal.NivelLog.INFO);
					//Llama a la consulta del saldo de la cuenta.
					htResult = tuxGlobal.web_red(trama_entrada);
					coderror = ( String ) htResult.get( "BUFFER" );
					EIGlobal.mensajePorTrace("TRAMA SALIDA SDCT = <<" +coderror+ ">>", EIGlobal.NivelLog.INFO);
					if (coderror.substring(0,2).equals("OK")){
						saldo = coderror.substring(16,30).trim();
						cuenta	= datos[1].trim();
						titular = datos[3].trim();
						aCuenta[0] = cuenta;
						aCuenta[1] = titular;
						aCuenta[2] = formatea(saldo);
						String[] a = (String []) aCuenta;
					    tabla_cuentas.put(aCuenta[0], a.clone());
						cuentas_regreso.add(a.clone());
						contador++;
					}else{
						cuenta	= datos[1].trim();
						titular = datos[3].trim();
						saldo = "No se pudo obtener informacion de esa cuenta";
						aCuenta[0] = cuenta;
						aCuenta[1] = titular;
						aCuenta[2] = saldo;
						String[] a = (String []) aCuenta;
					    tabla_cuentas.put(aCuenta[0], a.clone());
						cuentas_regreso.add(a.clone());
						contador++;
					}
		}
	    }//while
	}catch(Exception e){
	    e.printStackTrace();
			return 1;
	}finally{
			try{
				entrada.close();
			}catch(Exception e){
				e.printStackTrace();
				return 1;
			}
	}
	if ((contador>0)/*&&(contador<=Global.MAX_CTAS_MODULO)*/){ //MSD 24/03/2008 ...eliminando los fantasmas del pasado...
	    req.setAttribute("vector_cuentas",cuentas_regreso);
	    sess.setAttribute("vector_cuentas",cuentas_regreso);
	}else{
			return 1;
	}
	return 0;
    }

	private String sinComas(String a) {
		StringBuffer sb = new StringBuffer();
	    for (int k=0; k < a.trim().length() ;k++){
		    if(a.charAt(k)!=',')
			    sb.append(a.charAt(k));
	    }
		return sb.toString();
	}

	private   String formatea( String a){
		String temp ="";
		String deci = "";
		String regresa = "";
		int valexp = 0;
		int indice =	a.indexOf(".");
		int otroindice	= a.indexOf("E");
		try{
			if( otroindice>0 ){
				BigDecimal mivalor = new BigDecimal( a );
				a = mivalor.toString();
				indice =    a.indexOf(".");
			}
			if( indice>0 )
				a = (new BigDecimal(a+"0")).toString();
			else
				a = (new BigDecimal(a)).toString();
			indice =	a.indexOf(".");
			temp = a.substring (0, indice);
			deci = a.substring ( indice+1, indice+3 );

			String original = temp;
			int indexSN = 0;
			boolean bSignoNegativo = false;
			if( ( indexSN = original.indexOf("-") )> -1 ){
				bSignoNegativo = true;
				temp = original.substring( indexSN + 1 , temp.length() );
			}
			while( temp.length()>3 ){
				a = temp.substring(temp.length() -3, temp.length() );
				regresa = ","+a+regresa;
				temp = temp.substring(0,temp.length()-3);
			}
			regresa = temp + regresa+"."+deci;
			if( bSignoNegativo ){
				regresa = "($"+regresa +")";
			}
		}catch(Exception e){  }
		return regresa;
	}

	/**
	 * Metodo
	 * @param request
	 * @param response
	 * @return
	 * @throws ServletException
	 * @throws java.io.IOException
	 */
	public String ValidaSesionMicrositio( HttpServletRequest request, HttpServletResponse response )
    throws ServletException, java.io.IOException{
		final String LOG_METODO = "CuentaCargoMicrositio - ValidaSesionMicrositio - ";
		HttpSession sess;
		int resDup = 0;
		String idSession = null;
		String resultado = "";
		sess = request.getSession (false);
		mx.altec.enlace.bo.BaseResource session = null;
		EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
		if(sess == null){
			resultado = "KO";
			EIGlobal.mensajePorTrace(LOG_METODO + "sesion nula" , EIGlobal.NivelLog.DEBUG);
		}else{
			session = (BaseResource)request.getSession().getAttribute(SESSION);
			idSession = sess.getId();
			if(session == null || session.getUserID8 () == null || session.getUserID8 ().equals ("")){
				resultado = "KO";
				EIGlobal.mensajePorTrace(LOG_METODO + "Datos vacios", EIGlobal.NivelLog.DEBUG);
			}else{
				 session.setUltAcceso(Calendar.getInstance().getTime().getTime());
				 if(session.getTiempoActivo() > (Global.EXP_SESION * 60000)) {
					 resDup = cierraDuplicidad(session.getUserID8());
					 session.setSeguir(false);
					 sess.setAttribute(SESSION, session);
					 sess.removeAttribute(SESSION);
					 sess.invalidate();
					 return resultado;
				 }
				 if(session.getValidarSesion() && (!validaSesionDB(session.getUserID8() , idSession))){
					 resDup = cierraDuplicidad(session.getUserID8());
					 session.setSeguir(false);
					 sess.removeAttribute(SESSION);
					 sess.invalidate();
					return resultado;
				 }
			 	int statusToken = session.getToken().getStatus();
			 	if(session.getValidarToken() &&
			 	  !session.getTokenValidado()) {
			 		sess.setAttribute("unicontratoOK", "S");
			 		switch(statusToken) {
			 			case 0:		//activaci�n
			 					EIGlobal.mensajePorTrace(LOG_METODO + "Entra 0", EIGlobal.NivelLog.DEBUG);
			 				   	response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
			 				   	request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su token ya le fue entregado pero no est� activo. Por favor f�rmese en Enlace por Internet para activarlo e intente nuevamente.\", 1);");
			 				   	return "act";
			 			case 1:		//validaci�n
			 					EIGlobal.mensajePorTrace(LOG_METODO + "Entra 1", EIGlobal.NivelLog.DEBUG);
			 					request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
			 					guardaParametrosEnSession(request);
			 					response.sendRedirect("/Enlace" + IEnlace.TOKEN_MICROSITIO);
			 					return "token";
			 			case 2:		//bloqueado
			 					EIGlobal.mensajePorTrace(LOG_METODO + "Entra 2", EIGlobal.NivelLog.DEBUG);
			 					request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 1);");
			 					resDup = cierraDuplicidad(session.getUserID8());
			 					response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
			 					return "bloq";
			 			default:
			 					EIGlobal.mensajePorTrace(LOG_METODO + "Entra default", EIGlobal.NivelLog.DEBUG);
			 					request.getSession().setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Su Token es en un estatus que no le permite operar en Enlace Internet.\", 1);");
			 					response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
			 					return "errorToken";
			 		}
				}
				resultado = "OK";
				if(session.getTokenValidado())
					session.setTokenValidado(false);
				sess.setAttribute("unicontratoOK", null);
			}
			EIGlobal.mensajePorTrace( "CuentaCargoMicrositio-VSM - resultado, resDup: [" + resultado + "], [" + resDup + "]" , EIGlobal.NivelLog.DEBUG);
		}
		if(session != null){
			sess.setAttribute (SESSION, session);
	        if(session.getValidarSesion()) {
	         	actualizaSesionDB(session.getUserID8());
		 	 }
		}
		return resultado;
	}

	/**
	 * Metodo
	 */
    private void guardaParametrosEnSession (HttpServletRequest request){
		EIGlobal.mensajePorTrace("-333333aaaaa---------Interrumple el flujo--",EIGlobal.NivelLog.INFO);
		String template= request.getServletPath();
		EIGlobal.mensajePorTrace("5-****--------------->template**********--->"+template,EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla",template);
		HttpSession session = request.getSession(false);
		if(session != null){
			Map tmp = new HashMap();
			Enumeration enumer = request.getParameterNames();
			EIGlobal.mensajePorTrace("6665555-****--------------->enumer**********--->"+request.getParameter( "valida" ),EIGlobal.NivelLog.INFO);
			while(enumer.hasMoreElements()){
				String name = (String)enumer.nextElement();
				EIGlobal.mensajePorTrace(name+"----valor-------"+request.getParameter(name),EIGlobal.NivelLog.INFO);
				if(name.equals("lstContratos")){
					String con = (String)request.getParameter(name);
					EIGlobal.mensajePorTrace("setteando CONTRATO->"+con,EIGlobal.NivelLog.INFO);
					session.setAttribute("contratoBTIF", con);
				}
				tmp.put(name,request.getParameter(name));
			}
			session.setAttribute("parametros",tmp);
			tmp = new HashMap();
			enumer = request.getAttributeNames();
			while(enumer.hasMoreElements()){
				String name = (String)enumer.nextElement();
				tmp.put(name,request.getAttribute(name));
			}
			session.setAttribute("atributos",tmp);
			session.setAttribute("plantilla",template);
		}
	}

		/**
		 * @param usr usr
		 * @param request request
		 * @param sess sess
		 * @return boolean boolean
		 */
		public boolean validaDupMicrositio(String usr, HttpServletRequest request, BaseResource sess) {
			final String LOG_METODO = "CuentaCargoMicrositio - validaDupMicrositio - ";
			String usuario = null;
			if(usr.length() == 7) {
				usuario = convierteUsr7a8(usr);
			} else {
				usuario = usr;
			}
			long ultAcceso = -1;
			String ultAccesoStr = "";
			boolean valSesion = true;
			String activo = "";
	        String validarSesion = "";
			boolean acceso = false;
	        HttpSession session = request.getSession();
	        String sessionId = null;
			EIGlobal.mensajePorTrace (LOG_METODO + "Usuario a validar: <" + usuario + ">", EIGlobal.NivelLog.DEBUG);
	    	Connection conn1= null;
	    	PreparedStatement sDup1 =null;
	    	ResultSet rs1=null;
	    	Connection conn2=null;
	    	PreparedStatement sDup2 =null;
	    	try {
		     	conn1= createiASConn ( Global.DATASOURCE_ORACLE );
		     	String query = "SELECT TO_CHAR(ULT_ACCESO, 'DD-MON-YYYY HH24:MI', 'NLS_DATE_LANGUAGE = SPANISH') AS ULT_ACCESO, (SYSDATE - ULT_ACCESO) * 86400 AS TIEMPO, ACTIVO, VALIDAR_SESION, ID_SESION FROM EWEB_VALIDA_SESION WHERE CVE_USUARIO = ? ";
		     	sDup1 = conn1.prepareStatement(query);
		     	sDup1.setString(1, usuario);
		     	EIGlobal.mensajePorTrace (LOG_METODO + " Query: [" + query + "], usuario[" + usuario + "]", EIGlobal.NivelLog.DEBUG);
		     	rs1  = sDup1.executeQuery();
		     	while(rs1.next()) {
					ultAcceso = rs1.getLong("TIEMPO");
					ultAccesoStr = rs1.getString("ULT_ACCESO");
					activo = rs1.getString("ACTIVO");
	                validarSesion = rs1.getString("VALIDAR_SESION");
	                sessionId = rs1.getString("ID_SESION");
					EIGlobal.mensajePorTrace (LOG_METODO + "TIEMPO        : [[" + ultAcceso + "]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace (LOG_METODO + "ULT_ACCESO    : [" + ultAccesoStr + "]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace (LOG_METODO + "TIEMPO_SES    : [" + (Global.EXP_SESION * 60) + "]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace (LOG_METODO + "ACTIVO        : [" + activo + "]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace (LOG_METODO + "VALIDAR_SESION: [" + validarSesion + "]", EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace (LOG_METODO + "ID_SESION ANT : [" + sessionId + "]", EIGlobal.NivelLog.DEBUG);
				}
			}catch(SQLException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	                        EIGlobal.mensajePorTrace (LOG_METODO + "Usuario:[" + usuario + "] Problemas para validar acceso, se deniega", EIGlobal.NivelLog.ERROR);
				acceso = true;
				return acceso;
			}finally{
					try{
						if(rs1!=null){
							rs1.close();
							rs1=null;
						}
						if(sDup1!=null){
							sDup1.close();
							sDup1=null;
						}
						if(conn1!=null){
							conn1.close();
							conn1=null;
						}
					}catch(SQLException e1){
						EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
					}
			}
	        if("N".equals(validarSesion)) {
	           EIGlobal.mensajePorTrace (LOG_METODO + "[" + usuario + "] Usuario configurado para NO validar sesiones duplicadas -> se acepta acceso", EIGlobal.NivelLog.INFO);
	           acceso = true;
	           valSesion = false;
	        }else
	        	if(ultAcceso == -1) {
					EIGlobal.mensajePorTrace (LOG_METODO + "[" + usuario + "] Usuario sin info en tabla -> se acepta acceso", EIGlobal.NivelLog.INFO);
					acceso = true;
				}else
				  	if("S".equals(activo)) {
				  		EIGlobal.mensajePorTrace (LOG_METODO + "[" + usuario + "] Usuario activo en sistema, se validan horas", EIGlobal.NivelLog.INFO);
				  		if(ultAcceso > (Global.EXP_SESION * 60)) {
				  			acceso = true;
				  			EIGlobal.mensajePorTrace (LOG_METODO + "["  + usuario + "]Sesi&oacute;n vencida-> se acepta acceso", EIGlobal.NivelLog.INFO);
				  			//request.setAttribute ( "plantillaElegida", "cuadroDialogoEspecial2('Enlace ha detectado que no cerr&oacute; su sesi&oacute;n adecuadamente la &uacute;ltima vez. \\nPor favor d&eacute; click en Logout una vez que termine de usar su sesi&oacute;n.\\nPara cualquier duda comun&iacute;quese a Superl&iacute;nea Empresarial en M&eacute;xico al\\n51694343 o del interior al 018005095000', 4); " );
				  		}else {
				  				acceso = false;
				  				EIGlobal.mensajePorTrace (LOG_METODO + "["  + usuario + "] Sesi&oacute;n activa-> se deniega acceso", EIGlobal.NivelLog.INFO);
				  		}
				  	}else {
				  			EIGlobal.mensajePorTrace (LOG_METODO + "[" + usuario + "]Usuario inactivo en sistema, se acepta login", EIGlobal.NivelLog.INFO);
				  			acceso = true;
				  	}
			if(acceso && valSesion) {
				try {	conn2= createiASConn ( Global.DATASOURCE_ORACLE );
						String query = "";
						if(ultAcceso != -1) {
							query = "UPDATE EWEB_VALIDA_SESION SET ULT_ACCESO = SYSDATE, ACTIVO = 'S' WHERE CVE_USUARIO = ?";
						}
						else {
							query = "INSERT INTO EWEB_VALIDA_SESION VALUES (?, SYSDATE, 'S', 'S')";
						}
						sDup2 = conn2.prepareStatement(query);
						sDup2.setString(1,usuario);
						EIGlobal.mensajePorTrace (LOG_METODO + "Query2: <" + query + ">", EIGlobal.NivelLog.DEBUG);
						int result = sDup2.executeUpdate();
						EIGlobal.mensajePorTrace (LOG_METODO + "Registros actualizados: " + result, EIGlobal.NivelLog.DEBUG);
						if(result == 0){
							acceso = false;
						}
	            }catch(SQLException e){
	                EIGlobal.mensajePorTrace (LOG_METODO + "Problemas para actualizar acceso, se deniega", EIGlobal.NivelLog.INFO);
	            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	                       acceso = true;
	                       return acceso;
	            }finally{
	            		try{
							if(sDup2!=null){
								sDup2.close();
								sDup2=null;
							}
							if(conn2!=null){
								conn2.close();
								conn2=null;
							}
						}catch(SQLException e1){
		                    EIGlobal.mensajePorTrace (LOG_METODO + e1, EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
						}
				}
			}
			if(acceso) {
				if(valSesion) {
					sess.setUserID(usuario);
					sess.setUserID8(usuario);//cambio
					sess.setSid(session.getId());
					sess.setUltAcceso(Calendar.getInstance().getTime().getTime());
					sess.setTiempoSesion((Global.EXP_SESION * 60000));
					sess.setValidarSesion(valSesion);
					PreparedStatement sDup3 = null;
					sessionId = session.getId();
					try {
						conn2= createiASConn ( Global.DATASOURCE_ORACLE );
						String query;
						query = "UPDATE EWEB_VALIDA_SESION SET ID_SESION = ? WHERE CVE_USUARIO = ?";
						sDup3 = conn2.prepareStatement(query);
						sDup3.setString(1, sessionId);
						sDup3.setString(2, usuario);
						sDup3.executeUpdate();

					}catch(SQLException e){
		                EIGlobal.mensajePorTrace (LOG_METODO + "Problemas para actualizar acceso, se deniega", EIGlobal.NivelLog.INFO);
		            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		                       acceso = true;
		                       return acceso;
		            }finally{
						try{
							if(sDup2!=null){
								sDup2.close();
								sDup2=null;
							}
							if(conn2!=null){
								conn2.close();
								conn2=null;
							}
						}catch(SQLException e1){
							EIGlobal.mensajePorTrace (LOG_METODO + e1, EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(new Formateador().formatea(e1), EIGlobal.NivelLog.INFO);
						}
					}
				}
				//Guarda la fecha de Ultimo Acceso
				sess.setUltAccesoStr(ultAccesoStr);
				session.setAttribute (SESSION, sess);
			}
			return acceso;
		}

}