package mx.altec.enlace.servlets;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.InterbancariasRecibidasBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.InterbancariasRecibidasBO;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.bita.BitaTCTBean;

//VSWF
/**ISBAN
*Z712236 InterbancariasRecibidas
*02/09/2014
*/

public class InterbancariasRecibidas extends BaseServlet
{
	/**
	 * TEXTCUENTAX: texto de cuenta
	 */
	private static final String TEXTCUENTAX="textEnlaceCuenta";
	
	/**
	 * estatusOperacion : ESTATUS DE LA OPERACION
	 */
	private static final String estatusOperacion="A";
	
	/**
	 * serialVersionUID valor serial
	 */
	private static final long serialVersionUID = 8348767658223349564L;
	
	/**
	 * Metodo HttpServletRequest
	 * @param req request
	 * @param res response
	 * @throws ServletException expcecion en el Servlet
	 * @throws IOException Excepcion en la entrada salida de archivo
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * Metodo HttpServletRequest
	 * @param req request
	 * @param res response
	 * @throws ServletException expcecion en el Servlet
	 * @throws IOException Excepcion en la entrada salida de archivo
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**
	 * Metodo DefaultAction
	 * @param request  Recibe parametros
	 * @param response Respuesta parametros
	 * @throws IOException Excepcion en la entrada salida de archivo
	 * @throws ServletException expcecion en el Servlet
	 */
	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		
		CalendarNomina myCalendarNomina;
		EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: Entrando al metodo defaultAction... &", EIGlobal.NivelLog.INFO);
		String modulo = "";
 		final boolean sesionvalida = SesionValida( request, response );

 		final HttpSession sess = request.getSession();
 	    final BaseResource session = (BaseResource) sess.getAttribute("session");
 	    
        if (! sesionvalida ) {
        	return;
        }
        myCalendarNomina= new CalendarNomina();
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());
 		
 		modulo = request.getParameter("Modulo");
 		
		String VarFechaHoy = "";
		VarFechaHoy = myCalendarNomina.armaArregloFechas();
		String fechaHoy = "";
		final GregorianCalendar cal = new GregorianCalendar();
		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new java.util.Locale("es","mx"));
		fechaHoy = sdf.format(cal.getTime());
		request.setAttribute( "VarFechaHoy", VarFechaHoy);
		request.setAttribute("DiaHoy",fechaHoy);
		
        Date fechaActual = new Date();
	    String hoy = sdf.format(fechaActual);
	    request.setAttribute("fechaActual",hoy);
			
		EIGlobal.mensajePorTrace( "InterbancariasRecibidas.java :: valor de Modulo: " + modulo, EIGlobal.NivelLog.INFO );
			
		session.setModuloConsultar (IEnlace.MConsulta_Saldos);
			
		if ( (modulo == null) || ( ("").equals(modulo))) {
			modulo = "inicio";
		}
		if (("inicio").equals(modulo)) {
			inicioInterbancariasRecibidas(request, response);
		}else if (("consulta").equals(modulo)) {
			consultaInterbancariasRecibidas(request, response);
		}
	}
	
	
	/**
	 * Despliega la pagina principal de Interbancarias recibidas
	 * @param request peticion del cliente
	 * @param response respuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void inicioInterbancariasRecibidas(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		
		EIGlobal.mensajePorTrace("InterbancariasRecibidas.java :: Dentro de modulo=inicio y con Facultad CONSMOVTOS****", EIGlobal.NivelLog.INFO);
	    InterbancariasRecibidasBean resultado=new InterbancariasRecibidasBean();
	    request.setAttribute("resultadoConsulta", resultado);
	    
		request.setAttribute("Encabezado",CreaEncabezado("Consulta de movimientos de interbancarias recibidas","Consultas &gt; Movimientos &gt; Chequeras","s25800h",request));
		evalTemplate("/jsp/InterbancariasRecibidas.jsp", request, response );		
		
	}
	
	/**
	 * consulta de Interbancarias recibidas
	 * @param request peticion del cliente
	 * @param response respuesta al cliente
	 * @throws ServletException control de excepciones del servlet
	 * @throws java.io.IOException control de exceciones del archivo
	 */
	protected void consultaInterbancariasRecibidas(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		
		EIGlobal.mensajePorTrace("InterbancariasRecibidas.java :: Dentro de modulo=consulta y con Facultad CONSMOVTOS****", EIGlobal.NivelLog.INFO);
		InterbancariasRecibidasBO bo=new InterbancariasRecibidasBO();
		
 		final HttpSession sess = request.getSession();
 		BaseResource sesion = (BaseResource) sess.getAttribute ("session");
 		final String fecha = (String) getFormParameter(request, "fecha1"); // formatear la fecha enviar sin diagonales ddmmaaaa
 		String cuentaCargo = (String) getFormParameter(request, TEXTCUENTAX);
		if(null != cuentaCargo){
			String cuenta[]=cuentaCargo.split(" ");
			cuentaCargo=cuenta[0];
		}
		
		final String txtReferencia = request.getParameter("txtReferencia") != null ? request.getParameter("txtReferencia") : "";
		final String txtCveRastreo = request.getParameter("txtCveRastreo") != null ? request.getParameter("txtCveRastreo") : "";
			
		InterbancariasRecibidasBean interbancariasRecibidasBean=new InterbancariasRecibidasBean();
		interbancariasRecibidasBean.setFchTrasnfer(fecha);
		interbancariasRecibidasBean.setCuenta(cuentaCargo);
		interbancariasRecibidasBean.setReferencia(txtReferencia);
		interbancariasRecibidasBean.setCveRastreo(txtCveRastreo);
			
		InterbancariasRecibidasBean resultado=new InterbancariasRecibidasBean();
		try {
			resultado=bo.consultarInterbancariasRecibidas(interbancariasRecibidasBean, request, response);
		}catch (SQLException e){
			EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: Error de excepcion SQL "+e, EIGlobal.NivelLog.INFO);
		}catch(Exception e){
			EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: Error de excepcion "+e, EIGlobal.NivelLog.INFO);
		}
			
		if(!"TRIB0000".equals(resultado.getCodError())){
			EIGlobal.mensajePorTrace("InterbancariasRecibidas - consultaInterbancariasRecibidas(): no se encontraron datos", EIGlobal.NivelLog.DEBUG);
			despliegaPaginaError( "No se encontraron registros en la consulta.",
					"Consulta de movimientos de interbancarias recibidas", " Consultas &gt; Movimientos ", "Chequeras", request, response);
			return;
		}
		//Registrando en pistas y botacora de operaciones
		resultado.setCuenta(cuentaCargo!=null?cuentaCargo:"");//setea la cuenta para guardar en bitacora
		guardaBitacora(resultado, request, response);
		
		request.setAttribute("pCuenta", cuentaCargo);
		request.setAttribute("resultadoConsulta", resultado);
	    request.setAttribute ("MenuPrincipal", sesion.getStrMenu ());
	    request.setAttribute ("newMenu", sesion.getFuncionesDeMenu ());
		request.setAttribute("Encabezado",CreaEncabezado("Consulta de movimientos de interbancarias recibidas","Consultas &gt; Movimientos &gt; Chequeras","s25800h",request));
	
		evalTemplate("/jsp/InterbancariasRecibidas.jsp", request, response );		
	}
	
	/**
	 * Guarda bitacora
	 * @param request parametro HttpServletRequest
	 * @param response parametro HttpServletRequest
	 */
	public void guardaBitacora(InterbancariasRecibidasBean resultado, HttpServletRequest request,
			HttpServletResponse response){
		ServicioTux tuxGlobal = new ServicioTux();
 		Hashtable hs = null;
 		Integer referencia = 0 ;
 		String codError = null;
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute("session");
		
		String cuentaCargo = (String) getFormParameter(request, TEXTCUENTAX);
		if(null != cuentaCargo){
			String cuenta[]=cuentaCargo.split(" ");
			cuentaCargo=cuenta[0];
		}
		
		if (("ON").equals(Global.USAR_BITACORAS.trim())){
			final BitaHelper bh = new BitaHelperImpl(request, session,sess);
			bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
			BitaTransacBean bt = new BitaTransacBean();
			BitaTCTBean beanTCT = new BitaTCTBean ();
			beanTCT = bh.llenarBeanTCT(beanTCT);
			beanTCT.setCodError("CTIR0000");
			try {
				hs = tuxGlobal.sreferencia("901");
				referencia = (Integer) hs.get("REFERENCIA");
				beanTCT.setReferencia(referencia);
				bt = (BitaTransacBean)bh.llenarBean(bt);
				if(null != session.getToken().getSerialNumber()) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}
				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}
				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}
				bt.setIdFlujo(BitaConstants.EC_MOV_CHEQ_INT_RECIBIDAS);
				beanTCT.setTipoOperacion(BitaConstants.EC_MOV_CHEQ_INT_RECIBIDAS);
				beanTCT.setCuentaOrigen(resultado.getCuenta());
				beanTCT.setCuentaDestinoFondo(resultado.getNumReferencia());
				bt.setReferencia(referencia);
				bt.setCctaOrig(cuentaCargo.trim());
				bt.setEstatus(estatusOperacion);
				codError = hs.get("COD_ERROR").toString();
				bt.setIdErr(codError);
				bt.setNumBit(BitaConstants.EC_MOV_CHEQ_INT_RECIBIDAS_ENTRA);
				bt.setContrato(session.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);//EWEB_ADMIN_BITACORA
				BitaHandler.getInstance().insertBitaTCT(beanTCT);//TCT_BITACORA
				EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: bitacora exitosa ", EIGlobal.NivelLog.INFO);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: Error  SQLException al grabar en bitacora "+e, EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("***InterbancariasRecibidas.java :: Error de Exception al grabar en bitacora "+e, EIGlobal.NivelLog.INFO);
			}
		}
	}
}