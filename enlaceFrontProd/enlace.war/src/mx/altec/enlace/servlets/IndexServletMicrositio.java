package mx.altec.enlace.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.PagoMicrositioRefBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.PagoMicrositioRefBo;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;


public class IndexServletMicrositio extends BaseServlet
{
	/**
	 *Serie
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Atributo "IV-USER"
	 */
	private static final String IV_USER = "IV-USER";
	/**
	 * Servicion ID SAT
	 */
	private static final String ID_SERVICIO_SAT = "PMRF";

	/**
	 * Atributo servicio_id
	 */
	private static final String SERVICIO_ID = "servicio_id";
	/**
	 * Atributo linea_captura
	 */
	private static final String LINEA_CAPTURA = "linea_captura";
	/**
	 * Atributo CodError
	 */
	private static final String COD_ERROR = "CodError";
	/**
	 * Atributo MsgError
	 */
	private static final String MSG_ERROR = "MsgError";
	/**
	 * Atributo HoraError
	 */
	private static final String HORA_ERROR = "HoraError";

	/**
	 * Atributo  url_resp
	 */
	private static final String URL_RESP = "url_resp";
	/**
	 * Atributo  url_resp
	 */
	private static final String IMPORTE = "importe";

	/**
	 * init
	 * @param config ServletConfig
	 * @throws ServletException ServletException
	 */
	public void init (ServletConfig config)
		throws ServletException
	{
		super.init (config);
		java.util.Enumeration enumeration = config.getInitParameterNames ();

		//EIGlobal.mensajePorTrace ("PRUEBA " + enum.hasMoreElements (), 4);

		while (enumeration.hasMoreElements ()) {
			EIGlobal.mensajePorTrace ((String) enumeration.nextElement (), EIGlobal.NivelLog.INFO);
		}
		enumeration = config.getServletContext ().getAttributeNames ();
		while (enumeration.hasMoreElements ()) {
			EIGlobal.mensajePorTrace ((String) enumeration.nextElement (), EIGlobal.NivelLog.INFO);
		}
		enumeration = config.getServletContext ().getInitParameterNames ();
		while (enumeration.hasMoreElements ()) {
			EIGlobal.mensajePorTrace ((String) enumeration.nextElement (), EIGlobal.NivelLog.INFO);
		}
	}

	/**
	 * doGet
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		defaultAction(req, res);
	}

	/**
	 * doPost
	 * @param req HttpServletRequest
	 * @param res HttpServletResponse
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		defaultAction(req, res);
	}


	/****************************************************************
	 * displayMessage
	 * @param req  variable request
	 * @param res variable response
	 * @param messageText  mensaje a desplegar
	 * @throws ServletException Excepcion Generica
	 * @throws IOException Excepcion Generica
	 * ***************************************************************/
	public void displayMessage(HttpServletRequest req, HttpServletResponse res, String messageText)
		throws ServletException, IOException
	{
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println(messageText);
	}


	/****************************************************************
	 * defaultAction  Realiza acciones generales para get y post
	 * @param req  variable request
	 * @param res variable response
	 * @throws ServletException Excepcion generica
	 * @throws IOException Excepcion generica
	 * ***************************************************************/
	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		res.setContentType("text/html");
		String headerUser=(String)req.getHeader(IV_USER);
		try{
			// Definicion de objetos para transferencias
			String convenio = null;
			String importe = null;
			String referencia = null;
			String url = null;
			String concepto = null;
			String servicio_id = null;

			// Definicion de objetos par pago referenciado
			String idSat = "";
			String tipoPersona = "";
			String lineaCaptura = "";
			String importeSat = "";
			String rfc = "";
			String datosHash = "";
			String convenioSat = "";

			/* Comienza recepcion de parametros para cuando es pago de impuestos referenciado por micrositio
			 * Isban (IOV)
			 * P/022071 7 M021943 - Establecer la URL para el pago de impuestos
			 * */
			idSat = (String) req.getParameter("ID");
			convenioSat = (String) req.getParameter("CST1");

			if(null != idSat && "SAT".equals(idSat)|| (null != convenio && "0093".equals(convenio)))
			{
				EIGlobal.mensajePorTrace("IndexServletMicrositio::La opreacion a realizar es pago Referenciado datos....", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IndexServletMicrositio::Linea de captura :"+ req.getParameter("LC"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IndexServletMicrositio::Importe :"+ req.getParameter("IM"), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IndexServletMicrositio::Convenio :"+ req.getParameter("CST1"), EIGlobal.NivelLog.INFO);

				tipoPersona = (String) req.getParameter("TP");
				lineaCaptura = (String) req.getParameter("LC");
				importeSat = (String) req.getParameter("IM");
				rfc = (String) req.getParameter("RFC");
				datosHash = (String) req.getParameter("SH");

				PagoMicrositioRefBean beanPagRef =  null;
				PagoMicrositioRefBo boPagRef = new PagoMicrositioRefBo();
				boolean valErrPMRF = false;

				beanPagRef = new PagoMicrositioRefBean();
				beanPagRef.setIdSat(idSat);
				beanPagRef.setLineaCaptura(lineaCaptura);
				beanPagRef.setImporteSat(importeSat);
				beanPagRef.setRfc(rfc);
				beanPagRef.setTipoPersona(tipoPersona);
				beanPagRef.setConvenio(convenio);
				beanPagRef.setDatosHash(datosHash);


				//Funcionalidad para desplegar mensaje en caso de que algun campo no contenga informacion

				if(!boPagRef.validaDatos(beanPagRef) && !valErrPMRF)
				{
					msgErrorMicrositio = beanPagRef.getMsgError().toString();
					codErrorMicrositio = "2002";
					EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Datos PMRF Incorrectos" , EIGlobal.NivelLog.INFO);
					valErrPMRF = true;
				}

				//Fin valida datos

				//Se agrega funcionalidad para validar el hash

				if(!boPagRef.validaHash(beanPagRef) && !valErrPMRF)
				{
					msgErrorMicrositio = beanPagRef.getMsgError().toString();
					codErrorMicrositio = "2003";
					EIGlobal.mensajePorTrace( "LoginServletMicrositio - defaultAction - Hash Incorrecto" , EIGlobal.NivelLog.INFO);
					valErrPMRF = true;
				}
				//Fin Validacion Hash

				//Despliega Mensaje de error
				if(valErrPMRF)
				{
					String usrCSS1 = "";
					EIGlobal.mensajePorTrace("LoginServletMicrositio:: valErrPMRF " + valErrPMRF, EIGlobal.NivelLog.DEBUG);
					if(null!=req.getHeader(IV_USER)){
						usrCSS1 = req.getHeader(IV_USER);
					}
					else if(null!=session.getUserID()){
						usrCSS1 = session.getUserID();
					}

					EIGlobal.mensajePorTrace("LoginServletMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS1, EIGlobal.NivelLog.DEBUG);

					sess.setAttribute("usrCSS", usrCSS1);
					req.setAttribute (SERVICIO_ID, ID_SERVICIO_SAT);
					req.setAttribute (LINEA_CAPTURA, lineaCaptura);
					req.setAttribute (COD_ERROR, codErrorMicrositio);
					req.setAttribute (MSG_ERROR, msgErrorMicrositio);
					req.setAttribute (HORA_ERROR,ObtenHora ());
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}

			} else { // Flujo normal
				EIGlobal.mensajePorTrace("IndexServletMicrositio::el contenido del campo URL es:" + req.getParameter(URL_RESP), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IndexServletMicrositio::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)req.getParameter(URL_RESP)), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("IndexServletMicrositio::el contenido del campo URL despues es:" + req.getParameter(URL_RESP), EIGlobal.NivelLog.INFO);
				convenio = (String) req.getParameter("convenio");
			 	importe = (String) req.getParameter(IMPORTE);
			 	referencia = Util.HtmlEncode((String) req.getParameter("referencia"));
			 	url = Util.HtmlEncode((String)req.getParameter(URL_RESP));
	            concepto = Util.HtmlEncode((String)req.getParameter("concepto"));
	            servicio_id = Util.HtmlEncode((String)req.getParameter(SERVICIO_ID));

            if(concepto == null) {
				concepto = "";
			}
            if(servicio_id == null) {
				servicio_id = "";
			}
			importe = sinComas(formatea(importe));

			}

			if (null != convenio && null != referencia && null != importe && null != url && !"SAT".equals(idSat)){
				//Flujo Normal
				/**
				 * Correccion vulnerabilidades Deloitte
				 * Stefanini Nov 2013
				 */
				boolean valError = false;

				if(null!=convenio && Util.validaCCX(convenio, 1)){
					EIGlobal.mensajePorTrace("el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1003");
					EIGlobal.mensajePorTrace("El campo convenio no acepta caracteres especiales por favor introduzca solo n�meros->" + convenio, EIGlobal.NivelLog.INFO);
					valError = true;
				}
				if(null!=importe && Util.validaCCX(importe, 4)){
					EIGlobal.mensajePorTrace("el contenido del campo importe es correcto->" + importe, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1004");
					EIGlobal.mensajePorTrace("El campo importe no acepta caracteres especiales por favor introduzca n�meros y dos decimales->" + importe, EIGlobal.NivelLog.INFO);
					valError = true;
				}
				if(valError) {
					String usrCSS = "";
					EIGlobal.mensajePorTrace("IndexServletMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
					if(null!=req.getHeader(IV_USER)){
						usrCSS = req.getHeader(IV_USER);
					}
					else if(null!=session.getUserID()){
						usrCSS = session.getUserID();
					}
					EIGlobal.mensajePorTrace("IndexServletMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);

					sess.setAttribute("usrCSS", usrCSS);
					req.getSession().setAttribute("inyeccionURL","true");
					req.setAttribute("inyeccion", true);
					req.setAttribute (HORA_ERROR, ObtenHora());
					req.setAttribute(MSG_ERROR, "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}
				/**
				 * Fin correccion vulnerabilidades Deloitte
				 */
				req.setAttribute("convenio", convenio);
				req.setAttribute("referencia", referencia);
				req.setAttribute(IMPORTE, importe);
				req.setAttribute("url", url);
                req.setAttribute("concepto", concepto);
                req.setAttribute(SERVICIO_ID, servicio_id);
                req.setAttribute("origenLogin","1");
				if (headerUser!=null){
						headerUser = convierteUsr8a7(headerUser);
						EIGlobal.mensajePorTrace("LoginServlet Micrositio->" + headerUser,EIGlobal.NivelLog.INFO);
						evalTemplate("/jsp/devicePrintMicrositio.jsp",req,res);
						EIGlobal.mensajePorTrace("Pantalla SAM",EIGlobal.NivelLog.DEBUG);
				}
				else{
		 			evalTemplate(IEnlace.LOGIN_MICROSITIO, req, res);
				}

			} else if("SAT".equals(idSat) && null != importeSat && null != lineaCaptura && null != convenioSat) {

				/*
				 * ************************************************************************************************
				 * Entra  flujo pago Referenciado
				 * Isban (IOV)
			     * P/022071 7 M021943 - Establecer la URL para el pago de impuestos
				 * ************************************************************************************************
				 */

				boolean valError = false;

				if(null!=convenioSat && Util.validaCCX(convenioSat, 1)){
					EIGlobal.mensajePorTrace("el contenido del campo convenio es correcto ->" + convenio, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1003");
					EIGlobal.mensajePorTrace("El campo convenio no acepta caracteres especiales por favor introduzca solo n�meros->" + convenio, EIGlobal.NivelLog.INFO);
					valError = true;
				}

				if(valError){

					String usrCSS = "";
					EIGlobal.mensajePorTrace("IndexServletMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion", EIGlobal.NivelLog.DEBUG);
					if(null!=req.getHeader(IV_USER)){
						usrCSS = req.getHeader(IV_USER);
					}
					else if(null!=session.getUserID()){
						usrCSS = session.getUserID();
					}
					EIGlobal.mensajePorTrace("IndexServletMicrositio::validaCrossSiteScript:: usrCSS->"+usrCSS, EIGlobal.NivelLog.DEBUG);

					req.setAttribute (SERVICIO_ID, ID_SERVICIO_SAT);
					req.setAttribute (LINEA_CAPTURA, lineaCaptura);
					sess.setAttribute("usrCSS", usrCSS);
					req.setAttribute (HORA_ERROR, ObtenHora());
					req.setAttribute(MSG_ERROR, "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
					evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}

                req.setAttribute(SERVICIO_ID, ID_SERVICIO_SAT);
                req.setAttribute("convenio", convenioSat);
                req.setAttribute(IMPORTE, importeSat);
                req.setAttribute(LINEA_CAPTURA, lineaCaptura);
                req.setAttribute("origenLogin","1");

				if (headerUser!=null){
						headerUser = convierteUsr8a7(headerUser);
						EIGlobal.mensajePorTrace("LoginServlet Micrositio->" + headerUser,EIGlobal.NivelLog.INFO);
						evalTemplate("/jsp/devicePrintMicrositio.jsp",req,res);
						EIGlobal.mensajePorTrace("Pantalla SAM",EIGlobal.NivelLog.DEBUG);
				} else {
		 			evalTemplate(IEnlace.LOGIN_MICROSITIO, req, res);
				}

			} else {

				req.setAttribute(COD_ERROR, "0998");
				req.setAttribute(MSG_ERROR, "Alguno de los parametros no se recibio");
				req.setAttribute (HORA_ERROR,ObtenHora());

				if("SAT".equals(idSat)){
					req.setAttribute(IMPORTE, importeSat);
					req.setAttribute(SERVICIO_ID, ID_SERVICIO_SAT);
					req.setAttribute(LINEA_CAPTURA, lineaCaptura);
				} else {
					req.setAttribute("referencia", referencia);
					req.setAttribute(IMPORTE, importe);
					req.setAttribute ("url",url);
					req.setAttribute("concepto", concepto);
					req.setAttribute(SERVICIO_ID, servicio_id);
				}

				evalTemplate (IEnlace.ERROR_TMPL_MICROSITIO, req, res);  /*MSE*/
			}
		} catch(Exception e) {
			EIGlobal.mensajePorTrace( "***IndexServlet Micrositio ***  Excepci�n en IEnlace.LOGIN >>",EIGlobal.NivelLog.ERROR );
			e.printStackTrace();
			return; // error
		}
	}

	/**
	 * elimina comas del importe
	 * @param a parametro
	 * @return String sin comas
	 */
	private String sinComas (String a) {
		if(a == null) {
			return null;
		}
		StringBuffer sb = new StringBuffer();
	    for (int k=0; k < a.trim().length() ;k++){
		    if(a.charAt(k)!=',') {
			    sb.append(a.charAt(k));
		    }
	    }
		return sb.toString();
	}

	/**
	 * formatea
	 * @param a parametro
	 * @return String formato
	 */
	private String formatea ( String a){
		if(a == null) {
			return null;
		}
		String temp = "";
		String deci = "";
		String regresa = "";
		int indice = a.indexOf('.');
		if(indice<=0){
			a=a+".00";
			indice = a.indexOf('.');
		}
		if(indice==a.length()-1){
			a=a+"00";
			indice = a.indexOf('.');
		}
		int otroindice  = a.indexOf('E');
		try{
			if( otroindice>0 ){
				BigDecimal mivalor = new BigDecimal( a );
				a = mivalor.toString();
				indice =    a.indexOf('.');
			}
			if( indice>0 ){
				a = (new BigDecimal(a+"0")).toString();
			}
			else {
				a = (new BigDecimal(a)).toString();
			}
			indice =        a.indexOf('.');
			temp = a.substring (0, indice);
			deci = a.substring ( indice+1, indice+3 );

			String original = temp;
			int indexSN = 0;
			boolean bSignoNegativo = false;
			if( ( indexSN = original.indexOf('-') )> -1 ){
				bSignoNegativo = true;
				temp = original.substring( indexSN + 1 , temp.length() );
			}
			while( temp.length()>3 ){
				a = temp.substring(temp.length() -3, temp.length() );
				regresa = ","+a+regresa;
				temp = temp.substring(0,temp.length()-3);
			}
			regresa = temp + regresa+"."+deci;
			if( bSignoNegativo ){
				regresa = "($"+regresa +")";
			}
		}catch(Exception e){  }
		return regresa;
	}

}