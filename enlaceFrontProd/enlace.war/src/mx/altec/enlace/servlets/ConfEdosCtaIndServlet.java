package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import mx.altec.enlace.beans.ConfEdosCtaIndBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConfEdosCtaIndBO;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * @author Stefanini
 *
 */

/**
 *  @author Ignacio Rubio	 
 *  @category Cambio 
 *  @since Oct 2013 - M021044
 *  M021044 - Oct 10, 2013. Cambios de en los flujos base.
 *  1. Se elimina la inscripcion a XML, siempre estaran disponibles. La Configuracion aplica unicamente a PDF.
 *  2. Se eliminan las referencias a los flujos, solo aplica para PDF.
 *  3. Se elimina toda referencia a la transaccion DY01 y el codigo de retorno DYA0004, cambia por ODA0002
 *  
 */
public class ConfEdosCtaIndServlet extends BaseServlet {

	/** Constante serial version */
	private static final long serialVersionUID = 3634258462003593871L;
	/** Constante mensaje de error por facultades */
	private static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";
	
	/** 
	 * @author Ignacio Rubio
	 * @category Cambio 
	 * @since Oct 2013 - M021044
	 * 
	 * Constante codigo de exito de DY01 
	 *private static final String COD_EXITO = "DYA0004";
	 *
	 **/
	
	/** Constante codigo de exito de ODD4 */
	private static final String COD_EXITO = "ODA0002";
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Estados de Cuenta";
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Estados de Cuenta &gt; Modificar estado de env&iacute;o del estado de Cuenta Impreso &gt; Individual"; 
	/** Constante para estado de cuenta disponible */
	private static final String EDO_CTA_DISP = "edoCtaDisponible";
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_ConfigInd";
	/** Constante para session de BaseResource */
	private static final String BASE_RESOURCE = "session";
	/** Constante para flujo PDF **/
	private static final String FLUJO_PDF = "PDF";
	/** Constante para Secuencia de Domicilio **/
	private static final String SEQ_DOM = "hdSeqDomicilio";
	/** Constante para Estado de Cuenta Disponible **/
	private static final String EDOCTA_DISP = "hdEdoCtaDisponible";
	/** Constante para Suscripcion a Paperless **/
	private static final String SUSCR_PAPER = "hdSuscripPaperless";
	/** Constante para eval template **/
	private static final String EVAL_TEMPLATE="/jsp/confEdosCtasIndividualPDF.jsp";
	/** Constante para Cadena hdCodCliente **/
    private static final String CAD_HDCOD_CLIENTE="hdCodCliente";
	/** Constante para Cadena hdCuenta **/
    private static final String CAD_HDCUENTA="hdCuenta";
	/** Constante para Cadena MensajeErr **/
    private static final String CAD_MENSAJE_ERR="MensajeErr";
    /** Constante para Cadena de Cuadro Dialogo **/
    private static final String CAD_CUADRO_DIALOGO	= "cuadroDialogo('%s', 1);";
    /** TAG para logs **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfEdosCtaIndServlet ::: ";
	/** Constante para exportar cuentas **/
	private static final String CONTENT_TYPE="text/plain";
	/** Constante para exportar cuentas **/
	private static final String HNAME_CONTENT_DISPOSITION="Content-Disposition";
	/** Constante para exportar cuentas **/
	private static final String HVALUE_CONTENT_DISPOSITION="attachment;filename=Cuentas.txt";
	/** Numero de byte que se van a estar leyendo para la generacion del archivo de exportacion */
	private static final int BYTES_DOWNLOAD = 1024;
    
	
	/**
	 * @author Ignacio Rubio
	 * @category Cambio 
	 * @since Oct 2013 - M021044
	 *  
	 * Constante para flujo XML
	 * private static final String FLUJO_XML = "XML";
	 *  Constante para flujo paperless
	 * 
	 */
	
	private static final String SUS_PAPERLESS = "suscripPaperless";
	/** Constante para cuenta */
	private static final String CUENTA = "cuenta";
        /** Constante para cuenta nueva */
	private static final String CUENTA_NVA = "cuentaNva";
        /** Constante para URL de Config Individual */
	private static final String URL_CONFIND = "/jsp/confEdosCtasIndividual.jsp";
	
	/** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {    	
    	HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);
		String flujo = request.getParameter("flujoOp");
		String valida = request.getParameter("valida");
		
		boolean sesionvalida = SesionValida( request, response );
		if(sesionvalida) {
			if(session.getFacultad(BaseResource.FAC_EDO_CTA_IND)) {
				request.setAttribute("MenuPrincipal", session.getStrMenu());
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("Encabezado", CreaEncabezado(TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, request));
				//session.setModuloConsultar(IEnlace.MConsulta_Saldos);
			
				if ((flujo==null || "".equals(flujo)) && (valida==null || "".equals(valida))) {
					final BitaAdminBean bean = new BitaAdminBean();
					//[IECP-00] Entra a pantalla principal de Modificacion de Envio de Estado de Cuenta Individual
					bean.setTipoOp("");
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, request.getSession(), BitaConstants.EA_INGRESA_CONFIG_EDO_CTA_IND, bean, false);
					evalTemplate(URL_CONFIND, request, response);
				}else if("IND".equals(flujo)) {
					presentarCuentas(request, response);
				}else if("ctas".equals(flujo)) {
					exportar(request, response);
				}else if("cambiaCta".equals(flujo)) {
					cambiarCta(request, response);
				}else {
					configEdoCta(request, response);
				}
			} else {
				despliegaPaginaErrorURL(MSJ_ERROR_FAC, TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, request, response);
			}
		}
    	
    }
    
    /**
     * Metodo para presentar las cuentas y configuracion de estado de cuenta actual
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws java.io.IOException Excepcion
     */
    private void presentarCuentas(HttpServletRequest request, HttpServletResponse response) 
    throws ServletException, java.io.IOException{
    	ConfEdosCtaIndBO confEdosCtaIndBO = new ConfEdosCtaIndBO();
    	ConfEdosCtaIndBean confEdosCtaIndBean = null;
		final String cuenta = request.getParameter(CUENTA);
		
		//BaseResource session = (BaseResource) request.getSession().getAttribute(BASE_RESOURCE);
		SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy", Locale.US);
		String fechaHoy = sFormat.format(new Date());
		
		EIGlobal.mensajePorTrace("opcionFormato -> Flujo Unico para Estado de Cuenta PDF.", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("cuenta ->" + cuenta, EIGlobal.NivelLog.DEBUG);

		//[IECP-01] Modifica estado de envio de Estado de Cuenta Impreso Individual
	
		bitacorizaAdmin(request, confEdosCtaIndBean, BitaConstants.EA_OP_PDF_EDO_CTA_IND,true);
			
		confEdosCtaIndBean = confEdosCtaIndBO.mostrarCuentasPDF(cuenta);
		EIGlobal.mensajePorTrace("Fecha de configuracion anterior ->" + confEdosCtaIndBean.getFechaConfig(),EIGlobal.NivelLog.INFO);
			
		request.setAttribute("segmentoDom", confEdosCtaIndBean.getSecDomicilio());
		request.setAttribute("domicilio", confEdosCtaIndBean.getDomCompleto());
		request.setAttribute(EDO_CTA_DISP, confEdosCtaIndBean.isEdoCtaDisponible());
		request.setAttribute(SUS_PAPERLESS, confEdosCtaIndBean.isSuscripPaperless());
		request.setAttribute("codCliente", confEdosCtaIndBean.getCodCliente());
		if(confEdosCtaIndBean.isTieneProblemaDatos()){
			request.setAttribute("MENSAJE_PROBLEMA_DATOS", confEdosCtaIndBean.getMensajeProblemaDatos());
		}
		request.setAttribute(CUENTA, cuenta);
		request.setAttribute(CUENTA_NVA, cuenta);
			
		if(confEdosCtaIndBean.getFechaConfig() != null) {
			request.setAttribute("fechaConfigValida",
					!StringUtils.equals(fechaHoy, sFormat.format(confEdosCtaIndBean.getFechaConfig())) ? "SI" : "NO");
		}else {
			request.setAttribute("fechaConfigValida", "SI");
		}
		if(COD_EXITO.equals(confEdosCtaIndBean.getCodRetorno()) || "DYE0021".equals(confEdosCtaIndBean.getCodRetorno())
				|| ("ODE0065".equals(confEdosCtaIndBean.getCodRetorno()) && confEdosCtaIndBean.getCuentas().size() > 0 )) {
			request.getSession().setAttribute("cuentas", confEdosCtaIndBean.getCuentas());
			evalTemplate(EVAL_TEMPLATE, request, response);
		}else {
			EIGlobal.mensajePorTrace("CodRetorno ["+confEdosCtaIndBean.getCodRetorno()+"]",
					EIGlobal.NivelLog.DEBUG);
			despliegaPaginaErrorURL(confEdosCtaIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,"ConfEdosCtaIndServlet", request, response);
		}
    }
    
    /**
     * Metodo para subir al request atributos para PDF
     * @param request Request
     */
    private void reqPdf(HttpServletRequest request) {
    	request.setAttribute("segmentoDom", request.getParameter(SEQ_DOM));
		request.setAttribute("domicilio", request.getParameter("hdDomicilio"));
		request.setAttribute(EDO_CTA_DISP, "A".equals(request.getParameter(EDOCTA_DISP)) ? true : false);
		request.setAttribute(SUS_PAPERLESS, "S".equals(request.getParameter(SUSCR_PAPER)) ? true : false);
		request.setAttribute("codCliente", request.getParameter(CAD_HDCOD_CLIENTE));
		request.setAttribute(CUENTA, request.getParameter(CAD_HDCUENTA));
		request.setAttribute(CUENTA_NVA, request.getParameter(CAD_HDCUENTA));
		request.setAttribute("fechaConfigValida", request.getParameter("hdFechaConfigValida"));
    }
    
    /**
     * Metodo para realizar la configuracion de estado de cuenta individual
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    private void configEdoCta(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException{

    	ConfEdosCtaIndBO confEdosCtaIndBO=new ConfEdosCtaIndBO();
    	ConfEdosCtaIndBean confEdosCtaIndBean=null;
    	BitaAdminBean bean=new BitaAdminBean();
    	
    	String flujo=request.getParameter("flujoOp"),
    		   cuenta=FLUJO_PDF.equals(flujo) ? request.getParameter("hdCuentaNva") : request.getParameter(CAD_HDCUENTA),
    		   valida=request.getParameter("valida"), 
    		   aceptaContrato=request.getParameter("hdACAP"),
    		   mensaje="";
		boolean solVal=true, aceCon=false;
		boolean bajaPaperless;
		String[] checkboxPaperless = request.getParameterValues("suscripPaperless");
		if (checkboxPaperless != null) {
			bajaPaperless = false;
		} else {
			bajaPaperless = true;
		}
		EIGlobal.mensajePorTrace("Baja Paperless [" + bajaPaperless + "]", EIGlobal.NivelLog.DEBUG);
		
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);		
		EIGlobal.mensajePorTrace("Acepta convenio ["+aceptaContrato+"]", EIGlobal.NivelLog.DEBUG);
		
		if( validaPeticion(request, response, session, request.getSession(), valida) ) {
			  EIGlobal.mensajePorTrace("\n\n ENTRO A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
		}else {
			
			if ( aceptaContrato!=null && aceptaContrato.equalsIgnoreCase(BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS) ) {	
				EIGlobal.mensajePorTrace("Acepto contrato, bitacoriza aceptacion", EIGlobal.NivelLog.DEBUG);
				aceCon=true;
				// Se ingresa validacion para que unicamente guarde en pistas y
				// bitacora una sola vez, ya que posteriormente al ingresar tocan se invoca nuevamente
				// este metodo y puede duplicar el registro de pistas y bitacora
				if (valida == null && !bajaPaperless) {
					//[ACAP-00] Acepta Convenio de Aceptacion Paperless Individual
					bitacorizaAdmin(request, confEdosCtaIndBean, BitaConstants.EA_ACEPT_CONV_ACEPT_PAPERLESS,true);
					// [ACAP-00] Realizado guardado en bitacora de operaciones.
					bitacoriza(request, cuenta, BitaConstants.EA_CONV_ACEPT_PAPERLESS_CONFIRM, BitaConstants.CONCEPTO_ACEPTA_CONV_PAPERLESS);
				}
			}
			else {
				mensaje="No se acepto el Convenio de Aceptacion Paperless";
				EIGlobal.mensajePorTrace(mensaje, EIGlobal.NivelLog.DEBUG);
				request.setAttribute(CAD_MENSAJE_ERR,String.format(CAD_CUADRO_DIALOGO,mensaje));
				//request.setAttribute(CAD_MENSAJE_ERR,"cuadroDialogo('".concat(mensaje).concat("', 1);"));
				reqPdf(request);
				evalTemplate(EVAL_TEMPLATE, request, response);
				return;
			}	
			
			EIGlobal.mensajePorTrace("valida = ["+valida+"] aceCon = ["+aceCon+"] ", EIGlobal.NivelLog.DEBUG);
			if(valida == null && aceCon) {
				if(FLUJO_PDF.equals(flujo)) {
					//Se valida configuracion previa.... se Envia seqDomicilio y codCliente.
					String seqDomicilio=request.getParameter(SEQ_DOM);
					String codCliente=request.getParameter(CAD_HDCOD_CLIENTE);
					mensaje = confEdosCtaIndBO.consultaConfigPreviaPDF(seqDomicilio,codCliente);
					EIGlobal.mensajePorTrace(mensaje, EIGlobal.NivelLog.DEBUG);
					if(!"".equals(mensaje)) {
						request.setAttribute(CAD_MENSAJE_ERR,String.format(CAD_CUADRO_DIALOGO,mensaje));
						//request.setAttribute(CAD_MENSAJE_ERR,"cuadroDialogo('".concat(mensaje).concat("', 1);"));
						reqPdf(request);
						evalTemplate(EVAL_TEMPLATE, request, response);
						return;
					}	
					bean.setServTransTux("ODD4");
				}
				
				EIGlobal.mensajePorTrace("Solicita Validacion OTP", EIGlobal.NivelLog.DEBUG);
				solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.DISP_EDO_CTA);	
				if(session.getValidarToken() && session.getFacultad(BaseResource.FAC_VAL_OTP) &&
					session.getToken().getStatus() == 1 && solVal) {
					EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", 
							EIGlobal.NivelLog.DEBUG);
					ValidaOTP.guardaParametrosEnSession(request);
					request.getSession().removeAttribute("mensajeSession");
					ValidaOTP.mensajeOTP(request, "ConfigEdoCuenta");
					ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
				}else {
					EIGlobal.mensajePorTrace("Valida = 1", EIGlobal.NivelLog.DEBUG);
					valida = "1";
				}
			}
			
			if(valida!=null && "1".equals(valida) && aceCon) {
				EIGlobal.mensajePorTrace("Valida != null == ["+valida+"] aceCon = ["+aceCon+"]", EIGlobal.NivelLog.DEBUG);
				confEdosCtaIndBean = new ConfEdosCtaIndBean();
				//[CECI-00] Confirma Modificacion Paperless Individual
				bitacorizaAdmin(request, confEdosCtaIndBean, BitaConstants.EA_CONFIG_PDF_EDO_CTA_IND,true);
				// [CECI] Bitacora de Operaciones - Confirma Modificacion Paperless Indivual.
				bitacoriza(request, cuenta, BitaConstants.CONF_CONFIG_EDO_CUENTA_IND, BitaConstants.CONCEPTO_CONF_CONFIG_EDO_CUENTA_IND);
				
				//SOLICITA SUSCRIPCION A PAPERLESS
				String bitaC_ECP="";
				if(FLUJO_PDF.equals(flujo) && "S".equals(request.getParameter(SUS_PAPERLESS)) && 
						!"S".equals(request.getParameter(SUSCR_PAPER))) {
							EIGlobal.mensajePorTrace("Flujo PDF / SUS_PAPERLESS = \"S\" HDSUSCRIPPAPERLESS != \"S\" ", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("Llenado de bean:\n" +
									"\nsuscripPaperless: "+request.getParameter(SUS_PAPERLESS)+
									"\nseqDomicilio: "+request.getParameter(SEQ_DOM)+
									"\ncuenta: "+cuenta+
									"\ncodCliente: " + session.getUserID8() +
									"\ncodClientePersonas: "+request.getParameter(CAD_HDCOD_CLIENTE)+
									"\nhdEdoCtaDisponible: "+request.getParameter(EDOCTA_DISP)+
									"\nhdSuscripPaperless: "+request.getParameter(SUSCR_PAPER)+
									"\ncontrato: "+session.getContractNumber(),EIGlobal.NivelLog.DEBUG);							
							confEdosCtaIndBean = confEdosCtaIndBO.configuraPDF(
														request.getParameter(SUS_PAPERLESS), 
														request.getParameter(SEQ_DOM), 
														cuenta,
														session.getUserID8(),
														request.getParameter(CAD_HDCOD_CLIENTE),
														request.getParameter(EDOCTA_DISP),
														request.getParameter(SUSCR_PAPER), 
														session.getContractNumber());
							//[IECP-02] Finaliza la Inscripcion Estado de Cuenta a Paperless
							bitaC_ECP=BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS;
							// Realiza guardado de bitacora de operaciones [IECP].
							bitacoriza(request, cuenta,
									BitaConstants.EA_ALTA_INSCRIPCION_EDO_CTA_PAPERLESS,
									BitaConstants.INSCRIPCION_EDO_CUENTA_PAPERLESS);
				} 
				//QUITA SUSCRICPION A PAPERLESS]
				//else if(FLUJO_PDF.equals(flujo) && "N".equals(request.getParameter(SUS_PAPERLESS))) {
				else if (FLUJO_PDF.equals(flujo)) {
					EIGlobal.mensajePorTrace("FLUJO => PDF / SUS_PAPERLESS = \"N\" HDSUSCRIPPAPERLESS != \"N\" ", EIGlobal.NivelLog.DEBUG);
					confEdosCtaIndBean = confEdosCtaIndBO.configuraPDF(
							request.getParameter(SUS_PAPERLESS), 
							request.getParameter(SEQ_DOM), 
							cuenta, 
							session.getUserID8(),
							request.getParameter(CAD_HDCOD_CLIENTE), 
							request.getParameter(EDOCTA_DISP),
							request.getParameter(SUSCR_PAPER), 
							session.getContractNumber());
							//[BICP-00]  Finaliza la Baja Inscripcion Estado de Cuenta Paperless Individual
							bitaC_ECP=BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS;
							// Realiza guardado de bitacora de operaciones [BICP].
							bitacoriza(request, cuenta,
									BitaConstants.EDO_CUENTA_IND_BAJA_PAPERLESS,
									BitaConstants.CONCEPTO_BAJA_PAPERLESS);
				}							
				if("OK".equals(confEdosCtaIndBean.getCodRetorno()) || confEdosCtaIndBean.getCodRetorno().contains("ODA")) {
					EIGlobal.mensajePorTrace("CodRetorno = "+confEdosCtaIndBean.getCodRetorno()+" or contains "+confEdosCtaIndBean.getCodRetorno().contains("DYA"), 
							EIGlobal.NivelLog.DEBUG);
					notifica(request, session, confEdosCtaIndBean, flujo);					
					//[IECP-02]-[BICP-00]Finaliza la Inscripcion o Baja Estado de Cuenta a Paperless
					bitacorizaAdmin(request, confEdosCtaIndBean, bitaC_ECP, true);					
					mensaje = "La operaci&oacute;n se ha realizado exitosamente. Puede consultar el estatus en el m&oacute;dulo de Consulta de "
							.concat("Solicitudes de Configuraci&oacute;n con el Folio: ").concat(confEdosCtaIndBean.getFolioOp());
					request.setAttribute(CAD_MENSAJE_ERR,String.format(CAD_CUADRO_DIALOGO,mensaje));
					//request.setAttribute(CAD_MENSAJE_ERR,"cuadroDialogo('".concat(mensaje).concat("', 1);"));
					evalTemplate(URL_CONFIND, request, response);
				}else {
					EIGlobal.mensajePorTrace("ERROR ", EIGlobal.NivelLog.DEBUG);
					despliegaPaginaErrorURL(confEdosCtaIndBean.getDescRetorno(), TITULO_PANTALLA,POS_MENU,ARCHIVO_AYUDA,"ConfEdosCtaIndServlet", request, response);
				}
				ValidaOTP.reestableceParametrosEnSession(request);
			}
		}
    }
    
    /**
     * Metodo para bitacorizar en TCT
     * @param request Servlet request
     * @param numCuenta Cuenta
     * @param tipoOp Tipo de operacion
     * @param concepto Concepto
     */
	private void bitacoriza(HttpServletRequest request, String numCuenta, String tipoOp, String concepto) {
    	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));    	
    	BitacoraBO.bitacoraTCT(request, request.getSession(), tipoOp, referencia, numCuenta, "", BitaConstants.COD_ERR_PDF_XML, concepto);
    }

	/**
	 * Metodo para bitacorizar en Admin
	 * @param request request
	 * @param confEdosCtaIndBean confEdosCtaIndBean
	 * @param flujo flujo
	 * @param use use
	 */
	private void bitacorizaAdmin(HttpServletRequest request, ConfEdosCtaIndBean confEdosCtaIndBean, String flujo, boolean use) {
    	BitaAdminBean bean = new BitaAdminBean();
    	String numBit  = flujo;
    	String tipoOp  = " "; 
    	boolean token  = false;
    	String tranTux = " ";
    	String idTabla = " ";
    	String campo   = " ";
    	String tabla   = " ";
    	String datoNvo = " ";
    	String dato = " ";
    	EIGlobal.mensajePorTrace(this + "----******-----> ENTRA A REGISTRAR PISTA -->" + flujo+"<--", EIGlobal.NivelLog.INFO);
    	if (use) {
        	//Evalua flujo y asigna valores
        	if (flujo.equalsIgnoreCase(BitaConstants.EA_CONFIG_PDF_EDO_CTA_IND)) {
        		token = true;
        	}
        	if (flujo.equalsIgnoreCase(BitaConstants.EA_OP_PDF_EDO_CTA_IND)) {
        		tranTux="ODD4";
        	}
        	else if ( (flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS)) || 
        			(flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS)) ) {
        		tranTux="ODB2";
        	}
        	if ( (flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_PLESS)) || 
        			(flujo.equals(BitaConstants.CONFIG_IND_EDO_CUENTA_BPLESS)) ) {
        		tipoOp = "U";
        		idTabla = confEdosCtaIndBean.getFolioOp();
            	campo = "ID_EDO_CTA_CTRL";
            	tabla = "EWEB_EDO_CTA_DET";
            	datoNvo = confEdosCtaIndBean.getFolioOp();          	
            	            	
            	//campo = "001".equals(request.getParameter("tipoOp")) ? request.getParameter("hdCuenta") : request.getParameter("hdTarjeta"); 
            	datoNvo = "S".equals(request.getParameter(SUS_PAPERLESS)) ? "S" : "N";
            	dato = "S".equals(request.getParameter(SUS_PAPERLESS)) ? "N" : "S";
            	
            	
        	}
        	
        	//Carga Datos en el bean
        	bean.setNumBit(numBit);
        	bean.setTipoOp(tipoOp);
        	bean.setServTransTux(tranTux);
        	bean.setIdTabla(idTabla);
        	bean.setCampo(campo);
        	bean.setTabla(tabla);
        	bean.setDatoNvo(datoNvo);
        	bean.setDato(dato);
        	bean.setReferencia(100000);
        	BitacoraBO.bitacorizaAdmin(request, request.getSession(), numBit, bean, token);
    	}
    }
    
    /**
     * Metodo para realizar la notificacion
     * @param request Objeto request
     * @param session BaseResource
     * @param confEdosCtaIndBean Bean de entrada
     * @param flujo Flujo de operacion
     */
    private void notifica(HttpServletRequest request, BaseResource session, ConfEdosCtaIndBean confEdosCtaIndBean, String flujo) {
    	EmailSender emailSender = null;
		EmailDetails beanEmailDetails = null;
		
		emailSender = new EmailSender();
		beanEmailDetails = new EmailDetails();
		beanEmailDetails.setNumeroContrato(session.getContractNumber());
		beanEmailDetails.setRazonSocial(session.getNombreContrato());
		beanEmailDetails.setCuentaDestino(confEdosCtaIndBean.getCuenta());
		beanEmailDetails.setNumRef(confEdosCtaIndBean.getFolioOp());
		beanEmailDetails.setSecuencia(confEdosCtaIndBean.getSecDomicilio());
		beanEmailDetails.setEstatusFinal(String.valueOf(confEdosCtaIndBean.isSuscripPaperless()));
		beanEmailDetails.setEstatusActual(String.valueOf(confEdosCtaIndBean.isEdoCtaDisponible()));
		beanEmailDetails.setDescContrato(confEdosCtaIndBean.getDescCuenta());
		beanEmailDetails.setTipo_operacion(flujo);
				
		emailSender.sendNotificacion(request,IEnlace.EDO_CUENTA_IND,beanEmailDetails);
                //EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		
    }
	
	/**
	 * Metodo para exportar las cuentas a un archivo
	 * @param request servlet request
	 * @param response servlet response
	 */
	public void exportar(HttpServletRequest request, HttpServletResponse response) {	
		EIGlobal.mensajePorTrace(String.format("%s Inicia Metodo: EXPORTAR",LOG_TAG),NivelLog.INFO);
		
		List<?> listCuentas = (ArrayList<?>)request.getSession().getAttribute("cuentas");
		EIGlobal.mensajePorTrace(String.format("%s Se obtienen [%s] cuentas de la sesion.",LOG_TAG,(listCuentas!=null)?listCuentas.size():"0"), NivelLog.INFO);						
		
		try {
			StringBuilder tramas = new StringBuilder();		
			for(Object trama : listCuentas ){
				tramas.append(trama.toString());
				tramas.append("\n");
			}		
			response.setContentType(CONTENT_TYPE);
			response.setHeader(HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION);
			EIGlobal.mensajePorTrace(String.format("%s Se configuran los Headers :::::\n" +
					"ContentType [%s]\n" +
					"Header[%s,%s]",LOG_TAG,CONTENT_TYPE,HNAME_CONTENT_DISPOSITION,HVALUE_CONTENT_DISPOSITION),NivelLog.INFO);
			
			EIGlobal.mensajePorTrace(String.format("%s Crea objetos STREAM",LOG_TAG),NivelLog.INFO);			
			final InputStream input = new ByteArrayInputStream(tramas.toString().getBytes("UTF8"));
			final OutputStream os = response.getOutputStream();	
			
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			EIGlobal.mensajePorTrace(String.format("%s Comienza Descarga",LOG_TAG),NivelLog.INFO);
			while ((read = input.read(bytes)) != -1) {
				os.write(bytes, 0, read);
				os.flush();
				EIGlobal.mensajePorTrace(String.format("%sFlushing [%s] bytes Readed to outputStream.",
						LOG_TAG,read), NivelLog.DEBUG);
			}
			EIGlobal.mensajePorTrace(String.format("%s Cierra objetos STREAM",LOG_TAG),NivelLog.INFO);
			os.close();
			input.close();
			EIGlobal.mensajePorTrace(String.format("%s Termina Descarga",LOG_TAG),NivelLog.INFO);
		} catch (UnsupportedEncodingException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (IOException e) {
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} 
	}
	
	/**
	 * Metodo para consultar cambio de cuenta
	 * @param request servlet request
	 * @param response servlet response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public void cambiarCta(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		ConfEdosCtaIndBO confEdosCtaIndBO = new ConfEdosCtaIndBO();
		ConfEdosCtaIndBean confEdosCtaIndBean = new ConfEdosCtaIndBean();
		@SuppressWarnings("unchecked")
		List<String> listCuentas = (ArrayList<String>) request.getSession().getAttribute("cuentas");
		final String cuentaNva = request.getParameter(CUENTA_NVA)!=null ? request.getParameter(CUENTA_NVA).trim() : "";
		final String cuenta = request.getParameter(CAD_HDCUENTA);
		final String codCliente = request.getParameter(CAD_HDCOD_CLIENTE);
		
		EIGlobal.mensajePorTrace("Cuenta Nueva->" + cuentaNva, EIGlobal.NivelLog.INFO);
		
		confEdosCtaIndBean = confEdosCtaIndBO.consultarNuevaCta(cuentaNva, cuenta, codCliente, listCuentas);
		
		request.setAttribute("segmentoDom", request.getParameter(SEQ_DOM));
		request.setAttribute("domicilio", request.getParameter("hdDomicilio"));
		request.setAttribute("codCliente", codCliente);
		request.setAttribute(CUENTA, cuenta);
		request.setAttribute(CUENTA_NVA, cuentaNva);
		
		if ("S".equals(request.getParameter(SUSCR_PAPER))) {
			request.setAttribute(SUS_PAPERLESS, true);
		}else {
			request.setAttribute(SUS_PAPERLESS, false);
		}
		
		if(!"ERROR".equals(confEdosCtaIndBean.getCodRetorno())) {
			request.setAttribute(EDO_CTA_DISP, confEdosCtaIndBean.isEdoCtaDisponible());
			evalTemplate(EVAL_TEMPLATE, request, response);
		}else {
			if ("A".equals(request.getParameter(EDOCTA_DISP))) {
				request.setAttribute("edoCtaDisponible", true);
			}else {
				request.setAttribute("edoCtaDisponible", false);
			}
			request.setAttribute(CAD_MENSAJE_ERR,String.format(CAD_CUADRO_DIALOGO,confEdosCtaIndBean.getDescRetorno()));
			//request.setAttribute(CAD_MENSAJE_ERR,"cuadroDialogo('".concat(confEdosCtaIndBean.getDescRetorno()).concat("', 1);"));
			evalTemplate(EVAL_TEMPLATE, request, response);
		}
	}
	    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
		processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException Excepcion
     * @throws IOException Excepcion
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
		processRequest (request, response);
    }
}