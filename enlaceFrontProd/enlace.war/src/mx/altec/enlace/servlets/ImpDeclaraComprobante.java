
package mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class ImpDeclaraComprobante extends BaseServlet {

    //String contrato      = "";
    //String usuario       = "";
    //String clavePerfil   = "";
    //short  sucOpera      = (short)787;
	String cveBanco      = "14";

	GregorianCalendar calHoy = new GregorianCalendar();


    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		defaultAction( req, res );

	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{

		defaultAction( req, res );

	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	{
		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		String tipoComprobante = (String) req.getParameter("tipoComprobante");
			EIGlobal.mensajePorTrace( "***ImpDeclaraComprobante.class & tipoComprobante : " + tipoComprobante + " &", EIGlobal.NivelLog.INFO);
		String contrato      = "";
		String usuario       = "";
		String clavePerfil   = "";
		short  sucOpera      = (short)787;

		if ( SesionValida( req, res ) ) {
			contrato          = session.getContractNumber();
			sucOpera          = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
			usuario           = session.getUserID();
			clavePerfil       = session.getUserProfile();

			if (tipoComprobante.equals("RECEPCION")){
				generaComprobanteRecepcion( req, res,  sucOpera, cveBanco );
			}else if (tipoComprobante.equals("RECHAZO")){
				generaComprobanteRechazo( req, res,  sucOpera, cveBanco);
			}
		}
	}

	public void generaComprobanteRecepcion( HttpServletRequest req, HttpServletResponse res, short  sucOpera, String cveBanco )
		throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "***ImpDeclaraComprobante.class & generaComprobanteRecepcion &", EIGlobal.NivelLog.INFO);

		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		String strHoy   = EIGlobal.formatoFecha(calHoy, "dd/mm/aaaa");
		String decDatos = (String) req.getParameter("decDatos");

		String decRFC          = EIGlobal.BuscarToken(decDatos, '|',  1).trim();
		String decPeriodoIni   = EIGlobal.BuscarToken(decDatos, '|',  2).trim();
		String decPeriodoFin   = EIGlobal.BuscarToken(decDatos, '|',  3).trim();
		String decFolio        = EIGlobal.BuscarToken(decDatos, '|',  4).trim();
		String decImporte      = EIGlobal.BuscarToken(decDatos, '|',  5).trim();
		String pathArchivo     = EIGlobal.BuscarToken(decDatos, '|',  6).trim();
		String numCuenta       = EIGlobal.BuscarToken(decDatos, '|',  7).trim();
		String razonSocial     = EIGlobal.BuscarToken(decDatos, '|',  8).trim();
		String tipoDeclaracion = EIGlobal.BuscarToken(decDatos, '|', 12).trim();

		String strPeriodoIni = decPeriodoIni.substring(0, 2) + "/" + decPeriodoIni.substring(2, 6);
		String strPeriodoFin = decPeriodoFin.substring(0, 2) + "/" + decPeriodoFin.substring(2, 6);
		Double decDblImporte  = new Double(decImporte);

		String cuenta_razonSocial = "";
		String titTipoDeclaracion = "";

		if(decDblImporte.doubleValue() > 0){
			cuenta_razonSocial  = "\n<tr>";
			cuenta_razonSocial += "\n  <td class=\"tittabcom\" align=\"right\" width=\"111\">Cuenta:</td>";
			cuenta_razonSocial += "\n  <td class=\"textabcom\" nowrap width=\"90\">" + numCuenta + "</td>";
			cuenta_razonSocial += "\n  <td class=\"tittabcom\" nowrap width=\"137\" align=\"right\">Nombre o Raz&oacute;n Social:</td>";
			cuenta_razonSocial += "\n  <td class=\"textabcom\" nowrap width=\"128\">" + razonSocial + "</td>";
			cuenta_razonSocial += "\n</tr>";

			titTipoDeclaracion = "Tipo de Declaraci&oacute;n";
			if(tipoDeclaracion.equals("1"))
				tipoDeclaracion = "1 Provisional &nbsp;&nbsp;";
			else
				if(tipoDeclaracion.equals("2"))
					tipoDeclaracion = "2 Anual &nbsp;&nbsp;";
				else
					tipoDeclaracion = "3 Otros &nbsp;&nbsp;";
		}else{
			decFolio = "Sin folio";
			titTipoDeclaracion = "&nbsp;";
			tipoDeclaracion = "&nbsp;";
		}

		String fecha_hoy = EIGlobal.formatoFecha(calHoy, "dt, dd de mt de aaaa");

		req.setAttribute("fecha_hoy",          ObtenFecha());
		req.setAttribute("MenuPrincipal",      session.getStrMenu());
		req.setAttribute("Fecha",              ObtenFecha());
		req.setAttribute("ContUser",           ObtenContUser(req));
		req.setAttribute("numFolio",           decFolio);
		req.setAttribute("RFC",                decRFC);
		req.setAttribute("cuenta_razonSocial", cuenta_razonSocial);
		req.setAttribute("importe",            FormatoMoneda(decDblImporte.doubleValue()));
		req.setAttribute("periodoInicio",      strPeriodoIni);
		req.setAttribute("periodoFin",         strPeriodoFin);
		req.setAttribute("fechaCorta",         strHoy);
		req.setAttribute("titTipoDeclaracion", titTipoDeclaracion);
		req.setAttribute("tipoDeclaracion",    tipoDeclaracion);
		req.setAttribute("cveBanco",           cveBanco);
		req.setAttribute("sucursal",           String.valueOf(sucOpera) + " Banca Electr&oacute;nica");
		evalTemplate("/jsp/ImpDeclaraCompRecepcion.jsp", req, res);
	}


	public void generaComprobanteRechazo( HttpServletRequest req, HttpServletResponse res, short  sucOpera, String cveBanco)
		throws ServletException, IOException
	{
		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		EIGlobal.mensajePorTrace( "***ImpDeclaraComprobante.class & generaComprobanteRechazo &", EIGlobal.NivelLog.INFO);

		String strHoy   = EIGlobal.formatoFecha(calHoy, "dd/mm/aaaa");
		String decDatos = (String) req.getParameter("decDatos");
		String decRFC          = EIGlobal.BuscarToken(decDatos, '|',  1).trim();
		String decPeriodoIni   = EIGlobal.BuscarToken(decDatos, '|',  2).trim();
		String decPeriodoFin   = EIGlobal.BuscarToken(decDatos, '|',  3).trim();
		String decFolio        = EIGlobal.BuscarToken(decDatos, '|',  4).trim();
		String decImporte      = EIGlobal.BuscarToken(decDatos, '|',  5).trim();
		String pathArchivo     = EIGlobal.BuscarToken(decDatos, '|',  6).trim();
		String numCuenta       = EIGlobal.BuscarToken(decDatos, '|',  7).trim();
		String razonSocial     = EIGlobal.BuscarToken(decDatos, '|',  8).trim();
		String srvRFC          = EIGlobal.BuscarToken(decDatos, '|',  9).trim();
		String srvImporte      = EIGlobal.BuscarToken(decDatos, '|', 10).trim();
		String motivoRechazo   = EIGlobal.BuscarToken(decDatos, '|', 11).trim();
		String tipoDeclaracion = EIGlobal.BuscarToken(decDatos, '|', 12).trim();

		String strPeriodoIni = decPeriodoIni.substring(0, 2) + "/" + decPeriodoIni.substring(2, 6);
		String strPeriodoFin = decPeriodoFin.substring(0, 2) + "/" + decPeriodoFin.substring(2, 6);
		Double decDblImporte  = new Double(decImporte);

		String cuenta_razonSocial = "";

		if(decDblImporte.doubleValue() > 0){
			cuenta_razonSocial  = "\n<tr>";
			cuenta_razonSocial += "\n  <td class=\"tittabcom\" align=\"right\" nowrap>Cuenta:</td>";
			cuenta_razonSocial += "\n  <td class=\"textabcom\" nowrap>" + numCuenta + "</td>";
			cuenta_razonSocial += "\n</tr>";
			cuenta_razonSocial += "\n<tr>";
			cuenta_razonSocial += "\n  <td class=\"tittabcom\" align=\"right\" nowrap>Nombre o raz&oacute;n social:</td>";
			cuenta_razonSocial += "\n  <td class=\"textabcom\" nowrap>" + razonSocial + "</td>";
			cuenta_razonSocial += "\n</tr>";
		}
		if(tipoDeclaracion.equals("1"))
			tipoDeclaracion = "1 Provisional &nbsp;&nbsp;";
		else if(tipoDeclaracion.equals("2"))
			tipoDeclaracion = "2 Anual &nbsp;&nbsp;";
		else
			tipoDeclaracion = "3 Otros &nbsp;&nbsp;";
		String fecha_hoy = EIGlobal.formatoFecha(calHoy, "dt, dd de mt de aaaa");
//String cveBanco      = "14";

		req.setAttribute("fecha_hoy",          ObtenFecha());
		req.setAttribute("MenuPrincipal",      session.getStrMenu());
		req.setAttribute("Fecha",              ObtenFecha());
		req.setAttribute("ContUser",           ObtenContUser(req));
		req.setAttribute("numFolio",           decFolio);
		req.setAttribute("RFCPago",            srvRFC);
		req.setAttribute("RFCDeclaracion",     decRFC);
		req.setAttribute("cuenta_razonSocial", cuenta_razonSocial);
		req.setAttribute("importePago",        FormatoMoneda(srvImporte));
		req.setAttribute("importeDeclaracion", FormatoMoneda(decDblImporte.doubleValue()));
		req.setAttribute("periodoInicio",      strPeriodoIni);
		req.setAttribute("periodoFin",         strPeriodoFin);
		req.setAttribute("fechaCorta",         strHoy);
		req.setAttribute("tipoDeclaracion",    tipoDeclaracion);
		req.setAttribute("motivoRechazo",      motivoRechazo);
		req.setAttribute("cveBanco",           cveBanco);
		req.setAttribute("sucursal",           String.valueOf(sucOpera) + " Banca Electr&oacute;nica");
		evalTemplate("/jsp/ImpDeclaraCompRechazo.jsp", req, res);
	}
}