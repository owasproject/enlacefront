/*Banco Santander Mexicano
  Clase cmdpasswd  Ejecuta el cambio de password...
  @Autor:
  @version: 1.0
  fecha de creacion:
  responsable: Roberto Resendiz
  modificacion:Paulina Ventura Agustin 03/03/2001 - Cambio a WEB_RED
*/
/*Praxis
  LVO Luis ALberto Villanueva Oropeza
  28/12/2004
  Q22141
  CUANDO SE REALIZA UN CAMBIO DE PASSWORD SE PINTE EN EL LOG FECHA, HR,
  DIRECCIÓN IP(DE DONDE FUE REALIZADA
  LA TRANSACCIÓN),  LA LEYENDA: CMDPASSWD, NUMERO DE CONTRATO Y USUARIO
  EJEMPLO:
  [29/11/2004 12:02:03:9] DIRECCIÓN IP  CMDPASSWD  NUMERO DE CONTRATO  USUARIO
*/

package mx.altec.enlace.servlets;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
//import java.io.*;
import java.text.*;
import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.sql.*;


public class cmdpasswd extends BaseServlet {
	public void doGet( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {
		try {
			defaultAction( req, res );
		}
		catch(SQLException err_sql){}
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {
		try {
			defaultAction( req, res );
		}
		catch(SQLException err_sql){}
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException,SQLException {

		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		boolean sesionvalida = SesionValida( req, res );
		EIGlobal.mensajePorTrace("sesionvalida " + sesionvalida, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("sesion_cpasswd_ "+session.getFacultad(BaseResource.FAC_CAMBIO_PASSWORD ), EIGlobal.NivelLog.INFO);
		if (req.getParameter("bitacora") != null){

			try{
				EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

				int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
				String claveOperacion = BitaConstants.CAMBIO_CONTRASENA;
				String concepto = BitaConstants.CONCEPTO_CAMBIO_CONTRASENA;

				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);
				EIGlobal.mensajePorTrace( "-----------Fin bitacora->"+ coderror_bitConfirToken, EIGlobal.NivelLog.INFO);

				EIGlobal.mensajePorTrace("CambioPasswd <><><><><><>----------> Se inicia el envio de la notificacion para cambio de contraseña", EIGlobal.NivelLog.DEBUG);

				EmailSender sender = new EmailSender();

				EmailDetails detailsNotificacion = new EmailDetails();

				EIGlobal.mensajePorTrace("Cambio Password : Contrato -> "+ session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Cambio Password : Razon Social -> "+ session.getNombreContrato(), EIGlobal.NivelLog.DEBUG);

				detailsNotificacion.setNumeroContrato(session.getContractNumber());
				detailsNotificacion.setRazonSocial(session.getNombreContrato());
				detailsNotificacion.setEstatusActual("PASS0000");

				sender.sendNotificacion(req, IEnlace.CPASWD, detailsNotificacion);
				
				EIGlobal.mensajePorTrace("CmdPasswd <><><><>Inicia el cierre de dupl", EIGlobal.NivelLog.DEBUG);
			
				cierraDuplicidad(session.getUserID8());				

				EIGlobal.mensajePorTrace("CmdPasswd <><><><><><><><><><><> Termina el envio de la notificacion para cambio de contraseña", EIGlobal.NivelLog.DEBUG);
			} catch(Exception e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		} else {
			if( sesionvalida && session.getFacultad(BaseResource.FAC_CAMBIO_PASSWORD ) ) {
				CambiaPassword( req, res );
			}
			else {
				req.setAttribute("MenuPrincipal", session.getstrMenu());
				req.setAttribute("Fecha", ObtenFecha());
				req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				req.setAttribute("ContUser",ObtenContUser(req));
				evalTemplate(IEnlace.ERROR_TMPL,req,res);
			}
		}
	}

	public void CambiaPassword( HttpServletRequest req, HttpServletResponse res )
		throws IOException, ServletException/*,SQLException*/ {

		String IP = "";
		String clave="";
		String clavenva16="";
		String clavenva28="";
		//String claveconf="";
		String tramaEnviada="";
		String coderror="";
		//int contacto;
		Hashtable hsResult = null;
		HttpSession sess = req.getSession ();
		BaseResource session = (BaseResource) sess.getAttribute ("session");

		//jppm proy:2005-009-00, Cambio de 4 a 8 posiciones el password de enlace internet
		clave    = req.getParameter("clave");
		clavenva16 = req.getParameter("clavenva");

      	//200529800 Recomendaciones CNBV - Praxis - NVS - Inicio ---------------
		boolean pwdCorrecto = verificaValidezPwd(clavenva16);
		System.out.println("verificaValidezPwd = " + pwdCorrecto);
		if (pwdCorrecto == true) {
	      	pwdCorrecto = verificaPWDProhibido(session.getUserID8(), clavenva16);
			//System.out.println("verificaPWDProhibido = " + pwdCorrecto);
		}

		if (pwdCorrecto == false) {
			boolLogin = false;
				System.out.println("cambio de password no efectuados");
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute ("Encabezado", CreaEncabezadoContrato ("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a" ,"s26040h",req));

				req.setAttribute("MensajeCambioPass", "cuadroDialogo(\""+ convToHTML("Nueva Contrase&ntilde;a Inv&aacute;lida") +"\", 3);");
			req.setAttribute ( "Fecha", ObtenFecha () );
			evalTemplate ( IEnlace.CAMBIO_PASSWD, req, res );
			return;
		}

		clave = encriptaPwd(session.getUserID8(), clave, 20);
		clavenva28 = clavenva16;
		clavenva16 = encriptaPwd(session.getUserID8(), clavenva16, 10);
		clavenva28 = encriptaPwd(session.getUserID8(), clavenva28, 20);
      	//200529800 Recomendaciones CNBV - Praxis - NVS - Inicio ---------------

		tramaEnviada=session.getUserID8()+"@"+clave+"@"+clavenva16+"@"+clavenva28+"@"+session.getContractNumber()+"@";
		//System.out.println("Trama Enviada = " + tramaEnviada);
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		IP = req.getRemoteAddr();
		System.out.println("IP = >" +IP+ "<");
		try {
			hsResult = tuxGlobal.CambiaPassword(tramaEnviada);
		} catch (Exception e) {
			e.printStackTrace ();
			req.setAttribute ( "MensajeCambioPass", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);" );
			req.setAttribute ( "Fecha", ObtenFecha () );
			evalTemplate ( IEnlace.CAMBIO_PASSWD, req, res );
		}

		/* LVO Q22141, se agregregaron dos lineas
        Date fechaHrAct = new Date();
        SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");*/
        java.util.Date fechaHrAct = new java.util.Date();
        SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");

		coderror = ( String ) hsResult.get("BUFFER");
		EIGlobal.mensajePorTrace("cmdpasswd - El valor de coderror es: "+coderror , EIGlobal.NivelLog.INFO);

		//TODO BIT CU5051, se realiza el cambio de contraseña
		/*VSWF-HGG-I*/
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRASENA_REALIZA_CAMBIO_PASS);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber());
			}
			if(coderror!=null){
				if(coderror.length()>8){
					bt.setIdErr(coderror.substring(0,8));
				}else{
					bt.setIdErr(coderror.trim());
				}
			}else{
				bt.setIdErr(" ");
			}
			bt.setServTransTux("SEG_CAMBIAPWD");

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/*VSWF-HGG-F*/
		if ( (coderror == null || coderror.equals ("")) ) {
			boolLogin = false;
			req.setAttribute ( "MensajeCambioPass", "cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);" );
			req.setAttribute ( "Fecha", ObtenFecha () );
			evalTemplate ( IEnlace.CAMBIO_PASSWD, req, res );
		} else {
			String[] arrCodError=null;
			arrCodError=desentrama(coderror,'@');

			int	NumCodError = Integer.parseInt(arrCodError[0].substring(4,8));
			if (NumCodError == 0) {
			    /* LVO Q22141, se agrego una linea*/
			    /*System.out.println("["+fdate.format(fechaHrAct)+"]" +" "+ IP +"" + "CMDPASSWD "+ session.getContractNumber() +" "+session.getUserID());*/
			    System.out.println("["+fdate.format(fechaHrAct)+"] " +"IP "+IP + " CMDPASSWD "+ session.getContractNumber() +" "+session.getUserID8());

				String mensajeCambioPasswd=	"cuadroDialogo(\"Su contrase&ntilde;a ha sido cambiada<br> Inicie nuevamente su sesi&oacute;n \", 4);";
			    System.out.println("cambio de password efectuados");
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Administraci&oacute;n y Control","Cambio de contrase&ntilde;a &gt; Confirmar cambio", req));
				req.setAttribute("MensajeLogin", mensajeCambioPasswd );
				req.setAttribute("MensajeLogin01", "	cuadroDialogo(\"Su contrase&ntilde;a ha cambiado, reinicie su sesi&oacute;n\", 4); " );//cambio

                int resDup = cierraDuplicidad(session.getUserID8());
				//TODO BIT CU5051, cierra duplicidad en la tabla EWEB_VALIDA_SESION
                /*VSWF-HGG-I*/
                if (Global.USAR_BITACORAS.trim().equals("ON")){
                	BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

                	BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRASENA_CIERRA_DUPLICIDAD);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if(coderror!=null){
						if(coderror.length()>8){
							bt.setIdErr(coderror.substring(0,8));
						}else{
							bt.setIdErr(coderror.trim());
						}
					}

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
                }
				/*VSWF-HGG-F*/
                session.setSeguir(false);
                //session.guardaHash();
                //session.borraHash();
                sess.setAttribute("session", session);
                log( "logout.java::defaultAction() -> resDup: " + resDup );
                log( "logout.java::defaultAction() -> seguir: " + session.getSeguir() );

				session.setUserID8("");
				session.setPassword("");
				sess.invalidate();
				evalTemplate(IEnlace.LOGIN, req, res);
			}
			else if (NumCodError == 5) {//Usuario Bloqueado
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute("Encabezado", CreaEncabezado("Administraci&oacute;n y Control","Cambio de contrase&ntilde;a &gt; Confirmar cambio", req));
				req.setAttribute("MensajeLogin01", "cuadroDialogo(\""+ convToHTML(arrCodError[1]) +"\", 3);");

                int resDup = cierraDuplicidad(session.getUserID8());
				//TODO BIT CU5051, cierra duplicidad en la tabla EWEB_VALIDA_SESION
                /*VSWF-HGG-I*/
                if (Global.USAR_BITACORAS.trim().equals("ON")){
					BitaHelperImpl bh = new BitaHelperImpl(req, session, sess);

					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EA_CAMBIO_CONTRASENA_CIERRA_DUPLICIDAD);
					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}
					if(coderror!=null){
						if(coderror.length()>8){
							bt.setIdErr(coderror.substring(0,8));
						}else{
							bt.setIdErr(coderror.trim());
						}
					}

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
                }
    			try{
    				EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

    				int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
    				String claveOperacion = BitaConstants.CAMBIO_CONTRASENA;
    				String concepto = BitaConstants.CONCEPTO_CAMBIO_CONTRASENA;

    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);
    			} catch(Exception e) {
    				EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

    			}

				/*VSWF-HGG-f*/
                session.setSeguir(false);
                //session.guardaHash();
                //session.borraHash();
                sess.setAttribute("session", session);
                log( "logout.java::defaultAction() -> resDup: " + resDup );
                log( "logout.java::defaultAction() -> seguir: " + session.getSeguir() );

				session.setUserID8("");
				session.setPassword("");
				sess.invalidate();

				evalTemplate(IEnlace.LOGIN, req, res);
			}
			else {//Errores Genericos
				System.out.println("cambio de password no efectuados");
				req.setAttribute("newMenu", session.getFuncionesDeMenu());
				req.setAttribute ("Encabezado", CreaEncabezadoContrato ("Cambio de contrase&ntilde;a","Administraci&oacute;n y Control &gt; Cambio de contrase&ntilde;a" ,"s26040h",req));
				req.setAttribute("MensajeCambioPass", "cuadroDialogo(\""+ convToHTML(arrCodError[1]) +"\", 3);");
				evalTemplate(IEnlace.CAMBIO_PASSWD, req, res);
			}
		}
	}

	public String[] desentrama(String cadena, char caracter) {
		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		//int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++ ) {
		  if ( regtmp.charAt( i ) == caracter )
			totalCampos++;
		}
		camposTabla = new String[totalCampos];

		for (int i = 0; i < totalCampos; i++ ) {
			if ( regtmp.charAt( 0 ) == caracter ) {
				vartmp = " ";
				regtmp = regtmp.substring( 1, regtmp.length() );
			}
			else {
				vartmp = regtmp.substring( 0, regtmp.indexOf( caracter )).trim();
				if ( regtmp.indexOf( caracter ) > 0 )
					regtmp=regtmp.substring( regtmp.indexOf( caracter ) + 1,regtmp.length() );
			}
			camposTabla[i]=vartmp;
		}
		return camposTabla;
	}
}