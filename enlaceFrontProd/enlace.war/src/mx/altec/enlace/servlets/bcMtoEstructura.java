/****************************************************************************************
* Nombre: bcMtoEstructura.java
*
*
*
*
* Autor: Varios (Getronics)
* Modif: 17-03-2005 Se corrigen errores en alta de estructura (mto - getronics)
*
*
****************************************************************************************/
package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.lang.String;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.IOException;
import java.rmi.*;
import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BC_Cuenta;
import mx.altec.enlace.bo.BC_CuentaEstructura;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CuentasBO;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

import java.sql.*;


/**
** bcMtoEstructura is a blank servlet to which you add your own code.
*/
public class bcMtoEstructura extends BaseServlet
{
	// --- variables globales ----------------------------------------------------------
	//private String mensError, arregloCuentas[][];
	public BaseResource session;
	public ServletContext contexto;
//	EIGlobal EnlaceGlobal=new EIGlobal(session,contexto,this);
	EIGlobal EnlaceGlobal = new EIGlobal(session,contexto,this);
//	boolean error;
    // ---------------------------------------------------------------------------------
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

    //----------------------------------------------------------------------------------
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}



	/***********************************************************************************
	 *
	 * nombre: defaultAction
	 *
	 *
	 *
	 *
	 ***********************************************************************************/

	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{

		 HttpSession sess = req.getSession();
		 BaseResource session = null;
		 String mensError [] = new String[1];

		 int pant = 0;
		 int a    = 0;
		 String[] lc = null;

		 mensError[0] = "";

		 try
		 {

		 	session= (BaseResource) sess.getAttribute("session");
		 }
		 catch(Exception e)
		 {
			despliegaPaginaError("Error servicio no disponible por el momento","Base Cero ","Mantenimiento Estructura","s25010h",req, res );   // s26170h
		 }


		EIGlobal.mensajePorTrace("<bcMtoEstructura--> Entrando a defaultAction()", EIGlobal.NivelLog.INFO);

		StringBuffer varFechaHoy  = new StringBuffer ("");

		String parAccion    = (String)  req.getParameter("Accion"); if(parAccion == null) parAccion = "";
		String parTrama     = (String)  req.getParameter("Trama"); if(parTrama == null) parTrama = "";
		String parCuentas   = (String)  req.getParameter("Cuentas"); if(parCuentas == null) parCuentas = "";
		String[] parlineas  = (String[])req.getParameterValues("lineaCredito");
		String parpadreHija = (String)  req.getParameter("padreHija"); if(parpadreHija == null) parpadreHija = "";



		if(!SesionValida(req,res))
		{
			despliegaPaginaError("Error servicio no disponible por el momento","Base Cero ","Mantenimiento Estructura","s33010h",req, res );
			return;
		}


		varFechaHoy = new StringBuffer ("  //Fecha De Hoy");
		varFechaHoy.append ("\n  dia=new Array(");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  mes=new Array(");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  anio=new Array(");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n\n  //Fechas Seleccionadas");
		varFechaHoy.append ("\n  dia1=new Array(");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("DIA"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  mes1=new Array(");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("MES"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  anio1=new Array(");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (",");
		varFechaHoy.append (formateaFecha("ANIO"));
		varFechaHoy.append (");");
		varFechaHoy.append ("\n  Fecha=new Array('");
		varFechaHoy.append (formateaFecha("FECHA"));
		varFechaHoy.append ("','");
		varFechaHoy.append (formateaFecha("FECHA"));
		varFechaHoy.append ("');");

		String diasInhabiles = diasInhabilesAnt();
        String datesrvr = ObtenDia() + "-" + ObtenMes() + "-" + ObtenAnio();

		/*Modificacion para integracion. de las cuentas*/

		session.setModuloConsultar(IEnlace.MMant_estruc_B0);


		if(parAccion.equals("")) {
			if (!session.getFacultad(session.FAC_BASECONEST_BC)) {
				/*
				  * VSWF ARR -I
				  */
				 				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				 				try{
				 				BitaHelper bh = new BitaHelperImpl(req, session, sess);
				 				if (req.getParameter(BitaConstants.FLUJO)!=null){
				 				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				 				  }else{
				 				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				 				  }
				 				BitaTransacBean bt = new BitaTransacBean();
				 				bt = (BitaTransacBean)bh.llenarBean(bt);
				 				bt.setNumBit(BitaConstants.ER_ESQ_BASE0_MANT_ESTRUC_ENTRA);
				 				bt.setContrato(session.getContractNumber());

				 				BitaHandler.getInstance().insertBitaTransac(bt);
				 				}catch (SQLException ex){
				 					ex.printStackTrace();
				 				}catch(Exception e){

				 					e.printStackTrace();
				 				}
				 				}
				 	    		/*
				 	    		 * VSWF ARR -F
			    		 */

				parCuentas = consultaArbol(req,mensError);
			}  else
				mensError[0] = "Usted no tiene facultad para consultar estructuras.";
			}

		if(parAccion.equals("AGREGA"))  { if (session.getFacultad(session.FAC_BASAGRCTA_BC)) {parCuentas = agregaCuenta(parTrama, parCuentas,mensError);} else  mensError[0] = "Usted no tiene facultad para agregar cuentas.";}

		if(parAccion.equals("ELIMINA"))
		{
			if (session.getFacultad(session.FAC_BASELICTA_BC) || session.getFacultad(session.FAC_BASENVEST_BC) )
			{


				parCuentas = eliminaCuenta(parTrama, parCuentas);

				EIGlobal.mensajePorTrace("bcMtoEstructura-->ELIMINA-->:"+ parCuentas, EIGlobal.NivelLog.INFO);

				if (parCuentas.equals("") )
				{

					borraArbol(req,mensError);
					parCuentas = consultaArbol(req,mensError);


				}

				else
				{
					altaArbol(parCuentas,req,mensError);
					parCuentas = consultaArbol(req,mensError);
				}
			}
			else
			{
				mensError[0] = "Usted no tiene facultad para eliminar registros.";
				pant = 3;
			}

		}
		if(parAccion.equals("BORRA_ARBOL")){ if (session.getFacultad(session.FAC_BASBOREST_BC))  {borraArbol(req,mensError); parCuentas = consultaArbol(req,mensError);} else mensError[0] = "Usted no tiene facultad para Consultar el ~�rbol.";}
		if(parAccion.equals("ALTA_ARBOL")) { if (session.getFacultad(session.FAC_BASENVEST_BC))  {altaArbol(parCuentas,req,mensError); } else mensError[0] = "Usted no tiene facultad para enviar estructuras.";}

		if(parAccion.equals("MODIFICA"))
		{
			parTrama = tramaCuenta(parTrama, parCuentas);
			pant=1;
		}
		if(parAccion.equals("MODIFICA_MODIFICA")) { parCuentas = tramaModificaCuenta(parTrama, parCuentas, mensError); pant=1;
		/*
		 * VSWF ARR -I   Modificacion
		 */
		String tramaBit = parTrama.substring(1);
		String cuenta = tramaBit.substring(0,parTrama.indexOf("|"));
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){

			try{
				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit("ERBM04");
				bt.setContrato(session.getContractNumber());
				bt.setCctaOrig(cuenta) ;
				BitaHandler.getInstance().insertBitaTransac(bt);
			   }catch(SQLException e){
				e.printStackTrace();
			   }catch(Exception e){
				e.printStackTrace();
			}

		}
	/*
	 * VSWF ARR -F
	     */

		}
		if(parAccion.equals("MODIFICA_REGRESA")) {;/*modificaArbol(parCuentas); pant= 1;*/}


		if(parAccion.equals("REGRESA"))
		{
			EIGlobal.mensajePorTrace("bcMtoEstructura-->REGRESANDO-->:"+ parCuentas, EIGlobal.NivelLog.INFO);

			if (session.getFacultad(session.FAC_BASENVEST_BC))
			{

				altaArbol(parCuentas,req,mensError);
				parCuentas = consultaArbol(req,mensError);
			}
			else
			{
				mensError[0] = "Usted no tiene facultad para enviar estructuras.";
				pant = 3;
			}

		}


		if(mensError[0].length()>=3) if(mensError[0].substring(0,3).equals("X-P")) pant = 3;

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de estructura para Base Cero","Tesorer&iacute;a &gt; Esquema Base Cero &gt; Mantenimiento de estructura","s33010h",req));
		req.setAttribute("Cuentas",parCuentas);
		req.setAttribute("Mensaje",mensError[0]);
		req.setAttribute("Trama",parTrama);
		req.setAttribute("Fecha",ObtenFecha());
		req.setAttribute("Movfechas",datesrvr);
		req.setAttribute("FechaAyer",formateaFecha("FECHA"));
		req.setAttribute("FechaPrimero","01/"+formateaFecha("MES2")+"/"+formateaFecha("ANIO"));
		req.setAttribute("VarFechaHoy",varFechaHoy.toString());
		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
		req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
		req.setAttribute("LineaCredito",ctasLineaCredito());




		if (diasInhabiles.trim().length()==0)
		{
			pant = 3;
			mensError[0] = "X-PServicio no disponible por el momento";
		}


		switch(pant)
			{
			case 0: evalTemplate("/jsp/bcMtoEstructura.jsp",req,res); break;
			case 1: String cta="";

				    if(parTrama.indexOf("|")>=0)
				    {
					  cta=parTrama.substring(1,parTrama.indexOf("|"));
				 	}
					else
					{
						cta="";
					}

					System.out.println("Cuenta: "+cta+" trama: "+parTrama);

				    req.setAttribute("lineasCredito",generaLineasParaCombo(cta,req,res));
				    evalTemplate("/jsp/bcEditaEstructura.jsp?ls=parlineas",req,res);

				    break;

			case 3: despliegaPaginaError(mensError[0].substring(3),"Consulta y mantenimiento de Estructura","Tesorer&iacute;a Santander &gt; Mantenimiento de estructuras &gt; ", req, res); break;

			}

		}

	// --- Agrega una cuenta a la trama ------------------------------------------------
	private String agregaCuenta(String trama, String tramaCuentas, String [] mensError)
	{
		System.out.println("Valor de trama y  tramaCuentas>>" + trama  +"   <<<<>>>>" + tramaCuentas);
		String cuenta = trama.substring(1,trama.indexOf("|"));

		 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> tramaCuentas, y trama"+ tramaCuentas + " -- " + cuenta, EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> ---yyy trama---" + trama, EIGlobal.NivelLog.INFO);

		if(existeCuenta(cuenta,tramaCuentas))
		{
			mensError[0] = "La cuenta ya existe dentro del arbol, no puede agregarse nuevamente.";
			return tramaCuentas;
		}


		else
		{
			return ordenaArbol(trama + tramaCuentas);
		}
	}

	// --- Elimina una cuenta de la trama de cuentas -----------------------------------
	private String eliminaCuenta(String cuenta, String tramaCuentas)
	{
		int a;
		Vector cuentas;
		boolean consistente;
		BC_CuentaEstructura cta;


		cuentas = creaCuentas(tramaCuentas);

		for(a=0;a<cuentas.size();a++)
		{
			cta = (BC_CuentaEstructura)cuentas.get(a);

			if( cta.getNumCta().equals(cuenta) )
				cuentas.remove(cta);
		}
		do
		{
			consistente = true;
			for(a=0;a<cuentas.size();a++)
			{
				cta = (BC_CuentaEstructura)cuentas.get(a);

				if(cta.getPadre() != null)
				{
					if(cuentas.indexOf(cta.getPadre()) == -1)
						{
							consistente = false;
							cuentas.remove(cta);
						}
				}

			}
		}
		while(!consistente);

		return creaTrama(cuentas);
	}

	// --- Indica si una cuenta se encuentra dentro de una trama -----------------------
	private boolean existeCuenta(String cuenta, String tramaCuentas)
		{
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		boolean existe = false;
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) {existe = true; break;}
				}
			}
		catch(Exception e)
			{EIGlobal.mensajePorTrace("<DEGUG bcMtoEstructura> - Error en existeCuenta(): " + e.toString(), EIGlobal.NivelLog.INFO); }
		finally
			{return existe;}
		}



	// --- Ordena las cuentas segon el arbol -------------------------------------------
	private String ordenaArbol(String tramaEntrada)
		{
		StringBuffer tramaSalida = new StringBuffer ("");
		Vector cuentas, ctasOrden;
		BC_CuentaEstructura ctaAux;
		int a, b;

		b = 1;
		cuentas = creaCuentas(tramaEntrada);
		ctasOrden = new Vector();
		while(cuentas.size() > 0)
			{
			for(a=0;a<cuentas.size();a++)
				{
				ctaAux = ((BC_CuentaEstructura)cuentas.get(a));
				if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
				}
			b++;
			}
		cuentas = ctasOrden;
		ctasOrden = new Vector();
		for(a=0;a<cuentas.size();a++)
			{
			ctaAux = ((BC_CuentaEstructura)cuentas.get(a));
			if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
			}
		while(cuentas.size() > 0)
			{
			ctaAux = ((BC_CuentaEstructura)cuentas.get(0));
			ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
			cuentas.remove(ctaAux);
			}
		cuentas = ctasOrden;
		tramaSalida = new StringBuffer ("");
		for(a=0;a<cuentas.size();a++) {tramaSalida.append (((BC_CuentaEstructura)cuentas.get(a)).trama());}

		return tramaSalida.toString();
		}

	// --- Regresa un vector de cuentas a partir de una trama --------------------------
	public Vector creaCuentas(String tramaCuentas)
		{
		Vector cuentas = null;
		StringTokenizer tokens;
		BC_CuentaEstructura ctaAux1, ctaAux2;
		String tramasx;
	    String mensError [] = new String[1];
		mensError[0]="";
		int a, b;
		System.out.println("Entrando a crear las cuentas con trama: "+tramaCuentas);

		try
			{
			cuentas = new Vector();
			tokens = new StringTokenizer(tramaCuentas,"@");

			while(tokens.hasMoreTokens())
				{
				tramasx = tokens.nextToken().trim();
				System.out.println("Enviando las cuentas");
				System.out.println("LOS TOKENS " + tramasx);
				cuentas.add(BC_CuentaEstructura.creaCuentaEstructura("@" + tramasx));
				System.out.println("Se agrego: "+cuentas.get(0));
				for(a=0;a<cuentas.size();a++)
					{
					ctaAux1 = (BC_CuentaEstructura)cuentas.get(a);
					System.out.println("ctaAux: "+ctaAux1);
					if(!ctaAux1.posiblePadre.equals(""))
						for(b=0;b<cuentas.size();b++)
							{
							ctaAux2 = (BC_CuentaEstructura)cuentas.get(b);
							if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
								{
								ctaAux1.setPadre(ctaAux2);
								b=cuentas.size();
								}
							}
					}
				}
			}
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en creaCuentras(): " + e.toString(), EIGlobal.NivelLog.INFO);
			cuentas = new Vector();
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
		finally
			{return cuentas;}
		}

	// --- Regresa una trama con las cuentas de l�nea de cr'dito -----------------------
	private String ctasLineaCredito()
		{
		return "@400001|Linea 1@400002|Linea 2@400003|Linea 3@400004|Linea 4@400005|Linea 5";
		}

	//  Esta  funcion tiene la finalidad de consultar por tuxedo y traer el archivo y asi mismo
	//  desentramar el archivo y cargarlo al arbol de estructuras.
	//  Regresa una trama y lo carga al �arbol local
	//  Modifiacacion para integracion (Base Cero)

	public String consultaArbol(HttpServletRequest request,String [] mensError)
		{
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Entrando a consultaArbol()...", EIGlobal.NivelLog.INFO);
		String trama, tramaBC;
		String retornoTuxedo, aviso, tramaD, tramaC, avisoD, avisoDes;
		Vector cuentas;
		BC_CuentaEstructura cuenta;
		int a, b;
		retornoTuxedo = enviaTuxedo("CONSULTA", request, mensError);  //"/tmp/1001851.tmpMR.txt"; //enviaTuxedo("CONSULTA", request);
     	EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> **Este es el retorno de Tux:**" + retornoTuxedo, EIGlobal.NivelLog.DEBUG);
		tramaBC = leeArchivo(retornoTuxedo, request);
		if(tramaBC.equals("ERROR"))
			{ mensError[0] = "Su transacci&oacute;n no puede ser atendida en este momento"; }
		System.out.println("TRAMA DEL REGRESO DE LEER TUXEDO---->>>" + tramaBC);
//			if(trama.equals("TRAMA_@")) trama = "TRAMA_OK@";
		aviso = tramaBC.substring(0,tramaBC.indexOf("@"));
		tramaBC = tramaBC.substring(tramaBC.indexOf("@"));
		System.out.println("------------tramaBC" + tramaBC);
		if(!aviso.equals("TRAMA_OK"))  //avisoD
		{
			mensError[0] = "";
			 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en consultaArbol() -- tramas devueltas:\n  disp: " + tramaBC, EIGlobal.NivelLog.INFO);
			return "";
		}


System.out.println("TRAMABC : TRAMABC"+ tramaBC);
return "" + tramaBC.toString();

		}
	// --- Borra el arbol de la base de datos a trav's de Tuxedo ----------------------------
	private void borraArbol(HttpServletRequest request, String [] mensError)
		{
		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Entrando a borraArbol()", EIGlobal.NivelLog.INFO);
		String resultado = enviaTuxedo("BAJA", request,mensError);
		if(resultado.substring(0,8).equals("BASE0018"))
			{mensError[0] = resultado.substring(16,resultado.length());}
		if(resultado.substring(16,24).equals("BASE0000"))
			{mensError[0] = "La estructura fue borrada con &eacute;xito.";}
		else
			{mensError[0] = resultado.substring(16,resultado.length());} //25 -1
		 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Saliendo de borraArbol()", EIGlobal.NivelLog.INFO);
		}

	// --- Indica si una cuenta es hoja o padre ----------------------------------------
	public boolean esHoja(BC_CuentaEstructura cuenta, Vector cuentas)
		{
		BC_Cuenta ctaActual;
		boolean hoja;

		hoja = true;
		for(int a=0;a<cuentas.size() && hoja;a++)
			{
			ctaActual = ((BC_CuentaEstructura)cuentas.get(a)).getPadre();
			if(ctaActual != null) if(ctaActual.equals(cuenta)) hoja = false;
			}
		return hoja;
		}
	/*******************************************************************************/ //  MCL...
	String formateaFecha(String tipo){
		StringBuffer strFecha = new StringBuffer ("");
		java.util.Date Hoy = new java.util.Date();
		GregorianCalendar Cal = new GregorianCalendar();
		Cal.setTime(Hoy);
		do{
			Cal.add(Calendar.DATE,-1);
		}while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);
		if(tipo.equals("FECHA")){
			if(Cal.get(Calendar.DATE) <= 9)
			{strFecha.append ("0" + Cal.get(Calendar.DATE) + "/");}
			else
			{strFecha.append (Cal.get(Calendar.DATE) + "/");}
			if(Cal.get(Calendar.MONTH)+1 <= 9)
			{strFecha.append ("0" + (Cal.get(Calendar.MONTH)+1) + "/");}
			else
			{strFecha.append ((Cal.get(Calendar.MONTH)+1) + "/");
			strFecha.append (Cal.get(Calendar.YEAR));}
		}
		if(tipo.equals("MES2")){
			if(Cal.get(Calendar.MONTH)+1 <= 9)
			{strFecha.append ("0" + (Cal.get(Calendar.MONTH)+1));}
         	else
			{strFecha.append((Cal.get(Calendar.MONTH)+1));}
	    }
		if(tipo.equals("DIA"))
		{strFecha.append (Cal.get(Calendar.DATE));}
		if(tipo.equals("MES"))
		{strFecha.append (Cal.get(Calendar.MONTH)+1);}
		if(tipo.equals("ANIO"))
		{strFecha.append (Cal.get(Calendar.YEAR));}
		return strFecha.toString();
    }

	/************************** Arma dias inhabiles   ***********************************/

	String armaDiasInhabilesJS()
	{
		StringBuffer resultado = new StringBuffer ("");
		Vector diasInhabiles;

		int indice = 0;

		diasInhabiles = CargarDias(1);
		if(diasInhabiles!=null)
			{
			resultado = new StringBuffer (diasInhabiles.elementAt(indice).toString());
			for(indice = 1; indice<diasInhabiles.size(); indice++)
  				resultado.append (", " + diasInhabiles.elementAt(indice).toString());
			}
		return resultado.toString();
	}

/***********************************Carga Dias*********************************************/
	public Vector CargarDias(int formato)
	{
		String sqlDias;
		boolean    estado;
		Vector diasInhabiles = new Vector();
		Connection Conexion;
		PreparedStatement qrDias;
		ResultSet rsDias;

		sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
		try
		{
			Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			if(Conexion!=null)
			{
				qrDias = Conexion.prepareStatement( sqlDias );

				if(qrDias!=null)
				{
        			rsDias = qrDias.executeQuery();

					if(rsDias!=null)
					{
						rsDias.next();
						do
						{
							String dia  = rsDias.getString(1);
							String mes  = rsDias.getString(2);
							String anio = rsDias.getString(3);

							if(formato == 0)
							{
								dia=Integer.toString( Integer.parseInt(dia) );
								mes=Integer.toString( Integer.parseInt(mes) );
								anio=Integer.toString( Integer.parseInt(anio) );
							}

							String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
							diasInhabiles.addElement(fecha);
						}while(rsDias.next());

						rsDias.close();

					}
					else
					{
			  			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
					}
				}
				else
				{
       	  			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> puedo crear Query.", EIGlobal.NivelLog.ERROR);
				}
			}
			else
			{
       			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> No se puedo crear la conexion.", EIGlobal.NivelLog.ERROR);
			}

			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Cerrando conexion a ORACLE", EIGlobal.NivelLog.INFO);
			Conexion.close();
		}
		catch(SQLException sqle)
		{
      		EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> efectuo la sig. excepcion: ", EIGlobal.NivelLog.ERROR);
      		EIGlobal.mensajePorTrace( sqle.getMessage() , EIGlobal.NivelLog.ERROR);
		}

	return(diasInhabiles);
}
// ------------------------------------------------------------------------------------------

	// --- Regresa la trama de la cuenta indicada --------------------------------------
	private String tramaCuenta(String cuenta, String tramaCuentas)
		{
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String tramaFinal="";
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) tramaFinal = "@" + tramaCta;
				}
			}
		catch(Exception e)
			{ EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en tramaCuenta(): " + e.toString(), EIGlobal.NivelLog.INFO); }
		finally
			{return tramaFinal;}
		}

// --- Regresa una trama a partir de un vector de cuentas --------------------------
	public String creaTrama(Vector vectorCuentas)
		{
		StringBuffer regreso = new StringBuffer("");
		for(int a=0;a<vectorCuentas.size();a++)
			{regreso.append(((BC_CuentaEstructura)vectorCuentas.get(a)).trama());}
		return regreso.toString();
		}


	// --- Edita la trama con la nueva informaci�n -------------------------------------
	private String tramaModificaCuenta(String tramaCuenta, String tramaCuentas, String [] mensError)
		{
		StringTokenizer tokens;
		String tramaCta;
		String numCta;
		String cuenta;
		StringBuffer tramaFinal = new StringBuffer ("");

		tramaCuenta = tramaCuenta.substring(1);
		cuenta = tramaCuenta.substring(0,tramaCuenta.indexOf("|"));
		try
			{
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				tramaCta = tokens.nextToken();
				numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
				if(numCta.equals(cuenta)) tramaCta = tramaCuenta;
					{tramaFinal.append ("@" + tramaCta);}
				}
			mensError[0] = "Los datos fueron modificados";
			}
		catch(Exception e)
			{EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en tramaModificaCuenta(): " + e.toString(), EIGlobal.NivelLog.INFO); }
		finally
			{return tramaFinal.toString();}
		}


// --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
	// tramas tomadas de la aplicacin de Visual Basic   Provisional
	// --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
	private String enviaTuxedo(String tipo, HttpServletRequest request, String [] mensError)
		{
		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer trama = new StringBuffer ("");
		String tipoOper="";
		String arch="";
		String nuBit="";
		EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Entrando a enviaTuxedo bcMtoEstructura", EIGlobal.NivelLog.INFO);
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		boolean criterio = false;

			// Implica Archivo Remoto
			if(tipo.equals("ALTA"))
				{
				if(archRemoto.copiaLocalARemoto(session.getUserID8()+".tmp"))
					 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Copia remota ralizada (local a Tuxedo)", EIGlobal.NivelLog.DEBUG);
				else
					{
					 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> copia No realizada (local a Tuxedo)", EIGlobal.NivelLog.DEBUG);
					mensError[0] = "Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
					}
				}




		if(tipo.equals("ALTA"))
			{trama = new StringBuffer ("2EWEB|"+session.getUserID8()+"|B0MT|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"); //+IEnlace.DOWNLOAD_PATH+session.getUserID()+".tmp@";
		 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> LA TRAMA EN LA ALTA A ENVIAR ES :" +  trama, EIGlobal.NivelLog.DEBUG);
		 tipoOper="B0MT";
		 nuBit ="ERBM02";
		 }
		else if(tipo.equals("BAJA"))
			{trama = new StringBuffer ("1EWEB|"+session.getUserID8()+"|B0BA|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+ "|");
			 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> LA TRAMA EN LA BAJA ES:" + trama, EIGlobal.NivelLog.DEBUG);
				tipoOper="B0BA";
				nuBit ="ERBM03";
				}
		else if(tipo.equals("CONSULTA"))
			{trama = new StringBuffer ("2EWEB|"+session.getUserID8()+"|B0AC|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+ "|");
			 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> LA TRAMA EN LA CONSULTA A ENVIAR ES : " + trama, EIGlobal.NivelLog.DEBUG);
			tipoOper="B0AC";
		}
		else
			{trama = new StringBuffer (session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tmpR@" + "|6@|" + tipo + "| | |");
			criterio = true;
			arch=session.getUserID8()+".tmpR";
			}
		try
			{
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			if(!criterio) {ht = tuxedo.web_red(trama.toString());} else {ht = tuxedo.cuentas_modulo(trama.toString());}
			trama = new StringBuffer ((String)ht.get("BUFFER"));
			 EIGlobal.mensajePorTrace("<DEBUG bcToEstructura.java > tuxdo regresa: " + trama, EIGlobal.NivelLog.INFO);

			if(criterio && !((String)ht.get("COD_ERROR")).equals("BASE0000"))
				{
					criterio = false;
					mensError[0] = "Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
					EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en consdulta a Tuxedo", EIGlobal.NivelLog.DEBUG);
				}

			if(criterio)  //nueva Mod   04-07-2002
				if (!((String)ht.get("COD_ERROR")).equals("BASE0000"))
					{
					criterio = false;
					trama = new StringBuffer ((String)ht.get("COD_ERROR"));
					//setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
					//debug("Error en consulta a Tuxedo");
					}
				else
				{
					trama = new StringBuffer (session.getUserID8() + ".ctas");}   //nueva Mod 04-07-2002

			//trama = session.getUserID() + ".ctas";
// ----------------------------------------------------- Secci�n de archivo remoto -----
			if(tipo.equals("CONSULTA") || criterio)
				{
					{trama = new StringBuffer (trama.substring(trama.toString().lastIndexOf('/')+1,trama.length()));}
				if(archRemoto.copia(trama.toString()))
					{
					 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.DEBUG);
					trama = new StringBuffer (Global.DIRECTORIO_LOCAL + "/" +trama);
					}
				else
					{
					 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.DEBUG);
					mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
					}
				}

				/*
				 * VSWF ARR -I
				 */
				 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
					 if(!tipo.equals("CONSULTA")){
					try{
						String coderror =((String)ht.get("COD_ERROR"));
						BitaHelper bh = new BitaHelperImpl(request, session,request.getSession());
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(nuBit);
						bt.setContrato(session.getContractNumber());
						if(coderror!=null){
							if(coderror.substring(0,2).equals("OK")){
								bt.setIdErr(tipoOper + "0000");
							}else if(coderror.length()>8){
								bt.setIdErr(coderror.substring(0,8));
							}else{
								bt.setIdErr(coderror);
							}
						}
						if(tipoOper!=null && !tipoOper.equals(""))
							bt.setServTransTux(tipoOper);
						if(arch!=null && !arch.equals(""))
							bt.setNombreArchivo(arch);
						BitaHandler.getInstance().insertBitaTransac(bt);
					   }catch(SQLException e){
						e.printStackTrace();
					   }catch(Exception e){
						e.printStackTrace();
					}
					}
				}
			/*
			 * VSWF ARR -F
   		     */
// -------------------------------------------------------------------------------------
			}

		catch(Exception e)
			{
			 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error intentando conectar a Tuxedo: " + e.toString() + " <--> " + e.getMessage() + " <---> " + e.getLocalizedMessage(), EIGlobal.NivelLog.INFO);
			mensError[0] = "X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";
			}
		finally
			{return trama.toString();}
		}


/********************************************************************************************
*
* Nombre     : altaArbol
* Descripcion: Da de alta la estructura del arbol en la Base de Datos
*
*
*
*
********************************************************************************************/
private void altaArbol(String tramaCuentas, HttpServletRequest request, String [] mensError)
{
		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace(" <altaArbol-->bcMtoEstructura> ENTRANDO:"+tramaCuentas, EIGlobal.NivelLog.INFO);


		String resultadoTuxedo,
			   tramaArchivo;


		ArchivoRemoto archRemoto = new ArchivoRemoto();

		boolean fueCreadoArchivo;

	try {




		if(!tramaLista(tramaCuentas))
			{

				mensError[0] = "No se puede guardar la estructura, Existen cuentas a las que se deben agregar datos";

			}
		else if(hayHojasEnPrimerNivel(creaCuentas(tramaCuentas)))
			{

				mensError[0] = "No se puede guardar la estructura, no puede haber cuentas de primer nivel sin hijas";

			}
		else
			{
				fueCreadoArchivo = armaElArchivo(tramaCuentas, request);

				EIGlobal.mensajePorTrace("<altaArbol-->bcMtoEstructura> Valor de ********fueCreadoArchivo :"+ fueCreadoArchivo, EIGlobal.NivelLog.INFO);

				if(!fueCreadoArchivo)
					{

						mensError[0] = "Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde";

					}
				else
					{
						EIGlobal.mensajePorTrace("<altaArbol-->bcMtoEstructura> Entrando a la Peticion de tuxedo enviaTuxedo('ALTA',request)", EIGlobal.NivelLog.DEBUG);

						resultadoTuxedo = enviaTuxedo("ALTA", request, mensError);


						if(archRemoto.copia(session.getUserID8()))
						{


							tramaArchivo = leeArchivoAlta(resultadoTuxedo);

						}
						else
						{

							EIGlobal.mensajePorTrace("<altaArbol-->bcMtoEstructura> ALTA - Copia No realizada (local a Tuxedo)", EIGlobal.NivelLog.DEBUG);
							tramaArchivo="ERROR           BASE0001No se pudo efectuar la copia remota.";

						 }

						EIGlobal.mensajePorTrace("<altaArbol-->bcMtoEstructura> ****VALOR RESULTADO DE tramaArchivo***: " +tramaArchivo, EIGlobal.NivelLog.DEBUG);



						if(tramaArchivo.length()>24)
						{

							if(tramaArchivo.substring(16,24).equals("BASE0000"))
							{
								mensError[0] = "La Estructura Actual fue guardada con &eacute;xito.";

							}
							else												//	No se pudo hacer la alta correctamente.
							{
								if(tramaArchivo.length()>52)
								{
									System.out.println("ERROR EN ALTA DE ESTRUCTURA");

									if(tramaArchivo.indexOf('|') > 0 )
									{
										tramaArchivo = tramaArchivo.substring( tramaArchivo.indexOf('|') + 1, tramaArchivo.length() );
										tramaArchivo = tramaArchivo.substring( tramaArchivo.indexOf('|') + 1, tramaArchivo.length() );
										mensError[0] = tramaArchivo.substring( 0, tramaArchivo.indexOf('|')   );
									}
									else
									{

									  	mensError[0]=tramaArchivo.substring(24,51);
								 	}

								}
							}

						}


					}   //fin del else
			}		//fin del else


			}
			catch(Exception e)
			{
				EIGlobal.mensajePorTrace("<altaArbol-->bcMtoEstructura> Ocurrio la siguiente Excepcion en alta arbol" + e.toString(), EIGlobal.NivelLog.INFO);
			}
		}			//fin funcion



// --- Construye el archivo para enviar a Tuxedo la estructura local -----------------------
// las tramas que se envian fueron tomadas de la Aplicaci�n de Visual B.
	public boolean armaElArchivo(String tramaCuentas, HttpServletRequest request)
		{
		HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace(" <DEBUG bcMtoEstructura> Entro a armaElArchivo bcMtoEstructura- ", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Valor de la trama de entrada -- > " + tramaCuentas, EIGlobal.NivelLog.INFO);
		boolean nohayError = true;
		FileWriter archivo;
		StringBuffer buffer, tramaHijoPadre;
		Vector cuentas;
		int numHijoPadre, a, b,valornivel, valortotal;
		BC_CuentaEstructura cuenta;
		String techoPre;
		String hora1="";
		String hora2="";

		try
			{
			cuentas = creaCuentas(tramaCuentas);
			buffer = new StringBuffer("");
			numHijoPadre = 0;
			tramaHijoPadre = new StringBuffer("");
			for(b=0;b<cuentas.size();b++)
			{
				cuenta = (BC_CuentaEstructura)cuentas.get(b);
				numHijoPadre++;
				valornivel = cuenta.nivel();
				valortotal = cuentas.size();
				if(b>=1){
					hora1	   = cuenta.getHorarioOper1().trim();	//09:30
					System.out.println("HORA1 " +hora1);
					hora1	   = hora1.substring(0,2) + hora1.substring(3, 3);
					hora2	   = cuenta.getHorarioOper2().trim();
					System.out.println("HORA2 " +hora2);
					hora2	   = hora2.substring(0,2) + hora2.substring(3, 3);
				}


				techoPre = cuenta.getTechopre().trim();
				if(techoPre.equals("") || techoPre.equals(null) || techoPre.equals("$")) {techoPre = "0";}

				if(valornivel == 1){
					//verificamos todas las cuentas padre posibles
					tramaHijoPadre.append(session.getContractNumber());
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (cuenta.getNumCta());
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (cuenta.getLineaCred());
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (valortotal-1);
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (cuenta.nivel()-1);
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (session.getUserID8());
					tramaHijoPadre.append ("|");
				}
				else
				{	//verificamos todas las cuentas hijas posibles
					techoPre = cuenta.getTechopre().trim();
					if(techoPre.equals("") || techoPre.equals(null) || techoPre.equals("$")) {techoPre = "0";}

					tramaHijoPadre.append(cuenta.getNumCta());
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (cuenta.getLineaCred());
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (cuenta.nivel()-1);
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (techoPre);
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (hora1);
					tramaHijoPadre.append ("|");
					tramaHijoPadre.append (hora2);
					tramaHijoPadre.append ("|");
				}
			}
			buffer.append("" + tramaHijoPadre);

			buffer.append("\n");
			archivo = new FileWriter(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tmp");
			archivo.write("" + buffer);
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> *****Escribio al Archivo*****  1001851.tmp", EIGlobal.NivelLog.INFO);
			archivo.close();
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> *****se cerr� el archivo *****  1001851.tmp", EIGlobal.NivelLog.INFO);

			}
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> *****Si entre por una exception y hubo algun error 1001851.tmp", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error intentando construir archivo: " + e.toString() + " <---> " + e.getMessage(), EIGlobal.NivelLog.INFO);
			nohayError = false;
			}
		finally
			{return nohayError;}

		}

		// --- Lee el archivo de tuxedo y regresa una trama de cuentas
		// --- En esta parte solamente se lee la cadena que regresa tuxedo
	private String leeArchivoAlta(String nombreArchivo)
		{
			EIGlobal.mensajePorTrace("<bcMtoEstructura-->Llego a leeArchivoAlta", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("<bcMtoEstructura-->nombreArchivo de la trama: " + nombreArchivo , EIGlobal.NivelLog.INFO);

			StringBuffer Trama = new StringBuffer("");
			FileReader archivo;
			int car;

			try
				{
					// Se lee del archivo al StringBuffer
					archivo = new FileReader(nombreArchivo);
					car = 0;

					while(car != -1)
					{
						car = archivo.read();
						Trama.append("" + (char)car);
					}

					EIGlobal.mensajePorTrace("<bcMtoEstructura-->leeArchivoAlta> valor del contenido del archivo de tuxedo:-->" + Trama.toString(), EIGlobal.NivelLog.INFO);

					archivo.close();
				}
			catch(Exception e)
				{
					EIGlobal.mensajePorTrace("<bcMtoEstructura-->leeArchivoAlta> Error en bcMtoEstructura.leeArchivoAlta() ", EIGlobal.NivelLog.INFO);
				}
			finally
				{
					EIGlobal.mensajePorTrace("<bcMtoEstructura-->leeArchivoAlta> Saliendo de leeArchivoAlta", EIGlobal.NivelLog.INFO);
					return Trama.toString().trim();
				}
		}


// --- Indica si las cuentas de una estructura tienen todos sus datos capturados ---
	private boolean tramaLista(String trama)
	{
		Vector cuentas = creaCuentas(trama);
		BC_CuentaEstructura cuenta;
		boolean retorno = true;
		int num;

		for(num=0;num<cuentas.size() && retorno;num++)
		{
			cuenta = (BC_CuentaEstructura)cuentas.get(num);

		}
		return retorno;
	}


// --- Indica si no hay hojas en el nivel 1 del arbol ------------------------------
	private boolean hayHojasEnPrimerNivel(Vector cuentas)
	{
		BC_CuentaEstructura ctaActual;
		boolean hoja;

		hoja = false;
		for(int a=0;a<cuentas.size() && !hoja;a++)
		{
			ctaActual = (BC_CuentaEstructura)cuentas.get(a);
			if(ctaActual.nivel() == 1 && esHoja(ctaActual,cuentas)) hoja = true;
		}
		return hoja;
	}



	private String leeArchivo(String nombreArchivo, HttpServletRequest req)
		{

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Entre a leeArchivo ", EIGlobal.NivelLog.INFO);
		StringBuffer tramaTux = new StringBuffer(""), tramaRegreso = new StringBuffer("");
		StringTokenizer tokens;
		StringTokenizer tokenInterno;

		String  strAux, lineaTem, estructuras;  //tipoArbol,
		Vector cuentas;
		BufferedReader brArchivo;
		BC_CuentaEstructura cuenta;
		int car, a, b,contador = 0, longitud;
		StringTokenizer tramaHijos;
		StringTokenizer tramaHijitos;
		StringTokenizer compone;
		String cuentacheques, linea, nivel, techo, hora1, hora2, tokensHijos,cuenta1,cuenta2;
		StringBuffer untokenPadre;
		StringBuffer untokenHijo;
		StringBuffer nuevaTrama = new StringBuffer ("");
		StringBuffer cadena = new StringBuffer ("");
		String OK="ERROR@";
		String thora1;
		String thora2;

		int c=0;
		try
			{
			//ciclo hasta que termine de leer el archivo
			brArchivo = new BufferedReader ( new FileReader(nombreArchivo));
			while((lineaTem = brArchivo.readLine())!= null)
			{
				System.out.println("Se leyo: "+lineaTem);

				if(c==0)
				 {
				  System.out.println("Codigo de error: "+lineaTem.substring(16,24));
				  if(lineaTem.substring(16,24).equals("BASE0000"))
				   {
					  OK="TRAMA_OK@";
					  req.setAttribute("AltaBC","SI");
					 }
					 else { req.setAttribute("AltaBC","NO"); }

				 }
				if(c==1)
				{cadena.append (lineaTem.substring(lineaTem.indexOf("|")+1) +" | @");}
				if(c>1)
				{cadena.append (lineaTem + "@");}
				c++;
			}
		   System.out.println("CADENA ANTERIOR: "+cadena);

	if(c>=1)		// met� esta validaci�n
		{
			EI_Tipo A=new EI_Tipo();
			A.iniciaObjeto(cadena.toString());

			nuevaTrama=new StringBuffer ("@"+A.camposTabla[0][0]+"| |"+A.camposTabla[0][1]+ "|0|0|0|0|01/01/01|02/02/02");
			System.out.println("Padre: "+nuevaTrama);
			for(int i=1;i<A.totalRegistros;i++) {		///del for interno
				thora1 = A.camposTabla[i][4];
				thora1 = thora1.substring(0,2) + ":" + thora1.substring(2, 4);
				thora2 = A.camposTabla[i][5];
				thora2 = thora2.substring(0,2) + ":" + thora2.substring(2, 4);
			  nuevaTrama.append ("@"+A.camposTabla[i][0]+"|"+A.camposTabla[0][0]+ "|"+A.camposTabla[i][1] + "|"
		     + "00|" + A.camposTabla[i][3]+"|" + thora1 + "|" +thora2 + "|01/01/01|02/02/02");

			System.out.println("CADENA: "+nuevaTrama);
			}   ///del for interno
		}			// fin del if

		brArchivo.close();			//Modificacion 05/12/02
			}			/// del try
		catch(Exception e)
			{
			EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura> Error en leeArchivo(): " + e.toString(), EIGlobal.NivelLog.INFO);
			tramaRegreso = new StringBuffer("<<ERROR>>@trama en el archivo: " + tramaTux.toString());
			}
		finally
			{
			return "" + OK+nuevaTrama; /*tramaRegreso.toString();*/}
		}





String generaLineasParaCombo(String cuentaParaEscribir, HttpServletRequest req, HttpServletResponse res)
	{

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		String modulo=IEnlace.MMant_estruc_B0;
		StringBuffer lineasCredito = new StringBuffer ("");
		Hashtable cuentas=null;
		System.out.println("Entro a enviar el archivo de cuentas.");
		System.out.println("La cuenta que va es: "+cuentaParaEscribir);

		 if(creayCopiaArchivo(session.getUserID8(),cuentaParaEscribir))
		   {
			 System.out.println("se envio y se recuperan linea");
		     cuentas = consultaLineasCredito(req,cuentaParaEscribir);
		   }

		lineasCredito = new StringBuffer ((String)cuentas.get(cuentaParaEscribir));
		  System.out.println("Las lineas para esa cuenta son: "+lineasCredito);
		  if(lineasCredito.toString() == null)
			 lineasCredito = new StringBuffer ("");
		     lineasCredito.append (traeCuentasPropias(session.getUserID8(),session.getContractNumber())); //CSA
			 System.out.println("Las lineas y las lineas propias son: "+lineasCredito);
			return lineasCredito.toString();


	}



/*************************Funciones Auxiliares para la modificaci�n de las lineas****************/

public String traeCuentasPropias(String usuario,String contrato)
	 {
		String arcLinea="";
		StringBuffer cadena = new StringBuffer ("");
		StringBuffer cadenaTCT = new StringBuffer ("");

		EI_Tipo CtasLineas=new EI_Tipo();

		System.out.println("Se traeran las cuentas propias solamente");

		
		//CSA-----------
		CuentasBO cuentasBo = new CuentasBO();
		EIGlobal.mensajePorTrace("CSA: Consulta las cuentas del contrato :"+ contrato, EIGlobal.NivelLog.INFO);
		List<String> cuentas=cuentasBo.consultaCuentasRel(contrato);
		EIGlobal.mensajePorTrace("CSA: Numero de cuentas obtenidas :"+cuentas.size(), EIGlobal.NivelLog.INFO);
		Iterator<String> cuentasIter = cuentas.iterator();
		while(cuentasIter.hasNext()){
			arcLinea = cuentasIter.next();
			if(!arcLinea.equals("ERROR"))
			 {
					if(arcLinea.substring(0,5).trim().equals("ERROR"))
					{
						break;
					}
					if(arcLinea.substring(0,1).equals("2"))
					 {
						cadenaTCT.append (arcLinea.substring(0,arcLinea.lastIndexOf(';'))+" @");
					}
				 }
				else{
					break;
				}
		}
		EIGlobal.mensajePorTrace("CSA: Termino lectura de cuentas. ", EIGlobal.NivelLog.INFO);
		
		//CSA ----------------------
		
		System.out.println("Se leyeron los registros: "+cadenaTCT);
		CtasLineas.iniciaObjeto(';','@',cadenaTCT.toString());
		for(int i=0;i<CtasLineas.totalRegistros;i++)
		 {
		   System.out.println("Registro: "+i+" tipoRelacion: "+ CtasLineas.camposTabla[i][3] + " cveProducto: "+ CtasLineas.camposTabla[i][4]);
		   if(CtasLineas.camposTabla[i][3].trim().equals("P") && CtasLineas.camposTabla[i][4].trim().equals("0"))
			 {
			   System.out.println("Linea de credito: "+i+" tipoRelacion: "+ CtasLineas.camposTabla[i][3] + " cveProducto: "+ CtasLineas.camposTabla[i][4]);
		       cadena.append (CtasLineas.camposTabla[i][1]+" ");
			 }
		 }

	    return "$"+cadena;
	 }

public boolean creayCopiaArchivo(String usuario, String cuenta)
	 {
	     String nombreArchivo="";
		 boolean seGenero=true;

		 nombreArchivo=usuario+".tmp";
		 EI_Exportar ArcTux=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

		 if(ArcTux.creaArchivo())
		  {
			if(!ArcTux.escribeLinea(cuenta+"|"))
			 seGenero=false;
		  }
		 else
		  seGenero=false;

		 ArchivoRemoto archR = new ArchivoRemoto();
		 if(!archR.copiaLocalARemoto(nombreArchivo))
		   {
			  EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura>  No se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
			  seGenero=false;
		   }
		 else
			 EIGlobal.mensajePorTrace("<DEBUG bcMtoEstructura>  Si se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
		 ArcTux.cierraArchivo();
		 return seGenero;
	 }

/*********  funci�n Auxiliar de la parte de la modificaci�n de las lineas de cr�dito *///////

//-------------------------------------------------------------
	private Hashtable consultaLineasCredito(HttpServletRequest req,String cta)
		{

		HttpSession sess = req.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");

		Hashtable cuentas = new Hashtable();
		Hashtable ht;
		String trama, error;
		File archivo;
		BufferedReader entrada;
		boolean autorizaArch;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		try
			{


			// - Obtener contrato, usuario, etc

			System.out.println("Trama para traer lineas de credito");

			trama = "2EWEB|" + session.getUserID8() + "|B0AM|" + session.getContractNumber() + "|" + session.getUserID8() + "|" + session.getUserProfile() + "|";

			System.out.println("Trama: "+trama);

			// - Realizar consulta
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama); //cuentas_modulo
			trama = (String)ht.get("BUFFER");
			error = (String)ht.get("COD_ERROR");


			System.out.println("regreso el servicio: "+trama);

			// - Hacer copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());

			if(archRemoto.copia(trama))
				{
				EIGlobal.mensajePorTrace("<DEBUG cuentasLineasCredito.java> Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
				trama = Global.DIRECTORIO_LOCAL + "/" +trama;
				}
			else
				{
				EIGlobal.mensajePorTrace("<DEBUG cuentasLineasCredito.java> Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);

				}

			// - Consultar archivo
			System.out.println("Se abre el archivo: "+trama);
			archivo = new File(trama);
			entrada = new BufferedReader(new FileReader(archivo));

			// - Armar y retornar el hastable
			trama = entrada.readLine();
			System.out.println("<DEBUG cuentasLineasCredito> cabecera de archivo = " + trama);
			if(!trama.substring(0,2).equals("OK")) throw new Exception("error en el formato de las cuentas");

			while((trama=entrada.readLine())!=null)
				{

				 System.out.println("Se lee del archivo: "+trama);
				 cuentas.put(cta,trama);
				}
			}
		catch(Exception e)
			{

			}
		finally
			{
			return cuentas;
			}
		}
/******************************************************************************/



	public boolean enviaArchivoAmb(HttpServletRequest req)
	{
		ArchivoRemoto ArchRemoto = new ArchivoRemoto();
		ServicioTux st = new ServicioTux();

		FileWriter fwCuentas;
		BufferedReader cuentasx;
		String scuentasLi,trama;
		String archivoAmbiente = Global.DOWNLOAD_PATH + session.getUserID8() +".tmp";
		boolean ArchCreado = true;
		StringBuffer sbcuentasLi = new StringBuffer();


		try {
		 cuentasx = new BufferedReader (new FileReader(archivoAmbiente));
	    	while((scuentasLi = cuentasx.readLine())!=null)
		  {
			sbcuentasLi.append(scuentasLi);
	     }
		  fwCuentas = new FileWriter(Global.DOWNLOAD_PATH + session.getUserID8()+".tmp");
		  fwCuentas.write(""+sbcuentasLi.toString());
		} catch(IOException e) {
			  System.out.println("Ocurrio La siguiente Excepcion " + e.toString());
		  }
		  finally { return  ArchCreado; }
	  }



	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
		 HttpSession sess = request.getSession();

		 BaseResource session = (BaseResource) sess.getAttribute("session");
		 EIGlobal Global = new EIGlobal(session, getServletContext(), this );

		String contrato_ = session.getContractNumber();	//  29-04-02

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		request.setAttribute( "URL", "/Enlace/enlaceMig/bcMtoEstructura" );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	}