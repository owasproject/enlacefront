package mx.altec.enlace.servlets;
// Modificacion RMM 20021218 cierre de archivos, cambio de RandomAccesFile por BufferedReader
// RMM 20030214 Fase 2, aumentar limite superior de fechas
import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;

import javax.sql.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.Utilerias;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



import java.text.SimpleDateFormat;

public class ChesLinea extends BaseServlet
	{

	int salida = 0;
	Vector diasNoHabiles;
	java.util.Date dateHoy;
	GregorianCalendar calHoy;

	// ---
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	// ---
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException
		{defaultAction( req, res );}

	// ---
	public void defaultAction( HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		String modulo        = "";
		String contrato      = "";
		String usuario       = "";
		String clavePerfil   = "";
		String FacBenefNoReg = "";
		modulo  = (String) req.getParameter("Modulo");
		dateHoy = new java.util.Date();
		calHoy  = new GregorianCalendar();
		calHoy.setTime( dateHoy );
		diasNoHabiles = CargarDias(0);
                BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
        HttpSession sess = req.getSession();
		if ( SesionValida( req, res ) )
			{

			String valida = req.getParameter("valida");


			if (validaPeticion(req, res, session, sess, valida)) {
				EIGlobal.mensajePorTrace("Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
			} else {
				//Variables de sesion ...
				contrato          = session.getContractNumber();
				usuario           = session.getUserID8();
				clavePerfil       = session.getUserProfile();
				FacBenefNoReg     = session.getFacultad (session.FAC_BENEF_NOREG) ? "true" : "false";


				if (modulo!=null && modulo.equals( "0" ) ){
					//TODO: BIT CU 4171, EL cliente entra al flujo
					/*
		    		 * VSWF  ARR -I
		    		 */
					if (Global.USAR_BITACORAS.trim().equals("ON")){
					try{
					BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());

					if (req.getParameter(BitaConstants.FLUJO)!=null){
					       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
					  }else{
					   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					  }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_CH_SEG_REG_CHEQUES_LINEA_ENTRA);
					bt.setContrato(contrato);
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						e.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
					}
		    		/*
		    		 * VSWF ARR -F
		    		 */

					capturarCheque(usuario, clavePerfil, req, res, contrato, FacBenefNoReg );
				}
				else {

					///////////////////--------------challenge--------------------------
					String validaChallenge = req.getAttribute("challngeExito") != null
						? req.getAttribute("challngeExito").toString() : "";

					EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
						EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
						//ValidaOTP.guardaParametrosEnSession(req);
						guardaParametrosEnSession(req);
						validacionesRSA(req, res);
						return;
					}

					//Fin validacion rsa para challenge
					///////////////////-------------------------------------------------

					boolean valBitacora = true;
					 if (valida == null) {
							EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
							boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.CHEQUERA_LINEA);

							if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal) {
								EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
								guardaParametrosEnSession(req);

								req.getSession().removeAttribute("mensajeSession");

								ValidaOTP.mensajeOTP(req, "ChesLinea");

								ValidaOTP.validaOTP(req, res, IEnlace.VALIDA_OTP_CONFIRM);
							} else {
								valida = "1";
								valBitacora = false;
							}
						}
						if (valida != null && valida.equals("1")) {

							if (valBitacora)
							{
								try{

									EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

									HttpSession sessionBit = req.getSession(false);
									Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_CHEQUE_SEG;
									String concepto = BitaConstants.CTK_CONCEPTO_CHEQUE_SEG;
									String token = "0";
									if (req.getParameter("token") != null && !req.getParameter("token").equals(""));
									{token = req.getParameter("token");}

									double importeDouble = 0.0;
									String cuenta = "0";

									if (tmpParametros!=null) {
										Enumeration enumer = req.getParameterNames();

										while(enumer.hasMoreElements()) {
											String name = (String)enumer.nextElement();
										}

										String importe = tmpParametros.get("txtImporte").toString();
										importeDouble = Double.valueOf(importe).doubleValue();
										cuenta = tmpParametros.get("textcboCuentaCargo").toString();
									}

									sessionBit.removeAttribute("parametrosBitacora");

									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
								} catch(Exception e) {
									EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

								}
							}
							enviarCheque(clavePerfil,contrato, req, res, usuario );

						}


				 }
			}
		}
	}

	public int capturarCheque(String usuario, String clavePerfil, HttpServletRequest req, HttpServletResponse res, String contrato, String FacBenefNoReg )
			throws ServletException, IOException {
                BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
		int radio                  = 0;
		StringBuffer VarFechaHoy = new StringBuffer ("");
		String cadenaBeneficiarios = "";
		String cadenaCuentas       = "";
		VarFechaHoy  = new StringBuffer (armaArregloFechas());
		String strInhabiles = armaDiasInhabilesJS(req);
		//cadenaBeneficiarios = listarBeneficiarios(contrato, clavePerfil, sucOpera, usuario);
		String fechaLibramiento = EIGlobal.formatoFecha( calHoy, "dd/mm/aaaa" );
		String fechaLimite = obtenFechaLimite( 6 );
        String fechaEnd = obtenFechaLimite( 3 );
		String fechaLarga = EIGlobal.formatoFecha(calHoy, "dt, dd de mt de aaaa");

		req.setAttribute("VarFechaHoy",           VarFechaHoy);
		req.setAttribute("diasInhabiles",         strInhabiles);
		//req.setAttribute("beneficiarios",         cadenaBeneficiarios);
		req.setAttribute("FechaLibramiento",      fechaLibramiento);
		req.setAttribute("FechaLimite",           fechaLimite);
        req.setAttribute("fechaEnd", fechaEnd );
		req.setAttribute("newMenu",				  session.getFuncionesDeMenu());
		req.setAttribute("Encabezado",		  	  CreaEncabezado("Registro de Cheque Seguridad","Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; En L&iacute;nea", "s25850h", req));
		req.setAttribute("MenuPrincipal",		  session.getStrMenu());
		req.setAttribute("FacBenefNoReg",	      FacBenefNoReg);
		session.setModuloConsultar(IEnlace.MAlta_ctas_cheq_seg);
		evalTemplate("/jsp/ches/ChesLinea.jsp", req, res );
		return 1;
	}



	 public int enviarCheque(String clavePerfil,String contrato, HttpServletRequest req, HttpServletResponse res, String usuario )
			throws ServletException, IOException {
		EIGlobal.mensajePorTrace( "***ChesLinea.class & Entrando a enviarCheque &", EIGlobal.NivelLog.INFO);
      String cboCuentaCargo      = "";
      String txtNumCheque        = "";
      String txtImporte          = "";
      String txtFechaLibramiento = "";
      String txtFechaLimite      = "";

      if (req.getParameter("cboCuentaCargo") == null ||
    	 req.getParameter("txtNumCheque") == null ||
    	 req.getParameter("txtImporte") == null ||
    	 req.getParameter("txtFechaLibramiento") == null ||
    	 req.getParameter("hidFechaLimite") == null ) {

    	  cboCuentaCargo      = (String) req.getSession().getAttribute("cboCuentaCargo");
	      txtNumCheque        = (String) req.getSession().getAttribute("txtNumCheque");
	      txtImporte          = (String) req.getSession().getAttribute("txtImporte");
	      txtFechaLibramiento = (String) req.getSession().getAttribute("txtFechaLibramiento");
	      txtFechaLimite      = (String) req.getSession().getAttribute("hidFechaLimite");

      } else {

	      cboCuentaCargo      = (String) req.getParameter("cboCuentaCargo");
	      txtNumCheque        = (String) req.getParameter("txtNumCheque");
	      txtImporte          = (String) req.getParameter("txtImporte");
	      txtFechaLibramiento = (String) req.getParameter("txtFechaLibramiento");
	      txtFechaLimite      = (String) req.getParameter("hidFechaLimite");  //Cambio fecha sin limite 08/12/2003
      }

      /*
       * VSWF
       */
      BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");
      /*
       * VSWF
       */
      EIGlobal.mensajePorTrace( "***ChesLinea.class Fecha Sin Limite : hidFechaLimite= " + req.getParameter("hidFechaLimite") + ",txtFechaLimite= "+req.getParameter("txtFechaLimite")+".", EIGlobal.NivelLog.DEBUG);
	  ServicioTux tuxGlobal = new ServicioTux();
	  //tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

      int salida = 0;

      StringBuffer trama_entrada_ing = new StringBuffer ("");


      String retCodeRedSrvr      = "";
      String retCodeChseSrvr     = "";
      String strSalida           = "";
      String[] arrSalida;
      String trama_salida        = "";
      String trama_salidaRedSrvr = "";
      StringBuffer strMensaje = new StringBuffer ("");
      String strBoton            = "";
      StringBuffer encabezado = new StringBuffer ("");
      String tipo_operacion      = "";
      boolean errFlag            = true;
      String medio_entrega       = "";
      String path_archivo        = "";
      String nombre_archivo      = "";
      String nombreArchivoRemoto = "";
      int numRegistros           = 1;


      medio_entrega  = "1EWEB";
      tipo_operacion = "INGC";

      encabezado  = new StringBuffer (medio_entrega + "|" + usuario + "|" + tipo_operacion + "|" + contrato + "|");
      encabezado.append (usuario + "|" + clavePerfil + "|");


      trama_entrada_ing  = new StringBuffer (contrato + "@" + cboCuentaCargo + "@");
      trama_entrada_ing.append (txtNumCheque + "@" + txtImporte + "@");
      trama_entrada_ing.append (txtFechaLibramiento + "@" + txtFechaLimite + "@");

      trama_entrada_ing  = new StringBuffer (encabezado.toString() + trama_entrada_ing);

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al método
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignación de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada


	  EIGlobal.mensajePorTrace(fechaHr+"***ChesLinea.class trama_entrada_ing >>" + trama_entrada_ing + "<<", EIGlobal.NivelLog.DEBUG);

      try
	  {
	   	  Hashtable hs = tuxGlobal.web_red(trama_entrada_ing.toString());
		  trama_salidaRedSrvr = (String) hs.get("BUFFER");
		   EIGlobal.mensajePorTrace( "***ChesLinea.class trama_salidaRedSrvr >>" + trama_salidaRedSrvr + "<<", EIGlobal.NivelLog.DEBUG);
	  }
	  catch( java.rmi.RemoteException re )
	  {
		  re.printStackTrace();
	  } catch(Exception e) {}



      retCodeRedSrvr = trama_salidaRedSrvr.substring(0, 8).trim();

      if (retCodeRedSrvr.equals("OK"))
	  {
           if(EIGlobal.BuscarToken(trama_salidaRedSrvr, '@', 2).trim().length() == 0)
		  {	strMensaje.append ("Error Inesperado. Intente m&aacute;s tarde.");}
		   else
		  {strMensaje.append (EIGlobal.BuscarToken(trama_salidaRedSrvr, '@', 2));
//			TODO: BIT CU 4171, A4

			 /*
				 * VSWF ARR -I
				 */
			 if (Global.USAR_BITACORAS.trim().equals("ON")){
			 	try{
				BitaHelper bh = new BitaHelperImpl(req, session,req.getSession());
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CH_SEG_REG_CHEQUES_LINEA_INGRESO_CHEQ_SEGU);
				bt.setContrato(contrato);
				if(retCodeRedSrvr!=null){
					 if(retCodeRedSrvr.substring(0,2).equals("OK")){
						   bt.setIdErr("INGC0000");
					 }else if(retCodeRedSrvr.length()>8){
						  bt.setIdErr(retCodeRedSrvr.substring(0,8));
					 }else{
						  bt.setIdErr(retCodeRedSrvr.trim());
					 }
					}
				bt.setServTransTux(tipo_operacion);
				bt.setFechaProgramada(Utilerias.MdyToDate(txtFechaLimite.replace('-', '/')));
				if(cboCuentaCargo!=null && !cboCuentaCargo.equals(""))
					bt.setCctaOrig(cboCuentaCargo);
				if(txtImporte!=null && !txtImporte.equals(""))
					bt.setImporte(Double.parseDouble(txtImporte));
				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			 }
				/*
				 * VSWF ARR -F
				 */

		  }
     }
     else if (retCodeRedSrvr.equals("MANC"))
		 {strMensaje.append ("Mancomunidad: " + trama_salidaRedSrvr.substring(8, trama_salidaRedSrvr.length()));}
     else if (trama_salidaRedSrvr.length()>16)
		 {strMensaje.append (trama_salidaRedSrvr.substring(16,trama_salidaRedSrvr.length())); }
	 else
		 {strMensaje.append ("Problemas con la comunicación."); }

	 EIGlobal.mensajePorTrace( "***ChesLinea.class & strMensaje :" + strMensaje + " &", EIGlobal.NivelLog.INFO);
	 String fecha_hoy = EIGlobal.formatoFecha(calHoy, "dt, dd de mt de aaaa");
	 despliegaPaginaErrorURL(strMensaje.toString() ,"Registro de Cheque Seguridad","Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; En L&iacute;nea","s25850h","ChesLinea?Modulo=0", req, res);
	 EIGlobal.mensajePorTrace( "***ChesLinea.class & Saliendo de la pantalla de Envia Cheque: " + salida + " &", EIGlobal.NivelLog.INFO);

	return 1;
}




 String armaArregloFechas()
 {
      StringBuffer VarFechaHoy = new StringBuffer ("");
      VarFechaHoy.append ("\n  dia   = new Array("  + EIGlobal.formatoFecha(calHoy, "dd")   + "," + EIGlobal.formatoFecha(calHoy, "dd")   + ");");
      VarFechaHoy.append ("\n  mes   = new Array("  + EIGlobal.formatoFecha(calHoy, "mm")   + "," + EIGlobal.formatoFecha(calHoy, "mm")   + ");");
      VarFechaHoy.append ("\n  anio  = new Array("  + EIGlobal.formatoFecha(calHoy, "aaaa") + "," + EIGlobal.formatoFecha(calHoy, "aaaa")  + ");");
      VarFechaHoy.append ("\n\n");
      VarFechaHoy.append ("\n  dia1  = new Array("  + EIGlobal.formatoFecha(calHoy, "dd")   + "," + EIGlobal.formatoFecha(calHoy, "dd")   + ");");
      VarFechaHoy.append ("\n  mes1  = new Array("  + EIGlobal.formatoFecha(calHoy, "mm")   + "," + EIGlobal.formatoFecha(calHoy, "mm")   + ");");
      VarFechaHoy.append ("\n  anio1 = new Array("  + EIGlobal.formatoFecha(calHoy, "aaaa") + "," + EIGlobal.formatoFecha(calHoy, "aaaa")  + ");");
      VarFechaHoy.append ("\n  Fecha = new Array('" + EIGlobal.formatoFecha(calHoy, "dd/mm/aaaa") + "','" + EIGlobal.formatoFecha(calHoy, "dd/mm/aaaa") + "');");
      return VarFechaHoy.toString();
 }

 String obtenFechaLimite(int desp)
  {
      String fechaLimite = "";

      GregorianCalendar Cal = new GregorianCalendar();
      Cal.add( Calendar.MONTH, desp ); // RMM 20030214 Fase 2, aumentar limite superior de fechas
      Cal = evaluaDiaHabilAtras(Cal);
      fechaLimite = EIGlobal.formatoFecha(Cal, "dd/mm/aaaa");

      return fechaLimite;
  }


 GregorianCalendar evaluaDiaHabilAtras(GregorianCalendar fecha)
   {
      int     indice;
      int     diaDeLaSemana;
      boolean encontrado;

             if(diasNoHabiles!=null)
             {
               fecha.add( fecha.DAY_OF_MONTH, +1);
               do{
                   fecha.add( fecha.DAY_OF_MONTH,  -1);

                   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
                   encontrado = false;
					  String dia;
                      String mes;
                      String anio;
                      String strFecha;
                   for(indice=0; indice<diasNoHabiles.size(); indice++)
                   {
					dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
                    mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
                    anio = Integer.toString( fecha.get( fecha.YEAR) );
                    strFecha = dia.trim() + "/" + mes.trim() + "/" + anio.trim();
                      if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
                      {
                         //System.out.println("Dia es inhabil");
                         encontrado = true;
                         break;
                      }
                   }
               }while( (diaDeLaSemana == Calendar.SUNDAY)   ||
                       (diaDeLaSemana == Calendar.SATURDAY) ||
                       encontrado );
             }

         return fecha;
 }


  String armaDiasInhabilesJS(HttpServletRequest req)
   {
      int indice       = 0;
      StringBuffer resultado = new StringBuffer ("");
      Vector diasInhabiles;

      diasInhabiles = CargarDias(1);

             if((diasInhabiles!=null) && (diasInhabiles.size() > 0))
             {
                  resultado = new StringBuffer (diasInhabiles.elementAt(indice).toString());
                  for(indice=1; indice<diasInhabiles.size(); indice++)
                   {
                      resultado.append (", " + diasInhabiles.elementAt(indice).toString());
                   }
             }

      return resultado.toString();
   }

  /**
   * @author CSA se actualiza para el manejo de cierre a base de Datos.
   * @since 17/11/2013
   * @param formato
   * @return
   */
  public Vector CargarDias(int formato){
       Connection  Conexion = null;
       PreparedStatement qrDias;
       ResultSet rsDias = null;
       boolean    estado;
       String     sqlDias;
       Vector diasInhabiles = new Vector();
       try{
         sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate";
         Conexion = createiASConn(Global.DATASOURCE_ORACLE);
         if(Conexion!=null){
            qrDias = Conexion.prepareStatement( sqlDias );
            if(qrDias!=null){
          	  rsDias = qrDias.executeQuery();
               if(rsDias!=null){
				   String dia;
                   String mes;
                   String anio;
                  while( rsDias.next()){
					  EIGlobal.mensajePorTrace( "***ChesLinea.class & Leyendo fecha &", EIGlobal.NivelLog.INFO);
						dia  = rsDias.getString(1);
						mes  = rsDias.getString(2);
						anio = rsDias.getString(3);

                      if(formato == 0)
                       {
                          dia  =  Integer.toString( Integer.parseInt(dia) );
                          mes  =  Integer.toString( Integer.parseInt(mes) );
                          anio =  Integer.toString( Integer.parseInt(anio) );
                       }

                      String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
  					  EIGlobal.mensajePorTrace( "***ChesLinea.class & fecha no habil " + fecha + " &", EIGlobal.NivelLog.INFO);

                      diasInhabiles.addElement(fecha);

                  }

               }
               else
				   EIGlobal.mensajePorTrace( "***ChesLinea.class & Cannot Create ResultSet &", EIGlobal.NivelLog.INFO);
            }
            else
			    EIGlobal.mensajePorTrace( "***ChesLinea.class & Cannot Create Query &", EIGlobal.NivelLog.INFO);
         }
         else
			 EIGlobal.mensajePorTrace( "***ChesLinea.class & Error al intentar crear la conexion &",EIGlobal.NivelLog.ERROR);
        }catch( SQLException sqle ){
        	sqle.printStackTrace();
        } finally {
        	EI_Query.cierraResultSet(rsDias);
        	EI_Query.cierraConnection(Conexion);
		}
        return(diasInhabiles);
    }

  public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla",request.getServletPath());
		request.getSession().removeAttribute("bitacora");

      HttpSession session = request.getSession(false);
      if(session != null)
		{
    	  session.setAttribute("cboCuentaCargo", request.getParameter("cboCuentaCargo"));
    	  session.setAttribute("txtNumCheque", request.getParameter("txtNumCheque"));
    	  session.setAttribute("txtImporte", request.getParameter("txtImporte"));
    	  session.setAttribute("txtFechaLibramiento", request.getParameter("txtFechaLibramiento"));
    	  session.setAttribute("hidFechaLimite", request.getParameter("hidFechaLimite"));



			Map tmp = new HashMap();

		  EIGlobal.mensajePorTrace("importe-> " + request.getParameter("txtImporte"), EIGlobal.NivelLog.INFO);
          EIGlobal.mensajePorTrace("cuenta-> " + request.getParameter("textcboCuentaCargo"), EIGlobal.NivelLog.INFO);
          EIGlobal.mensajePorTrace("noCheque-> " + request.getParameter("txtNumCheque"), EIGlobal.NivelLog.INFO);

          tmp.put("txtImporte", request.getParameter("txtImporte"));
          tmp.put("textcboCuentaCargo", request.getParameter("textcboCuentaCargo"));
          tmp.put("txtNumCheque", request.getParameter("txtNumCheque"));

          session.setAttribute("parametros",tmp);

          tmp = new HashMap();
          Enumeration enumer = request.getAttributeNames();
          while(enumer.hasMoreElements()) {
              String name = (String)enumer.nextElement();
              tmp.put(name,request.getAttribute(name));
          }
          session.setAttribute("atributos",tmp);
      }

		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
  }

}