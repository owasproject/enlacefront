/**Banco Santander Mexicano

  Clase cancprog  llamada al servicio para cancelacion de operaciones programadas

  @Autor:

  @version: 1.0

  fecha de creacion:

  responsable: Roberto Resendiz

  Modificacion:Paulina Ventura Agustin 28/02/2001 - Cambio a WEB_RED
  Modificacion:Arturo Sanjuan Sanchez  06/03/2015 - Cambio cancelacion cuentas proyecto M022223

 **/
package mx.altec.enlace.servlets;



import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.TablaEwebNominaBean;
import mx.altec.enlace.beans.TablaTCTProgramacionBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;


public class cancprog extends BaseServlet{

	//int EXISTE_ERROR = 0;

	/**Literal para OPERACIONES_A_CANCELAR**/
	private static final String LITERAL_OPER_CANCELAR = "OPERACIONES_A_CANCELAR";
	/**Literal para session**/
	private static final String LITERAL_SESSION = "session";

	/**
	 * Serial generado
	 */
	private static final long serialVersionUID = 8638180503156272527L;

	/** {@inheritDoc} **/
	public void doGet( HttpServletRequest req, HttpServletResponse res )

	throws ServletException, IOException{

		defaultAction( req, res );

	}


	/** {@inheritDoc} **/
	public void doPost( HttpServletRequest req, HttpServletResponse res )

	throws ServletException, IOException{

		defaultAction( req, res );

	}


	/**
	 * Metodo inicial del servlet
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)

	throws ServletException, IOException {



		EIGlobal.mensajePorTrace("Entrando al Action ", EIGlobal.NivelLog.INFO);

		if(SesionValida(req, res)) {
			String arreglCancelar = (String) req.getParameter(LITERAL_OPER_CANCELAR);
			if(arreglCancelar.contains("OPER_NOMI")) {
				cancelacionOperacionesNomina(req, res);
			} else if (arreglCancelar.contains("OPER_DIBT")) {
				cancelacionOperacionesInterbancarias(req, res);
			} else {
				cancelacionOperacionesProgramadas(req, res);
			}
		}
		EIGlobal.mensajePorTrace("Sali de consulta de operaciones programadas", EIGlobal.NivelLog.INFO);

	}

	/**
	 * Metodo que genera un bean de nomina
	 * @param cadenaDatos : cadena de datos
	 * @return ConsultaNominaProgramadaBean
	 */
	private TablaEwebNominaBean crearDatosNomina (String cadenaDatos) {
		TablaEwebNominaBean beanDatos = new TablaEwebNominaBean();
		String datos[] = cadenaDatos.split("\\|");
		beanDatos.setFechaRecepcion(datos[0].trim());
		beanDatos.setFechaCargo(datos[1].trim());
		beanDatos.setFechaAplicacion(datos[2].trim());
		beanDatos.setHoraAplicacion(datos[3].trim());
		beanDatos.setCtaCargo(datos[4].trim());
		beanDatos.setNombreArchivo(datos[5].trim());
		beanDatos.setSecuencia(datos[6].trim());
		beanDatos.setNumeroRegistros(datos[7].trim());
		beanDatos.setImporteAplic(datos[8].trim());
		beanDatos.setEstatus(datos[9].trim());

		return beanDatos;
	}

	/**
	 * Metodo para genear el bean de TablaTCTProgramacionBean
	 * @param cadenaDatos : cadena de datos
	 * @return bean de datos
	 */
	private TablaTCTProgramacionBean crearDatosInterbancarias(String cadenaDatos) {
		TablaTCTProgramacionBean beanDatos = new TablaTCTProgramacionBean();
		String datos[] = cadenaDatos.split("\\|");

		beanDatos.setReferencia(datos[0]);
		beanDatos.setDescripcionCuenta(datos[1]);
		beanDatos.setFecha(datos[2]);
		beanDatos.setCuenta1(datos[3]);
		beanDatos.setCuenta2(datos[4]);
		beanDatos.setImporte(datos[5]);
		beanDatos.setBeneficiario(datos[6]);
		beanDatos.setFecha(datos[7]);
		beanDatos.setConcepto(datos[8]);
		beanDatos.setReferenciaInterbancaria(datos[9]);
		beanDatos.setFormaAplicaion(datos[10]);

		return beanDatos;
	}

	/**
	 * Metodo para cancelar las operaciones interbancarias
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	private void cancelacionOperacionesInterbancarias(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		String arreglCancelar = (String) req.getParameter(LITERAL_OPER_CANCELAR);
		String registros[] = arreglCancelar.split("\\@");
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);
		String usuario = session.getUserID8();
		String contrato=session.getContractNumber();
		String claveperfil=session.getUserProfile();
		List<TablaTCTProgramacionBean> listaDatos = new ArrayList<TablaTCTProgramacionBean>();

		crearEncabezado(req);

		for(int i = 0; i < registros.length; i++) {

			TablaTCTProgramacionBean beanDatos = crearDatosInterbancarias(registros[i]);

			StringBuffer tramaentrada=new StringBuffer("");
			tramaentrada.append("1EWEB|");
			tramaentrada.append(usuario);
			tramaentrada.append("|CPRO|");
			tramaentrada.append(contrato);
			tramaentrada.append('|');
			tramaentrada.append(usuario);
			tramaentrada.append('|');
			tramaentrada.append(claveperfil);
			tramaentrada.append('|');
			tramaentrada.append(beanDatos.getReferencia());
			// Modificación req.Q06-1374 (EIC) Se agrega a la trama la fecha de la operación a cancelar
			tramaentrada.append('|');
			tramaentrada.append(beanDatos.getFecha());

			EIGlobal.mensajePorTrace("trama de entrada : " + tramaentrada.toString() , EIGlobal.NivelLog.DEBUG);

			String coderror = "";

			try{
				ServicioTux tuxGlobal = new ServicioTux();
				Map hs = tuxGlobal.web_red(tramaentrada.toString());
				coderror = (String) hs.get("BUFFER");

			}catch( java.rmi.RemoteException re ){

				EIGlobal.mensajePorTrace(">>> Error:::" + re.getLocalizedMessage() + re.getMessage(), EIGlobal.NivelLog.ERROR);

			}catch (Exception e) {
				EIGlobal.mensajePorTrace("cancprog.java cancelacionOperacionesInterbancarias ERROR: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}

			EIGlobal.mensajePorTrace("trama de salida : " + coderror , EIGlobal.NivelLog.DEBUG);

			if ("OK".equals(coderror.substring(0,2))) {
				beanDatos.setEstatus("Operaci&oacute;n cancelada con &eacute;xito.");
				String refCancelacion = coderror.substring(11, coderror.length());
				beanDatos.setDatoAux(refCancelacion);
			} else if ( coderror.trim().length() > 48 ){
				String mensaje_servicio = coderror.substring( 16,49 );
				beanDatos.setEstatus(mensaje_servicio);
			} else {
				beanDatos.setEstatus("Operaci&oacute;n NO cancelada.");
			}
			listaDatos.add(beanDatos);

		}

		req.setAttribute("listaCancelacionInter", listaDatos);
		evalTemplate("/jsp/operacion_cancelada.jsp",req, res);
	}

	/**
	 * Metodo para cancelar las operaciones programadas de nomina
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	private void cancelacionOperacionesNomina(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		EIGlobal.mensajePorTrace("Entrando a cancelacion operaciones programadas de nomina", EIGlobal.NivelLog.INFO);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);
		List<TablaEwebNominaBean> listaRegistros = new ArrayList<TablaEwebNominaBean>();

		String contrato=session.getContractNumber();
		String usuario=session.getUserID8();
		String claveperfil=session.getUserProfile();

		String arreglCancelar = (String) req.getParameter(LITERAL_OPER_CANCELAR);
		String registros[] = arreglCancelar.split("\\@");

		crearEncabezado(req);

		for(int i = 0; i < registros.length; i++) {

			//String datos[] = registros[i].split("\\|");
			TablaEwebNominaBean datosBean = crearDatosNomina(registros[i]);
			String secuencia = datosBean.getSecuencia();
			String fechaRecepcion = datosBean.getFechaRecepcion().replaceAll("/", "");
			Map htResult = null;
			ServicioTux tuxGlobal = new ServicioTux();

			String tramaEntrada = "";// trama que se enviara a
			// WEB_RED
			tramaEntrada = "1EWEB|" + usuario + "|NOCA|" + contrato
			+ "|" + usuario + "|" + claveperfil + "|" + contrato
			+ "@" + secuencia + "@" + fechaRecepcion + "@" + datosBean.getFechaAplicacion() + "@" + datosBean.getCtaCargo() + "@" + Double.parseDouble(datosBean.getImporteAplic()) + "@Recibido@";
			EIGlobal.mensajePorTrace(">>> Trama enviada a NOCA:>>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);

			try {
				htResult = tuxGlobal.web_red(tramaEntrada);
			} catch (Exception e) {
				//re.printStackTrace();
				try {
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al ejecutar validacion de cuenta por medio de vfilesrvr", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("<DATOS GENERALES>", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("Usuario->" + session.getUserID8(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("Contrato->" + session.getContractNumber(), EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("Modulo a consultar->" + modulo, EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("Linea de truene->" + lineaError[0], EIGlobal.NivelLog.ERROR);
				} catch(Exception e2){
					EIGlobal.mensajePorTrace("OpcionesNomina::defaultAction::"
							+ "SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
							+ e2.getMessage(), EIGlobal.NivelLog.ERROR);
				}
			}

			String tramaSalida = (String) htResult.get("BUFFER");
			EIGlobal.mensajePorTrace(">>> El servicio nomi_cancela regreso: >>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
			if (tramaSalida == null || "".equals(tramaSalida.trim()) || tramaSalida.indexOf('@') == -1) {
				tramaSalida = "NOMI9999        @Error en el servicio, intente mas tarde@";
			}
			String estatus_transacion = tramaSalida.substring(0,posCar(tramaSalida, '@', 1));
			String mensaje_transacion = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@',2));

			//String estatus_transacion = "NOMI0000" ;
			//String mensaje_transacion = "ERROR" ;

			if ("NOMI0000".equals(estatus_transacion)) {
				datosBean.setEstatus("Cancelado");
			} else {
				datosBean.setEstatus(mensaje_transacion);
			}

			listaRegistros.add(datosBean);
		}
		req.setAttribute("listaCancelacionNomina", listaRegistros);
		evalTemplate("/jsp/operacion_cancelada.jsp",req, res);
		//RequestDispatcher disp = req.getRequestDispatcher("\\jsp\\operacion_cancelada.jsp");
		//disp.forward(req, res);
	}

	/**
	 * Metodo para crear el encabezado de la vista
	 * @param req : HttpServletRequest
	 */
	private void crearEncabezado(HttpServletRequest req) {
		String fecha_hoy=ObtenFecha();
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);

		req.setAttribute("fecha_hoy",fecha_hoy);
		req.setAttribute("titulo","Operaciones canceladas");
		req.setAttribute("ContUser",ObtenContUser(req));
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado( "Cancelaci&oacute;n de operaciones programadas","Consultas &gt; Operaciones Programadas &gt; Cancelaci&oacute;n","s25280h",req) );
		req.setAttribute("MenuPrincipal", session.getStrMenu());
	}


	/**
	 * Metodo para cancelar las operaciones programadas
	 * @param req : HttpServletRequest
	 * @param res : HttpServletResponse
	 * @return int
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	public int cancelacionOperacionesProgramadas(HttpServletRequest req, HttpServletResponse res)

	throws ServletException, IOException {

		EIGlobal.mensajePorTrace("Entrando a cancelacion operaciones programadas", EIGlobal.NivelLog.INFO);
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(LITERAL_SESSION);

		int EXISTE_ERROR = 0;

		String contrato="";

		String usuario="";

		String cadena_retorno="";

		short suc_opera=787;

		StringBuffer buffer=new StringBuffer("");

		int numeroReferencia=0;

		String arreglo_cancelar="";

		String scontador="";

		String coderror="";

		String fecha_hoy="";

		String claveperfil="";

		String mensaje_servicio = "";



		EXISTE_ERROR = 0;

		contrato=session.getContractNumber();

		usuario=session.getUserID8();

		claveperfil=session.getUserProfile();



		fecha_hoy=ObtenFecha();

		arreglo_cancelar = (String) req.getParameter(LITERAL_OPER_CANCELAR);

		EIGlobal.mensajePorTrace("arreglo_cancelar " + arreglo_cancelar, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace("\nscontador "+scontador, EIGlobal.NivelLog.INFO);



		EI_Tipo operacion= new EI_Tipo(this);





		// modificación 20-04-2001 jbg

		// cancelación simultanea de más de un cheque



		operacion.strCuentas	= arreglo_cancelar;

		operacion.strOriginal	= arreglo_cancelar;

		operacion.numeroRegistros();

		operacion.llenaArreglo();

		operacion.separaCampos();



		if (operacion.totalRegistros > 0 ){



			buffer.append( "<table width=350 border=0 cellspacing=2 cellpadding=3 bgcolor=#FFFFFF align=center>" );

			buffer.append("	<tr>");

			buffer.append("		<td class=tittabdat align=center>Ref.Programaci&oacute;n</td>");

			buffer.append("		<td class=tittabdat align=center>Tipo de Operaci&oacute;n</td>");

			buffer.append("		<td class=tittabdat align=center>Fecha de Aplicaci&oacute;n</td>");

			buffer.append("		<td class=tittabdat align=center>Cuenta Cargo</td>");

			buffer.append("		<td class=tittabdat align=center>Cuenta Abono</td>");

			buffer.append("		<td class=tittabdat align=center>Importe</td>");

			buffer.append("		<td class=tittabdat align=center>Estatus</td>");

			buffer.append("		<td class=tittabdat align=center>Ref.Cancelaci&oacute;n</td>");

			buffer.append("	</tr>");

			int residuo=0;

			//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo();

			ServicioTux tuxGlobal = new ServicioTux();

			//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

			for ( int indice = 0 ; indice < operacion.totalRegistros ; indice++ ){

				// Modificacion 25-01-05
				// Se movio esta linea que estaba fuera del for

				residuo=indice%2;

				String estilo = "";

				if (residuo==0) {

					estilo = " class=textabdatobs nowrap>";

				} else {

					estilo = " class=textabdatcla nowrap>";
				}

				if(operacion.strOriginal.contains("OPER_TRAN") || operacion.strOriginal.contains("OPER_PAGT")) {
					EIGlobal.mensajePorTrace(">>> Ejecuta cancelacion TRAN, PAGT " + coderror , EIGlobal.NivelLog.DEBUG);
					StringBuffer tramaentrada=new StringBuffer("");
					tramaentrada.append("1EWEB|");
					tramaentrada.append(usuario);
					tramaentrada.append("|CPRO|");
					tramaentrada.append(contrato);
					tramaentrada.append('|');
					tramaentrada.append(usuario);
					tramaentrada.append('|');
					tramaentrada.append(claveperfil);
					tramaentrada.append('|');
					tramaentrada.append(operacion.camposTabla[indice][0]);
					// Modificación req.Q06-1374 (EIC) Se agrega a la trama la fecha de la operación a cancelar
					tramaentrada.append('|');
					tramaentrada.append(operacion.camposTabla[indice][2]);

					EIGlobal.mensajePorTrace("trama de entrada : " + tramaentrada.toString() , EIGlobal.NivelLog.DEBUG);


					try{

						Map hs = tuxGlobal.web_red(tramaentrada.toString());

						coderror = (String) hs.get("BUFFER");

					}catch( java.rmi.RemoteException re ){
						EIGlobal.mensajePorTrace("cancprog.java cancelacionOperacionesProgramadas ERROR: " + re.getMessage(), EIGlobal.NivelLog.ERROR);
					}catch (Exception e) {
						EIGlobal.mensajePorTrace("cancprog.java cancelacionOperacionesProgramadas ERROR: " + e.getMessage(), EIGlobal.NivelLog.ERROR);
					}

				}


				EIGlobal.mensajePorTrace("trama de salida : " + coderror , EIGlobal.NivelLog.DEBUG);

				buffer.append("<tr>");

				buffer.append("<td align=center" );
				buffer.append(estilo );
				buffer.append(operacion.camposTabla[indice][0] );
				buffer.append("</td>");

				buffer.append("<td align=left"   );
				buffer.append(estilo );
				buffer.append(operacion.camposTabla[indice][1] );
				buffer.append("</td>");

				buffer.append("<td align=center" );
				buffer.append(estilo );
				buffer.append(operacion.camposTabla[indice][2] );
				buffer.append("</td>");



				if ( operacion.camposTabla[indice][3].trim().length() < 1 )
				{
					buffer.append("<td align=left"   );
					buffer.append(estilo );
					buffer.append("&nbdp;</td>");
				}

				else
				{
					buffer.append("<td align=left"   );
					buffer.append(estilo );
					buffer.append(operacion.camposTabla[indice][3] );
					buffer.append("</td>");
				}

				if ( operacion.camposTabla[indice][4].trim().length() < 1 )
				{
					buffer.append("<td align=left"   );
					buffer.append(estilo );
					buffer.append("&nbsp;</td>");
				}
				else
				{
					buffer.append("<td align=left" );
					buffer.append(estilo );
					buffer.append(operacion.camposTabla[indice][4] );
					buffer.append("</td>");
				}



				buffer.append("<td align=right"  );
				buffer.append(estilo );
				buffer.append(operacion.camposTabla[indice][5] );
				buffer.append("</td>");


				if ("OK".equals(coderror.substring(0,2))) {

					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append("Operaci&oacute;n cancelada con &eacute;xito.</td>");

					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append(coderror.substring(11, coderror.length() ) );
					buffer.append("</td>");

				}else if ( coderror.trim().length() > 48 ){

					mensaje_servicio = coderror.substring( 16,49 );

					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append(mensaje_servicio );
					buffer.append("</td>");

					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append("&nbsp;</td> ");
				}
				else
				{
					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append("Operaci&oacute;n NO cancelada." );
					buffer.append("</td>");

					buffer.append("<td align=center");
					buffer.append(estilo );
					buffer.append("&nbsp;</td> ");
				}



				buffer.append("</tr>");

			}

			buffer.append("</table>");

		}else{

			buffer=new StringBuffer("Sin operaciones a cancelar");

			EXISTE_ERROR = 1;

		}

		req.setAttribute("fecha_hoy",fecha_hoy);

		req.setAttribute("mensaje_salida_cancel",buffer.toString());

		req.setAttribute("titulo","Operaciones canceladas");

		req.setAttribute("ContUser",ObtenContUser(req));



		req.setAttribute("newMenu", session.getFuncionesDeMenu());

		// Modificación req.Q06-1374 (EIC) Se agrega el archivo de ayuda.
		req.setAttribute( "Encabezado", CreaEncabezado( "Cancelaci&oacute;n de operaciones programadas","Consultas &gt; Operaciones Programadas &gt; Cancelaci&oacute;n","s25280h",req) );


		req.setAttribute("MenuPrincipal", session.getStrMenu());



		if ( EXISTE_ERROR == 0){
			//TODO  BIT CU1141
			/*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */

			if ("ON".equals(Global.USAR_BITACORAS.trim())){
				try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_OPER_CONSULTA_OPER_PROG_CANCELACION);
					if(contrato!=null){
						bt.setContrato(contrato);
					}
					bt.setServTransTux("CPRO");
					if(coderror != null){
						if("OK".equals(coderror.substring(0,2))){
							bt.setIdErr("CPRO0000");
						}else if(coderror.length()>8){
							bt.setIdErr(coderror.substring(0,8));
						}else{
							bt.setIdErr(coderror.trim());
						}
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			/*
			 * 02/ENE/07
			 * VSWF-BMB-F
			 */

			evalTemplate("/jsp/operacion_cancelada.jsp",req, res);
			//RequestDispatcher disp = req.getRequestDispatcher("\\jsp\\operacion_cancelada.jsp");
			//disp.forward(req, res);
		}


		else {

			despliegaPaginaError(buffer.toString(),"Cancelaci&oacute;n de operaciones programadas","Consultas &gt; Operaciones Programadas &gt; Cancelaci&oacute;n", req, res);

		}

		EIGlobal.mensajePorTrace("Saliendo de  cancelacion operaciones programadas", EIGlobal.NivelLog.INFO);




		return 1;

	}

	/**
	 * Metodo auxiliar para extraer datos
	 * @param cad : cadenar
	 * @param car : caracter a buscaar
	 * @param cant : contador
	 * @return int
	 */
	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;
		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}

}