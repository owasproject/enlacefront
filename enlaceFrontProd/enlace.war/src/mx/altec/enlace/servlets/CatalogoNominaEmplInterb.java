/*************************************************************************************************/
 /** CatalogoNominaEmplInterb - Clase Principal para la administracion de Nomina Interbancaria	**/
 /** 	25/Jun/2009  																			**/
 /************************************************************************************************/

package	mx.altec.enlace.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import java.util.HashMap;
import java.util.Map;

import java.sql.*;
import javax.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/* everis JARA - Ambientacion - Librerias para Bitacora */
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CatNominaInterbAction;
import mx.altec.enlace.bo.CatNominaInterbBean;
import mx.altec.enlace.bo.CatNominaInterbConstantes;
import mx.altec.enlace.dao.CatNominaInterbDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;



public class CatalogoNominaEmplInterb extends BaseServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	String rutadeSubida= Global.DIRECTORIO_REMOTO;

	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		defaultAction(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		defaultAction(request, response);
	}

	/*
	  Metodo encargado de realizar los llamados a las
	  funciones de Busqueda, Baja y Eliminacion de Empleados
	  del nuevo Catalogo de Nomina */

	public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		boolean existReg = true;
		//ArrayList listaModifFinal = new ArrayList();
		ArrayList listaBajaFinal = new ArrayList();


		try{
			String numContrato="";
			String usuario="";
			String clavePerfil="";
			String modulo="";

			EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): Inicio ",EIGlobal.NivelLog.DEBUG);
			HttpSession sessionHttp = request.getSession();
			BaseResource session = (BaseResource) sessionHttp.getAttribute("session");
			boolean sesionvalida=SesionValida( request, response );

			if(sesionvalida)
			{
				numContrato=(session.getContractNumber()==null)?"":session.getContractNumber();
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): contrato: "+numContrato,EIGlobal.NivelLog.DEBUG);
				usuario=(session.getUserID8()==null)?"":session.getUserID8();
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): usuario: "+usuario,EIGlobal.NivelLog.DEBUG);
				clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): clavePerfil: "+clavePerfil,EIGlobal.NivelLog.DEBUG);
				modulo=(String) request.getParameter("Modulo");
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): el modulo es: "+modulo ,EIGlobal.NivelLog.DEBUG);

				try {
					request.setAttribute ("MenuPrincipal", session.getStrMenu ());
					request.setAttribute ("newMenu", session.getFuncionesDeMenu ());
				}
				catch(Exception e) {
					e.printStackTrace();
				}

				String opcion = request.getParameter("opcion");
				String pruebas = request.getParameter("cuentas");
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): pruebas: ["+ pruebas + "]" ,EIGlobal.NivelLog.DEBUG);
				if(opcion == null)
					opcion = "1";
				EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - defaultAction(): opcion: ["+ opcion + "]" ,EIGlobal.NivelLog.DEBUG);

				/***********************************************************
				 *** opcion 1 = Cargar pagina de busqueda de Empleados.  ***
				 **********************************************************/

				if(opcion.equals("1"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
											"B&uacute;squeda", "s37100h", request ) );

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra en opcion 1" ,EIGlobal.NivelLog.DEBUG);
					evalTemplate("/jsp/CatNominaInterbBusqueda.jsp", request, response );
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Sale de opcion 1" ,EIGlobal.NivelLog.DEBUG);
				}

				/**************************************************
				 *** Opcion 2 = Realizar Busqueda de Empleados  ***
				 **************************************************/
				else if(opcion.equals("2"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ("B&uacute;squeda de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
							"B&uacute;squeda", "s37100h", request ) );

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra en opcion 2" ,EIGlobal.NivelLog.DEBUG);
					CatNominaInterbAction action = new CatNominaInterbAction();
					CatNominaInterbDAO dao = new CatNominaInterbDAO();
					String valorBusqueda = request.getParameter("valorBusqueda");
					String radioValue = request.getParameter("radioValue");
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Dato Busqueda: " + valorBusqueda ,EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Filtro:  " + radioValue ,EIGlobal.NivelLog.DEBUG);
					String tablaEmpleados = "";
					int numeroPagina = 0;
					int numeroPaginas = 0;
					int inicioBloque = 0;
					int finBloque = 0;
					int totalReg = 0;
					int tamanoBloque = 15;
					String pagReg[] = null;
					String[][] arrDatosQuery = null;
					boolean errorExcepcion = false; 		// Verifica Errores por Manejo de Excepciones
					String archivoCatNomina = ""; //VC ESC crear Archivo

					if( sessionHttp.getAttribute("arrDatosQuery") == null || request.getParameter("numeroPagina") == null)
					{
						/* Invoca metodo para obtener numero de paginas */
						EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - - Consultado:  " + radioValue ,EIGlobal.NivelLog.DEBUG);

						pagReg =  action.consultaPaginas(numContrato, radioValue, valorBusqueda, tamanoBloque);

						if(!pagReg[0].equals("ERROR0000"))
						{
							numeroPaginas = Integer.parseInt(pagReg[0]);
							totalReg = Integer.parseInt(pagReg[1]);
							if( numeroPaginas != 0 ){
								numeroPagina = 1;
								finBloque = inicioBloque + tamanoBloque;
							}else{
								finBloque = totalReg;
							}
							request.setAttribute("numeroPagina", String.valueOf(numeroPagina));
							request.setAttribute("numeroPaginas", String.valueOf(numeroPaginas));
							request.setAttribute("inicioBloque", String.valueOf(inicioBloque));
							request.setAttribute("finBloque", String.valueOf(finBloque));
							request.setAttribute("tamanoBloque", String.valueOf(tamanoBloque));
							request.setAttribute("totalReg", String.valueOf(totalReg));

							/* Invoca metodo para obtener el arreglo general de los empleados */
							// Aqui voy
							arrDatosQuery = dao.buscaEmpleado(numContrato, radioValue, valorBusqueda, totalReg);

							if(!arrDatosQuery[0][0].equals("ERROR0000"))
							{
								//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">

								//Se omite bitaorización temporalmente
 								/*try{
								       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BUSQUEDA EMPLEADO",EIGlobal.NivelLog.INFO);
									  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
										EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
									  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
									  	BitaTCTBean bean = new BitaTCTBean ();
									  	bean.setReferencia(referencia);
									  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_BUSQUEDA_EMPL);
									  	bean.setConcepto(BitaConstants.ES_NOMINA_BUSQUEDA_EMPL_MSG);
									  	bean = bh.llenarBeanTCT(bean);
									    BitaHandler.getInstance().insertBitaTCT(bean);
						    		}catch(SQLException e){
						    			e.printStackTrace();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}
								EIGlobal.mensajePorTrace("TERMINA BITACORIZACION TCT BUSQUEDA EMPLEADO",EIGlobal.NivelLog.INFO);*/
								//</VC>
//								//VC ESC crear Archivo
								archivoCatNomina = session.getUserID8()+ CatNominaInterbConstantes._ARCHIVO_NOMINA;
								EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] Nombre Archivo:  " + archivoCatNomina ,EIGlobal.NivelLog.DEBUG);
								if(!arrDatosQuery[0][0].equals(CatNominaInterbConstantes._ERROR))
								{
									if(crearArchivo(arrDatosQuery,archivoCatNomina) != null)
										sessionHttp.setAttribute("archivoCatNomina",archivoCatNomina);
								}
								// /VC
								sessionHttp.setAttribute("arrDatosQuery", arrDatosQuery);

								tablaEmpleados = action.consultaEmpleados(
										arrDatosQuery,
										inicioBloque,
										finBloque,
										tamanoBloque,
										totalReg,
										numeroPagina,
										numeroPaginas);
								/*jp@everis*/

								if(tablaEmpleados.equals("")){
									EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - No hubo registros",EIGlobal.NivelLog.DEBUG);
									request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La b&uacute;squeda configurada, no gener&oacute resultados<br> Intente con otros valores\", 1);");
									tablaEmpleados = null;
								}
								request.setAttribute("codigoTabla", tablaEmpleados);
								request.setAttribute("valorBusqueda", valorBusqueda);
								request.setAttribute("radioValue", radioValue);
							} // If Manejo 2
							else {
								generaPantallaError(request, response, "Error inesperado. Fallo en la operaci&oacute;n con la Base de Datos. <br> Intente m&aacute;s tarde", "B&uacute;squeda");
								errorExcepcion = true;
							}
						} //if Manejo 1
						else{
							generaPantallaError(request, response, "Error inesperado. Fallo en la operaci&oacute;n con la Base de Datos. <br> Intente m&aacute;s tarde", "B&uacute;squeda");
							errorExcepcion = true;
						}
					}
					else
					{
						numeroPagina = Integer.parseInt(request.getParameter("numeroPagina").toString());
						numeroPaginas = Integer.parseInt(request.getParameter("numeroPaginas").toString());
						inicioBloque =  Integer.parseInt(request.getParameter("inicioBloque").toString());
						finBloque = Integer.parseInt(request.getParameter("finBloque").toString());
						totalReg = Integer.parseInt(request.getParameter("totalReg").toString());
						arrDatosQuery = (String[][])sessionHttp.getAttribute("arrDatosQuery");
						/* Invoca metodo para obtener el arreglo general de los empleados */
						tablaEmpleados = action.consultaEmpleados(
								arrDatosQuery,
								inicioBloque,
								finBloque,
								tamanoBloque,
								totalReg,
								numeroPagina,
								numeroPaginas);
						if(tablaEmpleados.equals("")){
							EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - No hubo registros.",EIGlobal.NivelLog.DEBUG);
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La b&uacute;squeda configurada, no gener&oacute resultados<br> Intente con otros valores\", 1);");
							tablaEmpleados = null;
						}
						request.setAttribute("codigoTabla", tablaEmpleados);
						request.setAttribute("valorBusqueda", valorBusqueda);
						request.setAttribute("radioValue", radioValue);
						request.setAttribute("numeroPagina", String.valueOf(numeroPagina));
						request.setAttribute("numeroPaginas", String.valueOf(numeroPaginas));
						request.setAttribute("inicioBloque", String.valueOf(inicioBloque));
						request.setAttribute("finBloque", String.valueOf(finBloque));
						request.setAttribute("tamanoBloque", String.valueOf(tamanoBloque));
						request.setAttribute("totalReg", String.valueOf(totalReg));
					}
					if(!errorExcepcion)
					{
						existReg = true;
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaInterbBusqueda.jsp", request, response );
					}
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Saliendo de opcion 2",EIGlobal.NivelLog.DEBUG);
					try{
						EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
	    				String claveOperacion = BitaConstants.CONSULTA_CAT_NOMINA_INTERB;
	    				String concepto = BitaConstants.CONCEPTO_CONSULTA_CAT_NOMINA_INTERB;

	    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
	    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

	    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

					} catch(Exception e) {
						EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

					}
				}



				/**************************************************
				 *** Opcion 5 = Realizar Baja de Empleados  ***
				 **************************************************/
				else if(opcion.equals("5"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ( "B&uacute;squeda de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
							"Baja de Empleados", "s37100h", request ) );

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra en opcion 5",EIGlobal.NivelLog.DEBUG);
					boolean fueBaja = false;
					CatNominaInterbAction action = new CatNominaInterbAction();

					String listaEmpleados = request.getParameter("cuentas");
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Empleados Baja " + listaEmpleados,EIGlobal.NivelLog.DEBUG);
					/*****************************************
					 * Inicio Funcionalidad de TOKEN
					 * **************************************/

			        //interrumpe la transaccion para invocar la validaci?n para Alta de proveedor

			       /* if(  valida==null) {
			            EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci?n est? parametrizada para solicitar la validaci?n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			            boolean solVal=ValidaOTP.solicitaValidacion(
			                    session.getContractNumber(),IEnlace.NOMINA);

			            if( session.getValidarToken() &&
			                    session.getFacultad(session.FAC_VAL_OTP) &&
			                    session.getToken().getStatus() == 1 &&
			                    solVal) {
			                EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit? la validaci?n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                //ValidaOTP.guardaParametrosEnSession(req);
			                guardaParametrosEnSession(req);
			                ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			            } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                valida="1";
						}
			        }
			        // retoma el flujo
			        if( valida!=null && valida.equals("1")) {*/

			 		String facultad = "";
			 		// Se validan las mismas facultades que para las altas
			 		if (session.getFacultad(session.FAC_AUTO_AB_OB))
			 			facultad = "AUTO";
			 		else
			 		if(session.getFacultad(session.FAC_ALTA_BN))
			 			facultad = "ALTA";


						fueBaja = action.bajaCtaEmplOnline(listaEmpleados, numContrato, usuario, facultad);
						EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Baja Exitosa: " + fueBaja,EIGlobal.NivelLog.DEBUG);
						if(fueBaja)	{
							System.out.println("CatalogoNominaEmpl  $ [defaultAction] - La baja del empleado Exitosa");
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							// JGAL Se comenta temporalmente
							/*try{

								String tipoCuenta = request.getParameter("rtipoCuenta");
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BAJA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean.setReferencia(referencia);
							 	if(CatNominaConstantes._CTA_INTERNA.equals(tipoCuenta)){
							 		bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_EMPL_LINEA);
							 		bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_EMPL_LINEA_MSG);
							  	}
							 	else{
							 		bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_INTERBAN_LINEA);
							 		bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_INTERBAN_LINEA_MSG);
							 	}

							  	bean = bh.llenarBeanTCT(bean);
							    BitaHandler.getInstance().insertBitaTCT(bean);
				    		}catch(SQLException e){
				    			e.printStackTrace();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION BAJA EMPLEADO ONLINE",EIGlobal.NivelLog.INFO);
				    		//</VC>
				    		 *
				    		 */
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"La baja del empleado se realiz&oacute <br> exitosamente.\", 1);");
							log ( "BITACORIZACION" );
							//Se comenta temporalmente
							//modificaPropiaCN(usuario, numContrato,  request);
							evalTemplate("/jsp/CatNominaInterbBusqueda.jsp", request, response );

						}
						else{
							EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - La baja del empleado NO EXITOSA",EIGlobal.NivelLog.DEBUG);
							generaPantallaError(request, response, "Fallo en la realizaci&oacute;n de la baja, posible rechazo. <br> Favor de revisar en la opci&oacute;n de Autoriza&oacute;n y Cancelaci&oacute;n.", "Baja en l&iacute;nea");
						}
					//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Sale de opcion 5",EIGlobal.NivelLog.DEBUG);
					try{
						EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
	    				String claveOperacion = BitaConstants.BAJA_CAT_NOMINA_INTERB;
	    				String concepto = BitaConstants.CONCEPTO_BAJA_CAT_NOMINA_INTERB;

	    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
	    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

	    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

					} catch(Exception e) {
						EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

					}

				}



				/********************************************************
				 *  Opcion = 6 - Baja Logica de Empleados por archivo 	*
				 ********************************************************/
				else if(opcion.equals("6"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ("Baja de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
									"Baja  por Archivo", "s37130h", request ) );
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra a opcion 6",EIGlobal.NivelLog.DEBUG);
					evalTemplate("/jsp/CatNominaInterbBajaArchivo.jsp", request, response );
					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Sale de opcion 6",EIGlobal.NivelLog.DEBUG);
				}



				/******************************************************************
				 *  Opcion = 7 - Realiza la lectura del archivo con las cuentas
				 *  			que seran dadas de baja del catalogo de nomina.
				 *  			Se realizaron las respectivas validaciones
				 *******************************************************************/
				else if(opcion.equals("7"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ("B&uacute;squeda de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
							"Baja por Archivo", "s37130h", request ) );

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra a opcion 7",EIGlobal.NivelLog.DEBUG);
					String tabla = "";
					CatNominaInterbBean bean = new CatNominaInterbBean();
					try {
						CatNominaInterbAction action = new CatNominaInterbAction();
						CatNominaInterbConstantes cons = new CatNominaInterbConstantes();
						getFileInBytes(request);
						String  nombreArchivo= (String)request.getAttribute("Archivo");//varible con el nombre del archivo que se esta mandando a llamar
						EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - NombreArchivo: "+ nombreArchivo, EIGlobal.NivelLog.DEBUG);

						if(nombreArchivo!=null)
						{
							ArrayList erroresArchivoModif;
							bean = action.validaArchivoBaja( rutadeSubida+"/" + nombreArchivo, numContrato, request);

							if(!bean.getFalloInesperado())
							{
								erroresArchivoModif = bean.getListaErrores();

								if(erroresArchivoModif.size() == 0)
								{
									listaBajaFinal = bean.getListaRegistros();

									if (bean.getTotalRegistros() <= 10) {
										tabla = action.creaDetalleImport(numContrato, bean.getListaRegistros(), cons._BAJA );
										request.setAttribute("TablaRegistros", tabla);
									}
									else {
										String lineaCant = "";
													lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
													lineaCant += "<tr><td>&nbsp;</td></tr>";
													lineaCant += "<tr>";
													lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
													lineaCant += "El total de registros a dar de baja son: " + bean.getTotalRegistros() + " <br><br>";
													lineaCant += "</td>";
													lineaCant += "</tr>";
													lineaCant += "</table>";

										request.setAttribute("LineaCantidad", lineaCant);
									}
								}
								else
								{
									String Linea = "";
									Iterator it = erroresArchivoModif.iterator();
									int  i = 0;
									while (it.hasNext()) {
										if(i==0){
											it.next();
										}
										i++;

										if(erroresArchivoModif.get(0).equals("Long")){
											EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Longitud", EIGlobal.NivelLog.DEBUG);
											Linea += "Longitud de Linea Incorrecta: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivoModif.get(0).equals("Formato")){
											EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Datos", EIGlobal.NivelLog.DEBUG);
											Linea += "Tipo de dato incorrecto: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
										else if(erroresArchivoModif.get(0).equals("Cuenta")){
											EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Cuentas"+ nombreArchivo, EIGlobal.NivelLog.DEBUG);
											Linea += "Cuenta Inexistente: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
										}
									}

									request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>"
											+ Linea + ".\", 1);");
								}
							}// Error Inesperado
							else
							{
								generaPantallaError(request, response, "Fallo en la Baja de registros en la Base de Datos. <br>Favor de Intentar mas tarde.", "Baja por Archivo");
							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					if(!bean.getFalloInesperado()){
						evalTemplate("/jsp/CatNominaInterbBajaArchivo.jsp", request, response );
					}

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Sale a opcion 7",EIGlobal.NivelLog.DEBUG);

				}

				/******************************************************************
				 *  Opcion 8=  - Da de Baja a un empleado del nuevo catalogo de
				 *  			nomina interbancaria
				 *******************************************************************/
				else if(opcion.equals("8"))
				{
					request.setAttribute ("Encabezado", CreaEncabezado ("B&uacute;squeda de Empleados de N&oacute;mina Interbancaria", "Servicios > N&oacute;mina Interbancaria > Registro de Cuentas > " +
							"B&uacute;squeda", "s37130h", request ) );

					EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Entra a opcion 8",EIGlobal.NivelLog.DEBUG);

					CatNominaInterbAction action = new CatNominaInterbAction();
					boolean actualizaciones = false;

				/*****************************************
				 * Inicio Funcionalidad de TOKEN
				 * **************************************/

		            //interrumpe la transaccion para invocar la validaci?n para Alta de proveedor

		           /* if(  valida==null) {
		                EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transacci?n est? parametrizada para solicitar la validaci?n con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

		                boolean solVal=ValidaOTP.solicitaValidacion(
		                        session.getContractNumber(),IEnlace.NOMINA);

		                if( session.getValidarToken() &&
		                        session.getFacultad(session.FAC_VAL_OTP) &&
		                        session.getToken().getStatus() == 1 &&
		                        solVal) {
		                    EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit? la validaci?n. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
		                    //ValidaOTP.guardaParametrosEnSession(req);
		                    guardaParametrosEnSession(req);
		                    ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
		                } else {
							ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
		                    valida="1";
						}
		            }
		            // retoma el flujo
		            if( valida!=null && valida.equals("1")) {*/
						actualizaciones = action.bajaCtaEmplArchivo(numContrato, usuario, listaBajaFinal);

						if(!actualizaciones) {
							System.out.println("bajaArchhivo -[defaultAction()]  Error en las cuentas del archivo--");
							generaPantallaError(request, response, "Fallo en la baja de empleados en la Base de Datos. Favor de Intentar m&aacute;s tarde", "Baja por Archivo");
						}

						else {
							request.setAttribute("MensajeLogin01", " cuadroDialogo(\"Se dieron de baja satisfactoriamente las cuentas.\", 1);");
							System.out.println("bajaArchivo --[defaultAction()]   El archivo se actualizo correctamente --");
							log ( "BITACORIZACION" );
							//Se comenta temporalmente
							//modificaPropiaCN(usuario, numContrato,  request);
							//<VC autor ="ESC" fecha ="17/07/2008" descripcion="Registro de bitacora de operaciones">
							// Se comenta temporalmente

							/*try{
						       EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT BAJA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);
							  	int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + referencia  ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean.setReferencia(referencia);
							  	bean.setTipoOperacion(BitaConstants.ES_NOMINA_BAJA_EMPL_ARCH);
							  	bean.setConcepto(BitaConstants.ES_NOMINA_BAJA_EMPL_ARCH_MSG);
							  	bean = bh.llenarBeanTCT(bean);
							    BitaHandler.getInstance().insertBitaTCT(bean);
				    		}catch(SQLException e){
				    			e.printStackTrace();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}*/
				    		EIGlobal.mensajePorTrace("TERMINA BITACORIZACION SALTO BAJA EMPLEADO ARCHIVO",EIGlobal.NivelLog.INFO);

				    		//</VC>
							evalTemplate("/jsp/CatNominaInterbBajaArchivo.jsp", request, response );
						}


           			//}
						/*****************************************
						 * Finaliza Funcionalidad de TOKEN
						 * **************************************/

						EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Sale a opcion 8",EIGlobal.NivelLog.DEBUG);
						try{
							EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

							int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		    				String claveOperacion = BitaConstants.BAJA_CAT_NOMINA_INTERB;
		    				String concepto = BitaConstants.CONCEPTO_BAJA_CAT_NOMINA_INTERB;

		    				EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
		    				EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);

		    				String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

						} catch(Exception e) {
							EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

						}

					}

				}/* everis JARA - Ambientacion - CERRANDO EL IF de sesion valida*/

			}/* everis JARA - Ambientacion - En caso de error GENERAL*/
		  	catch(Exception e){
		  		EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [defaultAction] - Hbo un error: " + e,EIGlobal.NivelLog.DEBUG);
		  		e.printStackTrace();

		  		despliegaPaginaError("Error inesperado en el Servidor.",
						"Cat&aacute;logo de N&oacute;mina",
						"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de N&oacute;mina",
						request, response );
		  	}
		}
	/*******************************************************************/
	/** Metodo: getFileInBytes para realizar importaciones de archivos.
	 * @return byte[]
	 * @param HttpServletRequest -> request
	/********************************************************************/
	public byte[] getFileInBytes ( HttpServletRequest request) throws IOException, SecurityException
	{
		String sFile = "";
	    System.out.println("CatalogoNominaEmpl  $ [GetFileInBytes] - Inicio ");
	    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [GetFileInBytes] - Inicio",EIGlobal.NivelLog.DEBUG);
		//MultipartRequest mprFile = new MultipartRequest ( request, rutadeSubida, 2097152 );

		FileInputStream entrada;

		sFile = getFormParameter(request, "fileName");
	    //sFile = mprFile.getFilesystemName ( name );
	    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [GetFileInBytes] - S1 : " + sFile,EIGlobal.NivelLog.DEBUG);
	    request.setAttribute("Archivo", sFile);


		 File fArchivo = new File ( ( rutadeSubida + "/" + sFile ).intern () );
		 byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
		 entrada = new FileInputStream ( fArchivo );
		 byte bByte = 0;
		 try{
		 for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
		     aBArchivo[ i ] = bByte;
		 }
			 } catch(Exception e) {
				 e.printStackTrace();
				 throw new IOException();
		 } finally {
			 entrada.close ();
			     //fArchivo.delete ();
		 }

		 //Enumeration e = mprFile.getParameterNames();
		 HashMap parametros = getFormParameters(request);
		 Iterator e = parametros.entrySet().iterator();

		 while(e.hasNext()) {
			 String parametro = (String) ((Map.Entry) e.next()).getKey();
			 //System.out.println("CatalogoNominaEmpl  $ [defaultAction] - paramtro: "+ parametro );
			 request.setAttribute(parametro, parametros.get(parametro));
		 }
	    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [GetFileInBytes] - Sin" ,EIGlobal.NivelLog.DEBUG);

		 return aBArchivo;
	}


		/***********************************************/
		/** Metodo: altaPropiaCN
		 ** Bitacorizaci?n para Alta de registro
		/***********************************************/
/*	    public static void altaPropiaCN (String usuario, String contrato, HttpServletRequest request) throws ServletException, IOException
	    {
	    	HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");

			if(Global.USAR_BITACORAS.trim().equals("ON")){
				System.out.println("La operaci?n es un alta" );
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es: " + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("El contrato: " + bt.getContrato());

				try{
					System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);
					bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_ALTA);
					System.out.println("Se realiza la bitacorizacion:" + bt.getNumBit());
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}

*/
		/**********************************************/
		/** Metodo: consultaPropiaCN
		 ** Bitacorizaci?n para Consulta de registro
		/**********************************************/
/*		public void consultaPropiaCN(String usuario, String contrato, HttpServletRequest request)throws ServletException, IOException
		{
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			System.out.println("La operaci?n es una consulta");

			if(Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es: " + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("Los datos a consultar: " + bt.getContrato());

				try{
					System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);
					bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_CONSULTA);
					System.out.println("Se realiza la bitacorizacion:" + bt.getNumBit());
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}

*/
		/*************************************************/
		/** Metodo: modificaPropiaCN
		 ** Bitacorizaci?n para Modificacion de registro
		/*************************************************/
	/*	public void modificaPropiaCN(String usuario, String contrato, HttpServletRequest request)throws ServletException, IOException
		{
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			System.out.println(" La operaci?n es una modificacion");

			if(Global.USAR_BITACORAS.trim().equals("ON")){
				BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
				bh.incrementaFolioFlujo("ECNP");
				//BitaTransacBean bt = new BitaTransacBean();
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setUsr(usuario);
				System.out.println("El usuario es: " + bt.getUsr());
				bt.setContrato(contrato);
				System.out.println("Los datos a consultar: " + bt.getContrato());

				try{
					System.out.println("BitaConstants.SESS_ID_FLUJO:" + BitaConstants.SESS_ID_FLUJO);
						bt.setNumBit(BitaConstants.EC_NOMINA_PROPIA_MODIFICA);
						System.out.println(" Se realiza la bitacorizacion:" + bt.getNumBit());
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException e){
					e.printStackTrace();
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}

*/
		/*****************************************************
		 ** Metodo que genera pantalla de Error, para
		 ** el manejo de excepciones por errores inesperados.
		*****************************************************/
		public void generaPantallaError(HttpServletRequest request, HttpServletResponse response, String mensaje, String funcion) throws Exception
		{
		    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [generaPantallaError] - Inicio",EIGlobal.NivelLog.DEBUG);
			System.out.println("CatalogoNominaEmpl $ [generaPantallaError] - Inicio");
			despliegaPaginaError(mensaje, "Cat&aacute;logo de N&oacute;mina",
			"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de Empleados  &gt; " + funcion,
			request, response );
		    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [generaPantallaError] - Fin",EIGlobal.NivelLog.DEBUG);
		}

		/***************************************************************
		 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
		 *** Metodo para exportar el catlogo de nomina a un archivo ****
		 **************************************************************/
		public String crearArchivo(String arrDatosQuery [][],String nombre)
		{
		    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [crearchivo] - Inicio",EIGlobal.NivelLog.DEBUG);
			String nomArchivo = null;
			String tmp="";
			int longitudes [] = {20,40,16};
			if(nombre !=null || nombre != "")
			{
			 nomArchivo = Global.DIRECTORIO_LOCAL + "/" + nombre;
			 EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [crearchivo] - Generando Archivo  " + nomArchivo,EIGlobal.NivelLog.DEBUG);
			 try {
		            File Exportado = new File(nomArchivo);

		            boolean Creado = Exportado.createNewFile();
		            if (!Creado) {
		                Exportado.delete();
		                Exportado.createNewFile();
		            }
		            FileWriter writer = new FileWriter(Exportado);
		            StringBuffer Linea = new StringBuffer ("");
					int j = 0;
					int i = 0;
					for (i = 0 ; i < arrDatosQuery.length ; i++) {
						Linea = new StringBuffer ("");
						Linea.append (rellenar(arrDatosQuery[i][1],"D",longitudes[0],' '));
						Linea.append (rellenar(arrDatosQuery[i][4],"D",longitudes[1],' '));
						Linea.append (rellenar(arrDatosQuery[i][8],"D",longitudes[2],' '));
						System.out.println(Linea.toString());
						Linea.append ("\n");
						writer.write(Linea.toString());
					}
					writer.flush ();
		            writer.close ();

		            //ESC 17/jun/2008 Correccion de envio de archivo remoto para poder ser descargado

		            ArchivoRemoto archRem = new ArchivoRemoto();
		            if(archRem.copiaLocalARemoto(nombre,"WEB"))
		            {
		            	nomArchivo = nombre;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Se creo el archivo remoto " + nomArchivo,EIGlobal.NivelLog.INFO);
		            }
		            else{
		            	nomArchivo = null;
		            	EIGlobal.mensajePorTrace("***CatalogoNominaEmpl.class Error al crear el archivo remoto remoto " + nomArchivo ,EIGlobal.NivelLog.INFO);
		            }

		        } catch (FileNotFoundException ex1) {
		        	nomArchivo = null;
		            System.err.println (ex1.getMessage ());
		            ex1.printStackTrace (System.err);
		        } catch (IOException ex2) {
		        	nomArchivo = null;
		            System.err.println(ex2.getMessage());
		            ex2.printStackTrace(System.err);
		        }
			}
		    EIGlobal.mensajePorTrace("CatalogoNominaEmplInterb - $ [crearchivo] - Fin",EIGlobal.NivelLog.DEBUG);
		    return nomArchivo;
		}

		/***************************************************************
		 *** Autor: Emmanuel Sanchez Castillo 04 jun 2008			****
		 *** Metodo para exportar el catlogo de nomina a un archivo ****
		 **************************************************************/

		public String rellenar(String cadena,String pos,int tamanio ,char carac )
		{
			int lon;
			String aux1="";
			if(cadena == null)
				cadena ="";

			lon = cadena.length();
			if(lon > tamanio)//Validamos la longitud de la cadena, si es mayor al tama?o deseado se corta
				cadena = cadena.substring(0,tamanio);

			lon = (tamanio - lon < 0 ? 0 : tamanio - lon ); //obtenemos la longitud de la cadena de relleno

			for(int i = 1 ; i <= lon ; i++)
			{
				aux1 += carac;
			}

			//validamos si se rellena a la izquierda o a la derecha
			if("I".equals(pos))
				aux1 = aux1 + cadena;
			else
				aux1 = cadena + aux1;

			return aux1;
		}

}