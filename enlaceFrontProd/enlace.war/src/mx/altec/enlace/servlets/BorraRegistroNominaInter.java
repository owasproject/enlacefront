/**Banco Santander Mexicano
* Clase	BorraRegistroNomina, Modificacion al archivo de	pago de	nomina
* @autor Rodrigo Godo
* @version 1.1
* fecha	de creacion: Diciembre 2000	- Enero	20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: verifica	cual de	las	operaciones	de mantenimiento de	archivo	fue	seleccionada y ejecuta
				dicha operacion, para ello se utilizan los metodos ejecutaAltas(), ejecutaBajas() y
				ejecutaModificaciones()
*/

package	mx.altec.enlace.servlets;

import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import javax.sql.*;

import mx.altec.enlace.bo.ArchivosNominaInter;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.ValidaArchivoPagos;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.dao.CatNomValidaInterbDao;



public class BorraRegistroNominaInter extends BaseServlet {

	public void doGet( HttpServletRequest request, HttpServletResponse response	)
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		boolean sesionvalida = SesionValida( request, response );
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String strOperacion = "";
		String[] numerosEmpleados = null;
		Vector diasNoHabiles = null;
		//String[] cuentas = null;
		if ( sesionvalida ) {
			//***********************************************
			//modificación para integración pva 07/03/2002
            //***********************************************

			//Vector diasNoHabilesJS;
			GregorianCalendar fechaHoy;
			CalendarNomina nomCalendar;


			session.setModuloConsultar(IEnlace.MEnvio_pagos_nom_IN);

			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo execute()&", EIGlobal.NivelLog.INFO);
			//request.setAttribute("MenuPrincipal", session.getstrMenu());
			//diasNoHabilesJS = new Vector();
			fechaHoy= new GregorianCalendar();
			nomCalendar = new CalendarNomina( );
			request.setAttribute("Fecha", ObtenFecha());
			request.setAttribute("ContUser", ObtenContUser(request));
			String laFechaHoy=EIGlobal.formatoFecha(fechaHoy,"aaaammdd");
			EIGlobal.mensajePorTrace( "***BorraRegistroNominaInter.class &la fecha que se enviara como la fecha del sistema es: "+laFechaHoy+"&", EIGlobal.NivelLog.INFO);
			request.setAttribute( "fechaHoy", laFechaHoy );

			String VarFechaHoy = "";
			String strInhabiles ="";
			try
			{
			  diasNoHabiles = nomCalendar.CargarDias();
			  strInhabiles = nomCalendar.armaDiasInhabilesJS();
			}catch(ArrayIndexOutOfBoundsException e)
			 {
				EIGlobal.mensajePorTrace("*** BorraRegistroNominaInter.class Error al crear los dias inhabiles", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("*** BorraRegistroNominaInter.class MSG "+e.getMessage(), EIGlobal.NivelLog.INFO);
				strInhabiles ="";
			 }
			Calendar fechaFin = nomCalendar.calculaFechaFin();
			VarFechaHoy = nomCalendar.armaArregloFechas();
			String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
			String Cuentas = "";
			String tipo_cuenta = "";
			String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
			datesrvr +=	"<Input	type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
			datesrvr +=	"<Input	type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";

			String cuentaCargo = session.getCuentaCargo();
			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class valor de cuentaCrgo: "+ cuentaCargo, EIGlobal.NivelLog.INFO);
			request.setAttribute( "textcuenta",cuentaCargo);
			request.setAttribute( "VarFechaHoy",VarFechaHoy);
			request.setAttribute( "fechaFin",strFechaFin);
			request.setAttribute( "diasInhabiles",strInhabiles);
			request.setAttribute( "Fechas",ObtenFecha(true));
			request.setAttribute( "FechaHoy",ObtenFecha(false) + " - 06" );

			String descargaArchivo = session.getRutaDescarga();
			String archivoName = (String )sess.getAttribute("nombreArchivo");
			String nombreArchivoOriginal = archivoName.substring( archivoName.lastIndexOf( "/" ) + 1 );
			request.setAttribute( "DescargaArchivo", session.getRutaDescarga() );

			//////se determina la hora del servidor///////////////////////////
			//////////////////////////////////////////////////////////////////
			String systemHour=ObtenHora();
			/**/request.setAttribute("horaSistema", systemHour);			//
			//////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////

			//request.setAttribute("DescargaArchivo",descargaArchivo);
			if ( request.getParameter( "operacion" ).equals( "baja" ) ) {
				ejecutaBajas( request, response, strOperacion,  numerosEmpleados, cuentaCargo);
			}
			if ( request.getParameter( "operacion" ).equals( "alta" ) ) {
				ejecutaAltas( request, response, strOperacion, numerosEmpleados , cuentaCargo);
			}
			if ( request.getParameter( "operacion" ).equals( "modificacion" ) ) {
				ejecutaModificacion( request, response, strOperacion, numerosEmpleados , cuentaCargo);
			}
		} else if ( sesionvalida ) {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response );
		}
		if(diasNoHabiles != null) {
			diasNoHabiles.clear();
			diasNoHabiles = null;
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Saliendo del metodo execute()&", EIGlobal.NivelLog.INFO);
	}

	//Metodo que ejecuta bajas en el archivo
	public void ejecutaBajas( HttpServletRequest request, HttpServletResponse response, String strOperacion,String[] numerosEmpleados, String cuentaCargo)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo ejecutaBajas: &", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    ValidaArchivoPagos validaFile = null;
	    String[] cuentas = null;
		String contenidoArchivoStr="";
		String strRegistro = request.getParameter("registro");
		strOperacion= request.getParameter("operacion");
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class #La cadena strRegistro es: #"+strRegistro, EIGlobal.NivelLog.DEBUG);
		long longRegistro=0;

		try{
			longRegistro=Long.parseLong(strRegistro);
		} catch(NumberFormatException e) {
			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Ha ocurrido una excepcion en: %ejecutaBajas()>>"+e.toString()+"<<", EIGlobal.NivelLog.INFO);
		}
		//String nombreArchivoEmp=session.getNameFileNomina();
		String nombreArchivoEmp = (String )sess.getAttribute("nombreArchivo");
		validaFile=new ValidaArchivoPagos(this, nombreArchivoEmp);
		String elArchivoNomina	= "";//nomArchNomina
		String elArchivo		= "";//nombreOriginal
		//File nominaFile= new File(session.getNameFileNomina());
		File nominaFile= new File((String )sess.getAttribute("nombreArchivo"));
		cuentas=arregloCuentas(nominaFile, Integer.parseInt(session.getTotalRegsNom()));
		numerosEmpleados=arregloNumeroEmpleado(nominaFile, Integer.parseInt(session.getTotalRegsNom()));
//		RandomAccessFile myNominaFile= new RandomAccessFile(nominaFile, "rw");

		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &El archivo que se generara es: "+elArchivoNomina+"&", EIGlobal.NivelLog.INFO);
		//ArchivosNominaInter es la clase que representa al archivo de pago de nomina
		//ArchivosNominaInter archivo3 = new ArchivosNominaInter(session.getNameFileNomina(),request);
		ArchivosNominaInter archivo3 = new ArchivosNominaInter((String )sess.getAttribute("nombreArchivo"),request);
		//se hacen las actualizaciones al sumario
		String Importe=request.getParameter("Importe");
		Importe=formateaImporte(Importe);
		archivo3.actualizaSumario("sumSecuencial",session.getLastSecNom(),false,request);
		archivo3.actualizaSumario("totalRegs",session.getTotalRegsNom(),false,request);
		archivo3.actualizaSumario("impTotal",Importe,false,request);

		//el metodo bajaRegistro() existe dentro de la clase ArchivosNominaInter y debe dar de baja fisicamente al registro seleccionado
		//se usa como argumento del metodo la posicion del registro que se eliminiara
		archivo3.bajaRegistro(longRegistro,request);

		archivo3.actualizaConsecutivos(Integer.parseInt(session.getTotalRegsNom()),request);

		contenidoArchivoStr= "" + archivo3.lecturaArchivo();
		//String archivoName=session.getNameFileNomina();
		String archivoName=(String )sess.getAttribute("nombreArchivo");
		request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
		request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria ","Servicios &gt; N&oacute;mina &gt; Altas","s25830h_b",request));
		//request.setAttribute("facultades",session.getFacultadesNomina());
		request.setAttribute("facultades",sess.getAttribute("facultadesN"));
		request.setAttribute("textcuenta",cuentaCargo);
		request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
		session.setCuentasArchivo(request.getParameter("CuentasArchivo"));
		//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado"));
		sess.setAttribute("numeroEmp",request.getParameter("NumerosEmpleado"));

		evalTemplate(IEnlace.MTO_NOMINAINTER_TMPL, request, response );
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Fin del metodo ejecutaBajas()&", EIGlobal.NivelLog.INFO);
	}//fin del metodo ejecutaBajas

	//metodo que da de alta registrso en el archivo
	public void ejecutaAltas( HttpServletRequest request, HttpServletResponse response, String strOperacion, String[] numerosEmpleados , String cuentaCargo)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo ejecutaAltas()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    String[] cuentas = null;
	    ValidaArchivoPagos validaFile = null;
		String contenidoArchivoStr = "";
		String adicionales = "";
		String zeros = "";

		String valorCuentaClabe   = "<input type=radio value=0 name=TipoCuenta checked>Cuenta CLABE";
		String valorCuentaTarjeta = "<input type=radio value=0 name=TipoCuenta >Tarjeta de D&eacute;bito";

		StringBuffer sb = new StringBuffer();
		//File nominaFile = new File( session.getNameFileNomina() );
		File nominaFile = new File(	(String )sess.getAttribute("nombreArchivo") );
		//se forma el arreglo de cuentas que se usara para comparar que una cuenta nueva no exista previamente dentro del archivo
		cuentas = arregloCuentas( nominaFile, Integer.parseInt( session.getTotalRegsNom() ) );
		//se forma el arreglo de numeros de empleado que se usara para comparar que una nuevo numero de empleado no exista previamente dentro del archivo
		numerosEmpleados = arregloNumeroEmpleado( nominaFile, Integer.parseInt( session.getTotalRegsNom() ) );

		strOperacion= request.getParameter("operacion");
		//validaFile = new ValidaArchivoPagos( this, session.getNameFileNomina() );
		validaFile = new ValidaArchivoPagos( this, (String )sess.getAttribute("nombreArchivo") );
		//clabe	 ******************
		//VALIDACIONES
		//la variable NumeroEmpleado asi como otras que se invocan mediante 'valIn' provienen de cuadros de
		//dialogo existentes en el template
		String TipoCuenta = request.getParameter("TipoCuenta");
		//*******************
        String NombreEmpleado = request.getParameter("NombreEmpleado");
	    String ClaveBanco = request.getParameter("Banco");
		ClaveBanco = ClaveBanco.substring (0, 5);
	    String PlazaBanxico = request.getParameter("valorPlaza");
		String NumeroCuenta = request.getParameter("NumeroCuenta");
        String Importe = request.getParameter("Importe");
        Importe = formateaImporte(Importe);


		/**
		* Facultad para cuentas no registradas
		* Busca la cuenta de cargo en las registradas
		* y verificar la facultad a cuentas no registradas
		*/

		/* Para la busqueda de cuentas */
	    llamado_servicioCtasInteg(IEnlace.MDep_Inter_Abono," "," "," ",session.getUserID8()+".ambca",request);

		Hashtable Facultades=(Hashtable)sess.getAttribute("FacultadesNominaInterbancaria");
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Facultades se obtienen: " + Facultades.size(), EIGlobal.NivelLog.INFO);
		boolean Acceso=((Boolean)Facultades.get("CuentasNoReg")).booleanValue();
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Acceso a cuentas no registradas:  " + Acceso, EIGlobal.NivelLog.INFO);

		//String datoscta[]=null;
		String errMsg="";
		//datoscta=BuscandoCtaInteg(NumeroCuenta.trim(),IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambca",IEnlace.MDep_Inter_Abono);
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Numero de cuenta: " + NumeroCuenta, EIGlobal.NivelLog.INFO);
		//EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Resultado: " + datoscta, EIGlobal.NivelLog.INFO);

		CatNomValidaInterbDao dao = new CatNomValidaInterbDao();
		try {
			dao.Crear();
		} catch(Exception e) {}
		boolean existeEnCatalogo = dao.existeEnCatalogo(session.getContractNumber(), NumeroCuenta.trim());
		dao.Cerrar();
		dao = null;

		if(!existeEnCatalogo && !Acceso) {
			  EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class La cuenta de Abono No Existe y no se tiene facultad.", EIGlobal.NivelLog.INFO);
			  errMsg="No se tiene facultad para cuentas no registradas";
			  request.setAttribute("Mensaje","cuadroDialogo('No se tiene facultad para cuentas no registradas. El registro no se agreg&oacute;', 3);");

			  EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point exit.1", EIGlobal.NivelLog.INFO);
			  request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25480.gif");
			  request.setAttribute("CuentaClabe","");
			  request.setAttribute("CuentaTarjeta" ,"");
			  request.setAttribute("CuentasArchivo", request.getParameter("CuentasArchivo"));
		}
		else
		   {
			   if(!existeEnCatalogo && !Acceso) {
				  EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class La cuenta de Abono No Existe en el catalogo de empleados", EIGlobal.NivelLog.INFO);
				  errMsg="Favor de registrar la cuenta de abono en el Cat&aacute;logo de Empleados.";
				  request.setAttribute("Mensaje","cuadroDialogo('Favor de registrar la cuenta de abono en el Cat&aacute;logo de Empleados. El registro fue aceptado.', 1);");

				  EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point exit.1", EIGlobal.NivelLog.DEBUG);
				  request.setAttribute("archivoImagen","/gifs/EnlaceMig/gic25020.gif");
				  request.setAttribute("CuentaClabe","");
				  request.setAttribute("CuentaTarjeta" ,"");
				  request.setAttribute("CuentasArchivo", request.getParameter("CuentasArchivo"));
			   }
		        //EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class CCBDatoscta = " + datoscta, EIGlobal.NivelLog.INFO);

				String concepto = request.getParameter("Concepto");
				if (concepto.length() < 40)
					for(int i = 0; i < 40-concepto.trim().length(); i++)
						concepto = concepto + " ";
				String referencia = request.getParameter("Referencia");
				if (referencia.length() < 7)
					for(int i = 0; i < 7-referencia.trim().length(); i++)
						referencia = referencia + " ";

				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 1", EIGlobal.NivelLog.INFO);

								contenidoArchivoStr= "<br>NombreEmpleado ="  + NombreEmpleado +", long=" + NombreEmpleado.length() + "<br> "+
									 "ClaveBanco =" +ClaveBanco +", long=" + ClaveBanco.length() + "<br> "+
									 "PlazaBanxico =" +PlazaBanxico +", long=" + PlazaBanxico.length() + "<br> "+
									 "NumeroCuenta =" +NumeroCuenta +", long=" + NumeroCuenta.length() + "<br> "+
									 "Importe =" +Importe +", long=" + Importe.length() + "<br>" +
									 "Concepto =" + concepto + ", long= " + concepto.length() + "<br>" +
									 "Referencia =" + referencia + ", long= " + referencia.length() + "<br>";

				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 2", EIGlobal.NivelLog.INFO);

				//la variable consecutivo determinara el valor del campo numeroSecuenacial que existe dentro de los registros de
				//detalle, consecutivo asumira un valor que se tomara directamente de la cantidad de registros de detalle
				//que existan dentro del archivo mediante el metodo  getRegsNom()
				String consecutivo=session.getTotalRegsNom();
				int temp= Integer.parseInt(consecutivo)+2;
				consecutivo=String.valueOf(temp);
				consecutivo= completaCamposNumericos(consecutivo.length(), 5)+consecutivo;
				String cadena= "2"+ consecutivo  + NombreEmpleado + TipoCuenta + "   "+
									NumeroCuenta + Importe + ClaveBanco + PlazaBanxico +
									concepto + referencia;

				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 3", EIGlobal.NivelLog.INFO);

				sb.append("2");
				sb.append(consecutivo);
				sb.append(NombreEmpleado.trim());
				if(NombreEmpleado.length()<55)
					for(int i=0;i<55-NombreEmpleado.trim().length();i++)
						sb.append(" ");
				sb.append(TipoCuenta);
				sb.append(NumeroCuenta);
				sb.append(Importe);
				sb.append(ClaveBanco);
				sb.append(PlazaBanxico);
				sb.append(concepto);
				if (concepto.length() < 40)
					for(int i = 0; i < 40-concepto.trim().length(); i++)
						sb.append(" ");
				sb.append(referencia);
				if (referencia.length() < 7)
					for(int i = 0; i < 40-referencia.trim().length(); i++)
						sb.append(" ");
				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 4", EIGlobal.NivelLog.INFO);
				ArchivosNominaInter archivo4 = new ArchivosNominaInter((String )sess.getAttribute("nombreArchivo"),request);
				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 5", EIGlobal.NivelLog.INFO);
				int totRegs=Integer.parseInt(session.getTotalRegsNom());   //totRegs funciona para conocer la cantidad de regs de detalle
				String registrosTotales=String.valueOf(totRegs);
				registrosTotales=completaCamposNumericos(registrosTotales.length(),5)+registrosTotales;
				archivo4.actualizaSumario("sumSecuencial",consecutivo,true,request);
				archivo4.actualizaSumario("totalRegs",registrosTotales,true,request);
				archivo4.actualizaSumario("impTotal",Importe,true,request);
				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 5.1", EIGlobal.NivelLog.INFO);
				archivo4.altaRegistro(cadena);
				archivo4.agregaRegistro(request);
				request.setAttribute("CuentasArchivo", request.getParameter("CuentasArchivo")+NumeroCuenta.trim()+"|");
				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 7", EIGlobal.NivelLog.INFO);
				archivo4.close();
				ArchivosNominaInter archivo5 = new ArchivosNominaInter((String )sess.getAttribute("nombreArchivo"),request);
				EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 6", EIGlobal.NivelLog.INFO);
		   }

		  request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
		  request.setAttribute("operacion",strOperacion);
		  request.setAttribute("newMenu", session.getFuncionesDeMenu());
		  request.setAttribute("MenuPrincipal", session.getStrMenu());
		  request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria ","Servicios &gt; N&oacute;mina &gt; Altas","s25800h",request));
		  request.setAttribute("archivoImagen","/gifs/EnlaceMig/gbo25480.gif");
		  request.setAttribute("altImagen","Alta");
		  request.setAttribute("comboBancos",comboBancos(request));
		  request.setAttribute("comboPlazas", comboPlazas());
		  String uso_cta_cheques = Global.USO_CTA_CHEQUE; //se obtiene de global
		  request.setAttribute("uso_cta_clabe", uso_cta_cheques);
		  request.setAttribute("textcuenta",cuentaCargo);
		  request.setAttribute("botLimpiar","<td align=left valign=top width=76><a href=javascript:js_limpiar();><img border=0 name=boton src=/gifs/EnlaceMig/gbo25250.gif width=76 height=22 alt=Limpiar></a></td>");
		  request.setAttribute("CuentaClabe",valorCuentaClabe);
		  request.setAttribute("CuentaTarjeta" ,valorCuentaTarjeta);

		  EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Trace Point 7", EIGlobal.NivelLog.INFO);

		  evalTemplate(IEnlace.NOMINADATOSINTER_TMPL, request, response );		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Fin del metodo ejecutaAltas()&", EIGlobal.NivelLog.INFO);
	}

	//metodo que modifica datos en el archivo
	public void ejecutaModificacion( HttpServletRequest request, HttpServletResponse response , String strOperacion, String[] numerosEmpleados, String cuentaCargo)
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo ejecutaModificacion()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    ValidaArchivoPagos validaFile = null;
	    String[] cuentas = null;
		String contenidoArchivoStr="";
	   //File nominaFile= new File(session.getNameFileNomina());
	   File nominaFile= new File((String )sess.getAttribute("nombreArchivo"));
		cuentas=arregloCuentas(nominaFile, Integer.parseInt(session.getTotalRegsNom()));
		numerosEmpleados=arregloNumeroEmpleado(nominaFile, Integer.parseInt(session.getTotalRegsNom()));
		strOperacion= request.getParameter("operacion");
		//validaFile=new ValidaArchivoPagos(this, session.getNameFileNomina());
		validaFile=new ValidaArchivoPagos(this, (String )sess.getAttribute("nombreArchivo"));
		String adicionales="";
		String zeros="";

		//clabe ************
		String TipoCuenta = request.getParameter("TipoCuenta");
		//************
		String NombreEmpleado = request.getParameter("NombreEmpleado");
	    String ClaveBanco = request.getParameter("Banco");
		ClaveBanco = ClaveBanco.substring (0, 3);
	    String PlazaBanxico = request.getParameter("valorPlaza");
		String NumeroCuenta = request.getParameter("NumeroCuenta");
		String Importe = request.getParameter("Importe");
		Importe = formateaImporte(Importe);

		String strRegistro = request.getParameter("registro");
		long longRegistro=0;
		try{ longRegistro=Long.parseLong(strRegistro);}
		catch(NumberFormatException e) {
						EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Error en el formato- ejecutaModificacion()&", EIGlobal.NivelLog.INFO);
						}
		//String nombreArchivoNom=session.getNameFileNomina();
		String nombreArchivoNom=(String )sess.getAttribute("nombreArchivo");
		ArchivosNominaInter archivo4 = new ArchivosNominaInter(nombreArchivoNom,request);
	    String secuencial=String.valueOf(longRegistro);
	    secuencial=completaCamposNumericos(secuencial.length(),5)+secuencial;
	    /*String cadena= "2"+ secuencial  + NumeroEmpleado  +
					         ApellidoPaterno + ApellidoMaterno + Nombre +
					         NumeroCuenta + Importe;			*/
		 //clabe ******
		//String cadena= NombreEmpleado  +"     "+NumeroCuenta + Importe+ClaveBanco+PlazaBanxico;
		String cadena= NombreEmpleado  + TipoCuenta + "   "+NumeroCuenta + Importe+ClaveBanco+PlazaBanxico;
		//***********************

		String anteriorImporte=request.getParameter("oldImporte");
		String Import=request.getParameter("Importe");
		anteriorImporte=formateaImporte(anteriorImporte);
		Import=formateaImporte(Import);
		int dif=Integer.parseInt(Import.trim())-Integer.parseInt(anteriorImporte.trim());
		String diferencia=String.valueOf(dif);
			archivo4.actualizaSumario("impTotal",diferencia,true,request);
			archivo4.edicionRegistros(longRegistro, cadena,request);
		archivo4.close();

		ArchivosNominaInter archivo5 = new ArchivosNominaInter(nombreArchivoNom,request);
		contenidoArchivoStr= "" +archivo5.lecturaArchivo();
		//String archivoName=session.getNameFileNomina();
		String archivoName=(String )sess.getAttribute("nombreArchivo");
		request.setAttribute("archivo_actual",archivoName.substring(archivoName.lastIndexOf("/")+1));
		request.setAttribute("ContenidoArchivo",""+contenidoArchivoStr);
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria","Servicios &gt; N&oacutemina &gt; Pagos","s25800h",request));
		//request.setAttribute("facultades",session.getFacultadesNomina());
		request.setAttribute("facultades",sess.getAttribute("facultadesN"));
		request.setAttribute("textcuenta",cuentaCargo);
		request.setAttribute("cantidadDeRegistrosEnArchivo",session.getTotalRegsNom());
		session.setCuentasArchivo(request.getParameter("CuentasArchivo"));

		//session.setNumerosEmpleado(request.getParameter("NumerosEmpleado"));
			sess.setAttribute("numeroEmp",request.getParameter("NumerosEmpleado"));

		 evalTemplate( IEnlace.MTO_NOMINAINTER_TMPL,	request, response );
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Fin del metodo ejecutaModificaciones()&", EIGlobal.NivelLog.INFO);
	} //fin del metodo ejecutaModificaciones

	//el metodo formateaImporte, permite redondear a dos decimales, y eliminar el punto
	//decimal del campo importe para almacenarlo en el archivo
	public String formateaImporte(String importe){
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo formteaImporte()&", EIGlobal.NivelLog.INFO);
		String importeFormateado="";
		/*int caracteres=0;
		if(importe.indexOf('.')==0)
			salirDelProceso("El importe debe ser mayor de 1 peso o superior", importe);

		for (int i=0; i<importe.length(); i++){
		String punto=String.valueOf(importe.charAt(i));
			if(punto.equals("."))
			caracteres++;
		}
		if (caracteres>1)
			salirDelProceso("Error el el campo Importe", importe);
		else{
			int i=0;
			i=importe.indexOf((int)'.');
			if(i>0){
				if(importe.substring(i,importe.length()).length()==1)
					importe=importe+"00";
				if(importe.substring(i,importe.length()).length()==2)
					importe=importe+"0";
				if(importe.substring(i,importe.length()).length()>3){
					importe=importe.substring(0,importe.indexOf('.')+3);
				}
				importeFormateado=importe.substring(0,i)+importe.substring(i+1,importe.length());
			}
			else{
				importeFormateado=importe+"00";
			}

		}
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Fin del metodo formateaImporte()&", EIGlobal.NivelLog.INFO);*/

		importeFormateado=importe.substring(0,importe.indexOf('.'))+importe.substring(importe.indexOf('.')+1,importe.length());
		return " "+importeFormateado;
			}

	//metodo que agrega espacios en blanco para completar la longitud del campo
	//alfanumerico requerido
	public String completaCampos(int longitudActual, int longitudRequerida){
		//EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo completaCampos()&", EIGlobal.NivelLog.INFO);
	    String blancos =" ";
	    int faltantes= longitudRequerida-longitudActual;
	    for (int i=1; i<faltantes; i++){
		 blancos=blancos+" ";
	    }
		if(faltantes>0)
		    return blancos;
		else
			return "";
	}//fin del metodo completaCampos()

	//metodo que agrega espacios en blanco para completar la longitud del campo
	//numerico requerido
	public String completaCamposNumericos(int longitudActual, int longitudRequerida){
		//EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo completaCamposNumericos()&", EIGlobal.NivelLog.INFO);
	    String ceros="0";
	    int faltantes= longitudRequerida-longitudActual;
	    for(int i=1; i<faltantes; i++){
		 ceros=ceros+"0";
	    }
		if(faltantes>0)
		    return ceros;
		else
			return "";
	}

	public boolean verificaDuplicidadCuentasNuevas(String[] cuentas, String cuentaNueva){
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo verificaDuplicidadCuentasNuevas()&", EIGlobal.NivelLog.INFO);
		boolean cuentaDuplicada=false;
		for(int i=0; i<cuentas.length; i++){
			if(cuentas[i].equals(cuentaNueva)){
				cuentaDuplicada=true;
				break;
			}
		}
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Saliendo del metodo verificaDuplicidadCuentasNuevas()&", EIGlobal.NivelLog.INFO);
		return cuentaDuplicada;
	}

//metodo para visualizar los errores en	una	pagina html
	public boolean salirDelProceso( String s, String variable,
			HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
		String contArchivoStr="";
	    contArchivoStr="<table border=1 align=center>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>Pago de Nomina Interbancaria</b></td></tr>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>Se han detectado errores en sus datos</b></td></tr>";
		contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+s+"</b></td></tr>";
	    contArchivoStr=contArchivoStr+"<tr><td colspan=9 align=center><b>"+variable+"</b></td></tr>";
		contArchivoStr=contArchivoStr+"</tr></table>";
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina Interbancaria ","Servicios &gt; N&oacute;mina &gt; Altas","s25810h",request));
		//request.setAttribute( "facultades", session.getFacultadesNomina() );
		request.setAttribute("facultades",sess.getAttribute("facultadesN"));
	    request.setAttribute( "erroresEnArchivo", "" + contArchivoStr );
		evalTemplate( IEnlace.ERR_NOMINA_TMPL, request,	response );
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Saliendo del metodo salirDelProceso()&", EIGlobal.NivelLog.INFO);
		return false;

	}//fin del metodo salirDelProceso
public String[] arregloCuentas(File nameArchivo, int numeroRegs){
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo arregloCuentas&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo	 = "";
		String[] arreglo		 = new String[numeroRegs];
		try{
			RandomAccessFile archivoLectura=new RandomAccessFile(nameArchivo,"r");
			try{
			archivoLectura.seek(41);
			lineaQueSeLeyo		= archivoLectura.readLine();
			if(lineaQueSeLeyo==null) {
			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &La linea fue nulo&", EIGlobal.NivelLog.INFO);
				lineaQueSeLeyo		= archivoLectura.readLine();
			}
			} catch(Exception e){
				e.printStackTrace();
			}
			if(lineaQueSeLeyo!=null)
				for(int i=0; i<arreglo.length; i++){
					//System.out.println(i+"      "+lineaQueSeLeyo);
					//clabe ************
					if(lineaQueSeLeyo.length()>80) //  para evitar el error
						arreglo[i]		= lineaQueSeLeyo.substring(61,81); // 72
					else
					i--;
					/*if(lineaQueSeLeyo.length()>71) //  para evitar el error
						arreglo[i]		= lineaQueSeLeyo.substring(61,72); // 72*/

					lineaQueSeLeyo	= archivoLectura.readLine();
				}

		} catch(IOException exception){
			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Ha ocurrido una excepcion en: %arregloCuentas()"+exception, EIGlobal.NivelLog.INFO);
		}
		return arreglo;
	}

public String[] arregloNumeroEmpleado(File nameArchivo, int numeroRegs){
		EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class &Entrando al metodo arregloNumeroEmpleado&", EIGlobal.NivelLog.INFO);
		String lineaQueSeLeyo	= "";
		String[] arregloNumeroEmpleado = new String[numeroRegs];
		try{
			RandomAccessFile archivoLectura = new RandomAccessFile(nameArchivo,"r");
			archivoLectura.seek(41);
			lineaQueSeLeyo = archivoLectura.readLine();
			for ( int i = 0; i < arregloNumeroEmpleado.length; i++ ) {
				if ( lineaQueSeLeyo.equals( "" ) )
					lineaQueSeLeyo = archivoLectura.readLine();
				if ( lineaQueSeLeyo == null )
					break;
				if(lineaQueSeLeyo.length()>13)
					arregloNumeroEmpleado[i] = lineaQueSeLeyo.substring( 6, 13 );
				else{
					i--;
					lineaQueSeLeyo = archivoLectura.readLine();
				}
			}
		} catch(IOException exception) {

			EIGlobal.mensajePorTrace("***BorraRegistroNominaInter.class Ha ocurrido una excepcion en: %arregloNumeroEmpleado()"+exception, EIGlobal.NivelLog.INFO);
		} catch(Exception e){
			e.printStackTrace();
		}
		return arregloNumeroEmpleado;
	}

	/**
	 * @author CSA se actualiza para el manejo de cierre a base de Datos.
	 * @since 17/11/2013
	 * @param request
	 * @return
	 */
	public String comboBancos(HttpServletRequest request){
		HttpSession sess = request.getSession();
	    BaseResource session = (BaseResource) sess.getAttribute("session");
	    Connection conn = null;
	    PreparedStatement contQuery = null;
	    ResultSet contResult = null;
		String comboBanco	= "";
		String totales		= "";
    	int totalBancos;
		String claveBanco   = "";
		String banco       = "";
		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				return "";
			}
			totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";
			contQuery=conn.prepareStatement(totales); // MODIFICAR
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "";
			}
			contResult=contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "";
			}
		    // MODIFICAR
			if( !contResult.next() ){
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No existe registro.", EIGlobal.NivelLog.ERROR);
				return "";
			}
			totalBancos=Integer.parseInt(contResult.getString(1));
			contResult.close(); // MODIFICAR
			EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

			/************************************************** Traer Bancos ... */
			String sqlbanco="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
			PreparedStatement bancoQuery=conn.prepareStatement(sqlbanco);

			if(bancoQuery==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "";
			}
			ResultSet bancoResult=bancoQuery.executeQuery(); // MODIFICAR
			if(bancoResult==null)
			{
				EIGlobal.mensajePorTrace("OpcionesNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "";
			}
			if(!bancoResult.next()){
					EIGlobal.mensajePorTrace("MDI_Interbancario - cuentasNoRegistradas(): ResultSet regreso sin valor.", EIGlobal.NivelLog.ERROR);
					return "";
				}
				comboBanco+="\n		 <SELECT NAME=Banco    class='tabmovtex'>";
				comboBanco+="\n		  <option value=0   class='tabmovtex'> Seleccione un Banco </OPTION>";

				// validación del banco SERFIN, SANTANDER
				claveBanco   = session.getClaveBanco();
				 if (claveBanco.equals("003"))
				 banco = "SERFIN";
					 else if (claveBanco.equals("014"))
				          banco = "SANTANDER";
				 String bancoTmp = "";
				for(int i=0; i<totalBancos; i++)
				 {
				    //bancoTmp = bancoResult.getString(2);
				  // if ( bancoTmp.indexOf(banco) >= 0 ) {
					//}else{
					comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
			//	comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"\n";
					//	}
					bancoResult.next();
			    }
				comboBanco+="\n		 </SELECT>";
				bancoResult.close();

		}catch( SQLException sqle ){
          	EIGlobal.mensajePorTrace( "***OpcionesNominaInter.class -comboBancos() & Error de sql la excepcion es &", EIGlobal.NivelLog.ERROR);
          	sqle.printStackTrace();
        }finally {
        	EI_Query.cierraConexion(contResult, contQuery, conn);
        }
		return comboBanco;
	}



	public String comboPlazas(){
		String comboPlazas="";
		comboPlazas+="\n		 <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas+="\n		 <input type=text size=25 name=PlazaBanxico value='MEXICO, DF' onFocus='blur();'>";
		comboPlazas+="\n		 <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String traeBanco(String claveB){

		String sqlbanco="";
		String comboBanco="";
		String totales="";
		String banco="";
		Connection conn=null;
		PreparedStatement contQuery;
		ResultSet contResult=null;
		PreparedStatement bancoQuery;
		//ResultSet bancoResult;
		int totalBancos;

		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);

			if(conn == null){
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR

			try{
				totales="Select nombre_corto from comu_interme_fin where num_cecoban<>0 and cve_interme='"+claveB+"'";
				contQuery=conn.prepareStatement(totales);

				if(contQuery == null){
					EIGlobal.mensajePorTrace("BorraRegistroNominaInter - iniciaComprobante(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
				}

				contResult=contQuery.executeQuery();

				if(contResult==null){
					EIGlobal.mensajePorTrace("BorraRegistroNominaInter - iniciaComprobante(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				}

				banco=contResult.getString(1);


				contResult.close();

				}
				catch(Exception e){
					EIGlobal.mensajePorTrace("BorraRegistroNominaInter - iniciaComprobante(): Exception, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);
				}

		}catch( SQLException sqle ){
          	EIGlobal.mensajePorTrace( "***BorraRegistroNominaInter.class -traeBanco(String claveB) & Error de sql la excepcion es &", EIGlobal.NivelLog.ERROR);
          	sqle.printStackTrace();
        }finally{
			try{
				contResult.close();
				conn.close();
			 }catch(Exception e) {}
		}
		return banco;
	}

	public String traePlaza(String claveP){
		String sqlbanco="";
		String comboBanco="";
		String totales="";
		String plaza="";
		Connection conn=null;
		PreparedStatement contQuery;
		ResultSet contResult=null;
		PreparedStatement bancoQuery;
		ResultSet bancoResult=null;
		int totalBancos;


		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);

			if(conn == null){
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - traePlaza(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR


		   try{
			  totales="Select descripcion from comu_pla_banxico where plaza_banxico='"+claveP+"'";
			  contQuery=conn.prepareStatement(totales);

			  if(contQuery == null){
				 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear Query.", EIGlobal.NivelLog.ERROR);
			  }

			  contResult=contQuery.executeQuery();

			  if(contResult==null){
				 EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): No se pudo crear ResultSet.", EIGlobal.NivelLog.ERROR);
			  }

			  plaza=contResult.getString(1);
			  contResult.close();
			}

			catch(Exception e){
			   EIGlobal.mensajePorTrace("MDI_Comprobante - iniciaComprobante(): Exception, Error... "+e.getMessage(), EIGlobal.NivelLog.INFO);
			}

		}catch( SQLException sqle ){
          	EIGlobal.mensajePorTrace( "***BorraRegistroNominaInter - traePlaza(String claveP): & Error de sql la excepcion es &", EIGlobal.NivelLog.ERROR);
          	sqle.printStackTrace();
        }finally{
			try{
			   conn.close();
			   contResult.close();
			}catch(Exception e) {}
		  }


		return plaza;

		}
public String colocaBanco(String bank, String claveB){
		EIGlobal.mensajePorTrace("BorraRegistroNominaInter colocaBanco(): claveB>> "+ claveB, EIGlobal.NivelLog.ERROR);
		String sqlbanco		= "";
		String comboBanco	= "";
		String totales		= "";
		Connection conn=null;
		PreparedStatement contQuery;
		ResultSet contResult=null;
		PreparedStatement bancoQuery;
		ResultSet bancoResult=null;
		int totalBancos;
		try{
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if(conn == null){
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR
			totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";

			contQuery=conn.prepareStatement(totales); // MODIFICAR
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
			contResult=contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
		    // MODIFICAR
			if(contResult.next())
			totalBancos=Integer.parseInt(contResult.getString(1));
			else
			totalBancos =0;

			contResult.close(); // MODIFICAR
			EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

			/************************************************** Traer Bancos ... */
//			bancoQuery=createQuery(); // MODIFICAR
			sqlbanco="Select cve_interme,nombre_corto,num_cecoban from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
			bancoQuery=conn.prepareStatement(sqlbanco);

			if(bancoQuery==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
			bancoResult=bancoQuery.executeQuery(); // MODIFICAR
			if(bancoResult==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
			/*****************************************************************************/
			/******************************************************************************/
			comboBanco+="\n		 <SELECT NAME='Banco'    class='tabmovtex'>";
			comboBanco+="\n		  <option value='"+claveB+"'   class='tabmovtex'>"+ bank +"</OPTION>\n";

				while(bancoResult.next()) {
				comboBanco+="<option value='"+bancoResult.getString(1)+"-"+bancoResult.getString(3)+"'>"+bancoResult.getString(2)+"\n";
				//comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"\n";
			    }
/*				bancoResult.next();
				for(int i=0; i<totalBancos; i++)
				{
					comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"</OPTION>\n\n";
					bancoResult.next(); // MODIFICAR
				} */

				bancoResult.close();

				comboBanco+="\n		 </SELECT>";

		}catch( SQLException sqle ){
          	EIGlobal.mensajePorTrace( "***BorraRegistroNominaInter - colocaBanco(String bank, String claveB): & Error de sql la excepcion es &", EIGlobal.NivelLog.ERROR);
          	sqle.printStackTrace();
        }finally{
			try{
				bancoResult.close();
				contResult.close();
				conn.close();
			 }catch(Exception e) {}
		}
		return comboBanco;
	}
	/*public String colocaBanco(String bank, String claveB){

		String sqlbanco="";
		String comboBanco="";
		String totales="";
		Connection conn;
		PreparedStatement contQuery;
		ResultSet contResult;
		PreparedStatement bancoQuery;
		ResultSet bancoResult;
		int totalBancos;


		try{

			conn = createiASConn("jdbc/enlace");

			if(conn == null){
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): Error al intentar crear la conexion.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}

//			contQuery=createQuery(); // MODIFICAR
			totales="Select count(*) from comu_interme_fin where num_cecoban<>0 ";

			contQuery=conn.prepareStatement(totales); // MODIFICAR
			if(contQuery == null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
			contResult=contQuery.executeQuery();
			if(contResult==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
		    // MODIFICAR
			totalBancos=Integer.parseInt(contResult.getString(1));
			contResult.close(); // MODIFICAR
			EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): Total de Bancos="+Integer.toString(totalBancos), EIGlobal.NivelLog.INFO);

//			bancoQuery=createQuery(); // MODIFICAR
			sqlbanco="Select cve_interme,nombre_corto from comu_interme_fin where num_cecoban<>0 order by nombre_corto";
			bancoQuery=conn.prepareStatement(sqlbanco);

			if(bancoQuery==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear Query.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}
			bancoResult=bancoQuery.executeQuery(); // MODIFICAR
			if(bancoResult==null)
			{
				EIGlobal.mensajePorTrace("BorraRegistroNominaInter - comboBancos(): No se puedo crear ResultSet.", EIGlobal.NivelLog.ERROR);
				return "NOSUCCESS";
			}


				comboBanco+="\n		 <SELECT NAME=Banco class='tabmovtex'>";
				comboBanco+="\n		  <option value="+claveB+ "class='tabmovtex'>"+ bank +"</OPTION>";


				while(bancoResult.next()) {
				comboBanco+="<option value='"+bancoResult.getString(1)+"'>"+bancoResult.getString(2)+"\n";
			    }

				bancoResult.close();

				comboBanco+="\n		 </SELECT>";

		}catch( SQLException sqle ){
          	EIGlobal.mensajePorTrace( "***BorraRegistroNominaInterr - colocaBanco(String bank, String claveB): & Error de sql la excepcion es &", 2);
          	sqle.printStackTrace();
          }
		return comboBanco;

	}

*/
	public String colocaPlaza(String plazaBank){
		String comboPlazas="";
		comboPlazas+="\n		 <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas+="\n		 <input type=text size=25 name=PlazaBanxico value='"+plazaBank+"' onFocus='blur();'>";
		comboPlazas+="\n		 <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceInternet/gbo25420.gif'></a>";
		return comboPlazas;

	}
}