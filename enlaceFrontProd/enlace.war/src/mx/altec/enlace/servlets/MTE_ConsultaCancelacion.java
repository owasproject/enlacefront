package mx.altec.enlace.servlets;

import java.util.*;
import java.io.IOException;

import javax.servlet.http.*;
import javax.servlet.*;

import java.io.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;




import java.sql.*;

//VSWF


public class MTE_ConsultaCancelacion extends BaseServlet
 {
	String pipe="|";

	/* Lista de Archivos (jsp) */
	/*
		/jsp/MTE_ConsultaInicio.jsp
	    /jsp/MTE_ConsultaRegTabla.jsp
		/jsp/MTE_ConsultaArcTabla.jsp
		/jsp/EI_Mensaje.jsp
	*/

	public void doGet( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	 {
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Metodo DoGet", EIGlobal.NivelLog.INFO);
		iniciaServlet( req, res );
	 }

	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException
	 {
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Metodo DoPost", EIGlobal.NivelLog.INFO);
		iniciaServlet( req, res );
	 }

	public void iniciaServlet( HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
	 {
		/************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String ventana= (String)req.getParameter("ventana");
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Entrando Tesofe ventana="+ventana, EIGlobal.NivelLog.INFO);

		if(ventana==null)
			ventana="0";

	    boolean sesionvalida = SesionValida( req, res);
		//boolean sesionvalida = true;
		if ( sesionvalida )
		 {

		   if(
			   session.getFacultad(session.FAC_RECDET) || session.getFacultad(session.FAC_RECCON) ||
			   session.getFacultad(session.FAC_IMPDEV) || session.getFacultad(session.FAC_IMPCAN) ||
			   session.getFacultad(session.FAC_ENVDEV) || session.getFacultad(session.FAC_ENVCAN) ||
			   session.getFacultad(session.FAC_CONREG) || session.getFacultad(session.FAC_CONARC)
		     )
			  {
				if(ventana.trim().equals("0")) // Consulta inicial
				  iniciaConsulta(req, res);
				else
				 if(ventana.trim().equals("1")) // Consulta Registros-Archivos
					generaTablaConsultar(req,res);
				else
				 if(ventana.trim().equals("2")) // Consulta Registros (Paginacion)
					generaTablaConsultarRegistrosPorPaginacion(req,res);
				else
				 if(ventana.trim().equals("3")) // Cancela-Envia Detalle de archivos
					generaCancelacionDetalle(req,res);
				else
				 if(ventana.trim().equals("4"))
				   generaTablaTipo(req,res);
			 }
			else
			 {
                String laDireccion = "document.location='SinFacultades'";

                req.setAttribute( "plantillaElegida", laDireccion);
                req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);
             }
		 }
	}

/*************************************************************************************/
/******************************************************************* iniciaConsulta  */
/*************************************************************************************/
	public void iniciaConsulta(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		String strInhabiles = armaDiasInhabilesJS();
		StringBuffer varFechaHoy=new StringBuffer();

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Entrando Tesofe inicio Consulta", EIGlobal.NivelLog.INFO);

		/************************************************** Arreglos de fechas para 'Hoy' ... */
		varFechaHoy.append("  //Fecha De Hoy");
		varFechaHoy.append("\n  dia=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n  mes=new Array(");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(");");

		varFechaHoy.append("\n\n  //Fechas Seleccionadas");
		varFechaHoy.append("\n  dia1=new Array(");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("DIA",0));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  mes1=new Array(");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("MES",0));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  anio1=new Array(");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(",");
		varFechaHoy.append(generaFechaAnt("ANIO",0));
		varFechaHoy.append(");");
		varFechaHoy.append("\n  Fecha=new Array('");
		varFechaHoy.append(generaFechaAnt("FECHA",0));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA",0));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA",0));
		varFechaHoy.append("','");
		varFechaHoy.append(generaFechaAnt("FECHA",0));
		varFechaHoy.append("');");

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Enviando parametros arreglos", EIGlobal.NivelLog.INFO);

		req.setAttribute("DiasInhabiles",strInhabiles);
		req.setAttribute("VarFechaHoy",varFechaHoy.toString());

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Enviando parametros dias", EIGlobal.NivelLog.INFO);

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req));

		req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Enviando fechas y encabezados", EIGlobal.NivelLog.INFO);

		req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
		req.setAttribute("web_application",Global.WEB_APPLICATION);

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Enviando las nuevas variables   y Verificando Facultades", EIGlobal.NivelLog.INFO);

		boolean CONARC=session.getFacultad(session.FAC_CONARC);
		boolean CONREG=session.getFacultad(session.FAC_CONREG);

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Facultad CONARC >" + CONARC, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Facultad CONREG >" + CONREG, EIGlobal.NivelLog.INFO);

		if(!CONARC && !CONREG)
		  despliegaPaginaErrorURL("<br>No tiene <font color=red>facultades</font> para opera la consulta de <i><font color=blue>archivos</font></i> y <i><font color=green>registros</font></i>.<br>","Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req,res);
		else{
			//TODO: BIT CU 4401 Cliente entra al flujo
			/*
			 * VSWF ARR -I
			 */
			if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			try{
			BitaHelper bh = new BitaHelperImpl(req, session, sess);

			if (req.getParameter(BitaConstants.FLUJO)!=null){
			       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			  }else{
			   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			  }

			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_TESOFE_CONS_ARCH_REG_ENTRA);
			bt.setContrato(session.getContractNumber());
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();
			}catch(Exception e){

				e.printStackTrace();
			}
			}
			/*
			 * VSWF ARR -F
			 */
		  evalTemplate("/jsp/MTE_ConsultaInicio.jsp", req, res );
		}
	}

 /*************************************************************************************/
/************************************************************* generaTablaConsultar  */
/*************************************************************************************/
	public void generaTablaConsultar(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
	    /************* Modificacion para la sesion ***************/
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Iniciando y generando la consulta", EIGlobal.NivelLog.INFO);

		String tipoConsulta=(String)req.getParameter("tipoConsulta");
		String [] estatusArc=req.getParameterValues("chkEstatusArc");
		String Fecha1_01=(String)req.getParameter("Fecha1_01");
		String Fecha1_02=(String)req.getParameter("Fecha1_02");
		String importe=(String)req.getParameter("importe");
		String secuencia=(String)req.getParameter("secuencia");

		String tipoFechaConsulta=(String)req.getParameter("tipoFechaConsulta");
		String RFC=req.getParameter("RFC");
		String secuenciaRegistro=(String)req.getParameter("secuenciaRegistro");
		String estatusReg=req.getParameter("chkEstatusReg");
		String Fecha2_01=(String)req.getParameter("Fecha2_01");
		String Fecha2_02=(String)req.getParameter("Fecha2_02");

		String usuario="";
		String contrato="";
		String clavePerfil="";
		String contratoConsulta="";

		String Result="";
		String tramaServicio="";
		String nombreArchivo="";
		String nombreArchivoExportacion="";
		String strFin="";
		String exportar="";

		StringBuffer estatusArchivo=new StringBuffer("");

		boolean CONARC=session.getFacultad(session.FAC_CONARC);
		boolean CONREG=session.getFacultad(session.FAC_CONREG);

		EIGlobal.mensajePorTrace( "Consult.java Facultad CONARC >" + CONARC, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "Consult.java Facultad CONREG >" + CONREG, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "Consult.java tipoConsulta " + tipoConsulta, EIGlobal.NivelLog.INFO);

		/*	fechas aaaammdd */
		Fecha1_01=formateaCamposfecha(Fecha1_01);
		Fecha1_02=formateaCamposfecha(Fecha1_02);
		Fecha2_01=formateaCamposfecha(Fecha2_01);
		Fecha2_02=formateaCamposfecha(Fecha2_02);

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se leyeron los parametros", EIGlobal.NivelLog.INFO);

		/* Variables de ambiente */
		usuario=(session.getUserID8()==null)?"":session.getUserID8();
		clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
		contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		contratoConsulta=contrato;

		if(estatusArc==null)
		  estatusArchivo.append("'R','C'");
		else
		 {
			for(int i=0;i<estatusArc.length;i++ )
			 {
			   estatusArchivo.append("'");
			   estatusArchivo.append(estatusArc[i]);
			   estatusArchivo.append("'");
			   strFin=(i!=estatusArc.length-1)?",":"";
			   estatusArchivo.append(strFin);
			 }
		 }

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> contrato " + contratoConsulta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> tipoConsulta " + tipoConsulta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> estatusArc " + estatusArchivo.toString(), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha1_01 " + Fecha1_01, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha1_02 " + Fecha1_02, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> importe " + importe, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> secuencia " + secuencia, EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> tipoFechaConsulta " + tipoFechaConsulta, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> secuenciaRegistro " + secuenciaRegistro, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> RFC " + RFC, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> estatusReg " + estatusReg, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha2_01 " + Fecha2_01, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha2_02 " + Fecha2_02, EIGlobal.NivelLog.INFO);

		int totalRegistros=0;
		int registrosPorPagina=0;
		int totalPaginas=0;
		int pagina=0;

		ArchivoRemoto archR = new ArchivoRemoto();

		/* Consulta de Registros*/
		if(tipoConsulta.trim().equals("R"))
		 {
			EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Armando trama para llamado a servicio de Consulta (Registros)", EIGlobal.NivelLog.INFO);

			if(CONREG)
			 {
				tramaServicio=
								contratoConsulta    + "@" +
								tipoFechaConsulta	+ "@" ;  //Consulta (G)eneral Consulta (D)etallada
				if(tipoFechaConsulta.equals("G"))
				 {
					tramaServicio +=
								Fecha2_01			+ "@" +
								Fecha2_02			+ "@" +
								estatusReg			+ "@" + pipe;
				 }
				else
				 {
					tramaServicio +=
								 secuenciaRegistro	+ "@" +
								 RFC				+ "@" + pipe;
				 }

				Result=llamaServicio(IEnlace.medioEntrega2,"TD11",contrato,usuario,clavePerfil,tramaServicio);
				if(Result==null || Result.equals("null"))
				 {
					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> error en el servicio (TD11)", EIGlobal.NivelLog.INFO);
					despliegaPaginaErrorURL("<br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.","Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req,res);
					return;
				 }
				else
				 {


					/*
						############################
						Tipo de archivo /tmp/1001851
						############################
					*/
					EI_Tipo trRes=new EI_Tipo();
					if(Result.trim().substring(0,2).equals("OK"))
					 {
						trRes.iniciaObjeto('@','|',Result+"|");
						String archivos=trRes.camposTabla[0][2];
						if(archivos.indexOf(";")>0)
						 {
						   nombreArchivo=archivos.substring(0,archivos.indexOf(';'));
						   nombreArchivoExportacion=archivos.substring(archivos.indexOf(';')+1);
						   /** Se requiere el archivo con Path.
						   if(nombreArchivo.indexOf("/")>=0)
							 nombreArchivo=nombreArchivo.substring(nombreArchivo.lastIndexOf("/")+1);
						   if(nombreArchivoExportacion.indexOf("/")>=0)
							 nombreArchivoExportacion=nombreArchivoExportacion.substring(nombreArchivoExportacion.lastIndexOf("/")+1);
						   */
						 }
						else
						 {
						   /** Se requiere el archivo con Path.
						   if(archivos.indexOf("/")>=0)
						   nombreArchivo=archivos.substring(archivos.lastIndexOf("/")+1);
						   */
						   nombreArchivo=archivos;
						 }
						EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> La consulta fue exitosa (Registros).", EIGlobal.NivelLog.INFO);
					 }
					else
					 {
						//nombreArchivo=Result.substring(Result.lastIndexOf('/')+1);
						despliegaPaginaErrorURL("No se pudo completar la consulta de <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.","Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req,res);
						return;
					 }

					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Archivo de registros: " + nombreArchivo, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Archivo de exportacion: " + nombreArchivoExportacion, EIGlobal.NivelLog.INFO);

					//###### Cambiar ....
					//nombreArchivo="/tmp/pruebaconsultaregistros.prueba";
					//###### Cambiar ....

					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Nombre Archivo " + nombreArchivo, EIGlobal.NivelLog.INFO);
					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Directorio Remoto WEB TESOFE: " + Global.DIRECTORIO_REMOTO_TESOFE, EIGlobal.NivelLog.INFO);

					if( archR.copiaTESOFE(nombreArchivo,Global.DIRECTORIO_LOCAL) )
					 {
					   /** Se requiere el archivo con Path.
					   nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+nombreArchivo;
					   */
					   if(nombreArchivoExportacion.trim().length()>0 && tipoFechaConsulta.equals("G") )
						 {
						   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Archivo de exportacion para consulta general", EIGlobal.NivelLog.INFO);
						   if(archR.copiaTESOFE(nombreArchivoExportacion,Global.DIRECTORIO_LOCAL))
							 {
							   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se copio desde Torcido ", EIGlobal.NivelLog.INFO);
								/* Se le quita el path al archivo de exportacion para copiarlo al WEB SRVR*/
								if(nombreArchivoExportacion.indexOf("/")>=0)
								  nombreArchivoExportacion=nombreArchivoExportacion.substring(nombreArchivoExportacion.lastIndexOf("/")+1);
								if(archR.copiaLocalARemoto(nombreArchivoExportacion,"WEB"))
								 {
								   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se copio a WEB-A", EIGlobal.NivelLog.INFO);
								   exportar= "<a href='/Download/"+nombreArchivoExportacion+"' border=0><img src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar' border=0></a>";
								 }
							 }
						 }
					   req.setAttribute("Exportar",exportar);
					   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Copia remota desde tuxedo se realizo correctamente. (Registros)", EIGlobal.NivelLog.INFO);

					   /* Se le quita el path al archivo para operar con el, en el directorio temporal del app*/
					   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se trajo (R): "+ nombreArchivo, EIGlobal.NivelLog.INFO);
					   nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
					   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se abre (R): "+nombreArchivo, EIGlobal.NivelLog.INFO);

					   totalRegistros=obtenTotalLineas(nombreArchivo,tipoFechaConsulta,req,res);
					   if(totalRegistros>0)
						 {
						   registrosPorPagina=Global.MAX_REGISTROS;
						   totalPaginas=calculaPaginas(totalRegistros,registrosPorPagina);

							EIGlobal.mensajePorTrace( "calculo pagina "+pagina, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "calculo totalRegistros "+totalRegistros, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "calculo registrosPorPagina "+registrosPorPagina, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace( "calculo totalPaginas "+totalPaginas, EIGlobal.NivelLog.INFO);

						   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Generando la pantalla (Registros)", EIGlobal.NivelLog.INFO);
						   generaTablaConsultarRegistros(estatusReg,tipoFechaConsulta,Fecha2_01,Fecha2_02,nombreArchivo,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,req,res);
						 }
						else if(totalRegistros<0)
						 {
							EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se pudo abrir el archivo ... (Registros)", EIGlobal.NivelLog.INFO);
							despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.","Consulta de Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55160h",req,res);
							return;
						 }
						else if(totalRegistros==0)
						 {
							EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Archivo encontrado pero sin registros ... (Registros)", EIGlobal.NivelLog.INFO);
							despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.","Consulta de Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55160h",req,res);
							return;
						 }

					 }
					else
					 {
						EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se pudo realizar la copia remota del archivo de tuxedo (Registros)", EIGlobal.NivelLog.INFO);
						despliegaPaginaErrorURL("La <i><font color=green>transacci&oacute;n</font></i> solicitada no se llevo a cabo.<br>Por favor intente mas tarde.","Consulta de Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55160h",req,res);
						return;
					 }
				 }
			 }
			else
			 {
				EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No tiene facultad para consulta (Registros)", EIGlobal.NivelLog.INFO);
				despliegaPaginaErrorURL("No tiene <i><font color=red>facultad</font></i> para realizar la consulta de <font color=green>Registros</font>.","Consulta de Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55160h",req,res);
			 }
		 }
		else   /* Consulta de Archivos */
		 {
			EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Armando trama para llamado a servicio de Consulta (Archivos)", EIGlobal.NivelLog.INFO);

			if(CONARC)
			 {
				tramaServicio=
								contrato					+ "@" +
								Fecha1_01					+ "@" +
								Fecha1_02					+ "@" +
								importe						+ "@" +
								secuencia					+ "@" +
								estatusArchivo.toString()	+ "@" + pipe;

				Result=llamaServicio(IEnlace.medioEntrega2,"TD08",contrato,usuario,clavePerfil,tramaServicio);


				if(Result==null || Result.equals("null"))
				 {
					EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> error en el servicio (TD11)", EIGlobal.NivelLog.INFO);
					despliegaPaginaErrorURL("<br>Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.","Consulta de Archivos ","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req,res);
					return;
				 }
				else
				 {
					EI_Tipo trRes=new EI_Tipo();
					if(Result.trim().substring(0,2).equals("OK"))
					 {
						trRes.iniciaObjeto('@','|',Result+"|");
						/** Se requiere el archivo con Path.
						if(trRes.camposTabla[0][2].indexOf("/")>=0)
							nombreArchivo=trRes.camposTabla[0][2].substring(trRes.camposTabla[0][2].lastIndexOf("/")+1);
						*/
						nombreArchivo=trRes.camposTabla[0][2];
						EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> La consulta fue exitosa (Archivos).", EIGlobal.NivelLog.INFO);
					 }
					else
					 {
						//nombreArchivo=Result.substring(Result.lastIndexOf('/')+1);
						despliegaPaginaErrorURL("No se pudo completar la consulta de <font color=green>registros</font> para el tipo de b&uacute;squeda seleccionado.","Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55140h",req,res);
						return;
					 }
					//###### Cambiar ....
					//nombreArchivo=IEnlace.LOCAL_TMP_DIR + "/" + contrato + ".rep";
					//nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/pruebaconsulta.prueba";
					//###### Cambiar ....

					if(archR.copiaTESOFE(nombreArchivo,Global.DIRECTORIO_LOCAL))
					 {
					   /*
					   nombreArchivo=trRes.camposTabla[0][2]
					   nombreArchivo=IEnlace.LOCAL_TMP_DIR + "/" + nombreArchivo;
					   */
					   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Copia remota desde tuxedo se realizo correctamente. (Archivos)", EIGlobal.NivelLog.INFO);
					   EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Generando la pantalla (Archivos)", EIGlobal.NivelLog.INFO);
					   // //TODO BIT CU 4401
						/*
						 * VSWF ARR -I
						 */
					   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
						   try{
							   BitaHelper bh = new BitaHelperImpl(req, session, sess);
							   BitaTransacBean bt = new BitaTransacBean();
							   bt = (BitaTransacBean)bh.llenarBean(bt);
							   bt.setNumBit(BitaConstants.ES_TESOFE_CONS_ARCH_REG_CONSULTA_ARCH);
							   bt.setContrato(contrato);
							   bt.setServTransTux("TD08");
							   if(Result!=null){
								   if(Result.substring(0,2).equals("OK")){
									   bt.setIdErr("TD080000");
								   }else if(Result.length()>8){
									   bt.setIdErr(Result.substring(0,8));
								   }else{
									   bt.setIdErr(Result.trim());
								   }
							   }
							   if(importe!=null && !importe.equals(""))
								   bt.setImporte(Double.parseDouble(importe));
							   bt.setNombreArchivo(nombreArchivoExportacion);
							   BitaHandler.getInstance().insertBitaTransac(bt);
						   }catch(SQLException e){
							   e.printStackTrace();
						   }catch(Exception e){
							   e.printStackTrace();
						   }
					   }
						/*
						 * VSWF ARR -F
						 */
					   generaTablaConsultarArchivos(nombreArchivo,Fecha1_01,Fecha1_02,importe,secuencia,estatusArchivo.toString(),req,res);
					 }
					else
					 {
						EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se pudo realizar la copia remota del archivo de tuxedo (Archivos)", EIGlobal.NivelLog.INFO);
						despliegaPaginaErrorURL("La <i><font color=green>transacci&oacute;n</font></i> solicitada no se llevo a cabo.<br>Por favor intente mas tarde.","Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55150h",req,res);
						return;
					 }
				 }
			 }
			else
			 {
				EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No tiene facultad para consulta (Archivos)", EIGlobal.NivelLog.INFO);
				despliegaPaginaErrorURL("No tiene <i><font color=red>facultad</font></i> para realizar la consulta de <font color=blue>Archivos</font>.","Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55160h",req,res);
			 }
		 }
  }


/*************************************************************************************/
/******************************************************************** llamaServicio  */
/*************************************************************************************/
  public String llamaServicio(String medioEntrega,String tipoOperacion,String contrato, String usuario,String clavePerfil,String tramaServicio) throws IOException
    {

	   String Result="";
	   String Trama="";

	   EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Servicio de Consulta Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);
		//Cabecera
				Trama = medioEntrega 			+ "|" +
						usuario 				+ "|" +
						tipoOperacion			+ "|" +
						contrato				+ "|" +
						usuario 				+ "|" +
						clavePerfil				+ "|" +
					    tramaServicio			;

	    EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Servicio de Consulta Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

		try
		 {

			Hashtable htResult = tuxGlobal.web_red( Trama );
			Result = ( String ) htResult.get( "BUFFER" );
	     } catch(Exception e){}

		EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);

		return Result;
	}

/*************************************************************************************/
/***************************************************************** obtenTotalLineas  */
/*************************************************************************************/
  public int obtenTotalLineas(String Archivo,String tipoFechaConsulta,HttpServletRequest req, HttpServletResponse res) throws IOException
    {
      int num=0;
	  int ind=0;
	  int pos=(tipoFechaConsulta.equals("D"))?5:2;

	  double importeT=0;
	  double importeTotal=0;

	  String linea="";
	  String imp="";

	  EI_Tipo t=new EI_Tipo();

	  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - El campo importe esta en pos: " + pos, EIGlobal.NivelLog.INFO);

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  while((linea=arc.readLine())!=null)
		   {
			 if(num>=1)
			   {
				 //Forma alterna para separar los importes (no muy practica)
				 //t.iniciaObjeto(';','@',linea+"@");
				 //imp=t.camposTabla[0][pos];

				 EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Tokenizer", EIGlobal.NivelLog.INFO);
				 java.util.StringTokenizer tokenizer = new java.util.StringTokenizer (linea,";");
				 for(int j=0;j<=pos;j++)
				   imp=tokenizer.nextToken ();
				 EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - El importe que se suma es: " + imp, EIGlobal.NivelLog.INFO);

				 try
					{
					  if(imp.trim().equals(""))
						importeT=0;
					  else
						importeT=new Double(imp.trim()).doubleValue();
					}catch(NumberFormatException e)
					{importeT=0;}
				  importeTotal+=importeT;
			   }
		 	 num++;
		   }
		  arc.close();
		}catch(IOException e)
		{
		  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Error al tratar de leer: "+ Archivo, EIGlobal.NivelLog.INFO);
		  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Err: "+e.toString() + " No reg: " + num, EIGlobal.NivelLog.INFO);
		  num=-1;
		}
	  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - El importe total es: " + importeTotal, EIGlobal.NivelLog.INFO);
	  if(req.getSession().getAttribute("ImporteGlobal")!=null)
		  req.getSession().removeAttribute("ImporteGlobal");
	  req.getSession().setAttribute("ImporteGlobal",FormatoMoneda(importeTotal));

	  if(num==1)
		 return 0;
      return num-1;
    }

/*************************************************************************************/
/***************************************************************** obtenTitulos  */
/*************************************************************************************/
  public String obtenTitulos(String Archivo) throws IOException
    {
      String line="";
	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));
		  line=arc.readLine()+"@";
		  if(line==null)
			 return "";
		}catch(IOException e)
		 {
			EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Error al tratar de leer: "+ Archivo, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Err: "+e.toString(), EIGlobal.NivelLog.INFO);
			line="";
		 }
      return line;
    }

/*************************************************************************************/
/***************************************************** seleccionaRegistrosDeArchivo  */
/*************************************************************************************/
  public String seleccionaRegistrosDeArchivo(String Archivo,int pagina,int regPorPagina,int totalReg) throws IOException
	{
	  String linea="";
	  StringBuffer result=new StringBuffer("");

	  int inicio=0;
	  int fin=0;

	  inicio=(pagina*regPorPagina);
	  fin=inicio+regPorPagina;
	  if(fin>totalReg)
		fin=totalReg;

	  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

	  linea=arc.readLine(); // Linea de datos para la tabla (Titulos y numero de datos)
	  for(int i=0;i<fin;i++)
	   {
		 linea=arc.readLine();
		 if(i>=inicio)
			result.append(linea+"@");
	   }
	  arc.close();
	  return result.toString();
	}

/*************************************************************************************/
/***************************************************** obtenDatos  */
/*************************************************************************************/
  public String obtenDatos(String Archivo) throws IOException
	{
	  String linea="";
	  StringBuffer result=new StringBuffer("");

	  try
		{
		  BufferedReader arc=new BufferedReader(new FileReader(Archivo));

		  while((linea=arc.readLine())!=null){
			 result.append(linea+"@");
		  }
		  arc.close();
		}catch(IOException e)
		 {
			EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Error al tratar de leer: "+ Archivo, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> - Err: "+e.toString(), EIGlobal.NivelLog.INFO);
			result = new StringBuffer("ERR");
		 }
	  return result.toString();
	}

/*************************************************************************************/
/**************************************************** generaTablaConsultarRegistros  */
/*************************************************************************************/
 public void generaTablaConsultarRegistrosPorPaginacion( HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Entrando a paginacion de archivo", EIGlobal.NivelLog.INFO);

	  String tipoFechaConsulta=(String)req.getParameter("tipoFechaConsulta");
	  String Fecha2_01=(String)req.getParameter("Fecha2_01");
	  String Fecha2_02=(String)req.getParameter("Fecha2_02");
	  String nombreArchivo=(String)req.getParameter("nombreArchivo");
	  String contratoConsulta=(String)req.getParameter("contratoConsulta");
	  String exportar=(String)req.getParameter("Exportar");
	  String estatusReg=req.getParameter("chkEstatusReg");

      int totalRegistros=Integer.parseInt((String)req.getParameter("totalRegistros"));
	  int registrosPorPagina=Integer.parseInt((String)req.getParameter("registrosPorPagina"));
	  int totalPaginas=Integer.parseInt((String)req.getParameter("totalPaginas"));
	  int pagina=Integer.parseInt((String)req.getParameter("pagina"));

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Busqueda por registros ", EIGlobal.NivelLog.INFO);

      generaTablaConsultarRegistros(estatusReg,tipoFechaConsulta,Fecha2_01,Fecha2_02,nombreArchivo,contratoConsulta,pagina,totalRegistros,registrosPorPagina,totalPaginas,exportar,req,res);
   }

/*************************************************************************************/
/**************************************************** generaTablaConsultarRegistros  */
/*************************************************************************************/
 public void generaTablaConsultarRegistros(String estatusReg, String tipoFechaConsulta,String Fecha2_01,String Fecha2_02,String nombreArchivo,String contratoConsulta,int pagina,int totalRegistros,int registrosPorPagina,int totalPaginas,String exportar,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {
      /************* Modificacion para la sesion ***************/
      HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> generando la tabla de consulta", EIGlobal.NivelLog.INFO);

	  String strRegistros="";
	  String strTitulos="";

	  StringBuffer tablaRegistros=new StringBuffer("");
	  StringBuffer botones=new StringBuffer("");

	  String[] titulos;

      int[] datos;
	  int[] align;
	  int[] values={0};

	  int pos=0;

	  EI_Tipo regSel=new EI_Tipo();
	  EI_Tipo headSel=new EI_Tipo();

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Preparando los datos.", EIGlobal.NivelLog.INFO);
	  strTitulos=obtenTitulos(nombreArchivo);

	  strRegistros=seleccionaRegistrosDeArchivo(nombreArchivo,pagina,registrosPorPagina,totalRegistros);

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se leyo el archivo.", EIGlobal.NivelLog.INFO);
	  headSel.iniciaObjeto(';','@',strTitulos);
	  regSel.iniciaObjeto(';','@',strRegistros);

	  String importeTotal="0";
	  StringBuffer cadenaImportes=new StringBuffer("");

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se realiza la suma y de los importes y el formateo de las fechas", EIGlobal.NivelLog.INFO);

	  if(tipoFechaConsulta.trim().equals("D"))
	   {
		 importeTotal=sumaImportes(regSel,5);
		 formateaFechaImporte(regSel,1,5);
		 cadenaImportes.append("\n <tr>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td class='tittabdat'>Importe Total: </td>");
		 cadenaImportes.append("\n  <td class='tittabdat' align=right> "+ importeTotal +"</td>");
		 cadenaImportes.append("\n  <td class='tittabdat'><br></td>");
		 cadenaImportes.append("\n <tr>");

		 cadenaImportes.append("\n <tr>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td class='textabdatobs'>Importe Global: </td>");
		 cadenaImportes.append("\n  <td class='textabdatobs' align=right> "+ req.getSession().getAttribute("ImporteGlobal") +"</td>");
		 cadenaImportes.append("\n  <td class='textabdatobs'><br></td>");
		 cadenaImportes.append("\n <tr>");

		 cadenaImportes.append("\n</table>");
	   }
	  else
	   {
		 importeTotal=sumaImportes(regSel, 2);
		 formateaFechaImporte(regSel,1, 2);
		 cadenaImportes.append("\n <tr>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td class='tittabdat'>Importe Total: </td>");
		 cadenaImportes.append("\n  <td class='tittabdat' align=right> "+ importeTotal +"</td>");
		 cadenaImportes.append("\n  <td class='tittabdat'><br></td>");
		 cadenaImportes.append("\n  <td class='tittabdat'><br></td>");
		 cadenaImportes.append("\n <tr>");

		 cadenaImportes.append("\n <tr>");
		 cadenaImportes.append("\n  <td ><br></td>");
		 cadenaImportes.append("\n  <td class='textabdatobs'>Importe Global: </td>");
		 cadenaImportes.append("\n  <td class='textabdatobs' align=right> "+ req.getSession().getAttribute("ImporteGlobal") +"</td>");
		 cadenaImportes.append("\n  <td class='textabdatobs'><br></td>");
		 cadenaImportes.append("\n  <td class='textabdatobs'><br></td>");
		 cadenaImportes.append("\n <tr>");

		 cadenaImportes.append("\n</table>");
	   }

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Numero de campos en la consulta: "+ headSel.camposTabla[0][0], EIGlobal.NivelLog.INFO);
	  datos=new int[Integer.parseInt(headSel.camposTabla[0][0]+1)];
	  titulos=new String[Integer.parseInt(headSel.camposTabla[0][0]+1)];
	  align=new int[Integer.parseInt(headSel.camposTabla[0][0]+1)];

	  datos[0]=Integer.parseInt(headSel.camposTabla[0][0]);
	  datos[1]=0;
	  titulos[0]="";

	  /***
	    Crear arreglo para formatear fechas, importes
	  */
	  for(int i=1;i<=(datos[0]);i++)
	   {
	     datos[i+1]=i-1;
		 titulos[i]=headSel.camposTabla[0][1+(2*(i-1))];
		 pos=Integer.parseInt(headSel.camposTabla[0][0+(2*i)].trim());
		 switch(pos)
		   {
			 case 0:
			 case 2: align[i-1]=1; break;
			 case 1: align[i-1]=2; break;
			 case 3: align[i-1]=0; break;
		   }
	   }

	  tablaRegistros.append(regSel.generaTabla(titulos,datos,values,align,0));
	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Objetos creados", EIGlobal.NivelLog.INFO);

	  tablaRegistros.append(cadenaImportes);

	  if(totalRegistros>registrosPorPagina)
	   {
		  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> Estableciendo links para las paginas", EIGlobal.NivelLog.INFO);
		  botones.append("\n <br>");
		  botones.append("\n <table width=620 border=0 align=center>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td align=center class='texfootpagneg'>");
		  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> Pagina actual = "+pagina, EIGlobal.NivelLog.INFO);
		  if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
		  if(totalPaginas>=2)
		   {
			 for(int i=1;i<=totalPaginas;i++)
			  if(pagina+1==i)
			   {
				 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
				 botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a> ");
			   }
		   }
		  if(totalRegistros>((pagina+1)*registrosPorPagina))
			 botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");

		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
	   }

	  req.setAttribute("tipoFechaConsulta",tipoFechaConsulta);
	  req.setAttribute("Fecha2_01",Fecha2_01);
	  req.setAttribute("Fecha2_02",Fecha2_02);

	  req.setAttribute("nombreArchivo",nombreArchivo);
	  req.setAttribute("contratoConsulta",contratoConsulta);
	  req.setAttribute("totalRegistros",Integer.toString(totalRegistros));
	  req.setAttribute("registrosPorPagina",Integer.toString(registrosPorPagina));
	  req.setAttribute("totalPaginas",Integer.toString(totalPaginas));
	  req.setAttribute("tablaRegistros",tablaRegistros.toString());
	  req.setAttribute("pagina",Integer.toString(pagina+1));
	  req.setAttribute("chkEstatusReg",estatusReg);

	  req.setAttribute("Botones",botones.toString());
	  req.setAttribute("Exportar",exportar);

	  req.setAttribute("Pagina","1");
	  req.setAttribute("ventana","2");

	  req.setAttribute("MenuPrincipal", session.getStrMenu());
	  req.setAttribute("newMenu", session.getFuncionesDeMenu());
	  req.setAttribute("Encabezado", CreaEncabezado("Consulta de Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros &gt Consulta por Registros","s55160h",req));

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se presenta pantalla de importacion", EIGlobal.NivelLog.INFO);

	  evalTemplate ("/jsp/MTE_ConsultaRegTabla.jsp", req, res );
   }

/*************************************************************************************/
/****************************************************************** cancelaArchivos  */
/*************************************************************************************/
 public void generaCancelacionDetalle(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
  {
	 /************* Modificacion para la sesion ***************/
     HttpSession sess = req.getSession();
	 BaseResource session = (BaseResource) sess.getAttribute("session");

	 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Cancelacion de archivos", EIGlobal.NivelLog.INFO);

	 String Fecha1_01=(String)req.getParameter("Fecha1_01");
	 String Fecha1_02=(String)req.getParameter("Fecha1_02");
	 String importe=(String)req.getParameter("importe");
	 String secuencia=(String)req.getParameter("secuencia");
	 String estatusArchivo=req.getParameter("estatusArchivo");
	 String nombreArchivo=req.getParameter("nombreArchivo");

	 String radTabla=req.getParameter("radTabla");
	 String correo=req.getParameter("correo");
	 String operacion=req.getParameter("operacion");

	 String fechaRegistro="";
	 String secuenciaRegistro="";
	 String numeroRegistro="";
	 String importeRegistro="";
	 String estatusRegistro="";

	 String tramaServicio="";
	 String tipoOperacion="";
	 String correoE="";
	 String Result="";

	 String usuario="";
	 String contrato="";
	 String clavePerfil="";

	 String msgError="";

	 /* Variables de ambiente */
	 usuario=(session.getUserID8()==null)?"":session.getUserID8();
	 clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
	 contrato=(session.getContractNumber()==null)?"":session.getContractNumber();

	 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> variables de ambiente.", EIGlobal.NivelLog.INFO);

	 EI_Tipo Registro= new EI_Tipo();
	 Registro.iniciaObjeto(radTabla);

	 fechaRegistro=Registro.camposTabla[0][0];
	 secuenciaRegistro=Registro.camposTabla[0][2];
	 numeroRegistro=Registro.camposTabla[0][3];
	 importeRegistro=Registro.camposTabla[0][4];
	 estatusRegistro=Registro.camposTabla[0][5];

	 if(correo.indexOf("@")>0)
		correoE=correo.substring(0,correo.indexOf("@")) + "[]" + correo.substring(correo.indexOf("@")+1);
	 if(operacion.equals("C"))
	  {
		 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se realiza la cancelacion de la secuencia: "+secuenciaRegistro, EIGlobal.NivelLog.INFO);
		 tipoOperacion=  "TD10";
		 tramaServicio=  contrato					+ "@" +
		 secuenciaRegistro			+ "@" ;
	  }
	 else
	  {
		 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se envia el detalle de archivo a: " + correo, EIGlobal.NivelLog.INFO);
		 tipoOperacion=  "TD09";
		 tramaServicio=  contrato					+ "@" +
			             correoE					+ "@" +
						 secuenciaRegistro			+ "@" ;
	  }

	 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Preparandose para el servicio.", EIGlobal.NivelLog.INFO);
	 Result=llamaServicio(IEnlace.medioEntrega1,tipoOperacion,contrato,usuario,clavePerfil,tramaServicio);

	 //Detalle de archivo ...
	 //Result="OK @     596@/tmp/1001851@";
	 //Result="TESD0012     597ERROR EN EL ENVIO DE MAIL";

	 if(Result==null || Result.equals("null"))
	  {
		 EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> error en el servicio : "+tipoOperacion, EIGlobal.NivelLog.INFO);
		 despliegaPaginaErrorURL("<br><br>Su <font color=red>transacci&oacute;n</font> no est&aacute; disponible en este momento.<br>Por favor intente mas tarde.","Consulta de Archivos ","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55150h",req,res);
		 return;
	  }
	 else
	  {
		 if(Result.substring(0,2).equals("OK"))
		  {
			 if(operacion.equals("C"))
				req.setAttribute("estatusOperacion","cuadroDialogo(\"Se cancelo el archivo con secuencia: "+secuenciaRegistro+"\", 1);");
			 else
				req.setAttribute("estatusOperacion","cuadroDialogo(\"Se envio el detalle del archivo con secuencia: "+secuenciaRegistro+" a: "+correo+"\", 1);");
		  }
		 else
		  {
			if(Result.length()>16)
				msgError=Result.substring(16);
			else
				msgError="Su <font color=red>transacci&oacute;n</font> no puede ser atendida en este momento.<br>Por favor intente mas tarde.";
		    req.setAttribute("estatusOperacion","cuadroDialogo(\"No se realiz&oacute; el env&iacute;o del archivo con secuencia: "+secuenciaRegistro+"<br>&lt;"+ msgError + "&gt;\");");
		  }
		 //TODO IT CU 4401
			/*
			 * VSWF ARR -I
			 */
		 if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_TESOFE_CONS_ARCH_REG_ENVIO_DET_ARCH);
			bt.setContrato(contrato);
			bt.setNombreArchivo(nombreArchivo);
			if(importe!=null && !importe.equals(""))
				bt.setImporte(Double.parseDouble(importe));
			bt.setServTransTux(tipoOperacion.trim());
			/*BMB-I*/
			if(Result!=null){
    			if(Result.substring(0,2).equals("OK")){
    				bt.setIdErr(tipoOperacion.trim()+"0000");
    			}else if(Result.length()>8){
	    			bt.setIdErr(Result.substring(0,8));
	    		}else{
	    			bt.setIdErr(Result.trim());
	    		}
    		}
			/*BMB-F*/
			try{
				BitaHandler.getInstance().insertBitaTransac(bt);
			}catch(SQLException e){
				e.printStackTrace();

			}catch(Exception e){
				e.printStackTrace();

			}
		 }
			/*
			 * VSWF ARR -F
			 */

		 generaTablaConsultarArchivos(nombreArchivo,Fecha1_01,Fecha1_02,importe,secuencia,estatusArchivo,req,res);
	  }
  }

/*************************************************************************************/
/**************************************************** generaTablaConsultarArchivos   */
/*************************************************************************************/
 public void generaTablaConsultarArchivos(String nombreArchivo,String Fecha1_01,String Fecha1_02,String importe,String secuencia,String estatusArchivo,HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
   {

      /************* Modificacion para la sesion ***************/
      HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Generando la tabla de consulta de archivos", EIGlobal.NivelLog.INFO);

	  /* Se le quita el path al archivo para operar con el, en el directorio temporal del app*/
	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se trajo (A): "+ nombreArchivo, EIGlobal.NivelLog.INFO);
	  nombreArchivo=IEnlace.LOCAL_TMP_DIR + nombreArchivo.substring(nombreArchivo.lastIndexOf("/"));
	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> El archivo que se abre (A): "+nombreArchivo, EIGlobal.NivelLog.INFO);

	  StringBuffer tablaRegistros=new StringBuffer("");
	  String consulta="";

	  String[] titulos={"Consulta de Archivos",
		       "Fecha de Recepci&oacute;n",
		       "Fecha de Aplicaci&oacute;n",
		       "Archivo",
		       "Secuencia",
		       "N&uacute;mero de Registros",
			   "Importe",
		       "Estatus"};

      int[] datos={7,1,0,1,2,3,4,5,6};
	  int[] values={7,0,1,3,4,5,6,2};
	  int[] align={1,1,2,1,1,2,0};

	  EI_Tipo regSel=new EI_Tipo();

	  consulta=obtenDatos(nombreArchivo);

	  if(consulta.trim().equals("ERR"))
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se pudo realiza la consulta el archivo tiene errores", EIGlobal.NivelLog.INFO);
		  despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.","Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55150h",req,res);
		  return;
	   }
	  else
	  if(consulta.trim().equals(""))
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se encontraron registros en el archivo.", EIGlobal.NivelLog.INFO);
		  despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el rango de fechas y/o estatus seleccionados","Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55150h",req,res);
		  return;
	   }
	  else
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Objetos creados para la consulta de archivos", EIGlobal.NivelLog.INFO);
		  regSel.iniciaObjeto(';','@',consulta);
		  /** Cambio formatos en vez de el campo 3 de secuencia se cambia el de importe con numero 5***/
		  formateaFechaImporte(regSel,0,1, 5);
		  String tmp=regSel.generaTabla(titulos,datos,values,align);
		  tablaRegistros.append(tmp);

		  req.setAttribute("Fecha1_01",Fecha1_01);
		  req.setAttribute("Fecha1_02",Fecha1_02);
		  req.setAttribute("importe",importe);
		  req.setAttribute("secuencia",secuencia);

		  req.setAttribute("estatusArchivo",estatusArchivo);
		  req.setAttribute("nombreArchivo",nombreArchivo);

		  req.setAttribute("tablaRegistros",tablaRegistros.toString());
		  req.setAttribute("Exportar",exportarArchivo(regSel, session.getUserID8()) );

		  req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
		  req.setAttribute("Encabezado", CreaEncabezado("Consulta de Archivos","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros &gt Consulta por Archivos","s55150h",req));

		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se presenta pantalla de consulta de archivos", EIGlobal.NivelLog.INFO);

		  evalTemplate ("/jsp/MTE_ConsultaArcTabla.jsp", req, res );
	   }
   }

/*************************************************************************************/
/********************************************************************* sumaImportes  */
/*************************************************************************************/
  public String sumaImportes(EI_Tipo Tipo,int importe)
	{
	  double importeT=0;
	  double importeTotal=0;

	  for(int i=0;i<Tipo.totalRegistros;i++)
		{
		  try
			{
		      if(Tipo.camposTabla[i][importe].trim().equals(""))
			    importeT=0;
			  else
				importeT=new Double(Tipo.camposTabla[i][importe]).doubleValue();
			}catch(NumberFormatException e)
			{importeT=0;}
		  importeTotal+=importeT;
	    }
	  return FormatoMoneda(importeTotal);
	}

/*************************************************************************************/
/************************************************************* formateaFechaImporte  */
/*************************************************************************************/
   public void formateaFechaImporte(EI_Tipo Tipo,int fecha,int importe)
	{
	  String anio="";
	  String mes="";
	  String dia="";

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Formateando la fecha y el importe del objeto tabla.", EIGlobal.NivelLog.INFO);

	  for(int i=0;i<Tipo.totalRegistros;i++)
		{
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha: " + Tipo.camposTabla[i][fecha], EIGlobal.NivelLog.INFO);
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Importe: " + Tipo.camposTabla[i][importe], EIGlobal.NivelLog.INFO);
          if(! (Tipo.camposTabla[i][fecha]==null || Tipo.camposTabla[i][fecha].trim().equals("")) )
            if (Tipo.camposTabla[i][fecha].trim().length()==8)
              {
                anio = Tipo.camposTabla[i][fecha].trim().substring(0,4);
                mes = Tipo.camposTabla[i][fecha].trim().substring(4,6);
                dia = Tipo.camposTabla[i][fecha].trim().substring(6);
                Tipo.camposTabla[i][fecha]=dia + "/" + mes + "/" + anio ;
              }
		  if(! (Tipo.camposTabla[i][importe]==null || Tipo.camposTabla[i][importe].trim().equals("")) )
			  Tipo.camposTabla[i][importe]=FormatoMoneda(Tipo.camposTabla[i][importe]);
		}

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Campos formateados ....", EIGlobal.NivelLog.INFO);
	}


/*************************************************************************************/
/************************************************************* formateaFechaImporte  */
/*************************************************************************************/
   public void formateaFechaImporte(EI_Tipo Tipo,int fecha,int fecha2, int importe)
	{
	  String anio="";
	  String mes="";
	  String dia="";

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Formateando la fecha y el importe del objeto tabla.", EIGlobal.NivelLog.INFO);

	  for(int i=0;i<Tipo.totalRegistros;i++)
		{
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Fecha: " + Tipo.camposTabla[i][fecha], EIGlobal.NivelLog.INFO);
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Importe: " + Tipo.camposTabla[i][importe], EIGlobal.NivelLog.INFO);
          if(! (Tipo.camposTabla[i][fecha]==null || Tipo.camposTabla[i][fecha].trim().equals("")) )
            if (Tipo.camposTabla[i][fecha].trim().length()==8)
              {
                anio = Tipo.camposTabla[i][fecha].trim().substring(0, 4);
                mes = Tipo.camposTabla[i][fecha].trim().substring(4,6);
                dia = Tipo.camposTabla[i][fecha].trim().substring(6);
                Tipo.camposTabla[i][fecha]=dia + "/" + mes + "/" + anio ;
              }
		  if(! (Tipo.camposTabla[i][fecha2]==null || Tipo.camposTabla[i][fecha2].trim().equals("")) )
            if (Tipo.camposTabla[i][fecha2].trim().length()==8)
              {
                anio = Tipo.camposTabla[i][fecha2].trim().substring(0, 4);
                mes = Tipo.camposTabla[i][fecha2].trim().substring(4,6);
                dia = Tipo.camposTabla[i][fecha2].trim().substring(6);
                Tipo.camposTabla[i][fecha2]=dia + "/" + mes + "/" + anio ;
              }
		  if(! (Tipo.camposTabla[i][importe]==null || Tipo.camposTabla[i][importe].trim().equals("")) )
			  Tipo.camposTabla[i][importe]=FormatoMoneda(Tipo.camposTabla[i][importe]);
		}

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Campos formateados ....", EIGlobal.NivelLog.INFO);
	}


/*************************************************************************************/
/********************************************************** despliegaPaginaErrorURL  */
/*************************************************************************************/
	public void despliegaPaginaErrorURL( String Error, String param1, String param2, String param3, HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
	{
		/************* Modificacion para la sesion ***************/
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		/*Pagina de ayuda para mensajes de error y boton regresar*/
		param3="s55200h";

		request.setAttribute( "FechaHoy",EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute( "Error", Error );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, param3,request ) );

		request.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		request.setAttribute( "URL", "MTE_ConsultaCancelacion?ventana=0");

		RequestDispatcher rdSalida = getServletContext().getRequestDispatcher( "/jsp/EI_Mensaje.jsp" );
		rdSalida.include( request, response );
	}

/*************************************************************************************/
/********************************************************** Esportacion de Archivo   */
/*************************************************************************************/
 public String exportarArchivo(EI_Tipo Registros,String usuario)
   {
     String nombreArchivo=usuario+".tes";
	 String strBoton="";
	 StringBuffer arcLinea;
	 String rfcUsuario="";

     EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);
	 EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): Nombre Archivo. "+nombreArchivo, EIGlobal.NivelLog.INFO);

     if(ArcSal.creaArchivo())
      {
        int c[]={0,1,2,3,4,5};
		// Indice del elemento deseado
        int b[]={12,25,5,5,15,10};
		// Longitud maxima del campo para el elemento

		EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): Exportando Registros.", EIGlobal.NivelLog.INFO);
        for(int i=0;i<Registros.totalRegistros;i++)
         {
           arcLinea=new StringBuffer("");
           for(int j=0;j<6;j++)
			 arcLinea.append(ArcSal.formateaCampo(Registros.camposTabla[i][c[j]],b[j]));
           arcLinea.append("\n");
           if(!ArcSal.escribeLinea(arcLinea.toString()))
			 EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): Algo salio mal escribiendo "+arcLinea.toString(), EIGlobal.NivelLog.INFO);
         }

		EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): Se llevo a cabo la exportacion", EIGlobal.NivelLog.INFO);
        ArcSal.cierraArchivo();
        // Boton de Exportacion de Archivo.
		//*************************************************************
		ArchivoRemoto archR = new ArchivoRemoto();
        if(archR.copiaLocalARemoto(nombreArchivo,"WEB"))
 	      strBoton="<a href='/Download/"+nombreArchivo+"'><img border=0 src='/gifs/EnlaceMig/gbo25230.gif' alt='Exportar'></a>";
		else
		  EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): No se pudo generar la copia remota", EIGlobal.NivelLog.INFO);
		//**************************************************************
      }
     else
      {
        //strBoton="<b>Exportacion no disponible<b>";
		EIGlobal.mensajePorTrace("MTE_ConsultaCancelacion-> exportarArhivo(): No se pudo llevar a cabo la exportacion.", EIGlobal.NivelLog.INFO);
      }
	return strBoton;
  }


/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

/*************************************************************************************/
/************************************************************* formatea Campos Fecha */
/*************************************************************************************/
	public String formateaCamposfecha(String fec)
	 {
		String fecha=fec;
		if(fec.trim().length()==10)
			fecha=fec.substring(6) + fec.substring(3,5) + fec.substring(0, 2);
		return fecha;
	 }

/*************************************************************************************/
/************************************************************* Arma dias inhabiles   */
/*************************************************************************************/
  String armaDiasInhabilesJS()
   {
      StringBuffer resultado=new StringBuffer("");
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado.append(diasInhabiles.elementAt(indice).toString());
		 for(indice=1; indice<diasInhabiles.size(); indice++)
		  {
  		    resultado.append( ", " );
			resultado.append(diasInhabiles.elementAt(indice).toString());
		  }
       }
      return resultado.toString();
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas
 * @author CSA se actualiza para el manejo de cierre a base de Datos.
 * @since 17/11/2013
 */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{

	  EIGlobal.mensajePorTrace("***transferencia.class Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	  Connection  Conexion = null;
	  PreparedStatement qrDias = null;
	  ResultSet rsDias = null;
	  String     sqlDias;
	  Vector diasInhabiles = new Vector();

	  boolean    estado;

	  sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha >= sysdate-(365*2)";
	  EIGlobal.mensajePorTrace("***tranferencia.class Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	  try
	   {
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE);
		 if(Conexion!=null)
		  {
		    qrDias = Conexion.prepareStatement(sqlDias);
		    if(qrDias!=null)
		     {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		        {
		          while( rsDias.next())
					{
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  //******************
		          }
		       }
			  else
		         EIGlobal.mensajePorTrace("***tranferencia.class  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
		    }
		   else
		      EIGlobal.mensajePorTrace("***transferencia.class  Cannot Create Query", EIGlobal.NivelLog.ERROR);
		 }
		else
		 EIGlobal.mensajePorTrace("***transferencia.class Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
	   }catch(SQLException sqle ){
	   	sqle.printStackTrace();
	   } finally {
		   try {
			EI_Query.cierraConexion(rsDias, qrDias, Conexion);
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("***transferencia.class Cierra Conexion", EIGlobal.NivelLog.ERROR);
		}

	   }
	 return(diasInhabiles);
	}

/*************************************************************************************/
/******************************************************************* fecha Consulta  */
/*************************************************************************************/
   String generaFechaAnt(String tipo,int dia)
    {
      StringBuffer strFecha = new StringBuffer("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);
      if(dia==1)
		{
		  do
		   {
			 Cal.add(Calendar.DATE,-1);
		   }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);
		}

      if(tipo.equals("FECHA"))
       {
         if(Cal.get(Calendar.DATE) <= 9)
		  {
            strFecha.append("0" );
			strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append("/");
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append("0");
			strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append("/");
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
        }
      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

  public void generaTablaTipo(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
      /************* Modificacion para la sesion ***************/
      HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");

	  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Generando la tabla de consulta de archivos", EIGlobal.NivelLog.INFO);

	  String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/pruebaconsulta.prueba";
	  String Fecha1_01="10/09/2004";
	  String Fecha1_02="10/09/2004";
	  String importe="200.00";
	  String secuencia="";
	  String estatusArchivo="";

	  StringBuffer tablaRegistros=new StringBuffer("");
	  String consulta="";

	  String[] titulos={"Consulta de Archivos",
		       "Fecha de Recepci&oacute;n",
		       "Archivo",
		       "Secuencia",
		       "N&uacute;mero de Registros",
			   "Importe",
		       "Estatus"};

      int[] datos={5,1,0,1,2,3,4,5};
	  int[] values={5,0,1,2,3,4,5};
	  int[] align={1,2,0,1,2,0};

	  EI_Tipo regSel=new EI_Tipo();

	  consulta=obtenDatos(nombreArchivo);

	  if(consulta.trim().equals("ERR"))
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se pudo realiza la consulta el archivo tiene errores", EIGlobal.NivelLog.INFO);
		  despliegaPaginaErrorURL("La <i><font color=blue>transacci&oacute;n</font></i> solicitada no se llevo a cabo correctamente.<br>Por favor intente mas tarde.","Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55000h",req,res);
		  return;
	   }
	  else
	  if(consulta.trim().equals(""))
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> No se encontraron registros en el archivo.", EIGlobal.NivelLog.INFO);
		  despliegaPaginaErrorURL("No se encontraron <font color=green>registros</font> para el rango de fechas y/o estatus seleccionados","Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros","s55000h",req,res);
		  return;
	   }
	  else
	   {
		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Objetos creados para la consulta de archivos", EIGlobal.NivelLog.INFO);

		  regSel.iniciaObjeto(';','@',consulta);
		  tablaRegistros.append(regSel.generaTabla(titulos,datos,values,align));

		  req.setAttribute("Fecha1_01",Fecha1_01);
		  req.setAttribute("Fecha1_02",Fecha1_02);
		  req.setAttribute("importe",importe);
		  req.setAttribute("secuencia",secuencia);
		  req.setAttribute("estatusArchivo",estatusArchivo);
		  req.setAttribute("nombreArchivo",nombreArchivo);

		  req.setAttribute("tablaRegistros",tablaRegistros.toString());

		  req.setAttribute("ClaveBanco",(session.getClaveBanco()==null)?"014":session.getClaveBanco());
		  req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
		  req.setAttribute("ClaveP", (session.getPassword()==null)?"":session.getPassword());

		  req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
		  req.setAttribute("Encabezado", CreaEncabezado("Consulta de Archivos y Registros","Servicios &gt; TESOFE &gt Consulta de Archivos y Registros &gt Consulta por Archivos","s55000h",req));

		  EIGlobal.mensajePorTrace( "MTE_ConsultaCancelacion-> Se presenta pantalla de consulta de archivos", EIGlobal.NivelLog.INFO);

		  evalTemplate ("/jsp/MTE_Tipo.jsp", req, res );
	   }
   }
}