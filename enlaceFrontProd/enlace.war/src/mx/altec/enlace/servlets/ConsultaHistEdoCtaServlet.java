package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import mx.altec.enlace.beans.ConsEdoCtaDetalleHistBean;
import mx.altec.enlace.beans.ConsultaEdoCtaHistBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConsultaEdoCtaHistBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

public class ConsultaHistEdoCtaServlet extends BaseServlet {
	/**
	 * serialVersion
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * SESSION_BR
	 */
	private static final String SESSION_BR = "session";
	
    /**
     * NUM_PAGINA_DETALLE
     */
    private static final String NUM_PAGINA_DETALLE = "numPagDet";
	
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Estados de Cuenta";
	
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Estados de Cuenta &gt; Solicitud de Periodos Hist&oacute;ricos PDF &gt; Consulta";
	
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_ConSoliPerHis";
	
	/** Constante para mensaje usuario sin facultades **/
	private static final String STR_SIN_FACULTADES =  "<br>Usuario sin facultades para operar con este servicio<br>";
	
	/**
	 * RANGO
	 */
	private static final int RANGO = 20;
	
	/**
	 * TIPO_OPER
	 */
	private static final String TIPO_OPER = "S";
	
	/**
	 * admin
	 */
	private final transient BitaAdminBean admin = new BitaAdminBean();
	
	/**
	 * Get
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doGet( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Metodo DoGet", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}

	/**
	 * Post
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void doPost( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Metodo DoPost", EIGlobal.NivelLog.DEBUG);
		defaultAction( req, res );
	}
	
	/**
	 * Metodo ejecutado en caso de get o post
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws IOException : manejo de excepcion
	 */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
	{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

		String opcion= (String)req.getParameter("opcion");
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet opcion " + opcion, EIGlobal.NivelLog.INFO);

		if(opcion==null){
			opcion="0";
		}
		int op = Integer.parseInt(opcion);
	    boolean sesionvalida = SesionValida( req, res);
		if ( sesionvalida )
		 {

			if(session.getFacultad(BaseResource.FAC_CONS_EDO_HIST))
			{
				/** switch (op){
					case 0:
						iniciaConsulta(req, res);
						break;
						
					case 2:
						realizaConsultaDetalle(req, res);
						break;
				}*/
                                if (op == 0){
                                    iniciaConsulta(req, res);
                                }else if(op == 2){
                                    realizaConsultaDetalle(req, res);
                                }
			}
			else{
				despliegaPaginaErrorURL(STR_SIN_FACULTADES, TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, req, res);
			}
		 }

	}
	
		/**
	 * Inicia la consulta de la configuracion
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws java.io.IOException : manejo de excepcion
	 */
	public void iniciaConsulta(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Metodo iniciaConsulta", EIGlobal.NivelLog.DEBUG);
		
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);
		
		generaMenu(req, session);
		
        req.setAttribute ("anio",ObtenAnio ());
        req.setAttribute ("mes",ObtenMes ());
        req.setAttribute ("dia",ObtenDia ());
        req.setAttribute("diasInhabiles", diasInhabilesAnt());
        
        admin.setTipoOp("");
        admin.setReferencia(100000);
        BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_HIST, admin, boolLogin);
		
		evalTemplate("/jsp/consultaEdoCtaHist.jsp", req, res);
	}
	
	/**
	 * Realiza la consulta del detalle
	 * @param req : request
	 * @param res : response
	 * @throws ServletException : manejo de excepcion
	 * @throws java.io.IOException : manejo de excepcion
	 */
	public void realizaConsultaDetalle(HttpServletRequest req,HttpServletResponse res) throws ServletException, java.io.IOException
	{
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Entra a metodo realizaConsultaDetalle", EIGlobal.NivelLog.DEBUG);

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION_BR);

		ConsultaEdoCtaHistBO edoCtaHistBO = new ConsultaEdoCtaHistBO();                
		ConsultaEdoCtaHistBean edoCtaHistBean = new ConsultaEdoCtaHistBean();
                
		String lineasArchivo = null;
		
		String folio = req.getParameter("folio") != null ? req.getParameter("folio") : "";
		String usuarioSol = req.getParameter("usuarioSol") != null ? req.getParameter("usuarioSol") : "";
		String fechaIni = req.getParameter("fechaIni") != null ? req.getParameter("fechaIni") : "";
		String fechaFin = req.getParameter("fechaFin") != null ? req.getParameter("fechaFin") : "";
		String estatus = req.getParameter("estatus") != null ? req.getParameter("estatus") : "";
		
		
		int numPag = req.getParameter(NUM_PAGINA_DETALLE) != null ? Integer.parseInt(req.getParameter(NUM_PAGINA_DETALLE)) : 1;
		int totalPaginas = 1;
		int regTotal = 0;
		
		edoCtaHistBean.setRegIni((numPag-1)*RANGO);
		edoCtaHistBean.setRegFin(numPag*RANGO);
		edoCtaHistBean.setFolio(folio);
		edoCtaHistBean.setUsuarioSolicitante(usuarioSol);
		edoCtaHistBean.setFechaInicio(fechaIni);
		edoCtaHistBean.setFechaFin(fechaFin);
		edoCtaHistBean.setEstatus(estatus);
		edoCtaHistBean.setContrato(session.getContractNumber());
        
        if("exp".equals(req.getParameter("archivo"))){
			res.setContentType("text/plain");
			res.setHeader(
					"Content-Disposition", "attachment;filename=EstatusPeriodosHistoricosPDF.txt");
			lineasArchivo = edoCtaHistBO.consultaEdoCtaDetalleTotalHist(edoCtaHistBean);
			InputStream input;
			
			try {
				input = new ByteArrayInputStream(lineasArchivo.getBytes("UTF8"));
				
				int read = 0;
				byte[] bytes = new byte[1024];
				OutputStream os = res.getOutputStream();

				while ((read = input.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				os.flush();
				os.close();
				return;
			} catch (UnsupportedEncodingException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			} catch (IOException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.DEBUG);
			}
		}

		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Invoca BO ConsultaEdoCtaHistBO.consultaEdoCtaHistDetalle", EIGlobal.NivelLog.DEBUG);
		List<ConsEdoCtaDetalleHistBean> edoCtaListHistDetalle = edoCtaHistBO.consultaEdoCtaHistDetalle(edoCtaHistBean);
		    
		if(edoCtaListHistDetalle != null && !edoCtaListHistDetalle.isEmpty() ){
			regTotal = edoCtaListHistDetalle.get(0).getTotalRegistros();
		}else{
			despliegaPaginaErrorURL("No existen datos disponibles para su consulta.","Estados de Cuenta","Estados de Cuenta &gt; Solicitud de Periodos Hist&oacute;ricos PDF &gt; Consulta",ARCHIVO_AYUDA,"ConsultaHistEdoCtaServlet?opcion=0",req,res);
			return;
		}
		
		if(regTotal>RANGO){
			totalPaginas = regTotal/RANGO;
			if(regTotal%RANGO > 0){
				totalPaginas++;
			}
		}

		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Regresa de ConsultaEdoCtaHistBO.consultaEdoCtaHistDetalle", EIGlobal.NivelLog.DEBUG);
		req.setAttribute("edoCtaListHistDetalle", edoCtaListHistDetalle); 
		req.setAttribute("totalPaginas",totalPaginas); 	
	
		req.setAttribute("idEdoCtaHist",folio);
		req.setAttribute("numPagDet",numPag); 
		
		req.setAttribute("folioC", req.getParameter("folio"));
		req.setAttribute("estatusC", req.getParameter("estatus"));
		req.setAttribute("descCta", req.getParameter("descCta"));
		req.setAttribute("numCta",req.getParameter("numCta"));
		req.setAttribute("usuarioSolC", req.getParameter("usuarioSol"));
		req.setAttribute("fechaIniC", req.getParameter("fechaIni"));
		req.setAttribute("fechaFinC", req.getParameter("fechaFin"));
		
		generaMenu(req, session);
	
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BitacoraBO.bitacoraTCT(req, sess, BitaConstants.CONSULTA_DET_HIST, 
					obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
					"", "", BitaConstants.COD_ERR_PDF_XML,
					BitaConstants.CONCEPTO_CONSULTA_HIST);
		}
		
		EIGlobal.mensajePorTrace( "ConsultaHistEdoCtaServlet-> Despliega consultaEdoCtaHistDetalle.jsp", EIGlobal.NivelLog.DEBUG);
		admin.setTipoOp(TIPO_OPER);
		admin.setReferencia(100000);
        BitacoraBO.bitacorizaAdmin(req, sess, BitaConstants.CONSULTA_RES_HIST, admin, boolLogin); 
		
		evalTemplate("/jsp/consultaEdoCtaHistDetalle.jsp", req, res);
	}

	/**
	 * generaMenu
	 * @param req : req
	 * @param session : session
	 */
	private void generaMenu(HttpServletRequest req, BaseResource session){
		
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Estados de Cuenta","Estados de Cuenta &gt; Solicitud de Periodos Hist&oacute;ricos PDF &gt; Consulta",ARCHIVO_AYUDA,req));
		
	}
}	