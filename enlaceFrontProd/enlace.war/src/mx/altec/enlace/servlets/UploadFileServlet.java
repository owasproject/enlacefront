package mx.altec.enlace.servlets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.CertificadoDigitalBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.CertificadoDigitalBOImpl;
import mx.altec.enlace.bo.DatosMancInter;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

import com.mx.isban.crypto.util.Utility;

import com.mx.isban.certificados.service.exception.GestorCertificadosException;


public class UploadFileServlet extends BaseServlet {
    /**
	 * serial generado
	 */
	private static final long serialVersionUID = 1L;
	
	/** CONSTANTE EXISTE **/
	private static final String EXISTE = "EXISTE";
	
	/**CONSTANTE ERROR**/
	private static final String ERROR = "ERROR";
	
	/**Cadena para el nombre del archivo**/
	private static final String CADENA_NOMBRE_ARCHIVO = "nombreArchivo";
	
	/**CONSTANTE SESSION**/
	private static final String SESSION = "session";	
	
	/**CONSTANTE CLASE**/
	private static final String CLASE = "Clase [ ";
	
	/**CONSTANTE CONCATENA**/
	private static final String CONCATENA = "] | ";
	
	  /** {@inheritDoc} */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra----- UploadFileServlet: --> doPost ver2", EIGlobal.NivelLog.INFO);
    	String nombreOriginal = getFormParameter(request, "fileName"), valida = request.getParameter("valida");
    	
    	//Baja archivo antes de entrar al token
    	if(nombreOriginal != null && !"".equals(nombreOriginal)) {
    		request.getSession().setAttribute(CADENA_NOMBRE_ARCHIVO, nombreOriginal);
    	}
    	
    	HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
    	boolean sesionvalida = SesionValida( request, response );
    	
    	if ( !sesionvalida ) {
			return;
		}
		
		if (valida == null) {
			valida = validaToken(request, response);
		}
		
		if (valida != null && "1".equals(valida)) {
			
			String certificadoStrign = "", nombreFile = "", password = getFormParameter2(request,"passwordTxt");
			
			nombreFile = request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO) == null ? "" : request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO).toString();
			
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra UploadFileServlet: --> Nombre Archivo despuest token ::: " + nombreFile, EIGlobal.NivelLog.INFO);
			
			File archivo = new File( IEnlace.LOCAL_TMP_DIR +"/"+ nombreFile );
			InputStream inStream = new FileInputStream(archivo);
			
			certificadoStrign = getCertificate(inStream, nombreFile, password, request, response);			
			if(inStream != null){
				inStream.close();
			}
			CertificadoDigitalBOImpl certificadoDigitalBO = new CertificadoDigitalBOImpl();
			CertificadoDigitalBean certificado = new CertificadoDigitalBean();
			certificado.setCertificado(certificadoStrign);
			boolean existe = Boolean.parseBoolean(request.getAttribute(EXISTE) != null ? request.getAttribute(EXISTE).toString() : "false"), resultado = false;
			certificado.setExiste(existe);
			String cliente = session.getUserID8(), contrato = session.getContractNumber();
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			
			try{
				resultado = certificadoDigitalBO.altaCertificado(certificado, Global.CANAL_CIFRADO, cliente, contrato);
			}catch(GestorCertificadosException e){
				EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ altaCertificado ] EXCEPCION:: " + e.getMessage() + "COD_ERROR:: " + e.getCodeError(), EIGlobal.NivelLog.DEBUG );
				
				if ( "CAEXT06".equals( e.getCodeError() ) ||  "CAEXT07".equals( e.getCodeError() ) || "CAEXT08".equals( e.getCodeError() )){
					request.setAttribute( ERROR , "El servicio de la entidad certificadora para validar su certificado no se encuentra disponible, p&oacute;ngase en contacto con su Administrador o int&eacute;ntelo m&aacute;s tarde." );
				}else if("CAEXT15".equals(e.getCodeError()) || "BO004".equals(e.getCodeError())){
					request.setAttribute( ERROR , "CAEXT15".equals(e.getCodeError()) ? "Entidad Certificadora No Registrada en el Sistema" : "El Certificado Digital est&aacute; pronto a expirar");
				}else if("8".equals(e.getCodeError()) || "5".equals(e.getCodeError())){
					request.setAttribute( ERROR , "8".equals(e.getCodeError()) ? "El Certificado Digital est&aacute; vencido" : "Favor de verificar el formato del Certificado.");
				}else if("CAEXT16".equals(e.getCodeError())){
					request.setAttribute(ERROR, "El Certificado Digital se encuentra Revocado");
				}else{
					request.setAttribute(ERROR, "No fue posible dar de Alta el Certificado Digital, p&oacute;ngase en contacto con su Administrador o int&eacute;ntelo m&aacute;s tarde");
				}
				bitacorizaProceso(request, referencia, "IMCD","0001",nombreFile,"R");
				direccionamiento(request, response);
				return;
			}catch(javax.xml.ws.soap.SOAPFaultException e){
				request.setAttribute(ERROR, "No fue posible dar de Alta el Certificado Digital, p&oacute;ngase en contacto con su Administrador o int&eacute;ntelo m&aacute;s tarde");
				bitacorizaProceso(request, referencia, "IMCD","0001",nombreFile,"R");
				direccionamiento(request, response);
				return;
			}
			
			EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ altaCertificado ] String de certificado:: " + certificadoStrign, EIGlobal.NivelLog.DEBUG );
			request.getSession().removeAttribute(CADENA_NOMBRE_ARCHIVO);
			resultadoAlta(request, response, certificadoDigitalBO, cliente, contrato, resultado,nombreFile);
		}
	        
     
    }

	/**
	 * @param request : request
	 * @param response : response
	 * @param certificadoDigitalBO : certificadoDigitalBO
	 * @param cliente : cliente
	 * @param contrato : contrato
	 * @param resultado : resultado
	 * @throws ServletException : Excepcion generada
	 * @throws IOException : Excepcion generada
	 */
	private void resultadoAlta(HttpServletRequest request,
			HttpServletResponse response,
			CertificadoDigitalBOImpl certificadoDigitalBO, String cliente,
			String contrato, 
			boolean resultado,
			String nombreArchivo) throws ServletException,
			IOException {
		
		//Si hay algun error en el certificado
		if(!resultado)  {
			request.setAttribute(ERROR, "El Certificado est&aacute; revocado, favor de intentar con un certificado activo.");
			request.setAttribute(EXISTE, "false");
			direccionamiento(request, response);
		} else {
			boolean validar = false;
			CertificadoDigitalBean certificadoDigital = new CertificadoDigitalBean();
			try{
				certificadoDigital = certificadoDigitalBO.consultaCertificado(Global.CANAL_CIFRADO, cliente, contrato, validar);
			}catch(BusinessException e){
				EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ altaCertificado ] EXCEPCION:: " + e.getMessage(), EIGlobal.NivelLog.DEBUG );
				request.setAttribute(ERROR, "No fue posible realizar la operaci&oacute;n.  ");
				direccionamiento(request, response);
				return;
			}
			
			request.setAttribute("datosCertificado", certificadoDigital);
			request.setAttribute(EXISTE, certificadoDigital.isExiste());
			request.setAttribute("EXITO", "Alta de Certificado Digital exitosa.");
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
			bitacorizaProceso(request, referencia, "IMCD","0000",nombreArchivo,"A");
		}
		
		direccionamiento(request, response);
	}
    
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @return String
	 * @throws IOException : exception
	 */
	@SuppressWarnings("static-access")
	private String validaToken(HttpServletRequest request, HttpServletResponse response) 
		throws IOException{
		EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSION);
		//boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.CIFRADO_DIGITAL);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), 1045);
		
		if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && 
				session.getToken().getStatus() == 1 && solVal) {
			EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
			
			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request, "CifradoDigital");
			ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
		} else {
			return "1";
		}
		return "";
		
	}
	
	/**
	 * Metodo para direccioar
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Exception
	 * @throws IOException : Exception
	 */
	private void direccionamiento (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSION);
		
		request.setAttribute("opcion", request.getParameter("opcion"));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Certificado digital","Administraci&oacute;n y Control &gt; Certificado digital", "CERTDIG01", request));
		request.setAttribute("RUTA_IMAGENES", Global.RUTA_IMAGENES);
		//request.getRequestDispatcher("/jsp/certificadoDigital.jsp").forward(request, response);
		request.getRequestDispatcher(Global.RUTA_CERTIFICADOJSP).forward(request, response);
	}
	
	
	/**
	 * @param request : request
	 * @param referencia : referencia clave de bitacora
	 * @param idFlujo : id del flujo
	 * @throws ServletException 
	 * @throws IOException 
	 */
	private void bitacorizaProceso(HttpServletRequest request,  int referencia, String idFlujo,String codigoError,String nombreArchivo,String status) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra UploadFileServlet: --> ::: bitacorizaProceso" , EIGlobal.NivelLog.INFO);
		String idFlujoBitacora = idFlujo;
		String numBit = idFlujoBitacora.concat("01");
		HttpSession sess = request.getSession();
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra UploadFileServlet: --> ::: bitacorizaProceso --> id bitacora ::: " + idFlujoBitacora , EIGlobal.NivelLog.INFO);
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			String idFlujoEntrada = (String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO);
			BaseResource session = (BaseResource) sess.getAttribute(SESSION);
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			Date hoy = new Date();
			
			bh.incrementaFolioFlujo(idFlujoBitacora);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(idFlujoBitacora);
			bt.setNumBit(numBit);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	
			bt.setReferencia(referencia);
			String codError = idFlujoBitacora + codigoError;
			bt.setIdErr(codError);
			bt.setIdToken(session.getToken().getSerialNumber());				
			bt.setFechaAplicacion(hoy);
			bt.setNombreArchivo(nombreArchivo);
			bt.setEstatus(status);
			
			request.getSession().setAttribute(BitaConstants.SESS_ID_FLUJO, idFlujoEntrada);

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos " + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			
			try {
 				BitaTCTBean beanTCT = new BitaTCTBean ();

 				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(idFlujo);
				beanTCT.setCodError(idFlujo.concat(codigoError));
				beanTCT.setUsuario(session.getUserID8());
	
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
			
		}
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra UploadFileServlet: --> ::: FIN ---> bitacorizaProceso" , EIGlobal.NivelLog.INFO);
	}
	
	/**
	 * @param isCert : isCert 
	 * @param nombreFile : nombreFile
	 * @param password : password	
	 * @param request : request
	 * @param response : response
	 * @return String certificado
	 * @throws ServletException : Excepcion generada
	 * @throws IOException : Excepcion generada
	 */
	private String getCertificate(InputStream isCert, String nombreFile, String password, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace("\n\n UploadFileServlet getCertificate ejecutando...", EIGlobal.NivelLog.INFO);

		InputStream inStream;
		String fileExtension;
		String fileName;
		String strCert;
		

		inStream = isCert;	
		fileName = nombreFile;	
		int i = fileName.lastIndexOf('.');		
		fileExtension = fileName.substring(i+1);
		strCert = null;
		
		EIGlobal.mensajePorTrace("\n\n UploadFileServlet getCertificate [NAME FILE]  " + fileName + " [EXTENSION] " + fileExtension , EIGlobal.NivelLog.INFO);
		
		if("p12".equals(fileExtension) || "pfx".equals(fileExtension)){	
			InputStream is = null;
			try {
				
				EIGlobal.mensajePorTrace("\n\n UploadFileServlet getCertificate password" + password, EIGlobal.NivelLog.INFO);
				X509Certificate cert = Utility.getContentPKSC12(inStream, password);
				is = new ByteArrayInputStream(cert.getEncoded());
				strCert = Utility.convertToPem(is);
				EIGlobal.mensajePorTrace("\n\n************** PKCS12 **********************", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(strCert, EIGlobal.NivelLog.INFO);
			} catch (CertificateEncodingException e) {
				EIGlobal.mensajePorTrace("[UploadFilServlet Exception getCertificate] "  + e.getMessage(), EIGlobal.NivelLog.ERROR);
				request.setAttribute(ERROR, "No fue posible dar de Alta el Certificado Digital, verifique que sea un archivo v�lido");
				direccionamiento(request, response);
			}catch (com.mx.isban.crypto.exception.BusinessException e){
				EIGlobal.mensajePorTrace("[UploadFilServlet Exception getCertificate] "  + e.getMessage(), EIGlobal.NivelLog.ERROR);
				request.setAttribute(ERROR, "No fue posible dar de Alta el Certificado Digital, verifique que sea un archivo v�lido y que la contrase�a sea correcta");
				direccionamiento(request, response);
			}finally{
				if(is != null){
					is.close();
				}
			}
		}else if("cer".equals(fileExtension) || "crt".equals(fileExtension) || "der".equals(fileExtension)){
			try {
				strCert = Utility.convertToPem(inStream);
				EIGlobal.mensajePorTrace("\n\n************** CERT **********************", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(strCert, EIGlobal.NivelLog.INFO);
			} catch (com.mx.isban.crypto.exception.BusinessException e) {
				EIGlobal.mensajePorTrace("[UploadFilServlet Exception getCertificate] "  + e.getMessage(), EIGlobal.NivelLog.ERROR);
				request.setAttribute(ERROR, "No fue posible dar de Alta el Certificado Digital, verifique que sea un archivo v�lido");
				direccionamiento(request, response);
			}finally{
				if(inStream != null){
					inStream.close();
				}
			}
		}
		return strCert;
	}	
	
	
}
