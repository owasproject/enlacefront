package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.Method;
import java.lang.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//02/01/07

public class BE_ConsultaSaldos extends BaseServlet{
    //int    salida;
    //String cta_saldo;
	//String tramaPosicion="";
	//String posicion;
	//String saldoCartera;

    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

      HttpSession sess = req.getSession();
      BaseResource session = (BaseResource) sess.getAttribute("session");

	  EIGlobal.mensajePorTrace( "***BE_ConsultaSaldos.class  &Entrando al metodo execute&",EIGlobal.NivelLog.INFO );
      boolean sesionvalida = SesionValida( req, res );
      //if(sesionvalida&&session.getFacCSaldo().trim().length()!=0){//10
	  if(sesionvalida && session.getFacultad(session.FAC_CONSULTA_SALDO)){
      	req.setAttribute("MenuPrincipal", session.getStrMenu());
	    req.setAttribute("newMenu", session.getFuncionesDeMenu());
	  	req.setAttribute("Encabezado", CreaEncabezado("Consulta de saldos por cuenta de Banca especializada","Consultas &gt; Saldos &gt; Por cuenta &gt; Banca Especializada", "s25030h",req));

	   //***********************************************
       //modificación para integración pva 09/03/2002
       //***********************************************

  	    EIGlobal.mensajePorTrace( "***BE_ConsultaSaldos  prev"+req.getParameter("prev"), EIGlobal.NivelLog.INFO);

  	    EIGlobal.mensajePorTrace( "***BE_ConsultaSaldos  next"+req.getParameter("next"), EIGlobal.NivelLog.INFO);

  	    EIGlobal.mensajePorTrace( "***BE_ConsultaSaldos  total"+req.getParameter("total"), EIGlobal.NivelLog.INFO);



		int total=0,prev=0,next=0;

		String paginacion="";

        if  ((req.getParameter("prev")!=null) &&

			  (req.getParameter("next")!=null) &&

			 (req.getParameter("total")!=null))

	    {
		  prev =  Integer.parseInt(req.getParameter("prev"));
          next =  Integer.parseInt(req.getParameter("next"));

		  total = Integer.parseInt(req.getParameter("total"));

		}
        else

	    {

           EIGlobal.mensajePorTrace( "***BE_ConsultaSaldos", EIGlobal.NivelLog.INFO);
   		   llamado_servicioCtasInteg(IEnlace.MConsulta_Saldos_BE," "," "," ",session.getUserID8()+".ambci",req);
           total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");
	        prev=0;

		    if (total>Global.NUM_REGISTROS_PAGINA)

		      next=Global.NUM_REGISTROS_PAGINA;

		   else

			  next=total;


		}
       //***********************************************

		int indice=1;
		int elementos=0;
        String strSalida2="";
        String Contrato=session.getContractNumber();

		//***********************************************
        //modificación para integración pva 09/03/2002
        //***********************************************
   		//String[][] arrayCuentas=BEContCtas(Contrato);

		String[][] arrayCuentas = ObteniendoCtasInteg(prev,next,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID8()+".ambci");

		//-- POST
		sess.setAttribute("CUENTAS_POSICION", arrayCuentas);
		// -- POST

		if(arrayCuentas.length>=2){//12
			int residuo=0;
			String tipo_cuenta="";
			int contador=1;

	        for (int indice2=1;indice2<=Integer.parseInt(arrayCuentas[0][0]);indice2++){
					residuo=contador % 2 ;
				String bgcolor="textabdatobs";
				contador++;
				if (residuo == 0){
					bgcolor="textabdatcla";
				}

			    //***********************************************
                //modificación para integración pva 06/03/2002
                //***********************************************
		        /*

				tipo_cuenta=arrayCuentas[indice2][1].substring(0, EIGlobal.NivelLog.ERROR);

				if (arrayCuentas[indice2][2].equals("P")||arrayCuentas[indice2][2].equals("T")){
					System.out.println("(1er filtro)la cuenta del arreglo es: "+arrayCuentas[indice2][1]);
					if(arrayCuentas[indice2][2].equals("P")){
						if(!existeFacultadCuenta(arrayCuentas[indice2][1],"CONSSALDOS", "-"))
							strSalida2 = strSalida2 + "<tr><td class=\""+bgcolor+"\" align=right><input type=checkbox name=cta value="+arrayCuentas[indice2][1] + "></td><td class=\""+bgcolor+"\" height=18>"+ arrayCuentas[indice2][1]+"</td><td class=\""+bgcolor+"\" height=18>&nbsp;"+arrayCuentas[indice2][4]+"</td>";
					}
					if(arrayCuentas[indice2][2].equals("T")){
						if(existeFacultadCuenta(arrayCuentas[indice2][1],"CONSSALDOTER", "+"))
							strSalida2 = strSalida2 + "<tr><td class=\""+bgcolor+"\" align=right><input type=checkbox name=cta value="+arrayCuentas[indice2][1] + "></td><td class=\""+bgcolor+"\" height=18>"+ arrayCuentas[indice2][1]+"</td><td class=\""+bgcolor+"\" height=18>&nbsp;"+arrayCuentas[indice2][4]+"</td>";
					}
				}*/
				//strSalida2 = strSalida2 + "<tr><td class=\""+bgcolor+"\" align=right><input type=checkbox name=cta value="+arrayCuentas[indice2][1] + "></td><td class=\""+bgcolor+"\" height=18>"+ arrayCuentas[indice2][1]+"</td><td class=\""+bgcolor+"\" height=18>&nbsp;"+arrayCuentas[indice2][4]+"</td>";
				strSalida2 = strSalida2 + "<tr><td class=\""+bgcolor+"\" align=right><input type=checkbox name=cta value="+indice2 + "></td><td class=\""+bgcolor+"\" height=18>"+ arrayCuentas[indice2][1]+"</td><td class=\""+bgcolor+"\" height=18>&nbsp;"+arrayCuentas[indice2][4]+"</td>";


			}
			req.setAttribute("cuentas",strSalida2);

			//***********************************************

           //modificación para integración pva 06/03/2002
            //***********************************************

			if (total>Global.NUM_REGISTROS_PAGINA)

		    {

              paginacion="<table  width=563 align=center>";

		       if(prev > 0)

                paginacion+= "<tr><td  align=right class=\"textabref\" ><A HREF=\"javascript:history.back();\">< Anteriores "+Global.NUM_REGISTROS_PAGINA+"</A></td></TR>";

              if(next < total)

              {

                if((next + Global.NUM_REGISTROS_PAGINA) <= total)

 	              paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(next -prev).toString() + " ></A></td></TR>";

                else
                  paginacion += "<tr><td  align=right class=\"textabref\"><A HREF=\"javascript:adelante();\">Siguientes " + new Integer(total-next).toString() + " ></A></td></TR>";

              }

              paginacion+="</table><br><br>";

    	       EIGlobal.mensajePorTrace( "***paginacion :"+paginacion, EIGlobal.NivelLog.INFO);

		    }
		    req.setAttribute("total", ""+total);

		    req.setAttribute("prev", ""+prev);

		    req.setAttribute("next", ""+next);

			req.setAttribute("paginacion", paginacion);

			req.setAttribute("varpaginacion", ""+Global.NUM_REGISTROS_PAGINA);
            //***********************************************
//			TODO  BIT CU1021
	        /*
			 * 02/ENE/07
			 * VSWF-BMB-I
			 */
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try{
			        BitaHelper bh = new BitaHelperImpl(req, session, sess);
			        if(req.getParameter(BitaConstants.FLUJO)!=null){
			        	bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
			        }else{
			        	bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
			        }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_SALDO_CUENTA_BANCA_ENTRA);
					if(Contrato!=null){
						bt.setContrato(Contrato);
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
	        /*
			 * VSWF-BMB-F
			 */

			evalTemplate(IEnlace.BE_CONSULTASALDO_TMPL, req, res);
		}
		else{
			EIGlobal.mensajePorTrace( "No existen cuentas asociadas a este contrato",EIGlobal.NivelLog.INFO );
			despliegaPaginaError("No existen cuentas asociadas a este contrato","Consultas de Banca especializada", "Consultas &gt; Saldos &gt; Por cuenta &gt; Banca Especializada", "s25030h", req, res);

		}
     }
     else if(sesionvalida){
       req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
       evalTemplate(IEnlace.ERROR_TMPL, req, res);
     }
    }
}