	/*************************************************************************************************/
	 /** Clase Principal para la administracion del nuevo Catalogo de Nomina **/
	 /** 						de empleados con cuentas interbancarias
	 /** 	15/oct/2007  																			**/
	 /** Nuevo Alcance																				**/
	 /** 	05 - Febrero - 2008																		**/
	 /** EVERIS MEXICO																				**/
	 /************************************************************************************************/

	package	mx.altec.enlace.servlets;

	import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.beans.NomPreAlta;
import mx.altec.enlace.beans.NomPreEmpleado;
import mx.altec.enlace.beans.NomPreLM1D;
import mx.altec.enlace.beans.NomPreLM1E;
import mx.altec.enlace.beans.NomPreCodPost;
import mx.altec.enlace.beans.TarjetaContrato;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CatNominaAction;
import mx.altec.enlace.bo.CatNominaBean;
import mx.altec.enlace.bo.CatNominaConstantes;
import mx.altec.enlace.bo.CatNominaValidaInterb;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EnlcTarjetaBO;
import mx.altec.enlace.bo.EnlcTarjetaBOImpl;
import mx.altec.enlace.bo.NomPreBusquedaAction;
import mx.altec.enlace.bo.ValidaArchivoAltas;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.NomPreEmpleadoDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.FileUtilities;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

	/**
	 * @author vision
	 * Clase que realiza acciones de nomina prepago
	 *
	 */
	public class NomPreAltaTarjeta extends BaseServlet
	{
		/**
		 * Serial
		 */
		private static final long serialVersionUID = -6225627412976885211L;
		/**
		 * log
		 */
		private static final String textoLog = NomPreAltaTarjeta.class.getName();
		private static final String rutadeSubida = Global.DOWNLOAD_PATH;

		/**
		 * Texto log
		 * @param funcion string
		 * @return String NomPreAltaTarjeta
		 */
		private String textoLog(String funcion){
			return NomPreAltaTarjeta.class.getName() + "." + funcion + "::";
		}

		/**
		 * Metodo que recibe las peticiones get
		 * @param request peticion
		 * @param response respuesta
		 * @throws ServletException excepcion
		 * @throws IOException excepcion
		 */
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			defaultAction (request, response);
		}

		/**
		 * Metodo que recibe las peticiones post
		 * @param request peticion
		 * @param response respuesta
		 * @throws ServletException excepcion
		 * @throws IOException excepcion
		 */
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			defaultAction (request, response);
		}

		/**
		 * Metodo default
		 * @param request peticion
		 * @param response respuesta
		 * @throws ServletException excepcion
		 * @throws IOException excepcion
		 */
		public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{
			ArrayList listaAltaFinal = new ArrayList();
			ArrayList listaModifInterb = new ArrayList();

			try{

				String numContrato="";
				String descContrato="";
				String usuario="";
				String clavePerfil="";

				RequestDispatcher rd = null;
				String opcion = "";

				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				boolean sesionvalida=SesionValida( request, response );
				if(sesionvalida)
				{
					String valida = getFormParameter(request,"valida" );
					if (validaPeticion(request, response, session, sess, valida)) {
						EIGlobal.mensajePorTrace(textoLog + "ENTRA A VALIDAR LA PETICION", EIGlobal.NivelLog.INFO);
					}
					else {
					/* everis JARA - Ambientacion - Obtencion de variables de session */
					numContrato=(session.getContractNumber()==null)?"":session.getContractNumber();
					//numContrato="01234567890";
					EIGlobal.mensajePorTrace("contrato: "+numContrato,EIGlobal.NivelLog.INFO);

					descContrato=(session.getNombreContrato()==null)?"":session.getNombreContrato();
					//descContrato="VERONICA LA UYUYUY";
					EIGlobal.mensajePorTrace("Descripcion de contrato: "+descContrato,EIGlobal.NivelLog.INFO);

					usuario=(session.getUserID8()==null)?"":session.getUserID8();
					//usuario=sess.getId();
					EIGlobal.mensajePorTrace("usuario: "+usuario,EIGlobal.NivelLog.INFO);
					clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();
					//clavePerfil="NO SE";
					EIGlobal.mensajePorTrace("clavePerfil: "+clavePerfil,EIGlobal.NivelLog.INFO);
					//modulo=(String) request.getParameter("Modulo");
					EIGlobal.mensajePorTrace("el modulo es: "+modulo ,EIGlobal.NivelLog.INFO);
					/* everis JARA - Ambientacion - Se carga los atributos de men princiapal ******/
					try
						{
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());
						}
					catch(Exception e)
						{
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						}
					/* everis JARA - Ambientacion -  Se carga los atributos de men princiapal ******/

					opcion = request.getParameter("opcion");

				//		 Si es la primera vez se carga la pagina por default
					if(opcion == null)
						opcion = "1";
					EIGlobal.mensajePorTrace("NomPreAltaTarjeta Opcion = " + opcion, NivelLog.DEBUG);


					/***********************************************************
					 *** opcion 1 = Cargar pagina de busqueda de Empleados.  ***
					 **********************************************************/
					if(opcion.equals("1"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute("Encabezado",CreaEncabezado("Alta de Relaci&oacute;n de Tarjetas", "Servicios > Tarjeta de Pagos Santander  > Asignaci&oacute;n Tarjeta-Empleado > Asignaci&oacute;n", "s37140h", request));//YHG NPRE
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());

				 		if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				 			return;
						}

						EIGlobal.mensajePorTrace("Entramos opcion 1 NomPreAltaTarjeta", NivelLog.INFO);

						//VSWF ESC se agrega la consulta de paises
						request.setAttribute("Paises", ObtenCatalogo("SELECT cve_pais,descripcion from comu_paises order by descripcion"));
						request.setAttribute("tarjeta", "");
						request.setAttribute("nom_empleado", "");
						request.setAttribute("rfc", "");
						request.setAttribute("homoclave", "");
						request.setAttribute("paterno", "");
						request.setAttribute("materno", "");
						request.setAttribute("num_id", "");
						request.setAttribute("num_empleado", "");
						request.setAttribute("nacimiento", "");
						request.setAttribute("correo", "");
						request.setAttribute("calle", "");
						request.setAttribute("numExt", "");
						request.setAttribute("numInt", "");
						request.setAttribute("postal", "");
						request.setAttribute("colonia", "");
						request.setAttribute("delegacion", "");
						request.setAttribute("ciudad", "");
						request.setAttribute("estado", "");
						request.setAttribute("lada", "");
						request.setAttribute("telefono", "");
						sess.removeAttribute("MensajeLogin01");
						sess.removeAttribute("bean");
						//sess.removeAttribute("tipoAlta");
						request.removeAttribute("TablaTotales");
						request.removeAttribute("LineaCantidad");
						sess.removeAttribute("nombreArchivo");
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						BitaTransacBean bt = new BitaTransacBean();
						bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_ENTRA);
						NomPreUtil.bitacoriza(request,null,null,true, bt);
						evalTemplate("/jsp/prepago/NomPreAltaTarjeta.jsp", request, response );

						EIGlobal.mensajePorTrace("NomPreAltaTarjeta.class>>Salimos de Opcion = 1", NivelLog.DEBUG);
					}else

					/**************************************************
					 *** Opcion 2 = Realizar Busqueda de Empleados  ***
					 **************************************************/
					if(opcion.equals("2"))
					{

						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute("Encabezado",CreaEncabezado("Alta de Relaci&oacute;n de Tarjetas", "Servicios >Tarjeta de Pagos Santander  > Asignaci&oacute;n Tarjeta-Empleado > Asignaci&oacute;n", "s37140h", request));//YHG NPRE

						EIGlobal.mensajePorTrace("Entramos en opcion = 2", NivelLog.INFO);

						if (valida == null)
						{
							EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
							boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
							if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal)
							{
								EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
								guardaParametrosEnSession(request);
								ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
							}
							else
							{
								valida = "1";
							}
						}
						if (valida != null && valida.equals("1"))
						{
						//boolean usaCuenta = false;
						int usaCuenta = 0;
						boolean altaExitosa = false;
						NomPreEmpleado bean = new NomPreEmpleado();

						bean.setNoEmpleado(getFormParameter(request, "num_empleado"));
						bean.setNombre(getFormParameter(request, "nom_empleado"));
						bean.setPaterno(getFormParameter(request, "paterno"));
						bean.setMaterno(getFormParameter(request, "materno"));
						bean.setRfc(getFormParameter(request, "rfc"));
						if(!(getFormParameter(request, "homoclave").equals("")));
							bean.setHomoclave(getFormParameter(request, "homoclave"));
						bean.setNumID(getFormParameter(request, "num_id"));//VSWF ESC nuevos campos NP
						String fecha = getFormParameter(request, "nacimiento");
						fecha = fecha.substring(6,fecha.length()) + "-" + fecha.substring(3,5) + "-" + fecha.substring(0,2);
						bean.setFechaNacimiento(fecha);
						bean.setSexo(getFormParameter(request, "sexo").substring(0,1));
						bean.setNacionalidad(getFormParameter(request, "nacionalidad").substring(0,getFormParameter(request, "nacionalidad").indexOf("=")));
						bean.setPais(getFormParameter(request, "vPais"));//VSWF ESC nuevos campos NP
						bean.setEstadoCivil(getFormParameter(request, "edo_civil").substring(0,1));
						if(!(getFormParameter(request, "correo").equals("")))
							bean.setCorreoElectronico(getFormParameter(request, "correo"));
						bean.setCalle(getFormParameter(request, "calle"));
						//Inicio VSWF ESC nuevos campos NP
						bean.setNumExt(getFormParameter(request, "numExt"));
						if(!(getFormParameter(request, "numInt").equals("")) && !(getFormParameter(request, "numInt").equals(null)))
							bean.setNumInt(getFormParameter(request, "numInt"));
						//Fin VSWF ESC nuevos campos NP
						bean.setColonia(getFormParameter(request, "colonia"));
						bean.setDelegacion(getFormParameter(request, "delegacion"));
						bean.setCiudad(getFormParameter(request, "ciudad"));
						bean.setEstado(getFormParameter(request, "estado"));
						bean.setCodigoPostal(getFormParameter(request, "postal"));
						if(!(getFormParameter(request, "lada").equals("")));
							bean.setLada(getFormParameter(request, "lada"));
						if(!(getFormParameter(request, "telefono").equals("")));
							bean.setTelefono(getFormParameter(request, "telefono"));
						//CatNominaBean bean = new CatNominaBean();
						//String numContrato = request.getParameter("numContrato");
						//CatNominaValidaInterb validaInterb = new CatNominaValidaInterb();
						//usaCuenta = validaInterb.validaCuentaInterb(bean.getNumContrato(), bean.getNumCuenta());
						NomPreLM1D lm1d = new NomPreLM1D();
						//NomPreTarjetaDAO daoTarjeta = new NomPreTarjetaDAO();
						lm1d = new NomPreBusquedaAction().obtenRelacionTarjetaEmpleado(session.getContractNumber(), getFormParameter(request, "tarjeta"), null, null);

						//EIGlobal.mensajePorTrace("Resultado de la transaccion lm1d :" + lm1d.getDetalle().get(0).getEmpleado().getNoEmpleado(), EIGlobal.NivelLog.INFO);
						if(sess.getAttribute("tipoAlta").toString().equals("AI"))
						{
							sess.setAttribute("tipoAlta","AL");
						}
						else
						{
							sess.setAttribute("tipoAlta","AI");
						}
						String codErr=null;
						if(lm1d!=null && lm1d.getCodigoOperacion()!=null){
							codErr=lm1d.getCodigoOperacion();
						}
						BitaTransacBean bt = new BitaTransacBean();
						bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_LINEA);
						NomPreUtil.bitacoriza(request,"LM1D", codErr, bt);

						//if(!usaCuenta)
						//validar con el objeto si regreso algo o no para saber si ya existe
						//if (usaCuenta == -100)
						//{
						//	generaPantallaError(request, response, "Fallo en el alta de registro Interbancario. Intente m&aacute;s Tarde.", "Alta en L&iacute;nea");
						//}
						//else
						//{	//if(usaCuenta == 0)
							if(lm1d!=null && lm1d.getDetalle()==null)
							{
								//CatNominaAction action = new CatNominaAction();

								/*****************************************
								 * Inicio Funcionalidad de TOKEN
								 * **************************************/

				                //interrumpe la transaccion para invocar la validacion para Alta de proveedor
				               /* if(  valida==null) {
				                    //EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccion esta parametrizada para solicitar la validacion con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

				                    boolean solVal=ValidaOTP.solicitaValidacion(
				                            session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

				                    if( session.getValidarToken() &&
				                            session.getFacultad(session.FAC_VAL_OTP) &&
				                            session.getToken().getStatus() == 1 &&
				                            solVal) {
				                        //EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicito la validacion. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
				                        //ValidaOTP.guardaParametrosEnSession(req);
				                        guardaParametrosEnSession(req);
				                        ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
				                    } else {
										ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
				                        valida="1";
									}
				                }
				                // retoma el flujo
				                if( valida!=null && valida.equals("1")) {*/
									NomPreEmpleadoDAO dao = new NomPreEmpleadoDAO();
									NomPreLM1E lm1e = new NomPreLM1E();
									String tarjeta = getFormParameter(request, "tarjeta");
									lm1e = dao.agregarEmpleado(numContrato, tarjeta, bean);
									EIGlobal.mensajePorTrace("Tarjeta: " +  tarjeta + " Resultado de la transaccion lm1e :" + lm1e.isCodExito() + "causa" + lm1e.getMensError(), NivelLog.INFO);
									//altaExitosa = dao.agregarEmpleado(numContrato, getFormParameter(request, "tarjeta"), bean);
									//altaExitosa = action.realizaAltaEmplInterb(bean);
									//ESTO VA SOLO QUE NECESITO SABER LA BASE DE DATOS PARA DESARROLLARLO
									//if(altaExitosa) {

									if(lm1e!=null && lm1e.isCodExito())
									{
										EIGlobal.mensajePorTrace("alta Exitosa", NivelLog.INFO);
										sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"Los datos han sido guardados <br>exitosamente\", 1);");


							    		String bgcolor = "textabdatobs";
										StringBuffer tablaTotales = new StringBuffer("");
										tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
										tablaTotales.append("<tr> ");
										tablaTotales.append("<td class=\"textabref\"></td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("</table>");
										tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
										tablaTotales.append("<tr>");
										tablaTotales.append("<td width=\"640\" class=\"tittabdat\" align=\"center\">Asignaci&oacute;n Tarjeta - Empleado</td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("<td class=\"");
										tablaTotales.append(bgcolor);
										tablaTotales.append("\" nowrap align=\"center\">Se ha registrado el alta correctamente ");
										//tablaTotales.append(folioArchivo);//se debe verificar que tipo de folio va aqui
										tablaTotales.append("&nbsp;</td>");
										tablaTotales.append("</table><br>");
										//tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");

										String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ " <td><A href = \" NomPreAltaTarjeta \" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table>";
										tablaTotales.append(botonestmp);
										request.setAttribute("TablaTotales", tablaTotales.toString());
										int referencia = 0;
										String codErrBit=" ";

										EnlcTarjetaBO boAlta;
										boAlta = new EnlcTarjetaBOImpl();
										TarjetaContrato tarjetaVinc;
										tarjetaVinc = new TarjetaContrato();
										tarjetaVinc.setEstatus("A");
										tarjetaVinc.setContrato(numContrato);
										tarjetaVinc.setTarjeta(tarjeta);
										tarjetaVinc.setUsuarioReg(bean.getNoEmpleado());

										try {
											ArrayList<TarjetaContrato> listTarj;
											listTarj = new ArrayList<TarjetaContrato>();
											listTarj.add(tarjetaVinc);
											boAlta.agregarTarjeta(listTarj, request);
										} catch (DaoException e) {
											EIGlobal.mensajePorTrace("No se pudo realizar la vinculaci�n de la tarjeta.", NivelLog.ERROR);
										}

										try
										{
											referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

											if(lm1d!=null && lm1d.getCodigoOperacion()!=null && !lm1d.getCodigoOperacion().trim().equals("")){
												codErrBit=lm1d.getCodigoOperacion();
											}
											String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID8()," ",0.0,NomPreUtil.ES_NP_TARJETA_ASIGNACION_LINEA_OPER," ",0," ");
										}catch(Exception e){
											EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}
										try {
											EmailSender emailSender = new EmailSender();
							    			EmailDetails beanEmailDetails = new EmailDetails();

							    			if (emailSender.enviaNotificacion(codErrBit)) {
								    			EIGlobal.mensajePorTrace("Notificacion en NomPreAltaTarjeta en linea", EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("Se debe enviar notificacion? " + emailSender.enviaNotificacion(codErrBit), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("Referencia ->" + referencia, EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("Tarjeta ->" + tarjeta, EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("Stauts ->" + codErrBit, EIGlobal.NivelLog.INFO);

								    			beanEmailDetails.setNumeroContrato(session.getContractNumber());
								    			beanEmailDetails.setRazonSocial(session.getNombreContrato());
								    			beanEmailDetails.setNumRef(String.valueOf(referencia));
								    			beanEmailDetails.setNumRegImportados(1);
								    			beanEmailDetails.setEstatusActual(codErrBit);


								    				emailSender.sendNotificacion(request, IEnlace.NOT_ASIGNACION_TARJETA, beanEmailDetails);

							    			}
										} catch (Exception e) {
						    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}
									}
									else {
										sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"Los datos ingresados en el alta<br>no han podido ser registrados <br>"+lm1e.getMensError()+"\", 1);");
										//evalTemplate("/jsp/prepago/NomPreAltaTarjetaResultado.jsp", request, response );
									}

							   //}
									/*****************************************
									 * Finaliza Funcionalidad de TOKEN
									 * **************************************/
							}
							else
							{
								EIGlobal.mensajePorTrace("Relacion Existente " + lm1d.getMensError(), NivelLog.INFO);
								sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"La cuenta bancaria ingresada <br>ya est&aacute; registrada en el cat&aacute;logo\", 1);");
							}
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							evalTemplate("/jsp/prepago/NomPreAltaTarjetaResultado.jsp", request, response );
						}//temina validacion token

							EIGlobal.mensajePorTrace("Salimos de Opcion = 1", NivelLog.DEBUG);
					}else

					/*******************************************************************************
					 *  Opcion = 6 - Muestra pantalla de Modificacion Masiva de datos personales de
					 *  			 Empleados con cuenta Interbanciaria que no tienen alguna cuenta
					 *  			 interna activa en el nuevo catalogo de nomina
					 *******************************************************************************/
					if (opcion.equals("6"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
												"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

						EIGlobal.mensajePorTrace("Entramos Opcion = 6", NivelLog.INFO);
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
						EIGlobal.mensajePorTrace("Salimos Opcion = 6", NivelLog.DEBUG);
					}else



					/******************************************************************
					 *  Opcion = 7 - Realiza la lectura del archivo con los registros
					 *  			que seran modificados con cuentas Interbancarias.
					 *  			Se realizaran las respectivas validaciones
					 *******************************************************************/
					if(opcion.equals("7"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
												"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

						EIGlobal.mensajePorTrace("Entramos Opcion = 7", NivelLog.INFO);
						String tabla = "";
						CatNominaBean bean = new CatNominaBean();
						try {
							CatNominaAction action = new CatNominaAction();
							CatNominaValidaInterb validaInterb = new CatNominaValidaInterb();
							getFileInBytes(request);
							EIGlobal.mensajePorTrace("Se carg&oacute; el archivo", NivelLog.INFO);
							String  nombreArchivo= (String)request.getAttribute("Archivo");//varible con el nombre del archivo que se esta mandando a llamar
							EIGlobal.mensajePorTrace("NombreArchivo: "+ nombreArchivo, NivelLog.INFO);

							if(nombreArchivo!=null)
							{
								ArrayList erroresArchivoModif;
								bean = validaInterb.validaArchModifInterb(Global.DOWNLOAD_PATH + nombreArchivo, numContrato, request);

								if(!bean.getFalloInesperado())
								{
									EIGlobal.mensajePorTrace("Fallo Inesperado", NivelLog.INFO);
									erroresArchivoModif = bean.getListaErrores();

									if(erroresArchivoModif.size() == 0)
									{
										listaModifInterb = bean.getListaRegistros();


										EIGlobal.mensajePorTrace("Tama�o de ArrayList " + listaModifInterb.size(), NivelLog.DEBUG);

										if (bean.getTotalRegistros() <= 10) {

											tabla = action.creaDetalleModifImport(numContrato, bean.getListaRegistros(), CatNominaConstantes._MODIF);
											request.setAttribute("TablaRegistros", tabla);

										}
										else {
											String lineaCant = "";
														lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
														lineaCant += "<tr><td>&nbsp;</td></tr>";
														lineaCant += "<tr>";
														lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
														lineaCant += "El total de registros a modificar son: " + bean.getTotalRegistros() + " <br><br>";
														lineaCant += "</td>";
														lineaCant += "</tr>";
														lineaCant += "</table>";


											//	 "<br><br> <td class='tittabdat' align=center bottom=\"middle\"> El total de registros a modificar son: "
											//						+ bean.getTotalRegistros() + "</td><br><br>";
											request.setAttribute("LineaCantidad", lineaCant);
										}
									}
									else
									{
										EIGlobal.mensajePorTrace("Erres archivo " + erroresArchivoModif.size(), NivelLog.INFO);
										String Linea = "";
										Iterator it = erroresArchivoModif.iterator();
										int  i = 0;
										EIGlobal.mensajePorTrace("Validando lineas", NivelLog.INFO);
										while (it.hasNext()) {
											if(i==0){
												it.next();
											}
											i++;

											if(erroresArchivoModif.get(0).equals("Long")){
												EIGlobal.mensajePorTrace("Datos", NivelLog.INFO);
												Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Longitud de L&iacute;nea Incorrecta. <br><br>"
												+ Linea + ".\", 1);");

											}
											else if(erroresArchivoModif.get(0).equals("Datos")){
												EIGlobal.mensajePorTrace("Datos", NivelLog.INFO);
												Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Tipo de dato incorrecto. <br><br>"
												+ Linea + ".\", 1);");

											}
											else if(erroresArchivoModif.get(0).equals("Cuenta")){
												EIGlobal.mensajePorTrace("Cuentas", NivelLog.INFO);
												Linea += "Error en l&iacute;nea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Cuenta en Existencia <br><br>"
												+ Linea + ".\", 1);");

											}
											else if(erroresArchivoModif.get(0).equals("Campo")){
												EIGlobal.mensajePorTrace("Campo", NivelLog.INFO);

												String str= null;
												str = (String)(it.next());
												EIGlobal.mensajePorTrace("------ ModifArchivoInterb ... variable str:" + str, NivelLog.DEBUG);

												int pos=str.indexOf('@');

												String campo = "", registro = "";

												campo = str.substring(0, pos);
												registro = str.substring(pos+1);

												Linea += "Campo Obligatorio: <b><FONT COLOR = RED> " + campo + "</font></b>" + " en l&iacute;nea: " + registro + "  <br>";
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> Hace falta campo obligatorio: <br><br>"
												+ Linea + ".\", 1);");

											}
											else if(erroresArchivoModif.get(0).equals("Cuenta Abono")){
												EIGlobal.mensajePorTrace("Cuenta Abono", NivelLog.INFO);
												Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> N&uacute;mero de cuenta menor a 18 d&iacute;gitos <br><br>"
												+ Linea + ".\", 1);");

											}
											else if(erroresArchivoModif.get(0).equals("Cuenta Interna")){
												EIGlobal.mensajePorTrace("Cuenta Abono", NivelLog.INFO);
												Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
												request.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> No se puede realizar Modificaci&oacute;n, cuenta asociada con otra cuenta Interna. <br><br>"
												+ Linea + ".\", 1);");

											}
										}

									}
							//		/* everis JARA - Ambientacion -  Redireccionando a jsp */
							//		evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
								}// Control de Excepciones
								else{
									generaPantallaError(request, response, "Fallo al momento de realizar validaciones contra la Base de Datos. <br> Favor de Intentar m&aacute;s tarde.", "Modificaci&oacute;n por Archivo");
								}
							}
						}
						catch(Exception e)
						{
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("PROBLEMA AL MODIFICAR POR ARCHIVO ", EIGlobal.NivelLog.ERROR);
						}
						if(!bean.getFalloInesperado()){
							/* everis JARA - Ambientacion -  Redireccionando a jsp */
							evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );
						}
						EIGlobal.mensajePorTrace("Fin Opcion 7", NivelLog.INFO);
					}


					/********************************************************************
					 *  Opcion = 8 - Realizar Modificaciones despues de haber sido		*
					 *  			validado y mostrado el detalle de la importacion	*
					 *  			de registros con modificaciones					  	*
					 *******************************************************************/
					else if (opcion.equals("8"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute ("Encabezado", CreaEncabezado ( "Modificaci&oacute;n por Archivo Cat&aacute;logo de N&oacute;mina", "Servicios > N&oacute;mina > " +
												"Empleados > Mantenimiento Interbancario > Modificaci&oacute;n por Archivo", "s37160h", request ) );

						EIGlobal.mensajePorTrace("Inicio Opcion = 8", NivelLog.INFO);
						//CargaArchivo cFile = new CargaArchivo();
						//cFile.cargaArchivo(request, "/tmp/", "ModifEmpleados.txt");
						CatNominaAction action = new CatNominaAction();
						boolean re;


						/*****************************************
						 * Inicio Funcionalidad de TOKEN
						 * **************************************/

			            //interrumpe la transaccion para invocar la validacion para Alta de proveedor
			            /*if(  valida==null) {
			                //EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccion esta parametrizada para solicitar la validacion con el OTP \n\n\n",EIGlobal.NivelLog.INFO);

			                boolean solVal=ValidaOTP.solicitaValidacion(
			                        session.getContractNumber(),IEnlace.NOMINA_INTERBANCARIA);

			                if( session.getValidarToken() &&
			                        session.getFacultad(session.FAC_VAL_OTP) &&
			                        session.getToken().getStatus() == 1 &&
			                        solVal) {
			                    //EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicito la validacion. \nSe guardan los parametros en sesion \n\n\n",EIGlobal.NivelLog.INFO);
			                    //ValidaOTP.guardaParametrosEnSession(req);
			                    guardaParametrosEnSession(req);
			                    ValidaOTP.validaOTP(req,res,Global.WEB_APPLICATION+IEnlace.VALIDA_OTP);
			                } else {
								ValidaOTP.guardaRegistroBitacora(req, "Token deshabilitado");
			                    valida="1";
							}
			            }
			            // retoma el flujo
			            if( valida!=null && valida.equals("1")){*/


							re = action.modificaDatosArchInterb(numContrato, usuario, listaModifInterb);
							if (re) {
								request.setAttribute("MensajeLogin01"," cuadroDialogo(\"La modificaci&oacute;n se realiz&oacute; <br> exitosamente.\", 1);");

								evalTemplate("/jsp/CatNominaModifArchivoInterb.jsp", request, response );

							}else{
								generaPantallaError(request, response, "Fallo en la realizaci&oacute;n de la operaci&oacute;n con la Base de Datos. Intente m&aacute;s Tarde", "Modificaci&oacute;n por Archivo");
							}


							EIGlobal.mensajePorTrace("Fin Opcion 8", NivelLog.INFO);

						//}
							/*****************************************
							 * Finaliza Funcionalidad de TOKEN
							 * **************************************/

					}else




					//		ALTA ARCHIVO INTERBANCARIA*******************

					/******************************************************************
					 *  Opcion = 12 - Muestra pantalla de Alta por Archivo Interbancaria
					 *******************************************************************/
					if (opcion.equals("12"))
					{
						try
						{

						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());
						request.setAttribute ("Encabezado", CreaEncabezado ( "Alta por Archivo Tarjeta de Pagos Santander ", "Servicios > Tarjeta de Pagos Santander > Asignaci&oacute;n Tarjeta - Empleado > Asignaci&oacute;n", "s37150h", request));//YHG NPRE
						EIGlobal.mensajePorTrace("Entramos Opcion = 12", NivelLog.INFO);
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/prepago/NomPreAltaTarjeta.jsp", request, response );
						EIGlobal.mensajePorTrace("Salimos Opcion = 12", NivelLog.INFO);
				 		}catch(Exception e){
			 				EIGlobal.mensajePorTrace("Problema al enviar el archivo " , EIGlobal.NivelLog.INFO);
				 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							sess.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, La operaci&oacute;n no se realiz&oacute;.\", 3);");
							generaPantallaError(request, response, "Fallo en el alta de Registros en la Base de Datos. Favor de Intentar m&aacute;s Tarde", "Alta por Archivo");

			 		}

					}else



					/******************************************************************
					 *  Opcion = 13 - Importacion y validaciones de archivo para
					 *  			generar el alta por archivo Interbancaria
					 *******************************************************************/
					if (opcion.equals("13"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute ("Encabezado", CreaEncabezado ( "Alta por Archivo Tarjeta de Pagos Santander ", "Servicios > Tarjeta de Pagos Santander > Asignaci&oacute;n Tarjeta - Empleado  > Asignaci&oacute;n", "s37150h", request));//YHG NPRE
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());
				 		EIGlobal.mensajePorTrace("Entramos Opcion = 13 por archivo de NomPreAltaTarjeta", NivelLog.INFO);
						String tabla = "";
						String nombreOriginal = "";

						try
						{
							//CatNominaAction action = new CatNominaAction();
							ValidaArchivoAltas validaInterb = new ValidaArchivoAltas();

							NomPreAlta bean = new NomPreAlta();
							getFileInBytes(request);

							EIGlobal.mensajePorTrace("Se carg&oacute; el archivo", NivelLog.INFO);
							String  nombreArchivo= (String)request.getAttribute("Archivo");				//varible con el nombre del archivo que se esta mandando a llamar
							EIGlobal.mensajePorTrace("NombreArchivo: "+ nombreArchivo, NivelLog.INFO);

							String nombreArchivoAux = nombreArchivo;

					        String originalName = "";
					        String sFile = nombreArchivo;//sess.getAttribute("nombreArchivo").toString();				//varible con el nombre del archivo que se esta mandando a llamar

					        nombreOriginal  = nombreArchivo;//sess.getAttribute("nombreArchivo").toString();				//varible con el nombre del archivo que se esta mandando a llamar
				            nombreOriginal = nombreOriginal.trim();
					        //sFile = (String)request.getAttribute("Archivo");

					        nombreOriginal = nombreOriginal.substring( nombreOriginal.lastIndexOf( '\\' ) + 1 );
					        originalName   = nombreOriginal;
							EIGlobal.mensajePorTrace("***NomImportNomina.class & nombreOriginalantes>"+nombreOriginal+ "<>", EIGlobal.NivelLog.INFO);
							nombreOriginal= nombreOriginal.substring(0,nombreOriginal.length()-4);

							request.getSession().setAttribute("Recarga_Ses_Multiples", "TRUE");
							EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago Creado.", EIGlobal.NivelLog.DEBUG);
							EIGlobal.mensajePorTrace("Original" + originalName, NivelLog.INFO);
					        //String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
							Calendar cal = Calendar.getInstance();
							nombreOriginal = session.getContractNumber() + cal.getTime().getTime();
							nombreArchivo  = "" + IEnlace.DOWNLOAD_PATH + nombreOriginal;
							String nombreArchivoMod = "" + IEnlace.DOWNLOAD_PATH + "Mod" + nombreOriginal;
							sess.setAttribute("nombreOriginal", nombreOriginal);
							if(nombreArchivoAux!=null)
							{
								ArrayList erroresArchivo;
								bean = validaInterb.validaArchivoAlta(Global.DOWNLOAD_PATH + nombreArchivoAux, numContrato, request);

								try{
							        EIGlobal.mensajePorTrace("***NomPreAltaTarjeta.class & Si el archivo existe, se renombra.", EIGlobal.NivelLog.INFO);
								// Renombra el archivo descargado
							    File archivo = new File( IEnlace.LOCAL_TMP_DIR +"/"+ sFile );
							    boolean renombrado = FileUtilities.copiarArchivo(IEnlace.LOCAL_TMP_DIR +"/"+ sFile , nombreArchivoMod);
								if (renombrado)
								 {
									archivo.delete();
								   EIGlobal.mensajePorTrace("***NomPreAltaTarjeta.class & Se renombra a: "+nombreArchivoMod, EIGlobal.NivelLog.INFO);
								   //archivo.renameTo(new File(nombreArchivoMod));
								  }
								else
								 {
								   EIGlobal.mensajePorTrace("***NomPreAltaTarjeta.class & El archivo no existe.", EIGlobal.NivelLog.INFO);
								   // throw file not found exception
								   throw new FileNotFoundException();
								 }
							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("***NomPreAltaTarjeta.class Ha ocurrido un problema en: %ejecutaCargaArchivo()"+e, EIGlobal.NivelLog.INFO);
							}

								erroresArchivo = bean.getListaErrores();
								EIGlobal.mensajePorTrace("Errores :" + erroresArchivo.toString(), NivelLog.DEBUG);

								if(sess.getAttribute("tipoAlta").toString().equals("AI"))
								{
									sess.setAttribute("tipoAlta","AL");
								}
								else
								{
									sess.setAttribute("tipoAlta","AI");
								}

								if(erroresArchivo.size() == 0)
								{
									listaAltaFinal = bean.getListaRegistros();

										String lineaCant = "";
													lineaCant += "<table width=760 border=0 cellspacing=0 cellpadding=0 height=40>";
													lineaCant += "<tr><td>&nbsp;</td></tr>";
													lineaCant += "<tr>";
													lineaCant += "<td class=texfootpagneg align=center bottom=\"middle\">";
													lineaCant += "El total de registros a dar de alta son: " + bean.getTotalRegistros() + " <br><br>";
													lineaCant += "</td>";
													lineaCant += "</tr>";
													lineaCant += "</table>";

										EIGlobal.mensajePorTrace("LineaCantidad : " + lineaCant, NivelLog.DEBUG);
										request.setAttribute("LineaCantidad", lineaCant);

										System.out.println("A>> Se establece linea cantidad " + (lineaCant == null ? "null" : lineaCant.length()));
										sess.removeAttribute("VALIDARALTA");
										sess.setAttribute("LineaCantidadSesDup", lineaCant);

										sess.setAttribute("bean", bean);
										sess.setAttribute("nombreArchivo", nombreArchivo);
										EIGlobal.mensajePorTrace("MODIFICACION NUEVOS CAMPOS AMX-10-00323-4A : " + lineaCant, NivelLog.DEBUG);
										this.modificaArchivo(request);
								}
								else
								{
									String Linea = "";
									Iterator it = erroresArchivo.iterator();


									int i = 0;
									while (it.hasNext()) {
										if(i==0){
											it.next();
										}
										i++;
										if(erroresArchivo.get(0).equals("Long")){
											EIGlobal.mensajePorTrace("Longitud", NivelLog.DEBUG);
											Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Longitud de L&iacute;nea Incorrecta. <br>"
											+ Linea + ".\", 1);");
										}
										// Codifo Bancario Santander
										else if(erroresArchivo.get(0).equals("CodBancario")){
											EIGlobal.mensajePorTrace("CodBancario", NivelLog.DEBUG);
											Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br> C&oacute;digo Bancario incorrecto . <br>"
											+ Linea + ".\", 1);");
										}
										else if(erroresArchivo.get(0).equals("Datos")){
											EIGlobal.mensajePorTrace("Datos", NivelLog.DEBUG);
											Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Tipo de dato incorrecto. <br>"
											+ Linea + ".\", 1);");
										}
										else if(erroresArchivo.get(0).equals("Cuentas")){
											EIGlobal.mensajePorTrace("Cuentas", NivelLog.DEBUG);
											Linea += "Error en linea: <b><FONT COLOR=RED> " + (it.next() + "</font></b><br>");
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Cuenta en Existencia. <br>"
											+ Linea + ".\", 1);");
										}
										else if(erroresArchivo.get(0).equals("Campo")){
											EIGlobal.mensajePorTrace("Campo", NivelLog.DEBUG);

											String str= null;
											str = (String)(it.next());
											EIGlobal.mensajePorTrace("------ AltaArchivoInterb ... variable str:" + str, NivelLog.DEBUG);

											int pos=str.indexOf('@');

											String campo = "", registro = "";

											campo = str.substring(0, pos);
											registro = str.substring(pos+1);

											Linea += "Campo Obligatorio: <b><FONT COLOR = RED> " + campo + "</font></b>" + " en l&iacute;nea: " + registro + "  <br>";
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  Hace falta campo obligatorio. <br>"
											+ Linea + ".\", 1);");

										}
										else if(erroresArchivo.get(0).equals("Cuenta Abono")){
											EIGlobal.mensajePorTrace("Cuenta Abono", NivelLog.DEBUG);
											EIGlobal.mensajePorTrace("Valor de i" + i, NivelLog.DEBUG);
											Linea += "Error en l&iacute;nea: <b><FONT COLOR = RED> " + (it.next()+ "</font></b><br>");
											sess.setAttribute("MensajeLogin01", " cuadroDialogoCatNomina(\"Se presentaron los siguientes errores: <br>  N&uacute;mero de cuenta menor a 18 d&iacute;gitos. <br>"+ Linea + ".\", 1);");
										}
									}
								}
							}
						}
						catch(Exception e)
						{
							EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                            EIGlobal.mensajePorTrace("Problema en Carga Archivo Nompre ", NivelLog.INFO);
						}
						BitaTransacBean bt = new BitaTransacBean();
						bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_IMPORTAR);
						if(nombreOriginal!=null)
							bt.setNombreArchivo(nombreOriginal);
						NomPreUtil.bitacoriza(request,null, null, bt);

						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						evalTemplate("/jsp/prepago/NomPreAltaTarjetaResultado.jsp", request, response );
						EIGlobal.mensajePorTrace("Fin Opcion 13", NivelLog.INFO);
					}

					/********************************************************************
					 *  Opcion = 14 - Realizar el Alta despues de haber sido		*
					 *  			validado y mostrado el detalle de la importacion	*
					 *******************************************************************/
					else if (opcion.equals("14"))
					{
						EIGlobal.mensajePorTrace("Entramos a opcion :" + opcion, NivelLog.INFO);
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute("Encabezado",CreaEncabezado("Alta de Relaci&oacute;n de Tarjetas", "Servicios > Tarjeta de Pagos Santander  > Asignaci&oacute;n Tarjeta-Empleado > Asignaci&oacute;n", "s37140h", request));//YHG NPRE
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());

				 		try{

				 		NomPreAlta bean = (NomPreAlta)sess.getAttribute("bean");

				 		System.out.println("A>> Validando duplicidad");

				 		int re = 0;

				 		if (sess.getAttribute("VALIDARALTA") == null) {

				 			re = creaTramaAltaMasiva(request, response, bean);

				 			if (re == 3) {

				 				System.out.println("A>> Se encontro archivo duplicado");

				 				sess.setAttribute("MensajeLogin01","cuadroDialogoMttoEmp(\"<B>Archivo Duplicado</B>\",\"El archivo ya existe. �Desea transmitirlo nuevamente?.\", 9);");

				 				String linea = (String) sess.getAttribute("LineaCantidadSesDup");

				 				System.out.println("A>> Linea " + (linea == null ? "null" : linea.length()));

				 				request.setAttribute("LineaCantidad", sess.getAttribute("LineaCantidadSesDup"));

				 				evalTemplate("/jsp/prepago/NomPreAltaTarjetaResultado.jsp", request, response );
				 				return;
				 			}
				 		}

				 		System.out.println("A>> Se validara token");

				 		if (valida == null)
						{
							EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
							boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
							if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal)
							{
								EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
								guardaParametrosEnSessionIE(request);
								ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
							}
							else
							{
								valida = "1";
							}
						}
				 		if (valida != null && valida.equals("1"))
							{


							//CatNominaAction action = new CatNominaAction();
							//ValidaArchivoAltas validaInterb = new ValidaArchivoAltas();


								//re = validaInterb.altaDatosArchivo(numContrato, usuario, descContrato, listaAltaFinal);

				 					System.out.println("A>> Se realiza el envio del archivo");

									re = creaTramaAltaMasiva(request, response, bean);

									if(re == 2)
									{
										System.out.println("A>> Archivo dado de alta correctamente");

										sess.setAttribute("MensajeLogin01"," cuadroDialogo(\"El Alta de registros se realiz&oacute; <br> exitosamente.\", 1);");



										/* everis JARA - Ambientacion -  Redireccionando a jsp */
										//evalTemplate("/jsp/CatNominaAltaArchivoInterb.jsp", request, response );
										String bgcolor = "textabdatobs";
										StringBuffer tablaTotales = new StringBuffer("");
										tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
										tablaTotales.append("<tr> ");
										tablaTotales.append("<td class=\"textabref\"></td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("</table>");
										tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
										tablaTotales.append("<tr>");
										tablaTotales.append("<td width=\"640\" class=\"tittabdat\" align=\"center\">Asignaci&oacute;n Tarjeta - Empleado</td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("<tr>");
										tablaTotales.append("<td class=\"");
										tablaTotales.append(bgcolor);
										tablaTotales.append("\" nowrap align=\"center\">Se ha registrado el alta de los registros correctamente </td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("<tr>");
										tablaTotales.append("<td class=\"");
										tablaTotales.append(bgcolor);
										tablaTotales.append("\" nowrap align=\"center\">El N&uacute;mero de Secuencia Correspondiente a la operaci&oacute;n es: " + session.getFolioArchivo());//se debe verificar que tipo de folio va aqui
										tablaTotales.append("&nbsp;</td>");
										tablaTotales.append("</tr>");
										tablaTotales.append("</table><br>");
										//tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");

										String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ " <td><A href = \" NomPreAltaTarjeta \" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table>";
										tablaTotales.append(botonestmp);
										request.setAttribute("TablaTotales", tablaTotales.toString());
											try {
											EmailSender emailSender = new EmailSender();
							    			EmailDetails beanEmailDetails = new EmailDetails();

							    				EIGlobal.mensajePorTrace("Notificacion en NomPreAltaTarjeta por archivo", EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("No. de Tarjetas Asignadas ->" + bean.getTotalRegistros(), EIGlobal.NivelLog.INFO);
								    			EIGlobal.mensajePorTrace("Referencia ->" + session.getFolioArchivo(), EIGlobal.NivelLog.INFO);

								    			beanEmailDetails.setNumeroContrato(session.getContractNumber());
								    			beanEmailDetails.setRazonSocial(session.getNombreContrato());
								    			beanEmailDetails.setNumRef(session.getFolioArchivo());
								    			beanEmailDetails.setNumRegImportados(bean.getTotalRegistros());
								    			beanEmailDetails.setEstatusActual("OKOK0000");//Cualquier codigo de error de existo

								    			emailSender.sendNotificacion(request, IEnlace.NOT_ASIGNACION_TARJETA, beanEmailDetails);


										} catch (Exception e) {
						    				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
										}

										evalTemplate("/jsp/prepago/NomPreAltaTarjetaResultado.jsp", request, response );

								}else{

									sess.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, La operaci&oacute;n no se realiz&oacute;.\", 3);");
									generaPantallaError(request, response, "Fallo en el alta de Registros en la Base de Datos. Favor de Intentar m&aacute;s Tarde", "Alta por Archivo");
								}


								EIGlobal.mensajePorTrace("Fin Opcion 14", NivelLog.INFO);
		           			//}
								/*****************************************
								 * Finaliza Funcionalidad de TOKEN
								 * **************************************/
					  }//Fin valida token
				 		}catch(Exception e){
				 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace("Problema al enviar el archivo ", EIGlobal.NivelLog.INFO);
								sess.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, La operaci&oacute;n no se realiz&oacute;.\", 3);");
								generaPantallaError(request, response, "Fallo en el alta de Registros en la Base de Datos. Favor de Intentar m&aacute;s Tarde", "Alta por Archivo");

				 		}
					}
					/********************************************************************
					 *  Opcion = 15 - Realizar la consulta de codigo postal	obteniendo *
					 *  			campo de colonias, delegacion, ciudad y estado	*
					 *******************************************************************/
					else if(opcion.equals("15"))
					{
						/* everis JARA - Ambientacion -  Crea encabezado con link de ayuda a pagina HTML (por Definir) */
						request.setAttribute("Encabezado",CreaEncabezado("Alta de Relaci&oacute;n de Tarjetas", "Servicios >Tarjeta de Pagos Santander  > Asignaci&oacute;n Tarjeta-Empleado > Asignaci&oacute;n", "s37140h", request));//YHG NPRE
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
				 		request.setAttribute("MenuPrincipal", session.getStrMenu());
				 		String codPostal = getFormParameter(request,"postal");
				 		String colonias = "";
				 		String delegacion = "";
				 		String ciudad = "";
				 		String estado = "";

						EIGlobal.mensajePorTrace("Modificacion de codigo postal NomPreAltaTarjeta 15", NivelLog.INFO);
						System.out.println("Modificacion de codigo postal ejecuta"  );


				 		if (!NomPreUtil.validaServicioNominaRedireccion(request, response)) {
				 			return;
						}

				 		sess.removeAttribute("MensajeLogin01");

						EIGlobal.mensajePorTrace("Entramos opcion 15 NomPreAltaTarjeta", NivelLog.INFO);

						request.setAttribute("Paises", ObtenCatalogo("SELECT cve_pais,descripcion from comu_paises order by descripcion"));

						NomPreEmpleadoDAO empleadoDAO=new NomPreEmpleadoDAO();
						NomPreCodPost npcodpostal = empleadoDAO.consultarCodigoPostal(codPostal);
						if(npcodpostal!=null && npcodpostal.getDetalle() != null && npcodpostal.isCodExito()){

							EIGlobal.mensajePorTrace("Existe codigo postal " + codPostal , NivelLog.INFO);
							ListIterator li = npcodpostal.getDetalle().listIterator();

							while (li.hasNext()){
								NomPreCodPost npCodPost = (NomPreCodPost) li.next();
								colonias += "<option CLASS='tabmovtex'>" + npCodPost.getColonia() + "</option> \n";
								delegacion = npCodPost.getDelegacion();
								ciudad = npCodPost.getCiudad();
								estado = npCodPost.getEstado();
							}
						}	else {
							EIGlobal.mensajePorTrace("No Existe Codigo Posta codigo postal " + codPostal , NivelLog.INFO);
							sess.setAttribute("MensajeLogin01", "cuadroDialogo(\"No existe c&oacute;digo postal: " + codPostal + "\", 1)");

							if (getFormParameter(request,"dirColoniaEmp") != null){
								colonias += "<option CLASS='tabmovtex'>" + getFormParameter(request,"dirColoniaEmp") + "</option> \n";
							}
							else {
								colonias = "";
							}
							if (getFormParameter(request,"dirDelegacionEmp") != null){
								delegacion = (String)request.getParameter("dirDelegacionEmp");
							}else {
								delegacion = "";
							}
							if (getFormParameter(request,"dirCiudadEmp") != null){
								ciudad = (String)request.getParameter("dirCiudadEmp");
							}
							else {
								ciudad = "";
							}
							if (getFormParameter(request,"dirEstadoEmp") != null){
								estado = (String)request.getParameter("dirEstadoEmp");
							}
							else {
								estado = "";
							}
						}

						request.setAttribute("tarjeta", getFormParameter(request,"tarjeta"));
						request.setAttribute("nom_empleado", getFormParameter(request,"nom_empleado"));
						request.setAttribute("rfc", getFormParameter(request,"rfc"));
						request.setAttribute("homoclave", getFormParameter(request,"homoclave"));
						request.setAttribute("paterno", getFormParameter(request,"paterno"));
						request.setAttribute("materno", getFormParameter(request,"materno"));
						request.setAttribute("num_id", getFormParameter(request,"num_id"));
						request.setAttribute("num_empleado", getFormParameter(request,"num_empleado"));
						request.setAttribute("nacimiento", getFormParameter(request,"nacimiento"));
						request.setAttribute("correo", getFormParameter(request,"correo"));
						request.setAttribute("calle", getFormParameter(request,"calle"));
						request.setAttribute("numExt",getFormParameter(request,"numExt"));
						request.setAttribute("numInt", getFormParameter(request,"numInt"));
						request.setAttribute("lada", getFormParameter(request,"lada"));
						request.setAttribute("telefono", getFormParameter(request,"telefono"));
						request.setAttribute("postal", codPostal);
						request.setAttribute("colonia", colonias);
						request.setAttribute("delegacion", ciudad);
						request.setAttribute("ciudad", delegacion);
						request.setAttribute("estado", estado);
						sess.setAttribute("tipoAlta","AL");

						sess.removeAttribute("bean");
						//sess.removeAttribute("tipoAlta");
						request.removeAttribute("TablaTotales");
						request.removeAttribute("LineaCantidad");
						sess.removeAttribute("nombreArchivo");
						/* everis JARA - Ambientacion -  Redireccionando a jsp */
						BitaTransacBean bt = new BitaTransacBean();
						bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_ENTRA);
						NomPreUtil.bitacoriza(request,null,null,true, bt);
						evalTemplate("/jsp/prepago/NomPreAltaTarjeta.jsp", request, response );

						EIGlobal.mensajePorTrace("NomPreAltaTarjeta.class>>Salimos de Opcion = 1", NivelLog.DEBUG);
					}

					/* everis JARA - Ambientacion - En caso de error*/
					else{
						despliegaPaginaError("Error inesperado.","B&uacute;squeda de Empleados Cat&aacute;logo de N&oacute;mina",
						"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Cat&aacute;logo de N&oacute;mina &gt; B&uacute;squeda",
						request, response );
						}

					}//fin validaPeticion
				}/* everis JARA - Ambientacion - CERRANDO EL IF*/

			}/* everis JARA - Ambientacion - En caso de error GENERAL*/
			catch(Exception e){
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("\n Se presento un problema Inesperado en Tarjeta de Pagos Santander: \n ", NivelLog.INFO);


		  		despliegaPaginaError("Error inesperado.","Tarjeta de Pagos Santander ",//YHG NPRE
						"Servicios &gt; N&oacute;mina",
						request, response );
		  		}
		}



		/*******************************************************************/
		/** Metodo: getFileInBytes para realizar importaciones de archivos.
		 * @return byte[]
		 * @param HttpServletRequest -> request
		/********************************************************************/
		public byte[] getFileInBytes ( HttpServletRequest request) throws IOException, SecurityException
		{
			String sFile = "";
			//MultipartRequest mprFile = new MultipartRequest ( request, rutadeSubida, 2097152 );

			FileInputStream entrada = null;

			sFile = getFormParameter(request, "fileName");
		    //sFile = mprFile.getFilesystemName ( name );
			EIGlobal.mensajePorTrace("[GetFileInBytes] - S1 : " + sFile, NivelLog.DEBUG);
		    request.setAttribute("Archivo", sFile);


			 File fArchivo = new File((rutadeSubida + "/" + sFile).intern());
		    //File fArchivo = new File ( ( "C:/Documents and Settings/srobles/Desktop/" + sFile ).intern () );
		    byte[] aBArchivo = new byte[ (int)fArchivo.length() ];

		    if(fArchivo.exists()){

		    	 try{
		    	entrada = new FileInputStream(fArchivo);
				 byte bByte = 0;

				 for (int i = 0; ((bByte = (byte)entrada.read())!=-1); i++ ) {
					 aBArchivo[i] = bByte;
				 }
		    	 } catch(Exception e) {
						 //e.printStackTrace();
		    		     request.removeAttribute("Archivo");
		    		     EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                                     throw new IOException();
				 } finally {
					if(entrada != null)
						entrada.close ();
					     //fArchivo.delete ();
				 }
		    }else{
		    	request.removeAttribute("Archivo");
		    }

			 //Enumeration e = mprFile.getParameterNames();
			 HashMap parametros = getFormParameters(request);
			 Iterator e = parametros.entrySet().iterator();

			 while(e.hasNext()) {
				 String parametro = (String) ((Map.Entry) e.next()).getKey();
				 request.setAttribute(parametro, parametros.get(parametro));
			 }
			 EIGlobal.mensajePorTrace("[getFileInBytes] - Fin ", NivelLog.INFO);
			 return aBArchivo;
		}







			 /********************************************/
			/** Metodo: consultaInterbCN
			 * Bitacorizacion para Consulta de registro
			 */
			/********************************************/


			public void consultaInterbCN(String usuario, String contrato, HttpServletRequest request)
			throws ServletException, IOException{

				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");

				EIGlobal.mensajePorTrace("La operaci&oacute;n es una consulta Interbancaria", NivelLog.INFO);

			}




			/********************************************/
			/** Metodo: modificaInterbCN
			 * Bitacorizacion para Modificacion de registro
			 */
			/********************************************/


			public void modificaInterbCN(String usuario, String contrato, HttpServletRequest request)
				throws ServletException, IOException{

				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");

				EIGlobal.mensajePorTrace("La operaci&oacute;n es una modificacion Interbancaria", NivelLog.INFO);




			}

			public void generaPantallaError(HttpServletRequest request, HttpServletResponse response, String mensaje, String funcion) throws Exception
			{
				EIGlobal.mensajePorTrace("[generaPantallaError] - Inicio", NivelLog.INFO);
				despliegaPaginaError(mensaje,
				"Cat&aacute;logo de N&oacute;mina",
				"Servicios &gt; N&oacute;mina &gt; Empleados &gt; Mantenimineto Interbancario &gt; " + funcion,
				request, response );
				//request.setAttribute("MensajeLogin01"," cuadroDialogo(\"Se genero un error, La busqueda no pudo ser realizada. Intente mas Tarde;.\", 3);");
				EIGlobal.mensajePorTrace("[generaPantallaError] - Fin",  NivelLog.INFO);
			}

			public int creaTramaAltaMasiva(HttpServletRequest request, HttpServletResponse response, NomPreAlta bean) throws ParseException,Exception
			{
				HttpSession sess = request.getSession();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				String contrato = session.getContractNumber();
				String empleado = session.getUserID8();
				String perfil = session.getUserProfile();
				String totalRegNom = String.valueOf(bean.getTotalRegistros());
				String digit = (request.getParameter("statusDuplicado") == null ? "1" : request.getParameter("statusDuplicado"));

				//java.util.Date Hoy = new java.util.Date();
				String fechaAplicacion = ObtenDia()+ObtenMes()+ObtenAnio();

				String servicio = "NPVA";
		        String folio = "";
		        String tramaEntrada = "";

				String validar = (String) sess.getAttribute("VALIDARALTA");
		        folio = (String) sess.getAttribute("FOLIO");
		        String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
				//Calendar cal = Calendar.getInstance();
				String nombreOriginal = sess.getAttribute("nombreOriginal").toString();//session.getContractNumber() + cal.getTime().getTime();
				String nombreArchivo  = "" + IEnlace.DOWNLOAD_PATH + nombreOriginal;
				 EIGlobal.mensajePorTrace("Este sube :" +nombreOriginal,  NivelLog.INFO);
			nombreArchivo = nombreArchivo + "/";
			nombreArchivo = nombreArchivo.substring(posCar(nombreArchivo, '/', 4) + 1, posCar(nombreArchivo, '/', 5));
			String rutaArch = nueva_ruta + "/" + nombreArchivo;
			EI_Exportar arcTmp = new EI_Exportar(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses");
			if (arcTmp.creaArchivo())
				EIGlobal.mensajePorTrace(textoLog + "Archivo .ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);

				try
				{
					String tipoCopia = "3";
					ArchivoRemoto archR = new ArchivoRemoto();
					if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
						EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
				} catch (Exception ioeSua) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
				}

		        if (folio == null || folio.equals(""))
		        {
		         sess.setAttribute("FOLIO", getFormParameter(request, "folio"));
		         folio = (String) sess.getAttribute("FOLIO");
		         EIGlobal.mensajePorTrace(textoLog + "El folio esta vacio", EIGlobal.NivelLog.DEBUG);
		        }

		        if (validar == null)
		         validar = "";
		        if (validar.compareTo("N") == 0)
		        {
		         servicio = "ATIM";
		        }

		        tramaEntrada = "1EWEB|" + empleado + "|" + servicio
		        + "|" + contrato + "|" + empleado + "|" + perfil
		        + "|" + contrato + "@" + fechaAplicacion + "@"
		        + totalRegNom + "@";

		        if("ATIM".equals(servicio)) {
		        	tramaEntrada += rutaArch + "@";
		        }

		        tramaEntrada += digit + "@| | | |";

		        System.out.println("A>> servicio = [" + servicio + "]");
				System.out.println("A>> valida = [" + validar + "]");
				System.out.println("A>> entrada = [" + tramaEntrada + "]");

		        EIGlobal.mensajePorTrace(textoLog + "Trama que se enviara >>" + tramaEntrada + "<<", EIGlobal.NivelLog.ERROR);
		        String tramaSalida = "";
		        String folioArchivo = "";

		        if (servicio.equals("ATIM"))
		        {
		         EIGlobal.mensajePorTrace("Crea folio",  NivelLog.INFO);
		         folioArchivo = generaFolio(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), servicio, totalRegNom);
		         session.setFolioArchivo(folioArchivo);
		         request.setAttribute("folioArchivo", folioArchivo);
		         EIGlobal.mensajePorTrace(textoLog + "folioArchivo >>" + folioArchivo + "<<", EIGlobal.NivelLog.ERROR);
		         // Envio de Datos ------------------------------
		         String mensajeErrorEnviar = envioDatos(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), folioArchivo, servicio, rutaArch, tramaEntrada);
		         EIGlobal.mensajePorTrace(textoLog + "mensajeErrorEnviar>>" + mensajeErrorEnviar + "<<", EIGlobal.NivelLog.ERROR);
		         // Servicio dispersor de transacciones ---------
		         if (mensajeErrorEnviar.equals("OK"))
		         {
		          tramaSalida = realizaDispersion(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), folioArchivo, servicio);
		         }
		        }
		        else
		        {
		         tramaSalida = ejecutaServicio(tramaEntrada);
		        }

		        System.out.println("A>> tramasalida = [" + tramaSalida + "]");

		        EIGlobal.mensajePorTrace("Trama de Entrada :" + tramaEntrada, NivelLog.DEBUG);
		        EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalida, NivelLog.DEBUG);
		        EIGlobal.mensajePorTrace(textoLog + "Trama salida>>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
		        String estatus_envio = "";
		        String fechaAplicacionFormato = "";
		        fechaAplicacionFormato = fechaAplicacion.substring( 0, 2) + "/" + fechaAplicacion.substring(2, 4) + "/" + fechaAplicacion.substring(4, 8);

		        if (!tramaSalida.startsWith("MANC"))
		        {
		         try
		         {
		          estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
		         } catch (Exception e) {
		        	 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		          estatus_envio = "";
		         }
		        }
		        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		        BitaTransacBean bt = new BitaTransacBean();
				bt.setNumBit(BitaConstants.ES_NP_TARJETA_ASIGNACION_IMPORTAR);
				if(nombreOriginal!=null)
					bt.setNombreArchivo(nombreOriginal);
				if(fechaAplicacionFormato!=null)
					bt.setFechaAplicacion(sdf.parse(fechaAplicacionFormato));

				NomPreUtil.bitacoriza(request,servicio, estatus_envio, bt);

				System.out.println("A>> estatus_envio = [" + estatus_envio + "]");

		        if (estatus_envio.equals("NOMI0000"))
		        {
		        	nombreArchivo = tramaSalida.substring(tramaSalida.lastIndexOf("/") + 1, tramaSalida.lastIndexOf("@"));
					try {
						String tipoCopia = "2"; // de /t/tuxedo... a /tmp
						ArchivoRemoto archR = new ArchivoRemoto();
						if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
							EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
					} catch (Exception ioeSua) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
					}
		         if (servicio.equals("NPVA"))
		         {
		          sess.setAttribute("VALIDARALTA", "N"); // Setear la variable global para que no se continue con la validacion.
		          request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
		          EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO.", EIGlobal.NivelLog.DEBUG);
		          /*System.out.println("A>> antes de llamar a creaTramaAltaMasiva");
		          int result = creaTramaAltaMasiva(request, response, bean); // Ejecutar nuevamente la operacion de envio para procesar el archivo.
		          System.out.println("A>> despues de llamar a creaTramaAltaMasiva " + result);
		          return result;*/
		         }
		         return 1;
		        }
		        if (estatus_envio.equals("NOMI0050")) {
	        		return 3;
				}
		        if (tramaSalida.equals("OK"))
		        {
		        	String arch_exportar = "";
					arch_exportar = session.getUserID8() + "Nom.doc";
					sess.removeAttribute("VALIDARALTA");
					try {
						String tipoCopia = "2"; // de /t/tuxedo... a /tmp
						ArchivoRemoto archR = new ArchivoRemoto();
						//copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
						//JGAL - Copia a ruta de download
						if (!archR.copiaOtroPathRemoto(nombreArchivo, Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportar, nueva_ruta))
							EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
					} catch (Exception ioeSua) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(ioeSua), EIGlobal.NivelLog.INFO);
					}
		        	return 2;
		        }
		        else
		        {
		        	return 0;
		        }
			}

			public String generaFolio(String usuario, String perfil, String contrato,
					String servicio, String registros) throws Exception{
				String result = "";
				String codError = "";
				String folioArchivo = "";
				String tramaParaFolio = usuario + "|" + perfil + "|" + contrato + "|"
						+ servicio + "|" + registros + "|";

				ServicioTux tuxGlobal = new ServicioTux();
				try {
					Hashtable hs = tuxGlobal.alta_folio(tramaParaFolio);
					result = (String) hs.get("BUFFER") + "";
				} catch (java.rmi.RemoteException re) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}

				if (result == null || result.equals("null"))
					result = "ERROR            Error en el servicio.";

				codError = result.substring(0, result.indexOf("|"));

				if (codError.trim().equals("TRSV0000")) {
					folioArchivo = result.substring(result.indexOf("|") + 1, result
							.length());
					folioArchivo = folioArchivo.trim();
					if (folioArchivo.length() < 12) {
						do {
							folioArchivo = "0" + folioArchivo;
						} while (folioArchivo.length() < 12);
					}
				}
				return folioArchivo;
			}

			public String ejecutaServicio(String trama) throws Exception{
				//textoLog("ejecutaServicio");
				Hashtable htResult = null;

				ServicioTux tuxGlobal = new ServicioTux();
				try {
					htResult = tuxGlobal.web_red(trama);
				} catch (java.rmi.RemoteException re) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				String tramaSalida = (String) htResult.get("BUFFER");
				//EIGlobal.mensajePorTrace(textoLog + "tramaSalida: [" + tramaSalida + "]", EIGlobal.NivelLog.ERROR);
				return tramaSalida;
			}
			public String realizaDispersion(String usuario, String perfil,
					String contrato, String folio, String servicio) throws Exception{
				//textoLog("realizaDispersion");
				String result = "";
				String codError = "";
				String mensajeErrorDispersor = "";
				String tramaParaDispersor = usuario + "|" + perfil + "|" + contrato
						+ "|" + folio + "|" + servicio + "|";

				ServicioTux tuxGlobal = new ServicioTux();
				try {
					Hashtable hs = tuxGlobal.inicia_proceso(tramaParaDispersor);
					result = (String) hs.get("BUFFER");
				} catch (java.rmi.RemoteException re) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				if (result == null || result.equals("null"))
					result = "ERROR            Error en el servicio.";
				//EIGlobal.mensajePorTrace(textoLog + "Dispersor de registros: Trama salida: " + result, EIGlobal.NivelLog.ERROR);
				codError = result.substring(0, result.indexOf("|"));
				//if (!codError.trim().equals("TRSV0000")) {
				//	EIGlobal.mensajePorTrace(textoLog + "No se realizo la dispersion de registros", EIGlobal.NivelLog.ERROR);
				//} else {
				//	EIGlobal.mensajePorTrace(textoLog + "Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.ERROR);
				//}
				mensajeErrorDispersor = result.substring(result.indexOf("|") + 1,
						result.length());
				return mensajeErrorDispersor;
			}

//			 metodo posCar que devuelve la posicion de un caracter en un String
			public int posCar(String cad, char car, int cant) {
				int result = 0, pos = 0;
				for (int i = 0; i < cant; i++) {
					result = cad.indexOf(car, pos);
					pos = result + 1;
				}
				return result;
			}

			public String envioDatos(String usuario, String perfil, String contrato,
					String folio, String servicio, String ruta, String trama)throws Exception {
				//textoLog("envioDatos");
				String result = "";
				String codError = "";
				String mensajeErrorEnviar = "";
				String tramaParaEnviar = usuario + "|" + perfil + "|" + contrato + "|"
						+ folio + "|" + servicio + "|" + ruta + "_preprocesar" + "|"
						+ ruta + "|" + trama;

				ServicioTux tuxGlobal = new ServicioTux();
				try {
					Hashtable hs = tuxGlobal.envia_datos(tramaParaEnviar);
					result = (String) hs.get("BUFFER") + "";
				} catch (java.rmi.RemoteException re) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}
				if (result == null || result.equals("null"))
					result = "ERROR            Error en el servicio.";
				//EIGlobal.mensajePorTrace(textoLog + "CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.ERROR);
				codError = result.substring(0, result.indexOf("|"));
				mensajeErrorEnviar = result.substring(result.indexOf("|") + 1, result.length());
				return mensajeErrorEnviar;
			}

			public boolean modificaArchivo(HttpServletRequest request)throws Exception{

				RandomAccessFile archivoOriginal;
				RandomAccessFile archivoRespaldo;
				String registroLeido = "";
				String registroAgregar = "";
				HttpSession sess = request.getSession();
				String nombreOriginal = sess.getAttribute("nombreOriginal").toString();
				BaseResource session = (BaseResource) sess.getAttribute("session");
				String contrato = session.getContractNumber();
				if(contrato.length() < 11){
					contrato = NomPreUtil.rellenar(contrato, 11, ' ', 'D');
				}
				String nomEmpresa = session.getNombreContrato();
				if(nomEmpresa.length() < 60){
					nomEmpresa = NomPreUtil.rellenar(nomEmpresa, 60, ' ', 'D');
				}
				//Inicio VSWF ESC nuevso campos NP
				try {
					File fileOriginal = new File(IEnlace.DOWNLOAD_PATH + "Mod" + nombreOriginal);
					File fileResp = new File(IEnlace.DOWNLOAD_PATH + nombreOriginal);
					archivoOriginal = new RandomAccessFile(fileOriginal, "rw");
					archivoRespaldo = new RandomAccessFile(fileResp, "rw" );
					archivoOriginal.seek(0);
					registroLeido = archivoOriginal.readLine();
					 while (registroLeido != null) {
						if(registroLeido.substring(0,1).equals("1")){
							archivoRespaldo.seek(0);
							registroAgregar += registroLeido.substring(0,7);
							String fecGeneracion= registroLeido.substring(11,15);
							fecGeneracion += registroLeido.substring(9,11);
							fecGeneracion += registroLeido.substring(7,9);
							registroAgregar += fecGeneracion;
							archivoRespaldo.writeBytes(registroAgregar + "\n");
							registroLeido = archivoOriginal.readLine();
						} else if (registroLeido.substring(0,1).equals("2")) {
							registroAgregar = registroLeido.substring(0, 6);
							registroAgregar += contrato;
							registroAgregar += nomEmpresa;
							String tarjeta = registroLeido.substring(428, 444);
							tarjeta = NomPreUtil.rellenar(tarjeta, 22, ' ', 'D');
							registroAgregar += tarjeta;
							registroAgregar += registroLeido.substring(13, 33);
							registroAgregar += registroLeido.substring(43, 63);
							String nomEmpleado = registroLeido.substring(63, 93);
							nomEmpleado = NomPreUtil.rellenar(nomEmpleado, 40, ' ', 'D');
							registroAgregar += nomEmpleado;
							registroAgregar += registroLeido.substring(93, 103);
							//registroAgregar += registroLeido.substring(6, 13);
							registroAgregar += formateaEmpleado(registroLeido.substring(6, 13));
							//jgal
							EIGlobal.mensajePorTrace(textoLog + " formateaEmpleado- invoca metodo: [" +
									formateaEmpleado(registroLeido.substring(6, 13)) + "]", EIGlobal.NivelLog.INFO);
							registroAgregar += registroLeido.substring(103, 106);
							registroAgregar += registroLeido.substring(106, 131);
							String fecNacimiento = registroLeido.substring(136, 140);
							fecNacimiento += registroLeido.substring(134, 136);
							fecNacimiento += registroLeido.substring(132, 134);
							registroAgregar += fecNacimiento;
							registroAgregar += registroLeido.substring(131, 132);
							registroAgregar += registroLeido.substring(140, 144);
							registroAgregar += registroLeido.substring(144, 169);
							registroAgregar += registroLeido.substring(169, 170);
							registroAgregar += NomPreUtil.rellenar(registroLeido.substring(170, 220), 60, ' ', 'D');
							registroAgregar += registroLeido.substring(220, 230);
							registroAgregar += registroLeido.substring(230, 240);
							registroAgregar += registroLeido.substring(240, 270);
							registroAgregar += registroLeido.substring(270, 300);
							registroAgregar += registroLeido.substring(300, 320);
							registroAgregar += registroLeido.substring(320, 350);
							registroAgregar += registroLeido.substring(350, 355);
							registroAgregar += registroLeido.substring(355, 360);
							registroAgregar += registroLeido.substring(360, 368);
							registroAgregar += registroLeido.substring(368, 398);
							registroAgregar += NomPreUtil.rellenar("", 8, ' ', 'D');;
							archivoRespaldo.writeBytes(registroAgregar + "\n");
							registroLeido = archivoOriginal.readLine();
						} else if (registroLeido.substring(0,1).equals("3")) {
							archivoRespaldo.writeBytes(registroLeido);
							registroLeido = archivoOriginal.readLine();
							archivoOriginal.close();
							archivoOriginal.close();

						}

						if(fileOriginal != null && fileOriginal.exists())
						{
							fileOriginal.delete();
						}
						//Fin VSWF ESC nuevso campos NP
					}
					return true;
				} catch (IOException e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					return false;
				}

			}

			public String formateaEmpleado(String empleado) {
				String result = "";
				String ceros = "0000000";
				String empleadoTmp = empleado.trim();
				if (!empleadoTmp.contains(" ")) {
					result = empleadoTmp;
				} else {
					for (int i = 0; i < empleadoTmp.length(); i ++) {
						if (empleadoTmp.charAt(i) != ' ') {
							result += empleadoTmp.charAt(i);
						}
					}
				}
				result = ceros.substring(0, (empleado.length() - result.length())) + result;
				return result;
			}


			public void guardaParametrosEnSessionIE(HttpServletRequest request) {
				textoLog("guardaParametrosEnSessionIE");
				EIGlobal.mensajePorTrace(textoLog + "Dentro de guardaParametrosEnSessionIE", EIGlobal.NivelLog.DEBUG);
				request.getSession().setAttribute("plantilla", request.getServletPath());

				HttpSession session = request.getSession(false);
				session.removeAttribute("bitacora");

				if (session != null) {
					Map<Object,Object> tmp = new HashMap<Object,Object>();

					EIGlobal.mensajePorTrace(textoLog + "valida-> " + getFormParameter(request, "valida"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "opcion-> " + getFormParameter(request, "opcion"), EIGlobal.NivelLog.DEBUG);

					String value;
					value = (String) getFormParameter(request, "valida");
					if (value != null && !value.equals(null)) {
						tmp.put("valida", getFormParameter(request, "valida"));
					}

					tmp.put("opcion", getFormParameter(request, "opcion"));

					if (request.getParameter("statusDuplicado") != null && request.getParameter("statusDuplicado").length() > 0) {

						tmp.put("statusDuplicado", request.getParameter("statusDuplicado"));
					}
					session.setAttribute("parametros", tmp);

					tmp = new HashMap<Object,Object>();
					Enumeration enumer = request.getAttributeNames();
					while (enumer.hasMoreElements()) {
						String name = (String) enumer.nextElement();
						tmp.put(name, request.getAttribute(name));
					}
					session.setAttribute("atributos", tmp);
				}
				EIGlobal.mensajePorTrace(textoLog + "Saliendo de guardaParametrosEnSessionIE", EIGlobal.NivelLog.DEBUG);
			}
			public void guardaParametrosEnSession(HttpServletRequest request) {
				textoLog("guardaParametrosEnSession");
				EIGlobal.mensajePorTrace(textoLog + "Dentro de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
				request.getSession().setAttribute("plantilla", request.getServletPath());

				HttpSession session = request.getSession(false);
				session.removeAttribute("bitacora");

				if (session != null) {
					Map<Object,Object> tmp = new HashMap<Object,Object>();

					EIGlobal.mensajePorTrace(textoLog + "valida-> " + getFormParameter(request, "valida"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "opcion-> " + getFormParameter(request, "opcion"), EIGlobal.NivelLog.DEBUG);

					EIGlobal.mensajePorTrace(textoLog + "registro-> " + getFormParameter(request, "registro"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "tipoArchivo-> " + getFormParameter(request, "tipoArchivo"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "statusDuplicado-> " + getFormParameter(request, "statusDuplicado"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "statushrc-> " + getFormParameter(request, "statushrc"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "encFechApli-> " + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "horario_seleccionado-> " + getFormParameter(request, "horario_seleccionado"), EIGlobal.NivelLog.DEBUG);
					String value;
					value = (String) getFormParameter(request, "valida");
					if (value != null && !value.equals(null)) {
						tmp.put("valida", getFormParameter(request, "valida"));
					}

					tmp.put("opcion", getFormParameter(request, "opcion"));
					tmp.put("num_empleado", getFormParameter(request, "num_empleado"));
					tmp.put("nom_empleado", getFormParameter(request, "nom_empleado"));
					tmp.put("paterno", getFormParameter(request, "paterno"));
					tmp.put("materno", getFormParameter(request, "materno"));
					tmp.put("rfc", getFormParameter(request, "rfc"));
					tmp.put("homoclave", getFormParameter(request, "homoclave"));
					tmp.put("nacimiento", getFormParameter(request, "nacimiento"));
					tmp.put("sexo", getFormParameter(request, "sexo"));
					tmp.put("nacionalidad", getFormParameter(request, "nacionalidad"));
					tmp.put("edo_civil", getFormParameter(request, "edo_civil"));
					tmp.put("correo", getFormParameter(request, "correo"));
					tmp.put("calle", getFormParameter(request, "calle"));
					tmp.put("numero", getFormParameter(request, "numero"));
					tmp.put("colonia", getFormParameter(request, "colonia"));
					tmp.put("delegacion", getFormParameter(request, "delegacion"));
					tmp.put("ciudad", getFormParameter(request, "ciudad"));
					tmp.put("estado", getFormParameter(request, "estado"));
					tmp.put("postal", getFormParameter(request, "postal"));
					tmp.put("lada", getFormParameter(request, "lada"));
					tmp.put("telefono", getFormParameter(request, "telefono"));
					tmp.put("tarjeta", getFormParameter(request, "tarjeta"));

					//Inicio VSWF ESC nuevos campos NP
					tmp.put("num_id", getFormParameter(request, "num_id"));
					tmp.put("vPais", getFormParameter(request, "vPais"));
					tmp.put("numExt", getFormParameter(request, "numExt"));
					if(getFormParameter(request, "numInt") != null)
						tmp.put("numInt", getFormParameter(request, "numInt"));
					else
						tmp.put("numInt", "");
					//Fin VSWF ESC nuevos campos NP

					session.setAttribute("parametros", tmp);

					tmp = new HashMap<Object,Object>();
					Enumeration enumer = request.getAttributeNames();
					while (enumer.hasMoreElements()) {
						String name = (String) enumer.nextElement();
						tmp.put(name, request.getAttribute(name));
					}
					session.setAttribute("atributos", tmp);
				}
				EIGlobal.mensajePorTrace(textoLog + "Saliendo de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
			}

			/********************************************/
			/**
			 * VSWF ESC
			 * Metodo: ObtenCatalogo
			 * Que realiza la conexion a BD para obtener los paises
			 */
			/********************************************/
		    public String ObtenCatalogo(String sqlTipoQuery) throws Exception{
		        String strOpcion = "";
		        Connection conn = null;
		        PreparedStatement TipoQuery = null;
		        ResultSet TipoResult = null;

		        try {
		            conn = createiASConn(Global.DATASOURCE_ORACLE);
		            if(conn == null) {EIGlobal.mensajePorTrace("Error al intentar crear la conexion.", EIGlobal.NivelLog.INFO); return "";}

		            TipoQuery = conn.prepareStatement(sqlTipoQuery);
		            if(TipoQuery == null) {EIGlobal.mensajePorTrace("No se pudo crear Query.", EIGlobal.NivelLog.INFO); return "";}

		            TipoResult = TipoQuery.executeQuery();
		            if(TipoResult == null) {EIGlobal.mensajePorTrace("No se pudo crear ResultSet.", EIGlobal.NivelLog.INFO); return "";}

		            strOpcion += "<option CLASS='tabmovtex'>MEXICO</option>\n";

		            while(TipoResult.next()) {
		            	if(!TipoResult.getString(2).trim().equals("MEXICO")){
		            		strOpcion += "<option CLASS='tabmovtex'>" + TipoResult.getString(2) + "</option> \n";
		            	}
		            }

		        } catch( SQLException sqle ) {
		        	EIGlobal.mensajePorTrace(new Formateador().formatea(sqle), EIGlobal.NivelLog.INFO);
		            return "";
		        } finally {
		            try {
		                TipoResult.close();
		                conn.close();
		            } catch(Exception e) {
		            	EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		            }
		        }

		        return strOpcion;
		    }
	}
