/*Banco Santander Mexicano
* Clase	MtoNomina, Carga la	plantilla para el mantenimineto	del	archivo	de pagos
* @author Rodrigo Godo
* @version 1.1
* fecha	de creacion: Diciembre 2000	- Enero	20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: Crea	un objeto del tipo TemplateMapBasic, carga un calendario para exhibirse	dentro del TemplateMap,
				asi como un combo con la lista de cuentas con las cuales puede operar el usuario,
				valida facultades para saber  que tipo de operaciones pueden efectuarse sobre los archivos de pago de nomina
				(creacion, importacion, altas, bajas, cambios, borrado y recuperacion de archivos,
				asi como la transmision de los pagos que habran de efectuarse).
*/

package	mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class InicioNomina extends BaseServlet {
	Vector diasNoHabiles;
	Vector diasNoHabilesJS;
	CalendarNomina myCalendarNomina;
	String nuevoMenu;

	public void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
				throws ServletException, IOException {
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
			throws IOException, ServletException {
		EIGlobal.mensajePorTrace("***InicioNomina.class &Entrando al metodo execute: &", EIGlobal.NivelLog.INFO);
		String modulo = "";
 		boolean sesionvalida = SesionValida( request, response );
 		HttpSession sess = request.getSession();
 	    BaseResource session = (BaseResource) sess.getAttribute("session");
         if (! sesionvalida ){return;}
 		try {
 			diasNoHabilesJS = new Vector();
 			myCalendarNomina= new CalendarNomina( );
 		}
 		catch(Exception e)
          {
 			despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina", "Importando Archivo", "s25800h", request, response );
    			EIGlobal.mensajePorTrace("***InicioNomina.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
          }
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/*************** Nomina Concepto de Archvo */
		if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
		  request.getSession().removeAttribute("conceptoEnArchivo");
		/*************** Nomina Concepto de Archvo */

         /* Modificacion validacion de facultades 19/09/2002 */
 		//System.out.println( "request.getSession ().getAttribute (\"INOMPAGONOM\")=" + request.getSession ().getAttribute ("INOMPAGONOM") );
 		/*System.out.println( "session.getFacultad INOMPAGONOM= " + session.getFacultad ("INOMPAGONOM") ); */
		/*	GETRONICS CP MEXICO, NAA.      EN ATENCION A LA INCIDENCIA IM335254
            MODIFICACION DE FACULTADES INOMENVIPAG Y CONSMOVTOS*/
          System.out.println( "session.getFacultad INOMENVIPAG= " + session.getFacultad ("INOMENVIPAG" ) );
	  System.out.println( "session.getFacultad CONSMOVTOS= " + session.getFacultad ("CONSMOVTOS") );

        //if( null!=(request.getSession ().getAttribute ("INOMPAGONOM") )  ) {
		if( session.getFacultad ("INOMPAGONOM") ) {
			//modificacion para integracion
			//String[][] arrayCuentas = ContCtasRelac(session.getContractNumber());
		//String cuentaCargo = request.getParameter("cuenta");
		//String cuentaCargotmp = "";
		// if (session.getCuentaCargo() != null)
		  //   cuentaCargotmp = session.getCuentaCargo();
	   			String VarFechaHoy = "";
				diasNoHabiles = myCalendarNomina.CargarDias();
				String strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
				Calendar fechaFin = myCalendarNomina.calculaFechaFin();
				VarFechaHoy = myCalendarNomina.armaArregloFechas();
				String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();
   		    //***********************************************
			//modificaci�n para integraci�n pva 07/03/2002
            //***********************************************
            /*
			String Cuentas		= "";
			String tipo_cuenta	= "";
			for ( int indice = 1; indice <= Integer.parseInt( arrayCuentas[0][0] ); indice++ ) {
 				tipo_cuenta=arrayCuentas[indice][1].substring(0, EIGlobal.NivelLog.ERROR);
 				if ( arrayCuentas[indice][2].equals( "P" ) && ( !tipo_cuenta.equals( "BM" ) ) &&
 						( !tipo_cuenta.equals( "SI" ) ) && ( !tipo_cuenta.equals( "49" ) ) )
 					if( arrayCuentas[indice][2].equals( "P" ) ) {
 						if(!existeFacultadCuenta(arrayCuentas[indice][1],"CARCHPROP", "-"))
 							Cuentas += "<OPTION	value = " + arrayCuentas[indice][1] + ">" +
 									arrayCuentas[indice][1] + "\n";
 					}
 			}*/
 			StringBuffer datesrvrt = new StringBuffer ("");   /*JEV 12/03/04 Inicio*/
			datesrvrt = new StringBuffer ("<Input type = \"hidden\" name =\"strAnio\" value = \"");
			datesrvrt.append (ObtenAnio());
			datesrvrt.append ("\">");
            datesrvrt.append ("<Input type = \"hidden\" name =\"strMes\" value = \"");
			datesrvrt.append (ObtenMes());
			datesrvrt.append ("\">");
            datesrvrt.append ("<Input type = \"hidden\" name =\"strDia\" value = \"");
			datesrvrt.append (ObtenDia());
			datesrvrt.append ("\">");    /*JEV 12/03/04 FIN*/
 /*			String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" +	ObtenAnio() + "\">";
 				   datesrvr	+= "<Input type = \"hidden\" name =\"strMes\" value = \"" +	ObtenMes() + "\">";
 				   datesrvr	+= "<Input type = \"hidden\" name =\"strDia\" value = \"" +	ObtenDia() + "\">";*/
  			/*modificacion para integracion
 			/*request.setAttribute( "Cuentas",
 					"<Option value = >Seleccione una cuenta" + Cuentas );*/
 			request.setAttribute( "VarFechaHoy", VarFechaHoy);
 			request.setAttribute( "fechaFin", strFechaFin);
 			request.setAttribute( "diasInhabiles", strInhabiles);
 			request.setAttribute( "fecha2", ObtenFecha( true ) );
 			request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
 		    request.setAttribute( "ContenidoArchivo", "" );
 			//request.setAttribute( "Encabezado",
 			//		CreaEncabezado( "Pago de N&oacute;mina",
 					//CreaEncabezado( "Consulta de N&oacute;mina",
 			//				"Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request ) );
 			String NumberOfRecords = "";
 			if(session.getNominaNumberOfRecords() != null)
 			 {
 				NumberOfRecords = session.getNominaNumberOfRecords();
 			 }
 			request.setAttribute( "totRegs", NumberOfRecords);
 			//request.setAttribute( "totRegs", session.getNominaNumberOfRecords());
 			estableceFacultades( request, response );
 		    //modificacion para integracion
 			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
			//request.setAttribute("cuenta", cuentaCargotmp);
			//request.setAttribute("cuentaCargotmp", cuentaCargotmp);
			//System.out.println("el valor de cuantaCargotmp ultimo>>>"+cuentaCargotmp);
			//evalTemplate( "/jsp/MantenimientoNomina.jsp", request, response );
			modulo = request.getParameter("Modulo");
			System.out.println("valor de modulo****"+ modulo);
			if ( (modulo == null) || ( modulo.equals("")))
                            modulo = "0";

			if (modulo.equals("0"))
			{

				//MSD Limpia variables de sesi�n usadas en dispersiones anteriores
				sess.removeAttribute("VALIDAR");
				sess.removeAttribute("HOR_SAT");
				sess.removeAttribute("HorarioNormal");
				sess.removeAttribute("VALIDAR");
				sess.removeAttribute("FOLIO");
				sess.removeAttribute("HR_P");
				sess.removeAttribute("archDuplicado");
				sess.removeAttribute("NOMIencFechApli");
				//MSD Limpia variables de sesi�n usadas en dispersiones anteriores

				/*	GETRONICS CP MEXICO, NAA.*/
				if(session.getFacultad ("INOMENVIPAG") )
				{
//					TODO: BIT CU 4141, El cliente entra al flujo
					/*
		    		 * VSWF ARR -I
		    		 */
					if (Global.USAR_BITACORAS.trim().equals("ON")){
						try{

						BitaHelper bh = new BitaHelperImpl(request, session,sess);
						bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA);
						bt.setContrato(session.getContractNumber());

						BitaHandler.getInstance().insertBitaTransac(bt);
						}catch (SQLException e){
							e.printStackTrace();
						}catch(Exception e){
							e.printStackTrace();
						}
					}
		    		/*
		    		 * VSWF ARR -F
		    		 */
System.out.println("DAG Entro modulo=0 y con Facultad INOMENVIPAG****");
				/***********************.  Req. Q14709. limpieza de variables de Sesion. *****************************/
				//Se controla la Recarga para evitar duplicidad de registros transferencia
                                    verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString()+".ses",true);
                                    if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null){
                                        request.getSession().removeAttribute("Recarga_Ses_Multiples");
                                        EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
                                    } else
                                        EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);
EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
				/**********************Termina limpieza de variables de Sesion**********************/
                                    request.setAttribute("Encabezado",CreaEncabezado("Pago de N&oacute;mina","Servicios &gt; N&oacute;mina &gt; Pagos","s25800h",request));
				    evalTemplate( "/jsp/MantenimientoNomina.jsp", request, response );
				}
			   else
				{
				   String laDireccion = "document.location='SinFacultades'";
				   request.setAttribute( "plantillaElegida", laDireccion);
				   request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				}
			}
		  else
			{
			  /*	GETRONICS CP MEXICO, NAA.*/
  			  if( session.getFacultad ("CONSMOVTOS") )
			   {
  				  //TODO: BIT CU 4151, El cliente entra al flujo
  				/*
  	    		 * VSWF
  	    		 */
  				if (Global.USAR_BITACORAS.equals("ON")){
	  				try{

	  				BitaHelper bh = new BitaHelperImpl(request, session,sess);
	  				bh.incrementaFolioFlujo(request.getParameter(BitaConstants.FLUJO));
	  				BitaTransacBean bt = new BitaTransacBean();
	  				bt = (BitaTransacBean)bh.llenarBean(bt);
	  				bt.setNumBit(BitaConstants.ES_NOMINA_PAGOS_CONSULTA_ENTRA);
	  				bt.setContrato(session.getContractNumber());
	  				BitaHandler.getInstance().insertBitaTransac(bt);
	  				}catch (SQLException e){
	  					e.printStackTrace();
	  				}catch(Exception e){
						e.printStackTrace();
	  				}
  				}
  	    		/*
  	    		 * VSWF
  	    		 */
				  request.setAttribute("Encabezado", CreaEncabezado("Consulta de N&oacute;mina",
				"Servicios &gt; N&oacute;mina  &gt; Consulta de N&oacute;mina", "s25800conh",request));
					request.setAttribute("Bitfechas",datesrvrt);
					evalTemplate( "/jsp/NominaDatos.jsp", request, response );
			   }
			  else
				{
				   String laDireccion = "document.location='SinFacultades'";
				   request.setAttribute( "plantillaElegida", laDireccion);
				   request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
				}
			}

		} else {
            //despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de Saldos de cuentas de cheques", "Consultas &gt; Saldos &gt; Por cuenta &gt; Cheques", "s25010h",req, res);
             // Modificado por incidencia SANT-324
             //req.getRequestDispatcher ("/EnlaceMig/SinFacultades").forward (req, res);
              //req.getRequestDispatcher ("/SinFacultades").forward (req, res);
              //req.getRequestDispatcher ("/jsp/SinFacRedirect.jsp").forward (req, res);
             //req.getRequestDispatcher ( Global.APP_PATH + "/WEB-INF/classes/EnlaceMig/SinFacultades").forward (req, res);
             String laDireccion = "document.location='SinFacultades'";
             request.setAttribute( "plantillaElegida", laDireccion);
             request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
             }

	}


 	// M�todo que establecer� las facultades para el m�dulo de pago de n�mina
 	public void estableceFacultades( HttpServletRequest request, HttpServletResponse response )
 			throws ServletException, IOException {
 		HttpSession sess = request.getSession();
         BaseResource session = (BaseResource) sess.getAttribute("session");
 		String eImportar="", eAltas="", eModificar="", eBaja="", eEnvio="", eRecuperar="",
 				eExportar="", eBorrar="";
 		String cHTML="<INPUT TYPE=\"hidden\" NAME=\"fImportar\" VALUE=\"";
 		String cHTML1="\">";

		//facultad para Importar
 		if(verificaFacultad("INOMIMPAPAG",request)) {
 			eImportar="ImportarVerdadero";
 		} else {
 			eImportar="SinFacultades";
 		}
 		//facultad para Alta de Registros
 		if(verificaFacultad("INOMALTAPAG",request)){
 			eAltas="AltaVerdadero";
 		} else {
 			eAltas="SinFacultades";
 		}
 		//facultad para Edicion de Registros
 		if(verificaFacultad("INOMMODIPAG",request)){
 			eModificar="CambiosVerdadero";
 		} else {
 			eModificar="SinFacultades";
 		}
 		//facultad para Baja de Registros
 		if(verificaFacultad("INOMBAJAPAG",request)){
 			eBaja="BajasVerdadero";
 		} else {
 			eBaja="SinFacultades";
 		}
 		//facultad para el Envio
 		if(verificaFacultad("INOMENVIPAG",request)){
 			eEnvio="EnvioVerdadero";
 		} else {
 			eEnvio="SinFacultades";
 			request.setAttribute("facultadEnvio",cHTML+eEnvio+cHTML1);
 		}
 		//facultad para la recuperacion
 		if(verificaFacultad("INOMACTUPAG",request)){
 			eRecuperar="RecuperarVerdadero";
 		} else {
 			eRecuperar="SinFacultades";
 		}
 		//facultad para exportar
 		if(verificaFacultad("INOMEXPOPAG",request)){
 			eExportar="ExportarVerdadero";
 		} else{
 			eExportar="SinFacultades";
 		}
 		//facultad para borrar un archivo
 		if(verificaFacultad("INOMBORRPAG",request)){
 			eBorrar="BorraVerdadero";
 		} else{
 			eBorrar="SinFacultades";
 		}
		//facultad para obtener HORARIOS PREMIER
		/**
		*		Modificado por Miguel Cortes Arellano
		*		Fecha 19/12/03
		*		Proposito: Verificar si el usuario tiene facultades para cliente premier. Y dar el correspondiente atributo a la cadena.
		*/
		String eHorarioPremier="";
		if(verificaFacultad("INOMHORARIOS",request)){
/*		int paso = 1;
		if (paso == 1){ */
			eHorarioPremier="PremierVerdadero";
		}else{
			eHorarioPremier="SinFacultades";
		}
		//String eHorarioPremier="PremierVerdadero";
		StringBuffer valor1 = new StringBuffer ("");  /*JEV 12/03/04*/
		valor1 = new StringBuffer (eImportar);
		valor1.append (",");
		valor1.append (eAltas);
		valor1.append (",");
		valor1.append (eModificar);
		valor1.append (",");
		valor1.append (eBaja);
		valor1.append (",");
		valor1.append (eEnvio);
		valor1.append (",");
		valor1.append (eRecuperar);
		valor1.append (",");
		valor1.append (eExportar);
		valor1.append (",");
		valor1.append (eBorrar);
		valor1.append (",");
		valor1.append (eHorarioPremier);    /*JEV 12/04/03 FIN*/

		sess.setAttribute("facultadesN",valor1.toString()); /*JEV 12/03/04*/
		//session.setFacultadesNomina(eImportar + "," + eAltas + "," + eModificar + "," + eBaja + "," + eEnvio + "," + eRecuperar + "," + eExportar + "," + eBorrar);
		//request.setAttribute("facultades",session.getFacultadesNomina());
		request.setAttribute("facultades",valor1.toString());    /*JEV 12/03/04*/
		//System.out.println("Facultades de nomina en la variable de ambiente: "+session.getFacultadesNomina());
		System.out.println("Facultades de nomina en la variable de ambiente: "+sess.getAttribute("facultadesN"));
	}

// Metodo para verificar existencia del Archivo de Sesion
public boolean verificaArchivos(String arc,boolean eliminar) throws ServletException, java.io.IOException{
	EIGlobal.mensajePorTrace( "InicioNomina.java Verificando si el archivo existe", EIGlobal.NivelLog.INFO);
	File archivocomp = new File( arc );
	if (archivocomp.exists()){
	   EIGlobal.mensajePorTrace( "InicioNomina.java El archivo si existe.", EIGlobal.NivelLog.INFO);
	   if(eliminar){
               archivocomp.delete();
               EIGlobal.mensajePorTrace( "InicioNomina.java El archivo se elimina.", EIGlobal.NivelLog.INFO);
           }
	   return true;
        }
	EIGlobal.mensajePorTrace( "InicioNomina.java El archivo no existe.", EIGlobal.NivelLog.INFO);
	return false;
}

}