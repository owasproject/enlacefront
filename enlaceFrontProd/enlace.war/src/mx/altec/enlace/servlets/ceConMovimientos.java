package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.*;
import java.sql.SQLException;
import java.text.*;

/**Banco Santander Mexicano
  Clase ceConMovimientos  Contiene las funciones para la presentacion de
  pantalla de Movimientos de Credito Electronico
  @Autor: Paula Hernandez
  @version: 1.0.1
  fecha de creacion:
  <P>Modificaciones:<BR>
  22/08/2002 Se agregaron las facultades de saldos y movimientos.<BR>
  30/09/2002 Se corrigieron acentos, se comentaron facultades y se elimin&oacute; el men&uacute; de ayuda.<BR>
  25/10/2002 Se agreg&oacute; la descripci&oacute;n de la cuenta al par&aacute;metro <I>noDisp</I>.
             Se remplazaron las llamadas est&aacute;ticas a <B>mensajePorTrace</B> por la de la nueva instancia.<BR>
  06/11/2002 Se agrega la descripci&oacute;n de la cuenta como atributo para llamar a <B>ceConMovimientos.jsp</B> y luego recuperalo<BR>
             como par&aacute;metro para formar <I>noDisp<I>.<BR>
  </P>
 */
public class ceConMovimientos extends BaseServlet{
    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	/**<code><B><I>defaultAction</I></B></code>
   * </P>22/08/2002: Se verifican las facultades de saldos y movimientos.<BR>
   * <P>
   */
	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

//	  String band = "0";
	  String modulo       = "";
	  String fileOut = "";
     Boolean facultadSaldo;
     Boolean facultadMovimientos;
     EIGlobal enlaceGlobal;
     String descripcionCuenta=new String(" ");

	 // Modificaci�n para usuarion concurrentes
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
     enlaceGlobal=new EIGlobal(session, getServletContext(), this);

	  modulo = (String) req.getParameter( "Modulo" );
	  log( "defaultAction()::2da::Modulo =" + modulo );

	  //	Modificai�n de usuarios concurrentes
	  //	if (session.getFacAltaCtaContrato().trim().equals("true"))
	  //	por if (session.getFacultad(session.FacAltaCtaContrato))

      boolean sesionvalida = SesionValida(req, res);
        if (sesionvalida)
		{

         if(session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC))
         {
            facultadSaldo=new Boolean(session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC));
         }
         else
         {
            facultadSaldo=new Boolean(false);
            enlaceGlobal.mensajePorTrace("No se obtuvo el valor de la facultad de saldos de cr�dito electr�nico.", EIGlobal.NivelLog.INFO);
         } // Fin if-else facultad saldos cr�dito electr�nico


         if(session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC))
         {
            facultadMovimientos=new Boolean(session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC));
         }
         else
         {
            facultadMovimientos=new Boolean(false);
            enlaceGlobal.mensajePorTrace("No se obtuvo el valor de la facultad de movimientos de cr�dito electr�nico.", EIGlobal.NivelLog.INFO);
         } // Fin if-else facultad movimientos cr�dito electr�nico



         // 30/09/2002 Se comentaron estas dos condiciones para revisar el m�dulo.
         if(!facultadSaldo.booleanValue())
         {
            getServletContext().getRequestDispatcher("/SinFacultades").forward(req, res);
            enlaceGlobal.mensajePorTrace("El usuario carece de la facultad de saldos de cr�dito electr�nico: "+session.getFacultad(session.FAC_TECRESALDOSC_CRED_ELEC), EIGlobal.NivelLog.INFO);
            return;
         } // Fin if vefificaci�n facultad saldos cr�dito electr�nico


         if(!facultadMovimientos.booleanValue())
         {
            getServletContext().getRequestDispatcher("/SinFacultades").forward(req, res);
            enlaceGlobal.mensajePorTrace("El usuario carece de la facultad de movimientos de cr�dito electr�nico: "+session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC), EIGlobal.NivelLog.INFO);
            return;
         } // Fin if vefificaci�n facultad saldos cr�dito electr�nico


//			fileOut = session.getUserID()+"movi.doc";
			fileOut = session.getUserID8()+".doc"; 

			boolean grabaArchivo = true;

			EI_Exportar archSalida;
			archSalida = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			archSalida.creaArchivo();


			if (modulo.equals( "0" ) )
			{

		      	if ( session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC))
				{
        			enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Entrando a ceConmovimeintos ", EIGlobal.NivelLog.INFO);

					String tramacta  = req.getParameter ("ctasLineas");
					enlaceGlobal.mensajePorTrace("<<<<< TRAMA CUENTA = " + tramacta + ">>>>>>>", EIGlobal.NivelLog.INFO);

					int      contador  = 0;
					boolean encontrado = false;

					String diasInhabiles = diasInhabilesAnt();

					if (diasInhabiles.trim().length()==0)
					{
				 		despliegaPaginaError("Servicio no disponible por el momento","Consulta de movimientos de Cr&eacute;dito Electr&oacute;nico", "Teorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos", "s32020h", req, res);
						return;
					}

					enlaceGlobal.mensajePorTrace("*** ceConMovimientos.class tramacta:" + tramacta, EIGlobal.NivelLog.INFO);


					String[] cta_posi2 = null;
					String cta = null;
					String noLinea = null;


					enlaceGlobal.mensajePorTrace("<<<<< TRAMA CUENTA 2 = " + tramacta + ">>>>>>>", EIGlobal.NivelLog.INFO);


					if (tramacta!=null)
					{
						if(tramacta.trim().length()==0) tramacta=null;
					}

					enlaceGlobal.mensajePorTrace("<<<<<  Pase el primer if >>>>>", EIGlobal.NivelLog.INFO);

					if (tramacta!=null)
               {
						cta_posi2 = desentramaC(tramacta,'|');
						cta=cta_posi2[1];
						noLinea=cta_posi2[2];
                  descripcionCuenta=cta_posi2[3];

                  if(descripcionCuenta==null || descripcionCuenta.length()<1)
                     descripcionCuenta=" ";
					}
               else
						cta=null;

					enlaceGlobal.mensajePorTrace("<<<<<  Cuenta = "+cta+" >>>>>", EIGlobal.NivelLog.INFO);
					enlaceGlobal.mensajePorTrace("<<<<<  Linea = "+noLinea+" >>>>>", EIGlobal.NivelLog.INFO);
               enlaceGlobal.mensajePorTrace("Descripci�n de la cuenta: "+descripcionCuenta, EIGlobal.NivelLog.INFO);

					enlaceGlobal.mensajePorTrace("<<<<<  Pase el primer if >>>>>", EIGlobal.NivelLog.INFO);

					req.setAttribute("cuenta",cta);
					req.setAttribute("nLinea",noLinea);
					req.setAttribute("MenuPrincipal", session.getStrMenu());

				   req.setAttribute("newMenu", session.getFuncionesDeMenu());
   				req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos de Cr&eacute;dito Electr&oacute;nico","Teorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos", "s32020h",req));
			      req.setAttribute("diasInhabiles",diasInhabiles);
               req.setAttribute("descripcionCuenta", descripcionCuenta);


			         req.setAttribute("Fecha",ObtenFecha());
                  req.setAttribute("fechaHoy", ObtenFecha(true));
				     String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
	                 datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
                     datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
                     req.setAttribute("Movfechas",datesrvr);
					 session.setModuloConsultar(IEnlace.MCredito_empresarial);

					 evalTemplate("/jsp/ceConMovimientos.jsp", req, res);
					 enlaceGlobal.mensajePorTrace("***ceConMovimientos.class /jsp/ceConMovimientos.jsp ", EIGlobal.NivelLog.INFO);
  					 enlaceGlobal.mensajePorTrace("***ceConMovimientos.class SALIENDO a ceConMovimientos ", EIGlobal.NivelLog.INFO);



		        }else // termina if ( session.getFAC_TECREMOVTOSC_CRED_ELEC().trim().length()!=0 )
					despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos de Cr&eacute;dito Electr&oacute;nico", "Teorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos", "s32020h", req, res);
			} // termina  if (modulo.equals( "0 ) )

			else if (modulo.equals( "1" ) )
			{

		      	if ( session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC))
				{

						enlaceGlobal.mensajePorTrace("*** ceConMovimientosDet.class entrando a ceConMovimientosDet", EIGlobal.NivelLog.INFO);

						int      contador  = 0;
						boolean encontrado = false;

						String diasInhabiles = diasInhabilesAnt();

						if (diasInhabiles.trim().length()==0)
						{
					 		despliegaPaginaError("Servicio no disponible por el momento","Movimientos","s32050h", req, res);
							return;
						}


        				enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Entrando a ceConMovimientosDet ", EIGlobal.NivelLog.INFO);

						String ctaSaldo     = "";
			            String tipoCuenta   = "";
			            String nombreCuenta = "";
						String Trama        = "";
						String []arrcta;
						String pipe         = "|";
						String arroba		= "@";
						String tramaCtaSaldo;
						String Result       = "";
						String contrato     = "";
					    String usuario      = "";
        				String clavePerfil  = "";
						String tabla = "";
						String estatusError = "";
						String errores = "";
						String titulo1 = "Consulta de movimientos Disposici&oacute;n";
						String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos";
						String ResultError = "";
						String noLineas = "";
						String bandera = "1";
						String flag = "0";  // 0 no hay datos, 1 hay datos
						String delDia = "";
						String impText = "";
						String FechaIni = "";
						String FechaFin = "";
						String NLinea = "";
//						float  importeReal = 0.00;
						int    anio = 0;
						int    mes  = 0;
						int    dia  = 0;
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Entrando a ceConSaldosDetalles", EIGlobal.NivelLog.INFO);


						ctaSaldo = (String )req.getParameter("Cuenta");
						delDia = (String )req.getParameter("deldia");
						NLinea = (String )req.getParameter("NLinea");

						impText = (String )req.getParameter("Importe");

						if ( impText == null )
							impText ="0";
						else if ( impText.trim().equals( "" ) )
						impText = "0";

						float importeReal = new Float(impText).floatValue();

						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class "+importeReal, EIGlobal.NivelLog.INFO);

						FechaIni = (String )req.getParameter("fecha1");

						anio = Integer.parseInt(FechaIni.substring(6,10));
						mes  = Integer.parseInt(FechaIni.substring(3,5));
						dia  = Integer.parseInt(FechaIni.substring(0,2));

						GregorianCalendar calFchIni   = new GregorianCalendar();
						calFchIni.set(anio,mes,dia);


						FechaFin = (String )req.getParameter("fecha2");

						anio = Integer.parseInt(FechaFin.substring(6,10));
						mes  = Integer.parseInt(FechaFin.substring(3,5));
						dia  = Integer.parseInt(FechaFin.substring(0,2));

						String txtFchIni;
						String txtFchFin;
						String txtcalDisp;

						txtFchFin = (String )req.getParameter("fecha1");
						txtFchIni = (String )req.getParameter("fecha2");
                  descripcionCuenta=req.getParameter("descripcionCuenta");

						GregorianCalendar calFchFin = new GregorianCalendar();
						calFchFin.set(anio,mes,dia);

						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+ctaSaldo, EIGlobal.NivelLog.INFO);

						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+delDia, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+FechaFin, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+FechaIni, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+impText, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class NLinea =  "+NLinea, EIGlobal.NivelLog.INFO);


						ServicioTux tuxGlobal = new ServicioTux();
						//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					    Hashtable htResult = null;

 						contrato      = session.getContractNumber();
	            		usuario       = session.getUserID8(); 
		    		    clavePerfil   = session.getUserProfile();

			            Trama+=IEnlace.medioEntrega2  +pipe;    // 1EWEB
				        Trama+=usuario          +pipe;			// empleado
					    Trama+="PC50"           +pipe;			// tipo_operacion
						Trama+=contrato         +pipe;			// contrato
	                    Trama+=usuario          +pipe;			// empleado
		                Trama+=clavePerfil      +pipe;			// clave_perfil
			            Trama+=contrato         +arroba;		// contrato
				        Trama+=ctaSaldo         +arroba;        // Cta cheques
						Trama+=usuario          +arroba;        // empleado
						Trama+=bandera          +pipe;			// bandera

		                enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

						try {
							    	htResult = tuxGlobal.web_red( Trama );
						    }
						catch ( java.rmi.RemoteException re )
							{
							    	re.printStackTrace();
						    }
						catch ( Exception e ){}

						Result = ( String ) htResult.get( "BUFFER" );
						enlaceGlobal.mensajePorTrace("ceConMovimientos.class : Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
						ResultError = Result;

						// Quitar (solo es simulacion)
//						Result=formateaResultLinea(usuario+"mov").trim();

						if(Result.substring(19,23).equals("0000"))
						{
							Result=formateaResultLinea(usuario, enlaceGlobal).trim();
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Trama Salida Formatea: "+Result, EIGlobal.NivelLog.INFO);
						}
						else
						Result="OPENFAIL";


						if(Result.equals("OPENFAIL"))
	                    {
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el if: "+Result, EIGlobal.NivelLog.INFO);
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el if: "+ResultError, EIGlobal.NivelLog.INFO);
							estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
							despliegaPaginaError(estatusError,titulo1,titulo2, "s32040h" , req, res );
						}
						else
						{
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el else:", EIGlobal.NivelLog.INFO);
							EI_Tipo ResultTmp=new EI_Tipo(this);

						    Result+=" @";
						    ResultTmp.strOriginal=Result;
					        ResultTmp.strCuentas=Result;
					        ResultTmp.numeroRegistros();
							ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
							ResultTmp.llenaArreglo();
					        ResultTmp.separaCampos();

							enlaceGlobal.mensajePorTrace("Rango de fecha "+FechaIni+" - " +FechaFin, EIGlobal.NivelLog.INFO);

							for (int i=0;i<ResultTmp.totalRegistros;i++)
							{

								String cta = new String(ResultTmp.camposTabla[i][0]);
								String lineaCredito = new String(ResultTmp.camposTabla[i][1]);
								String noDisposicion = new String(ResultTmp.camposTabla[i][2]);
								String fchDisp = new String(ResultTmp.camposTabla[i][3]);
								String fchVto = new String(ResultTmp.camposTabla[i][4]);
								String plazo = new String(ResultTmp.camposTabla[i][5]);
								String tasa = new String(ResultTmp.camposTabla[i][6]);
								float importe = new Float(ResultTmp.camposTabla[i][7]).floatValue();
								float interes = new Float(ResultTmp.camposTabla[i][8]).floatValue();
								float total = new Float(ResultTmp.camposTabla[i][9]).floatValue();
//							    Trama de Salida
//								65500144939|05000393947|0500321216|13-04-2002|15-04-2002|90|35|100000.00 |2111.11 |102111.11 |

								int anioAux = 0;
								int mesAux = 0;
								int diaAux = 0;

								anioAux = Integer.parseInt(fchDisp.substring(6,10));
								mesAux  = Integer.parseInt(fchDisp.substring(3,5));
								diaAux  = Integer.parseInt(fchDisp.substring(0,2));

								GregorianCalendar calDisp   = new GregorianCalendar();
								calDisp.set(anioAux,mesAux,diaAux);





								txtcalDisp = fchDisp.substring(0,2)+"/"+fchDisp.substring(3,5)+"/"+fchDisp.substring(6,10);


//								enlaceGlobal.mensajePorTrace("Fecha fchDisp "+fchDisp, EIGlobal.NivelLog.INFO);
//								enlaceGlobal.mensajePorTrace("Fecha Ini "+txtFchFin, EIGlobal.NivelLog.INFO);
//								enlaceGlobal.mensajePorTrace("Fecha Fin "+txtFchIni, EIGlobal.NivelLog.INFO);
//								enlaceGlobal.mensajePorTrace("Fecha fchDisp "+txtcalDisp, EIGlobal.NivelLog.INFO);
//								enlaceGlobal.mensajePorTrace("Descripci�n de la cuenta: "+descripcionCuenta, EIGlobal.NivelLog.INFO);

								enlaceGlobal.mensajePorTrace("Linea Credito: "+lineaCredito, EIGlobal.NivelLog.INFO);
								enlaceGlobal.mensajePorTrace("NLinea: "+NLinea, EIGlobal.NivelLog.INFO);

								if((calDisp.after(calFchIni) && calDisp.before(calFchFin)) || txtcalDisp.equals(txtFchFin) || txtcalDisp.equals(txtFchIni) )
								{
									if(lineaCredito.equals(NLinea))
									{
										enlaceGlobal.mensajePorTrace("Fecha ok "+fchDisp, EIGlobal.NivelLog.INFO);

										if (importeReal > 0)
										{
										  if (importe == importeReal)
										  {
											flag = "1";
											tabla += "<tr><td class=\"textabdatcla\" align=\"center\"><input type=\"radio\" name=\"noDisp\" value=\""+cta+"|"+ noDisposicion +"|"+descripcionCuenta+"|\"\"></td>";
										 	tabla += "<td class='textabdatcla' nowrap align=center>" + noDisposicion + "</td>";
										 	tabla += "<td class='textabdatcla' nowrap align=center>" + fchDisp.replace('-', '/') + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + fchVto.replace('-', '/') + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + plazo + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + tasa + "</td>";
											tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(importe) + "</td>" ;
											tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(total) + "</td>" ;
											tabla += "</tr>";


//											TODO BIT CU3091 Se realiza la operaci�n PC50
											/**
											 * VSWF
											 * 17/Enero/2007
											 */
											if (Global.USAR_BITACORAS.trim().equals("ON")){
												String cuentaBit = cta;
												BitaHelper bh = new BitaHelperImpl(req, session, sess);
												BitaTransacBean bt = new BitaTransacBean();
												bt = (BitaTransacBean)bh.llenarBean(bt);
												bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC50);
												bt.setContrato((contrato == null)? " ":contrato);
												bt.setImporte(importeReal);
												bt.setFechaAplicacion((calFchFin.getTime() == null)?new Date():calFchFin.getTime());
												bt.setFechaProgramada((calFchIni.getTime() == null)?new Date():calFchFin.getTime());
												bt.setNombreArchivo((fileOut == null)?" ":fileOut);

												if(cta!=null && !cta.equals("")){
													if(cta.length()>20){
														cuentaBit=cta.substring(0,20);
													}
													else
														cuentaBit = cta;
												}
												try {
													BitaHandler.getInstance().insertBitaTransac(bt);
												} catch (SQLException e) {
													e.printStackTrace();
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
											/**
											 * VSWF
											 */




										  }


										  String lineaArchivo = "";

										  lineaArchivo = noDisposicion+" "+fchDisp.replace('-', '/')+" "+fchVto.replace('-', '/')+" "+plazo+" "+tasa+" "+FormatoMonedaExportar(importe)+" "+FormatoMonedaExportar(total);

										  if (grabaArchivo)
										  {
  												archSalida.escribeLinea(lineaArchivo+ "\r\n");
										  }

										}
										else
										{   flag = "1";
											tabla += "<tr><td class=textabdatcla align=center><input type=radio name=noDisp value=\""+cta+"|"+ noDisposicion + "|" + descripcionCuenta +"|\"\"></td>";
										 	tabla += "<td class='textabdatcla' nowrap align=center>" + noDisposicion + "</td>";
										 	tabla += "<td class='textabdatcla' nowrap align=center>" + fchDisp.replace('-', '/') + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + fchVto.replace('-', '/') + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + plazo + "</td>";
											tabla += "<td class='textabdatcla' nowrap align=center>" + tasa + "</td>";
											tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(importe) + "</td>" ;
											tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(total) + "</td>" ;
											tabla += "</tr>";

											String lineaArchivo = "";

										    lineaArchivo = noDisposicion +"  "+  fchDisp.replace('-', '/') +"  "+ fchVto.replace('-', '/') +"  "+ plazo +"  "+ tasa +"  "+ FormatoMonedaExportar(importe) +"  "+ FormatoMonedaExportar(total);

										    if (grabaArchivo)
										    {
  										   	   archSalida.escribeLinea(lineaArchivo+ "\r\n");
										    }

//										  TODO BIT CU3091 Se realiza la operaci�n PC50
											/**
											 * VSWF
											 * 17/Enero/2007
											 */
											if (Global.USAR_BITACORAS.trim().equals("ON")){
												String cuentaBit = cta;
												BitaHelper bh = new BitaHelperImpl(req, session, sess);
												BitaTransacBean bt = new BitaTransacBean();
												bt = (BitaTransacBean)bh.llenarBean(bt);
												bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC50);
												bt.setContrato((contrato == null)? " ":contrato);
												bt.setImporte(importeReal);
												bt.setFechaAplicacion((calFchFin.getTime() == null)?new Date():calFchFin.getTime());
												bt.setFechaProgramada((calFchIni.getTime() == null)?new Date():calFchFin.getTime());
												bt.setNombreArchivo((fileOut == null)?" ":fileOut);

												if(cta!=null && !cta.equals("")){
													if(cta.length()>20){
														cuentaBit=cta.substring(0,20);
													}
													else
														cuentaBit = cta;
												}
												try {
													BitaHandler.getInstance().insertBitaTransac(bt);
												} catch (SQLException e) {
													e.printStackTrace();
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
											/**
											 * VSWF
											 */


										}

									}	// termina if(lineaCredito.equal(NLinea))

								}



								enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Autorizado: "+cta, EIGlobal.NivelLog.INFO);
								enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Dispuesto: "+lineaCredito, EIGlobal.NivelLog.INFO);
								enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Disponible: "+noDisposicion, EIGlobal.NivelLog.INFO);




							}//for


							if (grabaArchivo)
							archSalida.cierraArchivo();

							ArchivoRemoto archRemoto = new ArchivoRemoto();
							if(!archRemoto.copiaLocalARemoto(fileOut,"WEB"))
								enlaceGlobal.mensajePorTrace("***ceConMovimientos.class No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.INFO);

							String exportar = "";
   							if ( grabaArchivo )
							exportar = "<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A>";
							req.setAttribute("Exportar",exportar);

							req.setAttribute("valor",tabla);
							req.setAttribute("cta",ctaSaldo);
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							if(delDia.equals("HIST"))
							req.setAttribute("Encabezado", CreaEncabezado("Consulta de  Disposiciones hist&oacute;ricas","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos","s32050h",req));
							else
							req.setAttribute("Encabezado", CreaEncabezado("Consulta de  Disposiciones del d&iacute;a","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos","s32040h",req));
							req.setAttribute("diasInhabiles",diasInhabiles);

							session.setModuloConsultar(IEnlace.MCredito_empresarial);

							if (flag == "0")
							{
								 String error = "No Hay datos para el filtro indicado.";
								 estatusError=error+"<br>";
								 if(delDia.equals("HIST"))
							     despliegaPaginaError(estatusError,"Consulta de  Disposiciones hist&oacute;ricas","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos", "s32050h" , req, res );
								 else
								 despliegaPaginaError(estatusError,"Consulta de  Disposiciones del d&iacute;a","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos", "s32040h" , req, res );

							}
							else{
								evalTemplate("/jsp/ceConMovimientosDet.jsp", req, res);
							}
						} // else

				} // termina if if ( session.getFAC_TECREMOVTOSC_CRED_ELEC().trim().length()!=0 )

			} // termina  if (modulo.equals( "1" ) )
			else if (modulo.equals( "2" ) )
			{
			if ( session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC))
				{
        			enlaceGlobal.mensajePorTrace("***ceConMovimientosDet.class Entrando a ceConMovimientosDisp ", EIGlobal.NivelLog.INFO);


					String tramaDisp  = req.getParameter ("noDisp");
					enlaceGlobal.mensajePorTrace("<<<<< No Disposicion = " + tramaDisp + ">>>>>>>", EIGlobal.NivelLog.INFO);

					String noDisposicion = tramaDisp.substring(12,23);

					enlaceGlobal.mensajePorTrace("<<<<< No Disposicion = " + noDisposicion + ">>>>>>>", EIGlobal.NivelLog.INFO);
					int      contador  = 0;
					boolean encontrado = false;



					req.setAttribute("noDisposicion",noDisposicion);
					req.setAttribute("MenuPrincipal", session.getStrMenu());

				    req.setAttribute("newMenu", session.getFuncionesDeMenu());

   				    req.setAttribute("Encabezado", CreaEncabezado("Movimientos de Disposiciones","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos","s32060h",req));

	//		       	req.setAttribute("diasInhabiles",diasInhabiles);



			         req.setAttribute("Fecha",ObtenFecha());
				     String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
	                 datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
                     datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
                     req.setAttribute("Movfechas",datesrvr);
					 session.setModuloConsultar(IEnlace.MCredito_empresarial);

					 evalTemplate("/jsp/ceConMovimientosDisp.jsp", req, res);

  					 enlaceGlobal.mensajePorTrace("***ceConMovimientos.class SALIENDO a ceConMovimientosDisp ", EIGlobal.NivelLog.INFO);



		        }else // termina if ( session.getFAC_TECREMOVTOSC_CRED_ELEC().trim().length()!=0 )
					despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Movimientos de Disposiciones", "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos","s32060h", req, res);
			} // termina  if (modulo.equals( "2" ) )
			else if (modulo.equals( "3" ) )
			{

		      	if ( session.getFacultad(session.FAC_TECREMOVTOSC_CRED_ELEC))
				{

						enlaceGlobal.mensajePorTrace("*** ceConMovimientosDet.class entrando a ceConMovimientosDet", EIGlobal.NivelLog.INFO);

						int      contador  = 0;
						boolean encontrado = false;

						String diasInhabiles = diasInhabilesAnt();

						if (diasInhabiles.trim().length()==0)
						{
					 		despliegaPaginaError("Servicio no disponible por el momento","Movimientos","", req, res);
							return;
						}

        				enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Entrando a ceConMovDetalleDisp ", EIGlobal.NivelLog.INFO);

						String ctaSaldo     = "";
			            String tipoCuenta   = "";
			            String nombreCuenta = "";
						String Trama        = "";
						String []arrcta;
						String pipe         = "|";
						String arroba		= "@";
						String tramaCtaSaldo;
						String Result       = "";
						String contrato     = "";
					    String usuario      = "";
        				String clavePerfil  = "";
						String tabla = "";
						String estatusError = "";
						String errores = "";
						String titulo1 = "Detalle de Disposici&oacute;n";
						String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos";
						String ResultError = "";
						String noLineas = "";
						String bandera = "1";
						String flag = "0";
						String delDia = "";
						String impText = "";
						String FechaIAux = "";
						String FechaFAux = "";
						String fechaIni = "";
						String fechaFin = "";

						String    anio = "";
						String    mes  = "";
						String    dia  = "";

						String refeText = "";
						int refeReal = 0;
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Entrando a ceConMovDetallesDisp", EIGlobal.NivelLog.INFO);

						String disp  = req.getParameter ("nDisp");
						enlaceGlobal.mensajePorTrace("***no de Disposicion : "+disp, EIGlobal.NivelLog.INFO);

						delDia = (String )req.getParameter("deldia");

						impText = (String )req.getParameter("Importe");

						if ( impText == null )
							impText ="0";
						else if ( impText.trim().equals( "" ) )
							impText = "0";

						float importeReal = new Float(impText).floatValue();

						refeText = (String )req.getParameter("Refe");

						if ( refeText == null )
							refeText ="0";
						else if ( refeText.trim().equals( "" ) )
							refeText = "0";

						refeReal = Integer.parseInt(refeText);

						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class "+importeReal, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class "+refeText, EIGlobal.NivelLog.INFO);

						FechaIAux = (String )req.getParameter("fecha1");

						anio = FechaIAux.substring(8,10);
						mes  = FechaIAux.substring(3, 3);
						dia  = FechaIAux.substring(0, 2);

						fechaIni = dia+"-"+mes+"-"+anio;

						FechaFAux = (String )req.getParameter("fecha2");

						anio = FechaFAux.substring(8,10);
						mes  = FechaFAux.substring(3, 3);
						dia  = FechaFAux.substring(0, 2);

						fechaFin = dia+"-"+mes+"-"+anio;

						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+delDia, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+fechaFin, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+fechaIni, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("***ceConMovimientos.class Cuenta =  "+impText, EIGlobal.NivelLog.INFO);



						ServicioTux tuxGlobal = new ServicioTux();
						//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
					    Hashtable htResult = null;

 						contrato      = session.getContractNumber();
	            		usuario       = session.getUserID8();  
		    		    clavePerfil   = session.getUserProfile();

			            Trama+=IEnlace.medioEntrega2  +pipe;    // 2EWEB
				        Trama+=usuario          +pipe;			// empleado
					    Trama+="PD97"           +pipe;			// tipo_operacion
						Trama+=contrato         +pipe;			// contrato
	                    Trama+=usuario          +pipe;			// empleado
		                Trama+=clavePerfil      +pipe;			// clave_perfil
			            Trama+=disp             +arroba;		// NoDisposicion
				        Trama+=fechaIni         +arroba;        // fechaInicial
						Trama+=fechaFin         +arroba;        // fechaFinal
						Trama+=usuario          +pipe;			// empleado

		                enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

						try {
							    	htResult = tuxGlobal.web_red( Trama );
						    }
						catch ( java.rmi.RemoteException re )
							{
							    	re.printStackTrace();
						    }
						catch ( Exception e ){}

						Result = ( String ) htResult.get( "BUFFER" );
						enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
						ResultError = Result;

						// Quitar (solo es simulacion)
//						Result=formateaResultLineaDisp(usuario+"Disp").trim();

						if(Result.substring(19,23).equals("0000"))
						{
							Result=formateaResultLineaDisp(usuario, enlaceGlobal).trim();
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Trama Salida Formatea: "+Result, EIGlobal.NivelLog.INFO);
						}
						else
							Result="OPENFAIL";


						if(Result.equals("OPENFAIL"))
	                    {
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el if: "+Result, EIGlobal.NivelLog.INFO);
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el if: "+ResultError, EIGlobal.NivelLog.INFO);
							estatusError+=ResultError.substring(19,ResultError.length())+"<br>";

							//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
							//req.setAttribute("Errores",errores);
							despliegaPaginaError(estatusError,titulo1,titulo2,"s32060h", req, res );
						}
						else
						{
							enlaceGlobal.mensajePorTrace("ceConMovimientos.class: Estoy en el else:", EIGlobal.NivelLog.INFO);
							EI_Tipo ResultTmp=new EI_Tipo(this);

						    Result+=" @";
						    ResultTmp.strOriginal=Result;
					        ResultTmp.strCuentas=Result;
					        ResultTmp.numeroRegistros();
							ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
							ResultTmp.llenaArreglo();
					        ResultTmp.separaCampos();



							for (int i=0;i<ResultTmp.totalRegistros;i++)
							{

								String fecha = new String(ResultTmp.camposTabla[i][0]);
								String concepto = new String(ResultTmp.camposTabla[i][1]);
								String refeAux = new String(ResultTmp.camposTabla[i][2]);
								int    referencia;

								enlaceGlobal.mensajePorTrace("fererencia  "+refeAux, EIGlobal.NivelLog.INFO);
								enlaceGlobal.mensajePorTrace("fererencia (0,2) "+refeAux.substring(0,2), EIGlobal.NivelLog.INFO);

								if (refeAux.substring(0,2).equals("NO"))
								{
									enlaceGlobal.mensajePorTrace("fererencia (9,12) "+refeAux.substring(9,12), EIGlobal.NivelLog.INFO);
									referencia = Integer.parseInt(refeAux.substring(9,12));

								}
								else
								{
									referencia = Integer.parseInt(refeAux);
								}

								float cargo = new Float(ResultTmp.camposTabla[i][3]).floatValue();
								float abono = new Float(ResultTmp.camposTabla[i][4]).floatValue();


								if (importeReal > 0)
								{
									if (cargo == importeReal)
									{
									  flag = "1";
									  tabla += "<tr>";
									  tabla += "<td class='textabdatcla' nowrap align=center>" + fecha.replace('-', '/') + "</td>";
									  tabla += "<td class='textabdatcla' nowrap align=center>" + concepto + "</td>";
  									  tabla += "<td class='textabdatcla' nowrap align=center>" + referencia + "</td>";
									  tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(cargo) + "</td>" ;
									  tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(abono) + "</td>" ;
									  tabla += "</tr>";

									String lineaArchivo = "";

									lineaArchivo = fecha.replace('-', '/')+" "+concepto+" "+referencia+" "+FormatoMonedaExportar(cargo)+" "+FormatoMonedaExportar(abono);

									if (grabaArchivo)
									{
  										archSalida.escribeLinea(lineaArchivo+ "\r\n");
									}

									}



								} //if
								else if (refeText.length() > 0 && !refeText.equals("0"))
								{
									if (referencia == refeReal)
									{
										flag = "1";
									  tabla += "<tr>";
									  tabla += "<td class='textabdatcla' nowrap align=center>" + fecha.replace('-', '/') + "</td>";
									  tabla += "<td class='textabdatcla' nowrap align=center>" + concepto + "</td>";
  									  tabla += "<td class='textabdatcla' nowrap align=center>" + referencia + "</td>";
									  tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(cargo) + "</td>" ;
									  tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(abono) + "</td>" ;
									  tabla += "</tr>";

									  String lineaArchivo = "";

										lineaArchivo = fecha +"  "+  concepto +"  "+ referencia +"  "+ FormatoMonedaExportar(cargo) +"  "+ FormatoMonedaExportar(abono);

										if (grabaArchivo)
										{
  											archSalida.escribeLinea(lineaArchivo+ "\r\n");
										}

									}

								}
								else
								{   flag = "1";
									tabla += "<tr>";
									tabla += "<td class='textabdatcla' nowrap align=center>" + fecha.replace('-', '/') + "</td>";
									tabla += "<td class='textabdatcla' nowrap align=center>" + concepto + "</td>";
  									tabla += "<td class='textabdatcla' nowrap align=center>" + referencia + "</td>";
									tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(cargo) + "</td>" ;
									tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(abono) + "</td>" ;
									tabla += "</tr>";

									String lineaArchivo = "";

									lineaArchivo = fecha+" "+concepto+" "+referencia+" "+FormatoMonedaExportar(cargo)+" "+FormatoMonedaExportar(abono);

									if (grabaArchivo)
									{
  										archSalida.escribeLinea(lineaArchivo+ "\r\n");
									}

								}

							} //for


							if (grabaArchivo)
							archSalida.cierraArchivo();

							ArchivoRemoto archRemoto = new ArchivoRemoto();
							if(!archRemoto.copiaLocalARemoto(fileOut,"WEB"))
								enlaceGlobal.mensajePorTrace("***ceConMovimientos.class No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.INFO);

							String exportar = "";
   							if ( grabaArchivo )
							exportar = "<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A>";
							req.setAttribute("Exportar",exportar);


							req.setAttribute("valor",tabla);
							req.setAttribute("cta",ctaSaldo);
//							req.setAttribute("noDisposicion",noDisposicion);
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							req.setAttribute("Encabezado", CreaEncabezado("Detalle de Disposici&oacute;n","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Consultas &gt; Movimientos","s32150h", req));
							req.setAttribute("diasInhabiles",diasInhabiles);

							session.setModuloConsultar(IEnlace.MCredito_empresarial);

							if (flag == "0")
							{
								String error = "No Hay datos para el filtro indicado.";
  							    estatusError=error+"<br>";
							    despliegaPaginaError(estatusError,titulo1,titulo2,"s32150h", req, res );

							}else{
								//TODO BIT CU3091 Se realiza la operaci�n PD97
								/**
								 * VSWF
								 * 17/Enero/2007
								 */
								if (Global.USAR_BITACORAS.equals("ON")){
									BitaHelper bh = new BitaHelperImpl(req, session, sess);
									BitaTransacBean bt = new BitaTransacBean();
									bt = (BitaTransacBean)bh.llenarBean(bt);
									bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD97);
									bt.setContrato((contrato == null)?" ":contrato);
									bt.setImporte(importeReal);
									bt.setNombreArchivo((fileOut == null)?" ":fileOut);
									bt.setReferencia(refeReal);
									bt.setIdErr((ResultError.length()>=8)?ResultError.substring(0,8):(ResultError.length()>0)?ResultError:" ");


									try {
										BitaHandler.getInstance().insertBitaTransac(bt);
									} catch (SQLException e) {
										e.printStackTrace();
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
								/**
								 * VSWF
								 */
							evalTemplate("/jsp/ceConMovDetalleDisp.jsp", req, res);
							}
							enlaceGlobal.mensajePorTrace("***ceConMovimientos.class SALIENDO a ceConMovDetalleDisp ", EIGlobal.NivelLog.INFO);

						} // else

				} // termina if if ( session.getFacCMovtos().trim().length()!=0 )

			} // termina  if (modulo.equals( "3" ) )


        } else if( sesionvalida ) // if (sesionvalida)
		{
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos","", req, res);
        }

}

/*************************************************************************************/
/*************************************************** formatea Reultado para Linea    */
/*************************************************************************************/
    String formateaResultLinea(String usuario, EIGlobal enlaceGlobal)
	{

       String cadenaTCT="";
       String arcLinea="";
		// Quitar (solo es simulacion)
	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

//	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

	   int totalLineas=0;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 enlaceGlobal.mensajePorTrace("ceConSaldosDetalle- formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
	  {
          boolean noError=true;
		  do
		  {
				 arcLinea=ArcEnt.leeLinea();
			     if(!arcLinea.equals("ERROR"))
			     {
		          if(arcLinea.substring(0,5).trim().equals("ERROR"))
			         break;
				  totalLineas++;
				  if(totalLineas>=1)
                    cadenaTCT+=arcLinea+"@";
                 }
                 else
                 noError=false;
          }while(noError);
		  ArcEnt.cierraArchivo();
       }
       else
        return "OPENFAIL";
       return cadenaTCT;


    }


    String formateaResultLineaDisp(String usuario, EIGlobal enlaceGlobal)
	{

       String cadenaTCT="";
       String arcLinea="";


		// Quitar (solo es simulac�n)
	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

// 	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

	   int totalLineas=0;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 enlaceGlobal.mensajePorTrace("ceConSaldosDetalle- formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
	  {
          boolean noError=true;
		  do
		  {
				 arcLinea=ArcEnt.leeLinea();
			     if(!arcLinea.equals("ERROR"))
			     {
		          if(arcLinea.substring(0,5).trim().equals("ERROR"))
			         break;
				  totalLineas++;
				  if(totalLineas>=1)
                    cadenaTCT+=arcLinea+"@";
                 }
                 else
                 noError=false;
          }while(noError);
		  ArcEnt.cierraArchivo();
       }
       else
        return "OPENFAIL";
       return cadenaTCT;



    }

	public String FormatoMonedaExportar ( double cantidad ) {
         String formato = "";
         String language = "la"; // la
         String country = "MX";  // MX
         Locale local = new Locale (language,  country);
         NumberFormat nf = NumberFormat.getCurrencyInstance (local);
         if (cantidad < 0)  {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" -"+ formato.substring (4,formato.length ());
    	    		 else
    	    			 formato =" -"+ formato.substring (2,formato.length ());
             } catch(NumberFormatException e) {}
         } else {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" "+ formato.substring (3,formato.length ());
    	    		 else
    	    			 formato =" "+ formato.substring (1,formato.length ());
             } catch(NumberFormatException e) {}
         }
         return formato;
     }

}