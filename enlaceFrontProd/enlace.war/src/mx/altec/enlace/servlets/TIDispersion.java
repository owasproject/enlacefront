package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TI_Cuenta;
import mx.altec.enlace.bo.TI_CuentaDispersion;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;


import java.sql.*;

public class TIDispersion extends BaseServlet
     {

        // --- Entrada por metodo GET ------------------------------------------------------
        public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
                {defaultAction(req, res);}

        // --- Entrada por metodo POST -----------------------------------------------------
        public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
                {defaultAction(req, res);}

        // --- MAIN (Este metodo se ejecuta en cuanto se invoca al servlet) ----------------
        public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
                {
                debug("Entrando a defaultAction()");

                String parAccion = getFormParameter(req,"Accion"); if(parAccion == null) parAccion = "";
                String parTrama = getFormParameter(req,"Trama"); if(parTrama == null) parTrama = "";
                String parCuentas = getFormParameter(req,"Cuentas"); if(parCuentas == null) parCuentas = "";

                //CCB - Jan192004 - Variable para cambio de session para rdbuttons del jsp
                String Agrega = getFormParameter(req,"hdnAgrega");

		EIGlobal.mensajePorTrace("Agrega vcl: "+Agrega, EIGlobal.NivelLog.INFO);
                TIComunUsd tipoCta = new TIComunUsd();

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                int pant = 0;

                /*************CCB Modificacion para los valores de los rdButtons**/
				String moduloTI = (String)sess.getAttribute("moduloTI");
				EIGlobal.mensajePorTrace("ModuloTI vcl: "+moduloTI, EIGlobal.NivelLog.INFO);
				String accesaMod = "";

				if(moduloTI==null) moduloTI="";

				if (moduloTI.equals("DISPERSION"))
					accesaMod = "1";
				else
					accesaMod = "0";

				sess.setAttribute("moduloTI","DISPERSION");

				if(moduloTI==null) moduloTI="";

				String tipoMoneda = "";
                if(!accesaMod.equals("1"))
                {
                	tipoCta.cuentasContrato(req, res);
	            }
	            else
	            {
	                if(Agrega==null) Agrega = "";
	                EIGlobal.mensajePorTrace("CCBSess --> " + Agrega, EIGlobal.NivelLog.INFO);


	                if (Agrega.equals("USD"))
	                {
	                	sess.setAttribute("ctasUSD", "Existe");
	                	sess.setAttribute("ctasMN", "");
	                }
	                if (Agrega.equals("MN"))
	                {
	                	sess.setAttribute("ctasUSD", "");
	                	sess.setAttribute("ctasMN", "Existe");
	                }
					String Divisa = "";

					Divisa = (String)sess.getAttribute("DivisaDispersion");
					EIGlobal.mensajePorTrace ("DIVISA ACTUAL VCL..."+ Divisa,EIGlobal.NivelLog.INFO);

					if(Divisa != null){
						if(!Divisa.trim().equals("") && moduloTI.equals("DISPERSION") && Agrega.equals(""))
						{
							Agrega=Divisa;
							tipoMoneda = Divisa;
//							Divisa = Agrega;
							EIGlobal.mensajePorTrace("DIVISA ACTUALIZADA VCL...."+Divisa, EIGlobal.NivelLog.INFO);
//							sess.removeAttribute("Divisa");
//							sess.setAttribute("Divisa", Agrega);
						}
					}else
						Divisa = "";

					EIGlobal.mensajePorTrace ("La divisa ***** es: "+Agrega,EIGlobal.NivelLog.INFO);
					tipoMoneda=Agrega;
					//sess.removeAttribute("Divisa");
					sess.setAttribute("DivisaDispersion", Agrega);


					/*if((Divisa != null || !Divisa.trim().equals("")) && moduloTI.equals("CONCENTRACION") && Agrega.equals(""))
						Agrega=Divisa;
					else
						sess.removeAttribute("Divisa");
					sess.setAttribute("Divisa", Agrega);*/
	            }
	            /**************************************************************/

                if(!SesionValida(req,res)) return;

                setMensError("",req);

                if(!session.getFacultad(session.FacConEstr)) setMensError("X-PUsted no tiene facultad para consultar estructuras", req);

                session.setModuloConsultar(IEnlace.MMant_estruc_TI);
				String cuentaTemp = "";
				if(parAccion.equals("")) {parCuentas = consultaArbol(req);}
						EIGlobal.mensajePorTrace("Par Trama  "+ parTrama, EIGlobal.NivelLog.INFO);
                if(parAccion.equals("")) {parCuentas = consultaArbol(req);}
                if(parAccion.equals("AGREGA")) {cuentaTemp=parTrama;parCuentas = agregaCuenta(parTrama, parCuentas, req);}
                if(parAccion.equals("ELIMINA")) {cuentaTemp=parTrama;parCuentas = eliminaCuenta(parTrama, parCuentas, req);}
                if(parAccion.equals("EDITA")) {if(session.getFacultad(session.FacCapDisper)) {cuentaTemp = parTrama;parTrama = tramaCuenta(parTrama, parCuentas); pant=1;} else setMensError(":-/Usted no tiene facultad para capturar datos", req);}
                if(parAccion.equals("EDITA_MODIFICA")) {parCuentas = tramaModificaCuenta(parTrama, parCuentas, req); pant=1;}
                if(parAccion.equals("EDITA_ANTERIOR")) {parTrama = tramaAnterior(parTrama, parCuentas, req); pant=1;}
                if(parAccion.equals("EDITA_SIGUIENTE")) {parTrama = tramaSiguiente(parTrama, parCuentas, req); pant=1;}
                if(parAccion.equals("COPIA")) {if(session.getFacultad(session.FacCopEstr)) pant=2; else setMensError(":-/Usted no tiene facultad para copiar estructuras", req);}
                if(parAccion.equals("COPIA_CON")) {if(session.getFacultad(session.FacCopEstr)) parCuentas = tramaConcentracion(req);}
                if(parAccion.equals("COPIA_FON")) {if(session.getFacultad(session.FacCopEstr)) parCuentas = tramaFondeo(req);}
                if(parAccion.equals("BORRA_ARBOL")) {if(session.getFacultad(session.FacActParam)) {borraArbol(req); parCuentas = consultaArbol(req);} else setMensError(":-/Usted no tiene facultad para modificar par�metros",req);}
                if(parAccion.equals("ALTA_ARBOL")) {if(session.getFacultad(session.FacActParam)) altaArbol(parCuentas, req); else setMensError(":-/Usted no tiene facultad para modificar par�metros",req);}
                if(parAccion.equals("MODIFICA_ARBOL")) {if(session.getFacultad(session.FacActParam)) modificaArbol(parCuentas, req); else setMensError(":-/Usted no tiene facultad para modificar par�metros",req);}
                if(parAccion.equals("IMPORTA")) {parCuentas = importaDatos(req, parCuentas);}
                if(getMensError(req).length()>=3) if(getMensError(req).substring(0,3).equals("X-P")) pant = 3;

                req.setAttribute("MenuPrincipal", session.getStrMenu());
                req.setAttribute("newMenu", session.getFuncionesDeMenu());
                //req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de dispersi&oacute;n","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Dispersi&oacute;n","s29010h", req));
                req.setAttribute("Cuentas",parCuentas);
                req.setAttribute("Mensaje",getMensError(req));
                req.setAttribute("Trama",parTrama);
                req.setAttribute("CtasConcentracion",tramaCtasCon(req));
                req.setAttribute("URL","TIDispercion");


                //utilizar switch
                //TODO BIT Inicia el flujo
                //TODO BIT CU3155 Elimina registros
                //TODO BIT CU3154 Copia estructuras
                //				  Con la opcion COPIA_CON se copia la estructura de dispercion a Concentracion
                //				  Con la opcion COPIA_FON se copia la estructura de dispercion a Fodeo
                //TODO BIT CU3153 Borra estructura
                //TODO BIT CU3151 Se da de ALTA la Estructura
                //TODO BIT CU3152 Modifica Estructura
                //TODO BIT CU3156 Importa Estructura desde un archivo.
                //Nota ordenar las bitacoras para mejor presentacion del codigo
                /**
                 * VSWF - FVC - I
                 * 17/Enero/2007
                 */
                if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                int opcion = 0;
                if(parAccion.equals(""))
                	opcion = 0;
                if(parAccion.equals("AGREGA"))
                	opcion = 2;
                if(parAccion.equals("ELIMINA"))
                	opcion = 5;
                if(parAccion.equals("EDITA"))
                	opcion = 2;
                if(parAccion.equals("EDITA_MODIFICA"))
                	opcion = -1;
                if(parAccion.equals("EDITA_ANTERIOR"))
                	opcion = -1;
                if(parAccion.equals("EDITA_SIGUIENTE"))
                	opcion = -1;
                if(parAccion.equals("COPIA"))
                	opcion = -1;
                if(parAccion.equals("COPIA_CON"))
                	opcion = 4;
                if(parAccion.equals("COPIA_FON"))
                	opcion = 4;
                if(parAccion.equals("BORRA_ARBOL"))
                	opcion = 3;
                if(parAccion.equals("ALTA_ARBOL"))
                	opcion = 1;
                if(parAccion.equals("IMPORTA"))
                	opcion = 6;

                try {
                	BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					if(opcion == 0){
						if(req.getParameter(BitaConstants.FLUJO) != null)
							bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
						else
							bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					}
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());

					switch (opcion) {
					case 0:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_ENTRA);
						//bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;
					case 1:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ALTA_ARBOL);
						bt.setServTransTux("TI01");
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;
					case 2:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_MODIF_ARBOL);
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						bt.setServTransTux("TI04");
						EIGlobal.mensajePorTrace("Cuenta  "+ cuentaTemp, EIGlobal.NivelLog.INFO);
						if(parAccion.equals("EDITA")){
				  		 if(cuentaTemp !=null && !cuentaTemp.equals(""))
							{
							   if(cuentaTemp.length()> 20)
								{
								   bt.setCctaOrig(cuentaTemp.substring(0,20));
								}
								else
								{
						  		 bt.setCctaOrig(cuentaTemp);
								}
							}
						}
						if(parAccion.equals("AGREGA")){
				  		 if(cuentaTemp !=null && !cuentaTemp.equals(""))
							{
							if(cuentaTemp.startsWith("@") && cuentaTemp.indexOf('|') > 0 && cuentaTemp.indexOf('|') < 20)
					  	 		bt.setCctaOrig(cuentaTemp.substring(1,cuentaTemp.indexOf('|')));
							}
						}
					/*	if(parAccion.equals("AGREGA"))
							bt.setCctaOrig((parTrama == null)?" ":parTrama.substring(1,parTrama.indexOf("|")));
						else//Edita
							bt.setCctaOrig((parTrama == null)?" ":parTrama);*/
						break;
					case 3:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_BORRAR_ARBOL);
						bt.setServTransTux("TI03");
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;
					case 4:
						if(parAccion.equals("COPIA_CON"))
							bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_CONCENT);
						else
							bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_FONDEO);
						bt.setServTransTux("TI02");
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;
					case 5:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ELIMINA_REGISTROS_CONCENT);
						bt.setCctaOrig((parTrama == null)?" ":parTrama);
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;
					case 6:
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_IMPORTA_DATOS);
						//bt.setNombreArchivo(new String(getFileInBytes(req)));
						bt.setNombreArchivo(getFormParameter2(req,"fileName"));
						bt.setTipoMoneda((tipoMoneda == null)?" ":tipoMoneda);
						break;

					default:

						break;
					}

					if(opcion != -1)
						BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
                }
                /**
                 * VSWF - FVC - F
                 */
                switch(pant)
                        {
                        case 0: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de dispersi&oacute;n","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Dispersi&oacute;n","s29010h", req));
								evalTemplate("/jsp/TIDispersion.jsp",req,res);
								break;
                        case 1: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de dispersi&oacute;n","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Dispersi&oacute;n","s29090h", req));
								evalTemplate("/jsp/TIEditaDispersion.jsp",req,res);
								break;
                        case 2: req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de dispersi&oacute;n","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Dispersi&oacute;n","s29100h", req));
								evalTemplate("/jsp/TICopiaDispersion.jsp",req,res);
		  						break;
                        case 3: despliegaMensaje(getMensError(req).substring(3),"Consulta y mantenimiento de dispersi&oacute;n","Tesorer&iacute;a &gt; Tesorer&iacute;a inteligente &gt; Mantenimiento de estructuras &gt; Dispersi&oacute;n", req, res); break;
                        }

                debug(" Saliendo de defaultAction()");
                }

        // --- Agrega una cuenta -----------------------------------------------------------
        private String agregaCuenta(String trama, String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a agregaCuenta()");

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                String cuenta = trama.substring(1,trama.indexOf("|"));
                if(!session.getFacultad(session.FacCapDisper))
                        {
                        setMensError(":-/Usted no tiene facultad para capturar datos",req);
                        debug("Saliendo de agregaCuenta()");
                        return tramaCuentas;
                        }
                if(existeCuenta(cuenta,tramaCuentas))
                        {
                        setMensError(":-/La cuenta ya existe dentro del arbol, no puede agregarse nuevamente",req);
                        debug("Saliendo de agregaCuenta()");
                        return tramaCuentas;
                        }
                else
                        {
                        debug("Saliendo de agregaCuenta()");
                        return ordenaArbol(trama + tramaCuentas, req);
                        }
                }

        // --- Elimina una cuenta de la trama de cuentas -----------------------------------
        private String eliminaCuenta(String cuenta, String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a eliminaCuenta()");
                Vector cuentas;
                TI_CuentaDispersion cta;
                int a;
                boolean consistente;

                cuentas = creaCuentas(tramaCuentas,req);
                for(a=0;a<cuentas.size();a++)
                        {
                        cta = (TI_CuentaDispersion)cuentas.get(a);
                        if(cta.getNumCta().equals(cuenta)) cuentas.remove(cta);
                        }
                do
                        {
                        consistente = true;
                        for(a=0;a<cuentas.size();a++)
                                {
                                cta = (TI_CuentaDispersion)cuentas.get(a);
                                if(cta.getPadre() != null)
                                        if(cuentas.indexOf(cta.getPadre()) == -1)
                                                {consistente = false; cuentas.remove(cta);}
                                }
                        }
                while(!consistente);
                debug("Saliendo de eliminaCuenta()");
                return creaTrama(cuentas);
                }

        // --- Regresa la trama de la cuenta indicada --------------------------------------
        private String tramaCuenta(String cuenta, String tramaCuentas)
                {
                debug("Entrando a tramaCuenta()");
                StringTokenizer tokens;
                String tramaCta;
                String numCta;
                String tramaFinal="";
                try
                        {
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens())
                                {
                                tramaCta = tokens.nextToken();
                                numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
                                if(numCta.equals(cuenta)) tramaFinal = "@" + tramaCta;
                                }
                        }
                catch(Exception e)
                        {debug("Error en tramaCuenta(): " + e.toString());}
                finally
                        {
                        debug("Saliendo de tramaCuenta()");
                        return tramaFinal;
                        }
                }

        // --- Edita la trama con la nueva informacion -------------------------------------
        private String tramaModificaCuenta(String tramaCuenta, String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a tramaModificaCuenta()");
                StringTokenizer tokens;
                String tramaCta;
                String numCta;
                String cuenta;
                String tramaFinal="";

                tramaCuenta = tramaCuenta.substring(1);
                cuenta = tramaCuenta.substring(0,tramaCuenta.indexOf("|"));
                try
                        {
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens())
                                {
                                tramaCta = tokens.nextToken();
                                numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
                                if(numCta.equals(cuenta)) tramaCta = tramaCuenta;
                                tramaFinal += "@" + tramaCta;
                                }
                        setMensError(":-)Los datos fueron modificados",req);
                        }
                catch(Exception e)
                        {debug("Error en tramaModificaCuenta(): " + e.toString());}
                finally
                        {
                        debug("Saliendo de tramaModificaCuenta()");
                        return tramaFinal;
                        }
                }

        // --- Regresa la trama de la cuenta siguiente a la indicada -----------------------
        private String tramaSiguiente(String cuenta, String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a tramaSiguiente()");
                StringTokenizer tokens;
                String tramaCta;
                String numCta;
                String ctaSiguiente;
                String cta;
                boolean encontrado;

                encontrado = false;
                cta = "";
                ctaSiguiente = "";
                try
                        {
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens() && !encontrado)
                                {
                                tramaCta = tokens.nextToken();
                                ctaSiguiente = tramaCta.substring(0,tramaCta.indexOf("|"));
                                if(cta.equals(cuenta)) encontrado = true; else cta = ctaSiguiente;
                                }
                        if(!encontrado) {setMensError(":-/no existe cuenta siguiente",req); ctaSiguiente = cuenta;}
                        }
                catch(Exception e)
                        {
                        debug("Error en tramaSiguiente(): " + e.toString());
                        ctaSiguiente = cuenta;
                        }
                finally
                        {
                        debug("saliendo de tramaSiguiente()");
                        return tramaCuenta(ctaSiguiente,tramaCuentas);
                        }
                }

        // --- Regresa la trama de la cuenta siguiente a la indicada -----------------------
        private String tramaAnterior(String cuenta, String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a tramaAnterior()");
                StringTokenizer tokens;
                String tramaCta;
                String numCta;
                String ctaAnterior;
                String cta;
                boolean encontrado;

                encontrado = false;
                cta = "";
                ctaAnterior = "";
                try
                        {
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens() && !encontrado)
                                {
                                tramaCta = tokens.nextToken();
                                cta = tramaCta.substring(0,tramaCta.indexOf("|"));
                                if(cta.equals(cuenta)) encontrado = true; else ctaAnterior = cta;
                                }
                        if(!encontrado || ctaAnterior.equals(""))
                                {setMensError(":-/no existe cuenta anterior", req); ctaAnterior = cuenta;}
                        }
                catch(Exception e)
                        {
                        debug("Error en tramaAnterior(): " + e.toString());
                        ctaAnterior = cuenta;
                        }
                finally
                        {
                        debug("Saliendo de tramaAnterior()");
                        return tramaCuenta(ctaAnterior,tramaCuentas);
                        }
                }

        // --- Indica si una cuenta se encuentra dentro de una trama -----------------------
        private boolean existeCuenta(String cuenta, String tramaCuentas)
                {
                debug("Entrando a existeCuenta()");
                StringTokenizer tokens;
                String tramaCta;
                String numCta;
                String tramaFinal="";
                boolean existe = false;
                try
                        {
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens())
                                {
                                tramaCta = tokens.nextToken();
                                numCta = tramaCta.substring(0,tramaCta.indexOf("|"));
                                if(numCta.equals(cuenta)) {existe = true; break;}
                                }
                        }
                catch(Exception e)
                        {debug("Error en existeCuenta(): " + e.toString());}
                finally
                        {
                        debug("Saliendo de existeCuenta()");
                        return existe;
                        }
                }

        // --- Ordena las cuentas seg�n el arbol -------------------------------------------
        private String ordenaArbol(String tramaEntrada, HttpServletRequest req)
                {
                debug("Entrando a ordenaArbol()");
                String tramaSalida;
                Vector cuentas, ctasOrden;
                TI_CuentaDispersion ctaAux;
                int a, b;

                b = 1;
                cuentas = creaCuentas(tramaEntrada,req);
                ctasOrden = new Vector();
                while(cuentas.size() > 0)
                        {
                        for(a=0;a<cuentas.size();a++)
                                {
                                ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
                                if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
                                }
                        b++;
                        }
                cuentas = ctasOrden;
                ctasOrden = new Vector();
                for(a=0;a<cuentas.size();a++)
                        {
                        ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
                        if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
                        }
                while(cuentas.size() > 0)
                        {
                        ctaAux = ((TI_CuentaDispersion)cuentas.get(0));
                        ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
                        cuentas.remove(ctaAux);
                        }
                cuentas = ctasOrden;
                tramaSalida = "";
                for(a=0;a<cuentas.size();a++) tramaSalida += ((TI_CuentaDispersion)cuentas.get(a)).trama();

                debug("Saliendo de ordenaArbol()");
                return tramaSalida;
                }

        // --- Regresa un vector de cuentas a partir de una trama --------------------------
        public Vector creaCuentas(String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a creaCuentas()");
                Vector cuentas = null;
                StringTokenizer tokens;
                TI_CuentaDispersion ctaAux1, ctaAux2;
                int a, b;

                try
                        {
                        cuentas = new Vector();
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens())
                                {
                                cuentas.add(TI_CuentaDispersion.creaCuentaDispersion("@" + tokens.nextToken()));
                                for(a=0;a<cuentas.size();a++)
                                        {
                                        ctaAux1 = (TI_CuentaDispersion)cuentas.get(a);
                                        if(!ctaAux1.posiblePadre.equals(""))
                                                for(b=0;b<cuentas.size();b++)
                                                        {
                                                        ctaAux2 = (TI_CuentaDispersion)cuentas.get(b);
                                                        if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
                                                                {
                                                                ctaAux1.setPadre(ctaAux2);
                                                                b=cuentas.size();
                                                                }
                                                        }
                                        }
                                }
                        }
                catch(Exception e)
                        {
                        debug("Error en creaCuentas(): " + e.toString());
                        cuentas = new Vector();
                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
                        }
                finally
                        {
                        debug("Saliendo de creaCuentas()");
                        return cuentas;
                        }
                }

        // --- Regresa una trama a partir de un vector de cuentas --------------------------
        public String creaTrama(Vector vectorCuentas)
                {
                debug("Entrando a creaTrama()");
                StringBuffer regreso = new StringBuffer("");
                for(int a=0;a<vectorCuentas.size();a++)
                        {regreso.append(((TI_CuentaDispersion)vectorCuentas.get(a)).trama());}
                debug("Saliendo de creaTrama()");
                return regreso.toString();
                }



         // --- Indica si las cuentas de una estructura tienen todos sus datos capturados ---
        private boolean tramaLista(String trama, HttpServletRequest req)
                {
                debug("Entrando a tramaLista()");
                Vector cuentas = creaCuentas(trama, req);
                TI_CuentaDispersion cuenta;
                boolean retorno = true;
                int num;

                for(num=0;num<cuentas.size() && retorno;num++)
                        {
                        cuenta = (TI_CuentaDispersion)cuentas.get(num);
                        if(cuenta.getHorarios().size() == 0)
                                if(!(cuenta.nivel() == 1 && cuenta.getTipo() != 2 && cuenta.getTipo() != 3))
                                        retorno = false;
                        }
                debug("Saliendo de tramaLista()");
                return retorno;
                }


        private boolean validaCuentas(String trama, HttpServletRequest req,BaseResource session)
        { debug("trama-->"+trama);
        boolean retorno=true;


            ValidaCuentaContrato valCtas = null;
            try
             {
          	//YHG Agrega validacion de cuentas
          	  valCtas = new ValidaCuentaContrato();
            	   String []tramaCompleta=trama.split("@"); //separa tramas

            	  for(int i=0;i<tramaCompleta.length;i++)//ciclo de validacion de cuentas
            	  { EIGlobal.mensajePorTrace("tramaCompleta["+i+"] ..... " + tramaCompleta[i], EIGlobal.NivelLog.INFO);
            	    EIGlobal.mensajePorTrace("tramaCompleta["+i+"].indexOf('|') ..... " + (tramaCompleta[i].indexOf("|")), EIGlobal.NivelLog.INFO);
            	    if(tramaCompleta[i].indexOf("|")>-1){
	            		  String cuentaValidar=tramaCompleta[i].substring(0,tramaCompleta[i].indexOf("|"));
	            		  EIGlobal.mensajePorTrace("TIDispersion -  cuentaValidar:"+cuentaValidar+"<--", EIGlobal.NivelLog.INFO);

	            		  String datoscta[] = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
	            				  											session.getUserID8(), 
	            				                                            session.getUserProfile(),
	            				  											 cuentaValidar.trim(),
	            				  											 IEnlace.MMant_estruc_TI);
	     				 EIGlobal.mensajePorTrace("TIDispersion - datoscta: "+datoscta, EIGlobal.NivelLog.INFO);


	     				 if(datoscta==null){
	     					retorno=false;
	     					EIGlobal.mensajePorTrace("TIDispersion modificaEstructura- Entra a Break y rompe ciclo", EIGlobal.NivelLog.INFO);
	     					break;
	     				 }
            	    }
            	  }
            	  EIGlobal.mensajePorTrace("TIDispersion - retorno-->"+retorno, EIGlobal.NivelLog.INFO);
            	  return retorno;


             }

            catch (Exception e) {e.printStackTrace();}
            finally
    		{
    	    	try{
    	    		valCtas.closeTransaction();
    	    	}catch (Exception e) {
    	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
    			}
    		}
        return retorno;
        }

        private String[][] arregloCuentas(HttpServletRequest req)
                {
                debug("Entrando a arregloCuentas()");
                int a, b, car;
                String trama = "", retorno[][] = null;
                Vector cuentasR;
                TI_CuentaDispersion cuenta = null;

                // parche de Ramopn Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                try
                        {
                        String arregloCuentas[][] = (String[][])sess.getAttribute("ArregloCuentas");
                        if(arregloCuentas == null)
                                {
                                trama = enviaTuxedo(" ",req);
                                cuentasR = datosCuentas(trama, req);
                                retorno = new String[cuentasR.size()][3];
                                for(a=0;a<retorno.length;a++)
                                        {
                                        cuenta = (TI_CuentaDispersion)cuentasR.get(a);
                                        retorno[a][0] = cuenta.getNumCta();
                                        retorno[a][1] = cuenta.getDescripcion();
                                        retorno[a][2] = ""+cuenta.getTipo();
                                        }
                                sess.setAttribute("ArregloCuentas",retorno);
                                }
                        else
                                retorno = arregloCuentas;
                        }
                catch(Exception e)
                        {
                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req);
                        debug("Error en arregloCuentas():" + e.toString() + " -- trama devuelta: " + trama);
                        retorno = null;
                        }
                finally
                        {
                        debug("Saliendo de arregloCuentas()");
                        return retorno;
                        }
                }

        // --- Lee el archivo especificado y obtiene datos de cuentas ----------------------
        private Vector datosCuentas(String archivo, HttpServletRequest req)
                {
                debug("Entrando a datosCuentas()");
                int car;
                FileReader fuente;
                StringBuffer buffer;
                StringTokenizer separaTrama;
                Vector datos = null;
                TI_CuentaDispersion cuenta;

                try
                        {
                        fuente = new FileReader(archivo);
                        buffer = new StringBuffer("");
                        while((car = fuente.read()) != -1) {if(car != 10 && car != 13) buffer.append("" + (char)car);}
                        fuente.close();
                        while((car = buffer.toString().indexOf("@@")) != -1) buffer.insert(car+1," ");
                        separaTrama = new StringTokenizer(buffer.toString(),"@");
                        datos = new Vector();
                        while(separaTrama.hasMoreTokens())
                                {
                                cuenta = new TI_CuentaDispersion(separaTrama.nextToken());
                                separaTrama.nextToken();
                                cuenta.setDescripcion(separaTrama.nextToken());
                                separaTrama.nextToken(); separaTrama.nextToken(); separaTrama.nextToken();
                                cuenta.setTipo(Integer.parseInt(separaTrama.nextToken()));
                                separaTrama.nextToken();
                                datos.add(cuenta);
                                }
                        }
                catch(Exception e)
                        {
                        datos = null;
                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
                        debug("Error en datosCuentas(): " + e.toString());
                        }
                finally
                        {
                        debug("Saliendo de datosCuentas()");
                        return datos;
                        }
                }

        // --- Ejecuta una consulta a Tuxedo y regresa el aviso de Tuxedo ------------------
        private String enviaTuxedo(String tipo, HttpServletRequest req)
                {
                debug("Entrando a enviaTuxedo()");
                String trama = "";
                Hashtable ht;
                ServicioTux tuxedo = new ServicioTux();
                ArchivoRemoto archRemoto = new ArchivoRemoto();
                boolean criterio = false;

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");


// ----------------------------------------------------- Seccion de archivo remoto -----
                        if(tipo.equals("ALTA") || tipo.equals("MODIFICACION"))
                                {
                                if(archRemoto.copiaLocalARemoto(session.getUserID8()+".tei"))  
                                        debug("Copia remota realizada (local a Tuxedo)");
                                else
                                        {
                                        debug("Copia remota NO realizada (local a Tuxedo)");
                                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
                                        }
                                }
// -------------------------------------------------------------------------------------


				//String Divisa = req.getParameter("hdnDivisa")!=null?req.getParameter("hdnDivisa"):"ALL";
			   //CCB - Jan212004 - Variable para trama para manejo de USD / MN
//		        String Divisa = req.getParameter("hdnDivisa");
				String Divisa = (String)sess.getAttribute("DivisaDispersion");
				if((Divisa==null || Divisa.equals("")) && tipo.equals("CONSULTA")){
					Divisa="ALL";
				}
				 /*   if (sess.getAttribute("ctasUSD").equals("Existe"))
				    {
				  	  if(sess.getAttribute("ctasMN").equals("Existe"))
				  	  {
				 		Divisa = "MN";
				  	  }
				  	  else
				  	  {
				   		Divisa = "USD";
				  	  }
				    }
				    else
				    {
				  	  Divisa="MN";
				    }
			    }	*/

				if(tipo.equals("ALTA"))
					{trama = "1EWEB|"+session.getUserID8()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei@|"+Divisa+"|";}
				else if(tipo.equals("BAJA"))
					{trama = "1EWEB|"+session.getUserID8()+"|TI03|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@|"+Divisa+"|";}
				else if(tipo.equals("CONSULTA"))
					{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@|"+Divisa+"|";}
				else if(tipo.equals("MODIFICACION"))
					{trama = "1EWEB|"+session.getUserID8()+"|TI04|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei@|"+Divisa+"|";}
				else if(tipo.equals("CONCENTRACION"))
					{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@|"+Divisa+"|";}
				else if(tipo.equals("FONDEO"))
					{trama = "2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@F@|"+Divisa+"|";}
				else
					//{trama = session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID()+".ctas" + "|6@|" + tipo + "| | |"; criterio = true;}
					{trama = session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID8()+".ctas" + "|6@|" + tipo + "| | | |"+Divisa+"|"; criterio = true;}

                /*if(tipo.equals("ALTA"))
                        {trama = "1EWEB|"+session.getUserID()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID()+".tei@";}
                else if(tipo.equals("BAJA"))
                        {trama = "1EWEB|"+session.getUserID()+"|TI03|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@";}
                else if(tipo.equals("CONSULTA"))
                        {trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@D@";}
                else if(tipo.equals("MODIFICACION"))
                        {trama = "1EWEB|"+session.getUserID()+"|TI04|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID()+".tei@";}
                else if(tipo.equals("CONCENTRACION"))
                        {trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@C@";}
                else if(tipo.equals("FONDEO"))
                        {trama = "2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@F@";}
                else
                        {trama = session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+session.getUserID()+".ctas" + "|6@|" + tipo + "| | |"; criterio = true;}
                */
                try
                        {
                        //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
                        if(!criterio) {ht = tuxedo.web_red(trama);} else {ht = tuxedo.cuentas_modulo(trama);}
                        trama = (String)ht.get("BUFFER");
                        debug("M�todo usado: " + ((!criterio)?"web_red()":"cuentas_modulo()"));
                        debug("Tuxedo regresa en BUFFER: " + trama);
                        debug("Tuxedo regresa en COD_ERROR: " + (String)ht.get("COD_ERROR"));
                        if(criterio)
                                if (!((String)ht.get("COD_ERROR")).equals("CCTA0000"))
                                        {
                                        criterio = false;
                                        trama = (String)ht.get("COD_ERROR");
                                        //setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
                                        //debug("Error en consulta a Tuxedo");
                                        }
                                else
                                        trama = session.getUserID8() + ".ctas"; 
// ----------------------------------------------------- Seccion de archivo remoto -----
                        if(tipo.equals("CONSULTA") || tipo.equals("CONCENTRACION") || tipo.equals("FONDEO") || criterio)
                                {
                                trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
                                if(archRemoto.copia(trama))
                                        {
                                        debug("Copia remota realizada (Tuxedo a local)");
                                        trama = Global.DIRECTORIO_LOCAL + "/" +trama;
                                        }
                                else
                                        {
                                        debug("Copia remota NO realizada (Tuxedo a local)");
                                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
                                        }
                                }
// -------------------------------------------------------------------------------------
                        }
                catch(Exception e)
                        {
                        debug("Error intentando conectar a Tuxedo: " + e.toString() + " <---> " + e.getMessage() +  " <---> " + e.getLocalizedMessage());
                        setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);
                        }
                finally
                        {
                        debug("Saliendo de enviaTuxedo()");
                        return trama;
                        }
                }

        // --- Construye el archivo para enviar a Tuxedo -----------------------------------
        private boolean construyeArchivo(String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a construyeArchivo()");
                boolean noError = true;
                FileWriter archivo;
                StringBuffer buffer, tramaHijoPadre, tramaCuentasHoras;
                Vector cuentas, horarios;
                int numHijoPadre, numCuentasHoras, a, b;
                TI_CuentaDispersion cuenta;

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                try
                        {
                        cuentas = creaCuentas(tramaCuentas, req);
                        buffer = new StringBuffer("" + session.getContractNumber() + "@D@");
                        numHijoPadre = 0;
                        numCuentasHoras = 0;
                        tramaHijoPadre = new StringBuffer("");
                        tramaCuentasHoras = new StringBuffer("");
                        for(b=0;b<cuentas.size();b++)
                                {
                                cuenta = (TI_CuentaDispersion)cuentas.get(b);
                                numHijoPadre++;
                                tramaHijoPadre.append(cuenta.getNumCta() + "@"
                                                                +       ((cuenta.getPadre() == null)?"0":cuenta.getPadre().getNumCta()) + "@"
                                                                + cuenta.getDescripcion() + "@");
                                horarios = cuenta.getHorarios();
                                if(horarios.size() > 0)
                                        for(a=0;a<horarios.size();a++)
                                                {
                                                numCuentasHoras++;
                                                tramaCuentasHoras.append(cuenta.getNumCta() + "@"
                                                                + cuenta.getPeriodicidad() + "@"
                                                                + cuenta.nivel() + "@"
                                                                + cuenta.getImporte() + "@"
                                                                + ((cuenta.getLoConcentrado())?"S":"N") + "@"
                                                                + (String)horarios.get(a) + "@");
                                                }
                                }
                        buffer.append("" + numHijoPadre + "@" + tramaHijoPadre
                                                        + numCuentasHoras + "@" + tramaCuentasHoras);
                        buffer.insert(0," " + buffer.toString().length() + "@\n");
                        buffer.append("\n");
                        archivo = new FileWriter(IEnlace.DOWNLOAD_PATH+session.getUserID8()+".tei");
                        archivo.write("" + buffer);
                        archivo.close();
                        }
                catch(Exception e)
                        {
                        debug("Error intentando construir archivo: " + e.toString() + " <---> " + e.getMessage());
                        noError = false;
                        }
                finally
                        {
                        debug("Saliendo de construyeArchivo()");
                        return noError;
                        }
                }

        // --- Lee el archivo de tuxedo y regresa una trama de cuentas ---------------------
        private String leeArchivo(int sesionaDivisaDispersion, String nombreArchivo, HttpServletRequest req)
                {
                debug("Entrando a leeArchivo()");
				// Sesion para la divisa vcl
				HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");


                StringBuffer tramaT = new StringBuffer(""), tramaR = new StringBuffer("");
                StringTokenizer tokens;
                FileReader archivo;
                String tipoArbol, strAux;
				String Divisa="";
                Vector cuentas;
                TI_CuentaDispersion cuenta;
                int car, a, b;

                try
                        {
                        // Se lee del archivo al StringBuffer
                        archivo = new FileReader(nombreArchivo);
                        car = 0; while(car != -1) {car = archivo.read(); tramaT.append("" + (char)car);}
                        archivo.close();

                        // Se "corrige" la trama
                        while((a = tramaT.toString().indexOf("@@")) != -1) tramaT.insert(a+1," ");

                        // Se arma la trama mientras se verifica su integridad
                        tipoArbol = "";
						EIGlobal.mensajePorTrace ("tramat vcl: " + tramaT.toString(),EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace ("tramat 17-25 vcl: " + tramaT.toString().substring(17,25),EIGlobal.NivelLog.INFO);

                        if(!tramaT.toString().substring(17,25).equals("TEIN0000"))
                                {
									tramaR = tramaT;
									tramaR.insert(0,"<<ERROR>>@");
								}
                        else
                                {
								EIGlobal.mensajePorTrace ("Entro al else vcl",EIGlobal.NivelLog.INFO);
                                tokens = new StringTokenizer(tramaT.toString(),"@");
                                cuentas = new Vector();
                                for(a=0;a<3;a++) tokens.nextToken();
								Divisa = tokens.nextToken();
                                car = Integer.parseInt(tokens.nextToken());

								EIGlobal.mensajePorTrace(" ???????????? La Divisa en Dispersion es : " + Divisa, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace(" ???????????? El car    en Dispersion es : " + car, EIGlobal.NivelLog.INFO);
								if (sesionaDivisaDispersion == 1)
								{
									EIGlobal.mensajePorTrace ("Se sessiona la Divisa vcl:" + Divisa,EIGlobal.NivelLog.INFO);
									req.setAttribute("DivisaDispersion",Divisa);
									//sess.removeAttribute("Divisa");
									sess.setAttribute("DivisaDispersion", Divisa);
								}
                                for(a=0;a<car;a++)
                                        {
                                        tramaR.append("@" + tokens.nextToken() + "|");
                                        tokens.nextToken();
                                        tipoArbol = tokens.nextToken();
                                        strAux = tokens.nextToken();
                                        tramaR.append((strAux.equals("0"))?" ":strAux);
                                        tramaR.append("|" + tokens.nextToken() + "|0|0.0|N|N|0");
                                        }
								EIGlobal.mensajePorTrace("El tipo de arbol en leearchivo para Dispersion es: "+ tipoArbol, EIGlobal.NivelLog.INFO);
                                if(!tipoArbol.equals("D"))
                                        {tramaR.insert(0,"TRAMA_" + tipoArbol + "@");
										EIGlobal.mensajePorTrace ("tramaR vcl: "+ tramaR,EIGlobal.NivelLog.INFO);
										}
                                else
                                        {
									EIGlobal.mensajePorTrace ("entro al 2 else vcl",EIGlobal.NivelLog.INFO);
                                        cuentas = creaCuentas(tramaR.toString(),req);
										//Divisa = tokens.nextToken();
										EIGlobal.mensajePorTrace ("Divisa vcl: "+Divisa,EIGlobal.NivelLog.INFO);
                                        car = Integer.parseInt(tokens.nextToken());
										EIGlobal.mensajePorTrace ("car vcl: "+car,EIGlobal.NivelLog.INFO);

										EIGlobal.mensajePorTrace(" ------Si el tipo de arbol es D La Divisa en Dispersion es : " + Divisa, EIGlobal.NivelLog.INFO);
										EIGlobal.mensajePorTrace(" ------Si el tipo de arbol es D El car    en Dispersion es : " + car, EIGlobal.NivelLog.INFO);
                                        for(a=0;a<car;a++)
                                                {
                                                strAux = tokens.nextToken();
                                                for(b=0; !(cuenta = (TI_CuentaDispersion)cuentas.get(b)).getNumCta().equals(strAux);b++);
                                                if(cuenta.getHorarios().size() > 0)
                                                        {
                                                        for(b=0;b<5;b++) tokens.nextToken();
                                                        cuenta.agregaHorario(tokens.nextToken());
                                                        }
                                                else
                                                        {
                                                        tokens.nextToken();
                                                        cuenta.setPeriodicidad(Integer.parseInt(tokens.nextToken()));
                                                        tokens.nextToken();
                                                        cuenta.setImporte(Double.parseDouble(tokens.nextToken()));
                                                        cuenta.setLoConcentrado(tokens.nextToken().equals("S"));
                                                        cuenta.agregaHorario(tokens.nextToken());
                                                        }
                                                }
                                        tramaR = new StringBuffer("");
                                        for(a=0;a<cuentas.size();a++) tramaR.append(((TI_CuentaDispersion)cuentas.get(a)).trama());
                                        tramaR.insert(0,"TRAMA_OK@");
										req.setAttribute("DivisaDispersion",Divisa);
                                        }
                                }
                        }

                catch(Exception e)
                        {
                        debug("Error en leeArchivo(): " + e.toString());
                        tramaR = new StringBuffer("<<ERROR>>@trama en el archivo: " + tramaT.toString());
                        }
                finally
                        {
                        debug("Saliendo de leeArchivo()");
                        return tramaR.toString();
                        }
                }

        // --- Borra el arbol de la base de datos ------------------------------------------
        private void borraArbol(HttpServletRequest req)
                {
                debug("Entrando a borraArbol()");
                String resultado = enviaTuxedo("BAJA", req);
                if(resultado.substring(16,24).equals("TEIN0000"))
                        {setMensError("X-PLa estructura fue borrada", req);}
                else if(resultado.substring(16,20).equals("TEIN"))
                        {setMensError(":-/" + resultado.substring(25,resultado.length()-1), req);}
                else
                        {setMensError(":-/" + resultado.substring(16,resultado.length()), req);}
                debug("Saliendo de a borraArbol()");
                }

        // --- Da de alta el arbol en la base de datos -------------------------------------
        private void altaArbol(String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a altaArbol()");
                String resultadoTuxedo;
                boolean archivoCreado;

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                if(!validaCuentas(tramaCuentas, req,session))
                {setMensError(":-/Error H701:Cuenta Inv&aacute;lida", req);}
                else if(!tramaLista(tramaCuentas, req))
                        {setMensError(":-/No se puede guardar la estructura, Existen<BR>cuentas a las que se deben agregar datos", req);}
                else if(hayHojasEnPrimerNivel(creaCuentas(tramaCuentas,req)))
                        {setMensError(":-/No se puede guardar la estructura, no puede haber cuentas de primer nivel sin hijas", req);}
                else
                        {
                        archivoCreado = construyeArchivo(tramaCuentas, req);
                        if(!archivoCreado)
                                {setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde", req);}
                        else
                                {
                                resultadoTuxedo = enviaTuxedo("ALTA",req);
                                if(resultadoTuxedo.substring(16,24).equals("TEIN0000"))
                                        {setMensError("X-PLa estructura fue guardada", req);}
                                else if(resultadoTuxedo.substring(16,20).equals("TEIN"))
                                        {setMensError(":-/" +resultadoTuxedo.substring(25,resultadoTuxedo.length()-1), req);}
                                else
                                        {setMensError(":-/" +resultadoTuxedo.substring(16,resultadoTuxedo.length()), req);}
                                }
                        }
                debug("Saliendo de altaArbol()");
                }

        // --- Modifica el arbol en la base de datos ---------------------------------------
        private void modificaArbol(String tramaCuentas, HttpServletRequest req)
                {
                debug("Entrando a modificaArbol()");
                String resultadoTuxedo;
                boolean archivoCreado;

                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");
                if(!validaCuentas(tramaCuentas, req,session))
                	{setMensError(":-/Error H701:Cuenta Inv&aacute;lida", req);}
                else if(!tramaLista(tramaCuentas, req))
                        {setMensError(":-/No se puede guardar la estructura, Existen\ncuentas a las que se deben agregar datos", req);}
                else if(hayHojasEnPrimerNivel(creaCuentas(tramaCuentas,req)))
                        {setMensError(":-/No se puede guardar la estructura, no puede haber cuentas de primer nivel sin hijas", req);}
                else
                        {
                        archivoCreado = construyeArchivo(tramaCuentas, req);
                        if(!archivoCreado)
                                {setMensError("X-PSu transacci&oacute;n no puede ser atendida en este momento,<BR> intente m&aacute;s tarde", req);}
                        else
                                {
                                resultadoTuxedo = enviaTuxedo("MODIFICACION",req);
                                if(resultadoTuxedo.substring(16,24).equals("TEIN0000"))
                                        {setMensError("X-PLa estructura fue guardada", req);}
                                else if(resultadoTuxedo.substring(16,20).equals("TEIN"))
                                        {setMensError(":-/" +resultadoTuxedo.substring(25,resultadoTuxedo.length()-1), req);}
                                else
                                        {setMensError(":-/" +resultadoTuxedo.substring(16,resultadoTuxedo.length()), req);}
                                }
                        }
                debug("Saliendo de modificaArbol()");
                }

        // --- Regresa una trama con el arbol ----------------------------------------------
        private String consultaArbol(HttpServletRequest req)
                {
                debug("Entrando a consultaArbol()");
                String trama;
                String[][] arregloCuentas;
                String retornoDisp, tramaD, avisoD;
                String retornoConc, tramaC, avisoC;
                Vector cuentas;
                TI_CuentaDispersion cuenta;
                int a, b;

                arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
                retornoDisp = enviaTuxedo("CONSULTA", req);

			//***********************************************************************************
				tramaD = leeArchivo(1,retornoDisp,req);
				EIGlobal.mensajePorTrace ("tramaD vcl: " + tramaD,EIGlobal.NivelLog.INFO);

				retornoConc = enviaTuxedo("CONCENTRACION", req);
				tramaC = leeArchivo(0,retornoConc,req);
				EIGlobal.mensajePorTrace ("tramaC vcl: " + tramaC,EIGlobal.NivelLog.INFO);

				if(tramaD.equals("TRAMA_@")) tramaD = "TRAMA_OK@";
                if(tramaC.equals("TRAMA_@")) tramaC = "TRAMA_C@";

                avisoD = tramaD.substring(0,tramaD.indexOf("@"));
				EIGlobal.mensajePorTrace ("avisoD vcl: " + avisoD,EIGlobal.NivelLog.INFO);

				tramaD = tramaD.substring(tramaD.indexOf("@") + 1);
                avisoC = tramaC.substring(0,tramaC.indexOf("@"));
				EIGlobal.mensajePorTrace ("avisoC vcl: " + avisoC,EIGlobal.NivelLog.INFO);

				tramaC = tramaC.substring(tramaC.indexOf("@") + 1);
			//************************************************************************************

				if(!avisoD.equals("TRAMA_OK") || !avisoC.equals("TRAMA_C"))
                        {
                        String aux;
                        aux = ((avisoD.equals("TRAMA_OK"))?tramaC:tramaD);
                        if(aux.substring(17,21).equals("TEIN"))
                                {setMensError("X-P" + aux.substring(25, aux.length()), req);}
                        else
                                {setMensError("X-P" + aux.substring(16, aux.length()-1), req);}

                        debug("Error en consultaArbol() -- tramas devueltas: dispersion: " + tramaD + " -- concentracion: " + tramaC);
                        debug("Saliendo de consultaArbol()");
                        return "";
                        }
                trama = "";
                cuentas = creaCuentas(tramaD,req);
                for(a=0;a<cuentas.size();a++)
                        {
                        cuenta = (TI_CuentaDispersion)cuentas.get(a);
                        cuenta.setConcentracion(existeCuenta(cuenta.getNumCta(),tramaC));
                        for(b=0;b < arregloCuentas.length && !arregloCuentas[b][0].equals(cuenta.getNumCta());b++);
                        cuenta.setTipo(Integer.parseInt((b != arregloCuentas.length)?arregloCuentas[b][2]:"0"));
                        trama += cuenta.trama();
                        }
                debug("Saliendo de consultaArbol()");
                return ordenaArbol(trama, req);
                }

        // --- Regresa una trama con el arbol de concentracion -----------------------------
        private String tramaConcentracion(HttpServletRequest req)
                {
                debug("Entrando a tramaConcentracion()");
                String retornoTuxedo, trama, aviso;
                String[][] arregloCuentas;
                Vector cuentas;
                TI_CuentaDispersion cuenta;
                int a, b;

                retornoTuxedo = enviaTuxedo("CONCENTRACION", req); trama = leeArchivo(0,retornoTuxedo,req);
                if(trama.equals("TRAMA_@")) trama = "TRAMA_C@";
                aviso = trama.substring(0,trama.indexOf("@"));
                trama = trama.substring(trama.indexOf("@") + 1);
                if(!aviso.equals("TRAMA_C"))
                        {
                        if(trama.substring(17,21).equals("TEIN"))
                                {setMensError("X-P" + trama.substring(25, trama.length()), req);}
                        else
                                {setMensError("X-P" + trama.substring(16, trama.length()-1), req);}
                        debug("Error en tramaConcentracion() -- trama devuelta: " + trama);
                        debug("Saliendo de tramaConcentracion()");
                        return "";
                        }

                cuentas = creaCuentas(trama,req);
                arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
                trama = "";
                for(a=0;a<cuentas.size();a++)
                        {
                        cuenta = (TI_CuentaDispersion)cuentas.get(a);
                        for(b=0;b < arregloCuentas.length && !arregloCuentas[b][0].equals(cuenta.getNumCta());b++);
                        cuenta.setTipo(Integer.parseInt((b != arregloCuentas.length)?arregloCuentas[b][2]:"0"));
                        trama += cuenta.trama();
                        }

                debug("Saliendo de tramaConcentracion()");
                return ordenaArbol(trama, req);
                }

        // --- Regresa una trama con el arbol de fondeo ------------------------------------
        private String tramaFondeo(HttpServletRequest req)
                {
                debug("Entrando a tramaFondeo()");
                String retornoTuxedo, trama, aviso;
                String[][] arregloCuentas;
                Vector cuentas;
                TI_CuentaDispersion cuenta;
                int a, b;

                retornoTuxedo = enviaTuxedo("FONDEO",req); trama = leeArchivo(0,retornoTuxedo,req);
                if(trama.equals("TRAMA_@")) trama = "TRAMA_F@";
                aviso = trama.substring(0,trama.indexOf("@"));
                trama = trama.substring(trama.indexOf("@") + 1);
                if(!aviso.equals("TRAMA_F"))
                        {
                        if(trama.substring(17,21).equals("TEIN"))
                                {setMensError("X-P" + trama.substring(25, trama.length()), req);}
                        else
                                {setMensError("X-P" + trama.substring(16, trama.length()-1), req);}
                        debug("Error en tramaFondeo() -- trama devuelta: " + trama);
                        debug("Saliendo de tramaFondeo()");
                        return "";
                        }

                cuentas = creaCuentas(trama,req);
                arregloCuentas = arregloCuentas(req); if (arregloCuentas == null) return "";
                trama = "";
                for(a=0;a<cuentas.size();a++)
                        {
                        cuenta = (TI_CuentaDispersion)cuentas.get(a);
                        for(b=0;b < arregloCuentas.length && !arregloCuentas[b][0].equals(cuenta.getNumCta());b++);
                        cuenta.setTipo(Integer.parseInt((b != arregloCuentas.length)?arregloCuentas[b][2]:"0"));
                        trama += cuenta.trama();
                        }

                debug("Saliendo de tramaFondeo()");
                return ordenaArbol(trama, req);
                }

        // --- Regresa una trama de nombres de cuenta que est�n en concentracion -----------
        private String tramaCtasCon(HttpServletRequest req)
                {
                debug("Entrando a tramaCtasCon()");
                StringTokenizer tramaEntrada = new StringTokenizer(tramaConcentracion(req),"@");
                StringBuffer tramaSalida = new StringBuffer("");
                String strAux;

                try
                        {
                        while(tramaEntrada.hasMoreTokens())
                                {
                                strAux = tramaEntrada.nextToken();
                                tramaSalida.append("@" + strAux.substring(0,strAux.indexOf("|")));
                                }
                        }
                catch(Exception e)
                        {debug("error en tramaCtasCon(): " + e.toString());}
                finally
                        {
                        debug("Saliendo de tramaCtasCon()");
                        return tramaSalida.toString();
                        }
                }

        // --- Indica si una cuenta es hoja o padre ----------------------------------------
        public boolean esHoja(TI_CuentaDispersion cuenta, Vector cuentas)
                {
                debug("Entrando a esHoja()");
                TI_Cuenta ctaActual;
                boolean hoja;

                hoja = true;
                for(int a=0;a<cuentas.size() && hoja;a++)
                        {
                        ctaActual = ((TI_CuentaDispersion)cuentas.get(a)).getPadre();
                        if(ctaActual != null) if(ctaActual.equals(cuenta)) hoja = false;
                        }
                debug("Saliendo de esHoja()");
                return hoja;
                }

        // --- Indica si no hay hojas en el nivel 1 del arbol ------------------------------
        private boolean hayHojasEnPrimerNivel(Vector cuentas)
                {
                debug("Entrando a hayHojasEnPrimerNivel()");
                TI_CuentaDispersion ctaActual;
                boolean hoja;

                hoja = false;
                for(int a=0;a<cuentas.size() && !hoja;a++)
                        {
                        ctaActual = (TI_CuentaDispersion)cuentas.get(a);
                        if(ctaActual.nivel() == 1 && esHoja(ctaActual,cuentas)) hoja = true;
                        }
                debug("Saliendo de hayHojasEnPrimerNivel()");
                return hoja;
                }

        // --- Importa los datos del archivo a importar seleccionado por el usuario --------
        private String importaDatos(HttpServletRequest req, String original)
                {
                debug("Entrando a importaDatos()");
				
                int a, numLinea = 0;
                boolean errorLinea, errorGral;
                String archivo, linea, token, trama = "",
                        htmlLinea, htmlCabecera, htmlCuerpo, htmlPie,
                        otroArchivo,
                        nuevoNombre[];
                BufferedReader flujo;
                Vector cuentas;
                TI_CuentaDispersion cuenta, nuevoPadre;

                htmlCabecera = "<html><head><title>Estatus</title><link rel='stylesheet' href='/EnlaceMig/" +
                                "consultas.css' type='text/css'></head><body bgcolor='white'><form><table border=0" +
                                " width=420 class='textabdatcla' align=center><tr><th class='tittabdat'>informaci&" +
                                "oacute;n del Archivo Importado</th></tr><tr><td class='tabmovtex1' align=center><" +
                                "table border=0 align=center><tr><td class='tabmovtex1' align=center width=><img s" +
                                "rc='/gifs/EnlaceMig/gic25060.gif'></td><td class='tabmovtex1' align='center'><br>" +
                                "No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los" +
                                " siguientes errores.<br><br></td></tr></table></td></tr><tr><td class='tabmovtex1'>";
                htmlCuerpo = "";
                htmlPie = "<br></td></tr></table><table border=0 align=center><tr><td align=center><br><a " +
                                "href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=" +
                                "0></a></td></tr></table></form></body></html>";
                errorGral = false;

                //Se crea una lista con los n�meros de cuenta v�lidos para el contrato
                otroArchivo = enviaTuxedo(" ",req);
                cuentas = new Vector();
                try
                        {
                        // Se lee el archivo, y se procesa para que sus datos se lean desde un BufferedReader
                        archivo = new String(getFileInBytes(req));
                        flujo = new BufferedReader(new StringReader(archivo));
                        numLinea++;

                        // Se obtiene y se valida la cabecera del archivo
                        linea = flujo.readLine().trim();
                        EIGlobal.mensajePorTrace("CCBlinea = " + linea, EIGlobal.NivelLog.INFO);
                        EIGlobal.mensajePorTrace("CCBlinea.length() = " + linea.length(), EIGlobal.NivelLog.INFO);
                        if(linea.length() != 17)
							{htmlCuerpo = "No cumple con el Formato."; throw new Exception();}

                        if(!linea.substring(16,17).equals("D"))
                                {htmlCuerpo = "El arbol no es de dispersi&oacute;n."; throw new Exception();}

                        // Se valida y se va obteniendo la info de cada l�nea
                        while((linea = flujo.readLine()) != null)
                                {
								EIGlobal.mensajePorTrace("Dentro del WHILE ", EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace("Linea >"+linea+"<", EIGlobal.NivelLog.INFO);
                                if(linea.trim().equals("")) continue;
                                numLinea ++;
                                errorLinea = false;
                                htmlLinea = "<br><b>L&iacute;nea " + numLinea + "</b>";

                                if(!linea.substring(0,16).trim().equals("vac�o"))
                                        {
                                        cuenta = new TI_CuentaDispersion(linea.substring(0,16).trim());
                                        if(cuentas.indexOf(cuenta)== -1) cuentas.add(cuenta);
                                        }

                                cuenta = new TI_CuentaDispersion(linea.substring(16,32).trim());
                                if((a = cuentas.indexOf(cuenta)) != -1)
                                        if(((TI_CuentaDispersion)cuentas.get(a)).getHorarios().size() == 0)
                                                {cuentas.remove(a);}

                                if((a = cuentas.indexOf(cuenta)) == -1)
                                        {
                                        cuenta.posiblePadre = linea.substring(0,16).trim();
                                        cuenta.setDescripcion(linea.substring(34,74).trim());
                                        cuenta.setPeriodicidad(Integer.parseInt(linea.substring(74,75)));
                                        cuenta.agregaHorario(linea.substring(75,80));
                                        cuenta.setLoConcentrado(linea.substring(80,81).equals("S"));
                                        cuenta.setImporte(Double.parseDouble(linea.substring(81).trim()));
                                        //yhg comenta codigo
                                        //if(cuenta.getPeriodicidad() != 0) cuenta.setPeriodicidad(cuenta.getPeriodicidad()+1);
                                        //YHG Agrega linea
                                        if(cuenta.getPeriodicidad() != 0) cuenta.setPeriodicidad(cuenta.getPeriodicidad());
                                        if(cuenta.posiblePadre.equals("vac�o")) cuenta.posiblePadre = "";
                                        cuentas.add(cuenta);
                                        }
                                else
                                        {
                                        cuenta = (TI_CuentaDispersion)cuentas.get(a);
                                        cuenta.agregaHorario(linea.substring(75,80));
                                        }

                                nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");
                                if(nuevoNombre == null)
                                        {
                                        errorLinea = true;
                                        htmlLinea +="<br><DD><LI>La cuenta no existe: " + cuenta.getNumCta();
                                        }

                                if(cuenta.posiblePadre.equals(""))
                                        {nuevoNombre = new String[2]; nuevoNombre[1] = "";}
                                else
                                        nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");

                                if(nuevoNombre == null)
                                        {
                                        errorLinea = true;
                                        htmlLinea +="<br><DD><LI>La cuenta padre no existe: " + cuenta.posiblePadre;
                                        }

                                if(cuenta.getPeriodicidad()<0 || cuenta.getPeriodicidad()>6)
                                        {
                                        errorLinea = true;
                                        htmlLinea +="<br><DD><LI>La periodicidad no es correcta: " + cuenta.getPeriodicidad();
                                        }

                                if(cuenta.getImporte()<0)
                                        {
                                        errorLinea = true;
                                        htmlLinea +="<br><DD><LI>El importe de dispersi&oacute;n no es correcto: " + cuenta.getImporte();
                                        }
                                if(errorLinea) {errorGral = true; htmlCuerpo += htmlLinea;}
                                }

                        flujo.close();

                        if(errorGral)
                                {
                                trama = original;
                                setMensError("X-O" + htmlCabecera + htmlCuerpo + htmlPie, req);
                                }
                        else
                                {
                                for(a=0;a<cuentas.size();a++)
                                        {
                                        cuenta = (TI_CuentaDispersion)cuentas.get(a);
                                        nuevoNombre = BuscandoCtaInteg(cuenta.getNumCta(),otroArchivo,"6@");
                                        nuevoPadre = TI_CuentaDispersion.copiaCuenta(nuevoNombre[1], cuenta);
                                        cuentas.set(a,nuevoPadre);
                                        for(int x=0;x<cuentas.size();x++)
                                                if(((TI_CuentaDispersion)cuentas.get(x)).posiblePadre.equals(cuenta.getNumCta()))
                                                        ((TI_CuentaDispersion)cuentas.get(x)).posiblePadre = nuevoPadre.getNumCta();

                                        }

                                trama = "";
                                while(cuentas.size()>0) trama += ((TI_CuentaDispersion)cuentas.remove(0)).tramaPosible();
                                setMensError(":-)Archivo importado satisfactoriamente.", req);
                                }
                        }
                catch(Exception e)
                        {
                        debug("error en importaDatos(): " + e.toString());
                        if(htmlCuerpo.equals("")) htmlCuerpo = "No cumple con el Formato.";
                        htmlCuerpo = "<br><b>Linea: " + numLinea + ".</b> <font color=red>" + htmlCuerpo + "</font>";
                        trama = original;
                        setMensError("X-O" + htmlCabecera + htmlCuerpo + htmlPie, req);
                        }
                finally
                        {
                        debug("Saliendo de importaDatos()");
                        return ordenaArbol(trama, req);
                        }
                }

        //--- Toma el mensaje de error de la sesion ----------------------------------------
        private String getMensError(HttpServletRequest req)
                {
                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                String mensError = (String)sess.getAttribute("TI_MensError"); if(mensError == null) mensError = "";
                return mensError;
                }

        //--- Indica el mensaje de error para guardar en la sesion -------------------------
        private void setMensError(String mensError, HttpServletRequest req)
                {
                // parche de Ramon Tejada
                HttpSession sess = req.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");

                sess.setAttribute("TI_MensError",mensError);
                }

        //--- Mensaje de debugeo -----------------------------------------------------------
        private void debug(String mensaje) {EIGlobal.mensajePorTrace("<DEBUG TIDispersion>" + mensaje, EIGlobal.NivelLog.INFO);}

        /*************************************************************************************/
        /**************************************************************** despliega Mensaje  */
        /*************************************************************************************/
        public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
                {

                // parche de Ramon Tejada
                HttpSession sess = request.getSession();
                BaseResource session = (BaseResource) sess.getAttribute("session");
                EIGlobal Global = new EIGlobal(session , getServletContext() , this );

		EIGlobal.mensajePorTrace("Estoy en despliegaMensaje >" + param1+ "<  ------- >"+param2+"<", EIGlobal.NivelLog.INFO);

                String contrato_ = session.getContractNumber();

                if(contrato_==null) contrato_ ="";

                request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
                request.setAttribute( "Error", mensaje );
                request.setAttribute( "URL", ((getFormParameter(request,"Accion") == null)?"javascript:history.go(-1);":"TIDispersion?Accion=&") );

                request.setAttribute( "MenuPrincipal", session.getStrMenu() );
                request.setAttribute( "newMenu", session.getFuncionesDeMenu());
                request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s29010h", request));
				sess.setAttribute("Encabezado",CreaEncabezado( param1, param2, "s29010h", request));
                request.setAttribute( "NumContrato", contrato_ );
                request.setAttribute( "NomContrato", session.getNombreContrato() );
                request.setAttribute( "NumUsuario", session.getUserID8() );  
                request.setAttribute( "NomUsuario", session.getNombreUsuario() );

                evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
                }

        }