/**Banco Santander Mexicano
* Clase MtoNomina, Carga la plantilla para el mantenimineto del archivo de pagos
* @author Rodrigo Godo
* @version 1.1
* fecha de creacion: Diciembre 2000 - Enero 20001
* responsable: Roberto Guadalupe Resendiz Altamirano
* descripcion: Crea un objeto del tipo TemplateMapBasic, carga un calendario para exhibirse dentro del TemplateMap,
				asi como un combo con la lista de cuentas con las cuales puede operar el usuario,
				valida facultades para saber  que tipo de operaciones pueden efectuarse sobre los archivos de pago de nomina
				(creacion, importacion, altas, bajas, cambios, borrado y recuperacion de archivos,
				asi como la transmision de los pagos que habran de efectuarse).<BR>
* 12/10/2002 Se adecu&oacute; este servlet a los cambios del <I>BaseResource</I> y del manejo de sesi&oacute;n. Modific&oacute; Hugo Ru&iacute;z Zepeda.<BR>
*/

package mx.altec.enlace.servlets;

import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;


import java.io.IOException;

public class SinFacultades extends BaseServlet
{
	String nuevoMenu;

	public void doGet( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void doPost( HttpServletRequest request, HttpServletResponse response )
		throws ServletException, IOException
	{
		defaultAction( request, response );
	}

	public void defaultAction( HttpServletRequest request, HttpServletResponse response )
		throws IOException, ServletException
	{
      HttpSession sesion;
      BaseResource sesionBS;
      EIGlobal enlaceGlobal;
		//EIGlobal.mensajePorTrace( "***SinFacultades.class &Entrando al metodo execute: &", 4 );
		boolean sesionvalida = SesionValida( request, response );

		if (sesionvalida)
      {
			sesion=request.getSession();
			sesionBS =(BaseResource)sesion.getAttribute("session");
			enlaceGlobal=new EIGlobal(sesionBS, getServletContext(), this);

			request.setAttribute("Fecha", ObtenFecha());
			request.setAttribute("ContUser",ObtenContUser(request));
			request.setAttribute("newMenu", sesionBS.getFuncionesDeMenu());
			request.setAttribute("MenuPrincipal", sesionBS.getStrMenu());
			request.setAttribute("Encabezado", CreaEncabezado("Usuario sin facultades",
				"Imposible ejecutar esta operaci&oacute;n", "s29061h", request ) );
			evalTemplate( "/jsp/Sin_Facultades.jsp", request, response );
		}
      else
      {
			request.setAttribute( "MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, request, response );
		}
	} // Fin m�todo defaultAction


} // Fin clase SinFacultades