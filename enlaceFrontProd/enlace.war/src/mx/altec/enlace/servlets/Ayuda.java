package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class Ayuda extends BaseServlet
{
    public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
         defaultAction(req, res);
    }

	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
    {
		EIGlobal.mensajePorTrace("Entrando a ayuda", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("ClaveAyuda = >" +(String)req.getParameter("ClaveAyuda")+ "<", EIGlobal.NivelLog.DEBUG);

		req.setAttribute("ClaveAyuda", req.getParameter("ClaveAyuda"));
        evalTemplate("/jsp/Ayuda.jsp", req, res);
        return;
    }
}
