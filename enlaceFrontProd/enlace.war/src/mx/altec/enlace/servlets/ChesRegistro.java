/**
 * Isban Mexico
 *   Clase: ChesRegistro.java
 *   Descripci�n:
 *
 *   Control de Cambios:
 *   1.0 3/07/2013 Stefanini - Creaci�n
 */

package mx.altec.enlace.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.*;
import java.util.zip.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.SQLException;
import java.text.*;
import javax.naming.*;

import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.ches.ChesArchivoImportado;
import mx.altec.enlace.ches.ChesFacultades;
import mx.altec.enlace.ches.ChesValidacionException;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.MultipartRequest;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 * Registro de Cheques de seguridad por archivo.
 *
 * @author Rafael Martinez Montiel
 * @version 1.0
 */
public class ChesRegistro extends BaseServlet {
	// TODO: falta ponerles el final a las constantes en este bloque
	/**
	 * nombre de la clase para rapida referencia
	 */
	public static final String CLASS_NAME = ChesRegistro.class.getName();

	/**
	 * identificador para copia del directorio local a remoto
	 */
	public static final String LOCAL_A_REMOTO = "1";

	/**
	 * identificador para la copia del diractorio remoto al local
	 */
	public static final String REMOTO_A_LOCAL = "0";

	/**
	 * Formateador para las fechas
	 */
	public static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
			"dd/MM/yyyy");

	/**
	 * Cabecera para el modulo
	 */
	public static final String S_CABECERA = "Servicios &gt; Chequera Seguridad &gt; Registro de Cheques &gt; Por Archivo";

	/**
	 * Nombre del Modulo
	 */
	public static final String S_MODULO = "Registro de Cheque Seguridad";

	/**
	 * Dispatcher por defecto
	 */
	public static final String DEFAULT_DISPATCHER = "/jsp/ches/ChesRegistro.jsp";

	/**
	 * Dispatcher pos validacion de archivo
	 */
	public static final String ARCHREC_DISPATCHER = "/jsp/ches/ChesValidado.jsp";

	/**
	 * despachador para cuando ocurren distintos errores de validacion
	 */
	public static final String MULTERR_DISPATCHER = "/jsp/ches/ChesError.jsp";

	/**
	 * despachador para la respuesta del envio de archivos
	 */
	public static final String ENVRES_DISPATCHER = "/jsp/ches/ChesResultadoEnvio.jsp";

	/**
	 * valor para opcion de recuperar archivo y validar
	 */
	public static final int RECUPERAR = 1;

	/**
	 * valor deopcion para enviar el archivo
	 */
	public static final int ENVIAR = 2;

	/**
	 * llave para el nombre del archivo
	 */
	public static final String FILE_NAME = "Archivo";

	public static final String ARCHIVO_LOCAL = "chesArchivoImportadoLocal";

	/**
	 * llave para la fecha de envio
	 */
	public static final String FECHA_ENVIO = "fechaEnvio";

	/**
	 * llave para el numero de registros
	 */
	public static final String NUMERO_REGISTROS = "numRegistros";

	/**
	 * llave para el archivoImportado que fue enviado
	 */
	public static final String ARCHIVO_ENVIADO = "archivoEnviado";

	/**
	 * llave para el archivo
	 */
	public static final String ARCHIVO = "chesArchivo";

	/**
	 * identificador de la ayuda para el DEFAULT_DISPATCHER
	 * {@see DEFAULT_DISPATCHER}
	 */
	public static final String S_HLP_INICIAL = "s34000h";

	public static final String S_HLP_VALIDADO = "s34001h";

	public static final String S_HLP_RESPUESTA = "s34002h";

	/**
	 * Tipo de entrega para el registro de archivo
	 */
	protected static final String TIPO_ENTREGA_REGISTRO = "2";

	/**
	 * medio de entrega para el registro de archivo
	 */
	protected static final String MEDIO_ENTREGA = "EWEB";

	/**
	 * tipo de operacion para el registro de archivo
	 */
	protected static final String TIPO_OPERACION_REGISTRO = "INGA";

	/**
	 * codigo de error ok para la respuesta del registro de archivo
	 */
	protected static final String COD_ERROR_OK = "CHSN0000";

	// end TODO
	String archBit = "";

	/**
	 * Initializes the servlet.
	 *
	 * @param config
	 *            per spec
	 * @throws ServletException
	 *             per spec
	 */
	public void init(javax.servlet.ServletConfig config)
			throws ServletException {
		super.init(config);

	}

	/**
	 * Destroys the servlet.
	 */
	public void destroy() {

	}

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             per spec
	 * @throws IOException
	 *             per spec
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		EIGlobal
				.mensajePorTrace(CLASS_NAME
						+ ": entrando processRequest()",
						EIGlobal.NivelLog.INFO);

		if (!SesionValida(request, response)) {
			EIGlobal.mensajePorTrace(
					CLASS_NAME + ": saliendo processRequest() - (No Valida)",
					EIGlobal.NivelLog.INFO);
			return;
		}
		BaseResource baseResource = (BaseResource) request.getSession()
				.getAttribute("session");
		if (null == baseResource) {
			EIGlobal.mensajePorTrace(CLASS_NAME + ": baseResource="
					+ baseResource, EIGlobal.NivelLog.INFO);
			request.setAttribute("Error", IEnlace.MSG_PAG_NO_DISP);
			request.getRequestDispatcher("/jsp/EI_Error.jsp").forward(request,
					response);
			EIGlobal.mensajePorTrace(
					CLASS_NAME + ": saliendo processRequest()",
					EIGlobal.NivelLog.INFO);
			return;
		}

		String encabezado = CreaEncabezado(S_MODULO, S_CABECERA, S_HLP_INICIAL,
				request);
		request.setAttribute("encabezado", encabezado);

		if (!baseResource
				.getFacultad(mx.altec.enlace.ches.ChesFacultades.FAC_ENVIO)
				|| !baseResource
						.getFacultad(mx.altec.enlace.ches.ChesFacultades.FAC_ARCHIVO)) {
			// request.getRequestDispatcher ( "jsp/SinFacultades.jsp").forward
			// (request,response);
			despliegaPaginaError(
					"No tiene facultades para realizar esta operaci&oacute;n",
					S_MODULO, S_CABECERA, request, response);
			EIGlobal.mensajePorTrace(
					CLASS_NAME + ": saliendo processRequest()",
					EIGlobal.NivelLog.INFO);
			return;
		}
		baseResource.setModuloConsultar(IEnlace.MAlta_ctas_cheq_seg);

		HttpSession sess = request.getSession();
		String valida = request.getParameter("valida");
		boolean tokenConfirm = false;
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		boolean valBitacora = true;

		int opcion = -1;
		//if (null != request.getParameter("opcion")) {
		if (null != getFormParameter(request, "opcion")) {
			try {
				opcion = Integer.parseInt(getFormParameter(request, "opcion"));

				EIGlobal.mensajePorTrace("���������������������� opcion ->" + opcion, EIGlobal.NivelLog.INFO);
			} catch (NumberFormatException ex) {
				EIGlobal
						.mensajePorTrace(
								CLASS_NAME
										+ ": Error en el par�metro de opcion, deberia ser un n�mero."
										+ ex.getMessage(),
								EIGlobal.NivelLog.DEBUG);
			}
		}

		String requestDispatcher = DEFAULT_DISPATCHER;
		java.util.Enumeration en = request.getParameterNames();

		while (en.hasMoreElements()) {
			String parName = (String) en.nextElement();
			EIGlobal.mensajePorTrace(CLASS_NAME + " par " + parName + "="
					+ request.getParameter(parName), EIGlobal.NivelLog.INFO);
		}
			switch (opcion) {
			case RECUPERAR: {
				requestDispatcher = recuperar(request, response);
				break;
			}
			case ENVIAR: {
				if (validaPeticion(request, response, baseResource, sess, valida)) {
					EIGlobal.mensajePorTrace("Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
				} else {

					///////////////////--------------challenge--------------------------
					String validaChallenge = request.getAttribute("challngeExito") != null
						? request.getAttribute("challngeExito").toString() : "";

					EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

					if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
						EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
						ValidaOTP.guardaParametrosEnSession(request);

						validacionesRSA(request, response);
						return;
					}

					//Fin validacion rsa para challenge
					///////////////////-------------------------------------------------

					if (valida == null) {
						EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
						boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.CHEQUERA_LINEA);

						if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal) {
							EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);

							EIGlobal.mensajePorTrace("TOKEN CHEQUERA ARCHIVO", EIGlobal.NivelLog.INFO);
							request.getSession().setAttribute("plantilla",request.getServletPath());
							request.getSession().removeAttribute("bitacora");
							tokenConfirm = true;
							request.getSession().removeAttribute("mensajeSession");
							ValidaOTP.mensajeOTP(request, "ChesArchivo");

							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
						} else {
							requestDispatcher = enviar(request, response);
							valida = "1";
							valBitacora = false;
						}
					}

			break;

			}
		}
			default: {// Mostrando pagina inicial
				// TODO: BIT CU 4181, El cliente entra al flujo
				/*
				 * VSWF ARR -I
				 */
				if (Global.USAR_BITACORAS.trim().equals("ON")) {
					try {

						BitaHelper bh = new BitaHelperImpl(request, baseResource,
								request.getSession());

						if (request.getParameter(BitaConstants.FLUJO) != null) {
							bh.incrementaFolioFlujo((String) request
									.getParameter(BitaConstants.FLUJO));
						} else {
							bh.incrementaFolioFlujo((String) request.getSession()
									.getAttribute(BitaConstants.SESS_ID_FLUJO));
						}
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean) bh.llenarBean(bt);
						bt
								.setNumBit(BitaConstants.ES_CH_SEG_REG_CHEQUES_ARCHIVO_ENTRA);
						bt.setContrato(baseResource.getContractNumber());

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				/*
				 * VSWF ARR -F
				 */
				break;
			}
			}

			if (valida != null && valida.equals("1")) {
				tokenConfirm = false;
				if (valBitacora)
				{
					try{
						EIGlobal.mensajePorTrace( "*************************Entro c�digo bitacorizaci�n--->", EIGlobal.NivelLog.INFO);


						HttpSession sessionBit = request.getSession(false);

						int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
						String claveOperacion = BitaConstants.CTK_CHEQUE_SEG;
						String concepto = BitaConstants.CTK_CONCEPTO_CHEQUE_SEG;
						String token = "0";
						if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
						{token = request.getParameter("token");}
						String cuenta = "0";
						String importe = sessionBit.getAttribute("importeCheqR").toString().trim();
						String importe1 = importe.substring(0, importe.length()-2);
						String importe2 = importe.substring(importe.length()-2);
						double importeDouble = 0;

						importe = importe1.concat(".").concat(importe2);

						importeDouble = Double.parseDouble(importe);

						EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
						EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);

						String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
					} catch(Exception e) {
						EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);

					}
				}
		        //***********
				requestDispatcher = enviar(request, response);
		}

		EIGlobal.mensajePorTrace("�������������������������� tokenConfirm ->" + tokenConfirm, EIGlobal.NivelLog.INFO);

		if (!tokenConfirm) {
			EIGlobal.mensajePorTrace("�������������������������� entro a hacer con tokenConfirm=false", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("�������������������������� requestDispatcher ->" + requestDispatcher, EIGlobal.NivelLog.INFO);

			request.setAttribute("encabezado", encabezado);
			request.setAttribute("baseResource", baseResource);

			if (null != requestDispatcher && !requestDispatcher.equals("ZIP")) {
				EIGlobal.mensajePorTrace(CLASS_NAME + ": request dispatcher="
						+ requestDispatcher, EIGlobal.NivelLog.INFO);
				request.getRequestDispatcher(requestDispatcher).forward(request,
						response);
			}
			EIGlobal.mensajePorTrace(CLASS_NAME + ": saliendo processRequest() - Final",
					EIGlobal.NivelLog.INFO);

		}
		}

	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             per spec
	 * @throws IOException
	 *             per spec
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             per spec
	 * @throws IOException
	 *             per spec
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return info
	 */
	public String getServletInfo() {
		return "Registro de cheques de seguridad por archivo";
	}

	/**
	 * recupera un archivo enviado por el usuario
	 *
	 * @return request dispatcher a utilizar, null en caso de algun error
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @throws IOException
	 *             pero servlet spec
	 * @throws ServletException
	 *             per servlet spec
	 */
	protected String recuperar(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String requestDispatcher = null;
		String sFile = "";
		boolean errorEnZip = false;

		request.getSession().removeAttribute(ARCHIVO);
		EIGlobal.mensajePorTrace(CLASS_NAME + ": enviar archivo:"
				+ getFormParameter(request, "fileName"), EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace(CLASS_NAME + ": contentLength="
				+ request.getContentLength(), EIGlobal.NivelLog.DEBUG);
		java.io.File errorFile = new File(Global.DIRECTORIO_LOCAL, request
				.getRequestedSessionId()
				+ "error.doc");

		try {
			File fileCtas = new File(IEnlace.LOCAL_TMP_DIR,
					((BaseResource) request.getSession()
							.getAttribute("session")).getUserID8()
							+ ".ambci");
			llamado_servicioCtasInteg(IEnlace.MCredito_empresarial, " ", " ",
					" ", fileCtas.getName(), request);

			//
			// Modificacion para manejar el archivo directo de disco
			//
			// Asignacion dinamica de espacio
			/*EIGlobal.mensajePorTrace("***ChesRegistro.class:  (antes MultipartRequet):  "
					, EIGlobal.NivelLog.INFO);
			MultipartRequest mprFile = new MultipartRequest(request,
					IEnlace.LOCAL_TMP_DIR, 20971520);
			// MultipartRequest mprFile = new MultipartRequest ( request,
			// IEnlace.LOCAL_TMP_DIR,request.getContentLength() );
			EIGlobal.mensajePorTrace("***ChesRegistro.class:  (FileInputStream):  "
					, EIGlobal.NivelLog.INFO);

			java.io.FileInputStream entrada;
			EIGlobal.mensajePorTrace("***ChesRegistro.class:  (antes Enumeracion):  "
					, EIGlobal.NivelLog.INFO);

			java.util.Enumeration files = mprFile.getFileNames();

			// Se obtiene el nombre del archivo enviado
			if (files.hasMoreElements()) {
				String name = (String) files.nextElement();
				EIGlobal.mensajePorTrace("***ChesRegistro.class:  (en Enumeracion - name):  " + name
						, EIGlobal.NivelLog.INFO);
				sFile = mprFile.getFilesystemName(name); // <----
				EIGlobal.mensajePorTrace("***ChesRegistro.class:  (en Enumeracion - sFile):  " + sFile
						, EIGlobal.NivelLog.INFO);

			}*/
			sFile = getFormParameter(request, "fileName");
			EIGlobal.mensajePorTrace("***ChesRegistro.class: -- Archivo:  "
					+ sFile, EIGlobal.NivelLog.INFO);
			archBit = sFile;
			// Si el archivo es .zip se descomprime
			if (sFile.toLowerCase().endsWith(".zip")) {
				EIGlobal.mensajePorTrace(
						"***ChesRegistro.class: El archivo viene en zip",
						EIGlobal.NivelLog.INFO);

				final int BUFFER = 2048;
				BufferedOutputStream dest = null;
				FileInputStream fis = new FileInputStream(IEnlace.LOCAL_TMP_DIR
						+ "/" + sFile);
				ZipInputStream zis = new ZipInputStream(
						new BufferedInputStream(fis));
				ZipEntry entry;
				entry = zis.getNextEntry();

				EIGlobal.mensajePorTrace("***ChesRegistro.class: entry: "
						+ entry, EIGlobal.NivelLog.INFO);
				if (entry == null) {
					EIGlobal
							.mensajePorTrace(
									"***ChesRegistro.class: Entry fue null, archivo no es zip o esta da�ando ",
									EIGlobal.NivelLog.INFO);
					errorEnZip = true;
				} else {
					log("ImportNomina.java::Extrayendo" + entry);
					int count;
					byte data[] = new byte[BUFFER];

					// Se escriben los archivos a disco
					// Nombre del archivo zip
					File archivocomp = new File(IEnlace.LOCAL_TMP_DIR + "/"
							+ sFile);
					// Nombre del archivo que incluye el zip
					sFile = entry.getName();

					FileOutputStream fos = new FileOutputStream(
							IEnlace.LOCAL_TMP_DIR + "/" + sFile);
					dest = new BufferedOutputStream(fos, BUFFER);
					while ((count = zis.read(data, 0, BUFFER)) != -1) {
						dest.write(data, 0, count);
					}
					dest.flush();
					dest.close();

					// Borra el archivo comprimido
					if (archivocomp.exists())
						archivocomp.delete();
					log("ChesRegistro.java::Fin extraccion de: " + entry);
				}
				zis.close();
			}

			if (errorEnZip) {
				despliegaPaginaError(
						" Imposible Importar el archivo, no es un archivo zip o esta da�ado",
						"Pago de N&oacute;mina", "Importando Archivo",
						"s25800h", request, response);
				EIGlobal
						.mensajePorTrace(
								"***ImportNomina.class &primeralinea & Imposible Importar el archivo, no es un archivo zip o esta da�ado.",
								EIGlobal.NivelLog.INFO);
				return "ZIP";
			}

			log("ChesRegistro.java:: NOMBRE de archivo sFile: " + sFile);
			String fileStr = IEnlace.LOCAL_TMP_DIR + "/" + sFile;
			log("ChesRegistro.java:: NOMBRE de archivo fileStr: " + fileStr);

			if (!cambiaFechaLimitePago(fileStr)) {
				despliegaPaginaError(
						" No se llev&oacute; acabo la importaci&oacuten, intente nuevamente.",
						S_MODULO, S_CABECERA, S_HLP_VALIDADO, request, response);
				EIGlobal
						.mensajePorTrace(
								CLASS_NAME
										+ "Error al realizar cambio de fechas - fecha sin limite",
								EIGlobal.NivelLog.INFO);
				return "ZIP";
			}

			EIGlobal.mensajePorTrace(
					"***ChesRegistro.class & Se inicializo el arhivo .... 1.",
					EIGlobal.NivelLog.INFO);
			ChesArchivoImportado archivo = ChesArchivoImportado.parse(fileStr,
					diasInhabiles(), errorFile);
			EIGlobal.mensajePorTrace(
					"***ChesRegistro.class & Se inicializo el arhivo .... 2",
					EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace("���������������������� importe total ->" + archivo.getImporteTotal(), EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("���������������������� no de registros ->" + archivo.getNumRegistros(), EIGlobal.NivelLog.INFO);

			request.getSession().setAttribute("importeCheqR", String.valueOf(archivo.getImporteTotal()));
			request.getSession().setAttribute("registrosCheqR", String.valueOf(archivo.getNumRegistros()));

			/*
			 * //File archivoImportado = new File( Global.DIRECTORIO_LOCAL ,
			 * request.getRequestedSessionId () ); //java.io.FileWriter writer =
			 * new java.io.FileWriter( archivoImportado ); //writer.write (
			 * fileStr ); //writer.close(); //fileStr = null;
			 * //archivo.setFileName( archivoImportado.getName() );
			 */

			File archivoI = null;

			try {
				// Renombra el archivo descargado
				archivoI = new File(IEnlace.LOCAL_TMP_DIR + "/" + sFile);
				if (archivoI.exists()) {
					EIGlobal.mensajePorTrace(
							"***ChesRegistro.class & Se renombra ",
							EIGlobal.NivelLog.INFO);
					archivoI.renameTo(new File(IEnlace.LOCAL_TMP_DIR, request
							.getRequestedSessionId()));
					EIGlobal.mensajePorTrace(
							"***ChesRegistro.class & Nuevo nombre."
									+ archivoI.getName(),
							EIGlobal.NivelLog.INFO);
				} else {
					EIGlobal.mensajePorTrace(
							"***ChesRegistro.class & El archivo no existe.",
							EIGlobal.NivelLog.INFO);
					// throw file not found exception
				}
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(
						"***ChesRegistro.class Ha ocurrido una excepcion en: %ejecutaCargaArchivo()"
								+ e, EIGlobal.NivelLog.INFO);
			}

			EIGlobal.mensajePorTrace(
					"***ChesRegistro.class Se guarda el nombre de archivo "
							+ archivo.getFileName(), EIGlobal.NivelLog.INFO);
			request.getSession().setAttribute(ARCHIVO_LOCAL,
					new File(request.getRequestedSessionId()));
			request.setAttribute(NUMERO_REGISTROS, new Long(archivo
					.getNumRegistros()));

			log("ChesRegistro.java:: NOMBRE archivo.setFileName: " + archivo);

			archivo.getCheques().clear();
			// EIGlobal.mensajePorTrace (CLASS_NAME + ": archivo=" +
			// archivo.toString (), EIGlobal.NivelLog.DEBUG);
			request.setAttribute(FECHA_ENVIO, sdf
					.format(new java.util.GregorianCalendar().getTime()));
			// request.setAttribute ( NUMERO_REGISTROS , new Integer
			// (archivo.getCheques ().size () ));
			request.getSession().setAttribute(ARCHIVO, archivo);
			// EIGlobal.mensajePorTrace ( CLASS_NAME + ": archivo=" +
			// request.getSession ().getAttribute ( ARCHIVO),
			// EIGlobal.NivelLog.DEBUG);
			String encabezado = CreaEncabezado(S_MODULO, S_CABECERA,
					S_HLP_VALIDADO, request);
			request.setAttribute("encabezado", encabezado);
			requestDispatcher = ARCHREC_DISPATCHER;
		} catch (mx.altec.enlace.ches.ChesValidacionException ex) {
			StringBuffer msg = new StringBuffer(
					"El archivo contenia errores y no fue importado.");
			ArchivoRemoto archRem = new ArchivoRemoto();// Modificacion RMM
														// 20030131
			if (!archRem.copiaLocalARemoto(errorFile.getName(), "WEB")) {// Modificacion
																			// RMM
																			// 20030130
				EIGlobal.mensajePorTrace(CLASS_NAME
						+ ": No se pudo copiar el archivo de errores",
						EIGlobal.NivelLog.DEBUG);
			} else {
				request.setAttribute("ErrorFileName", errorFile.getName());
			}

			request.setAttribute("Error", ex.toString());
			request.setAttribute("URL", getServletName());
			requestDispatcher = MULTERR_DISPATCHER;
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(CLASS_NAME + " " + ex.toString(),
					EIGlobal.NivelLog.DEBUG);
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP, S_MODULO, S_CABECERA,
					request, response);
			//ex.printStackTrace();
		}

		return requestDispatcher;
	}

	/**
	 * envia un archivo al backend de Enlace atravez de tuxedo
	 *
	 * @param request
	 *            HttpServletRequest
	 * @param response
	 *            HttpServletResponse
	 * @throws IOException
	 *             per Servlet spec
	 * @throws ServletException
	 *             per Servlet spec
	 * @return requestDispatcher, null si ocurrio algun error
	 */
	protected String enviar(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String requestDispatcher = null;

		/*
		 * vswf
		 */
		BaseResource session = (BaseResource) request.getSession()
				.getAttribute("session");
		/*
		 * vswf
		 */
		ChesArchivoImportado archivo = (ChesArchivoImportado) request
				.getSession().getAttribute(ARCHIVO);
		EIGlobal.mensajePorTrace(CLASS_NAME + ": archivo=" + archivo,
				EIGlobal.NivelLog.INFO);
		if (null == archivo) {
			EIGlobal
					.mensajePorTrace(
							CLASS_NAME
									+ ": el archivo es nulo o se envio exitosamente, redireccionando a pagina de inicio",
							EIGlobal.NivelLog.INFO);
			return DEFAULT_DISPATCHER;
		}

		EIGlobal.mensajePorTrace(CLASS_NAME + ": enviando archivo",
				EIGlobal.NivelLog.INFO);
		ServicioTux servicio = new ServicioTux();
		EIGlobal.mensajePorTrace(CLASS_NAME + ": Servicio tux",
				EIGlobal.NivelLog.INFO);
		// servicio.setContext (
		// ((com.netscape.server.servlet.platformhttp.PlatformServletContext)
		// getServletContext ()).getContext () );
		EIGlobal.mensajePorTrace(CLASS_NAME + ": context",
				EIGlobal.NivelLog.INFO);
		BaseResource baseResource = (BaseResource) request.getSession()
				.getAttribute("session");
		EIGlobal.mensajePorTrace(CLASS_NAME + ": sesion",
				EIGlobal.NivelLog.INFO);
		File file = (File) request.getSession().getAttribute(ARCHIVO_LOCAL);
		EIGlobal.mensajePorTrace(CLASS_NAME + ": archivo que se envia: "
				+ file.getName(), EIGlobal.NivelLog.INFO);
		File remoto = new File(Global.DIRECTORIO_REMOTO_INTERNET, file
				.getName());
		java.io.BufferedWriter writer = null;
		java.io.BufferedReader reader = null;
		try {
			// //writer = new java.io.BufferedWriter (new java.io.FileWriter (
			// file ) );

			// // formatea el contenido del buffer
			// archivo.write ( writer );
			// // guarda el buffer tal cual
			// writer.write ( archivo.getBufferOriginal () );
			// writer.close ();
			// writer = null;
			ArchivoRemoto archRemoto = new ArchivoRemoto();
			boolean enviado = archRemoto.copia(file.getName(), remoto
					.getParent(), LOCAL_A_REMOTO);
			// boolean enviado = envia ( file );
			if (!enviado) {
				throw new Exception("Error al enviar el archivo");
			} else {
				EIGlobal.mensajePorTrace(CLASS_NAME
						+ ": archivo enviado exitosamente",
						EIGlobal.NivelLog.INFO);
				// TODO: BIT CU 4181, A3
				/*
				 * VSWF ARR -I
				 */
				if (Global.USAR_BITACORAS.trim().equals("ON")) {
					try {
						BitaHelper bh = new BitaHelperImpl(request, session,
								request.getSession());
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean) bh.llenarBean(bt);
						bt
								.setNumBit(BitaConstants.ES_CH_SEG_REG_CHEQUES_ARCHIVO_INGRESO_CHEQ_SEGU);
						bt.setContrato(session.getContractNumber());
						bt.setNombreArchivo(archBit);
						bt.setServTransTux(TIPO_OPERACION_REGISTRO);
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				/*
				 * VSWF ARR -F
				 */

			}
			archivo.setFileName(file.getName());
			String trama = generaTramaRegistro(baseResource, remoto, archivo);
			EIGlobal.mensajePorTrace(CLASS_NAME + ": trama envio=\n" + trama,
					EIGlobal.NivelLog.DEBUG);
			File archRetorno = new File(IEnlace.LOCAL_TMP_DIR + "/"
					+ baseResource.getUserID8() + "retChes");
			java.util.Hashtable ret = servicio.web_red(trama);

			if (null == ret) {
				EIGlobal
						.mensajePorTrace(
								CLASS_NAME
										+ ": ERROR el servicio no respondio de manera correcta, retorno null",
								EIGlobal.NivelLog.DEBUG);
				throw new Exception("Error al llamar al servicio. "
						+ IEnlace.MSG_PAG_NO_DISP);
			}

			String tramaRetorno = (String) ret.get("BUFFER");
			EIGlobal.mensajePorTrace(CLASS_NAME + ": trama retorno=\n"
					+ tramaRetorno, EIGlobal.NivelLog.DEBUG);

			// // COMMENTED OUT 20030108 inicio
			// // esta seccion de codigo se comenta o se descomenta dependiendo
			// si el
			// // servicio retorna una trama o un archivo
			//
			// File tmp = new File ( tramaRetorno );
			// tmp.renameTo ( archRetorno );
			//
			// reader = new java.io.BufferedReader ( new java.io.FileReader (
			// archRetorno ) );
			//
			// tramaRetorno = reader.readLine ();
			// EIGlobal.mensajePorTrace ( CLASS_NAME + ": trama en
			// archivoRetorno=\n" + tramaRetorno, EIGlobal.NivelLog.DEBUG);
			//
			// // COMMENTED OUT 20030108 fin

			// // COMMENTAR esta linea solo para validar la respuesta y cuando
			// el servicio no esta disponible
			requestDispatcher = validaRespuesta(tramaRetorno, request, archivo);
			String encabezado = CreaEncabezado(S_MODULO, S_CABECERA,
					S_HLP_RESPUESTA, request);
			request.setAttribute("encabezado", encabezado);
			// // DESCOMENTAR esta linea solo para validar la respuesta y cuando
			// el servicio no esta disponible
			// // DEBUG CODE INICIO
			// archivo.setNumSecuencia ( 123 );
			// archivo.setStatus ( "Estatus de Pureba" );
			// archivo.setFechaEnvio ( new java.util.GregorianCalendar () );
			// requestDispatcher = ENVRES_DISPATCHER;
			// // DEGUG CODE FIN
			// TODO: BIT CU 4181, Envio de archivo
			/*
			 * VSWF ARR -I
			 */
			if (Global.USAR_BITACORAS.trim().equals("ON")) {
				try {
					BitaHelper bh = new BitaHelperImpl(request, session,
							request.getSession());
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean) bh.llenarBean(bt);
					bt
							.setNumBit(BitaConstants.ES_CH_SEG_REG_CHEQUES_ARCHIVO_IMPORTAR_ARCHIVO);
					bt.setContrato(session.getContractNumber());
					if (file.getName() != null)
						bt.setNombreArchivo(archBit);
					if (tramaRetorno != null) {
						if (tramaRetorno.substring(0, 2).equals("OK")) {
							bt.setIdErr(TIPO_OPERACION_REGISTRO + "0000");
						} else if (tramaRetorno.length() > 8) {
							bt.setIdErr(tramaRetorno.substring(0, 8));
						} else {
							bt.setIdErr(tramaRetorno);
						}
					}
					bt.setServTransTux(TIPO_OPERACION_REGISTRO);
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			/*
			 * VSWF ARR -F
			 */

		} catch (IOException ex) {
			EIGlobal.mensajePorTrace(CLASS_NAME + ": " + ex.toString(),
					EIGlobal.NivelLog.DEBUG);
			//ex.printStackTrace();
			despliegaPaginaErrorURL("Error al enviar el archivo", S_MODULO,
					S_CABECERA, S_HLP_INICIAL, "ChesRegistro", request,
					response);
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(CLASS_NAME + ": " + ex.getMessage(),
					EIGlobal.NivelLog.DEBUG);
			//ex.printStackTrace();
			despliegaPaginaErrorURL(ex.getMessage(), S_MODULO, S_CABECERA,
					S_HLP_INICIAL, "ChesRegistro", request, response);
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (IOException e) {
					EIGlobal.mensajePorTrace(CLASS_NAME + ": " + e.toString(),
							EIGlobal.NivelLog.DEBUG);
					e.printStackTrace();
				}
				writer = null;
			}
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					EIGlobal.mensajePorTrace(CLASS_NAME + ": " + e.toString(),
							EIGlobal.NivelLog.DEBUG);
					e.printStackTrace();
				}
				reader = null;
			}
		}

		return requestDispatcher;
	}

	/**
	 * Metodo para enviar un archivo por rcp
	 *
	 * @param Archivo
	 *            Archivo que sera mandado a Tuxedo
	 * @return true si el archivo fue enviado correctamente, de lo contrario
	 *         false
	 */
	protected boolean envia(File Archivo) {
		try {
			int Respuesta = 1;
			Runtime rt = Runtime.getRuntime();
			String cmd = "";

			EIGlobal.mensajePorTrace("ChesRegistro:envia - Inicia", EIGlobal.NivelLog.INFO);
          //Version JSCH, Proyecto CNBV
           	boolean Respuestal = false;
            ArchivoRemoto envArch = new ArchivoRemoto();

			// Archivo de configuracion
			if (Global.HOST_LOCAL.equals(Global.HOST_REMOTO)
					&& Global.DIRECTORIO_LOCAL
							.equals(Global.DIRECTORIO_REMOTO_INTERNET)) {
				return true;
			} else if ((Global.HOST_LOCAL.equals(Global.HOST_REMOTO))
					&& !(Global.DIRECTORIO_LOCAL
							.equals(Global.DIRECTORIO_REMOTO_INTERNET))) {
				cmd = "cp " + Global.DIRECTORIO_LOCAL + "/" + Archivo.getName()
						+ " " + Global.DIRECTORIO_REMOTO_INTERNET;
			} else {

				//IF PROYECTO ATBIA1 (NAS) FASE II

	           	if(!envArch.copiaLocalARemoto(Archivo.getName(), Global.DIRECTORIO_REMOTO_INTERNET)){

						EIGlobal.mensajePorTrace("*** ChesRegistro.envia  No se realizo la copia remota:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
						Respuestal = false;

					}
					else {
					    EIGlobal.mensajePorTrace("*** ChesRegistro.envia archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);

					}
	        	}
	           	//*********************************************


			EIGlobal.mensajePorTrace(CLASS_NAME + ": cmd= " + cmd,
					EIGlobal.NivelLog.INFO);
			Process proc = rt.exec(cmd);
			Respuesta = proc.waitFor();
			EIGlobal.mensajePorTrace(CLASS_NAME + ": respuesta=" + Respuesta,
					EIGlobal.NivelLog.INFO);
			if (Respuesta == 0)
				Respuesta = proc.exitValue();
			EIGlobal.mensajePorTrace(CLASS_NAME + ": exitValue= respuesta="
					+ Respuesta, EIGlobal.NivelLog.INFO);
			proc.destroy();
			if (Respuesta == 0)
				return true;
			//Version JSCH, Proyecto CNBV
			if (Respuestal)
                return true;
		} catch (Exception ex) {
			EIGlobal.mensajePorTrace(ex.getMessage(), EIGlobal.NivelLog.INFO);
			ex.printStackTrace(System.err);
		}
		return false;
	}

	/**
	 * genera la trama para el registro de archivos
	 *
	 * @return trama a utilizar
	 * @param file
	 *            File con el path del archivo
	 * @param archivo
	 *            ChesArchivoImportado
	 * @param baseResource
	 *            BaseResource con la informacion del usuario
	 */
	protected String generaTramaRegistro(BaseResource baseResource, File file,
			ChesArchivoImportado archivo) {
		String retValue = null;
		StringBuffer temp = new StringBuffer();
		temp.append(TIPO_ENTREGA_REGISTRO);
		temp.append(MEDIO_ENTREGA);
		temp.append('|');
		temp.append(baseResource.getUserID8());
		temp.append('|');
		temp.append(TIPO_OPERACION_REGISTRO);
		temp.append('|');
		temp.append(baseResource.getContractNumber());
		temp.append('|');
		temp.append(baseResource.getUserID8());
		temp.append('|');
		temp.append(baseResource.getUserProfile());
		temp.append('|');
		temp.append(baseResource.getContractNumber());
		temp.append('@');
		temp.append(file.getPath());
		temp.append('@');
		temp.append(archivo.getNumRegistros());
		temp.append('@');
		temp.append(archivo.getImporteTotal());
		temp.append('@');

		retValue = temp.toString();
		return retValue;
	}

	/**
	 * Obyiene una lista con los dias inhabiles
	 *
	 * @return Lista con los dias inhabiles encapsulados en un GregorianCalendar
	 */
	public java.util.List diasInhabiles() {
		java.sql.Connection conn = null;
		java.sql.PreparedStatement ps = null;
		java.sql.ResultSet rs = null;
		java.util.List retValue = null;

		String sql = "SELECT TO_CHAR(fecha, 'dd/mm/yyyy') FROM comu_dia_inhabil WHERE cve_pais = 'MEXI' AND fecha >= SYSDATE";
		try {
			EIGlobal.mensajePorTrace(CLASS_NAME + ": data_source = "
					+ Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.INFO);
			if (null == (conn = createiASConn(Global.DATASOURCE_ORACLE))) {
				EIGlobal.mensajePorTrace(CLASS_NAME
						+ ": Error al intentar crear la conexion",
						EIGlobal.NivelLog.ERROR);
			} else if (null == (ps = conn.prepareStatement(sql))) {
				EIGlobal.mensajePorTrace(CLASS_NAME
						+ ": No se puede crear el statement",
						EIGlobal.NivelLog.ERROR);
			} else if (null == (rs = ps.executeQuery())) {
				EIGlobal.mensajePorTrace(CLASS_NAME
						+ ":  No se puede crear el ResultSet",
						EIGlobal.NivelLog.ERROR);
			} else {
				retValue = new java.util.LinkedList();
				while (rs.next()) {
					try {
						java.util.Date date = sdf.parse(rs.getString(1));
						retValue.add(date);
					} catch (java.text.ParseException e) {
						EIGlobal
								.mensajePorTrace(
										CLASS_NAME
												+ ": resultset contiene cadena que no puede ser convertida a fecha,"
												+ e.getMessage(),
										EIGlobal.NivelLog.DEBUG);
						continue;
					}
				}
			}
		} catch (java.sql.SQLException ex) {
			EIGlobal.mensajePorTrace(CLASS_NAME
					+ ": error al hacer la consulta, " + ex.getMessage(),
					EIGlobal.NivelLog.ERROR);
			ex.printStackTrace();
		} finally {
			if (null != rs) {
				try {
					rs.close();
				} catch (java.sql.SQLException e) {
					EIGlobal.mensajePorTrace(CLASS_NAME
							+ ": error al hacer cerrar el resultset, "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				rs = null;
			}
			if (null != ps) {
				try {
					ps.close();
				} catch (java.sql.SQLException e) {
					EIGlobal.mensajePorTrace(CLASS_NAME
							+ ": error al hacer cerrar el statement, "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				ps = null;
			}
			if (null != conn) {
				try {
					conn.close();
				} catch (java.sql.SQLException e) {
					EIGlobal.mensajePorTrace(CLASS_NAME
							+ ": error al hacer cerrar la coneccion, "
							+ e.getMessage(), EIGlobal.NivelLog.ERROR);
				}
				conn = null;
			}
		}

		return (retValue);
	}

	/**
	 * Valida la respuesta de el registro de archivos
	 *
	 * @param tramaRetorno
	 *            trama que retorno el servicio
	 * @param request
	 *            per spec
	 * @param archivo
	 *            ChesArchivo que se importo
	 * @throws Exception
	 *             en caso de que ocurra algun error en la validacion
	 * @return requestDisptacher a utilizar
	 */
	protected String validaRespuesta(String tramaRetorno,
			HttpServletRequest request, ChesArchivoImportado archivo)
			throws Exception {
		String retValue = null;

		if (null == tramaRetorno) {
			EIGlobal.mensajePorTrace(CLASS_NAME
					+ ": mensaje de respuesta vacio", EIGlobal.NivelLog.DEBUG);
			throw new Exception(IEnlace.MSG_PAG_NO_DISP);
		}

		if (tramaRetorno.startsWith("ALYM")) {
			EIGlobal
					.mensajePorTrace(
							CLASS_NAME
								+ ": ERROR: Se exceden los limites de la operacion, trama="
								+ tramaRetorno, EIGlobal.NivelLog.ERROR);
			throw new Exception("Error: " + tramaRetorno.substring(0, 8) + " " +
					tramaRetorno.substring(8));
		}

		if (!tramaRetorno.startsWith("OK")) {
			EIGlobal
					.mensajePorTrace(
							CLASS_NAME
									+ ": ERROR el servicio no respondio de manera esperada, trama="
									+ tramaRetorno, EIGlobal.NivelLog.DEBUG);
			throw new Exception(IEnlace.MSG_PAG_NO_DISP);
		}
		tramaRetorno = tramaRetorno.substring(8);
		String referencia = tramaRetorno.substring(0, 8);
		tramaRetorno = tramaRetorno.substring(8);
		String codError = tramaRetorno.substring(0, 8);
		tramaRetorno = tramaRetorno.substring(8);

		if (!codError.equals(COD_ERROR_OK)) {
			EIGlobal.mensajePorTrace(CLASS_NAME
					+ ": ERROR la trama no contiene el codErr=" + COD_ERROR_OK
					+ " sino=" + codError, EIGlobal.NivelLog.DEBUG);

			java.util.StringTokenizer errTokenizer = new java.util.StringTokenizer(
					tramaRetorno, "@");
			throw new Exception(errTokenizer.nextToken());
		}

		java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(
				tramaRetorno, "@");

		if (3 != tokenizer.countTokens()) {
			EIGlobal.mensajePorTrace(CLASS_NAME
					+ ": ERROR la trama no cumple el formato indicado",
					EIGlobal.NivelLog.DEBUG);
			throw new Exception(IEnlace.MSG_PAG_NO_DISP);
		}

		String nombreArchivo = tokenizer.nextToken();
		long numSecuencia = Long.parseLong(tokenizer.nextToken());
		String estatus = tokenizer.nextToken();

		archivo.setFileName(nombreArchivo);
		archivo.setNumSecuencia(numSecuencia);
		archivo.setStatus(estatus);
		archivo.setFechaEnvio(new java.util.GregorianCalendar());

		retValue = ENVRES_DISPATCHER;
		request.setAttribute(ARCHIVO_ENVIADO, archivo);
		String encabezado = CreaEncabezado(S_MODULO, S_CABECERA,
				S_HLP_RESPUESTA, request);
		request.setAttribute("encabezado", encabezado);
		return retValue;
	}

	protected String archivoImportado(HttpServletRequest request)
			throws IOException {
		EIGlobal.mensajePorTrace(CLASS_NAME + " file:"
				+ getFormParameter(request, FILE_NAME), EIGlobal.NivelLog.DEBUG);
		java.util.Enumeration headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String headerName = (String) headers.nextElement();
			// EIGlobal.mensajePorTrace ( CLASS_NAME + ": header " + headerName
			// + "=" + request.getHeader ( headerName), 6 );
		}
		String contentType = request.getHeader("Content-Type");
		// EIGlobal.mensajePorTrace ( CLASS_NAME + ": Content-Type=" +
		// contentType , 5 );
		String boundary = contentType.substring(contentType
				.indexOf("boundary="));
		boundary = boundary.substring(boundary.indexOf('=') + 1);
		boundary = boundary.trim();
		// EIGlobal.mensajePorTrace ( CLASS_NAME + ": boundary=" + boundary, 6
		// );
		java.io.BufferedReader reader = new java.io.BufferedReader(
				new java.io.InputStreamReader(request.getInputStream()));
		java.io.StringWriter sWriter = new java.io.StringWriter();
		java.io.BufferedWriter writer = new java.io.BufferedWriter(sWriter);
		String linea = null;

		while (null != (linea = reader.readLine())) {
			// EIGlobal.mensajePorTrace ( CLASS_NAME + " requestBody:" + linea ,
			// 6 );
			if (linea.trim().endsWith(boundary)) {
				linea = reader.readLine();
				// EIGlobal.mensajePorTrace ( CLASS_NAME + ": inicio boundary:"
				// + linea,6);
				if (-1 != linea.indexOf("name=\"" + FILE_NAME + "\"")) {
					// EIGlobal.mensajePorTrace ( CLASS_NAME + ": inicio archivo
					// importado", 6 );
					do {
						// System.out.println ( CLASS_NAME + ": archivo:" +
						// linea);
						// EIGlobal.mensajePorTrace ( CLASS_NAME + ": len="
						// +linea.length () , 6 );
						if (-1 != linea.indexOf(boundary)) {
							break;
						} else if (!linea.trim().equals("")) {
							if (-1 == linea.indexOf("Content-Type")
									&& -1 == linea
											.indexOf("Content-Disposition")) {
								writer.write(linea);
								writer.newLine();
							}
						}
					} while (null != (linea = reader.readLine()));

				}

			}
		}
		writer.close();
		// EIGlobal.mensajePorTrace ( CLASS_NAME + ": writer.toString()=\n" +
		// sWriter.toString (), 5 );
		// EIGlobal.mensajePorTrace ( CLASS_NAME + ": len=" + sWriter.toString
		// ().length (), 5 );
		return sWriter.toString();
	}

	// public byte[] getFileInBytes ( HttpServletRequest request, String
	// sNombreArchivo )
	// throws IOException, SecurityException {
	// MultipartRequest mprFile = new MultipartRequest ( request,
	// IEnlace.LOCAL_TMP_DIR, 2097152 );
	//
	// java.io.FileInputStream entrada;
	// String sFile = sNombreArchivo.substring (sNombreArchivo.lastIndexOf (
	// "\\" ) + 1 , sNombreArchivo.length () );
	// File fArchivo = new File ( ( IEnlace.LOCAL_TMP_DIR + "/" + sFile ).intern
	// () );
	// byte[] aBArchivo = new byte[ ( int ) fArchivo.length () ];
	// entrada = new java.io.FileInputStream ( fArchivo );
	// byte bByte = 0;
	// try {
	// for ( int i = 0; ( ( bByte = ( byte ) entrada.read () ) != -1 ); i++ ) {
	// aBArchivo[ i ] = bByte;
	// }
	// }catch(Exception e) {
	// throw new IOException ();
	// } finally {
	// entrada.close ();
	// fArchivo.delete ();
	// }
	// return aBArchivo;
	// }
	//
	public byte[] getFileInBytes(HttpServletRequest request)
			throws IOException, SecurityException {
		String sFile = "";
		MultipartRequest mprFile = new MultipartRequest(request,
				IEnlace.LOCAL_TMP_DIR, 10485760);
		java.io.FileInputStream entrada;
		java.util.Enumeration files = mprFile.getFileNames();

		if (files.hasMoreElements()) {
			String name = (String) files.nextElement();
			sFile = mprFile.getFilesystemName(name);
		}

		File fArchivo = new File((IEnlace.LOCAL_TMP_DIR + "/" + sFile).intern());
		byte[] aBArchivo = new byte[(int) fArchivo.length()];
		entrada = new java.io.FileInputStream(fArchivo);
		byte bByte = 0;
		try {
			for (int i = 0; ((bByte = (byte) entrada.read()) != -1); i++) {
				aBArchivo[i] = bByte;
			}
		} catch (Exception e) {
			throw new IOException();
		} finally {
			entrada.close();
			fArchivo.delete();
		}
		return aBArchivo;
	}

	public boolean cambiaFechaLimitePago(String nombreArch) {
		RandomAccessFile archivoES;
		String strRegistro = "", fechaLimitePago = "";
		long pos = 0, posFecha = 0;

		try {
			archivoES = new RandomAccessFile(nombreArch, "rw");
			archivoES.seek(0);

			while (strRegistro != null) {
				strRegistro = archivoES.readLine();

				if (strRegistro != null && strRegistro.length() > 0) {
					posFecha = pos + 122;
					pos = archivoES.getFilePointer();

					// System.out.println("*"+strRegistro+"*");
					// System.out.println("longitud = "+strRegistro.length());
					System.out.println("posicion inicio linea = -" + pos + "-");

					fechaLimitePago = strRegistro.substring(122);
					System.out.println("fecha= -" + fechaLimitePago + "-");
					System.out.println("PosFecha :" + posFecha + "-");

					if (fechaLimitePago.equals("          ")
							|| fechaLimitePago.equals("0000000000")) {
						archivoES.seek(posFecha);
						archivoES.writeBytes("31/12/9999");
						archivoES.readLine();
					}
				}
			}
			archivoES.close();
		} catch (Exception e) {
			System.out.println("Error : " + e);
			System.out.println("No se hizo la modificacion del archivo");
			return false;
		}

		return true;
	}

}// Fin clase