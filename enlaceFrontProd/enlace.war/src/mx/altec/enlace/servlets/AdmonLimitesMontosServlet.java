package mx.altec.enlace.servlets;

import static mx.altec.enlace.utilerias.Global.NP_JNDI_CONECTION_FACTORY;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_REQUEST;
import static mx.altec.enlace.utilerias.Global.NP_JNDI_QUEUE_RESPONSE;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.AdmLyMGL21;
import mx.altec.enlace.beans.AdmLyMGL25;
import mx.altec.enlace.beans.AdmLyMGL26;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.LYMGuardar;
import mx.altec.enlace.dao.AdmLyMGpoOperacionDAO;
import mx.altec.enlace.dao.AdmLyMLimitesContratoDAO;
import mx.altec.enlace.dao.EI_Query;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.Token;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.LimitesContrato;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesUsuario;
import mx.altec.enlace.jms.mq.conexion.MQQueueSession;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.bo.WSBloqueoToken;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaConstants;

public class AdmonLimitesMontosServlet extends BaseServlet{
 
 /** The bool login. */
 static boolean boolLogin = false;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8671088696208518991L;

	/**
	 * DoGet.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		doPost( request, response );
	  }

  	/**
  	 * Doost.
  	 *
  	 * @param request the request
  	 * @param response the response
  	 * @throws ServletException the servlet exception
  	 * @throws IOException Signals that an I/O exception has occurred.
  	 */
  	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		  if(!SesionValida(request, response)) {
				return;
			}

		    String lstrOperacion = request.getParameter("Operacion");
		    //String lstrJSON      = request.getParameter("JSON");
		    //String lstrJSONOut   = request.getParameter("JSONOut");
		    String lstrOrigen    = request.getParameter("Origen");
		    String lstrDestino   = request.getParameter("Destino");
		    String lstrError     = request.getParameter("Error");
		    String borrar        = request.getParameter("borrar");
			javax.servlet.http.HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
	        request.setAttribute("MenuPrincipal", session.getStrMenu());
	        request.setAttribute("newMenu", session.getFuncionesDeMenu());
	        String titulo = "L&iacute;mites y Montos";
	        request.setAttribute("Encabezado",
	             CreaEncabezado("Establecimiento de " + titulo,
	                             "Administraci&oacute;n y Control &gt; Establecimiento de L&iacute;mites y Montos - " + titulo,
	                             "s25570h", request));
		    EIGlobal.mensajePorTrace("AdmonLimitesMontosServlet - Recibe: " +
		    		"	\nOperacion: [" + lstrOperacion +  "]" +
		    		"	\nOrigen: [" + lstrOrigen +  "]" +
		    		"	\nDestino: [" + lstrDestino +  "]" +
		    		"	\nError: [" + lstrError +  "]",
		    		EIGlobal.NivelLog.DEBUG);
		    // Valida Facultades

		    if(!session.getFacultad(BaseResource.FAC_LYM_USR) &&
		       !session.getFacultad(BaseResource.FAC_LYM_CONTR)	) {
	    		String laDireccion = "document.location='SinFacultades'";
	    		request.setAttribute( "plantillaElegida", laDireccion);
	    		request.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(request,response);
	            return;
	    	}

		    if("".equals(lstrOrigen) && borrar == null) {
		    	borraParamSesion(request);
			    EIGlobal.mensajePorTrace("Se limpio sesion", EIGlobal.NivelLog.DEBUG);

		    }
		    if("".equals(lstrOrigen)){
			    EIGlobal.mensajePorTrace("AdmonLimitesMontosServlet - 1: ",
			    		EIGlobal.NivelLog.DEBUG);

				boolean solVal=ValidaOTP.solicitaValidacion(
						session.getContractNumber(),IEnlace.ADM_SUPERUSR);

				if( session.getValidarToken() && session.getToken().getStatus() == 1 && solVal) {
					request.getSession().setAttribute("valOTP", "true");
				}

			    //BeanAdminLyM
			    BeanAdminLyM bean = new BeanAdminLyM();

			    //Asigna los indicadores de Facultades
			    bean.setFacultadContrato(session.getFacultad(BaseResource.FAC_LYM_CONTR));
			    bean.setFacultadOperacion(session.getFacultad(BaseResource.FAC_LYM_USR));

			    //Obtiene los l�mites del contrato
	    		AdmLyMLimitesContratoDAO limCtr = new AdmLyMLimitesContratoDAO();
		    	MQQueueSession mqsess = null;
		    	AdmLyMGL25 res = null;
		    	try {
		    		mqsess = new MQQueueSession(
		    				NP_JNDI_CONECTION_FACTORY,
		    				NP_JNDI_QUEUE_RESPONSE,
		    				NP_JNDI_QUEUE_REQUEST);
		    		limCtr.setMqsess(mqsess);
		    		res = limCtr.consultaLimitesMontos("C", "",
		    				session.getContractNumber());
		    		EIGlobal.mensajePorTrace("AdmonUsuarioServlet - obtiene limite del contrato - " +
				    		"Error: [" + res.getLimites().size() +  "]", EIGlobal.NivelLog.ERROR);
		    		if(res.getLimites().size() > 0) {
		    			res.getLimites().get(0).setLimiteActivo(true);
		    			bean.setLimitesContrato(res.getLimites().get(0));
		    		}
		    		else {
		    			LimitesContrato limTmp = new LimitesContrato();
		    			limTmp.setContrato(session.getContractNumber());
		    			limTmp.setLimiteActivo(false);
		    			limTmp.setTipoOperacion("C");
		    			bean.setLimitesContrato(limTmp);
		    		}
		    	} catch (Exception x) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
			    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    	} finally {
		    		try {
		    			mqsess.close();
		    		}catch(Exception x) {
					    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
					    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    		}
		    	}
			    // Carga usuarios en Bean
			    ArrayList<LimitesUsuario> limitesUsr = new ArrayList<LimitesUsuario>();
        		String[][] arrayUsuarios = TraeUsuarios(request);
        		for(int z=1;z<=Integer.parseInt(arrayUsuarios[0][0].trim());z++){
        			System.out.println( "Valor del USUARIO ANTES: "+arrayUsuarios[z][1]+"&");
        			arrayUsuarios[z][1] = convierteUsr7a8(arrayUsuarios[z][1]);
        			System.out.println( "Valor del  DESPU�S: "+arrayUsuarios[z][1]+"&");
        			LimitesUsuario limUsr = new LimitesUsuario();
        			limUsr.setCodCliente(arrayUsuarios[z][1]);
        			limUsr.setNombre(arrayUsuarios[z][2]);
        			limitesUsr.add(limUsr);
        		}
        		bean.setListaLimitesContratos(limitesUsr);


        		AdmLyMGpoOperacionDAO dao = new AdmLyMGpoOperacionDAO();
		    	AdmLyMGL21 res1 = null;
		    	try {
		    		mqsess = new MQQueueSession(
		    				NP_JNDI_CONECTION_FACTORY,
		    				NP_JNDI_QUEUE_RESPONSE,
		    				NP_JNDI_QUEUE_REQUEST);
		    		dao.setMqsess(mqsess);
		    		res1 = dao.consultaGpoOperacion("  ");
		    		EIGlobal.mensajePorTrace("AdmonUsuarioServlet - obtiene limite del contrato - " +
				    		"Error: [" + res1.getGrupos().toString() +  "]", EIGlobal.NivelLog.ERROR);
		    		bean.setListaOperaciones(res1.getGrupos());
		    	} catch (Exception x) {
				    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
			    		"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    	} finally {
		    		try {
		    			mqsess.close();
		    		}catch(Exception x) {
					    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - setMostrarMancomunidad -" +
					    	"Error: [" + x +  "]", EIGlobal.NivelLog.ERROR);
		    		}
		    	}


        		sess.setAttribute("BeanAdminLyM", bean);
		    	evalTemplate (lstrDestino, request, response);
		    }
		    else if("detalle".equals(lstrOperacion)){
			    EIGlobal.mensajePorTrace("AdmonLimitesMontosServlet - 2: ",
			    		EIGlobal.NivelLog.DEBUG);
			    ///////////////////--------------challenge--------------------------
				String validaChallenge = request.getAttribute("challngeExito") != null
					? request.getAttribute("challngeExito").toString() : "";

				EIGlobal.mensajePorTrace("--------------->validaChallenge AdmonLimitesMontosServlet:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

				if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
					EIGlobal.mensajePorTrace("--------------->Entrando challenge AdmonLimitesMontosServlet...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
					ValidaOTP.guardaParametrosEnSession(request);

					validacionesRSA(request, response);
					return;
				}

				//Fin validacion rsa para challenge
				///////////////////-------------------------------------------------

		    	if(detalle())
		    		evalTemplate (lstrDestino, request, response);
		    } else if("desglose".equals(lstrOperacion)) {
			    EIGlobal.mensajePorTrace("AdmonLimitesMontosServlet - 3: ",
			    		EIGlobal.NivelLog.DEBUG);
		    	if(validaToken(request) != 0) {
		    		evalTemplate (lstrOrigen, request, response);
		    		return;
		    	}
		    	BeanAdminLyM beanIni = (BeanAdminLyM) sess.getAttribute("BeanAdminLyM");
		    	BeanAdminLyM beanFin = (BeanAdminLyM) sess.getAttribute("BeanAdminLyMFin");

		    	LYMGuardar guarda = new LYMGuardar(beanIni, beanFin);
		    	int respGrd = guarda.guardaLYM();
			    EIGlobal.mensajePorTrace("Resultado al guardar: " + respGrd, EIGlobal.NivelLog.DEBUG);

			    request.setAttribute("fecha", ObtenFecha());
				EmailSender sender = new EmailSender();
				EmailDetails details = new EmailDetails();
				details.setNumeroContrato(session.getContractNumber());
				details.setRazonSocial(session.getNombreContrato());
				details.setTipo_operacion("a nivel contrato");
			    if(guarda.getLimFin() != null) {
			    	int numRef = 0;
			    	String codError = "";
		    		request.setAttribute("contrato", session.getContractNumber());
			    	request.setAttribute("descrContr", session.getNombreContrato());
		    		request.setAttribute("opeContr", "Mantenimiento de L&iacute;mites por Contrato");

			    	if(guarda.getResContrato().getCodigoOperacion().equals("GL240000")) {
			    		request.setAttribute("estatusContr", "Operaci\u00f3n exitosa");
			    		codError = "MLCS0000";
			    	}
			    	else {
			    		request.setAttribute("estatusContr", guarda.getResContrato().getMensError());
			    		try {
			    			codError = "MLCS" + guarda.getResContrato().getCodigoOperacion().substring(3);
			    		}catch(Exception e) {
			    			codError = "MLCS9999";
			    		}
			    	}
					try {
						try{
					        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION LYM",EIGlobal.NivelLog.DEBUG);
					        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
							EIGlobal.mensajePorTrace("Referencia " + numRef ,EIGlobal.NivelLog.INFO);
						  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
						  	BitaTCTBean bean = new BitaTCTBean ();
						  	bean = bh.llenarBeanTCT(bean);
						  	bean.setReferencia(numRef);
						  	bean.setTipoOperacion("MLCS");
						  	bean.setCodError(codError);
						    BitaHandler.getInstance().insertBitaTCT(bean);
				    	}catch(SQLException e){
				    		e.printStackTrace();
				   		}catch(Exception e){
				   			e.printStackTrace();
				   		}
			    		request.setAttribute("refeContr", "" + numRef);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
					}

					try {
						details.setNumTransmision("" + numRef);
						details.setEstatusActual(codError);
						sender.sendNotificacion(request,IEnlace.NOT_ADMIN_LYM, details);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
		    	}

			    ArrayList<LimitesOperacion> datosUsuarios = new ArrayList<LimitesOperacion>();
			    datosUsuarios.addAll(guarda.getAgregados());
			    datosUsuarios.addAll(guarda.getModificados());
			    datosUsuarios.addAll(guarda.getBorrados());
			    ArrayList<AdmLyMGL26> resUsuarios = guarda.getResUsuario();

			    if(datosUsuarios.size() != resUsuarios.size()) {
			    	//ya vali�...
			    }
			    else if(datosUsuarios.size() > 0){
			    	String datosPantalla[][] = new String[datosUsuarios.size()][5];
			    	int numRef = 0;
			    	String codError = "";
					details.setTipo_operacion("");
					ArrayList<LimitesOperacion> operLYMNotificacion = new ArrayList<LimitesOperacion>();
			    	for(int i = 0; i < datosUsuarios.size(); i++) {
			    		LimitesOperacion lim = datosUsuarios.get(i);
			    		AdmLyMGL26 res = resUsuarios.get(i);
			    		operLYMNotificacion.add(lim);
			    		datosPantalla[i][0] = lim.getUsuario();
			    		datosPantalla[i][1] = lim.getNombreUsuario();
			    		if("AL".equals(lim.getOpcion())) {
			    			datosPantalla[i][2] = "Alta de L&iacute;mites por Usuario";
			    		}
			    		else if("MO".equals(lim.getOpcion())) {
			    			datosPantalla[i][2] = "Modificaci&oacute;n de L&iacute;mites por Usuario";
			    		}
			    		else if("BA".equals(lim.getOpcion())) {
			    			datosPantalla[i][2] = "Baja de L&iacute;mites por Usuario";
			    		}
			    		numRef = 0;
			    		if("GL260000".equals(res.getCodigoOperacion())) {
			    			codError = "MLUS0000";
			    			datosPantalla[i][3] = "Operaci&oacute;n exitosa";
			    		}
			    		else {
			    			codError = "MLUS" + res.getCodigoOperacion().substring(3);
			    			datosPantalla[i][3] = res.getMensError();
			    		}
						try {
							try{
						        EIGlobal.mensajePorTrace("ENTRA BITACORIZACION LYM",EIGlobal.NivelLog.DEBUG);
						        numRef = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								EIGlobal.mensajePorTrace("Referencia " + numRef ,EIGlobal.NivelLog.INFO);
							  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
							  	BitaTCTBean bean = new BitaTCTBean ();
							  	bean = bh.llenarBeanTCT(bean);
							  	bean.setReferencia(numRef);
							  	bean.setTipoOperacion("MLUS");
							  	bean.setCodError(codError);
							    BitaHandler.getInstance().insertBitaTCT(bean);
					    	}catch(SQLException e){
					    		e.printStackTrace();
					   		}catch(Exception e){
					   			e.printStackTrace();
					   		}
				    		datosPantalla[i][4] = "" + numRef;
						} catch (Exception e) {
							EIGlobal.mensajePorTrace("ERROR AL BITACORIZAR [" + e.getMessage() +  "]",EIGlobal.NivelLog.ERROR);
						}
			    	}
			    	request.setAttribute("datosPantalla", datosPantalla);
					try {
						details.setNumTransmision("" + numRef);
						details.setListaOperLYM(operLYMNotificacion);
						details.setEstatusActual(codError);
						sender.sendNotificacion(request,IEnlace.NOT_ADMIN_LYM, details);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					}
			    }


			    borraParamSesion(request);

				EIGlobal.mensajePorTrace("Enviando a <" + lstrDestino + ">", EIGlobal.NivelLog.DEBUG);
		    	evalTemplate (lstrDestino, request, response);
		    } else {
		    	System.out.println("Operacion incorrecta...");
		    	evalTemplate (lstrError, request, response);
		    }

		  }
	
	
	/**
	 * IntentoFallido.
	 *
	 * @param tok the tok
	 */
	public void intentoFallido(Token tok) {
		Connection conn = null;
	 try {
			conn= createiASConn ( Global.DATASOURCE_ORACLE );
			tok.incrementaIntentos(conn);

		} catch(SQLException e) {
                             EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		} finally {
			try {
				EI_Query.cierraConnection(conn);

			} catch (Exception e) {
				EIGlobal.mensajePorTrace( "Cerra conexion" , EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
	}

	public void limpiaIntentoFallido(Token tok) {
		Connection conn = null;
	try {
		conn= createiASConn ( Global.DATASOURCE_ORACLE );
		tok.reinicializaIntentos(conn);
		conn.close();
	} catch(SQLException e) {
                         EIGlobal.mensajePorTrace( "SQLE en BaseServlet al validar token" , EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
	} finally {
		try {
			conn.close();
		} catch (SQLException e) {
			EIGlobal.mensajePorTrace( "Cerrar conexion" , EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
		}
	}
}
		public int validaToken(HttpServletRequest request)
		throws ServletException, IOException{

		      HttpSession sess = request.getSession();
			  BaseResource session = (BaseResource) sess.getAttribute("session");
		      String error = null;
			   String usr = session.getUserID8();
			  if(request.getSession().getAttribute("valOTP") != null) {
		    		Token tok = session.getToken();
		    		Connection conn = null;
		    		boolean result = false;
					try {
						conn = BaseServlet.createiASConn_static(Global.DATASOURCE_ORACLE);
						result = tok.validaToken(conn, request.getParameter("token"));
					} catch (SQLException e) {
					    EIGlobal.mensajePorTrace("AdmonUsuarioServlet - validaToken -" +
					    		"Error: [" + e +  "]", EIGlobal.NivelLog.ERROR);
					}
					finally {
						try {
							EI_Query.cierraConnection(conn);
						} catch(Exception e) {}
					}
		    		if(result) {
						ValidaOTP.guardaRegistroBitacora(request,"Token valido.");
						limpiaIntentoFallido(tok);
						EIGlobal.mensajePorTrace( "------------TOKEN VALIDO---->", EIGlobal.NivelLog.DEBUG);
						return 0;
					}
					else{
					if(tok.getIntentos() >= 4) {
					//llama al WS de bloqueo de Token
					WSBloqueoToken bloqueo = new WSBloqueoToken();
					String[] resultado = bloqueo.bloqueoToken(usr);
					//Realizar envio de notificaci�n

					EmailSender emailSender=new EmailSender();
					EmailDetails emailDetails = new EmailDetails();

					String numContrato = session.getUserID();
					String nombreContrato = session.getNombreUsuario();

					emailDetails.setNumeroContrato(numContrato);
					emailDetails.setRazonSocial(nombreContrato);

					emailSender.sendNotificacion(request,IEnlace.BLOQUEO_TOKEN_INTENTOS,emailDetails);
					ValidaOTP.guardaRegistroBitacora(request,"BTIF Bloqueo de token por intentos fallidos.");
					BitaTCTBean beanTCT = new BitaTCTBean ();
					BitaHelper bh =	new BitaHelperImpl(request,session,request.getSession(false));

					beanTCT = bh.llenarBeanTCT(beanTCT);
					beanTCT.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					beanTCT.setTipoOperacion("BTIF");
					beanTCT.setCodError("BTIF0000");
					beanTCT.setOperador(session.getUserID8());
					beanTCT.setConcepto("Bloqueo de token por intentos fallidos");

					try {
						BitaHandler.getInstance().insertBitaTCT(beanTCT);
					}catch(Exception e) {
						EIGlobal.mensajePorTrace("AdminLimitesMontosServlet - Error al insertar en bitacora intentos fallidos",EIGlobal.NivelLog.ERROR);

					}

					bh.incrementaFolioFlujo((String)getFormParameter(request,BitaConstants.FLUJO));
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setIdFlujo("BTIF");
					bt.setNumBit("000000");
					bt.setReferencia(obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)));
					bt.setIdToken(tok.getSerialNumber());

					if (session.getContractNumber() != null) {
						bt.setContrato(session.getContractNumber().trim());
					}

					try {
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace (" - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace (" - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
					}
					intentoFallido(tok);
					//error = "Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�per l�nea Empresarial para desbloquearlo.";
					//request.setAttribute("error", error);
					request.setAttribute ( "MensajeLogin01", "cuadroDialogo(\"Token Bloqueado por intentos fallidos, favor de llamar a nuestra S�perl�nea Empresarial para desbloquearlo.\", 3);" );
					request.setAttribute ( "Fecha", ObtenFecha () );
					int resDup = cierraDuplicidad(session.getUserID8());
					sess.setAttribute("session", session);
					sess.invalidate();
					HttpServletResponse response = null;
					evalTemplate ( IEnlace.LOGIN, request, response );
					//RequestDispatcher disp = getServletContext ().getRequestDispatcher (IEnlace.LOGIN);
					//disp.include (request,response);
					return 1;
				}
				intentoFallido(tok);
				boolLogin = false;
				ValidaOTP.guardaRegistroBitacora(request,"Token no valido.");
				EIGlobal.mensajePorTrace( "------------TOKEN INVALIDO--REPITE-->", EIGlobal.NivelLog.DEBUG);
				error = "Contrase&ntilde;a incorrecta.";
	    		request.setAttribute("error", error);
				}
				return 1;
		    	}
			  return 0;
		  }


	  public boolean desglose(){

		  return true;
	  }

	  /**
  	 * Detalle.
  	 *
  	 * @return true, if successful
  	 */
  	public boolean detalle(){

		  return true;
	  }


	  /**
  	 * Baja.
  	 *
  	 * @return true, if successful
  	 */
  	public boolean baja(){

		  return false;
	  }

	  /**
  	 * Cambio.
  	 *
  	 * @return true, if successful
  	 */
  	public boolean cambio(){

		  return false;
	  }

	  /**
  	 * Consulta.
  	 *
  	 * @return true, if successful
  	 */
  	public boolean consulta(){

		  return false;
	  }

		/**
		 * Borra param sesion.
		 *
		 * @param request the request
		 */
		private void borraParamSesion(HttpServletRequest request) {
			request.getSession().removeAttribute("BeanAdminLyM");
			request.getSession().removeAttribute("BeanAdminLyMFin");
		}


}