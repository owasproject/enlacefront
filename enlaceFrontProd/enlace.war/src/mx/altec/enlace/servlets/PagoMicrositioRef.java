package mx.altec.enlace.servlets;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.Util;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.FlujosConstants;

/**
 * @author Israel Ortiz Vargas Pago Referenciado por Micrositio Realiza las
 *         validaciones y ejecuta el pago desde el Micrositio Enlace Graba
 *         Pistas de Auditoria, Bitacora de Operaciones y Envia Notificaciones
 */

public class PagoMicrositioRef extends BaseServlet {

	/** Version */
	private static final long serialVersionUID = 1L;
	/** Codigo de Exito de la operacion Micrositio */
	private static final String COD_EXITO_MICROSITIOREF = "PMRF0000";
	/** SESSION */
	private static final String SESSION = "session";
	/** TOKEN */
	private static final String TOKEN = "token";
	/** IMPORTE */
	private static final String IMPORTE = "importe";
	/** LINEA_CAPTURA */
	private static final String LINEA_CAPTURA = "linea_captura";
	/** SERVICIO_ID */
	private static final String SERVICIO_ID = "servicio_id";
	/** CUENTA_CARGO */
	private static final String CUENTA_CARGO = "cuentaCargo";
	/** FOLIO_OPER */
	private static final String FOLIO_OPER = "folioOper";
	/** COD_ERROR */
	private static final String COD_ERROR = "CodError";
	/** IV_USER */
	private static final String IV_USER = "IV-USER";
	/** HORA_ERROR */
	private static final String HORA_ERROR = "HoraError";
	/** MSG_ERROR */
	private static final String MSG_ERROR = "MsgError";
	/** CUENTA_ABONO */
	private static final String CUENTA_ABONO = "cuentaAbono";


	/**
	 * doGet
	 * @param req request
	 * @param res response
	 * @throws IOException IOException
	 * @throws ServletException ServletException
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		defaultAction(req, res);
	}

	/**
	 * doPost
	 * @param req request
	 * @param res response
	 * @throws IOException IOException
	 * @throws ServletException ServletException
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		defaultAction(req, res);
	}

	/**
	 * defaultAction
	 * @param req request
	 * @param res response
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void defaultAction(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		String msgErrorMicrositio2 = "";
		String codErrorMicrositio2 = "";
		String cuentaCargo = null;
		String importe = "";
		String convenio = null;
		String numUsuario = "";
		String nomContrato = "";
		String numContrato = "";
		String nomUsuario = "";
		String servicio_id = "";
		String lineaCaptura = "";
		ArrayList<String> nomUserCont = new ArrayList<String>();
		boolean conToken = false;
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		if (req.getParameter(TOKEN) != null) {
			try {
				importe = java.net.URLDecoder.decode((String) req
						.getParameter(IMPORTE));
				lineaCaptura = java.net.URLDecoder.decode((String) req
						.getParameter(LINEA_CAPTURA));
				servicio_id = java.net.URLDecoder.decode((String) req
						.getParameter(SERVICIO_ID));
				nomUserCont.add(importe);
				nomUserCont.add(lineaCaptura);
				nomUserCont.add(servicio_id);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("Error en parsear datos..."+ e, EIGlobal.NivelLog.INFO);
			}
		} else {
			importe = (String) req.getParameter(IMPORTE);
			lineaCaptura = (String) req.getParameter(LINEA_CAPTURA);
			servicio_id = (String) req.getParameter(SERVICIO_ID);
			nomUserCont.add(importe);
			nomUserCont.add(lineaCaptura);
			nomUserCont.add(servicio_id);
		}
		String sesionValida = ValidaSesionMicrositio(req, res);
		if ("OK".equals(sesionValida)) {
			try {
				numUsuario = session.getUserID8();
				nomUsuario = session.getNombreUsuario();
				numContrato = session.getContractNumber();
				nomContrato = session.getNombreContrato();
				nomUserCont.add(numUsuario);
				nomUserCont.add(nomUsuario);
				nomUserCont.add(numContrato);
				nomUserCont.add(nomContrato);
				EIGlobal.mensajePorTrace("PagoMicrositioRef ...................................................", EIGlobal.NivelLog.INFO);
				String folioOper = null;
				if (req.getParameter(TOKEN) != null) {
					convenio = java.net.URLDecoder.decode((String) req
							.getParameter("convenio"));
					cuentaCargo = java.net.URLDecoder.decode((String) req
							.getParameter(CUENTA_CARGO));
					folioOper = java.net.URLDecoder.decode(((String) req
							.getParameter(FOLIO_OPER)) == null ? "" : req
							.getParameter(FOLIO_OPER));
				} else {
					convenio = (String) req.getParameter("convenio");
					cuentaCargo = (String) req.getParameter(CUENTA_CARGO);
					folioOper = (String) req.getParameter(FOLIO_OPER);
				}

				/**
				 * Correccion vulnerabilidades Deloitte Stefanini Nov 2013
				 */
				boolean valError = false;

				if (Util.validaCCX(convenio, 1)) {
					EIGlobal.mensajePorTrace(
							"el contenido del campo convenio es correcto ->"
									+ convenio, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1000");
					EIGlobal.mensajePorTrace(
							"El campo convenio no acepta caracteres especiales por favor introduzca solo letras y n�meros->"
									+ convenio, EIGlobal.NivelLog.INFO);
					valError = true;
				}

				if (Util.validaCCX(cuentaCargo, 1)) {
					EIGlobal.mensajePorTrace(
							"el contenido del campo cuentaCargo es correcto->"
									+ cuentaCargo, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1006");
					EIGlobal.mensajePorTrace(
							"El campo cuenta cargo no acepta caracteres especiales por favor introduzca solo n�meros->"
									+ cuentaCargo, EIGlobal.NivelLog.INFO);
					valError = true;
				}

				if (Util.validaCCX(numContrato, 1)) {
					EIGlobal.mensajePorTrace(
							"el contenido del campo numContrato es correcto->"
									+ numContrato, EIGlobal.NivelLog.INFO);
				} else {
					req.setAttribute(COD_ERROR, "1011");
					EIGlobal.mensajePorTrace(
							"El campo contrato no acepta caracteres especiales por favor introduzca solo n�meros->"
									+ numContrato, EIGlobal.NivelLog.INFO);
					valError = true;
				}


				if (valError) {
					String usrCSS = "";
					EIGlobal.mensajePorTrace(
							"PagoMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion",
							EIGlobal.NivelLog.DEBUG);
					if (null != req.getHeader(IV_USER)) {
						usrCSS = req.getHeader(IV_USER);
					} else if (null != session.getUserID()) {
						usrCSS = session.getUserID();
					}
					EIGlobal.mensajePorTrace(
							"PagoMicrositioRef::validaCrossSiteScript:: usrCSS->"
									+ usrCSS, EIGlobal.NivelLog.DEBUG);

					sess.setAttribute("usrCSS", usrCSS);
					req.setAttribute(HORA_ERROR, ObtenHora());
					req.setAttribute(
							MSG_ERROR,
							"<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
					evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);
					return;
				}

				/**
				 * Fin correccion vulnerabilidades Deloitte
				 */

				conToken = false;

				/**
				 * Pistas de Auditoria MICE02 - Confirmar Datos
				 */

				if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim()
						.equals("ON")) {

					EIGlobal.mensajePorTrace(
							"Pago Micrositio Ref - defaultAction "
									+ "- Inicia grabado de Pistas de Auditoria Operacion Confirmar Datos",
							EIGlobal.NivelLog.DEBUG);
					try {
						BitaHelper bh = new BitaHelperImpl(req, session, sess);

						// Inicializo Bean de EWEB_TRAN_BITACORA(PISTASAUDITORAS)
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean) bh.llenarBean(bt);
						// Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)

						bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
						bt.setEstatus("A");
						bt.setContrato(session.getContractNumber());
						bt.setUsr(session.getUserID8());
						if (importe != null) {
							try {
								bt.setImporte(Double.valueOf(importe));
							} catch (NumberFormatException e) {
								bt.setImporte(0.0);
							}
						}
						if (cuentaCargo != null) {
							bt.setCctaOrig(cuentaCargo);
						}
						if (convenio != null) {
							bt.setCctaDest(convenio);
						}
						bt.setFechaAplicacion(new Date());
						bt.setServTransTux(FlujosConstants.pagoMicrositioRef);
						bt.setNumBit(BitaConstants.ME_BTN_CONF_DATOS);
						bt.setIdErr(this.COD_EXITO_MICROSITIOREF);
						if (session.getContractNumber() != null) {
							bt.setContrato(session.getContractNumber().trim());
						}
						if (session.getUserID8() != null) {
							bt.setCodCliente(session.getUserID8().trim());
						}
						if (req.getSession().getAttribute(
								BitaConstants.SESS_BAND_TOKEN) != null
								&& ((String) req.getSession().getAttribute(
										BitaConstants.SESS_BAND_TOKEN))
										.equals(BitaConstants.VALIDA)) {
							bt.setIdToken(session.getToken().getSerialNumber());
						}
						// Inserto en Bitacoras
						EIGlobal.mensajePorTrace(
								"Pago Micrositio - defaultAction - "
										+ "bitacorizando: ["
										+ BitaConstants.ME_BTN_CONF_DATOS + "]",
								EIGlobal.NivelLog.INFO);
						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						EIGlobal.mensajePorTrace(
								"Pago Micrositio - SQLException - Exception ["
										+ e + "]", EIGlobal.NivelLog.INFO);
					} catch (Exception e) {
						EIGlobal.mensajePorTrace(
								"Pago Micrositio - Exception [" + e + "]",
								EIGlobal.NivelLog.INFO);
					}
				}
				/**
				 * Pistas de Auditoria MICE02 - Confirmar Datos
				 */

				AplicaBN05(req, res, nomUserCont);
				// AplicaBN05(req,res,nomUserCont);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			if ("KO".equals(sesionValida)) {
				if (session != null) {
					int resDup = cierraDuplicidad(session.getUserID8());
				}
				msgErrorMicrositio2 = "Su sesi�n ha expirado.";
				codErrorMicrositio2 = "0999";
				req.setAttribute(COD_ERROR, codErrorMicrositio2);
				req.setAttribute(MSG_ERROR, msgErrorMicrositio2);
				req.setAttribute(HORA_ERROR, ObtenHora());
				req.setAttribute(IMPORTE, importe);
				req.setAttribute(LINEA_CAPTURA, lineaCaptura);
				req.setAttribute(SERVICIO_ID, servicio_id);
				evalTemplate("/jsp/errorMicrositio.jsp", req, res); /* MSE */
			}
		}

	}

	/**
	 * //public void AplicaBN05( HttpServletRequest req, HttpServletResponse
	 * res) throws ServletException, IOException { Aplica BN05
	 * @param req request
	 * @param res response
	 * @param nomUserCont nomUserCont
	 * @throws ServletException ServletException
	 * @throws IOException IOException
	 */
	public void AplicaBN05(HttpServletRequest req, HttpServletResponse res,
			ArrayList nomUserCont) throws ServletException, IOException {
		final String LOG_METODO = "PagoMicrositioRef - AplicaBN05 - ";
		EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
		String msgErrorMicrositio = "";
		String codErrorMicrositio = "";
		String importe = (String) nomUserCont.get(0);
		String contrato = "";
		String lineaCaptura = (String) nomUserCont.get(1);
		String usuario = "";
		String clave_perfil = "";
		String servicio_id = (String) nomUserCont.get(2);
		ArrayList<String> res390 = new ArrayList<String>();
		ValidaCuentaContrato validaCuentas = null;
		String sesionValida = ValidaSesionMicrositio(req, res);
		// Valida la sesion
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		if ("KO".equals(sesionValida)) {
			if (session != null) {
				EIGlobal.mensajePorTrace(LOG_METODO
						+ "Sesion Expirada, se cierra duplicidad",
						EIGlobal.NivelLog.DEBUG);
				int resDup = cierraDuplicidad(session.getUserID8());
			}
			msgErrorMicrositio = "Su sesi�n ha expirado.";
			codErrorMicrositio = "0999";
			req.setAttribute(COD_ERROR, codErrorMicrositio);
			req.setAttribute(MSG_ERROR, msgErrorMicrositio);
			req.setAttribute(HORA_ERROR, ObtenHora());
			evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);
			return;
		}
		/*
		 * Carga los Datos del Correo del usuario
		 */
		verificaSuperUsuario(session.getContractNumber(), req);
		EIGlobal.mensajePorTrace(
				LOG_METODO + "Es SuperUsuario " + session.getUserID8() + " ->"
						+ session.getMensajeUSR().isSuperUsuario(),
				EIGlobal.NivelLog.INFO);
		subeDatosDeContacto(session.getUserID8(), req);

		/*********************
		 * Getronics CP M�xico. Inicia modificaci�n por Q14886
		 * *****************************
		 * if(session.getFacultad(session.FAC_CARGO_CHEQUES) &&
		 * session.getFacultad(session.FAC_TRANSF_CHEQ_NOREG)){
		 *************************************************************************************************************/
		EIGlobal.mensajePorTrace(LOG_METODO + "Q14884: FAC_CARGO_CHEQUES ["
				+ session.getFacultad(BaseResource.FAC_CARGO_CHEQUES) + "]",
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace(LOG_METODO + "Q14884: FAC_ABOMICRO_PAGCH ["
				+ session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH) + "]",
				EIGlobal.NivelLog.DEBUG);
		if (session.getFacultad(BaseResource.FAC_CARGO_CHEQUES)
				&& session.getFacultad(BaseResource.FAC_ABOMICRO_PAGCH)) {
			/********* Getronics CP M�xico. Termina modificaci�n por Q14886 *****************/
			contrato = session.getContractNumber();
			usuario = session.getUserID8();
			clave_perfil = session.getUserProfile();

			/*
			 * modificacion para validar que la cuenta pertenezca al contrato
			 * seleccionado.
			 */
			validaCuentas = new ValidaCuentaContrato();

			String moduloOrigen = IEnlace.MCargo_transf_pesos;
			String ctaCargo = java.net.URLDecoder.decode((String) req
					.getParameter(CUENTA_CARGO));
			String cuentaAbono = java.net.URLDecoder.decode((String) req
					.getParameter(CUENTA_ABONO));

			String[] datosCuenta = validaCuentas.obtenDatosValidaCuenta(
					contrato, usuario, clave_perfil, ctaCargo, moduloOrigen);

			if (!Util.validaCCX(cuentaAbono, 1)) {
				req.setAttribute(COD_ERROR, "1008");
				req.setAttribute(
						MSG_ERROR,
						"<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.");
				req.setAttribute(HORA_ERROR, ObtenHora());
				req.setAttribute("inyeccion", true);
				evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);
				return;
			} else {
				EIGlobal.mensajePorTrace(
						"el contenido del campo cuentaAbono es correcto ->"
								+ cuentaAbono, EIGlobal.NivelLog.INFO);
			}

			int ctaAbonoValida = consultaB750(usuario, contrato, req, res,
					cuentaAbono);

			if (datosCuenta == null) {
				EIGlobal.mensajePorTrace(LOG_METODO
						+ "***0928 LA CUENTA CARGO PROPORCIONADA NO ES VALIDA",
						EIGlobal.NivelLog.INFO);
				codErrorMicrositio = "0928";
				msgErrorMicrositio = "La cuenta cargo proporcionada no es v&aacute;lida";
				req.setAttribute(COD_ERROR, codErrorMicrositio);
				req.setAttribute(MSG_ERROR, msgErrorMicrositio);
				req.setAttribute(HORA_ERROR, ObtenHora());
				evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);
			} else if (ctaAbonoValida == 1) {
				EIGlobal.mensajePorTrace(LOG_METODO
						+ "***0928 LA CUENTA ABONO PROPORCIONADA NO ES VALIDA",
						EIGlobal.NivelLog.INFO);
				codErrorMicrositio = "0929";
				msgErrorMicrositio = "La cuenta ABONO proporcionada no es v&aacute;lida";
				req.setAttribute(COD_ERROR, codErrorMicrositio);
				req.setAttribute(MSG_ERROR, msgErrorMicrositio);
				req.setAttribute(HORA_ERROR, ObtenHora());
				evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res);

			} else {
				res390 = ejecutaTransferenciaMicrositio(clave_perfil, usuario,
						contrato, nomUserCont, req, res); // Ejecuci�n de
															// transferencia
				
				int resp390 = new Integer(res390.get(0)).intValue();

				// if(result == 0){
				if (resp390 == 0) {
					EIGlobal.mensajePorTrace(LOG_METODO + "Contrato: ["
							+ session.getContractNumber() + "]",
							EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(LOG_METODO + "Nombre Contrato: ["
							+ session.getNombreContrato() + "]",
							EIGlobal.NivelLog.DEBUG);
					req.setAttribute("NumContrato", session.getContractNumber());
					req.setAttribute("NomContrato", session.getNombreContrato());

					evalTemplate("/jsp/operacionPagoMicrositioOK.jsp", req, res);
				} else {
					EIGlobal.mensajePorTrace(LOG_METODO+ "Hubo un error al ejecutar la BN05 ",EIGlobal.NivelLog.INFO);

					if (res390.size() > 1) {
						req.setAttribute(COD_ERROR, res390.get(1));
						EIGlobal.mensajePorTrace(LOG_METODO+ "codErrorMicrositio <<" + res390.get(1)+ ">>", EIGlobal.NivelLog.INFO);
						req.setAttribute(MSG_ERROR, res390.get(2));
						EIGlobal.mensajePorTrace(LOG_METODO + "msgError <<"+ res390.get(2) + ">>", EIGlobal.NivelLog.INFO);

						/**
						 * Correccion vulnerabilidades Deloitte Stefanini Nov
						 * 2013
						 */
						if (res390.size() > 3) {
							req.setAttribute("inyeccion", true);
						}
						/** Correccion vulnerabilidades Deloitte */
					} else {
						codErrorMicrositio = (String) res390.get(1);
						req.setAttribute(COD_ERROR, codErrorMicrositio);
						EIGlobal.mensajePorTrace(LOG_METODO+ "codErrorMicrositio <<" + codErrorMicrositio+ ">>", EIGlobal.NivelLog.INFO);
						msgErrorMicrositio = (String) res390.get(2);
						req.setAttribute(MSG_ERROR, msgErrorMicrositio);
						EIGlobal.mensajePorTrace(LOG_METODO + "msgError <<"+ msgErrorMicrositio + ">>",EIGlobal.NivelLog.INFO);
					}

					req.setAttribute(LINEA_CAPTURA, lineaCaptura);
					req.setAttribute(IMPORTE, importe);
					req.setAttribute(HORA_ERROR, ObtenHora());
					req.setAttribute(SERVICIO_ID, servicio_id);
					evalTemplate("/jsp/errorMicrositio.jsp", req, res); /* MSE */
				}
			}
		} else {
			msgErrorMicrositio = "No tiene facultades para realizar la operacion";
			codErrorMicrositio = "0099";
			req.setAttribute(COD_ERROR, codErrorMicrositio);
			req.setAttribute(MSG_ERROR, msgErrorMicrositio);
			req.setAttribute(HORA_ERROR, ObtenHora());
			req.setAttribute(LINEA_CAPTURA, lineaCaptura);
			req.setAttribute(IMPORTE, importe);
			req.setAttribute(SERVICIO_ID, servicio_id);
			evalTemplate(IEnlace.ERROR_TMPL_MICROSITIO, req, res); /* MSE */
		}
		EIGlobal.mensajePorTrace(LOG_METODO + "Termina clase&", EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo Encargado para ejecutar la transferencia de pago micrositio
	 *
	 * @param clave_perfil Clave perfil
	 * @param usuario usuario ejecuta
	 * @param contrato contrato para transferencia
	 * @param nomUserCont nombre usuario contrato
	 * @param req request
	 * @param res response
	 * @return resultado resutado operacion
	 * @throws ServletException Excepcion Generica
	 * @throws IOException Excepcion Generica
	 */
	public ArrayList ejecutaTransferenciaMicrositio(String clave_perfil,
			String usuario, String contrato, ArrayList nomUserCont,
			HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		final String LOG_METODO = "PagoMicrositioRef - ejecutaTransferenciaMicrositio - ";

		EIGlobal.mensajePorTrace(LOG_METODO + "Entrando",
				EIGlobal.NivelLog.INFO);
		ArrayList<String> resultado = new ArrayList<String>();
		String msgErrorMicrositio3 = "";
		String codErrorMicrositio3 = "";
		String titularCuentaCargo = "";
		EIGlobal.mensajePorTrace(
				"<-----------------prueba titular cargo--------------------->"
						+ titularCuentaCargo, EIGlobal.NivelLog.INFO);
		String fecha = "";
		String importe = "";
		String servicio_id = "";
		String cuentaCargo = "";
		String convenio = "";
		String numContrato = "";
		String lineaCaptura = "";
		String empresa = "";
		boolean conToken = false;
		String cuentaAbono = "";
		String trama_salida = "";
		String sreferencia_390 = "";

		/**
		 * Variable para folio de Referencia
		 */
		String folioOperacion = "0";
		/**
		 * Codigo de error Tuxedo
		 */
		String cod_error = "";

		String trama_entrada = "";
		String sreferencia = "";
		String saldoCtaOrigen = "";
		int salida = 0;

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);

		String hora = ObtenHora();
		EIGlobal.mensajePorTrace(
				"<-----------------prueba antes conToken --------------------->"
						+ titularCuentaCargo, EIGlobal.NivelLog.INFO);
		if (!conToken) {
			if (req.getParameter(TOKEN) != null) {
				try {
					cuentaCargo = java.net.URLDecoder.decode((String) req
							.getParameter(CUENTA_CARGO));
					titularCuentaCargo = java.net.URLDecoder.decode(new String(
							req.getParameter("titularCuentaCargo").getBytes(
									"ISO-8859-1"), "UTF-8"));
					cuentaAbono = java.net.URLDecoder.decode((String) req
							.getParameter(CUENTA_ABONO));
					fecha = java.net.URLDecoder.decode((String) req
							.getParameter("fecha"));
					empresa = java.net.URLDecoder.decode((String) req
							.getParameter("empresa"));
					convenio = java.net.URLDecoder.decode((String) req
							.getParameter("convenio"));

					importe = java.net.URLDecoder.decode((String) req
							.getParameter(IMPORTE));
					lineaCaptura = java.net.URLDecoder.decode((String) req
							.getParameter(LINEA_CAPTURA));
					servicio_id = java.net.URLDecoder.decode((String) req
							.getParameter(SERVICIO_ID));
				} catch (Exception e) {
					EIGlobal.mensajePorTrace("Error en parsear datos..."+ e, EIGlobal.NivelLog.INFO);
				}
			} else {
				cuentaCargo = (String) req.getParameter(CUENTA_CARGO);
				titularCuentaCargo = new String(req.getParameter("titularCuentaCargo").getBytes("ISO-8859-1"), "UTF-8");
				cuentaAbono = (String) req.getParameter(CUENTA_ABONO);
				fecha = (String) req.getParameter("fecha");
				empresa = (String) req.getParameter("empresa");
				convenio = (String) req.getParameter("convenio");
				servicio_id = (String) req.getParameter(SERVICIO_ID);
				importe = (String) req.getParameter(IMPORTE);
				lineaCaptura = (String) req.getParameter(LINEA_CAPTURA);

			}
			boolean valError = false;
			/**
			 * Correccion vulnerabilidades Deloitte Stefanini Nov 2013
			 */
			EIGlobal.mensajePorTrace(
					"<-----------------prueba antes validacion --------------------->"
							+ titularCuentaCargo, EIGlobal.NivelLog.INFO);


			if (Util.validaCCX(cuentaAbono, 1)) {
				EIGlobal.mensajePorTrace(
						"el contenido del campo cuentaAbono es correcto->"
								+ cuentaAbono, EIGlobal.NivelLog.INFO);
			} else {
				codErrorMicrositio3 = "1008";
				EIGlobal.mensajePorTrace(
						"El campo cuenta abono no acepta caracteres especiales por favor introduzca solo n�meros->"
								+ cuentaAbono, EIGlobal.NivelLog.INFO);
				valError = true;
			}

			if (Util.validaCCX(fecha, 3)) {
				EIGlobal.mensajePorTrace(
						"el contenido del campo fecha es correcto->" + fecha,
						EIGlobal.NivelLog.INFO);
			} else {
				codErrorMicrositio3 = "1009";
				EIGlobal.mensajePorTrace(
						"El campo fecha no acepta caracteres especiales por favor introduzca solo n�meros y 2 diagonales->"
								+ fecha, EIGlobal.NivelLog.INFO);
				valError = true;
			}


			if (valError) {
				String usrCSS = "";
				EIGlobal.mensajePorTrace(
						"CuentaCargoMicrositio::validaCrossSiteScript:: Verificando si el usuario ya esta en la sesion",
						EIGlobal.NivelLog.DEBUG);
				if (null != req.getHeader(IV_USER)) {
					usrCSS = req.getHeader(IV_USER);
				} else if (null != session.getUserID()) {
					usrCSS = session.getUserID();
				}
				EIGlobal.mensajePorTrace(
						"CuentaCargoMicrositio::validaCrossSiteScript:: usrCSS->"
								+ usrCSS, EIGlobal.NivelLog.DEBUG);

				sess.setAttribute("usrCSS", usrCSS);
				msgErrorMicrositio3 = "<strong>Estimado Cliente.</strong><br><br>Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000 y recibir� una correcta asesor�a.";
				resultado.add("-1");
				resultado.add(codErrorMicrositio3);
				resultado.add(msgErrorMicrositio3);
				resultado.add("true");
				return resultado;
			}
			/**
			 * fin Correccion vulnerabilidades Deloitte
			 */

		} 
		
		/**
		 * Se modifica para la nueva clave de operaci�n
		 */

		String tipo_cta_origen = "P";
		String tipo_cta_destino = "X";
		String estatus = "";

		EIGlobal.mensajePorTrace(LOG_METODO + "referencia " + sreferencia, EIGlobal.NivelLog.INFO);
		trama_entrada = "1EWEB|" + usuario + "|" + FlujosConstants.pagoMicrositioRef
				+ "|" + contrato + "|" + usuario + "|" + clave_perfil + "|"
				+ cuentaCargo + "|" + tipo_cta_origen + "|" + cuentaAbono + "|"
				+ tipo_cta_destino + "|" + importe + "|" + "@" + convenio + "@"
				+ lineaCaptura + "| |";
		// +referencia+"|"; //*****Cambio para comprobante fiscal

		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable hs = null;
		EIGlobal.mensajePorTrace(LOG_METODO + "WEB_RED-PMIC>>" + ":"
				+ trama_entrada, EIGlobal.NivelLog.INFO);
		try {
			hs = tuxGlobal.web_red(trama_entrada); // MODIFICAR
			trama_salida = (String) hs.get("BUFFER");
			cod_error = (String) hs.get("COD_ERROR");
			EIGlobal.mensajePorTrace(LOG_METODO + "BUFFER: [" + trama_salida
					+ "]", EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace(LOG_METODO + "COD_ERROR: [" + cod_error
					+ "]", EIGlobal.NivelLog.INFO);
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
			salida = 1;
			resultado.add("1");
		} catch (Exception e) {
			e.printStackTrace();
			salida = 1;
			resultado.add("1");
		}
		if (null == trama_salida || "null".equals(trama_salida))
		{
			trama_salida = "";
		}
		req.setAttribute("msgRstOperacion", "");

		if (!"".equals(trama_salida) || null != trama_salida) {
			EIGlobal.mensajePorTrace(LOG_METODO + "Trama salida: "+ trama_salida, EIGlobal.NivelLog.INFO);
			req.setAttribute("sreferencia_390", sreferencia_390);
			req.setAttribute(FOLIO_OPER, folioOperacion);
			req.setAttribute("referenciaMicrositio", "");
			req.setAttribute("saldoCtaOrigen", "");
			req.setAttribute("saldoDespues", "");

			if (cod_error != null && !cod_error.startsWith("MANC")) {
				EIGlobal.mensajePorTrace(LOG_METODO + "Operacion sin Mancomunidad o Firma A", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(LOG_METODO+ "Trama salida sin mancomunidad: [" + trama_salida+ "]", EIGlobal.NivelLog.DEBUG);

				if (trama_salida.startsWith("OK")) {

					EIGlobal.mensajePorTrace(LOG_METODO + "Operacion Aceptada",EIGlobal.NivelLog.INFO);
					estatus = "Aceptada";
					saldoCtaOrigen = "00.00";
					folioOperacion = trama_salida.substring(8, 16);
					sreferencia_390 = trama_salida.substring(16);
					req.setAttribute("sreferencia_390", sreferencia_390);
					req.setAttribute(FOLIO_OPER, folioOperacion);
					req.setAttribute("saldoDespues", "00.00");

					if (resultado.isEmpty()) {
						resultado.add("0");
					} else {
						resultado.clear();
						resultado.add("0");
					}

				} else {
					/**
					 * Pasa validacion de mancomunidad pero se rechada por otras
					 * validaciones
					 */
					EIGlobal.mensajePorTrace(LOG_METODO+ "Pasa validacion de mancomunidad pero se rechada por otras validaciones",EIGlobal.NivelLog.INFO);

					estatus = "No Ejecutada";
					salida = 0; // error
					if (resultado.isEmpty()) {
						resultado.add("1");
					} else {
						resultado.clear();
						resultado.add("1");
					}
					req.setAttribute(LINEA_CAPTURA, lineaCaptura);
					req.setAttribute(IMPORTE, importe);
					req.setAttribute(SERVICIO_ID, servicio_id);
					if (!"".equals(trama_salida) && null != trama_salida) {
						codErrorMicrositio3 = trama_salida.substring(0, 8).trim();
						msgErrorMicrositio3 = trama_salida.substring(16).trim();
						resultado.add(codErrorMicrositio3);
						resultado.add(msgErrorMicrositio3);
					} else {
						codErrorMicrositio3 = "0099";
						msgErrorMicrositio3 = "Ocurrio un error no esperado al ejecutar la transferencia.";

						if (trama_salida.length() >= 16) {
							resultado.add(cod_error);
							resultado.add(trama_salida.substring(16));
						} else {
							resultado.add(codErrorMicrositio3);
							resultado.add(msgErrorMicrositio3);
						}
					}
					// Pintar error
					if (trama_salida != null && trama_salida.length() > 16) {
						req.setAttribute("msgRstOperacion",
								trama_salida.substring(16));
					}
					EIGlobal.mensajePorTrace(LOG_METODO+ "Pasa validacion de mancomunidad pero se rechaza por otras validaciones ["+ msgErrorMicrositio3 + "]",EIGlobal.NivelLog.INFO);

				}
			} else if (cod_error != null && "MANC0000".equals(cod_error)
					&& trama_salida.startsWith("OK")) {
				EIGlobal.mensajePorTrace(LOG_METODO + "Operacion Mancomunada",
						EIGlobal.NivelLog.INFO);
				estatus = "Mancomunada";
			} else {
				/**
				 * No pasa la validacion de Mancomunidad por firmas, montos,
				 * etc.
				 */
				EIGlobal.mensajePorTrace(LOG_METODO+ "No pasa la validacion de Mancomunidad por firmas, montos, etc.",EIGlobal.NivelLog.INFO);
				estatus = "Rechazada";
				// Pintar error
				if (trama_salida != null && trama_salida.length() > 16) {
					req.setAttribute("msgRstOperacion",
							trama_salida.substring(16));
				}

			}
			EIGlobal.mensajePorTrace(LOG_METODO + "estatus: [" + estatus + "]",
					EIGlobal.NivelLog.DEBUG);
			req.setAttribute("numUsuario", nomUserCont.get(3));
			req.setAttribute("nomUsuario", nomUserCont.get(4));
			req.setAttribute("numContrato", nomUserCont.get(5));
			req.setAttribute("nomContrato", nomUserCont.get(6));
			req.setAttribute(CUENTA_CARGO, cuentaCargo);
			req.setAttribute(CUENTA_ABONO, cuentaAbono);
			req.setAttribute("titularCuentaCargo", titularCuentaCargo);
			req.setAttribute(IMPORTE, importe);
			req.setAttribute(LINEA_CAPTURA, lineaCaptura);
			req.setAttribute("fecha", fecha);
			req.setAttribute("hora", hora);
			req.setAttribute("estatus", estatus);
			req.setAttribute("empresa", empresa);
			req.setAttribute("convenio", convenio);
			req.setAttribute(SERVICIO_ID, servicio_id);

			if ("Mancomunada".equals(estatus)) {
				folioOperacion = trama_salida.substring(8, 16);
				req.setAttribute("sreferencia_390", folioOperacion);
				req.setAttribute(FOLIO_OPER, folioOperacion);
				salida = 0; // error
				if (resultado.isEmpty()) {
					resultado.add("0");
				} else {
					resultado.clear();
					resultado.add("0");
				}
			} else if ("Rechazada".equals(estatus) /*
													 * ||
													 * "".equals(camposTransferencia
													 * [1]) ||
													 * "".equals(camposTransferencia
													 * [2])
													 */) {
				salida = 0; // error
				if (resultado.isEmpty()) {
					resultado.add("0");
				} else {
					resultado.clear();
					resultado.add("0");
				}
				EIGlobal.mensajePorTrace(LOG_METODO
						+ "Los datos de la transferencia vienen vacios",
						EIGlobal.NivelLog.ERROR);
			} else if ("Aceptada".equals(estatus)) {
				req.setAttribute("referenciaMicrositio", trama_salida.substring(8,16));
				req.setAttribute("saldoCtaOrigen", "00.00");
				salida = 0; // OK
				if (resultado.isEmpty()) {
					resultado.add("0");
				} else {
					resultado.clear();
					resultado.add("0");
				}
			}
		} // --!trama_salida.equals("")

		/**
		 * Pistas de Auditoria MICE04 - Pago Micrositio Bitacora de Operaciones
		 * CPMS
		 */

		if ("0".equals((String) resultado.get(0))
				&& "ON".equals(mx.altec.enlace.utilerias.Global.USAR_BITACORAS
						.trim())) {
			EIGlobal.mensajePorTrace(
					LOG_METODO + "Inicia grabado de Bitacoras",
					EIGlobal.NivelLog.DEBUG);
			try {
				BitaHelper bh = new BitaHelperImpl(req, session, sess);

				// Inicializo Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean) bh.llenarBean(bt);

				// Lleno Bean de EWEB_TRAN_BITACORA(PISTAS AUDITORAS)
				bt.setIdFlujo(BitaConstants.ME_OPERACION_MICROSITIO);
				EIGlobal.mensajePorTrace(LOG_METODO + "folioOperacion: ["
						+ folioOperacion + "]", EIGlobal.NivelLog.DEBUG);
				if (!"".equals(folioOperacion.trim())) {
					bt.setReferencia(Long.valueOf(folioOperacion.trim()));
				}
				bt.setEstatus("A");
				bt.setContrato(session.getContractNumber());
				bt.setUsr(session.getUserID8());
				if (importe != null) {
					try {
						bt.setImporte(Double.valueOf(importe));
					} catch (NumberFormatException e) {
						bt.setImporte(0.0);
					}
				}
				if (cuentaCargo != null) {
					bt.setCctaOrig(cuentaCargo);
				}
				if (cuentaAbono != null) {
					bt.setCctaDest(cuentaAbono);
				}
				bt.setFechaAplicacion(new Date());
				bt.setServTransTux(FlujosConstants.pagoMicrositioRef);

				// Inicializo Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
				BitaTCTBean beanTCT = new BitaTCTBean();
				beanTCT = bh.llenarBeanTCT(beanTCT);

				// Datos Comunes
				bt.setNumBit(BitaConstants.ME_PAGO);
				bt.setIdErr(cod_error);
				beanTCT.setCodError(this.COD_EXITO_MICROSITIOREF);

				if (session.getContractNumber() != null) {
					bt.setContrato(session.getContractNumber().trim());
					beanTCT.setNumCuenta(session.getContractNumber().trim());
				}

				if (session.getUserID8() != null) {
					bt.setCodCliente(session.getUserID8().trim());
					beanTCT.setUsuario(session.getUserID8().trim());
					beanTCT.setOperador(session.getUserID8().trim());
				}

				if (req.getSession()
						.getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
						&& ((String) req.getSession().getAttribute(
								BitaConstants.SESS_BAND_TOKEN))
								.equals(BitaConstants.VALIDA)) {
					bt.setIdToken(session.getToken().getSerialNumber());
				}

				Integer ref_tct = 0;
				String codError = null;
				hs = tuxGlobal.sreferencia("901");
				codError = hs.get("COD_ERROR").toString();
				ref_tct = (Integer) hs.get("REFERENCIA");
				if (codError.endsWith("0000") && ref_tct != null) {
					beanTCT.setReferencia(ref_tct);
				}

				// Lleno Bean de TCT_BITACORA(BITACORA DE OPERACIONES)
				beanTCT.setTipoOperacion(BitaConstants.CTK_PAGO_MICROSITIO);

				// Inserto en Bitacoras
				EIGlobal.mensajePorTrace(LOG_METODO + "bitacorizando: ["
						+ BitaConstants.ME_PAGO + ", "
						+ BitaConstants.ME_OPERACION_MICROSITIO + "]",
						EIGlobal.NivelLog.INFO);
				BitaHandler.getInstance().insertBitaTransac(bt);
				BitaHandler.getInstance().insertBitaTCT(beanTCT);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace(LOG_METODO + "SQLException - [" + e
						+ "]", EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace(LOG_METODO + "Exception [" + e + "]",
						EIGlobal.NivelLog.INFO);
			}
			/**
			 * Pistas de Auditoria MICE04 - Pago Micrositio Bitacora de
			 * Operaciones CPMS
			 */

			/**
			 * Env�a notificaciones de la operaci�n
			 */
			EmailSender emailSender = new EmailSender();
			EmailDetails emailDetails = new EmailDetails();

			if (("TRAN0000".equals(cod_error) || "MANC0000".equals(cod_error))
					&& emailSender.enviaNotificacion(cod_error)) {
				emailDetails.setEstatusActual(cod_error);
				emailDetails.setImpTotal(ValidaOTP.formatoNumero(importe));
				emailDetails.setNumeroContrato(session.getContractNumber());
				emailDetails.setRazonSocial(session.getNombreContrato());
				emailDetails.setNumRef(folioOperacion);
				emailDetails.setTipoTransferenciaUni(" Pago Micrositio ");
				emailDetails.setCuentaDestino(cuentaAbono);
				emailDetails.setNumCuentaCargo(cuentaCargo);
				emailDetails.setConvenio(convenio);
				if (empresa != null) {
					emailDetails.setNombreConvenio(empresa.trim());
				}
				emailSender.sendNotificacion(req, IEnlace.NOT_PAGO_MICROSITIO,
						emailDetails);
			}
		}
		EIGlobal.mensajePorTrace(LOG_METODO + "resultado: [" + resultado.size()
				+ "]", EIGlobal.NivelLog.DEBUG);
		return resultado;
	}

	/**
	 * Valida la Sesion micrositio
	 *
	 * @param request
	 *            request
	 * @param response
	 *            response
	 * @return resultado sesion valida
	 * @throws ServletException
	 *             Excepcion generica
	 * @throws java.io.IOException
	 *             Excepcion generica
	 */
	public String ValidaSesionMicrositio(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		final String LOG_METODO = "PagoMicrositioRef - ValidaSesionMicrositio - ";
		String resultado = "";
		int resDup = 0;
		HttpSession sess;
		String idSession = null;
		sess = request.getSession(false);
		mx.altec.enlace.bo.BaseResource session = null;
		EIGlobal.mensajePorTrace(LOG_METODO + "inicio", EIGlobal.NivelLog.DEBUG);
		if (sess == null) {
			resultado = "KO";
			EIGlobal.mensajePorTrace(LOG_METODO + "sesion nula",
					EIGlobal.NivelLog.DEBUG);
		} else {
			idSession = sess.getId();
			session = (BaseResource) request.getSession().getAttribute(
					SESSION);
			if (session == null || session.getUserID8() == null
					|| "".equals(session.getUserID8())) {
				resultado = "KO";
				EIGlobal.mensajePorTrace(LOG_METODO + "Datos vacios",
						EIGlobal.NivelLog.DEBUG);
			} else {
				session.setUltAcceso(Calendar.getInstance().getTime().getTime());
				if (session.getTiempoActivo() > (Global.EXP_SESION * 60000)) {
					resDup = cierraDuplicidad(session.getUserID8());
					session.setSeguir(false);
					sess.setAttribute(SESSION, session);
					sess.removeAttribute(SESSION);
					sess.invalidate();
					return "KO";
				}
				if (session.getValidarSesion()
						&& (!validaSesionDB(session.getUserID8(), idSession))) {
					resDup = cierraDuplicidad(session.getUserID8());
					session.setSeguir(false);
					sess.removeAttribute(SESSION);
					sess.invalidate();
					return "KO";
				}
				int statusToken = session.getToken().getStatus();
				EIGlobal.mensajePorTrace(LOG_METODO + "Status Token: "
						+ statusToken, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(LOG_METODO + "Validar Token: "
						+ session.getValidarToken(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(LOG_METODO + "Token Validado2: "
						+ session.getTokenValidado(), EIGlobal.NivelLog.DEBUG);
				if (session.getValidarToken() && !session.getTokenValidado()) {
					switch (statusToken) {
					case 0: // activaci�n
						response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
						request.getSession()
								.setAttribute(
										"MensajeLogin01",
										"cuadroDialogo(\"Su token ya le fue entregado pero no est� activo. Por favor f�rmese en Enlace por Internet para activarlo e intente nuevamente.\", 1);");
						return "act";
					case 1: // validaci�n
								request.getSession().setAttribute(
								BitaConstants.SESS_BAND_TOKEN,
								BitaConstants.VALIDA);
								guardaParametrosEnSession(request, session.getContractNumber());
								response.sendRedirect("/Enlace"
								+ IEnlace.TOKEN_MICROSITIO);
						return TOKEN;
					case 2: // bloqueado
						resDup = cierraDuplicidad(session.getUserID8());
						request.getSession()
								.setAttribute(
										"MensajeLogin01",
										"cuadroDialogo(\"Su Token ha sido bloqueado; para operar en Enlace, es necesario que solicite un nuevo Token y lo recoja en Sucursal.\", 1);");
						response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
						return "bloq";
					default:
						request.getSession()
								.setAttribute(
										"MensajeLogin01",
										"cuadroDialogo(\"Su Token es en un estatus que no le permite operar en Enlace Internet.\", 1);");
						response.sendRedirect("/Enlace/jsp/loginMicrositio.jsp");
						return "errorToken";
					}
				}
				resultado = "OK";
			}
		}
		if (session != null) {
			sess.setAttribute(SESSION, session);
			if (session.getValidarSesion()) {
				actualizaSesionDB(session.getUserID8());
			}
		}
		EIGlobal.mensajePorTrace(LOG_METODO + "Fin datos OK - resultado: ["
				+ resultado + "], [" + resDup + "]", EIGlobal.NivelLog.DEBUG);
		return resultado;
	}

	/**
	 * Guarda los parametros de sesion
	 *
	 * @param request
	 *            request
	 * @param numContrato
	 *            numero de contrato
	 */
	private void guardaParametrosEnSession(HttpServletRequest request,
			String numContrato) {

		EIGlobal.mensajePorTrace("-333333aaaaa---------Interrumple el flujo--",
				EIGlobal.NivelLog.INFO);
		String template = request.getServletPath();
		EIGlobal.mensajePorTrace("5-****--------------->template**********--->"
				+ template, EIGlobal.NivelLog.INFO);
		request.getSession().setAttribute("plantilla", template);

		HttpSession session = request.getSession(false);
		if (session != null) {

			Map tmp = new HashMap();
			Enumeration enumer = request.getParameterNames();
			EIGlobal.mensajePorTrace(
					"6665555-****--------------->enumer**********--->"
							+ request.getParameter("valida"),
					EIGlobal.NivelLog.INFO);

			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				EIGlobal.mensajePorTrace(
						name + "----valor-------" + request.getParameter(name),
						EIGlobal.NivelLog.INFO);

				tmp.put(name, request.getParameter(name));
			}

			EIGlobal.mensajePorTrace("lstContratos----valor IOV -------"
					+ numContrato, EIGlobal.NivelLog.INFO);
			tmp.put("lstContratos", numContrato);

			session.setAttribute("parametros", tmp);

			tmp = new HashMap();
			enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}

	}

	/**
	 * Consulta la transacci�n B750
	 *
	 * @param usuario
	 *            usuario
	 * @param contrato
	 *            contrato
	 * @param req
	 *            request
	 * @param res
	 *            response
	 * @param ctaAbonoRec
	 *            cuenta abono
	 * @return salida respuesta
	 * @throws ServletException
	 *             Excepcion Generica
	 * @throws IOException
	 *             Excepcion Generica
	 */
	public int consultaB750(String usuario, String contrato,
			HttpServletRequest req, HttpServletResponse res, String ctaAbonoRec)
			throws ServletException, IOException {

		int salida = 0; // por default OK

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		String camposEmpresa[] = null;
		String trama_salida = "";
		String trama_entrada = "";
		String tipo_operacion = "CDEM"; // prefijo del servicio ( consulta datos
										// empresa Pago en l�nea)
		String cabecera = "1EWEB"; // medio de entrega ( regresa trama)

		String convenio = req.getParameter("convenio");

		String perfil = "";
		String pipe = "|";
		int tokens = 13;

		perfil = session.getUserProfile();

		if (!"".equals(convenio.trim())) {
			try {
				EIGlobal.mensajePorTrace(
						"***ConsultaDatosEmpresa.class  &facultad de consulta OK ...&",
						EIGlobal.NivelLog.INFO);
				trama_entrada = cabecera + "|" + usuario + "|" + tipo_operacion
						+ "|" + contrato + "|" + usuario + "|" + perfil + "|"
						+ convenio;
				EIGlobal.mensajePorTrace(
						"***ConsultaDatosEmpresa.class  trama de entrada >>"
								+ trama_entrada + "<<", EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace(
						"***ConsultaDatosEmpresa.class  ejecutando servicio ...",
						EIGlobal.NivelLog.DEBUG);

				ServicioTux tuxedoGlobal = new ServicioTux();
				try {
					Hashtable hs = tuxedoGlobal.web_red(trama_entrada);
					trama_salida = (String) hs.get("BUFFER");
				} catch (java.rmi.RemoteException re) {
					re.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (null == trama_salida  || "null".equals(trama_salida))
				{
					trama_salida = "";
				}
				EIGlobal.mensajePorTrace(
						"ConsultaDatosEmpresa - iniciaConsultaDatosEmpresa Trama salida: "
								+ trama_salida, EIGlobal.NivelLog.DEBUG);
				camposEmpresa = desentramaDatosEmpresa(trama_salida, tokens,
						pipe);
				if ("TUBO0000".equals(camposEmpresa[0])) {
					if ("".equals(camposEmpresa[4]) || "".equals(camposEmpresa[12])) {
						salida = 1; // error
						EIGlobal.mensajePorTrace(
								"***ConsultaDatosEmpresa.class  Los datos de la empresa vienen vacios >>"
										+ camposEmpresa[0] + "<<",
								EIGlobal.NivelLog.ERROR);
					} else {
						if (camposEmpresa[4].trim().length() > 11) {
							String ctaAbonofinal = camposEmpresa[4].substring(
									9, 20);
							if (ctaAbonofinal.trim().equalsIgnoreCase(
									ctaAbonoRec.trim())) {
								salida = 0; // OK
							} else {
								salida = 1; // error
							}
						}
					}
				} else {
					salida = 1; // error
				}
			} catch (Exception e) {
				salida = 1; // error
				EIGlobal.mensajePorTrace("***ConsultaDatosEmpresa.class  Excepci�n en consultaB750() >>"+ e, EIGlobal.NivelLog.ERROR);
			}
		} else {
			salida = 1;
			EIGlobal.mensajePorTrace(
					"***ConsultaDatosEmpresa.class  El contrato viene vacio ...&",
					EIGlobal.NivelLog.INFO);
		}
		return salida;
	}

	/**
	 * Desentrama datos de la empresa
	 *
	 * @param trama
	 *            trama entrada
	 * @param tokens
	 *            tokens
	 * @param separador
	 *            separadores
	 * @return aEmpresa datos empresa
	 */
	private String[] desentramaDatosEmpresa(String trama, int tokens,
			String separador) // Desentrama los datos que regresa por trama el
								// TUBO al ejecutar la CDEM
	{
		String[] aEmpresa = new String[tokens];
		int indice = trama.indexOf(separador); // String el pipe, o ;
		EIGlobal.mensajePorTrace(
				"B750_Micrositio - desentramaDatosEmpresa(): Formateando arreglo de datos.",
				EIGlobal.NivelLog.INFO);
		for (int i = 0; i < tokens; i++) {
			if (indice > 0){
				aEmpresa[i] = trama.substring(0, indice);
			}
			if (aEmpresa[i] == null){
				aEmpresa[i] = "	  ";
			}
			trama = trama.substring(indice + 1);
			indice = trama.indexOf(separador);
		}
		return aEmpresa;
	}
}