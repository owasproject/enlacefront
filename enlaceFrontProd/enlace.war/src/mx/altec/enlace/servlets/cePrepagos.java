package mx.altec.enlace.servlets;

import java.util.*;
import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.FormatosMoneda;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.*;

/**<code><B><I>cePrepagos</I></B></code>
* 22/08/2002: Se agreg&oacute; la verificaci&oacute;n de la facultad de prepago. Modificado por Hugo Ru&iacute;z Zepeda.<BR>
*/
/**
* <P>Modificaciones:<BR>
* 30/09/2002 Se corrigieron acentos. Se quit&oacute; el texto de ayuda mientras se define el mismo.<BR>
* 21/10/2002 Se cambiaron las llamadas est&aacute;ticas a <B>mensajePorTrace</B> por la llamada al objeto instanciado.<BR>
* </P>
*/

public class cePrepagos extends BaseServlet {

	String pipe         = "|";
   /*
	EI_Tipo Pagos;
	EI_Tipo Lineas;
	EI_Tipo TCTArchivo;
	EI_Tipo FacArchivo;
	EI_Tipo Seleccion;
   */


	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res ) throws IOException, ServletException {

	/*String estatusError = "";
	String Trama;
	String Result;
	String errores      = "";
	*/
	String fileOut = "";


	boolean existenCuentas;
	short  sucursalOpera;
	int salida;



		String modulo       = "";
		modulo = (String) req.getParameter( "Modulo" );
		//log( "defaultAction()::2da::Modulo =" + modulo );

		//Modificaci�n para usuarios concurrentes
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal enlaceGlobal = new EIGlobal(session , getServletContext() , this  );


		String contrato    = "";
		String usuario     = "";
		String clavePerfil = "";

//		String Trama  = "";
//		String Result = "";

		existenCuentas = false;
		sucursalOpera  = (short)787;
		salida         = 0;
      Boolean facultadPrePago;

      /*
		Pagos        = new EI_Tipo(this);
		Lineas       = new EI_Tipo(this);
		TCTArchivo   = new EI_Tipo(this);
		FacArchivo   = new EI_Tipo(this);
		Seleccion    = new EI_Tipo(this);
      */

	    FormatosMoneda FM=new FormatosMoneda();

		enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Entrando a Prepagos de Credito.", EIGlobal.NivelLog.INFO);
		boolean sesionvalida = SesionValida( req, res );
		if ( sesionvalida ) {

         /*22/08/2002 Hugo Ru�z Zepeda
         * Se valida la facultad de prepago; en caso que carezca de ella se redirige la
         * el flujo al servlet SinFacultades.
         */
         if(session.getFacultad(session.FAC_TECREPPAGOSM_CRED_ELEC))
         {
            facultadPrePago=new Boolean(session.getFacultad(session.FAC_TECREPPAGOSM_CRED_ELEC));
         }
         else
         {
            facultadPrePago=new Boolean(false);
            enlaceGlobal.mensajePorTrace("No se recibi� facultad de prepago.", EIGlobal.NivelLog.INFO);
         } // Fin if-else


         if(!facultadPrePago.booleanValue())
         {
            enlaceGlobal.mensajePorTrace("El usuario no tiene facultades para realizar prepago: "+session.getFacultad(session.FAC_TECREPPAGOSM_CRED_ELEC), EIGlobal.NivelLog.INFO);
            getServletContext().getRequestDispatcher("/SinFacultades").forward(req, res);
            return;
         } // Fin if facultad


			fileOut = session.getUserID8()+".doc";

			boolean grabaArchivo = true;

			EI_Exportar archSalida;
			archSalida = new EI_Exportar( IEnlace.DOWNLOAD_PATH + fileOut );
			archSalida.creaArchivo();


			if (modulo.equals( "0" ) )
			{

        			enlaceGlobal.mensajePorTrace("***cePrepagos.class Entrando a cePrepagos", EIGlobal.NivelLog.INFO);

					req.setAttribute("MenuPrincipal", session.getStrMenu());
				    req.setAttribute("newMenu", session.getFuncionesDeMenu());

 				    req.setAttribute("Encabezado", CreaEncabezado("Pagos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago ","s32080h", req)); // Falta Ayuda

			        req.setAttribute("Fecha",ObtenFecha());
				    String datesrvr = "<Input type = \"hidden\" name =\"strAnio\" value = \"" + ObtenAnio() + "\">";
					datesrvr += "<Input type = \"hidden\" name =\"strMes\" value = \"" + ObtenMes() + "\">";
	                datesrvr += "<Input type = \"hidden\" name =\"strDia\" value = \"" + ObtenDia() + "\">";
		            req.setAttribute("Movfechas",datesrvr);

	                session.setModuloConsultar(IEnlace.MCredito_empresarial);
                   //***********************************************

	                //TODO BIT CU3101 y CU3102  Entra al flujo

	                /**
					 * VSWF  - FVC - I
					 * 17/Enero/2007
					 */
	                if (Global.USAR_BITACORAS.equals("ON")){
	                	try {
			                BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							BitaTransacBean bt = new BitaTransacBean();
							bh.incrementaFolioFlujo(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS);//bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
							bt = (BitaTransacBean)bh.llenarBean(bt);

							bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_ENTRA);
							bt.setContrato((contrato == null)? " ":contrato);
							bt.setNombreArchivo((fileOut == null)? " ":fileOut);
							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (Exception e) {
							e.printStackTrace();
						}

					/**
					 * VSWF  - FVC - F
					 */

	                }
		           evalTemplate("/jsp/cePrepagos.jsp", req, res);
				   enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagos", EIGlobal.NivelLog.INFO);

		    }
			else if (modulo.equals( "1" ) )
			{

				String ctaSaldo     = "";
		        String tipoCuenta   = "";
		        String nombreCuenta = "";
				String Trama        = "";
				String []arrcta;
				String pipe         = "|";
				String arroba		= "@";
				String tramaCtaSaldo;
				String Result       = "";
				String tabla = "";
				String estatusError = "";
				String errores = "";
				String titulo1 = "Pagos de Cr&eacute;dito Electr&oacute;nico";
				String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago";
				String ResultError = "";
				String noLineas = "";
				String bandera = "0";

				tramaCtaSaldo = (String )req.getParameter("EnlaceCuenta");

				enlaceGlobal.mensajePorTrace("***cePrepagos.class Entrando a ceConSaldosDetalles", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***tramaCtaSaldo"+tramaCtaSaldo, EIGlobal.NivelLog.INFO);

				if (tramaCtaSaldo.indexOf("@")==-1)
			    {
				    ctaSaldo    = tramaCtaSaldo;
				    tipoCuenta   = "P";
				    nombreCuenta = "";
			    }
				else
		        {
				    arrcta       = desentramaC("3@"+tramaCtaSaldo,'@');
					ctaSaldo    = arrcta[1];
			        tipoCuenta   = arrcta[2];
			        nombreCuenta = arrcta[3];
			    }

				enlaceGlobal.mensajePorTrace("***cePrepagos.class cuentamov "+ctaSaldo, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***cePrepagos.class tipocuenta "+tipoCuenta, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***cePrepagos.class nombrecuenta "+nombreCuenta, EIGlobal.NivelLog.INFO);

				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				Hashtable htResult = null;

 				contrato      = session.getContractNumber();
            	usuario       = session.getUserID8();
        		clavePerfil   = session.getUserProfile();


	            Trama+=IEnlace.medioEntrega2  +pipe;    // 1EWEB
                Trama+=usuario          +pipe;			// empleado
                Trama+="PC50"           +pipe;			// tipo_operacion
                Trama+=contrato         +pipe;			// contrato
                Trama+=usuario          +pipe;			// empleado
                Trama+=clavePerfil      +pipe;			// clave_perfil
                Trama+=contrato         +arroba;		// contrato
                Trama+=ctaSaldo         +arroba;        // Cta cheques

				Trama+=usuario          +arroba;        // empleado
                Trama+=bandera          +pipe;			// bandera

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m�todo
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora

		 //MSD Q05-0029909 fin lineas agregada
                enlaceGlobal.mensajePorTrace(fechaHr+"cePrepagos - execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

				try {
					htResult = tuxGlobal.web_red( Trama );
					} catch ( java.rmi.RemoteException re ) {
					re.printStackTrace();
					}catch ( Exception e ){}

				Result = ( String ) htResult.get( "BUFFER" );
				enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
				ResultError = Result;

				if(Result.equals(Global.DOWNLOAD_PATH +usuario))
				{
				   enlaceGlobal.mensajePorTrace("cePrepagos.class equals(/tmp/1001851)", EIGlobal.NivelLog.INFO);
				   Result=formateaResultLinea(usuario, enlaceGlobal).trim();
				   enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Trama Salida Formatea: "+Result, EIGlobal.NivelLog.INFO);
				}
				else if(Result.substring(19,23).equals("0000"))
				{
					enlaceGlobal.mensajePorTrace("cePrepagos.class equals(0000)", EIGlobal.NivelLog.INFO);
					Result=formateaResultLinea(usuario, enlaceGlobal).trim();
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Trama Salida Formatea: "+Result, EIGlobal.NivelLog.INFO);
				}
				else
					Result="OPENFAIL";

				//Quitar (solo es simulaci�n)
//				Result=formateaResultLinea(usuario+"pag", enlaceGlobal).trim();

				if(Result.equals("") || Result.equals("NULL"))
				{
					enlaceGlobal.mensajePorTrace("cePrepagos.class - Result es null ", EIGlobal.NivelLog.INFO);
					estatusError="Su transacci�n no puede ser atendida en este momento.";
					despliegaPaginaError(estatusError,titulo1,titulo2,"s32090h", req, res );
				}
				else if(Result.equals("OPENFAIL") || Result.substring(0,5).trim().equals("ERROR") )
                {
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Estoy en el if: "+Result, EIGlobal.NivelLog.INFO);
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Estoy en el if: "+ResultError, EIGlobal.NivelLog.INFO);
//					estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
					if (ResultError.equals(Global.DOWNLOAD_PATH+"1001851"))
					estatusError="Su transacci�n no puede ser atendida en este momento.";
					else
					estatusError+=ResultError.substring(24,ResultError.length())+"<br>";
					//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
					//req.setAttribute("Errores",errores);
					despliegaPaginaError(estatusError,titulo1,titulo2,"s32090h", req, res );
				}
				else
				{
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Estoy en el else: PC50", EIGlobal.NivelLog.INFO);
					EI_Tipo ResultTmp=new EI_Tipo(this);

					Result+=" @";
					ResultTmp.strOriginal=Result;
					ResultTmp.strCuentas=Result;
					ResultTmp.numeroRegistros();
					ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
				    ResultTmp.llenaArreglo();
					ResultTmp.separaCampos();

					enlaceGlobal.mensajePorTrace("cePrepagos: Estoy en el else: PC50"+ResultTmp.strOriginal, EIGlobal.NivelLog.INFO);
					enlaceGlobal.mensajePorTrace("cePrepagos: Estoy en el else: PC50 # reg"+ResultTmp.totalRegistros, EIGlobal.NivelLog.INFO);


					noLineas += ctaSaldo+pipe+nombreCuenta+pipe;

					for (int i=0;i<ResultTmp.totalRegistros;i++)
					{

						enlaceGlobal.mensajePorTrace("cePrepagos: Estoy en el For: PC50", EIGlobal.NivelLog.INFO);
		  			    noLineas += ResultTmp.camposTabla[i][1]+pipe;

						String cuenta = new String(ResultTmp.camposTabla[i][0]);
						String noLinea = new String(ResultTmp.camposTabla[i][1]);
						String noDisp = new String(ResultTmp.camposTabla[i][2]);
						String descrip = new String(ResultTmp.camposTabla[i][3]);
						String fDisp = new String(ResultTmp.camposTabla[i][4]);
						String fVento = new String(ResultTmp.camposTabla[i][5]);
						float disponible = new Float(ResultTmp.camposTabla[i][6]).floatValue();
						float exigible = new Float(ResultTmp.camposTabla[i][7]).floatValue();

						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Autorizado: "+cuenta, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Dispuesto: "+noLinea, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Disponible: "+noDisp, EIGlobal.NivelLog.INFO);

						tabla += "<tr><td class=textabdatcla align=center><input type=radio name=ctasLineas onclick=\"javascript:radiovalor();\" value=\"5|"+cuenta+"|"+noLinea+"|"+noDisp+"|"+fVento+"|"+exigible+"|\"></td>";
					 	tabla += "<td class='textabdatcla' nowrap align=center>"+ cuenta + "</td>";
						tabla += "<td class='textabdatcla' nowrap align=center>" + noLinea + "</td>";
						tabla += "<td class='textabdatcla' nowrap align=center>" + noDisp + "</td>";
						tabla += "<td class='textabdatcla' colspan=2>" + descrip + "</td>";
						tabla += "<td class='textabdatcla' nowrap align=center>" + fDisp.replace('-','/') + "</td>";
						tabla += "<td class='textabdatcla' nowrap align=center>" + fVento.replace('-','/') + "</td>";
						tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(disponible) + "</td>" ;
						tabla += "<td class='textabdatcla' align=right nowrap> " + FormatoMoneda(exigible) + "</td>" ;
						tabla += "</tr>";
//						tabla += "<br>";
//						tabla += "<br>";


					    String lineaArchivo = "";

						lineaArchivo = cuenta +" "+  noLinea +" "+ noDisp +" "+ descrip +" "+ fDisp.replace('-','/') +" "+ fVento.replace('-','/') +" "+ FormatoMonedaExportar(disponible)+" "+ FormatoMonedaExportar(exigible);

						if (grabaArchivo)
						{
  							archSalida.escribeLinea(lineaArchivo+ "\r\n");
						}


						//TODO BIT CU3101 y CU3101 realiza la operacion PC50

					    /**
						 * VSWF - FVC - I
						 * 17/Enero/2007
						 */
						if (Global.USAR_BITACORAS.equals("ON")){
							try {
							    BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);

								bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PC50);
								bt.setContrato((contrato == null)? " ":contrato);
								bt.setNombreArchivo((fileOut == null)? " ":fileOut);
								bt.setImporte(disponible);
								bt.setCctaOrig((cuenta == null)? " ":cuenta);
								bt.setServTransTux("PC50");
								/*BMB-I*/
								if(ResultError!=null){
									if(ResultError.substring(0,2).equals("OK")){
										bt.setIdErr("PC500000");
									}else if(ResultError.length()>8){
										bt.setIdErr(ResultError.substring(0,8));
									}else{
										bt.setIdErr(ResultError.trim());
									}
								}
								/*BMB-F*/

								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						/**
						 * VSWF - FVC - F
						 */

					}// for


					if (grabaArchivo)
						archSalida.cierraArchivo();

					ArchivoRemoto archRemoto = new ArchivoRemoto();
					if(!archRemoto.copiaLocalARemoto(fileOut,"WEB"))
						enlaceGlobal.mensajePorTrace("***cePrepagos.class No se pudo crear archivo para exportar saldos", EIGlobal.NivelLog.INFO);

					String exportar = "";
   					if ( grabaArchivo )
						exportar = "<a href = \"/Download/" + fileOut + "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></A>";
					req.setAttribute("Exportar",exportar);


					req.setAttribute("valor",tabla);
					req.setAttribute("cta",ctaSaldo);
					req.setAttribute("MenuPrincipal", session.getStrMenu());
					req.setAttribute("newMenu", session.getFuncionesDeMenu());


					req.setAttribute("Encabezado", CreaEncabezado("Pagos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago","s32090h", req)); // Falta Ayuda

				    session.setModuloConsultar(IEnlace.MCredito_empresarial);

					evalTemplate("/jsp/cePrepagosDetalle.jsp", req, res);
					enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagosDetalle", EIGlobal.NivelLog.INFO);
				} //else
			} //if (modulo.equals( "1" ) )
			else if (modulo.equals( "2" ) )
			{

				String Trama        = "";
				String Trama2       = "";
//				String []arrcta;
				String pipe         = "|";
				String arroba		= "@";
				String tramaCta = "";
				String Result       = "";
				String plazo     = "";
				String tabla = "";
				String estatusError = "";
				String errores = "";
				String titulo1 = "Cotizaci&oacute;n de pagos de Cr&eacute;dito Electr&oacute;nico";
				String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago";
				String ResultError = "";
				String noLineas = "";
				String bandera = "0";
				String num_ref_error = "0";
				String banDisponible = "";
				String hora  = "";

				hora = enlaceGlobal.getHora("HH")+":"+enlaceGlobal.getHora("MM")+":"+enlaceGlobal.getHora("SS");

				enlaceGlobal.mensajePorTrace(" ---------> Hora < ------- "+hora, EIGlobal.NivelLog.INFO);

				tramaCta = (String )req.getParameter("ctaSelec");
				plazo = (String )req.getParameter("Plazo");
				banDisponible = (String )req.getParameter("disponible");

				enlaceGlobal.mensajePorTrace("***cePrepagos.class Entrando a cePrepagosCoti", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***tramaCtaSaldo"+tramaCta, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***tramaCtaSaldo"+banDisponible, EIGlobal.NivelLog.INFO);
				String[] cta_datos = null;
				String cta = null;
				String noLinea = null;
				String importe = null;
				String nDisp = null;
				String fVenc = null;
				String referencia = "";
				enlaceGlobal.mensajePorTrace("<<<<< TRAMA CUENTA 2 = " + tramaCta + ">>>>>>>", EIGlobal.NivelLog.INFO);


				if (tramaCta!=null)
				{
//					enlaceGlobal.mensajePorTrace("<<<<<  Pase el primer if >>>>>", EIGlobal.NivelLog.INFO);
					if(tramaCta.trim().length()==0) tramaCta=null;
				}

				if (tramaCta!=null) {

					cta_datos = desentramaC(tramaCta,'|');
					cta=cta_datos[1];
					noLinea=cta_datos[2];
					nDisp=cta_datos[3];
					fVenc=cta_datos[4];
					importe=cta_datos[5];
				}else
					cta=null;

				enlaceGlobal.mensajePorTrace("<<<<<  Cuenta = "+cta+" >>>>>", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("<<<<<  Linea = "+noLinea+" >>>>>", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("<<<<<  nDisp = "+nDisp+" >>>>>", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("<<<<<  Linea = "+importe+" >>>>>", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("<<<<<  Linea = "+fVenc+" >>>>>", EIGlobal.NivelLog.INFO);

//**				enlaceGlobal.mensajePorTrace("***movimientos.class cuentamov "+cta, EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class tipocuenta "+noLinea, EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class nombrecuenta "+importe, EIGlobal.NivelLog.INFO);

				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				Hashtable htResult = null;

 				contrato      = session.getContractNumber();
            	usuario       = session.getUserID8();
        		clavePerfil   = session.getUserProfile();

				if (banDisponible.equals("dispLC"))
				{

		            EIGlobal.mensajePorTrace("Estoy en  ---- >  DispLC  < ----- ", EIGlobal.NivelLog.INFO);

					Trama+=IEnlace.medioEntrega2  +pipe;    // 1EWEB
	                Trama+=usuario          +pipe;			// empleado
	                Trama+="CECT"           +pipe;			// tipo_operacion
		            Trama+=contrato         +pipe;			// contrato
	                Trama+=usuario          +pipe;			// empleado
	                Trama+=clavePerfil      +pipe;			// clave_perfil
	                Trama+=contrato         +arroba;		// contrato
	                Trama+=cta              +arroba;        // Ctacheques
					Trama+=noLinea			+arroba;        // LineaCredito
					Trama+=importe          +arroba;        // importe
					Trama+=plazo            +arroba;        // plazo
	                Trama+=" "              +pipe;			// concepto

		               enlaceGlobal.mensajePorTrace("cePrepagos - execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

					try {
						htResult = tuxGlobal.web_red( Trama );
						} catch ( java.rmi.RemoteException re ) {
						re.printStackTrace();
						}catch ( Exception e ){}

					Result = ( String ) htResult.get( "BUFFER" );
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
					ResultError = Result;

					// Quitar (simulaci�n)Para pruebas
//					Result = "OK   199640 CEES0000|21-11-2002|1002|  17.5000|              0.97|          1,008.94";

					num_ref_error = Result.substring(14,23);
					enlaceGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Trama ERROR: "+num_ref_error, EIGlobal.NivelLog.INFO);
					String aux = num_ref_error.substring(5,num_ref_error.length());
					enlaceGlobal.mensajePorTrace("ceConsultarDetalle - execute(): Codigo ERROR: "+aux, EIGlobal.NivelLog.INFO);

			        if(num_ref_error.substring(5,num_ref_error.length()).equals("0000"))
				    {

						String aux1 = Result.substring(24, Result.length());

   					    Result = aux1;

						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error = 0000:", EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error = 0000:"+Result, EIGlobal.NivelLog.INFO);

						EI_Tipo ResultTmp=new EI_Tipo(this);

						Result+=" @";
						ResultTmp.strOriginal=Result;
						ResultTmp.strCuentas=Result;
						ResultTmp.numeroRegistros();
//						ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
					    ResultTmp.llenaArreglo();
						ResultTmp.separaCampos();


						String fVento = new String(ResultTmp.camposTabla[0][0]);
						String plazo1 = new String(ResultTmp.camposTabla[0][1]);
						plazo1 = plazo1.substring(2, 4);
						String tasa = new String(ResultTmp.camposTabla[0][2]);
						String interes = new String(ResultTmp.camposTabla[0][3]);
						String total = new String(ResultTmp.camposTabla[0][4]);


						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): f.vencimiento: "+fVento, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): plazo: "+plazo1, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): tasa: "+tasa, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): interes: "+interes, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): total: "+total, EIGlobal.NivelLog.INFO);



						java.util.Date Hoy = new java.util.Date();
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Hoy: "+Hoy, EIGlobal.NivelLog.INFO);

						req.setAttribute("NoLinea",noLinea);
						req.setAttribute("Cuenta",cta);
						req.setAttribute("Plazo",plazo1);
						req.setAttribute("Tasa",tasa);
						req.setAttribute("Interes",interes);
						req.setAttribute("fDisp",enlaceGlobal.fechaHoy("dd/mm/aaaa"));
						req.setAttribute("Hora",hora);
						req.setAttribute("fVento",fVento);
						req.setAttribute("Importe",importe);
						req.setAttribute("Total",total.trim());
						req.setAttribute("MenuPrincipal", session.getStrMenu());
						req.setAttribute("newMenu", session.getFuncionesDeMenu());
						req.setAttribute("NDisp",nDisp);


						req.setAttribute("Encabezado", CreaEncabezado("Cotizaci&oacute;n de Pagos de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago","s32100h", req)); // Falta Ayuda

					    session.setModuloConsultar(IEnlace.MCredito_empresarial);

					    //TODO BIT CU3101 Realiza la operacion CECT

					    /**
						 * VSWF - FVC - I
						 * 17/Enero/2007
						 */
					    if (Global.USAR_BITACORAS.equals("ON")){
						    BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							BitaTransacBean bt = new BitaTransacBean();
							bt = (BitaTransacBean)bh.llenarBean(bt);

							bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CECT);
							bt.setContrato((contrato == null)? " ":contrato);
							bt.setImporte(Double.parseDouble((importe == null)? "0":importe));
							bt.setNombreArchivo((fileOut == null)? " ":fileOut);
							bt.setServTransTux("CECT");
							/*BMB-I*/
							if(Result!=null){
								if(ResultError.substring(0,2).equals("OK")){
									bt.setIdErr("CECT0000");
								}else if(ResultError.length()>8){
									bt.setIdErr(ResultError.substring(0,8));
								}else{
									bt.setIdErr(ResultError.trim());
								}
							}
							/*BMB-F*/

							try {
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (Exception e) {
								e.printStackTrace();
							}
					    }
						/**
						 * VSWF - FVC - F
						 */


						evalTemplate("/jsp/cePrepagosCotiza.jsp", req, res);
						enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagosCoti", EIGlobal.NivelLog.INFO);

					}else
					{

						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+Result, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+num_ref_error, EIGlobal.NivelLog.INFO);
//						estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
						estatusError+=ResultError.substring(24,ResultError.length())+"<br>";
						//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
						//req.setAttribute("Errores",errores);
						despliegaPaginaError(estatusError,titulo1,titulo2,"s32100h", req, res );

					} //else
					enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagosCoti", EIGlobal.NivelLog.INFO);

				}else if(banDisponible.equals("dispChe"))
				{

					enlaceGlobal.mensajePorTrace("Estoy en  ---- >  DispChe  < ----- ", EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class tipocuenta "+noLinea, EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class nombrecuenta "+importe, EIGlobal.NivelLog.INFO);


	 				contrato      = session.getContractNumber();
		        	usuario       = session.getUserID8();
					clavePerfil   = session.getUserProfile();

					Trama2+=IEnlace.medioEntrega1  +pipe;   // 1EWEB
			        Trama2+=usuario          +pipe;			// empleado
				    Trama2+="PD31"           +pipe;			// tipo_operacion
					Trama2+=contrato         +pipe;			// contrato
			        Trama2+=usuario          +pipe;			// empleado
			        Trama2+=clavePerfil      +pipe;			// clave_perfil
				    Trama2+=nDisp            +pipe;		    // noDisposicion



			        enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama entrada: "+Trama2, EIGlobal.NivelLog.INFO);

					try {
						htResult = tuxGlobal.web_red( Trama2);
						} catch ( java.rmi.RemoteException re ) {
						re.printStackTrace();
						}catch ( Exception e ){}

					Result = ( String ) htResult.get( "BUFFER" );
					enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
					ResultError = Result;
					// Para pruebas resultado de PD15
					// Quitar (similacion)Para pruebas
//					Result = "OK      202608 CEES0000 Transaccion ok";


					referencia = Result.substring(7,14);
					num_ref_error = Result.substring(14,23);
					enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama ERROR: "+num_ref_error, EIGlobal.NivelLog.INFO);
					String aux = num_ref_error.substring(5,num_ref_error.length());
					enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Codigo ERROR: "+aux, EIGlobal.NivelLog.INFO);

		            if(num_ref_error.substring(5,num_ref_error.length()).equals("0000"))
			        {


						req.setAttribute("Hora",hora);
						req.setAttribute("usuario",usuario);
						req.setAttribute("Ref",referencia);
						req.setAttribute("nDisp",nDisp);
						req.setAttribute("fVenci",fVenc);
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): importe : "+importe, EIGlobal.NivelLog.INFO);
						req.setAttribute("Importe",FormatoMoneda(importe));
						req.setAttribute("noLinea",noLinea);
						req.setAttribute("Cuenta",cta);
						req.setAttribute("fHoy",enlaceGlobal.fechaHoy("dd/mm/aaaa"));
						req.setAttribute("MenuPrincipal", session.getStrMenu());
						req.setAttribute("newMenu", session.getFuncionesDeMenu());


						req.setAttribute("Encabezado", CreaEncabezado("Comprobante de pago de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago","s32140h", req)); //Falta ayuda

						session.setModuloConsultar(IEnlace.MCredito_empresarial);

						//TODO BIT CU3102 realiza la operaci�n PD31

						/**
						 * VSWF - FVC - I
						 * 17/Enero/2007
						 */
						if (Global.USAR_BITACORAS.equals("ON")){
							try {
								BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
								BitaHelper bh = new BitaHelperImpl(req, session, sess);
								BitaTransacBean bt = new BitaTransacBean();
								bt = (BitaTransacBean)bh.llenarBean(bt);

								bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD31);
								bt.setContrato((contrato == null)? " ":contrato);
								bt.setImporte(Double.parseDouble((importe == null)? "0":importe));
								bt.setNombreArchivo((fileOut == null)? " ":fileOut);
								bt.setReferencia(Long.parseLong((referencia == null)? "0":referencia));
								bt.setServTransTux("PD31");
								/*BMB-I*/
								if(ResultError!=null){
									if(ResultError.substring(0,2).equals("OK")){
										bt.setIdErr("PD310000");
									}else if(ResultError.length()>8){
										bt.setIdErr(ResultError.substring(0,8));
									}else{
										bt.setIdErr(ResultError.trim());
									}
								}
								/*BMB-F*/
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						/**
						 * VSWF - FVC - F
						 */

						evalTemplate("/jsp/cePrepagosComprobP.jsp", req, res);
						enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagosCoti", EIGlobal.NivelLog.INFO);

					}
					else
					{

						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+Result, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+num_ref_error, EIGlobal.NivelLog.INFO);
//						estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
						estatusError+=ResultError.substring(24,ResultError.length())+"<br>";
						//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
							//req.setAttribute("Errores",errores);
						despliegaPaginaError(estatusError,"Comprobante de pago de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago","s32140h", req, res );

					} //else



				} // if (disponible == dispChe)

			} //if (modulo.equals( "2" ) )
			else if (modulo.equals( "3" ) )
			{

				String Trama        = "";
				String Trama2        = "";
				String []arrcta;
				String pipe         = "|";
				String arroba		= "@";
				String interes = "";
				String Result       = "";
				String tabla = "";
				String estatusError = "";
				String errores = "";
				String titulo1 = "Comprobante de pago de Cr&eacute;dito Electr&oacute;nico";
				String titulo2 = "Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago";
				String ResultError = "";
				String noLineas = "";
				String bandera = "0";
				String num_ref_error = "0";

				String cta = "";
				String lineaCredito = "";
  				String importe = "";
				String plazo = "";
				String tasa = "";
				String origenTrans = "P";
				String referencia = "";
				String hora  = "";
				String total = "";
				String NODisp = "";

								hora = enlaceGlobal.getHora("HH")+":"+enlaceGlobal.getHora("MM")+":"+enlaceGlobal.getHora("SS");

				enlaceGlobal.mensajePorTrace(" ---------> Hora < ------- "+hora, EIGlobal.NivelLog.INFO);

				cta = (String )req.getParameter("cta");
				lineaCredito = (String )req.getParameter("NoLinea");
				importe = (String )req.getParameter("Importe");
				plazo = (String )req.getParameter("Plazo");
				tasa = (String )req.getParameter("Tasa").trim();
				tasa = FM.quitaPunto(FM.daNuevaTasaFormateada(tasa));
				interes = (String )req.getParameter("Interes");
				total = (String )req.getParameter("Total");
				NODisp = (String )req.getParameter("NDisp");

				enlaceGlobal.mensajePorTrace("***cePrepagos.class Entrando a cePrepagosComprobante", EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***cta :"+cta, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***lineaCredito "+lineaCredito, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***importe "+importe, EIGlobal.NivelLog.INFO);

				enlaceGlobal.mensajePorTrace("***plazo "+plazo, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***tasa "+tasa, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***ImporteT "+total, EIGlobal.NivelLog.INFO);
				enlaceGlobal.mensajePorTrace("***NoDisp "+NODisp, EIGlobal.NivelLog.INFO);

				String importeT = plazo;

//**				enlaceGlobal.mensajePorTrace("***movimientos.class cuentamov "+cta, EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class tipocuenta "+noLinea, EIGlobal.NivelLog.INFO);
//**				enlaceGlobal.mensajePorTrace("***movimientos.class nombrecuenta "+importe, EIGlobal.NivelLog.INFO);

				ServicioTux tuxGlobal = new ServicioTux();
				//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
				Hashtable htResult = null;

 				contrato      = session.getContractNumber();
            	usuario       = session.getUserID8();
        		clavePerfil   = session.getUserProfile();



	            Trama+=IEnlace.medioEntrega2  +pipe;    // 1EWEB
                Trama+=usuario          +pipe;			// empleado
                Trama+="PD15"           +pipe;			// tipo_operacion
                Trama+=contrato         +pipe;			// contrato
                Trama+=usuario          +pipe;			// empleado
                Trama+=clavePerfil      +pipe;			// clave_perfil
                Trama+=contrato         +arroba;		// contrato
                Trama+=lineaCredito              +arroba;        // LineaCredito
				Trama+=plazo          +arroba;        // importe
//				Trama+=total.trim()          +arroba;        // importe
				Trama+=importe            +arroba;        // plazo
				Trama+=tasa             +arroba;        // tasa
                Trama+=origenTrans      +pipe;			// OrigenTransaccion

                enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

				try {
					htResult = tuxGlobal.web_red( Trama );
					} catch ( java.rmi.RemoteException re ) {
					re.printStackTrace();
					}catch ( Exception e ){}



				Result = ( String ) htResult.get( "BUFFER" );
				enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
				ResultError = Result;
				//TODO BIT CU3101 Realiza operaci�n PD15


				/**
				 * VSWF - FVC - I
				 * 17/Enero/2007
				 */
				if (Global.USAR_BITACORAS.equals("ON")){
					try {
						BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
						BitaHelper bh = new BitaHelperImpl(req, session, sess);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD15);
						bt.setContrato((contrato == null)?" ":contrato);
						bt.setImporte(Double.parseDouble((importe == null)? "0":importe));
						bt.setNombreArchivo((fileOut == null)? " ":fileOut);
						bt.setServTransTux("PD15");
						/*BMB-I*/
						if(ResultError!=null){
							if(ResultError.substring(0,2).equals("OK")){
								bt.setIdErr("PD150000");
							}else if(ResultError.length()>8){
								bt.setIdErr(ResultError.substring(0,8));
							}else{
								bt.setIdErr(ResultError.trim());
							}
						}
						/*BMB-F*/

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
				/**
				 * VSWF - FVC - F
				 */
				// Para pruebas resultado de PD15
				// Quitar (similacion)Para pruebas
//				Result = "OK      202608 CEES0000|05000476658|02|19-11-2002|27027|";

				num_ref_error = Result.substring(14,23);
				enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama ERROR: "+num_ref_error, EIGlobal.NivelLog.INFO);
				String aux = num_ref_error.substring(5,num_ref_error.length());
				enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Codigo ERROR: "+aux, EIGlobal.NivelLog.INFO);

                if(num_ref_error.substring(5,num_ref_error.length()).equals("0000"))
                {

					String aux1 = Result.substring(24, Result.length());

   				    Result = aux1;

					enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Error = 0000:", EIGlobal.NivelLog.INFO);
					enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Error = 0000:"+Result, EIGlobal.NivelLog.INFO);

					EI_Tipo ResultTmp=new EI_Tipo(this);

					Result+=" @";
					ResultTmp.strOriginal=Result;
					ResultTmp.strCuentas=Result;
					ResultTmp.numeroRegistros();
//					ResultTmp.totalRegistros = ResultTmp.totalRegistros -1;
				    ResultTmp.llenaArreglo();
					ResultTmp.separaCampos();




						String noDisp = new String(ResultTmp.camposTabla[0][0]);
						String plazoT = new String(ResultTmp.camposTabla[0][1]);
						String fVenci = new String(ResultTmp.camposTabla[0][2]);
						String noPagare = new String(ResultTmp.camposTabla[0][3]);

						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): noDisp: "+noDisp, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): plazo: "+plazo, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): fvenci: "+fVenci, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): nopagare: "+noPagare, EIGlobal.NivelLog.INFO);


						Trama2+=IEnlace.medioEntrega1  +pipe;   // 1EWEB
				        Trama2+=usuario          +pipe;			// empleado
					    Trama2+="PD31"           +pipe;			// tipo_operacion
						Trama2+=contrato         +pipe;			// contrato
				        Trama2+=usuario          +pipe;			// empleado
				        Trama2+=clavePerfil      +pipe;			// clave_perfil
					    Trama2+=NODisp            +pipe;		    // noDisposicion



				        enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama entrada: "+Trama2, EIGlobal.NivelLog.INFO);

						try {
							htResult = tuxGlobal.web_red( Trama2);
							} catch ( java.rmi.RemoteException re ) {
							re.printStackTrace();
							}catch ( Exception e ){}





						Result = ( String ) htResult.get( "BUFFER" );
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama Salida Result: "+Result, EIGlobal.NivelLog.INFO);
						ResultError = Result;
						//TODO BIT CU3101 realiza operaci�n PD31

						/**
						 * VSWF - FVC - I
						 * 17/Enero/2007
						 */
						if (Global.USAR_BITACORAS.equals("ON")){
							BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
							BitaTransacBean bt = new BitaTransacBean();
							BitaHelper bh = new BitaHelperImpl(req, session, sess);
							bt = (BitaTransacBean)bh.llenarBean(bt);

							bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD31);
							bt.setContrato((contrato == null)? " ":contrato);
							bt.setImporte(Double.parseDouble((importe == null)?" ":importe));
							bt.setNombreArchivo((fileOut == null)?" ":fileOut);
							bt.setServTransTux("PD31");
							/*BMB-I*/
							if(ResultError!=null){
								if(ResultError.substring(0,2).equals("OK")){
									bt.setIdErr("PD310000");
								}else if(ResultError.length()>8){
									bt.setIdErr(ResultError.substring(0,8));
								}else{
									bt.setIdErr(ResultError.trim());
								}
							}
							/*BMB-F*/

							try {
								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						/**
						 * VSWF - FVC - F
						 */
						// Para pruebas resultado de PD31
						// Quitar (similacion)Para pruebas
//					Result = "OK      202608 CEES0000 Transaccion ok";

						referencia = Result.substring(7,14);
						num_ref_error = Result.substring(14,23);
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Trama ERROR: "+num_ref_error, EIGlobal.NivelLog.INFO);
						String auxtemp = num_ref_error.substring(5,num_ref_error.length());
						enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): Codigo ERROR: "+auxtemp, EIGlobal.NivelLog.INFO);

			            if(num_ref_error.substring(5,num_ref_error.length()).equals("0000"))
				        {


							req.setAttribute("usuario",usuario);
							req.setAttribute("Ref",referencia);
							req.setAttribute("nDisp",noDisp);
							req.setAttribute("fVenci",fVenci);
							enlaceGlobal.mensajePorTrace("cePrepagosComprobante - execute(): importe : "+importe, EIGlobal.NivelLog.INFO);
//							req.setAttribute("Importe",importeT);
							req.setAttribute("Importe",FormatoMoneda(total));
							req.setAttribute("noLinea",lineaCredito);
							req.setAttribute("Cuenta",cta);
							req.setAttribute("fHoy",enlaceGlobal.fechaHoy("dd/mm/aaaa"));
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							req.setAttribute("Encabezado", CreaEncabezado("Comprobante de pago de Cr&eacute;dito Electr&oacute;nico","Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Pago","s32140h", req)); // Falta Ayuda


					req.setAttribute("Plazo",plazoT);
					req.setAttribute("Tasa",tasa);
					req.setAttribute("Interes",interes);
					req.setAttribute("Hora",hora);



							session.setModuloConsultar(IEnlace.MCredito_empresarial);


							evalTemplate("/jsp/cePrepagosComprobP.jsp", req, res);
							enlaceGlobal.mensajePorTrace("***cePrepagos.class SALIENDO a cePrepagosCoti", EIGlobal.NivelLog.INFO);

					}
					else
					{

						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+Result, EIGlobal.NivelLog.INFO);
						enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+num_ref_error, EIGlobal.NivelLog.INFO);
//						estatusError+=ResultError.substring(19,ResultError.length())+"<br>";
						estatusError+=ResultError.substring(24,ResultError.length())+"<br>";
						//errores+="\ncuadroDialogo('Error "+ResultError.substring(19,ResultError.length())+"', EIGlobal.NivelLog.DEBUG);";
							//req.setAttribute("Errores",errores);
						despliegaPaginaError(estatusError,titulo1,titulo2,"s32140h", req, res );

					} //else

				}
				else
				{

					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+Result, EIGlobal.NivelLog.INFO);
					enlaceGlobal.mensajePorTrace("cePrepagos.class - execute(): Error: "+num_ref_error, EIGlobal.NivelLog.INFO);
					estatusError+=ResultError.substring(24,ResultError.length())+"<br>";
					despliegaPaginaError(estatusError,titulo1,titulo2,"s32140h", req, res );

			     } //else


				enlaceGlobal.mensajePorTrace("***cePrepagosComprobante SALIENDO a cePrepagosComprobante", EIGlobal.NivelLog.INFO);
			} //if (modulo.equals( "3" ) )

		}//	if ( sesionvalida

    }


    /*************************************************************************************/
/*************************************************** formatea Reultado para Linea    */
/*************************************************************************************/
	   String formateaResultLinea(String usuario, EIGlobal enlaceGlobal)
     {

       String cadenaTCT="";
       String arcLinea="";


		// Quitar (solo es simulac�n)
	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

// 	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;

	   int totalLineas=0;

	   // **********************************************************  Copia Remota
	   ArchivoRemoto archR = new ArchivoRemoto();
	   if(!archR.copia(usuario))
		 enlaceGlobal.mensajePorTrace("ceConSaldosDetalle- formateaResultLinea(): No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
	   // **********************************************************

       EI_Exportar ArcEnt = new EI_Exportar(nombreArchivo);

       if(ArcEnt.abreArchivoLectura())
	  {
          boolean noError=true;
		  do
		  {
				 arcLinea=ArcEnt.leeLinea();
			     if(!arcLinea.equals("ERROR"))
			     {
		          if(arcLinea.substring(0,5).trim().equals("ERROR"))
			         break;
				  totalLineas++;
				  if(totalLineas>=1)
                    cadenaTCT+=arcLinea+"@";
                 }
                 else
                 noError=false;
          }while(noError);
		  ArcEnt.cierraArchivo();
       }
       else
        return "OPENFAIL";
       return cadenaTCT;



    }

	public String FormatoMonedaExportar ( double cantidad ) {
         String formato = "";
         String language = "la"; // la
         String country = "MX";  // MX
         Locale local = new Locale (language,  country);
         NumberFormat nf = NumberFormat.getCurrencyInstance (local);
         if (cantidad < 0)  {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" -"+ formato.substring (4,formato.length ());
    	    		 else
    	    			 formato =" -"+ formato.substring (2,formato.length ());
             } catch(NumberFormatException e) {}
         } else {
             formato = nf.format ( cantidad );
             try {
                 if (!(formato.substring (0,1).equals ("$")))
    	    		 if (formato.substring (0,3).equals ("MXN"))
    	    			 formato =" "+ formato.substring (3,formato.length ());
    	    		 else
    	    			 formato =" "+ formato.substring (1,formato.length ());
             } catch(NumberFormatException e) {}
         }
         return formato;
     }

}

