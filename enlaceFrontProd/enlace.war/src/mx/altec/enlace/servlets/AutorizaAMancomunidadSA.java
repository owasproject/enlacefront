/*
 * AutorizaAMancomunidadSA.java
 *
 * Created on 09 de septiembre de 2012
 *
 */

package mx.altec.enlace.servlets;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.SUALCLZS8;
import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.bo.SUAConsultaPagoAction;
import mx.altec.enlace.bo.SUAGenCompPDF;
import mx.altec.enlace.bo.SUALineaCapturaAction;
import mx.altec.enlace.dao.ConfigMancDAO;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.servicios.ValidaCuentaContrato;


/**
 *
 * @author  Stefanini
 * @version
 */
public class AutorizaAMancomunidadSA extends BaseServlet {

    private final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";
    public static final int NEXT = 1;
    public static final int PREV = 2;
	/**
	 * Initializes the servlet.
     */
    public void init (ServletConfig config) throws ServletException {
	super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    	EIGlobal.mensajePorTrace("같같같같같같같� AutorizaAMancomunidad", EIGlobal.NivelLog.DEBUG);
    	String next = request.getParameter("Next");
    	String opSubmit = "";
    	String indiceArchivo = "";
    	HttpSession sessionHttp = request.getSession();

    	if (request.getParameter("opSubmit") != null) {
    		opSubmit = request.getParameter("opSubmit");
    	}
    	if (request.getParameter("indiceArchivo") != null) {
    		indiceArchivo = request.getParameter("indiceArchivo");
    		sessionHttp.setAttribute("indiceArchivoSession",indiceArchivo);
    	}

		EIGlobal.mensajePorTrace("같같같같같같같� indiceArchivo ->" + indiceArchivo, EIGlobal.NivelLog.DEBUG);


		EIGlobal.mensajePorTrace("같같같같같같같� valor opSubmit ->" + opSubmit, EIGlobal.NivelLog.DEBUG);
    	if (opSubmit.equals("1")) {
	    	if(SesionValida (request, response)) {
			    BaseResource session = (BaseResource) request.getSession().getAttribute("session");

				/******************************************Inicia validacion OTP**************************************/
				String valida = request.getParameter ( "valida" );
				if ( next==null && validaPeticion( request, response,session,request.getSession (),valida)) {
					  EIGlobal.mensajePorTrace("\n\n ENTRO A VALIDAR LA PETICION \n\n", EIGlobal.NivelLog.DEBUG);
				}
				/******************************************Termina validacion OTP**************************************/
				else {
					boolean valBitacora = true;

					if(next==null && valida==null) {
						ArrayList notifica = new ArrayList ();
						notifica.add("");
						request.getSession().setAttribute ("datosNotificacion",notifica);

						boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(),IEnlace.MANCOMUNIDAD);

						ConfigMancDAO configMancDAO = new ConfigMancDAO();
				    	boolean regPorAut = false;

						ArrayList listResultArch = ((ArrayList) request.getSession().getAttribute("ResultadosMancomunidadSA"));
					    DatosMancSA datosArchivo = null;
					    int indiceInt = -1;

					    String cuenta = "";
					    String tipoOp = "";
					    EIGlobal.mensajePorTrace("같같같같같같같같같같 estraeOperaciones indiceArchivoSession ->" + request.getSession().getAttribute("indiceArchivoSession"), EIGlobal.NivelLog.DEBUG);

						if (request.getSession().getAttribute("indiceArchivoSession")!=null) {
							indiceInt = Integer.parseInt((String) request.getSession().getAttribute("indiceArchivoSession"));
						}

						datosArchivo = (DatosMancSA) listResultArch.get(indiceInt);

						regPorAut = configMancDAO.getRegPorAut(session.getContractNumber(), datosArchivo.getFch_registro(), datosArchivo.getFolio_archivo());

						EIGlobal.mensajePorTrace("AutorizaAMancomunidadSA.java :: valida facultad para autorizar nomina en linea", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("AutorizaAMancomunidadSA.java :: datosArchivo.getTipo_operacion(): "+datosArchivo.getTipo_operacion(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("AutorizaAMancomunidadSA.java :: FACULTADNLN: "+FACULTADNLN, EIGlobal.NivelLog.DEBUG);
						if("PNLI".equals(datosArchivo.getTipo_operacion()) && !verificaFacultad ( FACULTADNLN ,request)){
							request.setAttribute("mensajeError", "cuadroDialogo('Usuario sin facultad para autorizar N&oacute;mina en L&iacute;nea.');");
							EIGlobal.mensajePorTrace("AutorizaAMancomunidadSA.java :: Usuario sin facultad para autorizar N&oacute;mina en L&iacute;nea", EIGlobal.NivelLog.DEBUG);
							evalTemplate( "/jsp/GetAMancomunidadSA.jsp", request, response );
							return;
						}

						if (datosArchivo.getTipo_operacion().equals("IN04") ||
								datosArchivo.getTipo_operacion().equals("INTE") ||
								datosArchivo.getTipo_operacion().equals("PNOS") 
								||datosArchivo.getTipo_operacion().equals("PNLI")) {

								/* Para la cuenta cargo INTE */
								llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nom_IN," "," "," ",session.getUserID8()+".ambci",request);
								/* Para cuentas abono INTE*/
								llamado_servicioCtasInteg(IEnlace.MDep_Inter_Abono," "," "," ",session.getUserID8()+".ambca",request);

								/*paral lacuenta cargo PRE*/
								llamado_servicioCtasInteg(IEnlace.MEnvio_pagos_nomina," "," "," ",session.getUserID8()+".ambci",request);

								tipoOp = datosArchivo.getTipo_operacion().toString();
								cuenta = datosArchivo.getCta_cargo().toString();

								EIGlobal.mensajePorTrace("같같같같같같같같같같 Cuenta de cargo para validar ->" + cuenta, EIGlobal.NivelLog.DEBUG);

								if (!valCuenta(session.getContractNumber(),session.getUserID8(),session.getUserProfile(),cuenta,tipoOp))
								{
									request.setAttribute("mensajeError", "cuadroDialogo('Imposible Autorizar el archivo seleccionado, la cuenta  (" + cuenta + ")  no existe en el contrato o no est&aacute; disponible para el  servicio de autorizaci&oacute;n de n&oacute;mina.');");
									evalTemplate( "/jsp/GetAMancomunidadSA.jsp", request, response );
									return;
								}
						}
						
						if (datosArchivo.getEstatus().equals("C"))
						{
							request.setAttribute("mensajeError", "cuadroDialogo('Archivo u operaci�n previamente cancelado.', 4);");
							evalTemplate( "/jsp/GetAMancomunidadSA.jsp", request, response );
							return;
						}else if (datosArchivo.getEstatus().equals("A")) {
							request.setAttribute("mensajeError", "cuadroDialogo('Archivo previamente autorizado.', 4);");
							evalTemplate( "/jsp/GetAMancomunidadSA.jsp", request, response );
							return;
						}
						else if (!regPorAut){
							request.setAttribute("mensajeError", "cuadroDialogo('No existen m�s operaciones por procesar en el Archivo.', 4);");
							evalTemplate( "/jsp/GetAMancomunidadSA.jsp", request, response );
							return;
						}


						if( session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) &&
							session.getToken().getStatus() == 1 && solVal ) {

							request.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
							ValidaOTP.guardaParametrosEnSession(request);

							EIGlobal.mensajePorTrace("TOKEN MANCOMUNIDAD SA", EIGlobal.NivelLog.INFO);
							request.getSession().removeAttribute("mensajeSession");
						    ValidaOTP.mensajeOTP(request, "AutorizaManc_A_SA_A");

							ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
						}else{
							ValidaOTP.guardaRegistroBitacora(request,"Token deshabilitado.");
							valida="1";
							valBitacora = false;
						}
					}

					//retoma el flujo
					if((next!=null || (valida!=null && valida.equals("1")))) {
						if (valBitacora) {
							try {
								EIGlobal.mensajePorTrace( "*************************Entro cOdigo bitacorizaciOn--->", EIGlobal.NivelLog.INFO);

								HttpSession sessionBit = request.getSession(false);
								Map tmpParametros = (HashMap) sessionBit.getAttribute("parametrosBitacora");
								Map tmpAtributos = (HashMap) sessionBit.getAttribute("atributosBitacora");

								if (tmpParametros!=null && tmpAtributos!=null) {
									Enumeration enumer = request.getParameterNames();

									while(enumer.hasMoreElements()) {
										String name = (String)enumer.nextElement();
									}

									enumer = request.getAttributeNames();

									while(enumer.hasMoreElements()){
										String name = (String)enumer.nextElement();
									}
								}

								short suc_opera = Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
								int numeroReferencia  = obten_referencia_operacion(suc_opera);
								String claveOperacion = BitaConstants.CTK_OPERACIONES_MANC;;
								String concepto = BitaConstants.CTK_CONCEPTO_OPERACIONES_MANC;
								String token = "0";
								if (request.getParameter("token") != null && !request.getParameter("token").equals("")) {
									token = request.getParameter("token");
								}

								sessionBit.removeAttribute("parametrosBitacora");
								sessionBit.removeAttribute("atributosBitacora");

								EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
								EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);

								String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,"0",0.0,claveOperacion,"0","0",concepto,0);

							} catch(Exception e) {
								EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
							}
						}

						request.getSession ().setAttribute ("Next", next);

						request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
						request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
						request.getSession ().setAttribute("Encabezado", CreaEncabezado("Autorizaci&oacute;n de Operaciones Mancomunadas",
						"Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", "s26000h", request));
						EIGlobal.mensajePorTrace("같같같같같같같같같같 autorizaArchivo despues del token", EIGlobal.NivelLog.DEBUG);
						autorizaArchivo(request, response);
						EIGlobal.mensajePorTrace("\n\n\n FIN de RETOMA EL FLUJO\n\n", EIGlobal.NivelLog.DEBUG);
					}
				}
	    	}
		}else if(opSubmit.equals("2")){
			request.getSession ().setAttribute ("ResultadosMancDetalle", extraeOperaciones(request, response));
            request.getSession ().setAttribute ("IndexMancDetalle", new Integer (-50));
            pagina (request, response);
		}else {
			pagina (request, response);
		}
    }

	public ArrayList extraeOperaciones(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {
		request.getSession().removeAttribute("nominasAttr");
		ConfigMancDAO configMancDAO = new ConfigMancDAO();
		ArrayList listResultArch = ((ArrayList) request.getSession().getAttribute("ResultadosMancomunidadSA"));
		DatosMancSA datosArchivo = null;
		int indiceInt = -1;
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");

		EIGlobal.mensajePorTrace("같같같같같같같같같같 extraeOperaciones indiceArchivoSession ->" + request.getSession().getAttribute("indiceArchivoSession"), EIGlobal.NivelLog.DEBUG);

		if (request.getSession().getAttribute("indiceArchivoSession")!=null) {
			indiceInt = Integer.parseInt((String) request.getSession().getAttribute("indiceArchivoSession"));
		}

		if (indiceInt==-1 || listResultArch==null) {
			despliegaPaginaError ("Su transacci�n no puede ser atendida. Intente m�s tarde.","Consulta de Bit&aacute;cora de Mancomunidad",
  			      "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", request, response);
          return null;
		} else {
			datosArchivo = (DatosMancSA) listResultArch.get(indiceInt);
		}

		EIGlobal.mensajePorTrace("같같같같같같같같같같 estraeOperaciones listResultArch.size ->" + listResultArch.size(), EIGlobal.NivelLog.DEBUG);

		//request.getSession().removeAttribute("ResultadosMancomunidadSA");
		//request.getSession().removeAttribute("indiceArchivoSession");

		EIGlobal.mensajePorTrace("같같같같같같같같같같 extraeOperaciones getFolio_archivo ->" + datosArchivo.getFolio_archivo(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("같같같같같같같같같같 extraeOperaciones getTipo_operacion ->" + datosArchivo.getTipo_operacion(), EIGlobal.NivelLog.DEBUG);

		ArrayList resultados = null;
		try {
			EIGlobal.mensajePorTrace(":::extraeOperaciones::: ["+datosArchivo.getTipo_operacion()+"]", EIGlobal.NivelLog.DEBUG);
            if (datosArchivo.getTipo_operacion().equals("DIBT")) {
            	resultados = configMancDAO.getDetalleTI(datosArchivo.getFolio_archivo());
            	// Modificacion Mancomunidad Fase II LFER
            }else if(datosArchivo.getTipo_operacion().equals("IN04")){
            	resultados = configMancDAO.getDetalleNomInmTrad(datosArchivo.getFolio_archivo(), datosArchivo.getFch_registro());
            	request.getSession().setAttribute("nominasAttr","NOMI");
            }else if(datosArchivo.getTipo_operacion().equals("INTE")){
            	resultados = configMancDAO.getDetalleInterbancaria(datosArchivo.getFolio_archivo(), datosArchivo.getFch_registro());
            	request.getSession().setAttribute("nominasAttr","NOIT");
            }else if(datosArchivo.getTipo_operacion().equals("PNOS")){
            	resultados = configMancDAO.getDetalleNomInmTrad(datosArchivo.getFolio_archivo(), datosArchivo.getFch_registro());
            	request.getSession().setAttribute("nominasAttr","NOMI");
            	// Modificacion Mancomunidad Fase II LFER
            }else if(datosArchivo.getTipo_operacion().equals("PNLI")){
            	resultados = configMancDAO.getDetalleNominaLinea(datosArchivo.getFolio_archivo(), datosArchivo.getFch_registro());
            	request.getSession().setAttribute("nominasAttr","NOMI");
            	// Modificacion Mancomunidad Fase II Linea en linea LFER
            }else {
            	resultados = configMancDAO.getDetalleTMB(datosArchivo.getFolio_archivo(), session.getContractNumber());
            }

            EIGlobal.mensajePorTrace("같같같같같같같같같같 estraeOperaciones resultados ->" + resultados.size(), EIGlobal.NivelLog.DEBUG);

            if (resultados.size() == 0) {
            	despliegaPaginaError ("No hay registros para su consulta","Consulta de Bit&aacute;cora de Mancomunidad",
        			      "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", request, response);
                return null;
            }
		} catch (Exception ex) {
		    despliegaPaginaError ("Error al tratar de obtener la consulta", request, response);
		    EIGlobal.mensajePorTrace ("Error en consulta de Mancomunidad Archivos y Servicios: " + ex.getMessage (), EIGlobal.NivelLog.INFO);
	        EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
	    }
		return resultados;
	}


    /** Metodo para autorizar por archivo
     * @param request Objeto request del servlet
     * @param response Objeto response del servlet
     */
    private void autorizaArchivo (HttpServletRequest request, HttpServletResponse response)
    	throws ServletException, java.io.IOException {

    	BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
    	ServicioTux servicio = new ServicioTux();
    	ConfigMancDAO configMancDAO = new ConfigMancDAO();
    	boolean regPorAut = false;
    	String trama = "";
    	String result = "";
    	String codError = "";
    	String mensajeErrorFolio = "";
    	String mensajeErrorEnviar = "";
    	String mensajeErrorDispersor = "";
    	String folioArchivo = "";


		ArrayList listResultArch = ((ArrayList) request.getSession().getAttribute("ResultadosMancomunidadSA"));
		DatosMancSA datosArchivo = null;
		int indiceInt = -1;

		EIGlobal.mensajePorTrace("같같같같같같같같같같 estraeOperaciones indiceArchivoSession ->" + request.getSession().getAttribute("indiceArchivoSession"), EIGlobal.NivelLog.DEBUG);

		if (request.getSession().getAttribute("indiceArchivoSession")!=null) {
			indiceInt = Integer.parseInt((String) request.getSession().getAttribute("indiceArchivoSession"));
		}

		datosArchivo = (DatosMancSA) listResultArch.get(indiceInt);


    	String tramaParaFolio = session.getUserID8() + "|" + session.getUserProfile() + "|" + session.getContractNumber() + "|MNCA|" + datosArchivo.getRegistros() +"|";
		try {
			Hashtable hs = servicio.alta_folio(tramaParaFolio);
		 	result= (String) hs.get("BUFFER") +"";
		 } catch( java.rmi.RemoteException re ){
			EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
		 } catch(Exception e) {}

		if (result == null || "null".equals(result) || "".equals(result)) {
			result = "ERROR            Error en el servcio.";
		}

		EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: GENERADOR DE FOLIO : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);
		codError = result.substring(0, result.indexOf("|"));

		if (!codError.trim().equals("TRSV0000")) {
			mensajeErrorFolio = result.substring(result.indexOf("|") + 1, result.length());
		}
		else {
			folioArchivo = result.substring(result.indexOf("|") + 1, result.length() );
			folioArchivo = folioArchivo.trim();
			if (folioArchivo.length() < 12) {
				do {
					folioArchivo = "0" + folioArchivo;
				} while(folioArchivo.length() < 12);
			}
			if ((folioArchivo.length() != 12 ) || folioArchivo.equals("000000000000")) {
				mensajeErrorFolio = result ;
			}
			session.setFolioArchivo(folioArchivo);
			request.setAttribute("folioArchivo", folioArchivo);

			String tramaParaEnviar = session.getUserID8() + "|"
			+ session.getUserProfile() + "|"
			+ session.getContractNumber() + "|"
			+ session.getFolioArchivo() + "|MNCA|"
			+ datosArchivo.getFolio_archivo() + "|"
			+ session.getUserProfile() + " | |";

			try{
				Hashtable hs = servicio.envia_datos(tramaParaEnviar);
				result= (String) hs.get("BUFFER") +"";
			} catch(java.rmi.RemoteException re) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.ERROR);
	 		} catch(Exception e) {
	 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	 		}

	 		if(result == null || "null".equals(result) || "".equals(result)) {
	 			result = "ERROR            Error en el servicio.";
	 		}

			EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);
			codError = result.substring(0, result.indexOf("|"));

			if (!codError.trim().equals("TRSV0000")) {
				mensajeErrorEnviar = result.substring(result.indexOf("|") + 1, result.length());
				EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se enviaron los registros al controlador de registros Mensaje: " +mensajeErrorEnviar, EIGlobal.NivelLog.ERROR);
			}
			else {
				mensajeErrorEnviar = result.substring(result.indexOf("|") + 1, result.length() );
				if (mensajeErrorEnviar.equals("OK")) {
					String tramaParaDispersor = session.getUserID8() + "|"
						+ session.getUserProfile() + "|"
						+ session.getContractNumber() + "|"
						+ session.getFolioArchivo() + "|MNCA|";
					try {
				 		Hashtable hs = servicio.inicia_proceso(tramaParaDispersor);
				 		result= (String) hs.get("BUFFER") +"";
					} catch( java.rmi.RemoteException re ) {
				 		EIGlobal.mensajePorTrace(new Formateador().formatea(re), EIGlobal.NivelLog.INFO);
				 	} catch(Exception e) {
				 		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
				 	}

					if (result == null || "null".equals(result) || "".equals(result)) {
						result = "ERROR            Error en el servicio.";
					}

					EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  DISPERSOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.DEBUG);

					codError = result.substring(0, result.indexOf("|"));

					if (!codError.trim().equals("TRSV0000")) {
						mensajeErrorDispersor = result.substring(result.indexOf("|") + 1, result.length());
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias: No se realizo la dispersion de registros", EIGlobal.NivelLog.DEBUG);
					}
					else {
						mensajeErrorDispersor = result.substring(result.indexOf("|") + 1, result.length());
						EIGlobal.mensajePorTrace("transferencia.EjecutaTransferencias:  Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.DEBUG);
					}
				}
		}

		request.getSession().setAttribute("codError", codError);

		String mensaje_salida = "";
		mensaje_salida="<TABLE BORDER=0 ALIGN=CENTER CELLPADDING=3 CELLSPACING=2  width=300>";
		// EVERIS 28 Mayo 2007 Mostrar el numero de folio
		mensaje_salida += "<TR><td class=tittabcom align=center><br><i>Las operaciones est&aacute;n siendo autorizadas.</i><br>";
			mensaje_salida += "El n&uacute;mero de Folio correspondiente a esta operaci&oacute;n es: <br>";
			mensaje_salida += "<b><font color=red>" + request.getAttribute("folioArchivo") +"</font></b><br> ";
			mensaje_salida += "Utilice este n&uacute;mero de Folio para consultar las <b><font color=blue>referencias</font></b> y<br>";
			mensaje_salida += "<font color=green><b>estado</b></font> de las operaciones, en <br>";
			mensaje_salida += "<b>Consulta Folios</b>.</td> </TR>";
			mensaje_salida+="</TABLE>";

		//request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");
		//request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");

		//req.setAttribute("strDebug",strDebug);
		request.setAttribute("mensaje_salida",mensaje_salida);
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("newMenu", session.getFuncionesDeMenu());

		request.setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		request.setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");
		request.setAttribute("banco",session.getClaveBanco());
		//String Banco= session.getClaveBanco();


		//sess.setAttribute("strDebug",strDebug);
		request.getSession().setAttribute("mensaje_salida",mensaje_salida);
		request.getSession().setAttribute("MenuPrincipal", session.getStrMenu());
		request.getSession().setAttribute("newMenu", session.getFuncionesDeMenu());
		request.getSession().setAttribute("datoscontrato","\""+session.getContractNumber()+"  "+session.getNombreContrato()+"\"");
		request.getSession().setAttribute("datosusuario","\""+session.getUserID8()+"  "+session.getNombreUsuario()+"\"");

		request.setAttribute("Encabezado", CreaEncabezado("Consulta de Bit&aacute;cora de Mancomunidad ","Administraci&oacute;n y Control &gt  Mancomunidad  &gt	Consulta y Autorizaci&oacute;n","s25380h",request ));
		request.getSession().setAttribute("Encabezado", CreaEncabezado("Consulta de Bit&aacute;cora de Mancomunidad ","Administraci&oacute;n y Control &gt  Mancomunidad  &gt	Consulta y Autorizaci&oacute;n","s25380h",request ));
//		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");
		//request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_transferencias.gif\" ></a></td>");
		request.getSession().setAttribute("botonexp","<td align=center><a href = \"/Enlace/"+Global.WEB_APPLICATION+"/consultaServlet?entrada=1&opcAutMnc=S\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/consulta_folios.gif\" ></a></td>");

		request.setAttribute("web_application",Global.WEB_APPLICATION);
		request.getSession().setAttribute("web_application",Global.WEB_APPLICATION);
		request.getSession().setAttribute("banco",session.getClaveBanco());

		evalTemplate(IEnlace.MI_OPER_TMPL, request, response);


    }

/**
	 * VSWF - JGAL - SIPARE - 08-09-2009
	 *
	 * @param beanValidaciones
	 * @param request
	 * @param response
	 */

	/**
	 * ** Por folio
	 *
	 */

	private void generaComprobanteSUALC(HttpServletRequest req,
			HttpServletResponse res) throws ServletException, IOException {
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EIGlobal.mensajePorTrace("Enlace a SUAConsultaPagoServlet",
				EIGlobal.NivelLog.DEBUG);
		SUAConsultaPagoAction action = new SUAConsultaPagoAction();
		String folioSUA = req.getParameter("folioSUA");
		SUALCORM4 beanDao = action.anterioresSUALC("", folioSUA, "", "", "",
				"", 2);
		List lista = beanDao.getConsulta();
		if (beanDao.getCodErr().equals("Operacion Exitosa")) {
			if (lista.size() > 0) {
				SUALCORM4 detalle = (SUALCORM4) lista.get(0);
				double importeTotal;
				try {

					importeTotal = Double.valueOf(detalle.getImporteTotal());
					importeTotal = (importeTotal / 100);// por los decimales
				} catch (NumberFormatException exception) {
					importeTotal = 0;
				}
				String periodopagMes = null;
				String periodopagAnio = null;
				if (detalle.getPeriodoPago() != null
						&& detalle.getPeriodoPago().length() > 0) {
					try {
						periodopagAnio = detalle.getPeriodoPago().substring(0, 4);
						periodopagMes = detalle.getPeriodoPago().substring(4,
								detalle.getPeriodoPago().length());
					} catch (IndexOutOfBoundsException ibf) {
						periodopagAnio = "";
						periodopagMes = "";
					}
				} else {
					periodopagAnio = "";
					periodopagMes = "";
				}
				SUAGenCompPDF comp;
				comp = new SUAGenCompPDF(session.getContractNumber(), session
						.getNombreContrato(), session.getUserID(), session
						.getNombreUsuario(), detalle.getFolioSua(), detalle
						.getCuentaCargo(), "", detalle.getRegPatronal(),
						periodopagMes, periodopagAnio, detalle.getLinSua(),
						importeTotal, detalle.getNumeroMovimiento(),
						detalle.getTimestamp().substring(0, 10),
						detalle.getTimestamp().substring(11, 19).replace('.', ':'),
						"INTERNET", "EXITO", detalle.getMsjErr());

				pantCompSUA(req, res, comp.generaComprobanteBAOS());

			}else {
				req.setAttribute( "MenuPrincipal", "");
				String encabezado = "Consulta de Bit&aacute;cora hist&oacute;rica";
				req.setAttribute( "Encabezado", CreaEncabezado(encabezado,"Consultas &gt; Bit&aacute;cora","s25320h",req) );
				req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate( IEnlace.ERROR_TMPL, req, res );
			}
		}else {
			req.setAttribute( "MenuPrincipal", "");
			String encabezado = "Consulta de Bit&aacute;cora hist&oacute;rica";
			req.setAttribute( "Encabezado", CreaEncabezado(encabezado,"Consultas &gt; Bit&aacute;cora","s25320h",req) );
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate( IEnlace.ERROR_TMPL, req, res );
		}

	}

	/**
	 * Generacion del comprobante PDF
	 * @param req
	 * @param res
	 * @param archivo
	 * @throws ServletException
	 * @throws IOException
	 */
	private void  pantCompSUA(HttpServletRequest req, HttpServletResponse res, ByteArrayOutputStream archivo) throws ServletException, IOException
	{

		res.setHeader("Expires", "0");
		res.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		res.setHeader("Pragma", "public");
		// setting the content type
		res.setContentType("application/pdf");
		// the contentlength is needed for MSIE!!!
		res.setContentLength(archivo.size());
		// write ByteArrayOutputStream to the ServletOutputStream
		ServletOutputStream out = res.getOutputStream();
		archivo.writeTo(out);
		out.flush();
		out.close();

	}

	void grabaBita(SUALCLZS8 bo,BaseResource bs,String importe){
		EIGlobal.mensajePorTrace("AutorizaAMancomunidad - grabaBita - " +
				"Llama bitacora: " ,EIGlobal.NivelLog.DEBUG);
		try{
		if(bo!=null){
			//TODO if(bo.getMsjErr().equals("Operacion Exitosa")){
				double imp=0;
				try{
					imp=Double.parseDouble(importe);
				}catch(NumberFormatException e){
					imp=0.0;
				}catch (Exception e){
					imp=0.0;
				}
				EIGlobal.mensajePorTrace(obten_referencia_operacion
						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)) +" - "+
						("OK")+" - "+
						bs.getContractNumber()!=null?bs.getContractNumber():""+" - "+
						bs.getUserID()!=null?bs.getUserID():""+" - "+
						(bo.getCuentaCargo()!=null?bo.getCuentaCargo():"")+" - "+
						imp+" -" + ES_PAGO_SUA_LINEA_CAPTURA ,EIGlobal.NivelLog.DEBUG);
				if(bo.getFolioSua()==null || bo.getFolioSua().length()==0){
					bo.setFolioSua("0");
				}
				SUALineaCapturaAction action= new SUALineaCapturaAction();

				String llam=action.bitacoraSUALC(obten_referencia_operacion
						(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
						"SULC0000",
						bs.getContractNumber()!=null?bs.getContractNumber():"",
						bs.getUserID()!=null?bs.getUserID():"",
						(bo.getCuentaCargo()!=null?bo.getCuentaCargo():""),
						imp,ES_PAGO_SUA_LINEA_CAPTURA, "", 0, ""
						,bo.getFolioSua());
				EIGlobal.mensajePorTrace("AutorizaAMancomunidad - grabaBita - " +
						"Regreso bitacora: " + llam,EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("-----------------------------",EIGlobal.NivelLog.DEBUG);
			//TODO}
		}
		}catch(Exception e){
			EIGlobal.mensajePorTrace("Error al grabar bitacora: " + e.getMessage(),EIGlobal.NivelLog.ERROR);
		}
	}

	private void pagina (HttpServletRequest request, HttpServletResponse response)
		throws ServletException, java.io.IOException {

		int index = 0;
        int opc = NEXT;
        int paginacion = 50;
        boolean checaTodos = false;
        ArchivoRemoto archR = new ArchivoRemoto();
        ArrayList listResDetalle = new ArrayList();
        EIGlobal.mensajePorTrace("같같같같같같같같같같같같� IndexMancDetalle ->" + request.getSession ().getAttribute ("IndexMancDetalle"),EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("같같같같같같같같같같같같� OpcPag ->" + request.getParameter("OpcPag"),EIGlobal.NivelLog.DEBUG);
        try {
            index = ((Integer) request.getSession ().getAttribute ("IndexMancDetalle")).intValue();
        }catch (Exception ex) {
        }

        try {
            opc = Integer.parseInt((String) request.getParameter("OpcPag"));
        }catch (Exception ex) {
        }

        if (request.getSession().getAttribute("ResultadosMancDetalle") != null) {
        	listResDetalle = (ArrayList) request.getSession().getAttribute("ResultadosMancDetalle");
        } else {
        	return;
        }

        EIGlobal.mensajePorTrace("같같같같같같같같같같같같� pagina listResDetalle.size ->" + listResDetalle.size(),EIGlobal.NivelLog.DEBUG);

        switch (opc) {
            case NEXT: {
                if (!(index + 50 > listResDetalle.size())) {
                	index += paginacion;
                }
                break;
            }
            case PREV: {
                index -= paginacion;
                if (index < 0) {
                	index = 0;
                }
                break;
            }
        }
		EIGlobal.mensajePorTrace("같같같같같같같같같같같같� pagina index ->" + index,EIGlobal.NivelLog.DEBUG);

        File archivo = new File (IEnlace.DOWNLOAD_PATH + ((BaseResource)
        				request.getSession ().getAttribute ("session")).getContractNumber ()+ ".doc");
        FileWriter writer = new FileWriter (archivo);
        ListIterator listResultados = listResDetalle.listIterator (0);

        String nominasAttr=(String)request.getSession().getAttribute("nominasAttr");
        if(nominasAttr==null){
        	nominasAttr="";
        }

        if (listResDetalle.size()>0 && ((DatosMancSA) listResDetalle.get(0)).getTipo_operacion()!=null
        		&& (!"NOMI".equals(nominasAttr) && !"NOIT".equals(nominasAttr))) {
        	EIGlobal.mensajePorTrace("Pagina: TMB",NivelLog.DEBUG);
        	while (listResultados.hasNext()) {
	            DatosMancSA tmp = (DatosMancSA) listResultados.next();
	            writer.write (tmp.toTMBString().replace("&nbsp;", ""));
	        }
        }else if("NOMI".equals(nominasAttr) || "NOIT".equals(nominasAttr)){
        	//
        	EIGlobal.mensajePorTrace("Pagina: NOMI",NivelLog.DEBUG);
        	int contador = 0;
        	double sumatoria = 0.0f;
        	while (listResultados.hasNext()) {
        		DatosMancSA tmp = (DatosMancSA) listResultados.next();
	            try{
	            	if("NOMI".equals(nominasAttr))
	            	    writer.write (tmp.toNomiString());
	            	else
	            		writer.write (tmp.toNomiInteString());
	             sumatoria+=Double.parseDouble(tmp.getImporte());
	            }catch(NumberFormatException e){
	            	EIGlobal.mensajePorTrace("Importe incorrecto para sumatoria:"+tmp.getImporte(),NivelLog.DEBUG);
	            }catch(Exception e){
	            	EIGlobal.mensajePorTrace("Error al traer el detalle:"+e.getMessage(),NivelLog.DEBUG);
	            }
	            contador++;
	        }
        	DecimalFormat formato = new DecimalFormat ("0.00");
        	//totales
        	writer.write ("Total Archivo"+";"+contador+";"+ formato.format(sumatoria) +";");
        } else {
        	EIGlobal.mensajePorTrace("Pagina: en else",NivelLog.DEBUG);
        	while (listResultados.hasNext()) {
	            DatosMancSA tmp = (DatosMancSA) listResultados.next();
	            writer.write (tmp.toTIString().replace("&nbsp;", ""));
	        }
        }
        writer.flush ();
        writer.close ();

        if (!archR.copiaLocalARemoto(((BaseResource) request.getSession ().getAttribute ("session")).
        		getContractNumber () + ".doc", "WEB")) {
            EIGlobal.mensajePorTrace( "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
        } else {
            request.getSession().setAttribute("ArchivoManc", "/Download/" + ((BaseResource)
            		request.getSession ().getAttribute ("session")).getContractNumber () + ".doc");
        }

        listResultados = listResDetalle.listIterator (index);
        EIGlobal.mensajePorTrace("같같같같같같같같같같같같� pagina listResultados.hasNext ->" + listResultados.hasNext(),EIGlobal.NivelLog.DEBUG);

        while (listResultados.hasNext()) {
            DatosMancSA temp = (DatosMancSA) listResultados.next ();
            if (temp.pendiente()) checaTodos = true;
        }

        EIGlobal.mensajePorTrace("같같같같같같같같같같같같� pagina checaTodos ->" + checaTodos,EIGlobal.NivelLog.DEBUG);

        request.getSession ().setAttribute ("SeleccionaTodos", new Boolean (checaTodos));
        request.getSession ().setAttribute ("IndexMancDetalle", new Integer (index));
        request.getRequestDispatcher ("/jsp/GetDetalleMancSA.jsp").forward (request, response);
	}

	/**
	 * Metodo encargado de Validar la cuenta asociada al contrato
	 * @param numContrato		Numero de Contrato
	 * @param usr 				Numero de usuario
	 * @param usrPerfil 		Perfil de usuario
	 * @param numCuenta 		Numero de Cuenta
	 * @return result			Resultado esperado
	 */

	public boolean valCuenta(String contrato, String usuario, String perfil, String cuenta, String tipoOp)
	{
    	String datoscta[]	= null;
		boolean result = true;
		ValidaCuentaContrato valCtas = new ValidaCuentaContrato ();


		if(tipoOp.equals("IN04") || tipoOp.equals("PNLI"))
		{
			datoscta = valCtas.obtenDatosValidaCuenta(contrato,
													  usuario,
                                                      perfil,
                                                      cuenta,
                                                      IEnlace.MEnvio_pagos_nomina);
		}
		else if(tipoOp.equals("INTE")) {
			   datoscta = valCtas.obtenDatosValidaCuenta(contrato,
					                                  usuario,
                                                      perfil,
                                                      cuenta,
                                                      IEnlace.MCargo_transf_pesos);
		}
		else if(tipoOp.equals("PNOS")) {
			datoscta = BuscandoCtaInteg(cuenta,IEnlace.LOCAL_TMP_DIR + "/"+usuario+".ambci",IEnlace.MEnvio_pagos_nomina);
		}

		if(datoscta == null)
		{
		       result = false;
		}

		return result;
	}


    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
	processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo () {
	return "Short description";
    }

}