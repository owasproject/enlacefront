package mx.altec.enlace.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.DescargaEDCBean;

import mx.altec.enlace.beans.OW42Bean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.bo.ConsultaCtasAsociadasBO;
import mx.altec.enlace.bo.DescargaEstadoCuentaBO;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

public class DescargaPdfTdcServlet extends BaseServlet{

/********************************************************************/	
/**  Constantes locales 											*/
/********************************************************************/	
	/** Posicion de la cuenta en la cadena que se envia al momento de seleccionar una cuenta en la pantalla inicial */
	public static final int POSICION_NUMERO_CUENTA = 0;
	/** TAG para logs **/
	public static final String LOGTAG = "[Edc_PdfTdc] ::: DescargaPdfTdcServlet ::: ";
	/** Atributo del request ODD4 **/
	public static final String ODD4_BEAN_REQUEST = "odd4Bean";
	/** Atributo del request ODD4 **/
	public static final String TDC_BEAN_REQUEST = "tdcBean";
	/** Atributo del request OW42 **/
	public static final String OW42_BEAN_REQUEST = "ow42Bean";	
	/** Pantalla inicial */
	public static final String URL_INICIO = "DescargaEdoCtaPDFServlet?ventana=0";
	/** Variable para identificar la posicion del menu */
	public static final String STR_POSICION_MENU = "Estados de Cuenta &gt; Descarga de Estados de Cuenta &gt; PDF";
	/** Variable para identificar el texto: Estado de Cuenta */
	public static final String STR_ESTADO_CUENTA = "Estados de Cuenta";
	/** Constante para archivo de ayuda **/
	public static final String ARCHIVO_AYUDA = "aEDC_DescargaPDF";	
	/** Variable para identificar el texto: FechaHoy */
	public static final String STR_FECHA_HOY = "FechaHoy";
	/** Variable para identificar el menu principal */
	public static final String STR_MENU_PRINCIPAL = "MenuPrincipal";
	/** Variable para identificar el nuevo menu */
	public static final String STR_NEW_MENU = "newMenu";
	/** Variable para identificar el encabezado */
	public static final String STR_ENCABEZADO = "Encabezado";
	/** Atributo del request CambioCuentaTXT **/ 
	public static final String CAMBIO_CTATXT_REQUEST = "cambioCuentaTXT";
	/** Atributo del request URL Estado de cuenta **/
	public static final String URL_EDOCTA_REQUEST = "URLEdoCtaPDF";
	/** Atributo del request Error **/
	public static final String ERROR_REQUEST = "ERROR";
	/** JSP de Periodos**/
	public static final String JSP_PERIODOS = "/jsp/descargaEdoCtaPDF.jsp";
	/** JSP de Periodos TDC**/
	public static final String JSP_PERIODOS_TDC = "/jsp/descargaEdcPdfTdc.jsp";
	/** Serialized UID */
	private static final long serialVersionUID = -2399801289757600758L;
	/** Identificador de las sesion guardada en el request */
	private static final String SESSION_NAME = "session";
	/** Codigo de Transacciones */
	private static final String CODIGO_SERVICIO_TUX = "ODD4-OW42-WS2";
	/** JSP Inicial de seleccion de cuentas **/
	private static final String JSP_INICIO = "/jsp/descargaEdoCtaPDFInicio.jsp";
	/** Formato de la fecha */
	private static final String FORMATO_FECHAS = "dd/mm/aaaa";
	/** Atributo del request Clave Banco **/
	private static final String CVE_BANCO_REQUEST = "ClaveBanco";
	/** Atributo del request web_application **/ 
	private static final String WEB_APP_REQUEST = "web_application";
	/** Constante Clave Banco **/
	private static final String CVE_BANCO = "014" ;	
	/** Constante para mensaje de carga de **/
	private static final String STR_CARGA_FECHA_ENCAB = "%s -> Enviando fechas y encabezados";
	/** Constante para la opcion **/
	private static final String OPCIONX="opcion";

	
	/** Objeto de negocio **/
	private transient final DescargaEstadoCuentaBO descargaEstadoCuentaBO=new DescargaEstadoCuentaBO();	
	
	/**
	 * Inicia el proceso de la solicitud de Descarga de EdoCta PDF.
	 * @param request Objeto con la informacion del Request
	 * @param response Objeto con la informacion de Response
	 * @param ventana String con la informacion de la ventana
	 * @throws ServletException En caso de que exista un error en la operacion
	 * @throws IOException En caso de que exista un error en la operacion
	 */
	public void iniciaConsulta(HttpServletRequest request, HttpServletResponse response, String ventana) 
	throws ServletException, IOException {		
	    /************* Modificacion para la sesion ***************/
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		
		setRequest(request,response);
		
		if (httpSession.getAttribute(ODD4_BEAN_REQUEST)!=null) {
			EIGlobal.mensajePorTrace(String.format("%s Asegura un nuevo odd4Bean.",LOGTAG), NivelLog.DEBUG);
			httpSession.removeAttribute(ODD4_BEAN_REQUEST);
		}
		if ( ventana != null || "".equals(ventana) ) {
			/****************************** Bitacoriza *****************************/
			/** [DEDC-00] Entra a pantalla de descarga de Estados de Cuenta - PDF **/
			descargaEstadoCuentaBO.bitaAdmin(request,httpSession," "," ",CODIGO_SERVICIO_TUX, BitaConstants.DESC_EDOCTA_DEDC00, false);
		}
		/***************************** Carga Cuentas ***************************/
		request.setAttribute("ventana","0");
		request.setAttribute(OPCIONX,"");
		ctasTarjetas(request, response);
		
		/***************************** evalTemplate ****************************/
		session.setModuloConsultar(IEnlace.MOperaciones_mancom);		
		evalTemplate(JSP_INICIO, request, response );
	}	
	
	
	/**
	 * Metodo para consultar las cuentas y tarjetas a presentar en el combo
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void ctasTarjetas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setRequest(request,response);
		EIGlobal.mensajePorTrace(String.format("%s Inicia ctasTarjeta",
				LOGTAG), NivelLog.INFO);
	//	ConsultaCtasAsociadasBO consultaCtasAsociadasBO = new ConsultaCtasAsociadasBO(); 
		List<ConfEdosCtaArchivoBean> cuentas = null;new ArrayList<ConfEdosCtaArchivoBean>();
		BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		//ConCtasPdfXmlServlet pdfXml = new ConCtasPdfXmlServlet();
		ConfigModEdoCtaTDCBO configModEdoCtaTDCBO=new ConfigModEdoCtaTDCBO();
		boolean cambioContrato =ConfigMasPorTCAux.cambiaContrato(request.getSession(),  session.getContractNumber());
		//boolean cambioContrato = pdfXml.cambioContrato(request);
		
		String opcion = request.getParameter(OPCIONX) != null && !"".equals(request.getParameter(OPCIONX)) 
			? request.getParameter(OPCIONX) : ""; 
		EIGlobal.mensajePorTrace(this + "-----> opcion: " + opcion, EIGlobal.NivelLog.INFO);
		if (("".equals(opcion) && request.getSession().getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null)
				|| cambioContrato) {
			//cuentas = consultaCtasAsociadasBO.consultaCuentas(convierteUsr8a7(session.getUserID()), session.getContractNumber(), session.getUserProfile());
			cuentas = configModEdoCtaTDCBO.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()),
					session.getContractNumber(), session.getUserProfile());
			EIGlobal.mensajePorTrace("Longitud de la lista: "+ cuentas.size(),
					EIGlobal.NivelLog.DEBUG);
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			request.getSession().setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
		} else {
			EIGlobal.mensajePorTrace(String.format("%s pdfXml.filtrarDatos", LOGTAG),NivelLog.DEBUG);
			ConfigMasPorTCAux.filtrarDatos(request, response);			
		}		
		
		cuentas = (List<ConfEdosCtaArchivoBean>) request.getAttribute(EdoCtaConstantes.LISTA_CUENTAS);	
		
		if( cuentas.size() > 25 ){
			List<ConfEdosCtaArchivoBean> cuentasTarjetasAux = new ArrayList<ConfEdosCtaArchivoBean>();
			for( int i = 0 ; i < 25 ; i++ ){
				cuentasTarjetasAux.add( cuentas.get(i) );
			}
			cuentas = cuentasTarjetasAux;
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			request.setAttribute("msgOption", "La consulta gener&oacute; m&aacute;s de 25 resultados, debe ser m&aacute;s espec&iacute;fico en el filtro");
			request.setAttribute("msgExcede", "-1");
		}				
	}
	/**
	 * Metodo mostrarPeriodosPDFTDC para mostrar
	 * los periodos en pdf de las tarjetas de credito
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param txtCuenta Texto de la cuenta
	 * @throws ServletException Excepcion lanzada
	 * @throws IOException Excepcion lanzada
	 */
	public void mostrarPeriodosPDFTDC(HttpServletRequest request, HttpServletResponse response, String txtCuenta) 
			throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(String.format("%s Inicia mostrarPeriodosPDFTDC -> CuentaSeleccionada [%s]",
				LOGTAG,txtCuenta), NivelLog.INFO);
		final String[] datosCuentaSeleccionada = txtCuenta.split("\\|");
		setRequest(request,response);
		
		String numeroCuentaNuevo = request.getParameter(CAMBIO_CTATXT_REQUEST), numeroCuenta = datosCuentaSeleccionada[POSICION_NUMERO_CUENTA],
			errorCod = request.getAttribute(ERROR_REQUEST) != null ? request.getAttribute(ERROR_REQUEST).toString() : "";
		
		if(numeroCuentaNuevo == null || "".equals(numeroCuentaNuevo)) {
			EIGlobal.mensajePorTrace(String.format("%s Valor nulo o vacio para cambioCuentaTXT: [%s]", LOGTAG, numeroCuentaNuevo), NivelLog.DEBUG);
			numeroCuentaNuevo = numeroCuenta;
		}
		if("ERRORIN01".equals(errorCod) || "ERRORIN00".equals(errorCod)) {
			numeroCuenta = datosCuentaSeleccionada[POSICION_NUMERO_CUENTA];
		}
		if(numeroCuenta == null || numeroCuenta.length() == 0){
			despliegaPaginaConMensaje("Ocurrio un error al obtener los periodos", STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);
		} else {
			try {
				EIGlobal.mensajePorTrace(String.format("%s Numero de Cuenta Nuevo [%s]",
						LOGTAG, numeroCuentaNuevo), NivelLog.DEBUG);
				final HttpSession httpSession = request.getSession();
				final DescargaEDCBean edcBean=descargaEstadoCuentaBO.obtenDatosTDC(numeroCuentaNuevo);
				final OW42Bean ow42Bean = descargaEstadoCuentaBO.obtenerPeriodos(edcBean.getNumTarjeta(),edcBean.getNumCliente());
				
				String valorCambioCuenta = request.getAttribute(CAMBIO_CTATXT_REQUEST) != null 
				? request.getAttribute(CAMBIO_CTATXT_REQUEST).toString() :  edcBean.getNumTarjeta(); 
				
				EIGlobal.mensajePorTrace(String.format("%s Numero Tarjeta[%s]",LOGTAG,edcBean.getNumTarjeta()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Numero Cliente[%s]",LOGTAG,edcBean.getNumCliente()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Participante[%s]",LOGTAG,edcBean.getCodParticipante()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Numeros De TarjetaRel[%s]",LOGTAG,edcBean.getTarjetasRel()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Nombre Completo [%s]",LOGTAG,edcBean.obtenerNombreCompleto()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Domicilio Completo[%s]",LOGTAG,edcBean.obtenerDomicilioCompleto()), NivelLog.INFO);
				EIGlobal.mensajePorTrace(String.format("%s Parejas Periodo Folio[%s]",LOGTAG,ow42Bean.obtenerParejasPeriodoFolio().toString()), NivelLog.INFO);				
				
				request.setAttribute("cuentaSeleccionda", edcBean.getNumTarjeta());
				request.setAttribute("codigoCliente", edcBean.getNumCliente());
				request.setAttribute("titularCuenta", edcBean.obtenerNombreCompleto());
				request.setAttribute("domicilioRegistrado", edcBean.obtenerDomicilioCompleto());				
				request.setAttribute("periodosRecientes", ow42Bean.obtenerParejasPeriodoFolio());
				request.setAttribute(CAMBIO_CTATXT_REQUEST, valorCambioCuenta);
				httpSession.setAttribute("tarjetasRelacionadas", edcBean.getTarjetasRel());				
				httpSession.setAttribute(TDC_BEAN_REQUEST, edcBean);
				httpSession.setAttribute(OW42_BEAN_REQUEST, ow42Bean);				
				//if(odd4Bean.isTieneProblemaDatos()){
				//	request.setAttribute("MENSAJE_PROBLEMA_DATOS", odd4Bean.getMensajeProblemaDatos());
				//}
				EIGlobal.mensajePorTrace(LOGTAG+"Redireccionando evalTemplate("+JSP_PERIODOS_TDC+")", NivelLog.DEBUG);
				evalTemplate(JSP_PERIODOS_TDC, request, response );
			} catch (BusinessException e) {
				EIGlobal.mensajePorTrace(new Formateador().formatea(e), NivelLog.INFO);
				despliegaPaginaConMensaje(e.getMessage(), STR_ESTADO_CUENTA,STR_POSICION_MENU, ARCHIVO_AYUDA, request, response, URL_INICIO);				
			}
		}
	}
	
	/**
	 * Despliega una pagina de error
	 * @param error El error que se desea mostrar
	 * @param tituloPantalla Titulo de la pantalla de error
	 * @param posicionPantalla Posicion del contenido de la pantalla de error
	 * @param claveAyuda Clave de ayuda para identificar el error
	 * @param request Contiene la informacion de la peticion al servlet
	 * @param response Contiene la informacion de la respuesta del servlet
	 * @param URL Ruta a donde se va a redirigir la peticion
	 * @throws IOException En caso de error con las operaciones
	 * @throws ServletException En caso de error con las operaciones
	 */
	public void despliegaPaginaConMensaje(String error, String tituloPantalla, String posicionPantalla, String claveAyuda, 
			HttpServletRequest request, HttpServletResponse response, String URL) 
	throws IOException, ServletException{
		
		EIGlobal.mensajePorTrace("Entrando a despliegaPaginaConMensaje MENSAJE ->["+error+"]", NivelLog.DEBUG);
		
		final HttpSession sess = request.getSession();
		final BaseResource session = (BaseResource) sess.getAttribute(SESSION_NAME);
		final EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);

		request.setAttribute(STR_FECHA_HOY, EnlaceGlobal.fechaHoy( "dt, dd de mt de aaaa" ) );
		request.setAttribute("Error", error );
		
		request.setAttribute(STR_MENU_PRINCIPAL, session.getStrMenu() );
		request.setAttribute(STR_NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute(STR_ENCABEZADO, 
				CreaEncabezado(tituloPantalla, posicionPantalla, claveAyuda, request));
		
		descargaEstadoCuentaBO.agregarDatosComunes(request, session);
		request.setAttribute("URL", URL);
		
		final RequestDispatcher rdSalida = 
				getServletContext().getRequestDispatcher("/jsp/EI_Mensaje.jsp");
		rdSalida.include( request, response );
	}
	/**
	 * Metodo para agregar valores al request
	 * @since 23/04/2015
	 * @author FSW-Indra
	 * @param request HttpServletRequest informacion
	 * @param response HttpServletResponse resp
	 */
	public void setRequest(HttpServletRequest request, HttpServletResponse response){	
		final HttpSession httpSession = request.getSession();
		final BaseResource session = (BaseResource) httpSession.getAttribute(SESSION_NAME);
		final EIGlobal EnlaceGlobal = new EIGlobal(session, getServletContext(), this);
		
		request.setAttribute(STR_MENU_PRINCIPAL, session.getStrMenu());
		request.setAttribute(STR_NEW_MENU, session.getFuncionesDeMenu());
		request.setAttribute(STR_ENCABEZADO, CreaEncabezado(STR_ESTADO_CUENTA, STR_POSICION_MENU, ARCHIVO_AYUDA,request));
		request.setAttribute(STR_FECHA_HOY, EnlaceGlobal.fechaHoy(FORMATO_FECHAS));
		request.setAttribute(CVE_BANCO_REQUEST,(session.getClaveBanco() == null) ? CVE_BANCO : session.getClaveBanco());
		request.setAttribute(WEB_APP_REQUEST,Global.WEB_APPLICATION);		
		EIGlobal.mensajePorTrace(String.format(STR_CARGA_FECHA_ENCAB,LOGTAG), NivelLog.INFO);
	}
	
}