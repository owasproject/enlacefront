package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.CotizacionCE;
import mx.altec.enlace.bo.DisposicionCE;
import mx.altec.enlace.bo.FormatosMoneda;
import mx.altec.enlace.bo.InterpretaPC46;
import mx.altec.enlace.bo.InterpretaPC51;
import mx.altec.enlace.bo.InterpretaPD15;
import mx.altec.enlace.bo.TramasCE;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.rmi.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

/**
* ceVista is a blank servlet to which you add your own code.
* @author Hugo Ru&iacute;z Zepeda
* @version 2.5.0
* <P>
* Este servlet corrresponde al m&oacute;dulo de <B>Disposiciones vista</B>.<BR>
* Valida ls sesi&oacute;n, lee atributos y par&aacute;metros, efect&uacute;a las consultas a<BR>
* tuxedo, y redirige el despliegue a los <I>jsp</I>.
* </P>
* 27/05/2002 Se comenz&oacute; la documentaci&oacute;n del c&oacute;digo.
* 06/06/2002 Correcci&oacute;n restricciones
* 24/06/2002 <I>daRenglonSinLinea</I>: Si no tiene descripcion,  se asigna retorno de carro.
* 25/06/2002 Se cambi&oacute; el medio de entrega para construir trama de consulta de tasas (PC51).
* 09/07/2002 Se agreg&oacute; la llamada al servicio de cuentas.
* 16/07/2002 <I>total</I> se obtiene cada vez que se van a deplegar las cuentas.
* 01/08/2002 Se cambiaron los encabezados que aparecen en el <I>jsp<I>.<BR>
* 09/08/2002 Se comentarorn las llamadas al <I>garbage collector</I>.<BR>
* 15/08/2002 Se agreg&oacute; las facultades de saldo y disposici&oacute;n.
* 21/08/2002 S&oacute;lo se verifica la facultad de disposicion; se redirige el flujo a <I>SinFacultades</I><BR>
* si el usuario carece de esta facultad.<BR>
* 23/08/2002 Estandarizaci&oacute;n de variables.<BR>
* 30/08/2002 Sustituci&oacute;n caracteres por c&oacute;digos escape.<BR>
* 17/09/2002 Cambio color cuando s&iacute; existe l&iacute;nea de cr&eacute;dito.<BR>
* 18/09/2002 Se coment&oacute; un c&oacute;digo sin uso en <I>consultaLineas</I> y se regresa nulo en <I>calculaDisposicion</I> si la trama de respuesta no es correcta.<BR>
* 29/09/2002 Se cambi&oacute; un t&iacute;tulo y se elimin&oacute; un salto de carro.   Qued&oacute; pendiente probar una nueva<BR>
* forma de redireccionar la ventana de tasas.
* 30/09/2002 Se cambi&oacute; la forma de redireccionar la ventana de plazos (tasas).<BR>
* 01/10/2002 Se cambi&oacute; la forma de redireccionar la ventana emergente del comprobante.<BR>
* 02/10/2002 Se cambi&oacute; la forma de redireccionar la ventana de error cuando no obtiene datos de plazos (tasas).<BR>
* 08/10/2002 Se restringi&oacute; consultas l&iactue;neas de cr&eacute;dito a las que comienzan con 51, 52, 53, 54, 55, 65 y 22<BR>
* 10/10/2002 Se hicieron cambios por el cambio en el manejo del objeto sesi&oacute;n.<BR>
* 11/10/2002 Se modific&oacute; <B>consultaLineas</B> para agregar el par&aacute;metro <I>usuario</I> y borrar<BR>
* el archivo antes de efectuar la consulta de tuxedo.<BR>
* 11/10/2002 Se modificaron los m&eacute;tos relacionados con la sesi&oacute; y las facultades.<BR>
* 12/10/2002 Cambio redirecci&oacute;n cuando no tiene facultades.<BR>
* 14/10/2002 Uso del separador de archivos <B>sa</B>. Env&iacute;a l&iacute;nea cr&eacute;dito a&uacute;n cuando no se tiene informaci&oacute;n de tasas y <B>ClaveBanco</B>.<BR>
* 18/10/2002 Se agregaron mensajes a la bit&aacute;cora.<BR>
* 23/10/2002 Se cambiaron par&aacute;metros por atributos.  Se cambi&oacute; el manejo del comprobante.<BR>
* 25/10/2002 Se cambi&oacute; <B>consultaLineas</B> por los problemas en implantaci&oacute;n.   Copia remota.<BR>
* 29/10/2002 Se cambi� la forma en que abre la ventana emergente para tasas.  Se suben atributos en vez de par&aacute;metros.<BR>
* 31/10/2002 Cambio par&aacute;metros por atributos cuando no se obtiene informaci&oacute;n de tasas.<BR>
* 01/11/2002 Se agregan datos de la cuenta en el <I>Bean</I> de cotizaci&oacute;n.<BR>
* 07/11/2002 Se cambia el separador de campos para el archivo por una espacio.  Se cambi&oacute; el tipo de retorno para <B><I>calculaDisposicion</I></B>.<BR>
* 07/11/2002 Se corrige si el importe no es num&acute;rico.<BR>
* 08/11/2002 Se verifica que el concepto pueda ser una cadena vac&iacute;a pero no nula.<BR>
* 14/11/2002 Se cambian encabezados y se agregan los men&uacute;es de ayuda.<BR>
* 18/11/2002 Valor para la liga de <I>siguientes</I>.<BR>
* 19/11/2002 Se termin&oacute; la simulaci&oacute;n de registros (cuentas) por p&aacute;gina.<BR>
* 21/11/2002 Se formatea el importe que se despliega en el comprobante.<BR>
* 22/11/2002 Ortograf&iactue;a t&iacute;tulos.<BR>
* 03/12/2002 Se modific&oacute; <B><I>creaArchivo</I></B> para asegurar que se cierre el archivo<BR>
* si se presentan problemas de escritura en el mismo.  Se agrega fecha y hora a la ventana de plazos (tasas).<BR>
* 10/12/2002 Se quitaron variables no usadas y algunas instancias se convirtieron en variables locales.<BR>
* 18/12/2002 Se especifica al llamar las cuentas que sean las de cr&eacute;dito empresarial (cr&eacute;dito electr&oacute;nico)<BR>.
*/
public class ceVista extends BaseServlet
{

   /**Versi&oacute;n.<BR>
   */
   public static final String version="2.5.0";


  /**Ruta absoluta al directorio temporal de UNIX.
  */
  //private static String tmp=File.separator+"tmp"+File.separator;
  private static String tmp=IEnlace.DOWNLOAD_PATH;


   /**Constante de separador de archivo.
   */
   private static String sa=File.separator;



  /**Este objeto permite efectuar las consultas a loas servicios de tuxedo.
  */
  //private ServicioTux tuxedo;

  /**T&iacute;tulo para el jsp.
  */
  private String titulo="Disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico";

  /**Men&uacute; para indicar la navegaci&oacute;n dentro de la aplicaci&oacute;n
  */
  private String menuNavegacion="Tesorer&iacute;a &gt; Cr&eacute;dito &gt; Cr&eacute;dito Electr&oacute;nico &gt; Disposici&oacute;n";


  /**Indica el proceso en el cual se encuentra.
  */
  //private int numeroControl=0;

  /**Las cuentas de cheques que comienzan con estos dos n&uacute;meros pueden tener l&iacute;neas de cr&eacute;dito.
  */
  public static final int[] prefijosChequeras={51, 55, 65, 22};

    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }



    /**
    ** <code>displayMessage</code> allows for a short message to be streamed
    ** back to the user.  It is used by various NAB-specific wizards.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @param messageText the message-text to stream to the user.
    */
    public void displayMessage(HttpServletRequest req,
                      HttpServletResponse res,
                      String messageText)
                    throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();
        out.println(messageText);
    }

   /**Este m&eacute;todo inicia <I>enlaceGlobal</I> y estable la configuraci&oacute;n<BR>
   * para disponer de los servicios de tuxedo.
   */
    public void init()
    {
      //enlaceGlobal=new EIGlobal(this);

      EIGlobal.mensajePorTrace("Iniciando servlet \"ceConPosicion\".", EIGlobal.NivelLog.INFO);
      //tuxedo=new ServicioTux();
      //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
      //confirma=new StringBuffer("");
      //System.out.println("Iniciando servlet \"ceConPosicion\".");
    } // Fin m�todo init


   /** Este m&eacute;todo destruye al servlet al tirarse el servidor.
   */
    public void destroy()
    {
      EIGlobal.mensajePorTrace("Destruyendo servlet \"ceConPosicion\".", EIGlobal.NivelLog.INFO);
      //tuxedo=null;
    } // Fin m�todo destroy

    /**
    * <code>defaultAction</code> is the default entry-point for iAS-extended
    * <BR>Este m&eacute;todo responde a las peticiones "get" y "post" del iAS.<BR>
    * <P>
    * Lee los par&aacute;metros y las variables de sesi&oacute;n procesando la petici&oacute;n<BR><BR>
    * La variable <I>pantalla</I> indica el proceso que se efectuar&aacute;, redireccionando
    * el flujo a su respectiva pantalla y desplegando sus datos de la siguiente forma:<BR><BR>
    * <B><I>Pantalla=0</I></B> o el valor por defecto: Lee las cuentas de diez en diez;
    * si la linea es de tipo "P" llama a <I>consultaLineas</I>, de lo contrario guarda las l&iacute;neas
    * crea un rengl&oacute;n sin datos.
    * </P>
    * @param peticion es un objeto de tipo <I>HttpServletRequest</I> que controla las llamadas al servlet.
    * @param respuesta es un objeto de tipo <I>HttpServletResponse</I> con el cual redirige la salida al jsp.
    * @exception javax.servlet.ServletException es arrojada cuando hay una excepci&oacute;n con el servlet.
    * @exception java.io.IOException es arrojada cuando hay una excepci&oacute:n de entrada/salida.
    */
    public void defaultAction(HttpServletRequest peticion, HttpServletResponse respuesta)
                   throws ServletException, IOException
    {
      //ServicioTux st;
      String pantalla=null;
      String contrato;
      String usuario;
      String perfil;
      String cuenta;
      String lineaCredito;
      String descripcion;
      String importe;
      String plazo;
      String concepto;
      String cadIncluirConcepto;
      String datosCuenta;
      int incluirConcepto;
      String cadInicio, cadFin, cadTotal, cadAcumulado;
      String tramaPC46;
      //String nombreArchivoPC46;
      //File archivoPC46;
      int numeroPantalla=0;
      //PrintWriter salida;
      String[][] cuentas=null;
      int contador=0, residuo=-1, maximo=-1, chequera=0;
      StringBuffer tabla=new StringBuffer("");
      //String datosArchivo=new String("");
      int inicio=0, fin=0, total=0, acumulado=0, anterior=0, aux3=-1;
      //InterpretaPC46 sinDatos=new InterpretaPC46();
      StringBuffer datosArchivo=new StringBuffer("");
      DisposicionCE dispone;
      String rutaServidor="", relativa=null, esteServlet=null;
      String monto;
      String tasa;
      String importeTotal;
      String interes;
      String fechaVencimiento;
      StringBuffer mensajeConfirmacion=null;
      String ligaTasas=null;
      int numeroRenglonesDesplegados=0;
      Boolean facultadDisposicion;
      HttpSession sesion;
      EIGlobal enlaceGlobal;
      String confirma=new String("");
      String mensajeErrorDatos;


      System.gc();
      sesion=peticion.getSession();
      BaseResource sesionBS =(BaseResource)sesion.getAttribute("session");
      enlaceGlobal=new EIGlobal(sesionBS, getServletContext(), this);

      if(SesionValida(peticion, respuesta))
      {
        //respuesta.setContentType("text/html");
        //salida=respuesta.getWriter();

         /*
		  salida.println(session.getFAC_TECRESALDOSC_CRED_ELEC());
		  salida.println("<BR>");
		  salida.println(session.getFAC_TECREDISPOSC_CRED_ELEC());
        */

         /*
        if(session.getFAC_TECREDISPOSC_CRED_ELEC()!=null)
        {
            facultadDisposicion=new Boolean(session.getFAC_TECREDISPOSC_CRED_ELEC());
        }
        else
        {
            facultadDisposicion=new Boolean(false);
            enlaceGlobal.mensajePorTrace("No se obtuvo la facultad de disposici�n.", EIGlobal.NivelLog.INFO);
        } // Fin if-else facultadDisposicion
        */


         //if(!facultadDisposicion.booleanValue())
         if(!sesionBS.getFacultad(BaseResource.FAC_TECREDISPOSC_CRED_ELEC))
         {
            enlaceGlobal.mensajePorTrace("El usuario no tiene facultades de disposici�n. ", EIGlobal.NivelLog.INFO);
            //getServletContext().getRequestDispatcher("/SinFacultades").forward(peticion, respuesta);
            peticion.getRequestDispatcher("/SinFacultades").forward(peticion, respuesta);
            return;
            //peticion.getRequestDispatcher("SinFacultades").forward(peticion, respuesta);
         } // Fin if facultad disposici�n

        peticion.setAttribute("MenuPrincipal", sesionBS.getStrMenu());
		  peticion.setAttribute("newMenu", sesionBS.getFuncionesDeMenu());


        dispone=(DisposicionCE)sesion.getAttribute("cotiza");

         /*
        contrato=(String)sesion.getAttribute(atributoContrato);
        usuario=(String)sesion.getAttribute(atributoUsuario);
        perfil=(String)sesion.getAttribute(atributoPerfil);
        */
        contrato=sesionBS.getContractNumber();
        usuario=sesionBS.getUserID8();
        perfil=sesionBS.getUserProfile();

        mensajeErrorDatos=(String)peticion.getAttribute("mensajeErrorDatos");
        cuenta=peticion.getParameter("cuenta");
        lineaCredito=peticion.getParameter("lineaCredito");
        descripcion=peticion.getParameter("descripcion");
        pantalla=peticion.getParameter("pantalla");
        cadAcumulado=peticion.getParameter("acumulado");
        importe=peticion.getParameter("importe");
        plazo=peticion.getParameter("plazo");
        concepto=peticion.getParameter("concepto");
        cadIncluirConcepto=peticion.getParameter("incluirConcepto");
        datosCuenta=peticion.getParameter("datosCuenta");
        monto=peticion.getParameter("monto");
        tasa=peticion.getParameter("tasa");
        importeTotal=peticion.getParameter("importeTotal");
        interes=peticion.getParameter("interes");
        fechaVencimiento=peticion.getParameter("fechaVencimiento");
        //mensajeConfirmacion=peticion.getParameter("mensajeConfirmacion");
        //datosArchivo=new String("");

         //salida.println((getServletContext()).getRealPath("ceVista")+"<BR>");

         /*
         ligaTasas=peticion.getRealPath("");
         salida.println(ligaTasas+"<BR>");
         salida.println(peticion.getContextPath()+"<BR>");
         salida.println(peticion.getPathInfo()+"<BR>");
         salida.println(peticion.getPathTranslated()+"<BR>");
         salida.println(peticion.getRequestURI()+"<BR>");
         salida.println(peticion.getRequestURL()+"<BR>");
         salida.println(peticion.getServletPath()+"<BR>");
         */

         //salida.println(getServletContext().getRealPath("/"));

         ligaTasas="http://"+peticion.getServerName()+":"+peticion.getServerPort()+"/"+peticion.getContextPath()+"/jsp/tasasCE01.jsp";
        // salida.println(ligaTasas+"<BR>");

        if(pantalla==null || pantalla.length()<1)
          pantalla="";


        try
        {
          numeroPantalla=Integer.parseInt(pantalla);
        }
        catch(NumberFormatException nfe01)
        {
          numeroPantalla=0;
          enlaceGlobal.mensajePorTrace("No se obtuvo numero pantalla", EIGlobal.NivelLog.INFO);
        } // Fin try-catch pantalla

        if(dispone!=null)
        {
          if(dispone.getErrores().size()>0)
            numeroPantalla=3;
        } // Fin if



        switch(numeroPantalla)
        {
          case 0:
          default:
                //HttpSession sesionActual=peticion.getSession();
                //session.removeAttribute("mensajeConfirmacion");
                getServletContext().removeAttribute("mensajeConfirmacionCE");
                //numeroControl=0;

                peticion.removeAttribute("cotizacion");
                peticion.removeAttribute("cot01");
                peticion.removeAttribute("disposicion");
                peticion.removeAttribute("disp02");

                numeroRenglonesDesplegados=0;
                InterpretaPC46 sinDatos=new InterpretaPC46();
                String datosCL[]={contrato, usuario, perfil};
                int restan=0;

                if(verificaDatos(datosCL, enlaceGlobal))
                {

                  try
                  {
                     acumulado=Integer.parseInt(cadAcumulado);
                  }
                  catch(NumberFormatException nfe04)
                  {
                     acumulado=0;
                     enlaceGlobal.mensajePorTrace("No se obtuvo inicio: "+nfe04.getMessage(), EIGlobal.NivelLog.INFO);
                  } // Fin try-catch inicio

                  if(acumulado<0)
                     acumulado=0;

                  //sesionBS.setModuloConsultar(IEnlace.MCredito_empresarial);

                  if(acumulado==0)
                  {
                     llamado_servicioCtasInteg(IEnlace.MCredito_empresarial," "," "," ",sesionBS.getUserID8()+".ambci", peticion);
                     //total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambci");
                  } // Fin if creaci�n cuentas

                  //confirma=new StringBuffer("");
                  confirma=null;
                  //System.gc();
                  inicio=acumulado;
                  fin=acumulado+Global.NUM_REGISTROS_PAGINA;
                  total=getnumberlinesCtasInteg(IEnlace.LOCAL_TMP_DIR + "/"+sesionBS.getUserID8()+".ambci");
                  cuentas=ObteniendoCtasInteg(inicio, fin, IEnlace.LOCAL_TMP_DIR + "/"+usuario+".ambci");


                  if(fin>total)
                     fin=total;

                  restan=total-fin;

                  try
                  {
                     maximo=Integer.parseInt(cuentas[0][0]);
                     //salida.println("Maximo:"+maximo+"\n<BR>");
                  }
                  catch(NumberFormatException nfe)
                  {
                     //salida.println("No hay maximo: "+nfe.getMessage());
                     maximo=0;
                  } // Fin try-catch maximo

                  TramasCE tce=new TramasCE();
                  tce.setMedioEntrega(TramasCE.ARCHIVO);

                  for(int i=1;i<=maximo;i++)
                  {
                     //tabla+ =consultaLineas(cuentas[i][1], cuentas[i][4]);

                     try
                     {
                        chequera=Integer.parseInt(cuentas[i][1].substring(0, 2));
                     }
                     catch(NumberFormatException nfe)
                     {
                        chequera=0;
                     } // Fin try-catch conversion

                     if(cuentas[i][2].equalsIgnoreCase("P") && ((chequera>=prefijosChequeras[0] && chequera<=prefijosChequeras[1]) || chequera==prefijosChequeras[2] || chequera==prefijosChequeras[3]) )
                     {
                        String aux1;
                        String[] aux2;
                        int aux4=0;

                        tramaPC46=tce.tramaConsultaLineasCredito(usuario, contrato, perfil, cuentas[i][1]);



                       /* Aqu� no se indica la l�nea -internamente coloca un espacio en dicho campo.
                        */
						 //MSD Q05-0029909 inicio lineas agregadas
						 String IP = " ";
						 String fechaHr = "";												//variables locales al m�todo
						 IP = peticion.getRemoteAddr();											//ObtenerIP
						 java.util.Date fechaHrAct = new java.util.Date();
						 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
						 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
						 //MSD Q05-0029909 fin lineas agregada

					    enlaceGlobal.mensajePorTrace(fechaHr+"Trama consulta l�neas CE: "+tramaPC46, EIGlobal.NivelLog.INFO);

                        aux2=consultaLineas(tramaPC46, cuentas[i][4].trim(), numeroRenglonesDesplegados, usuario, enlaceGlobal);
                        aux1=aux2[0];

                        try
                        {
                           aux4=Integer.parseInt(aux2[2]);
                        }
                        catch(NumberFormatException nfe)
                        {
                           aux4=0;
                        } // Fin try-catch

                        numeroRenglonesDesplegados+=aux4;

                        //salida.println("Trama PC46: "+tramaPC46+" ***");
                        //salida.println("<BR>");

                        if(aux1!=null)
                        {
                           tabla.append(aux1);
                           //salida.println("Datos tabla:"+tabla);
                           //salida.println("<BR>");
                           datosArchivo.append(aux2[1]);
                        }
                        else
                        {
                           //tabla+ =daRenglonSinLinea(i, cuentas[i][1], cuentas[i][4]);
                           tabla.append(sinDatos.daRenglonSinLinea(numeroRenglonesDesplegados, cuentas[i][1], cuentas[i][4]));
                           //salida.println(sinDatos.getTodosLosDatos());
                           //salida.println("<BR>");
                           datosArchivo.append(sinDatos.getTodosLosDatos());
                           datosArchivo.append("\n");
                           numeroRenglonesDesplegados++;
                           //salida.println(datosArchivo+"\n<BR>");
                        } // Fin if aux1



                     }
                     else
                     {
                        //tabla+ =daRenglonSinLinea(i, cuentas[i][1], cuentas[i][4]);
                        tabla.append(sinDatos.daRenglonSinLinea(numeroRenglonesDesplegados, cuentas[i][1], cuentas[i][4]));
                        //tabla+ ="<BR>";
                        tabla.append("\n");
                        datosArchivo.append(sinDatos.getTodosLosDatos());
                        datosArchivo.append("\n");
                        numeroRenglonesDesplegados++;
                        //salida.println(datosArchivo+"\n<BR>");
                     } // Fin if 'P'





                  } // Fin for i

                  acumulado+=Global.NUM_REGISTROS_PAGINA;

                  if(creaArchivo(datosArchivo.toString(), usuario+".doc", enlaceGlobal))
                  {
                     enlaceGlobal.mensajePorTrace("Se creo el archivo disposiciones.", EIGlobal.NivelLog.INFO);
                  }
                  else
                  {
                     enlaceGlobal.mensajePorTrace("No se creo el archivo disposiciones", EIGlobal.NivelLog.INFO);
                  } // Fin if archivo


                  //salida.println(datosArchivo);
                  //salida.println("<BR>");

                   /*
                   relativa=peticion.getRequestURI();
                   esteServlet=peticion.getServletPath();

                   if(relativa!=null && esteServlet!=null)
                   {
                     aux3=relativa.indexOf(esteServlet);

                     if(aux3>0)
                     {
                        //rutaServidor=peticion.getServerName()+":"+peticion.getServerPort()+relativa.substring(0, aux3)+"jsp/tasasCE.jsp";
                     }
                  } // Fin if nulos

                  salida.println("Relativa: "+relativa);
                  salida.println("Servlet: "+esteServlet);
                  salida.println("Posicion: "+aux3);
                  salida.println("Servidor: "+peticion.getServerName());
                  salida.println("Puerto: "+peticion.getServerPort());
                  */

                  // En la siguiente l�nea se simulan l�neas de cr�dito
                  //tabla+ =(new InterpretaPC46()).daRenglonesSimulados();

                  //numeroControl=2;

                  if(mensajeErrorDatos!=null && mensajeErrorDatos.length()>0)
                     peticion.setAttribute("mensajeErrorDatos", mensajeErrorDatos);

                  peticion.setAttribute("Encabezado", CreaEncabezado(titulo, menuNavegacion, "s32110h", peticion));
                  peticion.setAttribute("acumulado", (new Integer(acumulado)).toString());
                  peticion.setAttribute("tabla", tabla.toString());
                  peticion.setAttribute("total", (new Integer(total)).toString());
                  peticion.setAttribute("restan", (new Integer(restan)).toString());
                  peticion.setAttribute("archivo", sa+"Download"+sa+usuario+".doc");
                  //evalTemplate("/jsp/disposicionCE01.jsp?pantalla=0&acumulado="+acumulado+"&tabla="+tabla+"&total="+total+"&archivo="+sa+"Download"+sa+usuario+".doc", peticion, respuesta);

                  //TODO BIT CU3111 inicia el flujo

                  	/**
					 * VSWF - FVC - I
					 * 17/Enero/2007
					 */
                  if (Global.USAR_BITACORAS.equals("ON")){
	                  try {
						BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
						BitaTransacBean bt = new BitaTransacBean();
						bh.incrementaFolioFlujo((String)peticion.getParameter(BitaConstants.FLUJO));
						bt = (BitaTransacBean)bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_DISPOS_ENTRA);
						bt.setContrato((contrato == null)?" ":contrato);
						//bt.setNombreArchivo(usuario + ".doc");

							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}

						//TODO BIT CU3111 llama servicio tuxedo
						try {
						BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
						BitaTransacBean bt = new BitaTransacBean();
						bh = new BitaHelperImpl(peticion, sesionBS, sesion);
						bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);

						bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_DISPOS_LLAMA_SERV_TUX);
						bt.setContrato(contrato);
						bt.setServTransTux("PC46");
						bt.setNombreArchivo(usuario + ".doc");


							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
                  }
					/**
					 * VSWF - FVC - F
					 */

                  evalTemplate("/jsp/disposicionCE01.jsp", peticion, respuesta);

                  /*
                  peticion.setAttribute("acumulado", new Integer(acumulado));
                  peticion.setAttribute("tabla", tabla);
                  peticion.setAttribute("total", new Integer(total));
                  peticion.setAttribute("archivo", "/Download/"+usuario+".doc");
                  evalTemplate("/jsp/disposicionCE01", peticion, respuesta);
                  */
                  //sesionActual=null;
                  //System.gc();
                }
                else
                {
                  enlaceGlobal.mensajePorTrace("Los atributos de sesi�n son nulos (consulta l�neas de cr�dito).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("Los atributos de sesi&oacute;n son nulos (consulta l&iacute;neas de cr&eacute;dito).", titulo, menuNavegacion, "s32110h", peticion, respuesta);
                } // Fin if-else verificaDatos
                break;
          case 1:
                String consultaTasas=null;
                String tasas=null;
                boolean verificacion=true;
                String datosTasas[]={contrato, usuario, perfil, lineaCredito};

                getServletContext().removeAttribute("datosTasasCE");
                getServletContext().removeAttribute("lineaCreditoCE");
                getServletContext().removeAttribute("claveBancoCE");
                getServletContext().removeAttribute("fechaCE");
                getServletContext().removeAttribute("horaCE");

                if(verificaDatos(datosTasas, enlaceGlobal))
                {
                   TramasCE consulta=new TramasCE();
                   InterpretaPC51 pc51=new InterpretaPC51();

                   /*
                   StringTokenizer obtieneLinea=new StringTokenizer(datosCuenta, "|");
                   obtieneLinea.nextToken();
                   lineaCredito=obtieneLinea.nextToken();
                   */



                   consulta.setMedioEntrega(TramasCE.TRAMA);
                   consultaTasas=consulta.tramaConsultaTasas(usuario, contrato, perfil, lineaCredito);
				 //MSD Q05-0029909 inicio lineas agregadas
				 String IP = " ";
				 String fechaHr = "";												//variables locales al m�todo
				 IP = peticion.getRemoteAddr();											//ObtenerIP
				 java.util.Date fechaHrAct = new java.util.Date();
				 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
				 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
				 //MSD Q05-0029909 fin lineas agregada


				 EIGlobal.mensajePorTrace(fechaHr+"ceVista - Trama: "+consultaTasas, EIGlobal.NivelLog.DEBUG);

                   tasas=consultaDisposicion(consultaTasas, enlaceGlobal);
                   pc51.setTramaRespuesta(tasas);


                   // Se simula la respuesta de consulta de tasas
                   //pc51.setTramaRespuesta("OK      198404 CEES0000/tmp/pruebaTasas.txt");

                   if(pc51.interpretaTrama())
                   {
                     //salida.println("Trama de respuesta correcta.<BR>");
                     enlaceGlobal.mensajePorTrace("Trama de respuesta correcta.", EIGlobal.NivelLog.DEBUG);

                   }
                   else
                   {
                     //salida.println("Trama de respuesta tasas incorrecta.<BR>");
                     enlaceGlobal.mensajePorTrace("Trama de respuesta tasas incorrecta.", EIGlobal.NivelLog.DEBUG);
                     verificacion=false;
                   } // Fin if interpreta

                   ArchivoRemoto ar= new ArchivoRemoto();

                   if(ar.copia(usuario))
                   {

                     if(pc51.leeArchivo())
                     {
                        //salida.println("Se ley&oacute; el archivo de tasas.<BR>");
                        //salida.println(pc51.getCadenaDatos()+"<BR>");
                        enlaceGlobal.mensajePorTrace("Se ley&oacute; el archivo de tasas.", EIGlobal.NivelLog.INFO);
                     }
                     else
                     {
                        //salida.println("No se pudo leer el archivo de tasas.<BR>");
                        enlaceGlobal.mensajePorTrace("No se pudo leer el archivo de tasas.", EIGlobal.NivelLog.INFO);
                        verificacion=false;
                     } // Fin if leeArchivo

                     if(pc51.getRenglonesLeidos()==0)
                     {
                        enlaceGlobal.mensajePorTrace("No se ley� ning�n rengl�n del archivo.", EIGlobal.NivelLog.INFO);
                     }
                     else
                     {
                        enlaceGlobal.mensajePorTrace("Se leyeron "+pc51.getRenglonesLeidos()+" renglones.", EIGlobal.NivelLog.INFO);
                     } // Fin if-else renglones
                   }
                   else
                   {
                     verificacion=false;
                     enlaceGlobal.mensajePorTrace("No se pudo hacer la copia remota.", EIGlobal.NivelLog.INFO);
                   } // Fin if-else copia remota

                     getServletContext().setAttribute("lineaCreditoCE", lineaCredito);
                     getServletContext().setAttribute("claveBancoCE", sesionBS.getClaveBanco());
                     getServletContext().setAttribute("fechaCE", ObtenFecha(true));
                     getServletContext().setAttribute("horaCE", ObtenHora().toUpperCase());


                   //if(pc51.interpretaTrama() && pc51.leeArchivo())
                   if(verificacion && pc51.getRenglonesLeidos()>0)
                   {
                     getServletContext().setAttribute("datosTasasCE", pc51.getCadenaDatos());
                     //peticion.setAttribute("Encabezado", CreaEncabezado("Consulta de tasas",titulo));
                     //evalTemplate("/jsp/tasasCE01.jsp", peticion, respuesta);
                     //<vswf:meg cambio de NASApp por Enlace 08122008>
                     respuesta.sendRedirect(respuesta.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+"/jsp/tasasCE01.jsp"));

                     /*
                     getServletContext().removeAttribute("datosTasasCE");
                     getServletContext().removeAttribute("lineaCreditoCE");
                     getServletContext().removeAttribute("claveBancoCE");
                     */
                   }
                   else
                   {
                        //despliegaPaginaError("No se obtuvo informaci&oacute;n de tasas.", peticion, respuesta);
                        /*
                        peticion.setAttribute("Encabezado", CreaEncabezado(titulo, menuNavegacion, peticion));
                        peticion.setAttribute("EstatusError", " ");
                        session.setAttribute("Error", "No se obtuvo informaci&oacute;n de tasas.");
                        session.setAttribute("Pagina", " ");
                        respuesta.sendRedirect(respuesta.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+"/jsp/EI_Error.jsp"));
                        */
                        //getServletContext().setAttribute("lineaCreditoCE", lineaCredito);
                        //getServletContext().setAttribute("claveBancoCE", sesionBS.getClaveBanco());
                	   	//<vswf:meg cambio de NASApp por Enlace 08122008>

                        respuesta.sendRedirect(respuesta.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+"/jsp/tasasCE01.jsp"));
                        enlaceGlobal.mensajePorTrace("No se obtuvo informaci�n de tasas.", EIGlobal.NivelLog.INFO);
                        //session.removeAttribute("Error");
                        //session.removeAttribute("Pagina");
                   } // Fin if verificacion
                }
                else
                {
                  enlaceGlobal.mensajePorTrace("No hay atributos o par�metros (tasas).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("No hay atributos o par&aacute;metros (tasas).", titulo, menuNavegacion, "s32110h", peticion, respuesta);
                } // Fin if verificaDatos
                break;
          case 2:
                String consultaDisposicion=null;
                String resultado=null;
                double auxImporte = 0;
                Hashtable respuestaDisposicion;
                //boolean entra=false;
                CotizacionCE cot=null;
                //confirma=new StringBuffer("");
                confirma=null;
                String mensajeError="";
                //System.gc();

                try
                {
                  incluirConcepto=Integer.parseInt(cadIncluirConcepto);
                }
                catch(Exception e)
                {
                  incluirConcepto=0;
                } // Fin try-catch

                if(incluirConcepto==0 || concepto==null)
                {
                  concepto="";
                } //Fin if

                String datosC[]={usuario, contrato, perfil, datosCuenta, importe, plazo};

                //if(numeroControl>=2)
                if(true)
                {
                  if(verificaDatos(datosC, enlaceGlobal))
                  {


                     Object[] resultadoDispone;
                     StringTokenizer separa=new StringTokenizer(datosCuenta, "|");
                     cuenta=separa.nextToken();
                     lineaCredito=separa.nextToken();
                     descripcion=separa.nextToken();
                     //salida.println("Cuenta: "+cuenta+" <BR>");
                     //salida.println("Linea: "+lineaCredito+" <BR>");

                     try
                     {
                        auxImporte=Double.parseDouble(importe);
                     }
                     catch(NumberFormatException nfe)
                     {
                        peticion.setAttribute("mensajeErrorDatos", "El importe dado no era num&eacute;rico: "+importe);
                        peticion.getRequestDispatcher("/ceVista?pantalla=0").forward(peticion, respuesta);
                        return;
                     } // Fin try-catch importe


                     TramasCE armaTrama=new TramasCE();
                     armaTrama.setMedioEntrega(TramasCE.TRAMA);
                     consultaDisposicion=armaTrama.consultaCotizacionDisposicion(usuario, contrato, perfil, cuenta, lineaCredito, importe, plazo, concepto);

					 //MSD Q05-0029909 inicio lineas agregadas
					 String IP = " ";
					 String fechaHr = "";												//variables locales al m�todo
					 IP = peticion.getRemoteAddr();											//ObtenerIP
					 java.util.Date fechaHrAct = new java.util.Date();
					 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
					 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignaci�n de fecha y hora
					 //MSD Q05-0029909 fin lineas agregada


					 EIGlobal.mensajePorTrace(fechaHr+"ceVista - Trama: "+consultaDisposicion, EIGlobal.NivelLog.DEBUG);


                     resultadoDispone=calculaDisposicion(consultaDisposicion, enlaceGlobal);
                     cot=(CotizacionCE)resultadoDispone[0];
                     mensajeError=(String)resultadoDispone[1];

                     /*
                     // Inicia simulaci�n de la cotizaci�n.
                     cot=new CotizacionCE();
                     cot.setDatosCuenta(datosCuenta);
                     //salida.println(datosCuenta+"<BR>");
                     enlaceGlobal.mensajePorTrace("Datos cuenta simulaci�n: "+datosCuenta, EIGlobal.NivelLog.INFO);




                     // Se simulan datos datos de respuesta
                     cot.setDatos("OK      202608 CEES0000|25-04-2001|1001|  10.0000|              0.27|          1,000.27 ");
                     // Fin de la simulaci�n de la cotizaci�n.
                     */



                     if(cot!=null)
                     {
                        enlaceGlobal.mensajePorTrace("CotizacionCE no es nulo.", EIGlobal.NivelLog.INFO);
                        cot.setCuenta(cuenta);
                        enlaceGlobal.mensajePorTrace("Cuenta: "+cot.getCuenta(), EIGlobal.NivelLog.INFO);
                        cot.setLineaCredito(lineaCredito);
                        enlaceGlobal.mensajePorTrace("L�nea de cr�dito: "+cot.getLineaCredito(), EIGlobal.NivelLog.INFO);
                        cot.setDescripcion(descripcion);
                        enlaceGlobal.mensajePorTrace("Descripci�n: " +cot.getDescripcion(), EIGlobal.NivelLog.INFO);
                        cot.setMonto(importe);
                        enlaceGlobal.mensajePorTrace("Importe: "+cot.getMonto(), EIGlobal.NivelLog.INFO);
                        cot.setConcepto(concepto);
                        enlaceGlobal.mensajePorTrace("Concepto: "+cot.getConcepto(), EIGlobal.NivelLog.INFO);
                        cot.setFechaAplicacion(ObtenFecha(true));
                        enlaceGlobal.mensajePorTrace("Fecha aplicaci�n: "+ ObtenFecha(true), EIGlobal.NivelLog.INFO);
                        peticion.setAttribute("cotizacion", cot);
                        //(getServletContext()).setAttribute("cotizacion", cot);
                        //numeroControl=3;
                        peticion.setAttribute("Encabezado", CreaEncabezado("Cotizaci&oacute;n de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32120h", peticion));
                        enlaceGlobal.mensajePorTrace("Cuenta: "+cuenta, EIGlobal.NivelLog.INFO);
                        peticion.setAttribute("cuenta", cuenta);
                        enlaceGlobal.mensajePorTrace("Descripci�n: "+descripcion, EIGlobal.NivelLog.INFO);
                        peticion.setAttribute("descripcion", descripcion);
                        enlaceGlobal.mensajePorTrace("L�nea cr�dito: "+lineaCredito, EIGlobal.NivelLog.INFO);
                        peticion.setAttribute("lineaCredito", lineaCredito);
                        enlaceGlobal.mensajePorTrace("Se subieron atributos; se va a llamar al jsp.", EIGlobal.NivelLog.INFO);
                        //evalTemplate("/jsp/cotizacionCE.jsp?cuenta="+cuenta+"&descripcion="+descripcion+"&lineaCredito="+lineaCredito, peticion, respuesta);

                        //TODO BIT CU3111 Ejecuta operaci�n PC47 realiza cotizaci�n

                        /**
    					 * VSWF - FVC - I
    					 * 17/Enero/2007
    					 */
                        if (Global.USAR_BITACORAS.equals("ON")){
	                        try {
	    					BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
	    					BitaTransacBean bt = new BitaTransacBean();
	    					bt = (BitaTransacBean)bh.llenarBean(bt);

	    					bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_DISPOS_OPER_RESDRV_PC47);
	    					bt.setContrato((contrato == null)?" ":contrato);
	    					bt.setImporte(auxImporte);
	    					bt.setServTransTux("PC47");
	    					bt.setNombreArchivo(usuario + ".doc");

	    					bt.setFechaAplicacion(new Date(System.currentTimeMillis()));


								BitaHandler.getInstance().insertBitaTransac(bt);
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
                        }
    					/**
    					 * VSWF - FVC - F
    					 */

                        evalTemplate("/jsp/cotizacionCE.jsp", peticion, respuesta);
                     }
                     else
                     {
                        enlaceGlobal.mensajePorTrace("No se pudo efectuar la cotizaci�n de la disposici�n (Disposici�n  Vista).\n"+mensajeError, EIGlobal.NivelLog.INFO);
                        despliegaPaginaError("No se pudo efectuar la cotizaci&oacute;n de la disposici&oacute;n.<BR>"+mensajeError, "Cotizaci&oacute;n de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32120h", peticion, respuesta);
                     } // Fin if-else cot
                  }
                  else
                  {
                     enlaceGlobal.mensajePorTrace("Los datos de sesi�n y/o par�metros son nulos (cotizaci�n).", EIGlobal.NivelLog.INFO);
                     despliegaPaginaError("Los datos de sesi&oacute;n y/o par&aacute;metros son nulos (cotizaci&oacute;n).", "Cotizaci&oacute;n de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico, menuNavegacion", "s32120h", peticion, respuesta);
                  } // Fin if verificaDatos
              }
              else
              {
                  //numeroControl=0;
                  enlaceGlobal.mensajePorTrace("Acceso indebido al proceso (cotizacion).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("Acceso indebido al proceso (cotizacion).", "Cotizaci&oacute;n de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32120h", peticion, respuesta);
              } // Fin if-else numeroControl
              break;
          case 3:
                CotizacionCE cce=new CotizacionCE();
                DisposicionCE dce=new DisposicionCE();
                InterpretaPD15 pd15=new InterpretaPD15();
                String consultaDispone, respDisp=null;
                mensajeConfirmacion=new StringBuffer("");
                confirma=null;
                FormatosMoneda formateaMoneda=new FormatosMoneda();

                if(concepto==null)
                  concepto="";

                String datosD[]={usuario, contrato, perfil, monto, plazo, tasa, importeTotal, interes, fechaVencimiento, cuenta, lineaCredito, descripcion};
                //System.gc();

                //if(numeroControl>=3)
                if(true)
                {
                  if(verificaDatos(datosD, enlaceGlobal))
                  {
                     cce.setConcepto(concepto);
                     cce.setMonto(monto);
                     cce.setPlazo(plazo);
                     cce.setTasa(tasa);
                     cce.setImporteTotal(importeTotal);
                     cce.setInteres(interes);
                     cce.setFechaVencimiento(fechaVencimiento);
                     cce.setCuenta(cuenta);
                     cce.setLineaCredito(lineaCredito);
                     cce.setDescripcion(descripcion);

                     /*
                     salida.println("Cuenta: "+cuenta+"\n<BR>");
                     salida.println("L&iacute;nea de cr&eacute;dito: "+lineaCredito+"\n<BR>");
                     salida.println("Descripci&oacute;n: "+descripcion+"\n<BR>");
                     */

                     dce.setUsuario(usuario);
                     dce.setConcepto(concepto);
                     dce.setCuenta(cuenta);
                     dce.setLineaCredito(lineaCredito);
                     dce.setDescripcion(descripcion);
                     dce.setPlazo(plazo);
                     dce.setContrato(contrato);
                     dce.setFechaOperacion(ObtenFecha(true));
                     dce.setHoraOperacion(ObtenHora());
                     //cce=(CotizacionCE)peticion.getAttribute("cot2");

                     //if(cce!=null)
                     //{
                     TramasCE armaDisposicion=new TramasCE();
                     //consultaDisposicion=armaDisposicion.consultaCotizacionDisposicion(usuario, contrato, perfil, cce.getCuenta(), cce.getLineaCredito(), cce.getMonto(), cce.getCadenaPlazo(), cce.getCadenaTasa(), " ");
                     //consultaDisposicion=armaDisposicion.consultaCotizacionDisposicion(usuario, contrato, perfil, cuenta, lineaCredito, monto, plazo, cce.getTasaSoloNumero(), " ");
                     consultaDisposicion=armaDisposicion.tramaConsultaDisposicion(usuario, contrato, perfil, lineaCredito, monto, plazo, tasa, " ");
                     //salida.println("Trama disposicion: "+consultaDisposicion+" <BR>");

                     respDisp=consultaDisposicion(consultaDisposicion, enlaceGlobal);

                     /*
                      *  Se simula una respuesta correcta del servicio de tuxedo
                      */
                      //respDisp="OK      202608 CEES0000|1545|16|10-05-2002";


                     if(respDisp!=null && respDisp.length()>1)
                     {


                       if(pd15.interpretaRespuesta(respDisp))
                       {

                         mensajeConfirmacion.append("<Table border=\"0\" align=\"center\">");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Contrato:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(dce.getContrato());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Usuario:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(dce.getUsuario());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Cuenta:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(cce.getCuenta());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>L&iacute;nea de cr&eacute;dito:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(cce.getLineaCredito());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Descripci&oacute;n:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(cce.getDescripcion());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Fecha del movimiento:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(dce.getFechaOperacion());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR>");
                         mensajeConfirmacion.append("<TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Hora del movimiento:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
                         mensajeConfirmacion.append(dce.getHoraOperacion());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Referencia:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(pd15.getNumeroReferencia());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Descripci&oacute;n del movimiento:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");

                         if(cce.getConcepto().length()>0)
                           mensajeConfirmacion.append(cce.getConcepto());
                         else
                           mensajeConfirmacion.append("<BR>");

                         mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Importe:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">$");
						 mensajeConfirmacion.append(formateaMoneda.daMonedaConComas(cce.getMonto(), false));
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>N&uacute;mero de disposici&oacute;n:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(pd15.getNumeroDisposicion());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Tasa:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(cce.getTasa());
						 mensajeConfirmacion.append("%</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Plazo:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(pd15.getPlazo());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Inter&eacute;s:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">$");
						 mensajeConfirmacion.append(cce.getInteres());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Total:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">$");
						 mensajeConfirmacion.append(cce.getImporteTotal());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("<TR><TD align=\"right\" class=\"textabref\">");
                         mensajeConfirmacion.append("<B>Fecha de vencimiento:</B></TD>");
                         mensajeConfirmacion.append("<TD class=\"textabref\">");
						 mensajeConfirmacion.append(pd15.getFechaVencimiento());
						 mensajeConfirmacion.append("</TD>");
                         mensajeConfirmacion.append("</TR>");

                         mensajeConfirmacion.append("</Table>");

                         //String abc="safsadf'ff'";

                         peticion.setAttribute("cot01", cce);
                         peticion.setAttribute("disp02", dce);
                         peticion.setAttribute("disposicion", pd15);
                         //HttpSession estaSesion=peticion.getSession();
                         //getServletContext();
                         getServletContext().setAttribute("mensajeConfirmacionCE", mensajeConfirmacion.toString());
                        /*
                         peticion.setAttribute("Encabezado", CreaEncabezado("Comprobante transferencia de cr&eacute;dito electr&oacute;nico", titulo));
                         enlaceGlobal.mensajePorTrace("Se han subido las variable de ambiente para el comprobante", EIGlobal.NivelLog.INFO);
                         evalTemplate("/jsp/comprobanteCE.jsp", peticion, respuesta);
                         */
                         //numeroControl=5;
                         peticion.setAttribute("Encabezado", CreaEncabezado("Comprobante de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32130h", peticion));
                         //evalTemplate("/jsp/comprobanteCE.jsp?mensajeConfirmacion="+mensajeConfirmacion, peticion, respuesta);


                         //TODO BIT CU3111 Ejecuta operaci�n PC47 Consulta disposici�n

                         /**
     					 * VSWF - FVC - I
     					 * 17/Enero/2007
     					 */
                         if (Global.USAR_BITACORAS.equals("ON")){
                        try {
     					BitaHelper bh = new BitaHelperImpl(peticion, sesionBS, sesion);
     					BitaTransacBean bt = new BitaTransacBean();
     					bt = (BitaTransacBean)bh.llenarBean(bt);

     					bt.setNumBit(BitaConstants.ER_CREDITO_ELECTRONICO_DISPOS_OPER_RESDRV_PC47);
     					bt.setContrato((contrato == null)?" ":contrato);
     					try
                        {
     						bt.setImporte(Double.parseDouble((importe == null)? "0":importe));
                        }
                        catch(NumberFormatException nfe)
                        {
                           peticion.setAttribute("mensajeErrorDatos", "El importe dado no era num&eacute;rico: "+importe);
                        }

     					bt.setNombreArchivo((usuario == null)?" ": usuario + ".doc");
     					bt.setServTransTux("PC47");
     					bt.setFechaAplicacion(new Date(System.currentTimeMillis()));


							BitaHandler.getInstance().insertBitaTransac(bt);
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
                         }
     					/**
     					 * VSWF - FVC - F
     					 */

                         evalTemplate("/jsp/comprobanteCE.jsp", peticion, respuesta);
                       }
                       else
                       {
                      //salida.println("La respuesta de la trama PD15 no es correcta.");

                         enlaceGlobal.mensajePorTrace("La respuesta de la trama PD15 no es correcta.", EIGlobal.NivelLog.DEBUG);
                         despliegaPaginaError("No se pudo efectuar la disposici&oacute;n.<BR>"+pd15.getMensajeError(), "Comprobante de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32130h", peticion, respuesta);
                       } // Fin if pd15


                     }
                     else
                     {
                       //salida.println("No se recibi� respuesta trama PD15 de tuxedo.");
                       enlaceGlobal.mensajePorTrace("No se recibi� respuesta trama PD15 de tuxedo.", EIGlobal.NivelLog.DEBUG);
                       despliegaPaginaError("No se pudo efectuar la disposici&oacute;n.", "Comprobante de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32130h", peticion, respuesta);
                     } // Fin if consulta
                   /*
                   }
                   else
                   {
                     salida.println("El atributo \"cotizacion\" es nulo.");
                     enlaceGlobal.mensajePorTrace("El atributo \"cotizacion\" es nulo.", EIGlobal.NivelLog.INFO);
                   } // Fin if
                   */
                }
                else
                {
                  enlaceGlobal.mensajePorTrace("No hay atributos o par�metros (disposici�n).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("No hay atributos o par&aacute;metros (disposici&oacute;n).", "Comprobante de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32130h", peticion, respuesta);
                } // Fin if-else verificaDatos
                }
                else
                {
                  //numeroControl=0;
                  enlaceGlobal.mensajePorTrace("Acceso indebido al proceso (disposicion).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("Acceso indebido al proceso (disposicion).", "Comprobante de disposici&oacute;n de Cr&eacute;dito Electr&oacute;nico", menuNavegacion, "s32130h", peticion, respuesta);
                } // Fin if-else numeroControl
                break;
          case 4:
               /*
               mensajeConfirmacion=(String)peticion.getAttribute("mensajeConfirmacion");
               salida.println("confirma: \n<BR>");
               salida.println(confirma);
               */
               String datosComprobante[]={contrato, usuario, perfil};

               //if(numeroControl==3 || numeroControl==5)
               if(true)
               {
                  if(verificaDatos(datosComprobante, enlaceGlobal))
                  {
                     confirma=(String)getServletContext().getAttribute("mensajeConfirmacionCE");

                     if(confirma!=null && confirma.length()>1)
                     {
                        //evalTemplate("/jsp/imprimeComprobanteCE.jsp", peticion, respuesta);
                        enlaceGlobal.mensajePorTrace("Comprobante disposici�n CE: "+confirma,EIGlobal.NivelLog.INFO);
                        getServletContext().setAttribute("claveBancoCE", sesionBS.getClaveBanco());
                        //<vswf:meg cambio de NASApp por Enlace 08122008>
                        respuesta.sendRedirect(respuesta.encodeRedirectURL("/Enlace/"+Global.WEB_APPLICATION+"/jsp/imprimeComprobanteCE.jsp"));

                        //evalTemplate("/jsp/imprimeComprobanteCE.jsp?claveBanco="+sesionBS.getClaveBanco(), peticion, respuesta);


                     }
                     else
                     {
                        despliegaPaginaError("No hay datos para desplegar el comprobante.", titulo, menuNavegacion, "s32110h", peticion, respuesta);
                        enlaceGlobal.mensajePorTrace("No hay datos para desplegar el comprobante.", EIGlobal.NivelLog.INFO);
                     } // Fin if-else
                  }
                  else
                  {
                     enlaceGlobal.mensajePorTrace("No hay atributos de sesi�n (imprime comprobante).", EIGlobal.NivelLog.INFO);
                     despliegaPaginaError("No hay atributos de sesi&oacute;n (imprime comprobante).", titulo, menuNavegacion, "s32110h", peticion, respuesta);
                  } // Fin if  verificaDatos
               }
               else
               {
                  //numeroControl=0;
                  enlaceGlobal.mensajePorTrace("Acceso indebido al proceso (Imprime comprobante).", EIGlobal.NivelLog.INFO);
                  despliegaPaginaError("Acceso indebido al proceso (Imprime comprobante).", titulo, menuNavegacion, "s32110h", peticion, respuesta);
               } // Fin if-else numeroControl

               break;
            /*
            case 6:
               //Este caso es de prueba
               respuesta.sendRedirect(respuesta.encodeRedirectURL("/NASApp/"+Global.WEB_APPLICATION+"/jsp/tasasCE01.jsp?datosTasas=9876543210&lineaCredito="+lineaCredito));
               break;
            */
        }// Fin switch

      } // Fin if sesionValida
    } // Fin m�todo defaultAction


    /**Este m&eacute;todo despliega el nombre todos los atributos contenidos en la enumeraci&oacute;n<BR>
    * @par&aacute;metro <I>atributos</I>: Contiene los atributos (de sesi&oacute;n o petici&oacute;n).
    *
    */
    public void despliegaAtributos(Enumeration atributos, PrintWriter imprime)
    {
      while(atributos.hasMoreElements())
          {
            imprime.println((String)atributos.nextElement()+"\n<BR>");
          } // Fin while
    } // Fin despliegaAtributos


   /**
   ** <code><B><I>consultaLineas</I></B><code> realiza la consulta de l&iacute;neas de cr&eacute;dito <B>PC46</B> a tuxedo de la cuenta dada.<BR>
   ** Si la cuenta contiene datos, los regresa en el arreglo.
   ** <P>Regresa un arreglo de cadenas de tres posiciones:<BR>
   ** El primero contiene los datos dentro de renglones y columnas de tablas HTML.<BR>
   ** El segundo contiene todos los datos de una l&iacute;nea separados por "pipe"; si hay m&aacute;s<BR>
   ** separados por <I>arrobas</I>.<BR>
   ** El tercero contiene el n�mero de l&iacute;neas (renglones) obtenidos convertido a cadena.
   ** </P>
   ** Si no obtuvo datos regresa nulo.
   ** @param tramaPC46 es la trama de consulta para este servicio.
   ** @param descripcion es la descripci&oacute;n de la cuenta.
   ** @param cuenta es el n&uacute;mero de cuenta de cheques.
   ** @param usuario es el c&oacute;digo de usuario con el que entra el usuario a la aplicaci&oacute;n
   ** @param enlaceGlobal permite enviar mensajes a la bit&aacute;cora.
   */
    public String[] consultaLineas(String tramaPC46, String descripcion, int cuenta, String usuario, EIGlobal enlaceGlobal)
    {
      Hashtable ht;
      String[] datosLineas=null;
      String respuesta=null;
      InterpretaPC46 ip46=null;
      String archivoRespuesta=null;
      String renglones=null;
      String recolecta=null;
      String[] regresa=new String[3];
      boolean esInterpretado=false;
      int leidos=0;
      StringTokenizer st;
      String archivo="";
      int partes=0;
      int posicion=-1;
      File archivoTuxedo;



     /*
      archivoTuxedo=new File(tmp+File.separator+usuario);


      if(archivoTuxedo.delete())
      {
         enlaceGlobal.mensajePorTrace("Se borr� el archivo.", EIGlobal.NivelLog.INFO);
      }
      else
      {
         enlaceGlobal.mensajePorTrace("No se borr� el archivo.", EIGlobal.NivelLog.INFO);
      } // Fin if-else archivo
      */


      try
      {
        ServicioTux tuxedo=new ServicioTux();
        //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        ht=tuxedo.web_red(tramaPC46);

        if(ht!=null)
        {
          respuesta=((String)ht.get("BUFFER")).trim();
			enlaceGlobal.mensajePorTrace("Respuesta consulta l�neas CE: "+respuesta, EIGlobal.NivelLog.DEBUG);

          if(respuesta!=null)
          {
            ip46=new InterpretaPC46();
            //ip46.setSeparador(" ");
            ip46.setTrama(respuesta);
            ip46.setDescripcion(descripcion.trim());
            ip46.setContadorInterno(cuenta);
            posicion=respuesta.indexOf("/");


            if(posicion>-1)
            {
             esInterpretado=true;
             archivo=respuesta.substring(posicion);
            } // Fin if posicion



            //Simulacion
            /*
            archivo="/tmp/pruebaLineas";
            esInterpretado=true;
            */

            if(esInterpretado)
            {
              archivoRespuesta=ip46.getNombreArchivo();
              enlaceGlobal.mensajePorTrace("Archivo l�neas CE: "+archivo, EIGlobal.NivelLog.INFO);

              ArchivoRemoto ar=new ArchivoRemoto();

              if(ar.copia(usuario))
              {
                  enlaceGlobal.mensajePorTrace("Se hizo la copia remota.", EIGlobal.NivelLog.INFO);
                  //if(ip46.leeArchivoRespuesta(archivoRespuesta))
                  if(ip46.leeArchivoRespuesta(archivo))
                  {
                     enlaceGlobal.mensajePorTrace("Se ley� el archivo de respuesta.", EIGlobal.NivelLog.INFO);
                     renglones=ip46.getRenglonesTabla();
                     recolecta=ip46.getTodosLosDatos()+"\n";
                     leidos=ip46.getRenglonesLeidos();

                  }
                  else
                  {
                     enlaceGlobal.mensajePorTrace("No se ley� el archivo de respuesta.", EIGlobal.NivelLog.INFO);
                     enlaceGlobal.mensajePorTrace("Se encontraron los siguientes problemas al tratar de leer el archivo: "+ip46.getErroresArchivo(), EIGlobal.NivelLog.INFO);
                  } // Fin if
               }
               else
               {
                  enlaceGlobal.mensajePorTrace("No se pudo hacer la copia remota.", EIGlobal.NivelLog.INFO);
               } // Fin if-else copiaArchivo

            }
            else
            {
              enlaceGlobal.mensajePorTrace("La trama de respuesta es de error: "+ip46.getErroresTrama(), EIGlobal.NivelLog.DEBUG);
            } // Fin if interpretaTrama
          }
          else
          {
            enlaceGlobal.mensajePorTrace("No hubo respuesta de tuxedo.", EIGlobal.NivelLog.DEBUG);
          } // Fin if respuesta

        } // Fin if ht



      }
      catch(RemoteException re)
      {
        enlaceGlobal.mensajePorTrace("Error al efectuar consulta de linea de credito: "+re.getMessage(), EIGlobal.NivelLog.INFO);
      }
      catch(Exception e)
      {
        enlaceGlobal.mensajePorTrace("Error al efectuar consulta de linea de credito: "+e.getMessage(), EIGlobal.NivelLog.INFO);
      } // Fin try-catch

      regresa[0]=renglones;
      regresa[1]=recolecta;
      regresa[2]=(new Integer(leidos)).toString();

      return regresa;
    } // Fin m�todo consultaLineas


   /**
   **<code><B><I>calculaDisposicion</I></B></code> efect&uacute;a la consulta de cotiaci&oacute;n por medio de <I>tuxedo</I>
   **<P>Regresa un objeto instanciado de tipo <I>CotizacionCE</I>, que interpreta la trama de respuesta.
   **</P>
   ** @param tramaPC47 contiene la trama de consulta de cotizaci&oacute;n de disposici&oacute;n <B>CETC</B>.
   ** @param enlaceGlobal envi&iacute;a mensajes a la bit&aacute;cora.
   */
    public Object[] calculaDisposicion(String tramaPC47, EIGlobal enlaceGlobal)
    {
      CotizacionCE cCE=new CotizacionCE();
      Hashtable ht;
      String respuesta;
      String mensajeError=new String("");
      Object[] arreglo=new Object[2];
      ServicioTux tuxedo;


      try
      {
        tuxedo=new ServicioTux();
        //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        ht=tuxedo.web_red(tramaPC47);
        respuesta=(String)ht.get("BUFFER");
        enlaceGlobal.mensajePorTrace("Respuesta cotizaci�n CE: "+respuesta, EIGlobal.NivelLog.DEBUG);
        //entra=true;

        if(!cCE.interpretaPC47(respuesta))
        {
            enlaceGlobal.mensajePorTrace("No pudo efectuar la disposici&oacute;n: "+respuesta, EIGlobal.NivelLog.INFO);
            mensajeError=cCE.getMensajeError();
            arreglo[0]=null;
            arreglo[1]=mensajeError;

            return arreglo;
        } // Fin if

        cCE.setDatos(respuesta);

      }
      catch(RemoteException re)
      {
        enlaceGlobal.mensajePorTrace("Error al solicitar servicio Tuxedo: "+re.getMessage(), EIGlobal.NivelLog.INFO);
      }
      catch(Exception e)
      {
        enlaceGlobal.mensajePorTrace("Error al solicitar servicio Tuxedo: "+e.getMessage(), EIGlobal.NivelLog.INFO);
      } // Fin try-catch



      //return cCE;
      arreglo[0]=cCE;
      arreglo[1]="";
      return arreglo;
    } // Fin calculaDisposcion


   /* Este m�todo se coment� porque el metodo "consultaTasas" no regresa nada.
    public String consultaTasas()
    {
      Hashtable ht;
      String respuestaTasas=null;

      try
      {
        ht=tuxedo.consultaTasas("abc", "1EWEB");
        respuestaTasas=(String)ht.get("BUFFER");
      }
      catch(RemoteException re)
      {
        enlaceGlobal.mensajePorTrace("Error al solicitar el servicio: "+re.getMessage(), EIGlobal.NivelLog.INFO);
      }
      catch(Exception e)
      {
        enlaceGlobal.mensajePorTrace("Error al solicitar el servicio: "+e.getMessage(), EIGlobal.NivelLog.INFO);
      } // Fin try-catcb





      return respuestaTasas;
    } // Fin consultaTasas
    */

   /**
   ** <code><B><I>daRenglonSinLinea</I></B></code> crea un rengl&oacute;n HTML para la cuenta sin l&iacute;neas de cr&eacute;dito,<BR>
   ** desplegando s&oacute;lo el n&uacute;mero de cuenta y su descripci&oacute;n.
   ** <P>Siempre regresa una cadena con un fondo claro u obscuro, dependiendo<BR>
   ** de su posici&oacute;n en la pantalla.
   ** </P>
   ** @param j indica la posici&oacute;n en la pantalla.
   ** @param cuenta es el n&uacute;mero de cuenta de la chequera.
   ** @param descripcion describe la cuenta de cheques.
   */
    public String daRenglonSinLinea(int j, String cuenta, String descripcion)
    {
      StringBuffer nuevoRenglon=new StringBuffer("");

      if(descripcion==null || descripcion.trim().length()<1)
      {
         descripcion="<BR>";
      } // Fin if descripcion

      if(j%2!=0)
      {
        nuevoRenglon.append("<TR>");
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append("<BR></TD>");
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append("<BR></TD>");
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append(cuenta);
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append("<BR><TD>");
		nuevoRenglon.append("<TD>");
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append(descripcion);
		nuevoRenglon.append("</TD>");
		nuevoRenglon.append(InterpretaPC46.fondoObscuro);
		nuevoRenglon.append("<BR></TR>");
      }
      else
      {
        nuevoRenglon.append("<TR>");
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append("<BR></TD>");
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append("<BR></TD>");
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append(cuenta);
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append("<BR><TD>");
		nuevoRenglon.append("<TD>");
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append(descripcion);
		nuevoRenglon.append("</TD>");
		nuevoRenglon.append(InterpretaPC46.fondoClaro);
		nuevoRenglon.append("<BR></TR>");
      } // Fin if modulo i

      return nuevoRenglon.toString();
    } // Fin daRenglonSinLinea


    /**
    ** <code><B><I>consultaLineas</I></B></code> crea un archivo de datos para exportar los datos desplegados en la<BR>
    ** primera pantalla. El archivo es creado como un archivo binario, pero los datos y retornos<BR>
    ** de carro se encuentran en <I>datosArchivo</I>, para que el archivo generado sea <I>bajado</I><BR>
    ** como archivo de texto.
    ** <P>
    ** Regresa verdadero si el archivo fue generado sin problemas; de lo contrario regresa falso.
    ** </P>
    ** @param datosArchivo Es la cadena con todos los datos para guardar en el archivo.<BR>
    ** @param nombreArchivo El nombre (sin ruta) del archivo incluyendo extensi&oacute;n.
    ** @param enlaceGlobal envi&iacute;a mensajes a la bit&aacute;cora.
    */
    public boolean creaArchivo(String datosArchivo, String nombreArchivo, EIGlobal enlaceGlobal)
    {
      BufferedOutputStream bos=null;
      boolean resultadoArchivo=false;



      if(nombreArchivo==null || nombreArchivo.length()<1)
        return false;

      try
      {
        bos=new BufferedOutputStream(new FileOutputStream(tmp+nombreArchivo));
        bos.write((datosArchivo.replace('|', ' ')).getBytes());
        bos.flush();
        resultadoArchivo=true;
      }
      catch(IOException iox01)
      {
        enlaceGlobal.mensajePorTrace("No se pudo crear el archivo: "+iox01.getMessage(), EIGlobal.NivelLog.INFO);
        //return false;
        resultadoArchivo=false;
      }
      finally
      {

         if(bos!=null)
         {
            try
            {
               bos.close();
            }
            catch(IOException iox02)
            {
               enlaceGlobal.mensajePorTrace("No se pudo cerrar el archivo: "+iox02.getMessage(), EIGlobal.NivelLog.INFO);
            } // Fin try-catch cerrar
         }// Fin if bos
      } // Fin try-catch-finally



      return resultadoArchivo;
    } // Fin creaArchivo

   /**
   ** <code><B><I>consultaDisposicion</I></B></code> efect&uacute;a la consulta al servicio de <I>tuxedo</I><BR>
   ** <P>Regresa la trama de respuesta como una cadena.
   ** @param enviaConsulta contiene la trama de consulta para cualquier servicio de <I>tuxedo</I>.
   ** @param enlaceGlobal envi&iacute;a mensajes a la bit&aacute;cora.
   ** </P>
   */
    public String consultaDisposicion(String enviaConsulta, EIGlobal enlaceGlobal)
    {
      Hashtable ht;
      String respuesta=null;
      ServicioTux tuxedo;

      try
      {
        tuxedo=new ServicioTux();
        //tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
        ht=tuxedo.web_red(enviaConsulta);
        respuesta=(String)ht.get("BUFFER");
        enlaceGlobal.mensajePorTrace("Respuesta disposici�n CE: "+respuesta, EIGlobal.NivelLog.DEBUG);
      }
      catch(RemoteException re)
      {
        respuesta="";
        enlaceGlobal.mensajePorTrace("Error en consulta de disposicion: "+re.getMessage(), EIGlobal.NivelLog.INFO);
      }
      catch(Exception e)
      {
        respuesta="";
        enlaceGlobal.mensajePorTrace("Error en consulta de disposicion: "+e.getMessage(), EIGlobal.NivelLog.INFO);
      } // Fin try-catch


      return respuesta;
    } // Fin consultaDisposicion

   /**
   ** <code><B><I>verificaDatos</I></B></code> revisa que los datos proporcionados (tales como<BR>
   ** atributos y par&aacute;metros) no sean nulos y contengan datos.
   ** <P>Regresa verdadero si todos los datos cumplen con lo anterior; si un dato no lo cumple<BR>
   ** interrumpe la revisi&oacute;n y regresa falso.</P>
   ** @param datos Es un arreglo de cadenas con todos los datos a verificar.
   ** @param enlaceGlobal envi&iacute;a mensajes a la bit&aacute;cora.
   */
    public boolean verificaDatos(String datos[], EIGlobal enlaceGlobal)
    {
      int tamanoArreglo=-1;

      if(datos==null)
         return false;

      tamanoArreglo=datos.length;

      for(int i=0;i<tamanoArreglo;i++)
      {
         if(datos[i]==null || datos[i].length()<1)
         {
            enlaceGlobal.mensajePorTrace("Dato nulo: "+i, EIGlobal.NivelLog.INFO);
            //pw.println("Dato nulo: "+i+" <BR>");
            return false;
         } // Fin if
      } // Fin for

      return true;
    } // Fin verificaDatos

} // Fin clase ceVista