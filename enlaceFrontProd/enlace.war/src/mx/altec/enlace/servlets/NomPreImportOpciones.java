
package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.text.*;

import mx.altec.enlace.beans.NominaArchBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.ArchivosNomina;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.LYMValidador;
import mx.altec.enlace.bo.NominaArchBO;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.NomPreUtil;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.bo.FormatoMoneda;

public class NomPreImportOpciones extends BaseServlet {

	private static final long serialVersionUID = 1L;

	final String HRPREMIER = "HorarioPremier";


	private String textoLog(String funcion){

		return  NomPreImportOpciones.class.getSimpleName() + "." + funcion + "::";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		defaultAction(request, response);
	}

	public void defaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{

		String textoLog = "";


		textoLog = textoLog("defaultAction");

		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean solicitarOTP = true;
		if (sesionvalida)
		{

			if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null)
			{
				EIGlobal.mensajePorTrace(textoLog + "Se va a efectuar la transaccion", EIGlobal.NivelLog.DEBUG);
				solicitarOTP = true;
			}

			String valida = request.getParameter("valida");
			if (validaPeticion(request, response, session, sess, valida))
			{
				EIGlobal.mensajePorTrace(textoLog + "Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
			}
			else
			{
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
				EIGlobal.mensajePorTrace(textoLog, EIGlobal.NivelLog.DEBUG);
				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser", ObtenContUser(request));

				String arch_exportar = "";
				String nombre_arch_salida = "";

				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());

				StringBuffer datesrvr = new StringBuffer("");
				datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
				datesrvr.append(ObtenAnio());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
				datesrvr.append(ObtenMes());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
				datesrvr.append(ObtenDia());
				datesrvr.append("\">");

				request.setAttribute("Fechas", ObtenFecha(true));
				request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");
				request.setAttribute("botLimpiar", "");

				String strOperacion = (String) getFormParameter(request, "operacion");
				EIGlobal.mensajePorTrace("El c&oacute;digo de operaci&oacute;n es:" + strOperacion, NivelLog.DEBUG);
				if (strOperacion == null || strOperacion.equals(""))
					strOperacion = "";
	            String strRegistro = (String) getFormParameter(request, "registro");
				if (strRegistro == null || strRegistro.equals(""))
					strRegistro = "";
				request.setAttribute("operacion", strOperacion);

				String tipoArchivotmp = getFormParameter(request, "tipoArchivo");
				EIGlobal.mensajePorTrace(textoLog + "tipoArchivo [" + tipoArchivotmp + "]", EIGlobal.NivelLog.DEBUG);
				if (tipoArchivotmp == null || tipoArchivotmp.equals(""))
					tipoArchivotmp = "";

				if (strOperacion.equals("regresarPagos"))
				{// regresara a la pagina inicial
					inicioPagos(request, response);
				}
				else
					if (strOperacion.equals("descarga"))
					{// Para descargar el archivo. Se envia desde el application
						descargaArchivos(request, response);
					}
					else
						if (strOperacion.equals("regresar"))
						{// regresara a la pagina inicial
							inicioConsulta(request, response);
						}
						else
							if (strOperacion.equals("regresarPagosInd"))
							{// regresara a la pagina inicial
								inicioPagosInd(request, response);
							}
							else
								if (strOperacion.equals("individual"))
								{
									pagoIndividual(request, response);
								}
								else
									if (strOperacion.equals("envio"))
									{

									EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Verificando datos en sesion", EIGlobal.NivelLog.DEBUG);
									String Concepto = "";
									String archivoConcepto = "";
									if (sess.getAttribute("conceptoEnArchivo") != null)
									{
										EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Si existe en sesion! Se lee el parametro concepto", EIGlobal.NivelLog.DEBUG);
										Concepto = (String) getFormParameter(request, "Concepto");
										archivoConcepto = (String) sess.getAttribute("nombreArchivo");
										EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Concepto: " + Concepto, EIGlobal.NivelLog.DEBUG);
										EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: archivoConcepto: " + archivoConcepto, EIGlobal.NivelLog.DEBUG);
										if (actualizaConceptoEnArchivo(archivoConcepto, Concepto))
										{
											EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Se actualizo el archivo", EIGlobal.NivelLog.DEBUG);
											sess.removeAttribute("conceptoEnArchivo");
										}
										else
										{
											despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de N&oacute;mina","Servicios &gt; Env&iacute;o de Archivos de N&oacute;mina",request, response);
											return;
										}
									}
									else
										EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Los registros en archivo contienen el concepto", EIGlobal.NivelLog.DEBUG);
									String validaChallenge = request.getAttribute("challngeExito") != null 
										? request.getAttribute("challngeExito").toString() : "";
									EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);
									if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
										EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
										guardaParametrosEnSession(request);
										validacionesRSA(request, response);
										return;
									} 
									EIGlobal.mensajePorTrace(textoLog + "Antes de Verificacion de Token, solicitarOTP = " + solicitarOTP, EIGlobal.NivelLog.DEBUG);

									boolean valBitacora = true;
									if (valida == null)
									{
										EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
										boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA_PREPAGO);

										if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal && solicitarOTP)
										{
											EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
											guardaParametrosEnSession(request);
											request.getSession().removeAttribute("mensajeSession");
											sess.setAttribute("importePrePagoS", Double.parseDouble(agregarPunto(session.getImpTotalNom().trim())));
											sess.setAttribute("registrosPrePagoS", session.getNominaNumberOfRecords().trim());
											ValidaOTP.mensajeOTP(request, "PrePagoNomina");
											ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
										}
										else
										{
											valida = "1";
											valBitacora = false;
										}
									}
									if (valida != null && valida.equals("1"))
									{
										boolean banderaTransaccion = true;

										if (request.getSession().getAttribute( "Ejec_Proc_Val_Pago") != null)
										{
											EIGlobal.mensajePorTrace( "-----------getRequestURL--a->"+ request.getRequestURL().toString(), EIGlobal.NivelLog.INFO);
											if (valBitacora && request.getRequestURL().toString().contains("transferencia"))
											{
												try{
														int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
														HttpSession sessionBit = request.getSession(false);
														String claveOperacion = BitaConstants.CTK_PAGO_NOMINA_PREP;
														String concepto = BitaConstants.CTK_CONCEPTO_PAGO_NOMINA_PREP;
														double importeDouble = 0;
														String cuenta ="0";
														String token = "0";
														if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
														{token = request.getParameter("token");}
														if (session.getCuentaCargo() != null && !session.getCuentaCargo().equals(""))
														{cuenta = session.getCuentaCargo();}
														if (sessionBit.getAttribute("importePrePagoS") != null && !sessionBit.getAttribute("importePrePagoS").equals(""))
														{String importe = sessionBit.getAttribute("importePrePagoS").toString();
														importeDouble = Double.valueOf(importe).doubleValue();}
														EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);
														String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
												} catch(Exception e) {
													EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);
												}

												sess.removeAttribute("importePrePagoS");
												sess.removeAttribute("registrosPrePagoS");
											}
											EIGlobal.mensajePorTrace(textoLog + "Es recarga de procedimiento por VALIDACION.", EIGlobal.NivelLog.DEBUG);
											banderaTransaccion = true;
										}
										else
											if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null && verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false))
											{
												EIGlobal.mensajePorTrace(textoLog + "El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
												banderaTransaccion = false;
											}
											else
												if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null)
												{
													EIGlobal.mensajePorTrace(textoLog + "Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
													request.getSession().removeAttribute("Recarga_Ses_Multiples");
													banderaTransaccion = false;
												}
												else
													if (verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false))
													{
														EIGlobal.mensajePorTrace(textoLog + "El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
														banderaTransaccion = false;
													}
													else
														if (request.getSession().getAttribute("Recarga_Ses_Multiples") == null && !verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses", false))
														{
															EIGlobal.mensajePorTrace(textoLog + "Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
															banderaTransaccion = true;
														}

										if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null)
										{
											request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
											EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago REMOVIDO.", EIGlobal.NivelLog.DEBUG);
										}

										EIGlobal.mensajePorTrace(textoLog + "banderaTransaccion:" + banderaTransaccion, EIGlobal.NivelLog.DEBUG);

										if (banderaTransaccion)
										{
											EI_Exportar arcTmp = new EI_Exportar(Global.DOWNLOAD_PATH + request.getSession().getId().toString() + ".ses");
											if (arcTmp.creaArchivo())
												EIGlobal.mensajePorTrace(textoLog + "Archivo .ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);

											request.getSession().setAttribute("Recarga_Ses_Multiples", "TRUE");
											EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago Creado.", EIGlobal.NivelLog.DEBUG);

											String nombreArchivo = (String) sess.getAttribute("nombreArchivo");
											String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
											String digit = getFormParameter(request, "statusDuplicado");
											String digit_d = getFormParameter(request, "statushrc");
											EIGlobal.mensajePorTrace(textoLog + "nueva_ruta ["+ nueva_ruta + "]", EIGlobal.NivelLog.DEBUG);
											nombreArchivo = nombreArchivo + "/";
											nombreArchivo = nombreArchivo.substring(posCar(nombreArchivo, '/', 4) + 1, posCar(nombreArchivo, '/', 5));

											String rutaArch = nueva_ruta + "/" + nombreArchivo;
											EIGlobal.mensajePorTrace(textoLog + "nombreArchivo dentro de envio" + nombreArchivo, EIGlobal.NivelLog.DEBUG);
											try
											{
												String tipoCopia = "3";
												ArchivoRemoto archR = new ArchivoRemoto();
												if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
													EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
											} catch (Exception ioeSua) {
												EIGlobal.mensajePorTrace(textoLog + "Excepcion %envio() >> " + ioeSua.getMessage() + " <<", EIGlobal.NivelLog.ERROR);
											}

											String fechaAplicacion = (String) getFormParameter(request, "encFechApli");//encFechApli
											request.setAttribute("encFechApli", fechaAplicacion);
											EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion PARAMETER" + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);

											if (fechaAplicacion != null)
												sess.setAttribute("NOMIencFechApli", fechaAplicacion);
											if (fechaAplicacion != null)
											{
												EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio LENGTH" + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);
												fechaAplicacion = fechaAplicacion.trim();
												fechaAplicacion = fechaAplicacion.substring(2, 4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
											}
											else
											{
												EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
												fechaAplicacion = (String) sess.getAttribute("NOMIencFechApli");
												fechaAplicacion = fechaAplicacion.trim();
												fechaAplicacion = fechaAplicacion.substring(2,4) + fechaAplicacion.substring(0, 2) + fechaAplicacion.substring(4, 8);
											}

											//java.util.Date dt = new java.util.Date();
											String HorarioDisp = "";
											String HorarioSele = "";

											EIGlobal.mensajePorTrace(textoLog + "Validacion de fecha", EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog + "Fecha del dia de hoy (true) " + ObtenFecha(true), EIGlobal.NivelLog.DEBUG);
											String fecha_hoy = ObtenFecha(true);
											String dd_hoy = fecha_hoy.substring(0, 2);
											String mm_hoy = fecha_hoy.substring(3, 5);
											String aa_hoy = fecha_hoy.substring(6, 10);
											EIGlobal.mensajePorTrace(textoLog + "Parametros " + "dd_hoy=" + dd_hoy + ", mm_hoy=" + mm_hoy + ", aa_hoy=" + aa_hoy, EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog + "fecha de hoy (false)" + ObtenFecha(false), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog + "Fecha Aplicacion fechaAplicacion.substring(0,2)=" + fechaAplicacion.substring(0, 2) + ", fechaAplicacion.substring(2,4)=" + fechaAplicacion.substring(2, 4) + ", fechaAplicacion.substring(4,8)=" + fechaAplicacion.substring(4, 8), EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog +  "digit >>" + digit + "<<", EIGlobal.NivelLog.ERROR);

											if ((dd_hoy.equals(fechaAplicacion.substring(0, 2))
													&& mm_hoy.equals(fechaAplicacion.substring(2, 4))
													&& aa_hoy.equals(fechaAplicacion.substring(4, 8)))
													&& ((Calendar.HOUR_OF_DAY == 16
															&& Calendar.MINUTE >= 30)
															|| Calendar.HOUR_OF_DAY >= 17))
											{
												EIGlobal.mensajePorTrace(textoLog + "Horario Premier >>" + HorarioSele + "<<", EIGlobal.NivelLog.ERROR);
												request.setAttribute("folio", (String) getFormParameter(request, "folio"));
												request.setAttribute("newMenu", session.getFuncionesDeMenu());
												request.setAttribute("MenuPrincipal", session.getStrMenu());
												request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

												String infoUser = "HORA LIMITE PARA APLICAR PAGOS HOY ES 16:30 HRS.";
												String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";

												infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
												request.setAttribute("infoUser", infoUser);

												if (sess.getAttribute("facultadesN") != null)
													request.setAttribute("facultades", sess.getAttribute("facultadesN"));
												request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
												sess.setAttribute("archDuplicado", "si");
												request.setAttribute("digit", "0");
												evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
												return;
											}

											EIGlobal.mensajePorTrace(textoLog +  "digit 2 >> " + digit + "<<", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion FINAL" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
											String cuenta="";
											String contrato = session.getContractNumber();
											String empleado = session.getUserID8();
											String perfil = session.getUserProfile();
											cuenta = session.getCuentaCargo();

											cuenta = (cuenta!=null ? cuenta.trim() : cuenta);

											EIGlobal.mensajePorTrace("Validando Cuenta null", EIGlobal.NivelLog.INFO);

											if(cuenta == null || "null".equals(cuenta) || "".equals(cuenta)){
												request.setAttribute("newMenu", session.getFuncionesDeMenu());
												request.setAttribute("MenuPrincipal", session.getStrMenu());
												request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta Inmediata Santander ","Servicios &gt; Tarjeta Inmediata Santander  &gt; Pagos","s25800h",request));
												String infoUser = "Error al enviar el archivo, intente mas tarde.";
												String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
												infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
												request.setAttribute("infoUser", infoUser);
												request.setAttribute("ContenidoArchivo", "");
							                    request.getSession().removeAttribute("folio");
							                    sess.setAttribute("FOLIO", "");
							                    System.out.println("LIMPIANDO FOLIO DESDE LA CLASE NOMPREDISPERSION");
							                    request.getSession().removeAttribute("folio");
							                    sess.setAttribute("FOLIO", "");
												evalTemplate("/jsp/prepago/NomPredispersion.jsp", request, response);
												return;
											}

											String importe = session.getImpTotalNom().trim();
											String importePunto = agregarPunto(importe);
											String totalRegNom = session.getTotalRegsNom().trim();
											String folio = "";
											//ServicioTux tuxGlobal = new ServicioTux();

											String tramaEntrada = "";
											String horarioPremier = "";
											String FacultadPremier = "";
											FacultadPremier = (String) sess.getAttribute("facultadesN");
											EIGlobal.mensajePorTrace(textoLog + "Facultad Premier CCBFP " + FacultadPremier, EIGlobal.NivelLog.INFO);
											if (FacultadPremier.indexOf("PremierVerdadero") != -1)
											{
												horarioPremier = getFormParameter(request, "horario_seleccionado");
												EIGlobal.mensajePorTrace(textoLog + "CCBHorarioPremierMod " + horarioPremier, EIGlobal.NivelLog.INFO);
												EIGlobal.mensajePorTrace(textoLog + "horarioPremier  Modificado >" + horarioPremier.trim() + "<", EIGlobal.NivelLog.DEBUG);

												request.setAttribute("horario_seleccionado", getFormParameter(request, "horario_seleccionado"));
												request.setAttribute("HorarioPremier", getFormParameter(request, "horario_seleccionado"));
												if (horarioPremier == null)
													horarioPremier = "17:00";
											}
											else
											{
												horarioPremier = "17:00";
											}

											sess.setAttribute("HR_P", getFormParameter(request, "horario_seleccionado"));
											EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(HR_P) >>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "digit 3 >>" + digit_d + "<<", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(archDuplicado)>>" + sess.getAttribute("archDuplicado") + "<<", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.ERROR);

											if (digit.compareTo("0") == 0)
											{
												horarioPremier = (String) sess.getAttribute("HR_P");
												EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
												digit = "0";
											}

											String servicio = "NPVP";
											String validar = (String) sess.getAttribute("VALIDAR");
											folio = (String) sess.getAttribute("FOLIO");

											if (folio == null || folio.equals(""))
											{
												sess.setAttribute("FOLIO", getFormParameter(request, "folio"));
												folio = (String) sess.getAttribute("FOLIO");
												EIGlobal.mensajePorTrace(textoLog + "El folio esta vacio", EIGlobal.NivelLog.DEBUG);

											}

											EIGlobal.mensajePorTrace(textoLog + "Folio [" + folio + "]", EIGlobal.NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog + "Validar [" + validar + "]", EIGlobal.NivelLog.DEBUG);

											String HorarioNormal = (String) sess .getAttribute("HorarioNormal");

											if (HorarioNormal == null)
												HorarioNormal = "";
											if (HorarioNormal.equals("1"))
											{
												if (digit_d.compareTo("0") == 0)
												{
													horarioPremier = "17:00";
													EIGlobal.mensajePorTrace(textoLog + "horarioPremier 17:00 >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
													digit = "0";
												}
											}

											if (validar == null)
												validar = "";
											if (validar.compareTo("N") == 0)
											{
												servicio = "PNOS";
											}

											tramaEntrada = "1EWEB|" + empleado + "|" + servicio
											+ "|" + contrato + "|" + empleado + "|" + perfil
											+ "|" + contrato + "@" + fechaAplicacion + "@"
											+ totalRegNom + "@" + importePunto + "@"
											+ cuenta + "@" + rutaArch + "@" + digit + "@"
											+ horarioPremier + "@|";

											if (folio.equals(""))
												tramaEntrada += " | | |";
											else
												tramaEntrada += cuenta + "|" + importePunto
												+ "|" + folio + "|";

											EIGlobal.mensajePorTrace(textoLog + "Trama que se enviara >>" + tramaEntrada + "<<", EIGlobal.NivelLog.ERROR);
											String tramaSalida = "";
											String folioArchivo = "";

											if (servicio.equals("PNOS"))
											{

												LYMValidador val = new LYMValidador();
							 					String resLYM = val.validaLyM(tramaEntrada);
							 					val.cerrar();///CSA
												if(!resLYM.equals("ALYM0000")) {

													request.setAttribute("folio", (String) getFormParameter(request, "folio"));
													request.setAttribute("newMenu", session.getFuncionesDeMenu());
													request.setAttribute("MenuPrincipal", session.getStrMenu());
													request.setAttribute("Encabezado", CreaEncabezado("Tarjeta de Pagos Santander ", "Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos", "s25800IIIh", request));//YHG NPRE
													String infoUser = resLYM;
													String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
													infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
													request.setAttribute("infoUser", infoUser);
													if (sess.getAttribute("facultadesN") != null)
														request.setAttribute("facultades", sess.getAttribute("facultadesN"));
													request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
													sess.setAttribute("archDuplicado", "si");
													request.setAttribute("digit", "0");
													evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
													return;

												}


												folioArchivo = generaFolio(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), servicio, totalRegNom);
												session.setFolioArchivo(folioArchivo);
												request.setAttribute("folioArchivo", folioArchivo);
												EIGlobal.mensajePorTrace(textoLog + "folioArchivo >>" + folioArchivo + "<<", EIGlobal.NivelLog.ERROR);
												// Envio de Datos ------------------------------
												String mensajeErrorEnviar = envioDatos(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), folioArchivo, servicio, rutaArch, tramaEntrada);
												EIGlobal.mensajePorTrace(textoLog + "mensajeErrorEnviar>>" + mensajeErrorEnviar + "<<", EIGlobal.NivelLog.ERROR);
												// Servicio dispersor de transacciones ---------
												if (mensajeErrorEnviar.equals("OK"))
												{
													tramaSalida = realizaDispersion(session.getUserID8(), session.getUserProfile(), session .getContractNumber(), folioArchivo, servicio);
												}
											}
											else
											{
												tramaSalida = ejecutaServicio(tramaEntrada);
											}
											EIGlobal.mensajePorTrace("Trama de Entrada :" + tramaEntrada, NivelLog.DEBUG);
											EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalida, NivelLog.DEBUG);
											EIGlobal.mensajePorTrace(textoLog + "Trama salida>>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
											String estatus_envio = "";
											String fechaAplicacionFormato = "";
											fechaAplicacionFormato = fechaAplicacion.substring( 0, 2) + "/" + fechaAplicacion.substring(2, 4) + "/" + fechaAplicacion.substring(4, 8);


											if (!tramaSalida.startsWith("MANC"))
											{
												try
												{
													estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
												} catch (Exception e) {
													estatus_envio = "";
												}
											}

											SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
											BitaTransacBean bt = new BitaTransacBean();
											double impBit=0.0;
											String codErrBit=" ";
											String ctaOriBit=" ";
											bt.setNumBit(BitaConstants.ES_NP_PAGOS_IMPORTACION_CONFIRMA);
											if(folioArchivo!=null && !folioArchivo.trim().equals(""))
												bt.setReferencia(Long.parseLong(folioArchivo));
											if(fechaAplicacionFormato!=null && !fechaAplicacionFormato.trim().equals(""))
												try {
													bt.setFechaAplicacion(sdf.parse(fechaAplicacionFormato));
												} catch (ParseException e1) {}
											if(estatus_envio!=null && !estatus_envio.trim().equals("")){
												codErrBit=estatus_envio;
												bt.setEstatus(estatus_envio.substring(0, 1));
											}
											if(importePunto!=null &&!importePunto.trim().equals("")){
												impBit=Double.parseDouble(importePunto);
												bt.setImporte(impBit);
											}
											if(cuenta!=null && !cuenta.trim().equals("")){
												bt.setCctaOrig(cuenta);
												ctaOriBit=cuenta;
											}


											NomPreUtil.bitacoriza(request,servicio, estatus_envio, bt);

											/*try
											{
												EIGlobal.mensajePorTrace("INICIA BITACORIZACION TCT Nomina Prepago DISPERSION POR ARCHIVO (Primera)",EIGlobal.NivelLog.INFO);
												int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
												String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,NomPreUtil.ES_NP_PAGOS_IMPORTACION_OPER," ",0," ");
											}catch(Exception e){
												EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
											}*/

											if (estatus_envio.equals("NOMI0000"))
											{

												if (servicio.equals("NPVP"))
												{
													sess.setAttribute("VALIDAR", "N"); // Setear la variable global para que no se continue con la validacion.
													request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
													EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO.", EIGlobal.NivelLog.DEBUG);
													defaultAction(request, response); // Ejecutar nuevamente la operacion de envio para procesar el archivo.
													return;
												}

												String transmision = tramaSalida.substring(posCar(tramaSalida, '@', 2) + 1, posCar(tramaSalida, '@', 3));
												String nombreArchivo_aceptado = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
												String fechaCargo = null;
												int start = posCar(tramaSalida, '@', 3) + 1;
												int end = tramaSalida.indexOf('@', start);

												if (start < end)
												{
													fechaCargo = tramaSalida.substring(start, end);
													fechaCargo = fechaCargo.substring(0, 2) + "/" + fechaCargo.substring(2, 4) + "/" + fechaCargo.substring(4, 8);
												}

												String bgcolor = "textabdatobs";
												StringBuffer tablaTotales = new StringBuffer("");
												tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
												tablaTotales.append("<tr> ");
												tablaTotales.append("<td class=\"textabref\">Pago de Tarjeta de Pagos Santander </td>");//YHG NPRE
												tablaTotales.append("</tr>");
												tablaTotales.append("</table>");
												tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
												tablaTotales.append("<tr> ");
												tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
												tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descripci&oacute;n</td>");

												if (null != fechaCargo)
												{
													tablaTotales.append("<td class='tittabdat' width='90' align='center'>Fecha de Cargo</td>");
												}

												tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
												tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
												tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
												tablaTotales.append("</tr>");
												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">");
												tablaTotales.append(session.getCuentaCargo());
												tablaTotales.append("&nbsp;</td>");
												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">");
												tablaTotales.append(sess.getAttribute("descpCuentaCargo"));
												tablaTotales.append("&nbsp;</td>");

												if (null != fechaCargo)
												{
													tablaTotales.append("<td class='");
													tablaTotales.append(bgcolor);
													tablaTotales.append("' align='center'>");
													tablaTotales.append(fechaCargo);
													tablaTotales.append("</td>");
												}

												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">");
												tablaTotales.append(fechaAplicacionFormato);
												tablaTotales.append("&nbsp;</td>");
												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">");
												tablaTotales.append(FormatoMoneda.formateaMoneda(new Double(importe).doubleValue()));
												tablaTotales.append("&nbsp;</td>");
												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">");
												tablaTotales.append(session.getNominaNumberOfRecords());
												tablaTotales.append("&nbsp;</td>");
												tablaTotales.append("</table><br>");
												tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");

												nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());

												request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
												request.setAttribute("newMenu", session.getFuncionesDeMenu());
												request.setAttribute("MenuPrincipal", session.getStrMenu());
												request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
												request.setAttribute("TablaTotales", tablaTotales.toString());

												String infoUser = "La nomina fue aceptada correctamente: " + nombreArchivo_aceptado + " n&acute;mero de transmisi&oacute;n = " + transmision;
												infoUser = "cuadroDialogoCatNominaPrep (\"" + infoUser + "\",1)";
												request.setAttribute("infoUser", infoUser);

												if (sess.getAttribute("facultadesN") != null)
													request.setAttribute("facultades", sess.getAttribute("facultadesN"));
												request.setAttribute("tipoArchivo", "enviado");
												String botonestmp = "<br>" + "<table align=center border=0 cellspacing=0 cellpadding=0><tr>" + " <td><A href = javascript:js_regresarPagos(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>" + "<td></td></tr> </table>";

												request.setAttribute("botones", botonestmp);
												request.setAttribute("ContenidoArchivo", " ");
												EIGlobal.mensajePorTrace(textoLog + "botones >>" + request .getAttribute("botones") + "<<", EIGlobal.NivelLog.ERROR);
												sess.removeAttribute("VALIDAR");


												evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
											}
											else
											{
												HorarioDisp = "";
												HorarioSele = "";

												if (FacultadPremier.indexOf("PremierVerdadero") != -1) {
													HorarioDisp = (String) sess.getAttribute("SelectHorarioPremier");
													HorarioSele = (String) getFormParameter(request, "horario_seleccionado");
													EIGlobal.mensajePorTrace(textoLog + "El horario seleccionado es:   " + HorarioSele, EIGlobal.NivelLog.INFO);
												}
												EIGlobal.mensajePorTrace(textoLog + "****************BOX 2********************", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + ">>" + sess.getAttribute("statusHorario") + "<<", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + ">>" + request.getAttribute("hr_lost") + "<<", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + ">>" + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + "*******************BOX 2*****************", EIGlobal.NivelLog.ERROR);
												EIGlobal.mensajePorTrace(textoLog + "HORARIO PREMIER>>" + HorarioSele + "<<", EIGlobal.NivelLog.ERROR);

												request.setAttribute("folio", (String) getFormParameter(request, "folio"));
												EIGlobal.mensajePorTrace(textoLog + "CCBEst_Env" + estatus_envio, EIGlobal.NivelLog.DEBUG);
												EIGlobal.mensajePorTrace(textoLog + "tramasalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
												EIGlobal.mensajePorTrace(textoLog + "folioArchivo[" + folioArchivo + "]", EIGlobal.NivelLog.INFO);


												if (tramaSalida.equals("OK"))
												{
													EIGlobal.mensajePorTrace(textoLog + "PNOS Correcto, procediendo a generar tabla y mensaje", EIGlobal.NivelLog.DEBUG);

													//Inicio Nomina Arch
													NominaArchBO archBO = new NominaArchBO();
													NominaArchBean archBean = new NominaArchBean();
													final SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy HH:MM");
													final Date fecha=new Date();
													final String fchCarga=f.format(fecha);

													archBean.setFechaCarga(fchCarga);
													archBean.setContrato(contrato);
													archBean.setNombreArchivo(nombreArchivo);
													archBean.setNumRegistros(totalRegNom);
													archBean.setImporte(importePunto);
													archBean.setEstatusCarga("N");
													archBean.setTipoArchivo("P");

													archBean.setBiatuxOrigen(validaTerciaBiatux(archBean).getBiatuxOrigen());


													archBO.registraNominaArch(archBean);

													//Fin Nomina Arch

													sess.setAttribute("HorarioNormal", "0");
													String tablaTotales = generaTablaTotales(session.getCuentaCargo(),sess.getAttribute("descpCuentaCargo").toString(),fechaAplicacionFormato,importe,session.getNominaNumberOfRecords().toString());

													StringBuffer mensajeUser = new StringBuffer();

													mensajeUser.append("La n&oacute;mina est&aacute; siendo procesada.<br>");
													mensajeUser.append("El n&uacute;mero de folio correspondiente a la operaci&oacute;n es:<br>");
													mensajeUser.append(folioArchivo);
													mensajeUser.append("<br>Utillice este n&uacute;mero para consultar las<br>");
													mensajeUser.append("referencias y estado de las referencias,<br>");
													mensajeUser.append("en Seguimiento de Transferencias.");

													String infoUser = "cuadroDialogoCatNominaPrep (\"" + mensajeUser.toString() + "\",1)";
													request.setAttribute("infoUser", infoUser);

													if (sess.getAttribute("facultadesN") != null)
													{
														request.setAttribute("facultades", sess.getAttribute("facultadesN"));
													}


													String botonestmp = "<br>" + "<table align=center border=0 cellspacing=0 cellpadding=0><tr>" + " <td><A href = javascript:js_regresarPagos(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>" + "<td></td></tr> </table></td></tr>";
													request.setAttribute("botones", botonestmp);

													nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());
													request.setAttribute("archivo_actual", nombreArchivo);
													request.setAttribute("newMenu", session.getFuncionesDeMenu());
													request.setAttribute("MenuPrincipal", session.getStrMenu());
													request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
													request.setAttribute("TablaTotales", tablaTotales);
													request.setAttribute("ContenidoArchivo", " ");
													request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
													EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);
													/*try
													{
														EIGlobal.mensajePorTrace("INICIA BITACORIZACION TCT Nomina Prepago DISPERSION POR ARCHIVO (segunda)",EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace("Importe:"+importe,EIGlobal.NivelLog.INFO);
														EIGlobal.mensajePorTrace("ImportePunto:"+importePunto,EIGlobal.NivelLog.INFO);
														if(importePunto!=null && !importePunto.trim().equals("")){
															impBit=Double.parseDouble(importePunto);
														}else{impBit=0.0;}
														ctaOriBit=session.getCuentaCargo();
														if(ctaOriBit==null || ctaOriBit.trim().equals("")){
															ctaOriBit=" ";
														}

														int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
														String bitacora = llamada_bitacora(referencia,"NOMI0000",session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,NomPreUtil.ES_NP_PAGOS_IMPORTACION_OPER," ",0," ");
													}catch(Exception e){
														EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
													}*/
													 try{
																   EmailSender emailSender = new EmailSender();
													     		   EmailDetails EmailDetails = new EmailDetails();

																   EIGlobal.mensajePorTrace("<><><><><><><><><><><><><><><><><><> [NOMPREIMPORTOPCIONES NOTIFICACIONES] <><><><><><><><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
														           EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> CONTRATO: " + session.getContractNumber().toString(), EIGlobal.NivelLog.INFO);
														           EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> NOMBRE CONTRATO: " + session.getNombreContrato().toString(), EIGlobal.NivelLog.INFO);
														           EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> REFERENCIA DE ENLACE: " + folioArchivo , EIGlobal.NivelLog.INFO);
														           int numReg = Integer.parseInt (totalRegNom);
														           EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> REGISTROS IMPORTADOS: " + numReg, EIGlobal.NivelLog.INFO);
														           double impDouble = Double.valueOf(importePunto).doubleValue();
														    	   EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> IMPORTE: " +  String.valueOf(impDouble), EIGlobal.NivelLog.INFO);
														    	   EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> CUENTA: " + cuenta, EIGlobal.NivelLog.INFO);
														    	   EIGlobal.mensajePorTrace("*********************[NOMPREIMPORTOPCIONES] -> tramaSalida: " + tramaSalida, EIGlobal.NivelLog.INFO);
//														    	   EIGlobal.mensajePorTrace("*********************NUM TRANSMISION: " + transmisionMsg, EIGlobal.NivelLog.INFO);
														    	   EmailDetails.setNumeroContrato(session.getContractNumber().toString());
														           EmailDetails.setRazonSocial(session.getNombreContrato().toString());
														           EmailDetails.setNumRef(folioArchivo);
														           EmailDetails.setNumCuentaCargo(cuenta);
														           EmailDetails.setImpTotal(ValidaOTP.formatoNumero(impDouble));
														           EmailDetails.setNumTransmision(folioArchivo);
														           EmailDetails.setNumRegImportados(numReg);
														           EmailDetails.setTipoPagoNomina("Tarjeta de Pagos Santander ");
												           		   EmailDetails.setEstatusFinal("Enviada");

													   	   if (tramaSalida.startsWith("MANC")){
														   	   EmailDetails.setEstatusActual("Mancomunada");
													   	   } else {
													   		   EmailDetails.setEstatusActual("Enviada");
														   }
																	try {
																		emailSender.sendNotificacion(request,IEnlace.PAGO_NOMINA_PRE, EmailDetails);
																	} catch (Exception e) {
															EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
																	}
											 		} catch(Exception e) {
											 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
											 		}
													evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
												}
												else
													if (estatus_envio.equals("NOMI0050"))
													{
														sess.setAttribute("HorarioNormal", "0");
														sess.setAttribute("HR_P", HorarioSele);
														EIGlobal.mensajePorTrace(textoLog + "HR_P 2>>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);

														request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
														request.setAttribute("newMenu", session.getFuncionesDeMenu());
														request.setAttribute("MenuPrincipal", session.getStrMenu());
														request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

														String infoUser = " Archivo Duplicado. �Desea enviar este archivo de cualquier manera? ";
														String infoEncabezadoUser = "<B>ALERTA</B>";
														infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR

														request.setAttribute("infoUser", infoUser);
														if (sess.getAttribute("facultadesN") != null)
															request.setAttribute("facultades", sess.getAttribute("facultadesN"));
														sess.setAttribute("archDuplicado", "si");

														request.setAttribute("digit", "0");
														request.setAttribute("tipoArchivo", "duplicado");
														request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
														EIGlobal.mensajePorTrace(textoLog + "El horario seleccionado antes de volver a llamar el MantenimientoNomina despuesa de archivo duplicado es: ========>" + HorarioSele + "<======", EIGlobal.NivelLog.DEBUG);
														request.setAttribute("horario_seleccionado", HorarioSele);
														request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
														EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por Duplicado.", EIGlobal.NivelLog.DEBUG);
														evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
													}
													else
														if (estatus_envio.equals("NOMI0051"))
														{
															sess.setAttribute("HorarioNormal", "1");
															sess.setAttribute("HR_P", "17:00");
															EIGlobal.mensajePorTrace(textoLog + "HR_P 2 >>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);

															request.setAttribute("archivo_actual ",nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
															request.setAttribute("newMenu", session.getFuncionesDeMenu());
															request.setAttribute("MenuPrincipal", session.getStrMenu());
															request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

															String infoUser = " El vol&uacute;men que desea dispersar solamente puede ser procesado en el horario de 17:00";
															String infoEncabezadoUser = "<B>Aviso</B>";
															infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
															request.setAttribute("infoUser", infoUser);
															if (sess.getAttribute("facultadesN") != null)
																request.setAttribute("facultades", sess.getAttribute("facultadesN"));
															request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
															request.setAttribute("horarioPremier", "17:00");
															sess.setAttribute("archDuplicado", "si");
															request.setAttribute("digit", "2");
															request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
															EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario de 17:00 hrs.", EIGlobal.NivelLog.DEBUG);
															evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
														}
														else
															if (estatus_envio.equals("NOMI0052"))
															{
																EIGlobal.mensajePorTrace(textoLog + "Verificando los horarios saturados", EIGlobal.NivelLog.ERROR);
																EIGlobal.mensajePorTrace(textoLog + "Horario seleccionado: " + HorarioSele, EIGlobal.NivelLog.ERROR);

																String cad = (String) sess.getAttribute("HOR_SAT");
																if (cad == null)
																{
																	sess.setAttribute("HOR_SAT", HorarioSele);
																	cad = HorarioSele + "|";
																}
																else
																	cad = cad + HorarioSele + "|";
																sess.setAttribute("HOR_SAT", cad);
																EIGlobal.mensajePorTrace(textoLog + "Horarios Saturados: " + cad, EIGlobal.NivelLog.ERROR);
																EIGlobal.mensajePorTrace(textoLog + ">> Antes de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
																EI_Tipo hrD = new EI_Tipo();
																hrD.iniciaObjeto(HorarioDisp + "@");

																String nuevosHorariosDisp = "";
																EIGlobal.mensajePorTrace(textoLog + ">> Se buscaran horarios saturados en sesion<<", EIGlobal.NivelLog.ERROR);

																for (int i = 0; i < hrD.totalCampos; i++)
																{
																	EIGlobal.mensajePorTrace(textoLog + ">> Horario disponible: " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
																	if (!hrD.camposTabla[0][i].trim().equals(""))
																		if (!hrD.camposTabla[0][i].equals(HorarioSele))
																			if (!(cad.indexOf(hrD.camposTabla[0][i].trim()) >= 0))
																			{
																				EIGlobal.mensajePorTrace(textoLog + ">> El horario no se encontro : " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
																				EIGlobal.mensajePorTrace(textoLog + ">> en saturados: " + cad + "<<", EIGlobal.NivelLog.DEBUG);
																				nuevosHorariosDisp += hrD.camposTabla[0][i] + "|";
																			}
																}

																HorarioDisp = nuevosHorariosDisp;
																EIGlobal.mensajePorTrace(textoLog + ">> Despues de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
																sess.setAttribute("HorarioNormal", "0");
																sess.setAttribute("HR_P", HorarioSele);
																request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
																request.setAttribute("newMenu", session.getFuncionesDeMenu());
																request.setAttribute("MenuPrincipal", session.getStrMenu());
																request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
																String infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible.";
																String infoEncabezadoUser = "<B>Horario No Disponible</B>";

																if (HorarioDisp.trim().length() < 4)
																{
																	infoUser = "Por el momento no se encuentra disponible ning&uacute;n horario. Por favor cambie la fecha de aplicaci&oacute;n del archivo para realizar su env&iacute;o.";
																	infoUser = "cuadroDialogoPagosNomina(\" <b>Horarios No Disponibles</b>\",\"" + infoUser + "\",10)";
																}
																else
																{
																	if (FacultadPremier.indexOf("PremierVerdadero") != -1)
																	{
																		infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible. Por favor elija otro horario entre los siguientes: ";
																		infoUser += obtenSeleccionInd(HorarioDisp, HorarioSele, 4);
																		infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
																	}
																	else
																	{
																		infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
																	}
																}

																request.setAttribute("infoUser", infoUser);
																if (sess.getAttribute("facultadesN") != null)
																	request.setAttribute("facultades", sess.getAttribute("facultadesN"));
																request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));

																if (FacultadPremier.indexOf("PremierVerdadero") != -1)
																{
																	request.setAttribute("horario_seleccionado", HorarioSele);
																}

																sess.setAttribute("archDuplicado", "si");
																request.setAttribute("digit", "0");
																request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
																EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario saturado.", EIGlobal.NivelLog.DEBUG);
																evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
															}
															else
																if (estatus_envio.equals("NOMI0020"))
																{
																	sess.setAttribute("HorarioNormal", "0");
																	request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
																	request.setAttribute("newMenu", session.getFuncionesDeMenu());
																	request.setAttribute("MenuPrincipal", session.getStrMenu());
																	request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

																	String infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n.";
																	String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";

																	if (FacultadPremier.indexOf("PremierVerdadero") != -1)
																	{
																		infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n. Por favor elija uno de los siguientes horarios:";
																		infoUser += obtenSeleccion(HorarioDisp, HorarioSele, 2);
																		infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
																	}
																	else
																	{
																		infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
																	}
																	request.setAttribute("infoUser", infoUser);

																	if (sess.getAttribute("facultadesN") != null)
																		request.setAttribute("facultades", sess.getAttribute("facultadesN"));
																	request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));

																	if (FacultadPremier.indexOf("PremierVerdadero") != -1)
																	{
																		request.setAttribute("horario_seleccionado", HorarioSele);
																	}

																	sess.setAttribute("archDuplicado", "si");
																	request.setAttribute("digit", "0");
																	request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
																	EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por media hora de anticipacion.", EIGlobal.NivelLog.DEBUG);
																	evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
																}
																else
																{
																	sess.setAttribute("HorarioNormal", "0");
																	String mensaje_error = "";
																	try
																	{
																		if (tramaSalida.startsWith("MANC"))
																			mensaje_error = tramaSalida.substring(16, tramaSalida.length());
																		else
																			mensaje_error = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
																	} catch (Exception e) {
																		mensaje_error = "";
																	}

																	EIGlobal.mensajePorTrace(textoLog + "mensaje_error: >>" + mensaje_error + "<<", EIGlobal.NivelLog.ERROR);
																	if (mensaje_error.equals(""))
																		mensaje_error = "Servcio no disponible en este momento, intente m&aacute;s tarde !.";

																	String bgcolor = "textabdatobs";
																	StringBuffer tablaTotales = new StringBuffer();
																	tablaTotales.append("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
																	tablaTotales.append("<tr> ");
																	tablaTotales.append("<td class=\"textabref\">Pago de Tarjeta de Pagos Santander </td>");//YHG NPRE
																	tablaTotales.append("</tr>");
																	tablaTotales.append("</table>");
																	tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
																	tablaTotales.append("<tr> ");
																	tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
																	tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descipci&oacute;n</td>");
																	tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
																	tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
																	tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
																	tablaTotales.append("</tr>");
																	tablaTotales.append("<td class=\"");
																	tablaTotales.append(bgcolor);
																	tablaTotales.append("\" nowrap align=\"center\">");
																	tablaTotales.append(session.getCuentaCargo());
																	tablaTotales.append("&nbsp;</td>");
																	tablaTotales.append("<td class=\"");
																	tablaTotales.append(bgcolor);
																	tablaTotales.append("\" nowrap align=\"center\">");
																	tablaTotales.append(sess.getAttribute("descpCuentaCargo"));
																	tablaTotales.append("&nbsp;</td>");
																	tablaTotales.append("<td class=\"");
																	tablaTotales.append(bgcolor);
																	tablaTotales.append("\" nowrap align=\"center\">");
																	tablaTotales.append(fechaAplicacionFormato);
																	tablaTotales.append("&nbsp;</td>");
																	tablaTotales.append("<td class=\"");
																	tablaTotales.append(bgcolor);
																	tablaTotales.append("\" nowrap align=\"center\">");
																	tablaTotales.append(formatoMoneda(importe));
																	tablaTotales.append("&nbsp;</td>");
																	tablaTotales.append("<td class=\"");
																	tablaTotales.append(bgcolor);
																	tablaTotales.append("\" nowrap align=\"center\">");
																	tablaTotales.append(session.getNominaNumberOfRecords());
																	tablaTotales.append("&nbsp;</td>" + "</table><br>");
																	nombreArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length());

																	request.setAttribute("archivo_actual", nombreArchivo.substring(nombreArchivo.lastIndexOf('/') + 1, nombreArchivo.length()));
																	request.setAttribute("newMenu", session.getFuncionesDeMenu());
																	request.setAttribute("MenuPrincipal", session.getStrMenu());
																	request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
																	request.setAttribute("TablaTotales", tablaTotales.toString());

																	String infoUser = mensaje_error;
																	infoUser = "cuadroDialogoCatNomina (\"" + infoUser + "\",1)";
																	request.setAttribute("infoUser", infoUser);
																	if (sess.getAttribute("facultadesN") != null)
																	{
																		request.setAttribute("facultades", sess.getAttribute("facultadesN"));
																	}

																	String botonestmp = "<br>" + "<table align=center border=0 cellspacing=0 cellpadding=0><tr>" + " <td><A href = javascript:js_regresarPagos(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>" + "<td></td></tr> </table></td></tr>";
																	request.setAttribute("botones", botonestmp);
																	request.setAttribute("ContenidoArchivo", " ");
																	request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
																	EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);

																	//posible bitacora
//																	codigo bitacora
																	/*try
																	{
																		EIGlobal.mensajePorTrace("INICIA BITACORIZACION TCT Nomina Prepago DISPERSION POR ARCHIVO (trercera)",EIGlobal.NivelLog.INFO);
																		impBit= Double.parseDouble(importe);
																		ctaOriBit=session.getCuentaCargo();
																		if(ctaOriBit==null || ctaOriBit.trim().equals("")){
																			ctaOriBit=" ";
																		}
																		if(estatus_envio!=null && !estatus_envio.trim().equals("")){
																			codErrBit=estatus_envio;
																		}else{codErrBit=" ";}
																		int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
																		String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,NomPreUtil.ES_NP_PAGOS_IMPORTACION_OPER," ",0," ");
																	}catch(Exception e){
																		EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
																	}*/
																	evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);

																}
											}
										}
										else
										{
											EIGlobal.mensajePorTrace(textoLog + "***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);

											request.setAttribute("MenuPrincipal", session.getStrMenu());
											request.setAttribute("newMenu", session.getFuncionesDeMenu());
											request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
											request.setAttribute("operacion", "tranopera");
											evalTemplate("/jsp/prepago/NomImportTranenProceso.jsp", request, response);
										}
									}// Termina else para interrumpe el flujo
								} // Termina operacion igual a envio

								if (strOperacion.equals("enviarCorreo"))
								{ // enviar el correo

									EIGlobal.mensajePorTrace(textoLog + "cancelar- ", EIGlobal.NivelLog.DEBUG);
									String contrato = session.getContractNumber();
									String empleado = session.getUserID8();
									String perfil = session.getUserProfile();
									String numEmpl="";
									String fchAplicacion="";
									String ctaCargo="";
									String numTarjeta="";
									String importe="";
									String estatus="";
									String nomArchivo="";

									String valores_cancelar = (String) getFormParameter(request, "valor_radio"); //valor del radio boton
									valores_cancelar = valores_cancelar.trim();
									EIGlobal.mensajePorTrace(textoLog + "cancelar- Valor del radio >>" + valores_cancelar, EIGlobal.NivelLog.DEBUG);
									String archivo = valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
									String secuencia = valores_cancelar.substring(posCar(valores_cancelar, '@', 1) + 1, posCar(valores_cancelar, '@', 2));
									String fechaRecepcion = valores_cancelar.substring(posCar(valores_cancelar, '@', 2) + 1, posCar(valores_cancelar, '@', 3));
									EIGlobal.mensajePorTrace(textoLog + "cancelar- fechaRecepcion:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- fechaRecepcion formateada:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- secuencia:>>" + secuencia, EIGlobal.NivelLog.DEBUG);
									String lista_nombres_archivos = (String) getFormParameter(request, "lista_nombres_archivos"); //del radio boton
									String arch_salida_consulta = (String) getFormParameter(request, "nombre_arch_salida"); // /tmp/10001851.nomCon
									String correo = (String) getFormParameter(request, "CorreoE");

									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
									request.setAttribute("Fechas", ObtenFecha(true));
									request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");

									if (contrato.equals("") || contrato == null)
										contrato = " ";
									if (archivo.equals(""))
										archivo = " ";

									EIGlobal.mensajePorTrace(textoLog + "cancelar- arch_salida_consulta:>>" + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
									arch_salida_consulta = arch_salida_consulta.trim();
									EIGlobal.mensajePorTrace(textoLog + "cancelar- arch_salida_consulta:>>" + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- archivo:>>" + archivo, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- contrato:>>" + contrato, EIGlobal.NivelLog.DEBUG);
									ServicioTux tuxGlobal = new ServicioTux();
									Hashtable htResult = null;
									String tramaEntrada = "";
									String direccionCorreo = "";

									if (correo.indexOf("@") > 0)
										direccionCorreo = correo.substring(0, correo.indexOf("@")) + "[]" + correo.substring(correo.indexOf("@") + 1);

									EIGlobal.mensajePorTrace(textoLog + "Correo>>" + direccionCorreo.trim() + "<<", EIGlobal.NivelLog.ERROR);
									tramaEntrada = "1EWEB|" + empleado + "|NPEM|" + contrato + "|" + empleado + "|" + perfil + "|" + contrato + "@" + secuencia + "@" + direccionCorreo + "@";
									//tramaEntrada = "1EWEB|" + empleado + "|NPEA|" + contrato + "|" + empleado + "|" + perfil + "|" + contrato + "@" + secuencia + "@" + direccionCorreo + "@";
									EIGlobal.mensajePorTrace(textoLog + "Trama enviada a NOCA:>>" + tramaEntrada + "<<", EIGlobal.NivelLog.ERROR);

									try
									{
										htResult = tuxGlobal.web_red(tramaEntrada);
									} catch (java.rmi.RemoteException re) {
										re.printStackTrace();
									} catch (Exception e) {
									}

									String tramaSalida = (String) htResult.get("BUFFER");
									EIGlobal.mensajePorTrace(textoLog + "El servicio nomi_enviardetalle regreso: >>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
									EIGlobal.mensajePorTrace("Trama de Entrada :" + tramaEntrada, NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalida, NivelLog.DEBUG);
									if (tramaSalida == null || tramaSalida.trim().equals(""))
										tramaSalida = "NOMI9999        @Error en el servicio, intente mas tarde@";

									String estatus_transacion = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
									String mensaje_transacion = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@', 2));

									ArchivosNomina archSalida = new ArchivosNomina(arch_salida_consulta, request);

									int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

									fchAplicacion = valores_cancelar.substring(posCar(valores_cancelar, '@', 3) + 1, posCar(valores_cancelar, '@', 4));
									ctaCargo= valores_cancelar.substring(posCar(valores_cancelar, '@', 4) + 1, posCar(valores_cancelar, '@', 5));
									if(sess.getAttribute("tipoBusqueda").equals("PI")){
										nomArchivo = valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
										importe=valores_cancelar.substring(posCar(valores_cancelar, '@', 5) + 1, posCar(valores_cancelar, '@', 6));
										estatus=valores_cancelar.substring(posCar(valores_cancelar, '@', 6) + 1, valores_cancelar.length());
									}else{
										numEmpl=valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
										numTarjeta=valores_cancelar.substring(posCar(valores_cancelar, '@', 5) + 1, posCar(valores_cancelar, '@', 6));
										importe=valores_cancelar.substring(posCar(valores_cancelar, '@', 6) + 1, posCar(valores_cancelar, '@', 7));
										estatus=valores_cancelar.substring(posCar(valores_cancelar, '@', 7) + 1, valores_cancelar.length());
									}

									SimpleDateFormat sfd=new SimpleDateFormat("dd/MM/yyyy");

									BitaTransacBean bt = new BitaTransacBean();
									bt.setNumBit(BitaConstants.ES_NP_PAGOS_CONSULTA_CORREO);

									if(secuencia!=null && !secuencia.trim().equals(""))
										bt.setReferencia(Long.parseLong(secuencia));

									if(nomArchivo!=null && !nomArchivo.trim().equals(""))
										bt.setNombreArchivo(nomArchivo);

									if(fchAplicacion!=null && !fchAplicacion.trim().equals("")){
										try {
											bt.setFechaAplicacion(sfd.parse(fchAplicacion));
										} catch (ParseException e1) {}
									}
									if(ctaCargo!=null && !ctaCargo.trim().equals(""))
										bt.setCctaOrig(ctaCargo);

									if(importe!=null && !importe.trim().equals("")){
										bt.setImporte(Double.parseDouble(importe));
									}

									if(estatus!=null && !estatus.trim().equals(""))
										bt.setEstatus(estatus.substring(0, 1));

									if(numEmpl!=null && !numEmpl.trim().equals(""))
										bt.setBeneficiario(numEmpl);
									if(numTarjeta!=null && !numTarjeta.trim().equals(""))
										bt.setCctaDest(numTarjeta);

									NomPreUtil.bitacoriza(request,"NOMI", estatus_transacion, bt);

									if (estatus_transacion.equals("NOMI0000"))
									{
										String contenidoArch = "";
										if(sess.getAttribute("tipoBusqueda").equals("PI"))
										{
											contenidoArch = archSalida.lecturaArchivoSalidaConsul(lista_nombres_archivos);
										}
										else
										{
											contenidoArch = archSalida.lecturaArchivoSalidaConsulInd(lista_nombres_archivos);
										}
										//String contenidoArch = archSalida.lecturaArchivoSalidaConsul(lista_nombres_archivos);
										if (contenidoArch.equals("") || contenidoArch.indexOf("Error ") >= 0 || contenidoArch.startsWith("Error"))
										{
											String elMensaje = "Error en la lectura del archivo, Intente m&aacute;s tarde por favor.";
											String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
											request.setAttribute("infoUser", infoUser);
											if (sess.getAttribute("facultadesN") != null)
												request.setAttribute("facultades", sess.getAttribute("facultadesN"));

											request.setAttribute("nombre_arch_salida", nombre_arch_salida);
											request.setAttribute("lista_nombres_archivos", lista_nombres_archivos);
											String botonestmp = "<br>" + "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
											+ "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table>";
											request.setAttribute("botones", botonestmp);
											request.setAttribute("ContenidoArchivo", contenidoArch);
											evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
											//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
										}
										else
										{
											ArchivosNomina archSalida1 = new ArchivosNomina(arch_salida_consulta, request);
											String contenidoArchivo = "";
											if(sess.getAttribute("tipoBusqueda").equals("PI"))
											{
												contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);
											}
											else
											{
												contenidoArchivo = archSalida1.lecturaArchivoSalidaConsulInd(lista_nombres_archivos);
											}

											lista_nombres_archivos += ";" + "";
											EIGlobal.mensajePorTrace(textoLog + "cancelar- lista_nombres_archivos:>>" + lista_nombres_archivos, EIGlobal.NivelLog.DEBUG);
											//String contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);
											String infoUser = "cuadroDialogo (\"" + mensaje_transacion + "\",1)";

											request.setAttribute("infoUser", infoUser);
											request.setAttribute("ContenidoArchivo", contenidoArchivo);
											request.setAttribute("nombre_arch_salida", arch_salida_consulta);

											if (sess.getAttribute("facultadesN") != null)
												request.setAttribute("facultades", sess.getAttribute("facultadesN"));

											request.setAttribute("lista_nombres_archivos", lista_nombres_archivos);
											arch_exportar = session.getUserID8() + "Nom.doc";
											String botonestmp = "<br>"
												+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
												+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
												+ "<td><A href = \"/Download/"
												+ arch_exportar
												+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>";
												if(sess.getAttribute("tipoBusqueda").equals("PI"))
												{
													String correo2 = "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>" ;
													botonestmp = botonestmp + correo2;
												}
												botonestmp = botonestmp + "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
												+ "<td></td></tr> </table>";
												//+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
												//+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
												//+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
												//+ "<td></td></tr> </table>";
											request.setAttribute("botones", botonestmp);
											evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
											//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
										}
									}
									else
									{ // Transaccion Exitosa
										String contenidoArchivo = "";
										if(sess.getAttribute("tipoBusqueda").equals("PI"))
										{
											contenidoArchivo = archSalida.lecturaArchivoSalidaConsul();
										}
										else
										{
											contenidoArchivo = archSalida.lecturaArchivoSalidaConsulInd();
										}

										//String contenidoArchivo = archSalida.lecturaArchivoSalidaConsul();
										request.setAttribute("ContenidoArchivo",contenidoArchivo);
										request.setAttribute("nombre_arch_salida",arch_salida_consulta);
										String infoUser = "cuadroDialogo (\""+ mensaje_transacion + "\",1)";
										request.setAttribute("infoUser", infoUser);

										if (sess.getAttribute("facultadesN") != null)
											request.setAttribute("facultades", sess.getAttribute("facultadesN"));
										arch_exportar = session.getUserID8() + "Nom.doc";
										String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
											+ "<td><A href = \"/Download/"
											+ arch_exportar
											+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>";
											if(sess.getAttribute("tipoBusqueda").equals("PI"))
											{
												String correo2 = "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>" ;
												botonestmp = botonestmp + correo2;
											}
											botonestmp = botonestmp + "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table>";
											//+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
											//+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
											//+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											//+ "<td></td></tr> </table>";
										request.setAttribute("botones", botonestmp);
										evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
										//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
									}
								}

								if (strOperacion.equals("cancelar"))
								{ // cancelar una nomina
									EIGlobal.mensajePorTrace(textoLog + "cancelar- ", EIGlobal.NivelLog.DEBUG);
									String diaRecpcion = "";
									String mesRecpcion = "";
									String anoRecpcion = "";
									String contrato = session.getContractNumber();
									String empleado = session.getUserID8();
									String perfil = session.getUserProfile();
									String numEmpl="";
									String fchAplicacion="";
									String ctaCargo="";
									String numTarjeta="";
									String importe="";
									String estatus="";
									String nomArchivo="";

									String valores_cancelar = (String) request.getParameter("valor_radio"); // valor del radio boton
									valores_cancelar = valores_cancelar.trim();
									EIGlobal.mensajePorTrace(textoLog + "cancelar- Valor del radio :::::::::>>" + valores_cancelar, EIGlobal.NivelLog.DEBUG);
									String archivo = valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
									String secuencia = valores_cancelar.substring(posCar(valores_cancelar, '@', 1) + 1, posCar(valores_cancelar, '@', 2));
									String fechaRecepcion = valores_cancelar.substring(posCar(valores_cancelar, '@', 2) + 1, posCar(valores_cancelar, '@', 3));
									EIGlobal.mensajePorTrace(textoLog + "cancelar- fechaRecepcion:>>" + fechaRecepcion, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(""+archivo+" "+secuencia+" "+fechaRecepcion, NivelLog.INFO);
									if (fechaRecepcion.length() >= 10)
									{
										diaRecpcion = fechaRecepcion.substring(0, posCar(fechaRecepcion, '/', 1));
										mesRecpcion = fechaRecepcion.substring(posCar(fechaRecepcion, '/', 1) + 1, posCar(fechaRecepcion, '/', 2));
										anoRecpcion = fechaRecepcion.substring(posCar(fechaRecepcion, '/', 2) + 1, fechaRecepcion.length());
										fechaRecepcion = diaRecpcion + mesRecpcion + anoRecpcion;
										fechaRecepcion = fechaRecepcion.trim();
									}

									EIGlobal.mensajePorTrace(textoLog + "cancelar- fechaRecepcion formateada:>>" + fechaRecepcion + "<<", EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- secuencia:>>" + secuencia + "<<", EIGlobal.NivelLog.DEBUG);
									String lista_nombres_archivos = (String) request.getParameter("lista_nombres_archivos"); // del radio boton
									String lista_names = (String) request.getParameter("valor_radio"); // del radio boton
									EIGlobal.mensajePorTrace(lista_nombres_archivos, NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(lista_names, NivelLog.DEBUG);
									String arch_salida_consulta = (String) request.getParameter("nombre_arch_salida");
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
									request.setAttribute("Fechas", ObtenFecha(true));
									request.setAttribute("FechaHoy", ObtenFecha(false)+ " - 06");

									if (contrato.equals("") || contrato == null)
										contrato = " ";
									if (archivo.equals(""))
										archivo = " ";

									EIGlobal.mensajePorTrace(textoLog + "cancelar- arch_salida_consulta:>>" + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
									arch_salida_consulta = arch_salida_consulta.trim();
									EIGlobal.mensajePorTrace(textoLog + "cancelar- arch_salida_consulta:>>" + arch_salida_consulta + "<>", EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- archivo:>>" + archivo, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace(textoLog + "cancelar- contrato:>>" + contrato, EIGlobal.NivelLog.DEBUG);
									ServicioTux tuxGlobal = new ServicioTux();
									Hashtable htResult = null;
									String tramaEntrada = "";
									if(sess.getAttribute("tipoBusqueda").equals("PI"))
										tramaEntrada = "1EWEB|" + empleado + "|CAMP|" + contrato + "|" + empleado + "|" + perfil + "|" + contrato + "@" + secuencia + "@" + fechaRecepcion + "@";
									else
										tramaEntrada = "1EWEB|" + empleado + "|CAPI|" + contrato + "|" + empleado + "|" + perfil + "|" + contrato + "@" + secuencia + "@" + fechaRecepcion + "@";

									EIGlobal.mensajePorTrace(textoLog + "Trama enviada:>>" + tramaEntrada + "<<", EIGlobal.NivelLog.ERROR);

									try
									{
										htResult = tuxGlobal.web_red(tramaEntrada);
									} catch (java.rmi.RemoteException re) {
										re.printStackTrace();
									} catch (Exception e) {
									}
									EIGlobal.mensajePorTrace("Trama de entrada :" + tramaEntrada, NivelLog.DEBUG);
									String tramaSalida = (String) htResult.get("BUFFER");
									EIGlobal.mensajePorTrace(textoLog + "El servicio nomi_cancela regreso: >>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
									EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalida, NivelLog.DEBUG);
									if (tramaSalida == null || tramaSalida.trim().equals(""))
										tramaSalida = "NOMI9999        @Error en el servicio, intente mas tarde@";

									String estatus_transacion = tramaSalida.substring(0,posCar(tramaSalida, '@', 1));
									String mensaje_transacion = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@',2));
									ArchivosNomina archSalida = new ArchivosNomina(arch_salida_consulta, request);
									EIGlobal.mensajePorTrace("NomPreAltaTarjeta " + sess.getAttribute("tipoBusqueda").toString().trim(), EIGlobal.NivelLog.DEBUG);

									fchAplicacion = valores_cancelar.substring(posCar(valores_cancelar, '@', 3) + 1, posCar(valores_cancelar, '@', 4));
									ctaCargo= valores_cancelar.substring(posCar(valores_cancelar, '@', 4) + 1, posCar(valores_cancelar, '@', 5));
									EIGlobal.mensajePorTrace("Fecha aplicacion=>"+fchAplicacion, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("ctaCargo=>"+ctaCargo, EIGlobal.NivelLog.DEBUG);
									String opertct="";
									double impBit=0.0;
									String ctaOriBit=" ";
									String ctaDesBit=" ";
									String codErrBit=" ";
									int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

									if(sess.getAttribute("tipoBusqueda").equals("PI")){
										opertct=NomPreUtil.ES_NP_PAGOS_CONSULTA_CANCELA_IE_OPER;
										nomArchivo = valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
										importe=valores_cancelar.substring(posCar(valores_cancelar, '@', 5) + 1, posCar(valores_cancelar, '@', 6));
										estatus=valores_cancelar.substring(posCar(valores_cancelar, '@', 6) + 1, valores_cancelar.length());

									}else{
										opertct=NomPreUtil.ES_NP_PAGOS_CONSULTA_CANCELA_PI_OPER;
										numEmpl=valores_cancelar.substring(0, posCar(valores_cancelar, '@', 1));
										numTarjeta=valores_cancelar.substring(posCar(valores_cancelar, '@', 5) + 1, posCar(valores_cancelar, '@', 6));
										importe=valores_cancelar.substring(posCar(valores_cancelar, '@', 6) + 1, posCar(valores_cancelar, '@', 7));
										estatus=valores_cancelar.substring(posCar(valores_cancelar, '@', 7) + 1, valores_cancelar.length());
									}

									SimpleDateFormat sfd=new SimpleDateFormat("dd/MM/yyyy");

									BitaTransacBean bt = new BitaTransacBean();
									bt.setNumBit(BitaConstants.ES_NP_PAGOS_CONSULTA_CANCELA);

									if(secuencia!=null && !secuencia.trim().equals(""))
										bt.setReferencia(Long.parseLong(secuencia));

									if(nomArchivo!=null && !nomArchivo.trim().equals(""))
										bt.setNombreArchivo(nomArchivo);

									if(fchAplicacion!=null && !fchAplicacion.trim().equals("")){
										try {
											bt.setFechaAplicacion(sfd.parse(fchAplicacion));
										} catch (ParseException e1) {}
									}
									if(ctaCargo!=null && !ctaCargo.trim().equals("")){
										bt.setCctaOrig(ctaCargo);
										ctaOriBit=ctaCargo;
									}else{ctaOriBit=" ";}

									if(importe!=null && !importe.trim().equals("")){
										impBit=Double.parseDouble(importe);
										bt.setImporte(impBit);
									}

									if(estatus!=null && !estatus.trim().equals(""))
										bt.setEstatus(estatus.substring(0, 1));

									if(numEmpl!=null && !numEmpl.trim().equals(""))
										bt.setBeneficiario(numEmpl);
									if(numTarjeta!=null && !numTarjeta.trim().equals("")){
										bt.setCctaDest(numTarjeta);
										ctaDesBit=numTarjeta;
									}else{ctaDesBit=" ";}
									if(estatus_transacion!=null && !estatus_transacion.trim().equals("")){
										codErrBit=estatus_transacion;
									}

									NomPreUtil.bitacoriza(request,"NOMI", codErrBit, bt);

									if (estatus_transacion.equals("NOMI0000"))
									{
										String contenidoArch = "";
										if(sess.getAttribute("tipoBusqueda").equals("PI"))
										{
											contenidoArch = archSalida.lecturaArchivoSalidaConsul(lista_nombres_archivos);
										}
										else
										{
											contenidoArch = archSalida.lecturaArchivoSalidaConsulInd(lista_names);
										}
										//String contenidoArch = archSalida.lecturaArchivoSalidaConsul(lista_nombres_archivos);
										if (contenidoArch.equals("")|| contenidoArch.indexOf("Error ") >= 0	|| contenidoArch.startsWith("Error"))
										{
											String elMensaje = "Error en la lectura del archivo, Intente m&aacute;s tarde por favor.";
											String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
											request.setAttribute("infoUser", infoUser);
											if (sess.getAttribute("facultadesN") != null)
												request.setAttribute("facultades", sess.getAttribute("facultadesN"));
											request.setAttribute("nombre_arch_salida",nombre_arch_salida);
											request.setAttribute("lista_nombres_archivos",lista_nombres_archivos);
											String botonestmp = "<br>"
												+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
												+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
												//+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
												+ " <td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
												+ "<td></td></tr> </table>";
											request.setAttribute("botones", botonestmp);
											request.setAttribute("ContenidoArchivo",contenidoArch);
											evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
											//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
										}
										else
										{/*
											ArchivosNomina archSalida1 = new ArchivosNomina(arch_salida_consulta, request);
											lista_nombres_archivos += ";" + archivo;
											EIGlobal.mensajePorTrace(textoLog + "cancelar- lista_nombres_archivos:>>" + lista_nombres_archivos + "<<", EIGlobal.NivelLog.DEBUG);
											String contenidoArchivo = "";
											if(sess.getAttribute("tipoBusqueda").equals("PI"))
											{
												contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);
											}
											else
											{
												contenidoArchivo = archSalida1.lecturaArchivoSalidaConsulInd(lista_names);
											}
											*/
											//String contenidoArchivo = archSalida1.lecturaArchivoSalidaConsul(lista_nombres_archivos);


											String tramaEntradaConsul = sess.getAttribute("tramaEntradaConsul").toString();
											 EIGlobal.mensajePorTrace("Trama de Entrada :" + tramaEntradaConsul, NivelLog.DEBUG);
											try {
												EIGlobal.mensajePorTrace(textoLog + "tramaEntrada >>" + tramaEntradaConsul + "<<", EIGlobal.NivelLog.INFO);
												htResult = tuxGlobal.web_red(tramaEntradaConsul);
											} catch (java.rmi.RemoteException re) {
												re.printStackTrace();
											} catch (Exception e) {
											}

											String tramaSalidaConsul = (String) htResult.get("BUFFER");

											EIGlobal.mensajePorTrace(textoLog + "El servicio consulta_nomi regreso:1 " + tramaSalidaConsul + "***", EIGlobal.NivelLog.DEBUG);
											 EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalidaConsul, NivelLog.DEBUG);

												String nombreArchivo = tramaSalidaConsul.substring(tramaSalidaConsul.lastIndexOf("/") + 1, tramaSalidaConsul.lastIndexOf("@"));
												String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
												try {
													String tipoCopia = "2"; // de /t/tuxedo... a /tmp
													ArchivoRemoto archR = new ArchivoRemoto();
													if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
														EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
												} catch (Exception ioeSua) {
													EIGlobal.mensajePorTrace(textoLog + "Excepcion %envio() >> " + ioeSua.getMessage() + " <<", EIGlobal.NivelLog.ERROR);
												}

												nombre_arch_salida = IEnlace.DOWNLOAD_PATH + nombreArchivo;
												ArchivosNomina archSalidaConsul = new ArchivosNomina(nombre_arch_salida,request);
												String contenidoArchConsul = null;
												if(sess.getAttribute("tipoBusqueda").equals("PI"))
												{
													contenidoArchConsul = archSalidaConsul.lecturaArchivoSalidaConsul();
												}
												else
												{
													contenidoArchConsul = archSalidaConsul.lecturaArchivoSalidaConsulInd();
												}

												String arch_exportarConsul = "";
												arch_exportarConsul = session.getUserID8() + "Nom.doc";
												try {
													String tipoCopia = "2"; // de /t/tuxedo... a /tmp
													ArchivoRemoto archR = new ArchivoRemoto();
													//copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
													//JGAL - Copia a ruta de download
													if (!archR.copiaOtroPathRemoto(nombreArchivo, Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportarConsul, nueva_ruta))
														EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
												} catch (Exception ioeSua) {
													EIGlobal.mensajePorTrace(textoLog + "Excepcion %envio() >> " + ioeSua.getMessage() + " <<", EIGlobal.NivelLog.ERROR);
												}

											mensaje_transacion = "SE HA REALIZADO LA CANCELACI&Oacute;N EXITOSAMENTE";
											String infoUser = "cuadroDialogo (\""+ mensaje_transacion + "\",1)";
											request.setAttribute("infoUser", infoUser);
											request.setAttribute("ContenidoArchivo",contenidoArchConsul);
											request.setAttribute("nombre_arch_salida",arch_salida_consulta);

											if (sess.getAttribute("facultadesN") != null)
												request.setAttribute("facultades", sess.getAttribute("facultadesN"));

											request.setAttribute("lista_nombres_archivos",lista_nombres_archivos);
											arch_exportar = session.getUserID8() + "Nom.doc";
											String botonestmp = "<br>"
												+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
												+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
												+"<td><A href = \"/Download/"
												+ arch_exportar
												+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>";
												if(sess.getAttribute("tipoBusqueda").equals("PI"))
												{
													String correo = "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>" ;
													botonestmp = botonestmp + correo;
												}
												botonestmp = botonestmp + "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
												+ "<td></td></tr> </table>";
												//+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
												//+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
												//+ " <td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
												//+ "<td></td></tr> </table>";
											request.setAttribute("botones", botonestmp);
											/*try
											{
												String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,opertct,ctaDesBit,0," ");
											}catch(Exception e){
												EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
											}*/
											evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
											//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
										}
									}
									else
									{ // Transaccion Exitosa
										String contenidoArchivo = "";
										if(sess.getAttribute("tipoBusqueda").equals("PI"))
										{
											contenidoArchivo = archSalida.lecturaArchivoSalidaConsul();
										}
										else
										{
											contenidoArchivo = archSalida.lecturaArchivoSalidaConsulInd();
										}

										request.setAttribute("ContenidoArchivo",contenidoArchivo);
										request.setAttribute("nombre_arch_salida",arch_salida_consulta);
										String infoUser = "cuadroDialogo (\""+ mensaje_transacion + "\",1)";
										request.setAttribute("infoUser", infoUser);

										if (sess.getAttribute("facultadesN") != null)
											request.setAttribute("facultades", sess.getAttribute("facultadesN"));

										arch_exportar = session.getUserID8() + "Nom.doc";
										String botonestmp = "<br>"
											+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
											+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
											+ "<td><A href = \"/Download/"
											+ arch_exportar
											+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>";
											if(sess.getAttribute("tipoBusqueda").equals("PI"))
											{
												String correo = "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>" ;
												botonestmp = botonestmp + correo;
											}
											botonestmp = botonestmp + "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											+ "<td></td></tr> </table>";
											//+ "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>"
											//+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
											//+ "<td><A href = javascript:js_regresar(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
											//+ "<td></td></tr> </table>";
										request.setAttribute("botones", botonestmp);
										evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
										//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
									}
								}
								if (strOperacion.equals("consulta"))
								{
									consultaArchivos(request, response);
								}

								String arcRecuperado = "";
								String tmpTipoArchivo = "";

								if (strOperacion.equals("reporte"))
								{
									request.setAttribute("archivo", "Pagos");
									arcRecuperado = getFormParameter(request, "tipoArchivo");
									String registrosEnArchivo = request.getParameter("cantidadDeRegistrosEnArchivo");
									EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE registrosEnArchivo dentro de reporte>>" + registrosEnArchivo + "<<", EIGlobal.NivelLog.DEBUG);
									arcRecuperado.trim();
									EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE arcRecuperado> antes>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);

									if (arcRecuperado.equals(""))
										arcRecuperado = "nuevo";

									EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE arcRecuperado>>" + arcRecuperado + "<<", EIGlobal.NivelLog.DEBUG);
									tmpTipoArchivo = arcRecuperado.substring(0, 1);
									EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE tmpTipoArchivo>>" + tmpTipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);

									if (tmpTipoArchivo.equals("r"))
									{
										nombre_arch_salida = Global.DOWNLOAD_PATH + getFormParameter(request, "archivo_actual");
										String secuencia_recu = getFormParameter(request,"transmision");
										ArchivosNomina archRecuperado = new ArchivosNomina(nombre_arch_salida, request);
										request.setAttribute("newMenu", session.getFuncionesDeMenu());
										request.setAttribute("MenuPrincipal", session.getStrMenu());
										request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//NPRE
										request.setAttribute("etiquetaNameFile","Nombre del archivo: Archivo Recuperado");
										request.setAttribute("etiquetaNumTransmision","N&uacute;mero de Transmisi&oacute;n: " + secuencia_recu);
										request.setAttribute("tipoArchivo", arcRecuperado);
										request.setAttribute("cantidadDeRegistrosEnArchivo",registrosEnArchivo);
										request.setAttribute("ContenidoArchivo", archRecuperado.reporteArchivoSalida());
									}
									else
									{
										String archivoName = (String)sess.getAttribute("nombreArchivo");
										String nombreArchivoOriginal = archivoName.substring(archivoName.lastIndexOf("/") + 1);
										ArchivosNomina archNvoImportado = new ArchivosNomina((String) sess.getAttribute("nombreArchivo"),request);
										request.setAttribute("newMenu", session.getFuncionesDeMenu());
										request.setAttribute("MenuPrincipal", session.getStrMenu());
										request.setAttribute("Encabezado",CreaEncabezado("Pago Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
										request.setAttribute("etiquetaNameFile","Nombre del archivo: " + nombreArchivoOriginal);
										request.setAttribute("etiquetaNumTransmision","N&uacute;mero de Transmisi&oacute;n: Archivo no transmitido");
										request.setAttribute("tipoArchivo", arcRecuperado);
										request.setAttribute("cantidadDeRegistrosEnArchivo",registrosEnArchivo);
										request.setAttribute("ContenidoArchivo",archNvoImportado.reporteArchivo());
									}
									evalTemplate(IEnlace.REPORTE_NOMINA_TMPL, request, response);
								}
			}// fin de else de validacion OTP.
		} // fin se sessionValida
		else
		{
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	// metodo posCar que devuelve la posicion de un caracter en un String
	public int posCar(String cad, char car, int cant) {
		int result = 0, pos = 0;
		for (int i = 0; i < cant; i++) {
			result = cad.indexOf(car, pos);
			pos = result + 1;
		}
		return result;
	}

	/**
	 * Modificado por Miguel Cortes Arellano Fecha: 23/12/03 Proposito: Regresa
	 * el valor del HTML FORM SLECT campo para la opcion requerida 1 Significa
	 * que se obtengan todas las horas excepto la elegida por el usuario 2
	 * Significa que se selecciona la hora despues de la seleccionada por el
	 * usuario. Empieza codigo SINAPSIS
	 */
	private String obtenSeleccion(String HorarioDisp, String HorarioSele,
			int Opcion) {
		String textoLog = textoLog("obtenSeleccion");
		String tempSelect = "<OPTION VALUE=" + HorarioSele + ">" + HorarioSele
				+ "</OPTION><BR>";
		StringTokenizer splitSelect = new StringTokenizer(HorarioDisp, "|");
		String tempFinalSelect = "";
		String tempActualToken = "";
		boolean found = false;
		StringTokenizer indSelect;
		String tempToken = "";
		tempFinalSelect = "<SELECT NAME=HorarioPremierDialogo>";
		while (splitSelect.hasMoreTokens()) {
			tempActualToken = splitSelect.nextToken();
			indSelect = new StringTokenizer(tempActualToken, "BR");
			while (indSelect.hasMoreTokens()) {
				tempToken = indSelect.nextToken();
				tempToken = "<OPTION VALUE=" + tempToken + ">" + tempToken
						+ "</OPTION><BR>";
				EIGlobal.mensajePorTrace(textoLog + "EL VALOR DEL tempFinalSelectACTUAL ES>>" + tempFinalSelect + "<<", EIGlobal.NivelLog.DEBUG);
				if (Opcion == 1) {
					if (tempToken.equals(tempSelect))
						continue;
					else
						tempFinalSelect += tempToken;
				} else if (Opcion == 2) {
					if (found)
						tempFinalSelect += tempToken;
					if (tempToken.equals(tempSelect))
						found = true;
				}
			}
		}
		tempFinalSelect += "</SELECT>";
		return tempFinalSelect;
	}

	private String obtenSeleccionInd(String HorarioDisp, String HorarioSele,
			int Opcion) {
		textoLog("obtenSeleccion");
		String tempSelect = "<OPTION VALUE=" + HorarioSele + ">" + HorarioSele
				+ "</OPTION><BR>";
		StringTokenizer splitSelect = new StringTokenizer(HorarioDisp, "|");
		String tempFinalSelect = "";
		String tempActualToken = "";
		boolean found = false;
		StringTokenizer indSelect;
		String tempToken = "";
		tempFinalSelect = "<SELECT NAME=HorarioPremierDialogo>";
		while (splitSelect.hasMoreTokens()) {
			tempActualToken = splitSelect.nextToken();
			//indSelect = new StringTokenizer(tempActualToken, "BR");
			tempActualToken = "<OPTION VALUE=" + tempActualToken + ">" + tempActualToken
			+ "</OPTION><BR>";/*
			while (indSelect.hasMoreTokens()) {
				tempToken = indSelect.nextToken();
				tempToken = "<OPTION VALUE=" + tempToken + ">" + tempToken
						+ "</OPTION><BR>";
				EIGlobal.mensajePorTrace(textoLog + "EL VALOR DEL tempFinalSelectACTUAL ES>>" + tempFinalSelect + "<<", EIGlobal.NivelLog.DEBUG);
				if (Opcion == 1) {
					if (tempToken.equals(tempSelect))
						continue;
					else
						tempFinalSelect += tempToken;
				} else if (Opcion == 2) {
					if (found)
						tempFinalSelect += tempToken;
					if (tempToken.equals(tempSelect))
						found = true;
				}
			}*/
			tempFinalSelect += tempActualToken;
		}
		tempFinalSelect += "</SELECT>";
		return tempFinalSelect;
	}

	public void leerArchivo(HttpServletRequest request,
			HttpServletResponse response, String tipoArchivo)
			throws IOException, ServletException {
		String textoLog = textoLog("leerArchivo");
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String tmpTipoArchivo = "";
		String contenidoArch = "";
		String cuentaCargo = session.getCuentaCargo();
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		if (sess.getAttribute("facultadesN") != null)
			request
					.setAttribute("facultades", sess
							.getAttribute("facultadesN"));
		request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
		String systemHour = ObtenHora();
		request.setAttribute("horaSistema", systemHour);
		String archivoName = (String) sess.getAttribute("nombreArchivo");
		ArchivoRemoto archR = new ArchivoRemoto();
		String nombreArchivoOriginal = archivoName.substring(archivoName
				.lastIndexOf("/") + 1);
		archR.copiaLocalARemoto(nombreArchivoOriginal, "WEB");
		if (session.getRutaDescarga() != null)
			request.setAttribute("DescargaArchivo", session.getRutaDescarga());
		request.setAttribute("archivo_actual", archivoName
				.substring(archivoName.lastIndexOf("/") + 1));
		if (sesionvalida) {// servidor para facultad a Mancomunidad
			String nombreArchivoEmp = "", contenidoArchivoStr = "";
			String registrosEnArchivo = request
					.getParameter("cantidadDeRegistrosEnArchivo");
			nombreArchivoEmp = (String) sess.getAttribute("nombreArchivo");
			ArchivosNomina archivo1 = new ArchivosNomina(nombreArchivoEmp,
					request);
			EIGlobal.mensajePorTrace(textoLog + "valor de tipoArchivo >>" + tipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			tipoArchivo.trim();
			if (tipoArchivo.equals(""))
				tipoArchivo = "nuevo";
			EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE tipoArchivo >>" + tipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			tmpTipoArchivo = tipoArchivo.substring(0, 4);
			EIGlobal.mensajePorTrace(textoLog + "EL VALOR DE tmpTipoArchivo >>" + tmpTipoArchivo + "<<", EIGlobal.NivelLog.DEBUG);
			if (tmpTipoArchivo.equals("r")) {
				contenidoArchivoStr += archivo1.lecturaArchivoSalida(request);
				if (contenidoArchivoStr != null
						&& contenidoArchivoStr.length() > 1) {
					contenidoArch = contenidoArchivoStr.substring(0,
							contenidoArchivoStr.indexOf("|"));
				}
			} else {
				contenidoArch += archivo1.lecturaArchivo();
			}
			request.setAttribute("cantidadDeRegistrosEnArchivo",
					registrosEnArchivo);
			request.setAttribute("ContenidoArchivo", contenidoArch);
			if (sess.getAttribute("facultadesN") != null)
				request.setAttribute("facultades", sess
						.getAttribute("facultadesN"));
			request.setAttribute("tipoArchivo", tipoArchivo);
			request.setAttribute("textcuenta", cuentaCargo);
			session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
			evalTemplate(IEnlace.MTO_NOMINA_TMPL, request, response);
		} else {
			request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, request, response);
		}
	}

	public String comboPlazas() {
		String comboPlazas = "";
		comboPlazas += "\n <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas += "\n <input type=text size=25 name=PlazaBanxico value='MEXICO, DF' onFocus='blur();'>";
		comboPlazas += "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String colocaPlaza(String plazaBank) {
		String comboPlazas = "";
		comboPlazas += "\n <input type=hidden name=valorPlaza value='01001'>";
		comboPlazas += "\n <input type=text size=25 name=PlazaBanxico value='"
				+ plazaBank + "' onFocus='blur();'>";
		comboPlazas += "\n <a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a>";
		return comboPlazas;
	}

	public String formatoMoneda(String cantidad) {

		String textoLog = "";

		textoLog = textoLog("formatoMoneda");

		EIGlobal.mensajePorTrace(textoLog + "***Entrando a formatoMoneda ", EIGlobal.NivelLog.DEBUG);
		cantidad = cantidad.trim();
		String language = "la"; // ar
		String country = "MX"; // AF
		Locale local = new Locale(language, country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		String cantidadDecimal = "";
		String formatoFinal = "";
		String cantidadtmp = "";
		if (cantidad == null || cantidad.equals(""))
			cantidad = "0.0";
		if (cantidad.length() > 2) {
			try {
				cantidadtmp = cantidad.substring(0, cantidad.length() - 2);
				cantidadDecimal = cantidad.substring(cantidad.length() - 2);
				formato = nf.format(new Double(cantidadtmp).doubleValue());
				if (formato.substring(0,3).equals("MXN")){
					formato = formato.substring(3, formato.length() - 2);
				} else {
					formato = formato.substring(1, formato.length() - 2);
				}
				formatoFinal = "$ " + formato + cantidadDecimal;
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(textoLog + "***Error de formatoMoneda " + e.toString(), EIGlobal.NivelLog.ERROR);
				formatoFinal = "$ " + "0.00";
			}
		} else {
			try {
				if (cantidad.length() > 1) {
					cantidadDecimal = cantidad.substring(cantidad.length() - 2);
					formatoFinal = "$ " + "0." + cantidadDecimal;
				} else {
					cantidadDecimal = cantidad.substring(cantidad.length() - 1);
					formatoFinal = "$ " + "0.0" + cantidadDecimal;
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(textoLog + "***Error de formatoMoneda " + e.toString(), EIGlobal.NivelLog.ERROR);
				formatoFinal = "$ " + "0.00";
			}
		}
		if (formatoFinal == null)
			formato = "";
		return formatoFinal;
	}

	public String agregarPunto(String importe) {

		String textoLog = "";

		textoLog = textoLog("agregarPunto");
		String importeConPunto = "";
		importe = importe.trim();
		if (importe == null || importe.equals(""))
			importe = "0.0";
		if (importe.length() > 2) {
			try {
				importe = importe.trim();
				String enteros = importe.substring(0, importe.length() - 2);
				enteros = enteros.trim();
				String decimales = importe.substring(importe.length() - 2,
						importe.length());
				importe = enteros + " " + decimales;
				importeConPunto = importe.replace(' ', '.');
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(textoLog + "***Error de formatoMoneda " + e.toString(), EIGlobal.NivelLog.ERROR);
				importeConPunto = "0.00";
			}
		} else {
			try {
				if (importe.length() > 1) {
					importe = importe.trim();
					String decimales = importe.substring(importe.length() - 2,
							importe.length());
					importe = "0" + " " + decimales;
					importeConPunto = importe.replace(' ', '.');
				} else {
					importe = importe.trim();
					String decimales = importe.substring(importe.length() - 1,
							importe.length());
					importe = "0" + " " + "0" + decimales;
					importeConPunto = importe.replace(' ', '.');
				}
			} catch (NumberFormatException e) {
				EIGlobal.mensajePorTrace(textoLog + "***Error de formatoMoneda " + e.toString(), EIGlobal.NivelLog.ERROR);
				importeConPunto = "0.00";
			}
		}
		if (importeConPunto == null)
			importeConPunto = "";
		return importeConPunto;
	}

	/** Descarga de archivos desde el AppServer */
	protected void descargaArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		String nombreDelArchivo = "rada.zip";
		response.setHeader("Content-disposition", "attachment; filename="
				+ nombreDelArchivo);
		response.setContentType("application/x-gzip");
		BufferedInputStream entrada = null;
		BufferedOutputStream salida = null;
		InputStream arc = new FileInputStream(IEnlace.LOCAL_TMP_DIR + "/"
				+ nombreDelArchivo);
		ServletOutputStream out = response.getOutputStream();
		entrada = new BufferedInputStream(arc);
		salida = new BufferedOutputStream(out);
		byte[] buff = new byte[1024];
		int cantidad = 0;
		while ((cantidad = entrada.read(buff, 0, buff.length)) != -1)
			salida.write(buff, 0, cantidad);
		salida.close();
		entrada.close();
	}

	/**
	 * Despliega la pagina principal de Pagos
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void inicioPagos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		Vector diasNoHabiles;
		CalendarNomina myCalendarNomina = null;

		String textoLog = textoLog("inicioPagos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		//request.setAttribute("newMenu", session.getFuncionesDeMenu());
		//request.setAttribute("MenuPrincipal", session.getStrMenu());
		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute("facultades", sess.getAttribute("facultadesN"));
		request.setAttribute("ContenidoArchivo", "");
		request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE

		if(request.getAttribute("folio") != null)
			request.getSession().removeAttribute("folio");

		System.out.println("ESTOY EN: inicioPagos");


		sess.setAttribute("VALIDAR", "");
		sess.setAttribute("FOLIO", "");

		EIGlobal.mensajePorTrace(textoLog + "**********CSA - LIMPIANDO VARIABLES FOLIO Y VALIDAR****", EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace("NomPreImportOpciones Facultad " + session.getFacultad("INOMENVIPAG"), NivelLog.INFO);
		if (session.getFacultad("INOMENVIPAG")) {
			EIGlobal.mensajePorTrace(textoLog + "DAG Entro modulo=0 y con Facultad INOMENVIPAG  DESDE OPCIONES NOMINA HELG****", EIGlobal.NivelLog.DEBUG);
			//Se controla la Recarga para evitar duplicidad de registros
			// transferencia
			verificaArchivos(Global.DOWNLOAD_PATH + request.getSession().getId().toString()
					+ ".ses", true);
			if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null) {
				request.getSession().removeAttribute("Recarga_Ses_Multiples");
				EIGlobal.mensajePorTrace(textoLog + "Inicio Pagos - Borrando variable de sesion desde OpcionesNomina", EIGlobal.NivelLog.DEBUG);
			}
		}

		try {

 			myCalendarNomina= new CalendarNomina( );
 		}
 		catch(Exception e)
          {
 			despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina", "Importando Archivo", "s25800h", request, response );
    			EIGlobal.mensajePorTrace("***TarjetaPrepago.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
          }
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/*************** Nomina Concepto de Archvo */
		if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
			request.getSession().removeAttribute("conceptoEnArchivo");
		/*************** Nomina Concepto de Archvo */

		String VarFechaHoy = "";
		diasNoHabiles = myCalendarNomina.CargarDias();
		String strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
		Calendar fechaFin = myCalendarNomina.calculaFechaFin();
		VarFechaHoy = myCalendarNomina.armaArregloFechas();
		String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();


		myCalendarNomina = null;
	 	diasNoHabiles.clear();
	 	diasNoHabiles = null;

			StringBuffer datesrvrt = new StringBuffer ("");
		datesrvrt = new StringBuffer ("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvrt.append (ObtenAnio());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvrt.append (ObtenMes());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvrt.append (ObtenDia());
		datesrvrt.append ("\">");

			request.setAttribute( "VarFechaHoy", VarFechaHoy);
			request.setAttribute( "fechaFin", strFechaFin);
			request.setAttribute( "diasInhabiles", strInhabiles);
			request.setAttribute( "fecha2", ObtenFecha( true ) );
			request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
		    request.setAttribute( "ContenidoArchivo", "" );

			String NumberOfRecords = "";
			if(session.getNominaNumberOfRecords() != null)
			 {
				NumberOfRecords = session.getNominaNumberOfRecords();
			 }
			request.setAttribute( "totRegs", NumberOfRecords);
			estableceFacultades( request, response );

			EIGlobal.mensajePorTrace("DAG Entro modulo=7 y con Facultad INOMENVIPAG****", NivelLog.DEBUG);

            verificaArchivos(Global.DOWNLOAD_PATH+request.getSession().getId().toString()+".ses",true);
            if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
            {
            	request.getSession().removeAttribute("Recarga_Ses_Multiples");
            	EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
            	EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
            }
            	EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);

            EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
            request.setAttribute("Encabezado",CreaEncabezado("Pago de Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos","s25800h",request));//YHG NPRE
            request.getSession().removeAttribute("folio");
            sess.setAttribute("FOLIO", "");
            request.removeAttribute("ArchivoErr");
    		//evalTemplate(request.getContextPath()+"NomPredispersion?Modulo=7", request, response);
            evalTemplate( "/jsp/prepago/NomPredispersion.jsp", request, response );
		}

	protected void inicioPagosInd(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		Vector diasNoHabiles;
		CalendarNomina myCalendarNomina = null;

		String textoLog = textoLog("inicioPagosInd");

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		//request.setAttribute("newMenu", session.getFuncionesDeMenu());
		//request.setAttribute("MenuPrincipal", session.getStrMenu());
		if (sess.getAttribute("facultadesN") != null)
			request.setAttribute("facultades", sess.getAttribute("facultadesN"));
		request.setAttribute("ContenidoArchivo", "");
		//request.setAttribute("Encabezado", CreaEncabezado("Pago de N&oacute;mina", "Servicios &gt; N&oacute;mina &gt; Pagos", "s25800h", request));

		if(request.getAttribute("folio") != null)
			request.getSession().removeAttribute("folio");

		System.out.println("ESTOY EN: inicioPagosInd");

		sess.setAttribute("FOLIO", "");
		sess.setAttribute("VALIDARIND", "");
		EIGlobal.mensajePorTrace(textoLog + "**********LIMPIANDO VARIABLES FOLIO Y VALIDAR****", EIGlobal.NivelLog.DEBUG);
		if (session.getFacultad("INOMENVIPAG")) {
			EIGlobal.mensajePorTrace(textoLog + "DAG Entro modulo=0 y con Facultad INOMENVIPAG  DESDE OPCIONES NOMINA HELG****", EIGlobal.NivelLog.DEBUG);
			//Se controla la Recarga para evitar duplicidad de registros
			// transferencia
			//verificaArchivos("/tmp/" + request.getSession().getId().toString()+ ".ses", true);
			if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null) {
				request.getSession().removeAttribute("Recarga_Ses_Multiples");
				EIGlobal.mensajePorTrace(textoLog + "Inicio Pagos - Borrando variable de sesion desde OpcionesNomina", EIGlobal.NivelLog.DEBUG);
			}
		}
		//evalTemplate("/jsp/MantenimientoNomina.jsp", request, response);

		try {
 			myCalendarNomina= new CalendarNomina( );
 		}
 		catch(Exception e)
          {
 			despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina", "Importando Archivo", "s25800h", request, response );
    			EIGlobal.mensajePorTrace("***TarjetaPrepago.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
          }
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/*************** Nomina Concepto de Archvo */
		if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
			request.getSession().removeAttribute("conceptoEnArchivo");
		/*************** Nomina Concepto de Archvo */

		String VarFechaHoy = "";
		diasNoHabiles = myCalendarNomina.CargarDias();
		String strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
		Calendar fechaFin = myCalendarNomina.calculaFechaFin();
		VarFechaHoy = myCalendarNomina.armaArregloFechas();
		String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();


		myCalendarNomina = null;
	 	diasNoHabiles.clear();
	 	diasNoHabiles = null;

			StringBuffer datesrvrt = new StringBuffer ("");
		datesrvrt = new StringBuffer ("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvrt.append (ObtenAnio());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvrt.append (ObtenMes());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvrt.append (ObtenDia());
		datesrvrt.append ("\">");

			request.setAttribute( "VarFechaHoy", VarFechaHoy);
			request.setAttribute( "fechaFin", strFechaFin);
			request.setAttribute( "diasInhabiles", strInhabiles);
			request.setAttribute( "fecha2", ObtenFecha( true ) );
			request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
		    request.setAttribute( "ContenidoArchivo", "" );

			String NumberOfRecords = "";
			if(session.getNominaNumberOfRecords() != null)
			 {
				NumberOfRecords = session.getNominaNumberOfRecords();
			 }
			request.setAttribute( "totRegs", NumberOfRecords);
			estableceFacultades( request, response );

			  EIGlobal.mensajePorTrace("DAG Entro modulo=9 y con Facultad INOMENVIPAG****", NivelLog.DEBUG);
		/***********************.  Req. Q14709. limpieza de variables de Sesion. *****************************/
		//Se controla la Recarga para evitar duplicidad de registros transferencia
		  //verificaArchivos("/tmp/"+request.getSession().getId().toString()+".ses",true);
		  if(request.getSession().getAttribute("Recarga_Ses_Multiples")!=null)
		  {
			  request.getSession().removeAttribute("Recarga_Ses_Multiples");
			  EIGlobal.mensajePorTrace( "Inicio Nomina - Borrando variable de sesion.", EIGlobal.NivelLog.INFO);
			  EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples1"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
		  } else
			  EIGlobal.mensajePorTrace( "Inicio Nomina - La variable ya fue borrada", EIGlobal.NivelLog.INFO);

		  EIGlobal.mensajePorTrace( "****************HELG*************Recarga_Ses_Multiples2"+ request.getSession().getAttribute("Recarga_Ses_Multiples"), EIGlobal.NivelLog.INFO);
		/**********************Termina limpieza de variables de Sesion**********************/
		  request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
		  request.setAttribute("Bitfechas",datesrvrt);
		  evalTemplate( "/jsp/prepago/NomPredispersionIndividual.jsp", request, response );

		//request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; N&oacute;mina Prepago &gt; Pagos &gt; Pago Individual","s25800h",request));
		//evalTemplate(request.getContextPath()+"NomPredispersion?Modulo=9", request, response);
	}

	/**
	 * muestra la pantalla de inicio de consultas de nomina
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void inicioConsulta(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		Vector diasNoHabiles;
		CalendarNomina myCalendarNomina = null;

		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Bitfechas", datesrvr.toString());
		request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
		if (sess.getAttribute("facultadesN") != null)
			request	.setAttribute("facultades", sess.getAttribute("facultadesN"));
		request.setAttribute("textcuenta", "");
		request.setAttribute("ContenidoArchivo", "");

		if(request.getAttribute("folio") != null)
			request.getSession().removeAttribute("folio");

		System.out.println("ESTOY EN: inicioConsulta");

		sess.setAttribute("FOLIO", "");

		//evalTemplate(IEnlace.NOMINADATOS_TMPL, request, response);

		try {
 			myCalendarNomina= new CalendarNomina( );
 		}
 		catch(Exception e)
          {
 			despliegaPaginaError( IEnlace.MSG_PAG_NO_DISP ,"Pago de N&oacute;mina", "Importando Archivo", "s25800h", request, response );
    			EIGlobal.mensajePorTrace("***TarjetaPrepago.class &Error de excepcion "+e, EIGlobal.NivelLog.INFO);
          }
 		request.setAttribute("Fecha", ObtenFecha());
 		request.setAttribute("ContUser",ObtenContUser(request));
 		request.setAttribute("newMenu", session.getFuncionesDeMenu());
 		request.setAttribute("MenuPrincipal", session.getStrMenu());

		/*************** Nomina Concepto de Archvo */
		if(request.getSession().getAttribute("conceptoEnArchivo")!=null)
			request.getSession().removeAttribute("conceptoEnArchivo");
		/*************** Nomina Concepto de Archvo */

		String VarFechaHoy = "";
		diasNoHabiles = myCalendarNomina.CargarDias();
		String strInhabiles = myCalendarNomina.armaDiasInhabilesJS();
		Calendar fechaFin = myCalendarNomina.calculaFechaFin();
		VarFechaHoy = myCalendarNomina.armaArregloFechas();
		String strFechaFin = new Integer(fechaFin.get(fechaFin.YEAR)).toString() + "/" + new Integer(fechaFin.get(fechaFin.MONTH)).toString() + "/" + new Integer(fechaFin.get(fechaFin.DAY_OF_MONTH)).toString();


		myCalendarNomina = null;
	 	diasNoHabiles.clear();
	 	diasNoHabiles = null;

			StringBuffer datesrvrt = new StringBuffer ("");
		datesrvrt = new StringBuffer ("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvrt.append (ObtenAnio());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvrt.append (ObtenMes());
		datesrvrt.append ("\">");
        datesrvrt.append ("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvrt.append (ObtenDia());
		datesrvrt.append ("\">");

			request.setAttribute( "VarFechaHoy", VarFechaHoy);
			request.setAttribute( "fechaFin", strFechaFin);
			request.setAttribute( "diasInhabiles", strInhabiles);
			request.setAttribute( "fecha2", ObtenFecha( true ) );
			request.setAttribute( "fechaHoy", ObtenFecha(false) + " - 06" );
		    request.setAttribute( "ContenidoArchivo", "" );

			String NumberOfRecords = "";
			if(session.getNominaNumberOfRecords() != null)
			 {
				NumberOfRecords = session.getNominaNumberOfRecords();
			 }
			request.setAttribute( "totRegs", NumberOfRecords);
			estableceFacultades( request, response );

		  sess.removeAttribute("tipoBusqueda");
		  request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
		  request.setAttribute("Bitfechas",datesrvrt);
		  evalTemplate( "/jsp/prepago/NomPreConsultadispersion.jsp", request, response );
		//evalTemplate(request.getContextPath()+"NomPredispersion?Modulo=8", request, response);
	}

	/**
	 * Realiza la consulta de archivos de nomina
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected void consultaArchivos(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {

		String textoLog = "";

		textoLog = textoLog("consultaArchivos");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer datesrvr = new StringBuffer("");
		datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
		datesrvr.append(ObtenAnio());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
		datesrvr.append(ObtenMes());
		datesrvr.append("\">");
		datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
		datesrvr.append(ObtenDia());
		datesrvr.append("\">");
		String nombre_arch_salida = "";
		EIGlobal.mensajePorTrace(textoLog + "Entro a consula de nomina****", EIGlobal.NivelLog.DEBUG);
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		String nueva_ruta = Global.DIRECTORIO_REMOTO_INTERNET;
		ServicioTux tuxGlobal = new ServicioTux();
		Hashtable htResult = null;
		String tramaEntrada = generarTramaConsultaArchivo(request, response);
		 EIGlobal.mensajePorTrace("Trama de Entrada :" + tramaEntrada, NivelLog.DEBUG);
		try {
			EIGlobal.mensajePorTrace(textoLog + "tramaEntrada >>" + tramaEntrada + "<<", EIGlobal.NivelLog.INFO);
			htResult = tuxGlobal.web_red(tramaEntrada);
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
		} catch (Exception e) {
		}
		if (null == htResult) {
			EIGlobal.mensajePorTrace(textoLog + "null ret despues de webred", EIGlobal.NivelLog.ERROR);
			 EIGlobal.mensajePorTrace("Error para generar la trama de salida", NivelLog.INFO);
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de N&oacute;mina","Servicios &gt; Consulta de N&oacute;mina", request,response);
			return;
		}
		String tramaSalida = (String) htResult.get("BUFFER");
		// Simulacion de la trama de regreso :
		// NOMI0001@Mensaje de error@
		// String tramaSalida = "NOMI0000@/tmp/1001851.conNom@";
		// "TCT_0010 1397Servicio de bitacora no se encuentra disponible";
		EIGlobal.mensajePorTrace(textoLog + "El servicio consulta_nomi regreso:1 " + tramaSalida + "***", EIGlobal.NivelLog.DEBUG);
		String elMensaje = "";
		String estatus_envio = "";
		String opertct="";
		 EIGlobal.mensajePorTrace("Trama de salida :" + tramaSalida, NivelLog.DEBUG);
		if (tramaSalida == null)
			tramaSalida = "";
		if (tramaSalida.indexOf("NOMI") >= 0) {
			elMensaje = tramaSalida.substring((posCar(tramaSalida, '@', 1)) + 1, posCar(tramaSalida, '@',2));
			EIGlobal.mensajePorTrace(textoLog + "elMensaje : >" + elMensaje + "<", EIGlobal.NivelLog.DEBUG);
			estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
			EIGlobal.mensajePorTrace(textoLog + "estatus_envio : >" + estatus_envio + "<", EIGlobal.NivelLog.DEBUG);
		} else {
			elMensaje = "";
			estatus_envio = "";
		}
		sess.setAttribute("tramaEntradaConsul", tramaEntrada);
		int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

		if(sess.getAttribute("tipoBusqueda").equals("IE"))
		{
			sess.setAttribute("tipoBusqueda", "PI");
			opertct=NomPreUtil.ES_NP_PAGOS_CONSULTA_CONSULTA_IE_OPER;
		}
		else
		{
			sess.setAttribute("tipoBusqueda", "IE");
			opertct=NomPreUtil.ES_NP_PAGOS_CONSULTA_CONSULTA_PI_OPER;
		}
		//bitacora
		String CuentaCargo=" ";
		String fchaAplicacion="";
		String importe="";
		String num_secuencia="";
		String num_empleado="";
		String num_tarjeta=" ";

		CuentaCargo = (String) getFormParameter(request, "textcuenta");
		fchaAplicacion = (String) getFormParameter(request, "fecha2");
		importe = (String) getFormParameter(request, "Importe");
		num_secuencia = (String) getFormParameter(request, "secuencia");
		num_empleado = (String) getFormParameter(request, "empleado");
		num_tarjeta = (String) getFormParameter(request, "tarjeta");

		if(CuentaCargo==null)
			CuentaCargo="";

		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		double impBita=0.0;
		String ctaOriBit=" ";
		String ctaDesBit=" ";
		String codErrBit=" ";

		BitaTransacBean bt = new BitaTransacBean();
		bt.setNumBit(BitaConstants.ES_NP_PAGOS_CONSULTA_CONSULTA);

		if(CuentaCargo!=null&&!CuentaCargo.trim().equals("")){
			bt.setCctaOrig(CuentaCargo);
			ctaOriBit=CuentaCargo;
		}else{ctaOriBit=" ";}

		if(importe!=null && !importe.trim().equals("")){
			impBita=Double.parseDouble(importe);
			bt.setImporte(impBita);
		}

		if(num_secuencia!=null && !num_secuencia.trim().equals(""))
			bt.setReferencia(Long.parseLong(num_secuencia));

		if(num_empleado!=null && !num_empleado.trim().equals(""))
			bt.setBeneficiario(num_empleado);

		if(num_tarjeta!=null && !num_tarjeta.trim().equals("")){
			bt.setCctaDest(num_tarjeta);
			ctaDesBit=num_tarjeta;
		}else{ctaDesBit=" ";}

		if(estatus_envio!=null && !estatus_envio.trim().equals("")){
			codErrBit=estatus_envio;
		}
		if(fchaAplicacion!=null && !fchaAplicacion.trim().equals(""))
			try {
				bt.setFechaAplicacion(sdf.parse(fchaAplicacion));
			} catch (ParseException e) {}



		NomPreUtil.bitacoriza(request,"NOMI", codErrBit, bt);



		if (estatus_envio.equals("NOMI0000"))
		{
			String nombreArchivo = tramaSalida.substring(tramaSalida.lastIndexOf("/") + 1, tramaSalida.lastIndexOf("@"));
			try {
				String tipoCopia = "2"; // de /t/tuxedo... a /tmp
				ArchivoRemoto archR = new ArchivoRemoto();
				if (!archR.copia(nombreArchivo, nueva_ruta, tipoCopia))
					EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
			} catch (Exception ioeSua) {
				EIGlobal.mensajePorTrace(textoLog + "Excepcion %envio() >> " + ioeSua.getMessage() + " <<", EIGlobal.NivelLog.ERROR);
			}
			nombre_arch_salida = IEnlace.DOWNLOAD_PATH + nombreArchivo;
			EIGlobal.mensajePorTrace(textoLog + "nombre_arch_salida: " + nombre_arch_salida + "***", EIGlobal.NivelLog.DEBUG);
			// copia el archivo en la ruta tmp
			// nombre_arch_salida = "/tmp/" + nombre_arch_salida;
			ArchivosNomina archSalida = new ArchivosNomina(nombre_arch_salida,request);
			String contenidoArch = null;
			if(sess.getAttribute("tipoBusqueda").equals("PI"))
			{
				contenidoArch = archSalida.lecturaArchivoSalidaConsul();
			}
			else
			{
				contenidoArch = archSalida.lecturaArchivoSalidaConsulInd();
			}
			if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0
					|| contenidoArch.equals("")
					|| contenidoArch.indexOf("Error ") >= 0
					|| contenidoArch.startsWith("Error"))
			{
				request.setAttribute("Bitfechas", datesrvr.toString());
				request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
				if (contenidoArch.indexOf("NO EXISTEN DATOS") >= 0)
					elMensaje = "NO EXISTEN DATOS PARA LA CONSULTA.";
				else
					elMensaje = "Error en la consulta, Intente m&aacute;s tarde por favor.";
				String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
				request.setAttribute("infoUser", infoUser);
				if (sess.getAttribute("facultadesN") != null)
					request.setAttribute("facultades", sess.getAttribute("facultadesN"));
				request.setAttribute("textcuenta", "");
				request.setAttribute("ContenidoArchivo", "");
				 EIGlobal.mensajePorTrace("Sale :" + sess.getAttribute("tipoBusqueda").toString().trim(), NivelLog.DEBUG);
				evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
				//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
			} else {
				String arch_exportar = "";
				arch_exportar = session.getUserID8() + "Nom.doc";
				try {
					String tipoCopia = "2"; // de /t/tuxedo... a /tmp
					ArchivoRemoto archR = new ArchivoRemoto();
					//copiaOtroPathRemoto(String Archivo, String Destino, String pathDestino)
					//JGAL - Copia a ruta de download
					if (!archR.copiaOtroPathRemoto(nombreArchivo, Global.DIRECTORIO_REMOTO_WEB+ "/" + arch_exportar, nueva_ruta))
						EIGlobal.mensajePorTrace("No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
				} catch (Exception ioeSua) {
					EIGlobal.mensajePorTrace(textoLog + "Excepcion %envio() >> " + ioeSua.getMessage() + " <<", EIGlobal.NivelLog.ERROR);
				}
				request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
				request.setAttribute("ContenidoArchivo", contenidoArch);
				request.setAttribute("textcuenta", "");
				request.setAttribute("nombre_arch_salida", nombre_arch_salida);
				String botonestmp = "<br>"
						+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
						+ "<td><A href = javascript:js_cancelar(); border = 0><img src = /gifs/EnlaceMig/gbo25190.gif border=0 alt=Cancelar></a></td>"
				//		+ "<td><A href = javascript:scrImpresion(); border = 0><img src = /gifs/EnlaceMig/gbo25240.gif border=0 alt=Imprimir></a></td>"
						+ "<td><A href = \"/Download/"
						+ arch_exportar
						+ "\"><img border=0 name=Exportar src=\"/gifs/EnlaceMig/gbo25230.gif\" alt=Exportar></a></td>";
						if(sess.getAttribute("tipoBusqueda").toString().equals("PI"))
						{
							String correo = "<td><A href = javascript:js_enviarCorreo(); border = 0><img src = /gifs/EnlaceMig/gbo25245.gif border=0 alt='Enviar detalle'></a></td>" ;
							botonestmp = botonestmp + correo;
						}
						botonestmp = botonestmp + "<td><A href = \"NomPredispersion?Modulo=8&"+BitaConstants.FLUJO+"=" + BitaConstants.ES_NP_PAGOS_CONSULTA+"\" border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
						+ "<td></td></tr> </table>";
				request.setAttribute("botones", botonestmp);
				/*try
				{
					EIGlobal.mensajePorTrace("ENTRA BITACORIZACION TCT Nomina Prepago Consulta Dispersion",EIGlobal.NivelLog.INFO);
					String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBita,opertct,ctaDesBit,0," ");
				}catch(Exception e){
					EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
				}*/
				evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
				//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
			}
		} else {// del Transaccion Exitosa
			if (elMensaje.equals(""))
				elMensaje = "Servicio no disponible por el momento.";
			request.setAttribute("Bitfechas", datesrvr.toString());
			request.setAttribute("Encabezado", CreaEncabezado("Consulta Tarjeta de Pagos Santander ","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Consulta de N&oacute;mina", "s25800conh",request));//YHG NPRE
			String infoUser = "cuadroDialogo (\"" + elMensaje + "\",1)";
			request.setAttribute("infoUser", infoUser);
			if (sess.getAttribute("facultadesN") != null)
				request.setAttribute("facultades", sess.getAttribute("facultadesN"));
			request.setAttribute("textcuenta", "");
			request.setAttribute("ContenidoArchivo", "");
			evalTemplate(IEnlace.TJT_NOMINADATOSDET_TML, request, response);
			//evalTemplate(IEnlace.TJT_NOMINADATOS_TML, request, response);
		}
	}

	/**
	 * Genera la trama para la consulta de archivos de nomina a partir de los
	 * parametros seleccionados. Trama:
	 *
	 * <pre>
	 * 1EWEB|&lt;b&gt;empleado&lt;/b&gt;|IN08|&lt;b&gt;no_contrato&lt;/b&gt;|&lt;b&gt;no_empleado&lt;/b&gt;|&lt;b&gt;perfil&lt;/b&gt;|&lt;b&gt;cadena_busqueda&lt;/b&gt;@&lt;b&gt;ruta_archivo_respuesta&lt;/b&gt;@
	 * </pre>
	 *
	 * @since version 1.2 RMM Refactored
	 */
	protected String generarTramaConsultaArchivo(HttpServletRequest request,
			HttpServletResponse response) {

		String textoLog = "";

		textoLog = textoLog("generarTramaConsultaArchivo");

		//BaseResource session = (BaseResource) request.getSession().getAttribute("session");
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		String sessionId = (String) session.getUserID8();
		EIGlobal.mensajePorTrace(textoLog + "sessionId [" + sessionId + "]", EIGlobal.NivelLog.DEBUG);
		 EIGlobal.mensajePorTrace("TipoBusqueda" + sess.getAttribute("tipoBusqueda").toString(), NivelLog.INFO) ;
		String contrato = session.getContractNumber();
		String no_empleado = (String)getFormParameter(request, "empleado");
		String no_tarjeta = (String)getFormParameter(request, "tarjeta");
		StringBuffer retValue = new StringBuffer("1EWEB|");
		if(sess.getAttribute("tipoBusqueda").equals("IE"))
		{
			retValue.append(sessionId).append("|CNIE|").append(contrato).append('|').append(sessionId).append('|').append(session.getUserProfile()).append('|');
		}
		else
		{
			retValue.append(sessionId).append("|CPNI|").append(contrato).append('|').append(sessionId).append('|').append(session.getUserProfile()).append('|');
		}
		String CuentaCargo = (String) getFormParameter(request, "textcuenta");
		String fchaRecepcion = (String) getFormParameter(request, "fecha1"); // formatear la fecha enviar sin diagonales ddmmaaaa
		String fchaAplicacion = (String) getFormParameter(request, "fecha2");
		String importe = (String) getFormParameter(request, "Importe");
		String num_secuencia = (String) getFormParameter(request, "secuencia");
		//String num_empleado = (String) getFormParameter(request, "empleado");
		//String num_tarjeta = (String) getFormParameter(request, "tarjeta");
		String ChkRecibido = (String) getFormParameter(request, "ChkRecibido");
		String ChkCancelado = (String) getFormParameter(request, "ChkCancelado");
		String ChkEnviado = (String) getFormParameter(request, "ChkEnviado");
		String ChkPagado = (String) getFormParameter(request, "ChkPagado");
		String ChkCargado = (String) getFormParameter(request, "ChkCargado");

		if(sess.getAttribute("tipoBusqueda").equals("IE"))
		{
			retValue.append("num_cuenta2='").append(contrato).append("'");
			if (null != CuentaCargo && !CuentaCargo.trim().equals(""))
			{
				retValue.append(" and cta_cargo='");
				if (10 < CuentaCargo.length()) {
					retValue.append(CuentaCargo.substring(0, 11));
				} else {
					retValue.append(CuentaCargo.trim());
				}
				retValue.append("'");
			}
			if (null != fchaRecepcion && !fchaRecepcion.trim().equals(""))
			{
				fchaRecepcion = fchaRecepcion.trim();
				retValue.append(" and fch_recep=to_date('");
				if (10 >= fchaRecepcion.length())
				{
					fchaRecepcion = fchaRecepcion.substring(0, 2)
							+ fchaRecepcion.substring(3, 5)
							+ fchaRecepcion.substring(6);
				}
				retValue.append(fchaRecepcion.trim()).append("','ddmmyyyy')");
			}
			if (null != fchaAplicacion && !fchaAplicacion.trim().equals(""))
			{
				fchaAplicacion = fchaAplicacion.trim();
				retValue.append(" and fch_aplic=to_date('");
				if (10 >= fchaAplicacion.length())
				{
					fchaAplicacion = fchaAplicacion.substring(0, 2)
							+ fchaAplicacion.substring(3, 5)
							+ fchaAplicacion.substring(6);
				}
				retValue.append(fchaAplicacion.trim()).append("','ddmmyyyy')");
			}
			boolean filterByStatus = false;
			if (null != ChkRecibido && ChkRecibido.startsWith("R"))
			{
				filterByStatus = true;
				retValue.append(" and estatus in ('").append(ChkRecibido.trim())
						.append("'");
			}
			if (null != ChkCancelado && ChkCancelado.startsWith("C"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkCancelado.trim()).append("'");
			}
			if (null != ChkEnviado && ChkEnviado.startsWith("E"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkEnviado.trim()).append("'");
			}
			if (null != ChkPagado && ChkPagado.startsWith("P"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkPagado.trim()).append("'");
			}
			if (null != ChkCargado && ChkCargado.startsWith("A"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkCargado.trim()).append("'");
			}
			if (filterByStatus) {
				retValue.append(")");
			}
			if (null != importe && !importe.trim().equals(""))
			{
				retValue.append(" and importe_aplic='").append(importe.trim())
						.append("'");
			}
			if (null != num_secuencia && !num_secuencia.trim().equals(""))
			{
				retValue.append(" and sec_pago='").append(num_secuencia.trim())
						.append("'");
			}
		}
		else
		{
			retValue.append("C.NUM_CUENTA2='").append(contrato.trim()).append("'");
			if (null != CuentaCargo && !CuentaCargo.trim().equals(""))
			{
				retValue.append(" and CUENTA_CARGO='");
				if (10 < CuentaCargo.length()) {
					retValue.append(CuentaCargo.substring(0, 11));
				} else {
					retValue.append(CuentaCargo.trim());
				}
				retValue.append("'");
			}
			if (null != fchaRecepcion && !fchaRecepcion.trim().equals(""))
			{
				fchaRecepcion = fchaRecepcion.trim();
				retValue.append(" and FCH_RECEPCION=to_date('");
				if (10 >= fchaRecepcion.length())
				{
					fchaRecepcion = fchaRecepcion.substring(0, 2)
							+ fchaRecepcion.substring(3, 5)
							+ fchaRecepcion.substring(6);
				}
				retValue.append(fchaRecepcion.trim()).append("','ddmmyyyy')");
			}
			if (null != fchaAplicacion && !fchaAplicacion.trim().equals(""))
			{
				fchaAplicacion = fchaAplicacion.trim();
				retValue.append(" and FCH_CARGO=to_date('");
				if (10 >= fchaAplicacion.length())
				{
					fchaAplicacion = fchaAplicacion.substring(0, 2)
							+ fchaAplicacion.substring(3, 5)
							+ fchaAplicacion.substring(6);
				}
				retValue.append(fchaAplicacion.trim()).append("','ddmmyyyy')");
			}
			if (null != no_tarjeta && !no_tarjeta.trim().equals(""))
			{
				retValue.append(" and NUM_TARJETA='").append(no_tarjeta.trim())
						.append("'");
			}
			if (null != no_empleado && !no_empleado.trim().equals(""))
			{
				retValue.append(" and NUM_EMPLEADO='").append(no_empleado.trim())
						.append("'");
			}
			boolean filterByStatus = false;
			if (null != ChkRecibido && ChkRecibido.startsWith("R"))
			{
				filterByStatus = true;
				retValue.append(" and D.estatus in ('").append(ChkRecibido.trim())
						.append("'");
			}
			if (null != ChkCancelado && ChkCancelado.startsWith("C"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and D.estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkCancelado.trim()).append("'");
			}
			if (null != ChkEnviado && ChkEnviado.startsWith("E"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and D.estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkEnviado.trim()).append("'");
			}
			if (null != ChkPagado && ChkPagado.startsWith("P"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and D.estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkPagado.trim()).append("'");
			}
			if (null != ChkCargado && ChkCargado.startsWith("A"))
			{
				if (!filterByStatus)
				{
					retValue.append(" and D.estatus in (");
					filterByStatus = true;
				} else {
					retValue.append(",");
				}
				retValue.append("'").append(ChkCargado.trim()).append("'");
			}
			if (filterByStatus) {
				retValue.append(")");
			}
			if (null != importe && !importe.trim().equals(""))
			{
				retValue.append(" and IMPORTE='").append(importe.trim())
						.append("'");
			}
			if (null != num_secuencia && !num_secuencia.trim().equals(""))
			{
				retValue.append(" and C.SECUENCIA='").append(num_secuencia.trim())
						.append("'");
			}
		}
		retValue.append('@').append(new java.io.File(Global.DIRECTORIO_REMOTO_INTERNET, request.getSession().getId()).getAbsolutePath()).append('@');
		return retValue.toString();
	}

	// Metodo para verificar existencia del Archivo de Sesion
	public boolean verificaArchivos(String arc, boolean eliminar)
			throws ServletException, java.io.IOException {
		String textoLog =textoLog("verificaArchivos");
		EIGlobal.mensajePorTrace(textoLog + "Verificando si el archivo existe", EIGlobal.NivelLog.DEBUG);
		File archivocomp = new File(arc);
		if (archivocomp.exists()) {
			EIGlobal.mensajePorTrace(textoLog + "El archivo si existe.", EIGlobal.NivelLog.DEBUG);
			if (eliminar) {
				archivocomp.delete();
				EIGlobal.mensajePorTrace(textoLog + "El archivo se elimina.", EIGlobal.NivelLog.DEBUG);
			}
			return true;
		}
		EIGlobal
				.mensajePorTrace("El archivo no existe.", EIGlobal.NivelLog.DEBUG);
		return false;
	}

	public void guardaParametrosEnSession(HttpServletRequest request) {

		String textoLog = "";

		textoLog = textoLog("guardaParametrosEnSession");

		EIGlobal.mensajePorTrace(textoLog + "Dentro de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla", request.getServletPath());

		HttpSession session = request.getSession(false);
		session.removeAttribute("bitacora");

		if (session != null) {
			Map<Object,Object> tmp = new HashMap<Object,Object>();

			EIGlobal.mensajePorTrace(textoLog + "valida-> " + getFormParameter(request, "valida"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "operacion-> " + getFormParameter(request, "operacion"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "registro-> " + getFormParameter(request, "registro"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "tipoArchivo-> " + getFormParameter(request, "tipoArchivo"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "statusDuplicado-> " + getFormParameter(request, "statusDuplicado"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "statushrc-> " + getFormParameter(request, "statushrc"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "encFechApli-> " + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "horario_seleccionado-> " + getFormParameter(request, "horario_seleccionado"), EIGlobal.NivelLog.DEBUG);
			String value;
			value = (String) getFormParameter(request, "valida");
			if (value != null && !value.equals(null)) {
				tmp.put("valida", getFormParameter(request, "valida"));
			}

			tmp.put("operacion", getFormParameter(request, "operacion"));
			tmp.put("registro", getFormParameter(request, "registro"));
			tmp.put("tipoArchivo", getFormParameter(request, "tipoArchivo"));
			tmp.put("statusDuplicado", getFormParameter(request, "statusDuplicado"));
			tmp.put("statushrc", getFormParameter(request, "statushrc"));
			tmp.put("encFechApli", getFormParameter(request, "encFechApli"));
			tmp.put("horario_seleccionado", getFormParameter(request, "horario_seleccionado"));
			tmp.put("folio", getFormParameter(request, "folio"));

			session.setAttribute("parametros", tmp);

			tmp = new HashMap<Object,Object>();
			Enumeration enumer = request.getAttributeNames();
			while (enumer.hasMoreElements()) {
				String name = (String) enumer.nextElement();
				tmp.put(name, request.getAttribute(name));
			}
			session.setAttribute("atributos", tmp);
		}
		EIGlobal.mensajePorTrace(textoLog + "Saliendo de guardaParametrosEnSession", EIGlobal.NivelLog.DEBUG);
	}

	/** ************* Nomina Concepto de Archvo */
	boolean actualizaConceptoEnArchivo(String nombreArchivo, String Concepto) {

		String textoLog ="";

		textoLog = textoLog("actualizaConceptoEnArchivo");
		String line = "";
		BufferedReader arc;
		BufferedWriter arcWrite;
		String nombreArchivoEscritura = nombreArchivo + ".nom";

		/*ESC HD1000000085131 - RFC:49313 - Se valida que el concepto no este nulo en el archivo*/
		if(Concepto == null || "null".equals(Concepto)){
			EIGlobal.mensajePorTrace("Error al agregar el concepto al archivo.", EIGlobal.NivelLog.ERROR);
			return false;
		}


		try {
			// Se le quita el path al archivo para operar con el, en el
			// directorio temporal del app
			EIGlobal.mensajePorTrace(textoLog + "El archivo origen: " + nombreArchivo, EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(textoLog + "El archivo destino: " + nombreArchivoEscritura, EIGlobal.NivelLog.DEBUG);

			arc = new BufferedReader(new FileReader(nombreArchivo));
			arcWrite = new BufferedWriter(
					new FileWriter(nombreArchivoEscritura));

			while ((line = arc.readLine()) != null) {
				if (line.length() > 126)
					line += Concepto;
				arcWrite.write(line + "\n");
			}

			File a = new File(nombreArchivo);
			File b = new File(nombreArchivoEscritura);
			a.delete();
			b.renameTo(new File(nombreArchivo));

			try {
				arc.close();
				arcWrite.close();
			} catch (IOException d) {
			}
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("Error al agregar el concepto al archivo.", EIGlobal.NivelLog.ERROR);
			return false;
		}
		return true;
	}

	/** ************* Nomina Concepto de Archvo */

	// Servicio Tuxedo para generar folio
	public String generaFolio(String usuario, String perfil, String contrato,
			String servicio, String registros) {
		String result = "";
		String codError = "";
		String folioArchivo = "";
		String tramaParaFolio = usuario + "|" + perfil + "|" + contrato + "|"
				+ servicio + "|" + registros + "|";

		ServicioTux tuxGlobal = new ServicioTux();
		try {
			Hashtable hs = tuxGlobal.alta_folio(tramaParaFolio);
			result = (String) hs.get("BUFFER") + "";
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (result == null || result.equals("null"))
			result = "ERROR            Error en el servicio.";

		codError = result.substring(0, result.indexOf("|"));

		if (codError.trim().equals("TRSV0000")) {
			folioArchivo = result.substring(result.indexOf("|") + 1, result
					.length());
			folioArchivo = folioArchivo.trim();
			if (folioArchivo.length() < 12) {
				do {
					folioArchivo = "0" + folioArchivo;
				} while (folioArchivo.length() < 12);
			}
		}
		return folioArchivo;
	}

	public String envioDatos(String usuario, String perfil, String contrato,
			String folio, String servicio, String ruta, String trama) {

		String textoLog = "";

		textoLog = textoLog("envioDatos");

		String result = "";
		String codError = "";
		String mensajeErrorEnviar = "";
		String tramaParaEnviar = usuario + "|" + perfil + "|" + contrato + "|"
				+ folio + "|" + servicio + "|" + ruta + "_preprocesar" + "|"
				+ ruta + "|" + trama;

		ServicioTux tuxGlobal = new ServicioTux();
		try {
			Hashtable hs = tuxGlobal.envia_datos(tramaParaEnviar);
			result = (String) hs.get("BUFFER") + "";
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null || result.equals("null"))
			result = "ERROR            Error en el servicio.";
		EIGlobal.mensajePorTrace(textoLog + "CONTROLADOR DE REGISTROS  : Trama salida: " + result, EIGlobal.NivelLog.ERROR);
		codError = result.substring(0, result.indexOf("|"));
		mensajeErrorEnviar = result.substring(result.indexOf("|") + 1, result.length());
		return mensajeErrorEnviar;
	}

	public String realizaDispersion(String usuario, String perfil,
			String contrato, String folio, String servicio) {
		String textoLog = textoLog("realizaDispersion");
		String result = "";
		String codError = "";
		String mensajeErrorDispersor = "";
		String tramaParaDispersor = usuario + "|" + perfil + "|" + contrato
				+ "|" + folio + "|" + servicio + "|";

		ServicioTux tuxGlobal = new ServicioTux();
		try {
			Hashtable hs = tuxGlobal.inicia_proceso(tramaParaDispersor);
			result = (String) hs.get("BUFFER");
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == null || result.equals("null"))
			result = "ERROR            Error en el servicio.";
		EIGlobal.mensajePorTrace(textoLog + "Dispersor de registros: Trama salida: " + result, EIGlobal.NivelLog.ERROR);
		codError = result.substring(0, result.indexOf("|"));
		if (!codError.trim().equals("TRSV0000")) {
			EIGlobal.mensajePorTrace(textoLog + "No se realizo la dispersion de registros", EIGlobal.NivelLog.ERROR);
		} else {
			EIGlobal.mensajePorTrace(textoLog + "Se realizo la dispersion de registros: " + mensajeErrorDispersor, EIGlobal.NivelLog.ERROR);
		}
		mensajeErrorDispersor = result.substring(result.indexOf("|") + 1,
				result.length());
		return mensajeErrorDispersor;
	}

	public String ejecutaServicio(String trama) {

		String textoLog = "";

		textoLog = textoLog("ejecutaServicio");
		Hashtable htResult = null;

		ServicioTux tuxGlobal = new ServicioTux();
		String tramaSalida = "";
		try {
			htResult = tuxGlobal.web_red(trama);
			tramaSalida = (String) htResult.get("BUFFER");
		} catch (java.rmi.RemoteException re) {
			re.printStackTrace();
		} catch (Exception e) {
			EIGlobal.mensajePorTrace("ERROR NomPreImportOpciones -> ejecutaServicio " + e.getMessage(), EIGlobal.NivelLog.ERROR);
		}

		EIGlobal.mensajePorTrace(textoLog + "tramaSalida: [" + tramaSalida + "]", EIGlobal.NivelLog.ERROR);
		return tramaSalida;
	}

	public String generaTablaTotales(String cuenta, String descripcion, String fchAplicacion, String importe, String registros){
		String bgcolor = "textabdatobs";

		StringBuffer tablaTotales = new StringBuffer();
		tablaTotales.append("<tr><td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		tablaTotales.append("<tr> ");
		tablaTotales.append("<td class=\"textabref\">Pago de n&oacute;mina</td>");
		tablaTotales.append("</tr>");
		tablaTotales.append("</table>");
		tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
		tablaTotales.append("<tr> ");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Cuenta de cargo</td>");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Descipci&oacute;n</td>");
		tablaTotales.append("<td width=\"70\" class=\"tittabdat\" align=\"center\">Fecha de aplicaci&oacute;n</td>");
		tablaTotales.append("<td width=\"80\" class=\"tittabdat\" align=\"center\">Importe total</td>");
		tablaTotales.append("<td width=\"100\" class=\"tittabdat\" align=\"center\">N&uacute;mero de registros</td>");
		tablaTotales.append("</tr>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(cuenta);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(descripcion);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(fchAplicacion);
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(formatoMoneda(importe));
		tablaTotales.append("&nbsp;</td>");
		tablaTotales.append("<td class=\"");
		tablaTotales.append(bgcolor);
		tablaTotales.append("\" nowrap align=\"center\">");
		tablaTotales.append(registros);
		tablaTotales.append("&nbsp;</td>" + "</table><br>");

		return tablaTotales.toString();
	}

	protected void pagoIndividual(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
	{
		String textoLog = textoLog("pagoIndividual");
		EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Verificando datos en sesion", EIGlobal.NivelLog.DEBUG);
		String Concepto = "";
		String archivoConcepto = "";
		boolean sesionvalida = SesionValida(request, response);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		boolean solicitarOTP = true;
		double impBit=0.0;
		String ctaOriBit="";
		String ctaDesBit="";
		String codErrBit=" ";

		if (sesionvalida)
		{
			if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null)
			{
				EIGlobal.mensajePorTrace(textoLog + "Se va a efectuar la transaccion", EIGlobal.NivelLog.DEBUG);
				solicitarOTP = true;
			}
			//String valida = "1";//request.getParameter("valida");
			String valida = request.getParameter("valida");
			if (validaPeticion(request, response, session, sess, valida))
			{
				EIGlobal.mensajePorTrace(textoLog + "Entro a validar Peticion", EIGlobal.NivelLog.DEBUG);
			}
			else
			{
				session.setModuloConsultar(IEnlace.MEnvio_pagos_nomina);
				EIGlobal.mensajePorTrace(textoLog, EIGlobal.NivelLog.DEBUG);

				request.setAttribute("Fecha", ObtenFecha());
				request.setAttribute("ContUser", ObtenContUser(request));
				request.setAttribute("newMenu", session.getFuncionesDeMenu());
				request.setAttribute("MenuPrincipal", session.getStrMenu());

				StringBuffer datesrvr = new StringBuffer("");
				datesrvr.append("<Input type = \"hidden\" name =\"strAnio\" value = \"");
				datesrvr.append(ObtenAnio());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strMes\" value = \"");
				datesrvr.append(ObtenMes());
				datesrvr.append("\">");
				datesrvr.append("<Input type = \"hidden\" name =\"strDia\" value = \"");
				datesrvr.append(ObtenDia());
				datesrvr.append("\">");

				request.setAttribute("Fechas", ObtenFecha(true));
				request.setAttribute("FechaHoy", ObtenFecha(false) + " - 06");
				request.setAttribute("botLimpiar", "");

				String strOperacion = (String) getFormParameter(request, "operacion");
				if (strOperacion == null || strOperacion.equals(""))
					strOperacion = "";
	            String strRegistro = (String) getFormParameter(request, "registro");
				if (strRegistro == null || strRegistro.equals(""))
					strRegistro = "";

				request.setAttribute("operacion", strOperacion);
	            String tipoArchivotmp = getFormParameter(request, "tipoArchivo");
				EIGlobal.mensajePorTrace(textoLog + "tipoArchivo [" + tipoArchivotmp + "]", EIGlobal.NivelLog.DEBUG);
				if (tipoArchivotmp == null || tipoArchivotmp.equals(""))
					tipoArchivotmp = "";

				if (sess.getAttribute("conceptoEnArchivo") != null)
				{
					EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Si existe en sesion! Se lee el parametro concepto", EIGlobal.NivelLog.DEBUG);
					Concepto = (String) getFormParameter(request, "Concepto");
					archivoConcepto = (String) sess.getAttribute("nombreArchivo");
					EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Concepto: " + Concepto, EIGlobal.NivelLog.DEBUG);
					EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: archivoConcepto: " + archivoConcepto, EIGlobal.NivelLog.DEBUG);

					if (actualizaConceptoEnArchivo(archivoConcepto, Concepto))
					{
						EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Se actualizo el archivo", EIGlobal.NivelLog.DEBUG);
						sess.removeAttribute("conceptoEnArchivo");
					}
					else
					{
						despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de N&oacute;mina","Servicios &gt; Env&iacute;o de Archivos de N&oacute;mina",request, response);
						return;
					}
				}
				else
					EIGlobal.mensajePorTrace(textoLog + "EnvioDeArchivo: Los registros en archivo contienen el concepto", EIGlobal.NivelLog.DEBUG);

				EIGlobal.mensajePorTrace(textoLog + "Antes de Verificacion de Token, solicitarOTP = " + solicitarOTP, EIGlobal.NivelLog.DEBUG);

				boolean valBitacora = true;
				if (valida == null)
				{
					EIGlobal.mensajePorTrace(textoLog + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
					boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA_PREPAGO);
					if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && session.getToken().getStatus() == 1 && solVal && solicitarOTP)
					{
						EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
						guardaParametrosEnSession(request);
						EIGlobal.mensajePorTrace("TOKEN PAGO INDIVIDUAL", EIGlobal.NivelLog.INFO);
						request.getSession().removeAttribute("mensajeSession");
						ValidaOTP.mensajeOTP(request, "PagoIndividual");
						ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
					}
					else
					{
						valida = "1";
						valBitacora = false;
					}
				}

				if (valida != null && valida.equals("1"))
				{
					boolean banderaTransaccion = true;
					if (request.getSession().getAttribute( "Ejec_Proc_Val_Pago") != null)
					{
						EIGlobal.mensajePorTrace( "-----------getRequestURL--a->"+ request.getRequestURL().toString(), EIGlobal.NivelLog.INFO);
						if (valBitacora && request.getRequestURL().toString().contains("transferencia"))
						{
							try{
									HttpSession sessionBit = request.getSession(false);
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_PAGO_NOMINA_PREP;
									String concepto = BitaConstants.CTK_CONCEPTO_PAGO_NOMINA_PREP;
								    String cuenta="0";
								    double importeDouble = 0;
									String token = "0";
									if (request.getParameter("token") != null && !request.getParameter("token").equals(""));
									{token = request.getParameter("token");}
									String importe = sessionBit.getAttribute("importe1").toString().trim();
									importeDouble = Double.valueOf(importe).doubleValue();
									cuenta = sessionBit.getAttribute("num_cuenta").toString().trim();
									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.INFO);
									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(request,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);
							} catch(Exception e) {
								EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.ERROR);
							}
						}
						EIGlobal.mensajePorTrace(textoLog + "Es recarga de procedimiento por VALIDACION.", EIGlobal.NivelLog.DEBUG);
						banderaTransaccion = true;
					}
					//else
						//if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null && verificaArchivos("/tmp/" + request.getSession().getId().toString() + ".ses", false))
						//{
							//EIGlobal.mensajePorTrace(textoLog + "El archivo y la variable todavia existe, se esta enviando.", EIGlobal.NivelLog.DEBUG);
							//banderaTransaccion = false;
						//}
						//else
							//if (request.getSession().getAttribute("Recarga_Ses_Multiples") != null)
							//{
								//EIGlobal.mensajePorTrace(textoLog + "Archivo no existe termino proceso y se borra Variable...", EIGlobal.NivelLog.DEBUG);
								//request.getSession().removeAttribute("Recarga_Ses_Multiples");
								//banderaTransaccion = false;
							//}
							//else
								//if (verificaArchivos("/tmp/" + request.getSession().getId().toString() + ".ses", false))
								//{
									//EIGlobal.mensajePorTrace(textoLog + "El archivo todavia existe, el proceso continua", EIGlobal.NivelLog.DEBUG);
									//banderaTransaccion = false;
								//}
								//else
									//if (request.getSession().getAttribute("Recarga_Ses_Multiples") == null && !verificaArchivos("/tmp/" + request.getSession().getId().toString() + ".ses", false))
									//{
										//EIGlobal.mensajePorTrace(textoLog + "Entrando por primera vez, variables limpias.", EIGlobal.NivelLog.DEBUG);
										//banderaTransaccion = true;
									//}
					if (request.getSession().getAttribute("Ejec_Proc_Val_Pago") != null)
					{
						request.getSession().removeAttribute("Ejec_Proc_Val_Pago");
						EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago REMOVIDO.", EIGlobal.NivelLog.DEBUG);
					}
					EIGlobal.mensajePorTrace(textoLog + "banderaTransaccion:" + banderaTransaccion, EIGlobal.NivelLog.DEBUG);

					if (banderaTransaccion)
					{
						//EI_Exportar arcTmp = new EI_Exportar("/tmp/" + request.getSession().getId().toString() + ".ses");
						//if (arcTmp.creaArchivo())
							//EIGlobal.mensajePorTrace(textoLog + "Archivo .ses Se creo exitosamente ", EIGlobal.NivelLog.DEBUG);
						String digit = getFormParameter(request, "statusDuplicado");
						String digit_d = getFormParameter(request, "statushrc");
						request.getSession().setAttribute("Recarga_Ses_Multiples", "TRUE");
						EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago Creado.", EIGlobal.NivelLog.DEBUG);

						String fechaApli=getFormParameter(request, "encFechApli");
						String folioBit= (String) getFormParameter(request, "folio");

						EIGlobal.mensajePorTrace(textoLog + "No se pudo copiar el archivo", EIGlobal.NivelLog.DEBUG);
						String fechaAplicacion = (String) getFormParameter(request, "encFechApli");//encFechApli
						request.setAttribute("encFechApli", fechaAplicacion);
						EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion PARAMETER" + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);

						if (fechaAplicacion != null)
							sess.setAttribute("NOMIencFechApli", fechaAplicacion);
						if (fechaAplicacion != null)
						{
							EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio LENGTH" + getFormParameter(request, "encFechApli"), EIGlobal.NivelLog.DEBUG);
							fechaAplicacion = fechaAplicacion.trim();
							fechaAplicacion = fechaAplicacion.substring(0,2) + fechaAplicacion.substring(3,5) + fechaAplicacion.substring(6,fechaAplicacion.length());
						} else
						{
							EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion dentro de envio" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
							fechaAplicacion = (String) sess.getAttribute("NOMIencFechApli");
							fechaAplicacion = fechaAplicacion.trim();
							fechaAplicacion = fechaAplicacion.substring(0,2) + fechaAplicacion.substring(3,5) + fechaAplicacion.substring(6,fechaAplicacion.length());
						}

						//java.util.Date dt = new java.util.Date();
						String HorarioDisp = "";
						String HorarioSele = "";
						EIGlobal.mensajePorTrace(textoLog + "Validacion de fecha", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "Fecha del dia de hoy (true) " + ObtenFecha(true), EIGlobal.NivelLog.DEBUG);
						String fecha_hoy = ObtenFecha(true);
						String dd_hoy = fecha_hoy.substring(0, 2);
						String mm_hoy = fecha_hoy.substring(3, 5);
						String aa_hoy = fecha_hoy.substring(6, 10);
						EIGlobal.mensajePorTrace(textoLog + "Parametros " + "dd_hoy=" + dd_hoy + ", mm_hoy=" + mm_hoy + ", aa_hoy=" + aa_hoy, EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "fecha de hoy (false)" + ObtenFecha(false), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "Fecha Aplicacion fechaAplicacion.substring(0,2)=" + fechaAplicacion.substring(0, 2) + ", fechaAplicacion.substring(2,4)=" + fechaAplicacion.substring(2, 4) + ", fechaAplicacion.substring(4,8)=" + fechaAplicacion.substring(4, 8), EIGlobal.NivelLog.DEBUG);
						if ((dd_hoy.equals(fechaAplicacion.substring(0, 2))
								&& mm_hoy.equals(fechaAplicacion.substring(2, 4))
								&& aa_hoy.equals(fechaAplicacion.substring(4, 8)))
								&& ((Calendar.HOUR_OF_DAY == 16
										&& Calendar.MINUTE >= 30)
										|| Calendar.HOUR_OF_DAY >= 17))
						{
							EIGlobal.mensajePorTrace(textoLog + "Horario Premier >>" + HorarioSele + "<<", EIGlobal.NivelLog.ERROR);
							request.setAttribute("folio", (String) getFormParameter(request, "folio"));
							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));//YHG NPRE
							String infoUser = "HORA LIMITE PARA APLICAR PAGOS HOY ES 16:30 HRS.";
							String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
							infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
							request.setAttribute("infoUser", infoUser);

							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess.getAttribute("facultadesN"));
							request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
							sess.setAttribute("archDuplicado", "si");
							request.setAttribute("digit", "0");
							evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
							return;
						}

						EIGlobal.mensajePorTrace(textoLog + "fechaAplicacion FINAL" + fechaAplicacion, EIGlobal.NivelLog.DEBUG);
						String cuenta=" ";
						String importe="";
						String contrato = session.getContractNumber();
						String empleado = session.getUserID8();
						String perfil = session.getUserProfile();
						cuenta = sess.getAttribute("num_cuenta").toString();
						importe = sess.getAttribute("importe1").toString();
						String importePunto = importe;
						/*
						if(importe.substring((importe.length()-3), (importe.length()-2)).equals("."))
						{
							importePunto = importe.substring(0,importe.length()-3) + importe.substring(importe.length()-2);
						}
						else
						{
							importePunto = importe;
						}
						*/
						String totalRegNom = "00001";
						String num_tarjeta=" ";
						num_tarjeta = sess.getAttribute("num_tarjeta").toString();
						String num_empleado = sess.getAttribute("num_empleado").toString();
						String paterno = sess.getAttribute("paterno").toString();
						String materno = sess.getAttribute("materno").toString();
						String nombre = sess.getAttribute("nombre").toString();

						String folio = "";
						//ServicioTux tuxGlobal = new ServicioTux();
						String tramaEntrada = "";
						String horarioPremier = "";
						String FacultadPremier = "";
						FacultadPremier = (String) sess.getAttribute("facultadesN");
						EIGlobal.mensajePorTrace(textoLog + "Facultad Premier CCBFP " + FacultadPremier, EIGlobal.NivelLog.INFO);

						if (FacultadPremier.indexOf("PremierVerdadero") != -1)
						{
							horarioPremier = getFormParameter(request, "horario_seleccionado");
							EIGlobal.mensajePorTrace(textoLog + "CCBHorarioPremierMod " + horarioPremier, EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "horarioPremier  Modificado >" + horarioPremier.trim() + "<", EIGlobal.NivelLog.DEBUG);
							request.setAttribute("horario_seleccionado", getFormParameter(request, "horario_seleccionado"));
							request.setAttribute("HorarioPremier", getFormParameter(request, "horario_seleccionado"));

							if (horarioPremier == null)
								horarioPremier = "17:00";
						}
						else
						{
							horarioPremier = "17:00";
						}

						sess.setAttribute("HR_P", getFormParameter(request, "horario_seleccionado"));
						EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(HR_P) >>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(textoLog + "sess.getAttribute(archDuplicado)>>" + sess.getAttribute("archDuplicado") + "<<", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace(textoLog + "BOX 1", EIGlobal.NivelLog.ERROR);

						if (digit.compareTo("0") == 0)
						{
							horarioPremier = (String) sess.getAttribute("HR_P");
							EIGlobal.mensajePorTrace(textoLog + "horarioPremier >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
							digit = "0";
						}

						String servicio = "NPVI";
						String validar = (String) sess.getAttribute("VALIDARIND");
						folio = (String) sess.getAttribute("FOLIO");

						if (folio == null || folio.equals(""))
						{
							sess.setAttribute("FOLIO", getFormParameter(request, "folio"));
							folio = (String) sess.getAttribute("FOLIO");
							EIGlobal.mensajePorTrace(textoLog + "El folio esta vacio", EIGlobal.NivelLog.DEBUG);
						}

						EIGlobal.mensajePorTrace(textoLog + "Folio [" + folio + "]", EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "Validar [" + validar + "]", EIGlobal.NivelLog.DEBUG);

						String HorarioNormal = (String)sess.getAttribute("HorarioNormal");

						if (HorarioNormal == null)
							HorarioNormal = "";
						if (HorarioNormal.equals("1"))
						{
							if (digit_d.compareTo("0") == 0)
							{
								horarioPremier = "17:00";
								EIGlobal.mensajePorTrace(textoLog + "horarioPremier 17:00 >>" + horarioPremier + "<<", EIGlobal.NivelLog.ERROR);
								digit = "0";
							}
						}

						if (validar == null)
							validar = "";
						if (validar.compareTo("N") == 0)
						{
							servicio = "PNIS";
						}

						tramaEntrada = "1EWEB|" + empleado + "|" + servicio
						+ "|" + contrato + "|" + empleado + "|" + perfil
						+ "|" + contrato + "@" + fechaAplicacion + "@"
						+ totalRegNom + "@" + importePunto + "@"
						+ cuenta + "@" + " " + "@" + digit + "@"
						+ horarioPremier + "@" + num_empleado + "@"
						+ num_tarjeta + "@" + paterno + "@" + materno + "@"
						+ nombre + "@" + "|";

						if (folio.equals(""))
							tramaEntrada += " | | |";
						else
							tramaEntrada += cuenta + "|" + importePunto + "|" + folio + "|";

						EIGlobal.mensajePorTrace(textoLog + "Trama que se enviara >>" + tramaEntrada + "<<", EIGlobal.NivelLog.ERROR);
						String tramaSalida = "";
						String folioArchivo = "";

						if (servicio.equals("PNIS"))
						{
							LYMValidador val = new LYMValidador();
		 					String resLYM = val.validaLyM(tramaEntrada);
		 					val.cerrar();//CSA
							if(!resLYM.equals("ALYM0000")) {

								request.setAttribute("folio", (String) getFormParameter(request, "folio"));
								request.setAttribute("newMenu", session.getFuncionesDeMenu());
								request.setAttribute("MenuPrincipal", session.getStrMenu());
								request.setAttribute("Encabezado", CreaEncabezado("Tarjeta de Pagos Santander ", "Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos", "s25800IIIh", request));//YHG NPRE
								String infoUser = resLYM;
								String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";
								infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",10)";// CERRAR
								request.setAttribute("infoUser", infoUser);
								if (sess.getAttribute("facultadesN") != null)
									request.setAttribute("facultades", sess.getAttribute("facultadesN"));
								request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
								sess.setAttribute("archDuplicado", "si");
								request.setAttribute("digit", "0");
								evalTemplate(IEnlace.TJT_NOMINA_TMPL, request, response);
								return;

							}
						//Aqui puede ir otro codigo o bien nos podemos siempre saltar al else
							folioArchivo = generaFolio(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), servicio, totalRegNom);
							session.setFolioArchivo(folioArchivo);
							request.setAttribute("folioArchivo", folioArchivo);
							EIGlobal.mensajePorTrace(textoLog + "folioArchivo >>" + folioArchivo + "<<", EIGlobal.NivelLog.ERROR);
							//String mensajeErrorEnviar = new String();
							String mensajeErrorEnviar = envioDatos(session.getUserID8(), session.getUserProfile(), session.getContractNumber(), folioArchivo, servicio, " ", tramaEntrada);
							EIGlobal.mensajePorTrace(textoLog + "mensajeErrorEnviar>>" + mensajeErrorEnviar + "<<", EIGlobal.NivelLog.ERROR);

							if (mensajeErrorEnviar.equals("OK"))
							{
								tramaSalida = realizaDispersion(session.getUserID8(), session.getUserProfile(), session .getContractNumber(), folioArchivo, servicio);
							}
						}
						else
						{
							tramaSalida = ejecutaServicio(tramaEntrada);
						}
						EIGlobal.mensajePorTrace("Trama entrada :" + tramaEntrada, NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Trama salida :" + tramaSalida, NivelLog.DEBUG);
						EIGlobal.mensajePorTrace(textoLog + "Trama salida>>" + tramaSalida + "<<", EIGlobal.NivelLog.ERROR);
						String estatus_envio = "";
						String fechaAplicacionFormato = "";
						//fechaAplicacionFormato = fechaAplicacion.substring( 0, 2) + "/" + fechaAplicacion.substring(2, 4) + "/" + fechaAplicacion.substring(4, 8);

						if (!tramaSalida.startsWith("MANC"))
						{
							try
							{
								estatus_envio = tramaSalida.substring(0, posCar(tramaSalida, '@', 1));
							} catch (Exception e) {
								estatus_envio = "";
							}
						}
						if(cuenta!=null && !cuenta.trim().equals("")){
							ctaOriBit=cuenta;
						}else{ctaOriBit=" ";}
						if(num_tarjeta!=null && !num_tarjeta.trim().equals("")){
							ctaDesBit=num_tarjeta;
						}else{ctaDesBit=" ";}
						if(importePunto!=null && !importePunto.trim().equals("")){
							impBit=Double.parseDouble(importePunto);
						}
						if(estatus_envio!=null && !estatus_envio.trim().equals("")){
							codErrBit=estatus_envio;
						}

						if (estatus_envio.equals("NOMI0000"))
						{
							if (servicio.equals("NPVI"))
							{
								sess.setAttribute("VALIDARIND", "N"); // Setear la variable global para que no se continue con la validacion.
								request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
								EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO.", EIGlobal.NivelLog.DEBUG);
								pagoIndividual(request, response); // Ejecutar nuevamente la operacion de envio para procesar el archivo.
								return;
							}

							String transmision = tramaSalida.substring(posCar(tramaSalida, '@', 2) + 1, posCar(tramaSalida, '@', 3));
							String nombreArchivo_aceptado = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
							String fechaCargo = null;
							int start = posCar(tramaSalida, '@', 3) + 1;
							int end = tramaSalida.indexOf('@', start);
							if (start < end)
							{
								fechaCargo = tramaSalida.substring(start, end);
								fechaCargo = fechaCargo.substring(0, 2) + "/" + fechaCargo.substring(2, 4) + "/" + fechaCargo.substring(4, 8);
							}

							String bgcolor = "textabdatobs";
							StringBuffer tablaTotales = new StringBuffer("");
							tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
							tablaTotales.append("<tr> ");
							tablaTotales.append("<td class=\"textabref\"></td>");
							tablaTotales.append("</tr>");
							tablaTotales.append("</table>");
							tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
							tablaTotales.append("<tr>");
							tablaTotales.append("<td width=\"640\" class=\"tittabdat\" align=\"center\">Pago de n&oacute;mina Individual</td>");
							tablaTotales.append("</tr>");
							tablaTotales.append("<td class=\"");
							tablaTotales.append(bgcolor);
							tablaTotales.append("\" nowrap align=\"center\">Se ha registrado el pago individual correctamente con el folio ");
							tablaTotales.append(folioArchivo);//se debe verificar que tipo de folio va aqui
							tablaTotales.append("&nbsp;</td>");
							tablaTotales.append("</table><br>");
							tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");

							request.setAttribute("newMenu", session.getFuncionesDeMenu());
							request.setAttribute("MenuPrincipal", session.getStrMenu());
							request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));//YHG NPRE
							request.setAttribute("TablaTotales", tablaTotales.toString());
							String infoUser = "La nomina fue aceptada correctamente: " + nombreArchivo_aceptado + " n&uacute;mero de transmisi&oacute;n = " + transmision;
							infoUser = "cuadroDialogoCatNominaPrep (\"" + infoUser + "\",1)";
							request.setAttribute("infoUser", infoUser);

							if (sess.getAttribute("facultadesN") != null)
								request.setAttribute("facultades", sess.getAttribute("facultadesN"));
							request.setAttribute("tipoArchivo", "enviado");
							String botonestmp = "<br>"
								+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
								+ " <td><A href = javascript:js_regresarPagosInd(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
								+ "<td></td></tr> </table>";
							request.setAttribute("botones", botonestmp);
							request.setAttribute("ContenidoArchivo", " ");
							sess.removeAttribute("VALIDARIND");
							EIGlobal.mensajePorTrace(textoLog + "botones >>" + request .getAttribute("botones") + "<<", EIGlobal.NivelLog.ERROR);

							evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
						}
						else
						{// de trasaccion exitosa
							HorarioDisp = "";
							HorarioSele = "";
							if (FacultadPremier.indexOf("PremierVerdadero") != -1)
							{
								HorarioDisp = (String)sess.getAttribute("SelectHorarioPremier");
								HorarioSele = (String)getFormParameter(request, "horario_seleccionado");
								EIGlobal.mensajePorTrace(textoLog + "El horario seleccionado es:   " + HorarioSele, EIGlobal.NivelLog.INFO);
							}
							EIGlobal.mensajePorTrace("Horarios disponibles :" + HorarioDisp, NivelLog.DEBUG);
							EIGlobal.mensajePorTrace(textoLog + "****************BOX 2********************", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + ">>" + sess.getAttribute("statusHorario") + "<<", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + ">>" + request.getAttribute("hr_lost") + "<<", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + ">>" + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + "****************************************", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + "*******************BOX 2*****************", EIGlobal.NivelLog.ERROR);
							EIGlobal.mensajePorTrace(textoLog + "HORARIO PREMIER>>" + HorarioSele + "<<", EIGlobal.NivelLog.ERROR);

							request.setAttribute("folio", (String) getFormParameter(request, "folio"));
							EIGlobal.mensajePorTrace(textoLog + "CCBEst_Env" + estatus_envio, EIGlobal.NivelLog.DEBUG);

							EIGlobal.mensajePorTrace(textoLog + "tramasalida [" + tramaSalida + "]", EIGlobal.NivelLog.INFO);
							EIGlobal.mensajePorTrace(textoLog + "folioArchivo[" + folioArchivo + "]", EIGlobal.NivelLog.INFO);

							//bitacora
//							bitacora
							EIGlobal.mensajePorTrace("Antes de bitacora confirma" , EIGlobal.NivelLog.INFO);
							SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
							BitaTransacBean bt = new BitaTransacBean();

							bt.setNumBit(BitaConstants.ES_NP_PAGOS_INDIVIDUAL_CONFIRMA);
							if(folioArchivo!=null && !folioArchivo.trim().equals("") )
								bt.setReferencia(Long.parseLong(folioArchivo));
							if(fechaApli!=null && !fechaApli.trim().equals(""))
								try {
									bt.setFechaAplicacion(sdf.parse(fechaApli));
								} catch (ParseException e1) {}
							if(cuenta!=null && !cuenta.trim().equals("")){
								bt.setCctaOrig(cuenta);
								ctaOriBit=cuenta;
							}else{ctaOriBit=" ";}
							if(num_tarjeta!=null && !num_tarjeta.trim().equals("")){
								bt.setCctaDest(num_tarjeta);
								ctaDesBit=num_tarjeta;
							}else{ctaDesBit=" ";}
							if(importePunto!=null && !importePunto.trim().equals("")){
								impBit=Double.parseDouble(importePunto);
								bt.setImporte(impBit);
							}
							if(estatus_envio!=null && !estatus_envio.trim().equals("")){
								codErrBit=estatus_envio;
							}
							NomPreUtil.bitacoriza(request,"NOMI", codErrBit, bt);

							/*try
							{
								int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
								String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,NomPreUtil.ES_NP_PAGOS_INDIVIDUAL_OPER,ctaDesBit,0," ");
							}catch(Exception e){
								EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
							}*/
							if (tramaSalida.equals("OK"))
							{
								EIGlobal.mensajePorTrace(textoLog + "PNOS Correcto, procediendo a generar tabla y mensaje", EIGlobal.NivelLog.DEBUG);
								sess.setAttribute("HorarioNormal", "0");

								String bgcolor = "textabdatobs";
								StringBuffer tablaTotales = new StringBuffer("");
								tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
								tablaTotales.append("<tr> ");
								tablaTotales.append("<td class=\"textabref\"></td>");
								tablaTotales.append("</tr>");
								tablaTotales.append("</table>");
								tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
								tablaTotales.append("<tr>");
								tablaTotales.append("<td width=\"640\" class=\"tittabdat\" align=\"center\">Pago de n&oacute;mina Individual</td>");
								tablaTotales.append("</tr>");
								tablaTotales.append("<td class=\"");
								tablaTotales.append(bgcolor);
								tablaTotales.append("\" nowrap align=\"center\">Se ha registrado el pago individual correctamente con el folio ");
								tablaTotales.append(folioArchivo);//se debe verificar que tipo de folio va aqui
								tablaTotales.append("&nbsp;</td>");
								tablaTotales.append("</table><br>");
								tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");

								StringBuffer mensajeUser = new StringBuffer();

								mensajeUser.append("La n&oacute;mina est&aacute; siendo procesada.<br>");
								mensajeUser.append("El n&uacute;mero de folio correspondiente a la operaci&oacute;n es:<br>");
								mensajeUser.append(folioArchivo);
								mensajeUser.append("<br>Utillice este n&uacute;mero para consultar las<br>");
								mensajeUser.append("referencias y estado de las referencias,<br>");
								mensajeUser.append("en Seguimiento de Transferencias.");

								String infoUser = "cuadroDialogoCatNominaPrep (\"" + mensajeUser.toString() + "\",1)";
								request.setAttribute("infoUser", infoUser);

								if (sess.getAttribute("facultadesN") != null)
								{
									request.setAttribute("facultades", sess.getAttribute("facultadesN"));
								}

								String botonestmp = "<br>"
									+ "<table align=center border=0 cellspacing=0 cellpadding=0><tr>"
									+ " <td><A href = javascript:js_regresarPagosInd() border = 0><img src = ../../gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>"
									+ "<td></td></tr> </table></td></tr>";

								request.setAttribute("botones", botonestmp);
								request.setAttribute("newMenu", session.getFuncionesDeMenu());
								request.setAttribute("MenuPrincipal", session.getStrMenu());
								request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));//YHG NPRE
								request.setAttribute("TablaTotales", tablaTotales);
								request.setAttribute("ContenidoArchivo", " ");
								request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
								sess.removeAttribute("VALIDARIND");
								EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);
//								codigo bitacora
								/*try
								{
									EIGlobal.mensajePorTrace("NomPreImportOpciones.class>>TramaEntrada>"+tramaEntrada+"<<", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("NomPreImportOpciones.class>>Cuenta>"+cuenta+"<<", EIGlobal.NivelLog.INFO);
									EIGlobal.mensajePorTrace("NomPreImportOpciones.class>>importePunto>"+importePunto+"<<", EIGlobal.NivelLog.INFO);
									int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									if(estatus_envio!=null && !estatus_envio.trim().equals("")){
										codErrBit=estatus_envio;
									}else{codErrBit=" ";}
									String bitacora = llamada_bitacora(referencia,codErrBit,session.getContractNumber(),session.getUserID(),ctaOriBit,impBit,NomPreUtil.ES_NP_PAGOS_INDIVIDUAL_OPER,ctaDesBit,0," ");
								}catch(Exception e){
									EIGlobal.mensajePorTrace("ENTRA EXCEPCION BITACORIZACION TCT Nomina Prepago ",EIGlobal.NivelLog.ERROR);
								}*/
								 try{
											   EmailSender emailSender = new EmailSender();
								     		   EmailDetails EmailDetails = new EmailDetails();
											   EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> Entrando a configuracion de notificacion <><><><><><><><><><><><><>", EIGlobal.NivelLog.INFO);
									           EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> CONTRATO: " + session.getContractNumber().toString(), EIGlobal.NivelLog.INFO);
									           EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> NOMBRE CONTRATO: " + session.getNombreContrato().toString(), EIGlobal.NivelLog.INFO);
									           EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> REFERENCIA DE ENLACE: " + folioArchivo , EIGlobal.NivelLog.INFO);
									           int numReg = Integer.parseInt (totalRegNom);
									           EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> REGISTROS IMPORTADOS: " + numReg, EIGlobal.NivelLog.INFO);
									           double impDouble = Double.valueOf(importePunto).doubleValue();
									    	   EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> IMPORTE: " +  String.valueOf(impDouble), EIGlobal.NivelLog.INFO);
									    	   EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> CUENTA: " + cuenta, EIGlobal.NivelLog.INFO);
									    	   EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> tramaSalida: " + tramaSalida, EIGlobal.NivelLog.INFO);

									    	   EmailDetails.setNumeroContrato(session.getContractNumber().toString());
									           EmailDetails.setRazonSocial(session.getNombreContrato().toString());
									           EmailDetails.setNumRef(folioArchivo);
									           EmailDetails.setNumCuentaCargo(cuenta);
									           EmailDetails.setImpTotal(ValidaOTP.formatoNumero(impDouble));
									           EmailDetails.setNumTransmision(folioArchivo);
									           EmailDetails.setNumRegImportados(numReg);

									           EmailDetails.setTipoPagoNomina("Tarjeta de Pagos Santander Individual");//YHG NPRE
											   EmailDetails.setEstatusFinal("Enviada");

								   			   if (tramaSalida.startsWith("MANC")) {
								   	    	      EIGlobal.mensajePorTrace("<><><><><><><><><><><><><>[NOMPREIMPORTOPCIONES] -> MANCOMUNADO" , EIGlobal.NivelLog.INFO);
								   	    	      EmailDetails.setEstatusActual("Mancomunada");
										       } else {
								   	    	      EmailDetails.setEstatusActual("Enviada");
										       }
											   try {
												  emailSender.sendNotificacion(request,IEnlace.PAGO_NOMINA_PRE, EmailDetails);
											   } catch ( Exception e ) {
													EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
												}
						 		} catch(Exception e) {
						 			EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
						 		}
								evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
							}
							else
								if (estatus_envio.equals("NOMI0050"))
								{
									sess.setAttribute("HorarioNormal", "0");
									sess.setAttribute("HR_P", HorarioSele);
									EIGlobal.mensajePorTrace(textoLog + "HR_P 2>>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);
									request.setAttribute("newMenu", session.getFuncionesDeMenu());
									request.setAttribute("MenuPrincipal", session.getStrMenu());
									request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
									String infoUser = " Pago Duplicado. �Desea enviar este pago de cualquier manera? ";
									String infoEncabezadoUser = "<B>ALERTA</B>";
									infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
									request.setAttribute("infoUser", infoUser);

									if (sess.getAttribute("facultadesN") != null)
										request.setAttribute("facultades", sess.getAttribute("facultadesN"));
									sess.setAttribute("archDuplicado", "si");
									request.setAttribute("digit", "0");
									request.setAttribute("tipoArchivo", "duplicado");
									request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
									EIGlobal.mensajePorTrace(textoLog + "El horario seleccionado antes de volver a llamar el MantenimientoNomina despuesa de archivo duplicado es: ========>" + HorarioSele + "<======", EIGlobal.NivelLog.DEBUG);
									request.setAttribute("horario_seleccionado", HorarioSele);
									request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
									sess.removeAttribute("VALIDARIND");
									EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por Duplicado.", EIGlobal.NivelLog.DEBUG);
									evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
								}
								else
									if (estatus_envio.equals("NOMI0051"))
									{
										sess.setAttribute("HorarioNormal", "1");
										sess.setAttribute("HR_P", "17:00");
										EIGlobal.mensajePorTrace(textoLog + "HR_P 2 >>" + sess.getAttribute("HR_P") + "<<", EIGlobal.NivelLog.ERROR);
										request.setAttribute("newMenu", session.getFuncionesDeMenu());
										request.setAttribute("MenuPrincipal", session.getStrMenu());
										request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
										String infoUser = " El vol&uacute;men que desea dispersar solamente puede ser procesado en el horario de 17:00";
										String infoEncabezadoUser = "<B>Aviso</B>";
										infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",9)";// ACEPTAR, CANCELAR
										request.setAttribute("infoUser", infoUser);

										if (sess.getAttribute("facultadesN") != null)
											request.setAttribute("facultades", sess.getAttribute("facultadesN"));
										request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));
										request.setAttribute("horarioPremier", "17:00");
										sess.setAttribute("archDuplicado", "si");
										request.setAttribute("digit", "2");
										request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
										sess.removeAttribute("VALIDARIND");
										EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario de 17:00 hrs.", EIGlobal.NivelLog.DEBUG);
										evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
									}
									else
										if (estatus_envio.equals("NOMI0052"))
										{
											EIGlobal.mensajePorTrace(textoLog + "Verificando los horarios saturados", EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + "Horario seleccionado: " + HorarioSele, EIGlobal.NivelLog.ERROR);
											String cad = (String) sess.getAttribute("HOR_SAT");

											if (cad == null)
											{
												sess.setAttribute("HOR_SAT", HorarioSele);
												cad = HorarioSele + "|";
											}
											else
												cad = cad + HorarioSele + "|";
											sess.setAttribute("HOR_SAT", cad);
											EIGlobal.mensajePorTrace(textoLog + "Horarios Saturados: " + cad, EIGlobal.NivelLog.ERROR);
											EIGlobal.mensajePorTrace(textoLog + ">> Antes de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
											EI_Tipo hrD = new EI_Tipo();
											hrD.iniciaObjeto(HorarioDisp + "@");

											String nuevosHorariosDisp = "";
											EIGlobal.mensajePorTrace(textoLog + ">> Se buscaran horarios saturados en sesion<<", EIGlobal.NivelLog.ERROR);
											for (int i = 0; i < hrD.totalCampos; i++)
											{
												EIGlobal.mensajePorTrace(textoLog + ">> Horario disponible: " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
												if (!hrD.camposTabla[0][i].trim().equals(""))
													if (!hrD.camposTabla[0][i].equals(HorarioSele))
														if (!(cad.indexOf(hrD.camposTabla[0][i].trim()) >= 0))
														{
															EIGlobal.mensajePorTrace(textoLog + ">> El horario no se encontro : " + hrD.camposTabla[0][i] + "<<", EIGlobal.NivelLog.DEBUG);
															EIGlobal.mensajePorTrace(textoLog + ">> en saturados: " + cad + "<<", EIGlobal.NivelLog.DEBUG);
															nuevosHorariosDisp += hrD.camposTabla[0][i] + "|";
														}
											}
											HorarioDisp = nuevosHorariosDisp;
											EIGlobal.mensajePorTrace(textoLog + ">> Despues de validacion de horarios: " + HorarioDisp + "<<", EIGlobal.NivelLog.ERROR);
											sess.setAttribute("HorarioNormal", "0");
											sess.setAttribute("HR_P", HorarioSele);
											request.setAttribute("newMenu", session.getFuncionesDeMenu());
											request.setAttribute("MenuPrincipal", session.getStrMenu());
											request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
											String infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible.";
											String infoEncabezadoUser = "<B>Horario No Disponible</B>";
											if (HorarioDisp.trim().length() < 4)
											{
												infoUser = "Por el momento no se encuentra disponible ning&uacute;n horario. Por favor cambie la fecha de aplicaci&oacute;n del archivo para realizar su env&iacute;o.";
												infoUser = "cuadroDialogoPagosNomina(\" <b>Horarios No Disponibles</b>\",\"" + infoUser + "\",10)";
											}
											else
											{
												if (FacultadPremier.indexOf("PremierVerdadero") != -1)
												{
													infoUser = "El horario de las " + HorarioSele + " por el momento no se encuentra disponible. Por favor elija otro horario entre los siguientes: ";
													infoUser += obtenSeleccionInd(HorarioDisp, HorarioSele, 4);
													infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
												}
												else
												{
													infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
												}
											}

											request.setAttribute("infoUser", infoUser);
											if (sess.getAttribute("facultadesN") != null)
												request.setAttribute("facultades", sess.getAttribute("facultadesN"));

											request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));

											if (FacultadPremier.indexOf("PremierVerdadero") != -1)
											{
												request.setAttribute("horario_seleccionado", HorarioSele);
											}

											sess.setAttribute("archDuplicado", "si");
											request.setAttribute("digit", "0");
											request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
											sess.removeAttribute("VALIDARIND");
											EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por horario saturado.", EIGlobal.NivelLog.DEBUG);
											evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
										}
										else
											if (estatus_envio.equals("NOMI0020"))
											{
												sess.setAttribute("HorarioNormal", "0");
												request.setAttribute("newMenu", session.getFuncionesDeMenu());
												request.setAttribute("MenuPrincipal", session.getStrMenu());
												request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
												String infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n.";
												String infoEncabezadoUser = "<B>Operaci&oacute;n Rechazada</B>";

												if (FacultadPremier.indexOf("PremierVerdadero") != -1)
												{
													infoUser = "Recuerde que los archivos deben enviarse con media hora de anticipaci&oacute;n. Por favor elija uno de los siguientes horarios:";
													infoUser += obtenSeleccion(HorarioDisp, HorarioSele, 2);
													infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",2)";// ENVIAR
												}
												else
												{
													infoUser = "cuadroDialogoPagosNomina(\"" + infoEncabezadoUser + "\",\"" + infoUser + "\",15)";// ENVIAR
												}

												request.setAttribute("infoUser", infoUser);

												if (sess.getAttribute("facultadesN") != null)
													request.setAttribute("facultades", sess.getAttribute("facultadesN"));
												request.setAttribute("ContenidoArchivo", sess.getAttribute("ContenidoArchivo"));

												if (FacultadPremier.indexOf("PremierVerdadero") != -1)
												{
													request.setAttribute("horario_seleccionado", HorarioSele);
												}

												sess.setAttribute("archDuplicado", "si");
												request.setAttribute("digit", "0");
												request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
												sess.removeAttribute("VALIDARIND");
												EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por media hora de anticipacion.", EIGlobal.NivelLog.DEBUG);
												evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
											}
											else
											{
												sess.setAttribute("HorarioNormal", "0");
												String mensaje_error = "";
												try
												{
													if (tramaSalida.startsWith("MANC"))
														mensaje_error = tramaSalida.substring(16, tramaSalida.length());
													else
														mensaje_error = tramaSalida.substring(posCar(tramaSalida, '@', 1) + 1, posCar(tramaSalida, '@', 2));
												} catch (Exception e) {
													mensaje_error = "";
												}

												EIGlobal.mensajePorTrace(textoLog + "mensaje_error: >>" + mensaje_error + "<<", EIGlobal.NivelLog.ERROR);
												if (mensaje_error.equals(""))
													mensaje_error = "Servcio no disponible en este momento, intente m&aacute;s tarde !.";

												String bgcolor = "textabdatobs";
												StringBuffer tablaTotales = new StringBuffer("");
												tablaTotales.append("<td align=center><table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
												tablaTotales.append("<tr> ");
												tablaTotales.append("<td class=\"textabref\"></td>");
												tablaTotales.append("</tr>");
												tablaTotales.append("</table>");
												tablaTotales.append("<table width=\"640\" border=\"0\" cellspacing=\"2\" cellpadding=\"3\" class=\"tabfonbla\">");
												tablaTotales.append("<tr>");
												tablaTotales.append("<td width=\"640\" class=\"tittabdat\" align=\"center\">Pago de n&oacute;mina Individual</td>");
												tablaTotales.append("</tr>");
												tablaTotales.append("<td class=\"");
												tablaTotales.append(bgcolor);
												tablaTotales.append("\" nowrap align=\"center\">No se ha podido registrar el pago individual correctamente. ");
												tablaTotales.append(folioArchivo);//se debe verificar que tipo de folio va aqui
												tablaTotales.append("&nbsp;</td>");
												tablaTotales.append("</table><br>");
												tablaTotales.append("<input type=\"hidden\" name=\"hdnPagBack\" value=1>");
												request.setAttribute("newMenu", session.getFuncionesDeMenu());
												request.setAttribute("MenuPrincipal", session.getStrMenu());
												request.setAttribute("Encabezado",CreaEncabezado("Pago Individual","Servicios &gt; Tarjeta de Pagos Santander  &gt; Pagos &gt; Pago Individual","s25800h",request));
												request.setAttribute("TablaTotales", tablaTotales.toString());

												String infoUser = mensaje_error;
												infoUser = "cuadroDialogoCatNomina (\"" + infoUser + "\",1)";
												request.setAttribute("infoUser", infoUser);

												if (sess.getAttribute("facultadesN") != null)
												{
													request.setAttribute("facultades", sess.getAttribute("facultadesN"));
												}

												String botonestmp = "<br>" + "<table align=center border=0 cellspacing=0 cellpadding=0><tr>" + " <td><A href = javascript:js_regresarPagosInd(); border = 0><img src = /gifs/EnlaceMig/gbo25320.gif border=0 alt=Regresar></a></td>" + "<td></td></tr> </table></td></tr>";
												request.setAttribute("botones", botonestmp);
												request.setAttribute("ContenidoArchivo", " ");
												sess.removeAttribute("VALIDARIND");
												request.getSession().setAttribute("Ejec_Proc_Val_Pago", "TRUE");
												EIGlobal.mensajePorTrace(textoLog + "Atributo Ejec_Proc_Val_Pago CREADO por cualquier motivo.", EIGlobal.NivelLog.DEBUG);
												evalTemplate(IEnlace.TJT_NOMINDIVIDUAL_TML, request, response);
											}
						}
					}
					else
					{
						EIGlobal.mensajePorTrace(textoLog + "***Entra NominaOcupada", EIGlobal.NivelLog.DEBUG);
						request.setAttribute("MenuPrincipal", session.getStrMenu());
						request.setAttribute("newMenu", session.getFuncionesDeMenu());
						request.setAttribute("Encabezado", CreaEncabezado("Transacci&oacute;n en Proceso", "Servicios &gt; N&oacute;mina &gt; Pagos", "s55290", request));
						request.setAttribute("operacion", "tranopera");
						sess.removeAttribute("VALIDARIND");
						evalTemplate("/jsp/prepago/NomImportTranenProceso.jsp", request, response);
					}
				}
			}
		}
	}
	public void estableceFacultades( HttpServletRequest request, HttpServletResponse response )throws ServletException, IOException
	{
 		HttpSession sess = request.getSession();
 		String eImportar="", eAltas="", eModificar="", eBaja="", eEnvio="", eRecuperar="", eExportar="", eBorrar="";
 		String cHTML="<INPUT TYPE=\"hidden\" NAME=\"fImportar\" VALUE=\"";
 		String cHTML1="\">";
/*
		//facultad para Importar
 		if(verificaFacultad("INOMIMPAPAG",request))
 		{
 			eImportar="ImportarVerdadero";
 		}
 		else
 		{
 			eImportar="SinFacultades";
 		}
 		//facultad para Alta de Registros
 		if(verificaFacultad("INOMALTAPAG",request))
 		{
 			eAltas="AltaVerdadero";
 		}
 		else
 		{
 			eAltas="SinFacultades";
 		}
 		//facultad para Edicion de Registros
 		if(verificaFacultad("INOMMODIPAG",request))
 		{
 			eModificar="CambiosVerdadero";
 		}
 		else
 		{
 			eModificar="SinFacultades";
 		}
 		//facultad para Baja de Registros
 		if(verificaFacultad("INOMBAJAPAG",request))
 		{
 			eBaja="BajasVerdadero";
 		}
 		else
 		{
 			eBaja="SinFacultades";
 		}
 		//facultad para el Envio
 		if(verificaFacultad("INOMENVIPAG",request))
 		{
 			eEnvio="EnvioVerdadero";
 		}
 		else
 		{
 			eEnvio="SinFacultades";
 			request.setAttribute("facultadEnvio",cHTML+eEnvio+cHTML1);
 		}
 		//facultad para la recuperacion
 		if(verificaFacultad("INOMACTUPAG",request))
 		{
 			eRecuperar="RecuperarVerdadero";
 		}
 		else
 		{
 			eRecuperar="SinFacultades";
 		}
 		//facultad para exportar
 		if(verificaFacultad("INOMEXPOPAG",request))
 		{
 			eExportar="ExportarVerdadero";
 		}
 		else
 		{
 			eExportar="SinFacultades";
 		}
 		//facultad para borrar un archivo
 		if(verificaFacultad("INOMBORRPAG",request))
 		{
 			eBorrar="BorraVerdadero";
 		}
 		else
 		{
 			eBorrar="SinFacultades";
 		}
		//facultad para obtener HORARIOS PREMIER
		String eHorarioPremier="";
		if(verificaFacultad("INOMHORARIOS",request))
		{
			eHorarioPremier="PremierVerdadero";
		}
		else
		{
			eHorarioPremier="SinFacultades";
		}
		*/
		StringBuffer valor1 = new StringBuffer ("");  /*JEV 12/03/04*/
		valor1 = new StringBuffer ("ImportarVerdadero");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("EnvioVerdadero");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("SinFacultades");
		valor1.append (",");
		valor1.append ("PremierVerdadero");

		sess.setAttribute("facultadesN",valor1.toString());
		request.setAttribute("facultades",valor1.toString());
		EIGlobal.mensajePorTrace("Facultades de nomina en la variable de ambiente: "+sess.getAttribute("facultadesN"), NivelLog.DEBUG);
	}

	/**
	 * validaTerciaBiatux lee archivo  para validar la tercia.
	 * @param bean datos de entrada
	 */
	public NominaArchBean validaTerciaBiatux(NominaArchBean bean) {
		BufferedReader arc;
		String archivo=Global.directorioConfignln+"/config.cfg";
		try {
			EIGlobal.mensajePorTrace(textoLog("validaTerciaBiatux") + "Archivo::::  " + archivo, EIGlobal.NivelLog.DEBUG);
			arc = new BufferedReader(new FileReader(archivo));
			String linea=arc.readLine();
			linea=linea != null ? linea : "";
			EIGlobal.mensajePorTrace("NomPreImportOpciones.java :: linea : "+linea, EIGlobal.NivelLog.INFO);
			bean.setBiatuxOrigen(linea.trim());
			EIGlobal.mensajePorTrace("NomPreImportOpciones.java :: servidorBiatux = "+bean.getBiatuxOrigen(), EIGlobal.NivelLog.INFO);
			arc.close();
		} catch (IOException e) {
			EIGlobal.mensajePorTrace("NomPreImportOpciones.java :: el archivo " + archivo + " no existe .:.:.", EIGlobal.NivelLog.INFO);
			bean.setBiatuxOrigen(" ");
			EIGlobal.mensajePorTrace("NomPreImportOpciones.java :: se pone por default en el equipo biatux el valor de '" + bean.getBiatuxOrigen() + "' .:.:. ", EIGlobal.NivelLog.INFO);
		}
		return bean;
	}
}