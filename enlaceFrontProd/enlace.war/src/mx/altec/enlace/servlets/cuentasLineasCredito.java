package mx.altec.enlace.servlets;

import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.sql.*;


/**
** cuentasSerfinSantander is a blank servlet to which you add your own code.
*/
public class cuentasLineasCredito extends  BaseServlet
	{

	// ---
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// ---
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// ---
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		boolean sesionvalida = SesionValida(req, res);
		String moduloconsultar= "";

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		if(sesionvalida)
			{
			String  ventana   = req.getParameter("Ventana");
			String  crialtair = req.getParameter("ctasantander");
			String  crihogan  = req.getParameter("ctaserfin");
			String  cridesc   = req.getParameter("descrip");
			String  opcion    = req.getParameter("opcion");

			if (crialtair==null) crialtair=" ";
			if (crihogan==null) crihogan=" ";
			if (cridesc==null) cridesc=" ";

			if (crialtair.length()==0) crialtair=" ";
			if (crihogan.length()==0) crihogan=" ";
			if (cridesc.length()==0) cridesc=" ";

			EIGlobal.mensajePorTrace("***cuentasSerfinSantander Ventana "+ventana, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***cuentasSerfinSantander opcion "+opcion, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("***cuentasSerfinSantander Modulo  "+ session.getModuloConsultar(), EIGlobal.NivelLog.INFO);
			moduloconsultar=session.getModuloConsultar();

			if ((moduloconsultar==null) || (opcion.equals("4")))
				{
				moduloconsultar=IEnlace.MMant_gral_ctas;
				opcion="4";
				}

			if ((moduloconsultar.equals(IEnlace.MCargo_transf_pesos)) && (opcion.equals("2")))
				moduloconsultar=IEnlace.MAbono_transf_pesos;
			else if ((moduloconsultar.equals(IEnlace.MCargo_transf_dolar)) && (opcion.equals("2")))
				moduloconsultar=IEnlace.MAbono_transf_dolar;
			else if ((moduloconsultar.equals(IEnlace.MCargo_transf_pesos)) && (opcion.equals("3")))
				moduloconsultar=IEnlace.MAbono_tarjeta_cred;
			else if ((moduloconsultar.equals(IEnlace.MDep_inter_cargo))&& (opcion.equals("2")))
				moduloconsultar=IEnlace.MDep_Inter_Abono;
			else if ((moduloconsultar.equals(IEnlace.MMant_gral_ctas))&& (opcion.equals("2")))
				moduloconsultar=IEnlace.MDep_Inter_Abono;
			else if ((moduloconsultar.equals(IEnlace.MCargo_cambio_pesos)) && (opcion.equals("1")))
				moduloconsultar=IEnlace.MCargo_cambio_pesos+IEnlace.MCargo_cambio_dolar;
			else if ((moduloconsultar.equals(IEnlace.MCargo_cambio_pesos)) && (opcion.equals("2")))
				moduloconsultar=IEnlace.MAbono_cambios_pesos+IEnlace.MAbono_cambio_dolar;
			else if ((moduloconsultar.equals(IEnlace.MCargo_TI_pes_dolar)) && (opcion.equals("2")))
				moduloconsultar=IEnlace.MAbono_TI;

			EIGlobal.mensajePorTrace("***cuentasSerfinSantander moduloconsultar  "+ moduloconsultar, EIGlobal.NivelLog.INFO);

			if (ventana.equals("0"))
				pant_inicial_filtro(req,res,moduloconsultar);
			else if (ventana.equals("1"))
				pant_resultado_consulta(req,res,moduloconsultar,crialtair,crihogan,cridesc,opcion);
			else if (ventana.equals("2"))
				pant_cuentas(req,res);
			}
		}

	// --------------------------------------------------------------------
	private int  pant_cuentas(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & pant_cuentas", EIGlobal.NivelLog.INFO);

		StringBuffer codigo=new StringBuffer("");
		String path_web=Global.WEB_APPLICATION;
		//<vswf:meg cambio de NASApp por Enlace 08122008>
		codigo.append("<FRAME SRC=\"/Enlace/");
		codigo.append(path_web);
		codigo.append("/cuentasLineasCredito?Ventana=0&opcion=1\" NAME=\"framefiltro\" SCROLLING=\"NO\" marginheight=\"0\" marginwidth=\"0\">");
		codigo.append("<FRAME SRC=\"/Enlace/");
		codigo.append(path_web);
		codigo.append("/jsp/framecuentas.jsp\" NAME=\"framecuentas\"   SCROLLING=\"AUTO\" marginheight=\"0\" marginwidth=\"0\">");
		codigo.append("<FRAME SRC=\"/Enlace/");
		codigo.append(path_web);
		codigo.append("/jsp/framebotones.jsp\" NAME=\"framebotones\"   SCROLLING=\"NO\" marginheight=\"0\" marginwidth=\"0\">");

		req.setAttribute("codigo",codigo.toString());
		evalTemplate("/jsp/pantcuentas.jsp",req,res);
		return 1;
		}

	// --------------------------------------------------------------------
	private int  pant_inicial_filtro(HttpServletRequest req, HttpServletResponse res,String modulo) throws ServletException, IOException
		{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & pant_inicial_filtro", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander.class & modulo:"+modulo, EIGlobal.NivelLog.INFO);

		if (modulo.equals(IEnlace.MDep_Inter_Abono))
			{
			req.setAttribute("titulo","<td colspan=2 class=titpag>Cuentas de Otros Bancos </td>");
			req.setAttribute("textboxserfin","");
			req.setAttribute("textboxsantander","<td class=tabtexcal>Cuenta</td><td class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
			}
		else if (session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
			{
			req.setAttribute("titulo","<td colspan=2 class=titpag>Cuentas </td>");
			req.setAttribute("textboxsantander","<td  class=tabtexcal>Cuenta</td><td  class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
			req.setAttribute("textboxserfin","");
			}
		else
			{
			req.setAttribute("titulo","<td colspan=2 class=titpag>Cuentas </td>");
			req.setAttribute("textboxsantander","<td  class=tabtexcal>Cuenta Nueva</td><td  class=tabtexcal><input type=text size=13 name=ctasantander class=tabmovtexbol >");
			req.setAttribute("textboxserfin","<td  class=tabtexcal>Cuenta Anterior   </td><td  class=tabtexcal><input type=text size=13 name=ctaserfin class=tabmovtexbol>");
			}
		evalTemplate("/jsp/framefiltro.jsp",req,res);
		return 1;
		}

	// --------------------------------------------------------------------
	private int  pant_resultado_consulta(HttpServletRequest req, HttpServletResponse res, String modulo, String caltair, String chogan,String cdescrip, String opcion) throws ServletException, IOException
		{
				HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		String archivo=session.getUserID()+".ctas";

		if (llamado_servicioCtasInteg(modulo,caltair,chogan,cdescrip,archivo,req)==true)
			{
			lectura_cuentas_para_pantalla(req,res,modulo,archivo, opcion);
			}
		else
			{
			req.setAttribute("MensajeError", "cuadroDialogo(\"Error al obtener cuentas\", 3); ");
			req.setAttribute("tramaok","");
			req.setAttribute("contadorok","");
			req.setAttribute("resultado","");
			req.setAttribute("tramadicional","");
			evalTemplate("/jsp/framecuentas.jsp",req,res);
			}
		return 1;
		}





    private int lectura_cuentas_para_pantalla(HttpServletRequest req, HttpServletResponse res, String modulo, String archivo, String opcion) throws ServletException, IOException
    {
   		EIGlobal.mensajePorTrace("***Entrando a  lectura_cuentas ", EIGlobal.NivelLog.INFO);
        StringBuffer resultado=new StringBuffer("");
		BufferedReader entrada;
		String linea = "";
		String clase="";
        String datos[] =null;
		String contratos_inversion="";
		String trama_contratos_inversion="";

		StringBuffer trama=new StringBuffer("");

		boolean continua=true;
		boolean sigue=true;
		int separadores =0;
		int contador=1;

		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		//-----//
		Hashtable cuentas;

		try
		{
			//-----//
			cuentas = consultaLineasCredito(req);

		    entrada = new BufferedReader( new FileReader( IEnlace.LOCAL_TMP_DIR + "/" + archivo));
		    if (modulo.equals(IEnlace.MDep_Inter_Abono))
   		      resultado=new StringBuffer("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
   		    else if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
		      resultado=new StringBuffer("<TR><TD class=tittabdat align=center >Cuenta Nueva</TD><TD class=tittabdat align=center >Cuenta Anterior</TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");
            else
   		      resultado=new StringBuffer("<TR><TD class=tittabdat align=center >Cuenta </TD><TD class=tittabdat align=center >Descripci&oacute;n</TD></tr>");

			while ( ( linea = entrada.readLine() ) != null )
			{
				//EIGlobal.mensajePorTrace("***linea "+linea, EIGlobal.NivelLog.INFO);
		        separadores=cuentaseparadores(linea);
			    //EIGlobal.mensajePorTrace("***separadores "+separadores, EIGlobal.NivelLog.INFO);

               continua=true;
				if (modulo.equals(IEnlace.MEnvio_pago_imp))
                {
					if (separadores!=17)
                      continua=false;
				}
				else if (modulo.equals(IEnlace.MDep_Inter_Abono))
				{
					if(separadores!=5)
                      continua=false;
				}
                else if(separadores!=8)
					continua=false;

                if (continua==true)
                {
      				 //EIGlobal.mensajePorTrace("***contador"+contador, EIGlobal.NivelLog.INFO);
					datos = desentramaC(""+separadores+"@"+linea,'@');
                    //EIGlobal.mensajePorTrace("***cuenta despues de desentramaC:"+datos[1], EIGlobal.NivelLog.INFO);

                    if ((contador % 2)== 0)
                        clase="tittabdat";
		            else
                        clase="textabdatobs";
					if (!(modulo.equals(IEnlace.MDep_Inter_Abono))&&
						!(modulo.equals(IEnlace.MEnvio_pago_imp)))
					{

						sigue=true;
						/*if ((modulo.equals(IEnlace.MPlazo_728)) ||
                            (modulo.equals(IEnlace.MPlazo_dolares)) )

                        {
                           trama_contratos_inversion=Obtener_Contratos_Inversion(datos[1]);
						   if (trama_contratos_inversion==null)
						     sigue=false;
						   else
                             contratos_inversion+ =trama_contratos_inversion;
                        }*/

                        if (sigue==true)
                        {

						  if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
							{
						      trama.append(contador);
							  trama.append("|");
							  trama.append(datos[1]);
							  trama.append("|");
							  trama.append(datos[2]);
							  trama.append("|");
							  trama.append(datos[3]);
							  trama.append("|");
							  trama.append(datos[4]);
							  trama.append("|");
							  trama.append(datos[5]);
							  trama.append("|");
							  trama.append(datos[6]);
							  trama.append("|");
							  trama.append(datos[7]);
							  trama.append("|");
							  trama.append(datos[8] );
							  trama.append((String)cuentas.get(datos[1]) );
							  trama.append("@");
							}
				          else
							{
						      trama.append(contador);
							  trama.append("|");
							  trama.append(datos[1]);
							  trama.append("| |");
							  trama.append(datos[3]);
							  trama.append("|");
							  trama.append(datos[4]);
							  trama.append("|");
							  trama.append(datos[5]);
							  trama.append("|");
							  trama.append(datos[6]);
							  trama.append("|");
							  trama.append(datos[7]);
							  trama.append("|");
							  trama.append(datos[8] );
							  trama.append((String)cuentas.get(datos[1]));
							  trama.append("@");
							}

                          /*
               			  if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
						    trama+ =contador+"|"+datos[1]+"|"+datos[2]+"|"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|"+datos[6]+"|"+datos[7]+"@";
				          else
						    trama+ =contador+"|"+datos[1]+"| |"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|"+datos[6]+"|"+datos[7]+"@";*/


					     if ( (modulo.equals(IEnlace.MMant_gral_ctas))  &&
							  (opcion.equals("4")))
							{
						      resultado.append("<TR><td class=");
							  resultado.append(clase);
							  resultado.append(" align=left >");
							  resultado.append(datos[1]);
							  resultado.append("</TD>");
							}
                         else
							{
							  resultado.append("<TR><td class=");
							  resultado.append(clase);
							  resultado.append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,");
							  resultado.append(contador);
							  resultado.append(");\">");
							  resultado.append(datos[1]);
							  resultado.append("</A></TD>");
							}

						  if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
						  {
							  if (datos[2].equals(""))
							   {
							     resultado.append("<td class=");
								 resultado.append(clase);
								 resultado.append(" align=left >&nbsp;&nbsp;</td>");
							   }
							  else
							   {
							     resultado.append("<td class=");
								 resultado.append(clase);
								 resultado.append(" align=left >");
								 resultado.append(datos[2]);
								 resultado.append("</td>");
							   }
                          }
					  	  if (datos[3].equals(""))
						    {
			                  resultado.append("<td class=");
							  resultado.append(clase);
							  resultado.append(" align=left >&nbsp;</td></tr>");
							}
			               else
							{
			                  resultado.append("<td class=");
							  resultado.append(clase);
							  resultado.append(" align=left >");
							  resultado.append(datos[3]);
							  resultado.append("</td></tr>");
							}
                           EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.INFO);
					       contador++;
						}
                    }
					else if (modulo.equals(IEnlace.MEnvio_pago_imp))
                    {
						if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
						 {
						   trama.append(contador);
						   trama.append("|");
						   trama.append(datos[1]);
						   trama.append("|");
						   trama.append(datos[2]);
						   trama.append("|");
						   trama.append(datos[3]);
						   trama.append("|");
						   trama.append(datos[4]);
						   trama.append("|");
						   trama.append(datos[5]);
						   trama.append("|");
						   trama.append(datos[6]);
						   trama.append("|");
						   trama.append(datos[7]);
						   trama.append("|");
						   trama.append(datos[8]);
						   trama.append(";");
						   trama.append(datos[9]);
						   trama.append(";");
						   trama.append(datos[10]);
						   trama.append(";");
						   trama.append(datos[11]);
						   trama.append(";");
						   trama.append(datos[12]);
						   trama.append(";");
						   trama.append(datos[13]);
						   trama.append(";");
						   trama.append(datos[14]);
						   trama.append(";");
						   trama.append(datos[15]);
						   trama.append(";");
						   trama.append(datos[16]);
						   trama.append(";");
						   trama.append(datos[17] );
						   trama.append((String)cuentas.get(datos[1]));
						   trama.append(";@");
						 }
				        else
						 {
						   trama.append(contador);
						   trama.append("|");
						   trama.append(datos[1]);
						   trama.append("| |");
						   trama.append(datos[3]);
						   trama.append("|");
						   trama.append(datos[4]);
						   trama.append("|");
						   trama.append(datos[5]);
						   trama.append("|");
						   trama.append(datos[6]);
						   trama.append("|");
						   trama.append(datos[7]);
						   trama.append("|");
						   trama.append(datos[8]);
						   trama.append(";");
						   trama.append(datos[9]);
						   trama.append(";");
						   trama.append(datos[10]);
						   trama.append(";");
						   trama.append(datos[11]);
						   trama.append(";");
						   trama.append(datos[12]);
						   trama.append(";");
						   trama.append(datos[13]);
						   trama.append(";");
						   trama.append(datos[14]);
						   trama.append(";");
						   trama.append(datos[15]);
						   trama.append(";");
						   trama.append(datos[16]);
						   trama.append(";");
						   trama.append(datos[17]);
						   trama.append((String)cuentas.get(datos[1]));
						   trama.append(";@");
						 }

   						/*
						if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
						  trama+ =contador+"|"+datos[1]+"|"+datos[2]+"|"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|"+datos[6]+"|"+datos[7]+";"+datos[8]+";"+datos[9]+";"+datos[10]+";"+datos[11]+";"+datos[12]+";"+datos[13]+";"+datos[14]+";"+datos[15]+";"+datos[16]+"@";
				        else
						  trama+ =contador+"|"+datos[1]+"| |"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|"+datos[6]+"|"+datos[7]+";"+datos[8]+";"+datos[9]+";"+datos[10]+";"+datos[11]+";"+datos[12]+";"+datos[13]+";"+datos[14]+";"+datos[15]+";"+datos[16]+"@";*/

					    resultado.append("<TR><td class=");
						resultado.append(clase);
						resultado.append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,");
						resultado.append(contador);
						resultado.append(");\">");
						resultado.append(datos[1]);
						resultado.append("</A></TD>");

						if (!session.getClaveBanco().equals(Global.CLAVE_SANTANDER))
						 {
			               resultado.append("<td class=");
						   resultado.append(clase);
						   resultado.append(" align=left >");
						   resultado.append(datos[2]);
						   resultado.append("</td>");
						 }
						if (datos[3].equals(""))
						 {
			               resultado.append("<td class=");
						   resultado.append(clase);
						   resultado.append(" align=left >&nbsp;</td></tr>");
						 }
			            else
						 {
			               resultado.append("<td class=");
						   resultado.append(clase);
						   resultado.append(" align=left >");
						   resultado.append(datos[3]);
						   resultado.append("</td></tr>");
						 }
                         EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.INFO);
					    contador++;
					}
					else
                    {

						trama.append(contador);
						trama.append("|");
						trama.append(datos[1]);
						trama.append("|");
						trama.append(datos[2]);
						trama.append("|");
						trama.append(datos[3]);
						trama.append("|");
						trama.append(datos[4]);
						trama.append("|");
						trama.append(datos[5]);
						trama.append("|");
						trama.append(traeBanco(datos[2]));
						trama.append("|");
						trama.append(traePlaza(datos[4]) );
						trama.append("|" );
						trama.append((String)cuentas.get(datos[1]));
						trama.append("@");

					    resultado.append("<TR><td class=");
						resultado.append(clase);
						resultado.append(" align=left ><A  href=\"javascript:CuentaSeleccionada(document.formacuentas.tramaok.value,");
						resultado.append(contador);
						resultado.append(");\">");
						resultado.append(datos[1]);
						resultado.append("</A></TD>");

			 		    if (datos[3].equals(""))
						 {
			               resultado.append("<td class=");
						   resultado.append(clase);
						   resultado.append(" align=left >&nbsp;</td></tr>");
						 }
			            else
						 {
			               resultado.append("<td class=");
						   resultado.append(clase);
						   resultado.append(" align=left >");
						   resultado.append(datos[3]);
						   resultado.append("</td></tr>");
						 }
                        EIGlobal.mensajePorTrace("***cuenta:"+datos[1], EIGlobal.NivelLog.INFO);
					    contador++;
                    }
					if(contador>Global.MAX_CTAS_MODULO)
                     break;
				}

	        }  //while
			entrada.close();
        } catch ( Exception e ) {
			System.out.println( "Error al traer cuentas" + e.toString() );
			e.printStackTrace();
		}

        if ((contador>0)&&(contador<=Global.MAX_CTAS_MODULO))
        {
          req.setAttribute("tramaok","\""+trama.toString()+"\"");
	      req.setAttribute("contadorok",""+contador);
          req.setAttribute("resultado",resultado.toString());
          req.setAttribute("tramadicional",contratos_inversion);
		}
		else if (contador>Global.MAX_CTAS_MODULO)
		{
          req.setAttribute("tramaok","");
	      req.setAttribute("contadorok","");
          req.setAttribute("resultado","");
          req.setAttribute("tramadicional","");
          req.setAttribute("MensajeError", "cuadroDialogo(\"Tiene que hacer una consulta mas especifica\", 3); ");
		}
		else
		{
   	      req.setAttribute("MensajeError", "cuadroDialogo(\"No se encontraron cuentas\", 3); ");
          req.setAttribute("tramaok","");
	      req.setAttribute("contadorok","");
          req.setAttribute("resultado","");
          req.setAttribute("tramadicional","");
        }
		evalTemplate("/jsp/framecuentas.jsp",req,res);
		return 1;
   }
/*
   private String Obtener_Contratos_Inversion(String cuenta)
   {
       String contratos_inversion=null;
       String aux2_contratos_inversion="";
       int contador_contratos=0;
	   int contador_contratos_considerar=0;
	   String Cuentas_operar[]=null;
	   int siguiente=0;
       String cadena_retorno_cuentas=llamada_cuentas_para_inversion7_28(cuenta);
       EIGlobal.mensajePorTrace("***cuentasSerfinSantander  "+cadena_retorno_cuentas, EIGlobal.NivelLog.ERROR);

       if (cadena_retorno_cuentas.substring(0,8).equals("POSI0000"))
       {
               try
               {
                 cadena_retorno_cuentas=cadena_retorno_cuentas.substring(9,cadena_retorno_cuentas.length());
                 contador_contratos=Integer.parseInt(cadena_retorno_cuentas.substring(0,cadena_retorno_cuentas.indexOf("|")));
                 if (contador_contratos>0)
                 {
                    cadena_retorno_cuentas=cadena_retorno_cuentas.substring(cadena_retorno_cuentas.indexOf("|")+1,cadena_retorno_cuentas.length());
                    cadena_retorno_cuentas=String.valueOf(contador_contratos*4)+"|"+cadena_retorno_cuentas;
                    Cuentas_operar = desentramaC(cadena_retorno_cuentas,'|');
                    siguiente=1;
 				    aux2_contratos_inversion="";
                    for(int i=1;i<=contador_contratos;i++)
                    {
                      if ((Cuentas_operar[siguiente].substring(0,2).equals("47"))||
						  (Cuentas_operar[siguiente].substring(0,2).equals("44"))||
						  (Cuentas_operar[siguiente].substring(0,2).equals("42")))
                      {
                        aux2_contratos_inversion+ =Cuentas_operar[siguiente]+"|";
                        contador_contratos_considerar++;
                      }
					  siguiente=siguiente+4;
                    }
                    if (contador_contratos_considerar>0)
					{
				       contratos_inversion="";
					   contratos_inversion+ =contratos_inversion+cuenta+"|"+contador_contratos_considerar+"|";
					   contratos_inversion+ =aux2_contratos_inversion;
                       contratos_inversion+ ="@";
					}

				 }
              }
			  catch(Exception e)
              {
                  EIGlobal.mensajePorTrace("***inversion7_28o.class "+e, EIGlobal.NivelLog.ERROR);
              }
		}
        EIGlobal.mensajePorTrace("***Contratos_inversion "+contratos_inversion, EIGlobal.NivelLog.INFO);
		return contratos_inversion;

   }

   private String llamada_cuentas_para_inversion7_28( String cuenta ) {
		EIGlobal.mensajePorTrace( "***cuentasSerfinSantander Entrando a " +
				"llamada_cuentas_para_inversion7_28 cuenta", 3 );

		String coderror;
		String tramaRegresoServicio = "";
		//TuxedoGlobal tuxGlobal = (TuxedoGlobal) session.getEjbTuxedo() ;
	   ServicioTux tuxGlobal = new ServicioTux();
	   tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		//TuxedoGlobal tuxGlobal = ( TuxedoGlobal ) session.getEjbTuxedo();
		//*****************************
       int reqStatus = 0;
       int nSize = 0;
   		//*****************************
	    Hashtable htResult = null;
		String POSI_OK_CODE="POSI0000";
		String trama_enviar = "";
		String buffer = "";
		trama_enviar = cuenta + "|C|";

		try {
		//*************************************
			try{
					Hashtable hs = tuxGlobal.web_red(trama_enviar);
					tramaRegresoServicio = (String) hs.get("BUFFER");
					System.out.println("trama regreso servicio: "+tramaRegresoServicio);

					//reqStatus = Integer.parseInt((String) hs.get("BUFFER"));
					}catch( java.rmi.RemoteException re ){
						re.printStackTrace();
				} catch (Exception e) {}

		//*************************************
	    	htResult = tuxGlobal.basicp( trama_enviar );

	    } catch ( java.rmi.RemoteException re ) {
	    	re.printStackTrace();
	    } catch (Exception e) {}

		coderror = ( String ) htResult.get( "COD_ERROR" );
		System.out.println( coderror );
		EIGlobal.mensajePorTrace("***inversion7_28 BASICP<<" + coderror, 3 );
		if ( coderror == null ) {
			buffer = "Problemas con BASICP";
			EIGlobal.mensajePorTrace("***inversion7_28 buffer:"+buffer, EIGlobal.NivelLog.INFO);

		} else if ( coderror.equals( POSI_OK_CODE ) ) {
				buffer = ( String ) htResult.get( "BUFFER" );
				buffer = coderror + "|" + buffer;
		} else {
				buffer = coderror;
		}
		EIGlobal.mensajePorTrace("***inversion7_28 buffer:<<"+buffer, EIGlobal.NivelLog.INFO);
		return buffer;
	}*/



	private String traeBanco( String cveBanco )
	 {
		String totales = "";
		String resBanco = cveBanco;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;

		try
		 {
			conn = createiASConn( Global.DATASOURCE_ORACLE );
		  	totales = "Select nombre_corto from comu_interme_fin where num_cecoban<>0 " +
		  			"and cve_interme='" + cveBanco + "'";
		  	psCont = conn.prepareStatement( totales );
		  	rsCont = psCont.executeQuery();
		  	rsCont.next();
		  	resBanco = rsCont.getString( 1 ).trim();

		 } catch ( Exception e )
		  {
			EIGlobal.mensajePorTrace( "MMC_Baja - traeBanco(): Ha ocurrido una excepcion: " +e.getMessage(), EIGlobal.NivelLog.ERROR );
			resBanco = cveBanco;
		  }
		 finally
		  {
			try
			 {
			   conn.close();
			   rsCont.close();
			 }catch(Exception e) {}
		  }

		return resBanco;
	}


	private String traePlaza( String cvePlaza )
	 {
		String totales = "";
		String resPlaza = cvePlaza;
		PreparedStatement psCont = null;
		ResultSet rsCont = null;
		Connection conn = null;
	    System.out.println("plaza a buscar "+cvePlaza);
		try
		 {
			conn = createiASConn( Global.DATASOURCE_ORACLE );
			totales = "Select descripcion from comu_pla_banxico where plaza_banxico='" + cvePlaza + "'";
			psCont = conn.prepareStatement( totales );
			rsCont = psCont.executeQuery();
			rsCont.next();
			resPlaza = rsCont.getString( 1 ).trim();
		} catch ( Exception e ) {
			EIGlobal.mensajePorTrace( "MMC_Baja - traePlaza(): Ha ocurrido una excepcion: " +e.getMessage(), EIGlobal.NivelLog.ERROR );
			return resPlaza = cvePlaza;
	    }
		finally
		  {
			try
			 {
			   conn.close();
			   rsCont.close();
			 }catch(Exception e) {}
		  }
	   EIGlobal.mensajePorTrace( "MMC_Baja - plaza " +resPlaza, EIGlobal.NivelLog.INFO); 		return resPlaza;
	 }


	//-------------------------------------------------------------
	private Hashtable consultaLineasCredito(HttpServletRequest req)
		{
		Hashtable cuentas = null;
		Hashtable ht;
		String trama;
		File archivo;
		BufferedReader entrada;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");


		try
			{
			// - Obtener contrato, usuario, etc
			trama = "TCT|" + session.getUserID() + "|B0AM|" + session.getContractNumber() + "|" + session.getUserID() + "|" + session.getUserProfile() + "|";

			// - Realizar consulta
			//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
			ht = tuxedo.web_red(trama);
			trama = (String)ht.get("BUFFER");
			System.out.println("<DEBUG cuentasLineasCredito.java> Tuxedo regresa: " + trama);

			// - Hacer copia remota
			trama = trama.substring(trama.lastIndexOf('/')+1,trama.length());
			if(archRemoto.copia(trama))
				{
				System.out.println("<DEBUG cuentasLineasCredito.java> Copia remota realizada (Tuxedo a local)");
				trama = Global.DIRECTORIO_LOCAL + "/" +trama;
				}
			else
				{
				System.out.println("<DEBUG cuentasLineasCredito.java> Copia remota NO realizada (Tuxedo a local)");
				}

			// - Consultar archivo
			archivo = new File(trama);
			entrada = new BufferedReader(new FileReader(archivo));

			// - Armar y retornar el hastable
			cuentas = new Hashtable();
			while((trama=entrada.readLine())!=null)
				{
				cuentas.put(trama.substring(0,trama.indexOf("|")),trama.substring(trama.indexOf("|")));
				System.out.println("<DEBUG cuentasLineasCredito> linea de cr�dito = " + trama);
				}
			}
		catch(Exception e)
			{
			System.out.println("<DEBUG cuentasLineasCredito.java> " + e);
			}
		finally
			{
			return cuentas;
			}
		}

}
