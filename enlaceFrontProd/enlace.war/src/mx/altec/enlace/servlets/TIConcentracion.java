package mx.altec.enlace.servlets;

import java.util.*;
import java.io.*;
import java.sql.SQLException;

import javax.servlet.http.*;
import javax.servlet.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.MTC_Importar;
import mx.altec.enlace.bo.TICuentaArchivo;
import mx.altec.enlace.bo.TI_CuentaDispersion;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servicios.ValidaCuentaContrato;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;




/**
** TIConcentracion is a blank servlet to which you add your own code.
*/
public class TIConcentracion extends BaseServlet
{
    /**
    ** <code>doGet</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */

    public void doGet(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }

    /**
    ** <code>doPost</code> is the entry-point of all HttpServlets.
    ** @param req the HttpServletRequest
    ** @param res the HttpServletResponse
    ** @exception javax.servlet.ServletException -- per spec
    ** @exception java.io.IOException -- per spec
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
                    throws ServletException, IOException
    {
         defaultAction(req, res);
    }


    /**
    ** <code>defaultAction</code> is the default entry-point for iAS-extended
    ** @param req the HttpServletRequest for this servlet invocation.
    ** @param res the HttpServletResponse for this servlet invocation.
    ** @exception javax.servlet.ServletException when a servlet exception occurs.
    ** @exception java.io.IOException when an io exception occurs during output.
    */
    public void defaultAction(HttpServletRequest req, HttpServletResponse res)
                   throws ServletException, IOException
    {
                String modulo="";
                String Agrega = (String)getFormParameter(req,"hdnAgrega");
                TIComunUsd tipoCta = new TIComunUsd();

                /************* Modificacion para la sesion ***************/
                HttpSession ses = req.getSession();
                BaseResource session = (BaseResource) ses.getAttribute("session");

                EIGlobal.mensajePorTrace("TIConcentracion - defaulAction(): Entrando a Tesoreria Inteligente.", EIGlobal.NivelLog.INFO);

                /*************CCB Modificacion para los valores de los rdButtons**/
				String moduloTI = (String)ses.getAttribute("moduloTI");
				String accesaMod = "";

				if(moduloTI==null) moduloTI="";

				if (moduloTI.equals("CONCENTRACION"))
					accesaMod = "1";
				else
					accesaMod = "0";

				ses.setAttribute("moduloTI","CONCENTRACION");


				EIGlobal.mensajePorTrace("TIConcentracion moduloTI->"+moduloTI , EIGlobal.NivelLog.INFO);



                EIGlobal.mensajePorTrace("TIConcentracion ->accesoMod->"+accesaMod , EIGlobal.NivelLog.INFO);
                if(!accesaMod.equals("1"))
                {
                	tipoCta.cuentasContrato(req, res);
	            }
	            else
	            {
	                if(Agrega==null) Agrega = "";
	                 EIGlobal.mensajePorTrace("TIConcentracion ->hdnAgrega -->"+Agrega , EIGlobal.NivelLog.INFO);


	                if (Agrega.equals("USD"))
	                {
	                	ses.setAttribute("ctasUSD", "Existe");
	                	ses.setAttribute("ctasMN", "");
	                }
	                if (Agrega.equals("MN"))
	                {
	                	ses.setAttribute("ctasUSD", "");
	                	ses.setAttribute("ctasMN", "Existe");
	                }
					String Divisa="";
					Divisa=(String)ses.getAttribute("Divisa");
					if(Divisa==null)Divisa="";
					if(Agrega==null){Agrega="";Divisa="";}
					if((Divisa != null || !Divisa.trim().equals("")) && moduloTI.equals("CONCENTRACION") && Agrega.equals(""))
						Agrega=Divisa;
					else
						ses.removeAttribute("Divisa");
					ses.setAttribute("Divisa", Agrega);
	            }
	            /**************************************************************/



            if(SesionValida(req,res))
                  {
                        modulo=(String) getFormParameter(req,"Modulo");
                        if(modulo==null)
                         modulo="0";

                        //***********************************************
                        //modificacion para integracon arv 8/4/02
                        //***********************************************
                        session.setModuloConsultar(IEnlace.MMant_estruc_TI);

                        if(modulo.equals("0"))
                         iniciaTesoreria(req,res);
                        else
                        if(modulo.equals("1"))
                          generaTablaEstructura(req,res);
                        else
                        if(modulo.equals("2"))
                          importaArchivo(req,res);
                        else
                        if(modulo.equals("3"))
                          altaEstructura(req,res);
                        else
                        if(modulo.equals("4"))
                          modificaEstructura(req,res);
                        else
                        if(modulo.equals("5"))
                          borraEstructura(req,res);
                        else
                        if(modulo.equals("6"))
                          editaEstructura(req,res);
                        else
                        if(modulo.equals("7"))
                          iniciaCopia(req,res);
                        else
                        if(modulo.equals("8"))
                          eliminaRegistro(req,res);
                        else
                        if(modulo.equals("9"))
                          copiaEstructura(req,res);
                        else
                         despliegaMensaje("Su transacci&oacute;n no puede ser atendida en este momento.","Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
                  }

                EIGlobal.mensajePorTrace("TIConcentracion - defaulAction(): Saliendo de Tesoreria Inteligente.", EIGlobal.NivelLog.INFO);
    }

/*************************************************************************************/
/******************************************************** iniciaTesoreria (Modulo=0) */
/*************************************************************************************/
   public void iniciaTesoreria(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
           String tipoCuenta="";
           String nombreArchivo="";
           String tipoArbol="C";

           String Trama="";
           String Result="";
           Hashtable hs = null;

           String arcLinea="";
           String cadenaTCT="";
		   String aux_cadenaTCT="";
           String mensajeError="";

           String tipoOperacion="";

           boolean comuError=false;

           int pos=0;
           int reg=0;

           EI_Tipo TCTArchivo=new EI_Tipo(this);
           EI_Tipo Anterior=new EI_Tipo(this);

           /************* Modificacion para la sesion ***************/
           HttpSession ses = req.getSession();
           BaseResource session = (BaseResource) ses.getAttribute("session");
           EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

           EIGlobal.mensajePorTrace("TIConcentracion - iniciaTesoreria(): Iniciando pantalla de Tesoreria Inteligente.", EIGlobal.NivelLog.INFO);

           ServicioTux TuxGlobal = new ServicioTux();
           //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
           //CCB - Variable para trama para manejo de USD / MN
           String Divisa = (String)ses.getAttribute("Divisa");
           if(Divisa==null || Divisa.equals("")){
        	   Divisa="ALL";
		   }

		              EIGlobal.mensajePorTrace("La divisa  or primera vez en concentracion es null  -->"+Divisa, EIGlobal.NivelLog.INFO);
/*			   if (ses.getAttribute("ctasUSD").equals("Existe"))
			   {
			  	 if(ses.getAttribute("ctasMN").equals("Existe"))
			  	 {
			 		Divisa = "MN";
			  	 }
			  	 else
			  	 {
			   		Divisa = "USD";
			  	 }
			   }
			   else
			   {
			  	 Divisa="MN";
			   }
		   }*/

           //String Divisa = req.getParameter("hdnDivisa")!=null?req.getParameter("hdnDivisa"):"ALL";

           tipoOperacion="TI02";

           Trama="2EWEB|"+session.getUserID8()+"|"+tipoOperacion+"|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@|"+Divisa+"|";
           EIGlobal.mensajePorTrace("TIConcentracion - iniciaTesoreria(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

                try
                 {
                   hs=TuxGlobal.web_red(Trama);
           Result=(String) hs.get("BUFFER");
                 }
                catch( java.rmi.RemoteException re )
                 {
                   re.printStackTrace();
                   Result=null;
                 }
                catch (Exception e) {}

           EIGlobal.mensajePorTrace("TIConcentracion - iniciaTesoreria(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);
           String nA = "";
           if(Result==null || Result.equals("null"))
            {
                  EIGlobal.mensajePorTrace("TIConcentracion - iniciaTesoreria(): El servicio no funciono correctamente: (null) ", EIGlobal.NivelLog.INFO);

                  //######################## Solo es temporal
                  //Result="ERROR";
                  //cadenaTCT="ERROR";
                  //######################## Solo es temporal

                  comuError=true;
                  mensajeError="Su transacci&oacute;n no puede ser atendida en este momento.<br>Por favor intente mas tarde.";
                }
           else
                {
                  nombreArchivo=Result.substring(Result.lastIndexOf('/')+1,Result.length());

                  ArchivoRemoto archR = new ArchivoRemoto();
                  if(!archR.copia(nombreArchivo))
                        EIGlobal.mensajePorTrace( "TIConcentracion - iniciaTesoreria(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
                  else
                    EIGlobal.mensajePorTrace( "TIConcentracion - iniciaTesoreria(): Copia remota OK.", EIGlobal.NivelLog.INFO);

                  nA = nombreArchivo;
                  nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArchivo;
                  EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
                  EIGlobal.mensajePorTrace( "TIConcentracion - iniciaTesoreria(): Path NombreArchivo."+nombreArchivo, EIGlobal.NivelLog.INFO);

                   if(ArcEnt.abreArchivoLectura())
                        {
                          arcLinea=ArcEnt.leeLinea();
                          if(arcLinea.substring(0,2).equals("OK"))
                                cadenaTCT=arcLinea;
                          else
                           {
                                 comuError=true;
                                 if(arcLinea.substring(0,5).trim().equals("ERROR"))
                                   mensajeError="Su transacci&ocaute;n no puede ser atendida. Por favor intente mas tarde.";
                                 else
                                   {
                                         //mensajeError="La informacion no se recibio correctamente. <br>Intente mas tarde.";
                                         EIGlobal.mensajePorTrace( "TIConcentracion - iniciaTesoreria(): No se reconocio el encabezado. "+arcLinea, EIGlobal.NivelLog.INFO);
                                     mensajeError=arcLinea.substring(16,arcLinea.length());
                                         comuError=true;
                                   }
                           }
                          ArcEnt.cierraArchivo();
                        }
                   else
                        {
                          EIGlobal.mensajePorTrace( "TIConcentracion - iniciaTesoreria(): Por alguna razon no pudo abrir el archivo", EIGlobal.NivelLog.INFO);
                          //########################  Solo para hacer pruebas
                          /*
                          cadenaTCT ="OK      63474   @TEIN0000@Transaccion Exitosa@4";
                          cadenaTCT+="@60501058867@80000648376@C@65500144939@DESCRIPCION 3";
                          cadenaTCT+="@60501058868@80000648376@C@65500144939@DESCRIPCION 2";
                          cadenaTCT+="@57003439833@80000648376@C@65500144939@DESCRIPCION 1";
                          cadenaTCT+="@65500144939@80000648376@C@0@DESCRIPCION 0 PADRE";
                          cadenaTCT+="@4";
                          cadenaTCT+="@60501058867@80000648376@0@2@1000.000000@0.000000@08:00";
                          cadenaTCT+="@60501058868@80000648376@6@2@20000.000000@0.000000@09:00";
                          cadenaTCT+="@57003439833@80000648376@6@2@30000.000000@0.000000@10:00";
                          cadenaTCT+="@57003439833@80000648376@6@2@40000.000000@0.000000@11:00";
                          cadenaTCT+="@";
                          //########################  Solo para hacer pruebas
                          */
                          comuError=true;
                          mensajeError="No se pudo obtener la informaci&oacute;n del archivo. Por favor intente mas tarde.";
                        }
                 }

           if(comuError)
                 despliegaMensaje(mensajeError,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
           else
                {
                   EIGlobal.mensajePorTrace("TIConcentracion - iniciaTesoreria(): El archivo contenia "+ cadenaTCT, EIGlobal.NivelLog.INFO);
                   boolean estructura=true;
                   if(cadenaTCT.substring(0,2).equals("OK"))
                        {
   					       pos=noA(3,cadenaTCT);
						   Divisa = cadenaTCT.substring(pos,cadenaTCT.indexOf("@",pos));
						   if(Divisa!=null && Divisa.trim()!=""){
							  ses.setAttribute("Divisa",Divisa);
						   }
						   EIGlobal.mensajePorTrace("------------ La divisa  que me devuelve en la trama del archivo es ----------- " + Divisa, EIGlobal.NivelLog.INFO);

						   aux_cadenaTCT = cadenaTCT.substring(0,pos);
						   EIGlobal.mensajePorTrace("La cadena aux_cadenaTCT  ==>" + aux_cadenaTCT + "<", EIGlobal.NivelLog.INFO);
                           pos=noA(4,cadenaTCT);
						   aux_cadenaTCT = aux_cadenaTCT + cadenaTCT.substring(pos,cadenaTCT.length());
						   EIGlobal.mensajePorTrace("La cadena aux_cadenaTCT  ==>" + aux_cadenaTCT + "<", EIGlobal.NivelLog.INFO);
                           reg=Integer.parseInt(cadenaTCT.substring(pos,cadenaTCT.indexOf("@",pos)));
						   cadenaTCT=aux_cadenaTCT;
 						  EIGlobal.mensajePorTrace("------------ Los reg son ----------- " + reg, EIGlobal.NivelLog.INFO);
                          if(reg>0)
                           {
					EIGlobal.mensajePorTrace("La cadenaTCT es : >" + cadenaTCT + "<", EIGlobal.NivelLog.INFO);
                                 Anterior=armaTramas(cadenaTCT,"C",false);
                                 TCTArchivo.iniciaObjeto(tramaConcentracion(Anterior));
                           }
                          else
                           {
                                 estructura=false;
                                 cadenaTCT+="No hay Registros";
                           }
                        }
                   else
                         estructura=false;

                   //################### Resultado del servicio
                   req.setAttribute("Consulta","<br><br>"+cadenaTCT);
                   //################### Resultado del servicio

                   if(estructura)
                        {
                          req.setAttribute("Alta","NO");
                          req.setAttribute("Modificar","OK");
                        }
                   else
                        {
                          req.setAttribute("Alta","OK");
                      req.setAttribute("Modificar","NO");
                        }

                   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
                   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
                   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
                   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

                   req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
                   req.setAttribute("MenuPrincipal", session.getStrMenu());
                   req.setAttribute("newMenu", session.getFuncionesDeMenu());
                   req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29000h",req));

                   req.setAttribute("Archivo",moduloImportar("true"));
                   req.setAttribute("TramasCuentas",TCTArchivo.strOriginal);
                   req.setAttribute("Tabla",generaTabla("Estructura de concentraci&oacute;n",6,TCTArchivo,req));

                   //Facultades...
                   req.setAttribute("AgregaEdita", (session.getFacultad(session.FacCapConcen))?"true":"false");
                   req.setAttribute("AltaModificaBorra",(session.getFacultad(session.FacActParam))?"true":"false");
                   req.setAttribute("Copiar",(session.getFacultad(session.FacCopEstr))?"true":"false");


                   //TODO BIT CU3141 inicio de flujo
                    /**
			 		 * VSWF - FVC - I
			 		 * 17/Enero/2007
			 		 */
                   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                    try {
						BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
						BitaHelper bh = new BitaHelperImpl(req, session, ses);
						BitaTransacBean bt = new BitaTransacBean();

						if(getFormParameter(req,BitaConstants.FLUJO) != null)
							bh.incrementaFolioFlujo((String)getFormParameter(req,BitaConstants.FLUJO));
						else
							bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));

						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_ENTRA);
						bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
						bt.setServTransTux("TI02");
						bt.setTipoMoneda((Divisa == null)?" ":Divisa);
						bt.setNombreArchivo((nA == null)?" ":nA);
						/*BMB-I*/
						if(Result!=null){
			    			if(Result.substring(0,2).equals("OK")){
			    				bt.setIdErr("TI020000");
			    			}else if(Result.length()>8){
				    			bt.setIdErr(Result.substring(0,8));
				    		}else{
				    			bt.setIdErr(Result.trim());
				    		}
			    		}
						/*BMB-F*/

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
                   }
			 		/**
			 		 * VSWF - FVC - F
			 		 */

                   evalTemplate( "/jsp/TIConcentracion.jsp", req, res );
                }
        }

/*************************************************************************************/
/************************************************** generaTablaEstructura (Modulo=1  */
/*************************************************************************************/
  void generaTablaEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          String strTramaCuenta="";
          String tramasCuentas="";

          EI_Tipo Niveles=new EI_Tipo(this);
          EI_Tipo CuentasOrdenadas=new EI_Tipo(this);

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

          EIGlobal.mensajePorTrace("TIConcentracion - generaTablaEstructura(): Armando estructura y tabla.", EIGlobal.NivelLog.INFO);

          String Divisa = (String)ses.getAttribute("Divisa");

          EIGlobal.mensajePorTrace("El valor de la divisa en  generaTablaEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);


          //Trama de la cuenta que se va a agragar
          strTramaCuenta=(String) getFormParameter(req,"Trama");
          //Todas las tramas anteriores
          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");

          EIGlobal.mensajePorTrace("TIConcentracion - generaTablaEstructura(): Validando informacion.", EIGlobal.NivelLog.INFO);

          tramasCuentas+=strTramaCuenta;
          Niveles.iniciaObjeto(tramasCuentas);
          CuentasOrdenadas.iniciaObjeto(tramaConcentracion(Niveles));

          EIGlobal.mensajePorTrace("TIConcentracion - generaTablaEstructura(): Generando Pantalla.", EIGlobal.NivelLog.INFO);

          //##################### Trama de la cuenta seleccionada
          strTramaCuenta+="<p><BR>"+generaTabla("Estructura de concentraci&oacute;n",0,Niveles,req);
          req.setAttribute("Consulta",strTramaCuenta);
          //##################### Trama de la cuenta seleccionada

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
          req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
          req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
          req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
          req.setAttribute("MenuPrincipal", session.getStrMenu());
          req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29000h",req));

          req.setAttribute("Archivo",moduloImportar("true"));
          req.setAttribute("Tabla",generaTabla("Estructura de concentraci&oacute;n",6,CuentasOrdenadas,req));
          req.setAttribute("TramasCuentas",CuentasOrdenadas.strOriginal);
          req.setAttribute("Alta",(String) getFormParameter(req,"Alta"));
          req.setAttribute("Modificar",(String) getFormParameter(req,"Modificar"));

          //Facultades...
          req.setAttribute("AgregaEdita", (session.getFacultad(session.FacCapConcen))?"true":"false");
          req.setAttribute("AltaModificaBorra",(session.getFacultad(session.FacActParam))?"true":"false");
          req.setAttribute("Copiar",(session.getFacultad(session.FacCopEstr))?"true":"false");

          evalTemplate( "/jsp/TIConcentracion.jsp", req, res );
        }

/*************************************************************************************/
/********************************************************* importaArchivo (Modulo=2) */
/*************************************************************************************/
  void importaArchivo(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          String linea="";
          String lineaArchivo="";
          String errores="";
          String header="";
          String footer="";
          String nombreArchivo="";
          String strArchivo="";
          String strErr="";
          String tramasCuentas="";

          String contratoArc="";
          String tipoArbolArc="";
          String[] datosCta=null;
          String cveProd="";
          String arcTrama="";

          String contrato="";

          int cont=0;
          int contOk=0;
          int existeError=0;

          boolean arcErrores=false;
          boolean importacion=true;

          EI_Tipo Niveles=new EI_Tipo(this);
          EI_Tipo CuentasOrdenadas=new EI_Tipo(this);
          MTC_Importar ArchImp = new MTC_Importar();
          TICuentaArchivo a=new TICuentaArchivo();
          ValidaCuentaContrato valCtas = null;

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

          contrato=session.getContractNumber();
          //Todas las tramas anteriores
          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");

          EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Entrando a importacion de archivo.", EIGlobal.NivelLog.INFO);

  		  String Divisa = (String)ses.getAttribute("Divisa");

           EIGlobal.mensajePorTrace("El valor de la divisa en  importarArchivo() ..... " + Divisa, EIGlobal.NivelLog.INFO);

           try
            {
                  strArchivo += new String( getFileInBytes(req));
            }catch(Exception e)
             {
                   strArchivo+="";
             }

                  if(strArchivo.length()>0)
                   {
                         cont=0;
                         contOk=1;

                         try
                          {
                        	 	valCtas = new ValidaCuentaContrato();
                                BufferedReader strOut=new BufferedReader(new StringReader(strArchivo));
                                EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Asignando buffer.", EIGlobal.NivelLog.INFO);

                                EI_Exportar ArcErr=new EI_Exportar(IEnlace.DOWNLOAD_PATH+"TIError.html");

                                if(ArcErr.creaArchivo())
                                 {
                                   arcErrores=true;
                                   header="<html>";
                                   header+="<head>\n<title>Estatus</title>\n";
                                   header+="<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>";
                                   header+="</head>";
                                   header+="<body bgcolor='white'>";
                                   header+="<form>";
                                   header+="<table border=0 width=420 class='textabdatcla' align=center>";
                                   header+="<tr><th class='tittabdat'>informacion del Archivo Importado</th></tr>";
                                   header+="<tr><td class='tabmovtex1' align=center>";

                                   header+="<table border=0 align=center><tr><td class='tabmovtex1' align=center width=>";
                                   header+="<img src='/gifs/EnlaceMig/gic25060.gif'></td>";
                                   header+="<td class='tabmovtex1' align='center'><br>No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los siguientes errores.<br><br>";
                                   header+="</td></tr></table>";

                                   header+="</td></tr>";
                                   header+="<tr><td class='tabmovtex1'>";

                                   if(!ArcErr.escribeLinea(header))
                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Algo salio mal escribiendo: "+strErr, EIGlobal.NivelLog.INFO);
                                 }
                                else
                                  arcErrores=false;

                                //llamado_servicioCtasInteg(IEnlace.MMant_estruc_TI," "," "," ",session.getUserID()+".ambcc",req);

                while((linea=strOut.readLine()) != null)
                             {
                                   if( linea.trim().equals(""))
                                     linea = strOut.readLine();
                                   if( linea == null )
                                          break;

                                   if(cont==0)
                                        {
                                          if(linea.length()>16 && linea.length()<18)
                                                {
                                                  contratoArc=linea.substring(0,16).trim();
                                                  tipoArbolArc=linea.substring(16,linea.length()).trim();
                                                  EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Contrato "+contratoArc+" tipo "+tipoArbolArc, EIGlobal.NivelLog.INFO);

                                                  if(!contratoArc.trim().equals(contrato))
                                                   {
                                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): El contrato no es el mismo.", EIGlobal.NivelLog.INFO);
                                                         importacion=false;
                                                         req.setAttribute("ArchivoFallo","cuadroDialogo('El contrato especificado en el archivo no corresponde con el actual. "+contrato+"',1);");
                                                   }
                                                  else
                                                  if(!tipoArbolArc.trim().equals("C"))
                                                   {
                                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): No es de concentracion.", EIGlobal.NivelLog.INFO);
                                                         importacion=false;
                                                         req.setAttribute("ArchivoFallo","cuadroDialogo('El archivo no es de Concentraci&oacute;n.',1);");
                                                   }
                                                }
                                           else
                                                {
                                                   //################# Error en el encabezado...
                                                   EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Error en el encabezado, el archivo no se abrira.", EIGlobal.NivelLog.INFO);
                                                   importacion=false;
                                                   req.setAttribute("ArchivoFallo","cuadroDialogo('El encabezado del archivo no cumple con el formato.',1);");
                                                }
                                        }
                                   else
                                        {
                                          ArchImp.formateaLinea(linea,Divisa);
                                          EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Formateando linea: "+cont+ " cod Err "+ArchImp.err, EIGlobal.NivelLog.INFO);

                                           if(ArchImp.err==0)
                                                {
                                                  existeError=0;
                                                  errores="";

                                                  //datosCta=BuscandoCtaInteg(ArchImp.cuentaHija,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambcc",IEnlace.MMant_estruc_TI);
                                                    datosCta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
                          																	  session.getUserID8(), 
                          																	  session.getUserProfile(),
                          																	  ArchImp.cuentaHija.trim(),
                          																	  IEnlace.MMant_estruc_TI);
                                                  if(datosCta==null)
                                                   {errores+="<br><DD><LI>La cuenta Hija No Existe. "+ArchImp.cuentaHija; existeError++;}
                                                  else
                                                   {ArchImp.cuentaHija=datosCta[1]; cveProd=datosCta[3];}

                                                  if(!ArchImp.cuentaPadre.trim().equals("0"))
                                                        {
                                                      //datosCta=BuscandoCtaInteg(ArchImp.cuentaPadre,IEnlace.LOCAL_TMP_DIR + "/"+session.getUserID()+".ambcc",IEnlace.MMant_estruc_TI);
	                                                    datosCta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
																  								  session.getUserID8(),  
																								  session.getUserProfile(),
																								  ArchImp.cuentaPadre.trim(),
																								  IEnlace.MMant_estruc_TI);
                                                          if(datosCta==null)
                                                                {errores+="<br><DD><LI>La cuenta Padre No Existe. "+ArchImp.cuentaPadre; existeError++;}
                                                          else
                                                                {ArchImp.cuentaPadre=datosCta[1]; cveProd=datosCta[3];}
                                                        }
                                                   else
                                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Es cuenta padre ", EIGlobal.NivelLog.INFO);

                                                  if(!ArchImp.verificaSaldoOp1())
                                                         {errores+="<br><DD><LI>El saldo Operativo no es correcto. "+ArchImp.saldoOp1; existeError++;}
                                                  if(!ArchImp.verificaSaldoOp1())
                                                         {errores+="<br><DD><LI>La inversi&oacute;n autom&aacute;tica no es correcta. "+ArchImp.saldoOp2; existeError++;}

                                                  EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Se verificaron los datos con errores: "+existeError, EIGlobal.NivelLog.INFO);

                                                  if(existeError==0)
                                                        {
                                                          arcTrama=ArchImp.cuentaHija+"|";
                                                          arcTrama+="NoSeUsa|";
                                                          arcTrama+=cambiaPeriodicidad(Integer.toString(ArchImp.diaCon))+"|";
                                                          arcTrama+=ArchImp.horaCon+"|";
                                                          arcTrama+=Double.toString(ArchImp.saldoOp1)+"|";
                                                          arcTrama+=ArchImp.invAuto+"|";
                                                          arcTrama+=Double.toString(ArchImp.saldoOp2)+"|";
                                                          arcTrama+=ArchImp.cuentaPadre+"|";
                                                          if(ArchImp.cuentaPadre.equals("0"))
                                                            arcTrama+="0|";
                                                          else
                                                                arcTrama+="1|";
                                                          arcTrama+="0|";
                                                          arcTrama+=ArchImp.descripcion+"|";
                                                          arcTrama+=cveProd+"|";
                                                          arcTrama+=Integer.toString(ArchImp.nivelArbol)+"@";
                                                      a.actualizaCuenta(arcTrama);

                                                          contOk++;
                                                        }
                                                  else
                                                        {
                                                          if(arcErrores)
                                                                {
                                                                  strErr="\n<br><b>Linea: "+Integer.toString(cont)+".</b>"+errores;
                                                                  if(!ArcErr.escribeLinea(strErr))
                                                                          EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Algo salio mal escribiendo: "+strErr, EIGlobal.NivelLog.INFO);
                                                                }
                                                        }
                                                }

                                               else if(ArchImp.err==1)
                                                {strErr="<br><b>Linea: "+cont+".</b> <font color=red>Error, Horario No valido para Sabado.</font>";
                                                if(!ArcErr.escribeLinea(strErr))
                                                       EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Error, Horario No valido para Sabado.: "+strErr, EIGlobal.NivelLog.INFO);


                                                }
                                                 else if(ArchImp.err==2)
                                                {strErr="<br><b>Linea: "+cont+".</b> <font color=red>Error, Domingo es un dia no valido.</font>";
                                                if(!ArcErr.escribeLinea(strErr))
                                                       EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Domingo es un dia no valido.: "+strErr, EIGlobal.NivelLog.INFO);


                                                }
                                           else
                                                {
                                                  if(arcErrores)
                                                        {
                                                          strErr="<br><b>Linea: "+cont+".</b> <font color=red>No cumple con el Formato.</font>";
                                                          if(!ArcErr.escribeLinea(strErr))
                                                                 EIGlobal.mensajePorTrace("MDI_Interbancario - generaTablaDepositos(): Algo salio mal escribiendo: "+strErr, EIGlobal.NivelLog.INFO);
                                                         }
                                                }
                                        }
                               cont++;
                 }

                                if(arcErrores)
                                 {
                                   footer="<br></td></tr>";
                                   footer+="</table>";
                                   footer+="<table border=0 align=center >";
                                   footer+="<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>";
                                   footer+="</table>";
                                   footer+="</form>";
                                   footer+="</body>\n</html>";

                                   if(!ArcErr.escribeLinea(footer))
                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Algo salio mal escribiendo: "+strErr, EIGlobal.NivelLog.INFO);
                                   ArcErr.cierraArchivo();

                                   ArchivoRemoto archR = new ArchivoRemoto();
                                   if(!archR.copiaLocalARemoto("TIError.html","WEB"))
                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): No se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
                                   else
                                         EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Si se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
                                 }

                          }catch(Exception e)
                           {
                                 //Problemas con el archivo.
                                 EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): Excepcion, Error: "+e.getMessage(), EIGlobal.NivelLog.INFO);
                                 importacion=false;
                                 req.setAttribute("ArchivoFallo","cuadroDialogo('El archivo especificado no tiene el formato correcto o tiene demasiados errores.',1);");
                           }finally{
	                   	    	try{
	                   	    		valCtas.closeTransaction();
	                   	    	}catch (Exception e) {
	                   	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
	                   			}
                           }
                   }
                  else
                   {
                     req.setAttribute("ArchivoFallo","cuadroDialogo('No existe el archivo o esta vacio. Por favor verifiquelo.',1);");
                     importacion=false;
                   }

          if(contOk!=cont && importacion)
           {
                 importacion=false;
                 req.setAttribute("ArchivoErr","true;");
           }

          if(importacion)
                {
                  a.cadenaCuentas();
                  tramasCuentas=a.Todas.strOriginal;
                }

          Niveles.iniciaObjeto(tramasCuentas);
          CuentasOrdenadas.iniciaObjeto(tramaConcentracion(Niveles));

          //#####################
          arcTrama+="<p><BR>"+generaTabla("Estructura de concentraci&oacute;n",0,Niveles,req);
          req.setAttribute("Consulta",arcTrama+"<BR>Lineas "+cont+" Correctas "+contOk);
          //#####################

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
          req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
          req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
          req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
          req.setAttribute("MenuPrincipal", session.getStrMenu());
          req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29000h",req));

          req.setAttribute("Archivo",moduloImportar("true"));
          req.setAttribute("Tabla",generaTabla("Estructura de concentraci&oacute;n",6,CuentasOrdenadas,req));
          req.setAttribute("TramasCuentas",tramasCuentas);
          req.setAttribute("Alta",(String) getFormParameter(req,"Alta"));
          req.setAttribute("Modificar",(String) getFormParameter(req,"Modificar"));

          //Facultades...
          req.setAttribute("AgregaEdita", (session.getFacultad(session.FacCapConcen))?"true":"false");
                   req.setAttribute("AltaModificaBorra",(session.getFacultad(session.FacActParam))?"true":"false");
                   req.setAttribute("Copiar",(session.getFacultad(session.FacCopEstr))?"true":"false");

                   //TODO BIT CU3146 importar archivo

                    /**
         	 		 * VSWF - FVC - I
         	 		 * 17/Enero/2007
         	 		 */
                   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
                   	try {
						BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
						BitaHelper bh = new BitaHelperImpl(req, session, ses);
						BitaTransacBean bt = new BitaTransacBean();
						bt = (BitaTransacBean)bh.llenarBean(bt);
						bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_IMPORTA_ARCH);
						bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
						bt.setNombreArchivo((session.getUserID8() == null)?" ":session.getUserID8() + ".ambcc");  

						bt.setTipoMoneda((Divisa == null)?" ":Divisa);

						BitaHandler.getInstance().insertBitaTransac(bt);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
                   }
         	 		/**
         	 		 * VSWF - FVC - F
         	 		 */

          evalTemplate( "/jsp/TIConcentracion.jsp", req, res );
        }

/*************************************************************************************/
/******************************************************** altaEstructura (Modulo=3)  */
/*************************************************************************************/
   public void altaEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          String tramasCuentas="";
          String strMensaje="";

          String Trama="";
          String Result="";
          Hashtable hs = null;

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");

          EIGlobal.mensajePorTrace("TIConcentracion - altaEstructura(): Entrando a Alta de estructura.", EIGlobal.NivelLog.INFO);

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  altaEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);

          EI_Tipo Cuentas=new EI_Tipo(this);
          ServicioTux TuxGlobal = new ServicioTux();
          //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");
          Cuentas.iniciaObjeto(tramasCuentas);

          if(!generaArchivoParaServicio(Cuentas,req))
           {
                 despliegaMensaje("No se pudo generar el archivo de intercambio.","Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
                 return;
           }

          String strArchivo=session.getUserID8()+".tic"; 
          ArchivoRemoto archR = new ArchivoRemoto();
          if(!archR.copiaLocalARemoto(strArchivo))
                EIGlobal.mensajePorTrace("TIConcentracion - altaEstructura(): No se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
          else
                EIGlobal.mensajePorTrace("TIConcentracion - altaEstructura(): Si se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
		  //CCB - Jan 17th 2004 - Modificacion de la trama para agregar estructuras en USD
          //Trama="1EWEB|"+session.getUserID()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+strArchivo+"@";
          EIGlobal.mensajePorTrace("CCBDivisa--> " + Divisa, EIGlobal.NivelLog.INFO);

          Trama="1EWEB|"+session.getUserID8()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+strArchivo+"@|"+Divisa;
          //Trama="1EWEB|"+session.getUserID()+"|TI01|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+strArchivo+"@"+"|"+Divisa;

          EIGlobal.mensajePorTrace("TIConcentracion - altaEstructura(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);
          ValidaCuentaContrato valCtas = null;
          String datoscta[]=null;
          try
           {

			valCtas = new ValidaCuentaContrato();

        	//YHG Agrega validacion de cuentas
       	   String []tramaCompleta=tramasCuentas.split("@"); //separa tramas
       	  for(int i=0;i<tramaCompleta.length;i++)//ciclo de validacion de cuentas
       	  { EIGlobal.mensajePorTrace("tramaCompleta["+i+"] ..... " + tramaCompleta[i], EIGlobal.NivelLog.INFO);
       	    EIGlobal.mensajePorTrace("tramaCompleta["+i+"].indexOf('|') ..... " + (tramaCompleta[i].indexOf("|")), EIGlobal.NivelLog.INFO);
       		  String cuentaValidar=tramaCompleta[i].substring(0,tramaCompleta[i].indexOf("|"));
       		  EIGlobal.mensajePorTrace("TIConcentracion - cuentaValidar: "+cuentaValidar, EIGlobal.NivelLog.INFO);




			  datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
															  session.getUserID8(), 
													          session.getUserProfile(),
													          cuentaValidar.trim(),
													          IEnlace.MMant_estruc_TI);
				 EIGlobal.mensajePorTrace("TIConcentracion - datoscta: "+datoscta, EIGlobal.NivelLog.INFO);



					  if(datoscta==null){
			        	  EIGlobal.mensajePorTrace("Rompe CICLO"+strMensaje, EIGlobal.NivelLog.ERROR);
			        	  break;

				 }
       	  }

       	  EIGlobal.mensajePorTrace("TIConcentracion - CONTINUA FLUJO: ", EIGlobal.NivelLog.INFO);

       	  if(datoscta!=null){
             hs=TuxGlobal.web_red(Trama);
             Result=(String) hs.get("BUFFER");
       	     }
           }
          catch( java.rmi.RemoteException re )
           {
             re.printStackTrace();
             Result=null;
           }
          catch (Exception e) {e.printStackTrace();}
  	    finally
		{
	    	try{
	    		valCtas.closeTransaction();
	    	}catch (Exception e) {
	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
			}


	    }
          EIGlobal.mensajePorTrace("TIConcentracion - altaEstructura(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);
          if(datoscta==null)
        	  strMensaje="Error H701:Cuenta Inv&aacute;lida";
          else if(Result==null || Result.equals("null"))
                strMensaje="Su transacci&oacute;n no puede ser atendida en este momento. Intenete mas tarde.";
          else
           {
                 //OK         91195TEIN0000@Transaccion Exitosa@
                 //OK         96155TEIN0001@Relacion ya Existente@
                 //TCT_0063   97300Servicio de Tesoreria Inteligente no esta disponible
                 if(Result.substring(0,2).equals("OK"))
                   {
                         if(Result.substring(16,24).trim().equals("TEIN0000"))
                           strMensaje="La estructura se dio de Alta satisfactoriamente ";
                         else if(Result.substring(16,24).trim().equals("TEIN0006"))
                           strMensaje="Error: La estructura tiene cuentas con divisas diferentes!";
                         else
                           strMensaje=Result.substring(Result.indexOf("@")+1,Result.lastIndexOf("@"));
                   }
                 else
                   strMensaje="La estructura No se pudo dar de alta. <br>"+Result.substring(16,Result.length());
           }

          //TODO BIT CU3141 Alta estructuras

          /**
	 		 * VSWF - FVC - I
	 		 * 17/Enero/2007
	 		 */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
	 		try {
	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, session, ses);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI01);
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				bt.setNombreArchivo((strArchivo == null)?" ":strArchivo);
				if(Result != null){
					if(Result.length() >= 8)
						bt.setIdErr(Result.substring(0,8));
					else
						bt.setIdErr(Result);
				}
				bt.setTipoMoneda((Divisa == null)?" ":Divisa);

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
	 		/**
	 		 * VSWF - FVC - F
	 		 */


          despliegaMensaje(strMensaje,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
        }

/*************************************************************************************/
/**************************************************** modificaEstructura (Modulo=4)  */
/*************************************************************************************/
   public void modificaEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          String tramasCuentas="";
          String strMensaje="";

          String Trama="";
          String Result="";
          Hashtable hs = null;

          EIGlobal.mensajePorTrace("TIConcentracion - modificaEstructura(): Entrando a Modifica Estructura.", EIGlobal.NivelLog.INFO);

          EI_Tipo Cuentas=new EI_Tipo(this);

          //String Divisa = req.getParameter("hdnDivisa")!=null?req.getParameter("hdnDivisa"):"SN";

          ServicioTux TuxGlobal = new ServicioTux();
          //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  modificaEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);

          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");
          Cuentas.iniciaObjeto(tramasCuentas);

          if(!generaArchivoParaServicio(Cuentas,req))
           {
                 despliegaMensaje("No se pudo generar el archivo de intercambio.","Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
                 return;
           }

          String strArchivo=session.getUserID8()+".tic"; 
          ArchivoRemoto archR = new ArchivoRemoto();
          if(!archR.copiaLocalARemoto(strArchivo))
                EIGlobal.mensajePorTrace("TIConcentracion - modificaEstructura(): No se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);
          else
                EIGlobal.mensajePorTrace("TIConcentracion - modificaEstructura(): Si se pudo generar copia remota de errores", EIGlobal.NivelLog.INFO);

          Trama="1EWEB|"+session.getUserID8()+"|TI04|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+IEnlace.DOWNLOAD_PATH+strArchivo+"@|"+Divisa+"|";

          EIGlobal.mensajePorTrace("TIConcentracion - modificaEstructura(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);
          ValidaCuentaContrato valCtas = null;
          String datoscta[] = null;
          try
           {
        	//YHG Agrega validacion de cuentas
        	  valCtas = new ValidaCuentaContrato();
          	   String []tramaCompleta=tramasCuentas.split("@"); //separa tramas
          	  for(int i=0;i<tramaCompleta.length;i++)//ciclo de validacion de cuentas
          	  { EIGlobal.mensajePorTrace("tramaCompleta["+i+"] ..... " + tramaCompleta[i], EIGlobal.NivelLog.INFO);
          	    //EIGlobal.mensajePorTrace("tramaCompleta["+i+"].indexOf('|') ..... " + (tramaCompleta[i].indexOf("|")), EIGlobal.NivelLog.INFO);
          	    if(tramaCompleta[i].indexOf("|")>-1){
          		  String cuentaValidar=tramaCompleta[i].substring(0,tramaCompleta[i].indexOf("|"));
          		  EIGlobal.mensajePorTrace("TIConcentracion -modificaEstructura  cuentaValidar: "+cuentaValidar, EIGlobal.NivelLog.INFO);
          		  datoscta = valCtas.obtenDatosValidaCuenta(session.getContractNumber(),
   															  session.getUserID8(), 
   													          session.getUserProfile(),
   													          cuentaValidar.trim(),
   													          IEnlace.MMant_estruc_TI);
   				 EIGlobal.mensajePorTrace("TIConcentracion modificaEstructura- datoscta: "+datoscta, EIGlobal.NivelLog.INFO);

   				 if(datoscta==null)  break;
          	  }

          	  }
          	  EIGlobal.mensajePorTrace("TIConcentracion - CONTINUA FLUJO: ", EIGlobal.NivelLog.INFO);

         	 if(datoscta!=null){
         		 hs=TuxGlobal.web_red(Trama);
         		 Result=(String) hs.get("BUFFER");}

           }
          catch( java.rmi.RemoteException re )
           {
             re.printStackTrace();
             Result=null;
           }
          catch (Exception e) {}
          finally
  		{
  	    	try{
  	    		valCtas.closeTransaction();
  	    	}catch (Exception e) {
  	    		EIGlobal.mensajePorTrace("Error al cerrar la conexi�n a MQ", EIGlobal.NivelLog.ERROR);
  			}


  	    }
          EIGlobal.mensajePorTrace("TIConcentracion - modificaEstructura(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);

          if(datoscta==null)
        	  strMensaje="Error H701:Cuenta Inv&aacute;lida";

         else
          if(Result==null || Result.equals("null"))
                strMensaje="Su transacci&oacute;n no puede ser atendida en este momento. Intenete mas tarde.";
          else
           {
                 if(Result.substring(0,2).equals("OK"))
                  {
                        if(Result.substring(20,24).trim().equals("0000"))
                          strMensaje="La estructura se Modific&oacute; satisfactoriamente ";
                        else
                          strMensaje=Result.substring(Result.indexOf("@")+1,Result.lastIndexOf("@"));
                  }
                 else
                   strMensaje="La estructura No se pudo Modificar. <br>"+Result.substring(16,Result.length());
           }

            //TODO BIT CU3142 Modifica estructura estructuras

            /**
	 		 * VSWF - FVC - I
	 		 * 17/Enero/2007
	 		 */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
	 		try {
	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, session, ses);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI04);
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				bt.setNombreArchivo((strArchivo == null)?" ":strArchivo);
				if(Result != null){
					if(Result.length() >= 24)
						bt.setIdErr(Result.substring(20,24));
				}
				bt.setTipoMoneda((Divisa == null)?" ":Divisa);

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
	 		/**
	 		 * VSWF - FVC - F
	 		 */


          despliegaMensaje(strMensaje,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
        }


/*************************************************************************************/
/******************************************************* borraEstructura (Modulo=5)  */
/*************************************************************************************/
   public void borraEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          String strMensaje="";

          String Trama="";
          String Result="";
          String tipoArbol="C";
          Hashtable hs = null;

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");

          EIGlobal.mensajePorTrace("TIConcentracion - borraEstructura(): Entrando a borra.", EIGlobal.NivelLog.INFO);

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  borraEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);

      ServicioTux TuxGlobal = new ServicioTux();
          //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

          Trama="1EWEB|"+session.getUserID8()+"|TI03|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@|"+Divisa+"|";

          EIGlobal.mensajePorTrace("TIConcentracion - borraEstructura(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);
          try
           {
             hs=TuxGlobal.web_red(Trama);
         Result=(String) hs.get("BUFFER");
           }
          catch( java.rmi.RemoteException re )
           {
             re.printStackTrace();
             Result=null;
           }
          catch (Exception e) {}
          EIGlobal.mensajePorTrace("TIConcentracion - borraEstructura(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);

          if(Result==null || Result.equals("null"))
                strMensaje="Su transacci&oacute;n no puede ser atendida en este momento. Intenete mas tarde.";
          else
           {
                 if(Result.substring(0,2).equals("OK"))
                   {
                         if(Result.substring(20,24).trim().equals("0000"))
                           strMensaje="La estructura se Borr&oacute; satisfactoriamente ";
                         else
                           strMensaje=Result.substring(Result.indexOf("@")+1,Result.lastIndexOf("@"));
                   }
                 else
                   strMensaje="La estructura No se pudo eliminar. <br>"+Result.substring(16,Result.length());
           }

          	//TODO BIT CU3143 Borra estructuras

          	/**
	 		 * VSWF - FVC - I
	 		 * 17/Enero/2007
	 		 */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
	 		try {
	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, session, ses);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI04);
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				if(Result.length() >= 8)
					bt.setIdErr(Result.substring(0,8));
				else
					bt.setIdErr(Result);
				bt.setTipoMoneda((Divisa == null)?" ":Divisa);

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
	 		/**
	 		 * VSWF - FVC -F
	 		 */

          despliegaMensaje(strMensaje,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
        }

/*************************************************************************************/
/******************************************************** editaEstructura (Modulo=6)  */
/*************************************************************************************/
   public void editaEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          EIGlobal.mensajePorTrace("TIConcentracion - editaEstructura(): Entrando a Editar estructura.", EIGlobal.NivelLog.INFO);

          String tramasCuentas="";

          //Todas las tramas anteriores
          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  editaEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);

          //####################### Muestra la tabla solo es informativo
          EI_Tipo Estructura= new EI_Tipo(this);
          Estructura.iniciaObjeto(tramasCuentas);
          req.setAttribute("Consulta",generaTabla("Estructura de concentraci&oacute;n",0,Estructura,req));
          //####################### Muestra la tabla solo es informativo

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
          req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
          req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());  
          req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
          req.setAttribute("MenuPrincipal", session.getStrMenu());
          req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29070h",req));

          req.setAttribute("TramasCuentas",tramasCuentas);

          //################ Si se llama al servicio para borrar si se puede dar de alta.
          req.setAttribute("Alta",(String) getFormParameter(req,"Alta"));
          req.setAttribute("Modificar",(String) getFormParameter(req,"Modificar"));
          req.setAttribute("Trama",(String) getFormParameter(req,"Trama"));

          //TODO BIT CU3142 Modifica estructura estructuras

          	/**
	 		 * VSWF - FVC - I
	 		 * 17/Enero/2007
	 		 */
          if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
	 		try {
	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
				BitaHelper bh = new BitaHelperImpl(req, session, ses);
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI04);
				bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
				bt.setTipoMoneda((Divisa == null)?" ":Divisa);

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
	 		/**
	 		 * VSWF - FVC - F
	 		 */

          evalTemplate( "/jsp/TIEditaConcentracion.jsp", req, res );
        }

/*************************************************************************************/
/******************************************************** iniciaCopia (Modulo=7)  */
/*************************************************************************************/
   public void iniciaCopia(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          EIGlobal.mensajePorTrace("TIConcentracion - iniciaCopia(): Entrando a Editar estructura.", EIGlobal.NivelLog.INFO);

          String tramasCuentas="";

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		  String Divisa = (String)ses.getAttribute("Divisa");
		  EIGlobal.mensajePorTrace("El valor de la divisa en iniciaCopia es :" + Divisa, EIGlobal.NivelLog.INFO);

          //Todas las tramas anteriores
          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
          req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
          req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
          req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
          req.setAttribute("MenuPrincipal", session.getStrMenu());
          req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29080h",req));

          req.setAttribute("TramasCuentas",tramasCuentas);

          //################ Si se llama al servicio para borrar si se puede dar de alta.
          req.setAttribute("Alta",(String) getFormParameter(req,"Alta"));
          req.setAttribute("Modificar",(String) getFormParameter(req,"Modificar"));

          evalTemplate( "/jsp/TICopiaConcentracion.jsp", req, res );
        }

/*************************************************************************************/
/******************************************************* eliminaRegistro (Modulo=8)  */
/*************************************************************************************/
   public void eliminaRegistro(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          EIGlobal.mensajePorTrace("TIConcentracion - eliminaRegistro(): Entrando a elimina Registro.", EIGlobal.NivelLog.INFO);

          String tramaCuentas="";
          String nuevaTramaCuentas="";
          String tramasCuentas="";
          String regEliminado="";

          int borrar=-1;

          EI_Tipo Anterior=new EI_Tipo(this);
          EI_Tipo Temporal=new EI_Tipo(this);
          EI_Tipo Nuevo=new EI_Tipo(this);

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  eliminaRegistro ..... " + Divisa, EIGlobal.NivelLog.INFO);

          borrar=Integer.parseInt((String) getFormParameter(req,"Borrar"));

          //Todas las tramas anteriores
          tramasCuentas=(String) getFormParameter(req,"TramasCuentas");

          Anterior.iniciaObjeto(tramasCuentas);

          EIGlobal.mensajePorTrace("TIConcentracion - eliminaRegistro(): Eliminando Objetos.", EIGlobal.NivelLog.INFO);

          //Eliminando registro de la trama
          for(int i=0;i<Anterior.totalRegistros;i++)
           {
                 if(i!=borrar)
                   {
                         for(int b=0;b<12;b++)
                           nuevaTramaCuentas+=Anterior.camposTabla[i][b]+"|";
                         nuevaTramaCuentas+=Anterior.camposTabla[i][12];
                         nuevaTramaCuentas+="@";
                   }
                  else
                   { //Registro que se va a eliminar
                         for(int b=0;b<12;b++)
                           regEliminado+=Anterior.camposTabla[i][b]+"|";
                         regEliminado+=Anterior.camposTabla[i][12];
                         regEliminado+="@";
                   }
           }

          Temporal.iniciaObjeto(nuevaTramaCuentas);
          Nuevo.iniciaObjeto(tramaConcentracion(Temporal));

          EIGlobal.mensajePorTrace("TIConcentracion - eliminaRegistro(): Generando Pantalla.", EIGlobal.NivelLog.INFO);

          //####### Trama del registro eliminado
          regEliminado+="<p><BR>"+generaTabla("Estructura de concentraci&oacute;n",0,Anterior,req);
          req.setAttribute("Consulta",regEliminado);
          //####### Trama del registro eliminado

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
          req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
          req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
          req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
          req.setAttribute("MenuPrincipal", session.getStrMenu());
          req.setAttribute("newMenu", session.getFuncionesDeMenu());
          req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29000h",req));

          req.setAttribute("Archivo",moduloImportar("true"));
          req.setAttribute("Tabla",generaTabla("Estructura de concentraci&oacute;n",6,Nuevo,req));
          req.setAttribute("TramasCuentas",Nuevo.strOriginal);
          req.setAttribute("Alta",(String) getFormParameter(req,"Alta"));
          req.setAttribute("Modificar",(String) getFormParameter(req,"Modificar"));

          //Facultades...
          req.setAttribute("AgregaEdita", (session.getFacultad(session.FacCapConcen))?"true":"false");
                   req.setAttribute("AltaModificaBorra",(session.getFacultad(session.FacActParam))?"true":"false");
                   req.setAttribute("Copiar",(session.getFacultad(session.FacCopEstr))?"true":"false");


                //TODO BIT CU3145 Elimina registros

                /**
       	 		 * VSWF - FVC - I
       	 		 * 17/Enero/2007
       	 		 */
                   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
       	 		try {
       	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					BitaHelper bh = new BitaHelperImpl(req, session, ses);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_ELIMINA_REGISTRO);
					bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
					bt.setTipoMoneda((Divisa == null)?" ":Divisa);

					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
                   }
       	 		/**
       	 		 * VSWF
       	 		 */
          evalTemplate( "/jsp/TIConcentracion.jsp", req, res );
        }

/*************************************************************************************/
/******************************************************* copiaEstructura (Modulo=9)  */
/*************************************************************************************/
   public void copiaEstructura(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
        {
          EIGlobal.mensajePorTrace("TIConcentracion - copiaEstructura(): Entrando a copia Estructura.", EIGlobal.NivelLog.INFO);

          String tramasCuentas="";
          String tipoArbol="";
          String tipoOperacion="";

          String tipoCuenta="";
          String nombreArchivo="";

          String arcLinea="";
          String cadenaTCT="";
		  String aux_cadenaTCT="";
          String mensajeError="";

          String Trama="";
          String Result="";
          Hashtable hs = null;

          String strTipoArbol="";



          boolean comuError=false;

          int pos=0;
          int reg=0;

          EI_Tipo TCTArchivo=new EI_Tipo(this);
          EI_Tipo Anterior=new EI_Tipo(this);

          ServicioTux TuxGlobal = new ServicioTux();
          //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

          /************* Modificacion para la sesion ***************/
          HttpSession ses = req.getSession();
          BaseResource session = (BaseResource) ses.getAttribute("session");
          EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		  String Divisa = (String)ses.getAttribute("Divisa");
          EIGlobal.mensajePorTrace("El valor de la divisa en  copiaEstructura ..... " + Divisa, EIGlobal.NivelLog.INFO);

          tipoArbol=(String) req.getParameter("Sel");
          if(tipoArbol==null)
                tipoArbol="D";

           tipoOperacion="TI02";

           if(tipoArbol.equals("D"))
             strTipoArbol="Dispersi&oacute;n";
           else
             strTipoArbol="Fondeo";

           Trama="2EWEB|"+session.getUserID8()+"|"+tipoOperacion+"|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@|"+Divisa+"|";
           EIGlobal.mensajePorTrace("TIConcentracion - copiaEstructura(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

                try
                 {
                   hs=TuxGlobal.web_red(Trama);
           Result=(String) hs.get("BUFFER");
                 }
                catch( java.rmi.RemoteException re )
                 {
                   re.printStackTrace();
                   Result=null;
                 }
                catch (Exception e) {}

           EIGlobal.mensajePorTrace("TIConcentracion - copiaEstructura(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);

          //####################### Temporal
          /*
          cadenaTCT ="OK      63474   @TEIN0000@Transaccion Exitosa@4";
          cadenaTCT+="@60501058867@80000648376@C@65500144939@DESCRIPCION 3";
          cadenaTCT+="@60501058868@80000648376@C@65500144939@DESCRIPCION 2";
          cadenaTCT+="@57003439833@80000648376@C@65500144939@DESCRIPCION 1";
          cadenaTCT+="@65500144939@80000648376@C@0@DESCRIPCION 0 PADRE";
          cadenaTCT+="@4";
          cadenaTCT+="@60501058867@80000648376@0@2@1000.000000@0.000000@08:00";
          cadenaTCT+="@60501058868@80000648376@6@2@20000.000000@0.000000@09:00";
          cadenaTCT+="@57003439833@80000648376@6@2@30000.000000@0.000000@10:00";
          cadenaTCT+="@57003439833@80000648376@6@2@40000.000000@0.000000@11:00";
          cadenaTCT+="@";
          */
          //####################### Temporal

          if(Result==null || Result.equals("null"))
            {
                  EIGlobal.mensajePorTrace("TIConcentracion - copiaEstructura(): El servicio no funciono correctamente: (null) ", EIGlobal.NivelLog.INFO);

                  //######################## Solo es temporal
                  //Result="ERROR";
                  //cadenaTCT="ERROR";
                  //######################## Solo es temporal

                  comuError=true;
                  mensajeError="Su transacci&oacute;n no puede ser atendida en este momento.<br>Por favor intente mas tarde.";
                }
           else
                {
                  nombreArchivo=Result.substring(Result.lastIndexOf('/')+1,Result.length());

                  ArchivoRemoto archR = new ArchivoRemoto();
                  if(!archR.copia(nombreArchivo))
                        EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
                  else
                    EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): Copia remota OK.", EIGlobal.NivelLog.INFO);

                  nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArchivo;
                  EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
                  EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): Path NombreArchivo."+nombreArchivo, EIGlobal.NivelLog.INFO);

                   if(ArcEnt.abreArchivoLectura())
                        {
                          arcLinea=ArcEnt.leeLinea();
                          if(arcLinea.substring(0,2).equals("OK")){
							   cadenaTCT+=arcLinea;
							   EIGlobal.mensajePorTrace( "La cadenaTCT es ----------- " + cadenaTCT, EIGlobal.NivelLog.INFO);
	 					       pos=noA(3,cadenaTCT);
							   Divisa = cadenaTCT.substring(pos,cadenaTCT.indexOf("@",pos));
							   //if(Divisa!=null && Divisa.trim()!=""){
							//	  req.setAttribute("Divisa",Divisa);
							 //  }
							   EIGlobal.mensajePorTrace( "La divisa  que me devuelve en la trama del archivo es ----------- " + Divisa, EIGlobal.NivelLog.INFO);
							   aux_cadenaTCT = cadenaTCT.substring(0,pos);
							   EIGlobal.mensajePorTrace( "La cadena aux_cadenaTCT  ==>" + aux_cadenaTCT + "<", EIGlobal.NivelLog.INFO);
		                       pos=noA(4,cadenaTCT);
							   aux_cadenaTCT = aux_cadenaTCT + cadenaTCT.substring(pos,cadenaTCT.length());
							   EIGlobal.mensajePorTrace( "La cadena aux_cadenaTCT  ==>" + aux_cadenaTCT + "<", EIGlobal.NivelLog.INFO);
					           reg=Integer.parseInt(cadenaTCT.substring(pos,cadenaTCT.indexOf("@",pos)));
							   cadenaTCT=aux_cadenaTCT;
 							   EIGlobal.mensajePorTrace( "------------ Los reg son ----------- " + reg, EIGlobal.NivelLog.INFO);
                          }else
                           {
                                 comuError=true;
                                 if(arcLinea.substring(0,5).trim().equals("ERROR"))
                                   mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
                                 else
                                   {
                                         //mensajeError="La informacion no se recibio correctamente. <br>Intente mas tarde.";
                                         EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): No se reconocio el encabezado. "+arcLinea, EIGlobal.NivelLog.INFO);
                                     mensajeError=arcLinea.substring(16,arcLinea.length());
                                   }
                           }
                          ArcEnt.cierraArchivo();
                          EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): Archivo: "+cadenaTCT, EIGlobal.NivelLog.INFO);
                        }
                   else
                        {
                          //########################  Solo para hacer pruebas
                          /*
                          cadenaTCT ="OK      63474   @TEIN0000@Transaccion Exitosa@4";
                          cadenaTCT+="@60501058867@80000648376@C@65500144939@DESCRIPCION 3";
                          cadenaTCT+="@60501058868@80000648376@C@65500144939@DESCRIPCION 2";
                          cadenaTCT+="@57003439833@80000648376@C@65500144939@DESCRIPCION 1";
                          cadenaTCT+="@65500144939@80000648376@C@0@DESCRIPCION "+strTipoArbol;
                          cadenaTCT+="@4";
                          cadenaTCT+="@60501058867@80000648376@0@2@1000.000000@0.000000@08:00";
                          cadenaTCT+="@60501058868@80000648376@6@2@20000.000000@0.000000@09:00";
                          cadenaTCT+="@57003439833@80000648376@6@2@30000.000000@0.000000@10:00";
                          cadenaTCT+="@57003439833@80000648376@6@2@40000.000000@0.000000@11:00";
                          cadenaTCT+="@";
                          //########################  Solo para hacer pruebas
                          */
                          comuError=true;
                          mensajeError="No se pudo obtener la informaci&oacute;n del archivo. <br>Por favor intente mas tarde.";
                        }
                 }

           if(comuError)
                 despliegaMensaje(mensajeError,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
           else
                {
                   EIGlobal.mensajePorTrace("TIConcentracion - copiaEstructura(): El archivo contenia "+ cadenaTCT, EIGlobal.NivelLog.INFO);
                   boolean estructura=true;
                   //OK      91174   @TEIN0000@Transaccion Exitosa@0@0@
                   if(cadenaTCT.substring(0,2).equals("OK"))
                        {
                          pos=noA(3,cadenaTCT);
                          reg=Integer.parseInt(cadenaTCT.substring(pos,cadenaTCT.indexOf("@",pos)));
                          if(reg>0)
                           {
                                 if(tipoArbol.equals("F"))
                                   Anterior=armaTramasFondeo(cadenaTCT,true);
                                 else
                                   Anterior=armaTramas(cadenaTCT,tipoArbol,true);

                                 TCTArchivo.iniciaObjeto(tramaConcentracion(Anterior));
                           }
                          else
                           {
                                 estructura=false;
                                 cadenaTCT+="No hay Registros";
                           }
                        }
                   else
                         estructura=false;

                   //################### Resultado del servicio
                   req.setAttribute("Consulta","<br><br>"+cadenaTCT);
                   //################### Resultado del servicio

                   if(estructura)
                        {
                          req.setAttribute("Alta","NO");
                          req.setAttribute("Modificar","OK");
                        }
                   else
                        {
                          req.setAttribute("Alta","OK");
                      req.setAttribute("Modificar","NO");
                        }

                   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
                   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
                   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8()); 
                   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

                   req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa."));
                   req.setAttribute("MenuPrincipal", session.getStrMenu());
                   req.setAttribute("newMenu", session.getFuncionesDeMenu());
                   req.setAttribute("Encabezado",CreaEncabezado("Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a Inteligente &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n","s29000h",req));

                   req.setAttribute("Archivo",moduloImportar("true"));
                   req.setAttribute("TramasCuentas",TCTArchivo.strOriginal);
                   req.setAttribute("Tabla",generaTabla("Estructura de "+strTipoArbol,6,TCTArchivo,req));

                   //Facultades...
                   req.setAttribute("AgregaEdita", (session.getFacultad(session.FacCapConcen))?"true":"false");
                   req.setAttribute("AltaModificaBorra",(session.getFacultad(session.FacActParam))?"true":"false");
                   req.setAttribute("Copiar",(session.getFacultad(session.FacCopEstr))?"true":"false");

                   //TODO BIT CU3144 Copia estructuras

                /**
       	 		 * VSWF - FVC - I
       	 		 * 17/Enero/2007
       	 		 */
                   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
       	 		try {
       	 			BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
					BitaHelper bh = new BitaHelperImpl(req, session, ses);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI02);
					bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
					bt.setTipoMoneda((Divisa == null)?" ":Divisa);
					bt.setServTransTux("TI02");
					bt.setNombreArchivo(Result.substring(Result.lastIndexOf('/')+1,Result.length()));
					/*BMB-I*/
					if(Result!=null){
		    			if(Result.substring(0,2).equals("OK")){
		    				bt.setIdErr("TI020000");
		    			}else if(Result.length()>8){
			    			bt.setIdErr(Result.substring(0,8));
			    		}else{
			    			bt.setIdErr(Result.trim());
			    		}
		    		}
					/*BMB-F*/
					BitaHandler.getInstance().insertBitaTransac(bt);
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
                   }
       	 		/**
       	 		 * VSWF - FVC - F
       	 		 */

                   evalTemplate( "/jsp/TIConcentracion.jsp", req, res );
                }
        }

/*************************************************************************************/
/******************************************************************************* noA */
/*************************************************************************************/
  int noA(int num,String strLinea)
        {
          int i,j,pos=0;

          for(i=0,j=0;j<num;i++)
           if(strLinea.charAt(i)=='@')
                {pos=i; j++;}
          return pos+1;
        }

/*************************************************************************************/
/*************************************************************** formatoPeriodicidad */
/*************************************************************************************/
// YHG Modiifca valores de dias-
  private String formatoPeriodicidad(String dia)
        {
          String per="0";
          if(dia.equals("Diaria"))
                per="0";
          if(dia.equals("Lunes"))
                per="1";
          if(dia.equals("Martes"))
                per="2";
          if(dia.equals("Miercoles"))
                per="3";
          if(dia.equals("Jueves"))
                per="4";
          if(dia.equals("Viernes"))
                per="5";
          if(dia.equals("Sabado"))
                per="6";
          if(dia.equals("Domingo"))
                per="7";

          EIGlobal.mensajePorTrace( "TIConcentracion - formatoPeriodicidad(): Dia: "+dia+" Codigo: "+per, EIGlobal.NivelLog.INFO);

          return per;
        }

/*************************************************************************************/
/**************************************************************** cambiaPeriodicidad */
/*************************************************************************************/
  //- YHG Modiifca valores de dias
  private String cambiaPeriodicidad(String dia)
        {
          String per="Diaria";

          if(dia.equals("0"))
                per="Diaria";
          if(dia.equals("1"))
                per="Lunes";
          if(dia.equals("2"))
                per="Martes";
          if(dia.equals("3"))
                per="Miercoles";
          if(dia.equals("4"))
                per="Jueves";
          if(dia.equals("5"))
                per="Viernes";
          if(dia.equals("6") )
                per="Sabado";

          EIGlobal.mensajePorTrace( "TIConcentracion - formatoPeriodicidad(): Dia: "+dia+" Codigo: "+per, EIGlobal.NivelLog.INFO);

          return per;
        }

/*************************************************************************************/
/**************************************************************** tramaConcentracion */
/*************************************************************************************/
  private String tramaConcentracion(EI_Tipo Tramas)
        {
            String tramaOrdenar="";
                String tramaOrdenada="";
                String nuevaTramaCuentas="";
                int a=0;

                EI_Tipo TmpCuentas= new EI_Tipo(this);

                EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Entrando a metodo", EIGlobal.NivelLog.INFO);

                if(Tramas.totalRegistros<3)
                  return Tramas.strOriginal;

        /*cuenta, cuenta padre, descripcion, periodicidad, saldo op.*/
                for(int i=0;i<Tramas.totalRegistros;i++)
                 {
                   tramaOrdenar+="@"+Tramas.camposTabla[i][0]+"|";
                   if(Tramas.camposTabla[i][7].trim().equals("0"))
                         tramaOrdenar+=" |";
                   else
                         tramaOrdenar+=Tramas.camposTabla[i][7].trim()+"|";
                   tramaOrdenar+=Integer.toString(i)+"|";
                   tramaOrdenar+=formatoPeriodicidad(Tramas.camposTabla[i][2])+"|";
                   tramaOrdenar+=Tramas.camposTabla[i][4]+"|";
                   tramaOrdenar+="N|";
                   tramaOrdenar+="N|";
                   tramaOrdenar+=Tramas.camposTabla[i][11];
                 }

                EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Trama para ordenar: "+tramaOrdenar, EIGlobal.NivelLog.INFO);
                tramaOrdenada=ordenaArbol(tramaOrdenar);
                EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Trama ordenada: "+tramaOrdenada, EIGlobal.NivelLog.INFO);
                TmpCuentas.iniciaObjeto(tramaOrdenada.substring(1,tramaOrdenada.length())+"@");

                for(int i=0;i<Tramas.totalRegistros;i++)
                 {
                   a=Integer.parseInt(TmpCuentas.camposTabla[i][2]);
                   for(int b=0;b<12;b++)
                         nuevaTramaCuentas+=Tramas.camposTabla[a][b]+"|";
                   nuevaTramaCuentas+=Tramas.camposTabla[a][12];
                   nuevaTramaCuentas+="@";
                 }
          return nuevaTramaCuentas;
        }

//######################################################################################
//######################################################################################
//######################################################################################
//######################################################################################
/**************************************************************************************/
  private String ordenaArbol(String tramaEntrada)
        {
                String tramaSalida;
                Vector cuentas, ctasOrden;
                TI_CuentaDispersion ctaAux;
                int a, b;

                b = 1;
                cuentas = creaCuentas(tramaEntrada);
                ctasOrden = new Vector();
                while(cuentas.size() > 0)
                        {
                        for(a=0;a<cuentas.size();a++)
                                {
                                ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
                                if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
                                }
                        b++;
                        }
                cuentas = ctasOrden;
                ctasOrden = new Vector();
                for(a=0;a<cuentas.size();a++)
                        {
                        ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
                        if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
                        }
                while(cuentas.size() > 0)
                        {
                        ctaAux = ((TI_CuentaDispersion)cuentas.get(0));
                        ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
                        cuentas.remove(ctaAux);
                        }
                cuentas = ctasOrden;
                tramaSalida = "";
                for(a=0;a<cuentas.size();a++) tramaSalida += ((TI_CuentaDispersion)cuentas.get(a)).trama();

                return tramaSalida;
        }

  public Vector creaCuentas(String tramaCuentas)
        {
                Vector cuentas = null;
                StringTokenizer tokens;
                TI_CuentaDispersion ctaAux1, ctaAux2;
                int a, b;

                try
                        {
                        cuentas = new Vector();
                        tokens = new StringTokenizer(tramaCuentas,"@");
                        while(tokens.hasMoreTokens())
                                {
                                cuentas.add(TI_CuentaDispersion.creaCuentaDispersion("@" + tokens.nextToken()));
                                for(a=0;a<cuentas.size();a++)
                                        {
                                        ctaAux1 = (TI_CuentaDispersion)cuentas.get(a);
                                        if(!ctaAux1.posiblePadre.equals(""))
                                                for(b=0;b<cuentas.size();b++)
                                                        {
                                                        ctaAux2 = (TI_CuentaDispersion)cuentas.get(b);
                                                        if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
                                                                {
                                                                ctaAux1.setPadre(ctaAux2);
                                                                b=cuentas.size();
                                                                }
                                                        }
                                        }
                                }
                        }
                catch(Exception e)
                        {EIGlobal.mensajePorTrace("TIConcentracion - importarArchivo(): <DEBUG RVV> Error en creaCuentas(): " + e.toString(), EIGlobal.NivelLog.INFO);}
                finally
                        {return cuentas;}
                }

/**************************************************************************************/
//######################################################################################
//######################################################################################
//######################################################################################
//######################################################################################

/*************************************************************************************/
/*** regresa cadena para la opcion de importacion desde archivo si tiene facultad    */
/*************************************************************************************/
   String moduloImportar(String facImpArc)
        {
           String strImp="<!-- No Tiene Facultad. //-->";

           if(facImpArc.trim().equals("true"))
                {
                        strImp="\n    <table border=0 class='textabdatcla' cellspacing=0 cellpadding=3>";
                        strImp+="\n     <tr>";
                        strImp+="\n      <td class='tittabdat'>Importar archivo</td>";
                        strImp+="\n     </tr>";
                        strImp+="\n     <tr>";
                        strImp+="\n      <td class='tabmovtex'><br></td>";
                        strImp+="\n     </tr>";
                        strImp+="\n     <tr>";
                        strImp+="\n      <td class='tabmovtex' align=center>Importar desde archivo &nbsp; <INPUT TYPE='file' NAME='Archivo' value=' Archivo '></td>";
                        strImp+="\n     </tr>";
                        strImp+="\n     <tr>";
                        strImp+="\n      <td class='tabmovtex' ><br></td>";
                        strImp+="\n     </tr>";
                        strImp+="\n     <tr>";
                        strImp+="\n      <td class='tabmovtex' align=center><a href='javascript:Importar();'><img src='/gifs/EnlaceMig/gbo25280.gif' border=0 alt='Aceptar'></a></td><br>";
                        strImp+="\n     </tr>";
                        strImp+="\n    </table>";
                }
           return strImp;
        }

/*************************************************************************************/
/**************************************************************** despliega Mensaje  */
/*************************************************************************************/
 public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
         String contrato_="";

         /************* Modificacion para la sesion ***************/
         HttpSession sess = request.getSession();
         BaseResource session = (BaseResource) sess.getAttribute("session");
         EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

         contrato_ = session.getContractNumber();
         if(contrato_==null)
            contrato_ ="";

         request.setAttribute( "FechaHoy",Global.fechaHoy( "dt, dd de mt de aaaa" ) );
         request.setAttribute( "Error", mensaje );
         StringBuffer sbURL = new StringBuffer("TIConcentracion?Modulo=0");

		 String Divisa = (String)sess.getAttribute("Divisa");
         EIGlobal.mensajePorTrace("El valor de la divisa en despliegaMensaje ..... " + Divisa,EIGlobal.NivelLog.INFO);

		 if(Divisa!=null){
           sbURL.append("&hdnDivisa=");
           sbURL.append("Divisa");
         }

         request.setAttribute( "URL", sbURL.toString() );

         request.setAttribute( "MenuPrincipal", session.getStrMenu() );
         request.setAttribute( "newMenu", session.getFuncionesDeMenu());
         request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s29000h",request ) );
		 sess.setAttribute("Encabezado",CreaEncabezado( param1, param2, "s29000h", request));
         request.setAttribute( "NumContrato", contrato_ );
         request.setAttribute( "NomContrato", session.getNombreContrato() );
         request.setAttribute( "NumUsuario", session.getUserID8() );
         request.setAttribute( "NomUsuario", session.getNombreUsuario() );

         evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
   }

/*************************************************************************************/
/************************************************ genera Tabla del arbol de cuentas  */
/*************************************************************************************/
   String generaTabla(String titulo, int ind, EI_Tipo Trans, HttpServletRequest req)
        {
           String[] titulos={ "",
                              "Cuenta",
                                  "Nivel",
                                                  "Periodicidad",
                                                  "Horario",
                                                  "Saldo Operativo",
                                  "Inv. Autom&aacute;tica",
                                  "Saldo Operativo",
                                  ""};

           int datos[]={7,0,0,12,2,3,4,5,6,7,8};
           int values[]={13,0,1,2,3,4,5,6,7,8,9,10,11,12};
           int align[]={0,1,1,1,2,1,2,0};

           /************* Modificacion para la sesion ***************/
           HttpSession ses = req.getSession();
           BaseResource session = (BaseResource) ses.getAttribute("session");
           EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

		   String Divisa = (String)ses.getAttribute("Divisa");
		   EIGlobal.mensajePorTrace("EL valor de la divisa en generaTabla es: " + Divisa, EIGlobal.NivelLog.INFO);

		   if(Divisa.equals("USD")){
			   datos[0]=5;
		   }
           EnlaceGlobal.formateaImporte(Trans, 4);
           EnlaceGlobal.formateaImporte(Trans,6);

           EIGlobal.mensajePorTrace("TIConcentracion - generatabla(): Generando Tabla de Estructura.", EIGlobal.NivelLog.INFO);

           titulos[0]=titulo;
           datos[1]=ind;

           return Trans.generaTablaIntegra(titulos,datos,values,align);
        }

/*************************************************************************************/
/*********************************************************************** armaTramas  */
/*************************************************************************************/
  EI_Tipo armaTramas(String cadenaServicio, String tipo,boolean taro)
   {
         String cuentas1="";
         String pipe="|";
         String nivel="";

         int indic=0;

         EI_Tipo Aux1=new EI_Tipo(this);

     Aux1.iniciaObjeto('@','|',cadenaServicio+"|");
         TICuentaArchivo a=new TICuentaArchivo();

         if(taro)
          if(!tipo.equals("C"))
           nivel="CPOTRESTR";

         for(int i=0;i<Integer.parseInt(Aux1.camposTabla[0][3]);i++)
          {
                 cuentas1 =Aux1.camposTabla[0][4+(i*5)]+pipe; //Cuenta
                 cuentas1+=nivel+pipe;  //Nivel
                 cuentas1+=""+pipe;  //Periodicidad
                 cuentas1+=""+pipe;  //Horario
                 cuentas1+="0"+pipe;  //SaldoOp1
                 cuentas1+="No"+pipe;  //InvAuto
                 cuentas1+="0"+pipe;  //SaldoOp2
                 cuentas1+=Aux1.camposTabla[0][7+(i*5)]+pipe;  //CuentaPadre
                 cuentas1+=""+pipe;  //NumeroNivel
                 cuentas1+=""+pipe;  //TipoNivel
                 cuentas1+=Aux1.camposTabla[0][8+(i*5)]+pipe; //Descripcion
                 cuentas1+="1"+pipe;  //ClaveProducto
                 cuentas1+="1";  //NivelTrama
                 cuentas1+="@";
                 indic=(8+(i*5))+1;
                 EIGlobal.mensajePorTrace("TIConcentracion - armaTramas(): Insertando cuenta "+cuentas1, EIGlobal.NivelLog.DEBUG);
                 a.actualizaCuenta(cuentas1);
          }

         for(int j=0;j<Integer.parseInt(Aux1.camposTabla[0][indic]);j++)
          {
         cuentas1 =Aux1.camposTabla[0][(indic+1)+(7*j)]+pipe; //Cuenta
                 cuentas1+=nivel+pipe;  //Nivel
                 if(tipo.equals("C"))
                  {
                    cuentas1+=cambiaPeriodicidad(Aux1.camposTabla[0][(indic+3)+(7*j)])+pipe; //Periodicidad
                        cuentas1+=Aux1.camposTabla[0][(indic+7)+(7*j)]+pipe;  //Horario
                        cuentas1+=Aux1.camposTabla[0][(indic+5)+(7*j)]+pipe;  //SaldoOp1
                        if(a.esCero(Aux1.camposTabla[0][(indic+6)+(7*j)]))
                          cuentas1+="No"+pipe;  //InvAuto
                        else
                          cuentas1+="Si"+pipe;  //InvAuto
                        cuentas1+=Aux1.camposTabla[0][(indic+6)+(7*j)]+pipe;  //SaldoOp2
                  }
                 else
                  {
                    cuentas1+=""+pipe;//cambiaPeriodicidad(Aux1.camposTabla[0][(indic+3)+(7*j)])+pipe; //Periodicidad
                        cuentas1+=""+pipe;//Aux1.camposTabla[0][(indic+7)+(7*j)]+pipe;  //Horario
                        cuentas1+="0"+pipe;  //SaldoOp1
                        cuentas1+="No"+pipe; //InvAuto
                        cuentas1+="0"+pipe;  //SaldoOp2
                  }
                 cuentas1+=""+pipe;  //CuentaPadre
                 cuentas1+=""+pipe;  //NumeroNivel
                 cuentas1+=""+pipe;  //TipoNivel
                 cuentas1+=""+pipe;  //Descripcion
                 cuentas1+="1"+pipe;  //ClaveProducto
                 cuentas1+=Aux1.camposTabla[0][(indic+4)+(7*j)];  //NivelTrama
                 cuentas1+="@";
                 EIGlobal.mensajePorTrace("TIConcentracion - armaTramas(): Agregando cuenta "+cuentas1, EIGlobal.NivelLog.DEBUG);
                 a.agregaCuenta(cuentas1);
          }

         a.cadenaCuentas();
     return a.Todas;
   }

/*************************************************************************************/
/******************************************************** generaArchivoParaServicio  */
/*************************************************************************************/
  public boolean generaArchivoParaServicio(EI_Tipo Arbol, HttpServletRequest req)
   {
         String nombreArchivo="";
         String strArc="";
         String cadHA="";
         String cadHB="";
         String strTramasH="";

         int horas=0;
         int total=0;

         boolean seGenero=true;

         /************* Modificacion para la sesion ***************/
         HttpSession sess = req.getSession();
         BaseResource session = (BaseResource) sess.getAttribute("session");

         nombreArchivo=session.getUserID8()+".tic";
         EI_Exportar ArcTux=new EI_Exportar(IEnlace.DOWNLOAD_PATH+nombreArchivo);

         if(ArcTux.creaArchivo())
          {
                strArc+=session.getContractNumber()+"@";
                strArc+="C"+"@";
                strArc+=Arbol.totalRegistros+"@";

                for(int i=0;i<Arbol.totalRegistros;i++)
                 {
                   strArc+=Arbol.camposTabla[i][0]+"@";
                   strArc+=Arbol.camposTabla[i][7]+"@";
                   strArc+=Arbol.camposTabla[i][10]+"@";
                 }

                for(int i=0;i<Arbol.totalRegistros;i++)
                 {
                   if(!Arbol.camposTabla[i][3].trim().equals(""))
                         {
                                horas=cuentaCampos(',',Arbol.camposTabla[i][3]);
                                cadHA=Arbol.camposTabla[i][3];
                                for(int j=0;j<horas;j++)
                                 {
                                   cadHB=cadHA.substring(0,cadHA.indexOf(',')).trim();
                                   strTramasH+=Arbol.camposTabla[i][0]+"@";
                                   strTramasH+=formatoPeriodicidad(Arbol.camposTabla[i][2])+"@";
                                   strTramasH+=Arbol.camposTabla[i][12]+"@";
                                   strTramasH+=Arbol.camposTabla[i][4]+"@";
                                   strTramasH+=Arbol.camposTabla[i][6]+"@";
                                   strTramasH+=cadHB+"@";
                                   cadHA=cadHA.substring(cadHA.indexOf(',')+1,cadHA.length()).trim();
                                   total++;
                                 }
                                strTramasH+=Arbol.camposTabla[i][0]+"@";
                                strTramasH+=formatoPeriodicidad(Arbol.camposTabla[i][2])+"@";
                                strTramasH+=Arbol.camposTabla[i][12]+"@";
                                strTramasH+=Arbol.camposTabla[i][4]+"@";
                                strTramasH+=Arbol.camposTabla[i][6]+"@";
                                strTramasH+=cadHA+"@";
                                total++;
                         }
                        EIGlobal.mensajePorTrace("TIConcentracion - generaArchivoParaServicio(): Total "+total, EIGlobal.NivelLog.DEBUG);
                 }

                strArc+=total+"@"+strTramasH;
                strArc=strArc.length()+"@\n"+strArc;
                if(!ArcTux.escribeLinea(strArc))
                 seGenero=false;
          }
         else
          seGenero=false;
         return seGenero;
   }

/*************************************************************************************/
/********************************************************************* cuentaCampos  */
/*************************************************************************************/
 int cuentaCampos(char car,String cadena)
  {
    int total=0;

    for(int a=0;a<cadena.length();a++)
     if(cadena.charAt(a)==car)
      total++;

    return total;
  }

/*************************************************************************************/
/*********************************************************************** armaTramas  */
/*************************************************************************************/
  EI_Tipo armaTramasFondeo(String cadenaServicio, boolean taro)
   {
         String cuentas1="";
         String pipe="|";
         String nivel="";



         int indic=0;

         EI_Tipo Aux1=new EI_Tipo(this);

     Aux1.iniciaObjeto('@','|',cadenaServicio+"|");
         TICuentaArchivo a=new TICuentaArchivo();

     if(taro)
                nivel="CPOTRESTR";

         for(int i=0;i<Integer.parseInt(Aux1.camposTabla[0][3]);i++)
          {
                 cuentas1 =Aux1.camposTabla[0][4+(i*5)]+pipe; //Cuenta
                 cuentas1+=nivel+pipe;  //Nivel
                 cuentas1+=""+pipe;  //Periodicidad
                 cuentas1+=""+pipe;  //Horario
                 cuentas1+="0"+pipe;  //SaldoOp1
                 cuentas1+="No"+pipe;  //InvAuto
                 cuentas1+="0"+pipe;  //SaldoOp2
                 cuentas1+=Aux1.camposTabla[0][7+(i*5)]+pipe;  //CuentaPadre
                 cuentas1+=""+pipe;  //NumeroNivel
                 cuentas1+=""+pipe;  //TipoNivel
                 cuentas1+=Aux1.camposTabla[0][8+(i*5)]+pipe; //Descripcion
                 cuentas1+="1"+pipe;  //ClaveProducto
                 cuentas1+="1";  //NivelTrama
                 cuentas1+="@";
                 indic=(8+(i*5))+1;
                 EIGlobal.mensajePorTrace("TIConcentracion - armaTramasFondeo(): Insertando cuenta "+cuentas1, EIGlobal.NivelLog.DEBUG);
                 a.actualizaCuenta(cuentas1);
          }

         for(int j=0;j<Integer.parseInt(Aux1.camposTabla[0][indic]);j++)
          {
         cuentas1 =Aux1.camposTabla[0][(indic+1)+(5*j)]+pipe; //Cuenta
                 cuentas1+=nivel+pipe;  //Nivel

                 cuentas1+=""+pipe;//cambiaPeriodicidad(Aux1.camposTabla[0][(indic+3)+(7*j)])+pipe; //Periodicidad
                 cuentas1+=""+pipe;//Aux1.camposTabla[0][(indic+7)+(7*j)]+pipe;  //Horario
                 cuentas1+="0"+pipe;  //SaldoOp1
                 cuentas1+="No"+pipe; //InvAuto
                 cuentas1+="0"+pipe;  //SaldoOp2

                 cuentas1+=""+pipe;  //CuentaPadre
                 cuentas1+=""+pipe;  //NumeroNivel
                 cuentas1+=""+pipe;  //TipoNivel
                 cuentas1+=""+pipe;  //Descripcion
                 cuentas1+="1"+pipe;  //ClaveProducto
                 cuentas1+=Aux1.camposTabla[0][(indic+5)+(5*j)];  //NivelTrama
                 cuentas1+="@";
                 EIGlobal.mensajePorTrace("TIConcentracion - armaTramasFondeo(): Agregando cuenta "+cuentas1, EIGlobal.NivelLog.DEBUG);
                 a.agregaCuenta(cuentas1);
          }

         a.cadenaCuentas();
     return a.Todas;
   }
}