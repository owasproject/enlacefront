package mx.altec.enlace.servlets;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.bo.TICuentaArchivo;
import mx.altec.enlace.bo.TI_CuentaDispersion;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


import java.io.*;
import java.util.*;
import java.lang.reflect.*;
import java.sql.*;

/**
** TIConMovtos is a blank servlet to which you add your own code.
*/
public class TIConMovtos extends BaseServlet
{
	short  sucursalOpera=(short)787;

    public void doGet( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException{
		defaultAction( req, res );
	}

	public void defaultAction( HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
       String clavePerfil="";
       String usuario="";
       String contrato="";

	   String modulo=(String) req.getParameter("Modulo");
	   if(modulo==null)
		 modulo="0";

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

	   EIGlobal.mensajePorTrace("TIConMovtos - execute(): Entrando a Consultas Tesoreria. Modulo="+modulo, EIGlobal.NivelLog.INFO);

       boolean sesionvalida=SesionValida( req, res );
       if(sesionvalida)
         {
		   //Variables de sesion ...
		   sucursalOpera=Short.parseShort(IEnlace.SUCURSAL_OPERANTE);
		   contrato=(session.getContractNumber()==null)?"":session.getContractNumber();
		   usuario=(session.getUserID8()==null)?"":session.getUserID8();
		   clavePerfil=(session.getUserProfile()==null)?"":session.getUserProfile();

		   EIGlobal.mensajePorTrace("Inicio bitacora en archivo TIConMovtos.java", EIGlobal.NivelLog.INFO);

           if(modulo.equals("0")) {
        	   //TODO CU3191, CU3201 Inicio de flujos para consulta de saldos
               /**
    	 		 * VSWF- FVC -I
    	 		 * 17/Enero/2007
    	 		 */

    			//if(((String)req.getParameter(BitaConstants.FLUJO) != null))
    				//if(!((String)req.getParameter(BitaConstants.FLUJO)).equals("null")){
        	   if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
        	   try{
	    		    BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
	    	 		BitaHelper bh = new BitaHelperImpl(req, session, sess);
	    	 		if((String)req.getParameter(BitaConstants.FLUJO)!= null){
	    	 			bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
	    	 		}
	    	 		else{
	    	 			bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO ));
	    	 		}
	    	 		BitaTransacBean bt = new BitaTransacBean();
	    	 		bt = (BitaTransacBean)bh.llenarBean(bt);

	    	 		//if(((String)req.getParameter(BitaConstants.FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC))
	    	 		if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC))
	    	 			bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC_ENTRA);

	    	 		if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP))
	    	 			bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP_ENTRA);

	    	 		bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());


	    	 		BitaHandler.getInstance().insertBitaTransac(bt);
	    	 		}catch(Exception e){
	    	 			e.printStackTrace();
	    	 		}
        	   }
    		   //}
    	 		/**
    	 		 * VSWF- FVC -F
    	 		 */
             iniciaConsultar( req, res );
           }else
			if(modulo.equals("1"))
             generaTablaConsultar(clavePerfil, usuario,contrato, req, res );
         }
		else
		if(sesionvalida)
		 {
           req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
           evalTemplate(IEnlace.ERROR_TMPL, req, res);
		 }
		EIGlobal.mensajePorTrace("TIConMovtos - execute(): Saliendo de Consultas Tesoreria.", EIGlobal.NivelLog.INFO);
    }


/*************************************************************************************/
/************************************* trae arbol de estructura de concentracion     */
/*************************************************************************************/
    public String consultaEstructuraConcentracion(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	 {
	   String nombreArchivo="";
	   String tipoArbol="C";

	   String tabla="";
	   String Trama="";
	   String Result="";
	   Hashtable hs = null;

	   String arcLinea="";
	   String cadenaTCT="";
	   String mensajeError="";
	   String strTipoMov="";
	   String tipo="";
	   String arcAyuda="";

	   boolean comuError=false;

	   EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
	   EI_Tipo TCTArchivo=new EI_Tipo(this);
	   EI_Tipo Anterior=new EI_Tipo(this);

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

		//CCB - Modificaci�n para recibir divisa
        String Divisa = req.getParameter("selDivisa");

        if(Divisa==null)
        {
		    if (sess.getAttribute("ctasUSD").equals("Existe"))
		    {
		  	  if(sess.getAttribute("ctasMN").equals("Existe"))
		  	  {
		 		Divisa = "MN";
		  	  }
		  	  else
		  	  {
		   		Divisa = "USD";
		  	  }
		    }
		    else
		    {
		  	  Divisa="MN";
		    }
	    }



	   tipo=(String) req.getParameter("Tipo");
	   if(tipo==null)
		  {tipo="C"; strTipoMov="Concentraci&oacute;n";}
	   else
	      strTipoMov=(tipo.equals("C"))?"Concentraci&oacute;n":"Dispersi&oacute;n";
	   arcAyuda=(tipo.equals("C"))?"s29050h":"s29060h";

	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraConcentracion(): Iniciando pantalla de Consulta de movimientos de concentracion.", EIGlobal.NivelLog.INFO);

	   ServicioTux TuxGlobal = new ServicioTux();
	   //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   //Trama="2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@";
	   Trama="2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@|"+Divisa+"|";
	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraConcentracion(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

		try
		 {
		   hs=TuxGlobal.web_red(Trama);
      	   Result=(String) hs.get("BUFFER");
		 }
		catch( java.rmi.RemoteException re )
		 {
		   re.printStackTrace();
		   Result=null;
		 }
		catch (Exception e) {}

	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraConcentracion(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);

	   if(Result==null || Result.equals("null"))
	    {
		  EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraConcentracion(): El servicio no funciono correctamente: (null) ", EIGlobal.NivelLog.INFO);

		  //######################## Solo es temporal
		  //Result="ERROR";
		  //cadenaTCT="ERROR";
		  //######################## Solo es temporal

		  comuError=true;
		  mensajeError="Su transacci&oacute;n no puede ser atendida en este momento.<br>Por favor intente m&aacute;s tarde.";
		}
	   else
		{
		  nombreArchivo=Result.substring(Result.lastIndexOf('/')+1,Result.length());

		  ArchivoRemoto archR = new ArchivoRemoto();
		  if(!archR.copia(nombreArchivo))
			EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraConcentracion(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
		  else
		    EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraConcentracion(): Copia remota OK.", EIGlobal.NivelLog.INFO);

		  nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArchivo;
		  EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraConcentracion(): Path NombreArchivo-> "+nombreArchivo, EIGlobal.NivelLog.INFO);

		   if(ArcEnt.abreArchivoLectura()){
			  arcLinea=ArcEnt.leeLinea();
			  if(arcLinea.substring(0,2).equals("OK"))
				cadenaTCT=arcLinea.substring(0,arcLinea.length())+" @";
			  else {
				    comuError=true;
				    if(arcLinea.substring(0,5).trim().equals("ERROR"))
				       mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente m&aacute;s tarde.";
				    else {
					      mensajeError="La informaci&oacute;n no se recibio correctamente. <br>Intente m&aacute;as tarde.";
					      EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): No se reconocio el encabezado."+arcLinea, EIGlobal.NivelLog.INFO);
				          //mensajeError=arcLinea.substring(16,arcLinea.length());
				        }
			   }
			  ArcEnt.cierraArchivo();
			}
		   else
			{

			  comuError=true;
			  mensajeError="No se pudo obtener la informaci&oacute;n del archivo. Por favor intente mas tarde.";
			}
		 }

	   if(comuError){
		  //despliegaPaginaError(mensajeError,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
		  tabla="";
		}
	   else	{
		      Anterior=armaTramas(cadenaTCT);
		      TCTArchivo.iniciaObjeto(tramaConcentracion(Anterior));
		      tabla=generaTabla("Estructura de "+strTipoMov,0,TCTArchivo);
		    }
	   return tabla;
	 }

/*************************************************************************************/
/************************************* trae arbol de estructura de concentracion     */
/*************************************************************************************/
    public String consultaEstructuraDispersion(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	 {
	   String nombreArchivo="";
	   String tipoArbol="D";

	   String tabla="";
	   String Trama="";
	   String Result="";
	   Hashtable hs = null;

	   String arcLinea="";
	   String cadenaTCT="";
	   String mensajeError="";
	   String strTipoMov="";
	   String tipo="";
	   String arcAyuda="";

	   boolean comuError=false;

	   EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
	   EI_Tipo TCTArchivo=new EI_Tipo(this);
	   EI_Tipo Anterior=new EI_Tipo(this);

	   tipo=(String) req.getParameter("Tipo");
	   if(tipo==null)
		 {tipo="D"; strTipoMov="Dispersi&oacute;n";}
	   else
		 strTipoMov=(tipo.equals("C"))?"Concentraci&oacute;n":"Dispersi&oacute;on";
	   arcAyuda=(tipo.equals("C"))?"s29050h":"s29060h";

	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraDispersion(): Iniciando pantalla de consulta de movimientos de dispersion.", EIGlobal.NivelLog.INFO);

	   ServicioTux TuxGlobal = new ServicioTux();
	   //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

		//CCB - Modificaci�n para recibir divisa
        String Divisa = req.getParameter("selDivisa");
        if(Divisa==null)
        {
		    if (sess.getAttribute("ctasUSD").equals("Existe"))
		    {
		  	  if(sess.getAttribute("ctasMN").equals("Existe"))
		  	  {
		 		Divisa = "MN";
		  	  }
		  	  else
		  	  {
		   		Divisa = "USD";
		  	  }
		    }
		    else
		    {
		  	  Divisa="MN";
		    }
	    }

	   //Trama="2EWEB|"+session.getUserID()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@";
	   Trama="2EWEB|"+session.getUserID8()+"|TI02|"+session.getContractNumber()+"|"+session.getUserID8()+"|"+session.getUserProfile()+"|"+session.getContractNumber()+"@"+tipoArbol+"@|"+Divisa+"|";
	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraDispersion(): Trama entrada: "+Trama, EIGlobal.NivelLog.INFO);

		try
		 {
		   hs=TuxGlobal.web_red(Trama);
      	   Result=(String) hs.get("BUFFER");
		 }
		catch( java.rmi.RemoteException re )
		 {
		   re.printStackTrace();
		   Result=null;
		 }
		catch (Exception e) {}

	   EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraDispersion(): Trama salida: "+Result, EIGlobal.NivelLog.INFO);

	   if(Result==null || Result.equals("null"))
	    {
		  EIGlobal.mensajePorTrace("TIConMovtos - consultaEstructuraDispersion(): El servicio no funciono correctamente: (null) ", EIGlobal.NivelLog.INFO);

		  //######################## Solo es temporal
		  //Result="ERROR";
		  //cadenaTCT="ERROR";
		  //######################## Solo es temporal

		  //comuError=true;
		  mensajeError="Su transacci&oacute;n no puede ser atendida en este momento.<br>Por favor intente mas tarde.";
		}
	   else
		{
		  nombreArchivo=Result.substring(Result.lastIndexOf('/')+1,Result.length());

		  ArchivoRemoto archR = new ArchivoRemoto();
		  if(!archR.copia(nombreArchivo))
			EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraDispersion(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
		  else
		    EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraDispersion(): Copia remota OK.", EIGlobal.NivelLog.INFO);

		  nombreArchivo=Global.DIRECTORIO_LOCAL + "/" +nombreArchivo;
		  EIGlobal.mensajePorTrace( "TIConMovtos - consultaEstructuraDispersion(): Path NombreArchivo."+nombreArchivo, EIGlobal.NivelLog.INFO);

		   if(ArcEnt.abreArchivoLectura())
			{
			  arcLinea=ArcEnt.leeLinea();
			  if(arcLinea.substring(0,2).equals("OK"))
				cadenaTCT=arcLinea.substring(0,arcLinea.length())+" @";
			  else
			   {
				 comuError=true;
				 if(arcLinea.substring(0,5).trim().equals("ERROR"))
				   mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
				 else
				   {
					 mensajeError="La informaci&oacute;n no se recibio correctamente. <br>Intente mas tarde.";
					 EIGlobal.mensajePorTrace( "TIConcentracion - copiaEstructura(): No se reconocio el encabezado."+arcLinea, EIGlobal.NivelLog.INFO);
				     //mensajeError=arcLinea.substring(16,arcLinea.length());
				   }
			   }
			  ArcEnt.cierraArchivo();
			}
		   else
			{


			  comuError=true;
			  mensajeError="No se pudo obtener la informaci&oacute;n del archivo. Por favor intente mas tarde.";
			}
		 }

	   if(comuError)
		{
		  //despliegaPaginaError(mensajeError,"Consulta y mantenimiento de concentraci&oacute;n","Tesorer&iacute;a &gt; Mantenimiento de estructuras &gt; Concentraci&oacute;n", req, res);
		  tabla="";
		}
	   else
		{
		   Anterior=armaTramas(cadenaTCT);
		   TCTArchivo.iniciaObjeto(tramaConcentracion(Anterior));
		   tabla=generaTabla("Estructura de "+strTipoMov,0,TCTArchivo);
		}
	   return tabla;
	 }

/*************************************************************************************/
/************************************************** Inicio de Consultas Modulo=0     */
/*************************************************************************************/
    public int iniciaConsultar(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{


	   String strInhabiles = armaDiasInhabilesJS();
	   String tipo="";
	   String strTipoMov="";
	   String arcAyuda="";

	   StringBuffer strTabla=new StringBuffer("");
	   StringBuffer varFechaHoy=new StringBuffer("");

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   tipo=(String) req.getParameter("Tipo");
	   if(tipo==null)
		 {tipo="C"; strTipoMov="Concentraci&oacute;n";}
	   else
		 strTipoMov=(tipo.equals("C"))?"Concentraci&oacute;n":"Dispersi&oacute;n";
	   arcAyuda=(tipo.equals("C"))?"s29050h":"s29060h";

	/************************************************** Genera tabla de busqueda ... */
       strTabla.append("\n<p><br>");

       strTabla.append("\n  <table width=250 border=0 cellspacing=2 cellpadding=3 class='tabfonbla' align=center>");
	   strTabla.append("\n    <tr>");
       strTabla.append("\n     <td class='tittabdat' colspan=2> Periodo a consultar</td>");
       strTabla.append("\n    </tr>");
       strTabla.append("\n    <tr>");
       strTabla.append("\n    	<td class='textabdatcla' valign=top colspan=2>&nbsp;Estructura:&nbsp;&nbsp;&nbsp;");
       //strTabla.append("\n    		<input type='text' name='txtTitle' value='Estructura:' size='9' style='border: none; font: bolder;  background-color: transparent;'>");
       strTabla.append("\n    		<select name='selDivisa'>");
       strTabla.append("\n    			<option value='MN'>Moneda Nacional</option>");
       strTabla.append("\n    			<option value='USD'>D&oacute;lares</option>");
       strTabla.append("\n    		</select>");
       strTabla.append("\n    	<td>");
       strTabla.append("\n    </tr>");
	   strTabla.append("\n    <tr align=center>");
       strTabla.append("\n     <td class='textabdatcla' valign=top colspan=2>");
	   strTabla.append("\n      <table width=100% border=0 cellspacing=5 cellpadding=0>");
	   strTabla.append("\n        <tr>");
	   strTabla.append("\n          <td class='textabdatcla'>");
	   strTabla.append("\n          <input type=radio name='Busqueda' value='HOY' onClick='cambiaFechas(this);' checked >");
	   strTabla.append("\n          Del d&iacute;a</td>");
	   strTabla.append("\n        <td>&nbsp;</td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n        <td class='textabdatcla'>");
	   strTabla.append("\n          <input type=radio name='Busqueda' value='HIS' onClick='cambiaFechas(this);' >");
	   strTabla.append("\n          Hist&oacute;rico </td>");
	   strTabla.append("\n        <td>&nbsp;</td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n        <td class='textabdatcla' align=right> De la fecha:</td>");
	   strTabla.append("\n        <td>");
	   //strTabla.append("\n          <input type=text name=FechaIni value='"+generaFechaAnt("FECHA")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
	   strTabla.append("\n          <input type=text name=FechaIni value='");
	   strTabla.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   strTabla.append("' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(0);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Inicial' align='absmiddle'></a>");
	   strTabla.append("\n        </td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n      <tr>");
	   strTabla.append("\n       <td class='textabdatcla' align=right>A la fecha:</td>");
	   strTabla.append("\n        <td>");
	   //strTabla.append("\n          <input type=text name=FechaFin value='"+generaFechaAnt("FECHA")+"' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(1);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Final' align='absmiddle'></a>");
	   strTabla.append("\n          <input type=text name=FechaFin value='");
	   strTabla.append(EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   strTabla.append("' size=10 height=14 align='absmiddle' class='textabdatcla' onFocus='blur();' maxlength=10><a HREF=\"javascript:SeleccionaFecha(1);\"><IMG SRC=\"/gifs/EnlaceMig/gbo25410.gif\" BORDER=0 alt='Cambiar Fecha Final' align='absmiddle'></a>");
	   strTabla.append("\n        </td>");
	   strTabla.append("\n      </tr>");
	   strTabla.append("\n     </table>");
	   strTabla.append("\n    </td>");
	   strTabla.append("\n   </tr>");
       strTabla.append("\n  </table>");

	   //#################### Cambiar ...
	   strTabla.append("\n  <input type=hidden name=Numero value=");
	   strTabla.append(Integer.toString(Global.MAX_REGISTROS));
	   strTabla.append(">");
	   //strTabla.append("\n  <input type=hidden name=Numero value=10>");
	   //####################

       strTabla.append("\n  <table border=0 cellspacing=0 cellpading=0 align=center>");
	   strTabla.append("\n    <tr>");
       strTabla.append("\n      <td colspan=3 align=center><br><a href='javascript:Enviar();'><img src='/gifs/EnlaceMig/gbo25220.gif' border=0></a></td>");
       strTabla.append("\n    </tr>");
       strTabla.append("\n  </table>");

/************************************************** Arreglos de fechas para 'Hoy' ... */
varFechaHoy.append("  //Fecha De Hoy");
       varFechaHoy.append("\n  dia=new Array(");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(","+generaFechaAnt("DIA"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  mes=new Array(");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  anio=new Array(");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(");");

       varFechaHoy.append("\n\n  //Fechas Seleccionadas");
       varFechaHoy.append("\n  dia1=new Array(");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("DIA"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  mes1=new Array(");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(",");
	   varFechaHoy.append(generaFechaAnt("MES"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  anio1=new Array(");
	   varFechaHoy.append(generaFechaAnt("ANIO"));
	   varFechaHoy.append(","+generaFechaAnt("ANIO"));
	   varFechaHoy.append(");");
       varFechaHoy.append("\n  Fecha=new Array('");
	   varFechaHoy.append(generaFechaAnt("FECHA"));
	   varFechaHoy.append("','");
	   varFechaHoy.append(generaFechaAnt("FECHA"));
	   varFechaHoy.append("');");

	   req.setAttribute("FechaIni","");
	   req.setAttribute("FechaFin","");
	   req.setAttribute("Numero","0");
	   req.setAttribute("Pagina","0");
	   req.setAttribute("TotalTrans","");
	   req.setAttribute("Busqueda","");

       req.setAttribute("DiasInhabiles",strInhabiles);  //Fechas
	   req.setAttribute("VarFechaHoy",varFechaHoy.toString());  //Fechas
       req.setAttribute("Modulo","1");
       req.setAttribute("Tabla",strTabla.toString());
       req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
	   req.setAttribute("FechaAyer",generaFechaAnt("FECHA"));
	   req.setAttribute("FechaDia",EnlaceGlobal.fechaHoy("dd/mm/aaaa"));
	   req.setAttribute("FechaPrimero","01/"+EnlaceGlobal.fechaHoy("mm/aaaa"));

	   req.setAttribute("MenuPrincipal", session.getStrMenu());
	   req.setAttribute("newMenu", session.getFuncionesDeMenu());
	   req.setAttribute("Encabezado",CreaEncabezado("Consulta de movimientos de estructura de "+strTipoMov,"Tesorer&iacute;a &gt; Consulta de movimientos &gt; "+strTipoMov,arcAyuda,req));

	   req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
	   req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	   req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	   req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());
	   req.setAttribute("Tipo",tipo);

	   //TODO CU3191, CU3201 Realiza consulta

	   	/**
		 * VSWF- FVC -I
		 * 17/Enero/2007
		 */
//	   if (EnlaceMig.Global.USAR_BITACORAS.equals("ON")){
//	   try{
//	   	BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
//		BitaHelper bh = new BitaHelperImpl(req, session, sess);
//		BitaTransacBean bt = new BitaTransacBean();
//
//		bt = (BitaTransacBean)bh.llenarBean(bt);
//		if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC))
//			bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC_REALIZA_CONSULTA);
//		if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP))
//			bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP_REALIZA_CONSULTA);
//		bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
//
//
//	 		BitaHandler.getInstance().insertBitaTransac(bt);
//	 		}catch(Exception e){
//	 			e.printStackTrace();
//	 		}
//	   }
		/**
		 * VSWF- FVC -F
		 */
       evalTemplate("/jsp/TIConMovtos.jsp", req, res);
       return 1;
     }

/*************************************************************************************/
/*********************************************** Resultados de la busqueda Modulo=1  */
/*************************************************************************************/
    public int generaTablaConsultar(String clavePerfil, String usuario, String contrato, HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{

       String fechaIniSer="";
       String fechaFinSer="";

       String fechaIni=(String) req.getParameter("FechaIni");
       String fechaFin=(String) req.getParameter("FechaFin");
       String tipoBusqueda=(String) req.getParameter("Busqueda");
	   String mensajeError="Error inesperado.";

	   String Result="";
	   String Trama="";

	   String arcLinea="";
	   String tipo="";
	   String strTipoMov="";
	   String arcAyuda="";
	   String tipoOperacion="";

	   StringBuffer botones=new StringBuffer("");
	   StringBuffer tablaRegistros=new StringBuffer("");

		EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar():-> Entrando.", EIGlobal.NivelLog.INFO);
	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");
	   EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

	   boolean comuError=false;

       int pagina=Integer.parseInt((String) req.getParameter("Pagina"));
       int regPagina=Integer.parseInt((String) req.getParameter("Numero"));
       int fin=0;

	   boolean status=false;
	   int totalLineas=0;

	   //################## Cambiar ....
	   String nombreArchivo=IEnlace.LOCAL_TMP_DIR+"/"+usuario;
	   //String nombreArchivo="/tmp/DIBC";
	   //##############################

	   EI_Exportar ArcEnt=new EI_Exportar(nombreArchivo);
	   EI_Tipo TCTArchivo=new EI_Tipo(this);

	   tipo=(String) req.getParameter("Tipo");
	   if(tipo==null)
		 {tipo="C"; strTipoMov="Concentraci&oacute;n";}
	   else
		 strTipoMov=(tipo.equals("C"))?"Concentraci&oacute;n":"Dispersi&oacute;n";
	   arcAyuda=(tipo.equals("C"))?"s29130h":"s29140h";

	   ServicioTux TuxGlobal = new ServicioTux();
	   //TuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());

	   EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Generando Fechas de consulta.", EIGlobal.NivelLog.INFO);
       if(tipoBusqueda.equals("HIS"))
        {
          fechaIniSer=fechaIni;
          fechaFinSer=fechaFin;
        }
       else
        {
          fechaIniSer=EnlaceGlobal.fechaHoy("dd/mm/aaaa");
          fechaFinSer=fechaIniSer;
        }
		EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Fecha ini "+fechaIniSer+ " Fecha fin "+fechaFinSer, EIGlobal.NivelLog.INFO);

       /************************************** Ejecuta servicio recupera TCTRED ***/
       tipoOperacion="TI05";
       String Divisa = req.getParameter("selDivisa");
          	   
	   Trama = 	IEnlace.medioEntrega2  + "|" +
				usuario 			+ "|" +
				tipoOperacion		+ "|" +
				contrato			+ "|" +
				usuario 			+ "|" +
				clavePerfil 		+ "|" +
				contrato			+ "@" +
				tipo				+ "@" +
				fechaIniSer			+ "@" +
				fechaFinSer			+ "@|"+
				Divisa				+"|";
		        //fechaFinSer			+ "@";
		        //fechaFinSer			+ "@|MN|";

	   System.out.println("CCBTramaMovtosUpd --> " + Trama);



	   EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Trama entrada: "+Trama, EIGlobal.NivelLog.DEBUG);

	   try{
			Hashtable hs = TuxGlobal.web_red(Trama);
			Result= ""+ (String) hs.get("BUFFER") +"" ;
			EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Trama salida: "+Result, EIGlobal.NivelLog.DEBUG);
		}catch( java.rmi.RemoteException re ){
				re.printStackTrace();
		} catch(Exception e) {}

	   if( !(Result==null || Result.equals("null")) )
		{
		 // **********************************************************  Copia Remota
		 ArchivoRemoto archR = new ArchivoRemoto();
		 if(!archR.copia(usuario))
		   EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): No se realizo la copia remota.", EIGlobal.NivelLog.INFO);
		 // **********************************************************

		 /*************************** Recuperar Transferencias TCTArchivo *********/
		 if(ArcEnt.abreArchivoLectura())
		  {
			arcLinea=ArcEnt.leeLinea();
			ArcEnt.cierraArchivo();
			if(arcLinea.substring(0,5).trim().equals("ERROR"))
			  {
				comuError=true;
				mensajeError="Su transacci&oacute;n no puede ser atendida. Por favor intente mas tarde.";
			  }
		  }
		 else
		  {
			comuError=true;
			mensajeError="Problemas de comunicacion. Por favor intente mas tarde.";
		  }
		}
	   else
        {
          comuError=true;
          mensajeError="Error en el servicio de consulta. Por favor intente mas tarde.";
        }

/********************************************************************************/
    if(!comuError)
     {

	    TCTArchivo.iniciaObjeto('@', '|', separaArchivoMovimientos(arcLinea));

		String ArchExp=usuario+"MTIexp.doc";
		boolean exportacion = GeneraArchivoExportacionTI(TCTArchivo, ArchExp, tipo, Divisa);

       if(TCTArchivo.totalRegistros>=1)
        {
		  botones.append("\n <br>");
		  botones.append("\n <table border=0 align=center cellpadding=0 cellspacing=0>");
		  botones.append("\n  <tr>");
		  botones.append("\n   <td COLSPAN=3 align=center class='texfootpagneg'>");
          if(pagina>0)
			 botones.append("&lt; <a href='javascript:BotonPagina(0);' class='texfootpaggri'>Anterior</a>&nbsp;");

		  if(calculaPaginas(TCTArchivo.totalRegistros,regPagina)>=2)
           {
			 for(int i=1;i<=calculaPaginas(TCTArchivo.totalRegistros,regPagina);i++)
			  if(pagina+1==i)
			   {
                 botones.append("&nbsp;");
				 botones.append(Integer.toString(i));
				 botones.append("&nbsp;");
			   }
			  else
			   {
			     botones.append("&nbsp;<a href='javascript:BotonPagina(");
				 botones.append(Integer.toString(i+3));
				 botones.append(");' class='texfootpaggri'>");
				 botones.append(Integer.toString(i));
				 botones.append("</a>&nbsp;");
			   }
           }

          if(TCTArchivo.totalRegistros>((pagina+1)*regPagina))
           {
             botones.append("<a href='javascript:BotonPagina(1);' class='texfootpaggri'>Siguiente</a> &gt;");
             fin=regPagina;
           }
          else
             fin=TCTArchivo.totalRegistros-(pagina*regPagina);

 		  botones.append("\n   </td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");
   	      botones.append("\n <table border=0 cellpadding=0 cellspacing=0 align=center>");
		  botones.append("\n  <tr align=center>");

/***/	  if (exportacion) //IM3638
			{
				botones.append("\n <td align=center><br><a href = \"/Download/" + ArchExp+ "\" border=0><img  border=\"0\" src=\"/gifs/EnlaceMig/gbo25230.gif\" ></a></td>"  );
			}

		  botones.append("\n    <td align='center'><br><a href='javascript:scrImpresion();' border=0><img src='/gifs/EnlaceMig/gbo25240.gif' border=0></a></td>");
		  botones.append("\n    <td align='center'><br><a href ='TIConMovtos?Tipo=");
		  botones.append(tipo);
		  botones.append("'> <img border='0' name='imageField32' src='/gifs/EnlaceMig/gbo25320.gif' width='83' height='22' alt='Regresar'></a>	</td>");
		  botones.append("\n  </tr>");
		  botones.append("\n </table>");

/************************************************* Encabezado de la pagina ... */

		  tablaRegistros.append("\n <table border=0 cellpadding=2 cellspacing=3 align=center>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");

          tablaRegistros.append("\n <table width=100% border=0 align=center cellspacing=0 cellpadding=0>");
          tablaRegistros.append("\n  <tr>");
		  if(Divisa.equals("MN")){
			  tablaRegistros.append("\n <td class='tabmovtexbol' width='200'>Movimientos en pesos");
		  }
		  if(Divisa.equals("USD")){
			  tablaRegistros.append("\n <td class='tabmovtexbol' width='200'>Movimientos en d&oacute;lares");
		  }
          tablaRegistros.append("\n  </td>");
          tablaRegistros.append("\n  </tr>");
		  tablaRegistros.append("\n  <tr><td>&nbsp;</td></tr>");
		  tablaRegistros.append("\n  <tr>");
          tablaRegistros.append("\n    <td class='textabref'><b>&nbsp;Total de registros encontrados: <font color=red>");
		  tablaRegistros.append(Integer.toString(TCTArchivo.totalRegistros));
		  tablaRegistros.append("</font> para el contrato: <font color=red>");
		  tablaRegistros.append(contrato);
		  tablaRegistros.append("</font>&nbsp;</td>");
          tablaRegistros.append("\n    <td align=right class='textabref'><b> del <font color=red>");
		  tablaRegistros.append(fechaIni);
		  tablaRegistros.append("</font> al <font color=red>");
		  tablaRegistros.append(fechaFin);
		  tablaRegistros.append("</font></td>");
          tablaRegistros.append("\n  </tr>");
          tablaRegistros.append("\n  <tr>");
          tablaRegistros.append("\n    <td align=left class='textabref'><b>&nbsp;Pagina <font color=blue>");
		  tablaRegistros.append(Integer.toString(pagina+1));
		  tablaRegistros.append("</font> de <font color=blue>");
		  tablaRegistros.append(Integer.toString(calculaPaginas(TCTArchivo.totalRegistros,regPagina)));
		  tablaRegistros.append("</font>&nbsp;</b></td>");
          tablaRegistros.append("\n    <td align=right class='textabref'>&nbsp;<b>Registros <font color=blue>");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+1));
		  tablaRegistros.append(" - ");
		  tablaRegistros.append(Integer.toString((pagina*regPagina)+fin));
		  tablaRegistros.append("</font></b>&nbsp;</td>");
          tablaRegistros.append("\n  </tr>");
          tablaRegistros.append("\n </table>");

		  EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Desplegando tabla TCTArchivo parametros: "+Integer.toString(pagina*regPagina)+","+Integer.toString(fin), EIGlobal.NivelLog.INFO);

		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n <tr>");
		  tablaRegistros.append("\n <td>");

  	      tablaRegistros.append(seleccionaRegistros(tipo,pagina*regPagina,fin,TCTArchivo,req,Divisa));

		  tablaRegistros.append("\n </td>");
		  tablaRegistros.append("\n </tr>");
		  tablaRegistros.append("\n </table>");

          req.setAttribute("TotalTrans",TCTArchivo.strOriginal);
          req.setAttribute("Busqueda",tipoBusqueda);
          req.setAttribute("FechaIni",fechaIni);
          req.setAttribute("FechaFin",fechaFin);
          req.setAttribute("FechaHoy",EnlaceGlobal.fechaHoy("dt, dd de mt de aaaa"));
          req.setAttribute("Botones",botones.toString());
          req.setAttribute("Registros",tablaRegistros.toString());
          req.setAttribute("Numero",Integer.toString(regPagina));
          req.setAttribute("Pagina",Integer.toString(pagina));
          req.setAttribute("Modulo","1");
          req.setAttribute("Divisa",Divisa); //CSA - JPSL
          
		  req.setAttribute("MenuPrincipal", session.getStrMenu());
		  req.setAttribute("newMenu", session.getFuncionesDeMenu());
		  req.setAttribute("Encabezado",CreaEncabezado("Consulta de movimientos de estructura de "+strTipoMov,"Tesorer&iacute;a &gt; Consulta de movimientos &gt; "+strTipoMov,arcAyuda,req));

          req.setAttribute("NumContrato", (session.getContractNumber()==null)?"":session.getContractNumber());
		  req.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
		  req.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
		  req.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

		  req.setAttribute("Tipo",tipo);
		  if(tipo.equals("C"))
		    req.setAttribute("Estructura",consultaEstructuraConcentracion(req,res));
		  else
			req.setAttribute("Estructura",consultaEstructuraDispersion(req,res));

		  //TODO CU3191, CU3201 Realiza consulta
		  /**
			 * VSWF - FVC - I
			 * 17/Enero/2007
			 */
		  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.equals("ON")){
		  try{
		  	BitaConstants.archivo = getServletContext().getRealPath("/") + "logBitacora.txt";
			BitaHelper bh = new BitaHelperImpl(req, session, sess);
			BitaTransacBean bt = new BitaTransacBean();

			bt = (BitaTransacBean)bh.llenarBean(bt);
			if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC))
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_CONC_REALIZA_CONSULTA);
			if(((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO)).equals(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP))
				bt.setNumBit(BitaConstants.ER_TESO_INTEL_CONS_MOV_DISP_REALIZA_CONSULTA);
			bt.setContrato((session.getContractNumber() == null)?" ":session.getContractNumber());
			bt.setNombreArchivo((usuario == null)?" ":usuario + "MTIexp.doc");
			bt.setServTransTux("TI05");
			/*BMB-I*/
			if(Result!=null){
    			if(Result.substring(0,2).equals("OK")){
    				bt.setIdErr("TI050000");
    			}else if(Result.length()>8){
	    			bt.setIdErr(Result.substring(0,8));
	    		}else{
	    			bt.setIdErr(Result.trim());
	    		}
    		}
			/*BMB-F*/

		 		BitaHandler.getInstance().insertBitaTransac(bt);
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		  }
			/**
			 * VSWF - FVC - F
			 */
          evalTemplate("/jsp/TIConMovtos.jsp", req, res);
        }
       else
		{
		  EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): No existen Registros.", EIGlobal.NivelLog.INFO);
          despliegaMensaje(tipo,"No se encontraron movimientos para esta estructura.","Consulta de movimientos de estructura de "+strTipoMov,"Tesorer&iacute;a &gt; Consulta de movimientos &gt; "+strTipoMov, req, res);
		}
     }
    else
	 {
	   EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar(): Error."+mensajeError, EIGlobal.NivelLog.INFO);
       despliegaMensaje(tipo,mensajeError,"Consulta de movimientos de estructura de "+strTipoMov,"Tesorer&iacute;a &gt; Consulta de movimientos &gt; "+strTipoMov, req, res);
	 }
	 EIGlobal.mensajePorTrace("TIConMovtos - generaTablaConsultar():-> Saliendo.", EIGlobal.NivelLog.INFO);
    return 1;
  }

/*************************************************************************************/
/******************************************************* separaArchivoMovimientos    */
/*************************************************************************************/
  String separaArchivoMovimientos(String strLinea)
	{
	  StringBuffer objTrama=new StringBuffer("");

	  int pos=0;
	  int fin=0;
	  int reg=0;
	  int totC=12;

      pos=noA(3,strLinea);
	  reg=Integer.parseInt(strLinea.substring(pos,strLinea.indexOf("@",pos)));
	  for(int i=0;i<reg;i++)
	   {
		 pos=noA(4+(i*totC),strLinea);
		 fin=noA((4+totC)+(i*totC),strLinea);
		 objTrama.append(strLinea.substring(pos,fin));
		 objTrama.append("|");
	   }
	  EIGlobal.mensajePorTrace("TIConMovtos - separaArchivoMovimientos(): Completa es: "+objTrama.toString(), EIGlobal.NivelLog.INFO);
	  return objTrama.toString();
	}

  int noA(int num,String strLinea)
	{
  	  int i,j,pos=0;

	  for(i=0,j=0;j<num;i++)
	   if(strLinea.charAt(i)=='@')
	  	{pos=i; j++;}
	  return pos+1;
	}

/*************************************************************************************/
/**************************************************** Regresa el total de paginas    */
/*************************************************************************************/
   int calculaPaginas(int total,int regPagina)
    {
      Double a1=new Double(total);
      Double a2=new Double(regPagina);
      double a3=a1.doubleValue()/a2.doubleValue();
      Double a4=new Double(total/regPagina);
      double a5=a3/a4.doubleValue();
      int totPag=0;

      if(a3<1.0)
        totPag=1;
      else
       {
         if(a5>1.0)
          totPag=(total/regPagina)+1;
         if(a5==1.0)
          totPag=(total/regPagina);
       }
      return totPag;
    }

/*************************************************************************************/
/******************************************************************* fecha Consulta  */
/*************************************************************************************/
   String generaFechaAnt(String tipo)
    {
      StringBuffer strFecha = new StringBuffer("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);

      do
       {
         Cal.add(Calendar.DATE,-1);
       }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);

      if(tipo.equals("FECHA"))
       {
         if(Cal.get(Calendar.DATE) <= 9)
		  {
             strFecha.append("0");
			 strFecha.append(Cal.get(Calendar.DATE));
			 strFecha.append("/");
		  }
         else
		  {
            strFecha.append(Cal.get(Calendar.DATE));
			strFecha.append("/");
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append("0");
			strFecha.append((Cal.get(Calendar.MONTH)+1));
			strFecha.append("/");
		  }
         else
		  {
            strFecha.append((Cal.get(Calendar.MONTH)+1));
			strFecha.append("/");
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
        }
      if(tipo.equals("DIA"))
        strFecha.append(Cal.get(Calendar.DATE));
      if(tipo.equals("MES"))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if(tipo.equals("ANIO"))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

   String traeTitular(String cuentaCargo, HttpServletRequest req)
    {
       String titular="SIN TITULAR";
	   String linea = "";
	   String cuentaaux="";
	   String ctaverificar="";

	   BufferedReader entrada;

	   /************* Modificacion para la sesion ***************/
	   HttpSession sess = req.getSession();
	   BaseResource session = (BaseResource) sess.getAttribute("session");

	   EIGlobal.mensajePorTrace("TIConMovtos - traeTitular();  traeTitular", EIGlobal.NivelLog.INFO);

	   try
		{
		    entrada = new BufferedReader( new FileReader(session.getArchivoAmb()));
   			linea = entrada.readLine();
			while ( ( linea = entrada.readLine() ) != null )
			 {
				if (  ( linea.length() > 2 ) &&
					  ( linea.substring( 0, 2 ).equals( "2;" ) ) )
				{
				   linea = linea.substring( 2, linea.length() );
                   cuentaaux=linea.substring(0,linea.indexOf(";"));
				   if (cuentaaux.equals(cuentaCargo))
				   {
				     linea=linea.substring(linea.indexOf(";")+1,linea.length());
				     titular=linea.substring(0,linea.indexOf(";"));
 			 		 EIGlobal.mensajePorTrace("***titular  "+titular, EIGlobal.NivelLog.INFO);
					 return titular;
				   }
				} //if  (  ( linea.length() > 2 ) && ( linea.substring( 0, 2 ).equals( "2;" ) ) )
	        }  //while
			entrada.close();
        } catch ( Exception e ) {
			EIGlobal.mensajePorTrace("TIConMovtos - Error al traer el cuentas " + e.toString(), EIGlobal.NivelLog.ERROR);
			e.printStackTrace();
		    return titular;
		}
	  return titular;
    }









/*************************************************************************************/
/***********************trae el nivel de una cuenta*********************************   */
/*************************************************************************************/
public String getNivel(String cuenta, String contrato, String tipoArbol)
{
	Connection  Conexion=null;
	PreparedStatement qrNivel;
	ResultSet rsNivel=null;
	String sqlNivel;
	String Tabla;
	String strNivel="";

	EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Entrando a getNivel tipoArbol="+ tipoArbol, EIGlobal.NivelLog.INFO);
    Tabla=(tipoArbol.equals("C"))?"tct_concentracio":"tct_dispersion";


	sqlNivel = "Select count(*) From "+Tabla+ " Where num_cuenta='"+cuenta+"' and num_cuenta2='"+contrato+"'";
	EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Query 0= "+sqlNivel, EIGlobal.NivelLog.ERROR);
	try
	{
		 Conexion = createiASConn(Global.DATASOURCE_ORACLE); EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Global.DATASOURCE_ORACLE= "+Global.DATASOURCE_ORACLE, EIGlobal.NivelLog.ERROR);
		 if(Conexion==null)
		 {
			 EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
		 }
		 else
		 {
		    qrNivel = Conexion.prepareStatement(sqlNivel);
		    if(qrNivel==null)
			{
				EIGlobal.mensajePorTrace("***TIConMovtos.getNivel  no se pudo crear resultset", EIGlobal.NivelLog.ERROR);
			}
			else
		    {
		  	   rsNivel = qrNivel.executeQuery();
				if(rsNivel==null)
				{
					EIGlobal.mensajePorTrace("***TIConMovtos.getNivel  no se pudo crear ResultSet", EIGlobal.NivelLog.ERROR);
				}
				else
				{
					rsNivel.next();
					strNivel  = rsNivel.getString(1);
					EIGlobal.mensajePorTrace("***TIConMovtos.getNivel resultado Query 0= "+strNivel, EIGlobal.NivelLog.ERROR);

					if (Integer.valueOf(strNivel).intValue()>0)
					{
						sqlNivel = "Select to_char(nivel) From "+Tabla+ " Where num_cuenta='"+cuenta+"' and num_cuenta2='"+contrato+"'";
						EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Query 1= "+sqlNivel, EIGlobal.NivelLog.ERROR);
						qrNivel = Conexion.prepareStatement(sqlNivel);
						if(qrNivel!=null)
						{
							rsNivel = qrNivel.executeQuery();
							if(rsNivel!=null)
							{
								rsNivel.next();
								strNivel  = rsNivel.getString(1);
								EIGlobal.mensajePorTrace("***TIConMovtos.getNivel resultado Query 1= "+strNivel, EIGlobal.NivelLog.ERROR);
							}
						}
						else EIGlobal.mensajePorTrace("***TIConMovtos.getNivel  no se pudo ejecutar Query 2", EIGlobal.NivelLog.ERROR);
					}
					else // no existe registro de esa cuenta, por lo que puede ser cuenta padre y hay que verificar que se encuentre en tct_tesorelcuen
					{
						strNivel  ="";
						sqlNivel = "Select num_cuenta_padre From tct_tesorelcuen Where num_cuenta='"+cuenta+"' and num_cuenta2='"+contrato+"' and TIPO_TRANSACCION ='"+tipoArbol+"'";
						EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Query 2= "+sqlNivel, EIGlobal.NivelLog.ERROR);
						qrNivel = Conexion.prepareStatement(sqlNivel);
						if(qrNivel==null)
						{
							EIGlobal.mensajePorTrace("***TIConMovtos.getNivel  no se pudo ejecutar Query 2", EIGlobal.NivelLog.ERROR);
						}
						else
						{
							rsNivel = qrNivel.executeQuery();
							if(rsNivel!=null)
							{
								rsNivel.next();
								EIGlobal.mensajePorTrace("***TIConMovtos.getNivel resultado Query 2= "+rsNivel.getString(1), EIGlobal.NivelLog.ERROR);
								strNivel= (rsNivel.getString(1).trim().equals("0"))?"1":"X";	// la X significa que la cuenta no se encuentra en ninguna de las dos tablas por lo tanto hay inconsistencia de datos y en teoria no deberian seplegarse X
							}
						}
					}
				}
			}
		 }
	}catch(SQLException sqle ){
		sqle.printStackTrace();
	}finally
	{
		try
		{
		   Conexion.close();
		   rsNivel.close();
		}catch(Exception e)
			{e.printStackTrace();}
	}

	if (strNivel == null || strNivel.equals(""))
	{
		strNivel="X";
		EIGlobal.mensajePorTrace("***TIConMovtos.getNivel inconsistencia de datos la cuenta "+cuenta+ " no se encontro en ninguna de las tablas", EIGlobal.NivelLog.ERROR);
	}

	EIGlobal.mensajePorTrace("***TIConMovtos.getNivel Saliendo de getNivel = "+ strNivel, EIGlobal.NivelLog.INFO);
	return(strNivel);
}



//******************************************************************************************
//******************************************************************************************
//********************creacion de metodos implementada para incidencia IM3638	************
//****************************************************************
//*******************formatea***************************************************************
//******************************************************************************************

	 public String formatea( String cantidad )
    {
		StringBuffer Formato=new StringBuffer("");
		EIGlobal.mensajePorTrace ("TIConMovtos.formatea entrando", EIGlobal.NivelLog.INFO);

		if(cantidad ==null ||cantidad.equals("") )
		  {return "0"; }
		else
		{
			Double Value = new Double(cantidad);
			if (Value.doubleValue()<=0)
			  {return "0";}
			else
			{
				if (cantidad.indexOf('.')>-1){
					// CSA - JPSL
					// Obtiene los decimales incluyendo el punto
					String decimales = cantidad.substring(
							cantidad.indexOf('.'), cantidad.length());
					// Valida el tama�o de la cadena de los decimales, se
					// compara con 3 ya que la cadena incluye el "."
					if (decimales.length() < 3) {
						//De ser menor a 3 se complementa la cantidad para que contenga dos decimales
						for (int i = decimales.length(); i < 3; i++)
							cantidad += "0";
					} else if (decimales.length() > 3) {
						//De ser mayor a 3, se recorta la cadena para que solo tome dos decimales
						cantidad = cantidad.substring(0,
								cantidad.indexOf('.') + 3);
					}
					// CSA - JPSL
				}
				int i=0;
				while (i<cantidad.length() )
				{
					if (cantidad.charAt(i)=='.' || cantidad.charAt(i)==',')	{i++;}
					else {Formato.append(cantidad.charAt(i)); i++;}
				}
			}
		}

		EIGlobal.mensajePorTrace ("TIConMovtos.formatea saliendo", EIGlobal.NivelLog.INFO);
		return Formato.toString();
	}


	public String completaConBlancos(String cadena,int longitud)
		// devuelve una cadena de tama�o "longitud" que incluya la cadena pasada
	{
		cadena.trim();
		EIGlobal.mensajePorTrace ("TIConMovtos.CompletaConBlancos>>> entrando", EIGlobal.NivelLog.INFO);

		if (cadena.length()>longitud)
		  {cadena = cadena.substring(0,longitud);}
		else if (longitud > cadena.length())
		{
			int totalBlancos=longitud-cadena.length();
			for (int i=1;i<=totalBlancos ; i++)
			{cadena +=" ";}
		}
		EIGlobal.mensajePorTrace ("TIConMovtos.CompletaConBlancos>>> salida='"+cadena+"'", EIGlobal.NivelLog.INFO);
		return cadena;
	}


	public boolean GeneraArchivoExportacionTI(EI_Tipo TCTArchivo, String fileName, String tipo, String Divisa)
	{
		//layout del archivo
		//NUM_CUENTA2@FECHA@REFERENCIA@NUM_CUENTA@TIP_TRANS@HORARIO@NUM_CUENTA_PADRE@IMPORTE@PERIODO@DESCRIP@INVERSION@MONTO_INV
		//  0           1       2          3          4        5       6                7       8         9     10         11

			EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion->Entrando", EIGlobal.NivelLog.INFO);
			StringBuffer strTabla=new StringBuffer("");
			EI_Exportar ArcSal=new EI_Exportar(IEnlace.DOWNLOAD_PATH +	fileName);

			if(!ArcSal.creaArchivo())
			{
				EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion->Error de creaci�n de archivo ", EIGlobal.NivelLog.INFO);
				return false;
			}
			else
			{
				/***///se genera los registros y se escriben a archivo armado segun layout que envio el usuario
				for(int i=0;i<TCTArchivo.totalRegistros;i++)
				{
					strTabla.append(completaConBlancos(" ",3));											// tres blancos
					strTabla.append(getNivel(TCTArchivo.camposTabla[i][3],TCTArchivo.camposTabla[i][0],TCTArchivo.camposTabla[i][4])); // Nivel  un caracter
					strTabla.append(completaConBlancos(" ",3));											// tres blancos
																										// cuenta de cargo  13 caracteres
					String Cta=(tipo.equals("C"))?TCTArchivo.camposTabla[i][3]:TCTArchivo.camposTabla[i][6];
					strTabla.append(completaConBlancos(Cta,13));
					strTabla.append(completaConBlancos(" ",3));											// tres blancos
																										// cuenta de abono  13 caracteres
					Cta=(tipo.equals("C"))?TCTArchivo.camposTabla[i][6]:TCTArchivo.camposTabla[i][3];
					strTabla.append(completaConBlancos(Cta,13));
					strTabla.append(completaConBlancos(TCTArchivo.camposTabla[i][1],10));				// fecha            10 caracteres
					String X=formatea(TCTArchivo.camposTabla[i][7]);
					strTabla.append(completaConBlancos(X,8));											// importe		   8 caracteres 6 y 2 sin punto
					strTabla.append(completaConBlancos(" ",3));											// tres blancos
					strTabla.append(completaConBlancos(TCTArchivo.camposTabla[i][5],5));				// Hora              5 caracteres
					strTabla.append(" ");																// un blanco
					strTabla.append(completaConBlancos(TCTArchivo.camposTabla[i][2],7));				// referencia        7 caracteres
					strTabla.append("  ");																// dos blancos
					strTabla.append(completaConBlancos(TCTArchivo.camposTabla[i][9],18));				// descripcion      18 caracteres
					if(tipo.equals("C") && !Divisa.equals("USD"))
					{
						if (TCTArchivo.camposTabla[i][10].equals("")){TCTArchivo.camposTabla[i][10]="N";} //se actualiza para poder ser visualizado tambien en la tabla
						strTabla.append(TCTArchivo.camposTabla[i][10]);									// indicador de inv automatica    1 caracter
						X=formatea(TCTArchivo.camposTabla[i][11]);
						strTabla.append(completaConBlancos(X,8));										// saldo operativo inv automatica 8 caracteres 6 y 2 sin punto
					}
					strTabla.append("\n");
					//EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion->Se escribe en archivo registro : "+Integer.toString(i)+"="+strTabla.toString(), EIGlobal.NivelLog.DEBUG);
					if(!ArcSal.escribeLinea(strTabla.toString()))
					{
						EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion->Error de escritura a archivo ", EIGlobal.NivelLog.INFO);
						ArcSal.cierraArchivo();
						return false;
					}
					strTabla.delete(0,strTabla.length());
				}
			}
			ArcSal.cierraArchivo();

			ArchivoRemoto archR = new ArchivoRemoto();
			if(!archR.copiaLocalARemoto(fileName,"WEB"))
			{
				EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion-> No se creo archivo de exportacion", EIGlobal.NivelLog.INFO);
				return false;
			}
			else
				EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion-> Se creo archivo de exportacion="+fileName, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace ("TIConMovtos.GeneraArchivoExportacion->Saliendo", EIGlobal.NivelLog.INFO);
			return true;
	}


/*************************************************************************************/
/********************************* tabla de movimientos seleccionados por intervalo  */
/*************************************************************************************/
   String seleccionaRegistros(String tipo, int ini, int fin, EI_Tipo Trans, HttpServletRequest req, String Divisa)
    {
      StringBuffer strTabla=new StringBuffer("");
      String colorbg="";
	  String str="";
	  String strComprobante="";

	  EIGlobal.mensajePorTrace("TIConMovtos - seleccionaRegistros():-> Entrando", EIGlobal.NivelLog.DEBUG);

	  /***/ //incidencia IM3638
	  int c[]={3,6,1,5,7,2,9,10,11};
	  String  align[]={"left","left","center","center","right","left","left","center","right","right"};
 	  int indexCD=0; /***/// indice variable segun el tipo de tabla

      EI_Tipo Consultas=new EI_Tipo(this);

	  /************* Modificacion para la sesion ***************/
	  HttpSession sess = req.getSession();
	  BaseResource session = (BaseResource) sess.getAttribute("session");
	  EIGlobal EnlaceGlobal = new EIGlobal(session , getServletContext() , this  );

      if(Trans.totalRegistros>=1)
       {

		 Consultas.iniciaObjeto(Trans.strOriginal);

         strTabla.append("\n<table border=0 align=center>");

         strTabla.append("\n<tr>");
		 strTabla.append("\n<th class='tittabdat'>&nbsp; Nivel &nbsp; </th>");
         if(tipo.trim().equals("C"))
		   {
		     strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Cargo &nbsp; </th>");
			 strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Abono &nbsp; </th>");
			 if(Divisa.equals("USD")){
				 indexCD=7;
			 }else{
				 indexCD=9;
			 }
		   }
		  else
	       {
			 strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Abono &nbsp; </th>");
			 strTabla.append("\n<th class='tittabdat'>&nbsp; Cuenta Cargo &nbsp; </th>");
			 indexCD=7;
		   }

		 strTabla.append("\n<th class='tittabdat'>&nbsp; Fecha &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat'>&nbsp; Hora &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat'>&nbsp; Importe &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat'>&nbsp; Referencia &nbsp; </th>");
         strTabla.append("\n<th class='tittabdat'>&nbsp; Descripcion &nbsp; </th>");
/***/ //modificacion incidencia IM3638
         if(tipo.trim().equals("C") && !Divisa.equals("USD"))
		   {
		     strTabla.append("\n<th class='tittabdat'>&nbsp; Inversi�n &nbsp; </th>");
			 strTabla.append("\n<th class='tittabdat'>&nbsp; Cantidad Invertida &nbsp; </th>");
		   }
/***/
		 strTabla.append("\n</tr>");

         EIGlobal.mensajePorTrace("TIConMovtos - seleccionaRegistros(): Generando la Tabla.", EIGlobal.NivelLog.INFO);

         if(fin>Trans.totalRegistros)
          fin=Trans.totalRegistros;

		 EIGlobal.mensajePorTrace("TIConMovtos - seleccionaRegistros(): Consultar De: "+Integer.toString(ini) +" a " +Integer.toString(ini+fin), EIGlobal.NivelLog.INFO);

		 EnlaceGlobal.formateaImporte(Trans,7);
		 EnlaceGlobal.formateaImporte(Trans,11);
         for(int i=ini;i<(ini+fin);i++)
          {
			if((i%2)==0)
             colorbg="class='textabdatobs'";
            else
             colorbg="class='textabdatcla'";
            strTabla.append("\n<tr>");

			String N =getNivel(Trans.camposTabla[i][3], Trans.camposTabla[i][0], Trans.camposTabla[i][4]);
			strTabla.append("<td ");strTabla.append(colorbg);strTabla.append(" align=center> &nbsp;");strTabla.append(N);strTabla.append(" &nbsp;</td>");

			for(int j=0;j<indexCD;j++)
             {
			   //EIGlobal.mensajePorTrace("TIConMovtos - seleccionaRegistros(): valor del registro " +Integer.toString(j)+" = "+Trans.camposTabla[i][c[j]], EIGlobal.NivelLog.DEBUG);
			   strTabla.append("<td ");
			   strTabla.append(colorbg);
			   strTabla.append(" align=");
			   strTabla.append(align[j]);
			   strTabla.append("> &nbsp;");
			   strTabla.append(Trans.camposTabla[i][c[j]]);
			   strTabla.append(" &nbsp;</td>");
             }
            strTabla.append("\n</tr>");
          }
         strTabla.append("\n</table>");
       }
	  EIGlobal.mensajePorTrace("TIConMovtos - seleccionaRegistros():-> saliendo", EIGlobal.NivelLog.DEBUG);
      return strTabla.toString();
    }

/*************************************************************************************/
/************************************************************* Arma dias inhabiles   */
/*************************************************************************************/
  String armaDiasInhabilesJS()
   {
      StringBuffer resultado=new StringBuffer("");
      Vector diasInhabiles;

	  int indice=0;

      diasInhabiles=CargarDias(1);
      if(diasInhabiles!=null)
	   {
		 resultado.append(diasInhabiles.elementAt(indice).toString());
		 for(indice=1; indice<diasInhabiles.size(); indice++)
		  {
  		    resultado.append(", ");
			resultado.append(diasInhabiles.elementAt(indice).toString());
		  }
       }
      return resultado.toString();
	}

/*************************************************************************************/
/**************************************************** Carga los dias de las tablas   */
/*************************************************************************************/
  public Vector CargarDias(int formato)
	{
			EIGlobal.mensajePorTrace("***TIConMovtos.CargarDias Entrando a CargaDias ", EIGlobal.NivelLog.INFO);

	Connection  Conexion=null;
	PreparedStatement qrDias;
	ResultSet rsDias=null;
	boolean    estado;
	String     sqlDias;
	Vector diasInhabiles = new Vector();

	 sqlDias = "Select to_char(fecha, 'dd'), to_char(fecha, 'mm'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI' and fecha <= sysdate";
	 EIGlobal.mensajePorTrace("***TIConMovtos.CargarDias Query"+sqlDias, EIGlobal.NivelLog.ERROR);
	 try
	  {
		Conexion = createiASConn(Global.DATASOURCE_ORACLE);
			 if(Conexion!=null)
		 {
		    qrDias = Conexion.prepareStatement(sqlDias);

		    if(qrDias!=null)
		    {
		  	  rsDias = qrDias.executeQuery();
		       if(rsDias!=null)
		       {
		          while( rsDias.next()){
		              String dia  = rsDias.getString(1);
		              String mes  = rsDias.getString(2);
		              String anio = rsDias.getString(3);
		              if(formato == 0)
		               {
		                  dia  =  Integer.toString( Integer.parseInt(dia) );
		                  mes  =  Integer.toString( Integer.parseInt(mes) );
		                  anio =  Integer.toString( Integer.parseInt(anio) );
		               }

		              String fecha = dia.trim() + "/"  + mes.trim() + "/" + anio.trim();
		              diasInhabiles.addElement(fecha);
					  //******************
		          }
				  //while (rsDias.fetchNext() == 0); //Aqui no existe ese metodo, ??? perguntar a Ruben
		             //*******************
		       }
				   else
		         EIGlobal.mensajePorTrace("***TIConMovtos.CargarDias  Cannot Create ResultSet", EIGlobal.NivelLog.ERROR);
		    }
				else
		      EIGlobal.mensajePorTrace("***TIConMovtos.CargarDias  Cannot Create Query", EIGlobal.NivelLog.ERROR);
		 }
			 else
				EIGlobal.mensajePorTrace("***TIConMovtos.CargarDias Error al intentar crear la conexion", EIGlobal.NivelLog.ERROR);
	   }catch(SQLException sqle ){
	   	sqle.printStackTrace();
	   }finally
		  {
			try
			 {
			   Conexion.close();
			   rsDias.close();
			 }catch(Exception e) {}
		  }
	 return(diasInhabiles);
	}


//######################################################################################
//######################################################################################
//######################################################################################
//######################################################################################
  private String formatoPeriodicidad(String dia)
	{
	  String per="0";
	  if(dia.equals("Diaria"))
		per="0";
	  if(dia.equals("Lunes"))
		per="2";
	  if(dia.equals("Martes"))
		per="3";
	  if(dia.equals("Miercoles"))
		per="4";
	  if(dia.equals("Jueves"))
		per="5";
	  if(dia.equals("Viernes"))
		per="6";

	  EIGlobal.mensajePorTrace( "TIConcentracion - formatoPeriodicidad(): Dia: "+dia+" Codigo: "+per, EIGlobal.NivelLog.INFO);

	  return per;
	}

  private String cambiaPeriodicidad(String dia)
	{
	  String per="Diaria";

	  if(dia.equals("0"))
		per="Diaria";
	  if(dia.equals("2"))
		per="Lunes";
	  if(dia.equals("3"))
		per="Martes";
	  if(dia.equals("4"))
		per="Miercoles";
	  if(dia.equals("5"))
		per="Jueves";
	  if(dia.equals("6"))
		per="Viernes";

	  EIGlobal.mensajePorTrace( "TIConcentracion - formatoPeriodicidad(): Dia: "+dia+" Codigo: "+per, EIGlobal.NivelLog.INFO);

	  return per;
	}

  private String tramaConcentracion(EI_Tipo Tramas)
	{
	    StringBuffer tramaOrdenar=new StringBuffer("");
		StringBuffer nuevaTramaCuentas=new StringBuffer("");

		String tramaOrdenada="";

		int a=0;

		EI_Tipo TmpCuentas= new EI_Tipo(this);

		EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Entrando a metodo", EIGlobal.NivelLog.INFO);

		if(Tramas.totalRegistros<3)
		  return Tramas.strOriginal;

        /*cuenta, cuenta padre, descripcion, periodicidad, saldo op.*/
		for(int i=0;i<Tramas.totalRegistros;i++)
		 {
		   tramaOrdenar.append("@");
		   tramaOrdenar.append(Tramas.camposTabla[i][0]);
		   tramaOrdenar.append("|");
		   if(Tramas.camposTabla[i][7].trim().equals("0"))
			 tramaOrdenar.append(" |");
		   else
			{
			  tramaOrdenar.append(Tramas.camposTabla[i][7].trim());
			  tramaOrdenar.append("|");
			}
		   tramaOrdenar.append(Integer.toString(i));
		   tramaOrdenar.append("|");
		   tramaOrdenar.append(formatoPeriodicidad(Tramas.camposTabla[i][2]));
		   tramaOrdenar.append("|");
		   tramaOrdenar.append(Tramas.camposTabla[i][4]);
		   tramaOrdenar.append("|");
		   tramaOrdenar.append("N|");
		   tramaOrdenar.append("N|");
		   tramaOrdenar.append(Tramas.camposTabla[i][11]);
		 }

		EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Trama para ordenar: "+tramaOrdenar.toString(), EIGlobal.NivelLog.INFO);
		tramaOrdenada=ordenaArbol(tramaOrdenar.toString());
		EIGlobal.mensajePorTrace( "TIConcentracion - tramaConcentracion(): Trama ordenada: "+tramaOrdenada, EIGlobal.NivelLog.INFO);
		TmpCuentas.iniciaObjeto(tramaOrdenada.substring(1,tramaOrdenada.length())+"@");

		for(int i=0;i<Tramas.totalRegistros;i++)
		 {
		   a=Integer.parseInt(TmpCuentas.camposTabla[i][2]);
		   for(int b=0;b<12;b++)
			{
			  nuevaTramaCuentas.append(Tramas.camposTabla[a][b]);
			  nuevaTramaCuentas.append("|");
			}
		   nuevaTramaCuentas.append(Tramas.camposTabla[a][12]);
		   nuevaTramaCuentas.append("@");
		 }
	  return nuevaTramaCuentas.toString();
	}

  private String ordenaArbol(String tramaEntrada)
	{
		StringBuffer tramaSalida;

		Vector cuentas, ctasOrden;

		TI_CuentaDispersion ctaAux;

		int a, b;

		b = 1;
		cuentas = creaCuentas(tramaEntrada);
		ctasOrden = new Vector();
		while(cuentas.size() > 0)
			{
			for(a=0;a<cuentas.size();a++)
				{
				ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
				if(ctaAux.nivel() == b) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
				}
			b++;
			}
		cuentas = ctasOrden;
		ctasOrden = new Vector();
		for(a=0;a<cuentas.size();a++)
			{
			ctaAux = ((TI_CuentaDispersion)cuentas.get(a));
			if(ctaAux.nivel() == 1) {cuentas.remove(ctaAux); ctasOrden.add(ctaAux); a--;}
			}
		while(cuentas.size() > 0)
			{
			ctaAux = ((TI_CuentaDispersion)cuentas.get(0));
			ctasOrden.add(ctasOrden.indexOf(ctaAux.getPadre()) + 1,ctaAux);
			cuentas.remove(ctaAux);
			}
		cuentas = ctasOrden;
		tramaSalida = new StringBuffer("");
		for(a=0;a<cuentas.size();a++)
		  tramaSalida.append( ((TI_CuentaDispersion)cuentas.get(a)).trama() );

		return tramaSalida.toString();
	}

  public Vector creaCuentas(String tramaCuentas)
	{
		Vector cuentas = null;
		StringTokenizer tokens;
		TI_CuentaDispersion ctaAux1, ctaAux2;
		int a, b;

		try
			{
			cuentas = new Vector();
			tokens = new StringTokenizer(tramaCuentas,"@");
			while(tokens.hasMoreTokens())
				{
				cuentas.add(TI_CuentaDispersion.creaCuentaDispersion("@" + tokens.nextToken()));
				for(a=0;a<cuentas.size();a++)
					{
					ctaAux1 = (TI_CuentaDispersion)cuentas.get(a);
					if(!ctaAux1.posiblePadre.equals(""))
						for(b=0;b<cuentas.size();b++)
							{
							ctaAux2 = (TI_CuentaDispersion)cuentas.get(b);
							if(ctaAux2.getNumCta().equals(ctaAux1.posiblePadre))
								{
								ctaAux1.setPadre(ctaAux2);
								b=cuentas.size();
								}
							}
					}
				}
			}
		catch(Exception e)
			{EIGlobal.mensajePorTrace("<DEBUG RVV> Error en creaCuentas(): " + e.toString(), EIGlobal.NivelLog.ERROR);}
		finally
			{return cuentas;}
		}
//######################################################################################
//######################################################################################
//######################################################################################
//######################################################################################

/*************************************************************************************/
/**************************************************************** despliega Mensaje  */
/*************************************************************************************/
 public void despliegaMensaje(String tipo, String mensaje, String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
	 String contrato_ ="";
	 String arcAyuda="";

	 /************* Modificacion para la sesion ***************/
	 HttpSession sess = request.getSession();
	 BaseResource session = (BaseResource) sess.getAttribute("session");
	 EIGlobal Global = new EIGlobal(session , getServletContext() , this  );

	 contrato_=session.getContractNumber();
	 if(contrato_==null)
	   contrato_ ="";
	 arcAyuda=(tipo.equals("C"))?"s29050h":"s29060h";

	 request.setAttribute( "FechaHoy",Global.fechaHoy( "dt, dd de mt de aaaa" ) );
	 request.setAttribute( "Error", mensaje );
	 request.setAttribute( "URL", "TIConMovtos?Tipo="+tipo+"&Modulo=0" );

	 request.setAttribute( "MenuPrincipal", session.getStrMenu() );
	 request.setAttribute( "newMenu", session.getFuncionesDeMenu());
	 request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, arcAyuda,request) );

	 request.setAttribute("NumContrato", contrato_);
	 request.setAttribute("NomContrato", (session.getNombreContrato()==null)?"":session.getNombreContrato());
	 request.setAttribute("NumUsuario", (session.getUserID8()==null)?"":session.getUserID8());
	 request.setAttribute("NomUsuario", (session.getNombreUsuario()==null)?"":session.getNombreUsuario());

	 evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
   }

/*************************************************************************************/
/************************************************ genera Tabla del arbol de cuentas  */
/*************************************************************************************/
   String generaTabla(String titulo, int ind, EI_Tipo Trans)
	{
	   String[] titulos={ "",
	                      "Cuenta",
		                  "Nivel",
						  "Periodicidad",
						  "Horario",
						  "Saldo Operativo",
		                  "Inv. Autom&aacute;tica",
		                  "Saldo Operativo",
		                  ""};

	   int datos[]={7,0,0,12,2,3,4,5,6,7,8};
	   int values[]={13,0,1,2,3,4,5,6,7,8,9,10,11,12};
	   int align[]={0,1,1,1,2,1,2,0};

	   EIGlobal.mensajePorTrace("TIConcentracion - generatabla(): Generando Tabla de Estructura.", EIGlobal.NivelLog.INFO);

	   titulos[0]=titulo;
	   datos[1]=ind;

	   return Trans.generaTablaIntegra(titulos,datos,values,align);
	}

/*************************************************************************************/
/********************************************************************** traeCuentas  */
/*************************************************************************************/
  EI_Tipo armaTramas(String cadenaServicio)
   {
	 String cuentas1="";
	 String pipe="|";

	 int indic=0;

	 EI_Tipo Aux1=new EI_Tipo(this);

     Aux1.iniciaObjeto('@','|',cadenaServicio+"|");
	 TICuentaArchivo a=new TICuentaArchivo();

	 for(int i=0;i<Integer.parseInt(Aux1.camposTabla[0][3]);i++)
	  {
		 cuentas1 =Aux1.camposTabla[0][4+(i*5)]+pipe; //Cuenta
		 cuentas1+=""+pipe;  //Nivel
		 cuentas1+=""+pipe;  //Periodicidad
		 cuentas1+=""+pipe;  //Horario
		 cuentas1+="0"+pipe;  //SaldoOp1
		 cuentas1+="No"+pipe;  //InvAuto
		 cuentas1+="0"+pipe;  //SaldoOp2
		 cuentas1+=Aux1.camposTabla[0][7+(i*5)]+pipe;  //CuentaPadre
		 cuentas1+=""+pipe;  //NumeroNivel
		 cuentas1+=""+pipe;  //TipoNivel
		 cuentas1+=Aux1.camposTabla[0][8+(i*5)]+pipe; //Descripcion
		 cuentas1+="1"+pipe;  //ClaveProducto
		 cuentas1+="1";  //NivelTrama
		 cuentas1+="@";
		 indic=(8+(i*5))+1;
		 a.insertaCuenta(cuentas1);
	  }

	 for(int j=0;j<Integer.parseInt(Aux1.camposTabla[0][indic]);j++)
	  {
         cuentas1 =Aux1.camposTabla[0][(indic+1)+(7*j)]+pipe; //Cuenta
		 cuentas1+=""+pipe;  //Nivel
		 cuentas1+=cambiaPeriodicidad(Aux1.camposTabla[0][(indic+3)+(7*j)])+pipe; //Periodicidad
		 cuentas1+=Aux1.camposTabla[0][(indic+7)+(7*j)]+pipe;  //Horario
		 cuentas1+=Aux1.camposTabla[0][(indic+5)+(7*j)]+pipe;  //SaldoOp1
		 if(a.esCero(Aux1.camposTabla[0][(indic+6)+(7*j)]))
		   cuentas1+="No"+pipe;  //InvAuto
		 else
		   cuentas1+="Si"+pipe;  //InvAuto
		 cuentas1+=Aux1.camposTabla[0][(indic+6)+(7*j)]+pipe;  //SaldoOp2
		 cuentas1+=""+pipe;  //CuentaPadre
		 cuentas1+=""+pipe;  //NumeroNivel
		 cuentas1+=""+pipe;  //TipoNivel
		 cuentas1+=""+pipe;  //Descripcion
		 cuentas1+="1"+pipe;  //ClaveProducto
		 cuentas1+=Aux1.camposTabla[0][(indic+4)+(7*j)];  //NivelTrama
		 cuentas1+="@";
		 a.agregaCuenta(cuentas1);
	  }
	 a.cadenaCuentas();
     return a.Todas;
   }
}