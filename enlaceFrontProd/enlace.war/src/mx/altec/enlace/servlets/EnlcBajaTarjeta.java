package mx.altec.enlace.servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EnlcTarjetaBO;
import mx.altec.enlace.bo.EnlcTarjetaBOImpl;
import mx.altec.enlace.dao.DaoException;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.IEnlace;


/**
 * Clase encargada de dar de baja tarjetas vinculadas a un contrato
 */
public class EnlcBajaTarjeta extends BaseServlet {
	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Log
	 */
	private static final String TEXTOLOG = EnlcBajaTarjeta.class.getName();

	/**
	 * Metodo que recibe las peticiones get
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(verificaFacultad ("BAJVINCTARJ",request)){

			if (!SesionValida(request, response)) {
				request.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
				evalTemplate(IEnlace.ERROR_TMPL, request, response);
				return;
			}
			request.getSession().removeAttribute("MensajeLogin01");
			HttpSession sess;
			sess = request.getSession();
			BaseResource session;
			session = (BaseResource) sess.getAttribute("session");
			request.setAttribute("MenuPrincipal", session.getStrMenu());
			request.setAttribute("newMenu", session.getFuncionesDeMenu());

			request.setAttribute("Encabezado",    CreaEncabezado("Baja de Tarjetas",
					"Servicios &gt; Tarjeta de Pagos Santander &gt; Vinculaci&oacute;n tarjeta &ndash; contrato &gt Consulta y Baja", "", request ) );
			eliminaRegistros(request, response);
		} else {
			request.getRequestDispatcher("/enlaceMig/SinFacultades").forward(request, response);
		}
	}

	/**
	 * Metodo que recibe las peticiones post
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Metodo que realiza la baja de tarjetas
	 * @param request peticion
	 * @param response respuesta
	 * @throws ServletException excepcion
	 * @throws IOException excepcion
	 */
	public void eliminaRegistros(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sess;
		sess = request.getSession();
		BaseResource session;
		session = (BaseResource) sess.getAttribute("session");
		EIGlobal.mensajePorTrace(TEXTOLOG + "Paso ESEBTV", EIGlobal.NivelLog.INFO);

		EIGlobal.mensajePorTrace(TEXTOLOG + "Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		String valida = getFormParameter(request,"valida" );
		if (valida == null)
		{
			boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), IEnlace.NOMINA);
			//CSA-Vulnerabilidad  Token Bloqueado-Inicio
			int estatusToken = obtenerEstatusToken(session.getToken());
			
			if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && estatusToken == 1 && solVal)
			{
				EIGlobal.mensajePorTrace("El usuario tiene Token. \nSolicita la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
				request.getSession().setAttribute("valida", "validado");
				ValidaOTP.guardaParametrosEnSession(request);
				ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP);
			}
			else if(session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && estatusToken == 2 && solVal){
				cierraSesionTokenBloqueado(session,request,response);
			}else{
				valida = "1";
			}
			//CSA-Vulnerabilidad  Token Bloqueado-Fin
		}
		if (valida != null && valida.equals("1"))
		{
			String[] tarjetasReq;
			tarjetasReq = request.getParameterValues("tarjetas");
			EnlcTarjetaBO tarjetasBO;
			tarjetasBO = new EnlcTarjetaBOImpl();
			try{
				tarjetasBO.eliminarTarjetas(tarjetasReq, request);
			} catch (DaoException e) {
				sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"Su transacci�n no puede ser atendida en este momento intente m�s tarde.\", 1);");
			}
			sess.setAttribute("MensajeLogin01", " cuadroDialogo(\"La Baja fue exitosa.\", 1);");
			request.getSession().removeAttribute("tarjetasRes");
			request.setAttribute("anio", ObtenAnio());
			request.setAttribute("mes", ObtenMes());
			request.setAttribute("dia", ObtenDia());
			String diasInhabiles;
			diasInhabiles = diasInhabilesAnt();
			request.setAttribute ("diasInhabiles",diasInhabiles);
			request.setAttribute ("Fecha",ObtenFecha ());
			evalTemplate("/jsp/filtroConsultaTarjeta.jsp", request, response );
		} else {
			request.getSession().removeAttribute("tarjetasRes");
			request.setAttribute("anio", ObtenAnio());
			request.setAttribute("mes", ObtenMes());
			request.setAttribute("dia", ObtenDia());
			request.setAttribute ("Fecha",ObtenFecha ());
			evalTemplate("/jsp/filtroConsultaTarjeta.jsp", request, response );
		}
	}
}
