package mx.altec.enlace.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.ConfEdosCtaArchivoBean;
import mx.altec.enlace.beans.ConfEdosCtaArchivoDatosBean;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BitacoraBO;
import mx.altec.enlace.bo.ConfigMasCuentasCuentaBO;
import mx.altec.enlace.bo.ConfigModEdoCtaTDCBO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;
import mx.altec.enlace.utilerias.EdoCtaConstantes;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet ConfigMasPorCuentaServlet
 */
public class ConfigMasPorCuentaServlet extends BaseServlet {
	
	/** Constante para session de BaseResource */
	private static final String BASE_RESOURCE = "session";
	/** serial del servlet */
	private static final long serialVersionUID = 1L;
	/** constante opcion */
	private static final String OPCION = "opcion";
	/** Constante para titulo de pantalla */
	private static final String TITULO_PANTALLA = "Estados de Cuenta";
	/** Constante para posicion de menu */
	private static final String POS_MENU = "Estados de Cuenta &gt; Modificar estado de env&iacute;o del estado de Cuenta Impreso &gt; Masivo";
	/** Constante para archivo de ayuda **/
	private static final String ARCHIVO_AYUDA = "aEDC_ConfigMas";	
	/** Constante mensaje de error por facultades */
	private static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";
	/** Constante mensaje de error por dias inhabiles */
	private static final String MSJ_ERROR_DIAS = "Esta funcionalidad &uacute;nicamente se encuentra disponible en d&iacute;as h&aacute;biles.";
	/** TAG para logs **/
	private static final String LOG_TAG = "[EDCPDFXML] ::: ConfigMasPorCuentaServlet ::: ";
	/** objeto de servicio BO */ 
	private static ConfigMasCuentasCuentaBO configMasCuentasCuenta = new ConfigMasCuentasCuentaBO();
	/** bean de bitacora */
	private static BitaAdminBean bean = new BitaAdminBean();

	/** referencia o folio de la operacion */
	private static int referencia = 0;
	/** bean de cuentas */
	private static List<ConfEdosCtaArchivoBean> cuentas = new ArrayList<ConfEdosCtaArchivoBean>();
	/** cliente */
	private static String cliente = "";
	/** nuemro de contrato */
	private static String contrato = "";
	private static ConfigModEdoCtaTDCBO configMasTCCuentaBO = new ConfigModEdoCtaTDCBO();
   
	
	/**
     * Default constructor. 
     */
    public ConfigMasPorCuentaServlet() {
    	bean.setTipoOp(" ");
		bean.setIdTabla(" ");
    }
	/**
	 * @see HttpServlet#doGet(HttpServletrequestuest requestuest, HttpServletResponse response)
	 */
    /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletrequestuest requestuest, HttpServletResponse response)
	 */
	/** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean sesionvalida = SesionValida(request, response);
		
		if (sesionvalida) {
			Date ahora = new Date();
	        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault());
	        String diaActual = formateador.format(ahora);
	        //String diasInhabiles = diasInhabilesDesp();
	        String diasInhabiles = diasInhabilesAnt();
	        Calendar c = Calendar.getInstance();
	        c.setTime(ahora);
	        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				
			if(!diasInhabiles.contains(diaActual) && dayOfWeek != 7 && dayOfWeek != 1 ){	
				EIGlobal.mensajePorTrace(
						String.format("%s >>>>>>>> YYY Entra a dias habiles <<<<<<<<",LOG_TAG), 
						NivelLog.INFO);
				metodoInicio(request, response);
			} else {
				despliegaPaginaErrorURL(
					MSJ_ERROR_DIAS,
					TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, request, response);
            }
		}
	}
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void metodoInicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace(String.format("%s Inicio metodoInicio.",LOG_TAG),
				NivelLog.DEBUG);
		String valida = request.getParameter("valida");
		EIGlobal.mensajePorTrace(String.format("%s valida [%s]",LOG_TAG,valida)
				, NivelLog.DEBUG);
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);
		referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		cliente = (String) session.getUserID8();
		contrato = session.getContractNumber();
		
		EIGlobal.mensajePorTrace(String.format("%s Valida facultades FAC_EDO_CTA_MAS [%s]",LOG_TAG,BaseResource.FAC_EDO_CTA_MAS),
				NivelLog.DEBUG);

		if(!session.getFacultad(BaseResource.FAC_EDO_CTA_MAS)) {
			despliegaPaginaErrorURL(
					MSJ_ERROR_FAC,
					TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, null, request, response);
			return;
		}
		
		String accion = request.getParameter("accion") != null ? request.getParameter("accion").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s accion [%s]",LOG_TAG,accion),NivelLog.DEBUG);
		if("".equals(accion) || "null".equalsIgnoreCase(accion)){
			//[CMED-00] Entra a Modificacion Paperless Masivo
			bean.setReferencia(100000);
			BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_ENTRA_MOD_PAPERLESS_MASIVO, bean,false);
			
			EIGlobal.mensajePorTrace(String.format(" Existen cuentas en sesion--->%s",httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS)),
					NivelLog.DEBUG);
			
			if (httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS) == null || ConfigMasPorTCAux.cambiaContrato(request.getSession(), contrato)) {
				cuentas = configMasTCCuentaBO.consultaTDCAdmCtr(convierteUsr8a7(session.getUserID()),session.getContractNumber(), session.getUserProfile());				
				httpSession.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
				EIGlobal.mensajePorTrace(String.format(" Entro a poner Cuentas en sesion YY--->%s",cuentas.size()),
						NivelLog.DEBUG);
				
			} else {
				cuentas = (List<ConfEdosCtaArchivoBean>)
							httpSession.getAttribute(EdoCtaConstantes.LISTA_CUENTAS); 
				EIGlobal.mensajePorTrace(String.format(" NO Entro a recuperar Cuentas YY--->%s",cuentas.size()),
						NivelLog.DEBUG);
					 				
			}
			
			request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
			EIGlobal.mensajePorTrace(String.format("  PONE LISTA DE CUENTAS EN REQUEST-->%s<--",EdoCtaConstantes.LISTA_CUENTAS),
					NivelLog.DEBUG);
			
			EIGlobal.mensajePorTrace(String.format("%s Longitud de la lista cuentas CCY--> [%s]",LOG_TAG,cuentas.size()),
					NivelLog.DEBUG);
			EIGlobal.mensajePorTrace(String.format("%s accion [%s]",LOG_TAG,accion),NivelLog.DEBUG);

			EIGlobal.mensajePorTrace(String.format("%s Va a Agregar Encabezados",LOG_TAG,cuentas.size()),
					NivelLog.DEBUG);
			agregarEncabezado("/jsp/configMasCuentaSeleccion.jsp", request, response);
		}else{
			if("3".equals(accion) || "4".equals(accion)){
				//Flujo de PDF
				if("3".equals(accion) && valida == null){
					EIGlobal.mensajePorTrace(String.format("%s accion = [%s] bitacorizaAdmin",LOG_TAG,accion), 
							NivelLog.DEBUG);
					//[CMED-02] Modifica estado de suscripcion a Paperless de forma masiva
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_MODIF_EDO_SUSCRIP_PAPERLESS_MASIVO, bean, false);
					// [ACAP-00] Realizado guardado en bitacora de operaciones.
					BitacoraBO.bitacoraTCT(request, httpSession,
							BitaConstants.EA_CONV_ACEPT_PAPERLESS_CONFIRM,
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)),
							"", "", BitaConstants.COD_ERR_PDF_XML,
							BitaConstants.CONCEPTO_ACEPTA_CONV_PAPERLESS);
				}
				procesarCuentas(request, response, accion);
			}else{
				EIGlobal.mensajePorTrace(String.format("%s Accion [%s]",LOG_TAG,accion), NivelLog.DEBUG);
				//BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_SELEC_CTAS_PAPERLESS_MASIVO, bean, false);
				String formato = "";
				String mostrar = "";
				if("1".equals(accion)){
					formato = "PDF";
					mostrar = "1";
					//[CMED-01] Selecciona cuentas para modificacion de suscripcion a Paperless Masivo
					EIGlobal.mensajePorTrace(String.format("%s Entra a bitacorizaAdmin",LOG_TAG),NivelLog.DEBUG);
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_SELEC_CTAS_PAPERLESS_MASIVO, bean, false);
				}
				request.setAttribute("formato", formato);
				request.setAttribute("mostrar", mostrar);
				request.setAttribute("contrato", cliente);
				configuracion(request, response);
			}
		}
		
	}
	
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void configuracion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cadena = request.getParameter("cadenaCuenta") != null ? request.getParameter("cadenaCuenta").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s Cadena: [%s]",LOG_TAG,cadena), NivelLog.DEBUG);
		List<ConfEdosCtaArchivoBean> cuentas = formatoCuentas(cadena, 2);
		EIGlobal.mensajePorTrace(String.format("%s Longitud de la lista: [%s]",LOG_TAG,cuentas.size()),
				NivelLog.DEBUG);
		request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);		
		request.setAttribute("elementos", cuentas.size());
		agregarEncabezado("/jsp/configMasCuentaParam.jsp", request, response);
	}
	
	/**
	 * @param cadena : String que contiene las cuentas
	 * @param opcion : tipo de cadena a formatear
	 * @return List<ConfEdosCtaArchivoBean> cuentas
	 */
	private List<ConfEdosCtaArchivoBean> formatoCuentas(String cadena, int opcion){
		
		EIGlobal.mensajePorTrace(String.format("%s Inicia formatoCuentas --> Cadena: [%s]",LOG_TAG,cadena),
				NivelLog.DEBUG);		
		List<ConfEdosCtaArchivoBean> cuentas = new ArrayList<ConfEdosCtaArchivoBean>();

		if(opcion == 1){
			EIGlobal.mensajePorTrace(String.format("%s Opcion [%s]",LOG_TAG,opcion),NivelLog.DEBUG);
			String[] cadenas = cadena.split("##");
			for(int i = 0; i < cadenas.length; i++){
				String valores = cadenas[i];
				String[] datosCuenta = valores.split("#");
				EIGlobal.mensajePorTrace(String.format("%s cadenas[%s]=[%s]",LOG_TAG,i,valores), NivelLog.DEBUG);
				ConfEdosCtaArchivoBean cuenta = new ConfEdosCtaArchivoBean();
				cuenta.setNomCuenta(datosCuenta[0]);
				cuenta.setNombreTitular(datosCuenta[1]);
				cuenta.setPapaerless(datosCuenta[2].charAt(0));
				cuentas.add(cuenta);
			}
		}		
		if(opcion == 2){
			EIGlobal.mensajePorTrace(String.format("%s Opcion [%s]",LOG_TAG,opcion),NivelLog.DEBUG);
			String[] cadenas = cadena.split("#");
			for(int i = 0; i < cadenas.length; i++){
				EIGlobal.mensajePorTrace(String.format("%s cadenas[%s]=[%s]",LOG_TAG,i,cadenas), NivelLog.DEBUG);
				ConfEdosCtaArchivoBean cuenta = new ConfEdosCtaArchivoBean();
				int longitud = cadenas[i].indexOf(' ');
				String noCuenta = cadenas[i].substring(0,longitud);
				String titular =  cadenas[i].substring(longitud+1, cadenas[i].length());
				cuenta.setNomCuenta(noCuenta);
				cuenta.setNombreTitular(titular);
				cuentas.add(cuenta);
			}
		}
		
		return cuentas;
	}
	
	/**
	 * @param cuentas : cuentas que se van a parametrizar
	 * @param usuario : usuario que realiza la operacion
	 * @param contrato : numero de contrato del cliente
	 * @return Map<String, String > con valores de la respuesta
	 */
	private Map<String, String > configurarCuentas(List<ConfEdosCtaArchivoBean> cuentas, String usuario, String contrato){
		
		ConfigMasCuentasCuentaBO configMasCuentasBO = new ConfigMasCuentasCuentaBO();
		
		ConfEdosCtaArchivoDatosBean confEdosDatosBean = new ConfEdosCtaArchivoDatosBean();
		confEdosDatosBean.setListaDatos(cuentas);
		confEdosDatosBean.setIdUsuario(usuario);
		confEdosDatosBean.setNumeroCuenta(contrato);
		
		Map<String, String > respuesta = new HashMap<String, String >();
		
		respuesta = configMasCuentasBO.mandaAlmacenarDatos(confEdosDatosBean);
		
		return respuesta;
	} 
	
	/**
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @param accion : accion que indica el flujo a seguir
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void procesarCuentas(HttpServletRequest request, HttpServletResponse response,
			String accion) throws ServletException, IOException{
		
		EIGlobal.mensajePorTrace(String.format("%s Inicia Metodo procesarCuentas.",LOG_TAG),NivelLog.DEBUG);
		String valida = request.getParameter("valida");
		String cuentasConfiguradas = request.getParameter("cadena") != null ? request.getParameter("cadena").toString() : "";
		EIGlobal.mensajePorTrace(String.format("%s CuentasConfiguradas: [%s]",LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);

		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);
		
		if( validaPeticion(request, response, session, request.getSession(), valida) ) {
			  EIGlobal.mensajePorTrace(String.format("%s >>>>> ENTRO A VALIDAR LA PETICION <<<<< ",LOG_TAG), NivelLog.DEBUG);
		}else {
			EIGlobal.mensajePorTrace(String.format("%s cuentas Configuradas: [%s]",LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
			if (valida == null) {				
				EIGlobal.mensajePorTrace(String.format("Valida [%s]",LOG_TAG,valida), NivelLog.DEBUG);
				//[ACAP-01] Acepta Convenio de Aceptacion Paperless Masivo
				bean.setReferencia(100000);
				BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_ACEPT_CONV_PAPERLESS_MASIVO, bean, false);
				valida = ConfigMasCuentasCuentaBO.validaToken(request, response, "ConfigMasPorCuentaServlet");
			}
			EIGlobal.mensajePorTrace(String.format("%s cuenta(s) Configurada(s): [%s]",LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
			if (valida != null && "1".equals(valida)) {
				if("3".equals(accion)){
					//[CECM-00] Confirma Modificacion Paperless Masiva
					bean.setReferencia(100000);
					BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_CONFR_MODIF_PAPERLESS_MASIVO, bean, true);
				}			
				List<ConfEdosCtaArchivoBean> cuentasParametrizadas = new ArrayList<ConfEdosCtaArchivoBean>();
				EIGlobal.mensajePorTrace(String.format("%s Cuenta(s) Configurada(s): [%s]",LOG_TAG,cuentasConfiguradas), NivelLog.DEBUG);
				cuentasParametrizadas = formatoCuentas(cuentasConfiguradas,1);
				if(cuentasParametrizadas != null){
					
					//Bitacorizacion
					BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.EA_ESTADOS_CUENTA_ARCHIVO_CONFIRM, 
							obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), "", "", BitaConstants.COD_ERR_PDF_XML, BitaConstants.CECM);
					
					//Registro en BD
					EIGlobal.mensajePorTrace(String.format("%s El usuario es: [%s]",LOG_TAG,cliente),NivelLog.DEBUG);
					Map<String, String > respuesta = configurarCuentas(cuentasParametrizadas, cliente , contrato);
					String folio = "";
					if(respuesta != null && "none".equals(respuesta.get("error"))){
						folio = respuesta.get("secuencia");
						//[CMED-03] Finaliza la modificacion de Paperless masiva
						
						BitacoraBO.bitacorizaAdmin(request, httpSession, BitaConstants.EA_FINAL_MODIF_PAPERLESS_MASIVO,setBitacoraAux(folio,""), false);
						BitacoraBO.bitacoraTCT(request, httpSession, BitaConstants.EA_ESTADOS_CUENTA_ARCHIVO_FIN, obten_referencia_operacion
								(Short.parseShort(IEnlace.SUCURSAL_OPERANTE)), "", "", BitaConstants.COD_ERR_PDF_XML, BitaConstants.CMED);		
						//envio de notficaciones
						String [] datos = new String[3];
						datos[0] = cuentasParametrizadas.get(0).getFormato();
						datos[1] = String.valueOf(cuentasParametrizadas.size());
						datos[2] = "0";
						EIGlobal.mensajePorTrace(String.format("%s datos[] = {%s}",LOG_TAG,datos),NivelLog.DEBUG);
						configMasCuentasCuenta.enviarNotificaciones(session, referencia, folio, request, datos);
						
						request.setAttribute("folio", folio);
						//cuentas = configMasCuentasCuenta.consultaCuentas(request, response);
						cuentas = configMasCuentasCuenta.consultaCuentas(convierteUsr8a7(session.getUserID()),session.getContractNumber(), session.getUserProfile(),request);
						EIGlobal.mensajePorTrace(String.format("%s cuentas---->"+cuentas.size(),LOG_TAG), NivelLog.DEBUG);
						request.setAttribute(EdoCtaConstantes.LISTA_CUENTAS, cuentas);
						
						agregarEncabezado("/jsp/configMasCuentaSeleccion.jsp", request, response);
					}else {
						despliegaPaginaErrorURL("Error al almacenar los datos", TITULO_PANTALLA,
								POS_MENU,
								ARCHIVO_AYUDA, "ConfigMasPorCuentaServlet", request, response);
					}
				}
			}						
		}
	}
	
	/**
	 * @param jsp : jsp al que se direccionara
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepcion
	 * @throws IOException : Excepcion
	 */
	private void agregarEncabezado(String jsp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		EIGlobal.mensajePorTrace(String.format("%s Entra a agregarEncabezados",LOG_TAG), NivelLog.DEBUG);
		HttpSession httpSession = request.getSession();
		BaseResource session = (BaseResource) httpSession.getAttribute(BASE_RESOURCE);
		request.setAttribute(OPCION, request.getParameter(OPCION));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado(TITULO_PANTALLA, POS_MENU, ARCHIVO_AYUDA, request));
		EIGlobal.mensajePorTrace(String.format("%s Termina agregarEncabezados",LOG_TAG), NivelLog.DEBUG);
		evalTemplate(jsp, request, response);		
	}
	
	/**
	 * Auxiliar para generar el bean
	 * @param secuencia : secuencia
	 * @param archivoName : archivoName
	 * @return BitaAdminBean : bean
	 */
	private BitaAdminBean setBitacoraAux(String secuencia, String archivoName) {
		BitaAdminBean bean = new BitaAdminBean();
		bean.setIdTabla(secuencia);
		bean.setCampo("ID_EDO_CTA_CTRL");
		bean.setTabla("EWEB_EDO_CTA_CTRL");
		bean.setDato(archivoName);
		bean.setDatoNvo(secuencia);
		bean.setTipoOp("I");
		bean.setReferencia(100000);
		return bean;
	}	
}