package mx.altec.enlace.servlets;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.SUALCORM4;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.bo.SUAConsultaPagoAction;
import mx.altec.enlace.bo.SUAGenCompPDF;
import mx.altec.enlace.bo.pdCalendario;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.PaginaListas;




/**
 * Servlet implementation class for Servlet: SUAConsultaPagoServlet
 *
 */
 public class SUAConsultaPagoServlet extends mx.altec.enlace.servlets.BaseServlet implements javax.servlet.Servlet {
    /**
	 *
	 */
	private static final long serialVersionUID = -2604204286651092170L;

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public SUAConsultaPagoServlet() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		consultas(request, response);
	}

	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		consultas(request, response);
	}
	/**
	 * Metodo para el manejo de las consultas de SUALC
	 * @author Leopoldo Espinosa R <VC> 28-08-2009
	 * @param req
	 * @param res
	 * @throws ServletException
	 * @throws IOException
	 *
	 */
	public void consultas(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException{

		EIGlobal.mensajePorTrace ("Entra en SUALC....Consulta Anteriores....."
				,EIGlobal.NivelLog.INFO);
		try{

			HttpSession sessionHttp = req.getSession();
			/*Obtencion de session del BaseResource */
			BaseResource session=(BaseResource)sessionHttp.getAttribute("session");

			boolean sesionvalida= SesionValida( req, res );
			//validacion de session general
			if(sesionvalida ){

				req.setAttribute("MenuPrincipal", session.getStrMenu());
				req.setAttribute("newMenu", session
                        .getFuncionesDeMenu());

				String archivoAyuda = "s31000h";
				req.setAttribute("Encabezado", CreaEncabezado("Seguimiento de Pagos S.U.A. por L&iacute;nea de Captura",
						"Servicios &gt; S.U.A. &gt; L&iacute;nea de Captura",
						archivoAyuda, req));

				String opcionConSUA = req.getParameter("opcionConSUA");

				if(opcionConSUA==null || opcionConSUA.length()==0){
					opcionConSUA="1";
				}
				//Consulta anteriores
				String redirJSP="/jsp/SUAConsultaPago.jsp";
				if(opcionConSUA.equals("1")){
					session.setModuloConsultar(IEnlace.MCargo_transf_pesos);
					EIGlobal.mensajePorTrace( "-------------Entro a pago directo por Registro." , EIGlobal.NivelLog.DEBUG);
					ArrayList diasInhabiles = getDiasInhabiles ();

					GregorianCalendar fecha1 = pdCalendario.getSigDiaHabil (new GregorianCalendar (), diasInhabiles);
					GregorianCalendar fecha2 = (GregorianCalendar) fecha1.clone ();
					fecha1.set (GregorianCalendar.MONTH, fecha1.get (GregorianCalendar.MONTH) - 3);
					fecha1 = pdCalendario.getAntDiaHabil (fecha1, getDiasInhabiles ());

					req.getSession ().setAttribute ("fechaLib", fecha1);

					req.getSession ().setAttribute ("Calendario", pdCalendario.getCalendario(diasInhabiles,fecha1,fecha2));
					req.getSession ().setAttribute ("fechaLim", fecha2);
					req.getSession ().removeAttribute ("pagina");
					req.getSession().removeAttribute("datosPI");
					req.getSession().removeAttribute("ObjPaginacion");
					req.getSession().removeAttribute("delFechaSUA");
					req.getSession().removeAttribute("alFechaSUA");
					req.getSession().removeAttribute("ctaCargoSUACons");
					req.getSession().removeAttribute("datos");
				}
				if(opcionConSUA.equals("1C")){
						//Valors iniciales

						req.setAttribute("contrato",session.getContractNumber ());
						req.setAttribute("del",req.getParameter("FechaLibramiento"));
						req.setAttribute("al",req.getParameter("FechaLimite"));
						req.getSession().setAttribute("delFechaSUA",req.getParameter("FechaLibramiento"));
						req.getSession().setAttribute("alFechaSUA",req.getParameter("FechaLimite"));
						//Realizamos consulta de movimientos

						EIGlobal.mensajePorTrace ("Cuenta: " + req.getParameter("cboCuentaCargo")
								,EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace ("Fecha1: " + req.getParameter("FechaLibramiento")
								,EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace ("Fecha2: " + req.getParameter("FechaLimite")
								,EIGlobal.NivelLog.DEBUG);
						SUAConsultaPagoAction action= new SUAConsultaPagoAction();
						SUALCORM4 sualcorm4 = action.anterioresSUALC
								( req.getParameter("cboCuentaCargo")
								 ,null, req.getParameter("FechaLibramiento"),
								 req.getParameter("FechaLimite"), null,null,3);
						req.getSession().setAttribute("ctaCargoSUACons", req.getParameter("CuentaCargo"));
						if(sualcorm4!=null && sualcorm4.getMsjErr().equals("Operacion Exitosa")){
							EIGlobal.mensajePorTrace ("Operacion Exitosa regresa "+sualcorm4.getConsulta().size()+" elementos"
									,EIGlobal.NivelLog.DEBUG);
							PaginaListas paginacion= new PaginaListas(sualcorm4.getConsulta(),SUALCORM4.PAGINACION);
							req.setAttribute("datos",paginacion.inicio());
							req.getSession().setAttribute("datosPI",sualcorm4.getConsulta());
							//subimos objeto de paginacion a session
							req.getSession().setAttribute("ObjPaginacion",paginacion);
							req.getSession().setAttribute("datos",paginacion.inicio());
							String USUARIO	= session.getUserID8();
							String CONTRATO = session.getContractNumber();

							String NOMBRE_ARCHIVO   = CONTRATO + USUARIO + "SUALC_Con.doc";
							String ARCHIVO_EXPORTAR = IEnlace.DOWNLOAD_PATH + NOMBRE_ARCHIVO;
							int exportar = exportarArchivo(NOMBRE_ARCHIVO , ARCHIVO_EXPORTAR, req, res );

							if(exportar==0){
								req.setAttribute("downloadFile", NOMBRE_ARCHIVO);
								req.getSession().setAttribute("dwF",NOMBRE_ARCHIVO);
							}
							req.setAttribute("pagini",paginacion.getPagina_actual());
							req.setAttribute("regIni",paginacion.getRegistro_Inicial());
							req.setAttribute("regFin",paginacion.getRegistro_Final());
							req.setAttribute("pagfin",paginacion.getPaginas_totales());
							req.setAttribute("regTotal",paginacion.getRegistrosTotales());
						}else{
							req.setAttribute("pagini",0);
							req.setAttribute("regIni",0);
							req.setAttribute("regFin",0);
							req.setAttribute("pagfin",0);
							req.setAttribute("regTotal",0);
							req.setAttribute("datos",null);
						}

					redirJSP="/jsp/SUAConsultaPago_Listado.jsp";
				}
				if(opcionConSUA.equals("I")){
					/***********************************************************
					 ***  Opcion para la impresion de comprobante             ***
					 **********************************************************/
					EIGlobal.mensajePorTrace("Enlace a SUAConsultaPagoServlet Impresion de Comprobante", EIGlobal.NivelLog.INFO);

					List<SUALCORM4> datos=null;//CSA
					if(req.getSession().getAttribute("datos")!=null){
						datos=(List<SUALCORM4>)req.getSession().getAttribute("datos"); //CSA
					}
					
					if(datos!=null && datos.size()>0 && req.getParameter("imprime")!=null
							&& req.getParameter("imprime").toString().length()>0){
						EIGlobal.mensajePorTrace("Datos:" + datos.size(), EIGlobal.NivelLog.DEBUG);
						EIGlobal.mensajePorTrace("Enlace a SUAConsultaPagoServlet", EIGlobal.NivelLog.DEBUG);
						double importeTotal;

						int valor=Integer.parseInt(req.getParameter("imprime").toString());
						SUALCORM4 beanDao=datos.get(valor);
						try{

							importeTotal=Double.parseDouble(beanDao.getImporteTotal());
							importeTotal=(importeTotal/100);//por los decimales
						}catch(NumberFormatException exception){
							importeTotal=0;
						}
						String periodopagMes=null;
						String periodopagAnio=null;
						if(beanDao.getPeriodoPago()!=null && beanDao.getPeriodoPago().length()>0){
							try{
								periodopagAnio=beanDao.getPeriodoPago().substring(0,4);
								periodopagMes=beanDao.getPeriodoPago().substring(4,beanDao.getPeriodoPago().length());
							}catch(IndexOutOfBoundsException ibf){
								periodopagAnio="";
								periodopagMes="";
							}
						}else{
							periodopagAnio="";
							periodopagMes="";
						}
						String fecha="";
						String hora="";
						if(beanDao.getTimestamp().length()>18){
							fecha=beanDao.getTimestamp().substring(0,10);
							hora=beanDao.getTimestamp().substring(11,19).replace(".",":");
						}else{
							fecha="";
							hora=beanDao.getTimestamp();
						}
						String cuentacrg="";
						if(req.getSession().getAttribute("ctaCargoSUACons")!=null){
							cuentacrg=(String)req.getSession().getAttribute("ctaCargoSUACons");
						}else{
							cuentacrg=beanDao.getCuentaCargo();
						}
						SUAGenCompPDF comp;
						EIGlobal.mensajePorTrace(" - "+session.getContractNumber()+" \n- "+
								session.getNombreContrato()+" \n- "+
							session.getUserID8()+" \n- "+
							session.getNombreUsuario()+" \n- "+
							beanDao.getFolioSua()+" \n- "+
							cuentacrg+" \n- "+
							""+" \n- "+
							beanDao.getRegPatronal()+" \n- "+
							periodopagMes+" \n- "+
							periodopagAnio+" \n- "+
							beanDao.getLinSua()+" \n- "+
							importeTotal+" \n- "+
							beanDao.getNumeroMovimiento()+" \n- "+
							beanDao.getPeriodoPago()+" \n- "+
							beanDao.getTimestamp()+" \n- "+
							"INTERNET"+" \n- "+
							"EXITO"+" \n- "+
							"",EIGlobal.NivelLog.DEBUG);
							comp = new SUAGenCompPDF(
								session.getContractNumber(),
								session.getNombreContrato(),
							session.getUserID8(),
							session.getNombreUsuario(),
							beanDao.getFolioSua(),
							cuentacrg,
							"",
							beanDao.getRegPatronal(),
							periodopagMes,
							periodopagAnio,
							beanDao.getLinSua(),
							importeTotal,
							beanDao.getNumeroMovimiento(),
							fecha,hora,
							"INTERNET",
							"EXITO"
							,"");

						pantCompSUA(req, res,comp.generaComprobanteBAOS());// archivo);
						redirJSP="pantCompSUA";//para no hacer redireccionamiento de pagina
					}else{
						//mandar mensaje de error

						EIGlobal.mensajePorTrace ("Error al imprimir el comprobante",EIGlobal.NivelLog.ERROR);
								redirJSP="/jsp/pantCompSUA.jsp";
					}
				}
				//Opcion de paginacion
				if (opcionConSUA.equals("P")){

					if(req.getSession().getAttribute("datosPI")!=null){
						List<SUALCORM4> datosSesion= (List)req.getSession().getAttribute("datosPI");
						if(datosSesion!=null && datosSesion.size()>0){
							EIGlobal.mensajePorTrace ("Paginamos de "+datosSesion.size()+" elementos"
									,EIGlobal.NivelLog.DEBUG);
							req.setAttribute("regTotal",datosSesion.size());
							if(req.getSession().getAttribute("ObjPaginacion")!=null){
								PaginaListas paginacion=(PaginaListas)req.getSession().getAttribute("ObjPaginacion");
								List<SUALCORM4> listaPaginada=null;
								if(req.getParameter("accionPagina").equals("Sig")){
									listaPaginada=paginacion.siguiente();
								}else if(req.getParameter("accionPagina").equals("Ant")){
									listaPaginada=paginacion.anterior();
								}else if(req.getParameter("accionPagina").equals("Fin")){
									listaPaginada=paginacion.fin();
								}else if(req.getParameter("accionPagina").equals("Prim")){
									listaPaginada=paginacion.inicio();
								}else{
									listaPaginada=datosSesion;
								}
								req.setAttribute("datos",listaPaginada);
								req.getSession().setAttribute("datos",listaPaginada);
								req.setAttribute("pagini",paginacion.getPagina_actual());
								req.setAttribute("regIni",paginacion.getRegistro_Inicial());
								req.setAttribute("regFin",paginacion.getRegistro_Final());
								req.setAttribute("pagfin",paginacion.getPaginas_totales());
								req.setAttribute("regTotal",paginacion.getRegistrosTotales());
							}else{

								req.setAttribute("pagini",0);
								req.setAttribute("regIni",0);
								req.setAttribute("regFin",0);
								req.setAttribute("datos",null);
								req.setAttribute("pagfin",0);
								req.setAttribute("regTotal",0);
							}
							req.setAttribute("contrato",session.getContractNumber ());
							req.setAttribute("del", req.getSession().getAttribute("delFechaSUA"));
							req.setAttribute("al",req.getSession().getAttribute("alFechaSUA"));
							req.setAttribute("downloadFile", req.getSession().getAttribute("dwF"));

							redirJSP="/jsp/SUAConsultaPago_Listado.jsp";
						}else{

							redirJSP="/jsp/SUAConsultaPago.jsp";
						}
					}else{

						redirJSP="/jsp/SUAConsultaPago.jsp";
					}
				}
				if(!redirJSP.equals("pantCompSUA")){
					evalTemplate(redirJSP,req,res);
				}
			}
		}catch(Exception e){
			EIGlobal.mensajePorTrace ("\n Hubo un error : \n"+e.getMessage(),EIGlobal.NivelLog.ERROR);

	  		despliegaPaginaError("Error inesperado en el Servidor.",
					"Encabezado", "Seguimiento de Pagos S.U.A. por Linea de Captura",
						"Servicios &gt; S.U.A. &gt; Linea de Captura",
					req, res );
	  	}

	}

	/**
	 * Generacion del comprobante PDF
	 * @param req
	 * @param res
	 * @param archivo
	 * @throws ServletException
	 * @throws IOException
	 */
	private void  pantCompSUA(HttpServletRequest req, HttpServletResponse res, ByteArrayOutputStream archivo) throws ServletException, IOException
	{


		res.setHeader("Expires", "0");
		res.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		res.setHeader("Pragma", "public");
		// setting the content type
		res.setContentType("application/pdf");
		// the contentlength is needed for MSIE!!!
		res.setContentLength(archivo.size());
		// write ByteArrayOutputStream to the ServletOutputStream
		ServletOutputStream out = res.getOutputStream();
		archivo.writeTo(out);
		out.flush();
		out.close();

	}


	/** Returns a short description of the servlet.
	*/
	public String getServletInfo() {
		return "Servlet de consulta de SUA-LC";
	}

	/**
	* Metodo para obtener los dias inhabiles a partir de la fecha actual
	* @return Lista con los dias inhabiles
	*/
	protected ArrayList getDiasInhabiles () {
		java.sql.ResultSet rs=null;
		java.sql.Connection conn=null;
		java.sql.Statement stmt=null;
		ArrayList<GregorianCalendar> diasInhabiles = new ArrayList<GregorianCalendar> ();
		GregorianCalendar fecha;

		int Dia, Mes, Anio;
		String sql = "select to_char(FECHA, 'dd'), to_char(FECHA, 'mm'), to_char(FECHA, 'yyyy') from COMU_DIA_INHABIL where CVE_PAIS = 'MEXI' and FECHA >= sysdate";
		try {
			conn = createiASConn(Global.DATASOURCE_ORACLE);
			if (conn == null) return null;
			stmt = conn.createStatement ();
			rs = stmt.executeQuery (sql);
			while (rs.next ()) {
				Dia = Integer.parseInt (rs.getString (1));
				Mes = Integer.parseInt (rs.getString (2));
				Anio = Integer.parseInt (rs.getString (3));
				Mes++;
				fecha = new GregorianCalendar (Anio, Mes - 2, Dia);
				diasInhabiles.add (fecha);
			}

			return diasInhabiles;
			} catch (java.sql.SQLException ex) {
				EIGlobal.mensajePorTrace("Error al Obtener Fechas", EIGlobal.NivelLog.ERROR);

				return null;
			}finally{
				try {
					if (rs!=null){
						rs.close ();
					}
				} catch (SQLException e) {
					// Auto-generated catch block
					//e.printStackTrace();
				}
				try {
					if(stmt!=null){
						stmt.close ();
					}
				} catch (SQLException e) {
					//Auto-generated catch block
					//e.printStackTrace();
				}
				try {
					if(conn!=null){
						conn.close ();
					}
				} catch (SQLException e) {
					//Auto-generated catch block
					//e.printStackTrace();
				}
			}

		}
	/**
	 * Metodo para la exportacion de Datos a archivo
	 * @param NOMBRE_ARCHIVO
	 * @param archivo_salida
	 * @param req
	 * @param res
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public int exportarArchivo(String NOMBRE_ARCHIVO, String archivo_salida,
			HttpServletRequest req, HttpServletResponse res )
			throws ServletException, IOException {

	    ArrayList<SUALCORM4> datos=null;
		if(req.getSession().getAttribute("datosPI")!=null){
			datos=(ArrayList<SUALCORM4>)req.getSession().getAttribute("datosPI");
		}
		int i = 0;
		EI_Exportar archivo_exportar = new EI_Exportar( archivo_salida );
		ArchivoRemoto archR=new ArchivoRemoto();
		archivo_exportar.creaArchivo();

		try{
			archivo_exportar.setAddLine(true);
			if(datos!=null){
				for( i = 0; i < datos.size(); i++ ){
					SUALCORM4 sualcorm4 = datos.get(i);
					double importe=0;
					try{

						importe=Double.parseDouble(sualcorm4.getImporteTotal());
						importe=(importe/100);//por los decimales
					}catch(NumberFormatException exception){
						importe=0;
					}
					archivo_exportar.escribeLinea(sualcorm4.getFolioSua()
							+" - "+sualcorm4.getCuentaCargo()
							+" - "+sualcorm4.getRegPatronal()
							+" - "+importe
							+" - "+sualcorm4.getTimestamp().replace(".",":")
							+" - Aceptada"
							+" - "+sualcorm4.getLinSua()
							+" - "+sualcorm4.getPeriodoPago());

				}
			}
			archivo_exportar.cierraArchivo();

			// Doble remote shell
			archR.copiaLocalARemoto( NOMBRE_ARCHIVO ,"WEB");
			EIGlobal.mensajePorTrace( "***SUAConsultaPagoServlet.class  HELG &Ejecutando copia local a remoto...&", EIGlobal.NivelLog.DEBUG);
		}catch(Exception e){

			EIGlobal.mensajePorTrace( "***SUAConsultaPagoServlet.class  Excepci�n en exportarArchivo() >>"+e.getMessage()+"<<", EIGlobal.NivelLog.ERROR);
			return 1;
		}

		return 0;
	} // metodo

}