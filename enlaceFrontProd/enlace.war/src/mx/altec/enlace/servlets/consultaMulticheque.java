package mx.altec.enlace.servlets;

import java.sql.SQLException;
import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Exportar;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

//09/01/07 VSWF

public class consultaMulticheque extends BaseServlet
	{

	// --- M�todo Get ------------------------------------------------------------------
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- M�todo Post -----------------------------------------------------------------
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{defaultAction(req, res);}

	// --- M�todo 'MAIN' ---------------------------------------------------------------
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		debug("Entrando a defaultAction");

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		//----- Si la sesi�n no es v�lida, se indica el error y se termina la ejecuci�n
		if(!SesionValida(req, res))
			{
			despliegaPaginaError(IEnlace.MSG_PAG_NO_DISP,"Consulta de movimientos","", req, res);
			return;
			}

		String diasInhabiles = diasInhabilesAnt();

		if (diasInhabiles.trim().length()==0)
			{
			despliegaPaginaError("Servicio no disponible por el momento","Movimientos","", req, res);
			return;
			}

		//----- Se capturan par�metros de la entidad invocadora

		String parAccion = (String)req.getParameter("Accion"); if(parAccion == null) parAccion = "";
		String parTrama = (String)req.getParameter("Trama"); if(parTrama == null) parTrama = "";
		String parOffset = (String)req.getParameter("Offset"); if(parOffset == null) parOffset = "0";
		String parAncho = (String)req.getParameter("Ancho"); if(parAncho == null) parAncho = "";
		String parTotal = (String)req.getParameter("Total"); if(parTotal == null) parTotal = "";
		String parCuenta = (String)req.getParameter("Cuenta"); if(parCuenta == null) parCuenta = "";
		String parFecha1 = (String)req.getParameter("Fecha1"); if(parFecha1 == null) parFecha1 = "";
		String parFecha2 = (String)req.getParameter("Fecha2"); if(parFecha2 == null) parFecha2 = "";
		String parArchivo = (String)req.getParameter("Archivo"); if(parArchivo == null) parArchivo = "";
		String parArchTux = (String)req.getParameter("ArchTux"); if(parArchTux == null) parArchTux = "";
		String parDivisa = (String)req.getParameter("Divisa"); if(parDivisa == null) parDivisa = "";
		String parCodigo = (String)req.getParameter("Codigo"); if(parCodigo == null) parCodigo = "";

		// ----- -------------------------------------------------------------------------

		int pantalla = 0;
		String strAux = "";
		String fechaIni = fechaInicial();
		String fechaFin = fechaFinal();
		String fechaHoy = formateaFecha(new GregorianCalendar());
		String mensError = null;

		//----- -------------------------------------------------------------------------
		session.setModuloConsultar(IEnlace.MCargo_transf_pesos);

		try
			{
			if(parAccion.equals("CONSULTAR"))
				{
				pantalla = 1;
				strAux = parTrama;


				if(parOffset.equals("0"))
					{
					//- Se construye la trama para enviar a Tuxedo
					strAux = construyeTrama(strAux, req);
					//- Se env�a la consulta
					strAux = enviaConsuta(strAux, req);
					//- se realiza copia remota
					strAux = parArchTux = traeArchivo(strAux);
					//- Se traduce el archivo y se cambia su formato para que se pueda exportar
					parArchivo = traduceArchivo(strAux, parTrama, req);
					//- Se leen los datos del archivo y se construye la trama para enviar
					strAux = leeArchivo(strAux);
					//- Se asignan los par�metros
					parDivisa = strAux.substring(0,strAux.indexOf("@")); strAux = strAux.substring(strAux.indexOf("@")+1);
					parCodigo = strAux.substring(0,strAux.indexOf("@")); strAux = strAux.substring(strAux.indexOf("@")+1);
					parAncho = "" + Global.NUM_REGISTROS_PAGINA;
					parTotal = "" + cuentaRegistros(strAux);
					StringTokenizer tokens = new StringTokenizer(parTrama.substring(1),"|");
					parCuenta = tokens.nextToken() + " " + tokens.nextToken();
					parFecha1 = tokens.nextToken();
					parFecha2 = tokens.nextToken();
					}
				else
					{
					//- Se leen los datos del archivo y se construye la trama para enviar
					strAux = leeArchivo(parArchTux);
					strAux = strAux.substring(strAux.indexOf("@")+1);
					strAux = strAux.substring(strAux.indexOf("@")+1);
					}

				//- Se 'recorta' la consulta para mostrar solo las cuentas de la p�gina seleccionada
				strAux = recortaInfo(strAux,Integer.parseInt(parOffset),Integer.parseInt(parAncho));
//		    	TODO CU1131
			    /*
				 * 09/ENE/07
				 * VSWF-BMB-I
				 */
				  if (mx.altec.enlace.utilerias.Global.USAR_BITACORAS.trim().equals("ON")){
				try{
					BitaHelper bh = new BitaHelperImpl(req, session, sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.EC_MOV_MULTICHEQUE_CONSULTA_MULTICHEQUE);
					if(session.getContractNumber()!=null){
						bt.setContrato(session.getContractNumber());
					}
					if(parCuenta!=null){
						if(parCuenta.length()>20){
							bt.setCctaOrig(parCuenta.substring(0,20));
						}else{
							bt.setCctaOrig(parCuenta.trim());
						}
					}
					bt.setServTransTux("MULT");
					if(strAux != null){
						if(strAux.substring(0,2).equals("OK")){
							bt.setIdErr("MULT0000");
						}else if(strAux.length()>8){
							bt.setIdErr(strAux.substring(0,8));
						}else{
							bt.setIdErr(strAux.trim());
						}
					}
					if(parArchivo!=null){
						bt.setNombreArchivo(parArchivo);
					}
					BitaHandler.getInstance().insertBitaTransac(bt);
				}catch(SQLException e){
					e.printStackTrace();
				}catch(Exception e){
					e.printStackTrace();
				}
			    /*
				 * VSWF-BMB-F
				 */
				}
				}
			}
		catch(Exception e)
			{
			pantalla = 2;
			mensError = e.getMessage();
			}
		finally
			{;}

		//----- Se asignan par�metros para el JSP
		req.setAttribute("diasInhabiles",diasInhabiles);
		req.setAttribute("FechaIni",fechaIni);
		req.setAttribute("FechaFin",fechaFin);
		req.setAttribute("FechaHoy",fechaHoy);
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("Encabezado", CreaEncabezado("Consulta de movimientos multicheque","Consultas &gt; Movimientos &gt; Multicheque","s27000h", req));
		req.setAttribute("Trama",parTrama);
		req.setAttribute("Datos",strAux);
		req.setAttribute("Offset",parOffset);
		req.setAttribute("Ancho",parAncho);
		req.setAttribute("Total",parTotal);
		req.setAttribute("Cuenta",parCuenta);
		req.setAttribute("Fecha1",parFecha1);
		req.setAttribute("Fecha2",parFecha2);
		req.setAttribute("Archivo",parArchivo);
		req.setAttribute("ArchTux",parArchTux);
		req.setAttribute("Divisa",parDivisa);
		req.setAttribute("Codigo",parCodigo);

		//----- Se selecciona la pantalla de salida seg�n los resultados de la consulta
		switch(pantalla)
			{
			case 0: evalTemplate("/jsp/ConsultaMultiCheque.jsp", req, res); break;
			case 1: evalTemplate("/jsp/ConsultaMultiResult.jsp", req, res); break;
			case 2: despliegaMensaje(mensError,"Consulta de movimientos multicheque","Consultas &gt; Movimientos &gt; Multicheque", req, res); break;
			}
		debug("Saliendo de defaultAction");

		}

	//--- Construye la trama para enviar a Tuxedo --------------------------------------
	private String construyeTrama(String trama, HttpServletRequest req)
		{
		debug("Entrando a construyeTrama");
		String cuenta, fecha1, fecha2, folio1, folio2, cheque1, cheque2;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		//- Se obtienen los datos de 'trama'
		cuenta = trama.substring(1,trama.indexOf("|"));
		trama = trama.substring(trama.indexOf("|") + 1);
		trama = trama.substring(trama.indexOf("|") + 1);
		fecha1 = trama.substring(0,trama.indexOf("|"));
		trama = trama.substring(trama.indexOf("|") + 1);
		fecha2 = trama.substring(0,trama.indexOf("|"));
		trama = trama.substring(trama.indexOf("|") + 1);
		folio1 = trama.substring(0,trama.indexOf("|"));
		trama = trama.substring(trama.indexOf("|") + 1);
		folio2 = trama.substring(0,trama.indexOf("|"));
		trama = trama.substring(trama.indexOf("|") + 1);
		cheque1 = trama.substring(0,trama.indexOf("|"));
		cheque2 = trama.substring(trama.indexOf("|") + 1,trama.length()-1);

		//- Se formatean las fechas
		fecha1 = fecha1.substring(6) + "-" + fecha1.substring(3,5) + "-" + fecha1.substring(0,2);
		fecha2 = fecha2.substring(6) + "-" + fecha2.substring(3,5) + "-" + fecha2.substring(0, 2);

		//- se construye la trama para Tuxedo
		trama = "2EWEB|" + session.getUserID8() + "|MULT|" + session.getContractNumber() + "|" + session.getUserID8()
			+ "|" + session.getUserProfile() + "|" + cuenta	+ "@" + fecha1 + "@" + fecha2
			+ "@" + folio1 + "@" + folio2 + "@" + cheque1 + "@" + cheque2 + "@|";

		debug("Saliendo de construyeTrama");
		return trama;
		}

	//--- Env�a 'trama' a Tuxedo y retorna el aviso de Tuxedo --------------------------
	private String enviaConsuta(String trama, HttpServletRequest req) throws Exception
		{
		debug("Entrando a enviaConsulta");
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		//tuxedo.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		ht = tuxedo.web_red(trama);
		trama = (String)ht.get("BUFFER");
		debug("Tuxedo regresa: " + trama);
		if(!trama.substring(0,8).equals("MULT0000")) throw new Exception(trama.substring(9));
		trama = Global.DOWNLOAD_PATH + session.getUserID8();
		debug("Saliendo de enviaConsulta");
		return trama;
		}

	//--- Realiza copia remota del archivo de resgreso de Tuxedo -----------------------
	private String traeArchivo(String path) throws Exception
		{
		debug("Entrando a traeArchivo");
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		path = path.substring(path.lastIndexOf('/')+1);
		if(!archRemoto.copia(path))
			{
			debug("Error en traeArchivo(): no pudo realizarse copia remota: " + path);
			throw new Exception("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
			}
		debug("Saliendo de traeArchivo");
		return Global.DIRECTORIO_LOCAL + "/" + path;
		}

	//--- Lee los datos del archivo y construye la trama para enviar -------------------
	private String leeArchivo(String path) throws Exception
		{
		debug("Entrando a leeArchivo");
		String trama = "",
			linea,
			fecha,
			folio,
			sucursal,
			entidad,
			cuenta,
			cheque,
			importe,
			status,
			causaDevol,
			ref;

		StringBuffer bufferLinea;
		StringTokenizer bufferTokens;
		BufferedReader archivo;
		int car;

		archivo = new BufferedReader(new FileReader(path));

		linea = archivo.readLine();
		car = linea.indexOf("|");
		trama = linea.substring(0,car) + "@" + linea.substring(car+1,linea.length()-1);

		while((linea = archivo.readLine()) != null)
			{
			bufferLinea = new StringBuffer(linea);
			while((car = bufferLinea.toString().indexOf("||")) != -1) bufferLinea.insert(car+1," ");
			bufferTokens = new StringTokenizer(bufferLinea.toString(),"|");
			fecha = bufferTokens.nextToken();
			folio = bufferTokens.nextToken();
			sucursal = bufferTokens.nextToken();
			entidad = bufferTokens.nextToken();
			cuenta = bufferTokens.nextToken();
			cheque = bufferTokens.nextToken();
			importe = bufferTokens.nextToken();
			status = bufferTokens.nextToken();
			causaDevol = bufferTokens.nextToken();
			ref = bufferTokens.nextToken();
			fecha = fecha.substring(8,10) + "/" + fecha.substring(5,7) + "/" + fecha.substring(0, 4);
			importe = formateaMoneda(importe);
			if(causaDevol.equals(" ")) causaDevol = "&nbsp;";

			trama += "@" + fecha + "|" + sucursal + "|" + entidad + "|" + cuenta + "|" + cheque + "|"
				+ importe + "|" + status + "|" + causaDevol + "|" + folio + "|" + ref;
			}
		archivo.close();
		debug("Saliendo de leeArchivo");
		return trama;
		}

	//--- Traduce el archivo de Tuxedo para que se pueda exportar ----------------------
	private String traduceArchivo(String nombreArchivo, String trama, HttpServletRequest req) throws Exception
		{
		debug("Entrando a traduceArchivo");
		BufferedReader archTux = new BufferedReader(new FileReader(nombreArchivo));
		String linea, propietario, cuenta;
		StringBuffer buffer;
		boolean primeraLinea = true;
		int car;

		// parche de Ram�n Tejada
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		EI_Exportar archivo;
		ArchivoRemoto archR;

		StringTokenizer tokens = new StringTokenizer(trama.substring(1),"|");
		cuenta = tokens.nextToken(); propietario = tokens.nextToken();

		nombreArchivo = session.getUserID8() + ".doc";
		archivo = new EI_Exportar(IEnlace.DOWNLOAD_PATH + nombreArchivo);
		archivo.creaArchivo();

		while((linea = archTux.readLine()) != null)
			{
			buffer = new StringBuffer(linea);
			while((car = buffer.toString().indexOf("|")) != -1) buffer.replace(car,car+1,",");
			if(primeraLinea)
				{
				primeraLinea = false;
				buffer.append(propietario + "," + cuenta);
				}
			else
				buffer.deleteCharAt(buffer.length()-1);
			archivo.escribeLinea(buffer.toString()+"\n");
			}

		archTux.close();

		archivo.cierraArchivo();
		archR = new ArchivoRemoto();
		if(!archR.copiaLocalARemoto(nombreArchivo,"WEB"))
			{
			debug("Error en traduceArchivo(): No se pudo crear archivo para exportar saldos");
			throw new Exception("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde");
			}
		debug("Saliendo de traduceArchivo");
		return "/Download/" + nombreArchivo;
		}

	//--- Cuenta los registros dentro de la trama --------------------------------------
	private int cuentaRegistros(String trama) throws Exception
		{
		debug("Entrando a cuentaRegistros");
		int numReg = 0;
		StringTokenizer tokens = new StringTokenizer(trama,"@");
		while(tokens.hasMoreTokens()) {numReg++; tokens.nextToken();}
		debug("Saliendo de cuentaRegistros");
		return numReg;
		}

	//--- 'Recorta' la consulta para mostrar solo las cuentas de la p�gina seleccionada-
	private String recortaInfo(String trama, int offset, int numCtas)  throws Exception
		{
		debug("Entrando a recortaInfo");
		int actual = 0;
		StringTokenizer tokens = new StringTokenizer(trama,"@");
		trama = "";
		while(tokens.hasMoreTokens() && actual<offset) {actual++; tokens.nextToken();}
		while(tokens.hasMoreTokens() && actual<offset+numCtas) {actual++; trama += "@"+tokens.nextToken();}
		debug("Saliendo de recortaInfo");
		return trama;
		}

	//--- Formatea una cadena que representa un n�mero que es una cant. de moneda ------
	private String formateaMoneda(String cadena)
		{
		debug("Entrando a formateaMoneda");
		cadena = cadena.trim();
		if(cadena.indexOf(".") == -1) cadena += ".00";
		while(cadena.indexOf(".")+3>cadena.length()) cadena +="0";
		while(cadena.indexOf(".")+3<cadena.length()) cadena = cadena.substring(0,cadena.length()-1);
		for(int a=cadena.length()-6;a>0;a-=3) cadena = cadena.substring(0,a) + "," + cadena.substring(a);
		debug("Saliendo de formateaMoneda");
		return "$" + cadena;
		}

	//---
	private String fechaInicial()
		{
		debug("Entrando a fechaInicial");
		GregorianCalendar fecIni = new GregorianCalendar();
		if(fecIni.get(Calendar.DATE) == 1) fecIni.add(Calendar.MONTH,-1); else fecIni.set(Calendar.DATE, 1);
		debug("Saliendo de fechaInicial");
		return formateaFecha(fecIni);
		}

	//---
	private String fechaFinal()
		{
		debug("Entrando a fechaFinal");
		GregorianCalendar fecFin = new GregorianCalendar();
		fecFin.add(Calendar.DATE,-1);
		debug("Saliendo de fechaFinal");
		return formateaFecha(fecFin);
		}

	//---
	private String formateaFecha(GregorianCalendar fecha)
		{
		debug("Entrando a formateaFecha");
		String dia, mes, anno;
		int m = fecha.get(Calendar.MONTH) + 1;
		dia = ((fecha.get(Calendar.DATE)<10)?"0":"") + fecha.get(Calendar.DATE);
		mes = ((m<10)?"0":"") + m;
		anno = "" + fecha.get(Calendar.YEAR);
		debug("Saliendo de formateaFecha");
		return dia + "/" + mes + "/" + anno;
		}

	//---
	private void debug(String mensaje)
		{EIGlobal.mensajePorTrace("<DEBUG Multicheque.java> " + mensaje, EIGlobal.NivelLog.INFO);}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal(session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		request.setAttribute( "URL", "ChesBenefBaja?ventana=10" );

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s27000h", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	}