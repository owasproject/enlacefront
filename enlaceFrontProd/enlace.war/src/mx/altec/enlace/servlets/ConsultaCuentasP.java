package mx.altec.enlace.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet para realizar pruebas unitarias
 */
public class ConsultaCuentasP extends HttpServlet {
	
	private static final long serialVersionUID = -3670295314163504077L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("error");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		StringBuilder str = new StringBuilder();
		str.append("{\"cuentas\": [ ");
		
		str.append("{");
		str.append("\"numero\":\"56533201896\",");
		str.append("\"descripcion\":\"Fermin Martinez Hernandez\",");
		str.append("\"tipo\":\"P\"");
		str.append("},");
		str.append("{");
		
		str.append("\"numero\":\"56533201896\",");
		str.append("\"descripcion\":\"Fermin Martinez Hernandez\",");
		str.append("\"tipo\":\"P\"");
		str.append("},");
		
		str.append("{");
		str.append("\"numero\":\"\",");
		str.append("\"descripcion\":\"La consulta gener� m�s de 25 resultados, debe ser m�s espec�fico en el filtro\",");
		str.append("\"tipo\":\"P\"");
		str.append("}");

		str.append("]}");
		
		String jsonRespuesta = str.toString();
		
		System.out.println("Respuesta: " + jsonRespuesta);

		res.setHeader("Content-Type", "application/json; charset=ISO-8859-1");
		ServletOutputStream sos = res.getOutputStream();
		sos.print(jsonRespuesta);
		sos.flush();
		sos.close();
	}
}