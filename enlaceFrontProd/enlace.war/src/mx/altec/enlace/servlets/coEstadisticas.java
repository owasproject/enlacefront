package mx.altec.enlace.servlets;


import java.sql.SQLException;
import java.util.*;
import java.math.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.servicioConfirming;
import mx.altec.enlace.dao.CombosConf;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;


public class coEstadisticas extends BaseServlet
	{
	/** Petici�n GET */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Petici�n POST */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{defaultAction(request, response);}

	/** Punto de atenci�n para todas las peticiones */
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

		// si la sesi�n no es v�lida, no se permite entrar al m�dulo
		if (!SesionValida(req, res))
			{
			req.setAttribute("MsgError", IEnlace.MSG_PAG_NO_DISP);
			evalTemplate(IEnlace.ERROR_TMPL, req, res);
			return;
			}


		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se asignan atributos para pasar al jsp
		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu",       session.getFuncionesDeMenu());
		req.setAttribute("diasInhabiles", diasInhabilesAnt());
		req.setAttribute("Encabezado",    CreaEncabezado("Reporte de estad&iacute;sticas de proveedores", "Servicos &gt; Confirming &gt; Estadisticas &gt; Reportes","s28310h",req));
		session.setModuloConsultar(IEnlace.MCargo_transf_pesos);
		// se redirige el control seg�n el tipo de operaci�n
		String opcion = req.getParameter("tipoReq");
		if(opcion == null){
//			TODO: BIT CU 4361. El cliente entra al flujo
			/*
    		 * VSWF ARR -I
    		 */
			if (Global.USAR_BITACORAS.trim().equals("ON")){
				try{

				BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
				if (req.getParameter(BitaConstants.FLUJO)!=null){
				       bh.incrementaFolioFlujo((String)req.getParameter(BitaConstants.FLUJO));
				  }else{
				   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
				  }
				BitaTransacBean bt = new BitaTransacBean();
				bt = (BitaTransacBean)bh.llenarBean(bt);
				bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_EST_REP_ENTRA);
				bt.setContrato(session.getContractNumber());

				BitaHandler.getInstance().insertBitaTransac(bt);
				}catch (SQLException ex){
					//ex.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = ex.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coEstadisticas", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("coEstadisticas::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ ex.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}catch(Exception e){
					//e.printStackTrace();
					StackTraceElement[] lineaError;
					lineaError = e.getStackTrace();
					EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coEstadisticas", EIGlobal.NivelLog.ERROR);
					EIGlobal.mensajePorTrace("coEstadisticas::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
											+ e.getMessage()
				               				+ "<DATOS GENERALES>"
								 			+ "Linea encontrada->" + lineaError[0]
								 			, EIGlobal.NivelLog.ERROR);
				}
			}
    		/*
    		 * VSWF ARR -F
    		 */
			try{
				filtro(req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar el filtro del servlet coEstadisticas", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coEstadisticas::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		else if(opcion.equals("1")){
			try{
				consulta("",req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la consulta del servlet coEstadisticas", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coEstadisticas::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}else if(opcion.equals("2")){
			try{
				detalle("",req,res);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar el detalle de los pagos del servlet coEstadisticas", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coEstadisticas::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
	}

	/** C�digo encargado de mostrar el filtro para las consultas */
	private void filtro(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource)req.getSession ().getAttribute ("session");

		//session.setModuloConsultar(IEnlace.MOperaciones_mancom);  <--- PENDIENTE: averiguar qu� fregaos es esto
		// Combo de proveedores detallado confirming fase II
        servicioConfirming arch_combos = new servicioConfirming();
        arch_combos.setUsuario(session.getUserID8());
        arch_combos.setContrato(session.getContractNumber());
        arch_combos.setPerfil(session.getUserProfile());
        req.setAttribute("arch_combos", arch_combos);

		// Se preparan atributos para enviar al jsp
		req.setAttribute("Lista",listaProv(req));
		req.setAttribute("Anio", ObtenAnio());
		req.setAttribute("Mes", ObtenMes());
		req.setAttribute("Dia", ObtenDia());

		// se pasa el control al jsp
		evalTemplate("/jsp/coEstadisticas.jsp",req,res);
		}

	/** C�digo encargado de atender las consultas */
	private void consulta(String mensaje, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		String trama = "";
		StringBuffer info = new StringBuffer("");
		Vector notasCanceladas = new Vector();

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se obtienen y configuran par�metros
		String parReg = req.getParameter("Reg");
		String parArch = req.getParameter("Arch");
		String parFecha1 = req.getParameter("fecha1");
		String parFecha2 = req.getParameter("fecha2");
		String parTDoc[] = req.getParameterValues("TDoc");
		String parImporte = req.getParameter("Importe");
		String parProveedor = req.getParameter("Proveedor");
		String parEstatus[] = req.getParameterValues("Estatus");
		String divisa = req.getParameter("Divisa");
		String cuentaCargo = req.getParameter("textCuentas");

        //JIRM STEFANINI 23/MAR/2009 VAR PAR AEL FILTRO DIVISA

		EIGlobal.mensajePorTrace("Divisa de reporte: >>>" + divisa, EIGlobal.NivelLog.DEBUG);
		if(divisa.equals("1"))
			divisa="MXN";
		if(divisa.equals("2"))
			divisa="USD";
		if(divisa.equals("0"))
			divisa="";

		if(parReg == null) parReg = "1";
		if(parArch == null) parArch = "";
		if(parTDoc == null) parTDoc = new String[0];;
		if(parFecha1 == null) parFecha1 = "";
		if(parFecha2 == null) parFecha2 = "";
		if(parImporte == null) parImporte = "";
		if(parEstatus == null) parEstatus = new String[0];
		if(parProveedor == null) parProveedor = "";

		for(int x=1;x<parTDoc.length;x++) parTDoc[x]="2";

		// PENDIENTE: la facultad
		//boolean Facu =   session.getFacultad(session.FAC_CCCANCELPAG_CONF);  req.setAttribute("facu",""+Facu);

		// Se crea la trama para Tuxedo
		trama = "2EWEB|" + session.getUserID8() + "|CFAE|" + session.getContractNumber() + "|" + session.getUserID8() + "|" +
			session.getUserProfile() + "|" + parReg + "@";
		if(parReg.equals("1")) trama += parArch; else trama += parFecha1 + "@" + parFecha2;
		trama += "@|";
		debug("Trama de env�o: " + trama, EIGlobal.NivelLog.DEBUG);

		// Se env�a la consulta a Tuxedo
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable ht = null;
		try
			{ht = tuxGlobal.web_red(trama);}
		catch(Exception e)
			{
			debug("Excepci�n al enviar trama a tuxedo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}
		trama = (String) ht.get("BUFFER");
		debug("tuxedo regresa:" + trama, EIGlobal.NivelLog.DEBUG);

		// Se recupera el archivo de resultados de la consulta mediante copia remota
		trama = trama.substring(trama.lastIndexOf('/')+1);
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		if(archRemoto.copia(trama))
			{
			debug("Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +trama;
			}
		else
			{
			debug("Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// Se renombra el archivo
		File nombreViejo = new File(trama);
		File nombreNuevo = new File(Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas");
		if(nombreViejo.renameTo(nombreNuevo))
			{
			debug("Archivo renombrado", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas";
			}
		else
			{
			debug("Error al renombrar archivo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// se construye la l�nea que indica en el jsp la selecci�n de consulta
		String sel = (parReg.equals("1"))?("Archivo: "+parArch):("Movimientos del " + parFecha1 + " al " + parFecha2);

		// Se obtiene lista de proveedores
		String lista[][] = listaProv(req);

		// --------------------- Se procesa el archivo ----------------------
		BufferedReader entrada = new BufferedReader(new FileReader(trama));
		String linea;
		String campos[] = new String[30];
		StringTokenizer parser;
		Hashtable datosTotales = new Hashtable();
		Hashtable datosTotalesNum = new Hashtable();
		Hashtable datosUnProveedor;
		Hashtable datosUnProveedorNum;
		String importeID,claveID, nombreID, auxID,redivisa="";
		BigDecimal numero;
		Hashtable datosDivisa = new Hashtable();

		int pos;
		boolean tieneDatos = false;

		entrada.readLine();
		while((linea=entrada.readLine()) != null)
			{
			while((pos=linea.indexOf(";;")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
			parser = new StringTokenizer(linea,";");
			try
				{
				for(pos=0;parser.hasMoreTokens();pos++) campos[pos] = parser.nextToken();
				while(++pos < 30) campos[pos] = "";
				}
			catch(Exception e)
				{
				debug("Excepci�n al parsear l�nea", EIGlobal.NivelLog.INFO);
				error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
				return;
				}

			for(int xx=0; xx<30; xx++)
				System.out.print(" <-<>-> "+campos[xx]); EIGlobal.mensajePorTrace("", EIGlobal.NivelLog.DEBUG);

			// En este punto se capturan los n�meros de las notas canceladas
			if(campos[0].equals("2") && campos[11].equals("C")) notasCanceladas.add(campos[4]);

			// AQUI ES EL LUGAR PARA APLICAR FILTROS
			if(!campos[0].equals("3"))
				{

				// parche para ya no pasar notas
				if(campos[0].equals("2")) continue;
				// -----------------------------

				int car;
				String aux;

				// filtro de fechas y proveedores
				if(parReg.equals("2"))
					{
					if(!parProveedor.equals("") && !parProveedor.equals(campos[3])) continue;
					aux=(campos[0].equals("1"))?campos[9]:campos[7];
					if(!(fechaMayor(aux,parFecha1) && fechaMenor(aux,parFecha2))) continue;
					}

				// filtro por estatus
				aux="";
				if(campos[0].equals("1"))
					{
					if(campos[18].equals("P") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("C") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("R"))
						{aux="V";}
					else if(campos[18].equals("P") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("L"))
						{aux="L";}
					else if(campos[18].equals("C") && campos[19].equals("P"))
						{aux="P";}
					else if(campos[18].equals("C") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("C") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("C"))
						{aux="Z";}
					else if(campos[18].equals("P") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("P") && campos[19].equals("S"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("A"))
						{aux="V";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="V";}
					}
				else
					{aux = campos[11];}
				for(car=0;car<parEstatus.length;car++) if(aux.equals(parEstatus[car])) break;
				campos[19] = aux;
				if(parEstatus.length>0 && car>=parEstatus.length) continue;
				//JIRM STEFANINI 23/MAR/2009 FILTRO DIVISA
				//	 filtro de divisa
				String divisaReg =campos[24];
				EIGlobal.mensajePorTrace("*******************************************************************************************************************", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Divisa seleccionada: >>>>" + divisa + " <<<<  Divisa de Registro: >>>>" + divisaReg + "   Campo1----->"+ campos[1], EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("*******************************************************************************************************************", EIGlobal.NivelLog.INFO);
			if(!divisa.equals("")){
//				if(divisa.trim().equals("USD"))
//					aux="USD";
//				else
//					aux="MXN";
//				if(!aux.trim().equals(aux2.trim())) continue;
				if(!divisa.trim().equals(divisaReg.trim())){
					EIGlobal.mensajePorTrace("Registro no motrado----->  Divisa de Registro: >>>>" + divisaReg + "  Campo1----->"+ campos[1],EIGlobal.NivelLog.INFO);
					continue;
				}
			}



//			JIRM STEFANINI 23/MAR/2009 FILTRO CUENTA DE CARGO
//			 filtro cuenta de cargo
			if(!cuentaCargo.equals(""))
				{
				cuentaCargo = cuentaCargo.substring(0,11);
				EIGlobal.mensajePorTrace("----- cuentacargo -*-----------------> " + cuentaCargo, EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("----- cuentacargo -*-----------------> " + campos[23], EIGlobal.NivelLog.INFO);

			     aux=campos[23];
				if(!aux.trim().equals(cuentaCargo.trim())) continue;



			}

				// filtro por tipo de documentos
				aux = campos[1];
				if(campos[1].equals("3") || campos[1].equals("4")) campos[1] = "2";
				for(car=0;car<parTDoc.length;car++) if(campos[1].equals(parTDoc[car])) break;
				if(parTDoc.length>0 && car==parTDoc.length) continue;
				campos[1] = aux;

				// filtro de importe
				if(!parImporte.equals(""))
					{
					aux=(campos[0].equals("1"))?campos[7]:campos[5];
					int x = (int)(Float.parseFloat(aux)*100);
					int y = (int)(Float.parseFloat(parImporte)*100);
					if(x!=y) continue;
					}

				}
			else
				continue;

			// Con una sola vez que pase el filtro, se tentr�n datos para mostrar
			tieneDatos = true;

			// Se guarda la info para (m�s abajo) escribir el archivo de exportaci�n y las l�neas para mostrar
			claveID = campos[0].equals("1")?campos[19]:campos[11];
			//importeID = campos[0].equals("1")?campos[7]:campos[5];
			importeID = campos[8]; // parche para mostrar importes neteados (para quitar este parche: borrar esta l�nea y descomentar la anterior)
			redivisa=campos[24];
			if((campos[1].equals("3") || campos[1].equals("4")) && campos[6].equals("D")) importeID = "-" + importeID;

			datosUnProveedor = (Hashtable)datosTotales.get(campos[3]);
			if(datosUnProveedor == null) datosTotales.put(campos[3] + "_" + campos[24],(datosUnProveedor = new Hashtable()));
			numero = (BigDecimal)datosUnProveedor.get(claveID);
			if(numero == null) datosUnProveedor.put(claveID,(numero = new BigDecimal("0")));

System.out.print((new BigDecimal(importeID)) + " <> " + numero + " + " + importeID + " = ");

			numero = numero.add(new BigDecimal(importeID));

EIGlobal.mensajePorTrace("" + numero, EIGlobal.NivelLog.DEBUG);
EIGlobal.mensajePorTrace("...................................................................DIVISA" + redivisa, EIGlobal.NivelLog.DEBUG);
EIGlobal.mensajePorTrace("...................................................................DIVISA" + redivisa, EIGlobal.NivelLog.DEBUG);
			//ABANDA **2009/06/04**
			//datosDivisa.add(campos[24]);


			datosUnProveedor.put(claveID,numero);

			datosUnProveedorNum = (Hashtable)datosTotalesNum.get(campos[3]);
			if(datosUnProveedorNum == null) datosTotalesNum.put(campos[3] + "_" + campos[24],(datosUnProveedorNum = new Hashtable()));
			numero = (BigDecimal)datosUnProveedorNum.get(claveID);
			if(numero == null) datosUnProveedorNum.put(claveID,(numero = new BigDecimal("0")));
			numero = numero.add(new BigDecimal("1"));
			datosUnProveedorNum.put(claveID,numero);

//			ABANDA **2009/06/04**
			datosDivisa.put(campos[3] + "_" + campos[24],campos[23]);
			datosDivisa.put(campos[3] + "_" + campos[24],campos[24]);

			}
		entrada.close();

		//
		FileWriter exportacion = new FileWriter(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + ".doc");
		Enumeration codigos = datosTotales.keys();
		Enumeration importes;
		BigDecimal aceptados, aceptadosNum;
		int x = 0;
		while(codigos.hasMoreElements())
			{
			aceptados = new BigDecimal("0");
			aceptadosNum = new BigDecimal("0");
			nombreID = (String)codigos.nextElement();
			datosUnProveedor = (Hashtable)datosTotales.get(nombreID);
			datosUnProveedorNum = (Hashtable)datosTotalesNum.get(nombreID);
			auxID = nombreID;
			divisa = (String) datosDivisa.get(auxID.trim());
			nombreID = nombreID.replaceAll("_" + divisa, "");
			nombreID = rellenar(getClave(nombreID,lista),20,' ','D') + rellenar(getProv(nombreID,lista),80,' ','D');
			//ABANDA **2009/06/04**
			numero = (BigDecimal)datosUnProveedorNum.get("R"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea = rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("R"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("P"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("P"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("L"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("L"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("A"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("A"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("C"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("C"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("V"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("V"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("Z"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("Z"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);


			linea = nombreID + rellenar(aceptadosNum.toString(),8,' ','I') + rellenar(aplana(aceptados.toString()),21,' ','I') + linea;
			String lineaExp = linea + rellenar(cuentaCargo,20,' ','I') + rellenar(divisa,4,' ','I');
			exportacion.write(lineaExp + "\n");
			linea = rellenar(auxID,20,' ','I') + linea;
			// ABANDA **2009/06/04 ** Se agrego el campo divisa a la trama
			info.append(linea + divisa + "\n"); // <-----------------------------------------------
			}
		exportacion.close();
		debug("informaci�n a mandar:\n" + info.toString(), EIGlobal.NivelLog.INFO);
		if(!archRemoto.copiaLocalARemoto (session.getUserID8() + ".doc", "WEB" ))
			{debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}

		// Si al final no se tienen l�neas para mostrar, se muestra un aviso
		if(!tieneDatos) {error("La consulta realizada no contiene datos",req,res); return;}

		// Se env�an las opciones de consulta al jsp para que no se pierdan
		req.setAttribute("Reg", parReg);
		req.setAttribute("Arch", parArch);
		req.setAttribute("fecha1", parFecha1);
		req.setAttribute("fecha2", parFecha2);
		req.setAttribute("TDoc", parTDoc);
		req.setAttribute("Importe", parImporte);
		req.setAttribute("Proveedor", parProveedor);
		req.setAttribute("Estatus", parEstatus);
		req.setAttribute("cuentaCargo", cuentaCargo);

		req.setAttribute("redivisa",redivisa);

		// Se asignan atributos para el jsp
		req.setAttribute("Exportacion","/Download/" + session.getUserID8() + ".doc");
		req.setAttribute("Mensaje",mensaje);
		req.setAttribute("Sel",sel);
		req.setAttribute("Info",info.toString());
		EIGlobal.mensajePorTrace("----->VER AQUI POR FAVOR INFOINFO ----------- "+info.toString(), EIGlobal.NivelLog.DEBUG);
		//inicia Modif PVA 08/03/2003
		req.setAttribute("Encabezado",    CreaEncabezado("Reporte de estad&iacute;sticas de proveedores", "Servicos &gt; Confirming &gt; Estadisticas &gt; Reportes","s28310Bh",req));
		//termina
//		TODO: BIT CU 4361. A4
		/*
		 * VSWF ARR -I
		 */
		if (Global.USAR_BITACORAS.trim().equals("ON")){
			try{
				if (parImporte=="" || parImporte==null)
					parImporte="0.00";
			BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_EST_REP_ACTUALIZA_CATALOGOS_CONF);
			bt.setContrato(session.getContractNumber());
			if(parImporte!=null && !parImporte.equals(""))
				bt.setImporte(Double.parseDouble(parImporte));
			bt.setServTransTux("CFAE");
			if(trama!=null){
				 if(trama.substring(0,2).equals("OK")){
					   bt.setIdErr("CFAE0000");
				 }else if(trama.length()>8){
					  bt.setIdErr(trama.substring(0,8));
				 }else{
					  bt.setIdErr(trama.trim());
				 }
				}
			bt.setNombreArchivo(session.getUserID8() + ".doc");
			BitaHandler.getInstance().insertBitaTransac(bt);
			}catch (SQLException ex){
			//ex.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = ex.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la consulta de estadisticas", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coEstadisticas::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ ex.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}catch(Exception e){
				//e.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e.getStackTrace();
				EIGlobal.mensajePorTrace("Error al ejecutar la consulta de estadisticas", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coEstadisticas::consulta:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		/*
		 * VSWF ARR -F
		 */
		evalTemplate("/jsp/coEstadisticasCon.jsp",req,res);
		}



	/** C�digo encargado de atender las consultas */
	private void detalle(String mensaje, HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{

		String trama = "";
		StringBuffer info = new StringBuffer("");
		StringBuffer divisa = new StringBuffer("");
		Vector notasCanceladas = new Vector();

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se obtienen y configuran par�metros
		String parReg = req.getParameter("Reg");
		String parArch = req.getParameter("Arch");
		String parFecha1 = req.getParameter("fecha1");
		String parFecha2 = req.getParameter("fecha2");
		String parTDoc[] = req.getParameterValues("TDoc");
		String parImporte = req.getParameter("Importe");
		String parProveedor = req.getParameter("Proveedor");
		String parEstatus[] = req.getParameterValues("Estatus");

		if(parReg == null) parReg = "2";
		if(parArch == null) parArch = "";
		if(parTDoc == null) parTDoc = new String[0];;
		if(parFecha1 == null) parFecha1 = "";
		if(parFecha2 == null) parFecha2 = "";
		if(parImporte == null) parImporte = "";
		if(parEstatus == null) parEstatus = new String[0];
		if(parProveedor == null) parProveedor = "";

		for(int x=1;x<parTDoc.length;x++) parTDoc[x]="2";

		// Se crea la trama para Tuxedo
		trama = "2EWEB|" + session.getUserID8() + "|CFAE|" + session.getContractNumber() + "|" + session.getUserID8() + "|" +
			session.getUserProfile() + "|" + parReg + "@";
		if(parReg.equals("1")) trama += parArch; else trama += parFecha1 + "@" + parFecha2;
		trama += "@|";
		debug("Trama de env�o: " + trama, EIGlobal.NivelLog.DEBUG);

		// Se env�a la consulta a Tuxedo
		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext()).getContext());
		Hashtable ht = null;
		try
			{ht = tuxGlobal.web_red(trama);}
		catch(Exception e)
			{
			debug("Excepci�n al enviar trama a tuxedo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}
		trama = (String) ht.get("BUFFER");
		debug("tuxedo regresa:" + trama, EIGlobal.NivelLog.DEBUG);

		// Se recupera el archivo de resultados de la consulta mediante copia remota
		trama = trama.substring(trama.lastIndexOf('/')+1);
		ArchivoRemoto archRemoto = new ArchivoRemoto();
		if(archRemoto.copia(trama))
			{
			debug("Copia remota realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +trama;
			}
		else
			{
			debug("Copia remota NO realizada (Tuxedo a local)", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// Se renombra el archivo
		File nombreViejo = new File(trama);
		File nombreNuevo = new File(Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas");
		if(nombreViejo.renameTo(nombreNuevo))
			{
			debug("Archivo renombrado", EIGlobal.NivelLog.INFO);
			trama = Global.DIRECTORIO_LOCAL + "/" +session.getUserID8()+".tramas";
			}
		else
			{
			debug("Error al renombrar archivo", EIGlobal.NivelLog.INFO);
			error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
			return;
			}

		// se construye la l�nea que indica en el jsp la selecci�n de consulta
		String sel = (parReg.equals("1"))?("Archivo: "+parArch):("Movimientos del " + parFecha1 + " al " + parFecha2);

		// Se obtiene lista de proveedores
		String lista[][] = listaProv(req);

		// --------------------- Se procesa el archivo ----------------------
		BufferedReader entrada = new BufferedReader(new FileReader(trama));
		FileWriter exportacion = new FileWriter(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_det.doc");
		String linea;
		String campos[] = new String[31];
		StringTokenizer parser;
		Hashtable datosTotales = new Hashtable();
		Hashtable datosTotalesNum = new Hashtable();
		Hashtable datosUnProveedor;
		Hashtable datosUnProveedorNum;
		String importeID,claveID, nombreID, redivisa="";
		BigDecimal numero;
		Vector lineasDoc = new Vector();

		int pos;
		boolean tieneDatos = false;

		entrada.readLine();
		while((linea=entrada.readLine()) != null)
			{
			while((pos=linea.indexOf(";;")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
			parser = new StringTokenizer(linea,";");
			try
				{
				for(pos=0;parser.hasMoreTokens();pos++) campos[pos] = parser.nextToken();
				while(++pos < 31) campos[pos] = "";
				}
			catch(Exception e)
				{
				debug("Excepci�n al parsear l�nea", EIGlobal.NivelLog.INFO);
				error("Su transacci&oacute;n no puede ser atendida en este momento<BR>intente m&aacute;s tarde",req,res);
				return;
				}

EIGlobal.mensajePorTrace("----->VER AQUI POR FAVOR ----------- ", EIGlobal.NivelLog.DEBUG); for(int xx=0; xx<30; xx++) EIGlobal.mensajePorTrace(" <-<>-> "+campos[xx],EIGlobal.NivelLog.DEBUG); EIGlobal.mensajePorTrace("",EIGlobal.NivelLog.DEBUG);

			// En este punto se capturan los n�meros de las notas canceladas
			if(campos[0].equals("2") && campos[11].equals("C")) notasCanceladas.add(campos[4]);

			// AQUI ES EL LUGAR PARA APLICAR FILTROS
			if(!campos[0].equals("3"))
				{

				// parche para ya no pasar notas
				if(campos[0].equals("2")) continue;
				// -----------------------------

				int car;
				String aux;

				// filtro de fechas y proveedor
				if(parReg.equals("2"))
					{
					if(!parProveedor.equals("") && !parProveedor.equals(campos[3])) continue;
					aux=(campos[0].equals("1"))?campos[9]:campos[7];
					if(!(fechaMayor(aux,parFecha1) && fechaMenor(aux,parFecha2))) continue;
					}

				// filtro por estatus
				aux="";
				if(campos[0].equals("1"))
					{
					if(campos[18].equals("P") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("C") && campos[19].equals("R"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("R"))
						{aux="V";}
					else if(campos[18].equals("P") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="C";}
					else if(campos[18].equals("C") && campos[19].equals("L"))
						{aux="L";}
					else if(campos[18].equals("C") && campos[19].equals("P"))
						{aux="P";}
					else if(campos[18].equals("C") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("C") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("Z"))
						{aux="Z";}
					else if(campos[18].equals("D") && campos[19].equals("C"))
						{aux="Z";}
					else if(campos[18].equals("P") && campos[19].equals("A"))
						{aux="A";}
					else if(campos[18].equals("P") && campos[19].equals("S"))
						{aux="R";}
					else if(campos[18].equals("V") && campos[19].equals("A"))
						{aux="V";}
					else if(campos[18].equals("V") && campos[19].equals("C"))
						{aux="V";}
					}
				else
					{aux = campos[11];}
				for(car=0;car<parEstatus.length;car++) if(aux.equals(parEstatus[car])) break;
				campos[21] = campos[19];
				campos[19] = aux;
				if(parEstatus.length>0 && car>=parEstatus.length) continue;

				// filtro por tipo de documentos
				aux = campos[1];
				if(campos[1].equals("3") || campos[1].equals("4")) campos[1] = "2";
				for(car=0;car<parTDoc.length;car++) if(campos[1].equals(parTDoc[car])) break;
				if(parTDoc.length>0 && car==parTDoc.length) continue;
				campos[1] = aux;

				// filtro de importe
				if(!parImporte.equals(""))
					{
					aux=(campos[0].equals("1"))?campos[7]:campos[5];
					int x = (int)(Float.parseFloat(aux)*100);
					int y = (int)(Float.parseFloat(parImporte)*100);
					if(x!=y) continue;
					}

				}
			else
				continue;

			// Con una sola vez que pase el filtro, se tentr�n datos para mostrar
			tieneDatos = true;

			// se rearreglan las fechas para acomodar la fecha de operaqci�n en al campo adecuado seg�n el status
			if(campos[0].equals("1"))
				{
				if(campos[21].length() == 0) campos[21]="x"; // <-- solo para asegurar long 1
				char statusPago = campos[21].charAt(0);
				switch(statusPago)
					{
					case 'R': campos[11]=campos[10]; break;
					case 'P': case 'C': case 'A': case 'Z': campos[11]=campos[11]; break;// lo se, es redundante, solo para 'recalcar'
					case 'L': campos[11]=campos[12]; break;
					case 'V': campos[11]=campos[13]; break;
					default: campos[11]=campos[10];
					}
				}

			//
			lineasDoc.add(campos[4] + "|" + tipoDocto(campos[1]) + "|" + aMoneda(campos[8]) + "|" +
				campos[10] + "|" + getStatus(campos[19]).substring(0,getStatus(campos[19]).length()-1) + "|" + campos[11] + "|");

			//
			exportacion.write(rellenar(campos[4],8,' ','D') +
				rellenar(campos[1],3,'0','I') +
				rellenar(aplana(campos[8]),21,' ','I') +
				rellenar(voltea(campos[10]),8,' ','I') +
				rellenar(campos[19],1,' ','I') +
				rellenar(voltea(campos[11]),8,' ','I') +
				"\n");

			// Se guarda la info para (m�s abajo) escribir el archivo de exportaci�n y las l�neas para mostrar
			claveID = campos[0].equals("1")?campos[19]:campos[11];
			//importeID = campos[0].equals("1")?campos[7]:campos[5];
			importeID = campos[8]; // parche para mostrar importes neteados (para quitar este parche: borrar esta l�nea y descomentar la anterior)
			redivisa=campos[24];
			if((campos[1].equals("3") || campos[1].equals("4")) && campos[6].equals("D")) importeID = "-" + importeID;

			datosUnProveedor = (Hashtable)datosTotales.get(campos[3]);
			if(datosUnProveedor == null) datosTotales.put(campos[3],(datosUnProveedor = new Hashtable()));
			numero = (BigDecimal)datosUnProveedor.get(claveID);
			if(numero == null) datosUnProveedor.put(claveID,(numero = new BigDecimal("0")));
			numero = numero.add(new BigDecimal(importeID));
			datosUnProveedor.put(claveID,numero);
			datosUnProveedorNum = (Hashtable)datosTotalesNum.get(campos[3]);
			if(datosUnProveedorNum == null) datosTotalesNum.put(campos[3],(datosUnProveedorNum = new Hashtable()));
			numero = (BigDecimal)datosUnProveedorNum.get(claveID);
			if(numero == null) datosUnProveedorNum.put(claveID,(numero = new BigDecimal("0")));
			numero = numero.add(new BigDecimal("1"));
			datosUnProveedorNum.put(claveID,numero);
			EIGlobal.mensajePorTrace("...................................................................DIVISA" + redivisa, EIGlobal.NivelLog.DEBUG);
			}
		entrada.close();
		exportacion.close();

		//
		Enumeration codigos = datosTotales.keys();
		Enumeration importes;
		BigDecimal aceptados, aceptadosNum;
		while(codigos.hasMoreElements())
			{
			aceptados = new BigDecimal("0");
			aceptadosNum = new BigDecimal("0");
			nombreID = (String)codigos.nextElement();
			datosUnProveedor = (Hashtable)datosTotales.get(nombreID);
			datosUnProveedorNum = (Hashtable)datosTotalesNum.get(nombreID);
			nombreID = rellenar(getClave(nombreID,lista),20,' ','D') + rellenar(getProv(nombreID,lista),80,' ','D');

			numero = (BigDecimal)datosUnProveedorNum.get("R"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea = rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("R"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("P"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("P"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("L"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("L"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("A"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("A"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("C"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("C"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("V"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("V"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);

			numero = (BigDecimal)datosUnProveedorNum.get("Z"); if(numero == null) numero = new BigDecimal("0");
			aceptadosNum = aceptadosNum.add(numero);
			linea += rellenar(numero.toString(),8,' ','I');
			numero = (BigDecimal)datosUnProveedor.get("Z"); if(numero == null) numero = new BigDecimal("0");
			linea += rellenar(aplana(numero.toString()),21,' ','I');
			aceptados = aceptados.add(numero);


			linea = nombreID + rellenar(aceptadosNum.toString(),8,' ','I') + rellenar(aplana(aceptados.toString()),21,' ','I') + linea;
			info.append(linea+"\n"); // <-----------------------------------------------
			for(pos=0; pos<lineasDoc.size();pos++) info.append("" + lineasDoc.get(pos) + "\n");
			}

		debug("informaci�n a mandar:\n" + info.toString(), EIGlobal.NivelLog.INFO);
		if(!archRemoto.copiaLocalARemoto (session.getUserID8() + "_det.doc", "WEB" ))
			{debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}

		// Si al final no se tienen l�neas para mostrar, se muestra un aviso
		if(!tieneDatos) {error("La consulta realizada no contiene datos",req,res); return;}

		// Se env�an las opciones de consulta al jsp para que no se pierdan
		req.setAttribute("Reg", parReg);
		req.setAttribute("Arch", parArch);
		req.setAttribute("fecha1", parFecha1);
		req.setAttribute("fecha2", parFecha2);
		req.setAttribute("TDoc", parTDoc);
		req.setAttribute("Importe", parImporte);
		req.setAttribute("Proveedor", parProveedor);
		req.setAttribute("Estatus", parEstatus);

		// Se asignan atributos para el jsp
		req.setAttribute("Exportacion","/Download/" + session.getUserID8() + "_det.doc");
		req.setAttribute("Mensaje",mensaje);

		req.setAttribute("Sel",sel);
		req.setAttribute("Info",info.toString());
		//inicia Modif PVA 08/03/2003
		req.setAttribute("Encabezado",    CreaEncabezado("Reporte de estad&iacute;sticas de proveedores", "Servicos &gt; Confirming &gt; Estadisticas &gt; Reportes","s28310Bh",req));
		//termina
		evalTemplate("/jsp/coEstadisticasDet.jsp",req,res);
		}

	private String tipoDocto(String tipo)
		{
		String resultado = "no especificado";
		if(tipo.equals("1"))
			resultado = "Factura";
		else if(tipo.equals("2"))
			resultado = "Otros";
		else if(tipo.equals("3"))
			resultado = "Nota de cargo";
		else if(tipo.equals("4"))
			resultado = "Nota de cr&eacute;dito";
		return resultado;
		}




/** Devuelve una lista con los nombres de proveedores ([ren][1]) y sus respectivos n�meros ([ren][0]) */
	private String[][] listaProv(HttpServletRequest req)
		{

		// parche que corrige la megaincidencia
		BaseResource session = (BaseResource) req.getSession ().getAttribute ("session");

		// Se prepara el objeto CombosConf usado para consultas a Tuxedo
		CombosConf arch_combos = new CombosConf();

		//arch_combos.getServicioTux().setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) getServletContext ()).getContext ());
		arch_combos.setUsuario(session.getUserID8());
		arch_combos.setContrato(session.getContractNumber());
		arch_combos.setPerfil(session.getUserProfile());

		String  divisa="";

        //JIRM STEFANINI 23/MAR/2009 VAR PAR AEL FILTRO DIVISA


		String archivo = arch_combos.envia_tux(divisa);

		Vector lineas = new Vector();
		String [][] lista = null;
		BufferedReader entrada = null;
		String linea = null;
		String campos[] = new String[6];
		StringTokenizer parser = null;


		try
			{
			entrada = new BufferedReader(new FileReader(archivo));
			int car;

			while((linea=entrada.readLine())!=null)
				{
				if(!linea.startsWith("9")) continue;
				while((car=linea.indexOf(";;"))!=-1) linea = linea.substring(0,car+1) + " " + linea.substring(car+1);
				lineas.add(linea);
				}
			lista = new String[lineas.size()][3];
			for(car=0;car<lineas.size();car++)
				{
				linea=(String)lineas.get(car);
				parser = new StringTokenizer(linea,";");
				parser.nextToken();
				for(int x=0;x<6;x++) campos[x]=parser.nextToken();
				if(campos[0].equals("F")) campos[3] += " " + campos[4] + " " + campos[5];
				lista[car][0]=campos[1];
				lista[car][1]=campos[3];
				lista[car][2]=campos[2];
				}
			}
		catch(Exception e)
			{
			debug("Excepci�n en listaProv", EIGlobal.NivelLog.INFO);
			}

		return lista;
		}

	/** Imprime un mensaje al log */
	private void debug(String mensaje, EIGlobal.NivelLog nivel)
		{
		EIGlobal.mensajePorTrace("<DEBUG coEstadisticas.java> " + mensaje, nivel);
		}

	/** Muestra una p�gina de error con la descripci�n indicada */
	private void error(String mensaje,HttpServletRequest request, HttpServletResponse response)
		{
		try
			{
			despliegaMensaje(mensaje,
				"Consulta y cancelaci&oacute;n de pagos",
				"Confirming &gt; Consulta y cancelaci&oacute;n de pagos",
				request,
				response);
			}
		catch(Exception e)
			{
			debug("Excepci�n al intentar mostar la pantalla de error", EIGlobal.NivelLog.INFO);
			}
		}

	/** Devuelve el nombre de un proveedor seg�n su n�mero */
	private String getProv(String clave, String[][] lista)
		{
		String prov = "no disponible";
		int pos;
		for(pos=0;pos<lista.length;pos++)
			{if(clave.equals(lista[pos][0])) {prov=lista[pos][1]; break;}}
		return prov;
		}

	/** Devuelve el nombre de un proveedor seg�n su n�mero */
	private String getClave(String clave, String[][] lista)
		{
		String prov = "no disponible";
		int pos;
		for(pos=0;pos<lista.length;pos++)
			{if(clave.equals(lista[pos][0])) {prov=lista[pos][2]; break;}}
		return prov;
		}

	/** Devuelve el estatus del documento seg�n el tipo indicado */
	private String getStatus(String tipoDoc)
		{
		if(tipoDoc.trim().equals("R"))
			tipoDoc = "Por Pagar ";
		else if(tipoDoc.trim().equals("L"))
			tipoDoc = "Liberados";
		else if(tipoDoc.trim().equals("P"))
			tipoDoc = "Pagados";
		else if(tipoDoc.trim().equals("A"))
			tipoDoc = "Anticipados";
		else if(tipoDoc.trim().equals("C"))
			tipoDoc = "Cancelados";
		else if(tipoDoc.trim().equals("Z"))
			tipoDoc = "Rechazados";
		else if(tipoDoc.trim().equals("V"))
			tipoDoc = "Vencidos";
		else if(tipoDoc.trim().equals("D"))
			tipoDoc = "Devueltos";
		else
			tipoDoc = "No especificado ";
		return tipoDoc;
		}

	/** Indica si 'mayor' es mayor a 'menor' */
	private boolean fechaMayor(String mayor, String menor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Indica si 'menor' es mayor a 'mayor' */
	private boolean fechaMenor(String menor, String mayor)
		{
		int max = Integer.parseInt(mayor.substring(6,10) + mayor.substring(3,5) + mayor.substring(0,2));
		int min = Integer.parseInt(menor.substring(6,10) + menor.substring(3,5) + menor.substring(0,2));
		return max>=min;
		}

	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num)
		{
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return "$" + num;
		}

	/** Prepara una importe para colocarlo en un archivo de exportaci�n */
	private String aplana(String num)
		{
		String aux = "";
		num=aMoneda(num);
		for(int x=0;x<num.length();x++) if(",$.".indexOf(num.substring(x,x+1)) == -1) aux += num.substring(x,x+1);
		return aux;
		}

	/** Prepara una fecha para colocarla en un archivo de exportaci�n */
	private String voltea(String fecha)
		{
		return fecha.substring(3,5) + fecha.substring(0,2) + fecha.substring(6,10);
		}

	/** rellena un campo al ancho indicado
	 * @param cad cadena a rellenar
	 * @param lon longitud a rellenar
	 * @param rel caracter de relleno
	 * @param tip tipo de relleno (I=Izq, D=Der)
	 */
	public static String rellenar (String cad, int lon, char rel, char tip)
		{
		String aux = "";
		if(cad.length()>lon) return cad.substring(0,lon);
		if(tip!='I' && tip!='D') tip = 'I';
		if( tip=='D' ) aux = cad;
			for (int i=0; i<(lon-cad.length()); i++) aux += ""+ rel;
		if( tip=='I' ) aux += ""+ cad;
		return aux;
		}

	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

		// parche de Ram�n Tejada
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		EIGlobal Global = new EIGlobal( session , getServletContext() , this );

		String contrato_ = session.getContractNumber();

		if(contrato_==null) contrato_ ="";

		request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
		request.setAttribute( "Error", mensaje );
		request.setAttribute( "URL", "coEstadisticas");

		request.setAttribute( "MenuPrincipal", session.getStrMenu() );
		request.setAttribute( "newMenu", session.getFuncionesDeMenu());
		request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "s29020h", request));

		request.setAttribute( "NumContrato", contrato_ );
		request.setAttribute( "NomContrato", session.getNombreContrato() );
		request.setAttribute( "NumUsuario", session.getUserID8() );
		request.setAttribute( "NomUsuario", session.getNombreUsuario() );

		evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}

	public static void main(String args[])
		{
		coEstadisticas x = new coEstadisticas();
		System.out.println(x.aplana("4109234.37"));
		}

	}