/**
 ******************************************************************************
 * 
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 * CertificadoDigitalServlet.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour 	  By 		Company 	     Description
 * ======= =========== ========= =============== ==============================
 * 1.0 	    21/10/2013 FSW-Indra  Indra Company      Creacion
 *
 ******************************************************************************
 **/
package mx.altec.enlace.servlets;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.beans.CertificadoDigitalBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.bo.CertificadoDigitalBO;
import mx.altec.enlace.bo.CertificadoDigitalBOImpl;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

/**
 * Servlet implementation class CertificadoDigitalServlet
 */
public class CertificadoDigitalServlet extends BaseServlet {
	

	
	/** Serial Number ID **/
	private static final long serialVersionUID = 1L;
	
	/**CONSTANTE ERROR**/
	private static final String ERROR = "ERROR";
       
	/**CONSTANTE EXISTE**/
	private static final String EXISTE = "EXISTE";
	
	/**CONSTANTE EXITO**/
	private static final String EXITO = "EXITO";	
	
	/**CONSTANTE REVOCADO**/
	private static final String REVOCADO = "REVOCADO";	
	
	/**CONSTANTE ESTATUS**/
	private static final String ESTATUS = "ESTATUS";	
	
	/**CONSTANTE SESSION**/
	private static final String SESSION = "session";	
	
	/**CONSTANTE CLASE**/
	private static final String CLASE = " Clase [ ";
	
	/**CONSTANTE CONCATENA**/
	private static final String CONCATENA = "] | ";
	
	/**Cadena para el nombre del archivo**/
	private static final String CADENA_NOMBRE_ARCHIVO = "nombreArchivo";

	  /** {@inheritDoc} */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		evaluarPeticion(request,response);
	}

	  /** {@inheritDoc} */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		evaluarPeticion(request,response);
	}
	
	/**
	 * Metodo que evlua la peticion del request o response
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 * @throws IOException :  Excepci�n de Entrada/Salida que se regresa al contenedor web en caso de falla.
	 */
	protected void evaluarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String accion = null; 
		accion = request.getParameter("accion") == null ? "" : request.getParameter("accion");
		
		EIGlobal.mensajePorTrace("Clase  [ " + this.getClass().getName() + "]  | " +  " method [ evaluarPeticion ] " + " peticion [ " + accion + " ]", EIGlobal.NivelLog.DEBUG);
		
		if("".equals(accion)){
			consultaCertificado(request,response);
		} else if("bajar".equals(accion)){		
			bajarHerramienta(request,response);			
		} else if("revocar".equals(accion)){
			revocarCertificado(request,response);
		} else if("bajarCer".equals(accion)){
			bajarCertificado(request,response);
		}			
		
	}

	
	/**
	 * Metodo para revocar el certificado
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 * @throws IOException :  Excepci�n de Entrada/Salida que se regresa al contenedor web en caso de falla.
	 */
	protected void revocarCertificado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + "]| " +  " method [ revocarCertificado ] ", EIGlobal.NivelLog.DEBUG);
		HttpSession sess = request.getSession();
		BaseResource session = (BaseResource) sess.getAttribute(SESSION);
		String valida = request.getParameter("valida");
		boolean sesionvalida = SesionValida( request, response );
    	
    	if ( !sesionvalida ) {
			return;
		}
		
		if (valida == null) {
			valida = validaToken(request, response);
		}
		
		
		
		if (valida != null && "1".equals(valida)) {
		
			
			String nombreArchivo = request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO) == null ? "" : request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO).toString();
			CertificadoDigitalBOImpl certificadoDigitalBO = new CertificadoDigitalBOImpl();
			
			String cliente = session.getUserID8();
			String contrato = session.getContractNumber();
			boolean resultado;
			resultado = false;
			try{
				resultado = certificadoDigitalBO.revocaCertificado(Global.CANAL_CIFRADO, cliente, contrato);
			}catch(BusinessException e){
				EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ revocarCertificado ] EXCEPCION:: " + e.getMessage(), EIGlobal.NivelLog.DEBUG );
				request.setAttribute("ERROR", "Por el momento no se puede realizar la operaci�n, int�ntelo m�s tarde. ");
			}
	
			int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));

			if(resultado) {
				bitacorizaProceso(request, referencia, "RCCE","0000",nombreArchivo,"A");
				request.setAttribute(EXITO, "Certificado Revocado.");
				consultaCertificado(request, response);
				return;
			}else{
				bitacorizaProceso(request, referencia, "RCCE","0001",nombreArchivo,"R");
				request.setAttribute(ERROR, "Por el momento no se puede realizar la operaci�n, int�ntelo m�s tarde.");
			}

			direccionamiento(request, response);
		}
	}
	
	/**
	 * Metodo de consulta de la pantalla de certificado
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 * @throws IOException :  Excepci�n de Entrada/Salida que se regresa al contenedor web en caso de falla.
	 */
	protected void consultaCertificado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + "]  | " +  " method [ consultaCertificado ] ", EIGlobal.NivelLog.DEBUG );
		
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSION);
		CertificadoDigitalBO certificadoBO = new CertificadoDigitalBOImpl();
		boolean isServicioActivo = false;
		boolean valida = false;
		String cliente = session.getUserID8();
		String contrato = session.getContractNumber();
		int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		String nombreArchivo = request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO) == null ? "" : request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO).toString();

		try {
			isServicioActivo = certificadoBO.servicioActivo();
			if(!isServicioActivo) {
				request.setAttribute(ERROR, "El Servicio de Importaci�n de Archivos Cifrados no se encuentra activo, por favor p�ngase en contacto con nosotros v�a Superl�nea, o acuda a una sucursal Santander para activar este servicio.");
				direccionamiento(request, response);
				return;
			}
			
			CertificadoDigitalBOImpl certificadoDigitalBO = new CertificadoDigitalBOImpl();
			CertificadoDigitalBean certificadoDigital = new CertificadoDigitalBean();
			
			try{
				certificadoDigital = certificadoDigitalBO.consultaCertificado(Global.CANAL_CIFRADO, cliente, contrato, valida);
				bitacorizaProceso(request, referencia, "CCCE","0000",nombreArchivo,"A");
			}catch(BusinessException e){
				EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ consultaCertificado ] EXCEPCION:: " + e.getMessage(), EIGlobal.NivelLog.DEBUG );
				request.setAttribute("ERROR", "Por el momento no se puede realizar la operaci�n, int�ntelo m�s tarde. ");
				bitacorizaProceso(request, referencia, "CCCE","0001",nombreArchivo,"R");
			}

			request.setAttribute("datosCertificado", certificadoDigital);
			if("R".equals(certificadoDigital.getEstatus())){
				request.setAttribute(REVOCADO, "block");
				request.setAttribute(ESTATUS, "REVOCADO");				
				
			}else if("V".equals(certificadoDigital.getEstatus())){
				request.setAttribute(EXISTE, certificadoDigital.isExiste());
			}else if("C".equals(certificadoDigital.getEstatus())){
				request.setAttribute(REVOCADO, "block");
				request.setAttribute(ESTATUS, "VENCIDO");
				
			}else{
				request.setAttribute(EXISTE, "false");
			}
		} catch (SQLException e) {
			request.setAttribute(ERROR, "Error en el Servicio de Importaci�n de Archivos Cifrados, por favor p�ngase en contacto con nosotros v�a Superl�nea, o acuda a una sucursal Santander para activar este servicio.");
			//evalTemplate("/jsp/certificadoDigital.jsp", request, response);
			evalTemplate(Global.RUTA_CERTIFICADOJSP, request, response);
			return;
		}
		
		direccionamiento(request, response);

	}	
	
	
	/**
	 * Validar Token
	 * @param request : request
	 * @param response : response
	 * @return String
	 * @throws IOException : exception
	 */
	@SuppressWarnings({ "static-access" })
	private String validaToken(HttpServletRequest request, HttpServletResponse response) 
		throws IOException{
		EIGlobal.mensajePorTrace("Se verifica si el usuario tiene token y si la transacci&oacute;n est&aacute; parametrizada para solicitar la validaci&oacute;n con el OTP", EIGlobal.NivelLog.DEBUG);
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSION);
		boolean solVal = ValidaOTP.solicitaValidacion(session.getContractNumber(), 1045);
		
		if (session.getValidarToken() && session.getFacultad(session.FAC_VAL_OTP) && 
				session.getToken().getStatus() == 1 && solVal) {
			EIGlobal.mensajePorTrace("   El usuario tiene Token. \nSolicito la validaci&oacute;n. \nSe guardan los parametros en sesion", EIGlobal.NivelLog.DEBUG);
			
			ValidaOTP.guardaParametrosEnSession(request);
			ValidaOTP.mensajeOTP(request, "CifradoDigital");
			ValidaOTP.validaOTP(request, response, IEnlace.VALIDA_OTP_CONFIRM);
		} else {
			return "1";
		}
		return "";
		
	}
	
	/**
	 * Metodo para bajar el software de cifrado
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 * @throws IOException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 */
	protected void bajarHerramienta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ bajarErramienta ] ", EIGlobal.NivelLog.DEBUG);
		FileInputStream archivo = null;
		ServletOutputStream ouputStream = null;
		try{
			archivo = new java.io.FileInputStream(Global.DIRECTORIO_REMOTO_WEB_ARCHIVO_CIFRADO + "/" + Global.NOM_HERRAMIENTA_CIFRADO);
			int longitud = archivo.available();
			byte[] datos = new byte[longitud];
			archivo.read(datos);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=" + Global.NOM_HERRAMIENTA_CIFRADO);
			ouputStream = response.getOutputStream();
			ouputStream.write(datos);
			ouputStream.flush();
		}catch (IOException e) {
			EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + "]   | " + "Error al bajar sowtware --->  " + e.getCause(), EIGlobal.NivelLog.DEBUG);
			request.setAttribute(ERROR, "Por el momento no es posible descargar el Software de cifrado, por favor p�ngase en contacto con nosotros v�a Superl�nea, acuda a una sucursal Santander o int�ntelo m�s tarde.");
			direccionamiento(request, response);
		}finally{
			if(archivo != null){
				archivo.close();
			}
			if(ouputStream != null){
				ouputStream.close();
			}
		}
	}
	
	/**
	 * Metodo para bajar el software de cifrado
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Excepci�n que se propaga al contenedor en caso de un error inesperado
	 * @throws IOException :  Excepci�n de Entrada/Salida que se regresa al contenedor web en caso de falla.
	 */
	protected void bajarCertificado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ bajarCertificado ] ", EIGlobal.NivelLog.DEBUG);
		CertificadoDigitalBO certificadoBO = new CertificadoDigitalBOImpl();
		byte[] key;
		key = null;
		int referencia = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
		String nombreArchivo = request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO) == null ? "" : request.getSession().getAttribute(CADENA_NOMBRE_ARCHIVO).toString();

		try {
			key = certificadoBO.consultaCertificadoCanal(Global.CANAL_CIFRADO);
		} catch (CertificateEncodingException e) {
			key = null;
			EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ bajarCertificado ] no existe valor ", EIGlobal.NivelLog.DEBUG);
		}catch(BusinessException e){
			EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ bajarCertificado ] EXCEPCION:: " + e.getMessage(), EIGlobal.NivelLog.DEBUG );
			key = null;
		}
		
		if(key == null) {
			request.setAttribute(ERROR, "Error al descargar el certificado, intentarlo mas tarde.");
			bitacorizaProceso(request, referencia, "DCBC","0001",nombreArchivo,"R");
			direccionamiento(request, response);
		}else{
			ServletOutputStream ouputStream = null;
			try{
				bitacorizaProceso(request, referencia, "DCBC","0000",nombreArchivo,"A");

				response.setContentType("application/x-x509-user-cert");
				response.setHeader("Content-Disposition","attachment;filename="+Global.NOM_CERTIFICADO_CANAL+".crt");
				ouputStream = response.getOutputStream();
				ouputStream.write(key);
				ouputStream.flush();
			}catch(IOException e){
				EIGlobal.mensajePorTrace(CLASE + this.getClass().getName() + CONCATENA +  " method [ bajarCertificado ] EXCEPCION:: " + e.getMessage(), EIGlobal.NivelLog.DEBUG );
			}finally{
				if(ouputStream != null){
					ouputStream.close();
				}
			}
		}
			
		
	}
	
	
	/**
	 * Metodo para direccioar
	 * @param request : HttpServletRequest
	 * @param response : HttpServletResponse
	 * @throws ServletException : Exception
	 * @throws IOException : Exception
	 */
	private void direccionamiento (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute(SESSION);
		request.setAttribute(EXISTE, request.getAttribute(EXISTE));
		
		request.setAttribute("opcion", request.getParameter("opcion"));
		request.setAttribute("newMenu", session.getFuncionesDeMenu());
		request.setAttribute("MenuPrincipal", session.getStrMenu());
		request.setAttribute("Encabezado", CreaEncabezado("Certificado digital","Administraci&oacute;n y Control &gt; Certificado digital", "CERTDIG01", request));
		request.setAttribute("RUTA_IMAGENES", Global.RUTA_IMAGENES);
		//request.getRequestDispatcher("/jsp/certificadoDigital.jsp").forward(request, response);
		request.getRequestDispatcher(Global.RUTA_CERTIFICADOJSP).forward(request, response);
		
	}
	
	/**
	 * @param request : request
	 * @param referencia : referencia clave de bitacora
	 * @param idFlujo : id del flujo
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void bitacorizaProceso(HttpServletRequest request,  int referencia, String idFlujo,String codigoError,String nombreArchivo,String status) throws ServletException, IOException {
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra CertificadoDigitalServlet: --> ::: bitacorizaProceso" , EIGlobal.NivelLog.INFO);
		String idFlujoBitacora = idFlujo;
		String numBit = idFlujoBitacora.concat("01");
		HttpSession sess = request.getSession();
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra CertificadoDigitalServlet: --> ::: bitacorizaProceso --> id bitacora ::: " + idFlujoBitacora , EIGlobal.NivelLog.INFO);
		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			String idFlujoEntrada = (String)request.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO);
			BaseResource session = (BaseResource) sess.getAttribute(SESSION);
			BitaHelper bh = new BitaHelperImpl(request, session, sess);
			Date hoy = new Date();
				bh.incrementaFolioFlujo(idFlujoBitacora);
			BitaTransacBean bt = new BitaTransacBean();
			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdFlujo(idFlujoBitacora);
			bt.setNumBit(numBit);
			if (session.getContractNumber() != null) {
				bt.setContrato(session.getContractNumber().trim());
			}	
			bt.setReferencia(referencia);
			String codError = idFlujoBitacora + codigoError;
			bt.setIdErr(codError);
			bt.setIdToken(session.getToken().getSerialNumber());				
			bt.setFechaAplicacion(hoy);
			bt.setEstatus(status);
			bt.setNombreArchivo(nombreArchivo);
			
			request.getSession().setAttribute(BitaConstants.SESS_ID_FLUJO, idFlujoEntrada);
			

			try {
				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("CertificadoDigitalFilter - Error al insertar en eweb_tran_bitacora intentos fallidos" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			
			try {
 				BitaTCTBean beanTCT = new BitaTCTBean ();

 				beanTCT = bh.llenarBeanTCT(beanTCT);

				beanTCT.setReferencia(referencia);
				beanTCT.setTipoOperacion(idFlujo);
				beanTCT.setCodError(idFlujo.concat(codigoError));
				beanTCT.setUsuario(session.getUserID8());
	
			    BitaHandler.getInstance().insertBitaTCT(beanTCT);

			} catch (SQLException e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace("CertificadoDigitalFilter - Error al insertar en bitacora transacional " + new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
			}
		}
		
		EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ::: FIN ---> bitacorizaProceso" , EIGlobal.NivelLog.INFO);
	}
	
}
