
package mx.altec.enlace.servlets;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoCfmg;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.Documento;
import mx.altec.enlace.bo.EI_Importar;
import mx.altec.enlace.bo.Proveedor;
import mx.altec.enlace.bo.servicioConfirming;
import mx.altec.enlace.dao.CalendarNomina;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.EmailSender;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.*;


public class coPagos extends BaseServlet
{
	/**
	 * Campo de tipo operacion.
	 */
    private transient String tipoOperacion = "";

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		try{
			defaultAction(req, res);
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Problema al ejecutar servlet coPagos", EIGlobal.NivelLog.WARN);
			EIGlobal.mensajePorTrace("coPagos::doPost:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.WARN);
		}
	}

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		try{
			defaultAction(req, res);
		}catch(Exception e){
			StackTraceElement[] lineaError;
			lineaError = e.getStackTrace();
			EIGlobal.mensajePorTrace("Problema al ejecutar servlet coPagos", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("coPagos::doPost:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ e.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.WARN);
		}
	}

	/**
	 * @param req Request
	 * @param res Response
	 * @throws ServletException ex
	 * @throws IOException ex
	 */
	public void defaultAction(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
		{
		  if(!SesionValida(req, res)) return;
		  HttpSession sess = req.getSession();
		  BaseResource session = (BaseResource) sess.getAttribute("session");
		  /*
		   * jgarcia
		   * Stefanini
		   * Se agrego este set para que cargue las cuentas con link y no las deje de manera informativa.
		   */
		  session.setModuloConsultar(IEnlace.MCargo_TI_pes_dolar);
/******************************************Inicia validacion OTP**************************************/
        String valida = getFormParameter(req, "valida" );
        if(validaPeticion( req,  res,session,sess,valida)){
             EIGlobal.mensajePorTrace("\n\n\n EN validacion OTP \n\n\n", EIGlobal.NivelLog.INFO);
        }
		/******************************************Termina validacion OTP**************************************/
		else
		{
		// FACULTADES
		sess.setAttribute("fac_CCALTMANPAG", new Boolean(verificaFacultad("CCALTMANPAG",req)));
		sess.setAttribute("fac_CCALTPGINTRNAL", new Boolean(verificaFacultad("CCALTPGINTRNAL",req)));
		sess.setAttribute("fac_CCIMPARCHPA", new Boolean(verificaFacultad("CCIMPARCHPA",req)));
		sess.setAttribute("fac_CCIMPPGINTRNAL", new Boolean(verificaFacultad("CCIMPPGINTRNAL",req)));
        EIGlobal.mensajePorTrace("Se verifica la facultad CCALTMANPAG->"
	              + ((Boolean)sess.getAttribute("fac_CCALTMANPAG")).toString(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("Se verifica la facultad CCALTPGINTRNAL->"
			              + ((Boolean)sess.getAttribute("fac_CCALTPGINTRNAL")).toString(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("Se verifica la facultad CCIMPARCHPA->"
			              + ((Boolean)sess.getAttribute("fac_CCIMPARCHPA")).toString(), EIGlobal.NivelLog.ERROR);
		EIGlobal.mensajePorTrace("Se verifica la facultad CCIMPPGINTRNAL->"
	              + ((Boolean)sess.getAttribute("fac_CCIMPPGINTRNAL")).toString(), EIGlobal.NivelLog.ERROR);
		if( !verificaFacultad("CCALTMANPAG",req) &&
            !verificaFacultad("CCALTPGINTRNAL",req) &&
            !verificaFacultad("CCIMPARCHPA",req) &&
            !verificaFacultad("CCIMPPGINTRNAL",req)) {

			   EIGlobal.mensajePorTrace("No se tiene la facultad CCALTMANPAG" , EIGlobal.NivelLog.WARN);

			   String laDireccion = "document.location='SinFacultades'";
			   req.setAttribute( "plantillaElegida", laDireccion);
			   req.getRequestDispatcher ( IEnlace.CARGATEMPLATE_TMPL ).forward(req,res);

			   return;
			}
		EIGlobal.mensajePorTrace("Se tiene la facultad CCALTMANPAG" , EIGlobal.NivelLog.WARN);

		String validaChall = req.getAttribute( "validaChallenge" ) != null
		? req.getAttribute( "validaChallenge" ).toString() : "";

		// Parche para evitar la doble llamada del Internet Explorer 6.0
		GregorianCalendar ahora = new GregorianCalendar();
		Long fechaAnt = (Long)sess.getAttribute("coPagosFecha");
		long fechaAhora =
			ahora.get(Calendar.YEAR) * 10000000000L +
			ahora.get(Calendar.MONTH) * 100000000L +
			ahora.get(Calendar.DATE) * 1000000L +
			ahora.get(Calendar.HOUR) * 10000L +
			ahora.get(Calendar.MINUTE) * 100L +
			ahora.get(Calendar.SECOND);

		if(fechaAnt != null)
			{
			 long numUltFecha = fechaAnt.longValue();
			}

		req.setAttribute("MenuPrincipal", session.getStrMenu());
		req.setAttribute("newMenu", session.getFuncionesDeMenu());
		req.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100h",req));

		CalendarNomina calendar = new CalendarNomina();
		calendar.CargarDias();
		req.setAttribute("diasInhabiles",calendar.armaDiasInhabilesJS());

		servicioConfirming arch_combos = new servicioConfirming();
		arch_combos.setUsuario	(session.getUserID8());
		arch_combos.setContrato	(session.getContractNumber());
		arch_combos.setPerfil	(session.getUserProfile());

		//jgarcia - Se guarda variable arch_combos en session para su posterior uso
		sess.setAttribute("arch_combos",arch_combos);
		req.setAttribute ("arch_combos", arch_combos);

		Hashtable clavesProv = new Hashtable();
		Hashtable codigosProv = new Hashtable();
		Hashtable nombresProv = new Hashtable();
		String divisaProv = "";

		int a, b;
		String ArchivoResp = null;

		try {
			/*
			 * jgarcia
			 * Stefanini
			 * Obtencion de la cuenta cargo para enviarla en la trama
			 * Se agrega a sesion la variable de cuenta cargo para filtrar los proveedores
			 */
			String cuentaCargo = getFormParameter(req, "Cuentas");
			String cuentaCliente = getFormParameter(req, "textCuentas");
			if(null != cuentaCargo && !"".equals(cuentaCargo)){
					sess.setAttribute("cuentaCargo",cuentaCargo);
			}else if(null == cuentaCargo || "".equals(cuentaCargo)){
				cuentaCargo = (String)sess.getAttribute("cuentaCargo");
				if(null == cuentaCargo)
					cuentaCargo = "";
			}

			if(null != cuentaCliente && !"".equals(cuentaCliente)){
				sess.setAttribute("cuentaCliente",cuentaCliente);
			}
			ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","",cuentaCargo);
			/*
			 * Fin del cambio
			 */
		} catch(Exception e) {
			debug("Error: " + e + " (no pudo generarse el archivo que contiene la info de los combos)", EIGlobal.NivelLog.INFO);
			despliegaMensaje("Servicio no disponible por el momento, intente m&aacute;s tarde",
				"Pagos","Servicios &gt; Confirming &gt; Pagos", req, res);
			return;
		}

		Vector codProv = null;
		Vector cveProv = null;
		Vector nomProv = null;
//		jgarcia - Stefanini - Creacin del vector para divisas
		Vector divProv = null;
		Proveedor arch_p = new Proveedor();

		codProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 3);
		cveProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 2);
		nomProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 1);
//		 jgarcia - Stefanini - Lectura de divisa
		divProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 5);

		if(null != divProv && divProv.size() > 0)
			divisaProv = (String)divProv.get(0);

		sess.setAttribute("divisaProv", divisaProv);

		if(null != divisaProv && "USD".equalsIgnoreCase(divisaProv)){
			sess.setAttribute("descDivisa", "DÓLAR AMERICANO");
		}else{
			sess.setAttribute("descDivisa", "MONEDA NACIONAL");
		}

		debug("coPagos - Divisa = >" +divisaProv+ "<", EIGlobal.NivelLog.DEBUG);
		if(codProv != null)
			{
			b = cveProv.size();
			for(a=0;a<b;a++)
				{
				  clavesProv.put((String)codProv.get(a),(String)cveProv.get(a));
				  codigosProv.put((String)cveProv.get(a),(String)codProv.get(a));
				  nombresProv.put((String)cveProv.get(a),(String)nomProv.get(a));
				}
			}

		codProv = null;
		cveProv = null;
		codProv = null;
		divProv = null;

		System.gc();
		boolean banBit=true;
		String parAccion = getFormParameter(req,"accion");
		debug("PARACCION = >" +parAccion+ "<", EIGlobal.NivelLog.DEBUG);

		if(parAccion == null)
			{
			sess.setAttribute("objArchivo", new ArchivoCfmg());
			sess.setAttribute("cfrm_folio","");
			parAccion = "0";
			banBit=false;
			}

		//
		ArchivoCfmg objArchivo = (ArchivoCfmg)sess.getAttribute("objArchivo");
		String nombreArchivo = getFormParameter(req,"archivo_actual");
		if(nombreArchivo == null) nombreArchivo = "";

		//
		boolean pasa;
		Documento doc;
		Vector errores;

		//
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		//Se verifica Facultad Exportar
		String facultadExportar = "false";
        if(verificaFacultad("CCEXPORARCH",req))
        	facultadExportar="true";
		req.setAttribute("facultadExportar",facultadExportar);
		req.setAttribute("urlExportar",("/Download/" + session.getUserID8() + "_coMtoExp.doc"));

		if(parAccion!=null){
			req.setAttribute("accion", parAccion);
		}
    	if("1".equals(validaChall)) {
    		parAccion = "6";
    	}

		if(getFormParameter(req,"numSelec")!=null){
			req.setAttribute("numSelec", getFormParameter(req,"numSelec"));
		}
		switch(Integer.parseInt(parAccion))
			{

			case 0: // --- inicio

					//TODO: BIT CU 4341, El cliente entra al flujo
					/*
		    		 * VSWF ARR -I
		    		 */
				//ABANDA **2009/06/03  --->Limpiar cuentaCargo
				String[] datosCuenta = obtenCuentaEje(session.getContractNumber());
				sess.setAttribute("cuentaCargo",datosCuenta[0].trim());
				sess.setAttribute("cuentaCliente",datosCuenta[0].trim() + "  " + datosCuenta[1].trim());

				if (Global.USAR_BITACORAS.trim().equals("ON")){
					if(!banBit){
					try{

					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					if (getFormParameter(req,BitaConstants.FLUJO)!=null){
					       bh.incrementaFolioFlujo(getFormParameter(req,BitaConstants.FLUJO));
					  }else{
					   bh.incrementaFolioFlujo((String)req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
					  }
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_PAGO_ENTRA);
					bt.setContrato(session.getContractNumber());

					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coPagos", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace("coPagos::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
												+ e.getMessage()
					               				+ "<DATOS GENERALES>"
									 			+ "Linea encontrada->" + lineaError[0]
									 			, EIGlobal.NivelLog.WARN);
					}catch(Exception e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("Error al iniciar bitacorizar servlet coPagos", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace("coPagos::defaultAction:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
												+ e.getMessage()
					               				+ "<DATOS GENERALES>"
									 			+ "Linea encontrada->" + lineaError[0]
									 			, EIGlobal.NivelLog.WARN);
					}
					}
				}
		    		/*
		    		 * VSWF ARR -F
		    		 */
				sess.setAttribute("cfrm_folio_hab","habilitado");
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;
			case 1: // --- alta
				if (!verificaFacultad( "CCALTMANPAG",req) &&
			        !verificaFacultad("CCALTPGINTRNAL",req))
					{
					req.setAttribute("mensaje", "\"Ud no tiene facultades para realizar el alta manual de pagos.\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
					}
				//inicia Modif PVA 08/03/2003
				req.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100Bh",req));
				//termina
				sess.setAttribute("cfrm_folio",getFormParameter(req,"importeTrans"));

				sess.setAttribute("cfrm_folio_hab","habilitado");

				evalTemplate("/jsp/coAltaPagos.jsp",req,res);
				break;
			case 2: // --- modificacion
				if (!verificaFacultad( "CCMODLOCPAG",req)) // modificacin de pagos
				{
					req.setAttribute("mensaje", "\"Ud no tiene facultades para modificar pagos\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
				}

				doc=objArchivo.getDoc(Integer.parseInt(getFormParameter(req,"numSelec")));
				req.setAttribute("documento",doc);
				//inicia Modif PVA 08/03/2003
				req.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100Ch",req));
				//termina
				sess.setAttribute("cfrm_folio",getFormParameter(req,"importeTrans"));
				sess.setAttribute("cfrm_folio_hab","habilitado");
				evalTemplate("/jsp/coAltaPagos.jsp",req,res);
				break;
			case 3: // --- regreso de alta
				doc = obtenDocumentoDeRequest(req, clavesProv, nombresProv);
				if((doc.gettipoCuenta().trim().equals("1") || doc.gettipoCuenta().trim().equals("2"))
						&& !verificaFacultad("CCALTMANPAG",req)){
					req.setAttribute("mensaje", "\"Ud. no tiene facultades para realizar el alta manual de pagos nacionales.\",3");
					evalTemplate("/jsp/coAltaPagos.jsp",req,res);
					break;
				}else if (doc.gettipoCuenta().trim().equals("5")
						&& !verificaFacultad("CCALTPGINTRNAL",req)){
					req.setAttribute("mensaje", "\"Ud. no tiene facultades para realizar el alta manual de pagos internacionales.\",3");
					evalTemplate("/jsp/coAltaPagos.jsp",req,res);
					break;
				}else{
					objArchivo.agregaDoc(doc);
					//inicia Modif PVA 08/03/2003
					req.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100Bh",req));
					//termina
					evalTemplate("/jsp/coAltaPagos.jsp",req,res);
					break;
				}
			case 4: // --- regreso de modificacion
				doc = obtenDocumentoDeRequest(req,clavesProv, nombresProv);
				objArchivo.setDoc(Integer.parseInt(getFormParameter(req,"numSelec")),doc);
				req.setAttribute("mensaje","\"Los datos fueron modificados\",1");
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;
			case 5: // --- borrar pago
				if (!verificaFacultad( "CCBAJLOCPAG",req))
					{
					req.setAttribute("mensaje", "\"Ud no tiene facultades para borrar pagos\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
					}
				objArchivo.eliminaDoc(Integer.parseInt(getFormParameter(req,"numSelec")));
				req.setAttribute("mensaje","\"El registro fue borrado\",1");
				sess.setAttribute("cfrm_folio",getFormParameter(req,"importeTrans"));
				sess.setAttribute("cfrm_folio_hab","habilitado");
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;
			case 6: // --- enviar archivo

                    //interrumpe la transaccion para invocar la validacin
				//--------------challenge--------------------------
				String validaChallenge = req.getAttribute("challngeExito") != null
					? req.getAttribute("challngeExito").toString() : "";

				EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);

				if(Global.VALIDA_RSA && !"SUCCESS".equals(validaChallenge)) {
					EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
					req.setAttribute("validaChallenge", "1");
					guardaParametrosEnSession(req);
					validacionesRSA(req, res);
					return;
				}

				//Fin validacion rsa para challenge
				//-------------------------------------------------
					boolean valBitacora = true;
				    if(  valida==null){
						EIGlobal.mensajePorTrace("\n\n\n Se verifica si el usuario tiene token y si la transaccin estß parametrizada para solicitar la validacin con el OTP \n\n\n", EIGlobal.NivelLog.DEBUG);

						boolean solVal=ValidaOTP.solicitaValidacion(
									session.getContractNumber(),IEnlace.CONFIRMING_PAGO_PROVEEDORES);

						if( session.getValidarToken() &&
								session.getFacultad(session.FAC_VAL_OTP) &&
								session.getToken().getStatus() == 1 && solVal){
							//VSWF-HGG -- Bandera para guardar el token en la bitacora
							req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
							EIGlobal.mensajePorTrace("\n\n\n El usuario tiene Token. \nSolicit la validacin. \nSe guardan los parametros en sesion \n\n\n", EIGlobal.NivelLog.DEBUG);
							guardaParametrosEnSession(req);

							System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°coPagos.java");
							req.getSession().removeAttribute("mensajeSession");
							ValidaOTP.mensajeOTP(req, "PagoProv");

							ValidaOTP.validaOTP(req,res,IEnlace.VALIDA_OTP_CONFIRM);
						}else{
					    	ValidaOTP.guardaRegistroBitacora(req,"Token deshabilitado.");
					    	valida="1";
					    	valBitacora = false;
					    }
					}
					//retoma el flujo
				    if( valida!=null && valida.equals("1")){

				    	if (valBitacora)
						{
							try{
									EIGlobal.mensajePorTrace( "*************************Entro código bitacorización--->", EIGlobal.NivelLog.INFO);

									HttpSession sessionBit = req.getSession(false);
									String token = "0";
									int numeroReferencia  = obten_referencia_operacion(Short.parseShort(IEnlace.SUCURSAL_OPERANTE));
									String claveOperacion = BitaConstants.CTK_PAGO_PROVEEDORES;
									String concepto =  BitaConstants.CTK_CONCEPTO_PAGO_PROVEEDORES;
									if (req.getParameter("token") != null && !req.getParameter("token").equals(""));
									{token = req.getParameter("token");}
									 double importeDouble = 0;
									 String cuenta = "0";
									 String importeTotal = "";

									importeTotal = sessionBit.getAttribute("importeCfmg").toString().trim();
									importeDouble = Double.valueOf(importeTotal).doubleValue();

									cuenta = (String)sessionBit.getAttribute("cuentaCargo");


									EIGlobal.mensajePorTrace( "-----------numeroReferencia--a->"+ numeroReferencia, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace( "-----------concepto--b->"+ concepto, EIGlobal.NivelLog.DEBUG);
				        			EIGlobal.mensajePorTrace( "-----------cuenta--b->"+ cuenta, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace( "-----------importe--b->"+ importeDouble, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace( "-----------claveOperacion--b->"+ claveOperacion, EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace( "-----------token--b->"+ token, EIGlobal.NivelLog.DEBUG);

									String coderror_bitConfirToken = ValidaOTP.bitacora_Confirma_OTP(req,numeroReferencia,cuenta,importeDouble,claveOperacion,"0",token,concepto,0);

							} catch(Exception e) {
								EIGlobal.mensajePorTrace("ENTRA PROBLEMA BITACORIZACION DE CONFIRMACION CON TOKEN -->" + e.getMessage(),EIGlobal.NivelLog.WARN);

							}
						}



						if (!verificaFacultad( "CCENVARCHPA",req)){
							req.setAttribute("mensaje", "\"Ud no tiene facultades para enviar archivos\",3");
							evalTemplate("/jsp/coPagos.jsp",req,res);
							break;
						}
						//SLF IM19625
						String nombretmp;
						nombretmp = objArchivo.getNombre();

						EIGlobal.mensajePorTrace("nombre : >" +nombretmp+ "<\n>" +((ArchivoCfmg)sess.getAttribute("objArchivo")).getTotalRegistros()+
										   "< total = >" +objArchivo.getTotalRegistros()+ "<\n\n", EIGlobal.NivelLog.DEBUG);

						if(objArchivo.getTotalRegistros()<1){
							req.setAttribute("mensaje", "\"El archivo esta vacio o no se ha generado correctamente, revise e intente nuevamente.\",3");
							evalTemplate("/jsp/coPagos.jsp",req,res);
							break;
						}

						if(objArchivo.getTotalRegistros()>30){
							objArchivo = new ArchivoCfmg();

							EIGlobal.mensajePorTrace("Archivo Original : >" +IEnlace.LOCAL_TMP_DIR+ "/" +session.getUserID8()+ "_coMtoProv_bckup && " +nombretmp+ "<",EIGlobal.NivelLog.DEBUG);

							errores = objArchivo.leeImportacion(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() + "_coMtoProv_bckup",codigosProv, nombresProv, req);

							objArchivo.setNombre(nombretmp);
						}
						/*
						 * jgarcia
						 * Stefanini
						 * Obtencin de la cuenta cargo para enviarla en el archivo que contiene los registros
						 */
						String cuentaCargoEnvio = (String)sess.getAttribute("cuentaCargo");
						if(cuentaCargoEnvio == null)
							cuentaCargoEnvio = "";

						if(validaCuentaEje(session.getContractNumber(), cuentaCargoEnvio )){
							objArchivo.archivoEnvio(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_coMtoProv", cuentaCargoEnvio);
							objArchivo = (ArchivoCfmg)sess.getAttribute("objArchivo");
							pasa = enviaArchivo(req);
							if(pasa)
							{
								sess.setAttribute("cfrm_folio",getFormParameter(req,"importeTrans"));
								sess.setAttribute("cfrm_folio_hab","inhabilitado");
								objArchivo.setEstado(objArchivo.ENVIADO);
								objArchivo.archivoExportacion(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_coMtoExp.doc", cuentaCargoEnvio);
								if(!archRemoto.copiaLocalARemoto (session.getUserID8() + "_coMtoExp.doc", "WEB" ))
									{debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);}
							}
							//LGN IM19625

							objArchivo.ReporteCorto();
			                                ValidaOTP.guardaBitacora((java.util.List)sess.getAttribute("bitacora"),tipoOperacion);
			                evalTemplate("/jsp/coPagos.jsp",req,res);
						}else{
							req.setAttribute("MenuPrincipal", session.getStrMenu());
							req.setAttribute("newMenu", session.getFuncionesDeMenu());
							req.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100h",req));
							req.setAttribute("Mensaje", "Error H701: Cuenta inv&aacute;lida.");
							evalTemplate( "/jsp/EI_Mensaje2.jsp", req, res);
						}

					}
				break;
			case 7: // --- recuperar archivo1
				if(!verificaFacultad( "CCACTUARCH",req))
				  {
					req.setAttribute("mensaje", "\"Ud no tiene facultades para recuperar archivos\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
				  }

				pasa = recuperaArchivo(req,clavesProv,nombresProv);
				objArchivo = (ArchivoCfmg)req.getSession().getAttribute("objArchivo");
				debug("Objeto en sesion : "+ objArchivo, EIGlobal.NivelLog.DEBUG);
				debug("Total de Registros Despues de recupera " + objArchivo.getTotalRegistros() , EIGlobal.NivelLog.INFO);

				if(pasa)
				{
					debug("Entrando ", EIGlobal.NivelLog.DEBUG);
					sess.setAttribute("cfrm_folio","");
					sess.setAttribute("cfrm_folio_hab","inhabilitado");
					objArchivo.setEstado(objArchivo.RECUPERADO);
					debug("Estado recuperado", EIGlobal.NivelLog.DEBUG);
					String cuentaCargoEnvio = (String)sess.getAttribute("cuentaCargo");
					if(cuentaCargoEnvio == null)
						cuentaCargoEnvio = "";
					objArchivo.archivoExportacion(Global.DIRECTORIO_LOCAL + "/" + session.getUserID8() + "_coMtoExp.doc", cuentaCargoEnvio);
					debug("Archivo de exportacion", EIGlobal.NivelLog.DEBUG);

					if(!archRemoto.copiaLocalARemoto ( session.getUserID8() + "_coMtoExp.doc", "WEB" ) )
					{
						debug("No se pudo enviar el archivo Exportado", EIGlobal.NivelLog.INFO);
					}

					debug("Total de Registros al final " + objArchivo.getTotalRegistros() , EIGlobal.NivelLog.INFO);

					if(objArchivo.getTotalRegistros() == 0)
						req.setAttribute("mensaje","\"El archivo recuperado est&aacute; vac&iacute;o o no existe\",1");
					else
						req.setAttribute("mensaje","\"El archivo fue recuperado con &eacute;xito\",1");
				}
				//LGN IM19625
				objArchivo.ReporteCorto();
				sess.setAttribute("objArchivo",objArchivo);
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;
			case 8: // --- borrar archivo
				if (!verificaFacultad( "CCELIMIARCH",req))
					{
					req.setAttribute("mensaje", "\"Ud no tiene facultades para eliminar archivos\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
					}
				objArchivo = new ArchivoCfmg();
				objArchivo.setNombre("");
				sess.setAttribute("objArchivo",objArchivo);
				sess.setAttribute("cfrm_folio","");
				sess.setAttribute("cfrm_folio_hab","habilitado");
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;
			case 9: // --- importar archivo
				 if(!verificaFacultad( "CCIMPARCHPA",req) &&
						 !verificaFacultad( "CCIMPPGINTRNAL",req))
				   {
				      req.setAttribute("mensaje", "\"Ud. no tiene facultades para realizar el alta de pagos por importación de archivos.\",3");
					  evalTemplate("/jsp/coPagos.jsp",req,res);
					  break;
				   }

// --------------------------------------------------
				EI_Importar arcImp = new EI_Importar();

				String nombreArchivoImp = "";
				boolean errorEnElArchivo = false;

				//Stefanini*19/05/2009*JAL
//				Se descomento para que realize la importacion en IAS
//				nombreArchivoImp = arcImp.importaArchivo(req,res);
//				Stefanini*19/05/2009*JAL
//				Se comenta porque solo se utiliza para WAS
				nombreArchivoImp=getFormParameter(req,"fileNameUpload");

				nombreArchivo = IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() + "_coMtoProv";

				try{
					EIGlobal.mensajePorTrace("coPagos - cambiaNombreArchivo:Si el archivo existe, se renombra.", EIGlobal.NivelLog.DEBUG);
					File archivo = new File( nombreArchivoImp );
					if (archivo.exists()){
						EIGlobal.mensajePorTrace("coPagos - cambiaNombreArchivo:Se renombra de: "+ nombreArchivoImp + " a "+nombreArchivo, EIGlobal.NivelLog.DEBUG);
						archivo.renameTo(new File(nombreArchivo));
					}else{
					   EIGlobal.mensajePorTrace("coPagos - cambiaNombreArchivo:El archivo no existe.", EIGlobal.NivelLog.DEBUG);
					   errorEnElArchivo=true;
					   // throw file not found exception
					}
				  }catch(Exception e){
					  EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
					  errorEnElArchivo=true;
				  }
// --------------------------------------------------
				//En este punto, ya debe existir un archivo sin 'zipeo' ni nada, para poder leer
					if(nombreArchivoImp.indexOf("ZIPERROR")>=0 || errorEnElArchivo)
					 {
						   despliegaPaginaError( " Imposible Importar el archivo, no es un archivo zip o est&aacute; dañado",
						   "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100Dh", req, res );
						   EIGlobal.mensajePorTrace("***coPagos.class - Primeralinea & Imposible Importar el archivo, no es un archivo zip o esta dañado." , EIGlobal.NivelLog.INFO);
					 }
					else {
						objArchivo = new ArchivoCfmg();
//SLF IM19625
                 try
				{
                 copy(nombreArchivo, IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8()+ "_coMtoProv_bckup");
				 EIGlobal.mensajePorTrace(">>Archivo copiado: " + IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8()+ "_coMtoProv_bckup", EIGlobal.NivelLog.INFO);
				 }
				catch(Exception e)
				{
					EIGlobal.mensajePorTrace("Error al copiar el archivo" + e.getMessage(), EIGlobal.NivelLog.ERROR);
				}


						errores = objArchivo.leeImportacion(IEnlace.LOCAL_TMP_DIR + "/" + session.getUserID8() + "_coMtoProv",codigosProv, nombresProv, req);

						if(cuentaErrores(errores)>0)
						  {
						   ///Mensaje de errores
						    EIGlobal.mensajePorTrace("SALE CON ERRORES", EIGlobal.NivelLog.INFO);
							String nombreArchError = session.getUserID8() + "_coMtoProv" + "_err.doc";
							req.setAttribute("mensajeImportacion",formateaErrores(errores,nombreArchError));
						    objArchivo = new ArchivoCfmg();
 						    sess.setAttribute("objArchivo",objArchivo);
						  }
						else {
								objArchivo.setNombre("IMPORTADO");
								objArchivo.setEstado(objArchivo.IMPORTADO);
								sess.setAttribute("cfrm_folio","");
								sess.setAttribute("cfrm_folio_hab","habilitado");
    							objArchivo.ReporteCorto();
		 						sess.setAttribute("objArchivo",objArchivo);
							 }

						 evalTemplate("/jsp/coPagos.jsp",req,res);
					 }
					break;
			case 10: // --- imprimir reporte
				if (!verificaFacultad("CCIMPRREPOR",req))
					{
					req.setAttribute("mensaje", "\"Ud no tiene facultades para imprimir reportes\",3");
					evalTemplate("/jsp/coPagos.jsp",req,res);
					break;
					}
				sess.setAttribute("cfrm_folio",getFormParameter(req,"importeTrans"));
				imprimeReporte(req,res);
				break;
			case 11: // --- crear archivo
				objArchivo.setNombre(nombreArchivo);
				req.setAttribute("mensaje","\"Archivo creado exitosamente\",1");
				evalTemplate("/jsp/coPagos.jsp",req,res);
				break;

			}
		 EIGlobal.mensajePorTrace("TERMINA IMPORTACION\n\n",EIGlobal.NivelLog.INFO);
		 sess.setAttribute("objArchivo",objArchivo);
  }
 }

	// ---
	/**
	 * @param req Request
	 * @return boolean
	 */
	private boolean enviaArchivo(HttpServletRequest req)
		{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");

		boolean resultado = true;
		ArchivoCfmg objArchivo = (ArchivoCfmg)sess.getAttribute("objArchivo");
		String trama;
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();

		StringBuffer tramaTux=new StringBuffer("");

		try
			{
			if(!archRemoto.copiaLocalARemoto(session.getUserID8() + "_coMtoProv"))
				{
				debug("No se pudo realizar la copia local a remoto", EIGlobal.NivelLog.INFO);
				req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
				return false;
				}
			else
				//SLF IM19625
				EIGlobal.mensajePorTrace("Archivo enviado satisfactoriamente ->" + session.getUserID8() + "_coMtoProv",EIGlobal.NivelLog.INFO);

                        tipoOperacion = "CFRP";

			tramaTux.append("2EWEB|");
			tramaTux.append(session.getUserID8());
			tramaTux.append("|" + tipoOperacion + "|");
			tramaTux.append(session.getContractNumber());
			tramaTux.append("|" );
			tramaTux.append(session.getUserID8());
			tramaTux.append("|");
			tramaTux.append(session.getUserProfile());
			tramaTux.append("|");
			tramaTux.append(Global.DIRECTORIO_LOCAL);
			tramaTux.append("/");
			tramaTux.append(session.getUserID8());
			tramaTux.append("_coMtoProv@");
			tramaTux.append(objArchivo.getNumLineas());
			tramaTux.append("@");
			tramaTux.append(objArchivo.getNombre());
			tramaTux.append("@");
			tramaTux.append(objArchivo.getSumaParcial());
			tramaTux.append("@");
			tramaTux.append(objArchivo.getImporteTotal());
			tramaTux.append("@M|");
			tramaTux.append(objArchivo.getImporteTotal());
			tramaTux.append("|");
			tramaTux.append(getFormParameter(req,"importeTrans"));
			tramaTux.append("| | |");

		 //MSD Q05-0029909 inicio lineas agregadas
		 String IP = " ";
		 String fechaHr = "";												//variables locales al m‰todo
		 String errorBit="";
		 String CodErrorOper="";
		 IP = req.getRemoteAddr();											//ObtenerIP
		 java.util.Date fechaHrAct = new java.util.Date();
		 SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
		 fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignacin de fecha y hora
		 //MSD Q05-0029909 fin lineas agregada

			debug(fechaHr+"trama de envio: " + tramaTux.toString(), EIGlobal.NivelLog.INFO);
			ht = tuxedo.web_red(tramaTux.toString());
			trama = (String)ht.get("BUFFER");
			CodErrorOper=ht.get("COD_ERROR")+"";
			errorBit=trama;

			debug("tuxedo regresa : " + trama, EIGlobal.NivelLog.INFO);
			if (trama.startsWith("ALYM")) {
				debug("No paso la validacion de Limites y Montos", EIGlobal.NivelLog.INFO);
				req.setAttribute("mensaje","\"" + trama.substring(16, trama.length()) + "\",3");
				return false;
			}
			trama = trama.substring(trama.lastIndexOf('/')+1);
			if(!archRemoto.copia(trama))
				{
				debug("No se pudo realizar la copia remota", EIGlobal.NivelLog.INFO);
				req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
				return false;
				}
			BufferedReader entrada = new BufferedReader(new FileReader(Global.DIRECTORIO_LOCAL + "/" + trama));
			trama = entrada.readLine();
			entrada.close();
			debug("Primera linea del archivo que regreso tuxedo: " + trama, EIGlobal.NivelLog.INFO);
			if(trama.indexOf(";") == -1)
				{
				req.setAttribute("mensaje","\"" + trama.substring(16) + "\",3");
				resultado = false;
				}
			else
				{
				trama = trama.substring(trama.indexOf(";")+1,trama.lastIndexOf(";"));
				GregorianCalendar hoy = new GregorianCalendar();
				objArchivo.setNumTransmision(trama);

				StringBuffer setAttr=new StringBuffer("");
				setAttr.append("\"Favor de recuperar el archivo con el n&uacute;mero de secuencia ");
				setAttr.append(trama);
				setAttr.append(" a partir de las ");
				setAttr.append(((hoy.get(Calendar.HOUR_OF_DAY)<10)?"0":"") );
				setAttr.append(	hoy.get(Calendar.HOUR_OF_DAY) );
				setAttr.append(":");
				setAttr.append(((hoy.get(Calendar.MINUTE)<10)?"0":"") );
				setAttr.append(hoy.get(Calendar.MINUTE) );
				setAttr.append(	" hrs\",1");

				req.setAttribute("mensaje",setAttr.toString());
				sess.setAttribute("objArchivo",objArchivo);
				sess.setAttribute("coPagosNumEnvio",trama);




				EIGlobal.mensajePorTrace("NumeroContrato ->" + session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RazonSocial ->" + session.getNombreContrato(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Referencia ->" + objArchivo.getNumTransmision(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("CuentaCargo ->" + sess.getAttribute("cuentaCargo"), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Importe ->" + objArchivo.getImporteTotal(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("No de transmision ->" + objArchivo.getNumTransmision(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("Reg. importados ->" + objArchivo.getTotalRegistros(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Estado ->" + objArchivo.getEstado(), EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("Folio de transmision ->" + objArchivo.getFolio_transmision(), EIGlobal.NivelLog.INFO);

				Documento docAux;

				String tipoPago = "";
				String expRegM = "M{".concat(String.valueOf(objArchivo.getTotalRegistros())).concat("}");
				String expRegO = "O{".concat(String.valueOf(objArchivo.getTotalRegistros())).concat("}");
				String expRegI = "I{".concat(String.valueOf(objArchivo.getTotalRegistros())).concat("}");

				StringBuffer tipoPagoBuffer = new StringBuffer("");

				Pattern patron = null;
				Matcher encaja = null;

				for(int a=0; a< objArchivo.getTotalRegistros() && a<30; a++){
					docAux = objArchivo.getDoc(a);
					if("1".equalsIgnoreCase(docAux.gettipoCuenta())){
						//MISMO BANCO"
						tipoPagoBuffer.append("M");
					}else {
						if("2".equalsIgnoreCase(docAux.gettipoCuenta())){
							//OTRO BANCO
							tipoPagoBuffer.append("O");
						}else {
							if("5".equalsIgnoreCase(docAux.gettipoCuenta())){
								//INTERNACIONAL
								tipoPagoBuffer.append("I");
							}
						}
					}
				}

				patron = Pattern.compile(expRegM);
				encaja = patron.matcher(tipoPagoBuffer.toString());
				if (encaja.find()) {
					tipoPago = "MISMO BANCO";
				}

				patron = Pattern.compile(expRegO);
				encaja = patron.matcher(tipoPagoBuffer.toString());
				if (encaja.find()) {
					tipoPago = "INTERBANCARIO";
				}

				patron = Pattern.compile(expRegI);
				encaja = patron.matcher(tipoPagoBuffer.toString());
				if (encaja.find()) {
					tipoPago = "INTERNACIONAL";
				}
				try {
					EIGlobal.mensajePorTrace("Inicio de Notificacion por Correo", EIGlobal.NivelLog.DEBUG);
					EmailSender emailSender = new EmailSender();

					if(emailSender.enviaNotificacion(CodErrorOper)){
						EmailDetails beanEmailDetails = new EmailDetails();
						beanEmailDetails.setNumeroContrato(session.getContractNumber());
						beanEmailDetails.setRazonSocial(session.getNombreContrato());
						beanEmailDetails.setNumRef(objArchivo.getNumTransmision());
						beanEmailDetails.setNumCuentaCargo(sess.getAttribute("cuentaCargo").toString());
						beanEmailDetails.setImpTotal(ValidaOTP.formatoNumero(objArchivo.getImporteTotal()));
						beanEmailDetails.setNumTransmision(objArchivo.getNumTransmision());
						beanEmailDetails.setNumRegImportados(Integer.parseInt(""+objArchivo.getTotalRegistros()));
						beanEmailDetails.setTipoPagoProveedor(tipoPago);
						beanEmailDetails.setEstatusActual(CodErrorOper);
						emailSender.sendNotificacion(req,IEnlace.PAGO_PROVEEDORES,beanEmailDetails);//MAAM 10/Julio/2010
					}
				} catch (Exception e) {
					EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
				}


//				TODO: BIT CU 4341, Envio de archivo
				/*
	    		 * VSWF ARR -I
	    		 */
				if (Global.USAR_BITACORAS.trim().equals("ON")){
					try{

					BitaHelper bh = new BitaHelperImpl(req, session,sess);
					BitaTransacBean bt = new BitaTransacBean();
					bt = (BitaTransacBean)bh.llenarBean(bt);
					bt.setNumBit(BitaConstants.ES_CONF_PAGO_PROV_PAGO_CONEX_DB);
					bt.setContrato(session.getContractNumber());
					if(objArchivo.getImporteTotal()!=null && !objArchivo.getImporteTotal().equals(""))
						bt.setImporte(Double.parseDouble(objArchivo.getImporteTotal()));
					bt.setNombreArchivo(objArchivo.getNombre());
					bt.setServTransTux(tipoOperacion);
					if(errorBit!=null){
						 if(errorBit.substring(0,2).equals("OK")){
							   bt.setIdErr("CFRP0000");
						 }else if(errorBit.length()>8){
							  bt.setIdErr(errorBit.substring(0,8));
						 }else{
							  bt.setIdErr(errorBit.trim());
						 }
						}
					if (req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
						&& ((String) req.getSession().getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
								.equals(BitaConstants.VALIDA))
						bt.setIdToken(session.getToken().getSerialNumber());

					req.getSession().removeAttribute(BitaConstants.SESS_BAND_TOKEN);
					BitaHandler.getInstance().insertBitaTransac(bt);
					}catch (SQLException e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("Error al realizar el envio del archivo", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace("coPagos::enviaArchivo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
												+ e.getMessage()
					               				+ "<DATOS GENERALES>"
									 			+ "Linea encontrada->" + lineaError[0]
									 			, EIGlobal.NivelLog.ERROR);
					}catch(Exception e){
						StackTraceElement[] lineaError;
						lineaError = e.getStackTrace();
						EIGlobal.mensajePorTrace("Error al realizar el envio del archivo", EIGlobal.NivelLog.ERROR);
						EIGlobal.mensajePorTrace("coPagos::enviaArchivo:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
												+ e.getMessage()
					               				+ "<DATOS GENERALES>"
									 			+ "Linea encontrada->" + lineaError[0]
									 			, EIGlobal.NivelLog.ERROR);
					}
				}
	    		/*
	    		 * VSWF ARR -F
	    		 */
				}
			} catch(Exception e){
			     debug("Excepcion: " + e, EIGlobal.NivelLog.INFO);
			     req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
			     resultado = false;
			    }

		   return resultado;
		}
	// ---
	
	/**
	 * @param req Request
	 * @param claves HashTable
	 * @param nombres HashTable
	 * @return boolean
	 */
	private boolean recuperaArchivo(HttpServletRequest req, Hashtable claves, Hashtable nombres)
		{
		HttpSession sess = req.getSession();
		BaseResource session = (BaseResource) sess.getAttribute("session");
		boolean resultado = true;
		ArchivoCfmg objArchivo = new ArchivoCfmg();
		objArchivo.setNombre("");

		String trama;
		StringBuffer tramaTux=new StringBuffer("");
		Hashtable ht;
		ServicioTux tuxedo = new ServicioTux();
		ArchivoRemoto archRemoto = new ArchivoRemoto();


		// Inicio - Se a±ade logica para llenar informacin de proveedores independientemente de la divisa
		servicioConfirming arch_combos = (servicioConfirming)sess.getAttribute ("arch_combos");
		if(null == arch_combos)
			arch_combos = new servicioConfirming();
		Vector codProv = null;
		Vector cveProv = null;
		Vector nomProv = null;
		String ArchivoResp = null;
		Proveedor arch_p = new Proveedor();
		Hashtable clavesProv = new Hashtable();
		Hashtable codigosProv = new Hashtable();
		Hashtable nombresProv = new Hashtable();
		try {
			ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");
			codProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 3);
			cveProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 2);
			nomProv = arch_p.coMtoProv_Leer(ArchivoResp,"9", 1);
			if(codProv != null){
				for(int a = 0; a < cveProv.size(); a++){
					  clavesProv.put((String)codProv.get(a),(String)cveProv.get(a));
					  codigosProv.put((String)cveProv.get(a),(String)codProv.get(a));
					  nombresProv.put((String)cveProv.get(a),(String)nomProv.get(a));
				}
			}
		} catch(Exception e) {
			clavesProv = claves;
			nombresProv = nombres;
		}
			codProv = null;
			cveProv = null;
			nomProv = null;
		// Fin - Se a±ade logica para llenar informacin de proveedores independientemente de la divisa


		try {
                        tipoOperacion = "CFEP";
			tramaTux.append("2EWEB|");
			tramaTux.append(session.getUserID8());
			tramaTux.append("|" + tipoOperacion + "|");
			tramaTux.append(session.getContractNumber());
			tramaTux.append("|");
			tramaTux.append(session.getUserID8());
			tramaTux.append("|");
			tramaTux.append(session.getUserProfile());
			tramaTux.append("|");
			tramaTux.append(getFormParameter(req,"numProv"));
			tramaTux.append("@|");

			//MSD Q05-0029909 inicio lineas agregadas
			String IP = " ";
			String fechaHr = "";												//variables locales al m‰todo
			IP = req.getRemoteAddr();											//ObtenerIP
			java.util.Date fechaHrAct = new java.util.Date();
			SimpleDateFormat fdate = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss:S");//formato fecha
			fechaHr ="["+fdate.format(fechaHrAct)+"] IP "+ IP+" ";			//asignacin de fecha y hora
			//MSD Q05-0029909 fin lineas agregada

			debug(fechaHr+"trama de envio: " + tramaTux.toString(), EIGlobal.NivelLog.INFO);
			ht = tuxedo.web_red(tramaTux.toString());
			trama = (String)ht.get("BUFFER");
			debug("tuxedo regresa: " + trama, EIGlobal.NivelLog.INFO);
			trama = trama.substring(trama.lastIndexOf('/')+1); //,trama.length()-1

			if(!archRemoto.copia(trama))
			{
				req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
				sess.setAttribute("objArchivo",new ArchivoCfmg());
				return false;
			}

			BufferedReader verifica = new BufferedReader(new FileReader(Global.DIRECTORIO_LOCAL + "/" + trama));
			String linea = verifica.readLine();
			debug("Primera linea del archivo de regreso: " + linea, EIGlobal.NivelLog.INFO);

			if(!linea.startsWith("OK"))
			{
				req.setAttribute("mensaje","\"" + linea.substring(16) + "\",3");
				sess.setAttribute("objArchivo",new ArchivoCfmg());
				return false;
			} else
				objArchivo.leeRecuperacion(Global.DIRECTORIO_LOCAL + "/" + trama, clavesProv, nombresProv, req);

			debug("Se recupera archivo de " + Global.DIRECTORIO_LOCAL + "/" + trama, EIGlobal.NivelLog.INFO);

			sess.setAttribute("objArchivo",objArchivo);
			debug("Total de Registros En Recupera " + objArchivo.getTotalRegistros() , EIGlobal.NivelLog.INFO);
		} catch(Exception e) {
			req.setAttribute("mensaje","\"Servicio no disponible por el momento, intente m&aacute;s tarde\",3");
			sess.setAttribute("objArchivo",new ArchivoCfmg());
			resultado = false;
		}
		return resultado;
	}

	// ---
	/**
	 * @param req Request
	 * @param clavesProv HashTable
	 * @param nombresProv HashTable
	 * @return Documento
	 */
	private Documento obtenDocumentoDeRequest(HttpServletRequest req, Hashtable clavesProv, Hashtable nombresProv)
		{
			Documento doc = new Documento();

			//Se agrego por HGV para el proyecto de Ley de Transparencia II
			String referencia = "";
			String proveedor = "";
			String tipoCta = "";

			try{
			/*
			 * jgarcia
			 * Stefanini
			 * Obtencion de la clave de la divisa para que con base en ella obtener su descripcion
			 */
			HttpSession sess = req.getSession();
			String divisaCliente = (String)sess.getAttribute("divisaProv");
			if(null == divisaCliente)
				divisaCliente = "";

			if ((getFormParameter(req,"Referencia").equals(null)) || (getFormParameter(req,"Referencia").equals("")) )
			{
				referencia = "0";
			} else {
				referencia = getFormParameter(req,"Referencia");
			}

			proveedor = getFormParameter(req,"Proveedor");
			tipoCta   = proveedor.substring(proveedor.indexOf("|")+1);
			proveedor = proveedor.substring(0,proveedor.indexOf("|"));

			debug("Referencia = >" +referencia+ "<", EIGlobal.NivelLog.DEBUG);
			debug("Tipo Cta   = >" +tipoCta+ "<", EIGlobal.NivelLog.DEBUG);
			debug("Proveedor  = >" +proveedor+ "<", EIGlobal.NivelLog.DEBUG);


			//Fin HGV

			doc.setproveedor(proveedor);
			doc.setipoCuenta(tipoCta);
			doc.setclaveProv((String)clavesProv.get(proveedor));
			doc.setnombreProv((String)nombresProv.get(doc.getclaveProv()));
			doc.settipo(Integer.parseInt(getFormParameter(req,"TipoDocumento")));
			doc.setnumdocto(getFormParameter(req,"NumeroDocumento"));
			if(doc.gettipo()>2)
				{
				 doc.setfactura_asociada(getFormParameter(req,"Asociado"));
				 doc.setnaturaleza(getFormParameter(req,"Naturaleza").charAt(0));
				}
			doc.setimporte_neto(getFormParameter(req,"ImporteDocumento"));
			doc.setfch_emis(getFormParameter(req,"FechaEmision"));
			doc.setfch_venc(getFormParameter(req,"FechaVencimiento"));
			//Se agrego por HGV para el proyecto de Ley de Transparencia II
			doc.setreferencia("" + Integer.parseInt(referencia));
			doc.setconcepto(getFormParameter(req,"Concepto").toUpperCase());
			/*
			 * jgarcia - Stefanini - Asignacion de la divisa al pago
			 */
			doc.setDivisa(divisaCliente);
			doc.setaplicacion("1");

			}catch(Exception e){
				EIGlobal.mensajePorTrace("coPagos::obtenDocumentoDeRequest:: Error->" + e.getMessage(), EIGlobal.NivelLog.ERROR);
			}
			//Fin HGV
			return doc;
		}

	// ---
	/**
	 * @param errores Vector
	 * @return int
	 */
	private int cuentaErrores(Vector errores) {
		int num = 0;
		int a;
		for(a = 0; a < errores.size(); a++) {
			num += ((Vector) errores.get(a)).size();
		}
		return num;
	}

	// ---
	/**
	 * @param vector Vector
	 * @param archivo String
	 * @return String
	 */
	private String formateaErrores(Vector vector,String archivo)
		{
		Vector vectorLinea;
		StringBuffer htmlPie= new StringBuffer("");
		StringBuffer htmlCuerpo = new StringBuffer("");
		StringBuffer htmlTotal = new StringBuffer("");
		StringBuffer htmlCabecera = new StringBuffer("");
		boolean hayarchivo = true;

		int a, b, total;

		FileWriter escribe = null;
		try{
			escribe = new FileWriter(IEnlace.LOCAL_TMP_DIR + "/" +archivo);
		}catch(Exception e) {
			hayarchivo= false;
		}
		BufferedWriter out = new BufferedWriter(escribe);

		total = cuentaErrores(vector);
		htmlTotal.append( "<br><br><br><b>TOTAL: " );
		htmlTotal.append(( (total > 1)?(total + " errores"):"1 error") );
		htmlTotal.append("</b>");

		htmlCabecera.append( "<html><head><title>Estatus</title><link rel='stylesheet' href='/EnlaceMig/");
		htmlCabecera.append("consultas.css' type='text/css'></head><body bgcolor='white'><form><table border=0");
		htmlCabecera.append(" width=420 class='textabdatcla' align=center><tr><th class='tittabdat'>informaci&");
		htmlCabecera.append("oacute;n del Archivo Importado</th></tr><tr><td class='tabmovtex1' align=center><");
		htmlCabecera.append("table border=0 align=center><tr><td class='tabmovtex1' align=center width=><img s");
		htmlCabecera.append("rc='/gifs/EnlaceMig/gic25060.gif'></td><td class='tabmovtex1' align='center'><br>");
		htmlCabecera.append("No se llevo a cabo la importaci&oacute;n ya que el archivo seleccionado tiene los");
		htmlCabecera.append(" siguientes errores.<br><br></td></tr></table></td></tr><tr><td class='tabmovtex1'>");

		htmlPie.append("<br></td></tr></table><table border=0 align=center><tr><td align=center><br><a ");
		htmlPie.append("href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></");
		htmlPie.append("a></td></tr></table></form></body></html>");

/*MSD Q05-20565
  Se corrigi el ciclo for puesto que manejaba los errores incorrectamente
*/
        String linea;
        int totalErrores = 0;
		try{
	    for(a=0;a<vector.size();a++)
			{
			vectorLinea = (Vector)vector.get(a);
			if(vectorLinea.size()>0)
			  {
				//pantalla
				if(totalErrores < 100) {
					htmlCuerpo.append("<br><b>L&iacute;nea ");
					htmlCuerpo.append( (a+1) );
					htmlCuerpo.append("</b>");
				}

				//archivo
				linea="linea" + (a+1) + " - \n";
				EIGlobal.mensajePorTrace("Linea->>" + linea, EIGlobal.NivelLog.DEBUG);
				out.write(linea,0,linea.length());
			  }
			 for(b=0;b<vectorLinea.size();b++)
			  {

				//pantalla
				if(totalErrores < 100) {
					htmlCuerpo.append("<br><DD><LI>");
					htmlCuerpo.append( ((String)vectorLinea.get(b)) );
				}

				//archivo
				linea=(String)vectorLinea.get(b)+"\n";
				out.write(linea,0,linea.length());


				totalErrores++;

			  }
			}
/*MSD Q05-20565*/

		out.flush();
		out.close();
		}catch(Exception e)
			{EIGlobal.mensajePorTrace("Error al escribir->" + e.getMessage(), EIGlobal.NivelLog.ERROR);}

/*MSD Q05-20565*/
		envia(new File(archivo));
/*MSD Q05-20565*/


        if(totalErrores>100) /*MSD Q05-20565*/
			{
					htmlTotal.append("<br>Solo se presentan los primeros 100 errores ");
			  		htmlTotal.append("<br><b>La lista completa de errores esta disponible ");
					htmlTotal.append("<a href=\\\"/Download/"+archivo+"\\\">aqui</a>");
			}

		StringBuffer Res=new StringBuffer("");

		Res.append(htmlCabecera.toString());
		Res.append(htmlCuerpo.toString());
		Res.append(htmlTotal.toString());
		Res.append(htmlPie.toString());

		return Res.toString();
		}

	// --
	private void debug(String mensaje, EIGlobal.NivelLog nivel)
		{
		  EIGlobal.mensajePorTrace("<debug coPagos.java> " + mensaje,nivel);
		}

////////////Funcion para imprimir Reporte//////////
// ---
 /**
  * @param request Request
  * @param response Response
  * @throws ServletException e
  * @throws IOException ex
  */
 private boolean imprimeReporte(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException
  {
     EIGlobal.mensajePorTrace("coPagos  inicio imprimeReporte: " , EIGlobal.NivelLog.INFO);

     HttpSession sess = request.getSession();
     BaseResource session = (BaseResource) sess.getAttribute("session");

     boolean resultado = true;
     String tituloSalida="";
     StringBuffer strTabla;
     String sColor="";
     Documento docregis = new Documento();
     String nombreprov="";
     String cuentaCargo = (String)sess.getAttribute("cuentaCargo");

     //Inicia Cdigo para obtener los nombres de los proveedores
     Proveedor arch_prov = new Proveedor();
     Hashtable provs = new Hashtable();
     servicioConfirming  arch_combos= new servicioConfirming();
	 arch_combos.setUsuario(session.getUserID8());
	 arch_combos.setContrato(session.getContractNumber());
	 arch_combos.setPerfil(session.getUserProfile());

     String ArchivoResp =
     arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","","");

     Vector ls_prov = null;
     Vector cv_trans = null;

     try
	 {
	   if(ArchivoResp != null)
       {
            ls_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 4);
            cv_trans = arch_prov.coMtoProv_Leer(ArchivoResp,"9", 3);
       }
       if(ls_prov != null)
	   {
		 for(int a=0;a<ls_prov.size();a++)
          provs.put((String)cv_trans.get(a),(String)ls_prov.get(a));
         EIGlobal.mensajePorTrace("coPagos  numero de proveedores: "+ls_prov.size(), EIGlobal.NivelLog.INFO);

       }
     }
	 catch(Exception e)
	 {
        ls_prov=null;
		cv_trans=null;
		provs=null;
         EIGlobal.mensajePorTrace("coPagos  no se obtuvo el catalogo de provedores: ", EIGlobal.NivelLog.INFO);

	 }
	 /////////////////fin Cdigo para obtener los nombres de proveedores
     ArchivoCfmg objArchivo = (ArchivoCfmg)
     sess.getAttribute("objArchivo");
     long totalregistros= objArchivo.getTotalRegistros();
	 Documento doc;

     EIGlobal.mensajePorTrace("coPagos  totalregistros: "+totalregistros, EIGlobal.NivelLog.INFO);

     if (totalregistros<=Global.MAX_REGISTROS)
     {
		strTabla=new StringBuffer("");
        strTabla.append("<table width=620 border=0 cellspacing=2 cellpadding=3 class=tabfonbla>");
        strTabla.append("<tr><td colspan=8 align=center class=tabmovtex>Reporte De Pagos</td></tr>");
		strTabla.append("<tr><td class=tittabdat align=center>Clave de Provedor</td>");
		strTabla.append("<td class=tittabdat align=center>Nombre o razon social</td>");
		strTabla.append("<td class=tittabdat align=center>Tipo de Documento</td>");
		strTabla.append("<td class=tittabdat align=center>Numero de Docto.</td>");
		strTabla.append("<td class=tittabdat align=center>Importe de Docto.</td>");
		strTabla.append("<td class=tittabdat align=center>Factura asociada</td>");
		strTabla.append("<td class=tittabdat align=center>Importe a pagar</td>");
		//HGCV Ley de Transparencia II
		strTabla.append("<td class=tittabdat align=center>Referencia</td></tr>");
		strTabla.append("<tr><td class=tittabdat align=center>Concepto</td>");
		//HGCV Ley de Transparencia II
		strTabla.append("<td class=tittabdat align=center>Fecha de Emision</td>");
		strTabla.append("<td class=tittabdat align=center>Fecha de Vencimiento</td>");
		strTabla.append("<td class=tittabdat align=center>Estatus</td>");
		strTabla.append("<td class=tittabdat align=center>Observaciones</td>");
		//jgarcia 14/Oct/2009 Se agregan campos Cuenta Cargo, Divisa, Forma de Pago
		strTabla.append("<td class=tittabdat align=center>Cuenta Cargo</td>");
		strTabla.append("<td class=tittabdat align=center>Divisa</td>");
		strTabla.append("<td class=tittabdat align=center>Forma de Pago</td></tr>");

		StringBuffer sbFormaPago = new StringBuffer("");
        for (int i=0;i<totalregistros;i++)
        {
		   doc = objArchivo.getDoc(i);
           if ((i%2)==0)
              sColor="textabdatobs";
           else
              sColor="textabdatcla";
           docregis=objArchivo.getDoc(i);

           //jgarcia 14/Oct/2009 Texto para la forma de pago
           if("1".equalsIgnoreCase(docregis.gettipoCuenta())){
				sbFormaPago.append("Mismo Banco");
			}else if("2".equalsIgnoreCase(docregis.gettipoCuenta())){
				sbFormaPago.append("Otros Bancos");
			}else if("5".equalsIgnoreCase(docregis.gettipoCuenta())){
				sbFormaPago.append("Internacional");
			}

		  strTabla.append("<tr><td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>");
		  strTabla.append(doc.getclaveProv());
		  strTabla.append("&nbsp;</td>");

          try
		  {
			  nombreprov=(String)provs.get(docregis.getproveedor());
          }
		  catch(Exception e)
		  {
              nombreprov="";
          }
		  if(nombreprov!=null)
			{
             strTabla.append("<td class='");
			 strTabla.append(sColor);
			 strTabla.append("'>");
			 strTabla.append(doc.getnombreProv());
			 strTabla.append("&nbsp;</td>");
			}
          else
			{
             strTabla.append("<td class='");
			 strTabla.append(sColor);
			 strTabla.append("'>&nbsp;</td>");
			}
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>");
		  strTabla.append(doc.jspTipo());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=right>");
		  strTabla.append(doc.jspNumdocto());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=right>");
		  strTabla.append(doc.jspImporte_neto());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=right>");
		  strTabla.append(doc.jspFactura_asociada());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=right>");
		  strTabla.append(doc.jspImporte());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=center>");
		  strTabla.append(doc.getreferencia());
		  strTabla.append("&nbsp;</td></tr>");
          strTabla.append("<tr><td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=right>");
		  strTabla.append(doc.getconcepto());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("' align=center>");
		  strTabla.append(doc.jspFch_emis());
		  strTabla.append("</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'align=center>");
		   strTabla.append(doc.jspFch_venc());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>");
		  strTabla.append(doc.jspCve_status());
		  strTabla.append("&nbsp;</td>");
          strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>" );
		  strTabla.append(doc.jspStatus());
		  strTabla.append("&nbsp;</td>");
		  //jgarcia 14/Oct/2009 - Se agregan campos Cuenta Cargo, Divisa, Forma de Pago
		  strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>" );
		  strTabla.append(cuentaCargo);
		  strTabla.append("&nbsp;</td>");

		  strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>" );
		  strTabla.append(doc.getDivisa());
		  strTabla.append("&nbsp;</td>");

		  strTabla.append("<td class='");
		  strTabla.append(sColor);
		  strTabla.append("'>" );
		  strTabla.append(sbFormaPago.toString());
		  strTabla.append("&nbsp;</td></tr>");

		  sbFormaPago.delete(0, sbFormaPago.length());
       }
       strTabla.append("</table>");
       if (objArchivo.getEstado()==2) //recuperado
       {
         tituloSalida="<table><tr><td class=tabmovtex>Nombre del Archivo:Recuperado"+"</td></tr><tr><td class=tabmovtex>Numero de Transmisi&oacute;n:"+objArchivo.getNumTransmision() +"</td></tr></table>";
         request.setAttribute("ContenidoTitulos", tituloSalida);
         request.setAttribute("r_numtran", objArchivo.getNumTransmision());
         request.setAttribute("ContenidoArchivo",  strTabla.toString());
       }
       else
       {
         tituloSalida="<table><tr><td class=tabmovtex>Nombre del Archivo: "+ objArchivo.getNombre() +"</td></tr><tr><td class=tabmovtex>Numero de Transmisi&oacute;n: No se ha transmitido</td></tr></table>";
         request.setAttribute("ContenidoTitulos", tituloSalida);
         request.setAttribute("ContenidoArchivo", strTabla.toString());
       }
	   //inicia Modif PVA 08/03/2003
	   request.setAttribute("Encabezado", CreaEncabezado( "Pagos","Servicios &gt; Confirming &gt; Pagos","s28100Dh",request));
       //termina
       evalTemplate("/jsp/reporte_pagos.jsp", request, response );
   }
   else
   {
       request.setAttribute("mensaje_js","cuadroDialogo (\"El reporte contiene mas de "+Global.MAX_REGISTROS+" registros, utilice la opcion exportar\")");
       evalTemplate(IEnlace.CONTROL_PAGOS, request, response );
    }
    EIGlobal.mensajePorTrace("coPagos  fin imprimeReporte: " , EIGlobal.NivelLog.INFO);

     return resultado;
  }


	/*************************************************************************************/
	/**************************************************************** despliega Mensaje  */
	/*************************************************************************************/
	public void despliegaMensaje(String mensaje,String param1, String param2,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		{

			// parche de Ramn Tejada
			HttpSession sess = request.getSession();
			BaseResource session = (BaseResource) sess.getAttribute("session");
			EIGlobal Global = new EIGlobal(session , getServletContext() , this );

			String contrato_ = session.getContractNumber();

			if(contrato_==null) contrato_ ="";

			request.setAttribute( "FechaHoy",Global.fechaHoy("dt, dd de mt de aaaa"));
			request.setAttribute( "Error", mensaje );
			request.setAttribute( "URL", ((getFormParameter(request,"Accion") == null)?"javascript:history.go(-1);":"TIDispersion?Accion=&") );

			request.setAttribute( "MenuPrincipal", session.getStrMenu() );
			request.setAttribute( "newMenu", session.getFuncionesDeMenu());
			request.setAttribute( "Encabezado", CreaEncabezado( param1, param2, "", request));

			request.setAttribute( "NumContrato", contrato_ );
			request.setAttribute( "NomContrato", session.getNombreContrato() );
			request.setAttribute( "NumUsuario", session.getUserID8() );
			request.setAttribute( "NomUsuario", session.getNombreUsuario() );

			evalTemplate( "/jsp/EI_Mensaje.jsp" , request, response );
		}


//facultades
	/**
	 * @param request Request
	 * @return String
	 */
public String facultades(HttpServletRequest request) {
	    HttpSession sess = request.getSession();
        BaseResource session = (BaseResource) sess.getAttribute("session");
		StringBuffer cadFacultades = new StringBuffer("");
		if ( verificaFacultad( "CCACTUARCH",request) )
			{
				cadFacultades.append("CCACTUARCH, ");
				session.setFacultad( "CCACTUARCH" );
			}
		if ( verificaFacultad( "CCALTMANPAG",request ) )
			{
				cadFacultades.append("CCALTMANPAG, ");
				session.setFacultad( "CCALTMANPAG" );
			}
		if ( verificaFacultad( "CCBAJLOCPAG",request ) )
			{
				cadFacultades.append("CCBAJLOCPAG, ");
				session.setFacultad( "CCBAJLOCPAG" );
			}
		if ( verificaFacultad( "CCELIMIARCH",request ) )
			{
				cadFacultades.append("CCELIMIARCH, ");
				session.setFacultad( "CCELIMIARCH" );
		    }
		if ( verificaFacultad( "CCENVARCHPA",request ) )
			{
				cadFacultades.append("CCENVARCHPA, ");
				session.setFacultad( "CCENVARCHPA" );
			}
		if ( verificaFacultad( "CCEXPORARCH",request ) )
			{
				cadFacultades.append("CCEXPORARCH, ");
				session.setFacultad( "CCEXPORARCH" );
			}
		if ( verificaFacultad( "CCIMPARCHPA",request ) )
			{
				cadFacultades.append("CCIMPARCHPA, ");
				session.setFacultad( "CCIMPARCHPA" );
			}
		if ( verificaFacultad( "CCIMPRREPOR",request ) )
			{
				cadFacultades.append("CCIMPRREPOR, ");
				session.setFacultad( "CCIMPRREPOR" );
			}
		if ( verificaFacultad( "CCMODLOCPAG",request ) )
   	       {
				cadFacultades.append("CCMODLOCPAG, ");
				session.setFacultad( "CCMODLOCPAG" );
		   }
		return cadFacultades.toString();
	}



/////////////////// SLF IM19625

   /**
    * @param source_name Origen
	* @param dest_name Destino
	* @throws Exception e
	*/
   public static void copy(String source_name, String dest_name)   throws Exception
   {
	   String cmd="cp "+source_name+" "+dest_name;
       EIGlobal.mensajePorTrace("Comando a ejecutar:" + cmd, EIGlobal.NivelLog.INFO);
	   execCmd(cmd);
   }

	 /**
	  * @param cmd String
	  * @throws Exception e
	  */
     static public void execCmd(String cmd) throws Exception
		 {
          Process pr = Runtime.getRuntime().exec(cmd.toString());
          pr.waitFor();
          /**
           * <VC autor="GGB" fecha="18/09/2007"
           * 		descripcion="Cerrar explicitamente los Stream que tiene el objeto Process">
           */
          try {
        	  pr.getErrorStream().close();
          } catch (Exception e) {
        	  EIGlobal.mensajePorTrace("coPagos.execCmd -> Mensaje: "
        			  + e.getMessage(), EIGlobal.NivelLog.INFO);
          }
          try {
			pr.getInputStream().close();
          } catch (Exception e) {
        	  EIGlobal.mensajePorTrace("coPagos.execCmd -> Mensaje: "
        			  + e.getMessage(), EIGlobal.NivelLog.INFO);
          }
          try {
			pr.getOutputStream().close();
          } catch (Exception e) {
        	  EIGlobal.mensajePorTrace("coPagos.execCmd -> Mensaje: "
        			  + e.getMessage(), EIGlobal.NivelLog.INFO);
          }
          /** </VC> */

         }


/*MSD Q05-20565*/
	/**
	 * @param Archivo File
	 * @return boolean
	 */
    public static boolean envia (File Archivo) {
       try {
 		    EIGlobal.mensajePorTrace("coPagos - dentro del metodo envia", EIGlobal.NivelLog.DEBUG);


		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_LOCAL = " + Global.DIRECTORIO_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.DIRECTORIO_REMOTO_WEB = " + Global.DIRECTORIO_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_LOCAL = " + Global.HOST_LOCAL, EIGlobal.NivelLog.DEBUG);
		    EIGlobal.mensajePorTrace("Global.HOST_REMOTO_WEB = " + Global.HOST_REMOTO_WEB, EIGlobal.NivelLog.DEBUG);
			//Req. Q15591  Se corrige validacion para no copiar, se incluye comparacion de rutas.

		    EIGlobal.mensajePorTrace("Envia. Pasa validacion para generar copia remota", EIGlobal.NivelLog.DEBUG);

		    //IF PROYECTO ATBIA1 (NAS) FASE II

           	boolean Respuestal = true;
            ArchivoRemoto envArch = new ArchivoRemoto();

           	if(!envArch.copiaLocalARemoto(Archivo.getName(),"WEB")){

					EIGlobal.mensajePorTrace("*** coPagos.envia  no se pudo copiar archivo remoto_web:" + Archivo.getName(), EIGlobal.NivelLog.ERROR);
					Respuestal = false;

				}
				else {
				    EIGlobal.mensajePorTrace("*** coPagos.envia archivo remoto copiado exitosamente:" + Archivo.getName(), EIGlobal.NivelLog.DEBUG);

				}

           	if (Respuestal){
			    return true;
			}


        } catch (Exception ex) {
			StackTraceElement[] lineaError;
			lineaError = ex.getStackTrace();
			EIGlobal.mensajePorTrace("Error al realizar el envio del archivo", EIGlobal.NivelLog.ERROR);
			EIGlobal.mensajePorTrace("coPagos::envia:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
									+ ex.getMessage()
		               				+ "<DATOS GENERALES>"
						 			+ "Linea encontrada->" + lineaError[0]
						 			, EIGlobal.NivelLog.ERROR);
        }
        return false;
    }
	/**
	 * @param request Request
	 */
public void guardaParametrosEnSession(HttpServletRequest request)
	{
		EIGlobal.mensajePorTrace("\n\n\n Dentro de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
		request.getSession().setAttribute("plantilla",request.getServletPath());

        HttpSession session = request.getSession(false);
        session.removeAttribute("bitacora");
        if(session != null)
		{
			Map tmp = new HashMap();

            tmp.put("accion",getFormParameter(request,"accion"));
            tmp.put("archivo_actual",getFormParameter(request,"archivo_actual"));
            tmp.put("importeTrans",getFormParameter(request,"importeTrans"));

            session.setAttribute("parametros",tmp);

            tmp = new HashMap();
            Enumeration enumer = request.getAttributeNames();
            while(enumer.hasMoreElements()){
                String name = (String)enumer.nextElement();
                tmp.put(name,request.getAttribute(name));
				EIGlobal.mensajePorTrace("\n\n\n Dentro del while de la enumeracion, sesion dif. de null \n\n\n", EIGlobal.NivelLog.DEBUG);
            }
            session.setAttribute("atributos",tmp);
        }
		EIGlobal.mensajePorTrace("\n\n\n Saliendo de guardaParametrosEnSession \n\n\n", EIGlobal.NivelLog.DEBUG);
    }

	/**
	 * Metodo encargado de regresar la cuenta eje
	 * @param contrato 			Numero de Contrato
	 * @param numCuenta			Numero de Cuenta
	 * @return datosCuenta 		Numero y Descripcin de la Cuenta
	 */
	private boolean validaCuentaEje(String contrato, String numCuenta) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String numCuentaFin = "";

		try {
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select a.num_cuenta as numCuenta "
				  + "From cfrm_clientes a, nucl_relac_ctas b "
				  + "Where a.num_cuenta2 ='" + contrato + "' "
				  + "And b.num_cuenta = a.num_cuenta "
				  + "And rownum = 1";

			EIGlobal.mensajePorTrace ("coPagos::validaCuentaEje:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				numCuentaFin = rs.getString("numCuenta")!=null?rs.getString("numCuenta"):"";
			}

			if(numCuenta.trim().equals(numCuentaFin.trim()))
				return true;

			EIGlobal.mensajePorTrace ("coPagos::validaCuentaEje:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("coPagos::validaCuentaEje:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				//e1.printStackTrace();
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coPagos::validaCuentaEje:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return false;
	}

	/**
	 * Metodo encargado de regresar la cuenta eje
	 * @param contrato 			Numero de Contrato
	 * @return datosCuenta 		Numero y Descripcin de la Cuenta
	 */
	private String[] obtenCuentaEje(String contrato) {
		Connection conn = null;
		Statement sDup = null;
		ResultSet rs = null;
		String query = null;
		String[] datosCuenta = new String[2];

		try {
			datosCuenta[0] = "";
			datosCuenta[1] = "";
			conn= createiASConn (Global.DATASOURCE_ORACLE );
			sDup = conn.createStatement();
			query = "Select a.num_cuenta as numCuenta, "
				  + "b.n_descripcion as descCuenta "
				  + "From cfrm_clientes a, nucl_relac_ctas b "
				  + "Where a.num_cuenta2 ='" + contrato + "' "
				  + "And b.num_cuenta = a.num_cuenta "
				  + "And rownum = 1";

			EIGlobal.mensajePorTrace ("coPagos::obtenCuentaEje:: -> Query:" + query, EIGlobal.NivelLog.INFO);
			rs = sDup.executeQuery(query);

			if(rs.next()){
				datosCuenta[0] = rs.getString("numCuenta")!=null?rs.getString("numCuenta"):"";
				datosCuenta[1] = rs.getString("descCuenta")!=null?rs.getString("descCuenta"):"";
			}

			EIGlobal.mensajePorTrace ("coPagos::obtenCuentaEje:: -> Finalizando proceso..", EIGlobal.NivelLog.INFO);
		}catch (SQLException e) {
			EIGlobal.mensajePorTrace ("coPagos::obtenCuentaEje:: -> Error->" + e.getMessage(), EIGlobal.NivelLog.INFO);
		}finally{
			try{
			rs.close();
			sDup.close();
			conn.close();
			}catch(SQLException e1){
				StackTraceElement[] lineaError;
				lineaError = e1.getStackTrace();
				EIGlobal.mensajePorTrace("Error al consultar la cuenta eje de pagos", EIGlobal.NivelLog.ERROR);
				EIGlobal.mensajePorTrace("coPagos::obtenCuentaEje:: SE DETECTO Y CONTROLO EL PROBLEMA SIGUIENTE->"
										+ e1.getMessage()
			               				+ "<DATOS GENERALES>"
							 			+ "Linea encontrada->" + lineaError[0]
							 			, EIGlobal.NivelLog.ERROR);
			}
		}
		return datosCuenta;
	}

}