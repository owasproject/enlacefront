/*
 * GetAMancomunidad.java
 *
 * Created on 5 de marzo de 2003, 11:35 AM
 */

package mx.altec.enlace.servlets;

import java.io.File;
import java.io.FileWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.beans.CAMancomunidadSABean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.DatosMancSA;
import mx.altec.enlace.dao.ConfigMancDAO;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Formateador;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;

//VSWF

/**
 *
 * @author  Horacio Oswaldo Ferro D&iacute;az
 * @version 1.0
 * Servlet para realizar consultas de Mancomunidad usando el servicio de Tuxedo
 */
public class GetAMancomunidadSA extends BaseServlet {

    public static final int NEXT = 1;
    public static final int PREV = 2;

    /** Initializes the servlet.
     */
    public void init (ServletConfig config) throws ServletException {
        super.init (config);

    }

    /** Destroys the servlet.
     */
    public void destroy () {

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        if (SesionValida (request, response)) {
            BaseResource session =
                (BaseResource) request.getSession ().getAttribute ("session");

            request.getSession ().setAttribute ("MenuPrincipal", session.getStrMenu ());
            request.getSession ().setAttribute ("newMenu", session.getFuncionesDeMenu ());
            request.getSession ().setAttribute ("Encabezado", CreaEncabezado ("Consulta de Bit&aacute;cora de Mancomunidad",
                "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n",
                "s25990h", request));
            int opc = 0;
            try {
                opc = Integer.parseInt ((String) request.getParameter ("opSubmit"));
            } catch (NumberFormatException ex) {
            }
            System.out.println ("Opcion Submit: " + opc);
            switch (opc) {
                case 0: {
                    consulta (request, response);
                    break;
                }
                case 1: {
                    pagina (request, response);
                    break;
                }
            }
        }
    }

    /** Consulta de Mancomunidad
     * @param request request del servlet
     * @param response response del servlet
     */
    protected void consulta (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        request.setAttribute ("Fecha", ObtenFecha ());
        BaseResource session = (BaseResource) request.getSession ().getAttribute ("session");
        CAMancomunidadSABean beanManc = new CAMancomunidadSABean();
        ConfigMancDAO configMancDAO = new ConfigMancDAO();
        StringBuffer estatus = new StringBuffer();

		if (request.getParameter("consArch") != null){
			request.getSession().removeAttribute("critBusArch");
		}

		if(request.getSession().getAttribute("critBusArch") == null){

			// Fechas de inicio y fin de consulta
			beanManc.setFecha1(request.getParameter("fecha1") != null ? request.getParameter("fecha1") : "");
			beanManc.setFecha2(request.getParameter("fecha2") != null ? request.getParameter("fecha2") : "");
			request.getSession().setAttribute("FechaIni", beanManc.getFecha1());
			request.getSession().setAttribute("FechaFin", beanManc.getFecha2());

        beanManc.setUsuario(   request.getParameter("usuario")    != null ? request.getParameter("usuario")    : "");
        beanManc.setCuenta(    request.getParameter("cuenta")     != null ? request.getParameter("cuenta")     : "");
        beanManc.setProducto(  request.getParameter("producto")   != null ? request.getParameter("producto")   : "");
        beanManc.setOpConsulta(request.getParameter("opConsulta") != null ? request.getParameter("opConsulta") : "");

        beanManc.setChkArchivo(  request.getParameter("chkArchivo")   != null ? request.getParameter("chkArchivo")   : "");
        beanManc.setChkServicios(request.getParameter("chkServicios") != null ? request.getParameter("chkServicios") : "");

        beanManc.setFolioArchivo( request.getParameter("folioArchivo")  != null ? request.getParameter("folioArchivo")  : "");
        beanManc.setImporte(      request.getParameter("importe")       != null ? request.getParameter("importe")       : "");
        beanManc.setFolioRegistro(request.getParameter("folioRegistro") != null ? request.getParameter("folioRegistro") : "");

        beanManc.setChkAutorizadas(  request.getParameter("chkAutorizadas")   != null ? request.getParameter("chkAutorizadas")   : "");
        beanManc.setChkPendientes(   request.getParameter("chkPendientes")    != null ? request.getParameter("chkPendientes")    : "");
        beanManc.setChkRechazadas(   request.getParameter("chkRechazadas")    != null ? request.getParameter("chkRechazadas")    : "");
        beanManc.setChkEjecutadas(   request.getParameter("chkEjecutadas")    != null ? request.getParameter("chkEjecutadas")    : "");
        beanManc.setChkCanceladas(   request.getParameter("chkCanceladas")    != null ? request.getParameter("chkCanceladas")    : "");
        beanManc.setChkNoEjecutadas( request.getParameter("chkNoEjecutadas")  != null ? request.getParameter("chkNoEjecutadas")  : "");
        beanManc.setChkMancomunada(  request.getParameter("chkMancomunada")   != null ? request.getParameter("chkMancomunada")   : "");
        beanManc.setChkPreAutorizada(request.getParameter("chkPreAutorizada") != null ? request.getParameter("chkPreAutorizada") : "");
        beanManc.setChkVerificada(   request.getParameter("chkVerificada")    != null ? request.getParameter("chkVerificada")    : "");

			request.getSession ().setAttribute ("critBusArch", beanManc);
        }else{
			beanManc = (CAMancomunidadSABean)request.getSession().getAttribute("critBusArch");
		}


        try {
            ArrayList resultados = configMancDAO.getDatosArchivo(beanManc, session.getContractNumber());

            if (resultados.size() == 0) {
            	despliegaPaginaError ("No hay registros para su consulta","Consulta de Bit&aacute;cora de Mancomunidad",
        			      "Administraci&oacute;n y Control &gt; Mancomunidad &gt; Consulta y Autorizaci&oacute;n", request, response);
                return;
            }
            System.out.println("Despues de crear la lista");

            if (Global.USAR_BITACORAS.trim().equals("ON")){
    			BitaHelper bh = new BitaHelperImpl(request, session, request.getSession(false));

    			BitaTransacBean bt = new BitaTransacBean();
    			bt = (BitaTransacBean)bh.llenarBean(bt);
    			bt.setNumBit(BitaConstants.EA_MANCOM_CONS_AUTO_ARCH_CONS_CUENTAS_MANCOM);

    			if (session.getContractNumber() != null) {
    				bt.setContrato(session.getContractNumber().trim());
    				bt.setNombreArchivo(session.getContractNumber() + ".doc");
    			}

    			try {
    				BitaHandler.getInstance().insertBitaTransac(bt);
    			} catch (SQLException e) {
    				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    			} catch (Exception e) {
    				 EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
    			}
                }

            request.getSession().setAttribute("ResultadosMancomunidadSA", resultados);
            request.getSession().setAttribute("IndexMancomunidadSA", new Integer (-50));
            pagina (request, response);
        } catch (Exception ex) {
            despliegaPaginaError ("Error al tratar de obtener la consulta", request, response);
            EIGlobal.mensajePorTrace ("Error en consulta de Mancomunidad Archivos y Servicios: " + ex.getMessage (), EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace(new Formateador().formatea(ex), EIGlobal.NivelLog.INFO);
        }
    }

    /** Metodo para realizar la paginaci&oacute;n de los resultados de la consulta
     * @param request Request del servlet
     * @param response Response del servlet
     */
    private void pagina (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        int index = 0;
        int opc = NEXT;
        int paginacion = 50;
        try {
            index = ((Integer) request.getSession().getAttribute("IndexMancomunidadSA")).intValue();
        } catch (Exception ex) {
        }
        try {
            opc = Integer.parseInt((String) request.getParameter("OpcPag"));
        } catch (Exception ex) {
        }
        switch (opc) {
            case NEXT: {
                if (!(index + 50 > ((ArrayList) request.getSession ().getAttribute (
                "ResultadosMancomunidadSA")).size ()))
                    index += paginacion;
                break;
            }
            case PREV: {
                index -= paginacion;
                if (index < 0) index = 0;
                break;
            }
        }

        File archivo = new File (IEnlace.DOWNLOAD_PATH +
            ((BaseResource) request.getSession().getAttribute("session")).getContractNumber () + ".doc");
        FileWriter writer = new FileWriter (archivo);
        ListIterator listResultados = ((ArrayList) request.getSession()
        		.getAttribute("ResultadosMancomunidadSA")).listIterator (0);

        while (listResultados.hasNext ()) {
            DatosMancSA temp = (DatosMancSA) listResultados.next ();
            writer.write (temp.toString ());
        }
        writer.flush ();
        writer.close ();

        ArchivoRemoto archR = new ArchivoRemoto();
        if (!archR.copiaLocalARemoto(((BaseResource)request.getSession ().getAttribute ("session"))
        		.getContractNumber () + ".doc", "WEB")) {
            EIGlobal.mensajePorTrace( "No se realizo la copia remota", EIGlobal.NivelLog.INFO);
        } else {
            request.getSession ().setAttribute ("ArchivoManc", "/Download/" +
                ((BaseResource) request.getSession ().getAttribute ("session")).getContractNumber () + ".doc");
        }

        listResultados =((ArrayList) request.getSession ().getAttribute("ResultadosMancomunidadSA"))
        				.listIterator (index);

        request.getSession().setAttribute("IndexMancomunidadSA", new Integer (index));
        request.getRequestDispatcher("/jsp/GetAMancomunidadSA.jsp").forward (request, response);
    }

    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        processRequest (request, response);
    }

    /** Returns a short description of the servlet.
     */
    public String getServletInfo () {
        return "Consulta y Autorizacion Mancomunidad Archivos y Servicios";
    }

}