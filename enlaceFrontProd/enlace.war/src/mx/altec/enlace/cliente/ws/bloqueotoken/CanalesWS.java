/**
 * CanalesWS.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf160638.12 v101006191000
 */

package mx.altec.enlace.cliente.ws.bloqueotoken;

public interface CanalesWS extends java.rmi.Remote {
    public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException;
    public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException;
    public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException;
    public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException;
    public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException;
    public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException;
    public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException;
    public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException;
    public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException;
    public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException;
    public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException;
}