/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BeanRespuestaGeneracion.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO para el tranporte de informacion de la respuesta generacion de comprobante con sello digital
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanRespuestaGeneracion", propOrder = {
    "compSelloDigital"
})
public class BeanRespuestaGeneracion
    extends Respuesta
{

    /** La variable comp sello digital. */
    protected String compSelloDigital;

    /**
     * Obtiene el valor de la variable comp sello digital.
     *
     * @return el comp sello digital
     */
    public String getCompSelloDigital() {
        return compSelloDigital;
    }

    /**
     * Coloca el valor de comp sello digital.
     *
     * @param value es el nuevo valor de comp sello digital
     */
    public void setCompSelloDigital(String value) {
        this.compSelloDigital = value;
    }

}
