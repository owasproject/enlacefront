/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * BeanDatosOperMasiva.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO para el transporte de los datos de una operacion masiva.
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanDatosOperMasiva", propOrder = {
    "fecha",
    "idCanal",
    "importe",
    "lineaCaptura",
    "numCuenta",
    "referencia"
})
public class BeanDatosOperMasiva {

    /** La variable fecha de la operacion. */
    protected String fecha;
    
    /** La variable idCanal del identificador del canal. */
    protected int idCanal;
    
    /** La variable importe de la operacion. */
    protected double importe;
    
    /** La variable linea de captura de la operacion. */
    protected String lineaCaptura;
    
    /** La variable numCuenta del numero de cuenta destino de la operacion. */
    protected String numCuenta;
    
    /** La variable referencia de la operacion. */
    protected String referencia;
    
    /**
     * Constructor vacio.
     */
    public BeanDatosOperMasiva() {
    }
    
    /**
     * Construnctor sobrecargado.
     *
     * @param fecha fecha de la operacion
     * @param idCanal identificador del canal
     * @param importe importe de la operacion
     * @param lineaCaptura linea de captura de la operacion
     * @param numCuenta numero de cuenta destino de la operacion
     * @param referencia de la operacion
     */
    public BeanDatosOperMasiva(String fecha, int idCanal, double importe, String lineaCaptura, String numCuenta, String referencia) {
        super();
        this.fecha = fecha;
        this.idCanal = idCanal;
        this.importe = importe;
        this.lineaCaptura = lineaCaptura;
        this.numCuenta = numCuenta;
        this.referencia = referencia;
    }

    /**
     * Obtiene el valor de la variable fecha.
     *
     * @return el fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Coloca el valor de fecha.
     *
     * @param value es el nuevo valor de fecha
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la variable id canal.
     *
     * @return el id canal
     */
    public int getIdCanal() {
        return idCanal;
    }

    /**
     * Coloca el valor de id canal.
     *
     * @param value es el nuevo valor de id canal
     */
    public void setIdCanal(int value) {
        this.idCanal = value;
    }

    /**
     * Obtiene el valor de la variable importe.
     *
     * @return el importe
     */
    public double getImporte() {
        return importe;
    }

    /**
     * Coloca el valor de importe.
     *
     * @param value es el nuevo valor de importe
     */
    public void setImporte(double value) {
        this.importe = value;
    }

    /**
     * Obtiene el valor de la variable linea captura.
     *
     * @return el linea captura
     */
    public String getLineaCaptura() {
        return lineaCaptura;
    }

    /**
     * Coloca el valor de linea captura.
     *
     * @param value es el nuevo valor de linea captura
     */
    public void setLineaCaptura(String value) {
        this.lineaCaptura = value;
    }

    /**
     * Obtiene el valor de la variable num cuenta.
     *
     * @return el num cuenta
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * Coloca el valor de num cuenta.
     *
     * @param value es el nuevo valor de num cuenta
     */
    public void setNumCuenta(String value) {
        this.numCuenta = value;
    }

    /**
     * Obtiene el valor de la variable referencia.
     *
     * @return el referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Coloca el valor de referencia.
     *
     * @param value es el nuevo valor de referencia
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

}
