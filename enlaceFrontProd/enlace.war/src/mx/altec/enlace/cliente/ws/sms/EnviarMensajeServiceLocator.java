/**
 * EnviarMensajeServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */


package mx.altec.enlace.cliente.ws.sms;

import mx.altec.enlace.utilerias.Global;


public class EnviarMensajeServiceLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, mx.altec.enlace.cliente.ws.sms.EnviarMensajeService {

    public EnviarMensajeServiceLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
        		Global.URL_SERVICIO_SMS,// "http://mxwebdes01.mx.bsch:8089/envioSMS/services/EnviarMensaje",
           "EnviarMensajeService"));

        context.setLocatorName("mx.altec.enlace.cliente.ws.sms.EnviarMensajeServiceLocator");
    }

    public EnviarMensajeServiceLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("mx.altec.enlace.cliente.ws.sms.EnviarMensajeServiceLocator");
    }

    // Utilizar para obtener la clase de proxy para enviarMensaje
    private final java.lang.String enviarMensaje_address =Global.URL_SERVICIO_SMS;// "http://mxwebdes01.mx.bsch:8089/envioSMS/services/EnviarMensaje";

    public java.lang.String getEnviarMensajeAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return enviarMensaje_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("EnviarMensaje");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return enviarMensaje_address;
        }
    }

    private java.lang.String enviarMensajePortName = "EnviarMensaje";

    // The WSDD port name defaults to the port name.
    private java.lang.String enviarMensajeWSDDPortName = "EnviarMensaje";

    public java.lang.String getEnviarMensajeWSDDPortName() {
        return enviarMensajeWSDDPortName;
    }

    public void setEnviarMensajeWSDDPortName(java.lang.String name) {
        enviarMensajeWSDDPortName = name;
    }

    public mx.altec.enlace.cliente.ws.sms.EnviarMensaje getEnviarMensaje() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getEnviarMensajeAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // es poco probable ya que URL se ha validado en WSDL2Java
        }
        return getEnviarMensaje(endpoint);
    }

    public mx.altec.enlace.cliente.ws.sms.EnviarMensaje getEnviarMensaje(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        mx.altec.enlace.cliente.ws.sms.EnviarMensaje _stub =
            (mx.altec.enlace.cliente.ws.sms.EnviarMensaje) getStub(
                enviarMensajePortName,
                (String) getPort2NamespaceMap().get(enviarMensajePortName),
                mx.altec.enlace.cliente.ws.sms.EnviarMensaje.class,
                "mx.altec.enlace.cliente.ws.sms.EnviarMensajeSoapBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(enviarMensajeWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.altec.enlace.cliente.ws.sms.EnviarMensaje.class.isAssignableFrom(serviceEndpointInterface)) {
                return getEnviarMensaje();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: No hay ninguna implementación de apéndice para la interfaz:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("EnviarMensaje".equals(inputPortName)) {
            return getEnviarMensaje();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        enviarMensajeWSDDPortName = prefix + "/" + enviarMensajePortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS//"http://mxwebdes01.mx.bsch:8089/envioSMS/services/EnviarMensaje"
        		, "EnviarMensajeService");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "EnviarMensaje",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
        if  (portName.getLocalPart().equals("EnviarMensaje")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "enviarMensaje", "enviarMensajeRequest"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
    }
}
