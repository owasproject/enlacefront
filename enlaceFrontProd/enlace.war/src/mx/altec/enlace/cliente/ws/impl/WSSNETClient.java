package mx.altec.enlace.cliente.ws.impl;

public interface WSSNETClient {
	
	/**
	 * M�todo que realiza la invoacion al cliente web service que obtiene
	 * las cuentas afiliadas a Supernet Comercios.
	 * @param codCliente
	 * @return arreglo de cuentas afiliadas.
	 */
	public String [] consultaCuentasCliente(String codCliente);

}
