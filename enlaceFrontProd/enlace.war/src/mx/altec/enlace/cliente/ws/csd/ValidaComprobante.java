/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ValidaComprobante.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * DTO para el tranporte de informacion de la validacion de comprobante con sello digital.
 *
 * @author FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaComprobante", propOrder = {
    "linCap",
    "imp",
    "ref",
    "fec",
    "idCanal",
    "numCuenta"
})
public class ValidaComprobante {

    /** La variable lin cap. */
    protected String linCap;
    
    /** La variable imp. */
    protected double imp;
    
    /** La variable ref. */
    protected String ref;
    
    /** La variable fec. */
    protected String fec;
    
    /** La variable id canal. */
    protected int idCanal;
    
    /** La variable num cuenta. */
    protected String numCuenta;

    /**
     * Obtiene el valor de la variable lin cap.
     *
     * @return el lin cap
     */
    public String getLinCap() {
        return linCap;
    }

    /**
     * Coloca el valor de lin cap.
     *
     * @param value es el nuevo valor de lin cap
     */
    public void setLinCap(String value) {
        this.linCap = value;
    }

    /**
     * Obtiene el valor de la variable imp.
     *
     * @return el imp
     */
    public double getImp() {
        return imp;
    }

    /**
     * Coloca el valor de imp.
     *
     * @param value es el nuevo valor de imp
     */
    public void setImp(double value) {
        this.imp = value;
    }

    /**
     * Obtiene el valor de la variable ref.
     *
     * @return el ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * Coloca el valor de ref.
     *
     * @param value es el nuevo valor de ref
     */
    public void setRef(String value) {
        this.ref = value;
    }

    /**
     * Obtiene el valor de la variable fec.
     *
     * @return el fec
     */
    public String getFec() {
        return fec;
    }

    /**
     * Coloca el valor de fec.
     *
     * @param value es el nuevo valor de fec
     */
    public void setFec(String value) {
        this.fec = value;
    }

    /**
     * Obtiene el valor de la variable id canal.
     *
     * @return el id canal
     */
    public int getIdCanal() {
        return idCanal;
    }

    /**
     * Coloca el valor de id canal.
     *
     * @param value es el nuevo valor de id canal
     */
    public void setIdCanal(int value) {
        this.idCanal = value;
    }

    /**
     * Obtiene el valor de la variable num cuenta.
     *
     * @return el num cuenta
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * Coloca el valor de num cuenta.
     *
     * @param value es el nuevo valor de num cuenta
     */
    public void setNumCuenta(String value) {
        this.numCuenta = value;
    }

}
