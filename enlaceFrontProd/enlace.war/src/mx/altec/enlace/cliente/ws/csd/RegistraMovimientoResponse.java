/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * RegistraMovimientoResponse.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Clase de respuesta para la generacion de la lista de movimientos para las operaciones masivas del WS Sellos digitales
 * FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registraMovimientoResponse", propOrder = {
    "_return"
})
public class RegistraMovimientoResponse {

    /** La variable refund. */
    @XmlElement(name = "return")
    protected Respuesta refund;

    /**
     * Obtiene el valor de la variable refund.
     *
     * @return el refund
     */
    public Respuesta getRefund() {
        return refund;
    }

    /**
     * Coloca el valor de refund.
     *
     * @param value es el nuevo valor de refund
     */
    public void setRefund(Respuesta value) {
        this.refund = value;
    }

}
