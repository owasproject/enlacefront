/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * ValidaComprobanteResponse.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * DTO para el tranporte de informacion de la respuesta validacion de comprobante con sello digital.
 *
 * @author FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaComprobanteResponse", propOrder = {
    "_return"
})
public class ValidaComprobanteResponse {

    /** La variable _return. */
    @XmlElement(name = "return")
    protected BeanRespuestaValidacion refund;

    /**
     * Obtiene el valor de la variable refund.
     *
     * @return el refund
     */
    public BeanRespuestaValidacion getRefund() {
        return refund;
    }

    /**
     * Coloca el valor de refund.
     *
     * @param value es el nuevo valor de refund
     */
    public void setRefund(BeanRespuestaValidacion value) {
        this.refund = value;
    }

}
