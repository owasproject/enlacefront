/**
 * BSCHUnBlockPwd.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.bloqueopwd;

public interface BSCHUnBlockPwd extends java.rmi.Remote {
    public java.lang.String userOpPwdUnBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException;
    public java.lang.String cambiarEstatus(java.lang.String appId, java.lang.String lang, java.lang.String usrId, int status, java.lang.String codWS) throws java.rmi.RemoteException;
    public java.lang.String userOpPwdBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException;
}
