/**
 * ConsultaCuentasClienteWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package mx.altec.enlace.cliente.ws.snetcomercios;

import mx.altec.enlace.utilerias.Global;

public class ConsultaCuentasClienteWSServiceLocator extends org.apache.axis.client.Service implements mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSService {

    public ConsultaCuentasClienteWSServiceLocator() {
    }


    public ConsultaCuentasClienteWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ConsultaCuentasClienteWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ConsultaCuentasClienteWSPort
    private java.lang.String ConsultaCuentasClienteWSPort_address = Global.URL_WEB_SERVICE_LOCATION;

    public java.lang.String getConsultaCuentasClienteWSPortAddress() {
        return ConsultaCuentasClienteWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ConsultaCuentasClienteWSPortWSDDServiceName = "ConsultaCuentasClienteWSPort";

    public java.lang.String getConsultaCuentasClienteWSPortWSDDServiceName() {
        return ConsultaCuentasClienteWSPortWSDDServiceName;
    }

    public void setConsultaCuentasClienteWSPortWSDDServiceName(java.lang.String name) {
        ConsultaCuentasClienteWSPortWSDDServiceName = name;
    }

    public mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS getConsultaCuentasClienteWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ConsultaCuentasClienteWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getConsultaCuentasClienteWSPort(endpoint);
    }

    public mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS getConsultaCuentasClienteWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSPortBindingStub _stub = new mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSPortBindingStub(portAddress, this);
            _stub.setPortName(getConsultaCuentasClienteWSPortWSDDServiceName());
            _stub.setTimeout(Integer.parseInt(Global.TIMEOUT_WEB_SERVICE));
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setConsultaCuentasClienteWSPortEndpointAddress(java.lang.String address) {
        ConsultaCuentasClienteWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSPortBindingStub _stub = new mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSPortBindingStub(new java.net.URL(ConsultaCuentasClienteWSPort_address), this);
                _stub.setPortName(getConsultaCuentasClienteWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ConsultaCuentasClienteWSPort".equals(inputPortName)) {
            return getConsultaCuentasClienteWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.supernetcomercios.santander.com/", "ConsultaCuentasClienteWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.supernetcomercios.santander.com/", "ConsultaCuentasClienteWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ConsultaCuentasClienteWSPort".equals(portName)) {
            setConsultaCuentasClienteWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
