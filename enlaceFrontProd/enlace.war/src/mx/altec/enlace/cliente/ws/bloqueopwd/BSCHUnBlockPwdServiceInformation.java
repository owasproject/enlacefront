/**
 * BSCHUnBlockPwdServiceInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.bloqueopwd;

public class BSCHUnBlockPwdServiceInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

    private static java.util.Map operationDescriptions;
    private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    private static void initOperationDescriptions() { 
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("cambiarEstatus", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc cambiarEstatus0Op = _cambiarEstatus0Op();
        list0.add(cambiarEstatus0Op);

        java.util.List list1 = new java.util.ArrayList();
        inner0.put("userOpPwdBlock", list1);

        com.ibm.ws.webservices.engine.description.OperationDesc userOpPwdBlock1Op = _userOpPwdBlock1Op();
        list1.add(userOpPwdBlock1Op);

        java.util.List list2 = new java.util.ArrayList();
        inner0.put("userOpPwdUnBlock", list2);

        com.ibm.ws.webservices.engine.description.OperationDesc userOpPwdUnBlock2Op = _userOpPwdUnBlock2Op();
        list2.add(userOpPwdUnBlock2Op);

        operationDescriptions.put("BSCHUnBlockPwd",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _cambiarEstatus0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc cambiarEstatus0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "status"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codWS"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","int");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}int");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "cambiarEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        cambiarEstatus0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("cambiarEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "cambiarEstatus"), _params0, _returnDesc0, _faults0, null);
        cambiarEstatus0Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "cambiarEstatusRequest"));
        cambiarEstatus0Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        cambiarEstatus0Op.setOption("outputName","cambiarEstatusResponse");
        cambiarEstatus0Op.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        cambiarEstatus0Op.setOption("buildNum","cf10631.06");
        cambiarEstatus0Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        cambiarEstatus0Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "cambiarEstatusResponse"));
        cambiarEstatus0Op.setOption("inputName","cambiarEstatusRequest");
        cambiarEstatus0Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        cambiarEstatus0Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        cambiarEstatus0Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return cambiarEstatus0Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _userOpPwdBlock1Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc userOpPwdBlock1Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrOprId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "tipoOper"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "userOpPwdBlockReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        userOpPwdBlock1Op = new com.ibm.ws.webservices.engine.description.OperationDesc("userOpPwdBlock", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "userOpPwdBlock"), _params0, _returnDesc0, _faults0, null);
        userOpPwdBlock1Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdBlockRequest"));
        userOpPwdBlock1Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        userOpPwdBlock1Op.setOption("outputName","userOpPwdBlockResponse");
        userOpPwdBlock1Op.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        userOpPwdBlock1Op.setOption("buildNum","cf10631.06");
        userOpPwdBlock1Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        userOpPwdBlock1Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdBlockResponse"));
        userOpPwdBlock1Op.setOption("inputName","userOpPwdBlockRequest");
        userOpPwdBlock1Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        userOpPwdBlock1Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        userOpPwdBlock1Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return userOpPwdBlock1Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _userOpPwdUnBlock2Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc userOpPwdUnBlock2Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrOprId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "tipoOper"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "userOpPwdUnBlockReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        userOpPwdUnBlock2Op = new com.ibm.ws.webservices.engine.description.OperationDesc("userOpPwdUnBlock", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "userOpPwdUnBlock"), _params0, _returnDesc0, _faults0, null);
        userOpPwdUnBlock2Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdUnBlockRequest"));
        userOpPwdUnBlock2Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        userOpPwdUnBlock2Op.setOption("outputName","userOpPwdUnBlockResponse");
        userOpPwdUnBlock2Op.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        userOpPwdUnBlock2Op.setOption("buildNum","cf10631.06");
        userOpPwdUnBlock2Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        userOpPwdUnBlock2Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdUnBlockResponse"));
        userOpPwdUnBlock2Op.setOption("inputName","userOpPwdUnBlockRequest");
        userOpPwdUnBlock2Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        userOpPwdUnBlock2Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        userOpPwdUnBlock2Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return userOpPwdUnBlock2Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}
