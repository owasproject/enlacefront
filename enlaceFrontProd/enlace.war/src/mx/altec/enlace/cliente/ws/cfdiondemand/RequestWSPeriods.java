
package mx.altec.enlace.cliente.ws.cfdiondemand;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestWSPeriods complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="requestWSPeriods">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cuentaFolioTDC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioUUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ingresoEgreso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPeriodos" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="periodo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoComprobante" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestWSPeriods", propOrder = {
    "cuentaFolioTDC",
    "folioUUID",
    "ingresoEgreso",
    "numPeriodos",
    "periodo",
    "serie",
    "tipoComprobante",
    "tipoCuenta"
})
public class RequestWSPeriods {

	/** variable cuentaFolioTDC **/
    protected String cuentaFolioTDC;
    /** variable folioUUID **/
    protected String folioUUID;
    /** variable ingresoEgreso **/
    protected String ingresoEgreso;
    /** variable numPeriodos **/
    protected Integer numPeriodos;
    /** variableperiodo  **/
    protected String periodo;
    /** variable serie **/
    protected String serie;
    /** variable tipoComprobante **/
    protected Integer tipoComprobante;
    /** variable tipoCuenta **/
    protected String tipoCuenta;

    /**
     * Gets the value of the cuentaFolioTDC property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCuentaFolioTDC() {
        return cuentaFolioTDC;
    }

    /**
     * Sets the value of the cuentaFolioTDC property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCuentaFolioTDC(String value) {
        this.cuentaFolioTDC = value;
    }

    /**
     * Gets the value of the folioUUID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFolioUUID() {
        return folioUUID;
    }

    /**
     * Sets the value of the folioUUID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFolioUUID(String value) {
        this.folioUUID = value;
    }

    /**
     * Gets the value of the ingresoEgreso property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIngresoEgreso() {
        return ingresoEgreso;
    }

    /**
     * Sets the value of the ingresoEgreso property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIngresoEgreso(String value) {
        this.ingresoEgreso = value;
    }

    /**
     * Gets the value of the numPeriodos property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getNumPeriodos() {
        return numPeriodos;
    }

    /**
     * Sets the value of the numPeriodos property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setNumPeriodos(Integer value) {
        this.numPeriodos = value;
    }

    /**
     * Gets the value of the periodo property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * Sets the value of the periodo property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPeriodo(String value) {
        this.periodo = value;
    }

     /**
     * Gets the value of the serie property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Sets the value of the serie property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Gets the value of the tipoComprobante property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getTipoComprobante() {
        return tipoComprobante;
    }

    /**
     * Sets the value of the tipoComprobante property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setTipoComprobante(Integer value) {
        this.tipoComprobante = value;
    }

    /** Gets the value of the tipoCuenta property
     *
     *  @return
     *  	possible object is
     *  {@link String }
     *
     */

    public String getTipoCuenta(){
    	return tipoCuenta;
    }

    /**
     * Sets the value of the tipoCuenta property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoCuenta(String value) {
        this.tipoCuenta = value;
    }

}