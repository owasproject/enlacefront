
package mx.altec.enlace.cliente.ws.pdfondemand;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validaExistencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validaExistencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestWS" type="{http://ws.estadocuenta.isban.mx/}requestWS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validaExistencia", propOrder = {
    "requestWS"
})
public class ValidaExistencia {

	/** Variable requestWS **/
    protected RequestWS requestWS;

    /**
     * Gets the value of the requestWS property.
     * 
     * @return
     *     possible object is
     *     {@link RequestWS }
     *     
     */
    public RequestWS getRequestWS() {
        return requestWS;
    }

    /**
     * Sets the value of the requestWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestWS }
     *     
     */
    public void setRequestWS(RequestWS value) {
        this.requestWS = value;
    }

}
