/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * RegistraMovimiento.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

// TODO: Auto-generated Javadoc
/**
 * Clase para generar la lista de movimientos para las operaciones masivas del WS Sellos digitales
 * FSW Everis.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registraMovimiento", propOrder = {
    "movimientos"
})
public class RegistraMovimiento {

    /** La variable movimientos. */
    @XmlElement(nillable = true)
    protected List<BeanDatosOperMasiva> movimientos;

    /**
     * Obtiene el valor de la variable movimientos.
     *
     * @return el movimientos
     */
    public List<BeanDatosOperMasiva> getMovimientos() {
        if (movimientos == null) {
            movimientos = new ArrayList<BeanDatosOperMasiva>();
        }
        return this.movimientos;
    }
    
    /**
     * Coloca el valor de movimientos.
     *
     * @param movimientos es el nuevo valor de movimientos
     */
    public void setMovimientos(List<BeanDatosOperMasiva> movimientos) {
        this.movimientos = movimientos;
    }

}
