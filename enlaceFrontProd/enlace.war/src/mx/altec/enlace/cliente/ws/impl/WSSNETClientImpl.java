package mx.altec.enlace.cliente.ws.impl;

import java.rmi.RemoteException;

import mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWSProxy;
import mx.altec.enlace.utilerias.EIGlobal;

public class WSSNETClientImpl implements WSSNETClient {

	public String[] consultaCuentasCliente(String codCliente) {
		EIGlobal.mensajePorTrace( "Iniciando llamada a web service a supernet comercios con codigo de cliente-------> "+ codCliente,EIGlobal.NivelLog.INFO);

		String [] cuentasCliente = null;
		ConsultaCuentasClienteWSProxy cliente = new ConsultaCuentasClienteWSProxy();

		try {
			cuentasCliente = cliente.consultaCuentasCliente(codCliente);
			EIGlobal.mensajePorTrace( "Se invoco al web service con exito",EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace( "total de cuentas afiliadas a supernet comercios:   " + cuentasCliente.length,EIGlobal.NivelLog.INFO);
		} catch (RemoteException e) {
			EIGlobal.mensajePorTrace( "Se genero un error al llamar al web sercice consultaCuentasCliente "+ e.getMessage(),EIGlobal.NivelLog.ERROR);
		}
		return cuentasCliente;
	}

}
