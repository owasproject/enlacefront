/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * Respuesta.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.cliente.ws.csd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * DTO de respuesta de operaciones del WS Sellos digitales
 * FSW Everis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuesta", propOrder = {
    "codError",
    "msgError",
    "objetoResultado"
})
@XmlSeeAlso({
    BeanRespuestaGeneracion.class,
    BeanRespuestaValidacion.class
})
public class Respuesta {

    /** La variable cod error. */
    protected String codError;
    
    /** La variable msg error. */
    protected String msgError;
    
    /** La variable objeto resultado. */
    protected Object objetoResultado;

    /**
     * Obtiene el valor de la variable cod error.
     *
     * @return el cod error
     */
    public String getCodError() {
        return codError;
    }

    /**
     * Coloca el valor de cod error.
     *
     * @param value es el nuevo valor de cod error
     */
    public void setCodError(String value) {
        this.codError = value;
    }

    /**
     * Obtiene el valor de la variable msg error.
     *
     * @return el msg error
     */
    public String getMsgError() {
        return msgError;
    }

    /**
     * Coloca el valor de msg error.
     *
     * @param value es el nuevo valor de msg error
     */
    public void setMsgError(String value) {
        this.msgError = value;
    }

    /**
     * Obtiene el valor de la variable objeto resultado.
     *
     * @return el objeto resultado
     */
    public Object getObjetoResultado() {
        return objetoResultado;
    }

    /**
     * Coloca el valor de objeto resultado.
     *
     * @param value es el nuevo valor de objeto resultado
     */
    public void setObjetoResultado(Object value) {
        this.objetoResultado = value;
    }

}
