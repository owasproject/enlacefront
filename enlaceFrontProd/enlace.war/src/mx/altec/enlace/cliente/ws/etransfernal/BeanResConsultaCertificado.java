/** 
*   Isban Mexico
*   Clase: BeanResConsultaCertificado.java
*   Descripcion: Objeto de tranferencia de informacion de los datos de respuesta de la consulta del certificado
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import mx.altec.enlace.utilerias.FielConstants;


/**
 * Objeto de tranferencia de informacion de los datos de respuesta de la consulta del certificado
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanResConsultaCertificado", propOrder = {
    "codRespuesta",
    "datosTitular",
    "estadoCertificado",
    "fchEmision",
    "fchExpiracion",
    "mensaje",
    "numSerie"
})
public class BeanResConsultaCertificado implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = 2791348070532734329L;
    
    /** La variable datos titular. */
    protected BeanDatosTitular datosTitular;

    /** La variable cod respuesta. */
    protected String codRespuesta = FielConstants.CADENA_VACIA;
    
    /** La variable estado certificado. */
    protected String estadoCertificado = FielConstants.CADENA_VACIA;
    
    /** La variable fch emision. */
    protected String fchEmision = FielConstants.CADENA_VACIA;
    
    /** La variable fch expiracion. */
    protected String fchExpiracion = FielConstants.CADENA_VACIA;
    
    /** La variable mensaje. */
    protected String mensaje = FielConstants.CADENA_VACIA;
    
    /** La variable num serie. */
    protected String numSerie = FielConstants.CADENA_VACIA;

    
    /**
     * Contructor de bean res consulta certificado.
     */
    public BeanResConsultaCertificado(){
    	super();
    	this.datosTitular = new BeanDatosTitular();
    }
    
    /**
     * Obtiene el valor de la propiedad codRespuesta.
     * 
     * @return el codigo de respuesta
     *     
     */
    public String getCodRespuesta() {
        return codRespuesta;
    }

    /**
     * Define el valor de la propiedad codRespuesta.
     * 
     * @param value es el nuevo valor del codigo de respuesta
     *     
     */
    public void setCodRespuesta(String value) {
        this.codRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad datosTitular.
     * 
     * @return los datos del titular
     *     
     */
    public BeanDatosTitular getDatosTitular() {
        return datosTitular;
    }

    /**
     * Define el valor de la propiedad datosTitular.
     * 
     * @param value es el nuevo valor de los datos del titular
     *     
     */
    public void setDatosTitular(BeanDatosTitular value) {
        this.datosTitular = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCertificado.
     * 
     * @return el estado del certificado
     *     
     */
    public String getEstadoCertificado() {
        return estadoCertificado;
    }

    /**
     * Define el valor de la propiedad estadoCertificado.
     * 
     * @param value es el nuevo valor del estado del certificado
     *     
     */
    public void setEstadoCertificado(String value) {
        this.estadoCertificado = value;
    }

    /**
     * Obtiene el valor de la propiedad fchEmision.
     * 
     * @return la fecha de emision
     *     
     */
    public String getFchEmision() {
        return fchEmision;
    }

    /**
     * Define el valor de la propiedad fchEmision.
     * 
     * @param value es el nuevo valor de la fecha de emision
     *     
     */
    public void setFchEmision(String value) {
        this.fchEmision = value;
    }

    /**
     * Obtiene el valor de la propiedad fchExpiracion.
     * 
     * @return la fecha de expiracion
     *     
     */
    public String getFchExpiracion() {
        return fchExpiracion;
    }

    /**
     * Define el valor de la propiedad fchExpiracion.
     * 
     * @param value es el nuevo valor de la fecha de expiracion
     *     
     */
    public void setFchExpiracion(String value) {
        this.fchExpiracion = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return el mensaje
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value es el nuevo valor del mensaje
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad numSerie.
     * 
     * @return el numero de serie
     *     
     */
    public String getNumSerie() {
        return numSerie;
    }

    /**
     * Define el valor de la propiedad numSerie.
     * 
     * @param value es el nuevo valor del numero de serie
     */
    public void setNumSerie(String value) {
        this.numSerie = value;
    }

    /**
     * Complementa la informacion general del objeto.
     */
    public void complemento() {
        if (estadoCertificado == null) {
            estadoCertificado = FielConstants.CADENA_VACIA;
        }
        if (fchEmision == null) {
            fchEmision = FielConstants.CADENA_VACIA;
        }
        if (fchExpiracion == null) {
            fchExpiracion = FielConstants.CADENA_VACIA;
        }
        if (mensaje == null) {
            mensaje = FielConstants.CADENA_VACIA;
        }
        if (numSerie == null) {
            numSerie = FielConstants.CADENA_VACIA;
        }
    }
}