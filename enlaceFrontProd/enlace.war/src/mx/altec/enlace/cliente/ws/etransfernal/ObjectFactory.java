/** 
*   Isban Mexico
*   Clase: ObjectFactory.java
*   Descripcion: Clase para crear instancias de los DTOs usados en el cliente WS
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import mx.altec.enlace.utilerias.FielConstants;


/**
 * Clase para crear instancias de los DTOs usados en el cliente WS
 * @author FSW Everis
 *
 */
@XmlRegistry
public class ObjectFactory {

	/** Constante para almacena la queue de consulta */
    private final static QName CONSULTA_CERTIFICADO_Q_NAME = new QName(FielConstants.NAMESPACE, "consultaCertificado");
    /** Constante para almacena la queue de respuesta de la consulta */
    private final static QName CONSULTA_CERTIFICADO_RESPONSE_Q_NAME = new QName(FielConstants.NAMESPACE, "consultaCertificadoResponse");


    /**
     * Creata una instancia de ConsultaCertificado
     * @return nueva instancia de ConsultaCertificado
     */
    public ConsultaCertificado createConsultaCertificado() {
        return new ConsultaCertificado();
    }

    /**
     * Creata una instancia de ConsultaCertificadoResponse
     * @return nueva instancia de ConsultaCertificadoResponse
     */
    public ConsultaCertificadoResponse createConsultaCertificadoResponse() {
        return new ConsultaCertificadoResponse();
    }

    /**
     * Creata una instancia de BeanResConsultaCertificado
     * @return nueva instancia de BeanResConsultaCertificado
     */
    public BeanResConsultaCertificado createBeanResConsultaCertificado() {
        return new BeanResConsultaCertificado();
    }

    /**
     * Creata una instancia de BeanReqConsultaCertificado
     * @return nueva instancia de BeanReqConsultaCertificado
     */
    public BeanReqConsultaCertificado createBeanReqConsultaCertificado() {
        return new BeanReqConsultaCertificado();
    }

    /**
     * Creata una instancia de BeanDatosTitular
     * @return nueva instancia de BeanDatosTitular
     */
    public BeanDatosTitular createBeanDatosTitular() {
        return new BeanDatosTitular();
    }

    /**
     * Creata una instancia de JAXBElement<ConsultaCertificado>
     * @param value dto de la consulta de certificado
     * @return nueva instancia de JAXBElement<ConsultaCertificado>
     */
    @XmlElementDecl(namespace = FielConstants.NAMESPACE, name = "consultaCertificado")
    public JAXBElement<ConsultaCertificado> createConsultaCertificado(ConsultaCertificado value) {
        return new JAXBElement<ConsultaCertificado>(CONSULTA_CERTIFICADO_Q_NAME, ConsultaCertificado.class, null, value);
    }

    /**
     * Creata una instancia de JAXBElement<ConsultaCertificadoResponse>
     * @param value dto de respuesta de la consulta de certificado
     * @return nueva instancia de JAXBElement<ConsultaCertificadoResponse>
     */
    @XmlElementDecl(namespace = FielConstants.NAMESPACE, name = "consultaCertificadoResponse")
    public JAXBElement<ConsultaCertificadoResponse> createConsultaCertificadoResponse(ConsultaCertificadoResponse value) {
        return new JAXBElement<ConsultaCertificadoResponse>(CONSULTA_CERTIFICADO_RESPONSE_Q_NAME, ConsultaCertificadoResponse.class, null, value);
    }

}
