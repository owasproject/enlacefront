/**
 * EnviarMensaje.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.sms;

public interface EnviarMensaje extends java.rmi.Remote {
    public java.lang.String enviarMensaje(java.lang.String destino, java.lang.String claveProveedor, java.lang.String numeroCliente, java.lang.String cveMensaje, java.lang.String fecha, java.lang.String mensaje) throws java.rmi.RemoteException;
}
