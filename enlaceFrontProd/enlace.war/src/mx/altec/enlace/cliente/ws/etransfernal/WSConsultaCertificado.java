/** 
*   Isban Mexico
*   Clase: WSConsultaCertificado.java
*   Descripcion: Interface para definir las funcionalidades del consumo del servicio web de FIEL
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.cliente.ws.etransfernal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import mx.altec.enlace.utilerias.FielConstants;

/**
 * Interface para definir las funcionalidades del consumo del servicio web de FIEL
 * @author FSW Everis
 *
 */
@WebService(name = "WSConsultaCertificado", targetNamespace = FielConstants.NAMESPACE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WSConsultaCertificado {


    /**
     * Metodo para consultar el certificado
     * @param param DTO de con la informacion de la consulta
     * @return
     *     returns mx.isban.etransfernal.ws.modulospid.BeanResConsultaCertificado
     */
    @WebMethod
    @WebResult(name = "respuesta", targetNamespace = "")
    @RequestWrapper(localName = "consultaCertificado", targetNamespace = FielConstants.NAMESPACE, className = "mx.isban.etransfernal.ws.modulospid.ConsultaCertificado")
    @ResponseWrapper(localName = "consultaCertificadoResponse", targetNamespace = FielConstants.NAMESPACE, className = "mx.isban.etransfernal.ws.modulospid.ConsultaCertificadoResponse")
    @Action(input = "http://moduloSPID.ws.eTransferNal.isban.mx/WSConsultaCertificado/consultaCertificadoRequest", output = "http://moduloSPID.ws.eTransferNal.isban.mx/WSConsultaCertificado/consultaCertificadoResponse")
    public BeanResConsultaCertificado consultaCertificado(
        @WebParam(name = "param", targetNamespace = "")
        BeanReqConsultaCertificado param);

}
