/** 
*   Isban Mexico
*   Clase: ConsultaCertificado.java
*   Descripcion: Objeto de tranferencia de informacion de la consulta del certificado.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/
package mx.altec.enlace.cliente.ws.etransfernal;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Objeto de tranferencia de informacion de la consulta del certificado
 * @author FSW Everis
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaCertificado", propOrder = {
    "param"
})
public class ConsultaCertificado implements Serializable {

    /** Serial Version Universal ID. */
    private static final long serialVersionUID = 6610802645917964166L;

	/**
	 * El param
	 */
    protected BeanReqConsultaCertificado param;

    /**
     * Obtiene el valor de la propiedad param.
     * 
     * @return el param
     *     
     */
    public BeanReqConsultaCertificado getParam() {
        return param;
    }

    /**
     * Define el valor de la propiedad param.
     * 
     * @param value es el nuevo valor del param
     *     
     */
    public void setParam(BeanReqConsultaCertificado value) {
        this.param = value;
    }

}
