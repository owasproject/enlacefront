package mx.altec.enlace.cliente.ws.bloqueotoken;

public class CanalesWSProxy implements CanalesWS {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private CanalesWS __canalesWS = null;

  public CanalesWSProxy() {
    _initCanalesWSProxy();
  }

  private void _initCanalesWSProxy() {

    if (_useJNDI) {
      try {
        javax.naming.InitialContext ctx = new javax.naming.InitialContext();
        __canalesWS = ((CanalesWSService)ctx.lookup("java:comp/env/service/CanalesWSService")).getCanalesWS();
      }
      catch (javax.naming.NamingException namingException) {}
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__canalesWS == null) {
      try {
        __canalesWS = (new CanalesWSServiceLocator()).getCanalesWS();

      }
      catch (javax.xml.rpc.ServiceException serviceException) {}
    }
    if (__canalesWS != null) {
      if (_endpoint != null)
        ((javax.xml.rpc.Stub)__canalesWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
      else
        _endpoint = (String)((javax.xml.rpc.Stub)__canalesWS)._getProperty("javax.xml.rpc.service.endpoint.address");
    }

  }


  public void useJNDI(boolean useJNDI) {
    _useJNDI = useJNDI;
    __canalesWS = null;

  }

  public String getEndpoint() {
    return _endpoint;
  }

  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (__canalesWS != null)
      ((javax.xml.rpc.Stub)__canalesWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

  }

  public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.estatus(codCliente, codIdAplicacion, codOperacion);
  }

  public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.contrato(codCliente, codIdAplicacion, codLang, codOperacion);
  }

  public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.consultaListaEstatus(lista, codIdAplicacion, codLang, codOperacion);
  }

  public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.bloqueo(codCliente, codIdAplicacion, codLang, codMotivo);
  }

  public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.desBloqueo(codCliente, codIdAplicacion);
  }

  public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.activacion(codCliente, codIdAplicacion, codLang);
  }

  public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.solicitud(codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa);
  }

  public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.consultaEstatus(codCliente, codIdAplicacion, codOperacion);
  }

  public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.consultaContrato(codCliente, codIdAplicacion, codLang, codOperacion);
  }

  public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.activarToken(codCliente, codIdAplicacion, codLang);
  }

  public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.bloquearToken(codCliente, codIdAplicacion, codLang, codMotivo);
  }

  public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.desBloquearToken(codCliente, codIdAplicacion);
  }

  public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException{
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS.solicitudToken(codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa);
  }


  public CanalesWS getCanalesWS() {
    if (__canalesWS == null)
      _initCanalesWSProxy();
    return __canalesWS;
  }

}