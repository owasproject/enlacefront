
package mx.altec.enlace.cliente.ws.cfdiondemand;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultPeriods complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultPeriods">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestWSPeriodsCFDI" type="{http://ws.CFDIOndemand.isban.mx/}requestWSPeriods" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultPeriods", propOrder = {
    "requestWSPeriodsCFDI"
})
public class ConsultPeriods {

	/** Variable requestWSPeriodsCFDI **/
    protected RequestWSPeriods requestWSPeriodsCFDI;

    /**
     * Gets the value of the requestWSPeriodsCFDI property.
     * 
     * @return
     *     possible object is
     *     {@link RequestWSPeriods }
     *     
     */
    public RequestWSPeriods getRequestWSPeriodsCFDI() {
        return requestWSPeriodsCFDI;
    }

    /**
     * Sets the value of the requestWSPeriodsCFDI property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestWSPeriods }
     *     
     */
    public void setRequestWSPeriodsCFDI(RequestWSPeriods value) {
        this.requestWSPeriodsCFDI = value;
    }

}
