/**
 * CanalesWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf160638.12 v101006191000
 */

package mx.altec.enlace.cliente.ws.bloqueotoken;

import mx.altec.enlace.utilerias.Global;

public class CanalesWSServiceLocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, CanalesWSService {

    public CanalesWSServiceLocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
        		Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));

        context.setLocatorName("CanalesWSServiceLocator");
    }

    public CanalesWSServiceLocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("CanalesWSServiceLocator");
    }

    // Utilizar para obtener la clase de proxy para canalesWS
    private final java.lang.String canalesWS_address = Global.URL_WS_BLOQ_TOKEN;

    public java.lang.String getCanalesWSAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return canalesWS_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("CanalesWS");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return canalesWS_address;
        }
    }

    private java.lang.String canalesWSPortName = "CanalesWS";

    // The WSDD port name defaults to the port name.
    private java.lang.String canalesWSWSDDPortName = "CanalesWS";

    public java.lang.String getCanalesWSWSDDPortName() {
        return canalesWSWSDDPortName;
    }

    public void setCanalesWSWSDDPortName(java.lang.String name) {
        canalesWSWSDDPortName = name;
    }

    public CanalesWS getCanalesWS() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getCanalesWSAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // es poco probable ya que URL se ha validado en WSDL2Java
        }
        return getCanalesWS(endpoint);
    }

    public CanalesWS getCanalesWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        CanalesWS _stub =
            (CanalesWS) getStub(
                canalesWSPortName,
                (String) getPort2NamespaceMap().get(canalesWSPortName),
                CanalesWS.class,
                "CanalesWSSoapBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(canalesWSWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (CanalesWS.class.isAssignableFrom(serviceEndpointInterface)) {
                return getCanalesWS();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: No hay ninguna implementación de apéndice para la interfaz:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("CanalesWS".equals(inputPortName)) {
            return getCanalesWS();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        canalesWSWSDDPortName = prefix + "/" + canalesWSPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "CanalesWS",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
        if  (portName.getLocalPart().equals("CanalesWS")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "Estatus", "EstatusRequest"),
                createCall(portName, "Contrato", "ContratoRequest"),
                createCall(portName, "consultaListaEstatus", "consultaListaEstatusRequest"),
                createCall(portName, "Bloqueo", "BloqueoRequest"),
                createCall(portName, "desBloqueo", "desBloqueoRequest"),
                createCall(portName, "Activacion", "ActivacionRequest"),
                createCall(portName, "Solicitud", "SolicitudRequest"),
                createCall(portName, "consultaEstatus", "consultaEstatusRequest"),
                createCall(portName, "consultaContrato", "consultaContratoRequest"),
                createCall(portName, "activarToken", "activarTokenRequest"),
                createCall(portName, "bloquearToken", "bloquearTokenRequest"),
                createCall(portName, "desBloquearToken", "desBloquearTokenRequest"),
                createCall(portName, "solicitudToken", "solicitudTokenRequest"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName no debe ser nulo.");
        }
    }
}