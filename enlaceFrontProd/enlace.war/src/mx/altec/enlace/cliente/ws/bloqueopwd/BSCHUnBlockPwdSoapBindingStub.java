/**
 * BSCHUnBlockPwdSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.bloqueopwd;

public class BSCHUnBlockPwdSoapBindingStub extends com.ibm.ws.webservices.engine.client.Stub implements BSCHUnBlockPwd {
    public BSCHUnBlockPwdSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[3];
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_SOAP11_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        javax.xml.namespace.QName compQName = null;
        javax.xml.namespace.QName compTypeQName = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _userOpPwdUnBlockOperation0;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrOprId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "tipoOper"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "userOpPwdUnBlockReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _userOpPwdUnBlockOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("userOpPwdUnBlock", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "userOpPwdUnBlock"), _params0, _returnDesc0, _faults0, "");
        _userOpPwdUnBlockOperation0.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdUnBlockRequest"));
        _userOpPwdUnBlockOperation0.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _userOpPwdUnBlockOperation0.setOption("outputName","userOpPwdUnBlockResponse");
        _userOpPwdUnBlockOperation0.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        _userOpPwdUnBlockOperation0.setOption("buildNum","cf10631.06");
        _userOpPwdUnBlockOperation0.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        _userOpPwdUnBlockOperation0.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdUnBlockResponse"));
        _userOpPwdUnBlockOperation0.setOption("inputName","userOpPwdUnBlockRequest");
        _userOpPwdUnBlockOperation0.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _userOpPwdUnBlockOperation0.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        _userOpPwdUnBlockOperation0.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _userOpPwdUnBlockOperation0.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _userOpPwdUnBlockIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getuserOpPwdUnBlockInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_userOpPwdUnBlockIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(BSCHUnBlockPwdSoapBindingStub._userOpPwdUnBlockOperation0);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_userOpPwdUnBlockIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String userOpPwdUnBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getuserOpPwdUnBlockInvoke0(new java.lang.Object[] {appId, lang, usrId, usrOprId, tipoOper}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (java.lang.String) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String.class);
        }
    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _userOpPwdBlockOperation1;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params1 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrOprId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "tipoOper"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params1[0].setOption("partName","string");
        _params1[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[1].setOption("partName","string");
        _params1[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[2].setOption("partName","string");
        _params1[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[3].setOption("partName","string");
        _params1[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[4].setOption("partName","string");
        _params1[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc1 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "userOpPwdBlockReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc1.setOption("partName","string");
        _returnDesc1.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults1 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _userOpPwdBlockOperation1 = new com.ibm.ws.webservices.engine.description.OperationDesc("userOpPwdBlock", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "userOpPwdBlock"), _params1, _returnDesc1, _faults1, "");
        _userOpPwdBlockOperation1.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdBlockRequest"));
        _userOpPwdBlockOperation1.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _userOpPwdBlockOperation1.setOption("outputName","userOpPwdBlockResponse");
        _userOpPwdBlockOperation1.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        _userOpPwdBlockOperation1.setOption("buildNum","cf10631.06");
        _userOpPwdBlockOperation1.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        _userOpPwdBlockOperation1.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "userOpPwdBlockResponse"));
        _userOpPwdBlockOperation1.setOption("inputName","userOpPwdBlockRequest");
        _userOpPwdBlockOperation1.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _userOpPwdBlockOperation1.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        _userOpPwdBlockOperation1.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _userOpPwdBlockOperation1.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _userOpPwdBlockIndex1 = 1;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getuserOpPwdBlockInvoke1(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_userOpPwdBlockIndex1];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(BSCHUnBlockPwdSoapBindingStub._userOpPwdBlockOperation1);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_userOpPwdBlockIndex1] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String userOpPwdBlock(java.lang.String appId, java.lang.String lang, java.lang.String usrId, java.lang.String usrOprId, java.lang.String tipoOper) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getuserOpPwdBlockInvoke1(new java.lang.Object[] {appId, lang, usrId, usrOprId, tipoOper}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (java.lang.String) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String.class);
        }
    }

    private static final com.ibm.ws.webservices.engine.description.OperationDesc _cambiarEstatusOperation2;
    static {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params2 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "appId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "usrId"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "status"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codWS"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params2[0].setOption("partName","string");
        _params2[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[1].setOption("partName","string");
        _params2[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[2].setOption("partName","string");
        _params2[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[3].setOption("partName","int");
        _params2[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}int");
        _params2[4].setOption("partName","string");
        _params2[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc2 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "cambiarEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc2.setOption("partName","string");
        _returnDesc2.setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults2 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _cambiarEstatusOperation2 = new com.ibm.ws.webservices.engine.description.OperationDesc("cambiarEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://DefaultNamespace", "cambiarEstatus"), _params2, _returnDesc2, _faults2, "");
        _cambiarEstatusOperation2.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "cambiarEstatusRequest"));
        _cambiarEstatusOperation2.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _cambiarEstatusOperation2.setOption("outputName","cambiarEstatusResponse");
        _cambiarEstatusOperation2.setOption("targetNamespace","http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws");
        _cambiarEstatusOperation2.setOption("buildNum","cf10631.06");
        _cambiarEstatusOperation2.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwdService"));
        _cambiarEstatusOperation2.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "cambiarEstatusResponse"));
        _cambiarEstatusOperation2.setOption("inputName","cambiarEstatusRequest");
        _cambiarEstatusOperation2.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _cambiarEstatusOperation2.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://appsqa02.mx.bsch:9080/axis/altecmx/BSCHUnBlockPwd.jws", "BSCHUnBlockPwd"));
        _cambiarEstatusOperation2.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _cambiarEstatusOperation2.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
    }

    private int _cambiarEstatusIndex2 = 2;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getcambiarEstatusInvoke2(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_cambiarEstatusIndex2];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(BSCHUnBlockPwdSoapBindingStub._cambiarEstatusOperation2);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_cambiarEstatusIndex2] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String cambiarEstatus(java.lang.String appId, java.lang.String lang, java.lang.String usrId, int status, java.lang.String codWS) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getcambiarEstatusInvoke2(new java.lang.Object[] {appId, lang, usrId, new java.lang.Integer(status), codWS}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        } 
        try {
            return (java.lang.String) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String.class);
        }
    }

}
