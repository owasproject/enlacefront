/**
 * ConsultaCuentasClienteWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package mx.altec.enlace.cliente.ws.snetcomercios;

public interface ConsultaCuentasClienteWSService extends javax.xml.rpc.Service {
    public java.lang.String getConsultaCuentasClienteWSPortAddress();

    public mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS getConsultaCuentasClienteWSPort() throws javax.xml.rpc.ServiceException;

    public mx.altec.enlace.cliente.ws.snetcomercios.ConsultaCuentasClienteWS getConsultaCuentasClienteWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
