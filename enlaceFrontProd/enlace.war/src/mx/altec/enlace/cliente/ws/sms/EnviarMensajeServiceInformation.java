/**
 * EnviarMensajeServiceInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf10631.06 v81706232132
 */

package mx.altec.enlace.cliente.ws.sms;

import mx.altec.enlace.utilerias.Global;

public class EnviarMensajeServiceInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

    private static java.util.Map operationDescriptions;
    private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    private static void initOperationDescriptions() { 
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("enviarMensaje", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc enviarMensaje0Op = _enviarMensaje0Op();
        list0.add(enviarMensaje0Op);

        operationDescriptions.put("EnviarMensaje",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _enviarMensaje0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc enviarMensaje0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "destino"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "claveProveedor"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "numeroCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "cveMensaje"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "fecha"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "mensaje"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partName","string");
        _params0[0].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[1].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[2].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[3].setOption("partName","string");
        _params0[3].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[4].setOption("partName","string");
        _params0[4].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        _params0[5].setOption("partName","string");
        _params0[5].setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "enviarMensajeReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partName","string");
        _returnDesc0.setOption("partQNameString","{http://schemas.xmlsoap.org/soap/encoding/}string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        enviarMensaje0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("enviarMensaje", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://msgEnvio.pampa.santander.com", "enviarMensaje"), _params0, _returnDesc0, _faults0, null);
        enviarMensaje0Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "enviarMensajeRequest"));
        enviarMensaje0Op.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        enviarMensaje0Op.setOption("outputName","enviarMensajeResponse");
        enviarMensaje0Op.setOption("targetNamespace",Global.URL_SERVICIO_SMS);
        enviarMensaje0Op.setOption("buildNum","cf10631.06");
        enviarMensaje0Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "EnviarMensajeService"));
        enviarMensaje0Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "enviarMensajeResponse"));
        enviarMensaje0Op.setOption("inputName","enviarMensajeRequest");
        enviarMensaje0Op.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        enviarMensaje0Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_SERVICIO_SMS, "EnviarMensaje"));
        enviarMensaje0Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return enviarMensaje0Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}
