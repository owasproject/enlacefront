/**
 * CanalesWSSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf160638.12 v101006191000
 */

package mx.altec.enlace.cliente.ws.bloqueotoken;

import mx.altec.enlace.utilerias.Global;

public class CanalesWSSoapBindingStub extends com.ibm.ws.webservices.engine.client.Stub implements CanalesWS {
    public CanalesWSSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws com.ibm.ws.webservices.engine.WebServicesFault {
        if (service == null) {
            super.service = new com.ibm.ws.webservices.engine.client.Service();
        }
        else {
            super.service = service;
        }
        super.engine = ((com.ibm.ws.webservices.engine.client.Service) super.service).getEngine();
        initTypeMapping();
        super.cachedEndpoint = endpointURL;
        super.connection = ((com.ibm.ws.webservices.engine.client.Service) super.service).getConnection(endpointURL);
        super.messageContexts = new com.ibm.ws.webservices.engine.MessageContext[13];
    }

    private void initTypeMapping() {
        javax.xml.rpc.encoding.TypeMapping tm = super.getTypeMapping(com.ibm.ws.webservices.engine.Constants.URI_SOAP11_ENC);
        java.lang.Class javaType = null;
        javax.xml.namespace.QName xmlType = null;
        javax.xml.namespace.QName compQName = null;
        javax.xml.namespace.QName compTypeQName = null;
        com.ibm.ws.webservices.engine.encoding.SerializerFactory sf = null;
        com.ibm.ws.webservices.engine.encoding.DeserializerFactory df = null;
        javaType = java.lang.String[].class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string");
        compTypeQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArraySerializerFactory.class, javaType, xmlType, null, compTypeQName);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArrayDeserializerFactory.class, javaType, xmlType, null, compTypeQName);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

        javaType = java.lang.String[][].class;
        xmlType = com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOfArrayOf_xsd_string");
        compTypeQName = com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string");
        sf = com.ibm.ws.webservices.engine.encoding.ser.BaseSerializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArraySerializerFactory.class, javaType, xmlType, null, compTypeQName);
        df = com.ibm.ws.webservices.engine.encoding.ser.BaseDeserializerFactory.createFactory(com.ibm.ws.webservices.engine.encoding.ser.ArrayDeserializerFactory.class, javaType, xmlType, null, compTypeQName);
        if (sf != null || df != null) {
            tm.register(javaType, xmlType, sf, df);
        }

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _estatusOperation0 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getestatusOperation0() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params0[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[0].setOption("partName","string");
        _params0[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[1].setOption("partName","string");
        _params0[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params0[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "EstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc0.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc0.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _estatusOperation0 = new com.ibm.ws.webservices.engine.description.OperationDesc("estatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Estatus"), _params0, _returnDesc0, _faults0, "");
        _estatusOperation0.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _estatusOperation0.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _estatusOperation0.setOption("inputName","EstatusRequest");
        _estatusOperation0.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "EstatusResponse"));
        _estatusOperation0.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _estatusOperation0.setOption("buildNum","cf160638.12");
        _estatusOperation0.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _estatusOperation0.setOption("outputName","EstatusResponse");
        _estatusOperation0.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _estatusOperation0.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "EstatusRequest"));
        _estatusOperation0.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _estatusOperation0.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _estatusOperation0;

    }

    private int _estatusIndex0 = 0;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getestatusInvoke0(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_estatusIndex0];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._estatusOperation0);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_estatusIndex0] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getestatusInvoke0(new java.lang.Object[] {codCliente, codIdAplicacion, codOperacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _contratoOperation1 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getcontratoOperation1() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params1 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params1[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[0].setOption("partName","string");
        _params1[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[1].setOption("partName","string");
        _params1[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[2].setOption("partName","string");
        _params1[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params1[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc1 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "ContratoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc1.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc1.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults1 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _contratoOperation1 = new com.ibm.ws.webservices.engine.description.OperationDesc("contrato", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Contrato"), _params1, _returnDesc1, _faults1, "");
        _contratoOperation1.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _contratoOperation1.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _contratoOperation1.setOption("inputName","ContratoRequest");
        _contratoOperation1.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ContratoResponse"));
        _contratoOperation1.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _contratoOperation1.setOption("buildNum","cf160638.12");
        _contratoOperation1.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _contratoOperation1.setOption("outputName","ContratoResponse");
        _contratoOperation1.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _contratoOperation1.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ContratoRequest"));
        _contratoOperation1.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _contratoOperation1.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _contratoOperation1;

    }

    private int _contratoIndex1 = 1;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getcontratoInvoke1(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_contratoIndex1];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._contratoOperation1);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_contratoIndex1] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getcontratoInvoke1(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaListaEstatusOperation2 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getconsultaListaEstatusOperation2() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params2 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "lista"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params2[0].setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _params2[0].setOption("partName","ArrayOf_xsd_string");
        _params2[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[1].setOption("partName","string");
        _params2[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[2].setOption("partName","string");
        _params2[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params2[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc2 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaListaEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOfArrayOf_xsd_string"), java.lang.String[][].class, true, false, false, false, true, false);
        _returnDesc2.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOfArrayOf_xsd_string");
        _returnDesc2.setOption("partName","ArrayOfArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults2 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _consultaListaEstatusOperation2 = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaListaEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaListaEstatus"), _params2, _returnDesc2, _faults2, "");
        _consultaListaEstatusOperation2.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _consultaListaEstatusOperation2.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaListaEstatusOperation2.setOption("inputName","consultaListaEstatusRequest");
        _consultaListaEstatusOperation2.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaListaEstatusResponse"));
        _consultaListaEstatusOperation2.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _consultaListaEstatusOperation2.setOption("buildNum","cf160638.12");
        _consultaListaEstatusOperation2.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _consultaListaEstatusOperation2.setOption("outputName","consultaListaEstatusResponse");
        _consultaListaEstatusOperation2.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaListaEstatusOperation2.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaListaEstatusRequest"));
        _consultaListaEstatusOperation2.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _consultaListaEstatusOperation2.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _consultaListaEstatusOperation2;

    }

    private int _consultaListaEstatusIndex2 = 2;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getconsultaListaEstatusInvoke2(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_consultaListaEstatusIndex2];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._consultaListaEstatusOperation2);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_consultaListaEstatusIndex2] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getconsultaListaEstatusInvoke2(new java.lang.Object[] {lista, codIdAplicacion, codLang, codOperacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[][]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[][]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[][].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _bloqueoOperation3 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getbloqueoOperation3() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params3 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codMotivo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params3[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params3[0].setOption("partName","string");
        _params3[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params3[1].setOption("partName","string");
        _params3[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params3[2].setOption("partName","string");
        _params3[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params3[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc3 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "BloqueoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc3.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc3.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults3 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _bloqueoOperation3 = new com.ibm.ws.webservices.engine.description.OperationDesc("bloqueo", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Bloqueo"), _params3, _returnDesc3, _faults3, "");
        _bloqueoOperation3.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _bloqueoOperation3.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _bloqueoOperation3.setOption("inputName","BloqueoRequest");
        _bloqueoOperation3.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "BloqueoResponse"));
        _bloqueoOperation3.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _bloqueoOperation3.setOption("buildNum","cf160638.12");
        _bloqueoOperation3.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _bloqueoOperation3.setOption("outputName","BloqueoResponse");
        _bloqueoOperation3.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _bloqueoOperation3.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "BloqueoRequest"));
        _bloqueoOperation3.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _bloqueoOperation3.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _bloqueoOperation3;

    }

    private int _bloqueoIndex3 = 3;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getbloqueoInvoke3(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_bloqueoIndex3];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._bloqueoOperation3);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_bloqueoIndex3] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getbloqueoInvoke3(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codMotivo}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _desBloqueoOperation4 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getdesBloqueoOperation4() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params4 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params4[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params4[0].setOption("partName","string");
        _params4[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params4[1].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc4 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "desBloqueoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc4.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc4.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults4 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _desBloqueoOperation4 = new com.ibm.ws.webservices.engine.description.OperationDesc("desBloqueo", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "desBloqueo"), _params4, _returnDesc4, _faults4, "");
        _desBloqueoOperation4.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _desBloqueoOperation4.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _desBloqueoOperation4.setOption("inputName","desBloqueoRequest");
        _desBloqueoOperation4.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloqueoResponse"));
        _desBloqueoOperation4.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _desBloqueoOperation4.setOption("buildNum","cf160638.12");
        _desBloqueoOperation4.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _desBloqueoOperation4.setOption("outputName","desBloqueoResponse");
        _desBloqueoOperation4.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _desBloqueoOperation4.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloqueoRequest"));
        _desBloqueoOperation4.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _desBloqueoOperation4.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _desBloqueoOperation4;

    }

    private int _desBloqueoIndex4 = 4;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getdesBloqueoInvoke4(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_desBloqueoIndex4];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._desBloqueoOperation4);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_desBloqueoIndex4] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getdesBloqueoInvoke4(new java.lang.Object[] {codCliente, codIdAplicacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _activacionOperation5 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getactivacionOperation5() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params5 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params5[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params5[0].setOption("partName","string");
        _params5[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params5[1].setOption("partName","string");
        _params5[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params5[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc5 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "ActivacionReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc5.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc5.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults5 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _activacionOperation5 = new com.ibm.ws.webservices.engine.description.OperationDesc("activacion", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Activacion"), _params5, _returnDesc5, _faults5, "");
        _activacionOperation5.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _activacionOperation5.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _activacionOperation5.setOption("inputName","ActivacionRequest");
        _activacionOperation5.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ActivacionResponse"));
        _activacionOperation5.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _activacionOperation5.setOption("buildNum","cf160638.12");
        _activacionOperation5.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _activacionOperation5.setOption("outputName","ActivacionResponse");
        _activacionOperation5.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _activacionOperation5.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ActivacionRequest"));
        _activacionOperation5.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _activacionOperation5.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _activacionOperation5;

    }

    private int _activacionIndex5 = 5;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getactivacionInvoke5(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_activacionIndex5];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._activacionOperation5);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_activacionIndex5] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getactivacionInvoke5(new java.lang.Object[] {codCliente, codIdAplicacion, codLang}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _solicitudOperation6 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getsolicitudOperation6() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params6 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codContratoEnlace"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCuentaCargo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codClienteEmpresa"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params6[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[0].setOption("partName","string");
        _params6[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[1].setOption("partName","string");
        _params6[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[2].setOption("partName","string");
        _params6[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[3].setOption("partName","string");
        _params6[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[4].setOption("partName","string");
        _params6[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[5].setOption("partName","string");
        _params6[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params6[6].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc6 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "SolicitudReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc6.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc6.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults6 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _solicitudOperation6 = new com.ibm.ws.webservices.engine.description.OperationDesc("solicitud", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "Solicitud"), _params6, _returnDesc6, _faults6, "");
        _solicitudOperation6.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _solicitudOperation6.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _solicitudOperation6.setOption("inputName","SolicitudRequest");
        _solicitudOperation6.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "SolicitudResponse"));
        _solicitudOperation6.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _solicitudOperation6.setOption("buildNum","cf160638.12");
        _solicitudOperation6.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _solicitudOperation6.setOption("outputName","SolicitudResponse");
        _solicitudOperation6.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _solicitudOperation6.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "SolicitudRequest"));
        _solicitudOperation6.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _solicitudOperation6.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _solicitudOperation6;

    }

    private int _solicitudIndex6 = 6;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getsolicitudInvoke6(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_solicitudIndex6];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._solicitudOperation6);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_solicitudIndex6] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getsolicitudInvoke6(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaEstatusOperation7 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getconsultaEstatusOperation7() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params7 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params7[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params7[0].setOption("partName","string");
        _params7[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params7[1].setOption("partName","string");
        _params7[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params7[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc7 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaEstatusReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc7.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc7.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults7 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _consultaEstatusOperation7 = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaEstatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaEstatus"), _params7, _returnDesc7, _faults7, "");
        _consultaEstatusOperation7.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _consultaEstatusOperation7.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaEstatusOperation7.setOption("inputName","consultaEstatusRequest");
        _consultaEstatusOperation7.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaEstatusResponse"));
        _consultaEstatusOperation7.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _consultaEstatusOperation7.setOption("buildNum","cf160638.12");
        _consultaEstatusOperation7.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _consultaEstatusOperation7.setOption("outputName","consultaEstatusResponse");
        _consultaEstatusOperation7.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaEstatusOperation7.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaEstatusRequest"));
        _consultaEstatusOperation7.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _consultaEstatusOperation7.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _consultaEstatusOperation7;

    }

    private int _consultaEstatusIndex7 = 7;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getconsultaEstatusInvoke7(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_consultaEstatusIndex7];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._consultaEstatusOperation7);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_consultaEstatusIndex7] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getconsultaEstatusInvoke7(new java.lang.Object[] {codCliente, codIdAplicacion, codOperacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _consultaContratoOperation8 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getconsultaContratoOperation8() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params8 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params8[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params8[0].setOption("partName","string");
        _params8[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params8[1].setOption("partName","string");
        _params8[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params8[2].setOption("partName","string");
        _params8[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params8[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc8 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "consultaContratoReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc8.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc8.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults8 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _consultaContratoOperation8 = new com.ibm.ws.webservices.engine.description.OperationDesc("consultaContrato", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "consultaContrato"), _params8, _returnDesc8, _faults8, "");
        _consultaContratoOperation8.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _consultaContratoOperation8.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaContratoOperation8.setOption("inputName","consultaContratoRequest");
        _consultaContratoOperation8.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaContratoResponse"));
        _consultaContratoOperation8.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _consultaContratoOperation8.setOption("buildNum","cf160638.12");
        _consultaContratoOperation8.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _consultaContratoOperation8.setOption("outputName","consultaContratoResponse");
        _consultaContratoOperation8.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _consultaContratoOperation8.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "consultaContratoRequest"));
        _consultaContratoOperation8.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _consultaContratoOperation8.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _consultaContratoOperation8;

    }

    private int _consultaContratoIndex8 = 8;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getconsultaContratoInvoke8(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_consultaContratoIndex8];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._consultaContratoOperation8);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_consultaContratoIndex8] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getconsultaContratoInvoke8(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _activarTokenOperation9 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getactivarTokenOperation9() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params9 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params9[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params9[0].setOption("partName","string");
        _params9[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params9[1].setOption("partName","string");
        _params9[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params9[2].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc9 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "activarTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc9.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc9.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults9 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _activarTokenOperation9 = new com.ibm.ws.webservices.engine.description.OperationDesc("activarToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "activarToken"), _params9, _returnDesc9, _faults9, "");
        _activarTokenOperation9.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _activarTokenOperation9.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _activarTokenOperation9.setOption("inputName","activarTokenRequest");
        _activarTokenOperation9.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "activarTokenResponse"));
        _activarTokenOperation9.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _activarTokenOperation9.setOption("buildNum","cf160638.12");
        _activarTokenOperation9.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _activarTokenOperation9.setOption("outputName","activarTokenResponse");
        _activarTokenOperation9.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _activarTokenOperation9.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "activarTokenRequest"));
        _activarTokenOperation9.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _activarTokenOperation9.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _activarTokenOperation9;

    }

    private int _activarTokenIndex9 = 9;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getactivarTokenInvoke9(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_activarTokenIndex9];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._activarTokenOperation9);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_activarTokenIndex9] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getactivarTokenInvoke9(new java.lang.Object[] {codCliente, codIdAplicacion, codLang}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _bloquearTokenOperation10 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getbloquearTokenOperation10() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params10 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codMotivo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params10[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params10[0].setOption("partName","string");
        _params10[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params10[1].setOption("partName","string");
        _params10[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params10[2].setOption("partName","string");
        _params10[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params10[3].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc10 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "bloquearTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc10.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc10.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults10 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _bloquearTokenOperation10 = new com.ibm.ws.webservices.engine.description.OperationDesc("bloquearToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "bloquearToken"), _params10, _returnDesc10, _faults10, "");
        _bloquearTokenOperation10.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _bloquearTokenOperation10.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _bloquearTokenOperation10.setOption("inputName","bloquearTokenRequest");
        _bloquearTokenOperation10.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "bloquearTokenResponse"));
        _bloquearTokenOperation10.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _bloquearTokenOperation10.setOption("buildNum","cf160638.12");
        _bloquearTokenOperation10.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _bloquearTokenOperation10.setOption("outputName","bloquearTokenResponse");
        _bloquearTokenOperation10.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _bloquearTokenOperation10.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "bloquearTokenRequest"));
        _bloquearTokenOperation10.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _bloquearTokenOperation10.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _bloquearTokenOperation10;

    }

    private int _bloquearTokenIndex10 = 10;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getbloquearTokenInvoke10(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_bloquearTokenIndex10];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._bloquearTokenOperation10);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_bloquearTokenIndex10] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getbloquearTokenInvoke10(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codMotivo}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _desBloquearTokenOperation11 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getdesBloquearTokenOperation11() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params11 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params11[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params11[0].setOption("partName","string");
        _params11[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params11[1].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc11 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "desBloquearTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc11.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc11.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults11 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _desBloquearTokenOperation11 = new com.ibm.ws.webservices.engine.description.OperationDesc("desBloquearToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "desBloquearToken"), _params11, _returnDesc11, _faults11, "");
        _desBloquearTokenOperation11.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _desBloquearTokenOperation11.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _desBloquearTokenOperation11.setOption("inputName","desBloquearTokenRequest");
        _desBloquearTokenOperation11.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloquearTokenResponse"));
        _desBloquearTokenOperation11.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _desBloquearTokenOperation11.setOption("buildNum","cf160638.12");
        _desBloquearTokenOperation11.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _desBloquearTokenOperation11.setOption("outputName","desBloquearTokenResponse");
        _desBloquearTokenOperation11.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _desBloquearTokenOperation11.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "desBloquearTokenRequest"));
        _desBloquearTokenOperation11.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _desBloquearTokenOperation11.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _desBloquearTokenOperation11;

    }

    private int _desBloquearTokenIndex11 = 11;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getdesBloquearTokenInvoke11(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_desBloquearTokenIndex11];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._desBloquearTokenOperation11);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_desBloquearTokenIndex11] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getdesBloquearTokenInvoke11(new java.lang.Object[] {codCliente, codIdAplicacion}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _solicitudTokenOperation12 = null;
    private static com.ibm.ws.webservices.engine.description.OperationDesc _getsolicitudTokenOperation12() {
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params12 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCliente"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codIdAplicacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codLang"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codOperacion"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codContratoEnlace"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codCuentaCargo"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "codClienteEmpresa"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false, false, false, true, false),
          };
        _params12[0].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[0].setOption("partName","string");
        _params12[1].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[1].setOption("partName","string");
        _params12[2].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[2].setOption("partName","string");
        _params12[3].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[3].setOption("partName","string");
        _params12[4].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[4].setOption("partName","string");
        _params12[5].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[5].setOption("partName","string");
        _params12[6].setOption("partQNameString","{http://www.w3.org/2001/XMLSchema}string");
        _params12[6].setOption("partName","string");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc12 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "solicitudTokenReturn"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "ArrayOf_xsd_string"), java.lang.String[].class, true, false, false, false, true, false);
        _returnDesc12.setOption("partQNameString","{" + Global.URL_WS_BLOQ_TOKEN + "}ArrayOf_xsd_string");
        _returnDesc12.setOption("partName","ArrayOf_xsd_string");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults12 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        _solicitudTokenOperation12 = new com.ibm.ws.webservices.engine.description.OperationDesc("solicitudToken", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://ws.token.asteci.net", "solicitudToken"), _params12, _returnDesc12, _faults12, "");
        _solicitudTokenOperation12.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWS"));
        _solicitudTokenOperation12.setOption("inputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _solicitudTokenOperation12.setOption("inputName","solicitudTokenRequest");
        _solicitudTokenOperation12.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "solicitudTokenResponse"));
        _solicitudTokenOperation12.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "CanalesWSService"));
        _solicitudTokenOperation12.setOption("buildNum","cf160638.12");
        _solicitudTokenOperation12.setOption("targetNamespace",Global.URL_WS_BLOQ_TOKEN);
        _solicitudTokenOperation12.setOption("outputName","solicitudTokenResponse");
        _solicitudTokenOperation12.setOption("outputEncodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
        _solicitudTokenOperation12.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName(Global.URL_WS_BLOQ_TOKEN, "solicitudTokenRequest"));
        _solicitudTokenOperation12.setUse(com.ibm.ws.webservices.engine.enumtype.Use.ENCODED);
        _solicitudTokenOperation12.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.RPC);
        return _solicitudTokenOperation12;

    }

    private int _solicitudTokenIndex12 = 12;
    private synchronized com.ibm.ws.webservices.engine.client.Stub.Invoke _getsolicitudTokenInvoke12(Object[] parameters) throws com.ibm.ws.webservices.engine.WebServicesFault  {
        com.ibm.ws.webservices.engine.MessageContext mc = super.messageContexts[_solicitudTokenIndex12];
        if (mc == null) {
            mc = new com.ibm.ws.webservices.engine.MessageContext(super.engine);
            mc.setOperation(CanalesWSSoapBindingStub._solicitudTokenOperation12);
            mc.setUseSOAPAction(true);
            mc.setSOAPActionURI("");
            super.primeMessageContext(mc);
            super.messageContexts[_solicitudTokenIndex12] = mc;
        }
        try {
            mc = (com.ibm.ws.webservices.engine.MessageContext) mc.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw com.ibm.ws.webservices.engine.WebServicesFault.makeFault(cnse);
        }
        return new com.ibm.ws.webservices.engine.client.Stub.Invoke(connection, mc, parameters);
    }

    public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new com.ibm.ws.webservices.engine.NoEndPointException();
        }
        java.util.Vector _resp = null;
        try {
            _resp = _getsolicitudTokenInvoke12(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa}).invoke();

        } catch (com.ibm.ws.webservices.engine.WebServicesFault wsf) {
            Exception e = wsf.getUserException();
            throw wsf;
        }
        try {
            return (java.lang.String[]) ((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue();
        } catch (java.lang.Exception _exception) {
            return (java.lang.String[]) super.convert(((com.ibm.ws.webservices.engine.xmlsoap.ext.ParamValue) _resp.get(0)).getValue(), java.lang.String[].class);
        }
    }

    private static void _staticInit() {
        _bloqueoOperation3 = _getbloqueoOperation3();
        _estatusOperation0 = _getestatusOperation0();
        _consultaContratoOperation8 = _getconsultaContratoOperation8();
        _contratoOperation1 = _getcontratoOperation1();
        _desBloqueoOperation4 = _getdesBloqueoOperation4();
        _desBloquearTokenOperation11 = _getdesBloquearTokenOperation11();
        _consultaListaEstatusOperation2 = _getconsultaListaEstatusOperation2();
        _consultaEstatusOperation7 = _getconsultaEstatusOperation7();
        _solicitudTokenOperation12 = _getsolicitudTokenOperation12();
        _activacionOperation5 = _getactivacionOperation5();
        _solicitudOperation6 = _getsolicitudOperation6();
        _bloquearTokenOperation10 = _getbloquearTokenOperation10();
        _activarTokenOperation9 = _getactivarTokenOperation9();
    }

    static {
       _staticInit();
    }
}