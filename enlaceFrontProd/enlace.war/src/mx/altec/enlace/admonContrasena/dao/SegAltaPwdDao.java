/*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripción="REGISTRO DE NUEVA CONTRASEÑA">*/
package mx.altec.enlace.admonContrasena.dao;


import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.altec.enlace.admonContrasena.vo.AtributosUsuarioVO;
import mx.altec.enlace.utilerias.EIGlobal;



public class SegAltaPwdDao extends ConnectionHandlerDAO{



	public SegAltaPwdDao() {
       super();
   }

  public SegAltaPwdDao(ConnectionHandler handler) {
        super(handler);
  }

	public String verificaContrato(String contrato){
		String status = "";

		try{
			Connection conn = getConnection();
			PreparedStatement ps =
				conn.prepareStatement(
						"SELECT  count(1) as existe FROM  nucl_cuentas WHERE num_cuenta = ?");
			ps.setString(1, contrato);

			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				long num = rs.getLong("existe");
				if (num==0){
					status="Contrato Enlace no existe";
				}
			}else{
				status="Contrato Enlace no existe";
			}
			ps.close();
			closeConnection();
		}catch (Exception e){
			status="Error al verificar existe contrato enlace";
		}

		return status;
	}

		public String verificaExisteUsuario(String usuario){
			String status = "";

			try{
				Connection conn = getConnection();
				PreparedStatement ps =
					conn.prepareStatement(
							"SELECT  count(1) as existe FROM  segu_usuarios WHERE cve_usuario = ?");
				ps.setString(1, usuario);

				ResultSet rs = ps.executeQuery();
				if (rs.next()){
					long num = rs.getLong("existe");
					if (num==0){
						status="Usuario no existe en SANTANDER";
					}
				}else{
					status="Usuario no existe en SANTANDER";
				}
				ps.close();
				closeConnection();
			}catch (Exception e){
				status="EError al verificar existe usuario en Santander";
			}


		return status;

	}


		public AtributosUsuarioVO obtenerAtributos(String usuario){

			AtributosUsuarioVO vo = null;

			try{
				Connection conn= getConnection();

				EIGlobal.mensajePorTrace("OBTENER ATRIBUTOS DE = " + usuario, EIGlobal.NivelLog.INFO);


				Statement st = conn.createStatement();



				StringBuffer sql = new StringBuffer("");

				sql.append("select NVL(FCH_ULT_MOD_NIP,sysdate) as ultimoModPwd,")
				.append(" NVL(CONTRASENA,' ') as contrasena,  ")
				.append("NVL(FCH_ULTIMO_LOGIN,sysdate) as ultimoLogin, FCH_EXP_NIP, LOGON_FALLIDOS,")
				.append("NUM_MAX_LOGONS, NUM_CUENTA, trunc(FCH_EXP_NIP,'dd') - trunc(SYSDATE,'dd') as dias, ")
				.append("NVL(contrasena28, ' ') as contra28, " )
				.append("NVL(activo, 'A') as activo FROM EWEB_VIGNIP ")
				.append("WHERE CVE_USR ='" )
				.append(usuario)
				.append("'");



				ResultSet rs = st.executeQuery(sql.toString());
				EIGlobal.mensajePorTrace("QUERY EJECUTADO BIEN" , EIGlobal.NivelLog.INFO);
				vo= new AtributosUsuarioVO();
				if (rs.next()){
					vo.setSql_FchUltModPWD(rs.getDate("ultimoModPwd"));
					vo.setSql_Contrasena(rs.getString("contrasena"));
					vo.setSql_FchUltimoLogin(rs.getDate("ultimoLogin"));
					vo.setSql_FchExpPWD(rs.getDate("FCH_EXP_NIP"));
					vo.setSql_LogonFallidos(rs.getInt("LOGON_FALLIDOS"));
					vo.setSql_NumMaxLogons(rs.getInt("NUM_MAX_LOGONS"));
					vo.setSql_ContratoActual(rs.getString("NUM_CUENTA"));
					vo.setSql_DiasParaExpirar(rs.getInt("dias"));
					vo.setSql_Contrasena28(rs.getString("contra28"));
					vo.setSql_ClienteActivo(rs.getString("activo"));
				}
				/*else{
					return null;
				}*/
				st.close();
				closeConnection();
			}catch (Exception e){
				try{
					closeConnection();
				}catch(Exception ex){
					ex.printStackTrace();
				}
				e.printStackTrace();
				return null;
			}

			EIGlobal.mensajePorTrace("VO = " + vo, EIGlobal.NivelLog.INFO);
		return vo;
		}

		public void eliminaUsuario(String usuario) throws SQLException, Exception{



				Connection conn= getConnection();

				Statement st = conn.createStatement();
				st.executeUpdate("delete FROM  eweb_vignip WHERE CVE_USR ='" + usuario + "'");
				EIGlobal.mensajePorTrace("EJECUTAR DELETE", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("delete FROM  eweb_vignip WHERE CVE_USR ='" + usuario + "'", EIGlobal.NivelLog.INFO);

				st.close();
				closeConnection();

		}

		public void registraUsuario(String usuario,String pwdEncriptado, String pwdAntEncriptado,
				AtributosUsuarioVO vo) throws SQLException, Exception{
				Connection conn= getConnection();
				Statement st = conn.createStatement();

				StringBuffer sql = new StringBuffer("");

				sql.append("INSERT INTO eweb_vignip (cve_usr, dias_vig_nip, fch_ult_mod_nip, fch_exp_nip, ")
				   .append("contrasena, fch_ultimo_login, logon_fallidos,")
				   .append("num_max_logons, num_cuenta, contrasena28, activo) values (")
				   .append("'")
				   .append(usuario)
				   .append("',")
				   .append("25,")
				   .append("SYSDATE,")
				   .append("SYSDATE+25,")
				   .append("'")
				   .append(pwdAntEncriptado)
				   .append("',")
				   .append("SYSDATE,")
				   .append("0,")
				   .append(vo.getSql_NumMaxLogons())
				   .append(",")
				   .append("0,")
				   .append("'")
				   .append(pwdEncriptado)
				   .append("',")
				   .append("'I')");

				st.executeUpdate(sql.toString());

				EIGlobal.mensajePorTrace("EJECUTAR INSERT", EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(sql.toString(), EIGlobal.NivelLog.INFO);

				st.close();
				closeConnection();

		}



		private String formatDateToString(
				Date fecha,
				String formatConstant) {
				String strFecha = "";

				if (fecha != null) {
					SimpleDateFormat formatter = new SimpleDateFormat(formatConstant);
					strFecha = formatter.format(fecha);
				}
				return strFecha;
			}


		public List contrasenasAnt(String usuario){
			List contrasenas = new ArrayList();

			try{
				Connection conn= getConnection();
				Statement st = conn.createStatement();
				StringBuffer sql = new StringBuffer("");

				sql.append("select  contrasena from eweb_nip_enla_his \n")
				.append(" where cve_usr = '" + usuario  + "' and (num_nip = (select nvl(max(num_nip),0) \n")
                                                                     .append("from eweb_nip_enla_his \n")
                                                                     .append("where cve_usr = '" + usuario + "') \n")
                                              .append(" or ( num_nip = (select nvl(max(num_nip) -1,0) \n")
                                                               .append(" from eweb_nip_enla_his \n")
                                                               .append(" where cve_usr = '" + usuario + "') \n")
                                                   .append(") \n")
                                            .append(")");
				ResultSet rs = st.executeQuery(sql.toString());
				while (rs.next()){
					contrasenas.add(rs.getString("contrasena"));
				}



				sql = new StringBuffer("");
				sql.append("select contrasena28 from eweb_vignip ")
				.append("where cve_usr = '" + usuario + "'");
				rs = st.executeQuery(sql.toString());
				while (rs.next()){
					contrasenas.add(rs.getString("contrasena28"));
				}
				st.close();
				closeConnection();


			}catch (Exception e){
				try{
					closeConnection();
				}catch(Exception ex){
					ex.printStackTrace();
				}
				e.printStackTrace();
			}





			return contrasenas;
		}

		public void insertaHistoricoContrasenas(String usuario, String contrato){

			try{
				Connection conn= getConnection();
				Statement st = conn.createStatement();
				StringBuffer sql = new StringBuffer("");

				java.sql.Date fecha=null;
				String contrasena28=null;
				boolean existeAnt = false;

				sql.append("select  fch_ult_mod_nip, contrasena28 from eweb_vignip \n")
				.append(" where cve_usr = '" + usuario  + "'");
				ResultSet rs = st.executeQuery(sql.toString());
				if (rs.next()){
					existeAnt=true;
					fecha = rs.getDate("fch_ult_mod_nip");
					contrasena28 = rs.getString("contrasena28");
				}


				if (existeAnt){
					sql = new StringBuffer("");
					sql.append("select nvl(max(num_nip),0)+1 as max from eweb_nip_enla_his where cve_usr = '" + usuario + "'");

					rs = st.executeQuery(sql.toString());
					int siguiente=0;
					if (rs.next()){
						siguiente= rs.getInt("max");
					}


					sql = new StringBuffer("");

					sql.append("insert into eweb_nip_enla_his values ('" + usuario + "'," + siguiente + "," +
									"'" + contrasena28 + "', SYSDATE, SYSDATE, '" +contrato + "')" );

					st.executeUpdate(sql.toString());

				}




			}catch (Exception e){
				try{
					closeConnection();
				}catch(Exception ex){
					ex.printStackTrace();
				}
				e.printStackTrace();
			}


		}

}
/*</VC>*/