/*<VC proyecto="200710001" autor="SCG" fecha="29/04/2007" descripción="Activacion de contraseña">*/
package mx.altec.enlace.admonContrasena.dao;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mx.altec.enlace.admonContrasena.vo.UtilVO;


public class ActivarContrasenaDAO extends ConnectionHandlerImpl {

	/**
	 * Codigo del cliente
	 */
	protected String codigoCliente;

	/**
	 * Folio de seguridad de la solicitud
	 */
	protected String folioSolicitud;

	public ActivarContrasenaDAO() {
		super();
	}

	public ActivarContrasenaDAO(String codigoCliente, String folioSolicitud) {
		super();
		this.codigoCliente = codigoCliente;
		this.folioSolicitud = folioSolicitud;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getFolioSolicitud() {
		return folioSolicitud;
	}

	public void setFolioSolicitud(String folioSolicitud) {
		this.folioSolicitud = folioSolicitud;
	}

	/**
	 * Activa la contrseña del cliente
	 *
	 * @return true en caso exitoso. false en caso de error
	 * @throws SQLException
	 * @throws Exception
	 */
	public boolean activarContrasena() throws SQLException, Exception {
		boolean r = false;
		// ConnectionHandlerImpl conn = null;
		try {

			int rengAfectados = 0;
			Statement s = getConnection().createStatement();
			rengAfectados = s.executeUpdate("UPDATE EWEB_VIGNIP SET ACTIVO = 'A', FCH_ULT_MOD_NIP=SYSDATE, FCH_EXP_NIP = SYSDATE+25 WHERE CVE_USR = '" + codigoCliente  + "'");
			getConnection().commit();
			if(rengAfectados == 1){
				//se actualizo un registro
				r = true;
			}else{
				//se actualizo cero o mas de un registro
				r = false;
			}
		} catch (SQLException se) {
			throw se;
		} catch (Exception e) {
			throw e;
		} finally {
			// conn.closeConnection();
			closeConnection();
		}
		return r;
	}

	/**
	 * Verifica que exista el folio de seguridad en la bd y la compara con el
	 * valor dado
	 *
	 * @return true en caso exitoso. false en caso de error
	 * @throws SQLException
	 * @throws Exception
	 */
	public boolean esFolioSolicitudOk() throws SQLException, Exception {
		boolean r = false;
		ConnectionHandlerImpl conn = null;
		try {
			conn = new ConnectionHandlerImpl();
			ResultSet rs = null;

			Statement s = conn.getConnection().createStatement();
			rs = s.executeQuery
				("SELECT FOLIOSOLICITUD FROM NIPM_SOLICITUDES " +
				 "WHERE CODIGOCLIENTE = '" + UtilVO.convierteUsr7a8(codigoCliente) + "' AND FOLIOSOLICITUD = '" + folioSolicitud +
				 "' AND ESTADO = 'A'");
			if (rs.next()) {
				r = true;
			}
		} catch (SQLException se) {
			throw se;
		} catch (Exception e) {
			throw e;
		} finally {
			conn.closeConnection();
		}
		return r;
	}
}

/* </VC> */
