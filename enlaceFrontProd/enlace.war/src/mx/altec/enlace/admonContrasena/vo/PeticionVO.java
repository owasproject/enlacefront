/**

  */

package mx.altec.enlace.admonContrasena.vo;

public class PeticionVO {

	private String folioPeticion;
	private String codigoCliente;
	private String materno;
	private String paterno;
	private String nombre;
	private String direccion;
	private String email;
	private String areaCode;
	private String telefono;
	private String areaCodeAlternativa;
	private String telefonoAlternativo;
	private String contrasena;
	private String fechaPeticion;
	private String estado;

	public PeticionVO()
	{
	     folioPeticion="";
	     codigoCliente="";
	     materno="";
	     paterno="";
	     nombre="";
	     direccion="";
	     email="";
	     areaCode="";
	     telefono="";
	     areaCodeAlternativa="";
	     telefonoAlternativo="";
	     contrasena="";
	     fechaPeticion="";
	     estado="";
	}

	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getCodigoCliente() {
		return codigoCliente;
	}
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFechaPeticion() {
		return fechaPeticion;
	}
	public void setFechaPeticion(String fechaPeticion) {
		this.fechaPeticion = fechaPeticion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getAreaCodeAlternativa() {
		return areaCodeAlternativa;
	}
	public void setAreaCodeAlternativa(String areaCodeAlternativa) {
		this.areaCodeAlternativa = areaCodeAlternativa;
	}
	public String getTelefonoAlternativo() {
		return telefonoAlternativo;
	}
	public void setTelefonoAlternativo(String telefonoAlternativo) {
		this.telefonoAlternativo = telefonoAlternativo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String dirrecion) {
		this.direccion = dirrecion;
	}
	public String getFolioPeticion() {
		return folioPeticion;
	}
	public void setFolioPeticion(String folioPeticion) {
		this.folioPeticion = folioPeticion;
	}
}
