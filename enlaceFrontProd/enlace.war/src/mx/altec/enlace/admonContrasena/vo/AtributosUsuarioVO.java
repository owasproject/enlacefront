package mx.altec.enlace.admonContrasena.vo;

import java.util.Date;

public class AtributosUsuarioVO {

	int testVar = 0;
	Date sql_FchUltModPWD;
	Date sql_FchExpPWD;
	String sql_Contrasena;
	Date sql_FchUltimoLogin;
	int  sql_LogonFallidos;
	int  sql_NumMaxLogons=3;
	String sql_ContratoActual;
	int  sql_DiasParaExpirar=0;
	int  shortNull = 0;
	String sql_Contrasena28;
	String sql_ClienteActivo;
	public int getShortNull() {
		return shortNull;
	}
	public void setShortNull(int shortNull) {
		this.shortNull = shortNull;
	}
	public String getSql_ClienteActivo() {
		return sql_ClienteActivo;
	}
	public void setSql_ClienteActivo(String sql_ClienteActivo) {
		this.sql_ClienteActivo = sql_ClienteActivo;
	}
	public String getSql_Contrasena() {
		return sql_Contrasena;
	}
	public void setSql_Contrasena(String sql_Contrasena) {
		this.sql_Contrasena = sql_Contrasena;
	}
	public String getSql_Contrasena28() {
		return sql_Contrasena28;
	}
	public void setSql_Contrasena28(String sql_Contrasena28) {
		this.sql_Contrasena28 = sql_Contrasena28;
	}
	public String getSql_ContratoActual() {
		return sql_ContratoActual;
	}
	public void setSql_ContratoActual(String sql_ContratoActual) {
		this.sql_ContratoActual = sql_ContratoActual;
	}
	public int getSql_DiasParaExpirar() {
		return sql_DiasParaExpirar;
	}
	public void setSql_DiasParaExpirar(int sql_DiasParaExpirar) {
		this.sql_DiasParaExpirar = sql_DiasParaExpirar;
	}
	public Date getSql_FchExpPWD() {
		return sql_FchExpPWD;
	}
	public void setSql_FchExpPWD(Date sql_FchExpPWD) {
		this.sql_FchExpPWD = sql_FchExpPWD;
	}
	public Date getSql_FchUltimoLogin() {
		return sql_FchUltimoLogin;
	}
	public void setSql_FchUltimoLogin(Date sql_FchUltimoLogin) {
		this.sql_FchUltimoLogin = sql_FchUltimoLogin;
	}
	public Date getSql_FchUltModPWD() {
		return sql_FchUltModPWD;
	}
	public void setSql_FchUltModPWD(Date sql_FchUltModPWD) {
		this.sql_FchUltModPWD = sql_FchUltModPWD;
	}
	public int getSql_LogonFallidos() {
		return sql_LogonFallidos;
	}
	public void setSql_LogonFallidos(int sql_LogonFallidos) {
		this.sql_LogonFallidos = sql_LogonFallidos;
	}
	public int getSql_NumMaxLogons() {
		return sql_NumMaxLogons;
	}
	public void setSql_NumMaxLogons(int sql_NumMaxLogons) {
		this.sql_NumMaxLogons = sql_NumMaxLogons;
	}

}