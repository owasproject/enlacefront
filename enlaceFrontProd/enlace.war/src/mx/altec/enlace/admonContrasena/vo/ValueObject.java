package mx.altec.enlace.admonContrasena.vo;


public class ValueObject {
	private int id;
	private String mensaje;
	private String usuario;
	private String folioSolicitud;
	private String estadoSolicitud;
	private String descRechacho;
	private String nombreUsuario;
	private String aPaterno;
	private String aMaterno;

	public String getDescRechacho() {
		return descRechacho;
	}
	public void setDescRechacho(String descRechacho) {
		this.descRechacho = descRechacho;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}
	public void setEstadoSolicitud(String estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}
	public String getFolioSolicitud() {
		return folioSolicitud;
	}
	public String getAMaterno() {
		return aMaterno;
	}
	public void setAMaterno(String materno) {
		aMaterno = materno;
	}
	public String getAPaterno() {
		return aPaterno;
	}
	public void setAPaterno(String paterno) {
		aPaterno = paterno;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public void setFolioSolicitud(String folioSolicitud) {
		this.folioSolicitud = folioSolicitud;
	}
}
