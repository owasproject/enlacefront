package mx.altec.enlace.admonContrasena.vo;


public class PeticionTO {
	private int FolioPeticion ;
	private String CodigoCliente;
	private String NombreCliente;
	private String paterno;
	private String materno;
	private String fechaPeticion;
	private String areaCode;
	private String areaCodeAlter;
	private String telefono;
	private String telefonoII;
	private String email;
	private String direccionCliente;

	/** Constructor con ambos argumentos.
	 *
	 * @param tipoFolio Tipo de folio para este registro.
	 * @param folio Folio para este registro.
	 */
	public PeticionTO(int FolioPeticion, String CodigoCliente, String NombreCliente, String Canal, String EstaSol, String FechaStatus, String ContratoEnlace, String CodigoClienteEmpresa, String SucursalAsignada, Integer NumeroSerie , String Color ,  String TipoSolicitud, String Documentacion, String ClaveEjecutivo, String NombreEjecutivo, String FechaSolIni, String FechaSolFin, String CuentaCargo)
	{
		super();
		this.FolioPeticion = FolioPeticion;
		this.CodigoCliente = CodigoCliente;
		this.NombreCliente = NombreCliente;
    }






	/** Constructor vacio. Inicializa los valores a null.
	 *
	 *
	 */
	public PeticionTO( )
	{
		super();
	}

	/**
	 * @return El  FolioSeguridad de este registro
	 */
	public int getFolioPeticion() {
		return FolioPeticion;
	}

	/**
	 * @param Coloca el FolioSeguridad de este registro
	 */
	public void setFolioSeguridad(int FolioPeticion) {
		this.FolioPeticion = FolioPeticion;
	}

	/**
	 * @return El tipo de CodigoCliente de este registro
	 */
	public String getCodigoCliente() {
		return CodigoCliente;
	}

	/**
	 * @param Actualiza el tipo de CodigoCliente de este registro.
	 */
	public void setCodigoCliente(String CodigoCliente) {
		this.CodigoCliente = CodigoCliente;
	}

	/**
	 * @return El tipo de NombreCliente de este registro
	 */
	public String getNombreCliente() {
		return NombreCliente;
	}

	/**
	 * @param Actualiza el tipo de NombreCliente de este registro.
	 */
	public void setNombreCliente(String NombreCliente) {
		this.NombreCliente = NombreCliente;
	}

	/**
	 * @return El tipo de Canal de este registro
	 */
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String NombreCliente) {
		this.paterno = NombreCliente;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String NombreCliente) {
		this.materno = NombreCliente;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String NombreCliente) {
		this.areaCode = NombreCliente;
	}

	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String NombreCliente) {
		this.telefono = NombreCliente;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String NombreCliente) {
		this.email = NombreCliente;
	}

	public String getDireccionCliente() {
		return direccionCliente;
	}

	public void setDireccionCliente(String NombreCliente) {
		this.direccionCliente = NombreCliente;
	}

	public String getFechaPeticion() {
		return fechaPeticion;
	}

	public void setFechaPeticion(String NombreCliente) {
		this.fechaPeticion = NombreCliente;
	}

	public String getTelefonoAlternativo() {
		return telefonoII;
	}

	public void setTelefonoAlternativo(String NombreCliente) {
		this.telefonoII = NombreCliente;
	}

	public String getAreaCodeAlter() {
		return areaCodeAlter;
	}

	public void setAreaCodeAlter(String NombreCliente) {
		this.areaCodeAlter = NombreCliente;
	}



}
