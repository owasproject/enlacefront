/*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripción="REGISTRO DE NUEVA CONTRASEÑA">*/

package mx.altec.enlace.admonContrasena.command;

import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.List;

import mx.altec.enlace.admonContrasena.dao.DatosPeticionDAO;
import mx.altec.enlace.admonContrasena.dao.DatosSolicitudDAO;
import mx.altec.enlace.admonContrasena.dao.SegAltaPwdDao;
import mx.altec.enlace.admonContrasena.dao.TransactionHandler;
import mx.altec.enlace.admonContrasena.vo.AtributosUsuarioVO;
import mx.altec.enlace.admonContrasena.vo.PeticionVO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.bo.Base64;
import mx.altec.enlace.utilerias.EIGlobal;


public class SegAltaPwd {

	String pwdNuevo;
	String usuario;
	String contrato;
	String pwdEncriptado;
	String pwdAntEncriptado;

	AtributosUsuarioVO atributos=null;


	private SegAltaPwdDao dao=null;
	private PeticionVO peticion;
	private DatosSolicitudDAO solicitudDAO;
	private DatosPeticionDAO peticionDAO;



	public PeticionVO getPeticion() {
		return peticion;
	}
	public void setPeticion(PeticionVO peticion) {
		this.peticion = peticion;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getPwdNuevo() {
		return pwdNuevo;
	}
	public void setPwdNuevo(String pwdNuevo) {
		this.pwdNuevo = pwdNuevo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public ValueObject altaPwd(){


		TransactionHandler handler = new TransactionHandler();
		boolean insertOk;
		ValueObject result = new ValueObject();

		String folioSolicitud="";
		String folioPeticion="";

		dao = new SegAltaPwdDao(handler);
		solicitudDAO = new DatosSolicitudDAO(handler);
		peticionDAO = new DatosPeticionDAO(handler);

		String status = "";


		try{
		EIGlobal.mensajePorTrace("ENTRA A SEGALTAPWD", EIGlobal.NivelLog.INFO);
		handler.beginTransaction();

		status = sql_obtenAtributosUsuario();
		if (!status.equals("")){
			result.setId(1);
			result.setMensaje(status);

		}

		EIGlobal.mensajePorTrace("DESPUES DE OBTEN ATRIBUTOS USUARIO", EIGlobal.NivelLog.INFO);
		sql_registraUsuarioAlta();
		EIGlobal.mensajePorTrace("DESPUES DE REGISTRA USUARIOS ALTA", EIGlobal.NivelLog.INFO);

		/*
		 * METODOS DE DAR DE ALTA DE LA SOLICITUD
		 */
		/*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripción="REGISTRO DE NUEVA CONTRASEÑA">*/

		EIGlobal.mensajePorTrace("Insertando peticion.... ", EIGlobal.NivelLog.INFO);
		insertOk = peticionDAO.insertaPeticion(peticion);
		// si se inserto la peticion realizamos los demas pasos

		if(insertOk)
		{
			folioPeticion = peticionDAO.getFolioPeticion(peticion);
			peticion.setFolioPeticion(folioPeticion);
			EIGlobal.mensajePorTrace("...Inserto peticion. folioPeticion: " + folioPeticion, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("Veficando solicitudes existentes", EIGlobal.NivelLog.INFO);
			if(solicitudDAO.existeSolicitudCliente(peticion))
			{
				EIGlobal.mensajePorTrace("Existe solicitud del cliente <"+peticion.getCodigoCliente()+">Cambiando el estado de las solicitudes a bloqueado", EIGlobal.NivelLog.INFO);
				solicitudDAO.bloqueaSolicitudes(peticion);
			}

			EIGlobal.mensajePorTrace("Insertando solicitud.... ", EIGlobal.NivelLog.INFO);
			insertOk = solicitudDAO.insertaSolicitud(peticion);
			//si se inserto la solicitud correctamente

		    //obtenemos el folio de la solicitud; ??
		   	folioSolicitud = solicitudDAO.getFolioSolicitud(peticion);
		   	EIGlobal.mensajePorTrace("...Inserto solicitud. FolioSolicitud: " + folioSolicitud, EIGlobal.NivelLog.INFO);
		   	//insertamos en el historico de solicitudes
		   	EIGlobal.mensajePorTrace("Insertando el historico...", EIGlobal.NivelLog.INFO);

		   	insertOk=solicitudDAO.insertaHistorico(folioSolicitud,peticion);

		    //si se inserto el registro del historico
		    EIGlobal.mensajePorTrace("Inserto el historico...", EIGlobal.NivelLog.INFO);
		    EIGlobal.mensajePorTrace("Insertando bitacora...", EIGlobal.NivelLog.INFO);
		    insertOk=solicitudDAO.InsertaBitacora(folioSolicitud,peticion.getEstado());

		    EIGlobal.mensajePorTrace("Inserto la bitacora", EIGlobal.NivelLog.INFO);
		    result.setId(0);
		   	result.setMensaje(folioSolicitud);
		    EIGlobal.mensajePorTrace("Registro Insertado", EIGlobal.NivelLog.INFO);
		}
		/*</VC> */

		handler.endTransaction();

		}catch (SQLException se){
			se.printStackTrace();
				try{
					handler.rollbackTransaction();
				}catch (Exception rollEx){
					rollEx.printStackTrace();
				}
				result.setId(1);
				result.setMensaje("Error SQL:"+se.getMessage());
		}catch (Exception e){
			e.printStackTrace();
				try{
					handler.rollbackTransaction();
				}catch (Exception rollEx){
					rollEx.printStackTrace();
				}
				result.setId(1);
				result.setMensaje("Error SQL:"+e.getMessage());
		}finally{

		}
		return result;
	}


	private void sql_registraUsuarioAlta()throws SQLException, Exception{
		dao.insertaHistoricoContrasenas(usuario, contrato);
		dao.eliminaUsuario(usuario);
		dao.registraUsuario(usuario,pwdEncriptado, pwdAntEncriptado, atributos);
	}

	private String sql_obtenAtributosUsuario(){
		String status = "";
		 atributos = dao.obtenerAtributos(usuario);
		if (atributos==null){
			return "Usuario no Registrado en Nuevo Esquema";
		}
		return status;
	}



	public String encriptaPwd(String usr, String pwd, int len){
	    EIGlobal.mensajePorTrace("Funcion encriptaPwd()", EIGlobal.NivelLog.INFO);
		String pwdEncript = "";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			String amessage = usr + pwd;
			byte [] hash = md.digest(amessage.getBytes());
			String a = new String(hash);

			pwdEncript = Base64.encodeBytes(hash, 0, len);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return pwdEncript;
	}


	public String getPwdAntEncriptado() {
		return pwdAntEncriptado;
	}
	public void setPwdAntEncriptado(String pwdAntEncriptado) {
		this.pwdAntEncriptado = pwdAntEncriptado;
	}
	public String getPwdEncriptado() {
		return pwdEncriptado;
	}
	public void setPwdEncriptado(String pwdEncriptado) {
		this.pwdEncriptado = pwdEncriptado;
	}


	public boolean checarHistorial(String usuario, String password){

		/*<VC proyecto="200710001" autor="AHO" fecha="29/05/2007" descripción="HISTORICO DE CONTRASEÑAS">*/

		EIGlobal.mensajePorTrace("CHECAR EL HISTORIAL", EIGlobal.NivelLog.DEBUG);
		dao = new SegAltaPwdDao();
		List contrasenasAnt = dao.contrasenasAnt(usuario);
		//EIGlobal.mensajePorTrace("PASSWORD A INSERTAR: " + password, EIGlobal.NivelLog.DEBUG);


			for (int i=0; i<contrasenasAnt.size(); i++){
				//EIGlobal.mensajePorTrace("CONTRASEÑA : " + (String)contrasenasAnt.get(i), EIGlobal.NivelLog.DEBUG);
				if (password.equals((String)contrasenasAnt.get(i))){

					return false;
				}

			}

		return true;

		/*</VC>*/


	}

}
/*</VC>*/