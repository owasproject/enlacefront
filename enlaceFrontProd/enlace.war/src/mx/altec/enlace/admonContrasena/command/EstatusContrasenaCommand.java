/**
 * Nombre: EstatusContrasenaCommand.java
 * Descripcion: Clase que gestiona la consulta el estatus de la contrase�a de
 * 				un cliente
 * Fecha:	VC JGR 27/04/2007
 */

package mx.altec.enlace.admonContrasena.command;

import java.sql.SQLException;

import mx.altec.enlace.admonContrasena.dao.EstatusContrasenaDAO;
import mx.altec.enlace.admonContrasena.vo.UtilVO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.utilerias.EIGlobal;


public class EstatusContrasenaCommand {

	public ValueObject estatusContrasena(String codigoCliente){
		EstatusContrasenaDAO estatusDAO = new EstatusContrasenaDAO();
		ValueObject regresaVO = new ValueObject();

		try{

			//1.Consulta el estatus de la contrase�a
			regresaVO = estatusDAO.consultaStatus(codigoCliente);

			//2.Consulta el estatus
			//regresaVO = estatusDAO.consultaUsuario(regresaVO, codigoCliente);
			regresaVO = estatusDAO.consultaDatosUsuario(
					regresaVO, codigoCliente);

			regresaVO = estatusDAO.consultaSolicitud(regresaVO,
					codigoCliente);

			regresaVO.setEstadoSolicitud(
					UtilVO.estado(regresaVO.getEstadoSolicitud().charAt(0)));

			EIGlobal.mensajePorTrace(
					"EstatusContrasenaCommand.estatusContrasena->1", EIGlobal.NivelLog.DEBUG);

		}catch(SQLException sqlEx){
			EIGlobal.mensajePorTrace("EstatusContrasenaCommand." +
					"estatusContrasena.SQLException-> " + sqlEx.toString(), EIGlobal.NivelLog.DEBUG);
			sqlEx.printStackTrace();

			regresaVO.setId(3);
			regresaVO.setMensaje(
			"cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
			return regresaVO;
		}catch(Exception e){
			EIGlobal.mensajePorTrace("EstatusContrasenaCommand." +
					"estatusContrasena.Exception-> " + e.toString(), EIGlobal.NivelLog.DEBUG);
			e.printStackTrace();

			regresaVO.setId(3);
			regresaVO.setMensaje(
			"cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
			return regresaVO;
		}
		return regresaVO;
	}


	public ValueObject consultarContrato(String codCliente){
		ValueObject vo = new ValueObject();
		vo.setId(0);
		vo.setMensaje("");
		EstatusContrasenaDAO estatusDAO = new EstatusContrasenaDAO();
		try{
			vo = estatusDAO.consultarContrato(codCliente);
		}catch(SQLException se){
			 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al validar obtener la contrasena\", 3);");
			 vo.setId(1);
			 se.printStackTrace();

		 }catch(Exception ee){
			 vo.setId(1);
			 vo.setMensaje("cuadroDialogo(\"Excepci�n general al obtener la contrasena\", 3);");
			 ee.printStackTrace();

	 }
		return vo;
	}


	public ValueObject consultarRazon(String contrato){
		ValueObject vo = new ValueObject();
		vo.setId(0);
		vo.setMensaje("");
		EstatusContrasenaDAO estatusDAO = new EstatusContrasenaDAO();
		try{
			vo = estatusDAO.consultarRazon(contrato);
		}catch(SQLException se){
			 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al validar obtener la razon social\", 3);");
			 vo.setId(1);
			 se.printStackTrace();

		 }catch(Exception ee){
			 vo.setId(1);
			 vo.setMensaje("cuadroDialogo(\"Excepci�n general al obtener la razon social\", 3);");
			 ee.printStackTrace();

	 }
		return vo;
	}





}