/*<VC proyecto="200710001" autor="AHO" fecha="29/04/2007" descripci�n="DESBLOQUEO DE CONTRASE�A">*/
package mx.altec.enlace.admonContrasena.command;

import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import javax.servlet.ServletException;

import mx.altec.enlace.admonContrasena.dao.DesbloquearContrasenaDAO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.bo.Base64;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.utilerias.IEnlace;




public class DesbloquearContrasenaCommand {
	private DesbloquearContrasenaDAO dao=null;
	private com.kivasoft.IContext context;

	public com.kivasoft.IContext getContext() {
		return context;
	}
	public void setContext(com.kivasoft.IContext context) {
		this.context = context;
	}



	public ValueObject validar(String usuario, String contrasena){
		ValueObject vo = new ValueObject();
		vo.setMensaje("");
		vo.setId(0);
		EIGlobal.mensajePorTrace("DESBLOQUEAR 1", EIGlobal.NivelLog.DEBUG);
		/*
		 * ***********************************************************************************
		 * OBTENEMOS UNA CONEXI�N DIRECTA A LA BASE DE DATOS								 *
		 * ***********************************************************************************
		 */
		dao = new DesbloquearContrasenaDAO();
		 /*
		  * ***********************************************************************************
		  */

		 /*
		 * ***********************************************************************************
		 * VALIDAMOS LA AUTENTICACI�N DEL USUARIO COMO EN EL LOGIN DE ENLACE				 *
		 * ***********************************************************************************
		 */
		 EIGlobal.mensajePorTrace("DESBLOQUEAR 2", EIGlobal.NivelLog.DEBUG);


		String pwdEncript1 = encriptaPwd(usuario, contrasena, 10);

		//EIGlobal.mensajePorTrace("PARAMETROS A ENCRIPTAR PWD: ", EIGlobal.NivelLog.INFO);
		//EIGlobal.mensajePorTrace(usuario + " " + contrasena + " 20", EIGlobal.NivelLog.INFO);
		String pwdEncript2 = encriptaPwd(usuario, contrasena, 20);
	    //EIGlobal.mensajePorTrace(pwdEncript2, EIGlobal.NivelLog.INFO);



		/******
		 * NUEVA VALIDACI�N DE CONTRASE�A
		 */

		 EIGlobal.mensajePorTrace("DESBLOQUEAR 3", EIGlobal.NivelLog.DEBUG);
		String contrasenaBase="";
		String descError;
		try{
			 EIGlobal.mensajePorTrace("DESBLOQUEAR 4", EIGlobal.NivelLog.DEBUG);
			contrasenaBase = dao.obtenContrasena(usuario);
			 EIGlobal.mensajePorTrace("DESBLOQUEAR 5", EIGlobal.NivelLog.DEBUG);
			 }catch(SQLException se){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al validar obtener la contrasena\", 3);");
				 se.printStackTrace();
				 return vo;
			 }catch(Exception ee){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n general al obtener la contrasena\", 3);");
				 ee.printStackTrace();
			 return vo;
		 }
			//EIGlobal.mensajePorTrace("contrasena base: " + contrasenaBase, EIGlobal.NivelLog.INFO);
			//EIGlobal.mensajePorTrace("contrasena : " + pwdEncript2, EIGlobal.NivelLog.INFO);
			 EIGlobal.mensajePorTrace("DESBLOQUEAR 6", EIGlobal.NivelLog.DEBUG);
			if (!contrasenaBase.equals(pwdEncript2)){

			descError = "Contrase&ntilde;a actual incorrecta.  " +
						"Usted ya ha hecho uso de su intento &uacute;nico de desbloqueo. " +
						"Favor de generar una nueva contrase&ntilde;a";
			/*
			 * *********************************************************************************************
			 * Si la autenticaci�n no fue exitosa, actualizamos el intento de desbloqueo                   *
			 * *********************************************************************************************
			 */
			try{
				dao.actualizaBloqueo(usuario);
			 }catch(SQLException se){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al actualizar los intentos de desbloqueo\", 3);");
				 se.printStackTrace();
				 return vo;
			 }
			 catch(Exception ee){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n general al actualizar los intentos de desbloqueo\", 3);");
				 ee.printStackTrace();
				 return vo;
			 }


				vo.setMensaje( "cuadroDialogo(\" "+ descError +"\", 4);" );

			}

		/*
		 * *********************************************************************************************************
		 * Si ya pas� la autenticaci�n, validamos que no haya "quemado" su unico intento de desbloqueo
		 * *********************************************************************************************************
		 */
			 EIGlobal.mensajePorTrace("DESBLOQUEAR 7", EIGlobal.NivelLog.DEBUG);
		if (vo.getMensaje().equals("")){
			try{

				long intentos = dao.seleccionaIntentosFallidos(usuario);


					 if (intentos!=0){
						 vo.setMensaje("cuadroDialogo(\"Ya ha realizado su intento de desbloqueo\", 3);");
						 return vo;
					 }else{
						 EIGlobal.mensajePorTrace("NO HA HECHO EFECTIVO SU INTENTO DE DESBLOQUEO!!!", EIGlobal.NivelLog.DEBUG);
					 }

				 EIGlobal.mensajePorTrace("DESBLOQUEAR 4", EIGlobal.NivelLog.DEBUG);
			 }catch(SQLException se){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al validar los intentos de desbloqueo\", 3);");
				 se.printStackTrace();
				 return vo;
			 }catch(Exception ee){
				 vo.setMensaje("cuadroDialogo(\"Excepci�n general al validar los intentos de desbloqueo\", 3);");
				 ee.printStackTrace();
			 return vo;
		 }
		}
		/*
		 * *********************************************************************************************************
		 */

		return vo;

	}

	public String desbloquear(String usuario){
		/*
		 * ********************************************************************************************************
		 * HACEMOS EL DESBLOQUEO
		 * HAY QUE DESBLOQUEAR LA CLAVE Y ACTUALIZAR LA BANDERA DE QUE YA QUEM� SU INTENTO
		 * ********************************************************************************************************
		 */
		 String error = "";
		 EIGlobal.mensajePorTrace("DESBLOQUEAR 8", EIGlobal.NivelLog.DEBUG);
			try{
				dao = new DesbloquearContrasenaDAO();
				dao.desbloquear(usuario);

			 }catch(SQLException se){
				 error = "cuadroDialogo(\"Excepci�n SQL al desbloquear\", 3);";
				 se.printStackTrace();

			 }catch(Exception ee){
				 error = "cuadroDialogo(\"Excepci�n general al desbloquear\", 3);";
				 ee.printStackTrace();

		 }

			EIGlobal.mensajePorTrace("DESBLOQUEAR 19", EIGlobal.NivelLog.DEBUG);


		/*
		 * ********************************************************************************************************
		 */
		return error;
	}

	public String convToHTML( String cadena)

{
	String s1="";
	String s2="";
	// [a] equivale a �
	// [e] equivale a �
	String[] dominio={"[a]","[e]","[i]","[o]","[u]"};
	String[] dominioX=  {"a","e","i","o","u"};
	int pos=0;

	while(cadena.indexOf("[n]")>0)
	{
		pos=cadena.indexOf("[n]");
		s1=cadena.substring(0,pos)+"&ntilde;";
		s2=cadena.substring(pos+3);
		cadena=s1+s2;
	}

	for (int i=0;i<dominio.length;i++)
	{
		while(cadena.indexOf(dominio[i])>0)
		{
			System.out.print(i);
			pos=cadena.indexOf(dominio[i]);
			s1=cadena.substring(0,pos)+"&"+dominioX[i]+"acute;";
			s2=cadena.substring(pos+3);
			cadena=s1+s2;
		}
	}
	return cadena;
}



	public String encriptaPwd(String usr, String pwd, int len){
	    EIGlobal.mensajePorTrace("Funcion encriptaPwd()", EIGlobal.NivelLog.INFO);
		String pwdEncript = "";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			String amessage = usr + pwd;
			byte [] hash = md.digest(amessage.getBytes());
			String a = new String(hash);

			pwdEncript = Base64.encodeBytes(hash, 0, len);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return pwdEncript;
	}

	public String[] desentramaS(String cadena, char caracter)
	{
		String vartmp="";
	    String regtmp="";
	    int totalCampos=0;
		int tamanioArreglo=0;
	    String[] camposTabla;

	    regtmp = cadena;
	    for (int i = 0; i < regtmp.length(); i++ )
		{
		  if ( regtmp.charAt( i ) == caracter )
			totalCampos++;
		}
		camposTabla = new String[totalCampos];
		for (int i = 0; i < totalCampos; i++ )
		{
			if ( regtmp.charAt( 0 ) == caracter )
			{
				vartmp = " ";
				regtmp = regtmp.substring( 1, regtmp.length() );
			}
			else
			{
				vartmp = regtmp.substring( 0, regtmp.indexOf( caracter )).trim();
				if ( regtmp.indexOf( caracter ) > 0 )
					regtmp=regtmp.substring( regtmp.indexOf( caracter ) + 1,regtmp.length() );
			}
			camposTabla[i]=vartmp;
		}
		return camposTabla;
	}


	public String obtenLongPwd(String usr){
	    EIGlobal.mensajePorTrace("Funcion obtenLongPwd()", EIGlobal.NivelLog.INFO);

		String tramaEnviada= usr;
		String coderror = "";

		ServicioTux ctRemoto = new ServicioTux();
		//ctRemoto.setContext (context);
		Hashtable htResult = null;

		try {
			System.out.println("Enviando Trama...");
			htResult = ctRemoto.longitudPassword(tramaEnviada);
			System.out.println("Trama Enviada... obteniendo respuesta");
			coderror = (String) htResult.get ("BUFFER");
		} catch (Exception e) {
			e.printStackTrace ();
		}
		return coderror;
	}


}
/*</VC>*/