package mx.altec.enlace.admonContrasena.command;
import java.sql.SQLException;

import mx.altec.enlace.admonContrasena.dao.DatosSolicitudDAO;
import mx.altec.enlace.admonContrasena.vo.PeticionTO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.utilerias.EIGlobal;

public class ReimpSolicitudCommand {



	public ValueObject seleccionaSolGenerada(String cod, String caracter){
		ValueObject vo=new ValueObject();
		vo.setId(0);
		vo.setMensaje("");


		try{

			EIGlobal.mensajePorTrace("en seleccionar sol generada caracter " + caracter, EIGlobal.NivelLog.DEBUG);
			DatosSolicitudDAO dao= new DatosSolicitudDAO( );
			vo.setMensaje(dao.seleccionaSolGenerada(cod, caracter));
			vo.setId(0);
		}catch(SQLException se){
			 vo.setMensaje("cuadroDialogo(\"Excepci�n SQL al validar obtener la contrasena\", 3);");
			 vo.setId(1);
			 se.printStackTrace();
			 return vo;
		 }catch(Exception ee){
			 vo.setMensaje("cuadroDialogo(\"Excepci�n general al obtener la contrasena\", 3);");
			 ee.printStackTrace();
			 vo.setId(1);
		 }


		return vo;

	}

	public PeticionTO consultaDatos(String Folio, String CodigoCliente)throws SQLException, Exception{
		//PeticionTO to= null;
		PeticionTO to = new PeticionTO();
		try{
			DatosSolicitudDAO dao= new DatosSolicitudDAO( );
	       to= dao.consultaReimprimeSolicitud(Integer.parseInt(Folio),CodigoCliente );
		}
		/*catch(SQLException e){
			System.out.println("error de consulta");
		}*/
		catch(Exception x){
			System.out.println("ocurrio una exepcion");
		}

		return to;

	}


}
