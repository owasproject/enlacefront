/*<VC proyecto="200710001" autor="SCG" fecha="29/04/2007" descripción="Activacion de contraseña">*/
package mx.altec.enlace.admonContrasena.command;

import java.sql.SQLException;
import java.util.Hashtable;

import javax.servlet.ServletContext;

import mx.altec.enlace.admonContrasena.dao.ActivarContrasenaDAO;
import mx.altec.enlace.admonContrasena.dao.DesbloquearContrasenaDAO;
import mx.altec.enlace.admonContrasena.vo.ValueObject;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.utilerias.EIGlobal;




public class ActivarContrasenaCommand {
	protected String codigoCliente;

	protected String folioSolicitud;

	protected ServletContext context;

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getFolioSolicitud() {
		return folioSolicitud;
	}

	public void setFolioSolicitud(String folioSolicitud) {
		this.folioSolicitud = folioSolicitud;
	}

	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}

	public static String leftPadCeros(String cadena, int width) {
		String aux = "";
		for (int i = cadena.length(); i < width; i++)
			aux += "0";
		cadena = aux + cadena;
		return cadena;
	}

	/**
	 * Activa la contraseña del cliente
	 *
	 * @param codigoCliente
	 *            codigo de cliente
	 * @param status
	 *            status de activacion
	 * @return Trama con status de la operacion
	 * @see <code>activar()</code>
	 */
	public ValueObject activar(String codigoCliente, String folioSolicitud) {
		this.setCodigoCliente(codigoCliente);
		this.setFolioSolicitud(folioSolicitud);
		return this.activar();
	}

	/**
	 * Activa la contraseña del cliente
	 *
	 * @return Trama con status de la operacion
	 */
	public ValueObject activar() {
		ValueObject regresaVO = new ValueObject();
		String coderror = "";
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activar(): BEGIN", EIGlobal.NivelLog.INFO);
		codigoCliente = leftPadCeros(codigoCliente, 8);
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activar():codCliente: " + codigoCliente, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activar():folioSolicitud: " + folioSolicitud, EIGlobal.NivelLog.INFO);

		if (codigoCliente != null && folioSolicitud != null) {
			String tramaEnviada = codigoCliente + "@A@";

			ServicioTux ctRemoto = new ServicioTux();
			if (this.context != null) {
				//ctRemoto.setContext(this.context);
				Hashtable htResult = null;

				try {
			//	htResult = ctRemoto.activaUser(tramaEnviada);
				} catch (Exception e) {
					e.printStackTrace();
					regresaVO.setId(1);
					regresaVO.setMensaje("cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
					return regresaVO;
				}

			//	coderror = (String) htResult.get("BUFFER");
				// log("SeguActNipEnlace() Codigo de Error = >" + coderror +
				// "<");
				coderror=null;
				if ((coderror == null || coderror.equals(""))) {
					regresaVO.setId(1);
					regresaVO.setMensaje("cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
				} else {
					regresaVO.setId(2);
					regresaVO.setMensaje(coderror);
				}
			} else {
				regresaVO.setId(1);
				regresaVO.setMensaje("cuadroDialogo(\"Contexto no asignado. Contacte al administrador.\", 3);");
			}
		} else {
			regresaVO.setId(1);
			regresaVO.setMensaje("cuadroDialogo(\"Parametros nulos[c:" + codigoCliente + ", f:" + folioSolicitud + "]. Contacte al administrador.\", 3);");
		}
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activar(): END", EIGlobal.NivelLog.INFO);
		return regresaVO;
	}

	/**
	 * Activa la contraseña del cliente
	 *
	 * @return Trama con status de la operacion
	 */
	public ValueObject activar(String c) {
		ValueObject vo = new ValueObject();
		String coderror = "";
		if (codigoCliente != null && folioSolicitud != null) {
			// String tramaEnviada = codigoCliente + "@" + status + "@";

			try {
				vo = activaUser();
			} catch (Exception e) {
				e.printStackTrace();
				vo.setId(1);
				vo.setMensaje("cuadroDialogo(\"Por Favor, Intente mas Tarde.\", 3);");
				return vo;
			}
		} else {
			vo.setId(1);
			vo.setMensaje("cuadroDialogo(\"Parametros nulos[c:" + codigoCliente + ", f:" + folioSolicitud + "]. Contacte al administrador.\", 3);");
		}
		return vo;
	}

	/**
	 * Activa la contraseña usando DAO (sustitucion de servicio tuxedo)
	 *
	 * @return Id y Mensaje de status de operacion
	 * @throws Exception
	 * @throws SQLException
	 */
	private ValueObject activaUser() throws Exception, SQLException {
		ActivarContrasenaDAO dao = new ActivarContrasenaDAO(codigoCliente, folioSolicitud);
		ValueObject vo = new ValueObject();
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activarUser(): BEGIN", EIGlobal.NivelLog.INFO);
		if (this.codigoCliente != null && this.folioSolicitud!=null) {
			try {

				if (dao.esFolioSolicitudOk()) {
					if(dao.activarContrasena()){
						vo.setId(2);
						vo.setMensaje("Se ha ACTIVADO su contrase&ntilde;a");
					}else{
						vo.setId(1);
						vo.setMensaje("cuadroDialogo(\"No se ha podido activar su contrase&ntilde;a.<br>" +
						              "Ha ocurrido un error en el servidor, favor de contactar al Administrador.\", 3);");
					}
				} else {
					vo.setId(1);
					vo.setMensaje("cuadroDialogo(\"El folio de solicitud que proporcion&oacute; no existe, \n o " +
					              "Solicitud no aprobada por Control Central de Cuentas\", 3);");
				}
			} catch (SQLException se) {
				vo.setId(1);
				vo.setMensaje("cuadroDialogo(\"ERROR SQL: Contacte al administrador.\", 3);");
				se.printStackTrace();
			} catch (Exception ee) {
				vo.setId(1);
				vo.setMensaje("cuadroDialogo(\"ERROR: Contacte al administrador.\", 3);");
				ee.printStackTrace();
			}
		}
		EIGlobal.mensajePorTrace("ActivarContrasenaCommand::activarUser(): END", EIGlobal.NivelLog.INFO);
		return vo;
	}


	public boolean validaContrasena(String usuario, String contrasena) throws Exception{

		DesbloquearContrasenaCommand desbloqCommand =
			new DesbloquearContrasenaCommand();
		DesbloquearContrasenaDAO desbloqDao = new DesbloquearContrasenaDAO();
		boolean valida = false;
		try{
			String pwdEncript2 = desbloqCommand.encriptaPwd(usuario, contrasena, 20);
			String contrasenaBase = desbloqDao.obtenContrasena(usuario);

			if (contrasenaBase.trim().equals(pwdEncript2.trim())){
				valida = true;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return valida;
	}


}
/* </VC> */