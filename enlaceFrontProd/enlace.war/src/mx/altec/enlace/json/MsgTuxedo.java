package mx.altec.enlace.json;

import mx.altec.enlace.json.parser.JSONParserException;
import mx.altec.enlace.utilerias.Global;

/**
 * Clase que define la trama de envio y recepci�n hacia el dispatcher
 * @author gguerrero
 *
 */
public class MsgTuxedo extends JSONObject{
	private static final long serialVersionUID = 1L;
	/**
	 * Nombre del servicio tuxedo a invocar
	 */
	private String svc = "";
	/**
	 * Elemento raiz de mensaje de envio
	 */
	private JSONObject rootEnv = new JSONObject();
	/**
	 * Elemento raiz de mensaje de recepcion (respuesta)
	 */
	private JSONObject rootRec = new JSONObject();
	/**
	 * Campos (fields) de envio
	 */
	private JSONObject fldsEnv = new JSONObject();
	/**
	 * Campos (fields) de recepci�n (respuesta)
	 */
	private JSONObject fldsRec = new JSONObject();

	/**
	 * Construye el bean de mensaje de respuesta a partir de un string JSON
	 * @param strJSONResponse Cadena en JSON que devuelve el servicio tuxedo
	 * @throws JSONParserException Si la cadena esta mal formada se arroja la excepci�n
	 */
	public MsgTuxedo(String strJSONResponse) throws JSONParserException{
		try{
		JSONObject objResponse = (JSONObject)JSONValue.parse(strJSONResponse);
		JSONObject objRootRec = (JSONObject)objResponse.get("msgRec");
		this.fldsRec = (JSONObject) objRootRec.get("fldsRec");
		}catch(Exception e){
			throw new JSONParserException();
		}

	}

	/**
	 * Construye el bean de mensaje de petici�n
	 */
	public MsgTuxedo(){
		this.put("msgEnv", rootEnv);
		rootEnv.put("fldsEnv", this.fldsEnv);
		this.put("msgRec", rootRec);
		rootRec.put("fldsRec", this.fldsRec);
	}
	/**
	 * Obtiene la representaci�n JSON del bean con el estado actual
	 * @return
	 */
	public String getStringJSON(){
		return this.toString();
	}

	/**
	 * Obtiene el nombre del servicio tuxedo a ejecutar
	 * @return
	 */
	public String getSvc() {
		return svc;
	}
	/**
	 * Establece el nombre del servicio tuxedo a ejecutar
	 * @param svc
	 */
	public void setSvc(String svc) {
		rootEnv.put("svc", svc);
		this.svc = svc;
	}
	/**
	 * Agrega un parametro de envio a tuxedo
	 * @param name
	 * @param value
	 */
	public void addFldEnv(String name, Object value) {
		this.fldsEnv.put(name, value);
	}
	/**
	 * Agrega un parametro de recepci�n a tuxedo
	 * @param name
	 * @param value
	 */
	public void addFldRec(String name, Object value) {
		this.fldsRec.put(name, value);
	}
	/**
	 * Obtiene el valor de retorno del servicio tuxedo como cadena
	 * @param name Nombre del parametro a obtener
	 * @return Valor de retorno como cadena
	 * @throws MsgTuxedoException Maneja el error en caso de no encontrar el parametro de respuesta
	 */
	public String getFldRecAsString(String name) throws MsgTuxedoException{
		Object value = this.fldsRec.get(name);
		if(value == null) throw new MsgTuxedoException(Global.JSON_TUX_FIELD_NOTFOUND_EXCEPTION + ", " + name);
		return value.toString();
	}
	/**
	 * Obtiene el valor de retorno del servicio tuxedo como wrapper double.
	 * @param name Nombre del parametro a obtener
	 * @return Valor de retorno como cadena
	 * @throws MsgTuxedoException Maneja el error en caso de no encontrar el parametro de respuesta
	 */
	public Double getFldRecAsDouble(String name) throws MsgTuxedoException{
		Object value = this.fldsRec.get(name);
		Double dblAux = new Double(0.00);
		if(value == null) throw new MsgTuxedoException(Global.JSON_TUX_FIELD_NOTFOUND_EXCEPTION + ", " + name);
		try{
			dblAux = new Double(value.toString());
		}catch(Exception ex){
			throw new MsgTuxedoException(Global.JSON_TUX_FIELD_EXCEPTION + " : " + value.toString() + " a Double");
		}
		return dblAux;
	}
	/**
	 * Obtiene el valor de retorno del servicio tuxedo como wrapper entero.
	 * @param name Nombre del parametro a obtener
	 * @return Valor de retorno como cadena
	 * @throws MsgTuxedoException Maneja el error en caso de no encontrar el parametro de respuesta
	 */
	public Integer getFldRecAsInteger(String name) throws MsgTuxedoException{
		Object value = this.fldsRec.get(name);
		Integer intAux = new Integer(0);
		if(value == null) throw new MsgTuxedoException(Global.JSON_TUX_FIELD_NOTFOUND_EXCEPTION + ", " + name);
		try{
			intAux = new Integer(value.toString());
		}catch(Exception ex){
			throw new MsgTuxedoException(Global.JSON_TUX_FIELD_EXCEPTION + " : " + value.toString() + " a Integer");
		}
		return intAux;
	}
	protected void finalize() throws Throwable{
        super.finalize(); 
        rootEnv.clear();
        rootRec.clear();
        fldsEnv.clear();
        fldsRec.clear();
        rootEnv = null;
        rootRec = null;
        fldsEnv = null;
        fldsRec = null;
	}
}