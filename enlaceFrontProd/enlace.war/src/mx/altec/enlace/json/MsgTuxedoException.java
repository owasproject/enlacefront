package mx.altec.enlace.json;

public class MsgTuxedoException extends Exception {

	public MsgTuxedoException() {
		// TODO Apéndice de constructor generado automáticamente
	}

	public MsgTuxedoException(String message) {
		super(message);
		// TODO Apéndice de constructor generado automáticamente
	}

	public MsgTuxedoException(Throwable cause) {
		super(cause);
		// TODO Apéndice de constructor generado automáticamente
	}

	public MsgTuxedoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Apéndice de constructor generado automáticamente
	}

}
