package mx.altec.enlace.jms.mq.conexion;

import java.util.ResourceBundle;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import com.ibm.mq.jms.MQQueue;

import javax.naming.NamingException;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;
import mx.altec.enlace.servicios.ServiceLocator;


/**
* The Class MQQueueSession.
*/
public class MQQueueSession {

/** The qcf. */
private QueueConnectionFactory qcf = null;

/** The connection. */
private QueueConnection connection = null;

/** The session. */
private QueueSession session = null;

/** The queue sender. */
private Queue queueSender = null;

/** The queue reciever. */
private Queue queueReciever = null;

/** The sender. */
private QueueSender sender = null;

/** The reciever. */
private QueueReceiver reciever = null;

/** The request message. */
private TextMessage requestMessage = null;

/** The selector. */
private String selector = null;

private String msgId = null;

/**
* Crea una nueva instancia de la clase MQQueueSession.
* @throws NamingException,JMSException the business exception
*/
public MQQueueSession(String jndiNombreFabrica,
			String jndiNombreEnvio, String jndiNombreRecepcion)
			throws NamingException,JMSException {


EIGlobal.mensajePorTrace("Creando objeto MQQueueSession", EIGlobal.NivelLog.DEBUG);
ServiceLocator servLocator = ServiceLocator.getInstance();

qcf = servLocator.getConnFactory(jndiNombreFabrica);
queueSender = servLocator.getQueue(jndiNombreEnvio);
queueReciever = servLocator.getQueue(jndiNombreRecepcion);

}

	public void open() throws JMSException {
		EIGlobal.mensajePorTrace("Metodo open()", EIGlobal.NivelLog.DEBUG);

		if (this.connection == null) {
			connection = qcf.createQueueConnection();
			connection.start();
		}
		if (this.session == null) {
			this.session = connection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
		}

	}


/**
* Cierra la conexion con MQ.
*/
public void close() {
	EIGlobal.mensajePorTrace("Metodo close()", EIGlobal.NivelLog.DEBUG);
	if (this.connection != null) {
		try {
			connection.close();
			connection = null;
		} catch (JMSException e) {
		}
	}

	if (this.session != null) {
		try {
			session.close();
			session = null;
		} catch (JMSException e) {
		}
	}
}

/**
* Obtener trama.
* @param trama Trama a env�ar hacia 390
* @return Trama de respuesta resultado de la ejecuci�n hacia 390
*/
public String enviaRecibeMensaje(String trama) throws MQQueueSessionException {
	EIGlobal.mensajePorTrace("obtenerTrama:[" + trama+ "]", EIGlobal.NivelLog.DEBUG);
	
	String respuestas = "Sin Datos en Trama";
	if (trama != (null)) {
		try {
			open();
			msgId = enviartrama(trama);
			respuestas = recibirtrama(msgId);
			EIGlobal.mensajePorTrace("tramaRes:[" + respuestas+ "]", EIGlobal.NivelLog.DEBUG);
		} catch (JMSException e1) {
		e1.printStackTrace();
		}
	}
	return respuestas;
}

/**
* Recibirtrama.
* @param msgId the msg id
* @return the string
*/
private String recibirtrama(String msgId) {
String resp = "";
EIGlobal.mensajePorTrace("recibirTrama:[" + msgId+ "]", EIGlobal.NivelLog.DEBUG);
try {
reciever = session.createReceiver(queueReciever, selector);
Message message = null;
message = reciever.receive(Integer.parseInt(Global.MQ_TIMEOUT) * 1000L);

requestMessage = (TextMessage) message;
if (message == null) {
requestMessage = session.createTextMessage();
requestMessage.setText("TIMEOUT");
} else {
requestMessage = (TextMessage) message;
}
resp = requestMessage.getText();
reciever.close();
reciever = null;
} catch (Exception e) {
}


return resp;
}

/**
* Enviartrama.
*
* @param tramas
* the tramas
*
* @return the string
*/
private String enviartrama(String tramas) {
	EIGlobal.mensajePorTrace("enviartrama:[" + tramas + "]", EIGlobal.NivelLog.DEBUG);
String msgId = "";
try {
sender = session.createSender(queueSender);
requestMessage = session.createTextMessage();
requestMessage.setText(tramas);
/*requestMessage.setJMSExpiration(ConfiguracionMQ.getInstance()
.getTIMEOUTEXPIRY());*/
requestMessage.setStringProperty("JMS_IBM_Format", ("MQSTR"
+ "    ").substring(0, 8));
requestMessage.setIntProperty("JMS_IBM_MsgType", 8);
MQQueue replyFromReceiver = (MQQueue) queueReciever;
MQQueue replyToQ = new MQQueue(replyFromReceiver
.getBaseQueueManagerName(), replyFromReceiver
.getBaseQueueName());
Queue reply = (Queue) replyToQ;
requestMessage.setJMSReplyTo(reply);
sender.send(requestMessage);
msgId = requestMessage.getJMSMessageID();
selector = "JMSCorrelationID = '"
+ msgId + "'";
sender.close();
sender = null;
} catch (Exception e) {
e.printStackTrace();
}
return msgId;
}

public void enviaMensaje(String msj) throws	MQQueueSessionException {
	enviartrama(msj);
}

public String recibeMensaje() throws MQQueueSessionException {
	return recibirtrama(msgId);
}

protected void finalize() throws Throwable {
	super.finalize();
	try {
		close();
	} catch (Exception e) {}
	qcf = null;
	connection = null;
	session = null;
	queueSender = null;
	queueReciever = null;
	sender = null;
	reciever = null;
	requestMessage = null;
	selector = null;
	msgId = null;
}

}