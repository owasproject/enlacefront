package mx.altec.enlace.gwt.adminlym.client;

import java.util.ArrayList;

import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminLyMServiceAsync {

	void setBean(BeanAdminLyM pstrBean, AsyncCallback callback);
	void getBean(String pstrVista, AsyncCallback<BeanAdminLyM> callback);
	void getListaLimitesOperacion(String pstrUsuario , AsyncCallback<ArrayList<LimitesOperacion>> callback );

}
