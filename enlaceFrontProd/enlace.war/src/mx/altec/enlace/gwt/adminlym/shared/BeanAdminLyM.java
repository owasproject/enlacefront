package mx.altec.enlace.gwt.adminlym.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class BeanAdminLyM implements IsSerializable {

	private LimitesContrato               LimitesContrato         = new LimitesContrato();
	private ArrayList<LimitesUsuario>     ListaLimitesContratos   = new ArrayList<LimitesUsuario>();
	private ArrayList<GrupoOperacion>     ListaOperaciones        = new ArrayList<GrupoOperacion>();
	private boolean                       FacultadContrato        = true;
	private boolean                       FacultadOperacion       = true;
	
	private ArrayList<LimitesOperacion>   ListaLimitesConsultados = new ArrayList<LimitesOperacion>();  	
	private ArrayList<LimitesOperacion>   ListaLimitesAgregados   = new ArrayList<LimitesOperacion>();  	
	private ArrayList<LimitesOperacion>   ListaLimitesModificados = new ArrayList<LimitesOperacion>();  	
	private ArrayList<LimitesOperacion>   ListaLimitesEliminados  = new ArrayList<LimitesOperacion>();  	
	private ArrayList<LimitesOperacion>   ListaLimitesFinal       = new ArrayList<LimitesOperacion>();  	
	
	public static boolean                 simulacionActivada      = false; 
	
	/**
	 * @return el LimitesContrato
	 */
	public LimitesContrato getLimitesContrato() {
		return LimitesContrato;
	}
	/**
	 * @param limiteContrato el LimitesContrato a establecer
	 */
	public void setLimitesContrato(LimitesContrato limitesContrato) {
		LimitesContrato = limitesContrato;
	}

	/**
	 * @return el listaLimitesContratos
	 */
	public ArrayList<LimitesUsuario> getListaLimitesContratos() {
		return ListaLimitesContratos;
	}
	/**
	 * @param listaLimitesContratos el listaLimitesContratos a establecer
	 */
	public void setListaLimitesContratos(
			ArrayList<LimitesUsuario> listaLimitesContratos) {
		ListaLimitesContratos = listaLimitesContratos;
	}
	
	/**
	 * @return el ListaOperaciones
	 */
	public ArrayList<GrupoOperacion> getListaOperaciones() {
		return ListaOperaciones;
	}
	/**
	 * @param ListaOperaciones el ListaOperaciones a establecer
	 */
	public void setListaOperaciones(ArrayList<GrupoOperacion> listaOperaciones) {
		ListaOperaciones = listaOperaciones;
	}
	public ArrayList<LimitesOperacion> getListaLimitesConsultados() {
		return ListaLimitesConsultados;
	}
	public void setListaLimitesConsultados(
			ArrayList<LimitesOperacion> listaLimitesConsultados) {
		ListaLimitesConsultados = listaLimitesConsultados;
	}
	public ArrayList<LimitesOperacion> getListaLimitesAgregados() {
		return ListaLimitesAgregados;
	}
	public void setListaLimitesAgregados(
			ArrayList<LimitesOperacion> listaLimitesAgregados) {
		ListaLimitesAgregados = listaLimitesAgregados;
	}
	public ArrayList<LimitesOperacion> getListaLimitesModificados() {
		return ListaLimitesModificados;
	}
	public void setListaLimitesModificados(
			ArrayList<LimitesOperacion> listaLimitesModificados) {
		ListaLimitesModificados = listaLimitesModificados;
	}
	public ArrayList<LimitesOperacion> getListaLimitesEliminados() {
		return ListaLimitesEliminados;
	}
	public void setListaLimitesEliminados(
			ArrayList<LimitesOperacion> listaLimitesEliminados) {
		ListaLimitesEliminados = listaLimitesEliminados;
	}
	public ArrayList<LimitesOperacion> getListaLimitesFinal() {
		return ListaLimitesFinal;
	}
	public void setListaLimitesFinal(ArrayList<LimitesOperacion> listaLimitesFinal) {
		ListaLimitesFinal = listaLimitesFinal;
	}
	public boolean isFacultadContrato() {
		return FacultadContrato;
	}
	public boolean isFacultadOperacion() {
		return FacultadOperacion;
	}
	
	public void setFacultadContrato(boolean facultadContrato) {
		FacultadContrato = facultadContrato;
	}
	public void setFacultadOperacion(boolean facultadOperacion) {
		FacultadOperacion = facultadOperacion;
	}
	
	public boolean getFacultadContrato() {
		return FacultadContrato;
	}
	public boolean getFacultadOperacion() {
		return FacultadOperacion;
	}
	
}
