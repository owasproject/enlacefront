package mx.altec.enlace.gwt.adminlym.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import mx.altec.enlace.gwt.adminlym.shared.BeanAdminLyM;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.gwt.adminlym.shared.LimitesUsuario;

import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.user.client.ui.Image;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClientEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AltaOperacion {

	private static HashMap<String,String>    ListaUsuarios         = new HashMap<String,String>(); 
	private static HashMap<String,String>    ListaOperaciones      = new HashMap<String,String>();
	private static String                    Usuario               = "";
	private static String                    Nombre                = "";
	private static String                    DescOper              = "";
	private static String                    CodOper               = "";
	private static String                    Monto                 = "";
	private static String                    Contrato              = "";
	private static String                    Opcion                = "";
	private static SelectItem                ComboUsuario          = new SelectItem ("usuario","Usuario"); 
	private static SelectItem                ComboOperaciones      = new SelectItem ("operacion","Operacion"); 
	private static TextItem                  TextMonto             = new TextItem("monto","Monto"); 
	private static Image                     BotonAceptar          = new Image("/gifs/EnlaceMig/gbo25280.gif");
	private static Image                     BotonCancelar         = new Image("/gifs/EnlaceMig/gbo25190.gif");
	private static boolean                   init                  = false;
	private static Window                    winModal              = new Window();
	private static ListGrid                  GridLimitesUsurios    = new ListGrid();	
	private static BeanAdminLyM              BeanAdmin             = new BeanAdminLyM();
	
	public static void init(){
		
        winModal.setTitle("Agregar operacion");   
        winModal.setShowMinimizeButton(false);   
        winModal.setIsModal(true);
        winModal.setAlign(Alignment.CENTER);
        winModal.setAlign(VerticalAlignment.CENTER);
        winModal.setShowModalMask(true);
        winModal.setWidth(280);   
        winModal.setHeight(180);   
        //winModal.setAutoWidth();
        //winModal.setAutoHeight();
        winModal.centerInPage();   
        winModal.addCloseClickHandler(new CloseClickHandler() {   
            public void onCloseClick(CloseClientEvent event) {   
                //winModal.destroy();   
            	winModal.hide();
            }   
        });   
        
        TextMonto.setKeyPressFilter("[0-9.]");
        //ComboUsuario.setType("comboBox");
        //ComboOperaciones.setType("comboBox");
        
    	VLayout VLayoutGeneral  = new VLayout();
    	VLayoutGeneral.setAlign(Alignment.CENTER);
    	//VLayoutGeneral.setWidth(250);
    	//VLayoutGeneral.setHeight(200);
    	
    	VLayout layoutInfo = new VLayout();
    	//layoutInfo.setAlign(Alignment.CENTER);
        DynamicForm form = new DynamicForm();   
        form.setFields(ComboUsuario, ComboOperaciones, TextMonto);
        layoutInfo.addMember(form);
       
        
        HLayout layoutBotones = new HLayout();
        //layoutBotones.setWidth(250);
        layoutBotones.setHeight(50);
        layoutBotones.addMember(BotonCancelar);
        layoutBotones.addMember(BotonAceptar);
        layoutBotones.setAlign(Alignment.RIGHT);
        
        VLayoutGeneral.addMember(layoutInfo);
        VLayoutGeneral.addMember(layoutBotones);
	    
	    BotonCancelar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				System.out.println("cancelar...");
				//winModal.destroy();
				winModal.hide();
			}
	    });
	    
	    BotonAceptar.addMouseDownHandler(new MouseDownHandler(){
			public void onMouseDown(MouseDownEvent event) {
				
				System.out.println("aceptar...");
				boolean altaOk = true;
				Nombre   = ComboUsuario.getValue()==null?"":(String)ComboUsuario.getValue();
				Usuario  = ComboUsuario.getDisplayValue(Nombre);
				CodOper  = ComboOperaciones.getValue()==null?"":(String)ComboOperaciones.getValue();
				DescOper = ComboOperaciones.getDisplayValue(CodOper);
				Monto    = TextMonto.getValue()==null?"":(String)TextMonto.getValue();
				Contrato = BeanAdmin.getLimitesContrato().getContrato();
				Opcion   = BeanAdmin.getLimitesContrato().getOpcion();
				
				System.out.println("Usuario  :" + Usuario);
				System.out.println("Nombre   :" + Nombre);
				System.out.println("DescOper :" + DescOper);
				System.out.println("CodOper  :" + CodOper);
				System.out.println("Monto    :" + Monto);
				System.out.println("Contrato :" + Contrato);
				System.out.println("Opcion   :" + Opcion);
				
				if(Nombre.equals(""))
					altaOk = false;
				if(CodOper.equals(""))
					altaOk = false;
				if(Monto.equals(""))
					altaOk = false;

				if(altaOk){
					ListGridRecord record = new ListGridRecord();
					record.setAttribute("pk"        , Usuario + CodOper);
					record.setAttribute("usuario"   , Usuario);
					record.setAttribute("nombre"    , Nombre);
					record.setAttribute("descOper"  , DescOper);
					record.setAttribute("claveOper" , CodOper);
					record.setAttribute("monto"     , Monto);
					record.setAttribute("estado"    , "Agregado");
					
					GridLimitesUsurios.addData(record);
					System.out.println("Registro agregado...");
					//winModal.destroy();
					
					System.out.println("Buscando Usuario ["+ Usuario + "] en BeanAdmin para ser agregado...");
					ArrayList<LimitesUsuario> lista = BeanAdmin.getListaLimitesContratos();
					for(int i=0;i<lista.size();i++){
						LimitesUsuario LimiteUsuarioTmp = lista.get(i);
						if(LimiteUsuarioTmp.getCodCliente().equals(Usuario)){
							System.out.println("Usuario encontrado[" + LimiteUsuarioTmp.getCodCliente() + "]...");
							System.out.println("Nombre Usuario encontrado[" + LimiteUsuarioTmp.getNombre() + "]...");
							System.out.println("Numero de Operaciones:" + LimiteUsuarioTmp.getListaLimitesOperacion().size());
							LimitesOperacion limite = new LimitesOperacion();
							limite.setClaveOperacion(CodOper);
							limite.setContrato(Contrato);
							limite.setDescripcionOperacion(DescOper);
							limite.setLimiteOperacion(Monto);
							limite.setOpcion(Opcion);
							limite.setUsuario(Usuario);
							limite.setNombreUsuario(Nombre);
							limite.setEstado("Agregado");
							LimiteUsuarioTmp.getListaLimitesOperacion().add(limite);
							BeanAdmin.getListaLimitesAgregados().add(limite);
						}
					}
					
					System.out.println("Buscando registro base para ser eliminado...");
					ListGridRecord listaRegistros[] = GridLimitesUsurios.getRecords();
					for(int iReg=0;iReg<listaRegistros.length;iReg++){
						ListGridRecord recorAux = listaRegistros[iReg];
						if(recorAux.getAttribute("usuario").equals(Usuario) && recorAux.getAttribute("claveOper").equals("")){
							System.out.println("Registro base encontrado...");
							System.out.println("Eliminado registro base ...");
							GridLimitesUsurios.removeData(recorAux);
							System.out.println("Registro base eliminado...");
							break;
						}
					}
					
					System.out.println("Seleccionando primer registro...");
					listaRegistros = GridLimitesUsurios.getRecords();
					for(int iReg=0;iReg<listaRegistros.length;iReg++){
						ListGridRecord recorAux = listaRegistros[iReg];
						if(recorAux.getAttribute("usuario").equals(Usuario)){
							GridLimitesUsurios.selectSingleRecord(recorAux);
							System.out.println("Registro seleccionado...");
							break;
						}
					}
					
					winModal.hide();
				}
				else{
					System.out.println("Falta seleccionar datos...");
					SC.say("Falta seleccionar algun dato"); 
				}
			}
	    });
	    
	    ComboUsuario.addChangeHandler(new ChangeHandler(){
			public void onChange(ChangeEvent event) {
		        String NombreUsuarioSel = (String)event.getValue();
		        String UsuarioSel       = ComboUsuario.getDisplayValue(NombreUsuarioSel);
		        
		        System.out.println("Nombre Usuario Seleccionado:" + NombreUsuarioSel);
		        System.out.println("Usuario Seleccionado       :" + UsuarioSel);
				
		        System.out.println("Cargando Lista de operaciones....");
		        System.out.println("Numero de Operaciones:" + ListaOperaciones.size());
		        LinkedHashMap<String, String> mapOperaciones = new LinkedHashMap<String, String>(ListaOperaciones);
		        System.out.println("Eliminando operaciones ya registradas del usuario....");
		        ListGridRecord    records[] = GridLimitesUsurios.getRecords();
		        ArrayList<String> operRegs  = new ArrayList<String>();
		        for (int iRecord = 0; iRecord < records.length; iRecord++) {
					ListGridRecord RecordTemp = records[iRecord];
					String lstrUsuario = RecordTemp.getAttribute("usuario"); 
					System.out.println("Usuario:" + lstrUsuario);
					if(lstrUsuario.equals(UsuarioSel)){
						String claveOper = RecordTemp.getAttribute("claveOper");
						System.out.println("Cleve registrada:" + claveOper);
						operRegs.add(claveOper);
					}
				}
		        for(int iKeys=0;iKeys<operRegs.size();iKeys++)
		        	mapOperaciones.remove(operRegs.get(iKeys));
		        	
		        ComboOperaciones.setValueMap(mapOperaciones);
		        System.out.println("Lista de Operaciones Carganda....");
				
			}
	    	
	    });
	    
        winModal.addItem(VLayoutGeneral);   
        winModal.show();   							
	}
	
	public static void showPanelAlta(){
		
		
        System.out.println("Cargando Lista de Usuarios....");
        System.out.println("Numero de Usuarios:" + ListaUsuarios.size());
        LinkedHashMap<String, String> mapUsuarios = new LinkedHashMap<String, String>(ListaUsuarios);
        ComboUsuario.setValueMap(mapUsuarios);
        System.out.println("Lista de Usuarios Carganda....");
        
        /*
        System.out.println("Cargando Lista de operaciones....");
        System.out.println("Numero de Operaciones:" + ListaOperaciones.size());
        LinkedHashMap<String, String> mapOperaciones = new LinkedHashMap<String, String>(ListaOperaciones);
        ComboOperaciones.setValueMap(mapOperaciones);
        System.out.println("Lista de Operaciones Carganda....");
		*/
        
		ComboUsuario.setValue("");
		ComboOperaciones.setValue("");
		TextMonto.setValue("");
        
        
		if(init)
			winModal.show();
		else{
			init();
			init = true;
		}
	}
	
	public static void setListaUsuarios(HashMap<String,String> pobjValue){
		ListaUsuarios = pobjValue;
	}
	
	public static void setListaOperaciones(HashMap<String,String> pobjValue){
		ListaOperaciones = pobjValue;
	}
	
	public static void setGridLimitesUsurios(ListGrid pobjValue){
		GridLimitesUsurios = pobjValue;
	}
	
	public static void setBeanAdmin(BeanAdminLyM pobjValue){
		BeanAdmin = pobjValue;
	}
	
}
