package mx.altec.enlace.gwt.adminlym.client;

import java.util.Map;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;


public class MontosUsuariosDS extends DataSource{

	private static MontosUsuariosDS           instance = null;

	private DataSourceTextField    pkField   = new DataSourceTextField("pk"        , "Id");   
	private DataSourceTextField    usuario   = new DataSourceTextField("usuario"   , "Usuario");
	private DataSourceTextField    nombre    = new DataSourceTextField("nombre"    , "Nombre");
	private DataSourceTextField    descOper  = new DataSourceTextField("descOper"  , "Operacion");
	private DataSourceTextField    claveOper = new DataSourceTextField("claveOper" , "Clave Operacion");
	private DataSourceTextField    monto     = new DataSourceTextField("monto"     , "Monto Autorizado");
	
	
	public MontosUsuariosDS(String pstrId){
		setID(pstrId);
        pkField.setHidden(true);   
        pkField.setPrimaryKey(true);   
		usuario.setRequired(true); 
		nombre.setRequired(true); 
		//monto.KeyPressFilter("[0-9.]");
		setFields(pkField, usuario, nombre, descOper, claveOper, monto);
		setClientOnly(true);
	}
	

	public static MontosUsuariosDS getInstance() {
		if(instance==null){
			instance = new MontosUsuariosDS("usuariosDS");
		}
		return instance;
	}
	
	public void setMapGrup(Map<String,String> pobjVal){
		usuario.setValueMap(pobjVal);
	}


}
