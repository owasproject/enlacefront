package mx.altec.enlace.gwt.adminlym.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class LimitesContrato implements IsSerializable {

	private String opcion;
	private String tipoOperacion;
	private String grupo;
	private String descripcionGrupo;
	private String contrato;
	private String monto;
	private boolean limiteActivo;

	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getDescripcionGrupo() {
		return descripcionGrupo;
	}
	public void setDescripcionGrupo(String descripcionGrupo) {
		this.descripcionGrupo = descripcionGrupo;
	}
	public boolean isLimiteActivo() {
		return limiteActivo;
	}
	public void setLimiteActivo(boolean limiteActivo) {
		this.limiteActivo = limiteActivo;
	}	

	

	
}
