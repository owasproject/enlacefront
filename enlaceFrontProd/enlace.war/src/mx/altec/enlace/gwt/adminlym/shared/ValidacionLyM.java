package mx.altec.enlace.gwt.adminlym.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ValidacionLyM implements IsSerializable {

	private String contrato = "";
	private String usuario = "";
	private String claveOperacion= "";
	private String importe= "";
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClaveOperacion() {
		return claveOperacion;
	}
	public void setClaveOperacion(String claveOperacion) {
		this.claveOperacion = claveOperacion;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	
}
