package mx.altec.enlace.gwt.adminusr.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class GrupoOperacion implements IsSerializable {
	private String clave;
	private String descripcion;
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
