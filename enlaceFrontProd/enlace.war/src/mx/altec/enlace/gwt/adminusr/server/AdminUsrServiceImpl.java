package mx.altec.enlace.gwt.adminusr.server;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import mx.altec.enlace.gwt.adminusr.client.AdminUsrService;
import mx.altec.enlace.gwt.adminusr.shared.BeanAdminUsr;
import mx.altec.enlace.gwt.adminusr.shared.Facultad;
import mx.altec.enlace.gwt.adminusr.shared.GrupoPerfiles;
import mx.altec.enlace.gwt.adminusr.shared.PerfilPrototipo;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
//@SuppressWarnings("serial")
public class AdminUsrServiceImpl extends RemoteServiceServlet implements AdminUsrService {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2161966197237034653L;

	public BeanAdminUsr getBean(String pstrVista) throws IllegalArgumentException {
		
		System.out.println("getBean()...");
		
		HttpSession sesion = getThreadLocalRequest().getSession();
		
		BeanAdminUsr bean = new BeanAdminUsr();
		
				
		try{
			System.out.println();
			if(pstrVista.equals("detalle")){
				System.out.println("Detalle...");
				bean = (BeanAdminUsr)sesion.getAttribute("BeanAdminUsrFin");
				if(bean==null){
					bean = (BeanAdminUsr)sesion.getAttribute("BeanAdminUsr");
					
					
					//TODO: Solo para pruebas
					/*
					if(bean==null){
						//bean = BeanAdminUsr.getBeanFalso(15,10,60,100,30,30);
						bean = BeanAdminUsr.getBeanFalso(3,3,3,10,0,1);
					}
					*/
					//bean.setFacultadesUsuarioNoSeleccionadas(getFacultadesNoSeleccionadas(bean));
					//bean.setFacultadesUsuarioExtNoSeleccionadas(getFacultadesExtNoSeleccionadas(bean));

					System.out.println("Regreso el BeanAdminUsr...");
				}
				else{
					System.out.println("Regreso el BeanAdminUsrFin...");
				}
			}
			else{
				System.out.println("Desglose...");
				bean = (BeanAdminUsr)sesion.getAttribute("BeanAdminUsrFin");
				System.out.println("Regreso el BeanAdminUsrFin...");
			}
			
			if(bean!=null){
				
				bean.setFacultadesUsuarioNoSeleccionadas(getFacultadesNoSeleccionadas(bean));
				bean.setFacultadesUsuarioExtNoSeleccionadas(getFacultadesExtNoSeleccionadas(bean));
				
				System.out.println();
				System.out.println("Contenido del Bean:");
				System.out.println("Firma                                   :" + bean.getFirma());
				System.out.println("Monto                                   :" + bean.getMonto());
				System.out.println("No. Facultades usuario seleccionadas    :" + bean.getFacultadesUsuario().size());
				System.out.println("No. Facultades no seleccionadas         :" + bean.getFacultadesUsuarioNoSeleccionadas().size());
				System.out.println("No. Facultades ext usuario seleccionadas:" + bean.getFacultadesExtUsuario().size());
				System.out.println("No. Facultades ext no seleccionadas     :" + bean.getFacultadesUsuarioExtNoSeleccionadas().size());
				System.out.println();
				
				System.out.println("El bean contiene las siguientes facultades seleccionadas para usuario:");
				for(int iFacultades=0; iFacultades<bean.getFacultadesUsuario().size();iFacultades++)
					System.out.println("Facultad seleccionada [" + (iFacultades + 1) + "]:" + bean.getFacultadesUsuario().get(iFacultades));

				System.out.println();

				System.out.println("El bean contiene las siguientes facultades no seleccionadas para usuario:");
				for(int iFacultades=0; iFacultades<bean.getFacultadesUsuarioNoSeleccionadas().size();iFacultades++)
					System.out.println("Facultad no seleccionada [" + (iFacultades + 1) + "]:" + bean.getFacultadesUsuarioNoSeleccionadas().get(iFacultades));

				System.out.println();
				
				System.out.println("El bean contiene las siguientes facultades extraordinarias seleccionadas:");
				for(int iFacultadesEx=0; iFacultadesEx<bean.getFacultadesExtUsuario().size();iFacultadesEx++)
					System.out.println("Facultad extraordinaria seleccionada [" + (iFacultadesEx + 1) + "]:" + bean.getFacultadesExtUsuario().get(iFacultadesEx));
				
				System.out.println();
				
				System.out.println("El bean contiene las siguientes facultades extraordinarias no seleccionadas:");
				for(int iFacultadesEx=0; iFacultadesEx<bean.getFacultadesUsuarioExtNoSeleccionadas().size();iFacultadesEx++)
					System.out.println("Facultad extraordinaria no seleccionada [" + (iFacultadesEx + 1) + "]:" + bean.getFacultadesUsuarioExtNoSeleccionadas().get(iFacultadesEx));
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println();
		return bean;
	}

	public void setBean(BeanAdminUsr bean) {
		
		System.out.println("setBean...");
		
		try{
			System.out.println("El bean contiene las siguientes facultades para usuario:");
			for(int iFacultades=0; iFacultades<bean.getFacultadesUsuario().size();iFacultades++)
				System.out.println("Facultad [" + (iFacultades + 1) + "]:" + bean.getFacultadesUsuario().get(iFacultades));
			
			System.out.println();
			
			System.out.println("El bean contiene las siguientes facultades extraordinarias:");
			for(int iFacultadesEx=0; iFacultadesEx<bean.getFacultadesExtUsuario().size();iFacultadesEx++)
				System.out.println("Facultad Ext [" + (iFacultadesEx + 1) + "]:" + bean.getFacultadesExtUsuario().get(iFacultadesEx));
				
			getThreadLocalRequest().getSession().setAttribute("BeanAdminUsrFin", bean);
			
			System.out.println();
			System.out.println("Bean guardado...");
		}
		catch(Exception e){
			e.printStackTrace();
		}
				
	}
	
	
	public ArrayList<String> getFacultadesNoSeleccionadas(BeanAdminUsr bean){
		
		ArrayList<String> laltFacultadesUsuarioNoSeleccionadas = new ArrayList<String>();
		
		if(bean==null)
			return laltFacultadesUsuarioNoSeleccionadas;
		
		/**
		 * Convirtiendo todas los grupos de facultades al
		 * formato: claveGrupo&descGrupo@clavePerfil&descPerfil@claveFacultad&descFacultad
		 */
		for(int iGrupo=0; iGrupo<bean.getTodosGrupoPerfiles().size();iGrupo++){
			GrupoPerfiles grupoAux = bean.getTodosGrupoPerfiles().get(iGrupo);
			String claveGrupo = grupoAux.getCveGrupo();
			String descGrupo  = grupoAux.getDescripcion();
			ArrayList<PerfilPrototipo> pefiles = grupoAux.getPerfiles();
			for(int iPerfil=0;iPerfil<pefiles.size();iPerfil++){
				PerfilPrototipo perfilAux = pefiles.get(iPerfil);
				String clavePerfil = perfilAux.getCvePerfil();
				String descPerfil  = perfilAux.getDescripcion();
				ArrayList<Facultad> facultades = perfilAux.getFacultades();
				for(int iFacultad=0;iFacultad<facultades.size();iFacultad++){
					Facultad facultadAux = facultades.get(iFacultad);
					String claveFacultad = facultadAux.getCveFacultad();
					String descFacultad  = facultadAux.getDescripcion();
					laltFacultadesUsuarioNoSeleccionadas.add(claveGrupo    + "&" + descGrupo.replaceAll(","," ")  + "@" + 
							                                 clavePerfil   + "&" + descPerfil.replaceAll(","," ") + "@" + 
							                                 claveFacultad + "&" + descFacultad.replaceAll(","," "));
				}
			}
		}
		
		/**
		 * Eliminando Facultades seleccionadas
		 */
		
		for(int iSeleccionadas=0;iSeleccionadas<bean.getFacultadesUsuario().size();iSeleccionadas++){
			String seleccionadaAux =  bean.getFacultadesUsuario().get(iSeleccionadas);
			for(int iNoSelecionadas=0;iNoSelecionadas<laltFacultadesUsuarioNoSeleccionadas.size();iNoSelecionadas++){
				String noSeleccionadaAux =  laltFacultadesUsuarioNoSeleccionadas.get(iNoSelecionadas);
				if(noSeleccionadaAux.equals(seleccionadaAux)){
					laltFacultadesUsuarioNoSeleccionadas.remove(noSeleccionadaAux);
					break;
				}
			}
		}
		
		return laltFacultadesUsuarioNoSeleccionadas;
		
	}
	
	public ArrayList<String> getFacultadesExtNoSeleccionadas(BeanAdminUsr bean){
		
		ArrayList<String> laltFacultadesUsuarioExtNoSeleccionadas = new ArrayList<String>();
		
		if(bean==null)
			return laltFacultadesUsuarioExtNoSeleccionadas;
		
		/**
		 * Convirtiendo las facultades ext al
		 * formato: claveFacultad|descFacultad
		 */
		for(int iFacultad=0;iFacultad<bean.getTodasFacultadesExt().size();iFacultad++){
			Facultad facultadExtAux =  bean.getTodasFacultadesExt().get(iFacultad);
			String   claveFacultad  = facultadExtAux.getCveFacultad();
			String   descFacultad   = facultadExtAux.getDescripcion();
			laltFacultadesUsuarioExtNoSeleccionadas.add(claveFacultad   + "&" + descFacultad.replaceAll(","," "));
		}
		
		/**
		 * Eliminando Facultades seleccionadas
		 */
		
		for(int iSeleccionadas=0;iSeleccionadas<bean.getFacultadesExtUsuario().size();iSeleccionadas++){
			String seleccionadaAux =  bean.getFacultadesExtUsuario().get(iSeleccionadas);
			for(int iNoSelecionadas=0;iNoSelecionadas<laltFacultadesUsuarioExtNoSeleccionadas.size();iNoSelecionadas++){
				String noSeleccionadaAux =  laltFacultadesUsuarioExtNoSeleccionadas.get(iNoSelecionadas);
				if(noSeleccionadaAux.equals(seleccionadaAux)){
					laltFacultadesUsuarioExtNoSeleccionadas.remove(noSeleccionadaAux);
					break;
				}
			}
		}
		
		return laltFacultadesUsuarioExtNoSeleccionadas;
		
	}
	
}
