package mx.altec.enlace.gwt.adminusr.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class GrupoPerfiles implements IsSerializable {
	
	
	private String cveGrupo;
	private String descripcion;
	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getDescEstatus() {
		return descEstatus;
	}

	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}

	private String estatus;
	private String descEstatus;
	
	private ArrayList<PerfilPrototipo> perfiles = new ArrayList<PerfilPrototipo>();

	public void setCveGrupo(String cveGrupo) {
		this.cveGrupo = cveGrupo;
	}

	public String getCveGrupo() {
		return cveGrupo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setPerfiles(ArrayList<PerfilPrototipo> perfiles) {
		this.perfiles = perfiles;
	}

	public ArrayList<PerfilPrototipo> getPerfiles() {
		return perfiles;
	}
	
	public void agregaPerfil(PerfilPrototipo perf) {
		perfiles.add(perf);
	}

	/*
	 public Object clone() throws CloneNotSupportedException {
		 GrupoPerfiles tmp = (GrupoPerfiles) super.clone();
		 tmp.setCveGrupo(cveGrupo);
		 tmp.setDescripcion(descripcion);
		 tmp.setPerfiles((ArrayList<PerfilPrototipo>) perfiles.clone());
		 return tmp;
	 }
	 */
	
	 public boolean equals(Object a) {
		 GrupoPerfiles tmp = (GrupoPerfiles)a;
		 if(!tmp.getCveGrupo().equals(this.cveGrupo)) {
			 return false;
		 }
		 if(!tmp.getPerfiles().containsAll(perfiles)) {
			 return false;
		 }
		 return true;
	 }	

}
