package mx.altec.enlace.gwt.adminusr.shared;


import com.google.gwt.user.client.rpc.IsSerializable;
import com.smartgwt.client.widgets.tree.TreeNode;

public class PartsTreeNode extends TreeNode implements IsSerializable{
	
    public PartsTreeNode(String name) {   
        this(name, "", new PartsTreeNode[]{});   
    }
    
    public PartsTreeNode(String name, String id) {   
        this(name, id, new PartsTreeNode[]{});   
    }   

    public PartsTreeNode(String name, String id, PartsTreeNode... children) {   
        setAttribute("Nombre", name);   
        setAttribute("Id", id);   
    }   
}   