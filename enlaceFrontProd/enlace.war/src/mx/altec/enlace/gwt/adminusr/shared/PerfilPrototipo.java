package mx.altec.enlace.gwt.adminusr.shared;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class PerfilPrototipo implements IsSerializable{

	private String cvePerfil;
	private String descripcion;
	private String nombreComercial;
	private boolean estado = true;
	
	private ArrayList <Facultad>facultades = new ArrayList <Facultad>();

	public String getCvePerfil() {
		return cvePerfil;
	}

	public void setCvePerfil(String cvePerfil) {
		this.cvePerfil = cvePerfil;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public ArrayList<Facultad> getFacultades() {
		return facultades;
	}

	public void setFacultades(ArrayList<Facultad> facultades) {
		this.facultades = facultades;
	}
	
	public void agregaFacultad(Facultad fac) {
		facultades.add(fac);
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public boolean getEstado() {
		return estado;
	}	

	/*
	 public Object clone() throws CloneNotSupportedException {
		 PerfilPrototipo tmp = (PerfilPrototipo) super.clone();
		 tmp.setCvePerfil(this.cvePerfil);
		 tmp.setDescripcion(this.descripcion);
		 tmp.setNombreComercial(this.nombreComercial);
		 tmp.setFacultades((ArrayList<Facultad>)this.facultades.clone());
		 return tmp;
	 }
	 */
	
	 public boolean equals(Object a) {
		 PerfilPrototipo tmp = (PerfilPrototipo)a;
		 if(!tmp.getCvePerfil().equals(this.cvePerfil)) {
			 return false;
		 }
		 if(!tmp.getFacultades().containsAll(facultades)) {
			 return false;
		 }
		 return true;
	 }

}
