package mx.altec.enlace.gwt.adminusr.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Facultad implements IsSerializable {
	
	private String cveFacultad;
	private String descripcion;
	private boolean estado = true;

	public String getCveFacultad() {
		return cveFacultad;
	}
	public void setCveFacultad(String cveFacultad) {
		this.cveFacultad = cveFacultad;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public boolean getEstado() {
		return estado;
	}

	/*
	 public Object clone() throws CloneNotSupportedException {
		 Facultad tmp = (Facultad) super.clone();
		 tmp.setCveFacultad(this.cveFacultad);
		 tmp.setDescripcion(this.descripcion);
		 return tmp;
	 }
	 */
	
	 public boolean equals(Object a) {
		 Facultad tmp = (Facultad)a;
		 if(tmp.getCveFacultad().equals(this.getCveFacultad())) {
			 return true;
		 }
		 else {
			 return false;
		 }
	 }

}
