package mx.altec.enlace.utilerias; 
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.ibm.misc.BASE64Decoder;
import mx.altec.enlace.bo.Base64;
import mx.altec.enlace.bo.BusinessException;

/**
 * @author FSW Indra
 *
 */
public class AesCryptoService {

	/** indicativo del tipo de cifrado */
	private static final String TIPO_CIFRADO = "AES";
	/** indicativo del modo de cifrado */
	private static final String MODO_CIFRADO = "AES/CBC/PKCS5Padding";
	/** indicativo del tama�o de la llave de cifrado */
	private static final int KEY_LENGTH = 16;
	/**Constante privada del tipo AesCryptoService que almacena el AesCryptoService */
	private static final  AesCryptoService AES_CRYPTO_SERVICE = new AesCryptoService();
	
	private static String moneda;
	
	/**Metodo que sirve para obtener una instancia del tipo AesCryptoService
	 * @return AesCryptoService
	 */
	public static AesCryptoService getInstance(){
		return AES_CRYPTO_SERVICE;
	}
	
	/**
	 * M�todo que encripta bajo el algoritmo AES-128 en modo CBC
	 * @param cadena Cadena a encriptar
	 * @return String cadena encriptada
	 * @throws BusinessException Excepcion de negocio
	 */
	public static String  aesEncrypt (String cadena, String divisa) throws BusinessException {
		Cipher cipher;
		String cadenaEncrypt = "";
		byte[] cadenaCifrada;
		byte[] cveSimetricaByte;
		byte[] cveSecret = new byte[16];
		byte[] iv = new byte[16];
		char[] encodeBase64;
		String claveSimetrica="";
		moneda=divisa;
		
		try {
			claveSimetrica=obtenerPassword();
			EIGlobal.mensajePorTrace("aesEncrypt::cadena::"+cadena, EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("claveSimetrica::"+claveSimetrica, EIGlobal.NivelLog.INFO);
			cveSimetricaByte = new BASE64Decoder().decodeBuffer(claveSimetrica);
			
			System.arraycopy(cveSimetricaByte, 16, iv,0,16);
			IvParameterSpec ivParam = new IvParameterSpec(iv);
			
			System.arraycopy(cveSimetricaByte, 0, cveSecret, 0, 16);
			SecretKeySpec key = new SecretKeySpec(cveSecret, 0, KEY_LENGTH, TIPO_CIFRADO);
			
			cipher = Cipher.getInstance(MODO_CIFRADO);
			cipher.init(Cipher.ENCRYPT_MODE, key, ivParam);
			
			cadenaCifrada = cipher.doFinal(cadena.getBytes());
			if (cadenaCifrada != null) {
				//codificar Base 64
				encodeBase64 = Base64.encodePortal(cadenaCifrada);
				//Sanitizar la cadena
				cadenaEncrypt = URLEncoder.encode(new String(encodeBase64),"UTF-8");
			}
			
		}catch (NoSuchPaddingException nspe) {
			EIGlobal.mensajePorTrace("NoSuchPadding: "+ new Formateador().formatea(nspe), EIGlobal.NivelLog.ERROR);
		}catch (NoSuchAlgorithmException nsae) {
			EIGlobal.mensajePorTrace("NoSuchAlgorithm: "+ new Formateador().formatea(nsae), EIGlobal.NivelLog.ERROR);
		}catch (InvalidAlgorithmParameterException iape) {
			EIGlobal.mensajePorTrace("InvalidAlgorithmParameter: "+ new Formateador().formatea(iape), EIGlobal.NivelLog.ERROR);
		}catch (InvalidKeyException ike) {
			EIGlobal.mensajePorTrace("InvalidKey: "+ new Formateador().formatea(ike), EIGlobal.NivelLog.ERROR);
		}catch (IllegalBlockSizeException ibse) {
			EIGlobal.mensajePorTrace("IllegalBlockSize: "+ new Formateador().formatea(ibse), EIGlobal.NivelLog.ERROR);
		}catch (IllegalArgumentException iae) {
			EIGlobal.mensajePorTrace("IllegalArgument: "+ new Formateador().formatea(iae), EIGlobal.NivelLog.ERROR);
		}catch (IOException ie) {
			EIGlobal.mensajePorTrace("IO: "+ new Formateador().formatea(ie), EIGlobal.NivelLog.ERROR);
		}catch (ArrayIndexOutOfBoundsException aiobe) {
			EIGlobal.mensajePorTrace("ArrayIndexOutOfBoundsException: "+ new Formateador().formatea(aiobe), EIGlobal.NivelLog.ERROR);
		} catch (javax.crypto.BadPaddingException e) {
			EIGlobal.mensajePorTrace("BadPaddingException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (InvalidKeySpecException e) {
			EIGlobal.mensajePorTrace("InvalidKeySpecException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (UnrecoverableEntryException e) {
			EIGlobal.mensajePorTrace("UnrecoverableEntryException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (CertificateException e) {
			EIGlobal.mensajePorTrace("CertificateException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		} catch (KeyStoreException e) {
			EIGlobal.mensajePorTrace("KeyStoreException" + new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
		}
		return cadenaEncrypt;
	}
	
	
    public static String obtenerPassword() 
    	throws InvalidKeySpecException, UnrecoverableEntryException, CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException {
    	char[] charPassword;
    	KeyStore ks = KeyStore.getInstance("JCEKS");
        ks.load(null, (Global.lLLAVE).toCharArray());
        KeyStore.PasswordProtection passwordProtection = new KeyStore.PasswordProtection((Global.lLLAVE).toCharArray());
    	//*******************INICIA TCS FSW 12/2016***********************************
        
        EIGlobal.mensajePorTrace("aesEncrypt::moneda::"+moneda, EIGlobal.NivelLog.INFO);
        if ("USD".equals(moneda)) {
        	FileInputStream fis = new FileInputStream(TranInterConstantes.ARCHIVOCONFIGURACIONUSD);
        	ks.load(fis, (Global.lLLAVE).toCharArray());
		} else {
			FileInputStream fis = new FileInputStream(TranInterConstantes.ARCHIVOCONFIGURACION);
			ks.load(fis, (Global.lLLAVE).toCharArray());
		}
        if ("USD".equals(moneda)) {
        	KeyStore.SecretKeyEntry skeUSD = (KeyStore.SecretKeyEntry) ks.getEntry(TranInterConstantes.ALIASUSD, passwordProtection);
        	SecretKey pbeksUSD = (SecretKey) skeUSD.getSecretKey();
        	byte[] endecodekeyUSD = pbeksUSD.getEncoded();
        	charPassword = (new String(endecodekeyUSD)).toCharArray();
		} else {
			KeyStore.SecretKeyEntry ske = (KeyStore.SecretKeyEntry) ks.getEntry(TranInterConstantes.ALIAS, passwordProtection);
			SecretKey pbeks = (SecretKey) ske.getSecretKey();
			byte[] endecodekey = pbeks.getEncoded();
			charPassword = (new String(endecodekey)).toCharArray();
		}
      //*******************FIN TCS FSW 12/2016***********************************
        return new String(charPassword);

    }
}