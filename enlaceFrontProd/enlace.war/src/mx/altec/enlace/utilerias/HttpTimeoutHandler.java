package mx.altec.enlace.utilerias;

import java.net.*;
import java.io.IOException;

public class HttpTimeoutHandler extends sun.net.www.protocol.http.Handler
{
	int fiTimeoutVal;
	HttpURLConnectionTimeout fHUCT;
	public HttpTimeoutHandler(int iT) { fiTimeoutVal = iT; }

    protected java.net.URLConnection openConnection(URL u) throws IOException {
		return fHUCT = new HttpURLConnectionTimeout(u, this, fiTimeoutVal);
    }

    String GetProxy() { return proxy; }		// breaking encapsulation
    int GetProxyPort() { return proxyPort; }    // breaking encapsulation

	public void Close() throws Exception
	{
		fHUCT.Close();
	}

	public Socket GetSocket()
	{
		return fHUCT.GetSocket();
	}
}

