/** 
 *   Isban Mexico
 *   Clase: TransferenciasMonitorPlusUtils
 *   Descripcion: Metodos auxiliares para cargar los campos de envio a 
 *   MonitorPlus
 *
 *   Control de Cambios:
 *   Diciembre del 2015 FSW Everis 
 */
package mx.altec.enlace.utilerias;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.CuentasDAO;
import mx.isban.mp.conector.ConectorMP;
/**
 * Clase utilitaria para cargar los campos de envio a Monitor Plus.
 * 
 * @author FSW Everis
 *
 */
public final class TransferenciasMonitorPlusUtils {
    
    /**
     * Constructor privado
     */
    private TransferenciasMonitorPlusUtils() {
    }


    
    /**
     * Envio de Transferencias Multiples
     * @param req Objeto con informacion inherente a la peticion
     * @param trans transaccion
     * @param importe importe de la operacion
     * @param ctaParams parametros de ctas
     * @param nomUsr nombre del usuario
     * @param referen numero de referncia
     * @param tipoOperacion tipo de operacion
     * @param session objeto con informacion de la session
     * @param sucursalOperante sucursal operante enlace
     * @param tipo_cta_destino tipo cuenta destino 
     * @param codError codigo de respuesta
     */
    public static void enviarTMultiple(HttpServletRequest req, String trans,
            String importe, String ctaParams, String nomUsr, String referen,
            String tipoOperacion, BaseResource session, String sucursalOperante, String tipo_cta_destino, String codError) {
        String[] ctas = ctaParams.split("[|]");
        String[] nomClientes = nomUsr.split("[|]"); 
        EIGlobal.mensajePorTrace("--------> ** Entra a enviarTMultiple"
                 + " | Codigo de transaccion " + trans + "|"
                 + " | importe " + importe + "|"
                 + " | Parametro ctaParams "+ ctaParams + "|"
                 + " | Parametros de cuentas origen "+ ctas[0] + "|"
                 + " | Parametros de cuentas destino "+ ctas[1] + "|"
                 + " | Parametro NombreUsr " + nomUsr + "|"
                 + " | Parametro Nombre titular origen " + nomClientes[2] + "|"
                 + " | Parametro Nombre titular origen " + nomClientes[6] + "|"
                 + " | referencia " + referen + "|"
                 + " | tipo operacion " + tipoOperacion + "|"
                 + " | sucursal operante " + sucursalOperante + "|"
                 + " | codigo de respuesta: " +codError+ "|"
                 , EIGlobal.NivelLog.DEBUG);
        // Carga valores por default
        Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, sucursalOperante, ctas[EnlaceMonitorPlusConstants.TMB_MULTPLE_CUENTA_ORIGEN], ctas[EnlaceMonitorPlusConstants.TMB_MULTPLE_CUENTA_DESTINO]);
        String error = mapa.get(90000);
        if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
            EIGlobal.mensajePorTrace(" enviarTMultiple Trx390 no obtuvo datos no se infroma a monitor ", EIGlobal.NivelLog.DEBUG);
            
        }else{
            // Tipo de moneda 3516
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_MON,
                    EnlaceMonitorPlusConstants.COD_MON_NAC);
            // Codigo de transaccion 3501
            mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, trans);
            //  Numero de Trx/PT ID 3548
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, referen);
            String importeF = String.valueOf(new BigDecimal(importe).movePointRight(2).longValue());

            // monto total 3508
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_TOTAL, importeF);
            // monto efectivo 3567
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_EFEC, importeF);
            // monto cheque propio 3549
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEPROP, EnlaceMonitorPlusConstants.CADENA_CERO);
            // monto cheque ajeno 3550
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEAJE, EnlaceMonitorPlusConstants.CADENA_CERO);
            // cuenta de origen 3540
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI,
                    ctas[EnlaceMonitorPlusConstants.TMB_MULTPLE_CUENTA_ORIGEN]);
            // cuenta destinatario 3504
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_DEST,
                    ctas[EnlaceMonitorPlusConstants.TMB_MULTPLE_CUENTA_DESTINO]);
            //  ID  Rastreo  Transferencias 3555
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_RASTREO_TRASFERENCIA, referen);
            // No de documento  cheque 3512
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, sucursalOperante);
            // Numero de Referencia 3563
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, referen);
            // Divisa 17
            mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA, EnlaceMonitorPlusConstants.DIVISA_PESOS );
            // identificador archivo 20
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
            // codigo de respuesta 
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, 
                    (codError.length()>=8)
                    ? codError.substring(4, 8) : EnlaceMonitorPlusConstants.COD_RESP);
            
            // Nva Trx:  3597, 3521,3539,3523,3524, 3571
            // Ceros o Blancos:  3543, 3573, 3570,15,16
            // NA: 3579, 15, 16
            // TMultiple: 3516,3501,3508,3567,3549,3550,3540,3504,33555,3512,3563, 17, 3579
            
            // Envia mapa a Monitor Plus
            EIGlobal.mensajePorTrace("---> Se envia mensaje a MonitorPlus desde **enviarTMultiple** <--- ", EIGlobal.NivelLog.DEBUG);
            ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF345);

        }
        
    }

    
    /**
     * Envio de transferencia interbancaria individual
     * @param req objeto con informacion inherente a la peticion
     * @param array arreglo con informacion de la operacion
     * @param posicion de operacion
     * @param session objeto con informacion de la session
     * @param sucursalOperante sucursal operante enlace
     * @param codErrorOper codigo de respuesta
     * @param numReferencia numero de referencia
     */
    public static void enviarTIBIndividual(HttpServletRequest req, String[][] array, int posicion, BaseResource session, String sucursalOperante, String codErrorOper, String numReferencia) {
        EIGlobal.mensajePorTrace("-------->enviarTIBIndividual<---------- "+"\n"+
                "Posicion "+posicion+"\n"+
                "Nombre Completo del Cliente Titular de la Cuenta Origen "+array[posicion][1]+"\n"+
                "Numero de la Cuenta Origen"+array[posicion][0]+"\n"+
                "Numero de la Cuenta Destino "+array[posicion][2]+"\n"+
                "Nombre del Titular de la Cuenta Destino "+array[posicion][3]+"\n"+
                "Monto Total "+array[posicion][4]+"\n"+
                "Numero de Referencia "+array[posicion][6] +"\n"+
                "Sucursal operante " + sucursalOperante +"\n"+
                "Codigo de respuesta: " +codErrorOper, EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, sucursalOperante, array[posicion][0], EnlaceMonitorPlusConstants.CADENA_VACIA);
        String error = mapa.get(90000);
        if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
            EIGlobal.mensajePorTrace(" enviarTMultiple Trx390 no obtuvo datos no se informa a monitor ", EIGlobal.NivelLog.DEBUG);
            
        }else{
            //Tipo de operacion que identifica la Transaccion que se esta realizando 3501
            mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_DIBT); 
            //Numero de la Cuenta que Origino la Transaccion. 3540
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI, array[posicion][0]);
            //Numero de la Cuenta que fue Destino en la Transaccion 3504
            mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_DEST, array[posicion][2]);
            String importeF = String.valueOf(new BigDecimal(array[posicion][4]).movePointRight(2).longValue());
            //Monto Total en Moneda que se realizo la Transaccion 3508
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_TOTAL, importeF);
            //Monto Total del Efectivo que fue utilizado en la Transaccion 3567
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_EFEC, importeF);
            //Monto Total en Cheques Propios que fue utilizado en la Transaccion 3549
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEPROP, EnlaceMonitorPlusConstants.CADENA_CERO);
            //Monto Total en Cheques de Bancos Ajenos que fue utilizado en la Transaccion 3550
            mapa.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEAJE, EnlaceMonitorPlusConstants.CADENA_CERO);
            //Numero de Referencia 3563
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, numReferencia);
            //No de documento  cheque 3512
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, numReferencia);
            //Numero de Operacion 3548        
            mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, numReferencia);
            //Tipo de Transaccion 3579
            mapa.put(EnlaceMonitorPlusConstants.KEY_TIP_TRANS, EnlaceMonitorPlusConstants.INDIV);
            // Tipo de moneda 3516
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_MON, EnlaceMonitorPlusConstants.COD_MON_NAC);
             //clave Transferencia Interbancaria 3543
            mapa.put(EnlaceMonitorPlusConstants.KEY_CVE_TRASNFER_INTERBANCARIA, numReferencia);
            //Numero de rastreo de trasferencia 3555
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_RASTREO_TRASFERENCIA, numReferencia);
            //Sucursal receptora 3573
            mapa.put(EnlaceMonitorPlusConstants.KEY_SUC_RECEP,(array[posicion][2]).substring(3, 6));
            //Banco destino 3570
             mapa.put(EnlaceMonitorPlusConstants.KEY_BANCO_DESTINO, (array[posicion][2]).substring(0, 3));
            // Banco cta asociada 15
            mapa.put(EnlaceMonitorPlusConstants.KEY_BANC_CTAAS, (array[posicion][2]).substring(0, 3));
            // Pais cta asociada 16
            mapa.put(EnlaceMonitorPlusConstants.KEY_PAIS_CTAAS, EnlaceMonitorPlusConstants.PAIS_BANC_CTAAS);
            // Divisa 17
            mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA, EnlaceMonitorPlusConstants.DIVISA_PESOS );
            // identificador archivo 20
            mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
            if((array[posicion][2]).length() == 18){
                mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, EnlaceMonitorPlusConstants.CLABE);
            }else if((array[posicion][2]).length() == 16){
                mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_CTA, EnlaceMonitorPlusConstants.TARJ_DEBITO);
            }
            // codigo de respuesta 3527
            mapa.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, 
                    (codErrorOper.length()>=8)
                    ? codErrorOper.substring(4, 8) : EnlaceMonitorPlusConstants.COD_RESP);
            //Nombre Cliente Titular Cta. Destino 3524
            mapa.put(EnlaceMonitorPlusConstants.KEY_NOMBRE_DESTINO, array[posicion][3]);

            
             //Nva Trx: 3597, 3521,3539,3523, 3524, 3571
             //TIBIndividual: 3501, 3540, 3504, 3508, 3567, 3549, 3550, 3563, 3512, 3548, 3579, 3516, 3543, 3555, 3570 
            //No Aplica: 3520, 3587, 3564, 3760, 3761, 3762, 3763, 3569, 3572
            //ceros o blancos:  3573
            
             EIGlobal.mensajePorTrace("-------->Se envia TIBIndividual a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
            ConectorMP.enviaMsg(mapa, EnlaceMonitorPlusConstants.MSG_ADF345);

        }

    }
    
    /**
     * Envio de transferencia interbancaria por archivo
     * @param req objeto con informacion inherente a la peticion
     * @param array arreglo con informacion de la operacion
     * @param folioArchivo arreglo con informacion de la operacion
     * @param session  informacion de la session
     * @param sucursalOperante sucursal operante enlace
     * @param contrato Contrato enlace 
     * @param codErrorOper codigo de respuesta
     */
    public static void enviarTIBArchivo(HttpServletRequest req, String[][] array, String folioArchivo, BaseResource session, String sucursalOperante, String contrato, String codErrorOper) {
        EIGlobal.mensajePorTrace("-------->enviarTIBArchivo<---------- "+"\n"+
                "Titular Cuenta Origen "+array[0][8]+"\n"+
                "Folio Archivo "+folioArchivo+"\n"+
                "Numero de la Cuenta Origen"+array[0][6]+"\n"+
                "Numero de la Cuenta Destino "+array[0][9]+"\n"+
                "Nombre del Titular de la Cuenta Destino "+array[0][11]+"\n"+
                "Monto Total "+array[0][13]+"\n"+
                "Direccion IP donde fue realizada la Transaccion "+req.getLocalAddr()+"\n"+
                "Sucursal operante " + sucursalOperante +"\n"+
                "Contrato " + contrato +"\n"+
                "Codigo de respuesta: " +codErrorOper
                , EIGlobal.NivelLog.DEBUG);
        if(ConectorMP.isInit()){
        Map<Integer, String> mapa = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, sucursalOperante,  EnlaceMonitorPlusConstants.CADENA_VACIA, EnlaceMonitorPlusConstants.CADENA_VACIA);
        //Tipo de operacion que identifica la Transaccion que se esta realizando 3501
        mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_DIBT);
        //Numero de Referencia 3563
        mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER, folioArchivo);
        //Numero de Operacion 3548        
        mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, folioArchivo);
        //Tipo de Transaccion 3579
        mapa.put(EnlaceMonitorPlusConstants.KEY_TIP_TRANS, EnlaceMonitorPlusConstants.MASIVA);
        //Tipo de moneda 3516
        mapa.put(EnlaceMonitorPlusConstants.KEY_COD_MON, EnlaceMonitorPlusConstants.COD_MON_NAC);
         //No de documento  cheque 3512
         mapa.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, folioArchivo);
         //clave Transferencia Interbancaria 3543
        mapa.put(EnlaceMonitorPlusConstants.KEY_CVE_TRASNFER_INTERBANCARIA, folioArchivo);
        //Numero de rastreo de trasferencia 3555
        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_RASTREO_TRASFERENCIA, folioArchivo);
        // Divisa 17
        mapa.put(EnlaceMonitorPlusConstants.KEY_DIVISA, EnlaceMonitorPlusConstants.DIVISA_PESOS );
        // identificador archivo 20
        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
        // codigo de respuesta 3527
        mapa.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, 
                (codErrorOper.length()>=8)
                ? codErrorOper.substring(4, 8) : EnlaceMonitorPlusConstants.COD_RESP);

        
        // Nva Trx: 3597, 3521, 3523, 3524,3571
         // TIBArchivo: 3501, 3563, 3548, 3579, 3516, 3512, 3543, 3555
        // No Aplica:  3540, 3504, 3508, 3567, 3549, 3550
        // ceros o blancos:  3573, 3570 
        
         EIGlobal.mensajePorTrace("-------->Se envia TIBArchivo a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("-------->Folio de archivo <---------- "+folioArchivo, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("-------->Numero de contrato <---------- "+session.getContractNumber(), EIGlobal.NivelLog.DEBUG);
        ConectorMP.enviarInfo(mapa, folioArchivo, EnlaceMonitorPlusConstants.MSG_ADF345, session.getContractNumber());
        }else{
        	EIGlobal.mensajePorTrace("El conector no esta inicializado, no se envia a MP ", EIGlobal.NivelLog.DEBUG);
        }
    }

    /**
     * Envio de transferencia Mismo Banco individual
     * @param cadena valores de la operacion
     * @param session objeto con informacion de la session
     * @param req Objeto con informacion inherente a la peticion
     * @param tipoCtaDestino tipo de cuenta destino
     * @param referencia referencia enlace
     * @param codErrorOper codigo de respuesta
     */
    public static void enviarTMBIndividual(String cadena, BaseResource session, HttpServletRequest req,
            String tipoCtaDestino, String referencia, String codErrorOper) {
        String[] datos= cadena.split("[|]"); 
         EIGlobal.mensajePorTrace("--------> ** Entra a enviarTMBIndividual |"
                 + "Numero de cuenta origen, cuenta destino Sucursal Operante y el importe: " + cadena+ "|"
                 + "tipo de moneda: "+(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA))            
                 + "tipo de cuenta destino: " + tipoCtaDestino + "|"
                 + "referencia: " + referencia + "|"
                 + "sucursal operante: " + datos[2] + "|"
                 + "Usuario Destino: " +datos [4]+ "|"
                 + "Usuario Origen: " + datos[5] + "|"
                 + " Cta Origen: " +datos [0]+ "|"
                 + "Cta Origen: " +datos [1]+ "|"
                 + "Codigo de Respuesta: " +codErrorOper+ "|"
                 , EIGlobal.NivelLog.DEBUG);
        if ((EnlaceMonitorPlusConstants.MON_NAC.equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA)) || EnlaceMonitorPlusConstants.DOLAR
            .equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA)))) {
            // crear el mapa para monitor Plus 
            Map<Integer, String> map = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, datos[2], datos[0], datos[1]);
            String error = map.get(90000);
            if(error != null && error != EnlaceMonitorPlusConstants.CADENA_VACIA){
                EIGlobal.mensajePorTrace(" enviarTMBIndividual Trx390 no obtuvo datos no se informa a MP ", EIGlobal.NivelLog.DEBUG);
                
            }else{
                //Codigo de la Moneda con la que se realizo la Transaccion 3516
                map.put(EnlaceMonitorPlusConstants.KEY_COD_MON,(EnlaceMonitorPlusConstants.DOLAR.equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA))) ? 
                        EnlaceMonitorPlusConstants.COD_MON_DOL : EnlaceMonitorPlusConstants.COD_MON_NAC);                    
                //tipo de transaccion 3579
                map.put(EnlaceMonitorPlusConstants.KEY_TIP_TRANS, EnlaceMonitorPlusConstants.INDIV);
                //tipo de servicio 3512
                map.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, referencia);
                //numero trx 3548
                map.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, referencia);
                   // Numero de Cuenta Origen     3540
                map.put(EnlaceMonitorPlusConstants.KEY_CTA_ORI, datos[0]);    
                // Numero de Cuenta a la que se hara el Abono 3504
                map.put(EnlaceMonitorPlusConstants. KEY_CTA_DEST, datos[1]);     
                // Codigo que identifica la Transaccion 3501
                map.put(EnlaceMonitorPlusConstants.KEY_TRANS,EnlaceMonitorPlusConstants.COD_TRAN_TMB);
                String importe = String.valueOf(new BigDecimal(datos[3]).movePointRight(2).longValue());
                // Monto Total 3508
                map.put(EnlaceMonitorPlusConstants.KEY_MONT_TOTAL,importe );
                // Monto Efectivo 3567
                map.put(EnlaceMonitorPlusConstants.KEY_MONT_EFEC,importe);    
                // Monto Cheque Propio 3549
                map.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEPROP, EnlaceMonitorPlusConstants.CADENA_CERO);
                // Monto Cheque Ajeno 3550
                map.put(EnlaceMonitorPlusConstants.KEY_MONT_CHEAJE, EnlaceMonitorPlusConstants.CADENA_CERO); 
                //Numero de rastreo de trasferencia 3555
                map.put(EnlaceMonitorPlusConstants.KEY_ID_RASTREO_TRASFERENCIA, referencia);
                // Numero de Referencia 3563            
                map.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER , referencia);
                // Divisa 17
                map.put(EnlaceMonitorPlusConstants.KEY_DIVISA,(EnlaceMonitorPlusConstants.DOLAR.equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA))) ? 
                        EnlaceMonitorPlusConstants.DIVISA_DOLARES : EnlaceMonitorPlusConstants.DIVISA_PESOS);                    
                // identificador archivo 20
                map.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, EnlaceMonitorPlusConstants.IDEN_SIN_ARCHIVO);
                // codigo de respuesta 3527
                map.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, 
                        (codErrorOper.length()>=8)
                        ? codErrorOper.substring(4, 8) : EnlaceMonitorPlusConstants.COD_RESP);
                
                
                // Nva trx : 3597,3521,3539,3557,3524,3558,3559,3560,3561,3537,3596,3535,3620,3571,3566,3641
                // default: 1,2,3,4,5,6,7,9,3505,3507,3518,3519,3536,3532,3506,3750,3752,3753,10 ** 3533,3502,3522 **.
                // DefaultTransferencia: 8,3545,3509,3510,3614,3751,3754,3514,3529,3530,3534,3511,3640,3503,3605,3515,3538,3544,3525,3576,3527,3532,3552,3553,3554,3556,3564,3761,3762,3763,3568,3569,3572,3587,3588,3589,3590,3591,3592,3593,3598,3755,3756,3757,3758,3759,3764,3765,3767,3766,3546 ** 3517 **.
                // TMBindividual: 3516,3579,3573,3512,3548,3540,3504,3501,3508,3567,3549,3550,3563.
                // No Aplica: 15, 16, 3543,3570.
                 

                EIGlobal.mensajePorTrace("-------->Se envia TMBIndividual a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
               ConectorMP.enviaMsg(map, EnlaceMonitorPlusConstants.MSG_ADF345);    

            }

        }
     }
    
      
    /**
     * Realiza el mapeo de los campos a monitor plus desde Transferencia por archivo
     * @param req Objeto con informacion inherente a la peticion
     * @param session variable de session    
     * @param regionOper region de operacion
     * @param sucursalOperante sucursal operante enlace
     * @param codErrorOper codigo de respuesta
     */
    public static void enviarTMBArchivo(HttpServletRequest req,     
            BaseResource session, String regionOper, String sucursalOperante, String codErrorOper) {        
        HttpSession sess = req.getSession();
        session = (BaseResource) sess.getAttribute("session");
        EIGlobal.mensajePorTrace("--------> ** Entra a enviarTMBArchivo|"
                 + "Sucursal Operante "    +regionOper    + "|"
                 + "tipo de moneda: "+(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA))+ "|"
                 + "sucursal Operante: " + sucursalOperante + "|"
                 + "Codigo de Respuesta: " +codErrorOper+ "|"
                 , EIGlobal.NivelLog.DEBUG);
        // crear el mapa para monitor Plus
        if(ConectorMP.isInit()){
        Map<Integer, String> map = EnlaceMonitorPlusConstants.cargaValoresDefaultTransferencia(req, session, sucursalOperante, EnlaceMonitorPlusConstants.CADENA_VACIA, EnlaceMonitorPlusConstants.CADENA_VACIA);

        if ((EnlaceMonitorPlusConstants.MON_NAC.equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA))
                || EnlaceMonitorPlusConstants.DOLAR
                        .equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA)))) {
            //Codigo de la Moneda con la que se realizo la Transaccion 3516
            map.put(EnlaceMonitorPlusConstants.KEY_COD_MON,
                    (EnlaceMonitorPlusConstants.DOLAR
                            .equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA)))
                                    ? EnlaceMonitorPlusConstants.COD_MON_DOL : EnlaceMonitorPlusConstants.COD_MON_NAC);
            // tipo de servicio 3512
            map.put(EnlaceMonitorPlusConstants.KEY_NUM_DOC_CHEQUE, session.getFolioArchivo());
            //numero trx 3548
            map.put(EnlaceMonitorPlusConstants.KEY_NUM_TRX, session.getFolioArchivo());               
            // tipo de transaccion 3579            
            map.put(EnlaceMonitorPlusConstants.KEY_TIP_TRANS, EnlaceMonitorPlusConstants.MASIVA);
            // Numero de Referencia 3563            
            map.put(EnlaceMonitorPlusConstants.KEY_NUM_REFER , session.getFolioArchivo());    
            // Codigo que identifica la Transaccion 3501
            map.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_TMB);
            // Divisa 17
            map.put(EnlaceMonitorPlusConstants.KEY_DIVISA,
                    (EnlaceMonitorPlusConstants.DOLAR
                            .equals(req.getSession().getAttribute(EnlaceMonitorPlusConstants.MONEDA)))
                                    ? EnlaceMonitorPlusConstants.DIVISA_DOLARES : EnlaceMonitorPlusConstants.DIVISA_PESOS);
            
            // identificador archivo 20
            map.put(EnlaceMonitorPlusConstants.KEY_ID_CTAAS_ARCHIVO, EnlaceMonitorPlusConstants.IDEN_CON_ARCHIVO);
            // codigo de respuesta 3527
            map.put(EnlaceMonitorPlusConstants.KEY_COD_RESPUESTA, 
                    (codErrorOper.length()>=8)
                    ? codErrorOper.substring(4, 8) : EnlaceMonitorPlusConstants.COD_RESP);

            
             // Nva Trx: 3597,3521,3523,3524,,3557,3560,3561,3537,3596,3535,3620,3571,3566,3641
             // default: -1,-2,-3,-4,-5,-6,-7,-9,3505,3507,3518,3519,3536,3532,3506,3750,3752,3753,-10 ** 3533,3502,3522 **.
             // DefaultTransferencia: -8,3545,3509,3510,3614,3751,3754,3514,3529,3530,3534,3511,3640,3503,3605,3515,3538,3544,3525,3576,3527,3552,3553,3554,3556,3564,3761,3762,3763,3568,3569,3572,3587,3588,3589,3590,3591,3592,3593,3598,3755,3756,3757,3758,3759,3764,3765,3767,3766,3546 ** 3517 **.
             // TMBArchivo: 3516,3573,3512,3539,3548,3579,3501,3555,3563.
             // No Aplica:  3540, 3504, 3508, 3567, 3549, 3550, 15, 16, 3543, 3570, 3555

            EIGlobal.mensajePorTrace("-------->Se envia TMB por archivo a Monitor Plus<---------- ", EIGlobal.NivelLog.DEBUG);
            ConectorMP.enviarInfo(map, session.getFolioArchivo(), EnlaceMonitorPlusConstants.MSG_ADF345,
                    session.getContractNumber());
          }   
        }else{
        	EIGlobal.mensajePorTrace("El conector no esta inicializado, no se envia a MP ", EIGlobal.NivelLog.DEBUG);
        }
    }
    
    
}

