// Decompiled by Jad v1.5.7d. Copyright 2000 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MultipartRequest.java

package mx.altec.enlace.utilerias;

//import com.afina.servlet.multipart.FilePart;
//import com.afina.servlet.multipart.MultipartParser;
//import com.afina.servlet.multipart.ParamPart;
//import com.afina.servlet.multipart.Part;
import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.afina.servlet:
//            UploadedFile

public class MultipartRequest
{

    public MultipartRequest(ServletRequest servletrequest, String s)
        throws IOException
    {
        this((HttpServletRequest)servletrequest, s);
    }

    public MultipartRequest(ServletRequest servletrequest, String s, int i)
        throws IOException
    {
        this((HttpServletRequest)servletrequest, s, i);
    }

    public MultipartRequest(HttpServletRequest httpservletrequest, String s)
        throws IOException
    {
        this(httpservletrequest, s, 0x100000);
    }

    public MultipartRequest(HttpServletRequest httpservletrequest, String s, int i)
        throws IOException
    {
        parameters = new Hashtable();
        files = new Hashtable();
        if(httpservletrequest == null)
            throw new IllegalArgumentException("request cannot be null");
        if(s == null)
            throw new IllegalArgumentException("saveDirectory cannot be null");
        if(i <= 0)
            throw new IllegalArgumentException("maxPostSize must be positive");
        dir = new File(s);
        if(!dir.isDirectory())
            throw new IllegalArgumentException("Not a directory: " + s);
        if(!dir.canWrite())
            throw new IllegalArgumentException("Not writable: " + s);
        parser = new MultipartParser(httpservletrequest, i);
        Part part;
        while((part = parser.readNextPart()) != null) 
        {
            String s1 = part.getName();
            if(part.isParam())
            {
                ParamPart parampart = (ParamPart)part;
                String s2 = parampart.getStringValue();
                Vector vector = (Vector)parameters.get(s1);
                if(vector == null)
                {
                    vector = new Vector();
                    parameters.put(s1, vector);
                }
                vector.addElement(s2);
            } else
            if(part.isFile())
            {
                FilePart filepart = (FilePart)part;
                String s3 = filepart.getFileName();
                if(s3 != null)
                {
                    filepart.writeTo(dir);
                    files.put(s1, new UploadedFile(dir.toString(), s3, filepart.getContentType()));
                } else
                {
                    files.put(s1, new UploadedFile(null, null, null));
                }
            }
        }
    }

    public String getContentType(String s)
    {
        try
        {
            UploadedFile uploadedfile = (UploadedFile)files.get(s);
            return uploadedfile.getContentType();
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    public File getFile(String s)
    {
        try
        {
            UploadedFile uploadedfile = (UploadedFile)files.get(s);
            return uploadedfile.getFile();
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    public Enumeration getFileNames()
    {
        return files.keys();
    }

    public String getFilesystemName(String s)
    {
        try
        {
            UploadedFile uploadedfile = (UploadedFile)files.get(s);
            return uploadedfile.getFilesystemName();
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    public String getParameter(String s)
    {
        try
        {
            Vector vector = (Vector)parameters.get(s);
            if(vector == null || vector.size() == 0)
            {
                return null;
            } else
            {
                String s1 = (String)vector.elementAt(vector.size() - 1);
                return s1;
            }
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    public Enumeration getParameterNames()
    {
        return parameters.keys();
    }

    public String[] getParameterValues(String s)
    {
        try
        {
            Vector vector = (Vector)parameters.get(s);
            if(vector == null || vector.size() == 0)
            {
                return null;
            } else
            {
                String as[] = new String[vector.size()];
                vector.copyInto(as);
                return as;
            }
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    private static final int DEFAULT_MAX_POST_SIZE = 0x100000;
    private File dir;
    private Hashtable parameters;
    private Hashtable files;
    private MultipartParser parser;
}
