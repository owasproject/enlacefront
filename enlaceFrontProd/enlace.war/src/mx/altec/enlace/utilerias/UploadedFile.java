// Decompiled by Jad v1.5.7d. Copyright 2000 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/SiliconValley/Bridge/8617/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   MultipartRequest.java

package mx.altec.enlace.utilerias;

import java.io.File;

// Referenced classes of package com.afina.servlet:
//            MultipartRequest

class UploadedFile
{

    UploadedFile(String s, String s1, String s2)
    {
        dir = s;
        filename = s1;
        type = s2;
    }

    public String getContentType()
    {
        return type;
    }

    public File getFile()
    {
        if(dir == null || filename == null)
            return null;
        else
            return new File(dir + File.separator + filename);
    }

    public String getFilesystemName()
    {
        return filename;
    }

    private String dir;
    private String filename;
    private String type;
}
