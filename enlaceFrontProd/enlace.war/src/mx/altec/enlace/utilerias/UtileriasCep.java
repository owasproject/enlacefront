package mx.altec.enlace.utilerias;

public class UtileriasCep {
	/**
	 * Metodo que realiza el formateo de la fecha
	 * @param fecha fecha a formatear
	 * @return String Fecha formateada
	 */
	public static String formateaFecha(String fecha){
		/**
		 * 09/06/2015
		 * 20150421
		 * **/
		StringBuffer fechaOrdenada=new StringBuffer();
		String[] Datos = null;
		Datos = fecha.split("/");
		fechaOrdenada.append(Datos[2]);
		fechaOrdenada.append(Datos[1]);
		fechaOrdenada.append(Datos[0]);
		return fechaOrdenada.toString();
	}
	/**
	 * Metodo para realizar las validaciones del campo si viene nulo o vacio
	 * @param campo valor del campo
	 * @return boolean bandera
	 */
	public static boolean validaCampo(String campo){
		boolean bandera=false;
		if(null!=campo&&!"null".equals(campo)&&!"".equals(campo)&&!"NULL".equals(campo)){
			bandera=true;
		}
		return bandera;
	}

}
