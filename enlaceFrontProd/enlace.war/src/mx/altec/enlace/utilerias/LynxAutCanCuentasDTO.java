package mx.altec.enlace.utilerias;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


/**
 * La clase LynxAutCanCuentasDTO sirve para el transporte de la informacion para el envio de la informacion a lynx para el modulo de autorizacion y cancelacion de cuentas.
 */
public class LynxAutCanCuentasDTO {
    
    /** La Constante TIPO_OPERACION_AUTORIZACION. Indica la operacion de autorizacion en el modulo */
    public static final String TIPO_OPERACION_AUTORIZACION = "A";
    
    /** La Constante TIPO_OPERACION_CANCELACION. Indica la operacion de cancelacion en el modulo */
    public static final String TIPO_OPERACION_CANCELACION = "C";
    
    /** La Constante NOM_ESTATUS_AUTORIZAR. Indica por medio de la descripcion el estatus que es un estatus de pendiende de activar */
    public static final String NOM_ESTATUS_AUTORIZAR = "Pendiente por activar";
    
    /** La Constante TIPO_CUENTA_INTERNACIONAL. Indica que es un tipo de cuenta internacional */
    public static final char TIPO_CUENTA_INTERNACIONAL = 'I';
    
    /** La Constante TIPO_CUENTA_EXTERNA. Indica que es un tipo de cuenta externa */
    public static final char TIPO_CUENTA_EXTERNA = 'E';
    
    /** La Constante TIPO_CUENTA_PROPIA. Indica que es un tipo de cuenta propia de la entidad */
    public static final char TIPO_CUENTA_PROPIA = 'P';
    
    /** La Constante TIPO_CUENTA_TERCERA. Indica que es un tipo de cuenta de un tercero*/
    public static final char TIPO_CUENTA_TERCERA = 'T';
    
    /** La Constante ESTATUS_PENDIENTE_AUTORIZAR. Indica el estatus pendiente pos activar de una cuenta */
    public static final String ESTATUS_PENDIENTE_AUTORIZAR = "Pendiente por autorizar";
    
    /** La Constante TIPO_OPERACION_AUTORIZACION. Indica la operacion de autorizacion en el modulo */
    public static final String TIPO_SOLICITUD_ALTA = "A";
    
    /** La Constante TIPO_OPERACION_CANCELACION. Indica la operacion de cancelacion en el modulo */
    public static final String TIPO_SOLICITUD_BAJA = "B";
    
    
    /** La variable clase cuenta contiene la informacion de que clase de cuenta  */
    private String claseCuenta;
    
    /** La variable num cuenta contiene la informacion del numero de cuenta. */
    private String numCuenta;
    
    /** La variable titular contiene la informacion del titular de la cuenta. */
    private String titular;
    
    /** La variable fecha registro contiene la informacion de la fecha de registro de la cuenta. */
    private String fechaRegistro;
    
    /** La variable origen alta contiene la informacion del canal origen de la cuenta. */
    private String origenAlta;
    
    /** La variable tipo operacion indica el tipo de operacion en donde se usan las constantes de la clase. */
    private String tipoOperacion;
    
    /** La variable nom estatus nombre del estatus de la cuenta. */
    private String nomEstatus;
    
    /** La variable descripcion status contiene la descripcion del estatus de la cuenta. */
    private String descripcionStatus;
    
    /** La variable tipo cuenta indica el tipo de cuenta, se usan las constantes de la clase. */
    private String tipoCuenta;
    
    /** La variable num contrato indica el numero de contraro que esta asignado. */
    private String numContrato;
    
    /** La variable num contrato indica el tipo de accion A = Alta, B =. */
    private String tipoAccion;
    
    /**
     * Obtiene el valor de la variable clase cuenta.
     *
     * @return el clase cuenta
     */
    public String getClaseCuenta() {
        return claseCuenta;
    }
    
    /**
     * Coloca el valor de clase cuenta.
     *
     * @param claseCuenta es el nuevo valor de clase cuenta
     */
    public void setClaseCuenta(String claseCuenta) {
        this.claseCuenta = claseCuenta;
    }
    
    /**
     * Obtiene el valor de la variable num cuenta.
     *
     * @return el num cuenta
     */
    public String getNumCuenta() {
        return numCuenta;
    }
    
    /**
     * Coloca el valor de num cuenta.
     *
     * @param numCuenta es el nuevo valor de num cuenta
     */
    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }
    
    /**
     * Obtiene el valor de la variable titular.
     *
     * @return el titular
     */
    public String getTitular() {
        return titular;
    }
    
    /**
     * Coloca el valor de titular.
     *
     * @param titular es el nuevo valor de titular
     */
    public void setTitular(String titular) {
        this.titular = titular;
    }
    
    /**
     * Obtiene el valor de la variable fecha registro.
     *
     * @return el fecha registro
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }
    
    /**
     * Coloca el valor de fecha registro.
     *
     * @param fechaRegistro es el nuevo valor de fecha registro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    /**
     * Obtiene el valor de la variable origen alta.
     *
     * @return el origen alta
     */
    public String getOrigenAlta() {
        return origenAlta;
    }
    
    /**
     * Coloca el valor de origen alta.
     *
     * @param origenAlta es el nuevo valor de origen alta
     */
    public void setOrigenAlta(String origenAlta) {
        this.origenAlta = origenAlta;
    }
    
    /**
     * Obtiene el valor de la variable tipo operacion.
     *
     * @return el tipo operacion
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }
    
    /**
     * Coloca el valor de tipo operacion.
     *
     * @param tipoOperacion es el nuevo valor de tipo operacion
     */
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    
    /**
     * Obtiene el valor de la variable nom estatus.
     *
     * @return el nom estatus
     */
    public String getNomEstatus() {
        return nomEstatus;
    }
    
    /**
     * Coloca el valor de nom estatus.
     *
     * @param nomEstatus es el nuevo valor de nom estatus
     */
    public void setNomEstatus(String nomEstatus) {
        this.nomEstatus = nomEstatus;
    }
    
    /**
     * Obtiene el valor de la variable descripcion status.
     *
     * @return el descripcion status
     */
    public String getDescripcionStatus() {
        return descripcionStatus;
    }
    
    /**
     * Coloca el valor de descripcion status.
     *
     * @param descripcionStatus es el nuevo valor de descripcion status
     */
    public void setDescripcionStatus(String descripcionStatus) {
        this.descripcionStatus = descripcionStatus;
    }
    
    /**
     * Obtiene el valor de la variable tipo cuenta.
     *
     * @return el tipo cuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }
    
    /**
     * Coloca el valor de tipo cuenta.
     *
     * @param tipoCuenta es el nuevo valor de tipo cuenta
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    /**
     * Obtiene el valor de la variable num contrato.
     *
     * @return el num contrato
     */
    public String getNumContrato() {
        return numContrato;
    }
    
    /**
     * Coloca el valor de num contrato.
     *
     * @param numContrato es el nuevo valor de num contrato
     */
    public void setNumContrato(String numContrato) {
        this.numContrato = numContrato;
    }
    
    /**
     * Obtiene el valor de la variable tipo accion.
     *
     * @return el tipo accion
     */
    public String getTipoAccion() {
        return tipoAccion;
    }
    
    /**
     * Coloca el valor de tipo accion.
     *
     * @param tipoAccion es el nuevo valor de tipo accion
     */
    public void setTipoAccion(String tipoAccion) {
        this.tipoAccion = tipoAccion;
    }
    
    /**
     * Tiene la funcion de la generacion del mapa lynx para su envio por medio del conector.
     *
     * @param req request de la peticion
     * @param contrato numero de contrato
     * @param idUsrCta usuario asociado al contrato
     * @return mapa cargado con los valores correspondientes a la transaccion
     */
    public Map<Integer, String> generaMapaLynx(HttpServletRequest req, 
            String contrato, String idUsrCta) {
        Map<Integer, String> mapaLynx = new HashMap<Integer, String>();
        
        EnlaceLynxConstants.cargaValoresDefaultNM(mapaLynx, req);
        DateFormat formatoHora = new SimpleDateFormat(
                EnlaceLynxConstants.FORMATO_FCH_ID_TRANS_UNI, 
                EnlaceLynxConstants.LOCALE_MX);
        DateFormat fmtFecha = new SimpleDateFormat(
                EnlaceLynxConstants.FORMATO_FCH_OPE_ENV, 
                EnlaceLynxConstants.LOCALE_MX);
        Date fechaOper = new Date();
        mapaLynx.put(3, formatoHora.format(fechaOper).concat(this.numCuenta));
        mapaLynx.put(23, fmtFecha.format(fechaOper));
        mapaLynx.put(24, mapaLynx.get(23));
        mapaLynx.put(7, idUsrCta);
        boolean esAlta = (TIPO_OPERACION_AUTORIZACION.equals(this.tipoOperacion)
                && TIPO_SOLICITUD_ALTA.equals(this.tipoAccion));
        boolean esMovil = this.numCuenta.length() == 10;
		switch (this.tipoCuenta.charAt(0)) {
		case 'E':
			mapaLynx.put(28, EnlaceLynxConstants.TIPOCTA_INTERBANCARIAS);
            if (esAlta) {
                mapaLynx.put(21, (esMovil ? EnlaceLynxConstants.TIPOTRAN_ACMOB
                        : EnlaceLynxConstants.TIPOTRAN_ACTOB));
            } else {
                mapaLynx.put(21, (esMovil ? EnlaceLynxConstants.TIPOTRAN_BCMOB
                        : EnlaceLynxConstants.TIPOTRAN_BCTOB));
            }
			break;
		case 'I':
			mapaLynx.put(28, EnlaceLynxConstants.TIPOCTA_INTERNACIONALES);
            mapaLynx.put(21, (esAlta ? EnlaceLynxConstants.TIPOTRAN_ACTBI
                    : EnlaceLynxConstants.TIPOTRAN_BCI));
			break;
		case 'T':
		case 'P':
			mapaLynx.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
            if (esMovil) {
                mapaLynx.put(21, (esAlta ? EnlaceLynxConstants.TIPOTRAN_ACMMB
                        : EnlaceLynxConstants.TIPOTRAN_BCMMB));
            } else {
                mapaLynx.put(21, (esAlta ? EnlaceLynxConstants.TIPOTRAN_ACTMB
                        : EnlaceLynxConstants.TIPOTRAN_BCTMB));
            }
			break;
		default:
			mapaLynx.put(28, EnlaceLynxConstants.TIPOCTA_MISMO_BANCO);
            mapaLynx.put(21, (esAlta ? EnlaceLynxConstants.TIPOTRAN_ACTMB
                    : EnlaceLynxConstants.TIPOTRAN_BCTMB));
			break;
		}

        mapaLynx.put(29, this.numCuenta);
        mapaLynx.put(30, EnlaceLynxConstants.ENT_ORIGEN);
        mapaLynx.put(37, EnlaceLynxConstants.CODRESP_OK);
        
        return mapaLynx;
    }

}
