/**
 * Isban Mexico
 *   Clase: EdoCtaConstantes.java
 *   Descripción: Clase utilizada para almacenar las constantes del modulo 
 *   de Estados de Cuenta
 *
 *   Control de Cambios:
 *   1.0 12/02/2014 Isban Mexico
 */

package mx.altec.enlace.utilerias;

public class EdoCtaConstantes {
	/** Concepto para token valido en descarga de estados de cuenta XML de Ingreso. */
	public static final String TOKEN_VALIDO_DESCARGA_XML_INGRESO = "Confirma Descarga de Estados de Cuenta de Ingreso";

	/** Concepto para token valido en descarga de estados de cuenta XML de Egreso. */
	public static final String TOKEN_VALIDO_DESCARGA_XML_EGRESO = "Confirma Descarga de Estados de Cuenta de Egreso";

	/** Codigo de operacion para ingreso al modulo de paperless TDC masivo. */
	public static final String EA_ENTRA_MOD_PAPERLESS_MASIVO_TDC = "TMED00";

	/** Concepto para descarga de estados de cuenta XML de Ingreso. */
	public static final String CONCEPTO_DESCARGA_XML_INGRESO = "Descarga de Estados de Cuenta XML de Ingreso";

	/** Concepto para descarga de estados de cuenta XML de Egreso. */
	public static final String CONCEPTO_DESCARGA_XML_EGRESO = "Descarga de Estados de Cuenta XML de Egreso";

	/**  [CDEC-02] Confirma Descarga de Estados de Cuenta - XML EGRESO. */
	public static final String DESC_EDOCTA_CDEC02 = "CDEC02";
	
	// Configuracion TC MASIVO
	
	/** constante opcion */
	public static final String OPCION = "opcion";
	/** Constante para session de BaseResource */
	public static final String BASE_RESOURCE = "session";
	/** Constante para titulo de pantalla */
	public static final String TITULO_PANTALLA = "Estados de Cuenta Tarjeta de Cr&eacute;dito";
	
	/** Constante para posicion de menu */
	public static final String POS_MENU = "Estados de Cuenta &gt; Modificar estado de env&iacute;o del estado de Cuenta Impreso &gt; Masivo";
	
	/** Constante para archivo de ayuda **/
	public static final String ARCHIVO_AYUDA = "aEDC_ConfigMas";
	
	/** TAG para logs **/
	public static final String LOG_TAG = "[EDCPDFXML] ::: ConfigMasPorTCServlet ::: ";
	
	
	/** Constante mensaje de error por facultades */
	public static final String MSJ_ERROR_FAC = "Usuario sin facultades para operar con este servicio";
	/** Constante mensaje de error por dias inhabiles */
	public static final String MSJ_ERROR_DIAS = "Esta funcionalidad &uacute;nicamente se encuentra disponible en d&iacute;as h&aacute;biles.";
	/**config masiva por tarjeta**/
	public static final int CONFIGMASPORTC = 1055; 
	
	/**Elemento en sesion con lista de cuentas**/
	public static final String LISTA_CUENTAS = "listaCuentasEdoCta";
	
	/**Elemento en sesion con lista de cuentas**/
	public static final String FECHA_CONFIG_MAS_TDC = "dd/MM/yyyy";
	
}
