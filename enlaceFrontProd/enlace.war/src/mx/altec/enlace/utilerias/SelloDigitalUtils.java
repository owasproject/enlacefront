/**
 * ISBAN Mexico - (c) Banco Santander Central Hispano
 * Todos los derechos reservados
 *
 * SelloDigitalUtils.java
 *
 * Control de versiones:
 *
 * Version  Date/Hour                    By                      Company     Description
 * -------  ------------------------     ----------------        --------    -----------------------------------------------------------------
 * 1.0      Feb 04, 2016 1:05:04 AM      FSW. Everis             IsbanMex.   Creacion de archivo
 */
package mx.altec.enlace.utilerias;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.cliente.ws.csd.BeanDatosOperMasiva;
import mx.altec.enlace.cliente.ws.csd.BeanRespuestaGeneracion;
import mx.altec.enlace.cliente.ws.csd.BeanRespuestaValidacion;
import mx.altec.enlace.cliente.ws.csd.ConstantesSD;
import mx.altec.enlace.cliente.ws.csd.Respuesta;
import mx.altec.enlace.cliente.ws.csd.WSServiceSD;
import javax.xml.ws.WebServiceException;
import org.apache.commons.codec.binary.Base64;

/**
 * Clase utilitaria para validar y hacer el llamado al WS de sellos digitales
 * 
 * @author FSW Everis
 */
public final class SelloDigitalUtils {

    /** Constructor privado */
    private SelloDigitalUtils() {

    }
    
    /**
     * Validar datos de comprobante en el WS Sellos Digitales
     * 
     * @param params parametros de llamado al WS
     * @param req objeto con informacion inherente de la peticion
     * @param resp objeto con informacion inherente de la respuesta
     * @return mensaje de error
     */
    public static String validarComprobante(String[] params, HttpServletRequest req, HttpServletResponse resp) {
        EIGlobal.mensajePorTrace("INICIO  validarComprobante", EIGlobal.NivelLog.INFO);
        String msgError = ConstantesSD.CADENA_VACIA;
        String html = ConstantesSD.CADENA_VACIA;
        StringBuilder resultHtml = new StringBuilder("");
        try {
            // Consume WS Sellos Digitales
            if (validaParams(params)) {
            	
            	try{
                BeanRespuestaValidacion beanResVal = WSServiceSD.getInstance().llamadoWSValidacion(params);
                EIGlobal.mensajePorTrace("validarComprobante WSValidacion codError|" + beanResVal.getCodError() + "| codRespuesta|"
                                         + beanResVal.getCodRespuesta() + "| camposAdic|" + beanResVal.getCampAdicionales() + "|", EIGlobal.NivelLog.INFO);
                if (ConstantesSD.CODE_OKSD.equals(beanResVal.getCodError()) && ConstantesSD.COD_RESPUESTA_CAM_ADIC.equals(beanResVal.getCodRespuesta())) {
					
                    html = crearContenidoPopUP(req, new String[] {
                        (beanResVal.getCampAdicionales() != null) ? beanResVal.getCampAdicionales() : ConstantesSD.CADENA_VACIA,
                            String.valueOf(beanResVal.getNumCampAdicionales()),
                            beanResVal.getNombreConcepto(),
                        params[ConstantesSD.PARAM_WS_VALIDACION_FCH], params[ConstantesSD.PARAM_WS_VALIDACION_ACCION]});
                    req.setAttribute(ConstantesSD.HIDDEN_NUM_CAMPOS_ADIC_WS, beanResVal.getNumCampAdicionales());
                    req.setAttribute(ConstantesSD.HIDDEN_CAMPOS_ADIC_WS, beanResVal.getCampAdicionales());
                    req.setAttribute(ConstantesSD.HIDDEN_CAMPOS_ADIC_OPER, new StringBuilder(
                        params[ConstantesSD.PARAM_WS_VALIDACION_LIN_CAMP]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_IMPORTE]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_REFERENCIA]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_FCH]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_ID_CANAL]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_NUM_CTA]).append(ConstantesSD.SIMPLE_PIPE)
                        .append(params[ConstantesSD.PARAM_WS_VALIDACION_ID_CONV]).append(ConstantesSD.SIMPLE_PIPE)
                        .toString());
                    if (html.isEmpty()) {
                        msgError = "Ocurri&oacute; un error al generar campos adicioanles";
                    }
                } else if (ConstantesSD.CODE_OKSD.equals(beanResVal.getCodError())
                           && ConstantesSD.COD_RESPUESTA_COMPROBANTE.equals(beanResVal.getCodRespuesta())) {
                    if (!imprimirPDF(resp, beanResVal.getCompSelloDigital(), params[ConstantesSD.PARAM_WS_VALIDACION_ID_CONV] + "_" + params[ConstantesSD.PARAM_WS_VALIDACION_LIN_CAMP])) {
                        msgError = "Ocurri&oacute; un error al generar PDF del comprobante con sello digital";
                    }
                } else {
                    msgError = beanResVal.getMsgError();
                }
                
            	}catch(WebServiceException wse){
            		EIGlobal.mensajePorTrace("WebServiceException " + wse , EIGlobal.NivelLog.ERROR);	
					msgError = "El servicio no esta disponible temporalmente.";
            	}
                
            } else {
                msgError = "Ocurri&oacute; un error al validar los valores de entrada para el llamado al servicio que genera comprobante con sello digital";
            }
            
        } catch (IndexOutOfBoundsException e) {
            EIGlobal.mensajePorTrace("ParseException" + e.getMessage() , EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("validarComprobante IndexOutOfBoundsException|" + e.getMessage() + "|", EIGlobal.NivelLog.INFO);
        }
        if (!msgError.isEmpty()) {
            resultHtml.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String [][]{{ConstantesSD.STYLE, "margin: auto; text-align: center;"}}), "<h3>" + msgError + "</h3>").getEtiquetaAttr());            
            EIGlobal.mensajePorTrace("validarComprobante msgError|" + msgError + "|", EIGlobal.NivelLog.INFO);
        } else {
            resultHtml.append(html);
        }
        EIGlobal.mensajePorTrace("FINALIZA validarComprobante", EIGlobal.NivelLog.INFO);
        return resultHtml.toString();
    }

    /**
     * Generar comprobante con sello digital
     * 
     * @param params parametros para la generacion de comprobante
     * @param req Objeto con informacion inherente de la peticion
     * @param resp Objeto con informacion inherente de la respuesta
     * @return mensaje de error
     */
    public static String generarComprobante(String[] params, HttpServletRequest req, HttpServletResponse resp) {
        EIGlobal.mensajePorTrace("INICIA generarComprobante params length|" + params.length + "|", EIGlobal.NivelLog.INFO);
        String msgError = ConstantesSD.CADENA_VACIA;
        StringBuilder resultHtml = new StringBuilder(ConstantesSD.CADENA_VACIA);
        String[] paramsFijos = params[0].split(ConstantesSD.SPLIT_SIMPLE_PIPE);

        for (String string : paramsFijos) {
            EIGlobal.mensajePorTrace("generarComprobante paramsFijo|" +  string + "|", EIGlobal.NivelLog.INFO);
        }
        if (validaParams(params[0].split(ConstantesSD.SPLIT_SIMPLE_PIPE)) 
                && (Integer.parseInt(req.getParameter(ConstantesSD.HIDDEN_NUM_CAMPOS_ADIC_WS)) <= 0 
                    || (Integer.parseInt(req.getParameter(ConstantesSD.HIDDEN_NUM_CAMPOS_ADIC_WS)) > 0  && validaParams(params[1].split(ConstantesSD.SPLIT_SIMPLE_PIPE))))) {
            EIGlobal.mensajePorTrace("generarComprobante paramsAdi|" +  params[1] + "|", EIGlobal.NivelLog.INFO);
			try{
            BeanRespuestaGeneracion beanResGen = WSServiceSD.getInstance().llamadoWSGeneracion(paramsFijos, params[1]);
            EIGlobal.mensajePorTrace("generarComprobante WS codError|" + beanResGen.getCodError() + "|", EIGlobal.NivelLog.INFO);
            if (ConstantesSD.CODE_OKSD.equals(beanResGen.getCodError())) {
                if (!imprimirPDF(resp, beanResGen.getCompSelloDigital(), paramsFijos[ConstantesSD.PARAM_WS_VALIDACION_ID_CONV] + "_"
                                                                         + paramsFijos[ConstantesSD.PARAM_WS_VALIDACION_LIN_CAMP])) {
                    msgError = "Ocurri&oacute; un error al generar PDF del comprobante con sello digital";
                }
            } else {
                msgError = beanResGen.getMsgError();
            }
			}catch(WebServiceException wse){
				EIGlobal.mensajePorTrace("WebServiceException" + wse , EIGlobal.NivelLog.ERROR);
				msgError = "El servicio no esta disponible temporalmente.";
			}
        } else {
            msgError = "Ocurri&oacute; un error al validar los valores de entrada para el llamado al servicio que genera comprobante con sello digital";
        }
        if (!msgError.isEmpty()) {
            resultHtml.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String [][]{{ConstantesSD.STYLE, "margin: auto; text-align: center;"}}), "<h3>" + msgError + "</h3>").getEtiquetaAttr());
            EIGlobal.mensajePorTrace("generarComprobante msgError|" + msgError + "|", EIGlobal.NivelLog.INFO);
        }
        EIGlobal.mensajePorTrace("FINALIZA generarComprobante", EIGlobal.NivelLog.INFO);
        return msgError;
    }

    /**
     * Guardado de transferencias masivo
     * 
     * @param req Objeto con informacion inherente de la peticion
     * @param operMasivas Lista de objetos a guardar
     * @return mensaje de error
     */
    public static String guardaMovimientos(HttpServletRequest req, List<BeanDatosOperMasiva> operMasivas) {
        EIGlobal.mensajePorTrace("INICIO guardaMovimientos list.size|" + (operMasivas != null ? operMasivas.size() : "null") + "|", EIGlobal.NivelLog.INFO);
        String msgError = ConstantesSD.CADENA_VACIA;
        if (operMasivas != null && !operMasivas.isEmpty()) {
            if (validaMovimientos(operMasivas)) {
            	try{
                Respuesta respuesta = WSServiceSD.getInstance().llamadoWSMasivo(operMasivas);
                EIGlobal.mensajePorTrace("guardaMovimientos WS respuesta|" + respuesta.getCodError() + "|" + respuesta.getMsgError() + "|", EIGlobal.NivelLog.INFO);
                if (!ConstantesSD.CODE_OKSD.equals(respuesta.getCodError())) {
                    msgError = respuesta.getMsgError();
                }
                
            	}catch(WebServiceException wse){
            		EIGlobal.mensajePorTrace("WebServiceException " + wse , EIGlobal.NivelLog.ERROR);
					msgError = "El servicio no esta disponible temporalmente.";
            	}
            } else {
                msgError = "Ocurri&oacute; un error al validar los valores de entrada para el llamado al servicio que genera comprobante con sello digital";
            }
        } else {
            msgError = "La lista de transferencias es vacia";
        }
        if (!msgError.isEmpty()) {
            EIGlobal.mensajePorTrace("guardaMovimientos msgError|" + msgError + "|", EIGlobal.NivelLog.INFO);
        }
        EIGlobal.mensajePorTrace("FINALIZA guardaMovimientos", EIGlobal.NivelLog.INFO);
        return msgError;
    }

    /**
     * Envia pdf a la vista
     *
     * @param resp Objeto con informacion inherente de la respuesta
     * @param pdfB64 cadena con pdf en base 64
     * @param nombrePDF nombre para el PDF
     * @return true si envio pdf, false de lo contrario
     */
    private static boolean imprimirPDF(HttpServletResponse resp, String pdfB64, String nombrePDF) {
        EIGlobal.mensajePorTrace("INICIO imprimirPDF", EIGlobal.NivelLog.INFO);
        try {
        OutputStream outputStream = resp.getOutputStream();
        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment; filename=\""+ nombrePDF +".pdf\"");
        EIGlobal.mensajePorTrace("imprimirPDF name|" + nombrePDF + ".pdf|", EIGlobal.NivelLog.INFO);
        outputStream.write(Base64.decodeBase64(pdfB64.getBytes()));
        outputStream.flush();
        outputStream.close();
        EIGlobal.mensajePorTrace("FINALIZA imprimirPDF", EIGlobal.NivelLog.INFO);
        } catch (IOException e){
            EIGlobal.mensajePorTrace("FINALIZA imprimirPDF error|" + e.getMessage(), EIGlobal.NivelLog.INFO);
            return false;
        }
        return true;
    }

    /**
     * Crear contenido html para popup de campos adicionales
     *
     * @param req Objeto con informacion inherente de la peticion
     * @param params mapa de atributos
     * @return codigo html generado
     */
    private static String crearContenidoPopUP(HttpServletRequest req, String[] params) {
        EIGlobal.mensajePorTrace("INICIO crearContenidoPopUP lengthParams|" + params.length + "|", EIGlobal.NivelLog.INFO);
        req.setAttribute(ConstantesSD.HIDDEN_POP_UP_CAMP_ADIC, params[ConstantesSD.PARAM_POP_UP_CAMP_ADIC]);
        req.setAttribute(ConstantesSD.HIDDEN_POP_UP_TOTAL_CAMP_ADIC, params[ConstantesSD.PARAM_POP_UP_TOTAL_CAMP_ADIC]);
        req.setAttribute(ConstantesSD.HIDDEN_POP_UP_CONCEPTO, params[ConstantesSD.PARAM_POP_UP_CONCEPTO]);
        SimpleDateFormat format = new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX);

        StringBuilder htmlResult = new StringBuilder(ConstantesSD.CADENA_VACIA);
        try {
            // Cadena para tabla de campos dinamicos
            StringBuilder tblCampos = new StringBuilder(ConstantesSD.CADENA_VACIA);
            //Cadena para tabla de campos fijos
            StringBuilder tblCamposEst = new StringBuilder(ConstantesSD.CADENA_VACIA);
            //Cadena para tabla de campos para la fecha
            StringBuilder tblCamposFec = new StringBuilder(ConstantesSD.CADENA_VACIA);

            // Div de header h2 para concepto
            htmlResult.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String [][]{{ConstantesSD.STYLE, "margin: auto; text-align: center;"}}), 
                        HTMLUtil.crearElemento(6, HTMLUtil.generarAttr(null), params[ConstantesSD.PARAM_POP_UP_CONCEPTO]).getEtiquetaAttr()).getEtiquetaAttr()
                        ).append("<br/>");
            EIGlobal.mensajePorTrace("crearContenidoPopUP header" + params[2], EIGlobal.NivelLog.INFO);
            try {
                   // Etiqueta de Fecha
                    tblCamposFec.append(HTMLUtil.crearElemento(3, HTMLUtil.generarAttr(null), "<td class=\"label\">Fecha</td><td style=\"color:gray\">" + format.format(format.parse(params[ConstantesSD.PARAM_POP_UP_FCH])) + "</td>").getEtiquetaAttr());
                    // Etiquetas de Nombre y domicilio
                    tblCamposEst.append(HTMLUtil.crearElemento(3, HTMLUtil.generarAttr(null), "<td class=\"label\">Nombre</td><td width=\"41%\"><input type=\"text\" maxlength=\"40\" name=\"Nombre\" class=\"uno\" id=\"Nombre\" /></td><td class=\"label\">C.P.</td><td><input type=\"text\" maxlength=\"5\" minlength=\"5\" name=\"CP\" class=\"dos\" id=\"CP\" /></td>").getEtiquetaAttr())
                    // Etiquetas de Colonia y Delegacio/municipio
                    .append(HTMLUtil.crearElemento(3, HTMLUtil.generarAttr(null), "<td class=\"label\">Domicilio</td><td><input type=\"text\" maxlength=\"50\" name=\"Domicilio\" class=\"uno\" id=\"Domicilio\" /></td><td class=\"label\">Delegaci&oacute;n/Municipio</td><td><input type=\"text\" maxlength=\"40\" class=\"dos\" name=\"DelegacionMunicipio\" id=\"DelegacionMunicipio\" /></td>").getEtiquetaAttr())
                    // Etiquetas de Codigo postal y estado
                    .append(HTMLUtil.crearElemento(3, HTMLUtil.generarAttr(null), "<td class=\"label\">Colonia</td><td><input type=\"text\" maxlength=\"40\" name=\"Colonia\" class=\"uno\" id=\"Colonia\" /></td><td class=\"label\">Estado</td><td><input type=\"text\" maxlength=\"40\" class=\"dos\" name=\"Estado\" id=\"Estado\" /></td>").getEtiquetaAttr());
            EIGlobal.mensajePorTrace("crearContenidoPopUP campos fijos table|" + tblCamposEst.toString() + "|", EIGlobal.NivelLog.INFO);
            } catch (ParseException e) {
                EIGlobal.mensajePorTrace("ParseException" + e.getMessage() , EIGlobal.NivelLog.INFO);
                htmlResult.replace(0, htmlResult.length()-1, "Ocurri\u00F3 un error al obtener los datos fisicos." + e.toString());
                EIGlobal.mensajePorTrace("crearContenidoPopUP error en obtener los datos fijos", EIGlobal.NivelLog.INFO);
            }
            String [] arrayCamposAdic = params[ConstantesSD.PARAM_POP_UP_CAMP_ADIC].split(ConstantesSD.SPLIT_DOBLE_PIPE);
            EIGlobal.mensajePorTrace("crearContenidoPopUP arrayCamposAdic.length|" + (arrayCamposAdic != null ? arrayCamposAdic.length : "0") + "|", EIGlobal.NivelLog.INFO);
            for (int i = 0; Integer.parseInt(params[ConstantesSD.PARAM_POP_UP_TOTAL_CAMP_ADIC]) > 0 && i <  arrayCamposAdic.length; i++) {
                String [] arrayCampoAdic = arrayCamposAdic[i].split(ConstantesSD.SPLIT_SIMPLE_PIPE);
                EIGlobal.mensajePorTrace("crearContenidoPopUP camposAdic ARRAY length|" 
                        + (arrayCampoAdic == null ? -1 : arrayCampoAdic.length)
                        + "|", EIGlobal.NivelLog.INFO);
                if (arrayCampoAdic != null && arrayCampoAdic.length == 3) {
                    String nomCamp = arrayCampoAdic[0];                   
                    StringBuilder input;
                    if(nomCamp.equals(ConstantesSD.FECHA_ENVENTO) || nomCamp.equals(ConstantesSD.FECHA_ESCRITURA) ){                                                          
                        input = new StringBuilder(HTMLUtil.crearElemento(7, HTMLUtil.generarAttr(new String [][]{
                                                                             {"id", nomCamp},
                                                                             {"name", nomCamp},
                                                                             {"placeholder", "AAAA-MM-DD"},
                                                                             {"type", "text"},
                                                                             {"autocomplete", "off"},
                                                                             {"onpaste", "return false"},
                                                                             {"maxLength", ConstantesSD.CADENA_VACIA + (ConstantesSD.TIPO_DATO_DOUBLE.equals(arrayCampoAdic[1]) ? (Integer.parseInt(arrayCampoAdic[2]) + 1) : arrayCampoAdic[2])}
                                                                             }), "").getEtiquetaAttr());
                    } else {
                     input = new StringBuilder(HTMLUtil.crearElemento(7, HTMLUtil.generarAttr(new String [][]{
                                                                            {"id", nomCamp},
                                                                            {"name", nomCamp},
                                                                            {"type", "text"},
                                                                            {"autocomplete", "off"},
                                                                            {"onpaste", "return false"},
                                                                            {"maxLength", ConstantesSD.CADENA_VACIA + (ConstantesSD.TIPO_DATO_DOUBLE.equals(arrayCampoAdic[1]) ? (Integer.parseInt(arrayCampoAdic[2]) + 1) : arrayCampoAdic[2])}
                                                                            }), "").getEtiquetaAttr());

                    }
                    EIGlobal.mensajePorTrace("crearContenidoPopUP input|" + i + "|", EIGlobal.NivelLog.INFO);
                    // Tr para campo adiconal dinamico
                    String tr = HTMLUtil.crearElemento(3, HTMLUtil.generarAttr(new String [][]{{"id", "tr" + nomCamp}}),
                                ( // Td con texto del campo
                                        HTMLUtil.crearElemento(8, HTMLUtil.generarAttr(new String [][]{{"id", "lbl" + nomCamp}}), nomCamp).getEtiquetaAttr()
                                        // Td con input del campo
                                        + HTMLUtil.crearElemento(4, HTMLUtil.generarAttr(new String [][]{{"id", "txt" + nomCamp}}), input.toString()).getEtiquetaAttr())
                                ).getEtiquetaAttr();
                    // Tr dinamico a table de campos
                    tblCampos.append("<div>")
                             .append(tr)
                             .append("</div>");
                }
                EIGlobal.mensajePorTrace("crearContenidoPopUP tr table|" + tblCampos.toString() + "|", EIGlobal.NivelLog.INFO);
            }
            EIGlobal.mensajePorTrace("crearContenidoPopUP camposAdic", EIGlobal.NivelLog.INFO);
       
            // Div con tabla con la fecha de datos fijos
            htmlResult.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "formDate"}}),
                    HTMLUtil.crearElemento(1, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "datEstat"}}),
                            tblCamposFec.toString()).getEtiquetaAttr()).getEtiquetaAttr());
            // DIV con la tabla de datos fijos
            htmlResult.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "formEst"}}),
                    HTMLUtil.crearElemento(1, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "datEstat"}}), tblCamposEst.toString()).getEtiquetaAttr()
            ).getEtiquetaAttr());

            //Div con tabla de datos dinamicos
            htmlResult.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "formDim"}}),
                    HTMLUtil.crearElemento(1, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "datDin"}}), tblCampos.toString()).getEtiquetaAttr()
            ).getEtiquetaAttr());

            //Div con los botones de continuar y de cancelar
            htmlResult.append(HTMLUtil.crearElemento(5, HTMLUtil.generarAttr(new String[][]{{ConstantesSD.CLASS, "formBtns"}}), 
                    "<a href=\"javascript: cerrarSD(this);\"><span>Cerrar</span> <a href=\"javascript: confirmarSD('/enlaceMig/comprobante_trans?mod=CSD');\">"
                    + "<span>Continuar</span></a>\n").getEtiquetaAttr());

            EIGlobal.mensajePorTrace("crearContenidoPopUP table|" + tblCampos.toString() + "|\n htmlResult|" + htmlResult.toString() + "|", EIGlobal.NivelLog.INFO);

            EIGlobal.mensajePorTrace("crearContenidoPopUP table|" + tblCampos.toString() + "|\n htmlResult|" + htmlResult.toString() + "|", EIGlobal.NivelLog.INFO);

        } catch (IndexOutOfBoundsException e) {
            htmlResult = new StringBuilder(ConstantesSD.COD_ERROR).append(e.toString());            
            EIGlobal.mensajePorTrace("ParseException" + e.getMessage() , EIGlobal.NivelLog.INFO);
            EIGlobal.mensajePorTrace("crearContenidoPopUP IndexOutOfBoundsException|" + e.getMessage() + "| causa|" + e.getMessage() + "|", EIGlobal.NivelLog.INFO);
        }
        EIGlobal.mensajePorTrace("FINALIZA crearContenidoPopUP", EIGlobal.NivelLog.INFO);
        return htmlResult.toString();
    }

    /**
     * Valida si algun valor es null o vacio
     *
     * @param params arreglo de parametros
     * @return true si un valor es null o vacio, false de lo contrario
     */
    private static boolean validaParams(String[] params) {
        int contErrores = 0;
        for (int i = 0; i < params.length; i++) {
            if (params[i] == null || params[i].isEmpty()) {
                contErrores++;
                EIGlobal.mensajePorTrace("validaParams param null o empty, posicion|" + i + "|", EIGlobal.NivelLog.INFO);
            }
        }
        return contErrores == 0;
    }

    /**
     * Valiacion de DTO BeanDatosOperMasiva
     * @param params lista de objetos a validar
     * @return true si los parametros son validos, false de lo contrario
     */
    private static boolean validaMovimientos(List<BeanDatosOperMasiva> params) {
        EIGlobal.mensajePorTrace("INICIA validaMovimientos", EIGlobal.NivelLog.DEBUG);
        
        SimpleDateFormat format = new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, ConstantesSD.LOCALE_MX);
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("hh:mm:ss");
		String hour = df.format(date);
		String[] fecFin = null;
		String formFec = null;
			
        for(int i = params.size()-1; i >= 0; i--){
            
            BeanDatosOperMasiva param = params.get(i);
            boolean flgError = false;
			fecFin = param.getFecha().split(" ");
			formFec = fecFin[0].concat(" ").concat(hour);
			param.setFecha(formFec);
			
			try {
                if (param.getLineaCaptura() == null || param.getLineaCaptura().isEmpty()) {
                    EIGlobal.mensajePorTrace("Linea de captura invalida ->" + param.getLineaCaptura(), EIGlobal.NivelLog.WARN);
                    flgError = true;
                }
                if (param.getNumCuenta() == null || param.getNumCuenta().isEmpty()) {
                    EIGlobal.mensajePorTrace("Cuenta invalida ->" + param.getNumCuenta(), EIGlobal.NivelLog.WARN);
                    flgError = true;
                }
                if (param.getReferencia() == null || param.getReferencia().isEmpty()) {
                    EIGlobal.mensajePorTrace("Referencia invalida ->" + param.getReferencia(), EIGlobal.NivelLog.WARN);
                    flgError = true;
                }
                if (format.format(format.parse(param.getFecha())) == null 
                        || format.format(format.parse(param.getFecha())).isEmpty()) {
                    EIGlobal.mensajePorTrace("Fecha invalida ->" + param.getFecha(), EIGlobal.NivelLog.WARN);
                    flgError = true;
                }
            } catch(ParseException e){
                EIGlobal.mensajePorTrace("Formato de Fecha invalida ->" + param.getFecha(), EIGlobal.NivelLog.WARN);
                flgError = true;
            }
            
            if(flgError){
                params.remove(i);
            }
        }
        EIGlobal.mensajePorTrace("Num de movimientos validados a enviar" + params.size(), EIGlobal.NivelLog.DEBUG);
        
        EIGlobal.mensajePorTrace("FINALIZA validaMovimientos", EIGlobal.NivelLog.DEBUG);
        return !params.isEmpty();
    }

    /**
     * Buesqueda y validacion de modulo para la validacion y generacion de comprobante con sello digital
     * @param request Objeto con informacion inherente sobre la peticion
     * @return cadena con el valor del modulo
     */
    public static String obtenerModulo (HttpServletRequest request){
        if (request.getAttribute(ConstantesSD.QUERY_PARAM_MOD) != null) {
            return (String) request.getAttribute(ConstantesSD.QUERY_PARAM_MOD);
        } else if (request.getParameter(ConstantesSD.QUERY_PARAM_MOD) != null) {
            return request.getParameter(ConstantesSD.QUERY_PARAM_MOD);
        } else {
            return ConstantesSD.MODULO_VSD;
        }
    }

    /**
     * Formatea fecha de pago, como la espera el WS
     * @param fch fecha de pago
     * @return cadena con fecha formateada
     */
    public static String armarFchPag(String fch){
        if (fch == null) {
            return new SimpleDateFormat(ConstantesSD.FORMATO_FCH_PAGO_WS, 
                    ConstantesSD.LOCALE_MX).format(new Date());
        }
        if (fch.length() == 16) {
            fch = fch + ":00";
        } else if (fch.length() == 10) {
            fch = fch + " 00:00:00";
        }
        String [] arrayFecha = fch.trim().substring(0, 10).split("/");
        return new StringBuilder(arrayFecha[2]).append(ConstantesSD.SIGNO_MENOS)
                .append(arrayFecha[1]).append(ConstantesSD.SIGNO_MENOS)
                .append(arrayFecha[0]).append(ConstantesSD.UN_ESPACIO)
                .append(fch.trim().substring(11, 19)).toString();
        
    }

	public static String fchAdicc(String fech){
		return fech;
	}
	
	}
