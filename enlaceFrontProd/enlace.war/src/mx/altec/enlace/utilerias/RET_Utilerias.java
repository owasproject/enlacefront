package mx.altec.enlace.utilerias;

import java.io.*;
import java.util.*;
import admin.api.*;

public class RET_Utilerias implements AdminAPIListener{

	/**
	 * Metodo para desconectar del admin api repentinamente
	 */
	public void onDisconnect(){
		EIGlobal.mensajePorTrace("RET_Utilerias::onDisconnect:: Conexion perdida",	EIGlobal.NivelLog.DEBUG);
	}

	/**
	 * Metodo para guardar el contrato dentro del admin api
	 *
	 * @param numContrato	-	Numero de Contrato Enlace
	 * @param numUsuario	-	Numero de Usuario Enlace
	 */
	public void guardaContratoAdmApi(String numContrato, String numUsuario){
		IConnectInfo cinfo = AdminAPIGID.createConnectInfo(Global.STR_HOST_FX_ONLINE, Integer.parseInt(Global.I_PORT_FX_ONLINE)); //host y puerto del SCS 180.181.33.69
		cinfo.setHeartbeat(0);
		AdminAPIGID.setLoggingEnabled(true);//Log
		try{
			AdminAPIGID.setLogOutputStream(new FileOutputStream("AdminAPI.log"));
		}catch(IOException e){
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::IOException " + e.getMessage(), EIGlobal.NivelLog.DEBUG);
		}
		AdminAPIGID api = new AdminAPIGID(new RET_Utilerias(), cinfo);

		int iReturncode;
		try{
			iReturncode = api.connect("apiuser", "4p1us3r", null);//usuario y contrase�a de administrador RET
		}catch(Exception e){
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::Error de conexion - " + e.getMessage(), EIGlobal.NivelLog.DEBUG);
			return;
		}
		if(iReturncode == AdminAPIGID.SUCCESS){
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi:: successful", EIGlobal.NivelLog.DEBUG);
			try{
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::numUsuario: " + numUsuario, EIGlobal.NivelLog.DEBUG);
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::numContrato: " + numContrato, EIGlobal.NivelLog.DEBUG);
				IUser user1 = api.getUserManager().find(numUsuario);
				user1.setFax(numContrato);//aqui pondr�amos el contrato o en otro campo que designemos y no utilicemos
				user1.save();
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi:: Contrato setteado en RET.", EIGlobal.NivelLog.DEBUG);
				api.disconnect(); //podriamos dejar la conexion abierta y solo realizar las modificaciones de los usuarios
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi:: Liberando conexi�n exitosamente.", EIGlobal.NivelLog.DEBUG);
			}catch(AdminAPIException e){
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::Error:",	EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::" +e.getMessage(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::" +e.getErrorNumber(), EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::" +e.getExceptionType(), EIGlobal.NivelLog.INFO);
			}
		}else{
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::No se pudo iniciar sesion : " + iReturncode,	EIGlobal.NivelLog.INFO);
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::" + AdminAPIGID.BAD_LOGIN,	EIGlobal.NivelLog.INFO);
		}
			EIGlobal.mensajePorTrace("RET_Utilerias::guardaContratoAdmApi::Termina.",	EIGlobal.NivelLog.DEBUG);
	}
}