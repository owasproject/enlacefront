package mx.altec.enlace.utilerias;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;


/**
 * @author Arturo
 *
 */
public final class RelacionFlujos {


	/**
	 *Mapa con los flujos 
	 */
	private static  Map<String, String> mapa;
	
	
	/**
	 *Constructor de la clase 
	 */
	private RelacionFlujos() {
		EIGlobal.mensajePorTrace(">>>>>>>>>>> RelacionFlujos() entro al constructor..." , EIGlobal.NivelLog.INFO);
	}

	/**
	 * Metodo para obtener el valor del mapa
	 * @param valor : valor buscado
	 * @return String valor
	 */

	public static synchronized  String getValue(String valor) {
		if(mapa == null ) {
			mapa = new HashMap<String, String>();
			obtieneValores();
			return mapa.get(valor);
		} else {
			return mapa.get(valor);
		}
	}


	/**
	 * Metodo de inicializacion del mapa
	 */
	private static void obtieneValores(){

		FileInputStream read = null;
		Properties props = new Properties ();

		try {
			read = new FileInputStream (Global.DIRECTORIO_REMOTO_WEB_ARCHIVO_CIFRADO+"/CertificadoDigital.properties");
			props.load (read);

			for (Iterator<Entry<Object,Object>> iterator = props.entrySet().iterator(); iterator.hasNext();) 
			{
				Entry<Object, Object> entry = (Entry<Object, Object>) iterator.next();
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: valores del mapa::: \n " + entry.getKey() +" : "+ entry.getValue() , EIGlobal.NivelLog.INFO);

				String[] valor = entry.getKey().toString().split("\\.");
				mapa.put(valor[1], entry.getValue().toString());
			}    

			// mapa = new HashMap<String, String>();

		} catch (IOException e) {
			EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: Error al leer el properties::: " + e.getMessage()  , EIGlobal.NivelLog.INFO);
		}

	}

}