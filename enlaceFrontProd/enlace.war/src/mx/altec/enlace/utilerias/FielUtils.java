/**
 *   Isban Mexico
 *   Clase: FielUtils.java
 *   Descripcion: Metodos utilitarios de uso comun a los modulos de FIEL.
 *
 *   Control de Cambios:
 *   1.0 22/06/2016  FSW. Everis
 */
package mx.altec.enlace.utilerias;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.servlets.BaseServlet;

/**
 * Metodos utilitarios de uso comun a los modulos de FIEL.
 *
 * @author FSW
 */
public final class FielUtils {

    /**
     * Constructur privado de una clase utilitaria.
     */
    private FielUtils() {
    }

    /**
     * Obtiene el parametro de validacion de Token
     *
     * @param req request de la peticion
     * @param servlet unidad desde donde se invoca
     * @return <code>true</code> si es validacion de Token
     */
    public static String validaRecToken(HttpServletRequest req,
            BaseServlet servlet) {
        String valida;
        if ((req.getAttribute("validaNull") != null && "si".equals(req.getAttribute("validaNull")))
                && (req.getAttribute("forzar") != null && "si".equals(req.getAttribute("forzar")))) {
            valida = null;
            EIGlobal.mensajePorTrace("------------valida---->" + valida, EIGlobal.NivelLog.DEBUG);
        } else {
            valida = servlet.getFormParameter(req, "valida");
            if ((valida == null || "".equals(valida))
                    && req.getAttribute("valida2") != null) {
                valida = req.getAttribute("valida2").toString();
            }
        }
        return valida;
    }

    /**
     * Carga el encabezado de la pagina para los JSP.
     *
     * @param req informacion de la peticion
     * @param servlet unidad que invoca la funcion
     * @param attrSession atributos de sesion
     * @param nomPag nombre de la pagina
     * @param rutaPag ruta de la posicion
     * @param ayudaPag clave de la ayuda a desplegar
     */
    public static void cargaEncabezadoPagina(HttpServletRequest req, BaseServlet servlet,
            BaseResource attrSession, String nomPag, String rutaPag, String ayudaPag) {
        req.setAttribute("NumContrato", (attrSession.getContractNumber() == null)
                ? FielConstants.CADENA_VACIA : attrSession.getContractNumber());
        req.setAttribute("NomContrato", (attrSession.getNombreContrato() == null)
                ? FielConstants.CADENA_VACIA : attrSession.getNombreContrato());
        req.setAttribute("NumUsuario", (attrSession.getUserID8() == null)
                ? FielConstants.CADENA_VACIA : attrSession.getUserID8());
        req.setAttribute("NomUsuario", (attrSession.getNombreUsuario() == null)
                ? FielConstants.CADENA_VACIA : attrSession.getNombreUsuario());
        req.setAttribute("MenuPrincipal", attrSession.getStrMenu());
        req.setAttribute("newMenu", attrSession.getFuncionesDeMenu());
        req.setAttribute("Encabezado", servlet.CreaEncabezado(nomPag, rutaPag, ayudaPag, req));
    }

    /**
     * Guarda la informacion indicada en las pistas de auditoria.
     *
     * @param req HTTP request
     * @param attrSession sesion interna de Enlace
     * @param operacion clave de operacion
     * @param tipoOperacion codigo de operacion
     * @param cuenta numero de cuenta
     * @param error codigo de error a grabar
     * @param grabaToken indica si graba la clave del token para una operacion
     * ejecutada
     */
    public static void grabaPistas(HttpServletRequest req, BaseResource attrSession,
            String operacion, String tipoOperacion, String cuenta, String error,
            boolean grabaToken, String referencia) {
        if (FielConstants.BITA_ACTIVA.equals(Global.USAR_BITACORAS.trim())) {
            BitaHelper bh = new BitaHelperImpl(req, attrSession, req.getSession(false));
            if (req.getParameter(BitaConstants.FLUJO) != null) {
                bh.incrementaFolioFlujo((String) req.getParameter(BitaConstants.FLUJO));
            } else {
                bh.incrementaFolioFlujo((String) req.getSession().getAttribute(BitaConstants.SESS_ID_FLUJO));
            }
            BitaTransacBean bt = new BitaTransacBean();
            bt = (BitaTransacBean) bh.llenarBean(bt);
            bt.setNumBit(operacion);
            if (attrSession.getContractNumber() != null) {
                bt.setContrato(attrSession.getContractNumber().trim());
            }
            if (tipoOperacion != null) {
                bt.setServTransTux(tipoOperacion);
            }
            if (error != null) {
                bt.setIdErr(error);
            }
            if (cuenta != null) {
                bt.setCctaDest(cuenta);
            }
            if (null != referencia){
                bt.setReferencia(Long.valueOf(referencia));
            }
            bt.setUsr(attrSession.getUserID8());
            if (grabaToken) {
                HttpSession ses = req.getSession();
                if (ses.getAttribute(BitaConstants.SESS_BAND_TOKEN) != null
                        && ((String) ses.getAttribute(BitaConstants.SESS_BAND_TOKEN)).trim()
                        .equals(BitaConstants.VALIDA)) {
                    bt.setIdToken(attrSession.getToken().getSerialNumber());
                }
            }

            try {
                BitaHandler.getInstance().insertBitaTransac(bt);
            } catch (Exception e) { // La funcion insertBitaTransac lanza tipo Exception
                EIGlobal.mensajePorTrace("Error aal grabar en pistas", EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTraceExcepcion(e);
            }
        }
    }

    /**
     * Resguarda e invoca la validacion de RSA.
     *
     * @param req peticion HTTP
     * @param res respuesta HTTP
     * @param ses objeto de sesion
     * @param servicio servlet desde donde se invoca
     * @param vieneDe indica la ruta desde donde se invoca
     * @return <code>true</code> cuando se ingreso a validar RSA
     */
    public static boolean validaRSA(HttpServletRequest req, HttpServletResponse res,
            HttpSession ses, BaseServlet servicio, String vieneDe) {
        String validaChallenge = req.getAttribute(FielConstants.RSA_PARAM_CHALLENGE) != null
                ? req.getAttribute(FielConstants.RSA_PARAM_CHALLENGE).toString()
                : FielConstants.EMPTY_STRING;
        EIGlobal.mensajePorTrace("--------------->validaChallenge:" + validaChallenge, EIGlobal.NivelLog.DEBUG);
        if (Global.VALIDA_RSA && !FielConstants.RSA_SUCCESS.equals(validaChallenge)) {
            EIGlobal.mensajePorTrace("--------------->Entrando challenge...." + validaChallenge, EIGlobal.NivelLog.DEBUG);
            ValidaOTP.guardaParametrosEnSession(req);
            req.setAttribute("vieneDe", vieneDe);
            ses.setAttribute("comboCuentaBackup", servicio.getFormParameter(req, "comboCuenta"));
            servicio.validacionesRSA(req, res);
            return true;
        }
        return false;
    }

    /**
     * Metodo de envio de notificaciones
     *
     * @param session Objeto inherente a la sesion
     * @param request Objeto inherente a la peticion
     * @param codErrorOper Codigo de error de la operacio
     * @param folioRegistro Folio de registro
     * @param tipoCuenta tipo de cuenta
     * @param operacionRealizada identificador de la operacion realizada
     */
    public static void enviarNotificaciones(BaseResource session, HttpServletRequest request, String codErrorOper,
            String folioRegistro, String tipoCuenta, int operacionRealizada,String cuenta) {
        EIGlobal.mensajePorTrace("Entro a enviar correo", EIGlobal.NivelLog.INFO);
        EmailSender sender = new EmailSender();

        if (sender.enviaNotificacion(codErrorOper)) {
            //EMAIL
            EmailDetails details = new EmailDetails();

            details.setNumeroContrato(session.getContractNumber());
            details.setRazonSocial(session.getNombreContrato());
            details.setUsuario(session.getUserID8());
            details.setNumRef(folioRegistro);
            details.setCuentaDestino(cuenta);
            details.setTipoOpCuenta(tipoCuenta);
            details.setEstatusActual(codErrorOper);

            EIGlobal.mensajePorTrace("Se envia informacion de correo", EIGlobal.NivelLog.INFO);
            sender.sendNotificacion(request, operacionRealizada, details);
        }
    }

    /**
     * Metodo graba bitacora
     *
     * @param req HTTP request
     * @param session Objeto inherente a la sesion
     * @param tipoOperacion descripcion del tipo de operacion
     * @param cuentaOrigen cuenta origen
     * @param referencia referencia de la cuenta
     * @param codErrorOper codigo de error de la operacion
     */
    public static void grabaBitacoraTCT(HttpServletRequest req, BaseResource session,
            String tipoOperacion, String cuentaOrigen, int referencia, String codErrorOper, String descripcion) {
        EIGlobal.mensajePorTrace("Entro a grabaBitacoraTCT", EIGlobal.NivelLog.INFO);

        EIGlobal.mensajePorTrace("FielUtils.java Entro Bitacora", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java usuario: " + session.getUserID8().trim(), EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java tipo operacion: " + tipoOperacion, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java cuenta Origen: " + cuentaOrigen, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java referencia: " + referencia, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java cod Error Oper: " + codErrorOper, EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("FielUtils.java Num Cuenta : " + session.getContractNumber().trim(), EIGlobal.NivelLog.DEBUG);
        BitaHelper bh = new BitaHelperImpl(req, session, req.getSession());
        BitaTCTBean beanTCT = new BitaTCTBean();
        beanTCT = bh.llenarBeanTCT(beanTCT);

        if (session.getUserID8() != null) {
            beanTCT.setUsuario(session.getUserID8().trim());
            beanTCT.setOperador(session.getUserID8().trim());
        }

        if (session.getContractNumber() != null) {
            beanTCT.setNumCuenta(session.getContractNumber().trim());
        }

        beanTCT.setTipoOperacion(tipoOperacion);
        beanTCT.setCuentaOrigen(cuentaOrigen);
        beanTCT.setReferencia(referencia);
        beanTCT.setCodError(codErrorOper);
        beanTCT.setConcepto(descripcion);

        try {
            BitaHandler.getInstance().insertBitaTCT(beanTCT);
        // La funcion insertBitaTransac lanza tipo Exception
        } catch (Exception e) {// No se puede sustituir por una especifica
            EIGlobal.mensajePorTrace("Error de grabado en bitacora de operaciones", EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTraceExcepcion(e);
        }
    }

    /**
     * Metodo que valida y guarda token
     *
     * @param req Objeto inherente a la solicitud
     * @param res objeto inherente a la respuesta
     * @param attrSession Objeto de sesion interna
     * @param servlet servlet que invoca
     * @param paginaRetorno Pantalla que se mostrara
     * @param nombrePag nombre de la pagina
     * @param rutaPag ruta para procesar la transaccion
     * @param ayuda clave de ayuda
     * @param urlCancelar ruta para cancelar
     * @param msjSes mensaje de confirmacion
     * @return pagina de retorno
     */
    public static String validaToken(HttpServletRequest req, HttpServletResponse res,
            BaseResource attrSession, BaseServlet servlet, String paginaRetorno,
            String nombrePag, String rutaPag, String ayuda, String urlCancelar, String msjSes) {
        EIGlobal.mensajePorTrace("Inicio validaToken paginaRetorno|" + paginaRetorno
                + "| nomPag|" + nombrePag
                + "| rutaPag|" + rutaPag
                + "|", EIGlobal.NivelLog.INFO);
        req.getSession().setAttribute(BitaConstants.SESS_BAND_TOKEN, BitaConstants.VALIDA);
        req.setAttribute(FielConstants.PARAM_MODULO, FielConstants.UNO_STR);
        EIGlobal.mensajePorTrace("validaToken guardan los parametros en sesion para validar Token", EIGlobal.NivelLog.INFO);
        ValidaOTP.guardaParametrosEnSession(req);

        req.getSession().removeAttribute(FielConstants.PARAM_MENSAJE_SESSION);
        HttpSession ses = req.getSession();
        ses.setAttribute(FielConstants.PARAM_PARAMETROS_BITACORA, ses.getAttribute(ValidaOTP.PARAMETROS));
        ses.setAttribute(FielConstants.PARAM_ATRIBUTOS_BITACORA, ses.getAttribute(ValidaOTP.ATRIBUTOS));
        ses.setAttribute(FielConstants.PARAM_MENSAJE_SESSION, msjSes);
        req.setAttribute(FielConstants.URL_CANCELAR, urlCancelar);
        paginaRetorno = FielConstants.JSP_FIEL_CONFIRMA;
        FielUtils.cargaEncabezadoPagina(req, servlet, attrSession, nombrePag,
                rutaPag, ayuda);
        try {
            req.getRequestDispatcher(paginaRetorno).forward(req, res);
            EIGlobal.mensajePorTrace("validaToken fordward|" + paginaRetorno + "|", EIGlobal.NivelLog.DEBUG);
        } catch (ServletException ex) {
            EIGlobal.mensajePorTrace("validaToken ServletException", EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTraceExcepcion(ex);
        } catch (IOException e) {
            EIGlobal.mensajePorTrace("validaToken IOException", EIGlobal.NivelLog.ERROR);
            EIGlobal.mensajePorTraceExcepcion(e);
        }
        return paginaRetorno;
    }

    /**
     * Metodo para realizar notificaciones, guardar en bitacora y grabar en
     * pistas de auditoria.
     *
     * @param req Objeto inherente a la solicitud
     * @param attrSession Objeto de sesion
     * @param referencia generada en enlace
     * @param cuenta numero de cuenta asociada
     * @param operacion clave de operacion
     * @param tipoOper tipo de operacion
     * @param codError codigo de error
     * @param tipoCuenta tipo de movimiento efectuado sobre la cuenta
     * @return true si los parametros son correctos, false de lo contrario
     */
    public static boolean realizarOperaciones(HttpServletRequest req, 
            BaseResource attrSession, int referencia, String cuenta, String operacion,
            String tipoOper, String codError, String tipoCuenta, String descripcion) {
        EIGlobal.mensajePorTrace("Inicio realizarOperaciones", EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("realizarOperaciones referencia|" + referencia
                + "| cuenta|" + cuenta
                + "| codigo error|" + codError
                + "| folio registro|" + referencia
                + "| operacion|" + operacion
                + "| tipo cuenta|" + tipoCuenta
                + "| tipo operacion|" + tipoOper
                + "| Descripcion|" + descripcion
                + "|", EIGlobal.NivelLog.DEBUG);

        String codigoError;
        if (FielConstants.COD_ERRROR_OK.equals(codError)) {
            codigoError = tipoOper.concat("0000");
            int claveNotif = IEnlace.NOT_FIEL_ALTA;
            if (FielConstants.BITA_FIEL_BAJA.equals(tipoOper)) {
                claveNotif = IEnlace.NOT_FIEL_BAJA;
            } else if (FielConstants.BITA_FIEL_MODIFICA.equals(tipoOper)) {
                claveNotif = IEnlace.NOT_FIEL_MODIFICACION;
            }
            enviarNotificaciones(attrSession, req,
                    codigoError,
                    String.valueOf(referencia),
                    tipoCuenta, claveNotif,cuenta);
            EIGlobal.mensajePorTrace("realizarOperaciones enviarNotificaciones", EIGlobal.NivelLog.DEBUG);
        } else {
            codigoError = tipoOper.concat( "0001" );
        }

        ValidaOTP.guardaBitacora((List) req.getSession().getAttribute(FielConstants.PARAM_BITACORA), tipoOper);
        EIGlobal.mensajePorTrace("realizarOperaciones guardaBitacora", EIGlobal.NivelLog.INFO);

        grabaPistas(req, attrSession, operacion, tipoOper, cuenta, codigoError, false, String.valueOf(referencia));
        EIGlobal.mensajePorTrace("realizarOperaciones grabaPistas", EIGlobal.NivelLog.INFO);

        grabaBitacoraTCT(req, attrSession, tipoOper, cuenta, referencia, codigoError,descripcion);
        EIGlobal.mensajePorTrace("Fin realizarOperaciones OK", EIGlobal.NivelLog.INFO);
        return Boolean.TRUE;
    }
}
