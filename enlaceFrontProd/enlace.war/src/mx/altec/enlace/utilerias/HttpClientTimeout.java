package mx.altec.enlace.utilerias;

import sun.net.www.http.HttpClient;
import java.net.*;
import java.io.*;
public class HttpClientTimeout extends HttpClient
{
    public HttpClientTimeout(URL url, String proxy, int proxyPort) throws IOException
	{
		super(url, proxy, proxyPort);
	}

    public HttpClientTimeout(URL url) throws IOException
	{
		super(url,(String)null, -1);
    }

	public void SetTimeout(int i) throws SocketException {
    	serverSocket.setSoTimeout(i);
    }

    /* This class has no public constructor for HTTP.  This method is used to
     * get an HttpClient to the specifed URL.  If there's currently an
     * active HttpClient to that server/port, you'll get that one.
	 *
	 * no longer syncrhonized -- it slows things down too much
	 * synchronize at a higher level
     */
    public static HttpClientTimeout GetNew(URL url)
    throws IOException {
	/* see if one's already around */
	HttpClientTimeout ret = (HttpClientTimeout) kac.get(url);
	if (ret == null) {
	    ret = new HttpClientTimeout (url);  // CTOR called openServer()
	} else {
	    ret.url = url;
	}
	// don't know if we're keeping alive until we parse the headers
	// for now, keepingAlive is false
	return ret;
    }

	public void Close() throws IOException
	{
		serverSocket.close();
	}

	public Socket GetSocket()
	{
		return serverSocket;
	}


}
