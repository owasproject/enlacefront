package mx.altec.enlace.utilerias;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import mx.altec.enlace.bo.ArchivoRemoto;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.EI_Tipo;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.BaseServlet;

import org.apache.log4j.AsyncAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * 		Control de cambios:
 * @author TCS
 * @version 1.1 Ago 10, 2016 Se agregan tratamiento FLAME 
 *
 */
public class EIGlobal implements Serializable {

	/**
	 * logger
	 */
	static Logger[] logger = null;
	/**
	 * currLogger 
	 */
	static int currLogger = 0;
  
    /**
     * enum NivelLog
     *
     */
	public static enum NivelLog {
		/**
		 *  Nivel depuracion Error
		 */
		ERROR,
		/**
		 *  Nivel depuracion Debug
		*/
		DEBUG,
		/**
		 *  Nivel depuracion Warning
		*/
		WARN,
		/**
		 *  Nivel depuracion Info
		*/
		INFO;
	}
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	//Comienza la programacion base
    /**
     * BaseServlet BS
     */
    public BaseServlet BS;
	/**
	 * BaseServlet BAL
	 */
	public BaseServlet BAL;
	/**
	 * BaseResource session
	 */
	public BaseResource session;
	/**
	 * ServletContext contexto
	 */
	public ServletContext contexto;

	// <vswf temporal meg>
    /**
     * Constructor EIGlobal
     * @param TempBS BaseServlet
     */
    public EIGlobal( BaseServlet TempBS )
	{
        BAL=TempBS;
	}
    //  <vswf temporal meg>

	/**
     * Constructor EIGlobal
	 * @param bs BaseResource
	 * @param con ServletContext
	 * @param basS BaseServlet
	 */
	public EIGlobal(BaseResource bs, ServletContext con, BaseServlet basS) {
		this.session = bs;
		this.contexto = con;
		this.BS = basS;
		this.BAL = basS;
	}

	/**
	 * Inicializador de Logger
	 */
	public static void init() {
		logger = new Logger[Global.INSTANCIAS_LOG4J];
		for (int i = 0; i < Global.INSTANCIAS_LOG4J; i++) {
			logger[i] = Logger.getLogger(EIGlobal.class.getName() + i);
			System.out.println("Configurando logger: " + i);
		}
		configura(Global.LOG4J_CONFIGURACION);
	}

    /**
     * Configura Logger
     * @param Archivo Archivo entrada
     */
    public static void configura (String Archivo) {
    	try{
			System.out.println("Abriendo archivo de configuracion: " + Archivo);
    		InputStream input = new FileInputStream(Archivo);
    		System.out.println("Configurando Log4j ...");
    		new DOMConfigurator().doConfigure(input, LogManager.getLoggerRepository());
    	}catch(IOException e){
        	System.out.println("Error al cargar log4j.xml : " + e.getMessage());
        }

		Logger rlogger = Logger.getRootLogger();
		AsyncAppender async = (AsyncAppender) rlogger.getAppender("default-async-appender");
		DailyRollingFileAppender fa = (DailyRollingFileAppender) async.getAppender("file-appender");
		if(fa != null)
		{
			fa.setFile(Global.LOG4J_LOG);
			fa.activateOptions();
			System.out.println("Log4j configurado correctamente... Archivo de log: " + Global.LOG4J_LOG);
		}
		else
		{
			System.out.println("No se encontro el appender para el log.");
		}
    }

    /**
     * Contabiliza tokens
     * @param Cadena cadena
     * @param Caracter caracter
     * @return cantidad tokens
     */
    public static int CuantosTokens(String Cadena, char Caracter)
    {
        int PosicionInicial = 0;
        int PosicionFinal   = 0;
        int Cuantos         = 0;

           PosicionFinal = Cadena.indexOf( Caracter, PosicionInicial);
           while(PosicionFinal != -1)
           {
              PosicionFinal   = Cadena.indexOf( Caracter, PosicionInicial);
              if(PosicionFinal != -1)
              {
                 Cuantos = Cuantos + 1;
                 PosicionInicial = PosicionFinal + 1;
              }
          }
          if( PosicionInicial < Cadena.length()){
              Cuantos = Cuantos + 1;
          }
        return(Cuantos);
    }

    /**
     * Busca tokens
     * @param Cadena cadena
     * @param Caracter caracter
     * @param Indice indice
     * @return cadena tokens
     */
    public static String BuscarToken(String Cadena, char Caracter, int Indice)
    {
        String Token           = "";
        int    PosicionInicial = 0;
        int    PosicionFinal   = 0;
        int    Cuantos         = 0;

           PosicionFinal = Cadena.indexOf( Caracter, PosicionInicial);
           while(PosicionFinal != -1)
           {
                PosicionFinal   = Cadena.indexOf( Caracter, PosicionInicial);
                if(PosicionFinal != -1)
                {
                   Cuantos = Cuantos + 1;
                   if(Indice == Cuantos)
                   {
                       Token = Cadena.substring( PosicionInicial, PosicionFinal);
                       break;
                   }
                   PosicionInicial = PosicionFinal + 1;
                }
            }

            if( PosicionInicial < Cadena.length())
            {
               Cuantos = Cuantos + 1;
               if( Indice == Cuantos){
                  Token = Cadena.substring(PosicionInicial);
               }
            }
		  if(Token == null||"".equals(Token.trim()) ){
			  Token = "   ";
		  }
        return(Token);
    }

    /**
     * Convierte a String
     * @param dato dato
     * @return cadena
     */
    public static String ToString( double dato )
    {
       String strValor;
       long   valor;

          valor = Math.round(dato * 100);
          strValor = Long.toString(valor).trim();
          if(strValor.length()>2){
             strValor = strValor.substring(0, strValor.length()-2) + "." + strValor.substring(strValor.length()-2);
          }else{
             if(strValor.length()==2){
                 strValor = "0." + strValor;
             }else{
                if(strValor.length()==1){
                   strValor = "0.0" + strValor;
                }else{
                     strValor = "0.00";
                }
             }
          }
          strValor=strValor.trim();

       return(strValor);
    }

    /**
     * Alinea cadena
     * @param cadena cadena
     * @param limite limite
     * @return cadena
     */
    public static String alinearCadena(String cadena, int limite)
    {
       String espacios = "";
       int    indice;

         for( indice = 0; indice<limite; indice++)
            espacios = espacios + " ";

         cadena.trim();
         if( cadena.length() < limite ){
            espacios = espacios.substring(0, limite - cadena.length()) + cadena;
         }else{
            espacios = cadena;
         }
       return(espacios);
    }

    /**
     * Obtiene subcadena
     * @param dato dato
     * @param inicio inicio
     * @param Longitud longitud
     * @return cadena
     */
    public static String SubString(String dato, int inicio, int Longitud )
      {
          int fin = inicio + Longitud-1;
          inicio = inicio - 1;
          if( fin < dato.length() ){
              return( dato.substring( inicio, fin).toUpperCase() );
          }else{
              return( dato.substring( inicio).toUpperCase() );
          }
      }

	  
	/**
	 * Exhibe el mensaje correspondiente al nivel de trace deseado
	 * @param mensaje mensaje
	 * @param nivel nivelo log
	 */
	public static void mensajePorTrace( String mensaje, NivelLog nivel ) {
		if (logger == null) {
			init();
		}
		switch(nivel){
			case DEBUG:
				logger[currLogger].debug(mensaje);
				break;
			case ERROR:
				logger[currLogger].error(mensaje);
				break;
			case WARN:
				logger[currLogger].warn(mensaje);
				break;
			case INFO:
				logger[currLogger].info(mensaje);
				break;
		}
		currLogger = (currLogger + 1) % Global.INSTANCIAS_LOG4J;

    }
	/**
	 * Exhibe el mensaje correspondiente al nivel de trace deseado
	 * @param mensaje mensaje
	 * @param nivel nivelo log
	 * @param e Excepcion
	 */
	public static void mensajePorTrace( String mensaje, NivelLog nivel, Exception e ) {
		if (logger == null) {
			init();
		}
		switch(nivel){
			case DEBUG:
				logger[currLogger].debug(mensaje, e);
				break;
			case ERROR:
				logger[currLogger].error(mensaje, e);
				break;
			case WARN:
				logger[currLogger].warn(mensaje, e);
				break;
			case INFO:
				logger[currLogger].info(mensaje, e);
				break;
		}
		currLogger = (currLogger + 1) % Global.INSTANCIAS_LOG4J;

    }

	/**
	 * Exhibe el mensaje correspondiente al nivel de trace debug
	 * @param mensaje Cadena con el mensaje
	 */
	public static void mensajePorTraceDebug( String mensaje) {
		if (logger == null) {
			init();
		}
		logger[currLogger].debug(mensaje);
		currLogger = (currLogger + 1) % Global.INSTANCIAS_LOG4J;
    }
	/**
	 * Exhibe el mensaje correspondiente al nivel de trace info
	 * @param mensaje Cadena con el mensaje
	 */
	public static void mensajePorTraceInfo( String mensaje) {
		if (logger == null) {
			init();
		}
		logger[currLogger].info(mensaje);
		currLogger = (currLogger + 1) % Global.INSTANCIAS_LOG4J;
    }

	
	/**Imprimime en consola el log completo de la excepcion
	 * 
	 * @param e : Excepcion ocurrida para trace. 
	 */
	public static void mensajePorTraceExcepcion( Exception e ) {
		
		if (logger == null) {
			init();
		}
	
		logger[currLogger].error(e);
			
		

    }
/*************************************************************************************/
/******************************* Regresa el dia, el a�o o el mes de la fecha actual  */
/************************************************************************************
 * @param tipo tipo
 * @return cadena
 */
public static String getFecha(String tipo)
    {
      String strElemento = "";
      Date Hoy = new Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Cal.setTime(Hoy);

      if("DIA".equals(tipo)){
        strElemento+=Cal.get(Calendar.DATE);
      }
      if("MES".equals(tipo)){
        strElemento+=(Cal.get(Calendar.MONTH)+1);
      }
      if("ANIO".equals(tipo)){
        strElemento+=Cal.get(Calendar.YEAR);
      }

      return strElemento;
    }



/*************************************************************************************/
/******************************* Regresa la hora actual en el formato que se indique */
/*******************************               HH, MM, SS, HHMMSS                    */
/************************************************************************************
 * @param tipo tipo
 * @return cadena
 */
public static String getHora(String tipo)
    {
      String strElemento = "";
      Date Hoy = new Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Cal.setTime(Hoy);



      if("HH".equals(tipo))
      {
      	if (Cal.get(Calendar.HOUR_OF_DAY) < 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.HOUR_OF_DAY);
      }

      if("MM".equals(tipo))
      {
	if (Cal.get(Calendar.MINUTE) < 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.MINUTE);
      }

      if("SS".equals(tipo))
      {
      	if (Cal.get(Calendar.SECOND)< 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.SECOND);
      }

      if("HHMMSS".equals(tipo))
      {
      	if (Cal.get(Calendar.HOUR_OF_DAY)< 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.HOUR_OF_DAY);
	if (Cal.get(Calendar.MINUTE)< 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.MINUTE);
      	if (Cal.get(Calendar.SECOND)< 10 ) {strElemento="0";}
        strElemento += Cal.get(Calendar.SECOND);
      }

      return strElemento;
    }

/*************************************************************************************/
/******************************* Regresa un vector con los dias no habiles del a�o   */
/******************************* consultados directamente de oracle                  */
/************************************************************************************
     * @return vector
     */
    public static Vector CargarDias()
    {
       /*IDataConn  Conexion;
       IQuery     qrDias;
       IResultSet rsDias;
       boolean    estado;
       String     sqlDias;
       Vector diasInhabiles = null;

         System.out.println("cargar dias no habiles");

         diasInhabiles = new Vector();
         sqlDias = "Select to_char(fecha, 'mm'), to_char(fecha, 'dd'), to_char(fecha, 'yyyy') From comu_dia_inhabil Where cve_pais = 'MEXI'";
         Conexion = BAL.createDataConn(0,GX_DA_DAD_DRIVERS.GX_DA_DRIVER_ORACLE_OCI,"", IEnlace.BASE_DATOS,IEnlace.DB_USER,IEnlace.DB_USER_PWD);
         //Conexion = createDataConn(0,GX_DA_DAD_DRIVERS.GX_DA_DRIVER_ORACLE_OCI,"", IEnlace.BASE_DATOS,IEnlace.DB_USER,IEnlace.DB_USER_PWD);
         //Usar el m�todo getConnection()en el javax.sql.DataSource interface page 33
         if(Conexion!=null)
         {
            System.out.println("conexion establecida");

            qrDias = BAL.createQuery();//Usar el CreateStatement en el java.sql.Connection interface Page 42
            //qrDias = createQuery();
            qrDias.setSQL(sqlDias);
            if(qrDias!=null)
            {
               System.out.println("objeto query listo");

         	  rsDias = Conexion.executeQuery(0, qrDias, null, null);
               if(rsDias!=null)
               {

                 System.out.println("objeto resultSet listo");
                 do{
                    System.out.println("Leyendo fecha");
                    String dia = rsDias.getValueString(1);
                    String mes = rsDias.getValueString(2);
                    String anio = rsDias.getValueString(3);
                    dia  =  Integer.toString( Integer.parseInt(dia) );
                    mes  =  Integer.toString( Integer.parseInt(mes) );
                    anio =  Integer.toString( Integer.parseInt(anio) );
                    String fecha = anio.trim() + "/"  + mes.trim() + "/" + dia.trim();
                    System.out.println("fecha no habil " + fecha );
                    diasInhabiles.addElement(fecha);
                  }while(rsDias.fetchNext()==0);
                  rsDias.close(0);
               }
               else
                   System.err.println("Cannot Create ResultSet");
            }
            else
       	     System.err.println("Cannot Create Query");
         }
         else
             System.err.println("Error al intentar crear la conexion");

        System.out.println("termina cargar dias no habiles");

        return(diasInhabiles);*/
        return new Vector();
	}


/*************************************************************************************/
/******************************* Regresa verdadero si la fecha es habil.             */
/*******************************                                                     */
/************************************************************************************
 * @param fecha fecha
 * @return boolean
 */
public static boolean esHabil(Calendar fecha)
     {

         int     indice;
         int     diaDeLaSemana;
  		 Vector  diasNoHabiles;
		 boolean habil = true;

         diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
 		 if(diaDeLaSemana == Calendar.SUNDAY || diaDeLaSemana == Calendar.SATURDAY)
		   {
			 habil = false;
		   }
		 else
		   {
			 diasNoHabiles = CargarDias();
			 if(diasNoHabiles!=null)
               {
			      for(indice=0; indice<diasNoHabiles.size(); indice++)
                   {
                      String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
                      String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
                      String anio = Integer.toString( fecha.get( fecha.YEAR) );
                      String strFecha = anio.trim() + "/" + dia.trim() + "/" + mes.trim();
                      if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
                       {
                          habil = false;
                          break;
                       }
                   }
			   }
		   }

         return habil;

     }


/*************************************************************************************/
/******************************* Regresa el siguiente dia habil  al que se le        */
/******************************* pasa como parametro                                 */
/************************************************************************************
     * @param fecha fecha
     * @param diasNoHabiles dias no habiles
     * @return calendario
     */
    public static Calendar EvaluaDiaHabil(Calendar fecha, Vector diasNoHabiles)
      {
         int      indice;
         int      diaDeLaSemana;
         boolean  encontrado;

             EIGlobal.mensajePorTrace("EvaluaDiaHabil", EIGlobal.NivelLog.DEBUG);
             EIGlobal.mensajePorTrace("Dia  " + fecha.get( fecha.DAY_OF_MONTH), EIGlobal.NivelLog.DEBUG );
             EIGlobal.mensajePorTrace("Mes  " + fecha.get( fecha.MONTH) , EIGlobal.NivelLog.DEBUG);
             EIGlobal.mensajePorTrace("Anio " + fecha.get( fecha.YEAR) , EIGlobal.NivelLog.DEBUG);

             if(diasNoHabiles!=null)
             {
               fecha.add( fecha.DAY_OF_MONTH, -1);
               do{
                   fecha.add( fecha.DAY_OF_MONTH,  1);

                   EIGlobal.mensajePorTrace("Dia por evaluar", EIGlobal.NivelLog.DEBUG);
                   EIGlobal.mensajePorTrace("Dia  " + fecha.get( fecha.DAY_OF_MONTH ) , EIGlobal.NivelLog.DEBUG);
                   EIGlobal.mensajePorTrace("Mes  " + fecha.get( fecha.MONTH ), EIGlobal.NivelLog.DEBUG );
                   EIGlobal.mensajePorTrace("Anio " + fecha.get( fecha.YEAR ), EIGlobal.NivelLog.DEBUG );

                   diaDeLaSemana = fecha.get( fecha.DAY_OF_WEEK );
                   encontrado = false;
                   for(indice=0; indice<diasNoHabiles.size(); indice++)
                   {
                      String dia  = Integer.toString( fecha.get( fecha.DAY_OF_MONTH) );
                      String mes  = Integer.toString( fecha.get( fecha.MONTH) + 1    );
                      String anio = Integer.toString( fecha.get( fecha.YEAR) );
                      String strFecha = anio.trim() + "/" + dia.trim() + "/" + mes.trim();
                      if( strFecha.equals(diasNoHabiles.elementAt(indice) ))
                      {
                         EIGlobal.mensajePorTrace("Dia es inhabil", EIGlobal.NivelLog.DEBUG);
                         encontrado = true;
                         break;
                      }
                   }
               }while( (diaDeLaSemana == Calendar.SUNDAY)   ||
                       (diaDeLaSemana == Calendar.SATURDAY) ||
//                       (diaDeLaSemana == Calendar.FRIDAY)   ||	opcion solo utilizada en el modulo SUAAnalizador
                       encontrado );
             }

             EIGlobal.mensajePorTrace("Termina EvaluaDiaHabil", EIGlobal.NivelLog.DEBUG);
             EIGlobal.mensajePorTrace("Dia limite encontrado", EIGlobal.NivelLog.DEBUG);
             EIGlobal.mensajePorTrace("Dia  " + fecha.get( fecha.DAY_OF_MONTH ), EIGlobal.NivelLog.DEBUG );
             EIGlobal.mensajePorTrace("Mes  " + fecha.get( fecha.MONTH ), EIGlobal.NivelLog.DEBUG );
             EIGlobal.mensajePorTrace("Anio " + fecha.get( fecha.YEAR ), EIGlobal.NivelLog.DEBUG );

         return fecha;
      }

  /**
   * Formato fecha
 * @param calFecha calendario
 * @param strFormato formato
 * @return cadena
 */
public static String formatoFecha(Calendar calFecha, String strFormato)
    {
      //Date Hoy = new Date();
      //GregorianCalendar Cal = new GregorianCalendar();

      String strFecha = strFormato;

      String dd="";
      String mm="";
      String aa="";
      String aaaa="";
      String dt="";
      String mt="";
      String th="";
      String tm="";
      String ts="";

      int indth=0;
      int indtm=0;
      int indts=0;
      int inddd=0;
      int indmm=0;
      int indaaaa=0;
      int indaa=0;
      int indmt=0;
      int inddt=0;

         String[] dias = { "Domingo",
                           "Lunes",
                           "Martes",
                           "Miercoles",
                           "Jueves",
                           "Viernes",
                           "Sabado"};
         String[] meses = {"Enero",
                           "Febrero",
                           "Marzo",
                           "Abril",
                           "Mayo",
                           "Junio",
                           "Julio",
                           "Agosto",
                           "Septiembre",
                           "Octubre",
                           "Noviembre",
                           "Diciembre"};

         if(calFecha.get(Calendar.DATE) <= 9){
           dd+="0" + calFecha.get(Calendar.DATE);
         }else{
           dd+=calFecha.get(Calendar.DATE);
         }
         if(calFecha.get(Calendar.MONTH)+1 <= 9){
           mm+="0" + (calFecha.get(Calendar.MONTH)+1);
         }else{
           mm+=(calFecha.get(Calendar.MONTH)+1);
         }
      	 if(calFecha.get(Calendar.HOUR_OF_DAY)<10){
      	   th+="0"+calFecha.get(Calendar.HOUR_OF_DAY);
      	}else{
      	   th+=calFecha.get(Calendar.HOUR_OF_DAY);
      	}
  	     if(calFecha.get(Calendar.MINUTE)<10){
           tm+="0"+calFecha.get(Calendar.MINUTE);
  	     }else{
           tm+=calFecha.get(Calendar.MINUTE);
  	     }
         if(calFecha.get(Calendar.SECOND)<10){
           ts+="0"+calFecha.get(Calendar.SECOND);
         }else{
           ts+=calFecha.get(Calendar.SECOND);
         }
         aaaa+=calFecha.get(Calendar.YEAR);
         aa+=aaaa.substring(aaaa.length()-2,aaaa.length());
         dt+=dias[calFecha.get(Calendar.DAY_OF_WEEK)-1];
         mt+=meses[calFecha.get(Calendar.MONTH)];

        while(indth>=0 || indtm>=0 || indts>=0 || inddd>=0 || indmm>=0 || indaaaa>=0 || indaa>=0 || indmt>=0 || inddt>=0)
       {
        indth=strFecha.indexOf("th");
        if(indth>=0){
         strFecha=strFecha.substring(0,indth)+th+strFecha.substring(indth+2,strFecha.length());
        }
        indtm=strFecha.indexOf("tm");
        if(indtm>=0){
         strFecha=strFecha.substring(0,indtm)+tm+strFecha.substring(indtm+2,strFecha.length());
        }
        indts=strFecha.indexOf("ts");
        if(indts>=0){
         strFecha=strFecha.substring(0,indts)+ts+strFecha.substring(indts+2,strFecha.length());
        }
        inddd=strFecha.indexOf("dd");
        if(inddd>=0){
         strFecha=strFecha.substring(0,inddd)+dd+strFecha.substring(inddd+2,strFecha.length());
        }
        indmm=strFecha.indexOf("mm");
        if(indmm>=0){
         strFecha=strFecha.substring(0,indmm)+mm+strFecha.substring(indmm+2,strFecha.length());
        }
        indaaaa=strFecha.indexOf("aaaa");
        if(indaaaa>=0){
         strFecha=strFecha.substring(0,indaaaa)+aaaa+strFecha.substring(indaaaa+4,strFecha.length());
        }
        indaa=strFecha.indexOf("aa");
        if(indaa>=0){
         strFecha=strFecha.substring(0,indaa)+aa+strFecha.substring(indaa+2,strFecha.length());
        }
        indmt=strFecha.indexOf("mt");
        if(indmt>=0){
         strFecha=strFecha.substring(0,indmt)+mt+strFecha.substring(indmt+2,strFecha.length());
        }
        inddt=strFecha.indexOf("dt");
        if(inddt>=0){
         strFecha=strFecha.substring(0,inddt)+dt+strFecha.substring(inddt+2,strFecha.length());
        }
      }
      return strFecha;
    }

 /**
 * Cuentas asociadas
 * @param strCuentas cuentas
 * @return arreglo
 */
public static String[][] arrCuentasAsociadas(String strCuentas)
    {
        String[][] arrCuentas = null;
        String[] Cuentas = desentramaC(strCuentas,'@'); //JRTG

        arrCuentas=new String[Integer.parseInt(Cuentas[0])+1][5];

        arrCuentas[0][0]=Cuentas[0];
        arrCuentas[0][1]="NUM_CUENTA";
        arrCuentas[0][2]="TIPO_RELAC_CTAS";
        arrCuentas[0][3]="CVE_PRODUCTO";
        arrCuentas[0][4]="NOMBRE_TITULAR";
        for (int indice=1;indice<=Integer.parseInt(Cuentas[0]);indice++)
        {
	      arrCuentas[indice]= desentramaC("4|" + Cuentas[indice] + "|",'|'); //JRTG
        }

        return arrCuentas;

    }

    /**
     * Obten Cuentas asociadas
     * @param strCuentas cuentas
     * @return arreglo
     */
    public static String[][] obtenCuentasAsociadas(String strCuentas)
    {
      String[][] arrCuentas=null;
      String strCuentas2 = strCuentas;
      String[] Cuentas = desentramaC(strCuentas2,'@');

        arrCuentas=new String[Integer.parseInt(Cuentas[0])+1][5];
        arrCuentas[0][0]=Cuentas[0];
        arrCuentas[0][1]="NUM_CUENTA";
        arrCuentas[0][2]="TIPO_RELAC_CTAS";
        arrCuentas[0][3]="CVE_PRODUCTO";
        arrCuentas[0][4]="NOMBRE_TITULAR";

        for (int indice=1;indice<=Integer.parseInt(Cuentas[0]);indice++)
        {
  	    arrCuentas[indice]= desentramaC("4|" + Cuentas[indice] + "|",'|');
        }

       return arrCuentas;

    }

 /**
  * Desentramar
 * @param buffer buffer
 * @param Separador separador
 * @return arreglo
 */
public static String[] desentramaC(String buffer, char Separador)
    {
      String bufRest;
      int cont;
      int intNoCont = Integer.parseInt(buffer.substring(0,buffer.indexOf(Separador)));
      String[] arregloSal= new String[intNoCont+1];
      arregloSal[0] = buffer.substring(0,buffer.indexOf(Separador));
      bufRest = buffer.substring(buffer.indexOf(Separador) + 1,buffer.length());
      for(cont=1;cont<=intNoCont;cont++)
      {
        arregloSal[cont] = bufRest.substring(0,bufRest.indexOf(Separador));
        bufRest = bufRest.substring(bufRest.indexOf(Separador) + 1,bufRest.length());
      }

      return arregloSal;
    }

  /**
   * Fecha actual
 * @param strFecha fecha
 * @return cadena
 */
public String fechaHoy(String strFecha)
    {
      Date Hoy = new Date();
      GregorianCalendar Cal = new GregorianCalendar();
      Cal.setTime(Hoy);

      String dd="";
      String mm="";
      String aa="";
      String aaaa="";
      String dt="";
      String mt="";
      String th="";
      String tm="";
      String ts="";

      int indth=0;
      int indtm=0;
      int indts=0;
      int inddd=0;
      int indmm=0;
      int indaaaa=0;
      int indaa=0;
      int indmt=0;
      int inddt=0;

         String[] dias = { "Domingo",
                           "Lunes",
                           "Martes",
                           "Miercoles",
                           "Jueves",
                           "Viernes",
                           "Sabado"};
         String[] meses = {"Enero",
                           "Febrero",
                           "Marzo",
                           "Abril",
                           "Mayo",
                           "Junio",
                           "Julio",
                           "Agosto",
                           "Septiembre",
                           "Octubre",
                           "Noviembre",
                           "Diciembre"};

         if(Cal.get(Calendar.DATE) <= 9){
           dd+="0" + Cal.get(Calendar.DATE);
         }else{
           dd+=Cal.get(Calendar.DATE);
         }
         if(Cal.get(Calendar.MONTH)+1 <= 9){
           mm+="0" + (Cal.get(Calendar.MONTH)+1);
         }else{
           mm+=(Cal.get(Calendar.MONTH)+1);
         }
      	 if(Cal.get(Calendar.HOUR_OF_DAY)<10){
      	   th+="0"+Cal.get(Calendar.HOUR_OF_DAY);
      	}else{
      	   th+=Cal.get(Calendar.HOUR_OF_DAY);
      	}
  	     if(Cal.get(Calendar.MINUTE)<10){
           tm+="0"+Cal.get(Calendar.MINUTE);
  	   }else{
           tm+=Cal.get(Calendar.MINUTE);
  	   }
         if(Cal.get(Calendar.SECOND)<10){
           ts+="0"+Cal.get(Calendar.SECOND);
         }else{
           ts+=Cal.get(Calendar.SECOND);
         }
         aaaa+=Cal.get(Calendar.YEAR);
         aa+=aaaa.substring(aaaa.length()-2,aaaa.length());
         dt+=dias[Cal.get(Calendar.DAY_OF_WEEK)-1];
         mt+=meses[Cal.get(Calendar.MONTH)];

      while(indth>=0 || indtm>=0 || indts>=0 || inddd>=0 || indmm>=0 || indaaaa>=0 || indaa>=0 || indmt>=0 || inddt>=0)
       {
        indth=strFecha.indexOf("th");
        if(indth>=0){
         strFecha=strFecha.substring(0,indth)+th+strFecha.substring(indth+2,strFecha.length());
        }
        indtm=strFecha.indexOf("tm");
        if(indtm>=0){
         strFecha=strFecha.substring(0,indtm)+tm+strFecha.substring(indtm+2,strFecha.length());
        }
        indts=strFecha.indexOf("ts");
        if(indts>=0){
         strFecha=strFecha.substring(0,indts)+ts+strFecha.substring(indts+2,strFecha.length());
        }
        inddd=strFecha.indexOf("dd");
        if(inddd>=0){
         strFecha=strFecha.substring(0,inddd)+dd+strFecha.substring(inddd+2,strFecha.length());
        }
        indmm=strFecha.indexOf("mm");
        if(indmm>=0){
         strFecha=strFecha.substring(0,indmm)+mm+strFecha.substring(indmm+2,strFecha.length());
        }
        indaaaa=strFecha.indexOf("aaaa");
        if(indaaaa>=0){
         strFecha=strFecha.substring(0,indaaaa)+aaaa+strFecha.substring(indaaaa+4,strFecha.length());
        }
        indaa=strFecha.indexOf("aa");
        if(indaa>=0){
         strFecha=strFecha.substring(0,indaa)+aa+strFecha.substring(indaa+2,strFecha.length());
        }
        indmt=strFecha.indexOf("mt");
        if(indmt>=0){
         strFecha=strFecha.substring(0,indmt)+mt+strFecha.substring(indmt+2,strFecha.length());
        }
        inddt=strFecha.indexOf("dt");
        if(inddt>=0){
         strFecha=strFecha.substring(0,inddt)+dt+strFecha.substring(inddt+2,strFecha.length());
        }
      }
      return strFecha;
    }

/*************************************************************************************/
/******************************************************* formatea importe en campo   */
/************************************************************************************
 * @param Tipo tipo
 * @param ind indice
 */
public  void formateaImporte( EI_Tipo Tipo, int ind )
  {
	 for(int i=0;i<Tipo.totalRegistros;i++)
	 {
		if(! ("".equals(Tipo.camposTabla[i][ind].trim())) )
		{
			// importe=new Float(Tipo.camposTabla[i][ind]).floatValue();
			// Tipo.camposTabla[i][ind]=FormatoMoneda(Float.toString(importe));
			 Tipo.camposTabla[i][ind]=FormatoMoneda(Tipo.camposTabla[i][ind]);
		}
	  }
   }

	/**
	 * Formato moneda
	 * @param cantidad
	 * @return
	 */
	public String FormatoMoneda( String cantidad )
	{
		String language = "la"; // ar
		String country = "MX";  // AF
		Locale local = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		String formato = "";
		if(cantidad ==null ||"".equals(cantidad))
			cantidad="0.0";

		double importeTemp = 0.0;
		importeTemp = new Double(cantidad).doubleValue();
		if (importeTemp < 0)
		{
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!("$".equals(formato.substring(0,1))))
		    		 if (formato.substring (0,3).equals ("(MX")){
		    			 formato ="$ -"+ formato.substring (4,formato.length () - 1);
		    		 }else{
		    			 formato ="$ -"+ formato.substring (2,formato.length ());
		    		 }
			} catch(NumberFormatException e) {formato="$0.00";}
		} else {
			try {
				formato = nf.format(new Double(cantidad).doubleValue());
				if (!(formato.substring(0,1).equals("$")))
		    		 if ("MXN".equals (formato.substring (0,3))){
		    			 formato ="$ "+ formato.substring (3,formato.length ());
		    		 }else{
		    			 formato ="$ "+ formato.substring (1,formato.length ());
		    		 }
			} catch(NumberFormatException e) {
				formato="$0.00";}
		} // del else
		if(formato==null)
			formato = "";
		return formato;
	}

	/**
	 * Formato moneda
	 * @param cantidad cantidad
	 * @return cadena
	 */
	public String FormatoMoneda( double cantidad )
	{
		String formato = "";
		String language = "la"; // la
		String country = "MX";  // MX
		Locale local = new Locale(language,  country);
		NumberFormat nf = NumberFormat.getCurrencyInstance(local);
		if (cantidad < 0)  {
			formato = nf.format( cantidad );
			try {
				if (!("$".equals(formato.substring(0,1))))
		    		 if ("MXN".equals(formato.substring (0,3))){
		    			 formato ="$ -"+ formato.substring (4,formato.length ());
		    		 }else{
		    			 formato ="$ -"+ formato.substring (2,formato.length ());
		    		 }
			} catch(NumberFormatException e) {}
		} else {
			formato = nf.format( cantidad );
			try {
				if (!("$".equals(formato.substring(0,1))))
		    		 if ("MXN".equals (formato.substring (0,3))){
		    			 formato ="$ "+ formato.substring (3,formato.length ());
		    		 }else{
		    			 formato ="$ "+ formato.substring (1,formato.length ());
		    		 }
			} catch(NumberFormatException e) {}
		}
		return formato;
	}


 /**
  * Formato importe
 * @param Tipo tipo
 * @param ind indice
 * @param val valor
 */
public void formateaImporte(EI_Tipo Tipo,int ind,float val)
   {
	 float importe=0;

     for(int i=0;i<Tipo.totalRegistros;i++)
	   {
		 if(! ("".equals(Tipo.camposTabla[i][ind].trim())) )
		   {
			 importe=new Float(Tipo.camposTabla[i][ind]).floatValue();
			 importe=importe/val;
			 Tipo.camposTabla[i][ind]=FormatoMoneda(Float.toString(importe));

		   }
	   }
   }

  /**
   * Obtener UserID
 * @return cadena
 */
public String ObtenContUserID()//Checar porque los metodos estan bilingues
  {
     return "<b>Contrato:</b>" + session.getContractNumber() +"&nbsp;" +
     		session.getNombreContrato() + "<BR>\n<b>Usuario:</b>" +
     		session.getUserID8() + "&nbsp;" + session.getNombreUsuario();
  }

		//Hace consulta para obtener las tasas de plazo
    public boolean llamada_consulta_tasas_plazos_por_servicio(String trama_buffer)
		throws ServletException, IOException
    {
		EIGlobal.mensajePorTrace("***llamada_consulta_tasas_plazos_por_servicio",EIGlobal.NivelLog.DEBUG);
        EIGlobal.mensajePorTrace("***trama_buffer:"+trama_buffer,EIGlobal.NivelLog.DEBUG);


		ServicioTux tuxGlobal = new ServicioTux();
		//tuxGlobal.setContext(((com.netscape.server.servlet.platformhttp.PlatformServletContext) contexto).getContext()    );

   	    Hashtable htResult = null;
		String CODE_ERROR_OK="TASA0000";
	    boolean respuesta=true;
		String coderror="";

		try
		{
	        EIGlobal.mensajePorTrace("***antes de llamar_servicio",EIGlobal.NivelLog.DEBUG);
			htResult = tuxGlobal.consultaTasas( trama_buffer,"|TCT|");
			coderror = ( String ) htResult.get("OUTPUTDATA");
 	        EIGlobal.mensajePorTrace("***coderror :"+coderror,EIGlobal.NivelLog.DEBUG);
			if(coderror==null){
				return false;
			}else if(coderror.indexOf('|')>0)
            {
			   coderror=coderror.substring(0,coderror.indexOf('|'));
			   if(!coderror.equals(CODE_ERROR_OK))
			   {
    	           EIGlobal.mensajePorTrace("***error :"+coderror,EIGlobal.NivelLog.DEBUG);
				   return false;
			   }
            }
			else{
				return false;
			}
	    }
		catch ( java.rmi.RemoteException re ) {
            EIGlobal.mensajePorTrace("***error:"+re.toString(),EIGlobal.NivelLog.DEBUG, re);
			}
		catch (Exception e) {
            EIGlobal.mensajePorTrace("***error:"+e.toString(),EIGlobal.NivelLog.DEBUG, e);

			}

		//traer archivo tasas.dat
		ArchivoRemoto archivo_remoto = new ArchivoRemoto();

 		if(!archivo_remoto.copiaOtroPathRemoto("tasas.dat",Global.DIRECTORIO_LOCAL) )
        {
	       EIGlobal.mensajePorTrace("***EIGlobal.class No se pudo copiar archivo remoto:"+IEnlace.REMOTO_TMP_DIR+"tasas.dat",EIGlobal.NivelLog.ERROR);
		   respuesta=false;
		}
		else
		{
	        EIGlobal.mensajePorTrace("***EIGlobal.class archivo remoto copiado"+IEnlace.REMOTO_TMP_DIR+"tasas.dat",EIGlobal.NivelLog.DEBUG);
        } // del sele

        EIGlobal.mensajePorTrace("***salida llamada_consulta_tasas_plazos_por_servicio: ",EIGlobal.NivelLog.DEBUG);
       return respuesta;
   }

/*************************************************************************************/
/******************************************************************* generaFechaAnt  */
/*************************************************************************************/
 /**
 *Te regresa un dia habil anterior al dia de hoy
 * @param tipo tipo
 * @param dia dia
 * @return cadena
 */
public static String generaFechaAnt(String tipo,int dia)
    {
      StringBuffer strFecha = new StringBuffer("");
      java.util.Date Hoy = new java.util.Date();
      GregorianCalendar Cal = new GregorianCalendar();

      Cal.setTime(Hoy);
      if(dia==1)
		{
		  do
		   {
			 Cal.add(Calendar.DATE,-1);
		   }while(Cal.get(Calendar.DAY_OF_WEEK)==1 || Cal.get(Calendar.DAY_OF_WEEK)==7);
		}

      if("FECHA".equals(tipo))
       {
         if(Cal.get(Calendar.DATE) <= 9)
		  {
            strFecha.append('0');
			strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append('/');
		  }
         else
		  {
            strFecha.append(Cal.get(Calendar.DATE) );
			strFecha.append('/');
		  }
         if(Cal.get(Calendar.MONTH)+1 <= 9)
		  {
            strFecha.append('0');
			strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append('/');
		  }
         else
		  {
            strFecha.append((Cal.get(Calendar.MONTH)+1) );
			strFecha.append('/');
		  }
         strFecha.append(Cal.get(Calendar.YEAR));
        }
      if("DIA".equals(tipo))
        strFecha.append(Cal.get(Calendar.DATE));
      if("MES".equals(tipo))
        strFecha.append((Cal.get(Calendar.MONTH)+1));
      if("ANIO".equals(tipo))
        strFecha.append(Cal.get(Calendar.YEAR));

      return strFecha.toString();
    }

	/**
	 * Metodo que busca los ceros a la izquiera dentro de un valor para su
	 * eliminacion
	 * @param valor el numero que contiene los ceros a la izquierda
	 * @return El numero sin los ceros a la izquierda
	 */
	public static String eliminaCerosImporte(String valor){
		while(valor.startsWith("0")){
			valor=valor.substring(1,valor.length());
		}
		return valor;
	}

}//fin clase
