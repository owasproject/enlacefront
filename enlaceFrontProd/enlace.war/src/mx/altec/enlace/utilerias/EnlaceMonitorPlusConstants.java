/** 
 *   Isban Mexico
 *   Clase: EnlaceMonitorPlusConstants.java
 *   Descripcion: Constantes comunes y metodos de apoyo para llenado de campos para el envio a monitor plus
 *
 *   Control de Cambios:
 *   Diciembre del 2015 FSW Everis 
 */

package mx.altec.enlace.utilerias;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.altec.enlace.beans.TrxLZADBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.TrxLZADBO;
import mx.altec.enlace.dao.CuentasDAO;
import mx.isban.mp.conector.ConectorMP;

/**
 * Contiene los valores por default y constantes comunes a tratamiento de
 * mensajes para Monitor Plus.
 * 
 * @author FSW
 * 
 */
public final class EnlaceMonitorPlusConstants {

    /** Key dia de transaccion */
    public static final int KEY_DAY_TRANS = 3;

    /** Key mes de transaccion */
    public static final int KEY_MES_TRANS = 4;

    /** Key anio de transaccion */
    public static final int KEY_YR_TRANS = 5;

    /** Key hora de transaccion */
    public static final int KEY_HR_TRANS = 6;

    /** Key min de transaccion */
    public static final int KEY_MIN_TRANS = 7;

    /** Key de la transaccion */
    public static final int KEY_TRANS = 3501;

    /** Key fecha de la transaccion con formato */
    public static final int KEY_FECHA = 3509;

    /** Key hora de transaccion */
    public static final int KEY_HR = 3510;

    /** Key monto total de la transaccion */
    public static final int KEY_MONT_TOTAL = 3508;

    /** Key monto en efectivo de la transaccion */
    public static final int KEY_MONT_EFEC = 3567;

    /** Key monto cheque propio */
    public static final int KEY_MONT_CHEPROP = 3549;

    /** Key monto cheque ajeno */
    public static final int KEY_MONT_CHEAJE = 3550;

    /** Key cuenta origen */
    public static final int KEY_CTA_ORI = 3540;

    /** Key cuenta destino */
    public static final int KEY_CTA_DEST = 3504;

    /** Key direccion ip */
    public static final int KEY_DICC_IP = 3556;

    /** Key nombre usario titular origen */
    public static final int KEY_NOM_USR_ORIGEN = 3523;

    /** Key numero de referencia */
    public static final int KEY_NUM_REFER = 3563;

    /** Key tipo de operacion */
    public static final int KEY_TIPO_OPERA = 3568;

    /** Key signo valor */
    public static final int KEY_SIGNO_VAL = 3597;

    /** Key saldo disponible */
    public static final int KEY_SALDO_DISPONIBLE = 3521;

    /** Key fecha apertura de cuenta */
    public static final int KEY_FCH_APERTURA_CTA = 3533;

    /** Key codigo de cliente */
    public static final int KEY_COD_CTE = 3539;

    /** Key monto de sobregiro autoirzado */
    public static final int KEY_MONTO_SOBREGIRO_AUTORIZADO = 3552;

    /** Key indicador de Sobregiro */
    public static final int KEY_INDICADOR_SOBREGIRO = 3553;

    /** Key flag tipo tuenta VIP */
    public static final int KEY_FLG_CTA_VIP = 3554;

    /** Key cosntante BYTEI */
    public static final int KEY_BYTEI = 1;

    /** Key modo analisis */
    public static final int KEY_MODO_ANALISIS = 2;

    /** Key genera transaccion */
    public static final int KEY_GEN_TRANS = 9;

    /** Key sucursal de la transaccion */
    public static final int KEY_SURCU_TRANS = 3505;

    /** Key usuario de la transaccion */
    public static final int KEY_USUA_TRANS = 3518;

    /** Key constante BYTEF */
    public static final int KEY_BYTEF = 10;

    /** Key constante codigo de evento */
    public static final int KEY_CODIGO_EVENTO = 8;

    /** Key id del cliente */
    public static final int KEY_ID_CLIENTE = 3507;

    /** Key entidad */
    public static final int KEY_ENTIDAD = 3545;

    /** Key transaccion */
    public static final int KEY_TRANSACC = 3511;

    /** Key id personal */
    public static final int KEY_ID_PERSONAL = 3640;

    /** Key canal transaccion */
    public static final int KEY_CANAL_TRANS = 3503;

    /** Key tipo de chequera */
    public static final int KEY_TIPO_CHEQUERA = 3605;

    /** Key cuenta origen */
    public static final int KEY_FLG_CTA_ORIGEN = 3515;

    /** Key cuenta destino */
    public static final int KEY_FLG_CTA_DESTINO = 3538;

    /** Key constante credito */
    public static final int KEY_CARGO= 3544;

    /** Key institucion del cliente */
    public static final int KEY_CLIEN_INSTI = 3522;

    /** Key autoriza transaccion */
    public static final int KEY_EMPLEADO_AUTORIZA = 3536;

    /** Key confirmacion de la transaccion */
    public static final int KEY_CONF_TRANS = 3576;

    /** Key resolucion de la pantalla */
    public static final int KEY_RES_PANT = 3755;

    /** Key color de pantalla */
    public static final int KEY_COL_PANT = 3756;

    /** Key tamanio de la pantalla */
    public static final int KEY_TAM_PANT = 3757;

    /** Key navegador del usuario */
    public static final int KEY_NAV_USUA = 3758;

    /** Key num release */
    public static final int KEY_NUM_RELEASE = 3759;

    /** Key version del browser */
    public static final int KEY_VER_BROW = 3764;

    /** Key sistema operativo */
    public static final int KEY_OS = 3765;

    /** Key ip del usuario */
    public static final int KEY_IP = 3766;

    /** Key idioma */
    public static final int KEY_IDIOMA = 3767;

    /** Key codigo del tipo sucursal */
    public static final int KEY_COD_TIP_SUCURSAL = 3614;

    /** Key estatus de cuenta o producto */
    public static final int KEY_EST_CTA_PROD = 3517;

    /** Key cuanta registrada a internet */
    public static final int KEY_REG_INTERNET = 3525;

    /** Key region de la sucursal */
    public static final int KEY_REGION_SUCURSAL = 3546;

    /** Key numero inicial talonario */
    public static final int KEY_INI_TALONARIO = 3588;

    /** Key numero final talonario */
    public static final int KEY_FIN_TALONARIO = 3589;

    /** Key ultimo numero de talonario */
    public static final int KEY_ULTI_NUM_TALONARIO = 3590;

    /** Key numero inicial chequera */
    public static final int KEY_INI_NUM_CHEQUE = 3591;

    /** Key numero final de cheque de pago */
    public static final int KEY_FIN_NUM_CHEQUE = 3592;

    /** Key ultimo cheque de pago */
    public static final int KEY_ULT_CHEQ_PAGO = 3593;

    /** Key id cliente destino */
    public static final int KEY_ID_CLIENTE_DES = 3598;

    /** Key uso futuro 1 alfanumerico */
    public static final int KEY_FUTURO_UNO_ALF = 3506;

    /** Key uso futuro 2 alfanumerico */
    public static final int KEY_FUTURO_DOS_ALF = 3750;

    /** Key uso futuro 3 alfanumerico */
    public static final int KEY_FUTURO_TRES_ALF = 3751;

    /** Key uso futuro 1 numerico */
    public static final int KEY_FUTURO_UNO_NUM = 3752;

    /** Key uso futuro 2 numerico */
    public static final int KEY_FUTURO_DOS_NUM = 3753;

    /** Key uso futuro 3 numerico */
    public static final int KEY_FUTURO_TRES_NUM = 3754;

    /** Key ISP internet service provider */
    public static final int KEY_ISP_INTERNET = 3514;

    /** Key origen alerta */
    public static final int KEY_ORIGEN_ALERT = 3530;

    /** KEY FECHA ULTIMO MOV CTA */
    public static final int KEY_FCH_ULT_MOV = 3641;

    /** KEY Indicador de fraude */
    public static final int KEY_INDICADOR_FRAUDE = 3529;

    /** KEY Ident Trx Fraude */
    public static final int KEY_IDENT_TRX_FRAUDE = 3534;

    /** KEY resolusion de pantalla */
    public static final int KEY_RESOLU_PANTALLA = 3755;

    /** KEY colore de la pantalla */
    public static final int KEY_COLOR_DE_PANTALLA = 3756;

    /** KEY Tamanio de la Ventana del Browser */
    public static final int KEY_TAMA_VENT_BRO = 3757;

    /** KEY Numero de version del browser */
    public static final int KEY_NUM_VERS_BROS = 3759;

    /** Key tipo de persona */
    public static final int KEY_TIPO_PERSONA = 3532;

    /** Key dato anterior */
    public static final int KEY_DATO_ANTERIOR = 3600;

    /** Key tipo de producto */
    public static final int KEY_TIP_PROD_DEST = 3571;

    /** key Direccion del cliente origen */
    public static final int KEY_DIRECCION = 3557;

    /** key Num de residencia del cliente origen */
    public static final int KEY_TELEFONO_RESIDENCIA = 3558;

    /** key Telefono de oficina del cliente origen */
    public static final int KEY_TELEFONO_OFICINA = 3559;

    /** key otro Telefono del cliente origen */
    public static final int KEY_TELEFONO_OTRO = 3560;

    /** Key codigo respuesta */
    public static final int KEY_COD_RESPUESTA = 3527;

    /** key mail del cliente origen */
    public static final int KEY_MAIL = 3561;

    /** key fecha de vinculacion del cliente origen */
    public static final int KEY_FECHA_VINCULACION_CLIENT = 3535;

    /** key Num de residencia del cliente origen */
    public static final int KEY_SEGM_CLIENT = 3620;

    /** Key referencia de paga serv 1 */
    public static final int KEY_REFE_PAGO_SERV_UNO = 3760;

    /** Key referencia de paga serv 2 */
    public static final int KEY_REFE_PAGO_SERV_DOS = 3761;

    /** Key referencia de paga serv 3 */
    public static final int KEY_REFE_PAGO_SERV_TRES = 3762;

    /** Key referencia de paga serv 4 */
    public static final int KEY_REFE_PAGO_SERV_CUATRO = 3763;

    /** Key Tipo de Servicio */
    public static final int KEY_TIPO_SERVI = 3564;

    /** Key Codigo P03 */
    public static final int KEY_COD_P03 = 3569;

    /** Key Banco destino */
    public static final int KEY_BANCO_DESTINO = 3570;

    /** Constante BYTEI Indica inicio de la trama. */
    public static final String BYTEI = "ByteI";

    /** Constante Mancomunda */
    public static final String MANC = "MANC";

    /** Constante OK */
    public static final String OK = "OK";

    /** Constante MODO_ANALISIS_N Indica modo de analisis no interactivo. */
    public static final String MODO_ANALISIS_N = "N";

    /** Constante MODO_ANALISIS_I Indica modo de analisis interactivo */
    public static final String MODO_ANALISIS_I = "I";

    /** Constante CODIGO_EVENTO_318 NOVEDADES */
    public static final String CODIGO_EVENTO_318 = "00318";

    /** Constante CODIGO_EVENTO_345 CUENTAS */
    public static final String CODIGO_EVENTO_345 = "00345";

    /** Constante CODIGO_EVENTO_310 APERTURAS DE CUENTAS */
    public static final String CODIGO_EVENTO_310 = "00310";

    /** Constante BYTEF Indica el fin de la trama. */
    public static final String BYTEF = "ByteF";

    /** Constante para el valor del id del usuario. */
    public static final String IV_USER = "iv-user";

    /** Estatuc cuenta o producto */
    public static final String ESTATUS_CUENTA = "A";

    /** Tipo de sucursal */
    public static final String TIPO_SUCURSAL = "0981";

    /** Numero de Empresa, Compania o Entidad. */
    public static final String CODIGO_COMPANIA_ENTIDAD = "ENLA";

    /** Constante para el valor del canal. */
    public static final String CANAL_INTERNET = "0981";

    /** Constante para el valor del navegador. */
    public static final String NAVEGADOR = "User-Agent";

    /** Constante para el valor de la IP. */
    public static final String REMOTE_ADDR = "iv-remote-address";

    /** Constante para el valor del lenguaje. */
    public static final String LENGUAJE = "Accept-Language";

    /** Constante para el valor del reverso. */
    public static final String REVERSO = "N";

    /** Constante CADENA_VACIA. */
    public static final String CADENA_VACIA = "";

    /** Constante de identificacion de tipo de Transaccion. */
    public static final String CARGO = "C";

    /** Constante de Registro de Internet. */
    public static final String CUENTA_REGISTRADA = "S";

    /** Tipo de mensaje. */
    public static final String TIPO_MENS_TRANS = "TRAN";

    /** Codigo del Tipo de Sucursal. */
    public static final String COD_TIP_SUCURSAL = "0981";

    /** Flag Cuenta Registrada Internet. */
    public static final String REG_INTERNET = "S";

    /** Codigo de la Moneda Nacional. */
    public static final String COD_MON_NAC = "484";

    /** Codigo de la Moneda Dolar. */
    public static final String COD_MON_DOL = "840";

    /** key numero de trx **/
    public static final int KEY_NUM_TRX = 3548;

    /** Formato de fecha con la hora para envio y de operacion */
    public static final String FORMATO_FECH_GRAL = "yyyyMMddHHmmss";

    /** Formato de fecha para envio y de operacion */
    public static final String FORMATO_FECH = "yyyyMMdd";

    /** Formato de la hora para envio y de operacion */
    public static final String FORMATO_HORA = "HHmmss";

    /** Constante LOCALE. */
    public static final Locale LOCALE_MX = new Locale("ES", "mx");

    /** Formato para dia */
    public static final String FORMATO_DD = "dd";

    /** Formato para mes */
    public static final String FORMATO_MM = "MM";

    /** Formato para anio */
    public static final String FORMATO_YYYY = "yyyy";

    /** Formato para horas */
    public static final String FORMATO_HH = "HH";

    /** Formato para minutos */
    public static final String FORMATO_MIN = "mm";

    /** Constante MONEDA */
    public static final String MONEDA = "moneda";

    /** Constante dolar */
    public static final String DOLAR = "Dolares";

    /** Constante Moneda Nacional */
    public static final String MON_NAC = "Moneda Nacional";

    /** Key codigo moneda */
    public static final int KEY_COD_MON = 3516;

    /** Constante Tipo de Operacion ADF310 */
    public static final String MSG_ADF310 = "ADF310";

    /** Constante tipo de mensaje ADF345 */
    public static final String MSG_ADF345 = "ADF345";

    /** Constante MSG_ADF_318 Tipo de mensaje a enviar a MonitorPlus */
    public static final String MSG_ADF_318 = "ADF318";

    /** Constante MAN_CONTRATO */
    public static final int MAN_CONTRATO = 1;

    /** Constante MAN_USUARIO */
    public static final int MAN_USUARIO = 0;

    /** Constante tipo de persona */
    public static final String TIPO_PERS = "M";

    /** key sucursal receptora de la transaccion */
    public static final int KEY_SUC_RECEP = 3573;

    /** Constante Usuario que confirma la transaccion */
    public static final String USU_CONF_TRANS = "00000";

    /** Key Nombre Cliente Titular Cta. Destino */
    public static final int KEY_NOMBRE_DESTINO = 3524;

    /** Constante DATOS_BROWSER */
    public static final String DATOS_BROWSER = "datosBrowser";

    /**
     * /** Constante Usuario que confirma la transaccion
     */
    public static final String INDIV = "I";

    /** Constante Usuario que confirma la transaccion */
    public static final String MASIVA = "M";

    /** Key del tipo de transaccion */
    public static final int KEY_TIP_TRANS = 3579;

    /** Signo valor */
    public static final String SIG_VALOR = " ";

    /** Key localidad */
    public static final int KEY_LOCALIDAD = 3520;

    /** Key ID Terminal */
    public static final int KEY_ID_TERMINAL = 3519;

    /** Codigo de producto */
    public static final int KEY_CODIGO_PRODUCTO = 3502;

    /** Codigo subproducto */
    public static final int KEY_CODIGO_SUBPRODUCTO = 3566;

    /** Sucursal Apertura Cuenta Origen */
    public static final int KEY_SUCURSAL_APERTURA_CO = 3537;

    /** Sucursal Apertura Cuenta Destino */
    public static final int KEY_SUCURSAL_APERTURA_CD = 3596;

    /** No de documento cheque */
    public static final int KEY_NUM_DOC_CHEQUE = 3512;

    /** Fecha Compensacion */
    public static final int KEY_FECHA_COMPENSACION = 3572;

    /** Tipo Usuario */
    public static final int KEY_TIPO_USUARIO = 3587;
    
    /** Tipo de Novedad */
    public static final int KEY_TIPO_NOVEDAD = 3607;

    /** Constante CADENA_CERO */
    public static final String CADENA_CERO = "0";
    
    /** Codigo de identificacion operacion Transferencia mismo banco. */
    public static final String COD_TRAN_TMB = "TRAN";
    
    /** Codigo de identificacion operacion Transferencia mismo banco. */
    public static final String COD_TRAN_DIBT = "DIBT";
    
    /** Codigo de identificacion operacion ordenes de pago ocurre (fisicos, morales, individual y por archivo). */
    public static final String COD_TRAN_OCUR = "OCUR";

    /** Codigo de identificacion operacion alta de usuario a contrato. */
    public static final String COD_TRAN_AUSS = "AUSS";

    /** Codigo de identificacion operacion mantenimiento de datos personales (actulizacion de contrato o usuario). */
    public static final String COD_TRAN_MODP = "MODP";
    
    /** Codigo de identificacion operacion de alta y activacion de cuentas. */
    public static final String COD_TRAN_ALCT = "ALCT";
    
    /** Constante ID_TERMINAL. */
    public static final String ID_TERMINAL = "ENLACE0981";
    
    /** Constante TMB_MULTPLE_CUENTA_ORIGEN */
    public static final int TMB_MULTPLE_CUENTA_ORIGEN = 0;
    
    /** Constante TMB_MULTPLE_CUENTA_DESTINO */
    public static final int TMB_MULTPLE_CUENTA_DESTINO = 1;
    
    /** Constante TMB_MULTPLE_COD_RESPUESTA */
    public static final int TMB_MULTPLE_COD_RESPUESTA = 3;
    
    /** Constante TMB_MULTPLE_CLIENTE_CTA_ORIGEN */
    public static final int TMB_MULTPLE_CLIENTE_CTA_ORIGEN = 0;
    
    /** Constante TMB_MULTPLE_CLIENTE_CTA_DESTINO */
    public static final int TMB_MULTPLE_CLIENTE_CTA_DESTINO = 1;
    
    /** Constante KEY_FDN_CONSTITUCION*/
    public static final int KEY_FDN_CONSTITUCION = 3626;
    
    /** Constante KEY_LDN_CONSTITUCION*/
    public static final int KEY_LDN_CONSTITUCION = 3627;
    
    /** Constante KEY_SEXO_CTAHABIENTE*/
    public static final int KEY_SEXO_CTAHABIENTE = 3628;
    
    /** Constante KEY_EDO_CIVIL_CTAHABIENTE*/
    public static final int KEY_EDO_CIVIL_CTAHABIENTE = 3629;

    /** Constante KEY_ACT_ECONOMICA*/
    public static final int KEY_ACT_ECONOMICA = 3630;
    
    /** Constante ALTA_CTAS_NOMBRE_CLIENTE*/
    public static final int ALTA_CTAS_NOMBRE_CLIENTE = 1;
    
    /** Constante ALTA_CTAS_NUMCTA_ORIGEN*/
    public static final int ALTA_CTAS_NUMCTA_ORIGEN = 1;
    /** Constante que indica el estatus pendiente pos activar de una cuenta */
    public static final String ESTATUS_PENDIENTE_AUTORIZAR = "Pendiente por autorizar"; 
    
    /** Constante que indica la operacion de autorizacion en el modulo */
    public static final String TIPO_OPERACION_AUTORIZACION = "A";
    
    /** Constante que indica por medio de la descripcion el estatus que es un estatus de pendiende de activar */
    public static final String NOM_ESTATUS_AUTORIZAR = "Pendiente por activar";
    
    /** Constante que indica la operacion de cancelacion en el modulo */
    public static final String TIPO_OPERACION_CANCELACION = "C";
    
    /** Constante que indica la operacion de autorizacion en el modulo */
    public static final String TIPO_SOLICITUD_ALTA = "A";
    
    /** Constante KEY_CVE_TRASNFER_INTERBANCARIA */
    public static final int KEY_CVE_TRASNFER_INTERBANCARIA = 3543;
    
    /** Constante KEY_ID_RASTREO_TRASFERENCIA */
    public static final int KEY_ID_RASTREO_TRASFERENCIA = 3555;
    
    /** Constante KEY_USGEN_TRAX_USCNTR.*/
    public static final int KEY_USGEN_TRAX_USCNTR = 3610;
    
    /** Constante TMB_MULTIPLE_NOM_USR_ORIGEN. */
    public static final int TMB_MULTIPLE_NOM_USR_ORIGEN = 2;
    
    /** Constante TMB_MULTIPLE_NOMBRE_DESTINO.*/
    public static final int TMB_MULTIPLE_NOMBRE_DESTINO = 6;
    
    /** Constante FLG_OPER_345. */
    public static final String FLG_OPER_345 = "1";
    
    /** Constante FLG_OPER_310.*/
    public static final String FLG_OPER_310 = "2";
    
    /** Constante FLG_OPER_318. */
    public static final String FLG_OPER_318 = "3";
    
    /** Constante FLG_OPER_X_ARCH. */
    public static final String FLG_OPER_X_ARCH = "4";
    
    /** Constante key de referencia contrato enlace.*/
    public static final int KEY_CONTRATO_ENLA = 11;
    
    /**  Constante key de Cta Eje.*/
    public static final int KEY_CTA_EJE = 12;
    
    /** Constante key de Pais ubicacion IP.*/
    public static final int KEY_PAIS_UB_IP = 13;
    
    /** Constante key Estado o Provincia asociada IP.*/
    public static final int KEY_PROV_IP = 14;
    
    /** Constante key Banco Cuenta Asociada.*/
    public static final int KEY_BANC_CTAAS = 15;
    
    /** Constante key Pais Cuenta Asociada.*/
    public static final int KEY_PAIS_CTAAS = 16;
    
    /** Constante key Divisa.*/
    public static final int KEY_DIVISA = 17;
    
    /** Constante key Clave ABA_SWIFT.*/
    public static final int KEY_CVE_ABA_SWIFT = 18;
    
    /** Constante key Tipo Cuenta.*/
    public static final int KEY_TIPO_CTA = 19;
    
    /** Constante key Identificador Cuenta Asociada Archivo.*/
    public static final int KEY_ID_CTAAS_ARCHIVO = 20;
    
    /** Constante DIVISA_PESOS.*/
    public static final String DIVISA_PESOS = "MXP";
    
    /** Constante DIVISA_DOLARES.*/
    public static final String DIVISA_DOLARES = "USD";
    
    /** Constante identificador con  archivo.*/
    public static final String IDEN_CON_ARCHIVO = "M";
    
    /** Constante identificador sin archivo.*/
    public static final String IDEN_SIN_ARCHIVO = "I";
    
    /** Constante pais banco asociado.*/
    public static final String PAIS_BANC_CTAAS = "Mexico";
    
    /** Constante key aplicacion.*/
    public static final int KEY_APLICACION = 21;
    
    /** Constante APLICACION .*/
    public static final String APLICACION_ENLA = "Enlace";
    
    /** Constante Clabe .*/
    public static final String CLABE = "CLABE";
    
    /** Constante Tarjeta de debito .*/
    public static final String TARJ_DEBITO = "Tarjeta de Debito";
    //S-Super Urs / U-Usr Normal
    /** Constante para tipo usuario normal.*/
    public static final String USR_NORMAL = "U-Usr Normal";
    
    /** Constante para tipo usuario super usuario.*/ 
    public static final String SUPER_USR = "S-Super Usr";
    
    /** Constante COD_ERROR.*/
    public static final String COD_ERROR = "ERQCE0000";
    
    /** Constante COD_RESP.*/
    public static final String COD_RESP = "0000";
    
    /** Constante TMB BANCO CTAAS*/
    public static final String TMB_BANCO_CTAAS = "Santander";
    
    /** Codigo subproducto */
    public static final int KEY_COD_SUBPRODUCTO_DEST = 22;
    
    /** Constante Clabe .*/
    public static final String  ALCTAS_BN_CLABE = "Cuenta clabe interbancaria";
    
    /** Constante Tarjeta de debito .*/
    public static final String ALCTAS_BN_TARJ_DEBITO = "Tarjeta debito / credito";
    
    /** Constante ALCTAS_MB*/
    public static final String ALCTAS_MB = "Mismo banco";
    
    /** Constante ALCTAS_MB*/
    public static final String ALCTAS_BI = "Cuenta internacional";

    /** Constante ALCTAS_MB*/
    public static final String ALCTAS_MOVIL_MB = "Cuenta movil mismo banco";

    /** Constante ALCTAS_MB*/
    public static final String ALCTAS_MOVIL_BN = "Cuenta movil interbancaria";
    
    /** Constante BROWSER_IEXPLORER.*/
    public static final String BROWSER_IEXPLORER = "Explorer";
    
    /** Constante BROWSER_UNDEFINED*/
    public static final String BROWSER_UNDEFINED = "undefined";
    
    /** Constante BROWSER_LANGUAGE */
    public static final String BROWSER_LANGUAGE = "Accept-Language";

    /**
     * Constructor privado de la clase
     */
    private EnlaceMonitorPlusConstants() {

    }

    /**
     * Carga los valores default para las operaciones que se enviaran a monitor
     * plus (TMB, TIB, T.Multiples, Pagos Ocurre, Alta de cuentas, autorizacion
     * y cancelacion de cuentas, Inclusion de usuario en contrato, Actualizacion
     * de datos de contrato o usuario)
     * @param req Objeto con informacion inherente a la peticion
     * @param sucursalOperante sucursal operante enlace
     * @param sesion Objeto de session
     * @return El mapa de valores default
     */
    public static Map<Integer, String> cargaValoresDefault(HttpServletRequest req, String sucursalOperante, BaseResource sesion) {
        EIGlobal.mensajePorTrace("Inicio cargaValoresDefault MP", EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = new HashMap<Integer, String>();
        Date fechaEnvio = new Date();
        // Inicio de trama Bytel 1
        mapa.put(KEY_BYTEI, BYTEI);
        // Modo Analisis 2
        mapa.put(KEY_MODO_ANALISIS, MODO_ANALISIS_N);
        // Dia de la transaccion 3
        mapa.put(EnlaceMonitorPlusConstants.KEY_DAY_TRANS, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_DD, EnlaceMonitorPlusConstants.LOCALE_MX)
            .format(fechaEnvio));
        // Mes de la transaccion 4
        mapa.put(EnlaceMonitorPlusConstants.KEY_MES_TRANS, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_MM, EnlaceMonitorPlusConstants.LOCALE_MX)
            .format(fechaEnvio));
        // Anio de la transaccion 5
        mapa.put(EnlaceMonitorPlusConstants.KEY_YR_TRANS, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_YYYY, EnlaceMonitorPlusConstants.LOCALE_MX)
            .format(fechaEnvio));
        // Hora de la transaccion 6
        mapa.put(EnlaceMonitorPlusConstants.KEY_HR_TRANS, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_HH, EnlaceMonitorPlusConstants.LOCALE_MX)
            .format(fechaEnvio));
        // Minuto de la transaccion 7
        mapa.put(EnlaceMonitorPlusConstants.KEY_MIN_TRANS, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_MIN, EnlaceMonitorPlusConstants.LOCALE_MX)
            .format(fechaEnvio));
        // Usuario Genera transaccion 9
        mapa.put(KEY_GEN_TRANS, req.getHeader(IV_USER));
        // sucursal de la transaccion 3505
        mapa.put(KEY_SURCU_TRANS, sucursalOperante);
        // Usuario que realiza la transaccion 3518
        mapa.put(KEY_USUA_TRANS, req.getHeader(IV_USER));
        // ID Terminal 3519
        mapa.put(EnlaceMonitorPlusConstants.KEY_ID_TERMINAL, EnlaceMonitorPlusConstants.ID_TERMINAL);
        // id sesion 1 3506
        mapa.put(EnlaceMonitorPlusConstants.KEY_FUTURO_UNO_ALF, req.getSession().getId()); // falta validar la longitud
        // Constante Bytel 10
        mapa.put(KEY_BYTEF, BYTEF);
        //contrato enlace 11
        mapa.put(EnlaceMonitorPlusConstants.KEY_CONTRATO_ENLA , sesion.getContractNumber());
        Map<Integer, String> tctCta = CuentasDAO.consultarDatosMP(sesion.getContractNumber());
        // cta Eje 12
        mapa.put(EnlaceMonitorPlusConstants.KEY_CTA_EJE, (tctCta != null && tctCta.get(1) != null) ? tctCta.get(1).trim() : EnlaceMonitorPlusConstants.CADENA_VACIA );
        // Cod Funcionario/Empleado
        mapa.put(KEY_EMPLEADO_AUTORIZA, req.getHeader(IV_USER));
        // Default: 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 3505, 3506, 3518, 3519
        // Ceros/blancos: 3536, 3750, 3752, 3753

        // DATOS DE BROWSER
        String datBr = (String) req.getSession().getAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER);
        if (datBr == null && req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER) != null) {
            datBr = (String) req.getParameter(EnlaceMonitorPlusConstants.DATOS_BROWSER);
            req.getSession().setAttribute(EnlaceMonitorPlusConstants.DATOS_BROWSER, datBr);
        }
        String[] datosBrower =  datBr != null && datBr.length() > 0 ? datBr.split("[|]") : null;
        if (datosBrower != null && datosBrower.length == 8) {
            for (String string : datosBrower) {
                EIGlobal.mensajePorTrace("cargaValoresDefault de datosBrowser|" + string + "|", EIGlobal.NivelLog.DEBUG);
            }
            // Resolucion de Pantalla 3755
            mapa.put(KEY_RES_PANT, datosBrower[0]);
            // Tamano de ventana 3757
            mapa.put(KEY_TAM_PANT, (datosBrower[1].indexOf(BROWSER_UNDEFINED)>-1)?EnlaceMonitorPlusConstants.CADENA_VACIA : datosBrower[1]);
            // Navegador y sistema operativo desde donde se conecta el usuario 3758
            mapa.put(KEY_NAV_USUA, datosBrower[3]);
            // *** Numero de release del Browser 3759
            mapa.put(KEY_NUM_RELEASE, datosBrower[4]);
            // Numero de la version del Browser 3764 
            mapa.put(KEY_VER_BROW, datosBrower[5]);
            // Sistema Operativo 3765
            mapa.put(KEY_OS, datosBrower[6]);
            // Colores de Pantalla 3756
            mapa.put(KEY_COL_PANT, datosBrower[2]);
            
            if(BROWSER_UNDEFINED.equals(datosBrower[7])){
            	EIGlobal.mensajePorTrace("UNDEF" , EIGlobal.NivelLog.DEBUG);
            	// Idioma del navegador 3767
            	mapa.put(KEY_IDIOMA, (req.getHeader(BROWSER_LANGUAGE)!= null && EnlaceMonitorPlusConstants.CADENA_VACIA.equals(req.getHeader(BROWSER_LANGUAGE))? req
            			.getHeader(BROWSER_LANGUAGE): EnlaceMonitorPlusConstants.CADENA_VACIA));
            }else{
            	
                // Idioma del navegador 3767
                mapa.put(KEY_IDIOMA, datosBrower[7]);
            }
        } else {
            EIGlobal.mensajePorTrace("cargaValoresDefaultTransferencia, datosBrowser | vacio" , EIGlobal.NivelLog.DEBUG);
        }
        
        EIGlobal.mensajePorTrace("Fin cargaValoresDefault MP", EIGlobal.NivelLog.DEBUG); 
        return mapa;
    }

    /**
     * Carga valores default para las operaciones
     * TMB (linea mn, archivo mn, dolares linea, dolares archivo),
     * TIB (individual, archivo)
     * OCUR (PER FIS individual, archivo, PER MOR individual, archivo)
     * @param req Objeto con informacion inherente a la peticion
     * @param sesion objeto con informacion de la session
     * @param sucursalOperante sucursal operante enlace
     * @param ctaOrigen Cuenta origen
     * @param ctaDestino Cuenta destino
     * @return mapa
     */
    public static Map<Integer, String> cargaValoresDefaultTransferencia(HttpServletRequest req, 
            BaseResource sesion, String sucursalOperante, String ctaOrigen,String ctaDestino) {
        EIGlobal.mensajePorTrace("Inicio cargaValoresDefaultTransferencia MP ctaOrigen |"
                + ctaOrigen + "| ctaDestino |" 
                + ctaDestino + "|", EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = cargaValoresDefault(req, sucursalOperante, sesion);
        // Constante 00345 8
        mapa.put(KEY_CODIGO_EVENTO, CODIGO_EVENTO_345);
        // Codigo Entidad /Compania 3545
        mapa.put(KEY_ENTIDAD, CODIGO_COMPANIA_ENTIDAD);
        
        Date fecha = new Date();
        // Fecha de la Realizacion de la Transaccion en formato AAAAMMDD 3509
        mapa.put(EnlaceMonitorPlusConstants.KEY_FECHA, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_FECH, EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Hora de la Realizacion de la Transaccion en formato HHMMSS 3510
        mapa.put(EnlaceMonitorPlusConstants.KEY_HR, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_HORA, EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Tipo Sucursal 3614
        mapa.put(EnlaceMonitorPlusConstants.KEY_COD_TIP_SUCURSAL,
                EnlaceMonitorPlusConstants.TIPO_SUCURSAL);
        // Identifica la transaccion 3511
        mapa.put(KEY_TRANSACC, REVERSO);
        // Id de empleado 3640
        mapa.put(KEY_ID_PERSONAL, req.getHeader(IV_USER));
        // Canal o Medio donde se origino la transaccion 3503
        mapa.put(KEY_CANAL_TRANS, CANAL_INTERNET);
        // Identificacion de la transaccion 3544
        mapa.put(KEY_CARGO, CARGO);
        // Identifica si la cuenta puede ser utilizada 3525
        mapa.put(KEY_REG_INTERNET, CUENTA_REGISTRADA);
        // Tipo persona 3532
        mapa.put(KEY_TIPO_PERSONA, TIPO_PERS);
        
        // tipo usuario 3587
        Map<Integer, String> tipoUs = CuentasDAO.consultarDatosMP(sesion.getContractNumber());
        mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_USUARIO, 
                (tipoUs != null && tipoUs.get(2) != null
                && tipoUs.get(2).equals(((BaseResource) req.getSession().getAttribute ("session")).getUserID8()))
                ? EnlaceMonitorPlusConstants.SUPER_USR : EnlaceMonitorPlusConstants.USR_NORMAL );

        // Direccion IP caller 3556
        mapa.put(KEY_DICC_IP, (req.getHeader(REMOTE_ADDR) != null && !EnlaceMonitorPlusConstants.CADENA_VACIA.equals(req.getHeader(REMOTE_ADDR).trim()) ? req
            .getHeader(REMOTE_ADDR) : req.getRemoteAddr()));
        // Direccion IP donde se origino la el cargue de aplicacion 3766
        mapa.put(KEY_IP, (req.getHeader(REMOTE_ADDR) != null && !EnlaceMonitorPlusConstants.CADENA_VACIA.equals(req.getHeader(REMOTE_ADDR).trim()) ? req
            .getHeader(REMOTE_ADDR) : req.getRemoteAddr()));
        
        // Se obtienen los valores de la trx 390
        TrxLZADBO trx390 = new TrxLZADBO();
        
		if (ctaOrigen != EnlaceMonitorPlusConstants.CADENA_VACIA
				&& ctaOrigen != null) {
			boolean valida = false;
			if (ConectorMP.isInit()) {
				valida = trx390.validaTrxLZAD(
						rellenaValores(false, 5, CODIGO_EVENTO_345.replaceFirst("^0*", "")),
						((ctaOrigen.length() == 11) ? rellenaValores(false, 20,
						ctaOrigen) : ctaOrigen),rellenaValores(false, 20, ctaDestino));
				if (valida && !COD_ERROR.equals(trx390.getBean().getCodError())) {
					EIGlobal.mensajePorTrace(
							"cargaValoresDefaultTransferencia, obtiene trx390 y  mapea campos ",
							EIGlobal.NivelLog.DEBUG);
					TrxLZADBean bean = trx390.getBean();
					llenarDatosTx(mapa, bean);

				}
			}else{
				EIGlobal.mensajePorTrace("EL conector no esta inicializado, no se invoca la TRX390", EIGlobal.NivelLog.DEBUG);
			}
			if (valida ^ true) {
                EIGlobal.mensajePorTrace("cargaValoresDefaultTransferencia, no se obtuvo datos de trx390, por tanto no se informan a MP", EIGlobal.NivelLog.DEBUG);
                mapa.put(90000, "TRXERROR");
            }
		} else {
			EIGlobal.mensajePorTrace(" Archivo no requiere trx390",
					EIGlobal.NivelLog.DEBUG);

		}
        
        // NvaTrax: 3507,3502,3566,3605,3517,3597,3521,3533,3522,3539,3537,3596,3523,3524,3557,3558,3559,3560,3561,3535,3620,3571,3566,3591,3592,3641
        // Ceros o Blancos: 3751, 3754, 3514, 3529, 3530, 3534, 3515, 3538, 3576, 3527, 3552, 3553, 3554, 3557, 3568, 3588, 3589, 3590, 3591, 3592, 3593, 3598, 3546
        // No aplican: 3520, 3587, 3564, 3760, 3761, 3762, 3763, 3569, 3572,13,14, 18
        // cargaValoresDefaultTransferencia: 8, 3545, 3509, 3510, 3614, 3511, 3640, 3503, 3544, 3525, 3532, 3556, 3755, 3756, 3757, 3758, 3759, 3764, 3765, 3767, 3766
        EIGlobal.mensajePorTrace("Fin cargaValoresDefaultTransferencia MP", EIGlobal.NivelLog.DEBUG);
        return mapa;
    }
    /**
     * Mapea campos obtenidos de la transaccion
     * @param mapa contiene los valores que se enviaran a MP
     * @param bean transporta los valores que se obtuvieron de la transaccion 
     */
	private static void llenarDatosTx(Map<Integer, String> mapa,
			TrxLZADBean bean) {
		// ID  Cliente RFC 3507 
		mapa.put(KEY_ID_CLIENTE, bean.getCampo(0));
		// Codigo de Producto Origen 3502
		mapa.put(KEY_CODIGO_PRODUCTO, bean.getCampo(1));
		// Codigo Subproducto Origen 3566
		mapa.put(KEY_CODIGO_SUBPRODUCTO, bean.getCampo(2));
		// Tipo de Chequera 3605
		mapa.put(KEY_TIPO_CHEQUERA, bean.getCampo(3));
		// Estatus de Cta. o Producto 3517
		mapa.put(KEY_EST_CTA_PROD, bean.getCampo(4));
		// Signo Valor 3597
		mapa.put(KEY_SIGNO_VAL, bean.getCampo(5));
		// Saldo Disponible 3521
		mapa.put(KEY_SALDO_DISPONIBLE, bean.getCampo(6));
		// Fecha Apertura Cuenta 3533
		mapa.put(KEY_FCH_APERTURA_CTA, bean.getCampo(7));
		// cod  Cliente / RIM Origen 3522
		mapa.put(KEY_CLIEN_INSTI, bean.getCampo(8));
		// cod Cliente / RIM Destino 3539
		mapa.put(KEY_COD_CTE, bean.getCampo(9));
		//  Sucursal Apertura Cuenta Origen 3537
		mapa.put(KEY_SUCURSAL_APERTURA_CO, bean.getCampo(10));
		// Sucursal Apertura Cuenta Destino 3596
		mapa.put(KEY_SUCURSAL_APERTURA_CD, bean.getCampo(11));
		// Nombre Cliente Titular Cta. Origen 3523
		mapa.put(KEY_NOM_USR_ORIGEN, bean.getCampo(12));
		// Nombre Cliente Titular Cta. Destino 3524
		mapa.put(KEY_NOMBRE_DESTINO, bean.getCampo(13));
		// Direccion Domicilio 3557
		mapa.put(KEY_DIRECCION, bean.getCampo(14) + bean.getCampo(15));
		// Telefono 1: Residencia 3558
		mapa.put(KEY_TELEFONO_RESIDENCIA, bean.getCampo(16));
		// Telefono 2: Oficina 3559
		mapa.put(KEY_TELEFONO_OFICINA, bean.getCampo(17));
		// Telefono 3: Otro 3560
		mapa.put(KEY_TELEFONO_OTRO, bean.getCampo(18));
		// Correo Electronico Email 3561
		mapa.put(KEY_MAIL, bean.getCampo(19));
		// Fecha Vinculacion Cliente 3535
		mapa.put(KEY_FECHA_VINCULACION_CLIENT, bean.getCampo(20));
		// Segmento del Cliente 3620
		mapa.put(KEY_SEGM_CLIENT, bean.getCampo(21));
		// Tipo Producto Destino 3571
		mapa.put(KEY_TIP_PROD_DEST, bean.getCampo(22));
		// Codigo Subproducto Destino 22
		mapa.put(KEY_COD_SUBPRODUCTO_DEST, bean.getCampo(23));
		// Numero Inicial Chequera 3591
		mapa.put(KEY_INI_NUM_CHEQUE, bean.getCampo(24));
		// Numero Fin Chequera 3592
		mapa.put(KEY_FIN_NUM_CHEQUE, bean.getCampo(25));
		// Fecha ultimo movimiento de la cuenta 3641
		mapa.put(KEY_FCH_ULT_MOV, bean.getCampo(26));
	}
    
    /**
     * Rellenar cadena con algun caracter
     * 
     * @param sonCeros indica si se rellena con ceros o espacios en blanco
     * @param longitud de cadena
     * @param valor del dato
     * @return cadena
     */
    public static String rellenaValores(boolean sonCeros, int longitud, String valor) {
        try {
            return sonCeros 
                    ? String.format("%0" + (longitud) + "d%s", 0, valor) 
                    : String.format("%1$-" + (longitud) + "s", valor);
        } catch (IllegalFormatException ie) {
            EIGlobal.mensajePorTrace("rellenaValores IllegalFormatException|" + ie.getMessage(), EIGlobal.NivelLog.DEBUG);
            return EnlaceMonitorPlusConstants.CADENA_VACIA;
        }
    }
    
    /**
     * Metodo que carga valores default para operaciones de Altas   
     * @param req Objeto con informacion inherente a la peticion
     * @param sucursalOperante Indica la clave de la sucursal operante enlace
     * @param ctaOrigen cuenta origen
     * @param session objeto con informacion de la session
     * @return El mapa con los valores default
     */
    public static Map<Integer, String> cargaValoresDefaultAltas(HttpServletRequest req,
            String sucursalOperante, String ctaOrigen, BaseResource session) {
        EIGlobal.mensajePorTrace("Inicio cargaValoresDefaultAltas MP ctaOrigen |"
                + ctaOrigen, EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = cargaValoresDefault(req, sucursalOperante, session);
        
        // Constante 00310 8
        mapa.put(KEY_CODIGO_EVENTO, CODIGO_EVENTO_310);
        // Codigo de Transaccion 3501
        mapa.put(EnlaceMonitorPlusConstants.KEY_TRANS, EnlaceMonitorPlusConstants.COD_TRAN_ALCT);
        // Canal o Medio donde se origino la transaccion 3503
        mapa.put(EnlaceMonitorPlusConstants.KEY_CANAL_TRANS, EnlaceMonitorPlusConstants.CANAL_INTERNET);
        // aplicacion 21
        mapa.put(EnlaceMonitorPlusConstants.KEY_APLICACION, EnlaceMonitorPlusConstants.APLICACION_ENLA);
        Date fecha = new Date();
        // Fecha de la Realizacion de la Transaccion en formato AAAAMMDD 3509
        mapa.put(EnlaceMonitorPlusConstants.KEY_FECHA, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_FECH, EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Hora de la Realizacion de la Transaccion en formato HHMMSS 3510
        mapa.put(EnlaceMonitorPlusConstants.KEY_HR, new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_HORA, EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Direccion IP caller 3556
        mapa.put(EnlaceMonitorPlusConstants.KEY_DICC_IP, (req.getHeader(EnlaceMonitorPlusConstants.REMOTE_ADDR) != null && !EnlaceMonitorPlusConstants.CADENA_VACIA.equals(req.getHeader(EnlaceMonitorPlusConstants.REMOTE_ADDR).trim()) ? req
            .getHeader(EnlaceMonitorPlusConstants.REMOTE_ADDR) : req.getRemoteAddr()));
        
        // Tipo persona 3532
        mapa.put(KEY_TIPO_PERSONA, TIPO_PERS);
        Map<Integer, String> datoCta = CuentasDAO.consultarDatosMP(session.getContractNumber());
        String ctaEje = (datoCta != null && datoCta.get(1)!= null ) ? datoCta.get(1).trim(): CADENA_VACIA;
        // Codigo cliente RIM Origen 3522
        mapa.put(EnlaceMonitorPlusConstants.KEY_CLIEN_INSTI, CuentasDAO.consultarIdUsuarioContrato(session.getContractNumber()));


         // Obtiene campos de la trx 390
        TrxLZADBO trx390 = new TrxLZADBO();
        
        if(ctaEje != EnlaceMonitorPlusConstants.CADENA_VACIA 
        		&& ctaEje != null){
        	boolean valida = false;
        	if(ConectorMP.isInit()){
        		valida = trx390.validaTrxLZAD(
        				rellenaValores(false, 5, CODIGO_EVENTO_310.replaceFirst("^0*", "")), 
        				((ctaEje.length()==11) ? rellenaValores(false, 20, ctaEje): ctaEje), CADENA_VACIA);
                if(valida && !COD_ERROR.equals(trx390.getBean().getCodError())){
                    EIGlobal.mensajePorTrace("cargaValoresDefaultAltas, obtiene trx390 y  mapea campos ", EIGlobal.NivelLog.DEBUG);
                    TrxLZADBean bean = trx390.getBean();
                    // ID- Cliente/RFC 3507
                    mapa.put(KEY_ID_CLIENTE, bean.getCampo(0));
                    // Fecha Apertura Cuenta 3533
                    mapa.put(KEY_FCH_APERTURA_CTA, bean.getCampo(12));
                    // Codigo de Producto 3502
                    mapa.put(KEY_CODIGO_PRODUCTO, bean.getCampo(2));
                    // Codigo de Subproducto 3566
                    mapa.put(KEY_CODIGO_SUBPRODUCTO, bean.getCampo(3));
                    //  Nombre Cliente Titular Cta Origen 3523
                    mapa.put(KEY_NOM_USR_ORIGEN, bean.getCampo(4));
                    //Direccion Domicilio 3557
                    mapa.put(KEY_DIRECCION, bean.getCampo(5) + bean.getCampo(6) );
                    // Telefono 1: Residencia 3558
                    mapa.put(KEY_TELEFONO_RESIDENCIA, bean.getCampo(7));
                    // Telefono 2: Oficina 3559
                    mapa.put(KEY_TELEFONO_OFICINA, bean.getCampo(8));
                    // Telefono 3: Otro 3560
                    mapa.put(KEY_TELEFONO_OTRO, bean.getCampo(9));
                    // Correo Electronico Email 3561
                    mapa.put(KEY_MAIL, bean.getCampo(10));
                    // Segmento del Cliente 3620
                    mapa.put(KEY_SEGM_CLIENT, bean.getCampo(11));
                    // Fecha Nacimiento / Constitucion 3626
                    mapa.put(KEY_FDN_CONSTITUCION, bean.getCampo(12));
                    // Lugar de Nacimiento / Constitucion 3627
                    mapa.put(KEY_LDN_CONSTITUCION, bean.getCampo(13));
                    // Sexo del Cuentahabiente 3628
                    mapa.put(KEY_SEXO_CTAHABIENTE, bean.getCampo(14));
                    // Estado Civil del Cuentahabiente 3629
                    mapa.put(KEY_EDO_CIVIL_CTAHABIENTE, bean.getCampo(15));
                    // Actividad Economica 3630
                    mapa.put(KEY_ACT_ECONOMICA, bean.getCampo(16));
         
                }else{
                    EIGlobal.mensajePorTrace("cargaValoresDefaultAltas, no se obtuvo datos de trx390, por tanto no se informan a MP", EIGlobal.NivelLog.DEBUG);
                    mapa.put(90000, "TRXERROR");
                    }
        	}else{
        		EIGlobal.mensajePorTrace("cargaValoresDefaultAltas el conector no esta inicializado no se invoca la TRX390", EIGlobal.NivelLog.DEBUG);
        	}
       
    }else{
        EIGlobal.mensajePorTrace(" Archivo no requiere trx390", EIGlobal.NivelLog.DEBUG);
        
    }

        // cargaValoresDefaultAltas: 8, 3501, 3503, 21, 3509, 3510, 3556, 3758, 3759, 3764, 3765, 3767, 3522, 3532
        // Nva trx: 3507, 3533, 3502, 3566, 3523, 3557, 3558, 3559, 3560, 3561, 3620 3626, 3627,3628, 3629, 3630
        // ceros/blancos: 3554, 13, 14
        EIGlobal.mensajePorTrace("Fin cargaValoresDefaultAltas MP", EIGlobal.NivelLog.DEBUG);
        return mapa;
        
    }
    
    /**
     * Metodo que carga valores por default entre las operaciones 
     * Inclusion de usuario en contrato y
     * Actualizacion de datos de contrato o usuario
     * @param req Objeto con informacion inherente a la peticion
     * @param sucursalOperante Indica la sucursal operante enlaca
     * @param session Objeto de sesion
     * @return Mapa con los valores default
     */
    public static Map<Integer, String> cargaValDefaultUsContrato(HttpServletRequest req,
            String sucursalOperante, BaseResource session) {
        EIGlobal.mensajePorTrace("----- > " + "ENTRA cargaValDefaultUsContrato PARA MP ** ",
                EIGlobal.NivelLog.DEBUG);
        Map<Integer, String> mapa = cargaValoresDefault(req, sucursalOperante, session);
        Map<Integer, String> datoCta = CuentasDAO.consultarDatosMP(session.getContractNumber());
        String ctaEje = (datoCta != null && datoCta.get(1)!= null ) ? datoCta.get(1).trim(): CADENA_VACIA;
        // Constante 00318 8
        mapa.put(KEY_CODIGO_EVENTO, CODIGO_EVENTO_318);
        // Canal o Medio donde se origino la transaccion 3503
        mapa.put(KEY_CANAL_TRANS, CANAL_INTERNET);
        Date fecha = new Date();
        // Fecha de la Realizacion de la Transaccion en formato AAAAMMDD 3509
        mapa.put(EnlaceMonitorPlusConstants.KEY_FECHA, 
                new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_FECH, 
                        EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Hora de la Realizacion de la Transaccion en formato HHMMSS 3510
        mapa.put(EnlaceMonitorPlusConstants.KEY_HR, 
                new SimpleDateFormat(EnlaceMonitorPlusConstants.FORMATO_HORA, 
                        EnlaceMonitorPlusConstants.LOCALE_MX).format(fecha));
        // Direccion IP caller 3556
        mapa.put(KEY_DICC_IP, (req.getHeader(REMOTE_ADDR) != null 
                && !EnlaceMonitorPlusConstants.CADENA_VACIA.equals(req.getHeader(REMOTE_ADDR).trim()) ? req
            .getHeader(REMOTE_ADDR) : req.getRemoteAddr()));
        // Tipo Sucursal 3614
         mapa.put(EnlaceMonitorPlusConstants.KEY_COD_TIP_SUCURSAL,
                 EnlaceMonitorPlusConstants.TIPO_SUCURSAL);
        // tipo usuario 3587
        mapa.put(EnlaceMonitorPlusConstants.KEY_TIPO_USUARIO, 
                (datoCta != null && datoCta.get(2) != null
                && datoCta.get(2).equals(((BaseResource) req.getSession().getAttribute ("session")).getUserID8()))
                ? EnlaceMonitorPlusConstants.SUPER_USR : EnlaceMonitorPlusConstants.USR_NORMAL );
         // cuenta origen 
         mapa.put(KEY_CTA_ORI, ctaEje);
         // Obtiene campos de la trx 390
        TrxLZADBO trx390 = new TrxLZADBO();
        if(ctaEje != EnlaceMonitorPlusConstants.CADENA_VACIA && ctaEje != null){
        	boolean valida = false;
        	if(ConectorMP.isInit()){
        		valida = trx390.validaTrxLZAD(rellenaValores(false, 5,CODIGO_EVENTO_318.replaceFirst("^0*", "")), 
        				((ctaEje.length()==11) ? rellenaValores(false, 20, ctaEje): ctaEje), CADENA_VACIA);
        		
                if(valida && !COD_ERROR.equals(trx390.getBean().getCodError()) ){
                    EIGlobal.mensajePorTrace("cargaValDefaultUsContrato, obtiene trx390 y  mapea campos ", EIGlobal.NivelLog.DEBUG);
                    TrxLZADBean bean = trx390.getBean();
                    // Cod Cliente / RIM Origen 3522
                    mapa.put(KEY_CLIEN_INSTI, bean.getCampo(0));
                    // Nombre Cliente Titular Cta Origen 3523
                    mapa.put(KEY_NOM_USR_ORIGEN, bean.getCampo(1));
                    // Codigo de Producto 3502
                    mapa.put(KEY_CODIGO_PRODUCTO, bean.getCampo(2));
                    // Codigo de Subproducto 3566
                    mapa.put(KEY_CODIGO_SUBPRODUCTO, bean.getCampo(3));
                    // No Cheque Inicial 3591
                    mapa.put(KEY_INI_NUM_CHEQUE, bean.getCampo(4));
                    // No Cheque Final 3592
                    mapa.put(KEY_FIN_NUM_CHEQUE, bean.getCampo(5));
                    // No Cheque Final 3605
                    mapa.put(KEY_TIPO_CHEQUERA, bean.getCampo(6));
                    // Estatus Cta o Producto 3517
                    mapa.put(KEY_EST_CTA_PROD, bean.getCampo(7));
                    // Signo Valor 3597
                    mapa.put(KEY_SIGNO_VAL, bean.getCampo(8));
                    // Saldo Disponible 3521
                    mapa.put(KEY_SALDO_DISPONIBLE, bean.getCampo(9));
                    // Sucursal Apertura Cuenta 3537
                    mapa.put(KEY_SUCURSAL_APERTURA_CO, bean.getCampo(10));
                    // ID- Cliente/RFC 3507
                    mapa.put(KEY_ID_CLIENTE, bean.getCampo(11));
                    // Fecha Apertura Cuenta 3533    
                    mapa.put(KEY_FCH_APERTURA_CTA, bean.getCampo(12));
                }else{
                    EIGlobal.mensajePorTrace("cargaValDefaultUsContrato, no se obtuvo datos de trx390, por tanto no se informan a MP", EIGlobal.NivelLog.DEBUG);
                    mapa.put(90000, "TRXERROR");
                }
        		
        	}else{
        		EIGlobal.mensajePorTrace("cargaValDefaultUsContrato, el conector no esta inicializado no se invoca la TRX390", EIGlobal.NivelLog.DEBUG);
        	}
        		
        }else{
            EIGlobal.mensajePorTrace(" Archivo no requiere trx390", EIGlobal.NivelLog.DEBUG);
            
        }
         // Nva trx: 3522,3523,3502,3566,3591,3592,3605,3517,3597,3521,3537,3507,3533
         // ceros/blancos: 3540,3534,3529,3530
        EIGlobal.mensajePorTrace("Fin cargaValDefaultUsContrato MP", EIGlobal.NivelLog.DEBUG);
        return mapa;
    }
}