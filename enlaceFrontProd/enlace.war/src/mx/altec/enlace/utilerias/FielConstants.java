/** 
*   Isban Mexico
*   Clase: FielConstants.java
*   Descripcion: Clase de constantes para modulo de SPID_FIEL.
*
*   Control de Cambios:
*   1.0 22/06/2016  FSW. Everis  
*/

package mx.altec.enlace.utilerias;

/**
 * Clase de constantes para modulo de SPID_FIEL
 * @author FSW Everis
 *
 */
public class FielConstants {
    
    /** Cadena para infdicar operacion exitosa. */
    public static final String COD_ERRROR_OK = "OK";
    
    /** Cadena para infdicar operacion no exitosa. */
    public static final String COD_ERRROR_KO = "KO";
    
    /** Constante CADENA_VACIA*/
    public static final String CADENA_VACIA = "";
    
    /** Constante PARAM_MODULO */
    public static final String PARAM_MODULO = "modulo";
    
    /** Constante INICIA_REGISTRO*/
    public static final String INICIA_REGISTRO = "0";
    
    /*
     Modulo JSP
    */
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_REGISTRO_INIT = "/jsp/fielRegInicio.jsp";
    
    /** Nombre de la pagina de validacion del registro de fiel. */
    public static final String JSP_FIEL_CONFIRMA = "/jsp/fielConfirma.jsp";
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_RESULTADO = "/jsp/fielResult.jsp";
    
    /** Constante para almacenar la ubicacion de jsp comprobante. */
    public static final String JSP_FIEL_COMPROBANTE = "/jsp/fielComprobante.jsp";
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_REGISTRO_COMPROBANTE = "/jsp/fielRegComprobante.jsp";
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_BAJA_INIT = "/jsp/fielBajaInicio.jsp";
    
    /** Nombre de la pagina de validacion del registro de fiel. */
    public static final String JSP_FIEL_BAJA_CONFIRMA = "/jsp/fielBajaConfirma.jsp";
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_BAJA_RESULTADO = "/jsp/fielBajaResultado.jsp";
    
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_BAJA_COMPROBANTE = "/jsp/fielBajaComprobante.jsp";
    
    // MODULO DE MODIFICACION
    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_MOD_INIT = "/jsp/fielModInicio.jsp";

    /** Nombre de la pagina de validacion del registro de fiel. */
    public static final String JSP_FIEL_MOD_CONFIRMA = "/jsp/fielModConfirma.jsp";

    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_MOD_RESULTADO = "/jsp/fielModResult.jsp";

    /** Nombre de la pagina inicial de registro de fiel. */
    public static final String JSP_FIEL_MOD_COMPROBANTE = "/jsp/fielModComprobante.jsp";

    /** Constante para indicar que el tipo de documento es Electronico */
    public static final String TIPO_DOCUMENTO = "CE";
    
    /**  Constante para indicar que los documentos a consultar seran solo vigentes */
    public static final String DOC_CONSULTAR = "V";
    
    /**  Constante para utilizar una cadena vacia */
    public static final String EMPTY_STRING = "";
    
    /** Constante para utilizar un caracter en blanco */
    public static final char BLANK_CHAR = ' ';
    
    /** Constante para utilizar un cero */
    public static final char ZERO_CHAR = '0';
    
    /** Constante para definir la orientacion de los espacios en blanco a la derecha */
    public static final char RIGHT_OR = 'D';
    
    /** Constante para definir la orientacion de los espacios en blanco a la izquierda */
    public static final char LEFT_OR = 'I';
    
    /**  Constante para dar de alta un documento electronico */
    public static final String ALTA_DOCUMENTO = "A";

    /** Constante para modificar un documento electronico */
    public static final String MODF_DOCUMENTO = "M";
    
    /** Constante para dar de baja un documento electronico */
    public static final String BAJA_DOCUMENTO = "B";
    
    /** Constante de formato de fecha para validaciones **/
    public static final String FORMATO_FECHA_DOCTO = "yyyy-MM-dd";
    
    /** Constante de expresion regular para formato de fecha en validaciones **/
    public static final String REGEX_FECHA_DOCTO = "\\d{4}-\\d{2}-\\d{2}";
    
    /** Valor que identifica cuando la valdacion de RSA fue exitosa. */
    public static final String RSA_SUCCESS = "SUCCESS";
    
    /** Parametro que acarrea el valor de validacion de Challenge. */
    public static final String RSA_PARAM_CHALLENGE = "challngeExito";
    
    /** Bandera indicativa de que la funcionalidad de bitacoras esta activa. */
    public static final String BITA_ACTIVA = "ON";
    
    /** Complemento de la clave de bitacora indicativa de entrada. */
    public static final String BITA_OPER_ENTRA = "01";
    
    /** Complemento de la clave de bitacora indicativa de ejecucion de la operacion. */
    public static final String BITA_OPER_EJEC = "02";
    
    /** Clave para bitacora de la operacion de alta de FIEL. */
    public static final String BITA_FIEL_ALTA = "FIEA";
    
    /** Clave para la entrada al modulo de alta de FIEL. */
    public static final String BITA_FIEL_ALTA_ENTRA = BITA_FIEL_ALTA + BITA_OPER_ENTRA;
    
    /** Clave para la ejecucion de la operacion de alta de FIEL. */
    public static final String BITA_FIEL_ALTA_EJEC = BITA_FIEL_ALTA + BITA_OPER_EJEC;
    
    /** Clave para bitacora de la operacion de cambio de FIEL. */
    public static final String BITA_FIEL_MODIFICA = "FIEM";
    
    /** Clave para la entrada al modulo de cambio de FIEL. */
    public static final String BITA_FIEL_MODIFICA_ENTRA = BITA_FIEL_MODIFICA + BITA_OPER_ENTRA;
    
    /** Clave para la ejecucion de la operacion de baja de FIEL. */
    public static final String BITA_FIEL_MODIFICA_EJEC = BITA_FIEL_MODIFICA + BITA_OPER_EJEC;
    
    /** Clave para bitacora de la operacion de baja de FIEL. */
    public static final String BITA_FIEL_BAJA = "FIEB";
    
    /** Clave para la entrada al modulo de baja de FIEL. */
    public static final String BITA_FIEL_BAJA_ENTRA = BITA_FIEL_BAJA + BITA_OPER_ENTRA;
    
    /** Clave para la ejecucion de la operacion de baja de FIEL. */
    public static final String BITA_FIEL_BAJA_EJEC = BITA_FIEL_BAJA + BITA_OPER_EJEC;
    
    
    /* WS client */
    /**
     * Constante para almacenar en namespace el servicio web
     */
    public static final String NAMESPACE = "http://moduloSPID.ws.eTransferNal.isban.mx/";
    /** Constante que guarda el valor de la url del WS */
    public static final String WS_URL = Global.fielUrl;
    /**
     * Constante para almacena el QName del servicio
     */
    public static final String Q_SERVICE  = "ConsultaCertificadoService";
    /**
     * Constante para almacena el QName del Port
     */
    public static final String Q_PORT  = "WSConsultaCertificadoImplPort";
    
    /** Constante para almacenar un espacio */
    public static final String UN_ESPACIO = " ";
    
    /** Constante para almacenar signo igual */
    public static final String SIGNO_IGUAL = "=";
    
    /** Constante para almacenar signo doble coma */
    public static final String SIGNO_DOBLE_COMA = "\"";
    
    /** Constante para almacenar signo mayor que */
    public static final String SIGNO_MAYOR = ">";

    /** Constante para almacenar signo menos */
    public static final String SIGNO_MENOS = "-";
    
    /** Constante para almacenar la cadena style */
    public static final String STYLE = "style";
    
    /** constante para almacenar la cadena class*/
    public static final String STR_CLASS = "class";
    
    /** constante para almacenar la cadena textabdatobs*/
    public static final String STR_TEXTABDATOBS = "textabdatobs";
    
    /** constante para almacenar numero magico tr*/
    public static final int NUMERO_MAGICO_TR = 3;
    
    /** constante para almacenar umero magico td */
    public static final int NUMERO_MAGICO_TD = 4;
    
    /** constante para almacenar la cadena datosFiel*/
    public static final String DATOS_FIEL = "datosFiel";
    
    /** constante para almacenar la cadena class*/
    public static final String STR_TITTABDT = "tittabdat";
    
    /** constante detalle fiel*/
    public static final String PARAM_DETALLE_FIEL = "detalleFiel";
    
    /** constante detalle fiel*/
    public static final String PARAM_DETALLE_FIEL_TRX54 = "beanAdminFielTrxod54";
    
    /** Constante para imprimir un cero **/
    public static final String ZERO_STR = "0";
    
    /** Constante para almacenar la etiqueta del menu fiel. */
    public static final String ETIQUETA_NOMBRE_MODULO_FIEL = "moduloFiel";
    /** Constante para almacenar el valor del menu baja fiel. */
    public static final String VALOR_NOMBRE_MODULO_BAJA_FIEL = "Baja de FIEL";
    /** Constante para almacenar la etiqueta msgError . */
    public static final String ETIQUETA_MSG_ERROR = "msgError";
    
    
    /** Constante PARAM_FIEL_RESULT */
    public static final String PARAM_FIEL_RESULT = "fielResult";
    /** Constante PARAM_PARAMETROS_BITACORA */
    public static final String PARAM_PARAMETROS_BITACORA = "parametrosBitacora";
    /** Constante PARAM_ATRIBUTOS_BITACORA */
    public static final String PARAM_ATRIBUTOS_BITACORA = "atributosBitacora";
    /** Constante PARAM_MENSAJE_SESSION */
    public static final String PARAM_MENSAJE_SESSION = "mensajeSession";
    /** Constante PARAM_BITACORA */
    public static final String PARAM_BITACORA = "bitacora";
    

    /* Mensajes de error */
    /** Constante para almacenar el mensaje de error al dar de baja una FIEL */
    public static final String MSG_ERROR_001 = "Ocurri\u00F3 un error al dar de baja la FIEL.";
    /** Constante para almacenar el mensaje de error para consultar una FIEL */
    public static final String MSG_ERROR_002 = "Ocurri\u00F3 un error al consultar la FIEL.";
    /** Constante para almacenar el mensaje 001 de baja de fiel */
    public static final String MSG_BAJA_001 = "Desea dar de baja la FIEL asociada a la cuenta ";
    /** Constante para almacenar el mensaje 002 de baja de fiel */
    public static final String MSG_BAJA_002 = "No se encuentra una FIEL asociada a la cuenta.";
    
    
    /** Constante para almacenar la mayor que */
    public static final String MAYOR_QUE_TITULO = " &gt; ";
    /** Constante para almacenar un pipe char */
    public static final char PIPE_CHAR = '|';
    /** Constante para almacenar el numero 1 */
    public static final String UNO_STR = "1";
    /** Constante para almacenar el parametro comboCuenta */
    public static final String PARAM_CBO_CTA = "comboCuenta";
    /** Constante para almacenar el parametro urlCancelar */
    public static final String URL_CANCELAR = "urlCancelar";
    /** Constante para almacenar el contexto enlace */
    public static final String CONTEXTO_ENLACE = "/Enlace/";
    /** Constante para almacenar signo de pregunta que abre */
    public static final String PREGUNTA_ABRE = "&#191;";
    /** Constante para almacenar signo de pregunta que cierra */
    public static final String PREGUNTA_CIERRA = "&#63;";
    /** Constante para almacenar el signo igual*/
    public static final String IGUAL = "=";
    /** Constante para almacenar una barra */
    public static final String BARRA = "|";

    /** Constante para almacenar el numero 0 */
    public static final int CERO_INT = 0;
    /** Constante para almacenar el numero 1 */
    public static final int UNO_INT = 1;
    /** Constante para almacenar el numero  */
    public static final int DOS_INT = 2;
    /** Constante para almacenar el numero 3 */
    public static final int TRES_INT = 3;
    /** Constante para almacenar el numero 4 */
    public static final int CUATRO_INT = 4;
    /** Constante para almacenar el numero 5 */
    public static final int CINCO_INT = 5;
    /** Constante para almacenar el numero 6 */
    public static final int SEIS_INT = 6;
    /** Constante para almacenar el numero 7 */
    public static final int SIETE_INT = 7;
    /** Constante para almacenar el numero 8 */
    public static final int OCHO_INT = 8;
    /** Constante para almacenar el numero 9 */
    public static final int NUEVE_INT = 9;
    /** Constante para almacenar el numero 10 */
    public static final int DIEZ_INT = 10;
    /** Constante para almacenar el numero 11 */
    public static final int ONCE_INT = 11;
    
    /** Constante para almacenar el char 1 */
    public static final char UNO_CHAR = '1';
    /** Constante para almacenar el char 2 */
    public static final char DOS_CHAR = '2';
    /** Constante para almacenar el char 3 */
    public static final char TRES_CHAR = '3';
    /** Constante para almacenar el char 4 */
    public static final char CUATRO_CHAR = '4';
    
    /* Acciones */
    /** Constante para almacenar la accion confirmar */
    public static final String ACCION_CONFIRMAR = MAYOR_QUE_TITULO.concat("Confirmaci\u00F3n");
    /** Constante para almacenar la accion resultado */
    public static final String ACCION_RESULTADO = MAYOR_QUE_TITULO.concat("Resultado");
    
    /* Ayuda */
    /** Constante para almacenar la ayuda de inicio del modulo baja */
    public static final String AYUDA_FIEL_BAJA = "s60070h";
    /** Constante para almacenar la ayuda de resultado del modulo modificacion */
    public static final String AYUDA_FIEL_MODIF = "s60050h";
    /** Constante para almacenar la ayuda de resultado del modulo registro */
    public static final String AYUDA_FIEL_REG = "s60060h";
    
    
    /** Constante para almacenar el estatus de certificado valido **/
    public static final String ESTATUS_CERT_VALIDO = "v\u00E1lido";
}
