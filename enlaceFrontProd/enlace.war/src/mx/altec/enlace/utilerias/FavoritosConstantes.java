package mx.altec.enlace.utilerias;

/**
 * Contiene todas las constantes para los Favoritos en los modulos de ENLACE.
 * 
 * @author
 * 
 */
public class FavoritosConstantes {
	/**
	 * ID que indica que redireccionara la peticion.
	 */
	public static final String ID_REDIR = "R";
	/**
	 * ID que identifica que el favorito es por archivo.
	 */
	public static final String ID_ES_ARCHIVO = "ARCH";
	/**
	 * ID que identifica que el favorito no es por archivo.
	 */
	public static final String ID_NO_ES_ARCHIVO = "NO_ARCH";
	/**
	 * ID del nombre del parametro que identifica que es por archivo.
	 */
	public static final String ID_PARAM_ES_ARCH = "paramArch";
	/**
	 * ID que indica que realizara una consulta.
	 */
	public static final String ID_CONSULTA = "C";
	/**
	 * ID que indica que avanzara una pagina.
	 */
	public static final String ID_PAGINADO = "P";
	/**
	 * ID que indica que se realizara una modificacion.
	 */
	public static final String ID_MODIFICAR = "M";
	/**
	 * ID que indica que se va a eliminar un favorito.
	 */
	public static final String ID_BORRAR = "B";
	/**
	 * ID para identificar en los atributos de sesion que es la lista de
	 * favoritos encontrados en una consulta.
	 */
	public static final String ID_LISTA_FAVORITOS = "favoritos";
	/**
	 * ID para identificar en los atributos de la peticion los favoritos a
	 * mostrar en la pagina actual.
	 */
	public static final String ID_FAVORITOS_PAGINA = "favoritosPagina";
	/**
	 * ID para identificar en los atributos de sesion que es el favorito
	 * seleccionado y que se va a cargar en la pantalla.
	 */
	public static final String ID_FAVORITO = "favorito";
	/**
	 * ID para identificar en los parametros de la peticion que es el numero que
	 * le corresponde a un favorito seleccionado en la lista de favoritos.
	 */
	public static final String ID_NUM_FAVORITO_SEL = "favoritoSel";
	/**
	 * ID para identificar en los parametros de la peticion que la peticion es
	 * recibida desde el serlet de favoritos.
	 */
	public static final String ID_PARAM_FAV = "deFavorito";
	/**
	 * ID para identificar el modulo desde donde se realizo la peticion.
	 */
	public static final String ID_MODULO = "modulo";
	/**
	 * ID para identificar el parametro que contiene el id del modulo de la
	 * consulta.
	 */
	public static final String ID_MODULO_PARAM = "moduloParam";
	/**
	 * ID para identificar cual es servlet al que se tendra que redireccionar
	 * cuando se debe de cargar la informacion de un favorito.
	 */
	public static final String ID_SERVLET_ORIGEN = "servletOrigen";
	/**
	 * ID para identificar que es el valor de la operacion a realiza.
	 */
	public static final String ID_OPERACION = "operacion";
	/**
	 * ID para identificar el indice incial para el paginado.
	 */
	public static final String ID_INICIO_PAGINA = "inicio";
	/**
	 * ID para identificar el indice final para el paginado.
	 */
	public static final String ID_FIN_PAGINA = "fin";
	/**
	 * Id para identificar el numero de pagina actual.
	 */
	public static final String ID_NUM_PAGINA = "pagina";
	/**
	 * ID para identificar el parametro que indica que es la pagina final.
	 */
	public static final String ID_ES_PAGINA_FINAL = "esPaginaFinal";
	/**
	 * ID para identificar el parametro que indica que la consulta es paginada.
	 */
	public static final String ID_ES_CONSULTA_PAGINADA = "esConsultaPaginada";
	/**
	 * ID para identificar el parametro que indica el numero de paginas.
	 */
	public static final String ID_NUMERO_PAGINAS = "numeroPaginas";
	/**
	 * Numero maximo de favoritos por pagina.
	 */
	public static final int MAX_FAVORITOS = 50;
	/**
	 * Indica que la consulta es paginada.
	 */
	public static final String SI_ES_PAGINADA = "siPaginado";
	/**
	 * Indica que no es pagina final.
	 */
	public static final String NO_ES_PAG_FINAL = "noEsFinal";
	/**
	 * Indica que no es una consulta paginada
	 */
	public static final String NO_ES_PAGINADA = "noPaginado";
	/**
	 * Indica que si es pagina final.
	 */
	public static final String SI_ES_PAG_FINAL = "siEsFinal";
	/**
	 * ID que indica el numero de favoritos.
	 */
	public static final String ID_NUM_FAVORITOS = "numFavoritos";
	/**
	 * Identifica que la peticion es desde favoritos.
	 */
	public static final String ES_PETICION_DE_FAVORITO = "1";
	/**
	 * Titulo de favoritos.
	 */
	public static final String TITULO_FAVORITOS = "Favoritos";
	/**
	 * Valor para indicar que la cuenta es registrada.
	 */
	public static final int ES_CUENTA_REGISTRADA = 2;
	/**
	 * Valor para indicar que la cuenta no registrada.
	 */
	public static final int ES_CUENTA_NO_REGISTRADA = 3;
	/**
	 * Id para identifcar que registros fueron seleccionados como favorito.
	 */
	public static final String ID_FAVORITOS_SEL = "favSel";
	/**
	 * Id para identificar el EI_Tipo con la lista de descripciones para los
	 * favoritos.
	 */
	public static final String ID_LISTA_DESCRIPCIONES = "listaFavoritos";
	/**
	 * Constante que identifica que la operacion es por SPEI.
	 */
	public static final String ID_SPEI_TI = "SPEI";

	/**
	 * Constante que identifica que la operacion es por TEF.
	 */
	public static final String ID_TEF_TI = "TEF";
	/**
	 * Utilizado para la lista de favoritos temporales.
	 */
	public static final String ID_LISTA_FAV_TMP = "listaFavTmp";
	/**
	 * Id para determinar que contiene la nueva descripcion de favorito.
	 */
	public static final String ID_NVO_DESC_FAV = "nvoDescFavorito";

}