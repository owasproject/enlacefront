package mx.altec.enlace.utilerias;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class SuaUtil {
	static int cPunto, cPesos;
	static boolean debug=false;

	public static void main(String[] args) {
//		switch (ValidaFormato (" LLL888888AAA", " SEB890808G42")) {
//		case 0:
//			System.out.println("Validado correctamente");
//			break;
//		case 1:
//			System.out.println("Error Caracter Alfanumerico con caracateres");
//			break;
//		case 2:
//			System.out.println("Error Caracter Alfanumerico");
//			break;
//		case 3:
//			System.out.println("Error Caracter Alfabetico");
//			break;
//		case 4:
//			System.out.println("Error Caracter Numero");
//			break;
//		case 5:
//			System.out.println("Error Caracter Numero con punto");
//			break;
//		case 9:
//			System.out.println("Error Tamanno");
//			break;
//		}
		//SuaUtil.val
		//System.out.println("Salida: "+ValidaFechaAAAAMMDD("20080718"));
		//escribeBitacora("Si fucniona");
		//validaNumericoVariable("1234");
//		System.out.println("Resultado: "+ValidaNombreEmpleado("REYparangaricutirimícuaroES$ZAMORATEparangaricutirimícuaroGUI$ANDRESparangaricutirimícuaro"));
	}

	/**
	 * @param Formato
	 * 		T (Todo) 				Alfanumericos y simbolos validos.
	 * 		A (Alfanumerico)		Alfanumericos
	 * 		L (Letra)				Alfabeticos
	 * 		7 (Numero ó Espacio)	Numeros o espacios en blanco
	 * 		8 (Numero sin punto)	Numero sin punto
	 * 		9 (Numero con punto)	Numeros con punto
	 * 		' ' (Espacio)			Espacio en blanco
	 * @param Cadena
	 * @return 0 Ok, 1 Error T, 2 Error A, 3 Error L, 4 Error 8, 5 Error 9, 6 Error Fecha de RFC, 9 Error Tamanno
	 *
	 * Ejemplo: ValidaFormato ("AAAA9999AAAA", "ABCD1234EFGH");
	 * Ejemplo: ValidaFormato ("LLLL888888AAA", "GICI810926TQ8");
	 * Ejemplo: ValidaFormato (" LLL888888AAA", "ABCD1234EFGH");
	 */
	public static int ValidaFormato(String Formato, String Cadena) {
		int iValida = 0;
		char caracterF=' ';
		char caracterC=' ';

		if (Formato.length() != Cadena.length()) return (9);	//ERROR de tamanno

		Formato = Formato.toUpperCase();
		Cadena = Cadena.toUpperCase();

		for(int i=0;i < Formato.length();i++) {
			caracterF=Formato.charAt(i);
			caracterC=Cadena.charAt(i);
			iValida=0;

			switch (caracterF) {
			case 'E':  		// Verificación de modo Alfanumerico y caracteres validos sin espacio
				iValida = 1;
				if ( validaCarValidoSinEspacio(caracterC) ) iValida=0;
				if ( validaLetra(caracterC) ) iValida=0;
				if ( validaNumerico(caracterC,false) ) iValida=0;
				if ( iValida == 1) return (1);
				break;
			case 'T':		// Verificación de modo Alfanumerico y caracteres validos
				iValida = 1;
				if ( validaCaracterValido(caracterC) ) iValida=0;
				if ( validaLetra(caracterC) ) iValida=0;
				if ( validaNumerico(caracterC,false) ) iValida=0;
				if ( iValida == 1) return (1);
				break;
			case 'A':		// Verificacion Alfanumerica
				iValida = 1;
				if ( validaLetra(caracterC) ) iValida=0;
				if ( validaNumerico(caracterC, false) ) iValida=0;
				if ( iValida == 1) return (2);
				break;
			case 'L':		// Verificacion de Letra
				iValida = 1;
				if ( validaLetra(caracterC) ) iValida=0;
				if ( iValida == 1) return (3);
				break;
			case '8':		// Verificación en modo numerico sin punto
				iValida = 1;
				if ( validaNumerico(caracterC,false) )iValida=0;
				if ( iValida == 1) return (4);
				break;
			case '9':		// Verificación en modo numerico con punto
				iValida = 1;
				cPunto = 0;
				if ( validaNumerico(caracterC, true) )iValida=0;
				if ( iValida == 1) return (5);
				break;
			case ' ':
				if (caracterC != ' ') return (3);
			}
			if (debug) System.out.println("Comparando un "+caracterF+" con "+caracterC+" Resultado "+iValida);
		}
		return (0);
	}
	/**
	 * validaRFCMoral
	 * @param Cadena
	 * @return
	 */
	public static int validaRFCMoral(String Cadena) {
		int iRespuesta = 0;
		iRespuesta =  (ValidaFormato (" LLL888888TTT", Cadena));
//		if (iRespuesta == 0){
//			if (ValidaFechaAAMMDD(Cadena.substring(4,10))){
//				iRespuesta = 0;
//			} else {
//				iRespuesta = 6;
//			}
//		}
		return iRespuesta;
	}
	/**
	 * validaRFCFisica
	 * @param Cadena
	 * @return
	 */
	public static int validaRFCFisica(String Cadena) {
		//return (ValidaFormato ("LLLL888888AAA", Cadena));
		int iRespuesta = 0;
		iRespuesta =  (ValidaFormato ("LLLL888888TTT", Cadena));
//		if (iRespuesta == 0){
//			if (ValidaFechaAAMMDD(Cadena.substring(4,10))){
//				iRespuesta = 0;
//			} else {
//				iRespuesta = 6;
//			}
//		}
		return iRespuesta;
	}

	/**
	 * validaRFC
	 * @param Cadena
	 * @return
	 */
	public static int validaRFC(String Cadena) {
		int iRespuesta = 0;
		if (Cadena.charAt(0) == ' '){
			//return (ValidaFormato (" LLL888888AAA", Cadena));
			iRespuesta =  (ValidaFormato (" LLL888888TTT", Cadena));
//			if (iRespuesta == 0){
//				if (ValidaFechaAAMMDD(Cadena.substring(4,10))){
//					iRespuesta = 0;
//				} else {
//					iRespuesta = 6;
//				}
//			}
			return iRespuesta;
		}
		else {
			//return (ValidaFormato ("LLLL888888AAA", Cadena));
			iRespuesta =  (ValidaFormato ("LLLL888888", Cadena.substring(0,10)));
			if (iRespuesta == 0) {
				iRespuesta =  (ValidaFormato ("TTT", Cadena.substring(10,13)));
				if (iRespuesta != 0) iRespuesta =  (ValidaFormato ("   ", Cadena.substring(10,13)));
			}

//			if (iRespuesta == 0){
//				if (ValidaFechaAAMMDD(Cadena.substring(4,10))){
//					iRespuesta = 0;
//				} else {
//					iRespuesta = 6;
//				}
//			}
			return iRespuesta;
		}
	}
	public static boolean validaCaracterValido(char c) {
			//Validando Caracteres validos
			switch (c) {
			case ' ':
			case '¡':
			case '\"':
			case '#':
			case '$':
			case '%':
			case '&':
			case '*':
			case '.':
			case '/':
			case ':':
			case ';':
			case '<':
			case '=':
			case '>':
			case '@':
			case '[':
			case ']':
			case '\\':
			case '\'':
			case '{':
			case '}':
				return (true);
			default:
				return(false);
			}
	}
	public static boolean validaNumerico(char c, boolean punto) {
		if (punto) {
			if (c == '.') {
				cPunto++;
				if (cPunto > 1) return (false);
				return (true);
			}
		}
		if (c >= '0' && c <= '9') return (true);
		return (false);
	}
	public static boolean validaLetra(char c) {
		if (c >= 'A' && c <= 'Z' || c == '/') {
			return (true);
		}
		return (false);
	}

	public static boolean ValidaFechaAAAAMMDD(String Cadena) {
		String Sanio, Smes, Sdia;
		Integer anio, mes, dia;

		if (Cadena.trim().length() == 0) return (true);

		anio=Integer.valueOf(Sanio=Cadena.substring(0, 4));
		mes=Integer.valueOf(Smes=Cadena.substring(4, 6));
		dia=Integer.valueOf(Sdia=Cadena.substring(6, 8));

		if ( SuaUtil.ValidaFormato("8888", Sanio) != 0) return (false); 	// Validando Año
		else if (SuaUtil.ValidaFormato("88", Smes) != 0) return (false);	// Validando Mes
		else if (SuaUtil.ValidaFormato("88", Sdia) != 0) return (false);	// Validando Mes

		// Verificación de rangos de fecha valida
		if (anio.intValue() < 1900 || anio.intValue() > 3000 ) return (false);
		if (mes.intValue() < 1 || mes.intValue() > 12 ) return (false);
		if (dia.intValue() < 1 || dia.intValue() > 31 ) return (false);

		return (true);
	}

	public static boolean ValidaFechaAAAAMM(String Cadena) {
		String Sanio, Smes;
		Integer anio, mes;

		anio=Integer.valueOf(Sanio=Cadena.substring(0, 4));
		mes=Integer.valueOf(Smes=Cadena.substring(4, 6));

		if ( SuaUtil.ValidaFormato("8888", Sanio) != 0) return (false); 	// Validando Año
		else if (SuaUtil.ValidaFormato("88", Smes) != 0) return (false);	// Validando Mes

		// Verificación de rangos de fecha valida
		if (anio.intValue() < 1900 || anio.intValue() > 3000 ) return (false);
		if (mes.intValue() < 1 || mes.intValue() > 12 ) return (false);

		return (true);
	}

	/**
	 *
	 * @param Nombre
	 * @return
	 * @Descripción: Sólo 2 simbolos de pesos, no estar juntos los $
	 * @Errores:
	 * 			0 Ok
	 * 			1 Mas o menos de 2 simbolos de $
	 * 			2 Están juntos los $
	 * 			3 Dos Espacios en blanco dentro del texto
	 * 			4 En caso de que la cadena sea mayor a 50
	 * 			5 Caracter inválido
	 */
	public static int ValidaNombreEmpleado(String Nombre) {
		int i=0, Max=0;
		char cAnterior, cActual;

		if (Nombre.length() > 50) return(4);	// Valida Longitud menor a 50
		Nombre=Nombre.trim();
		Max=Nombre.length();

		/* Inicia barrido para verificación */
		cPesos=0;
		cActual=' ';
		for (i=0;i < Max; i++) {
			cAnterior = cActual;
			cActual=Nombre.charAt(i);
			if (cActual == '$') {
				cPesos++;
				if (cActual == cAnterior) return(2);
			} else	if (cActual == ' ') {
				if (cActual == cAnterior) return (3);
			}
// Inicia modificacion AMR 14/08/2008  (Para que deje pasar caracteres en el nombre)
			//else if (!validaLetra(cActual))
			//return (5);
			else if (!validaLetra(cActual)){
				if (!validaCaracterValido(cActual)){ return (5); }
			}
//fin modificacion AMR
		}
		if (cPesos != 2)
			return(1);
		/* Fin barrido para verificación */

		return (0);
	}

	public static boolean ValidaFechaAAMMDD(String Cadena) {
		String Sanio, Smes, Sdia;
		Integer anio, mes, dia;

		anio=Integer.valueOf(Sanio=Cadena.substring(0, 2));
		mes=Integer.valueOf(Smes=Cadena.substring(2, 4));
		dia=Integer.valueOf(Sdia=Cadena.substring(4, 6));

		if ( SuaUtil.ValidaFormato("88", Sanio) != 0) return (false); 	// Validando Año
		else if (SuaUtil.ValidaFormato("88", Smes) != 0) return (false);	// Validando Mes
		else if (SuaUtil.ValidaFormato("88", Sdia) != 0) return (false);	// Validando Dia

		// Verificación de rangos de fecha valida
		//if (anio.intValue() <= 99 || anio.intValue() >= 0 ) return (false);
		if (mes.intValue() < 1 || mes.intValue() > 12 ) return (false);
		if (dia.intValue() < 1 || dia.intValue() > 31 ) return (false);

		return (true);
	}

	public static boolean validaNumericoEspacio (String Cadena) {
		boolean bSalida=true;
		char caracterC=' ';

		Cadena = Cadena.toUpperCase();

		caracterC = Cadena.charAt(0);
		if (caracterC == ' ') {
			for(int i=1;i < Cadena.length();i++) {
				caracterC=Cadena.charAt(i);
				if (caracterC != ' ') bSalida=false;
			}
		}
		else {
			for(int i=0;i < Cadena.length();i++) {
				caracterC=Cadena.charAt(i);
				if (! validaNumerico(caracterC,false)) { bSalida=false; break; }
			}
		}

		return (bSalida);
	}

	public static boolean validaNumericoVariable(String Cadena) {
		boolean bSalida=true;

		Cadena = Cadena.trim();
		try {
			Integer.valueOf(Cadena);
			bSalida=true;
		}
		catch (Exception e) {
			bSalida=false;
		}
		System.out.println(bSalida);
		return (bSalida);
	}

	public static boolean validaEspacios(String Cadena, int numMaximo) {
		if ( Cadena.length() != numMaximo )
			return (false);
		for (int i=0; i< numMaximo; i++)
			if (Cadena.charAt(i) != ' ')
				return (false);
		return true;
	}

	public static void escribeBitacora(String Bitacora) {
		Calendar hoy;
		hoy = new GregorianCalendar();
		System.out.println( hoy.get(Calendar.DAY_OF_MONTH) + "/" + (hoy.get(Calendar.MONTH)+1) + "/" + hoy.get(Calendar.YEAR) + " " + hoy.get(Calendar.HOUR) +  ":" + hoy.get(Calendar.MINUTE)+ " - " + Bitacora);
	}

	public static int Str2int (String Cantidad){
		return ( Integer.parseInt(Cantidad) );
	}

	public static int dbl2int (double Cantidad) {
		Double d=new Double(Redondear(Cantidad*100,2));
		return (d.intValue());
		//return ((Double.valueOf(Redondear(Cantidad,2)*100)).intValue());
	}
	public static long Str2long (String Cantidad) {
		return ( Long.parseLong(Cantidad)  );
	}
	public static double Redondear( double numero, int decimales ) {
		return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	}
	public static boolean comparaImportes( double srcImporte, double objImporte )
	{
		boolean estado;
		double diferencia;

		estado = true;
		diferencia = objImporte - srcImporte;
		diferencia = Math.abs(diferencia);

		if ( Redondear(diferencia, 2) < 0.01 )
			estado = false;
		return( estado );

		//return (comparaImportesInt(src));
	}

	public static boolean comparaImportesInt( int srcImporte, int objImporte )
	{
		boolean estado;
		double diferencia;

		estado = true;
		diferencia = objImporte - srcImporte;
		diferencia = Math.abs(diferencia);

		if ( diferencia < 1 )
			estado = false;
		return( estado );
	}

	/**
	 * Se valida caracteres especiales, menos el espacio
	 * @param c
	 * @return
	 */
	public static boolean validaCarValidoSinEspacio(char c) {
		//Validando Caracteres validos
		switch (c) {
		case '¡':
		case '\"':
		case '#':
		case '$':
		case '%':
		case '&':
		case '*':
		case '.':
		case '/':
		case ':':
		case ';':
		case '<':
		case '=':
		case '>':
		case '@':
		case '[':
		case ']':
		case '\\':
		case '\'':
		case '{':
		case '}':
			return (true);
		default:
			return(false);
		}
	}

	/**
	 * validaRFCTrabajador
	 * Metodo que valida el RFC de un trabajador, no se permite espacios
	 * @param cadena RFC del trabajador
	 * @return 0 validacion exitosa
	 *         en otro caso un numero que no es 0
	 */
	public static int validaRFCTrabajador (String cadena){
		int iRespuesta = 0;
		int iTam = cadena.length();
	    if (iTam == 10 || iTam > 10){
			iRespuesta = ValidaFormato("TTTTTTTTTT", cadena.substring(0,10));
		} else {
			iRespuesta = 1;
		}
		return iRespuesta;
	}

}