package mx.altec.enlace.utilerias;
// Conectar.java
// Se conecta a un servidor de correo por medio de un socket y manda el correo usando SMTP.

import java.net.*;
import java.io.*;

public class Conectar implements Serializable
{
  // Puerto de correo.
  static final int port = 25;
  public static int mandar(String de, String para, String datos, String pServidor)
  {
    try
    {
      InetAddress    addr   = InetAddress.getByName(pServidor);
      Socket         socket = new Socket(addr, port);
      BufferedReader in     = new BufferedReader( new InputStreamReader(socket.getInputStream()) );
      PrintWriter    out    = new PrintWriter( new BufferedWriter( new OutputStreamWriter(socket.getOutputStream()) ),true );
      String         str    = "";

      out.println("MAIL FROM:"+ de);
      str = in.readLine();
	  int i = 0;
	  int j = 0;
	  while( j<para.length() )
      {
        j = para.indexOf(",",i);
        if( j>=0 )
        {
          str = para.substring(i,j);
          out.println("RCPT TO:"+ str);
          str = in.readLine();
          i   = ++ j;
        }
        else
        {
          str = para.substring(i,para.length());
          out.println("RCPT TO:"+ str);
          str = in.readLine();
          j = para.length();
        }
      }
      out.println("DATA");
      str = in.readLine();
      out.println(datos);
      str = in.readLine();
      out.println(".");
      str = in.readLine();
      socket.close();
      return 1;
    } catch(Exception e) { e.printStackTrace(); return -1; }
  }
}
