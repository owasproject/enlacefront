/** 
*   Isban Mexico
*   Clase: EmailSender.java
*   Descripcion: Clase encargada de iniciar el proceso de envio de mail.
*   
*   Control de Cambios:
*   1.1   EVERIS        30/11/2007-Se crean funciones nuevas para  el envio de la notificacion por
*        cambio en el correo electronico. 
*   1.2  22/06/2016  FSW. Everis-Se agrega constantes. 
*/

package mx.altec.enlace.utilerias;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.beans.MensajeUSR;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.ValidaOTP;
import mx.altec.enlace.gwt.adminlym.shared.LimitesOperacion;
import mx.altec.enlace.utilerias.EIGlobal.NivelLog;

/**
 *  Clase encargada de iniciar el proceso de envio de mail
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Feb 18, 2007
 */
public class EmailSender {

    /**
     * Constante que indica que las cuentas se autorizan en Enlace
     */
    public static final String ENLACE_SOURCE = "ENLACE";

    /**
     * Constante que indica que las cuentas se autorizan en INTRANET
     */
    public static final String INTRANET_SOURCE = "INTRANET";

    /**
     * Constante para el asunto del mail de confirmacion a usuario
     */

    private static final String TITULO_NOTIFICACION = "Notificaci" + (char)243 + "n a usuario";

    /**
     * Constante para el asunto del mail de confirmacion a usuario y mantenimiento
     */

    private static final String TITULO_NOTIFICACION_MANTENIMIENTO = "Notificaci" + (char)243 + "n a usuario - cambio de dato de contacto";

    /**
     * Constante para el asunto del mail al analista de riesgos
     */

    private static final String TITULO_NOTIFICACION_ANALISTA = "Notificaci" + (char)243 + "n a Analista";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String REGISTROS_POR = " registros por ";

    /** Atributo static y final para guardar cadena constante manejada en el codigo. */
    private static final String INICIAL_CVE_DIVISA = "inicialCveDivisa";

    /**
     * titulo del Mail
     */
    private String subject;

    /**
     * variable utilizada para indicar las operaciones mancomunadas
     */
    private String estatusSMS="";

    /**
     * Asignar un valor a subject.
     * @param subject Valor a asignar.
     */
    public void setSubject(String subject){
        this.subject = subject;
    }

    /**
     * Obtener el valor de subject.
     * @return subject valor asignado.
     */
    public String getSubject(){
        return this.subject;
    }

    /**
     * Metodo maestro el cual inicia la transaccion para el envio de Email's
     * @param detalleCuentas    Vector conteniente de Objetos Container
     * @param numContrato       Numero de Contrato
     * @param descContrato      Descripcion del Contrato
     * @param request           Objeto request
     * @param idLote            Identificador Lote
     * @param session           Objeto BaseResource para obtener datos de sesion del cliente
     * @param numReg            Numero de registros
     */
    public void confirmaEmail(Vector detalleCuentas, String numContrato, String descContrato, HttpServletRequest request, String idLote, BaseResource session, String numReg){
        EmailDetails ed = null;
        String lotes = null;

        try{
            lotes = idLote;
            ed = new EmailDetails(detalleCuentas, numContrato, descContrato, lotes);

            MensajeUSR mensajeUSR = session.getMensajeUSR();
            ServicioMensajesUSR servicioMensajesUSR=new ServicioMensajesUSR();

            String mensajeCorreo = "";
            if(mensajeUSR != null ){

                mensajeCorreo = ed.msgType().toString();
            setSubject("Notificaci" + (char)243 + "n de alta de cuentas");
                EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [ContCtas_AL] -> CORREO                 [" + mensajeCorreo+"]", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [ContCtas_AL] -> session.getUserID8()   [" + session.getUserID8()+"]", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [ContCtas_AL] -> EmailContrato          [" + mensajeUSR.getEmailContrato()+"]", EIGlobal.NivelLog.DEBUG);

                servicioMensajesUSR.enviaEmailContrato(session.getUserID8(), mensajeUSR.getEmailContrato(), mensajeCorreo);
            }

            ed.setNumRegImportados(Integer.parseInt(numReg));

            sendNotificacion(request, IEnlace.NOT_ALTA_CUENTAS_CONTCTAS_AL, ed);

        }catch(Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
        }
    }

        /**
         *  yhg 10.07.2010: Metodo encargado de enviar mensajes de notificacion email y sms Capitulo X
         *
         *  @param request Objeto request de la peticion.
         *  @param operRealizada Identificador de la operacion realizada.
         *  @param emailDetails Objeto EmailDetails
         */
        public void sendNotificacion(HttpServletRequest request,int operRealizada, EmailDetails emailDetails) {

            EIGlobal.mensajePorTrace("EmailSender::inicio sendConfirmacionEmail", EIGlobal.NivelLog.DEBUG);



                try{

                        setSubject(TITULO_NOTIFICACION);

                        if(operRealizada!=IEnlace.PAGO_NOMINA_PRE && operRealizada!=IEnlace.NOT_MODIF_EMPL_MAS && operRealizada!=IEnlace.NOT_BAJA_EMPL_MAS
                                                                && operRealizada!=IEnlace.NOT_MODIF_EMPL_UNI && operRealizada!=IEnlace.NOT_BAJA_EMPL_UNI
                                                                && operRealizada != IEnlace.NOT_ALTA_EMPLEADOS && operRealizada != IEnlace.NOT_ALTA_CUENTAS_CONTCTAS_AL
                                                                && operRealizada != IEnlace.NOT_ALTA_CUENTAS_CATNOMINTERBREGISTRO && operRealizada != IEnlace.NOT_TRANSFERENCIAS_FX_ONLINE
                                                                && operRealizada != IEnlace.SERV_TARJETA_VINCULA
                                                                && operRealizada != IEnlace.BLOQUEO_TOKEN_INTENTOS
                                                                && operRealizada != IEnlace.DESVINCULACION_DISP && operRealizada != IEnlace.CAMBIO_IMAGEN
                                                                && operRealizada != IEnlace.CAMBIO_PREGUNTA && operRealizada != IEnlace.ENROLAMIENT0_RSA
                                                                && operRealizada != IEnlace.REVIEW_RSA_ANALISTA && operRealizada != IEnlace.DENY_RSA_ANALISTA
                                                                && operRealizada != IEnlace.REVIEW_RSA_CLIENTE
                                                                && operRealizada != IEnlace.SOLEDOCTAHIST
                                                                && operRealizada != IEnlace.EDO_CUENTA_IND
                                                                && operRealizada != IEnlace.CONFIGMASPORCUENTA
                                                                && operRealizada != EdoCtaConstantes.CONFIGMASPORTC) {
                            emailDetails.setEstatusActual(obtenDescripcionEstatus(emailDetails.getEstatusActual()));
                        }
                        String mensajeCorreo="";
                        String mensajeSMS="";
                        StringBuffer mensajeInicio = new StringBuffer();
                        StringBuffer mensajeSmsBuffer = new StringBuffer();
                        String divisa="";


                        switch(operRealizada){
                        //IRA Transferencia mismo Banco Masiva
                            case(IEnlace.MAS_TRANS_MISMO_BANCO):
                                String operacionEjecutada=" Transferencia mismo Banco ";

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_TRANS_MISMO_BANCO", EIGlobal.NivelLog.DEBUG);

                                if(emailDetails.isPAGT()){
                                    EIGlobal.mensajePorTrace("EmailSender::Pago de tarjeta -----> " + emailDetails.isPAGT(), EIGlobal.NivelLog.DEBUG);
                                    operacionEjecutada = " Pago Tarjeta de Credito ";
                                }

                                mensajeSmsBuffer
                                .append(operacionEjecutada).append(estatusSMS).append(emailDetails.getNumRegImportados())
                                .append(REGISTROS_POR).append(emailDetails.getImpTotal());

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_TRANS_MISMO_BANCO ---->>> TipoTransferenciaUni: " + emailDetails.getTipoTransferenciaUni(), EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasMas().toString();

                                break;
                                //Luis INTERBANCARIAS MULTIPLE
                            case(IEnlace.MAS_TRANS_INTERBANCARIAS):

                             if(request.getSession().getAttribute(INICIAL_CVE_DIVISA)!=null)
                                {
                                    divisa=" "+(String)request.getSession().getAttribute(INICIAL_CVE_DIVISA);
                                }
                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_TRANS_INTERBANCARIAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasMas().toString();

                                mensajeSmsBuffer
                                .append(" Transferencia Interbancaria ").append(estatusSMS).append(emailDetails.getNumRegImportados())
                                .append(REGISTROS_POR).append(emailDetails.getImpTotal()).append(divisa);


                                break;
                                //YHG INTERNACIONALES MULTIPLE
                            case(IEnlace.MAS_TRANS_INTERNACIONALES):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_TRANS_INTERNACIONALES", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasMas().toString();
                                mensajeSmsBuffer
                                .append(" Transferencia Internacional ").append(estatusSMS).append(emailDetails.getNumRegImportados())
                                .append(REGISTROS_POR).append(emailDetails.getImpTotal());

                                break;
                                //Luis Nomina Tradicional
                            case(IEnlace.PAGO_NOMINA):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_NOMINA", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoNomina().toString();

                                mensajeSmsBuffer
                                                .append(" Programacion pago Nomina Tradicional cta. ").append(estatusSMS)
                                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(),4)).append(' ')
                                                .append(emailDetails.getNumRegImportados())
                                                .append(REGISTROS_POR).append(emailDetails.getImpTotal());


                                break;
                                //Luis Nomina Interbancaria
                            case(IEnlace.PAGO_NOMINA_INTER):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_N�MINA_INTER", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoNomina().toString();
                                mensajeSmsBuffer
                                                .append(" Programacion pago Nomina Interbancaria cta. ").append(estatusSMS)
                                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(),4)).append(' ')
                                                .append(emailDetails.getNumRegImportados())
                                                .append(REGISTROS_POR).append(emailDetails.getImpTotal());

                                //mensajeSMS = mensajeSmsBuffer.toString();
                                break;
                            //IRA Nomina prepago y pago individual
                            case(IEnlace.PAGO_NOMINA_PRE):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_NOMINA_PRE", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoNominaPre().toString();
                                mensajeSmsBuffer
                                                .append(" Programaci�n Tarjeta de Pagos Santander cta. ").append(estatusSMS)
                                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(' ')
                                                .append(emailDetails.getNumRegImportados()).append(REGISTROS_POR)
                                                .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));

                                break;

                            //Daniel Pago SUA Disco
                            case(IEnlace.PAGO_SUA):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_SUA", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoImp().toString();

                                mensajeSmsBuffer
                                .append(" Pago SUA  ").append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(' ')
                                .append(" por ")
                                .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));


                                break;
                            //SIPARE 28/09/2012 - VMB
                            case(IEnlace.PAGO_SUA_LC):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_SUA_LC", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoImpLC().toString();

                                mensajeSmsBuffer
                                .append(" Pago SUA Linea de Captura con estatus  ").append(emailDetails.getEstatusActual())
                                .append(" por ")
                                .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));

                                break;

                                //YHG SAR
                            case(IEnlace.PAGO_SAR):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_SAR", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoImp().toString();
                                mensajeSmsBuffer
                                .append(" Pago SAR  ").append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(' ')
                                .append(" por ")
                                .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));

                                break;
                                //YHG PAGO DE IMPUESTOS
                            case(IEnlace.NOT_PAGO_IMPUESTOS):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_IMPUESTOS IMPORTE-->"+emailDetails.getImpTotal(), EIGlobal.NivelLog.INFO);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoImp().toString();
                                mensajeSmsBuffer
                                .append(" Pago de Impuestos  ").append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(' ')
                                .append(" por ")
                                .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));
                                break;

                            case(IEnlace.ALTA_PROVEEDORES):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_ALTA_PROVEEDORES", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionAltaProv().toString();
                                mensajeSmsBuffer
                                .append(" Alta de   ")
                                .append(emailDetails.getNumRegImportados()).append(' ')
                                .append(" Proveedores ").append(estatusSMS);

                                break;

                            case(IEnlace.PAGO_PROVEEDORES):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MAS_PAGO_PROVEEDORES", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoProv().toString();
                                mensajeSmsBuffer
                                    .append(" Programacion Pago  Proveedores cta. ").append(estatusSMS)
                                    .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(' ')
                                    .append(emailDetails.getNumRegImportados()).append(REGISTROS_POR)
                                    .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));
                                break;
                            //IRA Transferencia mismo Banco unico
                            case(IEnlace.UNI_TRANS_MISMO_BANCO):
                                 operacionEjecutada=" Transferencia mismo Banco ";
                                EIGlobal.mensajePorTrace("EmailSender::notificacion UNI_TRANS_MISMO_BANCO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasUni().toString();

                                if(emailDetails.isPAGT()){
                                    EIGlobal.mensajePorTrace("EmailSender::Pago de tarjeta -----> " + " Pago de Tarjeta ".equals(emailDetails.getTipoTransferenciaUni().substring(0, 17)), EIGlobal.NivelLog.INFO);
                                    operacionEjecutada=" Pago Tarjeta de Credito ";
                                }
                                mensajeSmsBuffer
                                .append(operacionEjecutada).append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(" a cuenta ")
                                .append(emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(),4))
                                .append(" por ").append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));

                                break;
                                //Luis MISMO INTERBANCARIA UNITARIA
                            case(IEnlace.UNI_TRANS_INTERBANCARIAS):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion UNI_TRANS_INTERBANCARIAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasUni().toString();


                                mensajeSmsBuffer
                                .append(" Transferencia Interbancaria ").append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(" a cuenta ")
                                .append(emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(),4))
                                .append(" por ").append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));


                                break;
                                //YHG INTERNACIONALES UNITARIA
                            case(IEnlace.UNI_TRANS_INTERNACIONALES):

                            if(request.getSession().getAttribute(INICIAL_CVE_DIVISA)!=null)
                                {
                                    divisa=" "+(String)request.getSession().getAttribute(INICIAL_CVE_DIVISA);
                                }
                                EIGlobal.mensajePorTrace("EmailSender::notificacion UNI_TRANS_INTERNACIONALES", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionTransferenciasUni().toString();
                                mensajeSmsBuffer
                                .append(" Transferencia Internacional ").append(estatusSMS)
                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4)).append(" a cuenta ")
                                .append(emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(),4))
                                .append(" por ").append(ValidaOTP.formatoNumero(emailDetails.getImpTotal())).append(divisa);
                                break;


                            case(IEnlace.PAGO_OCURRE)://MAAM 10/JUL/2010

                                EIGlobal.mensajePorTrace("EmailSender::notificacion PAGO_OCURRE", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoOcurre().toString();
                                mensajeSmsBuffer
                                .append(" Orden Pago Ocurre ").append(estatusSMS)
                                .append(emailDetails.getNumRegImportados()).append(REGISTROS_POR)
                                    .append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));
                                                            break;
                            case (IEnlace.MANC_GEN_FOLIO):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion MANC_GEN_FOLIO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionManGenFolio().toString();

                                mensajeSmsBuffer
                                    .append(" Generacion de folio ")
                                    .append(emailDetails.getTipo_operacion())
                                    .append(" cta. ").append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(),4))
                                    .append(" por ").append(emailDetails.getImpTotal());

                                EIGlobal.mensajePorTrace("EmailSender::notificacion MANC_GEN_FOLIO "+mensajeCorreo, EIGlobal.NivelLog.DEBUG);
                                EIGlobal.mensajePorTrace("EmailSender::notificacion MANC_GEN_FOLIO "+mensajeCorreo, EIGlobal.NivelLog.DEBUG);
                                break;

                                //Daniel Alta Cuentas
                            case (IEnlace.NOT_ALTA_CUENTAS):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ALTA_CUENTAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionCuentas().toString();
                                //mensajeSMS = " Alta de " + emailDetails.getNumRegImportados() + " cuentas ";
                                mensajeSmsBuffer.append(" Alta de ")
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" cuentas ");
                                break;
                            case (IEnlace.NOT_ALTA_CUENTAS_CONTCTAS_AL):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ALTA_CUENTAS_CONTCTAS_AL", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgType().toString();
                                mensajeSmsBuffer.append(" Alta de ")
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" cuentas ");
                                break;
                            case (IEnlace.NOT_ALTA_CUENTAS_CATNOMINTERBREGISTRO):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ALTA_CUENTAS_CATNOMINTERBREGISTRO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTypeConfirmacionInterb().toString();
                                mensajeSmsBuffer.append(" Alta de ")
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" cuentas ");
                                break;

                                //Daniel Cambio de contrase�a
                            case (IEnlace.CPASWD):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion CPASWD", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCambioPaswd().toString();
                                mensajeSmsBuffer.append(" Solicitud de Cambio de Password ");

                                break;

                                //Daniel Baja de Cuentas
                            case (IEnlace.NOT_BAJA_CUENTAS):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_BAJA_CUENTAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCuentas().toString();
                                mensajeSmsBuffer.append(" Baja de ")
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" cuentas ");
                                break;

                            case (IEnlace.NOT_ASIGNACION_TARJETA):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ASIGNACION_TARJETA", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionAsignacionTarjeta().toString();
                                mensajeSmsBuffer
                                                .append(" Asignacion de ")
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" tarjetas").append(estatusSMS);

                                break;

                            case (IEnlace.NOT_ALTA_EMPLEADOS):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ASIGNACION_TARJETA", EIGlobal.NivelLog.DEBUG);
                                //mensajeCorreo = emailDetails.msgTextNotificacionAltaEmp().toString();
                                mensajeCorreo = emailDetails.msgType(emailDetails.getFileAttachmentName()).toString();
                                EIGlobal.mensajePorTrace("<><><><><><><>[EmailSender] -> Mensaje correo -> [ " + mensajeCorreo + " ]", EIGlobal.NivelLog.DEBUG);
                                mensajeSmsBuffer
                                    .append(" Alta de ")
                                    .append(emailDetails.getNumRegImportados())
                                    .append(" Empleados").append(estatusSMS);

                                break;

                            case (IEnlace.NOT_CAMBIO_DIVISAS):

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_CAMBIO_DIVISAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCambioDivisas().toString();
                                mensajeSmsBuffer.append(" Cambio de Divisa ").append(estatusSMS);


                                if( emailDetails.getNumRegImportados() > 1 ) {

                                    mensajeSmsBuffer.append(emailDetails.getNumRegImportados()).append(" registros");

                                } else {

                                    mensajeSmsBuffer.append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4))
                                    .append( " a cuenta ")
                                    .append( emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(), 4));

                                }

                                mensajeSmsBuffer.append(" por " + emailDetails.getImpTotal().split("-")[1]).append(" MN ");

                                break;

                            case (IEnlace.NOT_MODIF_EMPL_MAS):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_MODIF_EMPL_MAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionModEmp().toString();
                                mensajeSmsBuffer.append(" Modificacion de ").append(emailDetails.getNumRegImportados()).append(" Empleados").append(estatusSMS);
                                break;

                            case (IEnlace.NOT_BAJA_EMPL_MAS):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_BAJA_EMPL_MAS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionBajaEmp().toString();
                                mensajeSmsBuffer.append(" Baja de ").append(emailDetails.getNumRegImportados()).append(" Empleados").append(estatusSMS);
                                break;

                            case (IEnlace.NOT_CREDITO_PREPAGO):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_CREDITO_PREPAGO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCreditoPrepago().toString();
                                mensajeSmsBuffer.append(" Tarjeta de Pagos Santander ").append(estatusSMS).append(" cta. ").append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4))//YHG NPRE
                                                .append(" por ").append(emailDetails.getImpTotal());
                                break;

                            case (IEnlace.NOT_MODIF_EMPL_UNI):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_MODIF_EMPL_UNI", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionModEmpUni().toString();
                                mensajeSmsBuffer.append(" Modificacion de cuenta ")
                                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4));
                                break;

                            case (IEnlace.NOT_BAJA_EMPL_UNI):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_BAJA_EMPL_UNI", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionBajaEmpUni().toString();
                                mensajeSmsBuffer.append(" Baja de cuenta ")
                                                .append(emailDetails.obtenSubCadena(emailDetails.getNumCuentaCargo(), 4));
                                break;
                            case (IEnlace.NOT_ADMIN_USUARIOS):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ADMIN_USUARIOS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionAdminUsuarios().toString();
                                mensajeSmsBuffer.append(emailDetails.getTipo_operacion()+" DE USUARIO, USUARIO " +
                                        emailDetails.obtenSubCadena(emailDetails.getNumRef(),4));
                                break;
                            case (IEnlace.NOT_ADMIN_LYM):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_ADMIN_LYM", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionLyM().toString();

                                if (! "".equals(emailDetails.getTipo_operacion().trim())) {
                                    mensajeSmsBuffer.append("ESTABLECIMIENTO DE LIMITES Y MONTOS A NIVEL CONTRATO ");
                                } else {
                                    ArrayList<LimitesOperacion> limites =  emailDetails.getListaOperLYM();
                                    if (limites.size() == 1) {
                                        mensajeSmsBuffer.append("ESTABLECIMIENTO DE LIMITES Y MONTOS, USUARIO " +
                                                limites.get(0).getUsuario() + " PRODUCTO " +
                                                limites.get(0).getDescripcionOperacion() + " ");
                                    } else {
                                        mensajeSmsBuffer.append("ESTABLECIMIENTO DE LIMITES Y MONTOS " +
                                                limites.size() + " Registros");
                                    }
                                }
                                break;
                            case(IEnlace.NOT_BENEFICIARIOS_PAGO_OCURRE):
                                operacionEjecutada=" Autorizaci�n Beneficiarios Pago Ocurre ";
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_BENEFICIARIOS_PAGO_OCURRE", EIGlobal.NivelLog.DEBUG);

                                mensajeSmsBuffer.append(operacionEjecutada)
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" registros con referencia de carga ")
                                                .append(emailDetails.getNumRef());
                                mensajeCorreo = emailDetails.msgTextNotificacionAutBenefPagOcurre().toString();

                                break;
                            case(IEnlace.NOT_BENEFICIARIOS_PAGO_DIRECTO):
                                operacionEjecutada=" Autorizaci�n Beneficiarios Pago Directo ";
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_BENEFICIARIOS_PAGO_DIRECTO", EIGlobal.NivelLog.DEBUG);

                                mensajeSmsBuffer.append(operacionEjecutada)
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" registros con referencia de carga ")
                                                .append(emailDetails.getNumRef());
                                mensajeCorreo = emailDetails.msgTextNotificacionAutBenefPagDirecto().toString();

                                break;

                            case(IEnlace.NOT_TRANSFERENCIAS_FX_ONLINE):
                                operacionEjecutada=" Transferencias FX Online ";
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_TRANSFERENCIAS_FX_ONLINE", EIGlobal.NivelLog.DEBUG);

                                mensajeSmsBuffer.append(operacionEjecutada)
                                                .append(emailDetails.getNumRegImportados())
                                                .append(" registros con referencia de Enlace ")
                                                .append(emailDetails.getNumRef())
                                                .append(" por un importe de ")
                                                .append(emailDetails.getImpTotal());
                                mensajeCorreo = emailDetails.msgTextNotificacionTransferenciaReuters().toString();

                                break;

                            case(IEnlace.BLOQUEO_TOKEN_INTENTOS):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion BLOQUEO_TOKEN_INTENTOS", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextBloqTokenIntFallidos().toString();

                                break;

                            case(IEnlace.SERV_TARJETA_VINCULA):
                                operacionEjecutada=" Alta Tarjetas Vinculacion ";
                                EIGlobal.mensajePorTrace("EmailSender::notificacion SERV_TARJETA_VINCULA", EIGlobal.NivelLog.DEBUG);
                                mensajeSmsBuffer.append("Alta de ")
                                                .append(emailDetails.getRegVincTarj()[0])
                                                .append(" Vinculaci�n de Tarjetas al Contrato ")
                                                .append(emailDetails.getRegVincTarj()[1])
                                                .append(", Carga ")
                                                .append(emailDetails.getRegVincTarj()[2]);
                                mensajeCorreo = emailDetails.msgTextAltaCtaNivel1().toString();
                                break;
                            //Stefanini parametros RSA
                            case(IEnlace.DESVINCULACION_DISP):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion DESVINCULACION", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionDesvinculacion().toString();
                                mensajeSmsBuffer.append(" Solicitud de Desvinculacion ");
                                break;

                            case(IEnlace.CAMBIO_IMAGEN):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion CABIO_IMAGEN", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCambioImag().toString();
                                mensajeSmsBuffer.append(" Solicitud de Cambio de Imagen ");
                                break;

                            case(IEnlace.CAMBIO_PREGUNTA):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion CABIO_PREGUNTA", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionCambioPreg().toString();
                                mensajeSmsBuffer.append(" Solicitud de Cambio de Preguna ");
                                break;

                            case(IEnlace.ENROLAMIENT0_RSA):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion ENROLAMIENTO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo = emailDetails.msgTextNotificacionEnrolamientoRSA().toString();
                                mensajeSmsBuffer.append(" Solicitud de Cambio de Enrolamiento ");
                                break;
                            case(IEnlace.REVIEW_RSA_ANALISTA):
                               EIGlobal.mensajePorTrace("EmailSender::notificacion REVIEW_RSA_ANALISTA", EIGlobal.NivelLog.DEBUG);
                               mensajeCorreo = emailDetails.msgTextNotificacionReviewAnalista().toString();
                               break;
                            case(IEnlace.REVIEW_RSA_CLIENTE):
                        EIGlobal.mensajePorTrace("EmailSender::notificacion REVIEW_RSA_CLIENTE", EIGlobal.NivelLog.DEBUG);
                        mensajeCorreo = emailDetails.msgTextNotificacionRSACliente().toString();
                        break;
                            case(IEnlace.DENY_RSA_ANALISTA):
                        EIGlobal.mensajePorTrace("EmailSender::notificacion DENY_RSA_ANALISTA", EIGlobal.NivelLog.DEBUG);
                        mensajeCorreo = emailDetails.msgTextNotificacionDenyAnalista().toString();
                        break;
                        /** Notificacion Pago Micrositio **/
                            case(IEnlace.NOT_PAGO_MICROSITIO):
                                if("0093".equals(emailDetails.getConvenio().trim())) {
                                    operacionEjecutada=" Pago Referenciado SAT Micrositio";
                                } else {
                                    operacionEjecutada=" Pago Micrositio";
                                }

                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_PAGO_MICROSITIO", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionPagoMicrositio().toString();
                                mensajeSmsBuffer
                                .append(operacionEjecutada).append(", al Convenio ")
                                .append(emailDetails.getConvenio())
                                .append(" por ").append(ValidaOTP.formatoNumero(emailDetails.getImpTotal()));
                                break;
                        /** Fin Notificacion Pago Micrositio **/

                        /**                          **/
                        case(IEnlace.EDO_CUENTA_IND):
                                operacionEjecutada="true".equals(emailDetails.getEstatusFinal().toString()) ? "Alta" : "Baja" ;
                                SimpleDateFormat sFormat = new SimpleDateFormat("dd-MM-yy hh:mm:ss", Locale.getDefault());
                                EIGlobal.mensajePorTrace("EmailSender::notificacion EDO_CUENTA_IND_PDF", EIGlobal.NivelLog.DEBUG);
                                mensajeSmsBuffer.append(operacionEjecutada)
                                                .append(" individual del servicio de Paperless para la ")
                                                /**
                                                 * Se agrega validacion para cuando es Cuenta o Tarjeta
                                                 * Configuracion Paperless Edo Cta Individual
                                                 * FSW Indra 03/03/2015 PYME
                                                 */
                                                .append( emailDetails.getTipo_operacion().contains("001") ? "cuenta " : "Tarjeta " )
                                                .append(emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(),4));
                                if( emailDetails.getTipo_operacion().contains("PDF") ) {
                                    mensajeCorreo = emailDetails.msgTextConfigEdoCtaIndPDF().toString();
                                }
                                break;

                        case(IEnlace.CONFIGMASPORCUENTA):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion Configuracion estados de cuenta masivo [CONFIGMASPORCUENTA]", EIGlobal.NivelLog.DEBUG);
                                EIGlobal.mensajePorTrace("EmailSender::notificacion\n"+
                                        "emailDetails.getFormatoArchivo()[0]"+emailDetails.getFormatoArchivo()[0]+"\n"+
                                        "emailDetails.getFormatoArchivo()[1]"+emailDetails.getFormatoArchivo()[1]+"\n"+
                                        "emailDetails.getFormatoArchivo()[2]"+emailDetails.getFormatoArchivo()[2]+"\n",
                                        NivelLog.DEBUG);
                                mensajeSmsBuffer.append("Modificacion masiva de estado de envio de Estado de Cuenta Impreso ")
                                                //.append(emailDetails.obtenSubCadena("000"+emailDetails.getFormatoArchivo()[1],emailDetails.getFormatoArchivo()[1].length()<4?4:emailDetails.getFormatoArchivo()[1].length() ))
                                                //.append(emailDetails.obtenSubCadena("000" + emailDetails.getNumRef(), emailDetails.getNumRef().length() < 4 ? 4 : emailDetails.getNumRef().length()))
                                                .append(emailDetails.getFormatoArchivo()[1])
                                                .append(" registro(s)");
                                mensajeCorreo = emailDetails.msgTextConfigMasPorCuenta().toString();
                                break;
                        case(EdoCtaConstantes.CONFIGMASPORTC):
                            EIGlobal.mensajePorTrace("EmailSender::notificacion Configuracion estados de cuenta masivo [CONFIGMASPORTC]", EIGlobal.NivelLog.DEBUG);
                            EIGlobal.mensajePorTrace("EmailSender::notificacion\n"+
                                    "emailDetails.getFormatoArchivo()[0]"+emailDetails.getFormatoArchivo()[0]+"\n"+
                                    "emailDetails.getFormatoArchivo()[1]"+emailDetails.getFormatoArchivo()[1]+"\n"+
                                    "emailDetails.getFormatoArchivo()[2]"+emailDetails.getFormatoArchivo()[2]+"\n",
                                    NivelLog.DEBUG);
                            mensajeSmsBuffer.append("Modificacion masiva de estado de envio de Estado de Cuenta Impreso ")
                                            //.append(emailDetails.obtenSubCadena("000"+emailDetails.getFormatoArchivo()[1],emailDetails.getFormatoArchivo()[1].length()<4?4:emailDetails.getFormatoArchivo()[1].length() ))
                                            //.append(emailDetails.obtenSubCadena("000" + emailDetails.getNumRef(), emailDetails.getNumRef().length() < 4 ? 4 : emailDetails.getNumRef().length()))
                                            .append(emailDetails.getFormatoArchivo()[1])
                                            .append(" registro(s)");
                            mensajeCorreo = emailDetails.msgTextConfigMasPorTC();
                            break;

                        case(IEnlace.SOLEDOCTAHIST):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion Solicitud estados de cuenta Historicos [SOLEDOCTAHIST]", EIGlobal.NivelLog.DEBUG);
                                //mensajeSmsBuffer.append(emailDetails.obtenSubCadena(emailDetails.getNumeroContrato(), 4));
                                mensajeSmsBuffer.append(" Solicitud de disposicion de Estados de Cuenta, cuenta ");
                                mensajeSmsBuffer.append(emailDetails.obtenSubCadena(emailDetails.getCuentaDestino(),4));
                                int nPer=emailDetails.getListaErrores().size()+emailDetails.getListaFolios().size();
                                mensajeSmsBuffer.append(", " + nPer + " periodos solicitados");
                                mensajeCorreo = emailDetails.msgTextSolicitaPeriodosHistoricos().toString();
                                break;

                        //OPERACIONES PARA LA FIEL.     
                        case(IEnlace.NOT_FIEL_BAJA):
                            EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_FIEL_BAJA", EIGlobal.NivelLog.DEBUG);
                            mensajeCorreo = emailDetails.msgTextNotificacionFielBaja().toString();
                            mensajeSmsBuffer.append(" Baja de ").append(emailDetails.getNumRegImportados()).append(" FIEL ");
                            break;
                         
                        case(IEnlace.NOT_FIEL_ALTA):
                            EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_FIEL_ALTA", EIGlobal.NivelLog.DEBUG);
                            mensajeCorreo = emailDetails.msgTextNotificacionFielAlta().toString();
                            mensajeSmsBuffer.append(" Registro de ").append(emailDetails.getNumRegImportados()).append(" FIEL ");
                            break;
                            
                        case(IEnlace.NOT_FIEL_MODIFICACION):
                            EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_FIEL_MODIFICACION", EIGlobal.NivelLog.DEBUG);
                            mensajeCorreo = emailDetails.msgTextNotificacionFielModificacion().toString();
                            mensajeSmsBuffer.append(" Modificacion de ").append(emailDetails.getNumRegImportados()).append(" FIEL ");
                            break;                        
    
                        /**                          **/
                        }

                        mensajeInicio.append("Santander Enlace ").append(emailDetails.obtenSubCadena(emailDetails.getNumeroContrato(),4));
                        mensajeSMS = mensajeInicio.toString().concat(" ").concat(mensajeSmsBuffer.toString());
                        BaseResource session = (BaseResource) request.getSession().getAttribute("session");
                        MensajeUSR mensajeUSR = session.getMensajeUSR();

                        //para correo analista
                        if(operRealizada == IEnlace.DENY_RSA_ANALISTA || operRealizada == IEnlace.REVIEW_RSA_ANALISTA) {
                            mensajeUSR.setEmailUsuario(Global.correoAnalistaRiesgos);
                            mensajeUSR.setMedioNotSuperUsuario("B1");
                            mensajeUSR.setSuperUsuario(false);
                        }
                        //fin para correo analista

                        if(mensajeUSR !=null){
                        ServicioMensajesUSR servicioMensajesUSR=new ServicioMensajesUSR();
                        EIGlobal.mensajePorTrace("SMS[" + mensajeSMS+"]", EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("CORREO[" + mensajeCorreo+"]", EIGlobal.NivelLog.DEBUG);
                        EIGlobal.mensajePorTrace("SIPARE[" + mensajeCorreo+"]", EIGlobal.NivelLog.DEBUG);//--
                        if(operRealizada == IEnlace.BLOQUEO_TOKEN_INTENTOS){
                        EIGlobal.mensajePorTrace("Bloqueo de Token por intentos fallidos, no se manda SMS", EIGlobal.NivelLog.DEBUG);
                        mensajeUSR.setIsSms(false);
                        }
                        if(operRealizada == IEnlace.DESVINCULACION_DISP || operRealizada == IEnlace.CAMBIO_IMAGEN || operRealizada == IEnlace.CAMBIO_PREGUNTA || operRealizada == IEnlace.ENROLAMIENT0_RSA
                        || operRealizada == IEnlace.DENY_RSA_ANALISTA|| operRealizada == IEnlace.REVIEW_RSA_ANALISTA|| operRealizada == IEnlace.REVIEW_RSA_CLIENTE || (operRealizada == IEnlace.NOT_PAGO_MICROSITIO && estatusSMS.contains("Mancomunada"))){
                                EIGlobal.mensajePorTrace("Operaciones RSA o Pago Micrositio Mancomunado, " +
                                        "no se manda SMS", EIGlobal.NivelLog.DEBUG);
                                mensajeUSR.setIsSms(false);
                            }

                            if(operRealizada == IEnlace.DENY_RSA_ANALISTA || operRealizada == IEnlace.REVIEW_RSA_ANALISTA) {
                                servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), mensajeSMS, mensajeCorreo,TITULO_NOTIFICACION_ANALISTA );
                            }else{
                                servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), mensajeSMS, mensajeCorreo,this.subject);
                            }
                        }

                } catch (Exception e) {
                    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                }

            EIGlobal.mensajePorTrace("EmailSender::Fin sendConfirmacionEmail", EIGlobal.NivelLog.DEBUG);

        }



        /**
         * metodo que trabaja con lista para armar tabla dinamica en mensaje de corrreo
         * @param request : request
         * @param operRealizada : operRealizada
         * @param emailDetails : emailDetails
         * @param listaNotifica : listaNotifica
         */
        public void sendNotificacion(HttpServletRequest request,int operRealizada, EmailDetails emailDetails, ArrayList listaNotifica) {

            EIGlobal.mensajePorTrace("EmailSender::inicio sendConfirmacionEmail", EIGlobal.NivelLog.DEBUG);
                try{
                        setSubject(TITULO_NOTIFICACION);

                        String mensajeCorreo="";
                        String mensajeSMS="";

                        StringBuffer mensajeSmsBuffer = new StringBuffer();
                        mensajeSmsBuffer.append("Santander Enlace ").append(emailDetails.obtenSubCadena(emailDetails.getNumeroContrato(), 4));

                        switch(operRealizada){

                            case(IEnlace.NOT_AUT_CANC_OPER_MANC):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_AUT_CANC_OPER_MANC", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionAutCancOperManc(listaNotifica).toString();

                                if (emailDetails.getTipo_operacion().equals("Autorizaci"+ (char)243 +"n")) {
                                    mensajeSmsBuffer.append(" Autorizacion de Operaciones Mancomunadas Nacionales ");
                                }
                                if (emailDetails.getTipo_operacion().equals("Cancelaci"+ (char)243 +"n")) {
                                    mensajeSmsBuffer.append(" Cancelacion de Operaciones Mancomunadas Nacionales ");
                                }
                                break;

                            case(IEnlace.NOT_AUT_CANC_OPER_MANC_INTER):
                                EIGlobal.mensajePorTrace("EmailSender::notificacion NOT_AUT_CANC_OPER_MANC", EIGlobal.NivelLog.DEBUG);
                                mensajeCorreo=emailDetails.msgTextNotificacionAutCancOperMancInter(listaNotifica).toString();
                                if (emailDetails.getTipo_operacion().equals("Autorizaci"+ (char)243 +"n")){
                                    mensajeSmsBuffer.append(" Autorizacion de Operaciones Mancomunadas Internacionales ");
                                }
                                if (emailDetails.getTipo_operacion().equals("Cancelaci"+ (char)243 +"n")) {
                                    mensajeSmsBuffer.append(" Cancelacion de Operaciones Mancomunadas Internacionales ");
                                }
                                break;
                        }

                        mensajeSmsBuffer.append(emailDetails.getNumRegImportados()).append(REGISTROS_POR);

                        if(emailDetails.getImpTotalMXN().trim().length()>0) {
                            mensajeSmsBuffer.append(ValidaOTP.formatoNumero(emailDetails.getImpTotalMXN())).append(" MXN ");
                        }
                        if(emailDetails.getImpTotalUSD().trim().length()>0) {
                            mensajeSmsBuffer.append(ValidaOTP.formatoNumero(emailDetails.getImpTotalUSD())).append(" USD");
                        }

                        mensajeSMS = mensajeSmsBuffer.toString();

                        BaseResource session = (BaseResource) request.getSession().getAttribute("session");
                        MensajeUSR mensajeUSR=session.getMensajeUSR();

                        ServicioMensajesUSR servicioMensajesUSR=new ServicioMensajesUSR();
                        EIGlobal.mensajePorTrace("SENDER -> ANTES DEL ENVIO" + mensajeSMS, EIGlobal.NivelLog.DEBUG);
                        servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), mensajeSMS, mensajeCorreo, this.subject);
                        EIGlobal.mensajePorTrace("SENDER -> DESPUES DEL ENVIO", EIGlobal.NivelLog.DEBUG);

                } catch (Exception e) {
                    EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
                }

            EIGlobal.mensajePorTrace("EmailSender::Fin sendConfirmacionEmail", EIGlobal.NivelLog.DEBUG);

        }

        /**
         * Metodo de envio de notificacion multiple.
         *
         * @param codigoError String con el codigo de error a validar.
         * @return boolean indicando si envio la notificacion (true) o no.
         */
        public boolean enviaNotificacionMultiple(String codigoError)
        {
            EIGlobal.mensajePorTrace("EmailSender::Ecodigo_error["+codigoError+"]-->", EIGlobal.NivelLog.DEBUG);
            String[] codError=codigoError.split("-");
            EIGlobal.mensajePorTrace("EmailSender::codError.length-->"+codError.length, EIGlobal.NivelLog.DEBUG);
            boolean enviaCorreo=false;
            try{
                for(int i=0;i<codError.length;i++){
                    EIGlobal.mensajePorTrace("EmailSender::codError["+i+"]-->"+codError[i], EIGlobal.NivelLog.DEBUG);
                    if(codError[i]!=null && codError[i].length()==8){


                        String  tipoError   = "";
                        String  terminacion = "";

                        tipoError   = codError[i].substring( 0, 4);
                        terminacion = codError[i].substring( 4,8 );

                        if ( "MANC".equals( tipoError ) ) {
                            enviaCorreo=true;
                        } else if ( "PROG".equals( tipoError ) &&  "0000".equals( terminacion ) ) {
                            enviaCorreo=true;  // programado
                        } else if ( "TRANSITO".equals( codError[i] ) ) {
                                        enviaCorreo=true;  // en tr�nsito
                        } else if ( "0000".equals( terminacion ) ) {
                                        enviaCorreo=true; // aceptado
                        }
                    }
                    if(enviaCorreo) {
                        return true;
                    }

                }
            }catch(Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }
            EIGlobal.mensajePorTrace("EmailSender::ENVIA CORREO ESTATUS["+codigoError+"]-->"+enviaCorreo, EIGlobal.NivelLog.DEBUG);
            return enviaCorreo;
        }

        /**
         * Metodo de envio de notificacion.
         *
         * @param codigoError String con el codigo de error a validar.
         * @return boolean indicando si envio la notificacion (true) o no.
         */
        public boolean enviaNotificacion(String codigoError)
        {
        if(codigoError.length()>=8){
         if(codigoError.length()>9 && codigoError.indexOf("-")>-1) {
             return enviaNotificacionMultiple( codigoError);
         } else {
             return enviaNotificacionUnico( codigoError);
         }
        }
        else{
            return false;//el codigo de error no es valido
        }

        }

        /**
         * Metodo para obtener la descripcion del estatus correspondiente al codigo
         * de error enviado como parametro.
         *
         * @param codigoError String con el codigo de error a validar.
         * @return String con la descripcion del estatus.
         */
        public String obtenDescripcionEstatus(String codigoError)
        {
             if(codigoError.length()>9 && codigoError.indexOf("-")>-1) {
                 return obtenDescripcionEstatusMultiple( codigoError);
             } else {
                  return obtenDescripcionEstatusUnico( codigoError);
             }


        }



        /**
         * Metodo para obtener la descripcion del estatus correspondiente al codigo
         * de error enviado como parametro (estatus multiple).
         *
         * @param codigoError String con el codigo de error a validar.
         * @return String con la descripcion del estatus.
         */
        private String obtenDescripcionEstatusMultiple(String codigoError)
        {EIGlobal.mensajePorTrace("EmailSender::codError-->"+codigoError, EIGlobal.NivelLog.DEBUG);
            String  estatus     = "";
            try{
                String  tipoError   = "";
                String  terminacion = "";
                int operManc=0;
                int operProg=0;
                int operTrans=0;
                int operAcep=0;
                int operRechazada=0;

                String[] codError=codigoError.split("-");

                EIGlobal.mensajePorTrace("EmailSender::codError.length-->"+codError.length, EIGlobal.NivelLog.DEBUG);
                for(int i=0;i<codError.length;i++){
                        EIGlobal.mensajePorTrace("EmailSender::codError["+i+"]-->"+codError[i], EIGlobal.NivelLog.DEBUG);
                        if(codError[i]!=null && codError[i].length()==8){

                            tipoError   =  codError[i].substring( 0, 4);
                            terminacion =  codError[i].substring( 4,8 );

                            if ( !"MANC".equals( tipoError ) ){
                                if ( !"PROG".equals( tipoError ) ){
                                    if ( "TRANSITO".equals( codError[i] ) )
                                        {   //estatus = "T"; // en tr�nsito
                                            operTrans++;

                                        }
                                    else if ( !"0000".equals( terminacion ) )
                                    {
                                        //estatus = "R"; // rechazado
                                        operRechazada++;
                                    }
                                    else{
                                        //estatus = "A"; // aceptado
                                        operAcep++;
                                        }
                                }else{

                                        if ( !"0000".equals( terminacion ) )
                                        {
                                            //estatus = "R";
                                            operRechazada++;
                                        }
                                        else{
                                            //estatus = "P"; // programado
                                            operProg++;
                                        }

                                } // if ( PROG )
                            }else{
                                //estatus = "M"; // mancomunada
                                operManc++;
                            } // if ( MANC )



                    }

                }
                 if(operAcep>0) {
                        estatus += " "+operAcep+" Aceptada ";
                 }
                     if(operManc>0){
                         estatus += " "+operManc+" Mancomunada ";
                         estatusSMS=" Mancomunada ";
                     }
                     if(operRechazada>0) {
                         estatus += " "+operRechazada+" Rechazada ";
                     }
                     if(operProg>0) {
                         estatus += " "+operProg+" Programada ";
                     }


            }
            catch(Exception e){
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
                EIGlobal.mensajePorTrace("EmailSender::ESTATUS["+codigoError+"]-->"+estatus, EIGlobal.NivelLog.DEBUG);
            return estatus;
        }

        /**
         * Metodo enviaNotificacionArchivo
         *
         * @param codigoError String con el codigo de error a validar.
         * @return boolean siempre en true.
         */
        public boolean enviaNotificacionArchivo(String codigoError)
        {
            return true;
        }

        /**
         * Metodo para envio de notificacion unico.
         *
         * @param codigoError String con el codigo de error a validar.
         * @return boolean indicando si envio la notificacion (true) o no.
         */
        private boolean enviaNotificacionUnico(String codigoError)
        {
            EIGlobal.mensajePorTrace("EmailSender::ENVIA CORREO ESTATUS["+codigoError+"]-->", EIGlobal.NivelLog.DEBUG);
            boolean enviaCorreo=false;

            try{
                String  tipoError   = "";
                String  terminacion = "";

                tipoError   = codigoError.substring( 0, 4);
                terminacion = codigoError.substring( 4,8 );

                if ( "MANC".equals( tipoError ) && "0000".equals( terminacion )){
                    enviaCorreo=true;
                }
                else if ( "PROG".equals( tipoError ) &&  "0000".equals( terminacion ) ){	
                    enviaCorreo=true;  // programado
                }
                else if ( "TRANSITO".equals( codigoError ) ){
                                enviaCorreo=true;  // en tr�nsito
                }
                else if ( "0000".equals( terminacion ) ){
                                enviaCorreo=true; // aceptado
                }
            }
            catch(Exception e){
            EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }

            EIGlobal.mensajePorTrace("EmailSender::ENVIA CORREO ESTATUS["+codigoError+"]-->"+enviaCorreo, EIGlobal.NivelLog.DEBUG);
            return enviaCorreo;
        }

        /**
         * Metodo para obtener la descripcion del estatus correspondiente al codigo
         * de error enviado como parametro (estatus unico).
         *
         * @param codigoError String con el codigo de error a validar.
         * @return String con la descripcion del estatus.
         */
        private String obtenDescripcionEstatusUnico(String codigoError)
        {
          String  estatus       = "";
            try{
                String  tipo_error  = "";
                String  terminacion = "";

                EIGlobal.mensajePorTrace("EmailSender::codigo_error["+codigoError+"]", EIGlobal.NivelLog.DEBUG);
                tipo_error  = codigoError.substring( 0, 4);
                terminacion = codigoError.substring( 4,8 );
                EIGlobal.mensajePorTrace("EmailSender::tipo_error["+tipo_error+"]", EIGlobal.NivelLog.DEBUG);
                EIGlobal.mensajePorTrace("EmailSender::terminacion["+terminacion+"]", EIGlobal.NivelLog.DEBUG);

                if ( !tipo_error.equals( "MANC" ) ){
                    if ( !tipo_error.equals( "PROG" ) ){
                        if ( codigoError.equals( "TRANSITO" ) )
                            estatus = "T"; // en tr�nsito
                        else if ( !terminacion.equals( "0000" ) )
                            estatus = "R"; // rechazado
                        else
                            estatus = "A"; // aceptado
                    } else {
                        if ( !terminacion.equals( "0000" ) )
                            estatus = "R";
                        else
                            estatus = "P"; // programado
                    } // if ( PROG )
                } else{
                    if (tipo_error.equals("ADMU") || tipo_error.equals("MLUS")) {
                         if ( !terminacion.equals( "0000") ){
                             estatus = "";
                         } else {
                             estatus = "X";
                         }
                    } else {
                        estatus = "M"; // mancomunada
                    }
                } // if ( MANC )

                // Estatus
                if      ( estatus.equals( "A" ) ) estatus = "Aceptada";
                else if ( estatus.equals( "R" ) ) estatus = "Rechazada";
                else if ( estatus.equals( "M" ) ){ estatus = "Mancomunada"; estatusSMS=" Mancomunada ";}
                else if ( estatus.equals( "P" ) ) estatus = "Programada";
            //  else if ( estatus.equals( "N" ) ) estatus = "Pendiente";
                else estatus = "No catalogado";

            }
            catch(Exception e){
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);}
                EIGlobal.mensajePorTrace("EmailSender::ESTATUS["+codigoError+"]-->"+estatus, EIGlobal.NivelLog.DEBUG);
            return estatus;
        }

        /**
         * Metodo para envio de notificacion de mantenimiento de datos.
         *
         * @param request Objeto request.
         * @param emailUser Email del usuario.
         * @param emailContrato Email del contrato.
         * @param emailDetails Objeto EmailDetails.
         */
        public void enviaNotificacionMantenimientoDatos( HttpServletRequest request, String emailUser, String emailContrato, EmailDetails emailDetails){

            EIGlobal.mensajePorTrace("<><>[MANTENIMIENTO DE DATOS] -> Inicia Envio de Notificacion<><>", EIGlobal.NivelLog.DEBUG);

            MensajeUSR mensajeUSR = new MensajeUSR();

            //Seteamos este valor para que solo se envie mail
            mensajeUSR.setMedioNotSuperUsuario("B1");
            mensajeUSR.setSuperUsuario(false);

            ServicioMensajesUSR servicioMensajesUSR = new ServicioMensajesUSR();

            HttpSession sess = request.getSession();
            BaseResource session = (BaseResource) sess.getAttribute("session");

            /*Envia el mail a la direccion del usuario*/

            mensajeUSR.setEmailUsuario(emailUser);

            try {

                servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), "", emailDetails.msgTypeConfirmacion().toString(), TITULO_NOTIFICACION_MANTENIMIENTO);

            } catch ( Exception e ) {

                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

            }


            /*Envia el mail a la direccion del contrato*/

            mensajeUSR.setEmailUsuario(emailContrato);

            try {

                servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), "", emailDetails.msgTypeConfirmacion().toString(), TITULO_NOTIFICACION_MANTENIMIENTO);

            } catch ( Exception e ) {

                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

            }


            /*Envia el mail a la direccion mail Nuevo*/

            mensajeUSR.setEmailUsuario(emailDetails.getMailNuevo());

            try {

                servicioMensajesUSR.enviarMensaje(mensajeUSR, session.getUserID8(), "", emailDetails.msgTypeConfirmacion().toString(), TITULO_NOTIFICACION_MANTENIMIENTO);

            } catch ( Exception e ) {

                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);

            }

        }




        /**
         * Metodo maestro el cual inicia la transaccion para el envio de Email's
         * @param detalleCuentasInterb  Vector conteniente de Objetos Container
         * @param numContrato           Numero de Contrato
         * @param descContrato          Descripcion del Contrato
         * @param request               Objeto request
         * @param idLote                Identificador Lote
         * @param session               Objeto BaseResource para obtener datos de sesion del cliente
         * @param numReg                Numero de registros
         */
        public void confirmaEmailInterb(Vector detalleCuentasInterb, String numContrato, String descContrato, HttpServletRequest request, String idLote, BaseResource session, String numReg){
            EIGlobal.mensajePorTrace("EmailSender::confirmaEmailInterb -Inicio-", EIGlobal.NivelLog.DEBUG);
            EmailDetails ed = null;

            try{
                ed = new EmailDetails(detalleCuentasInterb, numContrato, descContrato, idLote);
                ed.setNumeroContrato(numContrato);
                ed.setDescContrato(descContrato);

                //Mail Contrato
                MensajeUSR mensajeUSR = session.getMensajeUSR();
                ServicioMensajesUSR servicioMensajesUSR=new ServicioMensajesUSR();

                String mensajeCorreo = "";

                if(mensajeUSR != null ){

                    mensajeCorreo = ed.msgTypeConfirmacionInterb().toString();

                    setSubject("Notificaci" + (char)243 + "n de alta de cuentas");

                    EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [CatNomInterbRegistro] -> CORREO                [" + mensajeCorreo+"]", EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [CatNomInterbRegistro] -> session.getUserID8()  [" + session.getUserID8()+"]", EIGlobal.NivelLog.DEBUG);
                    EIGlobal.mensajePorTrace("<><><><>[EmailSender] -> Not Contrato [CatNomInterbRegistro] -> EmailContrato         [" + mensajeUSR.getEmailContrato()+"]", EIGlobal.NivelLog.DEBUG);

                    servicioMensajesUSR.enviaEmailContrato(session.getUserID8(), mensajeUSR.getEmailContrato(), mensajeCorreo);

                    }

                ed.setNumRegImportados(Integer.parseInt(numReg));

                sendNotificacion(request, IEnlace.NOT_ALTA_CUENTAS_CATNOMINTERBREGISTRO, ed);

            } catch (Exception e) {
                EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.INFO);
            }
            EIGlobal.mensajePorTrace("EmailSender - sendConfirmacionInterb - Fin", EIGlobal.NivelLog.DEBUG);
        }

    /**
     * Metodo para las notificaciones de Alta de Empleados
     *
     * @param request Objeto request.
     * @param nomArch Nombre del archivo.
     * @param numContrato Numero de contrato.
     * @param descContrato Descripcion del contrato.
     * @param numUsuario Codigo de usuario.
     * @param secuencia Numero de secuencia del archivo procesado.
     * @param registros Cantidad de registros contenidos en el achivo.
     */
    public void confirmaEmailEmpl(HttpServletRequest request, String nomArch, String numContrato, String descContrato, String numUsuario, String secuencia, int registros){
        EIGlobal.mensajePorTrace(" INI-----EmailSender -->Notificacion Alta de Empleados", EIGlobal.NivelLog.INFO);
        String contrato=null;
        String descripcionContrato=null;
        EIGlobal.mensajePorTrace(" EmailSender -->Parametro numContrato-->"+numContrato, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace(" EmailSender -->Parametro descContrato-->"+descContrato, EIGlobal.NivelLog.INFO);

        if(request.getSession().getAttribute("session")!=null){
            EIGlobal.mensajePorTrace(" EmailSender -->confirmaEmailEmpl Toma el valor de la sesion", EIGlobal.NivelLog.INFO);
            BaseResource session = (BaseResource)request.getSession().getAttribute("session");
            contrato=session.getContractNumber();
            descripcionContrato=session.getNombreContrato();
        }
        else{
            contrato=numContrato;
            descripcionContrato=descContrato;
        }

        EIGlobal.mensajePorTrace(" EmailSender -->Parametro Utilizado descContrato-->"+contrato, EIGlobal.NivelLog.INFO);
        EIGlobal.mensajePorTrace(" EmailSender -->Parametro descContrato-->"+descripcionContrato, EIGlobal.NivelLog.INFO);

        EmailDetails ed  = new EmailDetails(nomArch,  contrato, descripcionContrato, secuencia, registros);
        sendNotificacion(request, IEnlace.NOT_ALTA_EMPLEADOS, ed);
        EIGlobal.mensajePorTrace(" FIN-----EmailSender -->Notificacion Alta de Empleados", EIGlobal.NivelLog.INFO);
    }
}