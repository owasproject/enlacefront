package mx.altec.enlace.utilerias;

import com.santander.sbp.security.client.sso.listeners.ConnectionListener;

/**
 *  Clase Listener para detectar estatus de la conexion de la API Flame con Security Service de Flame
 *
 * @author Ricardo Martinez Hernandez
 * @version 1.0 Agosto 05, 2016
 */
public class FlameListener implements ConnectionListener {
	/**
	 * Varible connAlive para guardar el estatus de la conexion
	 */
	private transient boolean connAlive = false;
	
	/**
	 * Metodo para conocer el valor de <code>connAlive</code> y el estatus de la conexion
	 * 
	 * @return <code>true</code> si la conexi�n esta establecida, <code>false<code> si no ha conexon
	 */
	public boolean isConnAlive() {
		return connAlive;
	}

	/**
	 * Metodo que actualiza el valor de <code>connAlive</code> a <code>false</code> cuando se detecta una desconexion
	 * 
	 */
	@Override
	public void onConnectionLost() {
		connAlive = false;
	}

	/**
	 * Metodo que actualiza el valor de <code>connAlive</code> a <code>true</code> cuando se detecta un restablecimiento de la conexion
	 * 
	 */
	@Override
	public void onConnectionOpened() {
		connAlive = true;
	}
}
