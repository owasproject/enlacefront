package mx.altec.enlace.utilerias;

import java.net.*;

public class HttpTimeoutFactory implements URLStreamHandlerFactory
{
	int fiTimeoutVal;
	public HttpTimeoutFactory(int iT) { fiTimeoutVal = iT; }
	public URLStreamHandler createURLStreamHandler(String str)
	{
		return new HttpTimeoutHandler(fiTimeoutVal);
	}

}

