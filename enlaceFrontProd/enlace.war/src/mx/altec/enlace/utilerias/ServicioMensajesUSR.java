package mx.altec.enlace.utilerias;

import java.rmi.RemoteException;
import java.util.GregorianCalendar;

import mx.altec.enlace.beans.EnvioEmailBean;
import mx.altec.enlace.beans.MensajeUSR;

import mx.altec.enlace.cliente.ws.mail.CommonEmailWSResponseDTO;
import mx.altec.enlace.cliente.ws.mail.CommonEnvioEmailServiceImpl;
import mx.altec.enlace.cliente.ws.sms.EnviarMensajeProxy;


	public class ServicioMensajesUSR {
		private static EnviarMensajeProxy enviarMensajeProxy = null;
		static {
			enviarMensajeProxy= new EnviarMensajeProxy();
	}

	public void  enviarMensaje(MensajeUSR bean,String numCliente,String mensajeCel,String mensajeEmail,String subject)throws Exception{
		String correroRemitente=Global.CORREO_REMITENTE;//"notificaciones@notificaciones.santander.com.mx";
		String nombreRemitente=Global.NOMBRE_REMITENTE;//"Grupo Financiero Santander";
		EIGlobal.mensajePorTrace("--------------- Datos para envio notificacion ----------------------", EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("--------------- correroRemitente--->"+correroRemitente, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("--------------- nombreRemitente---->"+nombreRemitente, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EnviarUsuario ->" + bean.isEnviarUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("SuperUsuario ->" + bean.isSuperUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EnviarSuperUsuario ->" + bean.isEnviarSuperUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EmailUsuario ->" + bean.getEmailUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("EmailSuperUsuario ->" + bean.getEmailSuperUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("LadaSuperUsuario ->" + bean.getLadaSuperUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("NoCelularSuperUsuario ->" + bean.getNoCelularSuperUsuario(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("CompCelularSuperUsuario ->" + bean.getCompCelularSuperUsuario(), EIGlobal.NivelLog.DEBUG);
	//Si la configuracion es
		if(bean.isEnviarUsuario() )
		{
			EIGlobal.mensajePorTrace("---------------->SE ENVIARA MENSAJE POR CORREO AL USUARIO<----------------", EIGlobal.NivelLog.DEBUG);
			if( bean.getEmailUsuario() !=null && bean.getEmailUsuario().trim().length()>0)
			enviaEmail(numCliente,correroRemitente,bean.getEmailUsuario(),nombreRemitente,"",mensajeEmail,subject);
			else
			EIGlobal.mensajePorTrace("--->EL USUARIO NO CUENTA CON CORREO,NO ENVIA NOTIFICACION<----", EIGlobal.NivelLog.DEBUG);
			EIGlobal.mensajePorTrace("-----------------------------------------------------------------------", EIGlobal.NivelLog.DEBUG);
		}
		if(bean.isEnviarSuperUsuario())
		{
			EIGlobal.mensajePorTrace("---------------->SE ENVIARA MENSAJE SMS O/Y EMAIL POR  AL SUPER USUARIO<----------------", EIGlobal.NivelLog.DEBUG);
			if(bean.isEmail()){
				if( bean.getEmailSuperUsuario() !=null && bean.getEmailSuperUsuario().trim().length()>0)
					enviaEmail(numCliente,correroRemitente,bean.getEmailSuperUsuario(),nombreRemitente,"",mensajeEmail,subject);
				else
				 EIGlobal.mensajePorTrace("--->EL SUPER USUARIO NO CUENTA CON CORREO,NO ENVIA NOTIFICACION<----", EIGlobal.NivelLog.DEBUG);
			}
		    if(bean.isSms()){
		    	if( bean.getNoCelularSuperUsuario() !=null && bean.getNoCelularSuperUsuario().trim().length()>0
		    					&& bean.getLadaSuperUsuario() !=null && bean.getLadaSuperUsuario().trim().length()>0)
		    		enviaSms(numCliente,bean.getLadaSuperUsuario().concat(bean.getNoCelularSuperUsuario()),bean.getCompCelularSuperUsuario(),mensajeCel);
		    	else
		    		EIGlobal.mensajePorTrace("--->EL SUPER USUARIO NO TIENE UN TELEFONO,NO ENVIA NOTIFICACION<----", EIGlobal.NivelLog.DEBUG);
		    }
		    EIGlobal.mensajePorTrace("-----------------------------------------------------------------------", EIGlobal.NivelLog.DEBUG);
		}

		}

	private boolean  enviaEmail(String numCliente,String correoFrom,String correoTo,String fromName,String toName,String mensaje,String subject)
	{
		EIGlobal.mensajePorTrace("<---------------->Correo To: " + correoTo, EIGlobal.NivelLog.INFO);
		EnvioEmailBean envioEmailBean = new EnvioEmailBean();
		CommonEnvioEmailServiceImpl envioEmailServiceImpl = new CommonEnvioEmailServiceImpl();
		CommonEmailWSResponseDTO resultado = new CommonEmailWSResponseDTO();
		String fecha = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa th:tm:ts");
		//requeridos -> template,appId,from,to,subject,msgBody

		envioEmailBean.setAppId("ENLC"); //obligatorio
		envioEmailBean.setAttachmentName("");
		envioEmailBean.setBounceAddress("");
		envioEmailBean.setCentrCostos("");
		envioEmailBean.setCodigoCliente(numCliente);
		envioEmailBean.setFrom(correoFrom);  //obligatorio
		envioEmailBean.setFromName(fromName);
		envioEmailBean.setMsgBody(mensaje);    //obligatorio
		envioEmailBean.setRequestId("");
		envioEmailBean.setSentDate(fecha);
		envioEmailBean.setSubject(subject);     //obligatorio
		//envioEmailBean.setTemplate("ENLC01"); 
		
		EIGlobal.mensajePorTrace("<---------------->Entro normal: ", EIGlobal.NivelLog.INFO);
		envioEmailBean.setTemplate("ENLC01");  //obligatorio		
		
		EIGlobal.mensajePorTrace("<---------------->Template seleccionado: " + envioEmailBean.getTemplate(), EIGlobal.NivelLog.INFO);
		
		envioEmailBean.setTo(correoTo);        //obligatorio
		envioEmailBean.setToName(toName);
		envioEmailBean.setURLAttachment("");
		EIGlobal.mensajePorTrace("<---------------->ENVIO DE MAIL <----------------->", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("correoFrom ->" +correoFrom, EIGlobal.NivelLog.DEBUG);
		//EIGlobal.mensajePorTrace("mensaje ->" +mensaje, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("subject ->" +subject, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("correoTo ->" +correoTo, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("toName ->" +toName, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("fecha ->" +fecha, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("fromName ->" +fromName, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("numCliente ->" +numCliente, EIGlobal.NivelLog.DEBUG);
		resultado = envioEmailServiceImpl.enviaEmail(envioEmailBean);
		EIGlobal.mensajePorTrace("<------------------------------------------>", EIGlobal.NivelLog.DEBUG);

		return true;

	}

	private boolean  enviaSms(String numCliente,String numCel,String compania,String mensaje)
	{

		String resultado = "";

	try{


		mensaje=mensaje.toUpperCase()+" EL "+EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa th:tm");
		mensaje=mensaje.replaceAll("  ", " ");
		int longMsj=mensaje.length();

		if(longMsj>150){//150 longitud maxima de un sms
			int diferencia=longMsj-150;
			mensaje=mensaje.substring(diferencia,longMsj);
		}

		EIGlobal.mensajePorTrace("<---------------->ENVIO DE SMS () <----------------->", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Mensaje ["+mensaje+"]", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("Entra a enviar SMS" , EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("numCliente ->" +numCliente, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("numCel ->" +numCel, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("compania ->" +compania, EIGlobal.NivelLog.DEBUG);


		String fecha = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa th:tm:ts");

		resultado = enviarMensajeProxy.enviarMensaje(numCel, compania, numCliente, "001", fecha, mensaje);

		EIGlobal.mensajePorTrace("--->resultado ->" +resultado, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("<---------------------------------------->", EIGlobal.NivelLog.DEBUG);
	} catch (RemoteException e) {

		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);

	} catch (Exception e) {

		EIGlobal.mensajePorTrace(new Formateador().formatea(e), EIGlobal.NivelLog.ERROR);
	}
		return true;

	}

	public boolean  enviaEmailContrato(String numCliente, String correoTo, String mensaje) {
		EnvioEmailBean envioEmailBean = new EnvioEmailBean();
		CommonEnvioEmailServiceImpl envioEmailServiceImpl = new CommonEnvioEmailServiceImpl();
		CommonEmailWSResponseDTO resultado = new CommonEmailWSResponseDTO();
		String fecha = EIGlobal.formatoFecha(new GregorianCalendar(), "dd-mm-aaaa th:tm:ts");
		String correoFrom = Global.CORREO_REMITENTE;	//"notificaciones@notificaciones.santander.com.mx";
		String fromName = Global.NOMBRE_REMITENTE;	//"Grupo Financiero Santander";
		String subject = "Notificaci" + (char)243 + "n a usuario";
		//requeridos -> template,appId,from,to,subject,msgBody

		envioEmailBean.setAppId("ENLC"); //obligatorio
		envioEmailBean.setAttachmentName("");
		envioEmailBean.setBounceAddress("");
		envioEmailBean.setCentrCostos("");
		envioEmailBean.setCodigoCliente(numCliente);
		envioEmailBean.setFrom(correoFrom);  //obligatorio
		envioEmailBean.setFromName(fromName);
		envioEmailBean.setMsgBody(mensaje);    //obligatorio
		envioEmailBean.setRequestId("");
		envioEmailBean.setSentDate(fecha);
		envioEmailBean.setSubject(subject);     //obligatorio
		envioEmailBean.setTemplate("ENLC01");  //obligatorio
		envioEmailBean.setTo(correoTo);        //obligatorio
		envioEmailBean.setToName("");
		envioEmailBean.setURLAttachment("");
		EIGlobal.mensajePorTrace("<---------------->ENVIO DE MAIL CONTRATO <----------------->", EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("correoFrom ->" +correoFrom, EIGlobal.NivelLog.DEBUG);
		//EIGlobal.mensajePorTrace("mensaje ->" +mensaje, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("subject ->" +subject, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("correoTo ->" +correoTo, EIGlobal.NivelLog.INFO);
		EIGlobal.mensajePorTrace("fecha ->" +fecha, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("fromName ->" +fromName, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("numCliente ->" +numCliente, EIGlobal.NivelLog.DEBUG);
		resultado = envioEmailServiceImpl.enviaEmail(envioEmailBean);
		EIGlobal.mensajePorTrace("<------------------------------------------>", EIGlobal.NivelLog.DEBUG);

		return true;

	}

}