package mx.altec.enlace.utilerias;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class GzipArchivo {

	/*EVERIS
	 *Metodo de compresion de archivo al formato gzip
	 *Entrada: Cadena con Direccion y nombre de archivo a comprimir
	 *Salida: Booleano: True correcto, False incorrecto
	 *Jose Antonio Rodriguez Alba
	 *24 ABRIL 2007
	 **/

	public static boolean zipFile(String fileName, String outFileGZ)
	throws IOException
	{
		File archivoOriginal = new File(fileName);
		if (!archivoOriginal.exists())
			{
			System.out.println("\n El archivo" +fileName+ " no existe");
			return false;
			}
		else
			{
			String outFile = outFileGZ + ".gz";
	        FileInputStream in = new FileInputStream(fileName);
	        BufferedInputStream bin = new BufferedInputStream(in);
	        FileOutputStream out = new FileOutputStream( outFile );
	        GZIPOutputStream zipOut = new GZIPOutputStream(out);
	        BufferedOutputStream bout = new BufferedOutputStream(zipOut);
	        int chunk;
	        while ((chunk = bin.read()) != -1)
	        	{
	            bout.write(chunk);
	            }
			bout.close();
			zipOut.close();
			out.close();
			return true;
			}

	}





}




