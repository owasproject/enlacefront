package mx.altec.enlace.utilerias;



public class Ps7 {
	
	public static String armaCabeceraPS7
    //parametros
    (
        String canal,
        String servicio, //Servicio
        String espejo, //Espejo
        int    longitud, //Longitud
        String tecla  //Tecla funcion

    )
	{


			if (canal.equals("MQ")){
			StringBuffer cabeza =new StringBuffer();
			cabeza
	            .append("    ")     //terminal logico.
	            .append(Global.ADMUSR_MQ_USUARIO.trim())               	 
	            //.append(rellenar(servicio,4,' ','I'))        // 4  C�digo transacci�n
	            //.append(" ")                                   // Espacio
	            .append(servicio.trim())                       //C�digo transacci�n
	            .append(rellenar(""+(longitud+32),4,'0','I'))  // 4  Longitud del mensaje de entrada (cabecera + contenido)
	            .append("1")                              // 1  1=Altamira gestina el commit, 0=Altamira no gestiona el commit
	            .append("00001")                          // 5  N�mero de secuencia
        		.append("1")                              // 1  1=incorporar datos 2=autorizaci�n 4=reanudar conversaci�n 5=contin�a conversaci�n 6=autorizaci�n de transacci�n en conversaci�n
				.append("O")                              // 1  O=on line F=off line
				.append(rellenar(tecla,2,'0','I'))          // 2  00=enter 01..12=Fnn 13..=ShiftF1..F24 99=Clear
				.append("N")                              // 1  N=no impresora S=s� impresora
				.append("2");                             // 1  2=formato @DC 1=mapas @PA y@LI

			return cabeza.toString();
			}else{
				StringBuffer cabeza =new StringBuffer();
			  cabeza
				.append(rellenar(espejo,4,' ','I'))          // 4  C�digo espejo
				.append("PUTT") 						  // 4  Terminal l�gico  "PUXQ"     "PP06"
				.append("CSOPNET ")                       // 8  Usuario CICS     "CSOPNET " "B244661 "
				.append(rellenar(servicio,4,' ','I'))          // 4  C�digo transacci�n
				.append(rellenar(""+(longitud+32),4,'0','I'))  // 4  Longitud del mensaje de entrada (cabecera + contenido)
				.append("1")                              // 1  1=Altamira gestina el commit, 0=Altamira no gestiona el commit
				.append("12345")                          // 5  N�mero de secuencia
				.append("1")                              // 1  1=incorporar datos 2=autorizaci�n 4=reanudar conversaci�n 5=contin�a conversaci�n 6=autorizaci�n de transacci�n en conversaci�n
				.append("O")                              // 1  O=on line F=off line
				.append(rellenar(tecla,2,'0','I'))          // 2  00=enter 01..12=Fnn 13..=ShiftF1..F24 99=Clear
				.append("N")                              // 1  N=no impresora S=s� impresora
				.append("2");                              // 1  2=formato @DC 1=mapas @PA y@LI
			return cabeza.toString();
			}

	}
	
	public static String rellenar(String cad, int lon) {
		if (cad == null) {
			cad = "";
		}
		return rellenar(cad, lon, ' ', 'D');
	}
	
	public static String rellenar
    //parametros
    	(
	        String cad, //Cadena a rellenar
	        int    lon, //Longitud a rellenar
	        char   rel, //Caracter de relleno
	        char   tip  //Tipo de relleno {I=Izq,D=Der}
	    )
	{
	    if( cad.length()>lon ) return cad.substring(0,lon);
	    if( tip!='I' && tip!='D' ) tip = 'I';
	    String aux = "";
	    if( tip=='D' ) aux = cad;
	    for (int i=0; i<(lon-cad.length()); i++)
	       aux += ""+ rel;
	    if( tip=='I' ) aux += ""+ cad;
	    return aux;
	}
		
	

}
