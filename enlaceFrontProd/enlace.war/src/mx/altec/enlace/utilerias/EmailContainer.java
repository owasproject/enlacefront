package mx.altec.enlace.utilerias;

import java.io.Serializable;

/**
 *  Clase con las propiedades que forman el detalle de las cuentas a autorizar
 *
 * @author Gerardo Arciniega Espinosa
 * @version 1.0 Feb 26, 2007
 */
public class EmailContainer implements Serializable{

	/**
	 * Numero de Version Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero del contrato de la cuenta
	 */
	private String titular = null;

	/**
	 * Numero de la cuenta
	 */
	private String numCuenta = null;

	/**
	 * Clase de la cuenta
	 */
	private String claseCuenta = null;

	/**
	 * Fecha del registro de dicha cuenta
	 */
	private String fechRegistro = null;

	/**
	 * Origen de la alta de dicha cuenta
	 */
	private String origenAlta = null;

	/**
	 * Folio de lote de carga
	 */
	private String idLote = null;

	/**
	 * Obtener el valor de claseCuenta.
     *
     * @return claseCuenta valor asignado.
	 */
	public String getClaseCuenta() {
		return claseCuenta;
	}

	/**
     * Asignar un valor a claseCuenta.
     *
     * @param claseCuenta Valor a asignar.
     */
	public void setClaseCuenta(String claseCuenta) {
		this.claseCuenta = claseCuenta;
	}

	/**
	 * Obtener el valor de fechRegistro.
     *
     * @return fechRegistro valor asignado.
	 */
	public String getFechRegistro() {
		return fechRegistro;
	}

	/**
     * Asignar un valor a fechRegistro.
     *
     * @param fechRegistro Valor a asignar.
     */
	public void setFechRegistro(String fechRegistro) {
		this.fechRegistro = fechRegistro;
	}

	/**
	 * Obtener el valor de titular.
     *
     * @return titular valor asignado.
	 */
	public String getTitular() {
		return titular;
	}

	/**
     * Asignar un valor a titular.
     *
     * @param titular Valor a asignar.
     */
	public void setTitular(String numContrato) {
		this.titular = numContrato;
	}

	/**
	 * Obtener el valor de numCuenta.
     *
     * @return numCuenta valor asignado.
	 */
	public String getNumCuenta() {
		return numCuenta;
	}

	/**
     * Asignar un valor a numCuenta.
     *
     * @param numCuenta Valor a asignar.
     */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	/**
	 * Obtener el valor de origenAlta.
     *
     * @return origenAlta valor asignado.
	 */
	public String getOrigenAlta() {
		return origenAlta;
	}

	/**
     * Asignar un valor a origenAlta.
     *
     * @param origenAlta Valor a asignar.
     */
	public void setOrigenAlta(String origenAlta) {
		this.origenAlta = origenAlta;
	}

	/**
	 * Obtener el valor de idLote.
     *
     * @return idLote valor asignado.
	 */
	public String getIdLote() {
		return idLote;
	}

	/**
     * Asignar un valor a idLote.
     *
     * @param idLote Valor a asignar.
     */
	public void setIdLote(String idLote) {
		this.idLote = idLote;
	}




}
