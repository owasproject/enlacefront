package mx.altec.enlace.utilerias;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.altec.enlace.beans.ImagenDTO;
import mx.altec.enlace.beans.PreguntaDTO;
import mx.altec.enlace.beans.RSABeanAUX;
import mx.altec.enlace.bita.BitaAdminBean;
import mx.altec.enlace.bita.BitaConstants;
import mx.altec.enlace.bita.BitaHandler;
import mx.altec.enlace.bita.BitaHelper;
import mx.altec.enlace.bita.BitaHelperImpl;
import mx.altec.enlace.bita.BitaTCTBean;
import mx.altec.enlace.bita.BitaTransacBean;
import mx.altec.enlace.bo.BaseResource;
import mx.altec.enlace.bo.BusinessException;
import mx.altec.enlace.dao.EmailDetails;
import mx.altec.enlace.dao.RSADAO;
import mx.altec.enlace.servicios.ServicioTux;
import mx.altec.enlace.servlets.LongLivedCookie;
import mx.isban.rsa.aa.ws.ChallengeQuestionGroup;
import mx.isban.rsa.aa.ws.ChallengeQuestionManagementResponsePayload;
import mx.isban.rsa.aa.ws.DeviceRequest;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.conector.config.ConectorConfig;
import mx.isban.rsa.conector.servicios.ServiciosAA;
import mx.isban.rsa.stu.ws.Image;

import com.passmarksecurity.PassMarkDeviceSupportLite;

public class UtilidadesRSA  {

	/** Clave para bitacora challenge */
	public static final String CVE_CHALLENGE = "CHAL";
	/** Descripcion para bitacora challenge */
	public static final String DES_CHALLENGE = "Respuesta challenge de RSA";
	/** Clave para bitacora deny */
	public static final String CVE_DENY = "DENY";
	/** Descripcion para bitacora deny */
	public static final String DES_DENY = "Respuesta deny de RSA";
	/** Clave para bitacora review */
	public static final String CVE_REVIEW = "REVW";
	/** Descripcion para bitacora review */
	public static final String DES_REVIEW = "Respuesta review de RSA";
	/** Descripcion para aceptado */
	public static final String ACEPTADO = "A";
	/** Descripcion para aceptado */
	public static final String RECHAZADO = "R";
	/** Version de RSA */
	public static String VERSION_RSA = "RSA6";

	/**
	 * Genrera Bean para RSA
	 * @param request : request
	 * @param sessionID : sessionID
	 * @param transactionID : transactionID
	 * @param usuario : usuario
	 * @return RSABean : bean de rsa
	 */
	public RSABean generaBean (HttpServletRequest request, String sessionID,
			String transactionID, String usuario ) {

		try {
			if(RSADAO.consultaVersionRSA() == 1) {
				UtilidadesRSA.VERSION_RSA = "RSA7";
			} else {
				UtilidadesRSA.VERSION_RSA = "RSA6";
			}
		} catch (SQLException e1) {
			EIGlobal.mensajePorTrace("-->BaseServlet error consulta gfi rsa " + e1.getMessage() ,EIGlobal.NivelLog.ERROR);
		}

		RSABean rsaBean = new RSABean();

		// Datos para setear
    	final String devicePrint = request.getSession().getAttribute("valorDeviceP") != null ?
    			request.getSession().getAttribute("valorDeviceP").toString() : "";
    	final String fsoCookie = request.getSession().getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO) != null ?
    			request.getSession().getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO).toString() : UtilidadesRSA.getCookieValue(request);
    	final String cookie = UtilidadesRSA.getCookieValue(request);
    	final String userCountry = request.getLocale().getCountry();
		final String httpAcceptChars = request.getHeader("Accept-Charset") != null ? request.getHeader("Accept-Charset") : "UTF-8";
		final String httpAcceptEncoding = request.getHeader("Accept-Encoding");
		final String httpAcceptLanguage = request.getHeader("Accept-Language");
		final String httpReferrer = request.getHeader("Referer");
		final String userAgent = request.getHeader("user-agent");
		final String nullString = null;
		String httpAccept = request.getHeader("Accept") == null ? ""
				: request.getHeader("Accept");
		String ipAddress = request.getHeader("iv-remote-address") == null ? ""
				: request.getHeader("iv-remote-address");

		if ("".equals(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}
		if(httpAccept.equals(nullString) || "".equals(httpAccept)) {
			httpAccept = request.getHeader("accept");
		}
		//device requestuest
    	DeviceRequest deviceRequest = new DeviceRequest();

    	deviceRequest.setDevicePrint(devicePrint);
    	deviceRequest.setDeviceTokenCookie(cookie);
    	deviceRequest.setDeviceTokenFSO(fsoCookie);
    	deviceRequest.setHttpAccept(httpAccept);
    	deviceRequest.setHttpAcceptChars(httpAcceptChars);
    	deviceRequest.setHttpAcceptEncoding(httpAcceptEncoding);
    	deviceRequest.setHttpAcceptLanguage(httpAcceptLanguage);
    	deviceRequest.setHttpReferrer(httpReferrer);
    	deviceRequest.setIpAddress(ipAddress);
    	deviceRequest.setUserAgent(userAgent);


    	//rsaBean.setOrgName("Enlace");
		rsaBean.setOrgName(Global.ID_CANAL);
    	rsaBean.setAuthSessionId(sessionID);
    	rsaBean.setAuthTransactionId(transactionID);
    	rsaBean.setDeviceRequest(deviceRequest);
    	rsaBean.setIdSession(request.getSession().getId().toString());
    	rsaBean.setUserCountry(userCountry);
    	//rsaBean.setUserLanguage(request.getLocale().getLanguage());
    	rsaBean.setUserLanguage("ES");
    	rsaBean.setUserName(usuario);

    	return rsaBean;
	}

	/**
	 * Obtiene las 10 imagenes aleatorias
	 * @param rsaBean : bean
	 * @param request : request
	 * @param response : response
	 * @return List<ImagenDTO> : list
	 * @throws BusinessException : exception
	 */

	public List<ImagenDTO> obtenerImagenes (RSABean rsaBean, HttpServletRequest request,
			HttpServletResponse response) throws BusinessException {

		String deviceTokenFSO = "";
	    String deviceTokenCookie = "";

	    RSABeanAUX rsaBeanAUX = new RSABeanAUX();
	    ServiciosSTUResponse res = new ServiciosSTUResponse();
		//SiteToUserAuth siteToUser = new SiteToUserAuth();

		rsaBeanAUX.setRsaBean(rsaBean);
		rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.CONSULTA_IMAGENES);

		//res = siteToUser.browseImages(rsaBean);

		RSAFII rsa = new RSAFII();
		try {
			res = (ServiciosSTUResponse) rsa.ejecutaMetodosRSA7(rsaBeanAUX );
		} catch (ExecutionException e) {
			EIGlobal.mensajePorTrace("No hay conexion -RSA-:",EIGlobal.NivelLog.INFO);
		} catch (InterruptedException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
		}

		deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO();
	    deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie();

	    UtilidadesRSA.crearCookie(response, deviceTokenCookie);
		request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		Image [] imagenes = res.getSiteToUserBrowseResponse().getBrowsableImages();

		List<ImagenDTO> lista = new ArrayList<ImagenDTO>();
		for(int i = 0 ; i < 10; i++) {
			ImagenDTO dto = new ImagenDTO();
			dto.setImagen(imagenes[i].getData());
			dto.setIdImagen(imagenes[i].getPath());

			lista.add(dto);
		}

		return lista;
	}

	/**
	 * Obtiene las preguntas
	 * @param rsaBean : bean de rsa
	 * @param request : request
	 * @param response : response
	 * @return List<PreguntaDTO>: Lista de preguntas
	 * @throws BusinessException : exception
	 */
	public List<PreguntaDTO> obtenerPreguntas (RSABean rsaBean, HttpServletRequest request,
			HttpServletResponse response) throws BusinessException {

		String deviceTokenFSO = "";
	    String deviceTokenCookie = "";

	    RSABeanAUX rsaBeanAUX = new RSABeanAUX();
	    ServiciosAAResponse res = new ServiciosAAResponse();
		//SiteToUserAuth siteToUser = new SiteToUserAuth();

		rsaBeanAUX.setRsaBean(rsaBean);
		rsaBeanAUX.setValorMetodo(RSAFII.valorMetodo.CONSULTA_PREGUNTAS);

		//res = siteToUser.browseQuestion(rsaBean);

		RSAFII rsa = new RSAFII();
		try {
			res = (ServiciosAAResponse) rsa.ejecutaMetodosRSA7(rsaBeanAUX);
		} catch (ExecutionException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
		} catch (InterruptedException e) {
			EIGlobal.mensajePorTrace("No hay conexion con RSA",EIGlobal.NivelLog.INFO);
		}

		deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO();
	    deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie();

	    UtilidadesRSA.crearCookie(response, deviceTokenCookie);
		request.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		ChallengeQuestionManagementResponsePayload  question
			= new ChallengeQuestionManagementResponsePayload();
		question = res.getChallengeQuestionPayload();
		ChallengeQuestionGroup[] questions = question.getBrowsableChallQuesGroupList();

		List<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();

		HashSet<Integer> tmp =new HashSet<Integer>();
		while (tmp.size()<5) {
			Integer al = Integer.valueOf(new java.util.Random().nextInt(10));
			tmp.add(al);
		}

		Iterator<Integer> it=tmp.iterator();
		for(int i = 0 ; i < 5; i++) {
			PreguntaDTO pregunta = new PreguntaDTO();
			int ale1 = generaNumero(questions.length);
			int ale2 = (Integer)it.next();

			pregunta.setPreguntaTexto(questions[ale1].getChallengeQuestion(ale2).
					getQuestionText());
			pregunta.setPreguntaID(questions[ale1].getChallengeQuestion(ale2).
					getQuestionId());
			listaPreguntas.add(pregunta);
		}

		return listaPreguntas;
	}

	/**
	 * inicializa contexto rsa
	 */
	public void iniciarConexionRSA() {
		HashMap<String, String> hash;
		hash = new HashMap<String, String>();
		EIGlobal.mensajePorTrace("Global.RSA_CALLER_CREDENTIAL_AA " + Global.RSA_CALLER_CREDENTIAL_AA, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Global.RSA_CALLER_ID_AA " + Global.RSA_CALLER_ID_AA, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Global.RSA_WEBSERVICE_AA " + Global.RSA_WEBSERVICE_AA, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Global.RSA_CALLER_CREDENTIAL_STU " + Global.RSA_CALLER_CREDENTIAL_STU, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Global.RSA_CALLER_ID_STU " + Global.RSA_CALLER_ID_STU, EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Global.RSA_WEBSERVICE_STU " + Global.RSA_WEBSERVICE_STU, EIGlobal.NivelLog.DEBUG);
		hash.put("RSA_CALLER_CREDENTIAL", Global.RSA_CALLER_CREDENTIAL);
		hash.put("RSA_CALLER_ID", Global.RSA_CALLER_ID);
		hash.put("RSA_WEBSERVICE", Global.RSA_WEBSERVICE);
		hash.put("RSA_CALLER_CREDENTIAL_AA", Global.RSA_CALLER_CREDENTIAL_AA);
		hash.put("RSA_CALLER_ID_AA", Global.RSA_CALLER_ID_AA);
		hash.put("RSA_WEBSERVICE_AA", Global.RSA_WEBSERVICE_AA);
		hash.put("RSA_CALLER_CREDENTIAL_STU", Global.RSA_CALLER_CREDENTIAL_STU);
		hash.put("RSA_CALLER_ID_STU", Global.RSA_CALLER_ID_STU);
		hash.put("RSA_WEBSERVICE_STU", Global.RSA_WEBSERVICE_STU);
		hash.put("RSA_WS_TIMEOUT", Global.RSA_WS_TIMEOUT);
		hash.put("LOG_LEVEL", "ALL");
		hash.put("ID_CANAL", Global.ID_CANAL); //


		ConectorConfig.init(hash);

	}

	/**
	 * Metodo que regresa un numero aleatorio
	 * @param limite : limite superior
	 * @return int : numeor aleatorio
	 */
	private int generaNumero(int limite) {
		Random rand = new Random();
	    int num = rand.nextInt(limite);

		return num;
	}

	/**
	 * crea cookie
	 * @param response : HttpServletResponse
	 * @param valor : String valor
	 */
	public static void crearCookie(HttpServletResponse response, String valor) {

		Cookie cookieToken = new LongLivedCookie(valor);
		response.addCookie(cookieToken);
	}

	/**
	 * optiene el valor de la cookie
	 * @param request : HttpServletRequest
	 * @return : String
	 */
	public static String getCookieValue( HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		String stringNull = null;
		for (int i = 0; !cookies.equals(stringNull) && i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if ("PMData".equals(cookie.getName())) {
				return (cookie.getValue());
			}
		}
		return null;
	}

	/**
	 * Metodo que ejecuta la funcion analyze
	 * @param bean : bean de datos
	 * @return SiteToUserResponse : SiteToUserResponse
	 */
	public ServiciosAAResponse ejecutaAnalyze(RSABean bean, HttpServletRequest req) {

		final ServiciosAA analyze = new ServiciosAA();
		ServiciosAAResponse  resp = new ServiciosAAResponse();

		String deviceTokenFSO = "";

		try {
		    resp = analyze.analyzeSiteToUser(bean, VERSION_RSA);

		    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
		    req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		} catch (NullPointerException e) {
			EIGlobal.mensajePorTrace("Error generado por el API de RSA :::: " +
					e.getCause() +
					e.getLocalizedMessage() ,EIGlobal.NivelLog.INFO);
		}

	    return  resp;


	}

	/**
	 * Metodo que ejecuta la funcion analyze
	 * @param bean : bean de datos
	 * @return SiteToUserResponse : SiteToUserResponse
	 * @throws mx.isban.CmpRSA.Exception.BusinessException
	 */
	public ServiciosAAResponse ejecutaAnalyzePostLogin(RSABean bean, HttpServletRequest req, HttpServletResponse res) throws NullPointerException {

		final ServiciosAA analyze = new ServiciosAA();
		ServiciosAAResponse  resp = new ServiciosAAResponse();

		String deviceTokenFSO = "";
		String deviceTokenCookie = "";

		resp = analyze.analyzePostLogin(bean, VERSION_RSA);

	    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
	    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();

	    UtilidadesRSA.crearCookie(res, deviceTokenCookie);
	    req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);

		return  resp;


	}
	/**
	 * Metodo para notificar via correo electronico con evento Review y Deny
	 * @param request : request
	 * @param evento : evento
	 */
      public void mandarCorreo(String evento, HttpServletRequest req) {

            HttpSession ses = req.getSession();
            BaseResource session = (BaseResource) ses.getAttribute("session");
            EmailSender emailSender = new EmailSender();
            EmailDetails emailDetails = new EmailDetails();

            emailDetails.setUsuario(session.getUserID8().toString());
            emailDetails.setNumeroContrato(session.getContractNumber().toString());
            emailDetails.setRazonSocial(session.getNombreContrato().toString());

            emailSender.sendNotificacion(req,IEnlace.REVIEW_RSA_CLIENTE, emailDetails);

            if("DENY".equals(evento)){
                  //correo deny analista
                  emailSender.sendNotificacion(req,IEnlace.DENY_RSA_ANALISTA, emailDetails);

            }else if("REVIEW".equals(evento) || "LOCKOUT".equals(evento)){
                  //correo review analista
                  emailSender.sendNotificacion(req,IEnlace.REVIEW_RSA_ANALISTA, emailDetails);
            }
            //correo usuario
      }


	/**
	 * Metodo para bitacorizar en tabla TCT_BITACORA
	 * @param request : request
	 * @param bitaConstansValue : bitaConstansValue
	 * @param referencia : referencia
	 * @param codError : codigo de error
	 * @param concepto : concepto
	 */
	public static void bitacoraTCT(HttpServletRequest request, String bitaConstansValue,
			int referencia, String codError, String concepto) {
		try{
			EIGlobal.mensajePorTrace(">>>>> bitacotaTCT: dato: " + bitaConstansValue,	EIGlobal.NivelLog.DEBUG);
			HttpSession ses = request.getSession();
			BaseResource session = (BaseResource) ses.getAttribute("session");

		  	BitaHelper bh = new BitaHelperImpl(request, session, request.getSession());
		  	BitaTCTBean bean = new BitaTCTBean ();
		  	bean = bh.llenarBeanTCT(bean);
		  	bean.setReferencia(referencia);
		  	bean.setTipoOperacion(bitaConstansValue);
		  	bean.setCodError(codError);
		  	bean.setConcepto(concepto);

		  	BitaHandler.getInstance().insertBitaTCT(bean);
    	}catch(SQLException e){
    		EIGlobal.mensajePorTrace("Error en bitacotaTCT : " + e,
					EIGlobal.NivelLog.DEBUG);
   		}catch(Exception e){
   			EIGlobal.mensajePorTrace("Error en bitacotaTCT : " + e,
					EIGlobal.NivelLog.DEBUG);
   		}
	}


	/**
	 * Bitacoriza tabla EWEB_TRAN_BITACORA
	 * @param request : request
	 * @param numBit : numBit
	 */
	public static  void bitacorizaTransac(HttpServletRequest request, String numBit, String estatus,int referencia) {
		HttpSession ses = request.getSession();
		BaseResource session = (BaseResource) ses.getAttribute("session");

		if ("ON".equals(Global.USAR_BITACORAS.trim())){
			BitaHelper bh = new BitaHelperImpl(request, session, ses);
			BitaTransacBean bt = new BitaTransacBean();

			try {
			bh.incrementaFolioFlujo(numBit);

			bt = (BitaTransacBean)bh.llenarBean(bt);
			bt.setIdToken(session.getToken().getSerialNumber());
			bt.setIdFlujo(numBit);
			bt.setNumBit(numBit.concat("01"));
			bt.setContrato(session.getContractNumber().toString());
			Date hoy = new Date();
			bt.setFechaAplicacion(hoy);
			bt.setIdSesion(bt.getIdSesion().replace("-", ""));
			bt.setIdSesion(bt.getIdSesion().replace("_", ""));
			bt.setEstatus(estatus);

			String codError="RSA00000";


			bt.setReferencia(referencia);
			bt.setIdErr(codError);

				BitaHandler.getInstance().insertBitaTransac(bt);
			} catch (SQLException e) {
				EIGlobal.mensajePorTrace ("BitacoraBO - Error al insertar en bitacorizaTransac: " + e, EIGlobal.NivelLog.ERROR);
			} catch (Exception e) {
				EIGlobal.mensajePorTrace ("BitacoraBO - Error al insertar en bitacorizaTransac: " + e, EIGlobal.NivelLog.ERROR);
			}
		}
	}

}