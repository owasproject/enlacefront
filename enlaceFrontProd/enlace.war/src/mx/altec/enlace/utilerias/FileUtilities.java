package mx.altec.enlace.utilerias;

import java.io.*;
import mx.altec.enlace.utilerias.EIGlobal;

/**
 * @author  Cecilio Maldonado Avalos
 * @version 1.0
 * @since   03/11/2009
 */
public class FileUtilities{

	/**
	 * Copia un archivo de una ruta a otra en el mismo equipo
	 *
	 * @param archivoOrigen ruta y archivo a copiar
	 *
	 * @param archivoDestino ruta y archivo donde se generara la copia
	 *
	 * @return boolean true, si fue posible copiar el archivo,
	 *                 false, si el archivo no fue copiado.
	 */
	public static boolean copiarArchivo(String archivoOrigen, String archivoDestino){

		EIGlobal.mensajePorTrace ("Archivo Origen: " + archivoOrigen,
				EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace ("Archivo Destino: " + archivoDestino,
				EIGlobal.NivelLog.DEBUG);

		File f1 = null;
		File f2 = null;

		InputStream in = null;
		OutputStream out = null;

		try{

			f1 = new File(archivoOrigen);
			f2 = new File(archivoDestino);

			in = new FileInputStream(f1);
			out = new FileOutputStream(f2);

			byte[] buf = new byte[10240];
			int len;
			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}

			EIGlobal.mensajePorTrace ("Se copio el archivo: " + archivoOrigen +
					" a " + archivoDestino + " exitosamente", EIGlobal.NivelLog.INFO);
			return true;

		} catch(FileNotFoundException ex){
			EIGlobal.mensajePorTrace ("Error al copiar el archivo: " + archivoOrigen +
					" a " + archivoDestino, EIGlobal.NivelLog.ERROR);

			EIGlobal.mensajePorTrace (ex.getMessage() + " en el directorio especificado.",
					EIGlobal.NivelLog.DEBUG);

			return false;
		} catch(IOException e){
			EIGlobal.mensajePorTrace ("Error al copiar el archivo: " + archivoOrigen +
					" a " + archivoDestino, EIGlobal.NivelLog.INFO);

			EIGlobal.mensajePorTrace (e.getMessage(), EIGlobal.NivelLog.DEBUG);

			return false;
		} finally{

			archivoOrigen = "";
			archivoDestino = "";
			f1 = null;
			f2 = null;

			try {
				if (in != null){
					in.close();
				}
			} catch(IOException e){
				EIGlobal.mensajePorTrace ("Error al cerrar el objeto InputStream: " +
						e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}

			try {
				if (out != null){
					out.close();
				}
			} catch(IOException e){
				EIGlobal.mensajePorTrace ("Error al cerrar el objeto OutputStream: " +
						e.getMessage(), EIGlobal.NivelLog.DEBUG);
			}

		}
	}

	/*public static void main(String[] args){

		boolean res = FileUtilities.copiarArchivo(
				"D:\\origen\\FOR-DES-406 TKM 001.doc",
				"D:\\destino\\FOR-DES-406 TKM 001.doc");

		if (res){
			System.out.println("CopiaArchivoLocal.main -> Archivo copiado");
		} else {
			System.out.println("CopiaArchivoLocal.main -> El archivo no se copio");
		}
	}*/
}