/** Banco Santander Mexicano
 *		Clase Global
 *		@author Gerardo Salazar Vega
 *		@version 1.0
 *		fecha de creacion: 28 de Marzo del 2000
 *		responsable: Roberto Guadalupe Resendiz Altamirano
 * 	descripcion: Clase utilizada para el manejo de variables Globales
 * // Modificacion RMM 20021218 cerrado de archivos
 * 		Control de cambios:
 *     1.1 22/06/2016  FSW. Everis-Se agrega constante Fiel URL
 */

package mx.altec.enlace.utilerias;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import mx.isban.conector.lynx.ConectorLynx;
import mx.isban.mp.conector.ConectorMP;

/**
 *
 * @author Z708764
 *
 */
public final class Global {
    /**
     * Indica las IPs REST que usara el objeto Security Config de Flame para generar un Token de SSO
     */
    public static final String FLAME_REST_SECURITY_CONFIG;

    /**
     * Indica las IPs WS que usara el objeto Security Config de Flame para generar un Token de SSO
     */
    public static final String FLAME_WS_SECURITY_CONFIG;

    /**
     * Indica la Entidad Legal que representa a Enlace frente a Flame
     */
    public static final String FLAME_LEGAL_ENTITY;

    /**
     * Indica la URL que desplegara la aplicacion Flame
     */
    public static final String FLAME_URL;

    /**
     * Indica si SSL esta activo
     */
    public static final String FLAME_SSL_ENABLED;

    /**
     * Indica la ruta del KeyStore donde esta registrado el certificado de Flame
     */
    public static final String FLAME_KEYSTORE_PATH;

    /**
     * Indica la ruta del KeyStore donde esta registrado el certificado de Flame
     */
    public static final String FLAME_STORE_PASS;


    /**
     * Variable que indica la instancia del servidor
     */
	public static String SERVER_INSTANCIA="";
    //public static String ARCHIVO_CONFIGURACION = "C:\\proarchivapp\\was\\was61\\cfgEnlace\\EI.cfg";		//desarrollo
    //public static String LOG4J_CONFIGURACION = "C:\\proarchivapp\\was\\was61\\cfgEnlace\\log4j.properties";
    /** URL BANXICO */
	public static String uRLBANXICO;
	/**
	 * lLLAVESIMETRICA lLLAVESIMETRICA
     */
    public static String lLLAVE;
	/**
	 * sERIE sERIE
	 */
	public static String sERIE;
	//*******************INICIA TCS FSW 12/2016***********************************
	/**
	 * SERIESUSD SERIESUSD
	 */
	public static String sERIESUSD;
	
	public static String Urlbanxico_USD;
	//*******************FIN TCS FSW 12/2016***********************************
	/**
	 * Variable que indica la ruta del archivo de configuracion
	 */
	public static final String ARCHIVO_CONFIGURACION = "/proarchivapp/WebSphere8/enlace/cfg/EI.cfg";		//desarrollo //Cambio a WAS 8
	/**
	 * Variable que guarda la ruta del archivo de configuracion del log
	 */
    public static String LOG4J_CONFIGURACION = "/proarchivapp/WebSphere8/enlace/cfg/log4j.xml";  //Cambio a WAS 8
    /**
     * Variable que indica la ruta donde se va a guardar el log
     */
    public static String LOG4J_LOG = "/proarchivlog/WebSphere8/enlace/" + com.ibm.websphere.runtime.ServerName.getDisplayName() + ".log"; //Cambio a WAS 8
    /**
     * Variable que indica cuantas instancias de log se van a crear
     */
    public static int INSTANCIAS_LOG4J = 20;
    /**
     * Indica la url del servicio de correo
     */
    public static String URL_SERVICIO_EMAIL="";
    /**
     * Indica la url del servicio de SMS
     */
    public static String URL_SERVICIO_SMS="";
    /**
     * Indica cual es el correo del remitente
     */
    public static String  CORREO_REMITENTE="";
    /**
     * Indica el nombre del remitente
     */
    public static String  NOMBRE_REMITENTE="";
    /**
     * Indica el timeout del WS
     */
    public static int TIMEOUT_WS=5000;
    /**
     * Guarda el numero maximo de registros
     */
    public static int MAX_REGISTROS = 0;
    //public static int MAX_REGISTROS_CAMBIOS = 0;
    /**
     *modificacin para integracion pva 07/03/2002
     */
    public static int MAX_CTAS_MODULO       = 100;
    /**
     * Numero de registros por pagina
     */
    public static int NUM_REGISTROS_PAGINA  = 10;
    /**
     * Numero de registros por pagina
     */
	public static int NUM_REGISTROS_PAGINA1 = 10;
	/**
	 * Guarda la clave santader
	 */
    public static String CLAVE_SANTANDER    = "";
    /**
     * Guarda la clave serfin
     */
    public static String CLAVE_SERFIN       = "";
    /**
     * PATH PARA PNG DE COMPROBANTES
     */
    public static String PATH_PNG       = "";
    /**
     * Guarda el nivel de trace del log
     */
    public static int NIVEL_TRACE = 0;
    /**
     * Guarda la ruta de descargas
     */
    public static String DOWNLOAD_PATH = "";
    /**
     * Indica la base de datos
     */
    public static String BASE_DATOS = "";
    /**
     * Indica el usuario de la base de datos
     */
    public static String DB_USER = "";
    /**
     * Indica la clave de la base de datos
     */
    public static String DB_USER_PWD = "";
    /**
     * Guarda el tamano del archivo
     */
	public static String TAM_ARCHIVO = "";
	/**
	 * cambio
	 */
	public static String MAIL_01 = "";
	/**
	 * cambio
	 */
    public static String MAIL_02 = "";
    /**
     * cambio
     */
    public static String MAIL_03 = "";
    /**
     * cambio
     */
    public static String MAIL_04 = "";
    /**
     * cambio
     */
    public static String MAIL_05 = "";
    /**
     * cambio
     */
    public static String SMTP_SERVER = "";
    /**
     * cambio
     */
    public static String AFIL_DB_USER = "";
    /**
     * cambio
     */
    public static String AFIL_DB_USER_PWD = "";
    /**
     * Indica el directorio remoto
     */
    public static String DIRECTORIO_REMOTO = "";
    /**
     * Indica el host remoto
     */
    public static String HOST_REMOTO = "";
    /**
     * Indica el usuario remote
     */
    public static String USUARIO_REMOTO = "";
    /**
     * Directorio local
     */
    public static String DIRECTORIO_LOCAL = "";
    ////Version NAS, Proyecto CNBV
    //public static String NAS; QAF TECNICO
    //public static String USUARIO_REMOTO_SFTP_RL;
    //public static String USUARIO_REMOTO_SFTP_LR;
    //public static String KEY_PATH;
    //public static int    MAX_POOL;
    /**
     * Directorio remoto web
     */
    public static String DIRECTORIO_REMOTO_WEB = "";
    /**
     * Host remoto web
     */
    public static String HOST_REMOTO_WEB = "";
    /**
     * Usuario remoto web
     */
    public static String USUARIO_REMOTO_WEB = "";
    /**
     * Guarda la ruta del directorio remoto exp
     */
    public static String DIRECTORIO_REMOTO_EXP = "";
    /**
     * Indica el host remoto exp
     */
    public static String HOST_REMOTO_EXP = "";
    /**
     * Indica el usuario remoto exp
     */
    public static String USUARIO_REMOTO_EXP = "";
    //Version JSCH, Proyecto NAS
    /**
     * N O R M A  4 3
     * Ruta de internet
     */
	public static String N43_PATH_INTERNET = "";
	/**
	 * N O R M A  4 3
     * Ruta de interfaces
     */
	public static String N43_PATH_INTERFACES = "";
    /******* N O R M A  4 3 ***************/
	/**
	 * T E S O F E
	 * Direcotorio remoto TESOFE
	 */
	public static String DIRECTORIO_REMOTO_TESOFE = "";
	/**
	 * T E S O F E
     * Direcotorio remoto TESOFE internet
	 */
	public static String DIRECTORIO_REMOTO_TESOFE_INTERNET = "";
	/******* T E S O F E ******************/
	/**
	 * ALTA DE CUENTAS
	 * Directorio remoto cuentas
	 */
	public static String DIRECTORIO_REMOTO_CUENTAS = "";
	/******* ALTA DE CUENTAS ******************/
	/**
	 * MOVIMIENTOS
	 * Directorio de movimientos
	 */
	public static String DIRECTORIO_MOV = "";
	/******* MOVIMIENTOS ******************/
	/**
	 * Directorio remoto TASASRVR
	 */
    public static String DIRECTORIO_REMOTO_TASASRVR = "";
    /**
     * Registros maximos a importar nomina
     */
    public static String REG_MAX_IMPORTAR_NOMINA    = "";
    /**
     * Directorio remoto internet
     */
    public static String DIRECTORIO_REMOTO_INTERNET = "";
    /**
     * Ruta de la aplicacion
     */
    public static String APP_PATH = "";
    /**
     * Aplicacion web
     */
    public static String WEB_APPLICATION ="";
    /**
     * Host local
     */
    public static String HOST_LOCAL = "";
    /**
     * Guarda el protocolo
     */
    public static String PROTOCOLO = "";
    /**
     * Indica el datasource TUXEDO
     */
    public static String DATASOURCE_TUXEDO ="";
    /**
     * Indica el datasource ORACLE
     */
    public static String DATASOURCE_ORACLE ="";
    /**
     * Indica el datasource ORACLE2
     * Everis - Nueva Ubicacion de BD para Catalogo de Nomina
     */
    public static String DATASOURCE_ORACLE2 ="";
    /**
     * Indica el datasource de ORACLE3
     */
    public static String DATASOURCE_ORACLE3 ="";
    /**
     * Indica el datasource ORACLE4
     */
    public static String DATASOURCE_ORACLE4 ="";
    //Horario Reuters
    /**
     * Guarda hora FX online
     */
    public static String HORA_FX_ONLINE = "";
    /**
     * Guarda minutos FX online
     */
    public static String MINUTOS_FX_ONLINE = "";
    /**
     * Indica el host FX online
     */
    public static String STR_HOST_FX_ONLINE = "";
    /**
     * Indica el host FX online JUNCTION
     */
    public static String STR_HOST_FX_ONLINE_JUNCTION = "";
    /**
     * Indica el puerto FX online
     */
    public static String I_PORT_FX_ONLINE = "";
    /**
     * Indica el resource FX online
     */
    public static String STR_RESOURCE_FX_ONLINE = "";
    /**
     * STR_PWD_ENC_METHOD_FX_ONLINE
     */
    public static String STR_PWD_ENC_METHOD_FX_ONLINE = "";
    /**
     * Indica el centro de costos FX online
     */
    public static String CENTRO_COSTOS_FX_ONLINE = "";
    /**
     * Indica Hora FX online loq
     */
    public static String HORA_FX_ONLINE_LIQ  = "";
    /**
     * Indica minutos FX online liq
     */
    public static String MINUTOS_FX_ONLINE_LIQ  = "";
    /**
     * Horario SIPARE
     */
    public static String DIAS_INHABILES_SIPARE = "";
    //Administracion de OTPs
    /**
     * OTP nombre ini
     */
    public static String OTP_NOMBRE_INI="";
    /**
     * OTP ruta ini
     */
    public static String OTP_RUTA_INI="";
    /**
     * OTP bloquear
     */
    public static String OTP_BLOQUEAR="";
    /**
     * OTP activar
     */
    public static String OTP_ACTIVAR="";
    /**
     * OTP solicitar
     */
    public static String OTP_SOLICITAR="";
    /**
     * OTP consultar estado
     */
    public static String OTP_CONSULTARESTADO="";
    /**
     * OTP folio seguridad
     */
    public static String OTP_FOLIOSEGURIDAD="";
    /**
     *  OTP Ruta PDF contrato
     */
    public static String OTP_RUTA_PDF_CONTRATO="";
    /**
     * OTP ruta PDF logo
     */
    public static String OTP_RUTA_PDF_LOGO="";
    /**
     * OTP Ryta ODF logo enlace
     */
    public static String OTP_RUTA_PDF_LOGO_ENLACE="";
    /**
     * OTP ruta PDF solbloqcanc
     */
    public static String OTP_RUTA_PDF_SOLBLOQCANC="";
    /**
     * Indica si se van a USAR_BITACORAS
     */
    public static String USAR_BITACORAS="";
    /**
     * Indica si se puede realizar cambio de contrasena
     */
    public static String CONTRASENA_CAMBIO="";
    /**
     * Indica si se Expira sesion
     */
    public static int EXP_SESION;
    //modificacin para CLABE phh 08/08/2002
    /**
     * USO_CTA_CHEQUE
     */
    public static String USO_CTA_CHEQUE = "";
    /**
     * Indica la URL de c ENLACE
     */
	public static String URL_PASSMARK_ENLACE = "";
	/**
	 * Indica si se activa USO_CTA_CHEQUE
	 */
	public static String ACTIVAR_PASSMARK    = "";
	/**
	 * FCH_VAL_OTP
	 */
	public static String FCH_VAL_OTP = "";
	/**
	 * Indica la IP SMTP
	 */
	public static String IP_SMTP="";
	/**
	 * Indica el correo de envio
	 */
	public static String EMAIL_SEND="";
	//MODIFICACION PAGINDA DE INICIO DE SESION
	/**
	 * REDIR_SAM_LOGIN_URL
	 */
	public static String REDIR_SAM_LOGIN_URL="";
	/**
	 * REDIR_SAM_LOGIN_STATUS
	 */
	public static String REDIR_SAM_LOGIN_STATUS="";
	/**
	 * <VC proyecto="200710001" autor="JGR" fecha="15/06/2007"
	 * descripcin="EXPIRACION CONTRASENA SAM">
	 */
	public static String URL_EXPIRACION_CONTRASENA_SAM ="";
	/**
	 * CORREDOR_EXITO_SAM
	 */
	public static String CORREDOR_EXITO_SAM = "";
	/**
	 * CORREDOR_DIAS_EXPIRACION_SAM
	 */
	public static String CORREDOR_DIAS_EXPIRACION_SAM = "";
	/**
	 * CORREDOR_EXPIRACION__SAM
	 */
	public static String CORREDOR_EXPIRACION__SAM = "";
	/*</VC>*/
	/**
	 * <VC proyecto="200710001"
	 * autor="TIB"
	 * fecha="07/06/2007"
	 * descripcion="CAMBIO DE CONTRASENA">*
	 */
	public static String REDIR_CAMBIAR_PASSWORD ="";
	/**
	 * REDIR_LOGOUT_SAM
	 */
	public static String REDIR_LOGOUT_SAM ="";
	/*</VC>*/
	/**
	 * <VC proyecto="200710001"
	 * autor="AHO"
	 * fecha="18/06/2007"
	 * descripcion="RUTA PARA PDF DE SOLICITUD">
	 */
	public static String RUTA_PDF_SOLICITUD ="";
	/*</VC>*/
	/**<VC proyecto="200710001"
	 * autor="JGR"
	 * fecha="01-08-2007"
	 * descripcin="LIGA SUPERNET">
	 */
	public static String LIGA_SUPERNET = "";
	/**
	 * URL SUPERNET
	 */
	public static String URL_SUPERNET = "";
	/*</VC>*/
	/**
	 * Timeout de conexion
	 */
	public static String CONNECTION_TIMEOUT="";
	/**<VC autor ="ESC"
	 * fecha ="18/03/2008"
	 * descripcion ="Variable que contiene la junction del redir">
	 */
	public static String URL_ACCESO_UNICO = "";
	/* JSON TUXEDO*/
	/**
	 * Excepcion de TUXEDO al parsear la respuesta
	 */
	public static final String JSON_TUX_PARSER_EXCEPTION = "Respuesta tuxedo incorrecta, El formato de respuesta tuxedo valido es: {\"msgRec\":{\"fldsRec\":{\"NombreParamSalida1\": \"ValorSalida1\", \"NombreParamSalida2\": ValorSalida2}}} ";
	/**
	 * Excepcion de TUXEDO cuando hay un campo incorrecto
	 */
	public static final String JSON_TUX_FIELD_EXCEPTION = "No es posible convertir el valor ";
	/**
	 * Excepcion de TUXEDO cuando no se tiene respuesta
	 */
	public static final String JSON_TUX_FIELD_NOTFOUND_EXCEPTION = "No se encuentra el valor de salida en respuesta tuxedo";
	/*Variables MQ*/
	/**
	 * Indica el nombre de la factoria para JNDI
	 */
    public static String JNDI_CONECTION_FACTORY="";
    /**
     * Indica JNDI_QUEUE_REQUEST
     */
    public static String JNDI_QUEUE_REQUEST="";
    /**
     * Indica JNDI_QUEUE_RESPONSE
     */
    public static String JNDI_QUEUE_RESPONSE="";
    /**
     * Indica MQ_TIMEOUT
     */
    public static String MQ_TIMEOUT="";
    /**
     * Indica NP_JNDI_CONECTION_FACTORY
     */
    public static String NP_JNDI_CONECTION_FACTORY;
    /**
     * Indica NP_JNDI_QUEUE_REQUEST
     */
    public static String NP_JNDI_QUEUE_REQUEST;
    /**
     * Indica NP_JNDI_QUEUE_RESPONSE
     */
    public static String NP_JNDI_QUEUE_RESPONSE;
    /**
     * Indica NP_MQ_TIMEOUT
     */
    public static String NP_MQ_TIMEOUT;
    /**
     * Indica NP_MQ_USUARIO
     */
    public static String NP_MQ_USUARIO;
    /**
     * Indica NP_MQ_TERMINAL
     */
    public static String NP_MQ_TERMINAL;
    /**
     * Indica NP_ACTIVA_DEMOGRAFICO
     */
    public static String NP_ACTIVA_DEMOGRAFICO;
    /**
     * Indica NP_TMP_SERIALIZACION
     */
    public static String NP_TMP_SERIALIZACION;
    /**
     * Indica ADMUSR_MQ_USUARIO
     */
    public static String ADMUSR_MQ_USUARIO;
    /**
     * JPH VSWF 04/06/2009 CONSTANTE PARA SUPERNET COMERCIOS
     */
    public static String URL_SUPERNET_COMERCIOS = "";
    /**
     * Guarda PREF_CONTR_SNET_COMER
     */
    public static String PREF_CONTR_SNET_COMER = "";
    /**
     * Guarda URL_WEB_SERVICE_LOCATION
     */
    public static String URL_WEB_SERVICE_LOCATION ="";
    /**
     * Guarda DATA_SOURCE_SNET_COMER
     */
    public static String DATA_SOURCE_SNET_COMER = "";
    /**
     * Guarda TIMEOUT_DATASOURCE
     */
    public static String TIMEOUT_DATASOURCE = "";
    /**
     * Guarda TIMEOUT_WEB_SERVICE
     */
    public static String TIMEOUT_WEB_SERVICE = "";
    // JPH VSWF
    /**
     * Devuelve ACT_TESOFE
     */
    public static String ACT_TESOFE ="";
	/**
	 * <P020101
	 * Cuenta Nivel 3>
	 */
	public static String N3_VER_MENU ="";
	/**
	 * Indica si se valida nomina inmediata
	 */
	public static String VALIDA_NOMINA_INMEDIATA="";
	/* </P020101  Cuenta Nivel 3>*/
	/**
	 * <Menu de NEPE>
	 */
	public static String NEPE_MEN_PROV = "";
	/*<Menu de NEPE>*/
	/*<Nomina en linea >*/
	/**
	 * nln_ver_menu menu nomina en linea
	 */
	public static String nlnVerMenu ="";
	/**
	 * directorio_exportar_nln ruta exportacion
	 */
	public static String directorioExportarnln = "";
	/**
	 * directorioContingentenln ruta de contingencia
	 */
	public static String directorioContingentenln = "";
	/**
	 * directorioConfignln directorio de archivo buatux
	 */
	public static String directorioConfignln = "";
	/**
	 * registros_pagina_nln pegistros por pagina
	 */
    public static String registrosPaginanln = "";
    /**
     * nlnHora hora ventana de atencion
     */
    public static String nlnHora = "";
    /**
     * nlnMinutos minutos ventana de atencion
     */
    public static String nlnMinutos = "";
	/**
	 * <M002385  Mancomunidad Fase I>
	 */
	public static String MFI_VER_MENU ="";
	/**
	 * Cuenta Nivel 1 Fase II  P/020204  M/020323
	 */
	public static String CN1_VER_MENU ="";
    //LER VC PAGO SUA LC
	/**
	 * Devuelve SUALC_CTA_ABONO
	 */
    public static String SUALC_CTA_ABONO="";
    /**
     * Devuelve SUALC_VER_SIPARE
     */
    public static String SUALC_VER_SIPARE="";
    //LER VC PAGO SUA LC
    //VARIABLES NP
    /**
     * Guarda NOMPRE_VERMENU
     */
    public static String NOMPRE_VERMENU="";
    //VARIABLES NP
    /**
     * Condicion para menu de administracion de usuarios
     */
    public static String ADMONUSR_VER_MENU="";
    /**
     * Condicion para menu de Limites y Montos
     */
    public static String ADMONLYM_VER_MENU="";
    /**
     * Condicion para menu de Medios de Notificacion
     */
    public static String MEDNOT_VER_MENU="";
    /**
     * URL WS Bloqueo de PWD
     */
    public static String URL_WS_BLOQ_PWD = "";
    /**
     * URL WS Bloqueo de Token
     */
    public static String URL_WS_BLOQ_TOKEN = "";

    /** The status bloq intfallidos. */
    public static int STATUS_BLOQ_INTFALLIDOS;

    /** The status bloq inactividad. */
    public static int STATUS_BLOQ_INACTIVIDAD;

	//Datos para Conexion RSA Fase I
    /**
     * VALIDA_RSA
     */
    public static boolean VALIDA_RSA;
    /**
     * RSA_CALLER_CREDENTIAL
     */
    public static String RSA_CALLER_CREDENTIAL="";
    /**
     * RSA_CALLER_ID
     */
    public static String RSA_CALLER_ID="";
    /**
     * RSA_WS_TIMEOUT
     */
    public static String RSA_WS_TIMEOUT="";
    /**
     * RSA_WEBSERVICE
     */
    public static String RSA_WEBSERVICE="";
    /**
     * ID_CANAL
     */
    public static String ID_CANAL="";
    /**
     * RSA_CALLER_CREDENTIAL_AA
     */
    public static String RSA_CALLER_CREDENTIAL_AA;
    /**
     * RSA_CALLER_ID_AA
     */
    public static String RSA_CALLER_ID_AA;
    /**
     * RSA_WEBSERVICE_AA
     */
    public static String RSA_WEBSERVICE_AA;
    /**
     * RSA_CALLER_CREDENTIAL_STU
     */
    public static String RSA_CALLER_CREDENTIAL_STU;
    /**
     * RSA_CALLER_ID_STU
     */
    public static String RSA_CALLER_ID_STU;
    /**
     * RSA_WEBSERVICE_STU
     */
    public static String RSA_WEBSERVICE_STU;
    /**
     * VALIDA_RSA
     */
    public static boolean BANDERA_RSAFII;
    //fin Datos para Conexion RSA Fase I
    /**
     * Indica PREF_CONTR_SNET_EMPR
     */
	public static String PREF_CONTR_SNET_EMPR = "";
	/**
	 * Indica la ruta TESOFE MOV EDO CTA
	 */
	public static String RUTA_TESOFE_MOV_EDO_CTA = "";
	/**
	 * CORREO PARA EL ANALISTA DE RIESGOS
	 */
	public static String correoAnalistaRiesgos = "";
	/**
	 * Url de descarga de estado de cuenta.
	 */
	public static String urlDescargaEdoCta = "";
	/**
	 * Url del wsdl de servicio de periodos cfdi.
	 */
	public static String periodsCFDIServiceWsdlLocation = "";
	/**
	 * Url del wsdl de servicio valida existencia de estado de cuenta pdf.
	 */
	public static String validaExistenciaPDFWsdlLocation = "";
	/**
     * archCifVerMenu activar menu archivos cifrados
     */
    public static String archCifVerMenu ="";
	/**
	 * Ruta web service archivo cifrad
	 */
	public static String RUTA_WEBSERVICE_CIFRADO = "";
	/**
	 * Ruta web service archivo cifrad NomPredispersion
	 */
	public static String SERVLET_INVOCA_NOMPRE = "";
	/**
	 * Ruta web service archivo cifrad InicioNomina
	 */
	public static String SERVLET_INVOCA_ININOMINA = "";
	/**
	 * Ruta web service archivo cifrad InicioNominaInter
	 */
	public static String SERVLET_INVOCA_NOMINAINTER = "";
	/**
	 * Ruta web service archivo cifrad InicioNominaLn
	 */
	public static String SERVLET_INVOCA_NOMINALN = "";
	/**
	 * Ruta web service archivo cifrad JSP
	 */
	public static String RUTA_CERTIFICADOJSP = "";
	/**
	 * Ruta web service archivo cifrad
	 */
	public static String DIRECTORIO_REMOTO_WEB_ARCHIVO_CIFRADO = "";
	/**
	 * Ruta web service archivo cifrad
	 */
	public static String DIRECTORIO_REMOTO_ARCHIVO_CIFRADOS = "";
	/**
	 * Ruta
	 */
	public static String RUTA_IMAGENES = "";
	/**
	 * Nombre de canal para archivos cifrados
	 */
	public static String CANAL_CIFRADO = "";
	/**
	 * NOMBRE CERTIFICADO del Bco para Descarga Archivos Cifrados
	 */
	public static String NOM_CERTIFICADO_CANAL = "";
	/**
	 * NOMBRE NOM_HERRAMIENTA_CIFRADO Archivos Cifrados
	 */
	public static String NOM_HERRAMIENTA_CIFRADO = "";
    /**
     * VARIABLE PARA INDICAR SI YA SE INICIO EL CONECTOR DE LYNX.
     */
    public static boolean LYNX_INICIALIZADO = false;

	/**
     * Variable que indica si se muestra o no la ventana modal para instalar
     * trusteer rapport
     */
    public static boolean banderaTrusteer;

    /** The tr jndi conection factory. */
    public static String TR_JNDI_CONECTION_FACTORY;

    /** Objeto request para MQ. */
    public static String TR_JNDI_QUEUE_REQUEST;

    /** Objeto response para MQ. */
    public static String TR_JNDI_QUEUE_RESPONSE;

    /** Objeto timeout para MQ. */
    public static String TR_MQ_TIMEOUT;

    /**
     * Variable para guardar la ruta del WS de sellos digitales
     */
    public static String sDUrl = "";

    /**
     * Variable para guardar la ruta del WS de sellos digitales
     */
    public static String fielUrl = "";

    /** Variable para guardar la ruta de bundle para etiquetas del modulo FIEL*/
    public static String rutaBundleFiel = "";

	/**
	 * Indica el nombre de la clase Global
	 */
    private static final String CLASS_NAME = Global.class.getName ();
    /**
     * Guarda las propiedades
     */
    private static Map propiedades = new HashMap();

    /** Constructor privado.*/
    private Global(){

    }



    static {
            configura ( ARCHIVO_CONFIGURACION );
            FLAME_REST_SECURITY_CONFIG = (String)propiedades.get("FLAME_REST_SECURITY_CONFIG");
            FLAME_WS_SECURITY_CONFIG = (String)propiedades.get("FLAME_WS_SECURITY_CONFIG");
            FLAME_LEGAL_ENTITY = (String)propiedades.get("FLAME_LEGAL_ENTITY");
            FLAME_URL = (String)propiedades.get("FLAME_URL");
            FLAME_SSL_ENABLED = (String)propiedades.get("FLAME_SSL_ENABLED");
            FLAME_KEYSTORE_PATH = (String)propiedades.get("FLAME_KEYSTORE_PATH");
            FLAME_STORE_PASS = (String)propiedades.get("FLAME_STORE_PASS");
          }



    /*
     * jndiNombreFabrica,
			String jndiNombreEnvio, String jndiNombreRecepcion
     * */
    /**
     * Configura las variables
     * @param Archivo Indica el nombre del archivo del cual se van a obtener
     *          las variables
     */
    public static void configura ( String Archivo ) {
        FileInputStream read = null;// Modificacion RMM 20021218
        Properties props = new Properties ();
        try {
            read = new FileInputStream (Archivo);

            props.load (read);
            try {
                MAX_REGISTROS = Integer.parseInt (props.getProperty ("MAX_REGISTROS"));
            } catch (NumberFormatException ex) {
                EIGlobal.mensajePorTrace ("Error en parseInt configura() MAX_REGISTROS",EIGlobal.NivelLog.ERROR);
            }
            try {
                NIVEL_TRACE = Integer.parseInt (props.getProperty ("NIVEL_TRACE"));
            } catch ( NumberFormatException e ) {
                                     EIGlobal.mensajePorTrace ("Error en parseInt configura() NIVEL_TRACE",EIGlobal.NivelLog.ERROR);
                                    }
            try {
                  NUM_REGISTROS_PAGINA=Integer.parseInt (props.getProperty ("NUM_REGISTROS_PAGINA"));
                } catch ( NumberFormatException e ) {
                                  EIGlobal.mensajePorTrace ("Error en parseInt configura() NUM_REGISTROS_PAGINA",EIGlobal.NivelLog.ERROR);
                            }
			try {
                  NUM_REGISTROS_PAGINA1=Integer.parseInt (props.getProperty ("NUM_REGISTROS_PAGINA1"));
                } catch ( NumberFormatException e ) {
                                  EIGlobal.mensajePorTrace ("Error en parseInt configura() NUM_REGISTROS_PAGINA1",EIGlobal.NivelLog.ERROR);
                            }
            CLAVE_SANTANDER = props.getProperty ("CLAVE_SANTANDER", "");
            CLAVE_SERFIN = props.getProperty ("CLAVE_SERFIN", "");
            try {
                MAX_CTAS_MODULO = Integer.parseInt (props.getProperty ("MAX_CTAS_MODULO"));
            } catch (NumberFormatException ex) {
                EIGlobal.mensajePorTrace ("Error en parseInt configura() MAX_CTAS_MODULO",EIGlobal.NivelLog.ERROR);
             }
            LOG4J_CONFIGURACION = props.getProperty("LOG4J_CONFIGURACION");
            LOG4J_LOG = props.getProperty("LOG4J_LOG") + com.ibm.websphere.runtime.ServerName.getDisplayName() + ".log";
	     try {
                INSTANCIAS_LOG4J = Integer.parseInt (props.getProperty ("INSTANCIAS_LOG4J"));
            } catch (NumberFormatException ex) {
                EIGlobal.mensajePorTrace ("Error en parseInt configura() INSTANCIAS_LOG4J",EIGlobal.NivelLog.ERROR);
             }
            DOWNLOAD_PATH = props.getProperty ("DOWNLOAD_PATH", "");
            BASE_DATOS = props.getProperty ("BASE_DATOS","");
            DB_USER = props.getProperty ("DB_USER", "");
            DB_USER_PWD = props.getProperty ("DB_USER_PWD", "");
			TAM_ARCHIVO = props.getProperty ("TAM_ARCHIVO", "");
            DIRECTORIO_REMOTO = props.getProperty ("DIRECTORIO_REMOTO", "");
            HOST_REMOTO = props.getProperty ("HOST_REMOTO", "");
            USUARIO_REMOTO = props.getProperty ("USUARIO_REMOTO", "");
            DIRECTORIO_LOCAL = props.getProperty ("DIRECTORIO_LOCAL", "");

          //Version JSCH, Proyecto CNBV
            //NAS = props.getProperty ("NAS", "");
            //USUARIO_REMOTO_SFTP_RL = props.getProperty ("USUARIO_REMOTO_SFTP_RL", "");
            //USUARIO_REMOTO_SFTP_LR = props.getProperty ("USUARIO_REMOTO_SFTP_LR", "");
            //KEY_PATH = props.getProperty ("KEY_PATH", "");
            //MAX_POOL = Integer.parseInt(props.getProperty ("MAX_POOL", ""));


            DIRECTORIO_REMOTO_WEB = props.getProperty ("DIRECTORIO_REMOTO_WEB", "");
            HOST_REMOTO_WEB = props.getProperty ("HOST_REMOTO_WEB", "");
            USUARIO_REMOTO_WEB = props.getProperty ("USUARIO_REMOTO_WEB", "");

          //Version JSCH, Proyecto NAS

            /******* N O R M A  4 3 ***************/
        	N43_PATH_INTERNET = props.getProperty ("N43_PATH_INTERNET", "");
        	N43_PATH_INTERFACES = props.getProperty ("N43_PATH_INTERFACES", "");
        	/******* N O R M A  4 3 ***************/


			/******* T E S O F E ******************/
			DIRECTORIO_REMOTO_TESOFE = props.getProperty ("DIRECTORIO_REMOTO_TESOFE", "");
			DIRECTORIO_REMOTO_TESOFE_INTERNET = props.getProperty ("DIRECTORIO_REMOTO_TESOFE_INTERNET", "");
			/******* T E S O F E ******************/
			/******* ALTA DE CUENTAS ******************/
			DIRECTORIO_REMOTO_CUENTAS = props.getProperty ("DIRECTORIO_REMOTO_CUENTAS", "");
			/******* ALTA DE CUENTAS ******************/

			/******* MOVIMIENTOS ******************/
			DIRECTORIO_MOV = props.getProperty ("DIRECTORIO_MOV", "");
			/******* MOVIMIENTOS ******************/

            DIRECTORIO_REMOTO_EXP = props.getProperty ("DIRECTORIO_REMOTO_EXP", "");
            HOST_REMOTO_EXP = props.getProperty ("HOST_REMOTO_EXP", "");
            USUARIO_REMOTO_EXP = props.getProperty ("USUARIO_REMOTO_EXP", "");
            DIRECTORIO_REMOTO_TASASRVR = props.getProperty ("DIRECTORIO_REMOTO_TASASRVR", "");
            REG_MAX_IMPORTAR_NOMINA    = props.getProperty ("REG_MAX_IMPORTAR_NOMINA", "");
            DIRECTORIO_REMOTO_INTERNET = props.getProperty ("DIRECTORIO_REMOTO_INTERNET", "");
            APP_PATH = props.getProperty ("APP_PATH", "");
            WEB_APPLICATION = props.getProperty ("WEB_APPLICATION", "");
            HOST_LOCAL = props.getProperty ("HOST_LOCAL", "");
            PROTOCOLO = props.getProperty ("PROTOCOLO", "");
            MAIL_01 = props.getProperty ("MAIL_01", "");
            MAIL_02 = props.getProperty ("MAIL_02", "");
            MAIL_03 = props.getProperty ("MAIL_03", "");
            MAIL_04 = props.getProperty ("MAIL_04", "");
            MAIL_05 = props.getProperty ("MAIL_05", "");
            SMTP_SERVER = props.getProperty ("SMTP_SERVER", "");
            AFIL_DB_USER = props.getProperty ("AFIL_DB_USER", "");
            AFIL_DB_USER_PWD = props.getProperty ("AFIL_DB_USER_PWD", "");
            DATASOURCE_TUXEDO = props.getProperty ("DATASOURCE_TUXEDO", "");
            DATASOURCE_ORACLE = props.getProperty ("DATASOURCE_ORACLE", "");
            DATASOURCE_ORACLE2 = props.getProperty ("DATASOURCE_ORACLE2", "");  // Everis - Nueva Ubicacion de BD para Catalogo de Nomina
            DATASOURCE_ORACLE3 = props.getProperty ("DATASOURCE_ORACLE3", "");
            DATASOURCE_ORACLE4 = props.getProperty ("DATASOURCE_ORACLE4", "");

            HORA_FX_ONLINE = props.getProperty ("HORA_FX_ONLINE", "");
            MINUTOS_FX_ONLINE = props.getProperty ("MINUTOS_FX_ONLINE", "");
            STR_HOST_FX_ONLINE = props.getProperty ("STR_HOST_FX_ONLINE", "");
            STR_HOST_FX_ONLINE_JUNCTION = props.getProperty ("STR_HOST_FX_ONLINE_JUNCTION", "");
            HORA_FX_ONLINE_LIQ = props.getProperty ("HORA_FX_ONLINE_LIQ", "");
            MINUTOS_FX_ONLINE_LIQ = props.getProperty ("MINUTOS_FX_ONLINE_LIQ", "");
            I_PORT_FX_ONLINE = props.getProperty ("I_PORT_FX_ONLINE", "");
            STR_RESOURCE_FX_ONLINE = props.getProperty ("STR_RESOURCE_FX_ONLINE", "");
            STR_PWD_ENC_METHOD_FX_ONLINE = props.getProperty ("STR_PWD_ENC_METHOD_FX_ONLINE", "");
            CENTRO_COSTOS_FX_ONLINE = props.getProperty ("CENTRO_COSTOS_FX_ONLINE", "");

            DIAS_INHABILES_SIPARE = props.getProperty ("DIAS_INHABILES_SIPARE", "");


            USO_CTA_CHEQUE = props.getProperty ("USO_CTA_CHEQUE", "");

            OTP_NOMBRE_INI = props.getProperty ("OTP_NOMBRE_INI", "");
            OTP_RUTA_INI = props.getProperty ("OTP_RUTA_INI", "");
				OTP_BLOQUEAR = props.getProperty ("OTP_BLOQUEAR", "");
		    OTP_ACTIVAR = props.getProperty ("OTP_ACTIVAR", "");
		    OTP_SOLICITAR = props.getProperty ("OTP_SOLICITAR", "");
		    OTP_CONSULTARESTADO = props.getProperty ("OTP_CONSULTARESTADO", "");
		    OTP_FOLIOSEGURIDAD = props.getProperty ("OTP_FOLIOSEGURIDAD", "");
		    OTP_RUTA_PDF_CONTRATO = props.getProperty ("OTP_RUTA_PDF_CONTRATO", "");
		    OTP_RUTA_PDF_LOGO = props.getProperty ("OTP_RUTA_PDF_LOGO", "");
		    OTP_RUTA_PDF_LOGO_ENLACE = props.getProperty ("OTP_RUTA_PDF_LOGO_ENLACE", "");
		    OTP_RUTA_PDF_SOLBLOQCANC = props.getProperty ("OTP_RUTA_PDF_SOLBLOQCANC", "");
		    SERVER_INSTANCIA=props.getProperty ("SERVER_INSTANCIA", "");

			URL_PASSMARK_ENLACE=props.getProperty ("URL_PASSMARK_ENLACE", "");
			ACTIVAR_PASSMARK=props.getProperty ("ACTIVAR_PASSMARK", "");
			EXP_SESION = Integer.parseInt(props.getProperty ("EXP_SESION", ""));

			USAR_BITACORAS = props.getProperty("USAR_BITACORAS", "");
			CONTRASENA_CAMBIO = props.getProperty("CONTRASENA_CAMBIO", "");
			FCH_VAL_OTP = props.getProperty("FCH_VAL_OTP", "");

			IP_SMTP = props.getProperty("IP_SMTP", "");
			EMAIL_SEND = props.getProperty("EMAIL_SEND", "");
			REDIR_SAM_LOGIN_URL=props.getProperty("REDIR_SAM_LOGIN_URL","");//Cambio de pagina de Inicio
			REDIR_SAM_LOGIN_STATUS=props.getProperty("REDIR_SAM_LOGIN_STATUS","");//Guardado del status del Login
			CORREDOR_EXITO_SAM = props.getProperty("CORREDOR_EXITO_SAM","");

			/*<VC proyecto="200710001" autor="TIB" fecha="07/06/2007" descripcin="CAMBIO DE CONTRASENA">*/
			REDIR_LOGOUT_SAM =props.getProperty("REDIR_LOGOUT_SAM","");
			REDIR_CAMBIAR_PASSWORD =props.getProperty("REDIR_CAMBIAR_PASSWORD","");
			/*</VC>*/
			/*<VC proyecto="200710001" autor="JGR" fecha="15/06/2007" descripcin="EXPIRACION DE CONTRASENA">*/
			URL_EXPIRACION_CONTRASENA_SAM =props.getProperty("URL_EXPIRACION_CONTRASENA_SAM","");
			CORREDOR_EXITO_SAM =props.getProperty("CORREDOR_EXITO_SAM","");
			CORREDOR_DIAS_EXPIRACION_SAM =props.getProperty("CORREDOR_DIAS_EXPIRACION_SAM","");
			CORREDOR_EXPIRACION__SAM =props.getProperty("CORREDOR_EXPIRACION__SAM","");
			/*</VC>*/
			/*<VC proyecto="200710001" autor="AHO" fecha="18/06/2007" descripcin="RUTA PARA PDF DE SOLICITUD">*/
			RUTA_PDF_SOLICITUD=props.getProperty("RUTA_PDF_SOLICITUD", "");
			/*</VC>*/

			/*<VC proyecto="200710001" autor="JGR" fecha="01/08/2007" descripcin="LIGA SUPERNET">*/
			LIGA_SUPERNET = props.getProperty("LIGA_SUPERNET","");
			URL_SUPERNET = props.getProperty("URL_SUPERNET","");
			uRLBANXICO = props.getProperty("URL_BANXICO","");
			lLLAVE = props.getProperty("LLAVE","");
			sERIE = props.getProperty("SERIE","");
			//*******************Inicio TCS FSW 12/2016***********************************
			sERIESUSD = props.getProperty("SERIESUSD","");
			Urlbanxico_USD = props.getProperty("URL_BANXICO_USD","");
			//*******************FIN TCS FSW 12/2016***********************************
			/*</VC>*/
			PREF_CONTR_SNET_EMPR = props.getProperty("PREF_CONTR_SNET_EMPR","");
			CONNECTION_TIMEOUT = props.getProperty("CONNECTION_TIMEOUT","");
			/*<VC autor ="ESC" fecha ="18/03/2008" descripcion ="Variable que contiene la junction del redir"> */
			URL_ACCESO_UNICO = props.getProperty("URL_ACCESO_UNICO","");;
			/*</VC>*/
			/*Varibles MQ */
			JNDI_CONECTION_FACTORY= props.getProperty("JNDI_CONECTION_FACTORY","");
		    JNDI_QUEUE_REQUEST= props.getProperty("JNDI_QUEUE_REQUEST","");
		    JNDI_QUEUE_RESPONSE= props.getProperty("JNDI_QUEUE_RESPONSE","");
		    MQ_TIMEOUT= props.getProperty("MQ_TIMEOUT","6");
		    //seteo path de png's para comprobantes
		    PATH_PNG= props.getProperty("PATH_PNG","");
		    NP_JNDI_CONECTION_FACTORY = props.getProperty("NP_JNDI_CONECTION_FACTORY");
		    NP_JNDI_QUEUE_REQUEST = props.getProperty("NP_JNDI_QUEUE_REQUEST");
		    NP_JNDI_QUEUE_RESPONSE = props.getProperty("NP_JNDI_QUEUE_RESPONSE");
		    NP_MQ_TIMEOUT = props.getProperty("NP_MQ_TIMEOUT");
		    NP_MQ_USUARIO = props.getProperty("NP_MQ_USUARIO");
		    NP_MQ_TERMINAL = props.getProperty("NP_MQ_TERMINAL");
		    NP_ACTIVA_DEMOGRAFICO = props.getProperty("NP_ACTIVA_DEMOGRAFICO");
		    NP_TMP_SERIALIZACION = props.getProperty("NP_TMP_SERIALIZACION");

		    //JPH VSWF REQUERIMIENTO PARA LIGA ENLACE SUPERNET COMERCIOS 05/06/2009
		    URL_SUPERNET_COMERCIOS = props.getProperty("URL_SUPERNET_COMERCIOS","");
		    URL_WEB_SERVICE_LOCATION = props.getProperty("URL_WEB_SERVICE_LOCATION","");
		    DATA_SOURCE_SNET_COMER = props.getProperty("DATA_SOURCE_SNET_COMER","");
		    TIMEOUT_DATASOURCE = props.getProperty("TIMEOUT_DATASOURCE","");
		    TIMEOUT_WEB_SERVICE =props.getProperty("TIMEOUT_WEB_SERVICE","");
		    //FIN JPH
		    //LER VC PAGO SUA LC
		    SUALC_CTA_ABONO=props.getProperty("SUALC_CTA_ABONO","");
		    SUALC_VER_SIPARE=props.getProperty("SUALC_VER_SIPARE","");
		    //LER VC PAGO SUA LC

		    NOMPRE_VERMENU=props.getProperty("NOMPRE_VERMENU","");
		    //Condicion para menu de administracion de usuarios
		    ADMONUSR_VER_MENU = props.getProperty("ADMONUSR_VER_MENU","");
		    //Condicion para menu de Limites y Montos
		    ADMONLYM_VER_MENU = props.getProperty("ADMONLYM_VER_MENU","");
		    //Condicion para menu de Medios de Notificacion
		    MEDNOT_VER_MENU = props.getProperty("MEDNOT_VER_MENU","");;
		    // WS Bloqueo de PWD
		    URL_WS_BLOQ_PWD = props.getProperty("URL_WS_BLOQ_PWD","");
		    // WS Bloqueo de Token
		    URL_WS_BLOQ_TOKEN = props.getProperty("URL_WS_BLOQ_TOKEN","");

		    STATUS_BLOQ_INTFALLIDOS = Integer.parseInt(props.getProperty("STATUS_BLOQ_INTFALLIDOS", "3"));
		    STATUS_BLOQ_INACTIVIDAD = Integer.parseInt(props.getProperty("STATUS_BLOQ_INACTIVIDAD", "4"));

			// RSA
		    VALIDA_RSA = Boolean.valueOf(props.getProperty("VALIDAR_RSA",""));
			RSA_CALLER_CREDENTIAL=props.getProperty("RSA_CALLER_CREDENTIAL","");
			RSA_CALLER_ID=props.getProperty("RSA_CALLER_ID","");
			RSA_WS_TIMEOUT=props.getProperty("RSA_WS_TIMEOUT","");
			RSA_WEBSERVICE=props.getProperty("RSA_WEBSERVICE","");
			ID_CANAL=props.getProperty("ID_CANAL","");
			RSA_CALLER_CREDENTIAL_AA=props.getProperty("RSA_CALLER_CREDENTIAL_AA","");
			RSA_CALLER_ID_AA=props.getProperty("RSA_CALLER_ID_AA","");
			RSA_WEBSERVICE_AA=props.getProperty("RSA_WEBSERVICE_AA","");
			RSA_CALLER_CREDENTIAL_STU=props.getProperty("RSA_CALLER_CREDENTIAL_STU","");
			RSA_CALLER_ID_STU=props.getProperty("RSA_CALLER_ID_STU","");
			RSA_WEBSERVICE_STU=props.getProperty("RSA_WEBSERVICE_STU","");
			BANDERA_RSAFII = Boolean.valueOf(props.getProperty("BANDERA_RSAFII",""));
		    // Usuario para MQ
		    ADMUSR_MQ_USUARIO = props.getProperty("ADMUSR_MQ_USUARIO");




		    URL_SERVICIO_EMAIL = props.getProperty("URL_SERVICIO_EMAIL");
		    URL_SERVICIO_SMS = props.getProperty("URL_SERVICIO_SMS");
 		    CORREO_REMITENTE=props.getProperty("CORREO.REMITENTE");
		    NOMBRE_REMITENTE=props.getProperty("NOMBRE.REMITENTE");
		    ACT_TESOFE = props.getProperty ("ACT_TESOFE", "");
			N3_VER_MENU = props.getProperty ("N3_VER_MENU", "");
			VALIDA_NOMINA_INMEDIATA = props.getProperty ("VALIDA_NOMINA_INMEDIATA", "");
			MFI_VER_MENU = props.getProperty ("MFI_VER_MENU", "");
			NEPE_MEN_PROV = props.getProperty ("NEPE_MEN_PROV", "");
			CN1_VER_MENU = props.getProperty ("CN1_VER_MENU", "");
		    RUTA_TESOFE_MOV_EDO_CTA = props.getProperty ("RUTA_TESOFE_MOV_EDO_CTA", "");

		    correoAnalistaRiesgos = props.getProperty ("CORREO_ANALISTA_RIESGO", "");

		    nlnVerMenu = props.getProperty ("NLN_VER_MENU", "");
		    directorioExportarnln = props.getProperty ("DIRECTORIO_EXPORTAR_NLN", "");
		    directorioContingentenln = props.getProperty ("DIRECTORIO_CONTENGENTE_NLN", "");
		    directorioConfignln = props.getProperty ("DIRECTORIO_CONFIG_NLN", "");
		    registrosPaginanln = props.getProperty ("REGISTROS_PAGINA_NLN", "");
		    nlnHora = props.getProperty ("NLN_HORA", "");
		    nlnMinutos = props.getProperty ("NLN_MINUTOS", "");

			/** URLs ESTADOS DE CUENTA PDF/XML **/
			urlDescargaEdoCta = props.getProperty ("URL_DESCARGA_EDOCTA", "");
			periodsCFDIServiceWsdlLocation = props.getProperty("PERIODSCFDISERVICE", "");
			validaExistenciaPDFWsdlLocation = props.getProperty("VALIDAEXISTENCIAPDF", "");

		    /** Variable ver menu archivos cifrados **/
		    archCifVerMenu = props.getProperty ("ARCHCIF_VER_MENU", "");
			SERVLET_INVOCA_NOMPRE =props.getProperty("SERVLET_INVOCA_NOMPRE", "");
		    SERVLET_INVOCA_ININOMINA =props.getProperty("SERVLET_INVOCA_ININOMINA", "");
			SERVLET_INVOCA_NOMINAINTER = props.getProperty("SERVLET_INVOCA_NOMINAINTER", "");
			SERVLET_INVOCA_NOMINALN = props.getProperty("SERVLET_INVOCA_NOMINALN", "");
			RUTA_CERTIFICADOJSP =props.getProperty("RUTA_CERTIFICADOJSP", "");
			DIRECTORIO_REMOTO_ARCHIVO_CIFRADOS =props.getProperty("DIRECTORIO_REMOTO_ARCHIVO_CIFRADOS", "");
			RUTA_IMAGENES = props.getProperty("RUTA_IMAGENES", "");

			RUTA_WEBSERVICE_CIFRADO = props.getProperty ("RUTA_WEBSERVICE_CIFRADO", "");
		    DIRECTORIO_REMOTO_WEB_ARCHIVO_CIFRADO = props.getProperty ("DIRECTORIO_REMOTO_WEB_ARCHIVO_CIFRADO",
		    		"DIRECTORIO_REMOTO_ARCHIVO_CIFRADOS");

		    TR_JNDI_CONECTION_FACTORY = props.getProperty("TR_JNDI_CONECTION_FACTORY", "");
		    TR_JNDI_QUEUE_REQUEST = props.getProperty("TR_JNDI_QUEUE_REQUEST", "");
		    TR_JNDI_QUEUE_RESPONSE = props.getProperty("TR_JNDI_QUEUE_RESPONSE", "");
		    TR_MQ_TIMEOUT = props.getProperty("TR_MQ_TIMEOUT", "");

		    CANAL_CIFRADO = props.getProperty ("CANAL_CIFRADO", "ENLA");
			NOM_CERTIFICADO_CANAL = props.getProperty ("NOM_CERTIFICADO_CANAL", "CertificadoEnlace");
			NOM_HERRAMIENTA_CIFRADO = props.getProperty ("NOM_HERRAMIENTA_CIFRADO", "FileEncryption.jar");

			if(props.getProperty("TIMEOUT_WS")!=null) {
				TIMEOUT_WS	    = Integer.parseInt((String)props.getProperty(("TIMEOUT_WS")).trim());
			}

            String lynxConfig = props.getProperty("LYNX_CONFIG");
            EIGlobal.mensajePorTrace("Ruta Configurada de Conector Lynx: " + lynxConfig, EIGlobal.NivelLog.INFO);
            LYNX_INICIALIZADO = ConectorLynx.initConector(lynxConfig);
            EIGlobal.mensajePorTrace("Lynx incializado = " + LYNX_INICIALIZADO, EIGlobal.NivelLog.INFO);

            String mpConfig = props.getProperty("MP_CONFIG");
            EIGlobal.mensajePorTrace("Ruta Configurada de Conector Monitor Plus: " + mpConfig, EIGlobal.NivelLog.INFO);
            ConectorMP.initConector(mpConfig);

            fielUrl = props.getProperty("FIEL_URL");

			if (props.getProperty("BANDERA_TRUSTEER") != null) {
                banderaTrusteer = Boolean.valueOf(props.getProperty(
                        "BANDERA_TRUSTEER", ""));
            }

			sDUrl = props.getProperty("SD_URL");
			EIGlobal.mensajePorTrace("La URL Sellos Digitales >" + sDUrl + "<", EIGlobal.NivelLog.INFO);
            rutaBundleFiel = props.getProperty("RUTA_BUNDLE_FIEL");

        } catch ( FileNotFoundException e ) {
            EIGlobal.mensajePorTrace ( "ALERTA... no existe el archivo de configuracion" +
            " usando valores por omision",EIGlobal.NivelLog.ERROR);
        } catch ( IOException e ) {
            EIGlobal.mensajePorTrace ("Error en lectura de Configuracion: "+e.getMessage(),EIGlobal.NivelLog.ERROR);
        } catch ( Exception e ) {
            EIGlobal.mensajePorTrace (e.getMessage (),EIGlobal.NivelLog.ERROR);
        }finally{// Modificacion RMM 20021218 inicio
            if(null != read){
                    try {
						read.close ();
					} catch (IOException e) {
						 EIGlobal.mensajePorTrace (e.getMessage (),EIGlobal.NivelLog.ERROR);
					}
                read = null;
            }
        }// Modificacion RMM 20021218 fin

        propiedades = new HashMap();


		synchronized(Global.class) {
			Enumeration keys = props.keys ();
			while (keys.hasMoreElements ()) {
				String key = (String)keys.nextElement ();
				propiedades.put ( key, props.getProperty (key) );
			}
		}
    }

    /** obtiene el valor de una propiedad
     * @param key llave para el valor de la propiedad solicitada
     * @return Valor de la propiedad solicitada o null si no existe la llave
     */
    public static String getProperty(String key) {
        return (String) propiedades.get(key);
    }

    /** obtiene el valor de una propiedad
     * @param key llave para el valor de la propiedad solicitada
     * @param defaultValue valor por defecto en caso de no encontrar la llave
     * @return Valor de la propiedad solicitada o defaultValue
     *   si no existe la llave
     */
    public static String getProperty(String key, String defaultValue) {
        String retValue = (String) getProperty(key);
        if (null == retValue) {
            EIGlobal.mensajePorTrace( CLASS_NAME + ": usingDefault:" + defaultValue,  EIGlobal.NivelLog.INFO );
            retValue = defaultValue;
        }
        return retValue;
    }
}
/*2007.01.1*/