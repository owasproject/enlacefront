/**
 *   Isban Mexico
 *   Clase: FielValidaciones.java
 *   Descripcion: Clase utilitaria para validaciones de la FIEL.
 *
 *   Control de Cambios:
 *   1.0 22/06/2016  FSW. Everis
 */
package mx.altec.enlace.utilerias;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mx.altec.enlace.beans.BeanAdminFiel;
import mx.altec.enlace.cliente.ws.etransfernal.BeanDatosTitular;
import mx.altec.enlace.cliente.ws.etransfernal.BeanResConsultaCertificado;
import mx.altec.enlace.dao.DAOTrxPE61;

/**
 * Clase utilitaria para validaciones de la FIEL
 *
 * @author FSW Everis
 *
 */
public final class FielValidaciones {

    /**
     * Contructor de la clase FielValidaciones
     */
    private FielValidaciones() {
        super();
    }

    /**
     * Metodo para evaluar si BeanAdminFiel no es nulo para la alta de una fiel.
     *
     * @param beanAdminFiel DTO a evaluar
     * @return BeanAdminFiel DTO de resultado
     */
    public static BeanAdminFiel noEsNuloBeanAdminFiel(BeanAdminFiel beanAdminFiel) {
        if (beanAdminFiel == null) {
            beanAdminFiel = new BeanAdminFiel();
        } else if (beanAdminFiel.getDetalleConsulta() == null) {
            beanAdminFiel.setDetalleConsulta(new BeanResConsultaCertificado());
        } else {
            beanAdminFiel.getDetalleConsulta().complemento();
            if (beanAdminFiel.getDetalleConsulta().getDatosTitular() == null) {
                beanAdminFiel.getDetalleConsulta().setDatosTitular(new BeanDatosTitular());
            } else {
                BeanDatosTitular titular = beanAdminFiel.getDetalleConsulta().getDatosTitular();
                titular.rellenaNulos();
            }
        }
        return beanAdminFiel;
    }

    /**
     * Aplica reglas de negocio para validar los datos de entrada para la
     * transaccion OD54
     *
     * @param b el DTO con los datos a validar
     * @return true si los datos son validos
     */
    public static boolean validaDatosEntrada(BeanAdminFiel b) {
        if (null == b.getIndicadorOperacion() || b.getIndicadorOperacion().isEmpty()
                || (!FielConstants.ALTA_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase())
                && !FielConstants.BAJA_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase())
                && !FielConstants.MODF_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase()))) {
            EIGlobal.mensajePorTrace("Se requiere especificar un estatus valido para la operacion", EIGlobal.NivelLog.ERROR);
            return false;
        }
        if (null == b.getNumCliente() || b.getNumCliente().trim().length() != 8) {
            EIGlobal.mensajePorTrace("Se requiere especificar un n\u00FAmero de cliente v\u00E1lido de 8 caracteres", EIGlobal.NivelLog.ERROR);
            return false;
        }

        if (null == b.getDetalleConsulta().getNumSerie() || b.getDetalleConsulta().getNumSerie().trim().isEmpty()) {
            EIGlobal.mensajePorTrace("Se requiere especificar un n\u00FAmero de documento v\u00E1lido", EIGlobal.NivelLog.ERROR);
            return false;
        }

        if ((null == b.getSecuenciaDocumento() || b.getSecuenciaDocumento().trim().isEmpty())
                && (FielConstants.BAJA_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase())
                || FielConstants.MODF_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase()))) {
            EIGlobal.mensajePorTrace("Se requiere en caso de baja o modificaci\u00F3n de documento que se especifique la secuencia del documento", EIGlobal.NivelLog.ERROR);
            return false;
        }

        if (FielConstants.MODF_DOCUMENTO.equals(b.getIndicadorOperacion().trim().toUpperCase())
                && !validaDatosObligatoriosModificacion(b)) {
            return false;
        }

        if ((null != b.getDetalleConsulta().getFchEmision() && !b.getDetalleConsulta().getFchEmision().trim().isEmpty())
                && (null != b.getDetalleConsulta().getFchExpiracion() && !b.getDetalleConsulta().getFchExpiracion().trim().isEmpty())
                && !validaFechaExpedicionYVencimiento(b.getDetalleConsulta().getFchEmision(), b.getDetalleConsulta().getFchExpiracion())) {
            return false;
        }

        return true;
    }

    /**
     * Aplica las reglas de negocio para los campos cuando la transaccion OD54
     * es una modificacion
     *
     * @param b el DTO con los datos a validar
     * @return true si las validaciones son correctas
     */
    private static boolean validaDatosObligatoriosModificacion(BeanAdminFiel b) {
        boolean datosObligatoriosLlenos = false;
        datosObligatoriosLlenos = datosObligatoriosLlenos || (null != b.getDetalleConsulta().getDatosTitular().getPais() && !b.getDetalleConsulta().getDatosTitular().getPais().trim().isEmpty());
        datosObligatoriosLlenos = datosObligatoriosLlenos || (null != b.getDetalleConsulta().getDatosTitular().getNombreOrganizacion() && !b.getDetalleConsulta().getDatosTitular().getNombreOrganizacion().trim().isEmpty());
        datosObligatoriosLlenos = datosObligatoriosLlenos || (null != b.getDetalleConsulta().getDatosTitular().getEstado() && !b.getDetalleConsulta().getDatosTitular().getEstado().trim().isEmpty());
        datosObligatoriosLlenos = datosObligatoriosLlenos || (null != b.getDetalleConsulta().getFchEmision() && !b.getDetalleConsulta().getFchEmision().trim().isEmpty());
        datosObligatoriosLlenos = datosObligatoriosLlenos || (null != b.getDetalleConsulta().getFchExpiracion() && !b.getDetalleConsulta().getFchExpiracion().trim().isEmpty());

        if (!datosObligatoriosLlenos) {
            EIGlobal.mensajePorTrace("Se requiere en caso de modificaci\u00F3n que alguno de los siguientes campos este informado: c\u00F3digo pa\u00EDs, expedido por, lugar de expedici\u00F3n, fecha de expedici\u00F3n, fecha de vencimiento", EIGlobal.NivelLog.ERROR);
            return false;
        }
        return true;
    }

    /**
     * Aplica reglas de negocio para los campos fecha de expedicion y fecha de
     * vencimiento
     *
     * @param fechaExpedicion la fecha de expedicion
     * @param fechaVncimiento la fecha de vencimiento
     * @return true si las validaciones son correctas
     */
    private static boolean validaFechaExpedicionYVencimiento(String fechaExpedicion, String fechaVncimiento) {

        if (null != fechaExpedicion && !fechaExpedicion.trim().isEmpty()
                && null != fechaVncimiento && !fechaVncimiento.trim().isEmpty()) {

            if (!fechaExpedicion.matches(FielConstants.REGEX_FECHA_DOCTO) || !fechaVncimiento.matches(FielConstants.REGEX_FECHA_DOCTO)) {
                EIGlobal.mensajePorTrace("Se requiere que la fecha de expedici\u00F3n y vencimiento tengan el formato ".concat(FielConstants.FORMATO_FECHA_DOCTO), EIGlobal.NivelLog.ERROR);
                return false;
            }

            SimpleDateFormat sdf = new SimpleDateFormat(FielConstants.FORMATO_FECHA_DOCTO, new Locale("es", "MX"));
            sdf.setLenient(false);
            Date dateFechaExpedicion = null;
            Date dateFechaVncimiento = null;

            try {

                dateFechaExpedicion = sdf.parse(fechaExpedicion);
                dateFechaVncimiento = sdf.parse(fechaVncimiento);

                if (dateFechaExpedicion.after(new Date())) {
                    EIGlobal.mensajePorTrace("La fecha de expedici\u00F3n debe ser menor o igual que la fecha actual", EIGlobal.NivelLog.ERROR);
                    return false;
                }

                if (dateFechaExpedicion.after(dateFechaVncimiento)) {
                    EIGlobal.mensajePorTrace("La fecha de expedici\u00F3n no puede ser mas reciente que la fecha de vencimiento", EIGlobal.NivelLog.ERROR);
                    return false;
                }

                return true;

            } catch (ParseException e) {
                EIGlobal.mensajePorTrace("la fechas no vienen en el formato requerido ".concat(FielConstants.FORMATO_FECHA_DOCTO), EIGlobal.NivelLog.ERROR);
                EIGlobal.mensajePorTraceExcepcion(e);
                return false;
            }
        }
        EIGlobal.mensajePorTrace("La fecha de expedici\u00F3n y la fecha de vencimiento deben estar informadas", EIGlobal.NivelLog.ERROR);
        return false;
    }

    /**
     * Invoca la transaccion PE61 para obtener el RFC y lo compara con el dato
     * indicado por Banxico
     *
     * @param numCliente codigo de cliente
     * @param rfc registroi proporcionado por Banxico a comparar
     * @return <code>true</code> en caso de que sean iguales los RFC
     */
    public static boolean validaRFC(String numCliente, String rfc) {
        DAOTrxPE61 detalleRfc = new DAOTrxPE61();
        boolean resp = detalleRfc.ejecutaConsulta(numCliente,
                FielConstants.CADENA_VACIA, FielConstants.CADENA_VACIA);
        if (resp) {
            String rfcPE61 = detalleRfc
                    .obtenCampo(DAOTrxPE61.FMT_RESP_CAMPO_NUMDOCTO);
            resp = rfcPE61 != null && rfc.toUpperCase().contains(rfcPE61.trim().toUpperCase());
            EIGlobal.mensajePorTrace(" rfcPE61 |" + rfcPE61
                    + "| - |rfcBanxico |" + rfc + "| - |"
                    + "| - | rfcPE61 VS rfcBanxico |" + resp,
                    EIGlobal.NivelLog.ERROR);
        }
        return resp;
    }
}
