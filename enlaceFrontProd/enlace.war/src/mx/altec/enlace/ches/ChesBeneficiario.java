/*
 * ChesBeneficiario.java
 *
 * Created on 9 de enero de 2003, 06:07 PM
 */

package mx.altec.enlace.ches;

/** Contiene un beneficiario de Chequera de seguridad
 * @author Rafael Martinez Montiel
 */
public class ChesBeneficiario implements java.io.Serializable ,Comparable {

    /** Holds value of property cveBenef. */
    private String cveBenef;

    /** Holds value of property nombre. */
    private String nombre;

    /** Holds value of property fechaAlta. */
    private String fechaAlta;

    /** Creates a new instance of ChesBeneficiario */
    public ChesBeneficiario () {
    }

    /** Getter for property cveBenef.
     * @return Value of property cveBenef.
     */
    public String getCveBenef () {
        return this.cveBenef;
    }

    /** Setter for property cveBenef.
     * @param cveBenef New value of property cveBenef.
     */
    public void setCveBenef (String cveBenef) {
        this.cveBenef = cveBenef;
    }

    /** Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre () {
        return this.nombre;
    }

    /** Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre (String nombre) {
        this.nombre = nombre;
    }

    /** Getter for property fechaAlta.
     * @return Value of property fechaAlta.
     */
    public String getFechaAlta () {
        return this.fechaAlta;
    }

    /** Setter for property fechaAlta.
     * @param fechaAlta New value of property fechaAlta.
     */
    public void setFechaAlta (String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /** Obtiene un codigo de hash para el objeto
     * @return hashcode
     */
    public int hashCode (){
        return this.nombre.hashCode ();
    }

    /** Compara este Beneficiario contra otro Beneficiario ordinalmente
     * @param obj ChesBeneficiario contra el que se va a comparar
     * @return valor de comparacion
     */
    public int compareTo (Object obj) {
        try{
            ChesBeneficiario benef = (ChesBeneficiario) obj;
            return this.nombre.compareTo ( benef.getNombre () );
        }catch(ClassCastException ex){
            return this.nombre.compareTo ( obj.toString () );
        }
    }

}
