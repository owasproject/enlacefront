/*
 * ChesValidacionException.java
 *
 * Created on 3 de enero de 2003, 01:34 PM
 */

package mx.altec.enlace.ches;
import java.util.List;
/** representa un error al intentar hacer un parse de objetos
 * @author Rafael Martinez Montiel
 */
public class ChesValidacionException extends java.lang.Exception {

    /** Contenedor para multiples mensajes de error
     */
    private List parseExceptions = null;



    /**
     * Constructs an instance of <code>ChesValidacionException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public ChesValidacionException (String msg) {
        super (msg);
    }

    /** Constructs an instance of <code>ChesValidacionException</code> with the specified detail message and errors.
     * @param msg mensaje
     * @param parseExceptions Lista de errores
     */
    public ChesValidacionException (String msg, List parseExceptions) {
        this(msg);
        this.parseExceptions = parseExceptions;
    }

    /** Obtiene la representacion de este objeto
     * @return representacion en cadena de este objeto
     */
    public String toString () {
        StringBuffer retValue = new StringBuffer(this.getMessage () + "\n" );

        //retValue.append (this.getMessage ());
        if(null != this.parseExceptions){
            retValue.append ("<ul>\n");
            java.util.ListIterator li = parseExceptions.listIterator ();
            while(li.hasNext ()){
                retValue.append ("<li>"  + li.next ().toString () + "</li>\n" );
            }
            retValue.append ("</ul>\n");
        }
        return retValue.toString ();
    }

    public String parseExceptionsAsString( String prefix ){
        StringBuffer retValue = new StringBuffer();

        //retValue.append (this.getMessage ());
        if(null != this.parseExceptions){
            java.util.ListIterator li = parseExceptions.listIterator ();
            while(li.hasNext ()){
                Object obj = li.next ();
                if( obj instanceof ChesValidacionException ){
                    retValue.append( ((ChesValidacionException)obj).parseExceptionsAsString ( prefix + "\t" ) );
                }else{
                    retValue.append ( prefix  + obj.toString () + "\n" );
                }
            }

        }
        return retValue.toString ();
    }

    public String parseExceptionsAsString(){
        return parseExceptionsAsString( "" );
    }
    /** Obtiene la lista de errores
     * @return lista de errores
     */
    public List parseExceptions(){
        return this.parseExceptions;
    }

}
