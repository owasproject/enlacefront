package mx.altec.enlace.bita;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import mx.altec.enlace.utilerias.IEnlace;

public class Utilerias {
	
	public static String formatDateToString(
			Date fecha,
			String formato) {
		String strFecha = "";
		if (fecha != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(formato);
			strFecha = formatter.format(fecha);
		}
		return strFecha;
	}

	/**
	 * Regresa un Date con un String validado anteriormente y con un formato de forma "dd/MM/yyyy"
	 * @param String con el formato: dd/MM/yyyy
	 * @author Edgar Albedi
	 * @return la fecha formateada
	 */
	public static Date MdyToDate(String cadena) {
		if (cadena.length() == 10) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setLenient(false);
			Date fechaDate = null;
			try {
				fechaDate = sdf.parse(cadena);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return fechaDate;
		}

		return null;
	}


	public static String entreComillas(String campo){
		if (campo==null){
			return "null";
		}else{
			return "'" + campo + "'";
		}

	}

	public static String getStrDate(GregorianCalendar c) {
		 int m = c.get(GregorianCalendar.MONTH) + 1;
		 int d = c.get(GregorianCalendar.DATE);
		 String mm = Integer.toString(m);
		 String dd = Integer.toString(d);
		 return "" + (d < 10 ? "0" + dd : dd) + "-" + (m < 10 ? "0" + mm : mm) + "-" +  c.get(GregorianCalendar.YEAR);
		}
	
	/**
	 * Metodo para validar si los atributos del request
	 * contienen JavaScript inyectado, de ser asi, lo elimina.
	 * @param parametros, el valor del parametro a validar
	 * @return parametro, vacio si encuentra codigo script
	 */
	public static boolean contieneScript(String parametro){
	     
	    if (parametro != null){
	    	
	    	if (parametro.contains(IEnlace.CADENA_INICIAL_SCRIPT.toLowerCase()) || 
	    			parametro.contains(IEnlace.CADENA_INICIAL_SCRIPT.toUpperCase())
	    			|| parametro.contains(IEnlace.CADENA_FINAL_SCRIPT.toLowerCase())
	    			|| parametro.contains(IEnlace.CADENA_FINAL_SCRIPT.toUpperCase())
	    			|| parametro.contains(IEnlace.ABRIR_TAG) || parametro.contains(IEnlace.CERRAR_TAG)) {
	        	
	    			return true;
	        }
	    }
	    return false;
	}
	
	/**
	 * M�todo para validar si los parametros del request traen inyeccion de JavaScript,
	 * en caso de traer, el parametro se resetea.
	 * @param paramMaps, incluye todos los parametros del request
	 * @return boolean, true si encuentra javascript
	 */
	public static boolean validarRequest(Map<String, String[]> paramMaps){
		for(Map.Entry<String, String[]> parametro : paramMaps.entrySet()){
        	for(String valor : parametro.getValue()){
        		if(contieneScript(valor)){
        			return true;
        		}
        	}
        }
		return false;
	}	
}


