package mx.altec.enlace.bita;

import mx.altec.enlace.bita.BitaBean;

public interface BitaHelper {



	public long retrieveFolioFlujo();
	public String retrieveIdFlujo();

	public java.util.Date retrieveFecha();
	public String retrieveHora();
	public String retrieveDirIp();
	public String retrieveIdToken();
	public String retrieveCanal();
	public String retrieveUsr();
	public String retrieveIdWeb();
	public String retrieveIdSesion();
	public String retrieveNombreHostWeb();

	public String retrieveTransacCodCliente();
	public BitaBean llenarBean(BitaBean b);
	public long incrementaFolioFlujo(String idFlujo);
	public long incrementaFolioBitacora();

	public String getDBJNDIKey();

	public BitaTCTBean llenarBeanTCT(BitaTCTBean b);
}
