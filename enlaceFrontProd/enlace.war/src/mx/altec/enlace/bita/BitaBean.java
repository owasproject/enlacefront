package mx.altec.enlace.bita;

import java.util.Date;

public class BitaBean {

	private long folioBit;
	private long folioFlujo;
	private String idFlujo;
	private String numBit;
	private Date fecha;
	private String hora;
	private long referencia;
	private String servTransTux;
	private String dirIp;
	private String idToken;
	private String canal;
	private String usr;
	private String idWeb;
	private String idSesion;
	private String nombreHostWeb;
	private String codCliente=" ";



	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getDirIp() {
		return dirIp;
	}
	public void setDirIp(String dirIp) {
		this.dirIp = dirIp;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public long getFolioFlujo() {
		return folioFlujo;
	}
	public void setFolioFlujo(long folioFlujo) {
		this.folioFlujo = folioFlujo;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getIdFlujo() {
		return idFlujo;
	}
	public void setIdFlujo(String idFlujo) {
		this.idFlujo = idFlujo;
	}
	public String getIdSesion() {
		return idSesion;
	}
	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	public String getIdToken() {
		return idToken;
	}
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	public String getIdWeb() {
		return idWeb;
	}
	public void setIdWeb(String idWeb) {
		this.idWeb = idWeb;
	}
	public String getNombreHostWeb() {
		return nombreHostWeb;
	}
	public void setNombreHostWeb(String nombreHostWeb) {
		this.nombreHostWeb = nombreHostWeb;
	}
	public String getNumBit() {
		return  this.numBit;
	}
	public void setNumBit(String numBit) {
		this.numBit= numBit.substring(4,6);
	}
	public long getReferencia() {
		return referencia;
	}
	public void setReferencia(long referencia) {
		this.referencia = referencia;
	}
	public String getServTransTux() {
		return servTransTux;
	}
	public void setServTransTux(String servTransTux) {
		this.servTransTux = servTransTux;
	}
	public String getUsr() {
		return usr;
	}
	public void setUsr(String usr) {
		this.usr = usr;
	}
	public long getFolioBit() {
		return folioBit;
	}
	public void setFolioBit(long folioBit) {
		this.folioBit = folioBit;
	}

	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}



}
