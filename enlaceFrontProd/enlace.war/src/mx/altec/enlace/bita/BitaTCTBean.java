package mx.altec.enlace.bita;

import java.util.Date;

public class BitaTCTBean {
    int referencia;
    String uVersion;
    String numCuenta;
    Date  fecha;
    String hora;
    String tipoOperacion;
    double importe;
    long noTitulos;
    String cuentaOrigen;
    String cuentaDestinoFondo;
    String cuentaEnlace;
    String usuario;
    String codError;
    String tipoCtaDestino;
    String plazaOrigen;
    String plazaDestino;
    String plazaEnlace;
    long rutaOrigen;
    long rutaDestino;
    long rutaEnlace;
    String medioEntrega;
    String operador;
    long proteccion1;
    long proteccion2;
    long proteccion3;
    String dispositivo1;
    String dispositivo2;
    String concepto;

    public BitaTCTBean()
    {
        referencia= -1;
        uVersion = "";
        numCuenta = "" ;
        fecha = null;
        hora = "";
        tipoOperacion ="";
        importe = 0;
        noTitulos = 0;
        cuentaOrigen = "";
        cuentaDestinoFondo = "";
        cuentaEnlace = "";
        usuario = "";
        codError = "";
        tipoCtaDestino = "";
        plazaOrigen = "";
        plazaDestino ="";
        plazaEnlace = "";
        rutaOrigen = 0;
        rutaDestino = 0;
        rutaEnlace =0;
        medioEntrega = "";
        operador = "";
        proteccion1 = 0;
        proteccion2 = 0;
        proteccion3 = 0;
        dispositivo1 = "";
        dispositivo1 ="";
        concepto = "";

    }
	public String getCodError() {
		return codError;
	}
	public void setCodError(String codError) {
		this.codError = codError;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCuentaDestinoFondo() {
		return cuentaDestinoFondo;
	}
	public void setCuentaDestinoFondo(String cuentaDestinoFondo) {
		this.cuentaDestinoFondo = cuentaDestinoFondo;
	}
	public String getCuentaEnlace() {
		return cuentaEnlace;
	}
	public void setCuentaEnlace(String cuentaEnlace) {
		this.cuentaEnlace = cuentaEnlace;
	}
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public String getDispositivo1() {
		return dispositivo1;
	}
	public void setDispositivo1(String dispositivo1) {
		this.dispositivo1 = dispositivo1;
	}
	public String getDispositivo2() {
		return dispositivo2;
	}
	public void setDispositivo2(String dispositivo2) {
		this.dispositivo2 = dispositivo2;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getMedioEntrega() {
		return medioEntrega;
	}
	public void setMedioEntrega(String medioEntrega) {
		this.medioEntrega = medioEntrega;
	}
	public long getNoTitulos() {
		return noTitulos;
	}
	public void setNoTitulos(long noTitulos) {
		this.noTitulos = noTitulos;
	}
	public String getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public String getPlazaDestino() {
		return plazaDestino;
	}
	public void setPlazaDestino(String plazaDestino) {
		this.plazaDestino = plazaDestino;
	}
	public String getPlazaEnlace() {
		return plazaEnlace;
	}
	public void setPlazaEnlace(String plazaEnlace) {
		this.plazaEnlace = plazaEnlace;
	}
	public String getPlazaOrigen() {
		return plazaOrigen;
	}
	public void setPlazaOrigen(String plazaOrigen) {
		this.plazaOrigen = plazaOrigen;
	}
	public long getProteccion1() {
		return proteccion1;
	}
	public void setProteccion1(long proteccion1) {
		this.proteccion1 = proteccion1;
	}
	public long getProteccion2() {
		return proteccion2;
	}
	public void setProteccion2(long proteccion2) {
		this.proteccion2 = proteccion2;
	}
	public long getProteccion3() {
		return proteccion3;
	}
	public void setProteccion3(long proteccion3) {
		this.proteccion3 = proteccion3;
	}

	public int getReferencia() {
		return referencia;
	}
	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}
	public long getRutaDestino() {
		return rutaDestino;
	}
	public void setRutaDestino(long rutaDestino) {
		this.rutaDestino = rutaDestino;
	}
	public long getRutaEnlace() {
		return rutaEnlace;
	}
	public void setRutaEnlace(long rutaEnlace) {
		this.rutaEnlace = rutaEnlace;
	}
	public long getRutaOrigen() {
		return rutaOrigen;
	}
	public void setRutaOrigen(long rutaOrigen) {
		this.rutaOrigen = rutaOrigen;
	}
	public String getTipoCtaDestino() {
		return tipoCtaDestino;
	}
	public void setTipoCtaDestino(String tipoCtaDestino) {
		this.tipoCtaDestino = tipoCtaDestino;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getUVersion() {
		return uVersion;
	}
	public void setUVersion(String version) {
		uVersion = version;
	}

}