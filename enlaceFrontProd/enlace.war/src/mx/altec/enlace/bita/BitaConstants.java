package mx.altec.enlace.bita;

/**
 * The Class BitaConstants.
 */
public class BitaConstants {

	/**
	 * Constante para la cadena confirmaci.
	 */
	public static final String CONFIRMACI = "Confirmaci";
	/**
	 * Constante para la cadena "mina Interbancaria".
	 */
	public static final String NOM_INT = "mina Interbancaria";
	/**
	 * Cadena para la cadena "logo de N".
	 */
	public static final String CAD_LOGO = "logo de N";
	/**
	 * Atributo static y final para guardar valor constante manejado en el
	 * codigo.
	 */
	public static final char A_MIN_ACENTUADA = (char) 225;
	/**
	 * Atributo static y final para guardar valor constante manejado en el
	 * codigo.
	 */
	public static final char E_MIN_ACENTUADA = (char) 233;
	/**
	 * Atributo static y final para guardar valor constante manejado en el
	 * codigo.
	 */
	public static final char I_MIN_ACENTUADA = (char) 237;
	/**
	 * Atributo static y final para guardar valor constante manejado en el
	 * codigo.
	 */
	public static final char O_MIN_ACENTUADA = (char) 243;

	// CONSTANTES PARA EL MANEJO DE SESION
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String SESS_FOLIO_FLUJO = "FolioFlujo";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String SESS_ID_FLUJO = "IdFlujo";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String FLUJO = "flujo";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static String archivo;

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String SESS_BAND_TOKEN = "BandToken";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String VALIDA = "valida";
	// CONSTANTES DE INICIO DE SESIÃ“N
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String E_INICIO_SESION = "EISE";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String E_INICIO_SESION_ENTRA = "EISE01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String E_INICIO_SESION_AMBIENTA_SESION = "EISE02";

	// CONSTANTES DEL MÃ“DULO DE ENLACE-CONSULTAS
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_CHEQUE = "ECCC";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_CHEQUE_ENTRA = "ECCC01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_CHEQUE_CONS_SALDO_EFEC = "ECCC02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_CHEQUE_CONS_POS_CUENTA = "ECCC03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA = "ECCB";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA_ENTRA = "ECCB01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA_CONS_SALDO_CUENTA = "ECCB02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA_CONS_POS_CUENTA = "ECCB03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA_CONS_POS_BANCA = "ECCB04";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_BANCA_CONS_SOLO_POS_CUENTA = "ECCB05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_TARJETA = "ECCT";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_TARJETA_ENTRA = "ECCT01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_TARJETA_CONS_SALDO_CREDITO = "ECCT02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_TARJETA_CONS_POS_TARJETA = "ECCT03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CUENTA_TARJETA_CONS_POS_LINEA_CREDITO = "ECCT04";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE = "ECCH";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE_ENTRA = "ECCH01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE_CONS_SALDO_EFECTIVO = "ECCH02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE_CONS_POS_CUENTA = "ECCH03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE_CONS_COTIZA_INTERNACIONAL = "ECCH04";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_SALDO_CONS_CHEQUE_CONS_COTIZA_INTER_DEPOSITO = "ECCH05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_CHEQUERAS = "ECPS";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_CHEQUERAS_ENTRA = "ECPS01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_CHEQUERAS_CONS_POS_CHEQUERA = "ECPS02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA = "ECPB";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA_ENTRA = "ECPB01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA_CONS_SALDOS = "ECPB02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA_CONS_POS_SALDOS = "ECPB03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA_CONS_TRAE_POS_SALDOS = "ECPB04";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_BANCA_CONS_POS = "ECPB05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_TARJETA = "ECPT";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_TARJETA_ENTRA = "ECPT01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_TARJETA_CONS_SALDO_CREDITO = "ECPT02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_TARJETA_ENTRA_CONS_POS_TARJETA = "ECPT03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_POSICION_TARJETA_ENTRA_CONS_POS_LINEA_CREDITO = "ECPT04";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_LINEA = "ECCL";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_LINEA_ENTRA = "ECCL01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_LINEA_CONS_MOVS_CUENTA_CHEQUE = "ECCL02";

	// INTRBANCARIAS RECIBIDAS//
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS = "CTIR";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS_ENTRA = "CTIR01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS_POS = "CTIR02";
 
	//*******************INICIA TCS FSW 12/2016***********************************
	// 		INTRBANCARIAS RECIBIDAS DOLARES		//
	
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS_DOLAR = "TIRD";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS_DOLAR_ENTRA = "TIRD01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_INT_RECIBIDAS_DOLAR_POS = "TIRD02";

	//*******************FIN TCS FSW 12/2016***********************************
	
	
	
	
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO = "ECPR";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO_ENTRA = "ECPR01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO_TUX_PROG_VIG = "ECPR02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO_ALTA_REG_CONS_PROG = "ECPR03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO_CANCEL_PROGRAMACIONES = "ECPR04";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_REGISTRO_MODIF_PROGRAMACIONES = "ECPR05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_CONSULTA = "ECPC";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_CONSULTA_ENTRA = "ECPC01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_CHEQ_PROG_CONSULTA_RES_CONS_PROG = "ECPC02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_BANCA = "ECMB";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_BANCA_ENTRA = "ECMB01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_BANCA_CONS_MOVS_CUENTA_CHEQUE = "ECMB02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_TARJETA = "ECMT";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_TARJETA_ENTRA = "ECMT01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_TARJETA_CONSULTA_MOVS_TARJETA = "ECMT02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_TARJETA_CONS_INTERBANC = "ECMT03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_MULTICHEQUE = "ECMM";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_MULTICHEQUE_ENTRA = "ECMM01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_MOV_MULTICHEQUE_CONSULTA_MULTICHEQUE = "ECMM02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_OPER = "ECMO";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_OPER_ENTRA = "ECMO01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_OPER_CONSULTA_OPER_PROG = "ECMO02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_OPER_CONSULTA_OPER_PROG_CANCELACION = "ECMO03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA = "ECBI";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA_ENTRA = "ECBI01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA_CONSULTA_BIT_ACTUALIZA = "ECBI02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA_CONSULTA_BIT_OPERACIONES = "ECBI03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA_CONSULTA_BIT_DETALLE_DIV = "ECBI04";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_BITACORA_CONSULTA_BIT_DETALLE_INT = "ECBI05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_INFORME = "ECIN";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_INFORME_ENTRA = "ECIN01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_INFORME_CONS_COMISIONES_COB = "ECIN02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EC_INFORME_CONS_RECUP_INTERBANC = "ECIN03";

	// CONSTANTES DEL MÃ“DULO DE ENLACE-ADMINISTRACIÃ“N
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO = "EAMC";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_ENTRA = "EAMC01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_CONS_CUENTAS_MANC = "EAMC02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_REALIZA_OPERACIONES = "EAMC03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_GEN_FOLIOS = "EAMG";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_GEN_FOLIOS_ENTRA = "EAMG01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_GEN_FOLIOS_GENERA_FOLIO = "EAMG02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_OPER_INTER = "EAMO";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_OPER_INTER_ENTRA = "EAMO01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_OPER_INTER_CONS_CUENTAS_MANCOM = "EAMO02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_OPER_INTER_REALIZA_OPERACIONES = "EAMO03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_ARCH = "EAMA";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_ARCH_ENTRA = "EAMA01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_ARCH_CONS_CUENTAS_MANCOM = "EAMA02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_MANCOM_CONS_AUTO_ARCH_REALIZA_OPERACIONES = "EAMA03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRATO = "EACC";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRATO_ENTRA = "EACC01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRATO_TF_AMBIENTA_CONTRATO = "EACC02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRASENA = "EACO";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRASENA_ENTRA = "EACO01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRASENA_REALIZA_CAMBIO_PASS = "EACO02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CAMBIO_CONTRASENA_CIERRA_DUPLICIDAD = "EACO03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_CONSULTA = "EACN";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_CONSULTA_ENTRA = "EACN01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_CONSULTA_CONSULTA_CLABE = "EACN02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_CONSULTA_CONSULTA_CUENTAS_INTER = "EACN03";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_CONSULTA_CONSULTA_OTROS_BANCOS = "EACN04";
	/** Constante de bitacora para consultas de cuentas con numero movil. */
	public static final String EA_CUENTAS_CONSULTA_CONSULTA_NUMERO_MOVIL = "EACN05";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_ALTA = "EACA";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_ALTA_ENTRA = "EACA01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_ALTA_REALIZA_ALTA = "EACA02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_ALTA_IMPORTAR_ARCHIVO = "EACA03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_BAJA = "EACB";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_BAJA_ENTRA = "EACB01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_BAJA_REALIZA_BAJA = "EACB02";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_AUTO_CANC = "EAAC";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_AUTO_CANC_ENTRA = "EAAC01";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_AUTO_CANC_CONSULTA_CUENTA = "EAAC02";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_CUENTAS_AUTO_CANC_AUTORIZA_ALTA_BAJA = "EAAC03";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_USUARIOS = "EAUS";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_USUARIOS_ENTRA = "EAUS01";

	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_GEN_MANUAL_ARCHIVOS = "EAGM";
	/**
	 * Atributo static y final para guardar cadena constante manejada en el
	 * codigo.
	 */
	public static final String EA_GEN_MANUAL_ARCHIVOS_ENTRA = "EAGM01";

	// Stefanini constantes RSA
	/** enrolamiento*. */
	public static final String EA_ENROLAMIENTO_RSA = "ENRO01";

	/** desvinculacion*. */
	public static final String EA_DESV_DISP = "DDIS01";

	/** cambio imagen*. */
	public static final String EA_CAMBIO_IMAGEN = "REIM01";

	/** cambio pregunta*. */
	public static final String EA_CAMBIO_PREGUNTA = "REPS01";

	// fin Stefanini constantes RSA
	// Stefanini constantes cifrado
	/** cifrado*. */
	public static final String EA_CIFFRADO = "CIFR01";
	// FinStefanini constantes cifrado

	/** CLAVE DE BITACORA CONSULTA DE TRANSFERENCIAS INTERNACIONALES RECIBIDAS */
	public static final String CONS_TRAN_INTER_REC = "CTIT";
	/** CONSULTA DE TRANSFERENCIAS INTERNACIONALES RECIBIDAS 01 */
	public static final String EA_CONS_TRAN_INTER_REC = "CTIT01";

	// [INICIA] Enlace Internet Constantes PDF XML - IRF
	/** Configuracion Individual *. */
	/** CONFIRMA ACEPTACION DE CONVENIO PAPERLESS **/
	public static final String EA_ALTA_INSCRIPCION_EDO_CTA_PAPERLESS = "IECP";
	/** Entra a ConfiguraciÃ³n de Estados de Cuenta Individual*. */
	// public static final String EA_INGRESA_CONFIG_EDO_CTA_IND = "CIED00";
	public static final String EA_INGRESA_CONFIG_EDO_CTA_IND = "IECP00";
	/** Selecciona opciones de configuraciÃ³n Individual - PDF*. */
	// public static final String EA_OP_PDF_EDO_CTA_IND = "CIED01";
	public static final String EA_OP_PDF_EDO_CTA_IND = "IECP01";
	/** Confirma ConfiguraciÃ³n Estado de Cuenta Individual - PDF*. */
	public static final String EA_CONFIG_PDF_EDO_CTA_IND = "CECI00";
	/** Finaliza la configuraciÃ³n de Estado de Cuenta Individual - PDF*. */
	public static final String EA_FIN_PDF_EDO_CTA_IND = "CIED02";
	/** CONCEPTO CONFIG_IND_EDO_CUENTA*. */
	public static final String CONCEPTO_CONF_CONFIG_EDO_CUENTA_IND = "Confirma Modificaci"
			+ O_MIN_ACENTUADA
			+ "n Estado de Env"
			+ I_MIN_ACENTUADA
			+ "o de Estado de Cuenta Individual";

	/** INSCRIPCION EDO_CUENTA PAPERLESS*. */
	public static final String INSCRIPCION_EDO_CUENTA_PAPERLESS = "Inscripci"
			+ O_MIN_ACENTUADA + "n Estado de Cuenta a Paperless Individual";

	/** CONFIRMA ACEPTACION DE CONVENIO PAPERLESS *. */
	public static final String EA_CONV_ACEPT_PAPERLESS_CONFIRM = "ACAP";

	/** ACEPTA CONVENIO DE ACEPTACION PAPERLESS *. */
	public static final String EA_ACEPT_CONV_ACEPT_PAPERLESS = "ACAP00";

	/** CONCEPTO CONFIG_IND_EDO_CUENTA *. */
	public static final String CONCEPTO_ACEPTA_CONV_PAPERLESS = "Acepta Convenio de Aceptaci"
			+ O_MIN_ACENTUADA + "n Paperless";
	/** CONF_CONFIG_EDO_CUENTA_IND*. */
	public static final String CONF_CONFIG_EDO_CUENTA_IND = "CECI";
	/** CONFIG_IND_EDO_CUENTA*. */
	public static final String CONFIG_IND_EDO_CUENTA_ = "CIED";

	/** CONFIG_IND_EDO_CUENTA ALTA DE PAPERLESS*. */
	public static final String CONFIG_IND_EDO_CUENTA_PLESS = "IECP02";
	/** Codigo de operacion para baja de estado de cuenta paperless. **/
	public static final String EDO_CUENTA_IND_BAJA_PAPERLESS = "BICP";
	/** Concepto para baja de estado de cuenta paperless. **/
	public static final String CONCEPTO_BAJA_PAPERLESS = "Baja Inscripci"
			+ O_MIN_ACENTUADA + "n Estado de Cuenta Paperless Individual";

	/** CONFIG_IND_EDO_CUENTA BAKA DE PAPERLESS*. */
	public static final String CONFIG_IND_EDO_CUENTA_BPLESS = "BICP00";

	/** COD_ERR_PDF_XML. */
	public static final String COD_ERR_PDF_XML = "ECTA0000";

	/** Confirma ConfiguraciÃ³n Estado de Cuenta Masiva. */
	public static final String EA_ESTADOS_CUENTA_ARCHIVO_CONFIRM = "CECM";

	/** ConfiguraciÃ³n Masiva Edo Cuenta*. */
	public static final String CMED = "Modificaci" + O_MIN_ACENTUADA
			+ "n Paperless Masivo";

	/**
	 * Concepto correspondiente a configuracion de estado de cuenta masivo para
	 * el guardado de bitacora de operaciones.
	 */
	public static final String CECM = "Confirma Modificaci" + O_MIN_ACENTUADA
			+ "n Paperless Masiva";

	/** Codigo de operacion para ingreso al modulo de paperless masivo. */
	public static final String EA_ENTRA_MOD_PAPERLESS_MASIVO = "CMED00";

	/** Codigo de operacion para seleccion de cuentas de paperless masivo. */
	public static final String EA_SELEC_CTAS_PAPERLESS_MASIVO = "CMED01";

	/**
	 * Codigo de operacion para modificacion de suscripcion de paperless masivo.
	 */
	public static final String EA_MODIF_EDO_SUSCRIP_PAPERLESS_MASIVO = "CMED02";

	/** Codigo de operacion para aceptacion de convenio de paperless masivo. */
	public static final String EA_ACEPT_CONV_PAPERLESS_MASIVO = "ACAP01";

	/**
	 * Codigo de operacion para confirmacion de modificacion de paperless
	 * masivo.
	 */
	public static final String EA_CONFR_MODIF_PAPERLESS_MASIVO = "CECM00";

	/**
	 * Codigo de operacion para finalizacion de modificacion de paperless
	 * masivo.
	 */
	public static final String EA_FINAL_MODIF_PAPERLESS_MASIVO = "CMED03";

	/**
	 * Codigo de operacion para finalizacion de modificacion de paperless masivo
	 * (bitacora de operaciones).
	 */
	public static final String EA_ESTADOS_CUENTA_ARCHIVO_FIN = "CMED";

	/** entra configuracion*. */
	public static final String EA_ENTRA_CONFIRACION_ARCHIVO_EDOS_CUENTA = "CMED00";

	/** seleccion de archivo*. */
	public static final String EA_SELECCION_ARCHIVO_EDOS_CUENTA = "CMED01";

	/** acepta registros archivo*. */
	public static final String EA_ACEPTA_ARCHIVOS_EDOS_CUENTA = "CMED02";

	/** finaliza configuracion*. */
	public static final String EA_FIN_CONFIGURACION_EDOS_CUENTA = "CMED03";

	/** confirma configiracion de archivo*. */
	public static final String EA_CONFIRMA_CONFIRM_EDOS_CUENTA = "CECM00";

	/** seleccion cuentas y formato selecciÃ³n cuentas*. */
	public static final String EA_SELECCION_CUENTAS_FORMATO_CUENTA_EDOS_CUENTA = "CMED04";

	/** seleccion configura cuentas por selecciÃ³n cuentas*. */
	public static final String EA_CONFIGURA_PDF_CUENTA_EDOS_CUENTA = "CMED05";

	/** seleccion confirma configuraciÃ³n de cuentas por selecciÃ³n cuentas*. */
	public static final String EA_CONFIRMA_PDF_CUENTA_EDOS_CUENTA = "CECM01";

	/** seleccion fin de configuraciÃ³n de cuentas por selecciÃ³n cuentas*. */
	public static final String EA_FIN_CONFIGURA_PDF_CUENTA_EDOS_CUENTA = "CMED06";

	/** seleccion configura cuentas por selecciÃ³n cuentas*. */
	public static final String EA_CONFIGURA_XML_CUENTA_EDOS_CUENTA = "CMED07";

	/** seleccion confirma configuraciÃ³n de cuentas por selecciÃ³n cuentas*. */
	public static final String EA_CONFIRMA_XML_CUENTA_EDOS_CUENTA = "CECM02";

	/** seleccion fin de configuraciÃ³n de cuentas por selecciÃ³n cuentas*. */
	public static final String EA_FIN_CONFIGURA_XML_CUENTA_EDOS_CUENTA = "CMED08";

	/** Selecciona opciones de configuraciÃ³n Individual - XML*. */
	public static final String EA_OP_XML_EDO_CTA_IND = "CIED03";

	/** Confirma ConfiguraciÃ³n Estado de Cuenta Individual - XML*. */
	public static final String EA_CONFIG_XML_EDO_CTA_IND = "CECI01";

	/** Finaliza la configuraciÃ³n de Estado de Cuenta Individual - XML*. */
	public static final String EA_FIN_XML_EDO_CTA_IND = "CIED04";

	/** CONSULTA_DET_EDO_CUENTA*. */
	public static final String CONSULTA_DET_EDO_CUENTA = "CCON";

	/** CONSULTA_EDO_CUENTA*. */
	public static final String CONSULTA_EDO_CUENTA = "CCON00";

	/** CONSULTA_RES_EDO_CUENTA*. */
	public static final String CONSULTA_RES_EDO_CUENTA = "CCON01";

	/** CONSULTA_DETALLE_EDO_CUENTA*. */
	public static final String CONSULTA_DETALLE_EDO_CUENTA = "CCON02";

	/** CONCEPTO CONSULTA_EDO_CUENTA*. */
	public static final String CONCEPTO_CONSULTA_EDO_CUENTA = "Consulta de estado de modificaci"
			+ O_MIN_ACENTUADA + "n de Paperless";

	/** SOLCICITUD DE ESTADOS HISTORICOS. */
	public static final String SOLICITUD_ESTADOS_HISTORICOS = "DEDA";

	/** ENTRA SOLICITUD ESTADOS HISTORICOS*. */
	public static final String ENTRA_SOLICITUD_ESTADOS_HISTORICOS = "DEDA01";

	/** SELECCION PERIODOS HISTORICOS XML*. */
	public static final String SELECCIONA_PERIODOS_HISTORICOS_PDF = "DEDA02";

	/** FINALIZA SOLICITUD DE PERIODOS HISTORICOS PDF*. */
	public static final String FINALIZA_SOLICITUD_PERIODOS_HISTORICOS_PDF = "DEDA03";

	/** CONCEPTO SOLICITUD ESTADOS HISTORICOS. */
	public static final String CONCEPTO_SOL_EDO_HIST = "Solicitud de Periodos Hist"
			+ O_MIN_ACENTUADA + "ricos PDF";

	/** Codigo de operacion para DEDC06 para pistas de auditoria. */
	public static final String DESC_EDOCTA_DEDC06 = "DEDC06";

	/** Codigo de operacion para DEDC07 para pistas de auditoria. */
	public static final String DESC_EDOCTA_DEDC07 = "DEDC07";

	/** TOKEN VALIDO SOLCICITUD DE ESTADOS HISTORICOS. */
	public static final String TOKEN_VALIDO_SOL_EDO_HIST = "CSPH";

	/** CONFIRMACION PERIODOS HISTORICOS PDF*. */
	public static final String CONFIRMACION_PERIODOS_HISTORICOS_PDF = "CSPH00";

	/** CONCEPTO SOLICITUD ESTADOS HISTORICOS. */
	public static final String CONCEPTO_TOKEN_VALIDO_SOL_EDO_HIST = "Confirma Solicitud de Periodos Hist"
			+ O_MIN_ACENTUADA + "ricos";

	/** CONF_DESC_EDOCTA_CDEC*. */
	public static final String CONF_DESC_EDOCTA_CDEC = "CDEC";

	/** Concepto para token valido en descarga de estados de cuenta. */
	public static final String TOKEN_VALIDO_DESCARGA_EDO_CTA = "Confirma Descarga de Estados de Cuenta";

	/** Concepto para descarga de estados de cuenta. */
	public static final String CONCEPTO_DESCARGA_EDO_CTA = "Descarga de Estados de Cuenta";

	/** DESC_EDOCTA_DEDC*. */
	public static final String DESC_EDOCTA_DEDC = "DEDC";

	/** [DEDC-00] Entra a pantalla de descarga de Estados de Cuenta - PDF. */
	public static final String DESC_EDOCTA_DEDC00 = "DEDC00";

	/** [DEDC-01] Selecciona Periodo para Descarga - PDF. */
	public static final String DESC_EDOCTA_DEDC01 = "DEDC01";

	/** [CDEC-00] Confirma Descarga de Estados de Cuenta - PDF *. */
	public static final String DESC_EDOCTA_CDEC00 = "CDEC00";

	/** [DEDC-02] Inicia proceso de Descarga de Estado de Cuenta - PDF. */
	public static final String DESC_EDOCTA_DEDC02 = "DEDC02";

	/** [DEDC-03] Entra a pantalla de descarga de Estados de Cuenta - XML. */
	public static final String DESC_EDOCTA_DEDC03 = "DEDC03";

	/** [DEDC-04] Selecciona Cuenta y Periodo para Descarga - XML. */
	public static final String DESC_EDOCTA_DEDC04 = "DEDC04";

	/** [CDEC-01] Confirma Descarga de Estados de Cuenta - XML. */
	public static final String DESC_EDOCTA_CDEC01 = "CDEC01";

	/** [DEDC-05] Inicia proceso de Descarga de Estado de Cuenta - XML. */
	public static final String DESC_EDOCTA_DEDC05 = "DEDC05";

	/** ES_SOL_ESTADO_CUENTA_HISTORICO_XML*. */
	public static final String ES_SOL_ESTADO_CUENTA_HISTORICO_XML = "DEDA01";

	/** ES_SOL_ESTADO_CUENTA_HISTORICO_PDF*. */
	public static final String ES_SOL_ESTADO_CUENTA_HISTORICO_PDF = "DEDA00";

	/** CONSULTA_DET_HIST*. */
	public static final String CONSULTA_DET_HIST = "CONH";

	/** CONSULTA_HIST*. */
	public static final String CONSULTA_HIST = "CONH00";

	/** CONSULTA_RES_HIST*. */
	public static final String CONSULTA_RES_HIST = "CONH01";

	/** CONCEPTO_CONSULTA_HIST*. */
	public static final String CONCEPTO_CONSULTA_HIST = "Consulta de solicitud de Periodos Hist"
			+ O_MIN_ACENTUADA + "ricos PDF";

	// [FIN] Enlace Internet Constantes PDF XML - IRF

	// CONSTANTES DEL MÃ“DULO DE ENLACE-SERVICIOS
	/** CONSTANTES DEL MÃ“DULO DE ENLACE-SERVICIOS. */
	public static final String ES_CONSULTA = "ESCO";

	/** CONSTANTES DEL MÃ“DULO DE ENLACE-SERVICIOS. */
	public static final String ES_CONSULTA_ENTRA = "ESCO01";

	/** CONSTANTES DEL MÃ“DULO DE ENLACE-SERVICIOS. */
	public static final String ES_CONSULTA_TUX_GENERA_PAGO = "ESCO02";

	/** CONSTANTES DEL MÃ“DULO DE ENLACE-SERVICIOS. */
	public static final String ES_CONSULTA_CANCELA_PAGP = "ESCO03";

	/** CONSTANTE ES_PAGO_IMP_PROVISIONAL. */
	public static final String ES_PAGO_IMP_PROVISIONAL = "ESIP";

	/** CONSTANTE ES_PAGO_IMP_PROVISIONAL_ENTRA. */
	public static final String ES_PAGO_IMP_PROVISIONAL_ENTRA = "ESIP01";

	/** CONSTANTE ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO. */
	public static final String ES_PAGO_IMP_PROVISIONAL_TUX_GENERA_PAGO = "ESIP02";

	/** CONSTANTE ES_PAGO_IMP_PROVISIONAL_NUEVO_PAGO_IMP. */
	public static final String ES_PAGO_IMP_PROVISIONAL_NUEVO_PAGO_IMP = "ESIP03";

	/** CONSTANTE ES_PAGO_IMP_EJERCICIO. */
	public static final String ES_PAGO_IMP_EJERCICIO = "ESIE";

	/** CONSTANTE ES_PAGO_IMP_EJERCICIO_ENTRA. */
	public static final String ES_PAGO_IMP_EJERCICIO_ENTRA = "ESIE01";

	/** CONSTANTE ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO. */
	public static final String ES_PAGO_IMP_EJERCICIO_TUX_GENERA_PAGO = "ESIE02";

	/** CONSTANTE ES_PAGO_IMP_EJERCICIO_NUEVO_PAGO_IMP. */
	public static final String ES_PAGO_IMP_EJERCICIO_NUEVO_PAGO_IMP = "ESIE03";

	/** CONSTANTE ES_PAGO_IMP_ENT_FED. */
	public static final String ES_PAGO_IMP_ENT_FED = "ESIF";

	/** CONSTANTE ES_PAGO_IMP_ENT_FED_ENTRA. */
	public static final String ES_PAGO_IMP_ENT_FED_ENTRA = "ESIF01";

	/** CONSTANTE ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO. */
	public static final String ES_PAGO_IMP_ENT_FED_TUX_GENERA_PAGO = "ESIF02";

	/** CONSTANTE ES_PAGO_IMP_ENT_FED_NUEVO_PAGO_IMP. */
	public static final String ES_PAGO_IMP_ENT_FED_NUEVO_PAGO_IMP = "ESIF03";

	/** CONSTANTE ES_PAGO_IMP_DER_PROD_APR. */
	public static final String ES_PAGO_IMP_DER_PROD_APR = "ESID";

	/** CONSTANTE ES_PAGO_IMP_DER_PROD_APR_ENTRA. */
	public static final String ES_PAGO_IMP_DER_PROD_APR_ENTRA = "ESID01";

	/** CONSTANTE ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO. */
	public static final String ES_PAGO_IMP_DER_PROD_APR_TUX_GENERA_PAGO = "ESID02";

	/** CONSTANTE ES_PAGO_IMP_DER_PROD_APR_NUEVO_PAGO_IMP. */
	public static final String ES_PAGO_IMP_DER_PROD_APR_NUEVO_PAGO_IMP = "ESID03";

	/** CONSTANTE ES_PAGO_LINEA_CAPTURA. */
	public static final String ES_PAGO_LINEA_CAPTURA = "ESLC";

	/** CONSTANTE ES_PAGO_LINEA_CAPTURA_ENTRA. */
	public static final String ES_PAGO_LINEA_CAPTURA_ENTRA = "ESLC01";

	/** CONSTANTE ES_PAGO_LINEA_CAPTURA_REALIZA_VALIDACION. */
	public static final String ES_PAGO_LINEA_CAPTURA_REALIZA_VALIDACION = "ESLC02";

	/** CONSTANTE ES_AP_OP_PAGOS. */
	public static final String ES_AP_OP_PAGOS = "ESOP";

	/** CONSTANTE ES_AP_OP_PAGOS_ENTRA. */
	public static final String ES_AP_OP_PAGOS_ENTRA = "ESOP01";

	/** CONSTANTE ES_AP_OP_PAGOS_IMPORTA_ARCHIVO. */
	public static final String ES_AP_OP_PAGOS_IMPORTA_ARCHIVO = "ESOP02";

	/** CONSTANTE ES_AP_OP_PAGOS_OBTIENE_REF_SUA. */
	public static final String ES_AP_OP_PAGOS_OBTIENE_REF_SUA = "ESOP03";

	/** CONSTANTE ES_AP_OP_PAGOS_REALIZA_PAGO_IMSS. */
	public static final String ES_AP_OP_PAGOS_REALIZA_PAGO_IMSS = "ESOP04";

	/** CONSTANTE ES_AP_OP_PAGOS_REALIZA_CONSULTA_SUA. */
	public static final String ES_AP_OP_PAGOS_REALIZA_CONSULTA_SUA = "ESOP05";

	/** CONSTANTE ES_AP_OP_CONSULTAS. */
	public static final String ES_AP_OP_CONSULTAS = "ESOC";

	/** CONSTANTE ES_AP_OP_CONSULTAS_ENTRA. */
	public static final String ES_AP_OP_CONSULTAS_ENTRA = "ESOC01";

	/** CONSTANTE ES_AP_OP_CONSULTAS_CONSULTA_PAGO. */
	public static final String ES_AP_OP_CONSULTAS_CONSULTA_PAGO = "ESOC02";

	/** CONSTANTE ES_NOMINA_EMP_MANTEN. */
	public static final String ES_NOMINA_EMP_MANTEN = "ESEM";

	/** CONSTANTE ES_NOMINA_EMP_MANTEN_ENTRA. */
	public static final String ES_NOMINA_EMP_MANTEN_ENTRA = "ESEM01";

	/** CONSTANTE ES_NOMINA_EMP_MANTEN_ENVIAR. */
	public static final String ES_NOMINA_EMP_MANTEN_ENVIAR = "ESEM02";

	/** CONSTANTE ES_NOMINA_EMP_MANTEN_RECUPERA_ARCHIVO_NOMINA. */
	public static final String ES_NOMINA_EMP_MANTEN_RECUPERA_ARCHIVO_NOMINA = "ESEM03";

	/** CONSTANTE ES_NOMINA_EMP_MANTEN_IMPORTAR_ARCHIVO. */
	public static final String ES_NOMINA_EMP_MANTEN_IMPORTAR_ARCHIVO = "ESEM04";

	/** CONSTANTE ES_NOMINA_EMP_CONS_ALTA. */
	public static final String ES_NOMINA_EMP_CONS_ALTA = "ESEC";

	/** CONSTANTE ES_NOMINA_EMP_CONS_ALTA_ENTRA. */
	public static final String ES_NOMINA_EMP_CONS_ALTA_ENTRA = "ESEC01";

	/** CONSTANTE ES_NOMINA_EMP_CONS_ALTA_CONSULTA_GENERICA_ALTA. */
	public static final String ES_NOMINA_EMP_CONS_ALTA_CONSULTA_GENERICA_ALTA = "ESEC02";

	/** CONSTANTE ES_NOMINA_PAGOS_IMP_ENVIO. */
	public static final String ES_NOMINA_PAGOS_IMP_ENVIO = "ESPI";

	/** CONSTANTE ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA. */
	public static final String ES_NOMINA_PAGOS_IMP_ENVIO_ENTRA = "ESPI01";

	/** CONSTANTE ES_NOMINA_PAGOS_IMP_ENVIO_IMPORTA_ARCH_NOMINA. */
	public static final String ES_NOMINA_PAGOS_IMP_ENVIO_IMPORTA_ARCH_NOMINA = "ESPI02";

	/** Clave LNPI Clave envio de pago. */
	public static final String ES_NOMINALN_PAGOS_IMP_ENVIO = "LNPI";

	/** Clave LNPI01 Entra envio pago. */
	public static final String ES_NOMINALN_PAGOS_IMP_ENVIO_ENTRA = "LNPI01";

	/** CONSTANTE ES_NOMINA_PAGOS_CONSULTA. */
	public static final String ES_NOMINA_PAGOS_CONSULTA = "ESPC";

	/** CONSTANTE ES_NOMINA_PAGOS_CONSULTA_ENTRA. */
	public static final String ES_NOMINA_PAGOS_CONSULTA_ENTRA = "ESPC01";

	/** CONSTANTE ES_NOMINA_PAGOS_CONSULTA_REALIZA_CONSULTA. */
	public static final String ES_NOMINA_PAGOS_CONSULTA_REALIZA_CONSULTA = "ESPC02";

	/** Clave LNPC Clave consulta de pagos. */
	public static final String ES_NOMINALN_PAGOS_CONSULTA = "LNPC";

	/** Clave LNPC01 Entra a consulta. */
	public static final String ES_NOMINALN_PAGOS_CONSULTA_ENTRA = "LNPC01";

	/** CONSTANTE ES_NOMINA_INTERBANCARIA. */
	public static final String ES_NOMINA_INTERBANCARIA = "ESNI";

	/** CONSTANTE ES_NOMINA_INTERBANCARIA_ENTRA. */
	public static final String ES_NOMINA_INTERBANCARIA_ENTRA = "ESNI01";

	/** CONSTANTE ES_NOMINA_INTERBANCARIA_VALIDACION_ENVIO. */
	public static final String ES_NOMINA_INTERBANCARIA_VALIDACION_ENVIO = "ESNI02";

	/** CONSTANTE ES_NOMINA_INTERBANCARIA_IMPORTAR_ARCHIVO. */
	public static final String ES_NOMINA_INTERBANCARIA_IMPORTAR_ARCHIVO = "ESNI03";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_LINEA. */
	public static final String ES_CH_SEG_REG_CHEQUES_LINEA = "ESCL";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_LINEA_ENTRA. */
	public static final String ES_CH_SEG_REG_CHEQUES_LINEA_ENTRA = "ESCL01";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_LINEA_INGRESO_CHEQ_SEGU. */
	public static final String ES_CH_SEG_REG_CHEQUES_LINEA_INGRESO_CHEQ_SEGU = "ESCL02";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_ARCHIVO. */
	public static final String ES_CH_SEG_REG_CHEQUES_ARCHIVO = "ESCA";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_ARCHIVO_ENTRA. */
	public static final String ES_CH_SEG_REG_CHEQUES_ARCHIVO_ENTRA = "ESCA01";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_ARCHIVO_INGRESO_CHEQ_SEGU. */
	public static final String ES_CH_SEG_REG_CHEQUES_ARCHIVO_INGRESO_CHEQ_SEGU = "ESCA02";

	/** CONSTANTE ES_CH_SEG_REG_CHEQUES_ARCHIVO_IMPORTAR_ARCHIVO. */
	public static final String ES_CH_SEG_REG_CHEQUES_ARCHIVO_IMPORTAR_ARCHIVO = "ESCA03";

	/** CONSTANTE ES_CH_SEG_CONS_CHEQUES_CANC. */
	public static final String ES_CH_SEG_CONS_CHEQUES_CANC = "ESCC";

	/** CONSTANTE ES_CH_SEG_CONS_CHEQUES_CANC_ENTRA. */
	public static final String ES_CH_SEG_CONS_CHEQUES_CANC_ENTRA = "ESCC01";

	/** CONSTANTE ES_CH_SEG_CONS_CHEQUES_CANC_CANCELA_CHEQUES. */
	public static final String ES_CH_SEG_CONS_CHEQUES_CANC_CANCELA_CHEQUES = "ESCC02";

	/** CONSTANTE ES_CH_SEG_CONS_CHEQUES_CANC_GENERA_TRAMA_CHEQ. */
	public static final String ES_CH_SEG_CONS_CHEQUES_CANC_GENERA_TRAMA_CHEQ = "ESCC03";

	/** CONSTANTE ES_CH_SEG_CONS_ARCHIVOS. */
	public static final String ES_CH_SEG_CONS_ARCHIVOS = "ESCH";

	/** CONSTANTE ES_CH_SEG_CONS_ARCHIVOS_ENTRA. */
	public static final String ES_CH_SEG_CONS_ARCHIVOS_ENTRA = "ESCH01";

	/** CONSTANTE ES_CH_SEG_CONS_ARCHIVOS_CONS_DET_ARCH_CHEQ. */
	public static final String ES_CH_SEG_CONS_ARCHIVOS_CONS_DET_ARCH_CHEQ = "ESCH02";

	/** CONSTANTE ES_CH_SEG_CONS_ARCHIVOS_CONS_ARCH_CHEQ. */
	public static final String ES_CH_SEG_CONS_ARCHIVOS_CONS_ARCH_CHEQ = "ESCH03";

	/** CONSTANTE ES_CH_SEG_CONS_ARCHIVOS_IMPORTAR_ARCHIVO. */
	public static final String ES_CH_SEG_CONS_ARCHIVOS_IMPORTAR_ARCHIVO = "ESCH04";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_ALTA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_ALTA = "ESCT";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_ALTA_ENTRA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_ALTA_ENTRA = "ESCT01";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_ALTA_ALTA_CHEQUERA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_ALTA_ALTA_CHEQUERA = "ESCT02";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_BAJA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_BAJA = "ESCB";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_BAJA_ENTRA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_BAJA_ENTRA = "ESCB01";

	/** CONSTANTE ES_CHE_SEG_MAN_CUENTAS_BAJA_BAJA_CHEQUERA. */
	public static final String ES_CHE_SEG_MAN_CUENTAS_BAJA_BAJA_CHEQUERA = "ESCB02";

	/** CONSTANTE ES_PAGO_DIR_REG_LINEA. */
	public static final String ES_PAGO_DIR_REG_LINEA = "ESPL";

	/** CONSTANTE ES_PAGO_DIR_REG_LINEA_ENTRA. */
	public static final String ES_PAGO_DIR_REG_LINEA_ENTRA = "ESPL01";

	/** CONSTANTE ES_PAGO_DIR_REG_LINEA_OBTIENE_LISTA_BENEF. */
	public static final String ES_PAGO_DIR_REG_LINEA_OBTIENE_LISTA_BENEF = "ESPL02";

	/** CONSTANTE ES_PAGO_DIR_REG_LINEA_SECUENCIA_ARCH_ORDEN. */
	public static final String ES_PAGO_DIR_REG_LINEA_SECUENCIA_ARCH_ORDEN = "ESPL03";

	/** The Constant ES_PAGO_DIR_REG_PAGOS_ARCH. */
	public static final String ES_PAGO_DIR_REG_PAGOS_ARCH = "ESPA";

	/** The Constant ES_PAGO_DIR_REG_PAGOS_ARCH_ENTRA. */
	public static final String ES_PAGO_DIR_REG_PAGOS_ARCH_ENTRA = "ESPA01";

	/** The Constant ES_PAGO_DIR_REG_PAGOS_ARCH_IMPORTAR_ARCHIVO. */
	public static final String ES_PAGO_DIR_REG_PAGOS_ARCH_IMPORTAR_ARCHIVO = "ESPA03";

	/** The Constant ES_PAGO_DIR_CONS_MODIF. */
	public static final String ES_PAGO_DIR_CONS_MODIF = "ESCM";

	/** The Constant ES_PAGO_DIR_CONS_MODIF_ENTRA. */
	public static final String ES_PAGO_DIR_CONS_MODIF_ENTRA = "ESCM01";

	/** The Constant ES_PAGO_DIR_CONS_MODIF_REALIZA_CONS_PAGO_DIR. */
	public static final String ES_PAGO_DIR_CONS_MODIF_REALIZA_CONS_PAGO_DIR = "ESCM02";

	/** The Constant ES_PAGO_DIR_CONS_MODIF_REALIZA_CANC_PAGO_DIR. */
	public static final String ES_PAGO_DIR_CONS_MODIF_REALIZA_CANC_PAGO_DIR = "ESCM03";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_ALTA. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_ALTA = "ESMA";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_ALTA_ENTRA. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_ALTA_ENTRA = "ESMA01";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_ALTA_ALTA_REL_CUENTAS. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_ALTA_ALTA_REL_CUENTAS = "ESMA02";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_BAJA. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_BAJA = "ESMB";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_BAJA_ENTRA. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_BAJA_ENTRA = "ESMB01";

	/** The Constant ES_PAGO_DIR_MAN_CUENTAS_BAJA_BAJA_REL_CUENTAS. */
	public static final String ES_PAGO_DIR_MAN_CUENTAS_BAJA_BAJA_REL_CUENTAS = "ESMB02";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_ALTA. */
	public static final String ES_PAGO_DIR_MAN_BENEF_ALTA = "ESBA";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_ALTA_ENTRA. */
	public static final String ES_PAGO_DIR_MAN_BENEF_ALTA_ENTRA = "ESBA01";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_ALTA_ALTA_CUENTA_BENEF. */
	public static final String ES_PAGO_DIR_MAN_BENEF_ALTA_ALTA_CUENTA_BENEF = "ESBA02";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_BAJA. */
	public static final String ES_PAGO_DIR_MAN_BENEF_BAJA = "ESBB";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_BAJA_ENTRA. */
	public static final String ES_PAGO_DIR_MAN_BENEF_BAJA_ENTRA = "ESBB01";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_BAJA_BAJA_CUENTA_BENEF. */
	public static final String ES_PAGO_DIR_MAN_BENEF_BAJA_BAJA_CUENTA_BENEF = "ESBB02";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_MODIF. */
	public static final String ES_PAGO_DIR_MAN_BENEF_MODIF = "ESBM";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_MODIF_ENTRA. */
	public static final String ES_PAGO_DIR_MAN_BENEF_MODIF_ENTRA = "ESBM01";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_MODIF_CONS_BENEF. */
	public static final String ES_PAGO_DIR_MAN_BENEF_MODIF_CONS_BENEF = "ESBM02";

	/** The Constant ES_PAGO_DIR_MAN_BENEF_MODIF_MODIF_BENEF. */
	public static final String ES_PAGO_DIR_MAN_BENEF_MODIF_MODIF_BENEF = "ESBM03";

	/** The Constant ES_PAGO_DIR_CAT_SUCURSALES. */
	public static final String ES_PAGO_DIR_CAT_SUCURSALES = "ESCS";

	/** The Constant ES_PAGO_DIR_CAT_SUCURSALES_ENTRA. */
	public static final String ES_PAGO_DIR_CAT_SUCURSALES_ENTRA = "ESCS01";

	/** The Constant ES_PAGO_DIR_CAT_SUCURSALES_OBT_LISTA_SUCURS. */
	public static final String ES_PAGO_DIR_CAT_SUCURSALES_OBT_LISTA_SUCURS = "ESCS02";

	/** The Constant ES_CONF_PAGO_PROV_MANT_ALTA. */
	public static final String ES_CONF_PAGO_PROV_MANT_ALTA = "ESPT";

	/** The Constant ES_CONF_PAGO_PROV_MANT_ALTA_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_MANT_ALTA_ENTRA = "ESPT01";

	/** The Constant ES_CONF_PAGO_PROV_MANT_ALTA_MODIF_PROVEED. */
	public static final String ES_CONF_PAGO_PROV_MANT_ALTA_MODIF_PROVEED = "ESPT02";

	/** The Constant ES_CONF_PAGO_PROV_MANT_ALTA_ACT_CAT_CONF. */
	public static final String ES_CONF_PAGO_PROV_MANT_ALTA_ACT_CAT_CONF = "ESPT03";

	/** The Constant ES_CONF_PAGO_PROV_MANT_CONS. */
	public static final String ES_CONF_PAGO_PROV_MANT_CONS = "ESPS";

	/** The Constant ES_CONF_PAGO_PROV_MANT_CONS_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_MANT_CONS_ENTRA = "ESPS01";

	/** The Constant ES_CONF_PAGO_PROV_MANT_CONS_MODIF_PROVEED. */
	public static final String ES_CONF_PAGO_PROV_MANT_CONS_MODIF_PROVEED = "ESPS02";

	/** The Constant ES_CONF_PAGO_PROV_MANT_CONS_ACT_CAT_CONF. */
	public static final String ES_CONF_PAGO_PROV_MANT_CONS_ACT_CAT_CONF = "ESPS03";

	/** The Constant ES_CONF_PAGO_PROV_PAGO. */
	public static final String ES_CONF_PAGO_PROV_PAGO = "ESPP";

	/** The Constant ES_CONF_PAGO_PROV_PAGO_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_PAGO_ENTRA = "ESPP01";

	/** The Constant ES_CONF_PAGO_PROV_PAGO_ACT_CAT_CONF. */
	public static final String ES_CONF_PAGO_PROV_PAGO_ACT_CAT_CONF = "ESPP02";

	/** The Constant ES_CONF_PAGO_PROV_PAGO_CONEX_DB. */
	public static final String ES_CONF_PAGO_PROV_PAGO_CONEX_DB = "ESPP03";

	/** The Constant ES_CONF_PAGO_PROV_CONS_CANC. */
	public static final String ES_CONF_PAGO_PROV_CONS_CANC = "ESCN";

	/** The Constant ES_CONF_PAGO_PROV_CONS_CANC_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_CONS_CANC_ENTRA = "ESCN01";

	/** The Constant ES_CONF_PAGO_PROV_CONS_CANC_ACT_ARCH_PAGO_PROVEED. */
	public static final String ES_CONF_PAGO_PROV_CONS_CANC_ACT_ARCH_PAGO_PROVEED = "ESCN02";

	/** The Constant ES_CONF_PAGO_PROV_CONS_CANC_CANC_PAGO_PROVEED. */
	public static final String ES_CONF_PAGO_PROV_CONS_CANC_CANC_PAGO_PROVEED = "ESCN03";

	/** The Constant ES_CONF_PAGO_PROV_EST_REP. */
	public static final String ES_CONF_PAGO_PROV_EST_REP = "ESPE";

	/** The Constant ES_CONF_PAGO_PROV_EST_REP_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_EST_REP_ENTRA = "ESPE01";

	/** The Constant ES_CONF_PAGO_PROV_EST_REP_ACTUALIZA_CATALOGOS_CONF. */
	public static final String ES_CONF_PAGO_PROV_EST_REP_ACTUALIZA_CATALOGOS_CONF = "ESPE02";

	/** The Constant ES_CONF_PAGO_PROV_EST_GRA. */
	public static final String ES_CONF_PAGO_PROV_EST_GRA = "ESPG";

	/** The Constant ES_CONF_PAGO_PROV_EST_GRA_ENTRA. */
	public static final String ES_CONF_PAGO_PROV_EST_GRA_ENTRA = "ESPG01";

	/** The Constant ES_CONF_PAGO_PROV_EST_GRA_ACTUALIZA_GRAFICA. */
	public static final String ES_CONF_PAGO_PROV_EST_GRA_ACTUALIZA_GRAFICA = "ESPG02";

	/** The Constant ES_SAR_PAGOS. */
	public static final String ES_SAR_PAGOS = "ESSP";

	/** The Constant ES_SAR_PAGOS_ENTRA. */
	public static final String ES_SAR_PAGOS_ENTRA = "ESSP01";

	/** The Constant ES_SAR_PAGOS_CONSULTA_DEPENDENCIA. */
	public static final String ES_SAR_PAGOS_CONSULTA_DEPENDENCIA = "ESSP02";

	/** The Constant ES_SAR_PAGOS_REALIZA_PAGO_SAR. */
	public static final String ES_SAR_PAGOS_REALIZA_PAGO_SAR = "ESSP03";

	/** The Constant ES_SAR_CONS_CANCEL. */
	public static final String ES_SAR_CONS_CANCEL = "ESSC";

	/** The Constant ES_SAR_CONS_CANCEL_ENTRA. */
	public static final String ES_SAR_CONS_CANCEL_ENTRA = "ESSC01";

	/** The Constant ES_SAR_CONS_CANCEL_CONSULTA_DEPENDENCIA. */
	public static final String ES_SAR_CONS_CANCEL_CONSULTA_DEPENDENCIA = "ESSC02";

	/** The Constant ES_SAR_CONS_CANCEL_CONSULTA_PAGO_SAR. */
	public static final String ES_SAR_CONS_CANCEL_CONSULTA_PAGO_SAR = "ESSC03";

	/** The Constant ES_SAR_CONS_CANCEL_CONS_REIMP_PAGO_SAR. */
	public static final String ES_SAR_CONS_CANCEL_CONS_REIMP_PAGO_SAR = "ESSC04";

	/** The Constant ES_SAR_CONS_CANCEL_REALIZA_PAGO_SAR. */
	public static final String ES_SAR_CONS_CANCEL_REALIZA_PAGO_SAR = "ESSC05";

	/** The Constant ES_TESOFE_CONS_ARCH_REG. */
	public static final String ES_TESOFE_CONS_ARCH_REG = "ESTC";

	/** The Constant ES_TESOFE_CONS_ARCH_REG_ENTRA. */
	public static final String ES_TESOFE_CONS_ARCH_REG_ENTRA = "ESTC01";

	/** The Constant ES_TESOFE_CONS_ARCH_REG_CONSULTA_ARCH. */
	public static final String ES_TESOFE_CONS_ARCH_REG_CONSULTA_ARCH = "ESTC02";

	/** The Constant ES_TESOFE_CONS_ARCH_REG_ENVIO_DET_ARCH. */
	public static final String ES_TESOFE_CONS_ARCH_REG_ENVIO_DET_ARCH = "ESTC03";

	/** The Constant ES_TESOFE_RECAUDACIONES. */
	public static final String ES_TESOFE_RECAUDACIONES = "ESTR";

	/** The Constant ES_TESOFE_RECAUDACIONES_ENTRA. */
	public static final String ES_TESOFE_RECAUDACIONES_ENTRA = "ESTR01";

	/** The Constant ES_TESOFE_RECAUDACIONES_CONS_REC_GENERAL. */
	public static final String ES_TESOFE_RECAUDACIONES_CONS_REC_GENERAL = "ESTR02";

	/** The Constant ES_TESOFE_RECAUDACIONES_CONS_REC_DETALLE. */
	public static final String ES_TESOFE_RECAUDACIONES_CONS_REC_DETALLE = "ESTR03";

	/** The Constant ES_TESOFE_IMPORTACION_ENVIO. */
	public static final String ES_TESOFE_IMPORTACION_ENVIO = "ESTI";

	/** The Constant ES_TESOFE_IMPORTACION_ENVIO_ENTRA. */
	public static final String ES_TESOFE_IMPORTACION_ENVIO_ENTRA = "ESTI01";

	/** The Constant ES_TESOFE_IMPORTACION_ENVIO_ENVIO_ARCH. */
	public static final String ES_TESOFE_IMPORTACION_ENVIO_ENVIO_ARCH = "ESTI02";

	/* Variables agregadas para el mÃ³dulo de Tarjeta Prepago */
	/** The Constant ES_NP_REMESAS_RECEPCION. */
	public static final String ES_NP_REMESAS_RECEPCION = "ESRR";

	/** The Constant ES_NP_REMESAS_RECEPCION_ENTRA. */
	public static final String ES_NP_REMESAS_RECEPCION_ENTRA = "ESRR01";

	/** The Constant ES_NP_REMESAS_RECEPCION_CONSULTA. */
	public static final String ES_NP_REMESAS_RECEPCION_CONSULTA = "ESRR02";

	/** The Constant ES_NP_REMESAS_RECEPCION_RESUMEN. */
	public static final String ES_NP_REMESAS_RECEPCION_RESUMEN = "ESRR03";

	/** The Constant ES_NP_REMESAS_RECEPCION_CONFIRMA. */
	public static final String ES_NP_REMESAS_RECEPCION_CONFIRMA = "ESRR04";

	/** The Constant ES_NP_REMESAS_RECEPCION_RECHAZA. */
	public static final String ES_NP_REMESAS_RECEPCION_RECHAZA = "ESRR05";

	/** The Constant ES_NP_REMESAS_CONSULTA. */
	public static final String ES_NP_REMESAS_CONSULTA = "ESRC";

	/** The Constant ES_NP_REMESAS_CONSULTA_ENTRA. */
	public static final String ES_NP_REMESAS_CONSULTA_ENTRA = "ESRC01";

	/** The Constant ES_NP_REMESAS_CONSULTA_CONSULTA. */
	public static final String ES_NP_REMESAS_CONSULTA_CONSULTA = "ESRC02";

	/** The Constant ES_NP_TARJETA_ASIGNACION. */
	public static final String ES_NP_TARJETA_ASIGNACION = "ESAA";

	/** The Constant ES_NP_TARJETA_ASIGNACION_ENTRA. */
	public static final String ES_NP_TARJETA_ASIGNACION_ENTRA = "ESAA01";

	/** The Constant ES_NP_TARJETA_ASIGNACION_IMPORTAR. */
	public static final String ES_NP_TARJETA_ASIGNACION_IMPORTAR = "ESAA02";

	/** The Constant ES_NP_TARJETA_ASIGNACION_LINEA. */
	public static final String ES_NP_TARJETA_ASIGNACION_LINEA = "ESAA03";

	/** The Constant ES_NP_TARJETA_CONSULTA. */
	public static final String ES_NP_TARJETA_CONSULTA = "ESAC";

	/** The Constant ES_NP_TARJETA_CONSULTA_ENTRA. */
	public static final String ES_NP_TARJETA_CONSULTA_ENTRA = "ESAC01";

	/** The Constant ES_NP_TARJETA_CONSULTA_CONSULTA. */
	public static final String ES_NP_TARJETA_CONSULTA_CONSULTA = "ESAC02";

	/** The Constant ES_NP_TARJETA_CONSULTA_DEMOGRAFICOS. */
	public static final String ES_NP_TARJETA_CONSULTA_DEMOGRAFICOS = "ESAC03";

	/** The Constant ES_NP_TARJETA_CONSULTA_MODIFICA_DEMOGRAFICOS. */
	public static final String ES_NP_TARJETA_CONSULTA_MODIFICA_DEMOGRAFICOS = "ESAC04";

	/** The Constant ES_NP_TARJETA_BAJA. */
	public static final String ES_NP_TARJETA_BAJA = "ESAB";

	/** The Constant ES_NP_TARJETA_BAJA_ENTRA. */
	public static final String ES_NP_TARJETA_BAJA_ENTRA = "ESAB01";

	/** The Constant ES_NP_TARJETA_BAJA_CONSULTA. */
	public static final String ES_NP_TARJETA_BAJA_CONSULTA = "ESAB02";

	/** The Constant ES_NP_TARJETA_BAJA_CONFIRMA. */
	public static final String ES_NP_TARJETA_BAJA_CONFIRMA = "ESAB03";

	/** The Constant ES_NP_TARJETA_BLOQUEO. */
	public static final String ES_NP_TARJETA_BLOQUEO = "ESAQ";

	/** The Constant ES_NP_TARJETA_BLOQUEO_ENTRA. */
	public static final String ES_NP_TARJETA_BLOQUEO_ENTRA = "ESAQ01";

	/** The Constant ES_NP_TARJETA_BLOQUEO_CONSULTA. */
	public static final String ES_NP_TARJETA_BLOQUEO_CONSULTA = "ESAQ02";

	/** The Constant ES_NP_TARJETA_BLOQUEO_CONFIRMA. */
	public static final String ES_NP_TARJETA_BLOQUEO_CONFIRMA = "ESAQ03";

	/** The Constant ES_NP_TARJETA_REASIGNACION. */
	public static final String ES_NP_TARJETA_REASIGNACION = "ESAR";

	/** The Constant ES_NP_TARJETA_REASIGNACION_ENTRA. */
	public static final String ES_NP_TARJETA_REASIGNACION_ENTRA = "ESAR01";

	/** The Constant ES_NP_TARJETA_REASIGNACION_CONSULTA. */
	public static final String ES_NP_TARJETA_REASIGNACION_CONSULTA = "ESAR02";

	/** The Constant ES_NP_TARJETA_REASIGNACION_CONFIRMA. */
	public static final String ES_NP_TARJETA_REASIGNACION_CONFIRMA = "ESAR03";

	/** The Constant ES_NP_TARJETA_CONSULTA_ALTA. */
	public static final String ES_NP_TARJETA_CONSULTA_ALTA = "ESAE";

	/** The Constant ES_NP_TARJETA_CONSULTA_ALTA_ENTRA. */
	public static final String ES_NP_TARJETA_CONSULTA_ALTA_ENTRA = "ESAE01";

	/** The Constant ES_NP_TARJETA_CONSULTA_ALTA_CONSULTA. */
	public static final String ES_NP_TARJETA_CONSULTA_ALTA_CONSULTA = "ESAE02";

	/** The Constant ES_NP_TARJETA_CONSULTA_ALTA_CANCELA. */
	public static final String ES_NP_TARJETA_CONSULTA_ALTA_CANCELA = "ESAE03";

	/** The Constant ES_NP_TARJETA_CONSULTA_ALTA_CORREO. */
	public static final String ES_NP_TARJETA_CONSULTA_ALTA_CORREO = "ESAE04";

	/** The Constant ES_NP_PAGOS_IMPORTACION. */
	public static final String ES_NP_PAGOS_IMPORTACION = "ESAN";

	/** The Constant ES_NP_PAGOS_IMPORTACION_ENTRA. */
	public static final String ES_NP_PAGOS_IMPORTACION_ENTRA = "ESAN01";

	/** The Constant ES_NP_PAGOS_IMPORTACION_CARGA_ARCH. */
	public static final String ES_NP_PAGOS_IMPORTACION_CARGA_ARCH = "ESAN02";

	/** The Constant ES_NP_PAGOS_IMPORTACION_VALIDA_ARCH. */
	public static final String ES_NP_PAGOS_IMPORTACION_VALIDA_ARCH = "ESAN03";

	/** The Constant ES_NP_PAGOS_IMPORTACION_CONFIRMA. */
	public static final String ES_NP_PAGOS_IMPORTACION_CONFIRMA = "ESAN04";

	/** The Constant ES_NP_PAGOS_INDIVIDUAL. */
	public static final String ES_NP_PAGOS_INDIVIDUAL = "ESAI";

	/** The Constant ES_NP_PAGOS_INDIVIDUAL_ENTRA. */
	public static final String ES_NP_PAGOS_INDIVIDUAL_ENTRA = "ESAI01";

	/** The Constant ES_NP_PAGOS_INDIVIDUAL_CONSULTA. */
	public static final String ES_NP_PAGOS_INDIVIDUAL_CONSULTA = "ESAI02";

	/** The Constant ES_NP_PAGOS_INDIVIDUAL_CONFIRMA. */
	public static final String ES_NP_PAGOS_INDIVIDUAL_CONFIRMA = "ESAI03";

	/** The Constant ES_NP_PAGOS_CONSULTA. */
	public static final String ES_NP_PAGOS_CONSULTA = "ESAO";

	/** The Constant ES_NP_PAGOS_CONSULTA_ENTRA. */
	public static final String ES_NP_PAGOS_CONSULTA_ENTRA = "ESAO01";

	/** The Constant ES_NP_PAGOS_CONSULTA_CONSULTA. */
	public static final String ES_NP_PAGOS_CONSULTA_CONSULTA = "ESAO02";

	/** The Constant ES_NP_PAGOS_CONSULTA_CANCELA. */
	public static final String ES_NP_PAGOS_CONSULTA_CANCELA = "ESAO03";

	/** The Constant ES_NP_PAGOS_CONSULTA_CORREO. */
	public static final String ES_NP_PAGOS_CONSULTA_CORREO = "ESAO04";

	/** Vinculacion Alta. */
	public static final String ES_EN_ALTA_TARJETA_VINCULACION = "ALTC";

	/** The Constant ES_EN_ALTA_TARJETA_VINCULACION_MSG. */
	public static final String ES_EN_ALTA_TARJETA_VINCULACION_MSG = "Alta vinculaciÃ³n tarjeta";

	/** Vinculacion consulta. */
	public static final String ES_EN_CONS_TARJETA_VINCULACION = "COTC";

	/** The Constant ES_EN_CONS_TARJETA_VINCULACION_MSG. */
	public static final String ES_EN_CONS_TARJETA_VINCULACION_MSG = "Consulta vinculaciÃ³n tarjeta";

	/** Vinculacion Baja. */
	public static final String ES_EN_BAJA_TARJETA_VINCULACION = "BATC";

	/** The Constant ES_EN_BAJA_TARJETA_VINCULACION_MSG. */
	public static final String ES_EN_BAJA_TARJETA_VINCULACION_MSG = "Baja vinculaciÃ³n tarjeta";

	// FIN CONSTANTE BITACORIZACIÃ“N NOMINA PREPAGO
	// ----------------------------------

	// CONSTANTES DEL MÃ“DULO ENLACE-TESORERA

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION = "ERFI";

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION_ENTRA. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION_ENTRA = "ERFI01";

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION_SEL_CONTRATO. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION_SEL_CONTRATO = "ERFI02";

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION_OPER_SOLIC. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION_OPER_SOLIC = "ERFI03";

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_CPSI. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_CPSI = "ERFI04";

	/** The Constant ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_VTSI. */
	public static final String ER_INVERSIONES_FONDOS_INVERSION_OPER_REDSRV_VTSI = "ERFI05";

	/** The Constant ER_INVERSIONES_VISTA. */
	public static final String ER_INVERSIONES_VISTA = "ERIV";

	/** The Constant ER_INVERSIONES_VISTA_ENTRA. */
	public static final String ER_INVERSIONES_VISTA_ENTRA = "ERIV01";

	/** The Constant ER_INVERSIONES_VISTA_OPER_REDSRV_CPDI. */
	public static final String ER_INVERSIONES_VISTA_OPER_REDSRV_CPDI = "ERIV02";

	/** The Constant ER_INVERSIONES_VISTA_OPER_REDSRV_CPID. */
	public static final String ER_INVERSIONES_VISTA_OPER_REDSRV_CPID = "ERIV03";

	/** The Constant ER_INVERSIONES_VISTA_CONTINUA_PREPAGO. */
	public static final String ER_INVERSIONES_VISTA_CONTINUA_PREPAGO = "ERIV04";

	/** The Constant ER_REGISTRO_OPERACION. */
	public static final String ER_REGISTRO_OPERACION = "ERRO";

	/** The Constant ER_REGISTRO_OPERACION_ENTRA. */
	public static final String ER_REGISTRO_OPERACION_ENTRA = "ERRO01";

	/** The Constant ER_CAMBIO_INSTRUCCION. */
	public static final String ER_CAMBIO_INSTRUCCION = "ERCI";

	/** The Constant ER_CAMBIO_INSTRUCCION_ENTRA. */
	public static final String ER_CAMBIO_INSTRUCCION_ENTRA = "ERCI01";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO = "ERCS";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_ENTRA. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_ENTRA = "ERCS01";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_CDCR. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_CDCR = "ERCS02";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_TAR = "ERCS03";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_LIN. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_GENERA_TABLA_MOVS_LIN = "ERCS04";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_COMI. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_COMI = "ERCS05";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_RPIB. */
	public static final String ER_CREDITO_LINEA_CONSULTA_SALDO_OPER_REDSRV_RPIB = "ERCS06";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION = "ERCP";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_ENTRA. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_ENTRA = "ERCP01";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_CDCR. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_CDCR = "ERCP02";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RG01. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RG01 = "ERCP03";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_TAR = "ERCP04";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_LIN. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_GENERA_TABLA_MOVS_LIN = "ERCP05";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_COMI. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_COMI = "ERCP06";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RPIB. */
	public static final String ER_CREDITO_LINEA_CONSULTA_POSICION_OPER_REDSRV_RPIB = "ERCP07";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS = "ERCM";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_ENTRA. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_ENTRA = "ERCM01";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_TAR = "ERCM02";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_MOVT. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_MOVT = "ERCM03";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_LIN. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_GENERA_TABLA_MOVS_LIN = "ERCM04";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_RG02. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_RG02 = "ERCM05";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_RPIB. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_RPIB = "ERCM06";

	/** The Constant ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_COMI. */
	public static final String ER_CREDITO_LINEA_CONSULTA_MOVS_OPER_REDSRV_COMI = "ERCM07";

	/** The Constant ER_CREDITO_LINEA_PREPAGO. */
	public static final String ER_CREDITO_LINEA_PREPAGO = "ERLP";

	/** The Constant ER_CREDITO_LINEA_PREPAGO_ENTRA. */
	public static final String ER_CREDITO_LINEA_PREPAGO_ENTRA = "ERLP01";

	/** The Constant ER_CREDITO_LINEA_PREPAGO_OPER_REDSRV_RG01. */
	public static final String ER_CREDITO_LINEA_PREPAGO_OPER_REDSRV_RG01 = "ERLP02";

	/** The Constant ER_CREDITO_LINEA_PREPAGO_OPER_REDSRV_RG03. */
	public static final String ER_CREDITO_LINEA_PREPAGO_OPER_REDSRV_RG03 = "ERLP03";

	/** The Constant ER_CREDITO_LINEA_PREPAGO_EFECTUA_OPERACIONES. */
	public static final String ER_CREDITO_LINEA_PREPAGO_EFECTUA_OPERACIONES = "ERLP04";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS = "EREC";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_ENTRA. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_ENTRA = "EREC01";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_CEDC. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_CEDC = "EREC02";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD78. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD78 = "EREC03";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC49. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC49 = "EREC04";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC50. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PC50 = "EREC05";

	/** The Constant ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD97. */
	public static final String ER_CREDITO_ELECTRONICO_CONSULTAS_OPER_REDSRV_PD97 = "EREC06";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS = "EREP";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_ENTRA. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_ENTRA = "EREP01";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PC50. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PC50 = "EREP02";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CECT. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CECT = "EREP03";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD15. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD15 = "EREP04";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD31. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_PD31 = "EREP05";

	/** The Constant ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CEDC. */
	public static final String ER_CREDITO_ELECTRONICO_PAGOS_OPER_REDSRV_CEDC = "EREP06";

	/** The Constant ER_CREDITO_ELECTRONICO_DISPOS. */
	public static final String ER_CREDITO_ELECTRONICO_DISPOS = "ERED";

	/** The Constant ER_CREDITO_ELECTRONICO_DISPOS_ENTRA. */
	public static final String ER_CREDITO_ELECTRONICO_DISPOS_ENTRA = "ERED01";

	/** The Constant ER_CREDITO_ELECTRONICO_DISPOS_LLAMA_SERV_TUX. */
	public static final String ER_CREDITO_ELECTRONICO_DISPOS_LLAMA_SERV_TUX = "ERED02";

	/** The Constant ER_CREDITO_ELECTRONICO_DISPOS_OPER_RESDRV_PC47. */
	public static final String ER_CREDITO_ELECTRONICO_DISPOS_OPER_RESDRV_PC47 = "ERED03";

	/** The Constant ER_TESO_INTER_CAMBIOS. */
	public static final String ER_TESO_INTER_CAMBIOS = "ERIC";

	/** The Constant ER_TESO_INTER_CAMBIOS_ENTRA. */
	public static final String ER_TESO_INTER_CAMBIOS_ENTRA = "ERIC01";

	/** The Constant ER_TESO_INTER_CAMBIOS_CONSULTA_DB. */
	public static final String ER_TESO_INTER_CAMBIOS_CONSULTA_DB = "ERIC02";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIIF. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIIF = "ERIC03";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_CPA. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_CPA = "ERIC04";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_VTA. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_VTA = "ERIC05";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_NAME. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_NAME = "ERIC06";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_ECBE. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_ECBE = "ERIC07";

	/** The Constant ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIDP. */
	public static final String ER_TESO_INTER_CAMBIOS_OPER_REDSRV_DIDP = "ERIC08";

	/** The Constant ER_TESO_INTER_CAMBIOS_GENERA_COMPROBANTE. */
	public static final String ER_TESO_INTER_CAMBIOS_GENERA_COMPROBANTE = "ERIC09";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER. */
	public static final String ER_TESO_INTER_TRANSFER_INTER = "ERTI";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_ENTRA. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_ENTRA = "ERTI01";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DICO. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DICO = "ERTI02";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_LLAMA_SERV_TUX. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_LLAMA_SERV_TUX = "ERTI03";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DITA. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DITA = "ERTI04";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DIBT. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV_DIBT = "ERTI05";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_OPER_REDSRV = "ERTI06";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_CONSULTA_DB. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_CONSULTA_DB = "ERTI07";

	/** The Constant ER_TESO_INTER_TRANSFER_INTER_GENERA_COMPROBANTE. */
	public static final String ER_TESO_INTER_TRANSFER_INTER_GENERA_COMPROBANTE = "ERTI08";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC = "ERMC";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_ENTRA. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_ENTRA = "ERMC01";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI02. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI02 = "ERMC02";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_IMPORTA_ARCH. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_IMPORTA_ARCH = "ERMC03";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI01. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI01 = "ERMC04";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI04. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI04 = "ERMC05";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI03. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_OPER_REDSRV_TI03 = "ERMC06";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_CONC_ELIMINA_REGISTRO. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_CONC_ELIMINA_REGISTRO = "ERMC07";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP = "ERMD";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_ENTRA. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_ENTRA = "ERMD01";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_BORRAR_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_BORRAR_ARBOL = "ERMD02";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ALTA_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ALTA_ARBOL = "ERMD03";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_MODIF_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_MODIF_ARBOL = "ERMD04";

	/**
	 * The Constant
	 * ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ELIMINA_REGISTROS_CONCENT.
	 */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ELIMINA_REGISTROS_CONCENT = "ERMD05";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_CONS_ARBOL_CONSULT. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_CONS_ARBOL_CONSULT = "ERMD06";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_CONCENT. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_CONCENT = "ERMD07";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_FONDEO. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_TRAMA_COPIA_FONDEO = "ERMD08";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_IMPORTA_DATOS. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_IMPORTA_DATOS = "ERMD09";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ARREGLA_CUENTAS. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_DISP_TUX_ARREGLA_CUENTAS = "ERMD10";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND = "ERMF";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_ENTRA. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_ENTRA = "ERMF01";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_BORRAR_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_BORRAR_ARBOL = "ERMF02";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ALTA_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ALTA_ARBOL = "ERMF03";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_MODIF_ARBOL. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_MODIF_ARBOL = "ERMF04";

	/**
	 * The Constant
	 * ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ELIMINA_REGISTROS_CONCENT.
	 */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ELIMINA_REGISTROS_CONCENT = "ERMF05";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_CONS_ARBOL_CONSULT. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_CONS_ARBOL_CONSULT = "ERMF06";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_CONCENT. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_CONCENT = "ERMF07";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_DISP. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_TRAMA_COPIA_DISP = "ERMF08";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_IMPORTA_DATOS. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_IMPORTA_DATOS = "ERMF09";

	/** The Constant ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ARREGLA_CUENTAS. */
	public static final String ER_TESO_INTEL_MANT_ESTRUC_FOND_TUX_ARREGLA_CUENTAS = "ERMF10";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_CONC. */
	public static final String ER_TESO_INTEL_CONS_SALDO_CONC = "ERSC";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_CONC_ENTRA. */
	public static final String ER_TESO_INTEL_CONS_SALDO_CONC_ENTRA = "ERSC01";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_CONC_OPER_REDSRV_TI02. */
	public static final String ER_TESO_INTEL_CONS_SALDO_CONC_OPER_REDSRV_TI02 = "ERSC02";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_DISP. */
	public static final String ER_TESO_INTEL_CONS_SALDO_DISP = "ERSD";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_DISP_ENTRA. */
	public static final String ER_TESO_INTEL_CONS_SALDO_DISP_ENTRA = "ERSD01";

	/** The Constant ER_TESO_INTEL_CONS_SALDO_DISP_OPER_REDSRV_TI02. */
	public static final String ER_TESO_INTEL_CONS_SALDO_DISP_OPER_REDSRV_TI02 = "ERSD02";

	/** The Constant ER_TESO_INTEL_CONS_MOV_CONC. */
	public static final String ER_TESO_INTEL_CONS_MOV_CONC = "ERVC";

	/** The Constant ER_TESO_INTEL_CONS_MOV_CONC_ENTRA. */
	public static final String ER_TESO_INTEL_CONS_MOV_CONC_ENTRA = "ERVC01";

	/** The Constant ER_TESO_INTEL_CONS_MOV_CONC_REALIZA_CONSULTA. */
	public static final String ER_TESO_INTEL_CONS_MOV_CONC_REALIZA_CONSULTA = "ERVC02";

	/** The Constant ER_TESO_INTEL_CONS_MOV_DISP. */
	public static final String ER_TESO_INTEL_CONS_MOV_DISP = "ERVD";

	/** The Constant ER_TESO_INTEL_CONS_MOV_DISP_ENTRA. */
	public static final String ER_TESO_INTEL_CONS_MOV_DISP_ENTRA = "ERVD01";

	/** The Constant ER_TESO_INTEL_CONS_MOV_DISP_REALIZA_CONSULTA. */
	public static final String ER_TESO_INTEL_CONS_MOV_DISP_REALIZA_CONSULTA = "ERVD02";

	/** The Constant ER_ESQ_BASE0_MANT_ESTRUC. */
	public static final String ER_ESQ_BASE0_MANT_ESTRUC = "ERBM";

	/** The Constant ER_ESQ_BASE0_MANT_ESTRUC_ENTRA. */
	public static final String ER_ESQ_BASE0_MANT_ESTRUC_ENTRA = "ERBM01";

	/** The Constant ER_ESQ_BASE0_CONS_SALDO. */
	public static final String ER_ESQ_BASE0_CONS_SALDO = "ERBC";

	/** The Constant ER_ESQ_BASE0_CONS_SALDO_ENTRA. */
	public static final String ER_ESQ_BASE0_CONS_SALDO_ENTRA = "ERBC01";

	/** The Constant ER_ESQ_BASE0_PROG_OP_CONS. */
	public static final String ER_ESQ_BASE0_PROG_OP_CONS = "ERBP";

	/** The Constant ER_ESQ_BASE0_PROG_OP_CONS_ENTRA. */
	public static final String ER_ESQ_BASE0_PROG_OP_CONS_ENTRA = "ERBP01";

	/** The Constant ER_ESQ_BASE0_PROG_OP_REG. */
	public static final String ER_ESQ_BASE0_PROG_OP_REG = "ERBR";

	/** The Constant ER_ESQ_BASE0_PROG_OP_REG_ENTRA. */
	public static final String ER_ESQ_BASE0_PROG_OP_REG_ENTRA = "ERBR01";

	// CONSTANTES DEL MÃ“DULO ENLACE-TRANSFERENCIAS
	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA = "ETML";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA_ENTRA. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA_ENTRA = "ETML01";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA_VALIDA_DUPLICADOS. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA_VALIDA_DUPLICADOS = "ETML02";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA_AGREGAR_REGISTRO_BIT. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA_AGREGAR_REGISTRO_BIT = "ETML03";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA_IMPORTAR_ARCHIVO. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA_IMPORTAR_ARCHIVO = "ETML04";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_LINEA_EXPORTAR_ARCHIVO. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_LINEA_EXPORTAR_ARCHIVO = "ETML05";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_MULTI. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_MULTI = "ETMM";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_MULTI_ENTRA. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_MULTI_ENTRA = "ETMM01";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_MULTI_VALIDA_DUPLICADOS. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_MULTI_VALIDA_DUPLICADOS = "ETMM02";

	/** The Constant ET_CUENTAS_MISMO_BANCO_MN_MULTI_AGREGAR_REGISTRO_BIT. */
	public static final String ET_CUENTAS_MISMO_BANCO_MN_MULTI_AGREGAR_REGISTRO_BIT = "ETMM03";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES = "ETMD";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES_ENTRA. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES_ENTRA = "ETMD01";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES_VALIDA_DUPLICADOS. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES_VALIDA_DUPLICADOS = "ETMD02";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES_AGREGAR_REGISTRO_BIT. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES_AGREGAR_REGISTRO_BIT = "ETMD03";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES_IMPORTAR_ARCHIVO. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES_IMPORTAR_ARCHIVO = "ETMD04";

	/** The Constant ET_CUENTAS_MISMO_BANCO_DOLARES_EXPORTAR_ARCHIVO. */
	public static final String ET_CUENTAS_MISMO_BANCO_DOLARES_EXPORTAR_ARCHIVO = "ETMD05";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_FISICAS. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_FISICAS = "ETOF";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_ENTRA. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_ENTRA = "ETOF01";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_TRANSFERENCIA. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_TRANSFERENCIA = "ETOF02";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_IMPORTA_ARCHIVO. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_FISICAS_IMPORTA_ARCHIVO = "ETOF03";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_MORALES. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_MORALES = "ETOM";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_MORALES_ENTRA. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_MORALES_ENTRA = "ETOM01";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_MORALES_TRANSFERENCIA. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_MORALES_TRANSFERENCIA = "ETOM02";

	/** The Constant ET_ORDENES_PAGO_OCURRE_PERS_MORALES_IMPORTA_ARCHIVO. */
	public static final String ET_ORDENES_PAGO_OCURRE_PERS_MORALES_IMPORTA_ARCHIVO = "ETOM03";

	/** The Constant ET_ORDENES_PAGO_OCURRE_MANT_BENEF. */
	public static final String ET_ORDENES_PAGO_OCURRE_MANT_BENEF = "MBPO";

	/** The Constant ET_ORDENES_PAGO_OCURRE_AUT_BENEF. */
	public static final String ET_ORDENES_PAGO_OCURRE_AUT_BENEF = "ABPO";

	/** The Constant ET_ORDENES_PAGO_DIRECTO_MANT_BENEF. */
	public static final String ET_ORDENES_PAGO_DIRECTO_MANT_BENEF = "ABOP";

	/** The Constant ET_ORDENES_PAGO_DIRECTO_AUT_BENEF. */
	public static final String ET_ORDENES_PAGO_DIRECTO_AUT_BENEF = "ABPD";

	/** The Constant ET_INTERBANCARIAS. */
	public static final String ET_INTERBANCARIAS = "ETIN";

	/** The Constant ET_INTERBANCARIAS_ENTRA. */
	public static final String ET_INTERBANCARIAS_ENTRA = "ETIN01";

	/** The Constant ET_INTERBANCARIAS_GENERA_TABLA_DEPOSITOS. */
	public static final String ET_INTERBANCARIAS_GENERA_TABLA_DEPOSITOS = "ETIN02";

	/** The Constant ET_INTERBANCARIAS_VALIDA_DUPLICADOS. */
	public static final String ET_INTERBANCARIAS_VALIDA_DUPLICADOS = "ETIN03";

	/** The Constant ET_INTERBANCARIAS_REALIZA_OPER_TRANSFERENCIA. */
	public static final String ET_INTERBANCARIAS_REALIZA_OPER_TRANSFERENCIA = "ETIN04";

	/** The Constant ET_INTERBANCARIAS_INICIA_CONSULTA_ANTERIORES. */
	public static final String ET_INTERBANCARIAS_INICIA_CONSULTA_ANTERIORES = "ETIN05";

	/** The Constant ET_INTERBANCARIAS_RECUP_INTERBANCARIOS. */
	public static final String ET_INTERBANCARIAS_RECUP_INTERBANCARIOS = "ETIN06";

	/** The Constant ET_INTERBANCARIAS_CONS_COMISIONES_COBRADAS. */
	public static final String ET_INTERBANCARIAS_CONS_COMISIONES_COBRADAS = "ETIN07";

	/** The Constant ET_INTERBANCARIAS_GENERA_ARCHIVO_EXPORTACION. */
	public static final String ET_INTERBANCARIAS_GENERA_ARCHIVO_EXPORTACION = "ETIN08";

	/** The Constant ET_TARJETA_CREDITO_PAGO. */
	public static final String ET_TARJETA_CREDITO_PAGO = "ETTP";

	/** The Constant ET_TARJETA_CREDITO_PAGO_ENTRA. */
	public static final String ET_TARJETA_CREDITO_PAGO_ENTRA = "ETTP01";

	/** The Constant ET_TARJETA_CREDITO_PAGO_VALIDA_DUPLICADOS. */
	public static final String ET_TARJETA_CREDITO_PAGO_VALIDA_DUPLICADOS = "ETTP02";

	/** The Constant ET_TARJETA_CREDITO_PAGO_AGREGAR_REGISTRO_BIT. */
	public static final String ET_TARJETA_CREDITO_PAGO_AGREGAR_REGISTRO_BIT = "ETTP03";

	/** The Constant ET_TARJETA_CREDITO_PAGO_IMPORTAR_ARCHIVO. */
	public static final String ET_TARJETA_CREDITO_PAGO_IMPORTAR_ARCHIVO = "ETTP04";

	/** The Constant ET_TARJETA_CREDITO_PAGO_EXPORTAR_ARCHIVO. */
	public static final String ET_TARJETA_CREDITO_PAGO_EXPORTAR_ARCHIVO = "ETTP05";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION = "ETTD";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION_ENTRA. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION_ENTRA = "ETTD01";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION_VALIDA_DUPLICADOS. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION_VALIDA_DUPLICADOS = "ETTD02";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION_AGREGAR_REGISTRO_BIT. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION_AGREGAR_REGISTRO_BIT = "ETTD03";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION_IMPORTAR_ARCHIVO. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION_IMPORTAR_ARCHIVO = "ETTD04";

	/** The Constant ET_TARJETA_CREDITO_DISPOSICION_EXPORTAR_ARCHIVO. */
	public static final String ET_TARJETA_CREDITO_DISPOSICION_EXPORTAR_ARCHIVO = "ETTD05";

	/** The Constant ET_CAMBIOS. */
	public static final String ET_CAMBIOS = "ETCA";

	/** The Constant ET_CAMBIOS_ENTRA. */
	public static final String ET_CAMBIOS_ENTRA = "ETCA01";

	/** The Constant ET_CAMBIOS_COTIZACION_INTER. */
	public static final String ET_CAMBIOS_COTIZACION_INTER = "ETCA02";

	/** The Constant ET_CAMBIOS_CAMBIO_DIVISA. */
	public static final String ET_CAMBIOS_CAMBIO_DIVISA = "ETCA03";

	/** The Constant ET_CAMBIOS_VALIDA_CUENTA_NO_REG. */
	public static final String ET_CAMBIOS_VALIDA_CUENTA_NO_REG = "ETCA04";

	/** The Constant ET_CAMBIOS_CONS_CLABE_BANC_EST. */
	public static final String ET_CAMBIOS_CONS_CLABE_BANC_EST = "ETCA05";

	/** The Constant ET_CAMBIOS_INICIO_CONS_TRANSF_INTERN. */
	public static final String ET_CAMBIOS_INICIO_CONS_TRANSF_INTERN = "ETCA06";

	/** The Constant ET_CAMBIOS_CONS_TRANSF_INTERN. */
	public static final String ET_CAMBIOS_CONS_TRANSF_INTERN = "ETCA07";

	/** The Constant ET_TRANSFERENCIAS_INTER. */
	public static final String ET_TRANSFERENCIAS_INTER = "ETTI";

	/** The Constant ET_TRANSFERENCIAS_INTER_ENTRA. */
	public static final String ET_TRANSFERENCIAS_INTER_ENTRA = "ETTI01";

	/** The Constant ET_TRANSFERENCIAS_INTER_COTIZA_INTERNACIONAL. */
	public static final String ET_TRANSFERENCIAS_INTER_COTIZA_INTERNACIONAL = "ETTI02";

	/** The Constant ET_TRANSFERENCIAS_INTER_VALIDA_CLAVE_ABA. */
	public static final String ET_TRANSFERENCIAS_INTER_VALIDA_CLAVE_ABA = "ETTI03";

	/** The Constant ET_TRANSFERENCIAS_INTER_INICIA_CONS_TRANSF_INTER. */
	public static final String ET_TRANSFERENCIAS_INTER_INICIA_CONS_TRANSF_INTER = "ETTI04";

	/** The Constant ET_TRANSFERENCIAS_INTER_CONS_TRANSF_INTER. */
	public static final String ET_TRANSFERENCIAS_INTER_CONS_TRANSF_INTER = "ETTI05";

	/** The Constant ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS. */
	public static final String ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS = "ETTI06";

	/** The Constant ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT. */
	public static final String ET_TRANSFERENCIAS_INTER_AGREGAR_REGISTRO_BIT = "ETTI07";

	/**
	 * The Constant
	 * ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS_CLAVE_ESPECIAL.
	 */
	public static final String ET_TRANSFERENCIAS_INTER_VALIDA_TRANSF_DUPLICADAS_CLAVE_ESPECIAL = "ETTI08";

	/** The Constant ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA. */
	public static final String ET_TRANSFERENCIAS_FX_ONLINE_ACCESO_HERRAMIENTA = "ACRR";

	/** The Constant ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA. */
	public static final String ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_COMPLEMENTADA = "CTCR";

	/** The Constant ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA. */
	public static final String ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_LIBERADA = "LOPC";

	/** The Constant ET_TRANSFERENCIAS_FX_ONLINE_VALIDA_LIMITES_MONTOS. */
	public static final String ET_TRANSFERENCIAS_FX_ONLINE_VALIDA_LIMITES_MONTOS = "DITA";

	/** The Constant ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_MANCOMUNADA. */
	public static final String ET_TRANSFERENCIAS_FX_ONLINE_OPERACION_MANCOMUNADA = "MOPC";

	// CONSTANTES PARA ADMINISTRACIÃ“N DE CONTRASEÃ‘AS
	/** The Constant EAC_TOKEN. */
	public static final String EAC_TOKEN = "EACT";

	/** The Constant EAC_TOKEN_ENTRA. */
	public static final String EAC_TOKEN_ENTRA = "EACT01";

	/** The Constant EAC_CONSULTA_STATUS. */
	public static final String EAC_CONSULTA_STATUS = "EACS";

	/** The Constant EAC_CONSULTA_STATUS_ENTRA. */
	public static final String EAC_CONSULTA_STATUS_ENTRA = "EACS01";

	/** The Constant EAC_GENERA_CONTRASENIA. */
	public static final String EAC_GENERA_CONTRASENIA = "EACG";

	/** The Constant EAC_GENERA_CONTRASENIA_ENTRA. */
	public static final String EAC_GENERA_CONTRASENIA_ENTRA = "EACG01";

	/** The Constant EAC_GENERA_CONTRASENIA_REALIZADA. */
	public static final String EAC_GENERA_CONTRASENIA_REALIZADA = "EACG02";

	/** The Constant EAC_ACTIVA_CONTRASENIA. */
	public static final String EAC_ACTIVA_CONTRASENIA = "EACI";

	/** The Constant EAC_ACTIVA_CONTRASENIA_ENTRA. */
	public static final String EAC_ACTIVA_CONTRASENIA_ENTRA = "EACI01";

	/** The Constant EAC_ACTIVA_CONTRASENIA_REALIZADA. */
	public static final String EAC_ACTIVA_CONTRASENIA_REALIZADA = "EACI02";

	/** The Constant EAC_DESBLOQUEA_CONTRASENIA. */
	public static final String EAC_DESBLOQUEA_CONTRASENIA = "EACD";

	/** The Constant EAC_DESBLOQUEA_CONTRASENIA_ENTRA. */
	public static final String EAC_DESBLOQUEA_CONTRASENIA_ENTRA = "EACD01";

	/** The Constant EAC_DESBLOQUEA_CONTRASENIA_REALIZADA. */
	public static final String EAC_DESBLOQUEA_CONTRASENIA_REALIZADA = "EACD02";

	/** The Constant EAC_REIMPRESION. */
	public static final String EAC_REIMPRESION = "EACR";

	/** The Constant EAC_REIMPRESION_ENTRA. */
	public static final String EAC_REIMPRESION_ENTRA = "EACR01";

	/** The Constant EAC_REIMPRESION_ENTRA_REALIZADA. */
	public static final String EAC_REIMPRESION_ENTRA_REALIZADA = "EACR02";

	// everis 14-09-2007 CONSTANTES PARA PREGUNTAS SECRETAS ENLACE
	/** The Constant EPS_MULTICANAL. */
	public static final String EPS_MULTICANAL = "EPSM";

	/** The Constant EPS_MULTICANAL_CONSULTA_CLIENTE. */
	public static final String EPS_MULTICANAL_CONSULTA_CLIENTE = "EPSM01";

	/** The Constant EPS_MULTICANAL_ALTA_CLIENTE. */
	public static final String EPS_MULTICANAL_ALTA_CLIENTE = "EPSM02";

	/** The Constant EPS_MULTICANAL_DESACTIVA_CLIENTE. */
	public static final String EPS_MULTICANAL_DESACTIVA_CLIENTE = "EPSM03";

	// everis 22-02-2008 CONSTANTES PARA CATALOGO DE NOMINA
	/** The Constant EC_NOMINA_PROPIA. */
	public static final String EC_NOMINA_PROPIA = "ECNP";

	/** The Constant EC_NOMINA_PROPIA_CONSULTA. */
	public static final String EC_NOMINA_PROPIA_CONSULTA = "ECNP01";

	/** The Constant EC_NOMINA_PROPIA_ALTA. */
	public static final String EC_NOMINA_PROPIA_ALTA = "ECNP02";

	/** The Constant EC_NOMINA_PROPIA_MODIFICA. */
	public static final String EC_NOMINA_PROPIA_MODIFICA = "ECNP03";

	/** The Constant EC_NOMINA_INTERB. */
	public static final String EC_NOMINA_INTERB = "ECNI";

	/** The Constant EC_NOMINA_INTERB_CONSULTA. */
	public static final String EC_NOMINA_INTERB_CONSULTA = "ECNI01";

	/** The Constant EC_NOMINA_INTERB_ALTA. */
	public static final String EC_NOMINA_INTERB_ALTA = "ECNI02";

	/** The Constant EC_NOMINA_INTERB_MODIFICA. */
	public static final String EC_NOMINA_INTERB_MODIFICA = "ECNI03";

	/*
	 * <VC autor ="ESC" fecha = "25/03/2008" descripcion =
	 * "Bitacorizacion Redir"
	 */
	// Constantes Bitacora Salto Salto
	/** The Constant ER_REDIR_SALTO. */
	public static final String ER_REDIR_SALTO = "IRSN";

	/** The Constant ER_REDIR_SALTO_INICIA. */
	public static final String ER_REDIR_SALTO_INICIA = "IRSN01";
	// Constantes Bitacora Acceso Supernet.
	/** The Constant ER_REDIR_ACCESOSNET. */
	public static final String ER_REDIR_ACCESOSNET = "ACSN";

	/** The Constant ER_REDIR_ACCESOSNET_INICIA. */
	public static final String ER_REDIR_ACCESOSNET_INICIA = "ACSN01";

	/** The Constant ER_REDIR_ACCESOSNET_MSG. */
	public static final String ER_REDIR_ACCESOSNET_MSG = "Acceso desde Supernet";

	/** The Constant ER_REDIR_SALTO_MSG. */
	public static final String ER_REDIR_SALTO_MSG = "Cierre SesiÃ³n Redir";

	/** The Constant ER_REDIR_TIPOOPER. */
	public static final String ER_REDIR_TIPOOPER = "IRSN";
	/* </VC> */
	// <VC autor="ESC" fecha="17/07/2008"
	// descripcion="constantes de bitacora de operacion catalogo de nomina">
	/** The Constant ES_NOMINA_BUSQUEDA_EMPL. */
	public static final String ES_NOMINA_BUSQUEDA_EMPL = "BUEM";

	/** The Constant ES_NOMINA_BUSQUEDA_EMPL_MSG. */
	public static final String ES_NOMINA_BUSQUEDA_EMPL_MSG = "Busqueda de empleado";

	/** The Constant ES_NOMINA_REGISTRO_EMPLEADOS. */
	public static final String ES_NOMINA_REGISTRO_EMPLEADOS = "RECN";

	/** The Constant ES_NOMINA_REGISTRO_EMPLEADOS_MSG. */
	public static final String ES_NOMINA_REGISTRO_EMPLEADOS_MSG = "Registro de empleados en catalogo de nomina";

	/** The Constant ES_NOMINA_MOD_EMPL_EMPL_LINEA. */
	public static final String ES_NOMINA_MOD_EMPL_EMPL_LINEA = "MOEL";

	/** The Constant ES_NOMINA_MOD_EMPL_LINEA_MSG. */
	public static final String ES_NOMINA_MOD_EMPL_LINEA_MSG = "Modi de emple en lÃ­nea";

	/** The Constant ES_NOMINA_MOD_EMPL_ARCH. */
	public static final String ES_NOMINA_MOD_EMPL_ARCH = "MOEA";

	/** The Constant ES_NOMINA_MOD_EMPL_ARCH_MSG. */
	public static final String ES_NOMINA_MOD_EMPL_ARCH_MSG = "Modi de emple por archivo";

	/** The Constant ES_NOMINA_BAJA_EMPL_LINEA. */
	public static final String ES_NOMINA_BAJA_EMPL_LINEA = "BAEL";

	/** The Constant ES_NOMINA_BAJA_EMPL_LINEA_MSG. */
	public static final String ES_NOMINA_BAJA_EMPL_LINEA_MSG = "Baja en lÃ­nea";

	/** The Constant ES_NOMINA_BAJA_EMPL_ARCH. */
	public static final String ES_NOMINA_BAJA_EMPL_ARCH = "BAEA";

	/** The Constant ES_NOMINA_BAJA_EMPL_ARCH_MSG. */
	public static final String ES_NOMINA_BAJA_EMPL_ARCH_MSG = "Baja por archivo";

	/** The Constant ES_NOMINA_ALTA_INTERBAN_LINEA. */
	public static final String ES_NOMINA_ALTA_INTERBAN_LINEA = "ALIL";

	/** The Constant ES_NOMINA_ALTA_INTERBAN_LINEA_MSG. */
	public static final String ES_NOMINA_ALTA_INTERBAN_LINEA_MSG = "Alta interban en lÃ­nea";

	/** The Constant ES_NOMINA_ALTA_INTERBAN_ARCH. */
	public static final String ES_NOMINA_ALTA_INTERBAN_ARCH = "ALIA";

	/** The Constant ES_NOMINA_ALTA_INTERBAN_ARCH_MSG. */
	public static final String ES_NOMINA_ALTA_INTERBAN_ARCH_MSG = "Alta interban por archivo";

	/** The Constant ES_NOMINA_MOD_EMPL_INTERBAN_LINEA. */
	public static final String ES_NOMINA_MOD_EMPL_INTERBAN_LINEA = "MEIL";

	/** The Constant ES_NOMINA_MOD_EMPL_INTERBAN_LINEA_MSG. */
	public static final String ES_NOMINA_MOD_EMPL_INTERBAN_LINEA_MSG = "Modi emple inter en lÃ­nea";

	/** The Constant ES_NOMINA_MOD_EMPL_INTERBAN_ARCH. */
	public static final String ES_NOMINA_MOD_EMPL_INTERBAN_ARCH = "MEIA";

	/** The Constant ES_NOMINA_MOD_EMPL_INTERBAN_ARCH_MSG. */
	public static final String ES_NOMINA_MOD_EMPL_INTERBAN_ARCH_MSG = "Modi emple inter por archivo";

	/** The Constant ES_NOMINA_BAJA_INTERBAN_LINEA. */
	public static final String ES_NOMINA_BAJA_INTERBAN_LINEA = "BAIL";

	/** The Constant ES_NOMINA_BAJA_INTERBAN_LINEA_MSG. */
	public static final String ES_NOMINA_BAJA_INTERBAN_LINEA_MSG = "Baja emp inter en lÃ­nea";

	/** The Constant ES_NOMINA_BAJA_INTERBAN_ARCH. */
	public static final String ES_NOMINA_BAJA_INTERBAN_ARCH = "BAIA";

	/** The Constant ES_NOMINA_BAJA_INTERBAN_ARCH_MSG. */
	public static final String ES_NOMINA_BAJA_INTERBAN_ARCH_MSG = "Baja emp inter por archivo";
	// </VC>
	// <VC> Pago sua por linea de captura
	/** The Constant ES_PAGO_SUA_LINEA_CAPTURA. */
	public static final String ES_PAGO_SUA_LINEA_CAPTURA = "SULC";

	/* <P020101 Cuenta Nivel 3 Variables pistas de auditoria> */
	/** The Constant ES_NOMINAN3_RECEP_REMESA. */
	public static final String ES_NOMINAN3_RECEP_REMESA = "INAR";

	/** The Constant ES_NOMINAN3_RECEP_ENTRA. */
	public static final String ES_NOMINAN3_RECEP_ENTRA = "INAR01";

	/** The Constant ES_NOMINAN3_RECEP_RECIBE. */
	public static final String ES_NOMINAN3_RECEP_RECIBE = "INAR02";

	/** The Constant ES_NOMINAN3_RECEP_EJECUTA. */
	public static final String ES_NOMINAN3_RECEP_EJECUTA = "INAR03";

	/** The Constant ES_NOMINAN3_RECEP_EXPORTAR. */
	public static final String ES_NOMINAN3_RECEP_EXPORTAR = "INAR04";

	/** The Constant ES_NOMINAN3_CANCE_REMESA. */
	public static final String ES_NOMINAN3_CANCE_REMESA = "INXR";

	/** The Constant ES_NOMINAN3_CANCE_ENTRA. */
	public static final String ES_NOMINAN3_CANCE_ENTRA = "INXR01";

	/** The Constant ES_NOMINAN3_CANCE_RECIBE. */
	public static final String ES_NOMINAN3_CANCE_RECIBE = "INXR02";

	/** The Constant ES_NOMINAN3_CANCE_EJECUTA. */
	public static final String ES_NOMINAN3_CANCE_EJECUTA = "INXR03";

	/** The Constant ES_NOMINAN3_CANCE_EXPORTAR. */
	public static final String ES_NOMINAN3_CANCE_EXPORTAR = "INXR04";

	/** The Constant ES_NOMINAN3_CONSULTA_REMESA. */
	public static final String ES_NOMINAN3_CONSULTA_REMESA = "INCR";

	/** The Constant ES_NOMINAN3_CONSULTA_ENTRA. */
	public static final String ES_NOMINAN3_CONSULTA_ENTRA = "INCR01";

	/** The Constant ES_NOMINAN3_CONSULTA_CONSULTA. */
	public static final String ES_NOMINAN3_CONSULTA_CONSULTA = "INCR02";

	/** The Constant ES_NOMINAN3_CONSULTA_DETALLE. */
	public static final String ES_NOMINAN3_CONSULTA_DETALLE = "INCR03";

	/** The Constant ES_NOMINAN3_CONSULTA_CANCELA. */
	public static final String ES_NOMINAN3_CONSULTA_CANCELA = "INCR04";

	/** The Constant ES_NOMINAN3_CONSULTA_EXPORTAR. */
	public static final String ES_NOMINAN3_CONSULTA_EXPORTAR = "INCR05";

	/** The Constant ES_NOMINAN3_CANCE_TARJETA. */
	public static final String ES_NOMINAN3_CANCE_TARJETA = "INCT";

	/** The Constant ES_NOMINAN3_CANCE_TARJETA_ENTRA. */
	public static final String ES_NOMINAN3_CANCE_TARJETA_ENTRA = "INCR01";

	/** The Constant ES_NOMINAN3_CANCE_TARJETA_CONSULTA. */
	public static final String ES_NOMINAN3_CANCE_TARJETA_CONSULTA = "INCR02";

	/** The Constant ES_NOMINAN3_CANCE_TARJETA_CANCELA. */
	public static final String ES_NOMINAN3_CANCE_TARJETA_CANCELA = "INCR03";

	/** The Constant ES_NOMINAN3_ASIGNACION_IND. */
	public static final String ES_NOMINAN3_ASIGNACION_IND = "INAI";

	/** The Constant ES_NOMINAN3_ASIGNACION_IND_ENTRA. */
	public static final String ES_NOMINAN3_ASIGNACION_IND_ENTRA = "INAI01";

	/** The Constant ES_NOMINAN3_ASIGNACION_IND_ALTA. */
	public static final String ES_NOMINAN3_ASIGNACION_IND_ALTA = "INAI02";

	/** The Constant ES_NOMINAN3_ASIGNACION_ARCH. */
	public static final String ES_NOMINAN3_ASIGNACION_ARCH = "INAA";

	/** The Constant ES_NOMINAN3_ASIGNACION_ARCH_ENTRA. */
	public static final String ES_NOMINAN3_ASIGNACION_ARCH_ENTRA = "INAA01";

	/** The Constant ES_NOMINAN3_ASIGNACION_ARCH_RESUMEN. */
	public static final String ES_NOMINAN3_ASIGNACION_ARCH_RESUMEN = "INAA02";

	/** The Constant ES_NOMINAN3_ASIGNACION_ARCH_ALTA. */
	public static final String ES_NOMINAN3_ASIGNACION_ARCH_ALTA = "INAA03";

	/** The Constant ES_NOMINAN3_ASIGNACION_CONS. */
	public static final String ES_NOMINAN3_ASIGNACION_CONS = "INAC";

	/** The Constant ES_NOMINAN3_ASIGNACION_CONS_ENTRA. */
	public static final String ES_NOMINAN3_ASIGNACION_CONS_ENTRA = "INAC01";

	/** The Constant ES_NOMINAN3_ASIGNACION_CONS_CONSULTA. */
	public static final String ES_NOMINAN3_ASIGNACION_CONS_CONSULTA = "INAC02";

	/** The Constant ES_NOMINAN3_ASIGNACION_CONS_EXPORTAR. */
	public static final String ES_NOMINAN3_ASIGNACION_CONS_EXPORTAR = "INAC03";

	/* <P020101 Cuenta Nivel 3 Variables pistas de auditoria> */

	// <VC> fin Pago sua por linea de captura
	// LIGA ENLACE SUPERNET COMERCIOS.
	/** The Constant REDIRECCION_SUPERNET_COMERCIOS. */
	public static final String REDIRECCION_SUPERNET_COMERCIOS = "ESNC01";

	/** CONSTANTES DEL MICROSITIO ENLACE. */

	public static final String ME_OPERACION_MICROSITIO = "MICE";

	/** The Constant ME_INGRESO_TOKEN_ID. */
	public static final String ME_INGRESO_TOKEN_ID = "MICE00";

	/** The Constant ME_SEL_CONTRATO. */
	public static final String ME_SEL_CONTRATO = "MICE01";

	/** The Constant ME_BTN_CONF_DATOS. */
	public static final String ME_BTN_CONF_DATOS = "MICE02";

	/** The Constant ME_BTN_SALIR. */
	public static final String ME_BTN_SALIR = "MICE03";

	/** The Constant ME_PAGO. */
	public static final String ME_PAGO = "MICE04";

	// CONSTANTES PARA BITACORIZACION DE CONFIRMACIONES CON TOKEN (TABLA
	// TCT_BITACORA)

	/** Clave GFOL. */
	public static final String GENERACION_FOLIO_MANC = "GFOL";

	/** Mensaje Generaci&oacute;n de Folio de Mancomunidad. */
	public static final String CONCEPTO_GENERACION_FOLIO_MANC = "Generaci&oacute;n de Folio de Mancomunidad";

	/** The Constant CAMBIO_CONTRASENA. */
	public static final String CAMBIO_CONTRASENA = "PASS";
	/** CONCEPTO CAMBIO DE CONTRASEÃ‘A*. */
	public static final String CONCEPTO_CAMBIO_CONTRASENA = "Cambio de ContraseÃ±a";

	/** The Constant MODIFICACION_DATOS_PERSONALES. */
	public static final String MODIFICACION_DATOS_PERSONALES = "MODP";
	/** MODIFICACION CAMBIOS PERSONALES*. */
	public static final String CONCEPTO_MODIFICACION_DATOS_PERSONALES = "Modificaci"
			+ O_MIN_ACENTUADA + "n de Datos Personales ";

	/** The Constant CONSULTA_CAT_NOMINA_MISMO_BANCO. */
	public static final String CONSULTA_CAT_NOMINA_MISMO_BANCO = "CCNM";
	/** CONSULTA CATALOGO DE NOMINA MB*. */
	public static final String CONCEPTO_CONSULTA_CAT_NOMINA_MISMO_BANCO = "Consulta de Cat"
			+ A_MIN_ACENTUADA
			+ CAD_LOGO
			+ O_MIN_ACENTUADA
			+ "mina Mismo Banco";

	/** The Constant MODIFICACION_CAT_NOMINA_MISMO_BANCO. */
	public static final String MODIFICACION_CAT_NOMINA_MISMO_BANCO = "MCNM";
	/** MODIFICACION CATALOGO DE NOMINA MB*. */
	public static final String CONCEPTO_MODIFICACION_CAT_NOMINA_MISMO_BANCO = "Modificaci"
			+ O_MIN_ACENTUADA
			+ "n de Cat"
			+ A_MIN_ACENTUADA
			+ CAD_LOGO
			+ O_MIN_ACENTUADA + "mina Mismo Banco";

	/** The Constant BAJA_CAT_NOMINA_MISMO_BANCO. */
	public static final String BAJA_CAT_NOMINA_MISMO_BANCO = "BCNM";
	/** BAJA CATALOGO DE NOMINA MB*. */
	public static final String CONCEPTO_BAJA_CAT_NOMINA_MISMO_BANCO = "Baja de Cat"
			+ A_MIN_ACENTUADA
			+ CAD_LOGO
			+ O_MIN_ACENTUADA
			+ "mina Mismo Banco";

	/** The Constant CONSULTA_SEGUIMIENTO_TRANSFERENCIAS. */
	public static final String CONSULTA_SEGUIMIENTO_TRANSFERENCIAS = "COST";
	/** CONS. SEG TRANSFERENCIAS*. */
	public static final String CONCEPTO_CONSULTA_SEGUIMIENTO_TRANSFERENCIAS = "Consulta de Seguimiento de Transferencias";

	/** The Constant CONSULTA_CAT_NOMINA_INTERB. */
	public static final String CONSULTA_CAT_NOMINA_INTERB = "CCNI";
	/** CONS. CAT NOMINA INTERBANCARIA*. */
	public static final String CONCEPTO_CONSULTA_CAT_NOMINA_INTERB = "Consulta de Cat"
			+ A_MIN_ACENTUADA + CAD_LOGO + O_MIN_ACENTUADA + NOM_INT;

	/** The Constant BAJA_CAT_NOMINA_INTERB. */
	public static final String BAJA_CAT_NOMINA_INTERB = "BCNI";
	/** BAJA CATALOGO DE NOMINA INTERBANCARIA*. */
	public static final String CONCEPTO_BAJA_CAT_NOMINA_INTERB = "Baja de Cat"
			+ A_MIN_ACENTUADA + CAD_LOGO + O_MIN_ACENTUADA + NOM_INT;

	/** The Constant AUTORIZACION_CANCELACION_CUENTAS. */
	public static final String AUTORIZACION_CANCELACION_CUENTAS = "ACNI";
	/** AUTORIZACION Y CANCELACION DE CUENTAS*. */
	public static final String CONCEPTO_AUTORIZACION_CANCELACION_CUENTAS = "Autorizaci"
			+ O_MIN_ACENTUADA
			+ "n y Cancelaci"
			+ O_MIN_ACENTUADA
			+ "n de Cuentas en N" + O_MIN_ACENTUADA + NOM_INT;

	/** The Constant CTK_OPERACIONES_MANC. */
	public static final String CTK_OPERACIONES_MANC = "CMAN";
	/** CONFIRMCACION DE OPERACIONES MANCOMUNADAS*. */
	public static final String CTK_CONCEPTO_OPERACIONES_MANC = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Operaciones Mancomunadas";

	/** The Constant CTK_GENERACION_FOLIO_MANC. */
	public static final String CTK_GENERACION_FOLIO_MANC = "CGFO";
	/** GENERACION DE FOLIO DE OPERACIONES MANCOMUNADAS*. */
	public static final String CTK_CONCEPTO_GENERACION_FOLIO_MANC = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Generaci" + O_MIN_ACENTUADA
			+ "n de Folio";

	/** The Constant CTK_CAMBIO_CONTRATO. */
	public static final String CTK_CAMBIO_CONTRATO = "CCTR";
	/** CAMBIO DE CONTRATO*. */
	public static final String CONCEPTO_CAMBIO_CONTRATO = "Cambio de Contrato";

	/** The Constant CTK_TRANSFERENCIA_MN. */
	public static final String CTK_TRANSFERENCIA_MN = "CTRA";

	/** The Constant CTK_CONCEPTO_TRANSFERENCIA_MN. */
	public static final String CTK_CONCEPTO_TRANSFERENCIA_MN = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de transferencia Moneda Nacional";

	/** The Constant CTK_TRANSFERENCIA_DOLARES. */
	public static final String CTK_TRANSFERENCIA_DOLARES = "CTUS";

	/** The Constant CTK_CONCEPTO_TRANSFERENCIA_DOLARES. */
	public static final String CTK_CONCEPTO_TRANSFERENCIA_DOLARES = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de transferencia en D" + O_MIN_ACENTUADA
			+ "lares";

	/** The Constant CTK_PAGO_OCURRE_MORALES. */
	public static final String CTK_PAGO_OCURRE_MORALES = "COCM";

	/** The Constant CTK_CONCEPTO_PAGO_OCURRE_MORALES. */
	public static final String CTK_CONCEPTO_PAGO_OCURRE_MORALES = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago Ocurre Morales";

	/** The Constant CTK_PAGO_OCURRE_FISICAS. */
	public static final String CTK_PAGO_OCURRE_FISICAS = "COCF";

	/** The Constant CTK_CONCEPTO_PAGO_OCURRE_FISICAS. */
	public static final String CTK_CONCEPTO_PAGO_OCURRE_FISICAS = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago Ocurre F" + I_MIN_ACENTUADA
			+ "sicas";

	/** The Constant CTK_TRANSFERENCIAS_INTERB. */
	public static final String CTK_TRANSFERENCIAS_INTERB = "COTI";

	/** The Constant CTK_CONCEPTO_TRANSFERENCIAS_INTERB. */
	public static final String CTK_CONCEPTO_TRANSFERENCIAS_INTERB = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Transferencias Interbancarias";

	/** The Constant CTK_PAGO_TARJETA_CREDITO. */
	public static final String CTK_PAGO_TARJETA_CREDITO = "CPTC";

	/** The Constant CTK_CONCEPTO_PAGO_TARJETA_CREDITO. */
	public static final String CTK_CONCEPTO_PAGO_TARJETA_CREDITO = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de Tarjeta de Cr" + E_MIN_ACENTUADA
			+ "dito";

	/** The Constant CTK_DISP_TARJETA_CREDITO. */
	public static final String CTK_DISP_TARJETA_CREDITO = "CDTC";

	/** The Constant CTK_CONCEPTO_DISP_TARJETA_CREDITO. */
	public static final String CTK_CONCEPTO_DISP_TARJETA_CREDITO = CONFIRMACI
			+ O_MIN_ACENTUADA + "n Disposici" + O_MIN_ACENTUADA
			+ "n de Tarjeta de Cr" + E_MIN_ACENTUADA + "dito";

	/** The Constant CTK_TRANSFERENCIAS_INTER. */
	public static final String CTK_TRANSFERENCIAS_INTER = "CTIN";

	/** The Constant CTK_CONCEPTO_TRANSFERENCIAS_INTER. */
	public static final String CTK_CONCEPTO_TRANSFERENCIAS_INTER = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Transferencias Internacionales";

	/** The Constant CTK_PAGO_IMP_PROV. */
	public static final String CTK_PAGO_IMP_PROV = "CSAT";

	/** The Constant CTK_CONCEPTO_PAGO_IMP_PROV. */
	public static final String CTK_CONCEPTO_PAGO_IMP_PROV = "Confirmaci&oacute;n de Pago de Impuestos Provisionales";

	/** The Constant CTK_PAGO_IMP_EJER. */
	public static final String CTK_PAGO_IMP_EJER = "CSAE";

	/** The Constant CTK_CONCEPTO_PAGO_IMP_EJER. */
	public static final String CTK_CONCEPTO_PAGO_IMP_EJER = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de Impuestos del Ejercicio";

	/** The Constant CTK_PAGO_IMP_FEDER. */
	public static final String CTK_PAGO_IMP_FEDER = "CSEF";

	/** The Constant CTK_CONCEPTO_PAGO_IMP_FEDER. */
	public static final String CTK_CONCEPTO_PAGO_IMP_FEDER = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de Impuestos Entidades Federativas";

	/** The Constant CTK_PAGO_IMP_DER_PROD_APR. */
	public static final String CTK_PAGO_IMP_DER_PROD_APR = "CDPA";

	/** The Constant CTK_CONCEPTO_PAGO_IMP_DER_PROD_APR. */
	public static final String CTK_CONCEPTO_PAGO_IMP_DER_PROD_APR = CONFIRMACI
			+ O_MIN_ACENTUADA
			+ "n de Pago de Impuestos Derechos, Productos y Aprovechamiento";

	/** The Constant CTK_PAGO_IMP_DER_PROD_LCAP. */
	public static final String CTK_PAGO_IMP_DER_PROD_LCAP = "CPLC";
	/*
	 * Mejoras de Linea de Captura Se modifica la leyenda Linea de Captura por
	 * Pago Referenciado SAT Inicio RRR - Indra Marzo 2014
	 */
	/** The Constant CTK_CONCEPTO_PAGO_IMP_DER_PROD_LCAP. */
	public static final String CTK_CONCEPTO_PAGO_IMP_DER_PROD_LCAP = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de Impuestos Pago Referenciado SAT";
	/*
	 * Fin RRR - Indra Marzo 2014
	 */
	/** The Constant CTK_PAGO_SUA. */
	public static final String CTK_PAGO_SUA = "CSUA";

	/** The Constant CTK_CONCEPTO_PAGO_SUA. */
	public static final String CTK_CONCEPTO_PAGO_SUA = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de Impuestos SUA";

	/** The Constant CTK_PAGO_SUA_SIPARE. */
	public static final String CTK_PAGO_SUA_SIPARE = "CSIP";

	/** The Constant CTK_CONCEPTO_PAGO_SUA_SIPARE. */
	public static final String CTK_CONCEPTO_PAGO_SUA_SIPARE = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de pago SUA (SIPARE)";

	/** The Constant CTK_PAGO_NOMINA. */
	public static final String CTK_PAGO_NOMINA = "CPNS";
	/** CONFIRMACION DE PAGO NOMINA PREPAGO*. */
	public static final String CTK_CONCEPTO_PAGO_NOMINA = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de N" + O_MIN_ACENTUADA
			+ "mina Santander";

	/** Clave CONL Clave confirmacion de pago. */
	public static final String CTK_PAGO_NOMINALN = "CONL";

	/** Mensaje confirmacion de pago. */
	public static final String CTK_CONCEPTO_PAGO_NOMINALN = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de N" + O_MIN_ACENTUADA
			+ "mina en Linea";

	/** The Constant CTK_PAGO_NOMINA_INTERB. */
	public static final String CTK_PAGO_NOMINA_INTERB = "CPNI";
	/** CONFIRMACION DE PAGO NOMINA INTERBANCARIA*. */
	public static final String CTK_CONCEPTO_PAGO_NOMINA_INTERB = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago de N" + O_MIN_ACENTUADA + NOM_INT;

	/** The Constant CTK_PAGO_NOMINA_PREP. */
	public static final String CTK_PAGO_NOMINA_PREP = "CPNP";
	/** CONCEPTO DE PAGO NOMINA PREPAGO*. */
	public static final String CTK_CONCEPTO_PAGO_NOMINA_PREP = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago Tarjeta de Pagos Santander "; // YHG
																			// NPRE

	/** The Constant CTK_CHEQUE_SEG. */
	public static final String CTK_CHEQUE_SEG = "CCHS";
	/** CONCEPTO CHEQUERA SEGURIDAD*. */
	public static final String CTK_CONCEPTO_CHEQUE_SEG = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Liberaci" + O_MIN_ACENTUADA
			+ "n de Cheque Seguridad";

	/** The Constant CTK_PAGO_DIRECTO. */
	public static final String CTK_PAGO_DIRECTO = "CPAD";
	/** CONCEPTO PAGO DIRECTO*. */
	public static final String CTK_CONCEPTO_PAGO_DIRECTO = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Liberaci" + O_MIN_ACENTUADA
			+ "n de Pago Directo";

	/** The Constant CTK_PAGO_PROVEEDORES. */
	public static final String CTK_PAGO_PROVEEDORES = "COPP";
	/** CONCEPTO PAGO A PROVEEDORES*. */
	public static final String CTK_CONCEPTO_PAGO_PROVEEDORES = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago a Proveedores";

	/** The Constant CTK_CAMBIO_DIVISAS. */
	public static final String CTK_CAMBIO_DIVISAS = "CCDI";
	/** CONCEPTO PAGO CAMBIO DE DIVISAS*. */
	public static final String CTK_CONCEPTO_CAMBIO_DIVISAS = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Cambio de Divisas";

	/** The Constant CTK_PAGO_SAR. */
	public static final String CTK_PAGO_SAR = "CSAR";
	/** CONFIRMACION DE PAGO SAR*. */
	public static final String CTK_CONCEPTO_PAGO_SAR = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago SAR";

	/** The Constant CTK_COMPRA_FONDOS_INV. */
	public static final String CTK_COMPRA_FONDOS_INV = "CCFI";
	/** CONFIRMACION DE COMPRA DE FONDOS INVERSION*. */
	public static final String CTK_CONCEPTO_COMPRA_FONDOS_INV = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Compra de Fondos de Inversi"
			+ O_MIN_ACENTUADA + "n";

	/** The Constant CTK_VENTA_FONDOS_INV. */
	public static final String CTK_VENTA_FONDOS_INV = "CVFI";
	/** CONFIRMACION DE VENTA FONDOS DE INVERSION*. */
	public static final String CTK_CONCEPTO_VENTA_FONDOS_INV = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Venta de Fondos de Inversi"
			+ O_MIN_ACENTUADA + "n";

	/** The Constant CTK_CHEQUE_VISTA. */
	public static final String CTK_CHEQUE_VISTA = "CCHV";
	/** CONFIRMACION DE CHEQUES VISTA*. */
	public static final String CTK_CONCEPTO_CHEQUE_VISTA = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Cheque Vista";

	/** Confirmaciones desde el Micrositio. */
	public static final String CTK_PAGO_MICROSITIO = "CPMS";
	/** CONFIRMACION DE PAGO MS*. */
	public static final String CTK_CONCEPTO_PAGO_MS = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Pago Micrositio";

	/** The Constant CTK_INGRESO_MICROSITIO. */
	public static final String CTK_INGRESO_MICROSITIO = "CIMS";
	/** CONFIRMACION INGRESO A MICRO SITIO*. */
	public static final String CTK_CONCEPTO_INGRESO_MS = CONFIRMACI
			+ O_MIN_ACENTUADA + "n de Ingreso Micrositio";

	/**
	 * Codigo de operacion para la consulta de los favoritos.
	 */
	public static final String CONSULTA_FAVORITOS = "COPF";
	/**
	 * Codigo de operacion para la seleccion de favoritos.
	 */
	public static final String SELECCIONA_FAVORITO = "SOPF";
	/**
	 * Codigo de operacion para el alta de favorito.
	 */
	public static final String ALTA_FAVORITO = "AOPF";
	/**
	 * Codigo de operacion para el baja de favorito.
	 */
	public static final String BAJA_FAVORITO = "BOPF";
	/**
	 * Codigo de operacion para la modificacion de un favorito.
	 */
	public static final String MODIFICAR_FAVORITO = "MOPF";

}