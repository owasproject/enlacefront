package mx.altec.enlace.bita;


import java.io.PrintStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import mx.altec.enlace.servlets.BaseServlet;
import mx.altec.enlace.utilerias.EIGlobal;




public class ConnectionHelper {
	private Connection conexion = null;



	public ConnectionHelper(String key)throws SQLException, Exception
	{
		this.conexion = BaseServlet.createiASConn_static2(key);

	}

	public ConnectionHelper(String key,int ds)throws SQLException, Exception
	{
		this.conexion = BaseServlet.createiASConn_static2(key);

	}


	public ResultSet ejecutaQuery (String query)throws SQLException, Exception{
		Statement stm = null;
		ResultSet rs = null;
		 stm = conexion.createStatement();
		 try{
			 rs = stm.executeQuery(query);
			 EIGlobal.mensajePorTrace( "VSWF: BITACORIZANDO: " + query, EIGlobal.NivelLog.INFO);
		 }catch(SQLException se){
			 EIGlobal.mensajePorTrace( "BITACORIZACION DATOS PARA INSERT: " + query, EIGlobal.NivelLog.INFO);
		 }
		 catch(Exception ee){
			 EIGlobal.mensajePorTrace( "BITACORIZACION DATOS PARA INSERT: " + query, EIGlobal.NivelLog.INFO);
		 }


		 return rs;

	}


	public ResultSet insertaTransact(BitaTransacBean bt)throws SQLException, Exception{
	Statement stm = null;
	ResultSet rs = null;
	StringBuffer sb = new StringBuffer("");
	 stm = conexion.createStatement();
	 java.sql.PreparedStatement ps = null;
	 try{

		 sb.append("INSERT INTO EWEB_TRAN_BITACORA (FOLIO_BIT, FOLIO_FLUJO, ID_FLUJO, NUM_BIT, \n")
		.append("FECHA, FECHA_PROG, TIPO_MONEDA, \n")
		.append("NUM_TIT, REFERENCIA, IMPORTE, TIPO_CAMBIO, ID_ERR, HORA, DIR_IP, COD_CLIENTE, \n")
		.append("ID_TOKEN, BANCO_DEST, SERV_TRANS_TUX, CANAL, USR, CONTRATO, ID_WEB, ID_SESION, CCTA_ORIG, \n")
		.append("CCTA_DEST, NOMBRE_HOST_WEB, FECHA_DE_APLICACION, ESTATUS_DE_LA_OPERACION, NOMBRE_ARCH) \n ")
		.append("VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		 ps = conexion.prepareStatement(sb.toString());
		 ps.setLong(1, bt.getFolioBit());
		 ps.setLong(2,bt.getFolioFlujo());
		 ps.setString(3, bt.getIdFlujo());
		 ps.setString(4, bt.getNumBit());
		 ps.setDate(5,  new java.sql.Date(bt.getFecha().getTime())        );
		 if (bt.getFechaProgramada()!=null){
			 ps.setDate(6, new java.sql.Date(bt.getFechaProgramada().getTime()));
		 }else{
			 ps.setNull(6, Types.DATE);
		 }
		 if (bt.getTipoMoneda()!=null){
			 ps.setString(7, bt.getTipoMoneda());
		 }else{
			 ps.setNull(7, Types.VARCHAR);
		 }
		 ps.setLong(8, bt.getNumTit());
		 ps.setLong(9, bt.getReferencia());
		 ps.setDouble(10, bt.getImporte());
		 ps.setDouble(11, bt.getTipoCambio());
		 if (bt.getIdErr()!=null){
			 ps.setString(12, bt.getIdErr());
		 }else{
			 ps.setNull(12, Types.VARCHAR);
		 }
		 if (bt.getHora()!=null){
			 ps.setString(13, bt.getHora());
		 }else{
			 ps.setNull(13, Types.VARCHAR);
		 }
		 if (bt.getDirIp()!=null){
			 ps.setString(14, bt.getDirIp());
		 }else{
			 ps.setNull(14, Types.VARCHAR);
		 }
		 if (bt.getCodCliente()!=null){
			 ps.setString(15, BaseServlet.convierteUsr7a8(bt.getCodCliente()));
		 }else{
			 ps.setNull(15, Types.VARCHAR);
		 }
		 if (bt.getIdToken()!=null){
			 ps.setString(16, bt.getIdToken());
		 }else{
			 ps.setNull(16, Types.VARCHAR);
		 }
		 if (bt.getBancoDest()!=null){
			 ps.setString(17, bt.getBancoDest());
		 }else{
			 ps.setNull(17, Types.VARCHAR);
		 }
		 if (bt.getServTransTux()!=null){
			 ps.setString(18, bt.getServTransTux());
		 }else{
			 ps.setNull(18, Types.VARCHAR);
		 }
		 if (bt.getCanal()!=null){
			 ps.setString(19, bt.getCanal());
		 }else{
			 ps.setNull(19, Types.VARCHAR);
		 }
		 if (bt.getUsr()!=null){
			 ps.setString(20, BaseServlet.convierteUsr7a8(bt.getUsr()));
		 }else{
			 ps.setNull(20, Types.VARCHAR);
		 }
		 if (bt.getContrato()!=null){
			 ps.setString(21, bt.getContrato());
		 }else{
			 ps.setNull(21, Types.VARCHAR);
		 }
		 if (bt.getIdWeb()!=null){
			 ps.setString(22, bt.getIdWeb());
		 }else{
			 ps.setNull(22, Types.VARCHAR);
		 }
		 if (bt.getIdSesion()!=null){
			 ps.setString(23, bt.getIdSesion());
		 }else{
			 ps.setNull(23, Types.VARCHAR);
		 }
		 if (bt.getCctaOrig()!=null){
			 ps.setString(24, bt.getCctaOrig());
		 }else{
			 ps.setNull(24, Types.VARCHAR);
		 }

		 if (bt.getCctaDest()!=null){
			 ps.setString(25, bt.getCctaDest());
		 }else{
			 ps.setNull(25, Types.VARCHAR);
		 }
		 if (bt.getNombreHostWeb()!=null){
			 ps.setString(26, bt.getNombreHostWeb());
		 }else{
			 ps.setNull(26, Types.VARCHAR);
		 }

		 if (bt.getFechaAplicacion()!=null){
			 ps.setDate(27, new java.sql.Date(bt.getFechaAplicacion().getTime()));
		 }else{
			 ps.setNull(27, Types.DATE);
		 }

		 if (bt.getEstatus()!=null){
			 ps.setString(28, bt.getEstatus());
		 }else{
			 ps.setNull(28, Types.VARCHAR);
		 }
		 if (bt.getNombreArchivo()!=null){
			 ps.setString(29, bt.getNombreArchivo());
		 }else{
			 ps.setNull(29, Types.VARCHAR);
		 }

		  StringBuffer sbInsert = new StringBuffer("");
		 sbInsert.append(bt.getFolioBit()) .append(",")
		 .append(bt.getFolioFlujo()) .append(",")
		 .append(bt.getIdFlujo()) .append(",")
		 .append(bt.getNumBit()) .append(",")
		 .append(bt.getFecha()) .append(",")
		 .append(bt.getFechaProgramada()) .append(",")
		 .append(bt.getTipoMoneda()) .append(",")
		 .append(bt.getNumTit()) .append(",")
		 .append(bt.getReferencia()) .append(",")
		 .append(bt.getImporte()) .append(",")
		 .append(bt.getTipoCambio()) .append(",")
		 .append(bt.getIdErr()) .append(",")
		 .append(bt.getHora()) .append(",")
		 .append(bt.getDirIp()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getCodCliente())) .append(",")
		 .append(bt.getIdToken()) .append(",")
		 .append(bt.getBancoDest()) .append(",")
		 .append(bt.getServTransTux()) .append(",")
		 .append(bt.getCanal()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getUsr())) .append(",")
		 .append(bt.getContrato()) .append(",")
		 .append(bt.getIdWeb()) .append(",")
	     .append(bt.getIdSesion()) .append(",")
	      .append(bt.getCctaOrig()) .append(",")
	      .append(bt.getCctaDest()) .append(",")
	      .append(bt.getNombreHostWeb()) .append(",")
	      .append(bt.getFechaAplicacion()) .append(",")
	      .append(bt.getEstatus()) .append(",")
	      .append(bt.getNombreArchivo()) .append("\n");

		 EIGlobal.mensajePorTrace( "VSWF BITACORIZANDO DATOS: \n" + sbInsert.toString(), EIGlobal.NivelLog.INFO);
		 rs = ps.executeQuery();
	 }catch(SQLException se){

		 StringBuffer sbError = new StringBuffer("");
		 EIGlobal.mensajePorTrace( "BITACORIZACION DATOS PARA INSERT: " + sb.toString(), EIGlobal.NivelLog.INFO);
		 sbError.append(bt.getFolioBit()) .append(",")
		 .append(bt.getFolioFlujo()) .append(",")
		 .append(bt.getIdFlujo()) .append(",")
		 .append(bt.getNumBit()) .append(",")
		 .append(bt.getFecha()) .append(",")
		 .append(bt.getFechaProgramada()) .append(",")
		 .append(bt.getTipoMoneda()) .append(",")
		 .append(bt.getNumTit()) .append(",")
		 .append(bt.getReferencia()) .append(",")
		 .append(bt.getImporte()) .append(",")
		 .append(bt.getTipoCambio()) .append(",")
		 .append(bt.getIdErr()) .append(",")
		 .append(bt.getHora()) .append(",")
		 .append(bt.getDirIp()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getCodCliente())) .append(",")
		 .append(bt.getIdToken()) .append(",")
		 .append(bt.getBancoDest()) .append(",")
		 .append(bt.getServTransTux()) .append(",")
		 .append(bt.getCanal()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getUsr())) .append(",")
		 .append(bt.getContrato()) .append(",")
		 .append(bt.getIdWeb()) .append(",")
	     .append(bt.getIdSesion()) .append(",")
	      .append(bt.getCctaOrig()) .append(",")
	      .append(bt.getCctaDest()) .append(",")
	      .append(bt.getNombreHostWeb()) .append(",")
	      .append(bt.getFechaAplicacion()) .append(",")
	      .append(bt.getEstatus()) .append(",")
	      .append(bt.getNombreArchivo()) .append(",");

	     EIGlobal.mensajePorTrace( "DATOS: " + sbError.toString(), EIGlobal.NivelLog.INFO);
		 EIGlobal.mensajePorTrace(se.getMessage(), EIGlobal.NivelLog.INFO);
		 se.printStackTrace();
	 }
	 catch(Exception ee){
		 EIGlobal.mensajePorTrace( "BITACORIZACION DATOS PARA INSERT: " + sb.toString(), EIGlobal.NivelLog.INFO);

		 StringBuffer sbError = new StringBuffer("");
		  sbError.append(bt.getFolioBit()) .append(",")
		 .append(bt.getFolioFlujo()) .append(",")
		 .append(bt.getIdFlujo()) .append(",")
		 .append(bt.getNumBit()) .append(",")
		 .append(bt.getFecha()) .append(",")
		 .append(bt.getFechaProgramada()) .append(",")
		 .append(bt.getTipoMoneda()) .append(",")
		 .append(bt.getNumTit()) .append(",")
		 .append(bt.getReferencia()) .append(",")
		 .append(bt.getImporte()) .append(",")
		 .append(bt.getTipoCambio()) .append(",")
		 .append(bt.getIdErr()) .append(",")
		 .append(bt.getHora()) .append(",")
		 .append(bt.getDirIp()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getCodCliente())) .append(",")
		 .append(bt.getIdToken()) .append(",")
		 .append(bt.getBancoDest()) .append(",")
		 .append(bt.getServTransTux()) .append(",")
		 .append(bt.getCanal()) .append(",")
		 .append(BaseServlet.convierteUsr7a8(bt.getUsr())) .append(",")
		 .append(bt.getContrato()) .append(",")
		 .append(bt.getIdWeb()) .append(",")
	     .append(bt.getIdSesion()) .append(",")
	      .append(bt.getCctaOrig()) .append(",")
	      .append(bt.getCctaDest()) .append(",")
	      .append(bt.getNombreHostWeb()) .append(",")
	      .append(bt.getFechaAplicacion()) .append(",")
	      .append(bt.getEstatus()) .append(",")
	      .append(bt.getNombreArchivo()) .append(",");

	     EIGlobal.mensajePorTrace( "DATOS: " + sbError.toString(), EIGlobal.NivelLog.INFO);



		 EIGlobal.mensajePorTrace(ee.getMessage(), EIGlobal.NivelLog.INFO);
		 ee.printStackTrace();
	 }
	 finally{
		 ps.close();
	}

	 return rs;

	}

	public long nextFolioFlujo()throws SQLException, Exception{
		long i = 0;
		Statement stm = null;
		ResultSet rs = null;
		try{
		 stm = conexion.createStatement();
		 rs = stm.executeQuery("Select EWEB_SEQ_FOLIO_FLUJO.NextVal from dual");
		 if (rs.next()){
			 i = rs.getLong(1);
		 }

		}catch(Exception e){
			 e.printStackTrace();
		 }
		return i;
	}

	public long nextFolioBitacora()throws SQLException, Exception{
		long i = 0;
		Statement stm = null;
		ResultSet rs = null;
		try{
		 stm = conexion.createStatement();
		 rs = stm.executeQuery("Select EWEB_SEQ_FOLIO_BIT.NextVal from dual");

		 if (rs.next()){
			 i = rs.getLong(1);
		 }
		}catch(Exception e){
			 e.printStackTrace();
		 }

		return i;
	}

	public Connection getConnection(){
		return conexion;
	}

	public void cerrarConexion()throws SQLException, Exception{

			if (conexion != null && !conexion.isClosed()){
				conexion.close ();
				conexion = null;
			}

	}


}