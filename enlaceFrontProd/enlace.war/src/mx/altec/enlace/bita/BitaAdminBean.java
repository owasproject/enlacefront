package mx.altec.enlace.bita;

public class BitaAdminBean extends BitaBean {

	public int testVar = 0;
	private String tipoOp;
	private String idTabla;
	private String campo;
	private String tabla;
	private String dato;
	private String datoNvo;
	private long folioOp;
	public long getFolioOp() {
		return folioOp;
	}
	public void setFolioOp(long folioOp) {
		this.folioOp = folioOp;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getDato() {
		return dato;
	}
	public void setDato(String dato) {
		this.dato = dato;
	}
	public String getDatoNvo() {
		return datoNvo;
	}
	public void setDatoNvo(String datoNvo) {
		this.datoNvo = datoNvo;
	}
	public String getIdTabla() {
		return idTabla;
	}
	public void setIdTabla(String idTabla) {
		this.idTabla = idTabla;
	}
	public String getTabla() {
		return tabla;
	}
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}
	public String getTipoOp() {
		return tipoOp;
	}
	public void setTipoOp(String tipoOp) {
		this.tipoOp = tipoOp;
	}

}