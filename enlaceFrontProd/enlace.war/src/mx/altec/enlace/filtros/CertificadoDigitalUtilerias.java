package mx.altec.enlace.filtros;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.altec.enlace.utilerias.EIGlobal;
import mx.altec.enlace.utilerias.Global;

public class CertificadoDigitalUtilerias extends CertificadoDigitalFilter{
	
	
	/**cadena MMC_ALTA***/
	private static final String MMC_ALTA = "MMC_Alta";
	/**cadena fileName***/
	private static final String COMTO_PROVEEDOR = "coMtoProveedor";
	/**cadena fileName****/
	private static final String COPAGOS = "coPagos";
	/**cadena fileName***/
	private static final String NOMINTERREG = "CatNomInterbRegistro";
	/**cadena fileName***/
	private static final String PDREGISTRO = "pdRegistro";
	/**cadena fileName***/
	private static final String CHESREGISTRO= "ChesRegistro";
	/**cadena fileName***/
	private static final String NOM_EMPIMPORTA= "NomEmpImporta";
	/**cadena fileName***/
	private static final String ANOM_MTTOEMP= "ANomMttoEmp";
	/**cadena fileName***/
	private static final String NOM_PREALTATARJ= "NomPreAltaTarjeta";
	/**cadena fileName***/
	private static final String PREVALIDADOR_SERVLET= "PrevalidadorServlet";
	/**cadena fileName***/
	private static final String ASIGNACION_SERV= "AsignacionEmplServlet";
	/**cadena fileName***/
	private static final String MDI_ORDEN_PAGO_OCURRE= "MDI_Orden_Pago_Ocurre";
	/**cadena fileName***/
	private static final String IMPORT_NOMINA= "ImportNomina";
	/**cadena fileName***/
	private static final String IMPORT_NOMINALN= "ImportNominaLn";
	/**cadena fileName***/
	private static final String NOM_PREDISPERSION= "NomPredispersion";
	/**cadena fileName***/
	private static final String INICIO_NOMINA= "InicioNomina";
	/**cadena fileName***/
	private static final String INICIO_NOMINAINTER= "InicioNominaInter";
	/**cadena fileName***/
	private static final String INICIO_NOMINALN= "InicioNominaLn";
	/**canena mensajeCifrado**/
	private static final String TRANSFERENCIA = "transferencia";
	/**canena mensajeCifrado**/
	private static final String SERVLET_CIFRADO = "ServletInvocaCifrado";
	/**canena de Error mensajeCifrado**/
	private static final String MENSAJE_ERRROR = "mensajeCifradoError";
	/**canena de opcion***/
	private static final String OPCION = "opcion";
	/**cadena fileName***/
	private static final String CATALOGO_NOMINA_EMPL = "CatalogoNominaEmpl";

	/**
	 * @param request : HttpServletRequest
	 * @return String id de bitacora
	 * @throws ServletException exception
	 * @throws IOException exception
	 */
	public String obtieneIdBitacora(HttpServletRequest request) throws ServletException, IOException {
			
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> obtieneIdBitacora" , EIGlobal.NivelLog.INFO);
			
				String servletInvocaRuta = request.getServletPath();
				String [] arreglo = servletInvocaRuta.split("\\/");
				String servletInvoca = arreglo[2].trim();
				String idBitacora = "";
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ruta invoca "+servletInvocaRuta , EIGlobal.NivelLog.INFO);
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> SERV "+servletInvoca , EIGlobal.NivelLog.INFO);
				
				if(servletInvoca.equals(NOMINTERREG)) {
					idBitacora = "ACAI";
				} else if(servletInvoca.equals(TRANSFERENCIA)) {
					String flujoTranferencia = request.getSession().getAttribute(SERVLET_CIFRADO) != null 
						? request.getSession().getAttribute(SERVLET_CIFRADO).toString() : ""; 
					String id = request.getParameter("cifrado");
					idBitacora = "2".equals(id) ? "ACTN" : "1".equals(id) ? "ACTD" : flujoTranferencia.contains("transferenciaInt") ? "ACPT" : "";
				}else if("MDI_Interbancario".equals(servletInvoca)){
					idBitacora = "ACTB";
				}else if(servletInvoca.equals(ANOM_MTTOEMP) || servletInvoca.equals(NOM_EMPIMPORTA)){
					idBitacora = "ACAE";
				}else if(servletInvoca.equals( NOM_PREALTATARJ)){
					idBitacora = "ACAT";	
				}else if(servletInvoca.equals(PREVALIDADOR_SERVLET)){
					idBitacora = "ACAN";
				}else if(servletInvoca.contains(CATALOGO_NOMINA_EMPL)){
					String opcion = request.getParameter(OPCION);
					if("6".equals(opcion) || "7".equals(opcion) ) {
						idBitacora = "ACBN";
					} else if("9".equals(opcion) || "10".equals(opcion)) {
						idBitacora = "ACMN";
					}			
				}else{
					idBitacora = obtieneIdBita(servletInvoca, request);
				}
				
				return idBitacora;
			
			}

	/**
	 * @param servletInvoca : servletInvoca nombre del servlet
	 * @param request : HttpServletRequest
	 * @return String id de bitacora
	 * @throws ServletException exception
	 * @throws IOException exception
	 */
	public String obtieneIdBita(String servletInvoca, HttpServletRequest request) throws ServletException, IOException {
				
				String idBitacora = "";
				if(servletInvoca.equals(ASIGNACION_SERV)){
					idBitacora = "ACTE";
				}else if(servletInvoca.equals(COMTO_PROVEEDOR)){
					idBitacora = "ACAP";	
				}else if(servletInvoca.equals(COPAGOS)){
					idBitacora = "ACPP";	
				}else if(servletInvoca.equals(PDREGISTRO)){
					idBitacora = "ACPD";
				}else if(servletInvoca.contains(MDI_ORDEN_PAGO_OCURRE)){
					String personalidad = request.getParameter("personalidad");
					EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> obtieneIdBitacora ---- > flujo MDI_Orden_Pago_Ocurre" + personalidad, EIGlobal.NivelLog.INFO);
					idBitacora = "1".equals(personalidad) ? "ACOF" : "2".equals(personalidad) ? "ACOM" : "";
					servletInvoca = "2".equals(personalidad) ? servletInvoca.concat("Mor") : servletInvoca;	
				}else if(servletInvoca.equals(CHESREGISTRO)){
					idBitacora = "ACCH";	
				}else if(servletInvoca.equals(MMC_ALTA)){
					idBitacora = "ACAC";	
				}else if(NOM_PREDISPERSION.equals(servletInvoca) || "NomPreImportNomina".equals(servletInvoca)){
					idBitacora = "ACPI";	
				}else if(servletInvoca.equals(INICIO_NOMINA)|| servletInvoca.equals(IMPORT_NOMINA)){
					idBitacora = "ACPN";	
				}else if(servletInvoca.equals(INICIO_NOMINAINTER)|| "ImportNominaInter".equals(servletInvoca)){
					idBitacora = "ACNI";	
				}else if(servletInvoca.equals(INICIO_NOMINALN)|| servletInvoca.equals(IMPORT_NOMINALN)){
					idBitacora = "ACNL";	
				}
				
				return idBitacora;
			
			}

	/**
	 * Metodo de contro de archivo invalido
	 * @param request : request
	 * @param response : response
	 * @param requestWrapper : CertificadoDigitalWrapper request de la peticion
	 * @throws ServletException : exception
	 * @throws IOException : exception
	 */
	public void controlArchivoInvalido(HttpServletRequest request, ServletResponse response, CertificadoDigitalWrapper requestWrapper)
			throws ServletException, IOException {
			
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> controlArchivoInvalido" , EIGlobal.NivelLog.INFO);
			
				HttpServletResponse resp = (HttpServletResponse)response;
			
				String servletInvocaRuta = request.getServletPath();
				String [] arreglo = servletInvocaRuta.split("\\/");
				String servletInvoca = arreglo[2].trim(), parametros = "";
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ruta invoca "+servletInvocaRuta + "--> SERV "+servletInvoca , EIGlobal.NivelLog.INFO);
				if(servletInvoca.equals(NOMINTERREG)) {
					parametros = "?Modulo=0&flujo=ESEC";
				} else if(servletInvoca.equals(TRANSFERENCIA)) {
					String flujoTranferencia = request.getSession().getAttribute(SERVLET_CIFRADO) != null 
						? request.getSession().getAttribute(SERVLET_CIFRADO).toString() : ""; 
					
					String id = request.getParameter("cifrado");
					parametros = "2".equals(id) ? "?ventana=0&flujo=ETML" : "1".equals(id) ? "?ventana=4&flujo=ETMD" : flujoTranferencia.contains("transferenciaInt") ? "?ventana=3&flujo=ETTP" : "";
					
				}else if("MDI_Interbancario".equals(servletInvoca)){
					parametros = "?Modulo=0&flujo=ETIN";
				}else if(servletInvoca.equals(ANOM_MTTOEMP) || servletInvoca.equals(NOM_EMPIMPORTA)){
					servletInvocaRuta="/enlaceMig/ANomMttoEmp";
					parametros = "?page=1&flujo=ESEM";
				}else if(servletInvoca.equals( NOM_PREALTATARJ)){
					parametros = "?opcion=1&flujo=ESAA";	
				}else if(servletInvoca.equals(PREVALIDADOR_SERVLET)){
					parametros = "?opcion=1&flujo=ESEC";
			
				}else if(servletInvoca.contains(CATALOGO_NOMINA_EMPL)){
					String opcion = request.getParameter(OPCION);
					EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> controlArchivoInvalido ---- > flujo CatalogoNominaEmpl" + opcion, EIGlobal.NivelLog.INFO);
					if("6".equals(opcion) || "7".equals(opcion) ) {
						servletInvoca = servletInvoca.concat("Baja");
						parametros = "?opcion=6&flujo=ESEC";
					} else if("9".equals(opcion) || "10".equals(opcion)) {
						servletInvoca = servletInvoca.concat("Mod");
						parametros = "?opcion=9&flujo=ESEC";
					}			
				} else{
					String [] respuesta = obtenerServletRedireccion(servletInvoca, request);
					servletInvocaRuta = respuesta[0];
					parametros = respuesta[1];
					
				}
					
				EIGlobal.mensajePorTrace(">>>>>>>>>>> Entra filtro: --> ruta re direccion por error:::" +  request.getContextPath() +servletInvocaRuta + parametros, EIGlobal.NivelLog.INFO);
				request.getSession().removeAttribute(SERVLET_CIFRADO);
				request.getSession().setAttribute(MENSAJE_ERRROR, true);
				requestWrapper.getSession().setAttribute(MENSAJE_ERRROR, true);
				resp.sendRedirect(request.getContextPath() +servletInvocaRuta + parametros);		
			
			}

	/**
	 * Metodo de contro de archivo invalido
	 * @param servletInvoca : servletInvoca 
	 * @param request : request
	 * @return String []
	 */
	public String [] obtenerServletRedireccion(String servletInvoca, HttpServletRequest request) {
		
		String servletInvocaRuta = request.getServletPath(), parametros = "";
		String [] respuesta = new String [2];
		if(servletInvoca.equals(ASIGNACION_SERV)){
			parametros = "?opcion=iniciarArchivo&flujo=ESEC";
		}else if(servletInvoca.equals(COMTO_PROVEEDOR)){
			parametros = "?opcion=1&flujo=ESPT";	
		}else if(servletInvoca.equals(COPAGOS)){
			parametros = "?opcion=0&flujo=ESPP";	
		}else if(servletInvoca.equals(PDREGISTRO)){
			parametros = "?txtOpcion=13&flujo=ESPA";
	
		}else if(servletInvoca.contains(MDI_ORDEN_PAGO_OCURRE)){
			String personalidad = request.getParameter("personalidad");
			if("1".equals(personalidad)) {
				servletInvoca = servletInvoca.concat("Fis");
				parametros = "?personalidad=1&opcion=0&flujo=ETOF";
			} else if("2".equals(personalidad)) {
				servletInvoca = servletInvoca.concat("Mor");
				parametros = "?personalidad=2&opcion=0&flujo=ETOM";
			}			
		}else if(servletInvoca.equals(CHESREGISTRO)){
			parametros = "?operacion=inicio&flujo=ESCA";	
		}else if(servletInvoca.equals(MMC_ALTA)){
			parametros = "?Modulo=0&flujo=EACA";	
		}else if(NOM_PREDISPERSION.equals(servletInvoca) || "NomPreImportNomina".equals(servletInvoca)){
			//servletInvocaRuta="/enlaceMig/NomPredispersion";
			servletInvocaRuta=Global.SERVLET_INVOCA_NOMPRE;
			parametros = "?Modulo=7&flujo=ESAN";	
		}else if(servletInvoca.equals(INICIO_NOMINA)|| servletInvoca.equals(IMPORT_NOMINA)){
		//	servletInvocaRuta="/enlaceMig/InicioNomina";
			servletInvocaRuta=Global.SERVLET_INVOCA_NOMINALN;
			parametros = "?Modulo=0&flujo=LNPI&submodulo=2";	
		}else if(INICIO_NOMINAINTER.equals(servletInvoca)|| "ImportNominaInter".equals(servletInvoca)){
			//servletInvocaRuta="/enlaceMig/InicioNominaInter";
			servletInvocaRuta=Global.SERVLET_INVOCA_NOMINAINTER;
			parametros = "?flujo=ESNI";	
		}else if(servletInvoca.equals(INICIO_NOMINALN)|| servletInvoca.equals(IMPORT_NOMINALN)){
			//servletInvocaRuta="/enlaceMig/InicioNominaLn";
			servletInvocaRuta=Global.SERVLET_INVOCA_NOMINALN;
			parametros = "?Modulo=0&flujo=LNPI&submodulo=2";	
		}
		respuesta[0] = servletInvocaRuta;
		respuesta[1] = parametros;
		
		return respuesta;
	}

}