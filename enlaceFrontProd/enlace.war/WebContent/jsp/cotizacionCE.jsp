<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>

<%@ page import="mx.altec.enlace.bo.FormatosMoneda" %>
<%@ page import="java.util.Date" %>
<%@page import="mx.altec.enlace.bo.CotizacionCE"%>
<%--
   23/10/2002 Se cambiaron par�metros por atributos.
   22/10/2002 Se verificaron las funciones javascript.
   14/11/2002 Se hicieron unos cambios de dise�o y se cambi� el despliegue de la cantidad.
   15/11/2002 Cambio bot�n.
   18/11/2002 Cambio imagen.
   19/11/2002 Tama�o imagen enviar (Enviar/Transferir).
   22/11/2002 Se agreg� la l�nea de cr�dito en la pantalla.
--%>

<%!CotizacionCE cce;%>
<%--!Date hoy=new Date();% -->
<%--!Fechas f01=new Fechas(); --%>
<%!FormatosMoneda fm=new FormatosMoneda();%>
<%! String cuenta; %>
<%! String descripcion; %>
<%! String lineaCredito; %>
<%! String parametros; %>



<html>
<head>
<Meta name="versionServlet" content="2.2.3">
<title>Cotizaci&oacute;n de cr&eacute;dito electr&oacute;nico.</title>

<!-- Autor: Hugo Ruiz Zepeda -->

  <script language="JavaScript1.2" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>

  <script language="JavaScript1.2" type="text/javascript">

var envia=true;

function confirmacion(monto, linea, cuenta)
{

  if(envia)
  {
    cuadroDialogo('\xBF'+"Desea efectuar la disposici"+'\xF3'+"n por $"+monto+" de la l"+'\xED'+"nea de cr"+'\xE9'+"dito "+linea+", perteneciente a la cuenta "+cuenta+"?", 2);
  }
} // Fin confirmacion


function continua()
{
  if(envia)
  {
    if(respuesta==1)
    {
      document.cotizado.submit();
    }
    else
    {
      return false;
    }
  }

  return false;
} // Fin continuaEnvio

function enviar()
{
  document.cotizado.submit();
}
</script>

<script language="JavaScript" type="text/javascript">
function MM_preloadImages() { //v3.0

	var d=document;

	if(d.images){

		if(!d.MM_p)

			d.MM_p=new Array();

		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}

	}

}

function MM_swapImgRestore() { //v3.0

	var i,x,a=document.MM_sr;

	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_findObj(n, d) { //v3.0

	var p,i,x;

	if(!d) d=document;

	if((p=n.indexOf("?"))>0&&parent.frames.length) {

	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}


function MM_swapImage() { //v3.0

var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

 	<%= (String)request.getAttribute("newMenu") %>
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>
<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%= (String)request.getAttribute("MenuPrincipal") %>
       </TD>
  </TR>
</TABLE>

<%= (String)request.getAttribute("Encabezado") %>

<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>


<% cce=(CotizacionCE)request.getAttribute("cotizacion"); %>

<%
    /*
    cuenta=request.getParameter("cuenta");
    descripcion=request.getParameter("descripcion");
    lineaCredito=request.getParameter("lineaCredito");
    */

    cuenta=(String)request.getAttribute("cuenta");
    descripcion=(String)request.getAttribute("descripcion");
    lineaCredito=(String)request.getAttribute("lineaCredito");

    if(cuenta==null)
      cuenta="";

    if(descripcion==null)
      descripcion="";

    if(lineaCredito==null)
      lineaCredito="";
%>

<BR>

<TABLE  width="760" border="0" cellspacing="0" cellpadding="0">
 <TR>
  <TD align="center">

      <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	<tr>
		<td align="center" class="tittabdat">
			Cuenta
		</td>
	</tr>
          	<TR bgcolor="#CCCCCC">
            		<td align="center" class="textabdatcla">
              			<%=cce.getCuenta() %>
                  </TD>
            </TR>
            <TR bgcolor="#CCCCCC">
		<td align="center" class="textabdatobs">
			<%=cce.getDescripcion() %>
		</TD>
	</TR>
        </table>

   <BR>

   <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
      <TR>
         <td align="center" class="tittabdat">
            L&iacute;nea de cr&eacute;dito
         </td>
      </TR>
      <TR bgcolor="#CCCCCC">
         <td align="center" class="textabdatcla">
            <%=cce.getLineaCredito() %>
         </TD>
      </TR>
   </Table>

 </TD>
</TR>



<TR>
 <TD align="center">

<BR>

<TABLE width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla" bgcolor="#EBEBEB">
<TR>
<TD align="center" width="170" class="textabdatobs">Fecha de disposici&oacute;n:</TD>
<TD align="center" width="170" class="textabdatobs"> <%=cce.getFechaAplicacion() %> </TD>
</TR><TR bgcolor="#EBEBEB">
<TD align="center" width="170" class="textabdatcla">Fecha de vencimiento:</TD>
<TD align="center" width="170" class="textabdatcla"> <%=cce.getFechaVencimiento() %> </TD>
</TR><TR bgcolor="#CCCCCC">
<TD align="center" width="170" class="textabdatobs">Plazo:</TD>
<TD align="center" width="170" class="textabdatobs"> <%=cce.getPlazo() %>&nbsp;d&iacute;as</TD>
</TR><TR  bgcolor="#EBEBEB">
<TD align="center" width="170" class="textabdatcla">Tasa:</TD>
<TD align="center" width="170" class="textabdatcla"> <%=cce.getTasa() %>&nbsp;&#37;</TD>
</TR><TR bgcolor="#CCCCCC">
<TD align="center" width="170" class="textabdatobs">Importe:</TD>
<TD align="center" width="170" class="textabdatobs">$<% out.println(fm.daMonedaConComas(cce.getMonto(), false)); %> </TD>
</TR><TR  bgcolor="#EBEBEB">
<TD align="center" width="170" class="textabdatcla">Intereses:</TD>
<TD align="center" width="170" class="textabdatcla">$<%=cce.getInteres() %> </TD>
</TR><TR bgcolor="#CCCCCC">
<TD align="center" width="170" class="textabdatobs">Total:</TD>
<TD align="center" width="170" class="textabdatobs">$<%=cce.getImporteTotal() %> </TD>
</TR>
<TR>
<TD colspan="2"  class="tabmovtex11">
<BR><BR><BR>
<P align="justify">
Aceptamos que la disposici&oacute;n de cr&eacute;dito que hemos solicitado al amparo del contrato de cr&eacute;dito que tenemos celebrado, se verifique en los t&eacute;rminos y bajo las condiciones que aqu&iacute; aparecen.
</P>

<TR >
 <TD align="center" colspan="2">
   <BR>

<% parametros="ceVista?pantalla=3&cuenta="+cce.getCuenta()+"&lineaCredito="+cce.getLineaCredito()+"&monto="+cce.getMonto()+"&plazo="+ cce.getPlazo()+"&tasa="+ cce.getTasa()+"&concepto="+cce.getConcepto()+"&interes="+cce.getInteres()+"&fechaVencimiento="+cce.getFechaVencimiento()+"&importeTotal="+cce.getImporteTotal(); %>

<Form name="cotizado" action="ceVista" method="post">
  <Input type="hidden" name="pantalla" value="3">
  <Input type="hidden" name="cuenta" value=<% out.print("\""+cce.getCuenta()+"\""); %> >
  <Input type="hidden" name="descripcion" value=<% out.print("\""+cce.getDescripcion()+"\""); %> >
  <Input type="hidden" name="lineaCredito" value=<% out.print("\""+cce.getLineaCredito()+"\""); %> >
  <Input type="hidden" name="monto" value=<% out.print("\""+cce.getMonto()+"\""); %> >
  <Input type="hidden" name="plazo" value=<% out.print("\""+cce.getPlazo()+"\""); %> >
  <Input type="hidden" name="tasa" value=<% out.print("\""+cce.getTasa()+"\""); %> >
  <Input type="hidden" name="concepto" value=<% out.print("\""+cce.getConcepto()+"\""); %> >
  <Input type="hidden" name="interes" value=<% out.print("\""+cce.getInteres()+"\""); %> >
  <Input type="hidden" name="fechaVencimiento" value=<% out.print("\""+cce.getFechaVencimiento()+"\""); %> >
  <Input type="hidden" name="importeTotal" value=<% out.print("\""+cce.getImporteTotal()+"\""); %> >
</Form>


<TABLE   width="200" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
  <TR>
    <TD align="right" width="90">
    <!--
   <A href=<%=parametros %> border="0">
   -->
   <A href="javascript:confirmacion('<%=cce.getMonto()%>', '<%=cce.getLineaCredito()%>', '<%=cce.getCuenta()%>');"  border="0">
   <img src="/gifs/EnlaceMig/gbo25281.gif" width="110" height="22" hspace="0" vspace="0" border="0" align="Middle" alt="Enviar">
   </A>
 </TD>
 <TD align="left" width="90">
   <A href="ceVista?pantalla=0" border="0">
    <img src="/gifs/EnlaceMig/gbo25190.gif" width="90" hspace="0" vspace="0" border="0" align="Middle" alt="Cancelar">
   </A>
  </TD>
 </TR>
</TABLE>

<!--
 </TD>
</TR>
-->

</TD>
</TR>
</TABLE>


  </TD>
 </TR>
</TABLE>




</body>
</html>
