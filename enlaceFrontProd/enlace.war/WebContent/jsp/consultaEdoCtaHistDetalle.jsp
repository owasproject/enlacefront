<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@page import="mx.altec.enlace.beans.ConsEdoCtaDetalleHistBean"%>
<jsp:useBean id="edoCtaListHistDetalle" class="java.util.ArrayList" scope="request" />

<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>

  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
  <script type="text/javascript">

    var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
    var dia;
    var mes;
    var anio;
    var fechaseleccionada;
    var opcioncaledario;
    
    
    function js_regresarEdoCta()
	{
		document.FrmConEdoCtaHistDet.action="ConsultaHistEdoCtaServlet";
		document.FrmConEdoCtaHistDet.opcion.value="0";
		document.FrmConEdoCtaHistDet.submit();
	}
	
	function anteriores()
    {
    	var pag = document.FrmConEdoCtaHistDet.numPagDet.value;
    	pag--;
		document.FrmConEdoCtaHistDet.action="ConsultaHistEdoCtaServlet";
		document.FrmConEdoCtaHistDet.opcion.value="2";
		document.FrmConEdoCtaHistDet.numPagDet.value = pag;
		document.FrmConEdoCtaHistDet.archivo.value = "";
		document.FrmConEdoCtaHistDet.submit();
	}
	
	function siguientes()
    {
    	var pag = document.FrmConEdoCtaHistDet.numPagDet.value;
    	pag++;
		document.FrmConEdoCtaHistDet.action="ConsultaHistEdoCtaServlet";
		document.FrmConEdoCtaHistDet.opcion.value="2";
		document.FrmConEdoCtaHistDet.numPagDet.value = pag;
		document.FrmConEdoCtaHistDet.archivo.value = "";
		document.FrmConEdoCtaHistDet.submit();
	}
	
	function Exportar(){
		document.FrmConEdoCtaHistDet.action="ConsultaHistEdoCtaServlet";
		document.FrmConEdoCtaHistDet.opcion.value="2";
		document.FrmConEdoCtaHistDet.archivo.value = "exp";
		document.FrmConEdoCtaHistDet.submit();
	}
	
    
    function Actualiza()
    {
        if (opcioncalendario==1)
        document.FrmConEdoCtaHistDet.fecha1.value=fechaseleccionada;
        else if (opcioncalendario==2)
        document.FrmConEdoCtaHistDet.fecha2.value=fechaseleccionada;
    }
    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
    function PresentarCuentas()
    {
        msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
        msg.focus();
    }
    function actualizacuenta()
    {
        document.FrmConEdoCtaHistDet.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
        document.FrmConEdoCtaHistDet.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
    }
    function EnfSelCta()
    {
        if( document.FrmConEdoCtaHistDet.EnlaceCuenta.value.length<=0 )
            document.FrmConEdoCtaHistDet.textEnlaceCuenta.value="";
    }
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    function ctaPorParametro()
    {
        var miTram = "<% if( request.getAttribute("EnlaceCuenta")!=null) out.print(request.getAttribute("EnlaceCuenta")); else out.print(""); %>";
        if( miTram!="" )
        {
            var numCta = "";
            var ctaDes = "";
            var tipRel = "";
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            numCta = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            ctaDes = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            tipRel = miTram.substring( 0,miTram.indexOf("|") );
            document.FrmConEdoCtaHistDet.EnlaceCuenta.value     = numCta +"@"+ tipRel +"@"+ ctaDes +"@";
            document.FrmConEdoCtaHistDet.textEnlaceCuenta.value = numCta +" "+ ctaDes;
        }//fin if
    }
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>

  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="top">
    <td width="*">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
<%
   // if( request.getAttribute("Movfechas")!=null )
   //     out.println(request.getAttribute("Movfechas"));
%>
   </form>
  <form name="FrmConEdoCtaHistDet" method="post">
  <input type="hidden" name="opcion" value=""/>
  <input type="hidden" name="numPagDet" value="<%=request.getAttribute("numPagDet")%>"/>
  <input type="hidden" name="totalPaginas" value="<%=request.getAttribute("totalPaginas")%>"/>
  <input type="hidden" name="idEdoCta" value="<%=request.getAttribute("idEdoCta")%>"/>
  <input type="hidden" name="archivo"  value=""/>
  <input type="hidden" name="folio" value="<%=request.getAttribute("folioC")%>"/>
  <input type="hidden" name="usuarioSol" value="<%=request.getAttribute("usuarioSolC")%>"/>
  <input type="hidden" name="fechaIni" value="<%=request.getAttribute("fechaIniC")%>"/>
  <input type="hidden" name="fechaFin" value="<%=request.getAttribute("fechaFinC")%>"/>
  <input type="hidden" name="estatus" value="<%=request.getAttribute("estatusC")%>"/>

   <tr>
    <td align="center">
     <table width="800" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat"  align="center">Folio de Solicitud</td>
       <td class="tittabdat"  align="center">Estatus</td>
       <td class="tittabdat"  align="center">N&uacute;mero de Cuenta / Tarjeta</td>
       <td class="tittabdat"  align="center">Periodo</td>
       <td class="tittabdat"  align="center">Descripci&oacute;n de Cuenta / Tarjeta</td>
       <td class="tittabdat"  align="center">Secuencia de Domicilio de la Cuenta</td>
       <td class="tittabdat"  align="center">Fecha de solicitud</td>
       <td class="tittabdat"  align="center">Usuario solicitante</td>
      </tr>
      
      <%
      	if(edoCtaListHistDetalle != null && edoCtaListHistDetalle.size()>0){
      	
      		String estilo ="";
      		Integer pagIni = (Integer)request.getAttribute("numPagDet");
      		Integer pagFin = (Integer)request.getAttribute("totalPaginas");
      	
      		for(int i=0;i<edoCtaListHistDetalle.size();i++){
      			ConsEdoCtaDetalleHistBean result = (ConsEdoCtaDetalleHistBean)edoCtaListHistDetalle.get(i);
      			
      			if(i%2==0){
      				estilo = "textabdatobs";
      %>
			      <tr  bgcolor="#CCCCCC">
      <%		}else{ 
      				estilo = "textabdatcla";
      %>
			      <tr bgcolor="#EBEBEB">
      <%		} %>
		
		<td class="<%=estilo %>" align="center"><%=result.getIdEdoCtaHist()%></td>
	    <td class="<%=estilo %>" align="center"><%=result.getEstatus()%></td>
	    <td class="<%=estilo %>" align="center"><%=result.getNumCuenta()%></td>
	    <td class="<%=estilo %>" align="center"><%=result.getPeriodo()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getDescCta()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getNumSec()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getFchSol()%></td>
      	<td class="<%=estilo %>" align="center"><%=result.getIdUsuario()%></td>
      </tr>

      <%
      		}
       %>

     </table>
    </td>
   </tr>
   <tr>
    <td align="center">
   		<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr> 
	 			
   				<%if(pagIni.intValue() >1){%>
   
	  				<td><a href="javascript:anteriores();">Anteriores</a>&nbsp;</td>
	  			<%}
	  			
	  			if(pagFin.intValue()!= pagIni.intValue()){
	  			%>
	  				<td>&nbsp;<a href="javascript:siguientes();">Siguientes</a></td>
	  			
	  			<%} %>
	 		</tr>
		</table>
	</td>
   </tr>
   <tr>
   	<td>
   		&nbsp;
   	</td>
   </tr>
   <tr>
    <td align="center">
    	<table align="center" border="0" cellspacing="0" cellpadding="0">
	 		<tr>  
	 			<td><a href = "javascript:js_regresarEdoCta();"><img src = "/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar"/></a></td>
	 			
       			<td><a href = "javascript:Exportar()" > <img border="0" name="imageField" src="/gifs/EnlaceMig/gbo25230.gif" border="0" alt="Exportar" height="22" width="85"/></a></td>

	 		</tr>
		</table>
	</td>
   </tr>
   <%} %>
  </form>
  </table>
 </body>
	<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	</head>
</html>