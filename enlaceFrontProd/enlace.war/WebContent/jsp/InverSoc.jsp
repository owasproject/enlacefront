
<html>
 <head>
  <title> Fondos de Inversi&oacute;n</title>
  <!--
    Elaborado:  Francisco Serrato Jimenez (fsj)
    Aplicativo: Enlace Internet
    Proyecto:   MX-2002-257
                Getronics CP Mexico
    Archivo:   InverSoc.jsp
  -->
  <script language="JavaScript" src="/EnlaceMig/fw_menu.js"></script>
  <script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
  <script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script language="JavaScript">
   <!--
    <!-- *********************************************** -->
    <!-- modificación para integración pva 07/03/2002    -->
    <!-- *********************************************** -->
    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
    function PresentarCuentas()
    {
      msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
      msg.focus();
    }
    function actualizacuenta()
    {
      document.frmInverSoc2.cboCuenta.value=ctaselec+"|"+ctadescr+"|"+ctatipre+"|"+ctatipro+"|"+cfm;
      document.frmInverSoc2.textcboCuenta.value=ctaselec+" "+ctadescr;
    }
    /*************************************************************************************/
    /*
     6 N Requerido Numerico
     5 T Requerido Texto
     3 S Requerido Seleccion
     2 n Numerico
     1 t Texto
     0 i Ignorado
    */
    errores = new Array("el Contrato","oculto");
    evalua = "TX";
    function ValidaForma(forma)
    {
        if( ValidaTodo(forma,evalua,errores) )
        {
            forma.ventana.value=1;
            forma.submit();
        }
    }
<%
    if (request.getAttribute("newMenu")!= null)
        out.println(request.getAttribute("newMenu"));
%>
    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    {   //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
   //-->
  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
  <table width="571" border="0" cellspacing="0" cellpadding="0">
   <tr valign=top>
    <td width="*">
<%
    if (request.getAttribute("MenuPrincipal")!= null)
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td width="676" valign="top" align="left">
     <table width="666" border="0" cellspacing="6" cellpadding="0">
      <tr>
       <td width="528" valign="top" class="titpag">
<%
    if (request.getAttribute("Encabezado")!= null)
     out.println(request.getAttribute("Encabezado"));
%>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td align="center">
     <form  name="frmInverSoc2" method=post onSubmit="return ValidaForma(this);" action="InverSoc">
      <table border="0" cellspacing="2" cellpadding="3" width="250">
       <tr>
        <td class="tittabdat">Seleccione Contrato</td>
       </tr>
       <tr align="center" valign="middle">
        <td class="textabdatcla" align="left" valign="top">
         <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
           <td class="tabmovtex11">Contrato:</td>
          </tr>
          <tr>
           <td class="tabmovtex">
            <input type="text" name=textcboCuenta  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
            <A href="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
            <input type="hidden" name="cboCuenta" value="">
           </td>
          </tr>
         </table>
        </td>
       </tr>
      </table><br>
      <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" width="133" height="22">
       <tr>
        <td align="center" width="87">
         <a href="javascript:ValidaForma(document.frmInverSoc2);"><img border="0" name="Consultar" src="/gifs/EnlaceMig/gbo25570.gif" width="133" height="22" alt="Consultar fondos"></a>
        </td>
       </tr>
      </table><br>
      <input type=hidden name=ventana value=0>
     </form>
    </td>
   </tr>
  </table>
 </body>
</html>