<%@page contentType="text/html"%>
<%--Pagina para desplegar el resultado de la consulta de Mancomunidad--%>


<jsp:useBean id="ResultadosMancomunidadInter" scope="session" class="java.util.ArrayList"/>
<jsp:useBean id="CuentaConsulta" scope="session" class="java.lang.String"/>
<%--
<jsp:useBean id="SeleccionaTodos" scope="session" class="java.lang.Boolean"/>
<jsp:useBean id="IndexMancomunidadInter" scope="session" class="java.lang.Integer"/>
--%>

<%
	Boolean SeleccionaTodos = (Boolean) session.getAttribute("SeleccionaTodos");
	Integer IndexMancomunidadInter = (Integer) session.getAttribute("IndexMancomunidadInter");
	System.out.println ( "Asignacion IndexMancomunidadInter " );
	System.out.println ( IndexMancomunidadInter.intValue());
 %>

<%!
    public static final String ANTERIORES = "<a href='javascript:anteriores ();'>Anteriores&nbsp;";
    public static final String SIGUIENTES = "<a href='javascript:siguientes ();'>Siguientes&nbsp;";
%>

<html>
<head>
  <title>Banca Virtual</title>
  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
  <script language='JavaScript' src='/EnlaceMig/cuadroDialogo.js'></script>
  <script language='JavaScript' src='/EnlaceMig/scrImpresion.js'></script>
  <script language='JavaScript1.2' src='/EnlaceMig/fw_menu.js'></script>
  <script language='JavaScript'>

    <%-- Inicio Java Script App --%>
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }

    function MM_findObj(n, d) { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    <%=session.getAttribute ("newMenu")%>
    <%-- Fin Java Script App --%>

    function CheckAll()
	{
        for(i = 0;i < document.FrmAutoriza.elements.length; i++)
        if(document.FrmAutoriza.elements[i].type == "checkbox")
           document.FrmAutoriza.elements[i].checked = document.FrmAutoriza.todos.checked;
        return true;
    }

    function anteriores ()
	{
        document.FrmAutoriza.action = "GetAMancomunidadInter";
        document.FrmAutoriza.OpcPag.value = "<%=mx.altec.enlace.servlets.GetAMancomunidadInter.PREV%>";
        document.FrmAutoriza.submit ();
    }

    function siguientes ()
	{
        document.FrmAutoriza.action = "GetAMancomunidadInter";
        document.FrmAutoriza.submit ();
    }

    function autoriza (Opcion)
	{
		document.FrmAutoriza.TipoOp.value = Opcion;
        if (Opcion == "1")
			document.FrmAutoriza.Autoriza.value = "1";
        if (validaFolios ()) document.FrmAutoriza.submit ();
    }

    function validaFolios ()
	{
        var total = document.FrmAutoriza.elements.length;
        var j = 0;
        for (i = 0; i < total; i++)
		{
            var e = document.FrmAutoriza.elements[i];
            if (e.type == 'checkbox' && e.name != 'todos')
			{
                if (e.checked) {
                    j++;
                }
            }
        }
        if (j == 0)
		{
            cuadroDialogo ("Usted no ha seleccionado ninguna operaci&oacute;n.\n\nPor favor seleccione la(s) operacion(es) \npendiente(s) a modificar.",1);
            return false;
        }
        return true;
    }

  </script>
  <link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
</head>
<body topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' bgcolor='#ffffff' onload='MM_preloadImages("/gifs/EnlaceMig/gbo25131.gif","/gifs/EnlaceMig/gbo25111.gif","/gifs/EnlaceMig/gbo25151.gif","/gifs/EnlaceMig/gbo25031.gif","/gifs/EnlaceMig/gbo25032.gif","/gifs/EnlaceMig/gbo25051.gif","/gifs/EnlaceMig/gbo25052.gif","/gifs/EnlaceMig/gbo25091.gif","/gifs/EnlaceMig/gbo25092.gif","/gifs/EnlaceMig/gbo25012.gif","/gifs/EnlaceMig/gbo25071.gif","/gifs/EnlaceMig/gbo25072.gif","/gifs/EnlaceMig/gbo25011.gif")' background='/gifs/EnlaceMig/gfo25010.gif'>

<form onsubmit='' method='POST' action='OperacionesManc' name='FrmAutoriza'>

<table border='0' cellpadding='0' cellspacing='0' width='571'>
    <tr valign='top'>
      <td width='*'>
        <%=session.getAttribute ("MenuPrincipal")%>
      </td>
    </tr>
  </table>
  <%=session.getAttribute ("Encabezado")%>


<!-- Tipo de cambios vcl -->
<table align="center">
	<tr>
		<td>
			<table width="530" border="0" cellspacing="2" cellpadding="3" align="center">
          <tr>
            <td class="tittabdat" colspan="4"> Tipos de Cambios </td>
          </tr>
          <tr>
            <td class="textabdatcla" colspan="4">Compra</td>
          </tr>
          <tr>
            <td height="29" class="textabdatcla">Ventanilla USD:</td>
			<td class="textabdatcla">
				<input type="text" name="tcc_usd" onFocus='blur();' class="textabdatcla" value='<%=session.getAttribute("tcc_usd")%>'>
			</td>
            <td class="textabdatcla">Ventanilla EURO:</td>
			<td class="textabdatcla">
              <input type="text" name="tcc_euro" onFocus='blur();' class="textabdatcla" value='<%=session.getAttribute("tcc_euro")%>'>
            </td>
          </tr>
          <tr>
            <td class="textabdatcla" colspan="4">Venta</td>
          </tr>
          <tr>
            <td class="textabdatcla">Ventanilla USD:</td>
			<td class="textabdatcla">
              <input type="text" name="tcv_usd" onFocus='blur();' class="textabdatcla" value='<%=session.getAttribute("tcv_usd")%>'>
			</td>
            <td class="textabdatcla">Ventanilla EURO:</td>
			<td class="textabdatcla">
              <input type="text" name="tcv_euro" onFocus='blur();' class="textabdatcla" value='<%=session.getAttribute("tcv_euro")%>'>
			</td>
          </tr>
        </table>
		</td>
	</tr>
</table>
<br>
<!-- Fin de tipo de cambios vcl -->

  <br>

    <input type='hidden' name='OpcPag' value='<%=mx.altec.enlace.servlets.GetAMancomunidadInter.NEXT%>'>
    <input type='hidden' name='Opcion' value='1'>
    <input type='hidden' name='Autoriza' value='2'>
	<input type='hidden' name='TipoOp' id="TipoOp" value=''>
    <table width='100%' border='0' cellspacing='2' cellpadding='3' class='tabfonbla'>
      <%
      if (CuentaConsulta != null && !CuentaConsulta.equals ("")) {
      %>
      <tr>
        <td align='right' colspan='12'>Cuenta&nbsp;:&nbsp;<%=CuentaConsulta%></td>
      </tr>
      <%}%>
      <tr>
        <td colspan='12' class='texenccon'>Total de registros&nbsp;:&nbsp;<%=ResultadosMancomunidadInter.size ()%></td>
      </tr>
      <tr>
        <td colspan='12' class='texenccon'>Periodo del&nbsp;<%=session.getAttribute("fechaIni")%>
		&nbsp;al&nbsp;<%=session.getAttribute ("fechaFin")%></td>
      </tr>
      <tr>
        <td width="54" align='right' class='tittabdat'>
          Todos<%if(SeleccionaTodos.booleanValue ()) {%><input type='checkbox' name='todos' onclick='CheckAll ()'><%}%>
        </td>
        <td width="64" align='center' class='tittabdat'>
          Fecha Registro.
        </td>
        <td width="37" align='center' class='tittabdat'>
          Folio Reg.
        </td>
        <td width="41" align='center' class='tittabdat'>
          Tipo Oper.
        </td>
        <td width="45" align='center' class='tittabdat'>
          Fecha Auto.
        </td>
        <td width="39" align='center' class='tittabdat'>
          Folio Auto.
        </td>
        <td width="64" align='center' class='tittabdat'>
          Usuario Registr&oacute;
        </td>
        <td width="64" align='center' class='tittabdat'>
          Usuario Autoriz&oacute;
        </td>
        <td width="41" align='center' class='tittabdat'>
          Cta. cargo
        </td>
        <td width="44" align='center' class='tittabdat'>
          Cta. abono
        </td>
		<td width="68" align='center' class='tittabdat'>
          Beneficiario
        </td>
		<td width="35" align='center' class='tittabdat'>
          Divisa
        </td>
		<td width="60" align='center' class='tittabdat'>
          Importe MN
        </td>
        <td width="47" align='center' class='tittabdat'>
          Importe USD
        </td>
		<td width="58" align='center' class='tittabdat'>
          Importe Divisa
        </td>
		<td width="53" align='center' class='tittabdat'>
          Tipo Cambio
        </td>
        <td width="48" align='center' class='tittabdat'>
          Estado
        </td>
      </tr>
      <%
        int paginacion = 30;
        int index = IndexMancomunidadInter.intValue ();
        int siguiente = index + paginacion;
        int restantes = (ResultadosMancomunidadInter.size () - (index + 30)) > paginacion ? paginacion : ResultadosMancomunidadInter.size () - (index + 30);
        System.out.println ("Indice: " + index);
        System.out.println ("Restantes: " + restantes);
        java.util.ListIterator liResultados = ResultadosMancomunidadInter.listIterator (index);
        while (liResultados.hasNext () && index < siguiente)
		{
            Object o = liResultados.next ();
            try
			{
	 %>
      <%=((mx.altec.enlace.bo.DatosMancInter) o).getHTMLFormat (index)%>
      <%
            } catch (Exception ex) {
				System.out.println ("****ENTRO A UN A EXCEPTION****: " );
                ex.printStackTrace ();
                break;
            }
            index++;
        }
      %>
      <tr>
        <td colspan='13' align='right' class='texenccon'>
          Operaciones Mancomunadas Internacionales&nbsp;:&nbsp;<%=IndexMancomunidadInter.intValue () + 1%>&nbsp;-&nbsp;<%=((IndexMancomunidadInter.intValue () + paginacion) >= ResultadosMancomunidadInter.size ()) ? ResultadosMancomunidadInter.size () : IndexMancomunidadInter.intValue () + paginacion%>&nbsp;de&nbsp;<%=ResultadosMancomunidadInter.size ()%>
        </td>
      </tr>
    </table>

 <!-- inicio tabla nueva -->
 <table width="100%" border="0">
  <tr>
      <td align="center">
	  <%=IndexMancomunidadInter.intValue () == 0 ? "" : ANTERIORES + paginacion + "</a>&nbsp;"%><%=liResultados.hasNext () ? SIGUIENTES + restantes + "</a>" : ""%>
	  <br>
      </td>
  </tr>
  <tr >
      <td align="center">
	    <br>
	    <a href='javascript:autoriza (1);'> <img border='0' src='/gifs/EnlaceMig/gbo25430.gif' width='92' height='22' alt='Autorizar'/></a>
        <a href='javascript:autoriza (2);'> <img border='0' src='/gifs/EnlaceMig/gbo25190.gif' width='85' height='22' alt='Cancelar'/></a>
        <a href='javascript:scrImpresion ();'> <img border='0' src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' alt='Imprimir'> </a>
        <a href='<%=session.getAttribute ("ArchivoManc")%>'> <img border='0' src='/gifs/EnlaceMig/gbo25230.gif' width='85' height='22' alt='Exportar'></a>
		<a href='javascript:history.back ();'> <img border='0' src='/gifs/EnlaceMig/gbo25320.gif' width='83' height='22' alt='Regresar'> </a>
	  </td>
  </tr>
  <tr>
      <td align="center"><br>Ahora puede realizar el envio de las Transferencias.<br>
        <i>Este proceso puede tardar <font color=red>varios minutos</font>, por
        favor espere ...</i> </td>
  </tr>
</table>

	<!-- fin tabla nueva -->
  </form>
</body>
</html>