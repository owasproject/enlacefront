<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.EIGlobal" %>
<%!
	   private void writeMenu(javax.servlet.jsp.JspWriter out, String[] values,
	    String[] nameValues, String selectedValue){
		  try{
		  		for(int i=0; i<values.length; i++){
		  			if(values[i].trim().equals(selectedValue)){
		  				out.println("<option selected value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}else{
		  				out.println("<option value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}
		  		}
	  	  }catch(java.io.IOException e1){
	  	  	EIGlobal.mensajePorTrace("Error en pinta menu->" + e1.getMessage(),EIGlobal.NivelLog.ERROR);
	  	  }
	   }
%>

<%
	String tituloPantalla = "";
	String tituloHTML = "";
	String boton1 = "";
	String boton2 = "";
	String altBoton1 = "";
	String altBoton2 = "";
	String gbo25421 = "gbo25421.gif";
	String altGbo25421 = "";

	String parTipoSoc = (String) request.getAttribute("TipoSoc");
	String parPais = (String) request.getAttribute("Paises");
	String parMedio = (String) request.getAttribute("MedioInfo");
	String parForma = (String) request.getAttribute("FormaPago");
	String parBanco = (String) request.getAttribute("Bancos");
	String parPlaza = (String) request.getAttribute("Plazas");
	String parEstado = (String) request.getAttribute("Estados");
	String parAccion = (String) request.getAttribute("accion");
	String[] cveEstado = (String[])request.getAttribute("cveEstado");
	String[] descEstado = (String[])request.getAttribute("descEstado");
	String[] cvePais = (String[])request.getAttribute("cvePais");
	String[] descPais = (String[])request.getAttribute("descPais");
	String estadoSel = "";
	String cvePaisSel = "";
	String datosAdicReadOnly = "";

	if (parTipoSoc == null)
		parTipoSoc = "";
	if (parPais == null)
		parPais = "";
	if (parMedio == null)
		parMedio = "";
	if (parForma == null)
		parForma = "";
	if (parBanco == null)
		parBanco = "";
	if (parPlaza == null)
		parPlaza = "";
	if (parEstado == null)
		parEstado = "";
	if (parAccion == null) {
		parAccion = request.getParameter("accion");
		if (parAccion == null)
			parAccion = "1";
	}

	boolean nuevo = false;

	if (Integer.parseInt(parAccion) > 2 && !parAccion.equals("11")) {
		nuevo = true;
		parAccion = "" + (Integer.parseInt(parAccion) - 2);
	}


	ProveedorConf prov = (ProveedorConf) request
			.getAttribute("Proveedor"); // <---------------------------------
	if (prov == null) {
		prov = new ProveedorConf();
		prov.tipo = ((String)request.getParameter("tipoPersona") == null) ? prov.FISICA :
			(((String) request.getParameter("tipoPersona")).equals("F") ? prov.FISICA : prov.MORAL);
		//jgarcia - Es proveedor Nacional o Internacional
		prov.origenProveedor = (request.getParameter("origenProveedor") != null
			&& prov.INTERNACIONAL.equalsIgnoreCase((String)request.getParameter("origenProveedor"))?
				prov.INTERNACIONAL:prov.NACIONAL);
		if (!nuevo) {
			prov.claveProveedor = request.getParameter("vClave");
			prov.codigoCliente = request.getParameter("vCodigo");
			prov.noTransmision = request.getParameter("vSecuencia");
			prov.nombre = request.getParameter("vNombre");
			prov.apellidoPaterno = request.getParameter("vPaterno");
			prov.apellidoMaterno = request.getParameter("vMaterno");
			prov.tipoSociedad = request.getParameter("vTipoSoc");
			prov.rfc = request.getParameter("vRFC");
			prov.homoclave = request.getParameter("vHomoclave");
			prov.contactoEmpresa = request.getParameter("vContacto");
			prov.clienteFactoraje = request.getParameter("vFactoraje");
			prov.calleNumero = request.getParameter("vCalle");
			prov.colonia = request.getParameter("vColonia");
			prov.cdPoblacion = request.getParameter("vCiudad");
			prov.cp = request.getParameter("vCodigoPostal");
			// jgarcia 29/Sept/2009
	        // Si es proveedor Internacional el medio de informacion debe ser e-mail
	        // y para el proveedor Nacional no existe el campo vDatosAdic.
	        // En el caso del proveedor internacional en el campo de delegacion
	        // se guarda el estado.
        	prov.medioInformacion = "1";
			prov.datosAdic = request.getParameter("vDatosAdic");
        	prov.delegMunicipio = "";

			prov.pais = request.getParameter("vPais");
			prov.email = request.getParameter("vEmail");
			prov.ladaPrefijo = request.getParameter("vLada");
			prov.numeroTelefonico = request.getParameter("vTelefonico");
			prov.extensionTel = request.getParameter("vExTelefono");
			prov.fax = request.getParameter("vFax");
			prov.extensionFax = request.getParameter("vExFax");
			prov.formaPago = request.getParameter("vForma");
			prov.cuentaCLABE = request.getParameter("vCuenta");
			prov.banco = request.getParameter("vBanco");
			prov.sucursal = request.getParameter("vSucursal");
			prov.plaza = request.getParameter("vPlaza");
			prov.deudoresActivos = request.getParameter("vDeudores");
			prov.limiteFinanciamiento = request.getParameter("vLimite");
			prov.volumenAnualVentas = request.getParameter("vVolumen");
			prov.importeMedioFacturasEmitidas = request.getParameter("vImporte");
			prov.cveDivisa = request.getParameter("vCveDivisa");
			prov.cvePais = request.getParameter("vCvePais");
			prov.cveABA = request.getParameter("vCveABA");
			prov.tipoAbono = request.getParameter("vTipoAbono");

			if(request.getParameter("vEstado")!=null &&
  			   !request.getParameter("vEstado").trim().equals("0")){
  				prov.estado = request.getParameter("vEstado");
  			}else{
	  			prov.estado = "";
  			}			

			if(request.getParameter("vCtaBancoDestino") == null)
				prov.ctaBancoDestino = "";
			else
				prov.ctaBancoDestino = request.getParameter("vCtaBancoDestino");

			if(request.getParameter("vDescripcionBanco") != null &&
			  !request.getParameter("vDescripcionBanco").trim().equals("") &&
			  !request.getParameter("vDescripcionBanco").equals("Banco Extranjero")){
				prov.descripcionBanco = request.getParameter("vDescripcionBanco");
			}else{
				prov.descripcionBanco = "Banco Extranjero";
			}
			
			if(request.getParameter("vDescripcionCiudad") != null &&
			  !request.getParameter("vDescripcionCiudad").trim().equals("") &&			
			  !request.getParameter("vDescripcionCiudad").equals("Ciudad")){
				prov.descripcionCiudad = request.getParameter("vDescripcionCiudad");
			}else{
				prov.descripcionCiudad = "Ciudad";
			}	
			

			if(prov.cvePais!=null &&
			   prov.cvePais.trim().equals("USA")){
				datosAdicReadOnly = "READONLY";
				//document.formulario.vDatosAdic.readOnly = true;			
			}	

									
			prov.quitaNulos();
		}else{
			prov.descripcionCiudad = "Ciudad";
			prov.descripcionBanco = "Banco Extranjero";
		}
	}

	if(prov.estado!=null){
		estadoSel = prov.estado;
	}

	if(prov.cvePais!=null){
		cvePaisSel = prov.cvePais;
	}

	// Alta de Proveedores en Linea
	if (parAccion.equals("1")) {
		boton1 = "gbo25480.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "alta";
		altBoton2 = "limpiar";
		tituloPantalla = "Alta";
		tituloHTML = "Alta de Proveedor";
	}
	// Modificacion de Proveedor
	else if (parAccion.equals("2")) {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "modificar";
		altBoton2 = "limpiar";
		tituloPantalla = "Modificaci&oacute;n";
		tituloHTML = "Modificaci&oacute;n de Proveedor";
	}
	// Consulta y baja de proveedor
	else {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25500.gif";
		altBoton1 = "modificar";
		altBoton2 = "baja";
		tituloPantalla = "Consulta";
		tituloHTML = "Consulta y modificaci&oacute;n de proveedor";
	}
%>
<script type="text/javascript">
// Valida la selecion de tipo de sociedad --------------------------------------------------------
// Para proveedores Internacionales no es un campo obligatorio.
function valida_vSociedad(){
	return true;
}
// Valida el RFC ---------------------------------------------------------------------------------
function valida_vRFC(){
	var valorCampo = document.formulario.vRFC.value;
	var Indice;
	var formato = "";

	if(valorCampo.length == 0){
		document.formulario.vRFC.select();
		document.formulario.vRFC.focus();
		MensajeErrorLongitudNula("el ID Contribuyente/Num. Seg. Social");
		return false;
	}else{
		formato = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&]{1,20}$/;
	}
	if(formato != "") {
		if(formato.test(valorCampo)) {
			return true;
		}
	}
	document.formulario.vRFC.select();
	document.formulario.vRFC.focus();
	cuadroDialogo("El campo ID Contribuyente contiene caracteres no validos",3);
	return false;
}

// Valida la Homoclave --------------------------------------------------------------------------
//En este momento no existe la Homoclave para Proveedores Internacionales
function valida_vHomoclave(){
	return true;
}

//	Valida Colonia --------------------------------------------------------------------------
//En este momento no existe Colonia para Proveedores Internacionales
function valida_vColonia(){
	return true;
}

//	Valida Delegacion o Municipio --------------------------------------------------------------------------
//En este momento no existe Delegacion o Municipio para Proveedores Internacionales
function valida_vDelegacion(){
	return true;
}

// Valida Codigo Postal -------------------------------------------------------------------
function valida_vCodigoPostal(){
	var valorCampo = document.formulario.vCodigoPostal.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0 || valorCampo.length > 12) {
		document.formulario.vCodigoPostal.select();
		document.formulario.vCodigoPostal.focus();
		MensajeErrorLongitudNula("el C&oacute;digo Postal");
	}else { ValorRegreso = true;}
	return ValorRegreso;
}

// Valida selecion del estado -------------------------------------------------------------
function valida_vEstado(){
	var valorCampo = document.formulario.vEstado.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0){
		document.formulario.vEstado.select();
		document.formulario.vEstado.focus();
		MensajeErrorLongitudNula("el Estado");
	}else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vEstado.select();
		document.formulario.vEstado.focus();
		MensajeErrorCaracteresInvalidos("Estado");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la seleccion de medio de comunicacion -------------------------------------------
//En este momento el medio de comunicaci�n para proveedores internacionales ser� solamente el e-mail
function valida_vMedio(){
	var ValorRegreso = false;
	if(valida_vEmail()){
		ValorRegreso = true;
	}
	return ValorRegreso;
}

// Valida el Telefono ---------------------------------------------------------------------
function valida_vTelefono(){
	var valorCampo = document.formulario.vTelefonico.value;
	var valorCampo2 = document.formulario.vLada.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorLongitudNula("el Tel&eacute;fono");
	}else if(!EsNumerica(valorCampo)) {
		document.formulario.vTelefonico.select();
		document.formulario.vTelefonico.focus();
		MensajeErrorTipoDeDato("Tel&eacute;fono","Num&eacute;rico");
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida el Clave BIC ---------------------------------------------------------------------
function valida_vClaveBIC(){
	var valorCampo = document.formulario.vDatosAdic.value;
	var ValorRegreso = false;

	if(!EsAlfaNumerica(valorCampo)) {
		document.formulario.vDatosAdic.select();
		document.formulario.vDatosAdic.focus();
		cuadroDialogo("EL campo clave BIC debe ser Num&eacute;rico",3);
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida seleccion de la forma de pago ----------------------------------------------------
function valida_vForma(){
	var retorno = true;
	if(!valida_vClaveBIC() || !valida_vPaisForma() || !valida_vCtaBanco() ||
	   !valida_ABA() || !valida_vDescripcionBanco() || !valida_vDescripcionCiudad())
	   retorno = false;
	return retorno;
}

function ajustaPais(){
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	if(trim(valorCvePais) == "USA"){
		document.formulario.vCveABA.disabled = false;
		document.formulario.vDescripcionBanco.readOnly = true;
		document.formulario.vDescripcionCiudad.readOnly = true;
		document.formulario.vDatosAdic.readOnly = true;
	}else{
		document.formulario.vCveABA.disabled = true;
		document.formulario.vDescripcionBanco.readOnly = false
		document.formulario.vDescripcionCiudad.readOnly = false;
		document.formulario.vDatosAdic.readOnly = false;		
		document.formulario.vCveABA.value = "";
	}
}

function valida_vPaisForma(){
	var valorForma = document.formulario.vForma.value;
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	var ValorRegreso = true;
	if(trim(valorCvePais) == "0"){
		document.formulario.vCvePais.focus();
		cuadroDialogo("El Pa&iacute;s es obligatorio para forma de pago Internacional.",3);
		ValorRegreso = false;
	}else if(trim(valorCvePais) == "MEXI" && trim(valorForma) == "5"){
		document.formulario.vCvePais.focus();
		cuadroDialogo("El Pa&iacute;s Mexico no aplica para forma de pago Internacional.",3);
		ValorRegreso = false;
	}
	return ValorRegreso;
}

function valida_vDescripcionBanco() {
	var valorCampo = document.formulario.vDescripcionBanco.value;
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	var ValorRegreso = true;

	if(trim(valorCampo) == "Banco Extranjero" || trim(valorCampo) =="") {
		document.formulario.vDescripcionBanco.select();
		document.formulario.vDescripcionBanco.focus();
		cuadroDialogo("El banco destino es obligatorio",3);
		ValorRegreso = false;
	}
	return ValorRegreso;
}

function valida_vCtaBanco(){
 		var cuentaDestino = document.formulario.vCuenta.value;
		var ValorRegreso = true;

		if(cuentaDestino==""){
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino es obligatoria",3);
			ValorRegreso = false;
		}else if(!EsAlfaNumerica(cuentaDestino)){
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino contiene caracteres no validos.",3);
			ValorRegreso = false;
		}
		return ValorRegreso;
}

// Valida selecion de la clave ABA  ---------------------------------------------------
function valida_ABA(){
	var aba = document.formulario.vCveABA.value;
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	var ValorRegreso = true;
	if(trim(valorCvePais) == "USA"){
		if (aba == ""){
			cuadroDialogo("La Clave ABA es obligatoria",3);
			ValorRegreso = false;
		}else if(aba.length!=9){
			cuadroDialogo("La Clave ABA es incorrecta",3);
			ValorRegreso = false;
		}
	}
	return ValorRegreso;
}

// La divisa para Proveedores Internacionales por default es USD
function valida_vCveDivisa(){
	return true;
}

function TraerClavesABA(){
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	if(trim(valorCvePais) == "USA"){
		if(document.formulario.vDescripcionBanco.value!="" && 
		   document.formulario.vDescripcionBanco.value!="Banco Extranjero"){
			var Banco=document.formulario.vDescripcionBanco.value;
			Banco=escape(Banco);
			ventanaInfo2=window.open('EI_Bancos?BancoTxt='+Banco+"&Clave=1",'Bancos','width=250,height=350,toolbar=no,scrollbars=no');
			ventanaInfo2.focus();
			<%if((String)request.getAttribute("CiudadABA")!=null){ %>
				document.formulario.vDescripcionCiudad.value =  <%=(String) request.getAttribute("CiudadABA") %>;
			<%}else{%>
				document.formulario.vDescripcionCiudad.value = "";
			<%}%>
		}else{
	       cuadroDialogo("Seleccione un Banco.",1);
		}
	}else{
		cuadroDialogo("Solo aplica para cuentas en Estados Unidos.",3);
	}
}

function valida_vDescripcionCiudad() {
	var valorCampo = trim(document.formulario.vDescripcionCiudad.value);
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	var ValorRegreso = true;

	if(trim(valorCampo) == "Ciudad" || trim(valorCampo) == "") {
		document.formulario.vDescripcionCiudad.select();
		document.formulario.vDescripcionCiudad.focus();
		cuadroDialogo("La Plaza/Ciudad es obligatoria",3);
		ValorRegreso = false;
	}
	return ValorRegreso;
}

function TraerBancos(){
	var valorCvePais =  document.formulario.vCvePais.options[document.formulario.vCvePais.selectedIndex].value;
	if(trim(valorCvePais) == "USA"){
		ventanaInfo2=window.open('EI_Bancos','Banco','width=280,height=350,toolbar=no,scrollbars=no');
		ventanaInfo2=window.open('/Download/EI_Bancos.html','Banco','width=250,height=350,toolbar=no,scrollbars=no');
		ventanaInfo2.focus();
	}else{
		cuadroDialogo("Solo aplica para cuentas en Estados Unidos.",3);
	}
}

function quitaValorInicial(campo) {
	switch(campo) {
		case 1:
			if (document.formulario.vDescripcionBanco.value == "Banco Extranjero") {
				document.formulario.vDescripcionBanco.value = "";
			}
			break;
		case 2:
			if (document.formulario.vDescripcionCiudad.value == "Ciudad") {
				document.formulario.vDescripcionCiudad.value = "";
			}
			break;
	}
}

</script>
<table>
			<tr>
				<td class="tittabdat" colspan="2"><b> Capture los datos del proveedor </td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
					<table width="650" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tabmovtexbol" nowrap><b> * Clave de Proveedor</td>
								<td class="tabmovtexbol" nowrap><b> C&oacute;digo del cliente</td>
								<td class="tabmovtexbol" nowrap><b> No. Transmisi&oacute;n </td>
							</tr>
							<tr>
								<td class="tabmovtexbol" nowrap><input type="TEXT"
									name="vClave" value="<%= prov.claveProveedor %>" maxlength="20"
									onchange="this.value=this.value.toUpperCase(); return true;">
								</td>
								<td class="tabmovtexbol" nowrap>
									<input type="TEXT" name="vCodigo" value="<%= prov.codigoCliente %>"
									onFocus='blur();'></td>
								<td class="tabmovtexbol" nowrap><input type="text"
									name="vSecuencia" value="<%= prov.noTransmision %>"
									onFocus='blur();'></td>
							</tr>
							<tr>
								<td colspan="3">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<%
										if (prov.tipo == prov.FISICA) {
										%>
											<td class="tabmovtexbol" nowrap width="255"><b> * Nombre </td>
											<td class="tabmovtexbol" nowrap><b> * Apellido Paterno </td>
											<td class="tabmovtexbol" nowrap><b> Apellido Materno </td>
										<%
										} else {
										%>
										<td class="tabmovtexbol" nowrap width="255"><b> * Razon Social </td>
										<td class="tabmovtexbol" nowrap><b> Tipo de sociedad </td>
										<td></td>
										<%
										}
										%>
									</tr>
								</table>
							</tr>
							<tr>
								<td colspan="3">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<%
											if (prov.tipo == prov.FISICA) {
											%>
											<td class="tabmovtexbol" nowrap width="255">
												<input type="TEXT"	name="vNombre" value="<%= prov.nombre %>" maxlength="80"
												onchange="this.value=this.value.toUpperCase(); return true;">
											</td>
											<td class="tabmovtexbol" nowrap>
												<input type="TEXT" name="vPaterno" value="<%= prov.apellidoPaterno %>"
													maxlength="30"	onchange="this.value=this.value.toUpperCase(); return true;">
											</td>
											<td class="tabmovtexbol" nowrap>
												<input type="TEXT"	name="vMaterno" value="<%= prov.apellidoMaterno %>"
													maxlength="30"	onchange="this.value=this.value.toUpperCase(); return true;">
											</td>
											<%
											} else {
											%>
											<td class="tabmovtexbol" nowrap width="255">
												<input type="TEXT" name="vNombre" value="<%= prov.nombre %>" maxlength="80"
												onchange="this.value=this.value.toUpperCase(); return true;">
											</td>
											<td></td>
											<td class="tabmovtexbol" nowrap >
												<input type="TEXT" name="vTipoSoc"  value="<%= prov.tipoSociedad%>"
												maxlength="60" onchange="this.value=this.value.toUpperCase(); return true;">
											</td>
											<%
											}
											%>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="tabmovtexbol" nowrap><b id="TextContribuyente"> * ID Contribuyente/Num. Fiscal </td>
								<td class="tabmovtexbol" nowrap><b> Contacto en la empresa: </td>
							</tr>
							<tr>
								<td class="tabmovtexbol" nowrap>
									<input type="TEXT" name="vRFC" value="<%= prov.rfc %>"
									maxlength="20" onchange="this.value=this.value.toUpperCase(); return true;">
								</td>
								<td class="tabmovtexbol" nowrap>
									<input type="TEXT" name="vContacto" value="<%= prov.contactoEmpresa %>"
									maxlength="80" onchange="this.value=this.value.toUpperCase(); return true;">
								</td>
							</tr>
							<tr>
								<td class="tabmovtexbol" nowrap>No. Cliente para Factoraje:</td>
							</tr>
							<tr>
								<td class="tabmovtexbol" nowrap>
									<input type="TEXT" name="vFactoraje" value="<%= prov.clienteFactoraje %>"
									maxlength="8" onchange="this.value=this.value.toUpperCase(); return true;">
								</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2"><b> Domicilio</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="650" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Calle y n&uacute;mero</td>
						<td class="tabmovtexbol" nowrap><b> * Ciudad </td>
						<td class="tabmovtexbol" nowrap><b> * Estado/Provincia/Regi&oacute;n</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><input type="Text"
							name="vCalle" value="<%= prov.calleNumero %>" maxlength="60"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap><input type="Text"
							name="vCiudad" value="<%= prov.cdPoblacion %>" maxlength="35"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap><input type="TEXT"
							name="vEstado" value="<%= prov.estado %>"
							maxlength="35"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Codigo Postal(ZIP) </td>
						<td class="tabmovtexbol" nowrap><b> * Pa&iacute;s </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><input type="TEXT"
							name="vCodigoPostal" value="<%= prov.cp %>" maxlength="12"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<select name="vPais">
								<option value="0"></option>
								<%=request.getAttribute("Paises")%>
							</select>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2">
					<b> Medio de confirmaci&oacute;n
				</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="650" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Direcci&oacute;n de E-mail  </td>
						<td class="tabmovtexbol" nowrap><b> * Lada o prefijo  </td>
						<td class="tabmovtexbol" nowrap><b> * N&uacute;mero telef&oacute;nico  </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="Text" name="vEmail" value="<%= prov.email %>" maxlength="60"></td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vLada" value="<%= prov.ladaPrefijo %>" maxlength="12">
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vTelefonico" value="<%= prov.numeroTelefonico %>"
							maxlength="12" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Extensi&oacute;n Tel.</td>
						<td class="tabmovtexbol" nowrap><b> Fax</td>
						<td class="tabmovtexbol" nowrap><b> Extensi&oacute;n Fax</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vExTelefono" value="<%= prov.extensionTel %>" maxlength="12"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vFax" value="<%= prov.fax %>" maxlength="12"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vExFax" value="<%= prov.extensionFax %>" maxlength="12"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2"><b> Forma de pago</td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
				<table width="650" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="tabmovtexbol" nowrap><b> * Forma de pago </td>
						<td class="tabmovtexbol" nowrap><b> *Cuenta Destino / Cta. IBAN: </td>
						<td class="tabmovtexbol" nowrap><b> *Divisa: </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap colspan=1>
							<select name="vForma">
								<option CLASS='tabmovtex' value='<%=ProveedorConf.FORMA_PAGO_CVE_INT %>'><%=ProveedorConf.FORMA_PAGO_DESC_INT %></option>
							</select>
						</td>
						<td class="tabmovtexbol" nowrap><input type="TEXT"
							name="vCuenta" value="<%= prov.cuentaCLABE %>" maxlength="34"
							onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCveDiv" value="USD" maxlength="4"
							size="5" readonly="readonly">
							<select name="vCveDivisa">
								<option CLASS='tabmovtex' value='USD'>Dolar Americano</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> *Pa&iacute;s:</td>
						<td class="tabmovtexbol" nowrap><b> Clave ABA (USA):</td>
						<td class="tabmovtexbol" nowrap><b> *Banco Destino:</td>

					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<select name="vCvePais" onChange="ajustaPais();">
								<option value="0"></option>
							    <% writeMenu(out, cvePais,
							      				  descPais,
							    				  cvePaisSel.trim());
							    %>
							</select>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vCveABA" value="<%= prov.cveABA %>" <%= !"USA".equalsIgnoreCase(cvePaisSel.trim())?" disabled='disabled' ":" "%>
							maxlength="9" onChange="valida_vCveABA();">
							<a name="lupaABA" href="javascript:TraerClavesABA();" border="0">
							<img src="/gifs/EnlaceMig/<%= gbo25421 %>" border=0></a>
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vDescripcionBanco" value="<%= prov.descripcionBanco %>"
							maxlength="40" onchange="this.value=this.value.toUpperCase(); return true;" onFocus="quitaValorInicial(1)">
							<a href="javascript:TraerBancos();" border=0>
							<img src="/gifs/EnlaceMig/<%= gbo25421 %>" border=0></a>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> *Plaza/Ciudad: </td>
						<td class="tabmovtexbol" rowspan="2" colspan="2"><b> Clave BIC (Comunidad Europea): </td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vDescripcionCiudad" value="<%= prov.descripcionCiudad %>"
							maxlength="40" onchange="this.value=this.value.toUpperCase(); return true;" onFocus="quitaValorInicial(2)">
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> *Tipo de Abono:</td>
						<td class="tabmovtexbol" nowrap colspan="2">
							<input type="TEXT" name="vDatosAdic" value="<%= prov.datosAdic.trim() %>" <%=datosAdicReadOnly%>
							maxlength="11" onchange="this.value=this.value.toUpperCase(); return true;"></td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
						<select name="vTipoAbono">
							<option CLASS='tabmovtex' value="0"></option>
							<option CLASS='tabmovtex' selected value="01">Global por Vencimiento</option>
							<option CLASS='tabmovtex' value="02">Individual por Documento</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Deudores activos:</td>
						<td class="tabmovtexbol" nowrap><b> L&iacute;mite de financiamiento:</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input TYPE="TEXT" NAME="vDeudores" value="<%= prov.deudoresActivos %>"
							maxlength="5" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vLimite" value="<%= prov.limiteFinanciamiento %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
						</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap><b> Volumen anual de ventas</td>
						<td class="tabmovtexbol" nowrap><b> Importe medio de facturas emitidas</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vVolumen" value="<%= prov.volumenAnualVentas %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
						<td class="tabmovtexbol" nowrap>
							<input type="TEXT" name="vImporte" value="<%= prov.importeMedioFacturasEmitidas %>"
							maxlength="22" onchange="this.value=this.value.toUpperCase(); return true;">
						</td>
					</tr>
				</table>
				</td>
			</tr>
	</table>