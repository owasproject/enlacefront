<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!-- JavaScript del APP -->
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<SCRIPT LANGUAGE="JavaScript">

function ValidaCuentas(forma)
{
   var cont=0;
   var str="";
   forma.OPERACIONES_A_CANCELAR.value = "";

   for(i2=0;i2<forma.length;i2++)
    {
      if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='todosTRAN' )
       {
         if(forma.elements[i2].checked==true)
          {
		   forma.OPERACIONES_A_CANCELAR.value+=forma.elements[i2].name;
           str+="1";
           cont++;
          }
         else
          str+="0";
       }
    }
   if(cont==0)
    {
      cuadroDialogo("Debe seleccionar m&#237;nimo una operaci&#243;n.",1);
      //return false;
    }
	else {
		document.getElementById("ventana").value = "2";
		forma.submit();
	}
   //return true;
 }


function mandar_cancelar()
{
  document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value="";
  document.CONSULTAPROGRAMADAS.CONTADOR.value=0;

  for (i=0;i<document.CONSULTAPROGRAMADAS.elements.length;i++)
  {
     if (document.CONSULTAPROGRAMADAS.elements[i].type=="checkbox")
     {
        if (document.CONSULTAPROGRAMADAS.elements[i].checked==true)
        {
           document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value=document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value+document.CONSULTAPROGRAMADAS.elements[i].value+"@";
           document.CONSULTAPROGRAMADAS.CONTADOR.value=eval(document.CONSULTAPROGRAMADAS.CONTADOR.value)+1;
        }
     }
  }
  document.CONSULTAPROGRAMADAS.submit();
}


function mandar_cancelar_todas()
{
  document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value="";
  document.CONSULTAPROGRAMADAS.CONTADOR.value=0;

  for (i=0;i<document.CONSULTAPROGRAMADAS.elements.length;i++)
  {
     if (document.CONSULTAPROGRAMADAS.elements[i].type=="checkbox")
     {
       document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value=document.CONSULTAPROGRAMADAS.OPERACIONES_A_CANCELAR.value+document.CONSULTAPROGRAMADAS.elements[i].value+"@";
       document.CONSULTAPROGRAMADAS.CONTADOR.value=eval(document.CONSULTAPROGRAMADAS.CONTADOR.value)+1;
     }
  }
  if (confirm (" Cancelar Todas? ")==true)
    document.CONSULTAPROGRAMADAS.submit();
  else
    return false;
}
/*
function mandar_archivo()
{
   document.nombre_archivo.action="programada?ventana=1";
   document.CONSULTAPROGRAMADAS.submit();
}
*/

function mandar_archivo()
{
   if (document.CONSULTAPROGRAMADAS.nombre_archivo.value!="") window.location="/Download/"+document.CONSULTAPROGRAMADAS.nombre_archivo.value;
   else cuadroDialogo("No existen movimientos a exportar",1);
   ///document.CONSULTAPROGRAMADAS.submit();
}

var Nmuestra = 30;
function atras()
{
  //alert("entrando a funcion atras");
  if((parseInt(document.CONSULTAPROGRAMADAS.prev.value) - Nmuestra)>0)
  {
      document.CONSULTAPROGRAMADAS.next.value = document.CONSULTAPROGRAMADAS.prev.value;
      document.CONSULTAPROGRAMADAS.prev.value = parseInt(document.CONSULTAPROGRAMADAS.prev.value) - Nmuestra;
      document.CONSULTAPROGRAMADAS.action = "programada?ventana=0";
      document.CONSULTAPROGRAMADAS.submit();
  }
}

function adelante(pagina) {


 	var valPaginaActual =
 	document.getElementById("valPaginaActual").value = parseInt(document.getElementById("valPaginaActual").value) + pagina;
 	document.getElementById("valNextPrev").value = pagina;


	consultarOperacion(false);

}

 function CheckAll()
 {
   for(i=0; i<document.CONSULTAPROGRAMADAS.elements.length ;i++)
     if( document.CONSULTAPROGRAMADAS.elements[i].type=="checkbox" && document.CONSULTAPROGRAMADAS.elements[i].id ==="checkboxPAGT")
     	document.CONSULTAPROGRAMADAS.elements[i].checked = document.CONSULTAPROGRAMADAS.todos.checked;
   return true;
 }

 function CheckAllOtrasTablas(valTodos, idCheckbox)
 {
   for(i=0; i<document.CONSULTAPROGRAMADAS.elements.length ;i++)
     if( document.CONSULTAPROGRAMADAS.elements[i].type=="checkbox" && document.CONSULTAPROGRAMADAS.elements[i].id === idCheckbox)
     	document.CONSULTAPROGRAMADAS.elements[i].checked = document.getElementById(valTodos).checked;
   return true;
 }

function consultarOperacion(limpiar) {

	var tipoConsulta = "";
	var elementosRadio = document.getElementsByName("tipoConsulta");
	for(i = 0; i < elementosRadio.length; i++) {
		if(elementosRadio[i].checked === true) {
			tipoConsulta = elementosRadio[i].value;
		}
	}
	if(limpiar === true) {
		document.getElementById("limpiarConsulta").value = "true";
		document.getElementById("valNextPrev").value = "";
	} else {
		document.getElementById("limpiarConsulta").value = "false";
	}
	document.getElementById("ventana").value = "2";
	document.getElementById("idOperacionConsultar").value = tipoConsulta;
	document.CONSULTAPROGRAMADAS.action = "programada";
	document.CONSULTAPROGRAMADAS.submit();
}

function checkComboConsulta() {
	var tipoConsulta = document.getElementById("idOperacionConsultar").value;
	document.getElementById("tipoConsulta"+tipoConsulta).checked = "checked";
}

function exportarArchivo() {
	var isActivo = false;
	var tipoArchivo = "";

	if(document.getElementById("radioTXT").checked) {
		isActivo = true;
		tipoArchivo = "TXT";

	} else if(document.getElementById("radioXLS").checked) {
		isActivo = true;
		tipoArchivo = "XLS";
	}

	if(!isActivo) {
		alert("Debe seleccionar un tipo de archivo para exportar");
		return;
	}

	var tipoOperacion = document.getElementById("idOperacionConsultar").value;
	msg=window.open("programada?ventana=5&tipoArchivo="+tipoArchivo+"&idOperacionConsultar="+tipoOperacion,"Exportacion","location=no,directories=no,status=no,menubar=no,resizable=no,width=-100,height=-100, fullscreen=no");

}

</SCRIPT>
<!-- JavaScript del App -->
<script language="JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%=request.getAttribute("newMenu")%>

//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
</head>

<!-- <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/consultas_on.gif','images/transfer_on.gif','images/inver_on.gif','images/util_on.gif','images/ayuda_on.gif','images/final_on.gif')" background="/gifs/EnlaceMig/gfo25010.gif"> -->

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style="background-color: #FFFFFF; background: /gifs/EnlaceMig/gfo25010.gif"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); checkComboConsulta();">

	<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="571" style="border: 0">
		<tr valign="top">
			<td width="*">
				<!-- MENU PRINCIPAL --> ${MenuPrincipal }
			</td>
		</tr>
	</TABLE>
	${ Encabezado}
	<!-- imagen 09/04/2001-->

	<FORM NAME="CONSULTAPROGRAMADAS" id="CONSULTAPROGRAMADAS" METHOD="POST"
		ACTION="cancprog" onSubmit="return ValidaCuentas(this);">
		<input type="hidden" id="idOperacionConsultar" name="idOperacionConsultar" value="${ idOperacionConsultar}" />
		<input type="hidden" id="ventana" name="ventana" value="${ventana}" />
		<input type="hidden" id="ventana1" name="ventana1" value="${param.ventana}" />
		<input type="hidden" id="limpiarConsulta" name="limpiarConsulta" value="" />
		<input type="hidden" id="valNextPrev" name="valNextPrev" value="${valNextPrev}" />
		<input type="hidden" name="OPERACIONES_A_CANCELAR" id="OPERACIONES_A_CANCELAR"/>

		<div style=" width: 950px; text-align:center;">
		<table style="margin-left: auto; margin-right:auto " >
			<tr>
				<td>
					<div style="text-align:center;">
						<table style="background-color: #FFFFFF; border: 0;  margin: auto;  width: 350px; ">
							<tr>
								<td class="tittabdat" colspan="4"><label> Seleccione el tipo de operacion programada para consultar </label></td>
							</tr>
							<tr>
								<td class="textabdatcla"><label>Transferencias mismo banco: </label></td>
								<td class="textabdatcla"><input type="radio"
									name="tipoConsulta" id="tipoConsultaTRAN" value="TRAN"
									checked="checked" /></td>
								<td class="textabdatcla"><label>Transferencias Interbancarias: </label></td>
								<td class="textabdatcla"><input type="radio"
									name="tipoConsulta" id="tipoConsultaDIBT" value="DIBT" /></td>
							</tr>
							<tr>
								<td class="textabdatcla"><label>Nomina:</label></td>
								<td class="textabdatcla"><input type="radio"
									name="tipoConsulta" id="tipoConsultaNOMI" value="NOMI" /></td>
								<td class="textabdatcla"><label>Pago de tarjeta de credito:</label></td>
								<td class="textabdatcla"><input type="radio"
									name="tipoConsulta" id="tipoConsultaPAGT" value="PAGT" /></td>
							</tr>
							<tr>
								<td class="" colspan="4" align="center"><br/><a
									href="javascript:consultarOperacion(true);"> <img
										style="border: 0" width="83" height="22" alt="Consultar"
										name="Consultar" src="/gifs/EnlaceMig/gbo25220.gif" />
								</a></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<c:choose>
				<c:when test="${not empty listaConsulta}">
					<tr>
						<td>
							<input type="hidden" id="valInicio" name="valInicio" value="${valInicio }" />
							<input type="hidden" id="valFin" name="valFin" value="${valFin}" />
							<input type="hidden" id="valPaginaActual" name="valPaginaActual" value="${valPaginaActual}" />
							<input type="hidden" id="totalRegitros" name="totalRegitros" value="${totalRegitros}" />

							<table width="900px" style="border: 0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td style="text-align: left;"><c:if
											test="${ idOperacionConsultar eq 'TRAN'}">
											<div style="text-align:center;">
												<table style="background-color: #FFFFFF; border: 0;  margin: auto; ">
													<tbody>
														<tr>
															<fmt:setLocale value="es_MX"/>
															<td style="text-align: left;" class="texenccon" colspan="3"><label>Transferencias mismo Banco</label></td>
															<td style="text-align: right;" class="texenccon" colspan="4"><label>Total de operaciones programadas: ${fn:length(listaConsulta)}
																	&nbsp;por&nbsp;&nbsp;
																	<fmt:formatNumber value="${totalConsulta}" type="currency"/>
																	</label></td>
														</tr>
														<tr>
															<td style="text-align: center;" class="tittabdat"><label>Todos</label><input
																type="checkbox"
																onclick="CheckAllOtrasTablas('todosTRAN', 'checkboxTRAN')"
																name="todosTRAN" id="todosTRAN" /></td>
															<td style="text-align: center;" class="tittabdat"><label>Referencia</label></td>
															<td style="text-align: center;" class="tittabdat"><label>Operaci�n</label></td>
															<td style="text-align: center;" class="tittabdat"><label>Fecha</label></td>
															<td style="text-align: center;" class="tittabdat"><label>Cuenta cargo</label></td>
															<td style="text-align: center;" class="tittabdat"><label>Cuenta abono</label></td>
															<td style="text-align: center;" class="tittabdat"><label>Importe</label></td>
														</tr>
														<c:forEach items="${listaConsulta }" var="varLista"
															begin="${valInicio }" end="${valFin}" varStatus="valIter">
															<c:set var="nameCheckBox" scope="request"
																value="${varLista.referencia}|${varLista.descripcionCuenta}|${varLista.fecha}|${varLista.cuenta1}|
																	${varLista.cuenta2}|${varLista.importe}|OPER_TRAN|@" />
															<c:choose>
																<c:when test="${valIter.index % 2 == 0 }">
																	<c:set var="renglonClass" value="textabdatobs"
																		scope="request" />
																</c:when>
																<c:otherwise>
																	<c:set var="renglonClass" value="textabdatcla"
																		scope="request" />
																</c:otherwise>
															</c:choose>

															<tr>
																<td style="text-align: center;" class="${ renglonClass}"><input
																	type="checkbox" name="${nameCheckBox }"
																	id="checkboxTRAN" /></td>
																<td style="text-align: left;" class="${ renglonClass}">${ varLista.referencia}</td>
																<td style="text-align: center;" class="${ renglonClass}">${ varLista.descripcionCuenta}</td>
																<td style="text-align: center;" class="${ renglonClass}">${ varLista.fecha}</td>
																<td style="text-align: left;" class="${ renglonClass}">${ varLista.cuenta1}</td>
																<td style="text-align: left;" class="${ renglonClass}">${ varLista.cuenta2}</td>
																<!--<td style="text-align: right;" class="${ renglonClass}">$ ${ varLista.importe}</td>-->
																<fmt:setLocale value="es_MX"/>
																<td style="text-align: right;" class="${ renglonClass}"><fmt:formatNumber value="${varLista.importe}" type="currency"/></td>
															</tr>
														</c:forEach>
														<tr style="text-align: center">
															<td colspan="7"><c:if test="${valPaginaActual !=1}">

																	<a href="#" onclick="javascript:adelante(-1);"><label> &#60; Anteriores ${numeroRegXPagina} </label></a>

																</c:if> <c:if test="${fn:length(listaConsulta)-1 > valFin}">

																	<a href="#" onclick="javascript:adelante(1);"><label>
																			Siguientes ${numeroRegXPagina} &#62; </label></a>


																</c:if></td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:if>
										<c:if test="${ idOperacionConsultar eq 'PAGT'}">
										<div style="text-align:center;">
											<table style="background-color: #FFFFFF; border: 0;  margin: auto; ">
												<tbody>
													<tr>
														<fmt:setLocale value="es_MX"/>
														<td style="text-align: left;" class="texenccon" colspan="3"><label>Pago
																Tarjeta de Credito</label></td>
														<td style="text-align: right;" class="texenccon" colspan="4"><label>Total
																de operaciones programadas: ${fn:length(listaConsulta)}
																&nbsp;por&nbsp;&nbsp;<fmt:formatNumber value="${totalConsulta}" type="currency"/></label></td>
													</tr>
													<tr>
														<td style="text-align: center;" class="tittabdat"><label>Todos</label><input
															type="checkbox"
															onclick="CheckAllOtrasTablas('todosTRAN', 'checkboxTRAN')"
															name="todosTRAN" id="todosTRAN" /></td>
														<td style="text-align: center;" class="tittabdat"><label>Cuenta Cargo</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Tarjeta de Abono</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Importe</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Concepto</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Fecha de Aplicacion</label></td>
													</tr>
													<c:forEach items="${listaConsulta }" var="varLista"
														begin="${valInicio }" end="${valFin}" varStatus="valIter">

														<c:set var="nameCheckBox" scope="request"
															value="${varLista.referencia}|${varLista.descripcionCuenta}|${varLista.fecha}|${varLista.cuenta1}|
																${varLista.cuenta2}|${varLista.importe}|OPER_PAGT|@" />

														<c:choose>
															<c:when test="${valIter.index % 2 == 0 }">
																<c:set var="renglonClass" value="textabdatobs"
																	scope="request" />
															</c:when>
															<c:otherwise>
																<c:set var="renglonClass" value="textabdatcla"
																	scope="request" />
															</c:otherwise>
														</c:choose>
														<tr>
															<td style="text-align: center;" class="${ renglonClass}"><input
																type="checkbox" name="${nameCheckBox }"
																id="checkboxTRAN" /></td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.cuenta1}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.cuenta2}</td>
															<!--<td style="text-align: center;" class="${ renglonClass}">$ ${ varLista.importe}</td>-->
															<fmt:setLocale value="es_MX"/>
															<td style="text-align: center;" class="${ renglonClass}"><fmt:formatNumber value="${varLista.importe}" type="currency"/></td>															
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.descripcionCuenta}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.fecha}</td>

														</tr>
													</c:forEach>
													<tr style="text-align: center">
														<td colspan="7"><c:if test="${valPaginaActual !=1}">

																<a href="#" onclick="javascript:adelante(-1);"><label> &#60; Anteriores ${numeroRegXPagina}</label></a>

															</c:if> <c:if test="${fn:length(listaConsulta)-1 > valFin}">

																<a href="#" onclick="javascript:adelante(1);"><label> Siguientes ${numeroRegXPagina} &#62; </label></a>


															</c:if></td>
													</tr>
												</tbody>
											</table>
										</div>
										</c:if>
										<c:if test="${ idOperacionConsultar eq 'NOMI'}">
											<table cellspacing="2" cellpadding="3" width=""
												style="background-color: #FFFFFF; border: 0; margin-left: auto; margin-right:auto ">
												<tbody>
													<tr>
														<fmt:setLocale value="es_MX"/>
														<td style="text-align: left;" class="texenccon" colspan="4"><label>Nomina</label></td>
														<td style="text-align: right;" class="texenccon" colspan="7"><label>Total de operaciones programadas: ${fn:length(listaConsulta)}
																&nbsp;por&nbsp;&nbsp;<fmt:formatNumber value="${totalNomina}" type="currency"/></label></td>
													</tr>
													<tr>
														<td style="text-align: center;" class="tittabdat"><label>Seleccione</label><input
															type="checkbox"
															onclick="CheckAllOtrasTablas('todosTRAN', 'checkboxTRAN')"
															name="todosTRAN" id="todosTRAN" /></td>
														<td style="text-align: center;" class="tittabdat"><label>Fecha recepci�n</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Fecha Cargo</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Fecha Aplicaci�n</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Hora Aplicaci�n</label></td>
														<td style="text-align: left;" class="tittabdat"><label>Cuenta de Cargo</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Nombre del Archivo</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Secuencia</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Num. Registros</label></td>
														<td style="text-align: right;" class="tittabdat"><label>Importe Aplicado</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Estatus</label></td>
													</tr>
													<c:forEach items="${listaConsulta }" var="varLista"
														varStatus="valIter">
														<c:set var="nameCheckBox" scope="request"
															value="${varLista.fechaRecepcion}|${varLista.fechaCargo}|${varLista.fechaAplicacion}|${varLista.horaAplicacion}|
																	${varLista.ctaCargo}|${varLista.nombreArchivo}|${varLista.secuencia}|${varLista.numeroRegistros}|${varLista.importeAplic}|${varLista.estatus}|OPER_NOMI|@" />

														<c:choose>
															<c:when test="${valIter.index % 2 == 0 }">
																<c:set var="renglonClass" value="textabdatobs"
																	scope="request" />
															</c:when>
															<c:otherwise>
																<c:set var="renglonClass" value="textabdatcla"
																	scope="request" />
															</c:otherwise>
														</c:choose>
														<tr>
															<td style="text-align: center;" class="${ renglonClass}"><input
																type="checkbox" name="${nameCheckBox }"
																id="checkboxTRAN" /></td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.fechaRecepcion}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.fechaCargo}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.fechaAplicacion}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.horaAplicacion}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.ctaCargo}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.nombreArchivo}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.secuencia}</td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.numeroRegistros}</td>
															<fmt:setLocale value="es_MX"/>
															<td style="text-align: right;" class="${ renglonClass}">
															<fmt:formatNumber value="${varLista.importeAplic}" type="currency"/></td>
															<!--<td style="text-align: right;" class="${ renglonClass}">$ ${ varLista.importeAplic}</td>-->
															
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.estatus}</td>
														</tr>
													</c:forEach>
													<tr style="text-align: center">
														<td colspan="11" class="tabmovtex11">
															<c:if test="${valPaginaActual !=1}">
																<a href="#" onclick="javascript:adelante(-1);"><label> &#60; Anteriores ${numeroRegXPagina} </label></a>
															</c:if>
															Pagina ${ valPaginaActual} de ${ totalPaginas}

															<c:if test="${valFin < totalRegitros}">
																<a href="#" onclick="javascript:adelante(1);"><label> Siguientes ${numeroRegXPagina} &#62;</label></a>
															</c:if>
														</td>
													</tr>
												</tbody>
											</table>
										</c:if>
										<c:if test="${ idOperacionConsultar eq 'DIBT'}">
											<table cellspacing="2" cellpadding="3" width=""
												style="background-color: #FFFFFF; border: 0; margin-left: auto; margin-right:auto ">
												<tbody>
													<tr>
														<fmt:setLocale value="es_MX"/>
														<td style="text-align: left;" class="texenccon" colspan="5"><label>Transferencia Interbancaria</label></td>
														<fmt:setLocale value="es_MX"/>
														<td style="text-align: right;" class="texenccon" colspan="6"><label>Total de operaciones programadas: ${fn:length(listaConsulta)}
																&nbsp;por&nbsp;&nbsp;<fmt:formatNumber value="${totalConsulta}" type="currency"/></label></td>
													</tr>
													<tr>
														<td style="text-align: center;" class="tittabdat"><label>Todos</label><input
															type="checkbox"
															onclick="CheckAllOtrasTablas('todosTRAN', 'checkboxTRAN')"
															name="todosTRAN" id="todosTRAN" /></td>
														<td style="text-align: center;" class="tittabdat"><label>N�mero de Referencia</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Cuenta Origen</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Cuenta Destino / M�vil</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Beneficiario</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Importe</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Fecha</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Estatus</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Concepto del Pago/Transferencia</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Referencia Interbancaria</label></td>
														<td style="text-align: center;" class="tittabdat"><label>Forma Aplicaci�n</label></td>
													</tr>
													<c:forEach items="${listaConsulta }" var="varLista"
														begin="${valInicio }" end="${valFin}" varStatus="valIter">

														<c:set var="nameCheckBox" scope="request"
															value="${varLista.referencia}|${varLista.descripcionCuenta}|${varLista.fecha}|${varLista.cuenta1}|${varLista.cuenta2}|${varLista.importe}|${ varLista.beneficiario}|${ varLista.fecha}|${ varLista.concepto}|${ varLista.referenciaInterbancaria}|${ varLista.formaAplicaion}|OPER_DIBT|@" />

														<c:choose>
															<c:when test="${valIter.index % 2 == 0 }">
																<c:set var="renglonClass" value="textabdatobs"
																	scope="request" />
															</c:when>
															<c:otherwise>
																<c:set var="renglonClass" value="textabdatcla"
																	scope="request" />
															</c:otherwise>
														</c:choose>
														<tr>
															<td style="text-align: center;" class="${ renglonClass}"><input
																type="checkbox" name="${nameCheckBox }"
																id="checkboxTRAN" /></td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.referencia}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.cuenta1}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.cuenta2}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.beneficiario}</td>
															<fmt:setLocale value="es_MX"/>
															<!--<td style="text-align: right;" width="100%" class="${ renglonClass}">$ ${ varLista.importe}</td>-->
															<td style="text-align: right;" width="100%" class="${ renglonClass}"><fmt:formatNumber value="${varLista.importe}" type="currency"/></td>
															<td style="text-align: center;" class="${ renglonClass}">${ varLista.fecha}</td>
															<td style="text-align: center;" class="${ renglonClass}">Programada</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.concepto}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.referenciaInterbancaria}</td>
															<td style="text-align: left;" class="${ renglonClass}">${ varLista.formaAplicaion}</td>

														</tr>
													</c:forEach>
													<tr style="text-align: center">
														<td colspan="7" class="tabmovtex11"><c:if test="${valPaginaActual !=1}">

																<a href="#" onclick="javascript:adelante(-1);"><label> &#60; Anteriores ${numeroRegXPagina}</label></a>

															</c:if> <c:if test="${fn:length(listaConsulta)-1 > valFin}">

																<a href="#" onclick="javascript:adelante(1);"><label> Siguientes ${numeroRegXPagina} &#62; </label></a>


															</c:if></td>
													</tr>
												</tbody>
											</table>
										</c:if>
										</td>
								</tr>
								<tr>
									<td><br>
									<div style="text-align: center;">
										<table style="width=150px; margin: auto;">
											<tr>
												<td><input type="radio" name="radioExport"
													id="radioTXT"></td>
												<td class="tabmovtex11" ><label>Exporta en TXT</label></td>
											</tr>
											<tr>
												<td><input type="radio" name="radioExport"
													id="radioXLS" checked="checked" ></td>
												<td class="tabmovtex11" ><label>Exporta en XLS</label></td>
											</tr>
										</table>
									</div>
										<br></td>
								</tr>

							</table></td>
					</tr>
					<tr>
						<td align="center">
							<table style="border: 0; width: 760;" align="center">
								<tbody>
									<tr>
										<td>
											<table cellspacing="0" cellpadding="0" border="0" width="160"
												style="background-color: #FFFFFF" align="center">
												<tbody>
													<tr>
														<td width="85" style="text-align: right;"><a
															href="javascript:ValidaCuentas(document.CONSULTAPROGRAMADAS);">
																<img style="border: 0" width="85" height="22"
																alt="Cancelar" name="cancelar"
																src="/gifs/EnlaceMig/gbo25190.gif" />
														</a></td>
														<td width="83" style="text-align: left;"><a
															href="javascript:scrImpresion();"> <img
																style="border: 0" width="83" height="22" alt="Imprimir"
																name="imprimir" src="/gifs/EnlaceMig/gbo25240.gif" />
														</a></td>
														<td width="253" style="text-align: left;"><a style="border: 0"
															href="#" onclick="javascript:exportarArchivo();"> <img
																style="border: 0" width="85" height="22" alt="Exportar"
																name="exportar" src="/gifs/EnlaceMig/gbo25230.gif" />
														</a></td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:if test="${param.ventana != 0}">
						<tr>
							<td class="tittabdat" colspan="2" style="text-align: center">
								<label style="text-align: center">No existen operaci�nes
									programadas de esa consulta en este momento</label>
							</td>
						</tr>
					</c:if>
				</c:otherwise>
			</c:choose>
		</table>
		</div>
	</FORM>
</body>
</html>