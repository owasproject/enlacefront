<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>
<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.*" %>
<%@ page import="mx.altec.enlace.bo.*" %>

<html>
<head>
	<title>TESOFE - Importaci&oacute;n</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

	<%
		boolean facEnvCan=false;
	    boolean facEnvDev=false;

		BaseResource sess =(BaseResource)request.getSession().getAttribute("session");
		facEnvCan=sess.getFacultad("TESOENVCAN");
		facEnvDev=sess.getFacultad("TESOENVDEV");
	%>

 var FAC_ENV_CAN=<%=facEnvCan%>;
 var FAC_ENV_DEV=<%=facEnvDev%>;

	<%
	  String numeroRegistros="0";
	  String importeTotal="0.0";
	  String claveEmisora="0";
	  String codigoOperacion="0";
	  String fechaPresentacion="";
	  String fechaAplicacion="";
	  String tipoArchivo="";

	  if(request.getAttribute("numeroRegistros")!=null)
		  numeroRegistros = (String)request.getAttribute("numeroRegistros");
	  if(request.getAttribute("importeTotal")!=null)
		  importeTotal = (String)request.getAttribute("importeTotal");
	  if(request.getAttribute("claveEmisora")!=null)
		  claveEmisora = (String)request.getAttribute("claveEmisora");
	  if(request.getAttribute("codigoOperacion")!=null)
		  codigoOperacion = (String)request.getAttribute("codigoOperacion");
	  if(request.getAttribute("fechaPresentacion")!=null)
		  fechaPresentacion = (String)request.getAttribute("fechaPresentacion");
	  if(request.getAttribute("fechaAplicacion")!=null)
		  fechaAplicacion = (String)request.getAttribute("fechaAplicacion");
	  if(request.getAttribute("tipoArchivo")!=null)
		  tipoArchivo = (String)request.getAttribute("tipoArchivo");
	%>
 var numeroRegistros="<%=numeroRegistros%>";
 var respuesta=0;

 cuadroDialogo("Se importaron exitosamente: " + numeroRegistros + " registros.",1);

function continua()
{
  if(respuesta==1)
	{
	  document.TesImporta.action="MTE_Importar";
      document.TesImporta.submit();
	}
}

function js_enviar()
{
  var tipoArchivo="<%=tipoArchivo%>";
  if( (tipoArchivo=="Cancelaciones" && FAC_ENV_CAN)
	    ||
      (tipoArchivo=="Devoluciones" && FAC_ENV_DEV)
	)
     cuadroDialogo("Desea enviar el archivo?",2);
  else
    cuadroDialogo("No tiene facultad para enviar archivos de " + tipoArchivo,3);
}

function js_regresarImportacion()
{
  document.TesImporta.action="MTE_Importar";
  document.TesImporta.pagina.value="1";
  document.TesImporta.submit();
}

function js_verDetalle()
{
   /*
   document.TesImporta.action="/NASApp/enlaceMig/jsp/MTE_ImportaDetalle.jsp";
   document.TesImporta.target=myWin;
   document.TesImporta.submit();
   */
   var forma=document.TesImporta;
   var parametros="";

   parametros+="numeroRegistros="+forma.numeroRegistros.value;
   parametros+="&nombreArchivoImp="+forma.nombreArchivoImp.value;
   parametros+="&importeTotal="+forma.importeTotal.value;
   parametros+="&claveEmisora="+forma.claveEmisora.value;
   parametros+="&codigoOperacion="+forma.codigoOperacion.value;
   parametros+="&fechaPresentacion="+forma.fechaPresentacion.value;
   parametros+="&tipoArchivo="+forma.tipoArchivo.value;

   ventanaInfo=window.open('/Enlace/jsp/MTE_ImportaDetalle.jsp?'+parametros,'trainerWindow','width=780,height=400,toolbar=no,scrollbars=yes');
 }

function VentanaAyuda(cad)
 {
   return;
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>

 <FORM   NAME="TesImporta" action="jsp/MTE_ImportaDetalle.jsp" method="post">

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <td align=center>
    <table width="640" border="0" cellspacing="0" cellpadding="0">
	 <tr>
	  <td class="textabref"><%=tipoArchivo%></td>
	 </tr>
	</table>
	<table width="640" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
	 <tr>
	  <td width="80" class="tittabdat" align="center">Clave Emisora</td>
	  <td width="80" class="tittabdat" align="center">Cod. Operacion</td>
	  <td width="80" class="tittabdat" align="center">Fecha de Presentaci&oacute;n</td>
	  <td width="80" class="tittabdat" align="center">Fecha de Aplicaci&oacute;n</td>
	  <td width="80" class="tittabdat" align="center">Importe total</td>
	  <td width="80" class="tittabdat" align="center">N&uacute;mero de registros</td>
	 </tr>
	 <tr>
	  <td class="textabdatobs" nowrap align="center"><%=claveEmisora%></td>
	  <td class="textabdatobs" nowrap align="center"><%=codigoOperacion%></script></td>
	  <td class="textabdatobs" nowrap align="center"><%=fechaPresentacion%></td>
	  <td class="textabdatobs" nowrap align="center"><%=fechaAplicacion%></td>
	  <td class="textabdatobs" nowrap align="center"><%=FormatoMoneda.formateaMoneda(new Double(importeTotal).doubleValue())%></td>
	  <td class="textabdatobs" nowrap align="center"><%=numeroRegistros%></td>
	 </tr>
	 <!--
	 <tr>
	  <td width="40" valign=top >&nbsp;</td>
	  <td valign=middle width="120"  class=textabdatcla>Folio:&nbsp;<INPUT TYPE=text SIZE=15 NAME=folio></td>
	  <td width="80"  class=textabdatcla></td>
	 </tr>
	 //-->
	</table>
	<br>
	<table align=center border=0 cellspacing=0 cellpadding=0>
	 <tr>
	  <td><A href = "javascript:js_enviar();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt=Envio></a></td>
	  <td><A href = "javascript:js_regresarImportacion();" border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a></td>
	  <!-- <td><input type=image src = "/gifs/EnlaceMig/gbo25248.gif" border=0 alt="Detalle"></td> //-->
	  <td><A href = "javascript:js_verDetalle();" border = 0><img src = "/gifs/EnlaceMig/gbo25248.gif" border=0 alt="Detalle"></a></td>
	 </tr>
	</table>
   </td>
  </tr>
</table>


	<%
	String NomContrato = "";
	String NumContrato = "";
	String NumUsuario = "";
	String NomUsuario = "";
	String ClaveBanco = "";
	String webApp="";
	String nombreArchivoImp="";

	 if(request.getAttribute("NomContrato")!=null)
	   NomContrato= (String)request.getAttribute("NomContrato");

	 if(request.getAttribute("NumContrato")!=null)
	   NumContrato = (String)request.getAttribute("NumContrato");

	 if(request.getAttribute("NomUsuario")!=null)
	  NomUsuario = (String)request.getAttribute("NomUsuario");

	 if(request.getAttribute("NumUsuario")!=null)
	  NumUsuario = (String)request.getAttribute("NumUsuario");

	 if(request.getAttribute("ClaveBanco")!=null)
	  ClaveBanco = (String)request.getAttribute("ClaveBanco");

	 if (request.getAttribute("web_application")!= null)
	   webApp = (String)request.getAttribute("web_application");

	 if (request.getAttribute("web_application")!= null)
	   nombreArchivoImp = (String)request.getAttribute("nombreArchivoImp");
	%>

	<input type=hidden name=NomContrato value='<%=NomContrato%>'>
	<input type=hidden name=NumContrato value='<%=NumContrato%>'>
	<input type=hidden name=NomUsuario  value='<%=NomUsuario%>'>
	<input type=hidden name=NumUsuario  value='<%=NumUsuario%>'>
	<input type=hidden name=ClaveBanco  value='<%=ClaveBanco%>'>
	<input type=hidden name=WebApp value='<%=webApp.trim()%>'>

	<input type=hidden name=pagina value='3'>
	<input type=hidden name=operacion value='TESOFE'>
	<input type=hidden name=nombreArchivoImp value='<%=nombreArchivoImp%>'>

	<input type=hidden name=importeTotal value='<%=importeTotal%>'>
	<input type=hidden name=numeroRegistros value='<%=numeroRegistros%>'>
	<input type=hidden name=claveEmisora value='<%=claveEmisora%>'>
	<input type=hidden name=codigoOperacion value='<%=codigoOperacion%>'>
	<input type=hidden name=fechaPresentacion value='<%=fechaPresentacion%>'>
	<input type=hidden name=tipoArchivo value='<%=tipoArchivo%>'>

 </FORM>
</body>
</html>

<%!
public String FormatoMoneda ( double cantidad )
 {
   String formato = "";
   String language = "la"; // la
   String country = "MX";  // MX

   Locale local = new Locale (language,  country);
   NumberFormat nf = NumberFormat.getCurrencyInstance (local);
   if (cantidad < 0)
    {
       formato = nf.format ( cantidad );
       try
        {
          if (!(formato.substring (0,1).equals ("$")))
            formato ="$ -"+ formato.substring (2,formato.length ());
        }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   else
    {
       formato = nf.format ( cantidad );
       try
        {
          if (!(formato.substring (0,1).equals ("$")))
            formato ="$ "+ formato.substring (1,formato.length ());
        }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   return formato;
 }

public String FormatoMoneda ( String cantidad )
 {
   String language = "la"; // ar
   String country = "MX";  // AF
   String formato = "";

   Locale local = new Locale (language,  country);
   NumberFormat nf = NumberFormat.getCurrencyInstance (local);

   if(cantidad ==null ||cantidad.equals (""))
       cantidad="0.00";

   double importeTemp = 0.00;
   importeTemp = new Double (cantidad).doubleValue ();
   if (importeTemp < 0)
    {
       try
       {
         formato = nf.format (new Double (cantidad).doubleValue ());
		 if (!(formato.substring (0,1).equals ("$")))
			 formato ="$ -"+ formato.substring (2,formato.length ());
       }
       catch(NumberFormatException e) {formato="$ 00.00";}
    }
   else
    {
       try
        {
		  formato = nf.format (new Double (cantidad).doubleValue ());
		  if (!(formato.substring (0,1).equals ("$")))
			 formato ="$ "+ formato.substring (1,formato.length ());
        }
       catch(NumberFormatException e)
       {formato="$ 00.00";}
     }
    if(formato==null)
       formato = " 00.00";
   return formato;
 }
%>