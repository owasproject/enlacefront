 <%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Bienvenido a Enlace Internet</title>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<meta content="s26050" name="Codigo de Pantalla">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<link href="/EnlaceMig/consultas.css" type="text/css"
	rel="stylesheet">

<meta content="MSHTML 6.00.2900.2180" name="GENERATOR">
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" language="JavaScript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
/*<VC proyecto="200710001" autor="ESC" fecha="30/04/2007" descripción="Captura de datos del cliente">*/
function validaEntradas()
{

   // se valida que no falte ningun dato
   /*if(esVacio(document.registro.nombre.value))cuadroDialogo("Error: Falta nombre",1);
   else
      if(esVacio(document.registro.paterno.value))cuadroDialogo("Error: Falta apellido paterno",1);
   else
      if(esVacio(document.registro.materno.value))cuadroDialogo("Error: Falta apellido materno",1);
   else
       if(esVacio(document.registro.direccion.value))cuadroDialogo("Error: Falta direcci&oacute;n",1);
   else */
   if(esVacio(document.registro.email.value))cuadroDialogo("Falta correo electr&oacute;nico",10);
   else
       if(!validacorreo(document.registro.email.value))cuadroDialogo("Ingrese un correo electr&oacute;nico v&aacute;lido",10);
   else
       if(esVacio(document.registro.codigoArea.value))cuadroDialogo("Falta C&oacute;digo de &aacute;rea",10);
   else
   	   if(document.registro.codigoArea.value.length<2)cuadroDialogo("El c&oacute;digo de &aacute;rea debe tener al menos dos d&iacute;gitos",10);
   else
       if(esVacio(document.registro.telefonoMovil.value))cuadroDialogo("Falta Tel&eacute;fono m&oacute;vil",10);
   /*else
   	   if(document.registro.telefonoMovil.value.length!=7)cuadroDialogo("El tel&eacute;fono debe tener 7 d&iacute;gitos",10);*/
   else
       if(!esVacio(document.registro.codigoAreaAlt.value)&& esVacio(document.registro.telefonoMovilAlt.value))cuadroDialogo("Falta Tel&eacute;fono m&oacute;vil alterno",10);
   else
       if(esVacio(document.registro.codigoAreaAlt.value)&& !esVacio(document.registro.telefonoMovilAlt.value))cuadroDialogo("Falta C&oacute;digo de &aacute;rea alterno",10);
   else
      if(!esVacio(document.registro.codigoAreaAlt.value)&& document.registro.codigoAreaAlt.value.length<2)cuadroDialogo("El c&oacute;digo de &aacute;rea alterno debe tener al menos dos d&iacute;gitos",10);
  /* else
      if(!esVacio(document.registro.telefonoMovilAlt.value)&& document.registro.telefonoMovilAlt.value.length!=7)cuadroDialogo("El Tel&eacute;fono m&oacute;vil alterno debe tener 7 d&iacute;gitos",10);*/

   else
   	if ((document.registro.codigoArea.value.length + document.registro.telefonoMovil.value.length)!=10)cuadroDialogo("El tel&eacute;fono con su c&oacute;digo de &aacute;rea debe ser de 10 d&iacute;gitos",10);
   else
   	if (!esVacio(document.registro.codigoAreaAlt.value) && !esVacio(document.registro.telefonoMovilAlt.value)
   	&&    (document.registro.codigoAreaAlt.value.length + document.registro.telefonoMovilAlt.value.length)!=10 )	cuadroDialogo("El tel&eacute;fono alterno con su c&oacute;digo de &aacute;rea debe ser de 10 d&iacute;gitos",10);

   else
      if((document.registro.codigoArea.value+document.registro.telefonoMovil.value)==(document.registro.codigoAreaAlt.value+document.registro.telefonoMovilAlt.value)) cuadroDialogo("Tel&eacute;fono m&oacute;vil y el tel&eacute;fono m&oacute;vil alterno deben ser diferentes",10);
   else document.registro.submit();
}


function esVacio(valor)
{
   return (valor=="" || valor =="");
}
function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }





function limpiar()
{
 document.registro.email.value="";
 document.registro.codigoArea.value="";
 document.registro.telefonoMovil.value="";
 document.registro.codigoAreaAlt.value="";
 document.registro.telefonoMovilAlt.value="";

}

function validacorreo(email){
    //regx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    regx=/^[0-9A-Za-z][A-Za-z0-9_.-]*@[A-Za-z0-9_-]+\.[A-Za-z0-9_.]+[A-za-z]$/
    return regx.test(email);
}


function cancel(){
// vswf:meg cambio de NASApp por Enlace 08122008
		document.forms[0].action='/Enlace/enlaceMig/LoginServlet?opcion=2&accion=4';
		document.forms[0].submit();
	}
/*</VC>*/
</script>
<style type="text/css">
#listaOpciones{
	height: 300px;
	border: 1px solid black;
	font-family: Arial, Helvetica, sans-serif;
	width: 486px;
	background-image: url(img/fondo003.jpg);
	background-repeat: no-repeat;
	background-position: bottom right;
}

#listaOpciones img{
	border: 0px;
	position:relative;
	top: +15px;
}

#listaOpciones li{
	padding-bottom: 15px;
	list-style-type: none;
	list-style-position: outside;
}

#listaOpciones li a, #listaOpciones li a:active, #listaOpciones li a:link{
	color: red;
	text-decoration: none;
}
</style>
</head>
<body bgColor="#ffffff" leftMargin="0"
	background="img/gfo25010.gif" topMargin="0"
	onload="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gba25020.gif')"
	marginwidth="0" marginheight="0">
<table cellSpacing="0" cellPadding="0" width="600" border="0">
	<tbody>
		<tr>
			<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
					<table class="tabfonbla" cellSpacing="0" cellPadding="0"
						width="596" border="0">
						<tbody>
							<tr vAlign="top">
								<td rowSpan="2"><img height="41"
									src="/gifs/EnlaceMig/glo25010.gif" width="237" alt=""></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('contactanos','','/gifs/EnlaceMig/gbo25111.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25110.gif" width="30"
									border="0" name="contactanos" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Contáctenos"
									src="/gifs/EnlaceMig/gbo25120.gif" width="59"></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('atencion','','/gifs/EnlaceMig/gbo25131.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25130.gif" width="36"
									border="0" name="atencion" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Atención telefónica"
									src="/gifs/EnlaceMig/gbo25140.gif" width="90"></td>
								<td rowSpan="2"><a
									onmouseover="MM_swapImage('centro','','/gifs/EnlaceMig/gbo25151.gif',1)"
									onmouseout="MM_swapImgRestore()" href="javascript:;"><img
									height="33" src="/gifs/EnlaceMig/gbo25150.gif" width="33"
									border="0" name="centro" alt=""></a></td>
								<td rowSpan="2"><img height="20" alt="Centro de mensajes"
									src="/gifs/EnlaceMig/gbo25160.gif" width="93"></td>
								<td width="18" bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="18" alt=""></td>
							</tr>
							<tr vAlign="top">
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="18" alt=""></td>
							</tr>
						</tbody>
					</table>
					</td>
					<td>
					<table cellSpacing="0" cellPadding="0" width="100%"
						bgColor="#ffffff" border="0">
						<tbody>
							<tr>
								<td bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="35" alt=""></td>
								<td align="middle" rowSpan="2"><img height="41" alt=""
									src="/gifs/EnlaceMig/glo25020.gif" width="57"></td>
								<td width="100%" bgColor="#de0000"><img height="17"
									src="/gifs/EnlaceMig/gau25010.gif" width="5" alt=""></td>
							</tr>
							<tr>
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="35" alt=""></td>
								<td><img height="24" src="/gifs/EnlaceMig/gau25010.gif"
									width="5" alt=""></td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td vAlign="top" width="100%">

			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tbody>
					<tr>
						<td class="titpag" vAlign="bottom">Administrador de
						Contrase&ntilde;as</td>
						<td class="tabfonbla" align="right"><img height="33"
							alt="Sitio seguro" src="/gifs/EnlaceMig/gba25010.gif"
							width="126"></td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top"
				style="background-image: url(/gifs/EnlaceMig/gfo25020.gif); background-repeat: repeat-x;">&nbsp;</td>
		</tr>
		<tr>
			<td vAlign="top" width="100%">

			<table cellSpacing="0" cellPadding="0" border="0" width="100%">
				<tbody>

					<tr>
						<td class="texencrut">&nbsp;<img src="/gifs/EnlaceMig/gic25030.gif"
							alt=""> Administrador de Contrase&ntilde;as &gt; Generar Contrase&ntilde;a.</td>
						<td align="right">
						<table>
							<tr>
								<td class="texencfec" vAlign="top" noWrap align="right"
									height="12">
									<% java.util.Date dt = new java.util.Date(); %>

			<%

			String mes;

			switch (dt.getMonth()+1 )

			{

				case  1: mes = "Enero";		 break;

				case  2: mes = "Febrero";	 break;

				case  3: mes = "Marzo";		 break;

				case  4: mes = "Abril";		 break;

				case  5: mes = "Mayo";       break;

				case  6: mes = "Junio";		 break;

				case  7: mes = "Julio";		 break;

				case  8: mes = "Agosto";	 break;

				case  9: mes = "Septiembre"; break;

				case 10: mes = "Octubre";	 break;

				case 11: mes = "Noviembre";	 break;

				case 12: mes = "Diciembre";  break;

				default: mes = "Enero";

			}



			String strDia = "";

			switch (dt.getDay())

			{

				case  0: strDia = "Domingo";   break;

				case  1: strDia = "Lunes";     break;

				case  2: strDia = "Martes";    break;

				case  3: strDia = "Miercoles"; break;

				case  4: strDia = "Jueves";    break;

				case  5: strDia = "Viernes";   break;

				case  6: strDia = "Sabado";    break;

			}



			int anio = dt.getYear()+1900;

			String mifecha = strDia +" " + dt.getDate() + " de " +  mes + " del "+anio;



			String minutos_ = ""+dt.getMinutes();

			if(minutos_.trim().length()==1)

				minutos_ = "0"+minutos_;

			String mihora = dt.getHours()+":"+minutos_;



			%>

			<%=mifecha %>
			 <br>
								<%=mihora%></td>
								<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
								<td align="right"><a href="/Enlace/enlaceMig/logout?origen=1"
								onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)">
									<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a></td>
							</tr>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td align="center"><!--
Lista de Opciones Principales
-->
<!-- vswf:meg cambio de NASApp por Enlace 08122008 -->
			<form name="registro" action="/Enlace/enlaceMig/jsp/tokenMac2.jsp?numeroPantalla=8" method="post">
			<table cellSpacing="0" cellPadding="0" width="282" border="0"
				align="center">
				<tbody>
					<tr>
						<td>	<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td style="height:17px; width:11px; background-image: url(/gifs/EnlaceMig/izq00.gif); background-repeat: no-repeat;" ></td>
								<th class="tabmovtex11" style="background-image: url(/gifs/EnlaceMig/centro00.gif); background-repeat: repeat-x;" >Paso 2 de 3</th>
								<td style="height:17px; width:11px; background-image: url(/gifs/EnlaceMig/der00.gif); background-repeat: no-repeat;" ></td>
							</tr>
							</table></td>
					</tr>
					<tr>
						<td>
						<table class="textabdatcla" cellSpacing="0" cellPadding="0"
							width="282" border="0">
							<tbody>
								<tr>
									<td class="tittabdat" nowrap="nowrap" style="height: 20px;"><P>Valide y en su caso actualice sus datos</P><P>Esta&nbsp;informaci&oacute;n ser&aacute; utilizada para notificarle<BR>
									 el estatus de su solicitud.</P></td>
								</tr>
								<tr>
									<td>
									<table cellSpacing="10" cellPadding="0" width="300" border="0">
																<tbody>
																<!-- 		<tr>
																		<td class="tabmovtex11">
																			 Nombre. </td>
																		<td>
																			<input maxLength="40" name="nombre" value="">
																		</td>
																	</tr>
																<tr>
																		<td class="tabmovtex11">Apellido Paterno</td>
																		<td>
																			<input maxLength="20" name="paterno" value="">
																		</td>
																	</tr>
																<tr>
																		<td class="tabmovtex11">Apellido Materno</td>
																		<td>
																			<input maxLength="20" name="materno" value="">
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11">
																			Direcci&oacute;n:
																		</td>
																		<td>
																			<input maxLength="80" name="direccion" value="">
																		</td>
																	</tr> -->
																	<tr>
																		<td class="tabmovtex11" colspan="2">
																			<%=request.getSession().getAttribute("usuarioEnlace") %>
																		</td>

																	</tr>
																	<tr>
																		<td class="tabmovtex11">
																			 Correo electr&oacute;nico:
																		</td>
																		<td>
																			<input maxLength="80" name="email" value="<%=((String)request.getAttribute("email")).trim()%>">
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11">
																			 Tel&eacute;fono m&oacute;vil (celular o nextel).</td>
																		<td>
																			<input maxLength="3" size="3" name="codigoArea" value="<%=((String)request.getAttribute("areaCode")).trim()%>" onkeypress="return soloNumeros(event);" onkeyup="if(this.value.length==3){document.registro.telefonoMovil.focus()};">
																			<input maxLength="8" size="12" name="telefonoMovil" value="<%=((String)request.getAttribute("telefono")).trim()%>" onkeypress="return soloNumeros(event);" onkeyup="if(this.value.length==8){document.registro.codigoAreaAlt.focus()}">
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11">
																			 Tel&eacute;fono m&oacute;vil alterno (celular o nextel).</td>
																		<td>
																		    <input maxLength="3" size = "3" name="codigoAreaAlt" value="<%=((String)request.getAttribute("areaCodeAlt")).trim()%>" onkeypress="return soloNumeros(event);" onkeyup="if(this.value.length==3){document.registro.telefonoMovilAlt.focus()}">
																			<input maxLength="8" size = "12" name="telefonoMovilAlt" value="<%=((String)request.getAttribute("telefonoAlt")).trim()%>" onkeypress="return soloNumeros(event)" >
																		</td>
																	</tr>
																	<tr>
																		<td class="tabmovtex11" align="middle" colspan="2">
																			<table cellSpacing="0" cellPadding="0" border="0">
																				<tbody>
																					<tr>
																						<td width="76">
																							<a href="javascript:validaEntradas();"><!--<input type="image" border="0" name="imageField2" src="/NASApp/enlaceMig/jsp/img/continuar.gif" width="76" height="22" alt="Iniciar">-->  <IMG height="22"  alt="Aceptar" src="/gifs/EnlaceMig/gbo25280.gif"
																									border="0" name="imageField3"> </a>
																						</td>
																						<td width="77"><!--<input type="image" border="0" name="imageField" src="/gifs/EnlaceMig/gbo25650.gif" width="77" height="22" alt="Limpiar">-->

																							<a href="javascript:limpiar();"><img alt="Limpiar" src="/gifs/EnlaceMig/gbo25650.gif"  border="0" name="imageField"> </a>

																						</td>
																						<td width="77">
																						  <a href="javascript:cancel();"><IMG src="/gifs/EnlaceMig/cancelar.gif" alt="Cancelar" border="0" />
																					  	</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td background="/gifs/EnlaceMig/gfo25020.gif" height="12"><img
							height="12" src="/gifs/EnlaceMig/gau25010(1).gif" width="5"
							alt=""></td>
					</tr>
				</tbody>
			</table>
			 </form>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
<script>
document.registro.email.focus();
  <%= request.getAttribute("MensajeLogin01") %>
</script>