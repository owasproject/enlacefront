<html>
<head>
	<title>Transferencias Internacionales</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>

/************************************************************************************/
 var errores=new Array(
                   "TipoCambioVenta",
				   "TipoCambioCompra",
                   " la CUENTA.",
				   " la CUENTA Hidden",
				   "info",
                   " el IMPORTE",
				   "la divisa (clave)",
				   " la DIVISA",
				   " hidden TCV_Venta",
				   " hidden TCV_Compra",
				   " hidden Desdivisa");

 var evalua="XXTtinTSXXXX";

function EnviarForma()
{
  var impTmp=0;
  var longitud=0;

  if(ValidaTodo(document.Datos,evalua,errores))
   {
     impTmp=trimString(document.Datos.Importe.value);
     if(impTmp.indexOf(".")<0)
	  impTmp+=".00";
	 longitud=impTmp.substring(impTmp.indexOf(".")+1,impTmp.length).length;
	 if(longitud>2)
	  {
	    document.Datos.Importe.focus();
		cuadroDialogo("Solo se permiten 2 decimales",2);
	  }
	 else
	  {
	    if(longitud==1)
		 impTmp+="0";
		if(longitud==0)
		 impTmp+="00";
	    document.Datos.Importe.value=impTmp;
	    document.Datos.submit();
	  }
   }
}

function SeleccionaDivisa()
 {
   var forma=document.Datos;

   if(forma.Moneda.options[forma.Moneda.selectedIndex].value=="0")
	 forma.CveDivisa.value="";
   else
     var aux=forma.Moneda.options[forma.Moneda.selectedIndex].value;
	 forma.CveDivisa.value=aux.substring(0,3);
//	 forma.CveDivisa.value=aux;
   forma.DesDivisa.value=forma.Moneda.options[forma.Moneda.selectedIndex].text;
     forma.DirectoInverso.value=aux.substring(3);
 }

function MuestraInfo()
 {
   var forma=document.Datos;

   if(trimString(forma.textCuentas.value)=="")
    {
	  forma.Info.value="";
	  return;
	}

   if(trimString(ctatipro)=="6")
	forma.Info.value="El cliente vende.";
   else
    forma.Info.value="El cliente compra.";
 }

function Consultar()
 {
   document.Datos.action="MTI_Consultar?Modulo=0";
   document.Datos.submit();
 }

/******************  ********* ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>


<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.Datos.Cuentas.value=ctaselec+"|"+ctadescr+"|"+ctatipro+"@";
  document.Datos.textCuentas.value=ctaselec+" "+ctadescr;
  MuestraInfo();
}

function EnfSelCta()
{
   document.Datos.textCuentas.value="";
   SeleccionaDivisa();
}



</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" onLoad="EnfSelCta();">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
%>

<FORM  NAME="Datos" method=post action="MTI_Transferencia?Modulo=0">

  <p>
  <table align=center border=0 cellspacing=0 cellpadding=3 class='textabdatcla'>
   <tr>
	 <td colspan=2 class="tittabdat"> Selecci&oacute;n de Cuenta y Monto de Transferencias </td>
   </tr>

   <tr>
	 <td colspan=2><br></td>
   </tr>

   <tr>
    <td colspan=2>
	 <table border=0 width=100%>
	  <tr>
	   <td>
	    <table cellpadding=0 cellspacing=0 border=0 align=center>
	     <tr>
		  <td class='tabmovtex'> Tipo de cambio a la venta&nbsp;&nbsp; </td>
		  <td class='tabmovtex'> <input type="text"  name=TipoCambioVenta  SIZE=10 value='<%= request.getAttribute("TCV_Venta")%>' onFocus='blur();' class='tabmovtex'></td>
		 </tr>
	    </table>
	  </td>

	   <td>
	    <table cellpadding=0 cellspacing=0 border=0>
		 <tr>
		  <td class='tabmovtex'> Tipo de cambio a la compra &nbsp;&nbsp; </td>
		  <td class='tabmovtex'> <input type=text value='<%= request.getAttribute("TCV_Compra")%>' name="TipoCambioComTxt" onFocus='blur();' size='10' class='tabmovtex'></td>
		 </tr>
		</table>
	   </td>

	  </tr>
	 </table>

	</td>
   </tr>

   <tr>
    <td class='tabmovtex' colspan=2>
	  <div align="center">
	  Este tipo de cambio es informativo. Para hacer una cotizaci&oacute;n de tipo<br>
	  de cambio, seleccione la cuenta de cargo y el importe a operar
	  <br><font color=red><%= request.getAttribute("TipoCambioError")%></font>
	  </div>
	 </td>
   </tr>

   <tr>
    <td colspan=2><br></td>
   </tr>

   <tr>
    <td class='tabmovtex'>
	  Cuenta Cargo
	</td>
	<td class='tabmovtex'>
     <input type="text" name=textCuentas  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="" class='tabmovtex'>
	 <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
	 <input type="hidden" name="Cuentas" value="" >
	</td>
   </tr>

   <tr>
    <td class='tabmovtex'><br></td>
	<td class='tabmovtex'><input type=hidden name=Info size=20 onFocus="blur();"><br></td>
   </tr>

   <!--
   <tr>
    <td class='tabmovtex'>Importe</td>
	<td class='tabmovtex'><input type=text name=Importe size=10 MAXLENGTH="15"></td>
   </tr>
   //-->

   <input type=hidden name=Importe size=10 MAXLENGTH="15" value='0'>

   <tr>
    <td class='tabmovtex'>
	  Divisa
	</td>
	<td class='tabmovtex'>
	  <input type=text name=CveDivisa maxlength=5 size=5 onFocus='blur();' class='tabmovtex'>
	  <input type=hidden name=DirectoInverso>
	  <select name=Moneda class='tabmovtex' onChange='SeleccionaDivisa();'>
	    <option value=0> Seleccione Divisa </option>
	    <%
		   if(request.getAttribute("Divisas")!=null)
		      out.print(request.getAttribute("Divisas"));
		%>
	  </select>
	</td>
   </tr>

   <tr>
     <td colspan=2><br></td>
   </tr>
  </table>

  <br>
  <table border=0 cellpadding=0 cellspacing=0 align=center>
   <tr>
     <td><a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/gbo25310.gif" border=0></a></td>
	 <td><A href="javascript:Consultar();" border=0><img src="/gifs/EnlaceMig/gbo25400.gif" border=0></a></td>
   </tr>
  </table>

  <input type="hidden" name=TCV_Venta value=<%= request.getAttribute("TCV_Venta")%>>
  <input type=hidden name=TCV_Compra value=<%= request.getAttribute("TCV_Compra") %>>
  <input type=hidden name=DesDivisa >

</form>

</body>
</html>
