<%@ page import="mx.altec.enlace.bo.*" %>
<jsp:useBean id='Archivo'		class='mx.altec.enlace.bo.ArchivoConfirming' scope='session'/>
<jsp:useBean id='DA_Proveedor'	class='mx.altec.enlace.bo.Proveedor'			scope='session'/>
<%
String parTramaArchivo	= (String)request.getAttribute("tramaArchivo");
String parAccion		= (String)request.getAttribute("accion");
String parSecuencia		= (String)request.getAttribute("secuencia");
String parAceptados		= (String)request.getAttribute("aceptados");
String parRechazados	= (String)request.getAttribute("rechazados");
String parTotales		= (String)request.getAttribute("totales");
String parStatus		= (String)request.getAttribute("status");
String parDescStatus	= (String)request.getAttribute("desstatus");
String parFechaTrans	= (String)request.getAttribute("fechaTransmision");
String parFechaActual	= (String)request.getAttribute("fechaActual");

if(parTramaArchivo	== null) parTramaArchivo = "";
if(parSecuencia		== null) parSecuencia	 = "";

if(parFechaTrans	== null) parFechaTrans	= "";
if(parFechaActual	== null) parFechaActual	= "";
if(parStatus		== null) parStatus		= "";
if(parDescStatus	== null) parDescStatus	= "";
if(parAccion		== null) parAccion		= "";
if(parAceptados		== null) parAceptados	= "0";
if(parRechazados	== null) parRechazados	= "0";
if(parTotales		== null) parTotales		= "0";

if(parAccion.equals("6")) {	// Envia Archivo de Proveedores
	DA_Proveedor.setSecuencia(parSecuencia);
	DA_Proveedor.setRegAceptados	(Integer.parseInt(parAceptados));
	DA_Proveedor.setRegRechazados	(Integer.parseInt(parRechazados));
	DA_Proveedor.setRegTotales		(Integer.parseInt(parTotales));
	DA_Proveedor.setStatus	(parStatus);
	DA_Proveedor.setDescStatus		(parDescStatus);
	DA_Proveedor.setFechaActualizacion(parFechaActual);
}
else if(parAccion.equals("9")) {	// Recupera Archivo de Proveedores
	DA_Proveedor.setSecuencia(parSecuencia);
	DA_Proveedor.setFechaTransmision(parFechaTrans);
}

ArchivoConfirming archivo = null;
System.out.println("<>< ����� parTramaArchivo: " + parTramaArchivo);
if(!parTramaArchivo.equals("")) archivo = ArchivoConfirming.creaDeTrama(parTramaArchivo);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Enlace</title>

<meta http-equiv = "Content-Type" content="text/html">

<script language = "JavaScript1.2"	src = "/EnlaceMig/fw_menu.js">		</script>
<script language = "JavaScript"		src = "/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript"		src = "/EnlaceMig/scrImpresion.js">	</script>
<SCRIPT language = "JavaScript">

function MM_preloadImages() {	//v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {	//v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {		//v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {		//v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

// --- variables usadas para capturar el nombre de archivo
var respuesta		= 0;
var accionContinua	= 0;
var campoTexto		= "";

// --- Valida si el caracter recibido corresponde a un caracter numerico
function Caracter_EsNumerico(caracter)
{
	var ValorRetorno = true;

	if( !(caracter >= "0" && caracter <= "9") ) //Si no es Es numerico
		ValorRetorno = false;

	return ValorRetorno;
}

// --- Valida que la cadena sea Alfanumerica
function Alfanumerico(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if( !(caracter >= "0" && caracter <= "9") ) //Si no es Es numerico
			if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
				if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
					if ( caracter != "�" && caracter != "�")
						retorno = false;
	}
	return retorno;
}

// --- Valida si es un Numero de Secuencia
function EsNumeroDeSecuencia(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsNumerico(caracter))
			retorno = false;
	}
	return retorno;
}

// --- Elimina la Ruta, regresando solo el Nombre del Archivo
function EliminaRuta(Ruta)
{
	var pos;
	var Nombre = Ruta;
	var LongitudCadena = 0;

	pos = Ruta.indexOf('\\',0);
	LongitudCadena = Ruta.length;

	for(i=0; pos != -1; i++) {
		pos	 = Ruta.indexOf('\\',pos+1);
		if(pos != -1)
			Nombre = Ruta.substring(pos + 1,LongitudCadena);
	}
	return (Nombre);	// Regresa solo el Nombre del Archivo
}

// --- Se ejecuta despu�s de capturar una cadena en un Cuadro de Captura
function continuaCaptura()
{
	if(respuesta == 1) {
		if(accionContinua == 0) {
			if(vNombreArchivo(campoTexto)) {	// Nombre de Archivo
				document.MtoProveedores.archivo_actual.value = campoTexto;
				document.MtoProveedores.accion.value = "20";
				document.MtoProveedores.submit();
			}
		}
		else if (accionContinua == 3) {			// Recuperar Archivo
			if(EsNumeroDeSecuencia(campoTexto)) {
				document.MtoProveedores.comando.value = campoTexto + "@|";
				document.MtoProveedores.transmision.value = campoTexto;
				document.MtoProveedores.accion.value = "9";
				document.MtoProveedores.submit();
			}
			else {cuadroDialogo("El N&uacute;mero de Secuencia debe ser Num&eacute;rico.",3);}
		}
	}
}

// --- Se ejecuta despu�s de que se despliega un cuadro de confirmacion
function continua()
{
	if(respuesta==1) {
		if(accionContinua == 1) {	// Borrar un registro
			document.MtoProveedores.accion.value = "2";
			document.MtoProveedores.submit();
		}
		else if(accionContinua == 2) {	// Borrar un Archivo
			document.MtoProveedores.accion.value = "7";
			document.MtoProveedores.submit();
		}
	}
}

// --- valida que el nombre del Archivo sea Valido
function ValidaNombreArchivo(nombre)
{
	var ValorRegreso = false;

	if(nombre.length==0)			// Si el usuario no escribi� nada
		{cuadroDialogo("Nombre de Archivo no puede ser Null",3);}
	else if(parseInt(nombre) > 0 || nombre.substring(0,1) == "0")	// El nombre comienza con n�mero
		{cuadroDialogo("El nombre del archivo debe iniciar con un caracter",3);}
	else if(nombre.length > 12)		// El nombre es mayor de 12 caracteres
		{cuadroDialogo("El nombre del archivo debe tener maximo 10 caracteres",3);}
	else if(!Alfanumerico(nombre))	// El nombre no es Alfanumerico
		{cuadroDialogo("El nombre del archivo debe ser AlfaNumerico",3);}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

// --- valida que la extension del Archivo sea Valido
function ValidaExtension(extension)
{
	var ValorRegreso = false;

	if (!Alfanumerico(extension))	// Si la extension no es AlfaNumerica
		{cuadroDialogo("La extension del archivo debe ser AlfaNumerico",3);}
	else if(extension.length > 3)		// La extensi�n es mayor a tres caracteres
		{cuadroDialogo("La extension del archivo debe tener maximo 3 caracteres",3);}
	else {ValorRegreso = true;}

	return ValorRegreso;
}

// --- Valida el nombre del Archivo (Formato 10.3)
function vNombreArchivo(NombreArchivo)
{
	var puntoPos = NombreArchivo.indexOf('.');
	var nombre		= "";
	var extension	= "";
	var EsValidoNombreArchivo;

	nombre = (puntoPos != -1)?(NombreArchivo.substring(0,puntoPos)):NombreArchivo;
	extension = (puntoPos != -1)?NombreArchivo.substring(puntoPos+1):"";

	if(ValidaNombreArchivo(nombre))
		if(ValidaExtension(extension))
			return true;

	return false;
}

// --- Boton de Crear Nuevo Archivo
function js_nuevo()
{
	if(document.MtoProveedores.enviado.value == "si")
		cuadroDialogo("El archivo ya fue enviado. Debe crear uno nuevo", 3);
	else {
		accionContinua = 0;
		cuadroCaptura("Proporcione el nombre del archivo a crear: ", "Nombre del archivo de proveedores");
	}
}

// --- Boton de alta
function js_alta()
{
	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else if (document.MtoProveedores.enviado.value == "si")
		cuadroDialogo("No procesable, el archivo ya fue enviado. Cree uno nuevo", 3);
	else {
		document.MtoProveedores.accion.value = "1";
		document.MtoProveedores.submit();
	}
}

// ---  Boton baja de registro
function js_eliminar()
{
	var existenRegistros = <%= (archivo != null)?((archivo.longitud()>0)?"true":"false"):"false" %>;

	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else if (document.MtoProveedores.enviado.value == "si")
		cuadroDialogo("No procesable, el archivo ya fue enviado. Cree uno nuevo", 3);
	else if(!existenRegistros)
		cuadroDialogo("No existen registros para borrar",3);
	else {
		accionContinua = 1;
		cuadroDialogo("&iquest;Esta seguro de eliminar el proveedor seleccionado?", 2)
		// la acci�n contin�a en la funci�n 'continua()' despu�s de que el usuario toma su desici�n
	}
}

// --- Boton modificar
function js_modificar()
{
	var existenRegistros = <%= (archivo != null)?((archivo.longitud()>0)?"true":"false"):"false" %>;

	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else if (document.MtoProveedores.enviado.value == "si")
		cuadroDialogo("No procesable, el archivo ya fue enviado. Cree uno nuevo", 3);
	else if(!existenRegistros)
		cuadroDialogo("No existen registros para modificar",3);
	else {
		document.MtoProveedores.accion.value = "3";
		document.MtoProveedores.submit();
	}
}

// --- bot�n importar
function js_importar()
{
	var campo = document.MtoProveedores.Archivo;

	while(campo.value.substring(0,1) == " ") campo.value = campo.value.substring(1);
	while(campo.value.substring(campo.value.length-1) == " ") campo.value = campo.value.substring(0,campo.value.length-1);

	if(document.MtoProveedores.archivo_actual.value != "" || campo.value == "") {
		campo.focus();
		((document.MtoProveedores.archivo_actual.value != ""))? cuadroDialogo("No puede importar un archivo despues de crear uno nuevo", 3) : cuadroDialogo("Seleccione primero un archivo a importar", 3);
		return;
	}
	else {
		document.MtoProveedores.accion.value = "4";
		document.MtoProveedores.archivo_actual.value = EliminaRuta(campo.value);
		document.MtoProveedores.submit();
	}
}

// --- bot�n exportar
function js_exportar() {

	var existenRegistros = <%= (archivo != null)?((archivo.longitud()>0)?"true":"false"):"false" %>;

	if(document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else if(!existenRegistros)
		cuadroDialogo("No existen registros para exportar",3);
	else {
		document.MtoProveedores.accion.value = "5";
		document.MtoProveedores.submit();
	}
}

// --- Boton borrar
function js_borrar()
{
	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else {
		accionContinua = 2;
		cuadroDialogo("&iquest;Esta seguro de eliminar el archivo?", 2)
		// la acci�n contin�a en la funci�n 'continua()' despu�s de que el usuario toma su desici�n
	}
}

// --- Boton recuperar
function js_recuperar()
{
	if (document.MtoProveedores.archivo_actual.value == "")
		{cuadroDialogo("No ha creado, un archivo aun", 3);}
	else {
		accionContinua = 3;
		cuadroCaptura ("Escriba el n&uacute;mero de secuencia:", "Recupera");
	}
}

// --- Boton enviar
function js_enviar()
{
	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else if (document.MtoProveedores.enviado.value == "si")
		cuadroDialogo("No procesable, el archivo ya fue enviado. Cree uno nuevo", 3);
	else {
		document.MtoProveedores.accion.value = "6";
		document.MtoProveedores.archivo_actual = "";
		document.MtoProveedores.submit();
	}
}

// --- Boton Imprimir
function js_imprimir()
{
	if (document.MtoProveedores.archivo_actual.value == "")
		cuadroDialogo("No ha creado, ni importado un archivo aun", 3);
	else {
		document.MtoProveedores.accion.value = "8";
		document.MtoProveedores.submit();
	}
}

// -- Muestra los Errores al Importar un Archivo
function despliegaArchivoErrores()
{
	ventanaInfo1=window.open('','trainerWindow','width=450,height=300,toolbar=no,scrollbars=yes');
	ventanaInfo1.document.open();
	ventanaInfo1.document.write("<html>");
	ventanaInfo1.document.writeln("<head>\n<title>Mantenimiento a Proveedores</title>\n");
	ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
	ventanaInfo1.document.writeln("</head>");
	ventanaInfo1.document.writeln("<body bgcolor='white'>");
	ventanaInfo1.document.writeln("<form>");
	ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
	ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Informaci&oacute;n del Archivo Importado</th></tr>");
	ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");
	ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center width=0>");
	ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gic25060.gif'></td>");
	ventanaInfo1.document.writeln("<td class='tabmovtex1' align='center'><br> " + document.MtoProveedores.resumen.value + "<br><br>");
	ventanaInfo1.document.writeln("</td></tr></table>");
	ventanaInfo1.document.writeln("</td></tr>");
	ventanaInfo1.document.writeln("<tr><td class='tabmovtex1'>");
	ventanaInfo1.document.writeln(document.MtoProveedores.errores.value + "<br><br>");
	ventanaInfo1.document.writeln("</td></tr>");
	ventanaInfo1.document.writeln("</table>");
	ventanaInfo1.document.writeln("<table border=0 align=center >");
	ventanaInfo1.document.writeln("</body>\n</html>");
	ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'>");
	ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a>");
	ventanaInfo1.document.writeln("<A href='javascript:scrImpresion();' border=0>");
	ventanaInfo1.document.writeln("<img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' width='83' height='22'>");
	ventanaInfo1.document.writeln("</a></td></tr>");
	ventanaInfo1.document.writeln("</table>");
	ventanaInfo1.document.writeln("</form>");
	ventanaInfo1.document.writeln("</body>\n</html>");
	ventanaInfo1.document.close();
	ventanaInfo1.focus();
}

<%= (String) request.getAttribute("newMenu") %>

</SCRIPT>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%= request.getAttribute("despliegaEstatus" ) %>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%=(String) request.getAttribute("MenuPrincipal")%></TD>
  </TR>
</TABLE>

<%=(String) request.getAttribute("Encabezado" )%>


<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="MtoProveedores" METHOD="POST" ACTION="coMtoProveedor?opcion=1">
    <tr>
      <td align="center">
        <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="2">
              <table width="460" border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat" colspan="2"> Datos del archivo</td>
                </tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="450" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                        <td width="270" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Archivo:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <input type="text" name="archivo_actual" size="22" class="tabmovtex" value="<% if(request.getAttribute("archivo_actual" )!= null) out.print(request.getAttribute("archivo_actual")); %>"  onFocus='blur()'>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>N&uacute;mero de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% out.print(DA_Proveedor.getSecuencia()); %>" NAME="transmision" onFocus='blur();'>
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(DA_Proveedor.getFechaTransmision() != null) out.print(DA_Proveedor.getFechaTransmision()); %>" NAME="fechaTrans" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Fecha de actualizaci&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(DA_Proveedor.getFechaActualizacion() != null) out.print(DA_Proveedor.getFechaActualizacion());  %>" NAME="fechaAct" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Importe de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="0"  NAME="importeTrans" onFocus='blur();' >
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Total de registros:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% out.print(DA_Proveedor.getRegTotales()); %>" NAME="totRegs" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Registros aceptados:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% out.print(DA_Proveedor.getRegAceptados()); %>"  NAME="aceptados" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Registros rechazados: </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% out.print(DA_Proveedor.getRegRechazados()); %>"  NAME="rechazados" onFocus='blur();' >
                              </td>
                            </tr>
                            <!-- <tr>
                              <td class="tabmovtex" nowrap valign="middle">Registros
                                por transmitir:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <input type="text" name="textfield323" size="22" class="tabmovtex">
                              </td>
                            </tr> -->
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="200" height="94" align="center" valign="middle">
			  <A href = "javascript:js_nuevo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>
            </td>
          </tr>
          <tr>
            <td align="center" valign="bottom">
              <table width="200" border="0" cellspacing="2" cellpadding="3">
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" border="0" cellspacing="5" cellpadding="0">
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap> Importar archivo</td>
                      </tr>
                      <tr>
                        <td nowrap>
						  <input type="file" name="Archivo" size="15">
                        </td>
                      </tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
						  <A href = "javascript:js_importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
		  <tr>
			<td class="tabmovtex" align = "left" nowrap colspan = 2 width="100%">Si desea dar de alta m&aacute;s de 30 proveedores, debera hacerlo con importacion de archivo (Bot&oacute;n Browse o Buscar) </td>
		  </tr>
        </table>
        <br>
		<!-- Tabla de proveedores ----------------->
		<%
		if(archivo!=null) {
		%>
		<BR><BR>
		<TABLE class="textabdatcla" align="center" width = "90%" border=0 cellspacing=1>
			<TR>
				<TD class="tabmovtex" align = "left" nowrap colspan = "8" bgcolor = "#ffffff">Registro de Proveedores</TD>
			</TR>
			<TR>
				<TD class = "tittabdat" align = "center"> &nbsp;</TD>
				<TD class = "tittabdat" align = "center"><B>Clave de prov.</B></TD>
				<TD class = "tittabdat" align = "center"><B>Nombre o Raz&oacute;n Social</B></TD>
				<TD class = "tittabdat" align = "center"><B>R.F.C.</B></TD>
				<TD class = "tittabdat" align = "center"><B>Homo</B></TD>
				<TD class = "tittabdat" align = "center"><B>C&oacute;digo de cliente</B></TD>
				<TD class = "tittabdat" align = "center"><B>Estatus</B></TD>
				<TD class = "tittabdat" align = "center"><B>Descripci&oacute;n de estatus</B></TD>
			</TR>

			<%
			String estilo=""; int max=archivo.longitud();
			%>
			<%
			for(int a=0;a<max;a++) {
			%>
			<%
			estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla";
			%>
			<%
			Proveedor prov = archivo.obtenProveedor(a);
			%>
			<TR>
				<% String Nombre_RazonSocial = prov.getNombre().trim() + " " + prov.getApellidoPaterno().trim(); %>
				<TD class="<%= estilo %>" align=center><INPUT type=Radio name="ProvSel" value="<%= a %>" <%= ((a==0)?" checked":"") %>></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getClaveProveedor().trim().equals(""))?prov.getClaveProveedor():"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(Nombre_RazonSocial.equals(""))?Nombre_RazonSocial :"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getRFC().trim().equals(""))?prov.getRFC():"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getHomoclave().trim().equals(""))?prov.getHomoclave():"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getCodigoCliente().trim().equals(""))?prov.getCodigoCliente():"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getStatus().trim().equals(""))?prov.getStatus():"&nbsp;") %></TD>
				<TD class="<%= estilo %>"><%= (!(prov.getDescStatus().trim().equals(""))?prov.getDescStatus():"&nbsp;") %></TD>
			</TR>
			<% } %>
			<TR>
				<TD class="tabmovtex" align = "left" nowrap colspan = "8" bgcolor = "#ffffff"> Para modificar o dar de baja un registro, selecci&oacute;nelo previamente </TD>
			</TR>
		</TABLE>
		<BR>
		<% } %>

        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="top" width="66">
			  <A href = "javascript:js_alta();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            </td>
            <td align="left" valign="top" width="127">
  			  <A href = "javascript:js_eliminar();" border=0><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
            </td>
            <td align="left" valign="top" width="93">
   			  <A href = "javascript:js_modificar();" border=0><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
            </td>
            <td align="left" valign="top" width="83">
  			  <A href = "javascript:js_imprimir();" border=0><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="top" width="78">
   			  <A href = "javascript:js_enviar();" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            </td>
            <td align="left" valign="top" width="97">
   			  <A href = "javascript:js_recuperar();" border=0><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>
            </td>
            <td align="left" valign="top" width="77">
			  <A href = "javascript:js_borrar();" border=0><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>
            </td>
            <td align="left" valign="top" width="85">
			  <A href = "javascript:js_exportar();" border=0><img src="/gifs/EnlaceMig/gbo25230.gif" border=0 alt="Exportar" width="77" height="22"></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
	<INPUT type=hidden name=comando>
	<INPUT type=hidden name=accion>
	<INPUT type=hidden name=enviado	value="<%= ((request.getAttribute("enviado")==null)?"no":(String)request.getAttribute("enviado")) %>">
	<INPUT type=hidden name=tramaArchivo value="<%= request.getAttribute("tramaArchivo") %>">
	<INPUT type=hidden name=resumen	value="<% if ( request.getAttribute("resumen") != null) { out.println(request.getAttribute("resumen")); } %>">
	<INPUT type=hidden name=errores	value="<% if ( request.getAttribute("errores") != null) { out.println(request.getAttribute("errores")); } %>">
  </form>
</table>
</BODY>
</HTML>