<%@page import="java.util.List,java.util.Iterator" %>
<%@page import="mx.altec.enlace.utilerias.Global"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Solicitud de Estados de Cuenta Historicos</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript">
	<%
	  if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));
	  String codigoError = request.getAttribute("ERROR") != null ? request.getAttribute("ERROR").toString() : "";
	  String mensajeError = request.getAttribute("MENSAJEERROR") != null ? request.getAttribute("MENSAJEERROR").toString() : "";
	%>
  </script>

  <script type="text/javascript">
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
  </script>

  <script type="text/javascript">
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
  </script>

  <script type="text/javascript">
	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}
  </script>

  <script type="text/javascript">
	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
  </script>

  <script type="text/javascript">
	<%
	       if (request.getAttribute("newMenu")!= null) {
	       out.println(request.getAttribute("newMenu"));
	       }
	%>
  </script>

  <script type="text/javascript">
	var ctaselec;
	var ctadescr;
	var ctatipre;
	var ctatipro;
	var ctaserfi;
	var ctaprod;
	var ctasubprod;
	var tramadicional;
	var cfm;
  </script>

  <script type="text/javascript">
	var periodos = '';
	var periodosAgregados = '';
	var resultadoWS;
	var ventana;
  </script>

  <script type="text/javascript">
	var nombreMeses = {
		'ENE' : 'Enero', 'FEB' : 'Febrero', 'MAR' : 'Marzo',
		'ABR' : 'Abril', 'MAY' : 'Mayo', 'JUN' : 'Junio',
		'JUL' : 'Julio', 'AGO' : 'Agosto', 'SEP' : 'Septiembre',
		'OCT' : 'Octubre', 'NOV' : 'Noviembre', 'DIC' : 'Diciembre'};
  </script>

  <script type="text/javascript">
	var numeroMeses = {
		'ENE' : '01', 'FEB' : '02', 'MAR' : '03',
		'ABR' : '04', 'MAY' : '05', 'JUN' : '06',
		'JUL' : '07', 'AGO' : '08', 'SEP' : '09',
		'OCT' : '10', 'NOV' : '11', 'DIC' : '12'};
		
	function noenter(e) {
	    e = e || window.event;
	    var key = e.keyCode || e.charCode;
	    return key !== 13; 
           }

  </script>

  <script type="text/javascript">

//Este metodo envia todos los periodos seleccionados esten o no checados.
function EnviarForma(){
	var cCount = 0;
	var tbl = document.getElementById('tblPeriodosSeleccionados');
	var tmp = "";
	if(tbl.tBodies[0] != null){
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow && tbl.tBodies[0].rows[i].myRow.one.getAttribute('type') == 'checkbox') {
				//var checado=tbl.tBodies[0].rows[i].myRow.one.checked
				var check=tbl.tBodies[0].rows[i].myRow.one;
				tmp = tmp + check.value + "|";
				cCount++;
			}
		}
		if (tmp.length > 0 && cCount > 0) {
			document.getElementById("periodosSoliciar").value = tmp;
			document.Datos.action = "EstadoCuentaHistoricoServlet?ventana=2";
			document.Datos.submit();
		} else {
			cuadroDialogo("Por favor, seleccione o agrege un periodo hist&oacute;rico a solicitar.",4);
		}
	}
}

function myRowObject(one, two){
	this.one = one; // input checkbox
}

function trim(valorInput) {
	return valorInput.replace(/^\s+|\s+$/g,"");
}

function agregarPeriodoEnTabla(tipo){
	if (!validaPreviaADescarga(tipo)) {
		return;
	}
	var tbl = document.getElementById('tblPeriodosSeleccionados');
	var slcPeriodos = document.getElementById('slcPeriodos');
	
	
	if(slcPeriodos.length ==0){
		if(tipo == "c"){
			cuadroDialogo("No existen periodos disponibles para la cuenta seleccionada", 1);
		}else{
			cuadroDialogo("No existen periodos disponibles para la tarjeta seleccionada", 1);
		}
	return;
	}
	
	var slcPeriodosValor = slcPeriodos[slcPeriodos.selectedIndex].value;	
	var periodoSeleccionado = slcPeriodos[slcPeriodos.selectedIndex].text;	
	var folioOD=slcPeriodosValor.substring(0,8);
	var tipoedc=slcPeriodosValor.substring(9);
	var cuentaSeleccionada = document.getElementById("cambioCuentaTXT").value;
	var codigoCliente = document.getElementById('hdCodCliente').value;
	var folioOnDemand = slcPeriodos[slcPeriodos.selectedIndex].value;
	var mesSeleccionado = periodoSeleccionado.substring(0, 3);
	var anioSeleccionado = periodoSeleccionado.substr(periodoSeleccionado.length - 4);
	var num = tbl.tBodies[0].rows.length;
	//var anioMes = '[' + anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + ':::' + folioOnDemand + ']';
	var anioMes = '[' + anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + '@' + folioOnDemand + ']';

	if (tipo == "c" && slcPeriodosValor.indexOf('00000000')!=-1) {
		cuadroDialogo("No se gener\u00f3 Estado de Cuenta para el periodo seleccionado.",4);
	} else if (tipo == "t" &&  slcPeriodosValor.indexOf('00000000@000')!=-1) {
		cuadroDialogo("No se gener\u00f3 Estado de Cuenta para el periodo seleccionado.",4);
	}else if(periodosAgregados.indexOf(anioMes) >= 0){
			cuadroDialogo('El periodo ya ha sido agregado', 4);
		} else {
		
				var uri="EstadoCuentaHistoricoServlet?ventana=5&cadena="+periodoSeleccionado+"@"+folioOD+"@"+tipoedc+"@"+cuentaSeleccionada+"@"+codigoCliente;
				funcionAjax(uri);
				//ventana=window.open(uri,'periodos','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=100,height=100');
				//ventana.focus();
			};
}

function funcionAjax(uri) {
	var webService = "<%=Global.validaExistenciaPDFWsdlLocation%>";
	var xmlhttp;
	//Obtener el objeto xmlhttp dependiendo del explorador.
	if (window.XMLHttpRequest) {
	  xmlhttp=new XMLHttpRequest();
	} else {
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	//Esta funcion se ejecutara al obtener la respuesta de Ajax
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var regreso=xmlhttp.responseText;
			var codError = regreso.substring(0,3);
			if (codError == '301') {
				Agrega();
			}
			else if (codError == '100') {
				MensajeErrorOD();
			} else if (codError == '501') {
				cuadroDialogo("Estimado Cliente. Ya existe una solicitud realizada para esta cuenta y periodo realizada por este u otro contrato.", 4);
			} else {
				cuadroDialogo("Su transacci&oacute;n no puede ser atendida, intente m&aacute;s tarde", 4);
			}
		}
	}
	//Generar la peticion Ajax
	xmlhttp.open("GET",uri,true);
	xmlhttp.send();
}

function Agrega(){
	var tbl = document.getElementById('tblPeriodosSeleccionados');
	var slcPeriodos = document.getElementById('slcPeriodos');
	var periodoSeleccionado = slcPeriodos[slcPeriodos.selectedIndex].text;
	var folioOnDemand = slcPeriodos[slcPeriodos.selectedIndex].value;
	var mesSeleccionado = periodoSeleccionado.substring(0, 3);
	var anioSeleccionado = periodoSeleccionado.substr(periodoSeleccionado.length - 4);
	var num = tbl.tBodies[0].rows.length;
	//var anioMes = '[' + anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + ':::' + folioOnDemand + ']';
	var anioMes = '[' + anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + '@' + folioOnDemand + ']';

	// Agrega la fila
	var row = tbl.tBodies[0].insertRow(num);

	//Inserta checkbox
	var cell0 = row.insertCell(0);
	var cbEl = document.createElement('input');
	cbEl.setAttribute('type', 'checkbox');
	cbEl.setAttribute('name', 'chkPeriodoAgregado');
	cbEl.setAttribute('id', 'chkPeriodoAgregado' + anioSeleccionado + numeroMeses[mesSeleccionado]);
	//cbEl.setAttribute('value', anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + ':::' + folioOnDemand);
	cbEl.setAttribute('value', anioSeleccionado + '' +  numeroMeses[mesSeleccionado] + '@' + folioOnDemand);
	cell0.appendChild(cbEl);

	periodosAgregados += anioMes;

	//Inserta id de la empresa
	var cell1 = row.insertCell(1);
	var textNode = document.createTextNode(nombreMeses[mesSeleccionado] + ' ' + anioSeleccionado);
	cell1.appendChild(textNode);

	row.myRow = new myRowObject(cbEl);
	establecerClaseFila();
}

function MensajeErrorOD(){
	cuadroDialogo("Estimado Cliente. El Estado de Cuenta PDF para el periodo solicitado ya se encuentra "+
				  "disponible para ser descargado. La solicitud pudo haber sido realizada por este u otro contrato. " +
				  "Por favor realice la descarga desde el menu: "+
				  " \"Estados de Cuenta &gt; Descarga de Estados de Cuenta &gt; PDF \" ",1);
}

function eliminarSeleccionados(){
	var checkedObjArray = new Array();
	var cCount = 0;
	var tbl = document.getElementById('tblPeriodosSeleccionados');
	if(tbl.tBodies[0] != null){
		for (var i=0; i<tbl.tBodies[0].rows.length; i++) {
			if (tbl.tBodies[0].rows[i].myRow && tbl.tBodies[0].rows[i].myRow.one.getAttribute('type') == 'checkbox' && tbl.tBodies[0].rows[i].myRow.one.checked) {
					checkedObjArray[cCount] = tbl.tBodies[0].rows[i];
					periodosAgregados = periodosAgregados.replace('[' + tbl.tBodies[0].rows[i].myRow.one.value + ']', '');
					cCount++;
			}
		}
		if (checkedObjArray.length > 0) {
			eliminarFilas(checkedObjArray);
		} else {
			cuadroDialogo("Favor de seleccionar el periodo que desea eliminar de la lista", 1);
		}
	}
}

function eliminarFilas(rowObjArray){
	for (var i=0; i<rowObjArray.length; i++) {
			var rIndex = rowObjArray[i].sectionRowIndex;
			rowObjArray[i].parentNode.deleteRow(rIndex);
	}
	establecerClaseFila();
}

function establecerClaseFila(){
	var tbl = document.getElementById('tblPeriodosSeleccionados');
	var filas = tbl.tBodies[0].rows;
	for (var i=1; i<filas.length; i++) {
		filas[i].className = (i % 2)==0?'textabdatobs':'textabdatcla';
	}
}

function descargarCuentas(){
	document.Datos.action = document.Datos.action.replace('ventana=2', 'ventana=3');
	document.Datos.action = document.Datos.action.replace('ventana=4', 'ventana=3');
	document.Datos.submit();
}

function CambiarCuenta() {
	document.getElementById("cambioCuentaTXT").value = trim(document.getElementById("cambioCuentaTXT").value);
	var valorOriginal = "<%=request.getAttribute("cambioCuentaTXT")%>";
	var valorNuevo = document.getElementById("cambioCuentaTXT").value;
	var cuentasRelacionadas = obtieneCuentasRelacionadas();

	if(trim(valorNuevo) == null || trim(valorNuevo) == "") {
		return;
	} else if (trim(valorOriginal) != trim(valorNuevo)) {
		for (var i = 0; i < cuentasRelacionadas.length; i++) {
			if (trim(valorNuevo) == cuentasRelacionadas[i]) {
				document.Datos.action = "EstadoCuentaHistoricoServlet?ventana=4";
				document.Datos.submit();
				return;
			}
		}
		return;
	}
}

function CambiarTarjeta() {
	document.getElementById("cambioCuentaTXT").value = trim(document.getElementById("cambioCuentaTXT").value);
	var valorOriginal = "<%=request.getAttribute("cambioCuentaTXT")%>";
	var valorNuevo = document.getElementById("cambioCuentaTXT").value;
	var tarjetasRelacionadas = obtieneTarjetasRelacionadas();

	if(trim(valorNuevo) == null || trim(valorNuevo) == "") {
		return;
	} else if (trim(valorOriginal) != trim(valorNuevo)) {
		for (var i = 0; i < tarjetasRelacionadas.length; i++) {
			if (trim(valorNuevo) == tarjetasRelacionadas[i]) {
				document.Datos.action = "EstadoCuentaHistoricoServlet?ventana=4"
				document.Datos.submit();
				return;
			}
		}
		return;
	}
}

function validaPreviaADescarga(tipo) {
	var valorOriginal = "<%=request.getAttribute("cambioCuentaTXT")%>";
	var valorNuevo = document.getElementById("cambioCuentaTXT").value;
	var cuentasRelacionadas = "";
	if(tipo == "c"){
		cuentasRelacionadas = obtieneCuentasRelacionadas();
	}else{
		cuentasRelacionadas = obtieneTarjetasRelacionadas();
	}

	if(trim(valorNuevo) == null || trim(valorNuevo) == "") {
		cuadroDialogo('Por favor, introduzca una cuenta para proceder con la descarga del Estado de Cuenta.', 4);
		return false;
	} else if (trim(valorOriginal) != trim(valorNuevo)) {
		for (var i = 0; i < cuentasRelacionadas.length; i++) {
			if (trim(valorNuevo) == cuentasRelacionadas[i]) {
				document.Datos.action = document.Datos.action.replace('ventana=2', 'ventana=4');
				document.Datos.action = document.Datos.action.replace('ventana=3', 'ventana=4');
				return true;
			}
		}
		cuadroDialogo("El n\u00famero de cuenta debe formar parte de las cuentas " +
		"relacionadas a la secuencia de domicilio.\n Para consultar estas " +
		"cuentas de clic en el link del n\u00famero de secuencia de la pantalla." , 4);
		return false;
	} else {
		return true;
	}
}

function obtieneCuentasRelacionadas() {
	var cuentasRelacionadas = new Array();
	<%
	List<String> cuentasRelacionadas = (List<String>) request.getSession().getAttribute("cuentasRelacionadas");
	String cuentaTmp = null;
	String cuentaFormato = null;
	int i = 0;
	for (String obj : cuentasRelacionadas) {
		cuentaTmp = obj.trim();
		if (cuentaTmp.indexOf('B') == 0 && cuentaTmp.indexOf("BME") != 0) {
			cuentaFormato = "BME".concat(cuentaTmp.substring(1, cuentaTmp.length()));
		} else if (cuentaTmp.startsWith("0")){
			cuentaFormato = cuentaTmp.substring(1, cuentaTmp.length());
		} else {
			cuentaFormato = cuentaTmp;
		}
		%>
		cuentasRelacionadas[<%=i%>] = "<%=cuentaFormato.trim()%>";
		<%
		i++;
	}
	%>
	return cuentasRelacionadas;
}

function obtieneTarjetasRelacionadas() {
	var tarjetasRelacionadas = new Array();
	<%
	List<String> tarjetasRelacionadas = (List<String>) request.getSession().getAttribute("tarjetas");
	String tarjetaFormato = null;
	int j = 0;
	for (String obj : cuentasRelacionadas) {
		tarjetaFormato = obj.trim();
		%>
		tarjetasRelacionadas[<%=j%>] = "<%=tarjetaFormato.trim()%>";
		<%
		j++;
	}
	%>
	return tarjetasRelacionadas;
}

function inicio() {
	var error = "<%=codigoError%>";
	var mensajeError = "<%=mensajeError%>";
	if(error == "ERRORIN01") {
		cuadroDialogo("El n&uacute;mero de cuenta debe formar parte de las cuentas relacionadas a la secuencia " +
					"de domicilio. Para consultar estas cuentas de clic en el link del n&uacute;mero de secuencia de la pantalla",4);
	} else if(error == "ERRORIN00") {
		cuadroDialogo(mensajeError,4);
	}
}

function Regresar() {
	document.Datos.action = "EstadoCuentaHistoricoServlet?ventana=0";
	document.Datos.submit();
}


/**
 * Funcion para exportar Cuentas o Tarjetas
 */
function guardaArchivo(tipoExp) {
	document.Datos.action = "EstadoCuentaHistoricoServlet?ventana="+tipoExp;
	document.getElementById('tipoOp').value = tipoExp;
	document.Datos.submit();
}

</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onLoad="inicio();">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%
           if (request.getAttribute("MenuPrincipal")!= null) {
           out.println(request.getAttribute("MenuPrincipal"));
           }
    %>
   </td>
  </tr>
</table>

<%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
%>

<form  name="Datos" method="post" action="EstadoCuentaHistoricoServlet?ventana=2&formato=PDF">
	<input type="hidden" name="tipoOp" id="tipoOp" value="<%=request.getAttribute("tipoOp")%>" />
	<input type="hidden" name="valorOriginal" id="valorOriginal" value="<%=request.getAttribute("cambioCuentaTXT")%>" />
	<input type="hidden" name="tarjetas" id="tarjetas" value="<%=request.getSession().getAttribute("tarjetas")%>" />
	<p>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<table align="center">
					<input type="hidden" id="cuentaSeleccionada" name="cuentaSeleccionada" value="<%=request.getAttribute("cuentaSeleccionda")%>"/>
					<input type="hidden" id="cuentaTXT" name="cuentaTXT" value="<%=request.getAttribute("cambioCuentaTXT")%>"/>
					<input type="hidden" id="codigoCliente" name="codigoCliente" value="<%=request.getAttribute("codCliente")%>"/>
					<input type="hidden" id="numeroSecuencia" name="numeroSecuencia" value="<%=request.getAttribute("numeroSecuencia")%>"/>
					<input type="hidden" id="Cuentas" name="Cuentas" value="<%=request.getAttribute("cuentaFormatoOriginal")%>"/>
					<input type="hidden" id="periodosSoliciar" name="periodosSoliciar" value=""/>

					<tr id="idTrCuenta"
					style="display: <%="001".equals(request.getAttribute("tipoOp"))
					? "block"
					: "none"%>;">
					<input type="hidden"
						value='<%=request.getAttribute("segmentoDom")%>'
						name="hdSeqDomicilio" />
					<input type="hidden" value='<%=request.getAttribute("cuenta")%>'
						name="hdCuenta" />
					<input type="hidden"
						value='<%=request.getAttribute("codCliente")%>'
						name="hdCodCliente" id="hdCodCliente" />
					<input type="hidden" value='<%=request.getAttribute("domicilio")%>'
						name="hdDomicilio" />
					<input type="hidden"
						value='<%=request.getAttribute("fechaConfigValida")%>'
						name="hdFechaConfigValida" />
					<input type="hidden" value="N" name="hdACAP" />
					<td align="center">
						<table width="670" border="0" cellspacing="2" cellpadding="3">
							<thead>
								<tr>
									<td class="tittabdat" width="123" align="center">Seq</td>
									<td class="tittabdat" width="123" align="center">C&oacute;digo
										Cliente</td>
									<td class="tittabdat" width="123" align="center">Cuenta</td>
									<td class="tittabdat" width="423" align="center">Domicilio
										Registrado</td>
								</tr>
							</thead>
							<tbody>
								<tr class="textabdatobs">
									<td align="center" class="tabmovtexbol" align="center"><a
										href="#"
										title="Descargue las cuentas relacionadas a la secuencia de domicilio"
										onclick="javascript:guardaArchivo('EXPCTA');"> <%=request.getAttribute("segmentoDom")%>
									</a></td>
									<td align="center" class="tabmovtexbol" align="left">${codCliente}</td>
									<td align="center" class="tabmovtexbol" align="left">${cuenta}</td>
									<td align="left" class="tabmovtexbol" align="left">${domicilio}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr id="idTrTarjeta"
					style="display: <%="003".equals(request.getAttribute("tipoOp"))
					? "block"
					: "none"%>;">
					<td style="width: 50px">
          			</td>
					<td align="center">
						<table width="670" border="0" cellspacing="2" cellpadding="3">
							<input type="hidden" value='<%=request.getAttribute("credito")%>'
								name="hdTarjeta" />
							<input type="hidden"
								value='<%=request.getAttribute("contrato")%>' name="hdContrato" />
							<thead>
								<tr>
									<td class="tittabdat" width="123" align="center">Contrato</td>
									<td class="tittabdat" width="123" align="center">Cr&eacute;dito</td>
									<td class="tittabdat" width="123" align="center">Descripci&oacute;n</td>
									<td class="tittabdat" width="423" align="center">Domicilio
										Registrado</td>
								</tr>
							</thead>
							<tbody>
								<tr class="textabdatobs">
									<td align="center" class="tabmovtexbol" align="center"><a
										href="#"
										title="Descargue las tarjetas relacionadas a la tarjeta y contrato"
										onclick="javascript:guardaArchivo('EXPTARJ');"> <%=request.getAttribute("contrato")%>
									</a></td>
									<td align="center" class="tabmovtexbol" align="left">${credito}</td>
									<td align="center" class="tabmovtexbol" align="left">${descripcion}</td>
									<td align="left" class="tabmovtexbol" align="left">${domicilio}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</table>
				<table>
					<tr>
						<td colspan="4" class="texencconbol">
						<%=request.getAttribute("MENSAJE_PROBLEMA_DATOS") != null ? request.getAttribute("MENSAJE_PROBLEMA_DATOS") : "" %>
						</td>
					</tr>
				</table>
				<br>
				<table>
					<tr class="tabdatfonazu">
						<th class="tittabdat" style="display: <%="001".equals(request.getAttribute("tipoOp")) ? "block" : "none"%>;">Cambio de cuenta. </th>
						<th class="tittabdat" style="display: <%="003".equals(request.getAttribute("tipoOp")) ? "block" : "none"%>;">Cambio de tarjeta. </th>
					</tr>
					<tr>
						<td><input type="text" size="20" name="cambioCuentaTXT" id="cambioCuentaTXT" value="<%=request.getAttribute("cambioCuentaTXT")%>" onblur="<%="001".equals(request.getAttribute("tipoOp")) ? "CambiarCuenta" : "CambiarTarjeta" %>()" onkeypress="return noenter(event)"/></td>
					</tr>
				</table>
				</br>
				<table align="center">
					<tr class="tabdatfonazu">
						<th class="tittabdat"> Solicitud de Periodos hist&oacute;ricos en formato PDF. </th>
					</tr>
					<tr align="center">
				   		<td class="tabmovtex">
							<select id="slcPeriodos" name="slcPeriodos">
							<%
								EIGlobal.mensajePorTrace("Listando periodosRecientes",EIGlobal.NivelLog.DEBUG);
								List periodosRecientes = (List)request.getAttribute("periodosRecientes");
						        for(int contadorPeriodos = 0; contadorPeriodos < periodosRecientes.size(); contadorPeriodos++){
						        	String periodoTx = (String)periodosRecientes.get(contadorPeriodos);
						            if(periodoTx == null  || periodoTx.length() == 0){
						                continue;
						            }
									EIGlobal.mensajePorTrace("periodoReciente: ".concat(periodoTx),EIGlobal.NivelLog.DEBUG);
									String[] periodoTxSeparado = periodoTx.split(":::");
			
									if(periodoTxSeparado.length != 2){
										continue;
									}
									EIGlobal.mensajePorTrace("periodoTxSeparado[1]: ".concat(periodoTxSeparado[1]),EIGlobal.NivelLog.DEBUG);
									if ("003".equals(request.getAttribute("tipoOp")) && !periodoTxSeparado[1].contains("00000000@000")) {
			    			%>
							<option value="<%=periodoTxSeparado[1]%>"><%=periodoTxSeparado[0]%></option>
							<%
									}else if (!periodoTxSeparado[1].contains("00000000")){
										%>
											<option value="<%=periodoTxSeparado[1]%>"><%=periodoTxSeparado[0]%></option>
										<%
									}
								}
								EIGlobal.mensajePorTrace("Fin Lista periodosRecientes",EIGlobal.NivelLog.DEBUG);
							 %>
							</select>
						</td>
						<td class="tabmovtex">
							<a href="javascript:agregarPeriodoEnTabla('<%="001".equals(request.getAttribute("tipoOp")) ? "c" : "t" %>');"><img src="/gifs/EnlaceMig/gbo25290.gif" border="0"/></a>
						</td>
				  	</tr>
				</table>
				<table align="center" id="tblPeriodosSeleccionados" name="tblPeriodosSeleccionados">
					<tr class="tabdatfonazu">
						<th class="tittabdat" colspan="2"> Periodos a Solicitar </th>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" align="center">
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr>
				    	<td><a href="javascript:Regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar" border="0"/></a></td>
						<td><a href="javascript:eliminarSeleccionados();"><img src="/gifs/EnlaceMig/gbo25515.gif" alt="Eliminar" border="0"/></a></td>
						<td><a href="javascript:EnviarForma();"><img src="/gifs/EnlaceMig/Solicitar.gif" alt="Enviar" border="0"/></a></td>
				 	</tr>
				</table>
			</td>
		</tr>
	</table>
	</p>
</form>
</body>
</html>