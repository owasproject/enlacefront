<jsp:useBean id='facultadesAltaCtas' class='java.util.HashMap' scope='session'/>
<%@page import="EnlaceMig.*"%>


<html>
<head>
	<title>Alta de Empleados Nomina Interbancaria</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script type="text/javascript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
.componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
<%
if(request.getAttribute("ErroresEnArchivo")!=null)
 {
   System.out.println("Errores en archivo != null " + request.getAttribute("ErroresEnArchivo"));
   out.print(request.getAttribute("ErroresEnArchivo"));
 }
%>

function js_despliegaErrores(archivo)
 {
   ventanaInfo1=window.open('/Download/'+archivo,'errWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.focus();
 }

function cambiaTipoCuenta ()
{
	 var forma=document.AltaCuentas;
	 var valor = forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
		forma.strPlazaBN.value = "";
		forma.sucursalBN.value = "";
	 if (valor == "40") {
		forma.bancoBN.disabled = true;
		forma.cvePlazaBN.disabled = true;
		forma.sucursalBN.disabled = true; 
		forma.bancoBN.options[forma.bancoBN.selectedIndex].value = 0;
		forma.bancoBN.options[forma.bancoBN.selectedIndex].text = " Seleccione una cuenta ";
	 } else {
		forma.bancoBN.disabled = false;
		forma.cvePlazaBN.disabled = false;
		forma.sucursalBN.disabled = false; 
	 }
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function digitoVerificador(cuenta)
 {
    var pesos = "371371371371371371371371371";
    var DC = 0;
	var cuenta2 = cuenta.value.substring(0,17);
	//var cveBanco = claveBanco();

	for(i=0;i<cuenta2.length;i++)
	  DC += ( parseInt(cuenta2.charAt(i)) * parseInt(pesos.charAt(i))) % 10;
	DC = 10 - (DC % 10);
	if(DC == 10)	DC=0;
	  cuenta2 += DC;
	if((cuenta2) == cuenta.value)
	   return true;
	cuenta.focus();
	cuadroDialogo("La cuenta claBE no es correcta",3);
	return false;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Enviar()
 {
	var forma=document.AltaCuentas;
	var contador=0;

	for(j=0;j<forma.length;j++)
	  if(forma.elements[j].type=='checkbox')
		if(forma.elements[j].checked)
		  contador++;

	if(contador==0)
	 {
	   cuadroDialogo("Debe seleccionar al menos una opci&oacute;n",1);
	 }
	else
	 {
		var cadena="";
		for(x=1;x<forma.length;x++)
		 {
			if(forma.elements[x].type=="checkbox")
			 if(forma.elements[x].checked)
			   cadena+="1";
			 else
			   cadena+="0";
		 }
		forma.cuentasSel.value=cadena;
		forma.Modulo.value="3";
		forma.submit();
	 }

 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function Agregar()
 {
   var forma=document.AltaCuentas;
   var valRadio=-1;
   var cuenta="";
   var strTramaMB="";
   var strTramaBN="";
   var strTramaBI="";

   if(validaForma(forma))
    {
	   for(j=0;j<forma.length;j++)
		 if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
			if(forma.elements[j].checked)
			   valRadio=forma.elements[j].value;

	   if(valRadio==0)
		 {
		   cuenta=trimString(forma.numeroCuentaBN.value);
		   strTramaBN=trimString(forma.strTramaBN.value);
			if(strTramaBN.indexOf(cuenta)>=0)
			  {
				cuadroDialogo("La cuenta ya fue seleccionada.",3);
				return ;
			  }
		 }
	  textToUpper(forma);
      forma.action = "CatNomInterbRegistro";
	  forma.Modulo.value = "1";
	  forma.submit();
    }
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
 /* 
 Valida Banco y plaza
 */

 function validaBanco_Plaza(forma)
 {
	 var i=0;
	 var cuenta = parseInt(forma.numeroCuentaBN.value.substring(0,3),10);
	 i = forma.Bancos.value.indexOf("|" + cuenta + "|");
	 if (i == -1) {
		  forma.numeroCuentaBN.focus();
		  cuadroDialogo("El dato de banco en el numero de cuenta es incorrecto...",1);
		  return false;
	 }
	 return true;
 } 

 
function validaForma(forma)
 {
   /* Mismo Banco ********/
   var i=0;
   var j=0;
   var valRadio=-1;

   for(j=0;j<forma.length;j++)
     if(forma.elements[j].type=='radio' && forma.elements[j].name=='TipoAlta' )
	    if(forma.elements[j].checked)
	       valRadio=forma.elements[j].value;

   if(valRadio==-1)
	 {
	   cuadroDialogo("Debe seleccionar una opci&oacute;n",1);
	   return false;
	 }
   else
   /* Nacionales ********/
   if(valRadio==0)
	{
	  tipo=forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
	  banco=forma.bancoBN.options[forma.bancoBN.selectedIndex].value;
	  if(tipo==0)
		{
		  forma.tipoCuentaBN.focus();
		  cuadroDialogo("Debe seleccionar un tipo de cuenta",1);
		  return false;
		}
	  else
	   {
		 if(!validaCuenta(forma.numeroCuentaBN,"BN") || !validaTitularCuenta(forma.nombreTitularBN)) {
			 return false;
		 } else if (tipo=="02") {
			 if(banco==0)
			  {
				 forma.bancoBN.focus();
				 cuadroDialogo("Debe seleccionar un banco",1);
				 return false;
			  } else if (!isInteger(trimString(forma.sucursalBN.value))) {
					 forma.sucursalBN.focus();
					 cuadroDialogo("La sucursal del banco es num&eacute;rica",3);
					 return false;
			  } 
		   } 
	   }
    }
   else
   /* Importacion ********/
   if(valRadio==1)
	{
	   if(trimString(forma.Archivo.value)=="")
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Debe especificar el nombre del archivo",3);
		  return false;
		}

	   if(trimString(forma.strTramaBN.value)!="")
		{
		  forma.Archivo.focus();
		  cuadroDialogo("Se han registrado operaciones en l&iacute;nea, no puede importar archivos en este momento.",3);
		  return false;
		}
    }
   return true;
 }

function validaCuenta(cuenta,tipo)
 {
   if(trimString(cuenta.value)=="")
	 {
	   cuenta.focus();
	   cuadroDialogo("La cuenta es obligatoria",1);
	   return false;
	 }

	   if(isInteger(trimString(cuenta.value)))
		 {
		   if(tipo=="BN")
			 {
			   forma=document.AltaCuentas;
			   tipocuenta=forma.tipoCuentaBN.options[forma.tipoCuentaBN.selectedIndex].value;
			   if(tipocuenta=="40") /*** Cuenta ClaBE */
				 {
				   if(cuenta.value.length == 18)
					 {
					   if(!digitoVerificador(cuenta) || !validaBanco_Plaza(forma))
						 return false;
					 }
					else
					 {
						cuenta.focus();
						cuadroDialogo("La longitud de la cuenta no es correcta",3);
						return false;
					 }
				 } else if(tipocuenta=="02") /*** TDD - TDC */
				 {
				   if( !(cuenta.value.length == 16 || cuenta.value.length == 15) )
					 {
					   cuenta.focus();
					   cuadroDialogo("La longitud de la cuenta no corresponde con el tipo especificado",3);
					   return false;
					 }
				 }
			 }
		 }
		else
		 {
		   cuenta.focus();
		   cuadroDialogo("La cuenta debe ser num&eacute;rica.",3);
		   return false;
		 }
	 
	return true;
 }

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especiales(Txt1)
{
	//VSWF BMB se modific� la cadena strEspecial, agregando & como car�cter v�lido
   var strEspecial=" &ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("En el titular o descripcion de la cuenta, no se permiten caracteres especiales",3);
	   return false;
	 }
   return true;
}


/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
function especialesBI(Txt1,campo)
{
   var strEspecial=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,";
   var cont=0;

    for(i0=0;i0<Txt1.value.length;i0++)
	 {
       Txt2 = Txt1.value.charAt(i0);
	   if( strEspecial.indexOf(Txt2)!=-1 )
		 cont++;
     }
	if(cont!=i0)
	 {
	   Txt1.focus();
	   cuadroDialogo("No se permiten caracteres especiales para " + campo,3);
	   return false;
	 }
   return true;
}

function validaBanco(banco)
{
  if(trimString(banco.value).length==0)
	{
	  banco.focus();
	  cuadroDialogo("El banco es obligatorio",3);
	  return false;
 	}
  return true;
}



function validaTitularCuenta(titular)
 {
   if(trimString(titular.value).length==0)
	{
	  titular.focus();
	  cuadroDialogo("La descripci&oacute;n o titular de la cuenta es obligatoria",3);
	  return false;
  	}
   if(!especiales(titular))
	  return false
   return true;
 }

function Limpiar()
 {
	document.AltaCuentas.reset();
 }

function TraerPlazas()
{
	ventanaInfo2=window.open('EI_Plazas','Plazas','width=250,height=350,toolbar=no,scrollbars=no');
	ventanaInfo2=window.open('/Download/EI_Plazas.html','Plazas','width=250,height=350,toolbar=no,scrollbars=no');
	ventanaInfo2.focus();
}

function Selecciona(S)
{
  var v1="";
  var v2="";
  var v3="";

  var sforma=document.AltaCuentas;

  if(S==1)
	{
     v1=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].value;
	  v2=ventanaInfo2.document.Forma.SelPlaza.options[ventanaInfo2.document.Forma.SelPlaza.selectedIndex].text;
     if(v1)
		sforma.cvePlazaBN.value=v1;
	  if(v2)
		sforma.strPlazaBN.value=v2;
	}

}


function isInteger(num)
{
  var TemplateI = /^\d*$/;	 /*** Formato de numero entero sin signo */
  return TemplateI.test(num); /**** Compara "num" con el formato "Template" */
}

<%= request.getAttribute("newMenu")%>

function msgCifrado(){
	try {
		var mensajeCifrado = document.AltaCuentas.mensajeCifrado.value;
		if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null" ) {
			var arreglo = mensajeCifrado.split("|");
            var msj = arreglo[1];
            var tipo = arreglo[0];
           cuadroDialogo( msj ,tipo);
		}
	} catch (e){};
}

/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
</script>
</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="msgCifrado();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <%= request.getAttribute("MenuPrincipal")%>
    </TD>
  </TR>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>

<form name=transferencia>
 <input type=hidden name="Descripcion">
</form>

<FORM  NAME="AltaCuentas" ENCTYPE="multipart/form-data" method=post action="CatNomInterbRegistro">
 <INPUT TYPE="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>">
 <table width=760 border=0 cellpadding=0 cellspacing=0 >
 <tr>
  <td>

  <table ALIGN=center border=0 cellpadding=2 cellspacing=3 >

 
   <%
   if (((Boolean) facultadesAltaCtas.get ("AltaCtasBN")).booleanValue())
    {
   %>
   <!-- ****************************************************************************************************** //-->
   <!--  Otros Bancos //-->
   <!-- ****************************************************************************************************** //-->
   <tr>
     <td colspan=4 class="tittabdat"><INPUT TYPE="radio" value=0 NAME="TipoAlta" checked>&nbsp;<b>Cuentas de otros bancos Nacionales</b></td>
   </tr>

   <tr>
     <td class='textabdatcla' colspan=4 width='100%'>
       <table border=0 class='textabdatcla' cellspacing=3 cellpadding=2 >

	    <tr>
		 <td class="tabmovtex" width=10><br></td>
		 <td class="tabmovtex11" >Tipo de cuenta</td>
		 <td class='tabmovtex'>
		   <SELECT NAME=tipoCuentaBN class="componentesHtml" onchange="cambiaTipoCuenta();">
		   	 <option value=0 class='tabmovtex'> Seleccione un tipo </OPTION>
		     <option value='40'>Cuenta CLABE</option>
			 <option value='02'>Tarjeta de Cr&eacute;dito/D&eacute;bito</option>
		   </SELECT>
		 </td>
		</tr>

		<tr>
		  <td class="tabmovtex" width=10><br></td>
		  <td class='tabmovtex11' width=100>N&uacute;mero de Cuenta:</td>
		  <td class='tabmovtex'><INPUT TYPE=text class="componentesHtml" SIZE=25 MAXLENGTH=22 NAME=numeroCuentaBN></td>
		</tr>

		<tr>
		 <td class="tabmovtex" width=10><br></td>
	     <td class='tabmovtex11'>Nombre del Titular:</td>
		 <td class='tabmovtex'><INPUT TYPE=text class="componentesHtml" SIZE=25 MAXLENGTH=40 NAME=nombreTitularBN></td>
	    </tr>

		<tr>
		 <td class="tabmovtex" width=10><br></td>
         <td class='tabmovtex11'>Banco:</td>
          <td class='tabmovtex'>
		   <SELECT NAME=bancoBN class="componentesHtml"><option value=0 class='tabmovtex'> Seleccione un Banco </OPTION>
		       <%
			   String comboBancos="";
		       if(request.getAttribute("comboBancos")!=null)
				  comboBancos=(String)request.getAttribute("comboBancos");
			   %>
			   <%=comboBancos%>
		   </SELECT>
		  </td>
		</tr>

  	    <tr>
		 <td class="tabmovtex" width=10><br></td>
	     <td class='tabmovtex11'>Plaza:</td>
		 <td class='tabmovtex'>
			<INPUT TYPE=hidden NAME=cvePlazaBN>
			<INPUT TYPE=text class="componentesHtml" SIZE=25 MAXLENGTH=40 NAME=strPlazaBN onFocus="blur();">&nbsp;<a href='javascript:TraerPlazas();'><img border=0 src='/gifs/EnlaceMig/gbo25420.gif'></a></td>
	    </tr>

		<tr>
		 <td class="tabmovtex" width=10><br></td>
	     <td class='tabmovtex11'>Sucursal:</td>
		 <td class='tabmovtex'>
			<INPUT TYPE=text class="componentesHtml" SIZE=15 MAXLENGTH=5 NAME=sucursalBN>&nbsp;</td>
	    </tr>


        <tr>
         <td class="tabmovtex" colspan=3 ><br></td>
        </tr>

       </table>
     </td>
    </tr>

    <tr>
     <td class="tabmovtex" bgcolor=white colspan=4><br></td>
    </tr>
    <% 	String listaBancos = "";
    	if(request.getAttribute("listaBancos")!=null) {
    		listaBancos = (String)request.getAttribute("listaBancos");
    	}
    %>
 <td><INPUT TYPE=hidden NAME="Bancos" VALUE="<%=listaBancos%>"></td>

   <%
	}
   %>




   <%
    if (((Boolean) facultadesAltaCtas.get ("AltaCtasBN")).booleanValue())
     {
   %>
   
   <!-- ****************************************************************************************************** //-->
   <!-- Importar Archivo //-->
   <!-- ****************************************************************************************************** //-->
	  <tr>
		<td class='tittabdat' colspan=4><INPUT TYPE='radio' value=1 NAME="TipoAlta" >&nbsp;<b>Importar cuentas desde Archivo</b></td>
	  </tr>

	  <tr>
	   <td class='textabdatcla' colspan=4>
		<table border=0 class='textabdatcla' cellspacing=3 cellpadding=2 >
		 <tr>
		  <td class="tabmovtex" width=10><br></td>
		  <td class="tabmovtex11">Importar cuentas desde archivo &nbsp; <INPUT TYPE='file' NAME='Archivo' class="componentesHtml" value=' Archivo '></td>
		 </tr>
		 <tr>
		  <td colspan=2 class="tabmovtex"><br></td>
		 </tr>
		</table>
	   </td>
	  </tr>
   <%
    }
   %>

 </table>

 <br>
 <table align=center border=0 cellspacing=0 cellpadding=0>
    <tr>
     <td><A href = "javascript:Agregar();" border = 0><img src = "/gifs/EnlaceMig/gbo25290.gif" border=0></a></td>
     <td><A href = "javascript:Limpiar();" border = 0><img src = "/gifs/EnlaceMig/gbo25250.gif" border=0></a></td>
    </tr>
  </table>


   </td>
  </tr>
 </table>

 <%
  String Modulo="0";

  String strTramaBN="";

  String tablaBN="";

  if(request.getAttribute("Modulo")!=null)
    Modulo=(String)request.getAttribute("Modulo");
  if(request.getAttribute("strTramaBN")!=null)
    strTramaBN=(String)request.getAttribute("strTramaBN");
  if(request.getAttribute("tablaBN")!=null && !request.getAttribute("tablaBN").equals(""))
    tablaBN=(String)request.getAttribute("tablaBN")+ "<br><br>";

 %>


<table width=760 border=0 cellpadding=0 cellspacing=0 >
 <tr>
  <td width="100%"><%=tablaBN%></td>
 </tr>
</table>

 <input type=hidden name=Modulo value="<%=Modulo%>">
 <input type=hidden name=strTramaBN value="<%=strTramaBN%>">
 <input type=hidden name=cuentasSel >

 <%if(Modulo.trim().equals("1"))
  {
 %>
 <table width=760 border=0 cellspacing=0 cellpadding=0>
    <tr>
     <td align=center><A href = "javascript:Enviar();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a></td>
    </tr>
  </table>
  <%
   }
  %>
 </form>

</body>
</HTML>