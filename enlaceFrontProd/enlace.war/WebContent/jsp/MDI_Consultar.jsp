<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<html>
<head>
	<title>Consultas Deposito Interbancario</TITLE>
<script type="text/javascript"> 
	var contextPath = "${pageContext.request.contextPath}";
</script> 
<script src="/EnlaceMig/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="/EnlaceMig/json3.min.js" type="text/javascript"></script>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">

  var Indice=0;

  var registrosTabla=new Array();
  var camposTabla=new Array();
  var totalCampos=0;
  var totalRegistros=0;

function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/

<% if(request.getAttribute("VarFechaHoy")!= null) out.print(request.getAttribute("VarFechaHoy")); %>
  diasInhabiles = '<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;

   if(verificaRadios())
    {
	  msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	  msg.focus();
	}
 }

function Actualiza()
 {
   if(Indice==0)
     document.buscar.FechaIni.value=Fecha[Indice];
   else
     document.buscar.FechaFin.value=Fecha[Indice];
 }
/******************************************************************************************************* 
 *			INICIO BLOQUE CALENDAR CON 365 DIAS 
 *******************************************************************************************************/
	
	//Imprime arreglo de fechas con 365 dias enviado desde el servlet
	<%
		if(request.getAttribute("VarFechaHoy")!= null)
			out.print(request.getAttribute("VarFechaHoy"));
	%>
	 
	//Variable Indice para actualizar Calendario con fecha seleccionada
		var Indice=0;
	 
	 //FUNCION PARA ACTUALZIAR LA CAJA DETEXTO CON LA FECHA SELECCIONADA EN CALENDAR 
	 function Actualiza()
	 {
	     if(Indice==0)
			document.buscar.FechaIni.value=Fecha[Indice];
		 if(Indice==1)
			document.buscar.FechaFin.value=Fecha[Indice];
	 }
	
	//FUNCION PARA INVOCAR CALENDAR CON 365 DIAS
	 function SeleccionaFecha(ind){
		var m = new Date()
		Indice = ind;
	    n = m.getMonth();
	    
	    if(verificaRadios())
	    {
		 	msg = window.open("/EnlaceMig/EI_CalendarioLn.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	    	msg.focus();
		}
	}
	
/******************************************************************************************************* 
 *			FIN BLOQUE CALENDAR CON 365 DIAS 
 *******************************************************************************************************/

function Enviar()
 {
   if(VerificaFechas())
     document.buscar.submit();
 }



function exportarConsulta()
 {

    document.tabla.Modulo.value="3";
     document.tabla.submit();
 }


function verificaRadios(){
   if(($("#tipoMonedaMXN").is(':checked') || $("#tipoMonedaUSD").is(':checked')) && $("#BusquedaHOY").is(':checked')){
   		cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",3);
   		return false;
   } else {
   		return true;
   }
   /*if(document.buscar.elements[1].checked==true)
     return true;
   cuadroDialogo("La fecha no se puede cambiar para consultar los Movimientos del d&iacute;a",3);
   return false;*/
}

function cambiaFechas(ele)
 {
   if(ele.value=='HIS')
    {
	  document.buscar.FechaIni.value="<%= request.getAttribute("FechaPrimero") %>";
	  document.buscar.FechaFin.value="<%= request.getAttribute("FechaAyer") %>";
	}
   else
	{
	  document.buscar.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
	  document.buscar.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
	}
 }

function VerificaFechas()
 {
    for(i1=0;i1<document.buscar.length;i1++)
     if(document.buscar.elements[i1].type=='radio')
      if(document.buscar.elements[i1].checked==true)
        TipoB=document.buscar.elements[i1].value;

  if(TipoB=='HIS')
   {
	  	var fechaIni = document.buscar.FechaIni.value;
	   	var fechaFin = document.buscar.FechaFin.value;

	  	dia1[0] = fechaIni.substring(0,2);
	   	mes1[0] = fechaIni.substring(3,5);
	   	anio1[0] = fechaIni.substring(6,10);

	   	dia1[1] = fechaFin.substring(0,2);
	   	mes1[1] = fechaFin.substring(3,5);
	   	anio1[1] = fechaFin.substring(6,10);

	 if(anio1[0]==anio1[1]){
     	if(mes1[0]>mes1[1])
      	{
        	//alert("Error: La fecha Final no debe ser menor a la inicial.");
			cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
        	return false;
      	}
     	if(mes1[0]==mes1[1])
      		if(dia1[0]>dia1[1])
       	{
        	 //alert("Error: La fecha Inicial debe ser menor a la final.");
		 	cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.",3);
         	return false;
       	}
	 }else if(anio1[0]>anio1[1]){
		 cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
         return false;
	 }
   }
  return true;
 }

function BotonPagina(sigant)
 { document.tabla.Modulo.value="1";

   document.tabla.Pagina.value="" + sigant;//tot;
   document.tabla.submit();
 }

/********************************************************************************/
// Funciones para deplegar la ventana con la informacion de la cuenta

function despliegaDatos(campo2)
 {
   // cadena de Transferencias,indice del registro

   param1="&TransEnv="+formateaAmp(campo2);//csa
   param2="Indice=0";
   param5="&NumContrato="+document.tabla.NumContrato.value+"&NomContrato="+formateaAmp(document.tabla.NomContrato.value);
   param6="&NumUsuario="+document.tabla.NumUsuario.value+"&NomUsuario="+formateaAmp(document.tabla.NomUsuario.value);
   param7="&ClaveBanco="+document.tabla.ClaveBanco.value;
   param8="&tipoMoneda="+document.tabla.tipoMoneda.value;
   param1=formatea(param1);
   param5=formatea(param5);
   param6=formatea(param6);
   param7=formatea(param7);
   param8=formatea(param8);

   web_application=document.tabla.web_application.value;
   //alert("/NASApp/"+web_application+"/MDI_Comprobante"+param2+param1+param5+param6+param7);
   // vswf:meg cambio de NASApp por Enlace 08122008
   vc=window.open("/Enlace/"+web_application+"/MDI_Comprobante?"+param2+param1+param5+param6+param7+param8,'trainerWindow','width=470,height=500,toolbar=no,scrollbars=yes');
   vc.focus();
 }
//CSA-----------------
function formateaAmp(para)
{
  var formateado="";
  var car="";

  for(a2=0;a2<para.length;a2++)
   {
	  if(para.charAt(a2)=='&')
	   car="%26";
	  else
	   car=para.charAt(a2);

	  formateado+=car;
	}
  return formateado;
}
//------------------
function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
	  if(para.charAt(a2)==' ')
	   car="+";
	  else
	   car=para.charAt(a2);

	  formateado+=car;
	}
   return formateado;
 }

function numeroRegistros(campo2)
    {
      var i=0;
	  totalRegistros=0;

      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    var vartmp="";
	var i=0;

	strCuentas=campo2;
    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
    separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
              vartmp=regtmp.substring(0,regtmp.indexOf('|'));
              camposTabla[i][j]=vartmp;
              if(regtmp.indexOf('|')>0)
                regtmp=regtmp.substring(regtmp.indexOf('|')+1,regtmp.length);
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }

function nueva(indice)
 {
   var TramaGlobal="";
   var i=0;

   for(i=0;i<=totalCampos;i++)
     TramaGlobal+=camposTabla[indice][i]+"|";
   TramaGlobal+=" @";
   document.tabla.TransEnv.value=TramaGlobal;
 }

 function validaNavegador(formObj) {
 	var mensaje = 'Al momento de ingresar a \u00e9ste sitio, por su seguridad se cerrar\u00e1 la sesi\u00f3n abierta con Banco Santander M\u00e9xico S.A., Instituci\u00f3n de Banca M\u00faltiple, Grupo Financiero Santander M\u00e9xico, en caso de estar de acuerdo de click en Aceptar';
 	var bandera=true;
	if((navigator.userAgent).indexOf('MSIE')!=-1) {
		var aux=navigator.userAgent.substring((navigator.userAgent).indexOf('MSIE')+4,(navigator.userAgent).indexOf('MSIE')+9);
		var arreglo = aux.split('.');
		if(!(arreglo[0]>=10)){//valida que sea la version 10 o superior
		bandera=false;
		}
	}else if((navigator.userAgent).indexOf('Firefox')!=-1) {
		if(!(navigator.userAgent.substring((navigator.userAgent).indexOf('Firefox')+8,(navigator.userAgent).indexOf('Firefox')+10)>=23)){//valida que sea la version 23 o superior
			bandera=false;
		}
	}else if((navigator.userAgent).indexOf('Chrome')!=-1) {
		if(!(navigator.userAgent.substring((navigator.userAgent).indexOf('Chrome')+7,(navigator.userAgent).indexOf('Chrome')+9)>=28)){//valida que sea la version 28 o superior
			bandera=false;
		}
	}else {
		//alert('es otro navegador');
	}
	if(!bandera){
		mensaje='El portal de Banco de M\u00e9xico podr\u00eda no funcionar correctamente debido a la versi\u00f3n de su navegador, en caso de estar de acuerdo de click en Aceptar, Al momento de ingresar a \u00e9ste sitio, por su seguridad se cerrar\u00e1 la sesi\u00f3n abierta con Banco Santander M\u00e9xico S.A., Instituci\u00f3n de Banca M\u00faltiple, Grupo Financiero Santander M\u00e9xico';
	}
		if(!confirm(mensaje)){
			return false;
        }
	}

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %>
   </td>
  </tr>
</table>

<%= request.getAttribute("Encabezado") %>

<FORM  NAME="buscar" method=post onSubmit="return VerificaFechas();" action="MDI_Consultar">
  <td align=center><% if(request.getAttribute("Tabla")!= null) out.print(request.getAttribute("Tabla")); %></td>
  <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
  <input type=hidden name=Pagina value=0>
</FORM>

<FORM  NAME="tabla" method=post action="MDI_Consultar">
 <% if(request.getAttribute("Registros")!= null) out.print(request.getAttribute("Registros")); %>
 <% if(request.getAttribute("Botones")!= null) out.print(request.getAttribute("Botones")); %>
 <input type=hidden name=Modulo value=<%= request.getAttribute("Modulo") %>>
 <input type=hidden name=Pagina value=<%= request.getAttribute("Pagina") %>>
 <input type=hidden name=Numero value=<%= request.getAttribute("Numero") %>>
 <input type=hidden name=FechaIni value='<%= request.getAttribute("FechaIni") %>'>
 <input type=hidden name=FechaFin value='<%= request.getAttribute("FechaFin") %>'>
 <input type=hidden name=Busqueda value='<%= request.getAttribute("Busqueda") %>'>
 <input type=hidden name=NumContrato value='<%= request.getAttribute("NumContrato") %>'>
 <input type=hidden name=NomContrato value='<%= request.getAttribute("NomContrato") %>'>
 <input type=hidden name=NumUsuario value='<%= request.getAttribute("NumUsuario") %>'>
 <input type=hidden name=NomUsuario value='<%= request.getAttribute("NomUsuario") %>'>
 <input type=hidden name=ClaveBanco value='<%= request.getAttribute("ClaveBanco") %>'>
 <input type=hidden name=tipoMoneda value='<%= request.getAttribute("tipoMoneda") %>'>

  <%
  String web_application = "";
  if(session.getAttribute("web_application")!=null)
	web_application = (String)session.getAttribute("web_application");
  else if (request.getAttribute("web_application")!= null)
	web_application= (String)request.getAttribute("web_application");
  web_application=web_application.trim();
  if(web_application.equals(""))
    web_application="enlaceMig";
  %>

  <input type=hidden name="web_application" value = '<%=web_application%>'>
</FORM>

</body>
</html>