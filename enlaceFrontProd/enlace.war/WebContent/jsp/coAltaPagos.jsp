<!--%@ page import="EnlaceMig.Archivos"%>
< %@ page import="EnlaceMig.claveTrans"%>
< %@ page import="EnlaceMig.CombosConf"%-->
<%@ page import="java.util.*"%>
<!--%@ page import="EnlaceMig.*" %-->
<%@page import="mx.altec.enlace.bo.Documento"%>
<%@page import="mx.altec.enlace.bo.ArchivoCfmg"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.dao.ProveedorConf"%>
<jsp:useBean id = "arch_combos" class = "mx.altec.enlace.bo.servicioConfirming" scope="request"/>
<jsp:useBean id = "arch_prov" class = "mx.altec.enlace.bo.Proveedor" scope="request"/>
<jsp:useBean id = "baseServlet" class = "mx.altec.enlace.servlets.BaseServlet" scope="request"/>

<%!

String corta(String cadena)
	{
	final int largo = 30;
	if(cadena.length()>largo)
	    cadena = cadena.substring(0,largo);
	return cadena;
	}
%>

<%
	int a, b, total;
String aux;

String parAccion = baseServlet.getFormParameter(request,"accion");
EIGlobal.mensajePorTrace("coAltaPagos - parAccion:" + parAccion, EIGlobal.NivelLog.DEBUG);
if(parAccion.equals("3")) parAccion = "1";

Documento doc = (Documento)request.getAttribute("documento");
if(doc == null) doc = new Documento();
//jgarcia Inicio - Stefanini - Obtenci�n de la cuenta cargo para hacer el filtro al momento de obtener los proveedores
String cuentaCargo = (String)session.getAttribute("cuentaCargo");
if(cuentaCargo == null)
	cuentaCargo = "";

//jgarcia 09/Oct/2009 Obtencion de la divisa
String cveDivisa = (String)session.getAttribute("divisaProv");
String descDivisa = (String)session.getAttribute("descDivisa");

EIGlobal.mensajePorTrace("coAltaPagos - cuentaCargo:" + cuentaCargo,EIGlobal.NivelLog.DEBUG);
String ArchivoResp = arch_combos.coMtoProv_envia_tux(1,"CFAC","2EWEB","",cuentaCargo);
EIGlobal.mensajePorTrace("coAltaPagos - ArchivoResp:" + ArchivoResp,EIGlobal.NivelLog.DEBUG);
//jgarcia Fin
Vector ls_prov = null;
Vector cv_trans = null;
Vector tipo_cta = null;
//jgarcia 09/Oct/2009
//Operaciones para generar el vector con las claves del proveedor[cv_prov] para clave del proveedor
//y para guardar las descripciones de las tipos de cuenta[desc_tipo_cta].
Vector cv_prov = null;
Vector desc_tipo_cta = null;

if(ArchivoResp != null)
	{
	ls_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",1);
	cv_prov  = arch_prov.coMtoProv_Leer(ArchivoResp,"9",2);
	cv_trans = arch_prov.coMtoProv_Leer(ArchivoResp,"9",3);
	tipo_cta = arch_prov.coMtoProv_Leer(ArchivoResp,"9",4);	
	}

if(ls_prov != null)
	{
	total = ls_prov.size();
	String strA = null, strB = null;
	for(a=0;a<total-1;a++)
		for(b=a+1;b<total;b++)
	{
	strA = (String)ls_prov.get(a);
	strB = (String)ls_prov.get(b);
	if(strA.compareTo(strB)>0)
		{
		ls_prov.set(a,strB);
		ls_prov.set(b,strA);
		strA = (String)cv_trans.get(a);
		strB = (String)cv_trans.get(b);
		cv_trans.set(a,strB);
		cv_trans.set(b,strA);

		strA = (String)cv_prov.get(a);
		strB = (String)cv_prov.get(b);
		cv_prov.set(a,strB);
		cv_prov.set(b,strA);

		strA = (String)tipo_cta.get(a);
		strB = (String)tipo_cta.get(b);
		tipo_cta.set(a,strB);
		tipo_cta.set(b,strA);
		}
	}
	}

	if(tipo_cta != null){
	   desc_tipo_cta = new Vector(tipo_cta.size());
	   total = tipo_cta.size();
	   for(a = 0; a < total; a++){
	   	  int tipoCuenta;
	   	  try{
		  	tipoCuenta = Integer.parseInt((String)tipo_cta.get(a));
		  }catch(NumberFormatException e){
		  	tipoCuenta = 0;
		  }
		  EIGlobal.mensajePorTrace("coAltaPagos - desc_tipo_cta:" + (String)tipo_cta.get(a),EIGlobal.NivelLog.DEBUG);
		  switch(tipoCuenta){
		  	case 1:
		  		desc_tipo_cta.add(a, ProveedorConf.FORMA_PAGO_DESC_MISMO.replace("Transferencia ", ""));
		  		break;
		  	case 2:
		  		desc_tipo_cta.add(a, ProveedorConf.FORMA_PAGO_DESC_OTRO.replace("Transferencia ", ""));
		  		break;
		  	case 5:
		  		desc_tipo_cta.add(a, ProveedorConf.FORMA_PAGO_DESC_INT.replace("Transferencia ", ""));
		  		break;
		  	default:
		  		desc_tipo_cta.add(a, "");
		  		break;
		  }
	   }
	}

String[] valuesListaProveedor = new String[ls_prov.size()];
String[] textListaProveedor = new String[ls_prov.size()];
for(int count=0; count<ls_prov.size(); count++){
	valuesListaProveedor[count] = (String)cv_trans.get(count) + "|" + (String)tipo_cta.get(count);
	textListaProveedor[count] = corta((String)ls_prov.get(count)) + "|" + corta((String)cv_prov.elementAt(count))
								+ "|" + desc_tipo_cta.elementAt(count);
}
session.removeAttribute("listaProveedor");
session.removeAttribute("valuesListaProveedor");
session.removeAttribute("textListaProveedor");
session.setAttribute("listaProveedor",ls_prov);
session.setAttribute("valuesListaProveedor",valuesListaProveedor);
session.setAttribute("textListaProveedor",textListaProveedor);

//
ArchivoCfmg objArchivo = (ArchivoCfmg)session.getAttribute("objArchivo");
EIGlobal.mensajePorTrace("coAltaPagos - objArchivo:" + objArchivo.toString(),EIGlobal.NivelLog.DEBUG);
//
StringBuffer arregloFacturas = new StringBuffer();
java.util.Vector tramasFacturas = objArchivo.tramasFacturas();
total = tramasFacturas.size();
for(a=0;a<total;a++) {arregloFacturas.append(", '" + tramasFacturas.get(a) + "'");}
arregloFacturas.delete(0,2);

//
String hoy = "";
GregorianCalendar fecha = new GregorianCalendar();
aux = "" + fecha.get(Calendar.DATE); if(aux.length()<2) aux = "0" + aux; hoy += aux + "/";
aux = "" + (fecha.get(Calendar.MONTH)+1); if(aux.length()<2) aux = "0" + aux; hoy += aux + "/";
hoy += "" + fecha.get(Calendar.YEAR);


//
StringBuffer arregloFechas = new StringBuffer();
java.util.Vector fechas = objArchivo.tramasVencimientoFacturas();
total = fechas.size();
for(a=0;a<total;a++) arregloFechas.append(", '" + fechas.get(a) + "'");
arregloFechas.delete(0,2);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal;"%>
<HTML>
<HEAD>

	<TITLE> Datos del archivo</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript"    src="/EnlaceMig/scrImpresion.js"></script>
	<script language="javascript"    src="/EnlaceMig/cuadroDialogo.js"></script>
	<!-- script language = "JavaScript"  SRC="/EnlaceMig/Bitacora.js"></script -->
	<SCRIPT LANGUAGE="Javascript">
	javascript:window.history.forward(1);

	function MM_preloadImages()
	{
		var d=document;
		if(d.images)
		 {
		   if(!d.MM_p)
			  d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
		for(i=0; i<a.length; i++)
		    if (a[i].indexOf("#")!=0)
			   {
			     d.MM_p[j]=new Image;
		         d.MM_p[j++].src=a[i];
			   }
		   }
	}

	function MM_swapImgRestore()
	  {
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
			x.src=x.oSrc;
	}

	function MM_findObj(n, d) {
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++)   x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() {
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

	}

	<%=request.getAttribute( "newMenu" )%>

	// ------------------------------------------------------------------

	var numDoctoActual = <%=parAccion.equals("2")?("'" + doc.getproveedor() +
		"|" + doc.gettipo() + "|" + doc.getnumdocto() + "'"):("''")%>;
	var importeDoctoActual = <%=parAccion.equals("2")?("'" + doc.getimporte_neto() + "'"):("''")%>;
	var naturalezaDoctoActual = <%=parAccion.equals("2")?("'" + doc.getnaturaleza() + "'"):("''")%>;
	<%if(doc.gettipo() > 2) {%>
	var asociadaDoctoActual = "<%=doc.getproveedor() + "|1|" + doc.getfactura_asociada()%>";
	<%}%>

	/* jgarcia - 07/Oct/2009
	 * Se elimina la utilizaci�n de FormaAplica, Naturaleza y Asociado, asi como la validaci�n para
	 * los casos cuando el tipo de documento es mayor que 2 ya que en este momento solo
	 * se utilizan [1 - Factura] y [2 - Otros]
	*/
	function inicia(){
		if(<%=parAccion.equals("2")%>)
		{
			var provLst = document.AltaDePagos.Proveedor;
			// var natLst = document.AltaDePagos.Naturaleza;
			// var asoLst = document.AltaDePagos.Asociado;
			var tipoLst = document.AltaDePagos.TipoDocumento;

			//NumeroDocumento
			document.AltaDePagos.NumeroDocumento.value= "<%=doc.getnumdocto()%>";
			//ImporteDocumento
			document.AltaDePagos.ImporteDocumento.value= "<%=doc.getimporte_neto()%>";
			//FechaEmision;
			document.AltaDePagos.FechaEmision.value= "<%=doc.getfch_emis()%>";
			//FechaVencimiento;
			document.AltaDePagos.FechaVencimiento.value= "<%=doc.getfch_venc()%>";
			//Proveedor
			selecciona(provLst,"<%=doc.getproveedor()%>|<%=doc.gettipoCuenta()%>");
			cambioProveedor();
			//TipoDocumento
			selecciona(tipoLst,"<%=doc.gettipo()%>");

			//Agregado por HGV para proyecto Ley de Transparencia II
			//Concepto
			document.AltaDePagos.Concepto.value = "<%=doc.getconcepto()%>";
			//Referencia
			document.AltaDePagos.Referencia.value = "<%=doc.getreferencia()%>";

			// var aplicacion = document.AltaDePagos.FormaAplica.value = "";
			// Value en FormaAplica [doc.getaplicacion()]
			//alert ("APLICACION = >" +aplicacion+ "<");

			// if(aplicacion=="2")
			//   document.AltaDePagos.FormaAplica[1].checked=true;

			<%//if(doc.gettipo() > 2){%>
					//Asociado
					//selecciona(asoLst,"");
					//Segundo parametro de la funcion selecciona [doc.getfactura_asociada()]
					//Naturaleza
					//selecciona(natLst,"");
					//Segundo parametro de la funcion selecciona [doc.getnaturaleza()]

					//tipoLst.disabled = true;
					//provLst.disabled = true;
			<%//}%>
		}
	}

	// ---
	function regresar()
	{
		document.AltaDePagos.accion.value = '0';
		document.AltaDePagos.submit();
	}

	// ---
	/* jgarcia - 07/Oct/2009
	 * Se elimina la utilizaci�n de FormaAplica, Naturaleza y Asociado, asi como la validaci�n para
	 * los casos cuando el tipo de documento es mayor que 2 ya que en este momento solo
	 * se utilizan [1 - Factura] y [2 - Otros]
	*/
	function aceptar(){
		var correctoOp = true;
		var provLst = document.AltaDePagos.Proveedor;
		// var natLst = document.AltaDePagos.Naturaleza;
		// var asoLst = document.AltaDePagos.Asociado;
		var tipoLst = document.AltaDePagos.TipoDocumento;
		var provVal = trim(provLst.options[provLst.selectedIndex].value);

		// var apliLst = provVal.substring(provVal.indexOf("|") + 1);
		var provNom = provVal.substring(0,provVal.indexOf("|"));

		//alert ("Proveedor = >" +provVal+ "<");
		//alert ("Tipo Aplicaci�n = >" +apliLst+ "<");
		//alert ("Proveedor = >" +provNom+ "<");

		var numdocumento = provNom + "|" +
			trim(tipoLst.options[tipoLst.selectedIndex].value) + "|" +
			trim(document.AltaDePagos.NumeroDocumento.value);

		var facturaAsociada = provNom + "|1|";
			/* + trim(asoLst.options[asoLst.selectedIndex].value);*/

		/*if(document.AltaDePagos.FormaAplica[0].checked==true)
		  valorAplica = document.AltaDePagos.FormaAplica[0].value;
		else
		  valorAplica = document.AltaDePagos.FormaAplica[1].value;*/
		//alert(valorAplica);

		if(tipoLst.selectedIndex == 1)
		{
			if(numDoctoActual != "")
				facturaAsociada = numDoctoActual;
			else
				facturaAsociada = numdocumento;
		}

		var importeFacAsociada = importeDeFactura(facturaAsociada);

		if(numDoctoActual != "" && (tipoLst.selectedIndex < 3 ||
			(tipoLst.selectedIndex > 2 && asociadaDoctoActual == facturaAsociada)))
			{
			if(naturalezaDoctoActual == 'D')
				importeFacAsociada = suma(importeFacAsociada,importeDoctoActual);
			else
				importeFacAsociada = resta(importeFacAsociada,importeDoctoActual);
			}
		if(numDoctoActual == "" && numdocumento != "" && yaExiste(numdocumento))
			{
			cuadroDialogo("El n&uacute;mero de documento ya existe",3);
			correctoOp = false;
			}
		else if(numDoctoActual != "" && numDoctoActual != numdocumento && yaExiste(numdocumento))
			{
			cuadroDialogo("El n&uacute;mero de documento ya existe",3);
			correctoOp = false;
			}
		else if(provLst.selectedIndex < 1 && provVal.length == 0)
			{
			cuadroDialogo("Debe especificar un proveedor",3);
			correctoOp = false;
			}
		else if(tipoLst.selectedIndex < 1 && correctoOp)
			{
			cuadroDialogo("Debe especificar un tipo de documento",3);
			correctoOp = false;
			}
		else if(trim(document.AltaDePagos.NumeroDocumento.value) == "" && correctoOp)
			{
			cuadroDialogo("Debe especificar un n&uacute;mero de documento",3);
			correctoOp = false;
			}
		else if(!validaAlfa(document.AltaDePagos.NumeroDocumento.value) && correctoOp)
			{
			cuadroDialogo("El n&uacute;mero de documento contiene caracteres no v&aacute;lidos",3);
			correctoOp = false;
			}
		else if(document.AltaDePagos.ImporteDocumento.value == "" && correctoOp)
			{
			cuadroDialogo("Debe especificar un importe",3);
			correctoOp = false;
			}
		else if(!validaFlotante(document.AltaDePagos.ImporteDocumento.value) && correctoOp)
			{
			cuadroDialogo("El importe no es v&aacute;lido",3);
			correctoOp = false;
			}
		else if(tipoLst.selectedIndex == 1 && importeFacAsociada.substring(0,1) == "-")
			{
			importeFacAsociada = importeFacAsociada.substring(1);
			if(compara(importeFacAsociada,document.AltaDePagos.ImporteDocumento.value)>0)
				{
				cuadroDialogo("El importe (" + formateaAmoneda(document.AltaDePagos.ImporteDocumento.value) +
					") no alcanza a colocar el importe total (-" + formateaAmoneda(importeFacAsociada) +
					") en un valor mayor a 0.",3);
				correctoOp = false;
				}
			}
		/*else if(tipoLst.selectedIndex > 2)
			{
			if(natLst.selectedIndex == 0 && correctoOp)
				{
				cuadroDialogo("Debe especificar una naturaleza",3);
				correctoOp = false;
				}
			else if(asoLst.selectedIndex == 0 && correctoOp)
				{
				cuadroDialogo("Debe especificar una factura asociada",3);
				correctoOp = false;
				}
			else if(natLst.selectedIndex == 2 && importeFacAsociada.substring(0,1) == "-")
				{
				cuadroDialogo("El importe de la nota (" + formateaAmoneda(document.AltaDePagos.ImporteDocumento.value) +
					") es deudora y mayor al importe parcial de la factura asociada (" + formateaAmoneda(importeFacAsociada) +
					"), lo que resultar&iacute;a en un importe negativo.",3);
				correctoOp = false;
				}
			else if(natLst.selectedIndex == 2 && compara(importeFacAsociada,
				document.AltaDePagos.ImporteDocumento.value)<0)
				{
				cuadroDialogo("El importe de la nota (" + formateaAmoneda(document.AltaDePagos.ImporteDocumento.value) +
					") es deudora y mayor al importe parcial de la factura asociada (" + formateaAmoneda(importeFacAsociada) +
					"), lo que resultar&iacute;a en un importe negativo.",3);
				correctoOp = false;
				}
			else if(importeFacAsociada.substring(0,1) == "-")
				{
				importeFacAsociada = importeFacAsociada.substring(1);
				if(compara(document.AltaDePagos.ImporteDocumento.value,importeFacAsociada)<0)
					{
					cuadroDialogo("No puede usarse el importe especificado (" + document.AltaDePagos.ImporteDocumento.value +
						"), si se suma al importe parcial de la factura asociada (-" + importeFacAsociada +
						"), no alcanza a superar el importe negativo.",3);
					correctoOp = false;
					}
				}
			}*/
			//Se agrego por HGV para el proyecto de Ley de Transparencia II
			if(correctoOp){
				if (validaAlfanumerico(document.AltaDePagos.Concepto.value)){
					if (validaNumerico(document.AltaDePagos.Referencia.value)){
						correctoOp = true;
					} else {
						cuadroDialogo("La Referencia debe ser num&eacute;rica",3);
						correctoOp = false;
					}
				} else {
					cuadroDialogo("El concepto debe ser alfanum&eacute;rico, sin caracteres especiales",3);
					correctoOp = false;
				}
			}		

			//Fin HGV

			if(correctoOp==true){
				tipoLst.disabled = false;
				document.AltaDePagos.submit();
			}
	}

	/*
	 * jgarcia - 07/Oct/2009
	 * El radio button FormaAplica, que es donde se manda a llamar esta funcion, ya deja de existir.

	function cuentaInterbancaria()
	{
		var provLst = document.AltaDePagos.Proveedor;
		var provVal = trim(provLst.options[provLst.selectedIndex].value);
		var apliLst = provVal.substring(provVal.indexOf("|") + 1);

		//alert ("Tipo Aplicaci�n = >" +apliLst+ "<");

		if (apliLst == "1")
		 {
			cuadroDialogo("Solo aplica para Proveedores con cuenta Interbancaria",3);
			document.AltaDePagos.FormaAplica[0].checked=true;
		 }
	}
	*/

	// --- selecciona el valor 'valor' de la lista 'lista'
	function selecciona(lista,valor)
		{
		for(a=0;a<lista.length;a++)
			if(lista.options[a].value == valor) lista.selectedIndex = a;
		}

	// --- corta espacios a los extremos
	function trim(cadena)
		{
		while(cadena.substring(0,1) == " ") cadena = cadena.substring(1);
		while(cadena.substring(cadena.length-1) == " ") cadena = cadena.substring(0,cadena.length()-1);
		return cadena;
		}
		
	function ltrim(s) {
	   return s.replace(/^\s+/, "");
	}

	function rtrim(s) {
	   return s.replace(/\s+$/, "");
	}
	
	function trimCadena(s) {
	   return rtrim(ltrim(s));
	}

	// --------------------------------------------------------------------------------------------

	<%int totalregs = (int)objArchivo.getTotalRegistros();
	Documento docum;
	String docs = "";
	String importesFacturas = "";
	String importesTodos = "";
	int z;

	for(z=0;z<totalregs;z++)
		{
		docum = objArchivo.getDoc(z);
		docs += ", '" + docum.getproveedor() + "|" + docum.gettipo() + "|" + docum.getnumdocto() + "'";
		if(docum.gettipo() == 1)
		importesFacturas += ", '" + docum.getproveedor() + "|" + docum.gettipo() + "|" + docum.getnumdocto() +
				"|" + docum.getimporte() + "'";
		}
	if(docs.length()>2) docs = docs.substring(2);
	if(importesFacturas.length()>2) importesFacturas = importesFacturas.substring(2);%>
	var numDoctos = new Array(<%= docs %>);
	var importesFacturas = new Array(<%= importesFacturas %>);

	//
	function yaExiste(numDoc)
		{
		var a;
		var existe = false;
		for(a=0;a<numDoctos.length;a++)
			{if(numDoctos[a] == numDoc) existe = true;}
		return existe;
		}

	//
	function importeDeFactura(factura)
		{
		var a;
		var documento;
		var importe;
		for(a=0;a<importesFacturas.length;a++)
			{
			documento = importesFacturas[a].substring(0,importesFacturas[a].lastIndexOf("|"));
			importe = importesFacturas[a].substring(importesFacturas[a].lastIndexOf("|")+1);
			if(documento == factura) break;
			}
		if(documento != factura) importe = "-1";
		return importe;
		}

	//
	function formateaAmoneda(num)
		{
		var str = "" + num;
		var pos = str.indexOf(".");

		if(pos == -1) {pos = str.length; str += ".";}
		while(pos+3<str.length) str = str.substring(0,str.length-1);
		while(pos+3>str.length) str += "0";
		while(str.length<4) str= "0" + str;
		for(a=str.length-6;a>0;a-=3) str = str.substring(0,a) + "," + str.substring(a);
		str = "$" + str;
		return str;
		}

	//
	function estandarizaImporte(valor)
		{
		var pos = valor.indexOf(".");
		if(pos == -1)
			valor += "00";
		else
			{
			while(pos+3<valor.length) valor = valor.substring(0,valor.length-1);
			while(pos+3>valor.length) valor += "0";
			valor = valor.substring(0,valor.length-3) + valor.substring(valor.length-2);
			}
		while(valor.length<3) valor = "0" + valor;
		while(valor.length>3 && valor.substring(0,1) == "0") valor = valor.substring(1);
		return valor;
		}

	//
	function compara(valor1,valor2)
		{
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);
		if(valor1.length > valor2.length) return 1;
		if(valor1.length < valor2.length) return -1;
		for(a=0;a < valor1.length; a++)
			{
			if(parseInt(valor1.substring(a,a+1)) > parseInt(valor2.substring(a,a+1))) return 1;
			if(parseInt(valor1.substring(a,a+1)) < parseInt(valor2.substring(a,a+1))) return -1;
			}
		return 0;
		}

	//
	function suma(valor1,valor2)
		{
		var aux = "";
		var acarreo = 0, suma = 0, dig1 = 0, dig2 = 0;
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);
		if(compara(valor1,valor2)<0) {aux = valor1; valor1 = valor2; valor2 = aux; aux = "";}
		while(valor2.length<valor1.length) valor2 = "0" + valor2;

		for(a=valor1.length-1;a>-1;a--)
			{
			dig1 = parseInt(valor1.substring(a,a+1)) + acarreo;
			dig2 = parseInt(valor2.substring(a,a+1));
			suma = dig1 + dig2;
			acarreo = 0;
			while(suma>=10) {acarreo++; suma -= 10;}
			aux = suma + "" + aux;
			}
		if(acarreo > 0) {aux = acarreo + "" + aux;}
		while(aux.length > 3 && aux.substring(0,1) == "0") aux = aux.substring(1);
		while(aux.length < 3) aux = "0" + aux;
		aux = aux.substring(0,aux.length-2) + "." + aux.substring(aux.length-2);
		return aux;
		}

	//
	function resta(valor1,valor2)
		{
		var prefijo = "", aux = "";
		var deuda = 0, resta = 0, dig1 = 0, dig2 = 0;
		valor1 = estandarizaImporte(valor1);
		valor2 = estandarizaImporte(valor2);
		if(compara(valor1,valor2)<0) {aux = valor1; valor1 = valor2; valor2 = aux; prefijo = "-";}
		while(valor2.length<valor1.length) valor2 = "0" + valor2;
		aux = "";
		for(a=valor1.length-1;a>-1;a--)
			{
			dig1 = parseInt(valor1.substring(a,a+1)) - deuda;
			dig2 = parseInt(valor2.substring(a,a+1));
			deuda = 0;
			while(dig1<dig2) {dig1 += 10; deuda++;}
			resta = dig1 - dig2;
			aux = resta + "" + aux;
			}
		while(aux.length > 3 && aux.substring(0,1) == "0") aux = aux.substring(1);
		while(aux.length < 3) aux = "0" + aux;
		aux = aux.substring(0,aux.length-2) + "." + aux.substring(aux.length-2);
		return prefijo + aux;
		}

	// -------- validaciones ----------------------------------------------------------------------

	//
	var facturas = new Array(<%= arregloFacturas.toString() %>);
	var fechas = new Array(<%= arregloFechas.toString() %>);

	/*
	 * jgarcia - 07/Oct/2009
	 * Se elimina la utilizaci�n de Naturaleza y Asociado, asi como la validaci�n para
	 * los casos cuando el tipo de documento es mayor que 2 ya que en este momento solo
	 * se utilizan [1 - Factura] y [2 - Otros]
	*/
	function cambioProveedor(){
		var listaProv = document.AltaDePagos.Proveedor;
		// var listaAsoc = document.AltaDePagos.Asociado;
		var lon = 0;
		var proveedor = listaProv.options[listaProv.selectedIndex].value;
		var factura = "";

		proveedor = proveedor.substring(0,proveedor.indexOf("|"));

		//alert ("Proveedor - cambioProveedor() = " +proveedor);

		// listaAsoc.options.length = 0;

		// listaAsoc.options[lon++] = new Option("-------------------------------------","");
		for(a=0;a<facturas.length;a++)
		 {
			if(proveedor == facturas[a].substring(0,facturas[a].indexOf("|")))
		     {
				factura = facturas[a].substring(facturas[a].indexOf("|")+1);
				// listaAsoc.options[lon++] = new Option(factura,factura);
			 }
		 }
		// listaAsoc.selectedIndex = 0;
		// listaAsoc.length = lon;
		//colocaFecha();

		// Establecer forma de aplicacion Mismo Dia, para proveedores interbancarios
		/*
		var provLst = document.AltaDePagos.Proveedor;
		var provVal = trim(provLst.options[provLst.selectedIndex].value);
		var apliLst = provVal.substring(provVal.indexOf("|") + 1);
		if (apliLst == "1")
			document.AltaDePagos.FormaAplica[0].checked=true;
		*/
		ActualizaClave();
	}

	/*
	 * jgarcia - 07/Oct/2009
	 * Se elimina la utilizaci�n de Naturaleza y Asociado, asi como la validaci�n para
	 * los casos cuando el tipo de documento es mayor que 2 ya que en este momento solo
	 * se utilizan [1 - Factura] y [2 - Otros]
	*/
	function cambioTipo(){
		// var natLst = document.AltaDePagos.Naturaleza;
		// var asoLst = document.AltaDePagos.Asociado;
		var sel = document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value;
		sel = parseInt(sel);
		switch(sel){
			case 1: case 2:
				// natLst.disabled = true;
				//asoLst.disabled = true;
				// natLst.selectedIndex = 0;
				// asoLst.selectedIndex = 0;
				document.AltaDePagos.FechaVencimiento.value= "<%= hoy %>";
				break;
			case 0: case 3: case 4:
				// natLst.disabled = false;
				// asoLst.disabled = false;
				document.AltaDePagos.FechaVencimiento.value= "";
				break;
		}
		//colocaFecha();
	}

	/*
	 * jgarcia - 07/Oct/2009
	 * El combo Asociado, que es donde se manda a llamar esta funcion, ya deja de existir.

	function cambioAsociado()
		{
		colocaFecha();
		}
	*/

	//
	function focoProv()
		{
		if(<%= parAccion.equals("2") %>) document.AltaDePagos.Proveedor.blur();
		}

	//
	function focoTipo()
		{
		if(<%= parAccion.equals("2") %>) document.AltaDePagos.TipoDocumento.blur();
		}

	/*
	 * jgarcia - 07/Oct/2009
	 * El combo Asociado, que es donde se manda a llamar esta funcion, ya deja de existir.

	function focoAsociado()
		{
		var tipoDoc = document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value;
		tipoDoc = parseInt(tipoDoc);
		if(tipoDoc<3 && tipoDoc != 0) document.AltaDePagos.Asociado.blur();
		}
	*/

	/*
	 * jgarcia - 07/Oct/2009
	 * El combo Naturaleza, que es donde se manda a llamar esta funcion, ya deja de existir.

	function focoNaturaleza()
		{
		var tipoDoc = document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value;
		tipoDoc = parseInt(tipoDoc);
		if(tipoDoc<3 && tipoDoc != 0) document.AltaDePagos.Naturaleza.blur();
		}
	*/

	//
	function aceptaModifFechaFinal()
		{
		var tipoLst = document.AltaDePagos.TipoDocumento;
		if(tipoLst.selectedIndex >2)
			{
			cuadroDialogo("No se permite modificar fecha de vencimiento de notas",3);
			return false;
			}
		return true;
		}

	//
	function validaFlotante(valor)
		{
		var total = valor.length;
		var patron = "0123456789.";
		var valida = true;
		for(a=0;a<total && valida;a++) if(patron.indexOf(valor.substring(a,a+1)) == -1) valida = false;
		if(valor.indexOf(".")!=valor.lastIndexOf(".")) valida = false;
		if(valor.indexOf(".")!=-1 && valor.indexOf(".")+3<valor.length) valida = false;//para validar m�ximo dos decimales
		return valida;
		}

	//
	function validaAlfa(valor)
		{
		var total = valor.length;
		var patron = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-";
		var valida = true;
		for(a=0;a<total && valida;a++) if(patron.indexOf(valor.substring(a,a+1)) == -1) valida = false;
		return valida;
		}

	//Funcion agregada por HGV para el proyecto de Ley de Transparencia II
	function validaNumerico(valor)
	{
		var total = valor.length;
		var patron = "0123456789";
		var valida = true;
		for(a=0;a<total && valida;a++) if(patron.indexOf(valor.substring(a,a+1)) == -1) valida = false;
		return valida;
	}

	//Funcion agregada por HGV para el proyecto de Ley de Transparencia II
	function validaAlfanumerico(valor)
	{
		var TemplateC=/^([a-z]|[0-9]|[\s])*$/i;
		return TemplateC.test(document.AltaDePagos.Concepto.value);
	}

	/*
	 * jgarcia - 07/Oct/2009
	 * Se quita la utilizaci�n de la funcion colocaFecha, ya que esta hace
	 * referencia al combo Asociado y al de Naturaleza los cuales ya no
	 * se utilizan.
	 */

	/*
	function colocaFecha()
		{
		var tipo = document.AltaDePagos.TipoDocumento.selectedIndex;
		var factura = document.AltaDePagos.Asociado.options[document.AltaDePagos.Asociado.selectedIndex].value;
		var proveedor = document.AltaDePagos.Proveedor.options[document.AltaDePagos.Proveedor.selectedIndex].value;
		var facArreglo, provArreglo, fechaArreglo;
		if(document.AltaDePagos.Asociado.selectedIndex > 0 && tipo > 2)
			{
			for(a=0;a<fechas.length;a++)
				{
				provArreglo = fechas[a].substring(0,fechas[a].indexOf("|"));
				fechaArreglo = fechas[a].substring(fechas[a].lastIndexOf("|")+1);
				facArreglo = fechas[a].substring(fechas[a].indexOf("|")+1,fechas[a].lastIndexOf("|"));
				if(facArreglo == factura && provArreglo == proveedor) break;
				}
			if(a<fechas.length)
				document.AltaDePagos.FechaVencimiento.value = fechaArreglo;
			}
		if(document.AltaDePagos.Asociado.selectedIndex == 0 && tipo > 2)
			document.AltaDePagos.FechaVencimiento.value = "";
		}
	*/


	// --- Selecci�n de fechas ---------------------------------------------------------
	var js_diasInhabiles = <%=
	((request.getAttribute("diasInhabiles")!=null)?("\"" + (String)request.getAttribute("diasInhabiles") + "\""):("\"\"")) %>;
	var dia;
	var mes;
	var anio;
	var fecha_completa;
	var opcioncalendario;

	function Actualiza()
		{
		if (opcioncalendario==1)
			document.AltaDePagos.FechaEmision.value=fecha_completa;
		else if (opcioncalendario==2)
			document.AltaDePagos.FechaVencimiento.value=fecha_completa;
		}

	function WindowCalendar(opcion)
		{
		if(opcion == 2 && !aceptaModifFechaFinal()) return;
		opcioncalendario=opcion;

		var mesI, mesF, annI, annF, diaI, diaF;

		var dia = <%= fecha.get(Calendar.DATE) %>;
		var mes = <%= fecha.get(Calendar.MONTH) %>;
		var ann = <%= fecha.get(Calendar.YEAR) %>;

		if(opcioncalendario == 1)
			{diaI = 1; diaF = dia; mesI = mes - 2; mesF = mes;}
		else
			{diaI = dia; diaF = 31; mesI = mes; mesF = mes + 6;}
		annI = annF = ann;
		if(mesF > 11) {mesF-=12; annF++;}
		if(mesI < 0) {mesI+=12; annI--;}

		switch(mesF)
			{
			case 3: case 5: case 8: case 10: if(diaF>30) diaF=30; break;
			case 1: if(annF % 4 == 0) {if(diaF>29) diaF=29;} else {if(diaF>28) diaF=28;}
			}

		var texto = "PagosImpuestos?OPCION=20&diaI=" + diaI +
			"&diaF=" + diaF + "&mesI=" + mesI + "&mesF=" + mesF + "&anioI=" + annI + "&anioF=" +
			annF + "&diasInhabiles=" + escape(js_diasInhabiles) + "&mes=" + mes + "&anio=" +
			ann + "&priUlt=0#mesActual";
		msg=window.open(texto,"calendario","toolbar=no," +
			"location=no,directories=no,status=no,menubar=no" +
			",scrollbars=yes,resizable=no,width=330,height=260");
		msg.focus();
		}

	// jgarcia - 08/Oct/2009
	// Se agregan las variables [arregloProveedores, valorAnterior]
	// y las funciones [generarArreglo(), rellenarCombo(), ActualizaClave()]
	var arregloProveedores = new Array();
	var valorAnterior = "";
	function rellenarCombo(){
		textoInput = trimCadena(document.getElementById('cveProveedor').value);
		if(valorAnterior != textoInput){
			longInput = textoInput.length;
			combo = document.getElementById('Proveedor');
			var flag = 0;

			if(longInput > 0){
				comboLenght = combo.length;
				for(var i = 1; i < comboLenght; i++){
					var datos = combo.options[i].text.split("|");
					var claveTemp = datos[1];
					if(textoInput == claveTemp){
						combo.options[i].selected = true;
						flag = 1;
					}
				}
			}
			if(flag==0){
				cuadroDialogo("La clave de proveedor no corresponde a la Divisa o no esta registrado.",3);
				document.AltaDePagos.cveProveedor.value = "";
				document.AltaDePagos.Proveedor.selectedIndex = 0;
				return;
			}
			valorAnterior = textoInput;
		}
	}

	function generarArreglo(){
		combo = document.getElementById('Proveedor');
		for(var i = 0; i < combo.length; i++){
			arregloProveedores[i] = combo.options[i].value + "|" + combo.options[i].text;
		}
	}

	// -- Actualiza el combbo de Clave de Proveedor
	function ActualizaClave(){
		var prove = document.getElementById('Proveedor');
		var opciones = prove.options[prove.selectedIndex].text.split("|");
		if(opciones.length >= 2){
			var textoCombo = "";
			if(opciones[1].length > 0){
				textoCombo = trim(opciones[1]);
			}
			document.getElementById('cveProveedor').value = textoCombo;
			document.getElementById('cveProveedor').focus();
		}
	}

	</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="inicia();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');generarArreglo();" background="/gifs/EnlaceMig/gfo25010.gif">

	<!-- Encabezado y men� -->
	<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571"><tr valign="top"><td width="*">
	<% if ( request.getAttribute( "MenuPrincipal" ) != null ) { out.print( request.getAttribute( "MenuPrincipal" ) ); } %>
	</td></tr></table>
	<%= request.getAttribute( "Encabezado" ) %>


	<form name="AltaDePagos" method="POST" action="coPagos"> <!-- jsp/rvvPars.jsp -->
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr align="center">
			<td valign="top" width="661">
				<table width="350" border="0" cellspacing="0" cellpadding="0">
					<tr align="left" valign="middle">
						<td class="tittabdat" width="708"> Especifique los datos del pago </td>
					</tr>
					<tr align="center">
					  <td class="textabdatcla" valign="top" width="661">
						<table width="350" border="0" cellspacing="0" cellpadding="0">
						  <tr valign="top">
							<td width="270" align="left">
								<table width="100%" border="0" cellspacing="5" cellpadding="0">
									<tr align="left">
										<td colspan="4"><span class="texencconbol"> Proveedor </span></td>
									</tr>
									<tr>
										<td colspan="2">
										<table width="100%">
											<tr>
												<!-- jgarcia 08/Oct/2009 Se agregan el INPUT para la clave del proveedor -->
												<td align="left" nowrap class="tabmovtex" width="25%">Clave de Proveedor:</td>
												<td class="tabmovtex" nowrap width="25%">
													<input type="text" value="" name="cveProveedor" id="cveProveedor"
													onblur="rellenarCombo()" class="tabmovtex" tabindex="1" maxlength="20"/>
													<!--onchange="this.value=this.value.toUpperCase(); return true;"--> 
												</td>
											</tr>
										</table>
										</td>
										<td colspan="2">
										<table width="100%">
											<tr>
												<td class="tabmovtex" nowrap>Nombre:</td>
												<td class="tabmovtex" nowrap>
													<SELECT NAME="Proveedor" id="Proveedor" class="tabmovtex" onChange="cambioProveedor()"
													onFocus="focoProv()" tabindex="2">
													<option value=""></option>
													<% if(session.getAttribute("listaProveedor")!= null)
														   {
															  total = ((Vector)session.getAttribute("listaProveedor")).size();
															  for(a=0;a<total;a++)
															  {
																%>
													<option value="<%= ((String[])session.getAttribute("valuesListaProveedor"))[a] %>">
														<%= ((String[])session.getAttribute("textListaProveedor"))[a] %>
													</option>
													<%
															  }
														   }
														%>
													</SELECT>
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr align="left">
										<td colspan="4"><span class="texencconbol">Documento </span></td>
									</tr>
									<tr>
										<td width="25%" height="135">
										<table height="100%">
											<tr>
												<td class="tabmovtex" nowrap>Tipo de documento:</td>
											</tr>
											<tr>
												<td align="left" nowrap class="tabmovtex11" width="100">Divisa:</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap>Fecha de emisi&oacute;n:</td>
											</tr>
											<tr>
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de REFERENCIA INTERBANCARIA por N�MERO DE REFERENCIA -->											
												<td class="tabmovtex" nowrap>N&uacute;mero de Referencia:</td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->											
											</tr>
										</table>
										</td>
										<td width="30%" height="135">
										<table height="100%" width="100%">
											<tr>
												<td class="tabmovtex" nowrap>
													<select NAME="TipoDocumento"
													class="tabmovtex" onChange="cambioTipo()"
													onFocus="focoTipo()" tabindex="3">
														<option VALUE="0">-------------------------------------</option>
														<option VALUE="1">Factura</option>
														<option VALUE="2">Otros Documentos</option>
														<!-- <OPTION VALUE="3">Nota de cargo</option>
														 <OPTION VALUE="4">Nota de cr&eacute;dito</option>
														-->
													</select>
												</td>
											</tr>
											<tr>
												<td>
												<table>
													<tr>
														<td><input class="tabmovtex" type="text" value="<%=cveDivisa %>" disabled="disabled"
															name="cveDivisa" id="cveDivisa" size="4" maxlength="3" />
														</td>
														<td><input class="tabmovtex" type="text" value="<%=descDivisa %>" disabled="disabled"
															name="descDivisa" id="descDivisa" size="20" />
														</td>
													</tr>
												</table>
												</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap align="left">
													<input type="text" name="FechaEmision" value="<%= hoy %>" size=15
													onFocus='blur();' class="tabmovtex" maxlength=10>
													<a href="javascript:WindowCalendar(1);" align="absmiddle">
														<img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14"
														border="0" align="absmiddle">
													</a>
												</td>
											</tr>
											<tr>
												<!-- Agegado por HCV para el proyecto de Ley de Transparencia II -->
												<td class="tabmovtex" nowrap>
													<INPUT type="text" size="15" align="rigth" class="tabmovtex"
													name="Referencia" value="" maxlength="7" tabindex="6">
												</td>
											</tr>
										</table>
										</td>
										<td width="25%" height="135">
										<table height="100%">
											<tr>
												<td class="tabmovtex" nowrap>N&uacute;mero de documento:</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap>Importe:</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap>Fecha de vencimiento:</td>
											</tr>
											<tr>
												<!-- Agegado por HCV para el proyecto de Ley de Transparencia II -->
<!--GOR 20110113 (Req. Confirming - 3 - Inicio): Cambiar la etiqueta en las pantallas de CONCEPTO INTERBANCARIO por CONCEPTO DEL PAGO -->											
												<td class="tabmovtex" nowrap>Concepto del Pago:</td>
<!--GOR 20110113 (Req. Confirming - 3 - Fin) -->											
											</tr>
										</table>
										</td>
										<td height="135" width="151">
										<table height="100%">
											<tr>
												<td class="tabmovtex" nowrap>
													<INPUT TYPE="text" SIZE="17"
													ALIGN="rigth" class="tabmovtex" NAME="NumeroDocumento"
													VALUE="" maxlength="8" tabindex="4">
												</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap>
													<INPUT TYPE="text" SIZE="17"
													ALIGN="rigth" class="tabmovtex" NAME="ImporteDocumento"
													VALUE="" maxlength="21" tabindex="5">
												</td>
											</tr>
											<tr>
												<td class="tabmovtex" nowrap align="left">
													<input type="text" name="FechaVencimiento"
													value="<%= ((doc.gettipo()<3)?hoy:"") %>" size="15"
													onFocus='blur();' class="tabmovtex" maxlength=10>
													<A href="javascript:WindowCalendar(2);">
														<img src="/gifs/EnlaceMig/gbo25410.gif"
														width="12" height="14" border="0" align="absmiddle">
													</a>
												</td>
											</tr>
											<tr>
												<!-- Agegado por HCV para el proyecto de Ley de Transparencia II -->
												<td class="tabmovtex" nowrap>
													<INPUT TYPE="text" SIZE="15"
													ALIGN="rigth" class="tabmovtex" NAME="Concepto" VALUE=""
													maxlength="40" tabindex="7">
												</td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
							</td>
					      </tr>
						</table>
					  </td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td align="center">
		  <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
		    <tr>
			  <td align="center" valign="middle" width="66">
			    <A HREF="javascript:aceptar();" tabindex="8">
			    	<img border="0" src="/gifs/EnlaceMig/<%=
					(parAccion.equals("1"))?"gbo25480":"gbo25510" %>.gif">
				</a>
			  </td>
			  <% if(parAccion.equals("1")) { %>
			  <td align="left" valign="top" width="76">
			    <A HREF="javascript:document.AltaDePagos.reset();" tabindex="9"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a>
			  </td>
			  <% } %>
			  <td align="left" valign="top" width="83">
			    <A href="javascript:regresar()" tabindex="10"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
	</table>

	<input type="hidden" name="accion" value="<%= Integer.parseInt(parAccion)+2 %>">
	<input type="hidden" name="numSelec" value="<%= baseServlet.getFormParameter(request,"numSelec") %>">
</form>
	<%=
		(request.getAttribute("mensaje") != null)?
		("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
		");</script>"):""
	%>
</BODY>
</html>