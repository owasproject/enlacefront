<%--************************************************************
	**	Busqueda de Empleados - Cuentas N�mina Interbancaria  **
	**													 	  **
	**  25/06/2009 		JGAL							      **
	*******************************************************	--%>

<html>
<head>
<title>B&uacute;squeda de Empleados en el Cat&aacute;logo de N&oacute;mina Interbancaria</title>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<script type="text/javascript" language="javascript">


function continua()
{
	if (respuesta == 1)
	{
		document.ResultadosEmpleados.opcion.value = "5";
		document.ResultadosEmpleados.action = "CatalogoNominaEmplInterb";
		document.ResultadosEmpleados.submit();
	}
}


function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}
function cambioPagina(numPag,numPags,iniBlo,finBlo,tamBlo,totReg,cambPag)
{
	document.ResultadosEmpleados.opcion.value = "2";
	document.ResultadosEmpleados.numeroPaginas.value = numPags;
	document.ResultadosEmpleados.totalReg.value = totReg;
	if( cambPag == 'S' ){
		document.ResultadosEmpleados.numeroPagina.value = numPag + 1;
		document.ResultadosEmpleados.inicioBloque.value = parseInt(iniBlo)+tamBlo;
		document.ResultadosEmpleados.finBloque.value = parseInt(finBlo)+tamBlo;
	}
	else{
		document.ResultadosEmpleados.numeroPagina.value = numPag - 1;
		document.ResultadosEmpleados.inicioBloque.value = parseInt(iniBlo)-tamBlo;
		document.ResultadosEmpleados.finBloque.value = parseInt(finBlo)-tamBlo;
	}
	document.ResultadosEmpleados.action = "CatalogoNominaEmplInterb";
	document.ResultadosEmpleados.submit();
}


function Baja()
{ 
	if (listaElemBaja()) {
		if(cuadroDialogo("&iquest;Est&aacute; seguro de querer dar de baja <br> los registros seleccionados? \n" , 2 ) )
		{
			return true;
		}
	}
}


function Buscar()
{
	isCriterioValid = validaCriterioBusqueda(document.busquedaEmpleados.valorBusqueda.value);

	if(isCriterioValid != true)
	{
		return;
	}
	if(isCriterioValid == true)
	{
		document.busquedaEmpleados.opcion.value = "2";
		document.busquedaEmpleados.action = "CatalogoNominaEmplInterb";
		document.busquedaEmpleados.submit();
	}
}

var filtroFor;

function procesaSubmit(){
	Buscar();
	return false;
}

function validaCriterioBusqueda(criterio)
{
	var caracteresPermitidos = "abcdefghijklmn�opqrstuvwxyz�����" + "ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����" + "0123456789" + " ";
	var strCriterio = criterio;
	var allValid;
	var carInvalidos = 0;

	if(criterio == "" || criterio == null || (criterio == "  "))
	{
		return true;
	}

	for (j=0; j<strCriterio.length; j++)
	{
		caracter = strCriterio.charAt(j);
		for (k=0; k<caracteresPermitidos.length; k++)
		{
			if (caracter == caracteresPermitidos.charAt(k))
			{
				allValid = true;
				break;
			}
			if (k == caracteresPermitidos.length -1)
			{

				cuadroDialogo("No se permiten los siguientes caracteres: <br> @ ! � � ? \" # $ % & / ( ) = + - * / , ' { } [ ] _ � � * + ^ : ; ", 3 );
				allValid = false;
				carInvalidos++;
				break;
			}
		}
		if(carInvalidos != 0)
		{
			break;
		}
	}
	if (!allValid)
	{
		return false;
	}
	if (allValid)
	{
		return true;
	}
}

function SeleccionaTodos()
{
	var forma=document.ResultadosEmpleados;

  if(forma.Todas.checked==true)
   {
	  forma.Todas.checked=true;
     for(i2=0;i2<forma.length;i2++)
      if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
        forma.elements[i2].checked=true;
   }
  else
   {
     forma.Todas.checked=false;
     for(i2=0;i2<forma.length;i2++)
      if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='Todas')
        forma.elements[i2].checked=false;
   }
}

function listaElemBaja()
{
   var h=document.ResultadosEmpleados.elements.length;
   var forma = document.ResultadosEmpleados;
    var ctas="|"; // = new Array (h);
    var j=0;
    for (i=0; i< h; i++)
    {
      if (forma.elements[i].type=="checkbox")
      {
         if (forma.elements[i].checked==false)
        {
          var m=0;
        }
        else{
           var e = forma.elements[i];
           if(e.name != "Todas" && e.checked==true){
    //MHG (IM325073) se cambio el separador coma (,) por un pipe(|)
            ctas = ctas + forma.elements[i].value + "|";
            j++;
           }
        }
      }
      else
          var m=0;
    }

     forma.cuentas.value = ctas;
     forma.tot_cuentas.value = j;
   if(j==0)
   {
     cuadroDialogo("Usted no ha seleccionado un empleado.\n\nPor favor seleccione uno \npara realizar la operaci&oacute;n",3);
     return false;
   }
	return true;
}


function Limpiar()
{
	document.busquedaEmpleados.valorBusqueda.value        = "";
	document.busquedaEmpleados.radioValue[0].checked=true;
	document.busquedaEmpleados.valorBusqueda.focus();
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/


<% if (request.getAttribute("newMenu") != null ) { %>
<%= request.getAttribute("newMenu") %>
<%	} %>


</script>

</head>



<%-- Se construye la interfaz para que el usuario realice la b�squeda de
	empleados en el cat�logo de n�mina y posteriormente realice una baja
	o modificaci�n sobre el mismo.  --%>

	<BODY>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
			<!-- MENU PRINCIPAL -->
			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</TD>
	</TR>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>
		<FORM NAME="busquedaEmpleados" METHOD="Post" action="" onSubmit="return procesaSubmit();">
		<input type="hidden" name="opcion" value="">

		<CENTER>
		<br>
		<br>
			<table width="500" border="0" cellspacing="2" cellpadding="3" class="textabdatcla">
			<tbody>
				<tr>
					<td class="tittabdat" colspan=2 > Capture los datos para la consulta de Empleados<br></td>
				</tr>

				<tr>
					<td class="tabmovtex" width="150">Ingrese criterio de b&uacute;squeda: </td>
					<TD class="CeldaContenido" width="270">
						<input type="text" name="valorBusqueda" SIZE=70 MAXLENGTH=30 VALUE="" class="tabmovtex" onchange="convierteMayus(busquedaEmpleados.valorBusqueda)" >
					</td>
				</tr>

				<tr>
					<td class="tabmovtex" colspan=2>
						<input type="radio" name="radioValue" value="nombre" checked="checked">Nombre&nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="numCuenta">Num. Cuenta&nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="todos">Todos&nbsp; &nbsp; &nbsp;
						</td>
				</tr>
			</tbody>
			</table>



   			<br>

<%-- Se colocan los botones para realizar la b�squeda o el borrado.  --%>
		    <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
		    	<tr>
		    		<td align="right" valign="top" height="22" width="90"><a href="javascript:Buscar();">
          				<img border="0" name=Buscar value=Buscar src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Buscar">
        			</td>
        			<td align="left" valign="middle" height="22" width="76"><a href="javaScript:Limpiar()">
            				<img name=Limpiar value=Limpiar border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar">
         				</a>
         			</td>
         		</tr>
         	</table>
        </CENTER>
	</FORM>
<%-- Termina construcci�n de botones para la b�squeda  --%>






<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;


<FORM NAME="ResultadosEmpleados" METHOD="Post" action="">
	<input type="hidden" name="opcion" value="">
	<input type="hidden" name="numeroPagina" value="">
	<input type="hidden" name="numeroPaginas" value="">
 	<input type="hidden" name="inicioBloque" value="">
 	<input type="hidden" name="finBloque" value="">
 	<input type="hidden" name="totalReg" value="">
 	<input type="hidden" name="valorBusqueda" value="<%=request.getAttribute("valorBusqueda") %>">
 	<input type="hidden" name="radioValue" value="<%=request.getAttribute("radioValue") %>">
 		<%
 		if (request.getAttribute("codigoTabla") != null ) {
 		%>
		<%=
		 request.getAttribute("codigoTabla")
		 %>

  <%} %>

		<% if (request.getAttribute("notFound") != null ) { %>
		<%= request.getAttribute("notFound") %>
		<%	} %>
</FORM>

		<script language="javascript">
			<% if (request.getAttribute("valorBusqueda") != null ) { %>
			document.busquedaEmpleados.valorBusqueda.value = '<%= request.getAttribute("valorBusqueda") %>';
			<%	} %>

			<% if (request.getAttribute("radioValue") == null ) {
			System.out.println("Es la primera vez = " + request.getAttribute("radioValue") );%>
			document.busquedaEmpleados.radioValue[0].checked=true;
			<%}%>

			<% if (request.getAttribute("radioValue") != null ) {
			System.out.println("Estamos en el if el valor es = " + request.getAttribute("radioValue") );%>


			//Recuperamos el valor del bot�n del radio al momento de mostrar resultados
			<%	//for(int i = 0; i<7; i++)
				//{
						System.out.println("JSP - Estamos en el for el valor es = " + request.getAttribute("radioValue") );

						if(request.getAttribute("radioValue").equals("nombre")){
							System.out.println("**** entramos aL NOMBRE");
							%>
							document.busquedaEmpleados.radioValue[0].checked=true;
						<%}

						else if(request.getAttribute("radioValue").equals("numCuenta")){
							System.out.println("*** entramos NUMCUENTA");
						%>
							document.busquedaEmpleados.radioValue[1].checked=true;
						<%}%>
					<%//}%>
			<%	} %>
		</script>

<%--	BOT�N DE MODIFICAR REGISTRO		--%>
<%
	if (request.getAttribute("codigoTabla") != null) {
	/*jp@everis*/
	String numeroPagina = request.getAttribute("numeroPagina").toString();
	String numeroPaginas = request.getAttribute("numeroPaginas").toString();
	String inicioBloque = request.getAttribute("inicioBloque").toString();
	String finBloque = request.getAttribute("finBloque").toString();
	String tamanoBloque = request.getAttribute("tamanoBloque").toString();
	String totalReg = request.getAttribute("totalReg").toString();
	%>

	<center>
	<table width="140" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
		<tr>

		  <td align="right" valign="top" height="22" width="90">
		  <a href="/Download/<%=(request.getSession().getAttribute("archivoCatNomina") != null ? request.getSession().getAttribute("archivoCatNomina") : "" )%>">
	          <img border="0" name=Exportar value=Exportar src="/gifs/EnlaceMig/gbo25230.gif" width="90" height="22" alt="Exportar">
	 	  </a></td>
	 	   <td align="right" valign="top" height="22" width="90"><a href="javascript:Baja();">
	          <img border="0" name=Baja value=Baja src="/gifs/EnlaceMig/gbo25490.gif" width="70" height="22" alt="Baja">
	 	  </a></td>
	   </tr>
	 </table>
	 <br>
	  <!--  jp@everis -->
	 <table width="300" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF" height="22" class="tabfonbla">
	 	<tr>
	 		<%
	 		if( !numeroPagina.equals("0") ){
	 			if( numeroPagina.equals("1") ){
	 		%>
	 			<td class="textabref" width="25%" align="center">&nbsp;  </td>
	 			<td class="textabref" width="50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" width="100%" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref" width="100%">Registros del 1 al <%=Integer.parseInt(tamanoBloque)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">
	 				<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'S');" >  Siguiente >> </a>
	 			</td>
	 		<%
	 			}
	 			else if ( numeroPagina.equals(numeroPaginas)) {
	 			%>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'A');" >  << Anterior </a>
				</td>
	 			<td class="textabref" width="50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref">Resgistros del <%=Integer.parseInt(inicioBloque)+1%>
	 						al <%=Integer.parseInt(totalReg)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">&nbsp;  </td>
	 			<%
	 			}
	 			else{
	 			%>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'A');" >  << Anterior </a>
				</td>
	 			<td class="textabref" width=50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref">Resgistros del <%=Integer.parseInt(inicioBloque)+1%>
	 						al <%=Integer.parseInt(tamanoBloque)*Integer.parseInt(numeroPagina)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'S');" > Siguiente >> </a>
				</td>
	 			<%
	 			}
	 		}
	 		%>
	 	</tr>
	 </table>
	 <!--  jp@everis -->

	 </CENTER>

<%	} %>
<%--	FIN DE BOT�N DE MODIFICAR REGISTRO		--%>

	<script>
		<% if (request.getAttribute("MensajeLogin01") != null ) { %>
		<%= request.getAttribute("MensajeLogin01") %>
		<%	} %>
	</script>


</body>
</html>