<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>

<%@ page import="mx.altec.enlace.bo.FormatosMoneda" %>
<%@ page import="java.util.*" %>
<%@page import="mx.altec.enlace.bo.ConsultaPosicionCE"%>
<%@page import="mx.altec.enlace.bo.ConsultaSaldosCE"%>
<%--
23/08/2002 Se despliega el signo de pesos en el jsp.   Se agreg� la leyenda "Pesos Mexicanos".
12/10/2002 Se cambi� el constructor de "ConsultaSaldosCE".
16/10/2002 Se cambiaron las anclas por un forma y se agregaron funciones de javascrip para indicar atr�s o adelante.
18/11/2002 Se agreg� el bot�n de regresar para volver a consulta de saldos.
--%>

<%!String noLineas;%>
<%!String ctasLineas;%>
<%!String noDisp=new String("");%>
<%!String cadDireccion;%>
<%!String liga=new String("");%>
<%!String titulo=new String("");%>
<%!String newMenu;%>
<%!String menuPrincipal;%>
<%!boolean esConsultaPosicion=true;%>
<%!int direccion;%>
<%!ConsultaSaldosCE csCE;%>
<%!ConsultaPosicionCE posicionesCE;%>
<%!FormatosMoneda fm;%>
<%
   csCE=(ConsultaSaldosCE)application.getAttribute("csCE");
   posicionesCE=(ConsultaPosicionCE)application.getAttribute("posicionesCE");
   fm=new FormatosMoneda();



    noLineas=request.getParameter("noLineas");
    ctasLineas=request.getParameter("ctasLineas");
    noDisp=request.getParameter("noDisp");
    cadDireccion=request.getParameter("direccion");




   if(csCE==null)
   {
      csCE=new ConsultaSaldosCE();
      //out.println("<BR>csCE02 es nulo.<BR>");
   }


   if(posicionesCE==null)
   {
      posicionesCE=new ConsultaPosicionCE();
      //out.println("posicionesCE es nulo.<BR>");
   }



    try
    {
      direccion=Integer.parseInt(cadDireccion);
    }
    catch(NumberFormatException nfe)
    {
      direccion=0;
    }

    if(noDisp!=null && noDisp.length()>0)
    {
      esConsultaPosicion=false;
      titulo="Consulta de disposiciones de cr&eacute;dito electr&oacute;nico.";
    }
    else
    {
      esConsultaPosicion=true;
      titulo="Consulta de posiciones de cr&eacute;dito electr&oacute;nico.";
    }

   menuPrincipal=(String)request.getAttribute("MenuPrincipal");

   if(menuPrincipal==null)
   {
      menuPrincipal=(String)session.getAttribute("MenuPrincipal");

      if(menuPrincipal==null)
         menuPrincipal="";
   }

   newMenu=(String)request.getAttribute("newMenu");

   if(newMenu==null)
   {
      newMenu=(String)session.getAttribute("newMenu");

      if(newMenu==null)
         newMenu="";

   }
%>


<html>
<head>
	<title>
    <%= titulo %>
  </TITLE>

<!-- Autor: Hugo Ruiz Zepeda -->

  <script language="JavaScript" type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>

  <script language="JavaScript" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>

<script language="JavaScript" type="text/javascript">

function atras()
{
   document.posicionCE.direccion.value=-1;
   document.posicionCE.submit();
}

function adelante()
{
   document.posicionCE.direccion.value=1;
   document.posicionCE.submit();
}

</script>

  <script language="JavaScript" type="text/javascript">
<!--

function MM_preloadImages() { //v3.0

	var d=document;

	if(d.images){

		if(!d.MM_p)

			d.MM_p=new Array();

		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}

	}

}

function MM_swapImgRestore() { //v3.0

	var i,x,a=document.MM_sr;

	for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}



function MM_findObj(n, d) { //v3.0

	var p,i,x;

	if(!d) d=document;

	if((p=n.indexOf("?"))>0&&parent.frames.length) {

	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;

}



function MM_swapImage() { //v3.0

var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

		 	<%= newMenu %>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<%
  out.print("<!--");
  out.print('\u0048');
  out.print('\u0052');
  out.print('\u005A');
  out.println("-->");
%>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor="#ffffff" leftMargin="0" topMargin="0" marginwidth="0" marginheight="0"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%= menuPrincipal %>
       </TD>
  </TR>
</TABLE>

<%= (String)request.getAttribute("Encabezado") %>



  <Center>
<%

   /*
    if(esConsultaPosicion)
    {
      st=new StringTokenizer(ctasLineas, "|");
    }
    else
    {
      st=new StringTokenizer(noDisp, "|");
    }



    st.nextToken();
    linea=st.nextToken();
   */


    if(noDisp==null)
      noDisp="";

    if(ctasLineas==null)
      ctasLineas="";

    if(noLineas==null)
      noLineas="";

%>
  </Center>

    <Form name="posicionCE" method="post" action="ceConPosicion">
  		<table width="760" border="0" cellspacing="0" cellpadding="0">

        <TR>
				<td align="center">

            <TABLE width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
               <TR>
                  <td align="center" class="tittabdat">
                     Moneda
                  </TD>
               </TR>
               <TR bgcolor="#CCCCCC">
                  <td align="center" class="textabdatcla">
                     Pesos Mexicanos
                  </TD>
               </TR>
            </TABLE>

            <BR>

				<table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">

					<TR>
						<td align="center" width="70" class="tittabdat">
              Cuenta
						</td>
            <td align="center" width="70" class="tittabdat">
              Saldo
						</td>
						<td align="center" class="tittabdat">
              Descripci&oacute;n
						</td>
					</tr>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              <%= csCE.getCuenta() %>
						</td>
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(csCE.getSaldo(), false)); %>
						</td>
						<td align="center" class="textabdatcla">
              <%= csCE.getDescripcion() %>
						</td>
          </TR>
				</table>

        <BR>

        <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
					<tr>
						<td align="center" class="tittabdat">
              <% if(esConsultaPosicion)
                   out.println("L&iacute;nea de cr&eacute;dito:");
                 else
                   out.println("N&uacute;mero de disposici&oacute;n:");
              %>
						</td>
					</tr>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              &nbsp;
              <%= posicionesCE.getLinea() %>
						</td>
          </TR>
				</table>

        <BR>

        <table width="760" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td align="center" valign="top" class="texenccon">
              <span class="texencconbol">Capital</span>
            </td>
          </tr>
        </table>


        <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
					<tr>
						<td align="center" class="tittabdat">
              Vencido:
						</td>
						<td align="center" class="tittabdat">
              Vigente:
						</td>
					</tr>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getVencido(), false)); %>
            </td>
						<td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getVigente(), false)); %>
						</td>
          </TR>
				</table>

        <BR>

        <table width="760" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td align="center" valign="top" class="texenccon">
              <span class="texencconbol">Intereses</span>
            </td>
          </tr>
        </table>

        <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
					<tr>
						<td align="center" class="tittabdat">
              No exigible:
						</td>
						<td align="center" class="tittabdat">
              Exigible:
						</td>
            <td align="center" class="tittabdat">
              Moratorios:
						</td>
					</tr>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getNoExigible(), false)); %>
						</td>
						<td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getExigible(), false)); %>
						</td>
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getMoratorios(), false)); %>
						</td>
          </TR>
				</table>


        <BR>

        <table width="760" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td align="center" valign="top" class="texenccon">
              <span class="texencconbol">Otros adeudos</span>
            </td>
          </tr>
        </table>

                <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
					<tr>
						<td align="center" class="tittabdat">
              Gastos jur&iacute;dicos:
						</td>
						<td align="center" class="tittabdat">
              Seguro:
						</td>
            <td align="center" class="tittabdat">
              Otros:
						</td>
					</tr>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getJuridicos(), false)); %>
						</td>
						<td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getSeguro(), false)); %>
						</td>
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getOtros(), false)); %>
						</td>
          </TR>
				</table>

        <BR>

<!--
        <table width="760" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td align="center" valign="top" class="texenccon">
              <span class="texencconbol">Adeudo total</span>
            </td>
          </tr>
        </table>
-->

        <table width="450" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <TR>
            <td align="center" class="tittabdat">
              Adeudo total:
						</td>
          </TR>
          <TR bgcolor="#CCCCCC">
            <td align="center" class="textabdatcla">
              $<% out.println(fm.daMonedaConComas(posicionesCE.getTotal(), false)); %>
						</td>
          </TR>
				</table>

        <BR><BR>

<% liga="ceConPosicion?ctasLineas="+ctasLineas+"&noLineas="+noLineas+"&noDisp="+noDisp+"&direccion="; %>

				<!-- #BeginLibraryItem "/Library/footerPaginacion.lbi" -->
				<table width="180" border="0" cellspacing="0" cellpadding="0" class="tabfonbla">
					<tr>
                  <td align="center">

              <!--
							<input type="image" border="0" name="direccion"
								src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22"
								alt="Anterior" onClick="dirige(-1)" >
                out.println("<A href=\""+liga+"-1\">");
              -->
              <%--
                out.println("<A href=\""+liga+"-1\">");
              --%>
              <!--<A href="<% out.println(liga+"-1"); %>" border="0">-->
              <A href="javascript:atras()" border="0">
               <IMG src="/gifs/EnlaceMig/gbo25620.gif" border="0" width="90" height="22" alt="Anterior">
              </A>
						</td>
						<td align="center">

              <!--
							<input type="image" border="0" name="direccion" value="1"
								src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22"
								alt="Siguiente" onClick="dirige(1)" >
              -->
              <%--
                out.println("<A href=\""+liga+"1\">");
              --%>
              <!--<A href="<% out.println(liga+"1"); %>" border="0">-->
              <A href="javascript:adelante()" border="0">
              <IMG src="/gifs/EnlaceMig/gbo25610.gif" border="0" width="90"
              height="22"	alt="Siguiente"></A>
						</td>
            <td align="center">
              <!--
              <input type="image" border="0" name="Imprimir"
								src="/gifs/EnlaceMig/gbo25240.gif" width="90" height="22"
								alt="Imprimir" onClick="window.print();return false">
              -->
              <A href="" onClick="window.print();return false">
                <Img src="/gifs/EnlaceMig/gbo25240.gif" width="90" height="22" border="0" alt="imprimir">
              </A>
            </td>
                  <TD align="center">
                     <A href="ceConSaldos?Modulo=0">
                        <Img src="/gifs/EnlaceMig/gbo25320.gif" border="0" width="90" height="22" alt="Regresar">
                     </A>
                  </TD>
					</tr>
				</table>
			    </td>
			</tr>
		</table>

   <% if(ctasLineas!=null && ctasLineas.length()>0)
      {
         out.println("<input type=\"hidden\" name=\"ctasLineas\" value=\""+ctasLineas+"\">");
      }

      if(noLineas!=null && noLineas.length()>0)
      {
         out.println("<input type=\"hidden\" name=\"noLineas\" value=\""+noLineas+"\">");
      }

      if(noDisp!=null && noDisp.length()>0)
      {
         out.println("<input type=\"hidden\" name=\"noDisp\" value=\""+noDisp+"\">");
      }
   %>

    <input type="hidden" name="direccion" value="">

  </form>


</Body>
</HTML>

