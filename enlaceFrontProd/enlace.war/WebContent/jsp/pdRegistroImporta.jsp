<%@page contentType="text/html"%>

<jsp:useBean id='Message' class='java.lang.String' scope='session'/>
<jsp:useBean id='listaErrores' class='java.util.ArrayList' scope='session'/>
<jsp:useBean id='Permisos' class='java.util.HashMap' scope='session'/>
<jsp:useBean id='mapaBen' class='java.util.HashMap' scope='session'/>
<jsp:useBean id='NombreDeArchivo' class='java.lang.String' scope='session'/>
<jsp:useBean id='archivoEnviado' class='java.lang.String' scope='session'/>
<jsp:useBean id='fechaTransmisionTime' class='java.lang.String' scope='session'/>
<jsp:useBean id='fechaActualizacionTime' class='java.lang.String' scope='session'/>

<%@ page import="mx.altec.enlace.bo.pdBeneficiario" %>
<%@ page import="mx.altec.enlace.dao.pdImportarArchivo" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.*" %>

<%

Double importeTotal = (Double)session.getAttribute("importeTotal");
Long numLineas = (Long)session.getAttribute("numLineas");
Long numTransmision = (Long)session.getAttribute("numTransmision");
Long registrosAceptados = (Long)session.getAttribute("registrosAceptados");
Long registrosRechazados = (Long)session.getAttribute("registrosRechazados");

java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat ("dd/MM/yyyy");

boolean Envio = false;
boolean importar = false;
boolean getbEnviado = true;

int MAX_REG=100;

if(null == NombreDeArchivo)
  NombreDeArchivo=null;
if( null == numLineas || numLineas.intValue()<0  )
  numLineas = new Long (0);
if(null == importeTotal)
  importeTotal = new Double (0);

java.text.DecimalFormat nf = new java.text.DecimalFormat();
nf .applyPattern("############0.00");
nf .setMinimumFractionDigits(2);
nf .setMaximumFractionDigits(2);
String importeTotalString = nf.format(importeTotal);

if(null == archivoEnviado)
  archivoEnviado = "NO";

if(null == numTransmision)
  numTransmision=new Long (0);
if(null == fechaTransmisionTime)
  fechaTransmisionTime="";
if(null == fechaActualizacionTime)
  fechaActualizacionTime="";
if(null == registrosAceptados)
  registrosAceptados=new Long(0);
if(null == registrosRechazados)
  registrosRechazados=new Long(0);

String nombreOriginal=NombreDeArchivo;
if(NombreDeArchivo.indexOf("/")>=0)
  NombreDeArchivo=NombreDeArchivo.substring(NombreDeArchivo.lastIndexOf("/")+1);

boolean Deshabilita = (null != NombreDeArchivo);
importar = (session.getAttribute ("Importado") == null);
session.removeAttribute ("Importado");
Envio = (numLineas.intValue()) > 0;
%>
<html>
<head>
    <title>Enlace</title>
    <meta http-equiv="Content-Type" content="text/html">

    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>

    <script language="javascript">
	<%--Scripts necesarios para el cargado de la pagina--%>

	function MM_preloadImages() { //v3.0
	    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	    var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	    if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	<%----------------------------------------------------%>

	var numPagoSel = -1;

	<%-- Funcion para preparar el registro de un pago --%>
	function alta_pago() {
	    document.pdRegistro.txtOpcion.value = 2;
	    document.pdRegistro.submit();
	}

	<%-- Funcion para enviar el archivo de pagos a Tuxedo --%>
	function enviar() {
	    document.pdRegistro.txtOpcion.value = 10;
	    document.pdRegistro.submit();
	}

	<%-- Funcion para modificar un pago en el archivo --%>
	function modifica_pago(opc) {
	    if (numPagoSel == -1) {
	        cuadroDialogo("Debe seleccionar un pago para modificar", 3);
	    } else {
	        document.pdRegistro.txtOpcion.value = 6;
			document.pdRegistro.submit();
	    }
	}

	<%-- Funcion para recuperar el estado de los pagos --%>
	function recupera() {
	    document.pdRegistro.operacion.value = "recupera";
	    cuadroCaptura ("Escriba el n&uacute;mero de secuencia:", "Recupera");
	}

	<%-- Funcion para actualizar el n�mero de pago --%>
	function actualizaNumPago (pago) {
	    numPagoSel = pago;
	}

	<%-- Funcion para borrar un pago del archivo --%>
	function borra_pago(opc) {
	    if (numPagoSel == -1) {
	        cuadroDialogo("Debe seleccionar un pago para eliminar", 3);
		} else {
	        document.pdRegistro.operacion.value = "borrapago";
	        cuadroDialogo("Esta seguro que desea borrar el pago " + numPagoSel, 2);
	    }
	}

	<%-- Funcion para preparar el borrado de un archivo --%>
	function borra_archivo() {
		document.pdRegistro.txtOpcion.value=13;
		cuadroDialogo("Esta seguro de eliminar el archivo", 2);
	}

	<%-- Funcion para preparar la creacion de un archivo --%>
	function crea_archivo() {
		document.pdRegistro.operacion.value=("crea");
		cuadroCaptura ("Escriba el nombre del archivo", "Nuevo Archivo");
	}

	<%-- Funcion para seguir la ejecuci�n despues de preguntar algo --%>
	function continuaCaptura() {
		if (document.pdRegistro.operacion.value == "crea") {
	        if (campoTexto == "") {
	            alert("Proporcione un nombre para el archivo");
	        } else if (contieneEspacios (campoTexto)) {
	            alert("Proporcione un nombre sin espacios");
	        } else {
	            if (respuesta == 1) {
	                document.pdRegistro.txtArchivo.value = campoTexto;
	                document.pdRegistro.txtOpcion.value = 1;
					document.pdRegistro.submit();
	            }
	        }
	    } else if (document.pdRegistro.operacion.value == "borra") {
	        if (respuesta == 1) {
	            document.pdRegistro.txtOpcion.value = 4;
				document.pdRegistro.submit();
	        }
	    } else if (document.pdRegistro.operacion.value == "borrapago") {
	        if (respuesta == 1) {
	            document.pdRegistro.txtOpcion.value = 5;
				document.pdRegistro.submit();
	        }
	    } else if (document.pdRegistro.operacion.value == "recupera") {
	        if (respuesta == 1) {
	            document.pdRegistro.txtOpcion.value = 11;
				document.pdRegistro.NumReferencia.value = campoTexto;
	            document.pdRegistro.submit ();
	        }
	    }
	}

	function continua() {
	    if (document.pdRegistro.operacion.value == 'borra') {
	        if (respuesta == 1) {
	            document.pdRegistro.txtOpcion.value = 4;
	            document.pdRegistro.submit ();
	        }
	    } else {
	        if (respuesta == 1) {
	            document.pdRegistro.txtOpcion.value = 5;
	            document.pdRegistro.submit ();
	        }
	    }
	}

	<%-- Funcion para importar un archivo --%>
	function importar () {
	    if (document.pdRegistro.Archivo.value != "") {
	        archivo = document.pdRegistro.Archivo.value
	        document.pdRegistro.txtArchivo.value = archivo.substring(archivo.lastIndexOf('\\')+1, archivo.length);
	        document.pdRegistro.txtOpcion.value = 9;
	        document.pdRegistro.submit ();
	    }
	}

	<%-- Funcion para deshabilitar botones cuando el archivo no ha sido creado --%>
	function deshabilita() {
		cuadroDialogo ("Debe crear un archivo.", 3);
	}

	<%-- Funcion para mostrar un mensaje de no envio --%>
	function noenvio () {
		cuadroDialogo ("No hay datos para enviar.", 3);
	}

	<%-- Funcion para checar facultades --%>
	function nofacultad (mensaje) {
		cuadroDialogo ("No tienen facultad para " + mensaje, 3);
	}

	<%-- Funcion para mostrar los errores al importar el archivo --%>
	function muestraErrores() {
		msg = window.open("pdRegistro?txtOpcion=12","Errores","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=460,height=200");
		msg.focus();
	}

	function contieneEspacios (nombre) {
		for (x = 0; x < nombre.length; x++) {
			if (nombre.charAt(x) == ' ') return true;
		}
		return false;
	}

	<% if( request.getAttribute("newMenu") != null ) out.print( request.getAttribute("newMenu") ); %>
	</script>

    <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');
<%=(request.getAttribute("despliegaEstatus" )==null)?"":request.getAttribute("despliegaEstatus" )%>;<%if (!Message.equals("")) out.println("cuadroDialogo('" + Message + "', 1)");%><%if (!listaErrores.isEmpty ()) out.print ("muestraErrores();");%>"
background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*">
			<!-- MENU PRINCIPAL -->
			<%= request.getAttribute("MenuPrincipal") %>
		</td>
	</tr>
</table>

<%= request.getAttribute("Encabezado" ) %>

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form enctype="multipart/form-data" name="pdRegistro" method="POST" action="pdRegistro">
    <tr>
      <td align="center">
        <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="2">
              <table width="460" border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat" colspan="2"> Datos del archivo</td>
                </tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="450" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                        <td width="270" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Archivo:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <input type="text" name="txtArchivo" size="22" class="tabmovtex" value="<%=NombreDeArchivo%>"  onFocus='blur()'>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>N&uacute;mero de secuencia:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <%--Checar el numero de transaccion--%>
                                <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=numTransmision%>" name="transmision" onFocus='blur();'>
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=fechaTransmisionTime%>" name="fechaTrans" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Fecha
                                de actualizaci&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=fechaActualizacionTime%>" name="fechaAct" onFocus='blur();' >
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Total de registros:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
							   <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=numLineas%>" name="totRegs" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Registros aceptados:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
							   <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=registrosAceptados%>" name="aceptados" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Registros
                                rechazados: </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
							   <input type="text" size="22" align="rigth" class="tabmovtex" value="<%=registrosRechazados%>" name="rechazados" onFocus='blur();' >
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="200" height="94" align="center" valign="middle">
            <%
                if (((Boolean) Permisos.get ("Envio")).booleanValue()) {
            %>
                <a href = "javascript:crea_archivo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>
            <%
                } else {
            %>
                <a href='javascript:nofacultad("crear archivos.");'><IMG src='/gifs/EnlaceMig/gbo25550.gif' border='0' alt='Crear archivo' width='115' height='22'></a>
            <%
                }
            %>
            </td>
          </tr>
          <tr>
            <td align="center" valign="bottom">
				<table width="200" border="0" cellspacing="2" cellpadding="3">
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" border="0" cellspacing="5" cellpadding="0">
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>
                          Importar archivo</td>
                      </tr>
                      <tr>
                        <td nowrap>
						 <input type="file" name="Archivo" size="15">
                        </td>
                      </tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
                        <%
                            if (Envio) {
                        %>
                            <a href='javascript:cuadroDialogo("No puede importar un archivo si tiene otro abierto", 3);'><IMG src='/gifs/EnlaceMig/gbo25280.gif' border='0' alt='Aceptar' width='80' height='22'></a>
                        <%
                            } else if (((Boolean) Permisos.get ("Importar")).booleanValue()) {
                        %>
							<a href = "javascript:importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
                        <%
                            } else {
                        %>
                            <a href='javascript:nofacultad("importar archivos.");'><IMG src='/gifs/EnlaceMig/gbo25280.gif' border='0' alt='Aceptar' width='80' height='22'></a>
                        <%
                            }
                        %>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>

		<!-- Pagos //-->
		<table width="600" border="0" cellspacing="2" cellpadding="3">

     <%
		if( ! archivoEnviado.trim().equals("SI") )
		  {
     %>
 			<tr>
			  <td class="tittabdat" > Informaci&oacute;n del archivo Importado</td>
			</tr>
			<tr>
			   <td class="textabdatcla">
				  <table width="100%">
					<tr>
					  <td class="tabmovtex" nowrap>Se importaron  <font color=blue><b><%=numLineas.longValue ()%></b></font> registros satisfactoriamente.</td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>Con un importe total de <font color=blue><b>$ <%=importeTotalString%></b></font></td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>N&uacute;mero de secuencia: <font color=blue><b><%=numTransmision%></b></font></td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n: <font color=blue><b><%=fechaTransmisionTime%></b></font></td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>Fecha de actualizaci&oacute;n: <font color=blue><b><%=fechaActualizacionTime%></b></font></td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>Registros aceptados: <font color=blue><b><%=registrosAceptados%></b></font></td>
					</tr>
					<tr>
					  <td class="tabmovtex" nowrap>Registros rechazados: <font color=blue><b><%=registrosRechazados%></b></font></td>
					</tr>
				  </table>
				</td>
			</tr>

			<tr>
			  <td><br></td>
			<tr>

			<%
			if( numLineas.intValue()>MAX_REG) {
			%>
			<tr>
				<td class="tittabdat" > Archivo Importado</td>
			</tr>
			<tr>
				<td class="textabdatcla"><br>&nbsp;El Archivo contiene mas de <%=MAX_REG%> registros. <br>&nbsp;La informaci&oacute;n contenida no se presentar&aacute; en pantalla, para asegurar una mayor eficiencia en el proceso.<br><br>&nbsp;</td>
			</tr>
			<%
			}
			else {
			%>
			<tr>
				<td class="tabmovtex" width="100%">
				<%
				String Linea="";
				String color="textabdatobs";

				int varAux=0;

				mx.altec.enlace.bo.pdPago Temp = new mx.altec.enlace.bo.pdPago ();
				pdBeneficiario ben = (pdBeneficiario) mapaBen.get (Temp.getClaveBen ());
				%>
				<table border=0 cellpadding=2 cellspacing=3 width="100%">

					<tr>
						<td align='center' class='tittabdat' width='80'>Cuenta de cargo</td>
						<td align='center' class='tittabdat' width='60'>No. Pago</td>
						<td align='center' class='tittabdat' width='200'>Beneficiario</td>
						<td align='center' class='tittabdat' width='80'>Importe</td>
						<td align='center' class='tittabdat' width='70'>Fecha de libramiento</td>
						<td align='center' class='tittabdat' width='70'>Fecha l&iacute;mite de pago</td>
						<% if (Envio) {%>
						<td align='center' class='tittabdat' width='180'>Estatus Pago</td>
			<%}%>
					</tr>
					<%
					try {
						java.io.RandomAccessFile archivoTrabajo = new java.io.RandomAccessFile( new File( nombreOriginal), "r" );
						archivoTrabajo.seek(0);

						while( (Linea=archivoTrabajo.readLine() ) !=null) {
							/*Temp.setCuentaCargo (Linea.substring (0, 16).trim ());
							Temp.setNoPago (Linea.substring (16,36).trim ());
							Temp.setFechaLib (Linea.substring (36,46).trim ());
							Temp.setFechaPago (Linea.substring (46, 56).trim ());
							Temp.setClaveBen (Linea.substring (56, 69).trim ());
							Temp.setNomBen (Linea.substring (69, 129).trim ());
							Temp.setTSucursales (Linea.charAt(129));
							Temp.setClaveSucursal (Linea.substring (130, 134).trim ());
							Temp.setFormaPago (Linea.charAt (134));
							Temp.setImporteSin (Linea.substring (135, 151).trim ());
							Temp.setConcepto (Linea.substring (151).trim ());*/

							// Se declara variable para seleccion de layout a trabajar
							// (1 para 12 referencia a posiciones o 2 para referencia a 20 posiciones)
							String numLayout = "1";
							if (Linea.length () == 211) numLayout = "2";

							pdImportarArchivo.cargaLayout();

							int posicion1 = 0;
							int posicion2 = 0;

							// Cuenta Cargo 16 caracteres
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 0);
							Temp.setCuentaCargo (Linea.substring(posicion1, posicion2).trim());
							// Num Pago 12 o 20 caracteres
							posicion1 = posicion2;
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 1);
							Temp.setNoPago (Linea.substring (posicion1, posicion2).trim());
							// Fecha Lib 10 caracteres
							posicion1 = posicion2;
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 2);
							Temp.setFechaLib(Linea.substring (posicion1, posicion2).trim());
							// Fecha Pago 10 caracteres
							posicion1 = posicion2;
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 3);
							Temp.setFechaPago(Linea.substring (posicion1, posicion2).trim());
							// Beneficiario 13 caracteres
							posicion1 = posicion2;
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 4);
							Temp.setClaveBen(Linea.substring (posicion1, posicion2).trim());
							// Nombre Beneficiario 60 caracteres
							posicion1 = posicion2;
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 5);
							Temp.setNomBen(Linea.substring (posicion1, posicion2).trim());
							// Todas las sucursales 1 caracter
							posicion1 = pdImportarArchivo.getFieldSize(numLayout, 5);
							Temp.setTSucursales(Linea.charAt(posicion1));
							// Clave de sucursal 4 caracteres
							posicion1 = pdImportarArchivo.getFieldSize(numLayout, 6);
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 7);
							Temp.setClaveSucursal(Linea.substring (posicion1, posicion2).trim());
							// Forma de Pago 1 caracter
							posicion1 = pdImportarArchivo.getFieldSize(numLayout, 7);
							Temp.setFormaPago(Linea.charAt(posicion1));
							// Importe 16 caracteres
							posicion1 = pdImportarArchivo.getFieldSize(numLayout, 8);
							posicion2 = pdImportarArchivo.getFieldSize(numLayout, 9);
							Temp.setImporteSin(Linea.substring (posicion1, posicion2).trim());
							// Concepto 60 caracteres
							posicion1 = pdImportarArchivo.getFieldSize(numLayout, 9);
							Temp.setConcepto(Linea.substring(posicion1).trim());

							color=(varAux%2==0)?"textabdatobs":"textabdatcla";
							varAux++;
					%>
					<tr>
						<td align="center" class="<%=color%>" ><%=Temp.getCuentaCargo ()%></td>
						<td class="<%=color%>" align="right"><%=Temp.getNoPago ()%></td>
						<td class="<%=color%>"  align="left">
						<%
						if (Temp.getNomBen () != null && !Temp.getNomBen ().equals ("")) {
						%>
							<%=Temp.getNomBen ()%>
						<%
						}
						else {
							ben = (pdBeneficiario) mapaBen.get (Temp.getClaveBen ());
							if(null != ben) {
						%>
								<%=ben.getNombre ()%>
						<%
							}
							else {
						%>
								Beneficiario no registrado
						<%
							}
						}
					%>
						</td>
						<td class="<%=color%>" nowrap align="right"><%=(Temp.getImporteFor ())%></td>
						<td class="<%=color%>" nowrap align="center"><%=sdf.format (Temp.getFechaLib ().getTime ())%></td>
						<td class="<%=color%>" nowrap align="center"><%=sdf.format (Temp.getFechaPago ().getTime ())%></td>
						<%
						if (Envio) {
						%>
						<td class="<%=color%>" nowrap align='left'><%=Temp.getDescripcionEstatus()%>&nbsp;</td>
						<%
						}
						%>
					</tr>
						 <%
						}
					}catch(Exception e) {
						System.out.println(" Error en la lectura del archivo."+e);
					}
					%>
				</table>
			   </td>
			 </tr>
			<%
			   }
			%>
		<%
		  }
		else
		  {
			String importOk=(String)request.getAttribute("IMPORT_OK");
			importOk=(importOk==null)?"NO":importOk;
			if(importOk.trim().equals("SI"))
			  {
			%>
				<tr>
				  <td class="tittabdat" > Informaci&oacute;n del archivo Enviado</td>
				</tr>
				<tr>
				   <td class="textabdatcla">
					  <table width="100%">
						<tr>
						  <td class="tabmovtex" nowrap>Se enviaron  <font color=blue><b><%=numLineas.longValue ()%></b></font> registros.</td>
						</tr>
						<tr>
						  <td class="tabmovtex" nowrap>Con un importe total de <font color=blue><b>$ <%=importeTotalString%></b></font></td>
						</tr>
    					<tr>
    					  <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n: <font color=blue><b><%=fechaTransmisionTime%></b></font></td>
    					</tr>
    					<tr>
    					  <td class="tabmovtex" nowrap>Fecha de actualizaci&oacute;n: <font color=blue><b><%=fechaActualizacionTime%></b></font></td>
    					</tr>
    					<tr>
    					  <td class="tabmovtex" nowrap>Registros aceptados: <font color=blue><b><%=registrosAceptados%></b></font></td>
    					</tr>
    					<tr>
    					  <td class="tabmovtex" nowrap>Registros rechazados: <font color=blue><b><%=registrosRechazados%></b></font></td>
    					</tr>
					    <tr>
					     <td class="tabmovtex"><%if (!Message.equals("")) out.println(Message); else out.println("No se recibio el mensaje.");%></td>
					    </tr>
					  </table>
					</td>
				</tr>

				<tr>
				  <td><br></td>
				<tr>
			<%
			   }
			  else
			   {
			%>
				<tr>
				  <td class="tittabdat" > Informaci&oacute;n del archivo Enviado</td>
				</tr>
				<tr>
				   <td class="textabdatcla">
					  <table width="100%">
						<tr>
						  <td class="tabmovtex" nowrap>No se pudo enviar el archivo importado.</td>
						</tr>
						<tr>
						  <td class="tabmovtex" nowrap>Estatus: <font color=blue><b> <%if (!Message.equals("")) out.println(Message); else out.println("Error al eviar el archivo.");%></b></font></td>
						</tr>
					  </table>
					</td>
				</tr>

				<tr>
				  <td><br></td>
				<tr>
			<%
			   }
			%>
		<%
		  }
		%>

		</table>
		<p><br>

        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="66">
            <%
                if (Deshabilita)
				 {
            %>
                    <a href = "javascript:deshabilita();"><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            <%
                } else if (!((Boolean) Permisos.get ("Alta")).booleanValue()) {
            %>
                    <a href='javascript:nofacultad ("altas manuales");'><img src='/gifs/EnlaceMig/gbo25480.gif' border='0' alt='Alta' width='66' height='22'></a>
            <%
                } else {
            %>
                    <a href = "javascript:alta_pago();"><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="127">


                <%
                 if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
                %>
                    <a href = "javascript:borra_pago(2);"><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
                <%
                    } else {
                %>
                    <a href='javascript:nofacultad("borrar registros.")'><IMG src='/gifs/EnlaceMig/gbo25500.gif' border='0' alt='Baja de registros' width='127' height='22'></a>
                <%
                    }
                %>
            </td>
            <td align="left" valign="top" width="93">
                <%

				 if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
                %>
                    <a href = "javascript:modifica_pago(2);"><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
                <%
                    } else {
                %>
                    <a href='javascript:nofacultad("modificar registros.");'><IMG src='/gifs/EnlaceMig/gbo25510.gif' border='0' width='93' height='22'></a>
                <%
                    }
                %>
            </td>
            <td align="left" valign="top" width="83">
            <%
                if (((Boolean) Permisos.get ("Imprimir")).booleanValue()) {
            %>
                <a href = "javascript:self.print ();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>
            <%
                } else {
            %>
                <a href='javascript:nofacultad("Imprimir.")'><IMG src='/gifs/EnlaceMig/gbo25240.gif' border='0' alt='Imprimir' width='83' height='22'></a>
            <%
                }
            %>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="78">
            <%
                if (!Envio) {
            %>
                <a href='javascript:noenvio();'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></a>
            <%
                } else if (Deshabilita && importar) {
            %>
                <a href='javascript:deshabilita();'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></a>
            <%
                } else if (((Boolean) Permisos.get ("Envio")).booleanValue() && !getbEnviado) {
            %>
                <a href = "javascript:enviar();"><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            <%
                } else if (((Boolean) Permisos.get ("Modificar")).booleanValue() && getbEnviado) {
            %>
                <a href = "javascript:enviar();"><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            <%
                } else if (!getbEnviado) {
            %>
                <a href='javascript:nofacultad("enviar archivos.")'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></a>
            <%
                } else {
            %>
                <a href='javascript:nofacultad("modificar archivos enviados.")'><IMG src='/gifs/EnlaceMig/gbo25520.gif' border='0' alt='Enviar' width='78' height='22'></a>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="97">
            <%
                if (((Boolean) Permisos.get ("Recuperar")).booleanValue()) {
            %>
                <a href = "javascript:recupera();"><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>
            <%
                } else {
            %>
                <a href='javascript:nofacultad("recuperar archivos.")'><IMG src='/gifs/EnlaceMig/gbo25530.gif' border='0' alt='Recuperar' width='97' height='22'></a>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="77">
            <%
                if (Deshabilita && importar) {
            %>
                <a href='javascript:deshabilita();'><IMG src='/gifs/EnlaceMig/gbo25540.gif' border='0' alt='Borrar' width='77' height='22'></a>
            <%
                } else if (((Boolean) Permisos.get ("Manipulacion")).booleanValue()) {
            %>
                <a href = "javascript:borra_archivo();"><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>
            <%
                } else {
            %>
                <a href='javascript:nofacultad("borrar el archivo.")'><IMG src='/gifs/EnlaceMig/gbo25540.gif' border='0' alt='Borrar' width='77' height='22'>
            <%
                }
            %>
            </td>
            <td align="left" valign="top" width="85">
                <%
                    if (request.getSession().getAttribute("URLArchivo") != null) {
                %>
                <a href='/Download/<%=request.getSession().getAttribute("URLArchivo")%>'>
                    <img src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar' width='85' height='22'>
                </a>
                <%
                    }
                %>
            </td>
			<!--  Modificacion por everis de M�xico 29 05 2007 DVS M�dulo Seguimiento de Transacciones -->
			<td align="left" valign="top" width="85">
                <a href="consultaServlet?entrada=1" border=0><img src='/gifs/EnlaceMig/consulta_transferencias.gif' border=0 alt='Enviar Consulta'></a>
            </td>
			<!--  Fin Modificaci�n 29 05 2007 -->
          </tr>
        </table>
        <br>
      </td>
    </tr>
    <input type='hidden' name='txtOpcion' value='0'>
    <input type='hidden' name='operacion' value=''>
    <input type='hidden' name='NumReferencia' value=''>
  </form>
</table>

</body>
</html>
<%
session.removeValue ("Message");
%>