<html>
<!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace Banco Santander Mexicano</title>
<!-- #EndEditable --> <!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26360">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- #EndEditable -->
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<script language="JavaScript">
<!--

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
   d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
/*ivn Ma. Isabel Valencia Navarrete
  Q24562 27/Dic/2004 Praxis
  Cambio de n�mero telefonico */

<%= request.getAttribute("newMenu") %>

//-->
</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
	   <%= request.getAttribute("MenuPrincipal") %>
       </TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<!-- #EndLibraryItem --><!-- #BeginEditable "Contenido" -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <form name="form1" method="post" action="">
    <tr>
      <td width="200" valign="top">
        <table width="200" border="0" cellspacing="5" cellpadding="0">
          <tr>
           <!-- <td><img src="/gifs/EnlaceMig/gti25050.gif" width="57" height="25"></td>-->
          </tr>
        </table>
        <table width="200" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11">
              <blockquote>
               <!-- <p>&iquest;Ya revis&oacute;<a href="javascript:hlp=window.open( 'http://www.santander.com.mx/html/website/s55980.html', 'Faqs', 'toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=770,height=400'); hlp.focus();"> nuestra base de preguntas
                  y respuestas</a>?. Nuestros clientes generalmente encuentran
                  soluci&oacute;n a sus dudas en nuestros FAQ,s.</p>-->
                <p>Nuestro departamento de servico a clientes le atender&aacute;
                  gustosamente al recibir la siguiente forma.</p>
              </blockquote>
            </td>
          </tr>
        </table>
      </td>
      <td bgcolor="#CCCCCC" width="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
      <td width="358" valign="top">
        <table width="358" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="tabmovtex11" valign="top">
              <table border="0" cellspacing="3" cellpadding="0" width="348">
                <tr>
                  <td class="tabmovtex11">
                    <p>Estimado(a) <%= request.getAttribute("nombreUsuario") %>,
					   nuestros n&uacute;meros de atenci&oacute;n telef&oacute;nica son:</p>
                    <!--ivn Q24562
                      <p class="tabmovtexbol">En el D.F.: 51-40-82-80<br>-->
                    <p class="tabmovtexbol">En el D.F.: 51-69-43-43<br>
                      Resto del pa&iacute;s: 01-800-509-5000</p>
                    <p>Con gusto le atenderemos de lunes a viernes a partir de
                      las <span class="tabmovtexbol">8:30 y hasta las 19:30 hrs.</span><br>
                      Nuestros asesores especializados le ayudar&aacute;n a resolver
                      problemas:</p>
                    <ul>
                      <li>Con la aplicaci&oacute;n</li>
                      <li>De comunicaci&oacute;n</li>
                      <li>De configuraci&oacute;n con su navegador</li>
                      <li>Otros relacionados con Enlace por Internet</li>
                    </ul>
                    <p>Adicionalmente, le dar&aacute;n asesor&iacute;a para la
                      contrataci&oacute;n de otros servicios de Enlace; le proporcionar&aacute;n
                      informaci&oacute;n sobre tarifas y horarios y le guiar&aacute;n
                      en cualquier duda con la operaci&oacute;n del sistema.</p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td bgcolor="#CCCCCC" width="1"><img src="/gifs/EnlaceMig/gau25010.gif" width="1" height="1"></td>
      <td width="200" align="center" valign="top">
        <p>&nbsp;</p>
        <p><img src="/gifs/EnlaceMig/gil25020.gif" width="168" height="168"></p>
      </td>
    </tr>
  </form>
</table>
<!-- #EndEditable -->
</body>
<!-- #EndTemplate -->
</html>