
	<%@ page import="java.util.StringTokenizer" %>
	<%@ page import="java.util.Vector" %>
	<%@ page import="mx.altec.enlace.bo.TI_CuentaFondeo" %>

	<%
			// --- Obtenci�n de par�metros -----------------------------------------------------
			String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
			String parFuncionesMenu = (String)request.getAttribute("newMenu");
			String parEncabezado = (String)request.getAttribute("Encabezado");
			String parCuentas = (String)request.getAttribute("Cuentas");
			String parCuentasDesc = (String)request.getAttribute("CuentasDesc");
			String parMensaje = (String)request.getAttribute("Mensaje");
			String parTrama = (String)request.getAttribute("Trama");

			if(parMenuPrincipal == null) parMenuPrincipal = "";
			if(parFuncionesMenu == null) parFuncionesMenu = "";
			if(parEncabezado == null) parEncabezado = "";
			if(parCuentas == null) parCuentas = "";
			if(parCuentasDesc == null) parCuentasDesc = "";
			if(parMensaje == null) parMensaje = "";
			if(parTrama == null) parTrama = "";

			// --- Se preparan otras variables -------------------------------------------------
			Vector cuentas = ((new mx.altec.enlace.servlets.TIFondeo()).creaCuentas(parCuentas, request));
			String nombreCuenta = parTrama.substring(1,parTrama.indexOf("|"));
			int cont;
			for(cont=0;cont<cuentas.size() && !((TI_CuentaFondeo)cuentas.get(cont)).getNumCta().equals(nombreCuenta);cont++);
			TI_CuentaFondeo cuenta = (TI_CuentaFondeo)cuentas.get(cont);
			boolean esEditable = (cuenta.nivel() > 1);
	%>

<!--== COMIENZA CODIGO HTML ==========================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Bienvenido a Enlace Internet</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Rafael Villar Villar">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts =====================================================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>

	// --- BOTONES ---------------------------------------------------------------------
	function modificar()
		{
		var trama = "@";
		var a, b=0;
		var esEditable = <%= esEditable %>;

		//Se valida
		if(esEditable && !importeEsValido())
			{aviso("Indique un importe v&aacute;lido"); return;}

		if(esEditable && !document.Forma.Fondeo[0].checked && !document.Forma.Fondeo[1].checked)
			{aviso("Indique un sentido para el fondeo"); return;}

		if (!esEditable && (document.Forma.Importe.value != "") && habilitaConfirma)
			{
			confirma("Ha seleccionado agregar una cuenta en el nivel 1; por lo que se van a ignorar los datos capturados\n\n&iquest;Desea continuar?");
			accionConfirma = 1;
			return;
			}

		if(!esEditable)
			{
			document.Forma.Fondeo[0].checked = false;
			document.Forma.Fondeo[1].checked = false;
			document.Forma.Importe.value = "$0.00";
			}

		//Se construye la trama
		trama += "<%= cuenta.getNumCta() %>" + "|";
		trama += "<%= (cuenta.posiblePadre.equals(""))?" |":(cuenta.posiblePadre + "|") %>";
		trama += "<%= cuenta.getDescripcion() %>" + "|";

		trama += limpiaImporte();
		trama += (document.Forma.Fondeo[0].checked)?"|2":((document.Forma.Fondeo[1].checked)?"|1":"|0");

		document.Forma.Accion.value = "EDITA_MODIFICA";
		document.Forma.Trama.value = trama;
		document.Forma.submit();
		}

	function anterior()
		{
		document.Forma.Trama.value = "<%= parTrama.substring(1,parTrama.indexOf("|")) %>"
		document.Forma.Accion.value = "EDITA_ANTERIOR";
		document.Forma.submit();
		}

	function siguiente()
		{
		document.Forma.Trama.value = "<%= parTrama.substring(1,parTrama.indexOf("|")) %>"
		document.Forma.Accion.value = "EDITA_SIGUIENTE";
		document.Forma.submit();
		}

	function regresar()
		{
		document.Forma.Accion.value = "REGRESA";
		document.Forma.submit();
		}

	// --- Validaciones ----------------------------------------------------------------
	function limpiaImporte()
		{
		var valor = quitaComas(document.Forma.Importe.value);
		var car;

		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		if(valor.substring(0,1)=="$") valor=valor.substring(1);
		while(valor.length>0 && valor.substring(0,1) == " ") valor = valor.substring(1);
		while(valor.length>0 && valor.substring(valor.length-1) == " ")
			valor = valor.substring(0,valor.length-1);
		return valor;
		}

	function importeEsValido()
		{
		var valor = limpiaImporte();
		if(valor.substring(0,1)== ".") return false;
		if(valor.indexOf(".")!=-1) if(valor.indexOf(".")+3 < valor.length) return false;
		if(valor == "") return false;
		while(valor.length > 0)
			{
			car = valor.substring(0,1);
			if(isNaN(parseInt(car)) && car!="0" && car!=".") return false;
			valor = valor.substring(1);
			}
		return true;
		}

	function quitaComas(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.charAt(a) == ',') {texto = texto.substring(0,a) + texto.substring(a+1); a--;}
		return texto;
		}

	// --- Para cuadros de di�logo -----------------------------------------------------
	var respuesta = 0;
	var accionConfirma;
	var habilitaConfirma = true;

	function continua()
		{
		if(accionConfirma == 1)
			{
			if(respuesta == 1) modificar();
			habilitaConfirma = true;
			}
		else
			{
			if(respuesta == 1)
				{
				document.Forma.Accion.value = "BORRA_ARBOL";
				document.Forma.submit();
				}
			habilitaConfirma = true;
			}
		}

	function confirma(mensaje)
		{if(habilitaConfirma) cuadroDialogo(mensaje,2); habilitaConfirma = false;}

	function aviso(mensaje) {cuadroDialogo(mensaje,1);}

	function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

	// --- Inicio y env�o --------------------------------------------------------------
	function inicia()
		{
		document.Forma.Fondeo[0].checked = <%= cuenta.getFondeo() == 2 %>;
		document.Forma.Fondeo[1].checked = <%= cuenta.getFondeo() == 1 %>;
		document.Forma.Importe.value = "<%= cuenta.getStrImporte() %>";
		}

	function validaEnvio()
		{
		if(document.Forma.Accion.value == "") return false;
		return true;
		}

	// --- Funciones de men� -----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0)
				{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->

<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>




	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top"><td width="*">
	<%= parMenuPrincipal %>
	</td></tr></table>
	<%= parEncabezado %>




	<FORM name=Forma  onSubmit="return validaEnvio()" action="TIFondeo">
	<INPUT type=Hidden name="Accion" value="">
	<INPUT type=Hidden name="Trama" value="<%= cuenta.getNumCta() %>">
	<INPUT type=Hidden name="Cuentas" value="<%= parCuentas %>">

	<DIV align=center>

	<TABLE width=400px class="textabdatcla" border=0 cellspacing=0>
	<TR>
		<TD class="tittabdat">
		&nbsp;Especifique las condiciones de fondeo autom&aacute;tico
		</TD>
	</TR>
	<TR>
		<TD class='tabmovtex'>


			<TABLE>
			<TR>
				<TD width=300 class='tabmovtex'>&nbsp;Cuenta:</TD>
				<TD class='tabmovtex'>Nivel:</TD>
			</TR>
			<TR>
				<TD class='tabmovtex'>&nbsp;<%= cuenta.getNumCta() + " " + cuenta.getDescripcion()%></TD>
				<TD class='tabmovtex'><%= cuenta.nivel() %></TD>
			</TR>
			</TABLE>

			<BR><BR>

			<TABLE>
			<TR>
				<TD width=200 class="tabmovtex">Sentido del fondeo:</TD>
				<TD width=200 class="tabmovtex">Importe m&aacute;ximo:</TD>
			</TR>
			<TR valign=top>
				<TD style="font-weight:normal;" class="tabmovtex">
					<INPUT type="Radio" name="Fondeo" value="Doble"> Doble<BR>
					<INPUT type="Radio" name="Fondeo" value="Unico"> Unico
				</TD>
				<TD class="tabmovtex">
					<INPUT type="Text" name="Importe" size=10>
				</TD>
			</TR>
			</TABLE>

			<BR><BR>

		</TD>
	</TR>
	</TABLE>

	<DIV>
	<BR>
	<A href="javascript:modificar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25510.gif" alt="Modificar"></A><A href="javascript:anterior()"><IMG border=0 src="/gifs/EnlaceMig/gbo25620.gif" alt="Anterior"></A><A href="javascript:siguiente()"><IMG border=0 src="/gifs/EnlaceMig/gbo25610.gif" alt="Siguiente"></A><A href="javascript:regresar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></A>
	<BR>
	</DIV>

	</DIV>

	</FORM>

</BODY>

</HTML>

<%
	if(!parMensaje.equals(""))
		{
		String prefijo = parMensaje.substring(0,3);
		parMensaje = parMensaje.substring(3);
		if(prefijo.equals(":-)")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>");
		if(prefijo.equals(":-/")) out.println("<SCRIPT>avisoError('" + parMensaje + "');</SCRIPT>");
		}
%>
