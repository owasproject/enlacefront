<html>
<head>
	<title>TESOFE - Consulta de Archivos y Registros</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

<%
if(request.getAttribute("estatusOperacion")!=null)
		out.println(request.getAttribute("estatusOperacion"));
%>

function js_cancelaArchivos()
 {
   var valor="";
   for(i=0;i<document.TesConsulta.length;i++)
	 if(document.TesConsulta.elements[i].type=='radio' && document.TesConsulta.elements[i].checked)
	   valor = document.TesConsulta.elements[i].value;
   if(valor.indexOf("|R|")<0)
	 {
	   cuadroDialogo("Solo se permite la cancelaci&oacute;n de los archivos Recibidos",3);
	   return ;
	 }

   document.TesConsulta.action="MTE_ConsultaCancelacion";
   document.TesConsulta.operacion.value="C";
   document.TesConsulta.ventana.value='3';
   document.TesConsulta.submit();
 }

function js_enviaDetalle()
 {
   correo=document.TesConsulta.correo.value;
   var valor="";

   if(isEmpty(correo))
	  cuadroDialogo("Debe especificar el correo para esta operaci&oacute;n.",1);
   else
	{
	    for(i=0;i<document.TesConsulta.length;i++)
		 if(document.TesConsulta.elements[i].type=='radio' && document.TesConsulta.elements[i].checked)
		   valor = document.TesConsulta.elements[i].value;
	    if(valor.indexOf("|M|")>=0 || valor.indexOf("|C|")>=0)
		 {
		   cuadroDialogo("El detalle de archivos no esta disponible para los archivos Cancelados.",3);
		   return ;
		 }

		if(verificaCorreo(correo))
		 {
		   document.TesConsulta.action="MTE_ConsultaCancelacion";
		   document.TesConsulta.ventana.value='3';
		   document.TesConsulta.operacion.value="D";
		   document.TesConsulta.submit();
		 }
		else
		 cuadroDialogo("Debe especificar un correo v&aacute;lido.",2);
	}
 }

function js_regresarConsulta()
 {
   document.TesConsulta.action="MTE_ConsultaCancelacion";
   document.TesConsulta.ventana.value='0';
   document.TesConsulta.submit();
 }

function BotonPagina(sigant)
 {
   document.TesConsulta.action="MTE_ConsultaCancelacion";
   document.TesConsulta.submit();
 }

function verificaCorreo(correo)
 {
	var a=correo.indexOf("@");
	var dotfin=correo.lastIndexOf(".");
	var dotini=correo.indexOf(".");
	if(a<1 || dotfin<a || (dotfin==(a+1)) || (dotfin==correo.length-1) || dotfin<1 || (dotini==(a+1)))
	  return false;
    return true;
 }

function isEmpty(s)
{
  return ((s == null) || (trimString(s).length == 0));
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="//gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>


 <FORM   NAME="TesConsulta" METHOD="Post" ACTION="MTE_ConsultaCancelacion">

 <table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td align=center>
   <%
   	String Fecha1_01=(String)request.getAttribute("Fecha1_01");
	String Fecha1_02=(String)request.getAttribute("Fecha1_02");
	String nombreArchivo=(String)request.getAttribute("nombreArchivo");
	String importe=(String)request.getAttribute("importe");
	String secuencia=(String)request.getAttribute("secuencia");
	String estatusArchivo=(String)request.getAttribute("estatusArchivo");
   %>
   <table border=0>
    <tr>
	 <td>

	 <!-- Tabla de registros obtenidos //-->
	 <%=request.getAttribute("tablaRegistros")%>
	 <!-- fint Tabla //-->

 	  <br>
	  </table>
	   <table align=center>
		<tr>
		 <td class="tabmovtex"  align=center nowrap>
		   <br>Correo: <input type=text name=correo size=25><br>
		 </td>
		</tr>
	   </table>
	   <br>
	   <table align=center border=0 cellspacing=0 cellpadding=0>
		 <tr>
		   <!--<td><A href ="javascript:js_cancelaArchivos();" border = 0><img src = "/gifs/EnlaceMig/gbo25190.gif" border=0 alt=Cancelar></a></td> //-->
		   <td><A href ='javascript:scrImpresion();' border = 0><img src = "/gifs/EnlaceMig/gbo25240.gif" border=0 alt=Imprimir></a></td>
		   <td><%=request.getAttribute("Exportar")%></td>
		   <td><A href = 'javascript:js_enviaDetalle();' border = 0><img src = "/gifs/EnlaceMig/gbo25245.gif" border=0 alt='Enviar detalle'></a></td>
		   <td><A href = 'javascript:js_regresarConsulta();' border = 0><img src = "/gifs/EnlaceMig/gbo25320.gif" border=0 alt=Regresar></a></td>
		 </tr>
	   </table>
	   <br>
	  </table>

	</td>
   </tr>
  </table>

	<input type=hidden name=Fecha1_01 value='<%=Fecha1_01%>'>
	<input type=hidden name=Fecha1_02 value='<%=Fecha1_02%>'>
	<input type=hidden name=importe value='<%=importe%>'>
	<input type=hidden name=secuencia value='<%=secuencia%>'>
	<input type=hidden name=estatusArchivo value='<%=estatusArchivo%>'>
	<input type=hidden name=nombreArchivo value='<%=nombreArchivo%>'>

	<input type=hidden name=ventana value='3'>
	<input type=hidden name=operacion>

   </td>
  </tr>
 </table>

 </FORM>

</body>
</html>