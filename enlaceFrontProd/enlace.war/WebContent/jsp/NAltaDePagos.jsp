<%@ page import="mx.altec.enlace.bo.ObtieneFacturas"%>

<%@ page import="mx.altec.enlace.dao.CombosConf"%>
<%@ page import="java.util.*"%>


<%@page import="mx.altec.enlace.bo.Archivos"%>
<%@page import="mx.altec.enlace.bo.Archivos2"%>
<%@page import="mx.altec.enlace.bo.claveTrans"%>
<%@page import="mx.altec.enlace.bo.claveDoc"%>
<HTML>
<jsp:useBean id="arch_combos" class="mx.altec.enlace.dao.CombosConf" scope="request"/>
<HEAD>
<TITLE> Datos del archivo</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Bitacora.js"></script>
<SCRIPT LANGUAGE="Javascript">
var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>"
var Nmuestra = 30;
var numero_documento_inicial="";
var js_Proveedor="";
function continua(){
	if (respuesta==1) document.AltaDePagos.submit();
}
function js_limpiar(){
	if (document.AltaDePagos.operacion.value=="alta") document.AltaDePagos.reset();
}



function checaLongitud(cajatexto, longitud){
	cadena=cajatexto.value;
	diferencia=longitud-cadena.length;
	if (cadena.length<longitud){
		for(var i=0;i<diferencia;i++)
			cadena =cadena +" ";
	}
	cajatexto.value=cadena;
}

function js_Proveedor(){
	 js_Proveedor = document.AltaDePagos.Proveedor.options[document.AltaDePagos.Proveedor.selectedIndex]
}



function longitudCampos(){
	checaLongitud(document.AltaDePagos.Proveedor.options[document.AltaDePagos.Proveedor.selectedIndex],20)
	checaLongitud(document.AltaDePagos.NumeroDocumento,8);
	checaLongitud(document.AltaDePagos.ImporteDocumento,21);
}

function valida_campos(){
	if (document.AltaDePagos.NumeroDocumento.value=="") {
		cuadroDialogo ("El campo NUMERO DE DOCUMENTO es requerido.",3);
		return false;
		}
	if (document.AltaDePagos.ImporteDocumento.value=="") {
		cuadroDialogo ("El campo IMPORTE es requerido.",3);
		return false;
		}
	longitudCampos();
	return true;
}


function  js_aceptar(){
   document.AltaDePagos.action="coPagos?opcion=3";
   if (document.AltaDePagos.operacion.value=="alta" ||                                        document.AltaDePagos.operacion.value=="modificacion") {
	var js_total=parseInt(document.AltaDePagos.total_registros.value);
	var js_max=parseInt(document.AltaDePagos.max_registros.value);
	if (isNaN(js_total) || isNaN(js_max))
		{
			cuadroDialogo ("No es posible realizar la operacion",3);
    		return ;
		}
	if (js_total >= js_max)
		{
		cuadroDialogo ("Solo puede dar de alta "+ document.AltaDePagos.max_registros.value +" registros",1);
		return ;
		}
	}
	var jsNumPago=parseInt(document.AltaDePagos.NumeroDocumento.value);
 //	if (document.AltaDePagos.lista_numeros_pagos.value.indexOf(","+jsNumPago+",")!=-1 && numero_documento_inicial!=jsNumPago)
//		{
//			cuadroDialogo ("El numero de documento ya existe",1)
//			return
//		}
	//document.AltaDePagos.lista_numeros_pagos.value=document.AltaDePagos.lista_numeros_pagos.value + jsNumPago +",";

	if (valida_campos()) document.AltaDePagos.submit();
}

<%
		if(	session.getAttribute("VarFechaHoy") !=null)
			out.println( session.getAttribute("VarFechaHoy"));
		else
			if (request.getAttribute("VarFechaHoy")!= null)
				out.println(request.getAttribute("VarFechaHoy"));
%>

 diasInhabiles = '<%if(	session.getAttribute("diasInhabiles") !=null)
						out.print( session.getAttribute("diasInhabiles"));
					else
						if (request.getAttribute("diasInhabiles")!= null)
							out.print(request.getAttribute("diasInhabiles"));%>';





function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/pagosCalendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();

 }



function WindowCalendar(ind){
    var m=new Date();
    n=m.getMonth();
    Indice=ind;
    msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	msg.focus();
  }

function Actualiza()
 {
   if(Indice==0)
     document.AltaDePagos.FechaVencimiento.value=Fecha[Indice];
   else
     document.AltaDePagos.FechaEmision.value=Fecha[Indice];
   /*Problemas de Comunicacion*/
 }


function enfoca(){
 if (document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==1||   document.AltaDePagos.TipoDocumento.options[document.AltaDePagos.TipoDocumento.selectedIndex].value==2)
	 {
		document.AltaDePagos.Naturaleza.disabled=true;
		document.AltaDePagos.Naturaleza.selectedIndex = -1;
		document.AltaDePagos.Naturaleza.blur();
     }
 else{

		document.AltaDePagos.Naturaleza.disabled=false;

	 }

}

function MM_preloadImages() {
    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
    var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
    if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function ValidaNumero(campo,nombreCampo,punto)
{
	tmp = campo.value;
	var numeros="0123456789"
	if (punto) numeros+="."
    for (i = 0; i<tmp.length; i++) {
		car=tmp.charAt(i)
		if (numeros.indexOf(car)==-1) {
			campo.focus();
        	cuadroDialogo("El campo: "+nombreCampo.toUpperCase()+" contiene caracteres no num&eacute;ricos",3);
			return false
									  }
	}
	return true
}

<% if ( request.getAttribute( "newMenu" ) != null ) { out.print( request.getAttribute( "newMenu" ) ); } %>

<%! String Saca(String cadena, int num) {
    int longitud, m,i, posinicial, posfinal;
	 m = i = 0;
	 posinicial = 0;
	      while(m <= num) {
         	if (cadena.charAt(i) == ';')
				m++;
			if (m == (num-1))
				posinicial = i;
			i++;
          }
		  posfinal = i;
     return cadena.substring(posinicial+2, posfinal-1);
   }
%>
<%! String ar = "archivo_actual";
    String def;  %>

	<%! public String corta(String c) {
	 String aux;
	    aux = "";
	  if (c.length()  < 40)
		  return c;
	  else
	   {
         for(int i= 0; i < 40; i++)
		   aux+= String.valueOf(c.charAt(i));
	     return aux;
	   }
    }
%>

<%
		String nombreArchivoPagos=(String)request.getSession ().getAttribute ("NombreArchivo");
		ObtieneFacturas Facturas = new ObtieneFacturas(nombreArchivoPagos);
		String Filtro = request.getParameter("Proveedor");
		String arch = arch_combos.envia_tux();
		Archivos proveedores = new Archivos();
		Archivos2 documentos = new Archivos2();
		claveTrans cvtrans = new claveTrans();
		claveDoc docs = new claveDoc();
		Vector ls_doc =  new Vector();
		Vector ls_prov = new Vector();
		Vector cv_trans = new Vector();
		Vector cv_doc  = new Vector();
		Vector Fac_Proveedor = new Vector();
		Fac_Proveedor = Facturas.Facturas(Filtro);
		ls_prov = proveedores.mLee(arch, "9");
        ls_doc = documentos.mLee(arch, "4");
		cv_trans = cvtrans.mLee(arch, "9");
		cv_doc = docs.mLee(arch,"4");


%>



</SCRIPT>


</HEAD>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="document.AltaDePagos.Naturaleza.blur();document.AltaDePagos.Naturaleza.disabled=true;MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
	<% if ( request.getAttribute( "MenuPrincipal" ) != null ) { out.print( request.getAttribute( "MenuPrincipal" ) ); } %>
     </TD>
   </TR>
</TABLE>
<% if ( request.getAttribute( "Encabezado" ) != null ) { out.print( request.getAttribute( "Encabezado" ) ); } %>

<form name = "Frmfechas">
  <%= request.getAttribute("Bitfechas") %>
</form>
<FORM NAME="AltaDePagos" METHOD="POST" ACTION="AltaDePagos">
<table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="center">
   <table width="460" border="0" cellspacing="2" cellpadding="3">
      <tr align="left" valign="middle">
      <td class="tittabdat" colspan="2"> Especifique los datos del pago </td>
      </tr>
	  <tr align="center">
      <td class="textabdatcla" valign="top" colspan="2">
      <table width="450" border="0" cellspacing="0" cellpadding="0">
	     <tr valign="top">
         <td width="270" align="left">
		 <table width="150" border="0" cellspacing="5" cellpadding="0">
		    <tr>
    	    <td class="tabmovtex" nowrap> Proveedor: </td>
		    <td class="tabmovtex" nowrap width="185">
			     <SELECT NAME="Proveedor" >

                  <% int longitud = ls_prov.size();
                                for (int i=0; i<(longitud); i++) { %>
                                <OPTION VALUE="<%=cv_trans.elementAt(i)%>"><%= corta((String)(ls_prov.elementAt(i))) %>
                                <% } %>



			     </SELECT>
			</td>
		    </tr>
         </table>
		  <table width="150" border="0" cellspacing="5" cellpadding="0">
    			   <tr valign="top">
				    <td>
					     <table width="150" border="0" cellspacing="5" cellpadding="0">
						   <tr><td class="tabmovtex" nowrap>Tipo de documento : </td></tr>
						   <tr><td class="tabmovtex" nowrap>
						   <SELECT NAME="TipoDocumento" onChange="enfoca()">
						      	<% int longitud_2 = ls_doc.size();
                   for (int i=0; i<(longitud_2); i++) { %>
                   <OPTION VALUE="<%=cv_doc.elementAt(i)%>"><%= (ls_doc.elementAt(i)) %>
                   <% } %>
						    </SELECT>

						   </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>N&uacute;mero de documento</td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <INPUT TYPE="text" SIZE="24" ALIGN="rigth" class="tabmovtex" NAME="NumeroDocumento" VALUE="<% if ( request.getAttribute( "NumeroDocumento" ) != null ) { out.print( request.getAttribute( "NumeroDocumento" ) ); } %>" >
						   <td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>Asociar a factura n&uacute;mero </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <select name="Asociado">
							    <option value="">
							 </select>
						   <td>
						   </tr>
						 </table>
						 </td>
						 <td align="top">
						  <table width="150" border="0" cellspacing="5" cellpadding="0">
                           <tr>
        		           <td class="tabmovtex" nowrap>
						     Naturaleza :
						   </td>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <SELECT NAME="Naturaleza" onFocus="enfoca()">
							    <OPTION VALUE=" ">
						        <OPTION VALUE="1">Acredora
   							    <OPTION VALUE="2">Deudora
							 </select>
						   </td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>Importe</td>
						   </tr>
						   <tr>
						   <td class="tabmovtex" nowrap>
						     <INPUT TYPE="text" SIZE="24" ALIGN="rigth" class="tabmovtex" VALUE="" NAME="ImporteDocumento">
						   </td>
						   </tr>
						   </table>
						 </td>
					   </tr>
					  </table>
					   <table width="150" border="0" cellspacing="5" cellpadding="0">
			           </table>
				    <table width="150" border="0" cellspacing="5" cellpadding="0">
    				   <tr valign="top">
					     <td>
					     <table width="150" border="0" cellspacing="5" cellpadding="0">
						   <tr><td class="tabmovtex" nowrap>Fecha de emisi&oacute;n: </td></tr>
						   <tr align="left">
                             <td class="tabmovtex" nowrap>
                               <input type=text name=FechaEmision value="<%
									if(	session.getAttribute("Fechas") !=null)
										out.println( session.getAttribute("Fechas"));
									else
										if (request.getAttribute("Fechas")!= null)
											out.println(request.getAttribute("Fechas"));
								%>" size=22 onFocus='blur();' maxlength=10>
								<a href ="javascript:WindowCalendar(1);" align="absmiddle"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                              </td>
							  </tr>
						   </table>
						 </td>
						 <td align="top">
						  <table width="150" border="0" cellspacing="5" cellpadding="0">
                           <tr>
        		           <td class="tabmovtex" nowrap>
						     Fecha de vencimiento:
						   </td>
						   <tr align="left">
                              <td class="tabmovtex" nowrap>
                               <input type=text name=FechaVencimiento value="<%
									if(	session.getAttribute("Fechas") !=null)
										out.println( session.getAttribute("Fechas"));
									else
										if (request.getAttribute("Fechas")!= null)
											out.println(request.getAttribute("Fechas"));
								%>" size=22 onFocus='blur();' maxlength=10>
								<A href="javascript:SeleccionaFecha(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
                              </td>
							  </tr>
						   </table>
						 </td>
					   </tr>
					  </table>
		 </td>
		 </tr>
	  </table>
	  </td>
	  </tr>
	</table>
    </td>
	</tr>




</table>

<br>
<table width="760" border="0" cellspacing="0" cellpadding="0">
   <tr>
   <td align="center">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
                 <tr>
                   <td align="center" valign="middle" width="66">
                     <A HREF="javascript:js_aceptar();"><% if ( request.getAttribute( "botonOperacion" ) != null ) { out.print( request.getAttribute( "botonOperacion" ) ); } %></a>
                   </td>
                   <td align="left" valign="top" width="76">
                     <A HREF="javascript:js_limpiar();"><img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"></a>
                   </td>
                   <td align="left" valign="top" width="83">
                     <A HREF="javascript:document.AltaDePagos.action='coPagos?opcion=1';document.AltaDePagos.submit();"><img border="0" name="imageField3" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
                  </td>
                </tr>
          </table>
		 </td>
		 </tr>
</table>

 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "posicionRegistro" ) != null ) { out.print( request.getAttribute( "posicionRegistro" ) ); } %>" NAME="registro">
 <INPUT TYPE="hidden" VALUE="lectura" NAME="strImporta">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "tipo_archivo" ) != null ) { out.print( request.getAttribute( "tipo_archivo" ) ); } %>" NAME="tipo_archivo">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "total_registros" ) != null ) { out.print( request.getAttribute( "total_registros" ) ); } %>" NAME="total_registros">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "max_registros" ) != null ) { out.print( request.getAttribute( "max_registros" ) ); } %>" NAME="max_registros">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "operacion" ) != null ) { out.print( request.getAttribute( "operacion" ) ); } %>" NAME="operacion">
 <INPUT TYPE="hidden" VALUE="<% if ( request.getAttribute( "lista_numeros_pagos" ) != null ) { out.print( request.getAttribute( "lista_numeros_pagos" ) ); } %>" NAME="lista_numeros_pagos">
 <INPUT TYPE="hidden" VALUE="" NAME="Actual">
</form>
<script language="JavaScript1.2">
numero_documento_inicial=document.AltaDePagos.NumeroDocumento.value;
</script>
</BODY>
</html>


