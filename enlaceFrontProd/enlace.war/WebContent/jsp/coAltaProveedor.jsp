<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.bo.ArchivoConf" %>
<%@ page import="mx.altec.enlace.utilerias.EIGlobal" %>
<%@ page import="mx.altec.enlace.servlets.EI_Bancos" %>
<%!
	   private void writeMenu(javax.servlet.jsp.JspWriter out, String[] values,
	    String[] nameValues, String selectedValue){
		  try{
		  		for(int i=0; i<values.length; i++){
		  			if(values[i].trim().equals(selectedValue)){
		  				out.println("<option selected value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}else{
		  				out.println("<option value=\""+ values[i] + "\">" + nameValues[i] + "</option>");
		  			}
		  		}
	  	  }catch(java.io.IOException e1){
	  	  	EIGlobal.mensajePorTrace("Error en pinta menu->" + e1.getMessage(),EIGlobal.NivelLog.ERROR);
	  	  }
	   }
%>

<%
	String tituloPantalla = "";
	String tituloHTML = "";
	String boton1 = "";
	String boton2 = "";
	String altBoton1 = "";
	String altBoton2 = "";
	String gbo25421 = "gbo25421.gif";
	String altGbo25421 = "";

	String parAccion = (String) request.getAttribute("accion");

	if (parAccion == null) {
		parAccion = request.getParameter("accion");
		if (parAccion == null)
			parAccion = "1";
	}

	boolean nuevo = false;

	if (Integer.parseInt(parAccion) > 2 && !parAccion.equals("11")) {
		nuevo = true;
		parAccion = "" + (Integer.parseInt(parAccion) - 2);
	}
	// jgarcia - 02/Oct/2009
	// Obtenci�n del numero de proveedores para saber si se bloquea la seccion
	// de datos generales.
	String deshabilitaRadio = " disabled='disabled' ";
	String seleccionaRadio = " checked ";
	String origenGralProveedor = "";
	HttpSession sess = request.getSession();
	String pantallaOriProveedor = (String) request.getAttribute("pantallaOriProveedor");
	ArchivoConf objArchivo = (ArchivoConf)sess.getAttribute("objetoArchivo");
	if(objArchivo != null && objArchivo.getNumProveedores() > 0){
		try{
			ProveedorConf provTmp = objArchivo.obtenProv(0);
			origenGralProveedor = provTmp.origenProveedor;
			provTmp = null;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){
			System.out.println("coAltaProveedor.jsp - Error al obtener el numero de proveedores");
		}
	}

	ProveedorConf prov = (ProveedorConf) request
			.getAttribute("Proveedor"); // <---------------------------------
	if (prov == null) {
		prov = new ProveedorConf();
		prov.tipo = (request.getParameter("tipoPersona") == null) ? prov.FISICA :
			(((String) request.getParameter("tipoPersona")).equals("F") ? prov.FISICA : prov.MORAL);
		//jgarcia - Es proveedor Nacional o Internacional
		prov.origenProveedor = pantallaOriProveedor;
		//((String)request.getParameter("origenProveedor") != null
		//	&& prov.INTERNACIONAL.equalsIgnoreCase((String)request.getParameter("origenProveedor"))?
		//		prov.INTERNACIONAL:objArchivo != null && objArchivo.getNumProveedores() > 0?((ProveedorConf)objArchivo.obtenProv(0)).origenProveedor:prov.NACIONAL);
		if (!nuevo) {
			prov.tipoSociedad = request.getParameter("vTipoSoc");

			// jgarcia 29/Sept/2009
	        // Si es proveedor Internacional el medio de informacion debe ser e-mail
	        // y para el proveedor Nacional no existe el campo vDatosAdic.
	        // En el caso del proveedor internacional en el campo de delegacion
	        // se guarda el estado.
	        if(prov.INTERNACIONAL.equalsIgnoreCase(prov.origenProveedor)){
	        	prov.medioInformacion = "1";
	        }else{
				prov.medioInformacion = request.getParameter("vMedio");
	        }
			prov.pais = request.getParameter("vPais");
			prov.formaPago = request.getParameter("vForma");
			prov.banco = request.getParameter("vBanco");
			prov.tipoAbono = request.getParameter("vTipoAbono");
			prov.cveDivisa = request.getParameter("vCveDivisa");

			prov.quitaNulos();
		}
	}

	// Alta de Proveedores en Linea
	if (parAccion.equals("1")) {
		boton1 = "gbo25480.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "alta";
		altBoton2 = "limpiar";
		tituloPantalla = "Alta";
		tituloHTML = "Alta de Proveedor";
	}
	// Modificacion de Proveedor
	else if (parAccion.equals("2")) {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25250.gif";
		altBoton1 = "modificar";
		altBoton2 = "limpiar";
		tituloPantalla = "Modificaci&oacute;n";
		tituloHTML = "Modificaci&oacute;n de Proveedor";
	}
	// Consulta y baja de proveedor
	else {
		boton1 = "gbo25510.gif";
		boton2 = "gbo25500.gif";
		altBoton1 = "modificar";
		altBoton2 = "baja";
		tituloPantalla = "Consulta";
		tituloHTML = "Consulta y modificaci&oacute;n de proveedor";
	}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><%=tituloHTML%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript"> 
	// -------------------------------------------------
	function MM_preloadImages()
	 {	//v3.0
		var d=document;
		if(d.images)
		{
			if(!d.MM_p) d.MM_p=new Array();
				var i,j=d.MM_p.length,a=MM_preloadImages.arguments;

			for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
				{
					d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];
				}
		}
		}

	function MM_swapImgRestore()
	{//v3.0
		var i,x,a=document.MM_sr;

		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++)
			x.src=x.oSrc;
	}

	function MM_findObj(n, d)
	{		//v3.0
		var p,i,x;

		if(!d) d=document;

		if((p=n.indexOf("?"))>0&&parent.frames.length)
		{
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
		}

		if(!(x=d[n])&&d.all) x=d.all[n];

		for (i=0;!x&&i<d.forms.length;i++)
			x=d.forms[i][n];

		for(i=0;!x&&d.layers&&i<d.layers.length;i++)
			x=MM_findObj(n,d.layers[i].document);

		return x;
	}

	function MM_swapImage() {		//v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array();

		for(i=0;i<(a.length-2);i+=3)
			if ((x=MM_findObj(a[i]))!=null)
			{
				document.MM_sr[j++]=x;
				if(!x.oSrc)
					x.oSrc=x.src;

				x.src=a[i+2];
			}
	}
	// -------------------------------------------------
	//ARRAY CON CLAVES CECOBAN
	var clavesBanco = new Array(<%= request.getAttribute("clavesBanco") %>);
	// FSW IDS :::JMFR::: INICIA
	//ARRAY CON CLAVES INTERME PARA OBTENER INDEX CECOBAN
	var clavesBancoInt = new Array(<%= request.getAttribute("clavesBancoInt") %>);
	// Funcion para obtener el indice del arreglo clavesBanco mediante la clave interme del option 
	function getIndexForCveBanco(){
		for(i in clavesBancoInt){
			if(clavesBancoInt[i] === document.formulario.vBanco.value){
				return i;
			}	
		}
	}
	// FSW IDS :::JMFR::: FIN

	// --- Se ejecuta en cuanto la pagina se carga, inicializa los combos y el campo moral/fisica
	function inicia(){
		document.formulario.vCodigo.disabled = true;
		document.formulario.vSecuencia.disabled = true;
		<%if(Integer.parseInt(parAccion) >= 11){%>
			document.formulario.vClave.disabled = true;
		<%}else{%>
			document.formulario.vClave.disabled = false;
		<%}%>

		<%= (prov.tipo == prov.MORAL && prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor))?
				"selecciona(document.formulario.vTipoSoc, \"" + prov.tipoSociedad + "\");":"" %>
		selecciona(document.formulario.vPais, "<%= prov.pais %>");
		selecciona(document.formulario.vTipoAbono, "<%= prov.tipoAbono.trim() %>");

		<% if(prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)){ %>
			selecciona(document.formulario.vCveDivisa, "<%= prov.cveDivisa %>");
			if (document.formulario.vCveDivisa.value == "MXN"){
				cargaListas('bancosMXM');
			}else if (document.formulario.vCveDivisa.value == "USD"){
				cargaListas('bancosUSD');
			}
			selecciona(document.formulario.vMedio, "<%= prov.medioInformacion %>");
			seleccionaFormaPago(document.formulario.vForma, "<%= prov.formaPago %>");
			selecciona(document.formulario.vBanco, "<%= prov.banco %>");
			ajustaFormaPagoModif();
		<% }%>

		habilitarCampos();
	}

	// --- Reseta los controles
	function limpia(){

		var varTipoProvee;
		var varPersJuridi;
		
		document.formulario.reset();
		document.formulario.vClave.value = "";
		document.formulario.vCodigo.value = "";
		document.formulario.vSecuencia.value = "";
		document.formulario.vNombre.value = "";


	   	document.formulario.vDescripcionBanco.value = "";
		document.formulario.vContacto.value = "";
		document.formulario.vFactoraje.value = "";
		document.formulario.vCalle.value = "";
		document.formulario.vCiudad.value = "";
		document.formulario.vCodigoPostal.value = "";
		document.formulario.vPais.selectedIndex = 0;
		document.formulario.vEmail.value = "";
		document.formulario.vLada.value = "";
		document.formulario.vTelefonico.value = "";
		document.formulario.vExTelefono.value = "";
		document.formulario.vFax.value = "";
		document.formulario.vExFax.value = "";
		document.formulario.vForma.selectedIndex = 0;
		document.formulario.vCuenta.value = "";
		document.formulario.vCveDivisa.selectedIndex = 0;
		document.formulario.vCvePais.selectedIndex = 0;
		document.formulario.vCveABA.value = "";
		document.formulario.vDescripcionCiudad.value = "";
		document.formulario.vTipoAbono.selectedIndex = 1;
		document.formulario.vDeudores.value = "";
		document.formulario.vLimite.value = "";
		document.formulario.vVolumen.value = "";
		document.formulario.vImporte.value = "";

		for (var i = 0; i< document.formulario.vTProv.length; i++){
			if(document.formulario.vTProv[i].checked)
				varTipoProvee = document.formulario.vTProv[i].value;
		}

		for (var x = 0; x< document.formulario.vPersona.length; x++){
			if(document.formulario.vPersona[x].checked)
				varPersJuridi = document.formulario.vPersona[x].value;
		}



		if(varPersJuridi == "F"){
			document.formulario.vPaterno.value = "";
			document.formulario.vMaterno.value = "";
		}

		if(varPersJuridi == "M" && varTipoProvee == "N"){
			document.formulario.vTipoSoc.selectedIndex = 0;
		}else if(varPersJuridi == "M" && varTipoProvee == "I"){
			document.formulario.vTipoSoc.value = "";
		}

		document.formulario.vRFC.value = "";
		if (varTipoProvee == "N") {
			var select = document.getElementById('vBanco');
			select.options.length = 0;
			select.value = "";
			select.options[0] = new Option ("Seleccione una Divisa.", "0");
			
			document.formulario.vHomoclave.value = "";
			document.formulario.vColonia.value = "";
			document.formulario.vEstado.selectedIndex = 0;
			document.formulario.vMedio.selectedIndex = 0;
			document.formulario.vCveDiv.value = "";
			document.formulario.vBanco.selectedIndex = 0;
			document.formulario.vCtaBancoDestino.value = "";
			document.formulario.vDelegacion.value = "";
		}
       if (varTipoProvee == "I") {
			document.formulario.vDatosAdic.value = "";
			document.formulario.vEstado.value = "";
	   		document.formulario.vDescripcionBanco.value = "Banco Extranjero";
			document.formulario.vDescripcionCiudad.value = "Ciudad";
	   }

	}

	// --- Regresa el Control al Servlet coMtoProveedor.java
	function Regresar()
		{

		document.formulario.accion.value = "0";
		document.formulario.submit();
		}

	// --- Valida los Campos, arma la Trama y la Regresa al Servlet
	// jgarcia - 24/Sept/2009
	// En caso de que una de las funciones que se mandan a ejecutar no se encuentre en
	// este archivo, se encuentra en coAltaProveedorNacional o coAltaProveedorInter. En
	// la mayoria de los casos las funcinoes se encuentran en los dos archivos.
	function Aceptar(){
		var retorno = true;
		// Datos del Proveedor
		if(parseInt(document.formulario.accion.value)<11){
		if(retorno) retorno = valida_vCveProveedor();
		}

		if(retorno) retorno = valida_vNombre();
		if(document.formulario.tipoPersona.value == "F"){
			if(retorno) retorno = valida_vPaterno();
		}else if(document.formulario.tipoPersona.value == "M"){
			if(retorno) retorno = valida_vSociedad();
		}
		if(retorno) retorno = valida_vRFC();
		if(retorno) retorno = valida_vHomoclave();

		// Domicilio
		if(retorno) retorno = valida_vCalle();
		if(retorno) retorno = valida_vColonia();
		if(retorno) retorno = valida_vDelegacion();
		if(retorno) retorno = valida_vCiudad();
		if(retorno) retorno = valida_vCodigoPostal();
		if(retorno) retorno = valida_vEstado();
		if(retorno) retorno = valida_vPais();

		// Medio de Confirmacion
		if(retorno) retorno = valida_vMedio();
		if(retorno) retorno = valida_vTelefono();

		//Forma de pago
		if(retorno) retorno = valida_vForma();

		//Tipo de abono, L&iacute;mite de financiamiento, vomumen a. de ventas e importe fin.
		if(retorno) retorno = valida_vTipoAbono();
		if(retorno) retorno = valida_vLimite();
		if(retorno) retorno = valida_vVolumen();
		if(retorno) retorno = valida_vImporte();
		if(retorno) retorno = valida_vCveDivisa();

		if(retorno){
//			document.formulario.vLimite.value = formateaSinDecimal(document.formulario.vLimite.value);
//			document.formulario.vVolumen.value = formateaSinDecimal(document.formulario.vVolumen.value);
//			document.formulario.vImporte.value = formateaSinDecimal(document.formulario.vImporte.value);

			document.formulario.vCodigo.disabled = false;
			document.formulario.vSecuencia.disabled = false;
			document.formulario.vClave.disabled = false;
			if(parseInt(document.formulario.accion.value)<11)
				document.formulario.accion.value = "" + (parseInt(document.formulario.accion.value) + 2);
			else
				document.formulario.accion.value = "12";
			document.formulario.vCveDivisa.disabled = false;
			document.formulario.submit();
		}
	}

	// borra el proveedor
	function borra()
		{cuadroDialogo("El proveedor va a ser borrado,<br>&iquest;es correcto?",2);}
	var respuesta = 0;
	function continua()
		{
		if(respuesta == 1)
			{
			document.formulario.vCodigo.disabled = false;
			document.formulario.accion.value = "13";
			document.formulario.submit();
			}
		}

	//
	function formateaSinDecimal(valor)
		{
		var pos = valor.indexOf(".");
		if(pos == -1) return valor + "00";
		while(pos+3>valor.length) valor += "0";
		while(pos+3<valor.length) valor = valor.substring(0,valor.length-1);
		return valor;
		}


// -- Elimina los espacio existentes de una cadena, por el lado derecho e izquierdo y entre la cadena
function trim(inputString)
{
	if (typeof inputString != "string") { return inputString; }

	var retValue = inputString;
	var ch = retValue.substring(0, 1);

	while (ch == " ") {
		retValue = retValue.substring(1, retValue.length);
		ch = retValue.substring(0, 1);
	}
	ch = retValue.substring(retValue.length-1, retValue.length);
	while (ch == " ") {
		retValue = retValue.substring(0, retValue.length-1);
		ch = retValue.substring(retValue.length-1, retValue.length);
	}
	while (retValue.indexOf("  ") != -1) {
		retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length);
	}
	return retValue;
}

	// --- selecciona el valor 'valor' de la lista 'lista'
	function selecciona(lista,valor)
	{
		for(a=0;a<lista.length;a++){
			if(lista.options[a].value == valor){
				lista.selectedIndex = a;
			}
		}
	}
	
	// --- Selecciona la forma de pago correcta al consultar al proveedor.
	function seleccionaFormaPago(lista,valor)
	{
		var cuenta = document.formulario.vCuenta.value;
		if (valor == 1 && cuenta.length != 10) {
			lista.selectedIndex = 1;
		} else if (valor == 2 && cuenta.length != 10){
			lista.selectedIndex = 2;
		} else if (valor == 1 && cuenta.length == 10) {
			lista.selectedIndex = 3;
		} else if (valor == 2 && cuenta.length == 10) {
			lista.selectedIndex = 4;
		}
	}

// ------------------------------ FUNCIONES DE LAS VALIDACIONES -------------------------------------

// Valida si el caracter recibido corresponde a un caracter alfabetico
function Caracter_EsAlfa(caracter)
{
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
		if( !(caracter >= "&Aacute;" && caracter <= "&Uacute;") )
			if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
				if( !(caracter >= "&aacute;" && caracter <= "&uacute;") )
					if ( caracter != "�" && caracter != "�")
						ValorRetorno = false;
	return ValorRetorno;
}

// Valida si el caracter recibido corresponde a un caracter numerico
function Caracter_EsNumerico(caracter)
	{
	var ValorRetorno = true;
	if( !(caracter >= "0" && caracter <= "9") ) ValorRetorno = false;
	return ValorRetorno;
	}

// Valida si el caracter recibido corresponde a un caracter especial (en este caso "&" Ampersand)
function Caracter_EsEsp(caracter)
	{
	var ValorRetorno = true;
	var expr = /^[\&]?$/i;
	if( !expr.test(caracter) ) ValorRetorno = false;
	return ValorRetorno;
	}

// Valida que la cadena sea Alfanumerica ------------------------------------------------------------
function EsAlfaNumerica(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				retorno = false;
	}
	return retorno;
}

// Valida que la cadena sea Alfanumerica y permite el caracter especial "&"--------------------------
function EsAlfaNumericaEsp(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				if (!Caracter_EsEsp(caracter))
					retorno = false;
	}
	return retorno;
}

// Valida que la cadena sea Alfabetica --------------------------------------------------------------
function EsAlfa(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfa(caracter))
			if( caracter != " ")
				retorno = false;
	}
	return retorno;
}

	// Valida que la cadena sea Numerica ------------------------------------------------------------
	function EsNumerica(cadena)
		{
		var retorno = true;

		for(a=0;a<cadena.length && retorno;a++)
			{
			caracter = cadena.substring(a,a+1);
			if (!(caracter >= "0" && caracter <= "9")) retorno = false;
			}

		return retorno;
		}

// Valida si la longitud de la cadena es Correcta ---------------------------------------------------
function EsValidaLongitud(LongitudCadena,Patron)
{
	var VariableRetorno;
	VariableRetorno = (LongitudCadena > 0 && LongitudCadena <= Patron)?true:false;

	return(VariableRetorno);
}

// Valida si la cadena contiene caracteres Especiales -----------------------------------------------
function SonValidosCaracteresEspeciales(cadena){
	var ValorRetorno = true;
	var caracter;

	for(i=0; i < cadena.length && ValorRetorno; i++){
		caracter = cadena.substring(i,i+1);
		if (!Caracter_EsAlfa(caracter))
			if (!Caracter_EsNumerico(caracter))
				if( caracter != " " && caracter != "." && caracter != "#" && caracter != "&" && caracter != null ) //caracteres Especiales
					ValorRetorno = false;
	}
	return (ValorRetorno);
}

function MensajeErrorLongitudNula(NombreCampo)
{
	cuadroDialogo("Favor de teclear " + NombreCampo,3)
}
function MensajeErrorTipoDeDato(NombreCampo,TipoDato)
{
	cuadroDialogo("EL campo " + NombreCampo + " debe ser " + TipoDato,3)
}
function MensajeErrorCaracteresInvalidos(NombreCampo)
{
	cuadroDialogo("EL campo " + NombreCampo + " contiene car&aacute;cteres no v&aacute;lidos",3)
}
// --------------------------- VALIDACIONES DE CAMPOS ALPHANUMERICOS ------------------------------

// Se agrega funci�n para la validaci�n de la clave proveedor
function EsAlfaNumericaCveProv(cadena)
{
	var retorno = true;

	for(a=0;a<cadena.length && retorno;a++) {
		caracter = cadena.substring(a,a+1);
		if (!Caracter_EsAlfaCveProv(caracter))
			if (!Caracter_EsNumerico(caracter))
				if (!Caracter_EsEsp(caracter))
					retorno = false;
	}
	return retorno;
}

// Se agrega funci�n para la validaci�n de la clave proveedor quitando la �
function Caracter_EsAlfaCveProv(caracter)
{
	var ValorRetorno = true;

	if ( !(caracter >= "A" && caracter <= "Z") ) //Si no es Es Alpha Mayusculas
		if( !(caracter >= "&Aacute;" && caracter <= "&Uacute;") )
			if ( !(caracter >= "a" && caracter <= "z") ) // Si no es Alpha Minusculas
				if( !(caracter >= "&aacute;" && caracter <= "&uacute;") )
						ValorRetorno = false;
	return ValorRetorno;
}

// Valida Clave de Proveedor -----------------------------------------------------------------------
function valida_vCveProveedor(){
	var valorCampo = document.formulario.vClave.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vClave.select();
		document.formulario.vClave.focus();
		MensajeErrorLongitudNula("la Clave del Proveedor");
	}else if (!EsAlfaNumericaCveProv(valorCampo)) {
		document.formulario.vClave.select();
		document.formulario.vClave.focus();
		MensajeErrorTipoDeDato("Clave de Proveedor","Alfanum&eacute;rico");
	}else { ValorRegreso = true;}

	return ValorRegreso;
}

// Valida Apellido Paterno ------------------------------------------------------------------------
function valida_vPaterno(){
	var valorCampo = document.formulario.vPaterno.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vPaterno.select();
		document.formulario.vPaterno.focus();
		MensajeErrorLongitudNula("el Apellido Paterno");
	}else if (!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vPaterno.select();
		document.formulario.vPaterno.focus();
		MensajeErrorCaracteresInvalidos("Apellido Paterno");
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Nombre ---------------------------------------------------------------------------------
function valida_vNombre()
{
	var valorCampo = document.formulario.vNombre.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vNombre.select();
		document.formulario.vNombre.focus();
		MensajeErrorLongitudNula("el Nombre");
	}else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vNombre.select();
		document.formulario.vNombre.focus();
		MensajeErrorCaracteresInvalidos("Nombre");
	}else {ValorRegreso = true;}
	return ValorRegreso;
}

// Valida el Nombre del R.F.C. -------------------------------------------------------------------
function valida_NombreRFC(valorCampo){
	if(document.formulario.tipoPersona.value == "F"){
		// Persona F&iacute;sica
		return validaLetrasRFC(valorCampo);
	}else if(document.formulario.tipoPersona.value == "M"){
		// Persona Moral
		return true;
	}
	return false;
	}


	//
	function validaLetrasRFC(rfc)
		{
		var resultado="";
		nombre=document.formulario.vNombre.value.toUpperCase();
		pat=document.formulario.vPaterno.value.toUpperCase();
		mat=document.formulario.vMaterno.value.toUpperCase();
		rfc1=rfc.toUpperCase().substring(0,4);

		if(mat == "") mat = rfc.substring(2,3);


		nombre=quitaPrep(nombre)
		pat=quitaPrep(pat)
		mat=quitaPrep(mat)

		if (pat.length==0 || nombre.length==0 || mat.length==0 || rfc1.length!=4) return false;
		resultado=pat.charAt(0);
		var vocal = buscaVocal(pat.substring(1));
		if (vocal=="")
			{if (pat.length>1) resultado+=pat.charAt(1); else resultado+=rfc1.charAt(1);}
		else
			resultado+=vocal;

		resultado+=mat.charAt(0);
		resultado+=letraNombre(nombre);
		resultado=frases(resultado);
		if (resultado==rfc1) return true; else return false;
		}

	//
	function quitaPrep(cadena)
		{
		var temp = "", elem;
		var elem2 = new Array(),j=0;
		elem = cadena.split(" ");

		for (var i=0;i<elem.length;i++)
			if (elem[i]!="" && elem[i]!="DEL" && elem[i]!="DE" && elem[i]!="LA" && elem[i]!="LOS" && elem[i]!="LAS" &&
				elem[i]!="Y" && elem[i]!="MC" && elem[i]!="MAC" && elem[i]!="VON" && elem[i]!="VAN")
				{
				elem2[j]=elem[i];
				j++;
				}
		if (elem2.length==1) return elem2[0];
		for (var i=0;i<elem2.length;i++) temp+=" "+elem2[i];
		temp=temp.substring(1);
		return temp;
		}

	//
	function buscaVocal(cadena)
		{
		for (var i=0;i<cadena.length;i++)
			if (cadena.charAt(i)=="A" || cadena.charAt(i)=="E" || cadena.charAt(i)=="I" ||
				cadena.charAt(i)=="O" || cadena.charAt(i)=="U" || cadena.charAt(i)=="&")
				return cadena.charAt(i);
		return "";
		}

	//
	function letraNombre(cadena)
		{
		var resultado="",temp="",elementos;
		elementos =cadena.split(" ");

		if (elementos.length==1) return cadena.charAt(0);

		temp=elementos[0];

		if (!(temp=="MARIA" || temp=="MA" || temp=="MA." || temp=="JOSE")) return "" +temp.charAt(0);

		temp=elementos[1];
		return "" +temp.charAt(0);
		}

	//
	function frases(f)
		{
		if (f=="BUEI" || f=="BUEY" || f=="CACA" || f=="CACO" || f=="CAGA" || f=="CAGO" || f=="CAKA" || f=="CAKO" || f=="COGE" ||
			f=="COJA" || f=="COJE" || f=="COJI" || f=="COJO" || f=="CULO" || f=="FETO" || f=="GUEY" || f=="JOTO" || f=="KACA" ||
			f=="KACO" || f=="KAGA" || f=="KAGO" || f=="KOGE" || f=="KOJO" || f=="KAKA" || f=="KULO" || f=="LOCA" || f=="LOCO" ||
			f=="LOKA" || f=="LOKO" || f=="MAME" || f=="MAMO" || f=="MEAR" || f=="MEAS" || f=="MEON" || f=="MION" || f=="MOCO" ||
			f=="MULA" || f=="PEDA" || f=="PEDO" || f=="PENE" || f=="PUTA" || f=="PUTO" || f=="QULO" || f=="RATA" || f=="RUIN")
			return f.substring(0,3)+ "X"
		else
			return f;
		}



// Valida la Fecha del R.F.C. -------------------------------------------------------------------
function valida_FechaRFC(valorCampo,Indice)
{
	var ValorRegreso = true;
	var mes = valorCampo.substring(Indice, Indice + 2);
	var dia = valorCampo.substring(Indice + 2, Indice + 4);

	if((dia > 0 && dia <= 31) && (mes > 0 && mes <= 12)) {
		if(mes == 2 && dia > 29)	// Para el Mes de Febrero  MARL formato de A�o bisiesto
			{ValorRegreso = false;}
	}
	else {ValorRegreso = false;}

	return ValorRegreso;
}

// Valida Persona Contacto en la empresa -------------------------------------------------------
function valida_vContacto()
{
	var valorCampo = document.formulario.vContacto.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vContacto.select();
		document.formulario.vContacto.focus();
		MensajeErrorLongitudNula("la Persona de Contacto en la Empresa");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vContacto.select();
		document.formulario.vContacto.focus();
		MensajeErrorCaracteresInvalidos("Persona de Contacto en la Empresa");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida Numero de cliente para Factoraje ----------------------------------------------------
function valida_vFactoraje()
{
	var valorCampo = document.formulario.vFactoraje.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vFactoraje.select();
		document.formulario.vFactoraje.focus();
		MensajeErrorLongitudNula("el N&uacute;mero de Cliente para Factoraje");
	}
	else if(!EsAlfaNumerica(valorCampo)) {
		document.formulario.vFactoraje.select();
		document.formulario.vFactoraje.focus();
		MensajeErrorTipoDeDato("N&uacute;mero de Cliente para Factoraje","Alfanum&eacute;rico");
	}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

// Valida la Calle y numero ------------------------------------------------------------------
function valida_vCalle()
{
	var valorCampo = document.formulario.vCalle.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vCalle.select();
		document.formulario.vCalle.focus();
		MensajeErrorLongitudNula("la Calle y N&uacute;mero");
	}
	else if(!SonValidosCaracteresEspeciales(valorCampo)) {
		document.formulario.vCalle.select();
		document.formulario.vCalle.focus();
		MensajeErrorCaracteresInvalidos("Calle y N&uacute;mero");
	}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

// Valida Ciudad o Poblacion ---------------------------------------------------------------
function valida_vCiudad()
{
	var valorCampo = document.formulario.vCiudad.value;
	var tipoProveedor = document.formulario.vForma.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vCiudad.select();
		document.formulario.vCiudad.focus();
		MensajeErrorLongitudNula("la Ciudad o Poblaci&oacute;n");
	}
	else if(tipoProveedor == 1 || tipoProveedor == 2){
		var retorno = true;
		for(a=0;a<valorCampo.length && retorno;a++) {
			caracter = valorCampo.substring(a,a+1);
			if (!Caracter_EsAlfa(caracter))
				if (!Caracter_EsNumerico(caracter))
						if(caracter!=" ")
							retorno = false;
		}
		if(retorno==false){
			document.formulario.vCiudad.select();
			document.formulario.vCiudad.focus();
			MensajeErrorCaracteresInvalidos("Ciudad o Poblaci&oacute;n");
		}else {
			ValorRegreso = true;
		}
	}else { ValorRegreso = true; }
	return ValorRegreso;
}

// --------------------------- VALIDACIONES DE CAMPOS NUMERICOS ---------------------------------
// Valida el Fax --------------------------------------------------------------------------
function valida_vFax(opcion)
{
	var valorCampo = document.formulario.vFax.value;
	var valorCampo2 = document.formulario.vLada.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		MensajeErrorLongitudNula("el Fax");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		MensajeErrorTipoDeDato("Fax","Num&eacute;rico");
	}
	/*Q05-29284*/
	else if((valorCampo.length + valorCampo2.length) != 10) {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		cuadroDialogo("La clave lada y el fax deben sumar 10 posiciones.",3)
	}
	else if(valorCampo.charAt(0) == '0') {
		document.formulario.vFax.select();
		document.formulario.vFax.focus();
		cuadroDialogo("El n&uacute;mero de FAX no debe empezar con 0 ni incluir claves de marcaci&oacute;n (01) ni clave de larga distancia",3)
	}
	else { ValorRegreso = true; }

	return ValorRegreso;
}

// Valida la Lada -------------------------------------------------------------------------
function valida_vLada()
{
	var valorCampo = document.formulario.vLada.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vLada.select();
		document.formulario.vLada.focus();
		MensajeErrorLongitudNula("la Lada");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vLada.select();
		document.formulario.vLada.focus();
		MensajeErrorTipoDeDato("Lada","Num&eacute;rico");
	}
	else if(valorCampo.charAt(0) == '0') {
		document.formulario.vLada.select();
		document.formulario.vLada.focus();
		cuadroDialogo("La clave de larga distancia no debe empezar con 0 ni incluir claves de marcaci&oacute;n (01)",3)
	}
	else{ ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la Extension de Telefono --------------------------------------------------------
function valida_vExTelefono()
{
	var valorCampo = document.formulario.vExTelefono.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vExTelefono.select();
		document.formulario.vExTelefono.focus();
		MensajeErrorLongitudNula("la Extensi&oacute;n de Tel&eacute;fono");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vExTelefono.select();
		document.formulario.vExTelefono.focus();
		MensajeErrorTipoDeDato("Extensi&oacute;n de Tel&eacute;fono","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

// Valida la Extension del Fax ---------------------------------------------------------
function valida_vExFax()
{
	var valorCampo = document.formulario.vExFax.value;
	var ValorRegreso = false;

	if(valorCampo.length == 0) {
		document.formulario.vExFax.select();
		document.formulario.vExFax.focus();
		MensajeErrorLongitudNula("la Extensi&oacute;n de Fax");
	}
	else if(!EsNumerica(valorCampo)) {
		document.formulario.vExFax.select();
		document.formulario.vExFax.focus();
		MensajeErrorTipoDeDato("Extensi&oacute;n de Fax","Num&eacute;rico");
	}
	else { ValorRegreso = true; }
	return ValorRegreso;
}

	// Valida la Sucursal ------------------------------------------------------------------
	function valida_vSucursal()
		{
		var valorCampo = document.formulario.vSucursal.value;
		var ValorRegreso = false;

		if(valorCampo.length == 0)
			{
			document.formulario.vSucursal.select();
			document.formulario.vSucursal.focus();
			cuadroDialogo("Debe especificarse una sucursal cuando el tipo de pago es interbancario",3);
			}
		else if(!EsNumerica(valorCampo))
			{
			document.formulario.vSucursal.select();
			document.formulario.vSucursal.focus();
			cuadroDialogo("La sucursal debe ser num&eacute;rica",3);
			}
		else
			ValorRegreso = true;
		return ValorRegreso;
		}

		// Valida selecion del pais  --------------------------------------------------------------
function valida_vCvePais()
{
	var index_Pais =  document.formulario.vCvePais.selectedIndex;
	var ValorRegreso = true;

	if ( index_Pais == 0) {
		document.formulario.vCvePais.focus();
	    cuadroDialogo("Favor de Seleccionar un Pa&iacute;s",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}




// Valida el Limite de financiamiento --------------------------------------------------
function valida_vLimite()
	{
	var valor = document.formulario.vLimite.value;
	var retorno = true;

	var patron = "0123456789.";
	var posPunto = valor.indexOf(".");

	if(valor.length == 0)
		{retorno = true;}
	else if(posPunto != valor.lastIndexOf("."))
		{retorno = false;}
	else if(posPunto != -1 && posPunto + 3 < valor.length)
		{retorno = false;}
	else
		{
		for(a=0;a<valor.length;a++)
			if(patron.indexOf(valor.substring(a,a+1)) == -1) retorno = false;
		}

	if(!retorno) cuadroDialogo("el L&iacute;mite de financiamiento no es v&aacute;lido",3);

	return retorno;
	}

// Valida el Volumen anual de ventas --------------------------------------------------
function valida_vVolumen()
	{
	var valor = document.formulario.vVolumen.value;
	var retorno = true;

	var patron = "0123456789.";
	var posPunto = valor.indexOf(".");

	if(valor.length == 0)
		{retorno = true;}
	else if(posPunto != valor.lastIndexOf("."))
		{retorno = false;}
	else if(posPunto != -1 && posPunto + 3 < valor.length)
		{retorno = false;}
	else
		{
		for(a=0;a<valor.length;a++)
			if(patron.indexOf(valor.substring(a,a+1)) == -1) retorno = false;
		}

	if(!retorno) cuadroDialogo("El Volumen anual de ventas no es v&aacute;lido",3);

	return retorno;
	}

// Valida el Importe de financiamiento ------------------------------------------------
function valida_vImporte()
	{
	var valor = document.formulario.vImporte.value;
	var retorno = true;

	var patron = "0123456789.";
	var posPunto = valor.indexOf(".");

	if(valor.length == 0)
		{retorno = true;}
	else if(posPunto != valor.lastIndexOf("."))
		{retorno = false;}
	else if(posPunto != -1 && posPunto + 3 < valor.length)
		{retorno = false;}
	else
		{
		for(a=0;a<valor.length;a++)
			if(patron.indexOf(valor.substring(a,a+1)) == -1) retorno = false;
		}

	if(!retorno) cuadroDialogo("el Importe de financiamiento no es v&aacute;lido",3);

	return retorno;
	}

	// valida el d&iacute;gito verificador de la cuenta/CLABE
	function digitoVerificador(cuenta)
		{
		var pesos = "371371371371371371371371371";
		var DC = 0;
		var LlaveCuenta = cuenta.substring(0,cuenta.length-1);

		for(i=0;i<LlaveCuenta.length;i++)
			DC += (parseInt(LlaveCuenta.charAt(i)) * parseInt(pesos.charAt(i))) % 10;
		DC = 10 - (DC % 10);
		DC = (DC >= 10) ? 0 : DC;
		LlaveCuenta += "" + DC;
		if((LlaveCuenta) != cuenta) return false;

		return true;
		}


 function cuentaChequesDigVer(cuenta)
	{                //215701336592    8
	var ponderacion = "234567234567";
	var MODULO = 11;
	var ponderado =0;
	var Cta_compuesta = "21"+cuenta;
	var Cierto = true;

	for (i=0;i<Cta_compuesta.length-1;i++)
		ponderado += parseInt(Cta_compuesta.charAt(i))*parseInt(ponderacion.charAt(i));

	switch (ponderado % MODULO )
		{
		case 0:
			Cierto = (parseInt(Cta_compuesta.substring(12)) == 0)
			break;
		case 1:
			Cierto = false;
			break;
		default:
			var ponde = (ponderado % MODULO)- MODULO;
			Cierto = ((ponde == parseInt(Cta_compuesta.substring(12)))||(ponde == parseInt(Cta_compuesta.substring(12))*(-1)))
			break;
		}
	return Cierto;
	}

	// Valida Cuenta/CLABE ----------------------------------------------------------------
	function valida_vCuenta()
		{
		var retorno = true;
		var banco ="";
		var cvebanco ="";
		var prefijo="";
		var cuenta = document.formulario.vCuenta.value;
		var formaPago = document.formulario.vForma.options[document.formulario.vForma.selectedIndex].value;
		var formaPagoIndex = document.formulario.vForma.selectedIndex;;
		var claveDivisa = document.formulario.vCveDivisa.options[document.formulario.vCveDivisa.selectedIndex].value;

		// cuenta no debe ser nula
		if(cuenta.length == 0)
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino/M&oacute;vil es obligatoria",3);
			retorno = false;
			}

		// cuenta debe ser num&eacute;rica
		if(retorno && !EsNumerica(cuenta))
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino/M&oacute;vil debe ser num&eacute;rica",3);
			retorno = false;
			}

		// longitud de la cuenta
		if(retorno && formaPago == "2" && claveDivisa == "USD" && cuenta.length!=18 && !(cuenta.length >= 1 && cuenta.length<=11))
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La longitud de la Cuenta Destino debe estar entre 1 a 11 &oacute; 18 d&iacute;gitos",3);
			retorno = false;
			}
		if(retorno && formaPago == "1" && formaPagoIndex == "1" && cuenta.length != 11 && cuenta.length != 18)
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La longitud de la Cuenta Destino/M&oacute;vil debe ser de 11 &oacute; 18 d&iacute;gitos",3);
			retorno = false;
			}
		if(retorno && formaPago == "2" && formaPagoIndex == "2" && claveDivisa == "MXN" && cuenta.length != 18)
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La longitud de la Cuenta Destino/M&oacute;vil debe ser de 18 d&iacute;gitos",3);
			retorno = false;
			}
		if(retorno && (formaPagoIndex == "3" || formaPagoIndex == "4") && claveDivisa == "MXN" && cuenta.length != 10)
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La longitud de la Cuenta Destino/M&oacute;vil debe ser de 10 d&iacute;gitos",3);
			retorno = false;
			}


		if(cuenta.length==11)
 		   prefijo=cuenta.substring(0,2);
		else
		  if(cuenta.length==18)
			prefijo=cuenta.substring(6,8);

		/*
		alert("verificando ");
		alert("longitud "+ cuenta.length);
		alert("forma pago "+formaPago);
		alert("prefijo "+ prefijo);
		*/

		if(retorno && formaPago == 1
		           && (cuenta.length==11 || cuenta.length==18)
		           && claveDivisa == "MXN"
		           && (prefijo=="09" || prefijo=="42" || prefijo=="49" || prefijo=="82" ||
		               prefijo=="83" || prefijo=="97" || prefijo=="98"))
			{
			  document.formulario.vCuenta.select();
			  document.formulario.vCuenta.focus();
			  cuadroDialogo("La divisa no corresponde a la cuenta.",3);
			  retorno = false;
			}

		if(retorno && formaPago == 1
		           && (cuenta.length==11 || cuenta.length==18)
		           && claveDivisa == "USD"
		           && (prefijo=="08" || prefijo=="10" || prefijo=="11" || prefijo=="13" ||
					   prefijo=="15" || prefijo=="16" || prefijo=="17" || prefijo=="18" ||
					   prefijo=="19" || prefijo=="20" || prefijo=="21" || prefijo=="22" ||
  					   prefijo=="23" || prefijo=="24" || prefijo=="25" || prefijo=="26" ||
					   prefijo=="27" || prefijo=="28" || prefijo=="40" || prefijo=="43" ||
        			   prefijo=="44" || prefijo=="45" || prefijo=="46" || prefijo=="47" ||
					   prefijo=="48" || prefijo=="50" || prefijo=="51" || prefijo=="52" ||
					   prefijo=="53" || prefijo=="54" || prefijo=="55" || prefijo=="56" ||
        			   prefijo=="57" || prefijo=="58" || prefijo=="59" || prefijo=="60" ||
                       prefijo=="61" || prefijo=="62" || prefijo=="63" || prefijo=="64" ||
                       prefijo=="65" || prefijo=="66" || prefijo=="67" || prefijo=="68" ||
                       prefijo=="69" || prefijo=="70" || prefijo=="71" || prefijo=="72" ||
                       prefijo=="73" || prefijo=="74" || prefijo=="75" || prefijo=="76" ||
                       prefijo=="77" || prefijo=="78" || prefijo=="79" || prefijo=="80" ||
                       prefijo=="81" || prefijo=="85" || prefijo=="86" || prefijo=="87" ||
                       prefijo=="88" || prefijo=="89" || prefijo=="90" || prefijo=="91" ||
                       prefijo=="92" || prefijo=="93" || prefijo=="94" || prefijo=="95" ||
                       prefijo=="96" || prefijo=="97" || prefijo=="99"))
			{
			  document.formulario.vCuenta.select();
			  document.formulario.vCuenta.focus();
			  cuadroDialogo("La divisa no corresponde a la cuenta.",3);
			  retorno = false;
			}

		// validaci&oacute;n con el banco
		banco = cuenta.substring(0,3);
		if(retorno && cuenta.length == 18 && formaPago == "1" && banco != "014" && banco != "003")
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			//NVS - Q06-0018286
			cuadroDialogo("La Cuenta Destino interna no es de Santander",3);
			retorno = false;
			}

		while(banco.substring(0,1) == '0') banco = banco.substring(1)
		if(retorno && cuenta.length == 18 && formaPago == "2"
			       && parseInt(banco) != clavesBanco[getIndexForCveBanco()])
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino no coincide con el banco seleccionado",3);
			retorno = false;
			}

		// validaci&oacute;n del d&iacute;gito verificador cuenta CLAVE
		if(retorno && formaPago == "1" && cuenta.length == 11 && !cuentaChequesDigVer(cuenta))
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido",3);
			retorno = false;
			}

					// validaci&oacute;n del d&iacute;gito verificador cuenta CLAVE
		if(retorno && formaPago == "1" && cuenta.length == 18 && !digitoVerificador(cuenta))
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido",3);
			retorno = false;
			}

			// validaci&oacute;n del d&iacute;gito verificador cuenta CLABE para forma de pago 2 y divisa UDS
		if(retorno && formaPago == "2" && claveDivisa == "MXN" && cuenta.length == 18 && !digitoVerificador(cuenta))
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido",3);
			retorno = false;
			}

		// validaci&oacute;n del d&iacute;gito verificador cuenta de cheques
		if(retorno && formaPago == "2" && claveDivisa == "USD" && cuenta.length == 18 && !digitoVerificador(cuenta) )
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("El d&iacute;gito verificador de la Cuenta Destino no es v&aacute;lido",3);
			retorno = false;
			}

		// validaci&oacute;n con el banco
		cuenta = cuenta.substring(0,3); while(cuenta.substring(0,1) == '0') cuenta = cuenta.substring(1);
		if(retorno && formaPago == 2  && formaPagoIndex == "2" && claveDivisa == "MXN" &&
			parseInt(cuenta) != clavesBanco[getIndexForCveBanco()])
			{
			document.formulario.vCuenta.select();
			document.formulario.vCuenta.focus();
			cuadroDialogo("La Cuenta Destino no coincide con el banco seleccionado",3);
			retorno = false;
			}

		return retorno;
		}




// ----------------------------- VALIDACIONES DE COMBOS --------------------------------------


// Valida selecion del pais  --------------------------------------------------------------
function valida_vPais(){
	var index_Pais =  document.formulario.vPais.selectedIndex;
	var ValorRegreso = true;

	if ( index_Pais == 0) {
		document.formulario.vPais.focus();
	    cuadroDialogo("Favor de Seleccionar un Pa&iacute;s",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}

	// Valida selecion del Banco  --------------------------------------------------------------
	function valida_vPlaza()
		{
		var index_Plaza = document.formulario.vPlaza.selectedIndex;
		var ValorRegreso = true;

		if (index_Plaza < 1)
			{
			document.formulario.vPlaza.focus();
			cuadroDialogo("Favor de Seleccionar una Plaza",3);
			ValorRegreso = false;
			}
		return ValorRegreso;
		}

// Valida que el e-mail sea valido -------------------------------------------------------------
function valida_vEmail()
{
	var valorCampo = document.formulario.vEmail.value;
	var ValorRegreso = false;
	var pos = 0;
	var complemento;
	var commx;
	var mx;

	//marcos@santander.		Tipo = com.mx
	if(valorCampo.length != 0) {
//		alert("La cadena no es nula")
		if((pos = valorCampo.indexOf('@',0)) != -1) {
//			alert("La cadena tiene un Arroba (@) "+ pos)
			if((valorCampo.substring(0,pos)).length != 0) {
//				alert("obtenemos la cadena de la izquierda del Arroba (@)")
				if((valorCampo.substring(pos)).length > 1 ) {
//				   alert("Se verifica la parte derecha del Arroba si es mayor q uno")
//					complemento = valorCampo.substring(pos+1);
//					alert("obtenemos la parte de la derecha del Arroba (@) "+ complemento) //mex.boehringer-ingelheim.com
//					if((pos = complemento.indexOf('.',0)) != -1) {
//						if((complemento.substring(0,pos)).length != 0) {
//							if((complemento.substring(pos)).length > 1 ) {
//								commx = complemento.substring(pos+1);
//								if((pos = commx.indexOf('.',0)) != -1) {
//									if((commx.substring(0,pos)).length != 0) {
//										if((commx.substring(pos)).length > 1 ) {
//											mx = commx.substring(pos+1);
//											if(mx.indexOf('.',0) == -1 && mx.length >= 2) {
//												ValorRegreso = true;
//											}
//										}
//									}
//								}
//								else if(commx.length == 3) {ValorRegreso = true;}
//							}
//						}
//					}
					ValorRegreso = true;
				}
			}
		}
	}
	if(ValorRegreso == false) {
		document.formulario.vEmail.select();
		document.formulario.vEmail.focus();
		cuadroDialogo("Favor de teclear correctamente su e-mail",3);
	}

	return ValorRegreso;
}

// Habilita los campos correspondientes a una persona Fisca --------------------------------------
function HabilitaCamposValidos_Fisica()
	{
		//jgarcia
		var radioProv = document.formulario.vTProv[0];
		if(radioProv != null && radioProv.checked){
			document.formulario.origenProveedor.value = "N";
		}else{
			document.formulario.origenProveedor.value = "I";
		}
		document.formulario.tipoPersona.value = "F";
		document.formulario.submit();
	}

// Habilita los campos correspondientes a una persona Moral --------------------------------------
function HabilitaCamposValidos_Moral()
	{
		//jgarcia
		var radioProv = document.formulario.vTProv[0];
		if(radioProv != null && radioProv.checked){
			document.formulario.origenProveedor.value = "N";
		}else{
			document.formulario.origenProveedor.value = "I";
		}
		document.formulario.tipoPersona.value = "M";
		document.formulario.submit();
	}

// Stefanini ***** 24/03/2009 ****** validar que la cve_aba sea valida

function valida_vCveABA()
{
		var cveValida = false;
		var cveaba = document.formulario.vCveABA.value;
		if(!EsNumerica(cveaba)){
			cuadroDialogo("La Clave ABA debe ser num&eacute;rica",3);
		}else if(cveaba.length != 9){
			cuadroDialogo("La Clave ABA es incorrecta",3);
		}else{
			ventanaInfo2=window.open('EI_Bancos?BancoTxt=ABA&Clave='+trimString(cveaba),'Claves','width=300,height=180,toolbar=no,scrollbars=no');
		    ventanaInfo2.focus();
	    }

	return cveValida;
}

function Selecciona(S){
	if(S==2){
		var indice = ventanaInfo2.document.Forma.SelBanco.selectedIndex;
		var descBancoSel = ventanaInfo2.document.Forma.SelBanco.options[indice].text;
		document.formulario.vDescripcionBanco.value = descBancoSel;
    }else if(S==5){
	  var v1 = "";
	  var v2 = "";
	  var v3 = "";
      v1=ventanaInfo2.document.Forma.CiudadABA.value;
	  v2=ventanaInfo2.document.Forma.BancoABA.value;
	  v3=ventanaInfo2.document.Forma.EstadoABA.value;

	  if(v1 && v2 && v3){
	  	document.formulario.vDescripcionCiudad.value=v1+", "+v3;
	  	document.formulario.vDescripcionBanco.value=v2;
	  }else{
			document.formulario.vDescripcionCiudad.value="";
			document.formulario.vDescripcionBanco.value="";
	  }
    }
}

function valida_vTipoAbono(){
	var index_Pais =  document.formulario.vTipoAbono.selectedIndex;
	var ValorRegreso = true;

	if ( index_Pais == 0) {
		document.formulario.vTipoAbono.focus();
	    cuadroDialogo("Favor de Seleccionar un Tipo de Abono",3);
        ValorRegreso = false;
	}
	return ValorRegreso;
}
//---------------------------	TERMINAN VALIDACIONES	-----------------------------------------


		function valida_formaDivisaModif(){
			if(document.formulario.vForma.value == "1") {
				document.formulario.vCvePais.disabled = true;
				document.formulario.vBanco.disabled = true;
				document.formulario.vCveABA.disabled = true;
				document.formulario.vDescripcionBanco.disabled = true;
				document.formulario.vDescripcionCiudad.disabled = true;
				document.formulario.vCtaBancoDestino.disabled = true;
				document.formulario.vBanco.selectedIndex = 0;
				document.formulario.vCvePais.selectedIndex = 0;
			} else if(document.formulario.vForma.value == "2") {
				if(document.formulario.vCveDivisa.value == "MXN") {
				document.formulario.vCvePais.disabled = true;
				document.formulario.vBanco.disabled = false;
				document.formulario.vCveABA.disabled = true;
				document.formulario.vDescripcionBanco.disabled = true;
				document.formulario.vDescripcionCiudad.disabled = true;
				document.formulario.vCtaBancoDestino.disabled = true;
				document.formulario.vCvePais.selectedIndex = 0;
			} else if(document.formulario.vCveDivisa.value == "USD") {
				document.formulario.vCvePais.disabled = true;
				document.formulario.vBanco.disabled = false;
				document.formulario.vCveABA.disabled = false;
				document.formulario.vDescripcionBanco.disabled = false;
				document.formulario.vDescripcionCiudad.disabled = false;
				document.formulario.vCtaBancoDestino.disabled = false;
				//document.formulario.vCvePais.selectedIndex = 105; CSA
			}
		}
	}

		function ajustaFormaPagoModif(){
		switch(document.formulario.vForma.selectedIndex){
			case -1: case 0:
				document.formulario.vBanco.disabled = false;
				document.formulario.vDescripcionCiudad.disabled =  false;
				// jgarcia document.formulario.vSucursal.disabled = false;
				document.formulario.vCveABA.disabled = false;
				document.formulario.vDescripcionCiudad.disabled = false;
				document.formulario.vCvePais.disabled = false;
				document.formulario.vCveDivisa.selectedIndex = 0;
				document.formulario.vCveDiv.value = "";
				document.formulario.vBanco.selectedIndex = 0;
				break;
			case 1:
				valida_formaDivisaModif();
				break;
			case 2:
				valida_formaDivisaModif();
				break;
			case 3:
				valida_formaDivisaModif();
				break;
			case 4:
				valida_formaDivisaModif();
				break;
			}
		}

	function seleccionaPlaza()
		{
		if(document.formulario.vForma.selectedIndex == 1)
			document.formulario.vPlaza.blur();
		}

	function seleccionaSucursal()
		{
		if(document.formulario.vForma.selectedIndex == 1)
			document.formulario.vSucursal.blur();
		}

//Se agrego sta parte ----------------------------------------
	function TraerCiudades()
 {
 	//var ventanaInfo2;
 	  ventanaInfo2=window.open('EI_Ciudades','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2=window.open('/Download/EI_Ciudades.html','Ciudades','width=250,height=350,toolbar=no,scrollbars=no');
	  ventanaInfo2.focus();
 }
// ----------------------------------------------------------------------------------------------


<%= (String)request.getAttribute("newMenu") %>


//------------Cambio de Ventana Internacional Y Nacional
	function MuestraInter()
	{
		//jgarcia
		var radioPersona = document.formulario.vPersona[0];
		if(radioPersona != null && radioPersona.checked){
			document.formulario.tipoPersona.value = "F";
		}else{
			document.formulario.tipoPersona.value = "M";
		}
		document.formulario.origenProveedor.value = "I";
		document.formulario.submit();
	}

	function MuestraNacional(){
		//jgarcia
		var radioPersona = document.formulario.vPersona[0];
		if(radioPersona != null && radioPersona.checked){
			document.formulario.tipoPersona.value = "F";
		}else{
			document.formulario.tipoPersona.value = "M";
		}
		document.formulario.origenProveedor.value = "N";
		document.formulario.submit();
	};

//-------------- Habilitar campos despues de cargar toda la pagina --------------
function habilitarCampos() {
	<%if(Integer.parseInt(parAccion) >= 11){%>
		document.getElementById("ProveedorNacional").disabled = true;
		document.getElementById("ProveedorInternacional").disabled = true;
		document.getElementById("PersonaFisica").disabled = true;
		document.getElementById("PersonaMoral").disabled = true;
	<%}else{%>
		<%if(((Boolean)sess.getAttribute("fac_CCALTMANPR")).booleanValue()){%>
			document.getElementById("ProveedorNacional").disabled = false;
		<%}else{%>
			document.getElementById("ProveedorNacional").disabled = true;
		<%}%>

		<%if(((Boolean)sess.getAttribute("fac_CCALTPRINTRNAL")).booleanValue()){%>
			document.getElementById("ProveedorInternacional").disabled = false;
		<%}else{%>
		document.getElementById("ProveedorInternacional").disabled = true;
		<%}%>
		document.getElementById("PersonaFisica").disabled = false;
		document.getElementById("PersonaMoral").disabled = false;
	<%}%>
}
//-------------------------------------------------------------------------------

</script>
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); inicia();"
    style="bgcolor:#ffffff;background:/gifs/EnlaceMig/gfo25010.gif">

<%-- MENU PRINCIPAL --%>
<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*"><%=(String) request.getAttribute("MenuPrincipal")%>
		</td>
	</tr>
</table>
<%=(String) request.getAttribute("Encabezado")%>
<%-- -------------- --%>

<br />

<div align="center">
<p class="titpag"><%=tituloPantalla%> de Proveedor</p>
<form name="formulario" method="POST" action="coMtoProveedor">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td rowspan="2" class="tabmovtex">* El llenado de estos campos es
		obligatorio
		<table width="660" border="0" cellspacing="2" cellpadding="3">
			<tr>
				<td class="tittabdat" colspan="2"><b> Datos Generales del Proveedor </td>
			</tr>
			<tr align="center">
				<td class="textabdatcla" valign="top" colspan="2">
					<table width="650" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td></td>
							<td></td>
							<td class="tabmovtexbol" nowrap>
								<input type="RADIO" name="vTProv" value="N" id="ProveedorNacional"
								<%
									EIGlobal.mensajePorTrace("origenGralProveedor->"+origenGralProveedor,EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("prov.origenProveedor->"+prov.origenProveedor,EIGlobal.NivelLog.DEBUG);
									EIGlobal.mensajePorTrace("pantallaOriProveedor->"+pantallaOriProveedor,EIGlobal.NivelLog.DEBUG);
									if(prov.NACIONAL.equalsIgnoreCase(pantallaOriProveedor)){
										out.println(seleccionaRadio);
									}
									if((null != origenGralProveedor && origenGralProveedor.length() > 0) || Integer.parseInt(parAccion) >= 11){
										out.println(deshabilitaRadio);
									}
								%>
								onClick="MuestraNacional();" disabled />
								<b>Nacional</b>
							</td>
							<td></td>
							<td></td>
							<td class="tabmovtexbol" nowrap>
								<input type="RADIO"	name="vPersona" value="F" id="PersonaFisica"
								<%
									out.println(prov.tipo == prov.FISICA?seleccionaRadio:"");
									if(Integer.parseInt(parAccion) >= 11){
										out.println(deshabilitaRadio);
									}
								%>
								onClick="HabilitaCamposValidos_Fisica();" disabled />
								<b>F&iacute;sica</b>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="tabmovtexbol" nowrap>
								<b>Tipo de Proveedor</b>
							</td>
							<td></td>
							<td></td>
							<td class="tabmovtexbol" nowrap><b>Personalidad Juridica</b></td>
							<td class="tabmovtexbol" nowrap></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td class="tabmovtexbol" nowrap>
								<input type="RADIO" name="vTProv" value="I" id="ProveedorInternacional"
								<%
									if(prov.INTERNACIONAL.equalsIgnoreCase(pantallaOriProveedor)){
										out.println(seleccionaRadio);
									}
									if((null != origenGralProveedor && origenGralProveedor.length() > 0) || Integer.parseInt(parAccion) >= 11){
										out.println(deshabilitaRadio);
									}
								%>
								onClick="MuestraInter();" disabled />
								<b>Internacional</b>
							</td>
							<td></td>
							<td></td>
							<td class="tabmovtexbol" nowrap>
								<input type="RADIO" name="vPersona" value="M" id="PersonaMoral"
								<%
									out.println(prov.tipo == prov.MORAL?seleccionaRadio:"");
									if(Integer.parseInt(parAccion) >= 11){
										out.println(deshabilitaRadio);
									}
								%>
								onClick="HabilitaCamposValidos_Moral();" disabled />
								<b>Moral</b>
							</td>
							<td></td>
						</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="tittabdat" colspan="2">
					<% if(prov.INTERNACIONAL.equalsIgnoreCase(pantallaOriProveedor)){%>
						<div id="ProvInter">
							<jsp:include page="coAltaProveedorInter.jsp"></jsp:include>
						</div>
					<%}else{ %>
						<div id="ProvNacional">
							<jsp:include page="coAltaProveedorNacional.jsp">
							</jsp:include>
						</div>
					<%} %>
				</td>
			</tr>
			<tr>
				<td class="tabmovtex">* El llenado de estos campos es obligatorio</td>
			</tr>
		</table>
		<br />
		<br />
		<table align="center" style="border: 0px;" cellspacing="0" cellpadding="0">
			<tr>
				<%
					//########################## Facultades
					String facultadM = "false";
			        String facultadMI="false";
			        String facultadB="false";
			        String facultadBI="false";
					if (request.getAttribute("facultadM") != null) {
						facultadM = (String) request.getAttribute("facultadM");
						EIGlobal.mensajePorTrace("Facultad Modif " + facultadM
						+ " parAccion " + parAccion,EIGlobal.NivelLog.DEBUG);
					}

					if (request.getAttribute("facultadMI") != null) {
						facultadMI = (String) request.getAttribute("facultadMI");
						EIGlobal.mensajePorTrace("Facultad Modif Inter" + facultadMI
						+ " parAccion " + parAccion,EIGlobal.NivelLog.DEBUG);
					}

					if (request.getAttribute("facultadB") != null) {
						facultadB = (String) request.getAttribute("facultadB");
						EIGlobal.mensajePorTrace("Facultad Baja " + facultadB
						+ " parAccion " + parAccion,EIGlobal.NivelLog.DEBUG);
					}

					if (request.getAttribute("facultadBI") != null) {
						facultadBI = (String) request.getAttribute("facultadBI");
						EIGlobal.mensajePorTrace("Facultad Baja Inter " + facultadBI
						+ " parAccion " + parAccion,EIGlobal.NivelLog.DEBUG);
					}
					//########################## Facultades

					if (prov.NACIONAL.equals(pantallaOriProveedor)&&
					    facultadM.equals("true") &&
					    parAccion.equals("11")) {
							System.out.println("Tiene facultad para realizar modificaciones nacionales.");
							%>
							<td align="right" valign="top" height="197">
								<a href="javascript:Aceptar();" style="border: 0px;">
								<img src="/gifs/EnlaceMig/<%= boton1 %>" style="border: 0px;" alt="<%= altBoton1 %>" />
								</a>
							</td>
							<%
					}else if (prov.INTERNACIONAL.equals(pantallaOriProveedor)&&
					    facultadMI.equals("true") &&
					    parAccion.equals("11")) {
							System.out.println("Tiene facultad para realizar modificaciones internacionales.");
							%>
							<td align="right" valign="top" height="197">
								<a href="javascript:Aceptar();" style="border: 0px;">
								<img src="/gifs/EnlaceMig/<%= boton1 %>" style="border: 0px;" alt="<%= altBoton1 %>" />
								</a>
							</td>
							<%
					}else{
							System.out.println("No tiene facultad para realizar modificaciones.");
							%>
							<td align="right" valign="top" height="197">
								<br />
							</td>
							<%
					}

					if (parAccion.equals("1")) {
					%>
						<td align="right" valign="top" height="197">
							<a href="javascript:Aceptar();" style="border: 0px;">
							<img src="/gifs/EnlaceMig/<%= boton1 %>" style="border: 0px;" alt="<%= altBoton1 %>" />
							</a>
						</td>
					<%
					}
					%>

				<%
				if (!parAccion.equals("2")) {

					if(!parAccion.equals("11")){
					%>
						<td align="center" valign="top" height="197">
						<a href="javascript:limpia();" border=0>
						<img src="/gifs/EnlaceMig/<%= boton2 %>" border=0 alt="<%= altBoton2 %>"></a>
						</td>

					<%
					}else if (prov.NACIONAL.equals(pantallaOriProveedor)&&
					          facultadB.equals("true") &&
	  					      parAccion.equals("11")) {
					System.out.println("Tiene facultad para realizar baja de nacionales.");
					%>
						<td align="center" valign="top" height="197">
						<a href="javascript:borra();" border=0>
						<img src="/gifs/EnlaceMig/<%= boton2 %>" border=0 alt="<%= altBoton2 %>"></a>
						</td>
					<%
					}else if (prov.INTERNACIONAL.equals(pantallaOriProveedor)&&
					          facultadBI.equals("true") &&
	  					      parAccion.equals("11")) {
					System.out.println("Tiene facultad para realizar baja de internacionales.");
					%>
						<td align="center" valign="top" height="197">
						<a href="javascript:borra();" border=0>
						<img src="/gifs/EnlaceMig/<%= boton2 %>" border=0 alt="<%= altBoton2 %>"></a>
						</td>
					<%
					}else{
					System.out.println("No tiene facultad para realizar baja.");
					%>
						<td align="right" valign="top" height="197"><br>
						</td>
					<%
					}
				}
				%>

				<td align="left" valign="top" height="197"><a href="javascript:Regresar();"
					border=0><img src="/gifs/EnlaceMig/gbo25320.gif" border=0
					alt="Regresar"></a></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
 <input type=hidden name="tipoPersona"	value="<%= (prov.tipo == prov.FISICA)?"F":"M" %>" />
 <input	type=hidden name="accion" value="<%= parAccion %>" />
 <input	type=hidden name="opcion" value="<%= ((Integer.parseInt(parAccion)<11)?1:2) %>" />
 <input	type=hidden name="numSelec"	value="<%= request.getParameter("numSelec") %>" />
 <input	type=hidden name="archivo_actual"	value="<%= request.getParameter("archivo_actual") %>" />
 <input	type=hidden name=numProv value="<%= request.getAttribute("numProv") %>" />
 <input type=hidden name=uso_cta_cheque value="<%=request.getAttribute("USO_CTA_CHEQUE") %>" />
 <input type=hidden name="origenProveedor" value="<%= (prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)?prov.NACIONAL:prov.INTERNACIONAL) %>" />
</form>
</div>
</body>
</html>