<%@ page language="java" %>
<%@ page contentType="text/html;charset=ISO8859_1"%>
<html>
<head>
	<title>Prepagos de Cr&eacute;dito</TITLE>
<%-- 01/10/2002 Correci�n ortogr�fica. --%>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" type="text/javascript">

<%
     if (request.getAttribute("ResultPago")!= null) {
	out.println(request.getAttribute("ResultPago"));
     }
%>

function VentanaAyuda(cad)
{
  return;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function esNumerico(Txt1)
{
  var i1=0;
  var cont=0;

  if(Txt1=="")
   {
     //alert("El importe es obligatorio.");
     document.SaldoCred.Importe.focus();
	 cuadroDialogo("El importe es obligatorio.",3);
     return false;
   }

  for(i1=0;i1<Txt1.length;i1++)
   {
	 Txt2 = Txt1.charAt(i1);
	 if((Txt2>='0' && Txt2<='9') || Txt2=='.')
 	   cont++;
    }
  if(cont!=i1)
    {
      //alert("El importe debe ser num�rico.");
 	  document.SaldoCred.Importe.focus();
	  cuadroDialogo("El importe debe ser num�rico.",3);
	  return false;
	}

  if(Txt1.indexOf('.')<0)
     Txt1+=".00";
  document.SaldoCred.Importe.value=Txt1;
  return true;
}

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
	  if(para.charAt(a2)==' ')
	   car="+";
	  else
	   car=para.charAt(a2);

	  formateado+=car;
	}
   return formateado;
 }

function Pagos()
{
  if(esNumerico(document.SaldoCred.Importe.value))
   {
     var i=0;

     param1="&Importe="+document.SaldoCred.Importe.value;
	 param2="&FacArchivo="+document.SaldoCred.FacArchivo.value;
	 param3="&ClaveBanco="+document.SaldoCred.ClaveBanco.value;
	 param4="&NumUsuario="+document.SaldoCred.NumUsuario.value;
	 param5="&NomUsuario="+document.SaldoCred.NomUsuario.value;
	 param6="&NumContrato="+document.SaldoCred.NumContrato.value;
	 param7="&NomContrato="+document.SaldoCred.NomContrato.value;

	 for(i=0;i<document.SaldoCred.length;i++)
	  if(document.SaldoCred.elements[i].type=='radio')
	    if(document.SaldoCred.elements[i].checked==true)
		  param8="&radTabla="+document.SaldoCred.elements[i].value;

	 parametro=param1+param2+param3+param4+param5+param6+param7+param8;
	 parametro=formatea(parametro);

	//vc=window.open("MCL_Pagos?Modulo=1"+parametro,'trainerWindow','width=470,height=370,toolbar=no,scrollbars=no');
	// vc.focus();

     document.SaldoCred.action="MCL_Pagos";
     document.SaldoCred.submit();
   }
}

function Posicion()
{
  document.SaldoCred.action="MCL_Posicion";
  document.SaldoCred.submit();
}


<%
     if (request.getAttribute("newMenu")!= null) {
     out.println(request.getAttribute("newMenu"));
     }
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
          if (request.getAttribute("MenuPrincipal")!= null) {
          out.println(request.getAttribute("MenuPrincipal"));
          }
    %>
   </td>
  </tr>
</table>

<%
          if (request.getAttribute("Encabezado")!= null) {
          out.println(request.getAttribute("Encabezado"));
          }
%>

<FORM  NAME="SaldoCred" method=post action="MCL_Pagos">
  <p>
  <table align=center border=0>
   <tr>
    <td>
     <%
          if (request.getAttribute("Tabla")!= null)
		    out.println(request.getAttribute("Tabla"));
     %>
    </td>
   </tr>
   <tr>
    <td align=right nowrap class="tabmovtex11">Importe $&nbsp;<input type=text name=Importe size=10 maxlength=10></td>
   </tr>
  </table>

  <input type=hidden name=Modulo value=1>
  <input type=hidden name=NumUsuario value='<%=request.getAttribute( "NumUsuario" ) %>'>
  <input type=hidden name=NomUsuario value='<%=request.getAttribute( "NomUsuario" ) %>'>
  <input type=hidden name=NumContrato value='<%=request.getAttribute( "NumContrato" ) %>'>
  <input type=hidden name=NomContrato value='<%=request.getAttribute( "NomContrato" ) %>'>
  <input type=hidden name=ClaveBanco value='<%=request.getAttribute( "ClaveBanco" ) %>'>
  <input type=hidden name=FacArchivo value='<% if (request.getAttribute("FacArchivo")!= null) { out.print(request.getAttribute("FacArchivo")); } %>'>

  <p>
  <table align=center border=0 cellspacing=0 cellpadding=0>
   <tr>
     <td><A href = "javascript:Posicion();" border = 0><img src = "/gifs/EnlaceMig/gbo25270.gif" border="0" alt="Posici&oacute;n"></a></td>
     <td><A href = "javascript:Pagos();" border = 0><img src = "/gifs/EnlaceMig/gbo25350.gif" border="0" alt="Pagos"></a></td>
   </tr>
  </table>
</form>
</body>
</html>
