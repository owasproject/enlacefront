
<%@page import="mx.altec.enlace.bo.BaseResource"%>
<%-- ATENCION GENTE DE MANTENIMIENTO: NO PONER COMENTARIOS con // y asteriscos, porque env�a error de javascript--%>
<html>
<head>
<title>Banca Virtual</title>
<script type="text/javascript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<SCRIPT type="text/javascript" SRC="/EnlaceMig/list.js"></SCRIPT>
<script type="text/javascript" src="/EnlaceMig/validacionesFiltroCuentas.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta http-equiv="Expires" content="1"/>
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="version" content="1.0"/>
<meta http-equiv="JSP" content="mis_transferencias_tarjeta.jsp"/>
<%-- Se necesita importar el js de favoritos para poder realizar una consulta y carga de favoritos. --%>
<script type="text/javascript" src="/EnlaceMig/FavoritosEnlaceJS.js"></script>
<script type="text/javascript">

// FSW INDRA PyMES .. Marzo 2015
function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}

 // FSW INDRA PyMES .. Marzo 2015
</script>

<%
String lineaDISP = (String)request.getAttribute("TDC2CHQ");
if(lineaDISP == null || !lineaDISP.equals("1")) {%>


<script type="text/javascript">
<%-- *********************************************** --%>
<%-- modificaci�n para integraci�n pva 07/03/2002    --%>
<%-- *********************************************** --%>
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var opcion;
var tramadicional;
var cfm;

<%
	String tdc2chq = (String)request.getAttribute("TDC2CHQ");
	tdc2chq = (tdc2chq==null?"":tdc2chq);
%>
<%
	HttpSession     sess = request.getSession();
    mx.altec.enlace.bo.BaseResource session2 = (mx.altec.enlace.bo.BaseResource) sess.getAttribute("session");
	if( tdc2chq.equals("1") )
	{
		session2.setModuloConsultar("43@");
	}
%>

function CuentasCargo()
{
   opcion=1;
   //PresentarCuentas();
   // Q1364 - Maria Elena de la Pe�a
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de Cuentas'");

msg.focus();
}

function msgCifrado(){
	 // FSW INDRA PyMES .. Marzo 2015
  	//consultaCuentasTarjetaPago();
  	//consultaCuentasTarjetaAbono();
  	if (document.transferencia.esFav.value != null
			&& document.transferencia.esFav.value != ""
			&& document.transferencia.esFav.value == "1") {
		var cadenaCargo = '0,2,1';
		var cadenaAbono = '1,2,3';
		cadenaCargo = cadenaCargo + ',' + document.transferencia.numeroCuenta0.value + ',';
		cadenaAbono = cadenaAbono + ',' + document.transferencia.numeroCuenta1.value + ',';
		consultaMultiplesCuentas(cadenaCargo+'|'+cadenaAbono);
	} else {
		consultaMultiplesCuentas('0,2,1|1,2,3');
	}
 	document.transferencia.esFav.value = "";
	document.transferencia.numeroCuenta0.value = "";
	document.transferencia.numeroCuenta1.value = "";
  // FSW INDRA PyMES .. Marzo 2015
 	try {
	var mensajeCifrado = document.transferencia.mensajeCifrado.value;
	if(mensajeCifrado != null && mensajeCifrado != "" && mensajeCifrado != "null") {
		var arreglo = mensajeCifrado.split("|");
               var msj = arreglo[1];
               var tipo = arreglo[0];
               cuadroDialogo( msj ,tipo);
	}
	} catch (e){};

}

function CuentasAbono()
{
   // Q1364 - Maria Elena de la Pe�a
   opcion=2;
   //PresentarCuentas();
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=3","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de Cuentas'");

  msg.focus();

}

	/* INICIA CAMBIO PYME FSW - INDRA MARZO 2015 se agrega funcion
	 * para habilitar o deshabilitar la descripcion del favorito
	 */
	function modificarTxt(chbFavorito,numRen){
	   var txtFavorito = document.getElementById("favTxt"+numRen);

	   if(chbFavorito.checked == 0){ //deshabilitado
		   txtFavorito.disabled = true;
		   txtFavorito.value ="";
	   }else{
		   txtFavorito.disabled = false;
		   txtFavorito.value ="";
	   }
	}
/*
function PresentarCuentas()
{
  // Q1364 - Maria Elena de la Pe�a
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de Cuentas'");
  msg.focus();
}
*/
function actualizacuenta()
{
  if(opcion==1)
  {
	  document.transferencia.origen.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
      //document.transferencia.textorigen.value=ctaselec+" "+ctadescr;
  }
  else
  {
  	  document.transferencia.destino.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
      //document.transferencia.textdestino.value=ctaselec+" "+ctadescr;
  }
}

//<!-- Q1364 - Maria Elena de la Pe�a - inicio modificaci�n-->
function EnfSelCta()
{
	/** INICIO CAMBIO PYME FSW - INDRA MARZO 2015
		validacion para cargar un favorito */
	var bandera = <%=request.getAttribute("esFavorito")%>;
	if( bandera == "1" ){
		document.transferencia.concepto.value = "<%=request.getAttribute("concepto")%>";
		document.transferencia.monto.value = "<%=request.getAttribute("monto")%>";
		document.transferencia.numeroCuenta0.value = "<%=request.getAttribute("ctaCargoFav")%>";
		document.transferencia.numeroCuenta1.value = "<%=request.getAttribute("ctaAbonoFav")%>";
    document.getElementById('Favorito').value='1';
	}
	/** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
   //document.transferencia.textorigen.value="";
   //document.transferencia.textdestino.value="";
}

<%
if(request.getAttribute("ArchivoErr")!=null) {
%>
<%= request.getAttribute("ArchivoErr") %>
<%}%>
//<!-- Q1364 - Maria Elena de la Pe�a - fin modificaci�n -->

function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function validAmount (cantidad,band)// Cambio para comprobante fiscal
{

  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad
  var miles="";


  if (cantidadAux == "" || cantidadAux <= 0)
  {
    // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
    if (band=="IMPORTE")
      document.transferencia.monto.value="";
    else
	 document.transferencia.IVA.value="";
    //document.transferencia.monto.focus();
    cuadroDialogo("\n Capture el "+band+"  a transferir ",3);//*****Cambio para comprobante fiscal
    // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
       // Q1364 - Maria Elena de la Pe�a - inicion modificaci�n
       if (band=="IMPORTE")
	  document.transferencia.monto.value="";
     else
	   document.transferencia.IVA.value="";

      //document.transferencia.monto.focus();
      cuadroDialogo("Favor de ingresar un valor num\351rico en el "+band,3) //***** Cambio para comprobante fiscal
      // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
      return false
    }

	formato = /^[0-9]{0,8}(\.[0-9]{0,2}){0,1}$/;
	if (!formato.test(cantidad))
	{
		document.transferencia.monto.value="";
		cuadroDialogo("\n Solamente se permite capturar <br>hasta 8 digitos enteros y dos decimales",3);// Cambio para comprobante fiscal
		return false;
	}

    pos_punto = cantidadAux.indexOf (".")

    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
	cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
	entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
	cents = "00"
	entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
	cientos = entero.substring (entero.length - 3, entero.length)
	miles = entero.substring (0, entero.length - 3)
      }
      else
      {
	if (entero.length > 3) //-- si se teclearon mas de mil sin coma
	{
	  cientos = entero.substring (entero.length - 3, entero.length)
	  miles = entero.substring (0, entero.length - 3)
	}
	else
	{
	  if (entero.length == 0)
	    cientos = ""
	  else
	    cientos = entero
	  miles = ""
	}
      }

      if (miles != "")
	strAux1 = miles
      if (cientos != "")
	strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
	strAux2 = miles
      if (cientos != "")
	strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      transf = document.transferencia.monto.value
    }
    document.transferencia.montostring.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
	 document.transferencia.montostring.value = strAux2;
    return true;
  }
}

function Formatea_Importe2 (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
	cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
	entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
	cents = "00"
	entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
	cientos = entero.substring (entero.length - 3, entero.length)
	miles = entero.substring (0, entero.length - 3)
      }
      else
      {
	if (entero.length > 3) //-- si se teclearon mas de mil sin coma
	{
	  cientos = entero.substring (entero.length - 3, entero.length)
	  miles = entero.substring (0, entero.length - 3)
	}
	else
	{
	  if (entero.length == 0)
	    cientos = ""
	  else
	    cientos = entero
	  miles = ""
	}
      }

    if (miles != "")
	strAux1 = miles
      if (cientos != "")
	strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

   strAux1=Formatea_Importe(strAux1)
  }
  return strAux1;

}


<!-- Q1364 - Maria Elena de la Pe�a -->
function Valida_Date ()
{
  document.transferencia.fecha1.value=document.transferencia.fecha_completa.value;
  return result;
}

function validctasdif (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus()
    return false
  }
  return result;
}

function validctasdifNoReg (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus()
    return false
  }
  return result;
}

function definir_valores_checkbox()
{
  // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
  document.tabla_transferencias.arregloestatus2.value="";
  document.transferencia.arregloestatus1.values="";
  valor_asignar="";
  var chbx; //CAMBIO PYME FSW - INDRA MARZO 2015 para que favoritos no afecte a lo existente
  for (i=0;i<document.tabla_transferencias.elements.length;i++)
  {
     if (document.tabla_transferencias.elements[i].type=="checkbox")
     {
		valor_asignar="1";
		chbx = document.tabla_transferencias.elements[i];//CAMBIO PYME FSW - INDRA MARZO 2015 para que favoritos no afecte a lo existente
		if ( document.tabla_transferencias.elements[i].checked == true &&
        		(chbx.id).indexOf('favChb') == -1 ){//CAMBIO PYME FSW - INDRA MARZO 2015 para que favoritos no afecte a lo existente
        									   // se agrega otra condicion al if

		  valor_asignar="1";
		} else if( (chbx.id).indexOf('favChb') == -1 ){//CAMBIO PYME FSW - INDRA MARZO 2015 para que favoritos no afecte a lo existente
			valor_asignar="0";
	  	}
	  	if( (chbx.id).indexOf('favChb') == -1 ){//CAMBIO PYME FSW - INDRA MARZO 2015 para que favoritos no afecte a lo existente
			document.tabla_transferencias.arregloestatus2.value=document.tabla_transferencias.arregloestatus2.value+valor_asignar+"@";
		}
     }
  }
  document.transferencia.arregloestatus1.value=document.tabla_transferencias.arregloestatus2.value;
  // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
}

function EsAlfa(cadena,band)//Cambio para comprobante fiscal
{

  for (var i=0;i<cadena.length;i++)
  {
      if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9')))
      {
          // Q1364 - Maria Elena de la Pe�a
	  cuadroDialogo("No se permiten caracteres especiales en "+band+ ", como: acentos, comas, puntos, etc",3);//    *****Cambio para comprobante fiscal
	  return false;
      }

  }
  return true;
}

//<! -- Q1364 - Maria Elena de la Pe�a -->
function validar_tipo_cuentas(cta_origenv,cta_destinv)
{
    /* modificacion para integracion
	   var cta_origenv=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;
   var cta_destinv=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value;*/

   var cta_origenv=document.transferencia.origen.value;
   var cta_destinv=document.transferencia.destino.value;

   // Q1364 - Maria Elena de la Pe�a - inicion modificaci�n
   var sctaorigen=cta_origenv.substring(0,cta_origenv.indexOf("|"));
   var sctadestino=cta_destinv.substring(0,cta_destinv.indexOf("|"));

   if (  ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||
	 ((cta_origenv.substring(0,2)=="49") && (sctaorigen.length==11)) )
   {
     if  ( ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83"))||
	   ((cta_destinv.substring(0,2)=="49") && (sctadestino.length==11)))
	return true;
     else
     {
	cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);
	return false;
     }
   }
  else if ( ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||
	    ((cta_destinv.substring(0,2)=="49") && (sctadestino.length==11)))
  {
    if ( ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||
	 ((cta_origenv.substring(0,2)=="49") && (sctaorigen.length==11)) )
	return true;
     else
     {
	cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);
	return false;
     }
   }
   else
     return true;
     // Q1364 - Maria Elena de la Pe�a - fin modifica
}
/*-------------------------------------------------------------------*/
function EsNumero(Dato){
    var retorno=false;
	var CadenaNumeros= "0123456789";
	var EsteCaracter;
	var Contador = 0;
	for (var i=0; i < Dato.length; i++) {
		EsteCaracter = Dato.substring(i , i+1);
		if (CadenaNumeros.indexOf(EsteCaracter) != -1)
			Contador ++;
	}
	if (Contador == Dato.length)
	 {
		//Todos los caracteres son numeros!
	//alert("!Esta bien ! es un numero.");
	    retorno=LongitudCorrecta(document.transferencia.destinoNoReg.value)
 	 }
	else
	 {
                // Q1364 - Maria Elena de la Pe�a
		cuadroDialogo("N&uacute;mero de cuenta inv&aacute;lido",3);
		//document.transferencia.monto.focus();
	 }

	return retorno;
}

/*function validaDestinoNoReg(Dato){
    var retorno=false;
	EsNumero(document.transferencia.destinoNoReg.value);
	retono = true ;
	}
return retorno;
}*/

function LongitudCorrecta(Dato){
    var retorno=false;
	if (Dato.length == 11 || Dato.length == 16){
		retorno = true;
	} else {
                // Q1364 - Maria Elena de la Pe�a
		cuadroDialogo("Cuenta inv&aacute;lida.\nPara cuenta usar 11 d&iacute;gitos : 12345678901\nPara Tarjeta de Cr&eacute;dito usar 16 d&iacute;gitos : 1234567890123456",3);
		//document.transferencia.monto.focus();
	}
return retorno;
}

function CuentasDiferentesNoReg(Dato){
//var origen1 = "";
var retorno=false;
/*modificacion para integracion
origen1=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value */
origen1=document.transferencia.origen.value;
    //origen1 = String(document.transferencia.origenNoReg.value);
var cuentaOrigen = origen1.substring(0, origen1.indexOf("|"));
//alert(cuentaOrigen);
if (cuentaOrigen == Dato ){
	cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3)
	//document.transferencia.monto.focus();
	}
else {
	retorno = true;
	}
return retorno;
}
/*----------------------------------------------------*/
function validar_tipo_cuentasNoReg()
{
   /*  modificacion para integracion
	   var cta_origenv=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;*/
   var cta_origenv=document.transferencia.origen.value;
   var cta_destinv=document.transferencia.destinoNoReg.value;

   // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
   var sctaorigen=cta_origenv.substring(0,cta_origenv.indexOf("|"));
   var sctadestino=cta_destinv.substring(0,cta_destinv.length);

   if  (((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||
	((cta_origenv.substring(0,2)=="49") && (sctaorigen.length==11)) )
   {
     if  ( ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||
	   ((cta_destinv.substring(0,2)=="49") && (sctadestino.length==11)))
	return true;
     else
     {
       cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes 1",3);
       return false;
     }
   }
   else if (((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83")) ||
	    ((cta_destinv.substring(0,2)=="49") && (sctadestino.length==11)))
   {
     if (((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83")) ||
	((cta_origenv.substring(0,2)=="49") && (sctaorigen.length==11)) )
       return true;
     else
     {
       cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes 2",3);
       return false;
     }
     // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
   }
   else
     return true;
}
/*----------------------------------------------------*/

 js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';

var dia;
var mes;
var anio;
var fecha_completa;


function WindowCalendar()
{
    var m=new Date();
    n=m.getMonth();
    n=document.transferencia.mes.value-1;
    dia=document.transferencia.dia.value;
	mes=document.transferencia.mes.value;
	anio=document.transferencia.anio.value;

    // Q1364 - Maria Elena de la Pe�a
    msg=window.open("/EnlaceMig/calfut2.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

function Actualiza()
{
   document.transferencia.fecha_completa.value=fecha_completa;
}


/*function WindowCalendar1()
{
    var m=new Date();
    n=m.getMonth();
    n=document.transferencia.mes.value-1;

    msg=window.open("/EnlaceMig/caltrans2.htm#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}*/

function obtenmensaje()
{
    var cuenta="";
	var des="";
	var mensaje="";
    if(eval(document.transferencia.banderaNoReg.value)==0)
	{
       /*  modificacion para integracion
		   cuenta=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value; */
	   cuenta = document.transferencia.destino.value;
	   des=cuenta;
       cuenta=cuenta.substring(0,cuenta.indexOf("|"));
       des=des.substring(des.indexOf("|")+1,des.length);
       des=des.substring(des.indexOf("|")+1,des.length-1);
       mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +"  de "+des+" la cantidad de $"+Formatea_Importe2 (document.transferencia.monto.value) +"?";
    }
	else
	{
	   if  (document.transferencia.ChkOnline[0].checked)
       {

		  /*  modificacion para integracion cuenta=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value; */
	      cuenta = document.transferencia.destino.value;
		  des=cuenta;
          cuenta=cuenta.substring(0,cuenta.indexOf("|"));
          des=des.substring(des.indexOf("|")+1,des.length);
          des=des.substring(des.indexOf("|")+1,des.length-1);
          mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +"  de "+des+" la cantidad de $"+Formatea_Importe2 (document.transferencia.monto.value) +"?";
	   }
	   else
	   {
	     cuenta=document.transferencia.destinoNoReg.value;
	     mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +" la cantidad de $ "+Formatea_Importe2 (document.transferencia.monto.value) +"?";
       }
    }
    return mensaje;
}

function validarDISP()
{
var result=true;
/*
var cadenaOut  = new String ("");
cadenaOut=document.transferencia.RFC.value;
document.transferencia.RFC.value = cadenaOut.toUpperCase();
*/
  //alert("entra a validar");
  var tipo=0

  if (eval(document.transferencia.banderaNoReg.value)==0)
     tipo=0;
  else
{
     if(document.transferencia.ChkOnline[0].checked)
	   tipo=0;
     else
	   tipo=1;

  }

  if(tipo==0)
  {

      /*  modificacion para integracion
		  result = validctasdif (document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value,document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value)  */

	  if(document.transferencia.origen.value.length==0)

      {
		cuadroDialogo("Se debe especificar la <%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de Cargo",3);
        return false;
      }
	  if ((result==true)&&(document.transferencia.destino.value.length==0))
      {
		cuadroDialogo("Debe indicar la <%=tdc2chq.equals("1")?"Cuenta de Abono":"Tarjeta de cr&eacute;dito"%>",3);
		return false;
      }

	  if (result==true)
	    result = validctasdif (document.transferencia.origen.value,document.transferencia.destino.value)

       if (result == true)
      {
        result = Valida_Date (document.transferencia.dia.value,document.transferencia.mes.value,document.transferencia.anio.value);
      }
      if (result == true)
      {
        result=validar_tipo_cuentas();

      }
/***/

      if (result == true)
      {
        result = validAmount (document.transferencia.monto.value,"IMPORTE"); //cambio para comprobante fiscal
      }
/*     if (result == true)
      {
        //result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");//Cambio para comprobante fiscal
		result = validaConcepto(document.transferencia.concepto);
      }*/

      if (result == true)
	  {
        definir_valores_checkbox();
      }
//    ****Inicia cambio para comprobante fiscal
/***/
/*
     if(result==true)
	 {
	   if (document.transferencia.CHRCF[0].checked)
       {
         if (eval(document.transferencia.RFC.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
           return false;
	     }
         if (eval(document.transferencia.IVA.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el IVA",3);
           return false;
	     }
 	     result=EsAlfa(document.transferencia.RFC.value,"RFC");
         if (result==true)
          result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
         if(!formato_rfc_ok(document.transferencia.RFC))


		 {
	           cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		 }

       }
     }

*/
     //    ****termina cambio para comprobante fiscal

    return result;
  } //fin del if de ChkOnline ==0
	  if(tipo==1){
//     document.transferencia.FileTrans.value = "";
//		alert("Llegaa hasta la segunda opcion");
		if (document.transferencia.destinoNoReg.value ==""){
			cuadroDialogo("Debe indicar la <%=tdc2chq.equals("1")?"Cuenta de Abono":"Tarjeta de cr&eacute;dito"%>",3);
	  	    result = false;
		}
		else
		{

		  if(document.transferencia.origen.value.length==0)
          {
            result=false;
		    cuadroDialogo("Se debe especificar la <%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de Cargo",3);
          }
	  	  if(result==true)

		    result = EsNumero(document.transferencia.destinoNoReg.value);
  	      if (result == true)
		  {
		    /*  modificacion para integracion

				origen1 = document.transferencia.origen.options[ document.transferencia.origen.selectedIndex].value */
            origen1 = document.transferencia.origen.value;
            cuentaOrigen = origen1.substring(0, origen1.indexOf("|"))
 		    result = validctasdifNoReg(cuentaOrigen,document.transferencia.destinoNoReg.value)
		  }
  	      if (result == true)
		  {
	        result = Valida_Date (document.transferencia.dia.value,document.transferencia.mes.value,document.transferencia.anio.value);
	      }
		  if (result == true)
	      {
		    result=validar_tipo_cuentasNoReg();
//			alert("Llega hasta validar tipo de cuenta no reg")
	      }
	      if (result == true)
	      {
	        result = validAmount (document.transferencia.monto.value,"IMPORTE");
	      }
/*	     if (result == true)
		  {
	        //result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");
			result=validaConcepto(document.transferencia.concepto);
	      } */
	      if (result == true)
		  {
            definir_valores_checkbox();
	      }
		} //fin de else
	 } //fin del if de ChkOnline ==2

     //    ****Inicia cambio para comprobante fiscal
	 /***/
	 /*
     if(result==true)
	 {
	   if (document.transferencia.CHRCF[0].checked)
       {
         if (eval(document.transferencia.RFC.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
           return false;
	     }
         if (eval(document.transferencia.IVA.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el IVA",3);
           return false;
	     }
 	     result=EsAlfa(document.transferencia.RFC.value,"RFC");
         if (result==true)
          result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
         if(!formato_rfc_ok(document.transferencia.RFC))
		 {
	           cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		 }

       }
     }
	 */
     //    ****termina cambio para comprobante fiscal
    return result;
}


// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + "|" + res[1] + "|"  + res[2] + "|";
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES


function validar()
{
var result=true;


	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES

	obtenerCuentaSeleccionada("comboCuenta0", "origen");

	obtenerCuentaSeleccionada("comboCuenta1", "destino");


	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES


  // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
  document.transferencia.concepto.value=document.transferencia.concepto.value.toUpperCase();

  var ov;
  if (eval(document.transferencia.ChkOnline.length)==2)
    ov=1;
  else
    ov=2;
  var cta_abono="";

var cadenaOut  = new String ("");
//cadenaOut=document.transferencia.RFC.value;
//document.transferencia.RFC.value = cadenaOut.toUpperCase();

  if(document.transferencia.INICIAL.value=="SI")
  {
    if (eval(document.transferencia.ChkOnline.length)==2)
	{
	   if(document.transferencia.ChkOnline[0].checked)
	     document.transferencia.TIPOTRANS.value="LINEA";
       else
	     document.transferencia.TIPOTRANS.value="ARCHIVO";
    }
	else if  (eval(document.transferencia.ChkOnline.length)==3)
	{
       if((document.transferencia.ChkOnline[0].checked)||(document.transferencia.ChkOnline[1].checked))
	     document.transferencia.TIPOTRANS.value="LINEA";
       else
	     document.transferencia.TIPOTRANS.value="ARCHIVO";
	}
  }
  if (document.transferencia.INICIAL.value=="NO")
  {
     if(document.transferencia.TIPOTRANS.value=="LINEA")
     {
	   if (eval(document.transferencia.contador1.value)>=eval(document.transferencia.NMAXOPER.value))
	   {
	  result=false;
		  cuadroDialogo("Solo se permiten adicionar hasta "+document.transferencia.NMAXOPER.value +" operaciones en l&#237;nea",1);
		}
	 }
  }


  	//VSWF ESC validacion formulario multipart
	if(document.transferencia.TIPOTRANS.value == "ARCHIVO")
	{
		document.transferencia.encoding="multipart/form-data";
	}


  if((result==true)&&(document.transferencia.ChkOnline[0].checked))
  {
      //document.transferencia.FileTrans.value = "";
      /*  modificacion para integraci�n
		  result = validctasdif (document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value,document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value)
*/
      if(document.transferencia.origen.value.length==0)
      {
	result=false;
		cuadroDialogo("Se debe especificar la cuenta de Cargo",3);
      }
      if ((result==true)&&(document.transferencia.destino.value.length==0))
      {
	result=false;
		cuadroDialogo("Se debe especificar la cuenta de Abono",3);
      }

	  if (result==true)
	    result = validctasdif (document.transferencia.origen.value,document.transferencia.destino.value)

       if (result == true)
	result = Valida_Date();


      if (result == true)
       /* modificacion para integracion result=validar_tipo_cuentas(document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value,document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value);*/
       result=validar_tipo_cuentas(document.transferencia.origen.value,document.transferencia.destino.value);

     if (result == true)
	result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");//*****Cambio para comprobante fiscal
      if (result == true)
	result = validAmount (document.transferencia.monto.value,"IMPORTE"); //*****Cambio para comprobante fiscal
      if ((result == true)&&(document.transferencia.contador1.value>0))
	definir_valores_checkbox();
      /*modificacion para integracion
		  cta_abono=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value; */
       cta_abono=document.transferencia.destino.value;

	//****inicia Cambio para comprobatne fiscal
     /*if(result==true)


	 {
	   if (document.transferencia.CHRCF[0].checked)
       {
	 if (eval(document.transferencia.RFC.value.length)==eval(0))
	     {
	   cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
	   return false;
	     }
	 if (eval(document.transferencia.IVA.value.length)==eval(0))
	     {
	   cuadroDialogo("Se debe especificar el IVA",3);
	   return false;
	     }
 	     result=EsAlfa(document.transferencia.RFC.value,"RFC");
	 if (result==true)
	  result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
		if (result==true)
		{
	 if(!formato_rfc_ok(document.transferencia.RFC))
		 {
		   cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		 }

		}
      }
    }
*/
     //    ****termina cambio para comprobante fiscal

  } //fin del if de ChkOnline ==0

  if ((result==true)&&(document.transferencia.ChkOnline[ov].checked))
  {
	if(document.transferencia.FileTrans.value == "")
	{
		cuadroDialogo("Debe de asignar un nombre de archivo",3);
		return	false;
	}
	else {
  	   	//window.alert("Return true...");
  	   	return true;
  	}
   } //fin del if de ChkOnline ==1

   if((result==true)&&(eval(document.transferencia.ChkOnline.length)==3)&&    (document.transferencia.ChkOnline[1].checked))
		{

		  if(document.transferencia.origen.value.length==0)
	  {
	    result=false;
		    cuadroDialogo("Se debe especificar la cuenta de Cargo",3);
	  }

	  if ((result==true)&&(document.transferencia.destinoNoReg.value ==""))
	  {
		 cuadroDialogo("Debe indicar una cuenta de abono",3);
		 result = false;
	  }

	  	  if(result==true)
	  {
		    result = EsNumero(document.transferencia.destinoNoReg.value);
  	      if (result == true)
		  {
		    cta_abono=document.transferencia.destinoNoReg.value;
		    /*	modificacion para integracion
				origen1 = document.transferencia.origen.options[ document.transferencia.origen.selectedIndex].value */
	    origen1 = document.transferencia.origen.value;
	    cuentaOrigen = origen1.substring(0, origen1.indexOf("|"))
 		    result = validctasdif(cuentaOrigen,document.transferencia.destinoNoReg.value)
		  }
  	      if (result == true)
		    result = Valida_Date();
		  if (result == true)
	      /*  modificacion para integracion
			 result=validar_tipo_cuentasNoReg(document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value,document.transferencia.destinoNoReg.value);*/
	     result=validar_tipo_cuentasNoReg(document.transferencia.origen.value,document.transferencia.destinoNoReg.value);

	     if (result == true)
		   result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");//*****Cambio para comprobante fiscal

	      if (result == true)
		result = validAmount (document.transferencia.monto.value,"IMPORTE"); //*****Cambio para comprobante fiscal
	     if ((result == true)&&(document.transferencia.contador1.value>0))
	    definir_valores_checkbox();

	//    ****Inicia cambio para comprobante fiscal
    /*
      if(result==true)
	{
	  if (document.transferencia.CHRCF[0].checked)
      {
	if (eval(document.transferencia.RFC.value.length)==eval(0))
	    {
	  cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
	  return false;
	    }
	if (eval(document.transferencia.IVA.value.length)==eval(0))
	    {
	  cuadroDialogo("Se debe especificar el IVA",3);
	  return false;
	    }
 	    result=EsAlfa(document.transferencia.RFC.value,"RFC");
		if (result==true)
	  result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
	if(!formato_rfc_ok(document.transferencia.RFC))
		{
		   cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		}

      }
    }
*/
     //    ****termina cambio para comprobanye fiscal
		} //fin de else
	} //fin del if de ChkOnline ==1
    return result;
    // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
}

//*************************************************
//*****inicia Cambio para comprobante fiscal
//**********************************************

function formato_rfc_ok(rfc)
 {
   var fecha=6;

   var formato="";

   switch(rfc.value.length)
	 {
	   case 13: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
	   case 10: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
	   case 12: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
	   case 9: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
    }
   if(formato=="")
     return false;
   if(rfc.value.length==9 || rfc.value.length==12)
     fecha=5;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(fecha,fecha+2);
   var dia=rfc.value.substring(fecha+2,fecha+4);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
    }
   else
     return false;
   return true;
 }

/*function formato_rfc_ok(rfc)
 {
   if(rfc.value.length>11)
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/;
   else
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
   if(!formato.test(rfc.value))
	   return false;

   var mes=rfc.value.substring(6,8);
   var dia=rfc.value.substring(8,10);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
       }
	else
	 return false;
   return true;
     }
*/

//<! -- Q1364 - Maria Elena de la Pe�a - inicio modificaci�n -->
var impdialog;
function validar2(forma)
{
   var cont=0;
   var txtFav = new String (""); //CAMBIO PYME FSW - INDRA MARZO 2015
   if (document.tabla_transferencias.contador2.value>0)
   {
      if(document.tabla_transferencias.imptabla.value=="SI")
	  {
	  	var chb; //CAMBIO PYME FSW - INDRA MARZO 2015
		var descFav;
		 for( i2 = 0 ; i2 < forma.length ; i2++ ){
		 	if( forma.elements[i2].type == 'checkbox' ){
		     	if( forma.elements[i2].checked == true ){
					/** INICIA CAMBIO PYME FSW - INDRA MARZO 2015 obtiene el valor del check
			    		favorito si fue seleccionado */
			    	chb = forma.elements[i2];
				    if( (chb.id).indexOf('favChb') > -1 ){
						descFav = trimString(document.getElementById("favTxt"+(chb.id).substring(6)).value);
				    	if( descFav == '' ){
				    		cuadroDialogo("No se ha proporcionado una descripci&oacute;n para el favorito.",3);
      						return false;
				    	}
				    	if(!validaEspecialesFavorito(document.getElementById("favTxt"+(chb.id).substring(6)),"Favoritos"))
				    	{
				    		return false;
				    	}
		           		txtFav = txtFav + (chb.id).substring(6) + "@" + document.getElementById("favTxt"+(chb.id).substring(6)).value + "@";

		            } else {
		            // se pone en el else para que no afecte con el funcionamiento que
		            // ya tenia y solo cuente los check de cada operacion
		            	cont++;
		            }
		            /** FIN CAMBIO PYME FSW - INDRA MARZO 2015 */
				}
			}
		 }
		 document.tabla_transferencias.strFav.value = txtFav;
		 if(cont > 0)
		 {
			   definir_valores_checkbox();
			   result=true;
	  	   	   if (cont==1)
			   {
			      impdialog=1;
				  result=true;
		   }
			   else
			   {
			     impdialog=0;
				 document.tabla_transferencias.submit();
		   }
		 }
		 else
		     {
		   cuadroDialogo("Debe seleccionar m&#237;nimo una transferencia.",3);
		   result = false;
		     }
      }
   }
   else
   {
      cuadroDialogo("No se ha adicionado ninguna transferencia",3);
      result= false;
   }
   return result;
}
<%-- Q1364 - Maria Elena de la Pe�a - fin moficicaci�n --%>

var gConfirmacion= false;
function confirmacion()
{
  // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
<% if ( !tdc2chq.equals("1") ) {%>
  var  cuenta=document.tabla_transferencias.arregloctas_destino2.value;
  var  des=cuenta;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  des=des.substring(des.indexOf("|")+1,des.length);
  des=des.substring(des.indexOf("|")+1,des.length-2);
  var imp=document.tabla_transferencias.arregloimportes2.value;
  imp=imp.substring(0,imp.length-1);

  result=validar2(document.tabla_transferencias);
  gConfirmacion = result;
  if (( result == true )&&(impdialog==1))
  {
//   	cuadroDialogo(" Desea abonar a la cta: "+cuenta+" de "+des+ " la cantidad de  $"+Formatea_Importe2(imp)+"?",2);
  	document.tabla_transferencias.submit();
  }
  <%} else { %>
  result=validarDISP();
  gConfirmacion = result;
  if ( result == true )
  {
      mensaje=obtenmensaje();
      cuadroDialogo(mensaje,2);
   }
  <%}%>
   // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
}

function continua()
{
   if (gConfirmacion == true )
   {
      // Q1364 - Maria Elena de la Pe�a
      if (respuesta==1) document.tabla_transferencias.submit();
	else return false;

   }
   else
       return false;
}




function limpiar_datos()
{
	limpiarBusqueda();
   // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
   document.transferencia.concepto.value="";
   document.transferencia.monto.value="";
   //document.transferencia.textorigen.value="";
   document.transferencia.origen.value="";

 /*   modificacion para integracion
   document.transferencia.origen.selectedIndex=0;

   if (document.transferencia.ChkOnline[0].checked)
       document.transferencia.destino.selectedIndex=0;*/

   //document.transferencia.textdestino.value="";
   document.transferencia.destino.value="";
   if (eval(document.transferencia.ChkOnline.length)==3)
   	   document.transferencia.destinoNoReg.value="";
   document.transferencia.fecha_completa.value=document.transferencia.fecha1.value;

   document.transferencia.FileTrans.value="";
   document.transferencia.ChkOnline[0].checked=true;
   // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
}

function verificartipo()
{
    var respuesta=true;
    if (eval(document.transferencia.contador1.value)>0)
	{
	   respuesta=false;
           // Q1364 - Maria Elena de la Pe�a
	   cuadroDialogo("S&#243;lo se pueden agregar transferencias en l&#237;nea",3);
	   document.transferencia.ChkOnline[0].checked=true;
    }
    return respuesta;
}

//    ****inicia Cambio para comprobante fiscal

/***/
/*
function checaestatus(campo)
{
  var result=true;

  if (eval(document.transferencia.CHRCF.value)==1)
  {
    result=true;
  }
  else
  {

	document.transferencia.RFC.value="";
	document.transferencia.IVA.value="";
    campo.blur();
	result=false;
  }
  return result;
}
*/

/***/
/*

function inicial()
{
  document.transferencia.CHRCF[1].checked=true;
  document.transferencia.CHRCF.value=0;
  document.transferencia.RFC.focus();
  document.transferencia.IVA.focus();
  return true;
}*/

/***/
/*
function limpiar_cf()
{
  document.transferencia.RFC.value="";
  document.transferencia.IVA.value="";
  document.transferencia.CHRCF.value=0;
  document.transferencia.CHRCF[1].checked=true;
  return true;
}*/

/***/
/*function asignar_valor()
{
  document.transferencia.CHRCF.value=1;
}
*/

function trimString (str)
{
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function redondea(num)
{
  var a=0;
  var b=0;

  a=parseInt(Math.round(num*100));
  b=a/100;
  return b;
}

function quitar_ceros_izquierdos(dato)
{
	var EsteCaracter;
	var contador = 0;
	var sinceros ="";
	for (var i=0; i < dato.length; i++) {
		EsteCaracter = dato.substring(i , i+1);
		if (EsteCaracter=="0")
			contador ++;
		else
			break;
	}
	sinceros=dato.substring(contador,dato.length);
	return sinceros;
}

function validaConcepto(obj)
{
	res = false;
	if(trimString(obj.value)!="")
	{
		res = EsAlfa(obj.value,"CONCEPTO");
	}
	else
	{
	  cuadroDialogo("Capture el CONCEPTO ",3);
	}
	return res;
}

function validaConcepto2(obj)
{
	res = EsAlfa(obj.value,"CONCEPTO");
	if(res==false)
	{
		obj.value = "";
	}
}

function val_monto()
{

  var dato=document.transferencia.monto.value;

  var result=true;
  if  (trimString(dato)!="")
  {
    // Q1364 - Maria Elena de la Pe�a
    result=validAmount(dato);


    if (result==true)
	{

      if (dato.indexOf(".")>0)
        // Q1364 - Maria Elena de la Pe�a
	document.transferencia.monto.value=redondea(dato);





	  document.transferencia.monto.value=quitar_ceros_izquierdos(dato);

    }

  }
}

/***/
/*
function val_iva()
{

  var dato=document.transferencia.IVA.value;
  var result=true;
  if  (trimString(dato)!="")
  {
    result=validAmount(dato);
    if (result==true)
	{
      if (dato.indexOf(".")>0)
        // Q1364 - Maria Elena de la Pe�a
	document.transferencia.IVA.value=redondea(dato);
        document.transferencia.IVA.value=quitar_ceros_izquierdos(dato);
    }
  }
}
*/

function formateaImporte(valor){
        var cadena="";
        var cadenaDecimales="";
                num= parseFloat(valor);
                cadena=cadena+num;
                posicionPunto=cadena.indexOf('.');
                if(posicionPunto>=0){
                        cadenaDecimales=cadena.substring(posicionPunto+1);
                        if(cadenaDecimales.length==1)
                                cadena=cadena+"0";
                        else if(cadenaDecimales.length>2)
                                cadena=cadena.substring(0, posicionPunto+3);
                }
                else
                        cadena= cadena+".00";
        return cadena;
}

</SCRIPT>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript">

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>
<%
// Q1364 - Maria Elena de la Pe�a
%>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" onLoad=" EnfSelCta(); msgCifrado(); MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" >

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<%-- MENU PRINCIPAL --%>
	<%=request.getAttribute("MenuPrincipal")%></TD>
  </TR>
</TABLE>
<%=request.getAttribute("Encabezado")%>

<%-- CONTENIDO INICIO --%>
<%
// Q1364 - Maria Elena de la Pe�a
%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<%
if ( !tdc2chq.equals("1") ) {
%>

<FORM  NAME="transferencia" METHOD="POST"  ACTION="transferencia?ventana=1"
onSubmit="return validar();" onload= "return inicial();" > <%-- Cambio para comprobante fiscal --%>
<%
}else {
%>
<FORM  NAME="transferencia" METHOD="POST"  ACTION="transferencia?ventana=2" onSubmit="return validarDISP();" onload= "return inicial();"> <%-- Cambio para comprobante fiscal --%>
<%
}
%>
   <INPUT TYPE="HIDDEN" NAME="fecha_programada" VALUE=0>
   <INPUT TYPE="HIDDEN" NAME="montostring" VALUE=0>
   <INPUT TYPE="HIDDEN" NAME="fac_programadas1" VALUE=<%= request.getAttribute("fac_programadas1") %>>
   <INPUT TYPE="HIDDEN" NAME="divisa" VALUE=<%= request.getAttribute("divisa") %>>
   <INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%= request.getAttribute("trans") %>>
   <input type="hidden" name="mensajeCifrado" value="<%=request.getAttribute("mensajeCifrado")%>"/>
   <INPUT TYPE="HIDDEN" NAME="Favorito" id="Favorito">

<tr>
<%
// Q1364 - Maria Elena de la Pe�a
%>
<td align="center">
	<table width="490" border="0" cellspacing="2" cellpadding="3">
    <tr>
	<td class="tittabdat" colspan="2"> Capture los datos de su transferencia  </td>
    </tr>

    <tr>
	<td class="textabdatcla" valign="top" colspan="2"> <%@ include file="/jsp/filtroConsultaCuentasTarjetaPago.jspf" %>  </td>
    </tr>


    <tr align="center">
	<td class="textabdatcla" valign="top" colspan="2">
                <%
                // Q1364 - Maria Elena de la Pe�a
                %>
		<table width="480" border="0" cellspacing="0" cellpadding="0">
	   <tr valign="top">
	   <td width="270" >
		<table width="270" border="0" cellspacing="5" cellpadding="0">
		<tr>
                   <%
                   // Q1364 - Maria Elena de la Pe�a
                   %>
		   <%--<td class="tabmovtexbol" width="158" nowrap>Cuenta de cargo:</td>MSD Q1364--%>
		   <td class="tabmovtexbol" width="158" nowrap><%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de cargo:</td>
		</tr>
		<tr>
		    <td class="tabmovtexbol" width="158" nowrap>
				    <%-- Modificacion para integracion
					  <SELECT ID="FormsComboBox1" NAME="origen">
		      <%= request.getAttribute("cuentas_origen") %>
    			</SELECT>--%>
					 <%--<input type="text" name=textorigen  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					 <A HREF="javascript:CuentasCargo();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>--%>
					 <%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
					 <input type="hidden" name="origen" id="origen" value=""/>


		    </td>
		</tr>
		<tr>
                    <%
                    // Q1364 - Maria Elena de la Pe�a
                    %>
		    <%--<td class="tabmovtexbol" nowrap>Tarjeta Abono:</td>MSD Q1364--%>
		    <td class="tabmovtexbol" nowrap><%=tdc2chq.equals("1")?"Cuenta":"Tarjeta"%> abono:</td>
		</tr>
		<tr valign="middle">
		    <td class="tabmovtexbol" nowrap>
		     <input type="radio" name="ChkOnline" value="OnLine" checked/>Registrada


				    </td>
		</tr>
			<tr>
		    <td class="tabmovtexbol" nowrap>
					   <%-- Modificacion para integracion
		       <SELECT ID="FormsComboBox4" NAME="destino">
		       <%= request.getAttribute("cuentas_destino") %>
    			   </SELECT> --%>
		       <%--<input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					   <A HREF="javascript:CuentasAbono();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A> --%>
					   <input type="hidden" name="destino" id="destino" value=""/>

					    <%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
		       </td>
		 </tr>
		 <%=request.getAttribute("NoRegistradas")%>

		<%
		if ( !tdc2chq.equals("1") ) {
		%>

		 <tr valign="middle">
		    <td class="tabmovtexbol" nowrap>
                                        <%
                                        // Q1364 - Maria Elena de la Pe�a
                                        %>
					<input type = "radio" value = "OnFile" name = "ChkOnline" onClick="return verificartipo();"/>Importar
				    </td>
		</tr>
		<tr>
		    <td class="tabmovtexbol" nowrap>
                                         <%
                                         // Q1364 - Maria Elena de la Pe�a
                                         %>
					 <INPUT NAME="FileTrans" TYPE="file" size="20"/>
		    </td>
		</tr>
		<%
		}
		%>
		</table>
	      </td>
	      <td width="180" align="right">
			    <table width="165" border="0" cellspacing="5" cellpadding="0">
		<tr>
		    <td class="tabmovtexbol" nowrap>Importe:</td>
		</tr>
		<tr>
		    <td class="tabmovtexbol" nowrap>
                                      <%
                                      // Q1364 - Maria Elena de la Pe�a
                                      %>
				      <INPUT TYPE=TEXT NAME="monto" size="15" maxlength=15 class="tabmovtexbol" onBlur="val_monto();"/>
				</td>
		</tr>
	<%
	if ( !tdc2chq.equals("1") ) {
	%>
		<tr>
		    <td class="tabmovtexbol" nowrap>Concepto:</td>
		</tr>
		<tr>
		    <td class="tabmovtexbol" nowrap>
                      <%
                      // Q1364 - Maria Elena de la Pe�a
                      %>
		      <INPUT TYPE=TEXT NAME="concepto" size="22" maxlength=30 class="tabmovtexbol"/>
				    </td>
		</tr>
	<%
	}
	%>
		<tr>
		     <td class="tabmovtexbol" nowrap valign="middle">
					Fecha de aplicaci&oacute;n:
				     </td>
		</tr>
		<tr>
		     <td class="tabmovtexbol" nowrap valign="middle">
			<%=request.getAttribute("calendario_normal")%>
			<%=request.getAttribute("campos_fecha")%>
		     </td>
		</tr>


				<%--inicia Cambio para comprobante fiscal--%>
<%--	/***/ cambio por incidencia 397539 solicita que se elimine
				<tr>
				      <td class="tabmovtexbol" nowrap valign="middle">
					Requiere Estado de Cuenta Fiscal:
				     </td>
	       </tr>
		<tr>
				<td class="tabmovtexbol" nowrap valign="middle">
      	  			    <INPUT TYPE =RADIO	NAME=CHRCF VALUE=1 onclick="asignar_valor();">Si
				    <INPUT TYPE =RADIO	NAME=CHRCF VALUE=0 CHECKED  onClick="limpiar_cf();">No
 				     </td>
	       </tr>
	       <tr>

				    <td class="tabmovtexbol" nowrap>R.F.C. Beneficiario:
					<input type="TEXT" name="RFC"  size=15 maxlength=13 onFocus="checaestatus(this);">
					</td>
	       </tr>
			   <tr>
                            <%// Q1364 - Maria Elena de la Pe�a%>
 			    <td class="tabmovtexbol" nowrap>Importe I.V.A.: &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    <input type="TEXT" name="IVA" value="" size=13 maxlength=15 onFocus="checaestatus(this);"  onBlur="val_iva();"></td>
	       </tr>

                <!--termina Cambio para comprobante fiscal--%>
                <%
                // Q1364 - Maria Elena de la Pe�a
                %>
		<input type="hidden" name="CHRCF" value=0>

		</table>
		</td>
	</tr>
	</table>
	</td>
    </tr>
    </table>
        <%
        // Q1364 - Maria Elena de la Pe�a
        %>
<%
if ( !tdc2chq.equals("1") ) {
%>
	<table width="490" border="0" cellspacing="2" cellpadding="3">
    <tr>
	 <td class="textabref">D&eacute; click sobre el bot&oacute;n AGREGAR
	      para registrar su transferencia. Si desea realizar m&aacute;s de
	      30 operaciones deber&aacute; hacerlo con importaci&oacute;n de archivo,
	      (bot&oacute;n BROWSE).
	</td>
    </tr>
    </table>
<%
}
%>
	<br>
	<table border="0" cellspacing="0" cellpadding="0" style=" bgcolor: #FFFFFF;">
    <tr>
        <%
        // Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
        %>
<%
if ( !tdc2chq.equals("1") ) {
%>
	<td align="right" valign="top"	width="85">
		<input type="image" border="0"  name="Agregar" src="/gifs/EnlaceMig/gbo25290.gif"   width="89" height="22" alt="Agregar" >

	</td>
<%
}
else {
%>
        <td width="76">
		   <a href="javascript:confirmacion();">
    	   <img style=" border: 0;" src="/gifs/EnlaceMig/gbo25360.gif"/></a>
        </td>

<%
}
%>
	<td  valign="top"  width="76">
		   <a href="javascript:limpiar_datos();">
	   <img style=" border: 0;" src="/gifs/EnlaceMig/gbo25250.gif"/>
		   </a>
	</td>

        <%
        // Q1364 - Maria Elena de la Pe�a - fin modificaci�n
        %>
        <%-- INICIO FSW-INDRA PYME MARZO 2015 --%>
		<td valign="top" width="76">
			<a href='javascript:consultarFavoritos("PAGT","transferencia");' style=" border: 0;"><img src='/gifs/EnlaceMig/favoritos.png' alt="Consultar Favoritos" style="width: 115; border: 0;"></a>
		</td>
		<%-- FIN FSW-INDRA PYME MARZO 2015 --%>
    </tr>
    </table>
	<br>




<%-----------------------------------------------------------%>
<%-- CAMBIO PYME FSW - INDRA MARZO 2015 se agrega hidden esFav para el manejo de favortios --%>
<input type="hidden" name="esFav" id="esFav" value="<%=request.getAttribute("esFavorito")%>"/>
<INPUT TYPE="HIDDEN" NAME="arregloctas_origen1"   VALUE=<%= request.getAttribute("arregloctas_origen1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino1"  VALUE=<%= request.getAttribute("arregloctas_destino1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloimportes1"	  VALUE=<%= request.getAttribute("arregloimportes1") %>>
<INPUT TYPE="HIDDEN" NAME="arreglofechas1"	  VALUE=<%= request.getAttribute("arreglofechas1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos1"	  VALUE=<%= request.getAttribute("arregloconceptos1") %>>
<INPUT TYPE="HIDDEN" NAME="contador1"		  VALUE=<%= request.getAttribute("contador1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloestatus1"	  VALUE=<%= request.getAttribute("arregloestatus1") %>>

<INPUT TYPE="HIDDEN" NAME="fecha1" VALUE = <%= request.getAttribute("fecha1") %> >
<INPUT TYPE="HIDDEN" NAME="fecha2"  VALUE=<%= request.getAttribute("fecha2") %>>
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE=<%= request.getAttribute("TIPOTRANS") %>>
<INPUT TYPE="HIDDEN" NAME="INICIAL" VALUE=<%= request.getAttribute("INICIAL") %>>
<INPUT TYPE="HIDDEN" NAME="NMAXOPER" VALUE=<%= request.getAttribute("NMAXOPER") %>>
<%
// Q1364 - Maria Elena de la Pe�a - inicio modificaci�n
%>
<%
String ivas = "";
String rfcs = "";
if(request.getAttribute("arreglorfcs1")!=null&& !((String)request.getAttribute("arreglorfcs1")).trim().equals("null") )
rfcs= (String)request.getAttribute("arreglorfcs1");
if(request.getAttribute("arregloivas1")!=null&& !((String)request.getAttribute("arregloivas1")).trim().equals("null") )
ivas= (String)request.getAttribute("arregloivas1");
if(ivas==null||ivas.equals("null"))
	ivas="";
System.out.println("----------______________________IVA ="+ivas);
%>

<INPUT TYPE="HIDDEN" NAME="arreglorfcs1" VALUE=<%= rfcs%>>
<INPUT TYPE="HIDDEN" NAME="arregloivas1" VALUE=<%= ivas%>>


</FORM>

<FORM NAME="tabla_transferencias" METHOD="POST"  ACTION="transferencia?ventana=2" onSubmit="return validar2(this);">

<table width="750" border="0" cellspacing="2" cellpadding="3">
<tr>
     <td>&nbsp;</td>
     <td class="textabref" colspan="6">
<%
String contadoroperaciones= "";
String tabla="";
String botontransferir="";

contadoroperaciones=(String)request.getAttribute("contadoroperaciones");
tabla=(String)request.getAttribute("tabla");
botontransferir= (String)request.getAttribute("botontransferir");

if(contadoroperaciones==null)
	contadoroperaciones= "";

if(tabla==null)
	tabla= "";
if(botontransferir==null)
	botontransferir= "";
%>
	 <%=contadoroperaciones%>
	 </td>
</tr>
	<%=tabla%>
</table>
<br>
<table border="0" cellspacing="0" cellpadding="0" style=" bgcolor: #FFFFFF;">
<tr>
     <%=botontransferir%>
</tr>
</table>
<br>
</td>
</tr>
<%-- CAMBIO PYME FSW - INDRA MARZO 2015 se agrega hidden strFav para el manejo de favortios --%>
<input type="hidden" name="strFav" id="strFav" value=""/>
<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"   VALUE=<%= request.getAttribute("arregloctas_origen2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2"  VALUE=<%= request.getAttribute("arregloctas_destino2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloimportes2"	  VALUE=<%= request.getAttribute("arregloimportes2") %>>
<INPUT TYPE="HIDDEN" NAME="arreglofechas2"	  VALUE=<%= request.getAttribute("arreglofechas2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"	  VALUE=<%= request.getAttribute("arregloconceptos2") %>>
<INPUT TYPE="HIDDEN" NAME="contador2"		  VALUE=<%= request.getAttribute("contador2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloestatus2"	  VALUE=<%= request.getAttribute("arregloestatus2") %>>
<INPUT TYPE="HIDDEN" NAME="miSalida" VALUE=<%= request.getAttribute("miSalida") %>>
<INPUT TYPE="HIDDEN" NAME="imptabla"	   VALUE=<%=request.getAttribute("imptabla")==null?"SI":request.getAttribute("imptabla")%>>
<INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%= request.getAttribute("trans") %>>
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE=<%= request.getAttribute("TIPOTRANS") %>>
<INPUT TYPE="HIDDEN" NAME="datos2" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datoscta" VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="datosimp" VALUE=""/>
<%
String ivas2_ = "";
String rfcs2_ = "";
if(request.getAttribute("arregloivas2")!=null)
	ivas2_= (String)request.getAttribute("arregloivas2");
if(request.getAttribute("arreglorfcs2")!=null)
	rfcs2_=(String)request.getAttribute("arreglorfcs2");
%>

<INPUT TYPE="HIDDEN" NAME="arreglorfcs2" VALUE=<%= rfcs2_%>>
<INPUT TYPE="HIDDEN" NAME="arregloivas2" VALUE=<%= ivas2_ %>>
<INPUT TYPE="HIDDEN" NAME="TDC2CHQ" VALUE="<%= request.getAttribute("TDC2CHQ") %>"/>
<%
System.out.println("En la pagina");
System.out.println("En la pagina");
System.out.println(request.getAttribute("arreglorfcs1"));
System.out.println(request.getAttribute("arregloivas1"));
System.out.println("En la pagina");
System.out.println("En la pagina");
System.out.println(request.getAttribute("arreglorfcs2"));
System.out.println(request.getAttribute("arregloivas2"));
%>

</FORM>
</table>
<%
// Q1364 - Maria Elena de la Pe�a - fin modificaci�n
%>
	<%-- CONTENIDO FINAL --%>
</body>
</html>

<Script type="text/javascript">
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
</Script>
<%-- 5.5 --%>

<%
}else {
%>
<script type="text/javascript">
<%-- *********************************************** --%>
<%-- modificaci�n para integraci�n pva 07/03/2002    --%>
<%-- *********************************************** --%>
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var opcion;
var tramadicional;
var cfm;

<%
	String tdc2chq = (String)request.getAttribute("TDC2CHQ");
	tdc2chq = (tdc2chq==null?"":tdc2chq);
%>
<%
	HttpSession     sess = request.getSession();
    mx.altec.enlace.bo.BaseResource session2 = (mx.altec.enlace.bo.BaseResource) sess.getAttribute("session");
	if( tdc2chq.equals("1") )
	{
		session2.setModuloConsultar("43@");
	}
%>

function CuentasCargo()
{
   opcion=1;
   //PresentarCuentas();
	msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","<%=(tdc2chq.equals("1"))?"Tarjetas":"Cuentas"%>","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de <%=(tdc2chq.equals("1"))?"Tarjetas":"Cuentas"%>'");

    msg.focus();
}

function CuentasAbono()
{
   opcion=3;
   //PresentarCuentas();
   msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=3","<%=(tdc2chq.equals("1"))?"Cuentas":"Tarjetas"%>","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290, title='Consulta de <%=(tdc2chq.equals("1"))?"Cuentas":"Tarjetas"%>'");

  msg.focus();

}
/*
function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
*/
function actualizacuenta()
{
  if(opcion==1)
  {
	  document.transferencia.origen.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
      //document.transferencia.textorigen.value=ctaselec+" "+ctadescr;
  }
  else
  {
  	  document.transferencia.destino.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
      //document.transferencia.textdestino.value=ctaselec+" "+ctadescr;
  }
}


function isDigit (c)
{
  document.transferencia.monto.value="";
  cuadroDialogo("Capture el IMPORTE a transferir",3);
 // document.transferencia.monto.focus();

  return ((c >= "0") && (c <= "9"));

}

function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

function validAmount (cantidad,band)// Cambio para comprobante fiscal
{

  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad
  var miles="";


  if (cantidadAux == "" || cantidadAux <= 0)
  {
    document.transferencia.monto.value="";
    cuadroDialogo("\n Capture el "+band+"  a transferir ",3);// Cambio para comprobante fiscal
    //document.transferencia.monto.focus();
    return false;
  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  document.transferencia.monto.value="";
      cuadroDialogo("Favor de ingresar un valor num&eacute;rico en el "+band,3) //    **** Cambio para comprobante fiscal
      //document.transferencia.monto.focus()

      return false
    }

	formato = /^[0-9]{0,8}(\.[0-9]{0,2}){0,1}$/;
	if (!formato.test(cantidad))
	{
		document.transferencia.monto.value="";
		cuadroDialogo("\n Solamente se permite capturar <br>hasta 8 digitos enteros y dos decimales",3);// Cambio para comprobante fiscal
		return false;
	}

    pos_punto = cantidadAux.indexOf (".")

    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
        strAux2 = miles
      if (cientos != "")
        strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      transf = document.transferencia.monto.value
    }
    document.transferencia.montostring.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
	 document.transferencia.montostring.value = strAux2;
    return true;
  }
}

function Formatea_Importe2 (cantidad)
{
  strAux1 = ""
  strAux2 = ""
  entero = ""
  cantidadAux = cantidad


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

    if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

   strAux1=Formatea_Importe(strAux1)
  }
  return strAux1;

}



function Valida_Date (dia, mes, anio)
{
  result = true;
  document.transferencia.fecha1.value=document.transferencia.fecha_completa.value;
  return result;
}

function validctasdif (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus()
    return false
  }
  return result;
}

function validctasdifNoReg (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.transferencia.monto.focus()
    return false
  }
  return result;
}

function definir_valores_checkbox()
{
document.transferencia.arregloestatus2.value="1@";

 /* modificacion para integracion
document.transferencia.arregloctas_origen2.value=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value+"@";
if (document.transferencia.banderaNoReg.value==0)
  document.transferencia.arregloctas_destino2.value=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value+"@";
else
{
  if (document.transferencia.ChkOnline[0].checked)
  document.transferencia.arregloctas_destino2.value=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value+"@";
  else
    document.transferencia.arregloctas_destino2.value=document.transferencia.destinoNoReg.value+"@";
  }*/
  var cuentaorigen=document.transferencia.origen.value;
  var cuentadestino=document.transferencia.destino.value;
  document.transferencia.arregloctas_origen2.value=cuentaorigen+"@";
  if (document.transferencia.banderaNoReg.value==0)
    document.transferencia.arregloctas_destino2.value=cuentadestino+"@";
  else
  {
    if (document.transferencia.ChkOnline[0].checked)
       document.transferencia.arregloctas_destino2.value=cuentadestino+"@";
    else
       document.transferencia.arregloctas_destino2.value=document.transferencia.destinoNoReg.value+"@";
  }



  document.transferencia.arregloimportes2.value=document.transferencia.monto.value+"@";
  document.transferencia.arreglofechas2.value=document.transferencia.fecha_completa.value+"@";
  document.transferencia.arregloconceptos2.value=document.transferencia.concepto.value+"@";
  document.transferencia.arreglorfcs2.value=""+"@";//Cambio para comprobante fiscal
  document.transferencia.arregloivas2.value=""+"@";//Cambio para comprobante fiscal
/***/
/*
  document.transferencia.arreglorfcs2.value=document.transferencia.RFC.value+"@";//Cambio para comprobante fiscal
  document.transferencia.arregloivas2.value=document.transferencia.IVA.value+"@";//Cambio para comprobante fiscal
*/

  document.transferencia.contador2.value="1";
}



function EsAlfa(cadena,band)//Cambio para comprobante fiscal
{

  for (var i=0;i<cadena.length;i++)
  {
      if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9')))
      {
          cuadroDialogo("No se permiten caracteres especiales en "+band,3);//    *****Cambio para comprobante fiscal
          return false;
      }

  }
  return true;
}

function validar_tipo_cuentas()
{
   /* modificacion para integracion
	   var cta_origenv=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;
   var cta_destinv=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value;*/

   var cta_origenv=document.transferencia.origen.value;
   var cta_destinv=document.transferencia.destino.value;

   if ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83"))
   {
     if ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83"))
        return true;
     else
     {
        cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);
        return false;
     }
   }
   else if ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83"))
   {
     if ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83"))
        return true;
     else
     {
        cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);
        return false;
     }
   }
   else
     return true;
}
/*-------------------------------------------------------------------*/
function EsNumero(Dato){
    var retorno=false;
	var CadenaNumeros= "0123456789";
	var EsteCaracter;
	var Contador = 0;
	for (var i=0; i < Dato.length; i++) {
		EsteCaracter = Dato.substring(i , i+1);
		if (CadenaNumeros.indexOf(EsteCaracter) != -1)
			Contador ++;
	}
	if (Contador == Dato.length)
	 {
		//Todos los caracteres son numeros!
        //alert("!Esta bien ! es un numero.");
	    retorno=LongitudCorrecta(document.transferencia.destinoNoReg.value)
 	 }
	else
	 {
		cuadroDialogo("No es un n&uacute;mero de cuenta v&aacute;lido",3);
		//document.transferencia.monto.focus();
	 }

	return retorno;
}

/*function validaDestinoNoReg(Dato){
    var retorno=false;
	EsNumero(document.transferencia.destinoNoReg.value);
	retono = true ;
	}
return retorno;
}*/

function LongitudCorrecta(Dato){
    var retorno=false;
	if (Dato.length == 11 || Dato.length == 16){
		//CuentasDiferentesNoReg(document.transferencia.destinoNoReg.value);
		retorno = true;
	} else {
		cuadroDialogo("No es un n&uacute;mero de tarjeta  v&aacute;lido.\n",3);
		//document.transferencia.monto.focus();
		}
return retorno;
}

function CuentasDiferentesNoReg(Dato){
//var origen1 = "";
var retorno=false;
/*modificacion para integracion
origen1=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value */
origen1=document.transferencia.origen.value;
    //origen1 = String(document.transferencia.origenNoReg.value);
var cuentaOrigen = origen1.substring(0, origen1.indexOf("|"));
//alert(cuentaOrigen);
if (cuentaOrigen == Dato ){
	cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3)
	//document.transferencia.monto.focus();
	}
else {
	retorno = true;
	}
return retorno;
}
/*----------------------------------------------------*/
function validar_tipo_cuentasNoReg()
{
   /*  modificacion para integracion
	   var cta_origenv=document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value;*/
   var cta_origenv=document.transferencia.origen.value;
   var cta_destinv=document.transferencia.destinoNoReg.value;

   if ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83"))
   {
     if ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83"))
        return true;
     else
     {
       cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);

        return false;
     }
   }
   else if ((cta_destinv.substring(0,2)=="82") || (cta_destinv.substring(0,2)=="83"))
   {
     if ((cta_origenv.substring(0,2)=="82") || (cta_origenv.substring(0,2)=="83"))
        return true;
     else
     {
        cuadroDialogo("No se permite hacer transferencias entre cuentas de divisas diferentes",3);
        return false;
     }
   }
   else
     return true;
}
/*----------------------------------------------------*/

 js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';

var dia;
var mes;
var anio;
var fecha_completa;


function WindowCalendar()
{
    var m=new Date();
    n=m.getMonth();
    n=document.transferencia.mes.value-1;
    dia=document.transferencia.dia.value;
	mes=document.transferencia.mes.value;
	anio=document.transferencia.anio.value;

    msg=window.open("/EnlaceMig/calfut2.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}

function Actualiza()
{
   document.transferencia.fecha_completa.value=fecha_completa;
}


/*function WindowCalendar1()
{
    var m=new Date();
    n=m.getMonth();
    n=document.transferencia.mes.value-1;

    msg=window.open("/EnlaceMig/caltrans2.htm#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();
}*/

function obtenmensaje()
{
    var cuenta="";
	var des="";
	var mensaje="";
    if(eval(document.transferencia.banderaNoReg.value)==0)
	{
       /*  modificacion para integracion
		   cuenta=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value; */
	   cuenta = document.transferencia.destino.value;
	   des=cuenta;
       cuenta=cuenta.substring(0,cuenta.indexOf("|"));
       des=des.substring(des.indexOf("|")+1,des.length);
       des=des.substring(des.indexOf("|")+1,des.length-1);
       mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +"  de "+des+" la cantidad de $"+Formatea_Importe2 (document.transferencia.monto.value) +"?";
    }
	else
	{
	   if  (document.transferencia.ChkOnline[0].checked)
       {

		  /*  modificacion para integracion cuenta=document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value; */
	      cuenta = document.transferencia.destino.value;
		  des=cuenta;
          cuenta=cuenta.substring(0,cuenta.indexOf("|"));
          des=des.substring(des.indexOf("|")+1,des.length);
          des=des.substring(des.indexOf("|")+1,des.length-1);
          mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +"  de "+des+" la cantidad de $"+Formatea_Importe2 (document.transferencia.monto.value) +"?";
	   }
	   else
	   {
	     cuenta=document.transferencia.destinoNoReg.value;
	     mensaje= "Desea <%=tdc2chq.equals("1")?"Abonar a la cuenta":"pagar la tarjeta"%>:"+cuenta +" la cantidad de $ "+Formatea_Importe2 (document.transferencia.monto.value) +"?";
       }
    }
    return mensaje;
}

// FSW INDRA
// Marzo 2015
// Enlace PYMES

function obtenerCuentaSeleccionada(combo, elemento) {
	var select = document.getElementById(combo);
	if (!(select === undefined)) {
		var valor = select.options[select.selectedIndex].value;

		if(valor !== null
	   	&& "" !==valor){
			document.getElementById(elemento).value = obtenerCuenta(valor);
		}
	}
}

function obtenerCuenta(valor){
    var res = valor.split("\|");
     if (res[0]=="") {
      return "";
    }
    if(res.length>1){
	    return res[0] + "|" + res[1] + "|"  + res[2] + "|";
    }
}
// FSW INDRA
// Marzo 2015
// Enlace PYMES

function validar()
{
var result=true;
/*
var cadenaOut  = new String ("");
cadenaOut=document.transferencia.RFC.value;
document.transferencia.RFC.value = cadenaOut.toUpperCase();
*/
  //alert("entra a validar");

  // FSW INDRA
	// Marzo 2015
	// Enlace PYMES

	obtenerCuentaSeleccionada("comboCuenta0", "origen");

	obtenerCuentaSeleccionada("comboCuenta1", "destino");


	// FSW INDRA
	// Marzo 2015
	// Enlace PYMES


  var tipo=0

  if (eval(document.transferencia.banderaNoReg.value)==0)
     tipo=0;
  else
  {
     if(document.transferencia.ChkOnline[0].checked)
	   tipo=0;
     else
	   tipo=1;
  }

  if(tipo==0)
  {

      /*  modificacion para integracion
		  result = validctasdif (document.transferencia.origen.options[document.transferencia.origen.selectedIndex].value,document.transferencia.destino.options[document.transferencia.destino.selectedIndex].value)  */

	  if(document.transferencia.origen.value.length==0)
      {
		cuadroDialogo("Se debe especificar la <%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de Cargo",3);
        return false;
      }
	  if ((result==true)&&(document.transferencia.destino.value.length==0))
      {
		cuadroDialogo("Debe indicar la <%=tdc2chq.equals("1")?"Cuenta de Abono":"Tarjeta de cr&eacute;dito"%>",3);
		return false;
      }
	  if (result==true)
	    result = validctasdif (document.transferencia.origen.value,document.transferencia.destino.value)
       if (result == true)
      {
        result = Valida_Date (document.transferencia.dia.value,document.transferencia.mes.value,document.transferencia.anio.value);
      }
      if (result == true)
      {
        result=validar_tipo_cuentas();

      }
/***/

      if (result == true)
      {
        result = validAmount (document.transferencia.monto.value,"IMPORTE"); //cambio para comprobante fiscal
      }
/*     if (result == true)
      {
        //result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");//Cambio para comprobante fiscal
		result = validaConcepto(document.transferencia.concepto);
      }*/

      if (result == true)
	  {
        definir_valores_checkbox();
      }

//    ****Inicia cambio para comprobante fiscal
/***/
/*
     if(result==true)
	 {
	   if (document.transferencia.CHRCF[0].checked)
       {
         if (eval(document.transferencia.RFC.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
           return false;
	     }
         if (eval(document.transferencia.IVA.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el IVA",3);
           return false;
	     }
 	     result=EsAlfa(document.transferencia.RFC.value,"RFC");
         if (result==true)
          result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
         if(!formato_rfc_ok(document.transferencia.RFC))
		 {
	           cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		 }

       }
     }
*/
     //    ****termina cambio para comprobante fiscal

    return result;
  } //fin del if de ChkOnline ==0
	  if(tipo==1){
//     document.transferencia.FileTrans.value = "";
//		alert("Llegaa hasta la segunda opcion");
		if (document.transferencia.destinoNoReg.value ==""){
			cuadroDialogo("Debe indicar la <%=tdc2chq.equals("1")?"Cuenta de Abono":"Tarjeta de cr&eacute;dito"%>",3);
	  	    result = false;
		}
		else
		{

		  if(document.transferencia.origen.value.length==0)
          {
            result=false;
		    cuadroDialogo("Se debe especificar la <%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de Cargo",3);
          }

	  	  if(result==true)
		    result = EsNumero(document.transferencia.destinoNoReg.value);
  	      if (result == true)
		  {
		    /*  modificacion para integracion
				origen1 = document.transferencia.origen.options[ document.transferencia.origen.selectedIndex].value */
            origen1 = document.transferencia.origen.value;
            cuentaOrigen = origen1.substring(0, origen1.indexOf("|"))
 		    result = validctasdifNoReg(cuentaOrigen,document.transferencia.destinoNoReg.value)
		  }
  	      if (result == true)
		  {
	        result = Valida_Date (document.transferencia.dia.value,document.transferencia.mes.value,document.transferencia.anio.value);
	      }
		  if (result == true)
	      {
		    result=validar_tipo_cuentasNoReg();
//			alert("Llega hasta validar tipo de cuenta no reg")
	      }
	      if (result == true)
	      {
	        result = validAmount (document.transferencia.monto.value,"IMPORTE");
	      }
/*	     if (result == true)
		  {
	        //result=EsAlfa(document.transferencia.concepto.value,"CONCEPTO");
			result=validaConcepto(document.transferencia.concepto);
	      } */
	      if (result == true)
		  {
            definir_valores_checkbox();
	      }
		} //fin de else
	 } //fin del if de ChkOnline ==2

     //    ****Inicia cambio para comprobante fiscal
	 /***/
	 /*
     if(result==true)
	 {
	   if (document.transferencia.CHRCF[0].checked)
       {
         if (eval(document.transferencia.RFC.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el RFC del Beneficiario",3);
           return false;
	     }
         if (eval(document.transferencia.IVA.value.length)==eval(0))
	     {
           cuadroDialogo("Se debe especificar el IVA",3);
           return false;
	     }
 	     result=EsAlfa(document.transferencia.RFC.value,"RFC");
         if (result==true)
          result = validAmount (document.transferencia.IVA.value,"IMPORTE IVA");
         if(!formato_rfc_ok(document.transferencia.RFC))
		 {
	           cuadroDialogo("el RFC no es v&aacute;lido.",1);
			   return false;
		 }

       }
     }
	 */
     //    ****termina cambio para comprobante fiscal

    return result;
}


function formato_rfc_ok(rfc)
 {
   var fecha=6;

   var formato="";

   switch(rfc.value.length)
         {
           case 13: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
           case 10: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
           case 12: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/; break;
           case 9: formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/; break;
    }
   if(formato=="")
     return false;
   if(rfc.value.length==9 || rfc.value.length==12)
     fecha=5;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(fecha,fecha+2);
   var dia=rfc.value.substring(fecha+2,fecha+4);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
    }
   else
     return false;
   return true;
 }

/*function formato_rfc_ok(rfc)
 {
   if(rfc.value.length>11)
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9][0-9,A-Z,a-z][0-9,A-Z,a-z][0-9,A-Z,a-z]$/;
   else
     formato=/^[A-Z,a-z][A-Z,a-z][A-Z,a-z][A-Z,a-z]{0,1}[0-9][0-9][0-9][0-9][0-9][0-9]$/;
   if(!formato.test(rfc.value))
     return false;

   var mes=rfc.value.substring(6,8);
   var dia=rfc.value.substring(8,10);
   if( (dia >0 && dia<=31) && (mes >0 && mes<=12) )
    {
      if(mes==2 && dia >=30)
       return false;
	 }
	else
	 return false;
   return true;
 }
*/
var gConfirmacion= false;
function confirmacion()
{

  result=validar();
  gConfirmacion = result;
  if ( result == true )
  {<%
	BaseResource ses = (BaseResource) request.getSession ().getAttribute ("session");
	if (ses.getToken().getStatus() == 1) {%>
  		respuesta = 1;
  		continua();
  	<%} else {%>
	  	mensaje=obtenmensaje();
      	cuadroDialogo(mensaje,2);
  	<%}%>

      //mensaje=obtenmensaje();
      //cuadroDialogo(mensaje,2);
   }
}

function continua()
{
   if (gConfirmacion == true )
   {
      if (respuesta==1) document.transferencia.submit();
        else return false;

   }
   else
       return false;
}




function limpiar_datos()
{
	limpiarBusqueda();
    //modificacion para integracion document.transferencia.origen.selectedIndex=0;
	document.transferencia.origen.value="";
	//document.transferencia.textorigen.value="";
    document.transferencia.concepto.value="";
	document.transferencia.monto.value="";
    document.transferencia.destino.value="";
	//document.transferencia.textdestino.value="";

    if (eval(document.transferencia.banderaNoReg.value)==1)
	{
		//modificacion para integracion document.transferencia.destino.selectedIndex=0;
       document.transferencia.destinoNoReg.value="";
    }

	document.transferencia.fecha_completa.value=document.transferencia.fecha1.value;
	/***///limpiar_cf();
}

function verificartipo()
{
    var respuesta=true;
    if (eval(document.transferencia.contador1.value)>0)
	{
	   respuesta=false;
	   cuadroDialogo("S&oacute;lo se pueden agregar transferencias en l&iacute;nea",3);
	   document.transferencia.ChkOnline[0].checked=true;
    }
    return respuesta;
}

//    ****inicia Cambio para comprobante fiscal

/***/
/*
function checaestatus(campo)
{
  var result=true;

  if (eval(document.transferencia.CHRCF.value)==1)
  {
    result=true;
  }
  else
  {

	document.transferencia.RFC.value="";
	document.transferencia.IVA.value="";
    campo.blur();
	result=false;
  }
  return result;
}
*/

/***/
/*

function inicial()
{
  document.transferencia.CHRCF[1].checked=true;
  document.transferencia.CHRCF.value=0;
  document.transferencia.RFC.focus();
  document.transferencia.IVA.focus();
  return true;
}*/

/***/
/*
function limpiar_cf()
{
  document.transferencia.RFC.value="";
  document.transferencia.IVA.value="";
  document.transferencia.CHRCF.value=0;
  document.transferencia.CHRCF[1].checked=true;
  return true;
}*/

/***/
/*function asignar_valor()
{
  document.transferencia.CHRCF.value=1;
}
*/

function trimString (str)
{
    str = this != window? this : str;
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function redondea(num)
{
  var a=0;
  var b=0;

  a=parseInt(Math.round(num*100));
  b=a/100;
  return b;
}

function quitar_ceros_izquierdos(dato)
{
	var EsteCaracter;
	var contador = 0;
	var sinceros ="";
	for (var i=0; i < dato.length; i++) {
		EsteCaracter = dato.substring(i , i+1);
		if (EsteCaracter=="0")
			contador ++;
		else
			break;
	}
	sinceros=dato.substring(contador,dato.length);
	return sinceros;
}

function validaConcepto(obj)
{
	res = false;
	if(trimString(obj.value)!="")
	{
		res = EsAlfa(obj.value,"CONCEPTO");
	}
	else
	{
	  cuadroDialogo("Capture el CONCEPTO ",3);
	}
	return res;
}

function validaConcepto2(obj)
{
	res = EsAlfa(obj.value,"CONCEPTO");
	if(res==false)
	{
		obj.value = "";
	}
}

function val_monto()
{

  var dato=document.transferencia.monto.value;

  var result=true;
  if  (trimString(dato)!="")
  {

    result=validAmount(dato,"IMPORTE");

    if (result==true)
	{

      if (dato.indexOf(".")>0)
		{

			dato=formateaImporte(dato);

		}

	  document.transferencia.monto.value=quitar_ceros_izquierdos(dato);

    }

  }
}

/***/
/*
function val_iva()
{

  var dato=document.transferencia.IVA.value;
  var result=true;
  if  (trimString(dato)!="")
  {
    result=validAmount(dato);
    if (result==true)
	{
      if (dato.indexOf(".")>0)
      //document.transferencia.IVA.value=redondea(dato);
	  dato=formateaImporte(dato);
	  document.transferencia.IVA.value=quitar_ceros_izquierdos(dato);
    }
  }
}
*/

function formateaImporte(valor){
        var cadena="";
        var cadenaDecimales="";
                num= parseFloat(valor);
                cadena=cadena+num;
                posicionPunto=cadena.indexOf('.');
                if(posicionPunto>=0){
                        cadenaDecimales=cadena.substring(posicionPunto+1);
                        if(cadenaDecimales.length==1)
                                cadena=cadena+"0";
                        else if(cadenaDecimales.length>2)
                                cadena=cadena.substring(0, posicionPunto+3);
                }
                else
                        cadena= cadena+".00";
        return cadena;
}


</SCRIPT>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript">

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" onLoad="consultaMultiplesCuentas('0,2,1|1,2,3');MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" >

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<%-- MENU PRINCIPAL --%>
	<%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado") %>


<%--  /***/  <FORM  NAME="transferencia" METHOD=POST  ACTION="transferencia?ventana=2" onSubmit="return validar();" onload="return inicial();"> --%>
   <FORM  NAME="transferencia" METHOD="POST"  ACTION="transferencia?ventana=2" onSubmit="return validar();">
   <INPUT TYPE="HIDDEN" NAME="nombreFlujo" VALUE="tarjetaDisposicion"/>
   <INPUT TYPE="HIDDEN" NAME="montostring" VALUE=0>
   <INPUT TYPE="HIDDEN" NAME="fac_programadas1" VALUE=<%= request.getAttribute("fac_programadas1") %>>
   <INPUT TYPE="HIDDEN" NAME="divisa" VALUE=<%= request.getAttribute("divisa") %>>
   <INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%= request.getAttribute("trans") %>>


<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align=center>
	<table width="460" border="0" cellspacing="2" cellpadding="3">
    <tr>
        <td class="tittabdat" colspan="2"> Capture los datos de su transferencia  </td>
    </tr>
     <tr>
		<td class="textabdatcla" valign="top" colspan="2"> <%@ include file="/jsp/filtroConsultaCuentasTarjetaDisp.jspf" %>  </td>
    </tr>
    <tr align="center">
        <td class="textabdatcla" valign="top" colspan="2">
		   <table width="450" border="0" cellspacing="0" cellpadding="0">
           <tr valign="top">
              <td width="270" align="right">
                <table width="270" border="0" cellspacing="5" cellpadding="0">
                <tr>
                   <td class="tabmovtexbol" width="158" nowrap><%=tdc2chq.equals("1")?"Tarjeta":"Cuenta"%> de cargo:</td>
                </tr>
                <tr>
                    <td class="tabmovtexbol" width="158" nowrap>
                      <%-- Modificacion para integracion
					  <SELECT ID="FormsComboBox1" NAME="origen">
                      <%= request.getAttribute("cuentas_origen") %>
    		          </SELECT> --%>
					<%--<input type="text" name=textorigen  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					 <A HREF="javascript:CuentasCargo();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>--%>
					 <%@ include file="/jsp/listaCuentasTransferencias.jspf" %>
					 <input type="hidden" name="origen" id="origen" value=""/>


                    </td>
                </tr>
                <tr>
                    <td class="tabmovtexbol" nowrap><%=tdc2chq.equals("1")?"Cuenta":"Tarjeta"%> abono:</td>
                </tr>
                <tr valign="middle">
                    <td class="tabmovtexbol" nowrap>
                     <input type="radio" name="ChkOnline" value="OnLine" checked/>Registrada

				    </td>
                </tr>
		        <tr>
                    <td class="tabmovtexbol" nowrap>
					   <%-- Modificacion para integracion
                       <SELECT ID="FormsComboBox4" NAME="destino">
                       <%= request.getAttribute("cuentas_destino") %>
    		           </SELECT> --%>
                         <%--<input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					   <A HREF="javascript:CuentasAbono();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A> --%>
					   <input type="hidden" name="destino" id="destino" value=""/>

					   <%@ include file="/jsp/listaCuentasTransferencias.jspf" %>

                       </td>
                 </tr>
				 <% if( !tdc2chq.equals("1") ){ %>
                 <%= request.getAttribute("NoRegistradas") %>
				 <% }else{ %>
					<INPUT TYPE="HIDDEN" NAME="banderaNoReg" VALUE=0>
				 <% } %>
                 <tr valign="middle">
                    <td class="tabmovtexbol" nowrap>
				    </td>
                </tr>
                <tr>
                    <td class="tabmovtexbol" nowrap>
                    </td>
                </tr>
                </table>
              </td>
              <td width="180" align="right">
			    <table width="165" border="0" cellspacing="5" cellpadding="0">
                <tr>
                    <td class="tabmovtexbol" nowrap>Importe:</td>
                </tr>
                <tr>
                    <td class="tabmovtexbol" nowrap>
				      <INPUT TYPE="TEXT" NAME="monto" size="15" maxlength="11" class="tabmovtexbol" onBlur="val_monto();">
			        </td>
                </tr>
				<% if ( !tdc2chq.equals("1") ) {%>
					<tr>
						<td class="tabmovtexbol" nowrap>Concepto:</td>
					</tr>
					<tr>
						<td class="tabmovtexbol" nowrap>
						  <INPUT TYPE=TEXT NAME="concepto" size="22" maxlength="40" class="tabmovtexbol" onBlur="validaConcepto2(this);"/>
						</td>
					</tr>
				<% } else {%>
					<tr>
						<td>
							<INPUT TYPE="hidden" NAME="concepto" value=""/>
						</td>
					</tr>
				<% } %>
                <tr>
                     <td class="tabmovtexbol" nowrap valign="middle">
				        Fecha de aplicaci&oacute;n:
				     </td>
                </tr>
                <tr>
                     <td class="tabmovtexbol" nowrap valign="middle">
                        <%= request.getAttribute("calendario_normal") %>
                        <%= request.getAttribute("campos_fecha") %>
                     </td>
                </tr>


				<!--inicia Cambio para comprobante fiscal-->
<%--	/***/ cambio por incidencia 397539 solicita que se elimine
				<tr>
				      <td class="tabmovtexbol" nowrap valign="middle">
				        Requiere Estado de Cuenta Fiscal:
				     </td>
               </tr>
                <tr>
			        <td class="tabmovtexbol" nowrap valign="middle">
      	  			    <INPUT TYPE =RADIO  NAME=CHRCF VALUE=1 onclick="asignar_valor();">Si
				    <INPUT TYPE =RADIO  NAME=CHRCF VALUE=0 CHECKED  onClick="limpiar_cf();">No
 				     </td>
               </tr>
               <tr>

				    <td class="tabmovtexbol" nowrap>R.F.C. Beneficiario:
					<input type="TEXT" name="RFC"  size=15 maxlength=13 onFocus="checaestatus(this);">
					</td>
               </tr>
			   <tr>

				    <td class="tabmovtexbol" nowrap>Importe I.V.A.: &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="TEXT" name="IVA" size=15 maxlength=15 onFocus="checaestatus(this);"  onBlur="val_iva();" >
					</td>
               </tr>
--%>

			   <%--termina Cambio para comprobante fiscal--%>


              </table>
        	</td>
        </tr>
        </table>
        </td>
    </tr>
    </table>
	<br>
	<table border="0" cellspacing="0" cellpadding="0" style=" bgcolor: #FFFFFF;">
    <tr>
        <td width="76">
		   <a href="javascript:confirmacion();">
    	   <img border="0" src="/gifs/EnlaceMig/gbo25360.gif"/></a>
        </td>
        <td width="89">
		   <a href="javascript:limpiar_datos();">
           <img border="0" src="/gifs/EnlaceMig/gbo25250.gif"/></a>
        </td>

    </tr>
    </table>
	<br>
    </td>
    </tr>
</table>




<INPUT TYPE="HIDDEN" NAME="arregloctas_origen1"   VALUE=<%= request.getAttribute("arregloctas_origen1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino1"  VALUE=<%= request.getAttribute("arregloctas_destino1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloimportes1"      VALUE=<%= request.getAttribute("arregloimportes1") %>>
<INPUT TYPE="HIDDEN" NAME="arreglofechas1"        VALUE=<%= request.getAttribute("arreglofechas1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos1"     VALUE=<%= request.getAttribute("arregloconceptos1") %>>
<INPUT TYPE="HIDDEN" NAME="arreglorfcs1"     VALUE=<%= request.getAttribute("arreglorfcs1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloivas1"     VALUE=<%= request.getAttribute("arregloivas1") %>>
<INPUT TYPE="HIDDEN" NAME="contador1"             VALUE=<%= request.getAttribute("contador1") %>>
<INPUT TYPE="HIDDEN" NAME="arregloestatus1"       VALUE=<%= request.getAttribute("arregloestatus1") %>>

<INPUT TYPE="HIDDEN" NAME="fecha1" VALUE = <%= request.getAttribute("fecha1") %> >
<INPUT TYPE="HIDDEN" NAME="fecha2"  VALUE=<%= request.getAttribute("fecha2") %>>
<INPUT TYPE="HIDDEN" NAME="TIPOTRANS" VALUE=<%= request.getAttribute("TIPOTRANS") %>>
<INPUT TYPE="HIDDEN" NAME="INICIAL" VALUE=<%= request.getAttribute("INICIAL") %>>
<INPUT TYPE="HIDDEN" NAME="NMAXOPER" VALUE=<%= request.getAttribute("NMAXOPER") %>>

<INPUT TYPE="HIDDEN" NAME="TDC2CHQ" VALUE="<%= request.getAttribute("TDC2CHQ") %>"/>

<INPUT TYPE="HIDDEN" NAME="arregloctas_origen2"   VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="arregloctas_destino2"  VALUE=""/>
<INPUT TYPE="HIDDEN" NAME="arregloimportes2"      VALUE=<%= request.getAttribute("arregloimportes2") %>>
<INPUT TYPE="HIDDEN" NAME="arreglofechas2"        VALUE=<%= request.getAttribute("arreglofechas2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloconceptos2"     VALUE=<%= request.getAttribute("arregloconceptos2") %>>
<%--inicia Cambio para comprobante fiscal--%>
<INPUT TYPE="HIDDEN" NAME="arreglorfcs2"     VALUE=<%= request.getAttribute("arreglorfcs2") %>>
<INPUT TYPE="HIDDEN" NAME="arregloivas2"     VALUE=<%= request.getAttribute("arregloivas2") %>>
<%--termina Cambio para comprobante fiscal--%>
<INPUT TYPE="HIDDEN" NAME="arregloestatus2"       VALUE=<%= request.getAttribute("arregloestatus2") %>>
<INPUT TYPE="HIDDEN" NAME="contador2"             VALUE=<%= request.getAttribute("contador2") %>>
<INPUT TYPE="HIDDEN" NAME="imptabla"       VALUE="SI"/>
<INPUT TYPE="HIDDEN" NAME="trans" VALUE=<%= request.getAttribute("trans") %>>



</FORM>


	<%-- CONTENIDO FINAL --%>
</body>
</html>

<Script type="text/javascript">
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.html#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
</Script>
<%-- 5.5 --%>
<%}%>
