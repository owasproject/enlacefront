<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<html>
	<head>
		<title>Detalle Consulta Remesas</title>
	</head>
	<body>
		<%
		ArrayList listaRemesasAdm = (ArrayList) request.getAttribute("listaRemesasAdmin");
		if (listaRemesasAdm != null && listaRemesasAdm.size() > 0) {
		int numRemesas = listaRemesasAdm.size();
%>
		<table>
			<tr>
				<td align="left">
					Total de remesas: <%=numRemesas%>
				</td>
			</tr>
			<tr>
				<td align="center">
					<table width="500" border="1">
						<tr>
							<td align="center" width="275" class="tittabdat">N&uacute;mero Remesa</td>
							<td align="center" width="275" class="tittabdat">Fecha de Emisi&oacute;n</td>
							<td align="center" width="275" class="tittabdat">Fecha de Confirmaci&oacute;n o Cancelaci&oacute;n</td>
							<td align="center" width="275" class="tittabdat">N&uacute;mero de Registros</td>
							<td align="center" width="275" class="tittabdat">Estatus</td>
						</tr>
						<%
						int tamano = listaRemesasAdm.size();

						Iterator iterator = listaRemesasAdm.iterator();

						while (iterator.hasNext()) {

							RemesasBean remesa = (RemesasBean) iterator.next();
						%>
						<tr>
							<td> <%= remesa.getNumRemesa() %></td>
							<td><%= remesa.getFechaAltaRem() %></td>
							<!-- <td><%= remesa.getFechaRecepRem() %></td> -->
							<% if(remesa.getEstadoRemesa().equals("CANCELADA")){%>
							<td><%= remesa.getFechaBajaRem() %></td>
							<%}else if(remesa.getEstadoRemesa().equals("ENVIADA") || remesa.getEstadoRemesa().equals("NUEVA")){ %>
								<td></td>
							<%}else if(remesa.getEstadoRemesa().equals("ASIGNADA")){ %>
								<td><%= remesa.getFechaRecepRem() %></td>
							<%}else{ %>
								<td></td>
							<%} %>
							<td><%= remesa.getNumTarjetas() %></td>
							<td><%= remesa.getEstadoRemesa() %></td>
						</tr>
						<%
						}
						%>

					</table>
				</td>
			</tr>
	</table>
	<%
			}else{
		%>
<table>
	<tr>
		<td align="center">
			<table border="1">
				<tr>
					<td align="center">
						<br/>El contrato no tiene remesas asociadas<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
		</td>
	</tr>
</table>
<%	} %>
		<br>
	</body>
</html>