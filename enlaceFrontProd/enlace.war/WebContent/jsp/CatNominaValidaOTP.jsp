<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="EnlaceMig.*"%>
<html>
<head>
<title>Bienvenido a Enlace Internet</title>

<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s26050">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
<script language="JavaScript"    src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript"    src="/EnlaceMig/Convertidor.js"></script>
<script language="JavaScript"    src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/list.js"></SCRIPT>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>

<%// Comienza c�digo MKD -------------------------------------------------------
if(EnlaceMig.Global.USAR_MKD.equals("ON")){
%>
<script language="javascript" type="text/javascript" src="/EnlaceMig/mkd25x.js"></script>
<script language="javascript" type="text/javascript" src="/EnlaceMig/ObjetoSantander.js"></script>
<script language="javascript" type="text/javascript">
//Validamos si el producto esta instalado en la computadora del cliente.
if(mkd25x_installed()) {
	function loading_check(){
		if( mkd25x_loaded() == false ){
			window.setTimeout( loading_check, 200 );
		}else{
			mkd25x_set_authprefix( 'mkd25x' );
			mkd25x_set_authserver( 'www.cddmex.com' );
			mkd25x_set_protect_level('high');
			mkd25x_start_async( 'mf/mkd' );
		}
	}

	mkd25x_write_object();
	window.setTimeout( loading_check, 200 );
}

function Login() {
	if(mkd25x_loaded()){
		mkd25x_copy_to_form(document.form1);
	}
}
</script>
<%}//Termina c�digo MKD---------------------------------------------------------%>

<script language="javascript" type="text/javascript">
			function MM_preloadImages() { //v3.0
			 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
			   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
			}

			function MM_swapImgRestore() { //v3.0
			  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}

			function MM_findObj(n, d) { //v3.0
			  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
			  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
			}

			function MM_swapImage() { //v3.0
			  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
			   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
			}

			function unSoloContrato(){
			cuadroDialogo ('Solo existe un contrato asociado.',1);
			 }

			function MM_nbGroup(event, grpName) {
				var i,img,nbArr,args=MM_nbGroup.arguments;
			  	if (event == "init" && args.length > 2) {
			    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
				img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
				if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
				nbArr[nbArr.length] = img;
				for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
				  if (!img.MM_up) img.MM_up = img.src;
				  img.src = img.MM_dn = args[i+1];
				  nbArr[nbArr.length] = img;
			    } }
			  } else if (event == "over") {
			    document.MM_nbOver = nbArr = new Array();
				 for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
				if (!img.MM_up) img.MM_up = img.src;
				img.src = (img.MM_dn && args[i+2]) ? args[i+2] : args[i+1];
				nbArr[nbArr.length] = img;
			    }
			  } else if (event == "out" ) {
				 for (i=0; i < document.MM_nbOver.length; i++) {
			 img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
			  } else if (event == "down") {
				if ((nbArr = document[grpName]) != null)
			 for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
				 document[grpName] = nbArr = new Array();
			    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
				if (!img.MM_up) img.MM_up = img.src;
				img.src = img.MM_dn = args[i+1];
			 nbArr[nbArr.length] = img;
			  } }
			}

			function validaForma() {

				Login();

				token = document.form1.token.value;
				if(token.length != 8) {
					cuadroDialogo ("La longitud de la contrase&ntilde;a debe ser de 8 posiciones.", 1);
					return;
				}
				if(!validaFormato(token)) {
					cuadroDialogo ("La contrase&ntilde;a debe ser num&eacute;rica.", 1);
					return;
				}
				document.form1.submit();
			}

			function validaFormato (pwd) {
				var TemplateF=/^[\d]{8,8}$/i;  //dominio de 0-9
				return TemplateF.test(pwd);  //Compara "pwd" con el formato "Template"
			}
<% if( request.getSession().getAttribute("session")!=null)
out.println(((BaseResource)request.getSession().getAttribute("session")).getFuncionesDeMenu() );
%>

<%= request.getAttribute("MensajeLogin01") %>
</script>

<style type="text/css">
<!--
td {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #666666;
	text-decoration: none;
}

.titulo {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}

.tituloCopy {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #666666;
	text-decoration: none;
}
-->

</style>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
<form name="form1" method="post" action="/NASApp/enlaceMig/CatalogoNominaEmpl?opctrans=1&valida=0">

		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  			<tr valign="top">
				<td width="*">

<% if( request.getSession().getAttribute("session")!=null)
out.println(((BaseResource)request.getSession().getAttribute("session")).getStrMenu() );%>
					<!-- MENU PRINCIPAL -->
					<script language="JavaScript1.2">fwLoadMenus();</script>
					<!-- Este es el codigo HTML del Navegador. Aqui es donde estan todas las imagenes y los botones que mandan llamar a los javascripts del menu -->
					<!--%=request.getSession().getAttribute("Encabezado") %-->
				</TD>
  			</TR>
	<tr>

          <td align="right"><a href="/NASApp/enlaceMig/logout" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('finsesion','','/gifs/EnlaceMig/gbo25181.gif',1)">
<img src="/gifs/EnlaceMig/gbo25180.gif" width="44" height="49" name="finsesion" border="0"></a></td>


        </tr>
  			<TR>
  				 <TD ALIGN="CENTER">
    				<table width=500 border=0>
    					<tr>
    						<td align="center">
    							<table width="94%" border="0" cellspacing="0" cellpadding="0">
    								<tr>
    									<td height="20" align="center" class="titulo">&nbsp;</td>
    								</tr>
    								<tr>
    									<td height="20" align="center" bgcolor="#94B2DF" class="titulo"> <p class="tituloCopy"><font color="#000000">Capture
							              su contrase&ntilde;a din&aacute;mica</font></p>
							            </td>
							        </tr>
							        <tr>
							        	<td>&nbsp;</td>
							        </tr>
							        <tr>
							        	<td align="center">
							        		<table width="56%" border="0" cellspacing="0" cellpadding="0">
							        			<tr valign="middle">
							        				<td width="150" class="tituloCopy">Contrase&ntilde;a din&aacute;mica:</td>
													<td>
												<input name="token" type="text" size="8" maxlength="8">

										            </td>
										        </tr>
										    </table>
											<p>&nbsp;</p>
										</td>
									</tr>
									<tr>
										<td align="center"><a href="javascript:validaForma()"><img src="/gifs/EnlaceMig/gbo25280.gif" width="84" height="26" border="0"></a></td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr valign="top">
													<td width="36%"><strong>Paso 1.</strong> <br>Presione el bot&oacute;n de su token</td>
													<td>&nbsp;</td>
													<td width="36%"><strong>Paso 2. </strong><br>Capture la contrase&ntilde;a din&aacute;mica que aperece en
									                  el Token.</td>
									            </tr>
									        </table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="center">
								<table width="500" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="2"><img src="/gifs/EnlaceMig/img001.gif" width="173" height="49"></td>
										<td width="2"><img src="/gifs/EnlaceMig/img4.gif" width="42" height="49"></td>
										<td width="2"><img src="/gifs/EnlaceMig/img7.gif" width="97" height="49"></td>
								        <td><img src="/gifs/EnlaceMig/img12.gif" width="205" height="49"></td>
							        </tr>
							        <tr>
							          <td><img src="/gifs/EnlaceMig/img2.gif" width="173" height="56"></td>
								      <td><a href="#"><img src="/gifs/EnlaceMig/img5_an.gif" width="42" height="56" border="0"></a></td>
							          <td>
							          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							              <tr>
							              	<td><img src="/gifs/EnlaceMig/img8.gif" width="97"></td>
							              </tr>
							              <tr>
							              	<td>
							              		<table width="100%" border="0" cellspacing="0" cellpadding="0">
								                    <tr>
								                      <td width="2"><img src="/gifs/EnlaceMig/img9.gif" width="18"></td>
								                      <td align="center" bgcolor="#A3B28E"><font color="#000000">12345678</font></td>
								                    </tr>
								                </table>
								            </td>
								          </tr>
								          <tr>
								          	<td><img src="/gifs/EnlaceMig/img10.gif" width="97"></td>
								          </tr>
								        </table>
								      </td>
								      <td><img src="/gifs/EnlaceMig/img13.gif" width="205" height="56"></td>
								    </tr>
								    <tr>
										<td><img src="/gifs/EnlaceMig/img3.gif" width="173" height="30"></td>
										<td><img src="/gifs/EnlaceMig/img6.gif" width="42" height="30"></td>
										<td><img src="/gifs/EnlaceMig/img11.gif" width="97" height="30"></td>
										<td><img src="/gifs/EnlaceMig/img14.gif" width="205" height="30"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</TD>
			</TR>
		</TABLE>
  </form>
	</body>
</html>