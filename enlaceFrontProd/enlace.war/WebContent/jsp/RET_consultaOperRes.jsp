<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.math.BigDecimal" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@ page import="mx.altec.enlace.bo.RET_ControlConsOper"%>
<jsp:directive.page import="mx.altec.enlace.bo.RET_ControlConsOper"/>

<%@page import="mx.altec.enlace.bo.RET_OperacionVO"%><html>
<head>
	<% 
 	   RET_ControlConsOper controladorLista = (RET_ControlConsOper) session.getAttribute("controladorLista");	    	   
	%>
	<title>Consulta de Operaciones FX Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">
	// ---------------------------------------------------

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	// ---------------------------------------------------
	
	function continua(){
	
	}

	/*Función para el boton Ver detalle*/
	function verDetalle(){
		var forma = document.frmRetConsultaOperRes;
		var valRadio="-1";
		var contador = 0;
		
		for(j=0;j<forma.length;j++)
     		if(forma.elements[j].type=='radio' && forma.elements[j].name=='listOperRET' )
	    		if(forma.elements[j].checked)
	       			valRadio=forma.elements[j].value;

   		if(valRadio=="-1"){
	   		cuadroDialogo("Debe seleccionar una opci&oacute;n",11);	   		
	 	}else{
			forma.operSel.value = valRadio;
			forma.opcion.value = "verDetalle";
			forma.submit();
		}
	}	
	
	/*Funcion para el boton regresar*/
	function regresar(){
		document.frmRetConsultaOperRes.action = "RET_ConsultaOper";
  		document.frmRetConsultaOperRes.opcion.value = "iniciar";
  		document.frmRetConsultaOperRes.submit();
	}	

	/*Funcion para el boton exportar*/
	function downloadFile(){
		document.location.href = "<%= (String)session.getAttribute("fileExport") %>";				
	}
	
	/*Funcion para el link Primero*/
	function primero(){
		document.frmRetConsultaOperRes.action = "RET_ConsultaOper";
		document.frmRetConsultaOperRes.detalle.value = "N";
  		document.frmRetConsultaOperRes.opcion.value = "primero";
  		document.frmRetConsultaOperRes.submit();
	}
	
	/*Funcion para el link anterior*/
	function anterior(){
		document.frmRetConsultaOperRes.action = "RET_ConsultaOper";
		document.frmRetConsultaOperRes.detalle.value = "N";
  		document.frmRetConsultaOperRes.opcion.value = "anterior";
  		document.frmRetConsultaOperRes.submit();	
	}
				
	/*Funcion para el link siguiente*/
	function siguiente(){
		document.frmRetConsultaOperRes.action = "RET_ConsultaOper";
		document.frmRetConsultaOperRes.detalle.value = "N";
  		document.frmRetConsultaOperRes.opcion.value = "siguiente";
  		document.frmRetConsultaOperRes.submit();
	}
	
	/*Funcion para el link ultimo*/
	function ultimo(){
		document.frmRetConsultaOperRes.action = "RET_ConsultaOper";
		document.frmRetConsultaOperRes.detalle.value = "N";
  		document.frmRetConsultaOperRes.opcion.value = "ultimo";
  		document.frmRetConsultaOperRes.submit();	
	}

	</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>
	<br>
	<FORM NAME="frmRetConsultaOperRes" METHOD="POST" ACTION="RET_ConsultaOper" >
	<%		
	if(controladorLista!=null){
		EIGlobal.mensajePorTrace("Operaciones RET JSP->"+controladorLista.getLista().size(), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Operaciones RET!=null->"+(controladorLista!=null), EIGlobal.NivelLog.DEBUG);
		EIGlobal.mensajePorTrace("Operaciones RET.size()>0->"+(controladorLista.getLista().size()>0), EIGlobal.NivelLog.DEBUG);
	}	
	if(controladorLista!=null && controladorLista.getLista().size()>0){
	%>
	<table cellpadding='2' cellspacing='1' width = "80%" align="center">
		<tr>
			<td class='texenccon' colspan="9">
				Registros del <%=controladorLista.getNumRecordPageDesde()%> al <%=controladorLista.getNumRecordPageHasta()%><br>
				Pagina <%=controladorLista.getIdPage()%> de <%=controladorLista.getNumPages()%>
			</td>						
		</tr>
		<tr>
			<td class="tittabdat" align="center">
				<B>Detalle</B>
			</td>
			<td class="tittabdat" align = "center"><B>Folio Operación</B></td>
			<td class="tittabdat" align = "center"><B>Divisa Operante</B></td>
			<td class="tittabdat" align = "center"><B>Contra Divisa</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Operación</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Cambio</B></td>
			<td class="tittabdat" align = "center"><B>Importe Operación</B></td>
			<td align="center" class="tittabdat"><b>Fecha/Hora &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pactado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			<td align="center" class="tittabdat"><b>Fecha/Hora &nbsp;Complemento&nbsp;</b></td>
			<td align="center" class="tittabdat"><b>Fecha/Hora &nbsp;&nbsp;&nbsp;&nbsp;Liberación&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
			<td class="tittabdat" align = "center"><B>Usuario Registro</B></td>
			<td class="tittabdat" align = "center"><B>Usuario Autoriza</B></td>			
			<td class="tittabdat" align = "center"><B>Estatus Operación</B></td>	
		</tr>
		<%
		for(int i=0; i<controladorLista.getLista().size(); i++){%>
			<%if((i%2)==0){%>
				<tr>
					<%=((RET_OperacionVO)controladorLista.getLista().get(i)).toStringCon(0)%>
				</tr>
			<%}else{ %>
				<tr>
					<%=((RET_OperacionVO)controladorLista.getLista().get(i)).toStringCon(1)%>
				</tr>
			<%}%>
		<%}%>			
	</table>
	<br>
	<input type=hidden name=opcion >
	<input type=hidden name=detalle >
	<input type=hidden name=operSel >
	<br>

	<%
    EIGlobal.mensajePorTrace("controladorLista.getNumRecords()->"+(controladorLista.getNumRecords()), EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace("controladorLista.getMaxNumRecordsPage()->"+(controladorLista.getMaxNumRecordsPage()), EIGlobal.NivelLog.DEBUG);
	if(controladorLista.getIdPage()==1
		 && controladorLista.getNumRecords()>controladorLista.getMaxNumRecordsPage()){%>
		<div align='center'>
			<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
			<a href="javascript:ultimo();">Ultimos 50 ></a>
		</div>
	<%}else if(controladorLista.getIdPage()>1
				&& controladorLista.getIdPage()<controladorLista.getNumPages()){%>
		<div align='center'>				
			<a href="javascript:primero();"> Primeros 50</a>&nbsp;
			<a href="javascript:anterior();"> Anteriores 50</a>&nbsp;
			<a href="javascript:siguiente();">Siguientes 50 ></a>&nbsp;
			<a href="javascript:ultimo();">Ultimos 50 ></a>
		</div>			
	<%}else if(controladorLista.getIdPage()==controladorLista.getNumPages()
				&& controladorLista.getNumRecords()>controladorLista.getMaxNumRecordsPage()){%>
		<div align='center'>
			<a href="javascript:primero();"> Primeros 50</a>&nbsp;
			<a href="javascript:anterior();"> Anteriores 50</a>
		</div>			
	<%}%>
	
	<div align='center'>
		<a href="javascript:verDetalle();" border = 0><img src= "/gifs/EnlaceMig/gbo25248.gif" border=0 alt='Ver Detalle'></a>
		<a href="javascript:downloadFile();"><img src='/gifs/EnlaceMig/gbo25230.gif' border='0' alt='Exportar'></A>
	</div>
	<%}%>
		
	</FORM>
	
	<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
	%>
</body>
</html>