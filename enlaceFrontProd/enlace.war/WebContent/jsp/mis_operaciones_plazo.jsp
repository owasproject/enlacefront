<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language = "JavaScript" SRC="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<!-- JavaScript del App -->
<SCRIPT LANGUAGE="JavaScript">
var js_diasInhabiles = "<%= request.getAttribute( "diasInhabiles" ) %>"

function isDigit (c)
{
  document.transferencia.monto.value="";
  cuadroDialogo("\n Capture el IMPORTE a transferir ",3);
   //document.transferencia.monto.focus();
  return ((c >= "0") && (c <= "9"));

}

function Formatea_Importe(importe)
{
   decenas="";
   centenas="";
   millon="";
   millares="";
   importe_final=importe;
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length);

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1);
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2);
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3);
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}
function Formatea_Importe2 (cantidad)
{
  strAux1 = "";
  strAux2 = "";
  entero = "";
  cantidadAux = cantidad;


  pos_punto = cantidadAux.indexOf (".");

  num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

  if (pos_punto == 0)
     strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length);
  else
  {
     if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length);
        entero = cantidadAux.substring (0, pos_punto);
      }
      else
      {
        cents = "00";
        entero = cantidadAux;
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length);
        miles = entero.substring (0, entero.length - 3);
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length);
          miles = entero.substring (0, entero.length - 3);
        }
        else
        {
          if (entero.length == 0)
            cientos = "";
          else
            cientos = entero;
          miles = "";
        }
      }

      if (miles != "")
        strAux1 = miles ;
      if (cientos != "")
        strAux1 = strAux1 + cientos + ".";
      strAux1 = strAux1 + cents;

    strAux1=Formatea_Importe(strAux1);
  }
  return strAux1;

}


function validAmount (cantidad)
{

  strAux1 = "";
  strAux2 = "";
  entero = "";
  cantidadAux = cantidad;
  //modificacion para integracion
  //var cta_destino=document.plazo.destino.options[document.plazo.destino.selectedIndex].value;
  var cta_destino=document.plazo.destino.value;

  var tipo_cuenta=cta_destino.substring(0,2);

  if (cantidadAux == "" || cantidadAux <= 0)
  {
    document.plazo.importe.value="";
    cuadroDialogo("Capture el IMPORTE a transferir ",3);
    //document.plazo.importe.focus();
    return false;
  }
  else if  ((tipo_cuenta=="65") && (cantidadAux < 3000))
  {
    document.plazo.importe.value="";
    cuadroDialogo("\n Valor minimo de inversion es de $5 000 ",3);
    //document.plazo.importe.focus();
    return false;
  }
  else if  ((tipo_cuenta=="60") && (cantidadAux < 500))
  {
    document.plazo.importe.value="";
    cuadroDialogo("\n Valor minimo de inversion es de $500 ",3);
    //document.plazo.importe.focus();
    return false;

  }
  else
  {
    if (isNaN(cantidadAux))
    {
	  document.plazo.importe.value="";
      cuadroDialogo("Favor de ingresar un valor num\351rico en el  IMPORTE",3);
      //document.plazo.importe.focus()
      return false;
    }

    pos_punto = cantidadAux.indexOf (".");

    num_decimales=cantidad.substring(pos_punto + 1,cantidad.length);

    if (pos_punto == 0)
      strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
    else
    {
      if (pos_punto != -1)     //-- si se teclearon los centavos
      {
        cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
        entero = cantidadAux.substring (0, pos_punto)
      }
      else
      {
        cents = "00"
        entero = cantidadAux
      }

      pos_coma = entero.indexOf (",")
      if (pos_coma != -1)     //-- si son mas de mil
      {
        cientos = entero.substring (entero.length - 3, entero.length)
        miles = entero.substring (0, entero.length - 3)
      }
      else
      {
        if (entero.length > 3) //-- si se teclearon mas de mil sin coma
        {
          cientos = entero.substring (entero.length - 3, entero.length)
          miles = entero.substring (0, entero.length - 3)
        }
        else
        {
          if (entero.length == 0)
            cientos = ""
          else
            cientos = entero
          miles = ""
        }
      }

      if (miles != "")
        strAux1 = miles
      if (cientos != "")
        strAux1 = strAux1 + cientos + "."
      strAux1 = strAux1 + cents

      if (miles != "")
        strAux2 = miles
      if (cientos != "")
        strAux2 = strAux2 + cientos + "."
      strAux2 = strAux2 + cents

      transf = document.plazo.importe.value
    }
    document.plazo.importe.value = strAux1;

    strAux1=Formatea_Importe(strAux1)
    if (miles != "")
      document.plazo.importestring.value = strAux2;
    else
	  document.plazo.importestring.value = strAux1;
    return true;


  }
}

function Valida_Date() {
  result = true;
  document.plazo.fecha.value=document.plazo.fecha_completa.value;
  return result;
}

function validctasdif (from_account, to_account)
{
  result=true;

  if (from_account == to_account)
  {
    cuadroDialogo("La cuenta de cargo debe ser diferente a la cuenta de abono",3);
    //document.plazo.monto.focus()
    return false
  }
  return result;
}



function validar()
{
  result=Valida_Date();
  if (result==true)
    result=validAmount (document.plazo.importe.value);
  return result;
}

var gConfirmacion= false;
function confirmacion()
{
  //modificacion para integracion
  //var cuenta=document.plazo.destino.options[document.plazo.destino.selectedIndex].value;
  var cuenta=document.plazo.destino.value;
  var des=cuenta;
  cuenta=cuenta.substring(0,cuenta.indexOf("|"));
  des=des.substring(des.indexOf("|")+1,des.length);
  des=des.substring(des.indexOf("|")+1,des.length-1);

  result=validar();
  gConfirmacion = result;
  if ( result == true )
  {
     cuadroDialogo("¿ Desea transferir de la cuenta:"+ cuenta+" de "+des+" a Plazo la cantidad de $ "+Formatea_Importe2(document.plazo.importestring.value),2);
  }
}

function continua()
{
   if (gConfirmacion == true )
   {
      if (respuesta==1) document.plazo.submit();
        else return false;

   }
   else
       return false;
}


function limpiar_datos()
{
   document.plazo.importe.value="";
   //modificacion para integracion document.plazo.destino.selectedIndex=0;
   document.plazo.destino.value=""; document.plazo.fecha_completa.value=document.plazo.dia.value+"/"+document.plazo.mes.value+"/"+document.plazo.anio.value;
}

var dia;
var mes;
var anio;
var fecha_completa;


function WindowCalendar1()
{
    var m=new Date();
    n=m.getMonth();
    n=document.plazo.mes.value-1;

	dia=document.plazo.dia.value;
	mes=document.plazo.mes.value;
	anio=document.plazo.anio.value;
    msg=window.open("/EnlaceMig/calfut.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
    msg.focus();

}

function Actualiza()
{
   document.plazo.fecha_completa.value=fecha_completa;
}



function Consultar_tasas()
{
    tas=window.open("/EnlaceMig/tasas_plazo.html" ,"tasas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    tas.focus();

}

function llamada_posicion()
{
   //modificacion para integracion
   //document.plazo.cta_posi.value="2|"+document.plazo.destino.options[document.plazo.destino.selectedIndex].value;
   document.plazo.cta_posi.value="2|"+document.plazo.destino.value;
   document.plazo.action="poscpsrvr1";
   document.plazo.submit();
   return true;
}

 <!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}

function actualizacuenta()
{
  document.plazo.destino.value=ctaselec+"|"+ctatipre+"|"+ctadescr+"|";
  document.plazo.textdestino.value=ctaselec+" "+ctadescr;
}

</SCRIPT>
<script language="JavaScript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%
      if (request.getAttribute("newMenu")!= null) {
      out.println(request.getAttribute("newMenu"));
      }
%>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
<%
	String MenuPrincipal = "";
	MenuPrincipal =(String) request.getAttribute("MenuPrincipal");
	if ( MenuPrincipal == null )
		MenuPrincipal = "";
%>
	<%= MenuPrincipal %>
	</TD>
  </TR>
</TABLE>
      <%
           if (request.getAttribute("Encabezado")!= null) {
           out.println(request.getAttribute("Encabezado"));
           }
      %>
<FORM NAME="plazo" METHOD=POST  ACTION="plazo?ventana=2" onSubmit="return validar();">
<INPUT TYPE="HIDDEN" NAME="fecha_programada" VALUE=0>
<INPUT TYPE="HIDDEN" NAME="importestring" VALUE=0>
<INPUT TYPE="HIDDEN" NAME="fecha">
<INPUT TYPE="HIDDEN" NAME="fac_programadas1"      VALUE=
<%
     if (request.getAttribute("fac_programadas1")!= null) {
     out.println(request.getAttribute("fac_programadas1"));
     }
%>>
<INPUT TYPE="HIDDEN" NAME="tasas1"                 VALUE=
<%
     if (request.getAttribute("tasas1")!= null) {
     out.println(request.getAttribute("tasas1"));
     }
%>>
<INPUT TYPE="HIDDEN" NAME="cta_posi" VALUE="">

<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
      <td align="center">
        <table border="0" cellspacing="2" cellpadding="3" width="415">
          <tr>
            <td class="tittabdat" colspan="2">
              Capture
              los datos</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top" colspan="2">
              <table border="0" cellspacing="5" cellpadding="0" width="415">
                <tr>
                  <td class="tabmovtex" width="200" nowrap>Instrumento:</td>
                  <td class="tabmovtex" width="200" nowrap>Cuenta:</td>
                </tr>
				<tr>
                  <td class="tabmovtex" nowrap>
				  <SELECT NAME="plazo">
				  <OPTION SELECTED VALUE=7>  7 dias
				  <OPTION VALUE=14> 14   dias
                  <OPTION VALUE=21> 21   dias
                  <OPTION VALUE=28> 28   dias
                  <OPTION VALUE=30> 30   dias
                  <OPTION VALUE=56> 56   dias
                  <OPTION VALUE=91> 91   dias
                  <OPTION VALUE=182>182  dias
                  <OPTION VALUE=371>371  dias
                  </SELECT>
                  </td>
                  <td class="tabmovtex" nowrap>
					<!-- modificacion para integracion
					<SELECT NAME="destino">
					<%
                          if (request.getAttribute("cuentas_destino")!= null) {
                          out.println(request.getAttribute("cuentas_destino"));
                          }
                    %>
					</SELECT>-->
					<input type="text" name=textdestino  class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value="">
					<A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
					<input type="hidden" name="destino" value="" >
                  </td>
                </tr>
				<tr valign="middle">
                  <td class="tabmovtex" nowrap>&nbsp;</td>
                  <td class="tabmovtex" nowrap valign="top" align="left">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex">Importe:</td>
                        <td class="tabmovtex" valign="middle" align="left">
                          <input type="text" name="importe" size="22" class="tabmovtex">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
				<tr valign="top">
                  <td class="tabmovtex" nowrap colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="200" height="5"></td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap>Instrucci&oacute;n
                    al
                    vencimiento:</td>
                  <td class="tabmovtex" nowrap valign="top" align="left">
                    <table border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td class="tabmovtex">Fecha
                          de
                          aplicaci&oacute;n:</td>
                        <td class="tabmovtex" valign="middle" align="left">
 						  <%
                                 if (request.getAttribute("campos_fecha")!= null) {
                                 out.println(request.getAttribute("campos_fecha"));
                                 }
                          %>
						  </td>
                      </tr>
                    </table>
					  </td>
                </tr>
                <tr valign="middle">
                  <td class="tabmovtex" nowrap align="left" valign="top">
                    <table width="200" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=1 CHECKED>
                        </td>
                        <td class="tabmovtex" valign="middle" width="100%" nowrap>Reinvertir
                          capital
                          e
                          intereses
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=2 >
                        </td>
                        <td class="tabmovtex" valign="middle" nowrap>Reinvertir
                          capital
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex" valign="middle">
                          <input type="radio" name=inst1 VALUE=3 >
                        </td>
                        <td class="tabmovtex" valign="middle" nowrap>Transferir
                          capital
                          e
                          intereses
                        </td>
                      </tr>
                    </table>
					</td>
                  <td class="tabmovtex" nowrap valign="top" align="left">&nbsp;
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
		<br>
        <table width="227" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
          <tr>
            <td align="right" valign="top" height="22" width="80">
			   <a href="javascript:confirmacion();" border=0>
              <img src="/gifs/EnlaceMig/gbo25280.gif" border=0></a>
            </td>
            <td align="center" valign="top" height="22" width="76">
			   <a href="javascript:limpiar_datos();" border=0>
              <img src="/gifs/EnlaceMig/gbo25250.gif" border=0></a>
            </td>
            <td align="left" valign="top" height="22" width="71">
			   <a href="javascript:Consultar_tasas();" border=0>
              <img src="/gifs/EnlaceMig/gbo25580.gif" border=0></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
</table>
</form>
	<!-- CONTENIDO FINAL -->
</body>
</html>

<Script language = "JavaScript">
<!--
 function VentanaAyuda(ventana){
    hlp=window.open("/EnlaceMig/ayuda.jsp#" + ventana ,"hlp","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
    hlp.focus();
}
//-->
</Script>