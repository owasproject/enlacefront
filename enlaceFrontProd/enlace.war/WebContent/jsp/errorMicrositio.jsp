<%@page import="mx.altec.enlace.utilerias.*"%>
<%
System.out.println("Ya estoy en el jsp errorMicrositio ...............");
String importe  = (String)request.getParameter("importe");
String servicio_id = (String)request.getParameter("servicio_id");

if (null == importe)
	importe =  (String) request.getAttribute("importe");

if (null == servicio_id)
	servicio_id =  (String) request.getAttribute("servicio_id");

String referencia = "";
String url = "";
String concepto = "";
String lineaCaptura = "";

if(!"PMRF".equals(servicio_id))
{
	EIGlobal.mensajePorTrace("ErrorMicrositio.jsp::el contenido del campo URL es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("ErrorMicrositio.jsp::el contenido del campo URL despues del URLEncoder es:" + java.net.URLEncoder.encode((String)request.getParameter("url")), EIGlobal.NivelLog.INFO);
	EIGlobal.mensajePorTrace("ErrorMicrositio.jsp::el contenido del campo URL despues es:" + request.getParameter("url"), EIGlobal.NivelLog.INFO);
	referencia  = (String)request.getParameter("referencia");
	url  = request.getParameter("url");
	concepto = (String)request.getParameter("concepto");
} else {
	EIGlobal.mensajePorTrace("ErrorMicrositio.jsp::el contenido del campo linea de Captura es:" + request.getAttribute("linea_captura"), EIGlobal.NivelLog.INFO);
	lineaCaptura = (String) request.getAttribute("linea_captura");
	request.removeAttribute("inyeccion");
	request.removeAttribute("inyeccionURL");
	request.setAttribute("inyeccion", true);
	request.getSession().setAttribute("inyeccionURL","true");
}

if(request.getAttribute("inyeccion") != null) {
	if(!"PMRF".equals(servicio_id))
	{
		referencia = "";
		concepto = "";
	} else {
		lineaCaptura = "";
	}
	importe = "";
	servicio_id = "";

	if(request.getSession().getAttribute("inyeccionURL") != null){
		url = "LoginServletMicrositio";
	}
}

System.out.println("Los datos de la sesion en el errorMicrositio son estos.........................");
System.out.println("LA URL de regreso es: ........ " + url);
System.out.println("El importe        es: ........ " + importe);
System.out.println("El servicio_id    es: ........ " + servicio_id);

if(!"PMRF".equals(servicio_id))
{
	System.out.println("El concepto       es: ........ " + concepto);
	System.out.println("LA referencia     es: ........ " + referencia);
} else {
	System.out.println("La linea_captura     es: ........ "+ lineaCaptura);
}
String mensajeerr = (String)request.getAttribute( "MsgError" );
String coderr = (String)request.getAttribute( "CodError" );
String horaerr = (String)request.getAttribute( "HoraError" );
if(mensajeerr==null) mensajeerr = "";
if(coderr==null) coderr = "";
//if(horaerr==null) horaerr = "";

//String url_back = url + "?referencia=" + referencia + "&estatus=9" + "&mensaje=" + mensajeerr + "&folio_oper=" + "&importe=" + importe;
//System.out.println("La URL_BACK ==> " + url_back);
%>
<html>
<head>
<title>Santander</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/JavaScript">

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
var ventana;

function setFormLocation(){
	ventana=window.open('/Enlace/enlaceMig/logout?operacionOK=S','trainerWindow','width=580,height=350,toolbar=no,scrollbars=no,left=210,top=225');
}

function cerrar() {
	ventana.close();
}

//Funcion que realiza el llamado por metodo POST a la URL de Portal y redirecciona posteriormente al login de Enlace
function logoutSuperNet(){
	window.parent.LOGUEADO = false;
}


</script>
<link href="/EnlaceMig/pagos.css" rel="stylesheet" type="text/css"/>
</head>
<body bgcolor="#FFFFFF"leftmargin="20" topmargin="20" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('/gifs/EnlaceMig/cerrar_over.jpg')">
<form name="msgerror" method="POST" action="<%=url%>">
<input type="hidden" name="importe" value="<%=importe%>"/>
<input type="hidden" name="estatus" value="9"/>
<input type="hidden" name="folio_oper" value=""/>
<input type="hidden" name="mensaje" value="<%=mensajeerr%>"/>
<INPUT TYPE="hidden" name="servicio_id" value="<%=servicio_id%>"/>
<%if(!"PMRF".equals(servicio_id)) { %>
	<input type="hidden" name="referencia" value="<%=referencia%>"/>
	<INPUT TYPE="hidden" name="concepto" value="<%=concepto%>"/>
<%} else {%>
	<INPUT TYPE="hidden" name="lineaCaptura" value="<%=lineaCaptura%>"/>
<%} %>
<table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <td><table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2" background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
        </tr>
        <tr>
          <td> <table width="100%" border="0" cellspacing="0" cellpadding="8">
              <tr bgcolor="#FFFFFF">
                <td><strong></strong></td>
                <td><strong><span class="titulorojo">Alerta</span></strong></td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td><img src="/gifs/EnlaceMig/flama.jpg" width="42" height="38"/></td>
                <td> <p>N&uacute;mero de Mensaje: <%=coderr%>. Hora: <%=horaerr%></p>
                  <p><%=mensajeerr%></p></td>
              </tr>
              <tr bgcolor="#FFFFFF">
                <td>&nbsp;</td>
                <td align="center"> <p><a href="javascript:document.msgerror.submit(); setFormLocation(); "onMouseOver="MM_swapImage('Image11','','/gifs/EnlaceMig/cerrar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/cerrar.jpg" name="Image11" width="82" height="41" border="0" id="Image1"/></a></p></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
        </tr>
      </table></td>
  </tr>
</table>
</body>
</html>