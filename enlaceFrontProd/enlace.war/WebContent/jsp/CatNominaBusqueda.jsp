<%--*******************************************************
	**	Busqueda de Empleados en el Cat�logo de N�mina   **
	** 	Invocar modificaci�n de datos de empleados		 **
	** 	Baja de Empleado del Cat�logo de la empresa		 **
	**													 **
	**  15/10/2007 		Everis M�xico					 **
	*******************************************************	--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>B&uacute;squeda de Empleados en el Cat&aacute;logo de N&oacute;mina</title>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
<script type="text/javascript">


/*********************************************/
/*    Funcion continua - Everis 26/10/2007    */
/*********************************************/
function continua()
{
	if (respuesta == 1)
	{
		document.ResultadosEmpleados.opcion.value = "5";
		document.ResultadosEmpleados.action = "CatalogoNominaEmpl";
		document.ResultadosEmpleados.submit();
	}
}


/********************************************************/
/*    Funcion Modificar Empleado - Everis 23/10/2007    */
/********************************************************/
function Modificar()
{
	document.ResultadosEmpleados.opcion.value = "3";
	document.ResultadosEmpleados.action = "CatalogoNominaEmpl";
	document.ResultadosEmpleados.submit();
}
/*********  Fin Modificar Pregunta   ************/

/********************************************************/
/*    Funcion validaCaracter - Everis 24/10/2007  		*/
/********************************************************/
function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}
/*********  Fin Funcion validaCaracter ************/


/********************************************************/
/*    Funcion Cambio de Pagina - jp@Everis 15/02/2007    */
/********************************************************/
function cambioPagina(numPag,numPags,iniBlo,finBlo,tamBlo,totReg,cambPag)
{
	document.ResultadosEmpleados.opcion.value = "2";
	document.ResultadosEmpleados.numeroPaginas.value = numPags;
	document.ResultadosEmpleados.totalReg.value = totReg;
	if( cambPag == 'S' ){
		document.ResultadosEmpleados.numeroPagina.value = numPag + 1;
		document.ResultadosEmpleados.inicioBloque.value = parseInt(iniBlo)+tamBlo;
		document.ResultadosEmpleados.finBloque.value = parseInt(finBlo)+tamBlo;
	}
	else{
		document.ResultadosEmpleados.numeroPagina.value = numPag - 1;
		document.ResultadosEmpleados.inicioBloque.value = parseInt(iniBlo)-tamBlo;
		document.ResultadosEmpleados.finBloque.value = parseInt(finBlo)-tamBlo;
	}
	document.ResultadosEmpleados.action = "CatalogoNominaEmpl";
	document.ResultadosEmpleados.submit();
}


/********************************************************/
/*    Funcion Baja de Empleado - Everis 23/10/2007    */
/********************************************************/
function Baja()
{
<%--	document.ResultadosEmpleados.opcion.value = "4";--%>
<%--	document.ResultadosEmpleados.action = "../servlet/busquedaEmplNom";--%>
<%--	document.ResultadosEmpleados.submit();--%>
//		var empleadoElim = ResultadoEmpleados.txtPregunta.value;
		if(cuadroDialogo("&iquest;Est&aacute; seguro de querer dar de baja <br> el registro seleccionado? \n" , 2 ) )
		{
			return true;
		}

}
/*********  		Fin Baja		  ************/


/********************************************************/
/*    Funcion Buscar Empleado - Everis 23/10/2007    */
/********************************************************/
function Buscar()
{
	isCriterioValid = validaCriterioBusqueda(document.busquedaEmpleados.valorBusqueda.value);

	if(isCriterioValid != true)
	{
		return;
	}
	if(isCriterioValid == true)
	{
		document.busquedaEmpleados.opcion.value = "2";
		document.busquedaEmpleados.action = "CatalogoNominaEmpl";
		document.busquedaEmpleados.submit();
	}
}
// -- Fin de Funci�n Buscar - Everis --//

var filtroFor;

function procesaSubmit(){
	Buscar();
	return false;
}
/********************************************************/
/*    Funcion ValidaCriterio - Everis 23/08/2007    */
/********************************************************/
function validaCriterioBusqueda(criterio)
{
	var caracteresPermitidos = "abcdefghijklmn�opqrstuvwxyz�����" + "ABCDEFGHIJKLMN�OPQRSTUVWXYZ�����" + "0123456789" + " ";
	var strCriterio = criterio;
	var allValid;
	var carInvalidos = 0;

	if(criterio == "" || criterio == null || (criterio == "  "))
	{
		return true;
	}

	for (j=0; j<strCriterio.length; j++)
	{
		caracter = strCriterio.charAt(j);
		for (k=0; k<caracteresPermitidos.length; k++)
		{
			if (caracter == caracteresPermitidos.charAt(k))
			{
				allValid = true;
				break;
			}
			if (k == caracteresPermitidos.length -1)
			{

				cuadroDialogo("No se permiten los siguientes caracteres: <br> @ ! � � ? \" # $ % & / ( ) = + - * / , ' { } [ ] _ � � * + ^ : ; ", 3 );
				allValid = false;
				carInvalidos++;
				break;
			}
		}
		if(carInvalidos != 0)
		{
			break;
		}
	}
	if (!allValid)
	{
		return false;
	}
	if (allValid)
	{
		return true;
	}
}
// -- Fin ValidaCriterio - Everis --/


/********************************************************/
/*    Funcion Limpiar - Everis 23/10/2007    */
/********************************************************/
function Limpiar()
{
	document.busquedaEmpleados.valorBusqueda.value        = "";
	document.busquedaEmpleados.radioValue[0].checked=true;
	document.busquedaEmpleados.valorBusqueda.focus();
}
//  -- Fin de la funci�n Limpiar


/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//  PyMES .. Marzo 2015
// Exportar a archivo plano TXT - CSV
function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;
        }
    }
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=ARCHIVO&em=ExportModel&af=af&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
}

 // PyMES .. Marzo 2015


/********************************************************************************/


<% if (request.getAttribute("newMenu") != null ) { %>
<%= request.getAttribute("newMenu") %>
<%	} %>


</script>

<%--<link rel="stylesheet" href="EnlaceMig/consultas.css" type="text/css">--%>
</head>



<%-- Se construye la interfaz para que el usuario realice la b�squeda de
	empleados en el cat�logo de n�mina y posteriormente realice una baja
	o modificaci�n sobre el mismo.  --%>

	<BODY>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">

			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</td>
	</tr>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>



		<FORM NAME="busquedaEmpleados" METHOD="Post" action="" onSubmit="return procesaSubmit();">
		<input type="hidden" name="opcion" value=""/>

		<CENTER>
		<br>
		<br>
			<table width="500" border="0" cellspacing="2" cellpadding="3" class="textabdatcla">
			<tbody>
				<tr>
					<td class="tittabdat" colspan="2" > Capture los datos para la consulta de Empleados<br/></td>
				</tr>

				<tr>
					<td class="tabmovtex" width="150">Ingrese criterio de b&uacute;squeda: </td>
					<td class="CeldaContenido" width="270">
						<input type="text" name="valorBusqueda" SIZE="70" MAXLENGTH="30" VALUE="" class="tabmovtex" onchange="convierteMayus(busquedaEmpleados.valorBusqueda)" />
					</td>
				</tr>

				<tr>
					<td class="tabmovtex" colspan="2">
						<input type="radio" name="radioValue" value="id" checked="checked"/>Num. Empleado&nbsp;&nbsp; &nbsp;
						<input type="radio" name="radioValue" value="nombre"/>Nombre&nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="apellido"/>Apellido &nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="rfc"/>RFC&nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="numCuenta" />Num. Cuenta&nbsp; &nbsp; &nbsp;
						<input type="radio" name="radioValue" value="todos"/>Todos&nbsp; &nbsp; &nbsp;
						</td>
				</tr>
			</tbody>
			</table>
<%-- Termina construcci�n de elementos para la b�squeda  --%>



   			<br/>

<%-- Se colocan los botones para realizar la b�squeda o el borrado.  --%>
		    <table width="166" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
		    	<tr>
		    		<td align="right" valign="top" height="22" width="90"><a href="javascript:Buscar();">
          				<img border="0" name="Buscar" value="Buscar" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Buscar"/>
        			</td>
        			<td align="left" valign="middle" height="22" width="76"><a href="javaScript:Limpiar()">
            				<img name="Limpiar" value="Limpiar" border="0" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"/>
         				</a>
         			</td>
         		</tr>
         	</table>
        </CENTER>
	</FORM>
<%-- Termina construcci�n de botones para la b�squeda  --%>






<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;


<FORM NAME="ResultadosEmpleados" METHOD="Post" action="">
	<input type="hidden" name="opcion" value=""/>
	
	<input type="hidden" name="numeroPagina" value=""/>
	<input type="hidden" name="numeroPaginas" value=""/>
 	<input type="hidden" name="inicioBloque" value=""/>
 	<input type="hidden" name="finBloque" value=""/>
 	<input type="hidden" name="totalReg" value=""/>
 	<input type="hidden" name="valorBusqueda" value="<%=request.getAttribute("valorBusqueda") %>"/>
 	<input type="hidden" name="radioValue" value="<%=request.getAttribute("radioValue") %>"/>
 		<%
 		if (request.getAttribute("codigoTabla") != null ) {
 		%>
		<%=
		 request.getAttribute("codigoTabla")
		 %>

  <%} %>

		<% if (request.getAttribute("notFound") != null ) { %>
		<%= request.getAttribute("notFound") %>
		<%	} %>
</FORM>

		<script type="text/javascript">
			<% if (request.getAttribute("valorBusqueda") != null ) { %>
			document.busquedaEmpleados.valorBusqueda.value = '<%= request.getAttribute("valorBusqueda") %>';
			<%	} %>

			<% if (request.getAttribute("radioValue") == null ) {
			System.out.println("Es la primera vez = " + request.getAttribute("radioValue") );%>
			document.busquedaEmpleados.radioValue[0].checked=true;
			<%}%>

			<% if (request.getAttribute("radioValue") != null ) {
			System.out.println("Estamos en el if el valor es = " + request.getAttribute("radioValue") );%>


			//Recuperamos el valor del bot�n del radio al momento de mostrar resultados
			<%	//for(int i = 0; i<7; i++)
				//{
						System.out.println("JSP - Estamos en el for el valor es = " + request.getAttribute("radioValue") );

						if(request.getAttribute("radioValue").equals("id")){
							System.out.println("**** entramos AL ID");
							 %>
							document.busquedaEmpleados.radioValue[0].checked=true;
						<%}

						else if(request.getAttribute("radioValue").equals("nombre")){
							System.out.println("**** entramos aL NOMBRE");
							%>
							document.busquedaEmpleados.radioValue[1].checked=true;
						<%}

						else if(request.getAttribute("radioValue").equals("apellido")){
							System.out.println("*** entramos APELLIDO");
							%>
							document.busquedaEmpleados.radioValue[2].checked=true;
						<%}

						else if(request.getAttribute("radioValue").equals("rfc")){
							System.out.println("*** entramos RFC");
							%>
							document.busquedaEmpleados.radioValue[3].checked=true;
						<%}

						else if(request.getAttribute("radioValue").equals("numCuenta")){
							System.out.println("*** entramos NUMCUENTA");
						%>
							document.busquedaEmpleados.radioValue[4].checked=true;
						<%}%>
					<%//}%>
			<%	} %>
		</script>

<%--	BOT�N DE MODIFICAR REGISTRO		--%>
<%
	if (request.getAttribute("codigoTabla") != null) {
	/*jp@everis*/
	String numeroPagina = request.getAttribute("numeroPagina").toString();
	String numeroPaginas = request.getAttribute("numeroPaginas").toString();
	String inicioBloque = request.getAttribute("inicioBloque").toString();
	String finBloque = request.getAttribute("finBloque").toString();
	String tamanoBloque = request.getAttribute("tamanoBloque").toString();
	String totalReg = request.getAttribute("totalReg").toString();
	%>

	<center>
	<table width="140" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
		<tr>
			<td colspan="3" align="center" class="tabmovtex">
				<a style="cursor: pointer;" >Exporta en TXT <input id="tipoExportacion" type="radio" value ="txt"   name="tipoExportacion" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center" class="tabmovtex">
				<a style="cursor: pointer;" >Exportar en XLS <input id="tipoExportacion" type="radio"  checked value ="csv" name="tipoExportacion" /></a>
			</td>
		</tr>
		<tr>

		  <td align="right" valign="top" height="22" width="90">

		  <a href="javascript:exportarArchivoPlano();">
	          <img border="0" name="Exportar" value="Exportar" src="/gifs/EnlaceMig/gbo25230.gif" width="90" height="22" alt="Exportar"/>
	 	  </a></td>
	        <td align="right" valign="top" height="22" width="90"><a href="javascript:Modificar();">
	          <img border="0" name="Modificar" value="Modificar" src="/gifs/EnlaceMig/gbo25510.gif" width="90" height="22" alt="Modificar"/>
	 	  </a></td>
	 	   <td align="right" valign="top" height="22" width="90"><a href="javascript:Baja();">
	          <img border="0" name="Baja" value="Baja" src="/gifs/EnlaceMig/gbo25490.gif" width="70" height="22" alt="Baja"/>
	 	  </a></td>
	   </tr>
	 </table>
	 <br/>
	  
	 <table width="300" border="0" cellspacing="2" cellpadding="3" bgcolor="#FFFFFF" height="22" class="tabfonbla">
	 	<tr>
	 		<%
	 		if( !numeroPagina.equals("0") ){
	 			if( numeroPagina.equals("1") ){
	 		%>
	 			<td class="textabref" width="25%" align="center">&nbsp;  </td>
	 			<td class="textabref" width="50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" width="100%" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref" width="100%">Registros del 1 al <%=Integer.parseInt(tamanoBloque)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">
	 				<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'S');" >  Siguiente >> </a>
	 			</td>
	 		<%
	 			}
	 			else if ( numeroPagina.equals(numeroPaginas)) {
	 			%>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'A');" >  << Anterior </a>
				</td>
	 			<td class="textabref" width="50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref">Resgistros del <%=Integer.parseInt(inicioBloque)+1%>
	 						al <%=Integer.parseInt(totalReg)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">&nbsp;  </td>
	 			<%
	 			}
	 			else{
	 			%>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'A');" >  << Anterior </a>
				</td>
	 			<td class="textabref" width=50%" align="center">
	 				<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22" align="center" class="tabfonbla">
	 					<tr>
	 						<td class="textabref" align="center">P&aacute;gina <%=numeroPagina%> de <%=numeroPaginas%></td>
	 					</tr>
	 					<tr>
	 						<td class="textabref">Resgistros del <%=Integer.parseInt(inicioBloque)+1%>
	 						al <%=Integer.parseInt(tamanoBloque)*Integer.parseInt(numeroPagina)%></td>
	 					</tr>
	 				</table>
	 			</td>
	 			<td class="textabref" width="25%" align="center">
					<a href="javaScript:cambioPagina(<%=numeroPagina %>,<%=numeroPaginas %>,
	 				<%=inicioBloque %>,<%=finBloque %>,<%=tamanoBloque%>,<%=totalReg %>,'S');" > Siguiente >> </a>
				</td>
	 			<%
	 			}
	 		}
	 		%>
	 	</tr>
	 </table>
	 <!--  jp@everis -->

	 </center>

<%	} %>
<%--	FIN DE BOT�N DE MODIFICAR REGISTRO		--%>

	<script>
		<% if (request.getAttribute("MensajeLogin01") != null ) { %>
		<%= request.getAttribute("MensajeLogin01") %>
		<%	} %>
	</script>


</body>
</html>