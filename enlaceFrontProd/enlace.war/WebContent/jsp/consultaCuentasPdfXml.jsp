<%@page import="mx.altec.enlace.beans.ConfEdosCtaArchivoBean"%>
<%@page import="java.util.List"%>
<%@page	language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>Consulta de Cuentas</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<script type="text/javascript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" SRC= "/EnlaceMig/ocurre.js"></script>
<% 
List cuentas = (List) request.getAttribute("listaCuentasEdoCta");
int cantidad = cuentas.size();
%>
<script type="text/javascript">
	function cierraVentana(){
		window.close();
	}
	function cuentaSelecionada(cuenta,descripcion) {
		opener.ctaselec = cuenta;
		opener.ctadescr = descripcion;
		opener.actualizacuenta();
		setTimeout(function(){cierraVentana();},1000);
	}
	function filtrar() {
		document.getElementById("opcion").value = "1";
		document.filtroFrm.submit();
	}
    function inicio() {
		var cantidad = parseInt("<%=cantidad%>");
		if(cantidad > 50) {
			cuadroDialogo("Excedi&oacute; el n&uacute;mero de cuentas posibles a visualizar. Especifique su b&uacute;squeda por cuenta o descripci&oacute;n", 4);
		}
	}
	function esCuentaValida(cadena){
		if (/^([B][M][E])?[0-9]{0,11}$/.test(cadena.toUpperCase())) {
			return true;
		}
		else {
			return false;
		}
	}
	function EsAlfa(cadena, band){
		for (var i=0;i<cadena.length;i++){
			if(!(((cadena.charAt(i)==' ') && (i != 0))||(cadena.charAt(i)>='a' && cadena.charAt(i)<='z')||(cadena.charAt(i)>='A' && cadena.charAt(i)<='Z')||(cadena.charAt(i)>='0' && cadena.charAt(i)<='9'))){
				cuadroDialogo("No se permiten caracteres especiales en "+band+ ", como: acentos, comas, puntos, etc",4);
				return false;
			}
		}
		return true;
	}
	function validar() {
		var descripcion = document.getElementById('filtroDesc').value;
		var cadena=document.getElementById('filtroCuenta').value;
		var result=true;
		result= EsAlfa(descripcion,"Descripci&oacute;n");
		if(result==true) {
			document.getElementById('filtroDesc').value = descripcion.toUpperCase();
			if (cadena.length>0) {
				if (esCuentaValida(cadena)) {
					result=true;
				} else {
					//cuadroDialogo("El dato debe ser num\u00e9rico",4);
					cuadroDialogo("N\u00famero de cuenta inv\u00e1lido",4);
					result=false;
				}
			}
		}
		return result;
	}
	function seleccion() {
		if ( validar() == true) {
	  		filtrar();
		} 
	}
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body onload="inicio();">

<form name="filtroFrm" id="filtroFrm" "action="ConCtasPdfXmlServlet" method="post">
<input type="hidden" id="opcion" name="opcion" value="";>
<table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td colspan="2"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace Internet"/></td>
    </tr>
	<tr>
		<td colspan="2" class="titpag">Cuentas</td></tr>
	<tr>
		<td class="tabtexcal">Cuenta</td>
		<td class="tabtexcal"><input type="text" size="13" class="tabmovtexbol" name="filtroCuenta" id="filtroCuenta"/></td>
	</tr>
	<tr>
		<td class="tabtexcal">Descripci&oacute;n</td>
		<td class="tabtexcal"><input type="text" size="21" class="tabmovtexbol" name="filtroDesc" id="filtroDesc"/></td>
	</tr>
</table>
<br>
<div id="catalogo" style="height:170px; overflow-y:scroll; z-index:-1;">
<table align="center" valign="top" border="0">
	<thead>
		<tr>
			<th class="tittabdat" align="center">Cuenta</th>
			<th class="tittabdat" align="center">Descripci&oacute;n</th>
		</tr>
	</thead>
	<tbody>				  
	<%
		if(cuentas!=null || !cuentas.isEmpty()) {
			for (int i = 0; i < cuentas.size(); i++) {
				String tdClass = i % 2 == 0 ? "textabdatcla" : "textabdatobs";
				ConfEdosCtaArchivoBean bean = (ConfEdosCtaArchivoBean)cuentas.get(i); 
				if(i<50) {
				%>
		<tr>
			<td class="<%=tdClass%>" align="left"><a href="javascript:cuentaSelecionada('<%=bean.getNomCuenta()%>','<%=bean.getNombreTitular()%>')">
				<%=bean.getNomCuenta()%></a></td>
			<td class="<%=tdClass%>" align="left"><%=bean.getNombreTitular()%></td>
		</tr>
				<%
				}
			}
			if (request.getAttribute("MensajeCtas01")!=null) {
				out.print("<tr><td>"+request.getAttribute("MensajeCtas01")+"</td></tr>");
			}
		} else {
		%><tr><td colspan="2">No se encontraron cuentas para descarga.</td></tr><%
		}
	%>
	</tbody>
</table>
</div>

<table width="300" cellspacing="0" cellpadding="0" border="0" valign="top" align="center">
	<tbody>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="center">
				<a href="#" onclick="javascript:seleccion();"><img width="90" height="22" border="0" alt="Consultar" colspan="0" src="/gifs/EnlaceMig/gbo25220.gif"/></a>
				<a href="javascript:window.print();"><img width="83" height="22" border="0" alt="Imprimir" src="/gifs/EnlaceMig/gbo25240.gif"/></a>
				<a href="#" onclick="javascript:cierraVentana();"><img width="71" height="22" border="0"  alt="Cerrar" src="/gifs/EnlaceMig/gbo25200.gif"></a>
			</td>
		</tr>
	</tbody>
</table>
</form>
</body>
</html>