<head>

<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/Convertidor.js"></script>

<title>Comprobante de Transferencias Internacionales</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">
<%
    String Sfecha_pago = (String)request.getAttribute("FechaHoy");
    String Sanio_fecha_pago = Sfecha_pago.substring(6,10);
    int Ianio_fecha_pago = Integer.parseInt (Sanio_fecha_pago);
%>
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     <%
        if(request.getAttribute("ClaveBanco")!=null )
        {
          if(Ianio_fecha_pago<2005)
          {
            if(request.getAttribute("ClaveBanco").equals("014"))
              out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040.gif\"  BORDER=\"0\"></td>");
            else if(request.getAttribute("ClaveBanco").equals("003"))
              out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040s.gif\" BORDER=\"0\"></td>");
          } else {
            out.println("<td rowspan=\"2\" valign=\"middle\" class=\"titpag\" align=\"left\" width=\"35\"><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=\"0\"></td>");
          }
        }
     %>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
      <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" class="titenccom">Transferencias Internacionales</td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" valign="top" align="center">

              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
                <tr>
                  <td class="tittabcom" align="right" width="115">Contrato:</td>
                  <td class="textabcom" ><%= request.getAttribute("NumContrato") %> <%= request.getAttribute("NomContrato") %></td>
                </tr>

				<tr>
                  <td class="tittabcom" align="right" width="115">Usuario:</td>
				  <td class="textabcom" ><script>document.write(cliente_7To8("<%=request.getAttribute("NumUsuario")%>")); </script> <%= request.getAttribute("NomUsuario") %></td>
				</tr>

				<tr>
                  <td class="tittabcom" align="right" width="115">Fecha y hora:</td>
                  <td class="textabcom" ><%= request.getAttribute("FechaHoy") %> </td>
                </tr>

				<%= request.getAttribute("Tabla") %>

              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table border=0>
   <tr>
    <td class="textabcom" nowrap> &nbsp;&nbsp;&nbsp; <%= request.getAttribute("Mensaje") %></td>
   </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
              <!-- <input type="image" border="0" name="imprimir" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Imprimir"> //-->
			  <a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0"></a>
            </td>
            <td align="left" width="71">
			 <a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0"></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>