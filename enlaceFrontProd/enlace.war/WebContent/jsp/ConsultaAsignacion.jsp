<%@ page import="mx.altec.enlace.beans.*,java.util.*,java.text.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<html>
<head>
<title>Consulta de Asignaciones</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

<style type="text/css">
.componentesHtml{
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
}
</style>
<script language="javaScript">
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() {
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************************/
/*************************************************************************************/
function continua(){

}

<%
 String FechaHoy="";
 FechaHoy=(String)request.getAttribute("FechaHoy");
 if(FechaHoy==null)
    FechaHoy="";
%>

//Indice Calendario
	var Indice=0;


//Arreglos de fechas
<%
	if(request.getAttribute("VarFechaHoy")!= null)
		out.print(request.getAttribute("VarFechaHoy"));
%>

//Dias inhabiles
	diasInhabiles='<% if(request.getAttribute("DiasInhabiles")!= null) out.print(request.getAttribute("DiasInhabiles")); %>';

<%
	String DiaHoy="";
	if(request.getAttribute("DiaHoy")!= null)
 		DiaHoy=(String)request.getAttribute("DiaHoy");
%>


var entro = 0;
var js_diasInhabiles = "<%=request.getAttribute("diasInhabiles")%>";
 var fechaseleccionada;
 var opcioncalrem;
<%if (request.getAttribute("mensajecancela") != null) {
				out.println(request.getAttribute("mensajecancela"));
				request.removeAttribute("mensajecancela");
			}%>
<%if (request.getAttribute("VarFechaHoy") != null)
				out.print(request.getAttribute("VarFechaHoy"));%>

window.onload = function(){
		entro = 0;
		}

function limpiar() {
	document.MantNomina.reset();

}

function WindowCalendar()
    {
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalrem=1;
        msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }

    function WindowCalendar1()
    {
        var m=new Date();
        m.setFullYear(document.Frmfechas.strAnio.value);
        m.setMonth(document.Frmfechas.strMes.value);
        m.setDate(document.Frmfechas.strDia.value);
        n=m.getMonth();
        dia=document.Frmfechas.strDia.value;
        mes=document.Frmfechas.strMes.value;
        anio=document.Frmfechas.strAnio.value;
        opcioncalrem=2;
        msg=window.open("/EnlaceMig/calpas1.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
        msg.focus();
    }



function SeleccionaFechaApli(ind)
 {
   var m=new Date();
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/calNom32.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
   msg.focus();
 }

function validaCancel(){
	entro = 0;
	var forma = document.MantNomina;

	if(forma.fecha1.value.length > 0 ||
	   forma.fecha2.value.length > 0 ||
	   forma.valRemesa.value.length > 0 ||
	   forma.valNumEmpleado.value.length > 0 ||
	   forma.valEnviado.checked ||
	   forma.valAceptado.checked ||
	   forma.valRechazado.checked ||
	   forma.valAsignado.checked){
	   		forma.fecha1.value = "";
	   		forma.fecha2.value = "";
	   		forma.valRemesa.value = "";
	   		forma.valNumEmpleado.value = "";
	   		forma.valEnviado.checked = false;
	   		forma.valAceptado.checked = false;
	   		forma.valRechazado.checked = false;
	   		forma.valAsignado.checked = false;
			cuadroDialogo("El único filtro a considerar en la cancelación es el Folio");
	   		return false;
	}

	if (forma.valFolio.value == ""){
		cuadroDialogo("El n&uacute;mero de folio es obligatorio para la cancelación.");
		return false;
	}
	return true;
}

function valida(){
entro = 0;

	mandaDatosChecks();
	var f=document.MantNomina;

		if(f.valEnviado.checked ||f.valAceptado.checked || f.valRechazado.checked || f.valAsignado.checked)
		{

		   if(f.fecha1.value.length == 0 || f.fecha2.value.length==0)
		   {
		   		cuadroDialogo("Las fechas son obligatorias si activas alg\u00fan estatus", 1);
		   		return false;
		   }
		}
		else
		{

			if(f.fecha1.value.length == 0 && f.fecha2.value.length==0 && f.valFolio.value.length==0)
		   {
		   		if(f.valRemesa.value.length==0)
		   		{
		   			cuadroDialogo("Si no se ingresa fechas o folio, el campo remesa es obligatorio", 1);
		   		return false;
		   		}
		   }
		}
		if(f.fecha1.value.length > 0 && f.fecha2.value.length>0 )
		{
			if (mayor(f.fecha1.value, f.fecha2.value)){
					cuadroDialogo("La Fecha Inicial no puede ser mayor a la Fecha Final", 1);
					return false;
				}
		}

		if (f.valFolio.value != "" && !f.valFolio.value.match(/^[0-9]+$/)) {
			f.valFolio.value = "";
			cuadroDialogo("El n&uacute;mero de folio no es v&aacute;lido.", 1);
			return;
		}

		if (f.valRemesa.value != "" && !f.valRemesa.value.match(/^[0-9]+$/)) {
			f.valRemesa.value = "";
			cuadroDialogo("El n&uacute;mero de remesa no es v&aacute;lido.", 1);
			return;
		}

		if (f.valNumEmpleado.value != "" && !f.valNumEmpleado.value.match(/^[0-9]+$/)) {
			f.valNumEmpleado.value = "";
			cuadroDialogo("El n&uacute;mero de Empleado no es v&aacute;lido.", 1);
			return;
		}




	return true;
}

function mayor(fecha, fecha2){
var fechaIni=fecha.split("/");
var fechaFin=fecha2.split("/");
var fechainicial=fechaIni[2]+fechaIni[1]+fechaIni[0];
var fechafinal=fechaFin[2]+fechaFin[1]+fechaFin[0];
	if(parseInt(fechainicial)>parseInt(fechafinal)){
		return(true);
	}else{
		return(false);
	}
}

function consulta() {
	document.getElementById("enviando").style.visibility="hidden";
	document.getElementById("enviando2").style.visibility="visible";
	if (entro == 0) {
		entro = 1;
		var f=document.MantNomina;
		if(valida()){
			f.opcion.value="consulta";
			f.action="${ctx}ConsultaAsignacionServlet";
			f.submit();
		}else{
			document.getElementById("enviando").style.visibility="visible";
			document.getElementById("enviando2").style.visibility="hidden";
		}
	}

}

function cancela() {
	document.getElementById("enviando").style.visibility="hidden";
	document.getElementById("enviando2").style.visibility="visible";
	if (entro == 0) {
		entro = 1;
		if(validaCancel()){
			cuadroDialogo("&#191;Desea cancelar el folio?",2);
		}else{
			document.getElementById("enviando").style.visibility="visible";
			document.getElementById("enviando2").style.visibility="hidden";
		}
	}
}

function continua(){
	if(respuesta==1) {
		document.MantNomina.opcion.value="cancela";
		document.MantNomina.action.value="${ctx}ConsultaAsignacionServlet";
		document.MantNomina.submit();
	}else if (respuesta==2) {
		document.getElementById("enviando").style.visibility="visible";
		document.getElementById("enviando2").style.visibility="hidden";
	}
}

function soloNumeros(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
 }


function convierteMayus(campo)
{
	tmp = campo.value;
	tmp=tmp.toUpperCase();
	campo.value=tmp;
	return true;
}

function mandaDatosChecks(){
var f=document.MantNomina;
	if(f.valEnviado.checked)
	{
	 document.MantNomina.enviado.value='true';
	}
	if(f.valAceptado.checked)
	{
	 document.MantNomina.aceptado.value='true';
	}
	if(f.valRechazado.checked)
	{
	 document.MantNomina.rechazado.value='true';
	}
	if(f.valAsignado.checked)
	{
	 document.MantNomina.asignado.value='true';
	}

}

 function Actualiza()
    {
        if (opcioncalrem==1)
        document.MantNomina.fecha1.value=fechaseleccionada;
        else if (opcioncalrem==2)
        document.MantNomina.fecha2.value=fechaseleccionada;
    }

/*Funcion para actualizar el registro*/
function Actualiza(){
	if(Indice==0)
		document.MantNomina.fecha1.value=Fecha[Indice];
	if(Indice==1)
		document.MantNomina.fecha2.value=Fecha[Indice];
}

/*Funcion para mostrar los calendarios*/
function js_calendario(ind){
	var m = new Date()
	Indice = ind;
    n = m.getMonth();
    msg = window.open("/EnlaceMig/EI_Calendario.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
}

<%= request.getAttribute("newMenu")%>
</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
	background="/gifs/EnlaceMig/gfo25010.gif">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*"><%= request.getAttribute("MenuPrincipal")%></td>
	</tr>
</table>
<%= request.getAttribute("Encabezado")%>
<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
    <td width="10"><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="1"></td>
  </tr>
</table>
<form name="MantNomina" METHOD="POST" ACTION="">




<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
		<table cellspacing="2" cellpadding="4" border="0" width="600"
			class="textabdatcla">
			<tr>
				<td colspan="2" class="tittabdat">Consulta Estado
				Asignaci&oacute;n<br />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="tabmovtex">Ingrese criterios de
				b&uacute;squeda:</td>
			</tr>

			<tr>
				<td width="60%" valign="top">
				<table>
					<tr>
						<td class="tabmovtex">Folio:</td>
						<td class="CeldaContenido"><input maxlength="12" type="text" value=""
							name="valFolio" size="20" onkeypress="return soloNumeros(event)">
						</td>
					</tr>
					<tr>
						<td class="tabmovtex">Fecha Inicial:</td>
						<td class="tabmovtex" nowrap valign="middle"><input
							type="text" name="fecha1" size="12" class="tabmovtex"
							onfocus="blur();">
							<a href ="javascript:js_calendario(0);">
		                		<img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
		                	</a>
		                </td>
					</tr>
					<tr>
						<td class="tabmovtex">Fecha Final</td>
						<td class="tabmovtex" nowrap valign="middle"><input
							type="text" name="fecha2" size="12" class="tabmovtex"
							onfocus="blur();">
							<a href ="javascript:js_calendario(1);">
                     			<img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle">
                   			</a>
						</td>
					</tr>
					<tr>
						<td class="tabmovtex">Remesa:</td>
						<td class="CeldaContenido"><input maxlength="15" type="text" value=""
							name="valRemesa" size="20" onkeypress="return soloNumeros(event)">
						</td>
					</tr>
					<tr>
						<td class="tabmovtex">N&uacute;mero de Empleado:</td>
						<td class="CeldaContenido"><input type="text" value=""
							name="valNumEmpleado" size="20" maxlength="7"
							onkeypress="return soloNumeros(event)"></td>
					</tr>
				</table>
				</td>
				<td valign="top">
				<table>
					<tr>
						<td class="tabmovtex">Estatus:</td>
						<td class="tabmovtex" align="right">Enviada:</td>
						<td class="CeldaContenido"><input type="checkbox" value=""
							name="valEnviado"></td>
					</tr>
					<tr>
						<td></td>
						<td class="tabmovtex" align="right">Recibida:</td>
						<td class="CeldaContenido"><input type="checkbox" value=""
							name="valAceptado"></td>
					</tr>
					<tr>
						<td></td>
						<td class="tabmovtex" align="right">Rechazada:</td>
						<td class="CeldaContenido"><input type="checkbox" value=""
							name="valRechazado"></td>
					</tr>
					<tr>
						<td></td>
						<td class="tabmovtex" align="right">Asignada:</td>
						<td class="CeldaContenido"><input type="checkbox" value=""
							name="valAsignado"></td>
					</tr>
				</table>
				</td>
			</tr>



		</table>
		<br>
		<table id="enviando2" style="visibility:hidden" border=0 align=center cellpadding=0 cellspacing=0>
   			<tr align="center" class="tabmovtex">
   	 			<td>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
    		</tr>
    	</table>
		<table id="enviando" align=center border=0 cellspacing=0 cellpadding=0>
			<tr>
				<td>
					<div align='center'>
						<a href ="javascript:cancela();" border = 0>
							<img src = "/gifs/EnlaceMig/gbo25190.gif" alt=Cancelar border=0 >
						</a>
						<a href="javascript:consulta();" border=0>
							<img src="/gifs/EnlaceMig/gbo25220.gif" border=0>
						</a>
						<a href="javascript:limpiar();" border=0>
							<img src="/gifs/EnlaceMig/gbo25250.gif" border=0>
						</a>
					</div>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>


<INPUT TYPE="hidden" NAME="opcion"> <INPUT TYPE="hidden"
	NAME="tipoFiltro"> <INPUT TYPE="hidden" NAME="tipoArchivo"><input
	type="hidden" name="inicial" value="true"> <INPUT TYPE="hidden"
	NAME="enviado"> <INPUT TYPE="hidden" NAME="aceptado"> <INPUT
	TYPE="hidden" NAME="rechazado"> <INPUT TYPE="hidden"
	NAME="asignado"></form>
<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
%>
</body>
</HTML>