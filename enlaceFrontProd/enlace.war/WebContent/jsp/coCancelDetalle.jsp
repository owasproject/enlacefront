
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="mx.altec.enlace.bo.BaseResource"%>

<%
	HashMap detalles = (HashMap)session.getAttribute("detalles");
	if(detalles!=null){
		EIGlobal.mensajePorTrace("detalles>"+detalles.size(),EIGlobal.NivelLog.DEBUG);
	}
	String numLinea = (request.getParameter("reg")==null)?"":(String)request.getParameter("reg");
	EIGlobal.mensajePorTrace("numLinea>"+numLinea,EIGlobal.NivelLog.DEBUG);
	String linea = (String)detalles.get(numLinea);
	EIGlobal.mensajePorTrace("linea>"+linea,EIGlobal.NivelLog.DEBUG);
	int pos;
	StringTokenizer parser;
	String campos[] = new String[41];

	while((pos=linea.indexOf(";;")) != -1) linea = linea.substring(0,pos+1) + " " + linea.substring(pos+1);
	EIGlobal.mensajePorTrace("linea->"+linea,EIGlobal.NivelLog.DEBUG);
	parser = new StringTokenizer(linea,";");
	try{
		for(pos=0;parser.hasMoreTokens();pos++) campos[pos] = parser.nextToken();		
		for(;pos<41;pos++) campos[pos] = "";
	}catch(Exception e){}

	HttpSession sess = request.getSession ();
	BaseResource sesBase = (BaseResource) sess.getAttribute ("session");
	String contrato = sesBase.getContractNumber ();
	String nombre = sesBase.getNombreContrato ();
	String tipDoc = "";
	String refe = "";
	String imp = "";
	String fechVen = "";
	String fechOpe = "";	
	String forPag = "";
	String forApl = "";

	if(campos[0].equals("1")){
		tipDoc = ((campos[1].equals("1"))?"Factura":"Otros");
		refe = campos[17];
		imp = aMoneda(campos[7]);
		fechVen = campos[10];
		fechOpe = campos[11];
	}else
		if(campos[0].equals("2")){
			tipDoc = "Nota de " + ((campos[1].equals("3"))?"cargo":"cr&eacute;dito")+" "+(campos[6].equals("A")?"(acreedora)":"(deudora)");
			refe = campos[10];
			imp = aMoneda(campos[5]);
			fechOpe = campos[8];
		}
	if(campos[25].equals("1"))
		forPag = "Mismo banco";
	else if(campos[25].equals("2"))
		forPag = "Otros bancos";
	else if(campos[25].equals("5"))
		forPag = "Internacional";
	 if(campos[6].equals("1"))
		forApl="Mismo d&iacute;a";
	 else
	 	if(campos[6].equals("2"))
			forApl="D&iacute;a siguiente";
	  	else
			forApl="Mismo d&iacute;a";


%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">



<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@page import="java.util.HashMap"%>
<html>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     <td rowspan="2" valign="middle" class="titpag" align="left" width="35"><img SRC="/gifs/EnlaceMig/glo25040b.gif" BORDER="0"></td>
     <td rowspan="2" valign="middle" class="titpag" align="left" width="35"><img SRC="/gifs/EnlaceMig/Logo_Confirming.jpg" BORDER="0"></td>
    </tr>
  </table>

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
       <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" colspan="2" class="titenccom">Pago a Proveedores</td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" height="120" valign="top" align="center">
              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
                <tr>
                  <td class="tittabcom" align="right" width="0">Contrato:</td>
                  <td class="textabcom" align="left" nowrap><%=contrato%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Nombre o Raz&oacute;n Social:</td>
                  <td class="textabcom" align="left" nowrap><%=nombre%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">RFC:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[26]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta cargo:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[23]%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Clave Proveedor:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[35]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Nombre o Raz&oacute;n Social Proveedor:</td>
                  <td class="textabcom" align="left"> <%=campos[40]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">RFC Proveedor:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[27]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Tipo de documento:</td>
                  <td class="textabcom" align="left" nowrap> <%=tipDoc%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">No. de documento:</td>
                  <td class="textabcom" align="left"><%=campos[4]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Divisa:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[24]%></td>
                </tr>
                <%if(campos[25].equals("1") || campos[25].equals("2")){ %>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta Destino/M&oacute;vil:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[28]%></td>
                </tr>
                <%}else {%>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta Destino:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[28]%></td>
                </tr>
                <%}
                if(campos[25].equals("1")){ %>
				<tr>
                  <td class="tittabcom" align="right" width="0">Banco Destino:</td>
                  <td class="textabcom" align="left" nowrap>BANME</td>
                </tr>
                <%}else if(campos[25].equals("2")){%>
				<tr>
                  <td class="tittabcom" align="right" width="0">Banco Destino:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[37]%></td>
                </tr>
                <%}else if(campos[25].equals("5")){%>
				<tr>
                  <td class="tittabcom" align="right" width="0">Banco Destino:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[29]%></td>
                </tr>
                <%}
                if(campos[25].equals("5")){ %>
                <tr>
                  <td class="tittabcom" align="right" width="0">Pais/Plaza/Ciudad:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[30]%> <%=campos[32]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Clave ABA:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[31]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Clave BIC:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[33]%></td>
                </tr>
                <%}%>
                <tr>
<!--GOR 20110113 (Req. Confirming - 8 - Inicio): Cambiar en el comprobante la etiqueta donde dice REFERENCIA Y CONCEPTO INTERBANCARIO por N�MERO DE REFERENCIA Y CONCEPTO DEL PAGO -->
                  <td class="tittabcom" align="right" width="0">N&uacute;mero de Referencia y Concepto del Pago:</td>
<!--GOR 20110113 (Req. Confirming - 8 - Fin) -->
                  <td class="textabcom" align="left" nowrap><%=campos[21]%> <%=campos[22]%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Importe del Documento:</td>
                  <td class="textabcom" align="left" nowrap><%=imp%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Fecha de vencimiento:</td>
                  <td class="textabcom" align="left" nowrap><%=fechVen%></td>
                </tr>
                <tr>
<!--GOR 20110113 (Req. Confirming - 9 - Inicio): Incluir en el Comprobante de operaciones Nacionales la HORA DE APLICACI�N -->
				<%
				if(campos[25].equals("2")) {
 				%>											
                  <td class="tittabcom" align="right" width="0">Fecha de Operaci&oacute;n y Hora de Aplicaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap><%=campos[39]%></td>
				<% 
				} else {
				%>
                  <td class="tittabcom" align="right" width="0">Fecha de Operaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap><%=fechOpe%></td>
				<% 
				}
				%>
<!--GOR 20110113 (Req. Confirming - 9 - Fin)-->
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Forma de aplicaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap><%=forApl%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Referencia de operaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap><%=refe%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Forma de pago:</td>
                  <td class="textabcom" align="left" nowrap><%=forPag%></td>
                </tr>
<!--GOR 20110113 (Req. Confirming - 9 - Inicio): Incluir en el Comprobante de operaciones Nacionales la CLAVE DE RASTREO -->
				<%
				if(campos[25].equals("2")){
 				%>											
                <tr>
                  <td class="tittabcom" align="right" width="0">Clave de rastreo:</td>
                   <td class="textabcom" align="left" nowrap><%=campos[38]%></td>
                </tr>
				<% 
				}
				%>                
<!--GOR 20110113 (Req. Confirming - 9 - Fin)-->
                <tr>
                  <td class="tittabcom" align="right" width="0">Estatus:</td>
                  <td class="textabcom" align="left" nowrap><%=getStatus(campos[19])%></td>
                </tr>
              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
			<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0"></a>
            </td>
            <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0"></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </form>
</body>
</html>

<%!
	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num)
		{
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
		}

	/** Devuelve el estatus del documento segun el tipo indicado */
	private String getStatus(String tipoDoc)
		{
		if(tipoDoc.trim().equals("R"))
			tipoDoc = "Por Pagar";
		else if(tipoDoc.trim().equals("L"))
			tipoDoc = "Liberado";
		else if(tipoDoc.trim().equals("P"))
			tipoDoc = "Pagado";
		else if(tipoDoc.trim().equals("A"))
			tipoDoc = "Anticipado";
		else if(tipoDoc.trim().equals("C"))
			tipoDoc = "Cancelado";
		else if(tipoDoc.trim().equals("Z"))
			tipoDoc = "Rechazado";
		else if(tipoDoc.trim().equals("V"))
			tipoDoc = "Vencido";
		else if(tipoDoc.trim().equals("D"))
			tipoDoc = "Devuelto";
		else
			tipoDoc = "no especificado";
		return tipoDoc;
		}

/*************************************************************************************/
/*************************************************************************************/
/*public boolean fechaIgual(String f1, GregorianCalendar f2)
	{
		int anio1=new Integer(f1.substring(6)).intValue();
		int anio2=f2.get(Calendar.YEAR);
		int mes1=new Integer(f1.substring(3,5)).intValue();
		int mes2=f2.get(Calendar.MONTH)+1;
		int dia1=new Integer(f1.substring(0,2)).intValue();
		int dia2=f2.get(Calendar.DATE);

		System.out.println("\n>" + anio1 + " " + anio2);
		System.out.println(">" + dia1 + " " + dia2);
		System.out.println(">" + mes1 + " " + mes2);

		if(anio1==anio2 && mes1==mes2 && dia1==dia2)
			return true;
		return false;
	}

  public boolean fechaIgual(GregorianCalendar f1, GregorianCalendar f2)
	{
		int anio1=f1.get(Calendar.YEAR);
		int anio2=f2.get(Calendar.YEAR);
		int mes1=f1.get(Calendar.MONTH);
		int mes2=f2.get(Calendar.MONTH);
		int dia1=f1.get(Calendar.DATE);
		int dia2=f2.get(Calendar.DATE);

		if(anio1==anio2 && mes1==mes2 && dia1==dia2)
			return true;
		return false;
	}
*/
%>