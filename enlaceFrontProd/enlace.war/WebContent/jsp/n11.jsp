<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="java.io.InputStreamReader"%>
<HTML>
<HEAD>
<TITLE> Mantenimiento a Proveedores </TITLE>
<META NAME="Generator" CONTENT="EditPlus">
<META NAME="Author" CONTENT="">
<META NAME="Keywords" CONTENT="">
<META NAME="Description" CONTENT="">
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>





<SCRIPT LANGUAGE="Javascript">
var reg;
reg = "";
function js_nuevo(){
  cuadroDeCaptura("Nombre del archivo a crear: ", "Nombre del archivo de proveedores" );
}

function js_alta() {
	if (document.ChesPrincipal.archivo_actual.value != "") {
	 	  document.location = "AltaDeProveedor.jsp?archivo=" + document.ChesPrincipal.archivo_actual.value;
	}
	else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);
}

function setRegistro(valor) {
    document.ChesPrincipal.cosa.value = valor;
	reg = valor;

}

function js_eliminar() {
	if (document.ChesPrincipal.archivo_actual.value != "") {
	 	  document.location = "jsp/Baja.jsp?archivo=" + document.ChesPrincipal.archivo_actual.value +
			  "&Registro=" + reg;
	}
	else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);

}

function js_modificacion() {
	 if (document.ChesPrincipal.archivo_actual.value != "") {
		   document.location = "jsp/Modificacion.jsp?archivo=" +
		   document.ChesPrincipal.archivo_actual.value +
			   "&Registro=" + reg;
	 }
     else
	  cuadroDialogo("No ha creado ni importado un archivo aun", 3);

}


</SCRIPT>
</HEAD>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<%! String Saca(String cadena, int num) {
    int longitud, m,i, posinicial, posfinal;
	 m = i = 0;
	 posinicial = 0;
	      while(m <= num) {
         	if (cadena.charAt(i) == ';')
				m++;
			if (m == (num-1))
				posinicial = i;
			i++;
          }
		  posfinal = i;
     if (num ==0) return cadena.substring(posinicial, posfinal-1);
     else return cadena.substring(posinicial+2, posfinal-1);
   }
%>
<%! String ar = "archivo_actual";
    String def;  %>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">

     </TD>
   </TR>
</TABLE>

<table width="760" border="0" cellspacing="0" cellpadding="0">
   <FORM  NAME="ChesPrincipal" METHOD="POST" ACTION="ChesRegistro">

  <tr>

   <td align="center">

     <table width="680" border="0" cellspacing="0" cellpadding="0">

       <tr>

         <td rowspan="2">

           <table width="460" border="0" cellspacing="2" cellpadding="3">

             <tr>

               <td class="tittabdat" colspan="2"> Mantenimiento a proveedores </td>

             </tr>

             <tr align="center">

               <td class="textabdatcla" valign="top" colspan="2">

                 <table width="450" border="0" cellspacing="0" cellpadding="0">

                   <tr valign="top">

                     <td width="270" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>Archivo:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

                             <input type="text" name="archivo_actual" size="22" class="tabmovtex" value="<%   if (session.getAttribute("ArchivoActual")!= null)
							 out.print(session.getAttribute("ArchivoActual"));%>"  onFocus='blur()'>

                           </td>

                         </tr>

                       </table>

                     </td>

                     <td width="180" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>N&uacute;mero de transmisi&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

                             <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("transmision")!=null) out.print(request.getAttribute("transmision" )); %>" NAME="transmision" onFocus='blur();'>

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaTransmision")!=null) out.print(request.getAttribute("fechaTransmision" )); %>"  NAME="fechaTrans" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">Fecha

                             de actualizaci&oacute;n:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaActualizacion" ) != null) out.print(request.getAttribute("fechaActualizacion" ));  %>" NAME="fechaAct" onFocus='blur();' >

                           </td>

                         </tr>

                       </table>

                     </td>

                     <td width="180" align="right">

                       <table width="150" border="0" cellspacing="5" cellpadding="0">

                         <tr>

                           <td class="tabmovtex" nowrap>Total de registros:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(session.getAttribute("TotalRegistros" )!= null) out.print(String.valueOf(session.getAttribute("TotalRegistros"))); %>" NAME="totRegs" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>Registros aceptados:</td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap>

								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(session.getAttribute("RegistrosAceptados" )!= null) out.print(session.getAttribute("RegistrosAceptados")); %>"  NAME="aceptados" onFocus='blur();' >

                           </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">Registros

                             rechazados: </td>

                         </tr>

                         <tr>

                           <td class="tabmovtex" nowrap valign="middle">

							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("RegistrosRechazados" )!= null) out.print(request.getAttribute("RegistrosRechazados")); %>"  NAME="rechazados" onFocus='blur();' >

                           </td>

                         </tr>

					   </table>

                     </td>

                   </tr>

                 </table>

               </td>

             </tr>

           </table>

         </td>

         <td width="200" height="94" align="center" valign="middle">

			  <A href = "javascript:js_nuevo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>

         </td>

       </tr>

       <tr>

         <td align="center" valign="bottom">

           <table width="200" border="0" cellspacing="2" cellpadding="3">

             <tr align="center">

               <td class="textabdatcla" valign="top" colspan="2">

                 <table width="180" border="0" cellspacing="5" cellpadding="0">

                   <tr valign="middle">

                     <td class="tabmovtex" nowrap>

                       Importar archivo</td>

                   </tr>

                   <tr>

                     <td nowrap>

						  <input type="file" name="Archivo" size="15">

                     </td>

                   </tr>

                   <tr align="center">

                     <td class="tabmovtex" nowrap>

						  <A href = "javascript:js_importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>

                     </td>

                   </tr>

                 </table>

               </td>

             </tr>

           </table>

         </td>

       </tr>

     </table>

     <br>







     <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">

       <tr>

         <td align="right" valign="middle" width="66">

			  <A href = "javascript:js_alta();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>

         </td>

         <td align="left" valign="top" width="127">

  			  <A href = "javascript:js_eliminar();" border=0><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>

         </td>

         <td align="left" valign="top" width="93">

			  <A href = "javascript:js_modificacion();" border=0><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>

         </td>

         <td align="left" valign="top" width="83">

  			  <A href = "javascript:;" border=0><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>

         </td>

       </tr>

     </table>

     <br>

     <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">

       <tr>

         <td align="right" valign="middle" width="78">

			  <A href = "javascript:js_envio();" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>

         </td>

         <td align="left" valign="top" width="97">

			  <A href = "javascript:js_recupera();" border=0><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>

         </td>

         <td align="left" valign="top" width="77">

			  <A href = "javascript:js_borrar();" border=0><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>

         </td>

         <td align="left" valign="top" width="85">

			  <% if(request.getAttribute("lnkExportar") !=null) out.print(request.getAttribute("lnkExportar")); %>

         </td>

       </tr>



     </table>



     <br>

   </td>

 </tr>



     <%  if (session.getAttribute("ArchivoActual")!= null)  {
	 java.io.FileInputStream fins = new
	 java.io.FileInputStream(Global.DOWNLOAD_PATH+ session.getAttribute("ArchivoActual"));
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace	 
	          //java.io.DataInputStream dins = new java.io.DataInputStream(fins);
	          java.io.BufferedReader dins = new java.io.BufferedReader(new InputStreamReader(fins));
//VSWF RRG 08-Dic-2008 F
              String cadena = new String("algo");
				String c;
				c = "";

	             int contador = 0;
                      while (cadena !=null)  {
		                cadena = dins.readLine();
		                contador++;
                      }
              int numRegistros;
	          numRegistros = contador-1;
	          dins.close();
        	  fins.close();
			  String caden;
			  caden = "";
			  java.io.FileInputStream Fins = new java.io.FileInputStream(Global.DOWNLOAD_PATH + session.getAttribute("ArchivoActual"));
//VSWF RRG 08-Dic-2008 I Se quito la classe deprecada, para migracion Enlace			  
	          //java.io.DataInputStream Dins = new java.io.DataInputStream(Fins);
	          java.io.BufferedReader Dins = new java.io.BufferedReader(new InputStreamReader(fins));
//VSWF RRG 08-Dic-2008 F
     %>

     <table width="760" border="0" cellspacing="1" cellpadding="1">

           <tr>
		     <td class="tittabdat" colspan="1" >
		     </td>
             <td class="tittabdat" colspan="1" > Clave de prov.
		     </td>
             <td class="tittabdat" colspan="1" > Nombre o razon social
		     </td>
             <td class="tittabdat" colspan="1" > R.F.C.
		     </td>
			 <td class="tittabdat" colspan="1" > Homo
		     </td>
             <td class="tittabdat" colspan="1" > Codigo de Cliente
		     </td>
             <td class="tittabdat" colspan="1" > Estatus
		     </td>
             <td class="tittabdat" colspan="1" > Descripicion de Estatus
		     </td>

		    </tr>

               <% for(int h = 0; h < numRegistros; h++) { %>
				      <% c = Dins.readLine();%>
				   <tr>
				      <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap><input type ="radio" name = "cosa" value = "cosa"
			         onClick= 'setRegistro("<%=Saca(c, 17)%>");'></td>
                      <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,0)%></B></td>
                      <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,17)%></B></td>
						 <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,18)%></B></td>
						 <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,19)%></B></td>
						 <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,1)%></B></td>
						 <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,23)%></B></td>
						 <td <% if ((h%2)== 0) { %> class="textabdatcla" <%} else { %>
					  class ="textabdatobs" <%}%> nowrap>  <B><%=Saca(c,24)%></B></td>

				   </tr>
		     <% }
					   Dins.close();
			           Fins.close(); %>

      </table>
   <%
	 }
	   %>



</form>
</BODY>
</html>



