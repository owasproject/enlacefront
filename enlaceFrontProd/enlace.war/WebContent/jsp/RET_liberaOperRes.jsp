<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.math.BigDecimal" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>
<%@ page import="mx.altec.enlace.bo.RET_ControlConsOper"%>
<jsp:directive.page import="mx.altec.enlace.bo.RET_ControlConsOper"/>

<%@page import="mx.altec.enlace.bo.RET_OperacionVO"%>
<%@page import="mx.altec.enlace.bo.RET_CuentaAbonoVO"%><html>
<head>
	<%
 	   RET_OperacionVO beanOper = (RET_OperacionVO) session.getAttribute("beanOperacionRET");
	%>
	<title>Complemento y Liberación de Operaciones </title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
	<script language = "JavaScript" SRC= "/EnlaceMig/scrImpresion.js"></script>

	<SCRIPT language = "JavaScript">
	// ---------------------------------------------------

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	<%= request.getAttribute("newMenu") %>

	/*Funcion para sacar el comprobante de la operación*/
	function obtenComprobante(){
		vc=window.open("../jsp/RET_comprobante.jsp",'trainerWindow','width=490,height=640,toolbar=no,scrollbars=yes');
	   	vc.focus();
	}

	/*Funcion para el boton regresar*/
	function regresar(){
		document.frmRETLiberaOperRes.action = "RET_LiberaOper";
  		document.frmRETLiberaOperRes.opcion.value = "iniciar";
  		document.frmRETLiberaOperRes.submit();
	}
</SCRIPT>
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
	<!-- MENU PRINCIPAL -->
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top"><td width="*"><%= request.getAttribute("MenuPrincipal") %></td></tr>
	</TABLE>
	<%= request.getAttribute("Encabezado") %>
	<br>
	<FORM NAME="frmRETLiberaOperRes" METHOD="POST" ACTION="RET_LiberaOper" >
	<table cellpadding='2' cellspacing='1' width = "80%" align="center">
		<tr>
			<td class='texenccon' colspan="9">
				<br>
			</td>
		</tr>
		<tr>
			<td class="tittabdat" align = "center"><B>Referencia Enlace</B></td>
			<td class="tittabdat" align = "center"><B>Folio de Operación</B></td>
			<td class="tittabdat" align = "center"><B>Cuenta Cargo</B></td>
			<td class="tittabdat" align = "center"><B>Cuenta Abono</B></td>
			<td class="tittabdat" align = "center"><B>Beneficiario</B></td>
			<td class="tittabdat" align = "center"><B>Importe Divisa</B></td>
			<td class="tittabdat" align = "center"><B>Tipo de Cambio</B></td>
			<td class="tittabdat" align = "center"><B>Concepto</B></td>
			<td class="tittabdat" align = "center"><B>Estatus Operación</B></td>
			<td class="tittabdat" align = "center"><B>Observaciones</B></td>
		</tr>
		<%for(int i=0; i<beanOper.getDetalleAbono().size(); i++){%>
		<%String  folioOperacion	= ((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getFolioOperAbono();%>
			<tr>
				<td class="textabdatobs" align=center><%=beanOper.getFolioEnla()%></td>
				<td class="textabdatobs" align=center><%=folioOperacion != null ? folioOperacion : "N/A"%></td>
				<td class="textabdatobs" align=center><%=beanOper.getCtaCargo()%></td>
				<td class="textabdatobs" align=center><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getNumCuenta()%></td>
				<td class="textabdatobs" align=center><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getBenefCuenta()%></td>
				<td class="textabdatobs" align=center><%=aMoneda(((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getImporteAbono())%></td>
				<td class="textabdatobs" align=center><%=beanOper.getTcPactado()%></td>
				<td class="textabdatobs" align=center><%=beanOper.getConcepto()%></td>
				<%if(((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getEstatusOperacion().trim().equals("N")){ %>
					<td class="textabdatobs" align=center><font color=red><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getDescEstatusOperacion()%></font></td>
					<td class="textabdatobs" align=center><font color=red><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getObservacionesEnvio()%></font></td>
				<%}else{ %>
					<td class="textabdatobs" align=center><font color=green><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getDescEstatusOperacion()%></font></td>
					<td class="textabdatobs" align=center><font color=green><%=((RET_CuentaAbonoVO)beanOper.getDetalleAbono().get(i)).getObservacionesEnvio()%></font></td>
				<%} %>

			</tr>
		<%}%>
	</table>
	<input type=hidden name=opcion >
	<input type=hidden name=benefSel >
	<br>

			<div align='center'>
				<a href="javascript:regresar();"><img border="0" src="/gifs/EnlaceMig/gbo25320.gif" alt='Regresar'></a>
				<%if(beanOper.getEstatusOperacion().trim().equals("L")){ %>
					<a href="javascript:obtenComprobante();" border = 0><img src= "/gifs/EnlaceMig/gbo25210.gif" border=0 alt='Comprobante'></a>
				<%} %>
			</div>

	</FORM>
	<%=
	(request.getAttribute("mensaje") != null)?
	("<script language='javascript'>cuadroDialogo(" + (String)request.getAttribute("mensaje") +
	");</script>"):""
	%>
</body>
</html>
<%!
	/** Devuelve un texto que representa un valor monetario */
	private String aMoneda(String num){
		int pos = num.indexOf(".");
		if(pos == -1) {pos = num.length(); num += ".";}
		while(pos+3<num.length()) num = num.substring(0,num.length()-1);
		while(pos+3>num.length()) num += "0";
		while(num.length()<4) num = "0" + num;
		for(int y=num.length()-6;y>0;y-=3) num = num.substring(0,y) + "," + num.substring(y);
		return num;
	}
%>