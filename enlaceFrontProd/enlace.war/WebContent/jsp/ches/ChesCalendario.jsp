<%@page contentType="text/html" import="java.util.*,java.text.*,mx.altec.enlace.servlets.*" %>
<%-- jsp:useBean id="chesInhabiles" class="java.util.Set" scope="session"/ --%>
<jsp:useBean id="fechaMin" class="java.util.GregorianCalendar" scope="request" />
<jsp:useBean id="fechaMax" class="java.util.GregorianCalendar" scope="request" />
<jsp:useBean id="error" class="java.lang.String" scope="request" />

<%!
 SimpleDateFormat mesHeaderFormat = new SimpleDateFormat ("MMMM yyyy", new Locale("es","MX"));
 SimpleDateFormat diaFormat = new SimpleDateFormat ("dd");
 SimpleDateFormat sdf = new SimpleDateFormat ("dd/MM/yyyy");
%>
<%
	Set chesInhabiles = (Set) session.getAttribute("chesInhabiles");
 %>


<%-- Pagina para armar el calendario de dias habiles --%>

<html>
<head>
<STYLE TYPE="text/css">
.tittabdato {  font-family: Arial, Helvetica, sans-serif; font-size: 2px; font-weight: bold; background-color: #A4BEE4}
.tit {  font-family: Arial, Helvetica, sans-serif; font-size: 2px; font-weight: bold; background-color: #C7C7C7}
</Style>
    <title>Calendario</title>
    <LINK rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>
    <%-- Secci�n de Scripts
        todo: Cambiar a un archivo js --%>
    <SCRIPT language='javascript'>

    function seleccionaFecha ( fecha ) {
        opener.chesFecha = fecha;
        opener.continuaFecha ();
        self.close();
    }
    </SCRIPT>
</head>
<body bgcolor='white'>
<%-- Imagen del encabezado --%>
<table width="300" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="152"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace Internet"></td>
        <%--td align="right" valign="bottom"><img src="/gifs/EnlaceMig/glo25040.gif" ></td--%>
    </tr>
</table>

<% if( (null != error )&& (!error.equals(""))){
        %><CENTER><%= error %><CENTER><%
    } else { %>
    <%-- Tabla con el calendario --%>
    <%
    GregorianCalendar cal = (GregorianCalendar) fechaMin.clone();
    GregorianCalendar finMes = new GregorianCalendar();
    while ( cal.before( fechaMax) ){


    //if inicio de mes
        if( cal.equals( fechaMin ) || (1 == cal.get( cal.DAY_OF_MONTH )) ){
        finMes = (GregorianCalendar)cal.clone();
        finMes.set( finMes.DAY_OF_MONTH,1);
        finMes.add( finMes.MONTH,1);
        finMes.add( finMes.DAY_OF_MONTH,-1);
    %>
   <TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
        <tr>
            <td class= tit height=7 width='0.5'>.
                <!-- <img src='/gifs/website/gau10010.gif'  height='7' alt='..' name='.'> -->
            </td>
          <td height=7 width='299.5'>
          </td>
        </tr>
        <tr>
            <td class='tittabdato' height='2'>.
                <!--  <img src='/gifs/website/gau10010.gif' width='1' height='2' alt='..' name='.'>	-->
            </td>
          <td class='tittabdato'>.
          </td>
        </tr>
        <tr>
            <td class=tit height=7 width='0.5'>.
                <!-- <img src='/gifs/website/gau10010.gif' width='1' height='7' alt='..' name='.'> -->
            </td>
          <td  height=7 width='299.5'>
          </td>
        </tr>
    </TABLE>
  <TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
        <TR>
            <TD width='15'>
                <IMG src='/gifs/EnlaceMig/gau25010.gif' width='15' height='20' border='0' alt='Cerrar'>
            </TD>
            <TD class='titpag' width='285' valign='top' colspan='6'>
                <%= mesHeaderFormat.format ( cal.getTime() ) %>
            </TD>
        </TR>
    </TABLE>
    <TABLE width='300' border='0' cellspacing='5' cellpadding='0'>
        <TR>
            <TD class='tabtexcal' align='center'>Dom</TD>
            <TD class='tabtexcal' align='center'>Lun</TD>
            <TD class='tabtexcal' align='center'>Mar</TD>
            <TD class='tabtexcal' align='center'>Mie</TD>
            <TD class='tabtexcal' align='center'>Jue</TD>
            <TD class='tabtexcal' align='center'>Vie</TD>
            <TD class='tabtexcal' align='center'>Sab</TD>
        </TR>

        <%}//fin if inicio de mes
    //if inicio semana
        if( cal.equals( fechaMin ) || ( cal.SUNDAY == cal.get ( cal.DAY_OF_WEEK ) ) ){
    %>
        <TR>
        <%}//fin if inicio semana
        // if (primer dia mes o fecha inicial
        if(cal.equals ( fechaMin) || ( 1 == cal.get( cal.DAY_OF_MONTH )) ){
            //if no es domingo
            if( cal.SUNDAY != cal.get(cal.DAY_OF_WEEK ) ){
                int start = 0;
                switch( cal.get( cal.DAY_OF_WEEK) ){
                    case GregorianCalendar.MONDAY:
                        start = 1;break;
                    case GregorianCalendar.TUESDAY:
                        start = 2;break;
                    case GregorianCalendar.WEDNESDAY:
                        start = 3;break;
                    case GregorianCalendar.THURSDAY:
                        start = 4;break;
                    case GregorianCalendar.FRIDAY:
                        start = 5;break;
                    case GregorianCalendar.SATURDAY:
                        start = 6;break;
                }
                for( int c = 0;c<start;++c){%>
                <td> &nbsp; </td><%
                }
            }// end if no es domingo
        }//end if primer dia mes o fecha inicial
    // insert dia
    %>
            <td class='tabtexcal2' align='center'>
    <%
    // if dia inhabil o dom o sab
    	if (chesInhabiles == null)
    		chesInhabiles = (Set) new ArrayList();
        if((cal.SUNDAY  == cal.get(cal.DAY_OF_WEEK))
            ||(cal.SATURDAY == cal.get(cal.DAY_OF_WEEK) )
            ||((chesInhabiles.size() > 0) && (chesInhabiles.contains( cal ))) ){%>
                <%=diaFormat.format( cal.getTime() )%>
        <%}else{// else if inhabil o sab o dom%>
                <A href='javascript:seleccionaFecha("<%= sdf.format( cal.getTime() )%>")'><%= diaFormat.format( cal.getTime() )%></A>
        <%}//fin if inhabil o sab o dom%>
        </td>
    <%
        GregorianCalendar nextMonth =(GregorianCalendar) cal.clone();
        nextMonth.set( nextMonth.DAY_OF_MONTH,1);
        nextMonth.add( nextMonth.MONTH, 1);
        cal.add( cal.DAY_OF_MONTH, 1 );
        //if fin de mes o fecha final
        if( cal.equals ( fechaMax) || ( cal.equals( nextMonth ) ) ){
            int end = -1;
            switch ( cal.get( cal.DAY_OF_WEEK) ){
                case GregorianCalendar.SUNDAY:
                    end =0;
                    %></tr></table><%
                    break;

                case GregorianCalendar.MONDAY:
                    end = 6;break;
                case GregorianCalendar.TUESDAY:
                    end = 5;break;
                case GregorianCalendar.WEDNESDAY:
                    end = 4;break;
                case GregorianCalendar.THURSDAY:
                    end = 3;break;
                case GregorianCalendar.FRIDAY:
                    end = 2;break;
                case GregorianCalendar.SATURDAY:
                    end = 1;break;
            }
            for(int c = 0; c< end;++c){%>
                <td>&nbsp;</td><%
                if(c +1 == end){%>
            </tr></table>
                <%}
            }
        }
  // if fin de semana
        if(  ( cal.SUNDAY == cal.get ( cal.DAY_OF_WEEK ) ) ){
    %>

        </tr>
        <%}//fin if fin de semana

    }
}
%>

</TABLE>
<TABLE width='300' border='0' cellspacing='0' cellpadding='0'>
    <TR>
        <TD class='tittabdato' height=5>.
            <!-- <IMG src='/gifs/website/gau10010.gif' width='1' height='5'> -->
        </TD>
    </TR>
</TABLE> <BR>

<%-- Boton de cerrar --%>
<table width="300" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center"><a href="javascript:self.close();"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0" alt="Cerrar"></a>
        </td>
    </tr>
</table>

</body>
</html>