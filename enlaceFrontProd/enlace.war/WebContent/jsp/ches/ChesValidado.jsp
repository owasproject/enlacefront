<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.ches.*,java.text.SimpleDateFormat, mx.altec.enlace.servlets.*"%><%--
Nombre: ChesArchivoRecup.jsp
@author Rafael Martinez Montiel
Muestra el resultado de la validacion y permite enviar el archivo
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />
<!-- jsp:useBean id="numRegistros" scope="request" class="java.lang.Long" /-->
<jsp:useBean id="fechaEnvio" scope="request" class="java.lang.String" />

<%
   Long numRegistros = (Long)request.getAttribute("numRegistros");
 %>

<%@page import="mx.altec.enlace.bo.BaseResource;"%>
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language='javascript'>
function js_enviar(){
	<%
	BaseResource ses = (BaseResource) request.getSession ().getAttribute ("session");
  	if (ses.getToken().getStatus() == 1) {%>
  		respuesta = 1;
  		continua();
	<%} else {%>
	cuadroDialogo("Est&aacute; seguro que desea enviar este archivo?",2);
	<%}%>

    //document.frmRegistro.opcion.value = '<%= ChesRegistro.ENVIAR %>';
    //document.frmRegistro.submit();

}

var respuesta;

function continua(){
    if ( 1 == respuesta ){
        document.frmRegistro.opcion.value = '<%= ChesRegistro.ENVIAR %>';
        document.frmRegistro.submit();
    }
}
</SCRIPT>
<script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
<form  name="frmRegistro" action="ChesRegistro" method="POST" >
    <INPUT type='hidden' name='opcion' value=''>
    <table align="center" cellpadding='2'>
        <tr class="tabfonazu" >
            <th class="tittabdat" >Fecha de envio
            <th class="tittabdat" >N&uacute;mero de registros
        </tr>
        <tr class="textabdatcla">
            <td class="tabmovtex" align="center"><%= fechaEnvio %></td>
            <td class="tabmovtex" align="center"><%= numRegistros %></td>
        </tr>
        <tr>
            <td colspan='2' align="center">
                <A href = "javascript:js_enviar();"><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
                <A href='ChesRegistro?opcion=0'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
            </TD>
        </TR>
    </table>
</form>
</body>
</html>