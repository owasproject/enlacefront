<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.ches.*, mx.altec.enlace.servlets.*"%><%--
Nombre: ChesResultadoEnvio.jsp
@author Rafael Martinez Montiel
Muestra el resultado de la importacion exitosa
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />
<jsp:useBean id="chesArchivo" scope="session" class="mx.altec.enlace.ches.ChesArchivoImportado" />
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

   <script language="javascript">

function js_resultado(){

        cuadroDialogo("Se registraron correctamente: <%= chesArchivo.getNumRegistros() %> registros",1);


}
</script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');
            js_resultado();"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>
<TABLE align="center" >
    <TR>
        <TD>
            <TABLE align="center" cellpadding='2'>
                <TR class="tabfonazu">
                    <TH class="tittabdat" >Fecha de env&iacute;o</th>
                    <TH class="tittabdat" >Nombre de archivo</th>
                    <TH class="tittabdat" >N&uacute;mero de Secuencia</th>
                    <TH class="tittabdat" >N&uacute;mero de registros</th>
                    <TH class="tittabdat" >Importe total</th>
                    <TH class="tittabdat" >Estatus</th>
                </TR>
                <TR class="textabdatcla">
                    <TD class="tabmovtex" align="center">
                        <%= new java.text.SimpleDateFormat("dd/MM/yyyy").format(chesArchivo.getFechaEnvio().getTime()) %>
                    </TD>
                    <TD class="tabmovtex" align="center">
                        <%= chesArchivo.getFileName() %>
                    </TD>
                    <TD class="tabmovtex" align="center">
                        <%= chesArchivo.getNumSecuencia() %>
                    </TD>
                    <TD class="tabmovtex" align="center">
                        <%= chesArchivo.getNumRegistros() %>
                    </TD>
                    <TD class="tabmovtex" align="center">
                        <%= chesArchivo.importeTotalAsString() %>
                    </TD>
                    <TD class="tabmovtex" align="center">
                        <%= chesArchivo.getStatus() %>
                    </TD>
                </TR>
            </TABLE>
        </td>
    </tr>
    <tr>
        <td align="center">

            <a href=' javascript: self.print()' border='0'><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a>
            <A href='ChesRegistro?opcion=0'><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
        </TD>
    </TR>
</table>
<% session.removeAttribute( "chesArchivo" ); %>

</body>
</html>
