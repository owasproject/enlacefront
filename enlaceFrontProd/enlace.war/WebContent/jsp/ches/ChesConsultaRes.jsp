<html>
<head>
	<title>Cheque Seguridad-  Resultado de Consulta y Cancelaci&oacute;n</TITLE>
<%-- Modificacion Rafael Martinez, para permitir la impresion en Netscape --%>
<%--<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">--%>
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript" src="/EnlaceMig/cuadroDialogo.js"></script>

<script languaje="javaScript">
  var Indice=0;
  var registrosTabla=new Array();
  var camposTabla=new Array();
  var totalCampos=0;
  var totalRegistros=0;
  <%= request.getAttribute("ArchivoErr") %>

function VentanaAyuda(cad)
{
   ventanaAyuda=window.open('','trainerWindow','width=500,height=350,toolbar=no,scrollbars=auto');
   ventanaAyuda.document.open();
   ventanaAyuda.document.write("<html>");
   ventanaAyuda.document.write("\n<head>\n<title>Ayuda</title>\n</head>");
   ventanaAyuda.document.write("\n<body bgcolor='white'>");
   ventanaAyuda.document.write("\n<center>");
   ventanaAyuda.document.write("\n<table border=0 width=460>");
   ventanaAyuda.document.write("\n<form>");
   ventanaAyuda.document.write("\n<tr><th bgcolor='red'><font color='white'>Ayuda - Depositos Interbancarios</font></th></tr>");
   ventanaAyuda.document.write("\n<tr><td align=center><br><br><p><b><br>No esta disponible en este momento</b><br><p><br>");
   ventanaAyuda.document.write("\n</td></tr>");
   ventanaAyuda.document.write("\n<tr><td align=center><br><input type=button value=' Cerrar ventana ' onClick='window.close();'></td></tr>");
   ventanaAyuda.document.write("\n</form>");
   ventanaAyuda.document.write("\n</table>");
   ventanaAyuda.document.write("\n</center><p>");
   ventanaAyuda.document.write("\n</body>\n</html>");
   ventanaAyuda.focus();
}

//============================================================================================



<!---------------------------------------------------------------------------------------------->

<%= request.getAttribute("newMenu") %>

<!---------------------------------------------------------------------------------------------->

var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;
<%-- Modificacion para correccion de paginacion --%>
function adelante(){
    var next = document.frmConsultaRes.next.value;
    var prev = document.frmConsultaRes.prev.value;
    var total = document.frmConsultaRes.total.value;

    if ( parseInt(next) < parseInt (total) ){
        document.frmConsultaRes.prev.value = next;
        if ( parseInt(next) + Nmuestra >= parseInt (total) ) {
            document.frmConsultaRes.next.value = total;
        } else {
            document.frmConsultaRes.next.value = parseInt(next) + Nmuestra;
        }
        document.frmConsultaRes.action = "ChesConsulta?ventana=1";
        document.frmConsultaRes.submit();
    } else {
        cuadroDialogo("no hay mas registros",3);
    }
}

<%-- Modificacion Rafael Martinez Correci�n de Paginacion--%>
function atras() {
    var next = document.frmConsultaRes.next.value;
    var prev = document.frmConsultaRes.prev.value;
    var total = document.frmConsultaRes.total.value;
    if ( 0 != parseInt(prev) ) {
        document.frmConsultaRes.next.value = prev;
        document.frmConsultaRes.prev.value = prev - Nmuestra;

        document.frmConsultaRes.action = "ChesConsulta?ventana=1";
        document.frmConsultaRes.submit();
    } else {
        cuadroDialogo("no hay mas registros",3);
    }
}

function SeleccionaTodos()
 {
   forma=document.frmConsultaRes;

   for(i2=0;i2<forma.length;i2++)
   if(forma.elements[i2].type=='checkbox' || forma.elements[i2].name!='todos' )
         forma.elements[i2].checked = forma.todos.checked;
 }


function ValidaCuentas(forma)
{
   var cont=0;
   var str="";

   forma.cheque.value = "";

   for(i2=0;i2<forma.length;i2++)
    {
      if(forma.elements[i2].type=='checkbox' && forma.elements[i2].name!='todos' )
       {
         if(forma.elements[i2].checked==true)
          {
		   forma.cheque.value+=forma.elements[i2].name;
           str+="1";
           cont++;
          }
         else
          str+="0";
       }
    }
   if(cont==0)
    {
      cuadroDialogo("Debe Seleccionar m�nimo una cuenta.",3);
    }
	else
	{
	forma.submit();
	}
 }

//============================================================================================

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


<%= request.getAttribute("VarFechaHoy") %>

diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';


function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;
   if (Indice == 0){
		msg=window.open("/EnlaceMig/EI_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
   msg.focus();
  }
	if(Indice==1){
		msg=window.open("/EnlaceMig/chesCalendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
	    msg.focus();
	}
 }
function Actualiza()
 {
   if(Indice==0)
     document.frmConsulta.txtDeFecha.value=Fecha[Indice];
   else
     document.frmConsulta.txtAFecha.value=Fecha[Indice];
 }

function VerificaFechas(txtDeFecha, txtAFecha)
 {

 lib_year         = parseInt(txtDeFecha.value.substring(6,10),10);
 lib_month        = parseInt(txtDeFecha.value.substring(3, 5),10);
 lib_date         = parseInt(txtDeFecha.value.substring(0, 2),10);
 DeFecha = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtAFecha.value.substring(6,10),10);
 lim_month    = parseInt(txtAFecha.value.substring(3, 5),10);
 lim_date     = parseInt(txtAFecha.value.substring(0, 2),10);
 AFecha  = new Date(lim_year, lim_month, lim_date);

     if( DeFecha > AFecha)
      {
        //alert("La Fecha inicial del cheque no puede ser mayor a la Fecha Final.");
		cuadroDialogo("La fecha inicial del cheque no puede ser mayor a la fecha final", 1);
        return false;
      }

  return true;

 }

function BotonPagina(sigant)
 {
   var tot=0;
   tot=document.tabla.Pagina.value;

   if(sigant==0)
    tot--;
   if(sigant==1)
    tot++;
   if(sigant==2)
    tot=0;
   if(sigant==3)
    tot=(document.tabla.NumPag.selectedIndex);

   document.tabla.Pagina.value=tot;
   document.tabla.submit();
 }


function InsertaReg(forma)
  {
    var tot=0;
    var var1="";


    dCuentaCargo=forma.cboCuentaCargo.options[forma.cboCuentaCargo.selectedIndex].value;


    dNumCheque=forma.txtNumCheque.value;

    dImporte=forma.txtImporte.value;
    if(dImporte.indexOf('.')<0)
      dImporte+=".00";

    dDeFecha=forma.txtDeFecha.value;
    dAFecha=forma.txtAFecha.value;

    if(forma.TransReg.value=="" || forma.TransReg.value=="null")
     {
       var1="";
     }

   dCveBeneficiario = forma.cboBeneficiario.options[forma.cboBeneficiario.selectedIndex].value;

   if( dCveBeneficiario != "0")
    {
      dBeneficiario = forma.cboBeneficiario.options[forma.cboBeneficiario.selectedIndex].text;
    }
   else
    {
      dBeneficiario = forma.txtBeneficiario.value;
    }

   var1=forma.TransReg.value+dCuentaCargo+"|"+dNumCheque+"|"+dCveBeneficiario+"|"+dBeneficiario+"|"+dImporte+"|"+dFechaLibramiento+"|"+dFechaLimite+"|"+"@";

   forma.TransReg.value=var1;

  }



/**********************************************************************************/
// Funciones para deplegar la ventana con la informacion de la cuenta

var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

function despliegaDatos(campo2,indice)
 {
   // cadena de Transferencias, indicel del registro
   numeroRegistros(campo2);
   llenaTabla(campo2);
   nueva(indice);
   return false;
 }

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
      //alert("Registros encontrados: "+ totalRegistros);
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    strCuentas=campo2;
    var vartmp="";

    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
    separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
              vartmp=regtmp.substring(0,regtmp.indexOf('|'));
              camposTabla[i][j]=vartmp;
              if(regtmp.indexOf('|')>0)
                regtmp=regtmp.substring(regtmp.indexOf('|')+1,regtmp.length);
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }


</script>
<SCRIPT LANGUAGE="JavaScript">
<!--
function ChecaForma(){
//alert("Si se va aqui");
	if(this.frmConsultaRes.opcion.value=""){
		cuadroDialogo("No ha seleccionado ning&uacute;n cheque", 1);
		location.reload()
	}
	else {
		cuadroDialogo("A punto de cancelar el cheque seleccionado", 2);
		location.reload()
	}
//	location.reload()
frmConsulta.ventana.value=1;
}
//-->
</SCRIPT>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript"src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
background="/gifs/EnlaceMig/gfo25010.gif">

<!---------------------------------- Menu principal y Encabezado ------------------------------->

<table width="571" border="0" cellspacing="0" cellpadding="0">
  <tr valign=top>
    <td width="*">
		<!-- MENU PRINCIPAL -->
		<%= request.getAttribute("MenuPrincipal") %>
    </td>
  </tr>
</table>
<!-- #EndLibraryItem --><!-- #BeginEditable "Contenido" -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="676" valign="top" align="left">
      <table width="666" border="0" cellspacing="6" cellpadding="0">
        <tr>
          <td width="528" valign="top" class="titpag"><%= request.getAttribute("Encabezado") %></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<!-------------------------------- termina Menu principal y Encabezado --------------------------->

<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="frmConsultaRes" onSubmit="return ValidaCuentas(this);"  method=post action="ChesCancelar">
    <tr>
      <td align="center">
        <table width="760" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <tr>
            <td class="textabref" colspan="7">Total
              de registros:&nbsp;<%= request.getAttribute("total") %>
              <%-- Modificacion para solucion de incidencia [INC_PROD]:<ENLCON348>/Enlace Internet/Servicios
                    Rafael Martinez Montiel 01/10/2002--%>
                    Cuenta: <%=session.getAttribute ( "cuenta_inicio" )%>
              <%-- Fin Modificacion--%>
			</td>
          </tr>
          <tr> <%-- Modificacion Rafael Martinez 10/10/2002 Ocultar las casillas de seleccion en cheques cancelados--%>
          <% if( request.getAttribute("ChesCancelados") == null || !request.getAttribute("ChesCancelados").equals("true") ) {%>
                <td width="70" align="right" class="tittabdat">Todos
                  <input type="checkbox" name="todos" value="checkbox" onClick="SeleccionaTodos();" >
                </td>
                <% } %>
            <td align="center" class="tittabdat" width="100">No.de cheque</td>
            <td class="tittabdat" align="center" width="150">Beneficiario</td>
            <td class="tittabdat" align="center" width="100">Importe</td>
            <td class="tittabdat" align="center" width="100">Fecha de recepci&oacute;n</td>
            <td class="tittabdat" align="center" width="100">Fecha de libramiento</td>
            <td class="tittabdat" align="center">Fecha  l&iacute;mite  de pago</td>
          </tr>

		  <%= request.getAttribute("consulta") %>

        <br>
        <table width="233" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="22">
          <tr>
            <% if( request.getAttribute("ChesCancelados") == null || !request.getAttribute("ChesCancelados").equals("true") ) {%>
            <td align="right" valign="top" height="22" width="85"><a href="javascript:ValidaCuentas(document.frmConsultaRes);">
              <img border="0" name="cancelar" src="/gifs/EnlaceMig/gbo25190.gif" width="85" height="22" alt="Cancelar"></a>
            </td>
            <%}%>
            <td align="center" valign="top" height="22" width="85"><%= request.getAttribute("lnkExportar") %>
            </td>
            <td align="left" valign="top" height="22" width="83"><a href="javascript:self.print()">
              <img border="0" name="boton22" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Imprimir"></a>
            </td>
          </tr>
        </table>

        <br>
      </td>
    </tr>
	<input type=hidden name=cheque>
  </form>
</table>
</BODY>
</HTML>
