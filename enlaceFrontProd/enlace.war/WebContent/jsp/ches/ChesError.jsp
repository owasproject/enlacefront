<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.ches.*, mx.altec.enlace.servlets.*"%><%--
Nombre: ChesError
@author Rafael Martinez Montiel
Muestra una pagina de error sin centrar el contenido
--%><html>
<head><title>Chequera de Seguridad</title>
<jsp:useBean id="encabezado" scope="request" class="java.lang.String" />
<jsp:useBean id="baseResource" scope="request" class="mx.altec.enlace.bo.BaseResource" />
<jsp:useBean id="Error" scope="request" class="java.lang.String" />
<jsp:useBean id="URL" scope="request" class="java.lang.String" />
<jsp:useBean id="ErrorFileName" scope="request" class="java.lang.String" />
<meta http-equiv="Content-Type" content="text/html">
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
    <script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
    <script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
    <script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
    <script language='javascript'>
// Funciones de menu ===========================================================
<%= baseResource.getFuncionesDeMenu() %>
/******************  Esto no es mio ****************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************/

// Fin Funciones de menu =======================================================
    </script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
    onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif',
			'/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif',
			'/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
			'/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
			'/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
			'/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
			'/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
			'/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
			background="/gifs/EnlaceMig/gfo25010.gif">
    <%= baseResource.getStrMenu() %>
    <%= encabezado %>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="package.class" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>

<table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
 </table>

 <table width="400" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
   <td align="center">
     <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
	   <tr>
	     <td colspan="3"> </td>
	   </tr>
	   <tr>
	     <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
		 <td width="428" height="100" valign="top" align="center">
		   <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
			 <tr>
			   <td colspan="3">&nbsp;</td>
			 </tr>
			 <tr>
			   <td class="tittabcom" align="center">
<%= Error %>
			   </td>
			 </tr>

		   </table>
		 </td>
         <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." 	name=".."></td>
	   </tr>
	 </table>
	</td>
  </tr>
 </table>

 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
  </tr>
  <tr>
    <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
  <tr>
    <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
  </tr>
 </table>

 <table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td align="center">
	 <br>
	 <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
	  <tr>
        <% if (!ErrorFileName.trim().equals("")){%>
	    <td align="center">
                <a href="/Download/<%= ErrorFileName %>" ><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consulta" width="90" height='22'></a>
         </td>
         <%}%>
         <% if( !URL.equals("") ){%>
         <td align="center">
            <a href ="<%= URL %>"><img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
		</td>
        <%}%>
	  </tr>
	 </table>
	</td>
  </tr>
 </table>

</body>
</html>
