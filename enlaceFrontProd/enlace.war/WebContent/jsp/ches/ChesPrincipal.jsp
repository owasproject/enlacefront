<html>
<head>
<title>Enlace</title>

<meta http-equiv="Content-Type" content="text/html">

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<SCRIPT LANGUAGE="Javascript">
<!--


var respuesta = 0;
var campoTexto = "";

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function js_nuevo(){

     if(document.ChesPrincipal.FacArchivo.value != "true")
		  cuadroDialogo("No tiene facultad para dar de alta archivos de cheques", 3);
	  else
	   {
		  document.ChesPrincipal.operacion.value="nuevo";
		  cuadroCaptura("Proporcione el nombre del archivo a crear: ", "Nombre del archivo de Cheques" );
	   }
}

function continuaCaptura()
{
	if(document.ChesPrincipal.operacion.value=="recuperar")
	 {
		if (campoTexto!=null)
		 {
			if (isNaN(campoTexto) || campoTexto=="")
				cuadroDialogo("Proporcione un valor num�rico para la secuencia", 3);
			else
			 {
				document.ChesPrincipal.secuencia.value=campoTexto;
				document.ChesPrincipal.action="ChesRecuperar"
				document.ChesPrincipal.submit();
			 }
		 }
	 }
	else if(document.ChesPrincipal.operacion.value=="nuevo")
	 {
		if (campoTexto=="")
			cuadroDialogo("Proporcione un nombre para el archivo", 3);
		else
		 {
			if(respuesta == 1)
			 {
				if (campoTexto=="")
				 {
					cuadroDialogo("Proporcione un nombre para el archivo", 3);
				 }
				else
				 {
					document.ChesPrincipal.archivo_actual.value=campoTexto;
		            document.ChesPrincipal.TransReg.value="";
		            document.ChesPrincipal.operacion.value="inicio";
                    document.ChesPrincipal.submit();
				 }
			}
		 }
 	 }

}

function js_borrar(){
	if (document.ChesPrincipal.archivo_actual.value=="")
		cuadroDialogo("Antes de seleccionar esta operacion, debe crear o importar un archivo", 3);
	else
	 {
	    if(document.ChesPrincipal.FacArchivo.value != "true")
			cuadroDialogo("No tiene facultad para borrar archivos de cheques", 3);
		else
			cuadroDialogo("Esta seguro que desea eliminar este archivo ?", 2);
	 }
}

function continua()
 {
    if (respuesta==1)
	  {
	  	  document.ChesPrincipal.archivo_actual.value="";
		  document.ChesPrincipal.TransReg.value="";
		  document.ChesPrincipal.operacion.value="inicio";
          document.ChesPrincipal.submit();
	  }
 }

function js_envio(){

	if(document.ChesPrincipal.archivo_actual.value=="")
 	   cuadroDialogo("Antes de seleccionar esta operacion, debe crear o importar un archivo", 3);
	else
	 {
	    if(document.ChesPrincipal.FacEnviar.value != "true")
 		   cuadroDialogo("No tiene facultad para enviar archivos de cheques", 3);
	   else
	     {
	        if(document.ChesPrincipal.tipoArchivo.value=="importado")
	         {
	           document.ChesPrincipal.action="ChesEnviar"
	           document.ChesPrincipal.submit();
		     }
			else if(ValidaTabla(document.ChesPrincipal))
	         {
	           document.ChesPrincipal.action="ChesEnviar"
	           document.ChesPrincipal.submit();
		     }
		 }
	 }

}

function js_eliminar(){
var seleccionado = false;

 if(document.ChesPrincipal.tipoArchivo.value=="importado")
  cuadroDialogo("Imposible ejecutar esta operacion en un archivo importado", 3);
 else
  {
	if (document.ChesPrincipal.archivo_actual.value=="")
		cuadroDialogo("Antes de seleccionar esta operacion, debe crear un archivo", 3);
	else
	 {
		 if(document.ChesPrincipal.FacArchivo.value != "true")
			cuadroDialogo("No tiene facultad para borrar registros de cheques", 3);
		 else
		  {
		 	 for(i=0;i<(document.ChesPrincipal.elements.length);i++)
			   {
		  	     if (document.ChesPrincipal[i].type=="radio")
			      {
				     if(document.ChesPrincipal[i].checked)
				      {
					    seleccionado=true;
					    break;
					  }
			      }
			   }

			  if (seleccionado)
			   {
				  document.ChesPrincipal.operacion.value="baja";
				  document.ChesPrincipal.submit();
			   }
			  else
			   {
				  cuadroDialogo("Seleccione un registro por favor", 3);
			   }
		  }
	 }
  }
}

function js_modificacion(){
	var seleccionado =false;

 if(document.ChesPrincipal.tipoArchivo.value=="importado")
	cuadroDialogo("Imposible ejecutar esta operacion en un archivo importado", 3);
 else
  {
	if (document.ChesPrincipal.archivo_actual.value=="")
		cuadroDialogo("Antes de seleccionar esta operacion, debe crear un archivo", 3);
	else
	 {
		if(document.ChesPrincipal.FacArchivo.value != "true")
			cuadroDialogo("No tiene facultad para modificar registros de cheques", 3);
		else
		 {
			for(i=0;i<(document.ChesPrincipal.elements.length);i++) {
		  		if (document.ChesPrincipal[i].type=="radio")
				{
					if(document.ChesPrincipal[i].checked){
						seleccionado=true;
						break;
					}
				}
			}
			if (seleccionado){
				document.ChesPrincipal.operacion.value="modificacion";
				document.ChesPrincipal.submit();
			}
			else{
				cuadroDialogo("Seleccione un registro por favor", 3);
			}
		 }
	 }
  }
}


function js_alta(){

 if(document.ChesPrincipal.tipoArchivo.value=="importado")
	cuadroDialogo("Imposible ejecutar esta operacion en un archivo importado", 3);
 else
  {
	if (document.ChesPrincipal.archivo_actual.value=="")
		cuadroDialogo("Antes de seleccionar esta operacion, debe crear un archivo", 3);
	else
	 {
		if(document.ChesPrincipal.FacArchivo.value != "true")
			cuadroDialogo("No tiene facultad para dar de alta cheques", 3);
		else{
			document.ChesPrincipal.operacion.value="alta";
			document.ChesPrincipal.submit();
		}
	 }
  }
}

function js_importar(){

 archivo = "";

 if(document.ChesPrincipal.archivo_actual.value!="")
  {
     if(document.ChesPrincipal.tipoArchivo.value=="importado")
		cuadroDialogo("No puede importar otro archivo", 3);
	 else
		cuadroDialogo("No puede importar un archivo despues de crear uno nuevo", 3);
  }
 else
  {
	 if(document.ChesPrincipal.FacArchivo.value != "true")
		cuadroDialogo("No tiene facultad para importar archivos de cheques", 3);
	 else
	  {
		if(document.ChesPrincipal.Archivo.value=="")
			cuadroDialogo("Seleccione primero el archivo a importar con el boton Browse ...", 3);
		else{
			document.ChesPrincipal.operacion.value="importar";
			archivo=document.ChesPrincipal.Archivo.value;
			document.ChesPrincipal.archivo_actual.value=archivo.substring(archivo.lastIndexOf('\\')+1, archivo.length);
			document.ChesPrincipal.submit();
		}
	  }
  }

}

function js_recupera(){
		document.ChesPrincipal.operacion.value="recuperar";
		cuadroCaptura("Proporcione el n&uacute;mero de transmisi&oacute;n", "N&uacute;mero de transmisi&oacute;n");
}

// RFV

function despliegaEstatus()
 {
   ventanaInfo1=window.open('','trainerWindow','width=460,height=230,toolbar=no,scrollbars=yes');
   ventanaInfo1.document.open();
   ventanaInfo1.document.write("<html>");
   ventanaInfo1.document.writeln("<head>\n<title>Estatus</title>\n");

   ventanaInfo1.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
   ventanaInfo1.document.writeln("<script language='javascript' src='/EnlaceMig/scrImpresion.js'></script>");
   ventanaInfo1.document.writeln("</head>");
   ventanaInfo1.document.writeln("<body bgcolor='white'>");
   ventanaInfo1.document.writeln("<form>");
   ventanaInfo1.document.writeln("<table border=0 width=420 class='textabdatcla' align=center>");
   ventanaInfo1.document.writeln("<tr><th class='tittabdat'>Informaci&oacute;n de la Operaci&oacute;n</th></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1' align=center>");

   ventanaInfo1.document.writeln("<table border=0 align=center><tr><td class='tabmovtex1' align=center>");
   ventanaInfo1.document.writeln("<img src='" + document.ChesPrincipal.imgResumen.value + "'></td>");
   ventanaInfo1.document.writeln("<td class='tabmovtex1'><br>" + document.ChesPrincipal.resumenArchivo.value  + "<br><br>");
   ventanaInfo1.document.writeln("</td></tr></table>");

   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("<tr><td class='tabmovtex1'>");
   ventanaInfo1.document.writeln(document.ChesPrincipal.listaErrores.value+"<br><br>");
   ventanaInfo1.document.writeln("</td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("<table border=0 align=center >");
   ventanaInfo1.document.writeln("<tr><td align=center><br><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25200.gif' border=0></a></td></tr>");
   ventanaInfo1.document.writeln("</table>");
   ventanaInfo1.document.writeln("</form>");
   ventanaInfo1.document.writeln("</body>\n</html>");
   ventanaInfo1.focus();
}


function despliegaArchivoErrores()
 {

   msg=window.open("<% if(request.getAttribute("paginaErrores") != null) out.print(request.getAttribute("paginaErrores")); %>", "winErrores","width=460,height=200,toolbar=no,scrollbars=yes");

   msg.focus();

 }


function ValidaTabla(forma)
 {
   var cont=0;

   for(i2=0;i2<forma.length;i2++)
    {
      if(forma.elements[i2].type=='radio')
       {
		   cont++;
       }
    }
   if(cont==0)
    {
	  cuadroDialogo("Debe capturar m&iacute;nimo un Cheque.", 3);
      return false;
    }

   return true;

 }

<%= request.getAttribute("newMenu") %>

//-->
</SCRIPT>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= request.getAttribute("despliegaEstatus" ) %>"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado" ) %>


<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="ChesPrincipal" METHOD="POST" ACTION="ChesRegistro">
    <tr>
      <td align="center">
        <table width="680" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td rowspan="2">
              <table width="460" border="0" cellspacing="2" cellpadding="3">
                <tr>
                  <td class="tittabdat" colspan="2"> Datos del archivo</td>
                </tr>
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="450" border="0" cellspacing="0" cellpadding="0">
                      <tr valign="top">
                        <td width="270" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Archivo:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <input type="text" name="archivo_actual" size="22" class="tabmovtex" value="<% if(request.getAttribute("archivo_actual" )!= null) out.print(request.getAttribute("archivo_actual")); %>"  onFocus='blur()'>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>N&uacute;mero de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
                                <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("transmision")!=null) out.print(request.getAttribute("transmision" )); %>" NAME="transmision" onFocus='blur();'>
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Fecha de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaTransmision")!=null) out.print(request.getAttribute("fechaTransmision" )); %>"  NAME="fechaTrans" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Fecha
                                de actualizaci&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("fechaActualizacion" ) != null) out.print(request.getAttribute("fechaActualizacion" ));  %>" NAME="fechaAct" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Importe
                                de transmisi&oacute;n:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("importeEnviado" ) != null) out.print( request.getAttribute("importeEnviado" )); %>"  NAME="importeTrans" onFocus='blur();' >
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td width="180" align="right">
                          <table width="150" border="0" cellspacing="5" cellpadding="0">
                            <tr>
                              <td class="tabmovtex" nowrap>Total de registros:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("regsEnviados" )!= null) out.print(request.getAttribute("regsEnviados")); %>" NAME="totRegs" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>Registros aceptados:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap>
								<INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("regsAceptados" )!= null) out.print(request.getAttribute("regsAceptados")); %>"  NAME="aceptados" onFocus='blur();' >
                              </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">Registros
                                rechazados: </td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
							  <INPUT TYPE="text" SIZE="22" ALIGN="rigth" class="tabmovtex" VALUE="<% if(request.getAttribute("regsRechazados" )!= null) out.print(request.getAttribute("regsRechazados")); %>"  NAME="rechazados" onFocus='blur();' >
                              </td>
                            </tr>
                            <!-- <tr>
                              <td class="tabmovtex" nowrap valign="middle">Registros
                                por transmitir:</td>
                            </tr>
                            <tr>
                              <td class="tabmovtex" nowrap valign="middle">
                                <input type="text" name="textfield323" size="22" class="tabmovtex">
                              </td>
                            </tr> -->
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width="200" height="94" align="center" valign="middle">
			  <A href = "javascript:js_nuevo();" border = 0><img src="/gifs/EnlaceMig/gbo25550.gif" border=0 alt="Crear archivo" width="115" height="22"></a>
            </td>
          </tr>
          <tr>
            <td align="center" valign="bottom">
              <table width="200" border="0" cellspacing="2" cellpadding="3">
                <tr align="center">
                  <td class="textabdatcla" valign="top" colspan="2">
                    <table width="180" border="0" cellspacing="5" cellpadding="0">
                      <tr valign="middle">
                        <td class="tabmovtex" nowrap>
                          <!-- <input type="radio" name="radiobutton" value="radiobutton"> -->
                          Importar archivo</td>
                      </tr>
                      <tr>
                        <td nowrap>
						  <input type="file" name="Archivo" size="15">
                        </td>
                      </tr>
                      <tr align="center">
                        <td class="tabmovtex" nowrap>
						  <A href = "javascript:js_importar();" border = 0><img src="/gifs/EnlaceMig/gbo25280.gif" border=0 alt="Aceptar" width="80" height="22"></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br>

		<%= request.getAttribute("Tabla") %>

        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="66">
			  <A href = "javascript:js_alta();" border=0><img src="/gifs/EnlaceMig/gbo25480.gif" border=0 alt="Alta" width="66" height="22"></a>
            </td>
            <td align="left" valign="top" width="127">
  			  <A href = "javascript:js_eliminar()();" border=0><img src="/gifs/EnlaceMig/gbo25500.gif" border=0 alt="Baja de registro" width="127" height="22"></a>
            </td>
            <td align="left" valign="top" width="93">
   			  <A href = "javascript:js_modificacion();" border=0><img src="/gifs/EnlaceMig/gbo25510.gif" border=0 alt="Modificar" width="93" height="22"></a>
            </td>
            <td align="left" valign="top" width="83">
  			  <A href = "javascript:self.print();" border=0><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="83" height="22"></a>
            </td>
          </tr>
        </table>
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="78">
   			  <A href = "javascript:js_envio();" border=0><img src="/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar" width="78" height="22"></a>
            </td>
            <td align="left" valign="top" width="97">
   			  <A href = "javascript:js_recupera();" border=0><img src="/gifs/EnlaceMig/gbo25530.gif" border=0 alt="Recuperar" width="97" height="22"></a>
            </td>
            <td align="left" valign="top" width="77">
			  <A href = "javascript:js_borrar();" border=0><img src="/gifs/EnlaceMig/gbo25540.gif" border=0 alt="Borrar" width="77" height="22"></a>
            </td>
            <td align="left" valign="top" width="85">
			  <% if(request.getAttribute("lnkExportar") !=null) out.print(request.getAttribute("lnkExportar")); %>
            </td>
          </tr>
        </table>

        <br>
      </td>
    </tr>
	<INPUT TYPE="hidden" VALUE="0" NAME="operacion">
	<INPUT TYPE="hidden" VALUE="" NAME="secuencia">
	<INPUT TYPE="hidden" VALUE="<% if(request.getAttribute("tipoArchivo" ) != null) out.print(request.getAttribute("tipoArchivo" )); %>" NAME="tipoArchivo">
	<input type=hidden name=listaErrores value="<% if( request.getAttribute("listaErrores") !=null) out.print(request.getAttribute("listaErrores")); %> ">
	<input type=hidden name=resumenArchivo value="<% if(request.getAttribute("resumenArchivo" ) != null) out.print(request.getAttribute("resumenArchivo" )); %> ">
	<input type=hidden name=imgResumen value="<% if(request.getAttribute("imgResumen" ) != null) out.print(request.getAttribute("imgResumen" )); %> ">
	<input type=hidden name=TransReg value="<% if( request.getAttribute("TransReg"  ) !=null ) out.print(request.getAttribute("TransReg"  )); %>">
	<INPUT type=hidden name=tramaCuentas value="<% if(request.getAttribute("tramaCuentas")!=null) out.print(request.getAttribute("tramaCuentas")); %>">
	<INPUT type=hidden name=tramaBeneficiarios value="<% if( request.getAttribute("tramaBeneficiarios")!= null) out.print(request.getAttribute("tramaBeneficiarios")); %>">
	<INPUT type=hidden name=FacEnviar value="<% if(request.getSession().getAttribute("FacEnviar") != null) out.print(request.getSession().getAttribute("FacEnviar")); %>">
	<INPUT type=hidden name=FacArchivo value="<% if( request.getSession().getAttribute("FacArchivo") != null) out.print(request.getSession().getAttribute("FacArchivo")); %>">
  </form>
</table>

</BODY>

</HTML>

