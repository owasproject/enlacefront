<%@page contentType="text/html" buffer="none" import="mx.altec.enlace.ches.*,java.util.*,java.text.*, mx.altec.enlace.servlets.*" %>
<%-- @ include file   = "verify_log.jsp" --%>
<html>
<head>
<title>Enlace</title><!-- ChesLinea.jsp -->
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript">
  var Indice=0;
/******************  Esto no es mio ***************************************************/
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
/*************************************************************************************/
/*
 6 N Requerido Numerico
 5 T Requerido Texto
 3 S Reqerido Seleccion
 2 n Numerico
 1 t Texto
 0 i Ignorado
*/
 errores = new Array(" La CUENTA DE CARGO",
                     " La CUENTA DE CARGO ( HIDDEN )",
                     " El N&uacute;MERO DE CHEQUE",
                     " El IMPORTE",
                     " La FECHA DE LIBRAMIENTO",
                     " La FECHA L&iacute;MITE");
 evalua = new Array("TXXXXX", "TXNNTX");
<%= request.getAttribute("VarFechaHoy") %>
 diasInhabiles = '<%= request.getAttribute("diasInhabiles" ) %>';
function SeleccionaFecha(ind)
 {
   var m=new Date();
   n=m.getMonth();
   Indice=ind;
   msg=window.open("/EnlaceMig/chesCalendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");
   msg.focus();
 }
function Actualiza()
 {
   if(Indice==0)
     document.frmLinea.txtFechaLibramiento.value=Fecha[Indice];
   else
     document.frmLinea.txtFechaLimite.value=Fecha[Indice];
 }
function VerificaFechas(txtLibramiento, txtLimite)
 {
 lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
 lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
 lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
 fechaLibramiento = new Date(lib_year, lib_month, lib_date);
 lim_year     = parseInt(txtLimite.value.substring(6,10),10);
 lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
 lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
 fechaLimite  = new Date(lim_year, lim_month, lim_date);
 //alert ("fechaLimite= "+fechaLimite)
     if( fechaLibramiento > fechaLimite)
      {
        //alert("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.");
		cuadroDialogo("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.", 3);
        return false;
      }
  return true;
 }
function ValidaForma(forma)
 {	

    if(!ValidaTodo(forma,evalua[0],errores))
       {
		  return false;
       }
	 if(!ValidaTodo(forma,evalua[1],errores))
        {
		   return false;
        }

	<!-- Se agrega Validacion para la fecha limite de pago: 22/09/2003 GSV -->
	var flimite = quitar_espacios(forma.txtFechaLimite.value);
	forma.hidFechaLimite.value="";
	 if (flimite=="" || flimite=="0000000000") forma.hidFechaLimite.value="31/12/9999";
	 else if (!validarfecha(forma.txtFechaLimite,"Fecha L&iacute;mite de Pago")) return false;
			else forma.hidFechaLimite.value=forma.txtFechaLimite.value;

     indicePunto = forma.txtImporte.value.indexOf('.');
 	 indice = forma.txtImporte.value.length;
	 strDecimal = forma.txtImporte.value.substring(indicePunto+1, indice);	
	 
	 var valor = "";
	 valor = forma.txtImporte.value;

	 if(indicePunto < 0)
        strEnteros = forma.txtImporte.value;
	 else
		strEnteros = forma.txtImporte.value.substring(0, indicePunto);

 
	 if( strEnteros.length > 14 || valor == "." )
	   {
		   cuadroDialogo("El IMPORTE no es un valor v&aacute;lido.", 3);
		   return false;
	   }

	 if(indicePunto>-1 &&  strDecimal.length > 2)
	 {
			cuadroDialogo("El IMPORTE debe tener DOS decimales.", 3);
			return false;
	 }


     if(!VerificaFechas(forma.txtFechaLibramiento, forma.hidFechaLimite))
         {
           return false;
         }
   return true;
 }

 function validarfecha(campo_fch_a_val,dato)
{
  var fch_a_val=campo_fch_a_val.value;

  if(fch_a_val.length!=10)
  {
    campo_fch_a_val.value=""; 
	campo_fch_a_val.focus();
	cuadroDialogo("Error en "+dato+", el formato de fecha correcto es: dd/mm/yyyy", 3); 
	return false;
  }
  var sep1=fch_a_val.substring(2,3);
  var sep2=fch_a_val.substring(5,6);
  if((sep1!="/")||(sep2!="/"))
  {	
      campo_fch_a_val.value=""; 
	  campo_fch_a_val.focus();
	  cuadroDialogo("Error en "+dato+", el formato de fecha correcto es: dd/mm/yyyy", 3); 
	  return false;
  }

  var sdia=fch_a_val.substring(0,2);
  var smes=fch_a_val.substring(3,5);
  var sanio=fch_a_val.substring(6,fch_a_val.length);

  if((isNaN(sdia))||(isNaN(smes))||(isNaN(sanio)))
  {
      campo_fch_a_val.value=""; 
	  campo_fch_a_val.focus();
	  cuadroDialogo("Error en "+dato+", el formato de fecha correcto es: dd/mm/yyyy", 3); 
	  return false;
  }			   
  var valido = true;

  dia=parseInt(sdia,10);
  mes=parseInt(smes,10);
  anio=parseInt(sanio,10);

  switch(mes)
  {
       case 1: 
	   case 3: 
	   case 5: 
	   case 7: 
	   case 8: 
	   case 10: 
	   case 12:if(dia>31)
		         valido = false;
               break;
       case 4: 
	   case 6: 
	   case 9: 
	   case 11:if(dia>30) 
		        valido = false;
               break;
       case 2: if(anio%4 == 0 && dia>29 || anio%4 != 0&& dia>28)
                valido = false;
              break;
       default: valido = false;
  }
  if(valido==false)
  { 
      campo_fch_a_val.value=""; 
	  campo_fch_a_val.focus();
	  cuadroDialogo(dato+" inv&aacute;lida", 3); 
  }
  return valido; 

}

function quitar_espacios(valor)
{
  var cadena="";
  for(i=0;i<valor.length;i++)
  { 
    if(valor.charAt(i)!=' ')
      cadena+=valor.charAt(i);
  }  

  return cadena; 
}

<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;
function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.frmLinea.cboCuentaCargo.value=ctaselec;
  document.frmLinea.textcboCuentaCargo.value=ctaselec;
}
<%-- Adicion RMM 20030214 modificación de calendario--%>
<%!
  SimpleDateFormat sdf = new SimpleDateFormat ( "dd/MM/yyyy" );
%>
var chesFecha ="";
var opt;

function calendarioPre( field, fechaInicial, campoFinal){
	var el = document.frmLinea.elements;
	c =0;
	while( el[c].name != campoFinal ){++c;}
	fechaFinal = el[c].value;
    calendario( field, fechaInicial, fechaFinal,true);
}

function calendarioPost( field, campoInicial, fechaFinal){    
	var el = document.frmLinea.elements;
	c =0;
	while( el[c].name != campoInicial ){++c;}
	fechaInicial = el[c].value;   
	calendario( field, fechaInicial, fechaFinal,false );
}
function calendarioPostInclusive( field, campoInicial, fechaFinal){
    var el = document.frmLinea.elements;
	c =0;
	while( el[c].name != campoInicial ){++c;}
	fechaInicial = el[c].value;   
	calendario( field, fechaInicial, fechaFinal,true );
}
function calendario( field, fechaInicial, fechaFinal, inclusiveEnd){
    opt = field;
    fechaMin = fechaInicial.slice(0,2) + fechaInicial.slice( 3,5) + fechaInicial.slice(6,10);
    fechaMax = fechaFinal.slice(0,2) + fechaFinal.slice( 3,5) + fechaFinal.slice(6,10);
    msg = window.open("ChesConsulta?modulo=calendario"
    + "&" + "fechaMin=" + fechaMin
    + "&" + "fechaMax=" + fechaMax 
    + "&" + "inclusiveEnd=" + inclusiveEnd, "Calendario",
    "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=400,height=390");
    msg.focus();
}


function continuaFecha(){
    var el = document.frmLinea.elements;
    c = 0;
    while ( el[c].name != opt ){
        ++c;
    }
    el[c].value = chesFecha;
}
<%-- Fin adicion RMM 20020214--%>

<%= request.getAttribute("newMenu") %>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</HEAD>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif',
        '/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif',
        '/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif',
        '/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif',
        '/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif',
        '/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif',
        '/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')"
    background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado" ) %>
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <FORM  NAME="frmLinea" method=post action="ChesLinea">
    <tr>
      <td  align="center">
        <table border="0" cellspacing="0" cellpadding="0">
        <tr>
		<td class="textabdatcla" >
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
                 <td class="tittabdat" colspan="3">&nbsp;&nbsp;Capture los datos del cheque</td>
            </tr>
			<tr>
			     <td><br></td>
			</tr>
			<tr>
			     <td  class="tabmovtex" colspan="3" align=left nowrap>&nbsp;&nbsp;Cuenta de cargo:
                 </td>   
			</tr>
			<tr >
                 <td class="tabmovtex">&nbsp;&nbsp;
	  	         <input type="text" name=textcboCuentaCargo  class="tabmovtexbol" maxlength=16 size=16 onfocus="blur();" value="">
				 <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" width=12 height=14 border=0 align=absmiddle></A>
				 <input type="hidden" name="cboCuentaCargo" value="" >
                 </td>
 			</tr>
			<tr>
		         <td><br></td>
			</tr>
            <tr>
			    <td class="tabmovtex" nowrap>&nbsp;&nbsp; N&uacute;mero de cheque:</td>
			    <td>&nbsp;</td>
       			<td class="tabmovtex" nowrap>Importe del cheque:</td>
			</tr>
            <tr >
                <td class="tabmovtex" nowrap>&nbsp;&nbsp;
		        <INPUT TYPE="text" SIZE="20" class="tabmovtex" maxlength="7" NAME="txtNumCheque">
                </td>
			    <td>&nbsp;</td>
                <td class="tabmovtex" nowrap>
		        <INPUT TYPE="text" SIZE="20" class="tabmovtex" NAME="txtImporte" MAXLENGTH="17">
                </td>
			</tr>
			<tr>
			     <td><br></td>
			</tr>
			<tr>
                <td class="tabmovtex" nowrap>&nbsp;&nbsp;Fecha de libramiento:</td>
			    <td>&nbsp;</td>
				<td class="tabmovtex" nowrap valign="middle">Fecha l&iacute;mite de pago:</td>
			</tr>
	        <tr>
			     <td class="tabmovtex" nowrap>&nbsp;&nbsp;
				 <input type=text name=txtFechaLibramiento size="20" class="tabmovtex" value="<%= request.getAttribute("FechaLibramiento" ) %>" onFocus='blur();' maxlength=10>
                 </td> 
				 <td>&nbsp;</td>
				 <td class="tabmovtex" nowrap valign="middle">
			     <input type=text name=txtFechaLimite size="20" class="tabmovtex" value="<%= request.getAttribute("fechaEnd") %>"  maxlength=10>
				 <input type="hidden" name="hidFechaLimite" value="" >
		         <%-- <A HREF="javascript:SeleccionaFecha(1);"><IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario"></A>&nbsp;&nbsp; --%>
               <%-- adicion RMM 20030214 --%>
                    <a href="javascript:calendarioPostInclusive('txtFechaLimite','txtFechaLibramiento','<%= request.getAttribute("FechaLimite") %>');">
                        <IMG SRC="/gifs/EnlaceMig/gbo25410.gif" BORDER=0 width="12" height="14" alt="Calendario">
                    </a>
               <%-- fin RMM 2030214 --%>
                 </td>
			</tr>
			<tr>
			<td class="tabmovtex" nowrap valign="middle" colspan="3"><br>Si desea una fecha l&iacute;mite mayor a 6 meses, especif&iacute;quela manualmente<br></td>
			</tr>

		</table>
		</td>
		</tr>
         </td>
          </tr>
        </table>

        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="left" valign="top" width="133">
  			  <A href = "javascript:if(ValidaForma(document.frmLinea)) document.frmLinea.submit();" border=0><img src="/gifs/EnlaceMig/gbo25560.gif" border=0 alt="Registrar cheque" width="133" height="22"></a>
            </td>
            <td align="left" valign="top" width="76">
			  <A href = "javascript:document.frmLinea.reset();" border=0><img src="/gifs/EnlaceMig/gbo25250.gif" border=0 alt="Limpiar" width="76" height="22"></a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
  <input type=hidden name=Modulo value=1>
 </form>
</table>
</BODY>
</HTML>