<!--%@ page import="EnlaceMig.*" %-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="mx.altec.enlace.bo.ArchivoConf"%>
<%@page import="mx.altec.enlace.dao.ProveedorConf"%>
<%@page import="mx.altec.enlace.utilerias.EIGlobal"%>

<%!
	public String redondearNum(String num, int decimales ) {
		if (num != null && num.length() > 0) {
			//double numero = Math.round(num*Math.pow(10,decimales))/Math.pow(10,decimales);
			double numero = Double.parseDouble(num);
			String d = "";
			int i = 0;
			d = "" + numero;
			i = (d.substring(d.indexOf("."), d.length())).length();
			if(i==1)
				d += "00";  
			else if(i==2)
				d += "0";
			return d;
		} else {
			return "";
		}
	}
%>

<html>
<head>
<title>Enlace</title>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js">		</script>
<script language="JavaScript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript" src="/EnlaceMig/scrImpresion.js">	</script>
<script language="JavaScript">

	// --------------------------------------------------------------------------------------
	function MM_preloadImages() {	//v3.0
		var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() {	//v3.0
		var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) {		//v3.0
		var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() {		//v3.0
		var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	<%= (String) request.getAttribute("newMenu") %>
	// --------------------------------------------------------------------------------------

	</script>

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	bgcolor="#ffffff"
	onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif'); <%= request.getAttribute("despliegaEstatus") %>"
	background="/gifs/EnlaceMig/gfo25010.gif">

<!-- MENU PRINCIPAL -->
<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top">
		<td width="*"><%=(String) request.getAttribute("MenuPrincipal")%></td>
	</tr>
</table>

<!-- ENCABEZADO -->
<%=(String) request.getAttribute("Encabezado")%>

<%
	//--------------------------------------------------------------------------------

	String transmision;
	String archivo;

	boolean impresionDetallada = false;

	EIGlobal.mensajePorTrace(
			"impresion Detallada al entrar a JSP getParameter->" + ""
			+ request.getParameter("impresionDetallada"),
			EIGlobal.NivelLog.DEBUG);
	EIGlobal.mensajePorTrace(
			"impresion Detallada al entrar a JSP getAttribute->" + ""
			+ request.getAttribute("impresionDetallada"),
			EIGlobal.NivelLog.DEBUG);

	String parImpresionDetallada = (String) request
			.getAttribute("impresionDetallada");
	if (parImpresionDetallada != null)
		impresionDetallada = parImpresionDetallada.equals("S");

	ArchivoConf objArchivo = (ArchivoConf) session
			.getAttribute("objetoArchivo");
	if (objArchivo == null)
		objArchivo = new ArchivoConf("");

	transmision = objArchivo.getSecuencia();
	archivo = objArchivo.getNombre();
	if (transmision.trim().equals(""))
		transmision = "sin transmitir";
	if (archivo.trim().equals(""))
		archivo = "no proporcionado";

	// ----------------------------------------------------------------------------------
%>

<table>
	<tr>
		<td class=tabmovtex>Nombre de archivo: <font color=#003366><%=archivo%></font></td>
	</tr>
	<tr>
		<td class=tabmovtex>Transmisi&oacute;n: <font color=#003366><%=transmision%></td>
	</tr>
</table>

<div align="center"><br>
<br>

<!-- Tabla de proveedores --> <%
 if (objArchivo.getNumProveedores() > 0) {
 %>
<TABLE class="textabdatcla" align="center" border="1" cellspacing="1"
	cellpadding="4">
	<TR>
		<TD class="tabmovtex" align="left" nowrap colspan="12"
			bgcolor="#ffffff">Reporte de Proveedores</TD>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Clave de prov.</B></TD>
		<TD class="tittabdat" align="center"><B>Nombre o Raz&oacute;n
		Social</B></TD>
		<TD class="tittabdat" align="center"><B>RFC/ID
		Contribuyente/Num. Seg. Social</B></TD>
		<!-- DAG 290909 Se elimina columna y se une al RFC -->
		<!--TD class = "tittabdat" align = "center"><B>Homo</B></TD-->
		<TD class="tittabdat" align="center"><B>C&oacute;digo de
		cliente</B></TD>
		<TD class="tittabdat" align="center"><B>Estatus</B></TD>
		<TD class="tittabdat" align="center"><B>Descripci&oacute;n de
		estatus</B></TD>


		<%
		if (impresionDetallada) {
		%>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Domicilio</B></TD>
		<TD class="tittabdat" align="center"><B>Cd o poblaci&oacute;n</B></TD>
		<TD class="tittabdat" align="center"><B>C.P. (ZIP)</B></TD>
		<TD class="tittabdat" align="center"><B>Estado</B></TD>
		<TD class="tittabdat" align="center"><B>Medio de
		informaci&oacute;n</B></TD>
		<TD class="tittabdat" align="center"><B>Email</B></TD>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Lada o prefijo</B></TD>
		<TD class="tittabdat" align="center"><B>Tel&eacute;fono</B></TD>
		<TD class="tittabdat" align="center"><B>Ext tel</B></TD>
		<TD class="tittabdat" align="center"><B>Fax</B></TD>
		<TD class="tittabdat" align="center"><B>Ext fax</B></TD>
		<TD class="tittabdat" align="center"><B>Tipo de Proveedor</B></TD>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Forma de pago</B></TD>
		<TD class="tittabdat" align="center"><B>Cuenta Destino</B></TD>
		<TD class="tittabdat" align="center"><B>Divisa</B></TD>
		<TD class="tittabdat" align="center"><B>Pa&iacute;s</B></TD>
		<TD class="tittabdat" align="center"><B>Banco Destino</B></TD>
		<TD class="tittabdat" align="center"><B>Clave ABA</B></TD>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Sucursal</B></TD>
		<TD class="tittabdat" align="center"><B>Plaza/Ciudad</B></TD>
		<TD class="tittabdat" align="center"><B>Tipo de Abono</B></TD>
		<TD class="tittabdat" align="center"><B>Deudores activos</B></TD>
		<TD class="tittabdat" align="center"><B>Lim. financiamiento</B></TD>
		<TD class="tittabdat" align="center"><B>Volumen anual ventas</B></TD>
	</TR>
	<TR>
		<TD class="tittabdat" align="center"><B>Imp. Med. facturas<br>
		emitidas</B></TD>
	</TR>
	<%
	} else {
	%>
	<TD class="tittabdat" align="center"><B>Tipo de Prov</B></TD>
	<TD class="tittabdat" align="center"><B>Forma de Pago</B></TD>
	<TD class="tittabdat" align="center"><B>Cuenta Destino</B></TD>
	<TD class="tittabdat" align="center"><B>Divisa</B></TD>
	<TD class="tittabdat" align="center"><B>Pa&iacute;s</B></TD>
	<TD class="tittabdat" align="center"><B>Banco Destino</B></TD>
	</TR>
	<%
			}
			int total, a;
			String estilo = "";
			ProveedorConf prov;

			total = objArchivo.getNumProveedores();
			StringBuffer sbFormaPago = new StringBuffer("");

			for (a = 0; a < total; a++) {
				estilo = (estilo.equals("textabdatcla")) ? "textabdatobs"
				: "textabdatcla";
				prov = objArchivo.obtenProv(a);
				//DAG - Identificar la forma de pago
				if ("1".equalsIgnoreCase(prov.formaPago)) {
			sbFormaPago.append("T. Mismo Banco");
				} else if ("2".equalsIgnoreCase(prov.formaPago)) {
			sbFormaPago.append("T. Otros Bancos");
				} else {
			sbFormaPago.append("T. Internacional");
				}
	%>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=prov.claveProveedor%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=((prov.tipo == prov.FISICA) ? (prov.nombre+ " " + prov.apellidoPaterno + " " + prov.apellidoMaterno)	: (prov.nombre))%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.rfc	+ (prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor) ? prov.homoclave: "")%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.codigoCliente%>&nbsp;</TD>
		<TD class="<%= estilo %>" align="center">&nbsp;<%=prov.claveStatus%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.status%>&nbsp;</TD>
		<%
		if (impresionDetallada) {
		%>
	</TR>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=prov.calleNumero	+ ((prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor)) ? " "	+ prov.colonia	+ " " + prov.delegMunicipio	: "")%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.cdPoblacion%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.cp%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.estado%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.medioInformacion%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.email%>&nbsp;</TD>
	</TR>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=prov.ladaPrefijo%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.numeroTelefonico%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.extensionTel%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.fax%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.extensionFax%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=(prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor) ? "Nacional" : "Internacional")%>&nbsp;</TD>
	</TR>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=prov.formaPago%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.cuentaCLABE%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.cveDivisa%>&nbsp;</TD>
		<%
		EIGlobal.mensajePorTrace("PAIS GAE->" + prov.cvePais + "<", EIGlobal.NivelLog.DEBUG);
		%>
		<TD class="<%= estilo %>">&nbsp;<%=(prov.cvePais.trim().equals("") ? "M&eacute;xico" : prov.cvePais)%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.descripcionBanco%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.cveABA%>&nbsp;</TD>
	</TR>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=prov.sucursal%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.descripcionCiudad%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=prov.tipoAbono%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=redondearNum(prov.deudoresActivos, 2)%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=redondearNum(prov.limiteFinanciamiento, 2)%>&nbsp;</TD>
		<TD class="<%= estilo %>">&nbsp;<%=redondearNum(prov.volumenAnualVentas, 2)%>&nbsp;</TD>
	</TR>
	<TR>
		<TD class="<%= estilo %>">&nbsp;<%=redondearNum(prov.importeMedioFacturasEmitidas, 2)%>&nbsp;</TD>
	</TR>
	<%
	} else {
	%>
	<TD class="<%= estilo %>">&nbsp;<%=(prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor) ? "Nacional" : "Internacional")%>&nbsp;</TD>
	<TD class="<%= estilo %>">&nbsp;<%=sbFormaPago.toString()%>&nbsp;</TD>
	<TD class="<%= estilo %>">&nbsp;<%=prov.cuentaCLABE%>&nbsp;</TD>
	<TD class="<%= estilo %>">&nbsp;<%=prov.cveDivisa%>&nbsp;</TD>
	<%
	EIGlobal.mensajePorTrace("PAIS GAE->" + prov.cvePais + "<", EIGlobal.NivelLog.DEBUG);
	%>
	<TD class="<%= estilo %>">&nbsp;<%=(prov.cvePais.trim().equals("") ? "M&eacute;xico" : prov.cvePais)%>&nbsp;</TD>
	<TD class="<%= estilo %>">&nbsp;<%=prov.NACIONAL.equalsIgnoreCase(prov.origenProveedor) ? prov.banco : prov.descripcionBanco%>&nbsp;</TD>
	</TR>
	<%
		}sbFormaPago.delete(0, sbFormaPago.length());}
	%>
</TABLE>
<%
}
%> <br>

<!-- Botones --> <A href="javascript:scrImpresion();" border=0><img
	src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir"></a><A
	href="coMtoProveedor?opcion=1&accion=0" border=0><img
	src="/gifs/EnlaceMig/gbo25320.gif" border=0 alt="Regresar"></a></div>

<input type="hidden" name="opcion" value="1">
<input type="hidden" name="accion" value="0">
<input type=hidden name="archivo_actual"
	value="<%= request.getParameter("archivo_actual") %>">

</body>
</html>
