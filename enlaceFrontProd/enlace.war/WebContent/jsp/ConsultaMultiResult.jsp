
	<%@ page import="java.util.*" %>

	<%

	// --- Se obtienen par�metros ----------------------------------------------------------
	String parFuncionesMenu = (String)request.getAttribute("newMenu");
	String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
	String parEncabezado = (String)request.getAttribute("Encabezado");
	String parTrama = (String)request.getAttribute("Trama");
	String parDatos = (String)request.getAttribute("Datos");
	String parOffset = (String)request.getAttribute("Offset");
	String parAncho = (String)request.getAttribute("Ancho");
	String parTotal = (String)request.getAttribute("Total");
	String parCuenta = (String)request.getAttribute("Cuenta");
	String parFecha1 = (String)request.getAttribute("Fecha1");
	String parFecha2 = (String)request.getAttribute("Fecha2");
	String parArchivo = (String)request.getAttribute("Archivo");
	String parArchTux = (String)request.getAttribute("ArchTux");
	String parDivisa = (String)request.getAttribute("Divisa");
	String parCodigo = (String)request.getAttribute("Codigo");

	if(parFuncionesMenu == null) parFuncionesMenu = "";
	if(parMenuPrincipal == null) parMenuPrincipal = "";
	if(parEncabezado == null) parEncabezado = "";
	if(parTrama == null) parTrama = "";
	if(parDatos == null) parDatos = "";
	if(parOffset == null) parOffset = "";
	if(parAncho == null) parAncho = "";
	if(parTotal == null) parTotal = "";
	if(parCuenta == null) parCuenta = "";
	if(parFecha1 == null) parFecha1 = "";
	if(parFecha2 == null) parFecha2 = "";
	if(parArchivo == null) parArchivo = "";
	if(parArchTux == null) parArchTux = "";
	if(parDivisa == null) parDivisa = "";
	if(parCodigo == null) parCodigo = "";

	// --- Se calculan variables para imprimir ---------------------------------------------
	int total = 0,
		offset = 0,
		ctasXpag = 0,
		renglones = 0;

	String celda[][] = null;

	int a,b;
	StringTokenizer tokens, linea;

	try
		{
		total = Integer.parseInt(parTotal);
		offset = Integer.parseInt(parOffset);
		ctasXpag = Integer.parseInt(parAncho);
		renglones = total-offset; if(renglones>ctasXpag) renglones = ctasXpag;

		celda = new String[renglones][10];
		tokens = new StringTokenizer(parDatos,"@");
		for(a=0;a<renglones;a++)
			{
			linea = new StringTokenizer(tokens.nextToken(),"|");
			for(b=0;b<10;b++) celda[a][b] = linea.nextToken();
			}
		}
	catch(Exception e)
		{
		System.out.println("<DEBUG ConsultaMultiResult.jsp> Error en ConsultaMultiResult.jsp: " + e.toString());
		out.println("<DEBUG ConsultaMultiResult.jsp> Error en ConsultaMultiResult.jsp: " + e.toString());
		}

	%>


<!-- COMENZA CODIGO HTML ============================================================-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE>Bienvenido a Enlace Internet</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Rafael Villar Villar">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts =====================================================================-->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>

	// --- pide al server otra p�gina --------------------------------------------------
	function muestra(x)
		{
		document.Forma.Offset.value = "" + x;
		document.Forma.submit();
		}

	// --- exporta el documento --------------------------------------------------------
	function exportar()
		{
		document.Forma.Accion.value = "EXPORTAR";
		document.Forma.submit();
		}

	// --- imprime el texto en una sola l�nea ------------------------------------------
	function imprimeUnaLinea(texto)
		{
		for(a=0;a<texto.length;a++)
			if(texto.substring(a,a+1) != " ")
				document.write(texto.substring(a,a+1));
			else
				{
				document.write("&");
				document.write("n");
				document.write("b");
				document.write("s");
				document.write("p");
				document.write(";");
				}
		}

	// --- Funciones de men� -----------------------------------------------------------
	function MM_preloadImages()
		{ //v3.0
		var d=document;
		if(d.images)
			{
			if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
			for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
			}
		}

	function MM_swapImgRestore()
		{ //v3.0
		var i,x,a=document.MM_sr;
		for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}

	function MM_findObj(n, d)
		{ //v3.0
		var p,i,x;
		if(!d) d=document;
		if((p=n.indexOf("?"))>0&&parent.frames.length)
			{
			d=parent.frames[n.substring(p+1)].document;
			n=n.substring(0,p);
			}
		if(!(x=d[n])&&d.all) x=d.all[n];
		for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		return x;
		}

	function MM_swapImage()
		{ //v3.0
		var i,j=0,x,a=MM_swapImage.arguments;
		document.MM_sr=new Array;
		for(i=0;i<(a.length-2);i+=3)
			if((x=MM_findObj(a[i]))!=null)
				{
				document.MM_sr[j++]=x;
				if(!x.oSrc) x.oSrc=x.src;
				x.src=a[i+2];
				}
		}

	<%= parFuncionesMenu %>

	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->

<BODY background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>

	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="571">
	<tr valign="top"><td width="*">
	<%= parMenuPrincipal %>
	</td></tr></table>
	<%= parEncabezado %>

	<FORM name=Forma action="ChesBenefBaja">
	<INPUT name=Accion type=Hidden value="CONSULTAR">
	<INPUT name=Total type=Hidden value="<%= parTotal %>">
	<INPUT name=Ancho type=Hidden value="<%= parAncho %>">
	<INPUT name=Trama type=Hidden value="<%= parTrama %>">
	<INPUT name=Offset type=Hidden value="<%= parOffset %>">
	<INPUT name=Cuenta type=Hidden value="<%= parCuenta %>">
	<INPUT name=Fecha1 type=Hidden value="<%= parFecha1 %>">
	<INPUT name=Fecha2 type=Hidden value="<%= parFecha2 %>">
	<INPUT name=Archivo type=Hidden value="<%= parArchivo %>">
	<INPUT name=ArchTux type=Hidden value="<%= parArchTux %>">
	<INPUT name=Divisa type=Hidden value="<%= parDivisa %>">
	<INPUT name=Codigo type=Hidden value="<%= parCodigo %>">
	<input type="Hidden" name="ventana" value="10">


	<P class="tabmovtex">
	Cuenta <%= parCuenta %> - <%= parDivisa %><BR>
	C&oacute;digo de servicio: <%= parCodigo %>
	<!--Periodo: de <%= parFecha1 %> al <%= parFecha2 %>-->
	</P>

	<DIV align=center>

	<P class="tabmovtex" align=center>Movimientos: <%= offset + 1 %> a <%= offset + renglones %> de <%= total %></P>

	<TABLE width=600 class="textabdatcla" border=0 cellspacing=1>
	<TR>
		<TD class="tittabdat" align=center>Fecha</TD> <!--bgcolor=#90B0C0-->
		<TD class="tittabdat" align=center>Sucursal</TD>
		<TD class="tittabdat" align=center>Banco</TD>
		<TD class="tittabdat" align=center>Cuenta</TD>
		<TD class="tittabdat" align=center>N&uacute;mero de cheque</TD>
		<TD class="tittabdat" align=center>Importe</TD>
		<TD class="tittabdat" align=center>Estatus</TD>
		<TD class="tittabdat" align=center>Causa devol.</TD>
		<TD class="tittabdat" align=center>Folio</TD>
		<TD class="tittabdat" align=center>Referencia</TD>
	</TR>
	<% String estilo = "textabdatcla"; %>
	<% for(a=0;a<renglones;a++) { %>
	<TR>
		<% estilo=(estilo.equals("textabdatcla"))?"textabdatobs":"textabdatcla"; %>
		<TD class="<%= estilo %>" align=center><%= celda[a][0] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][1] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][2] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][3] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][4] %></TD>
		<TD class="<%= estilo %>" align=right><%= celda[a][5] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][6] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][7] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][8] %></TD>
		<TD class="<%= estilo %>" align=center><%= celda[a][9] %></TD>
	</TR>
	<% } %>
	</TABLE>

	<DIV>
	<%
	String pag;

	if(offset>0) { %><A href="javascript:muestra(<%= offset-ctasXpag %>)" class=texfootpaggri>&lt;Anterior</A>&nbsp;<% }
	if(total > ctasXpag) for(a=1;((a-1)*ctasXpag)<total;a++)
		{
		pag = "" + a;
		if((a-1)*ctasXpag != offset)
			pag = "<A href=\"javascript:muestra(" + ((a-1)*ctasXpag) + ")\" class=texfootpaggri>" + pag + "</A>";
		else
			pag = "<SPAN  class=texfootpaggri>" + pag + "</SPAN>";
		out.println(pag);
		}
	if(offset+ctasXpag<total) { %>&nbsp;<A href="javascript:muestra(<%= offset+ctasXpag %>)" class=texfootpaggri>Siguiente&gt;</A><% }
	%>
	</DIV>

	<BR><A href="<%= parArchivo %>"><IMG border=0 src="/gifs/EnlaceMig/gbo25230.gif" alt="Exportar"></A><A href="javascript:scrImpresion()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>

	</DIV>

	</FORM>

</BODY>

</HTML>
