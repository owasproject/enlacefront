<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="Proyecto" content="Pago SAR" />
<meta name="Descripcion" content="Este archivo contiene la estructura del comprobante que ser� impreso. Presenta los datos que describen a la transacci�n realizada." />
<meta name="Version" content="1.0" />
<script type="text/JavaScript">
   parent.frames['botones'].location.href='/Enlace/enlaceMig/jsp/BotonesCompSAR.jsp';
   
	function cerrar()
	{
		top.close();
		top.opener.enviaInicio();
	}
</script>

<style type="text/css">
.Estilo1 {
	font-family: "Times New Roman", Times, serif;
	font-size: 14px;
	font-weight: bold;
}
.Estilo2 {font-size: 18px}
</style>

</head>

<body>
<p><img src="/gifs/EnlaceMig/logo_sant.gif" width="348" height="51"/></p>
<p><font face="Arial, Helvetica, sans-serif"><strong><font size="3">S.A.R. Comprobante
  del pago por L&iacute;nea de Captura v&iacute;a Internet</font></strong></font></p>
<p> <font size="3" face="Arial, Helvetica, sans-serif"><strong>Datos del pago</strong></font></p>
<table width="800" border="0">
  <%--DWLayoutTable--%>
  <tr>
    <td width="32" height="22"> <pre></pre></td>
    <td width="160"><font size="2" face="Courier New, Courier, mono">Fecha de
      Pago:</font> </td>
    <td width="106"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("Fecha"));%>
      </font> </td>
    <td width="467"><font size="2" face="Courier New, Courier, mono">Hora: </font>
      <font size="2" face="Courier New, Courier, mono">
      <%
		if (request.getAttribute("Operacion").equals("A"))
			out.print(new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date()));
		else
			out.print(request.getAttribute("Hora"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">L&iacute;nea de Captura:</font>
    </td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("LineaCaptura"));%>
      </font> </td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Bimestre de Pago:&nbsp;
      </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("Bimestre"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Tipo de Pago:&nbsp; </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("TipoPago"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Importe Retiro:&nbsp;
      </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("Retiro"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Importe Vivienda:&nbsp;
      </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print( request.getAttribute("Vivienda"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Total Pagado:&nbsp; </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("Total"));%>
      </font></td>
  </tr>
  <tr>
    <td></td>
    <td><font size="2" face="Courier New, Courier, mono">Num. Operaci&oacute;n:&nbsp;
      </font></td>
    <td colspan="2"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("Referencia"));%>
      </font></td>
  </tr>
  <tr>
    <td width="32" height="22">&nbsp;</td>
    <td width="160"><font size="2" face="Courier New, Courier, mono">Tipo L&iacute;nea
      Captura:</font></td>
    <td width="106"><font size="2" face="Courier New, Courier, mono">
      <% out.print(request.getAttribute("TipoLineaCaptura"));%>
      </font> </td>
    <td width="467"> <font size="2" face="Courier New, Courier, mono">
     <% out.print(request.getAttribute("NombreTipoLineaCaptura"));%>
      </font></td>
  </tr>
</table>
<div align="center">
  <div align="left">
    <p><strong><font size="3" face="Arial, Helvetica, sans-serif">Datos de la
      Dependencia </font></strong></p>
    <table width="650" border="0">
      <tr>
        <td width="32"></td>
        <td width="160"><font size="2" face="Courier New, Courier, mono">Dependencia:</font>
        </td>
        <td width="443"> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("Dependencia"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">RFC dependencia:</font>
        </td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("RFC"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">Centro de Pago:</font>
        </td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("CentroPago"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">Domicilio: </font>
        </td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("Domicilio"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">Cuenta de Cargo:
          </font> </td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("CuentaCargo")+"  "+request.getAttribute("DescripCuenta"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">Contrato de Enlace:</font></td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("NumContrato")+"   "+request.getAttribute("NombreContrato"));%>
          </font></td>
      </tr>
      <tr>
        <td></td>
        <td><font size="2" face="Courier New, Courier, mono">Usuario de Enlace:</font></td>
        <td> <font size="2" face="Courier New, Courier, mono">
          <% out.print(request.getAttribute("IDUsuario")+"   "+request.getAttribute("NombreUsuario"));%>
          </font></td>
      </tr>
    </table>

  </div>
  <pre class="Estilo1" align="center"><img src="/gifs/EnlaceMig/call_center.gif" width="148" height="55" align="middle"/></pre>
</div>
<br/><br/>
<table border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center">
      <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
	  <tr>
	      <td align="center"><a href="" border="0" onClick="javascript:cerrar();">
            <img name="imageField32" src="/gifs/EnlaceMig/gbo25200.gif" width="83" height="22" alt="Cerrar" style="border: 0"/></a>
          </td>
	  <td align="center"><a href="javascript:parent.frames.comprobante.print();">
		  <img name="imageField33" src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" alt="Imprimir" style="border: 0"/></a>
		</td>
	  </tr>
	 </table>
	</td>
  </tr>
</table>

</body>
</html>