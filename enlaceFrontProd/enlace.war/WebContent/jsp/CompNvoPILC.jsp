
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="mx.altec.enlace.dao.EI_Query"%>
<%
  System.out.println("Entrando a CompNvoPILC.jsp");
  System.out.println("Generando comprobante de operaciones para pago de Impuestos (Linea de Captura)");

  String referencia390="";
  String referencia="";
  String contrato="";
  String desccto="";
  String usuario="";
  String concepto="";
  String importe="";
  String fecha="";
  String hora="";
  String ctacargo="";
  String titcta="";

  Vector data=new Vector();

  referencia=(request.getParameter("referencia")==null)?"":(String)request.getParameter("referencia");
  contrato=(request.getParameter("contrato")==null)?"":(String)request.getParameter("contrato");
  desccto=(request.getParameter("desccto")==null)?"":(String)request.getParameter("desccto");
  usuario=(request.getParameter("usuario")==null)?"":(String)request.getParameter("usuario");
  concepto=(request.getParameter("concepto")==null)?"":(String)request.getParameter("concepto");
  importe=(request.getParameter("importe")==null)?"":(String)request.getParameter("importe");
  fecha=(request.getParameter("fecha")==null)?"":(String)request.getParameter("fecha");
  hora=(request.getParameter("hora")==null)?"":(String)request.getParameter("hora");
  ctacargo=(request.getParameter("ctacargo")==null)?"":(String)request.getParameter("ctacargo");
  titcta= (request.getParameter("titcta")==null)?"":(String)request.getParameter("titcta");

  System.out.print("Referencia enlace="+referencia);

  if(request.getParameter( "referencia390" ) != null )
	 referencia390=(String)request.getParameter( "referencia390" );
  else
   {
	  String tabla="tct_Bitacora";

	  GregorianCalendar CalHoy = new GregorianCalendar();

	  if(!fechaIgual(fecha,CalHoy))
		 tabla="tct_Bitacora_his";

	  EI_Query DB=new EI_Query();
	  data=DB.ejecutaQueryCampo("select PROTECCION1 from "+tabla+" where REFERENCIA="+referencia+" and cuenta_origen='"+ctacargo+"' and num_cuenta='"+contrato+"' and fecha=to_date('"+fecha+"','dd/mm/yyyy') ");

	  referencia390=(data.size()<=0)?"0":(String)data.get(0);
   }
  System.out.print("Referencia 390="+referencia390);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>Comprobante</title>
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25390">
<meta name="Proyecto" content="Portal">
<meta name="Ultima version" content="07/05/2001 09:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body bgcolor="#ffffff">
<form name="form1" method="post" action="">

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
     <td rowspan="2" valign="middle" class="titpag" align="left" width="35"><img SRC="/gifs/EnlaceMig/glo25040b.gif" BORDER="0"></td>

    </tr>
  </table>

  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
       <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" colspan="2" class="titenccom">Recibo Bancario de Pago de Contribuciones Federales</td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center">
        <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td colspan="3"> </td>
          </tr>
          <tr>
            <td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
            <td width="428" height="120" valign="top" align="center">
              <table width="380" border="0" cellspacing="2" cellpadding="3" background="/gifs/EnlaceMig/gau25010.gif">
                <tr>
                  <td class="tittabcom" align="right" width="0">Contrato:</td>
                  <td class="textabcom" align="left" nowrap><%=contrato%> &nbsp; <%=desccto%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Usuario:</td>
                  <td class="textabcom" align="left" nowrap><%=usuario%> </td>
                </tr>

                <tr>
                  <td class="tittabcom" align="right" width="0">L&iacute;nea Captura:</td>
                  <td class="textabcom" align="left" nowrap> <%=concepto%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Importe Pagado:</td>
                  <td class="textabcom" align="left" nowrap> <%=importe%></td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Fecha y Hora:</td>
                  <td class="textabcom" align="left"> <%=fecha%> &nbsp;&nbsp;&nbsp;<%=hora%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">N&uacute;mero de Operaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap> <%=referencia390%> </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Cuenta de cargo:</td>
                  <td class="textabcom" align="left"><%=ctacargo%> &nbsp; <%=titcta%>
                  </td>
                </tr>
                <tr>
                  <td class="tittabcom" align="right" width="0">Medio Presentaci&oacute;n:</td>
                  <td class="textabcom" align="left" nowrap>INTERNET</td>
                </tr>
              </table>
            </td>
            <td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2" alt=".." name=".."></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td align="center"><br>
        <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
          <tr>
            <td align="right" width="83">
			<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" width="83" height="22" border="0"></a>
            </td>
            <td align="left" width="71"><a href="javascript:;" onClick="window.close()"><img src="/gifs/EnlaceMig/gbo25200.gif" width="71" height="22" border="0"></a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
 </form>
</body>
</html>

<%!
/*************************************************************************************/
/********************************************************************** fechaIgual   */
/*************************************************************************************/
public boolean fechaIgual(String f1, GregorianCalendar f2)
	{
		int anio1=new Integer(f1.substring(6)).intValue();
		int anio2=f2.get(Calendar.YEAR);
		int mes1=new Integer(f1.substring(3,5)).intValue();
		int mes2=f2.get(Calendar.MONTH)+1;
		int dia1=new Integer(f1.substring(0,2)).intValue();
		int dia2=f2.get(Calendar.DATE);

		System.out.println("\n>" + anio1 + " " + anio2);
		System.out.println(">" + dia1 + " " + dia2);
		System.out.println(">" + mes1 + " " + mes2);

		if(anio1==anio2 && mes1==mes2 && dia1==dia2)
			return true;
		return false;
	}

  public boolean fechaIgual(GregorianCalendar f1, GregorianCalendar f2)
	{
		int anio1=f1.get(Calendar.YEAR);
		int anio2=f2.get(Calendar.YEAR);
		int mes1=f1.get(Calendar.MONTH);
		int mes2=f2.get(Calendar.MONTH);
		int dia1=f1.get(Calendar.DATE);
		int dia2=f2.get(Calendar.DATE);

		if(anio1==anio2 && mes1==mes2 && dia1==dia2)
			return true;
		return false;
	}
%>
