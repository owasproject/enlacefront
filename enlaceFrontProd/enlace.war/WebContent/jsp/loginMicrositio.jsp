<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.Calendar" %>
<%@ page import="mx.altec.enlace.utilerias.*"%>
<%
    Calendar dt = Calendar.getInstance();
    dt.setTime( new java.util.Date() );
    int mes = dt.get(Calendar.MONTH);
    int dia = dt.get(Calendar.DAY_OF_MONTH);
    int anio = dt.get(Calendar.YEAR);

	java.util.Date hora = new java.util.Date();
	int hh = hora.getHours();
	int mm = hora.getMinutes();
	int ss = hora.getSeconds();
%>
<html>
	<head>
	<title>Santander</title>
	<meta http-equiv="Content-Type" content="text/html;"/>
	<meta name="Codigo de Pantalla" content="s26050"/>
	<meta http-equiv="Expires" content="1"/>
	<meta http-equiv="pragma" content="no-cache"/>

	<script type="text/JavaScript" SRC= "/EnlaceMig/microSitio_cuadroDialogo.js"></script>
	<script type="text/JavaScript" SRC= "/EnlaceMig/Convertidor.js"></script>

	<script type="text/JavaScript">

	function FrameAyuda(ventana)
	{	hlp = window.open("Ayuda?ClaveAyuda=" + ventana, "hlp", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450");
		hlp.focus();
	}

	//////////////////////////////////////////////////////////
	function document_registro_registrar_onclick()
	{
		ValidaEntradasLogin();
	}

	function limpiar()
	{
		document.registro.usuario.value="";
		document.registro.usuarioTxt.value="";
		document.registro.clave.value="";
		document.registro.contrato.value="";
	}
	/////////////////////////////////////////////////////////

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
		if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
	// distintivo
	function isDigit (c)
	{
	  return ((c >= "0") && (c <= "9"))
	}

	function isInteger (s)
	{
		var i

		for (i = 0; i < s.length; i++)
		{
			var c = s.charAt(i)
			if (!isDigit(c))
			return false
		}
		return true
	}

	//////////////////////////////////////////////////////////////////
	//
	//  validUser ()
	//
	//////////////////////////////////////////////////////////////////
	function validUsr (user)
	{
		result=isInteger (user)
		if (!isInteger (user))
		{
			document.registro.usuarioTxt.focus()
			cuadroDialogo ("El C&oacute;digo de Cliente Debe Ser Num\351rico.", 1)
			return false
		}
		// regresa false si el campo esta en blanco
		if (user == "")
		{
			document.registro.usuarioTxt.focus()
			cuadroDialogo ("\n Favor de teclear su C&oacute;digo de Cliente. ", 1)
			result = false
		} else
			// la clave de usuario debe ser de 7 digitos
		  if (user.length != 7)
		  {

			//alert ("\n La Clave de Usuario debe tener longitud de 7 d\355gitos. " +
			//       "\n Usted proporcion\363 una Clave de Usuario de " +
			//       user.length + " digitos.")
			//document.registro.usuarioTxt.focus()
			//completeUserID()
			return true
		  }
		  else
			result = true
	  return result
	}

	function validaFormato (pwd)
	{
		var TemplateF=/^[a-z\d]{0,20}$/i;
		return TemplateF.test(pwd);
	}

	//////////////////////////////////////////////////////////////////
	//
	//  validPwd ()
	//
	//////////////////////////////////////////////////////////////////
	function validPwd (pwd)
	{

	  if (!validaFormato (pwd))
	  {
		document.registro.clave.focus();
		document.registro.clave.value="";
		cuadroDialogo ("Contrase\361a inv&aacute;lida.", 1)
		return false
	  }
	  // regresa false si el campo esta en blanco
	  if (pwd == "")
	  {
		  //alert ("\n Favor de teclear su Contrase\361a. ")
		  cuadroDialogo ("\n Favor de teclear su Contrase\361a. ", 1)
		  //document.registro.usuario.value=cliente_7To8(document.registro.usuarioTxt.value);
		  //document.registro.clave.focus()
		  return false
	  } else {
		// el passwd debe tener longitud de 4 digitos
		if (pwd.length != 4)
		{
		  //alert ("\n La Clave de Acceso debe tener longitud de 4 d\355gitos. " +
		  //       "\n Usted proporcion\363 una Clave de Acceso de " +
		  //       pwd.length + " digitos.")
			//document.passwd.pwd_nvacontrasena.value=""
			//document.passwd.pwd_confcontrasena.value=""
			//document.passwd.pwd_nvacontrasena.focus()
			//completeUsrId()
			return true
		 } else
			result = true
	  }
	  return result
	}
	//////////////////////////////////////////////////////////////////
	//
	//  completeUsrID ()
	//
	//////////////////////////////////////////////////////////////////
	function completeUsrID (user)
	{
	  //user = document.registro.usuarioTxt.value
	  var result

	  if (user.length > 0)
	  {
		for (i = user.length; i < 7; i++)
		  user = "0" + user
	  }
	  if(user.length==8)
	   document.registro.usuario.value=cliente_8To7(user);
	 else
	   document.registro.usuario.value=user;
	  return user;
	}
	//////////////////////////////////////////////////////////////////
	//
	//  validPasswds ()
	//
	//////////////////////////////////////////////////////////////////
	function validPasswds()
	{
	  result = validPwd (document.passwd.pwd_contrasena.value)
	  if (result == true)
	  {
		result = validPwd (document.passwd.pwd_nvacontrasena.value)
		if (result == true)
		  result = validPwd (document.passwd.pwd_confcontrasena.value)
		else
		  return result
	  }
	  else
		return result

	  if (document.passwd.pwd_nvacontrasena.value == document.passwd.pwd_contrasena.value)
	  {
		cuadroDialogo ("La contrase\361a nueva debe ser diferente de la actual", 1)
		document.passwd.pwd_nvacontrasena.value=""
		document.passwd.pwd_confcontrasena.value=""
		document.passwd.pwd_nvacontrasena.focus()
		return false
	  }

	  if (document.passwd.pwd_nvacontrasena.value != document.passwd.pwd_confcontrasena.value)
	  {
		cuadroDialogo ("La contrase\361a nueva debe coincidir con su confirmaci\363n", 1)
		document.passwd.pwd_nvacontrasena.value=""
		document.passwd.pwd_confcontrasena.value=""
		document.passwd.pwd_nvacontrasena.focus()
		return false
	  }
	  return result
	}
	//////////////////////////////////////////////////////////////////
	//
	//  Valida_Date ()
	//
	//////////////////////////////////////////////////////////////////

	function Valida_Date (dia, mes, anio)
	{
	  result = true;

	  if ((dia == "") && (mes == "") && (anio == ""))
	  {
		//alert (MSG6001)
		cuadroDialogo ("Favor de llenar los campos de fecha", 1)
		return false
	  }
	  else
	  {
		if ((isNaN (dia)) || (isNaN (mes)) || (isNaN (anio)))
		{
		  //alert (MSG6002)
		  cuadroDialogo ("\n Favor de llenar el campo con un valor num\351rico", 1);
		  return false;
		}
	  }

	  if ((dia < 0) && (dia > 31))
	  {
		cuadroDialogo ("Digite una fecha valida", 3);
		return false;
	  }
	  // verifica si la fecha proporcionada es posterior a la actual
	  var hoy = new Date();

	  var fecha_capt = anio +
			((mes < 10) ? "0" : "") + mes +
				   ((dia < 10) ? "0" : "") + dia;
	  var hoy_al_reves = hoy.getFullYear() +
			   ((hoy.getMonth () + 1) < 10 ? "0" : "") +
				   (hoy.getMonth () + 1) +
			   ((hoy.getDate ()) < 10 ? "0" : "") + hoy.getDate ();

	  if (fecha_capt > hoy_al_reves)
	  {
		result = false
		//alert (MSG6003)
		cuadroDialogo ("\nLa fecha " + dia + "/" + mes + "/" + anio + " es posterior al dia de hoy", 1);
	  }
	  return true;

	}

	//////////////////////////////////////////////////////////////////
	//
	// Function valida_fechas Valida las fechas de entrada para movimientos
	//
	/////////////////////////////////////////////////////////////////
	function valida_fechas()

	{
		var hoy = new Date()
		var Anio = hoy.getYear()
		var Mes = ((hoy.getMonth () + 1) < 10 ? "0" : "") + (hoy.getMonth () + 1)
		var Dia = ((hoy.getDate ()) < 10 ? "0" : "") + hoy.getDate ();

		today=Anio+Mes+Dia

		if (document.activity.opcion[0].checked==true)
		{
		  // movimientos de hoy
		  document.activity.txt_fecha_ini.value=today
		  document.activity.txt_fecha_fin.value=today
		  return true
		}
		else
		{
		  if (document.activity.opcion[1].checked==true)
		  {
			// movimientos historicos
			result = Valida_Date(document.activity.dia.value,
					 document.activity.mes.value, document.activity.anio.value)

			if (result == true)
			{
			  result = Valida_Date(document.activity.fdia.value,
				   document.activity.fmes.value, document.activity.fanio.value)
			}
			else
			  return result

			if (result == true)
			{
		  anio =document.activity.anio.value
		  mes=document.activity.mes.value
		  dia=document.activity.dia.value
		  fanio=document.activity.fanio.value
		  fmes=document.activity.fmes.value
		  fdia=document.activity.fdia.value

			  anioAux = anio.substring (1, 3)
			  fanioAux = fanio.substring (1, 3)
			  document.activity.txt_fecha_ini.value=anioAux+mes+dia
			  document.activity.txt_fecha_fin.value=fanioAux+fmes+fdia
			  //alert (document.activity.txt_fecha_ini.value)
			  //alert (document.activity.txt_fecha_fin.value)
			}
			else
		  return false
		  }
		}

	  return result
	}

	//////////////////////////////////////////////////////////////////
	//
	//  validInputActivity ()
	//
	//////////////////////////////////////////////////////////////////
	function validInputActivity()
	{
		var hoy = new Date()
		var Anio = hoy.getYear()
		var Mes = ((hoy.getMonth () + 1) < 10 ? "0" : "") + (hoy.getMonth () + 1)
		var Dia = ((hoy.getDate ()) < 10 ? "0" : "") + hoy.getDate ();

		today=Anio+Mes+Dia

			if (document.activity.opcion[0].checked==true)
		{
		  document.activity.txt_fecha_ini.value=today
		  document.activity.txt_fecha_fin.value=today
		}
		else
		{
		  if (document.activity.opcion[1].checked==true)
		  {
			result = validDate(document.activity.dia.value,
					 document.activity.mes.value, document.activity.anio.value)

			if (result == true)
			{

			  result = validDate(document.activity.fdia.value,
				   document.activity.fmes.value, document.activity.fanio.value)
			}
			else
			  return result

			if (result == true)
			{
		  anio =document.activity.anio.value
		  mes=document.activity.mes.value
		  dia=document.activity.dia.value
		  fanio=document.activity.fanio.value
		  fmes=document.activity.fmes.value
		  fdia=document.activity.fdia.value

		  if (dia.length == 1)
			dia = "0" + dia

			  if (mes.length == 1)
				mes = "0" + mes

			  anioAux = anio.substring (1, 3)
			  fanioAux = fanio.substring (1, 3)
			  document.activity.txt_fecha_ini.value=anioAux+mes+dia
			  document.activity.txt_fecha_fin.value=fanioAux+fmes+fdia
			}
			else
		  return false
		  }
		}

	  return true
	}

	//////////////////////////////////////////////////////////////////
	//
	//  validAmount ()
	//
	//////////////////////////////////////////////////////////////////

	function validAmount (cantidad)
	{
	  strAux1 = ""
	  strAux2 = ""
	  entero = ""
	  cantidadAux = cantidad

	  if (cantidadAux == "" || cantidadAux <= 0)
	  {
		cuadroDialogo ("\n Favor de introducir un monto valido a transferir ", 3)
		document.transfer.txt_importe.value=""
		document.transfer.txt_importe.focus()
		return false
	  }
	  else
	  {
		if (isNaN(cantidadAux))
		{
		  cuadroDialogo ("Favor de ingresar un valor num\351rico de importe", 3)
		  document.transfer.txt_importe.focus()
		  document.transfer.txt_importe.value=""
		  return false
		}

		pos_punto = cantidadAux.indexOf (".")

		num_decimales=cantidad.substring(pos_punto + 1,cantidad.length)

		if (pos_punto == 0)
		  strAux1 = "0." + cantidadAux.substring (1, cantidadAux.length)
		else
		{
		  if (pos_punto != -1)     //-- si se teclearon los centavos
		  {
			cents = cantidadAux.substring (pos_punto + 1, cantidadAux.length)
			entero = cantidadAux.substring (0, pos_punto)
		  }
		  else
		  {
			cents = "00"
			entero = cantidadAux
		  }

		  pos_coma = entero.indexOf (",")
		  if (pos_coma != -1)     //-- si son mas de mil
		  {
			cientos = entero.substring (entero.length - 3, entero.length)
			miles = entero.substring (0, entero.length - 3)
		  }
		  else
		  {
			if (entero.length > 3) //-- si se teclearon mas de mil sin coma
			{
			  cientos = entero.substring (entero.length - 3, entero.length)
			  miles = entero.substring (0, entero.length - 3)
			}
			else
			{
			  if (entero.length == 0)
				cientos = ""
			  else
				cientos = entero
			  miles = ""
			}
		  }

		  if (miles != "")
			strAux1 = miles
		  if (cientos != "")
			strAux1 = strAux1 + cientos + "."
		  strAux1 = strAux1 + cents

		  if (miles != "")
			strAux2 = miles
		  if (cientos != "")
			strAux2 = strAux2 + cientos + "."
		  strAux2 = strAux2 + cents

		  transf = document.transfer.txt_importe.value
		}
		document.transfer.txt_importe.value = strAux1
		strAux1=Formatea_Importe(strAux1)
		if (miles != "")
		  document.transfer.txt_importe.value = strAux2

		return confirm (" Transferir $" + strAux1 + " ? ")
	  }
	}
	///////////////////////////////////////////////////////////////////
	//
	// validSelect() valida que algo sea seleccionado en un select
	//
	///////////////////////////////////////////////////////////////////
	function validSelect (operacion)
	{
	//  flag=document.form1.acct_selected.selectedIndex;
	  if (flag ==0)
	  {
		cuadroDialogo ("Favor de seleccionar una cuenta", 1)
		return false
	  }
	  else
		 if (operacion=="POSI")
		 {
		   num_cuenta=(document.form1.acct_selected.options[document.form1.acct_selected.selectedIndex].value).substring(0,11)
		   codicer=num_cuenta.substring(0,2)
		   if (codicer < 60 || codicer > 65)
		   {
			 cuadroDialogo ("Cuenta no valida para la operaci\363n seleccionada", 1);
		 document.form1.acct_selected.selectedIndex=-1;
			 return false
		   }
		 }
		 return true
	}

	///////////////////////////////////////////////////////////////////
	//
	//  validctasdif() Valida que la cuenta de cargo y la de abono sean
	//		   diferentes.
	//
	///////////////////////////////////////////////////////////////////

	function validctasdif (from_account, to_account,importe)
	{
	  if (from_account == to_account)
	  {
		cuadroDialogo ("La cuenta de cargo debe ser diferente a la cuenta de abono", 1)
		document.transfer.txt_importe.focus()
		return false
	  }
	  else
		   result=validAmount(importe)

	  return result

	}
	////////////////////////////////////////////////////////////////////////
	//
	// Formatea_Importe() Funcion que formatea los importes
	//
	///////////////////////////////////////////////////////////////////////

	function Formatea_Importe(importe)
	{
	   decenas=""
	   centenas=""
	   millon=""
	   millares=""
	   importe_final=importe
	   var posiciones=7;

	   posi_importe=importe.indexOf(".");
	   num_decimales=importe.substring(posi_importe + 1,importe.length)

	   if (posi_importe==-1)
		 importe_final= importe + ".00";

	   if (posi_importe==4)
	   {
		 centenas=importe.substring(1, posiciones);
		 millares=importe.substring(0,1);
		 importe_final= millares + "," + centenas;
	   }
	   if (posi_importe==5)
	   {
		 centenas=importe.substring(2, posiciones + 1);
		 millares=importe.substring(0,2);
		 importe_final= millares + "," + centenas;
	   }
	   if (posi_importe==6)
	   {
		 centenas=importe.substring(3, posiciones + 2);
		 millares=importe.substring(0,3);
		 importe_final= millares + "," + centenas;
	   }
	   if (posi_importe==7)
	   {
		 centenas=importe.substring(4, posiciones + 3);
		 millares=importe.substring(1,4);
		 millon=importe.substring(0,1)
		 importe_final= millon + "," + millares + "," + centenas;
	   }
	   if (posi_importe==8)
	   {
		 centenas=importe.substring(5, posiciones + 4);
		 millares=importe.substring(2,5);
		 millon=importe.substring(0,2)
		 importe_final= millon + "," + millares + "," + centenas;
	   }
	   if (posi_importe==9)
	   {
		 centenas=importe.substring(6, posiciones + 5);
		 millares=importe.substring(3,6);
		 millon=importe.substring(0,3)
		 importe_final= millon + "," + millares + "," + centenas;
	   }
	   return importe_final
	}

	/////////////////////////////////////////////////////////////////////////
	//
	// Function fechas() Imprime la fecha del dia actual
	//
	/////////////////////////////////////////////////////////////////////////
	function fechas()
	{
		 hoy = new Date()
		 Anio = hoy.getYear()
		 Mes = ((hoy.getMonth () + 1) < 10 ? "0" : "") + (hoy.getMonth () + 1)
		 Dia = ((hoy.getDate ()) < 10 ? "0" : "") + hoy.getDate ();

		 if (Anio > 50)
			Aniocurso="19"
		 else
			Aniocurso="20"
	}


	//////////////////////////////////////////////////////////////////////////
	//
	// Funcion Formatea_Fechas()  Formatea las fechas presentadas en pantalla
	//
	//////////////////////////////////////////////////////////////////////////

	function Formatea_Fechas(fecha)
	{
		fecha_anual=fecha.substring(0,2);
		if (fecha_anual > 50)
			fecha_anual= "19" + fecha_anual
		else
			fecha_anual= "20" + fecha_anual

		fecha_mes=fecha.substring(2,4);
		fecha_dia=fecha.substring(4,6);
		fecha_formateada=fecha_dia + "/" + fecha_mes + "/" + fecha_anual

		return fecha_formateada
	}

	//////////////////////////////////////////////////////////////////////////
	//
	// Funcion Formatea_Hora()  Formatea la hora presentadas en pantalla
	//
	//////////////////////////////////////////////////////////////////////////

	function Formatea_Hora(horas)
	{
		hora=horas.substring(0,2);
		minuto=horas.substring(2,4);
		segundo=horas.substring(4,6);
		hora_formateada= hora + ":" + minuto + ":" + segundo
		return hora_formateada
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	// ColocaFecha() Funcion que coloca la fecha historica en los selct
	//
	/////////////////////////////////////////////////////////////////////////////
	function ColocaFecha()
	{
	 if (document.activity.opcion[1].checked==true)
	 {
	  document.write ("<SELECT NAME=sel_diaini SIZE=1>");
	  for (dia; dia<=31; dia++)
	 {
		document.write ("<OPTION VALUE='" + dia + "'>" + dia);
	 }

	  document.write("</SELECT>");
	 }
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// FechaSeleccionada () Determina que fecha se seleciono.
	//
	//////////////////////////////////////////////////////////////////////////
	function FechaSeleccionada()
	{
	  result=true;
	  hoy=new Date();
	  dactual=hoy.getDate();
	  mactual=hoy.getMonth();
	  mactual=mactual +1;
	  indicedia=document.activity.sel_diaini.selectedIndex;
	  textodia=document.activity.sel_diaini.options[indicedia].text;
	  indicemes=document.activity.sel_mesini.selectedIndex;
	  textomes=document.activity.sel_mesini.options[indicemes].text;
	  indiceanio=document.activity.sel_anioini.selectedIndex;
	  textoanio=document.activity.sel_anioini.options[indiceanio].text;
	  textoanio=textoanio.substring(2,4);
	  indicefdia=document.activity.sel_diafin.selectedIndex;
	  textofdia=document.activity.sel_diafin.options[indicefdia].text;
	  indicefmes=  document.activity.sel_mesfin.selectedIndex;
	  textofmes=document.activity.sel_mesfin.options[indicefmes].text;
	  indicefanio=document.activity.sel_aniofin.selectedIndex;
	  textofanio=document.activity.sel_aniofin.options[indicefanio].text;
	  textofanio=textofanio.substring(2,4);
	  textodia_int=parseInt(textodia,10);
	  textomes_int=parseInt(textomes,10);
	  textofdia_int=parseInt(textofdia,10);
	  textofmes_int=parseInt(textofmes,10);
	  mes_consulta=mactual - 3;

	  if (textomes_int ==4 || textomes_int ==6 || textomes_int==9 || textomes_int==11)
	  {
		if(textodia_int > 30 )
		{
		  cuadroDialogo("El mes seleccionado no tiene 31 dias", 3);
		  return false;
		}
	  }
	  if (textomes_int == 2)
	  {
		if(textodia_int > 28 && (textoanio != "00" || textofanio != "00"))
		{
		  cuadroDialogo("El mes seleccionado solo tiene 28 dias", 1);
		  return false;
		}
	  }
	  if (textomes_int < mes_consulta)
	  {
		  cuadroDialogo("Solo puede consultar tres meses atras a partir de la fecha actual", 1);
		  return false;
	  }
	  if(textomes_int > textofmes_int)
	  {
		  cuadroDialogo ("La fecha inicial no puede ser mayor a la final", 3);
		  document.activity.sel_diaini[0].selected=true;
		  document.activity.sel_mesini[mactual-1].selected=true;
		  return false;
	  }
	  else
	  {
		  if (textofdia_int >dactual || textofmes_int > mactual)
		  {
			 cuadroDialogo ("La fecha final no puede ser mayor a la de hoy", 1);
			 document.activity.sel_diafin[dactual-1].selected=true;
			 document.activity.sel_mesfin[mactual-1].selected=true;
			 return false;
		  }
	  }
	  if(result==true)
	  {
		document.activity.txt_fecha_ini.value=textoanio + textomes + textodia;
		document.activity.txt_fecha_fin.value=textofanio + textofmes + textofdia;
	  }
	  else
		return result;
	}

	/////////////////////////////////////////////////////////////////////////////
	//
	// MuestraFecha() Si la opcion es dia muestra la fecha acutal de lo contrario
	//                muestra desde el primero del mes
	/////////////////////////////////////////////////////////////////////////////
	function MuestraFecha()
	{
	  indice=0
	  fecha=new Date();
	  mes_actual=fecha.getMonth();
	  mes_actual=mes_actual + 1;
	  dia_actual=fecha.getDate();
	  indice_dia=dia_actual -1;
	  indice_mes=mes_actual -1;

	 // if(document.activity.opcion[0].checked==true)
	 // {
		document.activity.sel_diaini[indice].selected=true;
		document.activity.sel_diafin[indice_dia].selected=true;
		document.activity.sel_mesini[indice_mes].selected=true;
		document.activity.sel_mesfin[indice_mes].selected=true;
	 // }
	 // else
	 // {
		document.activity.sel_diaini[indice].selected=true;
		document.activity.sel_diafin[indice_dia].selected=true;
		document.activity.sel_mesini[indice_mes].selected=true;
		document.activity.sel_mesfin[indice_mes].selected=true;
	 // }
	}

	///////////////////////////////////////////////////////////////
	//
	//Formatea_Cuenta()
	//
	//////////////////////////////////////////////////////////////////
	function Formatea_Cuenta(cuenta)
	{
	 codicer=cuenta.substring(0,2);
	 numecta=cuenta.substring(2,10);
	 digita=cuenta.substring(10,11);
	 cta_formateada=codicer + "-" + numecta + "-" + digita;
	 return cta_formateada
	}

	//////////////////////////////////////////////////////////////////
	//
	//Quita_Formato()
	//
	//////////////////////////////////////////////////////////////////

	function Quita_Formato(cuenta)
	{
		codicer=cuenta.substring(0,2)
		numecta=cuenta.substring(3,11);
		digita=cuenta.substring(12,13);
		sin_formato=codicer + numecta + digita
	}

	///////////////////////////////////////////////////////////////////
	//
	//MostrarHora()
	//
	///////////////////////////////////////////////////////////////////
	function MostrarHora()
	  {
		 hora_actual= new Date();
		 horas=hora_actual.getHours();
		 minutos=hora_actual.getMinutes();
		 segundos= hora_actual.getSeconds(),
		 cadena_hora = "" + ((horas > 12) ? horas - 12 : horas);
		 cadena_hora += ((minutos < 10) ? ":0" : ":") + minutos;
		 //cadena_hora += ((segundos < 10) ? ":0" : ":") + segundos;
		 cadena_hora += (horas >= 12) ? " P.M." : " A.M.";
		 document.movimientos.txt_hora.value=cadena_hora;
		 //tiempo=setTimeout("MostrarHora()",1000);
	   }

	///////////////////////////////////////////////////////////////////
	//
	//Limpia()
	//
	///////////////////////////////////////////////////////////////////
	function Limpia()
	  {
	   document.form1.acct_selected.selectedIndex=-1;
	   timerONE= window.setTimeout('BarraNav(99)',100);
	   BarraNav();
	  }

	////////////////////////////////////////////////////////////////////
	//
	//BarraNav()	Coloca un mensaje en la barra del navegador
	//
	////////////////////////////////////////////////////////////////////
	function BarraNav(seed)
	{
	  m1="E n l a c e"
	  m2="                                                        "
	  m3=""
	  msg= m1 + m2 + m3
	  out=""
	  c=1
	  if (seed >100)
	  {
		seed--;
		cmd="BarraNav(" + seed + ")";
		timerTwo=window.setTimeout(cmd,100);
	  }
	  else if (seed <=100 && seed >0)
	  {
		for (c=0;c<seed;c++){
		out +=" ";
		}
	  out += msg;
	  seed--;
	  cmd="BarraNav(" + seed + ")";
	  window.status= out;
	  timerTwo=window.setTimeout(cmd,100);
	  }
	  else if(seed <=0)
	  {
		if (-seed <=0)
		{
		   out += msg.substring(-seed,msg.length);
		   seed--;
		   cmd="BarraNav(" + seed + ")";
		   window.status=out;
		   timerTwo=window.setTimeout(cmd,100);
		}
		else
		{
		   window.status="";
		   timerTwo=window.setTimeout("BarraNav(100)",100);
		}
	  }
	}


	////////////////////////////////////////////////////////////////////
	//
	// Carga_Login() Es llamada en el Load de la forma
	//
	////////////////////////////////////////////////////////////////////
	function Carga_Login()
	{
	 document.registro.usuarioTxt.focus();
	 timerONE= window.setTimeout('BarraNav(99)',100);
	 BarraNav();
	 Location();
	}

	////////////////////////////////////////////////////////////////////
	//
	// Carga_Contract() Es llamada en el Load de la forma
	//
	////////////////////////////////////////////////////////////////////
	function Carga_Contract()
	{
	 timerONE= window.setTimeout('BarraNav(99)',100);
	 BarraNav();
	 Location('ContractTmpl.html','registro');
	}

	////////////////////////////////////////////////////////////////////
	//
	// LTrim ()	Devuelve una cadena sin los espacios a la izquierda
	//
	///////////////////////////////////////////////////////////////////
	function LTrim (s)
	{
	 var i=0;
	 var j=0;
	 //Busca el primer caracter <> de un espacio
	 for(i=0; i<=s.length -1; i++)
		  if(s.substring(i,i+1) != ' ')
		  {
			  j=i;
			  break;
		  }
	 return s.substring(j,s.length);
	}
	//////////////////////////////////////////////////////////////////////
	//
	//RTrim () Quita los espacios en blanco a la derecha de una cadena
	//
	//////////////////////////////////////////////////////////////////////
	function RTrim (s)
	{
	  var j=0;
	  //Busca el ultimo caracter diferente de un espacio
	  for (var i=s.length-1;i>-1;i--)
			if(s.substring(i,i+1) != ' ')
			{
				j=i;
				break;
			}
	  return s.substring(0,j+1);
	}
	/////////////////////////////////////////////////////////////////////////
	//
	//Trim () Quita los espacios a la izquierda y derecha de una cadena
	//
	/////////////////////////////////////////////////////////////////////////
	function Trim (s)
	{
	  return LTrim(RTrim(s));
	}

	function ValidaEntradasLogin()
	{
		var result;

		result=validUsr(document.registro.usuarioTxt.value);
		//alert(document.registro.usuarioTxt.value);
		if (result == true)
		{
			document.registro.usuarioTxt.value=completeUsrID(document.registro.usuarioTxt.value);
			result=validPwd(document.registro.clave.value);
			if (result == true)
			{
				document.registro.submit();
			}
		}
	}


	var dayarray=new Array("Domingo","Lunes","Martes","Mi&eacute;rcoles","Jueves","Viernes","S&aacute;bado");
	var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");


	var mydate=new Date(<%=anio%>,<%=mes%>,<%=dia%>,<%=hh%>,<%=mm%>,<%=ss%>);

	function reloj()
	 {
	   if(!document.layers && !document.all)
		 return;


	   var digital = new Date();
	   var year=digital.getYear();
	   if (year < 1000)
		  year+=1900;
	   var day=digital.getDay();
	   var month=digital.getMonth();
	   var daym=digital.getDate();
	   if (daym<10)
		  daym="0"+daym;

	   var horas = digital.getHours();
	   var minutos = digital.getMinutes();
	   var segundos = digital.getSeconds();
	   var amOrPm = "AM";
	   if (horas > 11)
		 amOrPm = "PM";
	   if (horas > 12)
		 horas = horas - 12;
	   if (horas == 0)
		 horas = 12;
	   if (minutos <= 9)
		 minutos = "0" + minutos;
	   if (segundos <= 9)
		 segundos = "0" + segundos;

	   dispTime = "<font color='666666' face='Verdana'>" + dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" | "+horas+":"+minutos+":"+segundos+" "+ amOrPm +"</font>";
	   //dispTime = horas + ":" + minutos + ":" + segundos + " " + amOrPm;
	   if (document.layers)
		{
		  document.layers.objReloj.document.write("<font face=Tahoma size=1>"+ dispTime + "</font>");
		  document.layers.objReloj.document.close();
		}
	   else
		if (document.all)
		  objReloj.innerHTML = dispTime;
	   setTimeout("reloj()", 1000);
	 }


	function ComparePassword(str1,str2)
	{
		if(str1==str2)
		{
			return true;
		} else {
			return false;
		}
	}

	function DoPost()
	{
	if(document.Flogin.clave.value!="" && document.Flogin.clavenva.value!="" && document.Flogin.claveconf.value){
	   if(isInteger(document.Flogin.clave.value)){
		 if(isInteger(document.Flogin.clavenva.value) && isInteger(document.Flogin.claveconf.value)){
		   if(document.Flogin.clavenva.value==document.Flogin.claveconf.value){
			  //alert("entro al submit!1");
			  document.Flogin.submit();
		   }else{
			 cuadroDialogo("Error\nConfirmaci&#243;n de password incorrecto", 1);
			 document.Flogin.clavenva.value=null;
			 document.Flogin.claveconf.value=null;
			 document.Flogin.clsvenva.focus();
		   }
		 }else{
		  cuadroDialogo("Error\nLa contrase&#241;a debe ser n&#250;merica", 1);
		  document.Flogin.clavenva.value=null;
		  document.Flogin.claveconf.value=null;
		  document.Flogin.clsvenva.focus();
		 }
	   }else{
		 cuadroDialogo("Error\nNombre de usuario o contrase&#241;a no valido", 1);
	   }
	}
	else
	{
	 cuadroDialogo("Error\nLos campos no pueden ser vacios", 1);
	}
	}

	// distintivo

	//-->
	</script>

	<link href="/EnlaceMig/pagos.css" rel="stylesheet" type="text/css"/>
	</head>
	<body onLoad="reloj();limpiar()" bgcolor="#FFFFFF">
		<span id="objReloj" style="position:absolute;left:285;top:60;"></span>
		<table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
		  <tr>
			<td width="2"><img src="/gifs/EnlaceMig/cheque_img.jpg" width="147" height="40"/></td>
			<td width="593" valign="top" bgcolor="#FF0000"><img src="/gifs/EnlaceMig/top.gif" width="453" height="40"/></td>
		  </tr>
		  <tr>
			<td background="/gifs/EnlaceMig/sombra_gris.jpg"><img src="/gifs/EnlaceMig/sombra_gris.jpg" width="18" height="14"/></td>
			<td background="/gifs/EnlaceMig/sombra.jpg"><img src="/gifs/EnlaceMig/sombra.jpg" width="21" height="14"/></td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
				<td>
				  <table width="100%" border="0" cellspacing="0" cellpadding="6">
					<tr>
					  <td class="titulorojo"><div align="center">
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					  </td>
							<tr>
							  <td align="center" class="titulorojo">El pago ser&aacute; realizado
								a trav&eacute;s de
							  </td>
							  <td width="2" align="right"><a href="javascript:;" onClick="MM_openBrWindow('/EnlaceMig/ayuda001_enlace.html','ayuda','scrollbars=yes,resizable=yes,width=340,height=420')" onMouseOver="MM_swapImage('Image31','','/gifs/EnlaceMig/ayuda_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/ayuda.jpg" name="Image31" width="47" height="40" border="0" id="Image3"></a></td>
							</tr>
						  </table>
						</div>
				</td>
					</tr>
			  </table>
			</td>
		  </tr>

		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td><form name="registro" method="post" action="LoginServletMicrositio?opcion=0">
				<input name="convenio" type="hidden" value="<%=request.getAttribute("convenio")%>"/>
				<input name="importe" type="hidden" value="<%=request.getAttribute("importe")%>"/>
				<INPUT TYPE="hidden" name="servicio_id" value="<%=request.getAttribute("servicio_id")%>"/>
				<% if ("PMRF".equals((String)request.getAttribute("servicio_id"))){%>
					<input name="linea_captura" type="hidden" value="<%=request.getAttribute("linea_captura")%>"/>
				<%
				EIGlobal.mensajePorTrace("Loginmicrositio---.jsp::Pago referenciado :" + request.getAttribute("linea_captura"), EIGlobal.NivelLog.INFO);
				} else { %>
					<input name="referencia" type="hidden" value="<%=request.getAttribute("referencia")%>"/>
					<input name="url" type="hidden" value="<%=request.getAttribute("url")%>"/>
					<INPUT TYPE="hidden" name="concepto" value="<%=request.getAttribute("concepto")%>"/>
				<%} %>

				<table width="100%" border="0" cellspacing="6" cellpadding="0">
				  <tr align="center">
					<td colspan="2"><img src="/gifs/EnlaceMig/enlace.jpg" width="253" height="69" border="0"/></td>
				  </tr>
				  <tr align="center">
					<td class="td" align="right">C&oacute;digo de Cliente:&nbsp;&nbsp;</td>
					<td align="left"> <input type="text" name="usuarioTxt" SIZE="16" MAXLENGTH="8"/>
					<input type="hidden" name="usuario" SIZE="11" MAXLENGTH="8"/></td>
				  </tr>
				  <tr align="center">
					<td class="td" align="right">Contrase&ntilde;a de Enlace:&nbsp;&nbsp;</td>
					<td align="left"> <INPUT TYPE="password" NAME="clave" SIZE="16" MAXLENGTH="20"/></td>
				  </tr>
				  <tr align="center">
					<td class="td" align="right">Contrato:&nbsp;&nbsp;</td>
					<td align="left"><INPUT TYPE="text" NAME="contrato" SIZE="16" MAXLENGTH="11"/></td>
				  </tr>
				  <tr align="center">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr class="td" align="center">
					<td>&nbsp;</td>
					<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr align="center">
						  <td><a href="javascript:ValidaEntradasLogin();" onMouseOver="MM_swapImage('Image1','','/gifs/EnlaceMig/entrar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/entrar.jpg" alt="Procesa los datos" name="Image1" width="82" height="41" border="0" id="Image1"/></a></td>
						  <td><a href="javascript:limpiar();" onMouseOver="MM_swapImage('Image2','','/gifs/EnlaceMig/limpiar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/limpiar.jpg" alt="Limpia los campos" name="Image2" width="85" height="41" border="0" id="Image2"/></a></td>
						  <td><a href="javascript:history.back(1);" onMouseOver="MM_swapImage('Image4','','/gifs/EnlaceMig/regresar_over.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/gifs/EnlaceMig/regresar.jpg" alt="Regresa a la p&aacute;gina de inicio" name="Image4" width="82" height="41" border="0" id="Image4"/></a></td>
						</tr>
					  </table></td>
				  </tr>
				</table>
			  </form></td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td align="right"><img src="/gifs/EnlaceMig/sitio_seg.jpg" width="66" height="25"/>&nbsp;&nbsp;&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td bgcolor="#F0EEEE">&nbsp;</td>
		  </tr>
		  <tr bgcolor="#CCCCCC">
			<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="32"/></td>
			<td><img src="/gifs/EnlaceMig/spacer.gif" width="10" height="10"/></td>
		  </tr>
		</table>
	</body>
</html>
<%
if(session.getAttribute("MensajeLogin01") != null && request.getAttribute("MensajeLogin01") == null) {
	request.setAttribute("MensajeLogin01", session.getAttribute("MensajeLogin01"));
	session.removeAttribute("MensajeLogin01");
}
%>
<script>
<%= request.getAttribute("MensajeLogin01") %>
</script>