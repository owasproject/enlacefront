<%@page import="mx.altec.enlace.bita.BitaConstants"%>
<html>
<head>
<title>Banca Virtual</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<!-- JavaScript del App -->
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/Mancomunidad.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
<Script language = "Javascript" >
var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>"

/******************************************************************************************************* 
 *			INICIO BLOQUE CALENDAR CON 365 DIAS 
 *******************************************************************************************************/
	
	//Imprime arreglo de fechas con 365 dias enviado desde el servlet
	<%
		if(request.getAttribute("VarFechaHoy")!= null)
			out.print(request.getAttribute("VarFechaHoy"));
	%>
	 
	//Variable Indice para actualizar Calendario con fecha seleccionada
		var Indice=0;
	 
	 //FUNCION PARA ACTUALZIAR LA CAJA DETEXTO CON LA FECHA SELECCIONADA EN CALENDAR 
	 function Actualiza()
	 {
	     if(Indice==0)
			document.Frmgetinfobit.fecha1.value=Fecha[Indice];
		 if(Indice==1)
			document.Frmgetinfobit.fecha2.value=Fecha[Indice];
	 }
	
	//FUNCION PARA INVOCAR CALENDAR CON 365 DIAS
	 function js_calendario(ind){
		var m = new Date()
		Indice = ind;
	    n = m.getMonth();
	    msg = window.open("/EnlaceMig/EI_CalendarioLn.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
	    msg.focus();
	}
	
/******************************************************************************************************* 
 *			FIN BLOQUE CALENDAR CON 365 DIAS 
 *******************************************************************************************************/
function AgregaFolio()
{
	var h = document.fsaldo.elements.length;
	var folios=""; // = new Array (h);
	var j = 0;
	for (i=0;i<document.fsaldo.elements.length;i++)
	{
		var e = document.fsaldo.elements[i];
		if ((e.type == "checkbox") && e.checked)
		{
			folios = folios + document.fsaldo.elements[i].value + ";";
			j++;
		}
	}
	document.fsaldo.foliosPen.value = folios;
	document.fsaldo.j.value = j;

	if(j==0)
	{
		//alert("USTED NO HA SELECIONADO UNA CUENTA.\n\nPOR FAVOR SELECCIONE UNA CUENTA \nPARA TRAER SU SALDO");
		document.fsaldo.allbox.focus();
		cuadroDialogo("USTED NO HA SELECIONADO UNA CUENTA.\n\nPOR FAVOR SELECCIONE UNA CUENTA \nPARA TRAER SU SALDO", 1);
		return false;
	}
}

function CheckAll()
{
	for (var i=0;i<document.frmbit.elements.length;i++)
	{
		var e = document.frmbit.elements[i];
		if (e.name != 'allbox')
			e.checked = document.frmbit.allbox.checked;
	}
}


var Nmuestra = <%= request.getAttribute( "varpaginacion" ) %>;
function atras(){
    if((parseInt(document.frmbit.prev.value) - Nmuestra)>0){
      document.frmbit.next.value = document.frmbit.prev.value;
      document.frmbit.prev.value = parseInt(document.frmbit.prev.value) - Nmuestra;
      document.frmbit.action = "GetAMancomunidad";
      document.frmbit.submit();
    }
}

function checa_reg(val){
/*
   var valor = parseInt(val);
   if(valor = 1)
     {document.Frmgetinfobit.Registro[0].checked = true;
	 document.Frmgetinfobit.Registro[1].checked = false;
	 }

   if(valor = 2)
     {document.Frmgetinfobit.Registro[0].checked = false;
	 document.Frmgetinfobit.Registro[1].checked = true;
	 }
*/
}

function adelante(){
 if(parseInt(document.frmbit.next.value) != parseInt(document.frmbit.total.value)){
    if((parseInt(document.frmbit.next.value) + Nmuestra)< parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + Nmuestra;
       document.frmbit.action = "GetAMancomunidad";
	  document.frmbit.submit();
     }else if((parseInt(document.frmbit.next.value) + Nmuestra) > parseInt(document.frmbit.total.value)){
      document.frmbit.prev.value = document.frmbit.next.value;
      document.frmbit.next.value = parseInt(document.frmbit.next.value) + (parseInt(document.frmbit.total.value) - parseInt(document.frmbit.next.value));
      document.frmbit.action = "GetAMancomunidad";
      document.frmbit.submit();
     }
 }else{
   //alert("no hay mas registros");
   cuadroDialogo("No hay mas registros", 1);

     }
 }

 /*function WindowCalendar(){
//  if(!document.Frmgetinfobit.deldia[0].checked){
    var m=new Date()

    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);

    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMan2.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
 /* }else{
    alert("Aviso\nLa fecha no se puede cambiar para consultar la bitacora del dia");
  }
}*/

/*function WindowCalendar1(){
//  if(!document.Frmgetinfobit.deldia[0].checked){
    var m=new Date()
    m.setFullYear(document.Frmfechas.strAnio.value);
    m.setMonth(document.Frmfechas.strMes.value);
    m.setDate(document.Frmfechas.strDia.value);
    n=m.getMonth();
    msg=window.open("/EnlaceMig/calMan12.html#" + n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
    msg.focus();
  /*}else{
    alert("Aviso\nLa fecha no se puede cambiar para consultar la bitacora del dia");
  }
}*/

<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.Frmgetinfobit.cuenta.value=ctaselec;
  document.Frmgetinfobit.textcuenta.value=ctaselec;
}


function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

<%= request.getAttribute("newMenu") %>

//-->
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="document.Frmgetinfobit.Registro[0].checked = true;PutDate();MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
      <!-- MENU PRINCIPAL -->
        <%= request.getAttribute("MenuPrincipal") %>
    </TD>
  </TR>
</TABLE>

<%= request.getAttribute("Encabezado") %>

<br>

<form name = "Frmfechas">
  <%= request.getAttribute("Bitfechas") %>
</form>

<FORM NAME="Frmgetinfobit" METHOD = "POST" ACTION="GetAMancomunidad">
  <table width="760" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
        <table width="620" border="0" cellspacing="2" cellpadding="3">
          <tr>
            <td class="tittabdat"> Capture los datos para su consulta</td>
          </tr>
          <tr align="center">
            <td class="textabdatcla" valign="top">
              <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                  <td align="left" width="280">
                    <table width="280" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" nowrap class="tabmovtex11" width="100">Usuario (opcional): </td>
                        <td class="tabmovtex" nowrap width="185">
                          <SELECT NAME="usuario" class="tabmovtex">
                            <Option value = " ">Seleccione un usuario (opcional)</Option>
								<%= request.getAttribute("Usuarios") %>
                          </SELECT>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Consulta por:</td>
                        <td class="tabmovtex11" nowrap align="left" valign="middle">
                          <input type = "radio" name = "Registro" value = "FCH_REGISTRO" checked>&nbsp;Por Fecha de Registro
                        </td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">&nbsp;</td>
                        <td class="tabmovtex11" nowrap valign="middle" align="left">
                          <input type = "radio" name = "Registro" value ="FCH_AUTORIZAC">&nbsp;Por Fecha de Autorizaci&oacute;n<P>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">De la fecha:</td>
                        <td class="tabmovtex" nowrap valign="middle">
                          <input type="text" name="fecha1" size="12" class="tabmovtex" readonly="readonly" OnFocus = "blur();" value="">
                          <a href="javascript:js_calendario(0);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">A la fecha:</td>
                        <td class="tabmovtex" nowrap>
                          <input type="text" name="fecha2" size="12" class="tabmovtex" readonly="readonly" OnFocus = "blur();" value="">
                          <a href="javascript:js_calendario(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" style="vertical-align:middle;"></a>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Importe:</td>
                        <td class="tabmovtex" nowrap><!-- Q13160 Ivonne Diaz Getronics Mexico 06/12/04 -->
                          <INPUT TYPE="Text" NAME="Importe" SIZE="15" maxlength=15 class="tabmovtex">
                        </td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="2"><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11">Folio de Registro:</td>
                        <td nowrap class="tabmovtex"><!-- Q13160 Ivonne Diaz Getronics Mexico 06/12/04 -->
                          <INPUT TYPE="Text" NAME="Folio_Registro" SIZE="15" maxlength=15 class="tabmovtex">
                        </td>
                      </tr>
                    </table><!-- Tabla 280 -->
                  </td>
                  <td align="left" width="350">
                    <table width="350" border="0" cellspacing="5" cellpadding="0">
                      <tr>
                        <td align="right" class="tabmovtex11" nowrap>Cuenta (opcional):</td>
                        <td class="tabmovtex" colspan="4" valign="middle" align="center">
                          <input type="text" name=textcuenta class="tabmovtexbol" maxlength=22 size=22 onfocus="blur();" value=""> <A HREF="javascript:PresentarCuentas();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A><input type="hidden" name="cuenta"  value=""></td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" colspan="5" nowrap><img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5"></td>
                      </tr>
                      <tr>
                        <td align="right" class="tabmovtex11" width="89" nowrap>Tipo de operaci&oacute;n:</td>
                        <td width="27" valign="middle" align="center">
                          <INPUT TYPE="CheckBox" value = "'TRAN'," NAME="ChkTranf">
                        </td>
                        <td class="tabmovtex11"  width="212" nowrap>Transferencia</td>
                        <!-- Modificacion Mancomunidad Fase II LFER -->
                        <td width="27" valign="middle" align="center">
                          <INPUT TYPE="CheckBox" value = "'PNIS'," NAME="ChkNomIn">
                        </td>
                        <td class="tabmovtex11"  width="212" nowrap>N&oacute;mina Prepago</td>
                        <!--  Modificacion Mancomunidad Fase II LFER  -->
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'CPCI','VTSI','CPSI','CPDI','CPID','CPDV','CPVD'," NAME="ChkInver"></td>
                        <td class="tabmovtex11" nowrap>Inversiones</td>
                        <td width="27" valign="middle" align="center">
                          <INPUT TYPE="CheckBox" value = "'PNLI'," NAME="ChkNomLn">
                        </td>
                        <td class="tabmovtex11"  width="212" nowrap>N&oacute;mina en L&iacute;nea</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'DIBT'," NAME="ChkInterb"></td>
                        <td class="tabmovtex11" colspan="3" nowrap>Interbancario</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'PAGT','RG03'," NAME="ChkCred"></td>
                        <td class="tabmovtex11" colspan="3" nowrap>Cr&eacute;dito</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'ESMA','PIMP','RE03','IN04'" NAME="ChkServ"></td>
                        <td class="tabmovtex11" colspan="3" nowrap>Servicios </td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <!-- INPUT TYPE="CheckBox" value = "'ESOP'" NAME="ChkSUA"-->
                          <INPUT TYPE="CheckBox" value = "'SULC'" NAME="ChkSUA"></td>
                        <td class="tabmovtex11" colspan="3" 	nowrap>S.U.A. L&iacute;nea  de Captura</td>
                      </tr>
                      <tr valign="top">
                        <td class="tabmovtex11" nowrap colspan="5">
                          <img src="/gifs/EnlaceMig/gau25010.gif" width="50" height="5">
                        </td>
                      </tr>

                      <tr>
                        <td align="right" class="tabmovtex11" nowrap>Estatus:</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'A'," NAME="ChkAceptadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Autorizadas</td>

                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'P'," NAME="ChkAceptadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Pendientes</td>

                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'R'," NAME="ChkRechazadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Rechazadas</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'E'," NAME="ChkRechazadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Ejecutadas</td>
                      </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'C'," NAME="ChkCanceladas"></td>
                        <td class="tabmovtex11" nowrap>Canceladas</td>
                         <td align="center" valign="middle">
                           <INPUT TYPE="CheckBox" value = "'N'" NAME="ChkNoEjecutadas">
                         </td>
                         <td class="tabmovtex11" nowrap>No Ejecutadas</td>
 					  </tr>
                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'M'," NAME="ChkPendientes">
                        </td>
                        <td class="tabmovtex11" nowrap>Mancomunada</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'U'" NAME="ChkNoEjecutadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Pre-Autorizada</td>

                      </tr>

                      <tr>
                        <td align="right" nowrap>&nbsp;</td>
                        <td align="center" valign="middle">
                          <INPUT TYPE="CheckBox" value = "'V'," NAME="ChkEjecutadas">
                        </td>
                        <td class="tabmovtex11" nowrap>Verificada</td>
                        <td align="center" valign="middle">
                          &nbsp;
                        </td>
                        <td class="tabmovtex11" nowrap>&nbsp;</td>

                      </tr>
                    </table><!-- Tabla 320 -->
                  </td>
                </tr>
              </table><!-- Tabla 600 -->
            </td>
          </tr>
        </table><!-- Tabla 620 -->
        <br>
        <table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="right" valign="middle" width="90">
              <A href ="javascript:DoPost();">
			   <img border="0" name="boton" src="/gifs/EnlaceMig/gbo25220.gif" width="90" height="22" alt="Consultar"><!-- Modificacion req.Q05-8266 NAA -->
              </a>
            </td>
            <td align="left" valign="top" width="76">
              <A href ="javascript:FrmClean();">
                <img border="0" name="boton" src="/gifs/EnlaceMig/gbo25250.gif" width="76" height="22" alt="Limpiar"><!-- Modificacion req.Q05-8266 NAA -->
              </a>
            </td>
          </tr>
        </table>
        <br>
      </td>
    </tr>
</table><!-- Tabla 760 -->
</FORM>
</body>
</html>