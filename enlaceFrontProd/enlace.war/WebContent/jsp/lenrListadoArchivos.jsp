<%@page import="java.util.List"%>
<%@page import="mx.altec.enlace.beans.DetalleCuentasBean"%>
<html>
<head>
<title>Archivos</title>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript" src="/EnlaceMig/passmark.js"></script>
<script type="text/javascript">

function checkMsg(msg){
	if( msg != '' ){
		cuadroDialogo(msg,1);
	}
}

function procesaSubmit(){
	if(validaRadio()){
		document.frmArchivos.submit();
	}else{
		cuadroDialogo("Es necesario seleccionar un archivo para ver el detalle de cuentas a autorizar",1);
	}
}

function validaRadio(){
	var isSeleccionado = false;
	
	if(document.frmArchivos.radioValue.length == undefined && document.frmArchivos.radioValue.checked ){
		isSeleccionado = true;
	}else{
		for ( var i = 0; i < document.frmArchivos.radioValue.length; i++ ){
			if ( document.frmArchivos.radioValue[i].checked ){
				isSeleccionado = true;
			}
		}
	}
	return isSeleccionado;
}

/***********************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/


<%if (request.getAttribute("newMenu") != null) {%>
<%=request.getAttribute("newMenu")%>
<%}%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<%    
	   Object obj = null;
	   obj = request.getAttribute("msg");
	   String msg = ( obj != null )?obj.toString():"";
%>
<body onload="javascript:checkMsg('<%= msg%>');">
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
		<tr valign="top">
			<td width="*">
				<!-- MENU PRINCIPAL --> 
				<%if (request.getAttribute("MenuPrincipal") != null) {%>
					<%=request.getAttribute("MenuPrincipal")%> 
				<%}%>
			</TD>
		</TR>
	</TABLE>
	<% if (request.getAttribute("Encabezado") != null ) { %>
		<%= request.getAttribute("Encabezado") %>
	<%	} %>
	<%List lista = (List) session.getAttribute("listaArchivos");
	int size = (lista != null) ? lista.size() : 0; 
	if(size > 0 ){
	%>	
	<table width="760">
	<tr>
	<td>
	
	<center>
	<form name="frmArchivos" action="LiberaEmpleadosServlet" method="post">
		<input type="hidden" name="opcion" value="2" />
		<table width="670" align="center">
			<thead>
				<tr>
					<td class="tittabdat" align="center" bgcolor="#94b2df" height="20" colspan="5">
						<p class="tituloCopy">
							<font color="#000000"> Archivos con empleados pendientes de registrar </font>
						</p>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr bgcolor="#94b2df">
					<td class="tittabdat">Sel</td>
					<td class="tittabdat">Folio</td>
					<td class="tittabdat">Fecha</td>
					<td class="tittabdat">Registros</td>
					<td class="tittabdat">No registrados</td>
				</tr>
				<%
					List listaCuentas = (List) session.getAttribute("listaArchivos");
					int tamanio = (listaCuentas != null) ? listaCuentas.size() : 0;
					DetalleCuentasBean beanCuenta = null;
					boolean estatusArchivo = false;
					for (int i = 0; i < tamanio; i++) {
						beanCuenta = (DetalleCuentasBean) listaCuentas.get(i);
						String tdClass = ( (i+1)%2 == 0 )?"textabdatcla":"textabdatobs";
						estatusArchivo = beanCuenta.getEstado().equals("T") ? false : true;
				%>
					<tr bgcolor="#C8C8C8">
						<td class="<%= tdClass%>" align="center"><input type="radio" name="radioValue"
							value="<%=beanCuenta.getFolio()%>" <%if (estatusArchivo) {%>
							disabled="disabled" <%}%> /></td>
						<td class="<%= tdClass%>" align="right"><%=beanCuenta.getFolio()%></td>
						<td class="<%= tdClass%>" align="right"><%=beanCuenta.getFecha()%></td>
						<td class="<%= tdClass%>" align="right"><%=beanCuenta.getNumeroRegistros()%></td>
						<td class="<%= tdClass%>" align="right"><%=beanCuenta.getNoRegistrados()%></td>
					</tr>
				<%}%>
			</tbody>
		</table>
		<br>
		<center>
			<a href="javascript:procesaSubmit();"><img
				src="/gifs/EnlaceMig/registrarCuentas1.GIF" border="0" height="22"
				width="133">
			</a>
		</center>
	</form>
	</center>
	</td>
	</tr>
	</table>
	<%} else{%>
		<table border="0" WIDTH="571">
			<tr>
				<td align="center" height="20" class="textabdatobs">
					<p class="tituloCopy">
						<font color="#000000"><b> No existen cuentas por autorizar</b> </font>
					</p>
				</td>
			</tr>
		</table>
	<%} %>
	<%request.removeAttribute("msg"); %>
</body>
</html>