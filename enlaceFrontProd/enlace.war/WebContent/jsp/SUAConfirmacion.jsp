<%@page contentType="text/html"%>
<jsp:useBean id='Mensaje' class='java.lang.String' scope='request'/>

<%@page import="mx.altec.enlace.beans.SUALCLZS8"%>
<%@page import="mx.altec.enlace.bo.FormatoMoneda"%>
<html>
<head>
<title>SUA Confirmacion de pago</title>
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script languaje="javaScript">

var fechaLib;
var fechaLim;
var modulo = 'Registro';

/******************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function VerificaFechas(txtLibramiento, txtLimite)
 {

 lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
 lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
 lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
 fechaLibramiento = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtLimite.value.substring(6,10),10);
 lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
 lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
 fechaLimite  = new Date(lim_year, lim_month, lim_date);

     if( fechaLibramiento > fechaLimite)
      {
        //alert("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.");
		cuadroDialogo("La Fecha de Libramiento no puede ser mayor a la Fecha L�mite.", 3);
        return false;
      }

  return true;

 }


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function imprimir(mensaje)
{
	if(mensaje==null || mensaje=='null'){
		//comprobnte PDF
  		window.open("SUALineaCapturaServlet?opcionLC=6","Impresion","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=580,height=500");
  		return;
  	}else{
  		cuadroDialogo(mensaje, 3);
  		//Alerta de error
  	}
}


<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>"
background="/gifs/EnlaceMig/gfo25010.gif">			  <!-- modificar para cuando es NomBen y cuando es no registrado ya que lo esta tomando para nomben y este no existe obviamente --> <!-- <%--if (escribe && request.getAttribute("Escibe" ) == null) out.print ("frmRegistro.NomBen.value = " + Pago.getClaveBen () + ";");--%> -->


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
 <%= request.getAttribute("Encabezado") %>
 
<script type="text/javascript">

function regresar(){
	document.ConfirmaLC.opcionLC.value=1;
	document.ConfirmaLC.submit();
}
</script>
<form name="ConfirmaLC"  method="post" action="SUALineaCapturaServlet">
<%  SUALCLZS8 bean= null;
	if(request.getSession().getAttribute("beanSUALC")!=null){
	bean = (SUALCLZS8)request.getSession().getAttribute("beanSUALC");
	}
%>
<input type="hidden" name="opcionLC" value="6">
	<TABLE>
  		<TR>
  			<TD>
	   			<table width="960" border="0" cellspacing="0" cellpadding="0">
		  			<!--tr>
		    			<td colspan="3" align="center" width="820" valign="top">Contrato : <%=request.getAttribute("contrato") %></td>
		    		</tr-->
		    		<tr>
		    			<td colspan="3">&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td colspan="3" align="center" class="textabdatcla">Resultado de la operaci&oacute;n</td>
		    		</tr>
		    		
		    		<tr>
		    			<td>
		    				<table width="100%" height="100%" >
		    					<tr>
		    						<td class="tittabdat" colspan="4" align="center"><b>L&iacute;nea de Captura</b></td>
		    					</tr>
		    					<tr>
		    						<td class="textabdatobs" colspan="4"  align="center"><%=request.getAttribute("lc")%></td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Cuenta cargo</b></td>
		    						<td class="textabdatobs"><%=request.getAttribute("cargo")%>&nbsp;<%=request.getAttribute("titular")%></td>
		    						<td class="tittabdat"><b>Importe IMSS</b></td>
		    						<td class="textabdatobs">
		    							<%if(bean!=null && bean.getImporteImss()!=null){
			    						    try{
												double importeTotal=Double.valueOf(bean.getImporteImss()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal/100))%>
											<%}catch(NumberFormatException exception){%>
												<%=bean.getImporteImss()%>
											<%}%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Registro Patronal</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getRegPatronal()!=null){%>
		    							<%=bean.getRegPatronal()%>
		    							<%}%></td>
		    						<td class="tittabdat"><b>Importe RCV</b></td>
		    						<td class="textabdatobs">
		    							<%if(bean!=null && bean.getImporteRCV()!=null){
			    						    try{
												double importeTotal=Double.valueOf(bean.getImporteRCV()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal/100))%>
											<%}catch(NumberFormatException exception){%>
												<%=bean.getImporteRCV()%>
											<%}%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Periodo de Pago</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getPeriodoPago()!=null ){
		    								if(bean.getPeriodoPago().length()>4){%>
		    								Mes&nbsp;<%=bean.getPeriodoPago().substring(4,bean.getPeriodoPago().length())%>
		    								&nbsp;A�o&nbsp;<%=bean.getPeriodoPago().substring(0,4)%>
		    								<%}else{%>
		    									<%=bean.getPeriodoPago()%>
		    								<%}
		    							}%></td>
		    						<td class="tittabdat"><b>Importe Vivienda</b></td>
		    						<td class="textabdatobs">
										<%if(bean!=null && bean.getImporteVivienda()!=null){
			    						    try{
												double importeTotal=Double.valueOf(bean.getImporteVivienda()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal/100))%>
											<%}catch(NumberFormatException exception){%>
												<%=bean.getImporteVivienda()%>
											<%}%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Origen Archivo</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getOrigenArchivo()!=null){%>
		    							<%=bean.getOrigenArchivo()%>
		    							<%}%></td>
		    						<td class="tittabdat"><b>Importe ACV</b></td>
		    						<td class="textabdatobs">
		    							<%if(bean!=null && bean.getImporteACV()!=null){
			    						    try{
												double importeTotal=Double.valueOf(bean.getImporteACV()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal/100))%>
											<%}catch(NumberFormatException exception){%>
												<%=bean.getImporteACV()%>
											<%}%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Folio S.U.A.</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getFolioSua()!=null){%>
		    							<%=bean.getFolioSua()%>
		    							<%}%></td>
		    						<td class="tittabdat"><b>Importe Total</b></td>
		    						<td class="textabdatobs">
										<%if(bean!=null && bean.getImporteTotal()!=null){
			    						    try{
												double importeTotal=Double.valueOf(bean.getImporteTotal()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal/100))%>
											<%}catch(NumberFormatException exception){%>
												<%=bean.getImporteTotal()%>
											<%}%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Fecha de Vencimiento</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getFechaVencimiento()!=null){%>
		    							<%=((bean.getFechaVencimiento().replace("-","/")))%>
		    							<%}%>&nbsp;
		    						</td>
		    						<td class="tittabdat"><b>Estatus</b></td>

		    						<%if(request.getAttribute("estatus")!= null && request.getAttribute("estatus").equals("Operaci&oacute;n Mancomunada")) {%>
		    							<td class="textabdatobs" style="color: green"><%=request.getAttribute("estatus")%></td>
			    					<%}else{%>
		    							<td class="textabdatobs"><%=request.getAttribute("estatus")%></td>
		    						<%}%>

		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Usuario</b></td>
		    						<td class="textabdatobs"><%=request.getAttribute("usuario")%></td>
		    						<td class="tittabdat"><b>N&uacute;mero de Operaci&oacute;n</b></td>
		    						<td class="textabdatobs"><%if(bean!=null && bean.getCodOperacion()!=null){%>
		    							<%=bean.getCodOperacion()%>
		    							<%}%>
		    						</td>
		    					</tr>
		    					<tr>
		    						<td class="tittabdat"><b>Fecha y Hora</b></td>
		    						<td class="textabdatobs"><%=request.getAttribute("fecha")%>
		    						&nbsp;
		    							<%if(bean!=null && bean.getHoraOperacion()!=null){%>
		    							<%=((bean.getHoraOperacion()))%>
		    							<%}%>
		    						</td>
		    						<td class="tittabdat"><b>Referencia</b></td>
		    						<td class="textabdatobs"><a href="javaScript:imprimir('<%=request.getAttribute("mensajeErr") %>');"><%=request.getAttribute("referencia")%></a>
		    						</td>
		    					</tr>		    					
		    				</table>
		    			</td>
		    		</tr>
		    		<tr>
		    			<td align="center">
		    				<table border="0" cellspacing="0" cellpadding="0">
		    					<tr>
		    						<td valign="top">
		    							<%if (request.getAttribute("referencia")!=null && String.valueOf(request.getAttribute("referencia")).length()>0 ){%>
		    							<a href="javascript:imprimir('<%=request.getAttribute("mensajeErr") %>');"><img border="0" name="ImprimirLC" src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></a>
		    							<%}else{%>
		    								&nbsp;
		    							<%}%>
		    						</td>
		    						<td>&nbsp;</td>
		    						<td>
		    							<a href="javascript:regresar();"><img border="0" name="regresarLC" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></a>
		    						</td>
		    						<td>&nbsp;</td>
		    					</tr>
		    				</table>
		    			</td>
		    		</tr>
		    	</table>
			</TD>
		</TR>
	</TABLE>
</form>
<%if(request.getAttribute("mensaje") != null) {%>			
			<script type="text/javascript">
			<%=request.getAttribute("mensaje")%>
			</script>
		<%}%>	
</body>
</html>
