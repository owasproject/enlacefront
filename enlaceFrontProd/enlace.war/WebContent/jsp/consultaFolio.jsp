<html>
	<head>
		<title>Seguimiento de Transferencias</title>

		<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
		<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
		<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>
		<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
		<meta http-equiv="pragma" content="no-cache">
		<script language="JavaScript">

				function MM_preloadImages()	//v3.0
				{
			        var d=document;
					if(d.images)
					{
						if(!d.MM_p)
							d.MM_p=new Array();

						var i, j=d.MM_p.length, a=MM_preloadImages.arguments;
						for(i=0; i<a.length; i++)
							if (a[i].indexOf("#")!=0)
							{
								d.MM_p[j]=new Image;
								d.MM_p[j++].src=a[i];
							}
					}
				}

				function MM_swapImgRestore() //v3.0
				{
					var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
				}

				function MM_findObj(n, d)	//v3.0
				{
					  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
					    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
					  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
					  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
				}

				function MM_swapImage() 	//v3.0
				{
					var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
					if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
				}

				function runSubmit()
			    {
					//Modificacion 18:45 DVS 23 05 2007
					fechaInicialCaja = document.consulta.fechaInicial.value;
					fechaFinalCaja = document.consulta.fechaFinal.value;

					folIniCaja = document.consulta.folioInicialCaja;
					folioInicialCaja = document.consulta.folioInicialCaja.value;
					folioFinalCaja = document.consulta.folioFinalCaja.value;

					hiddenFolioInicial = document.consulta.folioInicial;
					hiddenFolioFinal = document.consulta.folioFinal;

					var banderaFolios = testFolios(folioInicialCaja, folioFinalCaja, hiddenFolioInicial, hiddenFolioFinal, folIniCaja);
					var banderaFechas = testFechas(fechaInicialCaja, fechaFinalCaja);

					if( (banderaFolios != (true)) || (banderaFechas != (true)) )
					{
						return;
					}//else

					if( (banderaFolios == true) && (banderaFechas == true) )
					{
						document.consulta.action="consultaServlet";
						document.consulta.submit();
					}

			    }


				function testFechas(fechaInicialCaja, fechaFinalCaja)
				{
					fechaInicialUsuario = obtenValorFecha(fechaInicialCaja);
					fechaInicialTope = obtenValorFecha(obtenFechaInicial());

					fechaFinalUsuario = obtenValorFecha(fechaFinalCaja);
					fechaFinalTope = obtenValorFecha(obtenFechaFinal());


					if( (fechaInicialUsuario >= fechaInicialTope) && ( (fechaFinalUsuario <= fechaFinalTope) && (fechaFinalUsuario >= fechaInicialTope) ) ) //Modificacin 31/05/2007 DVS le agregu el ltimo && a esta condicin
					{
						if(fechaInicialUsuario >  fechaFinalUsuario)
						{
							cuadroDialogo("Fecha Inicial Invalida.\nIntroduzca una fecha menor o igual a: " + fechaFinalCaja + " ",1);
							return false;
						}//else
						if(fechaInicialUsuario <= fechaFinalUsuario)
						{
							return true;
						}
					}//else
					if(fechaInicialUsuario < fechaInicialTope)
					{
						cuadroDialogo("Fecha Inicial Invalida.\nIntroduzca una fecha mayor o igual a: " + obtenFechaInicial() + " ", 1);
						return false;
					}//else
					if(fechaFinalUsuario > fechaFinalTope)
					{
						cuadroDialogo("Fecha Final Invalida.\nIntroduzca una fecha menor o igual a: " + obtenFechaFinal() + " ", 1);
						return false;
					}

				}

				function obtenValorFecha(fecha)
				{
					arrFecha = fecha.split("/");	//creo un arreglo
					strFecha = arrFecha[2] + arrFecha[1] + arrFecha[0];	//concateno los elementos del arreglo anterior
					valorFecha = parseInt(strFecha);		//convierto a numero la cadena
					return valorFecha;						//regreso un valor numerico
				}


				function obtenFechaInicial()	//La fecha de hace un mes (30 dias)
				{
					milisegundos=parseInt(30*24*60*60*1000);	//Obtengo los milisegundos de 30 das

					fecha =new Date();
					tiempo=fecha.getTime();
					total=fecha.setTime(parseInt(tiempo-milisegundos));
					dia=fecha.getDate();
					mes=fecha.getMonth()+1;

					if (fecha.getYear() < 2000){
						anio = fecha.getYear() + 1900;
					}else{
						anio = fecha.getYear();
					}

					str_dia = new String(dia);
					str_mes = new String(mes);
					str_anio = new String(anio);

					if(str_dia.length == 1)
						str_dia = "0" + str_dia;
					if(str_mes.length == 1)
						str_mes = "0" + str_mes;

					fechaMenos30Dias = str_dia + "/" + str_mes + "/" + str_anio;
					return fechaMenos30Dias;

				}

				function obtenFechaFinal()	//la fecha de hoy
				{

					hoy=new Date();
					dia = hoy.getDate();
					mes = hoy.getMonth() + 1;

					if (hoy.getYear() < 2000){
						anio = hoy.getYear() + 1900;
					}else{
						anio = hoy.getYear();
					}

					str_dia = new String(dia);
					str_mes = new String(mes);
					str_anio = new String(anio);

					if(str_dia.length == 1)
						dia = "0" + dia;
					if(str_mes.length == 1)
						mes = "0" + mes;

					fecha_final = dia + "/" + mes + "/" + anio;
					return fecha_final;
				}


				function testFolios(folioInicialCaja, folioFinalCaja, hiddenFolioInicial, hiddenFolioFinal, folIniCaja)
				{
					numFolIni = parseInt(folioInicialCaja);
					numFolFin = parseInt(folioFinalCaja);

					if( (folioInicialCaja == null && folioFinalCaja == null) || (folioInicialCaja == null && folioFinalCaja == "") || (folioInicialCaja == "" && folioFinalCaja == null) || (folioInicialCaja == "" && folioFinalCaja == "") )
					{
						folioInicialCaja = "";
						folioFinalCaja = "";
						hiddenFolioInicial.value = " ";
						hiddenFolioFinal.value = " ";
						return true;
					}//else
					if( (folioInicialCaja == null && folioFinalCaja != null) || (folioInicialCaja == null && folioFinalCaja != "") || (folioInicialCaja == "" && folioFinalCaja != null) || (folioInicialCaja == "" && folioFinalCaja != "") )
					{
						folioInicialCaja = folioFinalCaja;
						hiddenFolioInicial.value = folioInicialCaja;
						hiddenFolioFinal.value = folioFinalCaja;
						return true;
					}//else
					if( (folioInicialCaja != null && folioFinalCaja == null) || (folioInicialCaja != null && folioFinalCaja == "") || (folioInicialCaja != "" && folioFinalCaja == null) || (folioInicialCaja != "" && folioFinalCaja == "") )
					{
						folioFinalCaja = folioInicialCaja;
						hiddenFolioFinal.value = folioFinalCaja;
						hiddenFolioInicial.value = folioInicialCaja;
						return true;
					}//else
					if( (folioInicialCaja != null && folioFinalCaja != null) || (folioInicialCaja != null && folioFinalCaja != "") || (folioInicialCaja != "" && folioFinalCaja != null) || (folioInicialCaja != "" && folioFinalCaja != "") )
					{
						if(numFolIni <= numFolFin)
						{
							hiddenFolioInicial.value = folioInicialCaja;
							hiddenFolioFinal.value = folioFinalCaja;
							return true;
						}
						if(numFolIni > numFolFin)
						{
							cuadroDialogo("El Folio Final debe ser mayor al Folio Inicial", 1);
							folioInicialCaja = "";
							folioFinalCaja = "";
							folIniCaja.focus();
							return false;
						}//else
					}
				}

				function cargaFechasEnCampos() 		//Carga la fecha inicial y final en los campos respectivos
				{
					fecInicial = obtenFechaFinal();						//Obtiene la fecha de hoy
					fecFinal = obtenFechaFinal();						//Obtiene la fecha de hoy
					document.consulta.fechaInicial.value = fecInicial;	//Asigna la fecha de hace 30 dias al input type text fechaInicial
					document.consulta.fechaFinal.value = fecFinal;		//Asigna la fecha de hoy al input type text fechaFinal
				}

				var nav4 = window.Event ? true : false;
				function acceptNum(evt)
				{
					// NOTA: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
					var key = nav4 ? evt.which : evt.keyCode;
					return (key <= 13 || (key >= 48 && key <= 57));
				}

				var js_diasInhabiles="<%= request.getAttribute("diasInhabiles") %>"
				function WindowCalendar()
				{
				    var m = new Date();
    				m.setFullYear(document.Frmfechas.strAnio.value);
    				m.setMonth(document.Frmfechas.strMes.value);
    				m.setDate(document.Frmfechas.strDia.value);
    				n=m.getMonth();
				    msg=window.open("/EnlaceMig/calSeguimiento2.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=380,height=450");
				    msg.focus();
				}


				function cuadroDialogoTransferencias(urlArchivo)
				{
					respuesta=0;
					var titulo="Descarga de Archivo";
					var imagen="gic25020.gif";

					imagen ="gic25050.gif";
					titulo ="Descarga Archivo";
					mensaje = "Para descargar el archivo, d&eacute; click en Crear Archivo";

					ventana=window.open('about:blank','_blank','width=380,height=180,resizable=yes,toolbar=no,scrollbars=no,left=210,top=225');ventana.focus();
					ventana.document.open();
					ventana.document.writeln("<html>");
					ventana.document.writeln("<head>\n<title>"+titulo+"</title>\n");
					ventana.document.writeln("<link rel='stylesheet' href='/EnlaceMig/consultas.css' type='text/css'>");
					ventana.document.writeln("</head>");
					ventana.document.writeln("<body bgcolor='white' onLoad=\"javascript:window.focus();\">");
					ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=3 cellpadding=0>");
					ventana.document.writeln("<tr><th height=25 class='tittabdat' colspan=2>"+ titulo + "</th></tr>");
					ventana.document.writeln("</table>");
					ventana.document.writeln("<table border=0 width=365 class='textabdatcla' align=center cellspacing=0 cellpadding=1>");
					ventana.document.writeln("<tr><td class='tabmovtex1' align=right width=50><img src='/gifs/EnlaceMig/"+imagen+"' border=0></td>");
					ventana.document.writeln("<td class='tabmovtex1' align=center width=300><br>"+mensaje+"<br><br>");
					ventana.document.writeln("</td></tr>");
					ventana.document.writeln("<tr><td class='tabmovtex1'>");
					ventana.document.writeln("</td></tr>");
					ventana.document.writeln("</table>");
					ventana.document.writeln("<table border=0 align=center cellspacing=6 cellpadding=0>");
					ventana.document.writeln("<tr><td align=right><a href=\"" + urlArchivo + "\" target=_blank onmouseup=\"javascript:window.setTimeout('window.close();',12000);\"><img src='/gifs/EnlaceMig/gbo25550.gif' border=0></a></td>");
					ventana.document.writeln("<td align=left><a href='javascript:window.close();'><img src='/gifs/EnlaceMig/gbo25190.gif' border=0></a></td></tr>");
					ventana.document.writeln("</table>");
					ventana.document.writeln("</body>\n</html>");
					ventana.document.close();
					ventana.focus();


				}//fin de function cuadroDialogoTransferencias


				<%= request.getAttribute("newMenu") %>

		</script>

	</head>

	<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<%
String[][] arrayUsuarios = (String [][]) request.getAttribute("usuarios");
if(arrayUsuarios == null) {
	arrayUsuarios = new String[1][3];
	arrayUsuarios[0][0] = "0";
}


%>

<form name = "Frmfechas">
  <%=request.getAttribute("Consfechas")%>
</form>


		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
			<tr valign="top">
				<td width="*">
					<!-- MENU PRINCIPAL -->
					<%= request.getAttribute("MenuPrincipal") %>
				</td>
			</tr>
		</TABLE><br>
		<%= request.getAttribute("Encabezado") %>




		<table width="760" border="0" cellspacing="0" cellpadding="0">
		<form method="post" name="consulta"> <!-- dentro del tag form estaba un onSubmit="runSubmit(this.form)" -->
				<input type="hidden" name="entrada" value="2">
				<input type="hidden" name="valorcal" value="">
				<input type="hidden" name="folioInicial">
				<input type="hidden" name="folioFinal">
				<INPUT TYPE="HIDDEN" NAME="fecha_hoy" VALUE = '<%= request.getAttribute("fecha_hoy") %>' >
			<tr>
				<td align="center">
						<table width="490" border="0" cellspacing="2" cellpadding="3">
							<tr>
								<td class="tittabdat" colspan="2">
									Capture los datos de su Consulta
								</td>
							</tr>
							<tr align="center">
								<td class="textabdatcla" valign="top" colspan="2">
									<table width="480" border="0" cellspacing="0" cellpadding="0">
										<tr valign="top">
											<td width="270">
												<table width="270" border="0" cellspacing="5" cellpadding="0">
													<tr>
														<td class="tabmovtexbol" width="158" nowrap>
															Folio Inicial:
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" width="158" nowrap>
															<input  tabindex="1" align="rigth" type="text" name="folioInicialCaja" class="tabmovtexbol" maxlength=12 size=22 onKeyPress="return acceptNum(event)"></input>
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" nowrap valign="middle">
															Fecha Inicial:
														</td>
													</tr>

													<tr>
														<td class="tabmovtexbol" nowrap>
															<input  type="text" name="fechaInicial" class="tabmovtexbol" maxlength=10 size=22 onfocus="this.blur()"></input>
															<a href="javascript:WindowCalendar();" onclick="javascript:document.consulta.valorcal.value=1" ><img tabindex="3" src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"></a>
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" width="158" nowrap>
															Usuario:
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" width="158" nowrap>
															<select name="usuario" class="tabmovtex" style="width:158px">
																 <option value="" selected>Seleccione un usuario</option>
																 <%for (int indice=1;indice<=Integer.parseInt(arrayUsuarios[0][0].trim());indice++){ %>
																 	<OPTION value = "<%=arrayUsuarios[indice][1]%>"><%=arrayUsuarios[indice][1]%> <%=arrayUsuarios[indice][2]%></OPTION>
																 <%}%>
															</select>


														</td>
													</tr>


												</table>
											</td>
											<td width="200" align="right">
												<table width="180" border="0" cellspacing="5" cellpadding="0">
													<tr>
														<td class="tabmovtexbol" nowrap>
															Folio Final:
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" nowrap>
															<input tabindex="2" type="text" name="folioFinalCaja" class="tabmovtexbol" maxlength=12 size=22 onKeyPress="return acceptNum(event)"></input>
														</td>
													</tr>
													<tr>
														<td class="tabmovtexbol" nowrap valign="middle">
															Fecha Final:
														</td>
													</tr>

													<tr>
														<td class="tabmovtexbol" nowrap>
															<input  type="text" name="fechaFinal" class="tabmovtexbol" maxlength=10 size=22 onfocus="this.blur()"></input>
															<a href="javascript:WindowCalendar();" onclick="javascript:document.consulta.valorcal.value=2"><img tabindex="4" src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0"></a>
														</td>
													</tr>


												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<table width="490" border="0" cellspacing="2" cellpadding="3">
							<tr>
								<td class="textabref">
									D&eacute; click sobre el bot&oacute;n CONSULTAR para verificar
									el estatus y el resultado de su transferencia.
								</td>
							</tr>
						</table>
						<br>
						<table border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
							<tr>

								<td align="right" valign="top" width="85">
									<!--<a tabindex="5" href="javascript:runSubmit(this.form)" onClick="runSubmit(this.form)"> <img border="0" src="/gifs/EnlaceMig/gbo25220.gif"> </a> -->
									<A  tabindex="5" href="#" onClick="runSubmit()" border=0><img src='/gifs/EnlaceMig/gbo25220.gif' border=0 alt="Enviar Consulta"></A>
								</td>


								<td valign="top" width="76">
									<a tabindex="6" href="consultaServlet?entrada=1"> <img border="0" src="/gifs/EnlaceMig/gbo25250.gif"> </a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<br>
			</form>
		</table>


			<% if (session.getAttribute("codigoHTML") != null ) { %>
					<%= session.getAttribute("codigoHTML") %>


					<script>
					<% if (request.getAttribute("MensajeLogin01") != null ) { %>
						<%= request.getAttribute("MensajeLogin01") %>
					<%	} %>
					</script>
			<%	} %>

			<script language="JavaScript">


				<%	if(request.getAttribute("URLArchivo") != null) { %>
					cuadroDialogoTransferencias(<%= request.getAttribute("URLArchivo") %>);

				<% } %>


				<% if (session.getAttribute("folioInicialSession") != null )
					{
						if( (session.getAttribute("folioInicialSession") == " ") || (session.getAttribute("folioInicialSession") == "") )
						{ %>

							document.consulta.folioInicialCaja.value = "";
							document.consulta.folioInicial.value = "";

					<%	} else if( (session.getAttribute("folioInicialSession") != " ") && (session.getAttribute("folioInicialSession") != "") ) { %>
							document.consulta.folioInicialCaja.value = "<%= session.getAttribute("folioInicialSession") %>";
						<%}
					}
				%>


				<% if (session.getAttribute("folioFinalSession") != null )
					{
						if( (session.getAttribute("folioFinalSession") == " ") || (session.getAttribute("folioFinalSession") == "") )
						{ %>
							document.consulta.folioFinalCaja.value = "";
							document.consulta.folioFinal.value = "";

					<%	} else if( (session.getAttribute("folioFinalSession") != " ") && (session.getAttribute("folioFinalSession") != "") ) { %>
							document.consulta.folioFinalCaja.value = "<%= session.getAttribute("folioFinalSession") %>";
						<%}
					}
				%>


				<% if ( (session.getAttribute("fechaInicialSession") != null ) ||(session.getAttribute("fechaFinalSession") != null) ) { %>
				document.consulta.fechaInicial.value = '<%= session.getAttribute("fechaInicialSession") %>';
				document.consulta.fechaFinal.value ='<%= session.getAttribute("fechaFinalSession") %>';
				<%}else { %>
					cargaFechasEnCampos();
				<%} %>
			</script>




	</body>

</html>