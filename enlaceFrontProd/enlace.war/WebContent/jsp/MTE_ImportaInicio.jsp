<html>
<head>
	<title>TESOFE - Importaci&oacute;n</TITLE>

<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javascript1.2" src="/EnlaceMig/scrImpresion.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

<%
if(request.getAttribute("ErroresEnArchivo")!=null)
 {
   System.out.println("Errores en archivo != null " + request.getAttribute("ErroresEnArchivo"));
   out.print(request.getAttribute("ErroresEnArchivo"));
 }
%>

function js_despliegaErrores(archivo)
 {
   ventanaInfo1=window.open('/Download/'+archivo,'errWindow','width=450,height=300,toolbar=no,scrollbars=yes');
   ventanaInfo1.focus();
 }

function js_importar()
 {
	var tipo="";
	var forma=document.TesImporta;

	if(trimString(forma.Archivo.value)=="")
	 {
		cuadroDialogo("Debe especificar nombre de Archivo.",3);
		return;
	 }
	forma.action="MTE_Importar";
	forma.submit();
 }

function VentanaAyuda(cad)
 {
   return;
 }

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
	 if(request.getAttribute("strScript") !=null)
		out.println( request.getAttribute("strScript") );
	 else
	  out.println("");
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
	  if(request.getAttribute("newMenu")!=null)
		out.println(request.getAttribute("newMenu"));
	 else
	   out.println("");
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<body background="/gifs/EnlaceMig/gfo25010.gif" bgColor=#ffffff leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<!-- MENU PRINCIPAL -->
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
	 if(request.getAttribute("MenuPrincipal")!=null)
		out.println(request.getAttribute("MenuPrincipal"));
	 else
	  out.println("");
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
	 if(request.getAttribute("Encabezado")!=null)
		out.println(request.getAttribute("Encabezado"));
	 else
	  out.println("");
%>


 <FORM   NAME="TesImporta" METHOD="Post" ACTION="MTE_Importar" enctype="multipart/form-data">

  <table width="760" border=0>
   <tr>
   <td class="texenccon" align=center valign="top">

        <table border="0" cellspacing="2" cellpadding="3" align=center>
		 <tr>
		   <td class="tittabdat" nowrap> Seleccione el archivo a importar</td>
		 </tr>
		 <tr>
		   <td class="textabdatcla" valign="top" >

			 <table border="0" cellspacing="0" cellpadding="0">
			   <tr>
			     <td class="tabmovtex">Buscar archivo:</td>
			   </tr>

			   <tr>
			     <td>
				   <INPUT TYPE="file" NAME="Archivo" size="20">
				 </td>
			   </tr>

			   <tr>
				 <td>
				  <br>
				 </td>
			   </tr>

			   <tr>
				 <td class="tabmovtex">
				   Categor&iacute;a de importaci&oacute;n:
				 </td>
			   </tr>

				<tr>
			     <td class="tabmovtex">
				   <dd><input type=radio name=Tipo value=Cancelaciones CHECKED>Archivo de Cancelaciones
				 </td>
				</tr>

				<tr>
				  <td class="tabmovtex">
				   <dd><input type=radio name=Tipo value=Devoluciones>Archivo de Devoluciones
				 </td>
			    </tr>

              </table>

           </td>
         <tr>
		   <td align=center>
		     <br><A href = "javascript:js_importar();" border=0><img src="/gifs/EnlaceMig/gbo25300.gif" border=0 alt="Importar" width="85" height="22"></a>
		   </td>
		 </tr>
       </table>

	</td>
	</tr>
	</table>

	<%
	String NomContrato = "";
	String NumContrato = "";
	String NumUsuario = "";
	String NomUsuario = "";
	String ClaveBanco = "";
	String webApp="";

	if(session.getAttribute("NomContrato") !=null)
	  NomContrato= (String)session.getAttribute("NomContrato");
	else
	 if(request.getAttribute("NomContrato")!=null)
	   NomContrato= (String)request.getAttribute("NomContrato");

	if(session.getAttribute("NumContrato") !=null)
	  NumContrato = (String)session.getAttribute("NumContrato");
	else
	 if(request.getAttribute("NumContrato")!=null)
	   NumContrato = (String)request.getAttribute("NumContrato");

	if(session.getAttribute("NomUsuario") !=null)
	  NomUsuario = (String)session.getAttribute("NomUsuario");
	else
	 if(request.getAttribute("NomUsuario")!=null)
	  NomUsuario = (String)request.getAttribute("NomUsuario");

	if(session.getAttribute("NumUsuario") !=null)
	  NumUsuario = (String)session.getAttribute("NumUsuario");
	else
	 if(request.getAttribute("NumUsuario")!=null)
	  NumUsuario = (String)request.getAttribute("NumUsuario");

	if(session.getAttribute("ClaveBanco") !=null)
	  ClaveBanco = (String)session.getAttribute("ClaveBanco");
	else
	  if(request.getAttribute("ClaveBanco")!=null)
	    ClaveBanco = (String)request.getAttribute("ClaveBanco");

	if(session.getAttribute("web_application")!=null)
	   webApp = (String)session.getAttribute("web_application");
	else
	 if (request.getAttribute("web_application")!= null)
	   webApp = (String)request.getAttribute("web_application");
	%>

	<input type=hidden name=NomContrato value='<%=NomContrato%>'>
	<input type=hidden name=NumContrato value='<%=NumContrato%>'>
	<input type=hidden name=NomUsuario  value='<%=NomUsuario%>'>
	<input type=hidden name=NumUsuario  value='<%=NumUsuario%>'>
	<input type=hidden name=ClaveBanco  value='<%=ClaveBanco%>'>
	<input type=hidden name=WebApp value='<%=webApp.trim()%>'>

	<input type=hidden name=pagina value='2'>
	<input type=hidden name=operacion value='TESOFE'>

 </FORM>
</body>
</html>