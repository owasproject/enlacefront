<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<%@ page import="mx.altec.enlace.bita.BitaConstants"%>
<jsp:include page="/jsp/prepago/headerPrepago.jsp"/>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script language="javascript">
function validar()
{
	var f=document.resultadoRemesas;
	var contador=0;
	for (i=0;i<f.elements.length;i++){
		if (f.elements[i].type=="radio"){
			if (f.elements[i].checked==true){
				contador=1;
			}
		}
	}
	if (contador==1){
		return true;
	}else{
		cuadroDialogo("Usted no ha seleccionado una remesa",1);
		return false;
	}
	return true;
}
function exportar() {
	var f=document.resultadoRemesas;
	f.operacion.value="4";
	f.salida.value = "excel";
	f.submit();
}
function registro(){
	//if(validar()){
		//cuadroDialogo("&#191;Desea recibir la remesa?",2);
	//}
	if(validar())
	{
	var f=document.resultadoRemesas;
	f.operacion.value="5";
	f.submit();
	}
}

function regresar(){
	document.resultadoRemesas.operacion.value="2";
	document.resultadoRemesas.submit();
}

function imprimir() {
	scrImpresion();
}

function continua()
{
	if(respuesta==1) {
		document.resultadoRemesas.operacion.value="5";
		document.resultadoRemesas.submit();
	}
}

/*Funcion para el boton exportar*/
function downloadFile(){
	document.resultadoRemesas.operacion.value="11";
	document.resultadoRemesas.submit();
}
</script>
<form action="${ctx}AdmonRemesasServlet" method="get" name="resultadoRemesas">
	<input type="hidden" name="operacion" value="1">
	<%
		LMXC lmxc = (LMXC) request.getAttribute("RemesasBean");
		if (lmxc != null && lmxc.getListaRemesas() != null && lmxc.getListaRemesas().size() > 0) {
			int numRemesas = lmxc.getListaRemesas().size();
	%>
		<table cellspacing="0" cellpadding="0" border="0" width="760">
			<tr>
				<td align="center" class="tabmovtex" >
					Total de remesas: <%=numRemesas%>
					<input type="hidden" name="fecha2" value="<%= lmxc.getFechaInicio()%>">
					<input type="hidden" name="fecha1" value="<%= lmxc.getFechaFinal()%>">
					<input type="hidden" name="tipoFecha" value="<%= lmxc.getTipoFecha()%>">
					<input type="hidden" name="folioRemesa2" value="<%= lmxc.getNumeroRemesa2()%>">

				</td>
			</tr>
			<tr>
				<td align="center">
				<table width="500" border="0" cellspacing="1" cellpadding="2" class="tabfonbla">
					<tr>
						<td align="center" width="50" class="tittabdat">
						</td>
						<td align="center" width="275" class="tittabdat">N&uacute;mero Remesa
						</td>
						<td align="center" width="275" class="tittabdat">Fecha de Emisi&oacute;n
						</td>
						<td align="center" width="275" class="tittabdat">Fecha de Confirmaci&oacute;n o Cancelaci&oacute;n
						</td>
						<td align="center" width="275" class="tittabdat">N&uacute;mero de Registros
						</td>
						<td align="center" width="275" class="tittabdat">Estatus
						</td>
					</tr>
					<%
					int tamano = lmxc.getListaRemesas().size();

					Iterator iterator = lmxc.getListaRemesas().iterator();

					boolean claro = false;
					int i=0;
					while (iterator.hasNext()) {

						RemesasBean remesa = (RemesasBean) iterator.next();
						String sClass = claro ? "textabdatcla" : "textabdatobs";
					%>
					<tr>
						<td align="center" width="60" class="<%=sClass%>">
							<input type="radio" name="folioRemesa" value="<%= remesa.getNumRemesa()%>|<%= remesa.getCentroDistribucion()%>">
							<input type="hidden" name="numCtroDist" value="<%= remesa.getCentroDistribucion()%>">

						</td>
						<td class="<%=sClass%>"> <%= remesa.getNumRemesa() %></td>
						<td class="<%=sClass%>"><%= remesa.getFechaAltaRem() %></td>
						<% if(remesa.getEstadoRemesa().equals("CANCELADA")){%>
							<td class="<%=sClass%>"><%= remesa.getFechaBajaRem() %></td>
						<%}else if(remesa.getEstadoRemesa().equals("ENVIADA") || remesa.getEstadoRemesa().equals("NUEVA")){ %>
							<td class="<%=sClass%>"></td>
						<%}else if(remesa.getEstadoRemesa().equals("ASIGNADA")){ %>
							<td class="<%=sClass%>"><%= remesa.getFechaRecepRem() %></td>
						<%}else{ %>
							<td class="<%=sClass%>"></td>
						<%} %>
						<td class="<%=sClass%>"><%= remesa.getNumTarjetas() %></td>
						<td class="<%=sClass%>"><%= remesa.getEstadoRemesa() %></td>
					</tr>
					<%
						claro = !claro;
						i++;
					}
					%>

				</table>
			</td>
			</tr>
			<tr>
				<td align="center" class="tabmovtex" >
					Remesas 1 - <%=numRemesas%> de <%=numRemesas%>
				</td>
			</tr>
			<tr>
			<td>
				<table height="40" cellspacing="0" cellpadding="0" border="0" width="760">
					<tr><td></td></tr>
					<tr>
						<td align="center" class="texfootpagneg">
							<table height="40" cellspacing="0" cellpadding="0" border="0" width="168">
								<tr>
									<td>
						 		 		<a href = "javascript:registro();"><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consultar" width="80" height="22"></a>
                        			</td>
									<td>
										<a href="javascript:scrImpresion();"><img src="/gifs/EnlaceMig/gbo25240.gif" border=0 alt="Imprimir" width="80" height="22"></a>
									</td>
									<td>
										<a href="javascript:downloadFile();"><img src="/gifs/EnlaceMig/gbo25230.gif"  border="0" alt="Exportar" width="80" height="22"/></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
			</td>
		</tr>
	</table>
	<%
			}else{
		%>
<table width="760" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table cellspacing="2" cellpadding="3" border="0" width="369" class="tabfonbla">
				<tr>
					<td align="left" class="tittabdat">Consulta de remesas</td>
				</tr>
				<tr>
					<td align="center" class="textabdatobs">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
			<a href="javascript:regresar();"><img src="/gifs/EnlaceMig/gbo25320.gif" border="0" alt="Regresar" width="80" height="22"/></a>
    	</td>
	</tr>
</table>
<%	} %>
</form>
<jsp:include page="/jsp/prepago/footerPrepago.jsp"/>