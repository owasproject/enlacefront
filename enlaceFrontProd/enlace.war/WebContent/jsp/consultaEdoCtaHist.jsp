<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
 <head>
  <title>Enlace Banco Santander Mexicano</title>

  <script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
  <script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
  <script type="text/javascript" src="/EnlaceMig/ConMov.js"></script>
  <script type="text/javascript">

    var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>";
    var dia;
    var mes;
    var anio;
    var fechaseleccionada;
    var opcionCalendario;
    var fecha_completa = "";
    
    function validaFormatoFecha(){
    	var re, fechaIni, f1, fhoy;
    	var dia = document.Frmfechas.strDia.value;	
		var mes = document.Frmfechas.strMes.value;	
		var ann = document.Frmfechas.strAnio.value;
    	
    	fechaIni = document.FrmMov.fechaIni.value;
    	f1 = fechaIni.substring(6,10) + fechaIni.substring(3,5) + fechaIni.substring(0,2);
    	fhoy = ann + mes + dia;
    	
    	re = /^(\d{2})\/(\d{2})\/(\d{4})$/;
    	
    	if(fechaIni != ''){
        	if(!re.test(fechaIni)) {
        		cuadroDialogo("La fecha debe tener un formato valido DD/MM/YYY",4);
        		cargaFecha();
        	}
        }else{
        	document.FrmMov.fechaFin.value = '';
        }
    }
    
    function validaBusqueda(){
    	var folio, usuarioSol, fechaIni, fechaFin, estatus, fcomp1, fcomp2, fhoy;
    	folio = document.FrmMov.folio.value;
    	usuarioSol = document.FrmMov.usuarioSol.value;
    	fechaIni = document.FrmMov.fechaIni.value;
    	fechaFin = document.FrmMov.fechaFin.value;
    	estatus = document.FrmMov.estatus.value;
        
        fcomp1 = new Date(fechaIni.substring(6,10),fechaIni.substring(3,5)-1,fechaIni.substring(0,2));
        fcomp2 = new Date(fechaFin.substring(6,10),fechaFin.substring(3,5)-1,fechaFin.substring(0,2));
        
        fhoy = document.Frmfechas.strAnio.value + document.Frmfechas.strMes.value + document.Frmfechas.strDia.value;
            
        if(folio == "" && fechaIni == "" && fechaFin == "" && estatus == ""){
    		cuadroDialogo("Por favor, debe proporcionar al menos un criterio para realizar la consulta",4);
    		cargaFecha();
    		return;
    	} else if (fechaIni != "") {
    		if (fechaIni != "" && fechaFin == "") {
    			cuadroDialogo("Al ingresar fecha inicial debe ingresar fecha final para realizar la b&uacute;squeda",1);
    			return;
        	}else if(fcomp1 > fcomp2){
    			cuadroDialogo("La fecha inicial debe ser menor o igual a la fecha final",4);
    			cargaFecha();
    			return;
    		}else if(validaRango(3)){                
                cuadroDialogo("El periodo no puede ser mayor a 3 meses" ,4);
                cargaFecha();
    			return;
    		}
    	} else if (usuarioSol != "") {
    		if (usuarioSol.length != 8) {
    			cuadroDialogo("El usuario solicitante debe estar formado por 8 d&iacute;gitos" ,4);
    			return;
    		}
    	}
    	ejecutaSubmit();
    }
    
    function limpiaForma(){
    	cargaFecha();
    	document.FrmMov.usuarioSol.value = '';
    	document.FrmMov.folio.value = '';
    	document.FrmMov.estatus.value = '';	   
    }
    
    function ejecutaSubmit(){
    	document.FrmMov.action = "ConsultaHistEdoCtaServlet";
    	document.FrmMov.submit();
    }
    
     function seleccionaFecha(opcion){
		opcionCalendario = opcion;
        var dia = document.Frmfechas.strDia.value;		//MARL BEGIN
		var mes = document.Frmfechas.strMes.value-1;	// INC. IM135515.
		var ann = document.Frmfechas.strAnio.value;		//MARL END
		
		if(opcion==1){	
			var fechaFin= new Date(ann,mes,dia);
			var fechaIni= new Date(ann,mes-18,dia);
			var diaI=fechaIni.getDate();
			var mesI=fechaIni.getMonth();
			var anioI=fechaIni. getFullYear();
			var diaF=fechaFin.getDate();
			var mesF=fechaFin.getMonth();
			var anioF=fechaFin.getFullYear();
			
			
			
			var texto= "jsp/" +
					"CalendarioHTML.jsp?diaI="+diaI+"&diaF="+diaF+"&mesI="+mesI+"&mesF="+mesF+"&" +
					"anioI="+anioI+"&anioF="+anioF+"&" +
					"&diasInhabiles=" + escape(js_diasInhabiles) + "&mes=" + mes + "&anio=" +
						ann + "&priUlt=0#mesActual";
			var msg=window.open(texto,"calendario","toolbar=no," +
						"location=no,directories=no,status=no,menubar=no" +
						",scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();
		}else{
			var fechaInicioSeleccionada=document.FrmMov.fechaIni.value;
			var diaIniSelec=fechaInicioSeleccionada.substring(0,2);
			var mesIniSelec=fechaInicioSeleccionada.substring(3,5);
			var anioIniSelec=fechaInicioSeleccionada.substring(6,10);
			
			var fechaIniSelec=new Date(anioIniSelec,mesIniSelec-1,diaIniSelec);
			var fechaFinSelec= new Date(anioIniSelec,mesIniSelec-1,diaIniSelec);
			fechaFinSelec.setMonth(fechaIniSelec.getMonth()+3);			
						
			var diaI=fechaIniSelec.getDate();
			var mesI=fechaIniSelec.getMonth();
			var anioI=fechaIniSelec. getFullYear();
			var diaF=fechaFinSelec.getDate();
			var mesF=fechaFinSelec.getMonth();
			var anioF=fechaFinSelec.getFullYear();
			
			var hoy=new Date();
			
			if(fechaFinSelec>hoy){
				diaF=hoy.getDate();
				mesF=hoy.getMonth();
				anioF=hoy.getFullYear();
			}
			
			
			var texto= "jsp/" +
					"CalendarioHTML.jsp?diaI="+diaI+"&diaF="+diaF+"&mesI="+mesI+"&mesF="+mesF+"&" +
					"anioI="+anioI+"&anioF="+anioF+"&" +
					"&diasInhabiles=" + escape(js_diasInhabiles) + "&mes=" + mes + "&anio=" +
						ann + "&priUlt=0#mesActual";
			var msg=window.open(texto,"calendario","toolbar=no," +
						"location=no,directories=no,status=no,menubar=no" +
						",scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();					
			
		}
		
	}

	function validaRango(meses) {
		var fechaIni=document.FrmMov.fechaIni.value;
		var fechaFin=new Date(document.FrmMov.fechaFin.value.substring(6,10),
							document.FrmMov.fechaFin.value.substring(3,5)-1,
							document.FrmMov.fechaFin.value.substring(0,2));
		var fechaFinR='';		
		var diaFR=fechaIni.substring(0,2);
		var mesFR=(fechaIni.substring(3,5)-0)+meses;
		var annFR=fechaIni.substring(6,10);
		if (mesFR>12) {
			annFR=(annFR-0)+1;
			mesFR=(0+(mesFR-12));
		}
		fechaFinR= new Date(annFR,mesFR-1,diaFR);		
		if (fechaFin > fechaFinR) {		
			return true;
		}else {
			return false;
		};
	}
   	
   	function cargaFecha(){
		var dia=document.Frmfechas.strDia.value;
		var mes=document.Frmfechas.strMes.value;
		var ann=document.Frmfechas.strAnio.value;
		document.FrmMov.fechaIni.value=dia+'/'+mes+'/'+ann;
		document.FrmMov.fechaFin.value=dia+'/'+mes+'/'+ann;	
	}
	
    function Actualiza()
    {
        if (opcionCalendario==1)
        	document.FrmMov.fechaIni.value=fecha_completa;
        else if (opcionCalendario==2)
        	document.FrmMov.fechaFin.value=fecha_completa;
    }
 
    var ctaselec;
    var ctadescr;
    var ctatipre;
    var ctatipro;
    var ctaserfi;
    var ctaprod;
    var ctasubprod;
    var tramadicional;
    var cfm;
    function PresentarCuentas()
    {
        msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
        msg.focus();
    }

    function actualizacuenta()
    {
        document.FrmMov.EnlaceCuenta.value=ctaselec+"@"+ctatipre+"@"+ctadescr+"@";
        document.FrmMov.textEnlaceCuenta.value=ctaselec+" "+ctadescr;
    }

    function MM_preloadImages()
    {   //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }
    function MM_swapImgRestore()
    {   //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    function MM_findObj(n, d)
    { //v3.0
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
        d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
    }
    function MM_swapImage()
    {   //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
    function ctaPorParametro()
    {
        var miTram = "<% if( request.getAttribute("EnlaceCuenta")!=null) out.print(request.getAttribute("EnlaceCuenta")); else out.print(""); %>";
        if( miTram!="" )
        {
            var numCta = "";
            var ctaDes = "";
            var tipRel = "";
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            numCta = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            ctaDes = miTram.substring( 0,miTram.indexOf("|") );
            miTram = miTram.substring( miTram.indexOf("|")+1 );
            tipRel = miTram.substring( 0,miTram.indexOf("|") );
            document.FrmMov.EnlaceCuenta.value     = numCta +"@"+ tipRel +"@"+ ctaDes +"@";
            document.FrmMov.textEnlaceCuenta.value = numCta +" "+ ctaDes;
        }//fin if
    }
	function esNumberico(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
    
<%
    if( request.getAttribute("newMenu")!=null )
        out.println(request.getAttribute("newMenu"));
%>

  </script>
  <link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
 </head>
 <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="cargaFecha();MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">
  <table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
   <tr valign="">
    <td width="">
<%
    if( request.getAttribute("MenuPrincipal")!=null )
        out.println(request.getAttribute("MenuPrincipal"));
%>
    </td>
   </tr>
  </table>
<%
    if( request.getAttribute("Encabezado")!=null )
        out.println(request.getAttribute("Encabezado"));
%>
  <table width="760" border="0" cellspacing="0" cellpadding="0">
   <form name = "Frmfechas">
			<input type ="hidden" name="strAnio" value="<%=request.getAttribute("anio") %>"/>
    	<input type ="hidden" name="strMes" value="<%=request.getAttribute("mes") %>"/>
			<input type ="hidden" name="strDia" value="<%=request.getAttribute("dia") %>"/>
   </form>
  <form name="FrmMov" method="post">
   <tr>
    <td align="center">
     <table width="600" border="0" cellspacing="2" cellpadding="3">
      <tr>
       <td class="tittabdat" colspan="2" align="center">Proporcione los datos para realizar la consulta</td>
      </tr>
        
	  <tr  bgcolor="#CCCCCC" align="left">
	  
		     <td class="textabdatobs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Folio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		     	<input type="text" name="folio" size="12" class="tabmovtexbol"  onBlur = "checkIntegerValue(this);" onKeyPress="return esNumberico(event);"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		
		     <td class="textabdatobs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usuario Solicitante:&nbsp;&nbsp;
		     	<input type="text" name="usuarioSol" size="12" class="tabmovtexbol" maxlength="8" onBlur = "checkIntegerValue(this);" onKeyPress="return esNumberico(event);"/></td>
	  </tr>
      
      <tr  bgcolor="#EBEBEB" align="left">
      
      	<td class="textabdatcla">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Inicial:&nbsp;&nbsp;
      		<input type="text" name="fechaIni" onblur="javascript:validaFormatoFecha();" size="12" class="tabmovtexbol"/>
        	<a href ="javascript:seleccionaFecha(1);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"/></a>
        </td>    
        
        <td class="textabdatcla">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Final:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	<input type="text" name="fechaFin" size="12" class="tabmovtexbol" readonly/>
           	<a href ="javascript:seleccionaFecha(2);"><img src="/gifs/EnlaceMig/gbo25410.gif" width="12" height="14" border="0" align="absmiddle"/></a>
        </td>
      </tr>
      <tr  bgcolor="#CCCCCC" align="left">
      	<td class="textabdatobs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Por Estatus&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      		<select name="estatus" class="tabmovtexbol">
            	<option value = "" selected>Seleccione Estatus</option>
            	<option value = "D">Disponible</option>
            	<option value = "P">Pendiente</option>
            	<option value = "I">Inexistente</option>
           </select>
        </td>
        <td class="textabdatobs">&nbsp;</td>
      </tr>
     </table>
     
     </br>
     
     <table width="160" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" height="25">
	   <tr id="enviar">
		 <td align="right" height="25"><a href="javascript:validaBusqueda(); "><img src="/gifs/EnlaceMig/gbo25220.gif" border="0"/></a></td>
         <td align="left"  height="25"><a href="javascript:limpiaForma();"><img src="/gifs/EnlaceMig/gbo25250.gif" border="0"/></a></td>
       </tr>
     </table>
    </td>
   </tr>
   <input type="hidden" name="FechaI" value=""/>
   <input type="hidden" name="FechaF" value=""/>
   <input type="hidden" name="opcion"  value="2"/>
  </form>
  </table>
 </body>
	<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	</head>
</html>