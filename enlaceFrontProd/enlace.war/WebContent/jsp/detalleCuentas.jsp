<%@page import="java.util.*"%>
<%@page import="mx.altec.enlace.beans.DetalleCuentasBean"%>
<%@page import="mx.altec.enlace.bo.BaseResource"%>

<html>
<head>
<title>Alta de empleados por archivo</title>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript" type="text/javascript" src="/EnlaceMig/passmark.js"></script>
<script type="text/javascript">

	function checkMsg(msg){
		if( msg != '' ){
			//alert(msg);
			cuadroDialogo(msg,1);
		}
	}

	function selectAll(){
		var form = document.forms["frmSelCta"];
		var len=form.elements.length;
		if(len >= 0){
			form.elements[0].checked = !form.elements[0].checked;
			for (var i=0; i<len ; i++){
			    if ( form.elements[i].type == "checkbox" )
			    	form.elements[i].checked = form.elements[0].checked;
			}
		}
	}

	function submit(){
		if( validarSeleccionado() ){
			var form = document.forms["frmSelCta"];
			recopilaCuentas();
			form.submit();
		}else{
			//alert('Debe seleccionar al menos una cuenta para proceder con el registro');
			cuadroDialogo("Debe seleccionar al menos una cuenta para proceder con el registro",1);
		}
	}

	function regresar(){


		document.location="/Enlace/enlaceMig/PrevalidadorServlet?opcion=1";

	}

	function validarSeleccionado(){
		var isSeleccionado = false;
		var rowsCount = document.getElementById('tblDat').rows.length;
		for (var i=0;i<rowsCount;i++) {
			if ( trim(document.getElementById('tblDat').rows[i].cells[1].innerHTML) != 'Cuenta' ){
				var inputs = document.getElementById('tblDat').rows[i].cells[0].getElementsByTagName('input');

				for( var j=0;j<inputs.length;j++ ){
					if( inputs[j] ){
						if (inputs[j].type == 'checkbox'){
							if ( inputs[j].checked ){
			 					isSeleccionado = true;
			 					break;
							}
						}
					}
				}
			}
		}
		return isSeleccionado;
	}

	function recopilaCuentas(){
		var aux = "";
		for (var i=0;i<document.getElementById('tblDat').rows.length;i++) {
			if ( trim(document.getElementById('tblDat').rows[i].cells[1].innerHTML) != 'Cuenta' ){
				var inputs = document.getElementById('tblDat').rows[i].cells[0].getElementsByTagName('input');
				for( var j=0;j<inputs.length;j++ ){
					if( inputs[j] ){
						if (inputs[j].type == 'checkbox'){
							if ( inputs[j].checked ){
								aux = aux + trim(document.getElementById('tblDat').rows[i].cells[1].innerHTML) + '@';
							}
						}
					}
				}
			}
		}
		var hdn = document.getElementById( 'ctas' );
		hdn.value = aux;
	}

	function trim(cadena)
	{
		for(var i=0; i<cadena.length; )
		{
			if(cadena.charAt(i)==" ")
				cadena=cadena.substring(i+1, cadena.length);
			else
				break;
		}

		for(var i=cadena.length-1; i>=0; i=cadena.length-1)
		{
			if(cadena.charAt(i)==" ")
				cadena=cadena.substring(0,i);
			else
				break;
		}

		return cadena;
	}

	/***********************************************************************/

	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}

	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}

	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}

	/********************************************************************************/


	<% if (request.getAttribute("newMenu") != null ) { %>
	<%= request.getAttribute("newMenu") %>
	<%	} %>


</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<%
	   Object obj = null;
	   obj = request.getAttribute("msg");
	   String msg = ( obj != null )?obj.toString():"";

	   obj = null;
	   obj = request.getAttribute("contrato");
	   String contrato = ( obj != null )?obj.toString():"";

	   obj = null;
	   obj = request.getAttribute("nombre");
	   String nombre = ( obj != null )?obj.toString():"";

	   List list = (List)session.getAttribute("listaDetalles");
	   int size = (list != null)?list.size():0;//Tamanio lista con datos de cuentas a registrar
	%>

<body onload="javascript:checkMsg('<%= msg%>');">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	<tr valign="top">
		<td width="*">
			<%-- MENU PRINCIPAL --%>
			<% if (request.getAttribute("MenuPrincipal") != null ) { %>
			<%= request.getAttribute("MenuPrincipal") %>
			<%	} %>
		</td>
	</tr>
</TABLE>

<% if (request.getAttribute("Encabezado") != null ) { %>
<%= request.getAttribute("Encabezado") %>
<%	} %>

	<% if( size > 0 ){ %>

	<table width="760">
	<tr>
	<td>

	<center>

	<form method="POST" action="DetalleCuentas" name="frmSelCta" id="frmSelCta">
	<input type="hidden" id="ctas" name="ctas" value="" />
	<table cellspacing="2" cellpadding="0" border="0" id="tblDat">
		<tbody>
			<tr>
				<td class="tittabdat" width="65" align="right">
					Todas
					<input type="checkbox" onclick="javascript:selectAll();" value="checkbox" name="allbox" />
				</td>
				<td class="tittabdat" align="center" width="100">Cuenta/M&oacutevil </td>
				<td class="tittabdat" align="center" width="350">Descripción </td>
			</tr>

			<%

			DetalleCuentasBean bean = null;
			    for ( int i=0;i< size;i++ ){
			        String bgFila = ( (i+1)%2 == 0 )?"#EBEBEB":"#CCCCCC";
			        String tdClass = ( (i+1)%2 == 0 )?"textabdatcla":"textabdatobs";
			        bean = (DetalleCuentasBean) list.get(i);
			        String nom = bean.getNombre() + " " +bean.getaPaterno()+ " "  + bean.getaMaterno();
			        String cuenta = "" + bean.getCuenta();

			%>
			<tr bgcolor="<%= bgFila%>">
				<td class="<%= tdClass%>"  align="right">
				    <input type="checkbox" value="chk" name="chkCta<%=cuenta %>" id="chkCta<%=cuenta %>" />
				</td>
				<td class="<%= tdClass%>" align="center"><%= cuenta %> </td><%-- Cuenta --%>
				<td class="<%= tdClass%>">&nbsp;&nbsp;<%= nom %> </td><%-- Contrato --%>
			</tr>
			<%  }  %>
		</tbody>
	</table>
	<br/>

	<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td align="center">
			<%
			boolean conFacultad = false;
			HttpSession sess = request.getSession ();

			if(sess != null){
				BaseResource brsess = (BaseResource) sess.getAttribute ("session");
				if(brsess != null)
					conFacultad = brsess.getFacultad("INOMENVIEMP");
			}
			if(conFacultad){ %>
			<a href="javascript:submit();"> <img alt="Registrar" src="/gifs/EnlaceMig/registrarCuentas1.GIF" border="0" /> </a>
			<%}%>
			<a href="javascript:regresar();"> <img alt="Registrar" src="/gifs/EnlaceMig/gbo25320.gif" border="0" /> </a>


		</td>
	</tr>
	</table>
	</form>

	</center>


	</td>
	</tr>
	</table>
	<% } %>
	<%request.removeAttribute("msg"); %>
</body>
</html>