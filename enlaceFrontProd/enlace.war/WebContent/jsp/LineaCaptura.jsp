<html>
<head>
<title>Pago SAR</TITLE>

<meta name="Generador" content="EditPlus">
<meta name="Autor" content="Juan Pablo Pe�a">
<meta name="Descripcion" content="Se encarga de mostrar el applet validador de la l�nea de captura, as� como enviar los datos necesarios para realizar la consulta de dependencias.">
<meta name="Version" content="1.0">


<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
<script language="javaScript" src="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript">

function getValue(){

   window.alert(document.CaptureLineWindow.getStatusLineCaptureValidate());										//***Descomentado
   document.frmVerifica.LineCaptureValidate.value = document.CaptureLineWindow.getStatusLineCaptureValidate();	//***Descomentado
   return true;
 }

function CuentasCargo()
{
   opcion=1;
   //PresentarCuentas();
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();

}



function formatDecimal(argvalue, addzero, decimaln)
{
  var numOfDecimal = (decimaln == null) ? 2 : decimaln;
  var number = 1;

  number = Math.pow(10, numOfDecimal);

  argvalue = Math.round(parseFloat(argvalue) * number) / number;
  argvalue = "" + argvalue;

  if (argvalue.indexOf(".") == 0)
    argvalue = "0" + argvalue;

  if (addzero == true) {
    if (argvalue.indexOf(".") == -1)
      argvalue = argvalue + ".";

    while ((argvalue.indexOf(".") + 1) > (argvalue.length - numOfDecimal))
      argvalue = argvalue + "0";
  }

	  return argvalue;
}

function valida_linea()
{
//AQUI SE MANEJA EL VALOR NULO QUE REGRESA EL APPLET CUANDO EL USUARIO DA CLIC EN CONTINUAR Y NO HA INTRODUCIDO NINGUN DATO DE LA L�NEA. SI NO SE HACE ESTO, AL LLAMAR AL CGI O LO QUE SE UTILICE COMO MANEJADOR SE GENERA UN ERROR SI NO SE VALIDA.

var s = (document.CaptureLineWindow.getStatusLineCaptureValidate()==null)?"X":document.CaptureLineWindow.getStatusLineCaptureValidate();

   if (s=="")
   {

      	return false;

   }

   /*if(isInteger(s))
		return true;
   else
		return false;*/
	if (isNaN(s)){

		return	false;
	}
	else {

		return true;
	}

 return false;

 }

function isInteger(s)
{

  var i;

  for(i=0;i<s.length;i++)
  {

  var c=s.charAt(i);
     if(!isDigit(c))
        return false;

  }

   return true;
 }

function isDigit(c)
{

  return ((c>="0")&&(c<="9"));

 }

function fncEnviarDatosPago()
{
   
   if(valida_linea())
   {

		//LA LINEA EN ROJO SE CORRESPONDE  CON LA DE ARRIBA. ESTE METODO REGRESA LOS MENSAJES DE //ERROR QUE MUESTRA EL APPLET.

		// CUANDO EXISTE UN ERROR  EL APPLET RETORNA UN MENSAJE ALFANUMERICO, PERO SI ES CORRECTO, REGRESA LA LINEA DE CAPTURA COMPLETA. ES DECIR, AUNQUE EN LA ETIQUETA DIGA QUE FUE VALIDADA CON �XITO EN TEXTO, LO QUE REGRESA ES UNA CADENA CONSTRUIDA CON NUMEROS.
		document.frmVerifica.LineCaptureValidate.value =   		document.CaptureLineWindow.getStatusLineCaptureValidate();
		var valida = getValue(); //*** Agregado

		window.document.frmVerifica.method = "get";
		window.document.frmVerifica.action = "ConsultaUsuarios";
		window.document.frmVerifica.submit();
   }
   else
   {
    	cuadroDialogo("La l&iacute;nea de captura no es valida. "+document.frmVerifica.LineCaptureValidate.value, 3);

   }

 }

/**********************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
		if (request.getAttribute("newMenu")!= null)
			out.println(request.getAttribute("newMenu"));
%>

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/***********************************************************/


</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</head>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif')" background="/gifs/EnlaceMig/gfo25010.gif">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
    <!-- MENU PRINCIPAL -->
	<%
			if (request.getAttribute("MenuPrincipal")!= null)
				out.println(request.getAttribute("MenuPrincipal"));
	%>
   </td>
  </tr>
</table>

	<%
			if (request.getAttribute("Encabezado")!= null)
				out.println(request.getAttribute("Encabezado"));
	%>


 <FORM  NAME="frmVerifica" method=post onSubmit="return fncEnviarDatosPago()">
   <span class="textabref">   </span>
   <table width="621" border="0">
     <tr>
       <td width="11" height="421">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
       <td width="600"><span class="textabref">
         <applet code="com.procesar.sri.business.captureline.gui.LineaCapturaGUI.class"
         id="CaptureLineWindow"
         archive="Validador20.zip"
         name="ValidadorLC-200"
         height="400" width="680"
         codebase="/EnlaceMig/"
         mayscript="true">
         </applet>
       </span></td>
     </tr>
   </table>
   <table>
   <tr>
	<!--<td align=center><br><% //if(request.getAttribute("Botones")!=null) out.print(request.getAttribute("Botones")); %></td>-->

		<td width="320">&nbsp;</td>

      <td class="textabref" colSpan="6" width="231"><a href="javascript:fncEnviarDatosPago();"><img src='/gifs/EnlaceMig/gbo25310.gif' border=0 alt='Continuar'></a></td>
   </tr>
 </table>


   <INPUT TYPE="hidden" VALUE="" NAME="LineCaptureValidate">
   <INPUT TYPE="hidden" VALUE="1" NAME="Modulo">


 </form>

</body>
</html>
