<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page language="java"	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@page import="mx.altec.enlace.utilerias.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.*"%>

<%@ page import="mx.altec.enlace.dao.*" %>
<%@ page import="mx.altec.enlace.utilerias.*" %>

<html>
<head>
	<title>Detalle de Archivo</title>

<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript">
	<%
	System.out.println("Se ejecuta MTE_ImportaDetalle.jsp");

String nombreArchivoImp="";
String totalRegistros="";
String numeroCtasInternas="";
String numeroCtasNacionales="";
String numeroCtasInternacionales="";
String usuario="";

if(request.getParameter("totalRegistros")!=null) {
	totalRegistros=(String)request.getParameter("totalRegistros");
}
if(request.getParameter("nombreArchivoImp")!=null) {
	nombreArchivoImp=(String)request.getParameter("nombreArchivoImp");
}
if(request.getParameter("numeroCtasInternas")!=null) {
	numeroCtasInternas=(String)request.getParameter("numeroCtasInternas");
}
if(request.getParameter("numeroCtasNacionales")!=null) {
	numeroCtasNacionales=(String)request.getParameter("numeroCtasNacionales");
}
if(request.getParameter("numeroCtasInternacionales")!=null) {
	numeroCtasInternacionales=(String)request.getParameter("numeroCtasInternacionales");
}
if(request.getParameter("usuario")!=null) {
	usuario=Util.HtmlEncode((String)request.getParameter("usuario"));
}


/**
 * Correccion vulnerabilidades Deloitte
 * Stefanini Nov 2013
 */
if(Util.validaCCX(numeroCtasInternas, 1) && Util.validaCCX(numeroCtasNacionales, 1)
	&& Util.validaCCX(numeroCtasInternacionales, 1) && Util.validaCCX(totalRegistros, 1)){
	System.out.println("Entrando MMC_ImportaDetalle.");
	System.out.println("Leyendo archivo: " + nombreArchivoImp);
	System.out.println("Leyendo totalRegistros: " + totalRegistros);
	System.out.println("Leyendo numeroCtasInternas: " + numeroCtasInternas);
	System.out.println("Leyendo numeroCtasNacionales: " + numeroCtasNacionales);
	System.out.println("Leyendo numeroCtasInternacionales: " + numeroCtasInternacionales);
	System.out.println("Leyendo usuario: " + usuario);
	%>
<html>
<head>
<title>Detalle de Archivo</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript">
function BotonPagina(sigant) {
	document.TesImporta.pagina.value=sigant-1;
	document.TesImporta.submit();
}
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body background="/gifs/EnlaceMig/gfo25010.gif" bgcolor="#ffffff"
	leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<form name="TesImporta" method="Post"
		action="/Enlace/jsp/MMC_ImportaDetalle.jsp">
		<table width="770" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td align="center">
					<table border="0" width="760" cellspacing="0" cellpadding="0">
						<tr>
							<td><br /></td>
						</tr>
					</table>
					<table width="760" border="0" cellspacing="2" cellpadding="3"
						class="tabfonbla">
						<tr>
							<td class="tittabdat" align="center">&nbsp; Nombre de
								Archivo Importado</td>
							<td class="tittabdat" align="center">&nbsp; Total de
								Registros</td>
							<td class="tittabdat" align="center">&nbsp; Cuentas Internas
							</td>
							<td class="tittabdat" align="center">&nbsp; Cuentas Externas
								Nacionales</td>
							<td class="tittabdat" align="center">&nbsp; Cuentas
								Internacionales</td>
							<td class="tittabdat" align="center">&nbsp; Usuario</td>
						</tr>
						<tr>
							<td class="textabdatobs" nowrap align="center"><%= nombreArchivoImp.substring(nombreArchivoImp.lastIndexOf("/")+1)%></td>
							<td class="textabdatobs" nowrap align="center"><%=totalRegistros%></td>
							<td class="textabdatobs" nowrap align="center"><%=numeroCtasInternas%></td>
							<td class="textabdatobs" nowrap align="center"><%=numeroCtasNacionales%></td>
							<td class="textabdatobs" nowrap align="center"><%=numeroCtasInternacionales%></td>
							<td class="textabdatobs" nowrap align="center"><%=usuario%></td>
						</tr>
					</table> <%
		System.out.println("Abriendo archivo ... ");
	%>
					<table border="0" width="760" cellspacing="0" cellpadding="0">
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td class="textabref">Detalle de archivo</td>
						</tr>
					</table> <%
		System.out.println("Generando pantalla");
		System.out.println("Registros: " + totalRegistros);

		BufferedReader archImp = new BufferedReader( new FileReader(nombreArchivoImp) );
		MAC_Registro Registro=new MAC_Registro();

		int inicio=0;
		int fin=0;
		int totalReg=Integer.parseInt(totalRegistros);

		int pagina=0;
		if(request.getParameter("pagina")!=null) {
		   pagina=Integer.parseInt(request.getParameter("pagina"));
   		}

		int regPorPagina=Global.MAX_REGISTROS;
		inicio=(pagina*regPorPagina);
		fin=inicio+regPorPagina;

		if(fin > totalReg) {
			fin=totalReg;
		}

		int totalPaginas=calculaPaginas(totalReg,regPorPagina);
		int A=regPorPagina*(pagina+1);
		int de=(A+1)-regPorPagina;

		if((pagina+1)==totalPaginas) {
			A=totalReg;
		}

		System.out.println("Archivo abierto");

		String line="";
		String clase="";
		String fecha="";

		String cveBanco="";
		String cvePlaza="";
		String sucursal="";
		String tipoCuenta="";

		String cvePais="";
		String cveDivisa="";
		String ciudad="";
		String banco="";
		String cveABA="";

		Registro.valida=false;
	%>
					<table border="0" width="760" cellspacing="2" cellpadding="3"
						class="tabfonbla">
						<tr>
							<td colspan="12" align="left" class="textabdatcla">
								<table width="100%" class="textabdatcla">
									<tr>
										<td align="left" class="textabdatcla"><b>&nbsp;Pagina
												<font color="blue"><%=(pagina+1)%></font> de <font
												color="blue"><%=totalPaginas%></font>&nbsp;
										</b></td>
										<td align="right" class="textabdatcla"><b>Registros <font
												color="blue"><%=de%></font> - <font color="blue"><%=A%></font></b>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tittabdat" align="center">Tipo Cuenta</td>
							<td class="tittabdat" align="center">Cuenta/M&oacute;vil</td>
							<td class="tittabdat" align="center">Descripci&oacute;n</td>
							<td class="tittabdat" align="center">Clave Banco</td>
							<td class="tittabdat" align="center">Clave Plaza</td>
							<td class="tittabdat" align="center">Sucursal</td>
							<td class="tittabdat" align="center">Tipo Cuenta</td>
							<td class="tittabdat" align="center">Clave Pais</td>
							<td class="tittabdat" align="center">Clave Divisa</td>
							<td class="tittabdat" align="center">Ciudad</td>
							<td class="tittabdat" align="center">Banco</td>
							<td class="tittabdat" align="center">Clave ABA/SWIFT</td>
						</tr>
						<%
		//for( int a=0;a<Integer.parseInt(totalRegistros.trim());a++ )
		//while((line=archImp.readLine())!=null)
		for(int a=0;a<fin;a++) {
			line=archImp.readLine();
			if(a>=inicio) {
				Registro.parse(line);
				clase=(a%2==0)?"textabdatobs":"textabdatcla";

				if(Registro.tipoRegistro.equals(Registro.SAN)) {
					cveBanco="-";
					cvePlaza="-";
					sucursal="-";
					tipoCuenta="-";

					cvePais="-";
					cveDivisa="-";
					ciudad="-";
					banco="-";
					cveABA="-";
				} else if(Registro.tipoRegistro.equals(Registro.EXT)) {
					cveBanco=Registro.cveBanco;
					cvePlaza=Registro.cvePlaza;
					sucursal=Registro.sucursal;
					if( null == Registro.sucursal.trim() || ("").equals(Registro.sucursal.trim()) || ("0000").equals(Registro.sucursal.trim())) {
		        	   sucursal = "0001";
		        	   EIGlobal.mensajePorTrace("MMC_ImportaDetalle.jsp-> - importarArchivo(): Se coloca la sucursal por default 0001", EIGlobal.NivelLog.DEBUG);
		            }
		            if( null == Registro.cvePlaza.trim() || ("").equals(Registro.cvePlaza.trim()) || ("00000").equals(Registro.cvePlaza.trim())) {
		        	   cvePlaza = "01001";
		        	   EIGlobal.mensajePorTrace("MMC_ImportaDetalle.jsp-> - importarArchivo(): Se coloca la plaza por default 01001", EIGlobal.NivelLog.DEBUG);
		            }
					if (Registro.tipoCuenta == Registro.CLABE) {
						tipoCuenta="Cuenta ClaBE";
					}
					if (Registro.tipoCuenta == Registro.CREDITO) {
						tipoCuenta="Tarjeta de Cr&eacute;dito/D&eacute;bito";
					}
					cvePais="-";
					cveDivisa = (Registro.cveDivisa == null || Registro.cveDivisa.trim().isEmpty())
                            ? "-" : Registro.cveDivisa;
					ciudad="-";
					banco="-";
					cveABA="-";
				} else if(Registro.tipoRegistro.equals(Registro.INT)) {
					cveBanco="-";
					cvePlaza="-";
					sucursal="-";
					tipoCuenta="-";

					cvePais=Registro.cvePais;
					cveDivisa=Registro.cveDivisa;
					ciudad=Registro.ciudad;
					banco=Registro.banco;
					//cveABA=Long.toString(Registro.cveABA);
					cveABA=line.substring(154).trim();
				} else if(Registro.tipoRegistro.equals(Registro.MOV)) {
					Registro.tipoRegistro = "M&Oacute;VIL";
					cveBanco = "BANME".equals(Registro.cveBanco)?"-":Registro.cveBanco;
					cvePlaza="-";
					sucursal="-";
					tipoCuenta="-";

					cvePais="-";
					cveDivisa="-";
					ciudad="-";
					banco="-";
					cveABA="-";
				}
	%>
						<tr>
							<td class="<%=clase%>" align="center"><%=Registro.tipoRegistro%>&nbsp;</td>
							<td class="<%=clase%>" align="left"><%=Registro.cuenta%>&nbsp;</td>
							<td class="<%=clase%>" align="left"><%=Registro.nombreTitular%>&nbsp;<script></script></td>
							<td class="<%=clase%>" align="center"><%=cveBanco%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=cvePlaza%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=sucursal%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=tipoCuenta%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=cvePais%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=cveDivisa%>&nbsp;</td>
							<td class="<%=clase%>" align="left"><%=ciudad%>&nbsp;</td>
							<td class="<%=clase%>" align="left"><%=banco%>&nbsp;</td>
							<td class="<%=clase%>" align="center"><%=cveABA%>&nbsp;</td>
						</tr>
						<%
			}
		}
		archImp.close();
		System.out.println("MMC_ImportaDetalle.jsp-> Se genero la pantalla");
	%>
					</table> <%
		System.out.println("MMC_ImportaDetalle.jsp-> Se genera la paginacion");
		out.print(botones(totalReg,regPorPagina,pagina,totalPaginas));
	%>
					<table align="center" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><br /></td>
						</tr>
						<tr>
							<td align="center"><a href="javascript:window.close();"
								><img src="/gifs/EnlaceMig/gbo25200.gif"
									border="0" alt="Cerrar" /></a></td>
						</tr>
					</table>

				</td>
			</tr>
		</table>
		<input type="hidden" name="nombreArchivoImp"
			value="<%=nombreArchivoImp%>" /> <input type="hidden"
			name="totalRegistros" value="<%=totalRegistros%>" /> <input
			type="hidden" name="numeroCtasInternas"
			value="<%=numeroCtasInternas%>" /> <input type="hidden"
			name="numeroCtasNacionales" value="<%=numeroCtasNacionales%>" />
		<input type="hidden" name="numeroCtasInternacionales"
			value="<%=numeroCtasInternacionales%>" /> <input type="hidden"
			name="usuario" value="<%=usuario%>" /> <input type="hidden"
			name="pagina" value="<%=pagina%>" />
	</form>
	<%!
String botones(int totalRegistros,int regPorPagina,int pagina,int totalPaginas)
{
	StringBuffer botones=new StringBuffer("");

			if(totalRegistros>regPorPagina) {
				System.out.println("MMC_ImportaDetalle.jsp-> Estableciendo links para las paginas");
				botones.append("\n <br>");
				botones.append("\n <table width=620 border=0 align=center>");
				botones.append("\n  <tr>");
				botones.append("\n   <td align=center class='texfootpagneg'>");
				System.out.println("MMC_ImportaDetalle.jsp-> Pagina actual = "+pagina);

				if(pagina>0) {
					botones.append("&lt; <a href='javascript:BotonPagina("+(pagina)+");' class='texfootpaggri'>Anterior</a>&nbsp;");
				}

				if(totalPaginas>=2) {
	 				for(int i=1;i<=totalPaginas;i++) {
						if(pagina+1==i) {
							botones.append("&nbsp;");
							botones.append(Integer.toString(i));
							botones.append("&nbsp;");
						} else {
							botones.append("&nbsp;<a href='javascript:BotonPagina(");
							botones.append(Integer.toString(i));
							botones.append(");' class='texfootpaggri'>");
							botones.append(Integer.toString(i));
							botones.append("</a> ");
						}
					}
				}

				if(totalRegistros>((pagina+1)*regPorPagina)) {
					botones.append("<a href='javascript:BotonPagina("+(pagina+2)+");' class='texfootpaggri'>Siguiente</a> &gt;");
					botones.append("\n   </td>");
					botones.append("\n  </tr>");
					botones.append("\n </table>");
				}
			}
			return botones.toString();
		}

		int calculaPaginas(int total,int regPagina) {
			Double a1 = new Double(total);
			Double a2 = new Double(regPagina);
			double a3 = a1.doubleValue()/a2.doubleValue();
			Double a4 = new Double(total/regPagina);
			double a5 = a3/a4.doubleValue();
			int totPag=0;

			if(a3<1.0) {
				totPag=1;
			} else {
				if(a5>1.0) {
					totPag=(total/regPagina)+1;
				}
				if(a5==1.0) {
      				totPag=(total/regPagina);
				}
			}
			return totPag;
		}
	%>
</body>
<%
} else {
	%>
<head>
<title>Enlace</title>
<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">

			function VentanaAyuda(cad) {
			  return;
			}

			/******************  Esto  no no es mio ***************************************************/

			function MM_preloadImages() { //v3.0
			  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
			    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
			}

			function MM_swapImgRestore() { //v3.0
			  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}

			function MM_findObj(n, d) { //v3.0
			  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
			  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
			}

			function MM_swapImage() { //v3.0
			  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
			   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
			}

			/********************************************************************************/
var ventana;

function setFormLocation(){
	ventana = window.open('/pkmslogout','','width=1,height=1,toolbar=no,scrollbars=no,left=210,top=225');
	window.focus();
	window.setTimeout('cerrar();', 100);
	window.opener.document.location = '<%=Global.REDIR_LOGOUT_SAM%>';
	window.setTimeout('logoutSuperNet();', 150);
}

function setFormLocationCerrar(){
		ventana = window.open('/pkmslogout','','width=1,height=1,toolbar=no,scrollbars=no,left=210,top=225');
		window.focus();
		ventana.close();
		window.opener.document.location = '<%=Global.REDIR_LOGOUT_SAM%>';
}

function cerrar() {
ventana.close();
}


//Funcion que realiza el redireccionamiento al login de Enlace
function logoutSuperNet(){
	window.close();
}
	<%
		if(	session.getAttribute("newMenu") != null) {
			out.println( session.getAttribute("newMenu"));
		} else  if (request.getAttribute("newMenu")!= null) {
			out.println(request.getAttribute("newMenu"));
		}
	%>
		</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body background="/EnlaceMig/imagenes/gfo25010.gif" bgcolor="#ffffff"
	leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onBeforeUnload="setFormLocationCerrar();">
	<table border="0" cellpadding="0" cellspacing="0" width="571">
		<tr valign="top">
			<td width="*">
				<!-- MENU PRINCIPAL --> <%
		String menuprincipal = "";
		if(session.getAttribute("MenuPrincipal")!=null) {
			menuprincipal = (String)session.getAttribute("MenuPrincipal");
		} else  if (request.getAttribute("MenuPrincipal")!= null) {
			menuprincipal= (String)request.getAttribute("MenuPrincipal");
		}
		out.println(menuprincipal);
	%>
			</td>
		</tr>
	</table>
	<%
		String encabezado = "";
		if(session.getAttribute("Encabezado")!=null) {
			encabezado = (String)session.getAttribute("Encabezado");
		} else if (request.getAttribute("Encabezado")!= null) {
			encabezado= (String)request.getAttribute("Encabezado");
		}
		out.println(encabezado);
	%>
	<p></p>
	<table width="760" border="0" align="center"'>
		<tr>
			<td class="texenccon" align="center" valign="top">
				<table width="430" border="0" cellspacing="0" cellpadding="0"
					align="center">
					<tr>
						<td class="tittabdat" colspan="3"><img
							src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".."
							name="." /></td>

					</tr>
					<tr>
						<td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif"
							width="5" height="2" alt=".." name="." /></td>
					</tr>
					<tr>
						<td class="tittabdat" colspan="3"><img
							src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".."
							name="." /></td>

					</tr>
				</table>
				<table width="400" border="0" cellspacing="0" cellpadding="0"
					align="center">
					<tr>
						<td align="center">
							<table width="430" border="0" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td width="21" background="/gifs/EnlaceMig/gfo25030.gif"><img
										src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2"
										alt=".." name=".." /></td>
									<td width="428" height="100" valign="top" align="center">
										<table width="380" border="0" cellspacing="2" cellpadding="3"
											background="/gifs/EnlaceMig/gau25010.gif">
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
											<tr>
												<td class="tittabcom" align="center">
													<strong>Estimado Cliente.</strong>
													<br /><br />

													Para mayor seguridad, favor de comunicarse con su ejecutivo o bien al 01-800-509-5000
													y recibir� una correcta asesor�a.
												</td>
											</tr>
											<tr>
												<td align="center">
													<a href="javascript:setFormLocation();">
														<img src="/gifs/EnlaceMig/gbo25200.gif" border="0" alt="Cerrar" />
													</a>
												</td>
											</tr>
										</table>
									</td>
									<td width="21" background="/gifs/EnlaceMig/gfo25040.gif"><img
										src="/gifs/EnlaceMig/gau00010.gif" width="21" height="2"
										alt=".." name=".." /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table width="430" border="0" cellspacing="0" cellpadding="0"
					align="center">
					<tr>
						<td class="tittabdat" colspan="3"><img
							src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".."
							name="." /></td>

					</tr>
					<tr>
						<td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif"
							width="5" height="2" alt=".." name="." /></td>
					</tr>
					<tr>
						<td class="tittabdat" colspan="3"><img
							src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".."
							name="." /></td>

					</tr>
				</table> <!--
				<table width="430" border="0" cellspacing="0" cellpadding="0" align=center>
					<tr>
						<td align="center"><br>
							<table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
								<tr>
									<td align="center"><a href ="parURL">
										<img border="0" name="imageField32" src="/gifs/EnlaceMig/gbo25320.gif" width="83" height="22" alt="Regresar"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				 -->
			</td>
		</tr>
	</table>
</body>
<%
}
%>
</html>
