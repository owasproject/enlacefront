<jsp:useBean id='facultadesBajaCtas' class='java.util.HashMap' scope='session'/>

<%@page import="mx.altec.enlace.utilerias.*"%>

<!--
 @author  Alejandro Rada Vazquez
 @descripcion Modulo de Baja de Cuentas para Enlace Internet
//-->

<html>
<head>
	<title>Baja de cuentas</TITLE>
<meta http-equiv="Expires" content="1">
<meta http-equiv="pragma" content="no-cache">

<script language="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="javaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="javaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<!-- Estilos //-->
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<style type="text/css">
..componentesHtml
  {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size: 11px;
	  color: #000000;
  }
</style>


<script language="JavaScript">
/*************************************************************************************/
function MM_preloadImages() {
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() {
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/
function Actualiza(TipoCuenta)
  {
    var tot=0;
    var var1="";

	forma =document.BajaCuentas;

	if(TipoCuenta=="otros")
	 {
        var carTmp=forma.cmbCtasTerceros.options[forma.cmbCtasTerceros.selectedIndex].value;
       	var cuenta = carTmp.substring(carTmp.indexOf("=") + 1, carTmp.length);
		forma.Descripcion.value=carTmp.substring(0,carTmp.indexOf('+'));
		forma.Banco.value=cuenta.substring(0, 3);
		forma.Plaza.value=carTmp.substring(carTmp.indexOf('+')+1,carTmp.indexOf('-'));
		forma.Sucursal.value="";
	 }
	return;
  }

function Limpiar()
 {
	document.BajaCuentas.reset();
 }

function Agregar()
 {
   var forma=document.BajaCuentas;
   var valorRadio="";
   var cuenta="";
   var strTramaBN="";
   var valor = forma.cmbCtasTerceros.options[forma.cmbCtasTerceros.selectedIndex].value;
   valor = valor.substring(valor.indexOf("=") + 1, valor.length);
   for(i2=0;i2<forma.length;i2++)
    {
	  if(forma.elements[i2].type=='radio' && forma.elements[i2].checked==true)
	   {
		 valorRadio=forma.elements[i2].value;
		 if(valorRadio=="0" && (trimString(valor)=="" || trimString(valor)=="0"))
		  {
			 cuadroDialogo("Seleccione una cuenta para dar de baja.",3);
		  }
		 else
		  {
			if(valorRadio=="0")
			  {
				cuenta=trimString(valor);
				strTramaBN=trimString(forma.strTramaBN.value);
				if(strTramaBN.indexOf(cuenta)>=0)
				  {
					cuadroDialogo("La cuenta ya fue seleccionada.",3);
					return ;
				  }
			  }

			forma.action="CatNomInterbBaja";
			forma.Modulo.value="1";
			forma.submit();
		  }
	   }
	}
}
 function cambiaCombo ()
 {
	 var forma=document.BajaCuentas;
	 var valor = forma.cmbCtasTerceros.options[forma.cmbCtasTerceros.selectedIndex].value;
	 forma.descripcion.value = valor.substring(0, valor.indexOf("+"));
 }
function Enviar()
 {
	var forma=document.BajaCuentas;
	var contador=0;

	for(j=0;j<forma.length;j++)
	  if(forma.elements[j].type=='checkbox')
		if(forma.elements[j].checked)
		  contador++;

	if(contador==0)
	 {
	   cuadroDialogo("Debe seleccionar al menos una opci&oacute;n",1);
	 }
	else
	 {
		var cadena="";
		for(x=1;x<forma.length;x++)
		 {
			if(forma.elements[x].type=="checkbox")
			 if(forma.elements[x].checked)
			   cadena+="1";
			 else
			   cadena+="0";
		 }
		forma.cuentasSel.value=cadena;
		forma.Modulo.value="3";
		forma.action="CatNomInterbBaja";
		forma.submit();
	 }

 }

<!-- *********************************************** -->
<!-- modificación para integración pva 07/03/2002    -->
<!-- *********************************************** -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;
var opcion;

function CuentasSantander()
{
  opcion=1;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");msg.focus();
}
function CuentasOtrosBancos()
{
  opcion=2;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=2","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");  msg.focus();
}

function CuentasExtranjeras()
{
  opcion=3;msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=3","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");  msg.focus();
}

/*function actualizacuenta()
{
  if (opcion==1)
  {
	  document.BajaCuentas.cmbCtasPropias.value=ctaselec+"|"+ctadescr+"|";
      document.BajaCuentas.textcmbCtasPropias.value=ctaselec;
	  document.BajaCuentas.textcmbCtasPropiasDescripcion.value=ctadescr;
  }
  else if(opcion==2)
  {
	  document.BajaCuentas.cmbCtasTerceros.value=ctaselec+"|"+ctadescr+"@"+cfm+"!"+ctatipro +"$"+ctasubprod+"~"+ctaserfi+"%"+ctaprod;
      document.BajaCuentas.textcmbCtasTerceros.value=ctaselec;
      Actualiza("otros");
  }
  else if(opcion==3)
  {
      document.BajaCuentas.cmbCtasExtranjeras.value=ctaselec+"|"+ctatipre+"@"+cfm+"!"+ctatipro +"$"+ctasubprod+"~"+ctaprod+"%"+ctadescr;
      document.BajaCuentas.textcmbCtasExtranjeras.value=ctaselec;
	  Actualiza("extranjeros");
  }
}*/


/*************************************************************************************/
<%= request.getAttribute( "newMenu" ) %>
/*************************************************************************************/
</script>

</HEAD>


<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
		onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif',
		'/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif',
		'/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif',
		'/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif',
		'/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif',
		'/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif',
		'/gifs/EnlaceMig/gbo25011.gif')"
		background="/gifs/EnlaceMig/gfo25010.gif">


	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
	    <tr valign="top">
			 <td width="*">
				<!--  MENU PRINCIPAL -->
				<%= request.getAttribute( "MenuPrincipal" ) %>
			  </TD>
		</TR>
	</TABLE>
	<%= request.getAttribute( "Encabezado" ) %>


<FORM  NAME="BajaCuentas" method=post action="CatNomInterbBaja">

 <table width=760 border=0 cellpadding=0 cellspacing=0 >
  <tr>
   <td>

     <table align=center border=0 cellpadding=0 cellspacing=0 >

   <%
       
       EIGlobal.mensajePorTrace("CatNomInterbBaja.jsp Obteniendo cuentas BN.",EIGlobal.NivelLog.INFO);

      if (((Boolean) facultadesBajaCtas.get("BajaCtasBN")).booleanValue())
       {
   		EIGlobal.mensajePorTrace("CatNomInterbBaja.jsp Se tiene la facultad BN.",EIGlobal.NivelLog.INFO);
   %>
   <!-- ****************************************************************************************************** //-->
   <!-- Cuentas Nacionales //-->
   <!-- ****************************************************************************************************** //-->

       <tr>
         <td class="tittabdat">
		    <INPUT TYPE="radio" value=0 NAME="radioTipoCta" checked>&nbsp;<b>Cuentas Bancos Nacionales</b>
		 </TD>
       </TR>

	   <tr>
		<td class='textabdatcla'>
		  <table border=0 width="100%" class='textabdatcla' cellspacing=3 cellpadding=2>

		   <tr>
			 <td width=80 class="tabmovtex11" align="right"> &nbsp; Cuenta: &nbsp; </td>
          <td class='tabmovtex'>
		   <SELECT NAME="cmbCtasTerceros"  class="componentesHtml" onchange="cambiaCombo();"><option value=0 class='tabmovtex'> Seleccione una Cuenta &nbsp; </OPTION>
		       <%
			   String comboCuentas="";
		       if(request.getAttribute("comboCuentas")!=null)
				  comboCuentas=(String)request.getAttribute("comboCuentas");
			   %>
			   <%=comboCuentas%>
		   </SELECT>
		  </td>
		   </TR>
		   <TR>
			 <td width=80 class="tabmovtex11" align="right">Titular: &nbsp; </td>
			 <td class="tabmovtex11"><input type="text" name=descripcion class="componentesHtml" size=35 onfocus="blur();" value="" disabled></td>
		  
		  
<!--			 <td class="tabmovtex11">
			   <input type="text" name=textcmbCtasTerceros  class="componentesHtml" size=22 onfocus="blur();">
			   <A HREF="javascript:CuentasOtrosBancos();"><IMG SRC="/gifs/EnlaceMig/gbo25420.gif" border=0 align=absmiddle></A>
			   <input type="hidden" name="cmbCtasTerceros" value="">
			 </TD>  -->
			 
			 
		   </TR>
		  <tr><td colspan=2 class="tabmovtex11" align="right">&nbsp;</td></tr>
		</table>
	   </td>
	  </tr>

      <tr><td class="tabmovtex11" align="right">&nbsp;</td></tr>

   <%
       }

      /**GAE**/
       EIGlobal.mensajePorTrace("CatNomInterbBaja.jsp Obteniendo cuentas por lote de carga.",EIGlobal.NivelLog.INFO);
   	//revisar facultad
   	//if (((Boolean) facultadesBajaCtas.get("BajaCtasIA")).booleanValue()){
   		//EIGlobal.mensajePorTrace("CatNomInterbBaja.jsp Se tiene la facultad de baja de cuentas por lote de carga.",4);
   %>
	   <!-- ****************************************************************************************************** //-->
	   <!-- 		 //GAE 	Baja Masiva de Cuentas //-->
	   <!-- ****************************************************************************************************** //-->
	   <tr>
	   	   <td class="tittabdat">
		   	    <INPUT TYPE='radio' value=1 NAME='radioTipoCta'><b>Baja de Cuentas Masiva</b>
       	   </td>
	   </tr>
   	   <tr>
	       <td class='textabdatcla'>
		   	    <table border=0 width="100%" class='textabdatcla' cellspacing=3 cellpadding=2>
		        	<tr>
			        	<td width="80" class='tabmovtex11' align='right'>
			        		Folio de lote: &nbsp;
			        	</td>
					    <td>
			  				<input type=text name=folioBajaLote  class="componentesHtml" size=22 value="">
						</td>
						<td colspan=2 class='tabmovtex11' align='right'>
							&nbsp;
						</td>
		  			</tr>
		  			<tr>
						<td colspan=4 class='tabmovtex11' align='right'>
							&nbsp;
						</td>
		  			</tr>
			</table>
   		</td>
 	</tr>

   <%
       /**FIN GAE**/
       //}
       EIGlobal.mensajePorTrace("CatNomInterbBaja.jsp Finalizando pantalla.",EIGlobal.NivelLog.INFO);
   %>
   </TABLE>

	<tr><td colspan=4 class="tabmovtex11" align="right">&nbsp;</td></tr>

   </td>
  </tr>

  	<tr>
	 <td align=center>
	   <A href = "javascript:Agregar();" border = 0><img src = "/gifs/EnlaceMig/gbo25290.gif" border=0></a><A href = "javascript:Limpiar();" border = 0><img src = "/gifs/EnlaceMig/gbo25250.gif" border=0></a>
	 </td>
	</tr>

 </table>

<%
  String Modulo="0";

  String strTramaBN="";

  String tablaBN="";

  /**GAE**/
    String tablaLotes="";
    String errorLote = null;
  /**FIN GAE**/

  if(request.getAttribute("Modulo")!=null)
    Modulo=(String)request.getAttribute("Modulo");
  if(request.getAttribute("strTramaBN")!=null)
    strTramaBN=(String)request.getAttribute("strTramaBN");
  if(request.getAttribute("tablaBN")!=null && !request.getAttribute("tablaBN").equals(""))
    tablaBN=(String)request.getAttribute("tablaBN")+"<br><br>";

  /**GAE**/
  if(request.getAttribute("tablaLotes")!=null && !request.getAttribute("tablaLotes").equals(""))
  	tablaLotes=(String)request.getAttribute("tablaLotes")+"<br><br>";

  if(request.getAttribute("errorLote")!=null && !request.getAttribute("errorLote").equals("")){
  	errorLote = (String)request.getAttribute("errorLote");
  }else{
	errorLote = "";
  }
  /**FIN GAE**/
 %>

<br>
<table width=760 border=0 cellpadding=0 cellspacing=0 >
 <tr>
  <td width="100%"><%=tablaBN%></td>
 </tr>
</table>

<!-- GAE -->
<%if(!errorLote.equals("")){ %>
<script language="JavaScript">
	cuadroDialogo("<%=errorLote%>", 3);
</script>
<%}%>
<%if(!tablaLotes.equals("")){ %>
<table width=760 border=0 cellpadding=0 cellspacing=0 >
 <tr>
  <td width="100%"><%=tablaLotes%></td>
 </tr>
</table>
<%}%>
<!-- FIN GAE -->

 <input type="hidden" name="Modulo" value="<%=Modulo%>">
 <input type='hidden' name='strTramaBN' value="<%=strTramaBN%>">
 <input type=hidden name=cuentasSel >

 <%if(Modulo.trim().equals("1") && (!strTramaBN.equals("") || errorLote.equals("")))
  {
 %>
 <table width=760 border=0 cellspacing=0 cellpadding=0>
    <tr>
     <td align=center><A href = "javascript:Enviar();" border = 0><img src = "/gifs/EnlaceMig/gbo25520.gif" border=0 alt="Enviar"></a></td>
    </tr>
  </table>
  <%
   }
  %>

</form>

</BODY>
</HTML>
