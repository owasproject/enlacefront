<%@page contentType="text/html" buffer="none"%>
<%@page  import="mx.altec.enlace.bo.pdSucursal, java.util.*" %>
<%!
    boolean colorClaro = false;
    int view = 100;
    String getBgColor(){
	if(colorClaro)
	    return "textabdatcla";
	else
	    return "textabdatobs";
    }
%>
<%
java.util.Collection sucursales = new ArrayList();

if(null != session.getAttribute("pdSucursales") ){
    sucursales = (java.util.TreeSet) session.getAttribute("pdSucursales");
}
int start = 0;
if(null != request.getAttribute("start")){
    start = ((Integer) request.getAttribute("start")).intValue();
}
java.util.Collection permisos = (java.util.Collection) request.getAttribute("pdSucPermisos");
if( null == permisos ) permisos = new ArrayList();
%>
<html>
<head>
<title>Consulta de Cat&aacute;logo de Sucursales</title>
<meta http-equiv="Content-Type" content="text/html">
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language="JavaScript" src= "/EnlaceMig/cuadroDialogo.js"></script>
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
<script language='javascript'>

function jsConsultar(){
    document.frmSucursales.opcion.value="consultar";
    document.frmSucursales.submit();
}


function js_desplazamiento(index){
	document.frmSucursales.opcion.value = "desplazar";
	document.frmSucursales.start.value = index;
	document.frmSucursales.submit();
}
</script>

<!-- Esto no es mio -->
<script language='javascript'>

<%= request.getAttribute("newMenu")%>
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

</script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');"
background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado")%>
<form action='pdSucursales' method='post' name='frmSucursales' id='frmSucursales' >
	<input type="hidden" name='start' value='<%= start %>' >
	<input type="hidden" name='opcion' value='' >
	<table align= "center">
		<tr><td style='cellpadding: 5px;' align="center">
			<table width='150px' align="center">
				<tr><td class='tittabdat'>Ordenar por</td></tr>
				<tr><td class='textabdatcla'>
					<table cellpadding='10px' cellspacing='10px' align="center">
						<tr>
							<td align="center">
								<select name='orden' >
									<option value='cve' <%if( null == request.getAttribute("orden") && null == session.getAttribute("orden")) out.print("selected"); %>>Clave Sucursal</option>
									<option value='nombre'>Nombre</option>
									<option value='domicilio'>Domicilio</option>
									<option value='del'>Delegaci&oacute;n</option>
									<option value='ciudad'>Ciudad</option>
									<option value='edo'>Estado</option>
									<option value='cp'>C.P.</option>
								</select>
							</td>
						</tr>
					</table>
				</td></tr>
				<tr>
					<td align="center"><a href='javascript:jsConsultar()' border='0'><img src="/gifs/EnlaceMig/gbo25220.gif" border=0 alt="Consulta" width="90" height='22'></a></td>
				</tr>
			</table>
		</td></tr>
		<tr>
			<td align="center">
				<table>
					<%
					  /*  if(null != request.getAttribute("start") && !request.getAttribute("start").equals(new Integer(0))){
						start = ((Integer) request.getAttribute("start")).intValue();
						 }*/%>

					<tr>
						<td class="tittabdat">Clave</td>
						<td class="tittabdat">Nombre</td>
						<td class="tittabdat">Domicilio</td>
						<td class="tittabdat">Del/Mpio.</td>
						<td class="tittabdat">Ciudad</td>
						<td class="tittabdat">Estado</td>
						<td class="tittabdat">CP</td>
						<td class="tittabdat">Tel</td>
					</tr>
						<%
									 Iterator it = sucursales.iterator();//llaves.iterator();

									 int c = 0;
									 while(it.hasNext() && c < start){
										it.next();
										++c;
									  }
									 while(it.hasNext()){
										if( start > c ){
										 it.next();
										 continue;
									      }
									        else
									           if(c >= start + view){
										    break;
									             }

									pdSucursal suc = (pdSucursal) it.next();
						%>
					<tr>
						<td class='<%=getBgColor()%>'><%= suc.getCveSucursal()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getNombre()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getCalleNum()%>, <%= suc.getColonia()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getDelegacion()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getCiudad()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getEstado()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getCp()%>&nbsp;</td>
						<td class='<%=getBgColor()%>'><%= suc.getTel()%>&nbsp;</td>
					</tr><%
							colorClaro = !colorClaro;
							++c;
							 }%>
					</table>
			</td>
		<tr><td align="center">
			<%if(sucursales.size() > 0) {%><%= start + 1%> - <%= (start + view)>(sucursales.size())?sucursales.size():start + view %> de <%= sucursales.size()%><%}%>
		</td></tr>
		<tr><td align="center" class='texfootpaggri'>
				<%
				if(start - view >= 0){%><a href='javascript:js_desplazamiento(<%= start - view %>)' class='texfootpaggri'>anterior</a>&nbsp;<%}
				for(int i = 0;i < sucursales.size();i+= view){
					if(start != i){
						%><a href='javascript:js_desplazamiento(<%= i %>)' class='texfootpaggri'><%=(i /view) +1 %></a><%
						}else{
						%><%= ( i / view ) + 1%><%
						}%>&nbsp;<%
					}
				if(start + view < sucursales.size()){%>&nbsp;<a href='javascript:js_desplazamiento(<%= start + view %>)' class='texfootpaggri'>siguiente</a><%}%>
		</td></tr>
		<tr><td><%if (null != sucursales && !sucursales.isEmpty() ) { %><table align="center"><tr><td><a href='<%
					if (! permisos.contains("PADIREXPARC")){
						 out.print("javascript:cuadroDialogo(\"No tiene Facultad para exportar Archivos\");");
					} else if( ( null == session.getAttribute("pdSucursalesFile") ||
                        session.getAttribute("pdSucursalesFile").equals("") ) ){
						 %>javascript:cuadroDialogo("No se pudo exportar el archvo.",1)<%
					} else {%>/Download/<%=session.getAttribute("pdSucursalesFile")%>'<%}%> >
                    <img src='/gifs/EnlaceMig/gbo25230.gif' border=0 alt='Exportar' height='22' width='85'></a></td>
				<td><a href='javascript:self.print()' ><img src='/gifs/EnlaceMig/gbo25240.gif' border=0 alt='Imprimir' height='22' width='83'></a></td>
		<tr></table><%}%></td>
		</tr>
	</table>
</form>
</body>
</html>
<%
request.removeAttribute("start");
%>