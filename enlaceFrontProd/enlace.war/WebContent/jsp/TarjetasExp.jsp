<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<html>
	<head>
		<title>Detalle Asignacion Tarjetas</title>
	</head>
	<body>
	<%
		LMXE lmxe = (LMXE) request.getAttribute("TarjetasBean");
		if (lmxe != null && lmxe.getListaTarjetas() != null && lmxe.getListaTarjetas().size() > 0) {
			int numTarjetas = lmxe.getListaTarjetas().size();
	%>
		<table>
			<tr>
				<td align="left">
					Total de tarjetas: <%=numTarjetas%>
				</td>
			</tr>
			<tr>
				<td align="center">
					<table width="500" border="1">
						<tr>
							<td align="center" width="275" class="tittabdat">N&uacute;mero Tarjeta
							</td>
							<td align="center" width="275" class="tittabdat">Estatus
							</td>
						</tr>
						<%
						int tamano = lmxe.getListaTarjetas().size();

						Iterator iterator = lmxe.getListaTarjetas().iterator();

						while (iterator.hasNext()) {

							TarjetasBean tarjeta = (TarjetasBean) iterator.next();
						%>
						<tr>
							<td><%= tarjeta.getTarjeta()%></td>
							<td><%= tarjeta.getEstadoTarjeta() %></td>
						</tr>
						<%
						}
						%>

					</table>
				</td>
			</tr>
	</table>
	<%
			}else{
		%>
<table>
	<tr>
		<td align="center">
			<table border="1">
				<tr>
					<td align="center">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>
					</td>
				</tr>
			</table>
			<br/>
		</td>
	</tr>
</table>
<%	} %>
		<br>
	</body>
</html>