<%@ page import="mx.altec.enlace.beans.*,java.util.*"%>
<html>
	<head>
		<title>Detalle Vinculación Tarjetas</title>
	</head>
	<body>
	<% 
		if(session.getAttribute("tarjetasRes") != null){
		List tarjetas = (List) session.getAttribute("tarjetasRes");
			int numTarjetas = tarjetas.size();
	%>
		<table>
			<tr>
				<td align="left">
					Total de tarjetas: <%=numTarjetas%>
				</td>
			</tr>
			<tr>
				<td align="center">						
					<table width="500" border="1">					
						<tr>
							<td class="tittabdat"> N&uacute;mero de Tarjeta</td>
							<td class="tittabdat"> Contrato</td>
						    <td class="tittabdat"> Estatus</td>
						    <td class="tittabdat"> Fecha de Vinculaci&oacute;n</td>
						 	<td class="tittabdat"> Lote de Carga</td>
						 	<td class="tittabdat"> Cliente Enlace </td>
						</tr>								 
						<%
						        ListIterator it = tarjetas.listIterator();
						        int reg = 0;
						        TarjetaContrato tarjeta = null;
						        boolean claro = false;
						        while( it.hasNext()){
						            reg++;
						            tarjeta = (TarjetaContrato)it.next();
						            if(null == tarjeta){
						                continue;
						       	}
						    %>
						<tr>
						            <td><%= tarjeta.getTarjeta() %> &nbsp;</td>
						            <td><%= tarjeta.getContrato() %> &nbsp;</td>
						            <td><%= tarjeta.getEstatus() %>&nbsp;</td>
						            <td><%= tarjeta.getFechaUltAcceso()%>&nbsp;</td>
						            <td><%= tarjeta.getIdLote()	%> &nbsp;</td>
						            <td><%= tarjeta.getUsuarioReg()%>&nbsp;</td>
						        </tr>
						<%
						        claro = !claro;
						    }
						%>
						</tr>
						
					</table>
				</td>
			</tr>
	</table>
	<%
			}else{
		%>
<table>			
	<tr>
		<td align="center">		
			<table border="1">
				<tr>
					<td align="center">
						<br/>No se encontraron registros en la b&uacute;squeda<br/><br/>		
					</td>
				</tr>				
			</table>
			<br/>
		</td>
	</tr>
</table>
<%	} %>
		<br>
	</body>
</html>