<html>
<head>
<title> Fecha Server </title>
</head>
<%
String Afecha = "";
if(session.getAttribute("fechaServer")==null) {
	if(request.getAttribute("fechaServer")!=null)
		Afecha = (String)request.getAttribute("fechaServer");
	else
		Afecha = "";
} else
	Afecha = (String)session.getAttribute("fechaServer");
%>
 <body>
<SCRIPT LANGUAGE="JavaScript">
<!--
<%
String claveBco = "";
mx.altec.enlace.bo.BaseResource brs= (mx.altec.enlace.bo.BaseResource)session.getAttribute("session");
String location = "/EnlaceMig/impuestos/";


/*Modificado por: Miguel Cortes Arellano
* Fecha 03/09/2003
* Comienza codigo Synapsis
* Basicamente dependiendo del pago del usuario llamamos uno de los nuevos Applets.
* Esto depende del tipo de pago.
* Adherimos 2 nuevos tipos de pagos. Pagos de Creditos Fiscales (004) y Pagos Coordinados con Entidades. (012)
*/

if(session.getAttribute("satTipoPago") != null && session.getAttribute("satTipoPago").equals("001")){
    location += "AppletImpuestoEnlaceProv.html?";
    location += Afecha + "&bnc=" + brs.getClaveBanco();
}else if(session.getAttribute("satTipoPago") != null && session.getAttribute("satTipoPago").equals("005")){
    location += "AppletImpuestoEnlaceEjercicio.html?";
    location += Afecha + "&bnc=" + brs.getClaveBanco();
}else if(session.getAttribute("satTipoPago") != null && session.getAttribute("satTipoPago").equals("004")){
    location += "AppletImpuestoEnlaceCredFiscal.html?";
    location += Afecha + "&bnc=" + brs.getClaveBanco();
}else if(session.getAttribute("satTipoPago") != null && session.getAttribute("satTipoPago").equals("012")){
	location += "AppletImpuestoEnlaceCoordEntidad.html?";
	location += Afecha + "&bnc=" + brs.getClaveBanco();
}else if(session.getAttribute("satTipoPago") != null && session.getAttribute("satTipoPago").equals("010")){
	location += "AppletImpuestoEnlaceDerechos.html?" + Afecha;
}

//location += Afecha + "&bnc=" + brs.getClaveBanco();
mx.altec.enlace.utilerias.EIGlobal.mensajePorTrace( "InterApplet.jsp location=" + location,mx.altec.enlace.utilerias.EIGlobal.NivelLog.DEBUG);
//Finaliza codigo Synapsis
%>

document.location = "<%= location %>";
//-->
</SCRIPT>
</body>
</html>