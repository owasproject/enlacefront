<html>
<head>
	<title>Comprobante</title>
	<meta http-equiv="Content-Type" content="text/html;">
	<meta name="Codigo de Pantalla" content="s25390">
	<meta name="Proyecto" content="Portal Santander">
	<meta name="Ultima version" content="07/05/2001 09:00">
	<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
    <script language="JavaScript1.2" type="text/javascript" src="/EnlaceMig/Convertidor.js"></script>  <!--Cambio 7to8  20/04/2004 jbg-->
	<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>

<script language="javascript">
var contrato = "";
var usuario1 = "";
var folio_registro = "";
var concepto = "";
var rfc = "";
var iva = "";
var titular = "";
var beneficiario = "";
var banco = "";
var pais = "";
var ciudad = "";
var divisa_abono = "";
var tipo_cambioMN = "";
var importeUSD = "";
var importe_divisa = "";
var fecha_auto = "";
var folio_auto = "";
var orden = "";
var rastreo = "";
var usuario_auto = "";
var estatus = "";
var cuenta_origen ="";
var cuenta_destino = "";
var importeMN = "";

function Formatea_Importe(importe)
{
   decenas=""
   centenas=""
   millon=""
   millares=""
   importe_final=importe
   var posiciones=7;

   posi_importe=importe.indexOf(".");
   num_decimales=importe.substring(posi_importe + 1,importe.length)

   if (posi_importe==-1)
     importe_final= importe + ".00";

   if (posi_importe==4)
   {
     centenas=importe.substring(1, posiciones);
     millares=importe.substring(0,1);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==5)
   {
     centenas=importe.substring(2, posiciones + 1);
     millares=importe.substring(0,2);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==6)
   {
     centenas=importe.substring(3, posiciones + 2);
     millares=importe.substring(0,3);
     importe_final= millares + "," + centenas;
   }
   if (posi_importe==7)
   {
     centenas=importe.substring(4, posiciones + 3);
     millares=importe.substring(1,4);
     millon=importe.substring(0,1)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==8)
   {
     centenas=importe.substring(5, posiciones + 4);
     millares=importe.substring(2,5);
     millon=importe.substring(0,2)
     importe_final= millon + "," + millares + "," + centenas;
   }
   if (posi_importe==9)
   {
     centenas=importe.substring(6, posiciones + 5);
     millares=importe.substring(3,6);
     millon=importe.substring(0,3)
     importe_final= millon + "," + millares + "," + centenas;
   }
   return importe_final
}

</script>

<body bgcolor="#ffffff">
<form name="form1" method="post" action="">
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>

	  <%if(request.getAttribute("ClaveBanco")!=null)
	  {
		 if(request.getAttribute("ClaveBanco").equals("014"))
           out.println("<td rowspan=2 valign=middle class=titpag align=right width=35><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=0 ></TD>");
		 else if(request.getAttribute("ClaveBanco").equals("003"))
		  out.println("<td rowspan=2 valign=middle class=titpag align=right width=35><IMG SRC=\"/gifs/EnlaceMig/glo25040b.gif\" BORDER=0 ></TD>");
      }
	  %>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="6" cellpadding="0" align="center">
    <tr>
      <td width="247" valign="top" class="titpag">Comprobante de operaci&oacute;n</td>
   	  <td valign="top" class="titpag" height="40"><img src="/gifs/EnlaceMig/glo25030.gif" width="152" height="30" alt="Enlace"></td>
    </tr>
    <tr>
      <td valign="top" class="titenccom">
	  <% if(request.getAttribute("titulo")!=null) out.println(request.getAttribute("titulo"));%>
	  </td>
    </tr>
  </table>
  <table width="430" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="2" alt=".." name="."></td>
    </tr>
    <tr>
      <td class="tittabdat" colspan="3"><img src="/gifs/EnlaceMig/gau25010.gif" width="5" height="8" alt=".." name="."></td>
    </tr>
  </table>
  <input type="hidden" name="hdnVerTrama" value="">
  <script>
	trama = opener.document.form.<%=request.getParameter("hdnTrama")%>.value;

	document.form1.hdnVerTrama.value = trama;
	var tipo_oper = trama.substring(0,trama.indexOf('|'));
	trama=trama.substring(trama.indexOf('|')+1,trama.length);


	if (tipo_oper=="DITA")
	{
		contrato = trama.substring(0,trama.indexOf('|'));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		usuario1 = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		folio_registro = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		if(trama.substring(0,trama.indexOf("|")).length>60)
		{
			concepto = trama.substring(0,trama.indexOf("+"));
			trama=trama.substring(trama.indexOf('+')+1,trama.length);

			rfc = trama.substring(0,trama.indexOf(" "));
			trama=trama.substring(trama.indexOf(' ')+5,trama.length);

			iva = trama.substring(0,trama.indexOf("|"));
			iva = Formatea_Importe(iva);
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
		}
		else
		{
			concepto = trama.substring(0,trama.indexOf("|"));
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
			rfc = "";
			iva = "";

		}

		titular = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		beneficiario = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		banco = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		pais = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		ciudad = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		divisa_abono = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		tipo_cambioMN = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		importeUSD = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		importe_divisa = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		importeMN = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		cta_origen = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		cta_destino = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		fecha_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		folio_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		orden = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		rastreo = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		usuario_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		estatus = trama.substring(0,trama.length);
	}
	else if (tipo_oper=="DIPD")
	{
		cuenta_origen = trama.substring(0,trama.indexOf('|'));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		cuenta_destino = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		importeMN = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		importeUSD = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		tipo_cambioMN = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		if(trama.substring(0,trama.indexOf("|")).length>60)
		{
			concepto = trama.substring(0,trama.indexOf("+"));
			trama=trama.substring(trama.indexOf('+')+1,trama.length);

			rfc = trama.substring(0,trama.indexOf(" "));
			trama=trama.substring(trama.indexOf(' ')+5,trama.length);

			iva = trama.substring(0,trama.indexOf("|"));
			iva = Formatea_Importe(iva);
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
		}
		else
		{
			concepto = trama.substring(0,trama.indexOf("|"));
			trama=trama.substring(trama.indexOf('|')+1,trama.length);
			rfc = "";
			iva = "";

		}

		usuario1 = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		contrato = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		fecha_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		folio_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		orden = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		rastreo = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		usuario_auto = trama.substring(0,trama.indexOf("|"));
		trama=trama.substring(trama.indexOf('|')+1,trama.length);

		estatus = trama.substring(0,trama.length);
	}

	document.writeln("<table width='400' border='0' cellspacing='0' cellpadding='0' align='center'>");
	document.writeln("<tr>");
	document.writeln("<td align='center'>");
	document.writeln("<table width='430' border='0' cellspacing='0' cellpadding='0' align='center'>");
	document.writeln("<tr>");
	document.writeln("<td colspan='3'> </td>");
	document.writeln("</tr>");
	document.writeln("<tr>");
	document.writeln("<td width='21' background='/gifs/EnlaceMig/gfo25030.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width='21' height='2' alt='..' name='..'></td>");
	document.writeln("<td width='428' height='150' valign='top' align='center'>");

	if(tipo_oper=='DITA')
	{
		document.writeln("<table width='380' border='0' cellspacing='2' cellpadding='3' background='/gifs/EnlaceMig/gau25010.gif'>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Contrato:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(contrato + "</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Usuario que registro:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
        document.writeln(cliente_7To8(usuario1)+ "</td>");/*Cambio 7to8 21/04/2004 jbg*/

		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Ref. de Cargo:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>Por Confirmar</td>");
		document.writeln("</tr>");
		document.writeln("<tr> ");
		document.writeln("<td class='tittabcom' align='right' width='0'>Concepto:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(concepto+"</td>");
		document.writeln("</tr>    ");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>IVA:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+iva+"</td");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>RFC Beneficiario:</td>");
		document.writeln("<td class='textabcom' align='left'>"+rfc+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Ordenante:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+cta_origen+ "  " +titular+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Beneficiario:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+cta_destino+ "  " +beneficiario+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Banco Receptor:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+banco+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Pa&iacute;s:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+pais+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Ciudad:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+ciudad+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Divisa:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+divisa_abono+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Tipo de cambio MN:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+tipo_cambioMN+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Importe en MN:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+importeMN+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Importe en USD:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+importeUSD+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Importe divisa:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+importe_divisa+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Fecha de autorizaci&oacute;n:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+fecha_auto+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>No. Orden:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+orden+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Ref. Rastreo:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+rastreo+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Usuario que autoriz&oacute;:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+cliente_7To8(usuario_auto)+"</td>");/*Cambio 7to8 21/04/2004 jbg*/
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Estatus:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+estatus+"</td>");
		document.writeln("</tr>");
		document.writeln("</table>");
	}
	else if (tipo_oper=="DIPD")
	{
		document.writeln("<table width='380' border='0' cellspacing='2' cellpadding='3' background='/gifs/EnlaceMig/gau25010.gif'>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Cuenta origen:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(cuenta_origen+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Cuenta destino:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(cuenta_destino + "</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Importe en MN:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+importeMN+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr> ");
		document.writeln("<td class='tittabcom' align='right' width='0'>Importe en USD:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(importeUSD + "</td>");
		document.writeln("</tr>    ");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Tipo de cambio MN:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>");
		document.writeln(tipo_cambioMN + "</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Concepto:</td>");
		document.writeln("<td class='textabcom' align='left'>"+concepto +"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>IVA:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+iva+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>RFC:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+rfc+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Usuario que registr&oacute;:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+cliente_7To8(usuario1)+"</td>");/*Cambio 7to8 21/04/2004 jbg*/
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Contrato:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+contrato+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Fecha de autorizaci&oacute;n:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+fecha_auto+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>No. Orden:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+orden+"</td>");
		document.writeln("</tr>");
		document.writeln("<tr>");
		document.writeln("<td class='tittabcom' align='right' width='0'>Usuario que autoriz&oacute;:</td>");
		document.writeln("<td class='textabcom' align='left' nowrap>"+cliente_7To8(usuario_auto)+"</td>"); /*Cabmio 7to8 21/04/2004 jbg*/
		document.writeln("</tr>");
	document.writeln("</table>");
	}
	document.writeln("</td>");
	document.writeln("<td width='21' background='/gifs/EnlaceMig/gfo25040.gif'><img src='/gifs/EnlaceMig/gau00010.gif' width='21' height='2' alt='..' name='..'></td>");
	document.writeln("</tr>");
	document.writeln("</table>");
	document.writeln("</td>");
	document.writeln("</tr>");
	document.writeln("</table>");
	document.writeln("<table width='430' border='0' cellspacing='0' cellpadding='0' align='center'>");
	document.writeln("<tr>");
	document.writeln("<td class='tittabdat' colspan='3'><img src='/gifs/EnlaceMig/gau25010.gif' width='5' height='8' alt='..' name='.'></td>");
	document.writeln("</tr>");
	document.writeln("<tr>");
	document.writeln("<td colspan='3'><img src='/gifs/EnlaceMig/gau25010.gif' width='5' height='2' alt='..' name='.'></td>");
	document.writeln("</tr>");
	document.writeln("<tr>");
	document.writeln("<td class='tittabdat' colspan='3'><img src='/gifs/EnlaceMig/gau25010.gif' width='5' height='2' alt='..' name='.'></td>");
	document.writeln("</tr>");
	document.writeln("</table>");

	if (tipo_oper=="DITA")
	{
		document.writeln("<table width='430' border='0' cellspacing='6' cellpadding='0' align='center'>");
		document.writeln("<tr><td class='textabcom' align='left'>");
		document.writeln("&nbsp;&nbsp;&nbsp;Esta operaci&oacute;n a&uacute;n no ha sido confirmada por el banco del beneficiario");
		document.writeln("</td></tr>");
		document.writeln("</table>");
	}

	document.writeln("<table width='430' border='0' cellspacing='0' cellpadding='0' align='center'>");
	document.writeln("<tr>");
	document.writeln("<td align='center'><br>");
	document.writeln("<table width='150' border='0' cellspacing='0' cellpadding='0' height='22'>");
	document.writeln("<tr>");
	document.writeln("<td align='right' width='83'>");
	document.writeln("<a href='javascript:scrImpresion();'><img src='/gifs/EnlaceMig/gbo25240.gif' width='83' height='22' border='0'></a>");
	document.writeln("</td>");
	document.writeln("<td align='left' width='71'><a href='javascript:;' onClick='window.close()'><img src='/gifs/EnlaceMig/gbo25200.gif' width='71' height='22' border='0'></a>");
	document.writeln("</td>");
	document.writeln("</tr>");
	document.writeln("</table></td>");
	document.writeln("</tr>");
	document.writeln("</table>");
	document.writeln("<br>");
	document.writeln("</form>");
	document.writeln("</body>");
	document.writeln("</html>");
</script>