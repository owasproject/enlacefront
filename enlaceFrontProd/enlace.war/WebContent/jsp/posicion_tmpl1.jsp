<html><!-- #BeginTemplate "/Templates/principal.dwt" -->
<head>
<!-- #BeginEditable "doctitle" -->
<title>Enlace</title>
<!-- #EndEditable -->

<!-- #BeginEditable "MetaTags" -->
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25100">
<meta name="Proyecto" content="Portal">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="27/04/2001 18:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->
<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>

<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Apr 23 20:21:38 GMT-0600 2001-->
<script language="JavaScript">
<!--

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
<%= request.getAttribute("newMenu") %>

//-->
</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff" onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25171.gif','/gifs/EnlaceMig/gbo25181.gif','/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');" background="/gifs/EnlaceMig/gfo25010.gif">

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
	<td width="*">
	<!-- MENU PRINCIPAL -->
	<%= request.getAttribute("MenuPrincipal") %></TD>
  </TR>
</TABLE>
<%= request.getAttribute("Encabezado") %>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<FORM NAME="fsaldo" METHOD="POST" onSubmit="return cuentachange()"  >
    <tr>
      <td align="center">
        <table width="278" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <tr>
            <td class="tittabdat" align="right" width="162">Saldo disponible cheques:</td>
            <td class="textabdatobs" align="right" width="98" nowrap>
			<%= request.getAttribute("disponible") %>
			</td>
          </tr>
          <tr>
            <td class="tittabdat" align="right">Saldo total Vista:</td>
            <td class="textabdatobs" align="right" nowrap>
			<%= request.getAttribute("vista") %>
			</td>
          </tr>
          <tr>
            <td class="tittabdat" align="right">Saldo total Plazo:</td>
            <td class="textabdatobs" align="right" nowrap>
			<%= request.getAttribute("totalValores") %>
			</td>
          </tr>
          <tr>
            <td class="tittabdat" align="right">Total de la Cartera:</td>
            <td class="textabdatobs" align="right" nowrap>
			<%= request.getAttribute("total") %>
			</td>
          </tr>
        </table>
        <br>
        <table width="602" border="0" cellspacing="2" cellpadding="3" class="tabfonbla">
          <tr>
            <td width="190" class="tittabdat" align="center">Instrumento</td>
            <td class="tittabdat" align="center" width="170">Instrucci&oacute;n
              al vencimiento</td>
            <td class="tittabdat" align="center" width="60">Tasa neta</td>
            <td class="tittabdat" align="center" width="50">Folio</td>
            <td class="tittabdat" align="center" width="90">Importe</td>
          </tr>
		  <%= request.getAttribute("posi") %>
         </table>
        <br>
        <table width="83" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
          <tr>
            <td align="center">

		<%
		String parVieneDeVista = request.getParameter("vieneDeVista");
		parVieneDeVista = (parVieneDeVista==null)?"javascript:history.go(-1)":"vista?ventana=0";
		%>

		<A HREF="<%= parVieneDeVista %>">
		   <IMG  border=0 SRC ="/gifs/EnlaceMig/gbo25320.gif" width=83 height=22 Alt=Regresar>
		   </A>
		   </TD>
		   <td align="center">
		   <a href="javascript:scrImpresion();" border=0>
		   <img  border="0" src="/gifs/EnlaceMig/gbo25240.gif">
		   </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </form>
</table>
<!-- #EndEditable -->
</body>
<!-- #EndTemplate --></html>
