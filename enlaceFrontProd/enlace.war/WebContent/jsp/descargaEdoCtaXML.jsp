<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="mx.altec.enlace.utilerias.EIGlobal" %>
<%@page import="mx.altec.enlace.beans.ConfEdosCtaArchivoBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>
	 <title>Descarga de Estados de Cuenta XML</title>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css" />
<link rel="stylesheet" href="EnlaceMig/consultas.css" type="text/css" />

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/descargaEdoCtaXML.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript" src="/EnlaceMig/ValidaFormas.js"></script>
<script type="text/javascript">

<%if(request.getAttribute("TiempoTerminado")!=null) out.print(request.getAttribute("TiempoTerminado"));%>

<%
       if (request.getAttribute("newMenu")!= null) {
       out.println(request.getAttribute("newMenu"));
       }
%>

<!-- modificacion para integracion pva 07/03/2002 -->
var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

var ventana;
var accion;
var xhr;

<%
  EIGlobal.mensajePorTrace("Verifica cuentaSeleccionada",EIGlobal.NivelLog.DEBUG);
  if (request.getAttribute("cuentaSeleccionada")!=null ) {
	 	out.print("ctaselec='"+request.getAttribute("cuentaSeleccionada")+"';");
	 	out.print("setTimeout(function(){actualiza();},500); ");
	 }
%>

function oculta() {
	document.getElementById("enviar").style.visibility="hidden";
	document.getElementById("enviar2").style.visibility="visible";
}

</script>


</head>
<body style="background-color: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);"
    leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onload="<%if ( request.getAttribute("URLEdoCtaXML")!= null ) {out.print("modificaBanderaCarga()");}%>">

<table WIDTH="760" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
<tr>
	<td>
		<table border="0" cellpadding="0" cellspacing="0" width="571">
		  <tr valign="top">
		   <td width="*">
			<c:if test="${not empty requestScope.MenuPrincipal}">
				${requestScope.MenuPrincipal}
			</c:if>
		   </td>
		  </tr>
		</table>
		<c:if test="${not empty requestScope.Encabezado}">
			${requestScope.Encabezado}
		</c:if>
		<form  name="Datos" method="post" action="" >
			<input type="hidden" name="hdCuentaNueva" id="hdCuentaNueva" value="" />
			<input type="text" id="idBanderaCarga" name="banderaCarga" value="0" style="display: none"/>
			<input type="hidden" name="Cuentas" id="Cuentas" value="" />
			<p></p>
			<table width="760" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" >
					<table width="560" align="center" border="0" cellspacing="0" cellpadding="3" >
					<tr>
						<td class="tittabdat" colspan="2">Seleccione la cuenta y el periodo que desea descargar</td>
					</tr>

					  	<%--
					  	<tr align="center">
					   		<td class="tabmovtex">Cuenta
					    		<input type="text" id="textCuentas" name="textCuentas"  class="tabmovtexbol" maxlength="22" size="22" onfocus="blur();"
					    		value='<request.getAttribute("cuentaSeleccionada") != null ? request.getAttribute("cuentaSeleccionada"):""%>' class="tabmovtex" />
					 			<input type="hidden" value=""/>
					 			<a href="javascript:PresentarCuentas();">
					 				<img alt="PresentarCuentas" src="/gifs/EnlaceMig/gbo25420.gif" border="0" align="absmiddle" />
					 			</a>
					 			<input type="hidden" name="Cuentas" id="Cuentas" value="" />
							</td>
					  	</tr>
						--%>

					<tr>
						<td class="textabdatcla" colspan="2">
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td align="right" class="tabmovtexbol" width="200" nowrap>Cuenta / Tarjeta:</td>
								<td width="200"><input type="text" name="filtroCuenta" id="filtroCuenta" style="width: 200px;"/></td>
								<td rowspan="2"><a title="Filtrar Cuentas" href="javascript:filtrarCuentas();">
										<img alt="Filtrar Cuentas" src="/gifs/EnlaceMig/Ir.png" style=" border: 0;" height="21" width="84" /></a>
								</td>
							</tr>
							<tr>
								<td width="200" align="right" class="tabmovtexbol" nowrap>Descripci&oacute;n Cuenta / Tarjeta:</td>
								<td width="200"><input type="text" name="filtroDesc" id="filtroDesc" style="width: 200px;"/></td>
							</tr>
							</table>
						</td>
					</tr>
					<tr><td width="560" class="textabdatcla" >&nbsp;</td></tr>
			 	</table>
			 	&nbsp;
			 	<table width="560" align="center" border="0" cellspacing="0" cellpadding="3" >
					<tr>
						<td class="textabdatcla">
							<table width="100%" border="0" cellspacing="0" cellpadding="5" >
							<tr align="center" >
								<td colspan="2" >
							  		<table width="450" cellspacing="0" cellpadding="0" border="0">
									<tbody><tr>
										<td width="100" align="right" class="tabmovtexbol" width="100" nowrap>
											Cuenta&nbsp;/&nbsp;Tarjeta:&nbsp;
										</td>
										<td colspan="2">
											<select style="width: 333px;" class="tabmovtex" id="textCuentas" name="textCuentas" onchange="AgregaPeriodos();">
												 <c:if test="${fn:length(listaCuentasEdoCta) gt 1}">
																	<option value="-1">
																		Seleccione una cuenta...
																	</option>
												</c:if>
											 	<c:choose>

													<c:when test="${not empty listaCuentasEdoCta}">

														<c:forEach items="${listaCuentasEdoCta}" var="cuenta">
															<option value="${cuenta.nomCuenta}">
																${cuenta.nomCuenta} ${cuenta.nombreTitular}
															</option>
														</c:forEach>
														<c:if test="${not empty requestScope.msgOption}">
															<option value="">
															${requestScope.msgOption}
															</option>
														</c:if>
													</c:when>
													<c:otherwise>
														<option value="-1">
															No se encontraron resultados...
														</option>
													</c:otherwise>
												</c:choose>

											</select>
										</td>
							        </tr></tbody>
							    	</table>
								</td>
							</tr>
							<tr align="center" class="textabdatcla" >
								<td align="center" class="tabmovtex" align="left" nowrap>
									<table width="200" align="center" border="0" cellspacing="2" cellpadding="3" >
										<tr>
											<td class="tabmovtex" align="right">
												<b>Tipo&nbsp;de&nbsp;XML:&nbsp;</b>
											</td>
											<td>&nbsp;</td>
											<td class="tabmovtex" align="left" nowrap>
												<c:choose>
													<c:when test="${not empty requestScope.tipoXML && requestScope.tipoXML eq 'I' }">
														<input type="RADIO" name="tipoXML" value="I" checked onClick="AgregaPeriodos();"/>Ingreso<br></br>
													</c:when>
													<c:otherwise>
														<input type="RADIO" name="tipoXML" value="I" onClick="AgregaPeriodos();"/>Ingreso<br></br>
													</c:otherwise>
												</c:choose>
												<c:choose>
													<c:when test="${not empty requestScope.tipoXML && requestScope.tipoXML eq 'E' }">
														<input type="RADIO" name="tipoXML" value="E" checked onClick="AgregaPeriodos();"/>Egreso
													</c:when>
													<c:otherwise>
														<input type="RADIO" name="tipoXML" value="E" onClick="AgregaPeriodos();"/>Egreso
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<c:choose>
								<c:when test="${not empty requestScope.problemaWS}">
									<tr align="center" class="textabdatcla" >
										<td colspan="2">
											${problemaWS}
										</td>
							  	   	</tr>
								</c:when>
								<c:otherwise>
									<tr align="center" class="textabdatcla" >
										<td colspan="2">Periodo&nbsp;
										<select id="slcPeriodos" name="slcPeriodos" >
											<option value="">&nbsp;</option>
											<%
												EIGlobal.mensajePorTrace("Lista periodosRecientes",EIGlobal.NivelLog.DEBUG);
												List periodosRecientes = (request.getAttribute("periodosRecientes")!=null) ?
												(List)request.getAttribute("periodosRecientes") : new ArrayList();
												EIGlobal.mensajePorTrace("Verifica periodoSeleccionado",EIGlobal.NivelLog.DEBUG);
												String periodoSeleccionado = (request.getAttribute("periodoSeleccionado")!=null) ?
												(String)request.getAttribute("periodoSeleccionado") : "";
									        	for(int contadorPeriodos = 0; contadorPeriodos < periodosRecientes.size(); contadorPeriodos++){
									        		String periodoTx = (String)periodosRecientes.get(contadorPeriodos);
									        		String selected = "";
									            	if(periodoTx == null  || periodoTx.length() == 0){
									                	continue;
									            	}
													String[] periodoTxSeparado = periodoTx.split(":::");
													if(periodoTxSeparado.length != 2){
														continue;
													}
													EIGlobal.mensajePorTrace("Verifica periodoSeleccionado igual al que selecciono previamente",EIGlobal.NivelLog.DEBUG);
													if (periodoSeleccionado.equals(periodoTxSeparado[0]) ){
														EIGlobal.mensajePorTrace("Seleccionado",EIGlobal.NivelLog.DEBUG);
														selected = "selected";
													}

													if (periodoTxSeparado[0].contains("@") && periodoTxSeparado[0].length() > 6) {
						    				%>
											<option value="<%=periodoTxSeparado[0]%>" <%=selected%>><%=periodoTxSeparado[1]%></option>
											<%
													}
												}
											%>
										</select>
										</td>
						  	   		</tr>
								</c:otherwise>
							</c:choose>
					  		<tr align="center" class="textabdatcla" >
								<td colspan="2">&nbsp;</td>
						  	</tr>
						</table>
						</td>
					</tr>

					</table>
					<table width="560" border="0" cellspacing="0" cellpadding="0" style="background-color: #FFFFFF" height="25">
						<tr id="enviar">
					    	<td colspan="2" align="center" style=" height: 25px;">
					    		<a title="El tiempo que tomar&aacute; la descarga del archivo depender&aacute; de su velocidad de conexi&oacute;n." href="javascript:EnviarForma();" onclick="javascript:oculta();" >
					    			<img alt="Descarga" src="/gifs/EnlaceMig/Descarga.gif" style=" border: 0;"/>
					    		</a>
					    	</td>
					 	</tr>
					 	<tr align="center"  id="enviar2" style="visibility:hidden" class="tabmovtex">
							<td colspan=2>Por favor espere, su transacci&oacute;n est&aacute; siendo procesada...</td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
		</form>
	</td>
</tr>

<div id="descarga" style="display:none;">
				<%
				  String URLEdoCtaXML = (request.getAttribute("URLEdoCtaXML")!= null)
				  							? (String)request.getAttribute("URLEdoCtaXML") : "";
				  request.getSession().setAttribute("URLEdoCtaXML",URLEdoCtaXML);
				  out.println("<input type='hidden' name='URLEdoCtaXML' value='"+URLEdoCtaXML+"' />");
				  if ( URLEdoCtaXML.trim().length()>0 ) {
				  		EIGlobal.mensajePorTrace("URLEdoCtaXML = ["+URLEdoCtaXML+"]",EIGlobal.NivelLog.DEBUG);
				        out.print("<script>"+
									"function modificaBanderaCarga() {"+
										"var banderaCarga = document.getElementById(\"idBanderaCarga\");"+
										"if (banderaCarga.value == \"0\") {"+
				        					"msg=window.open(\""+URLEdoCtaXML+"\",\"DescargaEDC\",\"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=400,height=200\",false);"+
				        					"msg.focus();"+
										"}"+
										"document.getElementById(\"idBanderaCarga\").value = \"1\";"+
									"}"+
				        		  "</script>");
				        EIGlobal.mensajePorTrace("Termina escritura de URL en jsp.",EIGlobal.NivelLog.DEBUG);
				  }
				%>
</div>

</TABLE>
</body>
</html>