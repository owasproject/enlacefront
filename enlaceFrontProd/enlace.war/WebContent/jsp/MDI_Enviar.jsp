<html>
<head>
	<title>Env&iacute;o y Resultado de Dep&oacute;sitos</TITLE>

<script type="text/javascript" src="/EnlaceMig/fw_menu.js"></script>
<script type="text/javascript" src="/EnlaceMig/scrImpresion.js"></script>
<script type="text/javascript" src="/EnlaceMig/cuadroDialogo.js"></script>
<script type="text/javascript">
//  PyMES .. Marzo 2015 
// Exportar a archivo plano TXT - CSV ARS para cambios Deploy
function exportarArchivoPlano() {

 	var test = document.getElementsByName("tipoExportacion");
    var sizes = test.length;
    var extencion;
    for (i=0; i < sizes; i++) {
            if (test[i].checked==true) {
        	extencion = test[i].value;    
        }
    }    	
	window.open("/Enlace/enlaceMig/ExportServlet?metodoExportacion=BEAN&em=ExportModel&nombreBeanExport=nombreBeanExportArch&tipoArchivo=" + extencion,"Bitacora","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=260");	
}

 // PyMES .. Marzo 2015
 
 
function VentanaAyuda(cad)
{
  return;
}

/******************  Esto no es mio ***************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/********************************************************************************/
// Funciones para deplegar la ventana con la informacion de la cuenta

var registrosTabla=new Array();
var camposTabla=new Array();
var totalCampos=0;
var totalRegistros=0;

function despliegaDatos(campo2)
 {
	 //modificacion csa : ass
   param1="&TransEnv="+formateaAmp(campo2);
   param2="Indice=0";
   param3="&NumContrato="+formateaAmp(document.tabla.NumContrato.value)+"&NomContrato"+formateaAmp(document.tabla.NomContrato.value);
   param4="&NumUsuario="+formateaAmp(document.tabla.NumUsuario.value)+"&NomUsuario="+formateaAmp(document.tabla.NomUsuario.value);
   param5="&ClaveBanco="+formateaAmp(document.tabla.ClaveBanco.value);
   
   param6="&tipoMoneda="+document.tabla.tipoMoneda.value;
   
   //fin modificacion csa : ass
   param1=formatea(param1);
   param3=formatea(param3);
   param4=formatea(param4);
   param5=formatea(param5);
  
  
   web_application=document.tabla.web_application.value;
	// vswf:meg cambio de NASApp por Enlace 08122008
   vc=window.open("/Enlace/"+web_application+"/MDI_Comprobante?"+param2+param1+param3+param4+param5+param6+"&origenComprobante=TIB",'trainerWindow','width=490,height=600,toolbar=no,scrollbars=yes');
   vc.focus();
 }

// csa : ass
function formateaAmp(para) {
  var formateado="";
  var car="";
  if(para.indexOf("&") != -1) { 
      for(a2=0;a2<para.length;a2++) {
    	  if(para.charAt(a2)=='&')
    	   car="%26";
    	  else
    	   car=para.charAt(a2);
    
    	  formateado+=car;
    	}
  } else {
    formateado = para;
  }
  return formateado;
 
}

function formatea(para)
 {
   var formateado="";
   var car="";

   for(a2=0;a2<para.length;a2++)
    {
	  if(para.charAt(a2)==' ')
	   car="+";
	  else
	   car=para.charAt(a2);

	  formateado+=car;
	}
   return formateado;
 }

function numeroRegistros(campo2)
    {
      totalRegistros=0;
      for(i=0;i<campo2.length;i++)
       {
         if(campo2.charAt(i)=='@')
           totalRegistros++;
       }
      //alert("Registros encontrados: "+ totalRegistros);
    }

function llenaTabla(campo2)
 {
    var strCuentas="";
    strCuentas=campo2;
    var vartmp="";

    if(totalRegistros>=1)
      {
        for(i=0;i<totalRegistros;i++)
         {
           vartmp=strCuentas.substring(0,strCuentas.indexOf('@'));
           registrosTabla[i]=vartmp;
           if(strCuentas.indexOf('@')>0)
             strCuentas=strCuentas.substring(strCuentas.indexOf('@')+1,strCuentas.length);
         }
      }
    separaCampos(registrosTabla);
 }

function separaCampos(arrCuentas)
 {
     var i=0;
     var j=0;
     var vartmp="";
     var regtmp="";

     for(i=0;i<=totalRegistros;i++)
       camposTabla[i]=new Array();

     totalCampos=0;
     if(totalRegistros>=1)
      {
        regtmp+=arrCuentas[0];
        for(i=0;i<regtmp.length;i++)
         {
           if(regtmp.charAt(i)=='|')
            totalCampos++;
         }
        for(i=0;i<totalRegistros;i++)
         {
           regtmp=arrCuentas[i];
           for(j=0;j<totalCampos;j++)
            {
              vartmp=regtmp.substring(0,regtmp.indexOf('|'));
              camposTabla[i][j]=vartmp;
              if(regtmp.indexOf('|')>0)
                regtmp=regtmp.substring(regtmp.indexOf('|')+1,regtmp.length);
            }
           camposTabla[i][j]=regtmp;
         }
      }
 }

function nueva(indice)
 {
   var TramaGlobal="";
   var i=0;

   for(i=0;i<=totalCampos;i++)
     TramaGlobal+=camposTabla[indice][i]+"|";
   TramaGlobal+="@";
   document.tabla.TransEnv.value=TramaGlobal;
 }

<%
	if(	session.getAttribute("strScript") !=null)
		out.println( session.getAttribute("strScript"));
	else
		out.println( request.getAttribute("strScript") );
%>
<%
	if(	session.getAttribute("newMenu") !=null)
		out.println( session.getAttribute("newMenu"));
	else
		out.println(request.getAttribute("newMenu"));
%>
</script>
<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css"/>
</head>

<body style=" bgcolor: #ffffff; background: url(/gifs/EnlaceMig/gfo25010.gif);" 
 leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

<table border="0" cellpadding="0" cellspacing="0" width="571">
  <tr valign="top">
   <td width="*">
	<%-- MENU PRINCIPAL --%>
	<%
	if(	session.getAttribute("MenuPrincipal") !=null)
		out.println( session.getAttribute("MenuPrincipal"));
	else
		out.println(request.getAttribute("MenuPrincipal"));
	%>
   </td>
  </tr>
</table>

<%
	if(	session.getAttribute("Encabezado") !=null)
		out.println( session.getAttribute("Encabezado"));
	else
		out.println(request.getAttribute("Encabezado"));
%>

 <FORM NAME="tabla" method="post" action="MDI_Comprobante">
 <input type="hidden" id="tipoMoneda" name="tipoMoneda" value="<%= request.getAttribute("infoTipoDivisa") %>" />
  <p>

  <table border=0 align=center>
   <tr>
    <td align="right" class="textabref">
     <%

	if(	session.getAttribute("totalRegistros") !=null)
		out.println( session.getAttribute("totalRegistros"));
	else
		out.println(request.getAttribute("totalRegistros"));

	%>&nbsp;
	 <%
	if(	session.getAttribute("importeTotal") !=null)
		out.println( session.getAttribute("importeTotal"));
	else
		 out.println(request.getAttribute("importeTotal"));
	 %>
	 &nbsp;
	<% if(request.getAttribute("infoTipoDivisa")!=null)  out.print(request.getAttribute("infoTipoDivisa")); %>
	</td>
   </tr>

   <tr>
    <td>
     <%

	if(	session.getAttribute("Tabla") !=null)
		out.println( session.getAttribute("Tabla"));
	else
		 out.println(request.getAttribute("Tabla"));
	%>
    </td>
   </tr>
  </table>

<%
String NomContrato = "";
String NumContrato = "";
String NumUsuario = "";
String NomUsuario = "";
String ClaveBanco = "";
String TotalTrans = "";

	if(session.getAttribute("NomContrato") !=null)
	  NomContrato= (String)session.getAttribute("NomContrato");
	else
	  NomContrato= (String)request.getAttribute("NomContrato");

	if(session.getAttribute("NumContrato") !=null)
	  NumContrato = (String)session.getAttribute("NumContrato");
	else
	  NumContrato = (String)request.getAttribute("NumContrato");

	if(session.getAttribute("NomUsuario") !=null)
	  NomUsuario = (String)session.getAttribute("NomUsuario");
	else
	  NomUsuario = (String)request.getAttribute("NomUsuario");

	if(session.getAttribute("NumUsuario") !=null)
	  NumUsuario = (String)session.getAttribute("NumUsuario");
	else
	  NumUsuario = (String)request.getAttribute("NumUsuario");

	if(session.getAttribute("ClaveBanco") !=null)
	  ClaveBanco = (String)session.getAttribute("ClaveBanco");
	else
	  ClaveBanco = (String)request.getAttribute("ClaveBanco");

	if(session.getAttribute("TotalTrans") !=null)
	  TotalTrans= (String)session.getAttribute("TotalTrans");
	else
	  TotalTrans= (String)request.getAttribute("TotalTrans");
%>

   <input type="hidden" name="TransEnv">
   <input type="hidden" name="NomContrato" value='<%=NomContrato%>'>
   <input type="hidden" name="NumContrato" value='<%=NumContrato%>'>
   <input type="hidden" name="NomUsuario"  value='<%=NomUsuario%>'>
   <input type="hidden" name="NumUsuario"  value='<%=NumUsuario%>'>
   <input type="hidden" name="TotalTrans"  value='<%=TotalTrans%>'>
   <input type="hidden" name="ClaveBanco"  value='<%=ClaveBanco%>'>

   <%
  String web_application = "";
  if(session.getAttribute("web_application")!=null)
	web_application = (String)session.getAttribute("web_application");
  else if (request.getAttribute("web_application")!= null)
	web_application= (String)request.getAttribute("web_application");
  web_application=web_application.trim();
  %>

  <input type="hidden" name="web_application" value = '<%=web_application%>'>
 </FORM>
 <p><center class='tabmovtex11'><a style="cursor: pointer;" >Exporta en TXT <input id="tipoExportacion" type="radio" value ="txt" name="tipoExportacion"/> </center>
 <p><center class='tabmovtex11'><a style="cursor: pointer;" >Exporta en XLS <input id="tipoExportacion" type="radio" value ="csv" checked name="tipoExportacion"/> </center>
 <p><center><A href = "javascript:scrImpresion();" style=" border: 0;"><img src = "/gifs/EnlaceMig/gbo25240.gif" style=" border: 0;"></a><a href="javascript:exportarArchivoPlano();"><img src="/gifs/EnlaceMig/gbo25230.gif"  border="0" alt="Exportar" width="80" height="22"/></a></center>

</body>
</html>