<!--
Nombre del formulario: "Forma"

Campos:
"Cuenta"			Lista		Cuentas pertenecientes al contrato
//"Periodicidad"	Radio	Valores posibles: "Diaria", "Semanal"
//"PerHoras"		Lista		Horas de 8:00 a 18:00
//"PerDias"			Lista		D"as de "Lunes" a "Viernes"
//"TipoDis"			Radio		Valores posibles: "LoConcentrado", "LoEspecificado"
"Dispersion"		Texto		Valor de moneda
"Archivo"			File		Archivo a importar
"CtaSel"			Radio		Selecciona una sola cuenta
"Accion"			Hidden		Indica la accion a tomar (alta, baja...)
"Trama"				Hidden		Lleva informacion al servlet (depende de la accion)
"Cuentas"			Hidden		Lleva informacion de todo el arbol mientras no se guarde

-->
<%@page import="mx.altec.enlace.bo.bc_CuentaProgFechas"%>

	<%@ page import="java.util.Vector" %>

	<%
		// --- Obtencion de parametros -----------------------------------------------------
		String accion = (String)request.getAttribute("Accion");
		String parMenuPrincipal = (String)request.getAttribute("MenuPrincipal");
		String parFuncionesMenu = (String)request.getAttribute("newMenu");
		String parEncabezado = (String)request.getAttribute("Encabezado");
		String parCuentas = (String)request.getAttribute("Cuentas");
		String parMensaje = (String)request.getAttribute("Mensaje");
		String parTrama = (String)request.getAttribute("Trama");
		String parDiasInhabiles = (String)request.getAttribute("ParDiasInhabiles");
		String parFecha = (String)request.getAttribute("Fecha");
		String parMovFechas = (String)request.getAttribute("Movfechas");
		String parFechaAyer = (String)request.getAttribute("FechaAyer");
		String parFechaPrimero = (String)request.getAttribute("FechaPrimero");
		//String parVarFechaHoy = (String)request.getAttribute("VarFechaHoy");
		String parFechaHoy = (String)request.getAttribute("FechaHoy");
		String parFechaDia = (String)request.getAttribute("FechaDia");
		String parComboMtoEst = (String)request.getAttribute("ComboMtoEst"); //comentado

		if(parMenuPrincipal == null) parMenuPrincipal = "";
		if(parFuncionesMenu == null) parFuncionesMenu = "";
		if(parEncabezado == null) parEncabezado = "";
		if(parCuentas == null) parCuentas = "";
		if(parMensaje == null) parMensaje = "";
		if(parTrama == null) parTrama = "";
		if(parDiasInhabiles == null) parDiasInhabiles = "";
		if(parFecha == null) parFecha = "";
		if(parMovFechas  == null) parMovFechas = "";
		if(parFechaAyer == null) parFechaAyer = "";
		if(parFechaPrimero == null) parFechaPrimero = "";
		//if(parVarFechaHoy == null) parVarFechaHoy = "";
		if(parFechaHoy == null) parFechaHoy = "";
		if(parFechaDia == null) parFechaDia = "";
		if(parComboMtoEst == null) parComboMtoEst = ""; //comentado

		// --- Se preparan otras variables -------------------------------------------------
		Vector cuentas;
		String mensError [] = new String[1];
		mensError[0]="";

		cuentas = ((new mx.altec.enlace.servlets.bcConFechasProg()).creaCuentas(parCuentas, request, mensError));
		int a, b;
	%>

<!-- COMIENZA CODIGO HTML -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<HTML>

<HEAD>
	<TITLE>Programaci&oacute;n de Operaci&oacute;n</TITLE>
	<META NAME="Generator" CONTENT="EditPlus">
	<META NAME="Author" CONTENT="Adrian Mayen">
	<LINK rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

	<!-- Scripts -->
	<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
	<script language="javascript" src="/EnlaceMig/scrImpresion.js"></script>
	<script language="JavaScript1.2" src="/EnlaceMig/cuadroDialogo.js"></script>
	<SCRIPT>
		//Obtenci�n de los respectivos dias Inhabiles y modificado para Integraci�n
		var js_diasInhabiles = "<%= request.getAttribute("diasInhabiles") %>"
		diasInhabiles = '<%= request.getAttribute("DiasInhabiles") %>';
		var indice =0;
		//--- calendarios y actualizaci�n de las cuentas seleccionadas ------------------
		var ctaselec;
		var ctadescr;
		var ctatipre;
		var ctatipro;
		var ctaserfi;
		var ctaprod;
		var ctasubprod;
		var tramadicional;
		var opcioncalendario;
		var cfm;
		var y=0;

		/*function actualizacuenta()
			{
			document.Forma.textdestino.value=ctaselec+" -- "+ctadescr;
			}*/


		/*function borrar()
			{
			confirma("La estructura va a ser borrada, &iquest;Desea continuar?");
			accionConfirma = 2;
			}*/

		function regresar()
			{
			document.Forma.Accion.value = "REGRESA";
			document.Forma.submit();
			}

		function eliminar()
			{
			var sel = ctaSel();
			if(sel == -1) {aviso("Debe seleccionar una cuenta para eliminar"); return;}

			confirma("Se va a borrar la cuenta seleccionada. &iquest;Desea continuar?");
			accionConfirma = 3;
			return;
			}

		function imprimir()
			{scrImpresion();}

		// --- Validaciones ----------------------------------------------------------------
		/*function Actualiza()
			{
			if(Indice==0)
				document.Forma.fechaInicial.value=Fecha[Indice];
			else
				document.Forma.fechaFinal.value=Fecha[Indice];
			}*/

		function selUno(num)
			{
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(a != num) document.Forma.CtaSel[a].checked = false;
			}

		function ctaSel()
			{
			var sel = -1;
			var max = document.Forma.CtaSel.length - 2;
			for(a=0;a<max;a++) if(document.Forma.CtaSel[a].checked) {sel = a; break;}
			return sel;
			}


		function condiciones()
			{
			if(document.Forma.textdestino == "") return;
	/*		if(!estaEnConcentracion(ctaselec))
				{
				document.Forma.TipoDis[0].disabled = true;
				document.Forma.TipoDis[1].checked = true;
				}
			else
				{
				document.Forma.TipoDis[0].disabled = false;
				}
			habImporte()*/
			}

		function imprimeUnaLinea(texto)
			{
			for(a=0;a<texto.length;a++)
				if(texto.substring(a,a+1) != " ")
					document.write(texto.substring(a,a+1));
				else
					{
					document.write("&");
					document.write("n");
					document.write("b");
					document.write("s");
					document.write("p");
					document.write(";");
					}
			}

		// --- Para cuadros de di�logo -----------------------------------------------------
		var respuesta = 0;
		var accionConfirma;
		var habilitaConfirma = true;

		function continua()
			{
			if(accionConfirma == 1)
				{
				if(respuesta == 1) agregar();
				}
			else if(accionConfirma == 2)
				{
				if(respuesta == 1)
					{
					document.Forma.Accion.value = "ELIMINA";
					document.Forma.submit();
					}
				}
			else
				{
				if(respuesta == 1)
					{
					//var a = creaTrama();
					document.Forma.Accion.value = "ELIMINA";
					document.Forma.Trama.value = document.Forma.CtaSel[ctaSel()].value;
					document.Forma.submit();
					}
				}
			habilitaConfirma = true;
			}

		function confirma(mensaje)
			{if(habilitaConfirma) cuadroDialogo(mensaje,2); habilitaConfirma = false;}

		function aviso(mensaje) {cuadroDialogo(mensaje,1);}

		function avisoError(mensaje) {cuadroDialogo(mensaje,3);}

		// --- Inicio y env"o --------------------------------------------------------------
		function inicia()
			{
			//	document.Forma.Periodicidad[0].checked = true;
			//	document.Forma.TipoDis[0].checked = true;
			//  document.Forma.PerDias.disabled = true;
			//	document.Forma.PerDias.selectedIndex = -1;
			//	condiciones();
			}


		// --- Funciones de men� -----------------------------------------------------------
		function MM_preloadImages()
			{ //v3.0
			var d=document;
			if(d.images)
				{
				if(!d.MM_p) d.MM_p=new Array();
				var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
				for(i=0; i<a.length; i++)
				if (a[i].indexOf("#")!=0)
					{d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
				}
			}

		function MM_swapImgRestore()
			{ //v3.0
			var i,x,a=document.MM_sr;
			for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
			}

		function MM_findObj(n, d)
			{ //v3.0
			var p,i,x;
			if(!d) d=document;
			if((p=n.indexOf("?"))>0&&parent.frames.length)
				{
				d=parent.frames[n.substring(p+1)].document;
				n=n.substring(0,p);
				}
			if(!(x=d[n])&&d.all) x=d.all[n];
			for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
			return x;
			}

		function MM_swapImage()
			{ //v3.0
			var i,j=0,x,a=MM_swapImage.arguments;
			document.MM_sr=new Array;
			for(i=0;i<(a.length-2);i+=3)
				if((x=MM_findObj(a[i]))!=null)
					{
					document.MM_sr[j++]=x;
					if(!x.oSrc) x.oSrc=x.src;
					x.src=a[i+2];
					}
			}

		<%= parFuncionesMenu %>

		function techoIsText(valor)
			{
			var x1=0;
			var cont=0;
			//Text1=document.Forma.valortecho.value;
			Text1=valor;
			for(x1=0;x1<Text1.length;x1++)
				{
				Text2 = Text1.charAt(x1);
				if((Text2>='0' && Text2<='9') || Text2=='.') cont++;
				}
			if(cont!=x1)
				{
				cuadroDialogo("El Techo presupuestal debe ser num&eacute;rico.",3);
				return false;
				}
			/*if(Text1.indexOf('.')<0)
				Text1+=".00";
			document.Forma.valortecho.value=Text1;
			return true;*/
			}

		function SeleccionaFecha(ind)
			{
			var m=new Date();
			n=m.getMonth();
			js_diasInhabiles = '<%= request.getAttribute("diasInhabiles") %>';
			Indice=ind;
			//alert("n= "+ n + ", Indice=" + Indice);
			/*msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=330,height=260");
			msg.focus();*/

			var parMovFechas1 = "<%= request.getAttribute("Movfechas") %>";
			var FrmFechasDia = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasMes = parMovFechas1.substring(0,parMovFechas1.indexOf("-"));
			parMovFechas1 = parMovFechas1.substring(parMovFechas1.indexOf("-") + 1);
			var FrmFechasAnio = parMovFechas1;

			dia = FrmFechasDia;//09;
			mes = FrmFechasMes;//10;
			anio = FrmFechasAnio;//2002;

			msg=window.open("/EnlaceMig/bc_Calendario.html#"+n,"calendario","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=340,height=260");
			msg.focus();
			}

		/*function cambiaFechas(ele)
			{
			if(ele.value=='techosi')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.FechaIni.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.FechaFin.value="<%= request.getAttribute("FechaDia") %>";
				}
			}

		function cambiaFechas(ele)
			{
			if(ele.value=='techo')
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaPrimero") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaAyer") %>";
				}
			else
				{
				document.Forma.fechaInicial.value="<%= request.getAttribute("FechaDia") %>";
				document.Forma.fechaFinal.value="<%= request.getAttribute("FechaDia") %>";
				}
			}*/

		function verificaFechas(){
			var TipoB="";
			var y=0;

			for(i1=0;i1<document.Forma.length;i1++)
			if(document.Forma.elements[i1].type=='radio')
				if(document.Forma.elements[i1].checked==true)
					{
					TipoB=document.Forma.elements[i1].value;
					break;
					}

			if(TipoB=="no")
				{
				if(mes1[0]>mes1[1] && anio1[0]==anio1[1])
					{
					cuadroDialogo("Error: La fecha Final no debe ser menor a la inicial.",3);
					return false;
					}
				if(mes1[0]==mes1[1] && anio1[0]==anio1[1])
					if(dia1[0]>dia1[1])
						{
						cuadroDialogo("Error: La fecha Inicial debe ser menor a la final.");
						return false;
						}
				}
			return true;
			}

			function fechaString(cadena){ // Elimina el "/" a la cadena de la fecha
			var fechaSin1 = "";
			var fechaSin2 = "";
			var ind;
			while ((ind = cadena.indexOf("/")) != -1){
				fechaSin1 = cadena.substring(0,ind);
				fechaSin2 = cadena.substring(ind+1, cadena.length);
				cadena = fechaSin1 + fechaSin2;
				ind = cadena.indexOf("/");
			}
			return cadena;
			}

		function ffocus(num){
			document.Forma.valortecho.focus();
			SeleccionaFecha(num);
		}

		/*function vigencia(){
			fecha = new Date();
			hora = fecha.getHours();
			//alert(fecha);
			if(hora >= 12){
				var dia  = fecha.getDate()+1;
				if (dia <= 9){
					dia  = "0" + dia;
				}
				var mes  = fecha.getMonth()+1;
				if (mes <= 9){
					mes  = "0" + mes;
				}
				var anio = fecha.getFullYear();
				var fechaFin = dia + "/" + mes + "/" + anio
				//alert("dia=" + dia + ", mes=" + mes + ", anio=" + anio);

				//alert("dia=" + dia + ", mes=" + mes + ", a&ntilde;o=" + anio);

				document.Forma.fechaInicial.value = fechaFin;
				document.Forma.fechaFinal.value = fechaFin;
			}else{
				document.Forma.fechaInicial.value="<%= parFechaDia %>";
				document.Forma.fechaFinal.value="<%= parFechaDia %>";
			}
		}*/

		<%= request.getAttribute("VarFechaHoy") %>   //del MCL_Movimientos
	</SCRIPT>

</HEAD>

<!-- CUERPO ==========================================================================-->
<BODY onload="inicia()" background="/gifs/EnlaceMig/gfo25010.gif" bgcolor=#ffffff>


	<form name = "Frmfechas">
		<%
		String FrmFechasAnio = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasMes = parMovFechas.substring(0,parMovFechas.indexOf("-"));
		parMovFechas = parMovFechas.substring(parMovFechas.indexOf("-") + 1);
		String FrmFechasDia = parMovFechas;
		%>

		<!-- input name=Fecha type=hidden value='<%--= request.getAttribute("FechaDia") --%>' size=10  onFocus='blur();' maxlength=10-->
		<Input type = "hidden" name ="strDia" value = "<%= FrmFechasAnio %>">
		<Input type = "hidden" name ="strMes" value = "<%= FrmFechasMes %>">
		<Input type = "hidden" name ="strAnio" value = "<%= FrmFechasDia %>">
	</form>

	<!-- MENU PRINCIPAL Y ENCABEZADO ---------------------------->
	<table border="0" cellpadding="0" cellspacing="0" width="610">
		<tr valign="top">
			<td width="*">
			<%= parMenuPrincipal %>
			</td>
		</tr>
	</table>
	<%= parEncabezado %>

	<FORM name="Forma" id="Forma" method="POST"  onSubmit="return inicia()" action="bcConFechasProg">
		<INPUT type=hidden name="Accion" value="">
		<INPUT type=hidden name="Trama" value="">
		<INPUT type=hidden name="Cuentas" value="<%= parCuentas %>">
		<!-- <INPUT type=Hidden name="Archivo" value=""> -->

		<DIV align=center>
		<!-- TABLA DE CUENTAS ------------------------------------------------------------->

		<TABLE width=650 border=0 cellspacing=1>
		<!-- <TR><TD class="tabmovtex"> Total de cuentas: <%= cuentas.size() %></TD></TR> -->
		</TABLE>

		<br>
		<TABLE width=500 class="textabdatcla" border=0 cellspacing=1 align="center">
			<TR>
				<TD class="tittabdat" align=center>Seleccione</TD>
				<TD class="tittabdat" align=center>No Cuenta</TD>
				<TD class="tittabdat" align=center>Peri&oacute;do de Programaci&oacute;n</TD>
				<TD class="tittabdat" align=center>Techo Presupuestal</TD>
				<TD class="tittabdat" align=center>Horario de programaci&oacute;n</TD>
			</TR>

			<!-- Esta secci�n se repite para cada cuenta -->
			<%
				//String espacio;
				String estilo;
				String horario;
				String formatoHorario;
				bc_CuentaProgFechas cta; // Se definio en modifica()
				String traeFechas;


				estilo = "textabdatobs";
				for(a=0;a<cuentas.size();a++)
					{
					estilo = (estilo.equals("textabdatobs"))?"textabdatcla":"textabdatobs";
					cta = (bc_CuentaProgFechas)cuentas.get(a);
			%>
				<TR>
					<TD class="<%= estilo %>" align=center><INPUT type=Radio name="CtaSel" value="<%=cta.getNumCta()%>|<%=cta.getVigencia()%>" onClick="selUno(<%= a %>)"></TD>
					 <!-- Cuenta -->
					<TD class="<%= estilo %>"><%= cta.getNumCta()%></TD>
					 <!-- Periodo de programacion-->
					<% traeFechas = cta.getVigencia();
						traeFechas = traeFechas.substring(3,traeFechas.length());
					%>
					<TD class="<%= estilo %>" align=center><%=traeFechas%></TD>
					 <!-- Techo Presupuestal -->
					<TD class="<%= estilo %>" align=center><%=(new mx.altec.enlace.servlets.BaseServlet()).FormatoMoneda(cta.getTechopre())%></TD>
					 <!-- Horario de programacion-->
					 <%
						horario = cta.getHorarios();
					    formatoHorario = horario.substring(0,5) + ":" + horario.substring(5,12) + ":" + horario.substring(12) ;
						formatoHorario = formatoHorario.substring(2,formatoHorario.length());
					 %>
					<TD class="<%= estilo %>" align=center><%= formatoHorario%></TD>
				</TR>
			<% } %>

			<TR>
		</TABLE>
		<BR>

		<!-- Botones Alta, Modificar---------------------->
			<TABLE>
				<TR align=center>
					<TD>
						<A href="javascript:imprimir()"><IMG border=0 src="/gifs/EnlaceMig/gbo25240.gif" alt="Imprimir"></A>
						<A href="javascript:eliminar()"><IMG border=0 src="/gifs/EnlaceMig/gbo25540.gif" alt="Borrar"></A>
					</TD>
				</TR>
			</TABLE>

		</DIV>

		<!-- Las siguientes dos l�neas son importantes, no borrar -->
		<INPUT type=Hidden name="CtaSel" value="X">
		<INPUT type=Hidden name="CtaSel" value="XX">


		<INPUT type=Hidden name="cuenta" value="">
	</FORM>
</BODY>

</HTML>

<% if(!parMensaje.equals("")) out.println("<SCRIPT>aviso('" + parMensaje + "');</SCRIPT>"); %>
