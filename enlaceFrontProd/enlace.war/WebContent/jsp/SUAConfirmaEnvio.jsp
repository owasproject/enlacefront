<%@page contentType="text/html"%>
<jsp:useBean id='Mensaje' class='java.lang.String' scope='request'/>

<%@page import="mx.altec.enlace.bo.FormatoMoneda"%>
<html>
<head>
<title>SUA Confirmacion de pago LC</title>
<script languaje="javaScript" SRC="/EnlaceMig/ValidaFormas.js"></script>
<script language="JavaScript1.2" src="/EnlaceMig/fw_menu.js"></script>
<script language = "JavaScript" SRC= "/EnlaceMig/cuadroDialogo.js"></script>

<script languaje="javaScript">

var fechaLib;
var fechaLim;
var modulo = 'Registro';

/******************************************************************/

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

/*************************************************************************************/

function VerificaFechas(txtLibramiento, txtLimite)
 {

 lib_year         = parseInt(txtLibramiento.value.substring(6,10),10);
 lib_month        = parseInt(txtLibramiento.value.substring(3, 5),10);
 lib_date         = parseInt(txtLibramiento.value.substring(0, 2),10);
 fechaLibramiento = new Date(lib_year, lib_month, lib_date);

 lim_year     = parseInt(txtLimite.value.substring(6,10),10);
 lim_month    = parseInt(txtLimite.value.substring(3, 5),10);
 lim_date     = parseInt(txtLimite.value.substring(0, 2),10);
 fechaLimite  = new Date(lim_year, lim_month, lim_date);

     if( fechaLibramiento > fechaLimite)
      {
        //alert("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.");
		cuadroDialogo("La Fecha de Libramiento no puede ser mayor a la Fecha Limite.", 3);
        return false;
      }

  return true;

 }


var ctaselec;
var ctadescr;
var ctatipre;
var ctatipro;
var ctaserfi;
var ctaprod;
var ctasubprod;
var tramadicional;
var cfm;

function PresentarCuentas()
{
  msg=window.open("cuentasSerfinSantander?Ventana=2&opcion=1","Cuentas","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=440,height=290");
  msg.focus();
}
function actualizacuenta()
{
  document.frmRegistro.CuentaCargo.value=ctaselec;
}

<%= request.getAttribute("newMenu") %>

</script>

<link rel="stylesheet" href="/EnlaceMig/consultas.css" type="text/css">

</HEAD>

<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff"
onLoad="MM_preloadImages('/gifs/EnlaceMig/gbo25131.gif','/gifs/EnlaceMig/gbo25111.gif','/gifs/EnlaceMig/gbo25151.gif','/gifs/EnlaceMig/gbo25031.gif','/gifs/EnlaceMig/gbo25032.gif','/gifs/EnlaceMig/gbo25051.gif','/gifs/EnlaceMig/gbo25052.gif','/gifs/EnlaceMig/gbo25091.gif','/gifs/EnlaceMig/gbo25092.gif','/gifs/EnlaceMig/gbo25012.gif','/gifs/EnlaceMig/gbo25071.gif','/gifs/EnlaceMig/gbo25072.gif','/gifs/EnlaceMig/gbo25011.gif');<%if (!Mensaje.equals("")) out.println("cuadroDialogo('" + Mensaje + "', 1)");%>"
background="/gifs/EnlaceMig/gfo25010.gif">			  <!-- modificar para cuando es NomBen y cuando es no registrado ya que lo esta tomando para nomben y este no existe obviamente --> <!-- <%--if (escribe && request.getAttribute("Escibe" ) == null) out.print ("frmRegistro.NomBen.value = " + Pago.getClaveBen () + ";");--%> -->


<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="571">
  <tr valign="top">
    <td width="*">
       <!-- MENU PRINCIPAL -->
       <%= request.getAttribute("MenuPrincipal") %></td>
  </tr>
</TABLE>
 <%= request.getAttribute("Encabezado") %>
<script type="text/javascript">
/*******************************
*Funcion para regresar a la 
*pantalla anterior
*<VC> LFER 18-08-2009
********************************/
function regresarLC(){
	
	window.history.back();
}
/*******************************
*Funcion para el llamado de Token 
y validacion de LC
*<VC> LFER 18-08-2009
********************************/
function enviarLC(){

 	document.ConfirmaLC.submit(); 	
	return;
}
</script>


<form name="ConfirmaLC"  method="post" action="SUALineaCapturaServlet?opcionLC=4">
<!-- input type="hidden" name="opctrans" value="1" -->
<!-- input type="hidden" name="valida" value="0" -->
<input type="hidden" value="<%=request.getAttribute("cuentaNombre")%>" name="cuentaNombre">
	<TABLE>
  		<TR>
  			<TD>
	   			<table width="860" border="0" cellspacing="0" cellpadding="0">
		  			<!--tr>
		    			<td colspan="3" align="center" width="596" valign="top">Contrato : <%=request.getAttribute("contrato") %></td>
		    		</tr-->
		    		<tr>
		    			<td colspan="3">&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td colspan="3" align="center" class="textabdatcla">Confirmaci&oacute;n de env&iacute;o</td>
		    		</tr>
		    		<tr>
		    			<td>
		    				<table width="100%" height="100%" >
		    					<tr class="tittabdat">
		    						<td><b>Cuenta cargo</b></td>
		    						<td width="160px"><b>Titular</b></td>
		    						<td><b>Fecha</b></td>
		    						<td><b>Importe</b></td>
		    						<td><b>L&iacute;nea de captura</b></td>
		    					</tr>
		    					<tr class="textabdatobs">
		    						<td><%=request.getAttribute("cargo")%><input type="hidden" value="<%=request.getAttribute("cargo")%>" name="cargo"> </td>
		    						<td><%=request.getAttribute("titular")%><input type="hidden" value="<%=request.getAttribute("titular")%>" name="titular"></td>
		    						<td><%=request.getAttribute("fecha")%><input type="hidden" value="<%=request.getAttribute("fecha")%>" name="fecha"></td>
		    						<td>
		    							
		    							<%if(request.getAttribute("importe")!=null){
			    						    try{
												double importeTotal=Double.valueOf(request.getAttribute("importe").toString()).doubleValue();%>
												<%=FormatoMoneda.formateaMoneda((importeTotal))%>
											<%}catch(NumberFormatException exception){%>
												<%=request.getAttribute("importe")%>
											<%}%>
		    							<%}%>
		    							<input type="hidden" value="<%=request.getAttribute("importe")%>" name="importe"></td>
		    						<td><%=request.getAttribute("lc")%><input type="hidden" value="<%=request.getAttribute("lc")%>" name="lc"></td>
		    					</tr>

		    				</table>
		    			</td>
		    		</tr>
		    		<%if(request.getAttribute("valida")!=null && request.getAttribute("valida").equals("false") ){ %>
	    			<tr>
	    				<td  align="center" class="tabtexcal2"><% if(request.getAttribute("validaMsj")!=null){%>
	    				<%=request.getAttribute("validaMsj")%>
	    				<%}else{%> 
	    				 Error Inesperado En validacion de Linea de Captura
	    				<%}%>
	    				</td>
	    			</tr>
		    		<%} %>
		    		<tr>
		    			<td align="center">
		    				<table border="0" cellspacing="0" cellpadding="0">
		    					<tr>
		    						<td valign="top">
		    						<%if(request.getAttribute("valida")!=null && request.getAttribute("valida").equals("false") ){ %>
		    							&nbsp;
		    						<%}else{%>
		    						<a href="javascript:enviarLC();"><img border="0" name="enviarLC" src="/gifs/EnlaceMig/gbo25520.gif" alt="Enviar"></a>
		    						<%} %>
		    						</td>

		    						<td valign="top">
		    							<a href="javascript:regresarLC();"><img border="0" name="limpiarLC" src="/gifs/EnlaceMig/gbo25320.gif" alt="Regresar"></a>
		    						</td>
		    						<td>&nbsp;</td>
		    					</tr>
		    				</table>
		    			</td>
		    		</tr>
		    	</table>
			</TD>
		</TR>
	</TABLE>
</form>
<%if(request.getAttribute("mensaje") != null) {%>			
			<script type="text/javascript">
			<%=request.getAttribute("mensaje")%>
			</script>
		<%}%>	
</body>
</html>
